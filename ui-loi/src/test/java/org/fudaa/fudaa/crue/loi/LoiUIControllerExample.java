/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.loi;

import java.awt.BorderLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.factory.LoiFactory;
import org.fudaa.fudaa.crue.loi.loiff.LoiUiController;

/**
 *
 * @author fred
 */
public class LoiUIControllerExample {

  public static void display(JComponent jc) {
    JFrame fr = new JFrame();
    fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    fr.setContentPane(jc);
    fr.setSize(500, 600);
    fr.setVisible(true);
  }

  public static void main(String[] args) {
    LoiFF loiFF = new LoiFF();
    loiFF.setType(EnumTypeLoi.LoiTQapp);
    LoiFactory.alimenteEvolutionFF(loiFF, new double[]{0, 2, 4, 6}, new double[]{0, 20, 40, 60});
    LoiUiController uiController = new LoiUiController();
    uiController.setEditable(true);
    uiController.setLoi(loiFF, CrueConfigMetierForTest.DEFAULT,false);
    JPanel pn = new JPanel(new BorderLayout(10, 10));
    pn.add(uiController.getPanel());
    pn.add(uiController.getToolbar(), BorderLayout.NORTH);
    pn.add(uiController.getTableGraphePanel(), BorderLayout.EAST);
    pn.add(uiController.getInfoController().getPanel(), BorderLayout.SOUTH);
    display(pn);
  }
}
