/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.loi;

import java.awt.BorderLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilCasier;
import org.fudaa.dodico.crue.metier.emh.LitUtile;
import org.fudaa.dodico.crue.metier.emh.PtProfil;
import org.fudaa.fudaa.crue.loi.casier.ProfilCasierLoiUiController;

/**
 *
 * @author fred
 */
public class ProfilCasierUIControllerExample {

  public static void display(JComponent jc) {
    JFrame fr = new JFrame();
    fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    fr.setContentPane(jc);
    fr.setSize(500, 600);
    fr.setVisible(true);
  }

  public static void main(String[] args) {
    DonPrtGeoProfilCasier profil = new DonPrtGeoProfilCasier(CrueConfigMetierForTest.DEFAULT);
    profil.addPtProfil(new PtProfil(0, 0));
    final PtProfil deb = new PtProfil(2, 2);
    profil.addPtProfil(deb);
    profil.addPtProfil(new PtProfil(3, 4));
    final PtProfil fin = new PtProfil(100, 4);
    profil.addPtProfil(fin);
    profil.addPtProfil(new PtProfil(120, 5));
    final LitUtile litUtile = new LitUtile();
    litUtile.setLimDeb(deb);
    litUtile.setLimFin(fin);
    profil.setLitUtile(litUtile);
    ProfilCasierLoiUiController uiController = new ProfilCasierLoiUiController();
    uiController.setProfilCasier(profil, CrueConfigMetierForTest.DEFAULT);
    JPanel pn = new JPanel(new BorderLayout(10, 10));
    pn.add(uiController.getPanel());
    pn.add(uiController.getToolbar(), BorderLayout.NORTH);
    pn.add(uiController.getTableGraphePanel(), BorderLayout.EAST);
    pn.add(uiController.getLitUtileState().getLabel(), BorderLayout.SOUTH);
    uiController.setEditable(true);
    display(pn);
  }
}
