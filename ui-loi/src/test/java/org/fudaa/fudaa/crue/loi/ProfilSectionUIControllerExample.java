package org.fudaa.fudaa.crue.loi;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.test.ReadHelperForTest;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHFactory;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.validation.ValidateAndRebuildProfilSection;
import org.fudaa.fudaa.crue.common.config.DefaultConfigDefaultValuesProvider;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.loi.common.CourbeMoyenneDeltaController;
import org.fudaa.fudaa.crue.loi.section.ProfilSectionLoiUiController;
import org.fudaa.fudaa.crue.loi.section.ProfilSectionOpenFrtTarget;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author fred
 */
public class ProfilSectionUIControllerExample {
    public static void display(JComponent jc) {
        JFrame fr = new JFrame();
        fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fr.setContentPane(jc);
        fr.setSize(500, 600);
        fr.setVisible(true);
    }

    private static List<DonFrt> createFrts() {
        List<DonFrt> res = new ArrayList<>();
        res.add(EMHFactory.createDonFrtZFk("Fk_Z"));
        res.add(EMHFactory.createDonFrtZFk("Fk_AAA"));
        res.add(EMHFactory.createDonFrtZFk("Fk_B124"));
        res.add(EMHFactory.createDonFrtZFk("Fk_Z124"));
        for (int i = 0; i < 100; i++) {
            res.add(EMHFactory.createDonFrtZFk("Fk_D" + i));
        }
        for (DonFrt donFrt : res) {
            donFrt.getLoi().getEvolutionFF().getPtEvolutionFF().add(new PtEvolutionFF(0, 3));
            donFrt.getLoi().getEvolutionFF().getPtEvolutionFF().add(new PtEvolutionFF(0, 4));
        }
        res.add(EMHFactory.createDonFrtZFkSto("Fk_Sto"));
        return res;
    }

    public static EMHScenario readScenario() {
        final EMHScenario emhScenario = new ReadHelperForTest(TestCoeurConfig.INSTANCE).readModeleAnsSetRelation(new CtuluLog(),
            "/M3-0_c9.dc", "/M3-0_c9.dh");
        IdRegistry.install(emhScenario);
        return emhScenario;
    }

    public static void main(String[] args) {
        EMHScenario scenario = readScenario();
        final EMHSousModele sousModele = scenario.getSousModeles().get(0);
        sousModele.getFrtConteneur().addAllFrt(createFrts());
        Map<String, EMH> toMapOfNom = TransformerHelper.toMapOfNom(scenario.getAllSimpleEMH());
        CatEMHBranche branche = (CatEMHBranche) toMapOfNom.get("B2");
        final List<RelationEMHSectionDansBranche> listeSections = branche.getListeSections();
        EMHSectionProfil emh = (EMHSectionProfil) listeSections.get(2).getEmh();
        DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection(emh);
        final ProfilSectionLoiUiController uiController = new ProfilSectionLoiUiController(new DefaultConfigDefaultValuesProvider());
        uiController.setProfilSection(profilSection, CrueConfigMetierForTest.DEFAULT, sousModele);
        uiController.setProfilSectionOpenFrtTarget(new ProfilSectionOpenFrtTarget() {
            @Override
            public void openFrt(DonFrt frt) {
                DialogHelper.showInfo("Open", frt.getNom());
            }

            @Override
            public boolean validateBeforeOpenFrtEditor() {
                return true;
            }
        });
        uiController.setPreviousSections(profilSection, null);
        uiController.getPreviousCourbes().setDecalageNMoins1(5);

        JPanel pn = new JPanel(new BorderLayout(10, 10));
        pn.add(uiController.getPanel());
        pn.add(uiController.getToolbar(), BorderLayout.NORTH);
        JPanel pnTable = new JPanel(new BorderLayout());
        pnTable.add(uiController.getTableGraphePanel());
        pnTable.add(uiController.getLabelValidating(), BorderLayout.SOUTH);
        JButton createNewAndTest = new JButton(" valide et affecte");
        pnTable.add(createNewAndTest, BorderLayout.NORTH);
        createNewAndTest.addActionListener(e -> {
            DonPrtGeoProfilSection createEdited = uiController.createEdited();
            ValidateAndRebuildProfilSection rebuild = new ValidateAndRebuildProfilSection(uiController.getCcm(), null);
            CtuluLog validateAndRebuildDonPrtGeoProfilSection = rebuild.validateAndRebuildDonPrtGeoProfilSection(createEdited);
            if (validateAndRebuildDonPrtGeoProfilSection.containsErrorOrSevereError()) {
                LogsDisplayer.displayError(validateAndRebuildDonPrtGeoProfilSection, "Validation");
            } else {
                uiController.setProfilSection(createEdited, uiController.getCcm(), sousModele);
            }
            uiController.setEditable(true);
        });
        pn.add(pnTable, BorderLayout.EAST);
        JPanel pnFenteDecalage = new JPanel(new FlowLayout(FlowLayout.LEFT));
        pnFenteDecalage.add(uiController.getFenteEditor().getPanel());
        pnFenteDecalage.add(uiController.getPreviousCourbes().getDecalPanel());
        pnFenteDecalage.add(new CourbeMoyenneDeltaController(uiController.getGraphe()).getPnInfo());
        pn.add(pnFenteDecalage, BorderLayout.SOUTH);
        uiController.setEditable(true);
        display(pn);
    }
}
