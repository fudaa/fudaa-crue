/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.common;

import org.fudaa.dodico.crue.config.ccm.ItemContentAbstract;
import org.fudaa.ebli.courbe.EGModel;

/**
 *
 * @author Frederic Deniger
 */
public interface CourbeModelWithKey extends EGModel {

  Object getKey();

  ItemContentAbstract getVariableAxeY();

  ItemContentAbstract getVariableAxeX();
}
