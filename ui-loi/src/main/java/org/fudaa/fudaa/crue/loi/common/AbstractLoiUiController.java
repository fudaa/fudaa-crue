package org.fudaa.fudaa.crue.loi.common;

import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.ebli.courbe.EGGrapheSimpleModel;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.crue.loi.loiff.LoiInfoPanelController;
import org.fudaa.fudaa.crue.loi.loiff.LoiTimeExportDecorator;

import java.awt.*;
import java.util.Set;

/**
 * @author Frederic Deniger
 */
public abstract class AbstractLoiUiController extends CourbesUiController {
    public static final String PROP_Z_XT = "Z(Xt)";
    protected EGCourbeSimple courbe;
    protected LoiInfoPanelController infoController;

    public AbstractLoiUiController() {
        getGraphe().getModel().setSelectedComponent(courbe);
    }

    public AbstractLoiUiController(final Set<String> actionToAvoid) {
        super(actionToAvoid);
        getGraphe().getModel().setSelectedComponent(courbe);
    }

    public LoiInfoPanelController getInfoController() {
        if (infoController == null) {
            infoController = new LoiInfoPanelController();
        }
        if (getLoiModel() != null) {
            infoController.setTypeLoi(getLoiModel().getConfigLoi());
        }
        return infoController;
    }

    public EGCourbeSimple getCourbe() {
        return courbe;
    }

    @Override
    protected void configureTablePanel() {
        tableGraphePanel.getTable().setModel(createTableModel());
        tableGraphePanel.getTable().getColumnModel().getColumn(0).setPreferredWidth(16);
        tableGraphePanel.getTable().getColumnModel().getColumn(0).setWidth(16);
        tableGraphePanel.getTable().getColumnModel().getColumn(1).setPreferredWidth(80);
        tableGraphePanel.getTable().getColumnModel().getColumn(2).setPreferredWidth(80);
        tableGraphePanel.setPreferredSize(new Dimension(300, 600));
        new LoiTimeExportDecorator(tableGraphePanel.getTable());
    }

    public AbstractLoiCourbeModel getLoiModel() {
        return courbe == null ? null : (AbstractLoiCourbeModel) courbe.getModel();
    }

    protected abstract AbstractLoiCourbeModel createCourbeModel(AbstractLoi loi, ConfigLoi configLoi);

    protected void setLoi(final AbstractLoi loi, final CrueConfigMetier ccm, final boolean usePresentationFormat) {
        setLoi(loi, ccm, usePresentationFormat, null);
    }

    protected void setLoi(final AbstractLoi loi, final CrueConfigMetier ccm, final boolean usePresentationFormat, final String axeName) {
        final EGGrapheSimpleModel grapheModel = getEGGrapheSimpleModel();
        final EGCourbeSimple oldCourbe = getCourbe();
        if (loi == null) {
            grapheModel.removeAllCurves(null);
            return;
        }
        final ConfigLoi configLoi = ccm.getConfLoi().get(loi.getType());
        final ItemVariable varOrdonnee = configLoi.getVarOrdonnee();
        EGAxeVertical axeV = findAxe(varOrdonnee);
        if (axeV == null) {
            axeV = createAxeVertical(varOrdonnee, usePresentationFormat, axeName);
        }
        configureAxeH(ccm, loi, usePresentationFormat);
        final AbstractLoiCourbeModel loiModel = createCourbeModel(loi, configLoi);
        if (courbe != null) {
            grapheModel.removeAllCurves(null);
        }
        courbe = createCourbe(axeV, loiModel);
        if (oldCourbe != null) {
            courbe.initGraphicConfigurationFrom(oldCourbe);
        }
        loiModel.setParentCourbe(courbe);
        courbe.setTitle(loi.getNom());
        courbe.setIconeModel(new TraceIconModel(TraceIcon.CARRE, 2, Color.BLACK));
        grapheModel.addCourbe(courbe, null);
        grapheModel.setSelectedComponent(courbe);
        tableGraphePanel.updateState();
        if (infoController != null) {
            infoController.setTypeLoi(getLoiModel().getConfigLoi());
        }
        tableGraphePanel.getTable().getColumnModel().getColumn(0).setCellRenderer(new LoiStateCellRenderer());
        tableGraphePanel.getTable().getColumnModel().getColumn(1).setHeaderValue(configLoi.getVarAbscisse().getNom());
        tableGraphePanel.getTable().getColumnModel().getColumn(2).setHeaderValue(varOrdonnee.getNom());
        getGraphe().restore();
        getGraphe().fullRepaint();
        panel.repaint();

        loiModel.addObserver((o, arg) -> {
            tableGraphePanel.courbeContentChanged(courbe, false);
            getGraphe().setXRangeIsModified();
            getGraphe().fullRepaint();
        });
        setEditable(super.editable);
    }

    @Override
    public void setEditable(final boolean b) {
        if (courbe != null) {
            final AbstractLoiCourbeModel loiModel = (AbstractLoiCourbeModel) courbe.getModel();
            loiModel.setEditable(b);
        }
        super.setEditable(b);
    }

    protected EGCourbeSimple createCourbe(final EGAxeVertical axeV, final AbstractLoiCourbeModel loiModel) {
        return new EGCourbeSimple(axeV, loiModel);
    }
}
