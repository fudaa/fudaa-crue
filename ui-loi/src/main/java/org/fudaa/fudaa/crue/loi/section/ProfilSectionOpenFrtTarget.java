/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import org.fudaa.dodico.crue.metier.emh.DonFrt;

/**
 * Interface permettant aux modules externes de proposer des actions d'ouverture des frottements.
 * 
 * @author Frederic Deniger
 */
public interface ProfilSectionOpenFrtTarget {
  
  
  void openFrt(DonFrt frt);
  
  /**
   * appelée avant l'ouverture des frottements
   * @return 
   */
  boolean validateBeforeOpenFrtEditor();
  
}
