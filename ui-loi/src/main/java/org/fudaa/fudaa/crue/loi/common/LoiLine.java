package org.fudaa.fudaa.crue.loi.common;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;
import org.fudaa.dodico.crue.validation.ValidationHelperLoi;

import java.util.List;

/**
 * @author Frederic Deniger
 */
public class LoiLine {
    protected PtEvolutionFF ptEvolutionFF;
    protected CtuluLogLevel error;
    String errorMessage;

    public PtEvolutionFF getPtEvolutionFF() {
        return ptEvolutionFF;
    }

    public void setPtEvolutionFF(PtEvolutionFF ptEvolutionFF) {
        this.ptEvolutionFF = ptEvolutionFF;
    }

    public LoiLine copy() {
        LoiLine loiLine = new LoiLine();
        loiLine.error = error;
        loiLine.errorMessage = errorMessage;
        loiLine.ptEvolutionFF = ptEvolutionFF.copy();
        return loiLine;
    }

    public void setErrorMessage(String msg) {
        this.errorMessage = msg;
    }

    public void setError(CtuluLogLevel error) {
        this.error = error;
    }

    public CtuluLogLevel getError() {
        return error;
    }

    public void updateValidity(ConfigLoi loi) {
        CtuluLog validate = ValidationHelperLoi.validate(ptEvolutionFF, loi);
        error = null;
        errorMessage = null;
        if (validate.containsErrorOrSevereError()) {
            error = CtuluLogLevel.ERROR;
        }
        if (validate.containsWarnings()) {
            error = CtuluLogLevel.WARNING;
        }
        if (validate.isNotEmpty()) {
            StringBuilder builder = new StringBuilder();
            builder.append("<html><body>");
            List<CtuluLogRecord> records = validate.getRecords();
            for (CtuluLogRecord record : records) {
                BusinessMessages.updateLocalizedMessage(record);
                builder.append(record.getLocalizedMessage());
                builder.append("<br>");
            }
            errorMessage = builder.toString();
        }
    }
}
