/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.common;

import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.fudaa.crue.loi.LoiMessages;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class CourbeVisibiltyUIAction extends EbliActionSimple {

  final CourbeVisibiltyUI ui;

  public CourbeVisibiltyUIAction(EGGraphe graphe) {
    super(NbBundle.getMessage(LoiMessages.class, "CourbeVisibility.DialogTitle"), BuResource.BU.getIcon("crystal_voir"), "COURBES_VIEWS");
    ui = new CourbeVisibiltyUI(graphe);
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    ui.display();
  }
}
