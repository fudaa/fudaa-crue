/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.casier;

import javax.swing.JLabel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.fudaa.fudaa.crue.loi.LoiMessages;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ProfilCasierLitUtileState implements TableModelListener {

  final ProfilCasierLoiTableModel model;
  JLabel lbLitUtileErreur;

  public ProfilCasierLitUtileState(ProfilCasierLoiTableModel model) {
    this.model = model;
    model.addTableModelListener(this);
  }

  @Override
  public void tableChanged(TableModelEvent e) {
    updateLabel();
  }

  public JLabel getLabel() {
    if (lbLitUtileErreur == null) {
      lbLitUtileErreur = new JLabel();
    }
    updateLabel();
    return lbLitUtileErreur;
  }

  private void updateLabel() {
    if (lbLitUtileErreur == null) {
      return;
    }

    boolean error = model.getCourbe() != null && (model.findLineIdx(ProfilCasierLine.Limite.DEB) < 0 || model.findLineIdx(ProfilCasierLine.Limite.FIN) < 0);
    if (error) {
      lbLitUtileErreur.setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.SEVERE));
      lbLitUtileErreur.setText(NbBundle.getMessage(LoiMessages.class, "ProfilCasier.LitUtileNotComplete.Error"));
    } else {
      lbLitUtileErreur.setIcon(null);
      lbLitUtileErreur.setText(null);
    }
  }
}
