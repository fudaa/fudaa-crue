package org.fudaa.fudaa.crue.loi.loiff;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.crue.config.ccm.ItemContentAbstract;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.crue.loi.common.CourbeModelWithKey;
import org.fudaa.fudaa.crue.loi.common.DefaultTestedContent;
import org.fudaa.fudaa.crue.loi.common.LoiWithValideStateCourbeModel;

import java.awt.*;
import java.util.Map;
import java.util.Observable;

/**
 * @author deniger
 */
public class SimpleLoiCourbeModel extends Observable implements CourbeModelWithKey, LoiWithValideStateCourbeModel {
  private Object key;
  private final LoiFF loi;
  private final DefaultTestedContent testedContent = new DefaultTestedContent();
  private String title = "";
  private final ItemContentAbstract varX;
  private final ItemContentAbstract varY;
  private double xMax;
  private double xMin;
  private boolean xrangeComputed;
  private double yMax;
  private double yMin;
  private boolean yrangeComputed;

  public SimpleLoiCourbeModel(LoiFF loi, ItemContentAbstract varX, ItemContentAbstract varY) {
    this.loi = loi;
    this.varX = varX;
    this.varY = varY;
  }

  @Override
  public ItemContentAbstract getVariableAxeY() {
    return varY;
  }

  @Override
  public ItemContentAbstract getVariableAxeX() {
    return varX;
  }

  @Override
  public boolean useSpecificIcon(int idx) {
    return false;
  }

  @Override
  public Color getSpecificColor(int idx) {
    return null;
  }

  @Override
  public void setTested(int row, boolean isX, CtuluLog log) {
    testedContent.setTested(row, isX, log);
  }

  @Override
  public boolean isTested(int row, boolean isX) {
    return testedContent.isTested(row, isX);
  }

  @Override
  public CtuluLog getLog(int row, boolean isX) {
    return testedContent.getLog(row, isX);
  }

  @Override
  public Object getKey() {
    return key;
  }

  public void setKey(Object key) {
    this.key = key;
  }

  private void computeRangeY() {
    if (getNbValues() == 0) {
      return;
    }
    if (!yrangeComputed) {
      yMax = getY(0);
      yMin = yMax;
      for (int i = getNbValues() - 1; i > 0; i--) {
        final double d = getY(i);
        if (d > yMax) {
          yMax = d;
        } else if (d < yMin) {
          yMin = d;
        }
      }
      yrangeComputed = true;
    }
  }

  private void computeRangeX() {
    if (getNbValues() == 0) {
      return;
    }
    if (!xrangeComputed) {
      xMax = getX(0);
      xMin = xMax;
      for (int i = getNbValues() - 1; i > 0; i--) {
        final double d = getX(i);
        if (d > xMax) {
          xMax = d;
        } else if (d < xMin) {
          xMin = d;
        }
      }
      xrangeComputed = true;
    }
  }

  @Override
  public boolean isModifiable() {
    return false;
  }

  @Override
  public boolean isXModifiable() {
    return false;
  }

  @Override
  public boolean addValue(double x, double y, CtuluCommandContainer commandContainer) {
    return false;
  }

  @Override
  public boolean addValue(double[] x, double[] y, CtuluCommandContainer commandContainer) {
    return false;
  }

  @Override
  public boolean deplace(int[] selectIdx, double deltaX, double deltaY, CtuluCommandContainer commandContainer) {
    return false;
  }

  @Override
  public EGModel duplicate() {
    return new SimpleLoiCourbeModel(loi, varX, varY);
  }

  @Override
  public void fillWithInfo(InfoData infoData, CtuluListSelectionInterface selectionInterface) {
    //nothing to do
  }

  @Override
  public int getNbValues() {
    return loi.getSize();
  }

  @Override
  public String getPointLabel(int idx) {
    return null;
  }

  @Override
  public boolean setTitle(String title) {
    this.title = title;
    return true;
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public double getX(int idx) {
    return loi.getAbscisse(idx);
  }

  @Override
  public double getXMax() {
    computeRangeX();
    return xMax;
  }

  @Override
  public double getXMin() {
    computeRangeX();
    return xMin;
  }

  @Override
  public double getY(int idx) {
    return loi.getOrdonnee(idx);
  }

  @Override
  public double getYMax() {
    computeRangeY();
    return yMax;
  }

  @Override
  public double getYMin() {
    computeRangeY();
    return yMin;
  }

  @Override
  public boolean isDuplicatable() {
    return false;
  }

  @Override
  public boolean isGenerationSourceVisible() {
    return false;
  }

  @Override
  public boolean isPointDrawn(int idx) {
    return true;
  }

  @Override
  public boolean isRemovable() {
    return true;//pour qu'elle puisse être enlevée.
  }

  @Override
  public boolean isReplayable() {
    return false;
  }

  @Override
  public boolean isSegmentDrawn(int idx) {
    return true;
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  @Override
  public boolean removeValue(int idxToRemove, CtuluCommandContainer commandContainer) {
    return false;
  }

  @Override
  public boolean removeValue(int[] idxToRemove, CtuluCommandContainer commandContainer) {
    return false;
  }

  @Override
  public void replayData(EGGrapheModel model, Map infos, CtuluUI impl) {
    //nothing to do
  }

  @Override
  public void restoreFromSpecificDatas(Object data, Map infos) {
    //nothing to do
  }

  @Override
  public Object savePersistSpecificDatas() {
    return null;
  }

  @Override
  public boolean setValue(int idx, double x, double y, CtuluCommandContainer commandContainer) {
    return false;
  }

  @Override
  public boolean setValues(int[] idx, double[] x, double[] y, CtuluCommandContainer commandContainer) {
    return false;
  }

  @Override
  public void viewGenerationSource(Map infos, CtuluUI impl) {
    //nothing to do
  }

  @Override
  public int[] getInitRows() {
    return null;
  }
}
