/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.common;

import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.ebli.courbe.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TreeSelectionEvent;
import java.awt.event.ItemEvent;

/**
 *
 * @author Frederic Deniger
 */
public class ComboboxCourbeSelector implements EGGrapheModelListener, EGSelectionListener {

  final EGGraphe graphe;
  JComboBox cb;

  public ComboboxCourbeSelector(final EGGraphe graphe) {
    this.graphe = graphe;
    graphe.getModel().addModelListener(this);
    graphe.getModel().addSelectionListener(this);
  }
  DefaultComboBoxModel cbModel;

  public JComboBox getCb() {
    if (cb == null) {
      cbModel = new DefaultComboBoxModel();
      cb = new JComboBox(cbModel);
      cb.setRenderer(new EGTreeCellRenderer(true));
      cb.addItemListener(e -> {
        if (e.getStateChange() == ItemEvent.SELECTED) {
          graphe.getModel().setSelectedComponent((EGCourbe) cb.getSelectedItem());
        }
      });
      updateContent();
    }
    return cb;
  }

  protected void updateContent() {
    final EGCourbe[] courbes = graphe.getModel().getCourbes();
    final Object oldSelected = cb.getSelectedItem();
    cbModel.removeAllElements();
    for (final EGCourbe eGCourbe : courbes) {
      if (eGCourbe.getAxeY() != null) {
        cbModel.addElement(eGCourbe);
      }
    }
    if (ArrayUtils.contains(courbes, oldSelected)) {
      cb.setSelectedItem(oldSelected);
    }

  }

  @Override
  public void structureChanged() {
    if (cb != null) {
      updateContent();
    }
  }

  @Override
  public void courbeContentChanged(final EGObject _c, final boolean _mustRestore) {
  }

  @Override
  public void courbeAspectChanged(final EGObject _c, final boolean _visibil) {
    if (cb != null) {
      cb.repaint(0);
    }
  }

  @Override
  public void axeContentChanged(final EGAxe _c) {
  }

  @Override
  public void axeAspectChanged(final EGAxe _c) {
  }

  @Override
  public void valueChanged(final TreeSelectionEvent e) {
    updateSelection();
  }

  @Override
  public void valueChanged(final ListSelectionEvent e) {
    updateSelection();
  }

  public void updateSelection() {
    if (cb != null && cb.getSelectedItem() != graphe.getSelectedComponent()) {
      cb.setSelectedItem(graphe.getSelectedComponent());
    }
  }
}
