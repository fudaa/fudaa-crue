/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.loiff;

import javax.swing.JTable;
import org.fudaa.ctulu.table.CtuluTableModelExportDecorator;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiCourbeModel;
import org.fudaa.fudaa.crue.loi.common.DefaultLoiTableModel;

/**
 *
 * @author Frederic Deniger
 */
public class LoiTimeExportDecorator implements CtuluTableModelExportDecorator {

  private final JTable table;

  public LoiTimeExportDecorator(JTable table) {
    this.table = table;
    table.putClientProperty(CtuluTableModelExportDecorator.CLIENT_PROPERTY_, this);
  }

  @Override
  public boolean hasSpecificExportValue(int col) {
    if (col == 1 && table.getModel() instanceof DefaultLoiTableModel) {
      DefaultLoiTableModel model = (DefaultLoiTableModel) table.getModel();
      AbstractLoiCourbeModel loiCourbeModel = (AbstractLoiCourbeModel) model.getCourbe().getModel();
      ItemVariable varAbscisse = loiCourbeModel.getConfigLoi().getVarAbscisse();
      if (varAbscisse.getNature().isDate() || varAbscisse.getNature().isDuration()) {
        return true;
      }
    }

    return false;
  }

  @Override
  public Object getValue(int row, int col) {
    if (col == 1 && table.getModel() instanceof DefaultLoiTableModel) {
      DefaultLoiTableModel model = (DefaultLoiTableModel) table.getModel();
      AbstractLoiCourbeModel loiCourbeModel = (AbstractLoiCourbeModel) model.getCourbe().getModel();
      if (!loiCourbeModel.isDefined(row)) {
        return null;
      }
      ItemVariable varAbscisse = loiCourbeModel.getConfigLoi().getVarAbscisse();
      if (varAbscisse.getNature().isDate() || varAbscisse.getNature().isDuration()) {
        return varAbscisse.format(loiCourbeModel.getX(row), DecimalFormatEpsilonEnum.COMPARISON);
      }
    }
    return table.getValueAt(row, col);
  }
}
