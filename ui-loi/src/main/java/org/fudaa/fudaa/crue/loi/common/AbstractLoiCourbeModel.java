package org.fudaa.fudaa.crue.loi.common;

import gnu.trove.TIntHashSet;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.ItemContentAbstract;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.loi.LoiMessages;
import org.openide.util.NbBundle;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * @author deniger
 */
public abstract class AbstractLoiCourbeModel extends Observable implements CourbeModelWithKey {
    protected List<LoiLine> lines;
    final ConfigLoi configLoi;
    double yMax;
    double yMin;
    boolean yrangeComputed;
    double xMax;
    double xMin;
    boolean xrangeComputed;
    final PtEvolutionFFComparator ptEvolutionsComparator = new PtEvolutionFFComparator();
    final LoiLineComparator loiLineComparator = new LoiLineComparator();
    Object key;
    boolean showError;
    EGCourbe parentCourbe;
    boolean editable;
    Set<PtEvolutionFF> strictlyIncreasingSet;
    String title = "";
    private boolean hasErrorInModification = false;

    protected AbstractLoiCourbeModel(ConfigLoi configLoi) {
        this.lines = new ArrayList<>();
        this.configLoi = configLoi;
        setKey(configLoi.getTypeLoi().getId());
        if (configLoi.getControleLoi().isStricltyIncreasing()) {
            strictlyIncreasingSet = new TreeSet<>(ptEvolutionsComparator);
        }
    }

    public boolean isShowError() {
        return showError;
    }

    public void setShowError(boolean showError) {
        this.showError = showError;
    }

    public boolean isHasErrorInModification() {
        return hasErrorInModification;
    }

    public void setHasErrorInModification(boolean hasErrorInModification) {
        this.hasErrorInModification = hasErrorInModification;
    }

    @Override
    public Object getKey() {
        return key;
    }

    public void setKey(Object key) {
        this.key = key;
    }

    @Override
    public ItemContentAbstract getVariableAxeY() {
        return configLoi == null ? null : configLoi.getVarOrdonnee();
    }

    @Override
    public ItemContentAbstract getVariableAxeX() {
        return configLoi == null ? null : configLoi.getVarAbscisse();
    }

    public LoiLine createLineItem(double _x, double _y) {
        LoiLine loiLine = createLoiLineInstance();
        loiLine.ptEvolutionFF = new PtEvolutionFF(_x, _y);
        loiLine.updateValidity(configLoi);
        return loiLine;
    }

    public ConfigLoi getConfigLoi() {
        return configLoi;
    }

    public double getAbscisseNormalizedValue(double x) {
        return getConfigLoi().getVarAbscisse().getNormalizedValue(x);
    }

    public double[] getAbscisseNormalizedValues(double[] x) {
        return getConfigLoi().getVarAbscisse().getNormalizedValues(x);
    }

    public double getOrdonneeNormalizedValue(double y) {
        return getConfigLoi().getVarOrdonnee().getNormalizedValue(y);
    }

    public double[] getOrdonneeNormalizedValues(double[] y) {
        return getConfigLoi().getVarOrdonnee().getNormalizedValues(y);
    }

    public String getAbscisse() {
        return configLoi.getVarAbscisse().getNom();
    }

    public String getOrdonnee() {
        return configLoi.getVarOrdonnee().getNom();
    }

    /**
     * @return liste nom modifiable des lines
     */
    public List<LoiLine> getLoiLines() {
        return Collections.unmodifiableList(lines);
    }

    /**
     * @return liste des copies des points
     */
    public List<PtEvolutionFF> getPoints() {
        List<PtEvolutionFF> res = new ArrayList<>();
        for (LoiLine line : lines) {
            res.add(line.ptEvolutionFF.copy());
        }
        return res;
    }

    private void addPoint(double _x, double _y) {
        final LoiLine ptEvolutionFF = createLineItem(_x, _y);
        lines.add(ptEvolutionFF);
        if (strictlyIncreasingSet != null) {
            strictlyIncreasingSet.add(ptEvolutionFF.ptEvolutionFF);
        }
    }

    protected void changeDone() {
        invalideRanges();
        sort();
        fireChanged();
    }

    private void invalideRanges() {
        xrangeComputed = false;
        yrangeComputed = false;
    }

    private boolean isNewPointIncreasingValid(int[] _selectIdx, int nb, double _deltaX, double _deltaY) {
        //on doit vérifier que la courbe est toujours strictement croissante
        if (strictlyIncreasingSet != null) {
            TreeSet<PtEvolutionFF> treeSet = new TreeSet<>(ptEvolutionsComparator);
            TIntHashSet idx = new TIntHashSet(_selectIdx);
            for (int i = 0; i < getNbValues(); i++) {
                if (!idx.contains(i)) {
                    treeSet.add(getPtEvol(i));
                }
            }
            for (int i = 0; i < nb; i++) {
                PtEvolutionFF ptEvol = getPtEvol(_selectIdx[i]);
                PtEvolutionFF ptEvolDelta = new PtEvolutionFF(ptEvol.getAbscisse() + _deltaX, ptEvol.getOrdonnee() + _deltaY);
                if (treeSet.contains(ptEvolDelta)) {
                    return false;
                }
                treeSet.add(ptEvolDelta);
            }
        }
        return true;
    }

    private boolean isNewPointRangeValid(int nb, int[] _selectIdx, double _deltaX, double _deltaY) {
        for (int i = 0; i < nb; i++) {
            PtEvolutionFF ptEvol = getPtEvol(_selectIdx[i]);
            double newX = ptEvol.getAbscisse() + _deltaX;
            double newY = ptEvol.getOrdonnee() + _deltaY;
            if (!isRangeValid(newX, newY)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return une nouvelle instance
     */
    protected abstract LoiLine createLoiLineInstance();

    public void setParentCourbe(EGCourbe parentCourbe) {
        this.parentCourbe = parentCourbe;
    }

    protected void fireChanged() {
        setChanged();
        notifyObservers(this);
        if (parentCourbe != null) {
            parentCourbe.fireCourbeContentChanged();
        }
    }

    protected void sortLoiLines() {
        Collections.sort(lines, loiLineComparator);
    }

    public void setPts(List<PtEvolutionFF> pts) {
        this.lines.clear();
        if (strictlyIncreasingSet != null) {
            strictlyIncreasingSet.clear();
        }
        for (PtEvolutionFF ptEvolutionFF : pts) {
            final LoiLine line = createLineItem(ptEvolutionFF.getAbscisse(), ptEvolutionFF.getOrdonnee());
            this.lines.add(line);
            if (strictlyIncreasingSet != null) {
                strictlyIncreasingSet.add(line.ptEvolutionFF);
            }
        }
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    protected void sort() {
        if (configLoi.getControleLoi().isIncreasing() || configLoi.getControleLoi().isStricltyIncreasing()) {
            Collections.sort(lines, loiLineComparator);
        }
    }

    private void computeRangeY() {
        if (getNbValues() == 0) {
            return;
        }
        if (!yrangeComputed) {
            yMax = getY(0);
            yMin = yMax;
            for (int i = getNbValues() - 1; i > 0; i--) {
                final double d = getY(i);
                if (d > yMax) {
                    yMax = d;
                } else if (d < yMin) {
                    yMin = d;
                }
            }
            yrangeComputed = true;
        }
    }

    private void computeRangeX() {
        if (getNbValues() == 0) {
            return;
        }
        if (!xrangeComputed) {
            xMax = getX(0);
            xMin = xMax;
            for (int i = getNbValues() - 1; i > 0; i--) {
                final double d = getX(i);
                if (d > xMax) {
                    xMax = d;
                } else if (d < xMin) {
                    xMin = d;
                }
            }
            xrangeComputed = true;
        }
    }

    @Override
    public boolean isModifiable() {
        return editable;
    }

    @Override
    public boolean isXModifiable() {
        return editable;
    }

    public boolean isRangeValid(double _x, double _y) {
        return configLoi.getVarAbscisse().getValidator().getRangeValidate().containsNumber(_x)
                && configLoi.getVarOrdonnee().getValidator().getRangeValidate().containsNumber(_y);
    }

    /**
     * @param newX
     * @param newY
     * @return dans le cas d'une loi strictement croissante renvoie true si le point n'est pas déjà défini
     */
    public boolean isControlValid(double _x, double _y) {
        if (strictlyIncreasingSet != null) {
            return !strictlyIncreasingSet.contains(new PtEvolutionFF(_x, _y));
        }
        return true;
    }

    public boolean isValid(double _x, double _y) {
        return isRangeValid(_x, _y) && isControlValid(_x, _y);
    }

    @Override
    public boolean addValue(double newX, double newY, CtuluCommandContainer _cmd) {
        double _x = getAbscisseNormalizedValue(newX);
        double _y = getOrdonneeNormalizedValue(newY);
        if (!isRangeValid(_x, _y)) {
            DialogHelper.showError(NbBundle.getMessage(LoiMessages.class, "LoiAddPoint.NotValid"));
            return false;
        }
        if (!isControlValid(_x, _y)) {
            DialogHelper.showError(NbBundle.getMessage(LoiMessages.class, "LoiAddPoint.NotStrictlyIncreasing"));
            return false;
        }

        addPoint(_x, _y);
        changeDone();
        return true;
    }

    @Override
    public boolean addValue(double[] initNewX, double[] initNewY, CtuluCommandContainer _cmd) {
        double[] newX = getAbscisseNormalizedValues(initNewX);
        double[] newY = getOrdonneeNormalizedValues(initNewY);
        int nb = newX.length;

        for (int i = 0; i < nb; i++) {
            if (!isRangeValid(newX[i], newY[i])) {
                if (showError) {
                    DialogHelper.showError(NbBundle.getMessage(LoiMessages.class, "LoiAddPoint.NotValid"));
                }
                hasErrorInModification = true;
                return false;
            }
            if (!isControlValid(newX[i], newY[i])) {
                if (showError) {
                    hasErrorInModification = true;
                    DialogHelper.showError(NbBundle.getMessage(LoiMessages.class, "LoiAddPoint.NotStrictlyIncreasing"));
                }
                return false;
            }
        }
        for (int i = 0; i < nb; i++) {
            addPoint(newX[i], newY[i]);
        }
        changeDone();
        return true;
    }

    @Override
    public boolean deplace(int[] _selectIdx, double _deltaX, double _deltaY, CtuluCommandContainer _cmd) {
        int nb = _selectIdx.length;
        boolean rangeValid = isNewPointRangeValid(nb, _selectIdx, _deltaX, _deltaY);
        if (!rangeValid) {
            DialogHelper.showError(NbBundle.getMessage(LoiMessages.class, "LoiMovePoint.NotValid"));
            return false;
        }
        boolean increasingValid = isNewPointIncreasingValid(_selectIdx, nb, _deltaX, _deltaY);
        if (!increasingValid) {
            DialogHelper.showError(NbBundle.getMessage(LoiMessages.class, "LoiMovePoint.NotStrictlyIncreasing"));
            return false;
        }
        for (int i = 0; i < nb; i++) {
            LoiLine line = getLoiLine(_selectIdx[i]);
            //enlever avant modification
            PtEvolutionFF ptEvol = line.ptEvolutionFF;
            if (strictlyIncreasingSet != null) {
                strictlyIncreasingSet.remove(line.ptEvolutionFF);
            }
            final double newX = getAbscisseNormalizedValue(ptEvol.getAbscisse() + _deltaX);
            final double newY = getOrdonneeNormalizedValue(ptEvol.getOrdonnee() + _deltaY);
            ptEvol.setAbscisse(newX);
            ptEvol.setOrdonnee(newY);
            //remettre après:
            if (strictlyIncreasingSet != null) {
                strictlyIncreasingSet.add(ptEvol);
            }
            line.updateValidity(configLoi);
        }
        changeDone();
        return true;
    }

    @Override
    public EGModel duplicate() {
        return null;
    }

    @Override
    public void fillWithInfo(InfoData _table, CtuluListSelectionInterface _selectedPt) {
    }

    @Override
    public int getNbValues() {
        return lines.size();
    }

    @Override
    public String getPointLabel(int _i) {
        return null;
    }

    @Override
    public Color getSpecificColor(int idx) {
        return null;
    }

    @Override
    public boolean setTitle(String title) {
        this.title = title;
        return true;
    }

    @Override
    public String getTitle() {
        return title;
    }

    protected PtEvolutionFF getPtEvol(int idx) {
        if (idx >= lines.size()) {
            return null;
        }
        return lines.get(idx).ptEvolutionFF;
    }

    public boolean isDefined(int idx) {
        return idx >= 0 && idx < lines.size();
    }

    protected LoiLine getLoiLine(int idx) {
        return lines.get(idx);
    }

    @Override
    public double getX(int _idx) {
        return getPtEvol(_idx).getAbscisse();
    }

    @Override
    public double getXMax() {
        computeRangeX();
        return xMax;
    }

    @Override
    public double getXMin() {
        computeRangeX();
        return xMin;
    }

    @Override
    public double getY(int _idx) {
        return getPtEvol(_idx).getOrdonnee();
    }

    @Override
    public double getYMax() {
        computeRangeY();
        return yMax;
    }

    @Override
    public double getYMin() {
        computeRangeY();
        return yMin;
    }

    @Override
    public boolean isDuplicatable() {
        return false;
    }

    @Override
    public boolean isGenerationSourceVisible() {
        return false;
    }

    @Override
    public boolean isPointDrawn(int _i) {
        return true;
    }

    @Override
    public boolean isRemovable() {
        return true;//pour qu'elle puisse être enlevée.
    }

    @Override
    public boolean isReplayable() {
        return false;
    }

    @Override
    public boolean isSegmentDrawn(int _i) {
        return true;
    }

    @Override
    public boolean isTitleModifiable() {
        return false;
    }

    @Override
    public boolean removeValue(int idxToRemove, CtuluCommandContainer _cmd) {
        PtEvolutionFF ptEvol = getPtEvol(idxToRemove);
        lines.remove(idxToRemove);
        if (strictlyIncreasingSet != null) {
            strictlyIncreasingSet.remove(ptEvol);
        }
        changeDone();
        return true;
    }

    @Override
    public boolean removeValue(int[] idxToRemove, CtuluCommandContainer _cmd) {
        int nb = idxToRemove.length;
        if (strictlyIncreasingSet != null) {
            for (int i = 0; i < nb; i++) {
                strictlyIncreasingSet.remove(getPtEvol(idxToRemove[i]));
            }
        }
        Arrays.sort(idxToRemove);
        for (int i = nb - 1; i >= 0; i--) {
            int idx = idxToRemove[i];
            if (idx < lines.size() && idx >= 0) {
                lines.remove(idx);
            }
        }
        changeDone();
        return true;
    }

    public void removeAll() {
        lines.clear();
        if (strictlyIncreasingSet != null) {
            strictlyIncreasingSet.clear();
        }
        changeDone();
    }

    @Override
    public void replayData(EGGrapheModel model, Map infos, CtuluUI impl) {
    }

    @Override
    public void restoreFromSpecificDatas(Object data, Map infos) {
    }

    @Override
    public Object savePersistSpecificDatas() {
        return null;
    }

    @Override
    public boolean setValue(int _i, double initNewX, double initNewY, CtuluCommandContainer _cmd) {
        double newX = getAbscisseNormalizedValue(initNewX);
        double newY = getOrdonneeNormalizedValue(initNewY);
        LoiLine ptEvol = getLoiLine(_i);
        double oldX = ptEvol.getPtEvolutionFF().getAbscisse();
        double oldY = ptEvol.getPtEvolutionFF().getOrdonnee();
        if (configLoi.getVarAbscisse().getEpsilon().isSame(oldX, newX)
                && configLoi.getVarOrdonnee().getEpsilon().isSame(oldY, newY)) {
            return false;
        }
        if (!isRangeValid(newX, newY)) {
            if (showError) {
                DialogHelper.showError(NbBundle.getMessage(LoiMessages.class, "LoiChangePoint.NotValid"));
            }
            hasErrorInModification = true;
            return false;
        }
        if (strictlyIncreasingSet != null) {
            strictlyIncreasingSet.remove(ptEvol.ptEvolutionFF);
            if (strictlyIncreasingSet.contains(new PtEvolutionFF(newX, newY))) {
                //on réinsère le point enlevé:
                strictlyIncreasingSet.add(ptEvol.ptEvolutionFF);
                if (showError) {
                    DialogHelper.showError(NbBundle.getMessage(LoiMessages.class, "LoiChangePoint.NotStrictlyIncreasing"));
                }
                hasErrorInModification = true;
                return false;
            }
        }
        ptEvol.ptEvolutionFF.setAbscisse(newX);
        ptEvol.ptEvolutionFF.setOrdonnee(newY);
        ptEvol.updateValidity(configLoi);
        if (strictlyIncreasingSet != null) {
            strictlyIncreasingSet.add(ptEvol.ptEvolutionFF);
        }
        ptEvol.updateValidity(configLoi);
        changeDone();
        return true;
    }

    @Override
    public boolean setValues(int[] _idx, double[] initNewX, double[] initNewY, CtuluCommandContainer _cmd) {
        double[] newX = getAbscisseNormalizedValues(initNewX);
        double[] newY = getOrdonneeNormalizedValues(initNewY);
        int nb = _idx.length;
        for (int i = 0; i < nb; i++) {
            if (!isRangeValid(newX[i], newY[i])) {
                hasErrorInModification = true;
                if (showError) {
                    DialogHelper.showError(NbBundle.getMessage(LoiMessages.class, "LoiChangePoint.NotValid"));
                }
                return false;
            }
        }

        if (strictlyIncreasingSet != null) {
            TreeSet<PtEvolutionFF> newSet = new TreeSet<>(ptEvolutionsComparator);
            TIntHashSet idxChanged = new TIntHashSet(_idx);
            for (int i = 0; i < nb; i++) {
                if (!idxChanged.contains(i)) {
                    newSet.add(getPtEvol(i));
                }
            }
            for (int i = 0; i < nb; i++) {
                PtEvolutionFF newPt = new PtEvolutionFF(newX[i], newY[i]);
                if (newSet.contains(newPt)) {
                    //on restaure les données
                    if (showError) {
                        DialogHelper.showError(NbBundle.getMessage(LoiMessages.class, "LoiChangePoint.NotStrictlyIncreasing"));
                    }
                    hasErrorInModification = true;
                    return false;
                }
                newSet.add(newPt);
            }
            strictlyIncreasingSet = newSet;
        }
        boolean changed = false;
        for (int i = 0; i < nb; i++) {
            LoiLine loiLine = getLoiLine(_idx[i]);
            double oldX = loiLine.getPtEvolutionFF().getAbscisse();
            double oldY = loiLine.getPtEvolutionFF().getOrdonnee();
            if (!configLoi.getVarAbscisse().getEpsilon().isSame(oldX, newX[i])
                    || !configLoi.getVarAbscisse().getEpsilon().isSame(oldY, newY[i])) {
                changed = true;
                loiLine.ptEvolutionFF.setAbscisse(newX[i]);
                loiLine.ptEvolutionFF.setOrdonnee(newY[i]);
            }
            loiLine.updateValidity(configLoi);
        }
        if (changed) {
            changeDone();
        }
        return changed;
    }

    @Override
    public void viewGenerationSource(Map infos, CtuluUI impl) {
    }

    @Override
    public int[] getInitRows() {
        return null;
    }

    private class PtEvolutionFFComparator extends SafeComparator<PtEvolutionFF> {
        @Override
        protected int compareSafe(PtEvolutionFF o1, PtEvolutionFF o2) {
            PropertyEpsilon epsilon = configLoi.getVarAbscisse().getEpsilon();
            if (epsilon.isSame(o1.getAbscisse(), o2.getAbscisse())) {
                return 0;
            }
            return o1.getAbscisse() > o2.getAbscisse() ? 1 : -1;
        }
    }

    protected class LoiLineComparator extends SafeComparator<LoiLine> {
        final PtEvolutionFFComparator comparator = new PtEvolutionFFComparator();

        @Override
        protected int compareSafe(LoiLine o1, LoiLine o2) {
            return comparator.compare(o1.ptEvolutionFF, o2.ptEvolutionFF);
        }
    }
}
