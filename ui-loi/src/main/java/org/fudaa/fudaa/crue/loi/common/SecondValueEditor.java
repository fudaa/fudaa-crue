/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.common;

import com.memoire.bu.BuStringValidator;
import com.memoire.bu.BuTextField;
import java.awt.Component;
import javax.swing.JComponent;
import javax.swing.text.JTextComponent;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluDurationFormatter;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.dodico.crue.common.time.SecondParser;

/**
 *
 * @author Frederic Deniger
 */
public class SecondValueEditor extends CtuluValueEditorDouble {

  final SecondParser secondParser;

  /**
   *
   * @param secondParser peut etre null si la date de deb ne l'est pas.
   * @param toSecond
   */
  public SecondValueEditor(SecondParser secondParser, CtuluNumberFormatI toSecond) {
    super(false);
    this.secondParser = secondParser;
    setFormatter(toSecond);
  }

  @Override
  public JComponent createEditorComponent() {
    final BuTextField tf = (BuTextField) super.createEditorComponent();

    return decore(tf);
  }

  private BuTextField decore(BuTextField tf) {
    tf.setStringValidator(new BuStringValidator() {
      @Override
      public String valueToString(final Object _value) {
        return SecondValueEditor.this.valueToString(_value);
      }

      @Override
      public Object stringToValue(final String _string) {
        return SecondValueEditor.this.stringToValue(_string);
      }

      @Override
      public boolean isStringValid(final String _string) {
        return SecondValueEditor.this.isValid(_string);
      }
    });
    return tf;
  }

  @Override
  public JComponent createCommonEditorComponent(boolean _addOld) {
    final BuTextField tf = (BuTextField) super.createCommonEditorComponent(_addOld);
    return decore(tf);

  }

  @Override
  public String toString(Object _s) {
    return valueToString(_s);
  }

  @Override
  public boolean isValid(String _s) {
    return stringToValue(_s) != null;
  }

  public String valueToString(final Object _s) {

    if (_s == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    if (_s instanceof Number) {
      return toString(((Number) _s).doubleValue());
    } else {
      try {
        return valueToString(stringToValue(_s.toString()));
      } catch (final NumberFormatException e) {
      }
    }
    return CtuluLibString.EMPTY_STRING;
  }

  public Double stringToValue(String _value) {
    if (StringUtils.isBlank(_value)) {
      return null;
    }
    try {
      Double res = Double.parseDouble(_value);
      return res;
    } catch (NumberFormatException numberFormatException) {
    }
    Double parse = secondParser == null ? null : secondParser.parseInSec(_value);
    if (parse != null) {
      return parse;
    }
    try {
      return (double) CtuluDurationFormatter.getSec(_value);
    } catch (NumberFormatException numberFormatException) {
    }
    return null;
  }

  @Override
  public String getStringValue(final Component _comp) {
    final String s = ((JTextComponent) _comp).getText();
    try {
      Double res = stringToValue(s);
      return valueToString(res);
    } catch (final NumberFormatException e) {
      return null;
    }
  }
}
