/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import java.util.List;
import org.fudaa.ctulu.ToStringTransformable;

/**
 *
 * Contient pour chaque point du profil ( ou ligne du tableau) les données relatives aux étiquettes.
 *
 * @author Frederic Deniger
 */
public class ProfilSectionEtiquetteContainer implements ToStringTransformable {

  final List<ProfilSectionEtiquette> etiquettes;

  public ProfilSectionEtiquetteContainer(List<ProfilSectionEtiquette> etiquettes) {
    this.etiquettes = etiquettes;
  }

  public List<ProfilSectionEtiquette> getEtiquettes() {
    return etiquettes;
  }

  @Override
  public String toString() {
    return getAsString();
  }


  @Override
  public String getAsString() {
    StringBuilder builder = new StringBuilder();
    if (etiquettes != null) {
      boolean first = true;
      for (ProfilSectionEtiquette profilSectionEtiquette : etiquettes) {
        if (first) {
          first = false;
        } else {
          builder.append(';');
        }
        builder.append(profilSectionEtiquette.getAsString());
      }
    }
    return builder.toString();
  }
}
