/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import com.jidesoft.popup.JidePopup;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.transformer.LogsHelper;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.config.lit.LitNommeLimite;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EtiquetteIndexed;
import org.fudaa.dodico.crue.metier.helper.LitNommeIndexed;
import org.fudaa.dodico.crue.metier.helper.LitNumeroteIndex;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.dodico.crue.validation.ValidateAndRebuildProfilSection;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.common.config.ConfigDefaultValuesProvider;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.loi.LoiMessages;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiCourbeModel;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiUiController;
import org.fudaa.fudaa.crue.loi.common.LoiPopupMenuReceiver;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ProfilSectionLoiUiController extends AbstractLoiUiController implements TableModelListener {

  public static final int FRT_COLUMN_IDX = 4;
  private final JLabel labelValidating = new JLabel();
  final ProfilSectionPreviousController previousCourbes;
  List<DonFrt> frt;
  List<DonFrt> frtStockage;
  List<ProfilSectionEtiquette> etiquettes;
  Map<LitNommeLimite, ProfilSectionEtiquette> etiquettesByLimites;
  Map<ItemEnum, ProfilSectionEtiquette> etiquettesByProfilEtiquette;
  ProfilSectionEtiquette etiquettesLimiteLit;
  CrueConfigMetier ccm;
  ProfilSectionDonFrtCellEditor donFrtCellEditor;
  ProfilSectionFenteController fenteController;
  final ConfigDefaultValuesProvider defaultValuesProvider;
  final JPanel pnEtiquettes;

  public ProfilSectionLoiUiController(ConfigDefaultValuesProvider defaultValuesProvider) {
    setUseVariableForAxeH(true);
    tableGraphePanel.getTable().getModel().addTableModelListener(this);
    tableGraphePanel.getTable().addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() >= 2) {
          doubleClickedDone(e);
        }
      }
    });
    frtCourbeModel = new ProfilSectionFrtCourbeModel();
    etiquetteCourbeModel = new ProfilSectionEtiquetteCourbeModel();
    previousCourbes = new ProfilSectionPreviousController(this);
    pnEtiquettes = new JPanel(new BorderLayout());
    pnEtiquettes.setOpaque(false);
    panel.add(pnEtiquettes, BorderLayout.NORTH);
    this.defaultValuesProvider = defaultValuesProvider;
    super.getGraphe().addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
          popup(e);
        }
      }
    });
  }

  public void doubleClickedDone(MouseEvent evt) {
    int column = tableGraphePanel.getTable().getSelectedColumn();
    if (column == FRT_COLUMN_IDX) {
      openSelectedFrt();

    }
  }

  @Override
  protected EGCourbeSimple createCourbe(EGAxeVertical axeV, AbstractLoiCourbeModel loiModel) {
    return ProfilSectionCourbeBuilder.build(axeV, (ProfiLSectionCourbeModel) loiModel);
  }

  @Override
  public LoiPopupMenuReceiver createLoiPopupReceiver() {
    return new ProfilPopupMenuReceiver(this);
  }

  public void popup(MouseEvent _evt) {
    if (getLoiModel() != null && getCourbe() != null) {
      if (panel.getSelection().getNbSelectedIndex() == 1) {
        int row = panel.getSelection().getMinIndex();
        final ProfilSectionEtiquette[] etiquettesAsArray = etiquettes.
                toArray(new ProfilSectionEtiquette[0]);
        JidePopup popup = ProfilSectionEtiquetteCellEditor.createEtiquettePopupEdition(etiquettesAsArray,
                (ProfilSectionLoiTableModel) tableGraphePanel.getTable().getModel(), row);
        popup.setOwner(getGraphe());
        popup.showPopup(_evt.getXOnScreen(), _evt.getYOnScreen());

      }
    }
  }

  public ProfilSectionPreviousController getPreviousCourbes() {
    return previousCourbes;
  }

  public ProfilSectionFenteController getFenteEditor() {
    if (fenteController == null) {
      fenteController = new ProfilSectionFenteController();
      if (profil != null) {
        fenteController.setProfil(profil, ccm);
      }
    }
    return fenteController;
  }

  public CrueConfigMetier getCcm() {
    return ccm;
  }
  boolean useSectionName;

  public void setUseSectionName(boolean useSectionName) {
    this.useSectionName = useSectionName;
    if (previousCourbes != null) {
      previousCourbes.setUseSectionName(useSectionName);
    }
  }

  /**
   * permet d'affecter les frottements qui seront utilisés dans les liste de choix des frottements.
   *
   * @param frts
   */
  private void updateWithAvailableFrottements(List<DonFrt> frts) {
    frt = new ArrayList<>();
    frtStockage = new ArrayList<>();
    for (DonFrt donFrt : frts) {
      if (EnumTypeLoi.LoiZFKSto.equals(donFrt.getLoi().getType())) {
        frtStockage.add(donFrt);
      } else {
        frt.add(donFrt);
      }
    }
    Collections.sort(frt, ObjetNommeByNameComparator.INSTANCE);
    Collections.sort(frtStockage, ObjetNommeByNameComparator.INSTANCE);
    if (donFrtCellEditor != null) {
      donFrtCellEditor.setFrt(frt);
      donFrtCellEditor.setFrtStockage(frtStockage);
    }
  }

  protected DonFrt getDonFrtByName(String id) {
    if (id == null) {
      return null;
    }
    Map<String, DonFrt> toMapOfId = TransformerHelper.toMapOfId(frt);
    DonFrt frt = toMapOfId.get(id.toUpperCase());
    if (frt != null) {
      return frt;
    }
    toMapOfId = TransformerHelper.toMapOfId(frtStockage);
    return toMapOfId.get(id.toUpperCase());

  }

  public JLabel getLabelValidating() {
    return labelValidating;
  }

  @Override
  public void tableChanged(TableModelEvent e) {
    if (updating || e.getColumn() == ProfilSectionLoiTableModel.COLUMN_FRT) {
      return;
    }
    updateValidationState();
    frtCourbeModel.reload(ccm);
    etiquetteCourbeModel.reload(this);
    getGraphe().courbeContentChanged(courbeFrt, false);
    getGraphe().courbeContentChanged(courbeEtiquette, false);

  }
  boolean addFrt = true;

  public void setAddFrt(boolean addFrt) {
    this.addFrt = addFrt;
  }

  @Override
  public ProfiLSectionCourbeModel getLoiModel() {
    return (ProfiLSectionCourbeModel) super.getLoiModel();
  }

  public CtuluLog validateLit() {
    if (getLoiModel() == null || ccm == null) {
      return null;
    }
    return new ProfilSectionLitValidation().valid(getLoiModel().getProfilSectionLoiLine(), ccm.getLitNomme());
  }
  DonPrtGeoProfilSection profil;
  EMHSousModele sousModeleParent;
  final ProfilSectionFrtCourbeModel frtCourbeModel;
  final ProfilSectionEtiquetteCourbeModel etiquetteCourbeModel;
  EGCourbeSimple courbeFrt;
  EGCourbeSimple courbeEtiquette;

  public DonPrtGeoProfilSection getProfil() {
    return profil;
  }

  /**
   * On précise le sous-modele pour les profils en cours de création.
   *
   * @param profil
   * @param ccm
   * @param sousModele
   */
  public void setProfilSection(DonPrtGeoProfilSection profil, CrueConfigMetier ccm, EMHSousModele sousModele) {
    setProfilSection(profil, ccm, sousModele, null);
  }

  /**
   *
   * @return le sous-modele parent du profil edite/créé
   */
  public EMHSousModele getSousModeleParent() {
    return sousModeleParent;
  }

  public void setProfilSection(DonPrtGeoProfilSection profil, CrueConfigMetier ccm, EMHSousModele sousModele, String initNom) {
    pnEtiquettes.removeAll();

    this.ccm = ccm;
    if (profil == null) {
      this.profil = null;
      this.sousModeleParent = null;
      super.setLoi(null, ccm, false);
      updateWithAvailableFrottements(Collections.emptyList());
      return;
    }
    etiquetteCourbeModel.setEtiquette(ccm.getLitNomme().getEtiquetteAxeHyd());
    sousModeleParent = sousModele;
    this.profil = profil;
    final List<LitNommeLimite> limites = ccm.getLitNomme().getLimites();
    pnEtiquettes.add(new JLabel(limites.get(0).getNom()), BorderLayout.WEST);
    pnEtiquettes.add(new JLabel(limites.get(limites.size() - 1).getNom()), BorderLayout.EAST);
    etiquettes = ProfilSectionEtiquette.getEtiquettes(ccm);
    etiquettesByLimites = ProfilSectionEtiquette.getEtiquettesByLimite(etiquettes);
    etiquettesByProfilEtiquette = ProfilSectionEtiquette.getEtiquettesByProfilEtiquette(etiquettes);
    etiquettesLimiteLit = ProfilSectionEtiquette.getEtiquettesLimiteLit(etiquettes);
    ((ProfilSectionLoiTableModel) tableGraphePanel.getTable().getModel()).setEtiquetteByLimite(etiquettesByLimites);
    String nom = initNom;
    if (nom == null) {
      nom = profil.getNom();
      if (useSectionName) {
        nom = profil.getEmh().getNom();
      }
    }
    updating = true;
    super.setLoi(LoiHelper.createFromProfil(profil.getPtProfil(), nom), ccm, false);
    final TableColumn etiquetteColumn = tableGraphePanel.getTable().getColumnModel().getColumn(3);
    //faut ajouter le renderer et editor.
    etiquetteColumn.setCellRenderer(new ProfilSectionEtiquetteCellRenderer());
    etiquetteColumn.setCellEditor(new ProfilSectionEtiquetteCellEditor(etiquettes, tableGraphePanel.getTable()));
    etiquetteColumn.setPreferredWidth(100);
    etiquetteColumn.setWidth(100);
    final TableColumn donFrtColumn = tableGraphePanel.getTable().getColumnModel().getColumn(FRT_COLUMN_IDX);
    donFrtColumn.setPreferredWidth(100);
    donFrtColumn.setWidth(100);
    donFrtColumn.setCellRenderer(new ProfilSectionDonFrtCellRenderer());
    donFrtCellEditor = new ProfilSectionDonFrtCellEditor(ccm.getLitNomme(), this);
    donFrtCellEditor.setFrt(frt);
    donFrtCellEditor.setFrtStockage(frtStockage);
    donFrtColumn.setCellEditor(donFrtCellEditor);
    buildModels(ccm, profil);
    if (fenteController != null) {
      fenteController.setProfil(profil, ccm);
    }
    setEditable(editable);
    if (addFrt) {
      addFrtCourbe(ccm);
    }

    getEGGrapheSimpleModel().setSelectedComponent(courbe);
    updating = false;
    updateValidationState();
    updateWithAvailableFrottements(sousModeleParent.getFrtConteneur().getListFrt());
    getGraphe().restore();
    getGraphe().fullRepaint();
  }
  boolean updating;

  public void setPreviousSections(DonPrtGeoProfilSection nMoins1, DonPrtGeoProfilSection nMoins2) {
    previousCourbes.setSection(nMoins1, nMoins2);
    getEGGrapheSimpleModel().setSelectedComponent(courbe);
  }

  @Override
  public void setEditable(boolean newEditState) {
    super.setEditable(newEditState);
    if (fenteController != null) {
      fenteController.setEditable(newEditState);
    }
    previousCourbes.setEditable(newEditState);

  }

  @Override
  protected AbstractTableModel createTableModel() {
    return new ProfilSectionLoiTableModel(tableGraphePanel, this);
  }

  @Override
  protected AbstractLoiCourbeModel createCourbeModel(AbstractLoi loi, ConfigLoi configLoi) {
    final ProfiLSectionCourbeModel courbeModel = ProfiLSectionCourbeModel.create((Loi)loi, configLoi);
    frtCourbeModel.setCourbeModel(courbeModel);
    return courbeModel;
  }

  public DonPrtGeoProfilSection createEdited() {
    if (validateLit().containsErrorOrSevereError()) {
      return null;
    }
    return new ProfilSectionEditionCreator(defaultValuesProvider).createEdited(this);

  }

  private void buildModels(CrueConfigMetier ccm, DonPrtGeoProfilSection profil) {
    ProfiLSectionCourbeModel model = getLoiModel();
    ValidateAndRebuildProfilSection indexBuilder = new ValidateAndRebuildProfilSection(ccm, null);
    CrueIOResu<List<LitNommeIndexed>> asIndexedData = indexBuilder.getAsIndexedData(profil);
    if (asIndexedData.getAnalyse().containsErrorOrSevereError()) {
      LogsDisplayer.displayError(asIndexedData.getAnalyse(), "");
    }
    //les lits nommes sont dans l'ordre
    List<LitNommeIndexed> litNommes = asIndexedData.getMetier();
    List<LitNommeLimite> limites = ccm.getLitNomme().getLimites();
    int limiteIdx = 0;
    model.getLoiLine(litNommes.get(0).getFirstIdx()).addEtiquette(etiquettesByLimites.get(limites.get(limiteIdx++)));
    for (LitNommeIndexed litNommeIndexed : litNommes) {
      if (litNommeIndexed.getLitNumerotesSize() == 0) {
        continue;
      }
      int last = litNommeIndexed.getlastIdx();
      ProfilSectionEtiquette litNommeEtiquette = etiquettesByLimites.get(limites.get(limiteIdx++));
      model.getLoiLine(last).addEtiquette(litNommeEtiquette);
      int litNumerotesSize = litNommeIndexed.getLitNumerotesSize();
      for (int i = 0; i < litNumerotesSize; i++) {
        LitNumeroteIndex litNumeroteIndex = litNommeIndexed.getLitNumeroteIndex(i);
        for (int idx = litNumeroteIndex.getIdxStart(); idx <= litNumeroteIndex.getIdxEnd(); idx++) {
          model.getLoiLine(idx).setFrt(litNumeroteIndex.getLitNumerote().getFrot());
        }
        if (i > 0) {
          model.getLoiLine(litNumeroteIndex.getIdxStart()).addEtiquette(etiquettesLimiteLit);
        }
      }
    }
    //les etiquettes:
    CrueIOResu<List<EtiquetteIndexed>> etiquettesAsIndexed = indexBuilder.getEtiquettesAsIndexed(profil);
    if (etiquettesAsIndexed.getAnalyse().containsErrorOrSevereError()) {
      LogsDisplayer.displayError(etiquettesAsIndexed.getAnalyse(), "");
    }
    List<EtiquetteIndexed> etiquettesIndexed = etiquettesAsIndexed.getMetier();
    for (EtiquetteIndexed etiquetteIndexed : etiquettesIndexed) {
      int idx = etiquetteIndexed.getIdx();
      if (idx >= 0) {
        model.getLoiLine(idx).addEtiquette(etiquettesByProfilEtiquette.get(etiquetteIndexed.getEtiquette().getTypeEtiquette()));
      }
    }

    //on enleve les LimLit sur les limites de litnommes.
    for (int i = 0; i < model.getNbValues(); i++) {
      model.getLoiLine(i).clean();
    }
  }
  EGAxeVertical axeFrt;

  public void addFrtCourbe(CrueConfigMetier ccm) throws MissingResourceException {
    ConfigLoi configLoi = ccm.getConfLoi().get(EnumTypeLoi.LoiZFK);
    if (axeFrt == null) {
      axeFrt = new EGAxeVertical(configLoi.getVarOrdonnee().getNom(), configLoi.getVarOrdonnee().getNature().getUnite());
      axeFrt.setSpecificFormat(new CtuluNumberFormatDefault(configLoi.getVarOrdonnee().getFormatter(DecimalFormatEpsilonEnum.COMPARISON)));
      axeFrt.setSpecificDetailFormat(new CtuluNumberFormatDefault(configLoi.getVarOrdonnee().getFormatter(DecimalFormatEpsilonEnum.COMPARISON)));
      axeFrt.setUserObject(configLoi.getVarOrdonnee());
      axeFrt.setUniteModifiable(false);
      axeFrt.setKey(configLoi.getTypeLoi().getId());
      axeFrt.setTitleModifiable(false);
      axeFrt.setDroite(true);
      axeFrt.setLineColor(Color.RED);
    }
    EGCourbeSimple oldFrt = courbeFrt;
    EGCourbeSimple oldEtiquette = courbeEtiquette;
    courbeFrt = new EGCourbeSimple(axeFrt, frtCourbeModel);
    frtCourbeModel.reload(ccm);
    courbeFrt.setTitle(NbBundle.getMessage(LoiMessages.class, "LoiFrotttement.CourbeName"));
    if (oldFrt != null) {
      courbeFrt.initGraphicConfigurationFrom(oldFrt);
    } else {
      courbeFrt.setIconeModel(new TraceIconModel(TraceIcon.LIGNE_HORIZONTAL, 5, Color.RED));
      courbeFrt.setLigneModel(new TraceLigneModel(TraceLigne.LISSE, 1, Color.RED));
    }
    courbeEtiquette = new EGCourbeSimple(null, etiquetteCourbeModel);
    courbeEtiquette.setTitle(etiquetteCourbeModel.etiquette.geti18n());
    if (oldEtiquette != null) {
      courbeEtiquette.initGraphicConfigurationFrom(oldEtiquette);
    } else {
      courbeEtiquette.setLigneModel(new TraceLigneModel(TraceLigne.LISSE, 1, Color.ORANGE));
    }
    getEGGrapheSimpleModel().addCourbe(courbeFrt, null, false);
    getEGGrapheSimpleModel().addCourbe(courbeEtiquette, null, false);
  }
  ProfilSectionOpenFrtTarget profilSectionOpenFrtTarget;

  public ProfilSectionOpenFrtTarget getProfilSectionOpenFrtTarget() {
    return profilSectionOpenFrtTarget;
  }

  public void setProfilSectionOpenFrtTarget(ProfilSectionOpenFrtTarget profilSectionOpenFrtTarget) {
    this.profilSectionOpenFrtTarget = profilSectionOpenFrtTarget;
  }

  protected boolean validateBeforeOpenFrtEditor() {
    return profilSectionOpenFrtTarget != null && profilSectionOpenFrtTarget.validateBeforeOpenFrtEditor();
  }

  @Override
  public EGFillePanel getPanel() {
    return super.getPanel();
  }

  void openSelectedFrt() {
    if (profilSectionOpenFrtTarget != null) {
      CtuluListSelection selection = getPanel().getSelection();
      if (selection != null && selection.getNbSelectedIndex() == 1) {
        ProfilSectionLine loi = ((ProfiLSectionCourbeModel) super.getLoiModel()).getLoiLine(selection.getMinIndex());
        if (profilSectionOpenFrtTarget.validateBeforeOpenFrtEditor()) {
          profilSectionOpenFrtTarget.openFrt(loi.getFrt());
        }
      }
    }

  }

  public void updateValidationState() throws MissingResourceException {
    CtuluLog log = validateLit();
    if (log == null || log.isEmpty()) {
      labelValidating.setIcon(null);
      labelValidating.setText(null);
      labelValidating.setToolTipText(null);
    } else {
      labelValidating.setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.ERROR));
      labelValidating.setText(org.openide.util.NbBundle.getMessage(LoiMessages.class, "ProfilSection.Lit.Error"));
      String toHtml = LogsHelper.toHtml(log);
      labelValidating.setToolTipText(toHtml);
    }
  }
}
