/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;

/**
 *
 * @author Frederic Deniger
 */
public class ProfilSectionDonFrtCellRenderer extends ObjetNommeCellRenderer {

  Color limitLitColor = new Color(220, 220, 220);

  @Override
  public Component getTableCellRendererComponent(JTable _table, Object _value, boolean _isSelected, boolean _hasFocus, int _row, int _column) {
    Component res = super.getTableCellRendererComponent(_table, _value, _isSelected, _hasFocus, _row, _column);
    ProfilSectionLoiTableModel model = (ProfilSectionLoiTableModel) _table.getModel();
    if (model.isNormalRow(_row)) {
      Color init = _table.getForeground();
      ProfilSectionLoiTableModel tableModel = (ProfilSectionLoiTableModel) _table.getModel();
      ProfilSectionLine line = tableModel.getLine(_row);
      final ProfiLSectionCourbeModel courbeModel = tableModel.getCourbeModel();
      boolean valueMustBeEmpy = courbeModel.isOutOfLitNommes(_row);
      if (!line.containsLimiteLitNomme() && !line.containsLimiteLit()) {
        init = Color.LIGHT_GRAY;
      } else {
        valueMustBeEmpy = courbeModel.isAfterLastLitNommeLine(_row + 1);//le dernier litNomme ne doit pas affiche de DonFrt
      }
      if (valueMustBeEmpy) {
        setText(StringUtils.EMPTY);
        setToolTipText(StringUtils.EMPTY);
      }
      setForeground(init);

    }
    return res;
  }

  @Override
  protected void setValue(Object _value) {
    if (_value != null && String.class.equals(_value.getClass())) {
      super.setText((String) _value);
      super.setToolTipText((String) _value);
    } else {
      super.setValue(_value);
    }
  }
}
