/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import gnu.trove.TDoubleArrayList;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.ItemContentAbstract;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.courbe.EGModelExportable;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.crue.loi.common.CourbeModelWithKey;

import java.awt.*;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Frederic Deniger
 */
public class ProfilSectionFrtCourbeModel implements CourbeModelWithKey, EGModelExportable {
  private static final String MODEL_PROFIL_FRT_KEY = "PROFIL_FRT";
  private ProfiLSectionCourbeModel courbeModel;
  private double[] x;
  private double[] y;
  private Boolean[] displayed;
  private double xmin;
  private double xmax;
  private double ymin;
  private double ymax;
  private Object key;
  private ItemContentAbstract varY;
  private ItemContentAbstract varX;

  @Override
  public Object getKey() {
    return key;
  }

  @Override
  public Color getSpecificColor(int idx) {
    return null;
  }

  @Override
  public boolean isViewableinTable() {
    return false;
  }

  @Override
  public boolean useSpecificIcon(int idx) {
    return false;
  }

  public void setKey(Object key) {
    this.key = key;
  }

  public void setCourbeModel(ProfiLSectionCourbeModel courbeModel) {
    this.courbeModel = courbeModel;
  }

  @Override
  public ItemContentAbstract getVariableAxeY() {
    return varY;
  }

  @Override
  public ItemContentAbstract getVariableAxeX() {
    return varX;
  }

  public void reload(CrueConfigMetier ccm) {
    if (courbeModel != null) {
      TDoubleArrayList xs = new TDoubleArrayList();
      TDoubleArrayList ys = new TDoubleArrayList();
      ArrayList<Boolean> displayedList = new ArrayList<>();
      int nbValues = courbeModel.getNbValues();
      DonFrt last = null;
      ConfigLoi configLoi = courbeModel.getConfigLoi();
      varX = null;
      varY = null;
      PropertyEpsilon epsilon = configLoi.getVarOrdonnee().getEpsilon();
      for (int i = 0; i < nbValues; i++) {
        ProfilSectionLine loiLine = courbeModel.getLoiLine(i);
        if (loiLine.containsLimiteLit() || loiLine.containsLimiteLitNomme()) {
          double x = loiLine.getPtEvolutionFF().getAbscisse();
          DonFrt frt = loiLine.getFrt();
          if (frt != null) {
            if (last != null) {
              xs.add(x);
              ys.add(LoiHelper.getOrdonneeMoyenne(last));
              displayedList.add(Boolean.FALSE);
            }
            final double ordonneeMoyenne = LoiHelper.getOrdonneeMoyenne(frt);
            xs.add(x);
            ys.add(ordonneeMoyenne);
            displayedList.add(Boolean.FALSE);
            final double ordonneeMax = LoiHelper.getOrdonneeMax(frt);
            final double ordonneeMin = LoiHelper.getOrdonneeMin(frt);
            if (!epsilon.isSame(ordonneeMoyenne, ordonneeMax) || !epsilon.isSame(ordonneeMoyenne, ordonneeMin)) {
              xs.add(x);
              ys.add(ordonneeMax);
              displayedList.add(Boolean.TRUE);
              xs.add(x);
              ys.add(ordonneeMin);
              displayedList.add(Boolean.TRUE);
              //pour repartir du milieu:
              xs.add(x);
              ys.add(ordonneeMoyenne);
              displayedList.add(Boolean.FALSE);
            }
          }
          if (ccm != null && frt != null && frt.getLoi() != null) {
            ConfigLoi configLoiFrt = ccm.getConfLoi().get(frt.getLoi().getType());
            if (configLoiFrt != null) {
              varX = configLoiFrt.getVarAbscisse();
              varY = configLoiFrt.getVarOrdonnee();
            }
          }
          last = frt;
        }
      }

      x = xs.toNativeArray();
      y = ys.toNativeArray();
      displayed = displayedList.toArray(new Boolean[0]);

      xmin = xs.isEmpty() ? 0 : xs.min();
      xmax = xs.isEmpty() ? 0 : xs.max();
      ymin = ys.isEmpty() ? 0 : ys.min();
      ymax = ys.isEmpty() ? 0 : ys.max();
      setKey(MODEL_PROFIL_FRT_KEY);//utilise pour persistence.
    }
  }

  @Override
  public boolean isRemovable() {
    return true;
  }

  @Override
  public boolean isDuplicatable() {
    return false;
  }

  @Override
  public int getNbValues() {
    return x == null ? 0 : x.length;
  }

  @Override
  public boolean isSegmentDrawn(int idx) {
    return true;
  }

  @Override
  public boolean isPointDrawn(int idx) {
    return displayed[idx].booleanValue();
  }

  @Override
  public String getPointLabel(int idx) {
    return null;
  }

  @Override
  public double getX(int idx) {
    return x[idx];
  }

  @Override
  public double getY(int idx) {
    return y[idx];
  }

  @Override
  public double getXMin() {
    return xmin;
  }

  @Override
  public double getXMax() {
    return xmax;
  }

  @Override
  public double getYMin() {
    return ymin;
  }

  @Override
  public double getYMax() {
    return ymax;
  }

  @Override
  public boolean isModifiable() {
    return false;
  }

  @Override
  public boolean isXModifiable() {
    return false;
  }

  @Override
  public boolean setValue(int idx, double x, double y, CtuluCommandContainer commandContainer) {
    return false;
  }

  @Override
  public boolean addValue(double x, double y, CtuluCommandContainer commandContainer) {
    return false;
  }

  @Override
  public boolean addValue(double[] x, double[] y, CtuluCommandContainer commandContainer) {
    return false;
  }

  @Override
  public boolean removeValue(int idx, CtuluCommandContainer commandContainer) {
    return false;
  }

  @Override
  public boolean removeValue(int[] idx, CtuluCommandContainer commandContainer) {
    return false;
  }

  @Override
  public boolean deplace(int[] selectIdx, double deltaX, double deltaY, CtuluCommandContainer commandContainer) {
    return false;
  }

  @Override
  public boolean setValues(int[] idx, double[] x, double[] y, CtuluCommandContainer commandContainer) {
    return false;
  }

  @Override
  public String getTitle() {
    return "Frottement";
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  @Override
  public boolean setTitle(String newName) {
    return false;
  }

  @Override
  public void fillWithInfo(InfoData infoData, CtuluListSelectionInterface selectedPt) {
  }

  @Override
  public EGModel duplicate() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void viewGenerationSource(Map infos, CtuluUI impl) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean isGenerationSourceVisible() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void replayData(EGGrapheModel model, Map infos, CtuluUI impl) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean isReplayable() {
    return false;
  }

  @Override
  public Object savePersistSpecificDatas() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void restoreFromSpecificDatas(Object data, Map infos) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public int[] getInitRows() {
    return null;
  }
}
