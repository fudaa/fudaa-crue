/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSectionFenteData;
import org.fudaa.fudaa.crue.common.view.ItemVariableCompositeView;
import org.fudaa.fudaa.crue.loi.LoiMessages;
import org.openide.util.NbBundle;

/**
 * Gestion des données relatives aux fentes.
 *
 * @author Frederic Deniger
 */
public class ProfilSectionFenteController extends Observable {

  final ItemVariableCompositeView fenteEditor = new ItemVariableCompositeView();
  final JCheckBox cbActive = new JCheckBox();
  final JPanel panel = new JPanel(new BorderLayout(5, 5));

  ProfilSectionFenteController() {
    cbActive.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateEnableState();
        setChanged();
        notifyObservers(ProfilSectionFenteController.this);
      }
    });
    cbActive.setText(NbBundle.getMessage(LoiMessages.class, "FenteData.CheckBox"));
    panel.add(cbActive, BorderLayout.NORTH);
    final JPanel fenteValues = fenteEditor.getPanel();
    fenteValues.setBorder(BorderFactory.createEmptyBorder(2, 15, 2, 0));
    panel.add(fenteValues);
    panel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));

  }
  boolean editable;

  public boolean isEditable() {
    return editable;
  }

  public boolean isValid() {
    if (cbActive.isSelected()) {
      return fenteEditor.isValide();
    }
    return true;
  }

  public void setEditable(boolean editable) {
    this.editable = editable;
    updateEnableState();
  }

  protected void updateEnableState() {
    cbActive.setEnabled(editable);
    fenteEditor.setEditable(cbActive.isSelected() && editable);
  }

  @Override
  public synchronized void addObserver(Observer o) {
    super.addObserver(o);
    fenteEditor.addObserver(o);
  }

  @Override
  public synchronized void deleteObserver(Observer o) {
    super.deleteObserver(o);
    fenteEditor.deleteObserver(o);
  }

  @Override
  public synchronized void deleteObservers() {
    super.deleteObservers();
    fenteEditor.deleteObservers();
  }

  public JPanel getPanel() {
    return panel;
  }

  protected DonPrtGeoProfilSectionFenteData getFente(CrueConfigMetier ccm) {
    if (cbActive.isSelected()) {
      DonPrtGeoProfilSectionFenteData res = new DonPrtGeoProfilSectionFenteData(ccm);
      res.setLargeurFente(fenteEditor.getValue(CrueConfigMetierConstants.PROP_LARGEUR_FENTE));
      res.setProfondeurFente(fenteEditor.getValue(CrueConfigMetierConstants.PROP_PROFONDEUR_FENTE));
      return res;
    }
    return null;
  }

  public void setCcm(CrueConfigMetier ccm) {
    fenteEditor.setVariables(Arrays.asList(ccm.getProperty(CrueConfigMetierConstants.PROP_LARGEUR_FENTE), ccm.getProperty(CrueConfigMetierConstants.PROP_PROFONDEUR_FENTE)));
  }

  public boolean hasVariables() {
    return fenteEditor.hasVariables();
  }

  public void setProfil(DonPrtGeoProfilSection section, CrueConfigMetier ccm) {
    DonPrtGeoProfilSectionFenteData fente = section.getFente();
    cbActive.setSelected(fente != null);
    if (fente == null) {
      fente = new DonPrtGeoProfilSectionFenteData(ccm);
    }
    setCcm(ccm);
    fenteEditor.setValue(CrueConfigMetierConstants.PROP_LARGEUR_FENTE, fente.getLargeurFente());
    fenteEditor.setValue(CrueConfigMetierConstants.PROP_PROFONDEUR_FENTE, fente.getProfondeurFente());
    updateEnableState();
    panel.revalidate();
  }
}
