/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.ebli.courbe.EGAxe;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.ebli.courbe.EGGrapheModelListener;
import org.fudaa.ebli.courbe.EGObject;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneAsIcon;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.loi.LoiMessages;
import org.fudaa.fudaa.crue.loi.common.LoiDefaultCourbeModel;
import org.openide.util.NbBundle;

/**
 * Gère l'historique des courbes affichés ( n-1, n-2).
 *
 * @author Frederic Deniger
 */
public class ProfilSectionPreviousController {

  void setUseSectionName(boolean useSectionName) {
    nMoins1Line.useSectionName = useSectionName;
    nMoins2Line.useSectionName = useSectionName;
  }

  private class Line implements ActionListener {

    private final String key;
    DonPrtGeoProfilSection section;
    final JTextField txtDecalage = BuTextField.createDoubleField();
    final JButton btApply = new JButton(NbBundle.getMessage(LoiMessages.class, "Apply.Label"));
    EGCourbeSimple courbe;
    final JLabel labelLigneAndName = new JLabel();
    final JLabel labelTitle = new JLabel();

    public Line(String key) {
      this.key = key;
      btApply.addActionListener(this);
      btApply.setEnabled(false);
      txtDecalage.setColumns(10);
      labelLigneAndName.setIcon(new TraceLigneAsIcon(null));
      labelLigneAndName.setText(DEFAULT);
      txtDecalage.getDocument().addDocumentListener(new DocumentListener() {
        @Override
        public void insertUpdate(DocumentEvent e) {
          decalageModified();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
          decalageModified();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
          decalageModified();
        }
      });
    }

    public JLabel getLabel() {
      return labelLigneAndName;
    }

    public JTextField getTxtDecalage() {
      return txtDecalage;
    }

    public JButton getBtApply() {
      return btApply;
    }

    public JLabel getLabelTitle() {
      return labelTitle;
    }

    public void setTitle(String title) {
      labelTitle.setText(title);
    }

    protected void decalageModified() {
      btApply.setEnabled(txtDecalage.isEditable());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      setDecalage(Double.parseDouble(txtDecalage.getText()));
      btApply.setEnabled(false);
    }
    boolean useSectionName;

    private void createCourbe(String title, DonPrtGeoProfilSection section, TraceLigneModel traceLigne) {
      final ConfigLoi configLoi = uiController.getCcm().getConfLoi().get(EnumTypeLoi.LoiPtProfil);
      LoiDefaultCourbeModel model = new LoiDefaultCourbeModel(section.getPtProfil(), configLoi.getVarAbscisse(), configLoi.getVarOrdonnee());
      model.setKey(key);
      EGCourbeSimple mainProfilCourbe = uiController.getCourbe();
      EGCourbeSimple old = courbe;
      courbe = new EGCourbeSimple(mainProfilCourbe.getAxeY(), model);
      if (old != null) {
        courbe.initGraphicConfigurationFrom(old);
      } else {
        courbe.setLigneModel(traceLigne);
      }
      courbe.setTitle(title);
      txtDecalage.setText("0");
      btApply.setEnabled(false);
      labelLigneAndName.setIcon(new TraceLigneAsIcon(courbe.getLigneModel()));
      if (useSectionName) {
        labelLigneAndName.setText(section.getEmh().getNom());
      } else {
        labelLigneAndName.setText(section.getNom());
      }

    }
    private static final String DEFAULT = "            ";

    private void resetCourbe() {
      courbe = null;
      txtDecalage.setText("0");
      btApply.setEnabled(false);
      labelLigneAndName.setIcon(new TraceLigneAsIcon(null));
      labelLigneAndName.setText(DEFAULT);
    }

    protected void updateIcon() {
      if (labelLigneAndName != null) {
        labelLigneAndName.setIcon(new TraceLigneAsIcon(courbe.getLigneModel()));
      }
    }

    public void setDecalage(double decal) {
      txtDecalage.setText(Double.toString(decal));
      if (courbe != null) {
        LoiDefaultCourbeModel model = (LoiDefaultCourbeModel) courbe.getModel();
        model.setDecalageY(decal);
        uiController.getGraphe().courbeContentChanged(courbe, false);
      }
    }

    public void setEditable(boolean editable) {
      txtDecalage.setEditable(editable && courbe != null);
    }
  }
  private final Line nMoins1Line = new Line("NMoins1");
  private final Line nMoins2Line = new Line("NMoins2");
  private final ProfilSectionLoiUiController uiController;
  private JPanel pnDecalage;

  public ProfilSectionPreviousController(ProfilSectionLoiUiController uiController) {
    this.uiController = uiController;
    uiController.getGraphe().addModelListener(new EGGrapheModelListener() {
      @Override
      public void structureChanged() {
      }

      @Override
      public void courbeContentChanged(EGObject _c, boolean _mustRestore) {
      }

      @Override
      public void courbeAspectChanged(EGObject _c, boolean _visibil) {
        if (_c == nMoins1Line.courbe) {
          nMoins1Line.updateIcon();
        } else if (_c == nMoins2Line.courbe) {
          nMoins2Line.updateIcon();
        }
      }

      @Override
      public void axeContentChanged(EGAxe _c) {
      }

      @Override
      public void axeAspectChanged(EGAxe _c) {
      }
    });
  }

  protected void updateIcons() {
    nMoins1Line.updateIcon();
    nMoins2Line.updateIcon();
  }

  public DonPrtGeoProfilSection getNmoins1() {
    return nMoins1Line.section;
  }

  public DonPrtGeoProfilSection getNmoins2() {
    return nMoins2Line.section;
  }

  public JPanel getDecalPanel() {
    if (pnDecalage == null) {
      pnDecalage = new JPanel(new BuGridLayout(4, 5, 5, true, false, false, false));
      nMoins1Line.setTitle(NbBundle.getMessage(LoiMessages.class, "ProfilSection.DecalageZNMoins1Label"));
      nMoins2Line.setTitle(NbBundle.getMessage(LoiMessages.class, "ProfilSection.DecalageZNMoins2Label"));
      pnDecalage.add(nMoins1Line.getLabel());
      pnDecalage.add(nMoins1Line.getLabelTitle());
      pnDecalage.add(nMoins1Line.getTxtDecalage());
      pnDecalage.add(nMoins1Line.getBtApply());
      pnDecalage.add(nMoins2Line.getLabel());
      pnDecalage.add(nMoins2Line.getLabelTitle());
      pnDecalage.add(nMoins2Line.getTxtDecalage());
      pnDecalage.add(nMoins2Line.getBtApply());
    }
    return pnDecalage;

  }

  void setSection(DonPrtGeoProfilSection nMoins1Profil, DonPrtGeoProfilSection nMoins2Profil) {
    this.nMoins1Line.section = nMoins1Profil;
    this.nMoins2Line.section = nMoins2Profil;
    if (nMoins1Profil != null) {
      String name = nMoins1Profil.getNom();
      if (this.nMoins1Line.useSectionName && nMoins1Profil.getEmh() != null) {
        name = nMoins1Profil.getEmh().getNom();
      }
      this.nMoins1Line.createCourbe(name, nMoins1Profil, new TraceLigneModel(TraceLigne.MIXTE, 1, Color.LIGHT_GRAY));
      uiController.getEGGrapheSimpleModel().addCourbe(this.nMoins1Line.courbe, null, false);
    } else {
      this.nMoins1Line.resetCourbe();
    }
    if (nMoins2Profil != null) {
      String name = nMoins2Profil.getNom();
      if (this.nMoins1Line.useSectionName && nMoins2Profil.getEmh() != null) {
        name = nMoins2Profil.getEmh().getNom();
      }
      this.nMoins2Line.createCourbe(name, nMoins2Profil, new TraceLigneModel(TraceLigne.POINTILLE, 1, Color.LIGHT_GRAY));
      uiController.getEGGrapheSimpleModel().addCourbe(this.nMoins2Line.courbe, null, false);
    } else {
      this.nMoins2Line.resetCourbe();
    }
  }

  public void setDecalageNMoins1(double decal) {
    nMoins1Line.setDecalage(decal);
  }

  public void setDecalageNMoins2(double decal) {
    nMoins2Line.setDecalage(decal);
  }

  public void setEditable(boolean editable) {
    nMoins1Line.setEditable(editable);
    nMoins2Line.setEditable(editable);
  }
}
