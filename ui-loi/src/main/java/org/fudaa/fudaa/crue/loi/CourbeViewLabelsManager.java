/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi;

import com.memoire.bu.BuGridLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.loi.ViewCourbeConfig;
import org.fudaa.fudaa.crue.loi.common.MouseAdapterDoubleCick;

/**
 *
 * @author Frederic Deniger
 */
public class CourbeViewLabelsManager {

  JPanel mainPanel;
  ViewCourbeConfig config;
  MouseAdapterDoubleCick doubleClickedListener;

  public CourbeViewLabelsManager(ViewCourbeConfig config) {
    this.config = config;
  }

  public void setConfig(ViewCourbeConfig config) {
    this.config = config;
  }

  public void setDoubleClickedListener(ActionListener doubleClickedListener) {
    if (this.doubleClickedListener != null && mainPanel != null) {
      Component[] components = mainPanel.getComponents();
      for (Component component : components) {
        JLabel lb = (JLabel) component;
        lb.removeMouseListener(this.doubleClickedListener);
      }
    }
    this.doubleClickedListener = doubleClickedListener == null ? null : new MouseAdapterDoubleCick(doubleClickedListener);
    installDoubleClickListener();
  }

  protected void createPanel() {
    mainPanel = new JPanel(new BuGridLayout(1, 0, 5, true, false));
    mainPanel.setOpaque(false);
    mainPanel.setDoubleBuffered(false);
    installDoubleClickListener();
  }

  public List<Pair<JLabel, LabelConfig>> getLabels() {
    List<Pair<JLabel, LabelConfig>> res = new ArrayList<>();
    Component[] components = mainPanel.getComponents();
    for (Component component : components) {
      JLabel lb = (JLabel) component;
      res.add(new Pair<>(lb, (LabelConfig) lb.getClientProperty("LabelConfig")));
    }
    return res;
  }

  public void applyConfig() {
    mainPanel.removeAll();
    for (LabelConfig labelConfig : config.getLabels()) {
      JLabel label = new JLabel();
      if (doubleClickedListener != null) {
        label.addMouseListener(doubleClickedListener);
      }
      labelConfig.apply(label);
      label.putClientProperty("LabelConfig", labelConfig);
      mainPanel.add(label);
    }
    mainPanel.revalidate();
    mainPanel.repaint(0);
  }

  public JPanel gePanel() {
    if (mainPanel == null) {
      createPanel();
    }
    return mainPanel;
  }

  protected void installDoubleClickListener() {
    if (this.doubleClickedListener != null && mainPanel != null) {
      Component[] components = mainPanel.getComponents();
      for (Component component : components) {
        JLabel lb = (JLabel) component;
        lb.addMouseListener(this.doubleClickedListener);
      }
    }
  }
}
