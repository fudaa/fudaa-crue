/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.common;

import com.jidesoft.swing.CheckBoxList;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGCourbeCellRenderer;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.fudaa.crue.common.helper.CheckBoxListPopupListener;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.loi.LoiMessages;
import org.openide.DialogDescriptor;
import org.openide.NotifyDescriptor;
import org.openide.util.NbBundle;

import javax.swing.*;

/**
 *
 * @author Frederic Deniger
 */
public class CourbeVisibiltyUI {

  final EGGraphe graphe;

  public CourbeVisibiltyUI(final EGGraphe graphe) {
    this.graphe = graphe;
  }

  public void display() {
    final EGCourbe[] courbes = graphe.getModel().getCourbes();
    final CheckBoxList list = new CheckBoxList(courbes);
    list.setCellRenderer(new EGCourbeCellRenderer());
    for (int i = 0; i < courbes.length; i++) {
      if (courbes[i].isVisible()) {
        list.addCheckBoxListSelectedIndex(i);
      }
    }

    final DialogDescriptor desc = new DialogDescriptor(new JScrollPane(list), NbBundle.getMessage(LoiMessages.class, "CourbeVisibility.DialogTitle"));
    desc.setOptions(new Object[]{NotifyDescriptor.OK_OPTION});
    list.getCheckBoxListSelectionModel().addListSelectionListener(e -> {
      if (e.getValueIsAdjusting()) {
        return;
      }
      for (int i = 0; i < courbes.length; i++) {
        courbes[i].setVisible(list.getCheckBoxListSelectionModel().isSelectedIndex(i));
      }
    });
    new CheckBoxListPopupListener(list);
    SysdocUrlBuilder.installDialogHelpCtx(desc, "bdlAffichageCourbes", null,false);
    DialogHelper.showQuestion(desc, getClass(), DialogHelper.NO_VERSION_FOR_PERSISTENCE);
  }
}
