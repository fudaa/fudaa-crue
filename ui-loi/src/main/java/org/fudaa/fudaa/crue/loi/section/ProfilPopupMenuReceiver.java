/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import java.awt.event.ActionEvent;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.loi.LoiMessages;
import org.fudaa.fudaa.crue.loi.common.LoiPopupMenuReceiver;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ProfilPopupMenuReceiver extends LoiPopupMenuReceiver {

  private final ProfilSectionLoiUiController sectionController;
  private OpenFrt openFrt;

  public ProfilPopupMenuReceiver(ProfilSectionLoiUiController panel) {
    super(panel.getPanel());
    this.sectionController = panel;
  }

  public ProfilSectionLoiUiController getSectionController() {
    return sectionController;
  }

  /**
   * a redefinir si on veut ajouter un item pour l'importer de fichier
   */
  protected void addImportItem() {

  }

  @Override
  protected void addItems() {
    super.addItems();
    addImportItem();
    popupMenu.addSeparator();
    openFrt = new OpenFrt(sectionController);
    popupMenu.add(openFrt);
  }

  @Override
  protected void updateItemStateBeforeShow() {
    super.updateItemStateBeforeShow();
    openFrt.updateStateBeforeShow();
  }

  protected static class OpenFrt extends EbliActionSimple {

    final ProfilSectionLoiUiController sectionController;

    public OpenFrt(ProfilSectionLoiUiController sectionController) {
      super(NbBundle.getMessage(LoiMessages.class, "OpenFrt.Action"), null, "OPEN_FRT");
      this.sectionController = sectionController;
    }

    @Override
    public void actionPerformed(ActionEvent _e) {
      sectionController.openSelectedFrt();
    }

    @Override
    public void updateStateBeforeShow() {
      CtuluListSelection selection = sectionController.getPanel().getSelection();
      setEnabled(selection != null && selection.getNbSelectedIndex() == 1);

    }
  }
}
