package org.fudaa.fudaa.crue.loi.section;

import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.courbe.*;

import java.awt.*;

/**
 * Surcharge de EGCourbeSimple  pour ajouter les widgets de configuration pour la coloration des types de lit.
 *
 * @author deniger
 */
public class ProfilSectionCourbe extends EGCourbeSimple {
  protected static final String PROP_COLOR_LIT_MINEUR = "COLOR_LIT_MINEUR";
  protected static final String PROP_COLOR_LIT_MAJEUR = "COLOR_LIT_MAJEUR";
  protected static final String PROP_COLOR_STOCKAGE = "COLOR_LIT_STOCKAGE";

  public ProfilSectionCourbe(EGAxeVertical egAxeVertical, ProfiLSectionCourbeModel profiLSectionCourbeModel) {
    super(egAxeVertical, profiLSectionCourbeModel);
  }

  @Override
  protected BConfigurableComposite createConfigurableDisplay() {
    return new BConfigurableComposite(new ProfileDisplayConfiguration(this), EbliLib.getS("Courbe"));
  }

  @Override
  public ProfiLSectionCourbeModel getModel() {
    return (ProfiLSectionCourbeModel) super.getModel();
  }

  public boolean setColorLitMineur(Color colorLitMineur) {
    if (colorLitMineur != getColorLitMineur()) {
      Color old = getModel().getColorLitMineur();
      getModel().setColorLitMineur(colorLitMineur);
      firePropertyChange(PROP_COLOR_LIT_MINEUR, old, colorLitMineur);
      fireCourbeAspectChanged(false);
    }
    return false;
  }

  public Color getColorLitMineur() {
    return getModel().getColorLitMineur();
  }

  public Color getColorLitMajeur() {
    return getModel().getColorLitMajeur();
  }

  public boolean setColorLitMajeur(Color colorLitMajeur) {
    if (colorLitMajeur != getColorLitMajeur()) {
      Color old = this.getColorLitMajeur();
      getModel().setColorLitMajeur(colorLitMajeur);
      firePropertyChange(PROP_COLOR_LIT_MAJEUR, old, colorLitMajeur);
      fireCourbeAspectChanged(false);
    }
    return false;
  }

  public Color getColorStockage() {
    return getModel().getColorStockage();
  }

  @Override
  public EGCourbePersist getPersitUiConfig() {
    EGCourbeSimplePersistBuilderDefault builder = new ProfilePersistBuilder();
    return builder.persistGraphicsData(this);
  }

  @Override
  public void applyPersitUiConfig(EGCourbePersist persist) {
    if (persist != null) {
      new ProfilePersistBuilder().initGraphicConfiguration(this, persist);
    }
  }

  protected boolean setColorStockage(Color colorStockage) {
    if (colorStockage != this.getColorStockage()) {
      Color old = this.getColorStockage();
      getModel().setColorStockage(colorStockage);
      firePropertyChange(PROP_COLOR_STOCKAGE, old, colorStockage);
      fireCourbeAspectChanged(false);
    }
    return false;
  }

  /**
   * Pour persister les données supplémentaires concernant les couleurs des lits.
   */
  private static class ProfilePersistBuilder extends EGCourbeSimplePersistBuilderDefault {
    @Override
    public void initGraphicConfiguration(EGCourbe courbeToModify, EGCourbePersist persist) {
      super.initGraphicConfiguration(courbeToModify, persist);
      ProfilSectionCourbe profilSectionCourbe = (ProfilSectionCourbe) courbeToModify;
      Color color = persist.getColor(PROP_COLOR_LIT_MINEUR);
      if (color != null) {
        profilSectionCourbe.setColorLitMineur(color);
      }
      color = persist.getColor(PROP_COLOR_LIT_MAJEUR);
      if (color != null) {
        profilSectionCourbe.setColorLitMajeur(color);
      }
      color = persist.getColor(PROP_COLOR_STOCKAGE);
      if (color != null) {
        profilSectionCourbe.setColorStockage(color);
      }
    }

    @Override
    protected void saveGraphicsData(EGCourbePersist res, EGCourbeSimple courbe) {
      super.saveGraphicsData(res, courbe);
      ProfilSectionCourbe profilSectionCourbe = (ProfilSectionCourbe) courbe;
      res.setColor(PROP_COLOR_LIT_MINEUR, profilSectionCourbe.getColorLitMineur());
      res.setColor(PROP_COLOR_LIT_MAJEUR, profilSectionCourbe.getColorLitMajeur());
      res.setColor(PROP_COLOR_STOCKAGE, profilSectionCourbe.getColorStockage());
    }
  }
}
