/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.common;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.crue.config.ccm.ItemContentAbstract;
import org.fudaa.dodico.crue.metier.emh.Point2D;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

import java.awt.*;
import java.util.List;
import java.util.Map;

/**
 * @author Frederic Deniger
 */
public class LoiDefaultCourbeModel implements EGModel, CourbeModelWithKey {
  private double deltaY;
  private Object key;
  private final List<? extends Point2D> points;
  private String title;
  private final ItemContentAbstract varX;
  private final ItemContentAbstract varY;
  private double xmax;
  private double xmin;
  private double ymax;
  private double ymin;

  public LoiDefaultCourbeModel(List<? extends Point2D> points, ItemContentAbstract varX, ItemContentAbstract varY) {
    this.varX = varX;
    this.varY = varY;

    this.points = points;
    xmin = points.get(0).getAbscisse();
    xmax = xmin;
    ymin = points.get(0).getOrdonnee();
    ymax = ymin;
    for (Point2D ptEvolutionFF : points) {
      xmin = Math.min(xmin, ptEvolutionFF.getAbscisse());
      xmax = Math.max(xmax, ptEvolutionFF.getAbscisse());
      ymin = Math.min(ymin, ptEvolutionFF.getOrdonnee());
      ymax = Math.max(ymax, ptEvolutionFF.getOrdonnee());
    }
  }

  @Override
  public Object getKey() {
    return key;
  }

  public void setKey(Object key) {
    this.key = key;
  }

  @Override
  public ItemContentAbstract getVariableAxeY() {
    return varY;
  }

  @Override
  public ItemContentAbstract getVariableAxeX() {
    return varX;
  }

  @Override
  public boolean useSpecificIcon(int idx) {
    return false;
  }

  public void setDecalageY(double deltaY) {
    this.deltaY = deltaY;
  }

  @Override
  public boolean isRemovable() {
    return true;
  }

  @Override
  public boolean isDuplicatable() {
    return false;
  }

  @Override
  public int getNbValues() {
    return points.size();
  }

  @Override
  public boolean isSegmentDrawn(int idx) {
    return true;
  }

  @Override
  public boolean isPointDrawn(int idx) {
    return true;
  }

  @Override
  public String getPointLabel(int idx) {
    return null;
  }

  @Override
  public Color getSpecificColor(int idx) {
    return null;
  }

  @Override
  public double getX(int idx) {
    return points.get(idx).getAbscisse();
  }

  @Override
  public double getY(int idx) {
    return points.get(idx).getOrdonnee() + deltaY;
  }

  @Override
  public double getXMin() {
    return xmin;
  }

  @Override
  public double getXMax() {
    return xmax;
  }

  @Override
  public double getYMin() {
    return ymin + deltaY;
  }

  @Override
  public double getYMax() {
    return ymax + deltaY;
  }

  @Override
  public boolean isModifiable() {
    return false;
  }

  @Override
  public boolean isXModifiable() {
    return false;
  }

  @Override
  public boolean setValue(int idx, double x, double y, CtuluCommandContainer commandContainer) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean addValue(double x, double y, CtuluCommandContainer commandContainer) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean addValue(double[] x, double[] y, CtuluCommandContainer commandContainer) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean removeValue(int idx, CtuluCommandContainer commandContainer) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean removeValue(int[] idx, CtuluCommandContainer commandContainer) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean deplace(int[] selectIdx, double deltaX, double deltaY, CtuluCommandContainer commandContainer) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean setValues(int[] idx, double[] x, double[] y, CtuluCommandContainer commandContainer) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  @Override
  public boolean setTitle(String newName) {
    this.title = newName;
    return true;
  }

  @Override
  public void fillWithInfo(InfoData infoData, CtuluListSelectionInterface selectedPt) {
  }

  @Override
  public EGModel duplicate() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void viewGenerationSource(Map infos, CtuluUI impl) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean isGenerationSourceVisible() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void replayData(EGGrapheModel model, Map infos, CtuluUI impl) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean isReplayable() {
    return false;
  }

  @Override
  public Object savePersistSpecificDatas() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void restoreFromSpecificDatas(Object data, Map infos) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public int[] getInitRows() {
    return null;
  }
}
