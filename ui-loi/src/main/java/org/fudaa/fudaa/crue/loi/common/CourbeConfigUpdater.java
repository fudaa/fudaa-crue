/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.common;

import org.fudaa.ebli.courbe.*;
import org.fudaa.fudaa.crue.options.config.CourbeConfig;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author Frederic Deniger
 */
public class CourbeConfigUpdater {

  private final AbstractLoiUiController loiUiController;
  private final boolean updateRange;

  public CourbeConfigUpdater(AbstractLoiUiController loiUiController, boolean updateRange) {
    this.loiUiController = loiUiController;
    this.updateRange = updateRange;
  }

  public void apply(CourbeConfig config) {
    if (config == null) {
      return;
    }
    EGCourbeSimple[] courbes = loiUiController.getEGGrapheSimpleModel().getCourbes();
    Collection<EGAxeVertical> axesY = new HashSet<>();
    for (EGCourbeSimple courbe : courbes) {
      if (courbe.getAxeY() != null) {
        axesY.add(courbe.getAxeY());
      }
      if (courbe.getModel() instanceof CourbeModelWithKey) {
        Object key = ((CourbeModelWithKey) courbe.getModel()).getKey();
        EGCourbePersist courbeConfig = config.getCourbeConfig(key);
        if (courbeConfig != null) {
          courbe.applyPersitUiConfig(courbeConfig);
        }
      }
    }
    EGAxeHorizontalPersist horizontalPersist = config.getHorizontalPersist();
    if (horizontalPersist != null) {
      final EGAxeHorizontal axeX = loiUiController.getAxeX();
      String unit = axeX.getUnite();
      String titre = axeX.getTitre();
      if (updateRange) {
        horizontalPersist.apply(axeX);
      } else {
        horizontalPersist.applyButRange(axeX);
      }
      axeX.setTitre(titre);
      axeX.setUnite(unit);

    }
    for (EGAxeVertical axeY : axesY) {
      Object key = axeY.getKey();
      String oldTitle = axeY.getTitre();
      String oldUnite = axeY.getUnite();
      EGAxeVerticalPersist axeVConfig = config.getAxeVConfig(key);
      if (axeVConfig != null) {
        if (updateRange) {
          axeVConfig.applyAllButRange(axeY);
        } else {
          axeVConfig.apply(axeY);
        }
        axeY.setTitre(oldTitle);
        axeY.setUnite(oldUnite);
      }
    }
    loiUiController.getPanel().getGraphe().repaint();
  }

  public CourbeConfig create(CourbeConfig init) {
    CourbeConfig res = init;
    if (res == null) {
      res = new CourbeConfig();
    }
    res.saveAxeH(new EGAxeHorizontalPersist(loiUiController.getAxeX()));
    EGCourbeSimple[] courbes = loiUiController.getEGGrapheSimpleModel().getCourbes();
    for (EGCourbeSimple courbe : courbes) {
      if (courbe.getModel() instanceof CourbeModelWithKey) {
        Object key = ((CourbeModelWithKey) courbe.getModel()).getKey();
        res.saveConfig(key, courbe.getPersitUiConfig());
      }
    }
    List<EGAxeVertical> axesY = loiUiController.getAxesY();
    for (EGAxeVertical axeY : axesY) {
      Object key = axeY.getKey();
      if (key != null) {
        res.saveAxeVConfig(key, new EGAxeVerticalPersist(axeY));
      }
    }
    return res;
  }
}
