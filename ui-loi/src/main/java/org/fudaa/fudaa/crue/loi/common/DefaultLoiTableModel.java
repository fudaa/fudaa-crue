package org.fudaa.fudaa.crue.loi.common;

import gnu.trove.TDoubleArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluDoubleParser;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTableExportInterface;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.ebli.courbe.EGAxe;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGTableGraphePanel;
import org.fudaa.ebli.courbe.EGTableGraphePanel.SpecTableModel;
import org.fudaa.ebli.courbe.EGTableModelUpdatable;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.loi.LoiMessages;
import org.openide.util.NbBundle;

/**
 * Etend le tableModel par defaut de ebli-graphe, pour ajouter une colonne indiquant les erreurs/warnings et pour ne rÃ©agir qu'au courbe des points.
 *
 * @author Frederic Deniger
 */
public class DefaultLoiTableModel extends SpecTableModel implements CtuluTableExportInterface, EGTableModelUpdatable {

  public DefaultLoiTableModel(EGTableGraphePanel graphePanel) {
    super(graphePanel);
    xColIndex = 1;
    yColIndex = 2;
  }

  @Override
  public int getColumnCount() {
    return 3;
  }

  @Override
  public boolean isColumnExportable(int colModel) {
    return colModel != 0;
  }

  @Override
  public int updateLines(final List _tab, final int _selectedColumm,
          final int _selectedRow, final CtuluDoubleParser _doubleParser, final CtuluCommandComposite _cmp) {
    final int nbLine = _tab.size();
    final int maxUpdate = Math.min(nbLine, getCourbe().getModel().getNbValues() - _selectedRow);
    setShowError(false);
    setHashErrorInModification(false);

    for (int i = 0; i < maxUpdate; i++) {
      final List listCell = (List) _tab.get(i);
      final int max = Math.min(getRowCount() - _selectedColumm, listCell.size());
      for (int j = 0; j < max; j++) {
        final int iCellule = i + _selectedRow;
        final int jCellule = j + _selectedColumm;
        if (jCellule >= 1) {
          final Object o = listCell.get(j);
          setValueInModel(o, _doubleParser, iCellule, jCellule, _cmp);
        }
      }
    }
    setShowError(false);
    if (isHasErrorInModification()) {
      DialogHelper.showError(NbBundle.getMessage(LoiMessages.class, "LoiChangePoint.MassiveModificationFailure"));
    }
    return maxUpdate;
  }

  @Override
  public void addValuesInModel(List _tab, CtuluDoubleParser _doubleParser, CtuluCommandComposite _cmp, int _maxUpdate) {
    final int nbLine = _tab.size();
    final TDoubleArrayList newX = new TDoubleArrayList(nbLine - _maxUpdate + 1);
    final TDoubleArrayList newY = new TDoubleArrayList(nbLine - _maxUpdate + 1);
    for (int i = _maxUpdate; i < nbLine; i++) {
      final List listCell = (List) _tab.get(i);
      if (listCell.size() >= 2) {
        Object o = listCell.get(0);
        boolean ok = false;
        double x = 0;
        double y = 0;
        if (o instanceof Number) {
          x = ((Number) o).doubleValue();
          ok = true;
        } else if (o != null) {
          try {
            x = _doubleParser.parse(o.toString());
            ok = true;
          } catch (final NumberFormatException e) {
            ok = false;
          }
        }
        if (ok) {
          o = listCell.get(1);
          if (o instanceof Number) {
            y = ((Number) o).doubleValue();
            ok = true;
          } else if (o != null) {
            try {
              y = _doubleParser.parse(o.toString());
              ok = true;
            } catch (final NumberFormatException e) {
              ok = false;
            }
          }
        }
        if (ok) {
          newX.add(x);
          newY.add(y);
        }

      }

    }
    if (newX.size() > 0) {
      boolean ok = addValueAt(newX.toNativeArray(), newY.toNativeArray(), _cmp);
      //on commence à la colonne 1 pour eviter la première colonne contenant l'index
      updateLines(_tab, 1, 0, _doubleParser, _cmp);
    }
  }

  protected void setShowError(boolean value) {
    if (getCourbe().getModel() instanceof AbstractLoiCourbeModel) {
      ((AbstractLoiCourbeModel) getCourbe().getModel()).setShowError(value);
    }
  }

  protected void setHashErrorInModification(boolean value) {
    if (getCourbe().getModel() instanceof AbstractLoiCourbeModel) {
      ((AbstractLoiCourbeModel) getCourbe().getModel()).setHasErrorInModification(value);
    }
  }

  protected boolean isShowError() {
    if (getCourbe().getModel() instanceof AbstractLoiCourbeModel) {
      return ((AbstractLoiCourbeModel) getCourbe().getModel()).isShowError();
    }
    return false;
  }

  protected boolean isHasErrorInModification() {
    if (getCourbe().getModel() instanceof AbstractLoiCourbeModel) {
      return ((AbstractLoiCourbeModel) getCourbe().getModel()).isHasErrorInModification();
    }
    return false;
  }

  protected boolean setValueInModel(final Object o, final CtuluDoubleParser _doubleParser, final int iCellule, final int jCellule,
          final CtuluCommandComposite _cmp) {
    boolean ok = false;
    double d = 0;
    if (o instanceof Number) {
      d = ((Number) o).doubleValue();
      ok = true;
    } else if (o != null) {
      try {
        d = _doubleParser.parse(o.toString());
        ok = true;
      } catch (final NumberFormatException e) {
        ok = false;
      }
    }
    if (ok) {
      ok = setValueAt(d, iCellule, jCellule, _cmp);
    }
    return ok;
  }

  @Override
  protected void selectedCourbeChanged(EGCourbe _a) {
    if (_a == null && getCourbe() == null) {
      return;
    }
    if (_a == null || _a.getModel() instanceof AbstractLoiCourbeModel) {
      super.selectedCourbeChanged(_a);
    }
  }

  @Override
  public Class getColumnClass(int _columnIndex) {
    if (_columnIndex == 0) {
      return String.class;
    }
    return super.getColumnClass(_columnIndex);
  }

  String getTitle(EGAxe axe) {
    String res = axe.getTitre();
    if (StringUtils.isNotBlank(axe.getUnite())) {
      res = res + PropertyNature.getUniteSuffixe(axe.getUnite());
    }
    return res;

  }

  @Override
  public String getColumnName(int _column) {
    if (_column == 0) {
      return "Sel.";
    }
    String res = CtuluLibString.ESPACE;
    if (getH() != null && _column == xColIndex) {
      res = getTitle(getH());
    } else if (_column == yColIndex && getCourbe() != null && getCourbe().getAxeY() != null) {
      res = getTitle(getCourbe().getAxeY());
    }
    return (res == null || res.length() == 0) ? CtuluLibString.ESPACE : res;
  }

  @Override
  public boolean isCellEditable(int _rowIndex, int _columnIndex) {
    if (_columnIndex == 0) {
      return false;
    }
    return super.isCellEditable(_rowIndex, _columnIndex);
  }

  @Override
  public Object getValueAt(int _rowIndex, int _columnIndex) {
    if (_columnIndex == 0) {
      return CtuluLibString.getString(_rowIndex + 1);
    }
    return super.getValueAt(_rowIndex, _columnIndex);
  }
}
