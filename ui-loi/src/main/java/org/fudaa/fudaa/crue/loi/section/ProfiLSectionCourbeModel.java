/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierLitNomme;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.config.lit.LitNommeLimite;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiCourbeModel;
import org.fudaa.fudaa.crue.loi.common.LoiLine;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public class ProfiLSectionCourbeModel extends AbstractLoiCourbeModel {
  private static final Color DEFAULT_GREEN = new Color(12, 178, 31);
  private static final Color DEFAULT_BROWN = new Color(109, 81, 20);
  private static final Color DEFAULT_RED = new Color(201, 40, 60);
  private Color colorLitMajeur = DEFAULT_GREEN;
  private Color colorLitMineur = DEFAULT_BROWN;
  private Color colorStockage = DEFAULT_RED;

  public static ProfiLSectionCourbeModel create(Loi loi, ConfigLoi configLoi) {
    ProfiLSectionCourbeModel res = new ProfiLSectionCourbeModel(configLoi);
    res.setPts(loi.getEvolutionFF().getPtEvolutionFF());
    return res;
  }

  public Color getColorLitMajeur() {
    return colorLitMajeur;
  }

  public void setColorLitMajeur(Color colorLitMajeur) {
    this.colorLitMajeur = colorLitMajeur;
  }

  public Color getColorLitMineur() {
    return colorLitMineur;
  }

  public void setColorLitMineur(Color colorLitMineur) {
    this.colorLitMineur = colorLitMineur;
  }

  public Color getColorStockage() {
    return colorStockage;
  }

  public void setColorStockage(Color colorStockage) {
    this.colorStockage = colorStockage;
  }

  @Override
  public void fireChanged() {
    super.fireChanged();
  }

  @Override
  public boolean useSpecificIcon(int idx) {
    ProfilSectionLine loiLine = getLoiLine(idx);
    return loiLine != null && CollectionUtils.isNotEmpty(loiLine.getEtiquettes());
  }

  public boolean isAfterLastLitNommeLine(int idx) {
    for (int i = idx; i < getNbValues(); i++) {
      if (getLoiLine(i).containsLimiteLitNomme()) {
        return false;
      }
    }
    return true;
  }

  @Override
  public String getPointLabel(int idx) {
    ProfilSectionLine loiLine = getLoiLine(idx);
    if (loiLine.containsLimiteLit() || loiLine.containsLimiteLitNomme()) {
      return loiLine.getLimitesAsString();
    }
    return super.getPointLabel(idx);
  }

  @Override
  public Color getSpecificColor(int idx) {
    LitNommeLimite before = null;
    LitNommeLimite after = null;
    for (int i = 0; i <= idx; i++) {
      ProfilSectionLine loiLine = getLoiLine(i);
      final LitNommeLimite lastDefinedLitNomme = loiLine.getLastDefinedLitNomme();
      if (lastDefinedLitNomme != null) {
        before = lastDefinedLitNomme;
      }
    }
    for (int i = idx + 1; i < lines.size(); i++) {
      ProfilSectionLine loiLine = getLoiLine(i);
      final LitNommeLimite lastDefinedLitNomme = loiLine.getFirstDefinedLitNomme();
      if (lastDefinedLitNomme != null) {
        after = lastDefinedLitNomme;
        break;
      }
    }
    //hors rive:
    if (after == null || before == null) {
      return null;
    }
    if (before.getIdx() == CrueConfigMetierLitNomme.MAJEUR_START
        || before.isLitMineurEnd()
    ) {
      return colorLitMajeur;
    }
    if (before.isLitMineurStart()) {
      return colorLitMineur;
    }
    return colorStockage;
  }

  public int getLastLitNommeIdx() {
    for (int i = getNbValues() - 1; i >= 0; i--) {
      if (getLoiLine(i).containsLimiteLitNomme()) {
        return i;
      }
    }
    return -1;
  }

  public boolean isOutOfLitNommes(int idx) {
    return isAfterLastLitNommeLine(idx) || isBeforeFirstLitNommeLine(idx);
  }

  private boolean isBeforeFirstLitNommeLine(int idx) {
    final int max = Math.min(idx, getNbValues() - 1);
    for (int i = 0; i <= max; i++) {
      if (getLoiLine(i).containsLimiteLitNomme()) {
        return false;
      }
    }
    return true;
  }

  List<ProfilSectionLine> getProfilSectionLoiLine() {
    List<ProfilSectionLine> res = new ArrayList<>();
    for (LoiLine loiLine : lines) {
      res.add((ProfilSectionLine) loiLine);
    }
    return res;
  }

  private ProfiLSectionCourbeModel(ConfigLoi configLoi) {
    super(configLoi);
  }

  public ProfilSectionLine findLine(ItemEnum limite) {
    int nb = getNbValues();
    for (int i = 0; i < nb; i++) {
      ProfilSectionLine loiLine = getLoiLine(i);
      if (loiLine.isUsed(limite)) {
        return loiLine;
      }
    }
    return null;
  }

  @Override
  protected ProfilSectionLine getLoiLine(int idx) {
    return (ProfilSectionLine) super.getLoiLine(idx);
  }

  @Override
  protected LoiLine createLoiLineInstance() {
    return new ProfilSectionLine();
  }
}
