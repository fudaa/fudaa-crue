/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.casier;

import org.fudaa.dodico.crue.comparaison.CrueComparatorHelper;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.comparator.Point2dAbscisseComparator;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilCasier;
import org.fudaa.dodico.crue.metier.emh.LitUtile;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.fudaa.crue.loi.casier.ProfilCasierLine.Limite;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiCourbeModel;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiUiController;

import javax.swing.table.AbstractTableModel;

/**
 * @author Frederic Deniger
 */
public class ProfilCasierLoiUiController extends AbstractLoiUiController {
    ProfilCasierLitUtileState litUtileState;
    CrueConfigMetier ccm;

    public ProfilCasierLoiUiController() {
        setUseVariableForAxeH(true);
    }

    public ProfilCasierLitUtileState getLitUtileState() {
        if (litUtileState == null) {
            litUtileState = new ProfilCasierLitUtileState((ProfilCasierLoiTableModel) tableGraphePanel.getTable().getModel());
        }
        return litUtileState;
    }

    @Override
    public ProfilCasierCourbeModel getLoiModel() {
        return (ProfilCasierCourbeModel) super.getLoiModel();
    }

    public CrueConfigMetier getCcm() {
        return ccm;
    }

    public void setProfilCasier(DonPrtGeoProfilCasier profil, CrueConfigMetier ccm) {
        this.ccm = ccm;
        if (profil == null) {
            super.setLoi(null, ccm, false);
            return;
        }
        super.setLoi(LoiHelper.createFromProfil(profil.getPtProfil(), profil.getNom()), ccm, false);
        //faut ajouter le renderer et editor.
        tableGraphePanel.getTable().getColumnModel().getColumn(3).setCellRenderer(new ProfilCasierLitUtileCellRenderer());
        tableGraphePanel.getTable().getColumnModel().getColumn(3).setCellEditor(new ProfilCasierLitUtileCellEditor());
        ProfilCasierCourbeModel model = getLoiModel();
        LitUtile litUtile = profil.getLitUtile();
        if (litUtile != null) {
            Point2dAbscisseComparator ptProfilComparator = new Point2dAbscisseComparator(
                    ccm.getConfLoi().get(EnumTypeLoi.LoiPtProfil).getVarAbscisse().getEpsilon());
            final int positionLimDeb = CrueComparatorHelper.getEqualsPtProfilSorted(litUtile.getLimDeb(), profil.getPtProfil(), ptProfilComparator);
            final int positionLimFin = CrueComparatorHelper.getEqualsPtProfilSorted(litUtile.getLimFin(), profil.getPtProfil(), ptProfilComparator);
            if (positionLimDeb >= 0) {
                model.getLoiLine(positionLimDeb).setLimite(Limite.DEB);
            }
            if (positionLimFin >= 0) {
                model.getLoiLine(positionLimFin).setLimite(Limite.FIN);
            }
        }
    }

    @Override
    protected AbstractTableModel createTableModel() {
        return new ProfilCasierLoiTableModel(tableGraphePanel);
    }

    @Override
    protected AbstractLoiCourbeModel createCourbeModel(AbstractLoi loi, ConfigLoi configLoi) {
        return ProfilCasierCourbeModel.create((Loi) loi, configLoi);
    }
}
