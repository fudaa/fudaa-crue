/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.casier;

import org.fudaa.ctulu.gui.CtuluCellTextRenderer;

/**
 *
 * @author Frederic Deniger
 */
public class ProfilCasierLitUtileCellRenderer extends CtuluCellTextRenderer {

  @Override
  protected void setValue(Object value) {
    if (value != null) {
      super.setValue(((ProfilCasierLine.Limite) value).getI18n());
    } else {
      super.setValue(value);
    }
  }
}
