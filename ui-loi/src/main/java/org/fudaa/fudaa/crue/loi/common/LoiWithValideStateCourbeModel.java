/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.common;

import org.fudaa.ctulu.CtuluLog;

/**
 * Pour des raisons de perf permet de stocker les validations des valeurs. A utilise que pour des courbes immuables !
 *
 * @author Frederic Deniger
 */
public interface LoiWithValideStateCourbeModel {

  void setTested(int row, boolean isX, CtuluLog log);

  boolean isTested(int row,boolean isX);

  CtuluLog getLog(int row, boolean isX);
}
