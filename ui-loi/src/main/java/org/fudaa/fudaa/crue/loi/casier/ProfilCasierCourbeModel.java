/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.casier;

import java.util.List;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilCasier;
import org.fudaa.dodico.crue.metier.emh.LitUtile;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.PtProfil;
import org.fudaa.fudaa.crue.loi.casier.ProfilCasierLine.Limite;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiCourbeModel;
import org.fudaa.fudaa.crue.loi.common.LoiLine;

/**
 *
 * @author Frederic Deniger
 */
public class ProfilCasierCourbeModel extends AbstractLoiCourbeModel {

  public static ProfilCasierCourbeModel create(Loi loi, ConfigLoi configLoi) {
    ProfilCasierCourbeModel res = new ProfilCasierCourbeModel(configLoi);
    res.setPts(loi.getEvolutionFF().getPtEvolutionFF());
    return res;
  }

  @Override
  public void fireChanged() {
    super.fireChanged();
  }

  @Override
  public boolean useSpecificIcon(int idx) {
    return false;
  }

  /**
   * Attention, ne remplit pas entièrement le profilCasier: le commentaire et longeur ne sont pas définit. La longeur est initalisée avec la valeur
   * par defaut.
   *
   * @return un profil casier avec les points et le litUtile si défini correction
   */
  public DonPrtGeoProfilCasier createProfilCasier(CrueConfigMetier ccm) {
    DonPrtGeoProfilCasier res = new DonPrtGeoProfilCasier(ccm);
    List<LoiLine> loiLines = super.getLoiLines();
    for (LoiLine loiLine : loiLines) {
      res.addPtProfil(new PtProfil(loiLine.getPtEvolutionFF()));
    }
    ProfilCasierLine litDeb = findLine(Limite.DEB);
    ProfilCasierLine litFin = findLine(Limite.FIN);
    if (litDeb != null && litFin != null) {
      LitUtile lit = new LitUtile();
      lit.setLimDeb(new PtProfil(litDeb.getPtEvolutionFF()));
      lit.setLimFin(new PtProfil(litFin.getPtEvolutionFF()));
      res.setLitUtile(lit);
    }
    return res;
  }

  public ProfilCasierCourbeModel(ConfigLoi configLoi) {
    super(configLoi);
  }

  public ProfilCasierLine findLine(ProfilCasierLine.Limite limite) {
    int nb = getNbValues();
    for (int i = 0; i < nb; i++) {
      ProfilCasierLine loiLine = getLoiLine(i);
      if (limite == loiLine.getLimite()) {
        return loiLine;
      }
    }
    return null;
  }

  public int findLineIdx(ProfilCasierLine.Limite limite) {
    int nb = getNbValues();
    for (int i = 0; i < nb; i++) {
      ProfilCasierLine loiLine = getLoiLine(i);
      if (limite == loiLine.getLimite()) {
        return i;
      }
    }
    return -1;
  }

  @Override
  protected ProfilCasierLine getLoiLine(int idx) {
    return (ProfilCasierLine) super.getLoiLine(idx);
  }

  @Override
  protected LoiLine createLoiLineInstance() {
    return new ProfilCasierLine();
  }
}
