package org.fudaa.fudaa.crue.loi.common;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.gui.PopupMenuReceiver;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ctulu.iterator.FixedIntegerIterator;
import org.fudaa.dodico.crue.common.time.ToStringTransformerSecond;
import org.fudaa.dodico.crue.common.transformer.CtuluNumberFormatAppender;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;
import org.fudaa.dodico.crue.metier.transformer.EnumNumberFormatter;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.courbe.*;
import org.fudaa.fudaa.crue.common.action.ExportImageAction;
import org.fudaa.fudaa.crue.common.action.ExportImageToClipboardAction;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.loi.loiff.LoiTimeExportDecorator;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;

/**
 * Gère le graphe 1d, l'axe horizontal, la barre d'outils et le caractère éditable.
 *
 * @author Frederic Deniger
 */
public class CourbesUiController implements CtuluImageProducer {
    protected final EGFillePanel panel;
    protected final EGTableGraphePanel tableGraphePanel;
    protected final EGAxeHorizontal axeH;
    protected final JPanel toolbar;
    protected boolean useVariableForAxeH;
    protected final List<EbliActionInterface> editCourbeActions = new ArrayList<>();
    protected boolean editable;

    public CourbesUiController() {
        this(new HashSet<>(Collections.singletonList("SUIVIALL")));
    }

    public CourbesUiController(Set<String> actionToAvoid) {
        final EGGrapheSimpleModel grapheModel = new LoiGrapheSimpleModel();
        axeH = new EGAxeHorizontal();
        grapheModel.setAxeX(axeH);
        final EGGraphe graphe = new EGGraphe(grapheModel);
        panel = new EGFillePanel(graphe);
        panel.setAddSimplifyAction(false);
        panel.setShowRefreshButtonInConfigurePanel(false);
        panel.setMinimumSize(new Dimension(300, 300));
        tableGraphePanel = new EGTableGraphePanel(true);
        tableGraphePanel.setAddTopButtons(false);
        configureTablePanel();//oblige de le faire ici.
        panel.majSelectionListener(tableGraphePanel);
        tableGraphePanel.setGraphe(graphe);
        tableGraphePanel.addPanelAction(panel);
        EbliActionInterface[] specificActions = panel.getSpecificActions();
        toolbar = new JPanel(new FlowLayout(FlowLayout.LEFT));
        Set<String> editAction = new HashSet<>(Arrays.asList("MOVE_POINT", "ADD_POINT", "SIMPLIFY"));
        for (EbliActionInterface action : specificActions) {
            if (action == null) {
                toolbar.add(new JSeparator());
            } else {
                final String name = (String) action.getValue(Action.ACTION_COMMAND_KEY);
                if (!actionToAvoid.contains(name)) {
                    toolbar.add(action.buildToolButton(EbliComponentFactory.INSTANCE));
                }
                if (editAction.contains(name)) {
                    editCourbeActions.add(action);
                    action.setEnabled(false);
                }
            }
        }
        toolbar.add(new JSeparator());
        toolbar.add(new CourbeVisibiltyUIAction(graphe).buildToolButton(EbliComponentFactory.INSTANCE));
        EGTableAction tableAction = (EGTableAction) getActions("TABLE").get(0);
        tableAction.setUi(CtuluUIForNetbeans.DEFAULT);
        tableAction.setAddOptions(false);
        tableAction.setAddCheckbox(true);
        tableAction.setShowColumnToExport(false);
        tableAction.setDisplayAll(true);
        PopupMenuReceiver popupTable = createLoiPopupReceiver();
        popupTable.install(tableGraphePanel.getTable(), CtuluUIForNetbeans.DEFAULT);
        LoiPopupMenuReceiver popupGraphe = createLoiPopupReceiver();
        popupGraphe.setEditEnable(true);
        popupGraphe.install(panel.getGraphe(), tableGraphePanel.getTable(), CtuluUIForNetbeans.DEFAULT);
        configurePasteAndImportButtons();
    }

    // Gere le copy/paste.
    public void configurePasteAndImportButtons() {
        final AbstractAction pasteAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                specificPaste();
            }
        };
        tableGraphePanel.getTable().getActionMap().put("paste", pasteAction);
        ActionListener[] actionListeners = tableGraphePanel.getBtCopy().getActionListeners();
        for (ActionListener actionListener : actionListeners) {
            tableGraphePanel.getBtCopy().removeActionListener(actionListener);
        }
        tableGraphePanel.getBtCopy().addActionListener(pasteAction);
        actionListeners = tableGraphePanel.getBtImport().getActionListeners();
        for (ActionListener actionListener : actionListeners) {
            tableGraphePanel.getBtImport().removeActionListener(actionListener);
        }
        tableGraphePanel.getBtImport().addActionListener(e -> specificImport());
    }

    protected void specificPaste() {
        tableGraphePanel.getTable().paste();
    }

    protected void specificImport() {
        tableGraphePanel.tableImport();
    }

    protected void configureTablePanel() {
        tableGraphePanel.getTable().setModel(createTableModel());
        tableGraphePanel.setPreferredSize(new Dimension(300, 600));
        new LoiTimeExportDecorator(tableGraphePanel.getTable());
    }

    public boolean isUseVariableForAxeH() {
        return useVariableForAxeH;
    }

    public void setUseVariableForAxeH(boolean useVariableForAxeH) {
        this.useVariableForAxeH = useVariableForAxeH;
    }

    public void installComboxSelector() {
        tableGraphePanel.getTitleLabel().setVisible(false);
        tableGraphePanel.remove(tableGraphePanel.getTitleLabel());
        ComboboxCourbeSelector cb = new ComboboxCourbeSelector(getGraphe());
        tableGraphePanel.add(cb.getCb(), BorderLayout.NORTH);
    }

    public EGGrapheSimpleModel getEGGrapheSimpleModel() {
        return (EGGrapheSimpleModel) getGraphe().getModel();
    }

    public EGTableGraphePanel getTableGraphePanel() {
        return tableGraphePanel;
    }

    public EGAxeHorizontal getAxeX() {
        return getEGGrapheSimpleModel().getAxeX();
    }

    public List<EGAxeVertical> getAxesY() {
        return getGraphe().getAllAxeVertical();
    }

    @Override
    public BufferedImage produceImage(final Map _params) {
        return panel.produceImage(_params);
    }

    @Override
    public BufferedImage produceImage(int _w, int _h, Map _params) {
        return panel.produceImage(_w, _h, _params);
    }

    @Override
    public Dimension getDefaultImageDimension() {
        return panel.getDefaultImageDimension();
    }

    public void addExportImagesToToolbar() {
        toolbar.add(new ExportImageAction(panel).buildToolButton(EbliComponentFactory.INSTANCE));
        toolbar.add(new ExportImageToClipboardAction(panel).buildToolButton(EbliComponentFactory.INSTANCE));
    }

    public JPanel getToolbar() {
        return toolbar;
    }

    public EGFillePanel getPanel() {
        return panel;
    }

    public EGGraphe getGraphe() {
        return panel.getGraphe();
    }

    public void removeEditActions() {
        tableGraphePanel.removeEditButtonsButCopy();
        removeActions("MOVE_POINT", "SIMPLIFY");
    }

    public void removeActions(String... actionCommands) {
        if (toolbar != null) {
            Set<String> command = new HashSet<>(Arrays.asList(actionCommands));
            Component[] components = toolbar.getComponents();
            for (Component component : components) {
                if (component instanceof AbstractButton) {
                    String actionCommand = ((AbstractButton) component).getActionCommand();
                    if (command.contains(actionCommand)) {
                        toolbar.remove(component);
                    }
                }
            }
        }
    }

    public List<EbliActionInterface> getActions(String... command) {
        EbliActionInterface[] specificActions = panel.getSpecificActions();
        List<EbliActionInterface> res = new ArrayList<>();
        Set<String> filter = new HashSet<>(Arrays.asList(command));
        for (EbliActionInterface action : specificActions) {
            if (action == null) {
                continue;
            }
            String cmd = (String) action.getValue(Action.ACTION_COMMAND_KEY);
            if (filter.contains(cmd)) {
                res.add(action);
            }
        }
        return res;
    }

    public List<EbliActionInterface> getEditActions() {
        return getActions("CONFIGURE", "CONFIGURE_REPERE");
    }

    public void addToolbarAction(EbliActionInterface action) {
        toolbar.add(action.buildToolButton(EbliComponentFactory.INSTANCE), toolbar.getComponentCount() - 2);
    }

    public void setEditable(boolean b) {
        editable = b;
        tableGraphePanel.updateState();
        for (EbliActionInterface ebliActionInterface : editCourbeActions) {
            ebliActionInterface.setEnabled(b);
        }
    }

    protected AbstractTableModel createTableModel() {
        return new EGTableGraphePanel.SpecTableModel(tableGraphePanel);
    }

    public void configureAxeH(final PropertyNature natureAbscisse, boolean usePresentation) {
        if (natureAbscisse == null) {
            return;
        }
        axeH.setTitre(natureAbscisse.getDisplayName());
        axeH.setUniteModifiable(false);
        axeH.setTitleModifiable(false);
        axeH.setUnite(natureAbscisse.getUnite());
        installFormatter(natureAbscisse, axeH, usePresentation);
    }

    public void configureAxeH(final ItemVariable varAbscisse, boolean usePresentation) {
        if (varAbscisse == null) {
            return;
        }
        configureAxeH(varAbscisse.getNature(), usePresentation);
        axeH.setTitre(varAbscisse.getDisplayNom());
        axeH.setUserObject(varAbscisse);
    }

    public EGAxeVertical findAxe(final ItemVariable varOrdonnee) {
        if (varOrdonnee == null) {
            return null;
        }
        return findAxe(varOrdonnee.getNature());
    }

    public EGAxeVertical findAxe(final PropertyNature nature) {
        if (nature == null) {
            return null;
        }
        EGCourbeSimple[] courbes = getEGGrapheSimpleModel().getCourbes();
        for (EGCourbeSimple courbe : courbes) {
            if (courbe.getAxeY() != null && nature.getNom().equals(courbe.getAxeY().getKey())) {
                return courbe.getAxeY();
            }
        }
        return null;
    }

    public EGAxeVertical createAxeVertical(final ItemVariable varOrdonnee, boolean usePresentation) {
        if (varOrdonnee == null) {
            return null;
        }
        return createAxeVerticalInternal(varOrdonnee.getNature(), usePresentation, null);
    }

    public EGAxeVertical createAxeVertical(final ItemVariable varOrdonnee, boolean usePresentation, String axeName) {
        if (varOrdonnee == null) {
            return null;
        }
        return createAxeVerticalInternal(varOrdonnee.getNature(), usePresentation, axeName);
    }

    public EGAxeVertical createAxeVertical(final PropertyNature nature, boolean usePresentation) {
        return createAxeVerticalInternal(nature, usePresentation, null);
    }

    /**
     * Création d'un axe vertical
     *
     * @param nature          la nature servant à créer l'axe
     * @param usePresentation true s'il faut utiliser l'espilon de présentation, false s'il faut utiliser l'epsilon de comparaison
     * @param axeName         le nom de l'axe pour le forcer. Si vide, le DisplayName de la nature est utilisé
     * @return l'axe créé
     */
    private EGAxeVertical createAxeVerticalInternal(final PropertyNature nature, boolean usePresentation, String axeName) {
        EGAxeVertical axeV = null;
        if (nature != null) {
            axeV = new EGAxeVertical(axeName == null || axeName.isEmpty() ? nature.getDisplayName() : axeName, nature.getUnite());
        } else {
            axeV = new EGAxeVertical("Null", "");
        }
        axeV.setUniteModifiable(false);
        axeV.setTitleModifiable(false);
        axeV.setKey(nature == null ? StringUtils.EMPTY : nature.getNom());
        installFormatter(nature, axeV, usePresentation);
        return axeV;
    }

    public void setAxeHSpecificFormats(CtuluNumberFormatI mainFormat, CtuluNumberFormatI detailFormat, final CtuluValueEditorI _valueEditor) {
        if (getAxeX() != null) {
            getAxeX().setSpecificFormat(mainFormat);
            getAxeX().setSpecificDetailFormat(detailFormat);
            getAxeX().setValueEditor(_valueEditor);
        }
    }

    protected void installFormatter(final PropertyNature var, EGAxe axe, boolean usePresentation) {
        if (var != null) {

            if (var.isEnum()) {
                axe.setSpecificFormat(new EnumNumberFormatter(var));
                axe.setAxisIterator(new FixedIntegerIterator(var.getItemEnumByValue().keys()));
                axe.setModeGraduations(EGAxe.MANUEL_LONGUEURPAS);
            } else {
                if (var.isDuration()) {
                    initAxeWithDuration(var, axe);
                } else {
                    axe.setSpecificFormat(new CtuluNumberFormatDefault(var.getFormatter(
                            usePresentation ? DecimalFormatEpsilonEnum.PRESENTATION : DecimalFormatEpsilonEnum.COMPARISON)));
                    axe.setSpecificDetailFormat(new CtuluNumberFormatDefault(var.getFormatter(DecimalFormatEpsilonEnum.COMPARISON)));
                }
            }
            axe.setIsDiscret(var.isEnum() || (var.getTypeNumerique() != null && var.getTypeNumerique().isEntier()));
        }
        axe.setUserObject(var);
    }

    public LoiPopupMenuReceiver createLoiPopupReceiver() {
        return new LoiPopupMenuReceiver(panel);
    }

    /**
     * Configure l'axeH.
     *
     * @param ccm
     * @param loi
     * @param usePresentationFormat
     */
    public void configureAxeH(CrueConfigMetier ccm, AbstractLoi loi, boolean usePresentationFormat) {
        final ItemVariable varAbscisse = ccm.getConfLoi().get(loi.getType()).getVarAbscisse();
        if (useVariableForAxeH) {
            configureAxeH(varAbscisse, usePresentationFormat);
        } else {
            configureAxeH(varAbscisse.getNature(), usePresentationFormat);
        }
    }

    public void initAxeWithDuration(final PropertyNature var, EGAxe axe) {
        final ToStringTransformerSecond toStringTransformerSecond = new ToStringTransformerSecond();
        final CtuluNumberFormatDefault toSecondFormat = new CtuluNumberFormatDefault(var.getFormatter(DecimalFormatEpsilonEnum.COMPARISON));
        axe.setValueEditor(new SecondValueEditor(toStringTransformerSecond, toSecondFormat));
        axe.setSpecificFormat(toStringTransformerSecond);
        axe.setSpecificFormatExport(toSecondFormat);
        axe.setSpecificDetailFormat(new CtuluNumberFormatAppender(toSecondFormat, " s"));
    }
}
