package org.fudaa.fudaa.crue.loi.res;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.gui.CtuluCellDecorator;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.LogsHelper;
import org.fudaa.dodico.crue.config.ccm.ItemContentAbstract;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.courbe.EGTableGraphePanel;
import org.fudaa.ebli.courbe.EGTableGraphePanel.SpecTableModel;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.fudaa.fudaa.crue.loi.common.*;

/**
 *
 * @author Frederic Deniger
 */
public class LoiStateResCellRenderer implements CtuluCellDecorator {

  @Override
  public void decore(JComponent _c, JList _table, Object _value, int _index) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void decore(JComponent _c, JTable _table, Object _value, int _row, int _col) {
    EGTableGraphePanel.SpecTableModel model = (EGTableGraphePanel.SpecTableModel) _table.getModel();
    EGModel courbeModel = model.getCourbe().getModel();
    final boolean stateCanBeSaved = courbeModel instanceof LoiWithValideStateCourbeModel;
    CtuluLog log = null;
    boolean isX = model.getXColIndex() == _col;
    boolean isY = model.getYColIndex() == _col;
    if (!isX && !isY) {
      return;
    }
    if (stateCanBeSaved) {
      LoiWithValideStateCourbeModel savedModel = (LoiWithValideStateCourbeModel) courbeModel;
      if (savedModel.isTested(_row, isX)) {
        log = savedModel.getLog(_row, isX);
      } else {
        log = createLog(model, _row, isX);
        if (log != null) {
          savedModel.setTested(_row, isX, log.isEmpty() ? null : log);
        }
      }
    } else {
      log = createLog(model, _row, isX);
    }
    ((JLabel) _c).setIcon(null);
    if (log != null && log.isNotEmpty()) {
      _c.setToolTipText(LogsHelper.toHtml(log));
      if (log.containsErrorOrSevereError()) {
        ((JLabel) _c).setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.ERROR));
      } else if (log.containsWarnings()) {
        ((JLabel) _c).setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.WARNING));
      }
    }
  }

  public CtuluLog createLog(SpecTableModel model, int _row, boolean isX) {
    CourbeModelWithKey courbeModel = (CourbeModelWithKey) model.getCourbe().getModel();
    ItemContentAbstract variable = isX ? courbeModel.getVariableAxeX() : courbeModel.getVariableAxeY();
    if (variable != null) {
      CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
      double value = isX ? model.getInitX(_row) : model.getInitY(_row);
      variable.getValidator().validateNumber(null, value, log, true);
      return log;
    }
    return null;
  }
}
