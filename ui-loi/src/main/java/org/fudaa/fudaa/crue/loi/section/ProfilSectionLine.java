package org.fudaa.fudaa.crue.loi.section;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizableTransformer;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.config.lit.LitNommeLimite;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.fudaa.crue.loi.LoiMessages;
import org.fudaa.fudaa.crue.loi.common.LoiLine;
import org.openide.util.NbBundle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Une ligne d'un profil casier qui permet de gérer les limites.
 *
 * @author Frederic Deniger
 */
public class ProfilSectionLine extends LoiLine {
  private final List<ProfilSectionEtiquette> etiquettes = new ArrayList<>();
  private final List<LitNommeLimite> limites = new ArrayList<>();
  private boolean containLimLit;
  private DonFrt frt;

  public List<ProfilSectionEtiquette> getEtiquettes() {
    return etiquettes;
  }

  public void addEtiquette(ProfilSectionEtiquette etiquette) {
    if (!this.etiquettes.isEmpty() && etiquettes.get(etiquettes.size() - 1) == etiquette) {
      return;
    }
    this.etiquettes.add(etiquette);
    if (etiquette.isLimiteLitNomme()) {
      limites.add(etiquette.getLitNommeLimite());
    }
    if (etiquette.isLimitLit()) {
      containLimLit = true;
    }
  }

  public void clean() {
    if (containsLimiteLit() && containsLimiteLitNomme()) {
      removeEtiquette(getLimLit());
    }
  }

  private ProfilSectionEtiquette getLimLit() {
    for (ProfilSectionEtiquette profilSectionEtiquette : etiquettes) {
      if (profilSectionEtiquette.isLimitLit()) {
        return profilSectionEtiquette;
      }
    }
    return null;
  }

  public boolean removeEtiquettes(Collection<ProfilSectionEtiquette> etiquettes) {
    boolean res = false;
    for (ProfilSectionEtiquette profilSectionEtiquette : etiquettes) {
      res |= removeEtiquette(profilSectionEtiquette);
    }
    return res;
  }

  private boolean removeEtiquette(ProfilSectionEtiquette etiquette) {
    if (etiquette == null) {
      return false;
    }
    boolean res = this.etiquettes.remove(etiquette);
    if (etiquette.isLimiteLitNomme()) {
      limites.remove(etiquette.getLitNommeLimite());
    }
    if (etiquette.isLimitLit()) {
      containLimLit = false;
    }
    return res;
  }

  public DonFrt getFrt() {
    return frt;
  }

  public void setFrt(DonFrt frt) {
    this.frt = frt;
  }

  void setEtiquettes(List newEtiquettes) {
    this.etiquettes.clear();
    limites.clear();
    containLimLit = false;
    if (newEtiquettes != null) {
      for (Object object : newEtiquettes) {
        addEtiquette((ProfilSectionEtiquette) object);
      }
    }
  }

  boolean containsLimiteLitNomme() {
    return !limites.isEmpty();
  }

  List<LitNommeLimite> getDefinedLimites() {
    return Collections.unmodifiableList(limites);
  }

  List<ItemEnum> getLocaleEtiquettes() {
    List<ItemEnum> res = new ArrayList<>();
    for (ProfilSectionEtiquette profilSectionEtiquette : etiquettes) {
      if (profilSectionEtiquette.isLocaleEtiquette()) {
        res.add(profilSectionEtiquette.getLocaleEtiquette());
      }
    }
    return res;
  }

  public boolean isUsed(ItemEnum et) {
    for (ProfilSectionEtiquette profilSectionEtiquette : etiquettes) {
      if (profilSectionEtiquette.isLocaleEtiquette() && profilSectionEtiquette.getLocaleEtiquette().equals(et)) {
        return true;
      }
    }
    return false;
  }

  protected LitNommeLimite getLastDefinedLitNomme() {
    return limites.isEmpty() ? null : limites.get(limites.size() - 1);
  }

  protected LitNommeLimite getFirstDefinedLitNomme() {
    return limites.isEmpty() ? null : limites.get(0);
  }

  boolean containsLimiteLit() {
    for (ProfilSectionEtiquette profilSectionEtiquette : etiquettes) {
      if (profilSectionEtiquette.isLimitLit()) {
        return true;
      }
    }
    return false;
  }

  String getLimitesAsString() {
    if (containLimLit) {
      return NbBundle.getMessage(LoiMessages.class, "ProfilSection.LimLitEtiquette");
    }
    if (!limites.isEmpty()) {
      return StringUtils.join(ToStringInternationalizableTransformer.toi18n(limites), "\n");
    }
    return StringUtils.EMPTY;
  }
}
