package org.fudaa.fudaa.crue.loi.common;

import java.awt.Component;
import javax.swing.JTable;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;

/**
 *
 * @author Frederic Deniger
 */
public class LoiStateCellRenderer extends CtuluCellTextRenderer {

  @Override
  public Component getTableCellRendererComponent(JTable _table, Object _value, boolean _isSelected, boolean _hasFocus, int _row, int _column) {
    super.getTableCellRendererComponent(_table, _value, _isSelected, _hasFocus, _row, _column);
    DefaultLoiTableModel model = (DefaultLoiTableModel) _table.getModel();
    AbstractLoiCourbeModel courbeModel = (AbstractLoiCourbeModel) model.getCourbe().getModel();
    setIcon(null);
    setToolTipText(null);
    if (_row >= courbeModel.getNbValues()) {
      return this;
    }
    LoiLine loiLine = courbeModel.getLoiLine(_row);
    if (loiLine.error != null) {
      setIcon(LogIconTranslationProvider.getIcon(loiLine.error));
    }
    if (loiLine.errorMessage != null) {
      setToolTipText(loiLine.errorMessage);
    }
    return this;
  }
}
