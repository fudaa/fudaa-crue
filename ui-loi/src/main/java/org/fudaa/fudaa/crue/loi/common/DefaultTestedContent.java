/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.common;

import gnu.trove.TIntObjectHashMap;
import java.util.BitSet;
import org.fudaa.ctulu.CtuluLog;

/**
 *
 * @author Frederic Deniger
 */
public class DefaultTestedContent implements LoiWithValideStateCourbeModel {

  private final BitSet testedX = new BitSet();
  private final BitSet testedY = new BitSet();
  private final TIntObjectHashMap<CtuluLog> logsByRowX = new TIntObjectHashMap<>();
  private final TIntObjectHashMap<CtuluLog> logsByRowY = new TIntObjectHashMap<>();

  private BitSet getTest(boolean isX) {
    return isX ? testedX : testedY;
  }

  private TIntObjectHashMap<CtuluLog> getLogs(boolean isX) {
    return isX ? logsByRowX : logsByRowY;
  }

  @Override
  public void setTested(int row, boolean isX, CtuluLog log) {
    getTest(isX).set(row);
    if (log != null) {
      getLogs(isX).put(row, log);
    }
  }

  @Override
  public boolean isTested(int row, boolean isX) {
    return getTest(isX).get(row);
  }

  @Override
  public CtuluLog getLog(int row, boolean isX) {
    return getLogs(isX).get(row);
  }
}
