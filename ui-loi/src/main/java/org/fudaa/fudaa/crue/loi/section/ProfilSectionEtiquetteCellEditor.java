/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import com.jidesoft.popup.JidePopup;
import com.jidesoft.swing.CheckBoxList;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import org.fudaa.ctulu.gui.CtuluCellButtonEditor;
import org.fudaa.fudaa.crue.common.helper.ToStringInternationalizableCellRenderer;
import org.fudaa.fudaa.crue.loi.LoiMessages;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ProfilSectionEtiquetteCellEditor extends CtuluCellButtonEditor {

  final ProfilSectionEtiquette[] etiquettes;
  final JTable ownerTable;

  public ProfilSectionEtiquetteCellEditor(List<ProfilSectionEtiquette> etiquettes, JTable ownerTable) {
    super(null);
    this.etiquettes = etiquettes.toArray(new ProfilSectionEtiquette[0]);
    this.ownerTable = ownerTable;
  }

  @Override
  public void setValue(Object value) {
    if (value != null) {
      String toString = ProfilSectionEtiquetteCellRenderer.etiquetteToString(value);
      super.setValue(toString);
      super.setToolTipText(toString);
    } else {
      super.setValue(value);
    }
  }

  @Override
  public void actionPerformed(final ActionEvent _ae) {
    doAction();
//    stopCellEditing();
  }

  @Override
  protected void doAction() {
    ownerTable.transferFocus();
    int editingColumn = ownerTable.getEditingColumn();
    int editingRow = ownerTable.getEditingRow();
    JidePopup popup = createEtiquettePopupEdition(etiquettes, (ProfilSectionLoiTableModel) ownerTable.getModel(), editingRow);
    popup.setOwner(ownerTable);
    Point location = ownerTable.getCellRect(editingRow, editingColumn, true).getLocation();
    SwingUtilities.convertPointToScreen(location, ownerTable);
    popup.showPopup(location.x, location.y);
  }

  public static JidePopup createEtiquettePopupEdition(ProfilSectionEtiquette[] etiquettes, ProfilSectionLoiTableModel tableModel, int editingRow) {
    List<ProfilSectionEtiquette> selected = tableModel.getEtiquettesAt(editingRow).getEtiquettes();
    CheckBoxList list = new CheckBoxListEtiquette(etiquettes, selected);
    list.addCheckBoxListSelectedValues(selected.toArray(new ProfilSectionEtiquette[0]));
    list.setCellRenderer(new ToStringInternationalizableCellRenderer());
    final boolean editable = tableModel.getCourbe().getModel().isModifiable();
    list.setCheckBoxEnabled(editable);
    final JidePopup popup = new JidePopup();
    popup.getContentPane().setLayout(new BorderLayout());
    popup.getContentPane().add(new JScrollPane(list));
    JButton btAccept = new JButton(NbBundle.getMessage(LoiMessages.class, "EditionValid"));
    JButton btClose = new JButton(NbBundle.getMessage(LoiMessages.class, "EditionCancel"));
    JPanel pnButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    if (editable) {
      pnButtons.add(btAccept);
    }
    pnButtons.add(btClose);
    final JLabel jLabel = new JLabel();
    popup.getContentPane().add(jLabel, BorderLayout.NORTH);
    popup.getContentPane().add(pnButtons, BorderLayout.SOUTH);
    popup.setResizable(true);
    popup.setMovable(true);
    popup.setAttachable(false);
    popup.setDefaultFocusComponent(list);
    final PopupMenuListenerAfterEdition popupMenuListenerImpl = new PopupMenuListenerAfterEdition(tableModel, editingRow, list);
    btClose.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        popup.hidePopupImmediately(true);
      }
    });
    btAccept.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        popupMenuListenerImpl.setAccept(true);
        popup.hidePopupImmediately(false);
      }
    });
    popup.addPopupMenuListener(popupMenuListenerImpl);
    return popup;
  }

  private static class PopupMenuListenerAfterEdition implements PopupMenuListener {

    boolean accept;
    int row;
    CheckBoxList list;
    ProfilSectionLoiTableModel tableModel;

    public PopupMenuListenerAfterEdition(ProfilSectionLoiTableModel tableModel, int row, CheckBoxList list) {
      this.row = row;
      this.list = list;
      this.tableModel = tableModel;
    }

    public void setAccept(boolean accept) {
      this.accept = accept;
    }

    public PopupMenuListenerAfterEdition() {
    }

    @Override
    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
    }

    @Override
    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
      if (accept) {
        Object[] checkBoxListSelectedValues = list.getCheckBoxListSelectedValues();
        tableModel.setEtiquettes(Arrays.asList(checkBoxListSelectedValues), row);
      }
    }

    @Override
    public void popupMenuCanceled(PopupMenuEvent e) {
    }
  }

  private static class CheckBoxListEtiquette extends CheckBoxList {

    private final Set<ProfilSectionEtiquette> initEtiquettes;

    public CheckBoxListEtiquette(ProfilSectionEtiquette[] listData, List<ProfilSectionEtiquette> selected) {
      super(listData);
      this.initEtiquettes = new HashSet<>(selected);
    }

    @Override
    public boolean isCheckBoxEnabled(int index) {
      if (!isCheckBoxEnabled()) {
        return false;
      }
      final ProfilSectionEtiquette element = (ProfilSectionEtiquette) getModel().getElementAt(index);
      if (initEtiquettes.contains(element) && element.isLimiteLitNomme()) {
        return false;
      }
      return super.isCheckBoxEnabled(index);
    }
  }
}
