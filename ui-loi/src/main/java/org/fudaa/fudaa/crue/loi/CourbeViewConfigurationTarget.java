/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi;

import java.util.List;
import javax.swing.Action;

/**
 *
 * @author Frederic Deniger
 */
public interface CourbeViewConfigurationTarget {

  void applyLabelsChanged();

  void applyTitleChanged();
  
  void setChanged();

  List<? extends Action> getAdditionalActions();

  ViewCourbeManager getManager();
  
}
