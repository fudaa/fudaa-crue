package org.fudaa.fudaa.crue.loi.loiff;

import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiCourbeModel;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiUiController;
import org.fudaa.fudaa.crue.loi.common.DefaultLoiTableModel;

import javax.swing.table.AbstractTableModel;
import java.util.Set;

/**
 * @author Frederic Deniger
 */
public class LoiUiController extends AbstractLoiUiController {
    public LoiUiController() {
    }

    public LoiUiController(Set<String> actionToAvoid) {
        super(actionToAvoid);
    }

    @Override
    protected AbstractTableModel createTableModel() {
        return new DefaultLoiTableModel(tableGraphePanel);
    }

    @Override
    protected AbstractLoiCourbeModel createCourbeModel(AbstractLoi loi, ConfigLoi configLoi) {
        if (loi instanceof Loi) {
            return LoiCourbeModel.create((Loi) loi, configLoi);
        }
        throw new IllegalAccessError("loi of type " + loi.getClass() + " is not supported");
    }

    @Override
    public void setLoi(AbstractLoi loi, CrueConfigMetier ccm, boolean usePresentationFormat) {
        super.setLoi(loi, ccm, usePresentationFormat);
    }
}
