/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi;

import javax.swing.ImageIcon;
import org.openide.util.ImageUtilities;

/**
 * Uniquement pour avoir les messages dans le même package.
 * @author Frederic Deniger
 */
public class LoiMessages {
  
  public static ImageIcon getIcon(String iconName) {
    return ImageUtilities.loadImageIcon("org/fudaa/fudaa/crue/loi/icons/" + iconName, false);
  }
  
}
