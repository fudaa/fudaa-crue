/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.common;

import com.memoire.bu.BuGridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TreeSelectionEvent;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ebli.courbe.EGAxe;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheModelListener;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.courbe.EGObject;
import org.fudaa.ebli.courbe.EGSelectionListener;
import org.fudaa.fudaa.crue.loi.LoiMessages;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class CourbeMoyenneDeltaController implements EGGrapheModelListener, EGSelectionListener {

  final EGGraphe graphe;
  JPanel pnInfo;
  JTextField txtMoy;
  JTextField txtDeltaMax;
  JTextField txtDeltaMin;

  public CourbeMoyenneDeltaController(EGGraphe graphe) {
    this.graphe = graphe;
  }

  @Override
  public void structureChanged() {
  }

  public JPanel getPnInfo() {
    if (pnInfo == null) {
      graphe.addModelListener(this);
      graphe.getModel().addSelectionListener(this);
      pnInfo = new JPanel(new BuGridLayout(2, 5, 5));
      pnInfo.add(new JLabel(NbBundle.getMessage(LoiMessages.class, "LoiInfo.DeltaMax.Label")));
      txtDeltaMax = new JTextField();
      txtDeltaMax.setColumns(10);
      txtDeltaMax.setEditable(false);
      pnInfo.add(txtDeltaMax);

      pnInfo.add(new JLabel(NbBundle.getMessage(LoiMessages.class, "LoiInfo.DeltaMin.Label")));
      txtDeltaMin = new JTextField();
      txtDeltaMin.setColumns(10);
      txtDeltaMin.setEditable(false);
      pnInfo.add(txtDeltaMin);

      pnInfo.add(new JLabel(NbBundle.getMessage(LoiMessages.class, "LoiInfo.Moyenne.Label")));
      txtMoy = new JTextField();
      txtMoy.setColumns(10);
      txtMoy.setEditable(false);
      pnInfo.add(txtMoy);

      updateInfos();
    }
    return pnInfo;
  }

  @Override
  public void axeAspectChanged(EGAxe _c) {
  }

  @Override
  public void axeContentChanged(EGAxe _c) {
  }

  @Override
  public void courbeAspectChanged(EGObject _c, boolean _visibil) {
  }

  @Override
  public void courbeContentChanged(EGObject _c, boolean _mustRestore) {
    if (graphe.getSelectedComponent() == _c) {
      updateInfos();
    }
  }

  private void updateInfos() {
    if (pnInfo != null) {
      txtDeltaMax.setText(StringUtils.EMPTY);
      txtDeltaMin.setText(StringUtils.EMPTY);
      txtMoy.setText(StringUtils.EMPTY);
      EGCourbe selectedComponent = graphe.getSelectedComponent();
      if (selectedComponent != null && selectedComponent.getModel().getNbValues() > 0) {
        CtuluNumberFormatI specificFormat = selectedComponent.getAxeY().getSpecificFormat();
        final double moy = getMoyenne(selectedComponent.getModel());
        txtMoy.setText(toString(moy, specificFormat));
        txtDeltaMax.setText(toString(getDeltaMax(selectedComponent.getModel(), moy), specificFormat));
        txtDeltaMin.setText(toString(getDeltaMin(selectedComponent.getModel(), moy), specificFormat));
      }
      txtDeltaMax.setToolTipText(txtDeltaMax.getText());
      txtDeltaMin.setToolTipText(txtDeltaMin.getText());
      txtMoy.setToolTipText(txtMoy.getText());
    }
  }

  private static String toString(double d, CtuluNumberFormatI fmt) {
    if (fmt == null) {
      return Double.toHexString(d);
    }
    return fmt.format(d);
  }

  private static double getMoyenne(EGModel model) {
    double total = 0;
    int nb = model.getNbValues();
    for (int i = 0; i < nb; i++) {
      total = total + model.getY(i);
    }
    return total / ((double) nb);
  }

  private static double getDeltaMax(EGModel model, double moy) {
    int nb = model.getNbValues();
    double max = Math.abs(model.getY(0) - moy);
    for (int i = 1; i < nb; i++) {
      double currentDelta = Math.abs(model.getY(i) - moy);
      max = Math.max(max, currentDelta);
    }
    return max;
  }

  private static double getDeltaMin(EGModel model, double moy) {
    int nb = model.getNbValues();
    double min = Math.abs(model.getY(0) - moy);
    for (int i = 1; i < nb; i++) {
      double currentDelta = Math.abs(model.getY(i) - moy);
      min = Math.min(min, currentDelta);
    }
    return min;
  }

  @Override
  public void valueChanged(TreeSelectionEvent e) {
    updateInfos();
  }

  @Override
  public void valueChanged(ListSelectionEvent e) {
    updateInfos();
  }
}
