/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi;

import com.jidesoft.swing.JideBorderLayout;
import com.jidesoft.swing.Resizable;
import com.jidesoft.swing.ResizablePanel;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuVerticalLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.projet.report.loi.ViewCourbeConfig;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.courbe.EGLegendPanelManager;
import org.fudaa.fudaa.crue.loi.common.MouseAdapterDoubleCick;

/**
 *
 * @author Frederic Deniger
 */
public class ViewCourbeManager implements CtuluImageProducer {

  private static final String PROPERTY_POS_PANEL = "POS_PANEL";
  ViewCourbeConfig config = new ViewCourbeConfig();
  final EGFillePanel graphe;
  final EGLegendPanelManager legendManager;
  final CourbeViewLabelsManager loiLabelsManager = new CourbeViewLabelsManager(config);
  JPanel mainPanel;
  JPanel eastPanel;
  JPanel westPanel;
  JPanel northPanel;
  JPanel southPanel;
  final JLabel title = new JLabel();
  MouseAdapterDoubleCick mouseListener;

  private class CustomResizablePanel extends ResizablePanel {

    public CustomResizablePanel() {
    }

    public CustomResizablePanel(boolean isDoubleBuffered) {
      super(isDoubleBuffered);
    }

    public CustomResizablePanel(LayoutManager layout) {
      super(layout);
    }

    public CustomResizablePanel(LayoutManager layout, boolean isDoubleBuffered) {
      super(layout, isDoubleBuffered);
    }

    @Override
    protected Resizable createResizable() {
      return new CustomResizabel(this);
    }
  }

  private class CustomResizabel extends Resizable {

    public static final int MINIMUM_SIZE_GRAPHE = 100;

    public CustomResizabel(JComponent component) {
      super(component);
    }

    @Override
    public void resizing(int resizeCorner, int newX, int newY, int newW, int newH) {
      Dimension minimumSize = _component.getMinimumSize();
      Dimension maximumSize = _component.getMaximumSize();
      if (_component == eastPanel && newW != _component.getWidth()) {
        maximumSize.width = Math.min(maximumSize.width, mainPanel.getWidth() - MINIMUM_SIZE_GRAPHE - westPanel.getWidth());
      }
      if (_component == westPanel && newW != _component.getWidth()) {
        maximumSize.width = Math.min(maximumSize.width, mainPanel.getWidth() - MINIMUM_SIZE_GRAPHE - eastPanel.getWidth());
      }
      if (_component == northPanel && newH != _component.getHeight()) {
        maximumSize.height = mainPanel.getHeight() - MINIMUM_SIZE_GRAPHE - southPanel.getHeight();
      }
      if (_component == southPanel && newH != _component.getHeight()) {
        maximumSize.height = mainPanel.getHeight() - MINIMUM_SIZE_GRAPHE - northPanel.getHeight();
      }
      maximumSize.height = Math.max(minimumSize.height, maximumSize.height);
      maximumSize.width = Math.max(minimumSize.width, maximumSize.width);
      if (newW < minimumSize.width) {
        newW = minimumSize.width;
      }
      if (newH < minimumSize.height) {
        newW = minimumSize.height;
      }
      if (newW > maximumSize.width) {
        newW = maximumSize.width;
      }
      if (newH > maximumSize.height) {
        newH = maximumSize.height;
      }
      _component.setPreferredSize(new Dimension(newW, newH));
      _component.getParent().doLayout();
    }
  }

  public void setActionListenerTitle(final ActionListener listener) {
    if (mouseListener != null) {
      title.removeMouseListener(mouseListener);
    }
    mouseListener = listener == null ? null : new MouseAdapterDoubleCick(listener);
    if (mouseListener != null) {
      title.addMouseListener(mouseListener);
    }

  }

  public void resetGrapheSize() {
    graphe.setPreferredSize(graphe.getMinimumSize());
    mainPanel.doLayout();
    mainPanel.revalidate();
  }

  public ViewCourbeConfig getConfig() {
    return config;
  }

  public void setConfig(ViewCourbeConfig config) {
    this.config = config;
    loiLabelsManager.setConfig(config);
  }

  public ViewCourbeManager(EGFillePanel graphe) {
    this.graphe = graphe;
    legendManager = new EGLegendPanelManager();
    legendManager.setGraphe(graphe.getGraphe());
    legendManager.setGroupByLastSeparator(BusinessMessages.ENTITY_SEPARATOR);
  }

  public EGLegendPanelManager getLegendManager() {
    return legendManager;
  }

  public ViewCourbeConfig getLoiLegendLabelsConfig() {
    return config;
  }

  public CourbeViewLabelsManager getLoiLabelsManager() {
    return loiLabelsManager;
  }

  protected void createPanel() {
    mainPanel = new JPanel(new JideBorderLayout(1, 1));
    mainPanel.add(graphe);
    mainPanel.setBackground(Color.WHITE);

    mainPanel.setDoubleBuffered(false);
    createEastPanel();
    mainPanel.add(eastPanel, BorderLayout.EAST);
    createWestPanel();
    mainPanel.add(westPanel, BorderLayout.WEST);
    createNorthPanel();
    mainPanel.add(northPanel, BorderLayout.NORTH);
    createSouthPanel();
    mainPanel.add(southPanel, BorderLayout.SOUTH);
  }

  public JPanel getPanel() {
    if (mainPanel == null) {
      createPanel();
      applyConfig();
    }
    return mainPanel;
  }

  public void createNorthPanel() {
    northPanel = new CustomResizablePanel();
    northPanel.setLayout(new BuVerticalLayout(5, true, true));
    configurePanel(northPanel);
  }

  public void createSouthPanel() {
    southPanel = new CustomResizablePanel();
    southPanel.setLayout(new BuVerticalLayout(5, true, true));
    configurePanel(southPanel);
  }

  public void createEastPanel() {
    eastPanel = new CustomResizablePanel(new BorderLayout());
    configurePanel(eastPanel);
  }

  public void createWestPanel() {
    westPanel = new CustomResizablePanel(new BorderLayout());
    configurePanel(westPanel);
  }
  final Border emptyBorder = BorderFactory.createEmptyBorder(3, 3, 3, 3);
  final CompoundBorder selectedBorder = BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.LIGHT_GRAY),
          BorderFactory.createEmptyBorder(2, 2, 2, 2));

  public void configurePanel(final JPanel panel) {
    panel.setOpaque(false);
    panel.setDoubleBuffered(false);
    panel.setBorder(emptyBorder);
    panel.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseEntered(MouseEvent e) {
        panel.setBorder(selectedBorder);
      }

      @Override
      public void mouseExited(MouseEvent e) {
        panel.setBorder(emptyBorder);
      }
    });

  }

  protected void removeComponent(JComponent jc) {
    if (jc != null && jc.getParent() != null) {
      jc.getParent().remove(jc);
    }
  }

  public JLabel getLabelTitle() {
    return title;
  }

  protected JPanel getEastOrWestPanel(int position) {
    if (ViewCourbeConfig.isEast(position)) {
      return eastPanel;
    }
    return westPanel;
  }

  public void removeTitleLabels() {
    removeComponent(title);
    removeComponent(loiLabelsManager.gePanel());
  }

  public void applyConfig() {
    removeComponent(title);
    removeComponent(legendManager.getPanel());
    removeComponent(loiLabelsManager.gePanel());
    int labelPostionToUse = config.getLabelsPosition();
    int legendPostionToUse = config.getLegendPosition();
    if (config.isDisplayTitle()) {
      config.getTitleConfig().apply(title);
      addComponent(title, config.getTitlePosition());
    }

    if (config.isDisplayLegend()) {
      addComponent(legendManager.getPanel(), legendPostionToUse);
    }
    if (config.isDisplayLabels()) {
      addComponent(loiLabelsManager.gePanel(), labelPostionToUse);
    }
    southPanel.setPreferredSize(null);
    eastPanel.setPreferredSize(null);
    westPanel.setPreferredSize(null);
    northPanel.setPreferredSize(null);
    southPanel.doLayout();
    eastPanel.doLayout();
    westPanel.doLayout();
    northPanel.doLayout();
    northPanel.revalidate();
    southPanel.revalidate();
    eastPanel.revalidate();
    westPanel.revalidate();
    mainPanel.revalidate();
    mainPanel.doLayout();
    mainPanel.repaint(0);
  }

  protected void addComponent(JComponent jc, int position) {
    if (position == SwingConstants.SOUTH) {
      southPanel.add(jc);
    } else if (position == SwingConstants.NORTH) {
      northPanel.add(jc);
    } else {
      JPanel panel = getEastOrWestPanel(position);
      if (ViewCourbeConfig.isNorth(position)) {
        addComponentInBorderLayout(panel, BorderLayout.NORTH, jc);
      } else {
        addComponentInBorderLayout(panel, BorderLayout.SOUTH, jc);
      }
    }
  }

  @Override
  public BufferedImage produceImage(final int _w, final int _h, final Map _params) {
    return CtuluLibImage.produceImageForComponent(mainPanel, _w, _h, _params);
  }

  @Override
  public Dimension getDefaultImageDimension() {
    return mainPanel.getSize();
  }

  @Override
  public BufferedImage produceImage(final Map _params) {
    return produceImage(mainPanel.getWidth(), mainPanel.getHeight(), _params);

  }

  protected void addComponentInBorderLayout(final JPanel mainPanelToUse, final String layoutPosition, JComponent jc) {
    BorderLayout layout = (BorderLayout) mainPanelToUse.getLayout();
    JComponent layoutComponent = (JComponent) layout.getLayoutComponent(layoutPosition);
    if (layoutComponent == null) {
      mainPanelToUse.add(jc, layoutPosition);
    } else {
      Boolean posPanel = (Boolean) layoutComponent.getClientProperty(PROPERTY_POS_PANEL);
      if (posPanel == null) {
        JPanel pnPos = new JPanel(new BuGridLayout(1, 0, 5));
        pnPos.putClientProperty(PROPERTY_POS_PANEL, Boolean.TRUE);
        pnPos.add(layoutComponent);
        mainPanelToUse.add(pnPos, layoutPosition);
        layoutComponent = pnPos;
      }
      layoutComponent.add(jc);
    }
  }
}
