/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi;

import com.jidesoft.swing.JideSplitButton;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Cree un bouton qui contient les menus nécessaires pour configurer une CourbeView.
 *
 * @author Frederic Deniger
 */
public class CourbeViewConfigurationButtonUI<T extends CourbeViewConfigurationTarget> {
  protected final T target;
  private JCheckBoxMenuItem cbTitleVisible;
  private JCheckBoxMenuItem cbLabelsVisible;
  private JCheckBoxMenuItem cbLegendVisible;
  private JCheckBoxMenuItem cbTitleTop;
  private boolean titleAvailable = true;
  private boolean labelAvailable = true;

  public CourbeViewConfigurationButtonUI(final T topComponent) {
    this.target = topComponent;
  }

  public void setTitleAvailable(final boolean titleAvailable) {
    this.titleAvailable = titleAvailable;
  }

  public void setLabelAvailable(final boolean labelAvailable) {
    this.labelAvailable = labelAvailable;
  }

  private JideSplitButton res;

  private void updateItems() {
    if (cbTitleVisible != null) {
      cbTitleVisible.setSelected(target.getManager().getConfig().isDisplayTitle());
    }
    if (cbLabelsVisible != null) {
      cbLabelsVisible.setSelected(target.getManager().getConfig().isDisplayLabels());
    }
    if (cbLegendVisible != null) {
      cbLegendVisible.setSelected(target.getManager().getConfig().isDisplayLegend());
    }
    if (cbTitleTop != null) {
      cbTitleTop.setSelected(target.getManager().getConfig().isTitlePositionTop());
    }
  }

  public JideSplitButton getButton() {
    if (res == null) {
      createButton();
    }
    return res;
  }

  JideSplitButton createButton() {

    res = new JideSplitButton() {
      @Override
      public void doClickOnMenu() {
        super.doClickOnMenu();
        updateItems();
      }
    };
    res.setIcon(LoiMessages.getIcon("configure.png"));
    res.setAlwaysDropdown(true);
    createCbVisible();
    if (titleAvailable) {
      res.add(cbTitleVisible);
    }
    if (labelAvailable) {
      res.add(cbLabelsVisible);
    }
    res.add(cbLegendVisible);
    res.add(new JSeparator());
    cbTitleTop = new JCheckBoxMenuItem(NbBundle.getMessage(CourbeViewConfigurationButtonUI.class, "Configuration.TitleOnTop.Label"));
    cbTitleTop.addActionListener(e -> {
      target.getManager().getConfig().setTitlePositionTop(cbTitleTop.isSelected());
      target.getManager().applyConfig();
      target.setChanged();
    });
    if (titleAvailable) {
      res.add(cbTitleTop);
    }
    final JMenu mnLegend = new JMenu(NbBundle.getMessage(CourbeViewConfigurationButtonUI.class, "Configuration.LegendPosition.Menu"));
    createPositionItems(mnLegend, e -> {
      final int pos = Integer.parseInt(e.getActionCommand());
      target.getManager().getConfig().setLegendPosition(pos);
      target.getManager().applyConfig();
      target.setChanged();
    });
    res.add(mnLegend);
    if (labelAvailable) {
      final JMenu mnLabels = new JMenu(NbBundle.getMessage(CourbeViewConfigurationButtonUI.class, "Configuration.LabelsPosition.Menu"));
      createPositionItems(mnLabels, e -> {
        final int pos = Integer.parseInt(e.getActionCommand());
        target.getManager().getConfig().setLabelsPosition(pos);
        target.getManager().applyConfig();
        target.setChanged();
      });
      res.add(mnLabels);
    }
    res.add(new JSeparator());
    if (titleAvailable) {
      res.add(NbBundle.getMessage(CourbeViewConfigurationButtonUI.class, "ConfigureTitle.Action")).addActionListener(e -> configureTitle());
    }
    if (labelAvailable) {
      res.add(NbBundle.getMessage(CourbeViewConfigurationButtonUI.class, "ConfigureLabels.Action")).addActionListener(e -> configureLabels());
    }
    final List<? extends Action> additionalActions = target.getAdditionalActions();
    if (additionalActions != null) {
      for (final Action action : additionalActions) {
        res.add(action);
      }
    }
    updateItems();

    return res;
  }

  private void createPositionItems(final JMenu dest, final ActionListener listener) {
    createItem(dest, "Configuration.Top.Position", SwingConstants.NORTH, listener);
    createItem(dest, "Configuration.Bottom.Position", SwingConstants.SOUTH, listener);
    dest.addSeparator();
    createItem(dest, "Configuration.TopWest.Position", SwingConstants.NORTH_WEST, listener);
    createItem(dest, "Configuration.TopEast.Position", SwingConstants.NORTH_EAST, listener);
    createItem(dest, "Configuration.BottomWest.Position", SwingConstants.SOUTH_WEST, listener);
    createItem(dest, "Configuration.BottomEast.Position", SwingConstants.SOUTH_EAST, listener);
  }

  public void configureTitle() {
  }

  public void configureLabels() {
  }

  private void createCbVisible() {
    cbTitleVisible = new JCheckBoxMenuItem(NbBundle.getMessage(CourbeViewConfigurationButtonUI.class, "Configuration.TitleVisible.Label"));
    cbTitleVisible.addActionListener(e -> {
      target.getManager().getConfig().setDisplayTitle(cbTitleVisible.isSelected());
      target.getManager().applyConfig();
    });
    cbLabelsVisible = new JCheckBoxMenuItem(NbBundle.getMessage(CourbeViewConfigurationButtonUI.class, "Configuration.LabelsVisible.Label"));
    cbLabelsVisible.addActionListener(e -> {
      target.getManager().getConfig().setDisplayLabels(cbLabelsVisible.isSelected());
      target.getManager().applyConfig();
    });
    cbLegendVisible = new JCheckBoxMenuItem(NbBundle.getMessage(CourbeViewConfigurationButtonUI.class, "Configuration.LegendVisible.Label"));
    cbLegendVisible.addActionListener(e -> {
      target.getManager().getConfig().setDisplayLegend(cbLegendVisible.isSelected());
      target.getManager().applyConfig();
    });
  }

  private void createItem(final JMenu dest, final String i18n, final int idx, final ActionListener listener) {
    final JMenuItem itemNW = dest.add(NbBundle.getMessage(CourbeViewConfigurationButtonUI.class, i18n));
    itemNW.setActionCommand(Integer.toString(idx));
    itemNW.addActionListener(listener);
  }

  public void installDoubleClickListener(final ViewCourbeManager loiLegendManager) {
    if (titleAvailable) {
      loiLegendManager.setActionListenerTitle(e -> configureTitle());
    }
    if (labelAvailable) {
      loiLegendManager.getLoiLabelsManager().setDoubleClickedListener(e -> configureLabels());
    }
  }
}
