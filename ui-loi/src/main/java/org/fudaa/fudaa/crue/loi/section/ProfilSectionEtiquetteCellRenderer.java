/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizableTransformer;

/**
 * CellRenderer pour les étiquettes afin d'afficher toutes les étiquettes associées à une ligne
 * @author Frederic Deniger
 */
public class ProfilSectionEtiquetteCellRenderer extends CtuluCellTextRenderer {

  @Override
  protected void setValue(Object value) {
    if (value != null) {
      String toString = etiquetteToString(value);
      super.setValue(toString);
      super.setToolTipText(toString);
    } else {
      super.setValue(value);
    }
  }

  protected static String etiquetteToString(Object value) {
    ProfilSectionEtiquetteContainer etiquette = (ProfilSectionEtiquetteContainer) value;
    final String toString = ToStringInternationalizableTransformer.toi18nString(etiquette.getEtiquettes(), ";");
    return toString;
  }
}
