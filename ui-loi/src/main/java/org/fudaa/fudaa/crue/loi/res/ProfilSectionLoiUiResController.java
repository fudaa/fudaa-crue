/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.res;

import org.fudaa.fudaa.crue.common.config.ConfigDefaultValuesProvider;
import org.fudaa.fudaa.crue.loi.section.ProfilSectionLoiUiController;

/**
 *
 * @author Frederic Deniger
 */
public class ProfilSectionLoiUiResController extends ProfilSectionLoiUiController {

  public ProfilSectionLoiUiResController(ConfigDefaultValuesProvider defaultValuesProvider) {
    super(defaultValuesProvider);
    tableGraphePanel.setAddPasteButtons(false);
  }

  @Override
  protected void configureTablePanel() {
    super.configureTablePanel();
    tableGraphePanel.getDefaultRenderer().setDecorator(new LoiStateResCellRenderer());
  }
}
