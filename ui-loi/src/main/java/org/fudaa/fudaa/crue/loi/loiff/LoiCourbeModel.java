package org.fudaa.fudaa.crue.loi.loiff;

import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.fudaa.crue.loi.common.*;

/**
 *
 * @author deniger
 */
public class LoiCourbeModel extends AbstractLoiCourbeModel {

  public static LoiCourbeModel create(Loi loi, ConfigLoi configLoi) {
    LoiCourbeModel res = new LoiCourbeModel(configLoi);
    res.setPts(loi.getEvolutionFF().getPtEvolutionFF());
    return res;
  }

  private LoiCourbeModel(ConfigLoi configLoi) {
    super(configLoi);
  }

  @Override
  protected LoiLine createLoiLineInstance() {
    return new LoiLine();
  }


  @Override
  public boolean useSpecificIcon(int idx) {
    return false;
  }
}
