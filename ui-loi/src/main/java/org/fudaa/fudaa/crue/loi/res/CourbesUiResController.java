/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.res;

import org.fudaa.fudaa.crue.loi.common.CourbesUiController;

/**
 *
 * @author Frederic Deniger
 */
public class CourbesUiResController extends CourbesUiController {

  public CourbesUiResController() {
    tableGraphePanel.setAddPasteButtons(false);
  }

  @Override
  protected void configureTablePanel() {
    super.configureTablePanel();
    tableGraphePanel.getDefaultRenderer().setDecorator(new LoiStateResCellRenderer());
   
  }

  @Override
  public void removeEditActions() {
    tableGraphePanel.removeEditButtons();
    removeActions("MOVE_POINT", "SIMPLIFY");
  }
}
