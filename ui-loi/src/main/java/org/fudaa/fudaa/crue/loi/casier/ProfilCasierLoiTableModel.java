/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.casier;

import org.fudaa.ebli.courbe.EGTableGraphePanel;
import org.fudaa.fudaa.crue.loi.LoiMessages;
import org.fudaa.fudaa.crue.loi.common.DefaultLoiTableModel;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ProfilCasierLoiTableModel extends DefaultLoiTableModel {

  public static final int COLUMN_LIT_UTILE = 3;

  public ProfilCasierLoiTableModel(EGTableGraphePanel graphePanel) {
    super(graphePanel);
  }

  @Override
  public int getColumnCount() {
    return 4;//la 4eme colonne afiche le caractère limite de lit.
  }

  @Override
  public Class getColumnClass(int _columnIndex) {
    if (_columnIndex == COLUMN_LIT_UTILE) {
      return ProfilCasierLine.Limite.class;
    }
    return super.getColumnClass(_columnIndex);
  }

  @Override
  public String getColumnName(int _column) {
    if (_column == COLUMN_LIT_UTILE) {
      return NbBundle.getMessage(LoiMessages.class, "LitUtile.Label");
    }
    return super.getColumnName(_column);
  }

  public ProfilCasierLine findLine(ProfilCasierLine.Limite limite) {
    ProfilCasierCourbeModel modele = (ProfilCasierCourbeModel) getCourbe().getModel();
    return modele.findLine(limite);
  }

  public int findLineIdx(ProfilCasierLine.Limite limite) {
    if (getCourbe() == null) {
      return -1;
    }
    ProfilCasierCourbeModel modele = (ProfilCasierCourbeModel) getCourbe().getModel();
    if (modele == null) {
      return -1;
    }
    return modele.findLineIdx(limite);
  }

  @Override
  public void setValueAt(Object _value, int _rowIndex, int _columnIndex) {
    if (_columnIndex == COLUMN_LIT_UTILE && _rowIndex < getCourbe().getModel().getNbValues()) {
      ProfilCasierLine.Limite limite = (ProfilCasierLine.Limite) _value;
      ProfilCasierLine line = getLine(_rowIndex);
      if (limite != null && limite != ProfilCasierLine.Limite.NONE) {
        int idx = findLineIdx(limite);
        if (idx >= 0) {
          getLine(idx).setLimite(null);
          fireTableRowsUpdated(idx, idx);
        }
      }
      line.setLimite(limite == ProfilCasierLine.Limite.NONE ? null : limite);
      fireTableRowsUpdated(_rowIndex, _rowIndex);
      fireChange();
    } else {
      super.setValueAt(_value, _rowIndex, _columnIndex);
    }
  }

  @Override
  public boolean isCellEditable(int _rowIndex, int _columnIndex) {
    if (_columnIndex == COLUMN_LIT_UTILE) {
      return getCourbe().getModel().isModifiable();
    }
    return super.isCellEditable(_rowIndex, _columnIndex);
  }

  @Override
  public Object getValueAt(int _rowIndex, int _columnIndex) {
    if (_columnIndex == COLUMN_LIT_UTILE) {
      if (_rowIndex < getCourbe().getModel().getNbValues()) {
        ProfilCasierLine line = getLine(_rowIndex);
        return line.getLimite();
      }
      return null;
    }
    return super.getValueAt(_rowIndex, _columnIndex);
  }

  protected void fireChange() {
    ProfilCasierCourbeModel modele = (ProfilCasierCourbeModel) getCourbe().getModel();
    modele.fireChanged();
  }

  private ProfilCasierLine getLine(int _rowIndex) {
    ProfilCasierCourbeModel modele = (ProfilCasierCourbeModel) getCourbe().getModel();
    return modele.getLoiLine(_rowIndex);
  }
}
