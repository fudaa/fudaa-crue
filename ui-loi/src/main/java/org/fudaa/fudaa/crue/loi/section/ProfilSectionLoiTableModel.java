/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import gnu.trove.TIntObjectHashMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluDoubleParser;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.config.lit.LitNommeLimite;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.ebli.courbe.EGTableGraphePanel;
import org.fudaa.fudaa.crue.loi.LoiMessages;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiUiController;
import org.fudaa.fudaa.crue.loi.common.DefaultLoiTableModel;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ProfilSectionLoiTableModel extends DefaultLoiTableModel {

  public static final int COLUMN_ETIQUETTE = 3;
  public static final int COLUMN_FRT = 4;
  protected final String donFrtLimiteLitAsString;
  protected final String donFrtLimiteLitNommeAsString;
  private final ProfilSectionLoiUiController profilSectionLoiUiController;

  public ProfilSectionLoiTableModel(EGTableGraphePanel graphePanel, ProfilSectionLoiUiController profilSectionLoiUiController) {
    super(graphePanel);
    this.profilSectionLoiUiController = profilSectionLoiUiController;
    donFrtLimiteLitAsString = org.openide.util.NbBundle.getMessage(LoiMessages.class, "ProfilSection.DonFrt.LimiteLit");
    donFrtLimiteLitNommeAsString = org.openide.util.NbBundle.getMessage(LoiMessages.class, "ProfilSection.DonFrt.LimiteLitNomme");
  }

  @Override
  public int getColumnCount() {
    return 5;
  }

  @Override
  public void fireTableRowsInserted(int firstRow, int lastRow) {
    super.fireTableRowsInserted(firstRow, lastRow);
    for (int i = 1; i < getRowCount() - 1; i++) {
      ProfilSectionLine loiLine = getLine(i);
      if (loiLine.getFrt() == null) {
        loiLine.setFrt(getLine(i - 1).getFrt());
        fireTableRowsUpdated(firstRow, firstRow);
      }
    }
  }

  @Override
  public Class getColumnClass(int _columnIndex) {
    if (_columnIndex == COLUMN_ETIQUETTE) {
      return List.class;
    }
    if (_columnIndex == COLUMN_FRT) {
      return DonFrt.class;
    }
    return super.getColumnClass(_columnIndex);
  }

  @Override
  protected boolean setValueInModel(final Object o, final CtuluDoubleParser _doubleParser, final int iCellule, final int jCellule,
          final CtuluCommandComposite _cmp) {
    if (jCellule == COLUMN_ETIQUETTE) {
      String etiquettesValue = o == null ? null : o.toString();
      String[] etiquettes = StringUtils.split(etiquettesValue, ";");
      final List<ProfilSectionEtiquette> allEtiquettes = ProfilSectionEtiquette.getEtiquettes(profilSectionLoiUiController.getCcm());
      List<ProfilSectionEtiquette> newEtiquettes = new ArrayList<>();
      final Map<String, ProfilSectionEtiquette> toMapOfString = CtuluLibArray.toMapOfString(allEtiquettes);
      if (etiquettes != null) {
        for (String etiquette : etiquettes) {
          final ProfilSectionEtiquette found = toMapOfString.get(etiquette);
          if (found != null) {
            newEtiquettes.add(found);
          }
        }
      }
      setEtiquettes(newEtiquettes, iCellule);

    } else if (jCellule == COLUMN_FRT) {
      String frt = o == null ? null : o.toString();
      DonFrt toSet = profilSectionLoiUiController.getDonFrtByName(frt);
      setValueAt(toSet, iCellule, jCellule);
    } else {
      return super.setValueInModel(o, _doubleParser, iCellule, jCellule, _cmp);
    }
    return true;
  }

  @Override
  public String
          getColumnName(int _column) {
    if (_column == COLUMN_ETIQUETTE) {
      return NbBundle.getMessage(LoiMessages.class, "Etiquette.Label");
    }

    if (_column == COLUMN_FRT) {
      return NbBundle.getMessage(LoiMessages.class, "DonFrt.Label");
    }
    //dans le cas présent, le titre de l'axe vertical est différent du titre de la colonne du tableau.
    //dans l'axe on utilise la nature et dans le tableau la variable
    if (_column == yColIndex) {
      String name = AbstractLoiUiController.PROP_Z_XT;
      //pour ajouter l'unité si présente
      if (getCourbe() != null && getCourbe().getAxeY() != null && StringUtils.isNotBlank(getCourbe().getAxeY().getUnite())) {
        name = name + PropertyNature.getUniteSuffixe(getCourbe().getAxeY().getUnite());
      }
      return name;
    }
    return super.getColumnName(_column);
  }

  @Override
  public void setValueAt(Object _value, int _rowIndex, int _columnIndex) {
    if (_columnIndex == COLUMN_ETIQUETTE) {

//     ne rien faire ici.setEtiquettes appelé explicitement par le CellEditor.
    } else if (_columnIndex == COLUMN_FRT) {
      if (_rowIndex < getCourbe().getModel().getNbValues()) {
        if (_value instanceof DonFrt) {
          ProfilSectionLine line = getLine(_rowIndex);
          line.setFrt((DonFrt) _value);
          fireTableRowsUpdated(_rowIndex, _rowIndex);
          getCourbeModel().fireChanged();
        }
      }
    } else {
      super.setValueAt(_value, _rowIndex, _columnIndex);
    }
    updateFrt();
  }

  private boolean isSame(List<ProfilSectionEtiquette> old, List newEtiquettes) {
    if (old == newEtiquettes) {
      return true;
    }
    if (old == null || newEtiquettes == null) {
      return false;
    }
    if (old.size() != newEtiquettes.size()) {
      return false;
    }

    int nb = old.size();
    for (int i = 0; i < nb; i++) {
      ProfilSectionEtiquette oldEtiquette = old.get(i);
      ProfilSectionEtiquette newEtique = (ProfilSectionEtiquette) newEtiquettes.get(i);
      if (oldEtiquette.getLabel().equals(newEtique.getLabel())) {
        return false;
      }
    }
    return true;
  }

  protected void setEtiquettes(List initEtiquettes, int row) {
    ProfilSectionLine line = getLine(row);
    List<ProfilSectionEtiquette> oldEtiquettes = line.getEtiquettes();
    if (isSame(oldEtiquettes, initEtiquettes)) {
      return;
    }
    List etiquettes = getEtiquettesNormalized(initEtiquettes, row);
    line.setEtiquettes(etiquettes);
    if (line.containsLimiteLit() && line.containsLimiteLitNomme()) {
      line.clean();
    }

    List<ProfilSectionEtiquette> etiquettesWithUniqueReference = new ArrayList<>();
    for (Object object : etiquettes) {
      ProfilSectionEtiquette newEtiquette = (ProfilSectionEtiquette) object;
      if (!newEtiquette.isLimitLit()) {
        etiquettesWithUniqueReference.add(newEtiquette);
      }
    }
    cleanOtherLines(etiquettesWithUniqueReference, row);
    fireTableRowsUpdated(row, row);
    fireChange();
    updateFrt();
  }

  @Override
  public boolean isCellEditable(int _rowIndex, int _columnIndex) {
    final boolean notLastEditorRow = isNormalRow(_rowIndex);
    if (_columnIndex == COLUMN_ETIQUETTE) {
      return notLastEditorRow;
    }
    if (_columnIndex == COLUMN_FRT) {
      if (!getCourbeModel().isModifiable()) {
        return false;
      }
      if (notLastEditorRow) {
        ProfilSectionLine line = getLine(_rowIndex);
        return (line.containsLimiteLitNomme() || line.containsLimiteLit()) && !getCourbeModel().isAfterLastLitNommeLine(_rowIndex + 1);
      }
      return false;
    }
    return super.isCellEditable(_rowIndex, _columnIndex);
  }

  protected void updateFrt() {
    DonFrt current = null;
    ProfiLSectionCourbeModel courbeModel = getCourbeModel();
    int lastIdx = courbeModel.getLastLitNommeIdx();
    for (int i = 0; i <= lastIdx; i++) {
      ProfilSectionLine line = getLine(i);
      if (line.containsLimiteLit() || line.containsLimiteLitNomme()) {
        current = line.getFrt();
      } else if (current != null) {
        if (current != line.getFrt()) {
          line.setFrt(current);
          fireTableRowsUpdated(i, i);
          courbeModel.fireChanged();
        }
      }
    }
  }

  /**
   * le tableModel ajoute une ligne en mode edition pour l'ajout de point.
   *
   * @param row
   * @return true si ce n'est pas la ligne d'ajout de points
   */
  protected boolean isNormalRow(int row) {
    return row < getCourbe().getModel().getNbValues();
  }

  public ProfilSectionEtiquetteContainer getEtiquettesAt(int row) {
    if (isNormalRow(row)) {
      ProfilSectionLine line = getLine(row);
      return new ProfilSectionEtiquetteContainer(line.getEtiquettes());
    }
    return null;
  }

  @Override
  public Object getValueAt(int _rowIndex, int _columnIndex) {
    if (_columnIndex == COLUMN_ETIQUETTE) {
      return getEtiquettesAt(_rowIndex);
    }
    if (_columnIndex == COLUMN_FRT) {
      if (isNormalRow(_rowIndex)) {
        ProfilSectionLine line = getLine(_rowIndex);
        return line.getFrt();
      }
      return null;
    }
    return super.getValueAt(_rowIndex, _columnIndex);
  }

  protected void fireChange() {
    ProfiLSectionCourbeModel modele = getCourbeModel();
    modele.fireChanged();
  }

  ProfilSectionLine getLine(int _rowIndex) {
    ProfiLSectionCourbeModel modele = getCourbeModel();
    return modele.getLoiLine(_rowIndex);
  }

  protected ProfiLSectionCourbeModel getCourbeModel() {
    return (ProfiLSectionCourbeModel) getCourbe().getModel();
  }

  public void cleanOtherLines(List<ProfilSectionEtiquette> toRemove, int rowToAvoid) {
    if (!toRemove.isEmpty()) {
      for (int i = 0; i < getCourbe().getModel().getNbValues(); i++) {
        if (i != rowToAvoid) {
          ProfilSectionLine otherLine = getLine(i);
          boolean removeAll = otherLine.removeEtiquettes(toRemove);
          if (removeAll) {
            fireTableRowsUpdated(i, i);
          }
        }
      }
    }
  }
  Map<LitNommeLimite, ProfilSectionEtiquette> etiquettesByLimites;

  void setEtiquetteByLimite(Map<LitNommeLimite, ProfilSectionEtiquette> etiquettesByLimites) {
    this.etiquettesByLimites = etiquettesByLimites;
  }

  public Map<ProfilSectionEtiquette, ProfilSectionLine> getLimiteByLine() {
    Map<ProfilSectionEtiquette, ProfilSectionLine> etiquetteByLine = new HashMap<>();
    for (int i = 0; i < getCourbe().getModel().getNbValues(); i++) {
      ProfilSectionLine line = getLine(i);
      List<ProfilSectionEtiquette> etiquettes = line.getEtiquettes();
      if (etiquettes != null) {
        for (ProfilSectionEtiquette profilSectionEtiquette : etiquettes) {
          if (profilSectionEtiquette.isLimiteLitNomme()) {
            etiquetteByLine.put(profilSectionEtiquette, line);
          }
        }
      }
    }
    return etiquetteByLine;
  }

  public List getEtiquettesNormalized(List initEtiquettes, int row) {
    TIntObjectHashMap<LitNommeLimite> limiteByIdx = new TIntObjectHashMap<>();
    Set<LitNommeLimite> keySet = etiquettesByLimites.keySet();
    for (LitNommeLimite litNommeLimite : keySet) {
      limiteByIdx.put(litNommeLimite.getIdx(), litNommeLimite);
    }
    List etiquettes = new ArrayList(initEtiquettes);
    int currentIdx = -1;
    for (Object object : initEtiquettes) {
      ProfilSectionEtiquette newEtiquette = (ProfilSectionEtiquette) object;
      if (newEtiquette.isLimiteLitNomme()) {
        if (currentIdx >= 0 && newEtiquette.getLitNommeLimite().getIdx() != currentIdx + 1) {//non consecutif
          for (int j = currentIdx + 1; j < newEtiquette.getLitNommeLimite().getIdx(); j++) {
            etiquettes.add(etiquettesByLimites.get(limiteByIdx.get(j)));
          }
        }
        currentIdx = newEtiquette.getLitNommeLimite().getIdx();
      }
    }
    int maxIdx = -1;
    int minIdx = -1;
    for (Object object : etiquettes) {
      ProfilSectionEtiquette newEtiquette = (ProfilSectionEtiquette) object;
      if (newEtiquette.isLimiteLitNomme()) {
        final int limiteIdx = newEtiquette.getLitNommeLimite().getIdx();
        if (maxIdx < 0) {
          maxIdx = limiteIdx;
          minIdx = limiteIdx;
        } else {
          maxIdx = Math.max(maxIdx, limiteIdx);
          minIdx = Math.min(minIdx, limiteIdx);
        }
      }
    }
    if (maxIdx >= 0) {
      CtuluListSelection rowToUpdate = new CtuluListSelection();
      for (int i = row + 1; i < getCourbe().getModel().getNbValues(); i++) {
        ProfilSectionLine lineSup = getLine(i);
        List<ProfilSectionEtiquette> toRemove = new ArrayList<>();
        for (ProfilSectionEtiquette etiquette : lineSup.getEtiquettes()) {
          if (etiquette.isLimiteLitNomme() && etiquette.getLitNommeLimite().getIdx() < maxIdx) {
            toRemove.add(etiquette);
            rowToUpdate.add(i);
            etiquettes.add(etiquette);
          }
        }
        if (!toRemove.isEmpty()) {
          lineSup.removeEtiquettes(toRemove);
        }
      }
      for (int i = 0; i < row; i++) {
        ProfilSectionLine lineInf = getLine(i);
        List<ProfilSectionEtiquette> toRemove = new ArrayList<>();
        for (ProfilSectionEtiquette etiquette : lineInf.getEtiquettes()) {
          if (etiquette.isLimiteLitNomme() && etiquette.getLitNommeLimite().getIdx() > minIdx) {
            toRemove.add(etiquette);
            rowToUpdate.add(i);
            etiquettes.add(etiquette);
          }
        }
        if (!toRemove.isEmpty()) {
          lineInf.removeEtiquettes(toRemove);
        }
      }
      if (!rowToUpdate.isEmpty()) {
        for (int i = rowToUpdate.getMinIndex(); i <= rowToUpdate.getMaxIndex(); i++) {
          if (rowToUpdate.isSelected(i)) {
            fireTableRowsUpdated(i, i);
          }
        }
      }
    }
    Collections.sort(etiquettes);
    return etiquettes;
  }
}
