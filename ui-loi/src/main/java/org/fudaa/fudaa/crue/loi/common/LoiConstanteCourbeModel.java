/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.common;

import com.memoire.fu.FuEmptyArrays;
import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.crue.config.ccm.ItemContentAbstract;
import org.fudaa.dodico.crue.io.extern.ExternContent;
import org.fudaa.dodico.crue.io.extern.ExternContentColumn;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.courbe.EgModelLabelNamed;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

import java.awt.*;
import java.util.Map;

/**
 * @author Frederic Deniger
 */
public class LoiConstanteCourbeModel extends DefaultTestedContent implements CourbeModelWithKey, EgModelLabelNamed {
  private String labelColumnName;

  public static LoiConstanteCourbeModel create(Object key, double xmin, double xmax, double y, ItemContentAbstract varX, ItemContentAbstract varY) {
    LoiConstanteCourbeModel res = new LoiConstanteCourbeModel(new double[]{xmin, xmax}, new double[]{y, y}, varX, varY);
    res.setKey(key);
    return res;
  }

  public static LoiConstanteCourbeModel create(Object key, double x1, double x2, double y1, double y2, ItemContentAbstract varX,
                                               ItemContentAbstract varY) {
    LoiConstanteCourbeModel res = new LoiConstanteCourbeModel(new double[]{x1, x2}, new double[]{y1, y2}, varX, varY);
    res.setKey(key);
    return res;
  }

  public static LoiConstanteCourbeModel create(Object key, TDoubleArrayList x, TDoubleArrayList y, ItemContentAbstract varX, ItemContentAbstract varY) {
    LoiConstanteCourbeModel res = new LoiConstanteCourbeModel(x.toNativeArray(), y.toNativeArray(), varX, varY);
    res.setKey(key);
    return res;
  }

  public static LoiConstanteCourbeModel create(Object key, ExternContentColumn x, ExternContentColumn y, ExternContent content,
                                               ItemContentAbstract varX, ItemContentAbstract varY) {
    LoiConstanteCourbeModel res = new LoiConstanteCourbeModel(x.toNativeArray(), y.toNativeArray(), varX, varY);
    res.setLabels(content.getLabelsMap());
    res.setKey(key);
    return res;
  }

  private final double[] xPoints;
  private final double[] yPoints;
  private double xmin;
  private double xmax;
  private double ymin;
  private double ymax;
  private Object key;
  private CtuluListSelection drawnSegment;
  private TIntObjectHashMap<String> labels;
  private Map<String, TIntObjectHashMap<String>> subLabels;
  private final ItemContentAbstract varX;
  private final ItemContentAbstract varY;
  private String title;

  @Override
  public String getLabelColumnName() {
    return labelColumnName;
  }

  public void setLabelColumnName(String labelColumnName) {
    this.labelColumnName = labelColumnName;
  }

  public LoiConstanteCourbeModel(double[] x, double[] y, ItemContentAbstract varX, ItemContentAbstract varY) {
    xPoints = x == null ? FuEmptyArrays.DOUBLE0 : x;
    yPoints = y == null ? FuEmptyArrays.DOUBLE0 : y;
    this.varX = varX;
    this.varY = varY;
    assert xPoints.length == yPoints.length;
    if (xPoints.length > 0) {
      xmin = xPoints[0];
      xmax = xmin;
      ymin = yPoints[0];
      ymax = ymin;
      for (int i = 1; i < xPoints.length; i++) {
        xmin = Math.min(xmin, xPoints[i]);
        xmax = Math.max(xmax, xPoints[i]);
        ymin = Math.min(ymin, yPoints[i]);
        ymax = Math.max(ymax, yPoints[i]);
      }
    }
  }

  public CtuluListSelection getDrawnSegment() {
    return drawnSegment;
  }

  @Override
  public boolean useSpecificIcon(int idx) {
    return false;
  }

  @Override
  public Color getSpecificColor(int idx) {
    return null;
  }

  public void setDrawnSegment(CtuluListSelection drawnSegment) {
    this.drawnSegment = drawnSegment;
  }

  public void setLabels(TIntObjectHashMap<String> labels) {
    this.labels = labels;
  }

  public void setSubLabels(Map<String, TIntObjectHashMap<String>> subLabels) {
    this.subLabels = subLabels;
  }

  private static final TIntObjectHashMap<String> EMPTY_LABEL = new TIntObjectHashMap<>();

  /**
   * @param key pour les sous label
   * @return les sous labels. hashmap vide si non trouvé ( non null).
   */
  public TIntObjectHashMap<String> getSubLabels(String key) {
    TIntObjectHashMap<String> res = null;
    if (subLabels != null) {
      res = subLabels.get(key);
    }
    if (res == null) {
      res = EMPTY_LABEL;
    }
    return res;
  }

  @Override
  public Object getKey() {
    return key;
  }

  public void setKey(Object key) {
    this.key = key;
  }

  @Override
  public ItemContentAbstract getVariableAxeY() {
    return varY;
  }

  @Override
  public ItemContentAbstract getVariableAxeX() {
    return varX;
  }

  @Override
  public boolean isRemovable() {
    return true;
  }

  @Override
  public boolean isDuplicatable() {
    return false;
  }

  @Override
  public int getNbValues() {
    return xPoints.length;
  }

  @Override
  public boolean isSegmentDrawn(int idx) {
    return drawnSegment == null ? true : drawnSegment.isSelected(idx);
  }

  @Override
  public boolean isPointDrawn(int idx) {
    return true;
  }

  @Override
  public String getPointLabel(int idx) {
    if (labels != null) {
      return labels.get(idx);
    }
    return null;
  }

  @Override
  public double getX(int idx) {
    return xPoints[idx];
  }

  @Override
  public double getY(int idx) {
    return yPoints[idx];
  }

  @Override
  public double getXMin() {
    return xmin;
  }

  @Override
  public double getXMax() {
    return xmax;
  }

  @Override
  public double getYMin() {
    return ymin;
  }

  @Override
  public double getYMax() {
    return ymax;
  }

  @Override
  public boolean isModifiable() {
    return false;
  }

  @Override
  public boolean isXModifiable() {
    return false;
  }

  @Override
  public boolean setValue(int i, double x, double y, CtuluCommandContainer ctuluCommandContainer) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean addValue(double x, double y, CtuluCommandContainer ctuluCommandContainer) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean addValue(double[] x, double[] y, CtuluCommandContainer commandContainer) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean removeValue(int i, CtuluCommandContainer commandContainer) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean removeValue(int[] i, CtuluCommandContainer commandContainer) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean deplace(int[] selectIdx, double deltaX, double deltaY, CtuluCommandContainer ctuluCommandContainer) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean setValues(int[] idx, double[] x, double[] y, CtuluCommandContainer commandContainer) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  @Override
  public boolean setTitle(String newName) {
    this.title = newName;
    return true;
  }

  @Override
  public void fillWithInfo(InfoData _table, CtuluListSelectionInterface _selectedPt) {
    //nothing to do
  }

  @Override
  public EGModel duplicate() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void viewGenerationSource(Map infos, CtuluUI impl) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean isGenerationSourceVisible() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void replayData(EGGrapheModel model, Map infos, CtuluUI impl) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean isReplayable() {
    return false;
  }

  @Override
  public Object savePersistSpecificDatas() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void restoreFromSpecificDatas(Object data, Map infos) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public int[] getInitRows() {
    return null;
  }
}
