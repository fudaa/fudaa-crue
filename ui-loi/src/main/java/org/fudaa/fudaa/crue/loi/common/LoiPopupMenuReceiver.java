package org.fudaa.fudaa.crue.loi.common;

import com.memoire.bu.BuMenuItem;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.ExportTableCommentSupplier;
import org.fudaa.ctulu.gui.PopupMenuReceiver;
import org.fudaa.ebli.courbe.EGFillePanel;

import javax.swing.*;

/**
 * @author Frederic Deniger
 */
public class LoiPopupMenuReceiver extends PopupMenuReceiver {
    boolean editEnable;
    private final EGFillePanel panel;
    private BuMenuItem refinedMenuItem;
    private BuMenuItem editMenuItem;
    private BuMenuItem alignPointMenuItem;
    private BuMenuItem removeMenuItem;

    public LoiPopupMenuReceiver(EGFillePanel panel) {
        this.panel = panel;
    }

    public BuMenuItem getAlignPointMenuItem() {
        return alignPointMenuItem;
    }

    public BuMenuItem getRefinedMenuItem() {
        return refinedMenuItem;
    }

    public EGFillePanel getPanel() {
        return panel;
    }

    @Override
    public void install(JComponent component, CtuluUI ui) {
        super.install(component, ui);
        build();
    }

    public boolean isEditEnable() {
        return editEnable;
    }

    public void setEditEnable(boolean editEnable) {
        this.editEnable = editEnable;
    }

    @Override
    protected void addItems() {
        if (editEnable) {
            editMenuItem = panel.addModifiyPointsMenuItem(popupMenu);
            editMenuItem.addActionListener(panel);
        }
        refinedMenuItem = panel.addRefinePointMenuItem(popupMenu);

        alignPointMenuItem = panel.addAlignPointMenuItem(popupMenu);
        removeMenuItem = panel.addRemovePointMenuItem(popupMenu);

        refinedMenuItem.addActionListener(panel);
        alignPointMenuItem.addActionListener(panel);
        removeMenuItem.addActionListener(panel);

        popupMenu.add(new JSeparator());
        super.addItems();
    }

    @Override
    protected void updateItemStateBeforeShow() {
        super.updateItemStateBeforeShow();
        if (editMenuItem != null) {
            editMenuItem.setEnabled(panel.isSelectedCourbeModifiable());
        }
        refinedMenuItem.setEnabled(panel.isSelectionRefinable());
        alignPointMenuItem.setEnabled(panel.isSelectionAlignable());
        removeMenuItem.setEnabled(panel.isSelectionRemovable());
        ExportTableCommentSupplier exportTableCommentSupplier = panel.getGraphe().getExportTableCommentSupplier();
        if (exportTableCommentSupplier != null && ctuluTableExportAction != null) {
            super.ctuluTableExportAction.setExportTableCommentSupplier(exportTableCommentSupplier);
        }
    }
}
