/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.common;

import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 *
 * @author Frederic Deniger
 */
public class MouseAdapterDoubleCick extends MouseAdapter {
  private final ActionListener listener;

  public MouseAdapterDoubleCick(ActionListener listener) {
    this.listener = listener;
  }

  @Override
  public void mouseClicked(MouseEvent e) {
    if (e.getClickCount() >= 2) {
      listener.actionPerformed(null);
    }
  }
  
}
