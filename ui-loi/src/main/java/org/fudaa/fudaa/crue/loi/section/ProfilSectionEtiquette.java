/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.ToStringTransformable;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierLitNomme;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.config.lit.LitNommeLimite;

/**
 *
 * @author Frederic Deniger
 */
public class ProfilSectionEtiquette implements ToStringInternationalizable, ToStringTransformable, Comparable<ProfilSectionEtiquette> {

  public static List<ProfilSectionEtiquette> getEtiquettes(CrueConfigMetier ccm) {
    List<ProfilSectionEtiquette> res = new ArrayList<>();
    //mis em premier pour l'ordre d'affichage.
    CrueConfigMetierLitNomme ccmLitNomme = ccm.getLitNomme();
    List<LitNommeLimite> limites = ccmLitNomme.getLimites();
    for (LitNommeLimite limite : limites) {
      res.add(new ProfilSectionEtiquette(limite));
    }
    res.add(new ProfilSectionEtiquette(ccmLitNomme.getEtiquetteLimLit().getName()));
    res.add(new ProfilSectionEtiquette(ccmLitNomme.getEtiquetteThalweg()));
    res.add(new ProfilSectionEtiquette(ccmLitNomme.getEtiquetteAxeHyd()));
    for (int i = 0; i < res.size(); i++) {
      ProfilSectionEtiquette profilSectionEtiquette = res.get(i);
      profilSectionEtiquette.setGlobalIdx(i);
    }
    return res;
  }

  public static ProfilSectionEtiquette getEtiquettesLimiteLit(List<ProfilSectionEtiquette> in) {
    for (ProfilSectionEtiquette profilSectionEtiquette : in) {
      if (profilSectionEtiquette.isLimitLit()) {
        return profilSectionEtiquette;
      }
    }
    return null;
  }

  public static Map<LitNommeLimite, ProfilSectionEtiquette> getEtiquettesByLimite(List<ProfilSectionEtiquette> in) {
    Map<LitNommeLimite, ProfilSectionEtiquette> res = new HashMap<>();
    for (ProfilSectionEtiquette profilSectionEtiquette : in) {
      if (profilSectionEtiquette.isLimiteLitNomme()) {
        res.put(profilSectionEtiquette.getLitNommeLimite(), profilSectionEtiquette);
      }
    }
    return res;
  }

  static Map<ItemEnum, ProfilSectionEtiquette> getEtiquettesByProfilEtiquette(List<ProfilSectionEtiquette> etiquettes) {
    Map<ItemEnum, ProfilSectionEtiquette> res = new HashMap<>();
    for (ProfilSectionEtiquette profilSectionEtiquette : etiquettes) {
      if (profilSectionEtiquette.isLocaleEtiquette()) {
        res.put(profilSectionEtiquette.getLocaleEtiquette(), profilSectionEtiquette);
      }
    }
    return res;
  }
  /**
   * c'est soit un litNomme
   */
  final LitNommeLimite litNommeLimite;
  /**
   * soit une étiquette
   */
  final ItemEnum etiquette;
  /**
   * soit limite de lit.
   */
  final String limLitName;
  final boolean limitLit;

  public LitNommeLimite getLitNommeLimite() {
    return litNommeLimite;
  }

  public String getLabel() {
    if (isLimiteLitNomme()) {
      return litNommeLimite.getNom();
    }

    if (isLocaleEtiquette()) {
      return etiquette.geti18n();
    }
    return limLitName;
  }

  @Override
  public String getAsString() {
    return geti18n();
  }

  @Override
  public String geti18n() {
    return getLabel();
  }

  @Override
  public int compareTo(ProfilSectionEtiquette o) {
    return globalIdx - o.globalIdx;
  }

  @Override
  public String geti18nLongName() {
    return getLabel();
  }
  int globalIdx;

  public int getGlobalIdx() {
    return globalIdx;
  }

  public void setGlobalIdx(int globalIdx) {
    this.globalIdx = globalIdx;
  }

  private ProfilSectionEtiquette(LitNommeLimite litNommeLimite) {
    this.litNommeLimite = litNommeLimite;
    limitLit = false;
    this.limLitName = null;
    etiquette = null;
  }

  private ProfilSectionEtiquette(ItemEnum etiquette) {
    this.etiquette = etiquette;
    this.litNommeLimite = null;
    this.limLitName = null;
    limitLit = false;
  }

  private ProfilSectionEtiquette(String limLitName) {
    this.etiquette = null;
    this.litNommeLimite = null;
    this.limLitName = limLitName;
    limitLit = true;
  }

  public boolean isLocaleEtiquette() {
    return etiquette != null;
  }

  public boolean isLimiteLitNomme() {
    return litNommeLimite != null;
  }

  public boolean isLimitLit() {
    return limitLit;
  }

  @Override
  public int hashCode() {
    return Integer.valueOf(globalIdx).hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ProfilSectionEtiquette other = (ProfilSectionEtiquette) obj;
    if (this.globalIdx != other.globalIdx) {
      return false;
    }
    return true;
  }

  public ItemEnum getLocaleEtiquette() {
    return etiquette;
  }
}
