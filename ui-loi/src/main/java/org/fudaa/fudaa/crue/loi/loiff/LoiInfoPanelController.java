package org.fudaa.fudaa.crue.loi.loiff;

import com.memoire.bu.BuGridLayout;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.fudaa.crue.loi.LoiMessages;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class LoiInfoPanelController {

  final JTextField txtTypeLoi;
  final JLabel lbTypeLoi;
  final JTextField txtTypeControlLoi;
  final JLabel lbTypeControlLoi;
  final JTextField txtExtrapolInf;
  final JLabel lbExtrapolInf;
  final JTextField txtExtrapolSup;
  final JLabel lbExtrapolSup;
  final JLabel lbAbscisseValidityContent;
  final JLabel lbAbscisseValidity;
  final JLabel lbOrdonneValidityContent;
  final JLabel lbOrdonneValidity;

  public LoiInfoPanelController() {
    txtTypeLoi = new JTextField(15);
    txtTypeLoi.setEditable(false);

    txtTypeControlLoi = new JTextField(15);
    txtTypeControlLoi.setEditable(false);

    txtExtrapolInf = new JTextField(15);
    txtExtrapolInf.setEditable(false);

    txtExtrapolSup = new JTextField(15);
    txtExtrapolSup.setEditable(false);

    lbTypeLoi = new JLabel(NbBundle.getMessage(LoiMessages.class, "TypeLoi.Label"));
    lbTypeControlLoi = new JLabel(NbBundle.getMessage(LoiMessages.class, "TypeControleLoi.Label"));
    lbExtrapolInf = new JLabel(NbBundle.getMessage(LoiMessages.class, "ExtrapolInf.Label"));
    lbExtrapolSup = new JLabel(NbBundle.getMessage(LoiMessages.class, "ExtrapolSup.Label"));

    lbAbscisseValidityContent = new JLabel();
    lbAbscisseValidity = new JLabel(NbBundle.getMessage(LoiMessages.class, "AbscisseValidity.Label"));
    lbOrdonneValidityContent = new JLabel();
    lbOrdonneValidity = new JLabel(NbBundle.getMessage(LoiMessages.class, "OrdonneValidity.Label"));
  }
  JPanel panel;

  public JPanel getPanel() {
    if (panel == null) {
      createPanel();
    }
    return panel;
  }

  public void setTypeLoi(ConfigLoi configLoi) {
    txtTypeLoi.setText(configLoi.getTypeLoi().toString());
    txtTypeControlLoi.setText(configLoi.getControleLoi().getNom());
    txtExtrapolInf.setText(configLoi.getExtrapolInf().getNom());
    txtExtrapolSup.setText(configLoi.getExtrapolSup().getNom());
    lbAbscisseValidityContent.setText(configLoi.getVarAbscisse().getValidator().getHtmlDesc());
    lbOrdonneValidityContent.setText(configLoi.getVarOrdonnee().getValidator().getHtmlDesc());
  }

  private void createPanel() {
    panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 10));
    JPanel info = new JPanel(new BuGridLayout(2, 5, 5));
    info.add(lbTypeLoi);
    info.add(txtTypeLoi);
    info.add(new JLabel());
    info.add(new JLabel());
    info.add(lbTypeControlLoi);
    info.add(txtTypeControlLoi);
    info.add(lbExtrapolInf);
    info.add(txtExtrapolInf);
    info.add(lbExtrapolSup);
    info.add(txtExtrapolSup);
    panel.add(info);
    final BuGridLayout buGridLayout = new BuGridLayout(2, 5, 5, true, false);
    buGridLayout.setYAlign(0f);
    buGridLayout.setXAlign(0f);
    JPanel valid = new JPanel(buGridLayout);
    valid.add(lbAbscisseValidity);
    valid.add(lbAbscisseValidityContent);
    valid.add(lbOrdonneValidity);
    valid.add(lbOrdonneValidityContent);
    panel.add(valid);

  }
}
