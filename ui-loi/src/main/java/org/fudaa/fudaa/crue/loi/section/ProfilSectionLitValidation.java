/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierLitNomme;
import org.fudaa.dodico.crue.config.lit.LitNommeLimite;

/**
 *
 * @author Frederic Deniger
 */
public class ProfilSectionLitValidation {

  CtuluLog valid(List<ProfilSectionLine> lines, CrueConfigMetierLitNomme configLitNomme) {
    CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    List<LitNommeLimite> expectedOrder = configLitNomme.getLimites();
    List<LitNommeLimite> editedOrder = new ArrayList<>();//conserve l'ordre
    int idx = 0;
    int litMineurStart = -1;
    int litMineurEnd = -1;
    for (ProfilSectionLine profilSectionLine : lines) {


      final List<LitNommeLimite> definedLimiteLitNomme = profilSectionLine.getDefinedLimites();
      if (!definedLimiteLitNomme.isEmpty()) {
        for (LitNommeLimite litNommeLimite : definedLimiteLitNomme) {
          if (litNommeLimite.isLitMineurStart()) {
            litMineurStart = idx;
          }
          if (litNommeLimite.isLitMineurEnd()) {
            litMineurEnd = idx;
          }
        }
        editedOrder.addAll(definedLimiteLitNomme);
      }
      idx++;
    }
    if (!editedOrder.equals(expectedOrder)) {
      res.addSevereError("profil.editionValidation.order.error", ToStringHelper.transformToNom(editedOrder), ToStringHelper.transformToNom(expectedOrder));
    }
    if (litMineurStart >= litMineurEnd) {
      res.addSevereError("validate.lit.litMineurNull");
    }
    return res;


  }
}
