/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.ebli.courbe.EGModelExportable;
import org.fudaa.fudaa.crue.loi.common.LoiConstanteCourbeModel;

/**
 *
 * @author Frederic Deniger
 */
public class ProfilSectionEtiquetteCourbeModel extends LoiConstanteCourbeModel implements EGModelExportable {

  double x;
  ItemEnum etiquette;

  public ProfilSectionEtiquetteCourbeModel() {
    super(new double[]{0, 0}, new double[]{0, 1}, null, null);
  }


  public void setEtiquette(ItemEnum etiquette) {
    this.etiquette = etiquette;
    if (etiquette != null) {
      setKey(etiquette.getId());
    }
  }
  boolean contained = false;

  protected void reload(ProfilSectionLoiUiController uiController) {
    if (uiController.getLoiModel() == null) {
      return;
    }
    ProfilSectionLine findLine = uiController.getLoiModel().findLine(etiquette);
    contained = findLine != null;
    if (findLine != null) {
      x = findLine.getPtEvolutionFF().getAbscisse();
    }
  }

  @Override
  public int getNbValues() {
    return contained ? super.getNbValues() : 0;
  }

  public void setX(double x) {
    this.x = x;
  }

  @Override
  public double getX(int idx) {
    return x;
  }

  @Override
  public boolean isViewableinTable() {
    return false;
  }
}
