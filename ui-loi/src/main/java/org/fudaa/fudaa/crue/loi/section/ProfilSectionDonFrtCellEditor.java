/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import com.jidesoft.swing.ComboBoxSearchable;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierLitNomme;
import org.fudaa.dodico.crue.config.lit.LitNommeLimite;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;

/**
 *
 * @author Frederic Deniger
 */
public class ProfilSectionDonFrtCellEditor extends DefaultCellEditor {

  List<DonFrt> frt;
  List<DonFrt> frtStockage;
  final CrueConfigMetierLitNomme ccmLitNomme;

  private static class CustomComboBoxSearchable extends ComboBoxSearchable {

    public CustomComboBoxSearchable(JComboBox comboBox) {
      super(comboBox);
    }

    @Override
    protected String convertElementToString(Object object) {
      return ((ObjetNomme) object).getNom();
    }
  }
  private final ProfilSectionLoiUiController uiController;

  public ProfilSectionDonFrtCellEditor(CrueConfigMetierLitNomme ccmLitNomme, ProfilSectionLoiUiController uiController) {
    super(new JComboBox());
    this.ccmLitNomme = ccmLitNomme;
    JComboBox cb = (JComboBox) super.editorComponent;
    new CustomComboBoxSearchable(cb);//pour activer la recherche
    cb.setRenderer(new ObjetNommeCellRenderer());
    this.uiController = uiController;
    cb.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() >= 2) {
          cancelCellEditing();
          ProfilSectionDonFrtCellEditor.this.uiController.openSelectedFrt();
        }
      }
    });
  }

  protected DonFrt[] getFrt() {
    if (frt == null) {
      return new DonFrt[0];
    }
    return frt.toArray(new DonFrt[0]);
  }

  protected void setFrt(List<DonFrt> frt) {
    this.frt = frt;
  }

  private DonFrt[] getFrtStockage() {
    return frtStockage.toArray(new DonFrt[0]);
  }

  protected void setFrtStockage(List<DonFrt> frtStockage) {
    this.frtStockage = frtStockage;
  }
  private Boolean frtNormalUsed;

  LitNommeLimite getPrecedingLitNomme(JTable table, int row) {
    ProfilSectionLoiTableModel tableModel = (ProfilSectionLoiTableModel) table.getModel();
    for (int i = row - 1; i >= 0; i--) {
      if (tableModel.getLine(i).containsLimiteLitNomme()) {
        return tableModel.getLine(i).getLastDefinedLitNomme();
      }
    }
    return null;
  }

  @Override
  public boolean isCellEditable(EventObject anEvent) {
    if (anEvent instanceof MouseEvent) {
      if (((MouseEvent) anEvent).getClickCount() >= 2) {
        stopCellEditing();
        return false;
      }
    }
    return super.isCellEditable(anEvent);
  }

  @Override
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
    ProfilSectionLoiTableModel tableModel = (ProfilSectionLoiTableModel) table.getModel();
    ProfilSectionLine line = tableModel.getLine(row);
    Object valueToUse = value;
    LitNommeLimite last = line.getLastDefinedLitNomme();
    if (last == null && line.containsLimiteLit()) {//on recupere le litNomme englobant:
      last = getPrecedingLitNomme(table, row);
    }
    if (last != null) {
      Boolean currentFrtNormalUsed = Boolean.valueOf(!ccmLitNomme.isStockage(last));
      if (!ObjectUtils.equals(frtNormalUsed, currentFrtNormalUsed)) {
        frtNormalUsed = currentFrtNormalUsed;
        JComboBox cb = (JComboBox) super.editorComponent;
        cb.setModel(new DefaultComboBoxModel(frtNormalUsed ? getFrt() : getFrtStockage()));
      }
    }

    return super.getTableCellEditorComponent(table, valueToUse, isSelected, row, column);
  }
}
