/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.crue.loi.LoiMessages;
import org.openide.util.NbBundle;

import java.awt.*;

/**
 * @author Frederic Deniger
 */
public class ProfilSectionCourbeBuilder {
  private ProfilSectionCourbeBuilder() {

  }

  public static EGCourbeSimple build(EGAxeVertical axeVertical, ProfiLSectionCourbeModel model) {
    EGCourbeSimple courbe = new ProfilSectionCourbe(axeVertical, model);
    courbe.setIconeModelSpecific(new TraceIconModel(TraceIcon.CARRE_PLEIN, 2, Color.BLACK));
    courbe.setUseSpecificIcon(true);
    courbe.setSpecificIconTitle(NbBundle.getMessage(LoiMessages.class, "EtiquetteIcon.Label"));
    return courbe;
  }
}
