/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.casier;

import java.awt.Component;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;

/**
 *
 * @author Frederic Deniger
 */
public class ProfilCasierLitUtileCellEditor extends DefaultCellEditor {

  public ProfilCasierLitUtileCellEditor() {
    super(new JComboBox(new Object[]{ProfilCasierLine.Limite.NONE, ProfilCasierLine.Limite.DEB, ProfilCasierLine.Limite.FIN}));
    getComponent().setRenderer(new ProfilCasierLitUtileCellRenderer());
  }

  @Override
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
    return super.getTableCellEditorComponent(table, value, isSelected, row, column);
  }

  @Override
  public final JComboBox getComponent() {
    return (JComboBox) super.getComponent();
  }
}
