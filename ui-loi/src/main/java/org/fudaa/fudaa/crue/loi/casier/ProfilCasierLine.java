package org.fudaa.fudaa.crue.loi.casier;

import org.fudaa.fudaa.crue.loi.LoiMessages;
import org.fudaa.fudaa.crue.loi.common.LoiLine;

/**
 * Une ligne d'un profil casier qui permet de gérer les limites.
 *
 * @author Frederic Deniger
 */
public class ProfilCasierLine extends LoiLine {

  public enum Limite {

    DEB(org.openide.util.NbBundle.getMessage(LoiMessages.class, "ProfilCasier.LitUtilDeb.Label")),
    FIN(org.openide.util.NbBundle.getMessage(LoiMessages.class, "ProfilCasier.LitUtilFin.Label")),
    NONE(" ");
    final String i18n;

    Limite(String i18n) {
      this.i18n = i18n;
    }

    public String getI18n() {
      return i18n;
    }
  }
  private Limite limite;

  public Limite getLimite() {
    return limite;
  }

  public void setLimite(Limite limite) {
    this.limite = limite;
  }
}
