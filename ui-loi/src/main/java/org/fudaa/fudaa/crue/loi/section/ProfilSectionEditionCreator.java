/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.section;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierLitNomme;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.config.lit.LitNommeLimite;
import org.fudaa.dodico.crue.edition.EditionProfilCreator;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilEtiquette;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.LitNumerote;
import org.fudaa.dodico.crue.metier.emh.PtProfil;
import org.fudaa.fudaa.crue.common.config.ConfigDefaultValuesProvider;

/**
 * Créé le DonPrtGeoProfilSection à partir des données en cours d'édition
 * @author Frederic Deniger
 */
public class ProfilSectionEditionCreator {

  private final ConfigDefaultValuesProvider defaultValuesProvider;

  ProfilSectionEditionCreator(ConfigDefaultValuesProvider defaultValuesProvider) {
    this.defaultValuesProvider = defaultValuesProvider;
  }

  /**
   * 
   * @param ptProfils les points
   * @param limDebAndFin l'indice du point
   * @return  litNumerote avec LimDeb = LimFin = ptProfils[limDebAndFin]
   */
  private LitNumerote createLitNumerote(List<PtProfil> ptProfils, int limDebAndFin) {
    LitNumerote litNumeroteEnCours = new LitNumerote();
    litNumeroteEnCours.setLimDeb(ptProfils.get(limDebAndFin));
    litNumeroteEnCours.setLimFin(litNumeroteEnCours.getLimDeb());
    return litNumeroteEnCours;
  }

  /**
   * 
   * @param controller contient les données en cours d'édition
   * @return DonPrtGeoProfilSection avec toutes les données y compris les étiquettes et les données fente
   */
  DonPrtGeoProfilSection createEdited(ProfilSectionLoiUiController controller) {
    ProfiLSectionCourbeModel model = controller.getLoiModel();
    List<PtProfil> ptProfils = new ArrayList<>();
    int lastRowWithLitNomme = createPtProfils(model, ptProfils);
    List<LitNumerote> newLitNumerote = createLitNumerotes(lastRowWithLitNomme, model, ptProfils);
    updateFrottements(controller, newLitNumerote);
    DonPrtGeoProfilSection res = new DonPrtGeoProfilSection();
    res.setPtProfil(ptProfils);
    res.setLitNumerote(newLitNumerote);
    res.setEtiquettes(createNewEtiquettes(model));
    if (controller.profil != null) {
      res.setEmh(controller.profil.getEmh());
    }
    res.setFente(controller.fenteController.getFente(controller.ccm));
    return res;
  }

  /**
   * 
   * @param model le model
   * @return  la liste des étiquettes.
   */
  private List<DonPrtGeoProfilEtiquette> createNewEtiquettes(ProfiLSectionCourbeModel model) {
    //on construit les étiquettes:
    List<DonPrtGeoProfilEtiquette> etiquettes = new ArrayList<>();
    for (int i = 0; i < model.getNbValues(); i++) {
      final ProfilSectionLine loiLine = model.getLoiLine(i);
      List<ItemEnum> localeEtiquettes = loiLine.getLocaleEtiquettes();
      for (ItemEnum typeEtiquette : localeEtiquettes) {
        DonPrtGeoProfilEtiquette etiquette = new DonPrtGeoProfilEtiquette();
        etiquette.setTypeEtiquette(typeEtiquette);
        etiquette.setPoint(new PtProfil(loiLine.getPtEvolutionFF()));
        etiquettes.add(etiquette);
      }
    }
    return etiquettes;
  }

  private int createPtProfils(ProfiLSectionCourbeModel model, List<PtProfil> ptProfils) {
    int lastRowWithLitNomme = -1;
    int nbPts = model.getNbValues();
    for (int i = 0; i < nbPts; i++) {
      final ProfilSectionLine loiLine = model.getLoiLine(i);
      ptProfils.add(new PtProfil(loiLine.getPtEvolutionFF()));
      if (loiLine.containsLimiteLitNomme()) {
        lastRowWithLitNomme = i;
      }

    }
    return lastRowWithLitNomme;
  }

  private List<LitNumerote> createLitNumerotes(int lastRowWithLitNomme, ProfiLSectionCourbeModel model, List<PtProfil> ptProfils) {
    LitNumerote litNumeroteEnCours = null;
    List<LitNumerote> newLitNumerote = new ArrayList<>();
    for (int idxPoint = 0; idxPoint <= lastRowWithLitNomme; idxPoint++) {//on va jusqu'à cette limite pour éviter de créer des LitNumerote inutiles.
      ProfilSectionLine loiLine = model.getLoiLine(idxPoint);
      List<LitNommeLimite> definedLimiteLitNomme = loiLine.getDefinedLimites();
      if (!definedLimiteLitNomme.isEmpty()) {

        int nbLitNomme = definedLimiteLitNomme.size();
        for (int litNommeIdx = 0; litNommeIdx < nbLitNomme; litNommeIdx++) {//on commence à 1
          //on termine la ligne en cours:
          if (litNumeroteEnCours != null) {
            litNumeroteEnCours.setLimFin(ptProfils.get(idxPoint));
          }
          final LitNomme after = definedLimiteLitNomme.get(litNommeIdx).getAfter();
          if (after != null) {
            litNumeroteEnCours = createLitNumerote(ptProfils, idxPoint);
            litNumeroteEnCours.setNomLit(after);
            newLitNumerote.add(litNumeroteEnCours);
          }
        }
        if (idxPoint < lastRowWithLitNomme && litNumeroteEnCours!=null) {//si on est à la derniere ligne avec un lit nomme, c'est un lit de stockage et on n'affecte pas le frott.
          litNumeroteEnCours.setFrot(loiLine.getFrt());
        }
      } else if (loiLine.containsLimiteLit() && litNumeroteEnCours != null) {//regle de gestion simple qui ignore les lits pliés.
        //on construit une limite de lit:
        litNumeroteEnCours.setLimFin(ptProfils.get(idxPoint));
        LitNomme currentLitNomme = litNumeroteEnCours.getNomLit();
        litNumeroteEnCours = createLitNumerote(ptProfils, idxPoint);
        litNumeroteEnCours.setNomLit(currentLitNomme);
        litNumeroteEnCours.setFrot(loiLine.getFrt());
        newLitNumerote.add(litNumeroteEnCours);
      }
    }
    return newLitNumerote;
  }

  private void updateFrottements(ProfilSectionLoiUiController controller, List<LitNumerote> newLitNumerote) {
    final CrueConfigMetierLitNomme litNommeConfig = controller.ccm.getLitNomme();
    DonFrt emptyDonFrtFkSto = null;
    EMHSousModele sousModele = controller.sousModeleParent;
    final EditionProfilCreator editionProfilCreator = new EditionProfilCreator(new UniqueNomFinder(), defaultValuesProvider.getDefaultValues());
    for (LitNumerote litNumerote : newLitNumerote) {
      LitNomme nomLit = litNumerote.getNomLit();
      //on reinitialise correctement les actifs
      litNumerote.setIsLitActif(litNommeConfig.isActif(nomLit));
      if (nomLit.equals(litNommeConfig.getMineur())) {
        litNumerote.setIsLitMineur(true);
      }
      //on reinitialise correctement les frottements:
      if (litNumerote.getFrot() == null) {
        if (litNumerote.getIsLitActif()) {
//          assert litNumerote.getIsLitMineur() == false;//ne doit jamais arriver.
          litNumerote.setFrot(editionProfilCreator.getOrCreateFrtMajeur(sousModele, controller.ccm));
        } else {
          if (emptyDonFrtFkSto == null) {
            emptyDonFrtFkSto = editionProfilCreator.addEmptyZFkStoIfNeeded(sousModele, controller.ccm);
          }
          litNumerote.setFrot(emptyDonFrtFkSto);
        }
      }
    }
  }
}
