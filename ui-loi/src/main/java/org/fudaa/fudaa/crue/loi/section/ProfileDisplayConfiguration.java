package org.fudaa.fudaa.crue.loi.section;

import org.fudaa.ebli.controle.BSelecteurColorChooserBt;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGCourbeConfigureTarget;
import org.fudaa.fudaa.crue.loi.LoiMessages;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.openide.util.NbBundle.getMessage;

/**
 * @author deniger
 */
class ProfileDisplayConfiguration extends EGCourbeConfigureTarget.Display {
  public ProfileDisplayConfiguration(EGCourbe egCourbe) {
    super(egCourbe);
  }

  @Override
  public Object getProperty(String key) {
    if (ProfilSectionCourbe.PROP_COLOR_LIT_MINEUR.equals(key)) {
      return ((ProfilSectionCourbe) target_).getColorLitMineur();
    }
    if (ProfilSectionCourbe.PROP_COLOR_LIT_MAJEUR.equals(key)) {
      return ((ProfilSectionCourbe) target_).getColorLitMajeur();
    }
    if (ProfilSectionCourbe.PROP_COLOR_STOCKAGE.equals(key)) {
      return ((ProfilSectionCourbe) target_).getColorStockage();
    }
    return super.getProperty(key);
  }

  @Override
  public boolean setProperty(String key, Object newProp) {
    if (ProfilSectionCourbe.PROP_COLOR_LIT_MINEUR.equals(key)) {
      return ((ProfilSectionCourbe) target_).setColorLitMineur((Color) newProp);
    }
    if (ProfilSectionCourbe.PROP_COLOR_LIT_MAJEUR.equals(key)) {
      return ((ProfilSectionCourbe) target_).setColorLitMajeur((Color) newProp);
    }
    if (ProfilSectionCourbe.PROP_COLOR_STOCKAGE.equals(key)) {
      return ((ProfilSectionCourbe) target_).setColorStockage((Color) newProp);
    }
    return super.setProperty(key, newProp);
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    List<BSelecteurInterface> selecteurs = new ArrayList<>(Arrays.asList(super.createSelecteurs()));
    selecteurs.add(create(ProfilSectionCourbe.PROP_COLOR_LIT_MINEUR, "ProfileDisplayConfiguration.LitMineur"));
    selecteurs.add(create(ProfilSectionCourbe.PROP_COLOR_LIT_MAJEUR, "ProfileDisplayConfiguration.LitMajeur"));
    selecteurs.add(create(ProfilSectionCourbe.PROP_COLOR_STOCKAGE, "ProfileDisplayConfiguration.Stockage"));
    return selecteurs.toArray(new BSelecteurInterface[0]);
  }

  private BSelecteurColorChooserBt create(String property, String translationCode) {
    BSelecteurColorChooserBt btLitMineur = new BSelecteurColorChooserBt(property);
    btLitMineur.setTitle(getMessage(LoiMessages.class, translationCode));
    return btLitMineur;
  }
}
