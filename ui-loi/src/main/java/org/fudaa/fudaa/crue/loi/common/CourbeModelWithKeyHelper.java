/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loi.common;

import java.util.Collection;
import org.fudaa.ebli.courbe.EGCourbeSimple;

/**
 *
 * @author Frederic Deniger
 */
public class CourbeModelWithKeyHelper {
  
  
  public static EGCourbeSimple find(Collection<EGCourbeSimple> list, Object key) {
    if (key == null) {
      return null;
    }
    for (EGCourbeSimple object : list) {
      if (object.getModel() != null && key.equals(((CourbeModelWithKey) object.getModel()).getKey())) {
        return object;
      }
    }
    return null;
  }
  
}
