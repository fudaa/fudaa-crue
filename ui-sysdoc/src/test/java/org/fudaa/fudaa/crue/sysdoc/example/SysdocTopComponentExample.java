/*
 GPL 2
 */
package org.fudaa.fudaa.crue.sysdoc.example;

import org.fudaa.fudaa.crue.sysdoc.SysdocTopComponent;
import org.fudaa.fudaa.crue.sysdoc.admin.SysdocAdminUI;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 *
 * @author Frederic Deniger
 */
public class SysdocTopComponentExample {
  public static void display(final JComponent jc) {
    final JFrame fr = new JFrame();
    fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    fr.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(final WindowEvent e) {
      }
    });

    fr.setContentPane(jc);
    fr.setSize(500, 600);
    fr.setVisible(true);
  }

  public static void main(String[] args) {
    System.setProperty("dev.etcDir", "C:\\data\\Fudaa-Crue\\etc");//en dur...
    System.setProperty("dev.userDir", "C:\\data\\Fudaa-Crue\\etc");//en dur...
    SysdocAdminUI.activeRole();
    SysdocTopComponent tc = new SysdocTopComponent();
    tc.open();
    display(tc);
  }
}
