/*
 GPL 2
 */
package org.fudaa.fudaa.crue.sysdoc;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.sysdoc.Sysdoc;
import org.fudaa.dodico.crue.sysdoc.common.DocEntry;
import org.fudaa.dodico.crue.sysdoc.common.SysdocFolder;
import org.fudaa.fudaa.crue.sysdoc.node.DocNodeFactory;
import static org.junit.Assert.*;
import org.junit.Test;
import org.openide.nodes.Node;

/**
 *
 * @author Frederic Deniger
 */
public class DocNodeFactoryTest {

  public DocNodeFactoryTest() {
  }

  @Test
  public void testBuildNodes() {
    DocNodeFactory factory = new DocNodeFactory();
    SysdocFolder folder = new SysdocFolder(null);
    List<DocEntry> entries = new ArrayList<>();
    DocEntry entry = createEntry("title");
    entry.addChild(createEntry("child1"));
    entry.addChild(createEntry("child2"));
    entries.add(entry);
    entries.add(createEntry("second"));
    DocEntry main = createEntry("main");
    main.addChild(entries.get(0));
    main.addChild(entries.get(1));
    Sysdoc sysdoc = new Sysdoc(folder, main);
    Node createRootNode = factory.createRootNode(sysdoc).first;
    Node[] nodes = createRootNode.getChildren().getNodes();
    assertEquals(2, nodes.length);
    assertEquals("title", nodes[0].getName());
    assertEquals("second", nodes[1].getName());

    Node[] children = nodes[0].getChildren().getNodes();
    assertEquals(2, children.length);
    assertEquals("child1", children[0].getName());
    assertEquals("child2", children[1].getName());
  }

  private DocEntry createEntry(String title) {
    DocEntry entry = new DocEntry();
    entry.setTitle(title);
    return entry;
  }
}
