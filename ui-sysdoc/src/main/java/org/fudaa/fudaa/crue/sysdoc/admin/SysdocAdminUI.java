/*
 GPL 2
 */
package org.fudaa.fudaa.crue.sysdoc.admin;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;
import org.fudaa.dodico.crue.projet.conf.OptionsEnum;
import org.fudaa.dodico.crue.projet.conf.UserConfiguration;
import org.fudaa.dodico.crue.projet.conf.UserOption;
import org.fudaa.dodico.crue.sysdoc.SysdocBuilder;
import org.fudaa.dodico.crue.sysdoc.index.FileIndexer;
import org.fudaa.dodico.crue.sysdoc.scan.LinkRefChecker;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.sysdoc.SysdocTopComponent;
import org.fudaa.fudaa.crue.sysdoc.services.SysdocMsgChecker;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.fudaa.fudaa.crue.sysdoc.admin.Bundle.*;

/**
 *
 * @author Frederic Deniger
 */
@Messages("CheckMsg.Button=Contr\u00f4ler Sydoc")
public class SysdocAdminUI {

  private final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

  /**
   * permet d'activer le role d'admin sysdoc: utiliser pour le OptionProcessor ManagerRoleOptionProcessor.
   */
  public static void activeRole() {
    System.setProperty("sydoc-admin", "true");
  }

  public static boolean isRoleActivated() {
    return "true".equals(System.getProperty("sydoc-admin"));
  }
  private SysdocTopComponent sysdocTopComponent;

  @Messages("SysdocAdmin.ChangeLanguage=Change la langue utilis\u00e9e dans l'application ( sauvegard\u00e9e dans la configuration utilisateur)")
  public void decore(SysdocTopComponent sysdocTopComponent) {
    this.sysdocTopComponent = sysdocTopComponent;
    JPanel top = new JPanel(new FlowLayout(FlowLayout.LEFT));
    sysdocTopComponent.add(top, BorderLayout.NORTH);
    String available = configurationManagerService.getOptionsManager().getOption(OptionsEnum.AVAILABLE_LANGUAGE).getValeur();
    String[] languages = StringUtils.split(available, ';');
    final JComboBox cbLanguage = new JComboBox(languages);
    cbLanguage.setEnabled(languages.length > 1);
    cbLanguage.setSelectedItem(configurationManagerService.getCurrentLocale());
    cbLanguage.setToolTipText(SysdocAdmin_ChangeLanguage());
    cbLanguage.addItemListener(e -> {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        String language = (String) cbLanguage.getSelectedItem();
        UserConfiguration currentUserConfiguration = configurationManagerService.getOptionsManager().getCurrentUserConfiguration();
        List<UserOption> options = new ArrayList<>(currentUserConfiguration.getOptions());
        UserOption userOption = getUserOption(options);
        if (userOption == null) {
          userOption = new UserOption(OptionsEnum.USER_LANGUAGE.getId(), language, StringUtils.EMPTY);
          options.add(userOption);
          currentUserConfiguration.setOptions(options);
        } else {
          userOption.setValeur(language);
        }
        configurationManagerService.saveUserOptions(currentUserConfiguration);
        SysdocAdminUI.this.sysdocTopComponent.reloadToc();
      }
    });

    top.add(cbLanguage);

    top.add(createBtBuildToc());
    top.add(createBtCheckMsg());
    top.add(createBtCheckLink());
  }

  private UserOption getUserOption(Collection<UserOption> options) {
    for (UserOption userOption : options) {
      if (userOption.getId().equals(OptionsEnum.USER_LANGUAGE.getId())) {
        return userOption;
      }
    }
    return null;
  }

  @Messages("ReloadToc.ProgressMsg=Construction de la table des mati\u00e8res")
  private void rebuildToc() {
    File sysdocLocaleDir = sysdocTopComponent.getSysdocLocaleDir();
    RebuildTocProcessor rebuildTocProcessor = new RebuildTocProcessor(sysdocLocaleDir);
    final String title = ReloadToc_ProgressMsg();
    CtuluLogGroup log = CrueProgressUtils.showProgressDialogAndRun(rebuildTocProcessor, title);
    SysdocAdminUI.this.sysdocTopComponent.setEmptyPage();
    SysdocAdminUI.this.sysdocTopComponent.reloadToc();
    if (log != null && log.containsSomething()) {
      LogsDisplayer.displayError(log, title);
    }
  }

  @Messages("BuildToc.Button=Construire l'arborescence")
  private JButton createBtBuildToc() {
    JButton btBuildToc = new JButton(BuildToc_Button());
    btBuildToc.addActionListener(e -> rebuildToc());
    return btBuildToc;
  }

  private JButton createBtCheckMsg() {
    JButton btBuildToc = new JButton(CheckMsg_Button());
    btBuildToc.addActionListener(e -> checkMsg());
    return btBuildToc;
  }

  @Messages("CheckLinks.Button=Contr\u00f4ler les liens")
  private JButton createBtCheckLink() {
    JButton btBuildToc = new JButton(CheckLinks_Button());
    btBuildToc.addActionListener(e -> checkLinks());
    return btBuildToc;
  }

  @Messages("CheckMsg.Success=Tous les liens issus des fichiers de messages sont corrects")
  private void checkMsg() {
    List<CoeurConfig> allCoeurConfigs = configurationManagerService.getCoeurManager().getAllCoeurConfigs();
    List<SysdocMsgChecker> checkers = new ArrayList<>();
    final String currentLocale = configurationManagerService.getCurrentLocale();
    for (CoeurConfig coeurConfig : allCoeurConfigs) {
      checkers.add(new SysdocMsgChecker(coeurConfig, currentLocale, sysdocTopComponent.getSysdocLocaleDir()));
    }
    CheckMsgProgressRunnable checkMsgProcessor = new CheckMsgProgressRunnable(checkers);
    CtuluLogGroup log = CrueProgressUtils.showProgressDialogAndRun(checkMsgProcessor, CheckMsg_Button());
    if (log != null && log.containsSomething()) {
      LogsDisplayer.displayError(log, CheckMsg_Button());
    } else {
      DialogHelper.showInfo(CheckMsg_Button(), CheckMsg_Success());
    }
  }

  @Messages("CheckLinks.Success=Tous les liens utilisés dans les fichiers sont corrects")
  private void checkLinks() {
    CheckLinksProgressRunnable checkMsgProcessor = new CheckLinksProgressRunnable(sysdocTopComponent.getSysdocLocaleDir());
    CtuluLogResult<CtuluLogGroup> log = CrueProgressUtils.showProgressDialogAndRun(checkMsgProcessor, CheckMsg_Button());
    if (log.getLog().containsErrorOrSevereError()) {
      LogsDisplayer.displayError(log.getLog(), CheckLinks_Button());
    } else if (log.getResultat().containsSomething()) {
      LogsDisplayer.displayError(log.getResultat(), CheckLinks_Button());
    } else {
      DialogHelper.showInfo(CheckLinks_Button(), CheckLinks_Success());
    }
  }

  private class RebuildTocProcessor implements ProgressRunnable<CtuluLogGroup> {

    private final File baseDir;

    RebuildTocProcessor(File baseDir) {
      this.baseDir = baseDir;
    }

    @Override
    public CtuluLogGroup run(ProgressHandle handle) {
      handle.switchToIndeterminate();
      SysdocBuilder builder = new SysdocBuilder(baseDir);
      CtuluLogGroup log = builder.build();
      FileIndexer fileIndexer = new FileIndexer(baseDir, configurationManagerService.getCurrentLocale());
      CtuluLog index = fileIndexer.index();
      if (index != null && index.isNotEmpty()) {
        log.addLog(index);
      }
      return log;
    }
  }

  public class CheckLinksProgressRunnable implements ProgressRunnable<CtuluLogResult<CtuluLogGroup>> {

    private final File dir;

    CheckLinksProgressRunnable(File dir) {
      this.dir = dir;
    }

    @Override
    public CtuluLogResult<CtuluLogGroup> run(ProgressHandle handle) {
      handle.switchToIndeterminate();
      LinkRefChecker refChecker = new LinkRefChecker(dir);
      return refChecker.checkLink();
    }
  }
}
