package org.fudaa.fudaa.crue.sysdoc;

import com.jidesoft.swing.JideSplitPane;
import com.jidesoft.swing.JideTabbedPane;
import com.jidesoft.swing.JideToggleButton;
import com.memoire.bu.BuBorders;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuResource;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.config.cr.LienDocumentation;
import org.fudaa.dodico.crue.sysdoc.Sysdoc;
import org.fudaa.dodico.crue.sysdoc.SysdocLoader;
import org.fudaa.dodico.crue.sysdoc.common.DocEntry;
import org.fudaa.dodico.crue.sysdoc.common.SysdocFolder;
import org.fudaa.dodico.crue.sysdoc.index.FileIndexer;
import org.fudaa.fudaa.crue.common.AbstractTopComponent;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.SysdocContrat;
import org.fudaa.fudaa.crue.sysdoc.admin.SysdocAdminUI;
import org.fudaa.fudaa.crue.sysdoc.node.DocNodeFactory;
import org.fudaa.fudaa.crue.sysdoc.services.SysdocService;
import org.jdesktop.swingx.JXList;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.HtmlBrowser;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.BeanTreeView;
import org.openide.explorer.view.TreeView;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.StyleSheet;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Top browserComponent which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.sysdoc//SysdocTopComponent//EN", autostore = false)
@TopComponent.Description(preferredID = SysdocTopComponent.TOPCOMPONENT_ID, persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "sysdoc", position = 1, openAtStartup = false)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.sysdoc.SysdocTopComponent")
@TopComponent.OpenActionRegistration(displayName = "#CTL_SysdocTopComponent", preferredID = SysdocTopComponent.TOPCOMPONENT_ID)
@ActionReference(path = "Menu/Window/" + SysdocContrat.SYDOC_ID, position = 1)
public final class SysdocTopComponent extends AbstractTopComponent implements ExplorerManager.Provider {
  public static final String TOPCOMPONENT_ID = "SysdocTopComponent";
  private final transient SysdocService sysdocService = Lookup.getDefault().lookup(SysdocService.class);
  private File sysdocLocaleDir;
  private final TreeView treeView;
  private final ExplorerManager em;
  private final HtmlBrowser browser;
  private final JTextField txtUrl;
  private final JideTabbedPane tabbedPane;
  private final JideSplitPane splitPane;
  private final JideToggleButton btHighlight;
  private final CustomFindBar findBar;
  private JTextField tf;
  private JList listResultat;
  private transient EditorSearchable editorSearchable;

  public SysdocTopComponent() {
    initComponents();

    setName(NbBundle.getMessage(SysdocTopComponent.class, "CTL_SysdocTopComponent"));
    setToolTipText(NbBundle.getMessage(SysdocTopComponent.class, "HINT_SysdocTopComponent"));
    browser = new HtmlBrowser();
    browser.getBrowserImpl().addPropertyChangeListener(evt -> {
      if (HtmlBrowser.Impl.PROP_URL.equals(evt.getPropertyName())) {
        urlChanged();
      }
    });
    browser.setEnableHome(true);
    customizeBrowser();

    txtUrl = getBrowserTextField();
    if (txtUrl != null) {
      txtUrl.setEditable(false);
    }
    treeView = new BeanTreeView();
    treeView.setRootVisible(true);
    splitPane = new JideSplitPane();
    tabbedPane = new JideTabbedPane();
    tabbedPane.add(treeView);
    tabbedPane.addTab(StringUtils.EMPTY, BuResource.BU.getIcon("crystal_arbre"), treeView, NbBundle.getMessage(SysdocTopComponent.class,
        "Sysdoc.TocTooltip"));
    tabbedPane.addTab(StringUtils.EMPTY, ImageUtilities.loadImageIcon("org/fudaa/fudaa/crue/sysdoc/icons/rechercher.png", false), createSearchPanel(),
        NbBundle.getMessage(SysdocTopComponent.class, "Sysdoc.SearchTooltip"));
    splitPane.add(tabbedPane);
    final JPanel pnBrowser = new JPanel(new BorderLayout());
    pnBrowser.add(browser);
    findBar = createFindComponent();
    btHighlight = new JideToggleButton(NbBundle.getMessage(SysdocTopComponent.class, "highlight.All"));
    btHighlight.addItemListener(e -> highlightStateChanged());
    final JPanel findBarPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
    findBarPanel.add(findBar);
    findBarPanel.add(btHighlight);
    pnBrowser.add(findBarPanel, BorderLayout.SOUTH);
    splitPane.add(pnBrowser);
    splitPane.setShowGripper(true);
    add(splitPane);
    treeView.setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
    final ActionMap map = this.getActionMap();
    em = new ExplorerManager();
    em.addPropertyChangeListener(evt -> {
      if (ExplorerManager.PROP_SELECTED_NODES.equals(evt.getPropertyName())) {
        nodeSelectionChanged();
      }
    });
    associateLookup(ExplorerUtils.createLookup(em, map));
    if (SysdocAdminUI.isRoleActivated()) {
      final SysdocAdminUI adminUi = new SysdocAdminUI();
      adminUi.decore(this);
    }
    updateSize();
  }

  @Override
  protected String getHelpCtxId() {
    return null;
  }

  private void highlightStateChanged() {
    if (btHighlight.isSelected() != this.editorSearchable.isHighlightEnabled()) {
      editorSearchable.setHighlightEnabled(btHighlight.isSelected());
      if (StringUtils.isNotBlank(findBar.getText())) {
        editorSearchable.highlight(findBar.getPattern());
      }
    }
  }

  private boolean fromSearchPanel;

  private JPanel createSearchPanel() {
    final JPanel pnSearch = new JPanel(new BorderLayout());
    tf = new JTextField(10);
    tf.setToolTipText(NbBundle.getMessage(SysdocTopComponent.class, "searchTextField.Tooltip"));
    final JButton bt = new JButton();
    final ImageIcon searchIcon = ImageUtilities.loadImageIcon("org/fudaa/fudaa/crue/sysdoc/icons/rechercher.png", false);
    bt.setIcon(searchIcon);
    bt.setBorderPainted(false);
    bt.setContentAreaFilled(false);
    final ActionListener actionListener = e -> search();
    bt.addActionListener(actionListener);
    tf.addActionListener(actionListener);
    final JPanel topPanel = new JPanel(new BorderLayout());
    topPanel.add(tf);
    topPanel.add(bt, BorderLayout.EAST);
    pnSearch.add(topPanel, BorderLayout.NORTH);
    listResultat = new JXList();
    listResultat.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    listResultat.getSelectionModel().addListSelectionListener(e -> {
      final SearchResultat resultat = (SearchResultat) listResultat.getSelectedValue();
      if (resultat != null) {
        try {
          fromSearchPanel = true;
          em.setSelectedNodes(new Node[]{resultat.getNode()});
        } catch (final PropertyVetoException ex) {
          Exceptions.printStackTrace(ex);
        }
      }
    });
    listResultat.setCellRenderer(new HitsRenderer());
    listResultat.setModel(new DefaultListModel());
    pnSearch.add(new JScrollPane(listResultat));
    return pnSearch;
  }

  private void search() {
    final SysdocFolder sysdocFolder = new SysdocFolder(sysdocLocaleDir);
    try (IndexReader reader = DirectoryReader.open(FSDirectory.open(sysdocFolder.getSysdocSearchIndexDir()))) {
      final IndexSearcher searcher = new IndexSearcher(reader);
      final Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
      final QueryParser parser = new QueryParser(Version.LUCENE_40, FileIndexer.CONTENTS_ID, analyzer);
      final Query query = parser.parse(tf.getText());
      final TopDocs search = searcher.search(query, 100);
      final float maxScore = search.getMaxScore();
      final ScoreDoc[] hits = search.scoreDocs;
      final DefaultListModel model = (DefaultListModel) listResultat.getModel();
      model.removeAllElements();
      for (final ScoreDoc scoreDoc : hits) {
        final Document doc = searcher.doc(scoreDoc.doc);
        final SearchResultat resultat = new SearchResultat();
        if (maxScore > 0) {
          resultat.setScore((int) (scoreDoc.score * 100 / maxScore));
        }
        final String path = doc.get(FileIndexer.PATH_ID);
        final Node node = nodeByPath.get(path);
        resultat.setNode(node);
        model.addElement(resultat);
      }
    } catch (final Exception exception) {
      Exceptions.printStackTrace(exception);
    }
  }

  /**
   * @return le dossier contenant les locales de Sysdoc
   */
  public File getSysdocLocaleDir() {
    return sysdocLocaleDir;
  }

  private void documentLoaded() {
    EventQueue.invokeLater(() -> {

      if (fromSearchPanel) {
        fromSearchPanel = false;
        findBar.setText(tf.getText());
        btHighlight.setSelected(true);
      }
      if (btHighlight.isSelected()) {
        editorSearchable.highlight(findBar.getPattern());
      }
    });
  }

  private void urlChanged() {
    if (sysdocLocaleDir != null && nodeByPath != null && !isUpdating) {
      try {
        final String initLocation = browser.getBrowserImpl().getLocation();
        final String location = URLDecoder.decode(initLocation, "UTF-8");
        if (isExternURL(new URL(location))) {
          nodeSelectionChanged();
          return;
        }

        final String base = sysdocLocaleDir.toURI().toURL().toString();
        String path = StringUtils.removeStart(location, base);
        path = StringUtils.substringBefore(path, LienDocumentation.SIGNET_SEPARATOR);
        final Node node = nodeByPath.get(path);
        if (node != null) {
          isUpdating = true;
          em.setSelectedNodes(new Node[]{node});
          isUpdating = false;
        }
      } catch (final Exception exception) {
        Logger.getLogger(SysdocTopComponent.class.getName()).log(Level.INFO, "message {0}", exception);
      }
    }
  }

  private JTextField getBrowserTextField() {
    final Component[] components = browser.getComponents();
    for (final Component component : components) {
      final JTextField browserTextField = getBrowserTextField((JComponent) component);
      if (browserTextField != null) {
        return browserTextField;
      }
    }
    return null;
  }

  private JTextField getBrowserTextField(final JComponent jc) {
    final Component[] components = jc.getComponents();
    for (final Component component : components) {
      if (component instanceof JTextField) {
        return (JTextField) component;
      }
      final JTextField under = getBrowserTextField((JComponent) component);
      if (under != null) {
        return under;
      }
    }
    return null;
  }

  private boolean isUpdating;

  private void nodeSelectionChanged() {
    if (isUpdating) {
      return;
    }
    changeUrlFromNode(null);
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return em;
  }

  @Override
  protected void componentOpened() {
    super.componentOpened();
    updateContent();
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables

  void writeProperties(final java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
    if (splitPane != null) {
      p.setProperty("version", "1.0");
      p.setProperty("splitPane.position", Integer.toString(splitPane.getDividerLocation(0)));
    }
  }

  private int initPosition = -1;

  void readProperties(final java.util.Properties p) {
    final String position = (String) p.get("splitPane.position");
    initPosition = -1;
    if (StringUtils.isNotBlank(position)) {
      try {
        initPosition = Integer.parseInt(position);
      } catch (final NumberFormatException numberFormatException) {
      }
      updateSize();
    }
  }

  private void updateSize() {
    if (initPosition > 0 && splitPane != null) {
      EventQueue.invokeLater(() -> splitPane.setDividerLocation(0, initPosition));
    }
  }

  @Override
  public void componentClosedTemporarily() {
    browser.getBrowserImpl().stopLoading();
  }

  @Override
  public void componentClosedDefinitly() {
    //nothing to do
  }

  public void display(final String path) {
    //test la locale
    updateContent();
    String pathForNode = path;
    boolean containSignet = false;
    if (path.contains(LienDocumentation.SIGNET_SEPARATOR)) {
      containSignet = true;
      pathForNode = StringUtils.substringBeforeLast(path, LienDocumentation.SIGNET_SEPARATOR);
    }

    final Node node = nodeByPath == null ? null : nodeByPath.get(pathForNode);
    //selectionne le noeud
    if (node == null) {
      DialogHelper.showError(getDisplayName(), org.openide.util.NbBundle.getMessage(SysdocTopComponent.class, "OpenDocLink.Error", path));
    } else {
      try {
        if (node.getParentNode() != null) {
          treeView.expandNode(node.getParentNode());
        }
        isUpdating = true;
        em.setSelectedNodes(new Node[]{node});
        if (containSignet) {
          changeUrlFromNode(StringUtils.substringAfterLast(path, LienDocumentation.SIGNET_SEPARATOR));
        } else {
          changeUrlFromNode(null);
        }
        isUpdating = false;
      } catch (final PropertyVetoException ex) {
        Exceptions.printStackTrace(ex);
      }
    }
  }

  private transient Map<String, Node> nodeByPath;
  private long timeStampToc;

  private void updateContent() {
    final File old = sysdocLocaleDir;
    sysdocLocaleDir = sysdocService.getSysdocLocalizedDir();
    boolean mustReload = !sysdocLocaleDir.equals(old);
    final SysdocFolder folder = new SysdocFolder(sysdocLocaleDir);
    try {
      HtmlBrowser.setHomePage(folder.getHomeDir().toURI().toURL().toString());
    } catch (final MalformedURLException ex) {
      Exceptions.printStackTrace(ex);
    }
    if (!mustReload && folder.getTocFile().exists()) {
      mustReload = folder.getTocFile().lastModified() > timeStampToc;
    }
    if (mustReload) {
      final SysdocLoader builder = new SysdocLoader(sysdocLocaleDir);
      final CtuluIOResult<Sysdoc> load = builder.load();
      if (load.getAnalyze().isNotEmpty()) {
        LogsDisplayer.displayError(load.getAnalyze(), getDisplayName());
      }

      timeStampToc = 0;
      if (folder.getTocFile().exists()) {
        timeStampToc = folder.getTocFile().lastModified();
      }
      final DocNodeFactory factory = new DocNodeFactory();
      final Pair<Node, Map<String, Node>> rootNodeMap = factory.createRootNode(load.getSource());
      nodeByPath = rootNodeMap.second;
      em.setRootContext(rootNodeMap.first);
      try {
        em.setSelectedNodes(new Node[]{rootNodeMap.first});
      } catch (final PropertyVetoException ex) {
        Exceptions.printStackTrace(ex);
      }
    }
  }

  private void changeUrlFromNode(final String signet) {
    browser.getBrowserImpl().stopLoading();
    final Node[] selectedNodes = em.getSelectedNodes();
    if (selectedNodes.length == 1) {
      final DocEntry lookup = selectedNodes[0].getLookup().lookup(DocEntry.class);
      if (lookup == null) {
        return;
      }
      final File target = new File(sysdocLocaleDir, lookup.getPath());
      if (!target.exists() && !lookup.isFolder()) {
        DialogHelper.showError(getDisplayName(), org.openide.util.NbBundle.getMessage(SysdocTopComponent.class, "FileNotFoundError", target.
            getAbsolutePath()));
      }
      if (target.exists()) {
        try {
          final URL url = target.toURI().toURL();
          if (StringUtils.isBlank(signet)) {
            browser.setURL(url);
          } else {
            browser.setURL(url.toString() + LienDocumentation.SIGNET_SEPARATOR + signet);
          }
        } catch (final MalformedURLException ex) {
          Exceptions.printStackTrace(ex);
        }
      }
    }
  }

  public void reloadToc() {
    updateContent();
  }

  public void setEmptyPage() {
    browser.getBrowserImpl().stopLoading();
    try {
      browser.setURL(new SysdocFolder(getSysdocLocaleDir()).getSysdocHelpFile().toURI().toURL());
    } catch (final Exception e) {
      Exceptions.printStackTrace(e);
    }
  }

  private void customizeBrowser() {
    final Component[] components = browser.getComponents();
    browser.remove(components[1]);

    final JButton bt = new JButton(ImageUtilities.loadImageIcon("org/fudaa/fudaa/crue/sysdoc/icons/home.png", false));
    bt.setToolTipText(NbBundle.getMessage(SysdocTopComponent.class, "HomeButton.Tooltip"));
    configureButton(bt);
    bt.addActionListener(e -> {
      try {
        final Node rootContext = getExplorerManager().getRootContext();
        if (rootContext != null) {
          getExplorerManager().setSelectedNodes(new Node[]{rootContext});
        }
      } catch (final PropertyVetoException ex) {
        Exceptions.printStackTrace(ex);
      }
    });

    final JButton btPrint = new JButton(ImageUtilities.loadImageIcon("org/fudaa/fudaa/crue/sysdoc/icons/browser.png", false));
    btPrint.setToolTipText(NbBundle.getMessage(SysdocTopComponent.class, "PrintButton.Tooltip"));
    configureButton(btPrint);
    btPrint.addActionListener(e -> {
      try {
        final URL url = browser.getBrowserImpl().getURL();
        if (url != null) {
          Desktop.getDesktop().browse(url.toURI());
        }
      } catch (final Exception ex) {
        Exceptions.printStackTrace(ex);
      }
    });

    final FlowLayout flowLayout = new FlowLayout(FlowLayout.LEFT, 2, 0);
    final JPanel pnButtons = new JPanel(flowLayout);

    pnButtons.add(btPrint);
    pnButtons.add(bt);
    final JPanel pn = new JPanel(new BuGridLayout(2, 0, 0, true, false));
    pn.add(pnButtons);
    pn.add(components[1]);
    Component browserComponent = browser.getBrowserComponent();
    if (browserComponent instanceof JScrollPane) {
      browserComponent = ((JScrollPane) browserComponent).getViewport().getView();
    }
    if (browserComponent instanceof JEditorPane) {
      ((JEditorPane) browserComponent).addHyperlinkListener(e -> {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
          hyperlinkActivated(e);
        }
      });
    }
    browser.add(pn, BorderLayout.NORTH);
  }

  private boolean isExternURL(final URL url) {
    if (url == null) {
      return true;
    }
    try {
      final SysdocFolder folder = new SysdocFolder(sysdocLocaleDir);
      final String baseURL = folder.getBaseDir().toURI().toURL().toString().toLowerCase();
      final String targetURL = url.toString().toLowerCase();
      return !targetURL.startsWith(baseURL);
    } catch (final MalformedURLException malformedURLException) {
    }
    return false;
  }

  private void hyperlinkActivated(final HyperlinkEvent e) {
    try {

      final URL url = e.getURL();
      if (url == null) {
        return;
      }
      final String targetURL = url.toString().toLowerCase();
      if (isExternURL(url)) {
        if (targetURL.startsWith("http") || targetURL.startsWith("mailto")) {
          Desktop.getDesktop().browse(url.toURI());
        } else {
          final File targetFile = new File(url.getFile());
          if (targetFile.exists()) {
            Desktop.getDesktop().browse(targetFile.toURI());
          } else {
            DialogHelper.showWarn(NbBundle.getMessage(SysdocTopComponent.class, "fileNotExist", targetFile.toString()));
          }
        }
      }
    } catch (final Exception ex) {
      Logger.getLogger(getClass().getName()).log(Level.WARNING, ex.getMessage(), ex);
    }
  }

  private void configureButton(final JButton button) {
    button.setOpaque(false);
    button.setBorderPainted(false);
    button.setFocusPainted(false);
    button.setContentAreaFilled(false);
    button.setBorder(BuBorders.EMPTY0000);
  }

  private StyleSheet fontStyleSheet;

  private CustomFindBar createFindComponent() {
    final Component browserComponent = ((JScrollPane) browser.getBrowserComponent()).getViewport().getView();
    final JEditorPane editor = (JEditorPane) browserComponent;
    editor.addPropertyChangeListener("page", evt -> documentLoaded());
    editorSearchable = new EditorSearchable(editor);
    final CustomFindBar newFindBar = new CustomFindBar(editorSearchable);
    final ActionMap actionMap = newFindBar.getActionMap();
    final String findCommand = "find";
    actionMap.put(findCommand, new AbstractAction() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        final Component browserComponent = ((JScrollPane) browser.getBrowserComponent()).getViewport().getView();
        final JEditorPane editor = (JEditorPane) browserComponent;
        final HTMLDocument doc = (HTMLDocument) editor.getDocument();
        final Font deriveFont = editor.getFont().deriveFont(editor.getFont().getSize() + 2f);
        fontStyleSheet = new StyleSheet();
        final String bodyRule = "body { font-family: " + deriveFont.getFamily() + "; "
            + "font-size: " + (deriveFont.getSize() + 15) + "pt; }";
        fontStyleSheet.addRule(bodyRule);
        doc.getStyleSheet().addStyleSheet(fontStyleSheet);
        doc.getStyleSheet().addRule(bodyRule);

        editor.setFont(deriveFont);
        KeyboardFocusManager.getCurrentKeyboardFocusManager()
            .focusNextComponent(newFindBar);
      }
    });
    final InputMap inputContext = newFindBar.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
    inputContext.put(KeyStroke.getKeyStroke("ctrl F"), findCommand);
    inputContext.put(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0), findCommand);
    return newFindBar;
  }
}
