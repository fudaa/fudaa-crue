/*
 GPL 2
 */
package org.fudaa.fudaa.crue.sysdoc.services;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.sysdoc.SysdocTopComponent;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

@ActionID(
        category = "Help",
        id = "org.fudaa.fudaa.crue.sysdoc.services.SysdocMainAction")
@ActionRegistration(displayName = "#CTL_SysdocMainAction")
@ActionReferences(value = {
  @ActionReference(path = "Menu/Help", position = 2),
  @ActionReference(path = "Shortcuts", name = "D-F1")})
@Messages("CTL_SysdocMainAction=Aide Principale")
public final class SysdocMainAction implements ActionListener {

  @Override
  public void actionPerformed(ActionEvent e) {
      ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);
      
      if(configurationManagerService.getOptionsManager().getAideOption().getSyDocActivation())  
      {
          TopComponent sysdocTopComponent = WindowManager.getDefault().findTopComponent(SysdocTopComponent.TOPCOMPONENT_ID);          
          if (!sysdocTopComponent.isOpened()) {
            sysdocTopComponent.open();
          }
          sysdocTopComponent.requestActive();
      }
      else
      {
          SysdocService sysdocService = Lookup.getDefault().lookup(SysdocService.class);
          //Appel de la clé 'index' dans le fichier FudaaCrue_Aide.xml
          sysdocService.display("index");            
      }   
  }
}
