/*
 GPL 2
 */
package org.fudaa.fudaa.crue.sysdoc;

import com.memoire.bu.BuGridLayout;
import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.crue.sysdoc.common.DocEntry;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class HitsRenderer extends JPanel implements ListCellRenderer {

  private final JLabel top = new JLabel();
  private final JLabel comment = new JLabel();

  HitsRenderer() {
    setLayout(new BuGridLayout(1, 0, 0));
    add(top);
    add(comment);
    comment.setFont(CtuluLibSwing.getMiniFont());
    comment.setBorder(BorderFactory.createEmptyBorder(1, 5, 3, 2));
    setOpaque(true);
  }

  @Override
  public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
    setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
    top.setText(StringUtils.EMPTY);
    top.setToolTipText(StringUtils.EMPTY);
    comment.setText(StringUtils.EMPTY);
    comment.setToolTipText(StringUtils.EMPTY);
    top.setForeground(isSelected ? list.getSelectionForeground() : list.getForeground());
    comment.setForeground(top.getForeground());
    SearchResultat doc = (SearchResultat) value;
    if (doc.getNode() != null) {
      if (!isSelected) {
        top.setText("<html><body>+<a href='nowhere'>" + doc.getNode().getDisplayName() + "</a></body></html>");
      } else {
        top.setText("<html><body>+<u>" + doc.getNode().getDisplayName() + "</u></body></html>");
      }
      DocEntry docEntry = doc.getNode().getLookup().lookup(DocEntry.class);
      top.setToolTipText("<html><body>" + docEntry.getPath() + "<br>" + NbBundle.getMessage(HitsRenderer.class, "SearchResultat.Accuracy", doc.getScore()) + "</body></html>");
      setToolTipText(top.getToolTipText());

      if (docEntry.getDescription() != null) {
        comment.setText(docEntry.getDescription());
        comment.setToolTipText(docEntry.getDescription());
      }

    }
    return this;
  }
}
