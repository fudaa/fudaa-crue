/*
 GPL 2
 */
package org.fudaa.fudaa.crue.sysdoc.node;

import javax.swing.Action;
import org.fudaa.dodico.crue.sysdoc.common.DocEntry;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frederic Deniger
 */
public class DocEntryNode extends AbstractNode {

  DocEntryNode(Children children, DocEntry docEntry) {
    super(children, Lookups.fixed(docEntry));
    setName(docEntry.getTitle());
    if (docEntry.isFolder()) {
      setIconBaseWithExtension("org/fudaa/fudaa/crue/sysdoc/icons/folder.png");
    } else {
      setIconBaseWithExtension("org/fudaa/fudaa/crue/sysdoc/icons/document.png");
    }
    setShortDescription(docEntry.getDescription());
  }


  @Override
  public Action[] getActions(boolean context) {
    return new Action[]{SystemAction.get(ChunckHtmlNodeAction.class)};
  }
}
