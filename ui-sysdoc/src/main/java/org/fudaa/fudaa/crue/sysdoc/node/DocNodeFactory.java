/*
 GPL 2
 */
package org.fudaa.fudaa.crue.sysdoc.node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.sysdoc.Sysdoc;
import org.fudaa.dodico.crue.sysdoc.common.DocEntry;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

/**
 *
 * @author Frederic Deniger
 */
public class DocNodeFactory {

  public Pair<Node, Map<String, Node>> createRootNode(Sysdoc sysdoc) {
    Map<String, Node> map = new HashMap<>();
    if (sysdoc == null) {
      return new Pair<>(Node.EMPTY, map);
    }
    DocEntry entry = sysdoc.getEntry();
    final AbstractNode node = createNode(entry, map);
    node.setIconBaseWithExtension("org/fudaa/fudaa/crue/sysdoc/icons/home.png");
    return new Pair<>(node, map);
  }

  private AbstractNode createNode(DocEntry docEntry, Map<String, Node> map) {
    Children children = Children.LEAF;
    if (!docEntry.getChildren().isEmpty()) {
      List<Node> childrenNode = new ArrayList<>();
      for (DocEntry childDocEntry : docEntry.getChildren()) {
        childrenNode.add(createNode(childDocEntry, map));
      }
      children = NodeHelper.createArray(childrenNode);
    }
    AbstractNode node = new DocEntryNode(children, docEntry);
    map.put(docEntry.getPath(), node);
    return node;
  }
}
