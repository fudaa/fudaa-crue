/*
 GPL 2
 */
package org.fudaa.fudaa.crue.sysdoc.admin;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import static org.fudaa.fudaa.crue.sysdoc.admin.Bundle.*;
import org.fudaa.fudaa.crue.sysdoc.services.SysdocMsgChecker;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;

/**
 *
 * @author Frederic Deniger
 */
public class CheckMsgProgressRunnable implements ProgressRunnable<CtuluLogGroup> {

  private final List<SysdocMsgChecker> checkers;

  public static class CheckerCallable implements Callable<CtuluLog> {

    private final SysdocMsgChecker checker;

    CheckerCallable(SysdocMsgChecker checker) {
      this.checker = checker;
    }

    @Override
    public CtuluLog call() {
      return checker.check();
    }
  }

  CheckMsgProgressRunnable(List<SysdocMsgChecker> baseDir) {
    this.checkers = baseDir;
  }

  @Messages(value = "CheckMsg.GroupName=Contr\u00f4le des liens depuis le fichier de message")
  @Override
  public CtuluLogGroup run(ProgressHandle handle) {
    CtuluLogGroup group = new CtuluLogGroup(null);
    group.setDescription(CheckMsg_GroupName());
    if (checkers.size() == 1) {
      CtuluLog check = checkers.get(0).check();
      group.addLog(check);
      return group;
    }
    handle.switchToIndeterminate();
    int nbThread = Math.min(Runtime.getRuntime().availableProcessors(), checkers.size());
    ExecutorService newFixedThreadPool = null;
    try {
      newFixedThreadPool = Executors.newFixedThreadPool(nbThread);
      List<CheckerCallable> callables = new ArrayList<>();
      for (SysdocMsgChecker sysdocMsgChecker : checkers) {
        callables.add(new CheckerCallable(sysdocMsgChecker));
      }
      List<Future<CtuluLog>> invokeAll = newFixedThreadPool.invokeAll(callables);
      for (Future<CtuluLog> future : invokeAll) {
        group.addLog(future.get());
      }
    } catch (Exception ex) {
      Exceptions.printStackTrace(ex);
    } finally {
      if (newFixedThreadPool != null) {
        newFixedThreadPool.shutdownNow();
      }
    }
    return group;
  }
}
