/*
 GPL 2
 */
package org.fudaa.fudaa.crue.sysdoc;

import java.util.regex.Pattern;
import org.jdesktop.swingx.JXFindBar;
import org.jdesktop.swingx.search.Searchable;

/**
 *
 * @author Frederic Deniger
 */
public class CustomFindBar extends JXFindBar {

  public CustomFindBar() {
  }

  CustomFindBar(Searchable searchable) {
    super(searchable);
  }

  String getText() {
    return super.searchField.getText();
  }

  Pattern getPattern() {
    return super.getPatternModel().getPattern();
  }

  void setText(String text) {
    super.searchField.setText(text);
  }
}
