/*
 GPL 2
 */
package org.fudaa.fudaa.crue.sysdoc.node;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.dodico.crue.sysdoc.chunck.ChunckProcessor;
import org.fudaa.dodico.crue.sysdoc.common.DocEntry;
import static org.fudaa.fudaa.crue.sysdoc.node.Bundle.*;
import org.fudaa.fudaa.crue.sysdoc.services.SysdocService;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.util.actions.NodeAction;

/**
 *
 * @author Frederic Deniger
 */
public class ChunckHtmlNodeAction extends NodeAction {

  private final transient SysdocService sysdocService = Lookup.getDefault().lookup(SysdocService.class);

  public ChunckHtmlNodeAction() {
    final ImageIcon loadImageIcon = ImageUtilities.loadImageIcon("org/fudaa/fudaa/crue/sysdoc/icons/browser.png", false);
    setIcon(loadImageIcon);
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected void performAction(final Node[] activatedNodes) {
    if (ArrayUtils.isEmpty(activatedNodes)) {
      return;
    }
    final DocEntry main = getDocEntry(activatedNodes);
    if (main.getChildren().isEmpty()) {
      final File html = new File(sysdocService.getSysdocLocalizedDir(), main.getPath());
      if (html.exists()) {
        try {
          Desktop.getDesktop().browse(html.toURI());
        } catch (final IOException ex) {
          Exceptions.printStackTrace(ex);
        }
      }
      return;
    }
    final ProgressHandle handle = ProgressHandleFactory.createHandle(getName());
    handle.start();
    handle.switchToIndeterminate();
    final Runnable runnable = () -> {
      try {
        final File targetFile = File.createTempFile("all", ".html");
        final ChunckProcessor processor = new ChunckProcessor();
        processor.chunck(sysdocService.getSysdocLocalizedDir(), main, targetFile);
        Desktop.getDesktop().browse(targetFile.toURI());
      } catch (final IOException iOException) {
        Logger.getLogger(ChunckHtmlNodeAction.class.getName()).log(Level.INFO, "message {0}", iOException);
      }
      handle.finish();
    };
    new Thread(runnable).start();
  }

  @Override
  protected boolean enable(final Node[] activatedNodes) {
    return activatedNodes.length > 0;
  }

  @Override
  @Messages("ChunkAction.Name=Version imprimable")
  public String getName() {
    return ChunkAction_Name();
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  private DocEntry getDocEntry(final Node[] activatedNodes) {
    DocEntry main;
    if (activatedNodes.length == 1) {
      main = activatedNodes[0].getLookup().lookup(DocEntry.class);
    } else {
      main = new DocEntry();
      for (final Node node : activatedNodes) {
        main.addChild(node.getLookup().lookup(DocEntry.class));
      }
    }
    return main;
  }
}
