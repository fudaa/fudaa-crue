/*
 GPL 2
 */
package org.fudaa.fudaa.crue.sysdoc.services;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

@ActionID(
        category = "Help",
        id = "org.fudaa.fudaa.crue.sysdoc.services.SysdocAction")
@ActionRegistration(displayName = "#CTL_SysdocAction")
@ActionReferences(value = {
  @ActionReference(path = "Menu/Help", position = 1),
  @ActionReference(path = "Shortcuts", name = "F1")})
@Messages("CTL_SysdocAction=Aide F1")
public final class SysdocAction implements ActionListener {

  private final SysdocService sysdocService = Lookup.getDefault().lookup(SysdocService.class);
  private final SelectedPerspectiveService selectedPerspectiveService = Lookup.getDefault().lookup(SelectedPerspectiveService.class);

  @Override
  public void actionPerformed(ActionEvent e) {
    String url = null;
    TopComponent activated = WindowManager.getDefault().getRegistry().getActivated();
    if (activated != null) {
      HelpCtx helpCtx = activated.getHelpCtx();
      if (helpCtx != null) {
        url = helpCtx.getHelpID();
      }
    }
    if (url == null) {
      PerspectiveEnum perspective = selectedPerspectiveService.getCurrentPerspectiveService().getPerspective();
      url = SysdocUrlBuilder.getPerspectiveHelpCtxId(perspective);
    }
    sysdocService.display(url);
  }
}
