/*
 GPL 2
 */
package org.fudaa.fudaa.crue.sysdoc;

import org.openide.nodes.Node;

/**
 * @author Frederic Deniger
 */
class SearchResultat {
  private Node node;
  private int score;

  Node getNode() {
    return node;
  }

  void setNode(Node node) {
    this.node = node;
  }

  int getScore() {
    return score;
  }

  void setScore(int score) {
    this.score = score;
  }
}
