/*
 GPL 2
 */
package org.fudaa.fudaa.crue.sysdoc;

import org.jdesktop.swingx.search.Searchable;
import org.openide.util.Exceptions;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Document;
import javax.swing.text.Segment;
import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Frederic Deniger
 */
public class EditorSearchable implements Searchable {
    private final JEditorPane editor;
    private final DefaultHighlighter defaultHighlighter;
    private final DefaultHighlighter.DefaultHighlightPainter defaultHighlightPainter = new DefaultHighlighter.DefaultHighlightPainter(Color.ORANGE);
    private boolean highlightEnabled;
    private int lastFoundIndex = -1;
    private MatchResult lastMatchResult;
    private String lastRegEx;

    public EditorSearchable(JEditorPane editor) {
        this.editor = editor;
        defaultHighlighter = new DefaultHighlighter();
        editor.setHighlighter(defaultHighlighter);
    }

    public boolean isHighlightEnabled() {
        return highlightEnabled;
    }

    public void setHighlightEnabled(boolean highlightEnabled) {
        clearHighlight();
        this.highlightEnabled = highlightEnabled;
    }

    private void clearHighlight() {
        defaultHighlighter.removeAllHighlights();
    }

    @Override
    public int search(String searchString) {
        highlight(searchString);
        return search(searchString, -1);
    }

    @Override
    public int search(String searchString, int columnIndex) {
        return search(searchString, columnIndex, false);
    }

    @Override
    public int search(String searchString, int columnIndex, boolean backward) {
        Pattern pattern = createPattern(searchString);
        return search(pattern, columnIndex, backward);
    }

    /**
     * checks if the searchString should be interpreted as empty. here: returns true if string is null or has zero length.
     *
     * @param searchString la chaine a rechercher
     * @return true if string is null or has zero length
     */
    private boolean isEmpty(String searchString) {
        return (searchString == null) || searchString.length() == 0;
    }

    @Override
    public int search(Pattern pattern) {
        return search(pattern, -1);
    }

    @Override
    public int search(Pattern pattern, int startIndex) {
        return search(pattern, startIndex, false);
    }

    /**
     * @return start position of matching string or -1
     */
    @Override
    public int search(Pattern pattern, final int startIndex,
                      boolean backwards) {
        highlight(pattern);
        if ((pattern == null)
                || (getDocument().getLength() == 0)
                || ((startIndex > -1) && (getDocument().getLength() < startIndex))) {
            updateStateAfterNotFound();
            return -1;
        }

        int start = startIndex;
        if (maybeExtendedMatch(startIndex)) {
            if (foundExtendedMatch(pattern, start)) {
                return lastFoundIndex;
            }
            start++;
        }

        int length;
        if (backwards) {
            start = 0;
            if (startIndex < 0) {
                length = getDocument().getLength() - 1;
            } else {
                length = -1 + startIndex;
            }
        } else {
            // start = startIndex + 1;
            if (start < 0) {
                start = 0;
            }
            length = getDocument().getLength() - start;
        }
        Segment segment = new Segment();

        try {
            getDocument().getText(start, length, segment);
        } catch (BadLocationException ex) {
            Logger.getLogger(EditorSearchable.class.getName()).log(Level.FINE,
                    "this should not happen (calculated the valid start/length) ", ex);
        }

        Matcher matcher = pattern.matcher(segment.toString());
        MatchResult currentResult = getMatchResult(matcher, !backwards);
        if (currentResult != null) {
            updateStateAfterFound(currentResult, start);
        } else {
            updateStateAfterNotFound();
        }
        return lastFoundIndex;
    }

    private void highlight(String search) {
        if (!highlightEnabled) {
            return;
        }
        Pattern compile = createPattern(search);
        highlight(compile);
    }

    private int highlight(Pattern pattern, final int startIndex) {
        if ((pattern == null)
                || (getDocument().getLength() == 0)
                || ((startIndex > -1) && (getDocument().getLength() < startIndex))) {
            updateStateAfterNotFound();
            return -1;
        }

        int start = startIndex;
        // start = startIndex + 1;
        if (start < 0) {
            start = 0;
        }
        int length = getDocument().getLength() - start;
        Segment segment = new Segment();

        try {
            getDocument().getText(start, length, segment);
        } catch (BadLocationException ex) {
            Logger.getLogger(EditorSearchable.class.getName()).log(Level.FINE,
                    "this should not happen (calculated the valid start/length) ", ex);
        }

        Matcher matcher = pattern.matcher(segment.toString());
        MatchResult currentResult = getMatchResult(matcher, true);
        if (currentResult != null) {
            int end = currentResult.end() + start;
            int found = currentResult.start() + start;
            try {
                defaultHighlighter.addHighlight(found, end, defaultHighlightPainter);
            } catch (BadLocationException ex) {
                Exceptions.printStackTrace(ex);
            }
            return found;
        }
        return -1;
    }

    /**
     * Search from same startIndex as the previous search. Checks if the match is different from the last (either extended/reduced) at the same
     * position. Returns true if the current match result represents a different match than the last, false if no match or the same.
     *
     * @param pattern le pattern recherche
     * @param start   l'indice de depart
     * @return true if the current match result represents a different match than the last, false if no match or the same.
     */
    private boolean foundExtendedMatch(Pattern pattern, int start) {
        // JW: logic still needs cleanup...
        if (pattern.pattern().equals(lastRegEx)) {
            return false;
        }
        int length = getDocument().getLength() - start;
        Segment segment = new Segment();

        try {
            getDocument().getText(start, length, segment);
        } catch (BadLocationException ex) {
            Logger.getLogger(EditorSearchable.class.getName()).log(Level.FINE, "this should not happen (calculated the valid start/length) ", ex);
        }
        Matcher matcher = pattern.matcher(segment.toString());
        MatchResult currentResult = getMatchResult(matcher, true);
        if (currentResult != null) {
            // JW: how to compare match results reliably?
            // the group().equals probably isn't the best idea...
            // better check pattern?
            if ((currentResult.start() == 0)
                    && (!lastMatchResult.group().equals(currentResult.group()))) {
                updateStateAfterFound(currentResult, start);
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the startIndex is a candidate for trying a re-match.
     *
     * @param startIndex indice de depart
     * @return true if the startIndex should be re-matched, false if not.
     */
    private boolean maybeExtendedMatch(final int startIndex) {
        return (startIndex >= 0) && (startIndex == lastFoundIndex);
    }

    /**
     * @param currentResult le resultat en cours
     * @param offset        offset pour la recherche
     * @return the start position of the selected text
     */
    private int updateStateAfterFound(MatchResult currentResult, final int offset) {
        int end = currentResult.end() + offset;
        int found = currentResult.start() + offset;
        editor.select(found, end);
        editor.getCaret().setSelectionVisible(true);
        lastFoundIndex = found;
        lastMatchResult = currentResult;
        lastRegEx = ((Matcher) lastMatchResult).pattern().pattern();
        return found;
    }

    /**
     * @param matcher  le matcher
     * @param useFirst whether or not to return after the first match is found.
     * @return <code>MatchResult</code> or null
     */
    private MatchResult getMatchResult(Matcher matcher, boolean useFirst) {
        MatchResult currentResult = null;
        while (matcher.find()) {
            currentResult = matcher.toMatchResult();
            if (useFirst) {
                break;
            }
        }
        return currentResult;
    }

    /**
     */
    private void updateStateAfterNotFound() {
        lastFoundIndex = -1;
        lastMatchResult = null;
        lastRegEx = null;
        editor.setCaretPosition(editor.getSelectionEnd());
    }

    private Document getDocument() {
        return editor.getDocument();
    }

    private Pattern createPattern(String searchString) {
        Pattern pattern = null;
        if (!isEmpty(searchString)) {
            pattern = Pattern.compile(searchString, 0);
        }
        return pattern;
    }

    protected void highlight(Pattern compile) {
        if (!highlightEnabled) {
            return;
        }
        clearHighlight();
        int idx = highlight(compile, -1);
        while (idx > 0) {
            idx = highlight(compile, idx + 1);
        }
    }
}
