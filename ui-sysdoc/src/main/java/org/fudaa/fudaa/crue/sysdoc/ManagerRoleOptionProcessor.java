/*
 GPL 2
 */
package org.fudaa.fudaa.crue.sysdoc;

import org.fudaa.fudaa.crue.sysdoc.admin.SysdocAdminUI;
import org.netbeans.api.sendopts.CommandException;
import org.netbeans.spi.sendopts.Env;
import org.netbeans.spi.sendopts.Option;
import org.netbeans.spi.sendopts.OptionProcessor;
import org.openide.util.lookup.ServiceProvider;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * {@link OptionProcessor} pour ouvrir la vue Sydoc en mode admin
 *
 * @author Frederic Deniger
 */
@ServiceProvider(service = OptionProcessor.class)
public class ManagerRoleOptionProcessor extends OptionProcessor {
    private final Option openAsManager = Option.withoutArgument(Option.NO_SHORT_NAME, "sydoc-admin");

    /**
     * @return sydoy-admin
     */
    @Override
    protected Set<Option> getOptions() {
        HashSet<Option> options = new HashSet<>();
        options.add(openAsManager);
        return options;
    }

    /**
     * @param env          l'environnement
     * @param optionValues les options
     * @see SysdocAdminUI#activeRole()
     */
    @Override
    protected void process(Env env, Map<Option, String[]> optionValues) throws CommandException {
        if (optionValues.containsKey(openAsManager)) {
            SysdocAdminUI.activeRole();
        }
    }
}
