/*
 GPL 2
 */
package org.fudaa.fudaa.crue.sysdoc.services;

import java.io.File;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;
import org.fudaa.dodico.crue.config.cr.CrueLogTranslator;
import org.fudaa.dodico.crue.config.cr.CrueTranslationReaderWriter;
import org.fudaa.dodico.crue.config.cr.LienDocumentation;
import static org.fudaa.fudaa.crue.sysdoc.services.Bundle.*;
import org.openide.util.NbBundle.Messages;

/**
 *
 * @author Frederic Deniger
 */
public class SysdocMsgChecker {

  private final CoeurConfig coeurConfig;
  private final String language;
  private final File sysdocDir;

  public SysdocMsgChecker(CoeurConfig coeurConfig, String language, File sysdocDir) {
    this.coeurConfig = coeurConfig;
    this.language = language;
    this.sysdocDir = sysdocDir;
  }

  @Messages({
    "CheckMessage.LogName=V\u00e9rification des messages du coeur {0} pour la langue {1}",
    "CheckMessage.FileNotExists=Le fichier de message n''existe pas: {0}",
    "CheckMessage.CantReadMsgFile=Le fichier contenant les messages ne peut pas \u00eatre lu. La v\u00e9rification n''a pas \u00e9t\u00e9 effectu\u00e9e. Fichier {0}",
    "CheckMessage.FileNotFound=Le lien de documentation pour {0} est inexistant {1}"
  })
  public CtuluLog check() {
    CtuluLog log = new CtuluLog(null);
    log.setDesc(CheckMessage_LogName(coeurConfig.getName(), language));
    final File messageURL = coeurConfig.getMessageURLFile(language);
    if (!messageURL.exists()) {
      log.addSevereError(CheckMessage_FileNotExists(messageURL.getAbsolutePath()));
    } else {
      CtuluLog translationLog = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
      CrueTranslationReaderWriter reader = new CrueTranslationReaderWriter();
      CrueIOResu<CrueLogTranslator> res = reader.readXML(messageURL, translationLog);
      CrueLogTranslator metier = res.getMetier();
      if (translationLog.isNotEmpty()) {
        translationLog.updateLocalizedMessage(BusinessMessages.RESOURCE_BUNDLE);
        log.addAllLogRecord(translationLog);
      }
      if (metier == null) {
        log.addSevereError(CheckMessage_CantReadMsgFile(coeurConfig.getMessageURLFile(language)));
      } else {
        List<String> ids = metier.getIds();
        Collections.sort(ids);
        for (String id : ids) {
          LienDocumentation lienDocumentation = metier.getLienDocumentation(id);
          if (lienDocumentation == null) {
            continue;
          }
          String hRef = lienDocumentation.getHRef();
          File htmlFile = new File(sysdocDir, hRef);
          if (!htmlFile.exists()) {
            final String absolutePath = htmlFile.getAbsolutePath();
            log.addError(CheckMessage_FileNotFound(id, absolutePath));
          }
        }
      }

    }
    return log;

  }
}
