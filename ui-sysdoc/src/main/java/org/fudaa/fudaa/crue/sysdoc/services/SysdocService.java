/*
 GPL 2
 */
package org.fudaa.fudaa.crue.sysdoc.services;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.projet.conf.AideEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocContrat;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.fudaa.fudaa.crue.sysdoc.SysdocTopComponent;
import org.netbeans.api.javahelp.Help;
import org.openide.modules.Places;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import org.openide.windows.WindowManager;

import javax.swing.event.ChangeListener;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.fudaa.fudaa.crue.sysdoc.services.Bundle.Sysdoc_DisplayMediaNotFound;

/**
 * Service principal pour Sydoc. Permet aussi d'implementer {@link Help} et {@link HelpCtx.Displayer}
 * Pas de lookup intene
 *
 * @author Frederic Deniger
 */
@ServiceProviders(value = {
        @ServiceProvider(service = Help.class, supersedes = {"org.netbeans.modules.javahelp.JavaHelp"}),
        @ServiceProvider(service = HelpCtx.Displayer.class),
        @ServiceProvider(service = SysdocService.class),
        @ServiceProvider(service = SysdocContrat.class)})
public class SysdocService extends Help implements SysdocContrat, HelpCtx.Displayer {
    private static final String DEFAULT_LINK = "DEFAUT";
    private static final String HTML_CODE = "<!DOCTYPE HTML>\n" + "<html>\n"
            + "    <head>\n"
            + "        <meta charset=\"UTF-8\">\n"
            + "        <meta http-equiv=\"refresh\" content=\"1;url=@@URL@@\">\n"
            + "        <title>Fudaa Crue</title>\n"
            + "    </head>\n"
            + "    <body>\n"
            + "        Suivez ce lien pour afficher la documentation <a href='@@URL@@'>Aide FudaaCrue</a>\n"
            + "    </body>\n"
            + "</html>";
    private static HashMap<String, String> helpIdLink = null;
    private final InstallationService installationService = Lookup.getDefault().lookup(InstallationService.class);
    private final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

    /**
     * Construit l'URL pour accéder à l'aide en ligne correspondante à helpId
     *
     * @param helpId identifiant de l'aide
     */
    private String getHelpURL(String helpId) {
        String locale = configurationManagerService.getCurrentLocale();
        final String cheminBase = configurationManagerService.getOptionsManager().getAideOption().getCheminBase();
        if(StringUtils.isBlank(cheminBase)){
            return "";
        }
        String path = String.format(cheminBase, locale);

        if (configurationManagerService.getOptionsManager().getAideOption().getType().equals(AideEnum.RELATIVE.toString())) {
            try {

                File pathSiteDir = installationService.getSiteDir();
                //Recherche du lecteur
                String drive = "";
                if (pathSiteDir.getCanonicalPath().length() > 1 && pathSiteDir.getCanonicalPath().charAt(1) == ':') {
                    drive = pathSiteDir.getCanonicalPath().substring(0, 1);
                }
                //Commande Net Use pour recuperer le nom du serveur pour generer le chemin UNC
                String[] command = new String[3];
                command[0] = "net";
                command[1] = "use";
                command[2] = drive + ":";
                Process p = Runtime.getRuntime().exec(command);

                BufferedReader err = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                String lineErr;
                while ((lineErr = err.readLine()) != null) {
                    Logger.getLogger(getClass().getName()).log(Level.WARNING,lineErr);
                }


                String netPath = "";
                try( BufferedReader read = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
                    String line = read.readLine();

                    while (line != null) {
                        if (line.contains("\\\\")) {
                            netPath = line.substring(line.indexOf("\\\\")).substring(2);
                            break;
                        }
                        line = read.readLine();
                    }
                }
                path = "/" + path;
                if (netPath.length() > 0) {
                    String pathConv = installationService.getSiteDir().getCanonicalPath().replace(drive + ":", netPath);
                    if ((System.getenv("USERDNSDOMAIN") != null && System.getenv("USERDOMAIN") != null) && pathConv.toLowerCase().contains(System.getenv(
                            "USERDNSDOMAIN").toLowerCase())) {
                        pathConv = pathConv.toLowerCase().replace(System.getenv("USERDNSDOMAIN").toLowerCase(), System.getenv("USERDOMAIN"));
                    }
                    path = "file://" + pathConv + path + getLink(helpId);
                } else {
                    path = "file://" + installationService.getSiteDir().getCanonicalPath() + path + getLink(helpId);
                }
                path = path.replace("\\", "/");
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        } else {
            path = path + getLink(helpId);
        }

        return path;
    }

    /**
     * Fournit le lien correspondant à l'ID interne FudaaCrue
     *
     * @param helpId identifiant de l'aide
     */
    private String getLink(String helpId) {
        String link = "";
        if (helpIdLink == null) {
            helpIdLink = configurationManagerService.readAide();
        }

        if (helpIdLink != null && helpIdLink.containsKey(helpId)) {
            link = helpIdLink.get(helpId);
        } else if (helpIdLink != null && helpIdLink.containsKey(DEFAULT_LINK)) {
            link = helpIdLink.get(DEFAULT_LINK);
        }
        return link;
    }

    /**
     * @return Boolean#TRUE
     */
    @Override
    public Boolean isValidID(String string, boolean bln) {
        return Boolean.TRUE;
    }

    /**
     * @param helpCtx utiliser pour récupéer le {@link HelpCtx#getHelpID()}
     * @param bln     ?
     * @see #display(String)
     */
    @Override
    public void showHelp(HelpCtx helpCtx, boolean bln) {
        display(helpCtx.getHelpID());
    }

    /**
     * @param helpCtx utiliser pour récupéer le {@link HelpCtx#getHelpID()}
     * @see #display(String)
     */
    @Override
    public boolean display(HelpCtx helpCtx) {
        display(helpCtx.getHelpID());
        return true;
    }

    /**
     * Appelle la classe parentes
     *
     * @param helpCtx utiliser pour récupéer le {@link HelpCtx#getHelpID()}
     */
    @Override
    public void showHelp(HelpCtx helpCtx) {
        super.showHelp(helpCtx);
    }

    /**
     * ne fait rien
     *
     * @param changeListener le listener
     */
    @Override
    public void addChangeListener(ChangeListener changeListener) {
    }

    /**
     * ne fait rien
     *
     * @param changeListener le listener
     */
    @Override
    public void removeChangeListener(ChangeListener changeListener) {
    }

    /**
     * @param path le path pour lequel l'aide doit être affiché
     */
    @Override
    public void display(String path) {

        if (configurationManagerService.getOptionsManager().getAideOption().getSyDocActivation()) {
            String lienHelp = getLink(path);
            SysdocTopComponent findTopComponent = (SysdocTopComponent) WindowManager.getDefault().findTopComponent(SysdocTopComponent.TOPCOMPONENT_ID);
            if (lienHelp != null) {
                findTopComponent.display(lienHelp);
            }
            if (!findTopComponent.isOpened()) {
                findTopComponent.open();
            }
            findTopComponent.requestActive();
        } else {
            try {
                String pageRedirection = HTML_CODE.replaceAll("@@URL@@", this.getHelpURL(path));
                if (installationService.getUserDir() != null) {
                    File userDirectory = Places.getUserDirectory();
                    FileUtils.writeStringToFile(new File(userDirectory.getCanonicalPath() + "/var/temp/aide.htm"), pageRedirection);
                    URL HelpUrl = new URL("file:///" + userDirectory.getCanonicalPath() + "/var/temp/aide.htm");
                    Desktop.getDesktop().browse(HelpUrl.toURI());
                } else {
                    Logger.getLogger(SysdocService.class.getName()).log(Level.WARNING, "Repertoire utilisateur non défini.");
                }
            } catch (Exception ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    /**
     * Afiche le média dans le desktop
     *
     * @param helpUrl l'url absolue de l'aide
     */
    @Override
    @Messages("Sysdoc.DisplayMediaNotFound=Le m\u00e9dia n''est pas trouv\u00e9. Chemin: {0}")
    public void displayMedia(String helpUrl) {
        if (helpUrl.startsWith("http")) {
            try {
                Desktop.getDesktop().browse(new URI(helpUrl));
            } catch (Exception ex) {
                Exceptions.printStackTrace(ex);
            }
            return;
        }
        File sysdocLocaleDir = getSysdocLocalizedDir();
        File target = new File(sysdocLocaleDir, helpUrl);
        if (!target.exists()) {
            DialogHelper.showError(Sysdoc_DisplayMediaNotFound(target.getAbsolutePath()));
        } else {
            try {
                Desktop.getDesktop().open(target);
            } catch (IOException ex) {
                Logger.getLogger(SysdocService.class.getName()).log(Level.WARNING, "message {0}", ex);
            }
        }
    }

    /**
     * @return le repertoire localisé ( en/fr) de sydoc
     */
    public File getSysdocLocalizedDir() {
        File sysdocBaseDir = new File(configurationManagerService.getCoeurManager().getSiteDir(), "sydoc");
        String local = configurationManagerService.getCurrentLocale();
        return new File(sysdocBaseDir, local);
    }

    /**
     * @return true Flag si  activation de SyDoc
     */
    @Override
    public boolean isSyDocActivated() {
        return configurationManagerService.getOptionsManager().getAideOption().getSyDocActivation();
    }
}
