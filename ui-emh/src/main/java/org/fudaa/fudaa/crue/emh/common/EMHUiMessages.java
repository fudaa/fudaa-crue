package org.fudaa.fudaa.crue.emh.common;

import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class EMHUiMessages {
  public static final String EMH_NON_ACTIVE = " " + org.openide.util.NbBundle.getMessage(TreeNodeEMH.class, "EMHTreeView.EMHNonActive");
  public static final String EMH_NON_ACTIVE_USER = " " + org.openide.util.NbBundle.getMessage(TreeNodeEMH.class, "EMHTreeView.EMH.nonActiveByUser");

  public static String getMessage(String code) {
    return NbBundle.getMessage(EMHUiMessages.class, code);
  }
  
  public static String getMessage(String code,String value){
    return NbBundle.getMessage(EMHUiMessages.class, code,value);
  }
}
