/*
 GPL 2
 */
package org.fudaa.fudaa.crue.emh.node;

import java.awt.Color;
import java.awt.Component;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import org.openide.nodes.Node;

/**
 *
 * @author Frederic Deniger
 */
public class LinkedEMHTableCellRenderer implements TableCellRenderer {

  private final TableCellRenderer init;

  public LinkedEMHTableCellRenderer(TableCellRenderer init) {
    this.init = init;
  }

  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component comp = init.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    if (value != null) {
      Node.Property property = (Node.Property) value;
      if (Boolean.TRUE.equals(property.getValue(NodeEMHDefault.PROP_LINK))) {
        Color htmlColor = table.getForeground();
        if (isSelected) {
          htmlColor = table.getSelectionForeground();
        } else {
          htmlColor = Color.BLUE;
        }
        try {
          String hexred = Integer.toHexString(htmlColor.getRed());
          String hexgreen = Integer.toHexString(htmlColor.getGreen());
          String hexblue = Integer.toHexString(htmlColor.getBlue());
          if ("0".equals(hexred)) {
            hexred = "00";
          }
          if ("0".equals(hexgreen)) {
            hexgreen = "00";
          }
          if ("0".equals(hexblue)) {
            hexblue = "00";
          }
          String html = "<u><font color=\"#" + hexred + hexgreen + hexblue + "\">" + property.getValue() + "</font></u>";
          property.setValue("htmlDisplayValue", html);
        } catch (Exception illegalAccessException) {
          Logger.getLogger(LinkedEMHTableCellRenderer.class.getName()).log(Level.INFO, "message {0}", illegalAccessException);
        }
      }
    }
    return comp;
  }
}
