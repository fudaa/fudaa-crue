package org.fudaa.fudaa.crue.emh.node;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertyStringReadOnly;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.emh.common.EMHUiMessages;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class NodeEMHDefault extends AbstractNode {

  public static final String PROP_VALUE = "Value";
  public static final String PROP_TYPE = "Type";
  public static final String PROP_LINK = "LINK";

  /**
   * Avec ce constructeur, la propriété value représentera le nom de l'EMH
   *
   * @param children
   * @param emh
   * @param displayName la valeur a utiliser pour le nom du noeud
   */
  public NodeEMHDefault(Children children, EMH emh, String displayName) {
    super(children, Lookups.fixed(emh, displayName));
    setDisplayName(displayName);
  }

  public NodeEMHDefault(Children children, EMH emh) {
    super(children, Lookups.singleton(emh));
    setDisplayName(emh.getNom());
  }

  public boolean isLinkableNode() {
    return getLookup().lookup(String.class) != null;
  }

  @Override
  public HelpCtx getHelpCtx() {
    EMH lookup = getLookup().lookup(EMH.class);
    if (lookup != null) {
      return new HelpCtx(SysdocUrlBuilder.getEMHHelpCtxId(lookup));
    }
    return super.getHelpCtx();
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    sheet.put(set);
    EMH emh = getLookup().lookup(EMH.class);
    String name = getLookup().lookup(String.class);
    String propertyName = PROP_VALUE;
    String suffix = StringUtils.EMPTY;
    if (!emh.getActuallyActive()) {
      suffix = EMHUiMessages.EMH_NON_ACTIVE;
    }
    if (name != null) {
      propertyName = PROP_TYPE;
      String propertyDisplayName = NbBundle.getMessage(NodeEMHDefault.class, "EMH.NameDescription.Name");
      PropertyStringReadOnly prop = new PropertyStringReadOnly(emh.getNom() + suffix, PROP_VALUE,
              propertyDisplayName,
              propertyDisplayName);
      prop.setValue(PROP_LINK, Boolean.TRUE);
      prop.setValue("htmlDisplayValue", "<u>" + emh.getNom() + suffix + "</u>");
      PropertyCrueUtils.configureNoCustomEditor(prop);
      set.put(prop);
    }
    String displayName = NbBundle.getMessage(NodeEMHDefault.class, "EMH.TypeDescription.Name");
    PropertyStringReadOnly prop = new PropertyStringReadOnly(emh.getLongTypei18n() + suffix, propertyName,
            displayName, displayName);
    PropertyCrueUtils.configureNoCustomEditor(prop);
    set.put(prop);
    List<EMHSousModele> parents = EMHHelper.getParents(emh);
    if (!parents.isEmpty()) {
      int idx = 0;
      displayName = NbBundle.getMessage(NodeEMHDefault.class, "EMH.SouModeleParent.Name");
      for (EMHSousModele ssModele : parents) {
        PropertyStringReadOnly propParent = new PropertyStringReadOnly(ssModele.getNom(), "parent." + (idx++),
                displayName, displayName);
        PropertyCrueUtils.configureNoCustomEditor(propParent);
        set.put(propParent);
      }
    }


    return sheet;
  }
}
