package org.fudaa.fudaa.crue.emh;

import com.memoire.bu.*;
import org.apache.commons.beanutils.LazyDynaMap;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.PopupMenuReceiver;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.result.Crue9ResultatCalculPasDeTemps;
import org.fudaa.dodico.crue.metier.result.OrdResDynamicPropertyFilter;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformerPoint2d;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.fudaa.crue.common.UserPreferencesSaver;
import org.fudaa.fudaa.crue.common.helper.JXTreeTableExtended;
import org.fudaa.fudaa.crue.common.helper.UiContext;
import org.fudaa.fudaa.crue.common.swing.CommonGuiLib;
import org.fudaa.fudaa.crue.common.swingx.AbstractTreeNode;
import org.fudaa.fudaa.crue.common.swingx.TreeNodeCleValeur;
import org.fudaa.fudaa.crue.emh.common.EMHManagerBuilder;
import org.fudaa.fudaa.crue.emh.common.EMHUiMessages;
import org.fudaa.fudaa.crue.emh.common.TableSelectionHistory;
import org.fudaa.fudaa.crue.emh.common.TreeNodeEMH;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.AbstractHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.TreeTableNode;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.NumberFormat;
import java.util.List;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Affichage rapide des EMH.
 *
 * @author Adrien Hadoux
 */
public class EMHTreePanel {

  private static final Color BLUE_DARKER = Color.BLUE.darker();
  /**
   *
   */
  private static final long serialVersionUID = 4662472594024929109L;
  private JXTreeTableExtended treeEMH;
  private final EMHProjet projet;
  private final EMHScenario scenarioLoaded;

  /**
   * @param projet
   */
  public EMHTreePanel(final EMHProjet projet, final EMHScenario scenarioLoaded) {
    this.projet = projet;
    this.scenarioLoaded = scenarioLoaded;

  }

  public void saveSize() {
    UserPreferencesSaver.saveTablePreferences(treeEMH);
  }

  public JXTreeTableExtended getTreeEMH() {
    return treeEMH;
  }

  private void displayInfo(final TreePath treePath) {
    if (treePath == null || !(treePath.getLastPathComponent() instanceof TreeNodeCleValeur)) {
      return;
    }
    final AbstractTreeNode r = ((AbstractTreeNode) (treePath.getLastPathComponent()));

    final Object val = r.getUserObject();
    if (val instanceof Loi) {
      showLoi((Loi) val);
    } else if (val instanceof DonFrt) {
      showFrottement((DonFrt) val);
    } else if (val instanceof List) {
      showPtProfil((List<PtProfil>) val);
    } else if (val instanceof ResultatPasDeTempsDelegate) {
      showPasDeTemps((ResultatPasDeTempsDelegate) val);
    } else if (val instanceof ResultatPasDeTemps) {
      showPasDeTemps(((ResultatPasDeTemps) val).getDelegate());
    } else if (val instanceof ResultatCalcul) {
      showResulat((ResultatCalcul) val);
    }

  }

  private static class FormattedCellRenderer extends CtuluCellTextRenderer {

    final NumberFormat fmt;

    /**
     * @param fmt
     */
    public FormattedCellRenderer(final NumberFormat fmt) {
      super();
      this.fmt = fmt;
    }

    @Override
    protected void setValue(final Object value) {
      super.setValue(fmt == null ? value : fmt.format(value));
    }
  }

  private static class Point2dXCellRenderer extends CtuluCellTextRenderer {

    final ToStringTransformerPoint2d fmt;

    /**
     * @param fmt
     */
    public Point2dXCellRenderer(final ToStringTransformerPoint2d fmt) {
      super();
      this.fmt = fmt;
    }

    @Override
    protected void setValue(final Object value) {
      super.setValue(fmt == null ? value : fmt.formatX(((Double) value).doubleValue()));
    }
  }

  private static class Point2dYCellRenderer extends CtuluCellTextRenderer {

    final ToStringTransformerPoint2d fmt;

    /**
     * @param fmt
     */
    public Point2dYCellRenderer(final ToStringTransformerPoint2d fmt) {
      super();
      this.fmt = fmt;
    }

    @Override
    protected void setValue(final Object value) {
      super.setValue(fmt == null ? value : fmt.formatY(((Double) value).doubleValue()));
    }
  }

  private static class LoiTableModel extends AbstractTableModel {

    final Loi loi;
    final String nameX;
    final String nameY;

    /**
     * @param loi
     * @param properties
     */
    public LoiTableModel(final Loi loi, final String nomX, final String nomY) {
      super();
      this.loi = loi;
      nameX = nomX;
      nameY = nomY;
    }

    @Override
    public int getColumnCount() {
      return 2;
    }

    @Override
    public Class<?> getColumnClass(final int columnIndex) {
      return Double.class;
    }

    @Override
    public String getColumnName(final int column) {
      if (column == 0) {
        return nameX;
      }
      return nameY;
    }

    @Override
    public int getRowCount() {
      return loi.getEvolutionFF().getPtEvolutionFF().size();
    }

    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
      final PtEvolutionFF pt = loi.getEvolutionFF().getPtEvolutionFF().get(rowIndex);
      return Double.valueOf(columnIndex == 0 ? pt.getAbscisse() : pt.getOrdonnee());
    }

    @Override
    public boolean isCellEditable(final int rowIndex, final int columnIndex) {
      return false;
    }
  }

  private static class PasDeTempsModel extends AbstractTableModel {

    final ResultatPasDeTempsDelegate pdt;
    final boolean isCrue9;
    final CrueConfigMetier properties;
    final NumberFormat fmtPdt;
    NumberFormat fmtRui;
    final List<ResultatTimeKey> keys = new ArrayList<>();

    /**
     * @param loi
     * @param properties
     */
    public PasDeTempsModel(final ResultatPasDeTempsDelegate loi, final CrueConfigMetier properties) {
      super();
      this.pdt = loi;
      isCrue9 = loi instanceof Crue9ResultatCalculPasDeTemps;
      this.properties = properties;
      fmtPdt = properties.getFormatter("pdt", DecimalFormatEpsilonEnum.COMPARISON);
      keys.addAll(loi.getResultatKeys());
    }

    @Override
    public int getColumnCount() {
      return isCrue9 ? 3 : 2;
    }

    @Override
    public String getColumnName(final int column) {
      if (column == 0) {
        return "Nom";
      }
      if (column == 1) {
        return "Temps";
      }
      return "Rui";
    }

    @Override
    public int getRowCount() {
      return pdt.getNbResultats();
    }

    private ResultatTimeKey getKey(final int rowIndex) {
      return keys.get(rowIndex);
    }

    private NumberFormat getRuiFmt() {
      if (fmtRui == null) {
        return properties.getFormatter("qruis", DecimalFormatEpsilonEnum.COMPARISON);
      }
      return fmtRui;
    }

    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
      final ResultatTimeKey key = getKey(rowIndex);
      if (columnIndex == 0) {
        return key.getNomCalcul();
      }
      if (columnIndex == 1) {
        final long duree = key.getDuree();
        if (duree >= 0) {
          return key.getTemps();
        }
      } else {
        return StringUtils.EMPTY;
      }
      if (isCrue9 && columnIndex == 2) {
        final Crue9ResultatCalculPasDeTemps crue9 = (Crue9ResultatCalculPasDeTemps) pdt;
        final int resultatPosition = crue9.getIndex(key);
        if (crue9.containsRui(resultatPosition)) {
          return getRuiFmt().format(crue9.getRuinou(resultatPosition));
        }
      }
      return StringUtils.EMPTY;

    }

    @Override
    public boolean isCellEditable(final int rowIndex, final int columnIndex) {
      return false;
    }
  }

  @SuppressWarnings("serial")
  private static class PtProfilTableModel extends AbstractTableModel {

    final List<PtProfil> loi;
    final String colYName;
    final String colXName;

    /**
     * @param loi
     * @param properties
     */
    public PtProfilTableModel(final List<PtProfil> loi, final String colXName, final String colYName) {
      super();
      this.loi = loi;
      this.colXName = colXName;
      this.colYName = colYName;
    }

    @Override
    public int getColumnCount() {
      return 2;
    }

    @Override
    public String getColumnName(final int column) {
      if (column == 0) {
        return colXName;
      }
      return colYName;
    }

    @Override
    public int getRowCount() {
      return loi.size();
    }

    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
      final PtProfil pt = loi.get(rowIndex);
      return Double.valueOf(columnIndex == 0 ? pt.getAbscisse() : pt.getOrdonnee());
    }

    @Override
    public Class<?> getColumnClass(final int columnIndex) {
      return Double.class;
    }

    @Override
    public boolean isCellEditable(final int rowIndex, final int columnIndex) {
      return false;
    }
  }

  private void showLoi(final Loi val) {
    final CrueConfigMetier propDefinition = projet.getPropDefinition();
    final ConfigLoi configLoi = propDefinition.getConfLoi().get(val.getType());
    final LoiTableModel model = new LoiTableModel(val, StringUtils.capitalize(configLoi.getVarAbscisse().getNom()),
            StringUtils.capitalize(configLoi.getVarOrdonnee().getNom()));
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuBorderLayout());
    final BuPanel pnTop = new BuPanel(new BuGridLayout(2, 6, 6));
    pnTop.setBorder(BorderFactory.createEmptyBorder(6, 0, 6, 0));
    final BuLabel comp = new BuLabel(EMHUiMessages.getMessage("EMHTreeView.Titre"));
    final Font ft = BuLib.deriveFont(comp.getFont(), Font.BOLD, 0);
    pnTop.add(comp).setFont(ft);
    pnTop.add(new BuLabel(val.getNom()));
    pnTop.add(new BuLabel(EMHUiMessages.getMessage("EMHTreeView.Commentaire"))).setFont(ft);
    pnTop.add(new BuLabel(val.getCommentaire()));
    if (val instanceof LoiDF) {
      pnTop.add(new BuLabel(EMHUiMessages.getMessage("EMHTreeView.DateZero"))).setFont(ft);
      pnTop.add(new BuLabel(ObjectUtils.toString(((LoiDF) val).getDateZeroLoiDF(), "")));
    }
    pn.add(pnTop, BorderLayout.NORTH);
    final JXTable jxTable = new JXTable(model);
    final ToStringTransformerPoint2d transformer = new ToStringTransformerPoint2d(configLoi.getVarAbscisse().getNom(),
            configLoi.getVarOrdonnee().getNom(), propDefinition, DecimalFormatEpsilonEnum.COMPARISON);
    jxTable.getColumn(0).setCellRenderer(new Point2dXCellRenderer(transformer));
    jxTable.getColumn(1).setCellRenderer(new Point2dYCellRenderer(transformer));
    alignRight(jxTable);
    pn.add(new BuScrollPane(jxTable));
    pn.setBorder(BuBorders.EMPTY3333);
    pn.setErrorTextUnable();
    jxTable.setName("emhTree.loi.table");
    CommonGuiLib.showDialogAndTable(pn, jxTable, "emhTree.loi.dialog", EMHUiMessages.getMessage("EMHTreeView.LoiDialogTitle"), StringUtils.EMPTY);

  }

  private void showPasDeTemps(final ResultatPasDeTempsDelegate val) {

    final PasDeTempsModel model = new PasDeTempsModel(val, projet.getPropDefinition());
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuBorderLayout());
    final JXTable jxTable = new JXTable(model);
    alignRight(jxTable);
    pn.add(new BuScrollPane(jxTable));
    pn.setBorder(BuBorders.EMPTY3333);
    pn.setErrorTextUnable();
    jxTable.setName("emhTree.pdt.table");
    CommonGuiLib.showDialogAndTable(pn, jxTable, "emhTree.pdt.dialog", "Pas de temps", StringUtils.EMPTY);

  }

  @SuppressWarnings("serial")
  private static class ResultatValueColumnCellRenderer extends CtuluCellTextRenderer {

    private final ResultatModel model;

    /**
     * @param model
     */
    public ResultatValueColumnCellRenderer(final ResultatModel model) {
      super();
      this.model = model;
    }

    @Override
    public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus,
                                                   final int row, final int column) {
      final JXTable myTable = (JXTable) table;
      final int modelRow = myTable.convertRowIndexToModel(row);
      final NumberFormat format = model.getFormat(modelRow);
      Object newValue = value;
      if (format != null && newValue != null) {
        newValue = format.format(newValue);
      }
      return super.getTableCellRendererComponent(table, newValue, isSelected, hasFocus, row, column);
    }
  }

  @SuppressWarnings("serial")
  private static class ResultatModel extends AbstractTableModel {

    @SuppressWarnings("unchecked")
    private final ResultatCalcul val;
    final CrueConfigMetier crueProperties;
    final List<String> props = new ArrayList<>();
    final List<Object> values = new ArrayList<>();
    List<String> visibleProps;
    final List<NumberFormat> valueFormat = new ArrayList<>();
    final OrdResScenario ordRes;

    public NumberFormat getFormat(final int row) {
      return valueFormat.get(row);
    }

    /**
     * @param val
     */
    @SuppressWarnings("unchecked")
    public ResultatModel(final ResultatCalcul val, final OrdResScenario ordRes,
            final CrueConfigMetier crueProperties) {
      super();
      this.val = val;
      this.ordRes = ordRes;
      this.crueProperties = crueProperties;
    }

    @Override
    public String getColumnName(final int column) {
      return column == 0 ? "Propriétés" : "Valeurs";
    }

    @Override
    public int getColumnCount() {
      return 2;
    }

    @Override
    public int getRowCount() {
      return props.size();
    }

    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
      if (columnIndex == 0) {
        return props.get(rowIndex);
      }
      return values.get(rowIndex);
    }

    @Override
    public Class<?> getColumnClass(final int columnIndex) {
      return columnIndex == 0 ? String.class : Number.class;
    }

    public void setResultatKey(final ResultatTimeKey key) {
      int lastRow = props.size() - 1;
      props.clear();
      values.clear();
      valueFormat.clear();
      if (lastRow > 0) {
        super.fireTableRowsDeleted(0, lastRow);
      }
      // this.idxPdt = idxPdt;
      try {
        final LazyDynaMap read = new LazyDynaMap(val.read(key));
        if (visibleProps == null) {
          visibleProps = new ArrayList<>(OrdResDynamicPropertyFilter.getOrdresPropsWithRegul(val, ordRes));
          Collections.sort(visibleProps);
        }
        for (final String object : visibleProps) {
          props.add(StringUtils.capitalize(object));
          final Object value = PropertyUtils.getProperty(read, object);
          valueFormat.add(crueProperties.getFormatter(object, DecimalFormatEpsilonEnum.COMPARISON));
          values.add(value);
        }
      } catch (final Exception e) {
        Exceptions.printStackTrace(e);
        Logger.getLogger(getClass().getName()).log(Level.SEVERE, "setResultatKey " + key, e);
      }
      lastRow = props.size() - 1;
      if (lastRow > 0) {
        super.fireTableRowsInserted(0, lastRow);
      }
    }
  }

  private void showResulat(final ResultatCalcul val) {

    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuBorderLayout());
    final ArrayList<ResultatTimeKey> sortedList = new ArrayList<>(val.getResultatPasDeTemps().getResultatKeys());
    final CrueConfigMetier crueProperties = projet.getPropDefinition();
    final ResultatModel model = new ResultatModel(val, scenarioLoaded.getOrdResScenario(), crueProperties);
    final JComboBox cb = new JComboBox(sortedList.toArray());
    cb.addItemListener(e -> {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        model.setResultatKey((ResultatTimeKey) cb.getSelectedItem());
      }
    });
    if (!sortedList.isEmpty()) {
      cb.setSelectedIndex(0);
    }
    model.setResultatKey((ResultatTimeKey) cb.getSelectedItem());
    pn.add(cb, BuBorderLayout.NORTH);
    final JXTable jxTable = new JXTable(model);
    jxTable.getColumn(1).setCellRenderer(new ResultatValueColumnCellRenderer(model));
    pn.add(new BuScrollPane(jxTable));
    pn.setBorder(BuBorders.EMPTY3333);
    pn.setErrorTextUnable();
    jxTable.setName("emhTree.res.table");
    CommonGuiLib.showDialogAndTable(pn, jxTable, "emhTree.res.dialog", EMHUiMessages.getMessage(
            "EMHTreeView.AffichageResulatDialog", val.getEmh().getNom()), StringUtils.EMPTY);

  }

  private void showPtProfil(final List<PtProfil> val) {

    final ConfigLoi configLoi = projet.getPropDefinition().getConfLoi().get(EnumTypeLoi.LoiPtProfil);
    final PtProfilTableModel model = new PtProfilTableModel(val, StringUtils.capitalize(configLoi.getVarAbscisse().getNom()),
            StringUtils.capitalize(configLoi.getVarOrdonnee().getNom()));
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuBorderLayout());
    final JXTable jxTable = new JXTable(model);
    jxTable.getColumn(0).setCellRenderer(new FormattedCellRenderer(configLoi.getVarAbscisse().getFormatter(DecimalFormatEpsilonEnum.COMPARISON)));
    jxTable.getColumn(1).setCellRenderer(new FormattedCellRenderer(configLoi.getVarOrdonnee().getFormatter(DecimalFormatEpsilonEnum.COMPARISON)));
    jxTable.setName("emhTree.ptProfil.table");
    alignRight(jxTable);
    pn.add(new BuScrollPane(jxTable));
    pn.setBorder(BuBorders.EMPTY3333);
    pn.setErrorTextUnable();
    CommonGuiLib.showDialogAndTable(pn, jxTable, "emhTree.ptProfil.dialog", EMHUiMessages.getMessage(
            "EMHTreeView.AffichageProfilDialog"), StringUtils.EMPTY);

  }

  private void alignRight(final JXTable jxTable) {
    jxTable.addHighlighter(new AbstractHighlighter() {
      @Override
      protected Component doHighlight(final Component component, final ComponentAdapter adapter) {
        ((JLabel) component).setHorizontalAlignment(SwingConstants.RIGHT);
        return component;
      }
    });
  }

  private void showFrottement(final DonFrt val) {
    showLoi(val.getLoi());

  }

  /**
   * Init la construction graphique.
   */
  public JPanel build(final String nameSuffix) {

    final Map<String, AbstractTreeNode> ref = new HashMap<>();
    final DefaultTreeTableModel treeModel = new EMHManagerBuilder().buildTreeScenario(projet.getPropDefinition(), scenarioLoaded,
            ref);

    final List<String> colonnes = new ArrayList<>();
    colonnes.add(EMHUiMessages.getMessage("EMHTreeView.ColumnPropertyName"));
    colonnes.add(EMHUiMessages.getMessage("EMHTreeView.ColumnValueName"));
    treeModel.setColumnIdentifiers(colonnes);
    treeEMH = new UiContext().createTreeTable(treeModel, true);
    final PopupMenuReceiver receiver = new PopupMenuReceiver() {
      JMenuItem add;

      @Override
      protected void updateItemStateBeforeShow() {
        super.updateItemStateBeforeShow();
        add.setEnabled(false);
        final TreePath path = treeEMH.getTreeSelectionModel().getSelectionPath();
        if (path != null) {
          final AbstractTreeNode lastPathComponent = (AbstractTreeNode) path.getLastPathComponent();
          final AbstractTreeNode dest = ref.get(lastPathComponent.getLinkId(1));
          add.setEnabled(dest != null);
        }
      }

      @Override
      protected void addItems() {
        super.addItems();
        popupMenu.addSeparator();
        add = popupMenu.add(NbBundle.getMessage(CommonGuiLib.class, "FollowLink.Action"));
        add.setToolTipText(NbBundle.getMessage(CommonGuiLib.class, "FollowLink.ActionTooltip"));

        add.addActionListener(e -> followLink(ref));
      }
    };
    receiver.install(treeEMH, null);
    treeEMH.setName("emhTree.treeTable." + nameSuffix);
    treeEMH.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(final MouseEvent e) {
        if (e.isAltDown()) {
          final int columnAtPoint = treeEMH.columnAtPoint(e.getPoint());
          if (columnAtPoint != 1) {
            return;
          }
          followLink(ref);

        } else if (e.getClickCount() == 2) {
          displayInfo(treeEMH.getTreeSelectionModel().getSelectionPath());
        }
      }
    });
    treeEMH.setTreeCellRenderer(new DefaultTreeCellRenderer() {
      @Override
      public Component getTreeCellRendererComponent(final JTree tree, final Object value, final boolean sel,
              final boolean expanded, final boolean leaf, final int row,
              final boolean hasFocus) {
        final Component res = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        final AbstractTreeNode treeNode = (AbstractTreeNode) value;
        if (sel) {
          setForeground(treeEMH.getSelectionForeground());
        } else {
          setForeground(treeNode.isActive() ? Color.BLACK : Color.GRAY);
        }
        setText(ObjectUtils.toString(treeNode.getValueAt(0)));
        if (treeNode.getIcon() != null) {
          setIcon(treeNode.getIcon());
        }
        return res;
      }
    });
    treeEMH.getColumn(1).setCellRenderer(new DefaultTableCellRenderer() {
      @Override
      public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
              final boolean hasFocus, final int row, final int column) {
        // setToolTipText(StringUtils.EMPTY);
        final Component res = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        final TreePath pathForRow = treeEMH.getPathForRow(row);
        final AbstractTreeNode lastPathComponent = (AbstractTreeNode) pathForRow.getLastPathComponent();
        Color htmlColor = getForeground();
        Color foreColor = getForeground();
        if (isSelected) {
          foreColor = table.getSelectionForeground();
          htmlColor = foreColor;

        } else {
          foreColor = lastPathComponent.isActive() ? Color.BLACK : Color.GRAY;
          htmlColor = lastPathComponent.isActive() ? Color.BLUE : BLUE_DARKER;
        }
        setForeground(foreColor);
        String text = getText();
        if (lastPathComponent.getUserObject() != null) {
          text = text + " (+)";
        }
        if (lastPathComponent.isLink(column)) {
          final String hexred = Integer.toHexString(htmlColor.getRed());
          final String hexgreen = Integer.toHexString(htmlColor.getGreen());
          final String hexblue = Integer.toHexString(htmlColor.getBlue());
          setText("<html><body><a color=\"#" + hexred + hexgreen + hexblue + "\" href="
                  + lastPathComponent.getLinkId(column) + ">" + text + "</a>");
        }
        if (lastPathComponent.getUserObject() != null) {
          setText(text);
          setToolTipText("<html><body>" + (StringUtils.isNotEmpty(getText()) ? getText() + "<br>" : "")
                  + EMHUiMessages.getMessage("EMHTreeView.ClicTooltip"));
        } else {
          setToolTipText(getText());
        }

        return res;
      }
    });
    treeEMH.packAll();
    treeEMH.setShowHorizontalLines(true);
    treeEMH.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    final JPanel pn = new BuPanel();
    pn.setLayout(new BuBorderLayout());
    pn.setBorder(BuBorders.EMPTY3333);
    tb = new JToolBar();
    final TableSelectionHistory history = new TableSelectionHistory(treeEMH);
    tb.add(history.getPreviousAction());
    tb.add(history.getNextAction());
    pn.add(tb, BorderLayout.NORTH);
    pn.add(new JScrollPane(treeEMH), BorderLayout.CENTER);
    UserPreferencesSaver.loadTablePreferences(treeEMH);
    EbliLib.updateMapKeyStroke(treeEMH, new Action[]{history.getPreviousAction(), history.getNextAction()});
    return pn;
  }

  public void followLink(final Map<String, AbstractTreeNode> ref) {
    final TreePath path = treeEMH.getTreeSelectionModel().getSelectionPath();
    if (path == null) {
      return;
    }
    final AbstractTreeNode lastPathComponent = (AbstractTreeNode) path.getLastPathComponent();
    if (lastPathComponent == null) {
      return;
    }
    final AbstractTreeNode dest = ref.get(lastPathComponent.getLinkId(1));
    treeEMH.selectAndShow(dest);
  }
  JToolBar tb;
  private JLabel lbTitle;

  public void selectLines(final Collection<Long> emhUid) {
    final TreeTableNode root = treeEMH.getTreeTableModel().getRoot();
    final Enumeration<? extends TreeTableNode> mainChildren = root.children();
    final List<TreeTableNode> selectedNodes = new ArrayList<>();
    while (mainChildren.hasMoreElements()) {
      final TreeTableNode mainChild = mainChildren.nextElement();
      final Enumeration<? extends TreeTableNode> children = mainChild.children();
      while (children.hasMoreElements()) {
        {
          final TreeTableNode child = children.nextElement();
          if (TreeNodeEMH.class.equals(child.getClass())
                  && emhUid.contains(((TreeNodeEMH) child).getEmh().getUiId())) {
            selectedNodes.add(child);
          }
        }
      }
      treeEMH.selectAndShow(selectedNodes, true);

    }
  }

  public void setTitle(final String title) {
    if (lbTitle == null) {
      lbTitle = new JLabel();
      tb.addSeparator();
      tb.add(lbTitle);
    }
    lbTitle.setText(title);
  }
}
