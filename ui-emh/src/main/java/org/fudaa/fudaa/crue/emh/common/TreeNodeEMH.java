/**
 * 
 */
package org.fudaa.fudaa.crue.emh.common;

import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.fudaa.crue.common.swingx.AbstractTreeNode;

public class TreeNodeEMH extends AbstractTreeNode {
  private final EMH emh;

  public TreeNodeEMH(final EMH emh) {
    this.emh = emh;
  }

  @Override
  public int getColumnCount() {
    return 2;
  }

  public EMH getEmh() {
    return emh;
  }

  @Override
  public boolean isEditable(final int column) {
    return false;
  }

  @Override
  public boolean isActive() {
    return emh.getActuallyActive();
  }

  @Override
  public Object getValueAt(final int c) {
    if (c == 0) {
      return emh.getNom();
    }
    if (!emh.getUserActive()) {
      return emh.getTypei18n() + EMHUiMessages.EMH_NON_ACTIVE_USER;
    }
    if (!emh.getActuallyActive()) {
      return emh.getTypei18n() + EMHUiMessages.EMH_NON_ACTIVE;
    }
    return emh.getTypei18n();

  }
}
