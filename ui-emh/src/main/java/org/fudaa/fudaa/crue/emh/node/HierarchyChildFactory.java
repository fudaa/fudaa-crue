/*
 GPL 2
 */
package org.fudaa.fudaa.crue.emh.node;

import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public class HierarchyChildFactory extends ChildFactory<EMH> {
    protected final boolean addChild = true;
    private final NodeEMHFactory nodeEMHFactory;
    protected final EMHSousModele emhSousModele;
    private final EnumCatEMH catEMH;

    public static Node createScenarioNode(final EMHScenario scenario, NodeEMHFactory factory) {
        return createScenarioNode(scenario, factory, null);
    }

    public static Node createScenarioNode(final EMHScenario scenario, NodeEMHFactory factory, Class sectionActionClass) {
        List<EMHSousModele> sousModeles = scenario.getSousModeles();
        List<Node> nodes = new ArrayList<>(sousModeles.size());
        for (EMHSousModele sousModele : sousModeles) {
            if(factory.isVisible(sousModele)){
                nodes.add(createSousModeleNode(sousModele, factory, sectionActionClass));
            }
        }
        Children.Array array = new Children.Array();
        array.add(nodes.toArray(new Node[0]));
        AbstractNode node = new AbstractNode(array, Lookup.EMPTY) {
            @Override
            public HelpCtx getHelpCtx() {
                return new HelpCtx(SysdocUrlBuilder.getEMHHelpCtxId(scenario));
            }
        };
        node.setDisplayName(scenario.getNom());
        return node;
    }

    public static Node createSousModeleNode(final EMHSousModele emhSousModele, NodeEMHFactory factory, Class sectionActionClass) {
        // création de manière statique des noeuds enfants du noeud Scénario pour les sous-modèles
        final Node[] nodesArray = new Node[4];
        nodesArray[0] = new AbstractNode(Children.create(new HierarchyChildFactory(emhSousModele, factory, EnumCatEMH.NOEUD), false), Lookup.EMPTY);
        nodesArray[0].setDisplayName(EnumCatEMH.NOEUD.geti18nPlural());
        nodesArray[1] = new AbstractNode(Children.create(new HierarchyChildFactory(emhSousModele, factory, EnumCatEMH.BRANCHE), false), Lookup.EMPTY);
        nodesArray[1].setDisplayName(EnumCatEMH.BRANCHE.geti18nPlural());
        nodesArray[2] = new AbstractNode(Children.create(new HierarchyChildFactory(emhSousModele, factory, EnumCatEMH.CASIER), false), Lookup.EMPTY);
        nodesArray[2].setDisplayName(EnumCatEMH.CASIER.geti18nPlural());
        if (sectionActionClass != null) {
            nodesArray[3] = new AbstractNode(Children.create(new HierarchyChildFactory(emhSousModele, factory, EnumCatEMH.SECTION), false), Lookup.EMPTY) {
                @Override
                public Action[] getActions(boolean context) {
                    return new Action[]{
                            SystemAction.get(sectionActionClass)
                    };
                }
            };
        } else {
            nodesArray[3] = new AbstractNode(Children.create(new HierarchyChildFactory(emhSousModele, factory, EnumCatEMH.SECTION), false), Lookup.EMPTY);
        }
        nodesArray[3].setDisplayName(EnumCatEMH.SECTION.geti18nPlural());
        Children.Array array = new Children.Array();
        array.add(nodesArray);

        AbstractNode node = new AbstractNode(array, Lookup.EMPTY) {
            @Override
            public HelpCtx getHelpCtx() {
                return new HelpCtx(SysdocUrlBuilder.getEMHHelpCtxId(emhSousModele));
            }
        };
        node.setDisplayName(emhSousModele.getNom());
        return node;
    }

    public HierarchyChildFactory(EMHSousModele emhSousModele, NodeEMHFactory factory, EnumCatEMH catEMH) {
        this.emhSousModele = emhSousModele;
        this.nodeEMHFactory = factory;
        this.catEMH = catEMH;
    }

    @Override
    protected Node createNodeForKey(EMH key) {
        return nodeEMHFactory.createNode(key, addChild);
    }

    @Override
    protected boolean createKeys(List<EMH> toPopulate) {
        List<EMH> allSimpleEMH = emhSousModele.getAllSimpleEMHinUserOrder();
        toPopulate.addAll(EMHHelper.selectEMHS(allSimpleEMH, catEMH));
        return true;
    }

    public static List<EMH> getEMHInNetworkOrder(List<EMH> target, EMHScenario scenario) {
        List<EMH> toPopulate = target;
        if (toPopulate == null) {
            toPopulate = new ArrayList<>();
        }
        List<EMH> allSimpleEMH = scenario.getAllSimpleEMHinUserOrder();
        toPopulate.addAll(EMHHelper.selectEMHS(allSimpleEMH, EnumCatEMH.NOEUD));
        toPopulate.addAll(EMHHelper.selectEMHS(allSimpleEMH, EnumCatEMH.BRANCHE));
        toPopulate.addAll(EMHHelper.selectEMHS(allSimpleEMH, EnumCatEMH.CASIER));
        toPopulate.addAll(EMHHelper.selectEMHS(allSimpleEMH, EnumCatEMH.SECTION));
        return toPopulate;
    }
}
