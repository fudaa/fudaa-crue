package org.fudaa.fudaa.crue.emh.common;

import com.memoire.bu.BuResource;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import javax.swing.Action;
import javax.swing.KeyStroke;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandAction;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.fudaa.crue.common.swing.CommonGuiLib;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

/**
 * @author deniger
 */
public class OutlineSelectionHistory implements PropertyChangeListener {

  private class ChangeSelectionCommand implements CtuluCommand {

    final Node[] newPath;
    final Node[] oldPath;

    /**
     * @param oldPath
     * @param newPath
     */
    public ChangeSelectionCommand(Node[] oldPath, Node[] newPath) {
      super();
      this.oldPath = oldPath;
      this.newPath = newPath;
    }

    private void changePath(Node[] path) {
      isUpdatingFromHistory = true;
      try {
        em.setSelectedNodes(path);
      } catch (PropertyVetoException ex) {
        Exceptions.printStackTrace(ex);
      }
      isUpdatingFromHistory = false;
    }

    @Override
    public void redo() {
      changePath(newPath);
    }

    @Override
    public void undo() {
      changePath(oldPath);
    }
  }
  CtuluCommandManager history;
  private boolean isUpdatingFromHistory;
  private Action nextAction;
  private Action previousAction;
  private final ExplorerManager em;

  /**
   * WARN: il faut bien faire attention a désinstaller cet history
   *
   * @param outlineView
   */
  public OutlineSelectionHistory(OutlineView table) {
    super();
    em = ExplorerManager.find(table);
    em.addPropertyChangeListener(this);
  }

  /**
   * @return l'historique des selections.
   */
  public CtuluCommandManager getHistory() {
    if (history == null) {
      history = new CtuluCommandManager();

    }
    return history;
  }

  /**
   * @return the nextAction
   */
  public Action getNextAction() {
    if (nextAction == null) {
      nextAction = new CtuluCommandAction.Redo(getHistory());
      nextAction.putValue(Action.NAME, "history.next");
      String value = EMHUiMessages.getMessage("History.next");
      nextAction.putValue(Action.LONG_DESCRIPTION, value);
      nextAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.ALT_MASK));
      nextAction.putValue(Action.SHORT_DESCRIPTION, "<html><body style='margin:1px'>" + value + "<br>"
              + CommonGuiLib.getAcceleratorText((KeyStroke) nextAction.getValue(Action.ACCELERATOR_KEY))
              + "</body></html>");
      nextAction.putValue(Action.SMALL_ICON, BuResource.BU.getToolIcon("crystal_avancervite"));
    }
    return nextAction;
  }

  /**
   * @return the previousAction
   */
  public Action getPreviousAction() {
    if (previousAction == null) {
      previousAction = new CtuluCommandAction.Undo(getHistory());
      previousAction.putValue(Action.NAME, "history.back");
      String value = EMHUiMessages.getMessage("History.back");
      previousAction.putValue(Action.LONG_DESCRIPTION, value);
      previousAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, InputEvent.ALT_MASK));
      previousAction.putValue(Action.SHORT_DESCRIPTION, "<html><body style='margin:1px'>" + value + "<br>"
              + CommonGuiLib.getAcceleratorText((KeyStroke) previousAction.getValue(Action.ACCELERATOR_KEY))
              + "</body></html>");
      previousAction.putValue(Action.SMALL_ICON, BuResource.BU.getToolIcon("crystal_reculervite"));
    }
    return previousAction;
  }

  /**
   *
   */
  public void uninstallListener() {
    em.removePropertyChangeListener(this);
  }
  Node[] keptOldSelection;

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (!isUpdatingFromHistory) {
      if (ExplorerManager.PROP_SELECTED_NODES.equals(evt.getPropertyName())) {
        Node[] oldNode = (Node[]) evt.getOldValue();
        Node[] newNode = (Node[]) evt.getNewValue();
        //cela peut arriver...

        if (oldNode.length == 0 && keptOldSelection != null) {
          oldNode = keptOldSelection;
        }
        if (CtuluLibArray.isEquals(newNode, oldNode)) {
          return;
        }
        keptOldSelection = null;
        if (oldNode.length > 0 && newNode.length > 0) {
          getHistory().addCmd(new ChangeSelectionCommand(oldNode, newNode));
        } else if (newNode.length == 0 && oldNode.length > 0) {
          keptOldSelection = oldNode;
        }
      } else if (ExplorerManager.PROP_ROOT_CONTEXT.equals(evt.getPropertyName())) {
        keptOldSelection = null;
        getHistory().clean();

      }
    }
  }
}
