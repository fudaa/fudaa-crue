package org.fudaa.fudaa.crue.emh.common;

import com.memoire.bu.BuResource;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandAction;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.fudaa.crue.common.helper.JXTreeTableExtended;
import org.fudaa.fudaa.crue.common.swing.CommonGuiLib;
import org.jdesktop.swingx.treetable.TreeTableNode;

/**
 * @author deniger
 */
public class TableSelectionHistory implements TreeSelectionListener {

  private class ChangeSelectionCommand implements CtuluCommand {

    private final Object newPath;
    private final Object oldPath;

    /**
     * @param oldPath
     * @param newPath
     */
    public ChangeSelectionCommand(TreePath oldPath, TreePath newPath) {
      super();
      this.oldPath = oldPath == null ? null : oldPath.getLastPathComponent();
      this.newPath = newPath == null ? null : newPath.getLastPathComponent();
    }

    private void changePath(Object path) {
      isUpdatingFromHistory = true;
      table.selectAndShow((TreeTableNode) path, false);
      isUpdatingFromHistory = false;
    }

    @Override
    public void redo() {
      changePath(newPath);
    }

    @Override
    public void undo() {
      changePath(oldPath);
    }
  }
  private CtuluCommandManager history;
  private boolean isUpdatingFromHistory;
  private Action nextAction;
  private Action previousAction;
  private final JXTreeTableExtended table;

  /**
   * WARN: il faut bien faire attention a désinstaller cet history
   *
   * @param table
   */
  public TableSelectionHistory(JXTreeTableExtended table) {
    super();
    this.table = table;
    table.addTreeSelectionListener(this);
  }

  /**
   * @return l'historique des selections.
   */
  public CtuluCommandManager getHistory() {
    if (history == null) {
      history = new CtuluCommandManager();

    }
    return history;
  }

  /**
   * @return the nextAction
   */
  public Action getNextAction() {
    if (nextAction == null) {
      nextAction = new CtuluCommandAction.Redo(getHistory());
      nextAction.putValue(Action.NAME, "history.next");
      String value = EMHUiMessages.getMessage("History.next");
      nextAction.putValue(Action.LONG_DESCRIPTION, value);
      nextAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.ALT_MASK));
      nextAction.putValue(Action.SHORT_DESCRIPTION, "<html><body style='margin:1px'>" + value + "<br>"
              + CommonGuiLib.getAcceleratorText((KeyStroke) nextAction.getValue(Action.ACCELERATOR_KEY))
              + "</body></html>");
      nextAction.putValue(Action.SMALL_ICON, BuResource.BU.getToolIcon("crystal_avancervite"));
    }
    return nextAction;
  }

  /**
   * @return the previousAction
   */
  public Action getPreviousAction() {
    if (previousAction == null) {
      previousAction = new CtuluCommandAction.Undo(getHistory());
      previousAction.putValue(Action.NAME, "history.back");
      String value = EMHUiMessages.getMessage("History.back");
      previousAction.putValue(Action.LONG_DESCRIPTION, value);
      previousAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, InputEvent.ALT_MASK));
      previousAction.putValue(Action.SHORT_DESCRIPTION, "<html><body style='margin:1px'>" + value + "<br>"
              + CommonGuiLib.getAcceleratorText((KeyStroke) previousAction.getValue(Action.ACCELERATOR_KEY))
              + "</body></html>");
      previousAction.putValue(Action.SMALL_ICON, BuResource.BU.getToolIcon("crystal_reculervite"));
    }
    return previousAction;
  }

  /**
   *
   */
  public void uninstallListener() {
    table.removeTreeSelectionListener(this);
  }

  @Override
  public void valueChanged(TreeSelectionEvent e) {
    if (!isUpdatingFromHistory) {
      getHistory().addCmd(new ChangeSelectionCommand(e.getOldLeadSelectionPath(), e.getNewLeadSelectionPath()));
    }
  }
}
