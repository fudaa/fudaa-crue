package org.fudaa.fudaa.crue.emh.node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.fudaa.crue.common.ContainerActivityVisibility;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 * @author deniger
 */
public class NodeEMHFactory {

    private final ContainerActivityVisibility containerActivityVisibility;

    public NodeEMHFactory(ContainerActivityVisibility containerActivityVisibility) {
        this.containerActivityVisibility = containerActivityVisibility;
    }

    public boolean isVisible(EMHSousModele sousModele) {
        if (containerActivityVisibility == null) {
            return true;
        }
        return containerActivityVisibility.isSousModeleVisible(sousModele);
    }


    protected AbstractNode createNodeEMH(Children children, EMH emh) {
        return new NodeEMHDefault(children, emh);
    }

    protected AbstractNode createNodeEMH(Children children, EMH emh, String displayName) {
        return new NodeEMHDefault(children, emh, displayName);
    }

    public AbstractNode createNode(EMH in, boolean addChild) {
        EnumCatEMH catType = in.getCatType();
        switch (catType) {
            case NOEUD:
                return createNodeEMH(createChild((CatEMHNoeud) in, addChild), in);
            case BRANCHE:
                return createNodeEMH(createChild((CatEMHBranche) in, addChild), in);
            case SECTION:
                return createNodeEMH(createChild((CatEMHSection) in, addChild), in);
            case CASIER:
                return createNodeEMH(createChild((CatEMHCasier) in, addChild), in);
            default:
                throw new IllegalAccessError(" not supported " + catType);
        }
    }

    public Children createChild(CatEMHNoeud noeud, boolean addChild) {
        if (addChild) {
            return createNodeChildren(noeud);
        }
        return Children.LEAF;
    }

    public Children createChild(CatEMHSection noeud, boolean addChild) {
        if (addChild) {
            return createNodeChildren(noeud);
        }
        return Children.LEAF;
    }

    public Children createChild(CatEMHCasier casier, boolean addChild) {
        if (addChild) {
            return createNodeChildren(casier);
        }
        return Children.LEAF;
    }

    public Children createChild(CatEMHBranche branche, boolean addChild) {
        if (addChild) {
            return createNodeChildren(branche);
        }
        return Children.LEAF;
    }

    public Children createNodeChildren(CatEMHNoeud noeud) {
        List<Node> nodes = new ArrayList<>();
        List<CatEMHBranche> branchesAmont = noeud.getBranchesAmont();
        Collections.sort(branchesAmont, ObjetNommeByNameComparator.INSTANCE);
        for (CatEMHBranche branche : branchesAmont) {
            nodes.add(createNodeEMH(Children.LEAF, branche, NbBundle.getMessage(NodeEMHFactory.class, "NodeEMH.BrancheAmont.Name")));
        }
        List<CatEMHBranche> branchesAval = noeud.getBranchesAval();
        Collections.sort(branchesAval, ObjetNommeByNameComparator.INSTANCE);
        for (CatEMHBranche branche : branchesAval) {
            nodes.add(createNodeEMH(Children.LEAF, branche, NbBundle.getMessage(NodeEMHFactory.class, "NodeEMH.BrancheAval.Name")));
        }
        CatEMHCasier casier = noeud.getCasier();
        if (casier != null) {
            nodes.add(createNodeEMH(Children.LEAF, casier, NbBundle.getMessage(NodeEMHFactory.class, "NodeEMH.Casier.Name")));
        }
        if (nodes.isEmpty()) {
            return Children.LEAF;
        }
        Children.Array res = new Children.Array();
        res.add(nodes.toArray(new Node[0]));
        return res;

    }

    public Children createNodeChildren(CatEMHCasier casier) {
        List<Node> nodes = new ArrayList<>();
        CatEMHNoeud noeud = casier.getNoeud();
        if (noeud != null) {
            nodes.add(
                    createNodeEMH(Children.LEAF, noeud, NbBundle.getMessage(NodeEMHFactory.class, "NodeEMH.Noeud.Name")));
        }
        if (nodes.isEmpty()) {
            return Children.LEAF;
        }
        Children.Array res = new Children.Array();
        res.add(nodes.toArray(new Node[0]));
        return res;

    }

    public Children createNodeChildren(CatEMHSection section) {
        List<Node> nodes = new ArrayList<>();
        CatEMHBranche branche = section.getBranche();
        if (branche != null) {
            nodes.add(
                    createNodeEMH(Children.LEAF, branche, NbBundle.getMessage(NodeEMHFactory.class, "NodeEMH.Branche.Name")));

        }
        if (section instanceof EMHSectionIdem) {
            CatEMHSection sectionRef = EMHHelper.getSectionRef((EMHSectionIdem) section);
            if (sectionRef != null) {
                nodes.add(
                        createNodeEMH(Children.LEAF, sectionRef, NbBundle.getMessage(NodeEMHFactory.class, "NodeEMH.SectionRef.Name")));
            }
        }
        if (nodes.isEmpty()) {
            return Children.LEAF;
        }
        Children.Array res = new Children.Array();
        res.add(nodes.toArray(new Node[0]));
        return res;

    }

    public Children createNodeChildren(CatEMHBranche branche) {
        List<Node> nodes = new ArrayList<>();
        CatEMHNoeud noeudAmont = branche.getNoeudAmont();
        if (noeudAmont != null) {
            nodes.add(
                    createNodeEMH(Children.LEAF, noeudAmont, NbBundle.getMessage(NodeEMHFactory.class, "NodeEMH.NoeudAmont.Name")));
        }
        CatEMHNoeud noeudAval = branche.getNoeudAval();
        if (noeudAval != null) {
            nodes.add(
                    createNodeEMH(Children.LEAF, noeudAval, NbBundle.getMessage(NodeEMHFactory.class, "NodeEMH.NoeudAval.Name")));
        }
        RelationEMHSectionDansBranche sectionAmont = branche.getSectionAmont();
        if (sectionAmont != null) {
            nodes.add(
                    createNodeEMH(Children.LEAF, sectionAmont.getEmh(), NbBundle.getMessage(NodeEMHFactory.class,
                            "NodeEMH.SectionAmont.Name")));
        }
        List<RelationEMHSectionDansBranche> listeSectionsSortedXP = branche.getListeSectionsSortedXP(null);
        for (RelationEMHSectionDansBranche relationEMHSectionDansBranche : listeSectionsSortedXP) {
            if (relationEMHSectionDansBranche.getPos() != EnumPosSection.AMONT && relationEMHSectionDansBranche.getPos() != EnumPosSection.AVAL) {
                nodes.add(
                        createNodeEMH(Children.LEAF, relationEMHSectionDansBranche.getEmh(),
                                NbBundle.getMessage(NodeEMHFactory.class,
                                        "NodeEMH.SectionIntermediaire.Name")));
            }
        }
        RelationEMHSectionDansBranche sectionAval = branche.getSectionAval();
        if (sectionAval != null) {
            nodes.add(
                    createNodeEMH(Children.LEAF, sectionAval.getEmh(), NbBundle.getMessage(NodeEMHFactory.class,
                            "NodeEMH.SectionAval.Name")));
        }
        CatEMHSection sectionPilote = EMHHelper.getSectionPilote(branche);
        if (sectionPilote != null) {
            nodes.add(
                    createNodeEMH(Children.LEAF, sectionPilote, NbBundle.getMessage(NodeEMHFactory.class,
                            "NodeEMH.SectionPilote.Name")));
        }

        if (nodes.isEmpty()) {
            return Children.LEAF;
        }
        Children.Array res = new Children.Array();
        res.add(nodes.toArray(new Node[0]));
        return res;

    }
}
