package org.fudaa.fudaa.crue.emh.common;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.Avancement;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.CommentaireContainer;
import org.fudaa.dodico.crue.metier.emh.CommentaireItem;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;
import org.fudaa.dodico.crue.metier.emh.DonCLimMScenario;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrt;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.DonFrtConteneur;
import org.fudaa.dodico.crue.metier.emh.DonLoiHYConteneur;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.InfosEMH;
import org.fudaa.dodico.crue.metier.emh.LitNumerote;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.OrdCalc;
import org.fudaa.dodico.crue.metier.emh.PdtVar;
import org.fudaa.dodico.crue.metier.emh.PlanimetrageNbrPdzCst;
import org.fudaa.dodico.crue.metier.emh.Regle;
import org.fudaa.dodico.crue.metier.emh.RelationEMH;
import org.fudaa.dodico.crue.metier.emh.ResPrtGeo;
import org.fudaa.dodico.crue.metier.emh.ResultatCalcul;
import org.fudaa.dodico.crue.metier.emh.ResultatPasDeTemps;
import org.fudaa.dodico.crue.metier.emh.ResultatPasDeTempsDelegate;
import org.fudaa.dodico.crue.metier.emh.Sorties;
import org.fudaa.dodico.crue.metier.emh.Trace;
import org.fudaa.dodico.crue.metier.emh.ValParam;
import org.fudaa.dodico.crue.metier.helper.AnnotationHelper;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransfomerFactory;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.fudaa.fudaa.crue.common.swingx.AbstractTreeNode;
import org.fudaa.fudaa.crue.common.swingx.TreeNodeCleValeur;
import org.fudaa.fudaa.crue.common.swingx.TreeNodeString;
import org.jdesktop.swingx.treetable.AbstractMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.joda.time.Duration;
import org.joda.time.base.AbstractPartial;
import org.openide.util.NbBundle;

/**
 * BUilder d'IHM propre aux EMH.
 *
 * @author Adrien Hadoux
 */
public class EMHManagerBuilder {

  private final static Logger LOGGER = Logger.getLogger(EMHManagerBuilder.class.getName());
  private CrueConfigMetier propertyDefinitionContainer;
  private final Set<String> toAvoid = new HashSet<>(Arrays.asList("class", "resContainer", "type", "catType",
          "typeAndEMH"));
  private ToStringTransformer toStringTransformer;

  private boolean propertyMustBeAvoid(final String name) {
    return toAvoid.contains(name);
  }

  private boolean isSortieContent(final Object param) {
    if (param != null) {
      final Class cl = param.getClass();
      return Sorties.class.equals(cl) || Avancement.class.equals(cl) || Trace.class.equals(cl);
    }
    return false;
  }

  private void addDescribeNode(final Object parent, final DefaultMutableTreeTableNode parentNode,
          final String propToAvoid, final Map<String, AbstractTreeNode> ref) {
    try {
      final TreeMap describe = new TreeMap(PropertyUtils.describe(parent));
      final boolean isParentDcsp = parent instanceof DonCalcSansPrt;
      for (final Iterator it = describe.entrySet().iterator(); it.hasNext();) {
        final Map.Entry entry = (Entry) it.next();
        final String name = (String) entry.getKey();
        if (propertyMustBeAvoid(name) || name.equals(propToAvoid)
                || !AnnotationHelper.isIhmVisible(name, parent.getClass())) {
          continue;
        }
        Object entryValue = entry.getValue();

        final boolean isLitNumerote = "litNumerote".equals(name);
        final boolean isPdtVar = "pdt".equals(name) && entryValue instanceof PdtVar;
        final boolean isFente = "fente".equals(name) && entryValue != null;
        final boolean isPtProfil = "ptProfil".equals(name);
        final boolean isOrdCalc = "ordCalc".equals(name);
        final boolean isRegle = "regles".equals(name);
        final boolean isParamNumCalc = entryValue != null && name.startsWith("paramNumCalc");
        final boolean isOrdRes = entryValue != null && entryValue.getClass().getSimpleName().startsWith("OrdRes");
        final boolean isCalc = "calc".equals(name) && isCollectOrIterator(entryValue);
        final boolean isValParam = "valParam".equals(name);
        final boolean isDisplayedLoi = entryValue instanceof Loi && (isParentDcsp);
        final boolean isResPrtGeoValues = parent instanceof ResPrtGeo && "values".equals(name);
        final boolean isResPasDeTemps = entryValue instanceof ResultatPasDeTemps;
        final boolean isResPasDeTempsDelegate = entryValue instanceof ResultatPasDeTempsDelegate;

        if (isPdtVar) {
          entryValue = ((PdtVar) entryValue).getElemPdt();
        }
        // toutes les listes acceptés
        final boolean isElemSeuil = "elemSeuilAvecPdc".equals(name) || "elemSeuil".equals(name);
        final boolean isResPrtReseauNoeuds = "resPrtReseauNoeuds".equals(name);
        final boolean isAcceptedList = (isElemSeuil || "regles".equals(name) || isPdtVar || isResPrtReseauNoeuds || "resPrtReseauNoeud".equals(
                name)) && entryValue instanceof Collection;

        final boolean isRes = isDisplayedLoi || isResPrtGeoValues || isResPasDeTemps;

        if (!isRes && !isLitNumerote && !isPtProfil && !isOrdCalc && !isCalc && !isValParam && !isAcceptedList
                && !isFente && !isParamNumCalc && isCollectOrIterator(entryValue)) {
          continue;
        }
        final boolean isObjetNomme = entryValue instanceof ObjetWithID;
        String value = null;
        if (isPtProfil) {
          value = ((List) entryValue).size() + " Points";
        } else if (isLitNumerote) {
          value = ((List) entryValue).size() + " Lits numérotés";
        } else if (isOrdCalc) {
          value = ((List) entryValue).size() + " Ordres de calculs";
        } else if (isCalc) {
          value = ((List) entryValue).size() + " calculs";
        } else if (isValParam) {
          value = ((List) entryValue).size() + " paramètres";
        } else if (isResPrtGeoValues) {
          value = ((Map) entryValue).size() + " résultats";
        } else if (isDisplayedLoi) {
          value = ((Loi) entryValue).getEvolutionFF().getPtEvolutionFF().size() + " données";
        } else if (isResPasDeTemps) {
          value = ((ResultatPasDeTemps) entryValue).getNbResultats() + " pas de temps";
        } else if (isResPasDeTempsDelegate) {
          value = ((ResultatPasDeTempsDelegate) entryValue).getNbResultats() + " pas de temps";
        } else if (isObjetNomme) {
          value = ((ObjetWithID) entryValue).getNom();
        } else if (isAcceptedList) {
          value = ((Collection) entryValue).size() + " Valeurs";
        } else {
          value = objectToString(name, entryValue);
        }

        final TreeNodeCleValeur child = new TreeNodeCleValeur(name, value);
        if (isObjetNomme && !isRes) {
          child.setLinkIdForColumnOne(((ObjetWithID) entryValue).getId());
        }
        if (isPtProfil || isDisplayedLoi || isResPasDeTemps || isResPasDeTempsDelegate) {
          child.setUserObject(entryValue);
          if (isDisplayedLoi) {
            child.add(new TreeNodeCleValeur("Commentaire", ((Loi) entryValue).getCommentaire()));
          }
        }
        if (isLitNumerote) {
          final List lits = (List) entryValue;
          int idx = 0;
          for (final Object object : lits) {
            final LitNumerote litNumerote = (LitNumerote) object;
            final LitNomme nomLit = litNumerote.getNomLit();
            final TreeNodeCleValeur nodeLit = new TreeNodeCleValeur(EMHUiMessages.getMessage("EMHTreeView.lit",
                    Integer.toString(++idx)),
                    nomLit == null ? "inconnu" : nomLit.getId());
            child.add(nodeLit);
            addDescribeNode(object, nodeLit, null, ref);
          }
        }

        if (isOrdCalc) {
          final List ordCalcs = (List) entryValue;
          for (final Object object : ordCalcs) {
            final OrdCalc ordCalc = (OrdCalc) object;
            final Calc calc = ordCalc.getCalc();
            final TreeNodeCleValeur nodeCalc = new TreeNodeCleValeur(calc.getId(), ordCalc.getType());
            child.add(nodeCalc);
            addDescribeNode(object, nodeCalc, "calc", ref);
            // on veut pointer vers les calc de DCLM

          }

        }
        if (isCalc) {
          final List lits = (List) entryValue;
          for (final Object object : lits) {
            final Calc calc = (Calc) object;
            final TreeNodeCleValeur nodeCalc = new TreeNodeCleValeur(calc.getId(), calc.getType());
            child.add(nodeCalc);
            addDescribeNode(object, nodeCalc, null, ref);
            if (parent instanceof DonCLimMScenario) {
              ref.put(calc.getId(), nodeCalc);
              addFolderForCalc(calc, nodeCalc);
            }
          }

        }
        if (isValParam) {
          final List lits = (List) entryValue;
          for (final Object object : lits) {
            final ValParam calc = (ValParam) object;
            final TreeNodeCleValeur nodeCalc = new TreeNodeCleValeur(calc.getId(), ObjectUtils.toString(calc.getValeurObjet()));
            child.add(nodeCalc);

          }
        }
        if (isOrdRes || isParamNumCalc || isFente || isSortieContent(entryValue)) {
          addDescribeNode(entryValue, child, null, ref);
        }
        if (isResPrtGeoValues) {
          final Map prtg = (Map) entryValue;
          final List<String> key = new ArrayList(prtg.keySet());
          Collections.sort(key);
          for (final String nom : key) {
            final Object newValue = prtg.get(nom);
            if (newValue instanceof Loi) {
              final TreeNodeCleValeur node = new TreeNodeCleValeur(StringUtils.capitalize(nom), "");
              node.setUserObject(newValue);
              child.add(node);
            } else {
              child.add(new TreeNodeCleValeur(StringUtils.capitalize(nom), TransformerEMHHelper.formatFromPropertyName(nom, newValue, propertyDefinitionContainer, DecimalFormatEpsilonEnum.COMPARISON)));
            }
          }


        }

        if (isRegle) {
          final List<Regle> col = (List<Regle>) entryValue;
          for (final Regle object : col) {
            String toString = StringUtils.EMPTY;
            if (object.getType().isOptr()) {
              toString = object.isActive() ? NbBundle.getMessage(EMHManagerBuilder.class, "regle.isActive") : NbBundle.getMessage(
                      EMHManagerBuilder.class, "regle.isNotActive");
            } else {
              final ValParam valParam = object.getValParam();
              toString = objectToString(name,
                      valParam == null ? "" : valParam.getValueObjetInString(this.propertyDefinitionContainer));
              if (!object.isActive()) {
                toString = toString + " (" + NbBundle.getMessage(EMHManagerBuilder.class, "regle.isNotActive") + ")";
              }
            }
            child.add(new TreeNodeCleValeur(object.getValParamNom(), toString));

          }
        } else if (isAcceptedList) {
          final Collection col = (Collection) entryValue;
          int i = 1;
          for (final Object object : col) {
            TreeNodeCleValeur cleValeur = null;
            if (isResPrtReseauNoeuds) {
              cleValeur = new TreeNodeCleValeur(objectToString(name, object), StringUtils.EMPTY);
              child.add(cleValeur);
            } else {
              cleValeur = new TreeNodeCleValeur(ToStringHelper.typeToi18n(object.getClass().getSimpleName()) + "[" + (i++)
                      + "]", objectToString(name, object));
              child.add(cleValeur);
            }
            if (isElemSeuil || isResPrtReseauNoeuds) {
              addDescribeNode(object, cleValeur, null, ref);
            }

          }
        }
        parentNode.add(child);

      }
    } catch (final Exception e) {
      LOGGER.log(Level.SEVERE, "addDescribeNode", e);
    }
  }

  private void addFolderForCalc(final Calc c, final AbstractTreeNode parent) {
    List<DonCLimM> listeDCLM = c.getlisteDCLM();
    if (listeDCLM == null) {
      listeDCLM = Collections.emptyList();
    }
    final TreeNodeCleValeur folderLiensEMH = new TreeNodeCleValeur(EMHUiMessages.getMessage("EMHTreeView.calc.emhUsed"),
            Integer.toString(listeDCLM.size()));
    parent.add(folderLiensEMH);
    for (final DonCLimM donCLimM : listeDCLM) {
      final EMH emh = donCLimM.getEmh();
      final TreeNodeCleValeur noderelationEMH = new TreeNodeCleValeur(emh.getTypei18n(), emh.getNom());
      noderelationEMH.setLinkIdForColumnOne(emh.getId());
      folderLiensEMH.add(noderelationEMH);
    }

  }

  private void addNodesForEMH(final DefaultMutableTreeTableNode rootNodes, final EMH emh,
          final Map<String, AbstractTreeNode> ref) {
    addNodesForEMH(rootNodes, emh, ref, null);
  }

  private void addNodesForEMH(final DefaultMutableTreeTableNode rootNodes, final EMH emh,
          final Map<String, AbstractTreeNode> ref, final CommentaireContainer comments) {
    final TreeNodeEMH nodeEMH = new TreeNodeEMH(emh);
    nodeEMH.setIcon(CrueIconsProvider.getIcon(emh.getCatType()));

    ref.put(emh.getId(), nodeEMH);
    rootNodes.add(nodeEMH);
    if (comments != null) {
      addCommentaire(comments, nodeEMH);
    }

    if (emh.isRelationsEMHNotEmpty()) {
      final List<RelationEMH> relationEMH = emh.getRelationEMH();
      final TreeNodeString nodeRelation = new TreeNodeString(EMHUiMessages.getMessage("EMHTreeView.relation"));
      nodeEMH.add(nodeRelation);
      for (final RelationEMH relation : relationEMH) {
        if (relation.getEmh() != null) {
          final TreeNodeCleValeur noderelationEMH = new TreeNodeCleValeur(relation.getType(), relation.getEmh().getNom());
          noderelationEMH.setLinkIdForColumnOne(relation.getEmh().getId());
          nodeRelation.add(noderelationEMH);
          addDescribeNode(relation, noderelationEMH, "emh", ref);
        }
      }
    }

    if (emh.isInfosEMHNotEmpty()) {
      final TreeNodeString nodeInfo = new TreeNodeString(EMHUiMessages.getMessage("EMHTreeView.infosEMH"));
      nodeEMH.add(nodeInfo);

      for (final InfosEMH info : emh.getInfosEMH()) {
        boolean addInfo = true;
        final String name = info.getTypei18n();
        AbstractTreeNode noderelationEMH = null;
        if (info instanceof ObjetWithID) {
          noderelationEMH = new TreeNodeCleValeur(name, ((ObjetNomme) info).getNom());
          ref.put(((ObjetWithID) info).getId(), noderelationEMH);
        } else if (info instanceof DonFrtConteneur) {
          addInfo = false;
          final List<DonFrt> listFrt = ((DonFrtConteneur) info).getListFrt();
          noderelationEMH = new TreeNodeCleValeur("DonFrtConteneur", listFrt.size() + " frottements");
          for (final DonFrt donFrt : listFrt) {
            final TreeNodeCleValeur nodeLeaf = new TreeNodeCleValeur(donFrt.getId(),
                    donFrt.getType() + " "
                    + donFrt.getLoi().getEvolutionFF().getPtEvolutionFF().size() + " valeurs");
            nodeLeaf.setUserObject(donFrt);
            ref.put(donFrt.getId(), nodeLeaf);
            noderelationEMH.add(nodeLeaf);
          }
        } else if (info instanceof DonLoiHYConteneur) {
          addInfo = false;
          final List<Loi> lois = ((DonLoiHYConteneur) info).getLois();
          noderelationEMH = new TreeNodeCleValeur("DonLoiHYConteneur", lois.size() + " lois");
          for (final Loi loi : lois) {

            final TreeNodeCleValeur leaf = new TreeNodeCleValeur(loi.getId(),
                    (loi.getUsed() ? "" : "Non utilisée. ")
                    + "Type: " + loi.getType().getId() + " " + loi.getEvolutionFF().getPtEvolutionFF().size() + " valeurs");

            leaf.add(new TreeNodeCleValeur("Commentaire", loi.getCommentaire()));
            noderelationEMH.add(leaf);
            ref.put(loi.getId(), leaf);
            leaf.setUserObject(loi);
          }
        } else if (info instanceof ResultatCalcul) {
          noderelationEMH = new TreeNodeCleValeur(name, "");
          noderelationEMH.setUserObject(info);
        } else {
          noderelationEMH = new TreeNodeString(name);
        }
        nodeInfo.add(noderelationEMH);
        if (addInfo) {
          addDescribeNode(info, noderelationEMH, null, ref);
        }

      }
    }
  }

  /**
   * Construit une tree model basique avec els EMH associés.
   *
   * @return modele du tree.
   */
  public DefaultTreeTableModel buildTreeScenario(final CrueConfigMetier configMetier, final EMHScenario data,
                                                 final Map<String, AbstractTreeNode> ref) {
    propertyDefinitionContainer = configMetier;
    toStringTransformer = ToStringTransfomerFactory.create(propertyDefinitionContainer,DecimalFormatEpsilonEnum.COMPARISON);
    final TreeNodeString root = new TreeNodeString("root");
    addNodesForEMH(root, data, ref, data);

    final TreeNodeString rootModels = new TreeNodeString("Modèles");
    root.add(rootModels);

    final TreeNodeString rootSubModels = new TreeNodeString("Sous-modèles");
    root.add(rootSubModels);

    for (final EMHModeleBase emhModel : data.getModeles()) {
      addNodesForEMH(rootModels, emhModel, ref, emhModel);

      for (final EMHSousModele emh : emhModel.getSousModeles()) {
        addNodesForEMH(rootSubModels, emh, ref, emh);

      }
    }

    final TreeNodeString rootNodes = new TreeNodeString("Noeuds");
    root.add(rootNodes);

    for (final EMH emh : data.getNoeuds()) {
      addNodesForEMH(rootNodes, emh, ref);

    }

    final TreeNodeString rootBranches = new TreeNodeString("Branches");
    root.add(rootBranches);

    for (final EMH emh : data.getBranches()) {
      addNodesForEMH(rootBranches, emh, ref);

    }

    final TreeNodeString rootCasiers = new TreeNodeString("Casiers");
    root.add(rootCasiers);

    for (final EMH emh : data.getCasiers()) {
      addNodesForEMH(rootCasiers, emh, ref);
    }

    final TreeNodeString rootSections = new TreeNodeString("Sections");
    root.add(rootSections);
    for (final EMH emh : data.getSections()) {
      addNodesForEMH(rootSections, emh, ref);

    }
    final DefaultTreeTableModel modeleTree = new DefaultTreeTableModel(root);

    return modeleTree;
  }

  private static boolean isCollectOrIterator(final Object valueObject) {
    return valueObject instanceof Iterator || valueObject instanceof Collection;
  }

  private String durationToString(final String key, final Duration d) {
    final double sec = (d.getMillis()) / 1000d;
    final NumberFormat formatter = this.propertyDefinitionContainer.getFormatter(key,DecimalFormatEpsilonEnum.COMPARISON);
    if (formatter == null) {
      return Double.toString(sec);
    }
    return formatter.format(sec) + " s";
  }

  private String objectToString(final String key, final Object entryValue) {
    if (entryValue == null) {
      return StringUtils.EMPTY;
    }
    if (entryValue instanceof ToStringTransformable) {
      return ((ToStringTransformable) entryValue).toString(this.propertyDefinitionContainer, EnumToString.COMPLETE, DecimalFormatEpsilonEnum.COMPARISON);
    }
    if (propertyDefinitionContainer.isPropertyDefined(key)) {
      final ItemVariable property = propertyDefinitionContainer.getProperty(key);
      if (property.getNature() != null && property.getNature().isEnum()) {
        return ObjectUtils.toString(entryValue);
      }
      return TransformerEMHHelper.formatFromPropertyName(key, entryValue, propertyDefinitionContainer,DecimalFormatEpsilonEnum.COMPARISON);
    }
    if (entryValue.getClass().isEnum()) {
      return entryValue.toString();
    }
    if (entryValue instanceof Duration) {
      return durationToString(key, ((Duration) entryValue));
    }
    if (entryValue instanceof AbstractPartial) {
      return entryValue.toString();
    }
    final String value = toStringTransformer.transform(entryValue);
    if (entryValue instanceof PlanimetrageNbrPdzCst) {//to be removed !
      return value;
    }
    return StringUtils.removeStart(value.trim(), ToStringHelper.typeToi18n(entryValue.getClass().getSimpleName()));
  }

  private static void addCommentaire(final CommentaireContainer contManager, final AbstractMutableTreeTableNode parent) {
    final Map<CrueFileType, CommentaireItem> commentaires = contManager.getCommentairesManager().getContent();
    if (MapUtils.isNotEmpty(commentaires)) {
      final TreeNodeString res = new TreeNodeString("Commentaires");
      parent.add(res);
      for (final Entry<CrueFileType, CommentaireItem> entry : commentaires.entrySet()) {
        res.add(new TreeNodeCleValeur(entry.getKey().name(), entry.getValue().getCommentaire()));

      }
    }
  }
}
