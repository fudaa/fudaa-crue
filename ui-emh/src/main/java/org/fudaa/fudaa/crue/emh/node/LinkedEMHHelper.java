/*
 GPL 2
 */
package org.fudaa.fudaa.crue.emh.node;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.fudaa.crue.emh.common.OutlineSelectionHistory;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.NodePopupFactory;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.nodes.NodeOp;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

/**
 * @author Frederic Deniger
 */
public class LinkedEMHHelper {

    public static void installShortcutsAndCellRenderer(final OutlineView outlineView, final ExplorerManager em) {
        outlineView.getOutline().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // Implémentation pour l'Hyperlien de la colonne Valeur de la vue Réseau (équivalent au "Aller à" du clic droit)
                if (e.isAltDown()) {
                    int columnAtPoint = outlineView.getOutline().columnAtPoint(e.getPoint());
                    if (columnAtPoint != 1) {
                        return;
                    }
                    followLink(em);
                }
            }
        });

        outlineView.setNodePopupFactory(new NodePopupFactory() {
            @Override
            public JPopupMenu createPopupMenu(int row, int column, Node[] selectedNodes,
                                              Component component) {

                // lecture des actions autorisés sur le node
                Action[] actions = NodeOp.findActions(selectedNodes);
                JPopupMenu res = Utilities.actionsToPopup(actions, component);

                if (selectedNodes != null && selectedNodes.length == 1 && selectedNodes[0] instanceof NodeEMHDefault) {
                    NodeEMHDefault emhNode = (NodeEMHDefault) selectedNodes[0];

                    if (emhNode.isLinkableNode()) {
                        // ajout du menu "Aller à" + événements associé uniquement si Node Linkable
                        res.addSeparator();
                        JMenuItem add = res.add(NbBundle.getMessage(LinkedEMHHelper.class, "FollowLink.Action"));
                        add.setToolTipText(NbBundle.getMessage(LinkedEMHHelper.class, "FollowLink.ActionTooltip"));
                        add.setEnabled(emhNode.isLinkableNode());

                        add.addActionListener(e -> followLink(em));
                    }
                }

                return res;
            }
        });
        outlineView.getOutline().getColumnModel().getColumn(1).setCellRenderer(new LinkedEMHTableCellRenderer(outlineView.getOutline().getDefaultRenderer(
                Node.Property.class)));
    }

    public static boolean followLink(ExplorerManager em) {
        Node[] selectedNodes = em.getSelectedNodes();
        if (selectedNodes.length != 1) {
            return true;
        }
        NodeEMHDefault node = (NodeEMHDefault) selectedNodes[0];
        if (node.isLinkableNode()) {
            EMH emh = node.getLookup().lookup(EMH.class);
            if (emh != null) {
                final Set<Long> uids = new HashSet<>();
                uids.add(emh.getUiId());
                selectEMHs(em, uids);

            }
        }
        return false;
    }

    /**
     * Sélection dans em des EMHs dont l'Uid est dans selectedUid
     *
     * @param em          l'ExplorerManager associé au composant graphique
     * @param selectedUid liste des Uid des EMHs à sélectionner
     */
    public static void selectEMHs(ExplorerManager em, Set<Long> selectedUid) {
        // Sélection des noeuds de 2ème niveau (groupement par CatEMH)
        Node[] nodes = em.getRootContext().getChildren().getNodes();
        List<Node> toSelect = new ArrayList<>();

        selectSimpleEMH(selectedUid, nodes, toSelect);

        try {
            // sélection des noeuds pointés par le parcours
            em.setSelectedNodes(toSelect.toArray(new Node[0]));
        } catch (PropertyVetoException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    private static void selectSimpleEMH(Set<Long> selectedUid, Node[] nodes, List<Node> toSelect) {
        if (nodes == null) {
            return;
        }
        if (selectedUid == null || selectedUid.isEmpty()) {
            return;
        }
        for (Node node : nodes) {
            EMH emh = node.getLookup() == null ? null : node.getLookup().lookup(EMH.class);
            if (emh != null && selectedUid.contains(emh.getUiId())) {
                toSelect.add(node);
            }
            if ((emh == null || emh.getCatType().isContainer()) && node.getChildren() != null) {
                selectSimpleEMH(selectedUid, node.getChildren().getNodes(), toSelect);
            }
        }

    }

    public static JPanel installBackNextButtons(OutlineView outlineView) {
        OutlineSelectionHistory history = new OutlineSelectionHistory(outlineView);
        JPanel pnTop = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final JButton btPrev = new JButton(history.getPreviousAction());
        btPrev.setText(StringUtils.EMPTY);
        pnTop.add(btPrev);
        final JButton btNext = new JButton(history.getNextAction());
        btNext.setText(StringUtils.EMPTY);
        pnTop.add(btNext);
        EbliLib.updateMapKeyStroke(outlineView, new Action[]{history.getPreviousAction(), history.getNextAction()});
        return pnTop;
    }
}
