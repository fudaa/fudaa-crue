/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.config.ccm;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierReaderXML.*;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

/**
 * @author deniger Test de CrueNatureLoader.
 */
public class CrueConfigMetierReaderXMLTest {
    /**
     */
    @Test
    public void testDaos() {
        final CrueConfigMetierReaderXML loader = new CrueConfigMetierReaderXML(TestCoeurConfig.INSTANCE);
        final CtuluLog log = new CtuluLog();
        final DaoConfigMetier readDaos = loader.readDaos("/CrueConfigMetierForTests.xml", log);
        computeLog(log);

        final DaoConfigMetier expectedDaos = this.getExpectedDaos();

        this.assertEqualsControlLois(expectedDaos.TypeControleLois, readDaos.TypeControleLois);
        this.assertEqualsExtrapolationLois(expectedDaos.TypeExtrapolLois, readDaos.TypeExtrapolLois);
        this.assertEqualsNumeriques(expectedDaos.TypeNumeriques, readDaos.TypeNumeriques);
        this.assertEqualsNatures(expectedDaos.Natures, readDaos.Natures);
        this.assertEqualsVariables(expectedDaos.Variables, readDaos.Variables);
        this.assertEqualsConfigLois(expectedDaos.ConfigLois, readDaos.ConfigLois);
        final List<DaoItemConstante> constantes = readDaos.Constantes.Constantes;
        assertEquals(2, constantes.size());
        final DaoItemConstante constante0 = constantes.get(0);
        assertEquals("DdPtgEpsilon", constante0.Nom);
        assertEquals("Nat_Z", constante0.Nature.NomRef);
        assertEquals("0.001", constante0.Valeur);
        assertEquals("0", constante0.MinNormalite.Valeur);
        assertEquals("+Infini", constante0.MaxNormalite.Valeur);
        assertEquals("0", constante0.MinValidite.Valeur);
        assertEquals("+Infini", constante0.MaxValidite.Valeur);

        final DaoItemConstante constante1 = constantes.get(1);
        assertEquals("DhLinSeuil", constante1.Nom);
        assertEquals("Nat_Beta", constante1.Nature.NomRef);
        assertEquals("0.1", constante1.Valeur);
        assertEquals("+0.00001", constante1.MinNormalite.Valeur);
        assertEquals("+0.1", constante1.MaxNormalite.Valeur);
        assertEquals("0", constante1.MinValidite.Valeur);
        assertEquals("+Infini", constante1.MaxValidite.Valeur);
    }

    private DaoConfigMetier getExpectedDaos() {
        final DaoConfigMetier daos = new DaoConfigMetier();

        daos.TypeControleLois = this.getControlLois();
        daos.TypeExtrapolLois = this.getExtrapolationLois();
        daos.TypeNumeriques = this.getNumeriques();
        daos.Natures = this.getNatures();
        daos.Variables = this.getVariables();
        daos.ConfigLois = this.getConfigLois();

        return daos;
    }

    private DaoControleLois getControlLois() {
        final DaoControleLois ctrlLois = new DaoControleLois();
        ctrlLois.Controles = new ArrayList<>();

        DaoControleLoi ctrlLoi = new DaoControleLoi();
        ctrlLoi.Nom = "Tco_Aucun";
        ctrlLois.Controles.add(ctrlLoi);

        ctrlLoi = new DaoControleLoi();
        ctrlLoi.Nom = "Tco_AbsCrt";
        ctrlLois.Controles.add(ctrlLoi);

        ctrlLoi = new DaoControleLoi();
        ctrlLoi.Nom = "Tco_AbsCrtStrict";
        ctrlLois.Controles.add(ctrlLoi);

        return ctrlLois;
    }

    private DaoExtrapolationLois getExtrapolationLois() {
        final DaoExtrapolationLois extraLois = new DaoExtrapolationLois();
        extraLois.Extrapolations = new ArrayList<>();

        DaoExtrapolationLoi extraLoi = new DaoExtrapolationLoi();
        extraLoi.Nom = "Tex_Aucun";
        extraLois.Extrapolations.add(extraLoi);

        extraLoi = new DaoExtrapolationLoi();
        extraLoi.Nom = "Tex_OrdCst";
        extraLois.Extrapolations.add(extraLoi);

        extraLoi = new DaoExtrapolationLoi();
        extraLoi.Nom = "Tex_OrdInterpolLineaire";
        extraLois.Extrapolations.add(extraLoi);

        return extraLois;
    }

    private DaoNumeriques getNumeriques() {
        final DaoNumeriques numeriques = new DaoNumeriques();
        numeriques.Numeriques = new ArrayList<>();

        DaoNumerique numerique = new DaoNumerique();
        numerique.Nom = "Tnu_Entier";
        numerique.Infini = "2000000000";
        numeriques.Numeriques.add(numerique);

        numerique = new DaoNumerique();
        numerique.Nom = "Tnu_Reel";
        numerique.Infini = "1.0E30";
        numeriques.Numeriques.add(numerique);

        return numeriques;
    }

    private DaoNatures getNatures() {
        final DaoNatures natures = new DaoNatures();
        natures.Natures = new ArrayList<>();

        DaoNature nature = new DaoNature();
        nature.Nom = "Nat_Beta";
        DaoReference reference = new DaoReference();
        reference.NomRef = "Tnu_Reel";
        nature.TypeNumerique = reference;
        nature.EpsilonComparaisonFC = "1.0E-5";
        nature.Unite = "";
        natures.Natures.add(nature);

        nature = new DaoNature();
        nature.Nom = "Nat_Z";
        reference = new DaoReference();
        reference.NomRef = "Tnu_Reel";
        nature.TypeNumerique = reference;
        nature.EpsilonComparaisonFC = "1.2E-3";
        nature.Unite = "m";
        natures.Natures.add(nature);

        return natures;
    }

    private DaoVariables getVariables() {
        final DaoVariables variables = new DaoVariables();
        variables.Variables = new ArrayList<>();

        DaoItemVariable variable = new DaoItemVariable();
        variable.Nom = "Beta";
        DaoReference reference = new DaoReference();
        reference.NomRef = "Nat_Beta";
        variable.Nature = reference;
        variable.ValeurDefaut = "1.0";
        DaoValeurStrictable minValidite = new DaoValeurStrictable();
        minValidite.Valeur = "0.0";
        variable.MinValidite = minValidite;
        DaoValeurStrictable maxValidite = new DaoValeurStrictable();
        maxValidite.Valeur = "+Infini";
        variable.MaxValidite = maxValidite;
        DaoValeurStrictable minNormalite = new DaoValeurStrictable();
        minNormalite.Valeur = "1.0";
        variable.MinNormalite = minNormalite;
        DaoValeurStrictable maxNormalite = new DaoValeurStrictable();
        maxNormalite.Valeur = "1.5";
        variable.MaxNormalite = maxNormalite;
        variables.Variables.add(variable);

        variable = new DaoItemVariable();
        variable.Nom = "Z";
        reference = new DaoReference();
        reference.NomRef = "Nat_Z";
        variable.Nature = reference;
        variable.ValeurDefaut = "";
        minValidite = new DaoValeurStrictable();
        minValidite.Valeur = "-Infini";
        variable.MinValidite = minValidite;
        maxValidite = new DaoValeurStrictable();
        maxValidite.Valeur = "+Infini";
        variable.MaxValidite = maxValidite;
        minNormalite = new DaoValeurStrictable();
        minNormalite.Valeur = "0.0";
        variable.MinNormalite = minNormalite;
        maxNormalite = new DaoValeurStrictable();
        maxNormalite.Valeur = "4000.0";
        variable.MaxNormalite = maxNormalite;
        variables.Variables.add(variable);

        return variables;
    }

    private DaoConfigLois getConfigLois() {
        final DaoConfigLois lois = new DaoConfigLois();
        lois.Lois = new ArrayList<>();

        DaoConfigLoi loi = new DaoConfigLoi();
        loi.Nom = "Fk";
        loi.Commentaire = "Frottement donné en Strickler";
        DaoReference typeControleLoi = new DaoReference();
        typeControleLoi.NomRef = "Tco_AbsCrtStrict";
        loi.TypeControleLoi = typeControleLoi;
        DaoReference extrapolInf = new DaoReference();
        extrapolInf.NomRef = "Tex_OrdCst";
        loi.ExtrapolInf = extrapolInf;
        DaoReference extrapolSup = new DaoReference();
        extrapolSup.NomRef = "Tex_OrdCst";
        loi.ExtrapolSup = extrapolSup;
        DaoReference varAbscisse = new DaoReference();
        varAbscisse.NomRef = "Z";
        loi.VarAbscisse = varAbscisse;
        DaoReference varOrdonnee = new DaoReference();
        varOrdonnee.NomRef = "K";
        loi.VarOrdonnee = varOrdonnee;
        lois.Lois.add(loi);

        loi = new DaoConfigLoi();
        loi.Nom = "Fn";
        loi.Commentaire = "Frottement donné en Manning";
        typeControleLoi = new DaoReference();
        typeControleLoi.NomRef = "Tco_AbsCrtStrict";
        loi.TypeControleLoi = typeControleLoi;
        extrapolInf = new DaoReference();
        extrapolInf.NomRef = "Tex_OrdCst";
        loi.ExtrapolInf = extrapolInf;
        extrapolSup = new DaoReference();
        extrapolSup.NomRef = "Tex_OrdCst";
        loi.ExtrapolSup = extrapolSup;
        varAbscisse = new DaoReference();
        varAbscisse.NomRef = "Z";
        loi.VarAbscisse = varAbscisse;
        varOrdonnee = new DaoReference();
        varOrdonnee.NomRef = "N";
        loi.VarOrdonnee = varOrdonnee;
        lois.Lois.add(loi);

        loi = new DaoConfigLoi();
        loi.Nom = "LoiPtProfil";
        loi.Commentaire = "Profil en travers";
        typeControleLoi = new DaoReference();
        typeControleLoi.NomRef = "Tco_AbsCrt";
        loi.TypeControleLoi = typeControleLoi;
        extrapolInf = new DaoReference();
        extrapolInf.NomRef = "Tex_Aucun";
        loi.ExtrapolInf = extrapolInf;
        extrapolSup = new DaoReference();
        extrapolSup.NomRef = "Tex_Aucun";
        loi.ExtrapolSup = extrapolSup;
        varAbscisse = new DaoReference();
        varAbscisse.NomRef = "Xt";
        loi.VarAbscisse = varAbscisse;
        varOrdonnee = new DaoReference();
        varOrdonnee.NomRef = "Z";
        loi.VarOrdonnee = varOrdonnee;
        lois.Lois.add(loi);

        return lois;
    }

    private void assertEqualsReference(final DaoReference expected, final DaoReference actual) {
        assertEquals(expected.NomRef, expected.NomRef);
    }

    private void assertEqualsValeurStrictable(final DaoValeurStrictable expected, final DaoValeurStrictable actual) {
        assertEquals(expected.Valeur, actual.Valeur);
    }

    private void assertEqualsControlLois(final DaoControleLois expected, final DaoControleLois actual) {
        assertEquals(expected.Controles.size(), actual.Controles.size());

        for (int i = 0; i < expected.Controles.size(); i++) {
            assertEquals(expected.Controles.get(i).Nom, actual.Controles.get(i).Nom);
        }
    }

    private void assertEqualsExtrapolationLois(final DaoExtrapolationLois expected, final DaoExtrapolationLois actual) {
        assertEquals(expected.Extrapolations.size(), actual.Extrapolations.size());

        for (int i = 0; i < expected.Extrapolations.size(); i++) {
            assertEquals(expected.Extrapolations.get(i).Nom, actual.Extrapolations.get(i).Nom);
        }
    }

    private void assertEqualsNumeriques(final DaoNumeriques expected, final DaoNumeriques actual) {
        assertEquals(expected.Numeriques.size(), actual.Numeriques.size());

        for (int i = 0; i < expected.Numeriques.size(); i++) {
            assertEquals(expected.Numeriques.get(i).Nom, actual.Numeriques.get(i).Nom);
            assertEquals(expected.Numeriques.get(i).Infini, actual.Numeriques.get(i).Infini);
        }
    }

    private void assertEqualsNatures(final DaoNatures expected, final DaoNatures actual) {
        assertEquals(expected.Natures.size(), actual.Natures.size());

        for (int i = 0; i < expected.Natures.size(); i++) {
            assertEquals(expected.Natures.get(i).Nom, actual.Natures.get(i).Nom);
            this.assertEqualsReference(expected.Natures.get(i).TypeNumerique, actual.Natures.get(i).TypeNumerique);
            assertEquals(expected.Natures.get(i).EpsilonComparaisonFC, actual.Natures.get(i).EpsilonComparaisonFC);
            assertEquals(expected.Natures.get(i).Unite, actual.Natures.get(i).Unite);
        }
    }

    private void assertEqualsVariables(final DaoVariables expected, final DaoVariables actual) {
        assertEquals(expected.Variables.size(), actual.Variables.size());

        for (int i = 0; i < expected.Variables.size(); i++) {
            assertEquals(expected.Variables.get(i).Nom, actual.Variables.get(i).Nom);
            this.assertEqualsReference(expected.Variables.get(i).Nature, actual.Variables.get(i).Nature);
            assertEquals(expected.Variables.get(i).ValeurDefaut, actual.Variables.get(i).ValeurDefaut);
            this.assertEqualsValeurStrictable(expected.Variables.get(i).MinValidite, actual.Variables.get(i).MinValidite);
            this.assertEqualsValeurStrictable(expected.Variables.get(i).MaxValidite, actual.Variables.get(i).MaxValidite);
            this.assertEqualsValeurStrictable(expected.Variables.get(i).MinNormalite, actual.Variables.get(i).MinNormalite);
            this.assertEqualsValeurStrictable(expected.Variables.get(i).MaxNormalite, actual.Variables.get(i).MaxNormalite);
        }
    }

    private void assertEqualsConfigLois(final DaoConfigLois expected, final DaoConfigLois actual) {
        assertEquals(expected.Lois.size(), actual.Lois.size());

        for (int i = 0; i < expected.Lois.size(); i++) {
            assertEquals(expected.Lois.get(i).Nom, actual.Lois.get(i).Nom);
            assertEquals(expected.Lois.get(i).Commentaire, actual.Lois.get(i).Commentaire);
            this.assertEqualsReference(expected.Lois.get(i).TypeControleLoi, actual.Lois.get(i).TypeControleLoi);
            this.assertEqualsReference(expected.Lois.get(i).ExtrapolInf, actual.Lois.get(i).ExtrapolInf);
            this.assertEqualsReference(expected.Lois.get(i).ExtrapolSup, actual.Lois.get(i).ExtrapolSup);
            this.assertEqualsReference(expected.Lois.get(i).VarAbscisse, actual.Lois.get(i).VarAbscisse);
            this.assertEqualsReference(expected.Lois.get(i).VarOrdonnee, actual.Lois.get(i).VarOrdonnee);
        }
    }

    @Test
    public void testXsd() {
        final CrueConfigMetierReaderXML loader = new CrueConfigMetierReaderXML(TestCoeurConfig.INSTANCE);
        final CtuluLog log = new CtuluLog();
        final boolean valide = loader.isValide("/CrueConfigMetierForTests.xml", log);
        computeLog(log);
        assertTrue(valide);
    }

    private void computeLog(final CtuluLog log) {
        if (log.containsErrorOrSevereError()) {
            log.printResume();
        }
    }

    @Test
    public void testNatureLoader() {
        final CrueConfigMetier metier = readNature();
        PropertyNature nature = metier.getNature("nat_Beta");
        assertEquals("nat_Beta", nature.getNom());
        assertEquals("tnu_Reel", nature.getTypeNumerique().getNom());
        assertEquals(1.0E-5, nature.getEpsilon().getEpsilonComparaison(), 1E-15);
        assertEquals(1.0E-4, nature.getEpsilon().getEpsilonPresentation(), 1E-15);

        assertEquals("", nature.getUnite());
        nature = metier.getNature("nat_Z");
        assertEquals("nat_Z", nature.getNom());
        assertEquals("tnu_Reel", nature.getTypeNumerique().getNom());
        assertEquals(1.2E-3, nature.getEpsilon().getEpsilonComparaison(), 1E-15);
        assertEquals(1.2E-4, nature.getEpsilon().getEpsilonPresentation(), 1E-15);
        assertEquals("m", nature.getUnite());
        final PropertyValidator validator = metier.getProperty("beta").getValidator();
        assertFalse(validator.getRangeValidate().isMaxStrict());
        assertTrue(validator.getRangeValidate().isMinStrict());
    }

    private CrueConfigMetier readNature() {
        final CrueConfigMetierReader loader = new CrueConfigMetierReader(TestCoeurConfig.INSTANCE);
        final CrueIOResu<CrueConfigMetier> load = loader.readConfigMetier("/CrueConfigMetierForTests.xml");
        assertTrue(load.getAnalyse().containsErrorOrSevereError());
        assertFalse(load.getAnalyse().containsWarnings());
        return load.getMetier();
    }

    @Test
    public void testLoisAndVerbositeIncomplete() {
        final CrueConfigMetierReader loader = new CrueConfigMetierReader(TestCoeurConfig.INSTANCE);
        final CrueIOResu<CrueConfigMetier> load = loader.readConfigMetier("/CrueConfigMetierForTestsFalse.xml");
        final CtuluLog analyse = load.getAnalyse();
        assertTrue(analyse.containsSevereError());
        //la premiere erreur concerne les lois manquantes:
        assertEquals(2, analyse.getRecords().size());
        final Iterator<CtuluLogRecord> iterator = analyse.getRecords().iterator();
        CtuluLogRecord next=iterator.next();
        assertEquals("configLoi.LoiNotDefinedFound.error", next.getMsg());
        next = iterator.next();
        assertEquals("config.verbositeRequiredLevelNotFound", next.getMsg());
        assertEquals("INFO", next.getArgs()[0]);
    }

    /**
     * Teste de CruePropertyLoader.
     */
    @Test
    public void testPropertyLoader() {
        final CrueConfigMetier metier = readNature();
        final Map<String, Double> defaultValues = getDefaultValues();
        for (final String key : defaultValues.keySet()) {
            assertTrue(metier.isPropertyDefined(key));
            assertTrue("test for " + key, metier.getProperty(key).isDefaultOrConstantValue(defaultValues.get(key)));
        }
        //test des enums
        final PropertyNature property = metier.getPropertyEnum("Ten_Severite");
        assertTrue(property.isEnum());
        final List<ItemEnum> enumValues = property.getEnumValues();
        assertEquals(3, enumValues.size());
        assertEquals("DEBUG3", enumValues.get(0).getName());
        assertEquals(0, enumValues.get(0).getId());
        assertEquals("INFO", enumValues.get(1).getName());
        assertEquals(1, enumValues.get(1).getId());
        assertEquals("FATAL", enumValues.get(2).getName());
        assertEquals(2, enumValues.get(2).getId());
        assertNotNull(metier.getVerbositeManager());
        ItemConstant constante = metier.getConstante("DhLinSeuil");
        assertEquals(0.1d, constante.getConstantValue().doubleValue(), 1e-15);
        constante = metier.getConstante("DdPtgEpsilon");
        assertEquals(0.001d, constante.getConstantValue().doubleValue(), 1e-15);
        assertNotNull(metier.getPropertyEnum("Ten_FormulePdc"));
    }

    private Map<String, Double> getDefaultValues() {
        final Map<String, Double> defaultValues = new HashMap<>();
        defaultValues.put("beta", 1.0);
        defaultValues.put("z", null);
        return defaultValues;
    }
}
