/*
GPL 2
 */
package org.fudaa.dodico.crue.projet.conf;

import java.util.Collections;

import org.fudaa.dodico.crue.config.ccm.Crue9DecimalFormat;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Frederic Deniger
 */
public class Crue9DecimalFormatBuilderTest {

  public Crue9DecimalFormatBuilderTest() {
  }

  @Test
  public void testIntialize() {
    final Configuration configuration = new Configuration();
    final SiteConfiguration siteConfiguration = new SiteConfiguration();
    configuration.setSite(siteConfiguration);
    final Crue9DecimalFormatBuilder builder = new Crue9DecimalFormatBuilder();
    final CrueConfigMetier ccm = CrueConfigMetierForTest.DEFAULT;
    builder.intialize(configuration, ccm);
    assertEquals(Crue9DecimalFormat.STATE.NON_ACTIVE, ccm.getCrue9DecimalFormat().getState());
    assertEquals(0, ccm.getCrue9DecimalFormat().getNumberOfDigits());

    final SiteOption option = new SiteOption(OptionsEnum.NB_EXPORT_CRUE9_SIGNIFICANT_FIGURES.getId(), "8", "", true);
    siteConfiguration.setOptions(Collections.singletonList(option));
    builder.intialize(configuration, ccm);
    assertEquals(Crue9DecimalFormat.STATE.ACTIVE, ccm.getCrue9DecimalFormat().getState());
    assertEquals(8, ccm.getCrue9DecimalFormat().getNumberOfDigits());

  }

}
