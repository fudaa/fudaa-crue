/*
 *  License GPL v2
 */
package org.fudaa.dodico.crue.config.ccm;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Un loader par default.
 *
 * @author deniger
 */
public final class CrueConfigMetierForTestTest {
    @Test
    public void testIsNotNull() {
        if (CrueConfigMetierForTest.errors.containsErrorOrSevereError()) {
            CrueConfigMetierForTest.errors.printResume();
        }
        assertFalse(CrueConfigMetierForTest.errors.containsErrorOrSevereError());
    }

    @Test
    public void testIsNotNull1_2() {
        assertFalse(CrueConfigMetierForTest.errors_1_2.containsErrorOrSevereError());
    }

    @Test
    public void testIsNotNull1_1_1() {
        assertFalse(CrueConfigMetierForTest.errors_1_1_1.containsErrorOrSevereError());
    }

    @Test
    public void testDebug3SeverityIndex() {
        final PropertyNature propertyEnum = CrueConfigMetierForTest.DEFAULT.getPropertyEnum("Ten_Severite");
        final List<ItemEnum> enumValues = new ArrayList<>(propertyEnum.getEnumValues());
        Collections.sort(enumValues);
        assertEquals(100, enumValues.get(enumValues.size() - 1).getId());
    }
}
