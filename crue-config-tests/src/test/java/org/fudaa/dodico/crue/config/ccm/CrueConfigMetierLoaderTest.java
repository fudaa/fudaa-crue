/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.config.ccm;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.config.lit.LitNommeLimite;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.joda.time.Duration;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author deniger Test de CrueNatureLoader.
 */
public class CrueConfigMetierLoaderTest {

  @Test
  public void testXsd() {
    final CrueConfigMetierReaderXML loader = new CrueConfigMetierReaderXML(TestCoeurConfig.INSTANCE);
    final CtuluLog log = new CtuluLog();
    final boolean valide = loader.isValide(CrueConfigMetierForTest.CONFIGMETIER_DEFAULT_FILE, log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    assertTrue(valide);
  }
  @Test
  public void testXsd_1_2() {
    final CrueConfigMetierReaderXML loader = new CrueConfigMetierReaderXML(TestCoeurConfig.INSTANCE_1_2);
    final CtuluLog log = new CtuluLog();
    final boolean valide = loader.isValide(CrueConfigMetierForTest.CONFIGMETIER_FILENAME_V_1_2, log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    assertTrue(valide);
  }

  @Test
  public void testXsd1_1_1() {
    final CrueConfigMetierReaderXML loader = new CrueConfigMetierReaderXML(TestCoeurConfig.INSTANCE_1_1_1);
    final CtuluLog log = new CtuluLog();
    final boolean valide = loader.isValide(CrueConfigMetierForTest.CONFIGMETIER_FILENAME_V_1_1_1, log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    assertTrue(valide);
  }

  @Test
  public void testPdtPerm() {
    final CrueConfigMetier props = CrueConfigMetierForTest.DEFAULT;
    final int val = (int) props.getDefaultDoubleValue("pdtPerm");
    assertEquals(3600, val);
    final Duration defaultDurationValue = props.getDefaultDurationValue("pdtPerm");
    assertEquals(3600, defaultDurationValue.getStandardSeconds());
  }

  @Test
  public void testEpsilon() {
    final CrueConfigMetier props = CrueConfigMetierForTest.DEFAULT;
    final double val = props.getNature("nat_CoefPdc").getEpsilon().getEpsilonComparaison();
    assertEquals(1E-5, val, 1E-10);
  }

  @Test
  public void testEpsilon1_1_1() {
    final CrueConfigMetier props = CrueConfigMetierForTest.DEFAULT_1_1_1;
    final double val = props.getNature("nat_CoefPdc").getEpsilon().getEpsilonComparaison();
    assertEquals(1E-5, val, 1E-10);
  }

  @Test
  public void testLitNommes() {
    final CrueConfigMetier props = CrueConfigMetierForTest.DEFAULT;
    final CrueConfigMetierLitNomme litNomme = props.getLitNomme();
    assertEquals("Lt_Mineur", litNomme.getMineur().getNom());
    assertEquals("Lt_StoD", litNomme.getStockageStart().getNom());
    assertEquals("Lt_MajD", litNomme.getMajeurStart().getNom());
    assertEquals("Lt_StoG", litNomme.getStockageEnd().getNom());
    assertEquals("Lt_MajG", litNomme.getMajeurEnd().getNom());

    assertEquals("Et_AxeHyd", litNomme.getEtiquetteAxeHyd().getAsString());
    assertEquals("Et_LimLit", litNomme.getEtiquetteLimLit().getAsString());
    assertEquals("Et_Thalweg", litNomme.getEtiquetteThalweg().getAsString());
  }

  @Test
  public void testLimites() {
    final CrueConfigMetier props = CrueConfigMetierForTest.DEFAULT;
    final CrueConfigMetierLitNomme litNomme = props.getLitNomme();
    final List<LitNommeLimite> limites = litNomme.getLimites();
    assertEquals(6, limites.size());
    for (int i = 0; i < limites.size(); i++) {
      final LitNommeLimite litNommeLimite = limites.get(i);
      assertEquals(i, litNommeLimite.getIdx());
    }
    assertEquals("RD", limites.get(0).getNom());
    assertEquals("Et_StoD-MajD", limites.get(1).getNom());
    assertEquals("RG", limites.get(5).getNom());
  }

  @Test
  public void testDefaultFile() {
    final CrueConfigMetierLoader loader = new CrueConfigMetierLoader();
    final CrueIOResu<CrueConfigMetier> load = loader.load(CrueConfigMetierForTest.CONFIGMETIER_DEFAULT_FILE, TestCoeurConfig.INSTANCE);
    assertNotNull(load);
    AbstractTestParent.testAnalyser(load.getAnalyse());
  }

}
