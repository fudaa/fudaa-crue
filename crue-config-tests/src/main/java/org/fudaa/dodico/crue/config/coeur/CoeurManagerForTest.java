/*
 *  License GPL v2
 */
package org.fudaa.dodico.crue.config.coeur;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Cree des coeur pour les tests.
 *
 * @author deniger
 */
public abstract class CoeurManagerForTest {
  public static final String CRUE10_1_NAME = "Crue10.1";
  public static final String CRUE10_2_NAME = "Crue10.2";
  public static final String CRUE10_2B_NAME = "Crue10.2b";

  public static CoeurManager createDefault() {
    final CoeurConfig c1_2 = TestCoeurConfig.INSTANCE_1_2;
    final CoeurConfig c1_1_1 = TestCoeurConfig.INSTANCE_1_1_1;
    return new CoeurManager(null, Arrays.asList(c1_2, c1_1_1));
  }

  public static CoeurManager create(final File baseDir) {
    final ArrayList<CoeurConfig> configs = new ArrayList<>();

    final CoeurConfig c10_1 = new TestCoeurConfig();
    c10_1.setXsdVersion("1.1.1");
    c10_1.loadCrueConfigMetier(CrueConfigMetierForTest.DEFAULT);
    c10_1.setCrueVertionTypeOTFA(CrueVersionType.CRUE10);
    c10_1.setName(CoeurManagerForTest.CRUE10_1_NAME);
    c10_1.setUsedByDefault(true);
    configs.add(c10_1);

    CoeurConfig c10_2 = new TestCoeurConfig();
    c10_2.setXsdVersion("1.2");
    c10_2.loadCrueConfigMetier(CrueConfigMetierForTest.DEFAULT);
    c10_2.setName(CoeurManagerForTest.CRUE10_2_NAME);
    c10_2.setUsedByDefault(true);
    c10_2.setCrueVertionTypeOTFA(CrueVersionType.CRUE10);
    configs.add(c10_2);
    c10_2 = new TestCoeurConfig();
    c10_2.setXsdVersion("1.2");
    c10_2.loadCrueConfigMetier(CrueConfigMetierForTest.DEFAULT);
    c10_2.setCrueVertionTypeOTFA(CrueVersionType.CRUE10);
    c10_2.setName(CoeurManagerForTest.CRUE10_2B_NAME);
    configs.add(c10_2);
    return new CoeurManager(baseDir, configs);
  }
}
