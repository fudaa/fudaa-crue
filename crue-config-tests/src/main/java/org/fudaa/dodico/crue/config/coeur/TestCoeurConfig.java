/*
 *  License GPL v2
 */
package org.fudaa.dodico.crue.config.coeur;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;

import java.net.URL;
import java.util.Collections;
import java.util.List;

/**
 * Classe utilisée pour les tests uniquement.
 *
 * @author Fred Deniger
 */
public class TestCoeurConfig extends CoeurConfig {
  public static final TestCoeurConfig INSTANCE = new TestCoeurConfig(Crue10VersionConfig.getLastVersion());
  public static final TestCoeurConfig INSTANCE_1_2 = new TestCoeurConfig(Crue10VersionConfig.V_1_2);
  public static final TestCoeurConfig INSTANCE_1_1_1 = new TestCoeurConfig(Crue10VersionConfig.V_1_1_1);

  public static TestCoeurConfig getInstance(final String version) {
    return new TestCoeurConfig(version);
  }

  private TestCoeurConfig(final String xsdVersion) {
    this(xsdVersion, xsdVersion);
  }

  public TestCoeurConfig() {
  }

  private TestCoeurConfig(final String xsdVersion, final String name) {
    setXsdVersion(xsdVersion);
    setUsedByDefault(true);
    setName(name);
  }

  @Override
  public List<String> getAvailableLanguagesForMessage() {
    return Collections.singletonList("fr_FR");
  }

  @Override
  public String getMessageURL(final String local) {
    return "/log/fr_FR.msg.xml";
  }

  @Override
  public URL getDefaultFile(final FileType fileType) {
    return getClass().getResource("/default-" + getXsdVersion() + "." + fileType.getExtension());
  }

  @Override
  public String getXsdUrl(final String xsdFile) {
    return "/xsd/" + xsdFile;
  }

  @Override
  public String getCrueConfigMetierURL() {
    if (Crue10VersionConfig.V_1_1_1.equals(getXsdVersion())) {
      return CrueConfigMetierForTest.CONFIGMETIER_FILENAME_V_1_1_1;
    }
    if (Crue10VersionConfig.V_1_2.equals(getXsdVersion())) {
      return CrueConfigMetierForTest.CONFIGMETIER_FILENAME_V_1_2;
    }
    return CrueConfigMetierForTest.CONFIGMETIER_DEFAULT_FILE;
  }

  @Override
  public CrueConfigMetier getCrueConfigMetier() {
    if (Crue10VersionConfig.V_1_1_1.equals(getXsdVersion())) {
      return CrueConfigMetierForTest.DEFAULT_1_1_1;
    }
    return CrueConfigMetierForTest.DEFAULT;
  }
}
