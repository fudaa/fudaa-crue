/*
 *  License GPL v2
 */
package org.fudaa.dodico.crue.config.ccm;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Un loader par default.
 *
 * @author deniger
 */
public final class CrueConfigMetierForTest {

   final static CtuluLog errors;
   final static CtuluLog errors_1_1_1;
   final static CtuluLog errors_1_2;
  public static final String CONFIGMETIER_FILENAME_V_1_1_1 = "/CrueConfigMetier-1.1.1.xml";
  public static final String CONFIGMETIER_FILENAME_V_1_2 = "/CrueConfigMetier-1.2.xml";
  /**
   * la path a utiliser par defaut
   */
  public static final String CONFIGMETIER_DEFAULT_FILE = "/CrueConfigMetier.xml";
  public final static CrueConfigMetier DEFAULT;
  public final static CrueConfigMetier DEFAULT_1_1_1;
  public final static CrueConfigMetier DEFAULT_1_2;

  static {
    CrueConfigMetierReader loader = new CrueConfigMetierReader(TestCoeurConfig.INSTANCE);
    CrueIOResu<CrueConfigMetier> load = loader.readConfigMetier(CONFIGMETIER_DEFAULT_FILE);
    if (!load.getAnalyse().isEmpty()) {
      Logger.getLogger(CrueConfigMetierForTest.class.getName()).log(Level.SEVERE, load.getAnalyse().getResume());
    }
    DEFAULT = load.getMetier();
    errors = load.getAnalyse();

    loader = new CrueConfigMetierReader(TestCoeurConfig.INSTANCE_1_2);
    load = loader.readConfigMetier(CONFIGMETIER_FILENAME_V_1_2);
    if (!load.getAnalyse().isEmpty()) {
      Logger.getLogger(CrueConfigMetierForTest.class.getName()).log(Level.SEVERE, load.getAnalyse().getResume());
    }
    DEFAULT_1_2 = load.getMetier();
    errors_1_2 = load.getAnalyse();

    loader = new CrueConfigMetierReader(TestCoeurConfig.INSTANCE_1_1_1);
    load = loader.readConfigMetier(CONFIGMETIER_FILENAME_V_1_1_1);
    if (!load.getAnalyse().isEmpty()) {
      Logger.getLogger(CrueConfigMetierForTest.class.getName()).log(Level.SEVERE, load.getAnalyse().getResume());
    }
    DEFAULT_1_1_1 = load.getMetier();
    errors_1_1_1 = load.getAnalyse();

  }

}
