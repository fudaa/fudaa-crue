package org.fudaa.fudaa.crue.modelling.emh;

import java.util.Map;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;

/**
 *
 * @author deniger
 */
public class ModellingEMHNoeudComponentTest {

  public static void configure(AbstractModellingEMHTopComponent top, String name, EMHScenario scenario) {
    Map<String, EMH> toMapOfNom = TransformerHelper.toMapOfNom(scenario.getAllSimpleEMH());
    EMH nd = toMapOfNom.get(name);
    top.setEMHUid(nd.getUiId());
    top.scenarioLoaded();
  }

  public static void main(String[] args) {
    EMHScenario scenario = ModellingTestHelper.readScenario();
    ModellingEMHNoeudTopComponent top = new ModellingEMHNoeudTopComponent();
    configure(top, "N1", scenario);
//    ModellingTestHelper.configure(top, scenario);
    top.setEditable(true);
//    top.setScenario(scenario);
    ModellingTestHelper.display(top);
  }
}
