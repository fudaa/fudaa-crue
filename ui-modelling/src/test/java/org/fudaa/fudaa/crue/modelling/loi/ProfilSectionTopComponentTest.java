package org.fudaa.fudaa.crue.modelling.loi;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class ProfilSectionTopComponentTest {
  
  public static void configure(ProfilSectionTopComponent top, EMHScenario scenario) {
    top.scenarioLoaded();
  }
  
  @Test
  public void testClose() {
    System.setProperty("java.awt.headless", "true");//sur le moteur d'intégration continue c'est le cas.
    ProfilSectionTopComponent top = new ProfilSectionTopComponent();
    try {
      top.scenarioUnloaded();
    } catch (Exception e) {
      Assert.fail(e.getMessage());
      
    }
    
  }
  
  public static void main(String[] args) {
    EMHScenario scenario = ModellingTestHelper.readScenario();
    ProfilSectionTopComponent top = new ProfilSectionTopComponent();
    configure(top, scenario);
    top.setEditable(true);
    ModellingTestHelper.display(top);
  }
}
