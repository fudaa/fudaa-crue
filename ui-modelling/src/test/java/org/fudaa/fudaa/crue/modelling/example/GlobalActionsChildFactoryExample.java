/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.example;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.modelling.ModellingContainersTopComponent;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;

/**
 *
 * @author deniger
 */
public class GlobalActionsChildFactoryExample {

  public static void main(String[] args) {
    EMHScenario emhScenario = ModellingTestHelper.readScenario();
    ModellingContainersTopComponent topComponent = new ModellingContainersTopComponent();
    topComponent.testScenarioLoaded(emhScenario);
    ModellingTestHelper.display(topComponent);

  }
}
