package org.fudaa.fudaa.crue.modelling.list;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSectionSansGeometrie;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;

/**
 *
 * @author deniger
 */
public class ModellingListSectionTopComponentTest {

  public static void main(String[] args) {
    EMHScenario scenario = ModellingTestHelper.readScenario();
    EMHSousModele sousModele = scenario.getModeles().get(0).getSousModeles().get(0);
    EMHSectionSansGeometrie testSection = new EMHSectionSansGeometrie("St_Testing");
    EMHRelationFactory.addRelationContientEMH(sousModele, testSection);
    ModellingListSectionTopComponent top = new ModellingListSectionTopComponent();
    ModellingTestHelper.configure(top, scenario);
    top.setEditable(true);
    top.setScenario(scenario);
    ModellingTestHelper.display(top);
  }
}
