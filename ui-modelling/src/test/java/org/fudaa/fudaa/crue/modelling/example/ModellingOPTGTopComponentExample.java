/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.example;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;
import org.fudaa.fudaa.crue.modelling.calcul.ModellingModeleOPTGTopComponent;

/**
 *
 * @author Frédéric Deniger
 */
public class ModellingOPTGTopComponentExample {

  public static void main(String[] args) {
    EMHScenario scenario = ModellingTestHelper.readScenario();
    ModellingModeleOPTGTopComponent top = new ModellingModeleOPTGTopComponent();
    ModellingTestHelper.configure(top, scenario);
    top.testScenarioLoaded(scenario);
    top.setEditable(true);
    ModellingTestHelper.display(top);
  }
}
