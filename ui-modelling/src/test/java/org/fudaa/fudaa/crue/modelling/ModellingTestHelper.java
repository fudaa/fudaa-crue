package org.fudaa.fudaa.crue.modelling;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.test.ReadHelperForTest;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.IdRegistry;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.PerspectiveState;
import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.fudaa.fudaa.crue.modelling.calcul.AbstractModeleTopComponent;
import org.fudaa.fudaa.crue.modelling.list.AbstractModellingListTopComponent;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioServiceImpl;
import org.openide.util.Lookup;

import javax.swing.*;

/**
 * @author deniger
 */
public class ModellingTestHelper {
    public static EMHScenario readScenarioCrue9() {
        final EMHScenario emhScenario = new ReadHelperForTest(TestCoeurConfig.INSTANCE).readModeleAnsSetRelation(new CtuluLog(),
            "/M3-0_c9.dc", "/M3-0_c9.dh");
        IdRegistry.install(emhScenario);
        return emhScenario;
    }

    public static EMHScenario readScenario() {
        ModellingScenarioServiceImpl modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioServiceImpl.class);
        SelectedPerspectiveService selectedPerspectiveService = Lookup.getDefault().lookup(SelectedPerspectiveService.class);
        selectedPerspectiveService.activePerspective(PerspectiveEnum.MODELLING);
        EMHScenario emhScenario = readScenarioCrue9();
        EMHProjet projet = new EMHProjet();
        projet.setPropDefinition(TestCoeurConfig.INSTANCE);
        modellingScenarioService.setForTest(emhScenario, projet);
        PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
        perspectiveServiceModelling.setState(PerspectiveState.MODE_EDIT);
        return emhScenario;
    }

    public static void configure(AbstractModellingListTopComponent top, EMHScenario scenario) {
        EMHSousModele get = scenario.getModeles().get(0).getSousModeles().get(0);
        top.setSousModeleUid(get.getUiId());
    }

    public static void configure(AbstractModeleTopComponent top, EMHScenario scenario) {
        top.setModeleUid(scenario.getModeles().get(0).getUiId());
    }

    public static void display(JComponent jc) {
        JFrame fr = new JFrame();
        fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fr.setContentPane(jc);
        fr.setSize(500, 600);
        fr.setVisible(true);
    }
}
