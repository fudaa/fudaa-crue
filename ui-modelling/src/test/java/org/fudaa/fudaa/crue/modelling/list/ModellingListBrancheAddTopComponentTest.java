package org.fudaa.fudaa.crue.modelling.list;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;

/**
 *
 * @author deniger
 */
public class ModellingListBrancheAddTopComponentTest {

  public static void main(String[] args) {
    EMHScenario scenario = ModellingTestHelper.readScenario();
    ModellingListBrancheAddTopComponent top = new ModellingListBrancheAddTopComponent();
    top.componentActivated();
    ModellingTestHelper.configure(top, scenario);
    top.setEditable(true);
    ModellingTestHelper.display(top);
  }
}
