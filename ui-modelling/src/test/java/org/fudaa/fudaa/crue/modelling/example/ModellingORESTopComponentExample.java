/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.example;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;
import org.fudaa.fudaa.crue.modelling.calcul.ModellingScenarioORESTopComponent;

/**
 *
 * @author Frédéric Deniger
 */
public class ModellingORESTopComponentExample {

  public static void main(String[] args) {
    EMHScenario scenario = ModellingTestHelper.readScenario();
    ModellingScenarioORESTopComponent top = new ModellingScenarioORESTopComponent();
    top.testScenarioLoaded(scenario);
    top.setEditable(true);
    ModellingTestHelper.display(top);
  }
}
