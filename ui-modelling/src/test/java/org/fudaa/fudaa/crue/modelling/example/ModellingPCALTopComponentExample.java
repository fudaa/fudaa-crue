package org.fudaa.fudaa.crue.modelling.example;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.editor.LocalDateTimeEditor;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;
import org.fudaa.fudaa.crue.modelling.calcul.ModellingScenarioPCALTopComponent;

/**
 *
 * @author Frédéric Deniger
 */
public class ModellingPCALTopComponentExample {

  public static void main(String[] args) {
    LocalDateTimeEditor.registerEditor();
    EMHScenario scenario = ModellingTestHelper.readScenario();
    ModellingScenarioPCALTopComponent top = new ModellingScenarioPCALTopComponent();
    top.testScenarioLoaded(scenario);
    top.setEditable(true);
    ModellingTestHelper.display(top);
  }
}
