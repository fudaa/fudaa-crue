/*
GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import java.io.File;
import java.io.IOException;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluExcelCsvFileReader;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class ModellingListCLimMsProgressRunnableTest {

  public ModellingListCLimMsProgressRunnableTest() {
  }

  @Test
  public void testReadXls() throws IOException {
    File f = CtuluLibFile.getFileFromJar("/org/fudaa/fudaa/crue/modelling/calcul/example.xlsx", File.createTempFile("example", ".xlsx"));
    testContent(f);

  }

  protected void testContent(File f) {
    assertNotNull(f);
    String[][] values = new CtuluExcelCsvFileReader(f).readFile();
    assertEquals(2, values.length);
    assertEquals(5, values[0].length);
    assertArrayEquals(new String[]{"A", "", "B", "C", "D"}, values[0]);
    assertEquals(5, values[1].length);
    assertArrayEquals(new String[]{"E", "F", "", "G", "H"}, values[1]);
  }
  @Test
  public void testReadCsv() throws IOException {
    File f = CtuluLibFile.getFileFromJar("/org/fudaa/fudaa/crue/modelling/calcul/example.csv", File.createTempFile("example", ".csv"));
    testContent(f);

  }

}
