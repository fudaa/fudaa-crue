package org.fudaa.fudaa.crue.modelling.list;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;

/**
 *
 * @author deniger
 */
public class ModellingListCasierTopComponentTest {

  public static void main(String[] args) {
    EMHScenario scenario = ModellingTestHelper.readScenario();
    ModellingListCasierTopComponent top = new ModellingListCasierTopComponent();
    ModellingTestHelper.configure(top, scenario);
    top.setEditable(true);
    top.setScenario(scenario);
    ModellingTestHelper.display(top);
  }
}
