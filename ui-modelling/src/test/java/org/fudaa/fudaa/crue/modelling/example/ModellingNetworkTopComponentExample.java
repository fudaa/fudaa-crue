package org.fudaa.fudaa.crue.modelling.example;

import java.awt.EventQueue;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.pdt.DurationPropertyEditor;
import org.fudaa.fudaa.crue.common.pdt.PrendreClicheFinPermanentEditor;
import org.fudaa.fudaa.crue.modelling.*;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Frédéric Deniger
 */
public class ModellingNetworkTopComponentExample {

  @Test
  public void testClose() {
    try {
      System.setProperty("java.awt.headless", "true");//sur le moteur d'intégration continue c'est le cas.
      Runnable runnable = new Runnable() {
        @Override
        public void run() {
          ModellingNetworkTopComponent top = new ModellingNetworkTopComponent();
          try {
            top.scenarioUnloaded();
          } catch (Exception e) {
            Assert.fail(e.getMessage());


          }
        }
      };
      EventQueue.invokeAndWait(runnable);
    } catch (Exception ex) {
      Assert.fail(ex.getMessage());
    }
  }

  public static void main(String[] args) {
    HierarchyChildFactoryExample.main(args);
  }
}
