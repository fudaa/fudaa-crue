/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.example;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.emh.Sorties;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;
import org.fudaa.fudaa.crue.modelling.calcul.SortiesNode;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.nodes.Node;

/**
 *
 * @author Frédéric Deniger
 */
public class PropertiesSheetExample {

  public static void main(String[] args) {
    SortiesNode node = new SortiesNode(new Sorties(), CrueConfigMetierForTest.DEFAULT, null);
    PropertySheet sheet = new PropertySheet();
    sheet.setNodes(new Node[]{node});
    ModellingTestHelper.display(sheet);
  }
}
