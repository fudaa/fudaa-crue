/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling;

import java.awt.EventQueue;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class ModellingContainersTopComponentTest {

  public ModellingContainersTopComponentTest() {
    System.setProperty("java.awt.headless", "true");//sur le moteur d'intégration continue c'est le cas.
  }

  @Test
  public void testClose() {
    try {
      Runnable runnable = new Runnable() {
        @Override
        public void run() {
          ModellingContainersTopComponent top = new ModellingContainersTopComponent();
          try {
            top.scenarioUnloaded();
          } catch (Exception e) {
            Assert.fail(e.getMessage());

          }
        }
      };
      EventQueue.invokeAndWait(runnable);
    } catch (Exception ex) {
      Assert.fail(ex.getMessage());
    }

  }
}
