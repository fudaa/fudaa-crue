package org.fudaa.fudaa.crue.modelling.list;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;

/**
 *
 * @author deniger
 */
public class ModellingListBrancheTopComponentTest {

  public static void main(String[] args) {
    EMHScenario scenario = ModellingTestHelper.readScenario();
    ModellingListBrancheTopComponent top = new ModellingListBrancheTopComponent();
    ModellingTestHelper.configure(top, scenario);
    top.setEditable(true);
    top.setScenario(scenario);
    ModellingTestHelper.display(top);
  }
}
