/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.example;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.emh.OrdPrtGeoModeleBase;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;
import org.fudaa.fudaa.crue.modelling.calcul.RegleView;

/**
 *
 * @author Frédéric Deniger
 */
public class RegleViewExample {

  public static void main(String[] args) {
    OrdPrtGeoModeleBase optg = new OrdPrtGeoModeleBase(CrueConfigMetierForTest.DEFAULT);
    RegleView view = new RegleView();
    view.init(optg.getRegles(), CrueConfigMetierForTest.DEFAULT);
    ModellingTestHelper.display(view);
  }
}
