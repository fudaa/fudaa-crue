package org.fudaa.fudaa.crue.modelling.list;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.validation.ValidateAndRebuildProfilSection;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;

/**
 *
 * @author deniger
 */
public class ModellingListFrottementTopComponentTest {

  public static void configure(ModellingListFrottementTopComponent top, EMHScenario scenario) {
    top.setSousModeleUid(scenario.getSousModeles().get(0).getUiId());
    top.scenarioLoaded();
  }

  public static void main(String[] args) {
    final EMHScenario scenario = ModellingTestHelper.readScenario();
    ValidateAndRebuildProfilSection v = new ValidateAndRebuildProfilSection(CrueConfigMetierForTest.DEFAULT, new ScenarioAutoModifiedState());
    v.validateScenario(scenario);
    final ModellingListFrottementTopComponent top = new ModellingListFrottementTopComponent();
    configure(top, scenario);
    top.setEditable(true);
    JPanel pn = new JPanel(new BorderLayout());
    pn.add(top);
    final JButton jButton = new JButton("scroll");
    pn.add(jButton,BorderLayout.NORTH);
    jButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        Map<String, EMH> toMapOfNom = TransformerHelper.toMapOfNom(scenario.getAllSimpleEMH());
        top.reloadWithBrancheUid(toMapOfNom.get("B2").getUiId());
      }
    });
    ModellingTestHelper.display(pn);
  }
}
