/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.example;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.test.ReadHelperForTest;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.fudaa.crue.modelling.ModellingNetworkTopComponent;
import org.fudaa.fudaa.crue.modelling.node.ModellingNodeEMHFactory;

import javax.swing.*;

/**
 *
 * @author deniger
 */
public class HierarchyChildFactoryExample {

  public static void main(String[] args) {
    CrueIOResu<CrueData> readModele = new ReadHelperForTest(TestCoeurConfig.INSTANCE).readModele(new CtuluLog(),
        "/M3-0_c9.dc", "/M3-0_c9.dh");;
    final CrueData res = readModele.getMetier();
    res.getScenarioData().setNom("Sc_Test");
    res.getModele().setNom("Mo_Toto");
    res.getSousModele().setNom(CruePrefix.changePrefix(res.getModele().getNom(), CruePrefix.P_MODELE, CruePrefix.P_SS_MODELE));
    EMHScenario emhScenario = res.getScenarioData();

    EMHRelationFactory.addRelationContientEMH(emhScenario, res.getModele());
    EMHRelationFactory.addRelationContientEMH(res.getModele(), res.getSousModele());
    ModellingNetworkTopComponent topCompoent = new ModellingNetworkTopComponent();
    topCompoent.getExplorerManager().setRootContext(ModellingNodeEMHFactory.createScenarioNode(emhScenario));
    JFrame frame = new JFrame();
    frame.setContentPane(topCompoent);
    frame.setSize(400, 500);
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.setVisible(true);

  }
}
