package org.fudaa.fudaa.crue.modelling.loi;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class DFRTTopComponentTest {

  @Test
  public void testClose() {

    DFRTTopComponent top = new DFRTTopComponent();
    try {
      top.scenarioUnloaded();
    } catch (Exception e) {
      Assert.fail(e.getMessage());

    }

  }

  public static void configure(DFRTTopComponent top, EMHScenario scenario) {
    top.setSousModeleUid(scenario.getModeles().get(0).getSousModeles().get(0).getUiId(), null);
    top.scenarioLoaded();
  }

  public static void main(String[] args) {
    EMHScenario scenario = ModellingTestHelper.readScenario();
    DFRTTopComponent top = new DFRTTopComponent();
    configure(top, scenario);
    top.setEditable(true);
    ModellingTestHelper.display(top);
  }
}
