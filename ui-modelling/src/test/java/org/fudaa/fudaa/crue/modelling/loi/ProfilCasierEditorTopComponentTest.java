/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.loi;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class ProfilCasierEditorTopComponentTest {

  public ProfilCasierEditorTopComponentTest() {
    System.setProperty("java.awt.headless", "true");//sur le moteur d'intégration continue c'est le cas.
  }

  @Test
  public void testClose() {

    ProfilCasierEditorTopComponent top = new ProfilCasierEditorTopComponent();
    try {
      top.scenarioUnloaded();
    } catch (Exception e) {
      Assert.fail(e.getMessage());

    }

  }
}
