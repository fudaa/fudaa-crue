package org.fudaa.fudaa.crue.modelling.emh;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;

/**
 *
 * @author deniger
 */
public class ModellingEMHCasierComponentTest {

  public static void main(String[] args) {
    EMHScenario scenario = ModellingTestHelper.readScenario();
    ModellingEMHCasierTopComponent top = new ModellingEMHCasierTopComponent();
    ModellingEMHNoeudComponentTest.configure(top, "Ca_N6", scenario);
//    ModellingTestHelper.configure(top, scenario);
    top.setEditable(true);
//    top.setScenario(scenario);
    ModellingTestHelper.display(top);
  }
}
