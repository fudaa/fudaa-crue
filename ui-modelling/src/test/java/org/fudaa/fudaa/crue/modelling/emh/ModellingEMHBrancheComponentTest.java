package org.fudaa.fudaa.crue.modelling.emh;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;

/**
 *
 * @author deniger
 */
public class ModellingEMHBrancheComponentTest {

  public static void main(String[] args) {
    EMHScenario scenario = ModellingTestHelper.readScenario();
    ModellingEMHBrancheTopComponent top = new ModellingEMHBrancheTopComponent();
    ModellingEMHNoeudComponentTest.configure(top, "B2", scenario);
    top.setEditable(true);
    ModellingTestHelper.display(top);
  }
}
