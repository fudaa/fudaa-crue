package org.fudaa.fudaa.crue.modelling;

import java.awt.EventQueue;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Frédéric Deniger
 */
public class ModellingNetworkTopComponentTest {

  @Test
  public void testClose() {
    try {
      System.setProperty("java.awt.headless", "true");//sur le moteur d'intégration continue c'est le cas.
      Runnable runnable = new Runnable() {
        @Override
        public void run() {
          ModellingNetworkTopComponent top = new ModellingNetworkTopComponent();
          try {
            top.scenarioUnloaded();
          } catch (Exception e) {
            Assert.fail(e.getMessage());


          }
        }
      };
      EventQueue.invokeAndWait(runnable);
    } catch (Exception ex) {
      Assert.fail(ex.getMessage());
    }
  }
}
