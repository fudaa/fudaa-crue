/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.example;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.emh.Planimetrage;
import org.fudaa.dodico.crue.metier.emh.PlanimetrageNbrPdzCst;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;
import org.fudaa.fudaa.crue.modelling.node.CommonObjectNode;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.nodes.Node;

/**
 *
 * @author Frédéric Deniger
 */
public class PropertiesSheetCommonObjectExample {

  public static void main(String[] args) {
    final PlanimetrageNbrPdzCst planimetrageNbrPdzCst = new PlanimetrageNbrPdzCst(CrueConfigMetierForTest.DEFAULT);
    planimetrageNbrPdzCst.setNbrPdz(0);
    CommonObjectNode<Planimetrage> node = new CommonObjectNode<>(
        planimetrageNbrPdzCst, CrueConfigMetierForTest.DEFAULT, null);
    PropertySheet sheet = new PropertySheet();
    sheet.setNodes(new Node[]{node});
    ModellingTestHelper.display(sheet);
  }
}
