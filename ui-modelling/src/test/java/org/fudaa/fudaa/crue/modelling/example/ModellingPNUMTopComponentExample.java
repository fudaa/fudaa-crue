/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.example;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.pdt.DurationPropertyEditor;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;
import org.fudaa.fudaa.crue.modelling.calcul.ModellingModelePNUMTopComponent;

/**
 *
 * @author Frédéric Deniger
 */
public class ModellingPNUMTopComponentExample {

  public static void main(String[] args) {
    DurationPropertyEditor.registerEditor();
    EMHScenario scenario = ModellingTestHelper.readScenario();
    ModellingModelePNUMTopComponent top = new ModellingModelePNUMTopComponent();
    ModellingTestHelper.configure(top, scenario);
    top.testScenarioLoaded(scenario);
    top.setEditable(true);
    ModellingTestHelper.display(top);
  }
}
