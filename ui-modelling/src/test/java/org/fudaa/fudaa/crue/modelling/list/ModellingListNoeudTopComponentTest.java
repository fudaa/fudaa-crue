package org.fudaa.fudaa.crue.modelling.list;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;

/**
 *
 * @author deniger
 */
public class ModellingListNoeudTopComponentTest {

  public static void main(String[] args) {
    EMHScenario scenario = ModellingTestHelper.readScenario();
    ModellingListNoeudTopComponent top = new ModellingListNoeudTopComponent();
    ModellingTestHelper.configure(top, scenario);
    top.setScenario(scenario);
    top.setEditable(true);
    ModellingTestHelper.display(top);
  }
}
