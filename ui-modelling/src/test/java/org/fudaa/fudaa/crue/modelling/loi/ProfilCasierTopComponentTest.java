package org.fudaa.fudaa.crue.modelling.loi;

import java.util.Map;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;

/**
 *
 * @author deniger
 */
public class ProfilCasierTopComponentTest {

  public static void configure(ProfilCasierEditorTopComponent top, String name, EMHScenario scenario) {
    Map<String, EMH> toMapOfNom = TransformerHelper.toMapOfNom(scenario.getAllSimpleEMH());
//    EMH casier = toMapOfNom.get(name);
//    top.setCasierUid(casier.getUiId());
    top.scenarioLoaded();
  }

  public static void main(String[] args) {
    EMHScenario scenario = ModellingTestHelper.readScenario();
    ProfilCasierEditorTopComponent top = new ProfilCasierEditorTopComponent();
    configure(top, "Ca_N6", scenario);
    top.setEditable(true);
    ModellingTestHelper.display(top);
  }
}
