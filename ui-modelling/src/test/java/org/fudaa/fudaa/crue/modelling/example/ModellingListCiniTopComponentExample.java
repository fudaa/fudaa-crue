package org.fudaa.fudaa.crue.modelling.example;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EnumMethodeInterpol;
import org.fudaa.fudaa.crue.modelling.ModellingTestHelper;
import org.fudaa.fudaa.crue.modelling.calcul.ModellingListCiniTopComponent;

/**
 *
 * @author deniger
 */
public class ModellingListCiniTopComponentExample {

  public static void main(String[] args) {
    EMHScenario scenario = ModellingTestHelper.readScenario();
    ModellingListCiniTopComponent top = new ModellingListCiniTopComponent();
    scenario.getModeles().get(0).getOrdPrtCIniModeleBase().setMethodeInterpol(EnumMethodeInterpol.BAIGNOIRE);
    ModellingTestHelper.configure(top, scenario);
    top.setEditable(true);
    top.testScenarioLoaded(scenario);
    ModellingTestHelper.display(top);
  }
}
