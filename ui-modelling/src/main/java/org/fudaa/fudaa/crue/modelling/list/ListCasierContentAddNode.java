package org.fudaa.fudaa.crue.modelling.list;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import org.fudaa.dodico.crue.edition.bean.ListCasierContent;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertySupportRead;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class ListCasierContentAddNode extends AbstractListContentNode {

  public ListCasierContentAddNode(ListCasierContent content,
          PerspectiveServiceModelling perspectiveServiceModelling, List<String> availableNoeuds) {
    super(Children.LEAF, Lookups.fixed(content, availableNoeuds), perspectiveServiceModelling);

  }

  @Override
  public boolean canDestroy() {
    return super.isEditMode();
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    ListCasierContent casierContent = getLookup().lookup(ListCasierContent.class);
    try {
      Node.Property nom = new PropertySupportRead<ListCasierContent, String>(casierContent, String.class, ListCommonProperties.PROP_NOM,
              ListCommonProperties.PROP_NOM,
              ListCasierContentNode.getCasierNameDisplay()) {

        @Override
        public String getValue() throws IllegalAccessException, InvocationTargetException {
          return getInstance().getNom();
        }
      };
      PropertyCrueUtils.configureNoCustomEditor(nom);
      set.put(nom);
      List<String> noeuds = getLookup().lookup(List.class);
      //noeud amont
      Node.Property noeud = new PropertySupportNoeudCasier(this, casierContent, noeuds);
      set.put(noeud);

      //active
      Node.Property active = new PropertySupportReflection(this, casierContent, Boolean.TYPE,
              ListCommonProperties.PROP_ACTIVE);
      active.setName(ListCommonProperties.PROP_ACTIVE);
      active.setDisplayName(AbstractListContentNode.getActiveDisplay());
      set.put(active);

      //commentaire
      Node.Property commentaire = new PropertySupportReflection(this, casierContent, String.class,
              ListCommonProperties.PROP_COMMENTAIRE);
      PropertyCrueUtils.setNullValueEmpty(commentaire);
      commentaire.setName(ListCommonProperties.PROP_COMMENTAIRE);
      commentaire.setDisplayName(AbstractListContentNode.getCommentDisplay());
      set.put(commentaire);
    } catch (NoSuchMethodException ex) {
      Exceptions.printStackTrace(ex);
    }
    sheet.put(set);
    return sheet;
  }
}
