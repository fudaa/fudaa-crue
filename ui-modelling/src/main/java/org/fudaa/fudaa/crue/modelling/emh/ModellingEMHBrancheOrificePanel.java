package org.fudaa.fudaa.crue.modelling.emh;

import com.memoire.bu.BuGridLayout;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.edition.bean.BrancheEditionContent;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtBrancheOrifice;
import org.fudaa.dodico.crue.metier.emh.ElemOrifice;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.common.view.ItemVariableView;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.explorer.ExplorerManager;
import org.openide.util.Lookup;

/**
 * Branche Type 2
 *
 * @author fred
 */
public class ModellingEMHBrancheOrificePanel extends JPanel implements ModellingEMHBrancheSpecificEditor, Observer {

  final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(
          ModellingScenarioService.class);
  PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
  final ModellingEMHBrancheTopComponent parent;
  final List<ItemVariableView> properties;

  ModellingEMHBrancheOrificePanel(CatEMHBranche branche, ModellingEMHBrancheTopComponent parent) {
    super(new BuGridLayout(2, 10, 10));
    this.parent = parent;
    DonCalcSansPrtBrancheOrifice dcsp = EMHHelper.selectFirstOfClass(branche.getInfosEMH(), DonCalcSansPrtBrancheOrifice.class);
    if (dcsp == null) {
      dcsp = new DonCalcSansPrtBrancheOrifice();
      dcsp.setElemOrifice(new ElemOrifice(getCcm()));
    }
    properties = ItemVariableView.create(DecimalFormatEpsilonEnum.COMPARISON, dcsp.getElemOrifice().clone(), getCcm(), ElemOrifice.PROP_SENSORIFICE, CrueConfigMetierConstants.PROP_LARGEUR,
            CrueConfigMetierConstants.PROP_ZSEUIL, ElemOrifice.PROP_HAUT, CrueConfigMetierConstants.PROP_COEF_D, ElemOrifice.PROP_COEFCTRLIM);
    for (ItemVariableView propertyEditorPanel : properties) {
      propertyEditorPanel.addObserver(this);
      add(new JLabel(propertyEditorPanel.getLabelName()));
      add(propertyEditorPanel.getPanelComponent());
    }
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return null;
  }

  @Override
  public void update(Observable o, Object arg) {
    parent.setModified(true);
  }

  private CrueConfigMetier getCcm() {
    return modellingScenarioService.getSelectedProjet().getPropDefinition();
  }

  @Override
  public void setEditable(boolean editable) {
    for (ItemVariableView object : properties) {
      object.setEditable(editable);

    }
  }

  @Override
  public void fillWithData(BrancheEditionContent content) {
    DonCalcSansPrtBrancheOrifice dcsp = new DonCalcSansPrtBrancheOrifice();
    dcsp.setElemOrifice(new ElemOrifice(getCcm()));
    ItemVariableView.transferToBean(dcsp.getElemOrifice(), properties);
    content.setDcsp(dcsp);
  }

  @Override
  public JComponent getComponent() {
    return this;
  }
}
