/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.emh;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.bean.BrancheEditionContent;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertySupportReadWrite;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.common.view.ItemVariableView;
import org.fudaa.fudaa.crue.modelling.list.ListRelationSectionContentFactoryNode;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 * Branche Type 2
 *
 * @author fred
 */
public class ModellingEMHBrancheStricklerPanel extends JPanel implements ModellingEMHBrancheSpecificEditor, Observer, ExplorerManager.Provider {
  
  final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(
          ModellingScenarioService.class);
  PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
  final ModellingEMHBrancheTopComponent parent;
  final ItemVariableView.TextFiedDistance distance;
  final OutlineView view;
  final Node avalNode;
  final ExplorerManager em = new ExplorerManager();
  
  ModellingEMHBrancheStricklerPanel(CatEMHBranche branche, ModellingEMHBrancheTopComponent parent) {
    super(new BorderLayout(10, 10));
    this.parent = parent;
    distance = new ItemVariableView.TextFiedDistance(getCcm(), DecimalFormatEpsilonEnum.COMPARISON);
    JPanel top = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
    top.add(new JLabel(NbBundle.getMessage(ModellingEMHBrancheStricklerPanel.class, "Distance.DisplayName")));
    top.add(distance.getPanelComponent());
    distance.setValue(branche.getLength());
    distance.addObserver(this);
    add(top, BorderLayout.NORTH);
    RelationEMHSectionDansBranche sectionAmont = branche.getSectionAmont();
    RelationEMHSectionDansBranche sectionAval = branche.getSectionAval();
    final ItemVariable propertyXp = getCcm().getProperty(CrueConfigMetierConstants.PROP_XP);
    Node amontLabelNode = createLabelNode(NbBundle.getMessage(ModellingEMHBrancheStricklerPanel.class, "Amont.DisplayName"));
    
    ListRelationSectionContentFactoryNode factory = new ListRelationSectionContentFactoryNode();
    Node amontNode = factory.create(branche, sectionAmont, getCcm());
    amontNode.setDisplayName(propertyXp.format(sectionAmont.getXp(), DecimalFormatEpsilonEnum.COMPARISON));
    
    avalNode = factory.create(branche, sectionAval, getCcm());
    avalNode.setDisplayName(propertyXp.format(sectionAval.getXp(), DecimalFormatEpsilonEnum.COMPARISON));
    removeWrite((AbstractNodeFirable) amontNode);
    removeWrite((AbstractNodeFirable) avalNode);
    
    Node avalLabelNode = createLabelNode(NbBundle.getMessage(ModellingEMHBrancheStricklerPanel.class, "Aval.DisplayName"));
    view = new OutlineView(NbBundle.
            getMessage(ModellingEMHBrancheStricklerPanel.class, "AbsHydraulique.ColumnName", propertyXp.getNature().getUnite()));
    view.setTreeSortable(false);
    view.setPropertyColumns(ListCommonProperties.PROP_NOM, NbBundle.getMessage(ModellingEMHBrancheStricklerPanel.class, "Section.ColumnName"));
    view.getOutline().setRootVisible(false);
    
    em.setRootContext(NodeHelper.createNode(Arrays.asList(amontLabelNode, amontNode, avalNode, avalLabelNode), "SECTIONS"));
    add(view);
  }
  
  public static void removeWrite(AbstractNodeFirable node) {
    PropertySupportReflection find = (PropertySupportReflection) node.find(ListRelationSectionContent.PROP_ABSCISSE_HYDRAULIQUE);
    find.setCanWrite(false);
    PropertySupportReadWrite section = (PropertySupportReadWrite) node.find(ListCommonProperties.PROP_NOM);
    section.setCanWrite(false);
  }
  
  @Override
  public ExplorerManager getExplorerManager() {
    return em;
  }
  
  private Node createLabelNode(String name) {
    final DelimiteurNode delimiteurNode = new DelimiteurNode(name);
    return delimiteurNode;
  }
  
  @Override
  public void update(Observable o, Object arg) {
    parent.setModified(true);
    try {
      Number value = distance.getValue();
      avalNode.setDisplayName(distance.getProperty().format(value, DecimalFormatEpsilonEnum.COMPARISON));
    } catch (Exception exception) {
      Exceptions.printStackTrace(exception);
    }
  }
  
  private CrueConfigMetier getCcm() {
    return modellingScenarioService.getSelectedProjet().getPropDefinition();
  }
  
  @Override
  public void setEditable(boolean editable) {
  }
  
  @Override
  public void fillWithData(BrancheEditionContent content) {
    content.setDistance(distance.getValue().doubleValue());
  }
  
  @Override
  public JComponent getComponent() {
    return this;
  }
}
