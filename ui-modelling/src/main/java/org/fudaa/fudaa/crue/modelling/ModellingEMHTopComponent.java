package org.fudaa.fudaa.crue.modelling;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.fudaa.crue.emh.EMHTreePanel;
import org.fudaa.fudaa.crue.emh.common.TreeNodeEMH;
import org.fudaa.fudaa.crue.modelling.node.ModellingNodeEMHFactory;
import org.fudaa.fudaa.crue.modelling.services.ModellingSelectedEMHService;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.modelling//ModellingEMHTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = ModellingEMHTopComponent.TOPCOMPONENT_ID,
        iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png",
        persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "modelling-topRight", openAtStartup = false, position = 2)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.modelling.ModellingEMHTopComponent")
@ActionReference(path = "Menu/Window/Modelling", position = 4)
@TopComponent.OpenActionRegistration(displayName = ModellingEMHTopComponent.TOPCOMPONENT_ACTION_DISPLAYNAME,
        preferredID = ModellingEMHTopComponent.TOPCOMPONENT_ID)
public final class ModellingEMHTopComponent extends AbstractModellingTopComponent implements LookupListener, ExplorerManager.Provider {
  
  final ModellingSelectedEMHService modellingSelectedEMHService = Lookup.getDefault().lookup(ModellingSelectedEMHService.class);
  public static final String TOPCOMPONENT_ID = "ModellingEMHTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  private Lookup.Result<Long> resultatNodeSelected;
  private SelectedEMHLookupListener selectedNodeLookupListener;
  private final ExplorerManager em = new ExplorerManager();
  private EMHTreePanel emhTreePanel;
  
  public ModellingEMHTopComponent() {
    initComponents();
    setName(NbBundle.getMessage(ModellingEMHTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingEMHTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    associateLookup(ExplorerUtils.createLookup(em, getActionMap()));
  }
  
  @Override
  protected String getViewHelpCtxId() {
    return "vueEMHs";
  }
  
  @Override
  public ExplorerManager getExplorerManager() {
    return em;
  }
  
  @Override
  protected void setEditable(boolean b) {
  }
  
  @Override
  public void cancelModificationHandler() {
  }
  
  @Override
  public void valideModificationHandler() {
  }
  final TreeSelectionListener treeSelectionListener = new TreeSelectionListener() {
    @Override
    public void valueChanged(TreeSelectionEvent e) {
      if (WindowManager.getDefault().getRegistry().getActivated() == ModellingEMHTopComponent.this) {
        TreePath[] selectionPaths = emhTreePanel.getTreeEMH().getTreeSelectionModel().getSelectionPaths();
        Set<EMH> emhs = new HashSet<>();
        if (selectionPaths != null && selectionPaths.length > 0) {
          for (TreePath treePath : selectionPaths) {
            Object lastPathComponent = treePath.getLastPathComponent();
            if (TreeNodeEMH.class.equals(lastPathComponent.getClass())) {
              emhs.add(((TreeNodeEMH) lastPathComponent).getEmh());
            }
          }
          //pour mettre à jour la fenêtre propriété
          ModellingNodeEMHFactory.updateExplorerManagerWithSelectedEmhs(getExplorerManager(), emhs);
          modellingSelectedEMHService.setSelectedEMHs(emhs);
        }
      }
    }
  };
  
  @Override
  protected void scenarioLoaded() {
    if (emhTreePanel != null) {
      emhTreePanel.getTreeEMH().removeTreeSelectionListener(treeSelectionListener);
    }
    emhTreePanel = new EMHTreePanel(getModellingService().getSelectedProjet(),
            getModellingService().getScenarioLoaded());
    this.removeAll();
    this.add(new JScrollPane(emhTreePanel.build(getClass().getSimpleName())));
    emhTreePanel.getTreeEMH().addTreeSelectionListener(treeSelectionListener);
    super.revalidate();
    this.repaint();
  }
  
  public void updatePanelFromSelection() {
    if (emhTreePanel != null) {
      Collection<? extends Long> allItems = resultatNodeSelected.allInstances();
      Set<Long> selectedUid = new HashSet<>();
      for (Long item : allItems) {
        if (item != null) {
          selectedUid.add(item);
        }
        emhTreePanel.selectLines(selectedUid);
      }
    }
  }
  
  private class SelectedEMHLookupListener implements LookupListener {
    
    @Override
    public void resultChanged(LookupEvent ev) {
      if (WindowManager.getDefault().getRegistry().getActivated() != ModellingEMHTopComponent.this) {
        updatePanelFromSelection();
      }
    }
  }
  
  @Override
  protected void scenarioReloaded() {
    scenarioLoaded();
  }
  
  @Override
  protected void scenarioUnloaded() {
    if (emhTreePanel != null) {
      emhTreePanel.getTreeEMH().removeTreeSelectionListener(treeSelectionListener);
      emhTreePanel.saveSize();
    }
    this.removeAll();
    this.add(new JLabel(MessagesModelling.getMessage("emhTopComponent.NoScenarioLoadedInformations")));
    super.revalidate();
    this.repaint();
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
  @Override
  public void componentClosedTemporarily() {
  }

  @Override
  protected void componentClosedDefinitlyHandler() {
    if (resultatNodeSelected != null) {
      resultatNodeSelected.removeLookupListener(selectedNodeLookupListener);
      resultatNodeSelected = null;
    }
    
  }
  
  @Override
  protected void componentOpenedHandler() {
    if (resultatNodeSelected == null) {
      selectedNodeLookupListener = new SelectedEMHLookupListener();
      resultatNodeSelected = modellingSelectedEMHService.getLookup().lookupResult(Long.class);
      resultatNodeSelected.addLookupListener(selectedNodeLookupListener);
    }
    updatePanelFromSelection();
    
  }
  
  void writeProperties(java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
    p.setProperty("version", "1.0");
    emhTreePanel.saveSize();
  }
  
  void readProperties(java.util.Properties p) {
  }
}
