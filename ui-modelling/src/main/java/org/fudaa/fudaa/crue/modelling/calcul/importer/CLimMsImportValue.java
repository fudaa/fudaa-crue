/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul.importer;

import java.util.Objects;

/**
 *
 * @author Frederic Deniger
 */
public class CLimMsImportValue {

  private final String emh;
  private final String typeClimM;
  private final String value;

  public CLimMsImportValue(final String emh, final String typeClimM, final String value) {
    this.emh = emh;
    this.typeClimM = typeClimM;
    this.value = value;
  }

  public String getEmh() {
    return emh;
  }


  public boolean isPermanent() {
    try {
      Double.parseDouble(value);
      return true;
    } catch (final NumberFormatException numberFormatException) {
    }
    return false;
  }

  public String getTypeClimM() {
    return typeClimM;
  }

  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return typeClimM + ", " + value;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 59 * hash + Objects.hashCode(this.emh);
    hash = 59 * hash + Objects.hashCode(this.typeClimM);
    hash = 59 * hash + Objects.hashCode(this.value);
    return hash;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final CLimMsImportValue other = (CLimMsImportValue) obj;
    if (!Objects.equals(this.emh, other.emh)) {
      return false;
    }
    if (!Objects.equals(this.typeClimM, other.typeClimM)) {
      return false;
    }
    return Objects.equals(this.value, other.value);
  }

}
