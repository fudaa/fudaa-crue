/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.OrdResScenario;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.modelling//ModellingScenarioORESTopComponent//EN",
autostore = false)
@TopComponent.Description(preferredID = ModellingScenarioORESTopComponent.TOPCOMPONENT_ID, iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png", persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ModellingScenarioORESTopComponent.MODE, openAtStartup = false, position = 1)
public final class ModellingScenarioORESTopComponent extends AbstractScenarioTopComponent {

  public static final String MODE = "modelling-ores";
  public static final String TOPCOMPONENT_ID = "ModellingScenarioORESTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;

  public ModellingScenarioORESTopComponent() {
    initComponents();
    setName(NbBundle.getMessage(ModellingScenarioORESTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingScenarioORESTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
  }

  @Override
  public void setEditable(boolean b) {
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueORES";
  }

  @Override
  public void valideModificationHandler() {
    EMHScenario scenario = getScenario();
    OrdResScenario ordResScenario = scenario.getOrdResScenario();
    scenario.removeInfosEMH(ordResScenario);
    scenario.addInfosEMH(editedObject);
    modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.ORES));//a pour effet de recharger le scenario
    setModified(false);
  }

  private class ChangeListener implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      setModified(true);
    }
  }
  ChangeListener changeListener;
  OrdResScenario editedObject;

  @Override
  protected JComponent buildCenterPanel(EMHScenario scenario, JComponent oldCenter) {
    changeListener = new ChangeListener();
    editedObject = scenario.getOrdResScenario().deepClone();
    OrdResScenarioNode node = new OrdResScenarioNode(editedObject, perspectiveServiceModelling);
    PropertySheet sheet = new PropertySheet();
    sheet.setNodes(new Node[]{node});
    node.addPropertyChangeListener(changeListener);
    return sheet;

  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  @Override
  public void componentClosedTemporarily() {
  }

  void writeProperties(java.util.Properties p) {
  }

  void readProperties(java.util.Properties p) {
  }
}
