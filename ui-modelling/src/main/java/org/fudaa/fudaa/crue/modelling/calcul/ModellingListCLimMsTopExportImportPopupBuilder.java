package org.fudaa.fudaa.crue.modelling.calcul;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.MissingResourceException;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluPopupListener;
import org.fudaa.ctulu.gui.CtuluTableSimpleExporter;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.crue.common.helper.AbstractFileImporterProgressRunnable;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.views.DonClimMTableModel;
import org.fudaa.fudaa.crue.views.LoiDisplayer;
import org.fudaa.fudaa.crue.views.LoiDisplayerInstaller;
import org.fudaa.fudaa.crue.views.export.DonClimMExportTableModel;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ModellingListCLimMsTopExportImportPopupBuilder extends LoiDisplayerInstaller implements CtuluPopupListener.PopupReceiver {

  final ModellingListCLimMsTopComponent tc;

  public ModellingListCLimMsTopExportImportPopupBuilder(ModellingListCLimMsTopComponent tc, LoiDisplayer loiDisplayer) {
    super(loiDisplayer);
    this.tc = tc;
  }

  @Override
  protected void fillWithCustomItems(JPopupMenu menu) {
    if (menu.getComponentCount() > 0) {
      menu.addSeparator();
    }
    menu.add(createImportMenuItem());
    menu.add(createExportMenuItem());
  }

  protected void exportTable() {
    DonClimMExportTableModel exportModel = new DonClimMExportTableModel(tc.tableModel, tc.getCcm());
    CtuluTableSimpleExporter.doExport(';', exportModel, CtuluUIForNetbeans.DEFAULT);
  }

  protected void importTable() {
    if (!tc.isEditable()) {
      return;
    }
   
    CtuluFileChooser fileChooser = AbstractFileImporterProgressRunnable.getImportXLSCSVFileChooser();
    
    final int res = fileChooser.showDialog(CtuluUIForNetbeans.DEFAULT.getParentComponent(), NbBundle.getMessage(
            ModellingListCLimMsTopExportImportPopupBuilder.class,
            "button.import.name"));
    if (res == JFileChooser.APPROVE_OPTION) {
      final DonClimMTableModel result = CrueProgressUtils.showProgressDialogAndRun(new ModellingListCLimMsProgressRunnable(fileChooser.
              getSelectedFile(), tc), NbBundle.
              getMessage(ModellingListCLimMsTopExportImportPopupBuilder.class, "button.import.name"));
      if (result != null) {
        tc.rebuildCenterFor(result);
        tc.setModified(true);
      }
    }
  }

  protected JMenuItem createExportMenuItem() throws MissingResourceException {
    final JMenuItem menuItemExport = new JMenuItem(NbBundle.getMessage(ModellingListCLimMsTopExportImportPopupBuilder.class, "button.export.name"));
    menuItemExport.setIcon(EbliResource.EBLI.getToolIcon("crystal_exporter"));
    menuItemExport.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        exportTable();
      }
    });
    return menuItemExport;
  }

  protected JMenuItem createImportMenuItem() {
    final JMenuItem menuItemImport = new JMenuItem(NbBundle.getMessage(ModellingListCLimMsTopExportImportPopupBuilder.class, "button.import.name"));
    menuItemImport.setToolTipText(NbBundle.getMessage(ModellingListCLimMsTopExportImportPopupBuilder.class, "button.importClimMs.tooltip"));
    menuItemImport.setEnabled(tc.isEditable());
    menuItemImport.setIcon(EbliResource.EBLI.getToolIcon("crystal_importer"));
    menuItemImport.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        importTable();
      }
    });
    return menuItemImport;
  }
}
