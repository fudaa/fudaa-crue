package org.fudaa.fudaa.crue.modelling.emh;

import com.memoire.bu.BuBorders;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.EditionChangeSection;
import org.fudaa.dodico.crue.edition.bean.SectionEditionContent;
import org.fudaa.dodico.crue.edition.bean.SectionPrevalidationContent;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.validation.util.DelegateValidatorSection;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.ToStringInternationalizableCellRenderer;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.ItemVariableView;
import org.fudaa.fudaa.crue.modelling.loi.ProfilSectionOpenOnSelectedSectionNodeAction;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ModellingConfigService;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Top component which displays something.
 */
@TopComponent.Description(preferredID = ModellingEMHSectionTopComponent.TOPCOMPONENT_ID,
    iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png", persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ModellingEMHSectionTopComponent.MODE, openAtStartup = false, position = 0)
public final class ModellingEMHSectionTopComponent extends AbstractModellingEMHTopComponent implements Observer {
  public static final String MODE = "modelling-emhSection";
  public static final String TOPCOMPONENT_ID = "ModellingEMHSectionTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  private final ModellingConfigService modellingConfigService = Lookup.getDefault().lookup(ModellingConfigService.class);
  private String initReferenceName;
  private String initDzName;
  private JButton btOpenProfil;
  private JComboBox cbType;
  private JComboBox cbSectionReference;
  private ItemVariableView.TextField tfDz;
  private JPanel pnDz;
  private JLabel lbDz;
  private JLabel lbDetails;
  private JLabel lbReference;
  private boolean editable;

  public ModellingEMHSectionTopComponent() {
    setName(NbBundle.getMessage(ModellingEMHSectionTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingEMHSectionTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
  }

  @Override
  protected void createComponents() {
    final JPanel buildNorthPanel = buildNorthPanel();
    btOpenProfil = new JButton(NbBundle.getMessage(ModellingEMHSectionTopComponent.class, "OpenProfilEditor.Label"));
    btOpenProfil.setEnabled(false);
    btOpenProfil.addActionListener(e -> openInProfilEditor());
    initReferenceName = NbBundle.getMessage(ModellingEMHSectionTopComponent.class, "Reference.Name");
    lbReference = new JLabel(initReferenceName);

    buildNorthPanel.add(lbReference);
    cbSectionReference = new JComboBox();
    cbSectionReference.addItemListener(e -> {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        sectionReferenceChangedInUI();
      }
    });
    buildNorthPanel.add(cbSectionReference);
    initDzName = NbBundle.getMessage(ModellingEMHSectionTopComponent.class, "Dz.Name");
    lbDz = new JLabel(initDzName);

    buildNorthPanel.add(lbDz);
    pnDz = new JPanel(new FlowLayout(FlowLayout.LEFT));
    buildNorthPanel.add(pnDz);
    buildNorthPanel.add(new JLabel());
    buildNorthPanel.add(btOpenProfil);
    lbDetails = new JLabel();
    lbDetails.setBorder(BuBorders.EMPTY3333);
    add(lbDetails, BorderLayout.NORTH);
    add(buildNorthPanel, BorderLayout.CENTER);
    add(createCancelSaveButtons(), BorderLayout.SOUTH);
  }

  @Override
  public HelpCtx getHelpCtx() {
    HelpCtx res = super.getHelpCtx();
    final Object selectedItem = cbType.getSelectedItem();
    if (selectedItem != null) {
      res = new HelpCtx(SysdocUrlBuilder.addSignet(res.getHelpID(), selectedItem.toString()));
    }
    return res;
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueEMHSection";
  }

  @Override
  protected void updateOtherComponents() {
    btOpenProfil.setEnabled(isProfilSection());
    final CatEMHSection section = getEMH();
    final SectionPrevalidationContent prevalidate = EditionChangeSection.createPrevalidateContent(section);
    final StringBuilder builder = new StringBuilder();
    final String sousModeleName = section.getParent().getNom();
    builder.append("<html><body>");
    if (prevalidate.isIsAval()) {
      builder.append(NbBundle.getMessage(ModellingEMHSectionTopComponent.class, "Section.SectionAval", prevalidate.getBranche().getNom(),
          sousModeleName));
    } else if (prevalidate.isIsAmont()) {
      builder.append(NbBundle.getMessage(ModellingEMHSectionTopComponent.class, "Section.SectionAmont", prevalidate.getBranche().getNom(),
          sousModeleName));
    } else if (prevalidate.getBranche() != null) {
      builder.append(NbBundle.getMessage(ModellingEMHSectionTopComponent.class, "Section.SectionInterne", prevalidate.getBranche().getNom(),
          sousModeleName));
    } else {
      builder.append(NbBundle.getMessage(ModellingEMHSectionTopComponent.class, "Section.InModele", sousModeleName));
    }
    if (prevalidate.getBranchePilotedBy() != null) {
      builder.append("<br>");
      builder.append(NbBundle.getMessage(ModellingEMHSectionTopComponent.class, "Section.SectionPilote", prevalidate.getBranchePilotedBy().getNom(),
          sousModeleName));
    }
    if (CollectionUtils.isNotEmpty(prevalidate.getSectionReferencing())) {
      for (final EMHSectionIdem emhSectionIdem : prevalidate.getSectionReferencing()) {
        builder.append("<br>");
        builder.append(NbBundle.getMessage(ModellingEMHSectionTopComponent.class, "Section.ReferencedBySectionIdem",
            emhSectionIdem.getNom()));
      }
    }
    builder.append("</body></html>");
    lbDetails.setText(builder.toString());
    lbDetails.setToolTipText(lbDetails.getText());
    final List<EnumSectionType> authorizedSections = prevalidate.getAuthorizedSections();
    cbType.setModel(new DefaultComboBoxModel(authorizedSections.toArray()));
    cbType.setSelectedItem(section.getSectionType());
    final List<CatEMHSection> sections = section.getParent().getSections();
    final List<Class<? extends CatEMHSection>> authorizedSectionRef = DelegateValidatorSection.getAuthorizedSectionRef();
    final List<CatEMHSection> selectInClasses = EMHHelper.selectInClasses(sections, authorizedSectionRef.toArray(new Class[0]));
    selectInClasses.remove(getEMH());
    cbSectionReference.setModel(new DefaultComboBoxModel(TransformerHelper.toNom(selectInClasses).toArray()));

    if (tfDz != null) {
      tfDz.deleteObserver(this);
      pnDz.remove(tfDz.getTxt());
    }
    final ItemVariable property = getCcm().getProperty(CrueConfigMetierConstants.PROP_DZ);
    tfDz = new ItemVariableView.TextField(property, DecimalFormatEpsilonEnum.COMPARISON);
    pnDz.add(tfDz.getTxt());
    tfDz.addObserver(this);

    if (EnumSectionType.EMHSectionIdem.equals(section.getSectionType())) {
      final CatEMHSection sectionRef = EMHHelper.getSectionRef((EMHSectionIdem) section);
      assert sectionRef != null;
      cbSectionReference.setSelectedItem(sectionRef.getNom());
      final DonPrtGeoSectionIdem dptg = EMHHelper.getDPTGSectionIdem((EMHSectionIdem) section);
      tfDz.setValue(Double.valueOf(dptg.getDz()));
    }
    typeChangedInUI();
    updateComponentEditableState();
  }

  @Override
  public void valideModificationHandler() {

    final CatEMHSection oldEMH = getEMH();
    final EnumSectionType newSectionType = (EnumSectionType) cbType.getSelectedItem();
    if (EnumSectionType.EMHSectionProfil.equals(oldEMH.getSectionType()) && !ObjectUtils.equals(newSectionType, oldEMH.getSectionType())) {
      final boolean go = DialogHelper.showQuestionAndSaveDialogConf(getDisplayName(), org.openide.util.NbBundle.getMessage(
          ModellingEMHSectionTopComponent.class,
          "changeSection.ProfilSectionWillBeLost"), getClass().getCanonicalName() + "changeSection");
      if (!go) {
        return;
      }
    }
    final SectionEditionContent content = new SectionEditionContent();
    final String newName = jEMHName.getText();
    content.setNewName(newName);
    content.setComment(jCommentaire.getText());
    content.setNewType(newSectionType);
    if (tfDz != null) {
      content.setDz((Double) tfDz.getValue());
    }
    final String selectedRef = (String) cbSectionReference.getSelectedItem();
    if (selectedRef != null) {
      final EMH emhRef = getScenario().getIdRegistry().getEmhByNom().get(selectedRef);
      if (emhRef != null) {
        content.setSectionRefUid(emhRef.getUiId());
      }
    }
    final CtuluLog error = new EditionChangeSection(modellingConfigService.getDefaultValues()).changeSection(oldEMH, content, getCcm());
    if (error.containsErrorOrSevereError()) {
      LogsDisplayer.displayError(error, getDisplayName());
    } else {
      modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.EMH_NAME));
      setModified(false);
    }
  }

  private void openInProfilEditor() {
    if (isProfilSection()) {
      ProfilSectionOpenOnSelectedSectionNodeAction.open(getEMHUid(), true);
    }
  }

  @Override
  public void update(final Observable o, final Object arg) {
    if (!isUpdating) {
      setModified(true);
    }
  }

  @Override
  protected void addTypeInNorthPanel(final JPanel pn) {
    cbType = new JComboBox(new DefaultComboBoxModel());
    cbType.addActionListener(modifiedActionListener);
    cbType.addItemListener(e -> {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        typeChangedInUI();
      }
    });
    cbType.setRenderer(new ToStringInternationalizableCellRenderer());
    pn.add(cbType);
  }

  @Override
  protected void setEditable(final boolean b) {
    this.editable = b;
    super.setEditable(b);
    updateComponentEditableState();
  }

  private void typeChangedInUI() {
    if (!isUpdating) {
      setModified(true);
    }
    final boolean visible = EnumSectionType.EMHSectionIdem.equals(cbType.getSelectedItem());
    cbSectionReference.setVisible(visible);
    lbDz.setText(visible ? initDzName : StringUtils.EMPTY);
    lbReference.setText(visible ? initReferenceName : StringUtils.EMPTY);
    if (tfDz != null) {
      tfDz.getTxt().setVisible(visible);
      if (visible && tfDz.getValue() == null) {
        tfDz.setValue(getCcm().getProperty(CrueConfigMetierConstants.PROP_DZ).getDefaultValue());
      }
    }
  }

  private void sectionReferenceChangedInUI() {
    if (!isUpdating) {
      setModified(true);
    }
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables

  private boolean isProfilSection() {
    return getEMH() != null && EMHSectionProfil.class.equals(getEMH().getClass());
  }

  public void updateComponentEditableState() {
    cbType.setEnabled(editable);
    cbSectionReference.setEnabled(editable);
  }
}
