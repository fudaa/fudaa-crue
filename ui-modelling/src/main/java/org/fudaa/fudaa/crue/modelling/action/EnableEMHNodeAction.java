package org.fudaa.fudaa.crue.modelling.action;

import org.openide.util.NbBundle;

/**
 *
 * @author pasinato
 */
public class EnableEMHNodeAction  extends ToggleActivationNodeAction {
  
    public EnableEMHNodeAction() {
        super(true, NbBundle.getMessage(ToggleActivationNodeAction.class, "EnableEMHNodeAction.Name"));
    }    
}
