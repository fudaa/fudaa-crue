package org.fudaa.fudaa.crue.modelling.action;

import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class DeleteEMHCascadeNodeAction extends DeleteEMHNodeAction {

  public DeleteEMHCascadeNodeAction() {
    super(true, NbBundle.getMessage(DeleteEMHNodeAction.class, "DeleteEMHCascadeAction.Name"));
  }
}
