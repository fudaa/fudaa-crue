/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.loi;

import com.memoire.bu.BuGridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.DoubleEpsilonComparator;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.ItemVariableView;
import org.openide.util.NbBundle;

/**
 * Les branches proposées sont celles qui acceptent des profilSection
 *
 * @author Frederic Deniger
 */
public class SousModeleBrancheCreationComponent extends SousModeleBrancheComponent {

  @Override
  protected List<CatEMHBranche> getBranches(EMHSousModele sousModele) {
    List<CatEMHBranche> branchesWithProfil = new ArrayList<>();
    for (CatEMHBranche branche : sousModele.getBranches()) {
      //seules les branches saint-venant acceptent plusieurs profils.
      if (EnumBrancheType.EMHBrancheSaintVenant.equals(branche.getBrancheType())) {
        branchesWithProfil.add(branche);
      }
    }
    branchesWithProfil.add(NULL_BRANCHE);
    return branchesWithProfil;
  }

  public static class CreationData {

    EMHSousModele sousModele;
    EMHBrancheSaintVenant branche;
    Double xp;
  }

  private static class NewProfilEditor {

    JPanel panel = new JPanel(new BuGridLayout(3, 5, 5));
    final ItemVariableView.TextField xpView;
    final JLabel lbDistance = new JLabel();
    SousModeleBrancheCreationComponent brancheSelection = new SousModeleBrancheCreationComponent();
    final ItemVariable property;

    public NewProfilEditor(ProfilSectionTopComponent topComponent, CreationData init) {
      panel = new JPanel(new BuGridLayout(3, 5, 5));
      panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
      property = topComponent.getCcm().getProperty(CrueConfigMetierConstants.PROP_XP);
      xpView = new ItemVariableView.TextField(property, DecimalFormatEpsilonEnum.COMPARISON);
      brancheSelection = new SousModeleBrancheCreationComponent();
      if (init == null) {
        brancheSelection.updateValuesAndSelection(topComponent.getScenario(), topComponent.sousModeleBranches.getSelectedBranche(), topComponent.sousModeleBranches.getSelectedSousModele());
      } else {
        brancheSelection.updateValuesAndSelection(topComponent.getScenario(), init.branche, init.sousModele);
      }
      panel.add(new JLabel(NbBundle.getMessage(SousModeleBrancheCreationComponent.class, "CreateProfil.SousModeleParent.Label")));
      panel.add(brancheSelection.getCbSousModeles());
      panel.add(SysdocUrlBuilder.createButtonHelpDialog(PerspectiveEnum.MODELLING, "creerNouveauProfilSection"));
      panel.add(new JLabel(NbBundle.getMessage(SousModeleBrancheCreationComponent.class, "CreateProfil.BrancheParent.Label")));
      panel.add(brancheSelection.getCbBranches());
      lbDistance.setToolTipText(NbBundle.getMessage(SousModeleBrancheCreationComponent.class, "CreateProfil.BrancheDistance.Tooltip"));
      panel.add(lbDistance);
      panel.add(new JLabel(property.getNom()));
      panel.add(xpView.getTxt());
      panel.add(xpView.getLabel());
      if (init != null && init.xp != null) {
        xpView.setValue(init.xp);
      }
      brancheSelectionChanged();
      brancheSelection.getCbBranches().addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          if (e.getStateChange() == ItemEvent.SELECTED) {
            brancheSelectionChanged();
          }
        }
      });
    }

    protected void brancheSelectionChanged() {
      CatEMHBranche selectedBranche = brancheSelection.getSelectedBranche();
      xpView.getTxt().setEnabled(selectedBranche != null);
      xpView.getLabel().setVisible(selectedBranche != null);
      if (selectedBranche != null) {
        lbDistance.setText(property.format(selectedBranche.getLength(), DecimalFormatEpsilonEnum.COMPARISON) + PropertyNature.getUniteSuffixe(property));
        lbDistance.setToolTipText(property.format(selectedBranche.getLength(), DecimalFormatEpsilonEnum.COMPARISON) + PropertyNature.getUniteSuffixe(property));
      }
    }

    protected CtuluLog validate() {
      EMHSousModele selectedSousModele = brancheSelection.getSelectedSousModele();
      CatEMHBranche selectedBranche = brancheSelection.getSelectedBranche();
      CtuluLog res = new CtuluLog();
      if (selectedSousModele == null) {
        res.addSevereError(NbBundle.getMessage(SousModeleBrancheCreationComponent.class, "CreationProfil.NoSousModeleSelected.error"));
      }
      if (selectedBranche != null) {
        String errorMsg = xpView.getErrorMsg();
        if (errorMsg != null) {
          res.addSevereError(errorMsg);
        } else if (xpView.getValue() == null) {
          res.addSevereError(NbBundle.getMessage(SousModeleBrancheCreationComponent.class, "CreationProfil.NoXpSet.error"));
        } else {
          List<RelationEMHSectionDansBranche> sections = selectedBranche.getSections();
          DoubleEpsilonComparator comparator = new DoubleEpsilonComparator(property.getEpsilon());
          TreeSet<Double> existingXp = new TreeSet<>(comparator);
          for (RelationEMHSectionDansBranche section : sections) {
            existingXp.add(section.getXp());
          }
          Double newXp = (Double) xpView.getValue();
          if (existingXp.contains(newXp)) {
            res.addSevereError(NbBundle.getMessage(SousModeleBrancheCreationComponent.class, "CreationProfil.XpDuplicate.error"));
          }
        }
      }
      return res;
    }

    CreationData getCreationData() {
      CreationData res = new CreationData();
      res.branche = (EMHBrancheSaintVenant) brancheSelection.getSelectedBranche();
      res.sousModele = brancheSelection.getSelectedSousModele();
      res.xp = (Double) xpView.getValue();
      return res;
    }
  }

  public static CreationData showCreateDialog(String title, ProfilSectionTopComponent topComponent) {
    return showCreateDialog(title, topComponent, null);
  }

  public static CreationData showCreateDialog(String title, ProfilSectionTopComponent topComponent, CreationData init) {
    NewProfilEditor editor = new NewProfilEditor(topComponent, init);
    boolean ok = DialogHelper.showQuestionOkCancel(title, editor.panel);
    if (!ok) {
      return null;
    }
    CtuluLog validate = editor.validate();
    if (validate.containsErrorOrSevereError()) {
      LogsDisplayer.displayError(validate, title);
      return showCreateDialog(title, topComponent, editor.getCreationData());
    }
    return editor.getCreationData();

  }
}
