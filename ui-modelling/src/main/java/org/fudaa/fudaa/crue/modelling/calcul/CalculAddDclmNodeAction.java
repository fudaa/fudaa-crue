/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuGridLayout;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.DclmFactory;
import org.fudaa.dodico.crue.metier.factory.DclmFactory.CalcBuilder;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.helper.ComboBoxHelper;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Frederic Deniger
 */
public class CalculAddDclmNodeAction extends AbstractEditNodeAction {

  final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);

  public CalculAddDclmNodeAction() {
    super(org.openide.util.NbBundle.getMessage(CalculAddDclmNodeAction.class, "AddDclm.ActionName"));
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    return activatedNodes.length == 1;
  }

  @Override
  protected void performAction(final Node[] activatedNodes) {
    final Node node = activatedNodes[0];
    final CalculEMHsNode targetNode = (CalculEMHsNode) node;
    final Calc calculParent = targetNode.getCalc();
    final EMHScenario scenario = modellingScenarioService.getScenarioLoaded();
    final List<CalcBuilder> creators = DclmFactory.getCreators(calculParent);
    creators.sort(ObjetNommeByNameComparator.INSTANCE);
    final ObjetNommeCellRenderer renderer = new ObjetNommeCellRenderer();
    final JComboBox cb = new JComboBox();
    cb.setRenderer(renderer);
    final JComboBox cbEMH = new JComboBox();
    cbEMH.setRenderer(renderer);
    ComboBoxHelper.setDefaultModel(cb, creators);
    final JPanel pn = new JPanel(new BuGridLayout(2, 5, 5));
    pn.setBorder(BuBorders.EMPTY3333);
    pn.add(new JLabel(NbBundle.getMessage(CalculAddDclmNodeAction.class, "AddDclm.Dclm.DisplayName")));
    pn.add(cb);
    pn.add(new JLabel(NbBundle.getMessage(CalculAddDclmNodeAction.class, "AddDclm.EMH.DisplayName")));
    pn.add(cbEMH);
    updateComboBox(scenario, cbEMH, (CalcBuilder) cb.getSelectedItem());
    final DialogDescriptor descriptor = new DialogDescriptor(pn, getName());

    cb.addItemListener(e -> {
      updateComboBox(scenario, cbEMH, (CalcBuilder) cb.getSelectedItem());
      descriptor.setValid(cb.getSelectedItem() != null && cbEMH.getSelectedItem() != null);
    });
    cbEMH.addItemListener(e -> descriptor.setValid(cb.getSelectedItem() != null && cbEMH.getSelectedItem() != null));
    descriptor.setValid(cb.getSelectedItem() != null && cbEMH.getSelectedItem() != null);
    SysdocUrlBuilder.installDialogHelpCtx(descriptor, "ajouterCLimM", PerspectiveEnum.MODELLING,false);
    final Dialog dialog = DialogDisplayer.getDefault().createDialog(descriptor);
    dialog.pack();
    dialog.setLocationRelativeTo(WindowManager.getDefault().getMainWindow());
    dialog.setVisible(true);
    if (NotifyDescriptor.OK_OPTION.equals(descriptor.getValue())) {
      final CalcBuilder builder = (CalcBuilder) cb.getSelectedItem();
      final DonCLimMCommonItem newDclm = DclmFactory.create(builder, modellingScenarioService.getSelectedProjet().getPropDefinition(),
              (EMH) cbEMH.getSelectedItem(), calculParent);
      if (calculParent.isTransitoire() && newDclm instanceof CalcTransItem) {
        final List<Loi> filterDLHY = LoiHelper.filterDLHY(scenario, ((CalcTransItem) newDclm).getTypeLoi());
        if (filterDLHY.isEmpty()) {
          DialogHelper.showError(getName(), NbBundle.getMessage(CalculAddDclmNodeAction.class, "AddDclmTransitoire.NoLoiError"));
          return;
        }
      }
      targetNode.addDclm(newDclm);

    }

  }

  protected void updateComboBox(final EMHScenario scenario, final JComboBox cbEMH, final CalcBuilder builder) {
    final List<EMH> allEMH = scenario.getAllEMH();
    final List<EMH> filteredEMH = new ArrayList<>();
    if (builder != null) {
      for (final EMH emh : allEMH) {
        if (builder.isAccepted(emh)) {
          filteredEMH.add(emh);
        }
      }
    }
    Collections.sort(filteredEMH, ObjetNommeByNameComparator.INSTANCE);
    ComboBoxHelper.setDefaultModel(cbEMH, filteredEMH);
  }
}
