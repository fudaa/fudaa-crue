package org.fudaa.fudaa.crue.modelling.edition;

import com.Ostermiller.util.CSVParser;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.EditionCreateEMH;
import org.fudaa.dodico.crue.edition.bean.ListCasierContent;
import org.fudaa.dodico.crue.edition.bean.ValidationMessage;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.list.AbstractListContentNode;
import org.fudaa.fudaa.crue.modelling.list.ListCasierContentAddNode;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ModellingConfigService;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioModificationService;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.util.*;
import java.util.List;

/**
 * @author deniger
 */
public class ListAjoutCasierProcess {
    protected final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
            ModellingScenarioModificationService.class);
    final ModellingConfigService modellingConfigService = Lookup.getDefault().lookup(ModellingConfigService.class);

    public static List<CatEMHNoeud> getAvailableNoeudsForCasiers(EMHSousModele sousModele) {
        List<CatEMHNoeud> noeuds = sousModele.getNoeuds();
        List<CatEMHNoeud> res = new ArrayList<>();
        for (CatEMHNoeud noeud : noeuds) {
            if (noeud.getCasier() == null) {
                res.add(noeud);
            }
        }
        Collections.sort(res, ObjetNommeByNameComparator.INSTANCE);
        return res;
    }

    public static List<CatEMHNoeud> getAvailableNoeudsForEditionCasiers(EMHSousModele sousModele) {
        List<CatEMHNoeud> noeuds = sousModele.getNoeuds();
        List<CatEMHNoeud> res = new ArrayList<>();
        for (CatEMHNoeud noeud : noeuds) {
            boolean canAdd = true;
            if (noeud.getCasier() != null) {
                canAdd = noeud.getCasier().getParent().equals(sousModele);
            }
            if (canAdd) {
                res.add(noeud);
            }
        }
        Collections.sort(res, ObjetNommeByNameComparator.INSTANCE);
        return res;
    }

    public static List<String> getAvailableNoeudsNameForCasiers(EMHSousModele sousModele) {
        List<CatEMHNoeud> availableNoeuds = getAvailableNoeudsForCasiers(sousModele);
        return TransformerHelper.toNom(availableNoeuds);
    }

    public static List<ListCasierContentAddNode> createNodes(PerspectiveServiceModelling perspectiveServiceModelling, EMHSousModele sousModele) {
        List<String> availableNoeuds = getAvailableNoeudsNameForCasiers(sousModele);
        try {
            String toCopy = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
            String[][] parse = CSVParser.parse(toCopy, '\t');
            List<ListCasierContentAddNode> contents = new ArrayList<>();
            for (String[] strings : parse) {
                if (strings.length >= 1) {
                    ListCasierContent newContent = new ListCasierContent(strings[0]);
                    if (strings.length >= 2) {
                        newContent.setNoeud(strings[1]);
                        if (strings.length >= 3) {
                            newContent.setCommentaire(strings[2]);
                        }
                    }

                    contents.add(new ListCasierContentAddNode(newContent, perspectiveServiceModelling, availableNoeuds));
                }
            }
            return contents;
        } catch (Exception exception) {
        }
        return Collections.emptyList();
    }

    /**
     * @param newCasiers liste de ListNoeudContentAddNode
     * @param sousModele le sous-modele
     * @return log de l'operation
     */
    public CtuluLog processAjout(Node[] newCasiers, EMHSousModele sousModele) {
        Map<ListCasierContent, ListCasierContentAddNode> contents = getContentList(newCasiers);
        CrueConfigMetier ccm = modellingScenarioModificationService.getModellingScenarioService().getSelectedProjet().getCoeurConfig().getCrueConfigMetier();
        EditionCreateEMH createProcess = new EditionCreateEMH(modellingConfigService.getDefaultValues());
        final ValidationMessage<ListCasierContent> createCasiersMessages = createProcess.createCasiers(sousModele, contents.keySet(), ccm);
        Collection<ListCasierContentAddNode> values = contents.values();
        AbstractListContentNode.reinitMessages(values);
        if (createCasiersMessages.accepted) {
            modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.EMH_CREATION));
        } else {
            List<ValidationMessage.ResultItem<ListCasierContent>> msgs = createCasiersMessages.messages;
            for (ValidationMessage.ResultItem<ListCasierContent> resultItem : msgs) {
                ListCasierContentAddNode node = contents.get(resultItem.bean);
                node.setContainError(true);
                node.setShortDescription("<html><body>" + StringUtils.join(resultItem.messages, "<br>") + "</body></html>");
            }
        }
        return createCasiersMessages.log;
    }

    private LinkedHashMap<ListCasierContent, ListCasierContentAddNode> getContentList(Node[] newCasiers) {
        LinkedHashMap<ListCasierContent, ListCasierContentAddNode> contents = new LinkedHashMap<>();
        for (Node node : newCasiers) {
            ListCasierContent content = node.getLookup().lookup(ListCasierContent.class);
            if (content != null) {
                contents.put(content, (ListCasierContentAddNode) node);
            }
        }
        return contents;
    }
}
