/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.NbSheetCustom;
import org.fudaa.fudaa.crue.modelling.action.ModellingGlobalFindAction;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.modelling//ModellingPropertiesTopComponent//EN",
                     autostore = false)
@TopComponent.Description(preferredID = ModellingPropertiesTopComponent.TOPCOMPONENT_ID,
                          iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png",
                          persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "modelling-bottomLeft", openAtStartup = false, position = 3)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.modelling.ModellingPropertiesTopComponent")
@ActionReference(path = "Menu/Window/Modelling",position = 9, separatorBefore = 8)
@TopComponent.OpenActionRegistration(displayName = ModellingPropertiesTopComponent.TOPCOMPONENT_ACTION_DISPLAYNAME,
                                     preferredID = ModellingPropertiesTopComponent.TOPCOMPONENT_ID)
public final class ModellingPropertiesTopComponent extends NbSheetCustom {

  public static final String TOPCOMPONENT_ID = "ModellingPropertiesTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;

  public ModellingPropertiesTopComponent() {
    super(true, PerspectiveEnum.MODELLING);
    setName(NbBundle.getMessage(ModellingPropertiesTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingPropertiesTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    ModellingGlobalFindAction.installAction(this);
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getTopComponentHelpCtxId("vueProprietes", PerspectiveEnum.MODELLING));
  }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
  void writeProperties(java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
    p.setProperty("version", "1.0");
  }

  void readProperties(java.util.Properties p) {
  }
}
