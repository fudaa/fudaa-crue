/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import javax.swing.Action;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.perspective.ActiveModelisation;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View", id = "org.fudaa.fudaa.crue.modelling.ModellingOpenListFrottementsAction")
@ActionRegistration(displayName = "#ModellingListFrottementsNodeAction.Name")
@ActionReferences({
  @ActionReference(path = ActiveModelisation.ACTIONS_MODELLING_VIEWS, position = 16, separatorAfter = 17)
})
public final class ModellingOpenListFrottementsAction extends AbstractModellingSousModeleOpenAction {

  public ModellingOpenListFrottementsAction() {
    putValue(Action.NAME, NbBundle.getMessage(ModellingOpenListFrottementsAction.class, "ModellingListFrottementsNodeAction.Name"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingOpenListFrottementsAction();
  }

  @Override
  protected void doAction(EMHSousModele modele) {
    ModellingOpenListFrottementsNodeAction.open(modele.getUiId(), true);
  }
}
