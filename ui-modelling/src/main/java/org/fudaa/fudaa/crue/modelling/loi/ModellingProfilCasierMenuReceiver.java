/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.loi;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.DonPrtGeoProfilSimplifier;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilCasier;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.view.ItemVariableView;
import org.fudaa.fudaa.crue.loi.casier.ProfilCasierLoiUiController;
import org.fudaa.fudaa.crue.loi.common.LoiPopupMenuReceiver;
import org.fudaa.fudaa.crue.modelling.services.ModellingConfigService;
import org.fudaa.fudaa.crue.options.config.ModellingGlobalConfiguration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
class ModellingProfilCasierMenuReceiver extends LoiPopupMenuReceiver {

  private SimplifyProfil simplifyAction;
  private final ProfilCasierEditorTopComponent topComponent;

  public ModellingProfilCasierMenuReceiver(ProfilCasierLoiUiController uiController, ProfilCasierEditorTopComponent topComponent) {
    super(uiController.getPanel());
    this.topComponent = topComponent;
  }

  @Override
  protected void addItems() {
    super.addItems();
    popupMenu.addSeparator();
    simplifyAction = new SimplifyProfil();
    popupMenu.add(simplifyAction);
  }

  @Override
  protected void updateItemStateBeforeShow() {
    super.updateItemStateBeforeShow();
    EGCourbe selectedComponent = getPanel().getGraphe().getSelectedComponent();
    final boolean editable = selectedComponent != null && selectedComponent.getModel().isModifiable();
    simplifyAction.setEnabled(editable);
  }

  protected class SimplifyProfil extends EbliActionSimple {

    public SimplifyProfil() {
      super(NbBundle.getMessage(ModellingProfilCasierMenuReceiver.class, "simplifyCasierProfilAction"), null, "SIMPLIFY_CASIER");
    }

    @Override
    public void actionPerformed(ActionEvent _e) {
      final CrueConfigMetier ccm = topComponent.getLoiUIController().getCcm();
      ItemVariable seuilVariable = ccm.getProperty(
              CrueConfigMetierConstants.PROP_DEFAULT_SEUILSIMPLIFPROFILCASIER);
      ModellingConfigService modellingConfigService = Lookup.getDefault().lookup(ModellingConfigService.class);
      Double initSeuilSimplifProfilCasier = modellingConfigService.getModellingGlobalConfiguration().getSeuilSimplifProfilCasier();
      ItemVariableView view = new ItemVariableView.TextField(seuilVariable, DecimalFormatEpsilonEnum.PRESENTATION);
      view.setValue(initSeuilSimplifProfilCasier);
      JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
      panel.add(new JLabel(NbBundle.getMessage(ModellingProfilCasierMenuReceiver.class, "seuilSimplifyCasierProfilLabel")));
      panel.add(view.getPanelComponent());
      boolean accepted = DialogHelper.showQuestionOkCancel(getTitle(), panel);
      if (!accepted || view.getValue() == null) {
        return;
      }
      Double seuil = (Double) view.getValue();
      if (seuil.compareTo(0d) <= 0) {
        return;
      }
      if (seuilVariable != null && !seuilVariable.getEpsilon().isSame(seuil, initSeuilSimplifProfilCasier)) {
        final ModellingGlobalConfiguration modellingGlobalConfiguration = modellingConfigService.getModellingGlobalConfiguration();
        modellingGlobalConfiguration.setSeuilSimplifProfilCasier(seuil);
        modellingConfigService.setModellingGlobalConfiguration(modellingGlobalConfiguration);
      }

      DonPrtGeoProfilCasier inEdition = topComponent.createEdited();
      DonPrtGeoProfilSimplifier updater = new DonPrtGeoProfilSimplifier(seuil, ccm);
      DonPrtGeoProfilCasier simplified = updater.simplify(inEdition);
      simplified.setEmh(inEdition.getEmh());
      topComponent.currentProfilModified(simplified);
    }
  }
}
