package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.list.ModellingListFrottementTopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;

/**
 *
 * @author deniger
 */
public abstract class ModellingOpenListFrottementsNodeAction extends AbstractModellingOpenTopNodeAction<ModellingListFrottementTopComponent> {

  public ModellingOpenListFrottementsNodeAction(String name) {
    super(name, ModellingListFrottementTopComponent.MODE, ModellingListFrottementTopComponent.TOPCOMPONENT_ID);
  }

  @Override
  protected TopComponent activateNewTopComponent(ModellingListFrottementTopComponent tc, Node selectedNode) {
    Long uid = getUid(selectedNode);
    if (uid == null) {
      return null;
    }
    tc.setSousModeleUid(uid);
    return tc;
  }

  public static void open(EMHSousModele emh, boolean reopen) {
    if (emh != null) {
      open(emh.getUiId(), reopen);
    }
  }

  public static void open(Long uid, boolean reopen) {
    if (uid == null) {
      return;
    }
    ModellingOpenListFrottementsNodeAction action = null;
    if (reopen) {
      action = SystemAction.get(ModellingOpenListFrottementsNodeAction.Reopen.class);
    } else {
      action = SystemAction.get(ModellingOpenListFrottementsNodeAction.NewFrame.class);
    }
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.singleton(uid));
    action.performAction(new Node[]{node});
  }

  @Override
  protected void activeWithValue(ModellingListFrottementTopComponent opened, Node selectedNode) {
    Long uid = getUid(selectedNode);
    if (uid != null) {
      opened.reloadWithSousModeleUid(uid);
    }
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return activatedNodes.length == 1 && (activatedNodes[0].getLookup().lookup(EMHSousModele.class) != null
            || activatedNodes[0].getLookup().lookup(Long.class) != null);
  }

  public static class Reopen extends ModellingOpenListFrottementsNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenListFrottementsNodeAction.class, "ModellingListFrottementsNodeAction.Name"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenListFrottementsNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenListFrottementsNodeAction.class, "ModellingListFrottementsNodeAction.NewFrame.Name"));
      setReopen(false);
    }
  }
}
