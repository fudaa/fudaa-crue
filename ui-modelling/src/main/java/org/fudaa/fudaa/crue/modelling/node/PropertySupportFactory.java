/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.node;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.emh.ValParam;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.property.ValidityValueListener;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;

/**
 *
 * @author Frédéric Deniger
 */
public class PropertySupportFactory {

  public static PropertySupportReflection createFor(ValParam valParam, AbstractNodeFirable node, CrueConfigMetier ccm) {
    try {
      PropertySupportReflection support = new PropertySupportReflection(node, valParam, valParam.getTypeData(), ValParam.PROP_VALEUR);
      support.setName(valParam.getNom());
      support.setDisplayName(BusinessMessages.getStringOrDefault(valParam.getNom() + ".shortDescription", valParam.getNom()));
      ItemVariable property = ccm.getProperty(StringUtils.uncapitalize(valParam.getNom()));
      if (property != null) {
        ValidityValueListener listener = new ValidityValueListener(property, support, node);
        listener.updateState();
      }
      return support;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return null;
  }

  public static Sheet.Set createSet(Object in, AbstractNodeFirable node) {
    Sheet.Set set = new Sheet.Set();
    set.setName(in.getClass().getSimpleName());
    set.setDisplayName(BusinessMessages.geti18nForClass(in.getClass()));
    PropertyNodeBuilder nodeBuilder = new PropertyNodeBuilder();
    List<PropertySupportReflection> createFromPropertyDesc = nodeBuilder.createFromPropertyDesc(DecimalFormatEpsilonEnum.COMPARISON, in, node);
    for (PropertySupportReflection propertySupportReflection : createFromPropertyDesc) {
      set.put(propertySupportReflection);
    }
    return set;
  }
}
