/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.action;

import java.util.List;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.view.ChooseInListHelper;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public abstract class AbstractModellingModeleOpenAction extends AbstractModellingOpenAction {

  @Override
  protected void doAction() {
    List<EMHModeleBase> modeles = scenarioService.getScenarioLoaded().getModeles();
    if (modeles.size() == 1) {
      doAction(modeles.get(0));
    } else {
      ChooseInListHelper<EMHModeleBase> chooser = new ChooseInListHelper<>();
      chooser.setDialogTitle(NbBundle.getMessage(AbstractModellingModeleOpenAction.class, "ChooseModele.DialogTitle"));
      chooser.setRenderer(new ObjetNommeCellRenderer());
      EMHModeleBase choose = chooser.choose(modeles, modeles.get(0));
      if (choose != null) {
        doAction(choose);
      }
    }

  }

  protected abstract void doAction(EMHModeleBase modele);
}
