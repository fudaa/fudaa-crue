/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.modelling.node.AbstractModellingNodeFirable;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frederic Deniger
 */
public class CalculCommentaireNode extends AbstractModellingNodeFirable {

  public CalculCommentaireNode(Children children, Calc calc, PerspectiveServiceModelling perspectiveServiceModelling) {
    super(children, Lookups.fixed(calc), perspectiveServiceModelling);
    setName(BusinessMessages.getString("Comment.property"));
  }

  public Calc getCalc() {
    return getLookup().lookup(Calc.class);
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(getLookup().lookup(Calc.class)));
  }

  @Override
  protected Sheet createSheet() {
    PropertyNodeBuilder builder = new PropertyNodeBuilder();
    PropertySupportReflection value = builder.createFromPropertyDesc(DecimalFormatEpsilonEnum.COMPARISON, getCalc(), this, "commentaire");
    PropertyCrueUtils.configureCustomEditor(value);

    value.setName(CalculNode.COLUMN_VALUE);
    Sheet res = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    res.put(set);
    set.put(value);
    return res;
  }
}
