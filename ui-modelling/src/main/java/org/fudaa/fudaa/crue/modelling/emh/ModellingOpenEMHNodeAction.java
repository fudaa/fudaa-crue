/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.emh;

import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.fudaa.crue.modelling.action.AbstractModellingOpenTopNodeAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;

/**
 *
 * @author Frédéric Deniger
 */
public class ModellingOpenEMHNodeAction extends AbstractModellingOpenTopNodeAction<AbstractModellingEMHTopComponent> {

  public ModellingOpenEMHNodeAction(final String name) {
    super(name, null, null);
  }

  public ModellingOpenEMHNodeAction() {
    super(NbBundle.getMessage(ModellingOpenEMHNodeAction.class, "ModellingOpenEMHNodeAction.DisplayName"), null, null);
  }

  @Override
  protected void activeWithValue(final AbstractModellingEMHTopComponent opened, final Node selectedNode) {
    final EMH emh = selectedNode.getLookup().lookup(EMH.class);
    opened.reloadWithEMHUid(emh.getUiId());
  }

  @Override
  protected TopComponent activateNewTopComponent(final AbstractModellingEMHTopComponent newTopComponent, final Node selectedNode) {
    final EMH emh = selectedNode.getLookup().lookup(EMH.class);
    newTopComponent.setEMHUid(emh.getUiId());
    return newTopComponent;
  }

  @Override
  protected String getTopComponentId(final Node selectedNode) {
    final EMH emh = selectedNode.getLookup().lookup(EMH.class);
    return getTopComponentId(emh);
  }

  public static void open(final EMH emh, final boolean reopen) {
    if (emh == null) {
      return;
    }
    ModellingOpenEMHNodeAction action = null;
    if (reopen) {
      action = SystemAction.get(ModellingOpenEMHNodeAction.Reopen.class);
    } else {
      action = SystemAction.get(ModellingOpenEMHNodeAction.NewFrame.class);
    }
    final AbstractNode node = new AbstractNode(Children.LEAF, Lookups.singleton(emh));
    action.performAction(new Node[]{node});

  }

  private Class getClass(final EMH emh) {
    final EnumCatEMH catType = emh.getCatType();
    switch (catType) {
      case NOEUD:
        return ModellingEMHNoeudTopComponent.class;
      case SECTION:
        return ModellingEMHSectionTopComponent.class;
      case CASIER:
        return ModellingEMHCasierTopComponent.class;
      case BRANCHE:
        return ModellingEMHBrancheTopComponent.class;
    }
    throw new UnsupportedOperationException("Not supported yet.");
  }

  private String getTopComponentId(final EMH emh) {
    final EnumCatEMH catType = emh.getCatType();
    switch (catType) {
      case NOEUD:
        return ModellingEMHNoeudTopComponent.TOPCOMPONENT_ID;
      case SECTION:
        return ModellingEMHSectionTopComponent.TOPCOMPONENT_ID;
      case CASIER:
        return ModellingEMHCasierTopComponent.TOPCOMPONENT_ID;
      case BRANCHE:
        return ModellingEMHBrancheTopComponent.TOPCOMPONENT_ID;
    }
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  protected String getMode(final TopComponent top) {
    final Class<? extends TopComponent> aClass = top.getClass();
    if (ModellingEMHNoeudTopComponent.class.equals(aClass)) {
      return ModellingEMHNoeudTopComponent.MODE;
    }
    if (ModellingEMHSectionTopComponent.class.equals(aClass)) {
      return ModellingEMHSectionTopComponent.MODE;
    }
    if (ModellingEMHCasierTopComponent.class.equals(aClass)) {
      return ModellingEMHCasierTopComponent.MODE;
    }
    //plusieurs type de branche
    if (ModellingEMHBrancheTopComponent.class.isAssignableFrom(aClass)) {
      return ModellingEMHBrancheTopComponent.MODE;
    }

    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    if (activatedNodes.length == 1) {
      final EMH emh = activatedNodes[0].getLookup().lookup(EMH.class);
      if (emh == null) {
        return false;
      }
      return !emh.getCatType().isContainer();
    }
    return false;
  }

  public static class Reopen extends ModellingOpenEMHNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenEMHNodeAction.class, "ModellingOpenEMHNodeAction.DisplayName"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenEMHNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenEMHNodeAction.class, "ModellingOpenEMHNodeAction.NewFrame.DisplayName"));
      setReopen(false);
    }
  }
}
