package org.fudaa.fudaa.crue.modelling.calcul.importer;

import com.memoire.bu.BuPreferences;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.fudaa.commun.FudaaPreferences;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.openide.DialogDescriptor;
import org.openide.util.NbBundle;

import java.awt.*;

public class RunCalculChooser {
  public static void main(String[] arg) {
    new RunCalculChooser().choose();
    BuPreferences.BU.writeIniFile();
    FudaaPreferences.FUDAA.writeIniFile();
    System.exit(0);
  }

  public Pair<EMHScenario, ResultatTimeKey> choose() {
    RunCalculChooserDialogPanel etudeScenarioDialog = new RunCalculChooserDialogPanel();
    etudeScenarioDialog.setPreferredSize(new Dimension(450, 450));
    DialogDescriptor dialogDescriptor = new DialogDescriptor(etudeScenarioDialog,
        NbBundle.getMessage(RunCalculChooser.class, "RunCalculChooser.DialogTitle"));
    etudeScenarioDialog.setDialogDescriptor(dialogDescriptor);
    boolean accepted = DialogHelper.showQuestion(dialogDescriptor, getClass(), null, PerspectiveEnum.MODELLING,
        DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    if (accepted) {
      return etudeScenarioDialog.getValues();
    }
    return null;
  }
}
