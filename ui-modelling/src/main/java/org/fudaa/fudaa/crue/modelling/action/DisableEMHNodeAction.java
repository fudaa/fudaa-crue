package org.fudaa.fudaa.crue.modelling.action;

import org.openide.util.NbBundle;

/**
 *
 * @author pasinato
 */
public class DisableEMHNodeAction extends ToggleActivationNodeAction {

    public DisableEMHNodeAction() {
        super(false, NbBundle.getMessage(ToggleActivationNodeAction.class, "DisableEMHNodeAction.Name"));
    }
}
