package org.fudaa.fudaa.crue.modelling.node;

import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.util.Lookup;

/**
 *
 * @author Frédéric Deniger
 */
public class AbstractModellingNodeFirable extends AbstractNodeFirable {

  protected final PerspectiveServiceModelling perspectiveServiceModelling;

  public AbstractModellingNodeFirable(Children children, Lookup lookup, PerspectiveServiceModelling perspectiveServiceModelling) {
    super(children, lookup);
    this.perspectiveServiceModelling = perspectiveServiceModelling;
    setNoImage();
  }

  public AbstractModellingNodeFirable(Children children, PerspectiveServiceModelling perspectiveServiceModelling) {
    super(children);
    this.perspectiveServiceModelling = perspectiveServiceModelling;
    setNoImage();
  }

  @Override
  public final boolean isEditMode() {
    //le premier test bizarre (perspectiveServiceModelling == null)  est utilise uniquement pour les tests.
    return perspectiveServiceModelling == null || perspectiveServiceModelling.isInEditMode();
  }

 
}
