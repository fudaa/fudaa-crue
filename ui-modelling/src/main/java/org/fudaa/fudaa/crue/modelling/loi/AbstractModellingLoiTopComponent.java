/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.loi;

import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiUiController;
import org.fudaa.fudaa.crue.modelling.AbstractModellingTopComponent;

/**
 *
 * @author Frederic Deniger
 */
public abstract class AbstractModellingLoiTopComponent extends AbstractModellingTopComponent {

  private LoiConfigController configController;
  private boolean dataModified;

  public final void setConfigModified() {
    super.setModified(true);
  }

  @Override
  public void setModified(boolean modified) {
    dataModified = modified;
    if (configController != null) {
      super.setModified(modified || configController.isConfigModified());
    } else {
      super.setModified(modified);
    }
  }

  @Override
  public final void valideModificationHandler() {
    configController.updating = true;
    if (dataModified) {
      boolean ok = valideModificationDataHandler();
      if (!ok) {
        configController.updating = false;
        return;
      }
    }
    configController.updating = false;
    configController.saveCourbeConfig();
    setModified(false);
  }

  protected abstract boolean valideModificationDataHandler();

  protected abstract AbstractLoiUiController getLoiUIController();

  protected abstract String getDefaultTopComponentId();

  @Override
  protected final void scenarioLoaded() {
    scenarioLoadedHandler();
    if (configController == null) {
      configController = new LoiConfigController(this, getLoiUIController());
    }
    configController.install();
  }

  protected void applyCourbeConfig() {
    if (configController != null) {
      configController.applyConfig();
    }
  }

  protected abstract void scenarioLoadedHandler();

  protected abstract void scenarioUnloadedHandler();

  @Override
  protected void setEditable(boolean b) {
    if (getLoiUIController() != null) {
      for (EbliActionInterface action : getLoiUIController().getEditActions()) {
        action.setEnabled(b);
      }
    }
  }

  protected void reloadCourbeConfig() {
    configController.reloadCourbeConfig();
  }

  @Override
  protected final void scenarioUnloaded() {
    if (configController != null) {
      configController.uninstall();
    }
    scenarioUnloadedHandler();
  }
}
