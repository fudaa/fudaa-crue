package org.fudaa.fudaa.crue.modelling;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.edition.EditionUpdateCalc;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.emh.ui.OrdCalcScenarioUiState;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.validation.ValidationHelper;
import org.fudaa.fudaa.crue.common.helper.DisableSortHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.view.CustomNodePopupFactory;
import org.fudaa.fudaa.crue.common.view.ExpandedNodesManager;
import org.fudaa.fudaa.crue.modelling.calcul.CalculNode;
import org.fudaa.fudaa.crue.modelling.calcul.CalculNodeFactory;
import org.fudaa.fudaa.crue.modelling.calcul.CalculOcalNode;
import org.fudaa.fudaa.crue.modelling.calcul.CalculRenameNodeAction;
import org.fudaa.fudaa.crue.modelling.listener.ExplorerManagerListenerHelper;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.*;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.study//ModellingComputeDataTopComponent//EN",
    autostore = false)
@TopComponent.Description(preferredID = ModellingComputeDataTopComponent.TOPCOMPONENT_ID,
    iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png",
    persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "modelling-bottomRight", openAtStartup = false, position = 1)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.modelling.ModellingComputeDataTopComponent")
@ActionReference(path = "Menu/Window/Modelling", position = 6)
@TopComponent.OpenActionRegistration(displayName = ModellingComputeDataTopComponent.TOPCOMPONENT_ACTION_DISPLAYNAME,
    preferredID = ModellingComputeDataTopComponent.TOPCOMPONENT_ID)
public final class ModellingComputeDataTopComponent extends AbstractModellingTopComponent implements ExplorerManager.Provider {
  public static final String TOPCOMPONENT_ID = "ModellingComputeDataTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  final ExplorerManagerListenerHelper helper;
  protected final OutlineView outlineView;
  private final ExpandedNodesManager expandedNodesManager;
  final DisableSortHelper sortDisabler;
  boolean editable;
  final CalcNodeListener calcNodeListener = new CalcNodeListener();

  public ModellingComputeDataTopComponent() {
    initComponents();
    setName(NbBundle.getMessage(ModellingComputeDataTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingComputeDataTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    helper = new ExplorerManagerListenerHelper(this);
    outlineView = new OutlineView("");
    outlineView.getOutline().setColumnHidingAllowed(false);
    outlineView.getOutline().setRootVisible(false);
    outlineView.setDefaultActionAllowed(true);
    outlineView.setTreeSortable(false);
    final CustomNodePopupFactory customNodePopupFactory = new CalculNodePopupFactory();
    customNodePopupFactory.setShowQuickFilter(false);
    outlineView.setNodePopupFactory(customNodePopupFactory);
    outlineView.addPropertyColumn(CalculNode.COLUMN_VALUE, NbBundle.getMessage(ModellingComputeDataTopComponent.class, "Compute.ColumnValeur.Name"));
    outlineView.addPropertyColumn(CalculNode.COLUMN_SENS, NbBundle.getMessage(ModellingComputeDataTopComponent.class, "Compute.ColumnSens.Name"));
    outlineView.addPropertyColumn(CalculNode.COLUMN_ACTIF, NbBundle.getMessage(ModellingComputeDataTopComponent.class, "Compute.ColumnActive.Name"));
    outlineView.getOutline().getColumnModel().getColumn(1).setWidth(30);
    outlineView.getOutline().getColumnModel().getColumn(1).setPreferredWidth(30);
    outlineView.getOutline().getColumnModel().getColumn(3).setWidth(30);
    outlineView.getOutline().getColumnModel().getColumn(3).setPreferredWidth(30);
    add(outlineView);
    add(super.createCancelSaveButtons(), BorderLayout.SOUTH);
    associateLookup(ExplorerUtils.createLookup(helper.getExplorerManager(), getActionMap()));
    expandedNodesManager = new ExpandedNodesManager(outlineView.getOutline().getOutlineModel());
    sortDisabler = new DisableSortHelper(outlineView);
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueListeCalculs";
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return helper.getExplorerManager();
  }

  @Override
  public void setEditable(final boolean b) {
    helper.setEditable(b);
    //tri désactivé si edition en cours.
    sortDisabler.setDisableSort(b);
    this.editable = b;
  }

  @Override
  public void cancelModificationHandler() {
    scenarioReloaded();
    setModified(false);
  }

  @Override
  public void valideModificationHandler() {
    final Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    //contiendra tous les calculs. Dans un premier temps ne contient que les permanents
    final List<Calc> newCalcs = new ArrayList<>();
    //que les transitoires
    final List<Calc> newCalcsTransitoire = new ArrayList<>();
    //contiendra tous les ordres de calcul. Dans un premier temps ne contient que les permanents
    final List<OrdCalc> newOrdCalcs = new ArrayList<>();
    //que les transitoires
    final List<OrdCalc> newOrdCalcsTransitoire = new ArrayList<>();
    //contiendra les données persistées dans l'éditeur ( données ui uniquement). Ne contient que les permanents au début
    final OrdCalcScenarioUiState uiStates = new OrdCalcScenarioUiState();
    //que les tansitoires
    final OrdCalcScenarioUiState uiStatesTransitoire = new OrdCalcScenarioUiState();
    for (final Node node : nodes) {
      final CalculNode calculNode = (CalculNode) node;
      final boolean isCalculPermanent = calculNode.getCalc().isPermanent();
      final OrdCalcScenarioUiState uiStatesToUse = isCalculPermanent ? uiStates : uiStatesTransitoire;
      final List<Calc> newCalcsToUse = isCalculPermanent ? newCalcs : newCalcsTransitoire;
      final List<OrdCalc> newOrdCalcsToUse = isCalculPermanent ? newOrdCalcs : newOrdCalcsTransitoire;
      newCalcsToUse.add(calculNode.getCalc());
      final CalculOcalNode ocalNode = calculNode.getOcalNode();
      if (ocalNode != null) {
        newOrdCalcsToUse.add(ocalNode.getOrdCalc());
        uiStatesToUse.addCalcOrdCalcUiStates(calculNode.getCalc().getId(), ocalNode.getOrdCalc());
      } else if (calculNode.getOldUsedOrdCalc() != null) {
        uiStatesToUse.addCalcOrdCalcUiStates(calculNode.getCalc().getId(), calculNode.getOldUsedOrdCalc());
      } else {
        Logger.getLogger(ModellingComputeDataTopComponent.class.getName()).log(Level.SEVERE, "calc without ocal data");
      }
    }
    //pour mettre dans l'ordre les permanents puis les transitoires
    uiStates.addAll(uiStatesTransitoire);
    newOrdCalcs.addAll(newOrdCalcsTransitoire);
    newCalcs.addAll(newCalcsTransitoire);

    final OrdCalcScenario ocalTemp = new OrdCalcScenario();
    final DonCLimMScenario dclmTemp = new DonCLimMScenario();
    for (final OrdCalc ordCalc : newOrdCalcs) {
      ocalTemp.addOrdCalc(ordCalc);
    }
    for (final Calc calc : newCalcs) {
      dclmTemp.addCalc(calc);
    }
    final CtuluLogGroup group = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    group.addLog(ValidationHelper.validateDonCLimMScenarioAndClim(dclmTemp, getCcm()));
    group.addLog(ValidationHelper.validateOrdCalcScenario(ocalTemp, getCcm()));
    if (group.containsSomething()) {
      LogsDisplayer.displayError(group, "");
    }
    if (group.containsFatalError()) {
      return;
    }

    final EditionUpdateCalc updater = new EditionUpdateCalc();
    updater.updateCalcs(newCalcs, newOrdCalcs, super.getScenario());
    modellingScenarioModificationService.getModellingScenarioService().getManagerScenarioLoaded().setUiOCalData(uiStates);
    modellingScenarioModificationService.setOcalUiStateModified();
    modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.OCAL));
    setModified(false);
    //pour les classes d'exemple/test uniquement
    if (Boolean.TRUE.equals(getClientProperty("TEST"))) {
      scenarioReloaded();
    }
  }

  /**
   * Enleve tous les listeners.
   */
  public void cleanListeners() {
    helper.cleanListener();
    if (helper.getExplorerManager().getRootContext() != null) {
      helper.getExplorerManager().getRootContext().removeNodeListener(calcNodeListener);
    }
  }

  @Override
  public void scenarioLoaded() {
    expandedNodesManager.saveState();
    cleanListeners();
    outlineView.putClientProperty(DonLoiHYConteneur.class, null);
    if (getScenario() != null) {
      final DonLoiHYConteneur cloneWithoutPoints = getScenario().getLoiConteneur().cloneWithoutPoints();
      outlineView.putClientProperty(DonLoiHYConteneur.class, cloneWithoutPoints);
    }
    //c'est la factory qui créé toute l'arborecence des noeuds de calcul:
    final CalculNodeFactory factory = new CalculNodeFactory(outlineView);
    outlineView.putClientProperty("modellingScenarioService.getScenarioLoaded()", ui);

    final List<CalculNode> createNodes = factory.createNodes();
    final Node createNode = NodeHelper.createNode(createNodes, "Calcs", false);
    helper.getExplorerManager().setRootContext(createNode);
    createNode.addNodeListener(calcNodeListener);
    helper.addListener(true);
    helper.getNodeListener().setModifiedIfArborescenceChanged(true);
    helper.getNodeListener().setModifiedIfDisplayNameChanged(false);
    helper.getNodeListener().setModifiedIfNameChanged(true);
    expandedNodesManager.restoreState();
  }

  @Override
  protected void scenarioReloaded() {
    scenarioLoaded();
  }

  @Override
  public void scenarioUnloaded() {
    helper.scenarioUnloaded();
    cleanListeners();
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables

  @Override
  public void componentClosedTemporarily() {
    expandedNodesManager.saveState();
  }

  @Override
  protected void componentOpenedHandler() {
    super.componentOpenedHandler();
    expandedNodesManager.restoreState();
  }

  void writeProperties(final java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
    p.setProperty("version", "1.0");
    p.setProperty("saved", "true");
    outlineView.writeSettings(p, "outlineView");
  }

  void readProperties(final java.util.Properties p) {
    final String saved = p.getProperty("saved");
    if ("true".equals(saved)) {
      outlineView.readSettings(p, "outlineView");
    }
  }

  protected void addNewPseudoCalc() {
    final String dialogTitle = NbBundle.getMessage(ModellingComputeDataTopComponent.class, "AddCalcPseudoPerm.Menu");
    final String chooseName = CalculRenameNodeAction.chooseName(CruePrefix.P_CALCUL_PSEUDOPERMANENT, null, getExplorerManager().getRootContext(), null, dialogTitle);
    if (chooseName != null) {
      final CalcPseudoPerm calc = new CalcPseudoPerm();
      calc.setNom(chooseName);
      final CalculNodeFactory fac = new CalculNodeFactory(outlineView);
      fac.addNewCalc(calc);
    }
  }

  protected void addNewTransCalc() {
    final String dialogTitle = NbBundle.getMessage(ModellingComputeDataTopComponent.class, "AddCalcTransitoire.Menu");
    final String chooseName = CalculRenameNodeAction.chooseName(CruePrefix.P_CALCUL_TRANSITOIRE, null, getExplorerManager().getRootContext(), null, dialogTitle);
    if (chooseName != null) {
      final CalcTrans calc = new CalcTrans();
      calc.setNom(chooseName);
      final CalculNodeFactory fac = new CalculNodeFactory(outlineView);
      fac.addNewCalc(calc);
    }
  }

  private class CalcNodeListener implements NodeListener {
    public void reorderChildren() {
      expandedNodesManager.saveState();
      setModified(true);
      expandedNodesManager.restoreState();
    }

    @Override
    public void propertyChange(final PropertyChangeEvent evt) {
    }

    @Override
    public void childrenAdded(final NodeMemberEvent ev) {
      reorderChildren();
    }

    @Override
    public void childrenRemoved(final NodeMemberEvent ev) {
      setModified(true);
    }

    @Override
    public void childrenReordered(final NodeReorderEvent ev) {
      setModified(true);
    }

    @Override
    public void nodeDestroyed(final NodeEvent ev) {
      setModified(true);
    }
  }

  private class CalculNodePopupFactory extends CustomNodePopupFactory {
    public CalculNodePopupFactory() {
      super(true, outlineView);
    }

    @Override
    public void addSpecificMenuItems(final JPopupMenu popupMenu, final int row, final int column, final Node[] selectedNodes, final Component component) {
      popupMenu.addSeparator();
      final JMenuItem addPseudoPerm = popupMenu.add(NbBundle.getMessage(ModellingComputeDataTopComponent.class, "AddCalcPseudoPerm.Menu"));
      final JMenuItem addTrans = popupMenu.add(NbBundle.getMessage(ModellingComputeDataTopComponent.class, "AddCalcTransitoire.Menu"));
      addPseudoPerm.setEnabled(editable);
      addTrans.setEnabled(editable);

      addPseudoPerm.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          addNewPseudoCalc();
        }
      });
      addTrans.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          addNewTransCalc();
        }
      });
    }

    @Override
    public boolean canMoveEndBeAdded(final Node[] selectedNodes) {
      for (final Node node : selectedNodes) {
        if (!node.getClass().equals(CalculNode.class)) {
          return false;
        }
      }
      return true;
    }
  }
}
