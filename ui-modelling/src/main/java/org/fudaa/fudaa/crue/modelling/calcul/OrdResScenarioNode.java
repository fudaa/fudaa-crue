/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.OrdRes;
import org.fudaa.dodico.crue.metier.emh.OrdResScenario;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertySupportReadWrite;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.modelling.node.AbstractModellingNodeFirable;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frédéric Deniger
 */
public class OrdResScenarioNode extends AbstractModellingNodeFirable {

  public OrdResScenarioNode(OrdResScenario ores, PerspectiveServiceModelling perspectiveServiceModelling) {
    super(Children.LEAF, Lookups.singleton(ores), perspectiveServiceModelling);
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(getLookup().lookup(OrdResScenario.class)));
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
    OrdResScenario ores = getLookup().lookup(OrdResScenario.class);
    sheet.put(createSet(ores.getOrdResNoeudNiveauContinu()));
    sheet.put(createSet(ores.getOrdResCasier()));
    sheet.put(createSet(ores.getOrdResSection()));
    sheet.put(createSet(ores.getOrdResBranchePdc()));
    sheet.put(createSet(ores.getOrdResBrancheSeuilTransversal()));
    sheet.put(createSet(ores.getOrdResBrancheSeuilLateral()));
    sheet.put(createSet(ores.getOrdResBrancheOrifice()));
    sheet.put(createSet(ores.getOrdResBrancheStrickler()));
    sheet.put(createSet(ores.getOrdResBrancheBarrageGenerique()));
    sheet.put(createSet(ores.getOrdResBrancheBarrageFilEau()));
    sheet.put(createSet(ores.getOrdResBrancheSaintVenant()));
    sheet.put(createSet(ores.getOrdResBrancheNiveauxAssocies()));
    sheet.put(createSet(ores.getOrdResModeleRegul()));
    return sheet;
  }

  private Sheet.Set createSet(OrdRes in) {
    Sheet.Set set = new Sheet.Set();
    set.setName(in.getClass().getSimpleName());
    set.setDisplayName(BusinessMessages.geti18nForClass(in.getClass()));
    ArrayList<String> ddes = CollectionCrueUtil.getSortedList(TransformerHelper.toId(in.getDdes()));
    for (String dde : ddes) {
      set.put(new DdePropertySupport(this, in, dde));
    }
    return set;
  }

  public static class DdePropertySupport extends PropertySupportReadWrite<OrdRes, Boolean> {

    private final String ddeName;

    public DdePropertySupport(AbstractNodeFirable node, OrdRes ores, String ddeName) {
      super(node, ores, Boolean.TYPE, ddeName, StringUtils.capitalize(ddeName));
      this.ddeName = ddeName;
    }

    @Override
    protected void setValueInInstance(Boolean newVal) {
      getInstance().setDdeValue(ddeName, newVal.booleanValue());
    }

    @Override
    public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
      return Boolean.valueOf(getInstance().getDdeValue(ddeName));
    }
  }
}
