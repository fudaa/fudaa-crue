/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import com.Ostermiller.util.CSVParser;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.table.CtuluExcelCsvFileReader;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.modelling.calcul.importer.CiniParserAndImporter;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.NbBundle;

import java.io.File;

/**
 * @author Frederic Deniger
 */
public class ModellingListCiniImportProgressRunnable implements ProgressRunnable<Object> {
  private File file;
  private final ModellingListCiniTopComponent topComponent;
  private String initValues;

  public ModellingListCiniImportProgressRunnable(File file, ModellingListCiniTopComponent topComponent) {
    this.file = file;
    this.topComponent = topComponent;
  }

  public ModellingListCiniImportProgressRunnable(String initValues, ModellingListCiniTopComponent topComponent) {
    this.topComponent = topComponent;
    this.initValues = initValues;
  }

  @Override
  public Object run(ProgressHandle handle) {
    String[][] values = null;
    if (initValues != null) {
      char sep = '\t';
      if (initValues.indexOf(';') > 0) {
        sep = ';';
      }
      values = CSVParser.parse(initValues, sep);
    } else if (file != null) {
      values = new CtuluExcelCsvFileReader(file).readFile();
    }
    if (values == null) {
      return null;
    }
    CiniParserAndImporter importer = new CiniParserAndImporter(values, topComponent.getExplorerManager().getRootContext());
    importer.setExplorerManager(topComponent.getExplorerManager());
    CtuluLog importData = importer.importData();
    if (importData.isNotEmpty()) {
      String title;
      if (file != null) {
        title = NbBundle.getMessage(ModellingListCiniImportProgressRunnable.class, "cini.importFile.bilan", file.
            getName());
      } else {
        title = NbBundle.getMessage(ModellingListCiniImportProgressRunnable.class, "cini.importClipboard.bilan");
      }
      LogsDisplayer.displayError(importData, title);
    }
    return null;
  }
}
