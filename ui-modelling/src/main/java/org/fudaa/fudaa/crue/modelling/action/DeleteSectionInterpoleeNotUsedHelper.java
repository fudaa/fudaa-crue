package org.fudaa.fudaa.crue.modelling.action;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;
import org.fudaa.dodico.crue.edition.EditionDelete;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.openide.util.NbBundle;

import java.util.List;

public class DeleteSectionInterpoleeNotUsedHelper {
  private final EMHSousModele sousModele;
  private final EMHScenario scenario;

  public DeleteSectionInterpoleeNotUsedHelper(EMHScenario scenario, EMHSousModele sousModele) {
    this.sousModele = sousModele;
    this.scenario = scenario;
  }


  public void operate() {
    List<CatEMHSection> sectionInterpoleeToDelete;
    if (sousModele == null) {
      sectionInterpoleeToDelete = EditionDelete.findSectionInterpoleeToDelete(scenario);
    } else {
      sectionInterpoleeToDelete = EditionDelete.findSectionInterpoleeToDelete(sousModele);
    }
    if (CollectionUtils.isNotEmpty(sectionInterpoleeToDelete)) {
      final boolean delete = deleteSectionAccepted(sectionInterpoleeToDelete);
      if (delete) {
        final EditionDelete.RemoveBilan removeBilan = EditionDelete.delete(sectionInterpoleeToDelete, scenario, false);
        LogsDisplayer.displayBilan(removeBilan);
      }
    }
  }

  public static boolean deleteSectionAccepted(List<CatEMHSection> sectionInterpoleeToDelete) {
    Object[] deleteOptions = new Object[]{
      NbBundle.getMessage(UpdateSousModeleFromFolderNodeAction.class, "DeleteSectionInterpoleeAction.OK"),
      NbBundle.getMessage(UpdateSousModeleFromFolderNodeAction.class, "DeleteSectionInterpoleeAction.Ignore"),
    };
    String emhs = ToStringHelper.transformToNom(sectionInterpoleeToDelete);
    emhs = StringUtils.abbreviate(emhs, 50);
    String questionCode = "DeleteSectionInterpoleeAction.Question";
    if (sectionInterpoleeToDelete.size() == 1) {
      questionCode = "DeleteSectionInterpoleeAction.QuestionOne";
    }
    final Object result = DialogHelper.showQuestion(
      NbBundle.getMessage(UpdateSousModeleFromFolderNodeAction.class, "DeleteSectionInterpoleeAction.Title"),
      NbBundle.getMessage(UpdateSousModeleFromFolderNodeAction.class, questionCode, sectionInterpoleeToDelete.size(), emhs),
      deleteOptions
    );
    return deleteOptions[0].equals(result);
  }
}
