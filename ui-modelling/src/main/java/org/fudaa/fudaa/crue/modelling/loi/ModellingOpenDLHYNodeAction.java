package org.fudaa.fudaa.crue.modelling.loi;

import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.fudaa.crue.modelling.action.AbstractModellingOpenTopNodeAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;

/**
 *
 * @author Frédéric Deniger
 */
public class ModellingOpenDLHYNodeAction extends AbstractModellingOpenTopNodeAction<DLHYTopComponent> {

  public ModellingOpenDLHYNodeAction(String name) {
    super(name, DLHYTopComponent.MODE, DLHYTopComponent.TOPCOMPONENT_ID);
  }

  public ModellingOpenDLHYNodeAction() {
    super(NbBundle.getMessage(ModellingOpenDLHYNodeAction.class, "ModellingOpenDLHYNodeAction.DisplayName"), DLHYTopComponent.MODE, DLHYTopComponent.TOPCOMPONENT_ID);
  }

  @Override
  protected void activeWithValue(DLHYTopComponent opened, Node selectedNode) {
    Loi loiToActivate = selectedNode.getLookup().lookup(Loi.class);
    opened.selectLoi(loiToActivate);
  }

  @Override
  protected TopComponent activateNewTopComponent(DLHYTopComponent newTopComponent, Node selectedNode) {
    Loi loiToActivate = selectedNode.getLookup().lookup(Loi.class);
//    DLHYTopComponent top = new DLHYTopComponent();
    if (loiToActivate != null) {
      newTopComponent.putClientProperty(DLHYTopComponent.PROPERTY_INIT_LOI, loiToActivate);
    }
    return newTopComponent;
  }

  public static void open(Loi loi, boolean reopen) {
    ModellingOpenDLHYNodeAction action = null;
    if (reopen) {
      action = SystemAction.get(ModellingOpenDLHYNodeAction.Reopen.class);
    } else {
      action = SystemAction.get(ModellingOpenDLHYNodeAction.NewFrame.class);
    }
    AbstractNode node = new AbstractNode(Children.LEAF, loi == null ? Lookup.EMPTY : Lookups.fixed(loi));
    action.performAction(new Node[]{node});
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return true;
  }

  public static class Reopen extends ModellingOpenDLHYNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenDLHYNodeAction.class, "ModellingOpenDLHYNodeAction.DisplayName"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenDLHYNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenDLHYNodeAction.class, "ModellingOpenDLHYNodeAction.NewFrame.DisplayName"));
      setReopen(false);
    }
  }
}
