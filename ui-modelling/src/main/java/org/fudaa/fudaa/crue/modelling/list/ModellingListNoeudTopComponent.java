/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.list;

import gnu.trove.TLongObjectHashMap;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.edition.bean.ListNoeudContent;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.action.ModellingOpenListNoeudsAddNodeAction;
import org.fudaa.fudaa.crue.modelling.edition.ListModificationProcess;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Liste des noeuds d'un scenario
 */
//@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.modelling//ModellingListNoeudTopComponent//EN",
//                     autostore = false)
@TopComponent.Description(preferredID = ModellingListNoeudTopComponent.TOPCOMPONENT_ID,
    iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png",
    persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ModellingListNoeudTopComponent.MODE, openAtStartup = false, position = 0)
public final class ModellingListNoeudTopComponent extends AbstractModellingListEditionTopComponent implements ExplorerManager.Provider, Lookup.Provider {
  //attention le mode modelling-listNoeuds doit être déclaré dans le projet branding (layer.xml)
  public static final String MODE = "modelling-listNoeuds";
  public static final String TOPCOMPONENT_ID = "ModellingListNoeudTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  private JButton btAddNoeuds;
  private JButton btReorderNoeuds;

  public ModellingListNoeudTopComponent() {
    initComponents();
    createComponent(AbstractListContentNode.getOrdreDisplay());
    setName(NbBundle.getMessage(ModellingListNoeudTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingListNoeudTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueListeNoeuds";
  }

  @Override
  protected void setEditable(final boolean b) {
    super.setEditable(b);
    if (btReorderNoeuds != null) {
      btReorderNoeuds.setEnabled(b);
    }
  }

  private void reorderNodes() {
    final Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    if (nodes.length == 0) {
      return;
    }
    final Map<String, Node> nodeByNoeudName = new HashMap<>();
    for (final Node node : nodes) {
      final CatEMHNoeud noeud = node.getLookup().lookup(CatEMHNoeud.class);
      nodeByNoeudName.put(noeud.getNom(), node);
    }
    getExplorerManager().getRootContext().getChildren().remove(nodes);
    final List<Node> newOrder = new ArrayList<>();
    final List<CatEMHBranche> branches = getSousModele().getBranches();
    for (final CatEMHBranche catEMHBranche : branches) {
      processNoeud(nodeByNoeudName, catEMHBranche.getNoeudAmontNom(), newOrder);
      processNoeud(nodeByNoeudName, catEMHBranche.getNoeudAvalNom(), newOrder);
    }
    final Node[] newNodeOrder = newOrder.toArray(new Node[0]);
    getExplorerManager().getRootContext().getChildren().add(newNodeOrder);
    boolean modified = false;
    for (int i = 0; i < newNodeOrder.length; i++) {
      if (newNodeOrder[i] != nodes[i]) {
        modified = true;
        break;
      }
    }
    if (modified) {
      setModified(true);
    }
  }

  @Override
  protected JPanel buildNorthPanel() {
    assert btAddNoeuds == null;//c'est normalement appele que dans le constructeur:
    btAddNoeuds = new JButton(ModellingOpenListNoeudsAddNodeAction.getAddNodeActionName());
    btAddNoeuds.addActionListener(e -> openAddNodeTopComponent());
    btReorderNoeuds = new JButton(NbBundle.getMessage(ModellingListNoeudTopComponent.class, "OrderNodesWithBranches.Name"));
    btReorderNoeuds.addActionListener(e -> reorderNodes());
    final JPanel top = super.buildNorthPanel();
    final JPanel pnBt = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    pnBt.add(btReorderNoeuds);
    pnBt.add(btAddNoeuds);

    final JPanel res = new JPanel(new BorderLayout());
    res.add(top);
    res.add(pnBt, BorderLayout.SOUTH);
    return res;
  }

  @Override
  protected void installPropertyColumns() {
    outlineView.setPropertyColumns(ListCommonProperties.PROP_NOM,
        ListNoeudContentNode.getNoeudNomDisplay(),
        ListCommonProperties.PROP_TYPE,
        AbstractListContentNode.getTypeDisplay(),
        ListCommonProperties.PROP_COMMENTAIRE,
        AbstractListContentNode.getCommentDisplay());
  }

  private void openAddNodeTopComponent() {
    ModellingOpenListNoeudsAddNodeAction.open(getSousModele());
  }

  @Override
  public void cancelModificationHandler() {
    //on recharge le tout
    scenarioReloaded();
    setModified(false);
  }

  @Override
  public void valideModificationHandler() {
    final Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    final List<CatEMHNoeud> noeuds = new ArrayList<>();
    final TLongObjectHashMap<String> commentaires = new TLongObjectHashMap<>();
    for (final Node node : nodes) {
      final CatEMHNoeud noeud = node.getLookup().lookup(CatEMHNoeud.class);
      final ListNoeudContent content = node.getLookup().lookup(ListNoeudContent.class);
      commentaires.put(noeud.getUiId(), content.getCommentaire());
      noeuds.add(noeud);
    }
    new ListModificationProcess().applyNoeuds((EMHSousModele) getEMHInLookup(), noeuds, commentaires);
    setModified(false);
  }

  @Override
  protected List<? extends Node> createNodes(final EMH in) {
    final ListNoeudContentFactory factory = new ListNoeudContentFactory();
    return factory.createNodes(((EMHSousModele) in).getNoeuds());
  }

  @Override
  protected void scenarioReloaded() {
    scenarioLoaded();
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables

  private void processNoeud(final Map<String, Node> nodeByNoeudName, final String noeudName, final List<Node> newOrder) {
    final Node node = nodeByNoeudName.get(noeudName);
    if (node != null) {
      newOrder.add(node);
      nodeByNoeudName.put(noeudName, null);
    }
  }
}
