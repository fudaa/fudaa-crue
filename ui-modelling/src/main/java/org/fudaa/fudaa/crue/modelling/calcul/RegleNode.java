/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.LogsHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.emh.Regle;
import org.fudaa.dodico.crue.metier.emh.ValParam;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.modelling.node.AbstractModellingNodeFirable;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frédéric Deniger
 */
public class RegleNode extends AbstractModellingNodeFirable {

  public RegleNode(Regle regle, CrueConfigMetier ccm, PerspectiveServiceModelling perspectiveServiceModelling) {
    super(Children.LEAF, Lookups.fixed(regle, ccm), perspectiveServiceModelling);
    setDisplayName(regle.getNom());
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(getLookup().lookup(Regle.class)));
  }

  private class StateUpdater implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if (evt.getPropertyName().equals(Regle.PROP_ACTIVE) || evt.getPropertyName().equals(ValParam.PROP_VALEUR)) {
        updateProperties();
      }
    }
  }
  private StateUpdater updater;

  private void updateProperties() {
    Set set = getSheet().get(Sheet.PROPERTIES);
    updateProperties(set);
  }

  private void updateProperties(Set set) {
    PropertySupportReflection valeur = (PropertySupportReflection) set.get(ValParam.PROP_VALEUR);
    if (valeur != null) {
      try {
        Property<?> activeProperty = set.get(Regle.PROP_ACTIVE);
        Boolean active = (Boolean) activeProperty.getValue();
        valeur.setCanWrite(active.booleanValue());
        ItemVariable value = (ItemVariable) valeur.getValue("validator");
        if (value != null) {
          restoreNoImageAndDefaultDescription();
          CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
          value.getValidator().validateValue(StringUtils.EMPTY, valeur.getValue(), log);
          if (log.isNotEmpty()) {
            if (log.containsErrors()) {
              setErrorIcon();
            } else {
              setWarningIcon();
            }
            setShortDescription(LogsHelper.toHtml(log));
          }
        }
      } catch (Exception exception) {
        Exceptions.printStackTrace(exception);
      }
    }
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    try {
      sheet.put(set);
      Regle regle = getLookup().lookup(Regle.class);
      CrueConfigMetier ccm = getLookup().lookup(CrueConfigMetier.class);
      PropertySupportReflection active = new PropertySupportReflection(this, regle, Boolean.TYPE, Regle.PROP_ACTIVE);
      active.setName(Regle.PROP_ACTIVE);
      active.setDisplayName(BusinessMessages.getString("active.property"));
      set.put(active);

      if (regle.getValParam() != null) {
        PropertySupportReflection valeur = new PropertySupportReflection(this, regle.getValParam(), regle.getValParam().getTypeData(), ValParam.PROP_VALEUR);
        final ItemVariable property = ccm.getProperty(regle.getValParam().getNom());
        if (property != null) {
          if (property.getValidator() != null) {
            valeur.setShortDescription(property.getValidator().getRangeValidate().getDesc());
          }
          valeur.setValue("validator", property);
          if (property.getValidator() != null) {
            String desc = property.getValidator().getHtmlDesc();
            setValue(DEFAULT_SHORT_DESCRIPTION, desc);
            setShortDescription(desc);
          }
        }
        valeur.setName(ValParam.PROP_VALEUR);
        valeur.setDisplayName(BusinessMessages.getString("valParamValeur.property"));
        set.put(valeur);

      }
    } catch (NoSuchMethodException ex) {
      Exceptions.printStackTrace(ex);
    }
    updateProperties(set);
    updater = new StateUpdater();
    addPropertyChangeListener(updater);
    return sheet;
  }
}
