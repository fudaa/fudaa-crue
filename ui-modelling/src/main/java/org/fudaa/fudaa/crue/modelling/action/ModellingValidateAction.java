package org.fudaa.fudaa.crue.modelling.action;

import javax.swing.Action;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioServiceImpl;
import org.fudaa.fudaa.crue.modelling.services.ModellingValidationProcessor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
          id = "org.fudaa.fudaa.crue.modelling.ModellingValidateAction")
@ActionRegistration(displayName = "#CTL_ModellingValidateAction")
@ActionReferences({
  @ActionReference(path = "Actions/Modelling", position = 2, separatorAfter = 3)
})
public final class ModellingValidateAction extends AbstractModellingAction {

  public ModellingValidateAction() {
    super(false);
    putValue(Action.NAME, NbBundle.getMessage(ModellingValidateAction.class, "CTL_ModellingValidateAction"));
  }

  @Override
  protected boolean getEnableState() {
    return super.getEnableState();
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingValidateAction();
  }

  @Override
  public void doAction() {
    final ModellingScenarioServiceImpl serviceImpl = (ModellingScenarioServiceImpl) scenarioService;
    ModellingValidationProcessor processor = new ModellingValidationProcessor(serviceImpl);
    CtuluLogGroup logs = CrueProgressUtils.showProgressDialogAndRun(processor, getActionTitle());
    serviceImpl.updateLastLogsGroup(logs);

  }
}
