/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.services;

import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.config.ConfigDefaultValuesProvider;
import org.fudaa.fudaa.crue.options.config.ConfigEtudeController;
import org.fudaa.fudaa.crue.options.config.CourbeConfig;
import org.fudaa.fudaa.crue.options.config.ModellingGlobalConfiguration;
import org.fudaa.fudaa.crue.options.config.OptionLoiConfiguration;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.lookup.ServiceProvider;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Map;

/**
 * Permet de gérer les modifications de configuration des vues ( loi) dans la perspective modélisation.
 * Stocker les {@link CourbeConfig} utilisé par les id de {@link org.openide.windows.TopComponent}
 * Ne contient pas de lookup.
 *
 * @author Frederic Deniger
 */
@ServiceProvider(service = ModellingConfigService.class)
public final class ModellingConfigService implements LookupListener, ConfigDefaultValuesProvider {
    /**
     * pour écouter les modifications de chargement du scenario courant.
     */
    private final Lookup.Result<EMHScenario> resultat;
    private final ConfigEtudeController configEtudeController = new ConfigEtudeController();
    private final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(ModellingScenarioModificationService.class);
    private final PropertyChangeSupport propertyChangeSupport;

    public ModellingConfigService() {
        resultat = modellingScenarioModificationService.getModellingScenarioService().getLookup().lookupResult(
                EMHScenario.class);
        resultat.addLookupListener(this);
        resultChanged(null);
        propertyChangeSupport = new PropertyChangeSupport(this);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
    }

    @Override
    public void resultChanged(LookupEvent ev) {
        //changement massif: on ne fait rien
        if (modellingScenarioModificationService.getModellingScenarioService().isReloading()) {
            return;
        }
        if (modellingScenarioModificationService.getModellingScenarioService().isScenarioLoaded()) {
            loadScenario();
        } else {
            unloadScenario();
        }
    }

    /**
     *
     * @return la configuration globale
     */
    public ModellingGlobalConfiguration getModellingGlobalConfiguration() {
        return configEtudeController.getModellingGlobalConfiguration();
    }

    public void setModellingGlobalConfiguration(ModellingGlobalConfiguration modellingGlobalConfiguration) {
        configEtudeController.setModellingGlobalConfiguration(modellingGlobalConfiguration);
        modellingScenarioModificationService.setStudyConfigModified();
    }

    /**
     *
     * @return la configuration globale des lois: {@link CourbeConfig} par {@link org.openide.windows.TopComponent}
     */
    public OptionLoiConfiguration getModellingLoiConfiguration() {
        return configEtudeController.getModellingLoiConfiguration();
    }

    /**
     *
     * @param topComponentId id du {@link org.openide.windows.TopComponent}
     * @return la configuration pour le topComponentId (id d'un {@link org.openide.windows.TopComponent})
     */
    public CourbeConfig getModellingLoiConfigurationModified(String topComponentId) {
        OptionLoiConfiguration modellingLoiConfiguration = configEtudeController.getModellingLoiConfiguration();
        if (modellingLoiConfiguration != null) {
            return modellingLoiConfiguration.getCourbeConfig(topComponentId);
        }
        return new CourbeConfig();
    }

    /**
     *
     * @param topComponentId l'id du {@link org.openide.windows.TopComponent}
     * @param config la nouvelle configuration
     */
    public void setModellingLoiConfigurationModified(String topComponentId, CourbeConfig config) {
        OptionLoiConfiguration modellingLoiConfiguration = configEtudeController.getModellingLoiConfiguration();
        if (modellingLoiConfiguration == null) {
            modellingLoiConfiguration = new OptionLoiConfiguration();
            configEtudeController.setModellingLoiConfiguration(modellingLoiConfiguration);
        }
        modellingLoiConfiguration.setCourbeConfig(topComponentId, config);
        propertyChangeSupport.firePropertyChange(topComponentId, Boolean.FALSE, Boolean.TRUE);
        modellingScenarioModificationService.setStudyConfigModified();
    }

    private void loadScenario() {
        configEtudeController.loadConfigEtude(modellingScenarioModificationService.getModellingScenarioService().getSelectedProjet());
    }

    private void unloadScenario() {
        configEtudeController.unload();
    }

    /**
     *
     * @return Map représentant les options globales.
     */
    Map<String, String> getGlobalOptions() {
        return configEtudeController.getGlobalOptions();
    }

    @Override
    public CreationDefaultValue getDefaultValues() {
        return configEtudeController.getModellingGlobalConfiguration().getDefaultValues();
    }
}
