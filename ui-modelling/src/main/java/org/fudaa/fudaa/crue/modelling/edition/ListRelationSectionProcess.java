/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.edition;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.EditionChangeRelationSection;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent;
import org.fudaa.dodico.crue.edition.bean.ValidationMessage;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.list.ListRelationSectionContentNode;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioModificationService;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 *
 * @author Frédéric Deniger
 */
public class ListRelationSectionProcess {

  protected final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
          ModellingScenarioModificationService.class);

  protected LinkedHashMap<ListRelationSectionContent, ListRelationSectionContentNode> getContentList(Node[] newNoeuds) {
    LinkedHashMap<ListRelationSectionContent, ListRelationSectionContentNode> contents = new LinkedHashMap<>();
    for (Node node : newNoeuds) {
      ListRelationSectionContent content = node.getLookup().lookup(ListRelationSectionContent.class);
      if (content != null) {
        contents.put(content, (ListRelationSectionContentNode) node);
      }
    }
    return contents;
  }

  public CtuluLog processModification(Node[] newRelationSection, EMHSousModele emhSousModele, CrueConfigMetier ccm) {
    LinkedHashMap<ListRelationSectionContent, ListRelationSectionContentNode> contents = getContentList(newRelationSection);
    List<ListRelationSectionContent> newContents = new ArrayList<>(contents.keySet());
    final ValidationMessage<ListRelationSectionContent> messages = new EditionChangeRelationSection().apply(newContents, emhSousModele, ccm);
    if (messages.accepted) {
      modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.EMH_RELATION));
    } else {
      for (ListRelationSectionContentNode node : contents.values()) {
        node.setContainError(false);
        node.setShortDescription(null);
      }
      List<ValidationMessage.ResultItem<ListRelationSectionContent>> msgs = messages.messages;
      for (ValidationMessage.ResultItem<ListRelationSectionContent> resultItem : msgs) {
        ListRelationSectionContentNode node = contents.get(resultItem.bean);
        node.setContainError(true);
        node.setShortDescription("<html><body>" + StringUtils.join(resultItem.messages, "<br>") + "</body></html>");
      }
    }
    return messages.log;

  }
}
