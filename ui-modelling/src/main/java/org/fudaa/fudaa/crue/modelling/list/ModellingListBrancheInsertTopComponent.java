package org.fudaa.fudaa.crue.modelling.list;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuGridLayout;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluDoubleParser;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.EditionBrancheInsert;
import org.fudaa.dodico.crue.edition.bean.BrancheInsertContent;
import org.fudaa.dodico.crue.metier.comparator.ToStringInternationalizableComparator;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.validation.util.DelegateValidatorBranche;
import org.fudaa.fudaa.crue.common.editor.DocumentListenerAdapter;
import org.fudaa.fudaa.crue.common.helper.ToStringInternationalizableCellRenderer;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.modelling.AbstractModellingTopComponent;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.openide.windows.TopComponent;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.util.List;

import static org.openide.util.NbBundle.getMessage;

/**
 * POur insérer des  branches.
 */
@TopComponent.Description(preferredID = ModellingListBrancheInsertTopComponent.TOPCOMPONENT_ID, iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png", persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ModellingListBrancheInsertTopComponent.MODE, openAtStartup = false, position = 0)
public final class ModellingListBrancheInsertTopComponent extends AbstractModellingTopComponent {
  //attention le mode modelling-listInsertBranche doit être déclaré dans le projet branding (layer.xml)
  public static final String MODE = "modelling-listInsertBranche";
  public static final String TOPCOMPONENT_ID = "ModellingListBrancheInsertTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  private final JLabel labelNoeudInsertion = new JLabel();
  private final JTextField textFieldNewBrancheName = new JTextField(20);
  private final JLabel labelNewBrancheValid = new JLabel();
  private final JTextField textFieldNewNoeudName = new JTextField(20);
  private final JLabel labelNewNoeudValid = new JLabel();
  private final JFormattedTextField textFieldLongueurBranche = new JFormattedTextField();
  private final JLabel labelLongeurBrancheValid = new JLabel();
  private final JLabel labelTypeBrancheValid = new JLabel();
  private final JComboBox<EnumBrancheType> cbTypeBranche;
  private final JComboBox<String> cbAvalOrAmont = new JComboBox<>(new String[]{
      getMessage(ModellingListBrancheInsertTopComponent.class, "ModellingListBrancheInsertTopComponent.Aval"),
      getMessage(ModellingListBrancheInsertTopComponent.class, "ModellingListBrancheInsertTopComponent.Amont")
  });
  private transient BrancheInsertContent brancheInsertContent;

  public ModellingListBrancheInsertTopComponent() {
    super();
    initComponents();
    textFieldLongueurBranche.setColumns(20);
    final List<EnumBrancheType> types = EnumBrancheType.getAvailablesBrancheType();
    types.sort(ToStringInternationalizableComparator.INSTANCE);
    cbTypeBranche = new JComboBox<>(types.toArray(new EnumBrancheType[0]));
    cbTypeBranche.setRenderer(new ToStringInternationalizableCellRenderer());
    setName(getMessage(ModellingListBrancheInsertTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(getMessage(ModellingListBrancheInsertTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    setBorder(BuBorders.EMPTY5555);
    add(createTopPanel(), BorderLayout.CENTER);
    add(createCancelSaveButtons(), BorderLayout.SOUTH);
  }

  private JPanel createTopPanel() {
    final JPanel top = new JPanel(new BuGridLayout(3, 5, 5, true, false, true, false));
    JLabel jLabel = new JLabel(getMessage(ModellingListBrancheInsertTopComponent.class, "ModellingListBrancheInsertTopComponent.NeoudInsertion"));
    jLabel.setToolTipText(getMessage(ModellingListBrancheInsertTopComponent.class, "ModellingListBrancheInsertTopComponent.NeoudInsertion.Help"));
    top.add(jLabel);
    top.add(labelNoeudInsertion);
    top.add(new JLabel());

    jLabel = new JLabel(getMessage(ModellingListBrancheInsertTopComponent.class, "ModellingListBrancheInsertTopComponent.NewBrancheName"));
    jLabel.setToolTipText(getMessage(ModellingListBrancheInsertTopComponent.class, "ModellingListBrancheInsertTopComponent.NewBrancheName.Help"));
    top.add(jLabel);
    top.add(textFieldNewBrancheName);
    labelNewBrancheValid.setPreferredSize(new Dimension(100, 15));
    top.add(labelNewBrancheValid);

    jLabel = new JLabel(getMessage(ModellingListBrancheInsertTopComponent.class, "ModellingListBrancheInsertTopComponent.NewBrancheType"));
    jLabel.setToolTipText(getMessage(ModellingListBrancheInsertTopComponent.class, "ModellingListBrancheInsertTopComponent.NewBrancheType.Help"));
    top.add(jLabel);
    top.add(cbTypeBranche);
    top.add(labelTypeBrancheValid);

    jLabel = new JLabel(getMessage(ModellingListBrancheInsertTopComponent.class, "ModellingListBrancheInsertTopComponent.NewBrancheDistance"));
    jLabel.setToolTipText(getMessage(ModellingListBrancheInsertTopComponent.class, "ModellingListBrancheInsertTopComponent.NewBrancheDistance.Help"));
    top.add(jLabel);
    top.add(textFieldLongueurBranche);
    top.add(labelLongeurBrancheValid);

    jLabel = new JLabel(getMessage(ModellingListBrancheInsertTopComponent.class, "ModellingListBrancheInsertTopComponent.NewBrancheAmontAval"));
    jLabel.setToolTipText(getMessage(ModellingListBrancheInsertTopComponent.class, "ModellingListBrancheInsertTopComponent.NewBrancheAmontAval.Help"));
    top.add(jLabel);
    top.add(cbAvalOrAmont);
    top.add(new JLabel());

    jLabel = new JLabel(getMessage(ModellingListBrancheInsertTopComponent.class, "ModellingListBrancheInsertTopComponent.NewNoeudName"));
    jLabel.setToolTipText(getMessage(ModellingListBrancheInsertTopComponent.class, "ModellingListBrancheInsertTopComponent.NewNoeudName.Help"));
    top.add(jLabel);
    top.add(textFieldNewNoeudName);
    labelNewBrancheValid.setPreferredSize(new Dimension(100, 15));
    top.add(labelNewNoeudValid);

    textFieldNewBrancheName.getDocument().addDocumentListener(new DocumentListenerAdapter() {
      @Override
      protected void update(final DocumentEvent e) {
        updateStateNewBrancheName();
      }
    });
    textFieldNewNoeudName.getDocument().addDocumentListener(new DocumentListenerAdapter() {
      @Override
      protected void update(final DocumentEvent e) {
        updateStateNewNoeudName();
      }
    });
    textFieldLongueurBranche.getDocument().addDocumentListener(new DocumentListenerAdapter() {
      @Override
      protected void update(final DocumentEvent e) {
        updateLongueurBrancheValid();
      }
    });
    cbTypeBranche.addItemListener(e -> updateLongueurBrancheValid());
    textFieldNewBrancheName.setEditable(false);
    textFieldNewNoeudName.setEditable(false);
    textFieldLongueurBranche.setEditable(false);
    cbTypeBranche.setEnabled(false);
    return top;
  }

  private void updateLongueurBrancheValid() {
    boolean isLongueurEditable = false;
    labelLongeurBrancheValid.setText(StringUtils.EMPTY);
    labelTypeBrancheValid.setIcon(null);
    labelTypeBrancheValid.setText(StringUtils.EMPTY);
    labelLongeurBrancheValid.setIcon(null);
    if (cbTypeBranche.isEnabled()) {
      final EnumBrancheType selectedItem = (EnumBrancheType) cbTypeBranche.getSelectedItem();
      if (selectedItem != null) {
        isLongueurEditable = DelegateValidatorBranche.getBrancheTypeWithDistanceNotNull().contains(selectedItem);
      } else {
        labelTypeBrancheValid.setForeground(Color.RED);
        labelTypeBrancheValid.setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.FATAL));
        labelTypeBrancheValid.setText(getMessage(ModellingListBrancheInsertTopComponent.class, "ModellingListBrancheInsertTopComponent.ChooseABrancheType"));
      }
    } else {
      textFieldLongueurBranche.setEditable(false);
    }
    textFieldLongueurBranche.setEditable(isLongueurEditable);
    labelLongeurBrancheValid.setVisible(isLongueurEditable);
    if (isLongueurEditable) {
      final Double aDouble = CtuluDoubleParser.parseValue(textFieldLongueurBranche.getText());
      if (aDouble == null) {
        labelLongeurBrancheValid.setText(getMessage(ModellingListBrancheInsertTopComponent.class, "ModellingListBrancheInsertTopComponent.LongueurNoValue"));
        labelLongeurBrancheValid.setForeground(Color.RED);
        labelLongeurBrancheValid.setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.FATAL));
      } else {
        final ItemVariable property = getCcm().getProperty(CrueConfigMetierConstants.PROP_XP);
        String validateErrorForValue = property.getValidator().getValidateErrorForValue(aDouble);
        if (validateErrorForValue == null && property.getEpsilon().isZero(aDouble)) {
          validateErrorForValue = BusinessMessages.getString("validation.branche.distanceMustNotBeNull", ((EnumBrancheType) cbTypeBranche.getSelectedItem()).geti18n());
        }
        if (validateErrorForValue != null) {
          labelLongeurBrancheValid.setText(validateErrorForValue);
          labelLongeurBrancheValid.setForeground(Color.RED);
          labelLongeurBrancheValid.setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.FATAL));
        }
      }
    }
    updateValidState();
  }

  private void updateStateNewBrancheName() {
    updateNameState(labelNewBrancheValid, textFieldNewBrancheName, EnumCatEMH.BRANCHE);
  }

  private void updateStateNewNoeudName() {
    updateNameState(labelNewNoeudValid, textFieldNewNoeudName, EnumCatEMH.NOEUD);
  }

  private void updateValidState() {
    if (isModified()) {
      final boolean valid =
          StringUtils.isBlank(labelNewBrancheValid.getText())
              && StringUtils.isBlank(labelNewNoeudValid.getText())
              && StringUtils.isBlank(labelTypeBrancheValid.getText())
              && StringUtils.isBlank(labelLongeurBrancheValid.getText());
      buttonSave.setEnabled(valid);
    }
  }

  private void updateNameState(final JLabel labelDisplayingErrors, final JTextField textField, final EnumCatEMH catEMH) {
    ModellingListBrancheSplitTopComponent.updateNameState(getScenario(), labelDisplayingErrors, textField, catEMH);
    updateValidState();
  }

  public void setNoeud(final CatEMHNoeud noeud) {
    clearData();
    cbAvalOrAmont.setSelectedIndex(0);
    final EditionBrancheInsert splitter = new EditionBrancheInsert(getCcm());
    brancheInsertContent = splitter.createContent(noeud).getResult();
    labelNoeudInsertion.setText(brancheInsertContent.neoudNameToSplitOn);
    textFieldNewNoeudName.setText(brancheInsertContent.newNoeudName);
    textFieldNewBrancheName.setText(brancheInsertContent.newBrancheName);
    textFieldLongueurBranche.setText(StringUtils.EMPTY);
    cbTypeBranche.setSelectedItem(null);
    labelNewNoeudValid.setVisible(true);
    labelNewBrancheValid.setVisible(true);
    labelTypeBrancheValid.setVisible(true);
    updateLongueurBrancheValid();
    textFieldLongueurBranche
        .setFormatterFactory(new DefaultFormatterFactory(new NumberFormatter(getCcm().getFormatter(CrueConfigMetierConstants.PROP_XP, DecimalFormatEpsilonEnum.COMPARISON))));
    updateEditableState();
  }

  private void clearData() {
    brancheInsertContent = null;

    labelNoeudInsertion.setText(StringUtils.EMPTY);
    textFieldNewBrancheName.setText(StringUtils.EMPTY);
    textFieldNewNoeudName.setText(StringUtils.EMPTY);
    textFieldLongueurBranche.setText(StringUtils.EMPTY);

    cbAvalOrAmont.setSelectedIndex(0);
    cbTypeBranche.setSelectedItem(null);

    labelNewBrancheValid.setVisible(false);
    labelNewNoeudValid.setVisible(false);
    labelLongeurBrancheValid.setVisible(false);
    labelTypeBrancheValid.setVisible(false);
  }

  @Override
  protected void scenarioLoaded() {
    //nothing to do
  }

  @Override
  protected void scenarioUnloaded() {
    setModified(false);
    clearData();
    close();
  }

  @Override
  protected void scenarioReloaded() {
    setModified(false);
    clearData();
  }

  @Override
  protected void setEditable(final boolean b) {
    final boolean editable = b && brancheInsertContent != null;
    textFieldNewNoeudName.setEditable(editable);
    textFieldNewBrancheName.setEditable(editable);
    cbTypeBranche.setEnabled(editable);
    if (editable) {
      setModified(true);
    }
    updateLongueurBrancheValid();
  }

  @Override
  public void cancelModificationHandler() {
    clearData();
    setModified(false);
  }

  @Override
  public void valideModificationHandler() {
    if (brancheInsertContent != null) {
      brancheInsertContent.newBrancheName = StringUtils.trim(textFieldNewBrancheName.getText());
      brancheInsertContent.newNoeudName = StringUtils.trim(textFieldNewNoeudName.getText());
      brancheInsertContent.newBrancheType = (EnumBrancheType) cbTypeBranche.getSelectedItem();
      brancheInsertContent.addAval = cbAvalOrAmont.getSelectedIndex() == 0;
      if (textFieldLongueurBranche.isEditable()) {
        brancheInsertContent.distance = CtuluDoubleParser.parseValue(textFieldLongueurBranche.getText());
      }
      final EditionBrancheInsert insertFunction = new EditionBrancheInsert(getCcm());
      final CtuluLog ctuluLog = insertFunction.insert(getScenario(), brancheInsertContent);
      if (ctuluLog.containsErrorOrSevereError()) {
        LogsDisplayer.displayError(ctuluLog, getName());
        return;
      }
      modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.EMH_CREATION, EnumModification.EMH_RELATION));
    }
    clearData();
    setModified(false);
    close();
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueInsertBranche";
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
}
