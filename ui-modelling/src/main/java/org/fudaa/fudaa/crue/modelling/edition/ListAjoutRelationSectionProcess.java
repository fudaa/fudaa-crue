package org.fudaa.fudaa.crue.modelling.edition;

import com.Ostermiller.util.CSVParser;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizableTransformer;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.EditionCreateEMH;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent;
import org.fudaa.dodico.crue.edition.bean.ValidationMessage;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.dodico.crue.validation.util.DelegateValidatorSection;
import org.fudaa.fudaa.crue.modelling.list.AbstractListContentNode;
import org.fudaa.fudaa.crue.modelling.list.ListRelationSectionContentAddNode;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ModellingConfigService;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioModificationService;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.util.*;
import java.util.List;

/**
 * @author deniger
 */
public class ListAjoutRelationSectionProcess {
    protected final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
            ModellingScenarioModificationService.class);
    final ModellingConfigService modellingConfigService = Lookup.getDefault().lookup(ModellingConfigService.class);

    public static List<String> getAvailableBranchesForSections(EMHSousModele sousModele) {
        List<EMHBrancheSaintVenant> branches = sousModele.getBranchesSaintVenant();
        List<String> branchesNoms = TransformerHelper.toNom(branches);
        Collections.sort(branchesNoms);
        CollectionCrueUtil.insertDataAtStart(branchesNoms, StringUtils.EMPTY);
        return branchesNoms;
    }

    public static List<CatEMHSection> getAvailableSectionForSectionIdemBase(final EMHSousModele sousModele) {
        List<EnumSectionType> authorizedSectionType = DelegateValidatorSection.getAuthorizedSectionRefType();
        List<CatEMHSection> sections = sousModele.getSections();
        for (Iterator<CatEMHSection> it = sections.iterator(); it.hasNext(); ) {
            CatEMHSection section = it.next();
            if (!authorizedSectionType.contains(section.getSectionType())) {
                it.remove();
            }
        }
        return sections;
    }

    public static List<String> getAvailableSectionForSectionIdem(EMHSousModele sousModele) {
        List<CatEMHSection> sections = getAvailableSectionForSectionIdemBase(sousModele);
        List<String> sectionsNoms = TransformerHelper.toNom(sections);
        Collections.sort(sectionsNoms);
        CollectionCrueUtil.insertDataAtStart(sectionsNoms, StringUtils.EMPTY);
        return sectionsNoms;
    }

    public static ListRelationSectionContentAddNode.NamesContent getAvailableData(EMHSousModele sousModele) {
        ListRelationSectionContentAddNode.NamesContent res = new ListRelationSectionContentAddNode.NamesContent(
                getAvailableBranchesForSections(sousModele),
                getAvailableSectionForSectionIdem(sousModele));
        return res;
    }

    public static List<ListRelationSectionContentAddNode> createNodes(PerspectiveServiceModelling perspectiveServiceModelling, EMHSousModele sousModele) {
        try {
            String toCopy = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
            String[][] parse = CSVParser.parse(toCopy, '\t');
            List<ListRelationSectionContentAddNode> contents = new ArrayList<>();
            Map<String, EnumSectionType> sectionByName = ToStringInternationalizableTransformer.toi18nMap(EnumSectionType.getAvailablesSectionType());
            ListRelationSectionContentAddNode.NamesContent brancheSectionNames = ListAjoutRelationSectionProcess.getAvailableData(sousModele);
            for (String[] strings : parse) {
                if (strings.length >= 1) {
                    ListRelationSectionContent newContent = new ListRelationSectionContent();
                    newContent.setBrancheNom(strings[0]);
                    if (strings.length >= 2) {
                        newContent.setNom(strings[1]);
                        if (strings.length >= 3) {
                            newContent.getSection().setSectionType(sectionByName.get(strings[2]));
                            if (strings.length >= 4) {
                                newContent.getSection().setReferenceSectionIdem(strings[3]);
                                if (strings.length >= 5) {
                                    try {
                                        newContent.getSection().setDz(Double.parseDouble(strings[4]));
                                    } catch (NumberFormatException numberFormatException) {//rien a faire
                                    }
                                    if (strings.length >= 6) {
                                        try {
                                            newContent.setAbscisseHydraulique(Double.parseDouble(strings[5]));
                                        } catch (NumberFormatException numberFormatException) {//rien a faire
                                        }
                                        if (strings.length >= 7) {
                                            newContent.getSection().setCommentaire(strings[6]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    contents.add(new ListRelationSectionContentAddNode(newContent, perspectiveServiceModelling, brancheSectionNames));
                }
            }
            return contents;
        } catch (Exception exception) {
        }
        return Collections.emptyList();
    }

    /**
     * @param newSections liste de ListNoeudContentAddNode
     * @param sousModele  le sous-moele
     * @return log de l'operation
     */
    public CtuluLog processAjout(Node[] newSections, EMHSousModele sousModele) {
        CrueConfigMetier ccm = modellingScenarioModificationService.getModellingScenarioService().getSelectedProjet().getCoeurConfig().getCrueConfigMetier();
        Map<ListRelationSectionContent, ListRelationSectionContentAddNode> contents = getContentList(newSections);
//        contentNode.setShortDescription("<html><body>" + StringUtils.join(msgs, "<br>") + "</body></html>");
        EditionCreateEMH createProcess = new EditionCreateEMH(modellingConfigService.getDefaultValues());
        final ValidationMessage<ListRelationSectionContent> createSectionsMessages = createProcess.createSections(sousModele, contents.keySet(), ccm);
        Collection<ListRelationSectionContentAddNode> values = contents.values();
        AbstractListContentNode.reinitMessages(values);
        if (createSectionsMessages.accepted) {
            modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.EMH_CREATION));
        } else {
            List<ValidationMessage.ResultItem<ListRelationSectionContent>> msgs = createSectionsMessages.messages;
            for (ValidationMessage.ResultItem<ListRelationSectionContent> resultItem : msgs) {
                ListRelationSectionContentAddNode node = contents.get(resultItem.bean);
                node.setContainError(true);
                node.setShortDescription("<html><body>" + StringUtils.join(resultItem.messages, "<br>") + "</body></html>");
            }
        }
        return createSectionsMessages.log;
    }

    protected LinkedHashMap<ListRelationSectionContent, ListRelationSectionContentAddNode> getContentList(Node[] newNoeuds) {
        LinkedHashMap<ListRelationSectionContent, ListRelationSectionContentAddNode> contents = new LinkedHashMap<>();
        for (Node node : newNoeuds) {
            ListRelationSectionContent content = node.getLookup().lookup(ListRelationSectionContent.class);
            if (content != null) {
                contents.put(content, (ListRelationSectionContentAddNode) node);
            }
        }
        return contents;
    }
}
