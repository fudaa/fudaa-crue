/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.list;

import com.memoire.bu.BuGridLayout;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.fudaa.crue.common.view.NodeEditionHelper;
import org.fudaa.fudaa.crue.modelling.list.ListNodeAddPopupFactory.AddEMHFromClipboard;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.nodes.*;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.util.List;

/**
 * Classe abstraite pour les vues chargées d'ajouter des EMHs.
 */
public abstract class AbstractModellingListAddTopComponent extends AbstractModellingListTopComponent {

  JButton btAddEMH;

  public AbstractModellingListAddTopComponent() {
    initComponents();
    createComponent(StringUtils.EMPTY);
    initActionComponents();
    outlineView.getOutline().setToolTipText(StringUtils.EMPTY);
    outlineView.setNodePopupFactory(new ListNodeAddPopupFactory(this));
  }

  protected boolean shouldSaveBeforeSwitchingSousModele(){
    return false;
  }

  protected final void clearView() {
    explorerHelper.scenarioUnloaded();
    outlineView.getOutline().repaint(0);
  }

  @Override
  public void componentActivated() {
    super.componentActivated();
    ExplorerUtils.activateActions(getExplorerManager(), true);
  }

  @Override
  protected void componentDeactivated() {
    super.componentDeactivated();
    ExplorerUtils.activateActions(getExplorerManager(), false);
  }

  protected JButton createAdditionnalAddButton() {
    return null;
  }

  protected final void initActionComponents() {
    btAddEMH = new JButton(NbBundle.getMessage(AbstractModellingListAddTopComponent.class, "Add.Name"));
    btAddEMH.addActionListener(e -> addEMHs());
    btAddEMH.setEnabled(false);
    final Action actionDelete = ExplorerUtils.actionDelete(getExplorerManager(), false);
    setFocusable(true);
    outlineView.setFocusable(true);
    outlineView.getOutline().setFocusable(true);
    final ActionMap map = getActionMap();
    final InputMap keys = getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    associateLookup(ExplorerUtils.createLookup(getExplorerManager(), map));
    keys.put(ListNodeAddPopupFactory.getDeleteKeyStroke(), "delete");
    keys.put(AddEMHFromClipboard.getKeyStroke(), "paste");
    map.put("paste", new AddEMHFromClipboard(this)); //NOI18N
    map.put("delete", actionDelete); //NOI18N

    final JPanel addRemove = new JPanel(new BuGridLayout(1));
    addRemove.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 3));
    addRemove.add(btAddEMH);
    final JButton createAdditionnalAddButton = createAdditionnalAddButton();
    if (createAdditionnalAddButton != null) {
      addRemove.add(createAdditionnalAddButton);
    }
    addRemove.add(new JSeparator());
    final JButton jButtonDelete = new JButton(actionDelete);
    jButtonDelete.setText(NbBundle.getMessage(AbstractModellingListAddTopComponent.class, "Delete.Name"));
    addRemove.add(jButtonDelete);
    add(addRemove, BorderLayout.EAST);
  }

  @Override
  public final void cancelModificationHandler() {
    explorerHelper.cleanListener();
    clearView();
    setModified(false);
  }

  protected abstract void addEMHsFromClipboard();

  protected abstract void addEMH();

  protected final void prepareRootContext() {
    if (getExplorerManager().getRootContext() == Node.EMPTY) {
      final Children.Array children = new Children.Array();
      final AbstractNode newRoot = new AbstractNode(children);
      getExplorerManager().setRootContext(newRoot);

      newRoot.addNodeListener(addRemoveListener);
      setModified(true);
    }
  }

  protected class NodeAddRemoveChangedListener implements NodeListener {

    @Override
    public void propertyChange(final PropertyChangeEvent evt) {
    }

    @Override
    public void childrenAdded(final NodeMemberEvent ev) {
      setModified(true);
    }

    @Override
    public void childrenRemoved(final NodeMemberEvent ev) {
      setModified(getExplorerManager().getRootContext().getChildren().getNodesCount() > 0);
    }

    @Override
    public void childrenReordered(final NodeReorderEvent ev) {
    }

    @Override
    public void nodeDestroyed(final NodeEvent ev) {
      setModified(getExplorerManager().getRootContext().getChildren().getNodesCount() > 0);
    }
  }
  final NodeAddRemoveChangedListener addRemoveListener = new NodeAddRemoveChangedListener();

  public void addEMHs() {
    prepareRootContext();
    addEMH();
  }

  @Override
  protected  void setEditable(final boolean b) {
    super.setEditable(b);
    btAddEMH.setEnabled(b);
  }


  @Override
  protected final void createComponent(final String firstColumnName) {
    super.createComponent(firstColumnName);

    outlineView.getOutline().getColumnModel().getColumn(0).setWidth(35);
    outlineView.getOutline().getColumnModel().getColumn(0).setMaxWidth(45);
    outlineView.getOutline().getColumnModel().getColumn(0).setPreferredWidth(35);
  }
  private boolean isValidating;

  @Override
  public final void valideModificationHandler() {
    isValidating = true;
    doValidationModification();
    isValidating = false;
  }

  protected abstract void doValidationModification();

  @Override
  protected final void scenarioLoaded() {
    if (!isValidating) {
      assert getExplorerManager().getRootContext() == Node.EMPTY;
      updateSousModeleNameInUI(getModellingService().getScenarioLoaded());
    }
  }

  protected void addNodesFromClipboard(final List<? extends Node> contents) {
    if (!isEditable()) {
      return;
    }
    if (!contents.isEmpty()) {
      prepareRootContext();
      final ExplorerManager explorerManager = getExplorerManager();
      NodeEditionHelper.addNodesFromClipboard(explorerManager, contents);

    }
  }

  @Override
  protected final void scenarioReloaded() {
    scenarioLoaded();
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
