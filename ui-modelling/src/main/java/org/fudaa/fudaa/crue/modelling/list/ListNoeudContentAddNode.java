package org.fudaa.fudaa.crue.modelling.list;

import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.edition.bean.ListNoeudContent;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class ListNoeudContentAddNode extends AbstractListContentNode {

  public ListNoeudContentAddNode(ListNoeudContent content,
          PerspectiveServiceModelling perspectiveServiceModelling) {
    super(Children.LEAF, Lookups.singleton(content), perspectiveServiceModelling);

  }

  @Override
  public boolean canDestroy() {
    return super.isEditMode();
  }

  

  @Override
  protected Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    ListNoeudContent noeudContent = getLookup().lookup(ListNoeudContent.class);
    try {
      Node.Property nom = PropertySupportReflection.createString(this, noeudContent, ListCommonProperties.PROP_NOM,
              ListCommonProperties.PROP_NOM,
              ListNoeudContentNode.getNoeudNomDisplay());
      set.put(nom);
      Node.Property commentaire = new PropertySupportReflection(this, noeudContent, String.class,
              ListCommonProperties.PROP_COMMENTAIRE);
      PropertyCrueUtils.setNullValueEmpty(commentaire);
      commentaire.setName(ListCommonProperties.PROP_COMMENTAIRE);
      commentaire.setDisplayName(AbstractListContentNode.getCommentDisplay());
      set.put(commentaire);
    } catch (NoSuchMethodException ex) {
      Exceptions.printStackTrace(ex);
    }
    sheet.put(set);
    return sheet;
  }
}
