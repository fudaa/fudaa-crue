/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.helper.ChooseNameView;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 * @author Frederic Deniger
 */
public class CalculRenameNodeAction extends AbstractEditNodeAction {
  public CalculRenameNodeAction() {
    super(org.openide.util.NbBundle.getMessage(CalculRenameNodeAction.class, "Rename.ActionName"));
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    return activatedNodes.length == 1;
  }

  @Override
  protected void performAction(final Node[] activatedNodes) {
    final Node node = activatedNodes[0];
    final CalculNode targetNode = (CalculNode) node;
    final String name = targetNode.getName();
    final String prefix = targetNode.getCalc().isPermanent() ? CruePrefix.P_CALCUL_PSEUDOPERMANENT : CruePrefix.P_CALCUL_TRANSITOIRE;
    final String dialogTitle = NbBundle.getMessage(CalculRenameNodeAction.class, "CalculNodeRename.DialogTitle", targetNode.getName());
    final String newName = chooseName(prefix, name, targetNode.getParentNode(), node, dialogTitle);
    if (newName != null) {
      targetNode.rename(newName);
    }
  }

  /**
   * @param prefixExpected soit Cc_P, soit CC_T
   * @param initName le nom inital. Si vide, le prefix sera utilise
   * @return le nom choisi
   */
  public static String chooseName(final String prefixExpected, final String initName, final Node rootNode, final Node toAvoid, final String dialogTitle) {
    final ChooseNameView view = new ChooseNameView();
    view.setDialogTitle(dialogTitle);
    final Node[] nodes = rootNode.getChildren().getNodes();
    for (final Node otherNode : nodes) {
      if (otherNode != toAvoid) {
        view.addUsedName(otherNode.getName());
      }
    }
    return view.chooseName(prefixExpected, StringUtils.isBlank(initName) ? prefixExpected : initName);
  }
}
