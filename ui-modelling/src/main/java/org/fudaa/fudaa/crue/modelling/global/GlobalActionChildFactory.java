package org.fudaa.fudaa.crue.modelling.global;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.common.node.NodeChildrenHelper;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

/**
 *
 * @author deniger
 */
public class GlobalActionChildFactory {

  public static Node createModeleNode(EMHModeleBase modele) {
    List<Node> child = new ArrayList<>();
    List<EMHSousModele> sousModeles = modele.getSousModeles();
    for (EMHSousModele eMHSousModele : sousModeles) {
      child.add(new GlobalContainerNode.EMHSousModeleNode(Children.LEAF, eMHSousModele));
    }
    return new GlobalContainerNode.EMHModeleNode(NodeChildrenHelper.createChildren(child), modele);
  }

  public static Node createGlobalContainerNode(EMHScenario scenario) {
    GlobalContainerNode scenarioNode = createScenarioNode(scenario);
    List<Node> main = new ArrayList<>();
    main.add(scenarioNode);
    AbstractNode mainNode = new AbstractNode(NodeChildrenHelper.createChildren(main));
    return mainNode;
  }

  private static GlobalContainerNode createScenarioNode(EMHScenario scenario) {
    List<Node> child = new ArrayList<>();
    List<EMHModeleBase> modeles = scenario.getModeles();
    for (EMHModeleBase eMHModeleBase : modeles) {
      child.add(createModeleNode(eMHModeleBase));
    }
    GlobalContainerNode scenarioNode = new GlobalContainerNode.EMHScenarioNode(NodeChildrenHelper.createChildren(child), scenario);
    return scenarioNode;
  }
}
