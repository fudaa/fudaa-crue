package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.list.AbstractModellingListTopComponent;
import org.openide.nodes.Node;
import org.openide.windows.TopComponent;

/**
 * @author deniger
 */
public abstract class AbstractModellingOpenListTopNodeAction<T extends AbstractModellingListTopComponent> extends AbstractModellingOpenTopNodeAction<T> {
  public AbstractModellingOpenListTopNodeAction(String name, String defaultMode, String topComponentId) {
    super(name, defaultMode, topComponentId);
  }

  @Override
  protected final boolean isEnable(Node[] activatedNodes) {
    return activatedNodes.length == 1 && activatedNodes[0].getLookup().lookup(EMHSousModele.class) != null;
  }

  @Override
  protected void activeWithValue(T opened, Node selectedNode) {
    EMHSousModele sousModele = selectedNode.getLookup().lookup(EMHSousModele.class);
    opened.reloadWithSousModeleUid(sousModele.getUiId());
  }

  @Override
  protected TopComponent activateNewTopComponent(T create, Node selectedNode) {
    EMHSousModele sousModele = selectedNode.getLookup().lookup(EMHSousModele.class);
    create.setSousModeleUid(sousModele.getUiId());
    return create;
  }
}
