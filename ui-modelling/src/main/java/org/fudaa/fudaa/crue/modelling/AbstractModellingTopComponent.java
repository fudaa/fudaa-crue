package org.fudaa.fudaa.crue.modelling;

import com.memoire.bu.BuBorders;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.AbstractTopComponent;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.UserSaveAnswer;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.modelling.action.ModellingGlobalFindAction;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioModificationService;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.windows.WindowManager;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeListener;
import java.util.Observable;
import java.util.Observer;

/**
 * @author deniger
 */
public abstract class AbstractModellingTopComponent extends AbstractTopComponent implements LookupListener {
  public static final String END_NAME_IF_MODIFIED = " *";
  protected final PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
  protected final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
      ModellingScenarioModificationService.class);
  protected boolean updating;//true si les donnees UI sont en train d'être modifiee
  boolean modified;
  ModificationObserver modificationObserver;
  Lookup.Result<EMHScenario> resultat;
  final FocusListener focusListener = new FocusListener() {
    @Override
    public void focusGained(final FocusEvent e) {
      updateEditableState();
    }

    @Override
    public void focusLost(final FocusEvent e) {
    }
  };
  final PropertyChangeListener stateListener = evt -> perspectiveStateChanged();

  protected AbstractModellingTopComponent() {
    setFocusable(true);
    ModellingGlobalFindAction.installAction(this);
    setBorder(BuBorders.EMPTY0505);
  }

  public boolean isUpdating() {
    return updating;
  }

  protected DocumentListener createDocumentListener() {
    return new DocumentListener() {
      @Override
      public void insertUpdate(final DocumentEvent e) {
        setModified(true);
      }

      @Override
      public void removeUpdate(final DocumentEvent e) {
        setModified(true);
      }

      @Override
      public void changedUpdate(final DocumentEvent e) {
        setModified(true);
      }
    };
  }

  public CrueConfigMetier getCcm() {
    return modellingScenarioModificationService.getModellingScenarioService().getSelectedProjet().getCoeurConfig().getCrueConfigMetier();
  }

  public CoeurConfigContrat getCoeurConfig() {
    return modellingScenarioModificationService.getModellingScenarioService().getSelectedProjet().getCoeurConfig();
  }

  protected EMHScenario getScenario() {
    return getModellingService().getScenarioLoaded();
  }

  protected void updateTopComponentName() {
    final String name = getName();
    if (modified && !name.endsWith(END_NAME_IF_MODIFIED)) {
      setName(name + END_NAME_IF_MODIFIED);
    } else if (!modified && name.endsWith(END_NAME_IF_MODIFIED)) {
      setName(StringUtils.removeEnd(name, END_NAME_IF_MODIFIED));
    }
  }

  @Override
  public void setName(final String name) {
    if (EventQueue.isDispatchThread()) {
      super.setName(name);
    } else {
      EventQueue.invokeLater(() -> AbstractModellingTopComponent.this.setName(name));
    }
  }

  protected abstract String getViewHelpCtxId();

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId(getViewHelpCtxId(), PerspectiveEnum.MODELLING);
  }

  /**
   * A redefinir pour ne modifier les données que si nécessaire.
   *
   * @param event l'event
   */
  protected void scenarioChanged(final ScenarioModificationEvent event) {
    scenarioReloaded();
  }

  protected ModellingScenarioService getModellingService() {
    return modellingScenarioModificationService.getModellingScenarioService();
  }

  protected JPanel createCancelSaveButtons() {
    return createCancelSaveButtons(null);
  }

  protected abstract void scenarioLoaded();

  protected abstract void scenarioUnloaded();

  protected abstract void scenarioReloaded();

  @Override
  public boolean isModified() {
    return modified;
  }

  public void setModified(final boolean modified) {
    if (updating) {
      return;
    }
    this.modified = modified;
    if (modified) {
      if (perspectiveServiceModelling != null) {//pour les tests uniquement
        perspectiveServiceModelling.setDirty(true);
      }
    }
    updateTopComponentName();
    if (buttonSave != null) {
      buttonSave.setEnabled(modified);
    }
    if (buttonCancel != null) {
      buttonCancel.setEnabled(modified);
    }
  }

  @Override
  public final void resultChanged(final LookupEvent ev) {
    if (getModellingService().isReloading()) {
      if (getModellingService().isScenarioLoaded()) {
        scenarioReloaded();
      }
    } else if (getModellingService().isScenarioLoaded()) {
      modificationObserver = new ModificationObserver();
      modellingScenarioModificationService.addObserver(modificationObserver);
      scenarioLoaded();
    } else {
      cleanTopComponent();
    }
  }

  private void cleanTopComponent() {
    scenarioUnloaded();
    if (modificationObserver != null) {
      modellingScenarioModificationService.deleteObserver(modificationObserver);
      modificationObserver = null;
    }
  }

  protected void updateEditableState() {
    setEditable(
        perspectiveServiceModelling != null && WindowManager.getDefault().getRegistry().getActivated() == this && perspectiveServiceModelling.
            isInEditMode());
  }

  @Override
  public final void componentClosedDefinitly() {
    if (resultat != null) {
      resultat.removeLookupListener(this);
      resultat = null;
    }
    writePreferences();
    cleanTopComponent();
    removeFocusListener(focusListener);
    componentClosedDefinitlyHandler();
  }

  protected void componentClosedDefinitlyHandler() {
  }

  protected void readPreferences() {
  }

  protected void writePreferences() {
  }

  @Override
  public void componentClosedTemporarily() {
  }

  abstract protected void setEditable(boolean b);

  @Override
  public boolean canClose() {
    if (isModified()) {
      final UserSaveAnswer askForSave = askForSave();
      if (UserSaveAnswer.CANCEL.equals(askForSave)) {
        return false;
      }
    }
    return super.canClose();
  }

  @Override
  protected final void componentOpened() {
    addFocusListener(focusListener);
    super.componentOpened();
    if (resultat == null) {
      resultat = getModellingService().getLookup().lookupResult(EMHScenario.class);
      resultat.addLookupListener(this);
      resultChanged(null);
    }
    componentOpenedHandler();
    readPreferences();
  }

  protected void componentOpenedHandler() {
  }

  protected void perspectiveStateChanged() {
    forceSave();
    updateEditableState();
  }

  @Override
  public void cancelModification() {
    cancelModificationHandler();
    perspectiveServiceModelling.setDirty(modellingScenarioModificationService.isModified());
  }

  @Override
  public boolean valideModification() {
    valideModificationHandler();
    perspectiveServiceModelling.setDirty(modellingScenarioModificationService.isModified());
    return true;
  }

  public abstract void cancelModificationHandler();

  public abstract void valideModificationHandler();

  @Override
  protected void componentDeactivated() {
    if (perspectiveServiceModelling == null) {
      return;
    }
    perspectiveServiceModelling.removeStateListener(stateListener);
    final UserSaveAnswer confirmSaveOrNot = askForSave();

    if (UserSaveAnswer.CANCEL.equals(confirmSaveOrNot)) {
      return;
    }
    setEditable(false);

    super.componentDeactivated();
  }

  @Override
  protected void componentActivated() {
    if (perspectiveServiceModelling == null) {
      return;
    }
    perspectiveServiceModelling.addStateListener(stateListener);
    updateEditableState();
    super.componentActivated();
  }

  private class ModificationObserver implements Observer {
    @Override
    public void update(final Observable o, final Object arg) {
      scenarioChanged((ScenarioModificationEvent) arg);
    }
  }
}
