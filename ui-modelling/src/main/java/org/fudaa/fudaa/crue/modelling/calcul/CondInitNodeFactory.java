/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.metier.helper.PropertyFinder;
import org.fudaa.dodico.crue.metier.helper.PropertyFinder.Description;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.modelling.calcul.importer.CiniParserAndImporter;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Construit des noms en clonant les dpti: un seul dpti cloné par EMH.
 *
 * @author Frédéric Deniger
 */
public class CondInitNodeFactory {
  final PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
  private final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);

  public List<Node> build(final EMHModeleBase modele) {
    final EMHSousModele sousModele = EMHHelper.concatInNotSorted(null, modele.getSousModeles());
    final EnumMethodeInterpol methodeInterpol = modele.getOrdPrtCIniModeleBase().getMethodeInterpol();
    final CrueConfigMetier ccm = modellingScenarioService.getSelectedProjet().getCoeurConfig().getCrueConfigMetier();
    final List<Node> nodes = new ArrayList<>();
    if (methodeInterpol.isNoeudActiveRequireDPTI()) {
      nodes.add(buildNoeud(methodeInterpol, sousModele, ccm));
    }
    if (methodeInterpol.isBrancheActiveRequireDPTI()) {
      nodes.add(buildBranche(sousModele, ccm));
    }
    if (methodeInterpol.isSectionActiveRequireDPTI()) {
      nodes.add(buildSection(sousModele, ccm));
    }
    if (methodeInterpol.isCasierActiveRequireDPTI()) {
      nodes.add(buildCasier(sousModele, ccm));
    }
    return nodes;
  }

  private Node buildNoeud(final EnumMethodeInterpol methodeInterpol, final EMHSousModele sousModele, final CrueConfigMetier ccm) {
    final List<Node> nodes = createNodes(sousModele.getNoeuds(), ccm);
    if (EnumMethodeInterpol.BAIGNOIRE.equals(methodeInterpol) && !nodes.isEmpty()) {
      final CondInitNode first = (CondInitNode) nodes.get(0);
      final PropertySupportReflection main = (PropertySupportReflection) first.find(CiniParserAndImporter.PROP_VALUE);
      final List<PropertySupportReflection> properties = new ArrayList<>();
      for (int i = 1; i < nodes.size(); i++) {
        final PropertySupportReflection property = (PropertySupportReflection) ((CondInitNode) nodes.get(i)).find(CiniParserAndImporter.PROP_VALUE);
        properties.add(property);
        try {
          property.setValue(main.getValue());
        } catch (final Exception exception) {
          Exceptions.printStackTrace(exception);
        }
        property.setCanWrite(false);
      }
      first.addPropertyChangeListener(new PropagateChangeListener(properties, main));
    }

    return NodeHelper.createNode(nodes, NbBundle.getMessage(CondInitNodeFactory.class, "CiniNoeuds.DisplayName"));
  }

  private void addNodes(final Map<Class, List<Description>> cache, final DonPrtCIni cini, final PropertyFinder finder, final EMH emh, final CrueConfigMetier ccm, final List<Node> nodes) {
    List<Description> descriptions = cache.get(cini.getClass());
    if (descriptions == null) {
      descriptions = finder.getAvailablePropertiesSorted(cini.getClass());
      cache.put(cini.getClass(), descriptions);
    }
    for (final Description description : descriptions) {
      nodes.add(createNode(emh, cini, description, ccm));
    }
  }

  private final HashMap<String, DonPrtCIni> clonedByEmhName = new HashMap<>();

  private CondInitNode createNode(final EMH emh, final DonPrtCIni cini, final Description description, final CrueConfigMetier ccm) {
    DonPrtCIni clonedCini = clonedByEmhName.get(emh.getNom());
    if (clonedCini == null) {
      clonedCini = cini.deepClone();
      clonedByEmhName.put(emh.getNom(), clonedCini);
    }
    return new CondInitNode(emh, clonedCini, description.property, ccm, perspectiveServiceModelling);
  }

  private Node buildBranche(final EMHSousModele sousModele, final CrueConfigMetier ccm) {
    final List<Node> nodes = createNodes(sousModele.getBranches(), ccm);
    return NodeHelper.createNode(nodes, NbBundle.getMessage(CondInitNodeFactory.class, "CiniBranches.DisplayName"));
  }

  private Node buildSection(final EMHSousModele sousModele, final CrueConfigMetier ccm) {
    final List<Node> nodes = createNodes(sousModele.getSections(), ccm);
    return NodeHelper.createNode(nodes, NbBundle.getMessage(CondInitNodeFactory.class, "CiniSections.DisplayName"));
  }

  private Node buildCasier(final EMHSousModele sousModele, final CrueConfigMetier ccm) {
    final List<Node> nodes = createNodes(sousModele.getCasiers(), ccm);
    return NodeHelper.createNode(nodes, NbBundle.getMessage(CondInitNodeFactory.class, "CiniCasiers.DisplayName"));
  }

  private List<Node> createNodes(final List<? extends EMH> emhs, final CrueConfigMetier ccm) {
    final PropertyFinder finder = new PropertyFinder();
    final Map<Class, List<Description>> cache = new HashMap<>();
    final List<Node> nodes = new ArrayList<>();
    for (final EMH emh : emhs) {
      final List<DonPrtCIni> dpti = emh.getDPTI();
      if (CollectionUtils.isNotEmpty(dpti)) {
        assert dpti.size() == 1;
        final DonPrtCIni cini = dpti.get(0);
        addNodes(cache, cini, finder, emh, ccm, nodes);
      }
    }
    return nodes;
  }

  private static class PropagateChangeListener implements PropertyChangeListener {
    private final List<PropertySupportReflection> properties;
    private final PropertySupportReflection main;

    public PropagateChangeListener(final List<PropertySupportReflection> properties, final PropertySupportReflection main) {
      this.properties = properties;
      this.main = main;
    }

    @Override
    public void propertyChange(final PropertyChangeEvent evt) {
      if (CiniParserAndImporter.PROP_VALUE.equals(evt.getPropertyName())) {
        for (final PropertySupportReflection propertySupportReflection : properties) {
          try {
            propertySupportReflection.setCanWrite(true);
            propertySupportReflection.setValue(main.getValue());
            propertySupportReflection.setCanWrite(false);
          } catch (final Exception exception) {
            Exceptions.printStackTrace(exception);
          }
        }
      }
    }
  }
}
