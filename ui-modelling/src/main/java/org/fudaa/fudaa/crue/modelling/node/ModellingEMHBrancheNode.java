package org.fudaa.fudaa.crue.modelling.node;

import javax.swing.Action;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.fudaa.crue.modelling.action.ChangeSousModeleParentNodeAction;
import org.fudaa.fudaa.crue.modelling.action.EnableEMHNodeAction;
import org.fudaa.fudaa.crue.modelling.action.DisableEMHNodeAction;
import org.fudaa.fudaa.crue.modelling.action.DeleteEMHCascadeNodeAction;
import org.fudaa.fudaa.crue.modelling.action.DeleteEMHUniqueNodeAction;
import org.fudaa.fudaa.crue.modelling.action.ModellingOpenListFrottementsOnBrancheNodeAction;
import org.fudaa.fudaa.crue.modelling.emh.ModellingOpenEMHNodeAction;
import org.openide.nodes.Children;
import org.openide.util.actions.SystemAction;

/**
 *
 * @author deniger
 */
public class ModellingEMHBrancheNode extends ModellingEMHNode {

  public ModellingEMHBrancheNode(Children children, EMH emh) {
    super(children, emh);
  }

  public ModellingEMHBrancheNode(Children children, EMH emh, String displayName) {
    super(children, emh, displayName);
  }

  @Override
  public Action getPreferredAction() {
    return SystemAction.get(ModellingOpenEMHNodeAction.Reopen.class);
  }

  @Override
  public Action[] getActions(boolean context) {
    return new Action[]{
              SystemAction.get(ModellingOpenEMHNodeAction.Reopen.class),
              SystemAction.get(ModellingOpenEMHNodeAction.NewFrame.class),
              null,
              SystemAction.get(ModellingOpenListFrottementsOnBrancheNodeAction.Reopen.class),
              SystemAction.get(ModellingOpenListFrottementsOnBrancheNodeAction.NewFrame.class),
              null,
              SystemAction.get(ChangeSousModeleParentNodeAction.class),
              null,
              SystemAction.get(EnableEMHNodeAction.class),
              SystemAction.get(DisableEMHNodeAction.class),
              null,
              SystemAction.get(DeleteEMHUniqueNodeAction.class),
              SystemAction.get(DeleteEMHCascadeNodeAction.class)};
  }
}
