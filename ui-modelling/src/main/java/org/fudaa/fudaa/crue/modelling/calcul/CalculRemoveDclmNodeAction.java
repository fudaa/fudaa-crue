/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import java.io.IOException;

import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 * @author Frederic Deniger
 */
public class CalculRemoveDclmNodeAction extends AbstractEditNodeAction {

  public CalculRemoveDclmNodeAction() {
    super(NbBundle.getMessage(CalculRemoveDclmNodeAction.class, "RemoveDclmAction.Name"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return true;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    try {
      for (Node node : activatedNodes) {
        node.destroy();
      }

    } catch (IOException ex) {
      Exceptions.printStackTrace(ex);
    }
  }
}
