/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.services;

import org.fudaa.fudaa.crue.common.services.AbstractSelectedEMHService;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 * conserve les UID des EMH sélectionnées. Pour les lookup voir {@link org.fudaa.fudaa.crue.common.services.AbstractSelectedEMHService}.
 *
 * @see org.fudaa.fudaa.crue.common.services.AbstractSelectedEMHService
 *
 * @author Frederic Deniger
 */
@ServiceProvider(service = ModellingSelectedEMHService.class)
public class ModellingSelectedEMHService extends AbstractSelectedEMHService implements Lookup.Provider {

  /**
   *
   * @return le lookup du service
   */
  @Override
  public Lookup getLookup() {
    return super.getLookup();
  }
}
