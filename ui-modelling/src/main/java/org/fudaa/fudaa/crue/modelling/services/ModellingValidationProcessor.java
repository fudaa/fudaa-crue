package org.fudaa.fudaa.crue.modelling.services;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Exceptions;

/**
 *
 * @author deniger
 */
public class ModellingValidationProcessor implements ProgressRunnable<CtuluLogGroup> {

  private final ModellingScenarioServiceImpl service;

  public ModellingValidationProcessor(ModellingScenarioServiceImpl service) {
    this.service = service;
  }

  @Override
  public CtuluLogGroup run(ProgressHandle handle) {
    if (handle != null) {
      handle.switchToIndeterminate();
    }
    ModellingValidXmlCallable xmlProcess = new ModellingValidXmlCallable(service);
    ModellingValidCallable validationProcess = new ModellingValidCallable(service);
    //on utilise 2 threads:
    ExecutorService newCachedThreadPool = Executors.newFixedThreadPool(2);
    CtuluLogGroup finalGroup = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    try {
      List<Future<CtuluLogGroup>> invokeAll = newCachedThreadPool.invokeAll(Arrays.asList(validationProcess, xmlProcess));
      for (Future<CtuluLogGroup> futureGroup : invokeAll) {
        CtuluLogGroup log = futureGroup.get();
        if (log != null && log.containsSomething()) {
          finalGroup.addGroup(log);

        }
      }
    } catch (Exception exception) {
      Exceptions.printStackTrace(exception);
    } finally {
      newCachedThreadPool.shutdownNow();
    }
    if (finalGroup.getGroups().size() == 1) {
      finalGroup = finalGroup.getGroups().get(0);
    }
    finalGroup.setDescription("common.validation");
    finalGroup.setDescriptionArgs(service.getScenarioLoaded().getNom());

    if (handle != null) {
      handle.finish();
    }
    return finalGroup;


  }
}
