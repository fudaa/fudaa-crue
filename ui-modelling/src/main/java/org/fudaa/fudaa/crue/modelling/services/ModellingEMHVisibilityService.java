package org.fudaa.fudaa.crue.modelling.services;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.common.ContainerActivityVisibility;
import org.openide.util.*;
import org.openide.util.Lookup.Result;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ServiceProvider;

/**
 * Service gérant la visibilité des sous-modèles et modèles EMH ainsi que le sous-modèle actif.
 * <table  border="1"  cellpadding="3" cellspacing="0"  summary="lookups">
 * <tr>
 * <th align="left">Lookup</th>
 * <th align="left">Commentaire</th>
 * <th align="left">Méthode utilisant le lookup</th>
 * </tr>
 * <tr>
 * <td>{@link ContainerActivityVisibility}</td>
 * <td>	L'état d'affichage (visible/pas visible) des EMHS et donne le sous-modèle actif</td>
 * <td>
 * <code>{@link #getContainerActivityVisibility()} }</code>
 * <code>{@link #activityChanged(ContainerActivityVisibility)} </code>
 * </td>
 * </tr>
 * </table>
 *
 * @author deniger
 */
@ServiceProvider(service = ModellingEMHVisibilityService.class)
public class ModellingEMHVisibilityService implements LookupListener, Lookup.Provider {
    @SuppressWarnings("FieldCanBeLocal")
    private final Result<EMHScenario> resultat;
    protected final InstanceContent dynamicContent = new InstanceContent();
    protected final Lookup lookup = new AbstractLookup(dynamicContent);
    final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);

    private boolean reloading;

    public ModellingEMHVisibilityService() {
        resultat = modellingScenarioService.getLookup().lookupResult(
                EMHScenario.class);
        resultat.addLookupListener(this);
    }


    /**
     * Doit etre utilisé par les listener pour ne pas modifier leur affichage lorsque les données sont en cours de rechargement.
     *
     * @return true si une mise à jour du status est en cours
     */
    public boolean isReloading() {
        return reloading;
    }

    /**
     * @return le service principal de gestion des scenarios
     */
    public ModellingScenarioService getModellingScenarioService() {
        return modellingScenarioService;
    }


    @Override
    public void resultChanged(LookupEvent ev) {
        //changement massif: on ne fait rien
        if (modellingScenarioService.isReloading()) {
            return;
        }
        if (modellingScenarioService.isScenarioLoaded()) {
            loadScenario();
        } else {
            unloadScenario();
        }
    }

    @Override
    public Lookup getLookup() {
        return lookup;
    }

    /**
     * la visibilité a été modifiée
     */
    public void activityChanged() {
        activityChanged(getContainerActivityVisibility());
    }

    /**
     * met à jour le lookup de ce service.
     *
     * @param visibility le nouvel etat de visibilité
     */
    private void activityChanged(ContainerActivityVisibility visibility) {
        try {
            //on utilise ce boolean reloading pour que les listeners ne modifient par l'affichage lorsque l'on "remove" l'ancien container
            reloading = true;
            if (modellingScenarioService.isScenarioLoaded()) {
                ContainerActivityVisibility old = getContainerActivityVisibility();
                if (old != null) {
                    dynamicContent.remove(old);
                }
                reloading = false;
                dynamicContent.add(visibility);
            }
        } finally {
            reloading = false;
        }
    }

    /**
     * Le scenario est déchargé-> enleve les listener les données du lookup de ce service
     */
    private void unloadScenario() {
        ContainerActivityVisibility containerActivityVisibility = getContainerActivityVisibility();
        if (containerActivityVisibility != null) {
            dynamicContent.remove(containerActivityVisibility);
        }
    }

    private void loadScenario() {
        unloadScenario();
        ContainerActivityVisibility activity = new ContainerActivityVisibility();
        activity.initialize(modellingScenarioService.getScenarioLoaded());
        dynamicContent.add(activity);
    }


    /**
     * @return la visibilité des conteneurs EMHs
     */
    public ContainerActivityVisibility getContainerActivityVisibility() {
        return lookup.lookup(ContainerActivityVisibility.class);
    }


}
