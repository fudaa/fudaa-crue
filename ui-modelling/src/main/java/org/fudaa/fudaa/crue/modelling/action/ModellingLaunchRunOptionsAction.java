/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.create.RunCreatorOptions;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioModificationService;
import org.fudaa.fudaa.crue.study.actions.RunOptionPanelBuilder;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;

@ActionID(category = "View",
    id = "org.fudaa.fudaa.crue.modelling.ModellingLaunchRunOptionsAction")
@ActionRegistration(displayName = "#CTL_ModellingLaunchRunOptionsAction")
@ActionReferences({
    @ActionReference(path = "Actions/Modelling", position = 15)
})
public final class ModellingLaunchRunOptionsAction extends AbstractModellingAction {
  final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
      ModellingScenarioModificationService.class);

  public ModellingLaunchRunOptionsAction() {
    super(true);
    putValue(Action.NAME, NbBundle.getMessage(ModellingLaunchRunOptionsAction.class, "CTL_ModellingLaunchRunOptionsAction"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingLaunchRunOptionsAction();
  }

  @Override
  public void doAction() {
    if (modellingScenarioModificationService.isModified()) {
      String okMsg = NbBundle.getMessage(ModellingLaunchRunOptionsAction.class, "saveBeforeLaunchingComputationOk");
      String cancelMsg = NbBundle.getMessage(ModellingLaunchRunOptionsAction.class, "saveBeforeLaunchingComputationCancel");
      Object returned = DialogHelper.
          showQuestion(getActionTitle(), NbBundle.getMessage(ModellingLaunchRunOptionsAction.class,
              "saveBeforeLaunchingComputation"), new String[]{okMsg, cancelMsg});
      if (!okMsg.equals(returned)) {
        return;
      }
      scenarioService.saveScenario();
    }

    RunOptionPanelBuilder panelBuilder = new RunOptionPanelBuilder(scenarioService.getManagerScenarioLoaded());
    RunCreatorOptions options = panelBuilder.getOptions();
    if (options == null) {//l'utilisateur a appuye sur cancel...
      return;
    }
    final ManagerEMHScenario managerEMHScenario = projetService.launchRun(scenarioService.getManagerScenarioLoaded(), options);
    scenarioService.updateManagerScenarioLoaded(managerEMHScenario);
  }
}
