/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.metier.emh.DonPrtCIni;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertyStringReadOnly;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.modelling.calcul.importer.CiniParserAndImporter;
import org.fudaa.fudaa.crue.modelling.list.AbstractListContentNode;
import org.fudaa.fudaa.crue.modelling.node.AbstractModellingNodeFirable;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frédéric Deniger
 */
public class CondInitNode extends AbstractModellingNodeFirable {

  public static final String PROP_TYPE_EMH = "typeEMH";
  public static final String PROP_UNITE = "unite";

  public static String getUnitDisplay() {
    return NbBundle.getMessage(CondInitNode.class, "Unit.Name");
  }

  public static String getValueDisplay() {
    return NbBundle.getMessage(CondInitNode.class, "Value.Name");
  }

  public CondInitNode(EMH emh, DonPrtCIni cini, String property, CrueConfigMetier ccm, PerspectiveServiceModelling perspectiveServiceModelling) {
    super(Children.LEAF, Lookups.fixed(emh, cini, property, ccm), perspectiveServiceModelling);
    setName(emh.getNom());
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(getLookup().lookup(DonPrtCIni.class)));
  }

  @Override
  protected Sheet createSheet() {
    Sheet res = Sheet.createDefault();
    Set set = Sheet.createPropertiesSet();
    EMH emh = getLookup().lookup(EMH.class);
    res.put(set);
    Node.Property typeEMH = new PropertyStringReadOnly(emh.getTypei18n(), PROP_TYPE_EMH,
            AbstractListContentNode.getTypeEMHDisplay(),
            null);
    PropertyCrueUtils.configureNoCustomEditor(typeEMH);
    set.put(typeEMH);
    String propertyName = getLookup().lookup(String.class);
    Node.Property type = new PropertyStringReadOnly(propertyName, ListCommonProperties.PROP_TYPE,
            AbstractListContentNode.getTypeDisplay(),
            null);
    PropertyCrueUtils.configureNoCustomEditor(type);
    set.put(type);
    CrueConfigMetier ccm = getLookup().lookup(CrueConfigMetier.class);

    DonPrtCIni cini = getLookup().lookup(DonPrtCIni.class);
    PropertyNodeBuilder builder = new PropertyNodeBuilder();
    PropertySupportReflection value = builder.createFromPropertyDesc(DecimalFormatEpsilonEnum.COMPARISON, cini, this, propertyName);
    //important pour retrouver la valeur par la suite
    value.setName(CiniParserAndImporter.PROP_VALUE);
    value.setDisplayName(getValueDisplay());
    ItemVariable property = ccm.getProperty(propertyName);

    PropertyNodeBuilder.configurePropertySupport(property, value, this);

    PropertyCrueUtils.configureNoCustomEditor(value);
    set.put(value);


    Node.Property unit = new PropertyStringReadOnly(StringUtils.defaultString(property.getNature().getUnite()), PROP_UNITE,
            getUnitDisplay(),
            null);
    PropertyCrueUtils.configureNoCustomEditor(unit);
    set.put(unit);
    return res;
  }
}
