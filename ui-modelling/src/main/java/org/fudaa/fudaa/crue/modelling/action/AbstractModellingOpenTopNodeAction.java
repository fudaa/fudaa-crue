package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.fudaa.crue.common.action.AbstractNodeAction;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.modelling.AbstractModellingTopComponent;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 * @author deniger
 */
public abstract class AbstractModellingOpenTopNodeAction<T extends TopComponent> extends AbstractNodeAction {
    private final String mode;
    private final String topComponentId;
    boolean reopen;

    /**
     * @param name nom de l'action
     * @param mode attention ce mode doit être déclaré dans le projet ui-branding
     */
    public AbstractModellingOpenTopNodeAction(final String name, final String mode, final String topComponentId) {
        super(name);
        this.mode = mode;
        this.topComponentId = topComponentId;
    }

    private static boolean canOpenNewTopComponent() {
        final TopComponent activated = WindowManager.getDefault().getRegistry().getActivated();
        if (activated instanceof AbstractModellingTopComponent) {
            final AbstractModellingTopComponent topComponent = (AbstractModellingTopComponent) activated;
            if (topComponent.isModified()) {
                DialogHelper.showError(NbBundle.getMessage(AbstractModellingOpenTopNodeAction.class, "CurrentViewIsModified.Error"));
                return false;
            }
        }
        return true;
    }

    public boolean isReopen() {
        return reopen;
    }

    public void setReopen(final boolean reopen) {
        this.reopen = reopen;
    }

    protected Long getUid(final Node selectedNode) {
        if (selectedNode == null) {
            return null;
        }
        Long uid = selectedNode.getLookup().lookup(Long.class);
        if (uid == null) {
            final EMH emh = selectedNode.getLookup().lookup(EMH.class);
            if (emh != null) {
                uid = emh.getUiId();
            }
        }
        if (uid == null) {
            return null;
        }
        return uid;
    }

    protected String getMode(final TopComponent top) {
        return mode;
    }

    private void openNewTopComponent(final Node[] activatedNodes) {
        final Node node = activatedNodes.length == 0 ? null : activatedNodes[0];
        final String topComponentIdToUse = getTopComponentId(node);
        T initTc = (T) WindowManager.getDefault().findTopComponent(topComponentIdToUse);
        boolean createNewTopComponent = false;
        //le topcomponent par défaut est déjà ouvert: on ouvre un autre
        if (initTc.isOpened()) {
            try {
                createNewTopComponent = true;
                initTc = (T) initTc.getClass().newInstance();
            } catch (final Exception ex) {
                Exceptions.printStackTrace(ex);
            }
        }
        final TopComponent openNew = activateNewTopComponent(initTc, node);
        //on ouvre le topcomponent dans le mode par défaut
        if (createNewTopComponent) {
            final String modeToUse = getMode(openNew);
            final Mode findMode = WindowManager.getDefault().findMode(modeToUse);
            if (findMode != null) {
                findMode.dockInto(openNew);
            }
        }
        initTc.open();
        initTc.requestActive();
    }

    protected abstract TopComponent activateNewTopComponent(T newTopComponent, Node selectedNode);

    protected String getTopComponentId(final Node selectedNode) {
        return topComponentId;
    }

    protected abstract void activeWithValue(T opened, Node selectedNode);

    @Override
    protected void performAction(final Node[] activatedNodes) {
        if (!canOpenNewTopComponent()) {
            return;
        }
        if (!reopen) {
            openNewTopComponent(activatedNodes);
        } else {
            final T findTopComponent = (T) WindowManager.getDefault().findTopComponent(getTopComponentId(activatedNodes.length == 0 ? null : activatedNodes[0]));
            if (!findTopComponent.isOpened()) {
                openNewTopComponent(activatedNodes);
            } else {
                activeWithValue(findTopComponent, activatedNodes.length == 0 ? null : activatedNodes[0]);
                findTopComponent.requestActive();
            }
        }
    }
}
