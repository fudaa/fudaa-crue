/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import javax.swing.JMenuItem;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.fudaa.crue.common.action.AbstractNodeAction;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.modelling.services.ModellingEditedCalculService;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class CalculEditActionNode extends AbstractNodeAction {

  final ModellingEditedCalculService modellingEditedCalculService = Lookup.getDefault().lookup(ModellingEditedCalculService.class);

  public CalculEditActionNode() {
    super(NbBundle.getMessage(CalculEditActionNode.class, "EditCalcul.ActionName"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    if (activatedNodes.length != 1) {
      return false;
    }
    Calc calc = activatedNodes[0].getLookup().lookup(Calc.class);
    return calc != null && calc.getUiId() != null;
  }

  @Override
  public void setEnabled(boolean e) {
    super.setEnabled(e);
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    Calc calc = activatedNodes[0].getLookup().lookup(Calc.class);
    if (calc != null) {
      modellingEditedCalculService.setEditedCalculUid(calc.getUiId());
    }
  }

  @Override
  public JMenuItem getPopupPresenter() {
    return NodeHelper.useBoldFont(super.getPopupPresenter());
  }
}
