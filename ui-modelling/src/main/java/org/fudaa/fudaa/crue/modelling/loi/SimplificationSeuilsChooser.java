/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.loi;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.SimplificationSeuils;
import org.fudaa.dodico.crue.edition.SimplificationSeuilsProvider;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.ItemVariableView;
import org.fudaa.fudaa.crue.modelling.services.ModellingConfigService;
import org.fudaa.fudaa.crue.options.config.ModellingGlobalConfiguration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class SimplificationSeuilsChooser implements SimplificationSeuilsProvider {

  private final CrueConfigMetier ccm;
  private final ModellingConfigService modellingConfigService = Lookup.getDefault().lookup(ModellingConfigService.class);

  public SimplificationSeuilsChooser(CrueConfigMetier ccm) {
    this.ccm = ccm;
  }

  public SimplificationSeuils choose() {
    ItemVariable seuilVariableCasier = ccm.getProperty(CrueConfigMetierConstants.PROP_DEFAULT_SEUILSIMPLIFPROFILCASIER);
    Double initSeuilSimplifProfilCasier = modellingConfigService.getModellingGlobalConfiguration().getSeuilSimplifProfilCasier();
    ItemVariableView viewCasier = new ItemVariableView.TextField(seuilVariableCasier, DecimalFormatEpsilonEnum.PRESENTATION);
    viewCasier.setValue(initSeuilSimplifProfilCasier);

    ItemVariable seuilVariableSection = ccm.getProperty(CrueConfigMetierConstants.PROP_DEFAULT_SEUILSIMPLIFPROFILSECTION);
    ItemVariableView viewSection = new ItemVariableView.TextField(seuilVariableSection, DecimalFormatEpsilonEnum.PRESENTATION);
    Double initSeuilSimplifProfilSection = modellingConfigService.getModellingGlobalConfiguration().getSeuilSimplifProfilSection();
    viewSection.setValue(initSeuilSimplifProfilSection);

    JPanel pn = new JPanel(new GridLayout(2, 2, 5, 10));
    SysdocUrlBuilder.installHelpShortcut(pn, SysdocUrlBuilder.getDialogHelpCtxId("simplifierProfil", PerspectiveEnum.MODELLING) );
    pn.add(new JLabel(NbBundle.getMessage(SimplificationSeuilsChooser.class, "seuilSimplifyProfilSectionLabel")));
    pn.add(viewSection.getPanelComponent());
    pn.add(new JLabel(NbBundle.getMessage(SimplificationSeuilsChooser.class, "seuilSimplifyCasierProfilLabel")));
    pn.add(viewCasier.getPanelComponent());
    boolean accepted = DialogHelper.
            showQuestionOkCancel(NbBundle.getMessage(SimplificationSeuilsChooser.class, "seuilsSimplifyProfilDialogTitle"), pn);
    if (!accepted) {
      return null;
    }
    Double seuilCasier = (Double) viewCasier.getValue();
    if (seuilCasier.compareTo(0d) <= 0) {
      return null;
    }
    Double seuilProfil = (Double) viewSection.getValue();
    if (seuilProfil.compareTo(0d) <= 0) {
      return null;
    }
    boolean changed = false;
    final ModellingGlobalConfiguration modellingGlobalConfiguration = modellingConfigService.getModellingGlobalConfiguration();
    if (!seuilVariableCasier.getEpsilon().isSame(seuilCasier, initSeuilSimplifProfilCasier)) {
      modellingGlobalConfiguration.setSeuilSimplifProfilCasier(seuilCasier);
      changed = true;
    }
    if (!seuilVariableSection.getEpsilon().isSame(seuilProfil, initSeuilSimplifProfilSection)) {
      modellingGlobalConfiguration.setSeuilSimplifProfilSection(seuilProfil);
      changed = true;
    }
    if (changed) {
      modellingConfigService.setModellingGlobalConfiguration(modellingGlobalConfiguration);
    }
    return new SimplificationSeuils(seuilCasier, seuilProfil);
  }

  @Override
  public SimplificationSeuils getSeuils() {
    return choose();
  }
}
