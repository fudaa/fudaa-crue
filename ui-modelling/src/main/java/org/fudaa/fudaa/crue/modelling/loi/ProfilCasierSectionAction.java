package org.fudaa.fudaa.crue.modelling.loi;

import javax.swing.Action;
import org.fudaa.fudaa.crue.modelling.action.*;
import org.fudaa.fudaa.crue.modelling.perspective.ActiveModelisation;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
        id = "org.fudaa.fudaa.crue.modelling.ProfilCasierSectionAction")
@ActionRegistration(displayName = "#ModellingOpenProfilCasierNodeAction.DisplayName")
@ActionReferences({
  @ActionReference(path = ActiveModelisation.ACTIONS_MODELLING_VIEWS, position = 2)
})
public final class ProfilCasierSectionAction extends AbstractModellingOpenAction {

  public ProfilCasierSectionAction() {
    putValue(Action.NAME, NbBundle.getMessage(ProfilCasierSectionAction.class, "ModellingOpenProfilCasierNodeAction.DisplayName"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ProfilCasierSectionAction();
  }

  @Override
  public void doAction() {
    ProfilCasierOpenNodeAction.open();
  }
}
