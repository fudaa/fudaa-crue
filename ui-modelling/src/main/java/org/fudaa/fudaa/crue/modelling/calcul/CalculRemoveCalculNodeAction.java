/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import java.io.IOException;

import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 * @author Frederic Deniger
 */
public class CalculRemoveCalculNodeAction extends AbstractEditNodeAction {


  private static final String ONE_CALCUL_NAME = NbBundle.getMessage(CalculRemoveCalculNodeAction.class, "RemoveCalculAction.Name");
  private static final String SEVERAL_CALCUL_NAME = NbBundle.getMessage(CalculRemoveCalculNodeAction.class, "RemoveCalculsAction.Name");

  public CalculRemoveCalculNodeAction() {
    super(ONE_CALCUL_NAME);
  }


  @Override
  public String getName() {
    final Node[] activatedNodes = getActivatedNodes();
    if (activatedNodes != null && activatedNodes.length > 1) {
      return SEVERAL_CALCUL_NAME;
    }
    return ONE_CALCUL_NAME;
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return true;
  }


  @Override
  protected void performAction(Node[] activatedNodes) {
    if (activatedNodes == null) return;
    for (Node activatedNode : activatedNodes) {
      try {
        CalculNode node = (CalculNode) activatedNode;
        node.destroy();
      } catch (IOException ex) {
        Exceptions.printStackTrace(ex);
      }
    }

  }
}
