package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;

/**
 *
 * @author Frederic Deniger
 */
public class AddSectionInBranchePanelContentData {

  public final CatEMHBranche branche;
  public final Double distanceMax;
  public final EnumSectionType typeSection;

  public AddSectionInBranchePanelContentData(CatEMHBranche branche, Double distanceMax, EnumSectionType typeSection) {
    this.branche = branche;
    this.distanceMax = distanceMax;
    this.typeSection = typeSection;
  }
}
