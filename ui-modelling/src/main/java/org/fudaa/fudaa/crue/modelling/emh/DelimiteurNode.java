/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.emh;

import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertyStringReadOnly;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author fred
 */
public class DelimiteurNode extends AbstractNode {
  
  public DelimiteurNode(String sectionName) {
    super(Children.LEAF, Lookups.singleton(sectionName));
    setIconBaseWithExtension(LogIconTranslationProvider.NO_IMAGE_PATH);
  }
  
  @Override
  protected Sheet createSheet() {
    Sheet sheet = new Sheet();
    Set set = Sheet.createPropertiesSet();
    String name = getLookup().lookup(String.class);
    sheet.put(set);
    PropertyStringReadOnly property = new PropertyStringReadOnly(name, ListCommonProperties.PROP_NOM, ListCommonProperties.PROP_NOM, null);
    PropertyCrueUtils.configureNoCustomEditor(property);
    set.put(property);
    return sheet;
  }
}
