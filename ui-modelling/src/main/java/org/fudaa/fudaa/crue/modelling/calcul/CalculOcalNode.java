/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.OrdCalc;
import org.fudaa.dodico.crue.metier.factory.OrdCalcFactory;
import org.fudaa.dodico.crue.metier.factory.OrdCalcFactory.OrdCalcCreator;
import org.fudaa.dodico.crue.metier.helper.PropertyFinder;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.modelling.node.AbstractModellingNodeFirable;
import org.fudaa.fudaa.crue.modelling.node.ModellingPropertyEditNode;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frederic Deniger
 */
public class CalculOcalNode extends AbstractModellingNodeFirable {

  public static Children createChildren(OrdCalc ocal, CrueConfigMetier ccm, PerspectiveServiceModelling perspectiveServiceModelling) {
    PropertyFinder finder = new PropertyFinder();
    List<Node> res = new ArrayList<>();
    res.add(new CalculOcalInitNode(ocal, perspectiveServiceModelling));
    List<PropertyFinder.Description> availablePropertiesSorted = finder.getAvailablePropertiesSorted(ocal.getClass());
    for (PropertyFinder.Description description : availablePropertiesSorted) {
      res.add(new ModellingPropertyEditNode(description, ccm, ocal, CalculNode.COLUMN_VALUE, perspectiveServiceModelling));
    }
    return NodeHelper.createArray(res);
  }
  final OutlineView outlineView;

  public CalculOcalNode(OutlineView outlineView, OrdCalc calc, PerspectiveServiceModelling perspectiveServiceModelling, CrueConfigMetier ccm) {
    super(createChildren(calc, ccm, perspectiveServiceModelling), Lookups.fixed(calc, ccm), perspectiveServiceModelling);
    setName(NbBundle.getMessage(CalculOcalNode.class, "Calcul.OCAL.Description"));
    this.outlineView = outlineView;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(getLookup().lookup(OrdCalc.class)));
  }

  public OrdCalc getOrdCalc() {
    return getLookup().lookup(OrdCalc.class);
  }

  public CrueConfigMetier getCcm() {
    return getLookup().lookup(CrueConfigMetier.class);
  }

  void changeInitMethod(String initMethod) {
    Calc calc = getOrdCalc().getCalc();
    Map<String, OrdCalcCreator> creator = OrdCalcFactory.createMap(getOrdCalc());
    OrdCalc newOrdCalc = creator.get(initMethod).create(calc, getCcm());
    final Children children = getParentNode().getChildren();
    children.remove(new Node[]{this});
    final CalculOcalNode calculOcalNode = new CalculOcalNode(outlineView, newOrdCalc, perspectiveServiceModelling, getCcm());
    children.add(new Node[]{calculOcalNode});
    NodeHelper.expandNode(calculOcalNode, outlineView);
    ExplorerManager newManager = ExplorerManager.find(outlineView);
    if (newManager != null) {
      try {
        newManager.setSelectedNodes(new Node[0]);
      } catch (PropertyVetoException ex) {
        Exceptions.printStackTrace(ex);
      }
    }

  }
}
