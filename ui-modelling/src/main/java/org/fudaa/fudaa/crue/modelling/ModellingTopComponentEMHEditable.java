package org.fudaa.fudaa.crue.modelling;

import org.fudaa.dodico.crue.metier.emh.EMH;

import java.util.Collection;

/**
 * @author deniger
 */
public interface ModellingTopComponentEMHEditable {
    /**
     * @param emhs    les emhs à supprimer. Attention si le scenario est cloné juste avant, il faut recuperer les bonnes instances
     * @param cascade true si suppression en cascade
     */
    void deleteEMH(Collection<EMH> emhs, boolean cascade);

    /**
     * @param emhs les emhs a simplifier
     */
    void simplifyProfils(Collection<EMH> emhs);

    /**
     * @param emhs               les emhs
     * @param targetSousModelUid l'ui du sous-modele cible
     */
    void changeSousModeleParent(Collection<EMH> emhs, Long targetSousModelUid);

     /**
     * @param emhs les emhs
     * @param status booléen pour changer la valeur de userActive
     */
    void toggleStatusEMH(Collection<EMH> emhs, boolean status);
}
