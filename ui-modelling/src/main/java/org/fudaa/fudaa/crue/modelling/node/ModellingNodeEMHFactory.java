package org.fudaa.fudaa.crue.modelling.node;

import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSectionProfil;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.fudaa.crue.emh.node.HierarchyChildFactory;
import org.fudaa.fudaa.crue.emh.node.NodeEMHFactory;
import org.fudaa.fudaa.crue.modelling.action.DeleteEMHSectionInterpoleeNotUsedNodeAction;
import org.fudaa.fudaa.crue.common.ContainerActivityVisibility;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author deniger
 */
public class ModellingNodeEMHFactory extends NodeEMHFactory {


  public ModellingNodeEMHFactory(ContainerActivityVisibility containerActivityVisibility) {
    super(containerActivityVisibility);
  }


  public static Node createScenarioNode(final EMHScenario scenario) {
    return createScenarioNode(scenario,null);
  }
  public static Node createScenarioNode(final EMHScenario scenario, ContainerActivityVisibility containerActivityVisibility) {
    return HierarchyChildFactory.createScenarioNode(scenario, new ModellingNodeEMHFactory(containerActivityVisibility), DeleteEMHSectionInterpoleeNotUsedNodeAction.class);
  }

  public static void updateExplorerManagerWithSelectedEmhs(final ExplorerManager em, final Collection<EMH> selected) {
    final List<Node> nodes = new ArrayList<>();
    for (final EMH emh : selected) {
      if (!emh.getCatType().isContainer()) {
        nodes.add(new ModellingNodeEMHFactory(null).createNode(emh, false));
      }
    }
    if (nodes.isEmpty()) {
      em.setRootContext(Node.EMPTY);
    } else {
      final Node[] nodesArray = nodes.toArray(new Node[0]);
      final Children.Array array = new Children.Array();
      array.add(nodesArray);
      final AbstractNode node = new AbstractNode(array);
      em.setRootContext(node);
      try {
        em.setSelectedNodes(nodesArray);
      } catch (final PropertyVetoException propertyVetoException) {
        Exceptions.printStackTrace(propertyVetoException);
      }
    }
  }

  @Override
  protected AbstractNode createNodeEMH(final Children children, final EMH emh, final String displayName) {
    if (EnumCatEMH.CASIER.equals(emh.getCatType())) {
      return new ModellingEMHCasierNode(children, emh, displayName);
    }
    if (EnumCatEMH.BRANCHE.equals(emh.getCatType())) {
      return new ModellingEMHBrancheNode(children, emh, displayName);
    }
    if (EMHSectionProfil.class.equals(emh.getClass())) {
      return new ModellingEMHSectionProfilNode(children, emh, displayName);
    }
    return new ModellingEMHNode(children, emh, displayName);
  }

  @Override
  protected AbstractNode createNodeEMH(final Children children, final EMH emh) {
    if (EnumCatEMH.CASIER.equals(emh.getCatType())) {
      return new ModellingEMHCasierNode(children, emh);
    }
    if (EnumCatEMH.BRANCHE.equals(emh.getCatType())) {
      return new ModellingEMHBrancheNode(children, emh);
    }
    if (EMHSectionProfil.class.equals(emh.getClass())) {
      return new ModellingEMHSectionProfilNode(children, emh);
    }
    return new ModellingEMHNode(children, emh);
  }
}
