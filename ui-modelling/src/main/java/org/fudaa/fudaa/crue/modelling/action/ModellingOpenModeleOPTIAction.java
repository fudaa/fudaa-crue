/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import javax.swing.Action;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.fudaa.crue.modelling.perspective.ActiveModelisation;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View", id = "org.fudaa.fudaa.crue.modelling.ModellingOpenModeleOPTIAction")
@ActionRegistration(displayName = "#ModellingModeleOPTINodeAction.Name")
@ActionReferences({
  @ActionReference(path = ActiveModelisation.ACTIONS_MODELLING_VIEWS, position = 9)
})
public final class ModellingOpenModeleOPTIAction extends AbstractModellingModeleOpenAction {

  public ModellingOpenModeleOPTIAction() {
    putValue(Action.NAME, NbBundle.getMessage(ModellingOpenModeleOPTIAction.class, "ModellingModeleOPTINodeAction.Name"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingOpenModeleOPTIAction();
  }

  @Override
  protected void doAction(EMHModeleBase modele) {
    ModellingOpenModeleOPTINodeAction.open(modele);
  }
}
