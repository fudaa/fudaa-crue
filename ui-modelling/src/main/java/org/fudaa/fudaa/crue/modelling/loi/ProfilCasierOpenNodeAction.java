package org.fudaa.fudaa.crue.modelling.loi;

import org.fudaa.fudaa.crue.modelling.action.AbstractModellingOpenTopNodeAction;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

/**
 *
 *
 * @author Frédéric Deniger
 */
public class ProfilCasierOpenNodeAction extends AbstractModellingOpenTopNodeAction<ProfilCasierEditorTopComponent> {

  public ProfilCasierOpenNodeAction(String name) {
    super(name, ProfilCasierEditorTopComponent.MODE, ProfilCasierEditorTopComponent.TOPCOMPONENT_ID);
  }

  public ProfilCasierOpenNodeAction() {
    super(NbBundle.getMessage(ProfilCasierOpenNodeAction.class, "ModellingOpenProfilCasierNodeAction.DisplayName"), ProfilCasierEditorTopComponent.MODE, ProfilCasierEditorTopComponent.TOPCOMPONENT_ID);
  }

  public static void open() {
    ProfilCasierOpenNodeAction action = SystemAction.get(ProfilCasierOpenNodeAction.Reopen.class);
    action.performAction(new Node[0]);
  }

  @Override
  protected void activeWithValue(ProfilCasierEditorTopComponent opened, Node selectedNode) {
  }

  @Override
  protected TopComponent activateNewTopComponent(ProfilCasierEditorTopComponent newTopComponent, Node selectedNode) {
    return newTopComponent;
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return true;
  }

  public static class Reopen extends ProfilCasierOpenNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ProfilCasierOpenNodeAction.class, "ModellingOpenProfilCasierNodeAction.DisplayName"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ProfilCasierOpenNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ProfilCasierOpenNodeAction.class, "ModellingOpenProfilCasierNodeAction.NewFrame.DisplayName"));
      setReopen(false);
    }
  }
}
