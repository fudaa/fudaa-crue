/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.list;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.LitNumerote;

/**
 *
 * @author Frederic Deniger
 */
public class ModellingListFrottementLine {

  private final Long uidBranche;
  private final Long uiSection;
  private final String name;
  private final List<LitNommeCell> litNommes = new ArrayList<>();

  public ModellingListFrottementLine(final CatEMHSection section) {
    this.uidBranche = section.getBranche().getUiId();
    this.uiSection = section.getUiId();
    name = section.getBranche().getNom() + " / " + section.getNom();
  }

  String getName() {
    return name;
  }

  @Override
  public String toString() {
    return getName();
  }

  Long getBrancheUid() {
    return uidBranche;
  }

  public Long getUiSection() {
    return uiSection;
  }

  public boolean applyModification() {
    boolean res = false;
    for (final LitNommeCell litNommeCell : litNommes) {
      res |= litNommeCell.applyModification();
    }
    return res;
  }

  public List<LitNommeCell> getLitNommes() {
    return litNommes;
  }

  public static class LitNumeroteCell {

    DonFrt currentFrt;
    private final LitNumerote litNumerote;
    // frottements autorisés pour ce lit
    private final DonFrt[] accepted;
    // mémorisation du parent (pour debug)
    private final ModellingListFrottementLine parent;

    public LitNumeroteCell(final LitNumerote litNumerote, final DonFrt[] accepted, final ModellingListFrottementLine parent) {
      this.litNumerote = litNumerote;
      this.accepted = accepted;
      currentFrt = litNumerote.getFrot();
      this.parent = parent;
    }

    public LitNumerote getLitNumerote() {
      return litNumerote;
    }

    public DonFrt[] getAccepted() {
      return accepted;
    }

    public DonFrt getCurrentFrt() {
      return currentFrt;
    }

    public void setCurrentFrt(final DonFrt donFrt) {
      currentFrt = donFrt;
    }

    public boolean applyModification() {
      if (litNumerote.getFrot() != currentFrt) {
        litNumerote.setFrot(currentFrt);
        return true;
      }
      return false;
    }

  }

  public static class LitNommeCell {

    private final List<LitNumeroteCell> litNumerotes = new ArrayList<>();
    // mémorisation du parent (pour debug)
    private final ModellingListFrottementLine parent;
    
    public LitNommeCell(final ModellingListFrottementLine parent) {
      this.parent = parent;
    }

    public boolean applyModification() {
      boolean res = false;
      for (final LitNumeroteCell litNumeroteCell : litNumerotes) {
        res |= litNumeroteCell.applyModification();
      }
      return res;
    }

    public List<LitNumeroteCell> getLitNumerotes() {
      return litNumerotes;
    }

    @Override
    public String toString() {
      final StringBuilder str;
      final LitNumeroteCell lm = litNumerotes.get(0);
      str = new StringBuilder(lm.currentFrt.getNom());
      
      // si plusieurs frottements, on les sépare par un #
      for(int i = 1; i < litNumerotes.size(); i++){
        str.append("#").append(litNumerotes.get(i).currentFrt.getNom());
      }
      
      return str.toString();
    }
  }
}
