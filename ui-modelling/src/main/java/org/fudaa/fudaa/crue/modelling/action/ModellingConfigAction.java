/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultConfigurationPanel;
import org.fudaa.fudaa.crue.modelling.config.ModellingGlobalConfigurationNode;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.fudaa.fudaa.crue.modelling.services.ModellingConfigService;
import org.fudaa.fudaa.crue.options.config.ModellingGlobalConfiguration;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.*;

@ActionID(category = "View",
id = "org.fudaa.fudaa.crue.modelling.ModellingConfigAction")
@ActionRegistration(displayName = "#CTL_ModellingConfig")
@ActionReferences({
  @ActionReference(path = "Actions/Modelling", position = 100, separatorBefore = 99, separatorAfter = 101)
})
public final class ModellingConfigAction extends AbstractModellingAction {

  final ModellingConfigService modellingConfigService = Lookup.getDefault().lookup(ModellingConfigService.class);

  public ModellingConfigAction() {
    super(false);
    putValue(Action.NAME, NbBundle.getMessage(ModellingConfigAction.class, "CTL_ModellingConfig"));
  }

  @Override
  public Action createContextAwareInstance(final Lookup actionContext) {
    return new ModellingConfigAction();
  }

  @Override
  public void doAction() {
    final ModellingGlobalConfiguration modellingGlobalConfiguration = modellingConfigService.getModellingGlobalConfiguration();
    final ModellingGlobalConfiguration cloned = modellingGlobalConfiguration.copy();
    final DefaultConfigurationPanel panel = new DefaultConfigurationPanel();
    final PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
    final ModellingGlobalConfigurationNode node = new ModellingGlobalConfigurationNode(cloned, perspectiveServiceModelling);
    panel.setNode(node);
    panel.setSelectedNode(node);

    final Object[] options;
    if (perspectiveServiceModelling.isInEditMode()) {
      options = new Object[]{NotifyDescriptor.OK_OPTION, NotifyDescriptor.CANCEL_OPTION};
    } else {
      options = new Object[]{NotifyDescriptor.CANCEL_OPTION};
    }
    final DialogDescriptor dialogDescriptor = new DialogDescriptor(panel, getActionTitle(), true, options,
            NotifyDescriptor.OK_OPTION,
            DialogDescriptor.DEFAULT_ALIGN,
            HelpCtx.DEFAULT_HELP,
            null);
    SysdocUrlBuilder.installDialogHelpCtx(dialogDescriptor, "editerConfigurationEtude",PerspectiveEnum.MODELLING,false);
    dialogDescriptor.setClosingOptions(new Object[]{NotifyDescriptor.OK_OPTION, NotifyDescriptor.CANCEL_OPTION});
    dialogDescriptor.setButtonListener(e -> {
      panel.stopEdition();
      final Object value = dialogDescriptor.getValue();
      //valider les valeurs:
      if (NotifyDescriptor.OK_OPTION.equals(value)) {
        modellingConfigService.setModellingGlobalConfiguration(cloned);
      }
    });
    final Dialog dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
    dialog.pack();
    dialog.setLocationRelativeTo(WindowManager.getDefault().getMainWindow());
    dialog.setVisible(true);
  }
}
