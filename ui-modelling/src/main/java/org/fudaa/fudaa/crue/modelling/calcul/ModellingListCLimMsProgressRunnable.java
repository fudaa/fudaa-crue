package org.fudaa.fudaa.crue.modelling.calcul;

import org.fudaa.ctulu.table.CtuluExcelCsvFileReader;
import org.fudaa.fudaa.crue.common.helper.AbstractFileImporterProgressRunnable;
import org.fudaa.fudaa.crue.modelling.calcul.importer.CLimMsImporter;
import org.fudaa.fudaa.crue.views.DonClimMTableModel;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

import java.io.File;

/**
 * @author Frederic Deniger
 */
public class ModellingListCLimMsProgressRunnable extends AbstractFileImporterProgressRunnable implements ProgressRunnable<DonClimMTableModel> {
    /**
     * Le top component associé au process d'importer
     */
    protected final ModellingListCLimMsTopComponent topComponent;

    public ModellingListCLimMsProgressRunnable(final File file, final ModellingListCLimMsTopComponent topComponent) {
        super(file);
        this.topComponent = topComponent;
    }

    @Override
    public DonClimMTableModel run(final ProgressHandle handle) {
        String[][] values = null;
        if (file != null) {
            values = new CtuluExcelCsvFileReader(file).readFile();
        }
        if (values == null) {
            return null;
        }

        final CLimMsImporter importer = new CLimMsImporter(topComponent.getCcm(), topComponent.getScenario(), this.topComponent.tableModel.getDonLoiHYConteneur(),
                values);
        return importer.importData();
    }
}
