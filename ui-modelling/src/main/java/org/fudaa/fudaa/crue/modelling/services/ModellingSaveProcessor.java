package org.fudaa.fudaa.crue.modelling.services;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.Pair;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

/**
 *
 * Valide le scenario et sauvegarde.
 *
 * @author deniger
 */
public class ModellingSaveProcessor implements ProgressRunnable<Pair<Set<String>, CtuluLogGroup>> {

  final ModellingScenarioServiceImpl modellingScenarioServiceImpl = Lookup.getDefault().lookup(ModellingScenarioServiceImpl.class);

  public ModellingSaveProcessor() {
  }

  @Override
  public Pair<Set<String>, CtuluLogGroup> run(ProgressHandle handle) {
    if (handle != null) {
      handle.switchToIndeterminate();
    }
    CtuluLogGroup res = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    res.setDescription("common.save");
    res.setDescriptionArgs(modellingScenarioServiceImpl.getScenarioLoaded().getNom());
    ModellingSaveScenarioCallable scenario = new ModellingSaveScenarioCallable();
    ModellingSaveGeoLocCallable geoLoc = new ModellingSaveGeoLocCallable();
    ModellingSaveStudyConfigCallable studyConfig = new ModellingSaveStudyConfigCallable();
    ModellingSaveStudyConfigExternCallable studyExternConfig = new ModellingSaveStudyConfigExternCallable();
    ExecutorService newCachedThreadPool = Executors.newFixedThreadPool(3);
    try {
      List<Future<CtuluLogGroup>> invokeAll = newCachedThreadPool.invokeAll(Arrays.asList(scenario, geoLoc, studyConfig,
              studyExternConfig));
      for (Future<CtuluLogGroup> future : invokeAll) {
        CtuluLogGroup group = future.get();
        if (group != null) {
          res.addGroup(group);
        }
      }
    } catch (Exception ex) {
      Exceptions.printStackTrace(ex);
    } finally {
      newCachedThreadPool.shutdownNow();
    }

    return new Pair<>(scenario.getModifiedFile(),res);
  }
}
