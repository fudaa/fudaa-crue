package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.modelling.services.ModellingEMHVisibilityService;
import org.fudaa.fudaa.crue.common.ContainerActivityVisibility;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class ChangeDefaultSousModeleNodeAction extends AbstractEditNodeAction {

  final ModellingEMHVisibilityService modellingEMHVisibilityService = Lookup.getDefault().lookup(ModellingEMHVisibilityService.class);

  public ChangeDefaultSousModeleNodeAction() {
    super(NbBundle.getMessage(ChangeDefaultSousModeleNodeAction.class, "ChangeDefaultSousModeleAction.Modele"));
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    if (activatedNodes.length != 1) {
      return false;
    }
    final EMHSousModele ssModele = activatedNodes[0].getLookup().lookup(EMHSousModele.class);
    if (ssModele == null) {
      return false;
    }
    //sous modele deja active.
    return !modellingEMHVisibilityService.getContainerActivityVisibility().isSousModeleActif(ssModele);
  }

  @Override
  protected void performAction(final Node[] activatedNodes) {
    final EMHSousModele ssModele = activatedNodes[0].getLookup().lookup(EMHSousModele.class);
    modellingEMHVisibilityService.getContainerActivityVisibility().setSousModeleActifUid(ssModele.getUiId());
    modellingEMHVisibilityService.activityChanged();
  }
}
