/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul.importer;

import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.metier.cini.CiniImportKey;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.openide.util.NbBundle.getMessage;

/**
 * @author Frederic Deniger
 */
public class CiniImporter {
  public static final String PROP_VALUE = "value";
  private Map<CiniImportKey, Object> toImport;
  private final Node mainNode;
  private ExplorerManager explorerManager;
  private boolean logIgnoredValue = true;

  public CiniImporter(final Node mainNode) {
    this.mainNode = mainNode;
  }


  public void setLogIfIgnoredValue(final boolean logIgnoredValue) {
    this.logIgnoredValue = logIgnoredValue;
  }


  public void setToImport(final Map<CiniImportKey, Object> toImport) {
    this.toImport = toImport;
  }

  public ExplorerManager getExplorerManager() {
    return explorerManager;
  }

  public void setExplorerManager(final ExplorerManager explorerManager) {
    this.explorerManager = explorerManager;
  }

  public Node getMainNode() {
    return mainNode;
  }

  private Node.Property findByName(final Node node) {
    final Node.PropertySet propertiesSet = node.getPropertySets()[0];
    final Node.Property<?>[] properties = propertiesSet.getProperties();
    for (final Node.Property<?> property : properties) {
      if (PROP_VALUE.equals(property.getName())) {
        return property;
      }
    }
    return null;
  }

  public void importData(final CtuluLog parentLog) {
    final List<Node> nodes = new ArrayList<>();
    for (final Node node : mainNode.getChildren().getNodes()) {
      nodes.addAll(Arrays.asList(node.getChildren().getNodes()));
    }
    final Map<CiniImportKey, Node> keys = new CiniImportKeyFromNodes(nodes).getKeys();
    final Map<CiniImportKey, Object> metier = toImport;
    int nbModifiedValues = 0;
    final List<Node> nodeToSelect = new ArrayList<>();
    for (final Map.Entry<CiniImportKey, Object> entry : metier.entrySet()) {
      final CiniImportKey ciniImportKey = entry.getKey();
      final Object value = entry.getValue();
      final Node node = keys.get(ciniImportKey);
      if (node != null) {
        final Node.Property valueProperty = findByName(node);
        try {
          if (valueProperty != null && !ObjectUtils.equals(valueProperty.getValue(), value)) {
            nbModifiedValues++;
            nodeToSelect.add(node);
            valueProperty.setValue(value);
          }
        } catch (final Exception exception) {
          Exceptions.printStackTrace(exception);
        }
      } else if (logIgnoredValue) {
        //on logue si demandé.
        parentLog.addWarn(getMessage(CiniImporter.class, "import.cini.ignoredData", entry.getKey().getEmh() + " / " + entry.getKey().getTypeValue()));
      }
    }
    if (!nodeToSelect.isEmpty() && explorerManager != null) {
      try {
        explorerManager.setSelectedNodes(nodeToSelect.toArray(new Node[0]));
      } catch (final PropertyVetoException ex) {
        Exceptions.printStackTrace(ex);
      }
    }
    parentLog.addInfo(getMessage(CiniImporter.class, "import.cini.nbModifiedValue", nbModifiedValues));
  }
}
