package org.fudaa.fudaa.crue.modelling.list;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.edition.bean.ListNoeudContent;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.util.Lookup;

/**
 *
 * @author deniger
 */
public class ListNoeudContentFactory {

  final PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);

  public List<ListNoeudContentNode> createNodes(List<CatEMHNoeud> noeuds) {
    List<ListNoeudContentNode> res = new ArrayList<>();
    int ordre = 0;
    for (CatEMHNoeud catEMHNoeud : noeuds) {
      ListNoeudContent noeud = new ListNoeudContent();
      noeud.setCommentaire(catEMHNoeud.getCommentaire());
//      noeud.setOrdre(++ordre);
      noeud.setNom(catEMHNoeud.getNom());
      noeud.setType(catEMHNoeud.getNoeudType());
      ListNoeudContentNode node = new ListNoeudContentNode(catEMHNoeud, noeud, perspectiveServiceModelling);
      node.setDisplayName(Integer.toString(++ordre));
      res.add(node);
    }
    return res;
  }
//  protected static void nodeDragged(final Node dragged, final Node target) {
//    if (dragged == null || target == null) {
//      return;
//    }
//    Node parentNode = dragged.getParentNode();
//    List<Node> children = new ArrayList(Arrays.asList(parentNode.getChildren().getNodes()));
//    parentNode.getChildren().remove(children.toArray(new Node[children.size()]));
//    int targetIdx = children.indexOf(target);
//    children.remove(dragged);
//    if (targetIdx >= children.size()) {
//      children.add(dragged);
//    } else {
//      children.add(targetIdx, dragged);
//    }
//    int ordre = 0;
//    for (Node node : children) {
//      node.setDisplayName(Integer.toString(++ordre));
//    }
//    parentNode.getChildren().add(children.toArray(new Node[children.size()]));
//  }
//
//  public static void nodeToEnd(final List<Node> toEnd) {
//    if (CollectionUtils.isEmpty(toEnd)) {
//      return;
//    }
//    Node parentNode = toEnd.get(0).getParentNode();
//    List<Node> children = new ArrayList(Arrays.asList(parentNode.getChildren().getNodes()));
//    parentNode.getChildren().remove(children.toArray(new Node[children.size()]));
//    children.removeAll(toEnd);
//    for (Node node : toEnd) {
//      children.add(node);
//    }
//    int ordre = 0;
//    for (Node node : children) {
//      node.setDisplayName(Integer.toString(++ordre));
//    }
//    parentNode.getChildren().add(children.toArray(new Node[children.size()]));
//  }
//
//  public static void nodeToStart(final List<Node> toStart) {
//    if (CollectionUtils.isEmpty(toStart)) {
//      return;
//    }
//    Node parentNode = toStart.get(0).getParentNode();
//    List<Node> children = new ArrayList(Arrays.asList(parentNode.getChildren().getNodes()));
//    parentNode.getChildren().remove(children.toArray(new Node[children.size()]));
//    children.removeAll(toStart);
//    List<Node> newChildren = new ArrayList<Node>();
//    for (Node node : toStart) {
//      newChildren.add(node);
//    }
//    newChildren.addAll(children);
//    int ordre = 0;
//    for (Node node : newChildren) {
//      node.setDisplayName(Integer.toString(++ordre));
//    }
//    parentNode.getChildren().add(newChildren.toArray(new Node[newChildren.size()]));
//  }
}
