package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.fudaa.crue.modelling.calcul.ModellingModeleOPTGTopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public abstract class ModellingOpenModeleOPTGNodeAction extends AbstractModellingOpenModeleNodeAction {

  public ModellingOpenModeleOPTGNodeAction(String name) {
    super(name, ModellingModeleOPTGTopComponent.MODE, ModellingModeleOPTGTopComponent.TOPCOMPONENT_ID);
  }

  public static void open(EMHModeleBase modele) {
    if (modele == null) {
      return;
    }
    ModellingOpenModeleOPTGNodeAction action = SystemAction.get(ModellingOpenModeleOPTGNodeAction.Reopen.class);
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(modele));
    action.performAction(new Node[]{node});
  }

  public static class Reopen extends ModellingOpenModeleOPTGNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenModeleOPTGNodeAction.class, "ModellingModeleOPTGNodeAction.Name"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenModeleOPTGNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenModeleOPTGNodeAction.class, "ModellingModeleOPTGNodeAction.NewFrame.Name"));
      setReopen(false);
    }
  }
}
