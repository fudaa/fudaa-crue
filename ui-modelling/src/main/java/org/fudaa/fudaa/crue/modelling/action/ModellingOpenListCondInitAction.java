/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import javax.swing.Action;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.fudaa.crue.modelling.perspective.ActiveModelisation;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View", id = "org.fudaa.fudaa.crue.modelling.ModellingOpenListCondInitAction")
@ActionRegistration(displayName = "#ModellingListCiniTopComponent.Name")
@ActionReferences({
  @ActionReference(path = ActiveModelisation.ACTIONS_MODELLING_VIEWS, position = 12)
})
public final class ModellingOpenListCondInitAction extends AbstractModellingModeleOpenAction {

  public ModellingOpenListCondInitAction() {
    putValue(Action.NAME, NbBundle.getMessage(ModellingOpenListCondInitAction.class, "ModellingListCiniTopComponent.Name"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingOpenListCondInitAction();
  }

  @Override
  protected void doAction(EMHModeleBase modele) {
    ModellingOpenListCondInitNodeAction.open(modele);
  }
}
