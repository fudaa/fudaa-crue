package org.fudaa.fudaa.crue.modelling.services;

import java.util.concurrent.Callable;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.projet.ScenarioLoader;

/**
 *
 * @author deniger
 */
public class ModellingValidCallable implements Callable<CtuluLogGroup> {

  private final ModellingScenarioServiceImpl service;

  public ModellingValidCallable(ModellingScenarioServiceImpl service) {
    this.service = service;
  }

  @Override
  public CtuluLogGroup call() throws Exception {
    return ScenarioLoader.validateScenario(service.getScenarioLoaded(), service.getSelectedProjet().getCoeurConfig(),
            service.getManagerScenarioLoaded().isCrue9(), new ScenarioAutoModifiedState());
  }
}
