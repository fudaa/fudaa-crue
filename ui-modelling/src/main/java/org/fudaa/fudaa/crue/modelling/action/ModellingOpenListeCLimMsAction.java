package org.fudaa.fudaa.crue.modelling.action;

import javax.swing.Action;
import org.fudaa.fudaa.crue.modelling.perspective.ActiveModelisation;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
        id = "org.fudaa.fudaa.crue.modelling.ModellingOpenListeCLimMsAction")
@ActionRegistration(displayName = "#ModellingOpenListeCLimMsNodeAction.Name")
@ActionReferences({
  @ActionReference(path = ActiveModelisation.ACTIONS_MODELLING_VIEWS, position = 25)
})
public final class ModellingOpenListeCLimMsAction extends AbstractModellingOpenAction {

  public ModellingOpenListeCLimMsAction() {
    putValue(Action.NAME, NbBundle.getMessage(ModellingOpenListeCLimMsAction.class, "ModellingOpenListeCLimMsNodeAction.Name"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingOpenListeCLimMsAction();
  }

  @Override
  public void doAction() {
    ModellingOpenListeCLimMsNodeAction.open(modellingScenarioService.getScenarioLoaded());
  }
}
