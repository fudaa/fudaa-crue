package org.fudaa.fudaa.crue.modelling.list;

import gnu.trove.TLongDoubleHashMap;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent.SectionData;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.validation.util.DelegateValidatorBranche;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.modelling.edition.ListAjoutRelationSectionProcess;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioVisuService;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

import java.text.NumberFormat;
import java.util.*;

/**
 * TODO utiliser ListRelationSectionContentFactory pour la construction des objects "métier".
 *
 * @author deniger
 */
public class ListRelationSectionContentFactoryNode {
  final PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
  final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);
  final ModellingScenarioVisuService modellingScenarioVisuService = Lookup.getDefault().lookup(ModellingScenarioVisuService.class);

  private static class NodeByXpComparator extends SafeComparator<Node> {
    @Override
    protected int compareSafe(final Node o1, final Node o2) {
      final ListRelationSectionContent content1 = o1.getLookup().lookup(ListRelationSectionContent.class);
      final ListRelationSectionContent content2 = o2.getLookup().lookup(ListRelationSectionContent.class);
      return compareComparable(content1, content2);
    }
  }

  public void sort(final Node mainNode, final boolean sortByXp) {
    final Node[] nodes = mainNode.getChildren().getNodes();
    mainNode.getChildren().remove(nodes);
    final LinkedHashMap<String, List<Node>> byBrancheName = new LinkedHashMap<>();
    for (final Node node : nodes) {
      final ListRelationSectionContent lookup = node.getLookup().lookup(ListRelationSectionContent.class);
      final String brancheNom = lookup.getBrancheNom();
      List<Node> brancheNodes = byBrancheName.get(brancheNom);
      if (brancheNodes == null) {
        brancheNodes = new ArrayList<>();
        byBrancheName.put(brancheNom, brancheNodes);
      }
      brancheNodes.add(node);
    }
    final Map<String, EMH> emhByNom = modellingScenarioService.getScenarioLoaded().getIdRegistry().getEmhByNom();
    final List<Node> newNode = new ArrayList<>();
    final List<EnumBrancheType> brancheTypeWithDistanceNotNull = DelegateValidatorBranche.getBrancheTypeWithDistanceNotNull();
    final NodeByXpComparator comparator = new NodeByXpComparator();
    for (final Map.Entry<String, List<Node>> entry : byBrancheName.entrySet()) {
      final String brancheName = entry.getKey();
      if (StringUtils.isNotBlank(brancheName)) {
        final List<Node> brancheNodes = entry.getValue();
        if (sortByXp) {
          final CatEMHBranche branche = (CatEMHBranche) emhByNom.get(brancheName);
          if (branche != null && brancheTypeWithDistanceNotNull.contains(branche.getBrancheType())) {
            Collections.sort(brancheNodes, comparator);
          }
        }
        newNode.addAll(brancheNodes);
      }
    }
    final List<Node> otherNodes = byBrancheName.get(StringUtils.EMPTY);
    if (otherNodes != null) {
      newNode.addAll(otherNodes);
    }
    mainNode.getChildren().add(newNode.toArray(new Node[0]));
  }

  public List<ListRelationSectionContentNode> createNodes(final EMHSousModele sousModele, final TLongDoubleHashMap distanceByBrancheUid) {
    final List<ListRelationSectionContentNode> res = new ArrayList<>();
    final Set<CatEMHSection> done = new HashSet<>();
    final List<CatEMHBranche> branches = sousModele.getBranches();
    final List<String> brancheSectionNames = ListAjoutRelationSectionProcess.getAvailableBranchesForSections(sousModele);
    final CrueConfigMetier propDefinition = modellingScenarioService.getSelectedProjet().getPropDefinition();
    final NumberFormat dzFormatter = propDefinition.getFormatter(CrueConfigMetierConstants.PROP_DZ, DecimalFormatEpsilonEnum.COMPARISON);
    final ItemVariable xpFormatter = propDefinition.getProperty(CrueConfigMetierConstants.PROP_XP);
    for (final CatEMHBranche branche : branches) {
      final List<RelationEMHSectionDansBranche> sections = branche.getSections();
      final double distanceSchematique = distanceByBrancheUid == null ? 0 : distanceByBrancheUid.get(branche.getUiId());
      final double distanceHydraul = branche.getLength();
      for (final RelationEMHSectionDansBranche relationSection : sections) {
        final CatEMHSection section = relationSection.getEmh();
        final ListRelationSectionContent sectionContent = new ListRelationSectionContent();
        sectionContent.setAbscisseHydraulique(relationSection.getXp());
        sectionContent.setAdditionalDataRelation(relationSection.getAdditionalData());
        double xSchematique = 0;
        if (!xpFormatter.getEpsilon().isZero(distanceHydraul)) {//pas de div par zero !
          xSchematique = distanceSchematique * relationSection.getXp() / distanceHydraul;
        }
        sectionContent.setAbscisseSchematique(xpFormatter.format(xSchematique, DecimalFormatEpsilonEnum.COMPARISON));
        sectionContent.setBrancheNom(branche.getNom());

        fillContentWithSectionData(sectionContent, section, dzFormatter);
        final ListRelationSectionContentNode node = new ListRelationSectionContentNode(section, sectionContent, brancheSectionNames,
          perspectiveServiceModelling);
        node.setDisplayName(StringUtils.EMPTY);
        res.add(node);
        done.add(section);
      }
    }
    final List<CatEMHSection> sections = sousModele.getSections();
    for (final CatEMHSection section : sections) {
      if (!done.contains(section)) {
        done.add(section);
        final ListRelationSectionContent sectionContent = new ListRelationSectionContent();
        sectionContent.setBrancheNom(StringUtils.EMPTY);
        fillContentWithSectionData(sectionContent, section, dzFormatter);
        final ListRelationSectionContentNode node = new ListRelationSectionContentNode(section, sectionContent, brancheSectionNames,
          perspectiveServiceModelling);
        node.setDisplayName(StringUtils.EMPTY);
        res.add(node);
      }
    }
    return res;
  }

  public ListRelationSectionContentNode create(final CatEMHBranche branche, final RelationEMHSectionDansBranche relationSection, final CrueConfigMetier ccm) {
    final NumberFormat dzFormatter = ccm.getProperty(CrueConfigMetierConstants.PROP_DZ).getFormatter(DecimalFormatEpsilonEnum.COMPARISON);
    final ItemVariable xpProperty = ccm.getProperty(CrueConfigMetierConstants.PROP_XP);
    final ListRelationSectionContent content = createSectionContent(branche, relationSection, dzFormatter);
    TLongDoubleHashMap distanceByBrancheUid = null;
    if (modellingScenarioVisuService.getPlanimetryVisuPanel() != null) {
      distanceByBrancheUid = modellingScenarioVisuService.getPlanimetryVisuPanel().getPlanimetryController().getDistanceByBrancheUid();
    }
    final double distanceHydraul = branche.getLength();
    final double distanceSchematique = distanceByBrancheUid == null ? 0 : distanceByBrancheUid.get(branche.getUiId());
    double xSchematique = 0;
    if (!xpProperty.getEpsilon().isZero(distanceHydraul)) {//pas de div par zero !
      xSchematique = distanceSchematique * relationSection.getXp() / distanceHydraul;
    }
    content.setAbscisseSchematique(xpProperty.format(xSchematique, DecimalFormatEpsilonEnum.COMPARISON));

    final ListRelationSectionContentNode node = new ListRelationSectionContentNode(relationSection.getEmh(), content,
      Collections.emptyList(), perspectiveServiceModelling);
    return node;
  }

  private static ListRelationSectionContent createSectionContent(final CatEMHBranche branche, final RelationEMHSectionDansBranche section, final NumberFormat formatter) {
    final ListRelationSectionContent content = new ListRelationSectionContent(section.getEmhNom());
    content.setBrancheNom(branche == null ? null : branche.getNom());
    ListRelationSectionContentFactoryNode.fillContentWithSectionData(content, section.getEmh(), formatter);
    content.setAbscisseHydraulique(section.getXp());
    content.setAdditionalDataRelation(section.getAdditionalData());
    return content;
  }

  public static void fillContentWithSectionData(final ListRelationSectionContent relationSection, final CatEMHSection section, final NumberFormat dzFormatter) {
    relationSection.setNom(section.getNom());
    final SectionData sectionData = relationSection.getSection();
    sectionData.setSectionType(section.getSectionType());
    sectionData.setCommentaire(section.getCommentaire());
    if (section.getSectionType().equals(EnumSectionType.EMHSectionProfil)) {
      final DonPrtGeoProfilSection profil = DonPrtHelper.getProfilSection((EMHSectionProfil) section);
      sectionData.setProfilSection(profil.getNom());
    } else if (section.getSectionType().equals(EnumSectionType.EMHSectionIdem)) {
      final CatEMHSection sectionRef = EMHHelper.getSectionRef((EMHSectionIdem) section);
      if (sectionRef != null) {
        sectionData.setReferenceSectionIdem(sectionRef.getNom());
      }
      final DonPrtGeoSectionIdem dptg = EMHHelper.getDPTGSectionIdem((EMHSectionIdem) section);
      sectionData.setDz(dptg.getDz());
    }
  }
}
