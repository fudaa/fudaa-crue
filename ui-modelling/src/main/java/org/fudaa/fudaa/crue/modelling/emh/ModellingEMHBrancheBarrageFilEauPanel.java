/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.emh;

import com.Ostermiller.util.CSVParser;
import com.memoire.bu.BuGridLayout;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.edition.bean.BrancheEditionContent;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.common.view.DefaultOutlineViewEditor;
import org.fudaa.fudaa.crue.common.view.ItemVariableView;
import org.fudaa.fudaa.crue.common.view.NodeEditionHelper;
import org.fudaa.fudaa.crue.modelling.loi.ModellingOpenDcspLoiNodeAction;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.util.List;
import java.util.*;

/**
 * Branche Type 2
 *
 * @author fred
 */
public final class ModellingEMHBrancheBarrageFilEauPanel extends JPanel implements ModellingEMHBrancheSpecificEditor, Observer {

    private final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(
            ModellingScenarioService.class);
    final transient PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
    private final ModellingEMHBrancheTopComponent parent;
    private List<ItemVariableView> properties;
    private JComboBox cbSectionPilote;
    private final SeuilView seuilView;

    ModellingEMHBrancheBarrageFilEauPanel(CatEMHBranche branche, ModellingEMHBrancheTopComponent parent) {
        super(new BorderLayout(5, 15));
        this.parent = parent;
        DonCalcSansPrtBrancheBarrageFilEau dcsp = EMHHelper.selectFirstOfClass(branche.getInfosEMH(), DonCalcSansPrtBrancheBarrageFilEau.class);
        if (dcsp == null) {
            dcsp = new DonCalcSansPrtBrancheBarrageFilEau(getCcm());
        }
        seuilView = new SeuilView();
        seuilView.initWith(dcsp);
        createTopPanel(branche, dcsp);
        add(seuilView);
    }

    @Override
    public ExplorerManager getExplorerManager() {
        return null;
    }

    private void createTopPanel(CatEMHBranche branche, DonCalcSansPrtBrancheBarrageFilEau dcsp) throws MissingResourceException {
        JPanel top = new JPanel(new BuGridLayout(4, 10, 10));
        add(top, BorderLayout.NORTH);

        properties = ItemVariableView.create(DecimalFormatEpsilonEnum.COMPARISON, dcsp, getCcm(), DonCalcSansPrtBrancheNiveauxAssocies.PROP_QLIMINF,
                DonCalcSansPrtBrancheNiveauxAssocies.PROP_QLIMSUP);
        for (ItemVariableView propertyEditorPanel : properties) {
            propertyEditorPanel.addObserver(this);

        }
        ItemVariableView first = properties.get(0);
        top.add(new JLabel(first.getLabelName()));
        top.add(first.getPanelComponent());
        top.add(new JLabel(NbBundle.getMessage(ModellingEMHBrancheBarrageFilEauPanel.class, "SectionPilote.DisplayName")));
        cbSectionPilote = ModellingEMHBrancheBarrageGeneriquePanel.createSectionPilote(branche);
        cbSectionPilote.addActionListener(parent.modifiedActionListener);
        top.add(cbSectionPilote);

        ItemVariableView second = properties.get(1);
        top.add(new JLabel(second.getLabelName()));
        top.add(second.getPanelComponent());
        top.add(new JLabel(NbBundle.getMessage(ModellingEMHBrancheBarrageFilEauPanel.class, "LoiQpilZam.Label.DisplayName")));
        JButton bt = new JButton(NbBundle.getMessage(ModellingEMHBrancheBarrageFilEauPanel.class, "LoiQpilZam.Button.DisplayName"));
        bt.addActionListener(e -> openLoiQpilZam());

        top.add(bt);
    }

    private void openLoiQpilZam() {
        ModellingOpenDcspLoiNodeAction.open(parent.getEMHUid(), EnumTypeLoi.LoiQpilZam, false);
    }

    @Override
    public void update(Observable o, Object arg) {
        parent.setModified(true);
    }

    private CrueConfigMetier getCcm() {
        return modellingScenarioService.getSelectedProjet().getPropDefinition();
    }

    @Override
    public void setEditable(boolean editable) {
        for (ItemVariableView object : properties) {
            object.setEditable(editable);
        }
        cbSectionPilote.setEnabled(editable);
        seuilView.setEditable(editable);
    }

    @Override
    public void fillWithData(BrancheEditionContent content) {
        DonCalcSansPrtBrancheBarrageFilEau dcsp = new DonCalcSansPrtBrancheBarrageFilEau(getCcm());
        ItemVariableView.transferToBean(dcsp, properties);
        dcsp.setElemBarrage(seuilView.getElemSeuil());
        content.setSectionPilote((CatEMHSection) cbSectionPilote.getSelectedItem());
        content.setDcsp(dcsp);
    }

    @Override
    public JComponent getComponent() {
        return this;
    }

    private class SeuilView extends DefaultOutlineViewEditor {

        void initWith(DonCalcSansPrtBrancheBarrageFilEau dcsp) {
            List<ElemBarrageNode> nodes = new ArrayList<>();
            if (dcsp != null) {
                Collection<ElemBarrage> elemBarrages = dcsp.getElemBarrage();
                CrueConfigMetier ccm = getCcm();
                for (ElemBarrage elemBarrage : elemBarrages) {
                    nodes.add(new ElemBarrageNode(elemBarrage.clone(), ccm, perspectiveServiceModelling));
                }
            }

            getExplorerManager().setRootContext(NodeHelper.createNode(nodes, "ElemBarrage", false));
            DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
        }

        @Override
        protected void changeInNode(String propertyName) {
            parent.setModified(true);
        }

        List<ElemBarrage> getElemSeuil() {
            List<ElemBarrage> res = new ArrayList<>();
            Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
            for (Node node : nodes) {
                ElemBarrage elemSeuil = node.getLookup().lookup(ElemBarrage.class);
                res.add(elemSeuil);
            }
            return res;
        }

        @Override
        protected JButton createPasteComponent() {
            JButton button = new JButton(NbBundle.getMessage(ModellingEMHBrancheSeuilTransversalPanel.class, "Paste.DisplayName"));
            button.addActionListener(e -> paste());
            return button;
        }

        protected void paste() {
            List<ElemBarrageNode> pastData = getPastData(getCcm(), perspectiveServiceModelling);
            if (!pastData.isEmpty()) {
                NodeEditionHelper.addNodesFromClipboard(getExplorerManager(), pastData);
            }
        }

        @Override
        protected void addElement() {
            ElemBarrage elem = new ElemBarrage(getCcm());
            Children children = getExplorerManager().getRootContext().getChildren();
            final ElemBarrageNode elemPdtNode = new ElemBarrageNode(elem, getCcm(), perspectiveServiceModelling);
            children.add(new Node[]{elemPdtNode});
            DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
        }

        private CrueConfigMetier getCcm() {
            return modellingScenarioService.getSelectedProjet().getPropDefinition();
        }

        @Override
        protected void createOutlineView() {
            view = new OutlineView(StringUtils.EMPTY);
            view.setPropertyColumns(CrueConfigMetierConstants.PROP_ZSEUIL,
                    ElemBarrageNode.getZSeuilDisplayName(),
                    CrueConfigMetierConstants.PROP_LARGEUR,
                    ElemBarrageNode.getLargeurDisplayName(),
                    ElemBarrage.PROP_COEF_NOY,
                    ElemBarrageNode.getCoefNoyDisplayName(),
                    ElemBarrage.PROP_COEF_DEN,
                    ElemBarrageNode.getCoefDenDisplayName());
            view.getOutline().setColumnHidingAllowed(false);
            view.getOutline().setRootVisible(false);
            view.setDragSource(true);
            view.setDropTarget(true);
        }
    }

    public static List<ElemBarrageNode> getPastData(CrueConfigMetier ccm, PerspectiveServiceModelling perspective) {
        try {
            String toCopy = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
            String[][] parse = CSVParser.parse(toCopy, '\t');
            List<ElemBarrageNode> contents = new ArrayList<>();
            for (String[] strings : parse) {
                if (strings.length >= 1) {
                    ElemBarrage newContent = new ElemBarrage(ccm);
                    contents.add(new ElemBarrageNode(newContent, ccm, perspective));
                    try {
                        newContent.setZseuil(Double.parseDouble(strings[0]));
                    } catch (NumberFormatException numberFormatException) {
                    }
                    if (strings.length >= 2) {
                        try {
                            newContent.setLargeur(Double.parseDouble(strings[1]));
                        } catch (NumberFormatException numberFormatException) {
                        }
                        if (strings.length >= 3) {
                            try {
                                newContent.setCoefNoy(Double.parseDouble(strings[2]));
                            } catch (NumberFormatException numberFormatException) {
                            }
                            if (strings.length >= 4) {
                                try {
                                    newContent.setCoefDen(Double.parseDouble(strings[3]));
                                } catch (NumberFormatException numberFormatException) {
                                }

                            }

                        }

                    }

                }
            }
            return contents;
        } catch (Exception exception) {
        }
        return Collections.emptyList();
    }
}
