package org.fudaa.fudaa.crue.modelling.list;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuBorders;
import com.memoire.bu.BuGridLayout;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.edition.EditionBrancheSaintVenantInternSplit;
import org.fudaa.dodico.crue.edition.bean.BrancheSplitContent;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.modelling.AbstractModellingTopComponent;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioVisuService;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.fudaa.fudaa.crue.planimetry.action.SplitBrancheProcess;
import org.fudaa.fudaa.crue.planimetry.layout.NetworkPresetData;
import org.jdesktop.swingx.JXTable;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.text.NumberFormat;

import static org.openide.util.NbBundle.getMessage;

/**
 * Top component which displays something.
 */
@TopComponent.Description(preferredID = ModellingListBrancheSplitTopComponent.TOPCOMPONENT_ID, iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png", persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ModellingListBrancheSplitTopComponent.MODE, openAtStartup = false, position = 0)
public final class ModellingListBrancheSplitTopComponent extends AbstractModellingTopComponent {
    //attention le mode modelling-listSplitBranche doit être déclaré dans le projet branding (layer.xml)
    public static final String MODE = "modelling-listSplitBranche";
    public static final String TOPCOMPONENT_ID = "ModellingListBrancheSplitTopComponent";
    public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
    public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
    private final JLabel labelBrancheSplitted = new JLabel();
    private final JLabel labelSectionUsedToSplit = new JLabel();
    private final JTextField textFieldNewBrancheName = new JTextField(20);
    private final JLabel labelNewBrancheValid = new JLabel();
    private final JTextField textFieldNewNoeudName = new JTextField(20);
    private final JLabel labelNewNoeudValid = new JLabel();
    private final JTable tableSection = new JXTable();
    private transient BrancheSplitContent brancheSplitContent;

    private final ModellingScenarioVisuService modellingScenarioVisuService = Lookup.getDefault().lookup(ModellingScenarioVisuService.class);

    public ModellingListBrancheSplitTopComponent() {
        super();
        initComponents();
        setName(getMessage(ModellingListBrancheSplitTopComponent.class, TOPCOMPONENT_ACTION));
        setToolTipText(getMessage(ModellingListBrancheSplitTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
        setBorder(BuBorders.EMPTY5555);
        add(createTopPanel(), BorderLayout.NORTH);
        add(createCancelSaveButtons(), BorderLayout.SOUTH);

        final JPanel center = new JPanel(new BuBorderLayout(0, 10));
        center.setBorder(BorderFactory.createTitledBorder(
                getMessage(ModellingListBrancheSplitTopComponent.class, "ModellingListBrancheSplitTopComponent.SectionZoneTitle"))
        );
        center.add(new JScrollPane(tableSection));

        add(center);
    }

    private JPanel createTopPanel() {
        final JPanel top = new JPanel(new BuGridLayout(3, 5, 5, true, false, true, true));
        JLabel jLabel = new JLabel(getMessage(ModellingListBrancheSplitTopComponent.class, "ModellingListBrancheSplitTopComponent.BrancheSplitted"));
        jLabel.setToolTipText(getMessage(ModellingListBrancheSplitTopComponent.class, "ModellingListBrancheSplitTopComponent.BrancheSplitted.Help"));
        top.add(jLabel);
        top.add(labelBrancheSplitted);
        final JLabel label = new JLabel();
        label.setPreferredSize(new Dimension(200, 15));
        top.add(label);
        jLabel = new JLabel(getMessage(ModellingListBrancheSplitTopComponent.class, "ModellingListBrancheSplitTopComponent.SectionUsedToSplit"));
        jLabel.setToolTipText(getMessage(ModellingListBrancheSplitTopComponent.class, "ModellingListBrancheSplitTopComponent.SectionUsedToSplit.Help"));

        top.add(jLabel);
        top.add(labelSectionUsedToSplit);
        top.add(new JLabel());

        jLabel = new JLabel(getMessage(ModellingListBrancheSplitTopComponent.class, "ModellingListBrancheSplitTopComponent.NewBrancheName"));
        jLabel.setToolTipText(getMessage(ModellingListBrancheSplitTopComponent.class, "ModellingListBrancheSplitTopComponent.NewBrancheName.Help"));
        top.add(jLabel);
        top.add(textFieldNewBrancheName);
        labelNewBrancheValid.setPreferredSize(new Dimension(100, 15));
        top.add(labelNewBrancheValid);

        jLabel = new JLabel(getMessage(ModellingListBrancheSplitTopComponent.class, "ModellingListBrancheSplitTopComponent.NewNoeudName"));
        jLabel.setToolTipText(getMessage(ModellingListBrancheSplitTopComponent.class, "ModellingListBrancheSplitTopComponent.NewNoeudName.Help"));
        top.add(jLabel);
        top.add(textFieldNewNoeudName);
        labelNewBrancheValid.setPreferredSize(new Dimension(100, 15));
        top.add(labelNewNoeudValid);

        textFieldNewBrancheName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(final DocumentEvent e) {
                updateStateNewBrancheName();
            }

            @Override
            public void removeUpdate(final DocumentEvent e) {
                updateStateNewBrancheName();
            }

            @Override
            public void changedUpdate(final DocumentEvent e) {
                updateStateNewBrancheName();
            }
        });
        textFieldNewNoeudName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(final DocumentEvent e) {
                updateStateNewNoeudName();
            }

            @Override
            public void removeUpdate(final DocumentEvent e) {
                updateStateNewNoeudName();
            }

            @Override
            public void changedUpdate(final DocumentEvent e) {
                updateStateNewNoeudName();
            }
        });
        textFieldNewBrancheName.setEditable(false);
        textFieldNewNoeudName.setEditable(false);
        return top;
    }

    private static String isNameValide(final JTextField textField, final EnumCatEMH catEMH) {
        return ValidationPatternHelper.isNameValidei18nMessage(textField.getText(), catEMH);
    }

    private void updateStateNewBrancheName() {
        updateNameState(getScenario(), labelNewBrancheValid, textFieldNewBrancheName, EnumCatEMH.BRANCHE);
        updateValidState();
    }

    private void updateStateNewNoeudName() {
        updateNameState(getScenario(), labelNewNoeudValid, textFieldNewNoeudName, EnumCatEMH.NOEUD);
        updateValidState();
    }

    private void updateValidState() {
        if (isModified()) {
            final boolean valid = StringUtils.isBlank(labelNewBrancheValid.getText()) && StringUtils.isBlank(labelNewNoeudValid.getText());
            buttonSave.setEnabled(valid);
        }
    }

    protected static void updateNameState(final EMHScenario scenario, final JLabel labelDisplayingErrors, final JTextField textField, final EnumCatEMH catEMH) {
        String nameValide = isNameValide(textField, catEMH);
        if (nameValide == null) {
            final EMH byName = scenario.getIdRegistry().findByName(textField.getText());
            if (byName != null) {
                nameValide = BusinessMessages.getString("create.emhAlreadyExists", textField.getText());
            }
        }
        updateValidState(labelDisplayingErrors, nameValide);
    }

    public void setSection(final CatEMHSection catEMHSection) {
        clearData();
        final EditionBrancheSaintVenantInternSplit splitter = new EditionBrancheSaintVenantInternSplit(getCcm());
        final CrueOperationResult<BrancheSplitContent> content = splitter.createContent(catEMHSection.getBranche(), catEMHSection);
        brancheSplitContent = null;
        if (content.isErrorOrEmpty()) {
            updateEditableState();
            LogsDisplayer.displayError(content.getLogs(), getName());
            return;
        }
        brancheSplitContent = content.getResult();
        labelBrancheSplitted.setText(brancheSplitContent.brancheNameAmontToSplit);
        labelSectionUsedToSplit.setText(brancheSplitContent.sectionNameToSplitOn);
        textFieldNewNoeudName.setText(brancheSplitContent.newNoeudName);
        textFieldNewBrancheName.setText(brancheSplitContent.newBrancheNameAval);
        labelNewNoeudValid.setVisible(true);
        labelNewBrancheValid.setVisible(true);
        final String newSectionName = EditionBrancheSaintVenantInternSplit.getNewSectionName(getScenario(), new UniqueNomFinder(), brancheSplitContent.newBrancheNameAval);
        final TableModel tableModel = new SectionTableModel(brancheSplitContent, getCcm(), newSectionName);
        tableSection.setModel(tableModel);
        updateEditableState();
    }

    private void clearData() {
        brancheSplitContent = null;
        labelBrancheSplitted.setText(StringUtils.EMPTY);
        labelSectionUsedToSplit.setText(StringUtils.EMPTY);
        textFieldNewBrancheName.setText(StringUtils.EMPTY);
        textFieldNewNoeudName.setText(StringUtils.EMPTY);
        labelNewBrancheValid.setVisible(false);
        labelNewNoeudValid.setVisible(false);
        tableSection.setModel(new SectionTableModel(null, null, null));
    }

    @Override
    protected void scenarioLoaded() {
        //nothing to do
    }

    @Override
    protected void scenarioUnloaded() {
        setModified(false);
        clearData();
        close();
    }

    @Override
    protected void scenarioReloaded() {
        setModified(false);
        clearData();
    }

    @Override
    protected void setEditable(final boolean b) {
        textFieldNewNoeudName.setEditable(b && brancheSplitContent != null);
        textFieldNewBrancheName.setEditable(textFieldNewNoeudName.isEditable());
        if (textFieldNewNoeudName.isEditable()) {
            setModified(true);
        }
    }

    @Override
    public void cancelModificationHandler() {
        clearData();
        setModified(false);
    }

    @Override
    public void valideModificationHandler() {
        if (brancheSplitContent != null) {
            brancheSplitContent.newBrancheNameAval = StringUtils.trim(textFieldNewBrancheName.getText());
            brancheSplitContent.newNoeudName = StringUtils.trim(textFieldNewNoeudName.getText());
            //on doit récupérer la géometrie de la branche
            SplitBrancheProcess splitBrancheProcess = new SplitBrancheProcess(modellingScenarioVisuService.getPlanimetryVisuPanel().getPlanimetryController());
            SplitBrancheProcess.Result branchesGeometries = splitBrancheProcess.split(brancheSplitContent.brancheToSplitUid, brancheSplitContent.sectionToSplitOnUid);

            final EditionBrancheSaintVenantInternSplit split = new EditionBrancheSaintVenantInternSplit(getCcm());
            final CtuluLog ctuluLog = split.split(getScenario(), brancheSplitContent);
            if (ctuluLog.containsErrorOrSevereError()) {
                LogsDisplayer.displayError(ctuluLog, getName());
                return;
            }
            IdRegistry idRegistry = getModellingService().getScenarioLoaded().getIdRegistry();
            EMH newBranche = idRegistry.findByName(brancheSplitContent.newBrancheNameAval);
            EMH newNoeud = idRegistry.findByName(brancheSplitContent.newNoeudName);
            if (newBranche != null && newNoeud != null) {
                NetworkPresetData presetData = new NetworkPresetData(getScenario());
                presetData.addBranche((CatEMHBranche) newBranche, branchesGeometries.newBrancheAval);
                presetData.addBranche((CatEMHBranche) idRegistry.getEmh(brancheSplitContent.brancheToSplitUid), branchesGeometries.newBrancheAmont);
                presetData.addNoeud((CatEMHNoeud) newNoeud, branchesGeometries.newBrancheAval.getCoordinateN(0));
                modellingScenarioVisuService.getPlanimetryVisuPanel().getPlanimetryController().savePreset(presetData);

            }
            modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.EMH_CREATION, EnumModification.EMH_RELATION));
        }
        clearData();
        setModified(false);
        close();
    }

    @Override
    protected String getViewHelpCtxId() {
        return "vueSplitBranche";
    }

    private static class SectionTableModel extends AbstractTableModel {
        private final BrancheSplitContent brancheSplitContent;
        private final NumberFormat xpFormatter;
        private final String[] firstLineContent = new String[3];

        public SectionTableModel(final BrancheSplitContent brancheSplitContent, final CrueConfigMetier crueConfigMetier, final String firstSectionName) {
            this.brancheSplitContent = brancheSplitContent;
            if (brancheSplitContent != null) {
                this.xpFormatter = crueConfigMetier.getFormatter(CrueConfigMetierConstants.PROP_XP, DecimalFormatEpsilonEnum.COMPARISON);
                firstLineContent[0] = firstSectionName + " " + getMessage(SectionTableModel.class, "SectionTableModel.TableNewSectionSuffix");
                firstLineContent[1] = EnumSectionType.EMHSectionIdem.geti18n();
                firstLineContent[2] = "0";
            } else {
                xpFormatter = null;
            }
        }

        @Override
        public String getColumnName(final int column) {
            if (column == 0) {
                return getMessage(SectionTableModel.class, "SectionTableModel.TableSectionName");
            }
            if (column == 1) {
                return getMessage(SectionTableModel.class, "SectionTableModel.TableSectionType");
            }
            return getMessage(SectionTableModel.class, "SectionTableModel.NewXp");
        }

        @Override
        public int getRowCount() {
            return brancheSplitContent == null ? 0 : (brancheSplitContent.sections.size() + 1);
        }

        @Override
        public int getColumnCount() {
            return 3;
        }

        @Override
        public Object getValueAt(final int rowIndex, final int columnIndex) {
            if (brancheSplitContent == null) {
                return StringUtils.EMPTY;
            }
            if (rowIndex == 0) {
                return firstLineContent[columnIndex];
            }
            final int sectionIdx = rowIndex - 1;
            final BrancheSplitContent.SectionContent sectionContent = brancheSplitContent.sections.get(sectionIdx);
            if (columnIndex == 0) {
                return sectionContent.nom;
            }
            if (columnIndex == 1) {
                return sectionContent.sectionType.geti18n();
            }
            return xpFormatter.format(sectionContent.abscisseHydraulique);
        }

        @Override
        public Class<?> getColumnClass(final int columnIndex) {
            return String.class;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout(0, 10));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
