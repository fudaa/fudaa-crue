package org.fudaa.fudaa.crue.modelling.global;

import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertySupportReadWrite;
import org.fudaa.fudaa.crue.modelling.action.*;
import org.fudaa.fudaa.crue.modelling.loi.ModellingOpenDFRTNodeAction;
import org.fudaa.fudaa.crue.modelling.loi.ModellingOpenDLHYNodeAction;
import org.fudaa.fudaa.crue.modelling.loi.ProfilCasierOpenNodeAction;
import org.fudaa.fudaa.crue.modelling.loi.ProfilSectionOpenNodeAction;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.fudaa.fudaa.crue.modelling.services.ModellingEMHVisibilityService;
import org.fudaa.fudaa.crue.common.ContainerActivityVisibility;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;
import java.util.MissingResourceException;

/**
 * @author deniger
 */
public abstract class GlobalContainerNode extends AbstractNodeFirable {

  final ModellingEMHVisibilityService modellingEMHVisibilityService = Lookup.getDefault().lookup(ModellingEMHVisibilityService.class);
  final PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
  public static final String PROP_VISIBILITY = "VISIBILTY";

  public static String getPropVisibilityDisplayName() throws MissingResourceException {
    return NbBundle.getMessage(GlobalContainerNode.class, "PropertyVisible.Name");
  }

  @Override
  public boolean isEditMode() {
    //le premier test bizarre (perspectiveServiceModelling == null)  est utilise uniquement pour les tests.
    return perspectiveServiceModelling == null || perspectiveServiceModelling.isInEditMode();
  }

  public class VisibilityPropertySupport extends PropertySupportReadWrite<Long, Boolean> {
    public VisibilityPropertySupport(AbstractNodeFirable node, Long instance) {
      super(node, instance, Boolean.TYPE, PROP_VISIBILITY, getPropVisibilityDisplayName(),
        null);
    }

    @Override
    public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
      Long instance = getInstance();
      return modellingEMHVisibilityService.getContainerActivityVisibility().isVisible(instance);
    }

    @Override
    protected void setValueInInstance(Boolean val) {
      if (val == null) {
        return;
      }
      Long instance = getInstance();
      modellingEMHVisibilityService.getContainerActivityVisibility().setVisible(instance, val);
      modellingEMHVisibilityService.activityChanged();
    }
  }

  public GlobalContainerNode(Children children, CatEMHConteneur modele) {
    super(children, Lookups.singleton(modele));
    setDisplayName(modele.getNom());
  }

  @Override
  protected Sheet createSheet() {
    CatEMHConteneur conteneur = getLookup().lookup(CatEMHConteneur.class);
    if (conteneur.getCatType().equals(EnumCatEMH.SCENARIO)) {
      return super.createSheet();
    }
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    set.put(new VisibilityPropertySupport(this, conteneur.getUiId()));
    sheet.put(set);
    return sheet;
  }

  public static class EMHSousModeleNode extends GlobalContainerNode {
    final Result<ContainerActivityVisibility> lookupResult;
    String actifDisplay = null;

    public EMHSousModeleNode(Children children, EMHSousModele modele) {
      super(children, modele);
      lookupResult = modellingEMHVisibilityService.getLookup().lookupResult(
        ContainerActivityVisibility.class);
      lookupResult.addLookupListener(new LookupListener() {
        @Override
        public void resultChanged(LookupEvent ev) {
          updateName();
        }
      });
      updateName();
    }

    private void updateName() {
      actifDisplay = null;
      EMHSousModele lookup = getLookup().lookup(EMHSousModele.class);
      if (modellingEMHVisibilityService != null && modellingEMHVisibilityService.getContainerActivityVisibility() != null && modellingEMHVisibilityService
        .getContainerActivityVisibility().isSousModeleActif(
          lookup)) {
        actifDisplay = "<b>" + lookup.getNom() + "</b>";
      }
      fireDisplayNameChange("", getDisplayName());
    }

    @Override
    public String getHtmlDisplayName() {
      return actifDisplay;
    }

    @Override
    public Action[] getActions(boolean context) {
      return new Action[]{
        SystemAction.get(ChangeDefaultSousModeleNodeAction.class),
        null,
        SystemAction.get(ModellingOpenListNoeudsNodeAction.Reopen.class),
        SystemAction.get(ModellingOpenListNoeudsNodeAction.NewFrame.class),
        SystemAction.get(ModellingOpenListNoeudsAddNodeAction.class),
        null,
        SystemAction.get(ModellingOpenListCasiersNodeAction.Reopen.class),
        SystemAction.get(ModellingOpenListCasiersNodeAction.NewFrame.class),
        SystemAction.get(ModellingOpenListCasiersAddNodeAction.class),
        null,
        SystemAction.get(ModellingOpenListBranchesNodeAction.Reopen.class),
        SystemAction.get(ModellingOpenListBranchesNodeAction.NewFrame.class),
        SystemAction.get(ModellingOpenListBranchesAddNodeAction.class),
        null,
        SystemAction.get(ModellingOpenListSectionsNodeAction.Reopen.class),
        SystemAction.get(ModellingOpenListSectionsNodeAction.NewFrame.class),
        SystemAction.get(ModellingOpenListSectionAddNodeAction.class),
        null,
        SystemAction.get(ModellingOpenDFRTNodeAction.Reopen.class),
        SystemAction.get(ModellingOpenDFRTNodeAction.NewFrame.class),
        null,
        SystemAction.get(ModellingOpenListFrottementsNodeAction.Reopen.class),
        SystemAction.get(ModellingOpenListFrottementsNodeAction.NewFrame.class),
        null,
        SystemAction.get(UpdateSousModeleFromFolderNodeAction.class)};
    }
  }

  public static class EMHModeleNode extends GlobalContainerNode {
    public EMHModeleNode(Children children, EMHModeleBase modele) {
      super(children, modele);
    }

    @Override
    public Action[] getActions(boolean context) {
      return new Action[]{
        SystemAction.get(ModellingOpenModeleOPTGNodeAction.Reopen.class),
        SystemAction.get(ModellingOpenModeleOPTGNodeAction.NewFrame.class),
        null,
        SystemAction.get(ModellingOpenModeleOPTRNodeAction.Reopen.class),
        SystemAction.get(ModellingOpenModeleOPTRNodeAction.NewFrame.class),
        null,
        SystemAction.get(ModellingOpenModeleOPTINodeAction.Reopen.class),
        SystemAction.get(ModellingOpenModeleOPTINodeAction.NewFrame.class),
        null,
        SystemAction.get(ModellingOpenModelePNUMNodeAction.Reopen.class),
        SystemAction.get(ModellingOpenModelePNUMNodeAction.NewFrame.class),
        null,
        SystemAction.get(ModellingOpenListCondInitNodeAction.Reopen.class),
        SystemAction.get(ModellingOpenListCondInitNodeAction.NewFrame.class)
      };
    }
  }

  public static class EMHScenarioNode extends GlobalContainerNode {
    public EMHScenarioNode(Children children, EMHScenario modele) {
      super(children, modele);
    }

    @Override
    public Action[] getActions(boolean context) {
      return new Action[]{
        SystemAction.get(ProfilSectionOpenNodeAction.Reopen.class),
        SystemAction.get(ProfilSectionOpenNodeAction.NewFrame.class),
        null,
        SystemAction.get(ProfilCasierOpenNodeAction.Reopen.class),
        SystemAction.get(ProfilCasierOpenNodeAction.NewFrame.class),
        null,
        SystemAction.get(ModellingOpenDLHYNodeAction.Reopen.class),
        SystemAction.get(ModellingOpenDLHYNodeAction.NewFrame.class),
        null,
        SystemAction.get(ModellingOpenScenarioORESNodeAction.Reopen.class),
        SystemAction.get(ModellingOpenScenarioORESNodeAction.NewFrame.class),
        null,
        SystemAction.get(ModellingOpenScenarioPCALNodeAction.Reopen.class),
        SystemAction.get(ModellingOpenScenarioPCALNodeAction.NewFrame.class),
        null,
        SystemAction.get(ModellingOpenListeCLimMsNodeAction.Reopen.class),
        SystemAction.get(ModellingOpenListeCLimMsNodeAction.NewFrame.class)
      };
    }
  }
}
