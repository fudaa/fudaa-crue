package org.fudaa.fudaa.crue.modelling.loi;

import com.memoire.bu.BuGridLayout;
import java.awt.BorderLayout;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.ExportTableCommentSupplier;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.validation.ValidationHelperLoi;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.loi.loiff.LoiUiController;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.fudaa.fudaa.crue.study.services.TableExportCommentSupplier;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Editeur de lois.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.loi//DcspEditorLoi//EN",
        autostore = false)
@TopComponent.Description(preferredID = DcspEditorLoiTopComponent.TOPCOMPONENT_ID,
        persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = DcspEditorLoiTopComponent.MODE, openAtStartup = false)
public final class DcspEditorLoiTopComponent extends AbstractModellingLoiTopComponent implements ExportTableCommentSupplier {

  public static final String MODE = "loi-dcspEditor"; //NOI18N
  public static final String TOPCOMPONENT_ID = "DcspEditorLoiTopComponent"; //NOI18N
  private final transient LoiUiController loiUiController;
  private final JTextField txtCommentaire;
  private final JLabel lbNom;

  public DcspEditorLoiTopComponent() {
    initComponents();
    setName(NbBundle.getMessage(DcspEditorLoiTopComponent.class, "CTL_" + TOPCOMPONENT_ID));
    setToolTipText(NbBundle.getMessage(DcspEditorLoiTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    loiUiController = new LoiUiController();
    loiUiController.addExportImagesToToolbar();
    add(loiUiController.getToolbar(), BorderLayout.NORTH);
    final JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, loiUiController.getPanel(), loiUiController.getTableGraphePanel());
    splitPane.setDividerLocation(550);
    splitPane.setOneTouchExpandable(true);
    splitPane.setContinuousLayout(true);
    add(splitPane);

    final JPanel nomComment = new JPanel(new BuGridLayout(2, 10, 10));
    nomComment.add(new JLabel(NbBundle.getMessage(DcspEditorLoiTopComponent.class, "Description.DisplayName")));
    lbNom = new JLabel();
    nomComment.add(lbNom);
    nomComment.add(new JLabel(NbBundle.getMessage(DcspEditorLoiTopComponent.class, "Commentaire.DisplayName")));
    txtCommentaire = new JTextField(30);
    txtCommentaire.setEditable(false);
    txtCommentaire.getDocument().addDocumentListener(super.createDocumentListener());
    nomComment.add(txtCommentaire);
    final JPanel info = new JPanel(new BorderLayout(5, 5));
    info.add(loiUiController.getInfoController().getPanel());
    info.add(nomComment, BorderLayout.NORTH);
    info.add(createCancelSaveButtons(), BorderLayout.SOUTH);
    add(info, BorderLayout.SOUTH);
  }
  Long uid;
  EnumTypeLoi typeLoi;

  @Override
  public void setEditable(final boolean b) {
    super.setEditable(b);
    loiUiController.setEditable(b);
    txtCommentaire.setEditable(b);
  }

  @Override
  protected String getDefaultTopComponentId() {
    return TOPCOMPONENT_ID;
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueEditionLoiDcsp";
  }

  @Override
  public List<String> getComments() {
    return new TableExportCommentSupplier(false).getComments(getName());
  }

  @Override
  public void cancelModificationHandler() {
    reloadCourbeConfig();
    scenarioReloaded();
  }

  public void setLoi(final Long uid, final EnumTypeLoi typeLoi) {
    this.uid = uid;
    this.typeLoi = typeLoi;
    scenarioLoaded();

  }

  private Loi getLoi() {
    final EMH emh = modellingScenarioModificationService.getModellingScenarioService().getScenarioLoaded().getIdRegistry().getEmh(uid);
    Loi loi = null;
    if (emh != null) {
      final List<DonCalcSansPrt> dcsp = emh.getDCSP();
      if (CollectionUtils.isNotEmpty(dcsp)) {
        final List<Loi> lois = new ArrayList<>();
        dcsp.get(0).fillWithLoi(lois);
        loi = findLoi(typeLoi, lois);
      }
    }
    return loi;
  }

  private Loi getLoi(final EMH emh) {
    Loi loi = null;
    if (emh != null) {
      final List<DonCalcSansPrt> dcsp = emh.getDCSP();
      if (CollectionUtils.isNotEmpty(dcsp)) {
        final List<Loi> lois = new ArrayList<>();
        dcsp.get(0).fillWithLoi(lois);
        loi = findLoi(typeLoi, lois);
      }
    }
    return loi;
  }

  @Override
  protected void scenarioLoadedHandler() {
    updating = true;
    final EMH emh = modellingScenarioModificationService.getModellingScenarioService().getScenarioLoaded().getIdRegistry().getEmh(uid);
    final Loi loi = getLoi(emh);
    txtCommentaire.setText(StringUtils.EMPTY);
    lbNom.setText(StringUtils.EMPTY);
    if (loi != null) {
      txtCommentaire.setText(loi.getCommentaire());
      final String descId = "DescriptionLoiDcsp." + typeLoi.getId();
      String desc = null;
      try {
        desc = NbBundle.getMessage(DcspEditorLoiTopComponent.class, descId, emh.getNom());
      } catch (final MissingResourceException missingResourceException) {
        Logger.getLogger(DcspEditorLoiTopComponent.class.getName()).log(Level.INFO, "message {0}", missingResourceException);
      }
      if (desc == null) {
        desc = emh.getNom() + ": " + typeLoi.toString();
      }
      lbNom.setText(desc);
      setName(desc);
    }
    loiUiController.setLoi(loi, getCcm(), false);
    loiUiController.getLoiModel().addObserver((o, arg) -> setModified(true));
    updating = false;
    loiUiController.getGraphe().setExportTableCommentSupplier(this);
    applyCourbeConfig();
    setModified(false);
  }

  private Loi findLoi(final EnumTypeLoi typeLoi, final List<Loi> lois) {
    for (final Loi loi : lois) {
      if (typeLoi.equals(loi.getType())) {
        return loi;
      }
    }
    return null;
  }

  @Override
  protected void scenarioReloaded() {
    scenarioLoaded();
  }

  @Override
  public LoiUiController getLoiUIController() {
    return loiUiController;
  }

  @Override
  protected void scenarioUnloadedHandler() {
    loiUiController.setLoi(null, null, false);
    close();
  }

  @Override
  public boolean valideModificationDataHandler() {
    final Loi current = getLoi();
    final Loi cloned = current.cloneWithoutPoints();
    final List<PtEvolutionFF> pts = loiUiController.getLoiModel().getPoints();
    cloned.getEvolutionFF().setPtEvolutionFF(pts);
    cloned.setCommentaire(txtCommentaire.getText());
    final CtuluLog valideLoi = ValidationHelperLoi.valideLoi(cloned, getCcm());
    boolean res = false;
    if (!valideLoi.containsErrorOrSevereError()) {
      res = true;
      current.setCommentaire(txtCommentaire.getText());
      current.getEvolutionFF().setPtEvolutionFF(pts);
      modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.DCSP));
    }
    if (valideLoi.isNotEmpty()) {
      LogsDisplayer.displayError(valideLoi, getName());
    }
    return res;

  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
  void writeProperties(final java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
  }

  void readProperties(final java.util.Properties p) {
  }
}
