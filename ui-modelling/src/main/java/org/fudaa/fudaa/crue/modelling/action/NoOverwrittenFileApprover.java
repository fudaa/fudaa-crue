package org.fudaa.fudaa.crue.modelling.action;

import com.memoire.bu.BuResource;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.fudaa.crue.common.helper.CrueFileChooserBuilder;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.FileHelper;
import org.openide.windows.WindowManager;

/**
 *
 * @author deniger
 */
public class NoOverwrittenFileApprover implements CrueFileChooserBuilder.SelectionApprover {

  private final List<String> extensions;

  public NoOverwrittenFileApprover(List<String> extension) {
    this.extensions = extension;
  }

  protected List<File> getFiles(final File init) {
    if (init == null) {
      return Collections.emptyList();
    }
    final File dir = init.getParentFile();
    final String name = FileHelper.getFilenameSansExtension(init.getName());
    final List<File> res = new ArrayList<>(extensions.size());
    for (final String ext : extensions) {
      res.add(new File(dir, CtuluLibFile.getFileName(name, ext)));
    }
    return res;
  }

  public boolean isFileOk(final File file) {
    final List<File> files = getFiles(file);
    List<String> overwrittenFile = new ArrayList<>(files.size());
    for (final File dest : files) {
      final String err = CtuluLibFile.canWrite(dest);
      if (err != null) {
        DialogHelper.showError(BuResource.BU.getString("Fichier"), err);
        return false;
      }
      if (dest.exists()) {
        overwrittenFile.add(dest.getName());
      }
    }
    if (!overwrittenFile.isEmpty()) {
      if (overwrittenFile.size() == 1) {
        return CtuluLibDialog.confirmeOverwriteFile(WindowManager.getDefault().getMainWindow(), overwrittenFile.get(0));
      }
      if (overwrittenFile.size() > 3) {
        overwrittenFile = overwrittenFile.subList(0, 2);
        overwrittenFile.add("...");
      }
      return CtuluLibDialog.confirmeOverwriteFiles(WindowManager.getDefault().getMainWindow(), StringUtils.join(overwrittenFile, "; "));
    }
    return true;
  }

  @Override
  public boolean approve(File selection) {
    if (selection != null) {
      return isFileOk(selection);
    }
    return false;
  }

  @Override
  public boolean approve(File[] selection) {
    return false;
  }
}
