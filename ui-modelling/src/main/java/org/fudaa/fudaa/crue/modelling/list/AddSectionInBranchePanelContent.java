package org.fudaa.fudaa.crue.modelling.list;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuGridLayout;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizableTransformer;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.fudaa.crue.common.helper.ComboBoxHelper;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.view.ItemVariableView;
import org.openide.DialogDescriptor;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.*;

/**
 *
 * @author Frederic Deniger
 */
public class AddSectionInBranchePanelContent implements Observer, ItemListener {

  private final JLabel lbDistance;
  private final JComboBox cbBranches;
  private final ItemVariableView.TextFiedDistance tfDistance;
  private DialogDescriptor dialog;
  private final Map<String, EMHBrancheSaintVenant> branchesByName;
  private final ItemVariable xp;
  private final JPanel pn;
  private final JComboBox cbTypeSection;
  // Dictionnaire de correspondance Nom Type de section - Type de section
  private final Map<String, EnumSectionType> sectionTypeMap;

  protected AddSectionInBranchePanelContent(final CrueConfigMetier ccm, final EMHSousModele sousModele) {
    final List<EMHBrancheSaintVenant> branches = sousModele.getBranchesSaintVenant();
    final List<String> branchesNoms = TransformerHelper.toNom(branches);
    Collections.sort(branchesNoms);
    branchesByName = TransformerHelper.toMapOfNom(branches);
    xp = ccm.getProperty(CrueConfigMetierConstants.PROP_XP);
    tfDistance = new ItemVariableView.TextFiedDistance(ccm, DecimalFormatEpsilonEnum.COMPARISON);

    cbBranches = new JComboBox();
    ComboBoxHelper.setDefaultModel(cbBranches, branchesNoms);
    lbDistance = new JLabel();
    pn = new JPanel(new BuGridLayout(3, 5, 10, true, true));
    pn.setBorder(BuBorders.EMPTY3333);
    pn.add(new JLabel(NbBundle.getMessage(ModellingListSectionAddTopComponent.class, "BrancheName.Name")));
    pn.add(cbBranches);
    pn.add(lbDistance);
    lbDistance.setText("                                         ");

    final List<EnumSectionType> types = EnumSectionType.getAvailablesSectionType();
    sectionTypeMap = ToStringInternationalizableTransformer.toi18nMap(types);
    cbTypeSection = new JComboBox(ToStringInternationalizableTransformer.toi18nMap(types).keySet().toArray());
    // SectionProfil par défaut
    cbTypeSection.setSelectedItem(EnumSectionType.EMHSectionProfil.geti18n());
    pn.add(new JLabel(NbBundle.getMessage(ModellingListSectionAddTopComponent.class, "TypeSection")));
    pn.add(cbTypeSection);
    pn.add(new JLabel());

    pn.add(new JLabel(NbBundle.getMessage(AddSectionInBranchePanelContent.class, "DistanceMax")));
    pn.add(tfDistance.getTxt());
    pn.add(tfDistance.getLabel());
    //listener
    cbBranches.addItemListener(this);
    tfDistance.addObserver(this);
  }

  protected void setSelectedBranche(final String brancheName) {
    if (branchesByName.containsKey(brancheName)) {
      cbBranches.setSelectedItem(brancheName);
    }
  }

  CatEMHBranche getSelectedBranche() {
    return branchesByName.get(cbBranches.getSelectedItem());
  }

  Double getDistance() {
    return (Double) tfDistance.getValue();
  }

  /**
   * @return le type de section sélectionné
   */
  EnumSectionType getSelectedSectionType() {
    if (sectionTypeMap != null && sectionTypeMap.containsKey(cbTypeSection.getSelectedItem())) {
      return sectionTypeMap.get(cbTypeSection.getSelectedItem());
    } else {
      return EnumSectionType.EMHSectionProfil;
    }
  }


  @Override
  public void update(final Observable o, final Object arg) {
    if (dialog != null) {
      final String errorMsg = tfDistance.getErrorMsg();
      dialog.setValid(errorMsg == null);
      dialog.getNotificationLineSupport().setErrorMessage(errorMsg);
    }
  }

  public boolean show() {
    dialog = new DialogDescriptor(pn, NbBundle.getMessage(AddSectionInBranchePanelContent.class, "AddSectionsInBanche.ActionName"));
    dialog.createNotificationLineSupport();
    tfDistance.validateData();
    update(null, null);
    updateBrancheDistance();
    //on change la version de persistence suite modification du contenu.
    return DialogHelper.showQuestion(dialog, getClass(), "2");
  }

  @Override
  public void itemStateChanged(final ItemEvent e) {
    if (e.getStateChange() == ItemEvent.SELECTED) {
      updateBrancheDistance();
    }
  }

  private void updateBrancheDistance() {
    final String brancheNom = (String) cbBranches.getSelectedItem();
    final EMHBrancheSaintVenant branche = branchesByName.get(brancheNom);
    if (branche == null) {
      lbDistance.setText(null);
      lbDistance.setToolTipText(null);
      return;
    }
    final double length = branche.getLength();
    final String txt = xp.format(length, DecimalFormatEpsilonEnum.COMPARISON)
            + xp.getNature().getUniteSuffixe();
    lbDistance.setText(ListBrancheContentNode.getLongueurDisplayName() + ": "
            + txt);
    lbDistance.setToolTipText(NbBundle.getMessage(AddSectionInBranchePanelContent.class, "Branche.InitialLength") + ": "
            + txt);
  }
}
