package org.fudaa.fudaa.crue.modelling.list;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.edition.bean.ListBrancheContent;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertyStringReadOnly;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;

import java.awt.datatransfer.Transferable;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.MissingResourceException;

/**
 * @author deniger
 */
public class ListBrancheContentNode extends AbstractListContentNode {
    final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);

    public ListBrancheContentNode(final CatEMHBranche branche, final ListBrancheContent content,
                                  final PerspectiveServiceModelling perspectiveService) {
        super(Children.LEAF, Lookups.fixed(branche, content), perspectiveService);
    }

    public static String getBrancheNameDisplay() throws MissingResourceException {
        return NbBundle.getMessage(ListBrancheContentNode.class, "BrancheName.Name");
    }

    public static String getNoeudAvalDisplayName() throws MissingResourceException {
        return NbBundle.getMessage(ListBrancheContentAddNode.class, "NoeudAval.Name");
    }

    public static String getNoeudAmontDisplayName() throws MissingResourceException {
        return NbBundle.getMessage(ListBrancheContentAddNode.class, "NoeudAmont.Name");
    }

    public static String getLongueurDisplayName() throws MissingResourceException {
        return NbBundle.getMessage(ListBrancheContentNode.class, "Longueur.Name");
    }

    @Override
    public void destroy() throws IOException {
    }

    @Override
    public PasteType getDropType(final Transferable t, final int action, final int index) {
        if (isEditMode()) {
            final Node[] dragged = DefaultNodePasteType.getMovedNodesInTransferable(t);
            return dragged==null?null:new DefaultNodePasteType(dragged, this);
        }
        return null;
    }

    @Override
    protected Sheet createSheet() {
        final Sheet sheet = Sheet.createDefault();
        final Sheet.Set set = Sheet.createPropertiesSet();
        final ListBrancheContent brancheContent = getLookup().lookup(ListBrancheContent.class);
        try {
            final Node.Property nom = new PropertyStringReadOnly(brancheContent.getNom(), ListCommonProperties.PROP_NOM,
                    getBrancheNameDisplay(),
                    null);
            PropertyCrueUtils.configureNoCustomEditor(nom);
            set.put(nom);
            //type
            final Node.Property type = new PropertyStringReadOnly(brancheContent.getType().geti18n(), ListCommonProperties.PROP_TYPE,
                    AbstractListContentNode.getTypeDisplay(),
                    null);
            PropertyCrueUtils.configureNoCustomEditor(type);
            set.put(type);
            //noeud amont
            final Node.Property noeudAmont = new PropertyStringReadOnly(brancheContent.getNoeudAmont(), ListBrancheContent.PROP_NOEUD_AMONT,
                    getNoeudAmontDisplayName(),
                    null);
            PropertyCrueUtils.configureNoCustomEditor(noeudAmont);
            set.put(noeudAmont);
            //noeud aval
            final Node.Property noeudAval = new PropertyStringReadOnly(brancheContent.getNoeudAval(), ListBrancheContent.PROP_NOEUD_AVAL,
                    getNoeudAvalDisplayName(),
                    null);
            PropertyCrueUtils.configureNoCustomEditor(noeudAval);
            set.put(noeudAval);

            //active
            final Node.Property active = new PropertySupportReflection(this, brancheContent, Boolean.TYPE,
                    ListCommonProperties.PROP_ACTIVE);
            active.setName(ListCommonProperties.PROP_ACTIVE);
            active.setDisplayName(AbstractListContentNode.getActiveDisplay());
            set.put(active);

            final CrueConfigMetier propDefinition = modellingScenarioService.getSelectedProjet().getPropDefinition();
            final NumberFormat formatter = propDefinition.getFormatter(CrueConfigMetierConstants.PROP_XP, DecimalFormatEpsilonEnum.COMPARISON);
            final String longeurFormatted;
            if (formatter != null) {
                longeurFormatted = formatter.format(brancheContent.getLongueur());
            } else {
                longeurFormatted = Double.toString(brancheContent.getLongueur());
            }

            //longueur
            final Node.Property longueur = new PropertyStringReadOnly(longeurFormatted, ListBrancheContent.PROP_LONGUEUR,
                    getLongueurDisplayName(),
                    null);
            PropertyCrueUtils.configureNoCustomEditor(longueur);
            set.put(longueur);

            //commentaire
            final Node.Property commentaire = new PropertySupportReflection(this, brancheContent, String.class,
                    ListCommonProperties.PROP_COMMENTAIRE);
            PropertyCrueUtils.setNullValueEmpty(commentaire);
            commentaire.setName(ListCommonProperties.PROP_COMMENTAIRE);
            commentaire.setDisplayName(AbstractListContentNode.getCommentDisplay());
            set.put(commentaire);
        } catch (final NoSuchMethodException ex) {
            Exceptions.printStackTrace(ex);
        }
        sheet.put(set);
        return sheet;
    }
}
