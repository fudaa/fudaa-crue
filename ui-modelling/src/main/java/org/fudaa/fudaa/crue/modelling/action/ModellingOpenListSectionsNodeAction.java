package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.list.ModellingListSectionTopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;
import org.openide.windows.WindowManager;

/**
 *
 * @author deniger
 */
public abstract class ModellingOpenListSectionsNodeAction extends AbstractModellingOpenListTopNodeAction {

  public ModellingOpenListSectionsNodeAction(String name) {

    super(name, ModellingListSectionTopComponent.MODE, ModellingListSectionTopComponent.TOPCOMPONENT_ID);
  }

//  @Override
//  protected AbstractModellingListTopComponent create() {
//    return new ModellingListSectionTopComponent();
//  }
  public static void open(EMHSousModele sousModele) {
    if (sousModele == null) {
      return;
    }
    ModellingOpenListSectionsNodeAction action = SystemAction.get(ModellingOpenListSectionsNodeAction.Reopen.class);
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(sousModele));
    action.performAction(new Node[]{node});
  }

  public static void openList(EMHSousModele sousModele, boolean reopen, String filter) {
    if (sousModele == null) {
      return;
    }
    ModellingOpenListSectionsNodeAction action = null;
    if (reopen) {
      action = SystemAction.get(ModellingOpenListSectionsNodeAction.Reopen.class);
    } else {
      action = SystemAction.get(ModellingOpenListSectionsNodeAction.NewFrame.class);
    }
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.singleton(sousModele));
    action.performAction(new Node[]{node});
    
    // on récupère un handle de la fenêtre ouverte
    ModellingListSectionTopComponent listSectionTC = (ModellingListSectionTopComponent) WindowManager.getDefault().findTopComponent(ModellingListSectionTopComponent.TOPCOMPONENT_ID);
    // passage du filtre sur la branche
    listSectionTC.setBrancheFilter(filter);
  }

  public static class Reopen extends ModellingOpenListSectionsNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenListSectionsNodeAction.class, "ModellingListSectionNodeAction.Name"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenListSectionsNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenListSectionsNodeAction.class, "ModellingListSectionNodeAction.NewFrame.Name"));
      setReopen(false);
    }
  }
}
