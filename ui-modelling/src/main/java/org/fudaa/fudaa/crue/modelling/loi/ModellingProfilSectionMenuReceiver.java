/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.loi;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.DonPrtGeoProfilSimplifier;
import org.fudaa.dodico.crue.edition.AlgoDonPrtGeoProfilSectionInverser;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.view.ItemVariableView;
import org.fudaa.fudaa.crue.loi.section.ProfilPopupMenuReceiver;
import org.fudaa.fudaa.crue.loi.section.ProfilSectionLoiUiController;
import org.fudaa.fudaa.crue.modelling.services.ModellingConfigService;
import org.fudaa.fudaa.crue.options.config.ModellingGlobalConfiguration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
class ModellingProfilSectionMenuReceiver extends ProfilPopupMenuReceiver {

  private InverseProfil inverseAction;
  private SimplifyProfil simplifyAction;
  EbliActionSimple importAction;
  private final ProfilSectionTopComponent topComponent;

  public ModellingProfilSectionMenuReceiver(ProfilSectionLoiUiController loiUiController, final ProfilSectionTopComponent topComponent) {
    super(loiUiController);
    this.topComponent = topComponent;
  }

  @Override
  protected void addImportItem() {
    importAction = new EbliActionSimple(NbBundle.getMessage(ModellingProfilSectionMenuReceiver.class, "tableImportAction"),
            EbliResource.EBLI.getToolIcon("crystal_importer"), "IMPORT") {
      @Override
      public void actionPerformed(ActionEvent _e) {
        topComponent.getLoiUIController().getTableGraphePanel().tableImport();
      }

    };
    importAction.setDefaultToolTip("<html>" + EbliLib.getS("Importer depuis un fichier Excel/csv") + "<br><b>" + EbliLib.getS(
            "Toutes les valeurs actuelles seront remplacées"));
    popupMenu.add(importAction);
  }

  @Override
  protected void addItems() {
    super.addItems();
    popupMenu.addSeparator();
    inverseAction = new InverseProfil();
    simplifyAction = new SimplifyProfil();
    popupMenu.add(inverseAction);
    popupMenu.add(simplifyAction);
  }

  @Override
  protected void updateItemStateBeforeShow() {
    super.updateItemStateBeforeShow();
    EGCourbe selectedComponent = getPanel().getGraphe().getSelectedComponent();
    final boolean editable = selectedComponent != null && selectedComponent.getModel().isModifiable();
    importAction.setEnabled(editable);
    inverseAction.setEnabled(editable);
    simplifyAction.setEnabled(editable);
  }

  protected class InverseProfil extends EbliActionSimple {

    public InverseProfil() {
      super(NbBundle.getMessage(ModellingProfilSectionMenuReceiver.class, "profileInverseAction"), null, "INVERSE");
    }

    @Override
    public void actionPerformed(ActionEvent _e) {
      ProfilSectionLoiUiController controller = getSectionController();
      DonPrtGeoProfilSection inEdition = controller.createEdited();
      final CrueConfigMetier ccm = controller.getCcm();
      AlgoDonPrtGeoProfilSectionInverser updater = new AlgoDonPrtGeoProfilSectionInverser(ccm);
      DonPrtGeoProfilSection inverse = updater.inverse(inEdition);
      inverse.setEmh(inEdition.getEmh());
      topComponent.currentProfilModified(inverse);
    }
  }

  protected class SimplifyProfil extends EbliActionSimple {

    public SimplifyProfil() {
      super(NbBundle.getMessage(ModellingProfilSectionMenuReceiver.class, "simplifySectionProfilAction"), null, "SIMPLIFY");
    }

    @Override
    public void actionPerformed(ActionEvent _e) {
      ItemVariable seuilVariable = getSectionController().getCcm().getProperty(CrueConfigMetierConstants.PROP_DEFAULT_SEUILSIMPLIFPROFILSECTION);
      ModellingConfigService modellingConfigService = Lookup.getDefault().lookup(ModellingConfigService.class);
      Double initSeuilSimplifProfilSection = modellingConfigService.getModellingGlobalConfiguration().getSeuilSimplifProfilSection();
      ItemVariableView view = new ItemVariableView.TextField(seuilVariable, DecimalFormatEpsilonEnum.PRESENTATION);
      view.setValue(initSeuilSimplifProfilSection);
      JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
      panel.add(new JLabel(NbBundle.getMessage(ModellingProfilSectionMenuReceiver.class, "seuilSimplifyProfilSectionLabel")));
      panel.add(view.getPanelComponent());
      boolean accepted = DialogHelper.showQuestionOkCancel(getTitle(), panel);
      if (!accepted || view.getValue() == null) {
        return;
      }
      Double seuil = (Double) view.getValue();
      if (seuil.compareTo(0d) <= 0) {
        return;
      }
      if (seuilVariable != null && !seuilVariable.getEpsilon().isSame(seuil, initSeuilSimplifProfilSection)) {
        final ModellingGlobalConfiguration modellingGlobalConfiguration = modellingConfigService.getModellingGlobalConfiguration();
        modellingGlobalConfiguration.setSeuilSimplifProfilSection(seuil);
        modellingConfigService.setModellingGlobalConfiguration(modellingGlobalConfiguration);
      }

      ProfilSectionLoiUiController controller = getSectionController();
      DonPrtGeoProfilSection inEdition = controller.createEdited();
      final CrueConfigMetier ccm = controller.getCcm();
      DonPrtGeoProfilSimplifier updater = new DonPrtGeoProfilSimplifier(seuil, ccm);
      DonPrtGeoProfilSection inverse = updater.simplify(inEdition);
      inverse.setEmh(inEdition.getEmh());
      topComponent.currentProfilModified(inverse);
    }
  }
}
