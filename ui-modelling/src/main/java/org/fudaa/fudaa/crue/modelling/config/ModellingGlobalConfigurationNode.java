package org.fudaa.fudaa.crue.modelling.config;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.modelling.node.AbstractModellingNodeFirable;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.fudaa.fudaa.crue.options.config.ModellingGlobalConfiguration;
import org.fudaa.fudaa.crue.options.config.ModellingGlobalConfigurationInfo;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class ModellingGlobalConfigurationNode extends AbstractModellingNodeFirable {

  final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);

  public ModellingGlobalConfigurationNode(ModellingGlobalConfiguration visuConfig, PerspectiveServiceModelling perspectiveServiceModelling) {
    super(Children.LEAF, Lookups.fixed(visuConfig, Lookup.getDefault().lookup(ModellingScenarioService.class).getSelectedProjet().getPropDefinition()), perspectiveServiceModelling);
    setDisplayName(NbBundle.getMessage(ModellingGlobalConfigurationNode.class, "ModellingGlobalConfigurationNode.DisplayName"));
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = new Sheet();
    CrueConfigMetier ccm = modellingScenarioService.getSelectedProjet().getPropDefinition();
    ModellingGlobalConfiguration lookup = getLookup().lookup(ModellingGlobalConfiguration.class);
    final Set set = ModellingGlobalConfigurationInfo.createSetDefaultLongeur(lookup);
    sheet.put(set);
    Node.Property property = set.get(ModellingGlobalConfiguration.PROP_DEFAULT_LITMINEUR);
    PropertyNodeBuilder.addValidateListener(property, ccm.getProperty(CrueConfigMetierConstants.PROP_DEFAULT_DX_XT), this);
    return sheet;
  }
}
