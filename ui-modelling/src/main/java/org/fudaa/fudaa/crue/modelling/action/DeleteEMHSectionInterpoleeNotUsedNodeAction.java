package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.edition.EditionDelete;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.modelling.ModellingTopComponentEMHEditable;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author deniger
 */
public class DeleteEMHSectionInterpoleeNotUsedNodeAction extends AbstractEditNodeAction {
  public DeleteEMHSectionInterpoleeNotUsedNodeAction() {
    super(NbBundle.getMessage(DeleteEMHSectionInterpoleeNotUsedNodeAction.class, "DeleteSectionInterpoleeAction.Title"));
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    return !findEMHToDelete(activatedNodes).isEmpty();
  }

  private List<EMH> findEMHToDelete(final Node[] activatedNodes) {
    EMHScenario scenario = findScenario(activatedNodes);
    if (scenario != null) {
      return new ArrayList<>(EditionDelete.findSectionInterpoleeToDelete(scenario));
    }
    return Collections.emptyList();
  }

  private EMHScenario findScenario(final Node[] activatedNodes) {
    if (activatedNodes.length > 0) {
      final Node[] nodes = activatedNodes[0].getChildren().getNodes();
      if (nodes != null && nodes.length != 0) {
        final EMH emh = nodes[0].getLookup().lookup(EMH.class);
        return EMHHelper.getScenario(emh);
      }
    }
    return null;
  }

  @Override
  protected void performAction(final Node[] activatedNodes) {

    final TopComponent activated = WindowManager.getDefault().getRegistry().getActivated();
    final EMHScenario scenario = findScenario(activatedNodes);
    if (scenario != null && activated instanceof ModellingTopComponentEMHEditable) {
      final List<CatEMHSection> sectionInterpoleeToDelete = EditionDelete.findSectionInterpoleeToDelete(scenario);
      if (DeleteSectionInterpoleeNotUsedHelper.deleteSectionAccepted(sectionInterpoleeToDelete)) {
        ((ModellingTopComponentEMHEditable) activated).deleteEMH(new ArrayList<>(sectionInterpoleeToDelete), false);
      }
    }
  }
}
