package org.fudaa.fudaa.crue.modelling.services;

import java.util.concurrent.Callable;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.planimetry.save.PlanimetryGisSaver;
import org.openide.util.Lookup;

/**
 *
 * @author deniger
 */
public class ModellingSaveGeoLocCallable implements Callable<CtuluLogGroup> {

  final ModellingScenarioVisuService modellingScenarioVisuService = Lookup.getDefault().lookup(ModellingScenarioVisuService.class);
  final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
          ModellingScenarioModificationService.class);

  @Override
  public CtuluLogGroup call() throws Exception {
    if (!modellingScenarioModificationService.isGeoLocModified()) {
      return null;
    }
    ModellingScenarioService modellingScenarioService = modellingScenarioVisuService.getModellingScenarioService();
    if (!modellingScenarioService.getSelectedProjet().getInfos().isDirOfConfigDefined()) {
      modellingScenarioModificationService.clearGeoLocModifiedState();
      return null;
    }
    PlanimetryGisSaver saver = new PlanimetryGisSaver(
            modellingScenarioVisuService.getPlanimetryVisuPanel().getPlanimetryController());
    saver.save(modellingScenarioService.getSelectedProjet().getInfos().getDirOfConfig());
    modellingScenarioModificationService.clearGeoLocModifiedState();
    CtuluLogGroup gr=new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    gr.setDescription("save.geoloc.done");
    return gr;
  }
}
