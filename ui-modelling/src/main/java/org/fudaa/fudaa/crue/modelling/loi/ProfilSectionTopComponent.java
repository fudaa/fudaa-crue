package org.fudaa.fudaa.crue.modelling.loi;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuResource;
import gnu.trove.TLongIntHashMap;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.gui.ExportTableCommentSupplier;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.edition.*;
import org.fudaa.dodico.crue.edition.EditionDelete.RemoveBilan;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.dodico.crue.validation.ValidateAndRebuildProfilSection;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.fudaa.crue.common.action.ExportImageAction;
import org.fudaa.fudaa.crue.common.action.ExportImageToClipboardAction;
import org.fudaa.fudaa.crue.common.helper.ComboBoxHelper;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.loi.CourbeViewConfigurationButtonUI;
import org.fudaa.fudaa.crue.loi.CourbeViewConfigurationTarget;
import org.fudaa.fudaa.crue.loi.ViewCourbeManager;
import org.fudaa.fudaa.crue.loi.common.LoiPopupMenuReceiver;
import org.fudaa.fudaa.crue.loi.section.ProfilSectionLoiUiController;
import org.fudaa.fudaa.crue.loi.section.ProfilSectionOpenFrtTarget;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ModellingConfigService;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.fudaa.fudaa.crue.study.services.TableExportCommentSupplier;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.*;
import java.util.List;

/**
 * Editeur de lois.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.loi//ProfilSectionTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = ProfilSectionTopComponent.TOPCOMPONENT_ID, persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ProfilSectionTopComponent.MODE, openAtStartup = false)
public final class ProfilSectionTopComponent extends AbstractModellingLoiTopComponent implements CourbeViewConfigurationTarget,
        ProfilSectionOpenFrtTarget, ExportTableCommentSupplier {
    public static final String PROPERTY_INIT_UID = "initUid";
    public static final String MODE = "loi-profilSectionEditor"; //NOI18N
    public static final String TOPCOMPONENT_ID = "ProfilSectionTopComponent"; //NOI18N
    private final ProfilSectionLoiUiController profilUiController;
    private final JTextField txtCommentaire;
    private final JTextField txtNom;
    private final JLabel lbNomValidation;
    private final JLabel lbCreationSection;
    private final ViewCourbeManager viewCourbeManager;
    protected final String initName;
    /**
     * Contient la liste des profils dans l'ordre
     */
    List<EMHSectionProfil> profils;
    SousModeleBrancheComponent sousModeleBranches;
    SousModeleBrancheCreationComponent.CreationData createData;
    /**
     * permet de sélectionner le profil précédent
     */
    private JButton btUp;
    /**
     * permet de sélectionner le profil suivant
     */
    private JButton btDown;
    private Action delete;
    private Action duplicate;
    private Action create;
    private final ModellingConfigService modellingConfigService = Lookup.getDefault().lookup(ModellingConfigService.class);
    /**
     * Pour chaque uid de la liste profils donne la position dans la liste.
     */
    private TLongIntHashMap positionsByProfils;
    private JComboBox cbSections;
    private EMHSectionProfil currentSection;
    private boolean isNewSection;
    private boolean editable;

    public ProfilSectionTopComponent() {
        initComponents();
        initName = NbBundle.getMessage(ProfilSectionTopComponent.class, "CTL_" + TOPCOMPONENT_ID);
        setName(initName);
        setToolTipText(NbBundle.getMessage(ProfilSectionTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
        profilUiController = new ProfilSectionLoiUiController(modellingConfigService) {
            @Override
            public LoiPopupMenuReceiver createLoiPopupReceiver() {
                return new ModellingProfilSectionMenuReceiver(this, ProfilSectionTopComponent.this);
            }
        };
        profilUiController.setProfilSectionOpenFrtTarget(this);
        profilUiController.setUseSectionName(true);

        profilUiController.getPanel().setPreferredSize(new Dimension(550, 350));
        final JLabel labelValidating = profilUiController.getLabelValidating();
        final JPanel pnTable = new JPanel(new BorderLayout(5, 5));
        pnTable.add(labelValidating, BorderLayout.SOUTH);
        pnTable.add(profilUiController.getTableGraphePanel());
        viewCourbeManager = new ViewCourbeManager(profilUiController.getPanel());
        viewCourbeManager.getConfig().setDisplayLabels(false);
        viewCourbeManager.getConfig().setDisplayTitle(false);

        //pour exporter les images:
        final JPanel toolbar = profilUiController.getToolbar();
        toolbar.add(new ExportImageAction(viewCourbeManager).buildToolButton(EbliComponentFactory.INSTANCE));
        toolbar.add(new ExportImageToClipboardAction(viewCourbeManager).buildToolButton(EbliComponentFactory.INSTANCE));
        add(toolbar, BorderLayout.NORTH);

        final CourbeViewConfigurationButtonUI configManager = new CourbeViewConfigurationButtonUI(this);
        configManager.installDoubleClickListener(viewCourbeManager);
        final JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, viewCourbeManager.getPanel(), pnTable);
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(550);
        splitPane.setContinuousLayout(true);
        add(splitPane);

        final JPanel nomComment = new JPanel(new BuGridLayout(3, 10, 10));
        nomComment.add(new JLabel(NbBundle.getMessage(ProfilSectionTopComponent.class, "Nom.DisplayName")));
        final DocumentListener documentListener = super.createDocumentListener();
        final DocumentListener nameChanged = new DocumentListener() {
            @Override
            public void insertUpdate(final DocumentEvent e) {
                titleChanged();
            }

            @Override
            public void removeUpdate(final DocumentEvent e) {
                titleChanged();
            }

            @Override
            public void changedUpdate(final DocumentEvent e) {
                titleChanged();
            }
        };
        txtNom = new JTextField(30);
        lbNomValidation = new JLabel();
        nomComment.add(txtNom);
        nomComment.add(lbNomValidation);
        nomComment.add(new JLabel(NbBundle.getMessage(ProfilSectionTopComponent.class, "Commentaire.DisplayName")));
        txtCommentaire = new JTextField(30);
        txtCommentaire.setEditable(false);
        txtCommentaire.getDocument().addDocumentListener(documentListener);
        txtNom.getDocument().addDocumentListener(documentListener);
        txtNom.getDocument().addDocumentListener(nameChanged);
        nomComment.add(txtCommentaire);
        final JPanel pnInfo = new JPanel(new BorderLayout(5, 5));
        final JPanel pnFente = profilUiController.getFenteEditor().getPanel();
        pnFente.setBorder(BorderFactory.createEtchedBorder());
        pnInfo.add(pnFente);
        pnInfo.add(nomComment, BorderLayout.NORTH);
        final JPanel pnDecal = profilUiController.getPreviousCourbes().getDecalPanel();
        pnInfo.add(pnDecal, BorderLayout.SOUTH);
        createComboBoxes();
        final JPanel pnActions = createsPanelActions();
        final JPanel pnSouth = new JPanel(new BorderLayout(10, 10));
        pnSouth.add(pnActions, BorderLayout.WEST);
        pnSouth.setBorder(BorderFactory.createEtchedBorder());
        pnSouth.add(pnInfo);
        final JPanel pnValid = new JPanel(new BorderLayout());
        lbCreationSection = new JLabel(" ");
        pnValid.add(createCancelSaveButtons(lbCreationSection), BorderLayout.SOUTH);
        pnSouth.add(pnValid, BorderLayout.SOUTH);
        add(pnSouth, BorderLayout.SOUTH);
        pnSouth.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        setEditable(false);
    }

    @Override
    public void setChanged() {
    }

    @Override
    protected String getViewHelpCtxId() {
        return "vueEditionProfilSections";
    }

    @Override
    public List<String> getComments() {
        return new TableExportCommentSupplier(false).getComments(getName());
    }

    @Override
    protected EMHScenario getScenario() {
        return modellingScenarioModificationService.getModellingScenarioService().getScenarioLoaded();
    }

    protected void titleChanged() {
        lbNomValidation.setText(StringUtils.EMPTY);
        lbNomValidation.setIcon(null);
        lbNomValidation.setToolTipText(null);
        if (currentSection == null) {
            return;
        }
        setName(initName + BusinessMessages.ENTITY_SEPARATOR + txtNom.getText());
        updateTopComponentName();
        final String error = validateNom();
        if (error != null) {
            lbNomValidation.setToolTipText(error);
            lbNomValidation.setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.SEVERE));
        }
    }

    protected String validateNom() {
        final String nom = txtNom.getText();
        final String error = ValidationPatternHelper.isNameValidei18nMessage(nom, EnumCatEMH.SECTION);
        return error;
    }

    private void createActions() {
        delete = new EbliActionSimple(NbBundle.getMessage(ProfilSectionTopComponent.class, "Delete.DisplayName"), BuResource.BU.getIcon("enlever"),
                "DELETE") {
            @Override
            public void actionPerformed(final ActionEvent _e) {
                delete();
            }
        };
        delete.putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(ProfilSectionTopComponent.class, "Delete.Description"));

        duplicate = new EbliActionSimple(NbBundle.getMessage(ProfilSectionTopComponent.class, "Duplicate.DisplayName"), BuResource.BU.getIcon("dupliquer"),
                "DUPLICATE") {
            @Override
            public void actionPerformed(final ActionEvent _e) {
                duplicate();
            }
        };
        duplicate.putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(ProfilSectionTopComponent.class, "Duplicate.Description"));

        create = new EbliActionSimple(NbBundle.getMessage(ProfilSectionTopComponent.class, "Create.DisplayName"), BuResource.BU.getIcon("creer"), "CREATE") {
            @Override
            public void actionPerformed(final ActionEvent _e) {
                create();
            }
        };
        create.putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(ProfilSectionTopComponent.class, "Create.Description"));
    }

    private void sectionSelectionChanged() {
        if (updating) {
            return;
        }
        currentSection = (EMHSectionProfil) cbSections.getSelectedItem();
        updateWithSectionProfil();
    }

    private void brancheSelectionChanged() {
        if (updating) {
            return;
        }
        final CatEMHBranche branche = sousModeleBranches.getSelectedBranche();
        final EMHSousModele sousModele = getSelectedSousModele();
        List<EMHSectionProfil> sections = null;
        if (branche == null) {
            if (sousModeleBranches.isAllBrancheSelected()) {
                sections = EMHHelper.getAllEMHSectionProfil(sousModele);
            } else {
                sections = EMHHelper.getEMHSectionProfilWithoutBranches(sousModele);
            }
        } else {
            sections = EMHHelper.getEMHSectionProfil(branche, getCcm());
        }
        ComboBoxHelper.setDefaultModel(cbSections, sections);
        cbSections.setSelectedItem(null);
    }

    @Override
    public void setEditable(final boolean b) {
        super.setEditable(b);
        editable = b;
        profilUiController.setEditable(b);
        txtCommentaire.setEditable(b);
        txtNom.setEditable(b);
        delete.setEnabled(b);
        duplicate.setEnabled(b);
        create.setEnabled(b);
    }

    @Override
    protected String getDefaultTopComponentId() {
        return TOPCOMPONENT_ID;
    }

    @Override
    public void cancelModificationHandler() {
        if (isNewSection) {
            isNewSection = false;
            profilUiController.setProfilSection(null, null, null);
            currentSection = (EMHSectionProfil) cbSections.getSelectedItem();
        }
        createData = null;
        updateLabelCreation();
        super.reloadCourbeConfig();
        scenarioReloaded();
        setModified(false);
    }

    @Override
    protected void scenarioLoadedHandler() {
        updating = true;
        EMHSectionProfil oldSection = getCurrentProfil();
        if (oldSection != null) {
            oldSection = (EMHSectionProfil) getScenario().getIdRegistry().getEmh(oldSection.getUiId());
        }
        currentSection = null;
        EMHSousModele sousModeleToSelect = null;
        CatEMHBranche brancheToSelect = null;
        if (oldSection != null) {
            brancheToSelect = oldSection.getBranche();
            sousModeleToSelect = oldSection.getParent();
        }
        sousModeleBranches.updateValuesAndSelection(getScenario(), brancheToSelect, sousModeleToSelect);
        brancheToSelect = sousModeleBranches.getSelectedBranche();
        if (sousModeleToSelect != null) {
            List<EMHSectionProfil> sections = null;
            if (brancheToSelect == null) {
                sections = EMHHelper.getEMHSectionProfilWithoutBranches(sousModeleToSelect);
            } else {
                sections = EMHHelper.getEMHSectionProfil(brancheToSelect, getCcm());
            }
            cbSections.setModel(new DefaultComboBoxModel(sections.toArray(new EMHSectionProfil[0])));
        }
        buildListProfilSection();
        cbSections.setSelectedItem(oldSection);
        updating = false;
        currentSection = oldSection;
        if (!profilUiController.getFenteEditor().hasVariables()) {
            profilUiController.getFenteEditor().setCcm(getCcm());
        }
        updateWithSectionProfil();
        isNewSection = false;
    }

    @Override
    protected void componentOpenedHandler() {
        super.componentOpenedHandler();
        final Long init = (Long) getClientProperty(PROPERTY_INIT_UID);
        if (init != null) {
            putClientProperty(PROPERTY_INIT_UID, null);
            selectProfil(init);
        }
        this.profilUiController.getGraphe().setExportTableCommentSupplier(this);
    }

    protected void currentProfilModified(final DonPrtGeoProfilSection newState) {
        updating = true;
        final EMHSectionProfil profil = getCurrentProfil();
        final EMHSousModele sousModeleToUse = getSousModeleToUseForLoiController(profil);
        profilUiController.setProfilSection(newState, getCcm(), sousModeleToUse);
        initializeListeners();
        updating = false;
        setModified(true);
    }

    protected void updateWithSectionProfil() {
        final EMHSectionProfil profil = getCurrentProfil();
        updating = true;
        txtCommentaire.setText(StringUtils.EMPTY);
        txtNom.setText(StringUtils.EMPTY);
        final DonPrtGeoProfilSection profilViewed = profilUiController.getProfil();
        final DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection(profil);
        final EMHSousModele sousModeleToUse = getSousModeleToUseForLoiController(profil);
        profilUiController.setProfilSection(profilSection, getCcm(), sousModeleToUse);
        //mise à jour des courbe précédente:
        //changement de sélection:
        DonPrtGeoProfilSection nMoins1 = profilUiController.getPreviousCourbes().getNmoins1();
        DonPrtGeoProfilSection nMoins2 = profilUiController.getPreviousCourbes().getNmoins2();
        if (profilViewed != null && profilViewed.getEmh() != null && (profil == null || !profilViewed.getEmh().getUiId().equals(profil.getUiId()))) {
            nMoins1 = profilViewed;
            nMoins2 = profilUiController.getPreviousCourbes().getNmoins1();
        }
        if (nMoins1 != null) {
            if (nMoins1.getEmh() == null || !getScenario().getIdRegistry().containEMH(nMoins1.getEmh().getUiId())) {//supprime
                nMoins1 = null;
            }
        }
        if (nMoins2 != null) {
            if (nMoins2.getEmh() == null || !getScenario().getIdRegistry().containEMH(nMoins2.getEmh().getUiId())) {//supprime
                nMoins2 = null;
            }
        }
        profilUiController.setPreviousSections(nMoins1, nMoins2);
        if (profil != null) {
            txtCommentaire.setText(profilSection.getCommentaire());
            final String desc = StringUtils.defaultString(profil.getNom());
            txtNom.setText(desc);
            setName(initName + BusinessMessages.ENTITY_SEPARATOR + desc);
            initializeListeners();
        } else {
            setName(initName);
        }
        profilUiController.setEditable(editable);
        applyCourbeConfig();
        profilUiController.getGraphe().restore();
        profilUiController.getGraphe().restore();
        updating = false;
        setModified(false);
    }

    @Override
    protected void scenarioReloaded() {
        scenarioLoaded();
    }

    @Override
    protected ProfilSectionLoiUiController getLoiUIController() {
        return profilUiController;
    }

    @Override
    protected void scenarioUnloadedHandler() {
        currentSection = null;
        isNewSection = false;
        putClientProperty(PROPERTY_INIT_UID, null);
        profilUiController.setProfilSection(null, null, null);
        profilUiController.setPreviousSections(null, null);
        updating = true;
        profils = null;
        positionsByProfils = null;
        cbSections.setModel(new DefaultComboBoxModel());
        sousModeleBranches.clearComboBoxes();
        updating = false;
    }

    EMHSectionProfil getCurrentProfil() {
        return currentSection;
    }

    @Override
    public void setModified(final boolean modified) {
        super.setModified(modified);
        duplicate.setEnabled(!modified && editable);
        create.setEnabled(!modified && editable);
        cbSections.setEnabled(!modified);
        sousModeleBranches.setEnabled(!modified);
        btDown.setEnabled(!modified);
        btUp.setEnabled(!modified);
    }

    void delete() {
        if (currentSection == null) {
            return;
        }
        final boolean ok = DialogHelper.showQuestion(NbBundle.getMessage(ProfilSectionTopComponent.class, "ProfilSection.DeleteConfirmation.Message", txtNom.
                getText()));
        if (!ok) {
            return;
        }
        if (isNewSection) {
            currentSection = (EMHSectionProfil) cbSections.getSelectedItem();
            scenarioReloaded();
            isNewSection = false;
        } else {
            final RemoveBilan deleteBilan = EditionDelete.delete(Collections.singletonList(currentSection), getScenario(), false);
            if (deleteBilan.nbSectionRemoved == 1) {
                currentSection = null;
                profilUiController.setProfilSection(null, null, null);
                modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.DPTG));
                scenarioReloaded();
            }
            LogsDisplayer.displayBilan(deleteBilan);
        }
    }

    @Override
    public CrueConfigMetier getCcm() {
        return super.getCcm();
    }

    void create() {
        createData = SousModeleBrancheCreationComponent.showCreateDialog((String) create.getValue(Action.NAME), this);
        if (createData == null) {
            return;
        }
        updateLabelCreation();
        final EditionSectionCreator creator = new EditionSectionCreator(new EditionProfilCreator(new UniqueNomFinder(), modellingConfigService.
                getDefaultValues()));
        currentSection = creator.createEMHSectionProfilDetached(getCcm(), createData.sousModele);
        isNewSection = true;
        updateWithSectionProfil();
        setModified(true);
    }

    void duplicate() {
        if (currentSection == null) {
            return;
        }
        createData = SousModeleBrancheCreationComponent.showCreateDialog((String) create.getValue(Action.NAME), this);
        if (createData == null) {
            return;
        }
        updateLabelCreation();
        final UniqueNomFinder nomFinder = new UniqueNomFinder();
        final DonPrtGeoProfilSection oldProfilSection = DonPrtHelper.getProfilSection(currentSection);
        currentSection = new EMHSectionProfil(nomFinder.findNewName(EnumCatEMH.SECTION, getScenario()));
        final DonPrtGeoProfilSection duplicated = new DonPrtGeoProfilSection();
        duplicated.initDataFrom(oldProfilSection);
        currentSection.addInfosEMH(duplicated);
        isNewSection = true;
        updateWithSectionProfil();
        setModified(true);
    }
    //

    protected EMHSousModele getSelectedSousModele() {
        return sousModeleBranches.getSelectedSousModele();
    }

    @Override
    public boolean valideModificationDataHandler() {
        if (currentSection == null) {
            return false;
        }
        final CtuluLog validateLit = profilUiController.validateLit();
        if (validateLit.containsErrorOrSevereError()) {
            LogsDisplayer.displayError(validateLit, getName());
            return false;
        }
        final DonPrtGeoProfilSection cloned = profilUiController.createEdited();
        cloned.setCommentaire(txtCommentaire.getText());
        final EMHSousModele sousModele = getSelectedSousModele();
        final CtuluLog valideSection = new ValidateAndRebuildProfilSection(getCcm(), null).validateAndRebuildDonPrtGeoProfilSection(cloned, sousModele);
        final String error = validateNom();
        final String newName = txtNom.getText();
        boolean res = false;
        if (error != null) {
            valideSection.addSevereError(error);
        } else {
            final List<CatEMHSection> allSections = getScenario().getSections();
            if (!isNewSection) {
                allSections.remove(currentSection);
            }
            final Set<String> toSetOfId = TransformerHelper.toSetOfId(allSections);
            if (toSetOfId.contains(newName.toUpperCase())) {
                valideSection.addSevereError("validation.NameAlreadyUsed", txtNom.getText());
            }
        }
        if (!valideSection.containsErrorOrSevereError()) {
            final DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection(currentSection);
            profilSection.initDataFrom(cloned);
            currentSection.setNom(newName);
            EditionRename.propagateSectionRename(currentSection, newName);//renommer le profil associé.

            if (isNewSection) {
                //on ajoute la section au sous-modele et la branche
                final EditionCreateEMH creator = new EditionCreateEMH(modellingConfigService.getDefaultValues());
                creator.addSection(currentSection, createData.sousModele, createData.branche, createData.xp, getCcm());
            }
            res = true;
            modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.DPTG));
            isNewSection = false;
            createData = null;
            updateLabelCreation();
            setModified(false);
        }
        if (valideSection.isNotEmpty()) {
            LogsDisplayer.displayError(valideSection, getName());
        }
        return res;
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    void writeProperties(final java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
    }

    void readProperties(final java.util.Properties p) {
    }

    void selectProfil(final Long uid) {
        if (isModified() || uid == null) {
            return;
        }
        if (positionsByProfils != null && positionsByProfils.contains(uid)) {
            selectProfilAtPosition(positionsByProfils.get(uid));
        }
    }

    protected void buildListProfilSection() {
        profils = new ArrayList<>();
        final List<EMHSousModele> sousModeles = getScenario().getSousModeles();
        for (final EMHSousModele sousModele : sousModeles) {
            final List<CatEMHBranche> branches = sousModele.getBranches();
            for (final CatEMHBranche branche : branches) {
                EMHHelper.fillEMHSectionProfil(branche, profils, getCcm());
            }
            EMHHelper.fillWithEMHSectionProfilWithoutBranches(sousModele, profils);
        }
        positionsByProfils = new TLongIntHashMap();
        final int nb = profils.size();
        for (int i = 0; i < nb; i++) {
            positionsByProfils.put(profils.get(i).getUiId(), i);
        }
    }

    protected void createComboBoxes() {
        cbSections = new JComboBox();
        cbSections.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    sectionSelectionChanged();
                }
            }
        });
        final ObjetNommeCellRenderer objetNommeCellRenderer = new ObjetNommeCellRenderer();
        cbSections.setRenderer(objetNommeCellRenderer);
        sousModeleBranches = new SousModeleBrancheComponent();
        sousModeleBranches.getCbBranches().addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    brancheSelectionChanged();
                }
            }
        });
    }

    void selectNextProfil() {
        selectProfilFromDelta(1);
    }

    void selectPreviousProfil() {
        selectProfilFromDelta(-1);
    }

    @Override
    public void applyLabelsChanged() {
    }

    @Override
    public void applyTitleChanged() {
    }

    @Override
    public List<? extends Action> getAdditionalActions() {
        return Collections.emptyList();
    }

    @Override
    public ViewCourbeManager getManager() {
        return viewCourbeManager;
    }

    protected JPanel createsPanelActions() {

        btDown = new JButton(ImageUtilities.loadImageIcon("org/fudaa/fudaa/crue/modelling/icons/down.png", false));
        btUp = new JButton(ImageUtilities.loadImageIcon("org/fudaa/fudaa/crue/modelling/icons/up.png", false));
        btDown.setToolTipText(NbBundle.getMessage(ProfilSectionTopComponent.class, "ProfilSection.SelectNext.Tooltip"));
        btUp.setToolTipText(NbBundle.getMessage(ProfilSectionTopComponent.class, "ProfilSection.SelectPrevious.Tooltip"));
        btDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                selectNextProfil();
            }
        });
        btUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                selectPreviousProfil();
            }
        });
        btDown.setBorderPainted(false);
        btDown.setBackground(null);
        btDown.setContentAreaFilled(false);
        btDown.setBorder(null);
        btUp.setBorderPainted(false);
        btUp.setBorder(null);
        btUp.setContentAreaFilled(false);
        btUp.setBackground(null);
        final BuGridLayout buGridLayout = new BuGridLayout(2, 0, 5, true, true, false, false, false);
        buGridLayout.setXAlign(0f);
        final JPanel pnActions = new JPanel(buGridLayout);
        pnActions.add(sousModeleBranches.getCbSousModeles());
        pnActions.add(new JLabel());//pour remplir la 2eme colonne
        pnActions.add(sousModeleBranches.getCbBranches());
        pnActions.add(new JLabel());
        pnActions.add(cbSections);
        final JPanel pnUpBottom = new JPanel(new BuGridLayout(2, 0, 0));
        pnUpBottom.add(btDown);
        pnUpBottom.add(btUp);
        pnActions.add(pnUpBottom);

        pnActions.add(new JSeparator());
        pnActions.add(new JLabel());
        createActions();
        pnActions.add(new JButton(create));
        pnActions.add(new JLabel());
        pnActions.add(new JButton(duplicate));
        pnActions.add(new JLabel());
        pnActions.add(new JSeparator());
        pnActions.add(new JLabel());
        pnActions.add(new JButton(delete));
        pnActions.add(new JLabel());
        return pnActions;
    }

    private void selectProfilFromDelta(final int delta) {
        if (CollectionUtils.isEmpty(profils)) {
            return;
        }
        if (!isNewSection) {
            final EMHSectionProfil currentProfil = getCurrentProfil();
            int pos = 0;
            if (currentProfil != null) {
                final int currentPos = positionsByProfils.get(currentProfil.getUiId());
                pos = currentPos + delta;
            }
            if (pos >= profils.size()) {
                return;
            }
            if (pos < 0) {
                return;
            }
            selectProfilAtPosition(pos);
        }
    }

    public void updateLabelCreation() {
        if (createData == null) {
            lbCreationSection.setText(" ");
            lbCreationSection.setToolTipText(null);
        } else {
            if (createData.branche != null) {
                final String xp = TransformerEMHHelper.formatFromPropertyName(CrueConfigMetierConstants.PROP_XP, createData.xp, getCcm(),
                        DecimalFormatEpsilonEnum.COMPARISON);
                final String xpCompare = TransformerEMHHelper.formatFromPropertyName(CrueConfigMetierConstants.PROP_XP, createData.xp, getCcm(),
                        DecimalFormatEpsilonEnum.COMPARISON);
                lbCreationSection.setText(NbBundle.
                        getMessage(ProfilSectionTopComponent.class, "CreateSectionInBranche.Label", createData.branche.getNom(), xp));
                lbCreationSection.setToolTipText(NbBundle.getMessage(ProfilSectionTopComponent.class, "CreateSectionInBranche.Label", createData.branche.
                        getNom(), xpCompare));
            } else {
                lbCreationSection.setText(NbBundle.getMessage(ProfilSectionTopComponent.class, "CreateSectionInSousModele.Label", createData.sousModele.
                        getNom()));
            }
        }
    }

    private void selectProfilAtPosition(final int pos) {
        final EMHSectionProfil current = profils.get(pos);
        final EMHSousModele parent = current.getParent();
        if (parent != sousModeleBranches.getSelectedSousModele()) {
            sousModeleBranches.getCbSousModeles().setSelectedItem(parent);
        }
        final CatEMHBranche branche = current.getBranche();
        final CatEMHBranche selectedBranche = sousModeleBranches.getSelectedBranche();
        if (selectedBranche != branche || branche == null) {
            sousModeleBranches.setSelectedBranche(branche);
        }
        cbSections.setSelectedItem(current);
    }

    private EMHSousModele getSousModeleToUseForLoiController(final EMHSectionProfil profil) {
        EMHSousModele sousModeleToUse = null;
        if (isNewSection) {
            sousModeleToUse = createData.sousModele;
        } else {
            sousModeleToUse = profil == null ? null : profil.getParent();
        }
        return sousModeleToUse;
    }

    @Override
    public void openFrt(final DonFrt frt) {
        ModellingOpenDFRTNodeAction.open(profilUiController.getSousModeleParent().getUiId(), frt.getNom(), false);
    }

    @Override
    public boolean validateBeforeOpenFrtEditor() {
        return true;
    }

    protected void initializeListeners() {
        final Observer observer = new Observer() {
            @Override
            public void update(final Observable o, final Object arg) {
                setModified(true);
            }
        };
        profilUiController.getLoiModel().addObserver(observer);
        profilUiController.getFenteEditor().deleteObservers();
        profilUiController.getFenteEditor().addObserver(observer);
    }
}
