/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.emh;

import com.memoire.bu.BuGridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.edition.bean.BrancheEditionContent;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtBrancheNiveauxAssocies;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.common.view.ItemVariableView;
import org.fudaa.fudaa.crue.modelling.loi.ModellingOpenDcspLoiNodeAction;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.explorer.ExplorerManager;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 * Branche Type 2
 *
 * @author fred
 */
public class ModellingEMHBrancheNiveauxAssociesPanel extends JPanel implements ModellingEMHBrancheSpecificEditor, Observer {

  final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(
          ModellingScenarioService.class);
  PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
  final ModellingEMHBrancheTopComponent parent;
  final List<ItemVariableView> properties;

  ModellingEMHBrancheNiveauxAssociesPanel(CatEMHBranche branche, ModellingEMHBrancheTopComponent parent) {
    super(new BuGridLayout(2, 10, 10));
    this.parent = parent;
    DonCalcSansPrtBrancheNiveauxAssocies dcsp = EMHHelper.selectFirstOfClass(branche.getInfosEMH(), DonCalcSansPrtBrancheNiveauxAssocies.class);
    if (dcsp == null) {
      dcsp = new DonCalcSansPrtBrancheNiveauxAssocies(getCcm());
    }
    properties = ItemVariableView.create(DecimalFormatEpsilonEnum.COMPARISON,dcsp, getCcm(), DonCalcSansPrtBrancheNiveauxAssocies.PROP_QLIMINF,
            DonCalcSansPrtBrancheNiveauxAssocies.PROP_QLIMSUP);
    for (ItemVariableView propertyEditorPanel : properties) {
      propertyEditorPanel.addObserver(this);
      add(new JLabel(propertyEditorPanel.getLabelName()));
      add(propertyEditorPanel.getPanelComponent());
    }
    add(new JLabel(NbBundle.getMessage(ModellingEMHBrancheNiveauxAssociesPanel.class, "LoiZavZam.Label.DisplayName")));
    JButton bt = new JButton(NbBundle.getMessage(ModellingEMHBrancheNiveauxAssociesPanel.class, "LoiZavZam.Button.DisplayName"));
    bt.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        openLoi();
      }
    });
    add(bt);
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return null;
  }

  public void openLoi() {
    ModellingOpenDcspLoiNodeAction.open(parent.getEMHUid(), EnumTypeLoi.LoiZavZam, false);
  }

  @Override
  public void update(Observable o, Object arg) {
    parent.setModified(true);
  }

  private CrueConfigMetier getCcm() {
    return modellingScenarioService.getSelectedProjet().getPropDefinition();
  }

  @Override
  public void setEditable(boolean editable) {
    for (ItemVariableView object : properties) {
      object.setEditable(editable);

    }
  }

  @Override
  public void fillWithData(BrancheEditionContent content) {
    DonCalcSansPrtBrancheNiveauxAssocies dcsp = new DonCalcSansPrtBrancheNiveauxAssocies(getCcm());
    ItemVariableView.transferToBean(dcsp, properties);
    content.setDcsp(dcsp);
  }

  @Override
  public JComponent getComponent() {
    return this;
  }
}
