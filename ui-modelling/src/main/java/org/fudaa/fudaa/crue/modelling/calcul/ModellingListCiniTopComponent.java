package org.fudaa.fudaa.crue.modelling.calcul;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.edition.EditionDPTIChanged;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.metier.emh.DonPrtCIni;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.DisableSortHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.node.NodeChildrenHelper;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.modelling.calcul.importer.CiniParserAndImporter;
import org.fudaa.fudaa.crue.modelling.list.AbstractListContentNode;
import org.fudaa.fudaa.crue.modelling.list.ListNoeudContentNode;
import org.fudaa.fudaa.crue.modelling.listener.ExplorerManagerListenerHelper;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Top component which displays something.
 */
@TopComponent.Description(preferredID = ModellingListCiniTopComponent.TOPCOMPONENT_ID, iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png",
    persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ModellingListCiniTopComponent.MODE, openAtStartup = false, position = 0)
public final class ModellingListCiniTopComponent extends AbstractModeleTopComponent implements ExplorerManager.Provider {
  //attention le mode modelling-listNoeuds doit être déclaré dans le projet branding (layer.xml)
  public static final String MODE = "modelling-listCini";
  public static final String TOPCOMPONENT_ID = "ModellingListCiniTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  private JTextField jMethodInterpolation;
  OutlineView outlineView;
  final transient ExplorerManagerListenerHelper helper;
  private boolean editable;
  private final transient DisableSortHelper sortDisabler;

  public ModellingListCiniTopComponent() {
    initComponents();
    createComponent(AbstractListContentNode.getEMHDisplay());
    setName(NbBundle.getMessage(ModellingListCiniTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingListCiniTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    ActionMap map = getActionMap();
    helper = new ExplorerManagerListenerHelper(this);
    associateLookup(ExplorerUtils.createLookup(getExplorerManager(), map));
    sortDisabler = new DisableSortHelper(outlineView);
  }

  @Override
  public void setEditable(boolean b) {
    this.editable = b;
    helper.setEditable(b);
    sortDisabler.setDisableSort(b);
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueCini";
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return helper.getExplorerManager();
  }

  private void createComponent(String firstColumnName) {
    outlineView = new OutlineView(firstColumnName);
    outlineView.getOutline().setColumnHidingAllowed(false);
    outlineView.getOutline().setRootVisible(false);
    outlineView.setNodePopupFactory(new ModellingListCiniTopComponentPopupFactory(this));
    jContainerName = new JTextField();
    jContainerName.setEditable(false);
    jContainerName.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(3, 2, 5, 2),
        jContainerName.getBorder()));
    setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 3));
    add(buildNorthPanel(), BorderLayout.NORTH);
    add(outlineView);
    add(createCancelSaveButtons(), BorderLayout.SOUTH);
    installPropertyColumns();
  }

  @Override
  protected JComponent buildCenterPanel(EMHScenario scenario, JComponent oldCenter) {
    helper.cleanListener();
    getExplorerManager().setRootContext(Node.EMPTY);
    jMethodInterpolation.setText(getModele().getOrdPrtCIniModeleBase().getMethodeInterpol().geti18n());
    List<? extends Node> createNodes = createNodes(getModele());
    AbstractNode root = new AbstractNode(NodeChildrenHelper.createChildren(createNodes), Lookups.singleton(getModele()));
    getExplorerManager().setRootContext(root);
    helper.addListener();
    EventQueue.invokeLater(() -> NodeHelper.expandAll(getExplorerManager(), outlineView));

    return outlineView;
  }

  @Override
  protected void scenarioUnloaded() {
    helper.cleanListener();
    super.scenarioUnloaded();
  }

  @Override
  protected void readPreferences() {
    DialogHelper.readInPreferences(outlineView, "outlineView", getClass());
  }

  @Override
  protected void writePreferences() {
    DialogHelper.writeInPreferences(outlineView, "outlineView", getClass());
  }

  @Override
  protected JPanel buildNorthPanel() {
    JPanel res = super.buildNorthPanel();
    jMethodInterpolation = new JTextField();
    jMethodInterpolation.setEditable(false);
    res.add(new JLabel(NbBundle.getMessage(ModellingListCiniTopComponent.class, "MethodInterpolation.DisplayName")));
    res.add(jMethodInterpolation);
    return res;
  }

  protected void installPropertyColumns() {
    outlineView.setPropertyColumns(
        CondInitNode.PROP_TYPE_EMH,
        ListNoeudContentNode.getTypeEMHDisplay(),
        ListCommonProperties.PROP_TYPE,
        AbstractListContentNode.getTypeDisplay(),
        CiniParserAndImporter.PROP_VALUE,
        CondInitNode.getValueDisplay(),
        CondInitNode.PROP_UNITE,
        CondInitNode.getUnitDisplay());
  }

  @Override
  public void cancelModificationHandler() {
    //on recharge le tout
    scenarioReloaded();
    setModified(false);
  }

  @Override
  public void valideModificationHandler() {
    Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    List<Pair<EMH, DonPrtCIni>> toModify = new ArrayList<>();
    Set<String> emhDone = new HashSet<>();
    for (Node node : nodes) {
      Node[] emhNodes = node.getChildren().getNodes();
      for (Node emhNode : emhNodes) {
        EMH emh = emhNode.getLookup().lookup(EMH.class);
        if (!emhDone.contains(emh.getNom())) {
          emhDone.add(emh.getNom());
          DonPrtCIni content = emhNode.getLookup().lookup(DonPrtCIni.class);
          toModify.add(new Pair<>(emh, content));
        }
      }
    }
    EditionDPTIChanged change = new EditionDPTIChanged();
    CtuluLog res = change.apply(toModify, getCcm());
    if (res.isNotEmpty()) {
      LogsDisplayer.displayError(res, getName());
    }
    if (!res.containsErrorOrSevereError()) {
      modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.DPTI));
      setModified(false);
    }
  }

  protected List<? extends Node> createNodes(EMH in) {
    CondInitNodeFactory factory = new CondInitNodeFactory();
    return factory.build((EMHModeleBase) in);
  }

  @Override
  protected void scenarioReloaded() {
    scenarioLoaded();
    repaint();
    revalidate();
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables

  boolean isEditable() {
    return editable;
  }
}
