package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.list.ModellingListBrancheAddTopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class ModellingOpenListBranchesAddNodeAction extends AbstractModellingOpenListTopNodeAction {

  public static String getAddNodeActionName() {
    return NbBundle.getMessage(ModellingOpenListBranchesAddNodeAction.class, "ModellingListBranchesAddNodeAction.Name");
  }

  public ModellingOpenListBranchesAddNodeAction() {

    super(getAddNodeActionName(), ModellingListBrancheAddTopComponent.MODE, ModellingListBrancheAddTopComponent.TOPCOMPONENT_ID);
    setReopen(false);
  }

  public static void open(EMHSousModele sousModele) {
    if (sousModele == null) {
      return;
    }
    ModellingOpenListBranchesAddNodeAction action = SystemAction.get(ModellingOpenListBranchesAddNodeAction.class);
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(sousModele));
    action.performAction(new Node[]{node});
  }
}
