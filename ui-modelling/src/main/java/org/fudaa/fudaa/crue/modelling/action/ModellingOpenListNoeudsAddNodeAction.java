package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.list.ModellingListNoeudAddTopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class ModellingOpenListNoeudsAddNodeAction extends AbstractModellingOpenListTopNodeAction {

  public static String getAddNodeActionName() {
    return NbBundle.getMessage(ModellingOpenListNoeudsAddNodeAction.class, "ModellingListNoeudsAddNodeAction.Name");
  }

  public ModellingOpenListNoeudsAddNodeAction() {

    super(getAddNodeActionName(), ModellingListNoeudAddTopComponent.MODE, ModellingListNoeudAddTopComponent.TOPCOMPONENT_ID);
    setReopen(false);
  }

  public static void open(EMHSousModele sousModele) {
    if (sousModele == null) {
      return;
    }
    ModellingOpenListNoeudsAddNodeAction action = SystemAction.get(ModellingOpenListNoeudsAddNodeAction.class);
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(sousModele));
    action.performAction(new Node[]{node});
  }
}
