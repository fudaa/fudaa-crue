package org.fudaa.fudaa.crue.modelling.loi;

import javax.swing.Action;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.action.*;
import org.fudaa.fudaa.crue.modelling.perspective.ActiveModelisation;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View", id = "org.fudaa.fudaa.crue.modelling.ModellingOpenDFRTAction")
@ActionRegistration(displayName = "#ModellingOpenDFRTNodeAction.DisplayName")
@ActionReferences({
  @ActionReference(path = ActiveModelisation.ACTIONS_MODELLING_VIEWS, position = 14, separatorBefore = 13)
})
public final class ModellingOpenDFRTAction extends AbstractModellingSousModeleOpenAction {

  public ModellingOpenDFRTAction() {
    putValue(Action.NAME, NbBundle.getMessage(ModellingOpenDFRTAction.class, "ModellingOpenDFRTNodeAction.DisplayName"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingOpenDFRTAction();
  }

  @Override
  protected void doAction(EMHSousModele sousModele) {
    ModellingOpenDFRTNodeAction.open(sousModele.getUiId(), true);
  }
}
