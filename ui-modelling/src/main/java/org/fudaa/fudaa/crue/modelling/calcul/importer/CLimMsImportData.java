/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul.importer;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Frederic Deniger
 */
public class CLimMsImportData {

  private final List<CLimMsImportLine> calculs = new ArrayList<>();
  private final List<String> emhs = new ArrayList<>();

  public List<CLimMsImportLine> getCalculs() {
    return calculs;
  }

  public List<String> getEmhs() {
    return emhs;
  }


}
