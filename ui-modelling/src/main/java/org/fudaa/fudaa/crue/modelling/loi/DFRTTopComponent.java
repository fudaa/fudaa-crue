package org.fudaa.fudaa.crue.modelling.loi;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuVerticalLayout;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.gui.ExportTableCommentSupplier;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByIdComparator;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.EMHFactory;
import org.fudaa.dodico.crue.metier.helper.DonFrtHelper;
import org.fudaa.dodico.crue.validation.ValidationHelperLoi;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.helper.ObjetWithIdCellRenderer;
import org.fudaa.fudaa.crue.common.helper.UserSaveAnswer;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.loi.common.CourbeMoyenneDeltaController;
import org.fudaa.fudaa.crue.loi.loiff.LoiUiController;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.fudaa.fudaa.crue.study.services.TableExportCommentSupplier;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.util.*;

/**
 * Editeur de lois.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.loi//DFRTTopComponent//EN",
  autostore = false)
@TopComponent.Description(preferredID = DFRTTopComponent.TOPCOMPONENT_ID, persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = DFRTTopComponent.MODE, openAtStartup = false)
public final class DFRTTopComponent extends AbstractModellingLoiTopComponent implements ItemListener, ExportTableCommentSupplier {
  public static final String MODE = "loi-dfrtEditor"; //NOI18N
  public static final String TOPCOMPONENT_ID = "DFRTTopComponent"; //NOI18N
  final LoiUiController loiUiController;
  final JTextField txtCommentaire;
  final JTextField txtNom;
  final JLabel lbNomValidation;
  final String initName;
  private JComboBox cbSousModele;

  public DFRTTopComponent() {
    initComponents();

    initName = NbBundle.getMessage(DFRTTopComponent.class, "CTL_" + TOPCOMPONENT_ID);
    setName(initName);
    setToolTipText(NbBundle.getMessage(DFRTTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    loiUiController = new LoiUiController();
    loiUiController.addExportImagesToToolbar();

    JPanel top=new JPanel(new BuVerticalLayout(5));
    top.add(createSousModelePanel());
    top.add(loiUiController.getToolbar());

    add(top, BorderLayout.NORTH);
    loiUiController.getPanel().setPreferredSize(new Dimension(550, 350));
    final JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, loiUiController.getPanel(), loiUiController.getTableGraphePanel());
    splitPane.setOneTouchExpandable(true);
    splitPane.setDividerLocation(550);
    splitPane.setContinuousLayout(true);
    add(splitPane);

    final JPanel nomComment = new JPanel(new BuGridLayout(3, 10, 10));
    nomComment.add(new JLabel(NbBundle.getMessage(DFRTTopComponent.class, "Nom.DisplayName")));
    final DocumentListener documentListener = super.createDocumentListener();
    final DocumentListener nameChanged = new DocumentListener() {
      @Override
      public void insertUpdate(final DocumentEvent e) {
        titleChanged();
      }

      @Override
      public void removeUpdate(final DocumentEvent e) {
        titleChanged();
      }

      @Override
      public void changedUpdate(final DocumentEvent e) {
        titleChanged();
      }
    };
    txtNom = new JTextField(30);
    lbNomValidation = new JLabel();
    nomComment.add(txtNom);
    nomComment.add(lbNomValidation);
    nomComment.add(new JLabel(NbBundle.getMessage(DFRTTopComponent.class, "Commentaire.DisplayName")));
    txtCommentaire = new JTextField(30);
    txtCommentaire.setEditable(false);
    txtCommentaire.getDocument().addDocumentListener(documentListener);
    txtNom.getDocument().addDocumentListener(documentListener);
    txtNom.getDocument().addDocumentListener(nameChanged);
    nomComment.add(txtCommentaire);
    final JPanel pnInfo = new JPanel(new BorderLayout(5, 5));
    pnInfo.add(loiUiController.getInfoController().getPanel());
    pnInfo.add(nomComment, BorderLayout.NORTH);
    pnInfo.add(createCancelSaveButtons(), BorderLayout.SOUTH);
    cbDonFrt = new JComboBox();
    cbDonFrt.addItemListener(this);
    cbDonFrt.setRenderer(new ObjetNommeCellRenderer());
    final JPanel pnActions = new JPanel(new BuGridLayout(1, 5, 5));
    pnActions.add(cbDonFrt);
    pnActions.add(new JSeparator());
    createActions();
    pnActions.add(new JButton(create));
    pnActions.add(new JButton(duplicate));
    pnActions.add(new JSeparator());
    pnActions.add(new JButton(delete));
    final JPanel pnSouth = new JPanel(new BorderLayout(10, 10));
    pnSouth.add(pnActions, BorderLayout.WEST);
    pnSouth.setBorder(BorderFactory.createEtchedBorder());
    pnSouth.add(pnInfo);
    pnSouth.add(new CourbeMoyenneDeltaController(loiUiController.getGraphe()).getPnInfo(), BorderLayout.EAST);
    add(pnSouth, BorderLayout.SOUTH);
    setEditable(false);
  }

  public JPanel createSousModelePanel() {
    cbSousModele = new JComboBox();
    cbSousModele.setRenderer(new ObjetNommeCellRenderer());
    cbSousModele.addItemListener(e -> {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        sousModeleSelectionChanged();
      }
    });

    JPanel pnSousModele = new JPanel(new BuGridLayout(2, 3, 3));
    pnSousModele.add(new JLabel(NbBundle.getMessage(DFRTTopComponent.class, "SousModele.Label")));
    pnSousModele.setBorder(BorderFactory.createEmptyBorder(5, 2, 5, 0));
    pnSousModele.add(cbSousModele);
    return pnSousModele;
  }

  /**
   * avant de changer le sous-modele sélectionné, on teste si modification et dans ce cas, il faut demander confirmation.
   */
  private void sousModeleSelectionChanged() {
    if (!isUpdating()) {
      if (cbSousModele.getSelectedItem() != null) {
        final Long uiId = ((EMHSousModele) cbSousModele.getSelectedItem()).getUiId();
        if (!uiId.equals(this.sousModeleUid)) {
          if (isModified()) {
            final UserSaveAnswer userSaveAnswer = askForSave(NbBundle.getMessage(getClass(), "ValidationVue.DontChangeSousModele"));
            if (UserSaveAnswer.CANCEL.equals(userSaveAnswer)) {
              cbSousModele.setSelectedItem(getScenario().getIdRegistry().getEmh(sousModeleUid));
            } else {
              reloadSousModeleUid(uiId, null);
            }
          } else {
            reloadSousModeleUid(uiId, null);
          }
        }
      }
    }
  }

  private Action delete;
  private Action duplicate;
  private Action create;

  @Override
  protected EMHScenario getScenario() {
    return modellingScenarioModificationService.getModellingScenarioService().getScenarioLoaded();
  }

  @Override
  protected String getDefaultTopComponentId() {
    return TOPCOMPONENT_ID;
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueEditionFrottements";
  }

  @Override
  public List<String> getComments() {
    return new TableExportCommentSupplier(false).getComments(getName());
  }

  protected void titleChanged() {
    lbNomValidation.setText(StringUtils.EMPTY);
    lbNomValidation.setIcon(null);
    lbNomValidation.setToolTipText(null);
    if (currentDonFrt == null) {
      return;
    }
    setName(initName + BusinessMessages.ENTITY_SEPARATOR + txtNom.getText());
    updateTopComponentName();
    final String error = validateNom();
    if (error != null) {
      lbNomValidation.setToolTipText(error);
      lbNomValidation.setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.SEVERE));
    }
  }

  Long sousModeleUid;
  String iniFrtName;

  public void setSousModeleUid(final Long sousModeleUid, final String initDonFrt) {
    this.sousModeleUid = sousModeleUid;
    this.iniFrtName = initDonFrt;
  }

  protected EMH updateSousModeleInCombobox() {
    if (getScenario() == null) {
      cbSousModele.setModel(new DefaultComboBoxModel());
      return null;
    }

    final List<EMHSousModele> sousModeles = getScenario().getSousModeles();
    EMH emh = getScenario().getIdRegistry().getEmh(sousModeleUid);
    cbSousModele.setModel(new DefaultComboBoxModel(sousModeles.toArray(new EMHSousModele[0])));
    cbSousModele.setSelectedItem(emh);
    return emh;
  }

  public void reloadSousModeleUid(final Long sousModeleUid, final String frtName) {
    if (!ObjectUtils.equals(this.sousModeleUid, sousModeleUid)) {
      this.sousModeleUid = sousModeleUid;
      this.iniFrtName = frtName;
      scenarioReloaded();
    } else if (frtName != null) {
      final int idx = getIdx(frtName);
      if (idx > 0) {
        cbDonFrt.setSelectedIndex(idx);
      }
    }
  }

  protected int getIdx(final String name) {
    final ComboBoxModel model = cbDonFrt.getModel();
    final int nb = model.getSize();
    for (int i = 0; i < nb; i++) {
      final DonFrt frt = (DonFrt) model.getElementAt(i);
      if (frt.getNom().equals(name)) {
        return i;
      }
    }
    return -1;
  }

  protected EMHSousModele getSousModele() {
    if (sousModeleUid == null) {
      return null;
    }
    return (EMHSousModele) modellingScenarioModificationService.getModellingScenarioService().getScenarioLoaded().getIdRegistry().getEmh(sousModeleUid);
  }

  protected String validateNom() {
    final String nom = txtNom.getText();
    return ValidationPatternHelper.isNameValidei18nMessage(nom, currentDonFrt.getLoi().getType());
  }

  private void createActions() {
    delete = new EbliActionSimple(NbBundle.getMessage(DFRTTopComponent.class, "Delete.DisplayName"), BuResource.BU.getIcon("enlever"), "DELETE") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        delete();
      }
    };
    delete.putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(DFRTTopComponent.class, "Delete.Description"));

    duplicate = new EbliActionSimple(NbBundle.getMessage(DFRTTopComponent.class, "Duplicate.DisplayName"), BuResource.BU.getIcon("dupliquer"), "DUPLICATE") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        duplicate();
      }
    };
    duplicate.putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(DFRTTopComponent.class, "Duplicate.Description"));

    create = new EbliActionSimple(NbBundle.getMessage(DFRTTopComponent.class, "Create.DisplayName"), BuResource.BU.getIcon("creer"), "CREATE") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        create();
      }
    };
    create.putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(DFRTTopComponent.class, "Create.Description"));
  }

  @Override
  public void itemStateChanged(final ItemEvent e) {
    if (e.getStateChange() == ItemEvent.SELECTED) {
      currentDonFrt = (DonFrt) cbDonFrt.getSelectedItem();
      updateWithDonFrt();
    }
  }

  @Override
  public void setEditable(final boolean b) {
    super.setEditable(b);
    editable = b;
    loiUiController.setEditable(b);
    txtCommentaire.setEditable(b);
    txtNom.setEditable(b);
    delete.setEnabled(b);
    duplicate.setEnabled(b);
    create.setEnabled(b);
  }

  @Override
  public LoiUiController getLoiUIController() {
    return loiUiController;
  }

  @Override
  public void cancelModificationHandler() {
    if (isNewDonFrt) {
      isNewDonFrt = false;
      currentDonFrt = (DonFrt) cbDonFrt.getSelectedItem();
    }
    scenarioReloaded();
    reloadCourbeConfig();
    setModified(false);
  }

  @Override
  protected void scenarioLoadedHandler() {
    final DonFrt oldLoi = getCurrentDonFrt();
    currentDonFrt = null;
    final EMHSousModele sousModele = getSousModele();
    if(sousModele==null){
      return;
    }
    final DonFrtConteneur dfrt = sousModele.getFrtConteneur();
    final List<DonFrt> lois = new ArrayList<>(dfrt.getListFrt());
    lois.sort(ObjetNommeByNameComparator.INSTANCE);
    cbDonFrt.setModel(new DefaultComboBoxModel(lois.toArray(new DonFrt[0])));
    cbDonFrt.setSelectedItem(null);
    int idx;
    if (iniFrtName != null) {
      idx = getIdx(iniFrtName);
    } else {
      idx = lois.indexOf(oldLoi);
    }
    if(idx<0 && !lois.isEmpty()){
      idx=0;
    }
    if (idx >= 0) {
      cbDonFrt.setSelectedIndex(idx);
    }else{
      updateWithDonFrt();
    }
    iniFrtName = null;
    isNewDonFrt = false;
    this.loiUiController.getGraphe().setExportTableCommentSupplier(this);
    updateSousModeleInCombobox();
  }

  private void updateWithDonFrt() {
    updating = true;
    final DonFrt donFrt = getCurrentDonFrt();
    txtCommentaire.setText(StringUtils.EMPTY);
    txtNom.setText(StringUtils.EMPTY);
    if (donFrt != null) {
      txtCommentaire.setText(donFrt.getLoi().getCommentaire());
      final String desc = StringUtils.defaultString(donFrt.getNom());
      txtNom.setText(desc);
      setName(initName + BusinessMessages.ENTITY_SEPARATOR + desc);
      loiUiController.setLoi(donFrt.getLoi(), getCcm(), false);
    }else{
      setName(initName);
      loiUiController.setLoi(null,getCcm(),false);
    }
    loiUiController.getLoiModel().addObserver((o, arg) -> setModified(true));
    loiUiController.setEditable(editable);
    applyCourbeConfig();
    updating = false;
    setModified(false);
  }

  @Override
  protected void scenarioReloaded() {
    scenarioLoaded();
  }

  @Override
  protected void scenarioUnloadedHandler() {
    loiUiController.setLoi(null, null, false);
    currentDonFrt = null;
    cbDonFrt.setModel(new DefaultComboBoxModel());
    close();
  }

  private final JComboBox cbDonFrt;
  private DonFrt currentDonFrt;
  private boolean isNewDonFrt;

  private DonFrt getCurrentDonFrt() {
    return currentDonFrt;
  }

  @Override
  public void setModified(final boolean modified) {
    super.setModified(modified);
    duplicate.setEnabled(!modified && editable);
    create.setEnabled(!modified && editable);
    cbDonFrt.setEnabled(!modified);
  }

  private boolean editable;

  private void delete() {
    final Set<CatEMHSection> usedBy = DonFrtHelper.getUsedBy(getSousModele(), new HashSet<>(Collections.singletonList(currentDonFrt))).get(currentDonFrt);

    if (CollectionUtils.isNotEmpty(usedBy)) {
      final ArrayList<CatEMHSection> sectionsUsingDonFrt = CollectionCrueUtil.getSortedList(usedBy, ObjetNommeByNameComparator.INSTANCE);
      final List<String> names = TransformerHelper.toNom(sectionsUsingDonFrt);
      final JPanel pn = new JPanel(new BorderLayout(5, 5));
      final String panelInfo = NbBundle.getMessage(DFRTTopComponent.class, "DeleteAbort.DonFrtIsUsed");
      pn.add(new JLabel(panelInfo), BorderLayout.NORTH);
      final JList list = new JList(names.toArray(new String[0]));
      pn.add(new JScrollPane(list));
      pn.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
      DialogHelper.showComponentError(pn, panelInfo);
      return;
    }
    final boolean ok = DialogHelper.showQuestion(NbBundle.getMessage(DFRTTopComponent.class, "DeleteConfirmation.Message", txtNom.getText()));
    if (ok) {
      final DonFrtConteneur dfrt = getSousModele().getFrtConteneur();
      if (!isNewDonFrt) {
        final DefaultComboBoxModel model = (DefaultComboBoxModel) cbDonFrt.getModel();
        final int idx = Math.max(0, model.getIndexOf(currentDonFrt) - 1);
        dfrt.removeFrt(currentDonFrt);
        currentDonFrt = null;

        List<DonFrt> listFrt = dfrt.getListFrt();
        if (idx < listFrt.size()) {
          dfrt.sort();
          listFrt = dfrt.getListFrt();
          currentDonFrt = listFrt.get(idx);
        }
        modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.DLHY));
      } else {
        //on sélectionne la loi disponible dans la combobox.
        currentDonFrt = (DonFrt) cbDonFrt.getSelectedItem();
        scenarioReloaded();
      }
      isNewDonFrt = false;
      setModified(false);
    }
  }

  private void create() {
    final List<EnumTypeLoi> loisType = new ArrayList<>();
    loisType.add(EnumTypeLoi.LoiZFK);
    loisType.add(EnumTypeLoi.LoiZFKSto);
    loisType.add(EnumTypeLoi.LoiZFN);
    loisType.add(EnumTypeLoi.LoiZFNSto);
    loisType.sort(ObjetNommeByIdComparator.INSTANCE);
    final JComboBox cb = new JComboBox(loisType.toArray(new EnumTypeLoi[0]));
    cb.setRenderer(new ObjetWithIdCellRenderer());
    cb.setSelectedItem(EnumTypeLoi.LoiZFK);
    final JPanel pn = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
    pn.add(new JLabel(NbBundle.getMessage(DFRTTopComponent.class, "CreateLoi.TypeLabel")));
    pn.add(cb);
    if (DialogHelper.showQuestionOkCancel((String) create.getValue(Action.NAME), pn) && cb.getSelectedItem() != null) {
      final EnumTypeLoi typeLoi = (EnumTypeLoi) cb.getSelectedItem();
      isNewDonFrt = true;
      currentDonFrt = EMHFactory.createDonFrt(typeLoi, CruePrefix.getPrefix(typeLoi));
      updateWithDonFrt();
      setModified(true);
    }
  }

  private void duplicate() {
    if (currentDonFrt == null) {
      return;
    }
    currentDonFrt = currentDonFrt.deepClone();
    currentDonFrt.setNom(CruePrefix.getPrefix(currentDonFrt.getLoi().getType()));
    isNewDonFrt = true;
    updateWithDonFrt();
    setModified(true);
  }

  @Override
  public boolean valideModificationDataHandler() {
    if (currentDonFrt == null) {
      return false;
    }
    boolean res = false;
    final DonFrt cloned = currentDonFrt.deepClone();
    final List<PtEvolutionFF> pts = loiUiController.getLoiModel().getPoints();
    cloned.getLoi().getEvolutionFF().setPtEvolutionFF(pts);
    cloned.getLoi().setCommentaire(txtCommentaire.getText());
    final CtuluLog valideLoi = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    ValidationHelperLoi.validateDonFrt(cloned, getCcm(), valideLoi, null);
    final String error = validateNom();
    if (error != null) {
      valideLoi.addSevereError(error);
    } else {
      final List<DonFrt> allLois = new ArrayList<>(getSousModele().getFrtConteneur().getListFrt());
      if (!isNewDonFrt) {
        allLois.remove(currentDonFrt);
      }
      final Set<String> toSetOfId = TransformerHelper.toSetOfId(allLois);
      if (toSetOfId.contains(txtNom.getText().toUpperCase())) {
        valideLoi.addSevereError("validation.NameAlreadyUsed", txtNom.getText());
      }
    }
    if (!valideLoi.containsErrorOrSevereError()) {
      currentDonFrt.getLoi().setCommentaire(txtCommentaire.getText());
      currentDonFrt.setNom(txtNom.getText());
      currentDonFrt.getLoi().getEvolutionFF().setPtEvolutionFF(pts);
      if (isNewDonFrt) {
        getSousModele().getFrtConteneur().addFrt(currentDonFrt);
      }
      res = true;
      modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.DFRT));
      isNewDonFrt = false;
    }
    if (valideLoi.isNotEmpty()) {
      LogsDisplayer.displayError(valideLoi, getName());
    }
    return res;
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  void writeProperties(final java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
  }

  void readProperties(final java.util.Properties p) {
  }

  void selectLoi(final Loi loiToActivate) {
    if (isModified()) {
      return;
    }
    cbDonFrt.setSelectedItem(loiToActivate);
  }
}
