/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.loi;

import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.fudaa.crue.common.helper.ComboBoxHelper;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class SousModeleBrancheComponent {

  static final CatEMHBranche NULL_BRANCHE = new CatEMHBranche(" ") {
    @Override
    public EnumBrancheType getBrancheType() {
      throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public EMH copyShallowFirstLevel() {
      return this;
    }

  };
  private static final CatEMHBranche ALL_BRANCHE = new CatEMHBranche(NbBundle.getMessage(SousModeleBrancheComponent.class, "AllBranche")) {
    @Override
    public EnumBrancheType getBrancheType() {
      throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public EMH copyShallowFirstLevel() {
      return this;
    }
  };
  private final JComboBox cbBranches;
  private final JComboBox cbSousModeles;

  public SousModeleBrancheComponent() {
    final ObjetNommeCellRenderer objetNommeCellRenderer = new ObjetNommeCellRenderer();
    cbBranches = new JComboBox();
    cbBranches.setRenderer(objetNommeCellRenderer);

    cbSousModeles = new JComboBox();
    cbSousModeles.setRenderer(objetNommeCellRenderer);
    cbSousModeles.addItemListener(e -> {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        sousModeleSelectionChanged();
      }
    });

  }

  public boolean isAllBrancheSelected() {
    final CatEMHBranche branche = (CatEMHBranche) cbBranches.getSelectedItem();
    return ALL_BRANCHE == branche;
  }

  protected CatEMHBranche getSelectedBranche() {
    CatEMHBranche branche = (CatEMHBranche) cbBranches.getSelectedItem();
    if (branche == SousModeleBrancheComponent.NULL_BRANCHE || branche == SousModeleBrancheComponent.ALL_BRANCHE) {
      branche = null;
    }
    return branche;
  }
  private boolean updating;

  void updateValuesAndSelection(final EMHScenario scenario, final CatEMHBranche initBrancheToSelect, final EMHSousModele initSousModelToSelect) {
    updating = true;
    final List<EMHSousModele> sousModelesList = scenario.getSousModeles();
    final EMHSousModele[] sousModelesArray = sousModelesList.toArray(new EMHSousModele[0]);
    EMHSousModele sousModeleToSelect = initSousModelToSelect;
    final IdRegistry idRegistry = scenario.getIdRegistry();
    if (sousModeleToSelect == null) {//on récupère l'ancienne selection
      sousModeleToSelect = getSelectedSousModele();
    }
    if (sousModeleToSelect != null) {
      sousModeleToSelect = (EMHSousModele) idRegistry.getEmh(sousModeleToSelect.getUiId());//au cas ou changement d'instance...
    }
    CatEMHBranche brancheToSelect = initBrancheToSelect;
    if (brancheToSelect == null) {
      brancheToSelect = getSelectedBranche();
    }
    if (brancheToSelect != null) {
      brancheToSelect = (CatEMHBranche) idRegistry.getEmh(brancheToSelect.getUiId());//au cas ou changement d'instance...
      sousModeleToSelect = brancheToSelect.getParent();//au cas une branche ait changé de sous-modele
    }
    cbSousModeles.setModel(new DefaultComboBoxModel(sousModelesArray));
    if (sousModeleToSelect == null && sousModelesArray.length == 1) {
      sousModeleToSelect = sousModelesArray[0];
    }
    cbSousModeles.setSelectedItem(sousModeleToSelect);
    if (sousModeleToSelect != null) {
      final List<CatEMHBranche> branches = getBranches(sousModeleToSelect);
      cbBranches.setModel(new DefaultComboBoxModel(branches.toArray(new CatEMHBranche[0])));
    }
    cbBranches.setSelectedItem(brancheToSelect);
    updating = false;

  }

  public JComboBox getCbBranches() {
    return cbBranches;
  }

  public JComboBox getCbSousModeles() {
    return cbSousModeles;
  }

  protected EMHSousModele getSelectedSousModele() {
    return (EMHSousModele) cbSousModeles.getSelectedItem();
  }

  /**
   *
   * @param sousModele le sous-modele
   * @return la liste des branches contenant des profil section.
   */
  protected List<CatEMHBranche> getBranches(final EMHSousModele sousModele) {
    if (sousModele == null) {
      return Collections.emptyList();
    }
    final List<CatEMHBranche> branchesWithProfil = new ArrayList<>();
    branchesWithProfil.add(ALL_BRANCHE);
    for (final CatEMHBranche branche : sousModele.getBranches()) {
      if (EMHHelper.containEMHSectionProfil(branche)) {
        branchesWithProfil.add(branche);
      }
    }
    if (EMHHelper.containEMHSectionProfilWithoutBranches(sousModele)) {
      branchesWithProfil.add(SousModeleBrancheComponent.NULL_BRANCHE);
    }
    return branchesWithProfil;
  }

  void setEnabled(final boolean enabled) {
    cbBranches.setEnabled(enabled);
    cbSousModeles.setEnabled(enabled);
  }

  private void sousModeleSelectionChanged() {
    if (updating) {
      return;
    }
    final EMHSousModele sousModele = getSelectedSousModele();
    final List<CatEMHBranche> branches = getBranches(sousModele);
    ComboBoxHelper.setDefaultModel(cbBranches, branches);
    cbBranches.setSelectedIndex(-1);
    if (!branches.isEmpty()) {
      cbBranches.setSelectedIndex(0);
    }
  }

  public void clearComboBoxes() {
    updating = true;
    cbBranches.setModel(new DefaultComboBoxModel());
    cbSousModeles.setModel(new DefaultComboBoxModel());
    updating = false;
  }

  void setSelectedBranche(final CatEMHBranche branche) {
    cbBranches.setSelectedItem(branche == null ? NULL_BRANCHE : branche);
  }
}
