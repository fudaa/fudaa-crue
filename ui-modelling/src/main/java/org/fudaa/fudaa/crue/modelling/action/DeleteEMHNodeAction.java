package org.fudaa.fudaa.crue.modelling.action;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.modelling.ModellingTopComponentEMHEditable;
import org.openide.nodes.Node;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 *
 * @author deniger
 */
public abstract class DeleteEMHNodeAction extends AbstractEditNodeAction {

  final boolean cascade;

  public DeleteEMHNodeAction(final boolean cascade, final String title) {
    super(title);
    this.cascade = cascade;
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    if (activatedNodes.length == 0) {
      return false;
    }
    final EMH emh = activatedNodes[0].getLookup().lookup(EMH.class);
    return emh != null && !emh.getCatType().isContainer();
  }

  @Override
  protected void performAction(final Node[] activatedNodes) {
    final TopComponent activated = WindowManager.getDefault().getRegistry().getActivated();
    if (activated instanceof ModellingTopComponentEMHEditable) {
      final List<EMH> emhs = new ArrayList<>();
      for (final Node node : activatedNodes) {
        final EMH emh = node.getLookup().lookup(EMH.class);
        if (!emh.getCatType().isContainer()) {
          emhs.add(emh);
        }
      }
      ((ModellingTopComponentEMHEditable) activated).deleteEMH(emhs, cascade);
    }

  }
}
