package org.fudaa.fudaa.crue.modelling.emh;

import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.fudaa.crue.planimetry.action.PlanimetryEMHEditor;

/**
 *
 * @author Frédéric Deniger
 */
public class ModellingOpenEMHPlanimetryAdapter implements PlanimetryEMHEditor {

  @Override
  public void edit(EMH emh, boolean reopenFrame) {
    ModellingOpenEMHNodeAction.open(emh, reopenFrame);
  }
}
