/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.listener;

import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import org.fudaa.fudaa.crue.modelling.AbstractModellingTopComponent;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

/**
 *
 * @author Frédéric Deniger
 */
public class ExplorerManagerListenerHelper implements ExplorerManager.Provider {

  protected final ExplorerManager em = new ExplorerManager();
  private final AbstractModellingTopComponent topComponent;

  public ExplorerManagerListenerHelper(final AbstractModellingTopComponent topComponent) {
    this.topComponent = topComponent;
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return em;
  }

  @SuppressWarnings("unused")
  public void setEditable(final boolean editable) {
    //only to generate an event
    final Node[] selectedNodes = em.getSelectedNodes();
    try {
      em.setSelectedNodes(new Node[0]);
      em.setSelectedNodes(selectedNodes);
    } catch (final PropertyVetoException ex) {
      Exceptions.printStackTrace(ex);
    }
  }
  private PropertyChangeListener nodePropertyChangeListener;
  private NodeInternChangedListener nodeInternListener;

  public NodeInternChangedListener getNodeListener() {
    if (nodeInternListener == null) {
      nodeInternListener = creategetNodeListener();
    }
    return nodeInternListener;
  }

  private PropertyChangeListener getNodePropertyChangeListener() {
    if (nodePropertyChangeListener == null) {
      nodePropertyChangeListener = creatNodePropertyChangeListener();
    }
    return nodePropertyChangeListener;
  }

  private NodeInternChangedListener creategetNodeListener() {
    return new NodeInternChangedListener(topComponent);
  }

  private PropertyChangeListener creatNodePropertyChangeListener() {
    return new NodeChangedListener(topComponent);
  }

  private void addListener(final Children children) {
    final Node[] nodes = children.getNodes();
    if (nodes != null) {
      for (final Node node : nodes) {
        addListenerToNode(node);
        final Children nodeChildren = node.getChildren();
        if (nodeChildren.getNodesCount() > 0) {
          addListener(nodeChildren);
        }

      }
    }
  }

  private void cleanListener(final Children children) {
    final Node[] nodes = children.getNodes();
    if (nodes != null) {
      for (final Node node : nodes) {
        removeListeners(node);
        final Children nodeChildren = node.getChildren();
        if (nodeChildren.getNodesCount() > 0) {
          cleanListener(nodeChildren);
        }

      }
    }
  }

  public void scenarioUnloaded() {
    cleanListener();
    em.setRootContext(Node.EMPTY);
  }

  private void addListenerToNode(final Node node) {
    node.addPropertyChangeListener(getNodePropertyChangeListener());
    node.addNodeListener(getNodeListener());
  }

  public void cleanListener() {
    removeListeners(em.getRootContext());
    final Children children = em.getRootContext().getChildren();
    cleanListener(children);
  }

  public void addListener() {
    final Children children = em.getRootContext().getChildren();
    addListener(children);
  }

  public void addListener(final boolean addListenerToRoot) {
    if (addListenerToRoot) {
      addListenerToNode(em.getRootContext());
    }
    addListener();
  }

  private void removeListeners(final Node node) {
    if (nodePropertyChangeListener != null) {
      node.removePropertyChangeListener(nodePropertyChangeListener);
    }
    if (nodeInternListener != null) {
      node.removeNodeListener(nodeInternListener);
    }
  }
}
