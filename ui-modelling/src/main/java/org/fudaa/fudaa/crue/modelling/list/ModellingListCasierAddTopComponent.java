/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.list;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.dodico.crue.edition.bean.ListCasierContent;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.modelling.edition.ListAjoutCasierProcess;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
//@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.modelling//ModellingListNoeudTopComponent//EN",
//                     autostore = false)
@TopComponent.Description(preferredID = ModellingListCasierAddTopComponent.TOPCOMPONENT_ID, iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png", persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ModellingListCasierAddTopComponent.MODE, openAtStartup = false, position = 0)
public final class ModellingListCasierAddTopComponent extends AbstractModellingListAddTopComponent {
//attention le mode modelling-listAddNoeuds doit être déclaré dans le projet branding (layer.xml)

  public static final String MODE = "modelling-listAddCasiers";
  public static final String TOPCOMPONENT_ID = "ModellingListCasierAddTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;

  public ModellingListCasierAddTopComponent() {

    setName(NbBundle.getMessage(ModellingListCasierAddTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingListCasierAddTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueAjoutCasiers";
  }

  @Override
  protected void addEMH() {

    Children children = getExplorerManager().getRootContext().getChildren();

    Node[] nodes = children.getNodes();
    ListCasierContent listCasierContent = createDefaultContent(nodes);
    final List<String> availableNoeudsNameForCasiers = ListAjoutCasierProcess.getAvailableNoeudsNameForCasiers(getSousModele());
    children.add(new Node[]{new ListCasierContentAddNode(listCasierContent, perspectiveServiceModelling, availableNoeudsNameForCasiers)});
  }

  private ListCasierContent createDefaultContent(Node[] nodes) {
    EMHScenario scenarioLoaded = getModellingService().getScenarioLoaded();
    Collection<String> names = TransformerHelper.toSetNom(scenarioLoaded.getIdRegistry().getEMHs(EnumCatEMH.CASIER));
    List<String> availableNoeuds = ListAjoutCasierProcess.getAvailableNoeudsNameForCasiers(getSousModele());
    for (Node node : nodes) {
      ListCasierContent content = node.getLookup().lookup(ListCasierContent.class);
      availableNoeuds.remove(content.getNoeud());
      names.add(content.getNom());
    }
    UniqueNomFinder nomFinder = new UniqueNomFinder();
    Collections.sort(availableNoeuds);
    String newName = null;
    ListCasierContent listCasierContent = null;
    if (!availableNoeuds.isEmpty()) {
      final String noeud = availableNoeuds.get(0);
      newName = CruePrefix.getNomAvecPrefixFor(noeud, EnumCatEMH.CASIER);
      newName = nomFinder.findUniqueName(names, newName);
      listCasierContent = new ListCasierContent(newName);
      listCasierContent.setNoeud(noeud);
    }
    if (newName == null) {
      listCasierContent = new ListCasierContent(nomFinder.findNewName(names, CruePrefix.P_CASIER));
    }
    return listCasierContent;
  }

  @Override
  protected void addEMHsFromClipboard() {
    addNodesFromClipboard(ListAjoutCasierProcess.createNodes(perspectiveServiceModelling, getSousModele()));
  }

  @Override
  protected void doValidationModification() {
    Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    ListAjoutCasierProcess process = new ListAjoutCasierProcess();
    EMHSousModele ssModele = (EMHSousModele) getModellingService().getScenarioLoaded().getIdRegistry().getEmh(getSousModeleUid());
    CtuluLog res = process.processAjout(nodes, ssModele);
    if (res.isEmpty()) {
      clearView();
      setModified(false);
    } else {
      LogsDisplayer.displayError(res, getDisplayName());
    }
  }

  @Override
  protected void installPropertyColumns() {
    outlineView.setPropertyColumns(ListCommonProperties.PROP_NOM,
            ListCasierContentNode.getCasierNameDisplay(),
            ListCasierContent.PROP_NOEUD,
            ListCasierContentNode.getCasierNoeudDisplayName(),
            ListCommonProperties.PROP_COMMENTAIRE,
            AbstractListContentNode.getCommentDisplay());
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
