/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.action;

import java.awt.event.ActionEvent;
import java.util.Collections;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioVisuService;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ModellingGlobalFindAction extends EbliActionSimple {

  public ModellingGlobalFindAction() {
    super("Find", null, "Find");
    setKey(KeyStroke.getKeyStroke("ctrl F"));
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final ModellingScenarioVisuService reportVisuPanelService = Lookup.getDefault().lookup(ModellingScenarioVisuService.class);
    if (reportVisuPanelService.getPlanimetryVisuPanel() != null) {
      reportVisuPanelService.getPlanimetryVisuPanel().find();
    }
  }

  public static void installAction(final JComponent c) {
    EbliLib.updateMapKeyStroke(c, Collections.singletonList(new ModellingGlobalFindAction()));

  }
}
