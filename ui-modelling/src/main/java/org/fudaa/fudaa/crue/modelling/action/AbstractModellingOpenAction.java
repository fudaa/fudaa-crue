/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.openide.util.Lookup;

public abstract class AbstractModellingOpenAction extends AbstractModellingAction {

  final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);

  public AbstractModellingOpenAction() {
    super(false);
  }

  @Override
  protected boolean validationAction() {
    return modellingScenarioService.isScenarioLoaded();
  }
}
