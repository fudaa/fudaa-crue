package org.fudaa.fudaa.crue.modelling.action;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.modelling.ModellingTopComponentEMHEditable;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 *
 * @author deniger
 */
public class SimplifiyNodeAction extends AbstractEditNodeAction {

  public SimplifiyNodeAction() {
    super(NbBundle.getMessage(SimplifiyNodeAction.class, "simplifyProfils.ActionName"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return (activatedNodes.length > 0);
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    TopComponent activated = WindowManager.getDefault().getRegistry().getActivated();
    if (activated instanceof ModellingTopComponentEMHEditable) {
      List<EMH> emhs = new ArrayList<>();
      for (Node node : activatedNodes) {
        EMH emh = node.getLookup().lookup(EMH.class);
        if (emh != null) {
          emhs.add(emh);
        }
      }
      ((ModellingTopComponentEMHEditable) activated).simplifyProfils(emhs);
    }

  }
}
