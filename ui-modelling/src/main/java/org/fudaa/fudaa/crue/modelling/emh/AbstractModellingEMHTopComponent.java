package org.fudaa.fudaa.crue.modelling.emh;

import com.memoire.bu.BuGridLayout;
import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.crue.edition.EditionRename;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.fudaa.crue.common.AbstractTopComponent;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocContrat;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.modelling.AbstractModellingTopComponent;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.MissingResourceException;

/**
 * Squelette pour les TopComponent permettant de modifer des EMH.
 *
 * @author deniger
 */
public abstract class AbstractModellingEMHTopComponent extends AbstractModellingTopComponent {
  JTextField jEMHName;
  JTextField jCommentaire;
  private JLabel jEMHNameError;
  private Long emhUid;
  final transient ActionListener modifiedActionListener = e -> setModified(true);

  public AbstractModellingEMHTopComponent() {
    initComponents();
    createComponents();
  }

  @Override
  public void valideModificationHandler() {
    final String newName = jEMHName.getText();
    final String error = new EditionRename().rename(getEMH(), newName);
    if (error != null) {
      DialogHelper.showError(error);
    } else {
      getEMH().setCommentaire(jCommentaire.getText());
      modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.EMH_NAME));
      setModified(false);
    }
  }

  protected void createCommentaireTxt() {
    jCommentaire = new JTextField(50);
    jCommentaire.getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void insertUpdate(final DocumentEvent e) {
        commentaireChanged();
      }

      @Override
      public void removeUpdate(final DocumentEvent e) {
        commentaireChanged();
      }

      @Override
      public void changedUpdate(final DocumentEvent e) {
        commentaireChanged();
      }
    });
  }

  protected JPanel createNorthPanel(final JComponent northEastCmp) throws MissingResourceException {
    jEMHName = new JTextField();
    jEMHName.setColumns(20);
    jEMHName.getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void insertUpdate(final DocumentEvent e) {
        nameChanged();
      }

      @Override
      public void removeUpdate(final DocumentEvent e) {
        nameChanged();
      }

      @Override
      public void changedUpdate(final DocumentEvent e) {
        nameChanged();
      }
    });
    jEMHNameError = new JLabel();
    JButton btHelp = SysdocUrlBuilder.createButtonHelp();
    btHelp.addActionListener(e -> showCommonHelp());
    final JPanel pn = new JPanel(new BuGridLayout(2, 5, 6));
    pn.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    pn.add(new JLabel(NbBundle.getMessage(AbstractModellingEMHTopComponent.class, "ListTopComponent.EMHName.Label")));
    final JPanel pnName = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));

    pnName.add(jEMHName);
    if (northEastCmp != null) {
      pnName.add(northEastCmp);
    }
    pnName.add(btHelp);
    pn.add(pnName);
    pn.add(new JLabel());
    pn.add(jEMHNameError);
    return pn;
  }

  @Override
  protected EMHScenario getScenario() {
    return super.modellingScenarioModificationService.getModellingScenarioService().getScenarioLoaded();
  }

  private void showCommonHelp() {
    final SysdocContrat sysdocService = Lookup.getDefault().lookup(SysdocContrat.class);
    if (sysdocService != null && getScenario() != null) {
      final EMH emh = getScenario().getIdRegistry().getEmh(emhUid);
      sysdocService.display(SysdocUrlBuilder.getEMHHelpCtxId(emh));
    }
  }

  @Override
  public void cancelModificationHandler() {
    scenarioReloaded();
  }

  protected abstract void createComponents();

  protected <T extends EMH> T getEMH() {
    return (T) getModellingService().getScenarioLoaded().getIdRegistry().getEmh(getEMHUid());
  }

  private void nameChanged() {
    setModified(true);
    updateNameState();
  }

  private void commentaireChanged() {
    setModified(true);
  }

  JTextField type;

  protected JPanel buildNorthPanel() {
    final JPanel pn = createNorthPanel(null);
    pn.add(new JLabel(NbBundle.getMessage(AbstractModellingEMHTopComponent.class, "Type.DisplayName")));
    addTypeInNorthPanel(pn);
    createCommentaireTxt();
    pn.add(new JLabel(NbBundle.getMessage(AbstractModellingEMHTopComponent.class, "Commentaire.DisplayName")));
    pn.add(jCommentaire);
    return pn;
  }

  @Override
  protected void scenarioReloaded() {
    scenarioLoaded();
  }

  boolean isUpdating;

  @Override
  public void setModified(final boolean modified) {
    if (modified && isUpdating) {
      return;
    }
    super.setModified(modified);
  }

  @Override
  public void scenarioLoaded() {
    isUpdating = true;
    final EMH emh = getEMH();
    if (emh != null) {
      setName(emh.getNom());
      jEMHName.setText(emh.getNom());
      jCommentaire.setText(emh.getCommentaire());
      if (type != null) {
        type.setText(emh.getTypei18n());
      }
      updateNameState();
      updateOtherComponents();
    } else {
      setName(CtuluLibString.EMPTY_STRING);
      jEMHName.setText(CtuluLibString.EMPTY_STRING);
      jCommentaire.setText(CtuluLibString.EMPTY_STRING);
      if (type != null) {
        type.setText(CtuluLibString.EMPTY_STRING);
      }
    }
    isUpdating = false;
    setModified(false);
  }

  protected void updateOtherComponents() {
  }

  @Override
  protected void scenarioUnloaded() {
    close();
  }

  private void updateNameState() {
    final EMH emh = getEMH();
    final String nameValide = isNameValide(emh);
    AbstractTopComponent.updateValidState(jEMHNameError, nameValide);
  }

  private String isNameValide(final EMH emh) {
    return ValidationPatternHelper.isNameValidei18nMessage(jEMHName.getText(), emh.getCatType());
  }

  public Long getEMHUid() {
    return emhUid;
  }

  public void setEMHUid(final Long sousModeleUid) {
    this.emhUid = sousModeleUid;
  }

  public void reloadWithEMHUid(final Long emhUid) {
    if (!ObjectUtils.equals(this.emhUid, emhUid)) {
      this.emhUid = emhUid;
      scenarioReloaded();
    }
  }

  @SuppressWarnings("unused")
  void writeProperties(final java.util.Properties p) {
    p.setProperty("version", "1.0");
  }
  @SuppressWarnings("unused")
  void readProperties(final java.util.Properties p) {
  }

  @Override
  protected void setEditable(final boolean b) {
    jEMHName.setEditable(b);
    jCommentaire.setEditable(b);
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables

  protected void addTypeInNorthPanel(final JPanel pn) {
    type = new JTextField(20);
    type.setEditable(false);
    pn.add(type);
  }
}
