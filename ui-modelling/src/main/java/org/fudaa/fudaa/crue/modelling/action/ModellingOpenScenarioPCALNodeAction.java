package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.modelling.calcul.ModellingScenarioPCALTopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public abstract class ModellingOpenScenarioPCALNodeAction extends AbstractModellingOpenScenarioNodeAction {

  public ModellingOpenScenarioPCALNodeAction(String name) {
    super(name, ModellingScenarioPCALTopComponent.MODE, ModellingScenarioPCALTopComponent.TOPCOMPONENT_ID);
  }

  public static void open(EMHScenario scenario) {
    if (scenario == null) {
      return;
    }
    ModellingOpenScenarioPCALNodeAction action = SystemAction.get(ModellingOpenScenarioPCALNodeAction.Reopen.class);
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(scenario));
    action.performAction(new Node[]{node});
  }

  public static class Reopen extends ModellingOpenScenarioPCALNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenScenarioPCALNodeAction.class, "ModellingScenarioPCALNodeAction.Name"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenScenarioPCALNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenScenarioPCALNodeAction.class, "ModellingScenarioPCALNodeAction.NewFrame.Name"));
      setReopen(false);
    }
  }
}
