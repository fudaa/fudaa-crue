package org.fudaa.fudaa.crue.modelling.services;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.loader.ScenarioLoaderServiceAbstract;
import org.fudaa.fudaa.crue.modelling.action.ModellingSaveAction;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Set;

/**
 * Le service de base de la perspective Modélisation permettant de gérer le scenario ouvert: chargement, sauvegarde,...
 * <table  border="1"  cellpadding="3" cellspacing="0"  summary="lookups">
 * <tr>
 * <th align="left">Lookup</th>
 * <th align="left">Commentaire</th>
 * <th align="left">Méthode utilisant le lookup</th>
 * </tr>
 * <tr>
 * <td>{@link EMHScenario}</td>
 * <td>
 * Le scenario chargé
 * </td>
 * <td><code>{@link #getScenarioLoaded()}</code></td>
 * </tr>
 * <tr>
 * <td>{@link ManagerEMHScenario}</td>
 * <td>	Le ManagerEMHScenario chargé: contient en plus des infos sur le run chargé mais non utile ici
 * <td><code>{@link #getManagerScenarioLoaded()}</code></td>
 * </tr>
 * <tr>
 * <td>{@link org.fudaa.ctulu.CtuluLogGroup}</td>
 * <td>Bilan de la dernière opération effectuée dans la perspective</td>
 * <td><code>{@link #getLastLogsGroup()}</code></td>
 * </tr>
 * </table>
 *
 * @author deniger
 */
@ServiceProviders(value = {
    @ServiceProvider(service = ModellingScenarioService.class),
    @ServiceProvider(service = ModellingScenarioServiceImpl.class)})
public class ModellingScenarioServiceImpl extends ScenarioLoaderServiceAbstract implements ModellingScenarioService {
  private final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  private File tempDir;
  private boolean reloading;

  public ModellingScenarioServiceImpl() {
    super(PerspectiveEnum.MODELLING);
  }

  /**
   * Fait appel à la méthode de la classe parent. Créé en plus un dossier temporaire pour les sauvegardes.
   *
   * @param projet le projet contenant le scenario
   * @param scenario le scenario a charger
   * @return EMHScenario chargé
   * @see ScenarioLoaderServiceAbstract#loadScenario(org.fudaa.dodico.crue.metier.etude.EMHProjet, org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario,
   *     org.fudaa.dodico.crue.metier.etude.EMHRun)
   */
  @Override
  public EMHScenario loadScenario(EMHProjet projet, ManagerEMHScenario scenario) {
    EMHScenario emhScenario = super.loadScenario(projet, scenario, null);
    if (emhScenario != null) {
      try {
        tempDir = CtuluLibFile.createTempDir(scenario.getId());
      } catch (IOException iOException) {
        Exceptions.printStackTrace(iOException);
      }
    }
    return emhScenario;
  }

  /**
   * A utiliser si un run a été créé par exemple
   *
   * @param managerEMHScenario le nouveau manager scenario
   */
  public void updateManagerScenarioLoaded(ManagerEMHScenario managerEMHScenario) {
    if (getManagerScenarioLoaded() != null && managerEMHScenario != null) {
      super.dynamicContent.remove(getManagerScenarioLoaded());
      super.dynamicContent.add(managerEMHScenario);
    }
  }

  /**
   * Avertit l'utilsateur des modifications apportées automatiquement lors du chargement du scenario
   *
   * @param modificationDoneWhileLoading les modifications effectuées au chargemenet
   */
  @Override
  protected void manageAutomaticModification(ScenarioAutoModifiedState modificationDoneWhileLoading) {
    //le scenario a été normalisé
    if (modificationDoneWhileLoading.isNormalized()) {

      //il faut donc avertir l'utilsateur.
      //invokeLater pour le faire après les actions en cours.
      EventQueue.invokeLater(() -> {
        DialogHelper.showWarn(NbBundle.getMessage(ModellingScenarioServiceImpl.class, "ScenarioAutoModifiedAtLoading.Message"));
        ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault()
            .lookup(ModellingScenarioModificationService.class);
        modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.ALL));
      });
    }
  }

  /**
   * Attention: a utiliser pour les tests uniquement !
   *
   * @param scenario le scenario chargé
   * @param projet le projet chargé.
   */
  public void setForTest(EMHScenario scenario, EMHProjet projet) {
    super.dynamicContent.add(scenario);
    projetService.setForTest(projet);
  }

  /**
   * @return le dossier temp associé au scenario chargé. Utilisé pour sauvegarder en 2 etapes les fichiers par exemple
   */
  public File getTempDir() {
    return tempDir;
  }

  @Override
  public EMHProjet getSelectedProjet() {
    return projetService.getSelectedProject();
  }

  @Override
  public boolean unloadScenario() {
    boolean res = super.unloadScenario();
    if (res) {
      CtuluLibFile.deleteDir(tempDir);
    }
    return res;
  }

  @Override
  public void updateLastLogsGroup(final CtuluLogGroup ctuluLogGroup) {
    if (EventQueue.isDispatchThread()) {
      super.updateLastLogsGroup(ctuluLogGroup);
    } else {
      EventQueue.invokeLater(new Runnable() {
        @Override
        public void run() {
          ModellingScenarioServiceImpl.super.updateLastLogsGroup(ctuluLogGroup);
        }
      });
    }
  }

  /**
   * @return true si le scenario ne contient pas d'erreurs.
   */
  @Override
  public boolean validScenario() {
    ModellingValidationProcessor processor = new ModellingValidationProcessor(this);
    CtuluLogGroup run = processor.run(null);
    updateLastLogsGroup(run);
    return getLastLogsGroup() != null && !getLastLogsGroup().containsFatalError();
  }

  /**
   * Sauvegarde toutes les données si nécessaire: geoloc, congif etude et le scenario. Lance l'action dans un ProgressRunner
   *
   * @return true la sauvegarde est ok.
   */
  @Override
  public boolean saveScenario() {
    ModellingSaveProcessor saveProcessor = new ModellingSaveProcessor();
    Pair<Set<String>, CtuluLogGroup> logs = CrueProgressUtils.showProgressDialogAndRun(saveProcessor,
        NbBundle.getMessage(ModellingSaveAction.class,
            "CTL_ModellingSaveAction"));
    updateLastLogsGroup(logs.second);
    if (!logs.first.isEmpty()) {
      projetService.scenarioSaved(getManagerScenarioLoaded(), logs.first);
    }
    return !logs.second.containsFatalError();
  }

  @Override
  public boolean isReloading() {
    return reloading;
  }

  /**
   * A utiliser dans le thread swing
   *
   * @param newScenario le nouveau scenario chargé
   */
  void setNewScenario(EMHScenario newScenario) {
    assert EventQueue.isDispatchThread();
    //l'ordre est important:
    reloading = true;
    final EMHScenario scenarioLoaded = getScenarioLoaded();
    if (scenarioLoaded != null) {
      dynamicContent.remove(scenarioLoaded);
    }
    dynamicContent.add(newScenario);
    reloading = false;
  }
}
