package org.fudaa.fudaa.crue.modelling.loi;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.ctulu.gui.ExportTableCommentSupplier;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.dodico.crue.validation.ValidateAndRebuildProfilSection;
import org.fudaa.dodico.crue.validation.ValidatorNomProfilCasierContent;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.loi.casier.ProfilCasierLoiUiController;
import org.fudaa.fudaa.crue.loi.common.LoiPopupMenuReceiver;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.fudaa.fudaa.crue.study.services.TableExportCommentSupplier;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Set;

/**
 * Editeur de lois.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.loi//ProfilCasierEditorLoiTopComponent//EN",
    autostore = false)
@TopComponent.Description(preferredID = ProfilCasierEditorTopComponent.TOPCOMPONENT_ID, persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ProfilCasierEditorTopComponent.MODE, openAtStartup = false)
public final class ProfilCasierEditorTopComponent extends AbstractModellingLoiTopComponent implements ExportTableCommentSupplier {
  public static final String MODE = "loi-profilCasierEditor"; //Defini dans le module branding
  public static final String TOPCOMPONENT_ID = "ProfilCasierEditorTopComponent"; //NOI18N
  private final transient ProfilCasierLoiUiController profilCasierLoiUiController;
  private final JTextField txtCommentaire;
  private final JLabel lbNomValidation;//contient les erreurs de validation sur le nom du oldCasier
  private final JTextField txtNomProfilCasier;
  private final JTextField txtLongeur;
  private transient SousModeleCasierComponent sousModelesCasiers;
  private JButton btUp;
  private JButton btDown;
  private JComboBox cbProfils;
  private transient DonPrtGeoProfilCasier currentProfil;
  private boolean isNewProfil;
  private final String initName;
  private transient List<DonPrtGeoProfilCasier> profilsCasiers;

  /**
   * Pour chaque uid de la liste profils donne la position dans la liste.
   */
  public ProfilCasierEditorTopComponent() {
    initComponents();
    initName = NbBundle.getMessage(ProfilCasierEditorTopComponent.class, "CTL_" + TOPCOMPONENT_ID);
    setName(initName);
    setToolTipText(NbBundle.getMessage(ProfilCasierEditorTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    profilCasierLoiUiController = new ProfilCasierLoiUiController() {
      @Override
      public LoiPopupMenuReceiver createLoiPopupReceiver() {
        return new ModellingProfilCasierMenuReceiver(this, ProfilCasierEditorTopComponent.this);
      }
    };
    add(profilCasierLoiUiController.getToolbar(), BorderLayout.NORTH);
    profilCasierLoiUiController.getPanel().setPreferredSize(new Dimension(550, 350));
    final JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, profilCasierLoiUiController.getPanel(), profilCasierLoiUiController.
        getTableGraphePanel());
    splitPane.setDividerLocation(550);
    splitPane.setOneTouchExpandable(true);
    splitPane.setContinuousLayout(true);
    add(splitPane);

    final JPanel pnProfilCasierData = new JPanel(new BuGridLayout(3, 10, 10));
    pnProfilCasierData.add(new JLabel(NbBundle.getMessage(ProfilCasierEditorTopComponent.class, "Nom.DisplayName")));
    final DocumentListener documentListener = super.createDocumentListener();
    final DocumentListener nameChanged = new DocumentListener() {
      @Override
      public void insertUpdate(final DocumentEvent e) {
        titleChanged();
      }

      @Override
      public void removeUpdate(final DocumentEvent e) {
        titleChanged();
      }

      @Override
      public void changedUpdate(final DocumentEvent e) {
        titleChanged();
      }
    };
    txtNomProfilCasier = new JTextField(30);
    lbNomValidation = new JLabel();
    pnProfilCasierData.add(txtNomProfilCasier);
    pnProfilCasierData.add(lbNomValidation);
    txtCommentaire = new JTextField(30);
    txtCommentaire.setEditable(false);
    txtCommentaire.getDocument().addDocumentListener(documentListener);
    pnProfilCasierData.add(new JLabel(NbBundle.getMessage(ProfilCasierEditorTopComponent.class, "Commentaire.DisplayName")));
    pnProfilCasierData.add(txtCommentaire);
    pnProfilCasierData.add(new JLabel());

    txtLongeur = BuTextField.createDoubleField();
    txtLongeur.setEditable(false);
    txtLongeur.getDocument().addDocumentListener(documentListener);
    txtLongeur.getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void insertUpdate(final DocumentEvent e) {
        uptateTxtLongueurTooltip();
      }

      @Override
      public void removeUpdate(final DocumentEvent e) {
        uptateTxtLongueurTooltip();
      }

      @Override
      public void changedUpdate(final DocumentEvent e) {
        uptateTxtLongueurTooltip();
      }
    });

    pnProfilCasierData.add(new JLabel(NbBundle.getMessage(ProfilCasierEditorTopComponent.class, "LongeurProfilCasier.DisplayName")));
    pnProfilCasierData.add(txtLongeur);

    txtNomProfilCasier.getDocument().addDocumentListener(documentListener);
    txtNomProfilCasier.getDocument().addDocumentListener(nameChanged);
    final JPanel pnInfo = new JPanel(new BorderLayout(5, 5));
    pnInfo.add(profilCasierLoiUiController.getLitUtileState().getLabel(), BorderLayout.NORTH);
    pnInfo.add(pnProfilCasierData);
    pnInfo.add(createCancelSaveButtons(), BorderLayout.SOUTH);

    final JPanel pnActions = createsPanelActions();
    final JPanel pnSouth = new JPanel(new BorderLayout(10, 10));
    pnSouth.add(pnActions, BorderLayout.WEST);
    pnSouth.setBorder(BorderFactory.createEtchedBorder());
    pnSouth.add(pnInfo);
    add(pnSouth, BorderLayout.SOUTH);
    setEditable(false);
  }

  private transient Action delete;
  private transient Action duplicate;
  private transient Action create;

  @Override
  protected EMHScenario getScenario() {
    return modellingScenarioModificationService.getModellingScenarioService().getScenarioLoaded();
  }

  @Override
  public ProfilCasierLoiUiController getLoiUIController() {
    return profilCasierLoiUiController;
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueEditionProfilCasiers";
  }

  private JPanel createsPanelActions() {
    //
    final ObjetNommeCellRenderer cellRenderer = new ObjetNommeCellRenderer();
    sousModelesCasiers = new SousModeleCasierComponent();
    sousModelesCasiers.getCbCasiers().addItemListener(e -> {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        selectedCasierChanged(false);
      }
    });

    cbProfils = new JComboBox();
    cbProfils.addItemListener(e -> {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        selectedProfilChanged();
      }
    });
    cbProfils.setRenderer(cellRenderer);

    //boutons
    btDown = new JButton(ImageUtilities.loadImageIcon("org/fudaa/fudaa/crue/modelling/icons/down.png", false));
    btUp = new JButton(ImageUtilities.loadImageIcon("org/fudaa/fudaa/crue/modelling/icons/up.png", false));
    btDown.setToolTipText(NbBundle.getMessage(ProfilSectionTopComponent.class, "ProfilSection.SelectNext.Tooltip"));
    btUp.setToolTipText(NbBundle.getMessage(ProfilSectionTopComponent.class, "ProfilSection.SelectPrevious.Tooltip"));
    btDown.addActionListener(e -> selectNextCasierProfil());
    btUp.addActionListener(e -> selectPreviousProfil());
    btDown.setBorderPainted(false);
    btDown.setBackground(null);
    btDown.setContentAreaFilled(false);
    btDown.setBorder(null);
    btUp.setBorderPainted(false);
    btUp.setBorder(null);
    btUp.setContentAreaFilled(false);
    btUp.setBackground(null);
    final BuGridLayout buGridLayout = new BuGridLayout(2, 0, 5, true, true, false, false, false);
    buGridLayout.setXAlign(0f);
    final JPanel pnActions = new JPanel(buGridLayout);
    pnActions.add(sousModelesCasiers.getCbSousModeles());
    pnActions.add(new JLabel());//pour remplir la 2eme colonne
    pnActions.add(sousModelesCasiers.getCbCasiers());
    pnActions.add(new JLabel());
    pnActions.add(cbProfils);
    final JPanel pnUpBottom = new JPanel(new BuGridLayout(2, 0, 0));
    pnUpBottom.add(btDown);
    pnUpBottom.add(btUp);
    pnActions.add(pnUpBottom);

    pnActions.add(new JSeparator());
    pnActions.add(new JLabel());
    createActions();
    pnActions.add(new JButton(create));
    pnActions.add(new JLabel());
    pnActions.add(new JButton(duplicate));
    pnActions.add(new JLabel());
    pnActions.add(new JSeparator());
    pnActions.add(new JLabel());
    pnActions.add(new JButton(delete));
    pnActions.add(new JLabel());
    return pnActions;
  }

  private Double getLongueur() {
    final String text = txtLongeur.getText();
    if (StringUtils.isBlank(text)) {
      return null;
    }
    try {
      return getCcm().getProperty(CrueConfigMetierConstants.PROP_DISTANCE).getNormalizedValue(Double.parseDouble(text));
    } catch (final NumberFormatException numberFormatException) {
      //nothing to do
    }
    return null;
  }

  private void titleChanged() {
    lbNomValidation.setText(StringUtils.EMPTY);
    lbNomValidation.setIcon(null);
    lbNomValidation.setToolTipText(null);
    if (currentProfil == null) {
      return;
    }
    updateTopComponentNameFromSelection();
    final String error = validateNom();
    if (error != null) {
      lbNomValidation.setToolTipText(error);
      lbNomValidation.setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.SEVERE));
    }
  }

  private String validateNom() {
    final String nom = getEditingNom();
    final CtuluLogRecord error = ValidatorNomProfilCasierContent.validNomProfilCasier(nom, getSelectedCasier());
    if (error != null) {
      BusinessMessages.updateLocalizedMessage(error);
      return error.getLocalizedMessage();
    }
    return null;
  }

  private void createActions() {
    delete = new EbliActionSimple(NbBundle.getMessage(ProfilCasierEditorTopComponent.class, "Delete.DisplayName"), BuResource.BU.getIcon("enlever"),
        "DELETE") {
      @Override
      public void actionPerformed(final ActionEvent actionEvent) {
        delete();
      }
    };
    delete.putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(ProfilCasierEditorTopComponent.class, "Delete.Description"));

    duplicate = new EbliActionSimple(NbBundle.getMessage(ProfilCasierEditorTopComponent.class, "Duplicate.DisplayName"), BuResource.BU.getIcon(
        "dupliquer"), "DUPLICATE") {
      @Override
      public void actionPerformed(final ActionEvent actionEvent) {
        duplicate();
      }
    };
    duplicate.putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(ProfilCasierEditorTopComponent.class, "Duplicate.Description"));

    create = new EbliActionSimple(NbBundle.getMessage(ProfilCasierEditorTopComponent.class, "Create.DisplayName"), BuResource.BU.getIcon("creer"),
        "CREATE") {
      @Override
      public void actionPerformed(final ActionEvent actionEvent) {
        create();
      }
    };
    create.putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(ProfilCasierEditorTopComponent.class, "Create.Description"));
  }

  @Override
  protected void componentOpenedHandler() {
    super.componentOpenedHandler();
    final Long init = (Long) getClientProperty(ProfilSectionTopComponent.PROPERTY_INIT_UID);
    if (init != null) {
      putClientProperty(ProfilSectionTopComponent.PROPERTY_INIT_UID, null);
      selectCasier(init);
    }
  }

  void selectCasier(final Long uid) {
    if (isModified() || uid == null) {
      return;
    }
    final EMH emh = getScenario().getIdRegistry().getEmh(uid);
    if (emh instanceof CatEMHCasier) {
      updateCasierSelection((CatEMHCasier) emh);
    }
  }

  private void selectProfilCasierAtPosition(final int pos) {
    if (profilsCasiers == null || pos < 0 || pos >= profilsCasiers.size()) {
      return;
    }
    final DonPrtGeoProfilCasier toSelect = profilsCasiers.get(pos);
    if (toSelect == null) {
      return;
    }
    final CatEMHCasier casier = (CatEMHCasier) toSelect.getEmh();
    if (casier == null) {
      return;
    }
    updateCasierSelection(casier);
    cbProfils.setSelectedItem(toSelect);
  }

  private int findProfilIdx(final String id) {
    if (id == null) {
      return -1;
    }
    final ComboBoxModel model = cbProfils.getModel();
    for (int i = model.getSize() - 1; i >= 0; i--) {
      final DonPrtGeoProfilCasier profil = (DonPrtGeoProfilCasier) model.getElementAt(i);
      if (id.equals(profil.getId())) {
        cbProfils.setSelectedIndex(i);
        return i;
      }
    }
    return -1;
  }

  private void selectedCasierChanged(final boolean restoreOldProfil) {
    final CatEMHCasier casier = getSelectedCasier();
    final DonPrtGeoProfilCasier oldProfil = currentProfil;
    String oldNomSelected = null;
    if (oldProfil != null) {
      oldNomSelected = oldProfil.getId();
    }
    currentProfil = null;
    if (casier != null) {
      final List<DonPrtGeoProfilCasier> profilCasiers = DonPrtHelper.getProfilCasier(casier);
      profilCasiers.sort(ObjetNommeByNameComparator.INSTANCE);
      cbProfils.setModel(new DefaultComboBoxModel(profilCasiers.toArray(new DonPrtGeoProfilCasier[0])));
      int idx = -1;
      //pour forcer la sélection a changer.
      cbProfils.setSelectedIndex(-1);
      if (restoreOldProfil) {
        idx = findProfilIdx(oldNomSelected);
      }
      if (idx >= 0) {
        cbProfils.setSelectedIndex(idx);
      } else if (!profilCasiers.isEmpty()) {
        cbProfils.setSelectedIndex(0);
      } else {
        cbProfils.setSelectedItem(null);
      }
    } else {
      cbProfils.setModel(new DefaultComboBoxModel());
    }
    updateTopComponentNameFromSelection();
    updateCrudButtons();
  }

  private void selectNextCasierProfil() {
    selectCasierProfilFromDelta(1);
  }

  private void selectPreviousProfil() {
    selectCasierProfilFromDelta(-1);
  }

  private void selectCasierProfilFromDelta(final int delta) {
    if (CollectionUtils.isEmpty(profilsCasiers)) {
      return;
    }
    if (!isNewProfil) {
      final DonPrtGeoProfilCasier selectedProfil = getSelectedCasierProfil();
      int pos = 0;
      if (selectedProfil != null) {
        final int currentPos = profilsCasiers.indexOf(selectedProfil);
        pos = currentPos + delta;
      }
      if (pos >= profilsCasiers.size()) {
        return;
      }
      if (pos < 0) {
        return;
      }
      selectProfilCasierAtPosition(pos);
    }
  }

  private void selectedProfilChanged() {

    currentProfil = (DonPrtGeoProfilCasier) cbProfils.getSelectedItem();
    updateWithProfilCasier();
    updateCrudButtons();
  }

  @Override
  public void setEditable(final boolean b) {
    super.setEditable(b);
    editable = b;
    profilCasierLoiUiController.setEditable(b);
    txtCommentaire.setEditable(b);
    txtLongeur.setEditable(b);
    txtNomProfilCasier.setEditable(b);
    updateCrudButtons();
  }

  @Override
  protected String getDefaultTopComponentId() {
    return TOPCOMPONENT_ID;
  }

  @Override
  public void cancelModificationHandler() {
    if (isNewProfil) {
      isNewProfil = false;
      currentProfil = (DonPrtGeoProfilCasier) cbProfils.getSelectedItem();
    }
    scenarioReloaded();
    reloadCourbeConfig();
    setModified(false);
  }

  private CatEMHCasier getSelectedCasier() {
    return sousModelesCasiers.getSelectedCasier();
  }

  private DonPrtGeoProfilCasier getSelectedCasierProfil() {
    return (DonPrtGeoProfilCasier) cbProfils.getSelectedItem();
  }

  @Override
  protected void scenarioLoadedHandler() {
    updating = true;

    CatEMHCasier oldCasier = getSelectedCasier();
    if (oldCasier != null) {
      oldCasier = (CatEMHCasier) getScenario().getIdRegistry().getEmh(oldCasier.getUiId());
    }
    sousModelesCasiers.updateValuesAndSelection(getScenario(), oldCasier);
    buildListProfilCasier();
    selectedCasierChanged(true);

    isNewProfil = false;
    updating = false;
    profilCasierLoiUiController.getGraphe().setExportTableCommentSupplier(this);
  }

  @Override
  public List<String> getComments() {
    return new TableExportCommentSupplier(false).getComments(getName() + " " + txtNomProfilCasier.getText());
  }

  private void uptateTxtLongueurTooltip() {
    final String txt = txtLongeur.getText();
    if (StringUtils.isBlank(txt)) {
      txtLongeur.setToolTipText(txt);
    }
    try {
      final Double value = Double.parseDouble(txt);
      txtLongeur.setToolTipText(TransformerEMHHelper.formatFromPropertyName("longeur", value, getCcm(), DecimalFormatEpsilonEnum.COMPARISON));
    } catch (final NumberFormatException numberFormatException) {
      txtLongeur.setToolTipText(txt);
    }
  }

  private void updateWithProfilCasier() {
    final DonPrtGeoProfilCasier profilCasier = getCurrentProfil();
    updateWithProfilCasier(profilCasier);
  }

  @Override
  protected void scenarioReloaded() {
    scenarioLoaded();
  }

  @Override
  protected void scenarioUnloadedHandler() {
    updating = true;
    profilCasierLoiUiController.setProfilCasier(null, null);
    cbProfils.setModel(new DefaultComboBoxModel());
    sousModelesCasiers.clearComboBoxes();
    currentProfil = null;
    profilsCasiers = null;
    updating = false;
  }

  private DonPrtGeoProfilCasier getCurrentProfil() {
    return currentProfil;
  }

  @Override
  public void setModified(final boolean modified) {
    super.setModified(modified);
    duplicate.setEnabled(!modified && editable);
    create.setEnabled(!modified && editable);
    cbProfils.setEnabled(!modified);
    btDown.setEnabled(!modified);
    btUp.setEnabled(!modified);
    sousModelesCasiers.setEnabled(!modified);
  }

  private boolean editable;

  private void delete() {
    final boolean ok = DialogHelper.showQuestion(NbBundle.getMessage(ProfilCasierEditorTopComponent.class, "ProfilCasier.DeleteConfirmation.Message",
        getEditingNom()));
    if (ok) {
      if (!isNewProfil) {
        final DefaultComboBoxModel model = (DefaultComboBoxModel) cbProfils.getModel();
        final int idx = Math.max(0, model.getIndexOf(currentProfil) - 1);
        final CatEMHCasier casier = getSelectedCasier();
        casier.removeInfosEMH(currentProfil);
        currentProfil = null;
        final List<DonPrtGeoProfilCasier> profilCasiers = DonPrtHelper.getProfilCasier(casier);
        if (idx < profilCasiers.size()) {
          currentProfil = profilCasiers.get(idx);
        }
        modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.DPTG));
      } else {
        //on sélectionne la loi disponible dans la combobox.
        currentProfil = (DonPrtGeoProfilCasier) cbProfils.getSelectedItem();
        scenarioReloaded();
      }
      isNewProfil = false;
      setModified(false);
    }
  }

  private void create() {
    isNewProfil = true;
    currentProfil = new DonPrtGeoProfilCasier(getCcm());
    currentProfil.setNom(findNewName());
    updateWithProfilCasier();
    setModified(true);
  }

  void currentProfilModified(final DonPrtGeoProfilCasier newState) {
    updateWithProfilCasier(newState);
    setModified(true);
  }

  private void duplicate() {
    if (currentProfil == null) {
      return;
    }
    currentProfil = currentProfil.cloneProfilCasier();
    currentProfil.setNom(findNewName());
    isNewProfil = true;
    updateWithProfilCasier();
    setModified(true);
  }

  DonPrtGeoProfilCasier createEdited() {
    final DonPrtGeoProfilCasier newProfilCasier = profilCasierLoiUiController.getLoiModel().createProfilCasier(getCcm());
    newProfilCasier.setCommentaire(txtCommentaire.getText());
    newProfilCasier.setNom(getEditingNom());
    final Double longueur = getLongueur();
    if (longueur != null) {

      newProfilCasier.setDistance(longueur);
    }
    return newProfilCasier;
  }

  @Override
  public boolean valideModificationDataHandler() {
    if (currentProfil == null || getSelectedCasier() == null) {
      return false;
    }
    boolean res = false;
    final DonPrtGeoProfilCasier newProfilCasier = createEdited();
    final Double longueur = getLongueur();
    final CtuluLog valideProfil = new ValidateAndRebuildProfilSection(getCcm(), null).validateDonPrtGeoProfilCasier(newProfilCasier);
    if (longueur == null) {
      valideProfil.addSevereError("validation.profilCasier.longueurNotSet");
    }
    final String error = validateNom();
    if (error != null) {
      valideProfil.addSevereError(error);
    } else {
      final Set<DonPrtGeoProfilCasier> allProfils = DonPrtHelper.getAllProfilCasier(getScenario());
      if (!isNewProfil) {
        allProfils.remove(currentProfil);
      }
      final Set<String> toSetOfId = TransformerHelper.toSetOfId(allProfils);
      if (toSetOfId.contains(getEditingNom().toUpperCase())) {
        valideProfil.addSevereError("validation.NameAlreadyUsed", getEditingNom());
      }
    }
    if (!valideProfil.containsErrorOrSevereError()) {
      currentProfil.initFrom(newProfilCasier);
      if (isNewProfil) {
        getSelectedCasier().addInfosEMH(currentProfil);
      }
      res = true;
      modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.DPTG));
      isNewProfil = false;
      setModified(false);
    }
    if (valideProfil.isNotEmpty()) {
      LogsDisplayer.displayError(valideProfil, getName());
    }
    return res;
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  void writeProperties(final java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
  }

  void readProperties(final java.util.Properties p) {
  }

  private String findNewName() {
    //on recherche un nom unique de ProfilCasier:
    final UniqueNomFinder nomFinder = new UniqueNomFinder();
    final String profilNamePrefix = CruePrefix.changePrefix(getSelectedCasier().getNom(), CruePrefix.P_CASIER, CruePrefix.P_PROFIL_CASIER);
    return nomFinder.findUniqueNameProfilCasier(profilNamePrefix, getScenario());
  }

  private String getEditingNom() {
    return txtNomProfilCasier.getText();
  }

  private void buildListProfilCasier() {
    profilsCasiers = new ArrayList<>();
    final List<EMHSousModele> sousModeles = getScenario().getSousModeles();
    for (final EMHSousModele sousModele : sousModeles) {
      final List<CatEMHCasier> ssCasiers = sousModele.getCasiers();
      for (final CatEMHCasier casier : ssCasiers) {
        final List<DonPrtGeoProfilCasier> profilCasiers = DonPrtHelper.getProfilCasier(casier);
        if (profilCasiers != null) {
          profilsCasiers.addAll(profilCasiers);
        }
      }
    }
  }

  private void updateCasierSelection(final CatEMHCasier casier) {
    final EMHSousModele parent = casier.getParent();
    if (parent != sousModelesCasiers.getSelectedSousModele()) {
      sousModelesCasiers.getCbSousModeles().setSelectedItem(parent);
    }
    final CatEMHCasier selectedCasier = sousModelesCasiers.getSelectedCasier();
    if (selectedCasier != casier) {
      sousModelesCasiers.setSelectedCasier(casier);
    }
  }

  private void updateCrudButtons() {
    delete.setEnabled(editable);
    duplicate.setEnabled(editable && getSelectedCasierProfil() != null);
    create.setEnabled(editable && getSelectedCasier() != null);
  }

  private void updateWithProfilCasier(final DonPrtGeoProfilCasier profilCasier) {
    updating = true;
    txtCommentaire.setText(StringUtils.EMPTY);
    txtNomProfilCasier.setText(StringUtils.EMPTY);
    if (profilCasier != null) {
      txtCommentaire.setText(profilCasier.getCommentaire());
      txtLongeur.setText(getCcm().getProperty(CrueConfigMetierConstants.PROP_DISTANCE).format(profilCasier.getDistance(),
          DecimalFormatEpsilonEnum.COMPARISON));
      uptateTxtLongueurTooltip();
      final String desc = StringUtils.defaultString(profilCasier.getNom());
      txtNomProfilCasier.setText(desc);
    }
    profilCasierLoiUiController.setProfilCasier(profilCasier, getCcm());
    profilCasierLoiUiController.getLoiModel().addObserver((o, arg) -> setModified(true));
    profilCasierLoiUiController.setEditable(editable);
    updating = false;
    applyCourbeConfig();
    setModified(false);
  }

  private void updateTopComponentNameFromSelection() throws MissingResourceException {
    String nameToUse = initName;
    final CatEMHCasier selectedCasier = getSelectedCasier();
    if (selectedCasier != null) {
      if (getSelectedCasierProfil() != null) {
        final String casier = selectedCasier.getNom() + " / " + getEditingNom();
        nameToUse = NbBundle.getMessage(ProfilCasierEditorTopComponent.class, "ProfilCasierView.Title", casier);
      } else {
        nameToUse = NbBundle.getMessage(ProfilCasierEditorTopComponent.class, "ProfilCasierView.Title", selectedCasier.getNom());
      }
    }
    setName(nameToUse);
    updateTopComponentName();
  }
}
