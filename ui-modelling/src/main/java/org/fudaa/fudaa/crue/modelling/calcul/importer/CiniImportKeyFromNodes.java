/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul.importer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fudaa.dodico.crue.metier.cini.CiniImportKey;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.openide.nodes.Node;

/**
 *
 * @author Frederic Deniger
 */
public class CiniImportKeyFromNodes {

  private final List<Node> nodes;

  public CiniImportKeyFromNodes(
          List<Node> nodes) {
    this.nodes = nodes;
  }

  /**
   *
   * @return les nodes par clés. Les identifiants sont en majuscule.
   */
  public Map<CiniImportKey, Node> getKeys() {
    Map<CiniImportKey, Node> keys = new HashMap<>();
    for (Node node : nodes) {
      EMH emh = node.getLookup().lookup(EMH.class);
      String type = node.getLookup().lookup(String.class);
      if (emh != null && type != null) {
        keys.put(new CiniImportKey(emh.getId(), type.toUpperCase()), node);
      }
    }
    return keys;

  }
}
