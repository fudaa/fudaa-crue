/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.EnumMethodeInterpol;
import org.fudaa.dodico.crue.metier.emh.EnumRegleInterpolation;
import org.fudaa.dodico.crue.metier.emh.OrdPrtCIniModeleBase;
import org.fudaa.dodico.crue.metier.emh.ValParam;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.modelling.node.CommonObjectNode;
import org.fudaa.fudaa.crue.modelling.node.PropertySupportFactory;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

/**
 *
 * @author Frédéric Deniger
 */
public class OrdPrtCIniModeleBaseNode extends CommonObjectNode<OrdPrtCIniModeleBase> {

  public OrdPrtCIniModeleBaseNode(OrdPrtCIniModeleBase object, CrueConfigMetier ccm, PerspectiveServiceModelling perspectiveServiceModelling) {
    super(object, ccm, perspectiveServiceModelling);
    setDisplayName(NbBundle.getMessage(OrdPrtCIniModeleBaseNode.class, "OPTIMethodeInterpolation.DisplayName"));
  }

  private class MethodInterpolationListener implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if (OrdPrtCIniModeleBase.PROP_METHOD_INTERPOLATION.equals(evt.getPropertyName())) {
        updateValParamSet();
      }
    }
  }

  private void updateValParamSet() {
    updateValParamSet(getSheet());
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(getLookup().lookup(OrdPrtCIniModeleBase.class)));
  }

  private void updateValParamSet(Sheet sheet) {
    final String vaParamSetName = "VAL_PARAM_SET";
    Set set = sheet.get(vaParamSetName);
    if (set != null) {
      sheet.remove(vaParamSetName);
    }
    OrdPrtCIniModeleBase object = getObject();
    EnumMethodeInterpol methodeInterpol = object.getMethodeInterpol();
    if (methodeInterpol != null) {
      List<EnumRegleInterpolation> requiredValParam = methodeInterpol.getRequiredValParam();
      final CrueConfigMetier ccm = getCcm();
      Collection<ValParam> setValParams = object.setValParams(requiredValParam, ccm);
      if (CollectionUtils.isNotEmpty(setValParams)) {
        Set valParamSet = new Set();
        valParamSet.setName(vaParamSetName);
        valParamSet.setDisplayName(NbBundle.getMessage(OrdPrtCIniModeleBaseNode.class, "methodInterpolationParameter.DisplayNames"));
        for (ValParam valParam : setValParams) {
          valParamSet.put(PropertySupportFactory.createFor(valParam, this, ccm));
        }
        sheet.put(valParamSet);
      }

    }
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = super.createSheet();
    addPropertyChangeListener(new MethodInterpolationListener());
    updateValParamSet(sheet);
    return sheet;
  }
}
