package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.fudaa.crue.modelling.calcul.ModellingModeleOPTRTopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public abstract class ModellingOpenModeleOPTRNodeAction extends AbstractModellingOpenModeleNodeAction {

  public ModellingOpenModeleOPTRNodeAction(String name) {
    super(name, ModellingModeleOPTRTopComponent.MODE, ModellingModeleOPTRTopComponent.TOPCOMPONENT_ID);
  }

  public static void open(EMHModeleBase modele) {
    if (modele == null) {
      return;
    }
    ModellingOpenModeleOPTRNodeAction action = SystemAction.get(ModellingOpenModeleOPTRNodeAction.Reopen.class);
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(modele));
    action.performAction(new Node[]{node});
  }

  public static class Reopen extends ModellingOpenModeleOPTRNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenModeleOPTRNodeAction.class, "ModellingModeleOPTRNodeAction.Name"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenModeleOPTRNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenModeleOPTRNodeAction.class, "ModellingModeleOPTRNodeAction.NewFrame.Name"));
      setReopen(false);
    }
  }
}
