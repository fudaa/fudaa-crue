/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.list;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.edition.bean.ListBrancheContent;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.modelling.edition.ListAjoutBrancheProcess;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Top component which displays something.
 */
@TopComponent.Description(preferredID = ModellingListBrancheAddTopComponent.TOPCOMPONENT_ID, iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png", persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ModellingListBrancheAddTopComponent.MODE, openAtStartup = false, position = 0)
public final class ModellingListBrancheAddTopComponent extends AbstractModellingListAddTopComponent {
//attention le mode modelling-listAddNoeuds doit être déclaré dans le projet branding (layer.xml)

  public static final String MODE = "modelling-listAddBranches";
  public static final String TOPCOMPONENT_ID = "ModellingListBrancheAddTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;

  public ModellingListBrancheAddTopComponent() {
    super();
    setName(NbBundle.getMessage(ModellingListBrancheAddTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingListBrancheAddTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueAjoutBranches";
  }

  @Override
  protected void addEMHsFromClipboard() {
    addNodesFromClipboard(ListAjoutBrancheProcess.createNodes(perspectiveServiceModelling, getSousModele()));
  }

  @Override
  protected void addEMH() {
    final EMHScenario scenarioLoaded = getModellingService().getScenarioLoaded();
    final Children children = getExplorerManager().getRootContext().getChildren();
    final Collection<String> names = TransformerHelper.toSetNom(scenarioLoaded.getIdRegistry().getEMHs(EnumCatEMH.BRANCHE));
    final Node[] nodes = children.getNodes();
    for (final Node node : nodes) {
      final ListBrancheContent content = node.getLookup().lookup(ListBrancheContent.class);
      if (content != null && StringUtils.isNotEmpty(content.getNom())) {
        names.add(content.getNom());
      }
    }
    final UniqueNomFinder nomFinder = new UniqueNomFinder();
    final String newName = nomFinder.findNewName(names, CruePrefix.P_BRANCHE);

    final List<String> noeudName = ListAjoutBrancheProcess.getAvailableNoeudsNameForBranches(getSousModele());
    Collections.sort(noeudName);
    final ListBrancheContent listBrancheContent = new ListBrancheContent(newName);
    children.add(new Node[]{new ListBrancheContentAddNode(listBrancheContent, noeudName, perspectiveServiceModelling)});
  }

  @Override
  protected void installPropertyColumns() {
    outlineView.setPropertyColumns(ListCommonProperties.PROP_NOM,
            ListBrancheContentNode.getBrancheNameDisplay(),
            ListCommonProperties.PROP_TYPE,
            AbstractListContentNode.getTypeDisplay(),
            ListBrancheContent.PROP_NOEUD_AMONT,
            ListBrancheContentNode.getNoeudAmontDisplayName(),
            ListBrancheContent.PROP_NOEUD_AVAL,
            ListBrancheContentNode.getNoeudAvalDisplayName(),
            ListBrancheContent.PROP_LONGUEUR,
            ListBrancheContentNode.getLongueurDisplayName(),
            ListCommonProperties.PROP_COMMENTAIRE,
            AbstractListContentNode.getCommentDisplay());
  }

  @Override
  protected void doValidationModification() {
    final Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    final ListAjoutBrancheProcess process = new ListAjoutBrancheProcess();
    final EMHSousModele ssModele = (EMHSousModele) getModellingService().getScenarioLoaded().getIdRegistry().getEmh(getSousModeleUid());
    final CtuluLog res = process.processAjout(nodes, ssModele);
    if (res.isEmpty()) {
      clearView();
      setModified(false);
//      close();
    } else {
      LogsDisplayer.displayError(res, getDisplayName());
    }
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
