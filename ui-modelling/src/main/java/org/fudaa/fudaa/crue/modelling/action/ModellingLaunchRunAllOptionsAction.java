package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioModificationService;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;

@ActionID(category = "View",
        id = "org.fudaa.fudaa.crue.modelling.ModellingLaunchRunAllOptionsAction")
@ActionRegistration(displayName = "#CTL_ModellingLaunchRunAllOptionsAction")
@ActionReferences({
  @ActionReference(path = "Actions/Modelling", position = 14)
})
public final class ModellingLaunchRunAllOptionsAction extends AbstractModellingAction {

  final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
          ModellingScenarioModificationService.class);

  public ModellingLaunchRunAllOptionsAction() {
    super(true);
    putValue(Action.NAME, NbBundle.getMessage(ModellingLaunchRunAllOptionsAction.class, "CTL_ModellingLaunchRunAllOptionsAction"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingLaunchRunAllOptionsAction();
  }

  @Override
  public void doAction() {
    if (modellingScenarioModificationService.isModified()) {
      String okMsg = NbBundle.getMessage(ModellingLaunchRunOptionsAction.class, "saveBeforeLaunchingComputationOk");
      String cancelMsg = NbBundle.getMessage(ModellingLaunchRunOptionsAction.class, "saveBeforeLaunchingComputationCancel");
      Object returned = DialogHelper.
              showQuestion(getActionTitle(), NbBundle.getMessage(ModellingLaunchRunOptionsAction.class,
              "saveBeforeLaunchingComputation"), new String[]{okMsg, cancelMsg});
      if (!okMsg.equals(returned)) {
        return;
      }
      scenarioService.saveScenario();
    }
    final ManagerEMHScenario managerEMHScenario = projetService.launchRunAllOptions(scenarioService.getManagerScenarioLoaded());
    scenarioService.updateManagerScenarioLoaded(managerEMHScenario);
  }
}
