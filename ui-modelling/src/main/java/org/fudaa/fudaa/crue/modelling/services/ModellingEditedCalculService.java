/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.services;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ServiceProvider;

/**
 * Conserve le calcul en cours d'édition. Bien utiliser des UID pour ne pas être sensible au clonage.
 * * <table  border="1"  cellpadding="3" cellspacing="0"  summary="lookups">
 * <tr>
 * <th align="left">Lookup</th>
 * <th align="left">Commentaire</th>
 * <th align="left">Méthode utilisant le lookup</th>
 * </tr>
 * <tr>
 * <td>{@link Long} </td>
 * <td>	uid du calcul en cours d'édition</td>
 * <td>
 * <code>{@link #setEditedCalculUid(Long)}</code>
 * <code>{@link #getEditedCalculUid()} </code>
 * </td>
 * </tr>
 * </table>
 *
 * @author Frederic Deniger
 */
@ServiceProvider(service = ModellingEditedCalculService.class)
public class ModellingEditedCalculService implements LookupListener, Lookup.Provider {
    /**
     * ecoute les chargements/dechargement de scenario
     */
    private final Lookup.Result<EMHScenario> resultat;
    /**
     * contient l'uid du calcul en cours d'edition
     */
    protected final InstanceContent dynamicContent = new InstanceContent();
    protected final Lookup lookup = new AbstractLookup(dynamicContent);
    final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);

    public ModellingEditedCalculService() {
        resultat = modellingScenarioService.getLookup().lookupResult(EMHScenario.class);
        resultat.addLookupListener(this);
    }

    /**
     * si le scenario est déchargé, supprime la reference vers le calcul en cours d'édition
     *
     * @param ev l'event
     */
    @Override
    public void resultChanged(LookupEvent ev) {
        //changement massif: on ne fait rien
        if (modellingScenarioService.isReloading()) {
            return;
        }
        if (!modellingScenarioService.isScenarioLoaded()) {
            removeCurrent();
        }
    }

    @Override
    public Lookup getLookup() {
        return lookup;
    }

    /**
     * @return l'uid du calcul en cours d'édition
     */
    public Long getEditedCalculUid() {
        return lookup.lookup(Long.class);
    }

    /**
     * @param uid le nouveau uid du calcul en cours d'édition. Enleve l'ancien
     */
    public void setEditedCalculUid(Long uid) {
        removeCurrent();
        if (uid != null) {
            dynamicContent.add(uid);
        }
    }

    /**
     * Supprime l'uid en cours d'édition
     */
    public void removeCurrent() {
        Long toRemove = getEditedCalculUid();
        if (toRemove != null) {
            dynamicContent.remove(toRemove);
        }
    }
}
