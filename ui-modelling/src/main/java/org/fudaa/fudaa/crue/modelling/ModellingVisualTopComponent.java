package org.fudaa.fudaa.crue.modelling;

import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.fudaa.crue.common.helper.TopComponentHelper;
import org.fudaa.fudaa.crue.modelling.node.ModellingNodeEMHFactory;
import org.fudaa.fudaa.crue.modelling.services.ModellingEditedCalculService;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioVisuService;
import org.fudaa.fudaa.crue.modelling.services.ModellingSelectedEMHService;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.fudaa.fudaa.crue.planimetry.PlanimetryCalqueState;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanelBuilder;
import org.fudaa.fudaa.crue.planimetry.services.PlanimetryModellingController;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.study//ModellingVisual//EN",
    autostore = false)
@TopComponent.Description(preferredID = ModellingVisualTopComponent.TOPCOMPONENT_ID,
    iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png",
    persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "modelling-editor", openAtStartup = false, position = 4)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.modelling.ModellingVisualTopComponent")
@ActionReference(path = "Menu/Window/Modelling", position = 1)
@TopComponent.OpenActionRegistration(displayName = ModellingVisualTopComponent.TOPCOMPONENT_ACTION_DISPLAYNAME,
    preferredID = ModellingVisualTopComponent.TOPCOMPONENT_ID)
public final class ModellingVisualTopComponent extends AbstractModellingTopComponent implements LookupListener, ExplorerManager.Provider {
  public static final String TOPCOMPONENT_ID = PlanimetryModellingController.TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  private final ModellingScenarioVisuService scenarioVisuService = Lookup.getDefault().lookup(ModellingScenarioVisuService.class);
  final ModellingEditedCalculService modellingEditedCalculService = Lookup.getDefault().lookup(ModellingEditedCalculService.class);
  final ModellingSelectedEMHService modellingSelectedEMHService = Lookup.getDefault().lookup(ModellingSelectedEMHService.class);
  private Lookup.Result<PlanimetryVisuPanel> resultatVisuPanel;
  private Lookup.Result<Long> resultatEditedCalcul;
  private Lookup.Result<Long> resultatSelectedEMHs;
  private final ExplorerManager em = new ExplorerManager();
  boolean isUpdatingFromHere;
  final PropertyChangeListener visuChanged = new PropertyChangeListener() {
    @Override
    public void propertyChange(final PropertyChangeEvent evt) {
      setModified(panel.getPlanimetryController().getState().isModified());
    }
  };
  PlanimetryVisuPanel panel;
  LayerSelectionListener selectionListener = null;
  SelectedEMHLookupListener selectedEMHLookupListener;
  private boolean isSelectionUpdating;
  VisuPanelLookupListener visuLookupListener;
  CalculLookupListener calculLookupListener;

  public ModellingVisualTopComponent() {
    initComponents();
    setName(NbBundle.getMessage(ModellingVisualTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingVisualTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    TopComponentHelper.setMainViewProperties(this);
    final ActionMap map = this.getActionMap();
    associateLookup(ExplorerUtils.createLookup(em, map));
  }

  @Override
  public Action[] getActions() {
    return TopComponentHelper.getMainViewActions();
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vuePlanimetrique";
  }

  @Override
  protected void componentActivated() {
    super.componentActivated();
    ExplorerUtils.activateActions(em, true);
    if (panel != null) {
      panel.requestFocusInWindow();
    }
  }

  @Override
  protected void componentDeactivated() {
    super.componentDeactivated();
    ExplorerUtils.activateActions(em, false);
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return em;
  }

  @Override
  protected void scenarioChanged(final ScenarioModificationEvent event) {
    scenarioReloaded();
  }

  @Override
  protected void setEditable(final boolean b) {
    final PlanimetryVisuPanel planimetryVisuPanel = scenarioVisuService.getPlanimetryVisuPanel();
    if (planimetryVisuPanel != null) {
      planimetryVisuPanel.getPlanimetryController().setEditable(b);
    }
  }

  @Override
  public void cancelModificationHandler() {
    //peut arriver si la panneau a été fermé.
    if (panel != null) {
      panel.getPlanimetryController().cancel();
    }
    setModified(false);
  }

  @Override
  public void valideModificationHandler() {
    final PlanimetryCalqueState state = panel.getPlanimetryController().getState();
    final boolean scenarioModified = state.isScenarioModified();
    final boolean geoLocModified = state.isGeoLocModified();
    final boolean configModified = state.isStudyConfigModified();

    if (geoLocModified) {
      modellingScenarioModificationService.setGeoLocModified();
    }
    if (configModified) {
      modellingScenarioModificationService.setStudyConfigModified();
    }
    if (state.isExternDataModified()) {
      modellingScenarioModificationService.setStudyExternConfigModified();
    }
    panel.getPlanimetryController().saveDone();
    if (scenarioModified) {
      isUpdatingFromHere = true;
      modellingScenarioModificationService.setScenarioModifiedMassive(
          panel.getPlanimetryController().getPlanimetryGisModelContainer().getScenario());
      isUpdatingFromHere = false;
    }
    setModified(false);
  }

  @Override
  public void setModified(final boolean modified) {
    if (modified && WindowManager.getDefault().getRegistry().getActivated() != this) {
      final PlanimetryCalqueState state = panel.getPlanimetryController().getState();
      assert state.isGeoLocModified();
      assert !state.isScenarioModified();
      assert !state.isStudyConfigModified();
      panel.getPlanimetryController().saveDone();
      modellingScenarioModificationService.setGeoLocModified();
    } else {
      super.setModified(modified);
    }
  }

  @Override
  protected void scenarioLoaded() {
    if (scenarioVisuService.getPlanimetryVisuPanel() != null) {
      scenarioVisuLoaded();
    }
  }

  protected void scenarioVisuLoaded() {
    this.removeAll();
    panel = scenarioVisuService.getPlanimetryVisuPanel();
    updateEditedCalcul();
    selectionListener = new LayerSelectionListener();
    panel.getScene().addSelectionListener(selectionListener);
    panel.getPlanimetryController().getState().addModifiedListener(visuChanged);
    final JPanel main = new JPanel();
    main.setLayout(new BorderLayout());
    panel.setPreferredSize(new Dimension(600, 700));
    main.add(createToolbar(panel), BorderLayout.NORTH);
    main.add(panel.createGisEditionPalette(), BorderLayout.WEST);
    main.add(panel);
    add(main);
    final JPanel pnSaveCancel = createCancelSaveButtons();
    final JPanel bottom = new JPanel(new BorderLayout(0, 5));
    bottom.add(panel.getEditZonePanel().getPanel(), BorderLayout.WEST);
    bottom.add(pnSaveCancel, BorderLayout.EAST);
    main.add(bottom, BorderLayout.SOUTH);
    this.repaint();
    super.revalidate();
    EventQueue.invokeLater(() -> {
      panel.getPlanimetryController().setEditable(false);
      panel.restaurer();
    });
  }

  public void updateEditedCalcul() {
    panel.getPlanimetryController().setEditedCalcul(modellingEditedCalculService.getEditedCalculUid());
  }

  protected void selectionChangedInPanel() {
    if (isSelectionUpdating) {
      return;
    }
    final List<EMH> selected = panel.getSelectedEMHs();
    //pour mettre à jour le service contenant les emhs sélectionnées
    modellingSelectedEMHService.setSelectedEMHs(selected);
    //pour mettre à jour la fenêtre propriétés
    ModellingNodeEMHFactory.updateExplorerManagerWithSelectedEmhs(getExplorerManager(), selected);
  }

  /**
   * Ne fait rien
   */
  @Override
  protected void scenarioReloaded() {
    //si le scenario est mis à jour depuis ce composant, on ne fait rien
    if (panel != null && !isUpdatingFromHere) {
      panel.getPlanimetryController().reload(getModellingService().getScenarioLoaded());
      updateEditedCalcul();
    }
  }

  protected JComponent createToolbar(final PlanimetryVisuPanel panel) {
    return PlanimetryVisuPanelBuilder.buildTopComponent(panel);
  }

  @Override
  protected void scenarioUnloaded() {
    if (panel != null) {
      panel.getPlanimetryController().getState().removeModifiedListener(visuChanged);
      panel.getScene().removeSelectionListener(selectionListener);
    }
    panel = null;
    this.removeAll();
    this.add(new JLabel(MessagesModelling.getMessage("emhTopComponent.NoScenarioLoadedInformations")));
    super.revalidate();
    this.repaint();
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables

  @Override
  protected void componentOpenedHandler() {
    if (resultatVisuPanel == null) {
      visuLookupListener = new VisuPanelLookupListener();
      resultatVisuPanel = scenarioVisuService.getLookup().lookupResult(PlanimetryVisuPanel.class);
      resultatVisuPanel.addLookupListener(visuLookupListener);
    }
    if (resultatEditedCalcul == null) {
      calculLookupListener = new CalculLookupListener();
      resultatEditedCalcul = modellingEditedCalculService.getLookup().lookupResult(Long.class);
      resultatEditedCalcul.addLookupListener(calculLookupListener);
    }
    if (resultatSelectedEMHs == null) {
      selectedEMHLookupListener = new SelectedEMHLookupListener();
      resultatSelectedEMHs = modellingSelectedEMHService.getLookup().lookupResult(Long.class);
      resultatSelectedEMHs.addLookupListener(selectedEMHLookupListener);
    }
  }

  @Override
  protected void componentClosedDefinitlyHandler() {
    if (resultatVisuPanel != null) {
      resultatVisuPanel.removeLookupListener(visuLookupListener);
      resultatVisuPanel = null;
    }
    if (resultatEditedCalcul != null) {
      resultatEditedCalcul.removeLookupListener(calculLookupListener);
      resultatEditedCalcul = null;
    }
    if (resultatSelectedEMHs != null) {
      resultatSelectedEMHs.removeLookupListener(selectedEMHLookupListener);
      resultatSelectedEMHs = null;
    }
  }

  void writeProperties(final java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
    p.setProperty("version", "1.0");
  }

  void readProperties(final java.util.Properties p) {
  }

  private class LayerSelectionListener implements ZSelectionListener {
    @Override
    public void selectionChanged(final ZSelectionEvent _evt) {
      selectionChangedInPanel();
    }
  }

  private class VisuPanelLookupListener implements LookupListener {
    @Override
    public void resultChanged(final LookupEvent ev) {
      if (scenarioVisuService.getPlanimetryVisuPanel() != null) {
        scenarioVisuLoaded();
      }
    }
  }

  private class CalculLookupListener implements LookupListener {
    @Override
    public void resultChanged(final LookupEvent ev) {
      if (scenarioVisuService.getPlanimetryVisuPanel() != null) {
        scenarioVisuService.getPlanimetryVisuPanel().getPlanimetryController().setEditedCalcul(modellingEditedCalculService.getEditedCalculUid());
      }
    }
  }

  private class SelectedEMHLookupListener implements LookupListener {
    @Override
    public void resultChanged(final LookupEvent ev) {

      if (panel == null || WindowManager.getDefault().getRegistry().getActivated() == ModellingVisualTopComponent.this) {
        return;
      }
      if (modellingSelectedEMHService.isUpdating()) {
        return;
      }
      final Collection<? extends Long> allItems = resultatSelectedEMHs.allInstances();
      final Collection<Long> uids = new ArrayList<>(allItems);
      isSelectionUpdating = true;
      panel.setSelectedEMHFromUids(uids);
      isSelectionUpdating = false;
    }
  }
}
