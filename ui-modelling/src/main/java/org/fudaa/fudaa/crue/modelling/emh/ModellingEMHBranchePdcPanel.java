/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.emh;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.edition.bean.BrancheEditionContent;
import org.fudaa.fudaa.crue.modelling.loi.ModellingOpenDcspLoiNodeAction;
import org.openide.explorer.ExplorerManager;

/**
 *
 * @author fred
 */
public class ModellingEMHBranchePdcPanel extends JPanel implements
        ModellingEMHBrancheSpecificEditor {

  private final Long brancheUid;

  public ModellingEMHBranchePdcPanel(Long brancheUid) {
    super(new FlowLayout(FlowLayout.LEFT, 10, 5));
    this.brancheUid = brancheUid;
    add(new JLabel(org.openide.util.NbBundle.getMessage(ModellingEMHBranchePdcPanel.class, "loiPdc.DisplayName")));
    JButton bt = new JButton(org.openide.util.NbBundle.getMessage(ModellingEMHBranchePdcPanel.class, "loiPdcButton.DisplayName"));
    bt.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        openLoi();
      }
    });
    add(bt);
  }

  @Override
  public void setEditable(boolean editable) {
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return null;
  }

  @Override
  public void fillWithData(BrancheEditionContent content) {
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  public void openLoi() {
       ModellingOpenDcspLoiNodeAction.open(brancheUid, EnumTypeLoi.LoiQPdc, false);
  }
}
