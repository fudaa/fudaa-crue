package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.edition.EditionBrancheSaintVenantInternSplit;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.fudaa.crue.modelling.list.ListRelationSectionContentNode;
import org.fudaa.fudaa.crue.modelling.list.ModellingListBrancheSplitTopComponent;
import org.openide.nodes.Node;
import org.openide.windows.TopComponent;

import static org.openide.util.NbBundle.getMessage;

/**
 * @author deniger
 */
public class ModellingOpenSplitBrancheNodeAction extends AbstractModellingOpenTopNodeAction<ModellingListBrancheSplitTopComponent> {
  public ModellingOpenSplitBrancheNodeAction() {

    super(getMessage(ModellingOpenSplitBrancheNodeAction.class, "ModellingOpenSplitBrancheNodeAction.Name"), ModellingListBrancheSplitTopComponent.MODE,
        ModellingListBrancheSplitTopComponent.TOPCOMPONENT_ID);
    setReopen(true);
  }

  @Override
  protected TopComponent activateNewTopComponent(final ModellingListBrancheSplitTopComponent newTopComponent, final Node selectedNode) {
    newTopComponent.setSection(getCatEMHSection(selectedNode));
    return newTopComponent;
  }

  @Override
  protected void activeWithValue(final ModellingListBrancheSplitTopComponent opened, final Node selectedNode) {
    opened.setSection(getCatEMHSection(selectedNode));
  }

  @Override
  public boolean isEnable(final Node[] activatedNodes) {
    return getSection(activatedNodes) != null;
  }

  private CatEMHSection getSection(final Node[] activatedNodes) {
    if (activatedNodes == null || activatedNodes.length != 1) {
      return null;
    }
    final Node node = activatedNodes[0];
    return getCatEMHSection(node);
  }

  private CatEMHSection getCatEMHSection(final Node node) {
    if (node instanceof ListRelationSectionContentNode) {
      final ListRelationSectionContentNode sectionContentNode = (ListRelationSectionContentNode) node;
      if (!sectionContentNode.isEditMode()) {
        return null;
      }
      final CatEMHSection section = sectionContentNode.getSection();
      if (section.getBranche() == null || !EditionBrancheSaintVenantInternSplit.isSectionUsableForSplit(section)) {
        return null;
      }
      if (EnumBrancheType.EMHBrancheSaintVenant.equals(section.getBranche().getBrancheType())) {
        if (section != section.getBranche().getSectionAval().getEmh() && section != section.getBranche().getSectionAmont().getEmh()) {
          return section;
        }
      }
    }
    return null;
  }
}
