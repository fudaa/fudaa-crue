/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import javax.swing.Action;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.fudaa.crue.modelling.perspective.ActiveModelisation;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View", id = "org.fudaa.fudaa.crue.modelling.ModellingOpenModelePNUMAction")
@ActionRegistration(displayName = "#ModellingModelePNUMNodeAction.Name")
@ActionReferences({
  @ActionReference(path = ActiveModelisation.ACTIONS_MODELLING_VIEWS, position = 8)
})
public final class ModellingOpenModelePNUMAction extends AbstractModellingModeleOpenAction {

  public ModellingOpenModelePNUMAction() {
    putValue(Action.NAME, NbBundle.getMessage(ModellingOpenModelePNUMAction.class, "ModellingModelePNUMNodeAction.Name"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingOpenModelePNUMAction();
  }

  @Override
  protected void doAction(EMHModeleBase modele) {
    ModellingOpenModelePNUMNodeAction.open(modele);
  }
}
