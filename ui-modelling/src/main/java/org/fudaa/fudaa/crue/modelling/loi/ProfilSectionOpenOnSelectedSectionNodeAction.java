package org.fudaa.fudaa.crue.modelling.loi;

import org.fudaa.dodico.crue.metier.emh.EMHSectionProfil;
import org.fudaa.fudaa.crue.modelling.action.AbstractModellingOpenTopNodeAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;

/**
 *
 * Ouvre l'éditeur de ProfilSection en sélectionnant une section.
 *
 * @author Frédéric Deniger
 */
public class ProfilSectionOpenOnSelectedSectionNodeAction extends AbstractModellingOpenTopNodeAction<ProfilSectionTopComponent> {

  public ProfilSectionOpenOnSelectedSectionNodeAction(String name) {
    super(name, ProfilSectionTopComponent.MODE, ProfilSectionTopComponent.TOPCOMPONENT_ID);
  }

  public ProfilSectionOpenOnSelectedSectionNodeAction() {
    super(NbBundle.getMessage(ProfilSectionOpenOnSelectedSectionNodeAction.class, "ModellingOpenProfilSectionNodeAction.DisplayName"), ProfilSectionTopComponent.MODE, ProfilSectionTopComponent.TOPCOMPONENT_ID);
  }

  @Override
  protected void activeWithValue(ProfilSectionTopComponent opened, Node selectedNode) {
    final Lookup lookup = selectedNode.getLookup();
    Long uid = lookup.lookup(Long.class);
    if (uid == null) {
      EMHSectionProfil profil = lookup.lookup(EMHSectionProfil.class);
      if (profil != null) {
        uid = profil.getUiId();
      }
    }
    if (uid != null) {
      opened.selectProfil(uid);
    }
  }

  @Override
  protected TopComponent activateNewTopComponent(ProfilSectionTopComponent top, Node selectedNode) {
    final Lookup lookup = selectedNode.getLookup();
    Long uid = lookup.lookup(Long.class);
    if (uid == null) {
      EMHSectionProfil profil = lookup.lookup(EMHSectionProfil.class);
      if (profil != null) {
        uid = profil.getUiId();
      }
    }
    if (uid != null) {
      top.putClientProperty(ProfilSectionTopComponent.PROPERTY_INIT_UID, uid);
    }
    return top;
  }

  public static void open(Long sectionUid, boolean reopen) {
    if (sectionUid == null) {
      return;
    }
    ProfilSectionOpenOnSelectedSectionNodeAction action = null;
    if (reopen) {
      action = SystemAction.get(ProfilSectionOpenOnSelectedSectionNodeAction.Reopen.class);
    } else {
      action = SystemAction.get(ProfilSectionOpenOnSelectedSectionNodeAction.NewFrame.class);
    }
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(sectionUid));
    action.performAction(new Node[]{node});
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    if (activatedNodes.length == 1) {
      final Lookup lookup = activatedNodes[0].getLookup();
      Long uid = lookup.lookup(Long.class);
      if (uid == null) {
        return lookup.lookup(EMHSectionProfil.class) != null;
      }
      return true;
    }
    return false;
  }

  public static class Reopen extends ProfilSectionOpenOnSelectedSectionNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ProfilSectionOpenOnSelectedSectionNodeAction.class, "ModellingOpenProfilSectionNodeAction.DisplayName"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ProfilSectionOpenOnSelectedSectionNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ProfilSectionOpenOnSelectedSectionNodeAction.class, "ModellingOpenProfilSectionNodeAction.NewFrame.DisplayName"));
      setReopen(false);
    }
  }
}
