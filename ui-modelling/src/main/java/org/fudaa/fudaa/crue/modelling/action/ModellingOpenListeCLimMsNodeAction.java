package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.modelling.calcul.ModellingListCLimMsTopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public abstract class ModellingOpenListeCLimMsNodeAction extends AbstractModellingOpenScenarioNodeAction {

  public ModellingOpenListeCLimMsNodeAction(String name) {
    super(name, ModellingListCLimMsTopComponent.MODE, ModellingListCLimMsTopComponent.TOPCOMPONENT_ID);
  }

  public static void open(EMHScenario scenario) {
    if (scenario == null) {
      return;
    }
    ModellingOpenListeCLimMsNodeAction action = SystemAction.get(ModellingOpenListeCLimMsNodeAction.Reopen.class);
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(scenario));
    action.performAction(new Node[]{node});
  }

  public static class Reopen extends ModellingOpenListeCLimMsNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenListeCLimMsNodeAction.class, "ModellingOpenListeCLimMsNodeAction.Name"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenListeCLimMsNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenListeCLimMsNodeAction.class, "ModellingOpenListeCLimMsNodeAction.NewFrame.Name"));
      setReopen(false);
    }
  }
}
