/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.loi;

import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.fudaa.crue.modelling.action.AbstractModellingOpenTopNodeAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;

/**
 *
 * @author Frédéric Deniger
 */
public class ModellingOpenDcspLoiNodeAction extends AbstractModellingOpenTopNodeAction<DcspEditorLoiTopComponent> {

  public ModellingOpenDcspLoiNodeAction(final String name) {
    super(name, DcspEditorLoiTopComponent.MODE, DcspEditorLoiTopComponent.TOPCOMPONENT_ID);
  }

  public ModellingOpenDcspLoiNodeAction() {
    super(NbBundle.getMessage(ModellingOpenDcspLoiNodeAction.class, "ModellingOpenDcspLoiNodeAction.DisplayName"), DcspEditorLoiTopComponent.MODE, DcspEditorLoiTopComponent.TOPCOMPONENT_ID);
  }

  @Override
  protected void activeWithValue(final DcspEditorLoiTopComponent opened, final Node selectedNode) {
    final Long uid = selectedNode.getLookup().lookup(Long.class);
    final EnumTypeLoi typeLoi = selectedNode.getLookup().lookup(EnumTypeLoi.class);
    opened.setLoi(uid, typeLoi);
  }

  @Override
  protected TopComponent activateNewTopComponent(final DcspEditorLoiTopComponent newTopComponent, final Node selectedNode) {
    final Long uid = selectedNode.getLookup().lookup(Long.class);
    final EnumTypeLoi typeLoi = selectedNode.getLookup().lookup(EnumTypeLoi.class);
    newTopComponent.setLoi(uid, typeLoi);
    return newTopComponent;
  }


  public static void open(final Long uid, final EnumTypeLoi typeLoi, final boolean reopen) {
    if (uid == null) {
      return;
    }
    final ModellingOpenDcspLoiNodeAction action;
    if (reopen) {
      action = SystemAction.get(ModellingOpenDcspLoiNodeAction.Reopen.class);
    } else {
      action = SystemAction.get(ModellingOpenDcspLoiNodeAction.NewFrame.class);
    }
    final AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(uid, typeLoi));
    action.performAction(new Node[]{node});
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    if (activatedNodes.length == 1) {
      final Long uid = activatedNodes[0].getLookup().lookup(Long.class);
      return uid != null;
    }
    return false;
  }

  public static class Reopen extends ModellingOpenDcspLoiNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenDcspLoiNodeAction.class, "ModellingOpenDcspLoiNodeAction.DisplayName"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenDcspLoiNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenDcspLoiNodeAction.class, "ModellingOpenDcspLoiNodeAction.NewFrame.DisplayName"));
      setReopen(false);
    }
  }
}
