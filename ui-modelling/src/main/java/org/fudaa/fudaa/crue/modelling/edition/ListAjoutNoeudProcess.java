package org.fudaa.fudaa.crue.modelling.edition;

import com.Ostermiller.util.CSVParser;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.EditionCreateEMH;
import org.fudaa.dodico.crue.edition.bean.ListNoeudContent;
import org.fudaa.dodico.crue.edition.bean.ValidationMessage;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.list.ListNoeudContentAddNode;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ModellingConfigService;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioModificationService;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.util.*;
import java.util.List;

/**
 * @author deniger
 */
public class ListAjoutNoeudProcess {
    protected final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
            ModellingScenarioModificationService.class);
    final ModellingConfigService modellingConfigService = Lookup.getDefault().lookup(ModellingConfigService.class);

    public static List<ListNoeudContentAddNode> createNodes(PerspectiveServiceModelling perspectiveServiceModelling) {
        try {
            String toCopy = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
            String[][] parse = CSVParser.parse(toCopy, '\t');
            List<ListNoeudContentAddNode> contents = new ArrayList<>();
            for (String[] strings : parse) {
                if (strings.length >= 1) {
                    ListNoeudContent newContent = new ListNoeudContent(strings[0]);
                    if (strings.length >= 2) {
                        newContent.setCommentaire(strings[1]);
                    }
                    contents.add(new ListNoeudContentAddNode(newContent, perspectiveServiceModelling));
                }
            }
            return contents;
        } catch (Exception exception) {
        }
        return Collections.emptyList();
    }

    /**
     * @param newNoeuds  liste de ListNoeudContentAddNode
     * @param sousModele le sous-modele
     * @return le log de l'opration
     */
    public CtuluLog processAjout(Node[] newNoeuds, EMHSousModele sousModele) {
        CrueConfigMetier ccm = modellingScenarioModificationService.getModellingScenarioService().getSelectedProjet().getCoeurConfig().getCrueConfigMetier();
        CtuluLog log = new CtuluLog();
        Map<ListNoeudContent, ListNoeudContentAddNode> contents = getContentList(newNoeuds);
        EditionCreateEMH createProcess = new EditionCreateEMH(modellingConfigService.getDefaultValues());
        final ValidationMessage<ListNoeudContent> createNoeudsMessages = createProcess.createNoeuds(sousModele, contents.keySet(), ccm);
        if (createNoeudsMessages.accepted) {
            modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.EMH_CREATION));
        } else {
            List<ValidationMessage.ResultItem<ListNoeudContent>> msgs = createNoeudsMessages.messages;
            for (ValidationMessage.ResultItem<ListNoeudContent> resultItem : msgs) {
                ListNoeudContentAddNode node = contents.get(resultItem.bean);
                node.setContainError(true);
                node.setShortDescription("<html><body>" + StringUtils.join(resultItem.messages, "<br>") + "</body></html>");
            }
        }
        return log;
    }

    private LinkedHashMap<ListNoeudContent, ListNoeudContentAddNode> getContentList(Node[] newNoeuds) {
        LinkedHashMap<ListNoeudContent, ListNoeudContentAddNode> contents = new LinkedHashMap<>();
        for (Node node : newNoeuds) {
            ListNoeudContent content = node.getLookup().lookup(ListNoeudContent.class);
            if (content != null) {
                contents.put(content, (ListNoeudContentAddNode) node);
            }
        }
        return contents;
    }
}
