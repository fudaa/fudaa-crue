/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluFileChooserCsvExcel;
import org.fudaa.ctulu.gui.CtuluTableSimpleExporter;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ctulu.table.CtuluTableModelDefault;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.dpti.DPTIToCiniImportKeyTransformer;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.cini.CiniImportKey;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.commun.impl.FudaaGuiLib;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.node.OutlineViewExportTableModel;
import org.fudaa.fudaa.crue.loader.CreateDPTIFromResultProcess;
import org.fudaa.fudaa.crue.modelling.calcul.importer.CiniImporter;
import org.fudaa.fudaa.crue.modelling.calcul.importer.RunCalculChooser;
import org.fudaa.fudaa.crue.modelling.list.ListNodeAddPopupFactory;
import org.fudaa.fudaa.crue.common.CrueFileTypeFilter;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.NodePopupFactory;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Collections;
import java.util.Map;

/**
 * @author Frederic Deniger
 */
public final class ModellingListCiniTopComponentPopupFactory extends NodePopupFactory {
  private final ModellingListCiniTopComponent topComponent;

  public ModellingListCiniTopComponentPopupFactory(final ModellingListCiniTopComponent tc) {
    this.topComponent = tc;
    setShowQuickFilter(false);
    final ActionMap map = tc.getActionMap();
    final InputMap keys = tc.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    keys.put(ListNodeAddPopupFactory.getDeleteKeyStroke(), "delete");
    keys.put(getPasteKeyStroke(), "paste");
    map.put("paste", new AbstractAction() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        importFromClipboard();
      }
    }); //NOI18N
  }

  @Override
  public JPopupMenu createPopupMenu(final int row, final int column, final Node[] selectedNodes, final Component component) {
    final JPopupMenu res = super.createPopupMenu(row, column, selectedNodes, component);
    if (res.getComponentCount() == 1) {
      // ajout du séparateur uniquement si un item est présent avant
      res.addSeparator();
    }

    final JMenuItem menuItemPaste = new JMenuItem(NbBundle.getMessage(ModellingListCiniTopComponentPopupFactory.class,
        "button.importFromClipboard.name"));
    menuItemPaste.setToolTipText(NbBundle.getMessage(ModellingListCiniTopComponentPopupFactory.class, "button.importFromClipboard.tooltip"));
    menuItemPaste.setEnabled(topComponent.isEditable());
    menuItemPaste.setAccelerator(getPasteKeyStroke());
    menuItemPaste.setIcon(EbliResource.EBLI.getToolIcon("crystal_coller"));
    menuItemPaste.addActionListener(e -> importFromClipboard());

    final JMenuItem menuItemImport = new JMenuItem(getMessageImport());
    menuItemImport.setToolTipText(NbBundle.getMessage(ModellingListCiniTopComponentPopupFactory.class, "button.importCini.tooltip"));
    menuItemImport.setIcon(EbliResource.EBLI.getToolIcon("crystal_importer"));
    menuItemImport.setEnabled(topComponent.isEditable());
    menuItemImport.addActionListener(e -> importTable());

    final JMenuItem menuItemImportFromResult = new JMenuItem(getMessageImportFromResult());
    menuItemImportFromResult.setToolTipText(NbBundle.getMessage(ModellingListCiniTopComponentPopupFactory.class, "button.importFromResult.tooltip"));
    menuItemImportFromResult.setIcon(EbliResource.EBLI.getToolIcon("crystal_importer"));
    menuItemImportFromResult.setEnabled(topComponent.isEditable());
    menuItemImportFromResult.addActionListener(e -> importFromResult());

    final JMenuItem menuItemImportFromDPTI = new JMenuItem(getMessageImportFromDPTI());
    menuItemImportFromDPTI.setToolTipText(NbBundle.getMessage(ModellingListCiniTopComponentPopupFactory.class, "button.importFromDPTI.tooltip"));
    menuItemImportFromDPTI.setIcon(EbliResource.EBLI.getToolIcon("crystal_importer"));
    menuItemImportFromDPTI.setEnabled(topComponent.isEditable());
    menuItemImportFromDPTI.addActionListener(e -> importFromDPTI());

    final JMenuItem menuItemExport = new JMenuItem(NbBundle.getMessage(ModellingListCiniTopComponentPopupFactory.class, "button.export.name"));
    menuItemExport.addActionListener(e -> exportTable());
    menuItemExport.setIcon(EbliResource.EBLI.getToolIcon("crystal_exporter"));
    res.add(menuItemPaste);
    res.add(menuItemImport);
    res.add(menuItemImportFromDPTI);
    res.add(menuItemImportFromResult);
    res.addSeparator();
    res.add(menuItemExport);

    return res;
  }

  private void importFromDPTI() {
    final CtuluFileChooser fileChooser = new CtuluFileChooser(true);
    fileChooser.addChoosableFileFilter(new CrueFileTypeFilter(Collections.singletonList(CrueFileType.DPTI)));
    fileChooser.setMultiSelectionEnabled(false);
    fileChooser.setAcceptAllFileFilterUsed(false);
    final File chooseFile = FudaaGuiLib.chooseFile(WindowManager.getDefault().getMainWindow(), false, fileChooser);
    if (chooseFile != null) {
      CrueProgressUtils.showProgressDialogAndRun(progressHandle -> {
        progressHandle.switchToIndeterminate();
        final CrueIOResu<Map<CiniImportKey, Object>> read = DPTIToCiniImportKeyTransformer.read(chooseFile, topComponent.getCoeurConfig());
        if (read.getAnalyse().containsErrorOrSevereError()) {
          LogsDisplayer.displayError(read.getAnalyse(), getMessageImportFromDPTI());
        }
        if (read.getMetier() != null) {
          final ExplorerManager explorerManager = topComponent.getExplorerManager();
          final CiniImporter importer = new CiniImporter(explorerManager.getRootContext());
          importer.setExplorerManager(explorerManager);
          importer.setToImport(read.getMetier());
          importer.importData(read.getAnalyse());
          LogsDisplayer.displayError(read.getAnalyse(), getMessageImportFromDPTI());
        }
        return null;
      }, getMessageImportFromDPTI());
    }
  }

  private String getMessageImport() {
    return NbBundle.getMessage(ModellingListCiniTopComponentPopupFactory.class, "button.import.name");
  }

  private String getMessageImportFromResult() {
    return NbBundle.getMessage(ModellingListCiniTopComponentPopupFactory.class, "button.importFromResult.name");
  }

  private String getMessageImportFromDPTI() {
    return NbBundle.getMessage(ModellingListCiniTopComponentPopupFactory.class, "button.importFromDPTI.name");
  }

  private void importFromResult() {
    final Pair<EMHScenario, ResultatTimeKey> values = new RunCalculChooser().choose();
    if (values != null) {
      final Map<CiniImportKey, Object> extracted = CreateDPTIFromResultProcess.extract(topComponent.getCoeurConfig().getCrueConfigMetier(),values.first, values.second);
      if (extracted != null) {
        CrueProgressUtils.showProgressDialogAndRun(
            progressHandle -> {
              progressHandle.switchToIndeterminate();
              final CtuluLog log = new CtuluLog();
              log.setDesc(getMessageImportFromResult());
              final ExplorerManager explorerManager = topComponent.getExplorerManager();
              final CiniImporter importer = new CiniImporter(explorerManager.getRootContext());
              importer.setExplorerManager(explorerManager);
              importer.setToImport(extracted);
              importer.importData(log);
              LogsDisplayer.displayError(log, log.getDesc());
              return null;
            }
            , getMessageImportFromResult());
      }
    }
  }

  private void exportTable() {
    final CtuluTableModelDefault defaultModel = new OutlineViewExportTableModel(topComponent.outlineView.getOutline(), null, null);
    CtuluTableSimpleExporter.doExport(';', defaultModel, CtuluUIForNetbeans.DEFAULT);
  }

  private void importFromClipboard() {
    if (!topComponent.isEditable()) {
      return;
    }
    String value = null;
    try {
      value = CtuluTable.getClipboard();
    } catch (final Exception ex) {
      Exceptions.printStackTrace(ex);
    }
    if (value != null) {
      CrueProgressUtils.showProgressDialogAndRun(new ModellingListCiniImportProgressRunnable(value, topComponent), getMessageImport());
    }
  }

  private void importTable() {
    if (!topComponent.isEditable()) {
      return;
    }
    final CtuluFileChooser fileChooser = CtuluFileChooserCsvExcel.createOpenFileChooser();
    final int res = fileChooser.showDialog(CtuluUIForNetbeans.DEFAULT.getParentComponent(), NbBundle.getMessage(ModellingListCiniTopComponent.class,
        "button.import.name"));
    if (res == JFileChooser.APPROVE_OPTION) {
      CrueProgressUtils.showProgressDialogAndRun(new ModellingListCiniImportProgressRunnable(fileChooser.getSelectedFile(), topComponent), getMessageImport());
    }
  }

  private static KeyStroke getPasteKeyStroke() {
    return KeyStroke.getKeyStroke("control V");
  }
}
