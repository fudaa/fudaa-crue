/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.ValParam;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.modelling.node.AbstractModellingNodeFirable;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frédéric Deniger
 */
public class ValParamNode extends AbstractModellingNodeFirable {

  public ValParamNode(ValParam valParam, CrueConfigMetier ccm, PerspectiveServiceModelling perspectiveServiceModelling) {
    super(Children.LEAF, Lookups.fixed(valParam, ccm), perspectiveServiceModelling);
    setDisplayName(valParam.getNom());
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(getLookup().lookup(ValParam.class)));
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
    try {
      Sheet.Set set = Sheet.createPropertiesSet();
      sheet.put(set);
      ValParam valParam = getLookup().lookup(ValParam.class);
      PropertySupportReflection support = new PropertySupportReflection(this, valParam, valParam.getTypeData(), ValParam.PROP_VALEUR);
      support.setName(ValParam.PROP_VALEUR);
      support.setDisplayName(BusinessMessages.getString("valParamValeur.property"));
      set.put(support);
      return sheet;
    } catch (NoSuchMethodException ex) {
      Exceptions.printStackTrace(ex);
    }
    return sheet;
  }
}
