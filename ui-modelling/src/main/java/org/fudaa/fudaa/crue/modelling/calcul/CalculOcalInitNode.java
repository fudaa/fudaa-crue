/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.emh.OrdCalc;
import org.fudaa.dodico.crue.metier.factory.OrdCalcFactory;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.modelling.node.AbstractModellingNodeFirable;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 * @author Frederic Deniger
 */
public class CalculOcalInitNode extends AbstractModellingNodeFirable {
  public static class InitMethod {
    Node parentNode;

    public Node getParentNode() {
      return parentNode;
    }

    public void setParentNode(final Node parentNode) {
      this.parentNode = parentNode;
    }

    String initMethod;

    public InitMethod(final String initMethod) {
      this.initMethod = initMethod;
    }

    @SuppressWarnings("unused")
    @PropertyDesc(i18n = "initMethod.property")
    public String getInitMethod() {
      return initMethod;
    }

    @SuppressWarnings("unused")
    public void setInitMethod(final String initMethod) {
      this.initMethod = initMethod;
      final CalculOcalNode ocalNode = (CalculOcalNode) parentNode.getParentNode();
      ocalNode.changeInitMethod(initMethod);
    }
  }

  public CalculOcalInitNode(final OrdCalc calc, final PerspectiveServiceModelling perspectiveServiceModelling) {
    super(Children.LEAF, Lookups.fixed(calc, new InitMethod(OrdCalcFactory.findName(calc))), perspectiveServiceModelling);
    setName(BusinessMessages.getString("initMethod.property"));
    getInitMethod().setParentNode(this);
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(getLookup().lookup(OrdCalc.class)));
  }

  public OrdCalc getCalc() {
    return getLookup().lookup(OrdCalc.class);
  }

  @Override
  public SystemAction getDefaultAction() {
    return SystemAction.get(CalculOpenListCondInitNodeAction.class);
  }

  @Override
  public SystemAction[] getActions() {
    return new SystemAction[]{SystemAction.get(CalculOpenListCondInitNodeAction.class)};
  }

  private InitMethod getInitMethod() {
    return getLookup().lookup(InitMethod.class);
  }

  @Override
  protected Sheet createSheet() {
    final PropertyNodeBuilder builder = new PropertyNodeBuilder();
    final PropertySupportReflection value = builder.createFromPropertyDesc(DecimalFormatEpsilonEnum.COMPARISON, getInitMethod(), this, "initMethod");
    value.setName(CalculNode.COLUMN_VALUE);
    final OrdCalc calc = getCalc();
    value.setTags(calc.isTransitoire() ? OrdCalcFactory.getOrdCalTransitoireNames() : OrdCalcFactory.getOrdCalPseudoNames());
    final Sheet res = Sheet.createDefault();
    final Sheet.Set set = Sheet.createPropertiesSet();
    set.put(value);
    res.put(set);
    return res;
  }
}
