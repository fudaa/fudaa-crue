/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.loi;

import javax.swing.Action;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.fudaa.crue.modelling.action.*;
import org.fudaa.fudaa.crue.modelling.perspective.ActiveModelisation;
import org.fudaa.fudaa.crue.views.LoiDisplayer;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
        id = "org.fudaa.fudaa.crue.modelling.ModellingOpenDLHYAction")
@ActionRegistration(displayName = "#ModellingOpenDLHYNodeAction.DisplayName")
@ActionReferences({
  @ActionReference(path = ActiveModelisation.ACTIONS_MODELLING_VIEWS, position = 3, separatorAfter = 4)
})
public final class ModellingOpenDLHYAction extends AbstractModellingOpenAction implements LoiDisplayer {

  public ModellingOpenDLHYAction() {
    putValue(Action.NAME, NbBundle.getMessage(ModellingOpenDLHYAction.class, "ModellingOpenDLHYNodeAction.DisplayName"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingOpenDLHYAction();
  }

  @Override
  public void doAction() {
    ModellingOpenDLHYNodeAction.open(null, true);
  }

  @Override
  public void display(Loi loi) {
    ModellingOpenDLHYNodeAction.open(loi, true);
  }
}
