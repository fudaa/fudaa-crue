package org.fudaa.fudaa.crue.modelling.list;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import com.memoire.bu.BuBorders;
import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.node.NodeChildrenHelper;
import org.fudaa.fudaa.crue.modelling.emh.ModellingOpenEMHNodeAction;
import org.openide.explorer.ExplorerUtils;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Node;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public abstract class AbstractModellingListEditionTopComponent extends AbstractModellingListTopComponent {

  public AbstractModellingListEditionTopComponent() {
    super();
    setBorder(BuBorders.EMPTY0505);
    associateLookup(ExplorerUtils.createLookup(super.explorerHelper.getExplorerManager(), getActionMap()));
  }

  @Override
  protected void createComponent(String firstColumnName) {
    super.createComponent(firstColumnName);
    outlineView.setDefaultActionAllowed(true);
    outlineView.getOutline().setAutoscrolls(true);
    outlineView.setNodePopupFactory(new ListNodePopupFactory(this));
    outlineView.getOutline().addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() >= 2 && !isModified()) {
          final Node[] selectedNodes = getExplorerManager().getSelectedNodes();
          if (ArrayUtils.isNotEmpty(selectedNodes)) {
            EMH emh = selectedNodes[0].getLookup().lookup(EMH.class);
            if (emh != null) {
              ModellingOpenEMHNodeAction.open(emh, true);
            }
          }
        }
      }
    });
    outlineView.setAutoscrolls(true);
    outlineView.getOutline().setAutoscrolls(true);


  }

  /**
   * Ne pas utiliser directement. Utiliser uniquement pour les tests.
   *
   * @param scenarioLoaded le scenario chargé.
   */
  protected void setScenario(EMHScenario scenarioLoaded) {
    assert sousModeleUid != null;
    explorerHelper.cleanListener();
    EMH emh = updateSousModeleNameInUI(scenarioLoaded);
    List<? extends Node> createNodes = createNodes(emh);
    AbstractNode root = new AbstractNode(NodeChildrenHelper.createChildren(createNodes), Lookups.singleton(emh));
    getExplorerManager().setRootContext(root);
    explorerHelper.addListener();
  }

  /**
   * @return le lookup contenu dans le root node.
   */
  protected EMH getEMHInLookup() {
    return getExplorerManager().getRootContext().getLookup().lookup(EMH.class);

  }

  protected abstract List<? extends Node> createNodes(EMH in);

  @Override
  public void cancelModificationHandler() {
    scenarioReloaded();
    setModified(false);
  }

  @Override
  protected void scenarioLoaded() {
    EMHScenario scenarioLoaded = getModellingService().getScenarioLoaded();
    setScenario(scenarioLoaded);
  }
}
