/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.loi;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.fudaa.ebli.courbe.EGAxe;
import org.fudaa.ebli.courbe.EGGrapheModelListener;
import org.fudaa.ebli.courbe.EGObject;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiUiController;
import org.fudaa.fudaa.crue.loi.common.CourbeConfigUpdater;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.fudaa.fudaa.crue.modelling.services.ModellingConfigService;
import org.fudaa.fudaa.crue.options.config.CourbeConfig;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class LoiConfigController implements EGGrapheModelListener, PropertyChangeListener {

  private boolean configModified;
  private final AbstractModellingLoiTopComponent topComponent;
  private final AbstractLoiUiController loiUIController;
  final PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
  final ModellingConfigService modellingConfigService = Lookup.getDefault().lookup(ModellingConfigService.class);

  public LoiConfigController(AbstractModellingLoiTopComponent topComponent, AbstractLoiUiController loiUIController) {
    this.topComponent = topComponent;
    this.loiUIController = loiUIController;
  }

  public boolean isConfigModified() {
    return configModified;
  }

  public void install() {
    if (updating) {
      return;
    }
    removeListeners();
    loiUIController.getEGGrapheSimpleModel().addModelListener(this);
    modellingConfigService.addPropertyChangeListener(topComponent.getDefaultTopComponentId(), this);
  }

  public void uninstall() {
    if (updating) {
      return;
    }
    current = null;
    removeListeners();
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (modellingConfigService.getModellingLoiConfiguration() != null) {
      reloadCourbeConfig();
    }
  }

  @Override
  public void structureChanged() {
  }

  @Override
  public void courbeContentChanged(EGObject _c, boolean _mustRestore) {
  }

  @Override
  public void courbeAspectChanged(EGObject _c, boolean _visibil) {
    configIsModified();
  }

  @Override
  public void axeContentChanged(EGAxe _c) {
  }

  @Override
  public void axeAspectChanged(EGAxe _c) {
    configIsModified();
  }
  CourbeConfig current;

  private void configIsModified() {
    if (!updating && perspectiveServiceModelling.isInEditMode() && !topComponent.isUpdating()) {
      configModified = true;
      current = new CourbeConfigUpdater(loiUIController, false).create(current);
      topComponent.setConfigModified();
    }
  }

  void setConfigModified(boolean b) {
    this.configModified = b;
  }
  boolean updating;

  void reloadCourbeConfig() {
    if (updating) {
      return;
    }
    updating = true;
    current = null;
    applyConfig();
    updating = false;
    setConfigModified(false);
  }

  void saveCourbeConfig() {
    if (configModified && current != null) {
      updating = true;
      modellingConfigService.setModellingLoiConfigurationModified(topComponent.getDefaultTopComponentId(), current.createCopy());
      updating = false;
      setConfigModified(false);
    }
  }

  public void applyConfig() {
    if (current == null) {
      final CourbeConfig commonConfig = modellingConfigService.getModellingLoiConfigurationModified(topComponent.getDefaultTopComponentId());
      if (commonConfig == null) {
        current = new CourbeConfig();
      } else {
        current = commonConfig.createCopy();
      }
    }
    CourbeConfigUpdater updater = new CourbeConfigUpdater(loiUIController, false);
    updating = true;
    updater.apply(current);
    updating = false;
  }

  public void removeListeners() {
    loiUIController.getEGGrapheSimpleModel().removeModelListener(this);
    modellingConfigService.removePropertyChangeListener(topComponent.getDefaultTopComponentId(), this);
  }
}
