package org.fudaa.fudaa.crue.modelling.node;

import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.fudaa.crue.emh.node.NodeEMHDefault;
import org.fudaa.fudaa.crue.modelling.action.ChangeSousModeleParentNodeAction;
import org.fudaa.fudaa.crue.modelling.action.DeleteEMHCascadeNodeAction;
import org.fudaa.fudaa.crue.modelling.action.DeleteEMHUniqueNodeAction;
import org.fudaa.fudaa.crue.modelling.emh.ModellingOpenEMHNodeAction;
import org.openide.nodes.Children;
import org.openide.util.actions.SystemAction;

import javax.swing.*;

/**
 * @author deniger
 */
public class ModellingEMHNode extends NodeEMHDefault {
  public ModellingEMHNode(final Children children, final EMH emh) {
    super(children, emh);
  }

  public ModellingEMHNode(final Children children, final EMH emh, final String displayName) {
    super(children, emh, displayName);
  }

  @Override
  public Action getPreferredAction() {
    return SystemAction.get(ModellingOpenEMHNodeAction.Reopen.class);
  }

  @Override
  public Action[] getActions(final boolean context) {
    return new Action[]{
        SystemAction.get(ModellingOpenEMHNodeAction.Reopen.class),
        SystemAction.get(ModellingOpenEMHNodeAction.NewFrame.class),
        null,
        SystemAction.get(ChangeSousModeleParentNodeAction.class),
        null,
        SystemAction.get(DeleteEMHUniqueNodeAction.class),
        SystemAction.get(DeleteEMHCascadeNodeAction.class)};
  }
}
