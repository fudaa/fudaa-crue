package org.fudaa.fudaa.crue.modelling.calcul.importer;

import gnu.trove.TLongObjectHashMap;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.bean.DonCLimMLineContent;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.DclmFactory;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.views.DonClimMTableModel;
import org.openide.util.NbBundle;

import java.util.*;

/**
 * Import un fichier contenant les valeurs des CLimMs.
 *
 * @author Frederic Deniger
 */
public class CLimMsImporter {
  private final String[][] values;
  private final CrueConfigMetier ccm;
  private final DonLoiHYConteneur clonedloiConteneur;
  private final EMHScenario scenario;

  public CLimMsImporter(final CrueConfigMetier ccm, final EMHScenario scenario, final DonLoiHYConteneur clonedloiConteneur, final String[][] values) {
    this.values = values;
    this.ccm = ccm;
    this.scenario = scenario;
    this.clonedloiConteneur = clonedloiConteneur;
  }

  public DonClimMTableModel importData() {
    final CLimMsImportLinesBuilder fromArray = new CLimMsImportLinesBuilder();
    final CrueIOResu<CLimMsImportData> importedData = fromArray.extract(values);
    final CtuluLog log = importedData.getAnalyse();
    if (log.containsErrorOrSevereError()) {
      LogsDisplayer.displayError(log, NbBundle.getMessage(CLimMsImporter.class, "importCLimMs.bilan"));
      return null;
    }

    final CLimMsImportData importedValues = importedData.getMetier();
    final Map<String, EMH> emhByNom = scenario.getIdRegistry().getEmhByNom();
    final CtuluLog logCheckEMH = valideImportDatas(emhByNom, importedValues);
    if (logCheckEMH.containsErrorOrSevereError()) {
      LogsDisplayer.displayError(logCheckEMH, NbBundle.getMessage(CLimMsImporter.class, "importCLimMs.bilan"));
      return null;
    }
    final List<DonCLimMLineContent> dclimFinal = new ArrayList<>();
    final List<EMH> emhsFinal = new ArrayList<>();
    final Set<String> nameAlreadyAdded = new HashSet<>();
    for (final String emh : importedValues.getEmhs()) {
      if (!nameAlreadyAdded.contains(emh)) {
        emhsFinal.add(emhByNom.get(emh));
      }
      nameAlreadyAdded.add(emh);
    }
    final String versHaut = EnumSensOuv.OUV_VERS_HAUT.geti18n();
    final String versBas = EnumSensOuv.OUV_VERS_BAS.geti18n();
    final Map<String, DonCLimMLineContent> createLine = new HashMap<>();
    for (final CLimMsImportLine imported : importedValues.getCalculs()) {
      //pas de lignes: on ignore
      if (imported.getValues().isEmpty()) {
        continue;
      }
      final String calculName = imported.getCalculId();
      DonCLimMLineContent currentLine = createLine.get(calculName);
      if (currentLine == null) {
        final boolean transitoire = imported.isTransitoire();
        final Calc newCalcul = transitoire ? new CalcTrans() : new CalcPseudoPerm();
        newCalcul.setNom(calculName);
        final TLongObjectHashMap<List<DonCLimM>> valueByEmhUid = new TLongObjectHashMap<>();
        currentLine = new DonCLimMLineContent(newCalcul, valueByEmhUid);
        createLine.put(calculName, currentLine);
        dclimFinal.add(currentLine);
      }
      final Calc newCalcul = currentLine.getCalc();
      final boolean transitoire = newCalcul.isTransitoire();
      final TLongObjectHashMap<List<DonCLimM>> valueByEmhUid = currentLine.getDclmsByEMHId();
      for (final CLimMsImportValue value : imported.getValues()) {
        final String nom = value.getEmh();
        final String typeClimM = value.getTypeClimM();
        final EMH emh = emhByNom.get(nom);
        final Map<String, DclmFactory.CalcBuilder> toMapOfNom = findBuilders(newCalcul, emh);
        final DclmFactory.CalcBuilder builder = toMapOfNom.get(typeClimM);
        DonCLimMCommonItem create = null;
        if (builder != null) {
          create = builder.create(ccm);
        } else if (versHaut.equals(typeClimM) || versBas.equals(typeClimM)) {
          final DclmFactory.CalcBuilder<? extends DonCLimMCommonItem> ouvBuilder = transitoire ? new DclmFactory.CalcTransBrancheOrificeManoeuvreCreator() : new DclmFactory.CalcPseudoPermBrancheOrificeManoeuvreCreator();
          if (toMapOfNom.containsKey(ouvBuilder.geti18n())) {
            create = ouvBuilder.create(ccm);
            final EnumSensOuv sens = versHaut.equals(typeClimM) ? EnumSensOuv.OUV_VERS_HAUT : EnumSensOuv.OUV_VERS_BAS;
            if (transitoire) {
              ((CalcTransBrancheOrificeManoeuvre) create).setSensOuv(sens);
            } else {
              ((CalcPseudoPermBrancheOrificeManoeuvre) create).setSensOuv(sens);
            }
          }
        }
        if (create == null) {
          log.addWarn(NbBundle.getMessage(CLimMsImporter.class, "importClimMs.typeNotValid", typeClimM, emh.
              getNom()));
        } else {
          create.setEmh(emh);
          create.setCalculParent(newCalcul);
          if (transitoire) {
            final CalcTransItem permItem = (CalcTransItem) create;
            final List<Loi> filterDLHY = LoiHelper.filterDLHY(clonedloiConteneur, permItem.getTypeLoi());
            final Loi loi = TransformerHelper.toMapOfId(filterDLHY).get(value.getValue().toUpperCase());
            if (loi != null) {
              permItem.setLoi(loi);
            } else {
              create = null;
              log.addWarn(NbBundle.getMessage(CLimMsImporter.class, "importClimMs.loiNotValid", value.getValue(), emh.getNom(), calculName, permItem.
                  getTypeLoi().getNom()));
            }
          } else {
            try {
              ((CalcPseudoPermItem) create).setValue(Double.parseDouble(value.getValue()));
            } catch (final NumberFormatException numberFormatException) {
              create = null;
              log.addWarn(NbBundle.getMessage(CLimMsImporter.class, "importClimMs.valueNotValid", value.getValue(), emh.getNom(), calculName));
            }
          }
        }
        if (create != null) {
          List<DonCLimM> currentList = valueByEmhUid.get(emh.getUiId());
          if (currentList == null) {
            currentList = new ArrayList<>();
            valueByEmhUid.put(emh.getUiId(), currentList);
          }
          currentList.add(create);
        }
      }
    }
    if (log.isNotEmpty()) {
      LogsDisplayer.displayError(log, NbBundle.getMessage(CLimMsImporter.class, "importCLimMs.bilan"));
    }
    return new DonClimMTableModel(dclimFinal, emhsFinal, clonedloiConteneur);
  }

  private CtuluLog valideImportDatas(final Map<String, EMH> emhByNom, final CLimMsImportData values) throws MissingResourceException {
    final CtuluLog logCheckEMH = new CtuluLog();
    for (final String emh : values.getEmhs()) {
      if (!emhByNom.containsKey(emh)) {
        logCheckEMH.addSevereError(NbBundle.getMessage(CLimMsImporter.class, "climImport.unknownEMH"), emh);
      }
    }
    for (final CLimMsImportLine calcul : values.getCalculs()) {
      if (!calcul.isNameValide()) {
        logCheckEMH.addSevereError(NbBundle.getMessage(CLimMsImporter.class, "climImport.nameCalculUnknown"), calcul.getCalculId());
      } else {
        final boolean ispermanent = calcul.isPermanent();
        for (final CLimMsImportValue value : calcul.getValues()) {
          if (value.isPermanent() != ispermanent) {
            if (ispermanent) {
              logCheckEMH.addSevereError(NbBundle.getMessage(CLimMsImporter.class, "climImport.calculPermanentNoPermanentValue"), calcul.
                  getCalculId(), value.getEmh());
            } else {
              logCheckEMH.addSevereError(NbBundle.getMessage(CLimMsImporter.class, "climImport.calculTransitoireNoTransitoireValue"), calcul.
                  getCalculId(), value.getEmh());
            }
          }
        }
      }
    }
    return logCheckEMH;
  }

  private Map<String, DclmFactory.CalcBuilder> findBuilders(final Calc calcul, final EMH emh) {
    final List<DclmFactory.CalcBuilder> creators = new ArrayList<>(DclmFactory.getCreatorsSens(calcul));
    Collection<Class<? extends DonCLimM>> nonExportableDclm = DclmFactory.getNonExportableDclm();
    for (final Iterator<DclmFactory.CalcBuilder> it = creators.iterator(); it.hasNext(); ) {
      final DclmFactory.CalcBuilder calcBuilder = it.next();
      if (!calcBuilder.isAccepted(emh) || nonExportableDclm.contains(calcBuilder.getDclmClass())) {
        it.remove();
      }
    }
    final Map<String, DclmFactory.CalcBuilder> toMapOfNom = new HashMap<>();
    for (final DclmFactory.CalcBuilder creator : creators) {
      toMapOfNom.put(creator.getShortName(), creator);
    }
    return toMapOfNom;
  }
}
