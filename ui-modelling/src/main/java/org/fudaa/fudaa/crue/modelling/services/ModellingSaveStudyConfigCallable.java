package org.fudaa.fudaa.crue.modelling.services;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.io.conf.CrueEtudeConfigurationHelper;
import org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager;
import org.fudaa.fudaa.crue.common.config.ConfigSaverHelper;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.options.config.ModellingLoiConfigurationConverter;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.save.ConfigSaverHdydaulicAndVisibility;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 * Sauvegarde de la config de niveau étude.
 *
 * @author deniger
 */
public class ModellingSaveStudyConfigCallable implements Callable<CtuluLogGroup> {

  final ModellingScenarioVisuService modellingScenarioVisuService = Lookup.getDefault().lookup(ModellingScenarioVisuService.class);
  final ModellingConfigService modellingConfigService = Lookup.getDefault().lookup(ModellingConfigService.class);
  final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
          ModellingScenarioModificationService.class);

  @Override
  public CtuluLogGroup call() throws Exception {
    if (!modellingScenarioModificationService.isStudyConfigModified()) {
      return null;
    }
    ModellingScenarioService modellingScenarioService = modellingScenarioVisuService.getModellingScenarioService();
    if (!modellingScenarioService.getSelectedProjet().getInfos().isDirOfConfigDefined()) {
      modellingScenarioModificationService.clearStudyConfigModifiedState();
      return null;
    }
    ConfigSaverHdydaulicAndVisibility saver = new ConfigSaverHdydaulicAndVisibility();
    PlanimetryController planimetryController = modellingScenarioVisuService.getPlanimetryVisuPanel().getPlanimetryController();
    LinkedHashMap<String, Node.Property> planimetryProperties = saver.getProperties(planimetryController.getVisuConfiguration(),
            planimetryController.getLayerVisibility());
    LinkedHashMap<String, String> planimetryValues = ConfigSaverHelper.transform(planimetryProperties);

    File destDir = modellingScenarioService.getSelectedProjet().getInfos().getDirOfConfig();
    CrueEtudeConfigurationHelper.InputData input = new CrueEtudeConfigurationHelper.InputData();
    input.planimetryOptions = planimetryValues;
    input.globalOptions = modellingConfigService.getGlobalOptions();
    input.loiOptions = modellingConfigService.getModellingLoiConfiguration().getConfigByTopComponentId();
    CrueEtudeConfigurationHelper.write(new File(destDir, GlobalOptionsManager.ETUDE_FILE), input, new ModellingLoiConfigurationConverter());
    CtuluLogGroup gr = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    gr.setDescription("save.configEtu.done");
    modellingScenarioModificationService.clearStudyConfigModifiedState();
    return gr;
  }
}
