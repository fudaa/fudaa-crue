/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.node;

import java.util.Collection;
import java.util.List;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frédéric Deniger
 */
public class CommonObjectNode<T> extends AbstractModellingNodeFirable {

  public CommonObjectNode(T object, CrueConfigMetier ccm, PerspectiveServiceModelling perspectiveServiceModelling) {
    super(Children.LEAF, Lookups.fixed(object, ccm), perspectiveServiceModelling);
  }

  protected T getObject() {
    Collection instance = getLookup().lookupAll(Object.class);
    for (Object object : instance) {
      if (!object.getClass().equals(CrueConfigMetier.class)) {
        return (T) object;
      }
    }
    return null;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(getObject()));
  }

  protected void configurePropertySupport(CrueConfigMetier ccm, PropertySupportReflection propertySupportReflection) {
    configurePropertySupport(ccm, propertySupportReflection, this);
  }

  public static void configurePropertySupport(CrueConfigMetier ccm, PropertySupportReflection propertySupportReflection, AbstractNodeFirable node) {
    ItemVariable property = ccm.getProperty(propertySupportReflection.getName());
    PropertyNodeBuilder.configurePropertySupport(property, propertySupportReflection, node);
  }

  protected CrueConfigMetier getCcm() {
    return getLookup().lookup(CrueConfigMetier.class);
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
    T instance = getObject();
    Sheet.Set set = new Sheet.Set();
    set.setName(instance.getClass().getSimpleName());
    set.setDisplayName(BusinessMessages.geti18nForClass(instance.getClass()));
    set.setShortDescription(set.getDisplayName());
    sheet.put(set);
    CrueConfigMetier ccm = getCcm();
    PropertyNodeBuilder nodeBuilder = new PropertyNodeBuilder();
    List<PropertySupportReflection> createFromPropertyDesc = nodeBuilder.createFromPropertyDesc(DecimalFormatEpsilonEnum.COMPARISON, instance, this);
    for (PropertySupportReflection propertySupportReflection : createFromPropertyDesc) {
      set.put(propertySupportReflection);
      configurePropertySupport(ccm, propertySupportReflection);

    }
    return sheet;
  }
}
