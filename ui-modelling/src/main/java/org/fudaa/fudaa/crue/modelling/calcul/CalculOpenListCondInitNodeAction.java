/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import javax.swing.JMenuItem;
import org.fudaa.fudaa.crue.common.action.AbstractNodeAction;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.modelling.action.ModellingOpenListCondInitAction;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 * Permet d'ouvrir la liste des conditions initiales.
 *
 * @author Frederic Deniger
 */
public class CalculOpenListCondInitNodeAction extends AbstractNodeAction {

  public CalculOpenListCondInitNodeAction() {
    super(NbBundle.getMessage(CalculOpenListCondInitNodeAction.class, "OpenListCondInit.ActionName"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return true;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    new ModellingOpenListCondInitAction().actionPerformed(null);
  }

  @Override
  public JMenuItem getPopupPresenter() {
    return NodeHelper.useBoldFont(super.getPopupPresenter());
  }
}
