/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.validation.ValidateInitialConditions;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.OrdPrtCIniModeleBase;
import org.fudaa.dodico.crue.validation.ValidationHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.modelling//ModellingModeleOPTITopComponent//EN",
autostore = false)
@TopComponent.Description(preferredID = ModellingModeleOPTITopComponent.TOPCOMPONENT_ID, iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png", persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ModellingModeleOPTITopComponent.MODE, openAtStartup = false, position = 1)
public final class ModellingModeleOPTITopComponent extends AbstractModeleTopComponent {

  public static final String MODE = "modelling-opti";
  public static final String TOPCOMPONENT_ID = "ModellingModeleOPTITopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;

  public ModellingModeleOPTITopComponent() {
    initComponents();
    setName(NbBundle.getMessage(ModellingModeleOPTITopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingModeleOPTITopComponent.class, "HINT_" + TOPCOMPONENT_ID));
  }

  @Override
  public void setEditable(boolean b) {
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueOPTI";
  }

  @Override
  public void valideModificationHandler() {
    CtuluLog log = ValidationHelper.validateOrdPrtCIniModeleBase(editedObject, getCcm());
    if (log.isNotEmpty()) {
      LogsDisplayer.displayError(log, getName());
    }
    if (!log.containsErrorOrSevereError()) {
      EMHModeleBase modele = getModele();
      OrdPrtCIniModeleBase old = modele.getOrdPrtCIniModeleBase();
      modele.removeInfosEMH(old);
      modele.addInfosEMH(editedObject);


      setModified(false);
      ValidateInitialConditions optiChanged = new ValidateInitialConditions();
      CtuluLog propagateChange = optiChanged.propagateChange(getModele(), getCcm());
      //il faut envoyer l'event après la propagation des modifications.
      modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.OPTI));//a pour effet de recharger le scenario
      if (propagateChange.isNotEmpty()) {
        LogsDisplayer.displayError(propagateChange, getDisplayName());
      }
    }

  }

  private class ChangeListener implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      setModified(true);
    }
  }
  ChangeListener changeListener;
  OrdPrtCIniModeleBase editedObject;

  @Override
  protected JComponent buildCenterPanel(EMHScenario scenario, JComponent oldCenter) {
    EMHModeleBase modele = getModele(scenario);
    JTabbedPane tabbedPane = new JTabbedPane();
    changeListener = new ChangeListener();

    editedObject = modele.getOrdPrtCIniModeleBase().deepClone();

    OrdPrtCIniModeleBaseNode optiNode = new OrdPrtCIniModeleBaseNode(editedObject, getCcm(), perspectiveServiceModelling);
    PropertySheet methodeInterpolation = new PropertySheet();
    methodeInterpolation.setNodes(new Node[]{optiNode});
    tabbedPane.add(NbBundle.getMessage(ModellingModeleOPTITopComponent.class, "OPTIMethodeInterpolation.DisplayName"), methodeInterpolation);

    RegleView view = new RegleView();
    CrueConfigMetier ccm = getCcm();
    view.init(editedObject.getRegles(), ccm, changeListener);
    tabbedPane.add(NbBundle.getMessage(ModellingModeleOPTITopComponent.class, "Regles.DisplayName"), view);

    SortiesNode node = new SortiesNode(editedObject.getSorties(), ccm, perspectiveServiceModelling);
    PropertySheet sheet = new PropertySheet();
    sheet.setNodes(new Node[]{node});
    tabbedPane.add(NbBundle.getMessage(ModellingModeleOPTITopComponent.class, "SortiesNode.DisplayName"), sheet);
    restoreTabbedPaneSelection(oldCenter, tabbedPane);


    node.addPropertyChangeListener(changeListener);
    optiNode.addPropertyChangeListener(changeListener);
    return tabbedPane;

  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  @Override
  public void componentClosedTemporarily() {
  }

  void writeProperties(java.util.Properties p) {
  }

  void readProperties(java.util.Properties p) {
  }
}
