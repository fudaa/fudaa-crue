package org.fudaa.fudaa.crue.modelling.action;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.modelling.ModellingTopComponentEMHEditable;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * @author deniger
 */
public class ChangeSousModeleParentNodeAction extends AbstractEditNodeAction {
  final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);

  public ChangeSousModeleParentNodeAction() {
    super(NbBundle.getMessage(ChangeSousModeleParentNodeAction.class, "ChangeSousModeleParentAction.Name"));
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    if (activatedNodes.length == 0 || modellingScenarioService.getScenarioLoaded().getSousModeles().size() <= 1) {
      return false;
    }
    final EMH emh = activatedNodes[0].getLookup().lookup(EMH.class);
    return emh != null && !emh.getCatType().isContainer();
  }

  @Override
  protected void performAction(final Node[] activatedNodes) {
    final TopComponent activated = WindowManager.getDefault().getRegistry().getActivated();
    if (activated instanceof ModellingTopComponentEMHEditable) {
      final List<EMH> emhs = new ArrayList<>();
      for (final Node node : activatedNodes) {
        final EMH emh = node.getLookup().lookup(EMH.class);
        if (!emh.getCatType().isContainer()) {
          emhs.add(emh);
        }
      }
      if (!emhs.isEmpty()) {

        performActionOnSelectedEMHs((ModellingTopComponentEMHEditable) activated, emhs);
      }
    }
  }

  private void performActionOnSelectedEMHs(ModellingTopComponentEMHEditable activated, List<EMH> emhs) {

    final List<EMHSousModele> sousModeles = modellingScenarioService.getScenarioLoaded().getSousModeles();
    final JComboBox<EMHSousModele> cb = new JComboBox<>(sousModeles.toArray(new EMHSousModele[0]));
    cb.setRenderer(new ObjetNommeCellRenderer());
    cb.setSelectedIndex(0);
    final JPanel pn = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 2));
    pn.add(new JLabel(NbBundle.getMessage(ChangeSousModeleParentNodeAction.class, "ChangeSousModeleParent.Label")));
    pn.add(cb);
    if (DialogHelper.showQuestion(getName(), pn, getInformationsForChangeSousModeleDialog(emhs))) {
      final EMHSousModele ssModele = (EMHSousModele) cb.getSelectedItem();
      if (ssModele != null) {
        activated.changeSousModeleParent(emhs, ssModele.getUiId());
      }
    }
  }

  private String getInformationsForChangeSousModeleDialog(List<EMH> emhs) {
    final Set<String> sousModeleParent = emhs.stream().map(emh -> TransformerHelper.toNom(EMHHelper.getParents(emh))).flatMap(List::stream).collect(Collectors.toCollection(TreeSet::new));
    String msg;
    if (sousModeleParent.size() == 1) {
      msg = "ChangeSousModeleParent.Info.OneParent";
    } else {
      msg = "ChangeSousModeleParent.Info.MultiParent";
    }
    return NbBundle.getMessage(ChangeSousModeleParentNodeAction.class, msg, StringUtils.join(sousModeleParent, ", "));
  }
}
