package org.fudaa.fudaa.crue.modelling.list;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.edition.bean.ListBrancheContent;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.util.Lookup;

/**
 *
 * @author deniger
 */
public class ListBrancheContentFactory {

  final PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
  ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);

  public List<ListBrancheContentNode> createNodes(List<CatEMHBranche> branches) {
    List<ListBrancheContentNode> res = new ArrayList<>();
    int ordre = 0;

    for (CatEMHBranche branche : branches) {
      ListBrancheContent brancheContent = new ListBrancheContent();
      brancheContent.setNom(branche.getNom());
      brancheContent.setType(branche.getBrancheType());
      brancheContent.setActive(branche.getUserActive());
      brancheContent.setCommentaire(branche.getCommentaire());
      double length = branche.getLength();
      brancheContent.setLongueur(length);
      brancheContent.setNoeudAmont(branche.getNoeudAmontNom());
      brancheContent.setNoeudAval(branche.getNoeudAvalNom());
      ListBrancheContentNode node = new ListBrancheContentNode(branche, brancheContent, perspectiveServiceModelling);
      node.setDisplayName(Integer.toString(++ordre));
      res.add(node);
    }
    return res;
  }
}
