package org.fudaa.fudaa.crue.modelling.calcul.editor;

import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.swing.DialogPanelDescriptor;
import org.fudaa.fudaa.crue.modelling.calcul.CalculDclmNode;
import org.fudaa.fudaa.crue.views.DclmManoeuvreRegulEditor;
import org.openide.DialogDescriptor;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

import javax.swing.*;
import java.awt.*;
import java.beans.FeatureDescriptor;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorSupport;

public class DonCLimMWithManoeuvreRegulDataProperty extends PropertySupportReflection<String> {
  public DonCLimMWithManoeuvreRegulDataProperty(CalculDclmNode node, DonCLimMWithManoeuvreRegulData instance) throws NoSuchMethodException {
    super(node, instance, ManoeuvreRegulData.class, "data");
  }


  public DonLoiHYConteneur getLoiConteneur() {
    return ((CalculDclmNode) super.node).getLoiConteneur();
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    return new RegulEditorSupport(canWrite(), (DonCLimMWithManoeuvreRegulData) instance, getLoiConteneur());
  }


  protected static class RegulEditorSupport extends PropertyEditorSupport implements ExPropertyEditor {

    private PropertyEnv env;

    private final DonCLimMWithManoeuvreRegulData regul;
    private final boolean canWrite;

    private DonLoiHYConteneur loiConteneur;

    public RegulEditorSupport(final boolean canWrite, final DonCLimMWithManoeuvreRegulData regul, DonLoiHYConteneur loiConteneur) {
      this.regul = regul;
      this.canWrite = canWrite;
      this.loiConteneur = loiConteneur;
    }

    @Override
    public String getAsText() {
      Object value = getValue();
      return value == null ? "" : value.toString();
    }

    @Override
    public void setAsText(final String text) throws IllegalArgumentException {
    }

    public Component getCustomEditor() {
      DclmManoeuvreRegulEditor editor = new DclmManoeuvreRegulEditor(regul, loiConteneur);
      DialogPanelDescriptor p = editor.create();
      editor.setEditable(canWrite);
      if (!canWrite) {
        p.getDialogDescriptor().setOptionType(JOptionPane.CLOSED_OPTION);
        p.getDialogDescriptor().setOptions(new Object[]{DialogDescriptor.CLOSED_OPTION});
      } else {
        env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
        env.addPropertyChangeListener(evt -> {
          setValue(new ManoeuvreRegulData((LoiFF) editor.getSelectedLoi(), editor.getEditedParam()));

        });
      }
      return DialogHelper.createDialog(p.getDialogDescriptor(), regul.getEmh().getNom(), env, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    }

    @Override
    public boolean supportsCustomEditor() {
      return true;
    }

    @Override
    public void attachEnv(PropertyEnv env) {
      FeatureDescriptor featureDescriptor = env.getFeatureDescriptor();
      PropertyCrueUtils.configureNoEditAsText(featureDescriptor);
      PropertyCrueUtils.configureCustomEditor(featureDescriptor);
      this.env = env;
    }
  }


}
