package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractHelpAction;
import org.openide.awt.*;

@ActionID(category = "View",
        id = "org.fudaa.fudaa.crue.modelling.ModellingHelpAction")
//iconBase ne semble pas fonctionner !
@ActionRegistration(displayName = "#CTL_ModellingHelpAction", iconBase = AbstractHelpAction.HELP_ICON)
@ActionReference(path = "Actions/Modelling", position = 10000, separatorBefore = 9999)
public final class ModellingHelpAction extends AbstractHelpAction {

  public ModellingHelpAction() {
    super(PerspectiveEnum.MODELLING);
  }
}
