/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.emh;

import javax.swing.JComponent;
import org.fudaa.dodico.crue.edition.bean.BrancheEditionContent;
import org.openide.explorer.ExplorerManager;

/**
 *
 * @author fred
 */
public interface ModellingEMHBrancheSpecificEditor extends ExplorerManager.Provider {

  JComponent getComponent();

  void fillWithData(BrancheEditionContent content);

  void setEditable(boolean editable);
}
