package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.fudaa.crue.modelling.list.ModellingListFrottementTopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;

/**
 *
 * @author deniger
 */
public abstract class ModellingOpenListFrottementsOnBrancheNodeAction extends AbstractModellingOpenTopNodeAction<ModellingListFrottementTopComponent> {

  public ModellingOpenListFrottementsOnBrancheNodeAction(String name) {
    super(name, ModellingListFrottementTopComponent.MODE, ModellingListFrottementTopComponent.TOPCOMPONENT_ID);
  }

  @Override
  protected TopComponent activateNewTopComponent(ModellingListFrottementTopComponent newTopComponent, Node selectedNode) {
    Long uid = getUid(selectedNode);
    if (uid == null) {
      return null;
    }
    newTopComponent.setBrancheUid(uid);
    return newTopComponent;
  }

  public static void openBranche(CatEMHBranche branche, boolean reopen) {
    if (branche != null) {
      open(branche.getUiId(), reopen);
    }
  }

  private static void open(Long uid, boolean reopen) {
    if (uid == null) {
      return;
    }
    ModellingOpenListFrottementsOnBrancheNodeAction action = null;
    if (reopen) {
      action = SystemAction.get(ModellingOpenListFrottementsOnBrancheNodeAction.Reopen.class);
    } else {
      action = SystemAction.get(ModellingOpenListFrottementsOnBrancheNodeAction.NewFrame.class);
    }
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.singleton(uid));
    action.performAction(new Node[]{node});
  }

  @Override
  protected void activeWithValue(ModellingListFrottementTopComponent opened, Node selectedNode) {
    Long uid = getUid(selectedNode);
    if (uid != null) {
      opened.reloadWithBrancheUid(uid);
    }
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return activatedNodes.length == 1 && (activatedNodes[0].getLookup().lookup(CatEMHBranche.class) != null
            || activatedNodes[0].getLookup().lookup(Long.class) != null);
  }

  public static class Reopen extends ModellingOpenListFrottementsOnBrancheNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenListFrottementsOnBrancheNodeAction.class, "ModellingListFrottementsNodeAction.Name"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenListFrottementsOnBrancheNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenListFrottementsOnBrancheNodeAction.class, "ModellingListFrottementsNodeAction.NewFrame.Name"));
      setReopen(false);
    }
  }
}
