package org.fudaa.fudaa.crue.modelling.list;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;

/**
 *
 * @author Frederic Deniger
 */
public class ModellingListFrottementLineCellRenderer extends AbstractCellEditor implements TableCellRenderer, TableCellEditor, ActionListener {

  final ObjetNommeCellRenderer objetNommeCellRenderer = new ObjetNommeCellRenderer();
  final JPanel res = new JPanel();
  List<JComboBox> currentBoxes = new ArrayList<>();
  private final ModellingListFrottementTopComponent topComponent;

  public ModellingListFrottementLineCellRenderer(ModellingListFrottementTopComponent topComponent) {
    this.topComponent = topComponent;
  }

  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    setValue(value, topComponent.isEditable());
    return res;
  }

  @Override
  public Object getCellEditorValue() {
    return null;
  }

  @Override
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
    setValue(value, true);
    return res;
  }

  public void setValue(Object value, boolean editable) {
    res.removeAll();
    ModellingListFrottementLine.LitNommeCell cell = (ModellingListFrottementLine.LitNommeCell) value;
    final int size = cell.getLitNumerotes().size();
    res.setLayout(new GridLayout(1, size));
    List<JComboBox> newCurrentBoxes = new ArrayList<>();
    for (int i = 0; i < size; i++) {
      final ModellingListFrottementLine.LitNumeroteCell litNumerote = cell.getLitNumerotes().get(i);
      JComboBox cb;
      if (i < currentBoxes.size()) {
        cb = currentBoxes.get(i);
      } else {
        cb = new JComboBox();
        cb.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            JComboBox targetCb = (JComboBox) e.getSource();
            // ouverture de l'éditeur de frottement si doucle clic et cellule éditable
            if (e.getClickCount() >= 2 && targetCb.isEnabled()) {
              topComponent.openDonFrt((DonFrt) targetCb.getSelectedItem());
            }
          }
        });
        cb.setRenderer(objetNommeCellRenderer);
        cb.addActionListener(this);
      }
      cb.putClientProperty("LIT", null);

      cb.setModel(new DefaultComboBoxModel(litNumerote.getAccepted()));
      cb.setSelectedItem(litNumerote.getCurrentFrt());
      if (!litNumerote.getLitNumerote().isLongueurNulle()) {
        cb.setEnabled(editable);
      } else {
        // si lit de longueur nulle, utilisation du frottement FkSto_K0 et non éditable
        cb.setEnabled(false);
      }
      cb.putClientProperty("LIT", litNumerote);

      newCurrentBoxes.add(cb);
      res.add(cb);
    }

    currentBoxes = newCurrentBoxes;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    JComboBox jc = (JComboBox) e.getSource();
    final ModellingListFrottementLine.LitNumeroteCell clientProperty = (ModellingListFrottementLine.LitNumeroteCell) jc.getClientProperty("LIT");
    if (clientProperty == null) {
      return;
    }
    final DonFrt newDonFrt = (DonFrt) jc.getSelectedItem();
    if (newDonFrt == clientProperty.getCurrentFrt()) {
      return;
    }
    topComponent.setModified(true);
    clientProperty.setCurrentFrt(newDonFrt);
  }
}
