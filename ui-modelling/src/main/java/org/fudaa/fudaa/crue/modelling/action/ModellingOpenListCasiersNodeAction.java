package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.list.ModellingListCasierTopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public abstract class ModellingOpenListCasiersNodeAction extends AbstractModellingOpenListTopNodeAction {

  public ModellingOpenListCasiersNodeAction(String name) {

    super(name, ModellingListCasierTopComponent.MODE, ModellingListCasierTopComponent.TOPCOMPONENT_ID);
  }

  public static void open(EMHSousModele sousModele) {
    if (sousModele == null) {
      return;
    }
    ModellingOpenListCasiersNodeAction action = SystemAction.get(ModellingOpenListCasiersNodeAction.Reopen.class);
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(sousModele));
    action.performAction(new Node[]{node});
  }

  public static class Reopen extends ModellingOpenListCasiersNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenListCasiersNodeAction.class, "ModellingListCasiersNodeAction.Name"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenListCasiersNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenListCasiersNodeAction.class, "ModellingListCasiersNodeAction.NewFrame.Name"));
      setReopen(false);
    }
  }
}
