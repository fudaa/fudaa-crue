/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import java.util.List;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.config.ccm.SeveriteManager;
import org.fudaa.dodico.crue.metier.emh.Avancement;
import org.fudaa.dodico.crue.metier.emh.Resultat;
import org.fudaa.dodico.crue.metier.emh.Sorties;
import org.fudaa.dodico.crue.metier.emh.Trace;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.modelling.node.AbstractModellingNodeFirable;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frédéric Deniger
 */
public class SortiesNode extends AbstractModellingNodeFirable {

  public SortiesNode(Sorties sortie, CrueConfigMetier ccm, PerspectiveServiceModelling perspectiveServiceModelling) {
    super(Children.LEAF, Lookups.fixed(sortie, ccm), perspectiveServiceModelling);
    setDisplayName(NbBundle.getMessage(SortiesNode.class, "SortiesNode.DisplayName"));
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(getLookup().lookup(Sorties.class)));
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
//    Sheet.Set set = Sheet.createPropertiesSet();
    Sorties sortie = getLookup().lookup(Sorties.class);
    CrueConfigMetier ccm = getLookup().lookup(CrueConfigMetier.class);
    sheet.put(createAvancementSheet(sortie, ccm));
    sheet.put(createResultatSheet(sortie, ccm));
    sheet.put(createTraceSheet(sortie, ccm));
    return sheet;
  }

  protected Set createAvancementSheet(Sorties sortie, CrueConfigMetier ccm) {
    Sheet.Set setAvancement = new Sheet.Set();
    Avancement avancement = sortie.getAvancement();
    setAvancement.setName("Avancement");
    setAvancement.setDisplayName(NbBundle.getMessage(SortiesNode.class, "Avancement.DisplayName"));
    PropertyNodeBuilder nodeBuilder = new PropertyNodeBuilder();
    List<PropertySupportReflection> createFromPropertyDesc = nodeBuilder.createFromPropertyDesc(DecimalFormatEpsilonEnum.COMPARISON, avancement, this);
    for (PropertySupportReflection propertySupportReflection : createFromPropertyDesc) {
      setAvancement.put(propertySupportReflection);
    }
    return setAvancement;
  }

  protected Set createResultatSheet(Sorties sortie, CrueConfigMetier ccm) {
    Sheet.Set setResultat = new Sheet.Set();
    Resultat resultat = sortie.getResultat();
    setResultat.setName("Resultat");
    setResultat.setDisplayName(NbBundle.getMessage(SortiesNode.class, "Resultat.DisplayName"));
    PropertyNodeBuilder nodeBuilder = new PropertyNodeBuilder();
    List<PropertySupportReflection> createFromPropertyDesc = nodeBuilder.createFromPropertyDesc(DecimalFormatEpsilonEnum.COMPARISON, resultat, this);
    for (PropertySupportReflection propertySupportReflection : createFromPropertyDesc) {
      setResultat.put(propertySupportReflection);
    }
    return setResultat;
  }

  protected Set createTraceSheet(Sorties sortie, CrueConfigMetier ccm) {
    Sheet.Set setTrace = new Sheet.Set();
    Trace resultat = sortie.getTrace();
    setTrace.setName("Trace");
    setTrace.setDisplayName(NbBundle.getMessage(SortiesNode.class, "Trace.DisplayName"));
    PropertyNodeBuilder nodeBuilder = new PropertyNodeBuilder();
    List<PropertySupportReflection> createFromPropertyDesc = nodeBuilder.createFromPropertyDesc(DecimalFormatEpsilonEnum.COMPARISON, resultat, this);
    for (PropertySupportReflection propertySupportReflection : createFromPropertyDesc) {
      String name = propertySupportReflection.getName();
      if (Trace.PROP_VERBOSITE_ECRAN.equals(name) || Trace.PROP_VERBOSITE_FICHIER.equals(name)) {
        PropertyNature propertyEnum = ccm.getPropertyEnum(SeveriteManager.VERBOSITE_ID);
        List<ItemEnum> enumValues = propertyEnum.getEnumValues();
        String[] values = new String[enumValues.size()];
        for (int i = 0; i < values.length; i++) {
          values[i] = enumValues.get(i).getName();
        }
        propertySupportReflection.setTags(values);
      }
      setTrace.put(propertySupportReflection);
    }
    return setTrace;
  }
}
