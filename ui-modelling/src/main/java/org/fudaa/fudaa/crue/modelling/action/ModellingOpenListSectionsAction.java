/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import javax.swing.Action;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.perspective.ActiveModelisation;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View", id = "org.fudaa.fudaa.crue.modelling.ModellingOpenListSectionsAction")
@ActionRegistration(displayName = "#ModellingListSectionNodeAction.Name")
@ActionReferences({
  @ActionReference(path = ActiveModelisation.ACTIONS_MODELLING_VIEWS, position = 21)
})
public final class ModellingOpenListSectionsAction extends AbstractModellingSousModeleOpenAction {

  public ModellingOpenListSectionsAction() {
    putValue(Action.NAME, NbBundle.getMessage(ModellingOpenListSectionsAction.class, "ModellingListSectionNodeAction.Name"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingOpenListSectionsAction();
  }

  @Override
  protected void doAction(EMHSousModele sousModele) {
    ModellingOpenListSectionsNodeAction.open(sousModele);
  }
}
