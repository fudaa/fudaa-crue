package org.fudaa.fudaa.crue.modelling.list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizableTransformer;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.bean.ListBrancheContent;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflectionDouble;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class ListBrancheContentAddNode extends AbstractListContentNode {

  final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);

  public ListBrancheContentAddNode(final ListBrancheContent content, final List<String> availableNoeuds,
                                   final PerspectiveServiceModelling AbstractListContentNode) {
    super(Children.LEAF, Lookups.fixed(content, availableNoeuds), AbstractListContentNode);
  }
  
  

  @Override
  public boolean canDestroy() {
    return super.isEditMode();
  }

  @Override
  protected Sheet createSheet() {
    final Sheet sheet = Sheet.createDefault();
    final Sheet.Set set = Sheet.createPropertiesSet();
    final ListBrancheContent brancheContent = getLookup().lookup(ListBrancheContent.class);
    final List<String> nodes = new ArrayList<>(getLookup().lookup(List.class));
    nodes.add(0, StringUtils.EMPTY);
    final String[] nodesName = nodes.toArray(new String[0]);
    try {
      final Node.Property nom = PropertySupportReflection.createString(this, brancheContent, ListCommonProperties.PROP_NOM,
              ListCommonProperties.PROP_NOM,
              ListBrancheContentNode.getBrancheNameDisplay());
      PropertyCrueUtils.configureNoCustomEditor(nom);
      set.put(nom);
      //type
      final PropertySupportReflection type = new PropertySupportReflection(this, brancheContent, EnumBrancheType.class,
              ListCommonProperties.PROP_TYPE);
      type.setName(ListCommonProperties.PROP_TYPE);
      type.setDisplayName(ListRelationSectionContentNode.getSectionTypeDisplay());
      final List<EnumBrancheType> types = EnumBrancheType.getAvailablesBrancheType();
      final List<String> values = ToStringInternationalizableTransformer.toi18n(types);
      Collections.sort(values);
      values.add(0, StringUtils.EMPTY);
      type.setTags(values.toArray(new String[0]));
      type.setTranslationForObjects(ToStringInternationalizableTransformer.toi18nMap(types));
      PropertyCrueUtils.configureNoCustomEditor(type);
      set.put(type);

      //noeud amont
      final PropertySupportReflection noeudAmont = PropertySupportReflection.createString(this, brancheContent,
              ListBrancheContent.PROP_NOEUD_AMONT, ListBrancheContent.PROP_NOEUD_AMONT,
              ListBrancheContentNode.getNoeudAmontDisplayName());
      noeudAmont.setTags(nodesName);
      set.put(noeudAmont);
      //noeud aval
      final PropertySupportReflection noeudAval = PropertySupportReflection.createString(this, brancheContent,
              ListBrancheContent.PROP_NOEUD_AVAL, ListBrancheContent.PROP_NOEUD_AVAL,
              ListBrancheContentNode.getNoeudAvalDisplayName());
      noeudAval.setTags(nodesName);
      PropertyCrueUtils.configureNoCustomEditor(noeudAval);
      set.put(noeudAval);

      final CrueConfigMetier propDefinition = modellingScenarioService.getSelectedProjet().getPropDefinition();
      final ItemVariable property = propDefinition.getProperty(CrueConfigMetierConstants.PROP_XP);
      //longueur
      final Node.Property longueur = PropertySupportReflectionDouble.createDouble(DecimalFormatEpsilonEnum.COMPARISON,this, brancheContent, ListBrancheContent.PROP_LONGUEUR,
              ListBrancheContent.PROP_LONGUEUR, ListBrancheContentNode.getLongueurDisplayName(), property);
      PropertyCrueUtils.configureNoCustomEditor(longueur);
      set.put(longueur);

      //commentaire
      final Node.Property commentaire = new PropertySupportReflection(this, brancheContent, String.class,
              ListCommonProperties.PROP_COMMENTAIRE);
      PropertyCrueUtils.setNullValueEmpty(commentaire);
      commentaire.setName(ListCommonProperties.PROP_COMMENTAIRE);
      commentaire.setDisplayName(AbstractListContentNode.getCommentDisplay());
      set.put(commentaire);
    } catch (final NoSuchMethodException ex) {
      Exceptions.printStackTrace(ex);
    }
    sheet.put(set);
    return sheet;
  }
}
