/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import javax.swing.Action;
import org.fudaa.fudaa.crue.comparison.ScenarioComparaisonLauncher;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
id = "org.fudaa.fudaa.crue.modelling.action.ModellingComparisonAction")
@ActionRegistration(displayName = "#CTL_ModellingComparison")
@ActionReferences({
  @ActionReference(path = "Actions/Modelling", position = 4)
})
public final class ModellingComparisonAction extends AbstractModellingAction {

  final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public ModellingComparisonAction() {
    super(false);
    putValue(Action.NAME, NbBundle.getMessage(AbstractModellingAction.class, "CTL_ModellingComparison"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingComparisonAction();
  }

  @Override
  public void doAction() {
    new ScenarioComparaisonLauncher(scenarioService.getManagerScenarioLoaded(), scenarioService.getScenarioLoaded(), projetService.getSelectedProject().getPropDefinition(),
            null, projetService.getSelectedProject(), true).compute();
  }
}
