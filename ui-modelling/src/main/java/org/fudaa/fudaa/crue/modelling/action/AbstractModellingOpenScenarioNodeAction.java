package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.modelling.AbstractModellingTopComponent;
import org.openide.nodes.Node;
import org.openide.windows.TopComponent;

/**
 *
 * @author deniger
 */
public abstract class AbstractModellingOpenScenarioNodeAction<T extends AbstractModellingTopComponent> extends AbstractModellingOpenTopNodeAction<T> {

  public AbstractModellingOpenScenarioNodeAction(String name, String mode, String topComponentId) {
    super(name, mode, topComponentId);
  }

  @Override
  protected final boolean isEnable(Node[] activatedNodes) {
    return activatedNodes.length == 1 && activatedNodes[0].getLookup().lookup(EMHScenario.class) != null;
  }

  @Override
  protected final void activeWithValue(T opened, Node selectedNode) {
  }

  @Override
  protected final TopComponent activateNewTopComponent(T newTopComponent, Node selectedNode) {
    return newTopComponent;
  }

}
