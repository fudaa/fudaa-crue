/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul.importer;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.openide.util.NbBundle;

/**
 * Permet simplement de lire les données. Ne fait pas de tests métiers sur les données. Test uniquement si doublons dans les données
 *
 * @author Frederic Deniger
 */
public class CLimMsImportLinesBuilder {

  public CLimMsImportLinesBuilder() {
  }

  public CrueIOResu<CLimMsImportData> extract(String[][] values) {
    CrueIOResu<CLimMsImportData> ioRes = new CrueIOResu<>();
    CLimMsImportData res = new CLimMsImportData();
    CtuluLog log = new CtuluLog();
    //on parse les colonne
    //calcul;EMH 1 (;
    ioRes.setAnalyse(log);
    ioRes.setMetier(res);
    if (values.length == 0) {
      return ioRes;
    }
    final int nbColumns = values[0].length;
    if (nbColumns % 2 == 0) {
      log.addSevereError(NbBundle.getMessage(CLimMsImportLinesBuilder.class, "climImport.wrongColumnsNumber"));
      return ioRes;
    }
    for (int i = 1; i < nbColumns; i += 2) {
      String emh1 = StringUtils.substringBeforeLast(values[0][i], "(").trim();
      String emh2 = StringUtils.substringBeforeLast(values[0][i + 1], "(").trim();
      if (!StringUtils.equals(emh1, emh2)) {
        log.addSevereError(NbBundle.getMessage(CLimMsImportLinesBuilder.class, "climImport.wrongColumnsOrganization"));
      } else {
        res.getEmhs().add(emh1);
      }
    }
    final int nbEmhs = res.getEmhs().size();
    for (int row = 1; row < values.length; row++) {
      String[] val = values[row];
      if (val.length > nbColumns) {
        log.addSevereError(NbBundle.getMessage(CLimMsImportLinesBuilder.class, "climImport.ligneWithWrongColumnsNumber"), Integer.
                toString(row + 1), Integer.toString(nbColumns));
        return ioRes;
      }
      String calcId = val[0];
      CLimMsImportLine line = new CLimMsImportLine(calcId);
      boolean existing = false;
      for (int i = 0; i < nbEmhs; i++) {
        String emh = res.getEmhs().get(i);
        int idxCol = 2 + 2 * i;
        if (val.length > idxCol) {
          CLimMsImportValue value = new CLimMsImportValue(emh, val[idxCol - 1], val[idxCol]);
          if (StringUtils.isNotBlank(value.getTypeClimM()) && StringUtils.isNotBlank(value.getValue())) {
            line.getValues().add(value);
            existing = true;
          }
        }
      }
      if (existing) {
        res.getCalculs().add(line);
      }
    }
    if (log.containsErrorOrSevereError()) {
      return ioRes;
    }

    return ioRes;
  }
}
