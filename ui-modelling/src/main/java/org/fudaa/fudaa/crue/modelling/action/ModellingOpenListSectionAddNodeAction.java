package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.list.ModellingListSectionAddTopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;

/**
 *
 * @author deniger
 */
public class ModellingOpenListSectionAddNodeAction extends AbstractModellingOpenListTopNodeAction<ModellingListSectionAddTopComponent> {

  public static String getAddSectionActionName() {
    return NbBundle.getMessage(ModellingOpenListSectionAddNodeAction.class, "ModellingListSectionAddNodeAction.Name");
  }

  public ModellingOpenListSectionAddNodeAction() {

    super(getAddSectionActionName(), ModellingListSectionAddTopComponent.MODE, ModellingListSectionAddTopComponent.TOPCOMPONENT_ID);
    setReopen(false);
  }

  @Override
  protected void activeWithValue(ModellingListSectionAddTopComponent opened, Node selectedNode) {
    super.activeWithValue(opened, selectedNode);
    AddSectionInBranchePanelContentData data = selectedNode.getLookup().lookup(AddSectionInBranchePanelContentData.class);
    if (data != null) {
      opened.addSectionsInBranche(data);
    }
  }

  @Override
  protected TopComponent activateNewTopComponent(ModellingListSectionAddTopComponent create, Node selectedNode) {
    super.activateNewTopComponent(create, selectedNode);
    AddSectionInBranchePanelContentData data = selectedNode.getLookup().lookup(AddSectionInBranchePanelContentData.class);
    if (data != null) {
      create.addSectionsInBranche(data);
    }
    return create;
  }

  public static void open(EMHSousModele sousModele) {
    open(sousModele, null);
  }

  public static void open(EMHSousModele sousModele, AddSectionInBranchePanelContentData data) {
    if (sousModele == null) {
      return;
    }
    ModellingOpenListSectionAddNodeAction action = SystemAction.get(ModellingOpenListSectionAddNodeAction.class);
    final Lookup fixed = data == null ? Lookups.fixed(sousModele) : Lookups.fixed(sousModele, data);
    AbstractNode node = new AbstractNode(Children.LEAF, fixed);
    action.performAction(new Node[]{node});
  }
}
