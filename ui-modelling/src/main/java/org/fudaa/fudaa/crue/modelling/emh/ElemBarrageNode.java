/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.emh;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.ElemBarrage;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertyStringReadOnly;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.modelling.node.CommonObjectNode;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.datatransfer.PasteType;

import java.awt.datatransfer.Transferable;

/**
 *
 * @author fred
 */
public class ElemBarrageNode extends CommonObjectNode<ElemBarrage> {

  public static final String PROP_ADU = "ADU";

  public ElemBarrageNode(ElemBarrage elemPdt, CrueConfigMetier ccm, PerspectiveServiceModelling perspectiveServiceModelling) {
    super(elemPdt, ccm, perspectiveServiceModelling);
    super.setNoImage();
    DefaultNodePasteType.setIndexUsedAsDisplayName(this);
  }

  public static String getCoefNoyDisplayName() {
      return BusinessMessages.getString("coefNoy.property");
  }
  public static String getCoefDenDisplayName() {
      return BusinessMessages.getString("coefDen.property");
  }

  public static String getLargeurDisplayName() {
      return BusinessMessages.getString("largeur.property");
  }

  public static String getZSeuilDisplayName() {
      return BusinessMessages.getString("zseuil.property");
  }

  @Override
  protected Sheet createSheet() {
    Sheet res = super.createSheet();
//    Set set = res.get(Sheet.PROPERTIES);
//    PropertyStringReadOnly readOnly = new PropertyStringReadOnly("1", PROP_ADU, getAduDisplayName(), null);
//    PropertyCrueUtils.configureNoCustomEditor(readOnly);
//    set.put(readOnly);

    return res;
  }


  @Override
  public boolean canDestroy() {
      return isEditMode();
  }

  @Override
  public HelpCtx getHelpCtx() {
      return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(getLookup().lookup(ElemBarrage.class)));
  }

  @Override
  public PasteType getDropType(Transferable t, int action, int index) {
      if (isEditMode()) {
          final Node dragged = DefaultNodePasteType.getMovedNodeInTransferable(t);
          return dragged == null ? null : new DefaultNodePasteType(dragged, this);
      }
      return null;
  }
}
