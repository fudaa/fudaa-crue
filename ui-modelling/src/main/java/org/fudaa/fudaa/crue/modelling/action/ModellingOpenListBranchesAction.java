/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import javax.swing.Action;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.perspective.ActiveModelisation;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View", id = "org.fudaa.fudaa.crue.modelling.ModellingOpenListBranchesAction")
@ActionRegistration(displayName = "#ModellingListBrancheNodeAction.Name")
@ActionReferences({
  @ActionReference(path = ActiveModelisation.ACTIONS_MODELLING_VIEWS, position = 18)
})
public final class ModellingOpenListBranchesAction extends AbstractModellingSousModeleOpenAction {

  public ModellingOpenListBranchesAction() {
    putValue(Action.NAME, NbBundle.getMessage(ModellingOpenListBranchesAction.class, "ModellingListBrancheNodeAction.Name"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingOpenListBranchesAction();
  }

  @Override
  protected void doAction(EMHSousModele sousModele) {
    ModellingOpenListBranchesNodeAction.open(sousModele);
  }
}
