/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import javax.swing.Action;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.fudaa.crue.modelling.perspective.ActiveModelisation;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View", id = "org.fudaa.fudaa.crue.modelling.ModellingOpenModeleOPTRAction")
@ActionRegistration(displayName = "#ModellingModeleOPTRNodeAction.Name")
@ActionReferences({
  @ActionReference(path = ActiveModelisation.ACTIONS_MODELLING_VIEWS, position = 11)
})
public final class ModellingOpenModeleOPTRAction extends AbstractModellingModeleOpenAction {

  public ModellingOpenModeleOPTRAction() {
    putValue(Action.NAME, NbBundle.getMessage(ModellingOpenModeleOPTRAction.class, "ModellingModeleOPTRNodeAction.Name"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingOpenModeleOPTRAction();
  }

  @Override
  protected void doAction(EMHModeleBase modele) {
    ModellingOpenModeleOPTRNodeAction.open(modele);
  }
}
