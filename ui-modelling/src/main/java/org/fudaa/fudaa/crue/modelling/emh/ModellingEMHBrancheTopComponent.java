package org.fudaa.fudaa.crue.modelling.emh;

import com.memoire.bu.BuGridLayout;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.edition.EditionChangeBranche;
import org.fudaa.dodico.crue.edition.bean.BrancheEditionContent;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.dodico.crue.validation.util.DelegateValidatorBranche;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.helper.ToStringInternationalizableCellRenderer;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.modelling.action.ModellingOpenListFrottementsOnBrancheNodeAction;
import org.fudaa.fudaa.crue.modelling.action.ModellingOpenListSectionsNodeAction;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ModellingConfigService;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioModificationService;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.windows.TopComponent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Top component which displays something.
 */
//@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.modelling//ModellingListBrancheTopComponent//EN",
//                     autostore = false)
@TopComponent.Description(preferredID = ModellingEMHBrancheTopComponent.TOPCOMPONENT_ID,
    iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png", persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ModellingEMHBrancheTopComponent.MODE, openAtStartup = false, position = 0)
public final class ModellingEMHBrancheTopComponent extends AbstractModellingEMHTopComponent {
  public static final String MODE = "modelling-emhBranche";
  public static final String TOPCOMPONENT_ID = "ModellingEMHBrancheTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  private JCheckBox cbActive;
  private JComboBox cbAmont;
  private JComboBox cbAval;
  private JButton btSectionAmont;
  private JButton btSectionAval;
  private JButton btListSection;
  private JButton btListFrt;
  private JComboBox cbType;
  final AbstractLookup lookup;
  final InstanceContent content;
  private final transient ModellingConfigService modellingConfigService = Lookup.getDefault().lookup(ModellingConfigService.class);

  public ModellingEMHBrancheTopComponent() {
    setName(NbBundle.getMessage(ModellingEMHBrancheTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingEMHBrancheTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    content = new InstanceContent();
    lookup = new AbstractLookup(content);
    associateLookup(lookup);
  }

  @Override
  public HelpCtx getHelpCtx() {
    HelpCtx res = super.getHelpCtx();
    final Object selectedItem = cbType.getSelectedItem();
    if (selectedItem != null) {
      res = new HelpCtx(SysdocUrlBuilder.addSignet(res.getHelpID(), selectedItem.toString()));
    }
    return res;
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueEMHBranche";
  }

  @Override
  protected JPanel buildNorthPanel() {
    cbActive = new JCheckBox(NbBundle.getMessage(ModellingEMHBrancheTopComponent.class, "CheckBoxActive.DisplayName"));
    final JPanel pn = super.createNorthPanel(cbActive);
    pn.add(new JLabel(NbBundle.getMessage(ModellingEMHBrancheTopComponent.class, "BrancheType.DisplayName")));
    cbType = new JComboBox(EnumBrancheType.getAvailablesBrancheType().toArray(new EnumBrancheType[0]));
    cbType.addActionListener(modifiedActionListener);
    cbType.addItemListener(e -> {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        udpdateSpecificCenter();
      }
    });
    cbType.setRenderer(new ToStringInternationalizableCellRenderer());
    pn.add(cbType);
    cbActive.addActionListener(modifiedActionListener);
    createCommentaireTxt();
    pn.add(new JLabel(NbBundle.getMessage(AbstractModellingEMHTopComponent.class, "Commentaire.DisplayName")));
    pn.add(jCommentaire);
    final JPanel pnNoeud = createPanelNoeuds();
    final JPanel pnSection = createPanelSections();
    final JPanel pnMain = new JPanel(new BorderLayout(5, 10));
    pnMain.add(pn, BorderLayout.NORTH);
    final JPanel pnNoeudSection = new JPanel(new BorderLayout());
    pnNoeudSection.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    pnNoeudSection.add(pnNoeud, BorderLayout.WEST);
    pnNoeudSection.add(pnSection, BorderLayout.EAST);
    pnMain.add(pnNoeudSection);
    return pnMain;
  }

  private void openSectionAmont() {
    final CatEMHBranche branche = getEMH();
    ModellingOpenEMHNodeAction.open(branche.getSectionAmont().getEmh(), true);
  }

  private void openSectionAval() {
    final CatEMHBranche branche = getEMH();
    ModellingOpenEMHNodeAction.open(branche.getSectionAval().getEmh(), true);
  }

  private void openListSection() {
    final CatEMHBranche branche = getEMH();
    // on passe en paramétre le nom de la branche, comme filtre pour la fenêtre
    ModellingOpenListSectionsNodeAction.openList(((CatEMHBranche) getEMH()).getParent(), true, branche.getNom());
  }

  private void openListFrt() {
    ModellingOpenListFrottementsOnBrancheNodeAction.openBranche(getEMH(), true);
  }

  private JPanel createPanelSections() {
    btSectionAmont = new JButton();
    btSectionAmont.addActionListener(e -> openSectionAmont());
    btSectionAval = new JButton();
    btSectionAval.addActionListener(e -> openSectionAval());
    final JPanel pnSection = new JPanel(new BuGridLayout(2, 5, 5));
    pnSection.add(new JLabel(NbBundle.getMessage(ModellingEMHBrancheTopComponent.class, "SectionAmont.DisplayName")));
    pnSection.add(btSectionAmont);
    pnSection.add(new JLabel(NbBundle.getMessage(ModellingEMHBrancheTopComponent.class, "SectionAval.DisplayName")));
    pnSection.add(btSectionAval);
    btListSection = new JButton(NbBundle.getMessage(ModellingEMHBrancheTopComponent.class, "OpenListSection.DisplayName"));
    btListSection.addActionListener(e -> openListSection());
    btListFrt = new JButton(NbBundle.getMessage(ModellingEMHBrancheTopComponent.class, "OpenListFrt.DisplayName"));
    btListFrt.addActionListener(e -> openListFrt());
    pnSection.add(new JLabel());
    pnSection.add(btListSection);
    pnSection.add(new JLabel());
    pnSection.add(btListFrt);
    btSectionAmont.setToolTipText(NbBundle.getMessage(ModellingEMHBrancheTopComponent.class, "SectionButton.Tooltip"));
    btSectionAval.setToolTipText(btSectionAmont.getToolTipText());
    btListSection.setToolTipText(NbBundle.getMessage(ModellingEMHBrancheTopComponent.class, "ListSectionButton.Tooltip"));

    return pnSection;
  }

  private JPanel createPanelNoeuds() {
    cbAmont = new JComboBox();
    cbAval = new JComboBox();
    final ObjetNommeCellRenderer renderer = new ObjetNommeCellRenderer();
    cbAmont.setRenderer(renderer);
    cbAval.setRenderer(renderer);
    final JPanel pnNoeud = new JPanel(new BuGridLayout(2, 5, 5));
    pnNoeud.add(new JLabel(NbBundle.getMessage(ModellingEMHBrancheTopComponent.class, "NoeudAmont.DisplayName")));
    pnNoeud.add(cbAmont);
    pnNoeud.add(new JLabel(NbBundle.getMessage(ModellingEMHBrancheTopComponent.class, "NoeudAval.DisplayName")));
    pnNoeud.add(cbAval);

    cbAmont.addActionListener(modifiedActionListener);
    cbAval.addActionListener(modifiedActionListener);
    return pnNoeud;
  }

  @Override
  public void valideModificationHandler() {
    final BrancheEditionContent modifications = new BrancheEditionContent();
    modifications.setType((EnumBrancheType) cbType.getSelectedItem());
    modifications.setCommentaire(super.jCommentaire.getText());
    modifications.setNom(jEMHName.getText());
    modifications.setActive(cbActive.isSelected());
    modifications.setNoeudAmont(cbAmont.getSelectedItem() == null ? null : ((CatEMHNoeud) cbAmont.getSelectedItem()).getNom());
    modifications.setNoeudAval(cbAval.getSelectedItem() == null ? null : ((CatEMHNoeud) cbAval.getSelectedItem()).getNom());
    final EditionChangeBranche edition = new EditionChangeBranche(modellingConfigService.getDefaultValues());
    currentCenter.second.fillWithData(modifications);
    final CatEMHBranche emh = getEMH();
    boolean renameSection = false;
    if (!modifications.getNom().equals(emh.getNom())) {
      final String optionRename = NbBundle.getMessage(ModellingEMHBrancheTopComponent.class, "BrancheChangeName.RenameSection");
      final String optionNot = NbBundle.getMessage(ModellingEMHBrancheTopComponent.class, "BrancheChangeName.NotRenameSection");
      final String dialogContent = org.openide.util.NbBundle.getMessage(ModellingEMHBrancheTopComponent.class, "BrancheChangeName.Content");
      renameSection = optionRename.equals(DialogHelper.showQuestion(
          getDisplayName(), dialogContent, new String[]{optionRename, optionNot}));
    }
    //CRUE-665: si les noeuds amont/aval changent, le sens de la branche doit être également modifié.
    final boolean isAmontAvalChanged = !StringUtils.equals(modifications.getNoeudAmont(), emh.getNoeudAmontNom()) || !StringUtils.equals(modifications.
        getNoeudAval(), emh.
        getNoeudAvalNom());
    final CtuluLog log = edition.changeBranche(emh, modifications, getCcm(), renameSection);
    if (!log.containsErrorOrSevereError()) {
      modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.EMH));
      if (isAmontAvalChanged) {
        //sens modifié: réenregistrer la branche dans le bon sens
        modellingScenarioModificationService.setGeoLocModified();
      }
      setModified(false);
    }
    if (log.isNotEmpty()) {
      LogsDisplayer.displayError(log, getName());
    }
  }

  private final transient ModellingEMHBrancheSpecificEditorFactory factory = new ModellingEMHBrancheSpecificEditorFactory();

  @Override
  protected void updateOtherComponents() {
    final CatEMHBranche branche = getEMH();
    cbActive.setSelected(branche.getUserActive());
    final CatEMHNoeud noeudAmont = branche.getNoeudAmont();
    final CatEMHNoeud noeudAval = branche.getNoeudAval();
    final List<CatEMHNoeud> noeudsList = branche.getParent().getNoeuds();
    final CatEMHNoeud[] nds = noeudsList.toArray(new CatEMHNoeud[0]);
    cbAmont.setModel(new DefaultComboBoxModel(nds));
    cbAmont.setSelectedItem(noeudAmont);
    cbAval.setModel(new DefaultComboBoxModel(nds));
    cbAval.setSelectedItem(noeudAval);
    cbType.setSelectedItem(branche.getBrancheType());
    btSectionAmont.setText(branche.getSectionAmont().getEmhNom());
    btSectionAval.setText(branche.getSectionAval().getEmhNom());
    if (EventQueue.isDispatchThread()) {
      udpdateSpecificCenter();
    } else {
      EventQueue.invokeLater(this::udpdateSpecificCenter);
    }
  }

  /**
   * le premier est le panel principal. le deuxieme est le panel spécifique au type de branche
   */
  private transient Pair<JPanel, ModellingEMHBrancheSpecificEditor> currentCenter;

  private void udpdateSpecificCenter() {
    final EnumBrancheType brancheType = (EnumBrancheType) cbType.getSelectedItem();
    if (brancheType == null) {
      return;
    }
    if (currentCenter != null) {
      remove(currentCenter.first);
      final ExplorerManager explorerManager = currentCenter.second.getExplorerManager();
      if (explorerManager != null) {
        explorerManager.removePropertyChangeListener(updater);
      }
    }
    content.set(Collections.emptyList(), null);
    final CatEMHBranche branche = getEMH();
    currentCenter = factory.create(branche, brancheType, this);
    add(currentCenter.first);
    currentCenter.second.setEditable(cbActive.isEnabled());
    final ExplorerManager explorerManager = currentCenter.second.getExplorerManager();
    if (explorerManager != null) {
      explorerManager.addPropertyChangeListener(updater);
    }
    repaint();
    revalidate();
    doLayout();
  }

  private final ExplorerManagerUpdater updater = new ExplorerManagerUpdater();

  private class ExplorerManagerUpdater implements PropertyChangeListener {
    @Override
    public void propertyChange(final PropertyChangeEvent evt) {
      final ExplorerManager explorerManager = currentCenter.second.getExplorerManager();
      if (explorerManager == null) {
        content.set(Collections.emptyList(), null);
      } else {
        final Node[] selectedNodes = explorerManager.getSelectedNodes();
        content.set(Arrays.asList(selectedNodes), null);
      }
    }
  }

  private final transient PropertyChangeListener geoLocListener = evt -> {
    final CatEMHBranche branche = getEMH();
    final List<EnumBrancheType> brancheTypeWithDistanceNotNull = DelegateValidatorBranche.getBrancheTypeWithDistanceNotNull();
    if (brancheTypeWithDistanceNotNull.contains(branche.getBrancheType())) {
      scenarioReloaded();//pour mettre à jour les x schematique
    }
  };

  @Override
  protected void setEditable(final boolean b) {
    super.setEditable(b);
    cbActive.setEnabled(b);
    cbAmont.setEnabled(b);
    cbAval.setEnabled(b);
    cbType.setEnabled(b);
    if (currentCenter != null) {
      currentCenter.second.setEditable(b);
    }
  }

  @Override
  protected void componentClosedDefinitlyHandler() {
    super.componentClosedDefinitlyHandler();
    modellingScenarioModificationService.removePropertyListener(ModellingScenarioModificationService.PROPERTY_GEOLOC_MODIFIED, geoLocListener);
  }

  @Override
  protected void componentOpenedHandler() {
    super.componentOpenedHandler();
    modellingScenarioModificationService.addPropertyListener(ModellingScenarioModificationService.PROPERTY_GEOLOC_MODIFIED, geoLocListener);
  }

  @Override
  protected void createComponents() {
    add(buildNorthPanel(), BorderLayout.NORTH);

    add(createCancelSaveButtons(), BorderLayout.SOUTH);
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
}
