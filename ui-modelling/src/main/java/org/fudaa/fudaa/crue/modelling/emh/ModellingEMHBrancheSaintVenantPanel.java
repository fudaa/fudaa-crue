package org.fudaa.fudaa.crue.modelling.emh;

import com.memoire.bu.BuGridLayout;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.bean.BrancheEditionContent;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.fudaa.crue.common.helper.DisableSortHelper;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.common.view.ItemVariableView;
import org.fudaa.fudaa.crue.modelling.list.ListRelationSectionContentFactoryNode;
import org.fudaa.fudaa.crue.modelling.node.CommonObjectNode;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Branche Type 2
 *
 * @author fred
 */
public class ModellingEMHBrancheSaintVenantPanel extends JPanel implements ModellingEMHBrancheSpecificEditor, Observer, ExplorerManager.Provider {
  private final transient ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(
      ModellingScenarioService.class);
  final transient PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
  final transient ModellingEMHBrancheTopComponent modellingEMHBrancheTopComponent;
  private final transient ItemVariableView.TextFiedDistance distance;
  final OutlineView view;
  private final Node avalNode;
  final ExplorerManager em = new ExplorerManager();
  private transient List<ItemVariableView> dcspProperties;
  private transient List<ItemVariableView> dptgProperties;
  private final transient  DisableSortHelper sortDisabler;

  ModellingEMHBrancheSaintVenantPanel(final CatEMHBranche branche, final ModellingEMHBrancheTopComponent parent) {
    super(new BorderLayout(10, 10));
    setPreferredSize(new Dimension(600, 800));
    this.modellingEMHBrancheTopComponent = parent;
    final CrueConfigMetier ccm = getCcm();
    distance = new ItemVariableView.TextFiedDistance(ccm, DecimalFormatEpsilonEnum.COMPARISON);
    DonCalcSansPrtBrancheSaintVenant dcsp = EMHHelper.selectFirstOfClass(branche.getInfosEMH(), DonCalcSansPrtBrancheSaintVenant.class);
    if (dcsp == null) {
      dcsp = new DonCalcSansPrtBrancheSaintVenant(ccm);
    }
    DonPrtGeoBrancheSaintVenant dptg = EMHHelper.selectFirstOfClass(branche.getInfosEMH(), DonPrtGeoBrancheSaintVenant.class);
    if (dptg == null) {
      dptg = new DonPrtGeoBrancheSaintVenant(ccm);
    }
    final JPanel pnTop = createTopPanel(dcsp, ccm, dptg, branche);
    add(pnTop, BorderLayout.NORTH);

    final RelationEMHSectionDansBranche sectionAval = branche.getSectionAval();
    final ItemVariable propertyXp = ccm.getProperty(CrueConfigMetierConstants.PROP_XP);
    final List<Node> nodes = new ArrayList<>();
    nodes.add(createLabelNode(NbBundle.getMessage(ModellingEMHBrancheSaintVenantPanel.class, "Amont.DisplayName")));
    final boolean isSaintVenant = branche.getBrancheType().equals(EnumBrancheType.EMHBrancheSaintVenant);

    final List<RelationEMHSectionDansBranche> listeSectionsSortedXP = branche.getListeSectionsSortedXP(ccm);
    final ListRelationSectionContentFactoryNode factory = new ListRelationSectionContentFactoryNode();
    //on ne s'occupe pas de la section aval:
    for (int i = 0; i < listeSectionsSortedXP.size() - 1; i++) {
      final RelationEMHSectionDansBranche relation = listeSectionsSortedXP.get(i);
      final AbstractNodeFirable sectionNode = factory.create(branche, relation, ccm);
      sectionNode.setDisplayName(propertyXp.format(relation.getXp(), DecimalFormatEpsilonEnum.COMPARISON));
      ModellingEMHBrancheStricklerPanel.removeWrite(sectionNode);
      nodes.add(sectionNode);
      final RelationEMHSectionDansBrancheSaintVenant relationSaintVenant = isSaintVenant ? ((RelationEMHSectionDansBrancheSaintVenant) relation).
          cloneValuesOnly() : new RelationEMHSectionDansBrancheSaintVenant(
          ccm);
      final CommonObjectNode<RelationEMHSectionDansBrancheSaintVenant> node = new CommonObjectNode<>(
          relationSaintVenant, ccm, perspectiveServiceModelling);
      node.addPropertyChangeListener(evt -> ModellingEMHBrancheSaintVenantPanel.this.modellingEMHBrancheTopComponent.setModified(true));
      nodes.add(node);
    }

    avalNode = factory.create(branche, sectionAval, ccm);
    avalNode.setDisplayName(propertyXp.format(sectionAval.getXp(), DecimalFormatEpsilonEnum.COMPARISON));
    ModellingEMHBrancheStricklerPanel.removeWrite((AbstractNodeFirable) avalNode);
    nodes.add(avalNode);
    nodes.add(createLabelNode(NbBundle.getMessage(ModellingEMHBrancheSaintVenantPanel.class, "Aval.DisplayName")));
    view = new OutlineView(NbBundle.getMessage(ModellingEMHBrancheSaintVenantPanel.class, "AbsHydraulique.ColumnName", propertyXp.getNature().
        getUnite()));
    view.setTreeSortable(false);
    sortDisabler = new DisableSortHelper(view);
    view.setPropertyColumns(
        ListCommonProperties.PROP_NOM, NbBundle.getMessage(ModellingEMHBrancheSaintVenantPanel.class, "Section.ColumnName"),
        RelationEMHSectionDansBrancheSaintVenant.PROP_COEFPOND, BusinessMessages.getString("coefPond.property"),
        RelationEMHSectionDansBrancheSaintVenant.PROP_COEFDIV, BusinessMessages.getString("coefDiv.property"),
        RelationEMHSectionDansBrancheSaintVenant.PROP_COEFCONV, BusinessMessages.getString("coefConv.property"));
    view.getOutline().setRootVisible(false);
    em.setRootContext(NodeHelper.createNode(nodes, "SECTIONS"));
    parent.getLookup();

    add(view);
  }

  private JPanel createTopPanel(final DonCalcSansPrtBrancheSaintVenant dcsp, final CrueConfigMetier ccm, final DonPrtGeoBrancheSaintVenant dptg,
                                final CatEMHBranche branche) throws MissingResourceException {
    final JPanel pnTop = new JPanel(new BuGridLayout(4, 10, 10));
    dcspProperties = ItemVariableView.create(DecimalFormatEpsilonEnum.COMPARISON, dcsp, ccm, DonCalcSansPrtBrancheSaintVenant.PROP_COEFRUIS,
        DonCalcSansPrtBrancheSaintVenant.PROP_COEFBETA, DonCalcSansPrtBrancheSaintVenant.PROP_COEFRUISQDM);
    for (final ItemVariableView propertyEditorPanel : dcspProperties) {
      propertyEditorPanel.addObserver(this);
      pnTop.add(new JLabel(propertyEditorPanel.getLabelName()));
      pnTop.add(propertyEditorPanel.getPanelComponent());
    }
    dptgProperties = ItemVariableView.create(DecimalFormatEpsilonEnum.COMPARISON, dptg, ccm, DonPrtGeoBrancheSaintVenant.PROP_COEFSINUO);
    for (final ItemVariableView propertyEditorPanel : dptgProperties) {
      propertyEditorPanel.addObserver(this);
      pnTop.add(new JLabel(propertyEditorPanel.getLabelName()));
      pnTop.add(propertyEditorPanel.getPanelComponent());
    }
    pnTop.add(new JLabel(NbBundle.getMessage(ModellingEMHBrancheSaintVenantPanel.class, "Distance.DisplayName")));
    pnTop.add(distance.getPanelComponent());
    distance.setValue(branche.getLength());
    distance.addObserver(this);
    return pnTop;
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return em;
  }

  private Node createLabelNode(final String name) {
    return new DelimiteurNode(name);
  }

  @Override
  public void update(final Observable o, final Object arg) {
    modellingEMHBrancheTopComponent.setModified(true);
    try {
      final Number value = distance.getValue();
      avalNode.setDisplayName(distance.getProperty().format(value, DecimalFormatEpsilonEnum.COMPARISON));
    } catch (final Exception exception) {
      Exceptions.printStackTrace(exception);
    }
  }

  private CrueConfigMetier getCcm() {
    return modellingScenarioService.getSelectedProjet().getPropDefinition();
  }

  @Override
  public void setEditable(final boolean editable) {
    for (final ItemVariableView object : dcspProperties) {
      object.setEditable(editable);
    }
    for (final ItemVariableView object : dptgProperties) {
      object.setEditable(editable);
    }
    distance.setEditable(editable);
    sortDisabler.setDisableSort(editable);
  }

  /**
   * @return n-1 relation: la derniere n'est pas prise en compte
   */
  private List<RelationEMHSectionDansBrancheSaintVenant> getRelations() {
    final Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    final List<RelationEMHSectionDansBrancheSaintVenant> res = new ArrayList<>();
    for (final Node node : nodes) {
      final RelationEMHSectionDansBrancheSaintVenant relation = node.getLookup().lookup(RelationEMHSectionDansBrancheSaintVenant.class);
      if (relation != null) {
        res.add(relation);
      }
    }
    return res;
  }

  @Override
  public void fillWithData(final BrancheEditionContent content) {
    final DonCalcSansPrtBrancheSaintVenant dcsp = new DonCalcSansPrtBrancheSaintVenant(getCcm());
    ItemVariableView.transferToBean(dcsp, dcspProperties);
    final DonPrtGeoBrancheSaintVenant dptg = new DonPrtGeoBrancheSaintVenant(getCcm());
    ItemVariableView.transferToBean(dptg, dptgProperties);
    content.setDistance(distance.getValue().doubleValue());
    content.setSaintVenantCoeff(getRelations());
    content.setDcsp(dcsp);
    content.setDptgSaintVenant(dptg);
  }

  @Override
  public JComponent getComponent() {
    return this;
  }
}
