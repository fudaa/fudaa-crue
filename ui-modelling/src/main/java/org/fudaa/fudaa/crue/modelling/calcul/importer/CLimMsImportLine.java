/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul.importer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;

/**
 *
 * @author Frederic Deniger
 */
public class CLimMsImportLine {

  private final String calculId;
  private final List<CLimMsImportValue> values = new ArrayList<>();

  public CLimMsImportLine(final String calculId) {
    this.calculId = calculId;
  }

  public String getCalculId() {
    return calculId;
  }

  public List<CLimMsImportValue> getValues() {
    return values;
  }

  public boolean isNameValide() {
    return calculId.startsWith(CruePrefix.P_CALCUL_PSEUDOPERMANENT) || calculId.startsWith(CruePrefix.P_CALCUL_TRANSITOIRE);
  }

  public boolean isTransitoire() {
    return calculId.startsWith(CruePrefix.P_CALCUL_TRANSITOIRE);
  }
  public boolean isPermanent() {
    return calculId.startsWith(CruePrefix.P_CALCUL_PSEUDOPERMANENT);
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 97 * hash + Objects.hashCode(this.calculId);
    return hash;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final CLimMsImportLine other = (CLimMsImportLine) obj;
    return Objects.equals(this.calculId, other.calculId);
  }

}
