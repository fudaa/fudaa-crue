package org.fudaa.fudaa.crue.modelling.listener;

import org.fudaa.fudaa.crue.modelling.AbstractModellingTopComponent;
import org.openide.nodes.*;

import java.beans.PropertyChangeEvent;

/**
 * ectoute les evts internes au node. Par defaut, ne modifie pas le topcomponent si noeud ajoute/enleve. Utilse modifiedInArborescenceChanged par
 * activer ce point.
 *
 * @author deniger
 */
public class NodeInternChangedListener implements NodeListener {
  private final AbstractModellingTopComponent target;
  private boolean modifiedIfArborescenceChanged = false;
  private boolean modifiedIfDisplayNameChanged = true;
  private boolean modifiedIfNameChanged = false;

  public NodeInternChangedListener(final AbstractModellingTopComponent target) {
    this.target = target;
  }

  public boolean isModifiedIfDisplayNameChanged() {
    return modifiedIfDisplayNameChanged;
  }

  public void setModifiedIfDisplayNameChanged(final boolean modifiedIfDisplayNameChanged) {
    this.modifiedIfDisplayNameChanged = modifiedIfDisplayNameChanged;
  }

  public boolean isModifiedInArborescenceChanged() {
    return modifiedIfArborescenceChanged;
  }

  public void setModifiedIfArborescenceChanged(final boolean modifiedIfArborescenceChanged) {
    this.modifiedIfArborescenceChanged = modifiedIfArborescenceChanged;
  }

  public void setModifiedIfNameChanged(final boolean modifiedIfNameChanged) {
    this.modifiedIfNameChanged = modifiedIfNameChanged;
  }

  @Override
  public void propertyChange(final PropertyChangeEvent evt) {
    if (modifiedIfDisplayNameChanged && Node.PROP_DISPLAY_NAME.equals(evt.getPropertyName())) {
      target.setModified(true);
    }
    if (modifiedIfNameChanged && Node.PROP_NAME.equals(evt.getPropertyName())) {
      target.setModified(true);
    }
  }

  @Override
  public void childrenAdded(final NodeMemberEvent ev) {
    if (modifiedIfArborescenceChanged) {
      target.setModified(true);
    }
  }

  @Override
  public void childrenRemoved(final NodeMemberEvent ev) {
    if (modifiedIfArborescenceChanged) {
      target.setModified(true);
    }
  }

  @Override
  public void childrenReordered(final NodeReorderEvent ev) {
    if (modifiedIfArborescenceChanged) {
      target.setModified(true);
    }
  }

  @Override
  public void nodeDestroyed(final NodeEvent ev) {
    if (modifiedIfArborescenceChanged) {
      target.setModified(true);
    }
  }
}
