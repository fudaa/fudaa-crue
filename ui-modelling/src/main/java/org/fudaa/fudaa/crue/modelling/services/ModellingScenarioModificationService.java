package org.fudaa.fudaa.crue.modelling.services;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.common.services.ModificationStateContainer;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Observable;

/**
 * Contient les aspects modifiés des différentes données. Met à jour la perspective ( setDirty) si nécessaire.
 *
 * @author deniger
 */
@ServiceProvider(service = ModellingScenarioModificationService.class)
public class ModellingScenarioModificationService extends Observable implements PropertyChangeListener {
    /**
     * si une donnée de géoloc est modifiée. Envoyé à chaque modification.
     */
    public static final String PROPERTY_GEOLOC_MODIFIED = "geoLocModified";
    /**
     * les données de niveau scenario sont modifiées: il s'agit de l'état de modification ( modifié ou non)
     */
    private static final String PROPERTY_SCENARIO_MODIFIED_STATE = "scenarioModified";
    private static final String PROPERTY_SCENARIO_OCAL_MODIFIED_STATE = "scenarioOcalModified";
    /**
     * il s'agit de l'état de modification ( modifié ou non): si l'état modifié des données geo est modifié
     */
    private static final String PROPERTY_GEOLOC_MODIFIED_STATE = "geoLocStateModified";
    /**
     * il s'agit de l'état de modification ( modifié ou non): si une donnée de niveau étude est modifiée
     */
    private static final String PROPERTY_STUDYCONFIG_MODIFIED_STATE = "studyConfigModified";
    /**
     * si l'état modifié ou non a été changé pour des données externes ( images, dessins).
     */
    private static final String PROPERTY_STUDYEXTERNCONFIG_MODIFIED_STATE = "studyExternConfigModified";
    final ModificationStateContainer modificationStateContainer = new ModificationStateContainer();
    final ModellingScenarioServiceImpl modellingScenarioServiceImpl = Lookup.getDefault().lookup(ModellingScenarioServiceImpl.class);

    public ModellingScenarioModificationService() {
        modificationStateContainer.addModifiedListener(this);
    }

    public ModellingScenarioService getModellingScenarioService() {
        return modellingScenarioServiceImpl;
    }

    /**
     * @param listener sera avertit si l'état "est modifié" a changé.
     */
    public void addModifiedStateListener(PropertyChangeListener listener) {
        modificationStateContainer.addModifiedListener(listener);
    }

    /**
     * @param listener listener de l'état "est modifié" a enleve.
     */
    public void removeModifiedListener(PropertyChangeListener listener) {
        modificationStateContainer.removeModifiedSateListener(listener);
    }

    /**
     * @param property la propriété à écouter.
     * @param listener le listener
     */
    public void addPropertyListener(String property, PropertyChangeListener listener) {
        modificationStateContainer.addPropertyListener(property, listener);
    }

    public void removePropertyListener(String property, PropertyChangeListener listener) {
        modificationStateContainer.removePropertyListener(property, listener);
    }

    /**
     * RAS des états modifiés concernant
     */
    public void clearGeoLocModifiedState() {
        modificationStateContainer.fireModification(PROPERTY_GEOLOC_MODIFIED);
        modificationStateContainer.clearModifiedState(PROPERTY_GEOLOC_MODIFIED_STATE);
    }

    /**
     * Les données geoloc ont été modifiées
     */
    public void setGeoLocModified() {
        modificationStateContainer.fireModification(PROPERTY_GEOLOC_MODIFIED);
        modificationStateContainer.setModified(PROPERTY_GEOLOC_MODIFIED_STATE);
    }

    /**
     * Utilise si modification massive.
     *
     * @param newScenario le scenario modifie
     */
    public void setScenarioModifiedMassive(EMHScenario newScenario) {
        modellingScenarioServiceImpl.setNewScenario(newScenario);
        modificationStateContainer.setModified(PROPERTY_SCENARIO_MODIFIED_STATE);
    }

    /**
     * la configuration {@link #PROPERTY_STUDYCONFIG_MODIFIED_STATE} a été modifié
     */
    public void setStudyConfigModified() {
        modificationStateContainer.setModified(PROPERTY_STUDYCONFIG_MODIFIED_STATE);
    }

    /**
     * la configuration {@link #PROPERTY_STUDYEXTERNCONFIG_MODIFIED_STATE} a été modifié
     */
    public void setStudyExternConfigModified() {
        modificationStateContainer.setModified(PROPERTY_STUDYEXTERNCONFIG_MODIFIED_STATE);
    }

    /**
     * annule l'état modifié concernnant les données du scenario et de ocal
     */
    public void clearScenarioModifiedState() {
        modificationStateContainer.clearModifiedState(PROPERTY_SCENARIO_MODIFIED_STATE);
        modificationStateContainer.clearModifiedState(PROPERTY_SCENARIO_OCAL_MODIFIED_STATE);
    }

    /**
     * @return true si les données du scenario courant sont modifiées
     * @see #PROPERTY_SCENARIO_MODIFIED_STATE
     */
    public boolean isScenarioModified() {
        return modificationStateContainer.isModified(PROPERTY_SCENARIO_MODIFIED_STATE);
    }

    /**
     * @param event l'event de modification
     */
    public void setScenarioModified(ScenarioModificationEvent event) {
        modificationStateContainer.setModified(PROPERTY_SCENARIO_MODIFIED_STATE);
        if (event != null && event.isEMHNameModification()) {
            setGeoLocModified();
        }
        setChanged();
        notifyObservers(event);
        clearChanged();
    }

    /**
     * @return true si les données de geoloc sont modifiées
     * @see #PROPERTY_GEOLOC_MODIFIED_STATE
     */
    public boolean isGeoLocModified() {
        return modificationStateContainer.isModified(PROPERTY_GEOLOC_MODIFIED_STATE);
    }

    /**
     * @return true si la configuration niveau etude a été modifiée
     * @see #PROPERTY_STUDYCONFIG_MODIFIED_STATE
     */
    public boolean isStudyConfigModified() {
        return modificationStateContainer.isModified(PROPERTY_STUDYCONFIG_MODIFIED_STATE);
    }

    /**
     * @return true si la configuration externe (image, dessins) a été modifiée
     * @see #PROPERTY_STUDYEXTERNCONFIG_MODIFIED_STATE
     */
    public boolean isStudyExternConfigModified() {
        return modificationStateContainer.isModified(PROPERTY_STUDYEXTERNCONFIG_MODIFIED_STATE);
    }

    /**
     * Utiliser pour savoir si on doit enregistrer les données ui des paramètres OCAL.
     *
     * @return true si les données ocal ont été modifiées
     */
    public boolean isScenarioOcalModified() {
        return modificationStateContainer.isModified(PROPERTY_SCENARIO_OCAL_MODIFIED_STATE);
    }

    /**
     * RAS de l'état modifiée #PROPERTY_STUDYCONFIG_MODIFIED_STATE
     */
    public void clearStudyConfigModifiedState() {
        modificationStateContainer.clearModifiedState(PROPERTY_STUDYCONFIG_MODIFIED_STATE);
    }

    /**
     * RAS de l'état modifiée #PROPERTY_STUDYEXTERNCONFIG_MODIFIED_STATE
     */
    public void clearStudyExternConfigModifiedState() {
        modificationStateContainer.clearModifiedState(PROPERTY_STUDYEXTERNCONFIG_MODIFIED_STATE);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        PerspectiveServiceModelling perpective = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
        perpective.setDirty(modificationStateContainer.isModified());
    }

    /**
     * appele par save pour nettoyer tous les flags "modifiés"
     */
    public void clear() {
        modificationStateContainer.clear();
    }

    /**
     *
     * @return true si qqchose de modifié
     */
    public boolean isModified() {
        return modificationStateContainer.isModified();
    }

    /**
     * Enregistre l'etat modifié des données UI de OCAL.
     */
    public void setOcalUiStateModified() {
        modificationStateContainer.setModified(PROPERTY_SCENARIO_OCAL_MODIFIED_STATE);
    }
}
