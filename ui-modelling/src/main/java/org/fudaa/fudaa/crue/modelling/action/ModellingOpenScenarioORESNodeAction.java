package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.modelling.calcul.ModellingScenarioORESTopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public abstract class ModellingOpenScenarioORESNodeAction extends AbstractModellingOpenScenarioNodeAction {

  public ModellingOpenScenarioORESNodeAction(String name) {
    super(name, ModellingScenarioORESTopComponent.MODE, ModellingScenarioORESTopComponent.TOPCOMPONENT_ID);
  }

  public static void open(EMHScenario scenario) {
    if (scenario == null) {
      return;
    }
    ModellingOpenScenarioORESNodeAction action = SystemAction.get(ModellingOpenScenarioORESNodeAction.Reopen.class);
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(scenario));
    action.performAction(new Node[]{node});
  }

  public static class Reopen extends ModellingOpenScenarioORESNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenScenarioORESNodeAction.class, "ModellingScenarioORESNodeAction.Name"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenScenarioORESNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenScenarioORESNodeAction.class, "ModellingScenarioORESNodeAction.NewFrame.Name"));
      setReopen(false);
    }
  }
}
