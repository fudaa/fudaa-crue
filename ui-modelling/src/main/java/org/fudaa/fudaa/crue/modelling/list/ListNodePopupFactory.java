/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.list;

import org.fudaa.ctulu.gui.CtuluTableSimpleExporter;
import org.fudaa.ctulu.table.CtuluTableModelDefault;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.node.OutlineViewExportTableModel;
import org.fudaa.fudaa.crue.modelling.ModellingTopComponentWithSpecificAction;
import org.fudaa.fudaa.crue.modelling.action.ModellingOpenInsertBrancheNodeAction;
import org.fudaa.fudaa.crue.modelling.calcul.ModellingListCLimMsTopExportImportPopupBuilder;
import org.fudaa.fudaa.crue.modelling.emh.ModellingOpenEMHNodeAction;
import org.fudaa.fudaa.crue.modelling.loi.ProfilSectionOpenOnSelectedSectionNodeAction;
import org.openide.explorer.view.NodePopupFactory;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Frederic Deniger
 */
public class ListNodePopupFactory extends NodePopupFactory {
  final AbstractModellingListEditionTopComponent topComponent;

  public ListNodePopupFactory(final AbstractModellingListEditionTopComponent topComponent) {
    this.topComponent = topComponent;
  }

  public static class OpenEMHAction extends AbstractAction {
    final transient EMH emh;

    public OpenEMHAction(final EMH emh) {
      super(NbBundle.getMessage(ListNodePopupFactory.class, "OpenEMHAction.Name", emh.getNom()));
      this.emh = emh;
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
      ModellingOpenEMHNodeAction.open(emh, true);
    }
  }

  public static class InsertBrancheAction extends AbstractAction {
    final transient CatEMHNoeud emh;

    public InsertBrancheAction(final CatEMHNoeud emh) {
      super(NbBundle.getMessage(ListNodePopupFactory.class, "InsertBrancheAction.Name", emh.getNom()));
      this.emh = emh;
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
      final ModellingOpenInsertBrancheNodeAction action = SystemAction.get(ModellingOpenInsertBrancheNodeAction.class);
      final AbstractNode node = new AbstractNode(Children.LEAF, Lookups.singleton(emh));
      action.performAction(new Node[]{node});
    }
  }

  public static class OpenEMHProfilAction extends AbstractAction {
    final transient EMH emh;

    public OpenEMHProfilAction(final EMH emh) {
      super(NbBundle.getMessage(ListNodePopupFactory.class, "OpenEMHProfilAction.Name", emh.getNom()));
      this.emh = emh;
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
      ProfilSectionOpenOnSelectedSectionNodeAction.open(emh.getUiId(), true);
    }
  }

  @Override
  public JPopupMenu createPopupMenu(final int row, final int column, final Node[] selectedNodes, final Component component) {
    final boolean isSpecific = topComponent instanceof ModellingTopComponentWithSpecificAction;
    final JPopupMenu res;
    if (isSpecific && !((ModellingTopComponentWithSpecificAction) topComponent).useQuickFilterActions(column)) {
      res = new JPopupMenu();
    } else {
      if (column == 0) {
        res = new JPopupMenu();
      } else {
        res = super.createPopupMenu(row, column, selectedNodes, component);
      }
    }
    if (selectedNodes != null && selectedNodes.length == 1) {
      final Node node = selectedNodes[0];
      final EMH emh = node.getLookup().lookup(EMH.class);
      if (emh != null) {
        res.insert(new JSeparator(), 0);
        if (!topComponent.isModified()) {
          if (EnumCatEMH.SECTION.equals(emh.getCatType())) {
            final CatEMHBranche branche = ((CatEMHSection) emh).getBranche();
            if (branche != null) {
              res.insert(new OpenEMHAction(branche), 0);
            }
          } else if (EnumCatEMH.BRANCHE.equals(emh.getCatType())) {
            final CatEMHBranche branche = ((CatEMHBranche) emh);
            res.insert(new InsertBrancheAction(branche.getNoeudAval()), 0);
            res.insert(new InsertBrancheAction(branche.getNoeudAmont()), 0);
            res.insert(new JSeparator(), 0);
            res.insert(new OpenEMHAction(branche.getNoeudAval()), 0);
            res.insert(new OpenEMHAction(branche.getNoeudAmont()), 0);
          }
        }
        if (EMHSectionProfil.class.equals(emh.getClass())) {
          final OpenEMHProfilAction profilAction = new OpenEMHProfilAction(emh);
          res.insert(profilAction, 0);
          profilAction.setEnabled(!topComponent.isModified());
        }
        final OpenEMHAction action = new OpenEMHAction(emh);
        res.insert(action, 0);
        NodeHelper.useBoldFont((JComponent) res.getComponent(0));
        action.setEnabled(!topComponent.isModified());
      }
      if (isSpecific) {
        ((ModellingTopComponentWithSpecificAction) topComponent).addCustomAction(node, res);
      }
    } else if (isSpecific) {
      ((ModellingTopComponentWithSpecificAction) topComponent).addCustomAction(null, res);
    }

    final JMenuItem menuItemExport = new JMenuItem(NbBundle.getMessage(ModellingListCLimMsTopExportImportPopupBuilder.class, "button.export.name"));
    menuItemExport.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        final CtuluTableModelDefault defaultModel = new OutlineViewExportTableModel(topComponent.outlineView.getOutline(), null, null);
        CtuluTableSimpleExporter.doExport(';', defaultModel, CtuluUIForNetbeans.DEFAULT);
      }
    });
    res.add(menuItemExport);

    return res;
  }
}
