package org.fudaa.fudaa.crue.modelling.action;

import javax.swing.Action;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.fudaa.crue.comparison.ScenarioComparaisonLauncher;
import org.fudaa.fudaa.crue.loader.LoaderService;
import org.fudaa.fudaa.crue.loader.ProjectLoadContainer;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
id = "org.fudaa.fudaa.crue.modelling.action.ModellingComparisonOtherEtuAction")
@ActionRegistration(displayName = "#CTL_ModellingComparisonOtherEtuAction")
@ActionReferences({
  @ActionReference(path = "Actions/Modelling", position = 5, separatorAfter = 6)
})
public final class ModellingComparisonOtherEtuAction extends AbstractModellingAction {

  private final LoaderService loaderService = Lookup.getDefault().lookup(LoaderService.class);
  final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public ModellingComparisonOtherEtuAction() {
    super(false);
    putValue(Action.NAME, NbBundle.getMessage(AbstractModellingAction.class, "CTL_ModellingComparisonOtherEtuAction"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingComparisonOtherEtuAction();
  }

  @Override
  public void doAction() {
    ProjectLoadContainer otherProjet = loaderService.loadProject();
    if (otherProjet != null) {
      final EMHProjet projet = otherProjet.getProjet();
      boolean isSame = otherProjet.getEtuFile().equals(projetService.getEtuFile());
      new ScenarioComparaisonLauncher(scenarioService.getManagerScenarioLoaded(), scenarioService.getScenarioLoaded(), projetService.getSelectedProject().getPropDefinition(),
              null, projet, isSame).compute();
    }
  }
}
