/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.emh;

import com.memoire.bu.BuGridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.edition.bean.BrancheEditionContent;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.common.view.ItemVariableView;
import org.fudaa.fudaa.crue.modelling.loi.ModellingOpenDcspLoiNodeAction;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.explorer.ExplorerManager;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 * Branche Type 2
 *
 * @author fred
 */
public class ModellingEMHBrancheBarrageGeneriquePanel extends JPanel implements ModellingEMHBrancheSpecificEditor, Observer {

  final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(
          ModellingScenarioService.class);
  PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
  final ModellingEMHBrancheTopComponent parent;
  final List<ItemVariableView> properties;
  final JComboBox cbSectionPilote;

  ModellingEMHBrancheBarrageGeneriquePanel(final CatEMHBranche branche, final ModellingEMHBrancheTopComponent parent) {
    super(new BuGridLayout(2, 10, 10));
    this.parent = parent;
    DonCalcSansPrtBrancheBarrageGenerique dcsp = EMHHelper.selectFirstOfClass(branche.getInfosEMH(), DonCalcSansPrtBrancheBarrageGenerique.class);
    if (dcsp == null) {
      dcsp = new DonCalcSansPrtBrancheBarrageGenerique(getCcm());
    }
    properties = ItemVariableView.create(DecimalFormatEpsilonEnum.COMPARISON,dcsp, getCcm(), DonCalcSansPrtBrancheNiveauxAssocies.PROP_QLIMINF,
            DonCalcSansPrtBrancheNiveauxAssocies.PROP_QLIMSUP);
    for (final ItemVariableView propertyEditorPanel : properties) {
      propertyEditorPanel.addObserver(this);
      add(new JLabel(propertyEditorPanel.getLabelName()));
      add(propertyEditorPanel.getPanelComponent());
    }
    add(new JLabel(NbBundle.getMessage(ModellingEMHBrancheBarrageGeneriquePanel.class, "SectionPilote.DisplayName")));
    cbSectionPilote = createSectionPilote(branche);
    cbSectionPilote.addActionListener(parent.modifiedActionListener);
    add(cbSectionPilote);
    add(new JLabel(NbBundle.getMessage(ModellingEMHBrancheBarrageGeneriquePanel.class, "LoiQDz.Label.DisplayName")));
    JButton bt = new JButton(NbBundle.getMessage(ModellingEMHBrancheBarrageGeneriquePanel.class, "LoiQDz.Button.DisplayName"));
    bt.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        openLoiQDz();
      }
    });
    add(bt);
    add(new JLabel(NbBundle.getMessage(ModellingEMHBrancheBarrageGeneriquePanel.class, "LoiQpilZam.Label.DisplayName")));
    bt = new JButton(NbBundle.getMessage(ModellingEMHBrancheBarrageGeneriquePanel.class, "LoiQpilZam.Button.DisplayName"));
    bt.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        openLoiQpilZam();
      }
    });
    add(bt);
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return null;
  }

  public static JComboBox createSectionPilote(final CatEMHBranche branche) {
    final List<CatEMHSection> sections = branche.getParent().getSections();
    final CatEMHSection sectionPilote = EMHHelper.getSectionPilote(branche);
    final JComboBox cbSectionPilote = new JComboBox(sections.toArray(new CatEMHSection[0]));
    cbSectionPilote.setRenderer(new ObjetNommeCellRenderer());
    cbSectionPilote.setSelectedItem(sectionPilote);
    return cbSectionPilote;
  }

  private void openLoiQDz() {
    ModellingOpenDcspLoiNodeAction.open(parent.getEMHUid(), EnumTypeLoi.LoiQDz, false);
  }

  private void openLoiQpilZam() {
    ModellingOpenDcspLoiNodeAction.open(parent.getEMHUid(), EnumTypeLoi.LoiQpilZam, false);
  }

  @Override
  public void update(final Observable o, final Object arg) {
    parent.setModified(true);
  }

  private CrueConfigMetier getCcm() {
    return modellingScenarioService.getSelectedProjet().getPropDefinition();
  }

  @Override
  public void setEditable(final boolean editable) {
    for (final ItemVariableView object : properties) {
      object.setEditable(editable);
    }
    cbSectionPilote.setEnabled(editable);
  }

  @Override
  public void fillWithData(final BrancheEditionContent content) {
    final DonCalcSansPrtBrancheBarrageGenerique dcsp = new DonCalcSansPrtBrancheBarrageGenerique(getCcm());
    ItemVariableView.transferToBean(dcsp, properties);
    content.setSectionPilote((CatEMHSection) cbSectionPilote.getSelectedItem());
    content.setDcsp(dcsp);
  }

  @Override
  public JComponent getComponent() {
    return this;
  }
}
