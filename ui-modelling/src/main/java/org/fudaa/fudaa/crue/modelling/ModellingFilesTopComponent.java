package org.fudaa.fudaa.crue.modelling;

import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZCalqueImageRaster;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioVisuService;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanelConfigurer;
import org.fudaa.fudaa.crue.planimetry.action.DeleteCalqueAction;
import org.fudaa.fudaa.crue.planimetry.controller.AbstractGroupExternController;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.*;
import java.util.List;

/**
 * Contient l'arbres de calques. Devrait être renomé.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.modelling//ModellingFilesTopComponent//EN",
    autostore = false)
@TopComponent.Description(preferredID = ModellingFilesTopComponent.TOPCOMPONENT_ID,
    iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png",
    persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "modelling-bottomLeft", openAtStartup = false, position = 2)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.modelling.ModellingFilesTopComponent")
@ActionReference(path = "Menu/Window/Modelling", position = 7)
@TopComponent.OpenActionRegistration(displayName = ModellingFilesTopComponent.TOPCOMPONENT_ACTION_DISPLAYNAME,
    preferredID = ModellingFilesTopComponent.TOPCOMPONENT_ID)
public final class ModellingFilesTopComponent extends AbstractModellingTopComponent implements LookupListener, PropertyChangeListener, Observer {
  public static final String TOPCOMPONENT_ID = "ModellingFilesTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  private final ModellingScenarioVisuService scenarioVisuService = Lookup.getDefault().lookup(
      ModellingScenarioVisuService.class);
  private Lookup.Result<PlanimetryVisuPanel> resultatVisuPanel;

  public ModellingFilesTopComponent() {
    initComponents();
    setName(NbBundle.getMessage(ModellingFilesTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingFilesTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
  }

  @Override
  protected void setEditable(boolean b) {
    if (panel != null) {
      panel.getPlanimetryController().getGroupExternController().setArbreEditable(b);
    }
    if (bArbreCalque != null) {
      bArbreCalque.setEditable(b);
    }
  }

  @Override
  public void cancelModificationHandler() {
  }

  @Override
  public void valideModificationHandler() {
  }

  private PlanimetryVisuPanel panel;
  private BArbreCalque bArbreCalque;

  @Override
  protected String getViewHelpCtxId() {
    return "vueGestionnaireCalques";
  }

  /**
   * rien a faire: on attend le visu panel
   */
  @Override
  protected void scenarioLoaded() {
    if (scenarioVisuService.getPlanimetryVisuPanel() != null) {
      scenarioVisuLoaded();
    }
  }

  protected void scenarioVisuLoaded() {
    panel = scenarioVisuService.getPlanimetryVisuPanel();
    this.removeAll();
    final BArbreCalqueModel arbreCalqueModel = panel.getArbreCalqueModel();
    bArbreCalque = new BArbreCalque(arbreCalqueModel);
    bArbreCalque.setRootVisible(false);
    PlanimetryVisuPanelConfigurer configurer = new PlanimetryVisuPanelConfigurer();
    add(configurer.createOneLayerCheckBox(panel), BorderLayout.NORTH);
    add(new JScrollPane(bArbreCalque));
    this.repaint();
    this.revalidate();
    List<BCalque> mainLayers = panel.getPlanimetryController().getMainLayers();
    for (BCalque bCalque : mainLayers) {
      bCalque.addPropertyChangeListener(BSelecteurCheckBox.PROP_VISIBLE, this);
    }
    panel.getPlanimetryController().getGroupExternController().addObserver(this);
    //une action de suppression permettant de conserver les noeuds dépliés.
    arbreCalqueModel.setActionDelete(new DeleteCalqueAction(arbreCalqueModel, bArbreCalque));

  }

  private final Set<String> propertiesToListenTo = new HashSet<>(
      Arrays.asList("title", "modele", "visible", ZCalqueImageRaster.PROP_IMAGE, AbstractGroupExternController.PROPERTY_LAYER_ADDED,
          AbstractGroupExternController.PROPERTY_LAYER_REMOVED,
          PlanimetryAdditionalLayerContrat.PROPERTY_LAYER_CONFIGURATION));

  @Override
  public void update(Observable o, Object arg) {
    if (propertiesToListenTo.contains(arg)) {
      modellingScenarioModificationService.setStudyExternConfigModified();
      modellingScenarioModificationService.setStudyConfigModified();
    }
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    modellingScenarioModificationService.setStudyConfigModified();
  }

  @Override
  protected void scenarioReloaded() {
    //rien a faire
  }

  @Override
  protected void scenarioUnloaded() {
    if (panel != null) {
      List<BCalque> mainLayers = panel.getPlanimetryController().getMainLayers();
      for (BCalque bCalque : mainLayers) {
        bCalque.removePropertyChangeListener(BSelecteurCheckBox.PROP_VISIBLE, this);
      }
      panel.getPlanimetryController().getGroupExternController().deleteObserver(this);
    }
    this.removeAll();
    bArbreCalque = null;
    this.add(new JLabel(MessagesModelling.getMessage("emhTopComponent.NoScenarioLoadedInformations")));
    super.revalidate();
    this.repaint();
  }

  boolean alreadyOpened;

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  @Override
  public void componentClosedTemporarily() {
  }

  void writeProperties(java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
    p.setProperty("version", "1.0");
  }

  void readProperties(java.util.Properties p) {
  }

  private class VisuPanelLookupListener implements LookupListener {
    @Override
    public void resultChanged(LookupEvent ev) {
      if (scenarioVisuService.getPlanimetryVisuPanel() != null) {
        scenarioVisuLoaded();
      }
    }
  }

  VisuPanelLookupListener visuLookupListener;

  @Override
  protected void componentOpenedHandler() {
    if (resultatVisuPanel == null) {
      visuLookupListener = new VisuPanelLookupListener();
      resultatVisuPanel = scenarioVisuService.getLookup().lookupResult(PlanimetryVisuPanel.class);
      resultatVisuPanel.addLookupListener(visuLookupListener);
    }
  }

  @Override
  protected void componentClosedDefinitlyHandler() {
    super.componentClosedDefinitlyHandler();
    if (resultatVisuPanel != null) {
      resultatVisuPanel.removeLookupListener(visuLookupListener);
      resultatVisuPanel = null;
    }
  }
}
