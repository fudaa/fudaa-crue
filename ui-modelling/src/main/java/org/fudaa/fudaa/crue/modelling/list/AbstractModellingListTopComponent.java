package org.fudaa.fudaa.crue.modelling.list;

import com.memoire.bu.BuGridLayout;
import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.DisableSortHelper;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.helper.UserSaveAnswer;
import org.fudaa.fudaa.crue.modelling.AbstractModellingTopComponent;
import org.fudaa.fudaa.crue.modelling.listener.ExplorerManagerListenerHelper;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.util.List;

/**
 * @author deniger
 */
public abstract class AbstractModellingListTopComponent extends AbstractModellingTopComponent implements ExplorerManager.Provider {
  private JComboBox cbSousModele;
  Long sousModeleUid;
  protected OutlineView outlineView;
  protected final ExplorerManagerListenerHelper explorerHelper;
  private DisableSortHelper sortDisabler;

  public AbstractModellingListTopComponent() {
    explorerHelper = new ExplorerManagerListenerHelper(this);
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return explorerHelper.getExplorerManager();
  }

  protected EMHSousModele getSousModele() {
    return (EMHSousModele) getModellingService().getScenarioLoaded().getIdRegistry().getEmh(getSousModeleUid());
  }

  protected JPanel buildNorthPanel() {
    JPanel pn = new JPanel(new BuGridLayout(2, 3, 3));
    pn.add(new JLabel(NbBundle.getMessage(AbstractModellingListTopComponent.class, "ListTopComponent.SousModele.Label")));
    pn.setBorder(BorderFactory.createEmptyBorder(5, 2, 5, 0));
    pn.add(cbSousModele);
    return pn;
  }

  protected EMH updateSousModeleNameInUI(EMHScenario scenarioLoaded) {
    if (scenarioLoaded == null) {
      cbSousModele.setModel(new DefaultComboBoxModel());
      return null;
    }

    final List<EMHSousModele> sousModeles = scenarioLoaded.getSousModeles();
    EMH emh = scenarioLoaded.getIdRegistry().getEmh(sousModeleUid);
    cbSousModele.setModel(new DefaultComboBoxModel(sousModeles.toArray(new EMHSousModele[0])));
    cbSousModele.setSelectedItem(emh);
    return emh;
  }

  public Long getSousModeleUid() {
    return sousModeleUid;
  }

  public void setSousModeleUid(Long sousModeleUid) {
    this.sousModeleUid = sousModeleUid;
  }

  public void reloadWithSousModeleUid(Long sousModeleUid) {
    if (!ObjectUtils.equals(this.sousModeleUid, sousModeleUid)) {
      this.sousModeleUid = sousModeleUid;
      scenarioReloaded();
    }
  }

  @Override
  protected void scenarioUnloaded() {
    explorerHelper.scenarioUnloaded();
    cbSousModele.setModel(new DefaultComboBoxModel());
    close();
  }

  boolean editable;

  @Override
  protected void setEditable(boolean b) {
    editable = b;
    explorerHelper.setEditable(b);
    sortDisabler.setDisableSort(b);
  }

  public final boolean isEditable() {
    return editable;
  }

  protected void createComponent(String firstColumnName) {
    outlineView = new OutlineView(firstColumnName);
    outlineView.getOutline().setColumnHidingAllowed(false);
    outlineView.getOutline().setRootVisible(false);
    outlineView.setDefaultActionAllowed(true);
    outlineView.setTreeSortable(false);
    sortDisabler = new DisableSortHelper(outlineView);
    cbSousModele = new JComboBox();
    cbSousModele.setRenderer(new ObjetNommeCellRenderer());
    cbSousModele.addItemListener(e -> {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        sousModeleSelectionChanged();
      }
    });
    add(buildNorthPanel(), BorderLayout.NORTH);
    add(outlineView);
    add(super.createCancelSaveButtons(), BorderLayout.SOUTH);
    installPropertyColumns();
  }

  protected boolean shouldSaveBeforeSwitchingSousModele() {
    return true;
  }

  private void sousModeleSelectionChanged() {
    if (cbSousModele.getSelectedItem() != null) {
      final Long uiId = ((EMHSousModele) cbSousModele.getSelectedItem()).getUiId();
      if (!uiId.equals(this.sousModeleUid)) {
        if (shouldSaveBeforeSwitchingSousModele() && isModified()) {
          final UserSaveAnswer userSaveAnswer = askForSave(NbBundle.getMessage(getClass(),"ValidationVue.DontChangeSousModele"));
          if (UserSaveAnswer.CANCEL.equals(userSaveAnswer)) {
            cbSousModele.setSelectedItem(getScenario().getIdRegistry().getEmh(sousModeleUid));
          } else {
            reloadWithSousModeleUid(uiId);
          }
        } else {
          reloadWithSousModeleUid(uiId);
        }
      }
    }
  }

  protected abstract void installPropertyColumns();

  @Override
  protected void readPreferences() {
    DialogHelper.readInPreferences(outlineView, "outlineView", getClass());
  }

  @Override
  protected void writePreferences() {
    DialogHelper.writeInPreferences(outlineView, "outlineView", getClass());
  }
}
