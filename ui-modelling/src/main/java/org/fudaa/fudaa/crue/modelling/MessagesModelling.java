package org.fudaa.fudaa.crue.modelling;

import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class MessagesModelling {

  public static String getMessage(String code) {
    return NbBundle.getMessage(MessagesModelling.class, code);
  }

  public static String getMessage(String code, String value) {
    return NbBundle.getMessage(MessagesModelling.class, code, value);
  }
}
