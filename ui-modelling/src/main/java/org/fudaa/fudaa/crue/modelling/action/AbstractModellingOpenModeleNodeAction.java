package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.fudaa.crue.modelling.calcul.AbstractModeleTopComponent;
import org.openide.nodes.Node;
import org.openide.windows.TopComponent;

/**
 *
 * @author deniger
 */
public abstract class AbstractModellingOpenModeleNodeAction<T extends AbstractModeleTopComponent> extends AbstractModellingOpenTopNodeAction<T> {

  public AbstractModellingOpenModeleNodeAction(String name, String mode, String topComponentId) {
    super(name, mode, topComponentId);
  }

  @Override
  protected final boolean isEnable(Node[] activatedNodes) {
    return activatedNodes.length == 1 && activatedNodes[0].getLookup().lookup(EMHModeleBase.class) != null;
  }

  @Override
  protected final void activeWithValue(T opened, Node selectedNode) {
    EMHModeleBase sousModele = selectedNode.getLookup().lookup(EMHModeleBase.class);
    AbstractModeleTopComponent list = opened;
    list.reloadWithModeleUid(sousModele.getUiId());
  }

  @Override
  protected final TopComponent activateNewTopComponent(T create, Node selectedNode) {
    EMHModeleBase modele = selectedNode.getLookup().lookup(EMHModeleBase.class);
//    AbstractModeleTopComponent create = create();
    create.setModeleUid(modele.getUiId());
    return create;
  }

//  protected abstract AbstractModeleTopComponent create();
}
