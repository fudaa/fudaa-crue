/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.loi;

import java.awt.event.ItemEvent;
import java.util.Collections;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.EMHCasierProfil;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.IdRegistry;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.fudaa.crue.common.helper.ComboBoxHelper;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;

/**
 *
 * @author Frederic Deniger
 */
public class SousModeleCasierComponent {

  private static final EMHCasierProfil NULL_CASIER = new EMHCasierProfil(" ");
  private final JComboBox cbCasier;
  private final JComboBox cbSousModeles;

  public SousModeleCasierComponent() {
    final ObjetNommeCellRenderer objetNommeCellRenderer = new ObjetNommeCellRenderer();
    cbCasier = new JComboBox();
    cbCasier.setRenderer(objetNommeCellRenderer);

    cbSousModeles = new JComboBox();
    cbSousModeles.setRenderer(objetNommeCellRenderer);
    cbSousModeles.addItemListener(e -> {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        sousModeleSelectionChanged();
      }
    });

  }

  protected CatEMHCasier getSelectedCasier() {
    CatEMHCasier casier = (CatEMHCasier) cbCasier.getSelectedItem();
    if (casier == SousModeleCasierComponent.NULL_CASIER) {
      casier = null;
    }
    return casier;
  }
  private boolean updating;

  void updateValuesAndSelection(final EMHScenario scenario, final CatEMHCasier initCasierToSelect) {
    updating = true;
    final List<EMHSousModele> sousModelesList = scenario.getSousModeles();
    final EMHSousModele[] sousModelesArray = sousModelesList.toArray(new EMHSousModele[0]);
    final IdRegistry idRegistry = scenario.getIdRegistry();
    CatEMHCasier casierToSelect = initCasierToSelect;
    if (casierToSelect == null) {
      casierToSelect = getSelectedCasier();
    }
    EMHSousModele sousModeleToSelect;
    if (casierToSelect != null) {
      casierToSelect = (CatEMHCasier) idRegistry.getEmh(casierToSelect.getUiId());//au cas ou changement d'instance...
      sousModeleToSelect = casierToSelect.getParent();//au cas une casier ait changé de sous-modele
    } else {
      sousModeleToSelect = getSelectedSousModele();
      if (sousModeleToSelect != null) {
        sousModeleToSelect = (EMHSousModele) idRegistry.getEmh(sousModeleToSelect.getUiId());//au cas ou changement d'instance...
      }
    }
    cbSousModeles.setModel(new DefaultComboBoxModel(sousModelesArray));
    if (sousModeleToSelect == null && sousModelesArray.length == 1) {
      sousModeleToSelect = sousModelesArray[0];
    }
    cbSousModeles.setSelectedItem(sousModeleToSelect);
    if (sousModeleToSelect != null) {
      final List<CatEMHCasier> casiers = getCasier(sousModeleToSelect);
      cbCasier.setModel(new DefaultComboBoxModel(casiers.toArray(new CatEMHCasier[0])));
    }
    cbCasier.setSelectedItem(casierToSelect);
    updating = false;

  }

  public JComboBox getCbCasiers() {
    return cbCasier;
  }

  public JComboBox getCbSousModeles() {
    return cbSousModeles;
  }

  protected EMHSousModele getSelectedSousModele() {
    return (EMHSousModele) cbSousModeles.getSelectedItem();
  }

  /**
   *
   * @param sousModele le sous-modele
   * @return la liste des casiers contenant des profil section.
   */
  private List<CatEMHCasier> getCasier(final EMHSousModele sousModele) {
    if (sousModele == null) {
      return Collections.emptyList();
    }
    final List<CatEMHCasier> casiers = sousModele.getCasiers();
    return EMHHelper.selectInClasses(casiers, EMHCasierProfil.class);
  }

  void setEnabled(final boolean enabled) {
    cbCasier.setEnabled(enabled);
    cbSousModeles.setEnabled(enabled);
  }

  private void sousModeleSelectionChanged() {
    if (updating) {
      return;
    }
    final EMHSousModele sousModele = getSelectedSousModele();
    final List<CatEMHCasier> branches = getCasier(sousModele);
    ComboBoxHelper.setDefaultModel(cbCasier, branches);
    cbCasier.setSelectedIndex(-1);
    if (!branches.isEmpty()) {
      cbCasier.setSelectedIndex(0);
    }
  }

  public void clearComboBoxes() {
    updating = true;
    cbCasier.setModel(new DefaultComboBoxModel());
    cbSousModeles.setModel(new DefaultComboBoxModel());
    updating = false;
  }

  void setSelectedCasier(final CatEMHCasier casier) {
    cbCasier.setSelectedItem(casier == null ? NULL_CASIER : casier);
  }
}
