/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.OrdCalc;
import org.fudaa.dodico.crue.metier.factory.OrdCalcFactory;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertyStringReadOnly;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.modelling.node.AbstractModellingNodeFirable;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.fudaa.fudaa.crue.modelling.services.ModellingEditedCalculService;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.*;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;

import java.awt.datatransfer.Transferable;
import java.io.IOException;

/**
 * @author Frederic Deniger
 */
public class CalculNode extends AbstractModellingNodeFirable implements LookupListener {
    public static final String COLUMN_VALUE = "VALUE";
    public static final String COLUMN_SENS = "PROPERTY";
    public static final String COLUMN_ACTIF = "ACTIF";
    private final Lookup.Result<Long> resultat;
    final ModellingEditedCalculService modellingEditedCalculService = Lookup.getDefault().lookup(ModellingEditedCalculService.class);
    final OutlineView outlineView;
    /**
     * le noeud ocal utilisé avant désactivation
     */
    private OrdCalc oldUsedOrdCalc;

    public CalculNode(final OutlineView outlineView, final Children children, final Calc calc, final CrueConfigMetier ccm,
                      final PerspectiveServiceModelling perspectiveServiceModelling) {
        super(children, Lookups.fixed(calc, ccm), perspectiveServiceModelling);
        setName(calc.getNom());
        this.outlineView = outlineView;
        resultat = modellingEditedCalculService.getLookup().lookupResult(Long.class);
        resultat.addLookupListener(this);
    }

    public OrdCalc getOldUsedOrdCalc() {
        return oldUsedOrdCalc;
    }

    /**
     * @param oldUsedOrdCalc le noeud conservant les anciennes données de l'ordre de calcul.
     */
    public void setOldUsedOrdCalc(final OrdCalc oldUsedOrdCalc) {
        this.oldUsedOrdCalc = oldUsedOrdCalc;
    }

    /**
     * Renomme le node et le calcul.
     *
     * @param newName le nouveau nom du calcul.
     */
    void rename(final String newName) {
        setName(newName);
        getCalc().setNom(newName);
    }

    @Override
    public boolean canDestroy() {
        return isEditMode();
    }

    @Override
    public String getHtmlDisplayName() {
        final boolean actif = (getOcalNode() != null);
        final boolean edited = getCalc().getUiId() != null && getCalc().getUiId().equals(modellingEditedCalculService.getEditedCalculUid());
        if (actif) {
            if (edited) {
                return "<b>" + getName() + "</b>";
            }
            return getName();
        }
        final String start = "<font color:\"AAAAAA\">";
        if (edited) {
            return start + "<b>" + getName() + "</b></font>";
        }
        return start + getName() + "</font>";
    }

    @Override
    public void destroy() throws IOException {
        resultat.removeLookupListener(this);
        super.destroy();
    }

    @Override
    public void resultChanged(final LookupEvent ev) {
        fireDisplayNameChange(null, getDisplayName());
    }

    @Override
    public HelpCtx getHelpCtx() {
        return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(getLookup().lookup(Calc.class)));
    }

    public PerspectiveServiceModelling getPerspectiveServiceModelling() {
        return perspectiveServiceModelling;
    }

    @Override
    protected void fireObjectChange(final String property, final Object oldValue, final Object newValue) {
        super.fireObjectChange(property, oldValue, newValue);
        fireDisplayNameChange(null, getDisplayName());
    }

    public Calc getCalc() {
        return getLookup().lookup(Calc.class);
    }

    public CrueConfigMetier getCcm() {
        return getLookup().lookup(CrueConfigMetier.class);
    }

    @Override
    public PasteType getDropType(final Transferable t, final int action, final int index) {
        if (isEditMode() && getOcalNode() != null) {
            final Node dragged = DefaultNodePasteType.getMovedNodeInTransferable(t);
            if (dragged.getClass().equals(CalculNode.class)) {
                final CalculNode calculNodeDragged = (CalculNode) dragged;
                if (!calculNodeDragged.isActivated() || (!getCalc().getClass().equals(calculNodeDragged.getCalc().getClass()))) {
                    return null;
                }
                return new DefaultNodePasteType(dragged, this);
            }
        }
        return null;
    }

    @Override
    protected Sheet createSheet() {
        final PropertyStringReadOnly readOnly = new PropertyStringReadOnly(getCalc().geti18n(), COLUMN_VALUE, NbBundle.getMessage(CalculNode.class,
                "CalculType.Description"), PROP_DISPLAY_NAME);
        PropertyCrueUtils.configureNoCustomEditor(readOnly);
        final Sheet res = Sheet.createDefault();
        final Sheet.Set set = Sheet.createPropertiesSet();
        res.put(set);
        set.put(readOnly);
        final ActiveOcal ocal = new ActiveOcal(this, isActivated());
        final PropertyNodeBuilder builder = new PropertyNodeBuilder();
        final PropertySupportReflection actif = builder.createFromPropertyDesc(DecimalFormatEpsilonEnum.COMPARISON, ocal, this, "actif");
        actif.setName(COLUMN_ACTIF);
        set.put(actif);
        return res;
    }

    public CalculOcalNode getOcalNode() {
        final Node[] nodes = getChildren().getNodes();
        for (final Node node : nodes) {
            if (CalculOcalNode.class.equals(node.getClass())) {
                return (CalculOcalNode) node;
            }
        }
        return null;
    }

    public boolean isActivated() {
        return getOcalNode() != null;
    }

    @Override
    public SystemAction getDefaultAction() {
        return SystemAction.get(CalculEditActionNode.class);
    }

    @Override
    public SystemAction[] getActions() {
        return new SystemAction[]{
                SystemAction.get(CalculEditActionNode.class),
                SystemAction.get(CalculRenameNodeAction.class),
                SystemAction.get(CalculDuplicateNodeAction.class),
                SystemAction.get(CalculRemoveCalculNodeAction.class),};
    }

    public static class ActiveOcal {
        final CalculNode node;
        boolean actif;

        public ActiveOcal(final CalculNode node, final boolean actif) {
            this.node = node;
            this.actif = actif;
            assert node != null;
        }

        @SuppressWarnings("unused")
        @PropertyDesc(i18n = "actif.property")
        public boolean isActif() {
            return actif;
        }

        /**
         * Change l'état du calcul et persiste si nécessaire les anciens paramétrages.
         *
         * @param actif true si actif
         */
        @SuppressWarnings("unused")
        public void setActif(final boolean actif) {
            this.actif = actif;
            final CalculOcalNode ocalNode = node.getOcalNode();
            final boolean willBecomeInactif = !actif;
            if (willBecomeInactif) {
                //on sauvegarde les données ordcalc
                node.oldUsedOrdCalc = ocalNode == null ? null : ocalNode.getOrdCalc();
                if (ocalNode != null) {
                    node.getChildren().remove(new Node[]{ocalNode});
                }
            } else if (ocalNode == null) {
                OrdCalc ocal = null;
                if (node.oldUsedOrdCalc != null) {
                    ocal = node.oldUsedOrdCalc;
                }
                if (ocal == null) {
                    ocal = OrdCalcFactory.createDefaultOcal(node.getCalc(), node.getCcm());
                }
                final CalculOcalNode newnode = new CalculOcalNode(node.outlineView, ocal, node.getPerspectiveServiceModelling(), node.getCcm());
                node.getChildren().add(new Node[]{newnode});
            }
        }
    }
}
