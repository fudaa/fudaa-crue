/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.list;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.NodePopupFactory;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ListNodeAddPopupFactory extends NodePopupFactory {

  final AbstractModellingListAddTopComponent topComponent;

  public ListNodeAddPopupFactory(AbstractModellingListAddTopComponent topComponent) {
    this.topComponent = topComponent;
  }

  public static class AddEMH extends AbstractAction {

    final AbstractModellingListAddTopComponent topComponent;

    public AddEMH(AbstractModellingListAddTopComponent topComponent) {
      super(NbBundle.getMessage(ListNodeAddPopupFactory.class, "Add.Name"));
      this.topComponent = topComponent;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      if (topComponent.isEditable()) {
        topComponent.addEMHs();
      }
    }
  }

  public static class AddEMHFromClipboard extends AbstractAction {

    final AbstractModellingListAddTopComponent topComponent;

    public AddEMHFromClipboard(AbstractModellingListAddTopComponent topComponent) {
      super(NbBundle.getMessage(ListNodeAddPopupFactory.class, "Paste.Name"));
      this.topComponent = topComponent;
      final KeyStroke keyStroke = getKeyStroke();
      putValue(Action.ACCELERATOR_KEY, keyStroke);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      if (topComponent.isEditable()) {
        topComponent.addEMHsFromClipboard();
      }
    }

    public static KeyStroke getKeyStroke() {
      return KeyStroke.getKeyStroke("control V");
    }
  }

  public static KeyStroke getDeleteKeyStroke() {
    return KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0);
  }

  @Override
  public JPopupMenu createPopupMenu(int row, int column, Node[] selectedNodes, Component component) {
    JPopupMenu res = super.createPopupMenu(row, column, selectedNodes, component);

    final Action actionDelete = ExplorerUtils.actionDelete(topComponent.getExplorerManager(), false);
    actionDelete.setEnabled(topComponent.isEditable());
    actionDelete.putValue(Action.ACCELERATOR_KEY, getDeleteKeyStroke());
    res.insert(new JSeparator(), 0);
    res.insert(actionDelete, 0);
    ((JMenuItem) res.getComponent(0)).setText(NbBundle.getMessage(ListNodeAddPopupFactory.class, "Delete.Name"));
    res.insert(new JSeparator(), 0);
    final AddEMHFromClipboard addEMHFromClipboard = new AddEMHFromClipboard(topComponent);
    res.insert(addEMHFromClipboard, 0);
    final AddEMH addEMH = new AddEMH(topComponent);
    addEMHFromClipboard.setEnabled(topComponent.isEditable());
    addEMH.setEnabled(topComponent.isEditable());
    res.insert(addEMH, 0);
    return res;
  }
}
