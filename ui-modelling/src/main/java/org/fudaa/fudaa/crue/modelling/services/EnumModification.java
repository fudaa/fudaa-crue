/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.services;

/**
 *
 * @author deniger
 */
public enum EnumModification {

  ALL, EMH_ORDER, COMMENTAIRE, ACTIVITY, EMH_RELATION, EMH_NAME, EMH, EMH_CREATION, EMH_COMMENTAIRE, OPTG, OPTR, OPTI, ORES, PNUM, PCAL, DPTI, 
  DCSP, DLHY, DPTG, DFRT, OCAL
}
