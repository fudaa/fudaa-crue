package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.fudaa.crue.modelling.calcul.ModellingModelePNUMTopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public abstract class ModellingOpenModelePNUMNodeAction extends AbstractModellingOpenModeleNodeAction {

  public ModellingOpenModelePNUMNodeAction(String name) {
    super(name, ModellingModelePNUMTopComponent.MODE, ModellingModelePNUMTopComponent.TOPCOMPONENT_ID);
  }

  public static void open(EMHModeleBase modele) {
    if (modele == null) {
      return;
    }
    ModellingOpenModelePNUMNodeAction action = SystemAction.get(ModellingOpenModelePNUMNodeAction.Reopen.class);
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(modele));
    action.performAction(new Node[]{node});
  }

  public static class Reopen extends ModellingOpenModelePNUMNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenModelePNUMNodeAction.class, "ModellingModelePNUMNodeAction.Name"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenModelePNUMNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenModelePNUMNodeAction.class, "ModellingModelePNUMNodeAction.NewFrame.Name"));
      setReopen(false);
    }
  }
}
