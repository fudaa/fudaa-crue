package org.fudaa.fudaa.crue.modelling.calcul;

import java.util.List;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.edition.EditionUpdateActiveClimMs;
import org.fudaa.dodico.crue.edition.bean.DonCLimMLineContent;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import static org.fudaa.fudaa.crue.modelling.calcul.AbstractScenarioTopComponent.CENTER_NAME;
import org.fudaa.fudaa.crue.modelling.loi.ModellingOpenDLHYAction;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.fudaa.fudaa.crue.views.DonCLimMLineBuilder;
import org.fudaa.fudaa.crue.views.DonClimMTableModel;
import org.openide.explorer.ExplorerManager;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@TopComponent.Description(preferredID = ModellingListCLimMsTopComponent.TOPCOMPONENT_ID,
        iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png", persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ModellingListCLimMsTopComponent.MODE, openAtStartup = false, position = 0)
public final class ModellingListCLimMsTopComponent extends AbstractScenarioTopComponent implements ExplorerManager.Provider {
  //attention le mode modelling-listCLims doit être déclaré dans le projet branding (layer.xml)

  public static final String MODE = "modelling-listCLims";
  public static final String TOPCOMPONENT_ID = "ModellingListCLimMsTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;

  boolean editable;
  DonClimMTableModel tableModel;
  
  public ModellingListCLimMsTopComponent() {
    initComponents();
    setName(NbBundle.getMessage(ModellingListCLimMsTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingListCLimMsTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    setDoubleBuffered(false);
  }

  @Override
  public EMHScenario getScenario() {
    return modellingScenarioService.getScenarioLoaded();
  }
  
  public boolean isEditable() {
    return editable;
  }

  @Override
  public void setEditable(boolean b) {
    this.editable = b;
    if (tableModel != null) {
      tableModel.setEditable(b);
    }
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueListeDCLM";
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return null;
  }

  protected void rebuildCenterFor(DonClimMTableModel newModel) {
    JComponent oldCenter = CtuluLibSwing.findChildByName(this, CENTER_NAME);
    remove(oldCenter);
    setDoubleBuffered(false);
    final ModellingOpenDLHYAction loiDisplayer = new ModellingOpenDLHYAction();
    final ModellingListCLimMsTopExportImportPopupBuilder popupReceiver = new ModellingListCLimMsTopExportImportPopupBuilder(
            this, loiDisplayer);
    Pair<JScrollPane, DonClimMTableModel> createScrollPane = new DonCLimMLineBuilder().createScrollPane(newModel, getCcm(), getScenario(), true,
            loiDisplayer,
            popupReceiver);
    tableModel = createScrollPane.second;
    final JScrollPane newCenter = createScrollPane.first;
    popupReceiver.install(newCenter);
    tableModel.addTableModelListener(new TableModelListener() {
      @Override
      public void tableChanged(TableModelEvent e) {
        setModified(true);
      }
    });
    add(newCenter);
    newCenter.setName(CENTER_NAME);
    revalidate();
    repaint();
    newCenter.revalidate();
    newCenter.repaint();
  }

  @Override
  protected void scenarioUnloaded() {
    super.scenarioUnloaded();
    tableModel = null;
  }

  @Override
  protected JComponent buildCenterPanel(EMHScenario scenario, JComponent oldCenter) {
    setDoubleBuffered(false);
    final ModellingOpenDLHYAction loiDisplayer = new ModellingOpenDLHYAction();
    final ModellingListCLimMsTopExportImportPopupBuilder popupReceiver = new ModellingListCLimMsTopExportImportPopupBuilder(
            this, loiDisplayer);
    Pair<JScrollPane, DonClimMTableModel> createScrollPane = new DonCLimMLineBuilder().createScrollPane(getCcm(), scenario, true, loiDisplayer,
            popupReceiver);
    tableModel = createScrollPane.second;
    popupReceiver.install(createScrollPane.first);
    tableModel.addTableModelListener(new TableModelListener() {
      @Override
      public void tableChanged(TableModelEvent e) {
        setModified(true);
      }
    });
    return createScrollPane.first;
  }

  @Override
  protected void readPreferences() {
  }

  @Override
  protected void writePreferences() {
  }

  @Override
  public void valideModificationHandler() {
    JComponent center = CtuluLibSwing.findChildByName(this, CENTER_NAME);
    if (center != null) {
      JTable table = (JTable) ((JScrollPane) center).getViewport().getView();
      if (table.getCellEditor() != null) {
        table.getCellEditor().stopCellEditing();
      }
    }
    List<DonCLimMLineContent> dclims = tableModel.getDclims();
    CtuluLogGroup res = new EditionUpdateActiveClimMs(getCcm()).updateCLimMs(dclims, getScenario());
    if (!res.containsFatalError()) {
      modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.OCAL));
      setModified(false);

    }
    if (res.containsSomething()) {
      LogsDisplayer.displayError(res, getName());
    }
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
}
