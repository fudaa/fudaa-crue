package org.fudaa.fudaa.crue.modelling.edition;

import gnu.trove.TLongObjectHashMap;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.EditionChangeActivity;
import org.fudaa.dodico.crue.edition.EditionChangeUtils;
import org.fudaa.dodico.crue.edition.EditionRename;
import org.fudaa.dodico.crue.edition.ReorderEMHProcess;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioModificationService;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author deniger
 */
public class ListCasierModificationProcess {
    protected final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
            ModellingScenarioModificationService.class);

    /**
     * @param emhSousModele le sous-modele
     * @param casiers       les casier
     * @param dataByUid     les données par casier uid
     * @return le bilan logs / contient erreur bloquante.
     */
    public Pair<CtuluLog, Boolean> applyCasier(EMHSousModele emhSousModele, List<CatEMHCasier> casiers,
                                               TLongObjectHashMap<CasierData> dataByUid, CrueConfigMetier ccm) {
        Pair<CtuluLog, Boolean> res = new Pair<>(new CtuluLog(), Boolean.TRUE);
        //on vérifie les noms
        boolean casierNameChanged = false;
        boolean casierRelationNoeudChanged = false;

        List<CatEMHNoeud> noeuds = emhSousModele.getNoeuds();
        List<EMH> casierFromOtherSousModeles = emhSousModele.getParent().getParent().getIdRegistry().getEMHs(EnumCatEMH.CASIER);
        casierFromOtherSousModeles.removeAll(casiers);
        Set<String> idOfExistingCasier = TransformerHelper.toSetOfId(casierFromOtherSousModeles);
        Map<String, CatEMHNoeud> noeudByNoms = TransformerHelper.toMapOfNom(noeuds);
        for (CatEMHCasier casier : casiers) {
            CasierData newData = dataByUid.get(casier.getUiId());
            final String newCasierName = newData.casierName;
            //laissé mais inutile car le nom du casier n'est plus modifiable
            if (!newCasierName.equals(casier.getNom())) {
                casierNameChanged = true;
                if (!ValidationPatternHelper.isNameValide(newCasierName, EnumCatEMH.CASIER)) {
                    res.second = Boolean.FALSE;
                    res.first.addSevereError(NbBundle.getMessage(ListCasierModificationProcess.class,
                            "CasierModification.NewCasierNameInvalide", newCasierName,
                            casier.getNom()));
                }
            }

            if (idOfExistingCasier.contains(newCasierName.toUpperCase())) {
                res.second = Boolean.FALSE;
                res.first.addSevereError(NbBundle.getMessage(ListCasierModificationProcess.class,
                        "CasierModification.NewCasierNameAlreadyUsed", newCasierName,
                        casier.getNom()));
            }
            idOfExistingCasier.add(newCasierName.toUpperCase());

            if (!newData.noeudName.equals(casier.getNoeud().getNom())) {
                casierRelationNoeudChanged = true;
                if (!noeudByNoms.containsKey(newData.noeudName)) {
                    res.second = Boolean.FALSE;
                    res.first.addSevereError(NbBundle.getMessage(ListCasierModificationProcess.class,
                            "CasierModification.NewCasierNameAlreadyUsed", newData.noeudName,
                            newCasierName));
                }
            }

            if (newData.noeudName != null && newData.casierName != null) {
                String casierNameForNoeud = CruePrefix.getNomAvecPrefixFor(newData.noeudName, EnumCatEMH.CASIER);
                if (!casierNameForNoeud.equals(newData.casierName)) {
                    res.second = Boolean.FALSE;
                    res.first.addSevereError("AddEMH.CasierNameNotCorrespondingToNoeud", newData.casierName, casierNameForNoeud);
                }
            }
        }
        //impossible de faire le change
        if (res.second.equals(Boolean.FALSE)) {
            return res;
        }

        boolean reorderDone = ReorderEMHProcess.reorder(emhSousModele, casiers, EnumCatEMH.CASIER);
        boolean activeModified = false;
        boolean commentaireModified = false;
        for (CatEMHCasier casier : casiers) {
            CasierData newData = dataByUid.get(casier.getUiId());
            if (newData != null) {
                if (casier.getUserActive() != newData.active) {
                    casier.setUserActive(newData.active);
                    //le changement du caractère actif sera propage par la suite.
                    activeModified = true;
                }
                if (!StringUtils.equals(newData.commentaire, casier.getCommentaire())) {
                    casier.setCommentaire(newData.commentaire);
                    commentaireModified = true;
                }
            }
        }

        //on applique les modification
        if (casierNameChanged) {
            for (CatEMHCasier casier : casiers) {
                CasierData newData = dataByUid.get(casier.getUiId());
                new EditionRename().rename(casier, newData.casierName);
            }
        }
        if (casierRelationNoeudChanged) {
            for (CatEMHCasier casier : casiers) {
                CasierData newData = dataByUid.get(casier.getUiId());
                CatEMHNoeud newNoeud = noeudByNoms.get(newData.noeudName);
                if (casier.getNoeud() != newNoeud) {
                    EditionChangeUtils.setNoeud(casier, newNoeud);
                }
            }
        }
        if (activeModified) {
            EditionChangeActivity change = new EditionChangeActivity();
            CtuluLog propagateChange = change.propagateActivityChanged(emhSousModele.getParent(), casiers, ccm);
            res.first.addAllLogRecord(propagateChange);
        }

        List<EnumModification> modifications = new ArrayList<>();
        if (reorderDone) {
            modifications.add(EnumModification.EMH_ORDER);
        }
        if (activeModified) {
            modifications.add(EnumModification.ACTIVITY);
        }
        if (casierNameChanged) {
            modifications.add(EnumModification.EMH_NAME);
        }
        if (casierRelationNoeudChanged) {
            modifications.add(EnumModification.EMH_RELATION);
        }
        if (commentaireModified) {
            modifications.add(EnumModification.COMMENTAIRE);
        }
        boolean modification = !modifications.isEmpty();
        if (modification && modellingScenarioModificationService != null) {//pour les tests:
            modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(modifications));
        }
        return res;
    }

    public static class CasierData {
        public boolean active;
        public String commentaire;
        public String noeudName;
        public String casierName;
    }
}
