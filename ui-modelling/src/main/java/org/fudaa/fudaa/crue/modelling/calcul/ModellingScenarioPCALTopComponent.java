package org.fudaa.fudaa.crue.modelling.calcul;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.ParamCalcScenario;
import org.fudaa.dodico.crue.metier.emh.Sorties;
import org.fudaa.dodico.crue.validation.ValidationHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import static org.fudaa.fudaa.crue.modelling.calcul.AbstractScenarioTopComponent.restoreTabbedPaneSelection;
import org.fudaa.fudaa.crue.modelling.node.CommonObjectNode;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.nodes.Node;
import org.openide.nodes.Node.Property;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.modelling//ModellingScenarioPCALTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = ModellingScenarioPCALTopComponent.TOPCOMPONENT_ID,
        iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png", persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ModellingScenarioPCALTopComponent.MODE, openAtStartup = false, position = 1)
public final class ModellingScenarioPCALTopComponent extends AbstractScenarioTopComponent {

  public static final String MODE = "modelling-pcal";
  public static final String TOPCOMPONENT_ID = "ModellingScenarioPCALTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;

  public ModellingScenarioPCALTopComponent() {
    initComponents();
    setName(NbBundle.getMessage(ModellingScenarioPCALTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingScenarioPCALTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vuePCAL";
  }

  @Override
  public void setEditable(boolean b) {
    if (btNoDate != null) {
      btNoDate.setEnabled(b);
    }
  }

  @Override
  public void valideModificationHandler() {
    EMHScenario scenario = getScenario();
    ParamCalcScenario pcal = scenario.getParamCalcScenario();
    CtuluLog log = ValidationHelper.validatePCAL(paramCalcScenario, getCcm());
    if (log.isNotEmpty()) {
      LogsDisplayer.displayError(log, getName());
    }
    if (!log.containsErrorOrSevereError()) {
      scenario.removeInfosEMH(pcal);
      scenario.addInfosEMH(paramCalcScenario);
      scenario.getOrdCalcScenario().setSorties(new Sorties(ocalScenarioSortie));
      modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.PCAL, EnumModification.OCAL));//a pour effet de recharger le scenario
      setModified(false);
    }
  }

  /**
   * Listener utilisé sur les composants graphiques pour changer l'état "modifié" de la vue.
   */
  private class ChangeListener implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      setModified(true);
    }
  }
  ChangeListener changeListener;
  ParamCalcScenario paramCalcScenario;
  Sorties ocalScenarioSortie;
  JButton btNoDate;

  @Override
  protected JComponent buildCenterPanel(EMHScenario scenario, JComponent oldCenter) {
    btNoDate = new JButton(NbBundle.getMessage(ModellingScenarioPCALTopComponent.class, "PCAL.DoNotUseDateDeb.Label"));
    changeListener = new ChangeListener();
    paramCalcScenario = scenario.getParamCalcScenario().deepClone();
    ocalScenarioSortie = new Sorties(scenario.getOrdCalcScenario().getSorties());

    final CommonObjectNode node = new CommonObjectNode(paramCalcScenario, getCcm(), perspectiveServiceModelling);
    PropertySheet sheet = new PropertySheet();
    sheet.setNodes(new Node[]{node});
    node.addPropertyChangeListener(changeListener);
    JPanel panel = new JPanel(new BorderLayout());
    JPanel pnButtons = new JPanel(new FlowLayout(FlowLayout.LEFT));
    pnButtons.add(btNoDate);
    panel.add(pnButtons, BorderLayout.NORTH);
    btNoDate.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        Property find = node.find(ParamCalcScenario.PROP_DATE_DEB);
        try {
          find.setValue(null);
        } catch (Exception ex) {
          Exceptions.printStackTrace(ex);
        }
      }
    });
    panel.add(sheet);
    JTabbedPane tabbedPane = new JTabbedPane();
    tabbedPane.addTab(NbBundle.getMessage(ModellingScenarioPCALTopComponent.class, "ModellingScenarioPCALTopComponent.ParamCalc.Tab"), panel);
    SortiesNode sortieNode = new SortiesNode(ocalScenarioSortie, getCcm(), perspectiveServiceModelling);
    sortieNode.addPropertyChangeListener(changeListener);
    PropertySheet sortieSheet = new PropertySheet();
    sortieSheet.setNodes(new Node[]{sortieNode});
    tabbedPane.add(NbBundle.getMessage(ModellingScenarioPCALTopComponent.class, "SortiesNode.DisplayName"), sortieSheet);
    restoreTabbedPaneSelection(oldCenter, tabbedPane);

    return tabbedPane;

  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
  @Override
  public void componentClosedTemporarily() {
  }

  void writeProperties(java.util.Properties p) {
  }

  void readProperties(java.util.Properties p) {
  }
}
