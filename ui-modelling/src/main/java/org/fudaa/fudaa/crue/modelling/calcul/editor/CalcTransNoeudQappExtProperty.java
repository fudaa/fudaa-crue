package org.fudaa.fudaa.crue.modelling.calcul.editor;

import com.memoire.bu.BuGridLayout;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.fudaa.crue.common.editor.CustomEditorAbstract;
import org.fudaa.fudaa.crue.common.helper.ComboBoxHelper;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.swing.DialogPanelDescriptor;
import org.fudaa.fudaa.crue.modelling.calcul.CalculDclmNode;
import org.fudaa.fudaa.crue.views.DclmHydrogramExternEditor;
import org.fudaa.fudaa.crue.views.DclmManoeuvreRegulEditor;
import org.openide.DialogDescriptor;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

import javax.swing.*;
import java.awt.*;
import java.beans.FeatureDescriptor;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorSupport;
import java.util.List;

public class CalcTransNoeudQappExtProperty extends PropertySupportReflection<String> {
  public CalcTransNoeudQappExtProperty(CalculDclmNode node, CalcTransNoeudQappExt instance) throws NoSuchMethodException {
    super(node, instance, CalcTransNoeudQappExt.Data.class, "data");
  }


  public DonLoiHYConteneur getLoiConteneur() {
    return ((CalculDclmNode) super.node).getLoiConteneur();
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    return new QappExtEditorSupport(canWrite(), (CalcTransNoeudQappExt) instance, getLoiConteneur());
  }


  protected static class QappExtEditorSupport extends PropertyEditorSupport implements ExPropertyEditor {

    private PropertyEnv env;

    private final CalcTransNoeudQappExt regul;
    private final boolean canWrite;

    private DonLoiHYConteneur loiConteneur;

    public QappExtEditorSupport(final boolean canWrite, final CalcTransNoeudQappExt regul, DonLoiHYConteneur loiConteneur) {
      this.regul = regul;
      this.canWrite = canWrite;
      this.loiConteneur = loiConteneur;
    }

    @Override
    public String getAsText() {
      Object value = getValue();
      return value == null ? "" : value.toString();
    }

    @Override
    public void setAsText(final String text) throws IllegalArgumentException {
    }

    public Component getCustomEditor() {
      DclmHydrogramExternEditor  editor = new DclmHydrogramExternEditor(regul, loiConteneur);
      DialogPanelDescriptor p = editor.create();
      editor.setEditable(canWrite);
      if (!canWrite) {
        p.getDialogDescriptor().setOptionType(JOptionPane.CLOSED_OPTION);
        p.getDialogDescriptor().setOptions(new Object[]{DialogDescriptor.CLOSED_OPTION});
      } else {
        env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
        env.addPropertyChangeListener(evt -> {
          setValue(new CalcTransNoeudQappExt.Data( editor.getNomFic(),editor.getNomSection(),editor.getNomCalc()));

        });
      }
      return DialogHelper.createDialog(p.getDialogDescriptor(), regul.getEmh().getNom(), env, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    }

    @Override
    public boolean supportsCustomEditor() {
      return true;
    }

    @Override
    public void attachEnv(PropertyEnv env) {
      FeatureDescriptor featureDescriptor = env.getFeatureDescriptor();
      PropertyCrueUtils.configureNoEditAsText(featureDescriptor);
      PropertyCrueUtils.configureCustomEditor(featureDescriptor);
      this.env = env;
    }
  }

}
