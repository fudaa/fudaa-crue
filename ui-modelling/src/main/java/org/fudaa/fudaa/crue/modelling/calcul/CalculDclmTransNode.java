/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.CalcTransItem;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;
import org.fudaa.dodico.crue.metier.emh.DonCLimMCommonItem;
import org.fudaa.dodico.crue.metier.emh.DonLoiHYConteneur;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.util.actions.SystemAction;

/**
 *
 * @author Frederic Deniger
 */
public class CalculDclmTransNode extends CalculDclmNode {

  public CalculDclmTransNode(Children children, DonCLimM dclm, CrueConfigMetier ccm, PerspectiveServiceModelling perspectiveServiceModelling,
                             DonLoiHYConteneur loiConteneur) {
    super(children, dclm, ccm, perspectiveServiceModelling, loiConteneur);
  }

  @Override
  public SystemAction[] getActions() {
    return new SystemAction[]{
      SystemAction.get(CalculOpenLoiNodeAction.class),
      SystemAction.get(CalculRemoveDclmNodeAction.class)
    };

  }

  @Override
  public SystemAction getDefaultAction() {
    return SystemAction.get(CalculOpenLoiNodeAction.class);
  }
}
