package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.fudaa.crue.modelling.calcul.ModellingListCiniTopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public abstract class ModellingOpenListCondInitNodeAction extends AbstractModellingOpenModeleNodeAction {

  public ModellingOpenListCondInitNodeAction(String name) {
    super(name, ModellingListCiniTopComponent.MODE, ModellingListCiniTopComponent.TOPCOMPONENT_ID);
  }

  public static void open(EMHModeleBase modele) {
    if (modele == null) {
      return;
    }
    ModellingOpenListCondInitNodeAction action = SystemAction.get(ModellingOpenListCondInitNodeAction.Reopen.class);
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(modele));
    action.performAction(new Node[]{node});
  }

  public static class Reopen extends ModellingOpenListCondInitNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenListCondInitNodeAction.class, "ModellingListCiniTopComponent.Name"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenListCondInitNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenListCondInitNodeAction.class, "ModellingListCiniTopComponent.NewFrame.Name"));
      setReopen(false);
    }
  }
}
