package org.fudaa.fudaa.crue.modelling.list;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorSupport;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent.SectionData;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertySupportReadWrite;
import org.openide.nodes.Node;
import org.openide.nodes.Node.Property;
import org.openide.nodes.Node.PropertySet;
import org.openide.util.Exceptions;

/**
 * La liste des tags doit contenir toutes section qui ne sont pas affectées.
 *
 * @author deniger
 */
public class PropertySupportSection extends PropertySupportReadWrite<ListRelationSectionContent, String> {

//  private List<String> allNodes;
  public PropertySupportSection(final AbstractNodeFirable node, final ListRelationSectionContent instance) {
    super(node, instance, String.class, ListCommonProperties.PROP_NOM, ListRelationSectionContentNode.getSectionNameDisplay(), null);
    PropertyCrueUtils.configureNoCustomEditor(this);
    setCanWrite(StringUtils.isNotBlank(getInstance().getBrancheNom()));
    node.addPropertyChangeListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(final PropertyChangeEvent evt) {
        if (ListRelationSectionContent.PROP_BRANCHE_NOM.equals(evt.getPropertyName())) {
          setCanWrite(StringUtils.isNotBlank(getInstance().getBrancheNom()));
        }
      }
    });
  }

  private Node find(final String name) {
    final Node[] nodes = node.getParentNode().getChildren().getNodes();
    for (final Node sectionNode : nodes) {
      final ListRelationSectionContent sectionContent = sectionNode.getLookup().lookup(ListRelationSectionContent.class);
      if (StringUtils.equals(name, sectionContent.getNom())) {
        return sectionNode;
      }
    }
    return null;
  }

  @Override
  protected void setValueInInstance(final String newVal) {
    final String oldValue = getInstance().getNom();
    if (!StringUtils.equals(oldValue, newVal)) {
      if (StringUtils.isBlank(newVal)) {
        getInstance().setBrancheNom(StringUtils.EMPTY);
        getInstance().setNom(oldValue);
        super.node.setDisplayName(StringUtils.EMPTY);
        fireObjectChange(ListRelationSectionContent.PROP_BRANCHE_NOM, Boolean.FALSE, Boolean.TRUE);

      } else {
        swapRelationSection(newVal, oldValue);
      }
    }
  }

  private void updateData(final String newVal, final SectionData sectionData) {
    getInstance().setNom(newVal);
    getInstance().setSection(sectionData);
    fireSectionEvents();
  }

  public Node.Property getPropertySet(final Node toChange) {
    final PropertySet set = toChange.getPropertySets()[0];
    final Property<?>[] properties = set.getProperties();
    for (final Node.Property property : properties) {
      if (property.getName().equals(getName())) {
        return property;
      }
    }
    return null;
  }

  protected void fireSectionEvents() {
    final List<String> props = ListRelationSectionContent.SECTION_DATA_PROPERTIES;
    for (final String prop : props) {
      fireObjectChange(prop, Boolean.FALSE, Boolean.TRUE);//evt bidon pour faire redessiner le tableau

    }
  }

  @Override
  public String getValue() throws IllegalAccessException, InvocationTargetException {
    return getInstance().getNom();
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    return new SectionPropertyEditor();


  }

  public void swapRelationSection(final String newVal, final String oldValue) {
    final Node toChange = find(newVal);
    if (toChange == null) {
      return;
    }
    final ListRelationSectionContent sectionContent = toChange.getLookup().lookup(ListRelationSectionContent.class);
    final SectionData oldSectionData = getInstance().getSection();
    updateData(newVal, sectionContent.getSection());
    fireSectionEvents();
    //on inverse
    final Property propertySet = getPropertySet(toChange);
    try {
      ((PropertySupportSection) propertySet).updateData(oldValue, oldSectionData);
    } catch (final Exception exception) {
      Exceptions.printStackTrace(exception);
    }
  }

  private class SectionPropertyEditor extends PropertyEditorSupport {

    public SectionPropertyEditor() {
    }

    @Override
    public String getAsText() {
      return (String) getValue();
    }

    @Override
    public void setAsText(final String string) {
      setValue(string);
    }
    String[] sectionNames;

    @Override
    public String[] getTags() {
      if (!canWrite()) {
        return null;
      }
      final List<String> sectionNames = new ArrayList<>();
      final Node[] nodes = node.getParentNode().getChildren().getNodes();
      final String thisBrancheName = getInstance().getBrancheNom();
      int count = 0;
      for (final Node sectionNode : nodes) {
        final ListRelationSectionContent content = sectionNode.getLookup().lookup(ListRelationSectionContent.class);
        if (content == null) {
          continue;
        }
        if (content == getInstance() || StringUtils.isBlank(content.getBrancheNom())) {
          sectionNames.add(content.getNom());
        }
        if (StringUtils.isNotBlank(thisBrancheName) && thisBrancheName.equals(content.getBrancheNom())) {
          count++;
        }
      }
      if (count >= 3) {
        sectionNames.add(0, StringUtils.EMPTY);
      }
      return sectionNames.toArray(new String[0]);
    }
  }
}
