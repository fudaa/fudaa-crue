package org.fudaa.fudaa.crue.modelling.list;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.common.view.MoveNodesToEndNodeAction;
import org.fudaa.fudaa.crue.common.view.MoveNodesToStartNodeAction;
import org.fudaa.fudaa.crue.modelling.node.AbstractModellingNodeFirable;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.PasteType;

import javax.swing.*;
import java.awt.datatransfer.Transferable;
import java.io.IOException;
import java.util.Collection;

/**
 * @author deniger
 */
public class AbstractListContentNode extends AbstractModellingNodeFirable {
  public AbstractListContentNode(final Children children, final Lookup lookup, final PerspectiveServiceModelling perspectiveServiceModelling) {
    super(children, lookup, perspectiveServiceModelling);
    DefaultNodePasteType.setIndexUsedAsDisplayName(this);
  }

  public static String getOrdreDisplay() {
    return NbBundle.getMessage(AbstractListContentNode.class, "Ordre.Name");
  }

  public static String getEMHDisplay() {
    return NbBundle.getMessage(AbstractListContentNode.class, "EMH.Name");
  }

  public static String getTypeDisplay() {
    return NbBundle.getMessage(AbstractListContentNode.class, "Type.Name");
  }

  public static String getTypeEMHDisplay() {
    return NbBundle.getMessage(AbstractListContentNode.class, "TypeEMH.Name");
  }

  public static String getActiveDisplay() {
    return NbBundle.getMessage(AbstractListContentNode.class, "Active.Name");
  }

  public static String getCommentDisplay() {
    return NbBundle.getMessage(AbstractListContentNode.class, "Commentaire.Name");
  }

  public static void reinitMessages(final Collection<? extends AbstractListContentNode> values) {
    //on réinitialise
    for (final AbstractListContentNode node : values) {
      node.reinitErrorMessages();
    }
  }

  private void reinitErrorMessages() {
    setContainError(false);
    setShortDescription(StringUtils.EMPTY);
  }

  public void setContainError(final boolean error) {
    if (error) {
      setSevereIcon();
    } else {
      setNoImage();
    }
  }

  @Override
  public Action[] getActions(final boolean context) {
    return new Action[]{
        SystemAction.get(MoveNodesToStartNodeAction.class),
        SystemAction.get(MoveNodesToEndNodeAction.class)
    };
  }

  @Override
  public final boolean canCopy() {
    return isEditMode();
  }

  @Override
  public PasteType getDropType(final Transferable t, final int action, final int index) {
    if (isEditMode()) {
      final Node dragged = DefaultNodePasteType.getMovedNodeInTransferable(t);
      if (dragged != null) {
        return new DefaultNodePasteType(dragged, this);
      }
    }
    return null;
  }

  @Override
  public void destroy() throws IOException {
    getParentNode().getChildren().remove(new Node[]{this});
  }

  @Override
  public void setDisplayName(final String s) {
    super.setDisplayName(s);
  }
}
