/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.OrdCalc;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.openide.nodes.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public class CalculDuplicateNodeAction extends AbstractEditNodeAction {
  public CalculDuplicateNodeAction() {
    super(org.openide.util.NbBundle.getMessage(CalculDuplicateNodeAction.class, "Duplicate.ActionName"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return activatedNodes.length == 1;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    Node node = activatedNodes[0];
    CalculNode targetNode = (CalculNode) node;
    Calc calc = targetNode.getCalc().deepClone();
    calc.setUiId(null);
    List<String> nodesName = new ArrayList<>();
    for (Node otherNode : targetNode.getParentNode().getChildren().getNodes()) {
      nodesName.add(otherNode.getName());
    }
    UniqueNomFinder nameFinder = new UniqueNomFinder();
    String uniqueName = nameFinder.findUniqueName(nodesName, calc.getNom(), 2);
    final String prefix = calc.isPermanent() ? CruePrefix.P_CALCUL_PSEUDOPERMANENT : CruePrefix.P_CALCUL_TRANSITOIRE;
    if (!uniqueName.startsWith(prefix)) {
      uniqueName = CruePrefix.changePrefix(uniqueName, CruePrefix.P_CALCUL, prefix);
      uniqueName= nameFinder.findUniqueName(nodesName, uniqueName, 2);
    }
    calc.setNom(uniqueName);
    CalculNodeFactory fac = new CalculNodeFactory(targetNode.outlineView);
    CalculOcalNode ocalNode = targetNode.getOcalNode();
    OrdCalc ordCalc;
    if (ocalNode != null) {
      ordCalc = ocalNode.getOrdCalc();
    } else {
      ordCalc = targetNode.getOldUsedOrdCalc();
    }
    final CalculNode newCalc = fac.addNewCalc(calc);
    //permet de conserver l'ordre de calcul dans le dupliquat.
    if (newCalc != null && ordCalc != null) {
      newCalc.setOldUsedOrdCalc(ordCalc.deepCloneButNotCalc(calc));
    }
  }
}
