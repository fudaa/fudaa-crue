package org.fudaa.fudaa.crue.modelling.calcul.importer;

import com.jidesoft.swing.JideScrollPane;
import com.jidesoft.swing.SearchableUtils;
import com.memoire.bu.BuBorders;
import com.memoire.bu.BuVerticalLayout;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.ResultatPasDeTempsParCalcul;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.etude.ScenarioRunContainer;
import org.fudaa.fudaa.crue.loader.CreateDPTIFromResultProcess;
import org.fudaa.fudaa.crue.loader.LoaderService;
import org.fudaa.fudaa.crue.loader.ProjectLoadContainer;
import org.openide.DialogDescriptor;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.util.List;

/**
 * @author deniger
 */
public class RunCalculChooserDialogPanel extends CtuluDialogPanel {
  private final JButton butttonPreview;
  private final DefaultComboBoxModel calculModel = new DefaultComboBoxModel();
  private final CtuluFileChooserPanel chooseEtu;
  private transient EMHProjet currentProject;
  private transient EMHScenario currentRun;
  private transient DialogDescriptor dialogDescriptor;
  private boolean isLoading;
  private final JLabel labelChooseCalcul;
  private final JLabel labelChooseRun;
  private final JList<ResultatTimeKey> listCalcul = new JList();
  private final JList<ScenarioRunContainer> listScenarioRun = new JList();
  private final LoaderService loaderService = Lookup.getDefault().lookup(LoaderService.class);
  private final DefaultComboBoxModel runScenarioModel = new DefaultComboBoxModel();

  /**
   */
  protected RunCalculChooserDialogPanel() {
    setBorder(BuBorders.EMPTY5555);
    final JPanel panelEtuRun = new JPanel(new BuVerticalLayout(5, true, true));
    chooseEtu = addFileChooserPanel(panelEtuRun, NbBundle.getMessage(RunCalculChooser.class, "RunCalculChooser.ChooseEtuLabel"), false, false);
    chooseEtu.setAllFileFilter(false);
    chooseEtu.setFilter(new FileFilter[]{LoaderService.createEtuFileFilter()});
    chooseEtu.getTf().setEditable(false);
    chooseEtu.getTf().getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void insertUpdate(final DocumentEvent e) {
        loadEtude();
      }

      @Override
      public void removeUpdate(final DocumentEvent e) {
        loadEtude();
      }

      @Override
      public void changedUpdate(final DocumentEvent e) {
        loadEtude();
      }
    });

    //choix des runs
    labelChooseRun = new JLabel(getScenarioChooserLabel());
    panelEtuRun.add(labelChooseRun);
    SearchableUtils.installSearchable(listScenarioRun);
    final JideScrollPane jideScrollPane = new JideScrollPane(listScenarioRun);
    panelEtuRun.add(jideScrollPane);
    listScenarioRun.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    listScenarioRun.addListSelectionListener(e -> loadCalcul(e));
    listScenarioRun.setModel(runScenarioModel);

    final JPanel panelCalcul = new JPanel(new BuVerticalLayout(5, true, true));
    //choix des calculs
    labelChooseCalcul = new JLabel(getCalculLabel());
    panelCalcul.add(labelChooseCalcul);
    SearchableUtils.installSearchable(listCalcul);
    panelCalcul.add(new JideScrollPane(listCalcul));
    listCalcul.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    listCalcul.setCellRenderer(new CtuluCellTextRenderer() {
      @Override
      protected void setValue(final Object value) {
        if (value instanceof ResultatTimeKey) {
          super.setValue(((ResultatTimeKey) value).getNomCalcul());
        } else {
          super.setValue(value);
        }
      }
    });
    listCalcul.addListSelectionListener(e -> updateValidState());
    listCalcul.setModel(calculModel);

    butttonPreview = new JButton(NbBundle.getMessage(RunCalculChooserDialogPanel.class, "EtudeRunDialog.PreviewButton"));
    panelCalcul.add(butttonPreview);
    butttonPreview.setEnabled(false);
    butttonPreview
        .addActionListener(e -> CreateDPTIFromResultProcess.displayPreview(this.currentRun, (ResultatTimeKey) calculModel.getSelectedItem(), currentProject.getPropDefinition()));

    setLayout(new BorderLayout(0, 5));
    add(panelEtuRun, BorderLayout.CENTER);
    add(panelCalcul, BorderLayout.SOUTH);
  }

  public Pair<EMHScenario, ResultatTimeKey> getValues() {
    return new Pair<>(currentRun, listCalcul.getSelectedValue());
  }

  private String getScenarioChooserLabel() {
    return NbBundle.getMessage(RunCalculChooser.class, "RunCalculChooser.ChooseRunLabel");
  }

  private String getCalculLabel() {
    return NbBundle.getMessage(RunCalculChooser.class, "RunCalculChooser.ChooseCalculLabel");
  }

  private String getLoadingLabel() {
    return NbBundle.getMessage(RunCalculChooser.class, "RunCalculChooser.Loading");
  }

  public void setDialogDescriptor(final DialogDescriptor dialogDescriptor) {
    this.dialogDescriptor = dialogDescriptor;
  }

  private void updateValidState() {
    final boolean valid = StringUtils.isNotBlank(chooseEtu.getTf().getText()) && isRunSelectionCorrect() && listCalcul.getSelectedValue() != null;
    butttonPreview.setEnabled(valid);
    dialogDescriptor.setValid(valid);
  }

  private boolean isRunSelectionCorrect() {
    return listScenarioRun.getSelectedValue() instanceof ScenarioRunContainer;
  }

  private void loadEtude() {
    isLoading = true;
    labelChooseRun.setText(getLoadingLabel());
    runScenarioModel.removeAllElements();
    calculModel.removeAllElements();
    final ProjectLoadContainer loadProject = loaderService.loadProject(chooseEtu.getFile());
    if (loadProject != null && loadProject.getProjet() != null) {
      currentProject = loadProject.getProjet();
      final java.util.List<ManagerEMHScenario> listeScenarios = loadProject.getProjet().getListeScenarios();
      for (final ManagerEMHScenario emhScenario : listeScenarios) {
        if (emhScenario.isCrue10() && emhScenario.isActive() && emhScenario.hasRun()) {
          emhScenario.getListeRuns().forEach(emhRun -> runScenarioModel.addElement(new ScenarioRunContainer(emhRun, emhScenario)));
        }
      }
    }
    if (runScenarioModel.getSize() == 0) {
      runScenarioModel.addElement(NbBundle.getMessage(RunCalculChooser.class, "RunCalculChooser.NoRun"));
    } else {
      //pour eviter d'avoir une selection par default
      runScenarioModel.insertElementAt("", 0);
    }
    listScenarioRun.setSelectedIndex(0);
    labelChooseRun.setText(getScenarioChooserLabel());
    updateValidState();
    isLoading = false;
  }

  private void loadCalcul(final ListSelectionEvent e) {
    if (isLoading || e.getValueIsAdjusting()) {
      return;
    }
    calculModel.removeAllElements();
    labelChooseCalcul.setText(getLoadingLabel());
    if (isRunSelectionCorrect()) {
      currentRun = loaderService.load(currentProject, listScenarioRun.getSelectedValue().scenario, listScenarioRun.getSelectedValue().run, true);
      if (currentRun != null && currentRun.getModeles().get(0).getResultatCalculPasDeTemps() != null) {
        final List<ResultatPasDeTempsParCalcul> resultatPasDeTempsParCalculList = currentRun.getModeles().get(0).getResultatCalculPasDeTemps().getResultatPasDeTempsParCalcul();
        resultatPasDeTempsParCalculList.forEach(resultatPasDeTempsParCalcul -> {
          resultatPasDeTempsParCalcul.getResultats().forEach(resultatTimeKey -> {
            if (resultatTimeKey.isPermanent()) {
              calculModel.addElement(resultatTimeKey);
            }
          });
        });
      }
    }

    if (calculModel.getSize() == 0) {
      calculModel.addElement(NbBundle.getMessage(RunCalculChooser.class, "RunCalculChooser.NoCalcul"));
    }
    labelChooseCalcul.setText(getCalculLabel());
    updateValidState();
  }
}
