package org.fudaa.fudaa.crue.modelling.list;

import gnu.trove.TLongDoubleHashMap;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.AlgoEMHSectionProfilEMHSectionIdemInverser;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.modelling.ModellingTopComponentWithSpecificAction;
import org.fudaa.fudaa.crue.modelling.action.AddSectionInBranchePanelContentData;
import org.fudaa.fudaa.crue.modelling.action.ModellingOpenInsertBrancheNodeAction;
import org.fudaa.fudaa.crue.modelling.action.ModellingOpenListSectionAddNodeAction;
import org.fudaa.fudaa.crue.modelling.action.ModellingOpenSplitBrancheNodeAction;
import org.fudaa.fudaa.crue.modelling.edition.ListRelationSectionProcess;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioModificationService;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioVisuService;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Edition des listes de section
 */
@TopComponent.Description(preferredID = ModellingListSectionTopComponent.TOPCOMPONENT_ID,
    iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png",
    persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ModellingListSectionTopComponent.MODE, openAtStartup = false, position = 0)
public final class ModellingListSectionTopComponent extends AbstractModellingListEditionTopComponent implements
    ModellingTopComponentWithSpecificAction {
  //attention le mode modelling-listSections doit être déclaré dans le projet branding (layer.xml)
  public static final String MODE = "modelling-listSections";
  public static final String TOPCOMPONENT_ID = "ModellingListSectionTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  private static final String TOUTES = "Toutes";
  private JButton btAddSections;
  private JButton btAddSectionInBranche;
  // cb de filtre sur les branches
  private JComboBox cbbBranches;
  // Nodes cachés par le filtre
  private List<ListRelationSectionContentNode> hideNodes;
  /**
   * appele uniquement si les données de geoloc sont modifiés.
   */
  private final PropertyChangeListener geoLocListener = evt -> scenarioReloaded();
  // filtre pouvant être fixé par une fenêtre appelante
  private String brancheFilter;

  public ModellingListSectionTopComponent() {
    initComponents();
    setName(NbBundle.getMessage(ModellingListSectionTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingListSectionTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    createComponent(StringUtils.EMPTY);
  }

  @Override
  public void setSousModeleUid(final Long sousModeleUid) {
    // Override du positionnement du sous-modèle pour rechargement du combobox de filtre des branches
    super.setSousModeleUid(sousModeleUid);
    // remplaçage du filtre sur les branches en fonction du sous-modèle sélectionné
    fillBranchesFilter();
  }

  /**
   * Fixe le filtre "Branche" de la fenêtre
   *
   * @param filter le nom de la branche pour le filtre
   */
  public void setBrancheFilter(final String filter) {
    brancheFilter = filter;
    cbbBranches.setSelectedItem(filter);
    scenarioReloaded();
  }

  @Override
  protected void readPreferences() {
    DialogHelper.readInPreferences(outlineView, "outlineView", getClass(), "2.0");
  }

  @Override
  protected void writePreferences() {
    DialogHelper.writeInPreferences(outlineView, "outlineView", getClass(), "2.0");
  }

  @Override
  protected void setEditable(final boolean b) {
    super.setEditable(b);
    btAddSectionInBranche.setEnabled(b);
    // Filtre sur les branches éditable si pas de filtre sur la fenêtre et si pas de modif en cours
    cbbBranches.setEnabled(StringUtils.isEmpty(brancheFilter));
  }

  @Override
  public void setModified(final boolean modified) {
    super.setModified(modified);
    // filtre branches enable si pas de modif en cours et pas de filtre sur la fenêtre
    cbbBranches.setEnabled(!modified && StringUtils.isEmpty(brancheFilter));
  }

  private JButton createAdditionnalAddButton() {
    btAddSectionInBranche = new JButton(NbBundle.getMessage(ModellingListSectionAddTopComponent.class, "AddSectionsInBanche.ActionName"));
    btAddSectionInBranche.addActionListener(e -> addSectionsInBranche());
    return btAddSectionInBranche;
  }

  private void addSectionsInBranche() {
    final AddSectionInBranchePanelContent builder = new AddSectionInBranchePanelContent(getCcm(), getSousModele());
    builder.setSelectedBranche(getCurrentSelectedBrancheName());
    if (builder.show() && builder.getDistance() != null && builder.getSelectedBranche() != null) {
      final CatEMHBranche branche = builder.getSelectedBranche();
      final double maxDistance = builder.getDistance();
      final EnumSectionType typeSection = builder.getSelectedSectionType();
      openAddNodeTopComponent(new AddSectionInBranchePanelContentData(branche, maxDistance, typeSection));
    }
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueListeSections";
  }

  @Override
  protected void componentOpenedHandler() {
    modellingScenarioModificationService
        .addPropertyListener(ModellingScenarioModificationService.PROPERTY_GEOLOC_MODIFIED, geoLocListener);
  }

  @Override
  protected void componentClosedDefinitlyHandler() {
    modellingScenarioModificationService.removePropertyListener(ModellingScenarioModificationService.PROPERTY_GEOLOC_MODIFIED, geoLocListener);
    // Suppression du filtre à la fermeture de la fenêtre
    brancheFilter = null;
  }

  @Override
  public void valideModificationHandler() {
    // Lecture des nodes de la fenêtre
    final Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();

    // Construction d'une liste de nodes, union des nodes de la fenêtres
    // et des nodes cachés par le filtre sur les branches
    final List<Node> allNodes = new ArrayList<>();
    allNodes.addAll(Arrays.asList(nodes));
    allNodes.addAll(hideNodes);

    final CtuluLog res = new ListRelationSectionProcess().processModification(allNodes.toArray(new Node[0]), getSousModele(), getCcm());
    //la modification a été effectuée
    if (!res.containsErrorOrSevereError()) {
      setModified(false);
    }
    if (res.isNotEmpty()) {
      LogsDisplayer.displayError(res, getDisplayName());
    }
  }

  @Override
  protected JPanel buildNorthPanel() {
    assert btAddSections == null;//c'est normalement appele que dans le constructeur:
    btAddSections = new JButton(ModellingOpenListSectionAddNodeAction.getAddSectionActionName());
    btAddSections.addActionListener(e -> openAddNodeTopComponent(null));
    final JPanel top = super.buildNorthPanel();
    final JPanel pnlSouth = new JPanel(new BorderLayout());
    final JPanel bt = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    pnlSouth.add(bt, BorderLayout.EAST);

    // création du bloc de filtre sur les branches
    final JLabel lbl = new JLabel(NbBundle.getMessage(ModellingListSectionTopComponent.class, "Filtre.Label"));
    cbbBranches = new JComboBox();
    top.add(lbl);
    final JPanel pnlCbb = new JPanel(new FlowLayout(FlowLayout.LEFT));
    top.add(pnlCbb);
    pnlCbb.add(cbbBranches);
    cbbBranches.addActionListener(e -> {
      scenarioReloaded();
      // on ne peut ajouter des sections que si pas de filtre
      btAddSections.setEnabled(cbbBranches.getSelectedIndex() == 0);
    });

    createAdditionnalAddButton();
    bt.add(btAddSections);
    bt.add(btAddSectionInBranche);
    final JPanel res = new JPanel(new BorderLayout());
    res.add(top);
    res.add(pnlSouth, BorderLayout.SOUTH);
    return res;
  }

  private void openAddNodeTopComponent(final AddSectionInBranchePanelContentData data) {
    final Runnable runnable = () -> ModellingOpenListSectionAddNodeAction.open(getSousModele(), data);
    EventQueue.invokeLater(runnable);
  }

  @Override
  protected void installPropertyColumns() {
    outlineView.setPropertyColumns(
        ListRelationSectionContent.PROP_BRANCHE_NOM,
        ListRelationSectionContentNode.getBrancheDisplay(),
        ListCommonProperties.PROP_NOM,
        ListRelationSectionContentNode.getSectionNameDisplay(),
        ListRelationSectionContent.PROP_SECTION_TYPE_NOM,
        ListRelationSectionContentNode.getSectionTypeDisplay(),
        ListRelationSectionContent.PROP_REFERENCE_SECTION_IDEM,
        ListRelationSectionContentNode.getReferenceDisplay(),
        ListRelationSectionContent.PROP_DZ,
        ListRelationSectionContentNode.getDzDisplay(),
        ListRelationSectionContent.PROP_PROFIL_SECTION,
        ListRelationSectionContentNode.getProfilDisplay(),
        ListRelationSectionContent.PROP_ABSCISSE_HYDRAULIQUE,
        ListRelationSectionContentNode.getAbsicsseHydrauliqueDisplay(),
        ListRelationSectionContent.PROP_ABSCISSE_SCHEMATIQUE,
        ListRelationSectionContentNode.getAbscisseSchematiqueDisplay(),
        ListCommonProperties.PROP_COMMENTAIRE,
        AbstractListContentNode.getCommentDisplay());
  }

  @Override
  protected void createComponent(final String firstColumnName) {
    super.createComponent(firstColumnName);
    outlineView.setDragSource(false);
    outlineView.setDropTarget(false);
  }

  @Override
  protected List<? extends Node> createNodes(final EMH in) {
    final ModellingScenarioVisuService modellingScenarioVisuService = Lookup.getDefault().lookup(ModellingScenarioVisuService.class);
    TLongDoubleHashMap distanceByBrancheUid = null;
    if (modellingScenarioVisuService.getPlanimetryVisuPanel() != null) {
      distanceByBrancheUid = modellingScenarioVisuService.getPlanimetryVisuPanel().getPlanimetryController().getDistanceByBrancheUid();
    }
    final ListRelationSectionContentFactoryNode factory = new ListRelationSectionContentFactoryNode();

    final List<ListRelationSectionContentNode> displayNodes = new ArrayList<>();
    hideNodes = new ArrayList<>();
    final List<ListRelationSectionContentNode> nodes = factory.createNodes((EMHSousModele) in, distanceByBrancheUid);
    // application du filtre sur les branches
    // on mémorise les nodes cachés dans une liste à part
    for (final ListRelationSectionContentNode node : nodes) {
      if (cbbBranches.getSelectedItem() == null || cbbBranches.getSelectedItem().equals(TOUTES) || node.getSection().getBranche() == null || node
          .getSection().getBranche().getNom().equals(cbbBranches.getSelectedItem())) {
        displayNodes.add(node);
      } else {
        hideNodes.add(node);
      }
    }

    return displayNodes;
  }

  @Override
  protected void scenarioReloaded() {
    scenarioLoaded();
  }

  /**
   * Remplissage de cbbBranches avec les branches du sous-modèles courant
   */
  private void fillBranchesFilter() {
    cbbBranches.removeAllItems();
    cbbBranches.addItem(TOUTES);
    final List<CatEMHBranche> branches = getSousModele().getBranches();
    for (final CatEMHBranche branche : branches) {
      cbbBranches.addItem(branche.getNom());
    }
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables

  private String getCurrentSelectedBrancheName() {
    String selectedBranche = null;
    final Node[] selectedNodes = getExplorerManager().getSelectedNodes();

    if (selectedNodes != null && selectedNodes.length >= 1) {
      final CatEMHSection section
          = selectedNodes[0].getLookup().lookup(CatEMHSection.class
      );
      if (section != null) {
        final CatEMHBranche branche = section.getBranche();
        if (branche != null) {
          selectedBranche = branche.getNom();
        }
      }
    }
    return selectedBranche;
  }

  @Override
  public void addCustomAction(final Node selectedNode, final JPopupMenu popupMenu) {
    final JMenuItem add = popupMenu.add(NbBundle.getMessage(ModellingListSectionTopComponent.class, "listSection.SortActionName"));
    add.setEnabled(isEditable());
    add.addActionListener(e -> new ListRelationSectionContentFactoryNode().sort(getExplorerManager().getRootContext(), true));
    if (selectedNode != null) {
      final EMH emh = selectedNode.getLookup().lookup(EMH.class);
      final JMenuItem inverseIdem = popupMenu.add(NbBundle.getMessage(ModellingListSectionTopComponent.class, "listSection.inverseIdemAndProfil"));
      inverseIdem.setEnabled(isEditable() && !isModified() && emh instanceof EMHSectionIdem && ((EMHSectionIdem) emh).getSectionRef() != null);
      inverseIdem.addActionListener(e -> inverseSectionIdem(emh));
      final ModellingOpenSplitBrancheNodeAction openSplitBrancheNodeAction = SystemAction.get(ModellingOpenSplitBrancheNodeAction.class);
      openSplitBrancheNodeAction.setEnabled(openSplitBrancheNodeAction.isEnable(new Node[]{selectedNode}));
      final ModellingOpenInsertBrancheNodeAction openInsertBrancheNodeAction = SystemAction.get(ModellingOpenInsertBrancheNodeAction.class);
      openInsertBrancheNodeAction.setEnabled(openInsertBrancheNodeAction.isEnable(new Node[]{selectedNode}));
      popupMenu.add(openSplitBrancheNodeAction);
      popupMenu.add(openInsertBrancheNodeAction);
    }
  }

  private void inverseSectionIdem(final EMH emh) {
    final EMHSectionIdem emhSectionIdem = (EMHSectionIdem) emh;
    final CatEMHSection other = emhSectionIdem.getSectionRef();

    //les branches
    final String brancheSectionIdem = emhSectionIdem.getBranche() == null ? "" : emhSectionIdem.getBranche().getNom();
    final String brancheSectionOther = other.getBranche() == null ? "" : other.getBranche().getNom();

    //les xps
    final ItemVariable itemVariable = getCcm().getProperty(CrueConfigMetierConstants.PROP_DZ);
    final String xpSectionIdem = itemVariable.format(EMHRelationFactory.getXP(emhSectionIdem), DecimalFormatEpsilonEnum.PRESENTATION);
    final String xpSectionOther = itemVariable.format(EMHRelationFactory.getXP(other), DecimalFormatEpsilonEnum.PRESENTATION);

    final CtuluHtmlWriter writer = new CtuluHtmlWriter(true);
    writer.addi18n(NbBundle.getMessage(ModellingListSectionTopComponent.class, "listSection.inverseIdemAndProfil.message"));
    writer.addi18n(NbBundle.getMessage(ModellingListSectionTopComponent.class, "listSection.inverseIdemAndProfil.Copie.message", other.getNom()));
    writer.addTag("br").close();
    final CtuluHtmlWriter.Tag table = writer.addStyledTag("table", "border=1;cellpadding=1;cellspacing=1", "border=1");
    CtuluHtmlWriter.Tag tr = writer.addTag("tr");
    writer.addTagAndText("td", "<b>" + NbBundle.getMessage(ModellingListSectionTopComponent.class, "listSection.inverseIdemAndProfil.section") + "</b>");
    writer.addTagAndText("td",
        "<b>" + NbBundle.getMessage(ModellingListSectionTopComponent.class, "listSection.inverseIdemAndProfil.currentBranche") + "</b>");
    writer.addTagAndText("td", "<b>" + NbBundle.getMessage(ModellingListSectionTopComponent.class, "listSection.inverseIdemAndProfil.currentXP") + "</b>");
    writer.addTagAndText("td",
        "<b>" + NbBundle.getMessage(ModellingListSectionTopComponent.class, "listSection.inverseIdemAndProfil.futureBranche") + "</b>");
    writer.addTagAndText("td", "<b>" + NbBundle.getMessage(ModellingListSectionTopComponent.class, "listSection.inverseIdemAndProfil.futureXP") + "</b>");
    tr.close();

    tr = writer.addTag("tr");
    writer.addTagAndText("td", emhSectionIdem.getTypei18n() + ": " + emhSectionIdem.getNom());
    writer.addTagAndText("td", brancheSectionIdem);
    writer.addTagAndText("td", xpSectionIdem);
    writer.addTagAndText("td", brancheSectionOther);
    writer.addTagAndText("td", xpSectionOther);
    tr.close();
    tr = writer.addTag("tr");
    writer.addTagAndText("td", other.getTypei18n() + ": " + other.getNom() + "_Copie");
    writer.addTagAndText("td", brancheSectionOther);
    writer.addTagAndText("td", xpSectionOther);
    writer.addTagAndText("td", brancheSectionIdem);
    writer.addTagAndText("td", xpSectionIdem);
    tr.close();
    table.close();
    final boolean accepted = DialogHelper
        .showQuestion(NbBundle.getMessage(ModellingListSectionTopComponent.class, "listSection.inverseIdemAndProfil.title", emhSectionIdem.getNom(),
            other.getNom()), writer.getHtml());
    if (accepted) {
      final CtuluLog log = new AlgoEMHSectionProfilEMHSectionIdemInverser(getCcm()).inverseSectionIdem(emhSectionIdem);
      if (log.containsErrorOrSevereError()) {
        LogsDisplayer.displayError(log, NbBundle.getMessage(ModellingListSectionTopComponent.class, "listSection.inverseIdemAndProfil"));
      } else {
        modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.ALL));
        perspectiveServiceModelling.setDirty(modellingScenarioModificationService.isModified());
        scenarioReloaded();
      }
    }
  }

  @Override
  public boolean useQuickFilterActions(final int columnIdx) {
    return columnIdx != 0;
  }
}
