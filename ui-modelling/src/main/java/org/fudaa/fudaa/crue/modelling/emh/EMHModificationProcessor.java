/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.emh;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.EditionChangeActivity;
import org.fudaa.dodico.crue.metier.emh.CatEMHActivable;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;

/**
 *
 * @author Frédéric Deniger
 */
public class EMHModificationProcessor {

  public CtuluLog changeActivity(CatEMHActivable emh, EMHModeleBase modele, CrueConfigMetier ccm, boolean newValue) {
    if (emh.getUserActive() == newValue) {
      return null;
    }
    emh.setUserActive(newValue);
    EditionChangeActivity change = new EditionChangeActivity();
    return change.propagateActivityChanged(modele, emh, ccm);
  }
}
