/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.list;

import gnu.trove.TIntArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;

/**
 *
 * @author Frederic Deniger
 */
public class ModellingListFrottementLineTableModel extends AbstractTableModel {

  private final List<ModellingListFrottementLine> lines;
  private final List<String> header;
  private final ModellingListFrottementTopComponent topComponent;

  public ModellingListFrottementLineTableModel(List<ModellingListFrottementLine> lines, List<String> header,
          final ModellingListFrottementTopComponent topComponent) {
    this.lines = lines == null ? Collections.emptyList() : lines;
    this.header = header == null ? Collections.emptyList() : header;
    this.topComponent = topComponent;
  }

  public int getMaxColumns(int column) {
    int res = 0;
    for (int i = 0; i < getRowCount(); i++) {
      ModellingListFrottementLine.LitNommeCell valueAt = getValueAt(i, column);
      if (valueAt != null) {
        res = Math.max(res, valueAt.getLitNumerotes().size());
      }
    }
    return res;
  }

  public ModellingListFrottementLine getLine(int row) {
    return lines.get(row);
  }

  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return columnIndex % 2 == 1 && topComponent.isEditable();
  }

  @Override
  public int getRowCount() {
    return lines.size();
  }

  @Override
  public int getColumnCount() {
    return header.size() * 2 - 1;
  }

  @Override
  public String getColumnName(int column) {
    if (column % 2 == 1) {
      return StringUtils.EMPTY;
    }
    return header.get(column / 2);
  }

  @Override
  public ModellingListFrottementLine.LitNommeCell getValueAt(int rowIndex, int columnIndex) {
    if (columnIndex % 2 == 0) {
      return null;
    }
    return lines.get(rowIndex).getLitNommes().get((columnIndex - 1) / 2);
  }

  boolean applyModification() {
    boolean res = false;
    for (ModellingListFrottementLine modellingListFrottementLine : lines) {
      boolean lineModified = modellingListFrottementLine.applyModification();
      if (lineModified) {
        res = true;
      }
    }
    return res;
  }

  void setEditable(boolean b) {
    fireTableDataChanged();
  }

  int findRowFor(CatEMHBranche branche) {
    for (int i = 0; i < lines.size(); i++) {
      ModellingListFrottementLine line = lines.get(i);
      if (line.getBrancheUid().equals(branche.getUiId())) {
        return i;

      }
    }
    return -1;
  }
  TIntArrayList findRowsFor(CatEMHBranche branche) {
    TIntArrayList res = new TIntArrayList();
    for (int i = 0; i < lines.size(); i++) {
      ModellingListFrottementLine line = lines.get(i);
      if (line.getBrancheUid().equals(branche.getUiId())) {
        res.add(i);

      }
    }
    return res;
  }
}
