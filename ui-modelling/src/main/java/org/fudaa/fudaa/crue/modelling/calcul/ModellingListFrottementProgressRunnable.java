package org.fudaa.fudaa.crue.modelling.calcul;

import org.fudaa.fudaa.crue.common.helper.AbstractFileImporterProgressRunnable;
import org.fudaa.fudaa.crue.modelling.calcul.importer.FrottementImporter;
import org.fudaa.fudaa.crue.modelling.list.ModellingListFrottementLineTableModel;
import org.fudaa.fudaa.crue.modelling.list.ModellingListFrottementTopComponent;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

import java.io.File;

/**
 * @author Frederic Deniger
 */
public class ModellingListFrottementProgressRunnable extends AbstractFileImporterProgressRunnable implements ProgressRunnable<ModellingListFrottementLineTableModel> {
    /**
     * Le top component associé au process d'importer
     */
    protected final ModellingListFrottementTopComponent topComponent;

    public ModellingListFrottementProgressRunnable(File file, ModellingListFrottementTopComponent topComponent) {
        super(file);
        this.topComponent = topComponent;
    }

    @Override
    public ModellingListFrottementLineTableModel run(ProgressHandle handle) {
        String[][] values = null;
        if (file != null) {
            values = readFile();
        }
        if (values == null) {
            return null;
        }

        FrottementImporter importer = new FrottementImporter(values, topComponent);
        importer.setTableModel(topComponent.tableModel);
        return importer.importData();
    }
}
