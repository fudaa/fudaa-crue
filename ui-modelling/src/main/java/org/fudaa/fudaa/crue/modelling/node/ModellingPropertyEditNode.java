/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.node;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.helper.PropertyFinder;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frederic Deniger
 */
public class ModellingPropertyEditNode extends AbstractModellingNodeFirable {

  final String propertyName;
  final Object parent;
  final PropertyFinder.Description desc;

  public ModellingPropertyEditNode(PropertyFinder.Description desc, CrueConfigMetier ccm, Object parent, String propertyName, PerspectiveServiceModelling perspectiveServiceModelling) {
    super(Children.LEAF, Lookups.fixed(desc, ccm), perspectiveServiceModelling);
    setName(desc.propertyDisplay);
    this.desc = desc;
    this.parent = parent;
    this.propertyName = propertyName;
  }

  @Override
  protected Sheet createSheet() {
    Sheet res = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    res.put(set);
    PropertyNodeBuilder builder = new PropertyNodeBuilder();
    PropertySupportReflection support = builder.createPropertySupport(DecimalFormatEpsilonEnum.COMPARISON,desc, this, parent);
    support.setName(propertyName);
    set.put(support);
    return res;
  }
}
