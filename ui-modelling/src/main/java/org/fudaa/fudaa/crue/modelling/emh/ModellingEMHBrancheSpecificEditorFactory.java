/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.emh;

import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.util.MissingResourceException;

/**
 * @author Frédéric Deniger
 */
public class ModellingEMHBrancheSpecificEditorFactory {
  Pair<JPanel, ModellingEMHBrancheSpecificEditor> create(final CatEMHBranche branche, final EnumBrancheType type,
                                                         ModellingEMHBrancheTopComponent parent) {
    JPanel res = createMainPanel(type);
    ModellingEMHBrancheSpecificEditor center = createCenterPanel(branche, type, parent);
    if (center != null) {
      res.add(center.getComponent());
    }
    return new Pair<>(res, center);
  }

  private JPanel createMainPanel(final EnumBrancheType type) throws MissingResourceException {
    JPanel res = new JPanel(new BorderLayout(0, 10));
    res.setBorder(BorderFactory.createTitledBorder(NbBundle.getMessage(ModellingEMHBrancheSpecificEditorFactory.class, "BrancheDescription", type.
        geti18n())));
    return res;
  }

  private ModellingEMHBrancheSpecificEditor createCenterPanel(CatEMHBranche branche, EnumBrancheType type, ModellingEMHBrancheTopComponent parent) {
    switch (type) {
      case EMHBranchePdc:
        return new ModellingEMHBranchePdcPanel(branche.getUiId());
      case EMHBrancheSeuilLateral:
        return new ModellingEMHBrancheSeuilLateralPanel(branche, parent);
      case EMHBrancheSeuilTransversal:
        return new ModellingEMHBrancheSeuilTransversalPanel(branche, parent);
      case EMHBrancheOrifice:
        return new ModellingEMHBrancheOrificePanel(branche, parent);
      case EMHBrancheStrickler:
        return new ModellingEMHBrancheStricklerPanel(branche, parent);
      case EMHBrancheNiveauxAssocies:
        return new ModellingEMHBrancheNiveauxAssociesPanel(branche, parent);
      case EMHBrancheBarrageGenerique:
        return new ModellingEMHBrancheBarrageGeneriquePanel(branche, parent);
      case EMHBrancheBarrageFilEau:
        return new ModellingEMHBrancheBarrageFilEauPanel(branche, parent);
      case EMHBrancheSaintVenant:
        return new ModellingEMHBrancheSaintVenantPanel(branche, parent);
    }

    return null;
  }
}
