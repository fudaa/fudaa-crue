/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import javax.swing.Action;
import org.fudaa.fudaa.crue.modelling.perspective.ActiveModelisation;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
id = "org.fudaa.fudaa.crue.modelling.ModellingOpenScenarioPCALAction")
@ActionRegistration(displayName = "#ModellingScenarioPCALNodeAction.Name")
@ActionReferences({
  @ActionReference(path = ActiveModelisation.ACTIONS_MODELLING_VIEWS, position = 6, separatorAfter = 7)
})
public final class ModellingOpenScenarioPCALAction extends AbstractModellingOpenAction {

  public ModellingOpenScenarioPCALAction() {
    putValue(Action.NAME, NbBundle.getMessage(ModellingOpenScenarioPCALAction.class, "ModellingScenarioPCALNodeAction.Name"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingOpenScenarioPCALAction();
  }

  @Override
  public void doAction() {
    ModellingOpenScenarioPCALNodeAction.open(modellingScenarioService.getScenarioLoaded());
  }
}
