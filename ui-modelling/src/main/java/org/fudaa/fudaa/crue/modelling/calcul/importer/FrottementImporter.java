package org.fudaa.fudaa.crue.modelling.calcul.importer;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.modelling.list.ModellingListFrottementLine;
import org.fudaa.fudaa.crue.modelling.list.ModellingListFrottementLineTableModel;
import org.fudaa.fudaa.crue.modelling.list.ModellingListFrottementTopComponent;
import org.openide.util.NbBundle;

import java.util.HashMap;
import java.util.Map;

/**
 * Import d'un fichier contenant les valeurs des frottements.
 *
 * @author Yoan GRESSIER
 */
public class FrottementImporter {
    // les valeurs importées par fichier
    private final String[][] values;
    // le model associé à l'importer
    private ModellingListFrottementLineTableModel model;
    // Dico des sections, indexées par leur nom
    private final Map<String, CatEMHSection> sections;

    public FrottementImporter(final String[][] values, final ModellingListFrottementTopComponent topComponent) {
        this.values = values;
        // construction d'un dico des sections, indexé par nom
        sections = new HashMap<>();
        for (final CatEMHSection section : topComponent.getSousModele().getSections()) {
            sections.put(section.getNom(), section);
        }
    }

    public void setTableModel(final ModellingListFrottementLineTableModel tableModel) {
        this.model = tableModel;
    }

    /**
     * Recherche de la ligne du model correspondant à la section en paramètre
     *
     * @param sectionName le nom de la section
     * @return la ligne correspondant à la section si elle est trouvée dans le
     *         modèle, null sinon
     */
    private ModellingListFrottementLine getLine(final String sectionName) {
        for (int i = 0; i < model.getRowCount(); i++) {
            final ModellingListFrottementLine line = model.getLine(i);
            if (sections.containsKey(sectionName) && line.getUiSection() == sections.get(sectionName).getUiId()) {
                return line;
            }
        }
        return null;
    }

    /**
     * Decoupage et lecture d'une chaine contenant un nom de section (ex
     * :"Br_BVR4 / St_P135.800" ou "Br_BVR4 / St_P135.800")
     *
     * @param chaine la chaîne à découper
     * @return la partie droite du découpage selon le caractère "/"
     */
    private String getNomSection(final String chaine) {
        final String[] splitChaine = chaine.split("/");
        if (StringUtils.isNotEmpty(chaine) && splitChaine.length > 0) {
            return splitChaine[splitChaine.length - 1].trim();
        } else {
            return StringUtils.EMPTY;
        }
    }

    /**
     * Lecture des noms des frottements contenus dans la chaîne en paramètre
     *
     * @param chaineFrt les noms de frottements, séparés par des "#"
     * @return un tableau contenant les noms de frottements lus
     */
    private String[] getFrts(final String chaineFrt) {
        return chaineFrt.split("#");
    }

    /**
     * Recherche d'un frottement par son nom dans une liste de frottements
     *
     * @param nomFrt      le frottement à chercher
     * @param frottements la liste de frottements pour la recherche
     * @return le frottement si trouvé, null sinon
     */
    private DonFrt getFrts(final String nomFrt, final DonFrt[] frottements) {
        if (frottements != null) {
            for (final DonFrt frt : frottements) {
                if (frt.getNom().equals(nomFrt)) {
                    return frt;
                }
            }
        }
        return null;
    }

    /**
     * Vérifie si les données à importer : on le même format que le model, même
     * nombre de lignes/colonnes, mêmes sections classées dans le même ordre et
     * mêmes lits classés dans le même ordre
     *
     * @param log le log de traitement
     * @return true si le contenu est conforme, false sinon
     */
    private boolean checkContent(final CtuluLog log) {
        // même nombre de lignes/colonnes
        if (model.getRowCount() != (values.length - 1)) {
            log.addSevereError(NbBundle.getMessage(FrottementImporter.class, "importFrt.nbLigneDiff", values.length - 1, model.getRowCount()));
        } else if (model.getColumnCount() != (values[0].length - 1)) {
            log.addSevereError(NbBundle.getMessage(FrottementImporter.class, "importFrt.nbColDiff", values[0].length - 1, model.getColumnCount()));
        } else {

            // mêmes sections classées dans le même ordre
            for (int i = 0; i < model.getRowCount(); i++) {
                final String nomSectionImportee = getNomSection(values[i + 1][0]);
                if (StringUtils.isEmpty(nomSectionImportee) || !sections.containsKey(nomSectionImportee) || model.getLine(i).getUiSection() != sections
                        .get(nomSectionImportee).getUiId()) {
                    log.addSevereError(NbBundle.getMessage(FrottementImporter.class, "importFrt.sectionMemeOrdre", i + 2));
                    break;
                }
            }

            // même headers
            for (int i = 0; i < model.getColumnCount(); i = i + 2) {
                final String header = model.getColumnName(i);
                if (StringUtils.isEmpty(header) || StringUtils.isEmpty(values[0][i + 1]) || !header.equals(values[0][i + 1])) {
                    log.addSevereError(NbBundle.getMessage(FrottementImporter.class, "importFrt.headerMemeOrdre", i + 2));
                    break;
                }
            }
        }

        if (log.containsErrorOrSevereError()) {
            // si erreurs, affichage du log
            LogsDisplayer.displayError(log, NbBundle.getMessage(CLimMsImporter.class, "importFrt.erreurFormat"));
            return false;
        } else {
            return true;
        }
    }

    /**
     * Traitement d'importer des données
     *
     * @return les données importees
     */
    public ModellingListFrottementLineTableModel importData() {
        final CtuluLog log = new CtuluLog();

        // vérification de la concordance du format des données d'entrée avec le model
        if (!checkContent(log)) {
            return null;
        }

        // parcours des lignes lues dans le fichier
        for (int indexLine = 1; indexLine < values.length; indexLine++) {
            final String[] line = values[indexLine];
            // lecture du nom de section
            final String nomSection = getNomSection(line[0]);
            if (StringUtils.isNotEmpty(nomSection)) {
                // lecture de la ligne associée dans le model
                final ModellingListFrottementLine listFrottementLine = getLine(nomSection);
                if (listFrottementLine != null) {
                    int i = 2;
                    for (final ModellingListFrottementLine.LitNommeCell litNommeCell : listFrottementLine.getLitNommes()) {
                        // lecture des frottements importés depuis le fichier pour cette cellule
                        final String[] frts = getFrts(line[i]);
                        int j = 0;
                        if (litNommeCell.getLitNumerotes().size() == frts.length) {
                            for (final ModellingListFrottementLine.LitNumeroteCell litNumeroteCell : litNommeCell.getLitNumerotes()) {
                                // pas d'importer sur un lit inexistant
                                if (!litNumeroteCell.getLitNumerote().isLongueurNulle()) {
                                    // recherche si le frottement importé est autorisé
                                    final DonFrt curFrt = getFrts(frts[j], litNumeroteCell.getAccepted());
                                    if (curFrt != null) {
                                        // si autorisé, on le fixe
                                        litNumeroteCell.setCurrentFrt(curFrt);
                                    } else {
                                        // frottement non autorisé
                                        log.addSevereError(NbBundle.getMessage(FrottementImporter.class, "importFrt.frtNonAutorise", i + 1, nomSection));
                                    }
                                } else if (frts.length > 0 && !frts[0].startsWith(EnumTypeLoi.LoiZFKSto.getNom())) {
                                    // frottement non autorisé
                                    log.addSevereError(NbBundle.getMessage(FrottementImporter.class, "importFrt.frtNonAutorise", i + 1, nomSection));
                                }
                                j++;
                            }
                        } else {
                            // log Lit numérotés de taille différentes
                            log.addSevereError(NbBundle.getMessage(FrottementImporter.class, "importFrt.litNumTailleDiff", i + 1, nomSection, frts.length,
                                    litNommeCell.getLitNumerotes().size()));
                        }

                        i += 2;
                    }
                } else {
                    // ligne non trouvée dans le modèle
                    log.addSevereError(NbBundle.getMessage(FrottementImporter.class, "importFrt.ligneNonTrouvee", nomSection));
                }
            }
        }

        if (log.containsErrorOrSevereError()) {
            LogsDisplayer.displayError(log, NbBundle.getMessage(CLimMsImporter.class, "importCLimMs.bilan"));
            return null;
        }

        return model;
    }
}
