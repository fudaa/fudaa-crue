package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.fudaa.crue.modelling.calcul.ModellingModeleOPTITopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public abstract class ModellingOpenModeleOPTINodeAction extends AbstractModellingOpenModeleNodeAction {

  public ModellingOpenModeleOPTINodeAction(String name) {
    super(name, ModellingModeleOPTITopComponent.MODE, ModellingModeleOPTITopComponent.TOPCOMPONENT_ID);
  }

  public static void open(EMHModeleBase modele) {
    if (modele == null) {
      return;
    }
    ModellingOpenModeleOPTINodeAction action = SystemAction.get(ModellingOpenModeleOPTINodeAction.Reopen.class);
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(modele));
    action.performAction(new Node[]{node});
  }

  public static class Reopen extends ModellingOpenModeleOPTINodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenModeleOPTINodeAction.class, "ModellingModeleOPTINodeAction.Name"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenModeleOPTINodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenModeleOPTINodeAction.class, "ModellingModeleOPTINodeAction.NewFrame.Name"));
      setReopen(false);
    }
  }
}
