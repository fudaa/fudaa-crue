package org.fudaa.fudaa.crue.modelling.list;

import java.awt.datatransfer.Transferable;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.MissingResourceException;
import javax.swing.Action;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.edition.bean.ListCasierContent;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertyStringReadOnly;
import org.fudaa.fudaa.crue.common.property.PropertySupportRead;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.modelling.edition.ListAjoutCasierProcess;
import org.fudaa.fudaa.crue.modelling.emh.ModellingOpenEMHNodeAction;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class ListCasierContentNode extends AbstractListContentNode {

  public static String getCasierNameDisplay() throws MissingResourceException {
    return NbBundle.getMessage(ListCasierContentNode.class, "CasierName.Name");
  }

  public static String getCasierNoeudDisplayName() throws MissingResourceException {
    return NbBundle.getMessage(ListCasierContentNode.class,
            "NoeudName.Name");
  }

  public ListCasierContentNode(CatEMHCasier casier, ListCasierContent content,
          PerspectiveServiceModelling perspectiveServiceModelling) {
    super(Children.LEAF, Lookups.fixed(casier, content), perspectiveServiceModelling);
  }

  @Override
  public PasteType getDropType(Transferable t, int action, int index) {
    return null;
  }

  @Override
  public Action[] getActions(boolean context) {
    return new Action[]{
              SystemAction.get(ModellingOpenEMHNodeAction.Reopen.class),
              SystemAction.get(ModellingOpenEMHNodeAction.NewFrame.class)
            };
  }

  @Override
  public void destroy() throws IOException {
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    ListCasierContent casierContent = getLookup().lookup(ListCasierContent.class);
    CatEMHCasier casier = getLookup().lookup(CatEMHCasier.class);
    try {
      Node.Property nom = new PropertySupportRead<ListCasierContent, String>(casierContent, String.class, ListCommonProperties.PROP_NOM,
              ListCommonProperties.PROP_NOM,
              ListCasierContentNode.getCasierNameDisplay()) {

        @Override
        public String getValue() throws IllegalAccessException, InvocationTargetException {
          return getInstance().getNom();
        }
      };
      nom.setDisplayName(getCasierNameDisplay());
      PropertyCrueUtils.configureNoCustomEditor(nom);
      set.put(nom);
      //type
      Node.Property type = new PropertyStringReadOnly(casierContent.getType().geti18n(), ListCommonProperties.PROP_TYPE,
              AbstractListContentNode.getTypeDisplay(),
              null);
      PropertyCrueUtils.configureNoCustomEditor(type);
      set.put(type);
      List<CatEMHNoeud> noeuds = ListAjoutCasierProcess.getAvailableNoeudsForEditionCasiers(casier.getParent());
      //noeud 
      Node.Property noeud = new PropertySupportNoeudCasier(this, casierContent, TransformerHelper.toNom(noeuds));
      set.put(noeud);

      //active
      Node.Property active = new PropertySupportReflection(this, casierContent, Boolean.TYPE,
              ListCommonProperties.PROP_ACTIVE);
      active.setName(ListCommonProperties.PROP_ACTIVE);
      active.setDisplayName(AbstractListContentNode.getActiveDisplay());
      set.put(active);

      //commentaire
      Node.Property commentaire = new PropertySupportReflection(this, casierContent, String.class,
              ListCommonProperties.PROP_COMMENTAIRE);
      PropertyCrueUtils.setNullValueEmpty(commentaire);
      commentaire.setName(ListCommonProperties.PROP_COMMENTAIRE);
      commentaire.setDisplayName(AbstractListContentNode.getCommentDisplay());
      set.put(commentaire);
    } catch (NoSuchMethodException ex) {
      Exceptions.printStackTrace(ex);
    }
    sheet.put(set);
    return sheet;
  }
}
