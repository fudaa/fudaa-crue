/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import javax.swing.JMenuItem;
import org.fudaa.dodico.crue.metier.emh.CalcTransItem;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.fudaa.crue.common.action.AbstractNodeAction;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.modelling.loi.ModellingOpenDLHYNodeAction;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class CalculOpenLoiNodeAction extends AbstractNodeAction {

  public CalculOpenLoiNodeAction() {
    super(NbBundle.getMessage(CalculOpenLoiNodeAction.class, "OpenLoi.ActionName"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return getLoi(activatedNodes) != null;
  }

  private Loi getLoi(Node[] activatedNodes) {
    if (activatedNodes.length == 1) {
      DonCLimM dclm = activatedNodes[0].getLookup().lookup(DonCLimM.class);
      if (dclm instanceof CalcTransItem) {
        return ((CalcTransItem) dclm).getLoi();
      }
    }
    return null;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    Loi loi = getLoi(activatedNodes);
    if (loi != null) {
      ModellingOpenDLHYNodeAction.open(loi, true);
    }
  }

  @Override
  public JMenuItem getPopupPresenter() {
    return NodeHelper.useBoldFont(super.getPopupPresenter());
  }
}
