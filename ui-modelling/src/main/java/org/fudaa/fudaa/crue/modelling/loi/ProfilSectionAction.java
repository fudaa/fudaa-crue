package org.fudaa.fudaa.crue.modelling.loi;

import javax.swing.Action;
import org.fudaa.fudaa.crue.modelling.action.*;
import org.fudaa.fudaa.crue.modelling.perspective.ActiveModelisation;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
id = "org.fudaa.fudaa.crue.modelling.ProfilSectionAction")
@ActionRegistration(displayName = "#ModellingOpenProfilSectionNodeAction.DisplayName")
@ActionReferences({
  @ActionReference(path = ActiveModelisation.ACTIONS_MODELLING_VIEWS, position = 1)
})
public final class ProfilSectionAction extends AbstractModellingOpenAction {

  public ProfilSectionAction() {
    putValue(Action.NAME, NbBundle.getMessage(ProfilSectionAction.class, "ModellingOpenProfilSectionNodeAction.DisplayName"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ProfilSectionAction();
  }

  @Override
  public void doAction() {
    ProfilSectionOpenNodeAction.open();
  }
}
