package org.fudaa.fudaa.crue.modelling.calcul;

import java.awt.BorderLayout;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.modelling.AbstractModellingTopComponent;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.openide.util.Lookup;

/**
 *
 * @author Frédéric Deniger
 */
public abstract class AbstractScenarioTopComponent extends AbstractModellingTopComponent {

  protected final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);

  protected static void restoreTabbedPaneSelection(JComponent oldCenter, JTabbedPane tabbedPane) {
    if (oldCenter != null) {
      JTabbedPane old = (JTabbedPane) oldCenter;
      int selectedIndex = old.getSelectedIndex();
      tabbedPane.setSelectedIndex(selectedIndex);
    }
  }

  @Override
  protected EMHScenario getScenario() {
    return modellingScenarioService.getScenarioLoaded();
  }

  @Override
  public void cancelModificationHandler() {
    scenarioReloaded();
    setModified(false);
  }

  /**
   * WARN: A utiliser uniquement pour les tests
   */
  public void testScenarioLoaded(EMHScenario scenario) {
    removeAll();
    buildComponents(scenario, null);
  }

  @Override
  protected void scenarioLoaded() {
    JComponent oldCenter = CtuluLibSwing.findChildByName(this, CENTER_NAME);
    removeAll();
    revalidate();
    buildComponents(modellingScenarioModificationService.getModellingScenarioService().getScenarioLoaded(), oldCenter);

  }
  public static final String CENTER_NAME = "main";

  protected void buildComponents(EMHScenario scenario, JComponent oldCenter) {
    final JComponent centerPanel = buildCenterPanel(scenario, oldCenter);
    centerPanel.setName(CENTER_NAME);
    add(centerPanel);
    add(createCancelSaveButtons(), BorderLayout.SOUTH);
    revalidate();
    repaint();
    centerPanel.revalidate();
    centerPanel.repaint();
  }

  protected abstract JComponent buildCenterPanel(EMHScenario scenario, JComponent oldCenter);

  @Override
  protected void scenarioReloaded() {
    scenarioLoaded();
  }

  @Override
  protected void scenarioChanged(ScenarioModificationEvent event) {
    scenarioReloaded();
  }

  @Override
  protected void scenarioUnloaded() {
    removeAll();
  }
}
