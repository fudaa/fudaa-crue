package org.fudaa.fudaa.crue.modelling.perspective;

import org.fudaa.dodico.crue.edition.EditionChecker;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.UserSaveAnswer;
import org.fudaa.fudaa.crue.common.services.AbstractPerspectiveService;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.common.services.PerspectiveService;
import org.fudaa.fudaa.crue.common.services.PerspectiveState;
import org.fudaa.fudaa.crue.modelling.*;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioServiceImpl;
import org.fudaa.fudaa.crue.study.services.CrueService;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Le service perspective de Modélisation.
 * @see AbstractPerspectiveService
 *
 * @author Fred Deniger
 */
@ServiceProviders(value = {
        @ServiceProvider(service = PerspectiveServiceModelling.class)
        ,
        @ServiceProvider(service = PerspectiveService.class)})
public class PerspectiveServiceModelling extends AbstractPerspectiveService {
    private final Set<String> components = Collections.unmodifiableSet(new LinkedHashSet<>(Arrays.asList(
            ModellingPropertiesTopComponent.TOPCOMPONENT_ID,
            ModellingContainersTopComponent.TOPCOMPONENT_ID,
            ModellingComputeDataTopComponent.TOPCOMPONENT_ID,
            ModellingFilesTopComponent.TOPCOMPONENT_ID,
            ModellingNetworkTopComponent.TOPCOMPONENT_ID,
            ModellingVisualTopComponent.TOPCOMPONENT_ID,
            ModellingCtuluLogTopComponent.TOPCOMPONENT_ID)));
    private final CrueService crueService = Lookup.getDefault().lookup(CrueService.class);

    /**
     * Ajouter les listener sur le service {@link CrueService} pour réagir aux chargement de projet et scenario.
     */
    public PerspectiveServiceModelling() {
        super(PerspectiveEnum.MODELLING);
        crueService.addEMHProjetStateLookupListener(ev -> projectLoadedChange());
        crueService.addModellingEMHScenarioLookupListener(ev -> scenarioLoadedChanged());
    }

    /**
     * Vérifie si une vue est en édition ou si la perspective est modifiée: dans ce cas demande confirmation à l'utilisateur.
     *
     * @return true la perspective peut être fermée.
     */
    @Override
    public boolean closing() {
        return confirmClose();
    }

    @Override
    public String getPathForViewsAction() {
        return ActiveModelisation.ACTIONS_MODELLING_VIEWS;
    }

    /**
     * Methode appelée au chargement/déchargement d'un scenario pour mettre à jour l'état de la perspective
     */
    protected void scenarioLoadedChanged() {
        final ModellingScenarioServiceImpl modellingScenarioServiceImpl = Lookup.getDefault().lookup(ModellingScenarioServiceImpl.class);
        //en cours de modification: on ne fait rien.
        if (modellingScenarioServiceImpl.isReloading()) {
            return;
        }

        //le projet est chargé en mode read-only: pas de question :)
        if (crueService.isProjetReadOnly()) {
            setState(PerspectiveState.MODE_READ_ONLY_ALWAYS);
        } else {
            final boolean scenarioLoadedInModelling = crueService.isAScenarioLoadedInModellingPerspective();
            //un scenario est chargé: on passe en mode read ( edition activable)
            //sinon, read-only temporaire ( pas d'édition car pas de scenario chargé).
            setState(scenarioLoadedInModelling ? PerspectiveState.MODE_READ : PerspectiveState.MODE_READ_ONLY_TEMP);
        }
    }

    /**
     * Modifié l'état de la perpsective en fonction du projet chargé et du mode de chargement (read-only).
     */
    protected void projectLoadedChange() {
        if (!crueService.isProjetCurrentlyReloading()) {
            //le projet est fermé: réinitialisé l'état à read-only
            if (crueService.getProjectLoaded() == null) {
                resetStateToReadOnlyTemp();
            } else {
                //un projet est chargé: modifie l'état pour prendre en compte le mode de chargement.
                //MODE_READ_ONLY_TEMP: la perspective n'est pas editable ( bouton edition desactive)
                //MODE_READ_ONLY_ALWAYS: indique que le projet sera toujours en mode read-only et aucun action utilisateur pourra changer
                setState(crueService.isProjetReadOnly() ? PerspectiveState.MODE_READ_ONLY_ALWAYS : PerspectiveState.MODE_READ_ONLY_TEMP);
            }
        }
    }

    /**
     * @return true: Edition possible dans cette perspective.
     */
    @Override
    public boolean supportEdition() {
        return true;
    }

    /**
     * Fait des vérifications <code>newState</code> vaut {@link PerspectiveState#MODE_EDIT}: si le scenario est du Crue9 ou si la xsd n'est pas editable ( trop
     * ancienne), le mode edition ne peut pas être activé
     *
     * @param newState le nouvel etat attendu
     * @return dans le cas EDITION, renvoie false si le scenario est de type Crue 9 ou si la xsd n'est pas editable.
     */
    @Override
    protected boolean canStateBeModifiedTo(final PerspectiveState newState) {
        if (PerspectiveState.MODE_EDIT.equals(newState)) {
            final ManagerEMHScenario manager = crueService.getManagerScenarioLoadedInModellingPerspective();
            final EMHProjet projectLoaded = crueService.getProjectLoaded();
            final String xsdVersion = projectLoaded.getCoeurConfig().getXsdVersion();
            if (manager != null) {
                if (manager.isCrue9() || !EditionChecker.isEditable(xsdVersion)) {
                    DialogHelper.showWarn(NbBundle.getMessage(PerspectiveServiceModelling.class, "Modelling.EditonImpossible"));
                    return false;
                }
            }
            if(crueService.isAocCampagneLoaded()){
                DialogHelper.showWarn(NbBundle.getMessage(PerspectiveServiceModelling.class, "Modelling.EditonImpossibleAocOpened"));
                return false;
            }
            if(crueService.isLhptLoaded()) {
                DialogHelper.showWarn(NbBundle.getMessage(getClass(), "Modelling.EditonImpossibleLhptOpened"));
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean activate() {
        return true;
    }

    @Override
    public Set<String> getDefaultTopComponents() {
        return components;
    }

    /**
     * Appelé par la methode {@link #closing()} et par l'action {@link org.fudaa.fudaa.crue.modelling.action.ModellingCloseAction} par valider la fermeture
     * de la perspective. Teste si une view est ouverte et modifiée, si le scenario est modifié.... et demande à l'utilisateur confirmation.
     *
     * @return true si la perspective peut être fermé ( si pas de modification ou si l'utilisateur accepte l'enregistrement puis fermeture).
     */
    public boolean confirmClose() {
        //un topCompoent est actif: si modifié on demande de valider ou pas les modifications en cours
        final TopComponent activated = WindowManager.getDefault().getRegistry().getActivated();
        if (activated instanceof AbstractModellingTopComponent) {
            final AbstractModellingTopComponent modellingTopComponent = (AbstractModellingTopComponent) activated;
            modellingTopComponent.forceSave();
        }
        //le scenario en cours est modifié: on demande confirmation
        if (isDirty()) {
            final UserSaveAnswer confirmSaveOrNot = DialogHelper.confirmSaveOrNot();
            if (UserSaveAnswer.CANCEL.equals(confirmSaveOrNot)) {
                //l utilisateur ne veut pas fermer:
                return false;
            }
            final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);
            //fermeture sans sauvegarde
            if (UserSaveAnswer.DONT_SAVE.equals(confirmSaveOrNot)) {
                modellingScenarioService.unloadScenario();
            } else {
                //fermeture avec sauvegarde: on renvoie le resultat de la sauvegarde. Si echec, pas de fermeture et l'utilsateur est avertit et peut voir
                //les raison.
                return modellingScenarioService.saveScenario();
            }
        }
        //pas de modification: on peut fermer.
        return true;
    }
}
