/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.DonCLimMCommonItem;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.modelling.node.AbstractModellingNodeFirable;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frederic Deniger
 */
public class CalculEMHsNode extends AbstractModellingNodeFirable {

  final OutlineView view;

  public CalculEMHsNode(OutlineView view, Children children, Calc calc, PerspectiveServiceModelling perspectiveServiceModelling) {
    super(children, Lookups.fixed(calc), perspectiveServiceModelling);
    this.view = view;
    setName(NbBundle.getMessage(CalculNodeFactory.class, "Calcul.ListEMH.Description"));
  }



  public Calc getCalc() {
    return getLookup().lookup(Calc.class);
  }

  @Override
  public SystemAction[] getActions() {
    return new SystemAction[]{SystemAction.get(CalculAddDclmNodeAction.class)};
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(getCalc()));
  }

  @Override
  public SystemAction getDefaultAction() {
    return SystemAction.get(CalculAddDclmNodeAction.class);
  }

  void addDclm(DonCLimMCommonItem newDclm) {
    CalculNodeFactory fac = new CalculNodeFactory(view);
    fac.addNewDclm(this, newDclm);
  }
}
