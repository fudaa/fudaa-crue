package org.fudaa.fudaa.crue.modelling.perspective;

import java.util.Collection;
import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractPerspectiveAction;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;
/**
 * Permet d'activer une perspective. Action utilisée dans la toobar pour changer de perspective. Pour la gestion des ordres des menus, voir le fichier
 * layer.xml
 */
@ActionID(category = "File", id = "org.fudaa.fudaa.crue.modelling.perspective.ActiveModelisation")
@ActionRegistration(displayName = "#CTL_ActiveModelisation")
@ActionReference(path = "Menu/Window", position = 3)
public final class ActiveModelisation extends AbstractPerspectiveAction {

  public static final String ACTIONS_MODELLING_VIEWS = "Actions/Modelling-Views";
  public static final String FOLDER_ACTION = "Actions/Modelling";

  public ActiveModelisation() {
    super("CTL_ActiveModelisation", PerspectiveServiceModelling.class.getName(), PerspectiveEnum.MODELLING);
    setBooleanState(false);
  }

  @Override
  protected String getFolderAction() {
    return FOLDER_ACTION;
  }

  @Override
  protected void addSpecificMenuAtEnd(JPopupMenu menu) {
    JMenu menuFrame = new JMenu(NbBundle.getMessage(ActiveModelisation.class, "MenuFrame.Name"));
    menu.addSeparator();
    menu.add(menuFrame);
    Collection all = Lookups.forPath(ACTIONS_MODELLING_VIEWS).lookupAll(Object.class);
    for (Object object : all) {
      if (object instanceof AbstractAction) {
        menuFrame.add((AbstractAction) object);
      } else if (object instanceof JSeparator) {
        menuFrame.addSeparator();
      }
    }
  }
}
