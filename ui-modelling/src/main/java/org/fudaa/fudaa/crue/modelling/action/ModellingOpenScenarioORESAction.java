/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import javax.swing.Action;
import org.fudaa.fudaa.crue.modelling.perspective.ActiveModelisation;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
id = "org.fudaa.fudaa.crue.modelling.ModellingOpenScenarioORESAction")
@ActionRegistration(displayName = "#ModellingScenarioORESNodeAction.Name")
@ActionReferences({
  @ActionReference(path = ActiveModelisation.ACTIONS_MODELLING_VIEWS, position = 5)
})
public final class ModellingOpenScenarioORESAction extends AbstractModellingOpenAction {

  public ModellingOpenScenarioORESAction() {
    putValue(Action.NAME, NbBundle.getMessage(ModellingOpenScenarioORESAction.class, "ModellingScenarioORESNodeAction.Name"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingOpenScenarioORESAction();
  }

  @Override
  public void doAction() {
    ModellingOpenScenarioORESNodeAction.open(modellingScenarioService.getScenarioLoaded());
  }
}
