/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.comparator.InfoEMHByUserPositionComparator;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPerm;
import org.fudaa.dodico.crue.metier.emh.CalcTrans;
import org.fudaa.dodico.crue.metier.emh.CalcTransItem;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;
import org.fudaa.dodico.crue.metier.emh.DonLoiHYConteneur;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.OrdCalc;
import org.fudaa.dodico.crue.metier.factory.OrdCalcFactory;
import org.fudaa.dodico.crue.metier.helper.CalcHelper;
import org.fudaa.dodico.crue.metier.helper.OrdCalcCloner;
import org.fudaa.dodico.crue.metier.helper.OrdCalcCloner.Result;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

/**
 * @author Frederic Deniger
 */
public class CalculNodeFactory {

  final PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
  private final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);
  private final OutlineView outlineView;
  private final DonLoiHYConteneur donLoiHYConteneurCloned;
  private final Map<String, Loi> clonedLoisByName;

  public CalculNodeFactory(final OutlineView outlineView) {
    this.outlineView = outlineView;
    donLoiHYConteneurCloned = (DonLoiHYConteneur) outlineView.getClientProperty(DonLoiHYConteneur.class);
    clonedLoisByName = TransformerHelper.toMapOfNom(donLoiHYConteneurCloned.getLois());
  }

  private Children createChildren(final Calc calc, final Map<Calc, OrdCalc> ordCalcByCalcs) {
    final List<Node> nodes = new ArrayList<>();
    final List<DonCLimM> listeDCLM = calc.getlisteDCLM();
    final List<CalculDclmNode> emhNodes = new ArrayList<>();
    for (final DonCLimM donCLimM : listeDCLM) {
      final CalculDclmNode node = createDclmNode(donCLimM);
      emhNodes.add(node);
    }
    nodes.add(new CalculCommentaireNode(Children.LEAF, calc, perspectiveServiceModelling));
    final CalculEMHsNode dclms = new CalculEMHsNode(outlineView, NodeHelper.createArray(emhNodes, false), calc, perspectiveServiceModelling);
    nodes.add(dclms);
    final OrdCalc ordCalc = ordCalcByCalcs.get(calc);
    if (ordCalc != null) {
      final CalculOcalNode ocalNode = new CalculOcalNode(outlineView, ordCalc, perspectiveServiceModelling, modellingScenarioService.getSelectedProjet().
          getPropDefinition());
      nodes.add(ocalNode);
    }

    return NodeHelper.createArray(nodes);
  }

  /**
   * @return les nodes du calcul.
   */
  public List<CalculNode> createNodes() {
    if (modellingScenarioService.getScenarioLoaded() == null) {
      return Collections.emptyList();
    }
    final List<CalculNode> nodes = new ArrayList<>();
    final OrdCalcCloner cloner = new OrdCalcCloner();
    final Result cloneOrdCalcAndCalc = cloner.
        cloneAndSort(modellingScenarioService.getScenarioLoaded(), modellingScenarioService.getManagerScenarioLoaded());
    final Map<Calc, OrdCalc> ordCalcByCalc = CalcHelper.getOrdCalcByCalc(cloneOrdCalcAndCalc.ordCalcs);
    //contient les ordcalc sauvegardés pour l'ui
    final Map<Calc, OrdCalc> uiOrdCalcByCalc = CalcHelper.getOrdCalcByCalc(cloneOrdCalcAndCalc.ordCalcsInUI);
    final CrueConfigMetier ccm = modellingScenarioService.getSelectedProjet().getPropDefinition();
    for (final Calc calc : cloneOrdCalcAndCalc.calcs) {
      final CalculNode node = new CalculNode(outlineView, createChildren(calc, ordCalcByCalc), calc, ccm,
          perspectiveServiceModelling);
      if (!node.isActivated()) {
        OrdCalc saved = uiOrdCalcByCalc.get(calc);
        if (saved == null) {
          saved = OrdCalcFactory.createDefaultOcal(calc, ccm);
        }
        node.setOldUsedOrdCalc(saved);

      }
      nodes.add(node);
    }
    return nodes;
  }

  public CalculNode addNewCalc(final Calc newCalc) {
    CalculNode node = null;
    try {
      final CrueConfigMetier ccm = modellingScenarioService.getSelectedProjet().getPropDefinition();
      node = new CalculNode(outlineView, createChildren(newCalc, Collections.emptyMap()), newCalc,
          ccm,
          perspectiveServiceModelling);
      node.setOldUsedOrdCalc(OrdCalcFactory.createDefaultOcal(newCalc, ccm));
      final ExplorerManager newManager = ExplorerManager.find(outlineView);
      final Node[] newNodeArray = new Node[]{node};
      newManager.getRootContext().getChildren().add(newNodeArray);
      newManager.setSelectedNodes(newNodeArray);
      NodeHelper.expandNode(node, outlineView);
    } catch (final PropertyVetoException ex) {
      Exceptions.printStackTrace(ex);
    }
    return node;
  }

  public CalculDclmNode createDclmNode(final DonCLimM donCLimM) {
    if (donCLimM.getCalculParent().isTransitoire()) {
      if (donCLimM instanceof CalcTransItem) {
        final CalcTransItem asTrans = (CalcTransItem) donCLimM;
        if (asTrans.getLoi() != null) {
          asTrans.setLoi(clonedLoisByName.get(asTrans.getLoi().getNom()));
        }
      }
      return new CalculDclmTransNode(Children.LEAF, donCLimM, modellingScenarioService.getSelectedProjet().getPropDefinition(),
          perspectiveServiceModelling, donLoiHYConteneurCloned);
    }
    return new CalculDclmNode(Children.LEAF, donCLimM, modellingScenarioService.getSelectedProjet().getPropDefinition(),
        perspectiveServiceModelling, donLoiHYConteneurCloned);
  }

  public void addNewDclm(final CalculEMHsNode parent, final DonCLimM newDclm) {
    final List<Node> nodes = new ArrayList<>(Arrays.asList(parent.getChildren().getNodes()));
    parent.getCalc().addDclm(newDclm);
    nodes.add(createDclmNode(newDclm));
    final List<Class> classesInOrder = newDclm.getCalculParent().isPermanent() ? CalcPseudoPerm.getDclmClassInOrder() : CalcTrans.getDclmClassInOrder();
    final Map<Class, List<CalculDclmNode>> byClass = new LinkedHashMap<>();
    for (final Class orderedClass : classesInOrder) {
      byClass.put(orderedClass, new ArrayList<>());
    }
    for (final Node node : nodes) {
      final CalculDclmNode dclmNode = (CalculDclmNode) node;
      byClass.get(dclmNode.getDCLM().getClass()).add(dclmNode);
    }

    final CalculDclmNodeComparator nodeComparator = new CalculDclmNodeComparator(new InfoEMHByUserPositionComparator(modellingScenarioService.
        getScenarioLoaded()));
    nodes.clear();
    for (final List<CalculDclmNode> list : byClass.values()) {
      list.sort(nodeComparator);
      nodes.addAll(list);
    }
    if (parent.getChildren().getNodesCount() > 0) {
      parent.getChildren().remove(parent.getChildren().getNodes());
    }
    parent.getChildren().add(nodes.toArray(new Node[0]));
  }

  private static class CalculDclmNodeComparator extends SafeComparator<CalculDclmNode> {

    final InfoEMHByUserPositionComparator emhComparator;

    public CalculDclmNodeComparator(final InfoEMHByUserPositionComparator emhComparator) {
      this.emhComparator = emhComparator;
    }

    @Override
    protected int compareSafe(final CalculDclmNode o1, final CalculDclmNode o2) {
      return emhComparator.compare(o1.getDCLM(), o2.getDCLM());
    }
  }
}
