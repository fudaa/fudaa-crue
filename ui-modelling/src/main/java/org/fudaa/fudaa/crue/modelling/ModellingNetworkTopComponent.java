package org.fudaa.fudaa.crue.modelling;

import com.rits.cloning.Cloner;

import java.awt.BorderLayout;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.swing.ActionMap;

import org.fudaa.dodico.crue.edition.EditionChangeSousModele;
import org.fudaa.dodico.crue.edition.EditionChangeSousModele.MoveToSousModeleResult;
import org.fudaa.dodico.crue.edition.EditionChangeUtils;
import org.fudaa.dodico.crue.edition.EditionDelete;
import org.fudaa.dodico.crue.edition.EditionDelete.RemoveBilan;
import org.fudaa.dodico.crue.edition.SimplificationProcessor;
import org.fudaa.dodico.crue.edition.SimplificationSeuils;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumCasierType;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.view.ExpandedNodesManager;
import org.fudaa.fudaa.crue.emh.node.LinkedEMHHelper;
import org.fudaa.fudaa.crue.emh.node.NodeEMHDefault;
import org.fudaa.fudaa.crue.modelling.loi.SimplificationSeuilsChooser;
import org.fudaa.fudaa.crue.modelling.node.ModellingNodeEMHFactory;
import org.fudaa.fudaa.crue.modelling.services.ModellingEMHVisibilityService;
import org.fudaa.fudaa.crue.modelling.services.ModellingSelectedEMHService;
import org.fudaa.fudaa.crue.common.ContainerActivityVisibility;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 * Top component Affichant le réseau.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.modelling//ModellingNetworkTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = ModellingNetworkTopComponent.TOPCOMPONENT_ID,
        iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png",
        persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "modelling-topRight", openAtStartup = false, position = 3)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.modelling.ModellingNetworkTopComponent")
@ActionReference(path = "Menu/Window/Modelling", position = 3)
@TopComponent.OpenActionRegistration(displayName = ModellingNetworkTopComponent.TOPCOMPONENT_ACTION_DISPLAYNAME,
        preferredID = ModellingNetworkTopComponent.TOPCOMPONENT_ID)
public final class ModellingNetworkTopComponent extends AbstractModellingTopComponent implements LookupListener, ExplorerManager.Provider,
        ModellingTopComponentEMHEditable {

    public static final String TOPCOMPONENT_ID = "ModellingNetworkTopComponent";
    public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
    public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
    private Lookup.Result<Long> resultatEMHsSelected;
    private Lookup.Result<NodeEMHDefault> resultatNodesSelected;

    private Lookup.Result<ContainerActivityVisibility> resultatActivity;

    final ModellingSelectedEMHService modellingSelectedEMHService = Lookup.getDefault().lookup(ModellingSelectedEMHService.class);

    final ModellingEMHVisibilityService modellingEMHVisibilityService = Lookup.getDefault().lookup(ModellingEMHVisibilityService.class);
    private final ExplorerManager em = new ExplorerManager();
    private final org.openide.explorer.view.OutlineView outlineView;
    private SelectedEMHLookupListener selectedEMHLookupListener;
    private SelectedNodeLookupListener selectedNodeLookupListener;
    private LookupListener activityLookupListener;
    private final ExpandedNodesManager expandedNodesManager;

    public ModellingNetworkTopComponent() {
        initComponents();
        outlineView = new OutlineView(NbBundle.getMessage(ModellingNetworkTopComponent.class,
                "ModellingNetworkTopComponent.FirstColumn.Name"));
        outlineView.getOutline().setFullyNonEditable(true);
        outlineView.getOutline().setFillsViewportHeight(true);
        outlineView.getOutline().setColumnHidingAllowed(false);
        outlineView.addPropertyColumn(NodeEMHDefault.PROP_VALUE,
                NbBundle.getMessage(ModellingNetworkTopComponent.class,
                        "ModellingNetworkTopComponent." + NodeEMHDefault.PROP_VALUE + ".Name"));

        setName(NbBundle.getMessage(ModellingNetworkTopComponent.class, TOPCOMPONENT_ACTION));
        setToolTipText(NbBundle.getMessage(ModellingNetworkTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
        ActionMap map = this.getActionMap();
        associateLookup(ExplorerUtils.createLookup(em, map));
        outlineView.setDefaultActionAllowed(true);
        add(outlineView);
        add(createCancelSaveButtons(), BorderLayout.SOUTH);
        LinkedEMHHelper.installShortcutsAndCellRenderer(outlineView, em);
        add(LinkedEMHHelper.installBackNextButtons(outlineView), BorderLayout.NORTH);
        expandedNodesManager = new ExpandedNodesManager(outlineView.getOutline().getOutlineModel());

    }

    @Override
    public ExplorerManager getExplorerManager() {
        return em;
    }

    @Override
    protected String getViewHelpCtxId() {
        return "vueReseau";
    }

    protected Collection<EMH> cloneIfNeeded(Collection<EMH> initEmhs) {
        Collection<EMH> emhs = initEmhs;
        if (cloned == null) {
            Cloner cloner = new Cloner();
            cloned = cloner.deepClone(getModellingService().getScenarioLoaded());
            Set<Long> toSetOfUId = TransformerEMHHelper.toSetOfUId(emhs);
            emhs = new ArrayList<>();
            for (Long uid : toSetOfUId) {
                emhs.add(cloned.getIdRegistry().getEmh(uid));
            }
        }
        return emhs;
    }

    @Override
    protected void componentActivated() {
        super.componentActivated();
        ExplorerUtils.activateActions(em, true);
    }

    EMHScenario cloned;

    @Override
    public void deleteEMH(Collection<EMH> initEmhs, boolean cascade) {
        Collection<EMH> emhs = cloneIfNeeded(initEmhs);
        RemoveBilan deleted = EditionDelete.delete(emhs, cloned, cascade);
        setModified(true);
        scenarioReloaded();
        LogsDisplayer.displayBilan(deleted);
    }
    
    @Override
    public void toggleStatusEMH(Collection<EMH> initEmhs, boolean status) {
        Collection<EMH> emhs = cloneIfNeeded(initEmhs);
        EditionChangeUtils.toggleStatus(emhs, status);
        setModified(true);
        scenarioReloaded();
    }

    @Override
    public void simplifyProfils(Collection<EMH> emhs) {
        Collection<EMH> sectionCasier = new ArrayList<>();
        for (EMH emh : emhs) {
            final Object subCatType = emh.getSubCatType();
            if (EnumSectionType.EMHSectionProfil.equals(subCatType) || EnumCasierType.EMHCasierProfil.equals(subCatType)) {
                sectionCasier.add(emh);
            }
        }
        if (sectionCasier.isEmpty()) {
            return;
        }
        Collection<EMH> toSimplify = cloneIfNeeded(sectionCasier);
        SimplificationSeuilsChooser seuilsChooser = new SimplificationSeuilsChooser(getCcm());
        SimplificationSeuils choose = seuilsChooser.choose();
        if (choose == null) {
            return;
        }
        new SimplificationProcessor(getCcm(), choose).simpify(toSimplify);
        setModified(true);
    }

    @Override
    public void changeSousModeleParent(Collection<EMH> initEmhs, Long targetSousModelUid) {
        Collection<EMH> emhs = cloneIfNeeded(initEmhs);
        EMHSousModele target = (EMHSousModele) getCurrentScenario().getIdRegistry().getEmh(targetSousModelUid);
        EditionChangeSousModele change = new EditionChangeSousModele();
        MoveToSousModeleResult changeSousModeleParent = change.changeSousModeleParent(emhs, target);
        setModified(true);
        scenarioReloaded();
        //important
        if (changeSousModeleParent.getNbEMHsModified() > 0) {
            modellingScenarioModificationService.setGeoLocModified();
        }
        LogsDisplayer.displayBilan(changeSousModeleParent);
    }

    @Override
    protected void componentDeactivated() {
        super.componentDeactivated();
        ExplorerUtils.activateActions(em, false);
    }

    private EMHScenario getCurrentScenario() {
        return cloned == null ? getModellingService().getScenarioLoaded() : cloned;
    }

    @Override
    protected void setEditable(boolean b) {
    }

    @Override
    public void cancelModificationHandler() {
        if (cloned != null) {
            cloned = null;
            scenarioReloaded();
            setModified(false);
        }
    }

    @Override
    public void valideModificationHandler() {
        if (cloned != null) {
            EMHScenario newScenario = cloned;
            cloned = null;
            modellingScenarioModificationService.setScenarioModifiedMassive(newScenario);
            setModified(false);
        }
    }

    @Override
    protected void scenarioLoaded() {
        expandedNodesManager.saveState();
        final EMHScenario currentScenario = getCurrentScenario();
        em.setRootContext(ModellingNodeEMHFactory.createScenarioNode(currentScenario, modellingEMHVisibilityService.getContainerActivityVisibility()));
        expandedNodesManager.restoreState();
    }

    @Override
    protected void scenarioReloaded() {
        scenarioLoaded();
    }

    @Override
    public void scenarioUnloaded() {
        if (modellingScenarioModificationService.getModellingScenarioService().isReloading()) {
            return;
        }
        em.setRootContext(Node.EMPTY);
    }

    private class SelectedEMHLookupListener implements LookupListener {

        private boolean updating;

        @Override
        public void resultChanged(LookupEvent ev) {
            updateSelection();
        }

        private void updateSelection() {
            if (updating) {
                return;
            }
            Window activeWindow = KeyboardFocusManager.getCurrentKeyboardFocusManager().
                    getActiveWindow();
            if (activeWindow == WindowManager.getDefault().getMainWindow() && WindowManager.getDefault().getRegistry().getActivated() == ModellingNetworkTopComponent.this) {
                return;
            }
            if (modellingSelectedEMHService.isUpdating()) {
                return;
            }
            updating = true;
            Set<Long> selectedUid = new HashSet<>(resultatEMHsSelected.allInstances());
            selectEMHs(selectedUid);
            updating = false;
        }
    }

    /**
     * met à jour la selection global si cette selection change
     */
    private class SelectedNodeLookupListener implements LookupListener {

        @Override
        public void resultChanged(LookupEvent ev) {
            if (WindowManager.getDefault().getRegistry().getActivated() != ModellingNetworkTopComponent.this) {
                return;
            }
            Collection<? extends NodeEMHDefault> allItems = resultatNodesSelected.allInstances();
            Set<Long> selectedUid = new HashSet<>();
            for (NodeEMHDefault node : allItems) {
                EMH emh = node.getLookup().lookup(EMH.class);
                if (emh != null) {
                    selectedUid.add(emh.getUiId());
                }
            }
            modellingSelectedEMHService.setSelectedUids(selectedUid);
        }
    }

    protected void selectEMHs(Set<Long> selectedUid) {
        LinkedEMHHelper.selectEMHs(getExplorerManager(), selectedUid);
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    @Override
    protected void componentOpenedHandler() {
        if (resultatEMHsSelected == null) {
            selectedEMHLookupListener = new SelectedEMHLookupListener();
            resultatEMHsSelected = modellingSelectedEMHService.getLookup().lookupResult(Long.class);
            resultatEMHsSelected.addLookupListener(selectedEMHLookupListener);
        }
        if (resultatNodesSelected == null) {
            selectedNodeLookupListener = new SelectedNodeLookupListener();
            resultatNodesSelected = getLookup().lookupResult(NodeEMHDefault.class);
            resultatNodesSelected.addLookupListener(selectedNodeLookupListener);
        }
        if (resultatActivity == null) {
            activityLookupListener = lookupEvent -> activityChanged();
            resultatActivity = modellingEMHVisibilityService.getLookup().lookupResult(ContainerActivityVisibility.class);
            resultatActivity.addLookupListener(activityLookupListener);
        }
        expandedNodesManager.restoreState();
    }

    protected void activityChanged() {
        if (getCurrentScenario() != null && !modellingEMHVisibilityService.isReloading()) {
            scenarioReloaded();
            if (selectedEMHLookupListener != null) {
                selectedEMHLookupListener.updateSelection();
            }

        }
    }

    @Override
    public void componentClosedTemporarily() {
        expandedNodesManager.restoreState();
    }

    @Override
    protected void componentClosedDefinitlyHandler() {
        if (resultatEMHsSelected != null) {
            resultatEMHsSelected.removeLookupListener(selectedEMHLookupListener);
            resultatEMHsSelected = null;
        }
        if (resultatNodesSelected != null) {
            resultatNodesSelected.removeLookupListener(selectedNodeLookupListener);
            resultatNodesSelected = null;
        }
        if (resultatActivity != null) {
            resultatActivity.removeLookupListener(activityLookupListener);
            resultatActivity = null;
        }
    }

    void writeProperties(java.util.Properties p) {
        DialogHelper.writeProperties(outlineView, "outlineView", p);
    }

    void readProperties(java.util.Properties p) {
        DialogHelper.readProperties(outlineView, "outlineView", p);
    }
}
