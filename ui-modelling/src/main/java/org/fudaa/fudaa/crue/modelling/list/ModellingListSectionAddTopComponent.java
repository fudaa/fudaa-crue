package org.fudaa.fudaa.crue.modelling.list;

import gnu.trove.TDoubleArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.modelling.action.AddSectionInBranchePanelContentData;
import org.fudaa.fudaa.crue.modelling.edition.ListAjoutRelationSectionProcess;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
//@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.modelling//ModellingListNoeudTopComponent//EN",
//                     autostore = false)
@TopComponent.Description(preferredID = ModellingListSectionAddTopComponent.TOPCOMPONENT_ID, iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png", persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ModellingListSectionAddTopComponent.MODE, openAtStartup = false, position = 0)
public final class ModellingListSectionAddTopComponent extends AbstractModellingListAddTopComponent {
//attention le mode modelling-listAddNoeuds doit être déclaré dans le projet branding (layer.xml)

  public static final String MODE = "modelling-listAddSections";
  public static final String TOPCOMPONENT_ID = "ModellingListSectionAddTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;

  public ModellingListSectionAddTopComponent() {
    super();
    setName(NbBundle.getMessage(ModellingListSectionAddTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingListSectionAddTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueAjoutSections";
  }

  @Override
  protected void addEMHsFromClipboard() {
    addNodesFromClipboard(ListAjoutRelationSectionProcess.createNodes(perspectiveServiceModelling, getSousModele()));
  }

  private Collection<String> getExsistingName() {
    EMHScenario scenarioLoaded = getModellingService().getScenarioLoaded();
    Children children = getExplorerManager().getRootContext().getChildren();

    Collection<String> existingNames = TransformerHelper.toSetNom(scenarioLoaded.getIdRegistry().getEMHs(EnumCatEMH.SECTION));
    Node[] nodes = children.getNodes();
    for (Node node : nodes) {
      ListRelationSectionContent content = node.getLookup().lookup(ListRelationSectionContent.class);
      if (content != null && StringUtils.isNotEmpty(content.getNom())) {
        existingNames.add(content.getNom());
      }
    }
    return existingNames;
  }

  @Override
  protected void addEMH() {
    UniqueNomFinder nomFinder = new UniqueNomFinder();
    String newName = nomFinder.findNewName(getExsistingName(), CruePrefix.P_SECTION);
    ListRelationSectionContentAddNode.NamesContent brancheSectionNames = ListAjoutRelationSectionProcess.getAvailableData(getSousModele());
    Children children = getExplorerManager().getRootContext().getChildren();
    children.add(new Node[]{new ListRelationSectionContentAddNode(new ListRelationSectionContent(newName), perspectiveServiceModelling,
      brancheSectionNames)});
  }

  @Override
  protected void installPropertyColumns() {
    outlineView.setPropertyColumns(
            ListRelationSectionContent.PROP_BRANCHE_NOM,
            ListRelationSectionContentNode.getBrancheDisplay(),
            ListCommonProperties.PROP_NOM,
            ListRelationSectionContentNode.getSectionNameDisplay(),
            ListRelationSectionContent.PROP_SECTION_TYPE_NOM,
            ListRelationSectionContentNode.getSectionTypeDisplay(),
            ListRelationSectionContent.PROP_REFERENCE_SECTION_IDEM,
            ListRelationSectionContentNode.getReferenceDisplay(),
            ListRelationSectionContent.PROP_DZ,
            ListRelationSectionContentNode.getDzDisplay(),
            ListRelationSectionContent.PROP_ABSCISSE_HYDRAULIQUE,
            ListRelationSectionContentNode.getAbsicsseHydrauliqueDisplay(),
            ListCommonProperties.PROP_COMMENTAIRE,
            AbstractListContentNode.getCommentDisplay());
  }

  @Override
  protected void doValidationModification() {
    Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    ListAjoutRelationSectionProcess process = new ListAjoutRelationSectionProcess();
    EMHSousModele ssModele = (EMHSousModele) getModellingService().getScenarioLoaded().getIdRegistry().getEmh(getSousModeleUid());
    CtuluLog res = process.processAjout(nodes, ssModele);
    if (res.isEmpty()) {
      clearView();
      setModified(false);
    } else {
      LogsDisplayer.displayError(res, getDisplayName());
    }
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  private AddSectionInBranchePanelContentData data;

  public void addSectionsInBranche(AddSectionInBranchePanelContentData data) {
    if (!isOpened()) {
      this.data = data;
    } else {
      addSectionsInBranche(data.branche, data.distanceMax, data.typeSection);
    }

  }

  @Override
  protected void componentOpenedHandler() {
    super.componentOpenedHandler();
    if (data != null) {
      addSectionsInBranche(data.branche, data.distanceMax, data.typeSection);
      data = null;
    }
  }

  private void addSectionsInBranche(CatEMHBranche branche, double maxDistance, EnumSectionType typeSection) {
    TDoubleArrayList newXp = createNewXps(branche, maxDistance);
    ListRelationSectionContentAddNode[] nodes = createNewNodes(newXp, branche, typeSection);
    if (nodes.length > 0) {
      prepareRootContext();
      Children children = getExplorerManager().getRootContext().getChildren();
      children.add(nodes);
    }
  }

  private TDoubleArrayList createNewXps(CatEMHBranche branche, double maxDistance) {
    List<RelationEMHSectionDansBranche> listeSectionsSortedXP = branche.getListeSectionsSortedXP(getCcm());
    TDoubleArrayList newXp = new TDoubleArrayList();
    for (int i = 0; i < listeSectionsSortedXP.size() - 1; i++) {
      double x0 = listeSectionsSortedXP.get(i).getXp();
      double x1 = listeSectionsSortedXP.get(i + 1).getXp();
      final double distance = x1 - x0;
      if (maxDistance < (distance)) {
        final int nbProfilsToAdd = EMHHelper.getDistDiviseurForDistMax(maxDistance, distance);
        double distMaxCorrige = distance / nbProfilsToAdd;
        double xToAdd = x0 + distMaxCorrige;
        while (xToAdd < x1) {
          newXp.add(xToAdd);
          xToAdd += distMaxCorrige;
        }
      }
    }
    return newXp;
  }

  private ListRelationSectionContentAddNode[] createNewNodes(TDoubleArrayList newXp, CatEMHBranche branche, EnumSectionType typeSection) {
    ListRelationSectionContentAddNode.NamesContent brancheSectionNames = ListAjoutRelationSectionProcess.getAvailableData(getSousModele());
    int nb = newXp.size();
    ListRelationSectionContentAddNode[] nodes = new ListRelationSectionContentAddNode[nb];
    UniqueNomFinder nomFinder = new UniqueNomFinder();
    List<String> newName = nomFinder.findNewNames(getExsistingName(), CruePrefix.P_SECTION, nb);
    for (int i = 0; i < nb; i++) {
      ListRelationSectionContent newSection = new ListRelationSectionContent(newName.get(i));
      newSection.setAbscisseHydraulique(newXp.get(i));
      newSection.setBrancheNom(branche.getNom());
      ListRelationSectionContent.SectionData data = new ListRelationSectionContent.SectionData();
      data.setSectionType(typeSection);
      newSection.setSection(data);
      nodes[i] = new ListRelationSectionContentAddNode(newSection, perspectiveServiceModelling, brancheSectionNames);
    }
    return nodes;
  }
}
