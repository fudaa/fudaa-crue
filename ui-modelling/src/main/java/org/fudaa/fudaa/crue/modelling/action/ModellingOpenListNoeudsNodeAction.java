package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.list.ModellingListNoeudTopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public abstract class ModellingOpenListNoeudsNodeAction extends AbstractModellingOpenListTopNodeAction {

  public ModellingOpenListNoeudsNodeAction(String name) {
    super(name, ModellingListNoeudTopComponent.MODE, ModellingListNoeudTopComponent.TOPCOMPONENT_ID);
  }

  public static void open(EMHSousModele sousModele) {
    if (sousModele == null) {
      return;
    }
    ModellingOpenListNoeudsNodeAction action = SystemAction.get(ModellingOpenListNoeudsNodeAction.Reopen.class);
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(sousModele));
    action.performAction(new Node[]{node});
  }

  public static class Reopen extends ModellingOpenListNoeudsNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenListNoeudsNodeAction.class, "ModellingListNoeudNodeAction.Name"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenListNoeudsNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenListNoeudsNodeAction.class, "ModellingListNoeudNodeAction.NewFrame.Name"));
      setReopen(false);
    }
  }
}
