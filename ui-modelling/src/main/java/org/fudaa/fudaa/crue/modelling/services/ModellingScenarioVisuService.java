package org.fudaa.fudaa.crue.modelling.services;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.modelling.emh.ModellingOpenEMHPlanimetryAdapter;
import org.fudaa.fudaa.crue.modelling.loi.SimplificationSeuilsChooser;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.common.ContainerActivityVisibility;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.layout.NetworkBuilder;
import org.fudaa.fudaa.crue.planimetry.layout.NetworkGisPositionnerResult;
import org.fudaa.fudaa.crue.planimetry.save.VisuLoadingResult;
import org.fudaa.fudaa.crue.planimetry.services.AdditionalLayerSaveStateListener;
import org.fudaa.fudaa.crue.planimetry.services.AdditionalLayersSaveServices;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.util.*;
import org.openide.util.Lookup.Result;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ServiceProvider;

import javax.swing.*;
import java.awt.*;

/**
 * Service gérant la vue planimetrie du scenario.
 * <table  border="1"  cellpadding="3" cellspacing="0"  summary="lookups">
 * <tr>
 * <th align="left">Lookup</th>
 * <th align="left">Commentaire</th>
 * <th align="left">Méthode utilisant le lookup</th>
 * </tr>
 * <tr>
 * <td>{@link PlanimetryVisuPanel}</td>
 * <td>Le conteneur pour l'affichage de la planimetrie.</td>
 * <td><code>{@link #getPlanimetryVisuPanel()} }</code></td>
 * </tr>
 * </table>
 *
 * @author deniger
 */
@ServiceProvider(service = ModellingScenarioVisuService.class)
public class ModellingScenarioVisuService implements LookupListener, Lookup.Provider, AdditionalLayerSaveStateListener {
    @SuppressWarnings("FieldCanBeLocal")
    private final Result<EMHScenario> resultat;
    private final Result<ContainerActivityVisibility> resultatActivity;
    protected final InstanceContent dynamicContent = new InstanceContent();
    protected final Lookup lookup = new AbstractLookup(dynamicContent);
    final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);
    final ModellingEMHVisibilityService modellingEMHVisibilityService = Lookup.getDefault().lookup(ModellingEMHVisibilityService.class);
    /**
     * Le gestionnaires des calques additionnels
     */
    final AdditionalLayersSaveServices additionalLayersSaveServices = Lookup.getDefault().lookup(AdditionalLayersSaveServices.class);

    public ModellingScenarioVisuService() {
        resultat = modellingScenarioService.getLookup().lookupResult(
                EMHScenario.class);
        resultat.addLookupListener(this);
        resultatActivity = modellingEMHVisibilityService.getLookup().lookupResult(
                ContainerActivityVisibility.class);
        resultatActivity.addLookupListener(lookupEvent -> activityChanged());
    }

    /**
     * @return le service principal de gestion des scenarios
     */
    public ModellingScenarioService getModellingScenarioService() {
        return modellingScenarioService;
    }

    /**
     * Si modifié par une perspective que Modelling, doit recharger les calques additionnels.
     *
     * @param source     la source de l'evenement: soit {@link #FROM_MODELLING} ou soit {@link #FROM_REPORT}
     * @param controller le controller à l'origine de l'evenement
     */
    @Override
    public void additionalLayerSavedBy(String source, PlanimetryController controller) {
        if (getPlanimetryVisuPanel() != null && !AdditionalLayerSaveStateListener.FROM_MODELLING.equals(source)) {
            getPlanimetryVisuPanel().getPlanimetryController().reloadAdditionLayersFrom(controller);
        }
    }

    @Override
    public void resultChanged(LookupEvent ev) {
        //changement massif: on ne fait rien
        if (modellingScenarioService.isReloading()) {
            return;
        }
        if (modellingScenarioService.isScenarioLoaded()) {
            loadScenario();
        } else {
            unloadScenario();
        }
    }

    @Override
    public Lookup getLookup() {
        return lookup;
    }

    /**
     * la visibilité a été modifiée
     */
    protected void activityChanged() {
        if (!modellingEMHVisibilityService.isReloading()) {
            PlanimetryVisuPanel planimetryVisuPanel = getPlanimetryVisuPanel();
            if (planimetryVisuPanel != null) {
                planimetryVisuPanel.getPlanimetryController().setContainerActivityVisibility(modellingEMHVisibilityService.getContainerActivityVisibility());
            }
        }
    }


    /**
     * Le scenario est déchargé-> enleve les listener les données du lookup de ce service
     */
    private void unloadScenario() {
        PlanimetryVisuPanel old = getPlanimetryVisuPanel();
        if (old != null) {
            dynamicContent.remove(old);
        }
        additionalLayersSaveServices.removeListener(this);
    }

    private void loadScenario() {
        unloadScenario();
        boolean dirOfConfigDefined = modellingScenarioService.getSelectedProjet().getInfos().isDirOfConfigDefined();
        SwingWorker loader = new VisuPanelLoader(modellingScenarioService.getScenarioLoaded());
        loader.execute();
        if (!dirOfConfigDefined) {
            EventQueue.invokeLater(() -> DialogHelper.showWarn(NbBundle.getMessage(ModellingScenarioVisuService.class,
                    "Visu.ConfigPathNotSet")));
        }
    }

    /**
     * @return la vue planimétrique
     */
    public PlanimetryVisuPanel getPlanimetryVisuPanel() {
        return lookup.lookup(PlanimetryVisuPanel.class);
    }


    /**
     * Le loader.
     */
    private class VisuPanelLoader extends SwingWorker<VisuLoadingResult, Object> {
        final ProgressHandle ph = ProgressHandleFactory.createHandle(NbBundle.getMessage(ModellingScenarioVisuService.class,
                "gisDataLoad.Task.Name"));
        private final EMHScenario scenarioLoaded;

        public VisuPanelLoader(EMHScenario scenarioLoaded) {
            this.scenarioLoaded = scenarioLoaded;
        }

        @Override
        protected VisuLoadingResult doInBackground() throws Exception {
            ph.start();
            ph.switchToIndeterminate();
            final EMHProjet selectedProjet = modellingScenarioService.getSelectedProjet();
            final EMHScenario scenario = scenarioLoaded;

            return VisuLoadingResult.loadVisuConfig(selectedProjet, scenario);
        }

        @Override
        protected void done() {
            try {
                VisuLoadingResult result = get();
                final NetworkGisPositionnerResult networkGisPositionnerResult = result.networkGisPositionnerResult;
                NetworkBuilder builder = new NetworkBuilder();
                PlanimetryVisuPanel panel = builder.loadCalqueFrom(CtuluUIForNetbeans.DEFAULT, scenarioLoaded, networkGisPositionnerResult,
                        networkGisPositionnerResult.getVisuConfiguration(),
                        networkGisPositionnerResult.getCcm(), true);
                panel.getPlanimetryController().apply(networkGisPositionnerResult.getLayerVisibility());
                panel.getPlanimetryController().setContainerActivityVisibility(modellingEMHVisibilityService.getContainerActivityVisibility());
                panel.getPlanimetryController().setEmhEditor(new ModellingOpenEMHPlanimetryAdapter());
                ModellingConfigService configService = Lookup.getDefault().lookup(ModellingConfigService.class);
                panel.getPlanimetryController().setDefaultValuesProvider(configService);
                panel.getPlanimetryController().setSimplificationSeuilsProvider(new SimplificationSeuilsChooser(networkGisPositionnerResult.getCcm()));
                panel.getPlanimetryController().setEmhEditor(new ModellingOpenEMHPlanimetryAdapter());
                if (CollectionUtils.isNotEmpty(result.additionnalLayer)) {
                    panel.getPlanimetryController().initWithAdditionnalLayers(result.additionnalLayer);
                }
                dynamicContent.add(panel);
                additionalLayersSaveServices.removeListener(ModellingScenarioVisuService.this);
                if (result.log != null && result.log.containsErrorOrSevereError()) {
                    LogsDisplayer.displayError(result.log, NbBundle.getMessage(ModellingScenarioVisuService.class,
                            "gisDataLoad.Task.Name"));
                }
                //les donnes sig peuvent etre mises lors du chargement. Pour l'instant c'est pour corriger un bug avec les multi-modele
                // voir CRUE-811
                if (networkGisPositionnerResult.isModifiedWhileLoading()) {
                    final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
                            ModellingScenarioModificationService.class);
                    DialogHelper.showWarn(NbBundle.getMessage(ModellingScenarioVisuService.class, "GisAutoModifiedAtLoading.Message"));
                    modellingScenarioModificationService.setGeoLocModified();

                }
            } catch (Exception exception) {
                Exceptions.printStackTrace(exception);
            } finally {
                ph.finish();
            }
        }
    }
}
