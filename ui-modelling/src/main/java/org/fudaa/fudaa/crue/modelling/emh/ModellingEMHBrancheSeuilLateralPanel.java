package org.fudaa.fudaa.crue.modelling.emh;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.edition.bean.BrancheEditionContent;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.fudaa.crue.common.helper.ToStringInternationalizableCellRenderer;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.common.view.DefaultOutlineViewEditor;
import org.fudaa.fudaa.crue.common.view.NodeEditionHelper;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 * Branche Type 4
 *
 * @author fred
 */
public class ModellingEMHBrancheSeuilLateralPanel extends JPanel implements ModellingEMHBrancheSpecificEditor {

  final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(
          ModellingScenarioService.class);
  final PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
  final ModellingEMHBrancheTopComponent parent;
  final JComboBox cbFormule;
  final SeuilView seuilView;

  ModellingEMHBrancheSeuilLateralPanel(CatEMHBranche branche, ModellingEMHBrancheTopComponent parent) {
    super(new BorderLayout(10, 10));
    this.parent = parent;
    JPanel top = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
    top.add(new JLabel(org.openide.util.NbBundle.getMessage(ModellingEMHBrancheSeuilLateralPanel.class, "FormulePdc.DisplayName")));
    cbFormule = new JComboBox(EnumFormulePdc.values());
    cbFormule.setRenderer(new ToStringInternationalizableCellRenderer());
    DonCalcSansPrtBrancheSeuilLateral dcsp = EMHHelper.selectFirstOfClass(branche.getInfosEMH(), DonCalcSansPrtBrancheSeuilLateral.class);
    if (dcsp != null) {
      cbFormule.setSelectedItem(dcsp.getFormulePdc());
    }
    top.add(cbFormule);
    cbFormule.addActionListener(parent.modifiedActionListener);
    add(top, BorderLayout.NORTH);
    seuilView = new SeuilView();
    seuilView.initWith(branche);
    add(seuilView);
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return null;
  }

  @Override
  public void setEditable(boolean editable) {
    cbFormule.setEnabled(editable);
    seuilView.setEditable(editable);
  }

  @Override
  public void fillWithData(BrancheEditionContent content) {
    DonCalcSansPrtBrancheSeuilLateral dcsp = new DonCalcSansPrtBrancheSeuilLateral();
    dcsp.setFormulePdc((EnumFormulePdc) cbFormule.getSelectedItem());
    dcsp.setElemSeuilAvecPdc(seuilView.getElemSeuilAvecPdc());
    content.setDcsp(dcsp);
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  private class SeuilView extends DefaultOutlineViewEditor {

    protected void initWith(CatEMHBranche branche) {
      DonCalcSansPrtBrancheSeuilLateral dcsp = EMHHelper.selectFirstOfClass(branche.getInfosEMH(), DonCalcSansPrtBrancheSeuilLateral.class);
      List<ElemSeuilAvecPdcNode> nodes = new ArrayList<>();
      if (dcsp != null) {
        List<ElemSeuilAvecPdc> elemSeuilAvecPdcList = dcsp.getElemSeuilAvecPdc();
        CrueConfigMetier ccm = getCcm();
        for (ElemSeuilAvecPdc elemSeuilAvecPdc : elemSeuilAvecPdcList) {
          nodes.add(new ElemSeuilAvecPdcNode(elemSeuilAvecPdc.clone(), ccm, perspectiveServiceModelling));
        }
      }
      getExplorerManager().setRootContext(NodeHelper.createNode(nodes, "ElemSeuilAvecPdc", false));
      DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
    }

    @Override
    protected void changeInNode(String propertyName) {
      parent.setModified(true);
    }

    List<ElemSeuilAvecPdc> getElemSeuilAvecPdc() {
      List<ElemSeuilAvecPdc> res = new ArrayList<>();
      Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
      for (Node node : nodes) {
        ElemSeuilAvecPdc elemSeuil = node.getLookup().lookup(ElemSeuilAvecPdc.class);
        res.add(elemSeuil);
      }
      return res;
    }

    @Override
    protected JButton createPasteComponent() {
      JButton button = new JButton(NbBundle.getMessage(ModellingEMHBrancheSeuilTransversalPanel.class, "Paste.DisplayName"));
      button.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          paste();
        }
      });
      return button;
    }

    protected void paste() {
      List<ElemSeuilAvecPdcNode> pastData = ModellingEMHBrancheSeuilTransversalPanel.getPastData(getCcm(), perspectiveServiceModelling);
      if (!pastData.isEmpty()) {
        NodeEditionHelper.addNodesFromClipboard(getExplorerManager(), pastData);
      }
    }

    @Override
    protected void addElement() {
      ElemSeuilAvecPdc elem = new ElemSeuilAvecPdc(getCcm());
      Children children = getExplorerManager().getRootContext().getChildren();
      final ElemSeuilAvecPdcNode elemPdtNode = new ElemSeuilAvecPdcNode(elem, getCcm(), perspectiveServiceModelling);
      children.add(new Node[]{elemPdtNode});
      DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
    }

    private CrueConfigMetier getCcm() {
      return modellingScenarioService.getSelectedProjet().getPropDefinition();
    }

    @Override
    protected void createOutlineView() {
      view = new OutlineView(StringUtils.EMPTY);
      view.setPropertyColumns(CrueConfigMetierConstants.PROP_ZSEUIL,
              ElemSeuilAvecPdcNode.getZSeuilDisplayName(),
              CrueConfigMetierConstants.PROP_LARGEUR,
              ElemSeuilAvecPdcNode.getLargeurDisplayName(),
              CrueConfigMetierConstants.PROP_COEF_D,
              ElemSeuilAvecPdcNode.getCoefDDisplayName(),
              CrueConfigMetierConstants.PROP_COEFPDC,
              ElemSeuilAvecPdcNode.getCoefPdcDisplayName());
      view.getOutline().setColumnHidingAllowed(false);
      view.getOutline().setRootVisible(false);
      view.setDragSource(true);
      view.setDropTarget(true);
    }
  }
}
