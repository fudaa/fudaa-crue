/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling;

import javax.swing.JPopupMenu;
import org.openide.nodes.Node;

/**
 *
 * @author Frederic Deniger
 */
public interface ModellingTopComponentWithSpecificAction {

  /**
   *
   * @param selectedNode vaut null si pas de sélection ou plus de 1.
   * @param popupMenu la popupmenu de destination
   */
  void addCustomAction(Node selectedNode, JPopupMenu popupMenu);

  boolean useQuickFilterActions(int columnIdx);
}
