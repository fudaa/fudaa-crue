/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.list;

import gnu.trove.TLongObjectHashMap;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.edition.bean.ListCasierContent;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.modelling.action.ModellingOpenListCasiersAddNodeAction;
import org.fudaa.fudaa.crue.modelling.edition.ListCasierModificationProcess;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Top component which displays something.
 */
//@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.modelling//ModellingLisCasierTopComponent//EN",
//                     autostore = false)
@TopComponent.Description(preferredID = ModellingListCasierTopComponent.TOPCOMPONENT_ID,
iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png",
persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ModellingListCasierTopComponent.MODE, openAtStartup = false, position = 0)
public final class ModellingListCasierTopComponent extends AbstractModellingListEditionTopComponent {
  //attention le mode modelling-listCasiers doit être déclaré dans le projet branding (layer.xml)

  public static final String MODE = "modelling-listCasiers";
  public static final String TOPCOMPONENT_ID = "ModellingListCasierTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  private JButton btAddCasiers;

  public ModellingListCasierTopComponent() {
    initComponents();
    setName(NbBundle.getMessage(ModellingListCasierTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingListCasierTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    createComponent(AbstractListContentNode.getOrdreDisplay());
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueListeCasiers";
  }

  @Override
  protected JPanel buildNorthPanel() {
    assert btAddCasiers == null;//c'est normalement appele que dans le constructeur:
    btAddCasiers = new JButton(ModellingOpenListCasiersAddNodeAction.getAddCasierActionName());
    btAddCasiers.addActionListener(e -> openAddNodeTopComponent());
    JPanel top = super.buildNorthPanel();
    JPanel bt = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    bt.add(btAddCasiers);

    JPanel res = new JPanel(new BorderLayout());
    res.add(top);
    res.add(bt, BorderLayout.SOUTH);
    return res;
  }

  private void openAddNodeTopComponent() {
    ModellingOpenListCasiersAddNodeAction.open(getSousModele());
  }

  @Override
  protected void installPropertyColumns() {
    outlineView.setPropertyColumns(ListCommonProperties.PROP_NOM,
            ListCasierContentNode.getCasierNameDisplay(),
            ListCommonProperties.PROP_TYPE,
            AbstractListContentNode.getTypeDisplay(),
            ListCasierContent.PROP_NOEUD,
            ListCasierContentNode.getCasierNoeudDisplayName(),
            ListCommonProperties.PROP_ACTIVE,
            AbstractListContentNode.getActiveDisplay(),
            ListCommonProperties.PROP_COMMENTAIRE,
            AbstractListContentNode.getCommentDisplay());
  }

  @Override
  public void valideModificationHandler() {
    Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    TLongObjectHashMap<ListCasierModificationProcess.CasierData> dataByUid = new TLongObjectHashMap<>();
    List<CatEMHCasier> casiers = new ArrayList<>();
    for (Node node : nodes) {
      CatEMHCasier casier = node.getLookup().lookup(CatEMHCasier.class);
      ListCasierContent casierContent = node.getLookup().lookup(ListCasierContent.class);
      ListCasierModificationProcess.CasierData data = new ListCasierModificationProcess.CasierData();
      data.active = casierContent.isActive();
      data.commentaire = casierContent.getCommentaire();
      data.casierName = casierContent.getNom();
      data.noeudName = casierContent.getNoeud();
      assert casier != null;
      dataByUid.put(casier.getUiId(), data);
      casiers.add(casier);
    }
    Pair<CtuluLog, Boolean> res = new ListCasierModificationProcess().applyCasier((EMHSousModele) getEMHInLookup(), casiers,
            dataByUid, getCcm());
    boolean isOk = res.second;
    if (res.first.containsErrorOrSevereError()) {
      LogsDisplayer.displayError(res.first, getName());
    }
    setModified(!isOk);

  }

  @Override
  protected List<? extends Node> createNodes(EMH in) {
    ListCasierContentFactory factory = new ListCasierContentFactory();
    return factory.createNodes(((EMHSousModele) in).getCasiers());
  }
  @Override
  protected void scenarioReloaded() {
    scenarioLoaded();
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
