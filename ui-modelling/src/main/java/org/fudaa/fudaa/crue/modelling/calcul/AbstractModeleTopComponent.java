package org.fudaa.fudaa.crue.modelling.calcul;

import com.memoire.bu.BuGridLayout;
import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.modelling.AbstractModellingTopComponent;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frédéric Deniger
 */
public abstract class AbstractModeleTopComponent extends AbstractModellingTopComponent {

  JTextField jContainerName = new JTextField();
  private long modeleUid;
  protected final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);

  private Long getModeleUid() {
    return modeleUid;
  }

  protected static void restoreTabbedPaneSelection(JComponent oldCenter, JTabbedPane tabbedPane) {
    if (oldCenter != null) {
      JTabbedPane old = (JTabbedPane) oldCenter;
      int selectedIndex = old.getSelectedIndex();
      tabbedPane.setSelectedIndex(selectedIndex);
    }
  }

  public void setModeleUid(Long modeleUid) {
    this.modeleUid = modeleUid;
  }

  @Override
  public void cancelModificationHandler() {
    scenarioReloaded();
    setModified(false);
  }

  public void reloadWithModeleUid(Long sousModeleUid) {
    if (!ObjectUtils.equals(this.modeleUid, sousModeleUid)) {
      this.modeleUid = sousModeleUid;
      scenarioReloaded();
    }
  }

  protected EMHModeleBase getModele(EMHScenario scenario) {
    return (EMHModeleBase) scenario.getIdRegistry().getEmh(getModeleUid());
  }

  protected EMHModeleBase getModele() {
    return (EMHModeleBase) modellingScenarioService.getScenarioLoaded().getIdRegistry().getEmh(getModeleUid());
  }

  protected JPanel buildNorthPanel() {
    JPanel pn = new JPanel(new BuGridLayout(2, 3, 3));
    pn.add(new JLabel(NbBundle.getMessage(AbstractModeleTopComponent.class, "Modele.Label")));
    jContainerName.setEditable(false);
    pn.setBorder(BorderFactory.createEmptyBorder(5, 2, 5, 1));
    pn.add(jContainerName);
    return pn;
  }

  /**
   * WARN: A utiliser uniquement pour les tests
   */
  public void testScenarioLoaded(EMHScenario scenario) {
    removeAll();
    buildComponents(scenario, null);
  }

  @Override
  protected void scenarioLoaded() {
    JComponent oldCenter = CtuluLibSwing.findChildByName(this, CENTER_NAME);
    removeAll();
    buildComponents(modellingScenarioModificationService.getModellingScenarioService().getScenarioLoaded(), oldCenter);

  }
  private static final String CENTER_NAME = "main";

  private void buildComponents(EMHScenario scenario, JComponent oldCenter) {
    add(buildNorthPanel(), BorderLayout.NORTH);
    EMH emh = scenario.getIdRegistry().getEmh(modeleUid);
    jContainerName.setText(emh.getNom());
    final JComponent centerPanel = buildCenterPanel(scenario, oldCenter);
    centerPanel.setName(CENTER_NAME);
    add(centerPanel);
    add(createCancelSaveButtons(), BorderLayout.SOUTH);
  }

  protected abstract JComponent buildCenterPanel(EMHScenario scenario, JComponent oldCenter);

  @Override
  protected void scenarioReloaded() {
    scenarioLoaded();
  }

  @Override
  protected void scenarioChanged(ScenarioModificationEvent event) {
    scenarioReloaded();
  }

  @Override
  protected void scenarioUnloaded() {
    removeAll();
    close();
  }
}
