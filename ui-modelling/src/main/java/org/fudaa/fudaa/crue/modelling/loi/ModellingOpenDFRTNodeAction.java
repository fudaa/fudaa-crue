package org.fudaa.fudaa.crue.modelling.loi;

import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.action.AbstractModellingOpenTopNodeAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;

/**
 *
 * @author Frédéric Deniger
 */
public class ModellingOpenDFRTNodeAction extends AbstractModellingOpenTopNodeAction<DFRTTopComponent> {

  public ModellingOpenDFRTNodeAction(String name) {
    super(name, DFRTTopComponent.MODE, DFRTTopComponent.TOPCOMPONENT_ID);
  }

  public ModellingOpenDFRTNodeAction() {
    super(NbBundle.getMessage(ModellingOpenDFRTNodeAction.class, "ModellingOpenDFRTNodeAction.DisplayName"), DFRTTopComponent.MODE, DFRTTopComponent.TOPCOMPONENT_ID);
  }

  public static void open(Long sousModeleUid, boolean reopen) {
    open(sousModeleUid, null, reopen);
  }

  public static void open(Long sousModeleUid, String frtName, boolean reopen) {
    if (sousModeleUid == null) {
      return;
    }
    ModellingOpenDFRTNodeAction action = null;
    if (reopen) {
      action = SystemAction.get(ModellingOpenDFRTNodeAction.Reopen.class);
    } else {
      action = SystemAction.get(ModellingOpenDFRTNodeAction.NewFrame.class);
    }
    AbstractNode node = new AbstractNode(Children.LEAF, frtName == null ? Lookups.fixed(sousModeleUid) : Lookups.fixed(sousModeleUid, frtName));
    action.performAction(new Node[]{node});
  }

  @Override
  protected void activeWithValue(DFRTTopComponent opened, Node selectedNode) {
    Long uid = selectedNode.getLookup().lookup(Long.class);
    if (uid == null) {
      EMHSousModele sousModele = selectedNode.getLookup().lookup(EMHSousModele.class);
      if (sousModele != null) {
        uid = sousModele.getUiId();
      }
    }
    String frtName = selectedNode.getLookup().lookup(String.class);
    if (uid != null) {
      opened.reloadSousModeleUid(uid, frtName);
    }
  }

  @Override
  protected TopComponent activateNewTopComponent(DFRTTopComponent top, Node selectedNode) {
    Long uid = selectedNode.getLookup().lookup(Long.class);
    if (uid == null) {
      EMHSousModele sousModele = selectedNode.getLookup().lookup(EMHSousModele.class);
      if (sousModele != null) {
        uid = sousModele.getUiId();
      }
    }
    if (uid == null) {
      return null;
    }
    top.setSousModeleUid(uid, selectedNode.getLookup().lookup(String.class));
    return top;
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return true;
  }

  public static class Reopen extends ModellingOpenDFRTNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenDFRTNodeAction.class, "ModellingOpenDFRTNodeAction.DisplayName"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenDFRTNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenDFRTNodeAction.class, "ModellingOpenDFRTNodeAction.NewFrame.DisplayName"));
      setReopen(false);
    }
  }
}
