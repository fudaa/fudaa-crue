package org.fudaa.fudaa.crue.modelling.list;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.MissingResourceException;
import javax.swing.Action;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.fudaa.crue.common.property.*;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class ListRelationSectionContentNode extends AbstractListContentNode {

  final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);

  // La section associée à ce ContentNode
  final CatEMHSection section;

  public CatEMHSection getSection() {
    return section;
  }

  public ListRelationSectionContentNode(CatEMHSection section, ListRelationSectionContent content,
          List<String> brancheAcceptingNewSection, PerspectiveServiceModelling perspectiveServiceModelling) {
    super(Children.LEAF, Lookups.fixed(section, content, brancheAcceptingNewSection), perspectiveServiceModelling);
    this.section = section;
  }

  @Override
  public Action[] getActions(boolean context) {
    return new Action[0];
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    final ListRelationSectionContent relationSectionContent = getLookup().lookup(ListRelationSectionContent.class);
    final PropertySupportReflection brancheNom = PropertySupportReflection.createString(this, relationSectionContent,
            ListRelationSectionContent.PROP_BRANCHE_NOM,
            ListRelationSectionContent.PROP_BRANCHE_NOM,
            ListRelationSectionContentNode.getBrancheDisplay());
    final PropertyChangeListenerForBrancheName propertyChangeListener = new PropertyChangeListenerForBrancheName(brancheNom);
    addPropertyChangeListener(propertyChangeListener);
    propertyChangeListener.applyChanges();
    PropertyCrueUtils.configureNoCustomEditor(brancheNom);
    set.put(brancheNom);

    Node.Property nomSection = new PropertySupportSection(this, relationSectionContent);
    set.put(nomSection);
    //noeud amont
    Node.Property typeSection = new PropertySupportRead<ListRelationSectionContent, String>(relationSectionContent, String.class,
            ListRelationSectionContent.PROP_SECTION_TYPE_NOM,
            getSectionTypeDisplay()) {
      @Override
      public String getValue() throws IllegalAccessException, InvocationTargetException {
        return getInstance().getSection().getSectionType().geti18n();
      }
    };
    PropertyCrueUtils.configureNoCustomEditor(typeSection);
    set.put(typeSection);
    //section idem reference

    Node.Property reference = new PropertySupportRead<ListRelationSectionContent, String>(relationSectionContent, String.class,
            ListRelationSectionContent.PROP_REFERENCE_SECTION_IDEM,
            getReferenceDisplay()) {
      @Override
      public String getValue() throws IllegalAccessException, InvocationTargetException {
        return getInstance().getSection().getReferenceSectionIdem();
      }
    };
    PropertyCrueUtils.configureNoCustomEditor(reference);
    set.put(reference);
    CrueConfigMetier propDefinition = modellingScenarioService.getSelectedProjet().getPropDefinition();
    final ItemVariable dzProperty = propDefinition.getProperty(CrueConfigMetierConstants.PROP_DZ);
    Node.Property dz = new PropertySupportRead<ListRelationSectionContent, String>(relationSectionContent, String.class,
            ListRelationSectionContent.PROP_DZ,
            getDzDisplay()) {
      @Override
      public String getValue() throws IllegalAccessException, InvocationTargetException {
        return dzProperty.format(getInstance().getSection().getDz(), DecimalFormatEpsilonEnum.COMPARISON);
      }
    };
    PropertyCrueUtils.configureNoCustomEditor(dz);
    set.put(dz);

    Node.Property profilSection = new PropertySupportRead<ListRelationSectionContent, String>(relationSectionContent, String.class,
            ListRelationSectionContent.PROP_PROFIL_SECTION,
            getProfilDisplay()) {
      @Override
      public String getValue() throws IllegalAccessException, InvocationTargetException {
        return getInstance().getSection().getProfilSection();
      }
    };
    PropertyCrueUtils.configureNoCustomEditor(profilSection);
    set.put(profilSection);

    ItemVariable xp = propDefinition.getProperty(CrueConfigMetierConstants.PROP_XP);
    PropertySupportReflection abscisseHydraulique = PropertySupportReflectionDouble.createDouble(DecimalFormatEpsilonEnum.COMPARISON, this,
            relationSectionContent,
            ListRelationSectionContent.PROP_ABSCISSE_HYDRAULIQUE,
            ListRelationSectionContent.PROP_ABSCISSE_HYDRAULIQUE, getAbsicsseHydrauliqueDisplay(), xp);

    PropertyCrueUtils.configureNoCustomEditor(abscisseHydraulique);
    set.put(abscisseHydraulique);
    if (StringUtils.isNotBlank(relationSectionContent.getBrancheNom())) {

      Node.Property abscisseSchematique = new PropertyStringReadOnly(relationSectionContent.getAbscisseSchematique(),
              ListRelationSectionContent.PROP_ABSCISSE_SCHEMATIQUE,
              getAbscisseSchematiqueDisplay(),
              null);
      PropertyCrueUtils.configureNoCustomEditor(abscisseSchematique);
      set.put(abscisseSchematique);
    }

    //commentaire
    Node.Property commentaire = new PropertySupportReadWrite<ListRelationSectionContent, String>(this, relationSectionContent, String.class,
            ListCommonProperties.PROP_COMMENTAIRE,
            AbstractListContentNode.getCommentDisplay()) {
      @Override
      protected void setValueInInstance(String newVal) {
        getInstance().getSection().setCommentaire(newVal);
      }

      @Override
      public String getValue() throws IllegalAccessException, InvocationTargetException {
        return getInstance().getSection().getCommentaire();
      }
    };
    PropertyCrueUtils.setNullValueEmpty(commentaire);
    set.put(commentaire);
    sheet.put(set);
    return sheet;
  }

  public static String getAbscisseSchematiqueDisplay() throws MissingResourceException {
    return NbBundle.getMessage(ListRelationSectionContentNode.class,
            "AbscisseSchematique.Name");
  }

  public static String getAbsicsseHydrauliqueDisplay() throws MissingResourceException {
    return NbBundle.getMessage(ListRelationSectionContentNode.class,
            "AbscisseHydraulique.Name");
  }

  public static String getProfilDisplay() throws MissingResourceException {
    return NbBundle.getMessage(ListRelationSectionContentNode.class,
            "ProfilSection.Name");
  }

  public static String getDzDisplay() throws MissingResourceException {
    return NbBundle.getMessage(ListRelationSectionContentNode.class, "Dz.Name");
  }

  public static String getReferenceDisplay() throws MissingResourceException {
    return NbBundle.getMessage(ListRelationSectionContentNode.class, "Reference.Name");
  }

  public static String getSectionTypeDisplay() throws MissingResourceException {
    return NbBundle.getMessage(ListRelationSectionContentNode.class, "SectionType.Name");
  }

  public static String getSectionNameDisplay() throws MissingResourceException {
    return NbBundle.getMessage(ListRelationSectionContentNode.class, "Section.Name");
  }

  public static String getBrancheDisplay() throws MissingResourceException {
    return NbBundle.getMessage(ListRelationSectionContentNode.class, "BrancheName.Name");
  }

  private class PropertyChangeListenerForBrancheName implements PropertyChangeListener {

    private final PropertySupportReflection brancheNom;

    public PropertyChangeListenerForBrancheName(PropertySupportReflection brancheNom) {
      this.brancheNom = brancheNom;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if (ListRelationSectionContent.PROP_BRANCHE_NOM.equals(evt.getPropertyName()) || ListCommonProperties.PROP_NOM.equals(evt.getPropertyName())) {
        applyChanges();
      }
    }

    public void applyChanges() {
      try {
        brancheNom.setTags(getLookup().lookup(List.class));
        if (StringUtils.isBlank((String) brancheNom.getValue())) {
          brancheNom.setCanWrite(true);
        } else {
          brancheNom.setTags(new String[]{(String) brancheNom.getValue()});
          brancheNom.setCanWrite(false);
        }
      } catch (Exception exception) {
      }
    }
  }
}
