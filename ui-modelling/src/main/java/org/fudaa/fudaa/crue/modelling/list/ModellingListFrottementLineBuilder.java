package org.fudaa.fudaa.crue.modelling.list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameSansPrefixComparator;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.EMHSectionProfil;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.LitNumerote;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.metier.helper.LitNommeIndexed;
import org.fudaa.dodico.crue.metier.helper.LitNumeroteIndex;
import org.fudaa.dodico.crue.validation.ValidateAndRebuildProfilSection;

/**
 *
 * @author Frederic Deniger
 */
public class ModellingListFrottementLineBuilder {

  private final EMHSousModele sousModele;
  private final CrueConfigMetier ccm;
  DonFrt[] frtStockageArray;
  DonFrt[] frtArray;
  private int maxNameSize = 0;

  public ModellingListFrottementLineBuilder(final EMHSousModele sousModele, final CrueConfigMetier ccm) {
    this.sousModele = sousModele;
    this.ccm = ccm;
  }

  public int getMaxNameSize() {
    return maxNameSize;
  }

  private void createFrts() {
    final List<DonFrt> frts = sousModele.getFrtConteneur().getListFrt();
    final List<DonFrt> frt = new ArrayList<>();
    final List<DonFrt> frtStockage = new ArrayList<>();
    for (final DonFrt donFrt : frts) {
      if (donFrt.getNom() != null) {
        maxNameSize = Math.max(maxNameSize, donFrt.getNom().length());
      }
      if (EnumTypeLoi.LoiZFKSto.equals(donFrt.getLoi().getType())) {
        frtStockage.add(donFrt);
      } else {
        frt.add(donFrt);
      }
    }
    Collections.sort(frt, ObjetNommeByNameSansPrefixComparator.INSTANCE);
    Collections.sort(frtStockage, ObjetNommeByNameSansPrefixComparator.INSTANCE);
    frtStockageArray = frtStockage.toArray(new DonFrt[0]);
    frtArray = frt.toArray(new DonFrt[0]);
  }

  public List<ModellingListFrottementLine> build() {
    createFrts();
    final List<ModellingListFrottementLine> res = new ArrayList<>();
    final List<CatEMHBranche> branches = sousModele.getBranches();
    final ValidateAndRebuildProfilSection rebuilder = new ValidateAndRebuildProfilSection(ccm, null);
    for (final CatEMHBranche branche : branches) {
      final List<EMHSectionProfil> emhSectionProfil = EMHHelper.getEMHSectionProfil(branche, ccm);
      for (final EMHSectionProfil section : emhSectionProfil) {
        final ModellingListFrottementLine line = new ModellingListFrottementLine(section);
        res.add(line);
        final List<LitNommeIndexed> litNommes = rebuilder.getAsIndexedData(DonPrtHelper.getProfilSection(section)).getMetier();
        for (final LitNommeIndexed litNommeIndexed : litNommes) {
          final ModellingListFrottementLine.LitNommeCell litNommeCell = new ModellingListFrottementLine.LitNommeCell(line);
          line.getLitNommes().add(litNommeCell);
          final int nb = litNommeIndexed.getLitNumerotesSize();
          for (int i = 0; i < nb; i++) {
            final LitNumeroteIndex litNumeroteIndex = litNommeIndexed.getLitNumeroteIndex(i);
            final LitNumerote litNumerote = litNumeroteIndex.getLitNumerote();
            
            final boolean litActif = litNumerote.getIsLitActif();
            // si le lit numéroté est de longueur null, on force le frottement FkSto_K0
            final DonFrt[] acceptedFrt = (litActif && ! litNumerote.isLongueurNulle()) ? frtArray : frtStockageArray;
            final ModellingListFrottementLine.LitNumeroteCell litNumeroteCell = new ModellingListFrottementLine.LitNumeroteCell(litNumerote, acceptedFrt, line);

            litNommeCell.getLitNumerotes().add(litNumeroteCell);
          }
        }
      }
    }
    return res;
  }
}
