package org.fudaa.fudaa.crue.modelling.action;

import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class DeleteEMHUniqueNodeAction extends DeleteEMHNodeAction {

  public DeleteEMHUniqueNodeAction() {
    super(false, NbBundle.getMessage(DeleteEMHNodeAction.class, "DeleteEMHAction.Name"));
  }

}
