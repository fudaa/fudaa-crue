package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.list.ModellingListBrancheTopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public abstract class ModellingOpenListBranchesNodeAction extends AbstractModellingOpenListTopNodeAction {

  public ModellingOpenListBranchesNodeAction(String name) {
    super(name, ModellingListBrancheTopComponent.MODE, ModellingListBrancheTopComponent.TOPCOMPONENT_ID);
  }

  public static void open(EMHSousModele sousModele) {
    if (sousModele == null) {
      return;
    }
    ModellingOpenListBranchesNodeAction action = SystemAction.get(ModellingOpenListBranchesNodeAction.Reopen.class);
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(sousModele));
    action.performAction(new Node[]{node});
  }

  public static class Reopen extends ModellingOpenListBranchesNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ModellingOpenListBranchesNodeAction.class, "ModellingListBrancheNodeAction.Name"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ModellingOpenListBranchesNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ModellingOpenListBranchesNodeAction.class, "ModellingListBrancheNodeAction.NewFrame.Name"));
      setReopen(false);
    }
  }
}
