package org.fudaa.fudaa.crue.modelling.list;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.edition.bean.ListCasierContent;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.util.Lookup;

/**
 *
 * @author deniger
 */
public class ListCasierContentFactory {

  final PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
  ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);

  public List<ListCasierContentNode> createNodes(List<CatEMHCasier> casiers) {
    List<ListCasierContentNode> res = new ArrayList<>();
    int ordre = 0;
    for (CatEMHCasier casier : casiers) {
      ListCasierContent casierContent = new ListCasierContent(casier.getNom());
      casierContent.setType(casier.getCasierType());
      casierContent.setActive(casier.getUserActive());
      casierContent.setCommentaire(casier.getCommentaire());
      casierContent.setNoeud(casier.getNoeud() == null ? StringUtils.EMPTY : casier.getNoeud().getNom());
      ListCasierContentNode node = new ListCasierContentNode(casier, casierContent, perspectiveServiceModelling);
      node.setDisplayName(Integer.toString(++ordre));
      res.add(node);
    }
    return res;
  }
}
