package org.fudaa.fudaa.crue.modelling.edition;

import com.Ostermiller.util.CSVParser;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizableTransformer;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.EditionCreateEMH;
import org.fudaa.dodico.crue.edition.bean.ListBrancheContent;
import org.fudaa.dodico.crue.edition.bean.ValidationMessage;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.fudaa.crue.modelling.list.AbstractListContentNode;
import org.fudaa.fudaa.crue.modelling.list.ListBrancheContentAddNode;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ModellingConfigService;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioModificationService;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.util.*;
import java.util.List;

/**
 * @author deniger
 */
public class ListAjoutBrancheProcess {
    protected final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
            ModellingScenarioModificationService.class);
    final ModellingConfigService modellingConfigService = Lookup.getDefault().lookup(ModellingConfigService.class);

    public static List<CatEMHNoeud> getAvailableNoeudsForBranches(EMHSousModele sousModele) {
        List<CatEMHNoeud> noeuds = sousModele.getNoeuds();
        Collections.sort(noeuds, ObjetNommeByNameComparator.INSTANCE);
        return noeuds;
    }

    public static List<String> getAvailableNoeudsNameForBranches(EMHSousModele sousModele) {
        List<CatEMHNoeud> availableNoeuds = getAvailableNoeudsForBranches(sousModele);
        return TransformerHelper.toNom(availableNoeuds);
    }

    public static List<ListBrancheContentAddNode> createNodes(PerspectiveServiceModelling perspectiveServiceModelling,
                                                              EMHSousModele sousModele) {
        try {
            String toCopy = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
            String[][] parse = CSVParser.parse(toCopy, '\t');
            List<String> noeuds = getAvailableNoeudsNameForBranches(sousModele);
            List<ListBrancheContentAddNode> contents = new ArrayList<>();
            List<EnumBrancheType> availablesBrancheType = EnumBrancheType.getAvailablesBrancheType();
            Map<String, EnumBrancheType> toi18nMap = ToStringInternationalizableTransformer.toi18nMap(availablesBrancheType);
            for (String[] strings : parse) {
                if (strings.length >= 1) {
                    ListBrancheContent newContent = new ListBrancheContent(strings[0]);
                    if (strings.length >= 2) {
                        newContent.setType(toi18nMap.get(strings[1]));
                        if (strings.length >= 3) {
                            newContent.setNoeudAmont(strings[2]);
                            if (strings.length >= 4) {
                                newContent.setNoeudAval(strings[3]);
                                if (strings.length >= 5) {
                                    newContent.setLongueur(Double.parseDouble(strings[4]));
                                    if (strings.length >= 6) {
                                        newContent.setCommentaire(strings[5]);
                                    }
                                }
                            }
                        }
                    }
                    contents.add(new ListBrancheContentAddNode(newContent, noeuds, perspectiveServiceModelling));
                }
            }
            return contents;
        } catch (Exception exception) {
        }
        return Collections.emptyList();
    }

    /**
     * @param newBranches liste de ListNoeudContentAddNode
     * @param sousModele  le sous-modele
     * @return le log de l'operation
     */
    public CtuluLog processAjout(Node[] newBranches, EMHSousModele sousModele) {
        Map<ListBrancheContent, ListBrancheContentAddNode> contents = getContentList(newBranches);
//    Collection<ListBrancheContentAddNode> values = contents.values();
        EditionCreateEMH createProcess = new EditionCreateEMH(modellingConfigService.getDefaultValues());
        CrueConfigMetier ccm = modellingScenarioModificationService.getModellingScenarioService().getSelectedProjet().getCoeurConfig().getCrueConfigMetier();
        final ValidationMessage<ListBrancheContent> createBrancheMessages = createProcess.createBranches(sousModele, contents.keySet(), ccm);
        Collection<ListBrancheContentAddNode> values = contents.values();
        AbstractListContentNode.reinitMessages(values);
        if (createBrancheMessages.accepted) {
            modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.EMH_CREATION));
        } else {
            List<ValidationMessage.ResultItem<ListBrancheContent>> msgs = createBrancheMessages.messages;
            for (ValidationMessage.ResultItem<ListBrancheContent> resultItem : msgs) {
                ListBrancheContentAddNode node = contents.get(resultItem.bean);
                node.setContainError(true);
                node.setShortDescription("<html><body>" + StringUtils.join(resultItem.messages, "<br>") + "</body></html>");
            }
        }
        return createBrancheMessages.log;
    }

    private LinkedHashMap<ListBrancheContent, ListBrancheContentAddNode> getContentList(Node[] newNoeuds) {
        LinkedHashMap<ListBrancheContent, ListBrancheContentAddNode> contents = new LinkedHashMap<>();
        for (Node node : newNoeuds) {
            ListBrancheContent content = node.getLookup().lookup(ListBrancheContent.class);
            if (content != null) {
                contents.put(content, (ListBrancheContentAddNode) node);
            }
        }
        return contents;
    }
}
