package org.fudaa.fudaa.crue.modelling.services;

import java.util.concurrent.Callable;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.save.ConfigSaverExternProcessor;
import org.fudaa.fudaa.crue.planimetry.services.AdditionalLayersSaveServices;
import org.openide.util.Lookup;

/**
 *
 * @author deniger
 */
public class ModellingSaveStudyConfigExternCallable implements Callable<CtuluLogGroup> {

  final ModellingScenarioVisuService modellingScenarioVisuService = Lookup.getDefault().lookup(ModellingScenarioVisuService.class);
  final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
          ModellingScenarioModificationService.class);
  final AdditionalLayersSaveServices additionalLayersSaveServices = Lookup.getDefault().lookup(AdditionalLayersSaveServices.class);

  @Override
  public CtuluLogGroup call() throws Exception {
    if (!modellingScenarioModificationService.isStudyExternConfigModified()) {
      return null;
    }
    ModellingScenarioService modellingScenarioService = modellingScenarioVisuService.getModellingScenarioService();
    final EMHProjet project = modellingScenarioService.getSelectedProjet();
    if (!project.getInfos().isDirOfConfigDefined()) {
      modellingScenarioModificationService.clearStudyExternConfigModifiedState();
      return null;
    }
    PlanimetryController planimetryController = modellingScenarioVisuService.getPlanimetryVisuPanel().getPlanimetryController();
    CtuluLogGroup gr = new ConfigSaverExternProcessor(project).saveAdditionalLayers(planimetryController);
    modellingScenarioModificationService.clearStudyExternConfigModifiedState();
    additionalLayersSaveServices.setAdditionalLayerSavedFromModelling(planimetryController);
    return gr;
  }
}
