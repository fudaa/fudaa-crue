package org.fudaa.fudaa.crue.modelling.loi;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuResource;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.gui.ExportTableCommentSupplier;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.time.ToStringTransformerDate;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.LoiFactory;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.dodico.crue.validation.ValidationHelperLoi;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.editor.LocalDateTimePanel;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.helper.ObjetWithIdCellRenderer;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.SysdocContrat;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.loi.common.SecondValueEditor;
import org.fudaa.fudaa.crue.loi.loiff.LoiUiController;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.fudaa.fudaa.crue.study.services.TableExportCommentSupplier;
import org.joda.time.LocalDateTime;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.*;
import java.util.List;

/**
 * Editeur de lois.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.loi//DLHYTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = DLHYTopComponent.TOPCOMPONENT_ID, persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = DLHYTopComponent.MODE, openAtStartup = false)
public final class DLHYTopComponent extends AbstractModellingLoiTopComponent implements ItemListener, ExportTableCommentSupplier {
    public static final String MODE = "loi-dlhyEditor"; //NOI18N
    public static final String TOPCOMPONENT_ID = "DLHYTopComponent"; //NOI18N
    public static final String PROPERTY_INIT_LOI = "initLoi";
    private final LoiUiController loiUiController;
    Action delete;
    Action duplicate;
    Action create;
    final JComboBox cbLois;
    Loi currentLoi;
    boolean isNewLoi;
    boolean editable;
    private final JTextField txtCommentaire;
    private final JTextField txtNom;
    private final JLabel lbNomValidation;
    private final String initName;
    private final DateDebEditor dateDeb;

    public DLHYTopComponent() {
        initComponents();
        initName = NbBundle.getMessage(DLHYTopComponent.class, "CTL_" + TOPCOMPONENT_ID);
        setName(initName);
        setToolTipText(NbBundle.getMessage(DLHYTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
        loiUiController = new LoiUiController();
        loiUiController.addExportImagesToToolbar();
        loiUiController.setUseVariableForAxeH(true);
        add(loiUiController.getToolbar(), BorderLayout.NORTH);
        loiUiController.getPanel().setPreferredSize(new Dimension(550, 350));
        final JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, loiUiController.getPanel(), loiUiController.getTableGraphePanel());

        splitPane.setDividerLocation(550);
        splitPane.setOneTouchExpandable(true);
        splitPane.setContinuousLayout(true);
        add(splitPane);

        final JPanel nomComment = new JPanel(new BuGridLayout(3, 10, 10));

        final DocumentListener documentListener = super.createDocumentListener();
        final DocumentListener nameChanged = new DocumentListener() {
            @Override
            public void insertUpdate(final DocumentEvent e) {
                titleChanged();
            }

            @Override
            public void removeUpdate(final DocumentEvent e) {
                titleChanged();
            }

            @Override
            public void changedUpdate(final DocumentEvent e) {
                titleChanged();
            }
        };
        txtNom = new JTextField(30);
        lbNomValidation = new JLabel();

        //ligne 1
        nomComment.add(new JLabel(NbBundle.getMessage(DLHYTopComponent.class, "Nom.DisplayName")));
        nomComment.add(txtNom);
        nomComment.add(lbNomValidation);

        txtCommentaire = new JTextField(30);
        txtCommentaire.setEditable(false);
        txtCommentaire.getDocument().addDocumentListener(documentListener);
        txtNom.getDocument().addDocumentListener(documentListener);
        txtNom.getDocument().addDocumentListener(nameChanged);
        //ligne 2
        nomComment.add(new JLabel(NbBundle.getMessage(DLHYTopComponent.class, "Commentaire.DisplayName")));
        nomComment.add(txtCommentaire);
        nomComment.add(new JLabel());

        dateDeb = new DateDebEditor();
        //ligne 3
        nomComment.add(new JLabel(NbBundle.getMessage(DLHYTopComponent.class, "DateDebut.DisplayName")));
        nomComment.add(dateDeb.getPn());
        final JPanel pnInfo = new JPanel(new BorderLayout(5, 5));
        pnInfo.add(loiUiController.getInfoController().getPanel());
        pnInfo.add(nomComment, BorderLayout.NORTH);
        pnInfo.add(createCancelSaveButtons(), BorderLayout.SOUTH);
        cbLois = new JComboBox();
        cbLois.addItemListener(this);
        cbLois.setRenderer(new ObjetNommeCellRenderer());
        final JPanel pnActions = new JPanel(new BuGridLayout(1, 5, 5));
        pnActions.add(cbLois);
        pnActions.add(new JSeparator());
        createActions();
        pnActions.add(new JButton(create));
        pnActions.add(new JButton(duplicate));
        pnActions.add(new JSeparator());
        pnActions.add(new JButton(delete));
        final JPanel pnSouth = new JPanel(new BorderLayout(10, 10));
        pnSouth.add(pnActions, BorderLayout.WEST);
        pnSouth.setBorder(BorderFactory.createEtchedBorder());
        pnSouth.add(pnInfo);
        add(pnSouth, BorderLayout.SOUTH);
        setEditable(false);
    }

    @Override
    public HelpCtx getHelpCtx() {
        HelpCtx res = super.getHelpCtx();
        final Object selectedItem = getCurrentLoi();
        if (selectedItem != null) {
            res = new HelpCtx(SysdocUrlBuilder.addSignet(res.getHelpID(), getCurrentLoi().getType().getId()));
        }
        return res;
    }

    @Override
    protected String getViewHelpCtxId() {
        return "vueDLHY";
    }

    @Override
    public LoiUiController getLoiUIController() {
        return loiUiController;
    }

    protected void dateDebModified() {
        if (getCurrentLoi() != null && getScenario().getParamCalcScenario().getDateDebSce() != null) {
            updateAxeXFormatter();
        }
    }

    @Override
    protected EMHScenario getScenario() {
        return modellingScenarioModificationService.getModellingScenarioService().getScenarioLoaded();
    }

    @Override
    public List<String> getComments() {
        return new TableExportCommentSupplier(false).getComments(getName());
    }

    @Override
    protected void componentOpenedHandler() {
        super.componentOpenedHandler();
        loiUiController.getGraphe().setExportTableCommentSupplier(this);
        final Loi init = (Loi) getClientProperty(PROPERTY_INIT_LOI);
        //on sélectionne la loi du meme nom
        if (init != null) {
            putClientProperty(PROPERTY_INIT_LOI, null);
            final int nb = cbLois.getModel().getSize();
            for (int i = 0; i < nb; i++) {
                if (init.getId().equals(((Loi) cbLois.getModel().getElementAt(i)).getId())) {
                    cbLois.setSelectedIndex(i);
                    break;
                }
            }
        }
    }

    protected void titleChanged() {
        lbNomValidation.setText(StringUtils.EMPTY);
        lbNomValidation.setIcon(null);
        lbNomValidation.setToolTipText(null);
        if (currentLoi == null) {
            return;
        }
        setName(initName + BusinessMessages.ENTITY_SEPARATOR + txtNom.getText());
        updateTopComponentName();
        final String error = validateNom();
        if (error != null) {
            lbNomValidation.setToolTipText(error);
            lbNomValidation.setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.SEVERE));
        }
    }

    protected String validateNom() {
        final String nom = txtNom.getText();
        final String error = ValidationPatternHelper.isNameValidei18nMessage(nom, currentLoi.getType());
        return error;
    }

    private void createActions() {
        delete = new EbliActionSimple(NbBundle.getMessage(DLHYTopComponent.class, "Delete.DisplayName"), BuResource.BU.getIcon("enlever"), "DELETE") {
            @Override
            public void actionPerformed(final ActionEvent _e) {
                delete();
            }
        };
        delete.putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(DLHYTopComponent.class, "Delete.Description"));

        duplicate = new EbliActionSimple(NbBundle.getMessage(DLHYTopComponent.class, "Duplicate.DisplayName"), BuResource.BU.getIcon("dupliquer"),
                "DUPLICATE") {
            @Override
            public void actionPerformed(final ActionEvent _e) {
                duplicate();
            }
        };
        duplicate.putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(DLHYTopComponent.class, "Duplicate.Description"));

        create = new EbliActionSimple(NbBundle.getMessage(DLHYTopComponent.class, "Create.DisplayName"), BuResource.BU.getIcon("creer"), "CREATE") {
            @Override
            public void actionPerformed(final ActionEvent _e) {
                create();
            }
        };
        create.putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(DLHYTopComponent.class, "Create.Description"));
    }

    @Override
    public void itemStateChanged(final ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            currentLoi = (Loi) cbLois.getSelectedItem();
            updateWithLoi();
        }
    }

    @Override
    protected String getDefaultTopComponentId() {
        return TOPCOMPONENT_ID;
    }

    @Override
    public void setEditable(final boolean b) {
        editable = b;
        super.setEditable(editable);
        dateDeb.setEditable(b);
        loiUiController.setEditable(b);
        txtCommentaire.setEditable(b);
        txtNom.setEditable(b);
        delete.setEnabled(b);
        duplicate.setEnabled(b);
        create.setEnabled(b);
    }

    @Override
    public void cancelModificationHandler() {
        if (isNewLoi) {
            isNewLoi = false;
            currentLoi = (Loi) cbLois.getSelectedItem();
        }
        reloadCourbeConfig();
        scenarioReloaded();
        setModified(false);
    }

    @Override
    protected void scenarioLoadedHandler() {
        final Loi oldLoi = getCurrentLoi();
        currentLoi = null;
        final DonLoiHYConteneur dlhy = getScenario().getLoiConteneur();
        final List<Loi> lois = new ArrayList<>(dlhy.getLois());
        Collections.sort(lois, ObjetNommeByNameComparator.INSTANCE);
        final int idx = lois.indexOf(oldLoi);
        cbLois.setModel(new DefaultComboBoxModel(lois.toArray(new Loi[0])));
        cbLois.setSelectedItem(null);
        if (idx >= 0) {
            cbLois.setSelectedIndex(idx);
        }
        isNewLoi = false;
    }

    protected void updateAxeXFormatter() {
        if (getCurrentLoi() != null) {
            final ConfigLoi configLoi = getCcm().getConfLoi().get(getCurrentLoi().getType());
            if (configLoi.getVarAbscisse().getNature().isDuration()) {
                final LocalDateTime dateZeroLoiDF = dateDeb.getTime();
                if (dateZeroLoiDF == null || getScenario().getParamCalcScenario().getDateDebSce() == null) {
                    loiUiController.configureAxeH(getCcm(), getCurrentLoi(), false);
                } else {
                    final ToStringTransformerDate transformer = new ToStringTransformerDate(dateZeroLoiDF);
                    final CtuluNumberFormatI detailFormat = loiUiController.getAxeX().getSpecificDetailFormat();
                    final SecondValueEditor oldSecondValueEditor = (SecondValueEditor) loiUiController.getAxeX().getValueEditor();
                    final SecondValueEditor editor = new SecondValueEditor(transformer, oldSecondValueEditor.getFormatter());
                    loiUiController.setAxeHSpecificFormats(transformer, detailFormat, editor);
                }
                loiUiController.getTableGraphePanel().updateState();
                loiUiController.getGraphe().fullRepaint();
            }
        }
    }

    protected void updateWithLoi() {
        updating = true;
        final Loi loi = getCurrentLoi();
        txtCommentaire.setText(StringUtils.EMPTY);
        txtNom.setText(StringUtils.EMPTY);
        if (loi != null) {
            txtCommentaire.setText(loi.getCommentaire());
            final String desc = StringUtils.defaultString(loi.getNom());
            txtNom.setText(desc);
            setName(initName + BusinessMessages.ENTITY_SEPARATOR + desc);
        }
        dateDeb.setLoi(loi);
        loiUiController.setLoi(loi, getCcm(), false);
        loiUiController.getLoiModel().addObserver(new Observer() {
            @Override
            public void update(final Observable o, final Object arg) {
                setModified(true);
            }
        });
        loiUiController.setEditable(editable);
        applyCourbeConfig();
        updateAxeXFormatter();
        updating = false;
        setModified(false);
    }

    @Override
    protected void scenarioReloaded() {
        scenarioLoaded();
    }

    @Override
    protected void scenarioUnloadedHandler() {
        loiUiController.setLoi(null, null, false);
        currentLoi = null;
        cbLois.setModel(new DefaultComboBoxModel());
    }

    Loi getCurrentLoi() {
        return currentLoi;
    }

    @Override
    public void setModified(final boolean modified) {
        super.setModified(modified);
        duplicate.setEnabled(!modified && editable);
        create.setEnabled(!modified && editable);
        cbLois.setEnabled(!modified);
    }

    void delete() {
        final boolean used = currentLoi.getUsed();
        if (used) {
            final List<String> usedBy = LoiHelper.getUsedBy(currentLoi, getCcm(), DecimalFormatEpsilonEnum.COMPARISON);
            final JPanel pn = new JPanel(new BorderLayout(5, 5));
            final String panelInfo = NbBundle.getMessage(DLHYTopComponent.class, "DeleteAbort.LoiIsUsed");
            pn.add(new JLabel(panelInfo), BorderLayout.NORTH);
            final JList list = new JList(usedBy.toArray(new String[0]));
            pn.add(new JScrollPane(list));
            pn.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
            DialogHelper.showComponentError(pn, panelInfo);
            return;
        }
        final boolean ok = DialogHelper.showQuestion(NbBundle.getMessage(DLHYTopComponent.class, "DeleteConfirmation.Message", txtNom.getText()));
        if (ok) {
            final DonLoiHYConteneur loiConteneur = getScenario().getLoiConteneur();
            if (!isNewLoi) {
                final DefaultComboBoxModel model = (DefaultComboBoxModel) cbLois.getModel();
                final int idx = Math.max(0, model.getIndexOf(currentLoi) - 1);
                loiConteneur.removeLois(currentLoi);
                currentLoi = null;
                //on sélectionne la loi précédente:
                if (idx < loiConteneur.getLois().size()) {
                    final List<Loi> lois = new ArrayList<>(loiConteneur.getLois());
                    Collections.sort(lois, ObjetNommeByNameComparator.INSTANCE);
                    currentLoi = lois.get(idx);
                }
                modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.DLHY));
            } else {
                //on sélectionne la loi disponible dans la combobox.
                currentLoi = (Loi) cbLois.getSelectedItem();
                scenarioReloaded();
            }
            isNewLoi = false;
            setModified(false);
        }
    }

    void create() {
        final List<EnumTypeLoi> loisType = getCcm().getCreatableDLHYLois();
        final JComboBox cb = new JComboBox(loisType.toArray(new EnumTypeLoi[0]));
        cb.setRenderer(new ObjetWithIdCellRenderer());
        cb.setSelectedItem(null);
        final JPanel pn = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
        pn.add(new JLabel(NbBundle.getMessage(DLHYTopComponent.class, "CreateLoi.TypeLabel")));
        pn.add(cb);
        final JButton createButtonHelp = SysdocUrlBuilder.createButtonHelp();
        pn.add(createButtonHelp);
        createButtonHelp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final SysdocContrat sysdocContrat = Lookup.getDefault().lookup(SysdocContrat.class);
                String dialogHelpCtxId = SysdocUrlBuilder.getDialogHelpCtxId("creerNouvelleLoi", PerspectiveEnum.MODELLING);
                final Object selectedItem = cb.getSelectedItem();
                if (selectedItem != null) {
                    dialogHelpCtxId = SysdocUrlBuilder.addSignet(dialogHelpCtxId, selectedItem.toString());
                }
                sysdocContrat.display(dialogHelpCtxId);
            }
        });
        if (DialogHelper.showQuestionOkCancel((String) create.getValue(Action.NAME), pn) && cb.getSelectedItem() != null) {
            final EnumTypeLoi typeLoi = (EnumTypeLoi) cb.getSelectedItem();
            isNewLoi = true;
            currentLoi = (Loi)LoiFactory.createLoi(typeLoi, getCcm());
            currentLoi.setNom(CruePrefix.getPrefix(currentLoi.getType()));
            updateWithLoi();
            setModified(true);
        }
    }

    void duplicate() {
        if (currentLoi == null) {
            return;
        }
        currentLoi = currentLoi.clonedWithoutUser();
        currentLoi.setNom(CruePrefix.getPrefix(currentLoi.getType()));
        isNewLoi = true;
        updateWithLoi();
        setModified(true);
    }

    @Override
    public boolean valideModificationDataHandler() {
        if (currentLoi == null) {
            return false;
        }
        boolean res = false;
        //on valide les données
        final Loi cloned = currentLoi.cloneWithoutPoints();
        if (LoiDF.class.equals(cloned.getClass())) {
            ((LoiDF) cloned).setDateZeroLoiDF(dateDeb.getTime());
        }
        final List<PtEvolutionFF> pts = loiUiController.getLoiModel().getPoints();
        cloned.getEvolutionFF().setPtEvolutionFF(pts);
        cloned.setCommentaire(txtCommentaire.getText());
        final CtuluLog valideLoi = ValidationHelperLoi.valideLoi(cloned, getCcm());
        final String error = validateNom();
        if (error != null) {
            valideLoi.addSevereError(error);
        } else {
            final List<Loi> allLois = LoiHelper.getAllLois(getScenario());
            if (!isNewLoi) {
                allLois.remove(currentLoi);
            }
            final Set<String> toSetOfId = TransformerHelper.toSetOfId(allLois);
            if (toSetOfId.contains(txtNom.getText().toUpperCase())) {
                valideLoi.addSevereError("validation.NameAlreadyUsed", txtNom.getText());
            }
        }
        //si validé on sauvegarde:
        if (!valideLoi.containsErrorOrSevereError()) {
            currentLoi.setCommentaire(txtCommentaire.getText());
            currentLoi.setNom(txtNom.getText());
            currentLoi.getEvolutionFF().setPtEvolutionFF(pts);
            if (isNewLoi) {
                getScenario().getLoiConteneur().addLois(currentLoi);
            }
            if (LoiDF.class.equals(currentLoi.getClass())) {
                ((LoiDF) currentLoi).setDateZeroLoiDF(dateDeb.getTime());
            }
            res = true;
            modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.DLHY));
            isNewLoi = false;
        }
        if (valideLoi.isNotEmpty()) {
            LogsDisplayer.displayError(valideLoi, getName());
        }
        return res;
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    void writeProperties(final java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
    }

    void readProperties(final java.util.Properties p) {
    }

    void selectLoi(final Loi loiToActivate) {
        if (isModified()) {
            return;
        }
        cbLois.setSelectedItem(loiToActivate);
    }

    protected class DateDebEditor {
        final LocalDateTimePanel pnDateDeb = new LocalDateTimePanel();
        final JCheckBox cbDateDeb = new JCheckBox();
        final JPanel pn = new JPanel(new FlowLayout(FlowLayout.LEFT));
        boolean updating;

        public DateDebEditor() {
            pn.add(cbDateDeb);
            cbDateDeb.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(final ItemEvent e) {
                    updateState();
                    modificationDone();
                }
            });
            pnDateDeb.addObserver(new Observer() {
                @Override
                public void update(final Observable o, final Object arg) {
                    modificationDone();
                }
            });
            pn.add(pnDateDeb);
        }

        private void modificationDone() {
            if (!updating) {
                dateDebModified();
                setModified(true);
            }
        }

        void updateState() {
            pnDateDeb.setAllEnable(cbDateDeb.isSelected() && cbDateDeb.isEnabled());
        }

        public JPanel getPn() {
            return pn;
        }

        LocalDateTime getTime() {
            return cbDateDeb.isSelected() ? pnDateDeb.getLocalDateTime() : null;
        }

        public void setLoi(final Loi loi) {
            updating = true;
            if (loi == null || !LoiDF.class.equals(loi.getClass())) {
                cbDateDeb.setSelected(false);
                pnDateDeb.setLocalDateTime(null);
                cbDateDeb.setEnabled(false);
                pnDateDeb.setAllEnable(false);
            } else {
                final LoiDF loiDF = (LoiDF) loi;
                cbDateDeb.setSelected(loiDF.getDateZeroLoiDF() != null);
                pnDateDeb.setLocalDateTime(loiDF.getDateZeroLoiDF());
            }
            updating = false;
        }

        public void setEditable(final boolean editable) {
            cbDateDeb.setEnabled(editable);
            updateState();
        }
    }
}
