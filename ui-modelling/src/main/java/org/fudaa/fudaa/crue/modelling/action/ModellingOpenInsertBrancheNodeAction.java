package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.fudaa.crue.modelling.list.ListRelationSectionContentNode;
import org.fudaa.fudaa.crue.modelling.list.ModellingListBrancheInsertTopComponent;
import org.openide.nodes.Node;
import org.openide.windows.TopComponent;

import static org.openide.util.NbBundle.getMessage;

/**
 * @author deniger
 */
public class ModellingOpenInsertBrancheNodeAction extends AbstractModellingOpenTopNodeAction<ModellingListBrancheInsertTopComponent> {
  public ModellingOpenInsertBrancheNodeAction() {

    super(getMessage(ModellingOpenInsertBrancheNodeAction.class, "ModellingOpenInsertBrancheNodeAction.Name"), ModellingListBrancheInsertTopComponent.MODE,
        ModellingListBrancheInsertTopComponent.TOPCOMPONENT_ID);
    setReopen(true);
  }

  @Override
  protected TopComponent activateNewTopComponent(final ModellingListBrancheInsertTopComponent newTopComponent, final Node selectedNode) {
    newTopComponent.setNoeud(getNoeud(selectedNode));
    return newTopComponent;
  }

  @Override
  public void performAction(final Node[] activatedNodes) {
    super.performAction(activatedNodes);
  }

  @Override
  protected void activeWithValue(final ModellingListBrancheInsertTopComponent opened, final Node selectedNode) {
    opened.setNoeud(getNoeud(selectedNode));
  }

  @Override
  public boolean isEnable(final Node[] activatedNodes) {
    return getNoeud(activatedNodes) != null;
  }

  private CatEMHNoeud getNoeud(final Node[] activatedNodes) {
    if (activatedNodes == null || activatedNodes.length != 1) {
      return null;
    }
    final Node node = activatedNodes[0];
    return getNoeud(node);
  }

  private CatEMHNoeud getNoeud(final Node node) {
    if (node instanceof ListRelationSectionContentNode) {
      final ListRelationSectionContentNode sectionContentNode = (ListRelationSectionContentNode) node;
      if (!sectionContentNode.isEditMode()) {
        return null;
      }
      final CatEMHSection section = sectionContentNode.getSection();
      if (section.getBranche() == null) {
        return null;
      }
      if (section == section.getBranche().getSectionAval().getEmh()) {
        return section.getBranche().getNoeudAval();
      }
      if (section == section.getBranche().getSectionAmont().getEmh()) {
        return section.getBranche().getNoeudAmont();
      }
    }
    return node.getLookup().lookup(CatEMHNoeud.class);
  }
}
