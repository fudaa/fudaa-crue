package org.fudaa.fudaa.crue.modelling.list;

import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.edition.bean.ListNoeudContent;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertyStringReadOnly;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;

import java.awt.datatransfer.Transferable;
import java.util.MissingResourceException;

/**
 * @author deniger
 */
public class ListNoeudContentNode extends AbstractListContentNode {
  public ListNoeudContentNode(final CatEMHNoeud noeud, final ListNoeudContent content, final PerspectiveServiceModelling perspectiveServiceModelling) {
    super(Children.LEAF, Lookups.fixed(noeud, content), perspectiveServiceModelling);
  }

  public static String getNoeudNomDisplay() throws MissingResourceException {
    return NbBundle.getMessage(ListNoeudContentNode.class, "NoeudName.Name");
  }

  @Override
  public PasteType getDropType(final Transferable t, final int action, final int index) {
    if (isEditMode()) {
      final Node dragged = DefaultNodePasteType.getMovedNodeInTransferable(t);
      return dragged == null ? null : new DefaultNodePasteType(dragged, this);
    }
    return null;
  }

  @Override
  protected Sheet createSheet() {
    final Sheet sheet = Sheet.createDefault();
    final Sheet.Set set = Sheet.createPropertiesSet();
    final ListNoeudContent noeudContent = getLookup().lookup(ListNoeudContent.class);
    try {
      final Node.Property nom = new PropertyStringReadOnly(noeudContent.getNom(), ListCommonProperties.PROP_NOM,
          getNoeudNomDisplay(),
          null);
      PropertyCrueUtils.configureNoCustomEditor(nom);
      set.put(nom);
      final Node.Property type = new PropertyStringReadOnly(noeudContent.getType().geti18n(), ListCommonProperties.PROP_TYPE,
          AbstractListContentNode.getTypeDisplay(),
          null);
      PropertyCrueUtils.configureNoCustomEditor(type);
      set.put(type);
      final Node.Property commentaire = new PropertySupportReflection(this, noeudContent, String.class,
          ListCommonProperties.PROP_COMMENTAIRE);
      PropertyCrueUtils.setNullValueEmpty(commentaire);
      commentaire.setName(ListCommonProperties.PROP_COMMENTAIRE);
      commentaire.setDisplayName(AbstractListContentNode.getCommentDisplay());
      set.put(commentaire);
    } catch (final NoSuchMethodException ex) {
      Exceptions.printStackTrace(ex);
    }
    sheet.put(set);
    return sheet;
  }
}
