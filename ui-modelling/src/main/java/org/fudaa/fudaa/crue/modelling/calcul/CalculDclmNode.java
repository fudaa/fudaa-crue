/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.CalcHelper;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.modelling.calcul.editor.DonCLimMWithManoeuvreRegulDataProperty;
import org.fudaa.fudaa.crue.modelling.calcul.editor.CalcTransNoeudQappExtProperty;
import org.fudaa.fudaa.crue.modelling.node.AbstractModellingNodeFirable;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

import java.io.IOException;
import java.util.*;

/**
 * @author Frederic Deniger
 */
public class CalculDclmNode extends AbstractModellingNodeFirable {
  private final DonLoiHYConteneur loiConteneur;

  public CalculDclmNode(final Children children, final DonCLimM dclm, final CrueConfigMetier ccm, final PerspectiveServiceModelling perspectiveServiceModelling,
                        final DonLoiHYConteneur loiConteneur) {
    super(children, Lookups.fixed(dclm, ccm), perspectiveServiceModelling);
    if (dclm.getEmh() == null) {
      setName("??????? / " + CalcHelper.getSuffixe(dclm, ccm));
    } else {
      setName(dclm.getEmh().getNom() + " / " + CalcHelper.getSuffixe(dclm, ccm));
    }
    this.loiConteneur = loiConteneur;
  }

  public DonLoiHYConteneur getLoiConteneur() {
    return loiConteneur;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(getLookup().lookup(DonCLimM.class)));
  }

  @Override
  public String getHtmlDisplayName() {
    final boolean actif = (getDCLM() != null) && getDCLM().getActuallyActive();
    if (actif) {
      return getName();
    }
    return "<font color:\"AAAAAA\">" + getName() + "</font>";
  }

  @Override
  public boolean canDestroy() {
    return isEditMode();
  }

  @Override
  public void destroy() throws IOException {
    final CalculEMHsNode parentNode = (CalculEMHsNode) getParentNode();
    parentNode.getCalc().removeDclm(getDCLM());
    super.destroy();
  }

  @Override
  public SystemAction[] getActions() {
    return new SystemAction[]{
        SystemAction.get(CalculRemoveDclmNodeAction.class)
    };
  }

  DonCLimM getDCLM() {
    return getLookup().lookup(DonCLimM.class);
  }

  CrueConfigMetier getCCM() {
    return getLookup().lookup(CrueConfigMetier.class);
  }

  @Override
  protected Sheet createSheet() {
    final PropertyNodeBuilder builder = new PropertyNodeBuilder();
    final DonCLimM dclm = getDCLM();
    final Sheet res = Sheet.createDefault();
    final Sheet.Set set = Sheet.createPropertiesSet();
    res.put(set);
    final List<PropertySupportReflection> properties = builder.createFromPropertyDesc(DecimalFormatEpsilonEnum.COMPARISON, dclm, this);
    for (final Iterator<PropertySupportReflection> it = properties.iterator(); it.hasNext(); ) {
      final PropertySupportReflection propertySupportReflection = it.next();
      final String name = propertySupportReflection.getName();
      if (DonCLimMCommonItem.PROP_USER_ACTIVE.equals(name)) {
        propertySupportReflection.setName(CalculNode.COLUMN_ACTIF);
        set.put(propertySupportReflection);
        it.remove();
      } else if (CalcPseudoPermBrancheOrificeManoeuvre.PROP_SENS_OUV.equals(name)) {
        PropertyNodeBuilder.configurePropertySupport(null, propertySupportReflection, this);
        propertySupportReflection.setName(CalculNode.COLUMN_SENS);
        set.put(propertySupportReflection);
        it.remove();
      }
    }
    if (dclm.getCalculParent().isPermanent()) {
      if (dclm instanceof CalcPseudoPermBrancheOrificeManoeuvreRegul) {
        try {
          DonCLimMWithManoeuvreRegulDataProperty prop = new DonCLimMWithManoeuvreRegulDataProperty(this, (CalcPseudoPermBrancheOrificeManoeuvreRegul) dclm);
          prop.setName(CalculNode.COLUMN_VALUE);
          set.put(prop);
        } catch (NoSuchMethodException e) {
          Exceptions.printStackTrace(e);
        }
      } else if (dclm instanceof CalcPseudoPermItem && ((CalcPseudoPermItem) dclm).containValue()) {
        final PropertySupportReflection value = properties.get(0);
        final String initName = value.getName();
        final ItemVariable property = getCCM().getProperty(initName);
        setShortDescription(property.getValidator().getHtmlDesc(getName()));
        PropertyCrueUtils.configureCustomEditor(value);
        value.setName(CalculNode.COLUMN_VALUE);
        PropertyNodeBuilder.addValidateListener(value, property, this);
        set.put(value);
      }
    } else {
      if (properties.size() > 1) {
        //manoeuvre
        for (final PropertySupportReflection propertySupportReflection : properties) {
          final Class valueType = propertySupportReflection.getValueType();
          if (Loi.class.isAssignableFrom(valueType)) {
            addLoiProperties(propertySupportReflection, set, dclm);
          }
        }
      } else if (!properties.isEmpty()) {
        final PropertySupportReflection value = properties.get(0);
        addLoiProperties(value, set, dclm);
      }
      if (dclm instanceof CalcTransBrancheOrificeManoeuvreRegul) {
        try {
          DonCLimMWithManoeuvreRegulDataProperty prop = new DonCLimMWithManoeuvreRegulDataProperty(this, (CalcTransBrancheOrificeManoeuvreRegul) dclm);
          prop.setName(CalculNode.COLUMN_VALUE);
          set.put(prop);
        } catch (NoSuchMethodException e) {
          Exceptions.printStackTrace(e);
        }
      }
      if (dclm instanceof CalcTransNoeudQappExt) {
        try {
          CalcTransNoeudQappExtProperty prop = new CalcTransNoeudQappExtProperty(this, (CalcTransNoeudQappExt) dclm);
          prop.setName(CalculNode.COLUMN_VALUE);
          set.put(prop);
        } catch (NoSuchMethodException e) {
          Exceptions.printStackTrace(e);
        }
      }
    }
    return res;
  }

  public void addLoiProperties(final PropertySupportReflection value, final Sheet.Set set, final DonCLimM dclm) throws MissingResourceException {
    value.setName(CalculNode.COLUMN_VALUE);
    set.put(value);
    final CalcTransItem transItem = (CalcTransItem) dclm;
    final List<Loi> filterDLHY = LoiHelper.filterDLHY(loiConteneur, transItem.getTypeLoi());
    final Map<String, Loi> toMapOfNom = TransformerHelper.toMapOfNom(filterDLHY);
    final ArrayList<String> sortedNames = CollectionCrueUtil.getSortedList(toMapOfNom.keySet());
    sortedNames.add(0, StringUtils.EMPTY);

    value.setTags(sortedNames.toArray(new String[0]));
    value.setTranslationForObjects(toMapOfNom);
  }
}
