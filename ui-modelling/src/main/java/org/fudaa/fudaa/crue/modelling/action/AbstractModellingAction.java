package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.CommonMessage;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractPerspectiveAwareAction;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.UserSaveAnswer;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.modelling.AbstractModellingTopComponent;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

import java.awt.event.ActionEvent;

public abstract class AbstractModellingAction extends AbstractPerspectiveAwareAction {
  protected final ModellingScenarioService scenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);
  private final Result<EMHScenario> resultat;

  public AbstractModellingAction(boolean enableInEditMode) {
    super(PerspectiveEnum.MODELLING, enableInEditMode);
    resultat = scenarioService.getLookup().lookupResult(EMHScenario.class);
    resultat.addLookupListener(this);
    resultChanged(null);
  }

  @Override
  protected boolean getEnableState() {
    return super.getEnableState() && scenarioService.isScenarioLoaded();
  }

  protected abstract void doAction();

  @Override
  public final void actionPerformed(ActionEvent e) {
    if (!validationAction()) {
      return;
    }
    doAction();
  }

  protected boolean isActivatedComponentModified() {
    TopComponent activated = WindowManager.getDefault().getRegistry().getActivated();
    if (activated instanceof AbstractModellingTopComponent) {
      AbstractModellingTopComponent topC = (AbstractModellingTopComponent) activated;
      return topC.isModified();
    }

    return false;
  }

  protected boolean validationAction() {
    if (isActivatedComponentModified()) {
      AbstractModellingTopComponent topC = (AbstractModellingTopComponent) WindowManager.getDefault().getRegistry().getActivated();
      String title = CommonMessage.getMessage("ValidationVue.DialogTitle");
      String save = CommonMessage.getMessage("ValidationVue.ValidAction");
      String dontSave = CommonMessage.getMessage("ValidationVue.CancelAction");
      String cancel = CommonMessage.getMessage("ValidationVue.DontLaunchAction");
      String message = CommonMessage.getMessage("ValidationVue.DialogMessage");
      UserSaveAnswer confirmSaveOrNot = DialogHelper.confirmSaveOrNot(
          title, message, save, dontSave, cancel);

      if (UserSaveAnswer.CANCEL.equals(confirmSaveOrNot)) {
        return false;
      }

      if (UserSaveAnswer.SAVE.equals(confirmSaveOrNot)) {
        topC.valideModification();
      }

      if (UserSaveAnswer.DONT_SAVE.equals(confirmSaveOrNot)) {
        topC.cancelModification();
      }
    }
    return true;
  }
}
