/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.calcul;

import java.awt.BorderLayout;
import java.beans.PropertyChangeListener;
import java.util.List;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.Regle;
import org.fudaa.dodico.crue.metier.emh.ValParam;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 *
 * @author Frédéric Deniger
 */
public class RegleView extends JPanel implements ExplorerManager.Provider {

  final PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
  final OutlineView view;
  List<Regle> regles;
  CrueConfigMetier ccm;

  public RegleView() {
    view = new OutlineView(BusinessMessages.getString("regle.property"));
    view.setPropertyColumns(Regle.PROP_ACTIVE, BusinessMessages.getString("active.property"), ValParam.PROP_VALEUR, BusinessMessages.getString("valParamValeur.property"));
    setLayout(new BorderLayout());
    view.getOutline().setRootVisible(false);
    add(view);
  }
  private final ExplorerManager em = new ExplorerManager();

  public OutlineView getView() {
    return view;
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return em;
  }

  public void init(List<Regle> regles, CrueConfigMetier ccm) {
    init(regles, ccm, null);
  }

  public void init(List<Regle> regles, CrueConfigMetier ccm, PropertyChangeListener listener) {
    this.regles = regles;
    this.ccm = ccm;
    Children.Array children = new Children.Array();
    Node[] res = new Node[regles.size()];
    for (int i = 0; i < regles.size(); i++) {
      res[i] = new RegleNode(regles.get(i), ccm, perspectiveServiceModelling);
      if (listener != null) {
        res[i].addPropertyChangeListener(listener);
      }
    }
    children.add(res);
    em.setRootContext(new AbstractNode(children));
  }
}
