package org.fudaa.fudaa.crue.modelling.loi;

import org.fudaa.fudaa.crue.modelling.action.AbstractModellingOpenTopNodeAction;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

/**
 *
 *
 * @author Frédéric Deniger
 */
public class ProfilSectionOpenNodeAction extends AbstractModellingOpenTopNodeAction<ProfilSectionTopComponent> {

  public ProfilSectionOpenNodeAction(String name) {
    super(name, ProfilSectionTopComponent.MODE, ProfilSectionTopComponent.TOPCOMPONENT_ID);
  }

  public ProfilSectionOpenNodeAction() {
    super(NbBundle.getMessage(ProfilSectionOpenNodeAction.class, "ModellingOpenProfilSectionNodeAction.DisplayName"), ProfilSectionTopComponent.MODE, ProfilSectionTopComponent.TOPCOMPONENT_ID);
  }

  @Override
  protected void activeWithValue(ProfilSectionTopComponent opened, Node selectedNode) {
  }

  @Override
  protected TopComponent activateNewTopComponent(ProfilSectionTopComponent newTopComponent, Node selectedNode) {
    return newTopComponent;
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return true;
  }

  public static void open() {
    ProfilSectionOpenNodeAction action = SystemAction.get(ProfilSectionOpenNodeAction.Reopen.class);
    action.performAction(new Node[0]);
  }

  public static class Reopen extends ProfilSectionOpenNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ProfilSectionOpenNodeAction.class, "ModellingOpenProfilSectionNodeAction.DisplayName"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ProfilSectionOpenNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ProfilSectionOpenNodeAction.class, "ModellingOpenProfilSectionNodeAction.NewFrame.DisplayName"));
      setReopen(false);
    }
  }
}
