package org.fudaa.fudaa.crue.modelling;

import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.DisableSortHelper;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.modelling.global.GlobalActionChildFactory;
import org.fudaa.fudaa.crue.modelling.global.GlobalContainerNode;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.modelling//ModellingContainersTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = ModellingContainersTopComponent.TOPCOMPONENT_ID,
        iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png",
        persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "modelling-topLeft", openAtStartup = false, position = 1)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.modelling.ModellingContainersTopComponent")
@ActionReference(path = "Menu/Window/Modelling", position = 2)
@TopComponent.OpenActionRegistration(displayName = ModellingContainersTopComponent.TOPCOMPONENT_ACTION_DISPLAYNAME,
        preferredID = ModellingContainersTopComponent.TOPCOMPONENT_ID)
public final class ModellingContainersTopComponent extends AbstractModellingTopComponent implements LookupListener, ExplorerManager.Provider {
  
  public static final String TOPCOMPONENT_ID = "ModellingContainersTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  private final ExplorerManager em = new ExplorerManager();
  private final OutlineView outlineView;
  private final DisableSortHelper sortDisabler;
  
  public ModellingContainersTopComponent() {
    initComponents();
    setName(NbBundle.getMessage(ModellingContainersTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingContainersTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    outlineView = new OutlineView(NbBundle.getMessage(ModellingContainersTopComponent.class, "ModellingContainersTopComponent.ColumnName"));
    outlineView.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 0));
    outlineView.getOutline().setRootVisible(false);
    outlineView.getOutline().setFullyNonEditable(true);
    outlineView.getOutline().setFillsViewportHeight(true);
    outlineView.getOutline().setColumnHidingAllowed(false);
    outlineView.addPropertyColumn(GlobalContainerNode.PROP_VISIBILITY, NbBundle.getMessage(ModellingContainersTopComponent.class,
            "ModellingContainersTopComponent.ColumnVisible"));
    add(outlineView);
    outlineView.getOutline().getColumnModel().getColumn(1).setWidth(20);
    outlineView.getOutline().getColumnModel().getColumn(1).setPreferredWidth(20);
    ActionMap map = this.getActionMap();
    associateLookup(ExplorerUtils.createLookup(em, map));
    sortDisabler = new DisableSortHelper(outlineView);
  }
  
  @Override
  protected String getViewHelpCtxId() {
    return "vueGestionnaireConteneurs";
  }
  
  @Override
  public ExplorerManager getExplorerManager() {
    return em;
  }
  
  @Override
  protected void setEditable(boolean b) {
    sortDisabler.setDisableSort(b);
  }
  
  @Override
  protected void componentActivated() {
    super.componentActivated();
    ExplorerUtils.activateActions(em, true);
  }
  
  @Override
  protected void componentDeactivated() {
    super.componentDeactivated();
    ExplorerUtils.activateActions(em, false);
  }
  
  @Override
  public void cancelModificationHandler() {
  }
  
  @Override
  public void valideModificationHandler() {
  }

  /**
   * WARN: A utiliser uniquement pour les tests
   */
  public void testScenarioLoaded(EMHScenario scenario) {
    em.setRootContext(GlobalActionChildFactory.createGlobalContainerNode(scenario));
  }
  
  @Override
  protected void scenarioLoaded() {
    em.setRootContext(GlobalActionChildFactory.createGlobalContainerNode(getModellingService().getScenarioLoaded()));
    NodeHelper.expandAll(em, outlineView);
  }
  
  @Override
  protected void scenarioReloaded() {
    scenarioLoaded();
  }
  
  @Override
  protected void scenarioChanged(ScenarioModificationEvent event) {
  }
  
  @Override
  protected void scenarioUnloaded() {
    em.setRootContext(Node.EMPTY);
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  @Override
  public void componentClosedTemporarily() {
  }

  void writeProperties(java.util.Properties p) {
    DialogHelper.writeProperties(outlineView, "outlineView", p);
  }
  
  void readProperties(java.util.Properties p) {
    DialogHelper.readProperties(outlineView, "outlineView", p);
  }
}
