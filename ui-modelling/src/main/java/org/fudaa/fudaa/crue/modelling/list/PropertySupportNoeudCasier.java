package org.fudaa.fudaa.crue.modelling.list;

import java.beans.PropertyEditor;
import java.beans.PropertyEditorSupport;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.edition.bean.ListCasierContent;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertySupportReadWrite;
import org.openide.nodes.Node;

/**
 *
 * @author deniger
 */
public class PropertySupportNoeudCasier extends PropertySupportReadWrite<ListCasierContent, String> {

  private final List<String> allNodes;

  public PropertySupportNoeudCasier(final AbstractNodeFirable node, final ListCasierContent instance, final List<String> allNodes) {
    super(node, instance, String.class, ListCasierContent.PROP_NOEUD, ListCasierContentNode.getCasierNoeudDisplayName(), null);
    PropertyCrueUtils.configureNoCustomEditor(this);
    this.allNodes = allNodes;
  }

  

  @Override
  protected void setValueInInstance(final String newVal) {
    final String casierName = CruePrefix.getNomAvecPrefixFor(newVal, EnumCatEMH.CASIER);
    final String oldCasierName = getInstance().getNom();
    getInstance().setNom(casierName);
    fireObjectChange(ListCommonProperties.PROP_NOM, oldCasierName, casierName);
    getInstance().setNoeud(newVal);
  }

  @Override
  public String getValue() throws IllegalAccessException, InvocationTargetException {
    return getInstance().getNoeud();
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    return new NoeudPropertyEditor();
  }

  private class NoeudPropertyEditor extends PropertyEditorSupport {

    public NoeudPropertyEditor() {
    }

    @Override
    public String getAsText() {
      return (String) getValue();
    }

    @Override
    public void setAsText(final String string) {
      setValue(string);
    }
    String[] coeurs;

    @Override
    public String[] getTags() {
      final List<String> noeudsNames = new ArrayList<>(allNodes);
      final Node[] nodes = node.getParentNode().getChildren().getNodes();
      for (final Node casierNode : nodes) {
        final ListCasierContent casierContent = casierNode.getLookup().lookup(ListCasierContent.class);
        if (casierContent != getInstance()) {
          noeudsNames.remove(casierContent.getNoeud());
        }
      }
      return noeudsNames.toArray(new String[0]);
    }
  }
}
