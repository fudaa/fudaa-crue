package org.fudaa.fudaa.crue.modelling.services;

import java.io.File;
import java.util.Map;
import java.util.concurrent.Callable;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.ScenarioSaverProcess;
import org.fudaa.dodico.crue.validation.ValidateModeleScenarioWithSchema;

/**
 *
 * @author deniger
 */
public class ModellingValidXmlCallable implements Callable<CtuluLogGroup> {

  private final ModellingScenarioServiceImpl service;

  public ModellingValidXmlCallable(ModellingScenarioServiceImpl service) {
    this.service = service;
  }

  @Override
  public CtuluLogGroup call() throws Exception {
    final ManagerEMHScenario managerScenario = service.getManagerScenarioLoaded();
    if (managerScenario.isCrue9()) {
      return null;
    }
    File validDir = new File(service.getTempDir(), "VALIDATION");
    if (validDir.exists()) {
      CtuluLibFile.deleteDir(validDir);
    }
    validDir.mkdirs();
    ScenarioSaverProcess saver = new ScenarioSaverProcess(service.getScenarioLoaded(), managerScenario,
                                            service.getSelectedProjet());
    saver.setOtherDir(validDir);
    CtuluLogGroup saveWithScenarioType = saver.saveWithScenarioType();
    if (saveWithScenarioType.containsFatalError()) {
      return saveWithScenarioType;
    }
    Map<String, File> inputFiles = service.getSelectedProjet().getInputFiles(managerScenario, validDir);
    ValidateModeleScenarioWithSchema schemaValidator = new ValidateModeleScenarioWithSchema(CrueFileHelper.createFrom(inputFiles),
                                                                                            managerScenario,
                                                                                            service.getSelectedProjet().getCoeurConfig());
    return schemaValidator.validate();
  }
}
