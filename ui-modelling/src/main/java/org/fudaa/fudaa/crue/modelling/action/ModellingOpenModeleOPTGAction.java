/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import javax.swing.Action;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.fudaa.crue.modelling.perspective.ActiveModelisation;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View", id = "org.fudaa.fudaa.crue.modelling.ModellingOpenModeleOPTGAction")
@ActionRegistration(displayName = "#ModellingModeleOPTGNodeAction.Name")
@ActionReferences({
  @ActionReference(path = ActiveModelisation.ACTIONS_MODELLING_VIEWS, position = 10)
})
public final class ModellingOpenModeleOPTGAction extends AbstractModellingModeleOpenAction {

  public ModellingOpenModeleOPTGAction() {
    putValue(Action.NAME, NbBundle.getMessage(ModellingOpenModeleOPTGAction.class, "ModellingModeleOPTGNodeAction.Name"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingOpenModeleOPTGAction();
  }

  @Override
  protected void doAction(EMHModeleBase modele) {
    ModellingOpenModeleOPTGNodeAction.open(modele);
  }
}
