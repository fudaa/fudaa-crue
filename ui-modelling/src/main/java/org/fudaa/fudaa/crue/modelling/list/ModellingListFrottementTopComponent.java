package org.fudaa.fudaa.crue.modelling.list;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuGridLayout;
import gnu.trove.TIntArrayList;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluTableSimpleExporter;
import org.fudaa.ctulu.table.CtuluTableModelInterface;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.fudaa.crue.common.grid.ColumnHeaderContent;
import org.fudaa.fudaa.crue.common.grid.TableHeaderCellRendererVertical;
import org.fudaa.fudaa.crue.common.helper.*;
import org.fudaa.fudaa.crue.common.swing.TableRowHeaderInstaller;
import org.fudaa.fudaa.crue.modelling.AbstractModellingTopComponent;
import org.fudaa.fudaa.crue.modelling.calcul.ModellingListCLimMsTopExportImportPopupBuilder;
import org.fudaa.fudaa.crue.modelling.calcul.ModellingListFrottementProgressRunnable;
import org.fudaa.fudaa.crue.modelling.emh.ModellingOpenEMHNodeAction;
import org.fudaa.fudaa.crue.modelling.loi.ModellingOpenDFRTNodeAction;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import javax.swing.*;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Squelette pour les TopComponent permettant de modifier des EMH.
 *
 * @author deniger
 */
@TopComponent.Description(preferredID = ModellingListFrottementTopComponent.TOPCOMPONENT_ID,
  iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png", persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ModellingListFrottementTopComponent.MODE, openAtStartup = false, position = 0)
public class ModellingListFrottementTopComponent extends AbstractModellingTopComponent {
  public static final String MODE = "modelling-listFrottement";
  public static final String TOPCOMPONENT_ID = "ModellingListFrottementTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String PROP_LIT_NUMEROTE = "LitNumerote";
  public static final String PROPERTY_BRANCHE = "BRANCHE";
  protected static final String PROPERTY_FOR_ROW_NAMES = "rowNames";
  Long sousModeleUid;
  private JScrollPane scrollPane;
  private JTable table;
  public ModellingListFrottementLineTableModel tableModel;
  // menu d'importer des fichiers XLS/CSV
  JMenuItem importItem;

  boolean isUpdating;
  CatEMHBranche brancheToSelect = null;
  private JComboBox cbSousModele;
  private JPanel pnSousModele;
  private boolean editable;

  public void createSousModelePanel() {
    cbSousModele = new JComboBox();
    cbSousModele.setRenderer(new ObjetNommeCellRenderer());
    cbSousModele.addItemListener(e -> {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        sousModeleSelectionChanged();
      }
    });

    pnSousModele = new JPanel(new BuGridLayout(2, 3, 3));
    pnSousModele.add(new JLabel(NbBundle.getMessage(ModellingListFrottementTopComponent.class, "ListTopComponent.SousModele.Label")));
    pnSousModele.setBorder(BorderFactory.createEmptyBorder(5, 2, 5, 0));
    pnSousModele.add(cbSousModele);
  }

  /**
   * avant de changer le sous-modele sélectionné, on teste si modification et dans ce cas, il faut demander confirmation.
   */
  private void sousModeleSelectionChanged() {
    if (!isUpdating()) {
      if (cbSousModele.getSelectedItem() != null) {
        final Long uiId = ((EMHSousModele) cbSousModele.getSelectedItem()).getUiId();
        if (!uiId.equals(this.sousModeleUid)) {
          if (isModified()) {
            final UserSaveAnswer userSaveAnswer = askForSave(NbBundle.getMessage(getClass(), "ValidationVue.DontChangeSousModele"));
            if (UserSaveAnswer.CANCEL.equals(userSaveAnswer)) {
              cbSousModele.setSelectedItem(getScenario().getIdRegistry().getEmh(sousModeleUid));
            } else {
              reloadWithSousModeleUid(uiId);
            }
          } else {
            reloadWithSousModeleUid(uiId);
          }
        }
      }
    }
  }


  public ModellingListFrottementTopComponent() {
    setName(NbBundle.getMessage(ModellingListFrottementTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingListFrottementTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    initComponents();
    createSousModelePanel();
    setBorder(BorderFactory.createCompoundBorder(getBorder(), BuBorders.EMPTY3333));
  }

  protected EMH updateSousModeleInCombobox() {
    if (getScenario() == null) {
      cbSousModele.setModel(new DefaultComboBoxModel());
      return null;
    }

    final List<EMHSousModele> sousModeles = getScenario().getSousModeles();
    EMH emh = getScenario().getIdRegistry().getEmh(sousModeleUid);
    cbSousModele.setModel(new DefaultComboBoxModel(sousModeles.toArray(new EMHSousModele[0])));
    cbSousModele.setSelectedItem(emh);
    return emh;
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueListeFrottements";
  }

  @Override
  public void valideModificationHandler() {
    if (table.getCellEditor() != null) {
      table.getCellEditor().stopCellEditing();
    }

    boolean modified = tableModel.applyModification();
    if (modified) {
      modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.DPTG));
    }
    setModified(false);
  }

  @Override
  public EMHScenario getScenario() {
    return super.modellingScenarioModificationService.getModellingScenarioService().getScenarioLoaded();
  }

  @Override
  public void cancelModificationHandler() {
    scenarioReloaded();
  }

  public EMHSousModele getSousModele() {
    return (EMHSousModele) getModellingService().getScenarioLoaded().getIdRegistry().getEmh(getSousModeleUid());
  }

  @Override
  protected void scenarioReloaded() {
    scenarioLoaded();
  }

  @Override
  public void setModified(boolean modified) {
    if (modified && isUpdating) {
      return;
    }
    super.setModified(modified);
  }

  @Override
  public void scenarioLoaded() {
    isUpdating = true;
    updateSousModeleInCombobox();
    removeAll();
    final EMH emh = getSousModele();
    setName(NbBundle.getMessage(ModellingListFrottementTopComponent.class, "NAME_ModellingListFrottementTopComponent", emh.getNom()));
    add(pnSousModele, BorderLayout.NORTH);
    createCenterComponent();
    add(createCancelSaveButtons(), BorderLayout.SOUTH);
    if (brancheToSelect != null) {
      scrollToBranche(brancheToSelect);
      brancheToSelect = null;
    }
    isUpdating = false;
    setModified(false);
    revalidate();
    repaint();
  }

  @Override
  protected void updateEditableState() {
    super.updateEditableState();
    if (importItem != null) {
      importItem.setEnabled(this.isEditable());
    }
  }

  protected void createCenterComponent() {
    ModellingListFrottementLineBuilder lineBuilder = new ModellingListFrottementLineBuilder(getSousModele(), getCcm());
    List<ModellingListFrottementLine> build = lineBuilder.build();
    int maxCharInName = lineBuilder.getMaxNameSize();
    String repeat = StringUtils.repeat("A", maxCharInName);
    JComboBox cb = new JComboBox(new String[]{repeat});
    cb.setSelectedItem(repeat);
    int cbWidth = cb.getPreferredSize().width;
    tableModel = new ModellingListFrottementLineTableModel(build, createHeaderString(), this);
    table = new JTable(tableModel);
    table.setRowHeight(20);
    int column = table.getColumnCount();
    ModellingListFrottementLineCellRenderer renderer = new ModellingListFrottementLineCellRenderer(this);
    ModellingListFrottementLineCellRenderer editor = new ModellingListFrottementLineCellRenderer(this);
    ModellingListFrottementSepCellRenderer rendererEmpty = new ModellingListFrottementSepCellRenderer();
    TableHeaderCellRendererVertical header = new TableHeaderCellRendererVertical();
    int width = header.getWidth();
    TableColumnModel columnModel = table.getColumnModel();
    for (int columnIdx = 0; columnIdx < column; columnIdx++) {
      final TableColumn tableColumn = columnModel.getColumn(columnIdx);
      tableColumn.setHeaderRenderer(header);
      if (columnIdx % 2 == 0) {
        tableColumn.setMinWidth(width);
        tableColumn.setMaxWidth(width);
        tableColumn.setCellRenderer(rendererEmpty);
      } else {
        tableColumn.setCellRenderer(renderer);
        tableColumn.setCellEditor(editor);
        int maxColumns = tableModel.getMaxColumns(columnIdx);
        tableColumn.setMinWidth(maxColumns * cbWidth);
      }
    }
    List<String> rows = new ArrayList<>();
    for (ModellingListFrottementLine modellingListFrottementLine : build) {
      rows.add(modellingListFrottementLine.getName());
    }
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    table.setCellSelectionEnabled(false);
    table.setRowSelectionAllowed(true);
    table.putClientProperty(PROPERTY_FOR_ROW_NAMES, rows);
    table.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() >= 2) {
          displayDonFrtOnMouse(e);
        }
      }
    });
    MouseListener doubleClikedOnHeader = new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() >= 2) {
          JTable table = (JTable) e.getSource();
          int selectedRow = table.getSelectedRow();
          if (selectedRow >= 0) {
            ModellingListFrottementLine line = tableModel.getLine(selectedRow);
            openSection(line.getUiSection());
          }
        }
      }
    };
    scrollPane = TableRowHeaderInstaller.install(table, rows, "", null, doubleClikedOnHeader);
    add(scrollPane);

    // création du pop-up menu permettant l'importer et l'export des données
    final JPopupMenu popupMenu = new JPopupMenu();
    importItem = new JMenuItem(NbBundle.getMessage(ModellingListFrottementTopComponent.class, "button.import.name"));
    importItem.setEnabled(this.isEditable());
    importItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        importTable();
      }
    });
    popupMenu.add(importItem);

    JMenuItem exportItem = new JMenuItem(NbBundle.getMessage(ModellingListFrottementTopComponent.class, "button.export.name"));
    exportItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        exportTable();
      }
    });
    popupMenu.add(exportItem);

    table.setComponentPopupMenu(popupMenu);
  }

  protected void importTable() {
    if (!this.isEditable()) {
      return;
    }

    CtuluFileChooser fileChooser = AbstractFileImporterProgressRunnable.getImportXLSCSVFileChooser();

    final int res = fileChooser.showDialog(CtuluUIForNetbeans.DEFAULT.getParentComponent(), NbBundle.getMessage(
      ModellingListCLimMsTopExportImportPopupBuilder.class,
      "button.import.name"));
    if (res == JFileChooser.APPROVE_OPTION) {
      final ModellingListFrottementLineTableModel result = CrueProgressUtils.showProgressDialogAndRun(
        new ModellingListFrottementProgressRunnable(fileChooser.getSelectedFile(), this),
        NbBundle.getMessage(ModellingListCLimMsTopExportImportPopupBuilder.class, "button.import.name"));
      if (result != null) {
        // on force la modif de la fenêtre pour pouvoir enregistrer
        this.setModified(true);
      }
    }
  }

  /**
   * Export csv/xlsx du tableau
   */
  protected void exportTable() {
    CtuluTableModelInterface defaultModel = new ModellingListFrottementExportTableModel(table);
    CtuluTableSimpleExporter.doExport(';', defaultModel, CtuluUIForNetbeans.DEFAULT);
  }

  private void displayDonFrtOnMouse(MouseEvent e) {
    int selectedColumn = table.columnAtPoint(e.getPoint());
    int selectedRow = table.rowAtPoint(e.getPoint());

    if (selectedColumn >= 0 && selectedRow >= 0) {
      final Object valueAt = table.getValueAt(selectedRow, selectedColumn);
      if (valueAt != null) {
        ModellingListFrottementLineCellRenderer cellRenderer = (ModellingListFrottementLineCellRenderer) table.getCellRenderer(selectedRow,
          selectedColumn);
        JComponent panel = (JComponent) cellRenderer.getTableCellRendererComponent(table, valueAt, true, true, selectedRow, selectedColumn);
        Rectangle cellRect = table.getCellRect(selectedRow, selectedColumn, true);
        int localX = e.getPoint().x - cellRect.x;
        panel.setSize(cellRect.width, cellRect.height);
        panel.doLayout();
        panel.revalidate();
        Component componentAt = panel.getComponentAt(localX, cellRect.height / 2);
        if (componentAt != null && componentAt instanceof JComboBox) {
          openDonFrt((DonFrt) ((JComboBox) componentAt).getSelectedItem());
        }
      }
    }
  }

  @Override
  protected void scenarioUnloaded() {
    close();
    scrollPane = null;
    table = null;
    tableModel = null;
  }

  public Long getSousModeleUid() {
    return sousModeleUid;
  }

  public void setSousModeleUid(Long brancheUid) {
    this.sousModeleUid = brancheUid;
  }

  public void reloadWithSousModeleUid(Long sousModeleUid) {
    if (!ObjectUtils.equals(this.sousModeleUid, sousModeleUid)) {
      this.sousModeleUid = sousModeleUid;
      scenarioReloaded();
    }
  }

  public void setBrancheUid(Long uid) {
    CatEMHBranche branche = (CatEMHBranche) getScenario().getIdRegistry().getEmh(uid);
    setSousModeleUid(branche.getParent().getUiId());
    brancheToSelect = branche;
  }

  public void reloadWithBrancheUid(Long uid) {
    CatEMHBranche branche = (CatEMHBranche) getScenario().getIdRegistry().getEmh(uid);
    reloadWithSousModeleUid(branche.getParent().getUiId());
    scrollToBranche(branche);
  }

  void writeProperties(java.util.Properties p) {
    p.setProperty("version", "1.0");
  }

  void readProperties(java.util.Properties p) {
  }

  @Override
  protected void setEditable(boolean b) {
    editable = b;
    if (tableModel != null) {
      tableModel.setEditable(b);
    }
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables

  //  public PopupReceiver createPopupReceiverOpenDfrt() {
//    CtuluPopupListener.PopupReceiver mouseListener = new CtuluPopupListener.PopupReceiver() {
//      @Override
//      public void popup(final MouseEvent _evt) {
//        JPopupMenu menu = new JPopupMenu();
//        JMenuItem add = menu.add(NbBundle.getMessage(ModellingListFrottementTopComponent.class, "OpenFrt.Menu"));
//        if (isModified()) {
//          add.setEnabled(false);
//          add.setToolTipText(NbBundle.getMessage(ModellingListFrottementTopComponent.class, "OpenFrtImpossibleModificationDone"));
//        } else {
//          add.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//              JComboBox cb = (JComboBox) _evt.getSource();
//              DonFrt frt = (DonFrt) cb.getSelectedItem();
//              if (frt != null) {
//                ModellingOpenDFRTNodeAction.open(getSousModele().getUiId(), frt.getNom(), false);
//              }
//            }
//          });
//        }
//        menu.show((JComponent) _evt.getSource(), _evt.getX(), _evt.getY());
//      }
//    };
//    return mouseListener;
//  }
  protected void openDonFrt(DonFrt frt) {
    if (isModified()) {
      DialogHelper.showWarn(NbBundle.getMessage(ModellingListFrottementTopComponent.class, "OpenFrtImpossibleModificationDone"));
    } else {
      ModellingOpenDFRTNodeAction.open(getSousModele().getUiId(), frt.getNom(), false);
    }
  }

  protected void openSection(Long sectionUid) {
    if (sectionUid == null) {
      return;
    }
    if (isModified()) {
      DialogHelper.showWarn(NbBundle.getMessage(ModellingListFrottementTopComponent.class, "OpenFrtImpossibleModificationDone"));
    } else {
      ModellingOpenEMHNodeAction.open(getScenario().getIdRegistry().getEmh(sectionUid), true);
    }
  }

  public List<ColumnHeaderContent> createHeader() {
    List<LitNomme> litNomme = getCcm().getLitNomme().getLitNomme();
    List<ColumnHeaderContent> header = new ArrayList<>();
    header.add(new ColumnHeaderContent(BusinessMessages.getString("DonPrtGeoProfilSection.shortName")));
    header.add(new ColumnHeaderContent(litNomme.get(0).getNom()));
    for (int i = 1; i < litNomme.size(); i++) {
      header.add(new ColumnHeaderContent(litNomme.get(i - 1).getNom() + BusinessMessages.ENTITY_SEPARATOR + litNomme.get(i).getNom()));
    }
    header.add(new ColumnHeaderContent(litNomme.get(litNomme.size() - 1).getNom()));
    return header;
  }

  public List<String> createHeaderString() {
    List<LitNomme> litNomme = getCcm().getLitNomme().getLitNomme();
    List<String> header = new ArrayList<>();
    header.add(litNomme.get(0).getNom());
    for (int i = 1; i < litNomme.size(); i++) {
      header.add(litNomme.get(i - 1).getNom() + BusinessMessages.ENTITY_SEPARATOR + litNomme.get(i).getNom());
    }
    header.add(litNomme.get(litNomme.size() - 1).getNom());
    return header;
  }

  private void scrollToBranche(CatEMHBranche branche) {

    TIntArrayList findRowFor = tableModel.findRowsFor(branche);
    table.clearSelection();
    int nb = findRowFor.size();
    for (int i = nb - 1; i >= 0; i--) {
      int row = findRowFor.get(i);
      if (row >= 0) {
        Rectangle cellRect = table.getCellRect(row, 0, true);
        table.scrollRectToVisible(cellRect);
        table.getSelectionModel().addSelectionInterval(row, row);
      }
    }
  }

  boolean isEditable() {
    return editable;
  }
}
