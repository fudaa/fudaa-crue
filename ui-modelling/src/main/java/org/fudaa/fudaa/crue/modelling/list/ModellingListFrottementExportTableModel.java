/*
GPL 2
 */
package org.fudaa.fudaa.crue.modelling.list;

import jxl.write.Label;
import jxl.write.WritableCell;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTableModelInterface;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;

/**
 * Un modèle pour adapter l'export cvs/Excel de la liste des frottement
 *
 * @author Frederic Deniger
 */
public class ModellingListFrottementExportTableModel implements CtuluTableModelInterface {
    //l'indice de la premiere colonne
    private final int idxColumnBrancheName = 0;
    //l'offset impliqué par la première colonne
    private final int offsetToApply = 1;
    private final JTable listFrtTable;
    private final List<String> rowNames;

    public ModellingListFrottementExportTableModel(JTable listFrtTable) {
        this.listFrtTable = listFrtTable;
        //le noms des lignes.
        rowNames = (List<String>) listFrtTable.getClientProperty(ModellingListFrottementTopComponent.PROPERTY_FOR_ROW_NAMES);
    }

    @Override
    public int[] getSelectedRows() {
        return null;
    }

    /**
     * @param rowIndex    indice de la ligne
     * @param columnIndex de la colonne
     * @return la valeur a utliser en (rowIndex,columnIndex). Pour la première colonne on utilise le nom de la ligne
     */
    @Override
    public Object getValue(int rowIndex, int columnIndex) {
        //on utilise le noms des lignes
        if (columnIndex == idxColumnBrancheName) {
            if (rowNames == null || rowNames.size() < rowIndex) {
                return CtuluLibString.EMPTY_STRING;
            }
            return rowNames.get(rowIndex);
        }
        Object val = listFrtTable.getValueAt(rowIndex, columnIndex - offsetToApply);
        if (val instanceof Node.Property) {
            try {
                return ((Node.Property) val).getValue();
            } catch (IllegalAccessException | InvocationTargetException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
        return val;
    }

    @Override
    public String getColumnName(int colIdx) {
        if (colIdx == idxColumnBrancheName) {
            return org.openide.util.NbBundle.getMessage(ModellingListFrottementExportTableModel.class, "listFrottementExport.first.column.name");
        }
        return listFrtTable.getColumnName(colIdx - offsetToApply);
    }

    @Override
    public WritableCell getExcelWritable(int _rowInModel, int _colInModel, int _rowInXls, int _colInXls) {
        final Object o = getValue(_rowInModel, _colInModel);
        if (o == null) {
            return null;
        }
        String s = o.toString();
        if (s == null) {
            return null;
        }
        s = s.trim();
        if (s.length() == 0) {
            return null;
        }
        return new Label(_colInXls, _rowInXls, s);
    }

    @Override
    public int getMaxCol() {
        return offsetToApply + listFrtTable.getColumnCount();
    }

    @Override
    public int getMaxRow() {
        return listFrtTable.getRowCount();
    }

    @Override
    public List<String> getComments() {
        return Collections.emptyList();
    }
}
