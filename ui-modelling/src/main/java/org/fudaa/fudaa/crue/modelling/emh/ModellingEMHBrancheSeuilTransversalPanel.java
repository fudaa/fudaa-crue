/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.emh;

import com.Ostermiller.util.CSVParser;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.*;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.edition.bean.BrancheEditionContent;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.fudaa.crue.common.helper.ToStringInternationalizableCellRenderer;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.common.view.DefaultOutlineViewEditor;
import org.fudaa.fudaa.crue.common.view.NodeEditionHelper;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 * Branche Type 2
 *
 * @author fred
 */
public class ModellingEMHBrancheSeuilTransversalPanel extends JPanel implements ModellingEMHBrancheSpecificEditor {

  final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(
          ModellingScenarioService.class);
  final PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
  final ModellingEMHBrancheTopComponent parent;
  final JComboBox cbFormule;
  final SeuilView seuilView;

  ModellingEMHBrancheSeuilTransversalPanel(CatEMHBranche branche, ModellingEMHBrancheTopComponent parent) {
    super(new BorderLayout(10, 10));
    this.parent = parent;
    JPanel top = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
    top.add(new JLabel(org.openide.util.NbBundle.getMessage(ModellingEMHBrancheSeuilTransversalPanel.class, "FormulePdc.DisplayName")));
    cbFormule = new JComboBox(EnumFormulePdc.values());
    cbFormule.setRenderer(new ToStringInternationalizableCellRenderer());
    DonCalcSansPrtBrancheSeuilTransversal dcsp = EMHHelper.selectFirstOfClass(branche.getInfosEMH(), DonCalcSansPrtBrancheSeuilTransversal.class);
    if (dcsp != null) {
      cbFormule.setSelectedItem(dcsp.getFormulePdc());
    }
    cbFormule.setEnabled(false);
    top.add(cbFormule);
    cbFormule.addActionListener(parent.modifiedActionListener);
    add(top, BorderLayout.NORTH);
    seuilView = new SeuilView();
    seuilView.initWith(branche);
    add(seuilView);
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return null;
  }

  @Override
  public void setEditable(boolean editable) {
    seuilView.setEditable(editable);
    cbFormule.setEnabled(editable);
  }

  @Override
  public void fillWithData(BrancheEditionContent content) {
    DonCalcSansPrtBrancheSeuilTransversal dcsp = new DonCalcSansPrtBrancheSeuilTransversal();
    dcsp.setFormulePdc((EnumFormulePdc) cbFormule.getSelectedItem());
    dcsp.setElemSeuilAvecPdc(seuilView.getElemSeuilAvecPdc());
    content.setDcsp(dcsp);
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  private class SeuilView extends DefaultOutlineViewEditor {

    protected void initWith(CatEMHBranche branche) {
      DonCalcSansPrtBrancheSeuilTransversal dcsp = EMHHelper.selectFirstOfClass(branche.getInfosEMH(), DonCalcSansPrtBrancheSeuilTransversal.class);
      List<ElemSeuilAvecPdcNode> nodes = new ArrayList<>();
      if (dcsp != null) {
        List<ElemSeuilAvecPdc> elemSeuilAvecPdc = dcsp.getElemSeuilAvecPdc();
        CrueConfigMetier ccm = getCcm();
        for (ElemSeuilAvecPdc elemSeuilAvecPdcNode : elemSeuilAvecPdc) {
          nodes.add(new ElemSeuilAvecPdcNode(elemSeuilAvecPdcNode.clone(), ccm, perspectiveServiceModelling));
        }
      }
      getExplorerManager().setRootContext(NodeHelper.createNode(nodes, "ElemSeuilAvecPdc", false));
      DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
    }

    @Override
    protected void changeInNode(String propertyName) {
      parent.setModified(true);
    }

    @Override
    protected JButton createPasteComponent() {
      JButton button = new JButton(NbBundle.getMessage(ModellingEMHBrancheSeuilTransversalPanel.class, "Paste.DisplayName"));
      button.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          paste();
        }
      });
      return button;
    }

    protected void paste() {
      List<ElemSeuilAvecPdcNode> pastData = getPastData(getCcm(), perspectiveServiceModelling);
      if (!pastData.isEmpty()) {
        NodeEditionHelper.addNodesFromClipboard(getExplorerManager(), pastData);
      }
    }

    List<ElemSeuilAvecPdc> getElemSeuilAvecPdc() {
      List<ElemSeuilAvecPdc> res = new ArrayList<>();
      Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
      for (Node node : nodes) {
        ElemSeuilAvecPdc elemSeuil = node.getLookup().lookup(ElemSeuilAvecPdc.class);
        res.add(elemSeuil);
      }
      return res;
    }

    @Override
    protected void addElement() {
      ElemSeuilAvecPdc elem = new ElemSeuilAvecPdc(getCcm());
      Children children = getExplorerManager().getRootContext().getChildren();
      final ElemSeuilAvecPdcNode elemPdtNode = new ElemSeuilAvecPdcNode(elem, getCcm(), perspectiveServiceModelling);
      children.add(new Node[]{elemPdtNode});
      DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
    }

    private CrueConfigMetier getCcm() {
      return modellingScenarioService.getSelectedProjet().getPropDefinition();
    }

    @Override
    protected void createOutlineView() {
      view = new OutlineView(StringUtils.EMPTY);
      view.setPropertyColumns(CrueConfigMetierConstants.PROP_ZSEUIL,
              ElemSeuilAvecPdcNode.getZSeuilDisplayName(),
              CrueConfigMetierConstants.PROP_LARGEUR,
              ElemSeuilAvecPdcNode.getLargeurDisplayName(),
              CrueConfigMetierConstants.PROP_COEF_D,
              ElemSeuilAvecPdcNode.getCoefDDisplayName(),
              CrueConfigMetierConstants.PROP_COEFPDC,
              ElemSeuilAvecPdcNode.getCoefPdcDisplayName());
      view.getOutline().setColumnHidingAllowed(false);
      view.getOutline().setRootVisible(false);
      view.setDragSource(true);
      view.setDropTarget(true);
    }
  }


  public static List<ElemSeuilAvecPdcNode> getPastData(CrueConfigMetier ccm, PerspectiveServiceModelling perspective) {
    try {
      String toCopy = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
      String[][] parse = CSVParser.parse(toCopy, '\t');
      List<ElemSeuilAvecPdcNode> contents = new ArrayList<>();
      for (String[] strings : parse) {
        int decal = 0;
        if (strings.length >= 5) {
          decal = 1;
        }
        if (strings.length >= 1 + decal) {
          ElemSeuilAvecPdc newContent = new ElemSeuilAvecPdc(ccm);
          contents.add(new ElemSeuilAvecPdcNode(newContent, ccm, perspective));
          try {
            newContent.setZseuil(Double.parseDouble(strings[decal]));
          } catch (NumberFormatException numberFormatException) {
          }
          if (strings.length >= 2 + decal) {
            try {
              newContent.setLargeur(Double.parseDouble(strings[1 + decal]));
            } catch (NumberFormatException numberFormatException) {
            }
            if (strings.length >= 3 + decal) {
              try {
                newContent.setCoefD(Double.parseDouble(strings[2 + decal]));
              } catch (NumberFormatException numberFormatException) {
              }
              if (strings.length >= 4 + decal) {
                try {
                  newContent.setCoefPdc(Double.parseDouble(strings[3 + decal]));
                } catch (NumberFormatException numberFormatException) {
                }
              }
            }
          }

        }
      }
      return contents;
    } catch (Exception exception) {
    }
    return Collections.emptyList();
  }
}
