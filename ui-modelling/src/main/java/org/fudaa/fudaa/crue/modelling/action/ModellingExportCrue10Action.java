/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import java.io.File;
import java.util.List;
import javax.swing.Action;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.dodico.crue.projet.OrdonnanceurCrue10;
import org.fudaa.dodico.crue.projet.ScenarioExporterCrue10;
import org.fudaa.fudaa.crue.common.helper.CrueFileChooserBuilder;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.FileHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.loader.LoaderService;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
          id = "org.fudaa.fudaa.crue.modelling.ModellingExportCrue10Action")
@ActionRegistration(displayName = "#CTL_ModellingExportCrue10Action")
@ActionReferences({
  @ActionReference(path = "Actions/Modelling", position = 11, separatorAfter = 12)
})
public final class ModellingExportCrue10Action extends AbstractModellingAction {

  final LoaderService loaderService = Lookup.getDefault().lookup(LoaderService.class);
  final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

  public ModellingExportCrue10Action() {
    super(false);
    putValue(Action.NAME, NbBundle.getMessage(ModellingExportCrue10Action.class, "CTL_ModellingExportCrue10Action"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingExportCrue10Action();
  }

  @Override
  public void doAction() {
    final CrueFileChooserBuilder builder = new CrueFileChooserBuilder(LoaderService.class).setTitle((String) getValue(Action.NAME)).
            setDefaultWorkingDirectory(loaderService.getDefaultDataHome()).setApproveText(NbBundle.getMessage(
            ModellingExportCrue10Action.class, "ExportFileChooser.ApproveText"));
    List<String> allExtensions = OrdonnanceurCrue10.getAllExtensions();
    builder.setSelectionApprover(new NoOverwrittenFileApprover(allExtensions));
    builder.setFilesOnly(true);
    builder.setFileHiding(true);
    File targetFile = builder.showSaveDialog();
    if (targetFile != null) {
      String higherGrammaire = configurationManagerService.getCoeurManager().getHigherGrammaire();
      CoeurConfig higherConfig = configurationManagerService.getCoeurManager().getCoeurConfigDefault(higherGrammaire);
      //on recupere le fichier dreg du modele en cours
      File dregFile=null;
      List<ManagerEMHModeleBase> modeles = scenarioService.getManagerScenarioLoaded().getFils();
      if(modeles.size()==1){
        FichierCrue fichierCrue = modeles.get(0).getFile(CrueFileType.DREG);
        if(fichierCrue!=null){
          dregFile= fichierCrue.getProjectFile(scenarioService.getSelectedProjet());
        }
      }
      ScenarioExporterCrue10 saver = new ScenarioExporterCrue10(FileHelper.getSansExtension(targetFile).getAbsolutePath(),
                                                                super.scenarioService.getScenarioLoaded(), higherConfig,dregFile);
      CtuluLogGroup bilan = saver.export();
      if (bilan.containsSomething()) {
        LogsDisplayer.displayError(bilan, (String) getValue(Action.NAME));
      } else {
        DialogHelper.showNotifyOperationTermine(NbBundle.getMessage(ModellingExportCrue10Action.class, "ExportCrue10.EndMessage"),
                                                NbBundle.getMessage(ModellingExportCrue10Action.class,
                                                                    "ExportCrue10.EndMessageDetails", FileHelper.getSansExtension(
                targetFile), higherGrammaire));
      }

    }

  }
}
