package org.fudaa.fudaa.crue.modelling.edition;

import gnu.trove.TLongObjectHashMap;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.EditionChangeActivity;
import org.fudaa.dodico.crue.edition.ReorderEMHProcess;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioModificationService;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.openide.util.Lookup;

/**
 *
 * @author deniger
 */
public class ListModificationProcess {

  public static class Data {

    public boolean active;
    public String commentaire;
  }
  protected final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
          ModellingScenarioModificationService.class);

  public void applyNoeuds(EMHSousModele sousModele, List<? extends CatEMHNoeud> emhs, TLongObjectHashMap<String> commentaires) {
    boolean reorderDone = ReorderEMHProcess.reorder(sousModele, emhs, EnumCatEMH.NOEUD);
    boolean commentaireModified = false;
    for (CatEMHNoeud noeud : emhs) {
      String newCommentaire = commentaires.get(noeud.getUiId());
      if (newCommentaire != null) {
        commentaireModified = true;
        if (!noeud.getCommentaire().equals(newCommentaire)) {
          noeud.setCommentaire(newCommentaire);
        }
      }
    }
    List<EnumModification> modifications = new ArrayList<>();
    if (reorderDone) {
      modifications.add(EnumModification.EMH_ORDER);
    }
    if (commentaireModified) {
      modifications.add(EnumModification.EMH_COMMENTAIRE);
    }
    if (!modifications.isEmpty() && modellingScenarioModificationService != null) {//pour les tests:
      modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(
              EnumModification.EMH_ORDER));
    }
  }

  public CtuluLog applyBranche(EMHSousModele sousModele, List<? extends CatEMHBranche> emhs, TLongObjectHashMap<Data> data, CrueConfigMetier ccm) {
    boolean reorderDone = ReorderEMHProcess.reorder(sousModele, emhs, EnumCatEMH.BRANCHE);
    boolean activeModified = false;
    CtuluLog res = null;
    boolean commentaireModified = false;
    for (CatEMHBranche branche : emhs) {
      Data newData = data.get(branche.getUiId());
      if (newData != null) {
        if (branche.getUserActive() != newData.active) {
          branche.setUserActive(newData.active);
          activeModified = true;
        }
        if (!branche.getCommentaire().equals(newData.commentaire)) {
          commentaireModified = true;
          branche.setCommentaire(newData.commentaire);
        }
      }
    }
    if (activeModified) {
      EditionChangeActivity change=new EditionChangeActivity();
      res=change.propagateActivityChanged(sousModele.getParent(),emhs, ccm);
    }
    List<EnumModification> modifications = new ArrayList<>();
    if (reorderDone) {
      modifications.add(EnumModification.EMH_ORDER);
    }
    if (activeModified) {
      modifications.add(EnumModification.ACTIVITY);
    }
    if (commentaireModified) {
      modifications.add(EnumModification.EMH_COMMENTAIRE);
    }
    if (!modifications.isEmpty() && modellingScenarioModificationService != null) {//pour les tests:
      modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(modifications));
    }
    return res;
  }
}
