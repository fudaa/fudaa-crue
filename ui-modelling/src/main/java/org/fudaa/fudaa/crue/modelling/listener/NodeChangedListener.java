package org.fudaa.fudaa.crue.modelling.listener;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.fudaa.fudaa.crue.modelling.AbstractModellingTopComponent;

/**
 *
 * @author Frédéric Deniger
 */
public class NodeChangedListener implements PropertyChangeListener {
  private final AbstractModellingTopComponent target;

  public NodeChangedListener(AbstractModellingTopComponent target) {
    this.target = target;
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    target.setModified(true);
  }
  
}
