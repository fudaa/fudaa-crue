/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import javax.swing.Action;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioModificationService;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
id = "org.fudaa.fudaa.crue.modelling.ModellingClose")
@ActionRegistration(displayName = "#CTL_ModellingClose")
@ActionReferences({
  @ActionReference(path = "Actions/Modelling", position = 3333)
})
public final class ModellingCloseAction extends AbstractModellingAction {

  final SelectedPerspectiveService selectedPerspectiveService = Lookup.getDefault().lookup(SelectedPerspectiveService.class);
  final PerspectiveServiceModelling perspectiveServiceModelling = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);


  public ModellingCloseAction() {
    super(false);
    putValue(Action.NAME, NbBundle.getMessage(ModellingCloseAction.class, "CTL_ModellingClose"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingCloseAction();
  }

  @Override
  protected boolean validationAction() {
    return perspectiveServiceModelling.confirmClose();
  }

  @Override
  public void doAction() {
    if (scenarioService.unloadScenario()) {
      PerspectiveServiceModelling perpective = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
      ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
              ModellingScenarioModificationService.class);
      perpective.setDirty(false);
      modellingScenarioModificationService.clear();
      selectedPerspectiveService.activePerspective(PerspectiveEnum.STUDY);
    }


  }
}
