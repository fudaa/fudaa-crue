package org.fudaa.fudaa.crue.modelling.services;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.Crue10FileFormatOCAL;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.ui.OrdCalcScenarioUiState;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.ScenarioSaverProcess;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * Valide le scenario et sauvegarde.
 *
 * @author deniger
 */
public class ModellingSaveScenarioCallable implements Callable<CtuluLogGroup> {
  private final ModellingScenarioServiceImpl modellingScenarioServiceImpl = Lookup.getDefault().lookup(ModellingScenarioServiceImpl.class);
  private final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
      ModellingScenarioModificationService.class);
  private final Set<String> fileModified = new HashSet<>();

  public ModellingSaveScenarioCallable() {
  }

  public Set<String> getModifiedFile() {
    synchronized (this) {
      return fileModified;
    }
  }

  @Override
  public CtuluLogGroup call() throws Exception {
    if (!modellingScenarioModificationService.isScenarioModified()) {
      return null;
    }
    ModellingValidationProcessor valid = new ModellingValidationProcessor(modellingScenarioServiceImpl);
    CtuluLogGroup run = valid.run(null);
    if (run.containsFatalError()) {
      return run;
    }
    File tempDir = getTargetDir();
    final ManagerEMHScenario managerScenarioLoaded = modellingScenarioServiceImpl.getManagerScenarioLoaded();
    ScenarioSaverProcess saver = new ScenarioSaverProcess(modellingScenarioServiceImpl.getScenarioLoaded(), managerScenarioLoaded,
        modellingScenarioServiceImpl.getSelectedProjet());
    saver.setOtherDir(tempDir);
    CtuluLogGroup saveWithScenarioType = saver.saveWithScenarioType();
    if (saveWithScenarioType.containsFatalError()) {
      return saveWithScenarioType;
    }
    CtuluLogGroup res = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    res.setDescription("save.scenario.log");
    CtuluLog log = res.createNewLog("save.scenario.done");
    //on copie les nouveaux fichiers ecrit dans un répertoire temp vers les bon fichiers
    Map<String, File> savedFiles = modellingScenarioServiceImpl.getSelectedProjet().getInputFiles(managerScenarioLoaded, tempDir);
    Map<String, File> destFiles = modellingScenarioServiceImpl.getSelectedProjet().getInputFiles(managerScenarioLoaded);
    for (File dest : destFiles.values()) {
      String error = CtuluLibFile.canWrite(dest);
      if (error != null) {
        log.addSevereError(error);
      }
    }
    if (log.containsSevereError()) {
      return res;
    }
    for (Map.Entry<String, File> entry : savedFiles.entrySet()) {
      String id = entry.getKey();
      File savedFile = entry.getValue();
      File destFile = destFiles.get(id);
      if (savedFile.isFile()) {
        if (!destFile.exists() || CrueFileHelper.isDifferentFiles(savedFile, destFile)) {
          CrueFileHelper.copyFileWithSameLastModifiedDate(savedFile, destFile);
          log.addInfo("save.scenario.file.done", id);
          fileModified.add(id);
        }
      }
    }
    saveUiOCAL(res);
    modellingScenarioModificationService.clearScenarioModifiedState();
    if (log.isEmpty()) {
      return null;
    }
    List<CtuluLogGroup> groups = saveWithScenarioType.getGroups();
    if (!groups.isEmpty()) {
      res.addGroup(groups.get(0));
    }
    return res;
  }

  private void saveUiOCAL(CtuluLogGroup res) {
    final ManagerEMHScenario managerScenarioLoaded = modellingScenarioServiceImpl.getManagerScenarioLoaded();
    //on sauvegarde les données ui des ocal
    if (modellingScenarioModificationService.isScenarioOcalModified()) {
      final CtuluLog uiOcalLog = res.createNewLog("loader.writeUiOcal");
      final EMHProjet projet = modellingScenarioServiceImpl.getSelectedProjet();
      final File uiOcalFile = projet.getInfos().getUiOCALFile(managerScenarioLoaded);
      //on créé le dossier parent s'il n'existe pas.
      File parentDir = uiOcalFile.getParentFile();
      if (parentDir != null && !parentDir.exists()) {
        parentDir.mkdirs();
      }
      String error = CtuluLibFile.canWrite(uiOcalFile);
      if (error != null) {
        uiOcalLog.addSevereError(error);
      } else {
        final OrdCalcScenarioUiState uiOCalData = managerScenarioLoaded.getUiOCalData();
        final Crue10FileFormat fmt = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.OCAL, projet.getCoeurConfig());
        Crue10FileFormatOCAL ocalFmt = (Crue10FileFormatOCAL) fmt;
        ocalFmt.writeUiState(uiOCalData, uiOcalFile, uiOcalLog, projet.getCoeurConfig().
            getCrueConfigMetier());
      }
    }
  }

  private File getTargetDir() {
    File tempDir = modellingScenarioServiceImpl.getTempDir();
    File dir = new File(tempDir, "histo_" + Long.toString(System.currentTimeMillis()));
    if (dir.exists()) {
      try {
        dir = CtuluLibFile.createTempDir("histo_", tempDir);
      } catch (IOException ex) {
        Exceptions.printStackTrace(ex);
      }
    }
    return dir;
  }
}
