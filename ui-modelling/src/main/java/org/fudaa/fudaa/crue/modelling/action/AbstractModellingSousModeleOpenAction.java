/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.action;

import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.modelling.services.ModellingEMHVisibilityService;
import org.openide.util.Lookup;

import java.util.List;

/**
 * @author Frederic Deniger
 */
public abstract class AbstractModellingSousModeleOpenAction extends AbstractModellingOpenAction {
  final ModellingEMHVisibilityService modellingEMHVisibilityService = Lookup.getDefault().lookup(ModellingEMHVisibilityService.class);

  @Override
  protected void doAction() {
    List<EMHSousModele> sousModeles = scenarioService.getScenarioLoaded().getSousModeles();
    if (sousModeles.size() == 1) {
      doAction(sousModeles.get(0));
    } else {
      final Long sousModeleActifUid = modellingEMHVisibilityService.getContainerActivityVisibility().getSousModeleActifUid();
      EMHSousModele choose = (EMHSousModele) scenarioService.getScenarioLoaded().getIdRegistry().getEmh(sousModeleActifUid);
      if(choose==null){
        choose=sousModeles.get(0);
      }
      if (choose != null) {
        doAction(choose);
      }
    }
  }

  protected abstract void doAction(EMHSousModele sousModele);
}
