/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.list;

import gnu.trove.TLongObjectHashMap;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.edition.bean.ListBrancheContent;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.modelling.action.ModellingOpenListBranchesAddNodeAction;
import org.fudaa.fudaa.crue.modelling.edition.ListModificationProcess;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
//@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.modelling//ModellingListBrancheTopComponent//EN",
//                     autostore = false)
@TopComponent.Description(preferredID = ModellingListBrancheTopComponent.TOPCOMPONENT_ID,
iconBase = "org/fudaa/fudaa/crue/modelling/rond-orange_16.png",
persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ModellingListBrancheTopComponent.MODE, openAtStartup = false, position = 0)
public final class ModellingListBrancheTopComponent extends AbstractModellingListEditionTopComponent implements Lookup.Provider {
  //attention le mode modelling-listBranches doit être déclaré dans le projet branding (layer.xml)

  public static final String MODE = "modelling-listBranches";
  public static final String TOPCOMPONENT_ID = "ModellingListBrancheTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  private JButton btAddBranches;

  public ModellingListBrancheTopComponent() {
    setName(NbBundle.getMessage(ModellingListBrancheTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ModellingListBrancheTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    initComponents();
    createComponent(AbstractListContentNode.getOrdreDisplay());
  }

  @Override
  protected String getViewHelpCtxId() {
    return "vueListeBranches";
  }

  @Override
  protected JPanel buildNorthPanel() {
    assert btAddBranches == null;//c'est normalement appele que dans le constructeur:
    btAddBranches = new JButton(ModellingOpenListBranchesAddNodeAction.getAddNodeActionName());
    btAddBranches.addActionListener(e -> openAddNodeTopComponent());
    final JPanel top = super.buildNorthPanel();
    final JPanel bt = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    bt.add(btAddBranches);

    final JPanel res = new JPanel(new BorderLayout());
    res.add(top);
    res.add(bt, BorderLayout.SOUTH);
    return res;
  }

  private void openAddNodeTopComponent() {
    ModellingOpenListBranchesAddNodeAction.open((EMHSousModele) getEMHInLookup());
  }

  @Override
  protected void installPropertyColumns() {
    outlineView.setPropertyColumns(ListCommonProperties.PROP_NOM,
            ListBrancheContentNode.getBrancheNameDisplay(),
            ListCommonProperties.PROP_TYPE,
            AbstractListContentNode.getTypeDisplay(),
            ListBrancheContent.PROP_NOEUD_AMONT,
            ListBrancheContentNode.getNoeudAmontDisplayName(),
            ListBrancheContent.PROP_NOEUD_AVAL,
            ListBrancheContentNode.getNoeudAvalDisplayName(),
            ListCommonProperties.PROP_ACTIVE,
            AbstractListContentNode.getActiveDisplay(),
            ListBrancheContent.PROP_LONGUEUR,
            ListBrancheContentNode.getLongueurDisplayName(),
            ListCommonProperties.PROP_COMMENTAIRE,
            AbstractListContentNode.getCommentDisplay());
  }

  @Override
  public void valideModificationHandler() {
    final Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    final TLongObjectHashMap<ListModificationProcess.Data> dataByUid = new TLongObjectHashMap<>();
    final List<CatEMHBranche> branches = new ArrayList<>();
    for (final Node node : nodes) {
      final CatEMHBranche branche = node.getLookup().lookup(CatEMHBranche.class);
      final ListBrancheContent brancheContent = node.getLookup().lookup(ListBrancheContent.class);
      final ListModificationProcess.Data data = new ListModificationProcess.Data();
      data.active = brancheContent.isActive();
      data.commentaire = brancheContent.getCommentaire();
      dataByUid.put(branche.getUiId(), data);
      assert branche != null;
      branches.add(branche);
    }
    final CtuluLog log = new ListModificationProcess().applyBranche((EMHSousModele) getEMHInLookup(), branches, dataByUid, getCcm());
    if (log != null && log.isNotEmpty()) {
      LogsDisplayer.displayError(log, getName());
    }
    setModified(false);

  }

  @Override
  protected List<? extends Node> createNodes(final EMH in) {
    final ListBrancheContentFactory factory = new ListBrancheContentFactory();
    return factory.createNodes(((EMHSousModele) in).getBranches());
  }
  @Override
  protected void scenarioReloaded() {
    scenarioLoaded();
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
