/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import javax.swing.Icon;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioModificationService;
import org.openide.awt.*;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
id = "org.fudaa.fudaa.crue.modelling.ModellingSaveAction")
//iconBase ne semble pas fonctionner !
@ActionRegistration(displayName = "#CTL_ModellingSaveAction", iconBase = "org/fudaa/fudaa/crue/modelling/action/save.png")
@ActionReferences({
  @ActionReference(path = "Toolbars/SaveModelling", position = 1),
  @ActionReference(path = "Actions/Modelling", position = 1)
})
public final class ModellingSaveAction extends AbstractModellingAction implements PropertyChangeListener {

  final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
          ModellingScenarioModificationService.class);

  public ModellingSaveAction() {
    super(false);
    putValue(Action.NAME, NbBundle.getMessage(ModellingSaveAction.class, "CTL_ModellingSaveAction"));
    final Icon icon = ImageUtilities.image2Icon(ImageUtilities.loadImage("org/fudaa/fudaa/crue/common/icons/save.png"));
    putValue(LARGE_ICON_KEY, icon);
    putValue(SMALL_ICON, icon);
    modellingScenarioModificationService.addModifiedStateListener(this);
  }

  @Override
  protected boolean getEnableState() {
    return super.getEnableState() && modellingScenarioModificationService != null && modellingScenarioModificationService.isModified();
  }

  @Override
  protected void updateEnableState() {
    super.updateEnableState();
    Toolbar toolbar = ToolbarPool.getDefault().findToolbar("SaveModelling");
    if (toolbar != null) {
      boolean visible = isMyPerspectiveActivated();
      toolbar.setVisible(visible);
    }
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    updateEnableState();
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingSaveAction();
  }

  @Override
  public void doAction() {
    if (scenarioService.saveScenario()) {
      DialogHelper.showNotifyOperationTermine(NbBundle.getMessage(ModellingSaveAction.class, "ModellingSaveAction.SucceedMessage",
              scenarioService.getManagerScenarioLoaded().getNom()));

      PerspectiveServiceModelling perpective = Lookup.getDefault().lookup(PerspectiveServiceModelling.class);
      perpective.setDirty(false);

    } else {
      DialogHelper.showError(NbBundle.getMessage(ModellingSaveAction.class, "ModellingSaveAction.ErrorMessage",
              scenarioService.getManagerScenarioLoaded().getNom()));
    }


  }
}
