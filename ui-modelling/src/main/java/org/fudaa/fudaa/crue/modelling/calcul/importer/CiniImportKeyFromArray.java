/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul.importer;

import gnu.trove.TIntArrayList;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluDoubleParser;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.metier.cini.CiniImportKey;
import org.fudaa.dodico.crue.metier.emh.EnumSensOuv;
import org.openide.util.NbBundle;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Frederic Deniger
 */
public class CiniImportKeyFromArray {
  private final String[][] values;
  private final Set<String> acceptedEMH;
  private final Set<String> acceptedType;

  public CiniImportKeyFromArray(String[][] values,
                                Set<CiniImportKey> acceptedKey) {
    this.values = values;
    acceptedEMH = new HashSet<>();
    acceptedType = new HashSet<>();
    for (CiniImportKey ciniImportKey : acceptedKey) {
      acceptedEMH.add(ciniImportKey.getEmh());
      acceptedType.add(ciniImportKey.getTypeValue());
    }
  }

  int findType(String[] values) {
    for (int i = 1; i < values.length; i++) {
      String type = StringUtils.defaultString(values[i]).toUpperCase();
      if (acceptedType.contains(type)) {
        return i;
      }
    }
    return -1;
  }

  public CrueIOResu<Map<CiniImportKey, Object>> getKeys() {
    TIntArrayList linesIgnoredBecauseOfUnknownType = new TIntArrayList();
    TIntArrayList linesIgnoredBecauseOfWrongValue = new TIntArrayList();
    Set<CiniImportKey> doublons = new HashSet<>();
    Map<CiniImportKey, Object> res = new HashMap<>();
    int line = 1;
    CtuluDoubleParser doubleParser = new CtuluDoubleParser();
    for (String[] strings : values) {
      if (strings != null && strings.length >= 3) {
        String emhNom = StringUtils.defaultString(strings[0]).toUpperCase();
        if (acceptedEMH.contains(emhNom)) {
          int idx = findType(strings);
          if (idx < 0) {
            linesIgnoredBecauseOfUnknownType.add(line);
            continue;
          }
          Object value = null;
          if (idx < strings.length - 1) {
            boolean ok = true;
            final String valueAsString = strings[idx + 1];
            if (EnumSensOuv.OUV_VERS_BAS.toString().equals(valueAsString) || EnumSensOuv.OUV_VERS_HAUT.toString().equals(valueAsString)) {
              value = EnumSensOuv.valueOf(valueAsString);
            } else {
              try {
                value = doubleParser.parse(valueAsString);
              } catch (NumberFormatException numberFormatException) {
                Logger.getLogger(CiniImportKeyFromArray.class.getName()).log(Level.INFO, "message", numberFormatException);
                ok = false;
              }
            }
            if (ok) {
              CiniImportKey key = new CiniImportKey(emhNom, strings[idx]);
              if (res.containsKey(key)) {
                doublons.add(key);
              }
              res.put(key, value);
            } else {
              linesIgnoredBecauseOfWrongValue.add(line);
            }
          }
        }
      }
      line++;
    }
    CtuluLog log = createLogs(linesIgnoredBecauseOfUnknownType, linesIgnoredBecauseOfWrongValue, doublons);
    return new CrueIOResu<>(res, log);
  }

  private CtuluLog createLogs(TIntArrayList linesIgnoredBecauseOfUnknownType, TIntArrayList linesIgnoredBecauseOfWrongValue, Set<CiniImportKey> doublons) {
    CtuluLog log = new CtuluLog();
    int size = linesIgnoredBecauseOfUnknownType.size();
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        log.addWarn(NbBundle.getMessage(CiniImportKeyFromArray.class, "import.cini.typeUnknown",
            linesIgnoredBecauseOfUnknownType.get(i)));
      }
    }
    size = linesIgnoredBecauseOfWrongValue.size();
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        log.addWarn(NbBundle.getMessage(CiniImportKeyFromArray.class, "import.cini.wrongValue",
            linesIgnoredBecauseOfWrongValue.get(i)));
      }
    }
    if (!doublons.isEmpty()) {
      for (CiniImportKey ciniImportKey : doublons) {
        log.addWarn(NbBundle.getMessage(CiniImportKeyFromArray.class, "import.cini.doublons"),
            ciniImportKey);
      }
    }
    return log;
  }
}
