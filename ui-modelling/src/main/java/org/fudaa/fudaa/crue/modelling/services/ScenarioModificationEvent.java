package org.fudaa.fudaa.crue.modelling.services;

import java.util.*;

/**
 * @author deniger
 */
public class ScenarioModificationEvent {
  final Set<EnumModification> modifiedPart;

  public ScenarioModificationEvent(EnumModification... modifiedPart) {
    this.modifiedPart = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(modifiedPart)));
  }

  public ScenarioModificationEvent(Collection<EnumModification> modifiedPart) {
    this.modifiedPart = Collections.unmodifiableSet(new HashSet<>(modifiedPart));
  }

  public boolean isEMHNameModification() {
    return modifiedPart.contains(EnumModification.EMH_NAME) || modifiedPart.contains(EnumModification.EMH);
  }
}
