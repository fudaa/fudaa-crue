package org.fudaa.fudaa.crue.modelling.loi;

import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.fudaa.crue.modelling.action.AbstractModellingOpenTopNodeAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;

/**
 *
 *
 * @author Frédéric Deniger
 */
public class ProfilCasierOpenOnSelectedCasierNodeAction extends AbstractModellingOpenTopNodeAction<ProfilCasierEditorTopComponent> {

  public ProfilCasierOpenOnSelectedCasierNodeAction(String name) {
    super(name, ProfilCasierEditorTopComponent.MODE, ProfilCasierEditorTopComponent.TOPCOMPONENT_ID);
  }

  public ProfilCasierOpenOnSelectedCasierNodeAction() {
    super(NbBundle.getMessage(ProfilCasierOpenOnSelectedCasierNodeAction.class, "ModellingOpenProfilCasierNodeAction.DisplayName"), ProfilCasierEditorTopComponent.MODE, ProfilCasierEditorTopComponent.TOPCOMPONENT_ID);
  }

  @Override
  protected void activeWithValue(ProfilCasierEditorTopComponent opened, Node selectedNode) {
    Long uid = selectedNode.getLookup().lookup(Long.class);
    if (uid == null) {
      CatEMHCasier casier = selectedNode.getLookup().lookup(CatEMHCasier.class);
      uid = casier.getUiId();
    }
    opened.selectCasier(uid);
  }

  @Override
  protected TopComponent activateNewTopComponent(ProfilCasierEditorTopComponent newTopComponent, Node selectedNode) {
//    ProfilCasierEditorTopComponent top = new ProfilCasierEditorTopComponent();
    Long uid = null;
    if (selectedNode != null) {
      uid = selectedNode.getLookup().lookup(Long.class);
      if (uid == null) {
        CatEMHCasier casier = selectedNode.getLookup().lookup(CatEMHCasier.class);
        uid = casier.getUiId();
      }
    }
    newTopComponent.putClientProperty(ProfilSectionTopComponent.PROPERTY_INIT_UID, uid);
    return newTopComponent;
  }

  public static void open(Long uid, boolean reopen) {
    if (uid == null) {
      return;
    }
    ProfilCasierOpenOnSelectedCasierNodeAction action = null;
    if (reopen) {
      action = SystemAction.get(ProfilCasierOpenOnSelectedCasierNodeAction.Reopen.class);
    } else {
      action = SystemAction.get(ProfilCasierOpenOnSelectedCasierNodeAction.NewFrame.class);
    }
    AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(uid));
    action.performAction(new Node[]{node});

  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    if (activatedNodes.length == 1) {
      final Lookup lookup = activatedNodes[0].getLookup();
      Long uid = lookup.lookup(Long.class);
      if (uid == null) {
        return lookup.lookup(CatEMHCasier.class) != null;
      }
      return true;
    }
    return false;
  }

  public static class Reopen extends ProfilCasierOpenOnSelectedCasierNodeAction {

    public Reopen() {
      super(NbBundle.getMessage(ProfilCasierOpenOnSelectedCasierNodeAction.class, "ModellingOpenProfilCasierNodeAction.DisplayName"));
      setReopen(true);
    }
  }

  public static class NewFrame extends ProfilCasierOpenOnSelectedCasierNodeAction {

    public NewFrame() {
      super(NbBundle.getMessage(ProfilCasierOpenOnSelectedCasierNodeAction.class, "ModellingOpenProfilCasierNodeAction.NewFrame.DisplayName"));
      setReopen(false);
    }
  }
}
