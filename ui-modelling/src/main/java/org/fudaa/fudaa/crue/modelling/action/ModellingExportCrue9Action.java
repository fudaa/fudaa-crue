/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.modelling.action;

import java.io.File;
import java.util.List;
import javax.swing.Action;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.projet.OrdonnanceurCrue9;
import org.fudaa.dodico.crue.projet.ScenarioExporterCrue9;
import org.fudaa.fudaa.crue.common.helper.CrueFileChooserBuilder;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.FileHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.loader.LoaderService;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
          id = "org.fudaa.fudaa.crue.modelling.ModellingExportCrue9Action")
@ActionRegistration(displayName = "#CTL_ModellingExportCrue9Action")
@ActionReferences({
  @ActionReference(path = "Actions/Modelling", position = 10)
})
public final class ModellingExportCrue9Action extends AbstractModellingAction {

  final LoaderService loaderService = Lookup.getDefault().lookup(LoaderService.class);

  public ModellingExportCrue9Action() {
    super(false);
    putValue(Action.NAME, NbBundle.getMessage(ModellingExportCrue9Action.class, "CTL_ModellingExportCrue9Action"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ModellingExportCrue9Action();
  }

  @Override
  public void doAction() {
    final CrueFileChooserBuilder builder = new CrueFileChooserBuilder(LoaderService.class).setTitle((String) getValue(Action.NAME)).
            setDefaultWorkingDirectory(loaderService.getDefaultDataHome());
    List<String> allExtensions = OrdonnanceurCrue9.getAllExtensions();
    builder.setFilesOnly(true);
    builder.setFileHiding(true);
    builder.setSelectionApprover(new NoOverwrittenFileApprover(allExtensions));
    builder.setApproveText(NbBundle.getMessage(ModellingExportCrue10Action.class, "ExportFileChooser.ApproveText"));
    File targetFile = builder.showSaveDialog();
    if (targetFile != null) {
      CrueConfigMetier crueConfigMetier = scenarioService.getSelectedProjet().getCoeurConfig().getCrueConfigMetier();
      ScenarioExporterCrue9 saver = new ScenarioExporterCrue9(super.scenarioService.getScenarioLoaded(),
                                                              scenarioService.getManagerScenarioLoaded(), crueConfigMetier);
      saver.exportFor(FileHelper.getSansExtension(targetFile).getAbsolutePath());
      CtuluLogGroup bilan = saver.getErrorManager();
      if (bilan.containsSomething()) {
        LogsDisplayer.displayError(bilan, (String) getValue(Action.NAME));
      } else {
        DialogHelper.showNotifyOperationTermine(NbBundle.getMessage(ModellingExportCrue10Action.class, "ExportCrue9.EndMessage"),
                                                NbBundle.getMessage(ModellingExportCrue10Action.class, "Export.EndMessageDetails",
                                                                    FileHelper.getSansExtension(targetFile)));
      }

    }

  }
}
