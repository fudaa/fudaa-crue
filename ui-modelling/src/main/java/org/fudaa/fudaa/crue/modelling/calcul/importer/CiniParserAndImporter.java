/*
 GPL 2
 */
package org.fudaa.fudaa.crue.modelling.calcul.importer;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.metier.cini.CiniImportKey;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Node;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author Frederic Deniger
 */
public class CiniParserAndImporter {
  public static final String PROP_VALUE = "value";
  private final CiniImporter ciniImporter;
  private final String[][] values;

  public CiniParserAndImporter(final String[][] values, final Node mainNode) {
    this.values = values;
    ciniImporter = new CiniImporter(mainNode);
    ciniImporter.setLogIfIgnoredValue(false);
  }


  public void setExplorerManager(final ExplorerManager explorerManager) {
    ciniImporter.setExplorerManager(explorerManager);
  }

  public CtuluLog importData() {
    final List<Node> nodes = new ArrayList<>();
    for (final Node node : ciniImporter.getMainNode().getChildren().getNodes()) {
      nodes.addAll(Arrays.asList(node.getChildren().getNodes()));
    }
    final Map<CiniImportKey, Node> keys = new CiniImportKeyFromNodes(nodes).getKeys();
    final CiniImportKeyFromArray fromArray = new CiniImportKeyFromArray(values, keys.keySet());
    final CrueIOResu<Map<CiniImportKey, Object>> mergeData = fromArray.getKeys();

    ciniImporter.setToImport(mergeData.getMetier());
    ciniImporter.importData(mergeData.getAnalyse());
    return mergeData.getAnalyse();
  }
}
