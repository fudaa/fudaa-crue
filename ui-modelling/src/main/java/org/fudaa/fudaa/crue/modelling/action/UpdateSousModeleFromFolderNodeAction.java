package org.fudaa.fudaa.crue.modelling.action;

import com.memoire.bu.BuVerticalLayout;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.projet.importer.SmImportFromFolder;
import org.fudaa.dodico.crue.projet.importer.SmUpdateAction;
import org.fudaa.dodico.crue.projet.importer.SmUpdateActionPreprocessor;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOperationsData;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOptions;
import org.fudaa.fudaa.crue.common.CrueFileTypeFilter;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.action.DrsoFileApprover;
import org.fudaa.fudaa.crue.common.helper.CrueFileChooserBuilder;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.modelling.services.EnumModification;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioModificationService;
import org.fudaa.fudaa.crue.modelling.services.ModellingScenarioVisuService;
import org.fudaa.fudaa.crue.modelling.services.ScenarioModificationEvent;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.planimetry.layout.NetworkGisPositionnerResult;
import org.fudaa.fudaa.crue.planimetry.save.PlanimetryGisLoader;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.io.File;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Action permettant d'updater un sous-modèle à partir d'un dossier
 *
 * @author deniger
 */
public class UpdateSousModeleFromFolderNodeAction extends AbstractEditNodeAction {
  final ModellingScenarioVisuService modellingScenarioVisuService = Lookup.getDefault().lookup(ModellingScenarioVisuService.class);
  private final transient ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);
  protected final ModellingScenarioModificationService modellingScenarioModificationService = Lookup.getDefault().lookup(
    ModellingScenarioModificationService.class);

  public UpdateSousModeleFromFolderNodeAction() {
    super(NbBundle.getMessage(UpdateSousModeleFromFolderNodeAction.class, "UpdateSousModeleFromFolderNodeAction.Name"));
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    if (activatedNodes.length != 1) {
      return false;
    }
    return activatedNodes[0].getLookup().lookup(EMHSousModele.class) != null;
  }

  @Override
  protected void performAction(final Node[] activatedNodes) {
    final File selectedDrsoFile = chooseDrsoFile();
    if (selectedDrsoFile == null) {
      return;
    }

    final EMHSousModele ssModele = activatedNodes[0].getLookup().lookup(EMHSousModele.class);
    EMHSousModele target = (EMHSousModele) modellingScenarioVisuService.getModellingScenarioService().getScenarioLoaded().getIdRegistry().getEmh(ssModele.getUiId());
    SmUpdateOptions options = chooseOptions(target.getNom(), selectedDrsoFile.getAbsolutePath());
    if (options == null) {
      return;
    }
    final String updateActionDescription = NbBundle.getMessage(UpdateSousModeleFromFolderNodeAction.class, "UpdateSousModeleFromFolderNodeAction.UpdateBilan", ssModele.getNom(),
      selectedDrsoFile.getAbsolutePath());
    Crue10FileFormatFactory.VersionResult findVersion = Crue10FileFormatFactory.findVersion(selectedDrsoFile);
    if (findVersion.getLog().containsErrorOrSevereError()) {
      LogsDisplayer.displayError(findVersion.getLog(), updateActionDescription);
      return;
    }
    String version = findVersion.getVersion();
    final CoeurConfig coeurConfigDefault = configurationManagerService.getCoeurManager().getCoeurConfigDefault(version);
    final EMHScenario targetScenario = target.getParent().getParent();
    final CrueOperationResult<SmUpdateOperationsData> smUpdaterData = CrueProgressUtils.showProgressDialogAndRun(progressHandle -> {
        progressHandle.switchToIndeterminate();
        return new SmUpdateActionPreprocessor(target, coeurConfigDefault).process(selectedDrsoFile, options);
      }, updateActionDescription
    );
    if (smUpdaterData.getLogs().containsError()) {
      LogsDisplayer.displayError(smUpdaterData.getLogs(), "UpdateSousModeleFromFolderNodeAction.UpdateNotValid");
      return;
    }
    if (smUpdaterData.getResult().getEmhNameToAddOrUpdate().isEmpty()) {
      DialogHelper.showWarn(getName(), NbBundle.getMessage(UpdateSousModeleFromFolderNodeAction.class, "UpdateSousModeleFromFolderNodeAction.NoEMHToImport"));
      return;
    }
    String question = NbBundle.getMessage(UpdateSousModeleFromFolderNodeAction.class, "UpdateSousModeleFromFolderNodeAction.ValidateOperationQuestion");
    final boolean accepted = LogsDisplayer
      .displayErrorWithQuestion(smUpdaterData.getLogs().createCleanGroup(),
        NbBundle.getMessage(UpdateSousModeleFromFolderNodeAction.class, "UpdateSousModeleFromFolderNodeAction.ValidateOperation"),
        updateActionDescription, question, DialogHelper.NO_VERSION_FOR_PERSISTENCE);

    // modification
    if (accepted) {
      final CtuluLogGroup actionLogGroup = CrueProgressUtils.showProgressDialogAndRun(progressHandle -> {
        progressHandle.switchToIndeterminate();
        final SmUpdateAction res = new SmUpdateAction(target, coeurConfigDefault.getCrueConfigMetier());
        final CtuluLogGroup ctuluLogGroup = res.performImport(ssModele, smUpdaterData.getResult());
        try {
          NetworkGisPositionnerResult resultat = new NetworkGisPositionnerResult(targetScenario,true);
          PlanimetryGisLoader gisLoader = new PlanimetryGisLoader();
          String sourceSousModele=smUpdaterData.getResult().getSourceSousModele().getId();
          File config=new File(SmImportFromFolder.getConfigDir(selectedDrsoFile),sourceSousModele);
          gisLoader.loadSousModeleFromDestDir(target,config , resultat);
          modellingScenarioVisuService.getPlanimetryVisuPanel().getPlanimetryController()
            .reload(targetScenario, resultat, smUpdaterData.getResult().getEmhNameToAddOrUpdate());
        } catch (Throwable e) {
          Logger.getLogger(UpdateSousModeleFromFolderNodeAction.class.getSimpleName()).log(Level.SEVERE,"can't loadGIS",e);
        }
        return ctuluLogGroup;
      }, updateActionDescription);
      new DeleteSectionInterpoleeNotUsedHelper(targetScenario, target).operate();
      modellingScenarioModificationService.setScenarioModified(new ScenarioModificationEvent(EnumModification.values()));

      //affichage du bilan
      if (actionLogGroup.containsSomething()) {
        LogsDisplayer.displayError(actionLogGroup, NbBundle.getMessage(UpdateSousModeleFromFolderNodeAction.class, "UpdateSousModeleFromFolderNodeAction.OperationDone"));
      } else {
        DialogHelper.showNotifyOperationTermine(NbBundle.getMessage(UpdateSousModeleFromFolderNodeAction.class, "UpdateSousModeleFromFolderNodeAction.OperationDone"),
          updateActionDescription);
      }
    }
  }

  /**
   * @return options choisies par l'utilisateur
   */
  private SmUpdateOptions chooseOptions(String targetSousModele, String drsoFile) {
    JCheckBox cbImportAllSections = new JCheckBox(NbBundle.getMessage(UpdateSousModeleFromFolderNodeAction.class, "UpdateSousModeleFromFolderNodeAction.ImportSections"));
    JCheckBox cbImportCasiers = new JCheckBox(NbBundle.getMessage(UpdateSousModeleFromFolderNodeAction.class, "UpdateSousModeleFromFolderNodeAction.ImportCasiers"));
    JCheckBox cbImportBranches = new JCheckBox(NbBundle.getMessage(UpdateSousModeleFromFolderNodeAction.class, "UpdateSousModeleFromFolderNodeAction.ImportBranches"));
    cbImportAllSections.setSelected(true);
    cbImportCasiers.setSelected(true);
    cbImportBranches.setSelected(true);
    JPanel pn = new JPanel(new BuVerticalLayout(5));
    pn.add(cbImportAllSections);
    pn.add(cbImportCasiers);
    pn.add(cbImportBranches);
    NotifyDescriptor nd = new NotifyDescriptor.Confirmation(pn, getName());
    final String yesOption = NbBundle.getMessage(UpdateSousModeleFromFolderNodeAction.class, "UpdateSousModeleFromFolderNodeAction.OptionsAccepted");
    nd.setOptions(new Object[]{yesOption, NotifyDescriptor.CANCEL_OPTION});
    nd.createNotificationLineSupport()
      .setInformationMessage(NbBundle.getMessage(UpdateSousModeleFromFolderNodeAction.class, "UpdateSousModeleFromFolderNodeAction.UpdateBilan", targetSousModele,
        drsoFile));
    final Object notify = DialogDisplayer.getDefault().notify(nd);
    boolean equals = notify.equals(yesOption);
    if (!equals) {
      return null;
    }
    SmUpdateOptions options = new SmUpdateOptions(cbImportAllSections.isSelected(), cbImportCasiers.isSelected(), cbImportBranches.isSelected());
    if (options.nothingImported()) {
      DialogHelper.showError(getName(), NbBundle.getMessage(UpdateSousModeleFromFolderNodeAction.class, "UpdateSousModeleFromFolderNodeAction.NoOptionsSelected"));
      return chooseOptions(targetSousModele, drsoFile);
    }
    return options;
  }

  private File chooseDrsoFile() {
    //choix du fichier
    final File home = configurationManagerService.getDefaultDataHome();
    final String dialogTitle = NbBundle.getMessage(getClass(), "UpdateSousModeleFromFolderNodeAction.ChooseDrsoFileTitle");
    final CrueFileChooserBuilder builder = new CrueFileChooserBuilder(getClass())
      .setTitle(dialogTitle)
      .setDefaultWorkingDirectory(home)
      .setApproveText(NbBundle.getMessage(getClass(), "UpdateSousModeleFromFolderNodeAction.DrsoFileAccepted"));
    builder.setFileFilter(new CrueFileTypeFilter(Collections.singletonList(CrueFileType.DRSO)));
    builder.setSelectionApprover(new DrsoFileApprover(modellingScenarioVisuService.getModellingScenarioService().getSelectedProjet(),
      dialogTitle, false));

    return builder.showOpenDialog();
  }
}
