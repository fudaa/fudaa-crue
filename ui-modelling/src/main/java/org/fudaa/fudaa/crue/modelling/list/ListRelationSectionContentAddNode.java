package org.fudaa.fudaa.crue.modelling.list;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.List;
import javax.swing.Action;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizableTransformer;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.bean.ListCommonProperties;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflectionDouble;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.modelling.perspective.PerspectiveServiceModelling;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class ListRelationSectionContentAddNode extends AbstractListContentNode {

  final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);

  private void addPropertiesToSet(final Set set) {
    final ListRelationSectionContent relationSectionContent = getLookup().lookup(ListRelationSectionContent.class);
    final NamesContent names = getLookup().lookup(NamesContent.class);
    final String[] branchesNames = names.getBranches().toArray(new String[0]);
    final String[] sectionNames = names.getSection().toArray(new String[0]);
    final CrueConfigMetier propDefinition = modellingScenarioService.getSelectedProjet().getPropDefinition();
    this.addPropertyChangeListener(new SectionDataListener());
    try {
      final PropertySupportReflection brancheNom = PropertySupportReflection.createString(this, relationSectionContent, ListRelationSectionContent.PROP_BRANCHE_NOM,
              ListRelationSectionContent.PROP_BRANCHE_NOM,
              ListRelationSectionContentNode.getBrancheDisplay());
      brancheNom.setTags(branchesNames);
      set.put(brancheNom);

      final Node.Property nomSection = PropertySupportReflection.createString(this, relationSectionContent, ListCommonProperties.PROP_NOM,
              ListCommonProperties.PROP_NOM,
              ListRelationSectionContentNode.getSectionNameDisplay());
      PropertyCrueUtils.configureNoCustomEditor(nomSection);
      set.put(nomSection);
      //noeud amont
      final List<EnumSectionType> types = EnumSectionType.getAvailablesSectionType();
      final List<String> toi18n = ToStringInternationalizableTransformer.toi18n(types);
      Collections.sort(toi18n);
      toi18n.add(0, StringUtils.EMPTY);

      final PropertySupportReflection typeSectionProperty = new PropertySupportReflection(this, relationSectionContent.getSection(), EnumSectionType.class,
              ListRelationSectionContent.PROP_SECTION_TYPE_NOM);
      typeSectionProperty.setName(ListRelationSectionContent.PROP_SECTION_TYPE_NOM);
      typeSectionProperty.setDisplayName(ListRelationSectionContentNode.getSectionTypeDisplay());
      typeSectionProperty.setTags(toi18n.toArray(new String[0]));
      typeSectionProperty.setTranslationForObjects(ToStringInternationalizableTransformer.toi18nMap(types));
      PropertyCrueUtils.configureNoCustomEditor(typeSectionProperty);
      set.put(typeSectionProperty);

      final EnumSectionType typeSection = relationSectionContent.getSection().getSectionType();
      //section idem reference
      if (typeSection != null && typeSection.equals(EnumSectionType.EMHSectionIdem)) {
        final PropertySupportReflection reference = PropertySupportReflection.createString(this, relationSectionContent.getSection(),
                ListRelationSectionContent.PROP_REFERENCE_SECTION_IDEM, ListRelationSectionContent.PROP_REFERENCE_SECTION_IDEM,
                ListRelationSectionContentNode.getReferenceDisplay());
        PropertyCrueUtils.configureNoCustomEditor(reference);
        reference.setTags(sectionNames);
        set.put(reference);

        //dz
        final ItemVariable property = propDefinition.getProperty(CrueConfigMetierConstants.PROP_DZ);
        final Node.Property dz = PropertySupportReflectionDouble.createDouble(DecimalFormatEpsilonEnum.COMPARISON,this,
                relationSectionContent.getSection(), ListRelationSectionContent.PROP_DZ,
                ListRelationSectionContent.PROP_DZ, ListRelationSectionContentNode.getDzDisplay(), property);
        PropertyCrueUtils.configureNoCustomEditor(dz);
        set.put(dz);
      }

      if (StringUtils.isNotBlank(relationSectionContent.getBrancheNom())) {
        final PropertySupportReflection abscisseHydraulique = PropertySupportReflectionDouble.createDouble(DecimalFormatEpsilonEnum.COMPARISON,this, relationSectionContent,
                ListRelationSectionContent.PROP_ABSCISSE_HYDRAULIQUE,
                ListRelationSectionContent.PROP_ABSCISSE_HYDRAULIQUE, ListRelationSectionContentNode.getAbsicsseHydrauliqueDisplay(),
                propDefinition.getProperty(CrueConfigMetierConstants.PROP_XP));

        PropertyCrueUtils.configureNoCustomEditor(abscisseHydraulique);
        set.put(abscisseHydraulique);
      }

      //commentaire
      final Node.Property commentaire = new PropertySupportReflection(this, relationSectionContent.getSection(), String.class,
              ListCommonProperties.PROP_COMMENTAIRE);
      PropertyCrueUtils.setNullValueEmpty(commentaire);
      commentaire.setName(ListCommonProperties.PROP_COMMENTAIRE);
      commentaire.setDisplayName(AbstractListContentNode.getCommentDisplay());
      set.put(commentaire);
    } catch (final NoSuchMethodException ex) {
      Exceptions.printStackTrace(ex);
    }
  }

  public static class NamesContent {

    final List<String> branches;
    final List<String> sections;

    public NamesContent(final List<String> branches, final List<String> section) {
      this.branches = branches;
      this.sections = section;
    }

    public List<String> getBranches() {
      return branches;
    }

    public List<String> getSection() {
      return sections;
    }
  }

  public ListRelationSectionContentAddNode(final ListRelationSectionContent content,
                                           final PerspectiveServiceModelling perspectiveServiceModelling, final NamesContent namesContent) {
    super(Children.LEAF, Lookups.fixed(content, namesContent), perspectiveServiceModelling);
    setDisplayName(StringUtils.EMPTY);
  }

  private class SectionDataListener implements PropertyChangeListener {

    @Override
    public void propertyChange(final PropertyChangeEvent evt) {
      if (ListRelationSectionContent.PROP_BRANCHE_NOM.equals(evt.getPropertyName())
              || ListRelationSectionContent.PROP_SECTION_TYPE_NOM.equals(evt.getPropertyName())) {
        final Set set = getSheet().get(Sheet.PROPERTIES);
        final Property<?>[] properties = set.getProperties();
        for (final Property<?> property : properties) {
          set.remove(property.getName());
        }
        addPropertiesToSet(set);
      }
    }
  }

  @Override
  public Action[] getActions(final boolean context) {
    return new Action[0];
  }

  @Override
  public boolean canDestroy() {
    return isEditMode();
  }

  @Override
  protected Sheet createSheet() {
    addPropertyChangeListener(null);
    final Sheet sheet = Sheet.createDefault();
    final Sheet.Set set = Sheet.createPropertiesSet();

    addPropertiesToSet(set);
    sheet.put(set);
    return sheet;
  }
}
