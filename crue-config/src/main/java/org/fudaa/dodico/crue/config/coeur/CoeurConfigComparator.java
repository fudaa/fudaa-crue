package org.fudaa.dodico.crue.config.coeur;

import org.fudaa.dodico.crue.common.SafeComparator;

/**
 *
 * @author deniger
 */
public class CoeurConfigComparator extends SafeComparator<CoeurConfig> {

  @Override
  protected int compareSafe(CoeurConfig o1, CoeurConfig o2) {
    return compareString(o1.getName(), o2.getName());
  }
}
