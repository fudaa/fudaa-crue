package org.fudaa.dodico.crue.config.ccm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.xml.XmlVersionFinder;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.contrat.XmlFileConfig;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierReaderXML.DaoConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierReaderXML.DaoConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierReaderXML.DaoControleLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierReaderXML.DaoExtrapolationLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierReaderXML.DaoItemConstante;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierReaderXML.DaoItemEnum;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierReaderXML.DaoItemVariable;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierReaderXML.DaoNature;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierReaderXML.DaoNumerique;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierReaderXML.DaoTypeEnum;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierReaderXML.DaoValeurStrictable;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.config.loi.ItemTypeControleLoi;
import org.fudaa.dodico.crue.config.loi.ItemTypeExtrapolationLoi;
import org.joda.time.LocalDateTime;

/**
 * @author deniger
 */
public class CrueConfigMetierReader {

    private final static Logger LOGGER = Logger.getLogger(CrueConfigMetierReader.class.getName());
    private final XmlFileConfig coeurConfig;

    public CrueConfigMetierReader(final XmlFileConfig xmlFileConfig) {
        super();
        this.coeurConfig = xmlFileConfig;
    }

    public CrueIOResu<CrueConfigMetier> readConfigMetier(final File fichier) {
        FileInputStream in = null;
        final CtuluLog analyser = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
        final CrueIOResu<CrueConfigMetier> newData = new CrueIOResu<>();
        newData.setAnalyse(analyser);
        analyser.setDesc("configMetier.readFile");
        analyser.setDescriptionArgs(fichier.getAbsolutePath());
        final CrueConfigMetierReaderXML readerXML = new CrueConfigMetierReaderXML(coeurConfig);
        if (readerXML.isValide(fichier, analyser)) {
            CrueConfigMetier res = null;
            try {
                in = new FileInputStream(fichier);
                res = readConfigMetier(in, analyser);
            } catch (final FileNotFoundException e) {
                LOGGER.log(Level.FINE, "readConfigMetier", e);
                final String path = fichier.getAbsolutePath();
                analyser.addError("io.FileNotFoundException.error", path);
            } finally {
                CtuluLibFile.close(in);
            }
            newData.setMetier(res);
            if (!containProfil(newData.getMetier())) {
                analyser.addError("configMetier.loiProfilRequise");
            }
        }
        return newData;

    }

    private boolean loadItemVariables(final DaoConfigMetier res, final Map<String, PropertyNature> propNature, final CtuluLog analyser,
                                        final Map<String, PropertyNature> propEnum, final CrueConfigMetier propDefinition) {
        final Map<String, ItemVariable> mapPropDefinitions = new HashMap<>();
        for (final DaoItemVariable variable : res.Variables.Variables) {
            if (variable.Nature != null) {
                final String natureName = StringUtils.uncapitalize(variable.Nature.NomRef);
                final PropertyNature propertyNat = propNature.get(natureName);
                if (propertyNat == null) {
                    analyser.addError("configMetier.natureNotFound", natureName, variable.Nom);
                    return false;
                }
                final PropertyValidator propertyVal = this.getValidator(variable, propertyNat.getTypeNumerique(), analyser);
                if (propertyVal == null) {
                    return false;
                }
                final ItemVariable propertyDef;
                if (propertyNat.isDate()) {
                    propertyDef = new ItemVariable(StringUtils.uncapitalize(variable.Nom), variable.ValeurDefaut,
                            propertyNat, propertyVal);
                } else {
                    propertyDef = new ItemVariable(StringUtils.uncapitalize(variable.Nom), this.getDouble(
                            variable.ValeurDefaut, propertyNat.getTypeNumerique()), propertyNat, propertyVal);
                }
                mapPropDefinitions.put(propertyDef.getNom(), propertyDef);
            } else if (variable.TypeEnum != null) {
                final PropertyNature propertyNat = propEnum.get(variable.TypeEnum.NomRef);
                if (propertyNat == null) {
                    analyser.addError("configMetier.natureNotFound", variable.TypeEnum.NomRef, variable.Nom);
                    return false;
                }
                final PropertyValidator validator = new PropertyValidator(null, null);
                final ItemVariable enumDef = new ItemVariable(StringUtils.uncapitalize(variable.Nom), variable.EnumValeurDefaut,
                        propertyNat, validator);
                validator.setParamToValid(enumDef);
                mapPropDefinitions.put(enumDef.getNom(), enumDef);

            }
        }
        propDefinition.setPropDefinition(mapPropDefinitions);
        return true;
    }

    /**
     * @return true if ok
     */
    private boolean loadItemConstantes(final DaoConfigMetier res, final Map<String, PropertyNature> propNature, final CtuluLog analyser,
                                         final Map<String, PropertyNature> propEnum, final CrueConfigMetier propDefinition) {
        final Map<String, ItemConstant> constantes = new HashMap<>();
        //no constante -> ok
        if (res.Constantes == null || res.Constantes.Constantes == null) {
            return true;
        }
        for (final DaoItemConstante variable : res.Constantes.Constantes) {
            if (variable.Nature != null) {
                final PropertyNature propertyNat = propNature.get(StringUtils.uncapitalize(variable.Nature.NomRef));
                final PropertyValidator propertyVal = this.getValidator(variable, propertyNat.getTypeNumerique(), analyser);
                if (propertyVal == null) {
                    return false;
                }
                final ItemConstant propertyDef = new ItemConstant(StringUtils.uncapitalize(variable.Nom), this.getDouble(
                        variable.Valeur, propertyNat.getTypeNumerique()),
                        propertyNat, propertyVal);
                constantes.put(propertyDef.getNom(), propertyDef);
            }
        }
        propDefinition.setPropConstantes(constantes);
        return true;
    }

    private boolean containProfil(final CrueConfigMetier l) {
        if (l == null) {
            return false;
        }
        for (final ConfigLoi configLoi : l.getConfLoi().values()) {
            if (EnumTypeLoi.LoiPtProfil.equals(configLoi.getTypeLoi())) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return le dao
     */
    private CrueConfigMetier readConfigMetier(final InputStream in,
                                              final CtuluLog analyser) {
        final CrueConfigMetierReaderXML readerXML = new CrueConfigMetierReaderXML(coeurConfig);
        final CrueConfigMetierReaderXML.DaoConfigMetier res = readerXML.readDaos(in, analyser);
        if (analyser.containsSevereError() || res == null) {
            return null;
        }

        final Map<String, PropertyTypeNumerique> typesNumeriques = new HashMap<>();

        for (final DaoNumerique num : res.TypeNumeriques.Numeriques) {
            final PropertyTypeNumerique propertyNum = new PropertyTypeNumerique(StringUtils.uncapitalize(num.Nom), num.Infini,
                    Double.parseDouble(num.Infini));

            typesNumeriques.put(propertyNum.getNom(), propertyNum);
        }

        final Map<String, PropertyNature> propNature = new HashMap<>();

        for (final DaoNature nature : res.Natures.Natures) {
            final PropertyTypeNumerique propertyNum = typesNumeriques.get(StringUtils.uncapitalize(nature.TypeNumerique.NomRef));

            if (propertyNum != null) {
                String epsilonComparaisonFC = nature.EpsilonComparaisonFC;
                //vas de la grammaire 1.1.1
                if (epsilonComparaisonFC == null) {
                    epsilonComparaisonFC = nature.EpsilonComparaison;
                }
                final PropertyNature propertyNat = new PropertyNature(StringUtils.uncapitalize(nature.Nom),
                        new PropertyEpsilon(
                                Double.parseDouble(epsilonComparaisonFC), Double.parseDouble(nature.EpsilonPresentation)), nature.Unite, propertyNum);

                propNature.put(propertyNat.getNom(), propertyNat);
            }
        }

        final Map<String, PropertyNature> propEnum = new HashMap<>();
        //
        for (final DaoTypeEnum nature : res.TypeEnums.Enums) {
            final List<ItemEnum> enums = new ArrayList<>();
            if (nature.ItemEnum != null) {
                //Attention: s'il n'y a pas d'id dans les enums on suppose que le premier est celui avec l'id le plus eleve
                //utile pour la verbosité.
                int currentIdx = nature.ItemEnum.size();
                for (final DaoItemEnum object : nature.ItemEnum) {
                    int idx = --currentIdx;
                    if (object.Id != null) {
                        idx = Integer.parseInt(object.Id);
                    }
                    enums.add(new ItemEnum(idx, object.Nom));
                }
            }
            final PropertyNature propertyNat = new PropertyNature(nature.Nom, enums);
            propEnum.put(propertyNat.getNom(), propertyNat);
        }
        final PropertyNature propertyEnum = propEnum.get(CrueConfigMetierConstants.ENUM_LIT_NOMME);
        if (propertyEnum == null || propertyEnum.getEnumValues().size() != 5) {
            analyser.addSevereError("configMetier.litNomme5Values.error");
            return null;
        }
        final CrueConfigMetier propDefinition = new CrueConfigMetier(propNature, propEnum);
        if (!loadItemVariables(res, propNature, analyser, propEnum, propDefinition)) {
            return null;
        }
        if (!loadItemConstantes(res, propNature, analyser, propEnum, propDefinition)) {
            return null;
        }

        loadLois(analyser, res, propDefinition);


        return propDefinition;
    }

    private PropertyValidator getValidator(final DaoItemVariable variable, final PropertyTypeNumerique type, final CtuluLog log) {

        final NumberRangeValidator rangeValidate = this.getRange(variable.Nom, variable.MinValidite, variable.MaxValidite, type, log);
        if (rangeValidate == null) {
            log.addSevereError("configMetier.validityRange.error", variable.Nom);
        }
        final NumberRangeValidator rangeNormalite = this.getRange(variable.Nom, variable.MinNormalite, variable.MaxNormalite, type,
                log);
        if (rangeNormalite == null) {
            log.addSevereError("configMetier.nomaliteRange.error", variable.Nom);
        }
        if (rangeNormalite != null && rangeValidate != null) {
            final PropertyValidator validator = new PropertyValidator(rangeValidate, rangeNormalite);
            if (!isCorrectValidator(validator)) {
                log.addSevereError("configMetier.validator.error", variable.Nom);
            }
            return validator;
        }
        return null;
    }

    private PropertyValidator getValidator(final DaoItemConstante variable, final PropertyTypeNumerique type, final CtuluLog log) {

        final NumberRangeValidator rangeValidate = this.getRange(variable.Nom, variable.MinValidite, variable.MaxValidite, type, log);
        if (rangeValidate == null) {
            log.addSevereError("configMetier.validityRange.error", variable.Nom);
        }
        final NumberRangeValidator rangeNormalite = this.getRange(variable.Nom, variable.MinNormalite, variable.MaxNormalite, type,
                log);
        if (rangeNormalite == null) {
            log.addSevereError("configMetier.nomaliteRange.error", variable.Nom);
        }
        if (rangeNormalite != null && rangeValidate != null) {
            final PropertyValidator validator = new PropertyValidator(rangeValidate, rangeNormalite);
            if (!isCorrectValidator(validator)) {
                log.addSevereError("configMetier.validator.error", variable.Nom);
            }
            return validator;
        }
        return null;
    }

    private NumberRangeValidator getRange(final String nom, final DaoValeurStrictable min, final DaoValeurStrictable max,
                                          final PropertyTypeNumerique type, final CtuluLog log) {
        final NumberRangeValidator range = new NumberRangeValidator();
        // case of date:
        final LocalDateTime minDate = getDate(min.Valeur);
        if (minDate != null) {
            final LocalDateTime maxDate = getDate(max.Valeur);
            if (maxDate == null) {
                log.addSevereError("configMetier.dateValue.badFormat", nom, min.Valeur, max.Valeur);
                return null;
            }
            range.setMax(new Double(maxDate.toDateTime().getMillis()));
            range.setMaxName(DateDurationConverter.dateToXsdUI(maxDate));
            range.setMin(new Double(minDate.toDateTime().getMillis()));
            range.setMinName(DateDurationConverter.dateToXsdUI(minDate));
            return range;
        }
        // double case:
        try {
            range.setMin(this.getDouble(min.Valeur, type));
        } catch (final NumberFormatException e) {
            log.addSevereError("configMetier.minValue.badFormat", nom, min.Valeur);
            return null;
        }
        try {
            range.setMax(this.getDouble(max.Valeur, type));
        } catch (final NumberFormatException e) {
            log.addSevereError("configMetier.maxValue.badFormat", nom, max.Valeur);
            return null;
        }
        range.setMaxStrict(max.Strict);
        range.setMinStrict(min.Strict);
        return this.isCorrectRange(range) ? range : null;
    }

    private LocalDateTime getDate(final String value) {
        if (value == null || value.indexOf('T') < 0) {
            return null;
        }
        return DateDurationConverter.getDate(value);
    }

    private Double getDouble(final String value, final PropertyTypeNumerique type) {
        Double dble = null;

        if (!CtuluLibString.isEmpty(value)) {
            if (value.equals("+Infini")) {
                if (type == null) {
                    dble = Double.POSITIVE_INFINITY;
                } else {
                    dble = type.getInfini();
                }
            } else if (value.equals("-Infini")) {
                if (type == null) {
                    dble = Double.NEGATIVE_INFINITY;
                } else {
                    dble = -type.getInfini();
                }
            } else {
                dble = Double.valueOf(value);
            }
        }

        return dble;
    }

    private boolean isCorrectValidator(final PropertyValidator property) {
        return (property.getRangeValidate() != null) && (property.getRangeNormalite() != null)
                && (property.getRangeNormalite().getMin().doubleValue() >= property.getRangeValidate().getMin().doubleValue())
                && (property.getRangeNormalite().getMax().doubleValue() <= property.getRangeValidate().getMax().doubleValue());
    }

    private boolean isCorrectRange(final NumberRangeValidator range) {
        if (range.isMinStrict() && range.isMaxStrict()) {
            return range.getMin().doubleValue() < range.getMax().doubleValue();
        } else {
            return range.getMin().doubleValue() <= range.getMax().doubleValue();
        }
    }

    private void loadLois(final CtuluLog analyser, final CrueConfigMetierReaderXML.DaoConfigMetier res,
                          final CrueConfigMetier propDefinition) {
        //utile pour version 1.1.1
        final HashMap<String, String> newIdByOldValue = new HashMap<>();
        final Map<String, ItemTypeExtrapolationLoi> itemTypeExtrapolation = new HashMap<>();

        for (final DaoExtrapolationLoi extraLoi : res.TypeExtrapolLois.Extrapolations) {
            final ItemTypeExtrapolationLoi typeExtra = new ItemTypeExtrapolationLoi(StringUtils.uncapitalize(extraLoi.Nom));

            itemTypeExtrapolation.put(typeExtra.getNom(), typeExtra);
        }

        final Map<String, ItemTypeControleLoi> itemTypeControle = new HashMap<>();

        for (final DaoControleLoi ctrlLoi : res.TypeControleLois.Controles) {
            final ItemTypeControleLoi typeCtrl = new ItemTypeControleLoi(StringUtils.uncapitalize(ctrlLoi.Nom));

            itemTypeControle.put(typeCtrl.getNom(), typeCtrl);
        }

        final Set<EnumTypeLoi> typeLoiDone = new HashSet<>();
        if (res != null && CollectionUtils.isNotEmpty(res.ConfigLois.Lois)) {
            final List<ConfigLoi> loisLoaded = new ArrayList<>(res.ConfigLois.Lois.size());
            for (final DaoConfigLoi confLoi : res.ConfigLois.Lois) {
                final ItemTypeExtrapolationLoi extrapolInf;
                final ItemTypeExtrapolationLoi extrapolSup;
                EnumTypeLoi typeLoi = null;
                boolean error = false;
                try {
                    String typeLoiName = confLoi.Nom;
                    if (newIdByOldValue.containsKey(typeLoiName)) {
                        typeLoiName = newIdByOldValue.get(typeLoiName);
                    }
                    typeLoi = EnumTypeLoi.getTypeLoi(typeLoiName);
                    // ce teste est en doublon avec la xsd qui va vérifier les Type en tant qu'ID....
                    if (typeLoiDone.contains(typeLoi)) {
                        error = true;
                        analyser.addSevereError("configLoi.TypeLoi.doublon.error", confLoi.Nom);
                    } else {
                        typeLoiDone.add(typeLoi);
                    }
                } catch (final Exception e) {
                    analyser.addError(e.getMessage(), e);
                    analyser.addSevereError("configLoi.TypeLoi.error", confLoi.Nom);
                    error = true;
                }
                // valeur optionnelle
                extrapolInf = itemTypeExtrapolation.get(StringUtils.uncapitalize(confLoi.ExtrapolInf.NomRef));
                if (extrapolInf == null) {
                    analyser.addSevereError("configLoi.extrapolInf.error", confLoi.Nom, confLoi.ExtrapolInf.NomRef);
                    error = true;
                }

                extrapolSup = itemTypeExtrapolation.get(StringUtils.uncapitalize(confLoi.ExtrapolSup.NomRef));
                if (extrapolSup == null) {
                    analyser.addSevereError("configLoi.extrapolSup.error", confLoi.Nom, confLoi.ExtrapolSup.NomRef);
                    error = true;
                }
                final ItemTypeControleLoi itemTypeControleLoi = itemTypeControle.get(StringUtils.uncapitalize(confLoi.TypeControleLoi.NomRef));
                if (itemTypeControleLoi == null) {
                    analyser.addSevereError("configLoi.controle.error", confLoi.Nom, confLoi.TypeControleLoi.NomRef);
                    error = true;
                }

                final ItemVariable varAbs = propDefinition.getProperty(StringUtils.uncapitalize(confLoi.VarAbscisse.NomRef));
                if (varAbs == null) {
                    analyser.addSevereError("configLoi.varAbs.error", confLoi.Nom, confLoi.VarAbscisse);
                    error = true;
                }
                final ItemVariable varOrd = propDefinition.getProperty(StringUtils.uncapitalize(confLoi.VarOrdonnee.NomRef));
                if (varOrd == null) {
                    analyser.addSevereError("configLoi.varOrd.error", confLoi.Nom, confLoi.VarOrdonnee);
                    error = true;
                }
                if (!error) {
                    loisLoaded.add(new ConfigLoi(typeLoi, itemTypeControleLoi, extrapolInf, extrapolSup, varAbs, varOrd,
                            confLoi.Commentaire));
                }

            }
            //on teste les lois uniquement sur la dernière version de XSD.
            if(Crue10VersionConfig.getLastVersion().equals(coeurConfig.getXsdVersion())) {
                final Collection<EnumTypeLoi> loiTypes = EnumTypeLoi.getListTypeLoi();
                final Collection<?> removeAll = ListUtils.removeAll(loiTypes, typeLoiDone);
                if (removeAll.size() > 0) {
                    analyser.addSevereError("configLoi.LoiNotDefinedFound.error", StringUtils.join(removeAll, "; "));
                }
            }
            propDefinition.setConfigLoi(CrueConfigMetierReader.createMap(loisLoaded));
        } else {
            analyser.addSevereError("configLoi.noLoiFound.error");
        }
        final SeveriteManager validVerbosite = SeveriteManager.validVerbosite(propDefinition, analyser);
        propDefinition.setVerbositeManager(validVerbosite);
    }

    /**
     * @param url the url
     * @return
     */
    private CrueIOResu<CrueConfigMetier> readConfigMetier(final URL url) {

        final CrueIOResu<CrueConfigMetier> newData = new CrueIOResu<>();
        final CtuluLog analyser = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
        newData.setAnalyse(analyser);

        if (url == null) {
            analyser.addError("file.url.null.error");
            return newData;
        }
        final String readVersion = new XmlVersionFinder().getVersion(url);
        if (!readVersion.equals(coeurConfig.getXsdVersion())) {
            analyser.addSevereError("configFile.versionNotCompatible", url.getFile(), readVersion,
                    coeurConfig.getXsdVersion());
            return newData;
        }
        InputStream in = null;
        CrueConfigMetier res = null;
        try {
            in = url.openStream();
            res = readConfigMetier(in, analyser);
        } catch (final IOException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            analyser.addError("io.xml.error", e.getMessage());
        } finally {
            CtuluLibFile.close(in);
        }
        newData.setMetier(res);
        return newData;

    }

    /**
     * @param pathToResource l'adresse du fichier a charger commencant par /
     * @return la configuration metier lue
     */
    protected CrueIOResu<CrueConfigMetier> readConfigMetier(final String pathToResource) {
        return readConfigMetier(getClass().getResource(pathToResource));
    }

    private static Map<EnumTypeLoi, ConfigLoi> createMap(final List<ConfigLoi> readConfigLoi) {
        if (readConfigLoi == null) {
            return null;
        }
        final Map<EnumTypeLoi, ConfigLoi> configs = new HashMap<>(readConfigLoi.size());
        for (final ConfigLoi c : readConfigLoi) {
            configs.put(c.getTypeLoi(), c);
        }
        return configs;
    }
}
