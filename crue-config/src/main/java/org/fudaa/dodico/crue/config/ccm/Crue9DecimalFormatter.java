/*
GPL 2
 */
package org.fudaa.dodico.crue.config.ccm;

import org.fudaa.ctulu.CtuluNumberFormatFortran;

/**
 *
 * @author Frederic Deniger
 */
public class Crue9DecimalFormatter {

  private final CtuluNumberFormatFortran formatter;

  public Crue9DecimalFormatter(Crue9DecimalFormat format) {
    if (format != null && format.isActive()) {
      formatter = new CtuluNumberFormatFortran(format.getNumberOfDigits());
    } else {
      formatter = null;
    }
  }

  public String format(double in) {
    if (formatter == null) {
      return Double.toString(in);
    }
    return formatter.format(in);
  }

}
