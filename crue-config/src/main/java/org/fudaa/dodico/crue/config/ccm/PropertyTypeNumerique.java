package org.fudaa.dodico.crue.config.ccm;

/**
 * Definition d'une propriété de type numérique
 */
public class PropertyTypeNumerique {
    private final double infini;
    private final String nom;
    private final String infiniAbsValue;
    private final String infiniNegValue;

    /**
     * @param infini la valeur de l'infini
     * @param nom    de la proprieté
     */
    public PropertyTypeNumerique(String nom, String infiniAbsValue, double infini) {
        super();
        this.nom = nom;
        this.infiniAbsValue = infiniAbsValue;
        this.infiniNegValue = "-" + infiniAbsValue;
        this.infini = Math.abs(infini);
    }

    public boolean isEntier() {
        return "Tnu_Entier".equals(nom);
    }

    public double getInfini() {
        return infini;
    }

    public String getNom() {
        return nom;
    }

    /**
     * @return the infiniAbsValue
     */
    protected String getInfiniAbsValue() {
        return infiniAbsValue;
    }

    /**
     * @return the infiniNegValue
     */
    protected String getInfiniNegValue() {
        return infiniNegValue;
    }
}
