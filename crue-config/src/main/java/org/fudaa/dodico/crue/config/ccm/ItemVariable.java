package org.fudaa.dodico.crue.config.ccm;

import org.apache.commons.lang3.StringUtils;

/**
 * @author deniger Definition d'une propriete avec son nom,valeur par defaut,....
 */
public class ItemVariable extends ItemContentAbstract {
  private final Number defaultValue;
  private final String defaultStringValue;

  public ItemVariable(final String nom, final Number defaultValue, final PropertyNature nature, final PropertyValidator validator) {
    super(nom, nature, validator);
    this.defaultValue = defaultValue;
    defaultStringValue = null;
  }

  protected ItemVariable(final String nom, final String defaultStringValue, final PropertyNature nature,
                         final PropertyValidator validator) {
    super(nom, nature, validator);
    this.defaultStringValue = StringUtils.upperCase(defaultStringValue);
    this.defaultValue = null;
  }

  public String getDefaultEnumValue() {
    return defaultStringValue;
  }

  public Number getDefaultValue() {
    return defaultValue;
  }

  @Override
  protected boolean isDefaultOrConstantValue(final Number valueToTest) {
    if (valueToTest != null && defaultValue != null) {
      final double currentVal = valueToTest.doubleValue();
      if (nature.isInfiniPositif(defaultValue)) {
        return nature.isInfiniPositif(valueToTest);
      }
      if (nature.isInfiniNegatif(defaultValue)) {
        return nature.isInfiniNegatif(valueToTest);
      }
      if (isEntier()) {
        return valueToTest.intValue() == defaultValue.intValue();
      }
      return getEpsilon().isSame(currentVal, defaultValue.doubleValue());
    } else {
      return valueToTest == null && defaultValue == null;
    }
  }
}
