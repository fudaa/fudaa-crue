package org.fudaa.dodico.crue.config.ccm;

import java.text.NumberFormat;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author deniger
 */
public abstract class ItemContentAbstract {

  protected final PropertyNature nature;
  private final String nom;
  private final PropertyValidator validator;
  private final String displayName;

  protected ItemContentAbstract(final String nom, final PropertyNature nature, final PropertyValidator validator) {
    super();
    this.nom = nom;
    displayName = StringUtils.capitalize(nom);
    this.nature = nature;
    this.validator = validator;
    validator.setParamToValid(this);
  }

  protected abstract boolean isDefaultOrConstantValue(final Number val);

  public String format(final double value, final DecimalFormatEpsilonEnum type) {
    return getNature().format(value, type);
  }

  public String format(final Number value, final DecimalFormatEpsilonEnum type) {
    return getNature().format(value, type);
  }

  public NumberFormat getFormatter(final DecimalFormatEpsilonEnum type) {
    return nature.getFormatter(type);
  }

  public String formatWithoutInfini(final double value, final DecimalFormatEpsilonEnum type) {
    return getNature().formatWithoutInfini(value, type);
  }

  public String formatWithoutInfini(final Number value, final DecimalFormatEpsilonEnum type) {
    return getNature().formatWithoutInfini(value, type);
  }

  public PropertyEpsilon getEpsilon() {
    return getNature().getEpsilon();
  }

  public PropertyNature getNature() {
    return nature;
  }

  public String getNom() {
    return nom;
  }

  public String getDisplayNom() {
    return displayName;
  }

  public PropertyValidator getValidator() {
    return validator;
  }

  public boolean isEntier() {
    return nature != null && "Nbr".equals(nature.getNom());
  }

  public boolean isInfiniNegatif(final double value) {
    return nature.isInfiniNegatif(value);
  }

  public boolean isInfiniNegatif(final Number value) {
    return nature.isInfiniNegatif(value);
  }

  public boolean isInfiniPositif(final double value) {
    return nature.isInfiniPositif(value);
  }

  public boolean isInfiniPositif(final Number value) {
    return nature.isInfiniPositif(value);
  }

  public boolean needFormatter() {
    return getNature().needFormatter();
  }

  public double getNormalizedValue(final double in) {
    return nature.getNormalizedValue(in);
  }

  public double[] getNormalizedValues(final double[] in) {
    if (in == null) {
      return null;
    }
    final double[] res = new double[in.length];
    for (int i = 0; i < in.length; i++) {
      res[i] = getNature().getNormalizedValue(in[i]);
    }
    return res;
  }
}
