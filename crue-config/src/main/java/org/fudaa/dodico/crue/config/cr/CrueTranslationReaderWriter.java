package org.fudaa.dodico.crue.config.cr;

import org.fudaa.dodico.crue.common.io.CrueXmlReaderWriterImpl;

/**
 * @author CANEL Christophe
 *
 */
public class CrueTranslationReaderWriter extends CrueXmlReaderWriterImpl<CrueDaoTranslation, CrueLogTranslator> {

  public CrueTranslationReaderWriter() {
    super("log", "1.0", new CrueConverterTranslation(), new CrueDaoStructureTranslation());
  }
}
