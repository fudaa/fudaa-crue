/*
 * GPL v2.
 */
package org.fudaa.dodico.crue.config.comparator;

import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;

import java.util.Comparator;

/**
 * @author deniger
 */
public class DoubleComparator implements Comparator<Double> {
    private final PropertyEpsilon epsPdt;

    /**
     * @param epsPdt epsislon
     */
    public DoubleComparator(PropertyEpsilon epsPdt) {
        super();
        this.epsPdt = epsPdt;
    }

    @Override
    public int compare(Double o1, Double o2) {
        if (o1 == o2) {
            return 0;
        }
        if (o1 == null) {
            return -1;
        }
        if (o2 == null) {
            return 1;
        }
        if ((epsPdt == null && o1.doubleValue() == o2.doubleValue())
                || (epsPdt != null && epsPdt.isSame(o1.doubleValue(), o2.doubleValue()))) {
            return 0;
        }
        return (o1.doubleValue() - o2.doubleValue()) > 0 ? 1 : -1;
    }
}
