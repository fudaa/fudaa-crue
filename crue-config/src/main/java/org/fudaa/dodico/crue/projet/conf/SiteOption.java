/*
 * To change this template, choose Tools | Templates and open the template in the editor.
 */
package org.fudaa.dodico.crue.projet.conf;

/**
 * @author Chris
 */
public class SiteOption extends Option {

  private boolean userVisible;

  public SiteOption(final String nom, final String valeur, final String commentaire, final boolean userVisible) {
    this.userVisible = userVisible;
    this.id = nom;
    this.valeur = valeur;
    this.commentaire = commentaire;
  }

  @Override
  public SiteOption clone() {
    return (SiteOption) super.clone();
  }

  @Override
  public boolean isUserVisible() {
    return userVisible;
  }

  @Override
  public boolean isEditable() {
    return false;
  }

  /**
   * used by xstream
   * @param userVisible
   */
  @SuppressWarnings("unused")
  public void setUserVisible(final boolean userVisible) {
    this.userVisible = userVisible;
  }
}
