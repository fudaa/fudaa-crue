package org.fudaa.dodico.crue.config.ccm;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.config.loi.LoiContrat;
import org.fudaa.dodico.crue.config.loi.LoiTypeContainer;
import org.joda.time.Duration;
import org.joda.time.LocalDateTime;

import java.text.NumberFormat;
import java.util.*;

/**
 * @author deniger
 */
public class CrueConfigMetier implements LoiTypeContainer {
  private Map<String, ItemVariable> propDefinition = Collections.emptyMap();
  private Map<String, ItemConstant> propConstante = Collections.emptyMap();
  private Map<EnumTypeLoi, ConfigLoi> confLoi = Collections.emptyMap();
  private Map<String, EnumTypeLoi> idConfLoi = Collections.emptyMap();
  private final Map<String, PropertyNature> propNature;
  private final Map<String, PropertyNature> propEnum;
  private final CrueConfigMetierLitNomme litNomme;

  private final Set<String> zRegulVariables = new HashSet<>();
  private final Set<String> qRegulVariables = new HashSet<>();
  private Crue9DecimalFormat crue9DecimalFormat = Crue9DecimalFormat.getDefault();

  protected CrueConfigMetier(final Map<String, PropertyNature> propNature, final Map<String, PropertyNature> propEnums) {
    super();
    this.propNature = propNature;
    this.propEnum = propEnums;
    this.litNomme = new CrueConfigMetierLitNomme(propEnums.get(CrueConfigMetierConstants.ENUM_LIT_NOMME), propEnums.get(
        CrueConfigMetierConstants.ENUM_ETIQUETTE));
  }

  public CrueConfigMetierLitNomme getLitNomme() {
    return litNomme;
  }

  public void setCrue9DecimalFormat(final Crue9DecimalFormat crue9DecimalFormat) {
    this.crue9DecimalFormat = crue9DecimalFormat;
  }

  public Crue9DecimalFormat getCrue9DecimalFormat() {
    return crue9DecimalFormat;
  }

  /**
   * @return liste des types de lois pouvant être créées par l'utilisateur
   */
  public List<EnumTypeLoi> getCreatableDLHYLois() {
    // LoiQZimp, LoiTOuv, LoiTQapp, LoiTQruis, LoiTZimp
    final List<EnumTypeLoi> res = new ArrayList<>();
    res.add(EnumTypeLoi.LoiQZimp);
    res.add(EnumTypeLoi.LoiTOuv);
    res.add(EnumTypeLoi.LoiTQapp);
    res.add(EnumTypeLoi.LoiTQruis);
    res.add(EnumTypeLoi.LoiTZimp);
    res.add(EnumTypeLoi.LoiQQ);
    res.add(EnumTypeLoi.LoiZZimp);
    res.add(EnumTypeLoi.LoiQOuv);
    return res;
  }

  public Collection<PropertyNature> getNumeriqueNatures() {
    return propNature.values();
  }

  public PropertyNature getPropertyEnum(final String prop) {
    return propEnum.get(prop);
  }

  public Map<EnumTypeLoi, ConfigLoi> getConfLoi() {
    return confLoi;
  }

  public boolean isKnownLoi(final String loiType) {
    String capitalized = loiType;
    if (capitalized == null) {
      return false;
    }
    if (capitalized.charAt(0) == 'l') {
      capitalized = StringUtils.capitalize(capitalized);
    }
    return idConfLoi.containsKey(capitalized);
  }

  public boolean isPropertyDefined(final String prop) {
    return propDefinition.containsKey(prop);
  }

  public NumberFormat getFormatter(final String nom, final DecimalFormatEpsilonEnum type) {
    final ItemVariable property = getProperty(nom);
    return property != null && (property.getNature().isDate() || property.needFormatter()) ? property.getFormatter(type)
        : null;
  }

  public double getDefaultDoubleValue(final String prop) {
    final Number res = getDefaultValue(prop);
    return res == null ? 0 : res.doubleValue();
  }

  public int getDefaultIntValue(final String prop) {
    final Number res = getDefaultValue(prop);
    return res == null ? 0 : res.intValue();
  }

  public String getDefaultStringEnumValue(final String prop) {
    final ItemVariable propDef = getProperty(prop);
    return propDef == null ? null : propDef.getDefaultEnumValue();
  }

  public LocalDateTime getDefaultDateValue(final String prop) {
    final ItemVariable propDef = getProperty(prop);
    if (propDef == null || StringUtils.isEmpty(propDef.getDefaultEnumValue()) || !propDef.getNature().isDate()) {
      return null;
    }
    return DateDurationConverter.getDate(propDef.getDefaultEnumValue());
  }

  public Duration getDefaultDurationValue(final String prop) {
    final ItemVariable propDef = getProperty(prop);
    if (propDef == null || !propDef.getNature().isDuration() || propDef.getDefaultValue() == null) {
      return null;
    }
    return new Duration(propDef.getDefaultValue().longValue() * 1000);
  }

  public Number getDefaultValue(final String prop) {
    final ItemVariable propDef = getProperty(prop);
    return propDef == null ? null : propDef.getDefaultValue();
  }

  private PropertyEpsilon getEpsilon(final ItemVariable prop) {
    return prop.getEpsilon();
  }

  public PropertyEpsilon getEpsilon(final String propName) {
    final ItemVariable prop = getProperty(propName);
    return getEpsilon(prop);
  }

  public ItemVariable getLoiAbscisse(final EnumTypeLoi loi) {
    return confLoi.get(loi).getVarAbscisse();
  }

  public ConfigLoi getConfigLoi(final LoiContrat loi) {
    return confLoi.get(loi.getType());
  }

  public ItemVariable getLoiAbscisse(final LoiContrat loi) {
    return getLoiAbscisse(loi.getType());
  }

  public PropertyEpsilon getLoiAbscisseEps(final EnumTypeLoi loi) {
    return getEpsilon(confLoi.get(loi).getVarAbscisse());
  }

  public PropertyEpsilon getLoiAbscisseEps(final LoiContrat loi) {
    return getLoiAbscisseEps(loi.getType());
  }

  private PropertyValidator getLoiAbscisseValidator(final EnumTypeLoi loi) {
    return confLoi.get(loi).getVarAbscisse().getValidator();
  }

  public PropertyValidator getLoiAbscisseValidator(final LoiContrat loi) {
    return getLoiAbscisseValidator(loi.getType());
  }

  public ItemVariable getLoiOrdonnee(final EnumTypeLoi loi) {
    return confLoi.get(loi).getVarOrdonnee();
  }

  public ItemVariable getLoiOrdonnee(final LoiContrat loi) {
    return getLoiOrdonnee(loi.getType());
  }

  private PropertyEpsilon getLoiOrdonneeEps(final EnumTypeLoi loi) {
    return getEpsilon(confLoi.get(loi).getVarOrdonnee());
  }

  public PropertyEpsilon getLoiOrdonneeEps(final LoiContrat loi) {
    return getLoiOrdonneeEps(loi.getType());
  }

  public PropertyValidator getLoiOrdonneeValidator(final EnumTypeLoi loi) {
    return confLoi.get(loi).getVarOrdonnee().getValidator();
  }

  public PropertyValidator getLoiOrdonneeValidator(final LoiContrat loi) {
    return getLoiOrdonneeValidator(loi.getType());
  }

  public PropertyNature getNature(final String natureName) {
    return propNature.get(natureName);
  }

  /**
   * @return une map non modifiable
   */
  public Map<String, ItemVariable> getPropDefinition() {
    return propDefinition;
  }

  public Map<String, ItemConstant> getPropConstante() {
    return propConstante;
  }

  public ItemVariable getProperty(final String prop) {
    ItemVariable propertyDefinition = propDefinition.get(prop);
    if (propertyDefinition == null) {
      propertyDefinition = propDefinition.get(StringUtils.uncapitalize(prop));
    }
    if (propertyDefinition == null) {
      String normalizedProp = StringUtils.uncapitalize(prop);
      if (qRegulVariables.contains(normalizedProp)) {
        return propDefinition.get(CrueConfigMetierConstants.PROP_Q);
      }
      if (zRegulVariables.contains(normalizedProp)) {
        return propDefinition.get(CrueConfigMetierConstants.PROP_Z);
      }
    }
    return propertyDefinition;
  }

  public ItemConstant getConstante(final String prop) {
    ItemConstant constante = propConstante.get(prop);
    if (constante == null) {
      constante = propConstante.get(StringUtils.uncapitalize(prop));
    }
    return constante;
  }

  @Override
  public EnumTypeLoi getTypeLoi(final String idTypeLoi) {
    return idConfLoi.get(idTypeLoi);
  }

  public PropertyValidator getValidator(final String var) {
    final ItemVariable prop = propDefinition.get(var);
    return prop == null ? null : prop.getValidator();
  }


  protected void setConfigLoi(final Map<EnumTypeLoi, ConfigLoi> confLoi) {
    this.confLoi = Collections.unmodifiableMap(confLoi);
    final Map<String, EnumTypeLoi> idConfLoiTmp = new HashMap<>(confLoi.size());

    for (final EnumTypeLoi it : confLoi.keySet()) {
      idConfLoiTmp.put(it.getId(), it);
    }
    idConfLoi = Collections.unmodifiableMap(idConfLoiTmp);
  }

  protected void setPropDefinition(final Map<String, ItemVariable> propDefinition) {
    this.propDefinition = Collections.unmodifiableMap(propDefinition);
    final PropertyFormaterBuilder formatBuilder = new PropertyFormaterBuilder();
    final Set<String> doneNature = new HashSet<>();
    for (final ItemVariable prop : propDefinition.values()) {
      final PropertyNature nature = prop.getNature();
      if (!nature.isEnum() && !doneNature.contains(nature.getNom())) {
        doneNature.add(nature.getNom());
        nature.setFormat(formatBuilder.getFormatter(nature, DecimalFormatEpsilonEnum.PRESENTATION), formatBuilder.getFormatter(nature,
            DecimalFormatEpsilonEnum.COMPARISON));
      }
    }
  }

  protected void setPropConstantes(final Map<String, ItemConstant> propDefinition) {
    this.propConstante = Collections.unmodifiableMap(propDefinition);
    final PropertyFormaterBuilder formatBuilder = new PropertyFormaterBuilder();
    final Set<String> doneNature = new HashSet<>();
    for (final ItemConstant prop : propDefinition.values()) {
      final PropertyNature nature = prop.getNature();
      if (!nature.isEnum() && !doneNature.contains(nature.getNom())) {
        doneNature.add(nature.getNom());
        nature.setFormat(formatBuilder.getFormatter(nature, DecimalFormatEpsilonEnum.PRESENTATION), formatBuilder.getFormatter(nature,
            DecimalFormatEpsilonEnum.COMPARISON));
      }
    }
  }

  private SeveriteManager verbositeManager;

  public SeveriteManager getVerbositeManager() {
    return verbositeManager;
  }

  protected void setVerbositeManager(final SeveriteManager verbositeManager) {
    this.verbositeManager = verbositeManager;
  }

  public void getConfLoi(final EnumTypeLoi LoiPtProfil) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  public void addZRegulVariables(List<String> zRegulVariables) {
    if (zRegulVariables != null) {
      this.zRegulVariables.addAll(zRegulVariables);
    }
  }

  public void addQRegulVariables(List<String> qRegulVariables) {
    if (qRegulVariables != null) {
      this.qRegulVariables.addAll(qRegulVariables);
    }
  }
}
