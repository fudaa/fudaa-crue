package org.fudaa.dodico.crue.config.loi;

public class ItemTypeControleLoi {
    private final String nom;

    /**
     * @param nom le nom du controle
     */
    public ItemTypeControleLoi(String nom) {
        super();
        this.nom = nom;
    }

    public boolean isIncreasing() {
        return "Tco_AbsCrt".equalsIgnoreCase(nom);
    }

    public boolean isStricltyIncreasing() {
        return "Tco_AbsCrtStrict".equalsIgnoreCase(nom);
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }
}
