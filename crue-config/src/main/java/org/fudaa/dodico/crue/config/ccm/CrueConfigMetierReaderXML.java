package org.fudaa.dodico.crue.config.ccm;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyReplacer;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.xml.UnicodeInputStream;
import org.fudaa.ctulu.xml.XmlVersionFinder;
import org.fudaa.dodico.crue.common.contrat.XmlFileConfig;
import org.fudaa.dodico.crue.common.io.CrueXmlReaderWriterImpl;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class CrueConfigMetierReaderXML {

  private final XmlFileConfig coeurConfig;

  public CrueConfigMetierReaderXML(final XmlFileConfig coeurConfig) {
    this.coeurConfig = coeurConfig;
  }

  protected static class DaoConfigLoi {

    String Commentaire;
    DaoReference ExtrapolInf;
    DaoReference ExtrapolSup;
    String Nom;
    DaoReference TypeControleLoi;
    DaoReference VarAbscisse;
    DaoReference VarOrdonnee;
  }

  protected static class DaoConfigLois {

    List<DaoConfigLoi> Lois;
  }

  /**
   * Le dao principal contenant tous les autres
   *
   * @author deniger
   */
  protected static class DaoConfigMetier {

    DaoConfigLois ConfigLois;
    DaoNatures Natures;
    DaoControleLois TypeControleLois;
    DaoExtrapolationLois TypeExtrapolLois;
    DaoNumeriques TypeNumeriques;
    DaoEnums TypeEnums;
    DaoConstantes Constantes;
    DaoVariables Variables;
  }

  protected static class DaoControleLoi {

    String Nom;
  }

  protected static class DaoControleLois {

    List<DaoControleLoi> Controles;
  }

  protected static class DaoExtrapolationLoi {

    String Nom;
  }

  protected static class DaoExtrapolationLois {

    List<DaoExtrapolationLoi> Extrapolations;
  }

  protected static class DaoNature {

    String EpsilonComparaison;
    String EpsilonComparaisonFC;
    String EpsilonPresentation;
    String Nom;
    DaoReference TypeNumerique;
    String Unite;
  }

  protected static class DaoNatures {

    List<DaoNature> Natures;
  }

  protected static class DaoNumerique {

    String Infini;
    String Nom;
  }

  protected static class DaoItemEnum {

    String Id;
    String Nom;
  }

  protected static class DaoTypeEnum {

    List<DaoItemEnum> ItemEnum;
    String Nom;
  }

  protected static class TypeEnumRef {

    String NomRef;
  }

  protected static class DaoNumeriques {

    List<DaoNumerique> Numeriques;
  }

  protected static class DaoEnums {

    List<DaoTypeEnum> Enums;
  }

  protected static class DaoReference {

    String NomRef;
  }

  protected static class DaoValeurStrictable {

    String Valeur;
    boolean Strict;
  }

  protected static class DaoValeurStrictableConverter implements Converter {

    @Override
    public boolean canConvert(final Class type) {
      return type.equals(DaoValeurStrictable.class);
    }

    @Override
    public void marshal(final Object source, final HierarchicalStreamWriter writer, final MarshallingContext context) {
      final DaoValeurStrictable valeur = (DaoValeurStrictable) source;
      writer.addAttribute("Strict", Boolean.toString(valeur.Strict));
      writer.setValue(valeur.Valeur);
    }

    @Override
    public Object unmarshal(final HierarchicalStreamReader reader, final UnmarshallingContext context) {
      final DaoValeurStrictable valeur = new DaoValeurStrictable();
      valeur.Valeur = reader.getValue();
      if (reader.getAttributeCount() > 0) {
        final String attribute = reader.getAttribute(0);
        valeur.Strict = Boolean.valueOf(attribute);
      }
      return valeur;
    }
  }

  protected static class DaoEnumConverter implements Converter {

    @Override
    public boolean canConvert(final Class type) {
      return type.equals(DaoItemEnum.class);
    }

    @Override
    public void marshal(final Object source, final HierarchicalStreamWriter writer, final MarshallingContext context) {
      final DaoItemEnum valeur = (DaoItemEnum) source;
      writer.setValue(valeur.Nom);
      writer.addAttribute("Id", valeur.Id);
    }

    @Override
    public Object unmarshal(final HierarchicalStreamReader reader, final UnmarshallingContext context) {
      final DaoItemEnum valeur = new DaoItemEnum();
      valeur.Nom = reader.getValue();
      if (reader.getAttributeCount() > 0) {//pour version 1.1.1 de CrueConfigMetier:
        valeur.Id = reader.getAttribute(0);
      }
      return valeur;
    }
  }

  protected static class DaoItemVariable {

    String Nom;
    TypeEnumRef TypeEnum;
    DaoReference Nature;// utiliser map de ref Nom-> PropertyNature
    String ValeurDefaut;// traduire en org.fudaa.dodico.crue.property.PropertyDefinition.defaultValue
    String EnumValeurDefaut;
    // ces 4 propriétés permettent de construire un PropertyValidator a utiliser pour la construction d'un
    // PropertyDefinition
    // org.fudaa.dodico.crue.property.PropertyValidator.setRangeValidate(NumberRangeValidator)
    // so MinValidite.Strict vaut true, le NumberRangeValidator.minExclusive vaut true. Idem pour le max.
    DaoValeurStrictable MinValidite;// doit permettre de construire un NumberRangeValidator a utiliser pour
    DaoValeurStrictable MaxValidite;// idem
    DaoValeurStrictable MinNormalite;// idem pour normalite
    DaoValeurStrictable MaxNormalite;// idem pour normalite
  }

  protected static class DaoItemConstante {

    DaoReference Nature;// utiliser map de ref Nom-> PropertyNature
    TypeEnumRef TypeEnum;
    String Nom;
    String Valeur;// traduire en org.fudaa.dodico.crue.property.PropertyDefinition.defaultValue
    DaoValeurStrictable MinValidite;// doit permettre de construire un NumberRangeValidator a utiliser pour
    DaoValeurStrictable MaxValidite;// idem
    DaoValeurStrictable MinNormalite;// idem pour normalite
    DaoValeurStrictable MaxNormalite;// idem pour normalite
    // ces 4 propriétés permettent de construire un PropertyValidator a utiliser pour la construction d'un
    // PropertyDefinition
    // org.fudaa.dodico.crue.property.PropertyValidator.setRangeValidate(NumberRangeValidator)
    // so MinValidite.Strict vaut true, le NumberRangeValidator.minExclusive vaut true. Idem pour le max.
  }

  protected static class DaoVariables {

    List<DaoItemVariable> Variables;
  }

  protected static class DaoConstantes {

    List<DaoItemConstante> Constantes;
    Object ItemConstanteVecteur;
  }
  private final static Logger LOGGER = Logger.getLogger(CrueConfigMetierReaderXML.class.getName());

  private static XStream initXmlParser(final CtuluLog analyse) {
    final XmlFriendlyReplacer replacer = new XmlFriendlyReplacer("#", "_");
    final DomDriver domDriver = new DomDriver("UTF-8", replacer);
    final XStream xstream = new XStream(domDriver);
    CrueXmlReaderWriterImpl.initXstreamSecurity(xstream);
    xstream.setMode(XStream.NO_REFERENCES);

    xstream.alias("CrueConfigMetier", DaoConfigMetier.class);

    xstream.registerConverter(new DaoValeurStrictableConverter());
    xstream.useAttributeFor(DaoReference.class, "NomRef");

    xstream.alias("TypeControleLois", DaoControleLois.class);
    xstream.alias("ItemTypeControleLoi", DaoControleLoi.class);
    xstream.useAttributeFor(DaoControleLoi.class, "Nom");
    xstream.addImplicitCollection(DaoControleLois.class, "Controles");

    xstream.alias("TypeExtrapolLois", DaoExtrapolationLois.class);
    xstream.alias("ItemTypeExtrapolLoi", DaoExtrapolationLoi.class);
    xstream.useAttributeFor(DaoExtrapolationLoi.class, "Nom");
    xstream.addImplicitCollection(DaoExtrapolationLois.class, "Extrapolations");

    xstream.alias("TypeNumeriques", DaoNumeriques.class);
    xstream.alias("ItemTypeNumerique", DaoNumerique.class);
    xstream.useAttributeFor(DaoNumerique.class, "Nom");
    xstream.addImplicitCollection(DaoNumeriques.class, "Numeriques");


    xstream.alias("TypeEnums", DaoEnums.class);
    xstream.alias("ItemTypeEnum", DaoTypeEnum.class);
    xstream.alias("ItemEnum", DaoItemEnum.class);
    xstream.alias("TypeEnum", TypeEnumRef.class);
    xstream.useAttributeFor(DaoTypeEnum.class, "Nom");
    xstream.useAttributeFor(TypeEnumRef.class, "NomRef");
    xstream.addImplicitCollection(DaoEnums.class, "Enums");
    xstream.addImplicitCollection(DaoTypeEnum.class, "ItemEnum", DaoItemEnum.class);
    xstream.registerConverter(new DaoEnumConverter());

    xstream.alias("Natures", DaoNatures.class);
    xstream.alias("ItemNature", DaoNature.class);
    xstream.useAttributeFor(DaoNature.class, "Nom");
    xstream.addImplicitCollection(DaoNatures.class, "Natures");

    xstream.alias("Variables", DaoVariables.class);
    xstream.alias("ItemVariable", DaoItemVariable.class);
    xstream.useAttributeFor(DaoItemVariable.class, "Nom");
    xstream.addImplicitCollection(DaoVariables.class, "Variables");

    xstream.alias("Constantes", DaoConstantes.class);
    xstream.alias("ItemConstante", DaoItemConstante.class);
    xstream.useAttributeFor(DaoItemConstante.class, "Nom");
    xstream.addImplicitCollection(DaoConstantes.class, "Constantes");
    //on ignore le ItemConstanteVecteur
    xstream.omitField(DaoConstantes.class, "ItemConstanteVecteur");

    xstream.alias("ConfigLois", DaoConfigLois.class);
    xstream.alias("ItemConfigLoi", DaoConfigLoi.class);
    xstream.useAttributeFor(DaoConfigLoi.class, "Nom");
    xstream.addImplicitCollection(DaoConfigLois.class, "Lois");

    return xstream;
  }

  public boolean isValide(final File xml, final CtuluLog res) {
    try {
      final boolean valide = isValide(xml.toURI().toURL(), res);
      res.setDesc("valid.xml");
      res.setDescriptionArgs(xml.getName());
      return valide;

    } catch (final MalformedURLException e) {
      res.manageException(e);
      LOGGER.log(Level.SEVERE, "isValide", e);
      return false;
    }
  }

  public boolean isValide(final String xml, final CtuluLog res) {

    return isValide(getClass().getResource(xml), res);
  }

  private boolean isValide(final URL xml, final CtuluLog res) {
    final String version = coeurConfig.getXsdVersion();
    if (!CrueConfigMetierLoader.KNOWN_VERSION.contains(version)) {
      res.addSevereError("io.config.versionNotSupported", version);
    }
    return CrueXmlReaderWriterImpl.isValide(xml, coeurConfig.getXsdUrl("CrueConfigMetier-" + version + ".xsd"), res);
  }

  protected DaoConfigMetier readDaos(final InputStream in, final CtuluLog analyser) {
    DaoConfigMetier res = null;
    final InputStreamReader reader = null;
    UnicodeInputStream unicodeStream = null;
    try {
      final XStream parser = CrueConfigMetierReaderXML.initXmlParser(analyser);
      // The unicodeInputStream is used to eliminate the BOM bug from java:
      unicodeStream = new UnicodeInputStream(in, XmlVersionFinder.ENCODING);
      unicodeStream.init();
      final BufferedReader contentRead = new BufferedReader(new InputStreamReader(unicodeStream,
              XmlVersionFinder.ENCODING));

      res = (DaoConfigMetier) parser.fromXML(contentRead);
    } catch (final Exception e) {
      LOGGER.log(Level.FINE, "io.xml.error", e);
      analyser.addError("io.xml.error", e.getMessage());
    } finally {
      CtuluLibFile.close(reader);
      CtuluLibFile.close(unicodeStream);
    }
    return res;
  }

  protected DaoConfigMetier readDaos(final String in, final CtuluLog analyser) {
    final URL resource = getClass().getResource(in);
    InputStream openStream = null;
    try {
      openStream = resource.openStream();
      return readDaos(openStream, analyser);
    } catch (final IOException e) {
      LOGGER.log(Level.FINE, "io.xml.error", e);
    } finally {
      CtuluLibFile.close(openStream);
    }
    return null;

  }
}
