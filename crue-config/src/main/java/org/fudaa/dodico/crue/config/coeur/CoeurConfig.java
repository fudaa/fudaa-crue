package org.fudaa.dodico.crue.config.coeur;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;

/**
 * Definit un coeur de calcul.
 *
 * @author deniger
 */
public class CoeurConfig implements CoeurConfigContrat {

  private static final String MSG_FILES_SUFFIX = ".msg.xml";
  /**
   * N'a pas de sens pour Crue 9.
   */
  private String xsdVersion;
  private String name;
  private String coeurFolderPath;
  private String comment;
  private boolean usedByDefault;
  private boolean crue9Dependant = true;
  private CrueVersionType crueVersionType;
  private CrueConfigMetier crueConfigMetier;
  private File baseDir;

  public File getBaseDir() {
    return baseDir;
  }

  @Override
  public boolean isCrue9Dependant() {
    return crue9Dependant;
  }

  public void setCrue9Dependant(boolean crue9Dependant) {
    this.crue9Dependant = crue9Dependant;
  }


  public void setBaseDir(File baseDir) {
    this.baseDir = baseDir;
  }

  /**
   * Appele par le charge de CrueConfigMetier.
   *
   * @param crueConfigMetier le {@link CrueConfigMetier}
   */
  public void loadCrueConfigMetier(CrueConfigMetier crueConfigMetier) {
    this.crueConfigMetier = crueConfigMetier;
  }

  @Override
  public URL getDefaultFile(FileType fileType) {
    File defaultFileDir = new File(getCoeurFolder(), CoeurManagerValidator.FOLDER_FICHIERS_VIERGES);
    try {
      return new File(defaultFileDir, "default." + fileType.getExtension()).toURI().toURL();
    } catch (MalformedURLException ex) {
      Logger.getLogger(CoeurConfig.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
  }

  @Override
  public String getXsdVersion() {
    return xsdVersion;
  }

  /**
   * @param xsdVersion the xsdVersion to set
   */
  public void setXsdVersion(String xsdVersion) {
    this.xsdVersion = xsdVersion;
  }

  /**
   * l'identifiant du coeur.
   *
   * @return the name
   */
  @Override
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;

    this.crueVersionType = name.equals("CRUE9") ? CrueVersionType.CRUE9 : CrueVersionType.CRUE10;
  }

  public File getCoeurFolder() {
    File file = new File(coeurFolderPath);
    if ((!file.isAbsolute()) && (this.baseDir != null)) {
      file = new File(baseDir, coeurFolderPath);
    }
    return file;
  }

  /**
   * @return the folder
   */
  @Override
  public String getCoeurFolderPath() {
    return coeurFolderPath;
  }

  @Override
  public String getXsdUrl(String xsdFile) {
    File xsdDir = new File(getCoeurFolder(), CoeurManagerValidator.FOLDER_XSD);
    return new File(xsdDir, xsdFile).getAbsolutePath();
  }

  @Override
  public String getMessageURL(String local) {
    File localFile = getMessageURLFile(local);
    return localFile.exists() ? localFile.getAbsolutePath() : null;
  }

  @Override
  public List<String> getAvailableLanguagesForMessage() {
    File msg = new File(getCoeurFolder(), CoeurManagerValidator.FOLDER_MESSAGES);
    if (!msg.exists()) {
      return Collections.emptyList();
    }
    File[] listFiles = msg.listFiles();
    List<String> locales = new ArrayList<>();
    for (File file : listFiles) {
      if (file.isFile() && file.getName().endsWith(MSG_FILES_SUFFIX)) {
        locales.add(StringUtils.removeEnd(file.getName(), MSG_FILES_SUFFIX));
      }
    }
    return locales;

  }

  @Override
  public String getExecFile() {
    File folder = new File(getCoeurFolder(), CoeurManagerValidator.FOLDER_EXE);
    return new File(folder, CoeurManagerValidator.FILE_CRUE_10_EXE).getAbsolutePath();
  }

  @Override
  public String getCrueConfigMetierURL() {
    return new File(getCoeurFolder(), CoeurManagerValidator.FILE_CRUE_CONFIG_METIER_XML).getAbsolutePath();
  }

  @Override
  public CrueConfigMetier getCrueConfigMetier() {
    if (crueConfigMetier == null) {
      throw new IllegalAccessError("CrueConfigMetier must be loaded before call");
    }
    return crueConfigMetier;
  }

  /**
   * @param folder the folder to set
   */
  public void setCoeurFolderPath(String folder) {
    this.coeurFolderPath = folder;
  }

  /**
   * @return the comment
   */
  public String getComment() {
    return comment;
  }

  /**
   * @param comment the comment to set.
   */
  public void setComment(String comment) {
    this.comment = comment;
  }

  @Override
  public boolean isUsedByDefault() {
    return usedByDefault;
  }

  /**
   * @param usedByDefault the usedByDefault to set
   */
  public void setUsedByDefault(boolean usedByDefault) {
    this.usedByDefault = usedByDefault;
  }


  /**
   * @param crueVertionType the crueVertionType to set
   */
  public void setCrueVertionTypeOTFA(CrueVersionType crueVertionType) {
    this.crueVersionType = crueVertionType;
  }

  /**
   *
   * @param local l'identifiant du local
   * @return le fichier devant contenir les messages. Ne fait pas de test sur son existence ou non...
   */
  public File getMessageURLFile(String local) {
    if (getCoeurFolder() == null) {
      return null;
    }
    File msg = new File(getCoeurFolder(), CoeurManagerValidator.FOLDER_MESSAGES);
    return new File(msg, local + MSG_FILES_SUFFIX);
  }
}
