package org.fudaa.dodico.crue.projet.conf;

import java.util.Map;
import org.fudaa.ctulu.CtuluLog;

/**
 *
 * @author deniger
 */
public interface OptionValidator {

  /**
   *
   * @param value la valeur a tester
   * @param log peut être null. Si non null, doit recevoir les message d'erreur
   * @return true si valid
   */
  boolean isValid(String value, CtuluLog log, Map<OptionsEnum, ? extends Option> readOnlySiteOptions);
}
