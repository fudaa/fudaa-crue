package org.fudaa.dodico.crue.config.ccm;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.config.loi.ItemTypeControleLoi;
import org.fudaa.dodico.crue.config.loi.ItemTypeExtrapolationLoi;

/**
 * @author deniger
 */
public class ConfigLoi {
    private final EnumTypeLoi typeLoi;
    private final String commentaire;
    private final ItemTypeExtrapolationLoi extrapolInf;
    private final ItemTypeExtrapolationLoi extrapolSup;
    private final ItemTypeControleLoi controleLoi;
    private final ItemVariable varAbscisse;
    private final ItemVariable varOrdonnee;

    public ConfigLoi(EnumTypeLoi typeLoi, ItemTypeControleLoi controleLoi, ItemTypeExtrapolationLoi extrapolInf, ItemTypeExtrapolationLoi extrapolSub,
                     ItemVariable varAbscisse, ItemVariable varOrdonnee, String commentaire) {
        super();
        this.typeLoi = typeLoi;
        this.controleLoi = controleLoi;
        this.commentaire = StringUtils.defaultString(commentaire);
        this.extrapolInf = extrapolInf;
        this.extrapolSup = extrapolSub;
        this.varAbscisse = varAbscisse;
        this.varOrdonnee = varOrdonnee;
    }

    public ItemTypeExtrapolationLoi getExtrapolInf() {
        return extrapolInf;
    }

    public ItemTypeExtrapolationLoi getExtrapolSup() {
        return extrapolSup;
    }

    public EnumTypeLoi getTypeLoi() {
        return typeLoi;
    }

    public ItemVariable getVarAbscisse() {
        return varAbscisse;
    }

    public ItemVariable getVarOrdonnee() {
        return varOrdonnee;
    }

    /**
     * @return the controleLoi
     */
    public ItemTypeControleLoi getControleLoi() {
        return controleLoi;
    }
}
