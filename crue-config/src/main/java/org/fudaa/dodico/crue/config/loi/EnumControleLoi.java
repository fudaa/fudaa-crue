package org.fudaa.dodico.crue.config.loi;

/**
 * @author deniger
 */
public enum EnumControleLoi {
  ABSCISSE_STRICT_CROISSANT, ABSCISSE_CROISSANT, AUCUN
  
}
