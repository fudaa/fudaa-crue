package org.fudaa.dodico.crue.config.cr;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueDaoStructure;

/**
 * @author CANEL Christophe
 *
 */
public class CrueDaoStructureTranslation implements CrueDaoStructure {

  /**
   * {@inheritDoc}
   */
  @Override
  public void configureXStream(final XStream xstream, final CtuluLog analyze) {
    xstream.alias("DecodageMessages", CrueDaoTranslation.class);
    xstream.alias("Message", Entry.class);
    xstream.useAttributeFor(Entry.class, "ID");
    xstream.useAttributeFor(LienDocumentation.class, "Signet");
    xstream.useAttributeFor(LienDocumentation.class, "Href");
    xstream.useAttributeFor(LienMultimedia.class, "Signet");
    xstream.useAttributeFor(LienMultimedia.class, "Href");
  }

  protected static class Entry {

    public String ID;
    public String Texte;
    public LienDocumentation LienDocumentation;
    public LienMultimedia LienMultimedia;
  }
}
