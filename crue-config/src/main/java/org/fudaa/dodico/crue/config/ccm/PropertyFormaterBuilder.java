package org.fudaa.dodico.crue.config.ccm;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.fudaa.ctulu.CtuluNumberFormatFixedFigure;

public class PropertyFormaterBuilder {

  private final Map<Integer, NumberFormat> nbDecFormat = new HashMap<>();
  private NumberFormat nbDecFormatForInteger;

  public int getNbDecimal(PropertyNature nature, DecimalFormatEpsilonEnum presentionOrComparison) {
    double epsilon = nature.getEpsilon().getEpsilon(presentionOrComparison);
    if (DecimalFormatEpsilonEnum.COMPARISON.equals(presentionOrComparison)) {
      epsilon = epsilon / 10;
    }
    return Math.abs((int) Math.floor(Math.log10(epsilon)));
  }

  public NumberFormat getFormatter(final PropertyNature nature, DecimalFormatEpsilonEnum presentionOrComparison) {
    if (nature == null) {
      return null;
    }
    if (nature.getTypeNumerique().isEntier()) {
      if (nbDecFormatForInteger == null) {
        DecimalFormat numberFormat = new DecimalFormat();
        numberFormat.setGroupingUsed(false);
        numberFormat.setMaximumFractionDigits(0);
        NumberFormatChoice fmt = new NumberFormatChoice(nature.getEpsilon(), numberFormat, new CtuluNumberFormatFixedFigure(10), 1E10, 1E-10);
        nbDecFormatForInteger = new NumberFormatInfini(fmt, nature);
      }
      return nbDecFormatForInteger;
    }
    String natureNom = nature.getNom();
    if ("nbr".equalsIgnoreCase(natureNom) || "num".equalsIgnoreCase(natureNom)) {
      return null;
    }
    int nbDec = getNbDecimal(nature, presentionOrComparison);
    NumberFormat numberFormat = this.nbDecFormat.get(nbDec);
    if (numberFormat == null) {
      numberFormat = new DecimalFormat();
      ((DecimalFormat) numberFormat).setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
      numberFormat.setGroupingUsed(false);
      numberFormat.setMaximumFractionDigits(nbDec);
      // pour ecrire correctement + et - infini
      NumberFormatChoice fmt = new NumberFormatChoice(nature.getEpsilon(), numberFormat, new CtuluNumberFormatFixedFigure(10), 1E10, 1E-10);
      numberFormat = new NumberFormatInfini(fmt, nature);
      nbDecFormat.put(nbDec, numberFormat);
    }
    return numberFormat;
  }
}
