package org.fudaa.dodico.crue.config.coeur;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.*;

public class CoeurManager {
  private final File siteDir;
  private final Map<String, CoeurConfig> coeurConfigs = new HashMap<>();

  public CoeurManager(final File siteDir, final Collection<CoeurConfig> configs) {
    this.siteDir = siteDir;
    this.addConfigs(configs);
  }

  protected static String getHigherGrammaire(final Set<String> xsd) {
    if (xsd.size() == 1) {
      return xsd.iterator().next();
    }
    final Map<String, String> xsdByXsdNormalise = new HashMap<>();
    for (final String string : xsd) {
      xsdByXsdNormalise.put(normalise(string), string);
    }
    final TreeSet<String> keys = new TreeSet<>(xsdByXsdNormalise.keySet());
    return xsdByXsdNormalise.get(keys.last());
  }

  private static String normalise(final String xsdVersion) {
    final int countMatches = StringUtils.countMatches(xsdVersion, ".");
    if (countMatches != 2) {
      return xsdVersion + ".0";
    }
    return xsdVersion;
  }

  public File getSiteDir() {
    return siteDir;
  }

  public String getHigherGrammaire() {
    final Set<String> xsd = getAvailableXsd();
    return getHigherGrammaire(xsd);
  }

  private Set<String> getAvailableXsd() {
    final Collection<CoeurConfig> values = coeurConfigs.values();
    final Set<String> xsd = new HashSet<>();
    for (final CoeurConfig coeurConfig : values) {
      xsd.add(coeurConfig.getXsdVersion());
    }
    return xsd;
  }

  public List<String> getAllXsd() {
    final List<String> res = new ArrayList<>(getAvailableXsd());
    Collections.sort(res);
    return res;
  }

  private void addConfigs(final Collection<CoeurConfig> configs) {
    for (final CoeurConfig coeurConfig : configs) {
      coeurConfig.setBaseDir(siteDir);
      this.addConfig(coeurConfig);
    }
  }

  private void addConfig(final CoeurConfig config) {
    coeurConfigs.put(config.getName().toUpperCase(), config);
  }

  public CoeurConfigContrat getCoeurConfig(final String coeurName) {
    final String id = StringUtils.upperCase(coeurName);
    return coeurConfigs.get(id);
  }

  /**
   * @param xsd l'identifiant du fichier xsd
   * @return liste des coeurConfig applicable à la xsd. Si utilisation par défaut, renvoie uniquement cette instance.
   */
  public CoeurConfig getCoeurConfigDefault(final String xsd) {
    for (final CoeurConfig coeurConfig : coeurConfigs.values()) {
      if (coeurConfig.getXsdVersion().equals(xsd)) {
        if (coeurConfig.isUsedByDefault()) {
          return coeurConfig;
        }
      }
    }
    return null;
  }

  public List<CoeurConfig> getAllCoeurConfig(final String xsd) {
    final List<CoeurConfig> res = new ArrayList<>();
    for (final CoeurConfig coeurConfig : coeurConfigs.values()) {
      if (coeurConfig.getXsdVersion().equals(xsd)) {
        res.add(coeurConfig);
      }
    }
    return res;
  }

  public List<CoeurConfig> getAllCoeurConfigs() {
    return new ArrayList<>(this.coeurConfigs.values());
  }
}
