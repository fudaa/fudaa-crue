package org.fudaa.dodico.crue.config.cr;

/**
 *
 * @author Frédéric Deniger
 */
public class LienMultimedia {

  private String Signet;
  private String Href;

  public String getSignet() {
    return Signet;
  }

  public String getHRef() {
    return Href;
  }
}
