/*
 GPL 2
 */
package org.fudaa.dodico.crue.config.lit;

import org.fudaa.dodico.crue.common.SafeComparator;

/**
 *
 * @author Frederic Deniger
 */
public class LitNommeComparator extends SafeComparator<LitNomme> {

  @Override
  protected int compareSafe(LitNomme o1, LitNomme o2) {
    return o1.getIdx() - o2.getIdx();
  }
}
