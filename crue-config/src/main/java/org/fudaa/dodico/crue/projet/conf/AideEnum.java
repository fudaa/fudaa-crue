package org.fudaa.dodico.crue.projet.conf;

public enum AideEnum {

  ABOSLUTE("Absolute"),
  RELATIVE("Relative");
  private final String name;

  AideEnum(final String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return this.name;
  }
}
