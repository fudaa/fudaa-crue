package org.fudaa.dodico.crue.projet.conf;

import java.util.List;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;

/**
 *
 * @author Chris
 */
public class SiteConfiguration {
  private List<CoeurConfig> coeurs;
  private List<SiteOption> options;
  private SiteAide aide;

    public SiteConfiguration() {
        aide = new SiteAide();
    }

  public List<CoeurConfig> getCoeurs() {
    return coeurs;
  }

  public void setCoeurs(List<CoeurConfig> coeurs) {
    this.coeurs = coeurs;
  }

  public List<SiteOption> getOptions() {
    return options;
  }

  public void setOptions(List<SiteOption> options) {
    this.options = options;
  }
  
    public SiteAide getAide() {
        return aide;
    }

    public void setAide(SiteAide aide) {
        this.aide = aide;
    }
}
