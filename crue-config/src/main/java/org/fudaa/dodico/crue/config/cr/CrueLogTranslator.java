package org.fudaa.dodico.crue.config.cr;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author deniger
 */
public class CrueLogTranslator {
  String locale;
  Map<String, String> entriesById;
  Map<String, LienDocumentation> liensDocsById;
  Map<String, LienMultimedia> liensMultimediaById;

  public String getLocale() {
    return locale;
  }

  public String getTranslation(final String id) {
    if (entriesById.containsKey(id)) {
      return entriesById.get(id);
    }
    return id;
  }

  public List<String> getIds() {
    return new ArrayList<>(entriesById.keySet());
  }

  public LienDocumentation getLienDocumentation(final String id) {
    return liensDocsById == null ? null : liensDocsById.get(id);
  }

  public LienMultimedia getLienMultimedia(final String id) {
    return liensMultimediaById == null ? null : liensMultimediaById.get(id);
  }

  public CtuluLogGroup translateGroup(final CtuluLogGroup logGroups) {
    if (logGroups == null) {
      return null;
    }
    final List<CtuluLog> logs = logGroups.getLogs();
    for (final CtuluLog ctuluLog : logs) {
      translate(ctuluLog);
    }
    return logGroups;
  }

  public void translate(final CtuluLog log) {
    if (log == null) {
      return;
    }
    for (final CtuluLogRecord record : log.getRecords()) {
      final String id = record.getId();
      if (id != null) {
        final Object[] args = record.getArgs();
        if (entriesById.containsKey(id)) {
          translateMessage(args, record, id);
        } else {
          record.setLocalizedMessage(id + ";" + StringUtils.join(args, ';'));
        }
        final LienDocumentation doc = liensDocsById.get(id);
        if (doc != null) {
          final String signet = doc.getSignet();
          String href = doc.getHRef();
          if (StringUtils.isNotBlank(signet)) {
            href = href + LienDocumentation.SIGNET_SEPARATOR + signet;
          }
          record.setHelpUrl(href);
        }
        final LienMultimedia multimedia = liensMultimediaById.get(id);
        if (multimedia != null) {
          String href = multimedia.getHRef();
          final String signet = multimedia.getSignet();
          if (StringUtils.isNotBlank(signet)) {
            href = href + LienDocumentation.SIGNET_SEPARATOR + signet;
          }
          record.setHelpSupportUrl(href);
        }
      }
    }
  }

  private void translateMessage(final Object[] args, final CtuluLogRecord record, final String id) {
    final String translatedMessage = entriesById.get(id);
    if (ArrayUtils.isNotEmpty(args)) {
      final String[] token;
      final String[] replacement;
      final int normalArgLength;
      //cas des vecteurs: il faut analyser le contenu
      if (translatedMessage.contains("{v}")) {
        final int max = findMaxReplacementIndex(translatedMessage);
        token = new String[max + 2];
        replacement = new String[max + 2];
        normalArgLength = Math.min(max + 1, args.length);
        token[max + 1] = "{v}";
        replacement[max + 1] = StringUtils.join(args, ';', max + 1, args.length);
      } else {
        token = new String[args.length];
        replacement = new String[args.length];
        normalArgLength = token.length;
      }
      for (int i = 0; i < normalArgLength; i++) {
        final String string = (String) args[i];
        replacement[i] = string;
        if (i == 0) {
          token[i] = "{nomEMH}";
        } else {
          token[i] = "{" + i + "}";
        }
      }
      record.setLocalizedMessage(StringUtils.replaceEach(translatedMessage, token, replacement));
    } else {
      record.setLocalizedMessage(translatedMessage);
    }
  }

  private int findMaxReplacementIndex(final String translatedMessage) throws NumberFormatException {
    final String[] splitPreserveAllTokens = StringUtils.split(translatedMessage, "{}");
    int max = 0;
    for (final String idFound : splitPreserveAllTokens) {
      if (StringUtils.isNumeric(idFound) && translatedMessage.contains("{" + idFound + "}")) {
        max = Math.max(max, Integer.parseInt(idFound));
      }
    }
    return max;
  }
}
