/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.config.ccm;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.config.lit.LitNommeLimite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author deniger
 */
public class CrueConfigMetierLitNomme {
  public static final int STO_START = 0;
  public static final int MAJEUR_START = 1;
  public static final int MINEUR = 2;
  public static final int MAJEUR_END = 3;
  public static final int STO_END = 4;

  private final List<ItemEnum> etiquettesEnums;
  private final List<LitNomme> litNommes;
  private final List<LitNommeLimite> limites;

  CrueConfigMetierLitNomme(final PropertyNature nature, final PropertyNature etiquettes) {
    List<ItemEnum> litNommesEnums = Collections.unmodifiableList(nature.getEnumValues());
    etiquettesEnums = Collections.unmodifiableList(etiquettes.getEnumValues());
    assert litNommesEnums.size() == 5;
    assert etiquettesEnums.size() >= 3;
    final ArrayList<LitNomme> currentLitNommes = new ArrayList<>();
    for (final ItemEnum itemEnum : litNommesEnums) {
      final LitNomme litNomme = new LitNomme(itemEnum.getName(), itemEnum.getId());
      currentLitNommes.add(litNomme);
    }
    this.litNommes = Collections.unmodifiableList(currentLitNommes);
    final ArrayList<LitNommeLimite> currentLimites = buildLimites(currentLitNommes);
    this.limites = Collections.unmodifiableList(currentLimites);
  }

  private static String getNameExtremite(final LitNomme nomme) {
    return "R" + nomme.getNom().charAt(nomme.getNom().length() - 1);
  }

  private static String getName(final LitNomme before, final LitNomme after) {
    return "Et_" + StringUtils.removeStart(before.getNom(), "Lt_") + "-" + StringUtils.removeStart(after.getNom(), "Lt_");
  }

  public List<ItemEnum> getEtiquettesEnums() {
    return etiquettesEnums;
  }

  public ItemEnum getEtiquetteAxeHyd() {
    return etiquettesEnums.get(0);
  }

  public ItemEnum getEtiquetteLimLit() {
    return etiquettesEnums.get(1);
  }

  public ItemEnum getEtiquetteThalweg() {
    return etiquettesEnums.get(2);
  }

  /**
   * @return limites dans l'ordre
   */
  public List<LitNommeLimite> getLimites() {
    return limites;
  }

  public List<LitNomme> getLitNomme() {
    return litNommes;
  }

  public LitNomme getStockageStart() {
    return litNommes.get(STO_START);
  }

  public LitNomme getMajeurStart() {
    return litNommes.get(MAJEUR_START);
  }

  public LitNomme getMineur() {
    return litNommes.get(MINEUR);
  }

  public LitNomme getMajeurEnd() {
    return litNommes.get(MAJEUR_END);
  }

  public LitNomme getStockageEnd() {
    return litNommes.get(STO_END);
  }

  /**
   * @param litNomme le litnomme
   * @return true si litNomme est un lit de stockage.
   */
  private boolean isStockage(final LitNomme litNomme) {
    return getStockageEnd().equals(litNomme) || getStockageStart().equals(litNomme);
  }

  public boolean isStockage(final LitNommeLimite litNomme) {
    return litNomme.isLast() || isStockage(litNomme.getAfter());
  }

  public boolean isActif(final LitNomme litNomme) {
    return !isStockage(litNomme);
  }

  private ArrayList<LitNommeLimite> buildLimites(final ArrayList<LitNomme> currentLitNommes) {
    final ArrayList<LitNommeLimite> currentLimites = new ArrayList<>();
    LitNomme lit = currentLitNommes.get(0);
    int idx = 0;
    currentLimites.add(new LitNommeLimite(getNameExtremite(lit), null, lit, idx++));
    for (int i = 1; i < currentLitNommes.size(); i++) {
      final LitNomme before = currentLitNommes.get(i - 1);
      final LitNomme after = currentLitNommes.get(i);
      currentLimites.add(new LitNommeLimite(getName(before, after), before, after, idx++));
    }
    lit = currentLitNommes.get(currentLitNommes.size() - 1);
    currentLimites.add(new LitNommeLimite(getNameExtremite(lit), lit, null, idx));
    return currentLimites;
  }
}
