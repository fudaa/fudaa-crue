package org.fudaa.dodico.crue.config.ccm;

import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;

/**
 * Un formatteur qui prend en compte le 1 et - infini defini pour crue.
 *
 * @author deniger
 */
@SuppressWarnings("serial")
public class NumberFormatInfini extends NumberFormat {
    private final NumberFormat base;
    private final PropertyNature nature;

    /**
     * @param numberFormat le format
     * @param nature       la nature
     */
    public NumberFormatInfini(NumberFormat numberFormat, PropertyNature nature) {
        super();
        this.base = numberFormat;
        this.nature = nature;
    }

    @Override
    public StringBuffer format(double arg0, StringBuffer arg1, FieldPosition arg2) {
        if (nature.isInfiniPositif(arg0)) {
            arg1.append(nature.getTypeNumerique().getInfiniAbsValue());
            return arg1;
        } else if (nature.isInfiniNegatif(arg0)) {
            arg1.append('-').append(nature.getTypeNumerique().getInfiniAbsValue());
            return arg1;
        }
        return base.format(arg0, arg1, arg2);
    }

    @Override
    public StringBuffer format(long arg0, StringBuffer arg1, FieldPosition arg2) {
        if (nature.isInfiniPositif(arg0)) {
            arg1.append(nature.getTypeNumerique().getInfiniAbsValue());
            return arg1;
        } else if (nature.isInfiniNegatif(arg0)) {
            arg1.append(nature.getTypeNumerique().getInfiniAbsValue());
            return arg1;
        }
        return base.format(arg0, arg1, arg2);
    }

    @Override
    public Number parse(String arg0, ParsePosition arg1) {
        return base.parse(arg0, arg1);
    }

    /**
     * @return the base
     */
    public NumberFormat getBase() {
        return base;
    }

    public NumberFormat getRealBase() {
        if (base instanceof NumberFormatChoice) {
            return ((NumberFormatChoice) base).getBase();
        }
        return base;
    }
}
