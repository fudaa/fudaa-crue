/*
 GPL 2
 */
package org.fudaa.dodico.crue.config.coeur;

/**
 *
 * @author Frederic Deniger
 */
public interface FileType {

  String getExtension();
}
