package org.fudaa.dodico.crue.projet.conf;

import java.util.Map;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;

/**
 *
 * @author deniger
 */
public class OptionValidatorLanguage implements OptionValidator {

  private final String optionName;

  public OptionValidatorLanguage(String optionName) {
    this.optionName = optionName;
  }

  @Override
  public boolean isValid(String o, CtuluLog log, Map<OptionsEnum, ? extends Option> onManager) {
    boolean valid = true;
    if (o == null) {
      if (log != null) {
        log.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
        log.addError("option.ValueIsNull.Error", Option.geti18n(optionName));
      }
      return false;
    }
    final String knownLanguages = onManager.get(OptionsEnum.AVAILABLE_LANGUAGE).getValeur();
    String[] languages = StringUtils.split(knownLanguages,';');
    boolean contained = ArrayUtils.contains(languages, o);
    if (!contained) {
      valid = false;
      if (log != null) {
        log.addError("option.LanguageUnknow.Error", o, knownLanguages);
      }
    }
    return valid;
  }
}
