package org.fudaa.dodico.crue.config.ccm;

import org.fudaa.ctulu.CtuluNumberFormatFixedFigure;

import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;

/**
 * @author deniger Choisit un formater en fonction de la valeur.
 */
@SuppressWarnings("serial")
public class NumberFormatChoice extends NumberFormat {
    private final NumberFormat base;
    private final CtuluNumberFormatFixedFigure scientific;
    private final double maxValue;
    private final double minValue;
    private final PropertyEpsilon epsilon;

    /**
     * @param numberFormat      le format
     * @param formatFixedFigure formatage
     * @param maxValue          limite haute pour utilise le format formatFixedFigure
     * @param minValue          limite basse pour utilise le format formatFixedFigure
     */
    public NumberFormatChoice(PropertyEpsilon epsilon, final NumberFormat numberFormat, final CtuluNumberFormatFixedFigure formatFixedFigure,
                              final double maxValue, final double minValue) {
        super();
        this.base = numberFormat;
        this.epsilon = epsilon;
        this.scientific = formatFixedFigure;
        this.maxValue = maxValue;
        this.minValue = minValue;
    }

    private boolean useScientific(final double value) {
        if (epsilon != null && epsilon.isZero(value)) {
            return false;
        }
        final double absValue = Math.abs(value);
        return (absValue > maxValue || absValue < minValue);
    }

    public NumberFormat getBase() {
        return base;
    }

    @Override
    public StringBuffer format(final double number, final StringBuffer toAppendTo, final FieldPosition pos) {
        if (useScientific(number)) {
            return scientific.format(number, toAppendTo, pos);
        } else {
            return base.format(number, toAppendTo, pos);
        }
    }

    @Override
    public StringBuffer format(final long number, final StringBuffer toAppendTo, final FieldPosition pos) {
        if (useScientific(number)) {
            return scientific.format(number, toAppendTo, pos);
        }
        return base.format(number, toAppendTo, pos);
    }

    @Override
    public Number parse(final String source, final ParsePosition parsePosition) {
        return base.parse(source, parsePosition);
    }
}
