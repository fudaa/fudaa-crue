package org.fudaa.dodico.crue.projet.conf;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.CloneTransformer;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author deniger
 */
public class GlobalOptionsManager {
  public static final String SITE_FILE = "FudaaCrue_Site.xml";
  public static final String ETUDE_FILE = "FudaaCrue_Etude.xml";
  public static final String ETUDE_AOC_FILE = "FudaaCrue_Etude_Aoc.xml";
  public static final String ETUDE_FILE_EXTERNE = "FudaaCrue_Etude_Externe.xml";
  public static final String USER_FILE = "FudaaCrue_User.xml";
  public static final String HELP_FILE = "FudaaCrue_Aide.xml";
  private final Map<String, OptionsEnum> enumById = new HashMap<>();
  private final Predicate visiblePredicate = new VisiblePredicate();
  private final Comparator<Option> comparator = new OptionComparator();
  private final Map<OptionsEnum, Option> siteOptionById = new EnumMap<>(OptionsEnum.class);
  private final Map<OptionsEnum, Option> userOptionById = new EnumMap<>(OptionsEnum.class);
  private SiteAide aide = null;
  private List<SiteOption> siteReadOnlyOptions;

  public GlobalOptionsManager() {
    final OptionsEnum[] values = OptionsEnum.values();
    for (final OptionsEnum optionsEnum : values) {
      enumById.put(optionsEnum.getId(), optionsEnum);
    }
  }

  public Map<OptionsEnum, ? extends Option> getReadOnlySiteOptions() {
    return build(siteReadOnlyOptions, true);
  }

  /**
   * @param user Configuration utilisateur
   * @return les user options qui ne redéfinissent pas des options site non éditables.
   */
  private Map<OptionsEnum, Option> getUserOptionClean(final UserConfiguration user) {
    //on enleve les options non éditable
    //on clone les user options pour éviter des modifications externe.
    final Map<OptionsEnum, Option> userOptions = build(user.getOptions(), true);
    final Set<Entry<OptionsEnum, Option>> entrySet = userOptions.entrySet();
    for (final Iterator<Entry<OptionsEnum, Option>> it = entrySet.iterator(); it.hasNext(); ) {
      final Entry<OptionsEnum, Option> entry = it.next();
      final Option option = siteOptionById.get(entry.getKey());
      if (option != null && !option.isEditable()) {
        it.remove();
      }
    }
    return userOptions;
  }

  public <T extends Option> Collection<T> getModifiedFromSiteOptions(final Collection<T> init) {
    final List<T> res = new ArrayList<>();
    for (final T option : init) {
      final OptionsEnum optionEnum = getOptionEnum(option);
      if (optionEnum != null) {
        final Option siteOption = siteOptionById.get(optionEnum);
        if (siteOption != null && !StringUtils.equals(StringUtils.trim(siteOption.getValeur()), StringUtils.trim(
            option.getValeur()))) {
          res.add(option);
        }
      }
    }
    return res;
  }

  public UserConfiguration getCurrentUserConfiguration() {
    final UserConfiguration userConfig = new UserConfiguration();
    final Collection<Option> values = userOptionById.values();
    final List<UserOption> userOption = new ArrayList<>();
    for (final Option option : values) {
      userOption.add((UserOption) option.clone());
    }
    userConfig.setOptions(userOption);
    return userConfig;
  }

  /**
   * @return all visible options
   */
  public Collection<Option> getVisibleOptionsCloned() {
    final Map<OptionsEnum, Option> res = new EnumMap<>(OptionsEnum.class);
    res.putAll(siteOptionById);
    res.putAll(userOptionById);
    return createList(res, true);
  }

  public String getSiteValue(final Option option) {
    final OptionsEnum optionEnum = getOptionEnum(option);
    final Option siteOption = siteOptionById.get(optionEnum);
    return siteOption == null ? null : siteOption.getValeur();
  }

  private Collection<Option> createList(final Map<OptionsEnum, Option> res, final boolean clone) {
    final ArrayList<Option> select = CollectionCrueUtil.select(res.values(), visiblePredicate);
    select.sort(comparator);
    if (clone) {
      CollectionUtils.transform(select, CloneTransformer.INSTANCE);
    }
    return select;
  }

  public OptionsEnum getOptionEnum(final Option option) {
    if (option == null) {
      return null;
    }
    return enumById.get(option.getId());
  }

  public Boolean getBooleanValue(final OptionsEnum optionEnum) {
    final Option option = getOption(optionEnum);
    return Boolean.TRUE.toString().equals(option.getValeur());
  }

  public Integer getIntegerValue(final OptionsEnum optionEnum) {
    final Option option = getOption(optionEnum);
    if (option == null) {
      return null;
    }
    return Integer.parseInt(option.getValeur());
  }

  public Integer getIntValue(final OptionsEnum optionEnum, final int defaultValue) {
    final Integer res = getIntegerValue(optionEnum);
    return res == null ? defaultValue : res.intValue();
  }

  public Option getOption(final OptionsEnum optionEnum) {
    final Option option = userOptionById.get(optionEnum);
    if (option == null) {
      return siteOptionById.get(optionEnum);
    }
    return option;
  }

  public SiteAide getAideOption() {
    return aide;
  }

  private void setAideOption(final SiteAide aide) {
    this.aide = aide;
  }

  private CtuluLogLevel getLevel(final boolean useFatal) {
    return useFatal ? CtuluLogLevel.ERROR : CtuluLogLevel.WARNING;
  }

  private void init(final UserConfiguration initUser, final CtuluLogGroup parentLogs, final boolean validOnly) {
    UserConfiguration user = initUser;
    if (user == null) {
      user = new UserConfiguration();
    }
    //user:
    final CtuluLog userLog = parentLogs.createLog();
    userLog.setDesc("conf.validConfUser");

    final Map<OptionsEnum, Option> siteReadOnlyOptionMap = build(siteReadOnlyOptions, false);
    validUserOptions(user, userLog, siteReadOnlyOptionMap, false);
    if (validOnly) {
      return;
    }

    final Map<OptionsEnum, Option> userOptions = getUserOptionClean(user);
    this.userOptionById.clear();
    this.userOptionById.putAll(userOptions);
  }

  private void validSiteOptions(final CtuluLog siteLog, final List<SiteOption> siteOptions,
                                final Map<OptionsEnum, ? extends Option> siteOptionMapInReadOnly) {
    validIsKnown(siteLog, siteOptions, true);//pour le site, on n'autorise pas d'erreurs.
    validNoTwice(siteLog, siteOptions, true);
    validRequiredButEmptyAndNotEditable(siteLog, siteOptionMapInReadOnly);
  }

  private void validUserOptions(final UserConfiguration user, final CtuluLog userLog,
                                final Map<OptionsEnum, ? extends Option> siteOptionMapInReadOnly, final boolean useFatal) {
    final Collection<UserOption> userOptions = user.getOptions();
    final Map<OptionsEnum, ? extends Option> userOptionMap = build(userOptions, false);
    validIsKnown(userLog, userOptions, useFatal);
    validNoTwice(userLog, userOptions, useFatal);
    validValuesOfOption(userLog, userOptionMap, siteOptionMapInReadOnly);
    validNoSiteOptionOveridden(userLog, siteOptionMapInReadOnly, userOptionMap, useFatal);
  }

  private Map<OptionsEnum, Option> build(final Collection<? extends Option> in, final boolean clone) {
    final Map<OptionsEnum, Option> res = new EnumMap<>(OptionsEnum.class);
    for (final Option option : in) {
      final OptionsEnum opt = enumById.get(option.getId());
      if (opt != null) {
        res.put(opt, clone ? option.clone() : option);
      }
    }
    return res;
  }

  public CtuluLogGroup reloadUserConfiguration(final UserConfiguration user) {
    final CtuluLogGroup logs = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    logs.setDescription("conf.validConfUser");
    init(user, logs, false);
    return logs;
  }

  public CtuluLogGroup validUserConfiguration(final UserConfiguration user) {
    final CtuluLogGroup logs = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    logs.setDescription("conf.validConfUser");
    init(user, logs, true);
    return logs;
  }

  public CtuluLogGroup init(final Configuration siteConfiguration, final UserConfiguration user) {
    siteReadOnlyOptions = siteConfiguration.getSite().getOptions();
    final UserConfiguration siteModifiableByUserOptions = siteConfiguration.getUser();
    final CtuluLogGroup logs = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    logs.setDescription("conf.validConfGlobal");
    final CtuluLog siteLog = logs.createLog();
    siteLog.setDesc("conf.validConfSite");
    //on ne clone pas les SiteOptions car ne seront jamais modifiée.
    final Map<OptionsEnum, Option> siteReadOnlyOptionMap = build(siteReadOnlyOptions, false);
    final Map<OptionsEnum, Option> siteAllOptionMap = build(siteModifiableByUserOptions.getOptions(), false);
    siteAllOptionMap.putAll(siteReadOnlyOptionMap);
    if (siteConfiguration.getSite().getAide() != null) {
      setAideOption(siteConfiguration.getSite().getAide());
    } else {
      siteLog.addRecord(getLevel(true), "conf.undefinedHelpConfiguration", "<SiteAide>");
    }
    validIsAllSetKnown(siteLog, siteAllOptionMap, true);//toutes les options doivent être définies
    validSiteOptions(siteLog, siteReadOnlyOptions, siteReadOnlyOptionMap);
    validValuesOfOption(siteLog, siteAllOptionMap, siteReadOnlyOptionMap);
    validUserOptions(siteModifiableByUserOptions, siteLog, siteReadOnlyOptionMap, true);
    this.siteOptionById.clear();
    this.siteOptionById.putAll(siteAllOptionMap);
    this.userOptionById.clear();
    init(user, logs, false);
    return logs;
  }

  private void validNoSiteOptionOveridden(final CtuluLog log, final Map<OptionsEnum, ? extends Option> siteOptionMap,
                                          final Map<OptionsEnum, ? extends Option> userOptionMap, final boolean useFatal) {
    final Set<OptionsEnum> keySet = userOptionMap.keySet();
    for (final OptionsEnum optionEnum : keySet) {
      final Option siteOption = siteOptionMap.get(optionEnum);
      if (siteOption != null && !siteOption.isEditable()) {
        log.addRecord(getLevel(useFatal), "conf.userConfOverridesSiteConf", siteOption.getId());
      }
    }
  }

  private void validIsKnown(final CtuluLog targetLog, final Collection<? extends Option> options, final boolean useFatal) {
    for (final Option option : options) {
      if (!enumById.containsKey(option.getId())) {
        targetLog.addRecord(getLevel(useFatal), "conf.optionUnknown", option.getId());
      }
    }
  }

  private void validNoTwice(final CtuluLog targetLog, final Collection<? extends Option> options, final boolean useFatal) {
    final Set<String> alreadySet = new HashSet<>();
    for (final Option option : options) {
      if (alreadySet.contains(option.getId())) {
        targetLog.addRecord(getLevel(useFatal), "conf.defineTwice", option.getId());
      } else {
        alreadySet.add(option.getId());
      }
    }
  }

  private void validRequiredButEmptyAndNotEditable(final CtuluLog targetLog, final Map<OptionsEnum, ? extends Option> optionMap) {

    for (final OptionsEnum optionEnum : enumById.values()) {
      if (optionEnum.isRequired()) {
        final Option option = optionMap.get(optionEnum);
        if (option != null && !option.isEditable() && StringUtils.isEmpty(option.getValeur())) {
          targetLog.addError("conf.optionRequiredEmptyButNoEditable", option.getId());
        }
      }
    }
  }

  private void validValuesOfOption(final CtuluLog targetLog, final Map<OptionsEnum, ? extends Option> optionMap,
                                   final Map<OptionsEnum, ? extends Option> siteReadOnlyOptionMap) {
    for (final OptionsEnum optionEnum : enumById.values()) {
      if (optionEnum.getValidator() != null) {
        final Option option = optionMap.get(optionEnum);
        if (option != null && option.getValeur() != null) {
          optionEnum.getValidator().isValid(option.getValeur(), targetLog, siteReadOnlyOptionMap);
        }
      }
    }
  }

  private void validIsAllSetKnown(final CtuluLog targetLog, final Map<OptionsEnum, ? extends Option> optionMap, final boolean useFatal) {

    for (final OptionsEnum enumOption : enumById.values()) {
      if (!optionMap.containsKey(enumOption)) {
        targetLog.addRecord(getLevel(useFatal), "conf.optionNotSet", enumOption.getId());
      }
    }
  }

  protected static class VisiblePredicate implements Predicate {
    @Override
    public boolean evaluate(final Object object) {
      return ((Option) object).isUserVisible();
    }
  }
}
