package org.fudaa.dodico.crue.config.ccm;

import org.fudaa.dodico.crue.common.contrat.XmlFileConfig;
import org.fudaa.dodico.crue.common.io.CrueIOResu;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * @author deniger
 */
public class CrueConfigMetierLoader {
  static final List<String> KNOWN_VERSION = Arrays.asList("1.1.1", "1.2","1.3");

  public CrueIOResu<CrueConfigMetier> load(final File crueConfig, final XmlFileConfig coeurConfig) {
    final CrueConfigMetierReader loader = new CrueConfigMetierReader(coeurConfig);
    return loader.readConfigMetier(crueConfig);
  }

  public CrueIOResu<CrueConfigMetier> load(final String crueConfig, final XmlFileConfig coeurConfig) {
    final CrueConfigMetierReader loader = new CrueConfigMetierReader(coeurConfig);
    return loader.readConfigMetier(crueConfig);
  }

}
