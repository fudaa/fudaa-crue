package org.fudaa.dodico.crue.projet.conf;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;

/**
 * Options
 */
public abstract class Option implements Cloneable {

  protected String id;
  protected String commentaire;
  protected String valeur;
  protected String displayName;

  public Option() {
    super();
  }
  
  public static String geti18n(String optionId) {
    return BusinessMessages.getString("option." + optionId + ".name");
  }

  @Override
  public Option clone() {
    try {
      return (Option) super.clone();
    } catch (CloneNotSupportedException ex) {
      Logger.getLogger(Option.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
  }

  public String getDisplayName() {
    if (displayName == null) {
      displayName = BusinessMessages.getStringOrDefault("option." + id + ".name", id);
    }
    return displayName;
  }

  public abstract boolean isUserVisible();

  public abstract boolean isEditable();

  public String getCommentaire() {
    if (StringUtils.isEmpty(commentaire)) {
      commentaire = BusinessMessages.getStringOrDefault("option." + id + ".comment", "<no comment>");
    }
    return commentaire;
  }

  public String getId() {
    return id;
  }

  public String getValeur() {
    return valeur;
  }
}