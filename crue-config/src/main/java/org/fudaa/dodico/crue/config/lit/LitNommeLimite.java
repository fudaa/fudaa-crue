/*
 GPL 2
 */
package org.fudaa.dodico.crue.config.lit;

import org.fudaa.ctulu.ToStringTransformable;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierLitNomme;

/**
 *
 * Une limite de litnomme. before ou after peut etre vide si premier dernier
 *
 * @author Frederic Deniger
 */
public class LitNommeLimite implements ToStringInternationalizable, ToStringTransformable, ObjetNomme {

  private final String nom;
  private final LitNomme before;
  private final LitNomme after;
  private final int idx;

  public LitNommeLimite(String nom, LitNomme before, LitNomme after, int idx) {
    this.nom = nom;
    this.before = before;
    this.after = after;
    this.idx = idx;
  }
  
  public boolean isLitMineurStart(){
    return idx== CrueConfigMetierLitNomme.MINEUR;
  }
  public boolean isLitMineurEnd(){
    return idx==CrueConfigMetierLitNomme.MAJEUR_END;
  }

  public boolean isFirst() {
    return before == null;
  }

  public boolean isLast() {
    return after == null;
  }

  @Override
  public int hashCode() {
    return Integer.valueOf(idx).hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final LitNommeLimite other = (LitNommeLimite) obj;
    return idx == other.getIdx();
  }

  public LitNomme getBefore() {
    return before;
  }

  public LitNomme getAfter() {
    return after;
  }

  public int getIdx() {
    return idx;
  }

  @Override
  public String geti18n() {
    return getNom();
  }

  @Override
  public String geti18nLongName() {
    return getNom();
  }

  @Override
  public String getAsString() {
    return getNom();
  }

  @Override
  public void setNom(String newNom) {
  }

  @Override
  public String getId() {
    return getNom();
  }

  @Override
  public String getNom() {
    return nom;
  }
}
