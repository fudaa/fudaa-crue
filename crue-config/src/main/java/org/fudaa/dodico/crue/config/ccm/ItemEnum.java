package org.fudaa.dodico.crue.config.ccm;

import org.fudaa.ctulu.ToStringTransformable;
import org.fudaa.dodico.crue.common.contrat.DoubleValuable;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

/**
 * @author deniger
 */
public final class ItemEnum implements Comparable<ItemEnum>, ToStringInternationalizable, ToStringTransformable, DoubleValuable {
  private final int id;
  private final String name;

  public ItemEnum(final int id, final String name) {
    this.id = id;
    this.name = name;
  }

  @Override
  public double toDoubleValue() {
    return id;
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  @Override
  public String geti18n() {
    return name;
  }

  @Override
  public String geti18nLongName() {
    return geti18n();
  }

  @Override
  public String getAsString() {
    return getName();
  }

  @Override
  public int compareTo(final ItemEnum obj) {
    if (obj == null) {
      return -1;
    }
    if (obj == this) {
      return 0;
    }
    return id - obj.id;
  }

  @Override
  public int hashCode() {
    return id;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (obj instanceof ItemEnum) {
      return id == ((ItemEnum) obj).id;
    }
    return false;
  }
}
