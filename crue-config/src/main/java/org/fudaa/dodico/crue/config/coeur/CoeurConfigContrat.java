package org.fudaa.dodico.crue.config.coeur;

import org.fudaa.dodico.crue.common.contrat.XmlFileConfig;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;

import java.net.URL;
import java.util.List;

/**
 * @author Fred Deniger
 */
public interface CoeurConfigContrat extends XmlFileConfig {
  String getName();

  CrueConfigMetier getCrueConfigMetier();

  String getCrueConfigMetierURL();

  /**
   * @param local fr_FR, en_EN
   * @return fr_FR.msg.xml
   */
  String getMessageURL(String local);

  List<String> getAvailableLanguagesForMessage();

  String getCoeurFolderPath();

  String getExecFile();

  URL getDefaultFile(FileType fileType);

  /**
   * @return the usedByDefault
   */
  boolean isUsedByDefault();

  boolean isCrue9Dependant();
}
