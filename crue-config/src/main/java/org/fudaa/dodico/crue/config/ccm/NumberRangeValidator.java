package org.fudaa.dodico.crue.config.ccm;

import org.apache.commons.lang3.StringUtils;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class NumberRangeValidator implements Cloneable {
  /**
   * The minimum number in this range.
   */
  private Number min;
  /**
   * The maximum number in this range.
   */
  private Number max;
  private String minName;
  private String maxName;
  private boolean minStrict;
  private boolean maxStrict;
  private ItemContentAbstract property;

  /**
   * Constructeur par defaut
   */
  public NumberRangeValidator() {
  }

  public NumberRangeValidator(final Number min, final Number max) {
    this.min = min;
    this.max = max;
  }

  public boolean containsVar() {
    return isMaxVariable() || isMinVariable();
  }

  private final static Logger LOGGER = Logger.getLogger(NumberRangeValidator.class.getName());

  /**
   * @return le clone de NumberRangeValidator
   */
  public NumberRangeValidator copy() {
    try {
      return (NumberRangeValidator) clone();
    } catch (final CloneNotSupportedException e) {
      LOGGER.log(Level.SEVERE, "copy", e);
    }
    return null;
  }

  private boolean containsMax(final Number number) {
    if (property != null) {
      if ((!property.getNature().isDate()) && property.getNature().isOverInfiniPositif(number.doubleValue())) {
        return false;
      }
    }
    if (max == null || (property != null && property.isInfiniPositif(max.doubleValue()))) {
      return true;
    }
    if (number == null) {
      return false;
    }
    final int compareMax = ((Comparable) max).compareTo(number.doubleValue());
    return (maxStrict) ? compareMax > 0 : compareMax >= 0;
  }

  private boolean containsMin(final Number number) {
    if (property != null) {
      if ((!property.getNature().isDate()) && property.getNature().isOverInfiniNegatif(number.doubleValue())) {
        return false;
      }
    }
    if (min == null || (property != null && property.isInfiniNegatif(min.doubleValue()))) {
      return true;
    }
    if (number == null) {
      return false;
    }
    final int compareMin = ((Comparable) min).compareTo(number.doubleValue());
    return (minStrict) ? compareMin < 0 : compareMin <= 0;
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  /**
   * @param number le nombre a comparer
   * @return true si contenu
   */
  public boolean containsNumber(final Number number) {
    if (number == null) {
      return false;
    }
    return containsMin(number) && containsMax(number);
  }

  public Number getMax() {
    return max;
  }

  private String maxToString() {
    if (StringUtils.isNotBlank(maxName)) {
      return maxName;
    }
    return property.format(max, DecimalFormatEpsilonEnum.COMPARISON);
  }

  private String minToString() {
    if (StringUtils.isNotBlank(minName)) {
      return minName;
    }
    return property.format(min, DecimalFormatEpsilonEnum.COMPARISON);
  }

  public String getDesc() {
    if (min == null && max == null) {
      return "non défini";
    }
    if (min == null) {
      if (maxStrict) {
        return "<" + maxToString();
      } else {
        return "<=" + maxToString();
      }
    }
    if (max == null) {
      if (minStrict) {
        return ">" + minToString();
      } else {
        return ">=" + minToString();
      }
    }
    final String crochBas = minStrict ? "]" : "[";
    final String crochHaut = maxStrict ? "[" : "]";
    final String res = crochBas + minToString() + ";" + maxToString() + crochHaut;
    if (property != null && property.getNature().isDuration()) {
      return "s: " + res;
    }
    return res;
  }

  public Number getMaximumNumber() {
    return max;
  }

  public String getMaxName() {
    return maxName;
  }

  public Number getMin() {
    return min;
  }

  public Number getMinimumNumber() {
    return min;
  }

  public String getMinName() {
    return minName;
  }

  public boolean isMaxStrict() {
    return maxStrict;
  }

  private boolean isMaxVariable() {
    return StringUtils.isNotBlank(maxName);
  }

  public boolean isMinStrict() {
    return minStrict;
  }

  private boolean isMinVariable() {
    return StringUtils.isNotBlank(minName);
  }

  public void setMax(final Number max) {
    this.max = max;
  }

  public void setMaxStrict(final boolean maxStrict) {
    this.maxStrict = maxStrict;
  }

  public void setMaxName(final String maxName) {
    this.maxName = maxName;
  }

  public void setMin(final Number min) {
    this.min = min;
  }

  public void setMinStrict(final boolean minStrict) {
    this.minStrict = minStrict;
  }

  public void setMinName(final String minName) {
    this.minName = minName;
  }

  /**
   * @return the paramToValid
   */
  protected ItemContentAbstract getParamToValid() {
    return property;
  }

  /**
   * @param paramToValid the paramToValid to set
   */
  protected void setParamToValid(final ItemContentAbstract paramToValid) {
    this.property = paramToValid;
  }
}
