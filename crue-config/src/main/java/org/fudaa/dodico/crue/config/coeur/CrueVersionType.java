package org.fudaa.dodico.crue.config.coeur;

/**
 * Enum pour différencier les versions "majeures" de crue.
 *
 * @author deniger
 */
public enum CrueVersionType {

    CRUE10("Crue10", "c10", "Crue 10"),
    CRUE9("Crue9", "c9", "Crue 9");
    private final String suffix;
    private final String typeId;
    private final String displayName;

    /**
     * @param typeId      le type
     * @param suffix      le suffixe
     * @param displayName le nom de la version
     */
    CrueVersionType(final String typeId, final String suffix, final String displayName) {
        this.suffix = suffix;
        this.typeId = typeId;
        this.displayName = displayName;
    }

    public static CrueVersionType getVersionFromType(final String type) {
        if (type == null) {
            return null;
        }
        return CrueVersionType.valueOf(type.toUpperCase());
    }

    public String getDisplayName() {
        return displayName;
    }

    /**
     * @return the suffix
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     * @return the typeId
     */
    public String getTypeId() {
        return typeId;
    }

}
