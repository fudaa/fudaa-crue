/*
 GPL 2
 */
package org.fudaa.dodico.crue.config.lit;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierLitNomme;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;

import java.util.*;

/**
 * @author Frederic Deniger
 */
public class ReportLongitudinalLimitHelper {
  private static String getEtiquetteId(final int idx) {
    return "Et_[" + idx + "]";
  }

  public static String getLimiteId(final int idx) {
    return "Limit_[" + idx + "]";
  }

  private final Map<String, ItemEnum> etiquetteById = new LinkedHashMap<>();
  private final Map<String, LitNommeLimite> limiteById = new LinkedHashMap<>();

  public ReportLongitudinalLimitHelper(final CrueConfigMetier ccm) {
    final CrueConfigMetierLitNomme litNomme = ccm.getLitNomme();
    final List<ItemEnum> etiquettesEnums = litNomme.getEtiquettesEnums();
    for (final ItemEnum itemEnum : etiquettesEnums) {
      if (!itemEnum.equals(litNomme.getEtiquetteLimLit())) {
        etiquetteById.put(getEtiquetteId(itemEnum.getId()), itemEnum);
      }
    }
    final List<LitNommeLimite> limites = litNomme.getLimites();
    for (int i = 0; i < limites.size(); i++) {
      final LitNommeLimite litNommeLimite = limites.get(i);
      limiteById.put(getLimiteId(i), litNommeLimite);
    }
  }

  public Map<String, String> getDisplayNameById() {
    final LinkedHashMap<String, String> idByDisplayName = getIdByDisplayName();
    final Map<String, String> displayNameById = new HashMap<>();
    for (final Map.Entry<String, String> entry : idByDisplayName.entrySet()) {
      displayNameById.put(entry.getValue(), entry.getKey());
    }
    return displayNameById;
  }

  public LinkedHashMap<String, String> getIdByDisplayName() {
    final LinkedHashMap<String, String> idByDisplayName = new LinkedHashMap<>();
    final int min = 3;
    final List<LitNommeLimite> limites = new ArrayList<>(limiteById.values());
    for (int i = 0; i < min; i++) {
      idByDisplayName.put(limites.get(i).getAsString(), getLimiteKey(limites.get(i)));
    }
    final List<ItemEnum> ets = new ArrayList<>(etiquetteById.values());
    for (final ItemEnum itemEnum : ets) {
      idByDisplayName.put(itemEnum.getAsString(), getEtiquetteKey(itemEnum));
    }
    for (int i = min; i < limites.size(); i++) {
      idByDisplayName.put(limites.get(i).getAsString(), getLimiteKey(limites.get(i)));
    }
    return idByDisplayName;
  }

  public ItemEnum getEtiquette(final String etiquetteId) {
    return etiquetteById.get(etiquetteId);
  }

  public LitNommeLimite getLimite(final String limiteId) {
    return limiteById.get(limiteId);
  }

  private static String getEtiquetteKey(final ItemEnum item) {
    return getEtiquetteId(item.getId());
  }

  private static String getLimiteKey(final LitNommeLimite item) {
    return getLimiteId(item.getIdx());
  }
}
