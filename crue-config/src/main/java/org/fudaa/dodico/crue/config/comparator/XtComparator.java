package org.fudaa.dodico.crue.config.comparator;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;

/**
 *
 * @author deniger
 */
public class XtComparator extends DoubleComparator{

  public XtComparator(CrueConfigMetier configMetier) {
    super(configMetier.getEpsilon("xt"));
  }
}
