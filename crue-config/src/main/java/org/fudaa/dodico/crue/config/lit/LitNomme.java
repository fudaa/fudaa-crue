package org.fudaa.dodico.crue.config.lit;

import org.fudaa.dodico.crue.common.contrat.ObjetNomme;

public class LitNomme implements ObjetNomme {
    private final int idx;
    private String nom;
    private String id;

    public LitNomme(final String nom, int idx) {
        super();
        this.idx = idx;
        setNom(nom);
    }

    @Override
    public final String getNom() {
        return nom;
    }

    /**
     * @param newNom le nouveau nom
     */
    @Override
    public final void setNom(final String newNom) {
        nom = newNom;
        if (nom != null) {
            id = nom.toUpperCase();
        }
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LitNomme other = (LitNomme) obj;
        return (this.id == null) ? (other.id == null) : this.id.equals(other.id);
    }

    @Override
    public String toString() {
        return getNom();
    }

    public int getIdx() {
        return idx;
    }

    @Override
    public String getId() {
        return id;
    }
}
