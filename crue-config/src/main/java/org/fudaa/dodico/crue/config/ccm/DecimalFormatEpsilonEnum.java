/*
 GPL 2
 */
package org.fudaa.dodico.crue.config.ccm;

/**
 *
 * @author Frederic Deniger
 */
public enum DecimalFormatEpsilonEnum {
  PRESENTATION,
  COMPARISON
}
