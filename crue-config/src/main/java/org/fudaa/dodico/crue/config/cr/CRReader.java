package org.fudaa.dodico.crue.config.cr;

import com.Ostermiller.util.CSVParser;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.config.ccm.SeveriteManager;
import org.joda.time.LocalDateTime;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class CRReader {
  private static final int COLUMN_ARGS = 6;
  private static final int COLUMN_LINE_NUMBER = 5;
  private static final int COLUMN_FILE = 4;
  private static final int COLUMN_DATE = 0;
  private static final int COLUMN_ID = 1;
  private static final int COLUMN_LEVEL = 2;
  private static final int COLUMN_FONCTION = 3;
  private static final Logger LOGGER = Logger.getLogger(CRReader.class.getName());
  private final int maxRecord;

  public CRReader(final int maxRecord) {
    this.maxRecord = maxRecord;
  }

  public static CtuluLogLevel getLevel(final String in) {
    if (in == null) {
      return CtuluLogLevel.ERROR;
    }
    if (in.startsWith("FATAL")) {
      return CtuluLogLevel.FATAL;
    }
    if (in.startsWith("ERRBLK")) {
      return CtuluLogLevel.SEVERE;
    }
    if (in.startsWith("ERR")) {
      return CtuluLogLevel.ERROR;
    }
    if (in.startsWith("WARN")) {
      return CtuluLogLevel.WARNING;
    }
    if (in.startsWith("DEBUG")) {
      return CtuluLogLevel.DEBUG;
    }
    if (in.startsWith("INFO")) {
      return CtuluLogLevel.INFO;
    }
    if (in.startsWith("FCT")) {
      return CtuluLogLevel.DEBUG;
    }
    return CtuluLogLevel.ERROR;
  }

  public static Map<String, Object> getDataMapWithoutDate(final CtuluLogRecord record) {
    final Map<String, Object> res = new HashMap<>();
    res.put("logSeverity", record.getLevelDetail());
    res.put("logFunction", record.getRessourceFunction());
    res.put("logLine", record.getRessourceLine());
    res.put("logIdmsg", record.getId());
    final Object[] args = record.getArgs();
    if (ArrayUtils.isNotEmpty(args)) {
      res.put("logEntity", ObjectUtils.toString(args[0]));
      if (args.length > 1) {
        res.put("logArgs", StringUtils.join(args, ";", 1, args.length));
      }
    } else {
      //toujours les ajouter pour faciliter les comparaisons.
      res.put("logEntity", null);
      res.put("logArgs", null);
    }
    return res;
  }

  public CrueIOResu<CtuluLog> read(final File f) {
    FileReader reader = null;
    CrueIOResu<CtuluLog> read = null;
    try {
      reader = new FileReader(f);
      read = read(reader);
    } catch (final FileNotFoundException ex) {
      LOGGER.log(Level.SEVERE, null, ex);
    } finally {
      CtuluLibFile.close(reader);
    }
    return read;
  }

  public CrueIOResu<CtuluLogLevel> getHigherLevel(final File f) {
    FileReader reader = null;
    CrueIOResu<CtuluLogLevel> read = null;
    try {
      reader = new FileReader(f);
      read = getHigherLevel(reader);
    } catch (final FileNotFoundException ex) {
      LOGGER.log(Level.SEVERE, null, ex);
    } finally {
      CtuluLibFile.close(reader);
    }
    return read;
  }

  public CrueIOResu<CtuluLog> read(final URL url) {
    InputStream input = null;
    CrueIOResu<CtuluLog> read = null;
    try {
      input = url.openStream();
      read = read(new InputStreamReader(input));
    } catch (final IOException ex) {
      LOGGER.log(Level.SEVERE, null, ex);
    } finally {
      CtuluLibFile.close(input);
    }
    return read;
  }

  public CrueIOResu<CtuluLogLevel> getHigherLevel(final URL url) {
    InputStream input = null;
    CrueIOResu<CtuluLogLevel> read = null;
    try {
      input = url.openStream();
      read = getHigherLevel(new InputStreamReader(input));
    } catch (final IOException ex) {
      LOGGER.log(Level.SEVERE, null, ex);
    } finally {
      CtuluLibFile.close(input);
    }
    return read;
  }

  private class LogRecorder {
    final CtuluLog readLog = new CtuluLog();
    boolean maxReached = false;
    CtuluLogRecord higherRecord = null;

    protected void addRecord(final CtuluLogRecord record) {
      if (record == null) {
        return;
      }
//si le max est atteint on cherche simplement à stocker la ligne avec la sévérité la plus grande
      if (maxReached) {
        if (higherRecord == null) {
          higherRecord = record;
        } else if (higherRecord.getLevel().isMoreVerboseThan(record.getLevel(), true)) {
          higherRecord = record;
        }
      } else {
        readLog.addRecord(record);
      }
      if (!maxReached && maxRecord > 0 && readLog.getRecords().size() >= maxRecord) {
        readLog.addRecord(createEndRecord());
        maxReached = true;
      }
    }

    protected CtuluLog getFinalLog() {
      if (higherRecord != null) {
        readLog.addRecord(higherRecord);
        higherRecord = null;
      }
      return readLog;
    }
  }

  private CrueIOResu<CtuluLog> read(final Reader f) {
    final LogRecorder readLog = new LogRecorder();
    final CtuluLog log = new CtuluLog();
    try {
      final LineNumberReader lineReader = new LineNumberReader(f);
      String line = lineReader.readLine();

      while (line != null) {
        final String[] split = StringUtils.splitPreserveAllTokens(line, ';');
        if (split.length > 3) {
          try {
            final String id = split[COLUMN_ID];
            final String levelDetail = split[COLUMN_LEVEL];
            final CtuluLogRecord record = new CtuluLogRecord(getLevel(levelDetail), id, id);
            record.setLevelDetail(levelDetail);
            readLog.addRecord(record);
            String dateValue = split[COLUMN_DATE];
            //dans le cas de la sortie standard, la ligne commence par E ou S ou @
            if (StringUtils.isNotEmpty(dateValue) && (!CharUtils.isAsciiNumeric(dateValue.charAt(0)))) {
              dateValue = dateValue.substring(1);
            }
            final LocalDateTime date = DateDurationConverter.getDate(dateValue);
            if (date != null) {
              record.setLogDate(date.toDateTime().toDate());
            }
            if (split.length > COLUMN_FONCTION) {
              record.setRessourceFunction(split[COLUMN_FONCTION]);
            }
            if (split.length > COLUMN_FILE) {
              record.setRessource(split[COLUMN_FILE]);
            }
            if (split.length > COLUMN_LINE_NUMBER) {
              record.setRessourceLine(split[COLUMN_LINE_NUMBER]);
            }
            final List args = new ArrayList();
            if (split.length > COLUMN_ARGS) {
              //on reconstruit la chaine pour prendre en compte les quotes:
              final StringBuilder builder = new StringBuilder();
              for (int i = COLUMN_ARGS; i < split.length; i++) {
                String splitToken = split[i];
                //pour les arguments supplémentaire i> COLUMN_ARGS, on double les \
                if (splitToken.indexOf('\\') >= 0 && i > COLUMN_ARGS) {
                  splitToken = StringUtils.replace(splitToken, "\\", "\\\\");
                }
                builder.append(splitToken);
                if (i < split.length - 1) {
                  builder.append(';');
                }
              }
              final CSVParser parser = new CSVParser(new StringReader(builder.toString()));
              parser.changeQuote('"');
              parser.changeDelimiter(';');
              final String[] otherArgs = parser.getLine();
              if (otherArgs != null) {
                args.addAll(Arrays.asList(otherArgs));
              }
            }
            record.setArgs(args.toArray());
          } catch (final Exception exception) {
            Logger.getLogger(CRReader.class.getName()).log(Level.INFO, "log invalid", lineReader.getLineNumber());
            Logger.getLogger(CRReader.class.getName()).log(Level.INFO, "log invalid", exception);
            readLog.addRecord(createDefaultRecord(line));
          }
        } else {
          readLog.addRecord(createDefaultRecord(line));
        }
        line = lineReader.readLine();
      }
    } catch (final IOException ioe) {
      LOGGER.log(Level.WARNING, ioe.getMessage(), ioe);
      log.addWarn("io.log.errorWhileReader", ioe.getMessage());
    }
    return new CrueIOResu<>(readLog.getFinalLog(), log);
  }

  private CtuluLogLevel getHigherLevel(final CtuluLogLevel current, final CtuluLogLevel read) {
    if (current == null) {
      return read;
    }
    if (read == null) {
      return current;
    }
    if (current.isMoreVerboseThan(read, true)) {
      return read;
    }
    return current;
  }

  private CrueIOResu<CtuluLogLevel> getHigherLevel(final Reader f) {
    CtuluLogLevel res = null;
    final CtuluLog log = new CtuluLog();
    try {
      final LineNumberReader lineReader = new LineNumberReader(f);
      String line = lineReader.readLine();
      while (line != null) {
        final String[] split = StringUtils.splitPreserveAllTokens(line, ';');
        if (split.length > 3) {
          try {
            final String levelDetail = split[COLUMN_LEVEL];
            final CtuluLogLevel readLevel = getLevel(levelDetail);
            res = getHigherLevel(res, readLevel);
          } catch (final Exception exception) {
            Logger.getLogger(CRReader.class.getName()).log(Level.INFO, "log invalid", lineReader.getLineNumber());
            Logger.getLogger(CRReader.class.getName()).log(Level.INFO, "log invalid", exception);
          }
        }
        line = lineReader.readLine();
      }
    } catch (final IOException ioe) {
      LOGGER.log(Level.WARNING, ioe.getMessage(), ioe);
      log.addWarn("io.log.errorWhileReader", ioe.getMessage());
    }
    return new CrueIOResu<>(res, log);
  }

  private CtuluLogRecord createDefaultRecord(final String line) {
    final CtuluLogRecord ctuluLogRecord = new CtuluLogRecord(CtuluLogLevel.INFO, line);
    ctuluLogRecord.setLevelDetail(SeveriteManager.INFO);
    ctuluLogRecord.setLocalizedMessage(line);
    ctuluLogRecord.setLogDate(null);
    return ctuluLogRecord;
  }

  private CtuluLogRecord createEndRecord() {
    final CtuluLogRecord ctuluLogRecord = new CtuluLogRecord(CtuluLogLevel.INFO, "maxReadLineReached");
//    ctuluLogRecord.setArgs(new Object[]{});
    ctuluLogRecord.setLocalizedMessage(BusinessMessages.getString(ctuluLogRecord.getMsg(), Integer.toString(maxRecord)));
    ctuluLogRecord.setLevelDetail("INFO");
    ctuluLogRecord.setLogDate(null);
    return ctuluLogRecord;
  }
}
