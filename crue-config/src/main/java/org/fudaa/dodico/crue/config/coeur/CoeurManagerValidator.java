package org.fudaa.dodico.crue.config.coeur;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;

/**
 * @author CANEL Christophe
 */
public class CoeurManagerValidator {

  public static final String FOLDER_XSD = "xsd";
  public static final String FOLDER_FICHIERS_VIERGES = "fichiers_vierges";
  public static final String FOLDER_MESSAGES = "msg";
  public static final String FILE_CRUE_10_EXE = "crue10.exe";
  public static final String FOLDER_EXE = "exe";
  public static final String FILE_CRUE_CONFIG_METIER_XML = "CrueConfigMetier.xml";

  /**
   *
   * @param manager le manager à valider.
   * @return resultat de validation
   */
  public CtuluLog validate(final CoeurManager manager) {
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("conf.validCoeur");
    //contient pour chaque xsd, l'id du coeur défini par défaut.
    final Map<String, String> idByDefaultByXsd = new HashMap<>();
    //contient la liste des xsd non reconnu par Fudaa-Crue
    for (final CoeurConfig coeur : manager.getAllCoeurConfigs()) {
      final String xsdVersion = coeur.getXsdVersion();
      if (!Crue10VersionConfig.isSupported(xsdVersion)) {
        log.addSevereError("coeur.validator.xsdNotSupported", coeur.getName(), xsdVersion);
      }
      if (coeur.isUsedByDefault()) {
        final String other = idByDefaultByXsd.get(xsdVersion);
        if (other == null) {
          idByDefaultByXsd.put(xsdVersion, coeur.getName());
        } else {
          log.addSevereError("coeur.validator.defaultNotUnique", xsdVersion, other, coeur.getName());
        }
      } else if (!idByDefaultByXsd.containsKey(xsdVersion)) {
        idByDefaultByXsd.put(xsdVersion, null);
      }

      this.validateFolder(coeur.getCoeurFolder(), log);
    }
    //    idByDefaultByXsd.
    for (final Entry<String, String> entry : idByDefaultByXsd.entrySet()) {
      if (entry.getValue() == null) {
        log.addSevereError("coeur.validator.noDefaultCoeur", entry.getKey());
      }

    }
    return log;
  }

  /**
   *
   * @param folder doit etre un dossier et contenir les fichiers/dossiers standard pour un coeur: CrueConfigMetier.xml et un répertoire exe.
   * @param log    contiendra le rapport de la validation.
   */
  private void validateFolder(final File folder, final CtuluLog log) {
    if (!folder.isDirectory()) {
      log.addSevereError("coeur.validator.directoryNotFound", folder.getAbsolutePath());

      return;
    }

    File toTest = new File(folder, FILE_CRUE_CONFIG_METIER_XML);
    if (!toTest.isFile()) {
      log.addSevereError("coeur.validator.fileNotFound", toTest.getAbsolutePath());
    }

    toTest = new File(folder, FOLDER_EXE);
    if (!toTest.isDirectory()) {
      log.addSevereError("coeur.validator.directoryNotFound", toTest.getAbsolutePath());
    } else {
      toTest = new File(toTest, FILE_CRUE_10_EXE);
      if (!toTest.isFile()) {
        log.addWarn("coeur.validator.fileNotFound", toTest.getAbsolutePath());
      }
    }

    toTest = new File(folder, FOLDER_FICHIERS_VIERGES);
    if (!toTest.isDirectory()) {
      log.addSevereError("coeur.validator.directoryNotFound", toTest.getAbsolutePath());
    }

    toTest = new File(folder, FOLDER_XSD);
    if (!toTest.isDirectory()) {
      log.addSevereError("coeur.validator.directoryNotFound", toTest.getAbsolutePath());
    }
    toTest = new File(folder, FOLDER_MESSAGES);
    if (!toTest.isDirectory()) {
      log.addWarn("coeur.validator.messageDirectoryNotFound", toTest.getAbsolutePath());
    }
  }
}
