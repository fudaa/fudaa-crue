/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.projet.conf;

/**
 *
 * @author Chris
 */
public class UserOption extends Option {

  public UserOption() {
  }

  public UserOption(String nom, String valeur, String commentaire) {
    this.id = nom;
    this.valeur = valeur;
    this.commentaire = commentaire;
  }

  @Override
  public UserOption clone() {
    return (UserOption) super.clone();
  }

  @Override
  public boolean isUserVisible() {
    return true;
  }

  @Override
  public boolean isEditable() {
    return true;
  }

  public void setCommentaire(String commentaire) {
    this.commentaire = commentaire;
  }

  public void setNom(String nom) {
    this.id = nom;
  }

  public void setValeur(String valeur) {
    this.valeur = valeur;
  }
}
