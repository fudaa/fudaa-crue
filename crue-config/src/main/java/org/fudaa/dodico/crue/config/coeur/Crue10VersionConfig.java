/*
 GPL 2
 */
package org.fudaa.dodico.crue.config.coeur;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Frederic Deniger
 */
public class Crue10VersionConfig {

  public static final String V_1_1_1 = "1.1.1";
  public static final String V_1_2 = "1.2";
  private static final String V_1_3 = "1.3";
  private static final List<String> versions = Arrays.asList(V_1_1_1, V_1_2,V_1_3);

  public static boolean isV1_1_1(final CoeurConfigContrat version) {
    return version!=null && V_1_1_1.equals(version.getXsdVersion());
  }
  public static boolean isV1_2(final CoeurConfigContrat version) {
    return version!=null && V_1_2.equals(version.getXsdVersion());
  }

  public static boolean isOptrSupported(CoeurConfigContrat version){
    return !isV1_1_1(version);
  }
  public static boolean isDregSupported(CoeurConfigContrat version){
    return isUpperThan_V1_2(version);
  }

  public static boolean isLastVerion(final CoeurConfigContrat version) {
    return version!=null && getLastVersion().equals(version.getXsdVersion());
  }

  public static boolean isLowerOrEqualsTo_V1_2(final CoeurConfigContrat coeurConfigContrat){
    return isV1_2(coeurConfigContrat)|| isV1_1_1(coeurConfigContrat);
  }
  public static boolean isLowerOrEqualsTo_V1_2(final String version){
    return V_1_1_1.equals(version)|| V_1_2.equals(version);
  }
  public static boolean isUpperThan_V1_2(final String version){
    return !isLowerOrEqualsTo_V1_2(version);
  }
  public static boolean isUpperThan_V1_2(final CoeurConfigContrat version){
    return !isLowerOrEqualsTo_V1_2(version);
  }
  public static boolean isV1_1_1(final String version) {
    return V_1_1_1.equals(version);
  }

  public static String getLastVersion() {
    return versions.get(versions.size() - 1);
  }

  public static boolean isSupported(final String version) {
    return versions.contains(version);
  }
}
