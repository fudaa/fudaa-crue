/*
GPL 2
 */
package org.fudaa.dodico.crue.projet.conf;

import java.util.List;
import org.fudaa.dodico.crue.config.ccm.Crue9DecimalFormat;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;

/**
 *
 * @author Frederic Deniger
 */
public class Crue9DecimalFormatBuilder {

  private int read(final SiteConfiguration in) {
    final List<SiteOption> options = in.getOptions();
    if (options == null) {
      return -1;
    }
    for (final SiteOption option : options) {
      if (OptionsEnum.NB_EXPORT_CRUE9_SIGNIFICANT_FIGURES.getId().equals(option.getId())) {
        try {
          return Integer.parseInt(option.getValeur());
        } catch (final NumberFormatException numberFormatException) {
        }
      }
    }

    return -1;
  }

  public Crue9DecimalFormat intialize(final Configuration configuration, final CrueConfigMetier ccm) {
    final int i = read(configuration.getSite());
    final Crue9DecimalFormat fmt;
    if (i < 0) {
      fmt = Crue9DecimalFormat.getDefault();
    } else {
      fmt = new Crue9DecimalFormat(i, Crue9DecimalFormat.STATE.ACTIVE);
    }
    ccm.setCrue9DecimalFormat(fmt);
    return fmt;
  }

}
