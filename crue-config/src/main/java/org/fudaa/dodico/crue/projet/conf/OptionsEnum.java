package org.fudaa.dodico.crue.projet.conf;

import java.io.File;

/**
 *
 * @author deniger
 */
public enum OptionsEnum {

  AVAILABLE_LANGUAGE("availableLanguage", String.class, "fr_FR", true),
  USER_LANGUAGE("userLanguage", String.class, "fr_FR", true, new OptionValidatorLanguage("userLanguage")),
  UI_UPDATE_FREQ("uiUpdateProgressFrequence", Double.class, "0.2", true, new OptionValidatorMinDouble(Double.valueOf(0.1),
          "uiUpdateProgressFrequence")),
  MAX_COMPARAISON_ITEM("maxComparisonItemsDisplayed", Integer.class, "500", true, new OptionValidatorMinDouble(Double.valueOf(1),
          "maxComparisonItemsDisplayed")),
  MAX_RESULTAT_DIFF_BY_EMH("maxComparisonResultsByEMH", Integer.class, "100", true, new OptionValidatorMinDouble(Double.valueOf(1),
          "maxComparisonResultsByEMH")),
  NB_EXPORT_CRUE9_SIGNIFICANT_FIGURES("nbExportCrue9SignificantFigures", Integer.class, "7", true, new OptionValidatorMinDouble(Double.valueOf(-1),
          "nbExportCrue9SignificantFigures")),
  MAX_LOG_ITEM("maxLogLinesRead", Integer.class, "500", true, new OptionValidatorMinDouble(Double.valueOf(1), "maxLogLinesRead")),
  EDITOR("externalEditor", File.class, null, true),
  CRUE9_EXE("crue9Exe", File.class, null, true),
  RPTR_EXE_OPTION("crue10.rptr.exeOption", String.class, "-r", true),
  RPTG_EXE_OPTION("crue10.rptg.exeOption", String.class, "-g", true),
  RPTI_EXE_OPTION("crue10.rpti.exeOption", String.class, "-i", true),
  RCAL_EXE_OPTION("crue10.rcal.exeOption", String.class, "-c", true);
  private final String id;
  private final Class expectedValue;
  private final String defaultValue;
  private final boolean required;
  private final OptionValidator validator;

  OptionsEnum(String id, Class expectValue, String defaultValue, boolean required) {
    this(id, expectValue, defaultValue, required, null);
  }

  OptionsEnum(String id, Class expectValue, String defaultValue, boolean required, OptionValidator validator) {
    this.id = id;
    this.required = required;
    this.expectedValue = expectValue;
    this.defaultValue = defaultValue;
    this.validator = validator;
  }

  public OptionValidator getValidator() {
    return validator;
  }

  public boolean isRequired() {
    return required;
  }

  public String getId() {
    return id;
  }

  public String getDefaultValue() {
    return defaultValue;
  }

  public Class getExpectedValue() {
    return expectedValue;
  }
}
