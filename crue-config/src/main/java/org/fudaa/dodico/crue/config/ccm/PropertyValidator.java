package org.fudaa.dodico.crue.config.ccm;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.common.contrat.DoubleValuable;
import org.joda.time.Duration;
import org.joda.time.LocalDateTime;

/**
 * Permet de valider une propriete de type décimal.
 *
 * @author deniger
 */
public class PropertyValidator {
    private final NumberRangeValidator rangeNormalite;
    private final NumberRangeValidator rangeValidate;
    private ItemContentAbstract paramToValid;

    /**
     * @param rangeValidate  validité
     * @param rangeNormalite normalité
     */
    public PropertyValidator(final NumberRangeValidator rangeValidate, final NumberRangeValidator rangeNormalite) {
        super();
        this.rangeValidate = rangeValidate;
        this.rangeNormalite = rangeNormalite;
    }

    public String getNormaliteWarningForValue(final double value) {
        if (rangeNormalite != null) {
            if (!rangeNormalite.containsNumber(value)) {
                return BusinessMessages.getString("number.notNormal",
                        paramToValid == null ? Double.toString(value) : paramToValid.formatWithoutInfini(value, DecimalFormatEpsilonEnum.COMPARISON),
                        rangeNormalite
                                .getDesc());
            }
        }
        return null;
    }

    private String getParamToDisplay() {
        return StringUtils.capitalize(paramToValid.getNom());
    }

    public void setParamToValid(final ItemContentAbstract paramToValid) {
        this.paramToValid = paramToValid;
        if (rangeNormalite != null) {
            rangeNormalite.setParamToValid(paramToValid);
        }
        if (rangeValidate != null) {
            rangeValidate.setParamToValid(paramToValid);
        }
    }

    public NumberRangeValidator getRangeNormalite() {
        return rangeNormalite;
    }

    public NumberRangeValidator getRangeValidate() {
        return rangeValidate;
    }

    public String getValidateErrorForValue(final double value) {
        if (rangeValidate != null) {
            if (!rangeValidate.containsNumber(value)) {
                return BusinessMessages.getString("number.notValid",
                        paramToValid == null ? Double.toString(value) : paramToValid.formatWithoutInfini(value, DecimalFormatEpsilonEnum.COMPARISON),
                        rangeValidate
                                .getDesc());
            }
        }
        return null;
    }

    public Pair<String, Object[]> getValidateErrorParamForValue(final double value) {
        if (rangeValidate != null) {

            if (!rangeValidate.containsNumber(value)) {
                String msg = "number.notValid";
                Object[] args = new Object[]{paramToValid == null ? Double.toString(value) : paramToValid
                        .formatWithoutInfini(value, DecimalFormatEpsilonEnum.COMPARISON), rangeValidate
                        .getDesc()};
                return new Pair<>(msg, args);
            }
        }
        return null;
    }

    private double getDouble(final Duration duration) {
        return duration.getMillis() / 1000d;
    }

    private Number getNumber(final Object res) {
        if (res == null) {
            return null;
        }
        final Class<?> c = res.getClass();
        if (Duration.class.equals(c)) {
            return getDouble((Duration) res);
        }
        if (LocalDateTime.class.equals(c)) {
            return ((LocalDateTime) res).toDateTime().getMillis();
        }
        if (ItemEnum.class.equals(c)) {
            return Integer.valueOf(((ItemEnum) res).getId());
        }
        if (res instanceof DoubleValuable) {
            return ((DoubleValuable) res).toDoubleValue();
        }
        if (c.isEnum()) {
            return null;
        }
        return (Number) res;
    }

    public void validateValue(final String prefix, final Object value, final CtuluLog res) {
        validateValue(prefix, value, res, true);
    }

    public void validateValue(final String prefix, final Object value, final CtuluLog res, boolean displayParam) {
        final Number number = getNumber(value);
        //cas particulier des enums dans java: a revoir.
        if (number == null && value != null && value.getClass().isEnum()) {
            return;
        }
        validateNumber(prefix, number, res, displayParam);
    }

    public void validateNumber(final String prefix, final Number value, final CtuluLog res) {
        validateNumber(prefix, value, res, true);
    }

    public void validateNumber(final String prefix, final Number value, final CtuluLog res, boolean displayParam) {
        // valeur par defaut: c'est tout bon.
        if (isDefaultValue(value)) {
            return;
        }
        if (paramToValid.getNature().isEnum()) {
            if (value == null || !paramToValid.getNature().getItemEnumByValue().contains(value.intValue())) {
                final String prop = displayParam ? "enum.notValid" : "enum.notValid.noParameter";
                final String msg = BusinessMessages.getString(prop, getParamToDisplay(), value == null ? "<EMPTY>" : Integer.toString(value.intValue()));
                res.addError((prefix == null ? "" : prefix + " ") + msg);
            }
        } else {
            boolean isValid = true;
            if (rangeValidate != null) {
                // parametre pouvant etre null.
                if (!rangeValidate.containsNumber(value)) {
                    isValid = false;
                    final String prop = displayParam ? "prop.notValid" : "prop.notValid.noParameter";
                    final String msg = BusinessMessages.getString(prop, getParamToDisplay(), formatNumber(value),
                            rangeValidate.getDesc());

                    res.addError((prefix == null ? "" : prefix + " ") + msg);
                }
            }
            if (isValid && rangeNormalite != null) {
                if (value == null) {
                    return;
                }
                if (!rangeNormalite.containsNumber(value)) {
                    final String prop = displayParam ? "prop.notNormal" : "prop.notNormal.noParameter";
                    final String msg = BusinessMessages.getString(prop, getParamToDisplay(), formatNumber(value),
                            rangeNormalite.getDesc());
                    res.addWarn((prefix == null ? "" : prefix + " ") + msg);
                }
            }
        }
    }

    private String formatNumber(final Number value) {
        return paramToValid.formatWithoutInfini(value, DecimalFormatEpsilonEnum.COMPARISON);
    }

    /**
     * @param valueToTest valeur a tester
     * @return true si valeur par défaut
     */
    private boolean isDefaultValue(final Number valueToTest) {
        return this.paramToValid.isDefaultOrConstantValue(valueToTest);
    }

    public String getHtmlDesc() {
        return getHtmlDesc(null);
    }

    public String getHtmlDesc(String prefix) {
        if (getRangeValidate() == null) {
            return StringUtils.EMPTY;
        }
        StringBuilder res = new StringBuilder();
        res.append("<html><body>");
        if (prefix != null) {
            res.append(prefix).append("<br>");
        }
        res.append(BusinessMessages.getString("validator.validity"));
        res.append(getRangeValidate().getDesc());
        res.append("<br>");
        res.append(BusinessMessages.getString("validator.normality"));
        res.append(getRangeNormalite().getDesc());
        res.append("</body></html>");
        return res.toString();
    }
}
