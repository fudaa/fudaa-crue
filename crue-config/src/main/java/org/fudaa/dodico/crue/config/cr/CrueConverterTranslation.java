package org.fudaa.dodico.crue.config.cr;

import java.util.HashMap;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueConverter;

/**
 * @author CANEL Christophe
 *
 */
public class CrueConverterTranslation implements CrueConverter<CrueDaoTranslation, CrueLogTranslator> {

  /**
   * {@inheritDoc}
   */
  @Override
  public CrueLogTranslator convertDaoToMetier(final CrueDaoTranslation dao, final CtuluLog ctuluLog) {
    final CrueLogTranslator metier = new CrueLogTranslator();
    metier.locale = dao.Localisation;
    metier.entriesById = new HashMap<>();
    metier.liensDocsById = new HashMap<>();
    metier.liensMultimediaById = new HashMap<>();
    for (final CrueDaoStructureTranslation.Entry entry : dao.Messages) {
      metier.entriesById.put(entry.ID, entry.Texte);
      if (entry.LienDocumentation != null) {
        metier.liensDocsById.put(entry.ID, entry.LienDocumentation);
      }
      if (entry.LienMultimedia != null) {
        metier.liensMultimediaById.put(entry.ID, entry.LienMultimedia);
      }
    }
    return metier;
  }

  /**
   * Not implemented !
   */
  @Override
  public CrueDaoTranslation convertMetierToDao(final CrueLogTranslator metier, final CtuluLog ctuluLog) {
    throw new IllegalAccessError("not implemented");
  }
}
