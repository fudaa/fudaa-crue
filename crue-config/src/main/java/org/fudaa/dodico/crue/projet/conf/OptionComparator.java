package org.fudaa.dodico.crue.projet.conf;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.SafeComparator;

/**
 *
 * Comparaison des DisplayName.
 * @author deniger
 */
public class OptionComparator extends SafeComparator<Option> {

  @Override
  protected int compareSafe(Option o1, Option o2) {
    return StringUtils.defaultString(o1.getDisplayName()).compareTo(StringUtils.defaultString(o1.getDisplayName()));
  }
}
