package org.fudaa.dodico.crue.config.ccm;

import gnu.trove.TIntObjectHashMap;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;

/**
 * @author deniger
 */
public class PropertyNature implements Comparable<PropertyNature> {

  public static final PropertyNature DEFAULT = new PropertyNature("?", PropertyEpsilon.DEFAULT, "", null);

  static {
    DEFAULT.setFormat(new DecimalFormat(), new DecimalFormat());
  }
  private final PropertyEpsilon eps;
  private final String unite;
  private final String nom;
  private final String displayName;
  private final PropertyTypeNumerique typeNumerique;
  private final boolean isEnum;
  private final List<ItemEnum> enumValues;
  private NumberFormat presentationFormat;
  private NumberFormat comparisonFormat;

  public PropertyNature(final String nom, final PropertyEpsilon eps, final String unite, final PropertyTypeNumerique typeNumerique) {
    super();
    this.eps = eps;
    this.typeNumerique = typeNumerique;
    this.nom = nom;
    displayName = StringUtils.substringAfter(nom, "_");
    this.unite = unite;
    isEnum = false;
    enumValues = Collections.emptyList();
  }

  public String getDisplayName() {
    return displayName;
  }

  @Override
  public int compareTo(final PropertyNature o) {
    if (o == null) {
      return 1;
    }
    return SafeComparator.compareString(nom, o.nom);
  }

  /**
   * @return the presentationFormat
   */
  public NumberFormat getFormatter(final DecimalFormatEpsilonEnum type) {
    return DecimalFormatEpsilonEnum.PRESENTATION.equals(type) ? presentationFormat : comparisonFormat;
  }

  /**
   * @param presentationFormat the presentationFormat to set
   */
  protected void setFormat(final NumberFormat presentationFormat, final NumberFormat comparisonFormat) {
    this.presentationFormat = presentationFormat;
    this.comparisonFormat = comparisonFormat;
  }

  public static String getUniteSuffixe(final String unite) {
    return " [" + unite + "]";
  }

  public String format(final double value, final DecimalFormatEpsilonEnum type) {
    try {
      final NumberFormat formatter = getFormatter(type);
      return formatter == null ? Double.toString(value) : formatter.format(value);
    } catch (final Exception e) {
      Logger.getLogger(getClass().getName()).log(Level.WARNING,e.getMessage(),e);
    }
    return null;
  }

  public String format(final Number value, final DecimalFormatEpsilonEnum type) {
    if (value == null) {
      return "null";
    }
    final NumberFormat formatter = getFormatter(type);
    return formatter == null ? value.toString() : formatter.format(value);
  }

  public String formatWithoutInfini(final double value, final DecimalFormatEpsilonEnum type) {
    final NumberFormat formatter = getFormatter(type);
    if (formatter instanceof NumberFormatInfini) {
      return ((NumberFormatInfini) formatter).getBase().format(value);
    }
    return formatter == null ? Double.toString(value) : formatter.format(value);
  }

  public String formatWithoutInfini(final Number value, final DecimalFormatEpsilonEnum type) {
    final NumberFormat formatter = getFormatter(type);
    if (value == null) {
      return "null";
    }
    if (isDate()) {
      return DateDurationConverter.dateToXsdUI(value.longValue());
    }
    if (formatter instanceof NumberFormatInfini) {
      return ((NumberFormatInfini) formatter).getBase().format(value);
    }
    return formatter == null ? value.toString() : formatter.format(value);
  }

  public double getNormalizedValue(final double in) {
    if (comparisonFormat == null) {
      return in;
    }
    final String fmt = comparisonFormat.format(in);
    try {
      return comparisonFormat.parse(fmt).doubleValue();
    } catch (final ParseException ex) {
      Logger.getLogger(ItemContentAbstract.class.getName()).log(Level.SEVERE, null, ex);
    }
    return in;
  }

  public static String getUniteSuffixe(final ItemVariable var) {
    if (var == null) {
      return StringUtils.EMPTY;
    }
    return getUniteSuffixe(var.getNature());
  }

  public String getUniteSuffixe() {
    return getUniteSuffixe(this);

  }

  public static String getUniteSuffixe(final PropertyNature var) {
    if (var == null) {
      return StringUtils.EMPTY;
    }
    final String unite = StringUtils.defaultIfEmpty(var.getUnite(), "-");
    return getUniteSuffixe(unite);
  }

  public List<ItemEnum> getEnumValues() {
    return enumValues;
  }
  private TIntObjectHashMap<ItemEnum> itemEnumByValue = null;

  public TIntObjectHashMap<ItemEnum> getItemEnumByValue() {
    if (itemEnumByValue == null && isEnum) {
      itemEnumByValue = new TIntObjectHashMap<>();
      for (final ItemEnum it : enumValues) {
        itemEnumByValue.put(it.getId(), it);
      }
    }
    return itemEnumByValue;
  }

  PropertyNature(final String enumName, final List<ItemEnum> values) {
    super();
    this.nom = enumName;
    displayName = StringUtils.substringAfter(nom, "_");
    isEnum = true;
    this.eps = null;
    this.typeNumerique = null;
    this.unite = null;
    this.enumValues = values == null ? Collections.emptyList() : Collections.unmodifiableList(values);
  }

  public boolean isDate() {
    return "nat_Date".equals(nom);

  }

  public boolean isDuration() {
    return "nat_Dt".equals(nom);

  }

  public boolean isInfiniPositif(final double value) {
    if (isEnum) {
      return false;
    }
    return value >= getTypeNumerique().getInfini() - getEpsilon().getEpsilonComparaison();
  }

  public boolean isOverInfiniPositif(final double value) {
    if (isEnum) {
      return false;
    }
    return value > getTypeNumerique().getInfini() + getEpsilon().getEpsilonComparaison();
  }

  private boolean isInfini(final double value) {
    if (isEnum) {
      return false;
    }
    return isInfiniPositif(value) || isInfiniNegatif(value);
  }

  private boolean isInfini(final Number value) {
    if (isEnum) {
      return false;
    }
    return isInfiniPositif(value.doubleValue()) || isInfiniNegatif(value.doubleValue());
  }

  public boolean isNotInfini(final double value) {
    return !isInfini(value);
  }

  public boolean isNotInfini(final Number value) {
    return !isInfini(value);
  }

  public boolean isInfiniNegatif(final double value) {
    if (isEnum) {
      return false;
    }
    return value <= -getTypeNumerique().getInfini() + getEpsilon().getEpsilonComparaison();
  }

  public boolean isOverInfiniNegatif(final double value) {
    if (isEnum) {
      return false;
    }
    return value < -getTypeNumerique().getInfini() - getEpsilon().getEpsilonComparaison();
  }

  public boolean isInfiniNegatif(final Number value) {
    if (isEnum) {
      return false;
    }
    return isInfiniNegatif(value.doubleValue());
  }

  public boolean isInfiniPositif(final Number value) {
    return isInfiniPositif(value.doubleValue());
  }

  public String getNom() {
    return nom;
  }

  public String getUnite() {
    return unite;
  }

  /**
   * @return the eps
   */
  public PropertyEpsilon getEpsilon() {
    return eps;
  }

  public PropertyTypeNumerique getTypeNumerique() {
    return typeNumerique;
  }

  public boolean isEnum() {
    return isEnum;
  }

  public boolean needFormatter() {
    return !isEnum;
  }
}
