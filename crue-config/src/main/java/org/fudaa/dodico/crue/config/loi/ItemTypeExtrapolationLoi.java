package org.fudaa.dodico.crue.config.loi;

public class ItemTypeExtrapolationLoi {
    private final String nom;

    /**
     * @param nom le mom de l'extrapolation
     */
    public ItemTypeExtrapolationLoi(String nom) {
        super();
        this.nom = nom;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    public boolean isNone() {
        return "tex_Aucun".equals(nom);
    }

    public boolean isConstant() {
        return "tex_OrdCst".equals(nom);
    }

    public boolean isLineaire() {
        return "tex_OrdInterpolLineaire".equals(nom);
    }
}
