package org.fudaa.dodico.crue.config.loi;

/**
 * @author deniger
 */
public enum EnumExtrapolLoi {
  ValCst, ValInterpol, Aucune
}
