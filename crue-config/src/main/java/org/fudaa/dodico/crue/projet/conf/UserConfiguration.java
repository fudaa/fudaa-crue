package org.fudaa.dodico.crue.projet.conf;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Chris
 */
public class UserConfiguration {

  private Collection<UserOption> options = new ArrayList<>();

  public Collection<UserOption> getOptions() {
    return options;
  }

  public void setOptions(Collection<UserOption> options) {
    this.options = options;
  }
}
