package org.fudaa.dodico.crue.config.ccm;

import gnu.trove.TObjectIntHashMap;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author deniger
 */
public class SeveriteManager {
  public static final String VERBOSITE_ID = "Ten_Severite";
  public static final String INFO = "INFO";
  public static final String DEFAULT_DEBUG_LEVEL_FOR_CRUE9 = "DEBUG2";
  public static final String DEBUG3 = "DEBUG3";
  private final Map<String, ItemEnum> verbosite;

  private SeveriteManager(final List<ItemEnum> initLevels) {
    final List<ItemEnum> levels = initLevels == null ? Collections.emptyList() : Collections.unmodifiableList(initLevels);
    verbosite = new HashMap<>(levels.size());
    for (final ItemEnum level : levels) {
      verbosite.put(level.getName(), level);
    }
  }

  public static TObjectIntHashMap<String> getLevelBySeverity(final CrueConfigMetier metier) {
    final PropertyNature propertyEnum = metier.getPropertyEnum(VERBOSITE_ID);
    final TObjectIntHashMap<String> res = new TObjectIntHashMap<>();
    if (propertyEnum != null) {
      final List<ItemEnum> enumValues = propertyEnum.getEnumValues();
      for (final ItemEnum elem : enumValues) {
        res.put(elem.getName(), elem.getId());
      }
    }
    return res;
  }

  public static SeveriteManager validVerbosite(final CrueConfigMetier metier, final CtuluLog log) {
    final PropertyNature propertyEnum = metier.getPropertyEnum(VERBOSITE_ID);
    if (propertyEnum == null) {
      log.addSevereError("config.verbositeNotFound", VERBOSITE_ID);
      return null;
    } else {
      final SeveriteManager mng = new SeveriteManager(propertyEnum.getEnumValues());
      if (!mng.isManaged(INFO)) {
        log.addSevereError("config.verbositeRequiredLevelNotFound", INFO);
      }
      if (!mng.isManaged(DEFAULT_DEBUG_LEVEL_FOR_CRUE9)) {
        log.addSevereError("config.verbositeRequiredLevelNotFound", DEFAULT_DEBUG_LEVEL_FOR_CRUE9);
      }
      return mng;
    }
  }

  public boolean isManaged(final String level) {
    return verbosite.containsKey(level);
  }

  /**
   * @param level1 level a tester
   * @param level2 level a tester
   * @return true si level1 est strictement plus verbeux que level2.
   */
  public boolean isStrictlyMoreVerbose(final String level1, final String level2) {
    if (StringUtils.equals(level1, level2) || level1 == null || level2 == null) {
      return false;
    }
    final ItemEnum i1 = verbosite.get(level1);
    final ItemEnum i2 = verbosite.get(level2);
    if (i1 != null && i2 != null) {
      return i1.getId() > i2.getId();
    }
    return false;
  }
}
