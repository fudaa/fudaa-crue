/*
GPL 2
 */
package org.fudaa.dodico.crue.config.ccm;

/**
 * @author Frederic Deniger
 */
public class Crue9DecimalFormat {
  public enum STATE {
    ACTIVE, NON_ACTIVE
  }

  private final int numberOfDigits;
  private final STATE state;

  public static Crue9DecimalFormat getDefault() {
    return new Crue9DecimalFormat(0, STATE.NON_ACTIVE);
  }

  public Crue9DecimalFormat(final int numberOfDigits, final STATE state) {
    this.numberOfDigits = numberOfDigits;
    this.state = state;
  }

  private boolean isInactive() {
    return STATE.NON_ACTIVE.equals(this.state) || numberOfDigits <= 0;
  }

  public boolean isActive() {
    return !isInactive();
  }

  public int getNumberOfDigits() {
    return numberOfDigits;
  }

  /**
   * @return si actif ou non.
   */
  public STATE getState() {
    return state;
  }
}
