package org.fudaa.dodico.crue.config.ccm;

import org.fudaa.dodico.crue.common.SafeComparator;

/**
 *
 * @author Frédéric Deniger
 */
public class DoubleEpsilonComparator extends SafeComparator<Double> {

  private final PropertyEpsilon epsilon;

  public DoubleEpsilonComparator(PropertyEpsilon epsilon) {
    this.epsilon = epsilon;
  }

  @Override
  protected int compareSafe(Double o1, Double o2) {
    if (epsilon.isSame(o1, o2)) {
      return 0;

    }
    return o1.compareTo(o2);
  }
}
