package org.fudaa.dodico.crue.projet.conf;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;

/**
 *
 * @author deniger
 */
public class OptionValidatorMinDouble implements OptionValidator {

  private final Double minValue;
  private final String optionName;

  public OptionValidatorMinDouble(final Double minValue, final String optionName) {
    this.minValue = minValue;
    this.optionName = optionName;
  }

  @Override
  public boolean isValid(final String o, final CtuluLog log, final Map<OptionsEnum, ? extends Option> onManager) {
    boolean valid = true;
    if (o == null) {
      if (log != null) {
        log.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
        log.addError("option.ValueIsNull.Error", Option.geti18n(optionName));
      }
      return false;
    }
    final Double valueOf;
    try {
      valueOf = Double.valueOf(o);
    } catch (final NumberFormatException numberFormatException) {
      Logger.getLogger(OptionValidatorMinDouble.class.getName()).log(Level.INFO, "option not a number", numberFormatException);
      if (log != null) {
        log.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
        log.addError("option.DoubleValueIsNotANumber.Error", Option.geti18n(optionName));
      }
      return false;
    }
    if (minValue.compareTo(valueOf) > 0) {
      if (log != null) {
        log.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
        log.addError("option.DoubleValueIsLowerThan.Error", Option.geti18n(optionName), minValue);
      }
      valid = false;
    }

    return valid;
  }
}
