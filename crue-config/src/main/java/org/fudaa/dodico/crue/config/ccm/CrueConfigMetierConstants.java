/*
 GPL 2
 */
package org.fudaa.dodico.crue.config.ccm;

/**
 * @author Frederic Deniger
 */
public final class CrueConfigMetierConstants {
  public static final String PROP_ZSEUIL = "zseuil";
  public static final String PROP_LARGEUR = "largeur";

  private CrueConfigMetierConstants() {

  }

  public static final String CONST_DELTAXT = "DxXtEpsilon";
  static final String ENUM_LIT_NOMME = "Ten_LitNomme";
  static final String ENUM_ETIQUETTE = "Ten_Etiquette";
  public static final String EPSCOMPREL_RPT_C9C9 = "EpsCompRel_RPT_c9c9";
  public static final String EPSCOMPREL_RPT_C9C10 = "EpsCompRel_RPT_c9c10";
  public static final String EPSCOMPREL_RPT_C10C10 = "EpsCompRel_RPT_c10c10";
  public static final String EPSCOMPREL_RCAL_C9C9 = "EpsCompRel_RCAL_c9c9";
  public static final String EPSCOMPREL_RCAL_C9C10 = "EpsCompRel_RCAL_c9c10";
  public static final String EPSCOMPREL_RCAL_C10C10 = "EpsCompRel_RCAL_c10c10";
  public static final String PROP_Z = "z";
  public static final String PROP_ZIMPOSE = "zimp";
  public static final String PROP_Q = "q";
  public static final String PROP_QAPP = "qapp";
  public static final String PROP_QRUIS = "qruis";
  public static final String PROP_ZF = "zf";
  public static final String PROP_XT = "xt";
  public static final String PROP_DISTANCE = "distance";
  public static final String PROP_XP = "xp";
  public static final String PROP_DZ = "dz";
  public static final String NAT_Z = "nat_Z";
  public static final String PROP_PROFONDEUR_FENTE = "profondeurFente";
  public static final String PROP_LARGEUR_FENTE = "largeurFente";
  public static final String PROP_DEFAULT_DX_XT = "dxXtDefaut";
  public static final String PROP_DEFAULT_SEUILSIMPLIFPROFILSECTION = "seuilSimplifProfilSection";
  public static final String PROP_DEFAULT_SEUILSIMPLIFPROFILCASIER = "seuilSimplifProfilCasier";
  public static final String PROP_TEMPSSCE = "tempsSce";
  /**
   * Coefficient de modulation de la loi de perte de charge (cf. FormulePdc)
   */
  public static final String PROP_COEFPDC = "coefPdc";
  public static final String PROP_NBR_PDT = "nbrPdt";
  public static final String PROP_DUREE_PDT = "dureePdt";
  // -- FICHIER DSCP --//
  /**
   * Coefficient de modulation du coefficient de Boussinesq (dans Crue8/9, DC.ALPHA)
   */
  public static final String PROP_COEF_BETA = "coefBeta";
  /**
   * Nombre de Froude limite supérieur pour l'annulation linéaire du terme d'inertie ; par défaut, égal à max(1,0 ; FrLinInf)
   */
  public static final String PROP_FR_LIN_SUP = "frLinSup";
  /**
   * Coefficient de relaxation en débit pour un calcul pseudo-permanent
   */
  public static final String PROP_COEF_RELAX_Q = "coefRelaxQ";
  /**
   * Coefficient de pondération amont/aval entre sections (Phi de Preissmann mais uniquement sur le calcul de J) ; indice i entre les sections i et
   * i+1 ; 0,5 si pondération équilibrée (défaut) ; 1 si pondération amont (en cas d'instabilité sur fort Froude)
   */
  public static final String PROP_COEF_POND = "coefPond";
  /**
   * Nombre de Courant maximal admissible en régime fluvial
   */
  public static final String PROP_CR_MAX_FLU = "crMaxFlu";
  /**
   * Nombre de Courant maximal admissible en régime torrentiel
   */
  public static final String PROP_CR_MAX_TOR = "crMaxTor";
  /**
   * Nombre maximal de pas de temps pseudo-permanents admissible pour le calcul d'un état permanent
   */
  public static final String PROP_NBR_PDT_MAX = "nbrPdtMax";
  /**
   * Tolérance maximale admissible en débit entre deux pas de temps pseudo-permanents successifs
   */
  public static final String PROP_TOL_MAX_Q = "tolMaxQ";
  /**
   * Tolérance maximale admissible en cote entre deux pas de temps pseudo-permanents successifs
   */
  public static final String PROP_TOL_MAX_Z = "tolMaxZ";
  /**
   * Coefficient de convergence entre sections ; indice i entre les sections i et i+1 (dernier ignoré) 0 si pas de prise en compte d'une perte de
   * charge supplémentaire; 0,5 est une valeur grande
   */
  public static final String PROP_COEF_CONV = "coefConv";
  /**
   * Coefficient de divergence entre sections ; indice i entre les sections i et i+1 (dernier ignoré) 0 si pas de prise en compte d'une perte de
   * charge supplémentaire ; 1,0 est une valeur grande
   */
  public static final String PROP_COEF_DIV = "coefDiv";
  /**
   * Nombre de pas de temps pendant lequel une propriété est appliquée (par exemple une certaine durée du pas de temps)
   */
  public static final String PROP_NBR_PDT_DECOUP = "nbrPdtDecoup";
  /**
   * Coefficient de débitance (par exemple d'un élément de seuil ou d'un orifice)
   */
  public static final String PROP_COEF_D = "coefD";
  /**
   * Nombre de Froude limite inférieur pour l'annulation linéaire du terme d'inertie
   */
  public static final String PROP_FR_LIN_INF = "frLinInf";
  /**
   * Coefficient modulation du débit linéique de ruissellement pour chaque branche ou casier (dans Crue8/9, DH.COFQRQ*DC.RUIS)
   */
  public static final String PROP_COEF_RUIS = "coefRuis";
  /**
   * Coefficient de relaxation en cote pour un calcul pseudo-permanent
   */
  public static final String PROP_COEF_RELAX_Z = "coefRelaxZ";
  /**
   * Coefficient de décentrage temporel du schéma de Preissmann
   */
  public static final String PROP_THETA_PREISSMANN = "thetaPreissmann";
  /**
   * Coefficient de sinuosité de la branche >= 1 (dans Crue9, calculée comme LongueurMineur/LongueurMajeur
   */
  public static final String PROP_COEF_SINUO = "coefSinuo";
  /**
   * Coefficient de contraction limite d'un orifice : plus forte contraction atteinte en cas de forte submersion amont; 0,65 par défaut
   */
  public static final String PROP_COEF_CTR_LIM = "coefCtrLim";
  /**
   * Débit limite supérieur admissible
   */
  public static final String PROP_Q_LIM_SUP = "qLimSup";
  /**
   * Débit limite inférieur admissible
   */
  public static final String PROP_Q_LIM_INF = "qLimInf";
  public static final String PROP_OUV = "ouv";
  public static final String PROP_DUREE_CALC = "dureeCalc";
  public static final String PROP_PDT_RES = "pdtRes";
  public static final String PROP_ZINI = "zini";
  public static final String PROP_QINI = "qini";
  public static final String PROP_QECH = "qech";
  public static final String PROP_QLAT = "qlat";
}
