package org.fudaa.dodico.crue.config.cr;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;

/**
 * @author CANEL Christophe
 *
 */
public class CrueDaoTranslation extends AbstractCrueDao {

  public String Localisation;
  public final List<CrueDaoStructureTranslation.Entry> Messages=new ArrayList<>();
}
