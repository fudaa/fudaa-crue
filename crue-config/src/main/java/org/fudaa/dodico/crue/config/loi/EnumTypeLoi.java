package org.fudaa.dodico.crue.config.loi;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Les type de lois
 */
public final class EnumTypeLoi implements ObjetWithID {
    private static final String PREFIX_LOI_DATE = "LoiT";
    private static final String PREFIX_LOI_Z = "LoiZ";
    private static final Map<String, EnumTypeLoi> BASE_TYPE_LOI = new HashMap<>();
    public final static EnumTypeLoi LoiZFK = new EnumTypeLoi("Fk", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiZFKSto = new EnumTypeLoi("FkSto", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiZFN = new EnumTypeLoi("Fn", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiZFNSto = new EnumTypeLoi("FnSto", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiPtProfil = new EnumTypeLoi("LoiPtProfil", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiTQ = new EnumTypeLoi("LoiTQ", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiTZ = new EnumTypeLoi("LoiTZ", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiQZ = new EnumTypeLoi("LoiQZ", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiSectionsZ = new EnumTypeLoi("LoiSectionsZ", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiTOuv = new EnumTypeLoi("LoiTOuv", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiTQapp = new EnumTypeLoi("LoiTQapp", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiTQruis = new EnumTypeLoi("LoiTQruis", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiTZimp = new EnumTypeLoi("LoiTZimp", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiQDz = new EnumTypeLoi("LoiQDz", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiQOuv = new EnumTypeLoi("LoiQOuv", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiQPdc = new EnumTypeLoi("LoiQPdc", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiQpilZam = new EnumTypeLoi("LoiQpilZam", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiQQ = new EnumTypeLoi("LoiQQ", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiQZimp = new EnumTypeLoi("LoiQZimp", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiZavZam = new EnumTypeLoi("LoiZavZam", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiZDact = new EnumTypeLoi("LoiZDact", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiZLsto = new EnumTypeLoi("LoiZLsto", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiZLtot = new EnumTypeLoi("LoiZLtot", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiZLact = new EnumTypeLoi("LoiZLact", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiZLcont = new EnumTypeLoi("LoiZLcont", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiZSact = new EnumTypeLoi("LoiZSact", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiZBeta = new EnumTypeLoi("LoiZBeta", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiZSplan = new EnumTypeLoi("LoiZSplan", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiZVol = new EnumTypeLoi("LoiZVol", BASE_TYPE_LOI);
    public final static EnumTypeLoi LoiZZimp = new EnumTypeLoi("LoiZZimp", BASE_TYPE_LOI);
    public final static EnumTypeLoi LstLitKsup = new EnumTypeLoi("LstLitKsup", BASE_TYPE_LOI);
    public final static EnumTypeLoi LstLitLinf = new EnumTypeLoi("LstLitLinf", BASE_TYPE_LOI);
    public final static EnumTypeLoi LstLitLsup = new EnumTypeLoi("LstLitLsup", BASE_TYPE_LOI);
    public final static EnumTypeLoi LstLitPsup = new EnumTypeLoi("LstLitPsup", BASE_TYPE_LOI);
    public final static EnumTypeLoi LstLitSsup = new EnumTypeLoi("LstLitSsup", BASE_TYPE_LOI);
    public final static EnumTypeLoi LstLitZf = new EnumTypeLoi("LstLitZf", BASE_TYPE_LOI);
    public final static EnumTypeLoi LstLitTypeLit = new EnumTypeLoi("LstLitTypeLit", BASE_TYPE_LOI);
    public final static EnumTypeLoi LstLitPosBordure = new EnumTypeLoi("LstLitPosBordure", BASE_TYPE_LOI);
    public static final EnumTypeLoi LoiZSmin = new EnumTypeLoi("LoiZSmin", BASE_TYPE_LOI);
    public static final EnumTypeLoi LoiZSmaj = new EnumTypeLoi("LoiZSmaj", BASE_TYPE_LOI);
    public static final EnumTypeLoi LoiZPmin = new EnumTypeLoi("LoiZPmin", BASE_TYPE_LOI);
    public static final EnumTypeLoi LoiZPmaj = new EnumTypeLoi("LoiZPmaj", BASE_TYPE_LOI);
    public static final EnumTypeLoi LoiZLmin = new EnumTypeLoi("LoiZLmin", BASE_TYPE_LOI);
    public static final EnumTypeLoi LoiZLmaj = new EnumTypeLoi("LoiZLmaj", BASE_TYPE_LOI);
    public static final EnumTypeLoi LoiZCoefW1 = new EnumTypeLoi("LoiZCoefW1", BASE_TYPE_LOI);
    public static final EnumTypeLoi LoiZCoefW2 = new EnumTypeLoi("LoiZCoefW2", BASE_TYPE_LOI);
    private final String id;

    private EnumTypeLoi(final String id) {
        this.id = id;
    }

    private EnumTypeLoi(final String id, final Map<String, EnumTypeLoi> savedSet) {
        this.id = id;
        if (savedSet != null) {
            savedSet.put(id, this);
        }
    }

    public static Collection<EnumTypeLoi> getListTypeLoi() {
        return BASE_TYPE_LOI.values();
    }

    public static EnumTypeLoi getTypeLoi(final String id) {
        String idToUse = id;
        if (idToUse == null) {
            return null;
        }
        if (idToUse.charAt(0) == 'l') {//pour éviter les problèmes avec les Loi/ loi
            idToUse = StringUtils.capitalize(idToUse);
        }
        final EnumTypeLoi typeLoi = BASE_TYPE_LOI.get(idToUse);
        return typeLoi == null ? createTypeLoi(idToUse) : typeLoi;
    }

    /**
     * @param id l'identifiant
     * @return type de loi non connu
     */
    private static EnumTypeLoi createTypeLoi(final String id) {
        return new EnumTypeLoi(id);
    }

    public boolean isDateAbscisse() {
        return getId().startsWith(PREFIX_LOI_DATE);
    }

    public boolean isLoiZ() {
        return getId().startsWith(PREFIX_LOI_Z);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EnumTypeLoi other = (EnumTypeLoi) obj;
        return ObjectUtils.equals(other.id, id);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getNom() {
        return getId();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "EnumTypeLoi [" + id + "]";
    }
}
