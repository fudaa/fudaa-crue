package org.fudaa.dodico.crue.config.ccm;

import org.apache.commons.lang3.ObjectUtils;

/**
 * @author deniger Definition d'une propriete avec son nom,valeur par defaut,....
 */
public class ItemConstant extends ItemContentAbstract {
    private final Number constantValue;

    protected ItemConstant(final String nom, final Number defaultValue, final PropertyNature nature, final PropertyValidator validator) {
        super(nom, nature, validator);
        this.constantValue = defaultValue;
    }

    public Number getConstantValue() {
        return constantValue;
    }

    /**
     * @return true si la valeur testée est egale à {@link #getConstantValue()}
     */
    @Override
    protected boolean isDefaultOrConstantValue(final Number valueToTest) {
        return ObjectUtils.equals(valueToTest, constantValue);
    }
}
