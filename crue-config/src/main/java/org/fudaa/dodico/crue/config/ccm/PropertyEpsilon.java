package org.fudaa.dodico.crue.config.ccm;

public class PropertyEpsilon {
    public static final PropertyEpsilon DEFAULT = new PropertyEpsilon(1e-15, 1e-15);
    private final double epsilonComparaison;
    private final double epsilonPresentation;

    public PropertyEpsilon(double epsilonComparaison, final double epsilonPresentation) {
        super();
        this.epsilonComparaison = Math.abs(epsilonComparaison);
        this.epsilonPresentation = Math.abs(epsilonPresentation);
    }

    /**
     * @return the epsilon
     */
    public double getEpsilonComparaison() {
        return epsilonComparaison;
    }

    public double getEpsilon(DecimalFormatEpsilonEnum presentation) {
        if (DecimalFormatEpsilonEnum.PRESENTATION.equals(presentation)) {
            return getEpsilonPresentation();
        }
        return getEpsilonComparaison();
    }

    /**
     * @return the epsilonPresentation
     */
    public double getEpsilonPresentation() {
        return epsilonPresentation;
    }

    public boolean isSame(double d1, double d2) {
        return isZero(d1 - d2);
    }

    public boolean isZero(double d1) {
        return d1 >= -epsilonComparaison / 2 && d1 <= epsilonComparaison / 2;
    }
}
