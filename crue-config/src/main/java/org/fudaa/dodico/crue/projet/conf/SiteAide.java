/*
 * To change this template, choose Tools | Templates and open the template in the editor.
 */
package org.fudaa.dodico.crue.projet.conf;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;

/**
 * @author Chris
 */
public class SiteAide implements Cloneable {
 
  private String commentaire;
  private boolean syDocActivation;
  private String cheminBase;
  private String type;
  
  public SiteAide() {
    super();
  }
  
  public SiteAide(boolean syDocActivation, String cheminBase, String type, String commentaire) {
    this.syDocActivation = syDocActivation;
    this.cheminBase = cheminBase ;
    this.commentaire = commentaire;
    this.type = type;
  }

  @Override
  public SiteAide clone() {
    try {
      return (SiteAide) super.clone();
    } catch (CloneNotSupportedException ex) {
      Logger.getLogger(Option.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
  }

    public String getCommentaire() {
        if (StringUtils.isEmpty(commentaire)) {
      commentaire = BusinessMessages.getString("<no comment>");
    }
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public boolean getSyDocActivation() {
        return syDocActivation;
    }

    public void setSyDocActivation(boolean syDocActivation) {
        this.syDocActivation = syDocActivation;
    }

    public String getCheminBase() {
        return cheminBase;
    }

    public void setCheminBase(String cheminBase) {
        this.cheminBase = cheminBase;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
