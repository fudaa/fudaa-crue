/*
 GPL 2
 */
package org.fudaa.dodico.crue.config.loi;

/**
 *
 * @author Frederic Deniger
 */
public interface LoiContrat {
  
  EnumTypeLoi getType();
  
}
