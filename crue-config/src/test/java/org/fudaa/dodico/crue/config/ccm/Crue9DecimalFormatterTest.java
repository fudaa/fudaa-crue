/*
GPL 2
 */
package org.fudaa.dodico.crue.config.ccm;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Frederic Deniger
 */
public class Crue9DecimalFormatterTest {

  public Crue9DecimalFormatterTest() {
  }

  @Test
  public void testFormat() {
    Crue9DecimalFormatter formatter = new Crue9DecimalFormatter(new Crue9DecimalFormat(8, Crue9DecimalFormat.STATE.ACTIVE));
    assertEquals("1.23", formatter.format(1.23));
    assertEquals("5.0E-9", formatter.format(0.000000005));
    assertEquals(".1234568", formatter.format(0.12345678));
    assertEquals("1.2346E8", formatter.format(123456789));
  }

}
