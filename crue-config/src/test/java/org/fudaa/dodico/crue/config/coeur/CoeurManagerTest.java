/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.config.coeur;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * @author CANEL Christophe
 */
public class CoeurManagerTest {
  @Test
  public void testValidator() {
    final Set<String> version = new HashSet<>();
    version.add("1.1");
    version.add("1.1.0");
    version.add("0.1.0");
    version.add("1.2");
    String higherGrammaire = CoeurManager.getHigherGrammaire(version);
    Assert.assertEquals("1.2", higherGrammaire);
    version.add("1.2.1");
    higherGrammaire = CoeurManager.getHigherGrammaire(version);
    Assert.assertEquals("1.2.1", higherGrammaire);
  }
}
