/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.config.cr;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.joda.time.LocalDateTime;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class TestCrueLogReader {

  public TestCrueLogReader() {
  }

  @Test
  public void testReadAndTranslate() {
    final CRReader reader = new CRReader(-1);
    final CrueIOResu<CtuluLog> read = reader.read(TestCrueLogReader.class.getResource("/log/M10-8_c9c10.ccal.log"));
    assertFalse(read.getAnalyse().containsErrorOrSevereError());
    final CrueLogTranslator translator = TestCrueTranslationReaderWriter.read();
    translator.translate(read.getMetier());
    final CtuluLogRecord get = read.getMetier().getRecords().get(2171);
    final String localizedMessage = get.getLocalizedMessage();
    assertEquals("Entrée méthode sur Sc_M201-0_c9c10 : arg1, arg2", localizedMessage);
  }

  @Test
  public void testHigherLevel() {
    final CRReader reader = new CRReader(-1);
    final CrueIOResu<CtuluLogLevel> read = reader.getHigherLevel(TestCrueLogReader.class.getResource("/log/M10-8_c9c10.ccal.log"));
    assertFalse(read.getAnalyse().containsErrorOrSevereError());
    assertEquals(CtuluLogLevel.SEVERE, read.getMetier());
  }

  @Test
  public void testTruncated10lines() {
    final CRReader reader = new CRReader(10);
    final CrueIOResu<CtuluLog> read = reader.read(TestCrueLogReader.class.getResource("/log/M10-8_c9c10.ccal.log"));
    assertEquals(CtuluLogLevel.SEVERE, read.getMetier().getHigherLevel());
    assertEquals(12, read.getMetier().getRecords().size());
    assertEquals(CtuluLogLevel.SEVERE, read.getMetier().getRecords().get(11).getLevel());
  }

  @Test
  public void testRead() {
    final CRReader reader = new CRReader(-1);
    final CrueIOResu<CtuluLog> read = reader.read(TestCrueLogReader.class.getResource("/log/M10-8_c9c10.ccal.log"));
    assertFalse(read.getAnalyse().containsErrorOrSevereError());
    final CtuluLog metier = read.getMetier();
    final List<CtuluLogRecord> records = new ArrayList<>(metier.getRecords());
    assertEquals(2173, records.size());
    CtuluLogRecord record = records.get(2170);
    assertEquals("ID_ENTREE_METHODE_P", record.getId());
    final String argsWithRetourChariot = (String) record.getArgs()[1];
    assertEquals("-1\\na la ligne", argsWithRetourChariot);
    record = records.get(2169);
    assertEquals("72", record.getId());
    final Date logDate = record.getLogDate();
    final LocalDateTime dateTime = LocalDateTime.fromDateFields(logDate);
    assertEquals("2012-04-04T09:37:22.000", DateDurationConverter.dateToXsd(dateTime));
    assertEquals("DEBUG3", record.getLevelDetail());
    assertEquals(CtuluLogLevel.DEBUG, record.getLevel());
    assertEquals("FichierXML.cpp", record.getRessource());
    assertEquals("333", record.getRessourceLine());
    assertEquals("ecrireFichier", record.getRessourceFunction());
    final Object[] args = record.getArgs();
    assertEquals(1, args.length);
    assertEquals("C:\\DATA\\FC\\TestIntegration\\EtuAV_TII\\RUNS\\Sc_AV_REG_Init_PR1_c9c10\\R2012-05-25-08h36m10s\\Etu_AV_TII.etu.xml", args[0]);

    final CtuluLogRecord modified = records.get(2167);//cette ligne a été modifiée pour enlever les 2  dernières colonnes
    assertEquals("ajouterResultatsCalcul", modified.getRessourceFunction());
    assertEquals(CtuluLogLevel.INFO, modified.getLevel());
    final Object[] argsModified = modified.getArgs();
    assertEquals(0, argsModified.length);

    final CtuluLogRecord with2Args = records.get(2166);
    assertEquals("lancerCalculTransitoirePdtFixe", with2Args.getRessourceFunction());
    final Object[] twoArgs = with2Args.getArgs();
    assertEquals(2, twoArgs.length);
    assertEquals("Sc_M10-8_c9c10", twoArgs[0]);
    assertEquals("Cc;\\_T1", twoArgs[1]);//modifie pour tester que le ; n'est pas pris en compte et \ bien supporté.

    record = records.get(2171);
    //test de la dernière ligne
    assertEquals("ID_ENTREE_METHODE_PP", record.getId());
    assertEquals(8, record.getArgs().length);

    record = records.get(2172);
    assertEquals("FichierXML.cpp", record.getRessource());
    assertEquals("157", record.getRessourceLine());
    assertEquals(2, record.getArgs().length);
    assertEquals("", record.getArgs()[0]);
    assertEquals("K:\\CRUE\\CRUEX\\3.Documents projets\\SPF\\Versions_EtuEx\\EtuEx.FCv0.60_crue10v10.0.11\\Runs\\Sc_M3-0_c10\\R2012-09-19-18h06m07s\\NomCliche1.rcli.xml", record.getArgs()[1]);
  }
}
