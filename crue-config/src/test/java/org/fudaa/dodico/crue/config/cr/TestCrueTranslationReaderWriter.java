/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.config.cr;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class TestCrueTranslationReaderWriter {

  public TestCrueTranslationReaderWriter() {
  }

  @Test
  public void testRead() {
    final CrueLogTranslator metier = read();
    assertNotNull(metier);
    assertEquals("fr_FR", metier.locale);
    assertEquals("Fichier {nomEMH} {1} : la version de grammaire du fichier ({2}) n'est pas prise en charge.",
            metier.getTranslation("ID_FICHIERXML_VERSION_GRAMMAIRE"));
    final LienDocumentation lienDocumentation = metier.getLienDocumentation("ID_VARIABLE_HORS_NORMALITE");
    assertEquals("DocumentationSignet", lienDocumentation.getSignet());
    assertEquals("FctCrue10/Dictionnaire Crue.html", lienDocumentation.getHRef());
    final LienMultimedia lienMultimedia = metier.getLienMultimedia("ID_VARIABLE_HORS_NORMALITE");
    assertEquals("MutlimediaSignet", lienMultimedia.getSignet());
    assertEquals("FctCrue10/Mutlimedia.html", lienMultimedia.getHRef());
  }

  @Test
  public void testTranslate() {
    final CrueLogTranslator metier = read();
    final CtuluLog test = new CtuluLog();
    test.addRecord(createRecord((String) null));
    test.addRecord(createRecord("EMH"));
    test.addRecord(createRecord("EMH", "tata"));
    test.addRecord(createRecord("EMH", "tata", "toto"));
    test.addRecord(createRecord("EMH", "tata", "toto", "unused"));
    metier.translate(test);
    final List<CtuluLogRecord> records = test.getRecords();
    assertEquals("Fichier {nomEMH} {1} : la version de grammaire du fichier ({2}) n'est pas prise en charge.",
            records.get(0).getLocalizedMessage());
    assertEquals("Fichier EMH {1} : la version de grammaire du fichier ({2}) n'est pas prise en charge.",
            records.get(1).getLocalizedMessage());
    assertEquals("Fichier EMH tata : la version de grammaire du fichier ({2}) n'est pas prise en charge.",
            records.get(2).getLocalizedMessage());
    assertEquals("Fichier EMH tata : la version de grammaire du fichier (toto) n'est pas prise en charge.",
            records.get(3).getLocalizedMessage());
    assertEquals("Fichier EMH tata : la version de grammaire du fichier (toto) n'est pas prise en charge.",
            records.get(4).getLocalizedMessage());

  }

  @Test
  public void testTranslateVector() {
    final CrueLogTranslator metier = read();
    final CtuluLog test = new CtuluLog();
    test.addRecord(createRecordVector("EMH", "arg1", "arg2", "v1", "v2", "v3"));
    test.addRecord(createRecordVector("EMH", "arg1", "arg2", "v1"));
    test.addRecord(createRecordVector("EMH", "arg1", "arg2"));
    test.addRecord(createRecordVector("EMH", "arg1"));
    metier.translate(test);
    assertEquals("Entrée méthode sur EMH : arg1, arg2",
            test.getRecords().get(0).getLocalizedMessage());
    assertEquals("Entrée méthode sur EMH : arg1, arg2",
            test.getRecords().get(1).getLocalizedMessage());
    assertEquals("Entrée méthode sur EMH : arg1, arg2",
            test.getRecords().get(2).getLocalizedMessage());
    assertEquals("Entrée méthode sur EMH : arg1, {2}",
            test.getRecords().get(3).getLocalizedMessage());
  }

  private CtuluLogRecord createRecord(final String... args) {
    final CtuluLogRecord ctuluLogRecord = new CtuluLogRecord(CtuluLogLevel.DEBUG, "test", "ID_FICHIERXML_VERSION_GRAMMAIRE");
    ctuluLogRecord.setArgs(args);
    return ctuluLogRecord;
  }

  private CtuluLogRecord createRecordVector(final String... args) {
    final CtuluLogRecord ctuluLogRecord = new CtuluLogRecord(CtuluLogLevel.DEBUG, "test", "ID_ENTREE_METHODE_PP");
    ctuluLogRecord.setArgs(args);
    return ctuluLogRecord;
  }

  protected static CrueLogTranslator read() {
    final CrueTranslationReaderWriter reader = new CrueTranslationReaderWriter();
    final CtuluLog log = new CtuluLog();
    final CrueIOResu<CrueLogTranslator> res = reader.readXML("/log/fr_FR.msg.xml", log);
    return res.getMetier();
  }

  public static void main(final String[] args) {
    final String[] splitPreserveAllTokens = StringUtils.split("{-1} essai {{{011} et {1} et {v}{v2}", "{}");
    for (final String string : splitPreserveAllTokens) {
      System.err.println(string);
    }
  }
}
