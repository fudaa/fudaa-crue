/*
 *  License GPL v2
 */
package org.fudaa.dodico.crue.config.ccm;

import org.junit.Test;

import java.util.TreeSet;

import static org.junit.Assert.*;

/**
 *
 * @author Frédéric Deniger
 */
public class DoubleEpsilonComparatorTest {

  public DoubleEpsilonComparatorTest() {
  }

  /**
   * Test of compareSafe method, of class DoubleEpsilonComparator.
   */
  @Test
  public void testCompareSafe() {
    final PropertyEpsilon epsilon =new PropertyEpsilon(1e-3,1e-3);
    final DoubleEpsilonComparator instance = new DoubleEpsilonComparator(epsilon);
    assertEquals(0, instance.compareSafe(0d, 0d));
    assertEquals(0, instance.compareSafe(0d, 0.0001d));
    assertTrue(instance.compareSafe(1d, 0.0001d) > 0);
    assertTrue(instance.compareSafe(0.0001d, 1d) < 0);
  }

  @Test
  public void testInTreeSet() {
    final PropertyEpsilon epsilon =new PropertyEpsilon(1e-3,1e-3);
    final TreeSet<Double> set = new TreeSet<>(new DoubleEpsilonComparator(epsilon));
    set.add(0d);
    assertEquals(1, set.size());
    set.add(0d);
    assertEquals(1, set.size());
    assertTrue(set.contains(0.0001d));
    assertFalse(set.contains(10d));
    set.add(10d);
    assertEquals(2, set.size());
  }
}
