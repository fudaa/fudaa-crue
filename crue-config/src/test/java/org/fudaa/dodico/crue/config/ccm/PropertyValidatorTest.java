/*
 *  License GPL v2
 */
package org.fudaa.dodico.crue.config.ccm;

import org.fudaa.ctulu.CtuluLog;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Frederic Deniger
 */
public class PropertyValidatorTest {
    @Test
    public void testValiditorStrict() {
        final NumberRangeValidator rangeValidate = new NumberRangeValidator(Double.valueOf(0), Double.valueOf(1));
        rangeValidate.setMinStrict(true);
        PropertyValidator propertyValidator = new PropertyValidator(rangeValidate, rangeValidate);
        PropertyTypeNumerique type=new PropertyTypeNumerique("Tnu_Reel","inf",1.0E30);
        propertyValidator.setParamToValid(new ItemVariable("test", "0", new PropertyNature("test", new PropertyEpsilon(1e-3, 1e-3), "", type), propertyValidator));
        final CtuluLog log = new CtuluLog();
        propertyValidator.validateNumber(null, (double) 0, log);
        assertTrue(log.containsErrors());
    }
}
