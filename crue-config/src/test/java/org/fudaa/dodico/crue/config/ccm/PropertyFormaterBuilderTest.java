/*
 GPL 2
 */
package org.fudaa.dodico.crue.config.ccm;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Frederic Deniger
 */
public class PropertyFormaterBuilderTest {
  public PropertyFormaterBuilderTest() {
  }

  @Test
  public void testCreateFormatter() {
    test(1e-2, 3, 1e-3, 3);
    test(1e-3, 4, 1e-3, 3);
    test(1e-5, 6, 1e-5, 5);
    test(1e-10, 11, 1e-22, 22);
    test(2e-2, 3, 2e-3, 3);
    test(3e-10, 11, 3e-22, 22);
  }

  private void test(double comparaison, int comparaisonFractionDigits, double presentation, int presentationFractionDigits) {
    PropertyFormaterBuilder builder = new PropertyFormaterBuilder();
    PropertyEpsilon epsilon = new PropertyEpsilon(comparaison, presentation);
    PropertyNature property = new PropertyNature("test", epsilon, "none", new PropertyTypeNumerique("Tnu_Reel", "", 1e23));
    NumberFormatInfini formatter = (NumberFormatInfini) builder.getFormatter(property, DecimalFormatEpsilonEnum.COMPARISON);
    assertEquals(comparaisonFractionDigits, formatter.getRealBase().getMaximumFractionDigits());
    formatter = (NumberFormatInfini) builder.getFormatter(property, DecimalFormatEpsilonEnum.PRESENTATION);
    assertEquals(presentationFractionDigits, formatter.getRealBase().getMaximumFractionDigits());
  }
}
