/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.config.coeur;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author CANEL Christophe
 */
public class CoeurManagerValidatorTest extends AbstractTestParent {
  @Test
  public void testValidator() {
    final CoeurManagerValidator validator = new CoeurManagerValidator();
    CtuluLog log = validator.validate(this.createValidCoeurManager());
    AbstractTestParent.testAnalyser(log);

    log = validator.validate(this.createCoeurManagerWithEmptyFoldersAndDefautNotUnique());

    Assert.assertTrue(log.containsSevereError());

    final List<CtuluLogRecord> records = new ArrayList<>(log.getRecords());

    Assert.assertEquals(6, records.size());
    Assert.assertEquals("coeur.validator.fileNotFound", records.get(0).getMsg());
    Assert.assertEquals("coeur.validator.directoryNotFound", records.get(1).getMsg());
    Assert.assertEquals("coeur.validator.directoryNotFound", records.get(2).getMsg());
    Assert.assertEquals("coeur.validator.directoryNotFound", records.get(3).getMsg());
    Assert.assertEquals("coeur.validator.messageDirectoryNotFound", records.get(4).getMsg());
    Assert.assertEquals("coeur.validator.defaultNotUnique", records.get(5).getMsg());
  }

  @Test
  public void testValidatorWithNoDefault() {
    final CoeurManager invalidCoeur = createInValidCoeurManagerWithNoDefaultFor1_2();
    final CoeurManagerValidator validator = new CoeurManagerValidator();
    final CtuluLog log = validator.validate(invalidCoeur);
    Assert.assertEquals(1, log.getRecords().size());
  }

  private CoeurManager createValidCoeurManager() {
    final File root = createTempDir();
    final File baseDir = new File(root, "coeurs");
    final File coeur1 = new File(baseDir, "coeur1");
    final File coeur2 = new File(baseDir, "coeur2");

    baseDir.mkdir();

    this.createValidFolderStructure(coeur1);
    this.createValidFolderStructure(coeur2);

    final List<CoeurConfig> coeurs = new ArrayList<>();

    CoeurConfig coeur = new CoeurConfig();
    coeur.setName("Coeur1");
    coeur.setCoeurFolderPath(coeur1.getAbsolutePath());//test with absolute
    coeur.setUsedByDefault(true);
    coeur.setXsdVersion("1.2");

    coeurs.add(coeur);

    coeur = new CoeurConfig();
    coeur.setName("Coeur2");
    coeur.setCoeurFolderPath("coeur2");//test with relative path
    coeur.setUsedByDefault(false);
    coeur.setXsdVersion("1.2");

    coeurs.add(coeur);

    return new CoeurManager(baseDir, coeurs);
  }

  private CoeurManager createInValidCoeurManagerWithNoDefaultFor1_2() {
    final File root = createTempDir();
    final File baseDir = new File(root, "coeurs");
    final File coeur1 = new File(baseDir, "coeur1");
    final File coeur2 = new File(baseDir, "coeur2");

    baseDir.mkdir();

    this.createValidFolderStructure(coeur1);
    this.createValidFolderStructure(coeur2);

    final List<CoeurConfig> coeurs = new ArrayList<>();

    CoeurConfig coeur = new CoeurConfig();
    coeur.setName("Coeur1");
    coeur.setCoeurFolderPath(coeur1.getAbsolutePath());//test with absolute
    coeur.setUsedByDefault(false);
    coeur.setXsdVersion("1.2");

    coeurs.add(coeur);

    coeur = new CoeurConfig();
    coeur.setName("Coeur2");
    coeur.setCoeurFolderPath("coeur2");//test with relative path
    coeur.setUsedByDefault(false);
    coeur.setXsdVersion("1.2");

    coeurs.add(coeur);

    return new CoeurManager(baseDir, coeurs);
  }

  private CoeurManager createCoeurManagerWithEmptyFoldersAndDefautNotUnique() {
    final File root = createTempDir();
    final File baseDir = new File(root, "coeurs");
    final File coeur1 = new File(baseDir, "coeur1");
    final File coeur2 = new File(baseDir, "coeur2");

    baseDir.mkdir();

    this.createValidFolderStructure(coeur1);
    this.createEmptyFolderStructure(coeur2);

    final List<CoeurConfig> coeurs = new ArrayList<>();

    CoeurConfig coeur = new CoeurConfig();
    coeur.setName("Coeur1");
    coeur.setCoeurFolderPath("coeur1");
    coeur.setUsedByDefault(true);
    coeur.setXsdVersion("1.2");

    coeurs.add(coeur);

    coeur = new CoeurConfig();
    coeur.setName("Coeur2");
    coeur.setCoeurFolderPath("coeur2");
    coeur.setUsedByDefault(true);
    coeur.setXsdVersion("1.2");

    coeurs.add(coeur);

    return new CoeurManager(baseDir, coeurs);
  }

  private void createValidFolderStructure(final File folder) {
    folder.mkdir();

    try {
      new File(folder, CoeurManagerValidator.FILE_CRUE_CONFIG_METIER_XML).createNewFile();

      new File(folder, CoeurManagerValidator.FOLDER_EXE).mkdir();
      new File(new File(folder, CoeurManagerValidator.FOLDER_EXE), CoeurManagerValidator.FILE_CRUE_10_EXE).createNewFile();
      new File(folder, CoeurManagerValidator.FOLDER_FICHIERS_VIERGES).mkdir();
      new File(folder, CoeurManagerValidator.FOLDER_XSD).mkdir();
      new File(folder, CoeurManagerValidator.FOLDER_MESSAGES).mkdir();
    } catch (final IOException e) {
      Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
      Assert.fail(e.getMessage());
    }
  }

  private void createEmptyFolderStructure(final File folder) {
    folder.mkdir();
  }
}
