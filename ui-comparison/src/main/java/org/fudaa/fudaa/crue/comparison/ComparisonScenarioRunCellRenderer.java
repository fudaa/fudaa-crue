package org.fudaa.fudaa.crue.comparison;

import java.awt.Color;
import javax.swing.tree.DefaultMutableTreeNode;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;

@SuppressWarnings("serial")
public class ComparisonScenarioRunCellRenderer extends CtuluCellTextRenderer {

  final String tooltip = MessageComparison.getMessage("Comparison.AlreadyLoaded");

  @Override
  protected void setValue(Object value) {
    if (value instanceof DefaultMutableTreeNode) {
      ScenarioRunBean content = (ScenarioRunBean) ((DefaultMutableTreeNode) value).getUserObject();
      if (content != null) {
        if (content.getRun() != null) {
          if (content.getScenario().getRunCourant() == content.getRun()) {
            setIcon(CrueIconsProvider.getIconRunCourant());
          } else {
            setIcon(CrueIconsProvider.getIconRun());
          }
          setText(content.getRun().getNom() + " (" + StringUtils.abbreviate(content.getRun().getInfosVersion().getCommentaire(),180) + ")");
          setToolTipText(content.getRun().getInfosVersion().getCommentaire());
        } else {
          setIcon(CrueIconsProvider.getIcon(EnumCatEMH.SCENARIO));
          setText(content.getScenario().getNom());
        }
        if (content.isSelectable()) {
          setBackground(Color.LIGHT_GRAY);

          setToolTipText(tooltip);
        }
      }
    } else {
      super.setValue(value);
    }
  }
}
