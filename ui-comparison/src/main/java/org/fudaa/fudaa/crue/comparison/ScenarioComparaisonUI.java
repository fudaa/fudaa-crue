package org.fudaa.fudaa.crue.comparison;

import com.memoire.bu.BuBorderLayout;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluTableExportAction;
import org.fudaa.ctulu.gui.PopupMenuReceiver;
import org.fudaa.ctulu.table.CtuluTableModelInterface;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.fudaa.fudaa.crue.common.action.AbstractActionState;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.JXTreeTableExtended;
import org.fudaa.fudaa.crue.common.helper.UiContext;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;

/**
 * Classe qui se charge de charger en mémoire un scénario donné.
 *
 * @author Adrien Hadoux
 */
public class ScenarioComparaisonUI {
  private JXTreeTableExtended table;
  ScenarioComparaisonController controller;

  public JXTreeTableExtended getTable() {
    return table;
  }

  private String getHelpId() {
    return SysdocUrlBuilder.getDialogHelpCtxId("bdlComparaisons", null);
  }

  @SuppressWarnings("serial")
  private static final class DiffCellRenderer extends DefaultTableCellRenderer {
    @Override
    protected void setValue(final Object value) {
      final String txt = ObjectUtils.toString(value);
      setText(txt);
      setToolTipText("");
      if (value != null) {
        final String[] split = StringUtils.split(txt, ';');
        final StringBuilder tltTip = new StringBuilder(txt.length() + 50);
        tltTip.append("<html><body>");
        for (int i = 0; i < split.length; i++) {
          final String string = split[i];
          if (i > 0) {
            tltTip.append(';').append(' ');
          }
          tltTip.append(string);
          if (i % 5 == 0) {
            tltTip.append("<br>");
          }
        }
        setToolTipText(tltTip.toString());
      }
    }
  }

  private class ComparaisonPopupMenuReceiver extends PopupMenuReceiver {
    public ComparaisonPopupMenuReceiver() {
      super();
    }

    @Override
    public void addItems() {
      super.addItems();
      popupMenu.addSeparator();
      popupMenu.add(createMenuItem(ScenarioComparaisonController.PROPERTY_COLLAPSE));
      popupMenu.add(createMenuItem(ScenarioComparaisonController.PROPERTY_EXPAND));
      popupMenu.add(createMenuItem(ScenarioComparaisonController.PROPERTY_CONSERVE_NODE_EXPANDED));
      popupMenu.addSeparator();
      popupMenu.add(createMenuItem(ScenarioComparaisonController.PROPERTY_COMPARE_ON_ID));
      popupMenu.add(createMenuItem(ScenarioComparaisonController.PROPERTY_HIDE_COMMENT));
      popupMenu.add(createMenuItem(ScenarioComparaisonController.PROPERTY_HIDE_ORDER));
    }
  }

  JComponent createMenuItem(String id) {
    return new JMenuItem(controller.getAction(id));
  }

  JComponent createToolButton(String id) {
    return new JButton(controller.getAction(id));
  }

  JComponent createCheckBox(String id) {
    AbstractActionState action = (AbstractActionState) controller.getAction(id);
    AbstractButton buildCheckBox = action.buildCheckBox();
    updateShortLabelIfPresent(id, buildCheckBox);

    return buildCheckBox;
  }

  private AbstractButton updateShortLabelIfPresent(String id, AbstractButton abstractButton) {
    try {
      String shortMessage = MessageComparison.getMessage(id + ".button.label");
      if (shortMessage != null) {
        abstractButton.setToolTipText(abstractButton.getToolTipText());
        abstractButton.setText(shortMessage);
      }
    } catch (MissingResourceException ex) {
      //nothing to do...
    }
    return abstractButton;
  }

  JComponent createButton(String id) {
    return new JButton(controller.getAction(id));
  }

  void displayComparaisonResult(final DefaultMutableTreeTableNode root,
                                ScenarioComparaisonController controller) {
    DialogHelper.showDialogAndTable(createComparaisonResultPanel(root, controller), CtuluDialog.QUIT_OPTION, table, "comparaison.dialog",
        MessageComparison.getMessage("Comparaison.DialogTitle"), DialogHelper.NO_VERSION_FOR_PERSISTENCE);
  }

  CtuluDialogPanel createComparaisonResultPanel(final DefaultMutableTreeTableNode root,
                                                ScenarioComparaisonController controller) {
    this.controller = controller;
    table = createTable(createTreeTableModel(root));
    List<String> comments = new ArrayList<>();
    comments.add(NbBundle.getMessage(ScenarioComparaisonUI.class, "Comparaison.DialogTitle"));
    comments.add(NbBundle.getMessage(ScenarioComparaisonUI.class, "Export.FirstScenario") + " " + controller.getNameCurrentScenario());
    comments.add(NbBundle.getMessage(ScenarioComparaisonUI.class, "Export.SecondScenario") + " " + controller.getNameOtherScenario());
    table.putClientProperty(CtuluTableModelInterface.EXPORT_COMMENT_PROPERTY, comments);
    final CtuluDialogPanel pn = createPanel();
    controller.initTableClientProperty();
    if (controller.tableMustBeSorted()) {
      controller.sortTable();
    }
    controller.autoExpand();
    SysdocUrlBuilder.installHelpShortcut(pn, getHelpId());

    return pn;
  }

  private CtuluDialogPanel createPanel() {
    final CtuluDialogPanel pn = new CtuluDialogPanel(false);
    pn.setLayout(new BuBorderLayout());
    pn.add(new JScrollPane(table));
    final JPanel pnBt = new JPanel(new FlowLayout(FlowLayout.LEFT));
    pnBt.add(createCheckBox(ScenarioComparaisonController.PROPERTY_CONSERVE_NODE_EXPANDED));
    final CtuluTableExportAction ctuluTableExportAction = new CtuluTableExportAction(new UiContext(), table);
    ctuluTableExportAction.setSeparator(';');
    pnBt.add(new JButton(ctuluTableExportAction));

    pn.add(pnBt, BorderLayout.SOUTH);
    final JToolBar tb = new JToolBar();
    tb.add(createToolButton(ScenarioComparaisonController.PROPERTY_COLLAPSE));
    tb.add(createToolButton(ScenarioComparaisonController.PROPERTY_EXPAND));
    tb.addSeparator();
    tb.add(createCheckBox(ScenarioComparaisonController.PROPERTY_COMPARE_ON_ID));
    tb.add(createCheckBox(ScenarioComparaisonController.PROPERTY_HIDE_COMMENT));
    tb.add(createCheckBox(ScenarioComparaisonController.PROPERTY_HIDE_ORDER));
    pn.add(tb, BorderLayout.NORTH);
    return pn;
  }

  @SuppressWarnings("serial")
  private JXTreeTableExtended createTable(final DefaultTreeTableModel modeleJX) {
    final Icon ok = CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/comparison/rond-vert.png");
    final Icon no = CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/comparison/rond-rouge.png");
    final Icon error = CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/comparison/avertissement.png");

    final JXTreeTableExtended treeTable = UiContext.createTreeTable(modeleJX, new ComparaisonPopupMenuReceiver());
    treeTable.setName("comparaison.treeTable");

    treeTable.getColumn(2).setCellRenderer(new DefaultTableCellRenderer() {
      @Override
      protected void setValue(final Object value) {
        ScenarioComparaisonTreeTableNode node = (ScenarioComparaisonTreeTableNode) value;
        setText(StringUtils.EMPTY);
        setIcon(null);
        if (node.isErrorNode()) {
          setText(StringUtils.EMPTY);
          setIcon(error);
          setToolTipText(node.msg);
        } else {
          Boolean res = node.isSame;
          setToolTipText(ObjectUtils.toString(value));
          if (res == Boolean.FALSE) {
            setIcon(no);
          } else {
            setIcon(ok);
          }
        }
      }
    });

    final DefaultTableCellRenderer diffCellRenderer = new DiffCellRenderer();
    treeTable.getColumn(3).setCellRenderer(diffCellRenderer);
    treeTable.getColumn(4).setCellRenderer(diffCellRenderer);
    treeTable.packAll();
    treeTable.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(final MouseEvent e) {
        if (e.getClickCount() == 2) {
          controller.displayInfoOnSelectedLine(treeTable);
        }
      }
    });
    return treeTable;
  }

  private DefaultTreeTableModel createTreeTableModel(final DefaultMutableTreeTableNode root) {
    final DefaultTreeTableModel modeleJX = new DefaultTreeTableModel(root);
    final List<String> colonnes = new ArrayList<>();
    colonnes.add(MessageComparison.getMessage("Comparaison.table.colonne.id"));
    colonnes.add(MessageComparison.getMessage("Comparaison.table.colonne.message"));
    colonnes.add(MessageComparison.getMessage("Comparaison.table.colonne.egal"));
    colonnes.add(MessageComparison.getMessage("Comparaison.table.colonne.courantValue", controller.getNameCurrentScenario()));
    colonnes.add(MessageComparison.getMessage("Comparaison.table.colonne.otherValue", controller.getNameOtherScenario()));
    colonnes.add(MessageComparison.getMessage("Comparaison.table.colonne.nbDiff"));
    modeleJX.setColumnIdentifiers(colonnes);
    return modeleJX;
  }
}
