/*
 GPL 2
 */
package org.fudaa.fudaa.crue.comparison;

import java.io.File;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.PredicateUtils;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.comparaison.ComparaisonSelectorLoader;
import org.fudaa.dodico.crue.comparaison.ExecuteComparaison;
import org.fudaa.dodico.crue.comparaison.ExecuteComparaisonResult;
import org.fudaa.dodico.crue.comparaison.config.ConfComparaisonConteneur;
import org.fudaa.dodico.crue.comparaison.io.ReaderConfig;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.result.OrdResDynamicPropertyFilter;
import org.fudaa.dodico.crue.projet.conf.OptionsEnum;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ComputeComparisonProgress implements ProgressRunnable<List<ExecuteComparaisonResult>> {

  private final File siteDir;
  private final CrueConfigMetier ccm;
  private final ManagerEMHScenario managerReference;
  private final EMHScenario referenceScenario;
  private final ManagerEMHScenario managerCible;
  private final EMHScenario cibleScenario;
  final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

  public ComputeComparisonProgress(File siteDir, CrueConfigMetier ccm, ManagerEMHScenario managerReference, EMHScenario referenceScenario, ManagerEMHScenario managerCible, EMHScenario cibleScenario) {
    this.siteDir = siteDir;
    this.ccm = ccm;
    this.managerReference = managerReference;
    this.referenceScenario = referenceScenario;
    this.managerCible = managerCible;
    this.cibleScenario = cibleScenario;
  }

  @Override
  public List<ExecuteComparaisonResult> run(ProgressHandle handle) {
    final ReaderConfig readerConfig = new ReaderConfig();
    final ConfComparaisonConteneur read = readerConfig.read(ReaderConfig.class.getResource("default-comparaison.xml"));
    final OrdResDynamicPropertyFilter filter = new OrdResDynamicPropertyFilter(referenceScenario.getOrdResScenario(),
            cibleScenario.getOrdResScenario());
    ComparaisonSelectorLoader selectorLoader = new ComparaisonSelectorLoader(siteDir, read);
    CrueOperationResult<Predicate> predicateToUse = selectorLoader.getPredicateToUse(managerReference, managerCible);
    if (predicateToUse.getLogs().containsSomething()) {
      LogsDisplayer.displayError(predicateToUse.getLogs(), NbBundle.getMessage(ScenarioComparaisonLauncher.class, "Comparison.ConfigFilesLoading.Bilan"));
      if (predicateToUse.getLogs().containsFatalError()) {
        return Collections.emptyList();
      }
    }
    Predicate predicate = predicateToUse.getResult();
    if (predicate == null) {
      predicate = PredicateUtils.truePredicate();
    }
    final Integer maxLogItem = configurationManagerService.getOptionsManager().getIntValue(OptionsEnum.MAX_LOG_ITEM, -1);
    final Integer maxDiffOnResultat = configurationManagerService.getOptionsManager().getIntValue(OptionsEnum.MAX_RESULTAT_DIFF_BY_EMH, -1);
    final ExecuteComparaison comp = new ExecuteComparaison(read, ccm, filter, maxLogItem, maxDiffOnResultat);
    final List<ExecuteComparaisonResult> launch = comp.launch(referenceScenario, cibleScenario, predicate);
    Collections.sort(launch);
    return launch;
  }
}
