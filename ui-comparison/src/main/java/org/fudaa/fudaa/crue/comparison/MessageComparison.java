package org.fudaa.fudaa.crue.comparison;

import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class MessageComparison {
  
  public static String getMessage(String code){
    return NbBundle.getMessage(MessageComparison.class, code);
  }
  
  public static String getMessage(String code,String value){
    return NbBundle.getMessage(MessageComparison.class, code,value);
  }
  
  
  
  
}
