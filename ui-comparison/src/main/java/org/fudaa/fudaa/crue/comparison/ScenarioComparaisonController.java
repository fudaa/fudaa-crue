package org.fudaa.fudaa.crue.comparison;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuScrollPane;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.TreePath;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluDefaultLogFormatter;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.crue.comparaison.ExecuteComparaisonResult;
import org.fudaa.dodico.crue.comparaison.tester.ResultatTest;
import org.fudaa.dodico.crue.projet.conf.OptionsEnum;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.fudaa.fudaa.crue.common.UserPreferencesSaver;
import org.fudaa.fudaa.crue.common.action.AbstractActionIcon;
import org.fudaa.fudaa.crue.common.action.AbstractActionState;
import org.fudaa.fudaa.crue.common.helper.JXTreeTableExtended;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.TreeTableNode;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 * Classe qui se charge de charger en mémoire un scénario donné.
 *
 * @author Adrien Hadoux
 */
public class ScenarioComparaisonController {

  public static final CtuluDefaultLogFormatter DEFAULT = new CtuluDefaultLogFormatter(false);
  private String maximumDiffReached;
  private int maximumToDisplay = -1;
  final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

  public String getMaximumDiffReached() {
    if (maximumDiffReached == null) {
      maximumDiffReached = NbBundle.getMessage(ScenarioComparaisonController.class, "maximumDiffReached");
    }
    return maximumDiffReached;
  }

  public int getMaxNumberToDisplay() {
    if (maximumToDisplay < 0) {
      Integer integerValue = configurationManagerService.getOptionsManager().getIntegerValue(OptionsEnum.MAX_COMPARAISON_ITEM);
      if (integerValue != null) {
        maximumToDisplay = integerValue.intValue();
      } else {
        maximumToDisplay = 1000;
      }
    }
    return maximumToDisplay;
  }

  @SuppressWarnings("serial")
  private class ConfigureAction extends AbstractActionState {

    private final String id;

    private ConfigureAction(String id) {
      super(MessageComparison.getMessage(id), null, id);
      this.id = id;
      setSelected(getPreferences(id));
    }

    @Override
    public void changeAction() {
      ui.getTable().putClientProperty(id, Boolean.valueOf(isSelected()));
      UserPreferencesSaver.savePreference(id, isSelected());
      doChange();
    }

    protected void doChange() {
      sortTable();
    }
  }

  /**
   * @author deniger
   */
  public static class ComparatorOkNotOk implements Comparator<ScenarioComparaisonTreeTableNode> {

    @Override
    public int compare(final ScenarioComparaisonTreeTableNode o1, final ScenarioComparaisonTreeTableNode o2) {
      if (o1 == o2) {
        return 0;
      }
      if (o1 == null) {
        return -1;
      }
      if (o2 == null) {
        return 1;
      }
      if (!o1.isSame.equals(o2.isSame)) {
        if (o1.isSame.booleanValue()) {
          return 1;
        }
        return -1;
      }
      return o1.compareTo(o2);
    }
  }
  
  protected static final String PROPERTY_COLLAPSE = "Comparaison.table.collapseAll";
  protected static final String PROPERTY_EXPAND = "Comparaison.table.expandAll";
  protected static final String PROPERTY_COMPARE_ON_ID = "Comparaison.sortOnId";
  protected static final String PROPERTY_CONSERVE_NODE_EXPANDED = "Comparaison.conserveNodesExpanded";
  protected static final String PROPERTY_HIDE_COMMENT = "Comparaison.hideCommentDifference";
  protected static final String PROPERTY_HIDE_ORDER = "Comparaison.hideOrderDifference";
  protected static final String PROPERTY_INIT_RESULT = "Comparaison.initResultat";
  final Map<String, AbstractAction> actions = new HashMap<>();
  private final Comparator<ScenarioComparaisonTreeTableNode> ComparatorOkNotOk = new ComparatorOkNotOk();
  private final String nameOtherScenario;
  private final String nameCurrentScenario;
  private final ScenarioComparaisonUI ui;

  public ScenarioComparaisonUI getUi() {
    return ui;
  }
  private boolean containsDifferences;

  /**
   * @param nameCurrentScenario nom du scenario courant
   * @param nameOtherScenario nom de l'autre scenario
   */
  public ScenarioComparaisonController(final String nameCurrentScenario, final String nameOtherScenario) {
    super();
    this.nameOtherScenario = nameOtherScenario;
    this.nameCurrentScenario = nameCurrentScenario;
    ui = new ScenarioComparaisonUI();
  }

  private void addNode(final ResultatTest resTest, final ScenarioComparaisonTreeTableNode parent) {
    if (resTest == null) {
      return;
    }
    if (resTest.isSame()) {
      return;
    }
    final ScenarioComparaisonTreeTableNode node = ScenarioComparaisonTreeTableNode.createNode(resTest);
    parent.add(node);
    final List<ResultatTest> fils = resTest.getFils();
    int maxNumber = getMaxNumberToDisplay();
    int max = Math.min(fils.size(), maxNumber);
    for (int i = 0; i < max; i++) {
      ResultatTest resultatTest = fils.get(i);
      addNode(resultatTest, node);
    }
    if (max < fils.size()) {
      ResultatTest maxReached = new ResultatTest(StringUtils.EMPTY, StringUtils.EMPTY, getMaximumDiffReached());
      maxReached.setSame(false);
      node.add(ScenarioComparaisonTreeTableNode.createNode(maxReached));
    }
  }

  void autoExpand() {
    if (UserPreferencesSaver.getPreference(PROPERTY_CONSERVE_NODE_EXPANDED, false)) {
      ui.getTable().expandAll();
    }
  }

  private Map<String, AbstractAction> buildActionsIfNeeded() {
    if (actions.isEmpty()) {
      AbstractAction sortOnIdAction = createSortOnIdAction();
      AbstractAction hideComment = createHideComment();
      AbstractAction hideOrder = createHideOrder();
      actions.put(PROPERTY_COMPARE_ON_ID, sortOnIdAction);
      actions.put(PROPERTY_HIDE_COMMENT, hideComment);
      actions.put(PROPERTY_HIDE_ORDER, hideOrder);
      actions.put(PROPERTY_CONSERVE_NODE_EXPANDED, createConserveTreeExpandedAction());
      actions.put(PROPERTY_COLLAPSE, createCollapseAction());
      actions.put(PROPERTY_EXPAND, createExpandAction());
      sortOnIdAction.setEnabled(containsDifferences);
      hideComment.setEnabled(containsDifferences);
      hideOrder.setEnabled(containsDifferences);
    }
    return actions;
  }

  private JPanel buildPnCompareFor(final String senar, final ScenarioComparaisonTreeTableNode lastPathComponent) {
    final BuGridLayout bg = new BuGridLayout(2);
    final JPanel pnCompare = new JPanel(bg);
    pnCompare.add(new JLabel("Valeur Scénario courant"));
    pnCompare.add(new JLabel("Valeur Scénario " + senar));
    pnCompare.add(new BuScrollPane(createTextArea(lastPathComponent.getValueA())));
    pnCompare.add(new BuScrollPane(createTextArea(lastPathComponent.getValueB())));
    return pnCompare;
  }

  @SuppressWarnings("serial")
  private AbstractAction createCollapseAction() {
    return new AbstractActionIcon(MessageComparison.getMessage(PROPERTY_COLLAPSE), CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/comparison/arrow-in.png"),
            PROPERTY_COLLAPSE) {
      @Override
      public void actionPerformed(final ActionEvent e) {
        ui.getTable().collapseAll();
      }
    };
  }

  @SuppressWarnings("serial")
  private AbstractAction createConserveTreeExpandedAction() {
    AbstractAction ebliActionChangeState = new ConfigureAction(PROPERTY_CONSERVE_NODE_EXPANDED) {
      @Override
      public void doChange() {
        if (isSelected()) {
          ui.getTable().expandAll();
        }
      }
    };
    return ebliActionChangeState;
  }

  @SuppressWarnings("serial")
  private AbstractAction createExpandAction() {
    return new AbstractActionIcon(MessageComparison.getMessage(PROPERTY_EXPAND), CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/comparison/arrow-out.png"),
            PROPERTY_EXPAND) {
      @Override
      public void actionPerformed(final ActionEvent e) {
        ui.getTable().expandAll();
      }
    };
  }

  private AbstractAction createHideComment() {
    return new ConfigureAction(PROPERTY_HIDE_COMMENT);
  }

  private AbstractAction createHideOrder() {
    return new ConfigureAction(PROPERTY_HIDE_ORDER);
  }

  private AbstractAction createSortOnIdAction() {
    return new ConfigureAction(PROPERTY_COMPARE_ON_ID);
  }

  private JTextArea createTextArea(final String text) {
    final JTextArea textArea = new JTextArea(text);
    textArea.setColumns(30);
    return textArea;
  }

  private DefaultMutableTreeTableNode createRootNode(final List<ExecuteComparaisonResult> launch) {

    final DefaultMutableTreeTableNode root = new DefaultMutableTreeTableNode();
    final List<ScenarioComparaisonTreeTableNode> allNodes = new ArrayList<>();
    containsDifferences = false;
    for (final ExecuteComparaisonResult cmpRes : launch) {

      final ScenarioComparaisonTreeTableNode node = new ScenarioComparaisonTreeTableNode();
      node.id = cmpRes.getId();
      node.msg = cmpRes.getMsg();
      node.setIsOrderDiff(cmpRes.isDiffOnOrder());
      node.nbObjectTested = cmpRes.getNbObjectTested();
      node.nbDifferences = cmpRes.getNbDifferences();
      CtuluLog ctuluLog = cmpRes.getLog();
      if ((ctuluLog != null) && ctuluLog.containsErrorOrSevereError()) {
        for (final CtuluLogRecord rec : ctuluLog.getRecords()) {
          final ScenarioComparaisonTreeTableNode nd = new ScenarioComparaisonTreeTableNode();
          nd.setErrorNode(true);
          nd.msg = DEFAULT.formatMessage(rec, ctuluLog.getDefaultResourceBundle());
          node.add(nd);
        }

      } else {
        final boolean contains = cmpRes.getRes() != null && CollectionUtils.isNotEmpty(cmpRes.getRes().getFils());
        if (!contains) {
          node.isSame = Boolean.TRUE;
        } else {
          node.isSame = cmpRes.getRes().isSame();
          for (final ResultatTest test : cmpRes.getRes().getFils()) {
            addNode(test, node);
          }
        }
      }
      if (!containsDifferences && Boolean.FALSE.equals(node.isSame)) {
        containsDifferences = true;
      }
      allNodes.add(node);
    }
    Collections.sort(allNodes, this.ComparatorOkNotOk);
    for (final ScenarioComparaisonTreeTableNode compareTreeTableNode : allNodes) {
      root.add(compareTreeTableNode);
    }

    return root;
  }

  protected void display(final List<ExecuteComparaisonResult> launch) {
    ui.displayComparaisonResult(createRootNode(launch), this);
  }

  public JPanel createView(final List<ExecuteComparaisonResult> launch) {
    return ui.createComparaisonResultPanel(createRootNode(launch), this);
  }

  AbstractAction getAction(String id) {
    return buildActionsIfNeeded().get(id);
  }

  protected boolean tableMustBeSorted() {
    return UserPreferencesSaver.getPreference(PROPERTY_COMPARE_ON_ID, false)
            || UserPreferencesSaver.getPreference(PROPERTY_HIDE_COMMENT, false)
            || UserPreferencesSaver.getPreference(PROPERTY_HIDE_ORDER, false);
  }

  protected void initTableClientProperty() {
    ui.getTable().putClientProperty(PROPERTY_COMPARE_ON_ID, getPreferences(PROPERTY_COMPARE_ON_ID));
    ui.getTable().putClientProperty(PROPERTY_HIDE_COMMENT, getPreferences(PROPERTY_HIDE_COMMENT));
    ui.getTable().putClientProperty(PROPERTY_HIDE_ORDER, getPreferences(PROPERTY_HIDE_ORDER));
  }

  private Boolean getPreferences(String id) {
    return Boolean.valueOf(UserPreferencesSaver.getPreference(id, false));
  }

  void displayInfoOnSelectedLine(final JXTreeTable table) {
    final TreePath selectionPath = table.getTreeSelectionModel().getSelectionPath();
    if (selectionPath == null) {
      return;
    }
    final ScenarioComparaisonTreeTableNode lastPathComponent = (ScenarioComparaisonTreeTableNode) selectionPath.getLastPathComponent();
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuBorderLayout());

    TreeTableNode parent = lastPathComponent.getParent();
    String parentTitle = "";
    while (parent instanceof ScenarioComparaisonTreeTableNode) {
      final String msg = ((ScenarioComparaisonTreeTableNode) parent).getMsg();
      if (StringUtils.isNotBlank(msg)) {
        parentTitle = msg;
      }
      parent = parent.getParent();
    }
    final JLabel comp = new JLabel("<html><body>"
            + (StringUtils.isNotBlank(parentTitle) ? (parentTitle + "<br><br>") : "") + lastPathComponent.getMsg());
    final Font deriveFont = BuLib.deriveFont(comp.getFont(), Font.BOLD, 1);
    comp.setFont(deriveFont);
    pn.add(comp, BorderLayout.NORTH);
    final boolean isSame = ObjectUtils.equals(lastPathComponent.getValueA(), lastPathComponent.getValueB());
    if (!lastPathComponent.isSame && lastPathComponent.getValueA() != null && isSame) {
      comp.setText(comp.getText() + " " + lastPathComponent.getValueA());
    }
    if (!isSame) {
      final JPanel pnCompare = buildPnCompareFor(nameOtherScenario, lastPathComponent);
      pn.add(pnCompare);
    } else if (!lastPathComponent.isSame && lastPathComponent.getChildCount() == 1) {

      final ScenarioComparaisonTreeTableNode childAt = (ScenarioComparaisonTreeTableNode) lastPathComponent.getChildAt(0);
      comp.setText(comp.getText() + "<br> " + childAt.getMsg());
      final JPanel pnCompare = buildPnCompareFor(nameOtherScenario, childAt);
      pn.add(pnCompare);

    }
    pn.setErrorTextUnable();
    pn.afficheModale(CtuluLibSwing.getFrameAncestor(table), CtuluDialog.OK_OPTION);

  }

  @SuppressWarnings("unchecked")
  private List<ScenarioComparaisonTreeTableNode> getInitNodes(final JXTreeTableExtended table) {
    List<ScenarioComparaisonTreeTableNode> initNodes = (List<ScenarioComparaisonTreeTableNode>) table.getClientProperty(PROPERTY_INIT_RESULT);
    if (initNodes == null) {
      final DefaultMutableTreeTableNode initRoot = (DefaultMutableTreeTableNode) table.getTreeTableModel().getRoot();
      final int childCount = initRoot.getChildCount();
      initNodes = new ArrayList<>(childCount);
      for (int i = 0; i < childCount; i++) {
        initNodes.add((ScenarioComparaisonTreeTableNode) initRoot.getChildAt(i));
      }
      table.putClientProperty(PROPERTY_INIT_RESULT, initNodes);
    }
    return initNodes;
  }

  void sortTable() {
    JXTreeTableExtended table = ui.getTable();
    final List<ScenarioComparaisonTreeTableNode> initNodes = getInitNodes(table);
    final boolean hideComment = table.getClientProperty(PROPERTY_HIDE_COMMENT) == Boolean.TRUE;
    final boolean hideOrder = table.getClientProperty(PROPERTY_HIDE_ORDER) == Boolean.TRUE;
    final int count = initNodes.size();
    final List<ScenarioComparaisonTreeTableNode> all = new ArrayList<>(count);

    for (final ScenarioComparaisonTreeTableNode tableNode : initNodes) {
      if (!isIgnored(hideOrder, tableNode)) {

        if (hideComment) {
          if (!tableNode.isCommentNode()) {
            all.add(tableNode.getNodeWithoutCommentResult());
          }
        } else {
          all.add(tableNode);
        }
      }
    }
    if (table.getClientProperty(PROPERTY_COMPARE_ON_ID) == Boolean.TRUE) {

      Collections.sort(all);
    } else {
      Collections.sort(all, this.ComparatorOkNotOk);
    }
    final DefaultMutableTreeTableNode root = new DefaultMutableTreeTableNode();
    for (final ScenarioComparaisonTreeTableNode compareTreeTableNode : all) {
      root.add(compareTreeTableNode);
    }
    final List<TableCellRenderer> columns = new ArrayList<>();
    final int nbCol = table.getColumnCount();
    for (int i = 0; i < nbCol; i++) {
      columns.add(table.getColumn(i).getCellRenderer());
    }
    table.getTreeTableModel().setRoot(root);
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        for (int i = 0; i < nbCol; i++) {
          ui.getTable().getColumn(i).setCellRenderer(columns.get(i));
        }
        ui.getTable().packAll();
        autoExpand();
      }
    });

  }

  private boolean isIgnored(final boolean hideOrder, final ScenarioComparaisonTreeTableNode tableNode) {
    return hideOrder && tableNode.isOrderNode();
  }

  /**
   * @return the nameOtherScenario
   */
  public String getNameOtherScenario() {
    return nameOtherScenario;
  }

  /**
   * @return the nameOtherScenario
   */
  public String getNameCurrentScenario() {
    return nameCurrentScenario;
  }
}
