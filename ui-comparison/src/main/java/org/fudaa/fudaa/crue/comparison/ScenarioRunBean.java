/**
 *
 */
package org.fudaa.fudaa.crue.comparison;

import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

/**
 * @author deniger
 */
public class ScenarioRunBean {
    private final ManagerEMHScenario scenario;
    private final EMHRun run;
    private final boolean isSelectable;

    /**
     * @param scenario   le scenraio
     * @param run        le run
     * @param isSelected true si selectionne
     */
    public ScenarioRunBean(ManagerEMHScenario scenario, EMHRun run, boolean isSelected) {
        super();
        this.scenario = scenario;
        this.run = run;
        this.isSelectable = isSelected;
    }

    /**
     * @return the scenario
     */
    public ManagerEMHScenario getScenario() {
        return scenario;
    }

    /**
     * @return the run
     */
    public EMHRun getRun() {
        return run;
    }

    /**
     * @return the isSelected
     */
    public boolean isSelectable() {
        return isSelectable;
    }
}
