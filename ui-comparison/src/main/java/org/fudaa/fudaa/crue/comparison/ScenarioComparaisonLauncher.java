package org.fudaa.fudaa.crue.comparison;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuPanel;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.comparaison.ExecuteComparaisonResult;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.loader.LoaderService;
import org.jdesktop.swingx.JXTree;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;

/**
 * Classe qui se charge de charger en memoire un scénario donné.
 *
 * @author Adrien Hadoux
 */
public class ScenarioComparaisonLauncher {
    private final EMHProjet projetCible;
    private final ManagerEMHScenario managerReference;
    private final EMHScenario reference;
    private final EMHRun runReference;
    private final boolean projetCibleIsSame;
    private final CrueConfigMetier crueConfigMetierRef;
    private final LoaderService loaderService = Lookup.getDefault().lookup(LoaderService.class);

    /**
     * @param managerReference  la mamager de référence
     * @param reference         le scenario de reference
     * @param crueConfigMetier  le CCM
     * @param runReference      le run de ref
     * @param projectCible      le projet cible
     * @param projetCibleIsSame true si meme projet poure reference et cible
     */
    public ScenarioComparaisonLauncher(ManagerEMHScenario managerReference, EMHScenario reference, CrueConfigMetier crueConfigMetier, EMHRun runReference,
                                       final EMHProjet projectCible, final boolean projetCibleIsSame) {
        super();
        this.crueConfigMetierRef = crueConfigMetier;
        this.projetCible = projectCible;
        this.managerReference = managerReference;
        this.runReference = runReference;
        this.reference = reference;
        this.projetCibleIsSame = projetCibleIsSame;
    }

    private String getScenarioReferenceName() {
        return getReferenceName(reference);
    }

    private String getRunReferenceName() {
        return getReferenceName(runReference);
    }

    private String getReferenceName(ObjetNomme objetNomme) {
        if (projetCibleIsSame) {
            return objetNomme == null ? null : objetNomme.getNom();
        }
        return null;
    }

    /**
     * Realise le calcul.
     */
    @SuppressWarnings("serial")
    public void compute() {
        if (reference == null) {
            DialogHelper.showError(MessageComparison.getMessage("Comparison.NoReferenceScenario"));
            return;
        }
        final List<ManagerEMHScenario> listeScenarios = new ArrayList<>(projetCible.getListeScenarios());
        final JXTree tree = CrueScenarioBuilder.buildListScenarioAndRun(listeScenarios, getScenarioReferenceName(),
                getRunReferenceName());
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        tree.expandAll();
        final CtuluDialogPanel pn = new CtuluDialogPanel(false) {
            @Override
            public boolean isDataValid() {
                return !tree.getSelectionModel().isSelectionEmpty();
            }
        };
        SysdocUrlBuilder.installHelpShortcut(pn, SysdocUrlBuilder.getDialogHelpCtxId("comparerScenario", PerspectiveEnum.POST));
        pn.setLayout(new BuBorderLayout(15, 3));
        pn.add(new JScrollPane(tree));
        tree.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                ScenarioRunBean value = null;
                if (tree.getSelectionPath() != null) {
                    value = (ScenarioRunBean) ((DefaultMutableTreeNode) tree.getSelectionPath().getLastPathComponent()).getUserObject();
                }
                if (value != null && value.isSelectable()) {
                    tree.clearSelection();
                }
                if (tree.isSelectionEmpty()) {
                    pn.setErrorText(MessageComparison.getMessage("Comparison.ScenarioChooserNoSelection"));
                } else {
                    pn.setErrorText(null);
                }
            }
        });
        final BuPanel fileToLoad = new BuPanel(new BuBorderLayout());
        final BuCheckBox cb = new BuCheckBox("Charger un fichier de comparaison");
        fileToLoad.add(cb, BuBorderLayout.WEST);
        final CtuluFileChooserPanel fileChooserPanel = pn.addFileChooserPanel(fileToLoad,
                "Charger un fichier de comparaison", false, false);
        CtuluLibSwing.setEnable(fileChooserPanel, false);
        cb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                CtuluLibSwing.setEnable(fileChooserPanel, cb.isSelected());
            }
        });
        if (pn.afficheModaleOk(WindowManager.getDefault().getMainWindow(), MessageComparison.getMessage("Comparison.LaunchDialogTitle",
                reference.getNom()))) {
            ScenarioRunBean value = (ScenarioRunBean) ((DefaultMutableTreeNode) tree.getSelectionPath().getLastPathComponent()).getUserObject();
            // on cree une nouvelle instance pour éviter les collisions avec les scenarios actuellement chargés dans
            // le projet:
            final ManagerEMHScenario scenarioToCompare = value.getScenario();
            final EMHRun runSelectionne = value.getRun();
            launchComparison(scenarioToCompare, runSelectionne);
        }
    }

    private void launchComparison(final ManagerEMHScenario managerCible, final EMHRun runSelectionne) {
        final EMHScenario cibleScenario = loaderService.load(projetCible, managerCible, runSelectionne);
        launchComparison(cibleScenario, managerCible, runSelectionne);
    }

    public boolean launchComparison(final EMHScenario cibleScenario, final ManagerEMHScenario managerCible,
                                    final EMHRun runSelectionne) throws MissingResourceException {
        if (cibleScenario == null) {
            return true;
        }
        ComputeComparisonProgress compareExecutor = new ComputeComparisonProgress(loaderService.getSiteDir(),
                crueConfigMetierRef,
                managerReference, reference,
                managerCible, cibleScenario);
        String otherScenario = runSelectionne == null ? cibleScenario.getNom() : cibleScenario.getNom() + " / "
                + runSelectionne.getNom();
        String currentScenario = runReference == null ? reference.getNom() : reference.getNom() + " / "
                + runReference.getNom();
        final List<ExecuteComparaisonResult> launch = CrueProgressUtils.showProgressDialogAndRun(compareExecutor,
                NbBundle.getMessage(ScenarioComparaisonLauncher.class, "ComparaisonAtionDescription", currentScenario, otherScenario));
        new ScenarioComparaisonController(currentScenario, otherScenario).display(launch);
        return false;
    }
}
