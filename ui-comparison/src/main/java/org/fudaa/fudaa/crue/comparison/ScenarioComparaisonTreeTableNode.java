/**
 *
 */
package org.fudaa.fudaa.crue.comparison;

import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.dodico.crue.comparaison.tester.ResultatTest;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;

public class ScenarioComparaisonTreeTableNode extends DefaultMutableTreeTableNode implements
        Comparable<ScenarioComparaisonTreeTableNode> {

  @Override
  public boolean isEditable(final int column) {
    return false;
  }

  @Override
  public int compareTo(final ScenarioComparaisonTreeTableNode o) {
    return id.compareTo(o.id);
  }
  protected String id;
  protected String msg;
  private Object a;
  private String valueA;
  private String valueB;
  int nbObjectTested = -1;
  Boolean isSame = Boolean.FALSE;
  private String propertyTested;
  private boolean isErrorNode;
  private boolean isOrderDiff;
  int nbDifferences;

  void setIsOrderDiff(boolean isOrderDiff) {
    this.isOrderDiff = isOrderDiff;
  }

  String getValueA() {
    return valueA;
  }

  String getValueB() {
    return valueB;
  }

  public String getMsg() {
    return msg;
  }
  
  
  
  boolean isCommentNode() {
    return (id != null && id.startsWith("COM")) || (propertyTested != null && propertyTested.contains("commentaire"));
  }

  boolean isOrderNode() {
    if (isOrderDiff) {
      return true;
    }
    final int count = getChildCount();
    for (int i = 0; i < count; i++) {
      if (((ScenarioComparaisonTreeTableNode) getChildAt(i)).isOrderNode()) {
        return true;
      }
    }
    return false;
  }

  private ScenarioComparaisonTreeTableNode cloneNode() {
    final ScenarioComparaisonTreeTableNode res = new ScenarioComparaisonTreeTableNode();
    res.id = id;
    res.isOrderDiff = isOrderDiff;
    res.msg = msg;
    res.a = a;
    res.valueA = valueA;
    res.valueB = valueB;
    res.nbObjectTested = nbObjectTested;
    res.nbDifferences = nbDifferences;
    res.isSame = isSame;
    res.propertyTested = propertyTested;
    res.isErrorNode = isErrorNode;
    return res;
  }

  ScenarioComparaisonTreeTableNode getNodeWithoutCommentResult() {
    final ScenarioComparaisonTreeTableNode res = cloneNode();
    final int childCount = getChildCount();
    for (int i = 0; i < childCount; i++) {
      final ScenarioComparaisonTreeTableNode child = (ScenarioComparaisonTreeTableNode) getChildAt(i);
      if (!child.isCommentNode()) {
        final boolean wasAlreadyALeaf = child.getChildCount() == 0;
        final ScenarioComparaisonTreeTableNode withoutCommentResult = child.getNodeWithoutCommentResult();
        final boolean isALeaf = withoutCommentResult.getChildCount() > 0;
        if (wasAlreadyALeaf || isALeaf) {
          res.add(withoutCommentResult);
        }
      }
    }
    // si on a supprime des differences filles et que le nouveau total est zero, on
    // met à jour le status ok
    if (getChildCount() > 0 && res.getChildCount() == 0) {
      res.isSame = Boolean.TRUE;
    }
    return res;
  }


  @Override
  public Object getValueAt(final int column) {
    if (column == 0) {
      return id;
    }
    if (column == 1) {
      return msg;
    }
    if (column == 2) {
      return this;
    }
    if (column == 3) {
      return valueA;
    }
    if (column == 4) {
      return valueB;
    }
    return nbDifferences;
  }

  @Override
  public int getColumnCount() {
    return 6;
  }

  /**
   * @return the id
   */
  public String getId() {
    return id;
  }

  protected static ScenarioComparaisonTreeTableNode createNode(final ResultatTest resTest) {
    final ScenarioComparaisonTreeTableNode node = new ScenarioComparaisonTreeTableNode();
    node.msg = resTest.getTraductedMsg();
    node.isOrderDiff = resTest.isOrderDiff();
    node.propertyTested = resTest.getPropertyTested();
    node.a = resTest.getObjetA();
    node.valueA = ObjectUtils.toString(resTest.getPrintA(), ObjectUtils.toString(resTest.getObjetA(), "null"));
    node.valueB = ObjectUtils.toString(resTest.getPrintB(), ObjectUtils.toString(resTest.getObjetB(), "null"));
    node.nbObjectTested = resTest.getNbObjectTested();
    node.isSame = resTest.isSame();
    node.nbDifferences=resTest.getNbDifferences();
    return node;
  }

  /**
   * @return the isErrorNode
   */
  boolean isErrorNode() {
    return isErrorNode;
  }

  /**
   * @param isErrorNode the isErrorNode to set
   */
  void setErrorNode(final boolean isErrorNode) {
    this.isErrorNode = isErrorNode;
  }
}
