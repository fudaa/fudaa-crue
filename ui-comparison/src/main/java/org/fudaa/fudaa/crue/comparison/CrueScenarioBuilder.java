package org.fudaa.fudaa.crue.comparison;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.helper.UiContext;
import org.jdesktop.swingx.JXTree;

import javax.swing.tree.DefaultMutableTreeNode;
import java.util.List;

/**
 * Gere les interfaces relative a un scenario Crue. Gere le lien entre un EMHScenario metier et l'interface propose par
 * les actions scenarios.
 *
 * @author Adrien Hadoux
 */
public class CrueScenarioBuilder {
    /**
     * Cree la liste avec ou non bordure.
     *
     * @param baseScenarios     les scenarios
     * @param scenarioReference les scenario de reference
     * @param runReference      le run de reference
     * @return l'arbre
     */
    public static JXTree buildListScenarioAndRun(final List<ManagerEMHScenario> baseScenarios,
                                                 String scenarioReference, String runReference) {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode();
        if (baseScenarios != null) {

            for (final ManagerEMHScenario managerScenario : baseScenarios) {
                boolean used = false;
                if (StringUtils.equals(scenarioReference, managerScenario.getNom()) && runReference == null) {
                    used = true;
                }
                DefaultMutableTreeNode scenarioNode = new DefaultMutableTreeNode(new ScenarioRunBean(managerScenario, null, used));
                root.add(scenarioNode);
                List<EMHRun> listeRuns = managerScenario.getListeRuns();
                if (CollectionUtils.isNotEmpty(listeRuns)) {
                    for (EMHRun emhRun : listeRuns) {
                        used = false;
                        if (StringUtils.equals(scenarioReference, managerScenario.getNom())
                                && StringUtils.equals(runReference, emhRun.getNom())) {
                            used = true;
                        }
                        scenarioNode.add(new DefaultMutableTreeNode(new ScenarioRunBean(managerScenario, emhRun, used)));
                    }
                }
            }
        }
        final JXTree tree = UiContext.createTree(root);
        tree.setRootVisible(false);
        tree.setCellRenderer(new ComparisonScenarioRunCellRenderer());

        return tree;
    }
}
