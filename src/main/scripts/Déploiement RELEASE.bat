ECHO ON

:: Path de la version actuelle
SET NewReleasePath=fudaacrue
SET OldReleasePath=P:\FudaaCrue

:: Sauvegarde de la prod actuelle
xcopy /E %OldReleasePath%\bin .\FudaaCrue.old\bin\
move .\FudaaCrue.old\bin\fudaacrue.old.exe .\FudaaCrue.old\bin\fudaacrue.exe
xcopy /E %OldReleasePath%\fudaacrue .\FudaaCrue.old\fudaacrue\
xcopy /E %OldReleasePath%\platform .\FudaaCrue.old\platform\
xcopy %OldReleasePath%\etc\fudaacrue.clusters .\FudaaCrue.old\etc\

pause

:: Suppression des répertoires java
rmdir /S /Q %OldReleasePath%\fudaacrue
rmdir /S /Q %OldReleasePath%\platform

:: Suppression de l'exe renommé
del %OldReleasePath%\bin\fudaacrue.old.exe

pause

:: Répertoire bin
SET Env="bin"
xcopy /E /Y %NewReleasePath%\%Env%\fudaacrue.exe %OldReleasePath%\%Env%\

:: Répertoire etc
SET Env="etc"
xcopy /E /Y %NewReleasePath%\%Env%\fudaacrue.clusters %OldReleasePath%\%Env%\

:: Répertoire fudaacrue
SET Env="fudaacrue"
xcopy /E %NewReleasePath%\%Env% %OldReleasePath%\%Env%\

:: Répertoire platform
SET Env="platform"
xcopy /E %NewReleasePath%\%Env% %OldReleasePath%\%Env%\

:: répertoire historique depuis Q
xcopy /E /Y Q:\Qualif_Exec\FudaaCrue\etc\historique P:\FudaaCrue\etc\historique\

:: Doc en ligne
xcopy /E /Y Q:\Qualif_Exec\FudaaCrue\etc\aide\fr_FR\SyDoC_HTML P:\FudaaCrue\etc\aide\fr_FR\SyDoC_HTML\

pause