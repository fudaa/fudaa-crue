ECHO ON

:: Path de la version actuelle
SET OldQualifPath=Q:\Qualif_Exec\FCvX.X
:: Path de g�n�ration de la version SNAPSHOT
SET NewQualifPath=C:\Projets\FudaaCrue\src\trunk\soft\fudaa-crue\ui-application\target\fudaacrue

:: Cr�ation du r�pertoire de sauvegarde
rmdir /S /Q %OldQualifPath%.old
mkdir %OldQualifPath%.old

:: Sauvegarde de la qualif actuelle
xcopy /E %OldQualifPath%\bin %OldQualifPath%.old\bin\
xcopy /E %OldQualifPath%\fudaacrue %OldQualifPath%.old\fudaacrue\
xcopy /E %OldQualifPath%\platform %OldQualifPath%.old\platform\
xcopy %OldQualifPath%\etc\fudaacrue.clusters %OldQualifPath%.old\etc\

:: Suppression des r�pertoires java
rmdir /S /Q %OldQualifPath%\fudaacrue
rmdir /S /Q %OldQualifPath%\platform

:: Path Source � renseigner pour le d�ploiement 
ECHO D�ploiement du SNAPSHOT depuis %NewQualifPath%

:: R�pertoire bin
SET Env="bin"
xcopy /E /Y %NewQualifPath%\%Env%\fudaacrue.exe %OldQualifPath%\%Env%\

:: R�pertoire etc
SET Env="etc"
xcopy /E /Y %NewQualifPath%\%Env%\fudaacrue.clusters %OldQualifPath%\%Env%\

:: R�pertoire fudaacrue
SET Env="fudaacrue"
xcopy /E %NewQualifPath%\%Env% %OldQualifPath%\%Env%\

:: R�pertoire platform
SET Env="platform"
xcopy /E %NewQualifPath%\%Env% %OldQualifPath%\%Env%\

pause