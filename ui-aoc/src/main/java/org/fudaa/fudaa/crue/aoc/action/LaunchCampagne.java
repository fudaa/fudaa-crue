/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.aoc.action;

import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * @author Chris
 */
public class LaunchCampagne extends AbstractAocAction {
    public LaunchCampagne() {
        super("LaunchCampagne.ActionName");
        putValue(Action.NAME, NbBundle.getMessage(LaunchCampagne.class, "LaunchCampagne.ActionName"));
        putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(LaunchCampagne.class, "LaunchCampagne.ShortDescription"));
    }

    @Override
    protected void updateEnabledState() {
        setEnabled(aocService.getCurrentAOC() != null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        perspectiveServiceAoc.setDirty(true);
        try {
            aocService.executeCampagne();
        } finally {
            perspectiveServiceAoc.setDirty(false);
        }
    }
}
