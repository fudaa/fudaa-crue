package org.fudaa.fudaa.crue.aoc.action;

import org.fudaa.fudaa.crue.aoc.AocEditeurLoiStrickerTopComponent;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;

import javax.swing.*;
/**
 * Ouverture de {@link AocEditeurLoiStrickerTopComponent}
 */
@ActionID(category = "View",
        id = "org.fudaa.fudaa.crue.aocService.action.AocEditeurLoiStrickerAction")
@ActionRegistration(displayName = "#CTL_AocEditeurLoiStrickerAction")
@ActionReference(path = "Actions/AOC", position = 5)
public final class AocEditeurLoiStrickerAction extends AbstractAocTopComponentOpenerAction {
    public AocEditeurLoiStrickerAction() {
        super("CTL_AocEditeurLoiStrickerAction", AocEditeurLoiStrickerTopComponent.MODE, AocEditeurLoiStrickerTopComponent.TOPCOMPONENT_ID);
    }

    @Override
    public Action createContextAwareInstance(Lookup actionContext) {
        return new AocEditeurLoiStrickerAction();
    }
}
