package org.fudaa.fudaa.crue.aoc.importer;

import org.fudaa.dodico.crue.aoc.projet.AocParamCalc;
import org.fudaa.fudaa.crue.common.helper.AbstractFileImporterProgressRunnable;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

import java.io.File;
import java.util.List;

/**
 * Handler pour importer un fichier csv ou excel.
 */
public class AocParamCalcFileImporter extends AbstractFileImporterProgressRunnable implements ProgressRunnable<List<AocParamCalc>> {
    /**
     * @param file le fichier à importer
     */
    public AocParamCalcFileImporter(File file) {
        super(file);
    }

    @Override
    public List<AocParamCalc> run(ProgressHandle progressHandle) {
        String[][] values = null;
        if (file != null) {
            values = readFile();
        }
        if (values == null) {
            return null;
        }
        return AocParamCalcNodesImporter.extractParamCalc(values);
    }
}
