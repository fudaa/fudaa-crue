package org.fudaa.fudaa.crue.aoc.nodes;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluTableSimpleExporter;
import org.fudaa.dodico.crue.aoc.helper.AocParamCalcSorter;
import org.fudaa.dodico.crue.aoc.projet.AocParamCalc;
import org.fudaa.dodico.crue.validation.ValidatingItem;
import org.fudaa.fudaa.crue.aoc.AocListeVariationClimCalculsTopComponent;
import org.fudaa.fudaa.crue.aoc.importer.AocParamCalcFileImporter;
import org.fudaa.fudaa.crue.aoc.importer.AocParamCalcNodesImporter;
import org.fudaa.fudaa.crue.aoc.importer.AocProfilSectionFileImporter;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.node.NodeChildrenHelper;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.view.DefaultEditableOutlineViewEditor;
import org.fudaa.fudaa.crue.common.view.DefaultEditableOutlineViewEditorPopupFactory;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Vue gérant les {@link AocListeVariationClimCalculsTopComponent}
 */
public class AocListeVariationClimCalculsView extends DefaultEditableOutlineViewEditor<AocListeVariationClimCalculsTopComponent, String, AocParamCalcParentNode> {
  public AocListeVariationClimCalculsView(AocListeVariationClimCalculsTopComponent parent) {
    super(parent);
    setUsedIndexAsDisplayName(false);
    ((DefaultEditableOutlineViewEditorPopupFactory) view.getNodePopupFactory()).setAddCollapseAllNode(true);
    ((DefaultEditableOutlineViewEditorPopupFactory) view.getNodePopupFactory()).setView(view);
  }

  @Override
  public void setEditable(boolean editable) {
    super.setEditable(editable);
    btPaste.setEnabled(editable);
    btImport.setEnabled(btPaste.isEnabled());
  }

  @Override
  protected void initActions() {
    useAddButton = false;
    useRemoveButton = false;
    super.initActions();
  }

  @Override
  protected JComponent[] createOtherButtons() {
    JComponent[] res = super.createOtherButtons();
    res[0].setToolTipText(NbBundle.getMessage(AocListeVariationClimCalculsView.class, "Import.Details"));
    return res;
  }

  @Override
  protected JButton createPasteComponent() {
    JButton btPaste = super.createPasteComponent();
    btPaste.setText(NbBundle.getMessage(AocListeVariationClimCalculsView.class, "Paste.DisplayName"));
    btPaste.setToolTipText(NbBundle.getMessage(AocListeVariationClimCalculsView.class, "Paste.Details"));
    return btPaste;
  }

  /**
   * Remplace tout le contenu du tableau par le presse-papier
   */
  protected void replaceAllByClipboardData() {
    if (!this.editable) {
      return;
    }
    List<AocParamCalc> pastData = AocParamCalcNodesImporter.getPastData();
    if (!pastData.isEmpty()) {
      updateNodesValueWith(pastData);
    }
  }

  @Override
  public void exportAll() {
    AocListeCalculsExportTableModel defaultModel = new AocListeCalculsExportTableModel(getAllAocParamCalcNodes());
    CtuluTableSimpleExporter.doExport(';', defaultModel, CtuluUIForNetbeans.DEFAULT);
  }

  @Override
  public void copyToClipboard(boolean all) {
    Node[] initialSelection = all ? NodeChildrenHelper.getNodesAsArray(getExplorerManager()) : getExplorerManager().getSelectedNodes();
    Set<AocParamCalcNode> selected = new TreeSet<>();
    for (Node node : initialSelection) {
      if (node instanceof AocParamCalcNode) {
        selected.add((AocParamCalcNode) node);
      } else if (node instanceof AocParamCalcParentNode) {
        for (Node childNode : node.getChildren().getNodes()) {
          selected.add((AocParamCalcNode) childNode);
        }
      }
    }
    StringBuilder data = new StringBuilder();
    for (Node node : selected) {
      if (data.length() > 0) {
        data.append('\n');
      }
      data.append(((AbstractNodeFirable) node).getDataAsString());
    }
    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    if (clipboard != null) {
      clipboard.setContents(new StringSelection(data.toString()), null);
    }
  }

  /**
   * Remplace tout le contenu du tableau par le contenu d'un fichier
   */
  protected void importData() {
    if (!this.editable) {
      return;
    }

    CtuluFileChooser fileChooser = AocProfilSectionFileImporter.getImportXLSCSVFileChooser();

    final int res = fileChooser.showDialog(CtuluUIForNetbeans.DEFAULT.getParentComponent(), NbBundle.getMessage(
        AocListeVariationClimCalculsView.class, "Import.DisplayName"));
    if (res == JFileChooser.APPROVE_OPTION) {
      final List<AocParamCalc> result = CrueProgressUtils.showProgressDialogAndRun(
          new AocParamCalcFileImporter(fileChooser.getSelectedFile()),
          NbBundle.getMessage(
              AocListeVariationClimCalculsView.class, "Import.DisplayName"));
      if (CollectionUtils.isEmpty(result)) {
        DialogHelper.showWarn(NbBundle.getMessage(getClass(), "Import.NoDataFound"));
      } else {
        updateNodesValueWith(result);
      }
    }
  }

  @Override
  protected void addElement() {
    //rien a faire
  }

  @Override
  protected void createOutlineView() {
    view = new OutlineView(AocNodeHelper.getCalculDisplay());
    view.getOutline().setColumnHidingAllowed(false);
    view.getOutline().setRootVisible(false);
    view.setTreeSortable(false);
    view.setPropertyColumns(
        AocNodeHelper.PROP_VALUE,
        AocNodeHelper.getValueDisplay(),
        AocNodeHelper.PROP_DELTAQ_VALUE,
        AocNodeHelper.getDeltaQDisplay());
  }

  /**
   * Copie le contenu du presse-papier dans le tableau-> remplace le contenu des cellules sélectionnées et ajoute si nécessaire des entrées.
   */
  public void pasteInSelectedNodes() {
    final List<AocParamCalc> pastData = AocParamCalcNodesImporter.getPastData();
    updateNodesValueWith(pastData);
  }

  private void updateNodesValueWith(List<AocParamCalc> pastData) {
    if (CtuluLibArray.isNotEmpty(pastData)) {
      AocParamCalcSorter sorter = new AocParamCalcSorter(pastData);
      Children children = getExplorerManager().getRootContext().getChildren();
      int nbCalcul = children.getNodesCount();
      for (int i = 0; i < nbCalcul; i++) {
        final Children aocParamCalcNodes = children.getNodeAt(i).getChildren();
        int nbAocParamCalc = aocParamCalcNodes.getNodesCount();
        for (int j = 0; j < nbAocParamCalc; j++) {
          boolean modified = ((AocParamCalcNode) aocParamCalcNodes.getNodeAt(j)).updateWith(sorter);
          if (modified) {
            getTopComponentParent().setModified(true);
          }
        }
      }
    }
    validateData();
  }

  /**
   * @return liste des EchelleSection éditées dans les noeuds
   */
  private List<AocParamCalcNode> getAllAocParamCalcNodes() {
    List<AocParamCalcNode> res = new ArrayList<>();
    for (Node mainNode : getExplorerManager().getRootContext().getChildren().getNodes()) {
      for (Node childNode : mainNode.getChildren().getNodes()) {
        res.add((AocParamCalcNode) childNode);
      }
    }
    return res;
  }

  /**
   * @return liste des EchelleSection éditées dans les noeuds
   */
  public List<AocParamCalc> getAllAocParamCalcNodesValues() {
    List<AocParamCalcNode> allNodes = getAllAocParamCalcNodes();
    List<AocParamCalc> res = new ArrayList<>();
    for (AocParamCalcNode node : allNodes) {
      res.add(node.getMainValue());
    }
    return res;
  }

  /**
   * @return liste des EchelleSection éditées dans les noeuds
   */
  private List<ValidatingItem<AocParamCalc>> getAllAocParamCalcNodesForValidation() {
    List<AocParamCalcNode> allNodes = getAllAocParamCalcNodes();
    List<ValidatingItem<AocParamCalc>> res = new ArrayList<>();
    for (AocParamCalcNode node : allNodes) {
      res.add(new ValidatingItem<>(node.getMainValue(), node));
    }
    return res;
  }

  public boolean validateData() {
    if (getTopComponentParent().getValidationHelper() != null) {
      final CtuluLog log = getValidatingResult();
      return !log.containsErrorOrSevereError();
    }
    return true;
  }

  public CtuluLog getValidatingResult() {
    return getTopComponentParent().getValidationHelper().validateParamCalc(getAllAocParamCalcNodesForValidation());
  }
}
