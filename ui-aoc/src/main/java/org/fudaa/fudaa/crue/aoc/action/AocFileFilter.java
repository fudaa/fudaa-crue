/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.aoc.action;

import org.fudaa.dodico.crue.io.CustomFileFilterExtension;
import org.openide.util.NbBundle;

import java.io.File;

/**
 * @author F Deniger
 */
public class AocFileFilter {
  private static final String AOC_EXTENSION = "aoc.xml";

  public static javax.swing.filechooser.FileFilter create() {
    return new CustomFileFilterExtension(AOC_EXTENSION, NbBundle.getMessage(AocFileFilter.class, "AocFileFilterDescription"));
  }

  public static File addAocExtensionIfNeeded(File aocFile) {
    if (aocFile == null) {
      return null;
    }
    if (!aocFile.getName().endsWith(AOC_EXTENSION)) {
      return new File(aocFile.getParentFile(), aocFile.getName() + "." + AOC_EXTENSION);
    }
    return aocFile;
  }
}
