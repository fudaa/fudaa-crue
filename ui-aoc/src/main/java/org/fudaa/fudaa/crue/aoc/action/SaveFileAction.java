package org.fudaa.fudaa.crue.aoc.action;

import org.openide.util.Lookup.Result;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * @author Chris
 */
public class SaveFileAction extends AbstractAocAction {
    @SuppressWarnings("FieldCanBeLocal")
    private final Result<Boolean> modifiedResult;

    public SaveFileAction() {
        super("SaveFileAction.ActionName");
        putValue(Action.NAME, NbBundle.getMessage(SaveFileAction.class, "SaveFileAction.ActionName"));
        putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(SaveFileAction.class, "SaveFileAction.ShortDescription"));
        modifiedResult = aocService.getLookup().lookupResult(Boolean.class);
        modifiedResult.addLookupListener(this);
    }

    @Override
    protected void updateEnabledState() {
        setEnabled(aocService.getCurrentAOC() != null && aocService.isModified());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        aocService.saveCurrent();
    }
}
