package org.fudaa.fudaa.crue.aoc.importer;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculPermanent;
import org.fudaa.fudaa.crue.aoc.nodes.AocLigneEauCalculPermanentNode;
import org.fudaa.fudaa.crue.aoc.nodes.AocLigneEauCalculPermanentView;
import org.fudaa.fudaa.crue.common.swing.CommonGuiLib;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Methodes utilitaires pour importer des noeuds profil-section
 */
public class AocLoiCalculPermanentNodesImporter {
    private AocLoiCalculPermanentNodesImporter() {
    }

    /**
     * @param calculs les calculs du scenario en cours
     * @param lois    les loi du lhpt en cours
     * @return les noeuds issus des données stockées dans le Clipboard
     */
    public static List<AocLigneEauCalculPermanentNode> getPastData(String[] calculs, String[] lois, Map<String, String> commentByCalculId) {
        try {
            return extractLoiCalculPermanentNode(calculs, lois,commentByCalculId, CommonGuiLib.getStringsFromClipboard());
        } catch (Exception exception) {
            Logger.getLogger(AocLoiCalculPermanentNodesImporter.class.getName()).log(Level.WARNING, "read Data", exception);
        }
        return Collections.emptyList();
    }

    /**
     * parse les données <code>parse</code> pour construire des nodes {@link AocLigneEauCalculPermanentNode}
     *
     * @param calculs les calculs du scenario en cours
     * @param lois    les loi du lhpt en cours
     * @param parse   tableau de valeurs à parser
     * @return les noeuds
     */
    public static List<AocLigneEauCalculPermanentNode> extractLoiCalculPermanentNode(String[] calculs, String[] lois, Map<String, String> commentByCalculId,
                                                                                     String[][] parse) {
        List<AocLigneEauCalculPermanentNode> contents = new ArrayList<>();
        for (String[] lineContent : parse) {
            //on ignore la premier ligne
            if (ArrayUtils.isEmpty(parse) || StringUtils.startsWith(lineContent[0], "#") || AocLigneEauCalculPermanentView.FIRST_COLUMN_NAME_SELECTION
                    .equals(lineContent[0])) {
                continue;
            }
            int offsetColumn = 0;
            //permet de prendre en compte le cas ou la première colonne est un indice.
            if (lineContent.length >= 4 && !StringUtils.isBlank(lineContent[3])) {
                offsetColumn = 1;
            }
            if (lineContent.length >= 3 + offsetColumn) {
                String calculRef = StringUtils.trim(lineContent[offsetColumn]);
                String loiRef = StringUtils.trim(lineContent[1 + offsetColumn]);
                int ponderation = getInteger(StringUtils.trim(lineContent[2 + offsetColumn]), AocLoiCalculPermanent.DEFAULT_PONDERATION_VALUE);
                AocLoiCalculPermanent calculPermanent = new AocLoiCalculPermanent();
                calculPermanent.setCalculRef(calculRef);
                calculPermanent.setLoiRef(loiRef);
                calculPermanent.setPonderation(ponderation);
                AocLigneEauCalculPermanentNode node = new AocLigneEauCalculPermanentNode(calculPermanent, calculs, lois,commentByCalculId);
                contents.add(node);
            }
        }
        return contents;
    }

    public static int getInteger(String string, int defautValue) {
        try {
            return (int) Double.parseDouble(string);
        } catch (Exception ex) {
            //nothing to do here

        }
        return defautValue;
    }
}
