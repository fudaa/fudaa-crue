package org.fudaa.fudaa.crue.aoc;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.aoc.projet.AocValidationGroupe;
import org.fudaa.dodico.crue.aoc.validation.AocValidationCroiseeValidator;
import org.fudaa.fudaa.crue.aoc.nodes.AocValidationCroiseeView;
import org.fudaa.fudaa.crue.aoc.nodes.AocValidationGroupeNode;
import org.fudaa.fudaa.crue.aoc.service.AocService;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultTopComponentParent;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Liste des calculs
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.aoc//AocValidationCroiseeTopComponent//EN",
    autostore = false)
@TopComponent.Description(preferredID = AocValidationCroiseeTopComponent.TOPCOMPONENT_ID, persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = AocValidationCroiseeTopComponent.MODE, openAtStartup = false)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.aoc.AocValidationCroiseeTopComponent")
@TopComponent.OpenActionRegistration(displayName = "#CTL_AocValidationCroiseeTopComponent",
    preferredID = AocValidationCroiseeTopComponent.TOPCOMPONENT_ID)
@ActionReference(path = "Menu/Window/AOC", position = 7)
public final class AocValidationCroiseeTopComponent extends AbstractAocEditeurTopComponent implements DefaultTopComponentParent, PropertyChangeListener {
  public static final String MODE = "aoc-validationCroisee"; //NOI18N
  public static final String TOPCOMPONENT_ID = "AocValidationCroiseeTopComponent"; //NOI18N
  private final AocValidationCroiseeView validationCroiseeView;

  public AocValidationCroiseeTopComponent() {
    super(NbBundle.getMessage(AocValidationCroiseeTopComponent.class, "CTL_" + TOPCOMPONENT_ID));
    initComponents();
    validationCroiseeView = new AocValidationCroiseeView(this);
    validationCroiseeView.setEditable(false);
    add(validationCroiseeView, BorderLayout.CENTER);
    setToolTipText(NbBundle.getMessage(AocValidationCroiseeTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    aocCoampagnedChanged();
    add(createCancelSaveButtons(), BorderLayout.SOUTH);
    setEditable(false);
    associateLookup(ExplorerUtils.createLookup(validationCroiseeView.getExplorerManager(), getActionMap()));
  }

  @Override
  public OutlineView getView() {
    return validationCroiseeView.getView();
  }

  @Override
  protected void aocCoampagnedChanged() {
    List<AocValidationGroupeNode> nodes = new ArrayList<>();
    final AocCampagne aoc = aocService.getCurrentAOC();
    if (aoc != null && aocService.getAocValidationHelper() != null) {
      String[] availableLois = AocValidationCroiseeValidator.getAvailableLois(aoc);
      Arrays.sort(availableLois);
      for (AocValidationGroupe validationGroupe : aoc.getDonnees().getValidationCroisee().getGroupes()) {
        AocValidationGroupeNode node = new AocValidationGroupeNode(validationGroupe.clone(), availableLois);
        nodes.add(node);
      }
    }
    validationCroiseeView.update(nodes);
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    //on recharge toutes les données. Le plus précis serait de mettre à jour la validation et les lois disponibles.
    if (AocService.PROPERTY_LOI_CALCUL_PERMANENT.equals(evt.getPropertyName())) {
      aocCoampagnedChanged();
    }
  }

  @Override
  protected void componentOpenedHandler() {
    super.componentOpenedHandler();
    aocCoampagnedChanged();
    aocService.getPropertyChangeSupport().addPropertyChangeListener(AocService.PROPERTY_LOI_CALCUL_PERMANENT, this);
  }

  @Override
  protected void componentClosedDefinitlyHandler() {
    super.componentClosedDefinitlyHandler();
    aocService.getPropertyChangeSupport().removePropertyChangeListener(AocService.PROPERTY_LOI_CALCUL_PERMANENT, this);
  }

  @Override
  protected void setEditable(boolean b) {
    super.setEditable(b);
    validationCroiseeView.setEditable(b);
  }

  @Override
  public boolean valideModification() {
    if (!isModified()) {
      return true;
    }
    CtuluLog validatingResult = validationCroiseeView.getValidatingResult();
    if (validatingResult.isNotEmpty()) {
      LogsDisplayer.displayError(validatingResult, getName());
    }
    if (validatingResult.containsErrorOrSevereError()) {
      return false;
    }
    final List<AocValidationGroupe> allNodes = validationCroiseeView.getAllNodesValues();
    aocService.setValidationGroupes(allNodes);
    setModified(false);
    return true;
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueValidationCroiseeAoc", PerspectiveEnum.AOC);
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  @SuppressWarnings("unused")
  void writeProperties(java.util.Properties p) {
    // rien a persister
  }

  @SuppressWarnings("unused")
  void readProperties(java.util.Properties p) {
    // rien a persister
  }
}
