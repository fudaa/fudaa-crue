package org.fudaa.fudaa.crue.aoc.nodes;

import org.fudaa.dodico.crue.aoc.projet.AocLoiStrickler;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.fudaa.crue.aoc.perspective.PerspectiveServiceAoc;
import org.fudaa.fudaa.crue.common.property.AbstractNodeValueFirable;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

/**
 * Node pour les donnees {@link AocLoiStrickler}
 *
 * @author deniger
 */
public class AocLoiStricklerNode extends AbstractNodeValueFirable<AocLoiStrickler> implements NodeWithLoisContrat {
    private final String[] lois;
    private final  PerspectiveServiceAoc abstractPerspectiveService = Lookup.getDefault().lookup(PerspectiveServiceAoc.class);

    /**
     * @param loiStrickler les données
     * @param lois         les lois
     */
    public AocLoiStricklerNode(AocLoiStrickler loiStrickler, String[] lois) {
        super(Children.LEAF, Lookups.fixed(loiStrickler));
        setNoImage();
        if (loiStrickler.getLoiRef() == null) {
            setName("");
        } else {
            setName(loiStrickler.getLoiRef());
        }
        DefaultNodePasteType.setIndexUsedAsDisplayName(this);
        this.lois = AbstractAocLoiCalculNode.addEmptyString(lois);
    }

    @Override
    public String[] getLois() {
        return lois;
    }

    @Override
    public boolean canCut() {
        return canDestroy();
    }

    @Override
    public boolean canDestroy() {
        return isEditMode();
    }

    @Override
    public String getDataAsString() {
        final AocLoiStrickler aocLoiStrickler = getAocLoiStrickler();
        return aocLoiStrickler.getLoiRef() + "\t" + aocLoiStrickler.getMin() + "\t" + aocLoiStrickler.getIni() + "\t" + aocLoiStrickler
                .getMax();
    }

    @Override
    public boolean isEditMode() {
        return abstractPerspectiveService == null || abstractPerspectiveService.isInEditMode();
    }

    @Override
    public HelpCtx getHelpCtx() {
        return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(AocLoiStrickler.class));
    }

    @Override
    protected Sheet createSheet() {
        Sheet res = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        set.setName(AocLoiStrickler.class.getSimpleName());
        set.setDisplayName(BusinessMessages.geti18nForClass(AocLoiStrickler.class));
        AocLoiStrickler aocLoiStrickler = getAocLoiStrickler();
        res.put(set);

        AocLoiRefProperty.createLoiProperty(this, set, aocLoiStrickler);

        createProperty(set, aocLoiStrickler, AocNodeHelper.PROP_MIN, AocNodeHelper.getMinDisplay());
        createProperty(set, aocLoiStrickler, AocNodeHelper.PROP_INI, AocNodeHelper.getIniDisplay());
        createProperty(set, aocLoiStrickler, AocNodeHelper.PROP_MAX, AocNodeHelper.getMaxDisplay());

        return res;
    }

    private void createProperty(Sheet.Set set, AocLoiStrickler aocLoiStrickler, String prop, String display) {
        PropertyNodeBuilder builder = new PropertyNodeBuilder();
        PropertySupportReflection propertyPonderation = builder
                .createFromPropertyDesc(DecimalFormatEpsilonEnum.COMPARISON, aocLoiStrickler, this, prop);
        //important pour retrouver la valeur par la suite
        propertyPonderation.setName(prop);
        propertyPonderation.setDisplayName(display);
        PropertyCrueUtils.configureNoCustomEditor(propertyPonderation);
        set.put(propertyPonderation);
    }

    private AocLoiStrickler getAocLoiStrickler() {
        return getLookup().lookup(AocLoiStrickler.class);
    }

    @Override
    public AocLoiStrickler getMainValue() {
        return getAocLoiStrickler();
    }

    public void replaceValueBy(AocLoiStricklerNode newValue) {
        if (newValue != null) {
            final AocLoiStrickler aocLoiStrickler = newValue.getAocLoiStrickler();
            getAocLoiStrickler().setLoiRef(aocLoiStrickler.getLoiRef());
            getAocLoiStrickler().setMin(aocLoiStrickler.getMin());
            getAocLoiStrickler().setIni(aocLoiStrickler.getIni());
            getAocLoiStrickler().setMax(aocLoiStrickler.getMax());
            firePropertyChange(AocNodeHelper.PROP_LOI, null, getAocLoiStrickler().getLoiRef());
            firePropertyChange(AocNodeHelper.PROP_MIN, null, getAocLoiStrickler().getMin());
            firePropertyChange(AocNodeHelper.PROP_INI, null, getAocLoiStrickler().getIni());
            firePropertyChange(AocNodeHelper.PROP_MAX, null, getAocLoiStrickler().getMax());
        }
    }
}
