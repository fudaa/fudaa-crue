package org.fudaa.fudaa.crue.aoc;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuButtonLayout;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.aoc.service.LhptService;
import org.fudaa.fudaa.crue.common.AbstractTopComponent;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.helper.UserSaveAnswer;
import org.fudaa.fudaa.crue.common.services.AbstractPerspectiveService;
import org.openide.util.Lookup;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

/**
 * @author deniger
 */
public abstract class AbstractLhptTopComponent extends AbstractTopComponent implements PropertyChangeListener {
  private static final String END_NAME_IF_MODIFIED = " *";
  private final transient ItemListener scenarioItemListener;
  protected boolean updating;//true si les donnees UI sont en train d'être modifiee
  protected boolean editable;
  protected final transient AbstractPerspectiveService attachedPerspective;
  final transient LhptService service = Lookup.getDefault().lookup(LhptService.class);
  private final JComboBox<ManagerEMHScenario> cbScenario = new JComboBox();
  private boolean modified;
  private boolean editableForTest;
  private final transient FocusListener focusListenerToChangeEditableState = new FocusAdapter() {
    @Override
    public void focusGained(FocusEvent e) {
      updateEditableState();
    }
  };
  /**
   * ecoute les changements d'état ( editable ou pas) de la perspective.
   */
  private final transient PropertyChangeListener stateListener = evt -> perspectiveStateChanged();
  private JButton btValidation = new JButton();
  private JButton btReload = new JButton();

  protected AbstractLhptTopComponent(AbstractPerspectiveService attachedPerspective) {
    this.attachedPerspective = attachedPerspective;
    setFocusable(true);
    scenarioItemListener = e -> {
      if (e.getStateChange() == ItemEvent.DESELECTED) {
        service.unloadLoiMesure((ManagerEMHScenario) e.getItem(), AbstractLhptTopComponent.this);
        selectedScenarioChanged(null);
      } else {
        selectedScenarioChanged(getSelectedScenario());
      }
    };
    cbScenario.setRenderer(new ObjetNommeCellRenderer());
  }

  protected JPanel createCbScenarioPanel() {
    JPanel res = new JPanel(new BorderLayout());
    res.add(new JLabel(MessagesAoc.getMessage("Scenario.Name")), BorderLayout.WEST);
    res.add(cbScenario, BorderLayout.CENTER);
    btValidation = new JButton(MessagesAoc.getMessage("LhptValidation.Button"));
    btValidation.setToolTipText(MessagesAoc.getMessage("LhptValidation.Description"));
    btValidation.addActionListener(e -> showValidation());

    btReload = new JButton(MessagesAoc.getMessage("LhptReload.Button"));
    btReload.setToolTipText(MessagesAoc.getMessage("LhptReload.Description"));
    btReload.addActionListener(e -> reloadCurrentScenario());
    JPanel pnButton = new JPanel(new BuButtonLayout());
    pnButton.add(btValidation);
    pnButton.add(btReload);
    res.add(pnButton, BorderLayout.EAST);
    updateStateButtonValidationAndReload();
    res.setBorder(BuBorders.EMPTY5555);
    return res;
  }

  private void reloadCurrentScenario() {
    boolean ok = DialogHelper
        .showQuestion(MessagesAoc.getMessage("LhptReload.Button"), MessagesAoc.getMessage("LhptReload.DialogText", getSelectedScenario().getNom()));
    if (ok) {
      if (isModified()) {
        cancelModification();
      }
      service.reloadFromDisk(getSelectedScenario());
      setModified(false);
    }
  }

  private void updateStateButtonValidationAndReload() {
    btValidation.setEnabled(!isModified() && getSelectedScenario() != null && service.getLoadedLoiMesure(getSelectedScenario()) != null);
    btReload.setEnabled(getSelectedScenario() != null && service.getLoadedLoiMesure(getSelectedScenario()) != null);
  }

  private void showValidation() {
    if (getSelectedScenario() != null) {
      final LhptService.LoiMesurePostState loadedLoiMesure = service.getLoadedLoiMesure(getSelectedScenario());
      if (loadedLoiMesure != null) {
        service.validLoiMesure(loadedLoiMesure, MessagesAoc.getMessage("LhptValidation.DialogTitle", getSelectedScenario().getNom()));
      }
    }
  }

  public LhptService getService() {
    return service;
  }

  public AbstractPerspectiveService getAttachedPerspective() {
    return attachedPerspective;
  }

  protected abstract void currentDataChanged(ManagerEMHScenario emhScenario, LhptService.LoiMesurePostState loiMesuresPost);

  protected final void selectedScenarioChanged(ManagerEMHScenario emhScenario) {
    LhptService.LoiMesurePostState loiMesure = null;
    if (emhScenario != null) {
      loiMesure = service.loadLoiMesure(emhScenario, this);
    }
    currentDataChanged(emhScenario, loiMesure);
    updateStateButtonValidationAndReload();
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (!isOpened()) {
      return;
    }
    if (LhptService.PROPERTY_PROJECT_LOADED.equals(evt.getPropertyName())) {
      updateListOfScenarios();
    } else {
      //une action de rechargement a été demandée et concerne les données en cours d'édition:
      final ManagerEMHScenario selectedScenario = getSelectedScenario();
      if (selectedScenario != null && evt.getPropertyName().equals(LhptService.getPropertyLoiMesure(selectedScenario))) {
        //on recharge les données
        LhptService.LoiMesurePostState loiMesure = service.loadLoiMesure(selectedScenario, this);
        currentDataChanged(selectedScenario, loiMesure);
        setModified(false);
        updateStateButtonValidationAndReload();
      }
    }
  }

  private void updateListOfScenarios() {
    final List<ManagerEMHScenario> listeScenarios = service.getListeScenarios();
    ManagerEMHScenario old = getSelectedScenario();
    String oldId = old == null ? null : old.getId();
    cbScenario.setSelectedItem(null);
    //pas d'evt pendant la mise à jour de la liste
    cbScenario.removeItemListener(scenarioItemListener);
    cbScenario.removeAllItems();
    cbScenario.addItem(null);
    ManagerEMHScenario toBeSelected = null;
    for (ManagerEMHScenario scenario : listeScenarios) {
      if (oldId != null && oldId.equals(scenario.getId())) {
        toBeSelected = scenario;
      }
      cbScenario.addItem(scenario);
    }
    cbScenario.addItemListener(scenarioItemListener);
    cbScenario.setSelectedItem(toBeSelected);
  }

  public ManagerEMHScenario getSelectedScenario() {
    return (ManagerEMHScenario) cbScenario.getSelectedItem();
  }

  protected JPanel createCancelSaveButtons() {
    return createCancelSaveButtons(null);
  }

  protected void updateTopComponentName() {
    String name = getName();
    if (name == null) {
      return;
    }
    if (modified && !name.endsWith(END_NAME_IF_MODIFIED)) {
      setName(name + END_NAME_IF_MODIFIED);
    } else if (!modified && name.endsWith(END_NAME_IF_MODIFIED)) {
      setName(StringUtils.removeEnd(name, END_NAME_IF_MODIFIED));
    }
  }

  @Override
  public boolean isModified() {
    return modified;
  }

  public void setModified(boolean modified) {
    if (updating) {
      return;
    }
    this.modified = modified;
    cbScenario.setEnabled(!modified);
    updateTopComponentName();
    if (buttonSave != null) {
      buttonSave.setEnabled(modified);
    }
    if (buttonCancel != null) {
      buttonCancel.setEnabled(modified);
    }
    updateStateButtonValidationAndReload();
  }

  /**
   * utiliser uniquement pour les test
   */
  public void setEditableForTest() {
    editableForTest = true;
  }

  private void updateEditableState() {
    if (editableForTest) {
      setEditable(true);
    } else {
      setEditable(
          WindowManager.getDefault().getRegistry().getActivated() == this && (attachedPerspective == null || attachedPerspective.isInEditMode()));
    }
  }

  @Override
  public final void componentClosedDefinitly() {
    service.removeProjectListener(this);
    removeFocusListener(focusListenerToChangeEditableState);
    service.removeProjectListener(this);
    componentClosedDefinitlyHandler();
  }

  protected void componentClosedDefinitlyHandler() {
  }

  @Override
  public final void componentClosedTemporarily() {
    service.removeProjectListener(this);
  }

  public boolean isEditable() {
    return editable;
  }

  protected void setEditable(boolean b) {

    this.editable = b;
  }

  @Override
  public boolean canClose() {
    if (isModified()) {
      UserSaveAnswer askForSave = askForSave();
      if (UserSaveAnswer.CANCEL.equals(askForSave)) {
        return false;
      }
    }
    return super.canClose();
  }

  @Override
  public final void componentOpened() {
    addFocusListener(focusListenerToChangeEditableState);
    super.componentOpened();
    service.addProjectListener(this);
    if (service.isProjectLoaded()) {
      updateListOfScenarios();
    }
    componentOpenedHandler();
  }

  protected void componentOpenedHandler() {
  }

  private void perspectiveStateChanged() {
    updateEditableState();
  }

  @Override
  protected void componentDeactivated() {
    UserSaveAnswer confirmSaveOrNot = askForSave();

    if (UserSaveAnswer.CANCEL.equals(confirmSaveOrNot)) {
      return;
    }
    if (attachedPerspective != null) {
      attachedPerspective.removeStateListener(stateListener);
    }
    setEditable(false);

    super.componentDeactivated();
  }

  @Override
  protected void componentActivated() {
    if (attachedPerspective != null) {
      attachedPerspective.addStateListener(stateListener);
    }
    updateEditableState();
    super.componentActivated();
  }
}
