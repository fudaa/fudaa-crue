package org.fudaa.fudaa.crue.aoc.nodes;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.aoc.ParametrageProfilSection;
import org.fudaa.fudaa.crue.common.property.AbstractNodeValueFirable;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.services.AbstractPerspectiveService;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.lookup.Lookups;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Noeud affichant une ligne profil->section
 *
 * @author deniger
 */
public class ParametrageProfilSectionNode extends AbstractNodeValueFirable<ParametrageProfilSection> implements NodeWithSectionsContrat {
  private final AbstractPerspectiveService abstractPerspectiveService;
  private final String[] sections;

  public ParametrageProfilSectionNode(AbstractPerspectiveService abstractPerspectiveService, ParametrageProfilSection aocEchellesSection,
                                      String[] sections) {
    super(Children.LEAF, Lookups.fixed(aocEchellesSection));
    setNoImage();
    this.abstractPerspectiveService = abstractPerspectiveService;
    setName(aocEchellesSection.getPk());
    this.sections = sections;
    DefaultNodePasteType.setIndexUsedAsDisplayName(this);
  }

  @Override
  public String[] getSections() {
    return sections;
  }

  @Override
  public String getDataAsString() {
    final ParametrageProfilSection echellesSection = getEchellesSection();
    return echellesSection.getPk() + "\t" + echellesSection.getSectionRef();
  }

  @Override
  public boolean canCut() {
    return canDestroy();
  }

  @Override
  public boolean canDestroy() {
    return isEditMode();
  }

  @Override
  public boolean isEditMode() {
    return abstractPerspectiveService == null || abstractPerspectiveService.isInEditMode();
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(getEchellesSection()));
  }

  @Override
  protected Sheet createSheet() {
    Sheet res = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setName(ParametrageProfilSection.class.getSimpleName());
    set.setDisplayName(BusinessMessages.geti18nForClass(ParametrageProfilSection.class));
    ParametrageProfilSection aocEchellesSection = getEchellesSection();
    res.put(set);

    AocNodeHelper.createProperty(this, set, aocEchellesSection, AocNodeHelper.PROP_PROFIL, AocNodeHelper.getEchelleDisplay());

    try {
      AocSectionProperty propertySection = new AocSectionProperty(this, aocEchellesSection);
      propertySection.setName(AocNodeHelper.PROP_SECTION);
      propertySection.setDisplayName(AocNodeHelper.getSectionDisplay());
      PropertyCrueUtils.configureNoCustomEditor(propertySection);
      set.put(propertySection);
    } catch (NoSuchMethodException e) {
      Logger.getLogger(ParametrageProfilSectionNode.class.getName()).log(Level.SEVERE, "can't find method", e);
    }
    //important pour retrouver la valeur par la suite

    return res;
  }

  public ParametrageProfilSection getEchellesSection() {
    return getLookup().lookup(ParametrageProfilSection.class);
  }

  @Override
  public ParametrageProfilSection getMainValue() {
    return getEchellesSection();
  }

  public void replaceValueBy(ParametrageProfilSectionNode newValue) {
    if (newValue != null) {
      getEchellesSection().setSectionRef(newValue.getEchellesSection().getSectionRef());
      getEchellesSection().setPk(newValue.getEchellesSection().getPk());
      firePropertyChange(AocNodeHelper.PROP_SECTION, null, getEchellesSection().getSectionRef());
      firePropertyChange(AocNodeHelper.PROP_PROFIL, null, getEchellesSection().getPk());
    }
  }
}
