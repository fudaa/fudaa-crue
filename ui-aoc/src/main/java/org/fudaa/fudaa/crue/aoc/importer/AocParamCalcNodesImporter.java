package org.fudaa.fudaa.crue.aoc.importer;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluDoubleParser;
import org.fudaa.dodico.crue.aoc.projet.AocParamCalc;
import org.fudaa.fudaa.crue.aoc.nodes.AocNodeHelper;
import org.fudaa.fudaa.crue.common.swing.CommonGuiLib;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Methodes utilitaires pour importer des noeuds AocParamCalc
 */
public class AocParamCalcNodesImporter {
    private AocParamCalcNodesImporter() {
    }

    /**
     * @return les données issus des données stockées dans le Clipboard
     */
    public static List<AocParamCalc> getPastData() {
        try {
            return extractParamCalc(CommonGuiLib.getStringsFromClipboard());
        } catch (Exception exception) {
            Logger.getLogger(AocParamCalcNodesImporter.class.getName()).log(Level.WARNING, "read Data", exception);
        }
        return Collections.emptyList();
    }

    /**
     * parse les données <code>parse</code> pour construire des nodes {@link AocParamCalc}
     *
     * @param parse tableau de valeurs à parser
     * @return les noeuds
     */
    public static List<AocParamCalc> extractParamCalc(String[][] parse) {
        List<AocParamCalc> contents = new ArrayList<>();
        CtuluDoubleParser doubleParser = new CtuluDoubleParser();
        doubleParser.setEmptyIsNull(true);
        for (String[] lineContent : parse) {
            //on ignore la premier ligne
            if (ArrayUtils.isEmpty(parse) || StringUtils.startsWith(lineContent[0], "#") || AocNodeHelper.getCalculDisplay()
                    .equals(lineContent[0])) {
                continue;
            }
            if (lineContent.length >= 5) {
                if (StringUtils.isNotBlank(lineContent[4])) {
                    String calculRef = StringUtils.trim(lineContent[0]);
                    String emhRef = StringUtils.trim(lineContent[1]);
                    double deltaQ = doubleParser.parse(StringUtils.trim(lineContent[4]));
                    AocParamCalc aocParamCalc = new AocParamCalc();
                    aocParamCalc.setCalculRef(calculRef);
                    aocParamCalc.setEmhRef(emhRef);
                    aocParamCalc.setDeltaQ(deltaQ);
                    contents.add(aocParamCalc);
                }
            }
        }
        return contents;
    }
}
