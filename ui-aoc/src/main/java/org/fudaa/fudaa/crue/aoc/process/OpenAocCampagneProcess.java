/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.aoc.process;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.io.CrueFileFormatBuilderAOC;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.fudaa.crue.aoc.MessagesAoc;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;

import java.io.File;

/**
 * Un process pour ouvrir un fichier de campogne Aoc.
 *
 * @author Fred Deniger
 */
public class OpenAocCampagneProcess implements ProgressRunnable<AocCampagne> {
  private final File aocFile;
  private final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

  public OpenAocCampagneProcess(File aocFile) {
    this.aocFile = aocFile;
  }

  @Override
  public AocCampagne run(ProgressHandle handle) {
    handle.switchToIndeterminate();
    final CoeurConfig coeurConfigDefault = configurationManagerService.getCoeurManager().getCoeurConfigDefault(configurationManagerService.getCoeurManager().getHigherGrammaire());
    final Crue10FileFormat<AocCampagne> fileFormat = new CrueFileFormatBuilderAOC().getFileFormat(coeurConfigDefault);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueIOResu<AocCampagne> resu = fileFormat.read(aocFile, log, null);
    if (log.isNotEmpty()) {
      LogsDisplayer.displayError(log, MessagesAoc.getMessage("OpenAoc.BilanDialog", aocFile.getName()));
    }

    handle.finish();
    if (resu.getMetier() != null) {
      resu.getMetier().setAocFile(aocFile);
    }
    return resu.getMetier();
  }
}
