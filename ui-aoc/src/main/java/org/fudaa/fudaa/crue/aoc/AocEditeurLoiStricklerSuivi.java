/*
 * @creation 23 juin 2004
 * @modification $Date: 2006-09-19 14:55:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.crue.aoc;

import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.courbe.*;
import org.fudaa.ebli.trace.TraceBox;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.fudaa.crue.aoc.nodes.AocLoiStricklerNode;
import org.fudaa.fudaa.crue.aoc.nodes.AocLoiStricklerView;
import org.fudaa.fudaa.crue.loi.loiff.LoiUiController;
import org.openide.nodes.Node;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;

/**
 * @author Fred Deniger
 * @version $Id: EGInteractionSuivi.java,v 1.13 2006-09-19 14:55:53 deniger Exp $
 */
public class AocEditeurLoiStricklerSuivi extends EGInteractiveComponent implements MouseListener, MouseMotionListener, PropertyChangeListener {
  private final AocLoiStricklerView view;
  private final transient LoiUiController loiUiController;
  private transient TraceIcon traceIcon;
  private final EGGraphe egGraphe;
  private transient TraceBox traceBox;
  private Color traceColor;

  /**
   */
  public AocEditeurLoiStricklerSuivi(final AocLoiStricklerView view, LoiUiController loiUiController) {
    this.view = view;
    this.loiUiController = loiUiController;
    egGraphe = loiUiController.getGraphe();
    egGraphe.addMouseListener(this);
    egGraphe.addMouseMotionListener(this);
    view.getExplorerManager().addPropertyChangeListener(this);
    setActive(true);
  }

  @Override
  public String getDescription() {
    return EbliLib.getS("suivi");
  }

  private void updateWithMouse(final int mouseX) {
    if (!isActive()) {
      return;
    }
    final EGCourbe ci = egGraphe.getSelectedComponent();
    if (ci == null) {
      return;
    }
    int x = (int) Math.rint(egGraphe.getTransformer().getXReel(mouseX));
    if (x >= 0 && x < view.getExplorerManager().getRootContext().getChildren().getNodesCount()) {
      Node toSelect = view.getExplorerManager().getRootContext().getChildren().getNodeAt(x);
      try {
        view.getExplorerManager().setSelectedNodes(new Node[]{toSelect});
      } catch (PropertyVetoException e) {
        //nothing to do
      }
    }
  }

  private TIntArrayList getSelectedPoints() {
    TIntArrayList res = new TIntArrayList();
    final Node[] selectedNodes = view.getExplorerManager().getSelectedNodes();
    for (Node selectedNode : selectedNodes) {
      if (CtuluLibString.isNumeric(selectedNode.getDisplayName())) {
        double val = Double.parseDouble(selectedNode.getDisplayName());
        res.add(((int) val) - 1);
      }
    }
    return res;
  }

  @Override
  protected void paintComponent(final Graphics graphics) {
    final Color old = graphics.getColor();
    if (traceBox == null) {
      traceBox = new TraceBox();
    }
    int[] selected = getSelectedPoints().toNativeArray();
    if (selected.length == 0) {
      return;
    }
    EGAxeVertical axeVertical = null;
    if (loiUiController.getEGGrapheSimpleModel().getNbEGObject() > 0) {
      axeVertical = loiUiController.getEGGrapheSimpleModel().getCourbe(0).getAxeY();
    }
    if (axeVertical == null) {
      return;
    }
    final EGCourbeSimple[] courbes = loiUiController.getEGGrapheSimpleModel().getCourbes();
    final EGRepere r = egGraphe.getTransformer();
    final int xmax = axeVertical.isDroite() ? r.getMaxEcranX() + egGraphe.getOffSet(axeVertical) + 3 : r.getMaxEcranX();
    final int xmin = axeVertical.isDroite() ? r.getMinEcranX() : r.getMinEcranX() - egGraphe.getOffSet(axeVertical) - 3;
    for (int i = 0; i < selected.length; i++) {
      int value = selected[i];
      final int x = r.getXEcran(value);
      graphics.setColor(getTraceColor());
      final int maxY = r.getMaxEcranY() + 2;
      graphics.drawLine(x, r.getMinEcranY(), x, maxY);

      traceBox.setColorText(axeVertical.getLineColor());
      traceBox.getTraceLigne().setCouleur(axeVertical.getLineColor());

      traceBox.setVPosition(SwingConstants.CENTER);
      traceBox.setHPosition(SwingConstants.CENTER);
      traceBox.paintBox((Graphics2D) graphics, x, r.getMinEcranY() - 10,
          ((AocLoiStricklerNode) view.getExplorerManager().getRootContext().getChildren().getNodeAt(value)).getMainValue().getLoiRef());
      traceBox.setHPosition(axeVertical.isDroite() ? SwingConstants.LEFT : SwingConstants.RIGHT);
      for (EGCourbeSimple courbe : courbes) {
        double yReel = courbe.getModel().getY(value);
        if (axeVertical.getRange().isValueContained(yReel)) {
          graphics.setColor(getTraceColor());
          final int y = r.getYEcran(yReel, axeVertical);
          graphics.drawLine(xmin, y, xmax, y);
          getSelectedIcone().paintIconCentre(graphics, x, y);
          traceBox.paintBox((Graphics2D) graphics, xmax, y, axeVertical.getStringInterpolatedAffiche(yReel, true));
        }
      }
    }

    graphics.setColor(old);
  }

  private Color getTraceColor() {
    if (traceColor == null) {
      traceColor = Color.lightGray;
    }
    return traceColor;
  }

  private TraceIcon getSelectedIcone() {
    if (traceIcon == null) {
      traceIcon = new TraceIcon(TraceIcon.CARRE_PLEIN, 4);
      traceIcon.setCouleur(Color.gray);
    }
    return traceIcon;
  }

  @Override
  public void mouseClicked(final MouseEvent mouseEvent) {
    if (mouseEvent.isConsumed()) {
      return;
    }
    if (!mouseEvent.isPopupTrigger()) {
      updateWithMouse(mouseEvent.getX());
    }
  }

  @Override
  public void mouseDragged(final MouseEvent mouseEvent) {
    //rien a faire
  }

  @Override
  public void mouseEntered(final MouseEvent mouseEvent) {
    //rien a faire
  }

  @Override
  public void mouseExited(final MouseEvent mouseEvent) {
    //rien a faire
  }

  @Override
  public void mouseMoved(final MouseEvent mouseEvent) {
    //rien a faire
  }

  @Override
  public void mousePressed(final MouseEvent mouseEvent) {
    //rien a faire
  }

  @Override
  public void mouseReleased(final MouseEvent mouseEvent) {
    //rien a faire
  }

  @Override
  public void setActive(final boolean _b) {
    super.setActive(_b);
    if (!_b && egGraphe != null) {
      egGraphe.repaint();
    }
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    repaint();
  }
}
