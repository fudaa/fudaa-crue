package org.fudaa.fudaa.crue.aoc.action;

import org.fudaa.fudaa.crue.aoc.AocLoisCalculsTransitoiresTopComponent;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;

import javax.swing.*;

/**
 * Ouverture de {@link AocLoisCalculsTransitoiresTopComponent}
 */
@ActionID(category = "View",
        id = "org.fudaa.fudaa.crue.aocService.action.AocLoisCalculsTransitoiresAction")
@ActionRegistration(displayName = "#CTL_AocLoisCalculsTransitoiresAction")
@ActionReference(path = "Actions/AOC", position = 4)
public final class AocLoisCalculsTransitoiresAction extends AbstractAocTopComponentOpenerAction {
    public AocLoisCalculsTransitoiresAction() {
        super("CTL_AocLoisCalculsTransitoiresAction", AocLoisCalculsTransitoiresTopComponent.MODE, AocLoisCalculsTransitoiresTopComponent.TOPCOMPONENT_ID);
    }

    @Override
    public Action createContextAwareInstance(Lookup actionContext) {
        return new AocLoisCalculsTransitoiresAction();
    }
}
