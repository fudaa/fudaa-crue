package org.fudaa.fudaa.crue.aoc.nodes;

import org.fudaa.dodico.crue.aoc.projet.AocValidationGroupe;
import org.fudaa.fudaa.crue.aoc.perspective.PerspectiveServiceAoc;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorSupport;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Propriete pour l'affichage des lois
 *
 * @author deniger
 */
public class AocValidationGroupeLoisRefProperty extends PropertySupportReflection<String> {
    /**
     * @param node     doit etre de type AbstractNodeFirable
     * @param instance l'objet a modifier
     */
    private AocValidationGroupeLoisRefProperty(NodeWithLoisContrat node, AocValidationGroupe instance) throws NoSuchMethodException {
        super((AbstractNodeFirable) node, instance, String.class, "getLoisAsString", "setLoisAsString");
        PropertyCrueUtils.configureNoEditAsText(this);
    }

    public static void createLoisProperty(NodeWithLoisContrat node, Sheet.Set set, AocValidationGroupe validationGroupe) {
        try {
            AocValidationGroupeLoisRefProperty propertyLois = new AocValidationGroupeLoisRefProperty(node, validationGroupe);
            //important pour retrouver la valeur par la suite
            propertyLois.setName(AocNodeHelper.PROP_LOIS);
            propertyLois.setDisplayName(AocNodeHelper.getLoisDisplay());
            set.put(propertyLois);
        } catch (NoSuchMethodException e) {
            Logger.getLogger(AocValidationGroupeLoisRefProperty.class.getName()).log(Level.SEVERE, "can't find method", e);
        }
    }

    @Override
    public PropertyEditor getPropertyEditor() {
        return new AocLoisRefPropertyEditor(((AocValidationGroupeNode) this.node).getAvailableLois());
    }

    public static class AocLoisRefPropertyEditor extends PropertyEditorSupport implements ExPropertyEditor {
        private final String[] lois;
        final PerspectiveServiceAoc perspectiveService = Lookup.getDefault().lookup(PerspectiveServiceAoc.class);
        private PropertyEnv env;

        protected AocLoisRefPropertyEditor(String[] lois) {
            this.lois = lois;
        }

        @Override
        public String getAsText() {
            return (String) getValue();
        }

        @Override
        public void setAsText(String string) {
            setValue(string);
        }

        @Override
        public Component getCustomEditor() {
            JDialog jd;

            // encapsulation dans un JDialog pour gérer la persistence
            if (perspectiveService.isInEditMode()) {
                jd = DialogHelper
                        .createOKCancelDialog(new AocValidationGroupeLoisCustomEditor(this, env, lois,true).getComponent(), this.getClass().getName(),
                                AocNodeHelper.getLoisDisplay(), true,
                                env, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
            } else {
                jd = DialogHelper
                        .createCloseDialog(new AocValidationGroupeLoisCustomEditor(this, env, lois,false).getComponent(), this.getClass().getName(),
                                AocNodeHelper.getLoisDisplay(), true,
                                DialogHelper.NO_VERSION_FOR_PERSISTENCE);
            }
            return jd;
        }

        @Override
        public void attachEnv(PropertyEnv env) {
            this.env = env;
        }

        @Override
        public boolean supportsCustomEditor() {
            return true;
        }
    }
}
