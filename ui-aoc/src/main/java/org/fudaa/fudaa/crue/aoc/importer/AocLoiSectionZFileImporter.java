package org.fudaa.fudaa.crue.aoc.importer;

import org.fudaa.fudaa.crue.aoc.courbe.AocLoiLine;
import org.fudaa.fudaa.crue.common.helper.AbstractFileImporterProgressRunnable;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

import java.io.File;
import java.util.Collections;
import java.util.List;

/**
 * Handler pour importer un fichier csv ou excel.
 */
public class AocLoiSectionZFileImporter extends AbstractFileImporterProgressRunnable implements ProgressRunnable<List<AocLoiLine>> {
    /**
     * @param file le fichier à importer
     */
    public AocLoiSectionZFileImporter(File file) {
        super(file);
    }

    @Override
    public List<AocLoiLine> run(ProgressHandle progressHandle) {
        String[][] values = null;
        if (file != null) {
            values = readFile();
        }
        if (values == null) {
            return Collections.emptyList();
        }
        return AocLoiSectionZImporter.extractLoiSectionZ(values);
    }
}
