package org.fudaa.fudaa.crue.aoc.courbe;

import java.util.*;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.PropertyValidator;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.aoc.ParametrageProfilSection;
import org.fudaa.dodico.crue.metier.emh.LoiTF;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionTF;
import org.fudaa.fudaa.crue.aoc.service.LhptService;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiCourbeModel;
import org.fudaa.fudaa.crue.loi.common.LoiLine;

/**
 * Un modele pour les courbe SectionsZ.
 *
 * @author deniger
 */
public class LHPTLoiSectionZCourbeModel extends AbstractLoiCourbeModel {
    private final LhptService.LoiMesurePostState loiMesurePostData;
    private LoiTF loiTF;
    /**
     * Contient les position des profils. Mis à jour par {@link #updatePts()}.
     */
    private final Map<String, Double> profilPosition = new HashMap<>();
    private CrueConfigMetier crueConfigMetier;

    public LHPTLoiSectionZCourbeModel(ConfigLoi configLoi, LhptService.LoiMesurePostState loiMesurePostData) {
        super(configLoi);
        this.loiMesurePostData = loiMesurePostData;
        lines = new ArrayList<>();
    }

    protected Double getProfilPosition(String profil) {
        return profilPosition.get(profil);
    }

    @Override
    protected AocLoiLine getLoiLine(int idx) {
        return (AocLoiLine) super.getLoiLine(idx);
    }

    public List<PtEvolutionTF> createEvolutionTF() {
        List<PtEvolutionTF> res = new ArrayList<>();
        for (LoiLine line : lines) {
            AocLoiLine aocLoiLine = (AocLoiLine) line;
            res.add(new PtEvolutionTF(aocLoiLine.getProfil(), aocLoiLine.getPtEvolutionFF().getOrdonnee()));
        }
        return res;
    }

    @Override
    public boolean setValue(int i, double initNewX, double initNewY, CtuluCommandContainer commandContainer) {
        final AocLoiLine loiLine = getLoiLine(i);
        return super.setValue(i, loiLine.getPtEvolutionFF().getAbscisse(), initNewY, commandContainer);
    }

    public void setCrueConfigMetier(CrueConfigMetier crueConfigMetier) {
        this.crueConfigMetier = crueConfigMetier;
    }

    @Override
    protected LoiLine createLoiLineInstance() {
        throw new IllegalAccessError("Not implemented");
    }

    @Override
    public boolean useSpecificIcon(int idx) {
        return false;
    }

    @Override
    public void setPts(List<PtEvolutionFF> pts) {
        throw new IllegalAccessError("Not implemented");
    }

    public boolean isProfilExists(String profilName) {
        return profilName != null && profilPosition.containsKey(profilName);
    }

    protected void profilUpdated() {
        super.sortLoiLines();
        revalidateLines();
        changeDone();
    }

    public void addLine(String newProfilName, Double newValue) {
        AocLoiLine line = new AocLoiLine();
        line.setProfil(newProfilName);
        PtEvolutionFF ptInCourbe = new PtEvolutionFF();
        line.setPtEvolutionFF(ptInCourbe);
        ptInCourbe.setOrdonnee(newValue);
        if (profilPosition.containsKey(newProfilName)) {
            ptInCourbe.setAbscisse(profilPosition.get(newProfilName));
        }
        lines.add(line);
        profilUpdated();
    }

    private void updatePts() {
        profilPosition.clear();
        lines.clear();
        final List<ParametrageProfilSection> profilSection = loiMesurePostData.getLoiMesuresPost().getEchellesSections().getEchellesSectionList();
        final Map<String, Double> sectionsPosition = loiMesurePostData.getSections();
        for (ParametrageProfilSection aocProfilSection : profilSection) {
            String section = aocProfilSection.getSectionRef();
            String profil = aocProfilSection.getPk();
            if (sectionsPosition.containsKey(section)) {
                profilPosition.put(profil, sectionsPosition.get(section));
            } else {
                profilPosition.put(profil, 0d);
            }
        }
        final List<PtEvolutionTF> ptEvolutionTF = loiTF.getEvolutionTF().getPtEvolutionTF();
        for (PtEvolutionTF pointInLoi : ptEvolutionTF) {
            final String profilName = pointInLoi.getAbscisse();
            AocLoiLine line = new AocLoiLine();
            line.setProfil(profilName);
            PtEvolutionFF ptInCourbe = new PtEvolutionFF();
            line.setPtEvolutionFF(ptInCourbe);
            ptInCourbe.setOrdonnee(pointInLoi.getOrdonnee());
            if (profilPosition.containsKey(profilName)) {
                ptInCourbe.setAbscisse(profilPosition.get(profilName).doubleValue());
            } else {
                ptInCourbe.setAbscisse(0);
            }
            lines.add(line);
        }
        revalidateLines();
        super.sortLoiLines();
    }

    private void revalidateLines() {
        int idx = 0;
        Set<String> definedProfil = new HashSet<>();
        for (LoiLine line : lines) {
            idx++;
            AocLoiLine aocLoiLine = (AocLoiLine) line;
            line.setError(null);
            line.setErrorMessage(null);
            final String profil = aocLoiLine.getProfil();
            if (StringUtils.isBlank(profil)) {
                line.setError(CtuluLogLevel.ERROR);
                line.setErrorMessage(BusinessMessages.getString("aoc.validation.loiMesure.profilBlank", idx));
            } else if (!profilPosition.containsKey(profil)) {
                line.setError(CtuluLogLevel.ERROR);
                line.setErrorMessage(BusinessMessages.getString("aoc.validation.loiMesure.profilUsedInLoiAndNotFound", idx, profil));
            } else {
                if (definedProfil.contains(profil)) {
                    line.setError(CtuluLogLevel.ERROR);
                    line.setErrorMessage(BusinessMessages.getString("aoc.validation.loiMesure.profilUsedSeveralTime", Integer.toString(idx),
                            profil));
                }
                definedProfil.add(profil);
            }
            if (line.getError() == null) {
                final PropertyValidator loiOrdonneeValidatorForLoiTF = crueConfigMetier.getLoiOrdonneeValidator(EnumTypeLoi.LoiSectionsZ);
                String error = loiOrdonneeValidatorForLoiTF.getValidateErrorForValue(line.getPtEvolutionFF().getOrdonnee());
                if (error != null) {
                    line.setError(CtuluLogLevel.ERROR);
                    line.setErrorMessage(error);
                } else {
                    String warning = loiOrdonneeValidatorForLoiTF.getNormaliteWarningForValue(line.getPtEvolutionFF().getOrdonnee());
                    if (warning != null) {
                        line.setError(CtuluLogLevel.WARNING);
                        line.setErrorMessage(warning);
                    }
                }
            }
        }
    }

    public void setCourbe(LoiTF loi) {
        this.loiTF = loi;
        updatePts();
    }

    public Object[] getAvailableProfils(int row) {
        TreeSet<String> availableProfils = new TreeSet<>(profilPosition.keySet());
        int currentRow = 0;
        for (LoiLine line : getLoiLines()) {
            if (currentRow != row) {
                final String profil = ((AocLoiLine) line).getProfil();
                if (profil != null) {
                    availableProfils.remove(profil);
                }
            }
            currentRow++;
        }
        return availableProfils.toArray(new Object[0]);
    }

    public void pasteLines(List<AocLoiLine> pastDataList) {
        Map<String, AocLoiLine> existingLine = new HashMap<>();
        for (LoiLine line : lines) {
            AocLoiLine aocLoiLine = (AocLoiLine) line;
            existingLine.put(aocLoiLine.getProfil(), aocLoiLine);
        }
        for (AocLoiLine pastData : pastDataList) {
            if (StringUtils.isNotBlank(pastData.getProfil())) {
                if (existingLine.containsKey(pastData.getProfil())) {
                    existingLine.get(pastData.getProfil()).getPtEvolutionFF().setOrdonnee(pastData.getPtEvolutionFF().getOrdonnee());
                } else if (profilPosition.containsKey(pastData.getProfil())) {
                    final Double aDouble = profilPosition.get(pastData.getProfil());
                    pastData.getPtEvolutionFF().setAbscisse(aDouble);
                    lines.add(pastData);
                }
            }
        }
        profilUpdated();
    }
}
