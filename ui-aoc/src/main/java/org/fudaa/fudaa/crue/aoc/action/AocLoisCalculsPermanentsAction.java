package org.fudaa.fudaa.crue.aoc.action;

import org.fudaa.fudaa.crue.aoc.AocLignesEauCalculsPermanentsTopComponent;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;

import javax.swing.*;

/**
 * Ouverture de {@link AocLignesEauCalculsPermanentsTopComponent}
 */
@ActionID(category = "View",
        id = "org.fudaa.fudaa.crue.aocService.action.AocLoisCalculsPermanentsAction")
@ActionRegistration(displayName = "#CTL_AocLoisCalculsPermanentsAction")
@ActionReference(path = "Actions/AOC", position = 3)
public final class AocLoisCalculsPermanentsAction extends AbstractAocTopComponentOpenerAction {
    public AocLoisCalculsPermanentsAction() {
        super("CTL_AocLoisCalculsPermanentsAction", AocLignesEauCalculsPermanentsTopComponent.MODE, AocLignesEauCalculsPermanentsTopComponent.TOPCOMPONENT_ID);
    }

    @Override
    public Action createContextAwareInstance(Lookup actionContext) {
        return new AocLoisCalculsPermanentsAction();
    }
}
