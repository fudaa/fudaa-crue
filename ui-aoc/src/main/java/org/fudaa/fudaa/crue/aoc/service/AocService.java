package org.fudaa.fudaa.crue.aoc.service;

import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.aoc.exec.AocExecProcessor;
import org.fudaa.dodico.crue.aoc.projet.*;
import org.fudaa.dodico.crue.aoc.validation.AocCalculUsageUpdater;
import org.fudaa.dodico.crue.aoc.validation.AocValidationHelper;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.fudaa.crue.aoc.MessagesAoc;
import org.fudaa.fudaa.crue.aoc.action.AocFileFilter;
import org.fudaa.fudaa.crue.aoc.perspective.PerspectiveServiceAoc;
import org.fudaa.fudaa.crue.aoc.process.OpenAocCampagneProcess;
import org.fudaa.fudaa.crue.aoc.process.SaveAocCampagneProcess;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.*;
import org.fudaa.fudaa.crue.loader.EtudeAndScenarioChooser;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;

import java.awt.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.openide.util.NbBundle.getMessage;

/**
 * <table  border="1"  cellpadding="3" cellspacing="0"  summary="lookups">
 * <tr>
 * <th align="left">Lookup</th>
 * <th align="left">Commentaire</th>
 * <th align="left">Méthode utilisant le lookup</th>
 * </tr>
 * <tr>
 * <td>{@link AocCampagne}</td>
 * <td>	Le conteneur complet de la campagne</td>
 * <td><code>{@link #getCurrentAOC()}</code></td>
 * </tr>
 * <tr>
 * <tr>
 * <td>{@link org.fudaa.ctulu.CtuluLogGroup}</td>
 * <td>permet de connaître l'état de validation de la campagne en cours.<br>
 * Mis à jour par la méthode {@link #validateAll()}.</td>
 * <td><code>{@link #getCurrentValidationState()}</code></td>
 * </tr>
 * <tr>
 * <td>Boolean</td>
 * <td>Permet de connaître l'état "modifié" de la campagne</td>
 * <td><code>{@link #isModified()}</code></td>
 * </tr>
 * </table>
 *
 * @author deniger
 */
@ServiceProviders(value = {
    @ServiceProvider(service = AocService.class),
    @ServiceProvider(service = AocServiceContrat.class)})
public class AocService implements AocServiceContrat {
  /**
   * Propriété pour les listeners écoutant les modifications sur les loi ->calculs permanents
   */
  public static final String PROPERTY_LOI_CALCUL_PERMANENT = AocLoiCalculPermanent.class.getName();
  /**
   * Propriété pour les listeners écoutant les modifications sur les loi ->calculs Transitoire
   */
  private static final String PROPERTY_LOI_CALCUL_TRANSITOIRE = AocLoiCalculTransitoire.class.getName();
  /**
   * Propriété pour les listeners écoutant les modifications sur les loi stricklers
   */
  private static final String PROPERTY_LOI_STRICKLER = AocLoiStrickler.class.getName();
  /**
   * Propriété pour les listeners écoutant les modifications sur les param calc
   */
  private static final String PROPERTY_LOI_PARAM_CALC = AocParamCalc.class.getName();
  /**
   * Propriété pour les listeners écoutant les modifications sur les données de validation croisée.
   */
  private static final String PROPERTY_VALIDATION_CROISEE = AocValidationGroupe.class.getName();
  /**
   * Pour écouter les modifications au niveau du scénario
   * Doit être conservé en variable de classe pour éviter d'être garbagé
   */
  @SuppressWarnings({"FieldCanBeLocal", "unused"})
  private final Lookup.Result<EMHScenario> emhScenarioResult;
  /**
   * le service global de configuration ( les coeurs, options de l'application)
   */
  private final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);
  /**
   * Pour modifier le contenu du lookup de ce service
   */
  private final InstanceContent dynamicContent = new InstanceContent();
  /**
   * le lookup de la classe. Mettre à jour la doc principale (voir commentaire de la classe) si modification dans le contenu.
   */
  private final Lookup aocLookup = new AbstractLookup(dynamicContent);
  /**
   * Utilisé pour ouvrir le scénario de la campagne AOC
   */
  private final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);
  /**
   * Utilisé pour ouvrir l'étude de la campagne AOC
   */
  private final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  /**
   * pour fournir au topcomponent les données lois- mesures
   */
  private final LhptService lhptService = Lookup.getDefault().lookup(LhptService.class);
  /**
   * le service de lancement de runs. Utilisés pour configurer le lanceur d'une campagne
   */
  private final PostRunService postRunService = Lookup.getDefault().lookup(PostRunService.class);
  /**
   * les listeners qui écoutent les modifications sur le lhpt
   */
  private final Set<PropertyChangeListener> loiMesureListeners = new HashSet<>();
  /**
   * permet de valider toutes les données liées au scénario.
   */
  private AocValidationHelper aocValidationHelper;
  /**
   * Support pour envoi d'événements
   */
  private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

  public AocService() {
    emhScenarioResult = modellingScenarioService.getLookup().lookupResult(EMHScenario.class);
  }

  /**
   * @return le lookup du service.
   */
  @Override
  public Lookup getLookup() {
    return aocLookup;
  }

  public LhptService getLhptService() {
    return lhptService;
  }

  @Override
  public boolean isLhptLoaded() {
    return getLhptService().isLoaded();
  }

  /**
   * Chargement de données LHPT et ajout listener si nécessaire et non null
   *
   * @param listener listener a ajouter en tant que tel si non null
   * @return les données chargées.
   */
  public LhptService.LoiMesurePostState loadLoiMesureForCurrentScenario(final PropertyChangeListener listener) {
    if (isAocCampagneLoaded()) {
      final String nomScenario = getCurrentAOC().getNomScenario();
      final LhptService.LoiMesurePostState scenario = loadLoiMesurePostState(nomScenario, listener);
      if (scenario != null) {
        return scenario;
      }
    }
    return null;
  }

  private LhptService.LoiMesurePostState loadLoiMesurePostState(final String nomScenario, final PropertyChangeListener listener) {
    final ManagerEMHScenario scenario = projetService.getSelectedProject().getScenario(nomScenario);
    if (scenario != null) {
      if (listener != null) {
        loiMesureListeners.add(listener);
      }
      return lhptService.loadLoiMesure(scenario, listener, false);
    }
    return null;
  }

  /**
   * Met à jour les lookup avec la nouvelle campagne
   *
   * @param campagne la campagne à charger
   */
  private void setCampagne(final AocCampagne campagne) {

    //le projet/scenario de la campagne AOC doit être chargée:

    EMHProjet projectLoaded = modellingScenarioService.getSelectedProjet();
    final String newEtuPath = campagne.getEtudeChemin();
    File newEtuFile = null;
    if (newEtuPath != null) {
      newEtuFile = new File(newEtuPath).getAbsoluteFile();
      if (!newEtuFile.exists()) {
        newEtuFile = null;
      }
    }
    //on ferme le projet si différent
    if (projectLoaded != null) {
      final File etuFile = projectLoaded.getInfos().getEtuFile().getAbsoluteFile();
      //le projet doit être le meme que celui de la campagne
      if (!etuFile.equals(newEtuFile)) {
        projetService.close();
        final EMHScenario scenarioLoaded = modellingScenarioService.getScenarioLoaded();
        if (scenarioLoaded != null) {
          modellingScenarioService.unloadScenario();
        }
        projectLoaded = null;
      }
    }
    //on charge le nouveau projet si nécessaire
    if (projectLoaded == null && newEtuFile != null) {
      //on ouvre en lecture seule:
      projetService.load(newEtuFile, false);
      projectLoaded = projetService.getSelectedProject();
    }
    EMHScenario scenarioLoaded = null;
    if (projectLoaded != null) {
      //on ferme le scenario si différent
      scenarioLoaded = modellingScenarioService.getScenarioLoaded();
      if (scenarioLoaded != null && !scenarioLoaded.getNom().equals(campagne.getNomScenario())) {
        modellingScenarioService.unloadScenario();
        scenarioLoaded = null;
      }
      //on charge le nouveau scenario
      if (scenarioLoaded == null && campagne.getNomScenario() != null) {
        //attention charge les données en arrière plan....
        final ManagerEMHScenario managerEMHScenario = projectLoaded.getScenario(campagne.getNomScenario());
        if (managerEMHScenario != null) {
          scenarioLoaded = modellingScenarioService.loadScenario(projectLoaded, managerEMHScenario);
        }
      }
    }
    if (scenarioLoaded != null) {
      final LhptService.LoiMesurePostState loiMesurePostState = loadLoiMesurePostState(scenarioLoaded.getNom(),
          evt -> validateAll());
      if (loiMesurePostState != null) {
        aocValidationHelper = new AocValidationHelper(campagne, scenarioLoaded, loiMesurePostState.getLoiMesuresPost(),
            getCcm());
        //on ajoute les nouvelles valeurs
        dynamicContent.add(campagne);
        //etat non modifié
        setModified(false);
        //on valide la campagne
        validateAll();
      }
    }
    if (newEtuFile == null) {
      DialogHelper.showError(getMessage(MessagesAoc.class, "AocService.EtuFileNotFound", newEtuPath));
    } else if (scenarioLoaded == null) {
      DialogHelper.showError(getMessage(MessagesAoc.class, "AocService.ScenarioNotFound", newEtuPath, campagne.getNomScenario()));
    }
    //on active la perspective AOC à la fin
    EventQueue.invokeLater(() -> {
      final SelectedPerspectiveService service = Lookup.getDefault().lookup(SelectedPerspectiveService.class);
      service.activePerspective(PerspectiveEnum.AOC);
    });
  }

  public AocValidationHelper getAocValidationHelper() {
    return aocValidationHelper;
  }

  /**
   * @return le {@link org.fudaa.ctulu.CtuluLogGroup} du lookup
   */
  public CtuluLogGroup getCurrentValidationState() {
    return aocLookup.lookup(CtuluLogGroup.class);
  }

  /**
   * mise à jour de la validation de la campagne en cours
   */
  private void validateAll() {
    final CtuluLogGroup currentValidation = getCurrentValidationState();
    final CtuluLogGroup newValidation = aocValidationHelper.validate().createCleanGroup();
    removeIfNotNull(currentValidation);
    dynamicContent.add(newValidation);
  }

  /**
   * A utiliser pour indiquer que la campagne en cours est modifiée.
   */
  public void setModified() {
    setModified(true);
  }

  /**
   * @return true si campagne en cours est modifiée
   */
  public boolean isModified() {
    return Boolean.TRUE.equals(aocLookup.lookup(Boolean.class));
  }

  /**
   * Enregistre le caractère "modifié" de la campagne en cours dans le lookup.
   *
   * @param newValue true si la campagne en cours est modifié. false si enregistré
   */
  private void setModified(final boolean newValue) {
    final Boolean isModified = isModified();
    if (!ObjectUtils.equals(newValue, isModified)) {
      dynamicContent.remove(isModified);
      dynamicContent.add(newValue);
    }
    if (newValue) {
      validateAll();
    }
  }

  public boolean closeCurrent() {
    if (getCurrentAOC() != null) {
      final boolean modified = isModified();
      final Object showQuestionCancel;
      final String title = MessagesAoc.getMessage("CloseAoc.DialogTitle");
      //si modifie,l'utilisateur a la choix entre femer et sauver/fermer sans sauver et annuler.
      if (modified) {
        final String message = MessagesAoc.getMessage("CloseModifiedAoc.DialogMessage");
        showQuestionCancel = DialogHelper.showQuestionCancel(title, message);
      } else {
        //pas de modification: on demande simplement confirmation pour la fermeture
        final String message = MessagesAoc.getMessage("CloseAoc.DialogMessage");
        final boolean close = DialogHelper.showQuestion(title, message);
        //on transforme la réponse pour être compatible avec le cas "modifie".
        showQuestionCancel = close ? NotifyDescriptor.NO_OPTION : NotifyDescriptor.CANCEL_OPTION;
      }
      //l'utilisateur annule la fermeture, on ne fait rien
      if (DialogHelper.isCancelOption(showQuestionCancel)) {
        return false;
      }
      //on sauvegarde si modifie et si l'utilisateur a repondu "yes".
      if (modified && DialogHelper.isYesOption(showQuestionCancel) && !saveCurrent()) {
        //Si erreur lors de la sauvegarde on ne ferme pas.
        return false;
      }
      //on ferme
      unsetCampagne();
    }
    return true;
  }

  /**
   * @return true si {@link #getCurrentValidationState} ne contient pas d'erreurs
   * @see #getCurrentValidationState()
   */
  private boolean isValid() {
    return getCurrentValidationState() == null || !getCurrentValidationState().containsError();
  }

  /**
   * Nettoyage du lookup du service
   */
  private void unsetCampagne() {
    final AocCampagne current = getCurrentAOC();
    //on nettoie le lookup du serive
    if (current != null) {
      clearLhptListener();
    }
    aocValidationHelper = null;
    removeIfNotNull(getCurrentValidationState());
    removeIfNotNull(isModified());
    removeIfNotNull(current);
  }

  private void removeIfNotNull(final Object object) {
    if (object != null) {
      dynamicContent.remove(object);
    }
  }

  /**
   * Méthode interne pour désinscrire tous les listeners sur le fichier LHPT.
   */
  private void clearLhptListener() {
    final String nomScenario = getCurrentAOC().getNomScenario();
    if (projetService.getSelectedProject() != null) {
      final ManagerEMHScenario scenario = projetService.getSelectedProject().getScenario(nomScenario);
      if (scenario != null) {
        for (final PropertyChangeListener loiMesureListener : loiMesureListeners) {
          lhptService.unloadLoiMesure(scenario, loiMesureListener);
        }
      }
    }
    loiMesureListeners.clear();
  }

  /**
   * @return true si campagne chargée
   */
  @Override
  public boolean isAocCampagneLoaded() {
    return getCurrentAOC() != null;
  }

  public AocCampagne getCurrentAOC() {
    return aocLookup.lookup(AocCampagne.class);
  }

  public boolean saveCurrent() {
    final AocConfigService configService = Lookup.getDefault().lookup(AocConfigService.class);
    if (configService.isModified()) {
      final CtuluLog configSaveLog = configService.save();
      if (configSaveLog != null && configSaveLog.containsErrorOrSevereError()) {
        LogsDisplayer.displayError(configSaveLog, MessagesAoc.getMessage("SaveAocUi.ProcessName"));
      }
    }
    if (getCurrentAOC() != null && isModified()) {
      final CtuluLogGroup newValidation = aocValidationHelper.validate(false).createCleanGroup();
      if (newValidation.containsError() || newValidation.containsFatalError()) {
        LogsDisplayer.displayError(newValidation, MessagesAoc.getMessage("AocSaveAbort.CampagneWithError"));
        return false;
      }
      final SaveAocCampagneProcess process = new SaveAocCampagneProcess(getCurrentAOC());
      CrueProgressUtils.showProgressDialogAndRun(process, MessagesAoc.getMessage("SaveAoc.ProcessName"));
      setModified(Boolean.FALSE);
      return true;
    }
    return true;
  }

  public void reloadCurrent() {
    final AocCampagne currentAoc = getCurrentAOC();
    if (currentAoc != null) {
      final File aocFile = currentAoc.getAocFile();
      unsetCampagne();
      if (aocFile.exists()) {
        open(aocFile);
      }
    }
  }

  public void open(final File toOpen) {
    //on va fermer la campagne en cours
    if (getCurrentAOC() != null && !closeCurrent()) {
      //si refusé par l'utilisateur, on annule l'ouverture du nouveau fichier
      return;
    }
    final OpenAocCampagneProcess process = new OpenAocCampagneProcess(toOpen);
    final AocCampagne campagne = CrueProgressUtils.showProgressDialogAndRun(process,
        MessagesAoc.getMessage(
            "OpenAoc.ProcessName", toOpen.getName()));
    if (campagne != null) {
      setCampagne(campagne);
    }
    if (!isValid()) {
      LogsDisplayer.displayError(getCurrentValidationState(), MessagesAoc.getMessage("Load.CampagneWithError"));
    }
  }

  public void executeCampagne() {
    //si la configuration du site pas correcte: on ne lance pas
    if (!configurationManagerService.isConfigValidShowMessage()) {
      return;
    }
    //validation et sauvegarde de la campagne. si pas corret: on ne lance pas
    if (!saveCurrent()) {
      return;
    }
    final AocExecProcessor executor = new AocExecProcessor(configurationManagerService.getCoeurManager(),
        configurationManagerService.getConnexionInformation(),
        postRunService.getCalculCrueRunnerManagerOtfa());

    final LaunchAocCampagneRunner process = new LaunchAocCampagneRunner(executor, getCurrentAOC().getAocFile());
    process.go();
  }

  /**
   * @return la configuration à utiliser: il s'agit de la plus récente.
   */
  public CoeurConfig getCoeurConfigToUseForNumericalParameters() {
    return configurationManagerService.getCoeurManager().getCoeurConfigDefault(configurationManagerService.getCoeurManager().getHigherGrammaire());
  }

  public void createNew() { //on ne peut pas fermer la campagne en cours: on sort....
    if (!closeCurrent()) {
      return;
    }
    final File home = configurationManagerService.getDefaultDataHome();
    final FileChooserBuilder builder = new FileChooserBuilder(getClass()).setTitle(MessagesAoc.getMessage(
        "createAoc.FileChooser")).setDefaultWorkingDirectory(
        home).setApproveText(MessagesAoc.getMessage("createAoc.FileChooser"));
    builder.setFileFilter(AocFileFilter.create());
    File selectedAocFile = builder.showSaveDialog();
    if (selectedAocFile != null) {
      selectedAocFile = AocFileFilter.addAocExtensionIfNeeded(selectedAocFile);
      final AocCampagne campagne = new AocCampagne(
          configurationManagerService.getCoeurManager().getCoeurConfigDefault(configurationManagerService.getCoeurManager().getHigherGrammaire()).getCrueConfigMetier());
      campagne.setAocFile(selectedAocFile);

      final EtudeAndScenarioChooser etudeAndScenarioChooser = new EtudeAndScenarioChooser();
      etudeAndScenarioChooser.setScenarioNameFilter(ValidationPatternHelper.getScenarioFilterForAoc());
      final Pair<String, String> etudeAndScenario = etudeAndScenarioChooser.choose(
          //taille max acceptée
          ValidationPatternHelper.TAILLE_MAX_NOM_SCENARIO_AOC
          //le message d'erreur tooltip
          , MessagesAoc.getMessage("createAoc.tailleMaxScenarioErrorMessage", Integer.toString(ValidationPatternHelper.TAILLE_MAX_NOM_SCENARIO_AOC))
      );
      if (etudeAndScenario != null) {
        campagne.setEtudeChemin(etudeAndScenario.first);
        campagne.setNomScenario(etudeAndScenario.second);
        setCampagne(campagne);
        Lookup.getDefault().lookup(PerspectiveServiceAoc.class).setState(PerspectiveState.MODE_EDIT);
        setModified(true);
      }
    }
  }

  /**
   * @param loisCalculsPermanents les nouvelles lois->CalculPermanents
   */
  public void setLoisCalculsParmanents(final List<AocLoiCalculPermanent> loisCalculsPermanents) {
    if (getCurrentAOC() != null) {
      getCurrentAOC().getDonnees().getLoisCalculsPermanents().setClonedLois(loisCalculsPermanents);
      //on met à jour les listes de calculs et les validations croisées
      final AocCalculUsageUpdater usageUpdater = new AocCalculUsageUpdater();
      usageUpdater.updateListCalculFromLoisCalcul(getCurrentAOC().getDonnees(), getAocValidationHelper());
      usageUpdater.updateValidationCroiseeFromLoisCalcul(getCurrentAOC().getDonnees());
      propertyChangeSupport.firePropertyChange(PROPERTY_LOI_CALCUL_PERMANENT, null, loisCalculsPermanents);
      setModified();
    }
  }

  /**
   * @param loiCalculTransitoires les nouvelles AocLoiCalculTransitoire
   */
  public void setLoisCalculsTransitoires(final List<AocLoiCalculTransitoire> loiCalculTransitoires) {
    if (getCurrentAOC() != null) {
      getCurrentAOC().getDonnees().getLoisCalculsTransitoires().setClonedLois(loiCalculTransitoires);
      propertyChangeSupport.firePropertyChange(PROPERTY_LOI_CALCUL_TRANSITOIRE, null, loiCalculTransitoires);
      setModified();
    }
  }

  public void setLoisStricker(final List<AocLoiStrickler> loisStricker) {
    if (getCurrentAOC() != null) {
      getCurrentAOC().getDonnees().getLoisStrickler().setClonedLois(loisStricker);
      propertyChangeSupport.firePropertyChange(PROPERTY_LOI_STRICKLER, null, loisStricker);
      setModified();
    }
  }

  public CrueConfigMetier getCcm() {
    return projetService.getCcm();
  }

  public void setParaCalc(final List<AocParamCalc> aocParamCalcs) {
    if (getCurrentAOC() != null) {
      getCurrentAOC().getDonnees().getListeCalculs().setClonedParams(aocParamCalcs);
      propertyChangeSupport.firePropertyChange(PROPERTY_LOI_PARAM_CALC, null, aocParamCalcs);
      setModified();
    }
  }

  public void setValidationGroupes(final List<AocValidationGroupe> validationGroupes) {
    if (getCurrentAOC() != null) {
      getCurrentAOC().getDonnees().getValidationCroisee().setClonedGroupes(validationGroupes);
      propertyChangeSupport.firePropertyChange(PROPERTY_VALIDATION_CROISEE, null, validationGroupes);
      setModified();
    }
  }

  public PropertyChangeSupport getPropertyChangeSupport() {
    return propertyChangeSupport;
  }
}
