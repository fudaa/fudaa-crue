package org.fudaa.fudaa.crue.aoc.importer;

import org.fudaa.fudaa.crue.aoc.nodes.AocLigneEauCalculPermanentNode;
import org.fudaa.fudaa.crue.common.helper.AbstractFileImporterProgressRunnable;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Handler pour importer un fichier csv ou excel.
 */
public class AocLoiCalculPermanentFileImporter extends AbstractFileImporterProgressRunnable implements ProgressRunnable<List<AocLigneEauCalculPermanentNode>> {
  /**
   * les calculs du scenario sélectionné
   */
  private final String[] calculs;
  /**
   * les lois
   */
  private final String[] lois;
  /**
   * les commentaires
   */
  private final Map<String, String> commentByCalculId;

  /**
   * @param file le fichier à importer
   * @param calculs les calculs disponibles
   * @param lois les lois disponibles
   */
  public AocLoiCalculPermanentFileImporter(File file, String[] calculs, String[] lois, Map<String, String> commentByCalculId) {
    super(file);
    this.calculs = calculs;
    this.lois = lois;
    this.commentByCalculId = commentByCalculId;
  }

  @Override
  public List<AocLigneEauCalculPermanentNode> run(ProgressHandle progressHandle) {
    String[][] values = null;
    if (file != null) {
      values = readFile();
    }
    if (values == null) {
      return null;
    }
    return AocLoiCalculPermanentNodesImporter.extractLoiCalculPermanentNode(calculs, lois, commentByCalculId, values);
  }
}
