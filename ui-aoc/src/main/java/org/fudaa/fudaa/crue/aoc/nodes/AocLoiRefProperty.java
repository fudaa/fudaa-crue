package org.fudaa.fudaa.crue.aoc.nodes;

import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.openide.nodes.Sheet;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Propriete pour l'affichage des lois
 *
 * @author deniger
 */
public class AocLoiRefProperty extends PropertySupportReflection<String> {
  /**
   * @param node doit etre de type AbstractNodeFirable
   * @param instance l'objet a modifier
   */
  private AocLoiRefProperty(NodeWithLoisContrat node, Object instance) throws NoSuchMethodException {
    super((AbstractNodeFirable) node, instance, String.class, "getLoiRef", "setLoiRef");
    setTags(node.getLois());
  }

  public static void createLoiProperty(NodeWithLoisContrat node, Sheet.Set set, Object abstractLoiCalcul) {
    try {
      AocLoiRefProperty propertyLois = new AocLoiRefProperty(node, abstractLoiCalcul);
      //important pour retrouver la valeur par la suite
      propertyLois.setName(AocNodeHelper.PROP_LOI);
      propertyLois.setDisplayName(AocNodeHelper.getLoiDisplay());
      PropertyCrueUtils.configureNoCustomEditor(propertyLois);
      set.put(propertyLois);
    } catch (NoSuchMethodException e) {
      Logger.getLogger(AocLoiRefProperty.class.getName()).log(Level.SEVERE, "can't find method", e);
    }
  }

  public static void createLigneEauProperty(NodeWithLoisContrat node, Sheet.Set set, Object abstractLoiCalcul) {
    try {
      AocLoiRefProperty propertyLois = new AocLoiRefProperty(node, abstractLoiCalcul);
      //important pour retrouver la valeur par la suite
      propertyLois.setName(AocNodeHelper.PROP_LOI);
      propertyLois.setDisplayName(AocNodeHelper.getLigneEauDisplay());
      PropertyCrueUtils.configureNoCustomEditor(propertyLois);
      set.put(propertyLois);
    } catch (NoSuchMethodException e) {
      Logger.getLogger(AocLoiRefProperty.class.getName()).log(Level.SEVERE, "can't find method", e);
    }
  }
}
