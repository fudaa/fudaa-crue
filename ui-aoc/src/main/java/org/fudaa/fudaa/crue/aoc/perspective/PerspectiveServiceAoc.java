package org.fudaa.fudaa.crue.aoc.perspective;

import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.fudaa.crue.aoc.AocCampagneTopComponent;
import org.fudaa.fudaa.crue.aoc.service.AocService;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.UserSaveAnswer;
import org.fudaa.fudaa.crue.common.services.AbstractPerspectiveService;
import org.fudaa.fudaa.crue.common.services.PerspectiveService;
import org.fudaa.fudaa.crue.common.services.PerspectiveState;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Le service perspective de AOC.
 *
 * @author Fred Deniger
 */
@ServiceProviders(value = {
    @ServiceProvider(service = PerspectiveServiceAoc.class),
    @ServiceProvider(service = PerspectiveService.class)})
public final class PerspectiveServiceAoc extends AbstractPerspectiveService implements LookupListener {
  private final Set<String> components = Collections.unmodifiableSet(new HashSet<>(Collections.singletonList(
      AocCampagneTopComponent.TOPCOMPONENT_ID)));
  @SuppressWarnings("FieldCanBeLocal")
  private final Result<AocCampagne> lookupResult;
  @SuppressWarnings("FieldCanBeLocal")
  private final Result<Boolean> lookupDirtyResult;
  private final AocService aocService = Lookup.getDefault().lookup(AocService.class);

  /**
   * Ajoute des listener sur le service {@link AocService} pour réagir au (dé)chargement d'un campagne et aux modifications de la
   * campagne.
   */
  public PerspectiveServiceAoc() {
    super(PerspectiveEnum.AOC);
    //ajoutes des listeners
    //campagne ouverte ?
    lookupResult = aocService.getLookup().lookupResult(AocCampagne.class);
    lookupResult.addLookupListener(this);
    //campagne modifiée?
    lookupDirtyResult = aocService.getLookup().lookupResult(Boolean.class);
    lookupDirtyResult.addLookupListener(ev -> {
    });
    setState(PerspectiveState.MODE_READ_ONLY_ALWAYS);
  }

  @Override
  public String getPathForViewsAction() {
    return "Actions/AOC";
  }

  /**
   * @param ev event
   */
  @Override
  public void resultChanged(LookupEvent ev) {
    if (aocService.getCurrentAOC() != null) {
      setState(PerspectiveState.MODE_READ);
      setDirty(aocService.isModified());
    } else {
      setState(PerspectiveState.MODE_READ_ONLY_TEMP);
      setDirty(false);
    }
  }

  /**
   * @return true
   */
  @Override
  public boolean supportEdition() {
    return true;
  }

  /**
   * @return true si pas de modification sur la campagne ouverte. Sinon demande confirmation au service {@link AocService#closeCurrent()
   *     }.
   * @see AocService#closeCurrent()
   */
  @Override
  public boolean closing() {
    return !isDirty() || aocService.closeCurrent();
  }

  /**
   * @param newState nouvel etat de la perspective
   * @return true dans presque tous les cas. Si l'utilisateur veut repasser en mode read et si la campagne est modifiée, demande
   *     confirmation à l'utilisateur.
   */
  @Override
  protected boolean canStateBeModifiedTo(PerspectiveState newState) {
    //pas de campagne: on doit rester en mode read only temp
    if (aocService.getCurrentAOC() == null) {
      return PerspectiveState.MODE_READ_ONLY_TEMP.equals(newState);
    }
    //pas de probleme pour le mode edit
    if (PerspectiveState.MODE_EDIT.equals(newState)) {
      return true;
    }
    //pour passer en mode read, on doit vérifier si la campagne en cours est modifiée.
    if (PerspectiveState.MODE_READ.equals(newState)) {
      //Si oui, on demande confirmation à l'utilisateur pour sauvegarde et fermeture
      if (aocService.isModified()) {
        UserSaveAnswer saveAnswer = DialogHelper.confirmSaveOrNot();
        if (UserSaveAnswer.CANCEL.equals(saveAnswer)) {
          //fermeture annulée par l'utilsateur:
          return false;
        }
        if (UserSaveAnswer.SAVE.equals(saveAnswer)) {
          //l'utilisateur veut une sauvegarde: on renvoie le resultat de l'opération ( si échec pas de fermeture).
          return aocService.saveCurrent();
        } else {
          //l'utilisateur ne veut pas sauvegarder: on recharge simplement la campagne pour être bien aligné avec les fichiers persistés
          aocService.reloadCurrent();
        }
      }
      //si non, pas de probleme.
      return true;
    }
    return false;
  }


  /**
   * @return true.
   */
  @Override
  public boolean activate() {
    return true;
  }

  @Override
  public Set<String> getDefaultTopComponents() {
    return components;
  }
}
