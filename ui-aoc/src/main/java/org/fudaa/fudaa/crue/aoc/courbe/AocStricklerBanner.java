package org.fudaa.fudaa.crue.aoc.courbe;

import org.fudaa.ctulu.CtuluRange;
import org.fudaa.dodico.crue.aoc.projet.AocLoiStrickler;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGHorizontalBanner;
import org.fudaa.ebli.courbe.EGRepere;
import org.fudaa.ebli.trace.TraceBox;
import org.fudaa.fudaa.crue.aoc.nodes.AocLoiStricklerNode;
import org.fudaa.fudaa.crue.aoc.nodes.AocLoiStricklerView;
import org.fudaa.fudaa.crue.loi.loiff.LoiUiController;

import javax.swing.*;
import java.awt.*;

/**
 * POur affichier les graduations sous forme de noms des strickler
 *
 * @author deniger
 */
public class AocStricklerBanner implements EGHorizontalBanner {
    private final AocLoiStricklerView view;
    private final LoiUiController loiUiController;
    private final TraceBox traceBox;

    public AocStricklerBanner(AocLoiStricklerView view, LoiUiController loiUiController) {
        this.view = view;
        this.loiUiController = loiUiController;
        loiUiController.getAxeX().setAxisIterator(new CustomTickIterator());
        loiUiController.getAxeX().setSpecificFormat(null);
        traceBox = new TraceBox();
        traceBox.setVertical(true);
        traceBox.setVPosition(SwingConstants.TOP);
        traceBox.setHPosition(SwingConstants.CENTER);
        traceBox.setDrawFond(false);
        traceBox.setDrawBox(false);
    }

    private AocLoiStrickler getValue(int i) {
        return ((AocLoiStricklerNode) view.getExplorerManager().getRootContext().getChildren().getNodeAt(i)).getMainValue();
    }

    private void initBox() {
        EGAxeHorizontal axeHorizontal = loiUiController.getAxeX();
        traceBox.setColorText(axeHorizontal.getLineColor());
        traceBox.setDrawFond(false);
        traceBox.setDrawBox(false);
    }

    private int getNbValues() {
        return view.getExplorerManager().getRootContext().getChildren().getNodesCount();
    }

    @Override
    public void dessine(Graphics2D graphics2D, EGRepere egRepere) {
        Font old = graphics2D.getFont();
        initBox();
        graphics2D.setFont(loiUiController.getAxeX().getFont());
        int yMin = 3+ egRepere.getH() - getHeightNeeded(graphics2D) - loiUiController.getAxeX().getBottomHeightNeeded(graphics2D);
        int nbValues = getNbValues();
        CtuluRange range = egRepere.getXAxe().getRange().intersectWith(new CtuluRange(0, nbValues - 1));
        if (!range.isNill()) {
            int xMin = (int) Math.max(0, range.getMin());
            int xMax = (int) Math.min(nbValues - 1d, range.getMax());
            for (int i = xMin; i <= xMax; i++) {
                int iEcran = egRepere.getXEcran(i);
                traceBox.paintBox(graphics2D, iEcran, yMin, getValue(i).getLoiRef());
            }
        }
        graphics2D.setFont(old);
    }

    @Override
    public int getHeightNeeded(Graphics2D graphics2D) {
        Font old = graphics2D.getFont();
        Font font = loiUiController.getAxeX().getFont();
        if (font == null) {
            font = old;
        }
        FontMetrics fontMetrics = graphics2D.getFontMetrics(font);
        initBox();
        int h = 0;
        int nbValues = getNbValues();
        for (int i = 0; i < nbValues; i++) {
            h = Math.max(h, fontMetrics.stringWidth(getValue(i).getLoiRef()));
        }
        graphics2D.setFont(old);
        return 3+ h + traceBox.getTotalHeightMargin();
    }
}
