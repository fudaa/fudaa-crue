package org.fudaa.fudaa.crue.aoc;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.NbSheetCustom;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Proprietés des noeuds sélectionnés.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.aoc//AocPropertiesTopComponent//EN",
    autostore = false)
@TopComponent.Description(preferredID = AocPropertiesTopComponent.TOPCOMPONENT_ID,
    persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "aoc-topLeft", openAtStartup = false)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.aoc.AocPropertiesTopComponent")
@TopComponent.OpenActionRegistration(displayName = "#CTL_AocPropertiesAction",
    preferredID = AocPropertiesTopComponent.TOPCOMPONENT_ID)
@ActionReference(path = "Menu/Window/AOC", position = 9, separatorBefore = 8)
public final class AocPropertiesTopComponent extends NbSheetCustom {
  public static final String TOPCOMPONENT_ID = "AocPropertiesTopComponent";

  public AocPropertiesTopComponent() {
    super(true, PerspectiveEnum.AOC);
    setName(NbBundle.getMessage(AocPropertiesTopComponent.class, "CTL_AocPropertiesAction"));
    setToolTipText(NbBundle.getMessage(AocPropertiesTopComponent.class, "HINT_AocPropertiesAction"));
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getTopComponentHelpCtxId("vueProprietes", PerspectiveEnum.AOC));
  }

  @SuppressWarnings("unused")
  void writeProperties(java.util.Properties p) {
    p.setProperty("version", "1.0");
  }

  @SuppressWarnings("unused")
  void readProperties(java.util.Properties p) {
  }
}
