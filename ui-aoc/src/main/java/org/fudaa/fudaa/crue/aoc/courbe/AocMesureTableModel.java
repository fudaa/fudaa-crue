package org.fudaa.fudaa.crue.aoc.courbe;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGTableGraphePanel;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiCourbeModel;
import org.fudaa.fudaa.crue.loi.common.DefaultLoiTableModel;

/**
 * @author deniger
 */
public class AocMesureTableModel extends DefaultLoiTableModel {
  private String newProfil;
  private Double newValue;
  private final String profilColName = BusinessMessages.getString("pk.property");
  private boolean updating;

  public AocMesureTableModel(EGTableGraphePanel graphePanel) {
    super(graphePanel);
  }

  private boolean isCurrentLoiSectionZ() {
    final EGCourbe courbe = getCourbe();
    if (courbe == null) {
      return false;
    }
    AbstractLoiCourbeModel model = (AbstractLoiCourbeModel) courbe.getModel();
    return EnumTypeLoi.LoiSectionsZ.equals(model.getConfigLoi().getTypeLoi());
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    if (isCurrentLoiSectionZ() && isAddingValuesRow(rowIndex)) {
      if (columnIndex == xColIndex) {
        return newProfil;
      }
      if (columnIndex == yColIndex) {
        return newValue == null ? null : getY(newValue);
      }
    }

    if (isCurrentLoiSectionZ() && !isAddingValuesRow(rowIndex) && columnIndex == xColIndex) {
      final EGCourbe courbe = getCourbe();
      AbstractLoiCourbeModel model = (AbstractLoiCourbeModel) courbe.getModel();
      LHPTLoiSectionZCourbeModel tfModel = (LHPTLoiSectionZCourbeModel) model;
      return tfModel.getLoiLine(rowIndex).getProfil();
    }
    return super.getValueAt(rowIndex, columnIndex);
  }

  @Override
  public String getColumnName(int column) {
    if (column == xColIndex && isCurrentLoiSectionZ()) {
      return profilColName;
    }
    return super.getColumnName(column);
  }

  @Override
  public Class getColumnClass(int columnIndex) {
    if (columnIndex == xColIndex && isCurrentLoiSectionZ()) {
      return String.class;
    }
    return super.getColumnClass(columnIndex);
  }

  @Override
  public void setValueAt(Object value, int rowIndex, int columnIndex) {
    if (updating) {
      return;
    }
    final boolean currentLoiSectionZ = isCurrentLoiSectionZ();
    if (currentLoiSectionZ && rowIndex == getCourbe().getModel().getNbValues()) {
      if (columnIndex == xColIndex) {
        newProfil = (String) value;
      }
      if (columnIndex == yColIndex) {
        //noinspection ConstantConditions
        newValue = (Double) value;
      }
      if (newValue != null && newProfil != null) {
        updating = true;
        LHPTLoiSectionZCourbeModel model = (LHPTLoiSectionZCourbeModel) getCourbe().getModel();
        model.addLine(newProfil, newValue);
        fireTableRowsInserted(0, rowIndex);
        fireTableDataChanged();
        newValue = null;
        newProfil = null;
        updating = false;
      }
    } else if (currentLoiSectionZ && columnIndex == xColIndex) {

      LHPTLoiSectionZCourbeModel model = (LHPTLoiSectionZCourbeModel) getCourbe().getModel();
      String profil = (String) value;
      Double pos = model.getProfilPosition(profil);
      final AocLoiLine loiLine = model.getLoiLine(rowIndex);
      loiLine.setProfil(profil);
      if (pos == null) {
        loiLine.getPtEvolutionFF().setAbscisse(0);
      } else {
        loiLine.getPtEvolutionFF().setAbscisse(pos);
      }
      model.profilUpdated();
      fireTableDataChanged();
    } else {
      super.setValueAt(value, rowIndex, columnIndex);
    }
  }
}
