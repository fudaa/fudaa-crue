package org.fudaa.fudaa.crue.aoc.service;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.aoc.validation.LHPTContentValidator;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.io.CrueFileFormatBuilderLHPT;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierLHPTSupport;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

/**
 * Classe permettant de sauvegarder le contenu du fichier LHPT.
 */
public class LhptSaver implements ProgressRunnable<CtuluLogGroup> {
    private final LhptService service;

    public LhptSaver(LhptService service) {
        this.service = service;
    }

    @Override
    public CtuluLogGroup run(ProgressHandle progressHandle) {
        final List<LhptService.LoiMesurePostState> modifiedLoiMesurePostState = service.getModifiedLoiMesurePostState();
        CtuluLogGroup group = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
        progressHandle.switchToIndeterminate();
        for (LhptService.LoiMesurePostState loiMesurePostState : modifiedLoiMesurePostState) {
            CtuluLogGroup log = save(loiMesurePostState, progressHandle);
            group.addGroup(log);
        }
        boolean modifed = new FichierLHPTSupport(service.getProjetService().getSelectedProject()).updateLHPTFilesInProject();
        if (modifed) {
            ((EMHProjetServiceImpl) service.getProjetService()).reload("LHPT", false);
        }
        return group;
    }

    private CtuluLogGroup save(LhptService.LoiMesurePostState loiMesurePostState, ProgressHandle progressHandle) {
        LHPTContentValidator validator = new LHPTContentValidator();

        progressHandle.setDisplayName(loiMesurePostState.scenario.getNom());
        final EMHProjet currentProject = service.getProjetService().getSelectedProject();

        final CtuluLogGroup logGroup = validator
                .valid(loiMesurePostState.loiMesuresPost, new TreeSet<>(Arrays.asList(loiMesurePostState.getSectionsSortedByName())),
                        loiMesurePostState.scenario.getNom(),
                        currentProject.getPropDefinition());

        final File lhptFile = FichierLHPTSupport.getLHPTFile(currentProject, loiMesurePostState.scenario);
        CtuluLog log = logGroup.createNewLog(loiMesurePostState.scenario.getNom());
        log.setResource(lhptFile.getAbsolutePath());
        final boolean written = new CrueFileFormatBuilderLHPT().getFileFormat(currentProject.getCoeurConfig())
                .writeMetierDirect(loiMesurePostState.loiMesuresPost, lhptFile, log, currentProject.getCoeurConfig().getCrueConfigMetier());
        if (!written) {
            log.addSevereError("aoc.lhptFile.notWritten", lhptFile.getName());
        }
        return logGroup;
    }
}
