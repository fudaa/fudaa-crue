package org.fudaa.fudaa.crue.aoc;

import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class MessagesAoc {

  public static String getMessage(String code) {
    return NbBundle.getMessage(MessagesAoc.class, code);
  }

  public static String getMessage(String code, String value) {
    return NbBundle.getMessage(MessagesAoc.class, code, value);
  }
}
