package org.fudaa.fudaa.crue.aoc.courbe;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.crue.aoc.projet.AocLoiStrickerValue;
import org.fudaa.dodico.crue.aoc.projet.AocLoiStrickler;
import org.fudaa.dodico.crue.config.ccm.ItemContentAbstract;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.courbe.EgModelLabelNamed;
import org.fudaa.ebli.palette.BPaletteInfo;
import org.fudaa.fudaa.crue.aoc.nodes.AocLoiStricklerNode;
import org.fudaa.fudaa.crue.aoc.nodes.AocLoiStricklerView;
import org.fudaa.fudaa.crue.loi.common.CourbeModelWithKey;

import java.awt.*;
import java.util.Map;

/**
 * @author deniger
 */
public class AocStrickerCourbeModel implements EGModel, EgModelLabelNamed, CourbeModelWithKey {
  private final AocLoiStrickerValue value;
  private final AocLoiStricklerView view;

  public AocStrickerCourbeModel(AocLoiStricklerView view, AocLoiStrickerValue value) {
    this.view = view;
    this.value = value;
  }

  @Override
  public Object getKey() {
    return value;
  }

  @Override
  public ItemContentAbstract getVariableAxeY() {
    return null;
  }

  @Override
  public ItemContentAbstract getVariableAxeX() {
    return null;
  }

  @Override
  public boolean isRemovable() {
    return true;
  }

  @Override
  public boolean isDuplicatable() {
    return false;
  }

  @Override
  public int getNbValues() {
    return view.getExplorerManager().getRootContext().getChildren().getNodesCount();
  }

  @Override
  public boolean isSegmentDrawn(int i) {
    return true;
  }

  @Override
  public boolean isPointDrawn(int i) {
    return true;
  }

  protected AocLoiStrickler getValue(int i) {
    return ((AocLoiStricklerNode) view.getExplorerManager().getRootContext().getChildren().getNodeAt(i)).getMainValue();
  }

  @Override
  public String getPointLabel(int i) {
    return getValue(i).getLoiRef();
  }

  @Override
  public Color getSpecificColor(int idx) {
    return null;
  }

  @Override
  public double getX(int i) {
    return i;
  }

  @Override
  public double getY(int i) {
    return value.getValue(getValue(i));
  }

  @Override
  public double getXMin() {
    return 0;
  }

  @Override
  public double getXMax() {
    return getNbValues();
  }

  @Override
  public double getYMin() {
    double res = 0;
    final int nbValues = getNbValues();
    if (nbValues > 0) {
      res = getY(0);
      for (int i = 1; i < nbValues; i++) {
        res = Math.min(res, getY(i));
      }
    }
    return res;
  }

  @Override
  public double getYMax() {
    double res = 0;
    final int nbValues = getNbValues();
    if (nbValues > 0) {
      res = getY(0);
      for (int i = 1; i < nbValues; i++) {
        res = Math.max(res, getY(i));
      }
    }
    return res;
  }

  @Override
  public boolean isModifiable() {
    return false;
  }

  @Override
  public boolean isXModifiable() {
    return false;
  }

  @Override
  public boolean setValue(int i, double v, double v1, CtuluCommandContainer ctuluCommandContainer) {
    return false;
  }

  @Override
  public boolean addValue(double v, double v1, CtuluCommandContainer ctuluCommandContainer) {
    return false;
  }

  @Override
  public boolean addValue(double[] doubles, double[] doubles1, CtuluCommandContainer ctuluCommandContainer) {
    return false;
  }

  @Override
  public boolean removeValue(int i, CtuluCommandContainer ctuluCommandContainer) {
    return false;
  }

  @Override
  public boolean removeValue(int[] ints, CtuluCommandContainer ctuluCommandContainer) {
    return false;
  }

  @Override
  public boolean deplace(int[] ints, double v, double v1, CtuluCommandContainer ctuluCommandContainer) {
    return false;
  }

  @Override
  public boolean setValues(int[] ints, double[] doubles, double[] doubles1, CtuluCommandContainer ctuluCommandContainer) {
    return false;
  }

  @Override
  public String getTitle() {
    return value.geti18n();
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  @Override
  public boolean setTitle(String s) {
    return false;
  }

  @Override
  public void fillWithInfo(BPaletteInfo.InfoData infoData, CtuluListSelectionInterface ctuluListSelectionInterface) {
//rien a faire
  }

  @Override
  public EGModel duplicate() {
    return null;
  }

  @Override
  public void viewGenerationSource(Map map, CtuluUI ctuluUI) {
    //rien a faire
  }

  @Override
  public boolean isGenerationSourceVisible() {
    return false;
  }

  @Override
  public void replayData(EGGrapheModel egGrapheModel, Map map, CtuluUI ctuluUI) {
//rien a faire
  }

  @Override
  public boolean isReplayable() {
    return false;
  }

  @Override
  public Object savePersistSpecificDatas() {
    return null;
  }

  @Override
  public void restoreFromSpecificDatas(Object o, Map map) {
//rien a faire
  }

  @Override
  public boolean useSpecificIcon(int i) {
    return false;
  }

  @Override
  public int[] getInitRows() {
    return new int[0];
  }

  @Override
  public String getLabelColumnName() {
    return "";
  }
}
