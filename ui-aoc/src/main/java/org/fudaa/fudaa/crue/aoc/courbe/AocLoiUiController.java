package org.fudaa.fudaa.crue.aoc.courbe;

import org.fudaa.fudaa.crue.loi.loiff.LoiUiController;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Surcharge de la classe {@link LoiUiController} pour prendre en compte les loi LoiTF
 *
 * @author fred deniger
 */
public class AocLoiUiController extends LoiUiController {
    public AocLoiUiController() {
        super(new HashSet<>(Arrays.asList("SELECT", "SUIVI", "SUIVIALL", "MOVE_POINT", "ADD_POINT", "SIMPLIFY")));
    }
}
