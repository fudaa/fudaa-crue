package org.fudaa.fudaa.crue.aoc.importer;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluDoubleParser;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.crue.aoc.projet.AocValidationGroupe;
import org.fudaa.fudaa.crue.aoc.nodes.AocValidationGroupeNode;
import org.fudaa.fudaa.crue.common.swing.CommonGuiLib;
import org.fudaa.fudaa.crue.common.view.DefaultEditableOutlineViewEditor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Methodes utilitaires pour importer des noeuds AocValidationGroupeNode
 */
public class AocValidationGroupeNodesImporter {
    private AocValidationGroupeNodesImporter() {
    }

    /**
     * @return les données issus des données stockées dans le Clipboard
     */
    public static List<AocValidationGroupeNode> getPastData(String[] lois) {
        try {
            return extractValidationGroupe(lois, CommonGuiLib.getStringsFromClipboard());
        } catch (Exception exception) {
            Logger.getLogger(AocValidationGroupeNodesImporter.class.getName()).log(Level.WARNING, "read Data", exception);
        }
        return Collections.emptyList();
    }

    /**
     * parse les données <code>parse</code> pour construire des nodes {@link AocValidationGroupeNode}
     *
     * @param parse tableau de valeurs à parser
     * @return les noeuds
     */
    public static List<AocValidationGroupeNode> extractValidationGroupe(String[] lois, String[][] parse) {
        List<AocValidationGroupeNode> contents = new ArrayList<>();
        CtuluDoubleParser doubleParser = new CtuluDoubleParser();
        doubleParser.setEmptyIsNull(true);
        for (String[] lineContent : parse) {
            //on ignore la premier ligne
            if (ArrayUtils.isEmpty(parse) || StringUtils.startsWith(lineContent[0], "#") || DefaultEditableOutlineViewEditor
                    .FIRST_COLUMN_NAME_SELECTION
                    .equals(lineContent[0])) {
                continue;
            }
            int offsetColumn = 0;
            //permet de prendre en compte le cas ou la première colonne est un indice.
            if (lineContent.length >= 5 && !StringUtils.isBlank(lineContent[4])) {
                offsetColumn = 1;
            }
            if (lineContent.length >= 3 + offsetColumn) {
                AocValidationGroupe calculTransitoire = new AocValidationGroupe();
                calculTransitoire.setNom(StringUtils.trim(lineContent[offsetColumn]));
                calculTransitoire.setLoisAsString(StringUtils.trim(lineContent[1 + offsetColumn]));
                String commentaire = CtuluLibString.EMPTY_STRING;
                if (lineContent.length >= 4 + offsetColumn) {
                    commentaire = StringUtils.trim(lineContent[3 + offsetColumn]);
                }
                calculTransitoire.setCommentaire(commentaire);
                calculTransitoire.setEffectifApprentissage(AocLoiCalculPermanentNodesImporter
                        .getInteger(StringUtils.trim(lineContent[2 + offsetColumn]), AocValidationGroupe.EFFECTIF_APPRENTISSAGE_DEFAULT_VALUE));

                AocValidationGroupeNode node = new AocValidationGroupeNode(calculTransitoire, lois);
                contents.add(node);
            }
        }
        return contents;
    }
}
