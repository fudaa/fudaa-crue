package org.fudaa.fudaa.crue.aoc.courbe;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.iterator.NumberIntegerIterator;

/**
 * @author deniger
 */
public class CustomTickIterator extends NumberIntegerIterator {
    @Override
    public String currentLabel() {
        return CtuluLibString.EMPTY_STRING;
    }

    @Override
    public String formatValue(double _v) {
        return currentLabel();
    }
}
