package org.fudaa.fudaa.crue.aoc.nodes;

import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculTransitoire;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Node pour les donnees {@link AocLoiCalculTransitoire}
 *
 * @author deniger
 */
public class AocLoiCalculTansitoireNode extends AbstractAocLoiCalculNode<AocLoiCalculTransitoire> implements NodeWithSectionsContrat {
  private final String[] sections;

  public AocLoiCalculTansitoireNode(AocLoiCalculTransitoire aocLoiCalculTransitoire, String[] calculs, String[] lois, String[] sections, Map<String, String> commentByCalculId) {
    super(aocLoiCalculTransitoire, calculs, lois, commentByCalculId);
    //on ajoute une ligne vide au cas ou pas de sections sont choisies
    this.sections = addEmptyString(sections);
  }

  public String[] getSections() {
    return sections;
  }

  @Override
  public String getDataAsString() {
    final AocLoiCalculTransitoire echellesSection = getAocLoiCalculTransitoire();
    return echellesSection.getCalculRef() + "\t" + echellesSection.getLoiRef() + "\t" + echellesSection.getSectionRef() + "\t" + echellesSection
        .getPonderation();
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(AocLoiCalculTransitoire.class));
  }

  @Override
  protected Sheet createSheet() {
    Sheet res = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setName(AocLoiCalculTransitoire.class.getSimpleName());
    set.setDisplayName(BusinessMessages.geti18nForClass(AocLoiCalculTransitoire.class));
    AocLoiCalculTransitoire aocLoiCalculTransitoire = getAocLoiCalculTransitoire();
    res.put(set);

    createCalculProperty(set, aocLoiCalculTransitoire);
    AocLoiRefProperty.createLoiProperty(this, set, aocLoiCalculTransitoire);
    createPonderationProperty(set, aocLoiCalculTransitoire);
    createSectionProperty(set, aocLoiCalculTransitoire);

    return res;
  }

  private AocLoiCalculTransitoire getAocLoiCalculTransitoire() {
    return getLookup().lookup(AocLoiCalculTransitoire.class);
  }

  private void createSectionProperty(Sheet.Set set, AocLoiCalculTransitoire abstractLoiCalcul) {
    try {
      AocSectionProperty sectionProperty = new AocSectionProperty(this, abstractLoiCalcul);
      //important pour retrouver la valeur par la suite
      sectionProperty.setName(AocNodeHelper.PROP_SECTION);
      sectionProperty.setDisplayName(AocNodeHelper.getSectionDisplay());
      PropertyCrueUtils.configureNoCustomEditor(sectionProperty);
      set.put(sectionProperty);
    } catch (NoSuchMethodException e) {
      Logger.getLogger(ParametrageProfilSectionNode.class.getName()).log(Level.SEVERE, "can't find method", e);
    }
  }

  @Override
  public AocLoiCalculTransitoire getMainValue() {
    return getAocLoiCalculTransitoire();
  }

  public void replaceValueBy(AocLoiCalculTansitoireNode newValue) {
    if (newValue != null) {
      final AocLoiCalculTransitoire loiCalculPermanent = newValue.getAocLoiCalculTransitoire();
      getAocLoiCalculTransitoire().setCalculRef(loiCalculPermanent.getCalculRef());
      getAocLoiCalculTransitoire().setLoiRef(loiCalculPermanent.getLoiRef());
      getAocLoiCalculTransitoire().setSectionRef(loiCalculPermanent.getSectionRef());
      getAocLoiCalculTransitoire().setPonderation(loiCalculPermanent.getPonderation());
      firePropertyChange(AocNodeHelper.PROP_CALCUL, null, getAocLoiCalculTransitoire().getCalculRef());
      firePropertyChange(AocNodeHelper.PROP_LOI, null, getAocLoiCalculTransitoire().getLoiRef());
      firePropertyChange(AocNodeHelper.PROP_SECTION, null, getAocLoiCalculTransitoire().getSectionRef());
      firePropertyChange(AocNodeHelper.PROP_PONDERATION, null, getAocLoiCalculTransitoire().getPonderation());
    }
  }
}
