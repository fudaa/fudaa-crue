package org.fudaa.fudaa.crue.aoc.nodes;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.fudaa.crue.aoc.MessagesAoc;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.openide.nodes.Sheet;

/**
 * Classe d'aide pour les nodes AOC.
 *
 * @author deniger
 */
public class AocNodeHelper {
  public static final String PROP_VALUE = "value";
  public static final String PROP_DELTAQ_VALUE = "deltaQ";
  public static final String PROP_PROFIL = "pk";
  public static final String PROP_SECTION = "section";
  public static final String PROP_CALCUL = "calculRef";
  public static final String PROP_LOI = "loiRef";
  public static final String PROP_PONDERATION = "ponderation";
  public static final String PROP_MIN = "min";
  public static final String PROP_INI = "ini";
  public static final String PROP_MAX = "max";
  public static final String PROP_NOM = "nom";
  public static final String PROP_LOIS = "lois";
  public static final String PROP_COMMENTAIRE = "commentaire";
  public static final String PROP_EFFECTIF_APPRENTISSAGE = "effectifApprentissage";

  private AocNodeHelper() {
  }

  public static String getGroupeDisplay() {
    return BusinessMessages.getString("aoc.groupe.shortName");
  }

  public static String getLoisDisplay() {
    return BusinessMessages.getString("aoc.lois.shortName");
  }

  public static String getCommentaireDisplay() {
    return BusinessMessages.getString("aoc.commentaire.shortName");
  }

  public static String getEffectifApprentissageDisplay() {
    return BusinessMessages.getString("aoc.effectifApprentissage.shortName");
  }

  public static String getDeltaQDisplay() {
    return BusinessMessages.getString("aoc.deltaQ.property");
  }

  public static String getValueDisplay() {
    return MessagesAoc.getMessage("Value.Name");
  }

  public static String getEchelleDisplay() {
    return MessagesAoc.getMessage("Echelle.Name");
  }

  public static String getSectionDisplay() {
    return MessagesAoc.getMessage("Section.Name");
  }

  public static String getCalculDisplay() {
    return MessagesAoc.getMessage("Calcul.Name");
  }

  public static String getLoiDisplay() {
    return MessagesAoc.getMessage("Loi.Name");
  }

  public static String getLigneEauDisplay() {
    return MessagesAoc.getMessage("LigneEau.Name");
  }

  public static String getPonderationDisplay() {
    return MessagesAoc.getMessage("Ponderation.Name");
  }

  public static String getMinDisplay() {
    return MessagesAoc.getMessage("Min.Name");
  }

  public static String getIniDisplay() {
    return MessagesAoc.getMessage("Ini.Name");
  }

  public static String getMaxDisplay() {
    return MessagesAoc.getMessage("Max.Name");
  }

  public static void createProperty(AbstractNodeFirable node, Sheet.Set set, Object object, String property,
                                    String propertyDisplayName) {
    createProperty(node, set, object, property, property, propertyDisplayName);
  }

  private static void createProperty(AbstractNodeFirable node, Sheet.Set set, Object object, String property, String propertyName,
                                     String propertyDisplayName) {
    PropertyNodeBuilder builder = new PropertyNodeBuilder();
    PropertySupportReflection propertyPonderation = builder
        .createFromPropertyDesc(DecimalFormatEpsilonEnum.COMPARISON, object, node, property);
    //important pour retrouver la valeur par la suite
    propertyPonderation.setName(propertyName);
    propertyPonderation.setDisplayName(propertyDisplayName);
    PropertyCrueUtils.configureNoCustomEditor(propertyPonderation);
    set.put(propertyPonderation);
  }
}
