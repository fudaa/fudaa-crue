package org.fudaa.fudaa.crue.aoc.nodes;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculTransitoire;
import org.fudaa.fudaa.crue.aoc.AocLoisCalculsTransitoiresTopComponent;
import org.fudaa.fudaa.crue.aoc.MessagesAoc;
import org.fudaa.fudaa.crue.aoc.importer.AocLoiCalculTransitoireFileImporter;
import org.fudaa.fudaa.crue.aoc.importer.AocLoiCalculTransitoireNodesImporter;
import org.fudaa.fudaa.crue.aoc.importer.AocProfilSectionFileImporter;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.view.DefaultEditableOutlineViewEditor;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.util.*;

/**
 * Vue gerant les noeud {@link org.fudaa.dodico.crue.aoc.projet.AocLoiCalculTransitoire}
 *
 * @author deniger
 */
public class AocLoiCalculTransitoireView extends DefaultEditableOutlineViewEditor<AocLoisCalculsTransitoiresTopComponent, AocLoiCalculTransitoire, AocLoiCalculTansitoireNode> {
  public AocLoiCalculTransitoireView(final AocLoisCalculsTransitoiresTopComponent parent) {
    super(parent);
  }

  @Override
  public void setEditable(final boolean editable) {
    super.setEditable(editable);
    btAdd.setEnabled(editable && ArrayUtils.isNotEmpty(getAvailableLois()) && ArrayUtils
        .isNotEmpty(getAvailableCalculs()));
    btPaste.setEnabled(btAdd.isEnabled());
    btImport.setEnabled(btPaste.isEnabled());
  }

  /**
   * Remplace tout le contenu du tableau par le presse-papier
   */
  @Override
  protected void replaceAllByClipboardData() {
    if (!this.editable) {
      return;
    }
    final List<AocLoiCalculTansitoireNode> pastData = AocLoiCalculTransitoireNodesImporter
        .getPastData(getAvailableCalculs(), getAvailableLois(),
            getAvailableSections(), getCommentByCalculId());
    if (!pastData.isEmpty()) {
      update(pastData);
    }
    getTopComponentParent().setModified(true);
    DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
  }

  /**
   * Remplace tout le contenu du tableau par le contenu d'un fichier
   */
  @Override
  protected void importData() {
    if (!this.editable) {
      return;
    }

    final CtuluFileChooser fileChooser = AocProfilSectionFileImporter.getImportXLSCSVFileChooser();

    final int res = fileChooser.showDialog(CtuluUIForNetbeans.DEFAULT.getParentComponent(), NbBundle.getMessage(
        AocLoiCalculTransitoireView.class, "Import.DisplayName"));
    if (res == JFileChooser.APPROVE_OPTION) {
      final List<AocLoiCalculTansitoireNode> result = CrueProgressUtils.showProgressDialogAndRun(
          new AocLoiCalculTransitoireFileImporter(fileChooser.getSelectedFile(), getAvailableCalculs(),
              getAvailableLois(), getAvailableSections(), getCommentByCalculId()),
          NbBundle.getMessage(
              AocLoiCalculTransitoireView.class, "Import.DisplayName"));
      if (CollectionUtils.isEmpty(result)) {
        DialogHelper.showWarn(NbBundle.getMessage(getClass(), "Import.NoDataFound"));
      } else {
        update(result);
        // on force la modif de la fenêtre pour pouvoir enregistrer
        getTopComponentParent().setModified(true);
      }
    }
  }

  @Override
  protected void addElement() {
    //on va essayer d'ajoute un élément configuré avec une loi non associée
    //les lois deja utilisées dans l'editeur
    final Set<String> usedLois = getUsedLois();
    final Set<String> usedCalculs = getUsedCalculs();
    final Set<String> usedSections = getUsedSections();
    //les lois disponibles
    final String[] availableLois = getAvailableLois();
    final String[] availableCalculs = getAvailableCalculs();
    final String[] availableSections = getAvailableSections();
    //aucune loi dispo
    if (usedLois.size() == availableLois.length) {
      DialogHelper.showWarn(MessagesAoc.getMessage("CantAddCalculTransitoireLoiAllUsed.Message"));
      return;
    }
    if (usedSections.size() == availableSections.length) {
      DialogHelper.showWarn(MessagesAoc.getMessage("CantAddSectionAllUsed.Message"));
      return;
    }

    final String usableLoi = getFirstUsable(availableLois, usedLois);
    final String usableCalcul = getFirstUsable(availableCalculs, usedCalculs);
    final String usableSection = getFirstUsable(availableSections, usedSections);
    final AocLoiCalculTransitoire aocEchellesSection = new AocLoiCalculTransitoire(usableCalcul, usableLoi, usableSection, 1);
    final AocLoiCalculTansitoireNode node = new AocLoiCalculTansitoireNode(aocEchellesSection, availableCalculs,
        availableLois, availableSections, getCommentByCalculId());
    final Children children = getExplorerManager().getRootContext().getChildren();
    children.add(new Node[]{node});
    DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
  }

  /**
   * @return liste des lois deja definies dans le composant
   */
  private Set<String> getUsedLois() {
    final Set<String> usedLois = new HashSet<>();
    for (final Node otherNode : getExplorerManager().getRootContext().getChildren().getNodes()) {
      usedLois.add(((AocLoiCalculTansitoireNode) otherNode).getMainValue().getLoiRef());
    }
    return usedLois;
  }

  /**
   * @return liste des calculs deja definies dans le composant
   */
  private Set<String> getUsedCalculs() {
    final Set<String> usedCalculs = new HashSet<>();
    for (final Node otherNode : getExplorerManager().getRootContext().getChildren().getNodes()) {
      usedCalculs.add(((AocLoiCalculTansitoireNode) otherNode).getMainValue().getCalculRef());
    }
    return usedCalculs;
  }

  /**
   * @return liste des calculs deja definies dans le composant
   */
  private Set<String> getUsedSections() {
    final Set<String> usedSections = new HashSet<>();
    for (final Node otherNode : getExplorerManager().getRootContext().getChildren().getNodes()) {
      usedSections.add(((AocLoiCalculTansitoireNode) otherNode).getMainValue().getSectionRef());
    }
    return usedSections;
  }

  @Override
  protected void createOutlineView() {
    view = new OutlineView(FIRST_COLUMN_NAME_SELECTION);
    view.getOutline().setColumnHidingAllowed(false);
    view.getOutline().setRootVisible(false);
    view.setTreeSortable(false);
    view.setPropertyColumns(
        AocNodeHelper.PROP_CALCUL,
        AocNodeHelper.getCalculDisplay(),
        AocNodeHelper.PROP_LOI,
        AocNodeHelper.getLoiDisplay(),
        AocNodeHelper.PROP_SECTION,
        AocNodeHelper.getSectionDisplay(),
        AocNodeHelper.PROP_PONDERATION,
        AocNodeHelper.getPonderationDisplay());
  }

  /**
   * Copie le contenu du presse-papier dans le tableau-> remplace le contenu des cellules sélectionnées et ajoute si nécessaire des entrées.
   */
  @Override
  public void pasteInSelectedNodes() {
    final Node[] selectedNodes = getExplorerManager().getSelectedNodes();
    final List<AocLoiCalculTansitoireNode> pastData = AocLoiCalculTransitoireNodesImporter
        .getPastData(getAvailableCalculs(), getAvailableLois(), getAvailableSections(), getCommentByCalculId());
    if (selectedNodes.length > 0) {
      final int nbSelectedNodeToModify = Math.min(pastData.size(), selectedNodes.length);
      for (int i = 0; i < nbSelectedNodeToModify; i++) {
        ((AocLoiCalculTansitoireNode) selectedNodes[i]).replaceValueBy(pastData.get(i));
      }
    }
    //on ajoute les autres noeuds
    final List<AocLoiCalculTansitoireNode> newNodeToAdd = new ArrayList<>();
    for (int i = selectedNodes.length; i < pastData.size(); i++) {
      newNodeToAdd.add(pastData.get(i));
    }
    if (!newNodeToAdd.isEmpty()) {
      final Children children = getExplorerManager().getRootContext().getChildren();
      children.add(newNodeToAdd.toArray(new Node[0]));
      DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
    }
    validateData();
  }

  private String[] getAvailableCalculs() {
    return getTopComponentParent().getValidationHelper().getAvailableCalculTransitoiresArray();
  }

  private String[] getAvailableLois() {
    return getTopComponentParent().getValidationHelper().getAvailableLoisCalculsTransitoiresArray();
  }

  private String[] getAvailableSections() {
    return getTopComponentParent().getValidationHelper().getAvailableSectionsArray();
  }

  private Map<String, String> getCommentByCalculId() {
    return getTopComponentParent().getValidationHelper().getCommentByCalculId();
  }

  @Override
  public boolean validateData() {
    if (getTopComponentParent().getValidationHelper() != null) {
      final CtuluLog log = getValidatingResult();
      return !log.containsErrorOrSevereError();
    }
    return true;
  }

  public CtuluLog getValidatingResult() {
    return getTopComponentParent().getValidationHelper().validateCalculTransitoire(getAllNodesForValidation());
  }
}
