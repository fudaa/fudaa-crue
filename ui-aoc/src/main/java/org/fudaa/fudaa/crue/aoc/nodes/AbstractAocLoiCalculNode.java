package org.fudaa.fudaa.crue.aoc.nodes;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.crue.aoc.projet.AbstractLoiCalcul;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculPermanent;
import org.fudaa.fudaa.crue.aoc.perspective.PerspectiveServiceAoc;
import org.fudaa.fudaa.crue.common.property.AbstractNodeValueFirable;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Node pour les donnees {@link AocLoiCalculPermanent}
 *
 * @author deniger
 */
public abstract class AbstractAocLoiCalculNode<O extends AbstractLoiCalcul> extends AbstractNodeValueFirable<O> implements NodeWithLoisContrat {
  private final PerspectiveServiceAoc abstractPerspectiveService = Lookup.getDefault().lookup(PerspectiveServiceAoc.class);
  private final String[] calculs;
  private String[] lois;
  private final Map<String, String> commentByCalculId;

  protected AbstractAocLoiCalculNode(O loiCalcul, String[] calculs, String[] lois, Map<String, String> commentByCalculId) {
    super(Children.LEAF, Lookups.singleton(loiCalcul));
    this.commentByCalculId = commentByCalculId;
    setNoImage();
    if (loiCalcul.getCalculRef() == null) {
      setName("");
    } else {
      setName(loiCalcul.getCalculRef());
    }
    this.calculs = addEmptyString(calculs);
    this.lois = addEmptyString(lois);
    DefaultNodePasteType.setIndexUsedAsDisplayName(this);
  }

  public static String[] addEmptyString(String[] in) {
    if (CtuluLibArray.isEmpty(in)) {
      return in;
    }
    String[] res = new String[in.length + 1];
    res[0] = CtuluLibString.EMPTY_STRING;
    System.arraycopy(in, 0, res, 1, in.length);
    return res;
  }

  public Map<String, String> getCommentByCalculId() {
    return commentByCalculId;
  }

  public String[] getCalculs() {
    return calculs;
  }

  public String[] getLois() {
    return lois;
  }

  public void updateLois(String[] lois) {
    this.lois = lois;
    final Property property = super.find(AocNodeHelper.PROP_LOI);
    if (property != null) {
      ((AocLoiRefProperty) property).setTags(lois);
    }
  }

  @Override
  public boolean canCut() {
    return canDestroy();
  }

  @Override
  public boolean canDestroy() {
    return isEditMode();
  }

  @Override
  public boolean isEditMode() {
    return abstractPerspectiveService == null || abstractPerspectiveService.isInEditMode();
  }

  protected void createCalculProperty(Sheet.Set set, AbstractLoiCalcul abstractLoiCalcul) {
    try {
      AocCalculRefProperty propertyCalcul = new AocCalculRefProperty(this, abstractLoiCalcul);
      propertyCalcul.setName(AocNodeHelper.PROP_CALCUL);
      propertyCalcul.setDisplayName(AocNodeHelper.getCalculDisplay());
      PropertyCrueUtils.configureNoCustomEditor(propertyCalcul);
      set.put(propertyCalcul);
    } catch (NoSuchMethodException e) {
      Logger.getLogger(ParametrageProfilSectionNode.class.getName()).log(Level.SEVERE, "can't find method", e);
    }
  }

  protected void createPonderationProperty(Sheet.Set set, AbstractLoiCalcul abstractLoiCalcul) {
    AocNodeHelper.createProperty(this, set, abstractLoiCalcul, AocNodeHelper.PROP_PONDERATION, AocNodeHelper.getPonderationDisplay());
  }
}
