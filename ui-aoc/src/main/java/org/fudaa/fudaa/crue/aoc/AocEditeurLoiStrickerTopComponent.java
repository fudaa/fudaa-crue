package org.fudaa.fudaa.crue.aoc;

import com.jidesoft.swing.JideSplitButton;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.aoc.projet.AocLoiStrickerValue;
import org.fudaa.dodico.crue.aoc.projet.AocLoiStrickler;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.fudaa.crue.aoc.courbe.AocLoiConfigController;
import org.fudaa.fudaa.crue.aoc.courbe.AocLoiUiController;
import org.fudaa.fudaa.crue.aoc.courbe.AocStrickerCourbeModel;
import org.fudaa.fudaa.crue.aoc.courbe.AocStricklerBanner;
import org.fudaa.fudaa.crue.aoc.nodes.AocLoiStricklerNode;
import org.fudaa.fudaa.crue.aoc.nodes.AocLoiStricklerView;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultTopComponentParent;
import org.fudaa.fudaa.crue.loi.CourbeViewConfigurationButtonUI;
import org.fudaa.fudaa.crue.loi.CourbeViewConfigurationTarget;
import org.fudaa.fudaa.crue.loi.ViewCourbeManager;
import org.fudaa.fudaa.crue.loi.loiff.LoiUiController;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.view.OutlineView;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Éditeur de lois de frottement
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.aoc//AocEditeurLoiStrickerTopComponent//EN",
    autostore = false)
@TopComponent.Description(preferredID = AocEditeurLoiStrickerTopComponent.TOPCOMPONENT_ID, persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = AocEditeurLoiStrickerTopComponent.MODE, openAtStartup = false)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.aoc.AocEditeurLoiStrickerTopComponent")
@TopComponent.OpenActionRegistration(displayName = "#CTL_AocEditeurLoiStrickerTopComponent",
    preferredID = AocEditeurLoiStrickerTopComponent.TOPCOMPONENT_ID)
@ActionReference(path = "Menu/Window/AOC", position = 5)
public final class AocEditeurLoiStrickerTopComponent extends AbstractAocEditeurTopComponent implements DefaultTopComponentParent, CourbeViewConfigurationTarget, AocLoiTopComponentInterface {
  public static final String MODE = "aoc-editeurLoiStrickler"; //NOI18N
  public static final String TOPCOMPONENT_ID = "AocEditeurLoiStrickerTopComponent"; //NOI18N
  private final transient ViewCourbeManager viewCourbeManager;
  private final AocLoiStricklerView outlineViewEditor;
  private transient AocLoiConfigController configController;
  private final transient LoiUiController loiUiController;

  public AocEditeurLoiStrickerTopComponent() {
    super(NbBundle.getMessage(AocEditeurLoiStrickerTopComponent.class, "CTL_" + TOPCOMPONENT_ID));
    initComponents();
    setToolTipText(NbBundle.getMessage(AocEditeurLoiStrickerTopComponent.class, "HINT_" + TOPCOMPONENT_ID));

    //les courbes
    loiUiController = new AocLoiUiController();
    loiUiController.addExportImagesToToolbar();
    loiUiController.setUseVariableForAxeH(true);
    loiUiController.getAxeX().setFont(EGGraphe.DEFAULT_FONT);
    loiUiController.getAxeX().setIsDiscret(true);
    loiUiController.getAxeX().setGraduations(false);

    editActions.addAll(loiUiController.getEditActions());
    loiUiController.getPanel().setPreferredSize(new Dimension(550, 350));
    loiUiController.setEditable(false);
    //legendes
    viewCourbeManager = new ViewCourbeManager(loiUiController.getPanel());
    viewCourbeManager.getConfig().setDisplayLabels(false);
    viewCourbeManager.getConfig().setDisplayTitle(false);
    CourbeViewConfigurationButtonUI configManager = new CourbeViewConfigurationButtonUI(this);
    configManager.installDoubleClickListener(viewCourbeManager);
    configManager.setLabelAvailable(false);
    configManager.setTitleAvailable(false);
    final JideSplitButton button = configManager.getButton();

    final JPanel toolbar = loiUiController.getToolbar();
    toolbar.add(button);
    super.editComponent.add(button);

    outlineViewEditor = new AocLoiStricklerView(this);
    outlineViewEditor.getView().setTreeSortable(false);
    loiUiController.getGraphe().setHorizontalBanner(new AocStricklerBanner(this.outlineViewEditor, loiUiController));
    //pour mettre à jour la selection des points
    loiUiController.getPanel().getView().addInteractiveCmp(new AocEditeurLoiStricklerSuivi(outlineViewEditor, loiUiController));
    //on construit le panel qui contient les courbes
    JPanel graphePanel = new JPanel(new BorderLayout());
    graphePanel.add(viewCourbeManager.getPanel());
    graphePanel.add(BorderLayout.NORTH, toolbar);

    JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, graphePanel, outlineViewEditor);
    splitPane.setDividerLocation(550);
    splitPane.setOneTouchExpandable(true);
    splitPane.setContinuousLayout(true);
    add(splitPane, BorderLayout.CENTER);
    setEditable(false);
    aocCoampagnedChanged();
    add(createCancelSaveButtons(), BorderLayout.SOUTH);
  }

  @Override
  public String getDefaultTopComponentId() {
    return getClass().getSimpleName();
  }

  @Override
  public void cancelModification() {
    super.cancelModification();
    configController.reloadCourbeConfig();
  }

  @Override
  public boolean isUpdating() {
    return outlineViewEditor.isUpdating();
  }

  @Override
  public void setConfigModified() {
    super.setModified(true);
  }

  @Override
  public void applyLabelsChanged() {
//rien a faire
  }

  @Override
  public void applyTitleChanged() {
    //rien a faire ici
  }

  @Override
  public void setChanged() {
    //rien a faire ici
  }

  @Override
  public List<? extends Action> getAdditionalActions() {
    return Collections.emptyList();
  }

  @Override
  public ViewCourbeManager getManager() {
    return viewCourbeManager;
  }

  @Override
  public OutlineView getView() {
    return outlineViewEditor.getView();
  }

  @Override
  protected void aocCoampagnedChanged() {
    List<AocLoiStricklerNode> nodes = new ArrayList<>();
    final AocCampagne aoc = aocService.getCurrentAOC();
    if (aoc != null) {
      final String[] availableFrottementsArray = aocService.getAocValidationHelper().getAvailableFrottementsArray();
      for (AocLoiStrickler loiStrickler : aoc.getDonnees().getLoisStrickler().getLois()) {
        AocLoiStricklerNode node = new AocLoiStricklerNode(loiStrickler.clone(), availableFrottementsArray);
        nodes.add(node);
      }
    }
    outlineViewEditor.update(nodes);
    loiUiController.getEGGrapheSimpleModel().removeAllCurves(null);
    if (aocService.getCcm() != null && aoc != null) {
      final EGAxeVertical axeVertical = loiUiController.createAxeVertical(aocService.getCcm().getNature("Nat_Num"), true);
      axeVertical.setTitre(CtuluLibString.EMPTY_STRING);
      createCourbe(axeVertical, AocLoiStrickerValue.MIN);
      createCourbe(axeVertical, AocLoiStrickerValue.INI);
      createCourbe(axeVertical, AocLoiStrickerValue.MAX);
      if (configController == null) {
        configController = new AocLoiConfigController(this, loiUiController);
      }
      configController.applyConfig();
      configController.install();
      loiUiController.getGraphe().restore();
      loiUiController.getGraphe().fullRepaint();
    } else if (configController != null) {
      configController.uninstall();
    }
  }

  private void createCourbe(EGAxeVertical axeVertical, AocLoiStrickerValue value) {
    final EGCourbeSimple obj = new EGCourbeSimple(axeVertical, new AocStrickerCourbeModel(this.outlineViewEditor, value));
    Color color = Color.RED;
    if (AocLoiStrickerValue.MIN.equals(value)) {
      color = Color.BLUE;
    }
    if (AocLoiStrickerValue.MAX.equals(value)) {
      color = Color.GREEN;
    }
    obj.getLigneModel().setEpaisseur(2);
    obj.getLigneModel().setColor(color);
    loiUiController.getEGGrapheSimpleModel().addCourbe(
        obj, null);
  }

  @Override
  public boolean isModified() {
    return super.isModified() || configController.isConfigModified();
  }

  @Override
  public void setModified(boolean modified) {
    super.setModified(modified);
    if (modified) {
      loiUiController.getGraphe().setXRangeIsModified();
      if (loiUiController.getGraphe().isAutoRestore()) {
        loiUiController.getGraphe().restore();
      }
      loiUiController.getGraphe().fullRepaint();
    }
  }

  @Override
  public boolean valideModification() {
    if (!isModified()) {
      return true;
    }
    configController.setUpdating(true);
    CtuluLog validatingResult = outlineViewEditor.getValidatingResult();
    if (validatingResult.isNotEmpty()) {
      LogsDisplayer.displayError(validatingResult, getName());
    }
    if (validatingResult.containsErrorOrSevereError()) {
      configController.setUpdating(false);
      return false;
    }
    final List<AocLoiStrickler> allNodesValues = outlineViewEditor.getAllNodesValues();
    aocService.setLoisStricker(allNodesValues);
    boolean configModified = configController.saveCourbeConfig();
    if (configModified) {
      getAttachedPerspective().setDirty(true);
    }
    setModified(false);
    configController.setUpdating(false);
    return true;
  }

  @Override
  protected void setEditable(boolean b) {
    super.setEditable(b);
    outlineViewEditor.setEditable(b);
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueEditeurLoiStrickler", PerspectiveEnum.AOC);
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  @SuppressWarnings("unused")
  void writeProperties(java.util.Properties p) {
    //pas de persistence
  }

  @SuppressWarnings("unused")
  void readProperties(java.util.Properties p) {
    //pas de persistence
  }
}
