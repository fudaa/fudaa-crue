package org.fudaa.fudaa.crue.aoc.courbe;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuTableCellRenderer;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;
import org.fudaa.dodico.crue.metier.emh.LoiTF;
import org.fudaa.fudaa.crue.aoc.importer.AocLoiSectionZFileImporter;
import org.fudaa.fudaa.crue.aoc.importer.AocLoiSectionZImporter;
import org.fudaa.fudaa.crue.aoc.importer.AocProfilSectionFileImporter;
import org.fudaa.fudaa.crue.aoc.nodes.AocLoiStricklerView;
import org.fudaa.fudaa.crue.aoc.service.LhptService;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiCourbeModel;
import org.fudaa.fudaa.crue.loi.common.LoiPopupMenuReceiver;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.util.List;

/**
 * @author deniger
 */
public class AocMesureLoiUiController extends AocLoiUiController {
  private CrueConfigMetier ccm;
  private LhptService.LoiMesurePostState loiMesurePostData;

  @Override
  protected void specificPaste() {
    if (getCourbe() == null) {
      return;
    }
    AbstractLoiCourbeModel model = (AbstractLoiCourbeModel) getCourbe().getModel();
    if (EnumTypeLoi.LoiSectionsZ.equals(model.getConfigLoi().getTypeLoi())) {
      final List<AocLoiLine> pastData = AocLoiSectionZImporter.getPastData();
      if (CollectionUtils.isNotEmpty(pastData)) {
        ((LHPTLoiSectionZCourbeModel) model).pasteLines(pastData);
      }
    } else {
      getTableGraphePanel().getTable().paste();
    }
  }

  @Override
  protected void specificImport() {
    if (getCourbe() == null) {
      return;
    }
    AbstractLoiCourbeModel model = (AbstractLoiCourbeModel) getCourbe().getModel();
    if (!model.isModifiable()) {
      return;
    }
    if (EnumTypeLoi.LoiSectionsZ.equals(model.getConfigLoi().getTypeLoi())) {
      CtuluFileChooser fileChooser = AocProfilSectionFileImporter.getImportXLSCSVFileChooser();

      final int res = fileChooser.showDialog(CtuluUIForNetbeans.DEFAULT.getParentComponent(), NbBundle.getMessage(
          AocLoiStricklerView.class, "Import.DisplayName"));
      if (res == JFileChooser.APPROVE_OPTION) {
        final List<AocLoiLine> result = CrueProgressUtils.showProgressDialogAndRun(
            new AocLoiSectionZFileImporter(fileChooser.getSelectedFile()),
            NbBundle.getMessage(
                AocLoiStricklerView.class, "Import.DisplayName"));
        if (CollectionUtils.isEmpty(result)) {
          DialogHelper.showWarn(NbBundle.getMessage(getClass(), "Import.NoDataFound"));
        } else {
          model.removeAll();
          ((LHPTLoiSectionZCourbeModel) model).pasteLines(result);
        }
      }
    } else {
      getTableGraphePanel().tableImport();
    }
  }

  protected void clearAndPaste() {
    if (getCourbe() == null) {
      return;
    }
    AbstractLoiCourbeModel model = (AbstractLoiCourbeModel) getCourbe().getModel();
    if (model.isModifiable()) {
      model.removeAll();
      specificPaste();
    }
  }

  @Override
  protected AbstractLoiCourbeModel createCourbeModel(AbstractLoi loi, ConfigLoi configLoi) {

    if (loi instanceof LoiTF) {
      final LHPTLoiSectionZCourbeModel courbeModelTF = new LHPTLoiSectionZCourbeModel(configLoi, loiMesurePostData);
      courbeModelTF.setCrueConfigMetier(ccm);
      courbeModelTF.setCourbe((LoiTF) loi);
      return courbeModelTF;
    }
    return super.createCourbeModel(loi, configLoi);
  }

  @Override
  public LoiPopupMenuReceiver createLoiPopupReceiver() {
    return new AocLoiPopupMenuReceiver(panel, this);
  }

  public void setLoiMesurePostData(LhptService.LoiMesurePostState loiMesurePostData) {
    this.loiMesurePostData = loiMesurePostData;
  }

  @Override
  protected void setLoi(AbstractLoi loi, CrueConfigMetier ccm, boolean usePresentationFormat, String axeName) {
    this.ccm = ccm;
    super.setLoi(loi, ccm, usePresentationFormat, axeName);
    //on doit modifier l'axe horizontal si loisection z:
    boolean isSection = loi != null && EnumTypeLoi.LoiSectionsZ.equals(loi.getType());
    if (isSection) {
      getAxeX().setValueEditor(new ProfilValueEditor());
      getGraphe().setHorizontalBanner(new AocLoiSectionZBanner(this));
    } else {
      getGraphe().setHorizontalBanner(null);
    }
  }

  @Override
  protected AbstractTableModel createTableModel() {
    return new AocMesureTableModel(getTableGraphePanel());
  }

  private class ProfilValueEditor implements CtuluValueEditorI {
    boolean editable;

    @Override
    public Class getDataClass() {
      return String.class;
    }

    public String getSeparator() {
      return null;
    }

    @Override
    public void setValue(Object object, Component component) {
      ((JComboBox) component).setSelectedItem(object);
    }

    @Override
    public String toString(Object o) {
      return o == null ? CtuluLibString.EMPTY_STRING : o.toString();
    }

    @Override
    public boolean isEmpty(Component component) {
      return StringUtils.isBlank(getStringValue(component));
    }

    @Override
    public boolean isEditable() {
      return editable;
    }

    @Override
    public void setEditable(boolean editable) {
      this.editable = editable;
    }

    @Override
    public String getStringValue(Component component) {
      return (String) ((JComboBox) component).getSelectedItem();
    }

    @Override
    public Object parseString(String s) {
      return s;
    }

    @Override
    public Object getValue(Component component) {
      return getStringValue(component);
    }

    @Override
    public JComponent createEditorComponent() {
      throw new IllegalAccessError("not implemented");
    }

    @Override
    public JComponent createCommonEditorComponent() {
      throw new IllegalAccessError("not implemented");
    }

    @Override
    public TableCellEditor createTableEditorComponent() {
      return createCommonTableEditorComponent();
    }

    @Override
    public TableCellEditor createCommonTableEditorComponent() {
      return new DefaultCellEditor(new BuComboBox()) {
        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
          BuComboBox cb = (BuComboBox) super.getTableCellEditorComponent(table, value, isSelected, row, column);
          LHPTLoiSectionZCourbeModel model = (LHPTLoiSectionZCourbeModel) getCourbe().getModel();
          cb.setAllItems(model.getAvailableProfils(row));
          cb.setSelectedItem(value);
          return cb;
        }
      };
    }

    @Override
    public TableCellRenderer createTableRenderer() {
      return new BuTableCellRenderer();
    }

    @Override
    public boolean isValueValidFromComponent(Component component) {
      return isValid(((JComboBox) component).getSelectedItem());
    }

    @Override
    public String getValidationMessage() {
      return null;
    }

    @Override
    public boolean isValid(Object object) {
      LHPTLoiSectionZCourbeModel model = (LHPTLoiSectionZCourbeModel) getCourbe().getModel();
      return model.isProfilExists((String) object);
    }
  }
}
