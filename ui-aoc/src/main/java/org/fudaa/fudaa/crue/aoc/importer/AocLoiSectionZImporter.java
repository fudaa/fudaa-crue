package org.fudaa.fudaa.crue.aoc.importer;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluDoubleParser;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;
import org.fudaa.fudaa.crue.aoc.courbe.AocLoiLine;
import org.fudaa.fudaa.crue.aoc.nodes.AocLigneEauCalculPermanentNode;
import org.fudaa.fudaa.crue.aoc.nodes.AocLigneEauCalculPermanentView;
import org.fudaa.fudaa.crue.common.swing.CommonGuiLib;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Methodes utilitaires pour importer des lois stricker
 */
public class AocLoiSectionZImporter {
    private AocLoiSectionZImporter() {
    }

    /**
     * @return les noeuds créés à partir des données stockées dans le Clipboard
     */
    public static List<AocLoiLine> getPastData() {
        try {
            return extractLoiSectionZ(CommonGuiLib.getStringsFromClipboard());
        } catch (Exception exception) {
            Logger.getLogger(AocLoiSectionZImporter.class.getName()).log(Level.WARNING, "read Data", exception);
        }
        return Collections.emptyList();
    }

    /**
     * parse les données <code>parse</code> pour construire des nodes {@link AocLigneEauCalculPermanentNode}
     *
     * @param parse tableau de valeurs à parser
     * @return les noeuds
     */
    public static List<AocLoiLine> extractLoiSectionZ(
            String[][] parse) {
        List<AocLoiLine> contents = new ArrayList<>();
        CtuluDoubleParser doubleParser = new CtuluDoubleParser();
        for (String[] lineContent : parse) {
            //on ignore la premier ligne
            if (ArrayUtils.isEmpty(parse) || StringUtils.startsWith(lineContent[0], "#") || AocLigneEauCalculPermanentView.FIRST_COLUMN_NAME_SELECTION
                    .equals(lineContent[0])) {
                continue;
            }
            int offsetColumn = 0;
            //permet de prendre en compte le cas ou la première colonne est un indice.
            if (lineContent.length >= 3 && !StringUtils.isBlank(lineContent[2])) {
                offsetColumn = 1;
            }

            if (lineContent.length >= 2 + offsetColumn) {
                String profil = StringUtils.trim(lineContent[offsetColumn]);
                final String valueAsString = lineContent[1 + offsetColumn];
                if (StringUtils.isBlank(valueAsString) || !doubleParser.isValid(valueAsString)) {
                    continue;
                }
                Double value = doubleParser.parse(valueAsString);
                AocLoiLine line = new AocLoiLine();
                line.setProfil(profil);
                line.setPtEvolutionFF(new PtEvolutionFF(0, value));
                contents.add(line);
            }
        }
        return contents;
    }
}
