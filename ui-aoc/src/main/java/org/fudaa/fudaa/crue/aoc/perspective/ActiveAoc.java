package org.fudaa.fudaa.crue.aoc.perspective;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractPerspectiveAction;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;

/**
 * Permet d'activer une perspective. Action utilisée dans la toobar pour changer de perspective. Pour la gestion des ordres des menus, voir le fichier
 * layer.xml
 */
@ActionID(category = "File", id = "org.fudaa.fudaa.crue.aoc.perspective.ActiveAoc")
@ActionRegistration(displayName = "#CTL_ActiveAoc")
@ActionReference(path = "Menu/Window", position = 7)
public final class ActiveAoc extends AbstractPerspectiveAction {

  public ActiveAoc() {
    super("CTL_ActiveAoc", PerspectiveServiceAoc.class.getName(), PerspectiveEnum.AOC);
    setBooleanState(false);
  }

  @Override
  protected String getFolderAction() {
    return "Actions/AOC";
  }
}
