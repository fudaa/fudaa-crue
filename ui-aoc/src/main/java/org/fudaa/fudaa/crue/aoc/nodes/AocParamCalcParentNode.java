package org.fudaa.fudaa.crue.aoc.nodes;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.aoc.ParametrageProfilSection;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPerm;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.AbstractNodeValueFirable;
import org.fudaa.fudaa.crue.common.property.PropertyStringReadOnly;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.lookup.Lookups;

import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

/**
 * Noeud affichant une ligne profil->section
 *
 * @author deniger
 */
public class AocParamCalcParentNode extends AbstractNodeValueFirable<String> {
  /**
   * @param children les {@link AocParamCalcNode}
   * @param calcul le nom du calcul
   */
  public AocParamCalcParentNode(Children children, String calcul) {
    super(children, Lookups.fixed(calcul));
    setNoImage();
    setName(calcul);
    setDisplayName(calcul);
  }

  @Override
  public String getDataAsString() {
    return getMainValue();
  }

  @Override
  public Transferable clipboardCopy() {
    StringBuilder stringBuilder = new StringBuilder();
    for (Node node : getChildren().getNodes()) {
      if (stringBuilder.length() > 0) {
        stringBuilder.append('\n');
      }
      stringBuilder.append(((AbstractNodeFirable) node).getDataAsString());
    }
    return new StringSelection(stringBuilder.toString());
  }

  @Override
  public boolean canCut() {
    return false;
  }

  @Override
  public boolean canDestroy() {
    return false;
  }

  @Override
  public boolean isEditMode() {
    return false;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(getMainValue()));
  }

  @Override
  protected Sheet createSheet() {
    Sheet res = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setName(ParametrageProfilSection.class.getSimpleName());
    set.setDisplayName(BusinessMessages.geti18nForClass(CalcPseudoPerm.class));
    String type = CalcPseudoPerm.class.getSimpleName();
    String value = BusinessMessages.getStringOrDefault(type + ".shortName", type);
    res.put(set);
    PropertyStringReadOnly valueProperty = new PropertyStringReadOnly(value, AocNodeHelper.PROP_VALUE,
        AocNodeHelper.getValueDisplay(), AocNodeHelper.getValueDisplay());
    //important pour retrouver la valeur par la suite
    set.put(valueProperty);
    return res;
  }

  @Override
  public String getMainValue() {
    return getLookup().lookup(String.class);
  }
}
