package org.fudaa.fudaa.crue.aoc;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuResource;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.gui.ExportTableCommentSupplier;
import org.fudaa.dodico.crue.aoc.validation.LHPTContentValidator;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.time.ToStringTransformerDate;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.LoiFactory;
import org.fudaa.dodico.crue.validation.ValidationHelperLoi;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.aoc.courbe.AocLoiConfigController;
import org.fudaa.fudaa.crue.aoc.courbe.AocMesureLoiUiController;
import org.fudaa.fudaa.crue.aoc.courbe.LHPTLoiSectionZCourbeModel;
import org.fudaa.fudaa.crue.aoc.service.LhptService;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.editor.LocalDateTimePanel;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.helper.ObjetWithIdCellRenderer;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.AbstractPerspectiveService;
import org.fudaa.fudaa.crue.common.services.SysdocContrat;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.loi.common.SecondValueEditor;
import org.fudaa.fudaa.crue.study.services.TableExportCommentSupplier;
import org.joda.time.LocalDateTime;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

public class ParametrageMesurePostTraitementTopComponent extends AbstractLhptTopComponent implements ExportTableCommentSupplier, AocLoiTopComponentInterface {
  private final JPanel center = new JPanel();
  private JComboBox cbLois;
  private transient AbstractLoi currentLoi;
  private transient Action delete;
  private transient Action duplicate;
  private transient Action create;
  private transient AocMesureLoiUiController loiUiController;
  private JTextField txtCommentaire;
  private JTextField txtNom;
  private JLabel lbNomValidation;
  private final String initName;
  private boolean isNewLoi;
  private transient DateDebEditor dateDebutEditor;
  private LocalDateTime dateDebutValue;
  private transient AocLoiConfigController configController;
  private boolean dataModified;

  public ParametrageMesurePostTraitementTopComponent(AbstractPerspectiveService attachedPerspective) {
    super(attachedPerspective);
    initComponents();
    initName = MessagesAoc.getMessage("CTL_ParametrageMesurePostTraitementTopComponent");
    setName(initName);
    setToolTipText(MessagesAoc.getMessage("HINT_ParametrageMesurePostTraitementTopComponent"));
    add(createCbScenarioPanel(), BorderLayout.NORTH);
    buildLoiPanel();
    add(center, BorderLayout.CENTER);
    add(createCancelSaveButtons(), BorderLayout.SOUTH);
    setEditable(false);
  }

  private CrueConfigMetier getCcm() {
    return service.getProjetService().getCcm();
  }

  private void buildLoiPanel() {
    center.setLayout(new BorderLayout());
    loiUiController = new AocMesureLoiUiController();
    loiUiController.addExportImagesToToolbar();
    loiUiController.setUseVariableForAxeH(true);
    loiUiController.getPanel().setPreferredSize(new Dimension(550, 350));
    JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, loiUiController.getPanel(), loiUiController.getTableGraphePanel());

    splitPane.setDividerLocation(550);
    splitPane.setOneTouchExpandable(true);
    splitPane.setContinuousLayout(true);
    JPanel nomComment = new JPanel(new BuGridLayout(3, 10, 10));

    txtNom = new JTextField(30);
    lbNomValidation = new JLabel();

    //ligne 1
    nomComment.add(new JLabel(NbBundle.getMessage(ParametrageMesurePostTraitementTopComponent.class, "Nom.DisplayName")));
    nomComment.add(txtNom);
    nomComment.add(lbNomValidation);

    txtCommentaire = new JTextField(30);
    txtCommentaire.setEditable(false);
    //ligne 2
    nomComment.add(new JLabel(NbBundle.getMessage(ParametrageMesurePostTraitementTopComponent.class, "Commentaire.DisplayName")));
    nomComment.add(txtCommentaire);
    nomComment.add(new JLabel());

    //ligne 3
    dateDebutEditor = new DateDebEditor();
    //ligne 3
    nomComment.add(new JLabel(NbBundle.getMessage(ParametrageMesurePostTraitementTopComponent.class, "DateDebut.DisplayName")));
    nomComment.add(dateDebutEditor.getjPanel());

    JPanel pnInfo = new JPanel(new BorderLayout(5, 5));
    pnInfo.add(loiUiController.getInfoController().getPanel());
    pnInfo.add(nomComment, BorderLayout.NORTH);
    pnInfo.add(createCancelSaveButtons(), BorderLayout.SOUTH);
    cbLois = new JComboBox();
    cbLois.addItemListener(e -> {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        currentLoi = (AbstractLoi) cbLois.getSelectedItem();
        updateWithLoi();
        loiUiController.getGraphe().restore();
      }
    });
    cbLois.setRenderer(new ObjetNommeCellRenderer());

    //creation des
    JPanel pnActions = new JPanel(new BuGridLayout(1, 5, 5));
    pnActions.add(cbLois);
    pnActions.add(new JSeparator());
    createActions();
    pnActions.add(new JButton(create));
    pnActions.add(new JButton(duplicate));
    pnActions.add(new JSeparator());
    pnActions.add(new JButton(delete));
    JPanel pnSouth = new JPanel(new BorderLayout(10, 10));
    pnSouth.add(pnActions, BorderLayout.WEST);
    pnSouth.setBorder(BorderFactory.createEtchedBorder());
    pnSouth.add(pnInfo);

    //on ajoute les panel au panel center
    center.add(loiUiController.getToolbar(), BorderLayout.NORTH);
    center.add(splitPane, BorderLayout.CENTER);
    center.add(pnSouth, BorderLayout.SOUTH);

    DocumentListener nameChanged = new DocumentListener() {
      @Override
      public void insertUpdate(DocumentEvent e) {
        titleOrCommentChanged();
      }

      @Override
      public void removeUpdate(DocumentEvent e) {
        titleOrCommentChanged();
      }

      @Override
      public void changedUpdate(DocumentEvent e) {
        titleOrCommentChanged();
      }
    };
    txtNom.getDocument().addDocumentListener(nameChanged);
    txtCommentaire.getDocument().addDocumentListener(nameChanged);
  }

  private void titleOrCommentChanged() {
    lbNomValidation.setText(StringUtils.EMPTY);
    lbNomValidation.setIcon(null);
    lbNomValidation.setToolTipText(null);
    if (currentLoi == null) {
      return;
    }
    setName(initName + BusinessMessages.ENTITY_SEPARATOR + txtNom.getText());
    updateTopComponentName();
    String error = validateNom();
    if (error != null) {
      lbNomValidation.setToolTipText(error);
      lbNomValidation.setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.SEVERE));
    }
    if (!isUpdating()) {
      setModified(true);
    }
  }

  @Override
  public String getDefaultTopComponentId() {
    return getClass().getSimpleName();
  }

  @Override
  public boolean isUpdating() {
    return updating;
  }

  @Override
  public void setConfigModified() {
    super.setModified(true);
  }

  private void createActions() {
    delete = new EbliActionSimple(NbBundle.getMessage(ParametrageMesurePostTraitementTopComponent.class, "Delete.DisplayName"), BuResource.BU.getIcon("enlever"),
        "DELETE") {
      @Override
      public void actionPerformed(ActionEvent actionEvent) {
        delete();
      }
    };
    delete.putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(ParametrageMesurePostTraitementTopComponent.class, "Delete.Description"));

    duplicate = new EbliActionSimple(NbBundle.getMessage(ParametrageMesurePostTraitementTopComponent.class, "Duplicate.DisplayName"),
        BuResource.BU.getIcon("dupliquer"),
        "DUPLICATE") {
      @Override
      public void actionPerformed(ActionEvent actionEvent) {
        duplicate();
      }
    };
    duplicate.putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(ParametrageMesurePostTraitementTopComponent.class, "Duplicate.Description"));

    create = new EbliActionSimple(NbBundle.getMessage(ParametrageMesurePostTraitementTopComponent.class, "Create.DisplayName"), BuResource.BU.getIcon("creer"),
        "CREATE") {
      @Override
      public void actionPerformed(ActionEvent actionEvent) {
        create();
      }
    };
    create.putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(ParametrageMesurePostTraitementTopComponent.class, "Create.Description"));
  }

  @Override
  protected void currentDataChanged(ManagerEMHScenario emhScenario, LhptService.LoiMesurePostState loiMesuresPost) {
    //en cours de mise à jour par ce component: on ne fait rien:
    if (configController != null && configController.isUpdating()) {
      return;
    }

    if (loiMesuresPost == null) {
      dateDebutValue = null;
      currentLoi = null;
      cbLois.setModel(new DefaultComboBoxModel());
    } else {
      dateDebutValue = loiMesuresPost.getDateDebSce();
      updateComboBoxList(loiMesuresPost);
    }
    isNewLoi = false;
    if (configController == null) {
      configController = new AocLoiConfigController(this, loiUiController);
    }
    loiUiController.setLoiMesurePostData(loiMesuresPost);
    updateWithLoi();
    updateActionEditable();

    if (emhScenario != null) {
      configController.install();
    } else {
      configController.uninstall();
    }
    loiUiController.getGraphe().restore();
  }

  private void updateComboBoxList(LhptService.LoiMesurePostState loiMesuresPost) {
    AbstractLoi oldLoi = getCurrentLoi();
    java.util.List<AbstractLoi> lois = new ArrayList<>(loiMesuresPost.getLoiMesuresPost().getLois());
    lois.sort(ObjetNommeByNameComparator.INSTANCE);
    int idx = Math.max(0, lois.indexOf(oldLoi));
    cbLois.setModel(new DefaultComboBoxModel(lois.toArray(new AbstractLoi[0])));
    if (idx >= 0 && cbLois.getModel().getSize() > 0) {
      cbLois.setSelectedIndex(idx);
      currentLoi = (AbstractLoi) cbLois.getSelectedItem();
    } else {
      currentLoi = null;
    }
  }

  private String validateNom() {
    return ValidationPatternHelper.isNameValidei18nMessage(txtNom.getText(), currentLoi.getType());
  }

  @Override
  public boolean valideModification() {
    if (currentLoi == null) {
      return false;
    }
    configController.setUpdating(true);
    boolean modificationDone = false;
    if (dataModified) {
      modificationDone = saveData();
      if (!modificationDone) {
        configController.setUpdating(false);
        return false;
      }
      if (getAttachedPerspective() != null) {
        getAttachedPerspective().setDirty(true);
      }
    }
    configController.setUpdating(false);
    boolean configModified = configController.saveCourbeConfig();
    if (configModified) {
      getAttachedPerspective().setDirty(true);
    }
    setModified(false);
    return modificationDone || configModified;
  }

  private boolean saveData() {
    boolean modificationDone = false;
    AbstractLoi cloned = currentLoi.cloneWithoutPoints();
    cloned.setNom(txtNom.getText());
    if (LoiDF.class.equals(cloned.getClass())) {
      ((LoiDF) cloned).setDateZeroLoiDF(dateDebutEditor.getTime());
    }
    CtuluLog valideLoi;
    cloned.setCommentaire(txtCommentaire.getText());

    //les valeurs a setter si nécessaie
    java.util.List<PtEvolutionFF> pointsInCourbe = null;
    java.util.List<PtEvolutionTF> pointsTFInCourbe = null;

    //on valide
    if (cloned instanceof Loi) {
      pointsInCourbe = loiUiController.getLoiModel().getPoints();
      final Loi clonedLoi = (Loi) cloned;
      clonedLoi.getEvolutionFF().setPtEvolutionFF(pointsInCourbe);
      valideLoi = ValidationHelperLoi.valideLoi(clonedLoi, getCcm());
    } else if (cloned instanceof LoiTF) {
      LHPTLoiSectionZCourbeModel modelTF = (LHPTLoiSectionZCourbeModel) loiUiController.getLoiModel();
      final LoiTF clonedLoi = (LoiTF) cloned;
      pointsTFInCourbe = modelTF.createEvolutionTF();
      clonedLoi.getEvolutionTF().setPtEvolutionTF(pointsTFInCourbe);
      LHPTContentValidator validator = new LHPTContentValidator();
      valideLoi = validator
          .validatLoi(service.getLoadedLoiMesure(getSelectedScenario()).getLoiMesuresPost(), service.getProjetService().getCcm(), clonedLoi);
    } else {
      return false;
    }

    String error = validateNom();
    if (error != null) {
      valideLoi.addSevereError(error);
    } else {
      //on cherche si le nom n'est pas deja utilisé
      final java.util.List<AbstractLoi> lois = service.getLoadedLoiMesure(getSelectedScenario()).getLoiMesuresPost().getLois();
      java.util.List<AbstractLoi> allLois = new ArrayList<>(lois);
      if (!isNewLoi) {
        allLois.remove(currentLoi);
      }
      Set<String> toSetOfId = TransformerHelper.toSetOfId(allLois);
      if (toSetOfId.contains(txtNom.getText().toUpperCase())) {
        valideLoi.addSevereError("validation.NameAlreadyUsed", txtNom.getText());
      }
    }
    if (!valideLoi.containsErrorOrSevereError()) {
      currentLoi.setCommentaire(txtCommentaire.getText());
      boolean nomChanged = currentLoi.getNom().equals(txtNom.getText());
      currentLoi.setNom(txtNom.getText());
      if (currentLoi instanceof Loi) {
        ((Loi) currentLoi).getEvolutionFF().setPtEvolutionFF(pointsInCourbe);
      } else if (currentLoi instanceof LoiTF) {
        ((LoiTF) currentLoi).getEvolutionTF().setPtEvolutionTF(pointsTFInCourbe);
      }
      if (LoiDF.class.equals(currentLoi.getClass())) {

        ((LoiDF) currentLoi).setDateZeroLoiDF(dateDebutEditor.getTime());
      }
      if (isNewLoi) {
        service.saveLoiAdded(getSelectedScenario(), currentLoi);
        updateComboBoxList(service.getLoadedLoiMesure(getSelectedScenario()));
      } else {
        service.saveLoiChanged(getSelectedScenario());
        if (nomChanged) {
          updateComboBoxList(service.getLoadedLoiMesure(getSelectedScenario()));
        }
      }
      modificationDone = true;
      isNewLoi = false;
      setModified(false);
      if (getAttachedPerspective() != null) {
        getAttachedPerspective().setDirty(true);
      }
    }
    if (valideLoi.isNotEmpty()) {
      LogsDisplayer.displayError(valideLoi, getName());
    }

    return modificationDone;
  }

  @Override
  public void cancelModification() {
    super.selectedScenarioChanged(getSelectedScenario());
    reloadCourbeConfig();
    setModified(false);
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueParametrageMesurePostTraitement", PerspectiveEnum.REPORT);
  }

  @Override
  public java.util.List<String> getComments() {
    return new TableExportCommentSupplier(false).getComments(getName());
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  @SuppressWarnings("unused")
  void writeProperties(java.util.Properties p) {
    //rien de persiste
  }

  @SuppressWarnings("unused")
  void readProperties(java.util.Properties p) {
    //rien de persiste
  }

  private void delete() {
    boolean ok = DialogHelper.showQuestion(NbBundle.getMessage(ParametrageMesurePostTraitementTopComponent.class, "DeleteConfirmation.Message", txtNom.getText()));
    if (ok) {
      //loi existante
      if (!isNewLoi) {
        DefaultComboBoxModel model = (DefaultComboBoxModel) cbLois.getModel();
        int idx = Math.max(0, model.getIndexOf(currentLoi) - 1);
        service.removeLoiInScenario(currentLoi, getSelectedScenario());
        currentLoi = null;
        //on sélectionne la loi précédente:
        final java.util.List<AbstractLoi> loiList = service.getLoadedLoiMesure(getSelectedScenario()).getLoiMesuresPost().getLois();
        if (idx < loiList.size()) {
          java.util.List<AbstractLoi> lois = new ArrayList<>(loiList);
          lois.sort(ObjetNommeByNameComparator.INSTANCE);
          currentLoi = lois.get(idx);
        }
        selectedScenarioChanged(getSelectedScenario());
      } else {
        //on sélectionne la loi disponible dans la combobox.
        currentLoi = (Loi) cbLois.getSelectedItem();
      }
      setModified(false);
      isNewLoi = false;
    }
  }

  @Override
  public void setModified(boolean modified) {
    dataModified = modified;
    if (configController != null) {
      super.setModified(modified || configController.isConfigModified());
    } else {
      super.setModified(modified);
    }
    duplicate.setEnabled(!modified && editable);
    create.setEnabled(!modified && editable);
    cbLois.setEnabled(!modified);
  }

  private void create() {
    java.util.List<EnumTypeLoi> loisType = Arrays.asList(EnumTypeLoi.LoiTQ, EnumTypeLoi.LoiTZ, EnumTypeLoi.LoiQZ, EnumTypeLoi.LoiSectionsZ);
    final JComboBox cb = new JComboBox(loisType.toArray(new EnumTypeLoi[0]));
    cb.setRenderer(new ObjetWithIdCellRenderer());
    cb.setSelectedItem(null);
    JPanel pn = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
    pn.add(new JLabel(NbBundle.getMessage(ParametrageMesurePostTraitementTopComponent.class, "CreateLoi.TypeLabel")));
    pn.add(cb);
    JButton createButtonHelp = SysdocUrlBuilder.createButtonHelp();
    pn.add(createButtonHelp);
    createButtonHelp.addActionListener(e -> {
      SysdocContrat sysdocContrat = Lookup.getDefault().lookup(SysdocContrat.class);
      String dialogHelpCtxId = SysdocUrlBuilder.getDialogHelpCtxId("creerNouvelleLoi", PerspectiveEnum.MODELLING);
      Object selectedItem = cb.getSelectedItem();
      if (selectedItem != null) {
        dialogHelpCtxId = SysdocUrlBuilder.addSignet(dialogHelpCtxId, selectedItem.toString());
      }
      sysdocContrat.display(dialogHelpCtxId);
    });
    if (DialogHelper.showQuestionOkCancel((String) create.getValue(Action.NAME), pn) && cb.getSelectedItem() != null) {
      EnumTypeLoi typeLoi = (EnumTypeLoi) cb.getSelectedItem();
      isNewLoi = true;
      currentLoi = LoiFactory.createLoi(typeLoi, getCcm());
      currentLoi.setNom(CruePrefix.getPrefix(currentLoi.getType()));
      updateWithLoi();
      setModified(true);
    }
  }

  private AbstractLoi getCurrentLoi() {
    return currentLoi;
  }

  private void updateWithLoi() {
    updating = true;
    AbstractLoi loi = getCurrentLoi();
    txtCommentaire.setText(StringUtils.EMPTY);
    txtNom.setText(StringUtils.EMPTY);
    if (loi != null) {
      txtCommentaire.setText(loi.getCommentaire());
      String desc = StringUtils.defaultString(loi.getNom());
      txtNom.setText(desc);
      setName(initName + BusinessMessages.ENTITY_SEPARATOR + desc);
      loiUiController.setLoi(loi, getCcm(), false);
      loiUiController.getLoiModel().addObserver((o, arg) -> setModified(true));
    } else {
      setName(initName);
      loiUiController.setLoi(null, getCcm(), false);
    }
    dateDebutEditor.setLoi(loi);
    loiUiController.setEditable(editable);
    applyCourbeConfig();
    updateAxeXFormatter();
    updating = false;
    updateActionEditable();
    setModified(false);
  }

  @Override
  protected void setEditable(boolean b) {
    super.setEditable(b);
    if (loiUiController != null) {
      for (EbliActionInterface action : loiUiController.getEditActions()) {
        action.setEnabled(b);
      }
    }
    updateActionEditable();
  }

  private void updateActionEditable() {
    boolean editable = isEditable();
    final boolean isLoiSelected = editable && getCurrentLoi() != null;
    dateDebutEditor.setEditable(isLoiSelected && getCurrentLoi().getType().isDateAbscisse());
    loiUiController.setEditable(isLoiSelected);
    txtCommentaire.setEditable(isLoiSelected);
    txtNom.setEditable(isLoiSelected);
    delete.setEnabled(isLoiSelected || isNewLoi);
    duplicate.setEnabled(isLoiSelected && !isNewLoi);
    create.setEnabled(editable && getSelectedScenario() != null && !isNewLoi);
  }

  private void dateDebModified() {
    if (getCurrentLoi() != null && dateDebutValue != null) {
      updateAxeXFormatter();
    }
  }

  private void reloadCourbeConfig() {
    configController.reloadCourbeConfig();
  }

  private void duplicate() {
    if (currentLoi == null) {
      return;
    }
    currentLoi = currentLoi.clonedWithoutUser();
    currentLoi.setNom(CruePrefix.getPrefix(currentLoi.getType()));
    isNewLoi = true;
    updateWithLoi();
    setModified(true);
  }

  private void applyCourbeConfig() {
    if (configController != null && !configController.isUpdating()) {
      configController.applyConfig();
    }
  }

  private void updateAxeXFormatter() {
    if (getCurrentLoi() != null) {
      ConfigLoi configLoi = getCcm().getConfLoi().get(getCurrentLoi().getType());
      if (configLoi.getVarAbscisse().getNature().isDuration()) {
        LocalDateTime dateZeroLoiDF = dateDebutEditor.getTime();
        if (dateZeroLoiDF == null || dateDebutValue == null) {
          loiUiController.configureAxeH(getCcm(), getCurrentLoi(), false);
        } else {
          ToStringTransformerDate transformer = new ToStringTransformerDate(dateZeroLoiDF);
          final CtuluNumberFormatI detailFormat = loiUiController.getAxeX().getSpecificDetailFormat();
          SecondValueEditor oldSecondValueEditor = (SecondValueEditor) loiUiController.getAxeX().getValueEditor();
          SecondValueEditor editor = new SecondValueEditor(transformer, oldSecondValueEditor.getFormatter());
          loiUiController.setAxeHSpecificFormats(transformer, detailFormat, editor);
        }
        loiUiController.getTableGraphePanel().updateState();
        loiUiController.getGraphe().fullRepaint();
      }
    }
  }

  protected class DateDebEditor {
    final LocalDateTimePanel pnDateDeb = new LocalDateTimePanel();
    final JCheckBox cbDateDeb = new JCheckBox();
    final JPanel jPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    boolean updating;

    protected DateDebEditor() {
      jPanel.add(cbDateDeb);
      cbDateDeb.addItemListener(e -> {
        updateState();
        modificationDone();
      });
      pnDateDeb.addObserver((o, arg) -> modificationDone());
      jPanel.add(pnDateDeb);
    }

    private void modificationDone() {
      if (!updating) {
        dateDebModified();
        setModified(true);
      }
    }

    void updateState() {
      pnDateDeb.setAllEnable(cbDateDeb.isSelected() && cbDateDeb.isEnabled());
    }

    protected JPanel getjPanel() {
      return jPanel;
    }

    LocalDateTime getTime() {
      return cbDateDeb.isSelected() ? pnDateDeb.getLocalDateTime() : null;
    }

    protected void setLoi(AbstractLoi loi) {
      updating = true;
      if (loi == null || !LoiDF.class.equals(loi.getClass())) {
        cbDateDeb.setSelected(false);
        pnDateDeb.setLocalDateTime(null);
        cbDateDeb.setEnabled(false);
        pnDateDeb.setAllEnable(false);
      } else {
        LoiDF loiDF = (LoiDF) loi;
        cbDateDeb.setSelected(loiDF.getDateZeroLoiDF() != null);
        pnDateDeb.setLocalDateTime(loiDF.getDateZeroLoiDF());
      }
      updating = false;
    }

    protected void setEditable(boolean editable) {
      cbDateDeb.setEnabled(editable);
      updateState();
    }
  }
}
