package org.fudaa.fudaa.crue.aoc.action;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractHelpAction;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;

@ActionID(category = "View",
        id = "org.fudaa.fudaa.crue.aocService.action.AocHelpAction")
@ActionRegistration(displayName = "#CTL_AocHelpAction", iconBase = AbstractHelpAction.HELP_ICON)
@ActionReference(path = "Actions/AOC", position = 100, separatorBefore = 99)
public final class AocHelpAction extends AbstractHelpAction {

    public AocHelpAction() {
        super(PerspectiveEnum.AOC);
    }


}
