package org.fudaa.fudaa.crue.aoc;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.aoc.validation.AocValidationHelper;
import org.fudaa.fudaa.crue.aoc.perspective.PerspectiveServiceAoc;
import org.fudaa.fudaa.crue.aoc.service.AocService;
import org.fudaa.fudaa.crue.common.AbstractTopComponent;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.UserSaveAnswer;
import org.fudaa.fudaa.crue.common.services.AbstractPerspectiveService;
import org.openide.explorer.view.OutlineView;
import org.openide.util.Lookup;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * @author deniger
 */
public abstract class AbstractAocEditeurTopComponent extends AbstractTopComponent {
  private static final String END_NAME_IF_MODIFIED = " *";
  @SuppressWarnings("FieldCanBeLocal")
  private final transient Lookup.Result<AocCampagne> aocCampagneResult;
  private final transient PerspectiveServiceAoc perspectiveServiceAoc = Lookup.getDefault().lookup(PerspectiveServiceAoc.class);
  protected boolean updating;//true si les donnees UI sont en train d'être modifiee
  protected final transient List<Action> editActions = new ArrayList<>();
  final transient List<JComponent> editComponent = new ArrayList<>();
  private boolean editable;
  protected final transient AocService aocService = Lookup.getDefault().lookup(AocService.class);
  private final transient FocusListener customfocusListener = new FocusAdapter() {
    @Override
    public void focusGained(FocusEvent e) {
      updateEditableState();
    }
  };
  /**
   * ecoute les changements d'état ( editable ou pas) de la perspective.
   */
  private final transient PropertyChangeListener stateListener = evt -> perspectiveStateChanged();
  private boolean modified;

  protected AbstractAocEditeurTopComponent(String initName) {
    setFocusable(true);
    setName(initName);
    aocCampagneResult = aocService.getLookup().lookupResult(AocCampagne.class);
    aocCampagneResult.addLookupListener(ev -> aocCoampagnedChanged());
  }

  protected abstract void aocCoampagnedChanged();

  @Override
  public void cancelModification() {
    aocCoampagnedChanged();
    setModified(false);
  }


  public AocValidationHelper getValidationHelper() {
    return aocService.getAocValidationHelper();
  }

  protected JPanel createCancelSaveButtons() {
    return createCancelSaveButtons(null);
  }

  private void updateTopComponentName() {
    String name = getName();
    if (name == null) {
      return;
    }
    if (modified && !name.endsWith(END_NAME_IF_MODIFIED)) {
      setName(name + END_NAME_IF_MODIFIED);
    } else if (!modified && name.endsWith(END_NAME_IF_MODIFIED)) {
      setName(StringUtils.removeEnd(name, END_NAME_IF_MODIFIED));
    }
  }

  @Override
  public boolean isModified() {
    return modified;
  }

  public void setModified(boolean modified) {
    if (updating) {
      return;
    }
    this.modified = modified;
    updateTopComponentName();
    if (buttonSave != null) {
      buttonSave.setEnabled(modified);
    }
    if (buttonCancel != null) {
      buttonCancel.setEnabled(modified);
    }
  }

  private void updateEditableState() {
    setEditable(
        WindowManager.getDefault().getRegistry().getActivated() == this && (perspectiveServiceAoc.isInEditMode()));
  }

  @Override
  public final void componentClosedDefinitly() {
    removeFocusListener(customfocusListener);
    writePreferences();
    componentClosedDefinitlyHandler();
  }

  protected void componentClosedDefinitlyHandler() {
  }

  @Override
  public final void componentClosedTemporarily() {
  }

  public boolean isEditable() {
    return editable;
  }

  protected void setEditable(boolean b) {

    this.editable = b;
    for (Action action : editActions) {
      action.setEnabled(b);
    }
    for (JComponent jComponent : editComponent) {
      jComponent.setEnabled(b);
    }
  }

  @Override
  public boolean canClose() {
    if (isModified()) {
      UserSaveAnswer askForSave = askForSave();
      if (UserSaveAnswer.CANCEL.equals(askForSave)) {
        return false;
      }
    }
    return super.canClose();
  }

  @Override
  public final void componentOpened() {
    addFocusListener(customfocusListener);
    super.componentOpened();
    readPreferences();
    componentOpenedHandler();
  }

  protected abstract OutlineView getView();

  protected void componentOpenedHandler() {
  }

  protected String getOutlinePrefixForPreferences() {
    return "outlineView";
  }

  private void readPreferences() {
    DialogHelper.readInPreferences(getView(), getOutlinePrefixForPreferences(), getClass());
  }

  private void writePreferences() {
    DialogHelper.writeInPreferences(getView(), getOutlinePrefixForPreferences(), getClass());
  }

  private void perspectiveStateChanged() {
    updateEditableState();
  }

  @Override
  protected void componentDeactivated() {
    UserSaveAnswer confirmSaveOrNot = askForSave();

    if (UserSaveAnswer.CANCEL.equals(confirmSaveOrNot)) {
      return;
    }
    perspectiveServiceAoc.removeStateListener(stateListener);
    setEditable(false);

    super.componentDeactivated();
  }

  @Override
  protected void componentActivated() {
    perspectiveServiceAoc.addStateListener(stateListener);
    updateEditableState();
    super.componentActivated();
  }

  public AbstractPerspectiveService getAttachedPerspective() {
    return perspectiveServiceAoc;
  }
}
