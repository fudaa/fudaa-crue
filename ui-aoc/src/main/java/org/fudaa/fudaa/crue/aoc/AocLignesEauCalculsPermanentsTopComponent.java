package org.fudaa.fudaa.crue.aoc;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculPermanent;
import org.fudaa.fudaa.crue.aoc.nodes.AbstractAocLoiCalculNode;
import org.fudaa.fudaa.crue.aoc.nodes.AocLigneEauCalculPermanentNode;
import org.fudaa.fudaa.crue.aoc.nodes.AocLigneEauCalculPermanentView;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultTopComponentParent;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Editeur Lois  Calculs Permanents
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.aoc//AocLignesEauCalculsPermanentsTopComponent//EN",
    autostore = false)
@TopComponent.Description(preferredID = AocLignesEauCalculsPermanentsTopComponent.TOPCOMPONENT_ID, persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = AocLignesEauCalculsPermanentsTopComponent.MODE, openAtStartup = false)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.aoc.AocLignesEauCalculsPermanentsTopComponent")
@TopComponent.OpenActionRegistration(displayName = "#CTL_AocLignesEauCalculsPermanentsTopComponent",
    preferredID = AocLignesEauCalculsPermanentsTopComponent.TOPCOMPONENT_ID)
@ActionReference(path = "Menu/Window/AOC", position = 3)
public final class AocLignesEauCalculsPermanentsTopComponent extends AbstractAocEditeurTopComponent implements ExplorerManager.Provider, DefaultTopComponentParent {
  public static final String MODE = "aoc-loisCalculsPermanents"; //NOI18N
  public static final String TOPCOMPONENT_ID = "AocLignesEauCalculsPermanentsTopComponent"; //NOI18N
  private final AocLigneEauCalculPermanentView outlineViewEditor;

  public AocLignesEauCalculsPermanentsTopComponent() {
    super(NbBundle.getMessage(AocLignesEauCalculsPermanentsTopComponent.class, "CTL_" + TOPCOMPONENT_ID));
    initComponents();
    setToolTipText(NbBundle.getMessage(AocLignesEauCalculsPermanentsTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    outlineViewEditor = new AocLigneEauCalculPermanentView(this);
    add(outlineViewEditor, BorderLayout.CENTER);
    setEditable(false);
    aocCoampagnedChanged();
    add(createCancelSaveButtons(), BorderLayout.SOUTH);
    associateLookup(ExplorerUtils.createLookup(outlineViewEditor.getExplorerManager(), getActionMap()));
  }

  @Override
  protected String getOutlinePrefixForPreferences() {
    //pour prendre en compte le changement de nom.
    return "outlineViewV2";
  }

  @Override
  public boolean valideModification() {
    if (!isModified()) {
      return true;
    }
    CtuluLog validatingResult = outlineViewEditor.getValidatingResult();
    if (validatingResult.isNotEmpty()) {
      LogsDisplayer.displayError(validatingResult, getName());
    }
    if (validatingResult.containsErrorOrSevereError()) {
      return false;
    }
    final List<AocLoiCalculPermanent> allNodesValues = outlineViewEditor.getAllNodesValues();
    aocService.setLoisCalculsParmanents(allNodesValues);
    setModified(false);
    return true;
  }

  @Override
  public OutlineView getView() {
    return outlineViewEditor.getView();
  }

  @Override
  protected void aocCoampagnedChanged() {
    List<AocLigneEauCalculPermanentNode> nodes = new ArrayList<>();
    final AocCampagne aoc = aocService.getCurrentAOC();
    if (aoc != null && aocService.getAocValidationHelper() != null) {
      //pour enregistrer le listener
      aocService.loadLoiMesureForCurrentScenario(evt -> updateLoiInNodes());
      String[] calculs = aocService.getAocValidationHelper().getAvailableCalculPermanentsArray();
      final Map<String, String> commentByCalculId = aocService.getAocValidationHelper().getCommentByCalculId();
      String[] lois = aocService.getAocValidationHelper().getAvailableLoisCalculPermanentsArray();
      for (AocLoiCalculPermanent aocLoiCalculPermanent : aoc.getDonnees().getLoisCalculsPermanents().getLois()) {
        AocLigneEauCalculPermanentNode node = new AocLigneEauCalculPermanentNode(aocLoiCalculPermanent.clone(), calculs, lois, commentByCalculId);
        nodes.add(node);
      }
    }
    outlineViewEditor.update(nodes);
  }

  private void updateLoiInNodes() {
    outlineViewEditor.setUpdating(true);
    final List<AocLigneEauCalculPermanentNode> allNodes = outlineViewEditor.getAllNodes();
    if (CollectionUtils.isNotEmpty(allNodes) && aocService.getAocValidationHelper() != null) {
      String[] lois = aocService.getAocValidationHelper().getAvailableLoisCalculPermanentsArray();
      lois = AbstractAocLoiCalculNode.addEmptyString(lois);
      for (AocLigneEauCalculPermanentNode node : allNodes) {
        node.updateLois(lois);
      }
    }
    outlineViewEditor.getValidatingResult();
    outlineViewEditor.setUpdating(false);
  }

  public ExplorerManager getExplorerManager() {
    return outlineViewEditor.getExplorerManager();
  }

  @Override
  protected void setEditable(boolean b) {
    super.setEditable(b);
    outlineViewEditor.setEditable(b);
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueLignesEauCalculsPermanents", PerspectiveEnum.AOC);
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  @SuppressWarnings("unused")
  void writeProperties(java.util.Properties p) {
    //rien a persister
  }

  @SuppressWarnings("unused")
  void readProperties(java.util.Properties p) {
    //rien a persister
  }
}
