/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.aoc.process;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.io.CrueAOCReaderWriter;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.joda.time.LocalDateTime;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;

import java.io.File;

/**
 * @author Chris
 */
public class SaveAocCampagneProcess implements ProgressRunnable<Boolean> {
  private final InstallationService installationService = Lookup.getDefault().lookup(InstallationService.class);
  private final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);
  private final File aocFile;
  private final AocCampagne aocCampagne;

  public SaveAocCampagneProcess(AocCampagne campagne) {
    this.aocFile = campagne.getAocFile();
    this.aocCampagne = campagne;
  }

  @Override
  public Boolean run(ProgressHandle handle) {
    final String auteur = installationService.getConnexionInformation().getCurrentUser();
    final LocalDateTime date = installationService.getConnexionInformation().getCurrentDate();

    if (this.aocCampagne.getAuteurCreation() == null) {
      this.aocCampagne.setAuteurCreation(auteur);
      this.aocCampagne.setDateCreation(date);
    } else {
      this.aocCampagne.setAuteurModification(auteur);
      this.aocCampagne.setDateModification(date);
    }
    final CoeurConfig coeurConfigDefault = configurationManagerService.getCoeurManager().getCoeurConfigDefault(configurationManagerService.getCoeurManager().getHigherGrammaire());
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueAOCReaderWriter aocReaderWriter = new CrueAOCReaderWriter("1.2", coeurConfigDefault.getCrueConfigMetier());
    final CrueIOResu<AocCampagne> otfaResu = new CrueIOResu<>(this.aocCampagne);
    return aocReaderWriter.writeXMLMetier(otfaResu, this.aocFile, log, null);
  }
}
