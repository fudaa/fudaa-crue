package org.fudaa.fudaa.crue.aoc;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.helper.AocParamCalcSorter;
import org.fudaa.dodico.crue.aoc.projet.AocParamCalc;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPermItem;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;
import org.fudaa.dodico.crue.metier.helper.CalcHelper;
import org.fudaa.fudaa.crue.aoc.nodes.AocListeVariationClimCalculsView;
import org.fudaa.fudaa.crue.aoc.nodes.AocParamCalcNode;
import org.fudaa.fudaa.crue.aoc.nodes.AocParamCalcParentNode;
import org.fudaa.fudaa.crue.aoc.service.AocService;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.node.NodeChildrenHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultTopComponentParent;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * visualisateur des Variations des CLimMs des Calculs.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.aoc//AocListeVariationClimCalculsTopComponent//EN",
    autostore = false)
@TopComponent.Description(preferredID = AocListeVariationClimCalculsTopComponent.TOPCOMPONENT_ID, persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = AocListeVariationClimCalculsTopComponent.MODE, openAtStartup = false)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.aoc.AocListeVariationClimCalculsTopComponent")
@TopComponent.OpenActionRegistration(displayName = "#CTL_AocListeVariationClimCalculsTopComponent",
    preferredID = AocListeVariationClimCalculsTopComponent.TOPCOMPONENT_ID)
@ActionReference(path = "Menu/Window/AOC", position = 6)
public final class AocListeVariationClimCalculsTopComponent extends AbstractAocEditeurTopComponent implements DefaultTopComponentParent, PropertyChangeListener {
  public static final String MODE = "aoc-listeCalculs"; //NOI18N
  public static final String TOPCOMPONENT_ID = "AocListeVariationClimCalculsTopComponent"; //NOI18N
  /**
   * 2eme id de persistence pour prendre en compte le changement de nom.
   */
  private final AocListeVariationClimCalculsView outlineViewEditor;

  public AocListeVariationClimCalculsTopComponent() {
    super(NbBundle.getMessage(AocListeVariationClimCalculsTopComponent.class, "CTL_" + TOPCOMPONENT_ID));
    initComponents();
    setToolTipText(NbBundle.getMessage(AocListeVariationClimCalculsTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    outlineViewEditor = new AocListeVariationClimCalculsView(this);

    add(outlineViewEditor, BorderLayout.CENTER);
    aocCoampagnedChanged();
    add(createCancelSaveButtons(), BorderLayout.SOUTH);
    associateLookup(ExplorerUtils.createLookup(outlineViewEditor.getExplorerManager(), getActionMap()));
    setEditable(false);
  }

  @Override
  public boolean valideModification() {
    if (!isModified()) {
      return true;
    }
    CtuluLog validatingResult = outlineViewEditor.getValidatingResult();
    if (validatingResult.isNotEmpty()) {
      LogsDisplayer.displayError(validatingResult, getName());
    }
    if (validatingResult.containsErrorOrSevereError()) {
      return false;
    }
    final List<AocParamCalc> allNodesValues = outlineViewEditor.getAllAocParamCalcNodesValues();
    aocService.setParaCalc(allNodesValues);
    setModified(false);
    return true;
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (AocService.PROPERTY_LOI_CALCUL_PERMANENT.equals(evt.getPropertyName())) {
      aocCoampagnedChanged();
      final List<AocParamCalc> allNodesValues = outlineViewEditor.getAllAocParamCalcNodesValues();
      aocService.setParaCalc(allNodesValues);
    }
  }

  @Override
  protected void componentOpenedHandler() {
    super.componentOpenedHandler();
    aocCoampagnedChanged();
    aocService.getPropertyChangeSupport().addPropertyChangeListener(AocService.PROPERTY_LOI_CALCUL_PERMANENT, this);
  }

  @Override
  protected void componentClosedDefinitlyHandler() {
    super.componentClosedDefinitlyHandler();
    aocService.getPropertyChangeSupport().removePropertyChangeListener(AocService.PROPERTY_LOI_CALCUL_PERMANENT, this);
  }

  @Override
  protected void aocCoampagnedChanged() {
    List<AocParamCalcParentNode> nodes = new ArrayList<>();
    if (aocService.getCurrentAOC() != null) {
      AocParamCalcSorter sortedValues = new AocParamCalcSorter(aocService.getCurrentAOC().getDonnees().getListeCalculs());
      final CrueConfigMetier ccm = aocService.getCcm();
      for (Map.Entry<String, List<CalcPseudoPermItem>> entry : aocService.getAocValidationHelper().getAvailableDclmByCalcul().entrySet()) {
        String nomCalcul = entry.getKey();
        final List<CalcPseudoPermItem> donCLimMs = entry.getValue();
        List<AocParamCalcNode> children = new ArrayList<>();
        for (DonCLimM donCLimM : donCLimMs) {
          final String nomEMH = donCLimM.getEmh().getNom();
          AocParamCalc value = sortedValues.getAndRemove(nomCalcul, nomEMH);
          if (value == null) {
            value = new AocParamCalc(nomCalcul, nomEMH);
          } else {
            value = new AocParamCalc(value);
          }
          CalcPseudoPermItem pseudPermItem = (CalcPseudoPermItem) donCLimM;
          String ccmProp = pseudPermItem.getCcmVariableName();
          ItemVariable property = ccm.getProperty(ccmProp);
          final String donCLimMValue = property.format(pseudPermItem.getValue(),
              DecimalFormatEpsilonEnum.PRESENTATION);
          children.add(
              new AocParamCalcNode(super.getAttachedPerspective(), value, CalcHelper.getSuffixe(donCLimM, aocService.getCcm()), donCLimMValue));
        }
        nodes.add(new AocParamCalcParentNode(NodeChildrenHelper.createChildren(children), nomCalcul));
      }
      final List<AocParamCalc> unknownAocParamCalcs = sortedValues.getAocParamCalcs();
      if (CollectionUtils.isNotEmpty(unknownAocParamCalcs)) {
        StringBuilder list = new StringBuilder();
        list.append("<ul>");
        for (AocParamCalc unknownAocParamCalc : unknownAocParamCalcs) {
          list.append("<li>");
          list.append(unknownAocParamCalc.getNom());
          list.append("</li>");
        }
        list.append("</ul>");
        DialogHelper.showWarn(getName(), MessagesAoc.getMessage("Aoc.DclmNotLoadedAndWillBeLost", list.toString()));
      }
    }
    outlineViewEditor.update(nodes);
  }

  @Override
  protected void setEditable(boolean b) {
    super.setEditable(b);
    outlineViewEditor.setEditable(b);
  }

  @Override
  public OutlineView getView() {
    return outlineViewEditor.getView();
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueListeVariationClimm", PerspectiveEnum.AOC);
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  @SuppressWarnings("unused")
  void writeProperties(java.util.Properties p) {
    // rien a persister
  }

  @SuppressWarnings("unused")
  void readProperties(java.util.Properties p) {
    // rien a persister
  }
}
