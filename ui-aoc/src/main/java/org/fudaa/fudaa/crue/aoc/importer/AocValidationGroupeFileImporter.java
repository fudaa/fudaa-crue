package org.fudaa.fudaa.crue.aoc.importer;

import org.fudaa.fudaa.crue.aoc.nodes.AocValidationGroupeNode;
import org.fudaa.fudaa.crue.common.helper.AbstractFileImporterProgressRunnable;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

import java.io.File;
import java.util.List;

/**
 * Handler pour importer un fichier csv ou excel.
 */
public class AocValidationGroupeFileImporter extends AbstractFileImporterProgressRunnable implements ProgressRunnable<List<AocValidationGroupeNode>> {
    /**
     * les lois
     */
    private final String[] lois;

    /**
     * @param file le fichier à importer
     */
    public AocValidationGroupeFileImporter(File file, String[] lois) {
        super(file);
        this.lois = lois;
    }

    @Override
    public List<AocValidationGroupeNode> run(ProgressHandle progressHandle) {
        String[][] values = null;
        if (file != null) {
            values = readFile();
        }
        if (values == null) {
            return null;
        }
        return AocValidationGroupeNodesImporter.extractValidationGroupe(lois, values);
    }
}
