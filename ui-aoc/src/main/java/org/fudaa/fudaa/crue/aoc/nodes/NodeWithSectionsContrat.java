package org.fudaa.fudaa.crue.aoc.nodes;

/**
 * * Contrat pour les noeuds pouvant fournir des sections
 *
 * @author deniger
 */
public interface NodeWithSectionsContrat {
    /**
     * @return les sections fournies par le node
     */
    String[] getSections();
}
