package org.fudaa.fudaa.crue.aoc.importer;

import org.fudaa.fudaa.crue.aoc.nodes.AocLoiStricklerNode;
import org.fudaa.fudaa.crue.common.helper.AbstractFileImporterProgressRunnable;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

import java.io.File;
import java.util.Collections;
import java.util.List;

/**
 * Handler pour importer un fichier csv ou excel.
 */
public class AocLoiStrickerFileImporter extends AbstractFileImporterProgressRunnable implements ProgressRunnable<List<AocLoiStricklerNode>> {
    /**
     * les lois
     */
    private final String[] loisFrottements;

    /**
     * @param file            le fichier à importer
     * @param loisFrottements les frottements disponibles
     */
    public AocLoiStrickerFileImporter(File file, String[] loisFrottements) {
        super(file);
        this.loisFrottements = loisFrottements;
    }

    @Override
    public List<AocLoiStricklerNode> run(ProgressHandle progressHandle) {
        String[][] values = null;
        if (file != null) {
            values = readFile();
        }
        if (values == null) {
            return Collections.emptyList();
        }
        return AocLoiStricklerNodesImporter.extractLoiStricklerNode(loisFrottements, values);
    }
}
