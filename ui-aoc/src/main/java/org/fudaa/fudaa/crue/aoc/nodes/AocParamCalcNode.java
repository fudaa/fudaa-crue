package org.fudaa.fudaa.crue.aoc.nodes;

import org.fudaa.dodico.crue.aoc.helper.AocParamCalcSorter;
import org.fudaa.dodico.crue.aoc.projet.AocParamCalc;
import org.fudaa.dodico.crue.aoc.projet.AocTempoCCM;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.aoc.ParametrageProfilSection;
import org.fudaa.fudaa.crue.common.property.AbstractNodeValueFirable;
import org.fudaa.fudaa.crue.common.property.PropertyStringReadOnly;
import org.fudaa.fudaa.crue.common.services.AbstractPerspectiveService;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.lookup.Lookups;

/**
 * Noeud affichant une ligne profil->section
 *
 * @author deniger
 */
public class AocParamCalcNode extends AbstractNodeValueFirable<AocParamCalc> implements Comparable<AocParamCalcNode> {
  private final String donCLimMValue;
  private final String dclmType;
  private final AbstractPerspectiveService abstractPerspectiveService;

  public AocParamCalcNode(AbstractPerspectiveService abstractPerspectiveService, AocParamCalc aocParamCalc, String dclmType,
                          String donCLimMValue) {
    super(Children.LEAF, Lookups.fixed(aocParamCalc));
    this.donCLimMValue = donCLimMValue;
    this.dclmType = dclmType;
    setNoImage();
    this.abstractPerspectiveService = abstractPerspectiveService;
    setName(aocParamCalc.getEmhRef() + " / " + dclmType);
  }

  @Override
  public int compareTo(AocParamCalcNode o) {
    if (o == this) {
      return 0;
    }
    if (o == null) {
      return 1;
    }
    int res = getMainValue().getCalculRef().compareTo(o.getMainValue().getCalculRef());
    if (res != 0) {
      return res;
    }
    res = getMainValue().getEmhRef().compareTo(o.getMainValue().getEmhRef());
    if (res != 0) {
      return res;
    }
    res = Double.valueOf(getMainValue().getDeltaQ()).compareTo(Double.valueOf(o.getMainValue().getDeltaQ()));
    return res;
  }

  public String getDclmType() {
    return dclmType;
  }

  public String getDonCLimMValue() {
    return donCLimMValue;
  }

  @Override
  public String getDataAsString() {
    final AocParamCalc aocParamCalc = getMainValue();
    return aocParamCalc.getCalculRef()
        + "\t" + aocParamCalc.getEmhRef() + "\t" + getDclmType() + "\t" + getDonCLimMValue() + "\t" + aocParamCalc.getDeltaQ();
  }

  @Override
  public boolean canCut() {
    return false;
  }

  @Override
  public boolean canDestroy() {
    return false;
  }

  @Override
  public boolean isEditMode() {
    return abstractPerspectiveService == null || abstractPerspectiveService.isInEditMode();
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(getMainValue()));
  }

  @Override
  protected Sheet createSheet() {
    Sheet res = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setName(ParametrageProfilSection.class.getSimpleName());
    set.setDisplayName(BusinessMessages.geti18nForClass(AocParamCalc.class));
    AocParamCalc aocParamCalc = getMainValue();
    res.put(set);
    PropertyStringReadOnly valueProperty = new PropertyStringReadOnly(donCLimMValue, AocNodeHelper.PROP_VALUE,
        AocNodeHelper.getValueDisplay(), AocNodeHelper.getValueDisplay());
    //important pour retrouver la valeur par la suite
    set.put(valueProperty);

    AocNodeHelper.createProperty(this, set, aocParamCalc, AocNodeHelper.PROP_DELTAQ_VALUE, AocNodeHelper.getDeltaQDisplay());

    return res;
  }

  @Override
  public AocParamCalc getMainValue() {
    return getLookup().lookup(AocParamCalc.class);
  }

  public boolean updateWith(AocParamCalcSorter sorter) {
    if (sorter != null) {
      final AocParamCalc mainValue = getMainValue();
      final AocParamCalc aocParamCalc = sorter.get(mainValue.getCalculRef(), mainValue.getEmhRef());
      if (aocParamCalc != null && !AocTempoCCM.getDeltaQ().getEpsilon().isSame(mainValue.getDeltaQ(), aocParamCalc.getDeltaQ())) {
        mainValue.setDeltaQ(aocParamCalc.getDeltaQ());
        return true;
      }
    }
    return false;
  }
}
