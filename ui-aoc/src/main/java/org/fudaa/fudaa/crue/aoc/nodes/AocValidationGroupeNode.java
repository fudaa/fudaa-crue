package org.fudaa.fudaa.crue.aoc.nodes;

import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculTransitoire;
import org.fudaa.dodico.crue.aoc.projet.AocValidationGroupe;
import org.fudaa.dodico.crue.aoc.projet.AocValidationLoi;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;
import org.fudaa.fudaa.crue.aoc.perspective.PerspectiveServiceAoc;
import org.fudaa.fudaa.crue.common.property.AbstractNodeValueFirable;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

/**
 * @author deniger
 */
public class AocValidationGroupeNode extends AbstractNodeValueFirable<AocValidationGroupe> implements NodeWithLoisContrat {
  private final String[] lois;
  private final PerspectiveServiceAoc abstractPerspectiveService = Lookup.getDefault().lookup(PerspectiveServiceAoc.class);

  public AocValidationGroupeNode(AocValidationGroupe validationGroupe, String[] lois) {
    super(Children.LEAF, Lookups.singleton(validationGroupe));
    //on ajoute une ligne vide au cas ou pas de sections sont choisies
    this.lois = lois;
    setNoImage();
    setName(validationGroupe.getNom());
    DefaultNodePasteType.setIndexUsedAsDisplayName(this);
  }

  @Override
  public boolean canCut() {
    return canDestroy();
  }

  @Override
  public boolean canDestroy() {
    return isEditMode();
  }

  @Override
  public String[] getLois() {
    return lois;
  }

  @Override
  public String getDataAsString() {
    final AocValidationGroupe aocValidationGroupe = getMainValue();
    return aocValidationGroupe.getNom() + "\t" + ToStringHelper.transformToNom(aocValidationGroupe.getLois()) + "\t" + aocValidationGroupe
        .getEffectifApprentissage() + "\t" + aocValidationGroupe
        .getCommentaire();
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(AocLoiCalculTransitoire.class));
  }

  @Override
  public boolean isEditMode() {
    return abstractPerspectiveService.isInEditMode();
  }

  @Override
  protected Sheet createSheet() {
    Sheet res = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setName(AocLoiCalculTransitoire.class.getSimpleName());
    set.setDisplayName(BusinessMessages.geti18nForClass(AocValidationGroupe.class));
    AocValidationGroupe aocValidationGroupe = getMainValue();
    res.put(set);
    AocNodeHelper.createProperty(this, set, aocValidationGroupe, AocNodeHelper.PROP_NOM, AocNodeHelper.getGroupeDisplay());
    AocValidationGroupeLoisRefProperty.createLoisProperty(this, set, aocValidationGroupe);
    AocNodeHelper
        .createProperty(this, set, aocValidationGroupe, AocNodeHelper.PROP_EFFECTIF_APPRENTISSAGE, AocNodeHelper.getEffectifApprentissageDisplay());
    AocNodeHelper.createProperty(this, set, aocValidationGroupe, AocNodeHelper.PROP_COMMENTAIRE, AocNodeHelper.getCommentaireDisplay());

    return res;
  }

  @Override
  public AocValidationGroupe getMainValue() {
    return getLookup().lookup(AocValidationGroupe.class);
  }

  public void replaceValueBy(AocValidationGroupeNode newValue) {
    if (newValue != null) {
      final AocValidationGroupe loiCalculPermanent = newValue.getMainValue();
      getMainValue().setNom(loiCalculPermanent.getNom());
      getMainValue().setClonedLois(loiCalculPermanent.getLois());
      getMainValue().setEffectifApprentissage(loiCalculPermanent.getEffectifApprentissage());
      getMainValue().setCommentaire(loiCalculPermanent.getCommentaire());
      firePropertyChange(AocNodeHelper.PROP_NOM, null, getMainValue().getNom());
      firePropertyChange(AocNodeHelper.PROP_LOIS, null, getMainValue().getLoisAsString());
      firePropertyChange(AocNodeHelper.PROP_EFFECTIF_APPRENTISSAGE, null, getMainValue().getEffectifApprentissage());
      firePropertyChange(AocNodeHelper.PROP_COMMENTAIRE, null, getMainValue().getCommentaire());
    }
  }

  /**
   * @return lois utilisables pour ce noeud: toutes les lois sauf celles utilisées par d'autre groupe.
   */
  public String[] getAvailableLois() {
    TreeSet<String> availableLois = new TreeSet<>(Arrays.asList(lois));
    final Node[] nodes = getParentNode().getChildren().getNodes();
    for (Node node : nodes) {
      if (node != null && node != this) {
        final List<AocValidationLoi> usedLois = ((AocValidationGroupeNode) node).getMainValue().getLois();
        for (AocValidationLoi aocValidationLoi : usedLois) {
          availableLois.remove(aocValidationLoi.getLoiRef());
        }
      }
    }
    return availableLois.toArray(new String[0]);
  }
}
