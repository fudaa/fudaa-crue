package org.fudaa.fudaa.crue.aoc.action;

import org.fudaa.fudaa.crue.aoc.AocListeVariationClimCalculsTopComponent;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;

import javax.swing.*;

/**
 * Ouverture {@link AocListeVariationClimCalculsTopComponent}
 */
@ActionID(category = "View",
    id = "org.fudaa.fudaa.crue.aocService.action.AocListeVariationClimCalculsAction")
@ActionRegistration(displayName = "#CTL_AocListeVariationClimCalculsAction")
@ActionReference(path = "Actions/AOC", position = 6)
public final class AocListeVariationClimCalculsAction extends AbstractAocTopComponentOpenerAction {
  public AocListeVariationClimCalculsAction() {
    super("CTL_AocListeVariationClimCalculsAction", AocListeVariationClimCalculsTopComponent.MODE, AocListeVariationClimCalculsTopComponent.TOPCOMPONENT_ID);
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new AocListeVariationClimCalculsAction();
  }
}
