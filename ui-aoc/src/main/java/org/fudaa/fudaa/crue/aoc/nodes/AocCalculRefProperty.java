package org.fudaa.fudaa.crue.aoc.nodes;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.aoc.projet.AbstractLoiCalcul;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Propriete pour l'affichage des calculs
 *
 * @author deniger
 */
public class AocCalculRefProperty extends PropertySupportReflection<String> {
  public AocCalculRefProperty(AbstractAocLoiCalculNode node, AbstractLoiCalcul instance) throws NoSuchMethodException {
    super(node, instance, String.class, "getCalculRef", "setCalculRef");
    final Map<String, String> commentByCalculId = node.getCommentByCalculId();
    setTags(node.getCalculs());
    Map<String, Object> translations = new HashMap<>();
    for (String calcul : node.getCalculs()) {
      translations.put(calcul, calcul);
    }
    for (Map.Entry<String, String> entry : commentByCalculId.entrySet()) {
      final String calculName = entry.getKey();
      translations.put(calculName + " - " + entry.getValue(), calculName);
    }
    setTranslationForObjects(translations);
    setUsei18nAsTag(true);
  }

  @Override
  protected void valueUpdated(String val) {
    String shortDes = getNameAndComment(val);
    setShortDescription(shortDes);
  }

  private String getNameAndComment(String val) {
    String shortDes = val;
    final Map<String, String> commentByCalculId = ((AbstractAocLoiCalculNode) node).getCommentByCalculId();
    String comment = commentByCalculId.get(val);
    if (!StringUtils.isBlank(comment)) {
      shortDes = shortDes + " - " + comment;
    }
    return shortDes;
  }

  @Override
  public void setValue(String newValue) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    super.setValue(newValue);
  }

}
