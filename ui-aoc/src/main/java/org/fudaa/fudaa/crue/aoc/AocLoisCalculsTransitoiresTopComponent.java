package org.fudaa.fudaa.crue.aoc;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculTransitoire;
import org.fudaa.fudaa.crue.aoc.nodes.AbstractAocLoiCalculNode;
import org.fudaa.fudaa.crue.aoc.nodes.AocLoiCalculTansitoireNode;
import org.fudaa.fudaa.crue.aoc.nodes.AocLoiCalculTransitoireView;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultTopComponentParent;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Editeur Lois  Calculs Transitoires
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.aoc//AocLoisCalculsTransitoiresTopComponent//EN",
    autostore = false)
@TopComponent.Description(preferredID = AocLoisCalculsTransitoiresTopComponent.TOPCOMPONENT_ID, persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = AocLoisCalculsTransitoiresTopComponent.MODE, openAtStartup = false)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.aoc.AocLoisCalculsTransitoiresTopComponent")
@TopComponent.OpenActionRegistration(displayName = "#CTL_AocLoisCalculsTransitoiresTopComponent",
    preferredID = AocLoisCalculsTransitoiresTopComponent.TOPCOMPONENT_ID)
@ActionReference(path = "Menu/Window/AOC", position = 4)
public final class AocLoisCalculsTransitoiresTopComponent extends AbstractAocEditeurTopComponent implements ExplorerManager.Provider, DefaultTopComponentParent {
  public static final String MODE = "aoc-loisCalculsTransitoires"; //NOI18N
  public static final String TOPCOMPONENT_ID = "AocLoisCalculsTransitoiresTopComponent"; //NOI18N
  private final transient AocLoiCalculTransitoireView outlineViewEditor;

  public AocLoisCalculsTransitoiresTopComponent() {
    super(NbBundle.getMessage(AocLoisCalculsTransitoiresTopComponent.class, "CTL_" + TOPCOMPONENT_ID));
    initComponents();
    setToolTipText(NbBundle.getMessage(AocLoisCalculsTransitoiresTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    outlineViewEditor = new AocLoiCalculTransitoireView(this);
    add(outlineViewEditor, BorderLayout.CENTER);
    setEditable(false);
    aocCoampagnedChanged();
    add(createCancelSaveButtons(), BorderLayout.SOUTH);
    associateLookup(ExplorerUtils.createLookup(outlineViewEditor.getExplorerManager(), getActionMap()));
  }

  @Override
  protected void setEditable(boolean b) {
    super.setEditable(b);
    outlineViewEditor.setEditable(b);
  }

  @Override
  public OutlineView getView() {
    return outlineViewEditor.getView();
  }

  private void updateLoiInNodes() {
    outlineViewEditor.setUpdating(true);
    final List<AocLoiCalculTansitoireNode> allNodes = outlineViewEditor.getAllNodes();
    if (CollectionUtils.isNotEmpty(allNodes) && aocService.getAocValidationHelper() != null) {
      String[] availableLois = aocService.getAocValidationHelper().getAvailableLoisCalculsTransitoiresArray();
      availableLois = AbstractAocLoiCalculNode.addEmptyString(availableLois);
      for (AocLoiCalculTansitoireNode node : allNodes) {
        node.updateLois(availableLois);
      }
    }
    outlineViewEditor.getValidatingResult();
    outlineViewEditor.setUpdating(false);
  }

  @Override
  public boolean valideModification() {
    if (!isModified()) {
      return true;
    }
    CtuluLog validatingResult = outlineViewEditor.getValidatingResult();
    if (validatingResult.isNotEmpty()) {
      LogsDisplayer.displayError(validatingResult, getName());
    }
    if (validatingResult.containsErrorOrSevereError()) {
      return false;
    }
    final List<AocLoiCalculTransitoire> allNodesValues = outlineViewEditor.getAllNodesValues();
    aocService.setLoisCalculsTransitoires(allNodesValues);
    setModified(false);
    return true;
  }

  @Override
  protected void aocCoampagnedChanged() {
    //la campagne est modifiée: on recréé les noeuds et on les recharge
    java.util.List<AocLoiCalculTansitoireNode> nodes = new ArrayList<>();
    final AocCampagne aoc = aocService.getCurrentAOC();
    if (aoc != null && aocService.getAocValidationHelper() != null) {
      String[] calculs = aocService.getAocValidationHelper().getAvailableCalculTransitoiresArray();
      String[] lois = aocService.getAocValidationHelper().getAvailableLoisCalculsTransitoiresArray();
      String[] sections = aocService.getAocValidationHelper().getAvailableSectionsArray();
      Map<String, String> commentByCalculId = aocService.getAocValidationHelper().getCommentByCalculId();
      aocService.loadLoiMesureForCurrentScenario(evt -> updateLoiInNodes());
      for (AocLoiCalculTransitoire aocLoiCalculTransitoire : aoc.getDonnees().getLoisCalculsTransitoires().getLois()) {
        AocLoiCalculTansitoireNode node = new AocLoiCalculTansitoireNode(aocLoiCalculTransitoire.clone(), calculs, lois, sections, commentByCalculId);
        nodes.add(node);
      }
    }
    outlineViewEditor.update(nodes);
  }

  public ExplorerManager getExplorerManager() {
    return outlineViewEditor.getExplorerManager();
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueLoisCalculsTransitoires", PerspectiveEnum.AOC);
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  @SuppressWarnings("unused")
  void writeProperties(java.util.Properties p) {
    // rien a persister
  }

  @SuppressWarnings("unused")
  void readProperties(java.util.Properties p) {
    // rien a persister
  }
}
