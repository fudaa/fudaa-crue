package org.fudaa.fudaa.crue.aoc.importer;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.aoc.projet.AocLoiStrickler;
import org.fudaa.fudaa.crue.aoc.nodes.AocLigneEauCalculPermanentNode;
import org.fudaa.fudaa.crue.aoc.nodes.AocLigneEauCalculPermanentView;
import org.fudaa.fudaa.crue.aoc.nodes.AocLoiStricklerNode;
import org.fudaa.fudaa.crue.common.swing.CommonGuiLib;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Methodes utilitaires pour importer des lois stricker
 */
public class AocLoiStricklerNodesImporter {
    private AocLoiStricklerNodesImporter() {
    }

    /**
     * @param loisFrottement les loi du scenario en cours
     * @return les noeuds créés à partir des données stockées dans le Clipboard
     */
    public static List<AocLoiStricklerNode> getPastData(String[] loisFrottement) {
        try {
            return extractLoiStricklerNode(loisFrottement, CommonGuiLib.getStringsFromClipboard());
        } catch (Exception exception) {
            Logger.getLogger(AocLoiStricklerNodesImporter.class.getName()).log(Level.WARNING, "read Data", exception);
        }
        return Collections.emptyList();
    }

    /**
     * parse les données <code>parse</code> pour construire des nodes {@link AocLigneEauCalculPermanentNode}
     *
     * @param loisFrottements les loi du lhpt en cours
     * @param parse           tableau de valeurs à parser
     * @return les noeuds
     */
    public static List<AocLoiStricklerNode> extractLoiStricklerNode(String[] loisFrottements,
                                                                    String[][] parse) {
        List<AocLoiStricklerNode> contents = new ArrayList<>();
        for (String[] lineContent : parse) {
            //on ignore la premier ligne
            if (ArrayUtils.isEmpty(parse) || StringUtils.startsWith(lineContent[0], "#") || AocLigneEauCalculPermanentView.FIRST_COLUMN_NAME_SELECTION
                    .equals(lineContent[0])) {
                continue;
            }
            int offsetColumn = 0;
            //permet de prendre en compte le cas ou la première colonne est un indice.
            if (lineContent.length >= 5 && !StringUtils.isBlank(lineContent[4])) {
                offsetColumn = 1;
            }
            if (lineContent.length >= 4 + offsetColumn) {
                String loiRef = StringUtils.trim(lineContent[offsetColumn]);
                AocLoiStrickler aocLoiStrickler = new AocLoiStrickler();
                aocLoiStrickler.setLoiRef(loiRef);
                aocLoiStrickler.setMin(AocLoiCalculPermanentNodesImporter.getInteger(StringUtils.trim(lineContent[1 + offsetColumn]), 0));
                aocLoiStrickler.setIni(AocLoiCalculPermanentNodesImporter.getInteger(StringUtils.trim(lineContent[2 + offsetColumn]), 0));
                aocLoiStrickler.setMax(AocLoiCalculPermanentNodesImporter.getInteger(StringUtils.trim(lineContent[3 + offsetColumn]), 0));
                AocLoiStricklerNode node = new AocLoiStricklerNode(aocLoiStrickler, loisFrottements);
                contents.add(node);
            }
        }
        return contents;
    }
}
