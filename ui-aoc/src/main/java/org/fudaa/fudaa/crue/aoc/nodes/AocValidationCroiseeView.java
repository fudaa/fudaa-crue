package org.fudaa.fudaa.crue.aoc.nodes;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculTransitoire;
import org.fudaa.dodico.crue.aoc.projet.AocValidationGroupe;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.fudaa.crue.aoc.AocValidationCroiseeTopComponent;
import org.fudaa.fudaa.crue.aoc.importer.AocValidationGroupeFileImporter;
import org.fudaa.fudaa.crue.aoc.importer.AocValidationGroupeNodesImporter;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.view.DefaultEditableOutlineViewEditor;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Vue gerant les noeud {@link AocLoiCalculTransitoire}
 *
 * @author deniger
 */
public class AocValidationCroiseeView extends DefaultEditableOutlineViewEditor<AocValidationCroiseeTopComponent, AocValidationGroupe, AocValidationGroupeNode> {
    public AocValidationCroiseeView(AocValidationCroiseeTopComponent parent) {
        super(parent);
    }

    @Override
    public void setEditable(boolean editable) {
        super.setEditable(editable);
        btAdd.setEnabled(editable && ArrayUtils.isNotEmpty(getAvailableLois()));
        btPaste.setEnabled(btAdd.isEnabled());
        btImport.setEnabled(btPaste.isEnabled());
    }

    /**
     * Remplace tout le contenu du tableau par le presse-papier
     */
    protected void replaceAllByClipboardData() {
        if (!this.editable) {
            return;
        }
        List<AocValidationGroupeNode> pastData = AocValidationGroupeNodesImporter
                .getPastData(getAvailableLois());
        if (!pastData.isEmpty()) {
            update(pastData);
        }
        DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
        getTopComponentParent().setModified(true);
    }

    /**
     * Remplace tout le contenu du tableau par le contenu d'un fichier
     */
    protected void importData() {
        if (!this.editable) {
            return;
        }

        CtuluFileChooser fileChooser = AocValidationGroupeFileImporter.getImportXLSCSVFileChooser();

        final int res = fileChooser.showDialog(CtuluUIForNetbeans.DEFAULT.getParentComponent(), NbBundle.getMessage(
                AocValidationCroiseeView.class, "Import.DisplayName"));
        if (res == JFileChooser.APPROVE_OPTION) {
            final List<AocValidationGroupeNode> result = CrueProgressUtils.showProgressDialogAndRun(
                    new AocValidationGroupeFileImporter(fileChooser.getSelectedFile(), getAvailableLois()),
                    NbBundle.getMessage(
                            AocValidationCroiseeView.class, "Import.DisplayName"));
            if (CollectionUtils.isEmpty(result)) {
                DialogHelper.showWarn(NbBundle.getMessage(getClass(), "Import.NoDataFound"));
            } else {
                update(result);
                // on force la modif de la fenêtre pour pouvoir enregistrer
                getTopComponentParent().setModified(true);
            }
        }
    }

    @Override
    protected void addElement() {
        //on va essayer d'ajoute un élément configuré avec une loi non associée
        //les lois deja utilisées dans l'editeur
        AocValidationGroupe aocValidationGroupe = new AocValidationGroupe();
        aocValidationGroupe.setNom(findNewName(getAllNodesValues()));
        aocValidationGroupe.setCommentaire("");
        final AocValidationGroupeNode node = new AocValidationGroupeNode(aocValidationGroupe, getAvailableLois());
        Children children = getExplorerManager().getRootContext().getChildren();
        children.add(new Node[]{node});
        DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
    }

    private String findNewName(List<AocValidationGroupe> allNodesValues) {
        final Set<String> usedNames = TransformerHelper.toSetNom(allNodesValues);
        String prefix = "Groupe ";

        if (usedNames.isEmpty()) {
            return prefix + CtuluLibString.UN;
        }
        for (int i = 0; i < usedNames.size() + 1; i++) {
            String res = prefix + CtuluLibString.getString(i + 1);
            if (!usedNames.contains(res)) {
                return res;
            }
        }
        return "";
    }

    @Override
    protected void createOutlineView() {
        view = new OutlineView(FIRST_COLUMN_NAME_SELECTION);
        view.getOutline().setColumnHidingAllowed(false);
        view.getOutline().setRootVisible(false);
        view.setTreeSortable(false);
        view.setPropertyColumns(
                AocNodeHelper.PROP_NOM,
                AocNodeHelper.getGroupeDisplay(),
                AocNodeHelper.PROP_LOIS,
                AocNodeHelper.getLoisDisplay(),
                AocNodeHelper.PROP_EFFECTIF_APPRENTISSAGE,
                AocNodeHelper.getEffectifApprentissageDisplay(),
                AocNodeHelper.PROP_COMMENTAIRE,
                AocNodeHelper.getCommentaireDisplay());
    }

    /**
     * Copie le contenu du presse-papier dans le tableau-> remplace le contenu des cellules sélectionnées et ajoute si nécessaire des entrées.
     */
    public void pasteInSelectedNodes() {
        Node[] selectedNodes = getExplorerManager().getSelectedNodes();
        final List<AocValidationGroupeNode> pastData = AocValidationGroupeNodesImporter
                .getPastData(getAvailableLois());
        if (selectedNodes.length > 0) {
            final int nbSelectedNodeToModify = Math.min(pastData.size(), selectedNodes.length);
            for (int i = 0; i < nbSelectedNodeToModify; i++) {
                ((AocValidationGroupeNode) selectedNodes[i]).replaceValueBy(pastData.get(i));
            }
        }
        //on ajoute les autres noeuds
        List<AocValidationGroupeNode> newNodeToAdd = new ArrayList<>();
        for (int i = selectedNodes.length; i < pastData.size(); i++) {
            newNodeToAdd.add(pastData.get(i));
        }
        if (!newNodeToAdd.isEmpty()) {
            Children children = getExplorerManager().getRootContext().getChildren();
            children.add(newNodeToAdd.toArray(new Node[0]));
            DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
        }
        validateData();
    }

    private String[] getAvailableLois() {
        return getTopComponentParent().getValidationHelper().getAvailableLoisValidationCroisee();
    }

    public boolean validateData() {
        if (getTopComponentParent().getValidationHelper() != null) {
            final CtuluLog log = getValidatingResult();
            return !log.containsErrorOrSevereError();
        }
        return true;
    }

    public CtuluLog getValidatingResult() {
        return getTopComponentParent().getValidationHelper().validateValidationCroisee(getAllNodesForValidation());
    }
}
