package org.fudaa.fudaa.crue.aoc.importer;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculPermanent;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculTransitoire;
import org.fudaa.fudaa.crue.aoc.nodes.AocLigneEauCalculPermanentView;
import org.fudaa.fudaa.crue.aoc.nodes.AocLoiCalculTansitoireNode;
import org.fudaa.fudaa.crue.common.swing.CommonGuiLib;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Methodes utilitaires pour importer des noeuds profil-section
 */
public class AocLoiCalculTransitoireNodesImporter {
  private AocLoiCalculTransitoireNodesImporter() {
  }

  /**
   * @param calculs les calculs du scenario en cours
   * @param lois les loi du lhpt en cours
   * @return les noeuds issus des données stockées dans le Clipboard
   */
  public static List<AocLoiCalculTansitoireNode> getPastData(String[] calculs, String[] lois, String[] sections, Map<String, String> commentByCalculId) {
    try {
      return extractLoiCalculTransitoireNode(calculs, lois, sections, commentByCalculId, CommonGuiLib.getStringsFromClipboard());
    } catch (Exception exception) {
      Logger.getLogger(AocLoiCalculTransitoireNodesImporter.class.getName()).log(Level.WARNING, "read Data", exception);
    }
    return Collections.emptyList();
  }

  /**
   * parse les données <code>parse</code> pour construire des nodes {@link AocLoiCalculTansitoireNode}
   *
   * @param calculs les calculs du scenario en cours
   * @param lois les loi du lhpt en cours
   * @param sections les sections actives du scenario
   * @param parse tableau de valeurs à parser
   * @return les noeuds
   */
  public static List<AocLoiCalculTansitoireNode> extractLoiCalculTransitoireNode(String[] calculs, String[] lois, String[] sections, Map<String, String> commentByCalculId,
                                                                                 String[][] parse) {
    List<AocLoiCalculTansitoireNode> contents = new ArrayList<>();
    for (String[] lineContent : parse) {
      //on ignore la premier ligne
      if (ArrayUtils.isEmpty(parse) || StringUtils.startsWith(lineContent[0], "#") || AocLigneEauCalculPermanentView.FIRST_COLUMN_NAME_SELECTION
          .equals(lineContent[0])) {
        continue;
      }
      int offsetColumn = 0;
      //permet de prendre en compte le cas ou la première colonne est un indice.
      if (lineContent.length >= 5 && !StringUtils.isBlank(lineContent[4])) {
        offsetColumn = 1;
      }
      if (lineContent.length >= 4 + offsetColumn) {
        String calculRef = StringUtils.trim(lineContent[offsetColumn]);
        String loiRef = StringUtils.trim(lineContent[1 + offsetColumn]);
        String sectionRef = StringUtils.trim(lineContent[2 + offsetColumn]);
        int ponderation = AocLoiCalculPermanentNodesImporter
            .getInteger(StringUtils.trim(lineContent[3 + offsetColumn]), AocLoiCalculPermanent.DEFAULT_PONDERATION_VALUE);
        AocLoiCalculTransitoire calculTransitoire = new AocLoiCalculTransitoire();
        calculTransitoire.setCalculRef(calculRef);
        calculTransitoire.setLoiRef(loiRef);
        calculTransitoire.setSectionRef(sectionRef);
        calculTransitoire.setPonderation(ponderation);
        AocLoiCalculTansitoireNode node = new AocLoiCalculTansitoireNode(calculTransitoire, calculs, lois, sections, commentByCalculId);
        contents.add(node);
      }
    }
    return contents;
  }
}
