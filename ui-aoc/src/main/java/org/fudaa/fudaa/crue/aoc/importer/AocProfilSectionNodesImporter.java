package org.fudaa.fudaa.crue.aoc.importer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.aoc.ParametrageProfilSection;
import org.fudaa.fudaa.crue.aoc.nodes.ParametrageProfilSectionNode;
import org.fudaa.fudaa.crue.aoc.nodes.ParametrageProfilSectionView;
import org.fudaa.fudaa.crue.common.services.AbstractPerspectiveService;
import org.fudaa.fudaa.crue.common.swing.CommonGuiLib;

/**
 * Méthodes utilitaires pour importer des noeuds profil-section
 */
public class AocProfilSectionNodesImporter {
    /**
     * @param abstractPerspectiveService la perspective parente
     * @param sections                   les sections du scénario en cours
     * @return les noeuds issus des données stockées dans le Clipboard
     */
    public static List<ParametrageProfilSectionNode> getPastData(AbstractPerspectiveService abstractPerspectiveService, String[] sections) {
        try {
            String[][] parse = CommonGuiLib.getStringsFromClipboard();
            return extractProfilSectionNode(abstractPerspectiveService, sections, parse);
        } catch (Exception exception) {
        }
        return Collections.emptyList();
    }

    /**
     * parse les données <code>parse</code> pour construire des nodes AocProfilSectionNode
     *
     * @param abstractPerspectiveService la perspective parente
     * @param sections                   les sections du scenario en cours
     * @param parse                      tableau de valeurs à parser
     * @return les noeuds
     */
    public static List<ParametrageProfilSectionNode> extractProfilSectionNode(AbstractPerspectiveService abstractPerspectiveService, String[] sections,
                                                                      String[][] parse) {
        List<ParametrageProfilSectionNode> contents = new ArrayList<>();
        for (String[] lineContent : parse) {
            //on ignore la premier ligne
            if (ArrayUtils.isEmpty(parse) || StringUtils.startsWith(lineContent[0], "#") | ParametrageProfilSectionView.FIRST_COLUMN_NAME_SELECTION
                    .equals(lineContent[0])) {
                continue;
            }
            int offsetColumn = 0;
            //permet de prendre en compte le cas ou la première colonne est un indice.
            if (lineContent.length >= 3 && !StringUtils.isBlank(lineContent[2])) {
                offsetColumn = 1;
            }
            if (lineContent.length >= 2 + offsetColumn) {
                String pk = StringUtils.trim(lineContent[offsetColumn]);
                String sectionRef = StringUtils.trim(lineContent[1 + offsetColumn]);
                ParametrageProfilSection profilSection = new ParametrageProfilSection();
                profilSection.setSectionRef(sectionRef);
                profilSection.setPk(pk);
                ParametrageProfilSectionNode node = new ParametrageProfilSectionNode(abstractPerspectiveService, profilSection, sections);
                contents.add(node);
            }
        }
        return contents;
    }
}
