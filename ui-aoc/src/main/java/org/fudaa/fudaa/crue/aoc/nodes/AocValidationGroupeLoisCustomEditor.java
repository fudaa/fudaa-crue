package org.fudaa.fudaa.crue.aoc.nodes;

import com.jidesoft.swing.CheckBoxList;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.aoc.projet.AocValidationGroupe;
import org.fudaa.fudaa.crue.common.editor.CustomEditorAbstract;
import org.fudaa.fudaa.crue.common.helper.CheckBoxListPopupListener;
import org.openide.explorer.propertysheet.PropertyEnv;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Editeur specifique pour les lois d'un groupe de validation.
 *
 * @author deniger
 */
public class AocValidationGroupeLoisCustomEditor extends CustomEditorAbstract {
    private final String[] availableLois;
    private CheckBoxList list;
    private final boolean editable;

    public AocValidationGroupeLoisCustomEditor(AocValidationGroupeLoisRefProperty.AocLoisRefPropertyEditor editor, PropertyEnv env,
                                               final String[] availableLois, boolean editable) {
        super(editor, env);
        this.availableLois = availableLois;
        this.editable = editable;
    }

    @Override
    protected Component createComponent() {
        final DefaultListModel dataModel = new DefaultListModel();
        for (String loi : availableLois) {
            dataModel.addElement(loi);
        }
        list = new CheckBoxList(dataModel);
        new CheckBoxListPopupListener(list);
        Set<String> toSelect = new HashSet<>(Arrays.asList(AocValidationGroupe.stringToLois((String) editor.getValue())));
        final int nb = availableLois.length;
        for (int i = 0; i < nb; i++) {
            if (toSelect.contains(list.getModel().getElementAt(i))) {
                list.addCheckBoxListSelectedIndex(i);
            }
        }
        list.setEnabled(editable);
        return new JScrollPane(list);
    }

    @Override
    protected Object getPropertyValue() {
        if (list == null) {
            return StringUtils.EMPTY;
        }
        return AocValidationGroupe.loisArrayToString(list.getCheckBoxListSelectedValues());
    }
}
