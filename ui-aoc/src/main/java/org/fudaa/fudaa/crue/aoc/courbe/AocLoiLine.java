package org.fudaa.fudaa.crue.aoc.courbe;

import org.fudaa.fudaa.crue.loi.common.LoiLine;

/**
 * @author deniger
 */
public class AocLoiLine extends LoiLine {
    private String profil;

    public String getProfil() {
        return profil;
    }

    public void setProfil(String profil) {
        this.profil = profil;
    }
}
