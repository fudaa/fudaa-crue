package org.fudaa.fudaa.crue.aoc.nodes;

/**
 * Contrat pour les noeuds pouvant fournir des lois
 * @author deniger
 */
public interface NodeWithLoisContrat {
    /**
     *
     * @return les lois gérées par le node
     */
    String[] getLois();
}
