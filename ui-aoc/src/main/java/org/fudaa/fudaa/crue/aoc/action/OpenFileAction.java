/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.aoc.action;

import org.fudaa.fudaa.crue.aoc.service.AocService;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;

/**
 * Ouvrir une campagne AOC.
 */
public class OpenFileAction extends AbstractAction {
  private final transient AocService aocService = Lookup.getDefault().lookup(AocService.class);
  private final transient ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

  public OpenFileAction() {
    putValue(Action.NAME, NbBundle.getMessage(OpenFileAction.class, "OpenFileAction.ActionName"));
    putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(OpenFileAction.class, "OpenFileAction.ShortDescription"));
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    File home = configurationManagerService.getDefaultDataHome();
    final String name = (String) getValue(NAME);
    final FileChooserBuilder builder = new FileChooserBuilder(getClass()).setTitle(name).setDefaultWorkingDirectory(home).setApproveText(name);
    builder.setFileFilter(AocFileFilter.create());
    File toOpen = builder.showOpenDialog();
    if (toOpen != null) {
      if (toOpen.canWrite()) {
        aocService.open(toOpen);
      } else {
        // fichier en lecture seule : notification à l'utilisateur
        DialogHelper.showError(NbBundle.getMessage(OpenFileAction.class, "OpenFileAction.ErreurReadOnlyFile"));
      }
    }
  }
}
