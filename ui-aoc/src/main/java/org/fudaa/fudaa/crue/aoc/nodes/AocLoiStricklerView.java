package org.fudaa.fudaa.crue.aoc.nodes;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.dodico.crue.aoc.projet.AocLoiStrickler;
import org.fudaa.fudaa.crue.aoc.AocEditeurLoiStrickerTopComponent;
import org.fudaa.fudaa.crue.aoc.MessagesAoc;
import org.fudaa.fudaa.crue.aoc.importer.AocLoiStrickerFileImporter;
import org.fudaa.fudaa.crue.aoc.importer.AocLoiStricklerNodesImporter;
import org.fudaa.fudaa.crue.aoc.importer.AocProfilSectionFileImporter;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.view.DefaultEditableOutlineViewEditor;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Vue gerant les noeuds {@link AocLoiStrickler}
 *
 * @author deniger
 */
public class AocLoiStricklerView extends DefaultEditableOutlineViewEditor<AocEditeurLoiStrickerTopComponent, AocLoiStrickler, AocLoiStricklerNode> {
  public AocLoiStricklerView(AocEditeurLoiStrickerTopComponent parent) {
    super(parent);
  }

  @Override
  public void setEditable(boolean editable) {
    super.setEditable(editable);
    btAdd.setEnabled(editable && CollectionUtils.isNotEmpty(getTopComponentParent().getValidationHelper().getAvailableFrottements()));
    btPaste.setEnabled(btAdd.isEnabled());
    btImport.setEnabled(btPaste.isEnabled());
  }

  /**
   * Remplace tout le contenu du tableau par le presse-papier
   */
  protected void replaceAllByClipboardData() {
    if (!this.editable) {
      return;
    }
    List<AocLoiStricklerNode> pastData = AocLoiStricklerNodesImporter
        .getPastData(getAvailableFrottementsArray());
    if (!pastData.isEmpty()) {
      update(pastData);
    }
    DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
    getTopComponentParent().setModified(true);
  }

  private String[] getAvailableFrottementsArray() {
    return getTopComponentParent().getValidationHelper().getAvailableFrottementsArray();
  }

  /**
   * Remplace tout le contenu du tableau par le contenu d'un fichier
   */
  protected void importData() {
    if (!this.editable) {
      return;
    }

    CtuluFileChooser fileChooser = AocProfilSectionFileImporter.getImportXLSCSVFileChooser();

    final int res = fileChooser.showDialog(CtuluUIForNetbeans.DEFAULT.getParentComponent(), NbBundle.getMessage(
        AocLoiStricklerView.class, "Import.DisplayName"));
    if (res == JFileChooser.APPROVE_OPTION) {
      final List<AocLoiStricklerNode> result = CrueProgressUtils.showProgressDialogAndRun(
          new AocLoiStrickerFileImporter(fileChooser.getSelectedFile(), getAvailableFrottementsArray()),
          NbBundle.getMessage(
              AocLoiStricklerView.class, "Import.DisplayName"));
      if (CollectionUtils.isEmpty(result)) {
        DialogHelper.showWarn(NbBundle.getMessage(getClass(), "Import.NoDataFound"));
      } else {
        update(result);
        // on force la modif de la fenêtre pour pouvoir enregistrer
        getTopComponentParent().setModified(true);
      }
    }
  }

  @Override
  protected void addElement() {
    Children children = getExplorerManager().getRootContext().getChildren();
    //les calculs deja utilisés dans l'editeur
    Set<String> usedLois = getUsedLois();
    //les calculs disponibles
    final String[] availablesCalcul = getAvailableFrottementsArray();
    if (usedLois.size() == availablesCalcul.length) {
      DialogHelper.showWarn(MessagesAoc.getMessage("CantAddLoiAllUsed.Message"));
      return;
    }

    String loi = getFirstUsable(availablesCalcul, usedLois);
    AocLoiStrickler aocEchellesSection = new AocLoiStrickler(loi, 0, 0, 0);
    final AocLoiStricklerNode node = new AocLoiStricklerNode(aocEchellesSection, getAvailableFrottementsArray());
    children.add(new Node[]{node});
    DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
  }

  /**
   * @return liste des sections deja definies dans le composant
   */
  private Set<String> getUsedLois() {
    Set<String> usedCalcul = new HashSet<>();
    for (Node otherNode : getExplorerManager().getRootContext().getChildren().getNodes()) {
      usedCalcul.add(((AocLoiStricklerNode) otherNode).getMainValue().getLoiRef());
    }
    return usedCalcul;
  }

  @Override
  protected void createOutlineView() {
    view = new OutlineView(FIRST_COLUMN_NAME_SELECTION);
    view.getOutline().setColumnHidingAllowed(false);
    view.getOutline().setRootVisible(false);
    view.setTreeSortable(false);
    view.setPropertyColumns(
        AocNodeHelper.PROP_LOI,
        AocNodeHelper.getLoiDisplay(),
        AocNodeHelper.PROP_MIN,
        AocNodeHelper.getMinDisplay(),
        AocNodeHelper.PROP_INI,
        AocNodeHelper.getIniDisplay(),
        AocNodeHelper.PROP_MAX,
        AocNodeHelper.getMaxDisplay());
  }

  /**
   * Copie le contenu du presse-papier dans le tableau-> remplace le contenu des cellules sélectionnées et ajoute si nécessaire des entrées.
   */
  public void pasteInSelectedNodes() {
    Node[] selectedNodes = getExplorerManager().getSelectedNodes();
    final List<AocLoiStricklerNode> pastData = AocLoiStricklerNodesImporter
        .getPastData(getAvailableFrottementsArray());
    if (selectedNodes.length > 0) {
      final int nbSelectedNodeToModify = Math.min(pastData.size(), selectedNodes.length);
      for (int i = 0; i < nbSelectedNodeToModify; i++) {
        ((AocLoiStricklerNode) selectedNodes[i]).replaceValueBy(pastData.get(i));
      }
    }
    //on ajoute les autres noeuds
    List<AocLoiStricklerNode> newNodeToAdd = new ArrayList<>();
    for (int i = selectedNodes.length; i < pastData.size(); i++) {
      newNodeToAdd.add(pastData.get(i));
    }
    if (!newNodeToAdd.isEmpty()) {
      Children children = getExplorerManager().getRootContext().getChildren();
      children.add(newNodeToAdd.toArray(new Node[0]));
      DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
    }
    validateData();
  }

  public boolean validateData() {
    if (getTopComponentParent().getValidationHelper() != null) {
      final CtuluLog log = getValidatingResult();
      return !log.containsErrorOrSevereError();
    }
    return true;
  }

  public CtuluLog getValidatingResult() {
    return getTopComponentParent().getValidationHelper().validateLoiStricker(getAllNodesForValidation());
  }
}
