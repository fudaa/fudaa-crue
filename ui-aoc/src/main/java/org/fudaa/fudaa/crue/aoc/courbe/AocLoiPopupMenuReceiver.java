package org.fudaa.fudaa.crue.aoc.courbe;

import com.memoire.bu.BuMenuItem;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.fudaa.crue.aoc.MessagesAoc;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiCourbeModel;
import org.fudaa.fudaa.crue.loi.common.LoiPopupMenuReceiver;

import javax.swing.*;
import java.awt.*;

/**
 * @author deniger
 */
public class AocLoiPopupMenuReceiver extends LoiPopupMenuReceiver {
    private BuMenuItem clearPaste;
    private final AocMesureLoiUiController aocMesureLoiUiController;

    public AocLoiPopupMenuReceiver(EGFillePanel panel, AocMesureLoiUiController aocMesureLoiUiController) {
        super(panel);
        this.aocMesureLoiUiController = aocMesureLoiUiController;
    }

    @Override
    protected void addItems() {
        super.addItems();
        clearPaste = popupMenu.addMenuItem(MessagesAoc.getMessage("ClearAndPaste.Action"), "CLEAR_PASTE",
                false, e -> aocMesureLoiUiController.clearAndPaste());
        popupMenu.remove(clearPaste);
        popupMenu.add(clearPaste, 8);
    }

    @Override
    protected void updateItemStateBeforeShow() {
        super.updateItemStateBeforeShow();
        final EGCourbe egCourbe = getPanel().getGraphe().getSelectedComponent();
        if (egCourbe != null) {
            AbstractLoiCourbeModel courbeModel = (AbstractLoiCourbeModel) egCourbe.getModel();
            if (EnumTypeLoi.LoiSectionsZ.equals(courbeModel.getConfigLoi().getTypeLoi())) {
                getAlignPointMenuItem().setEnabled(false);
                getRefinedMenuItem().setEnabled(false);
            }
        }
        final boolean currentIsModifiable = egCourbe != null && egCourbe.getModel().isModifiable();
        clearPaste.setEnabled(currentIsModifiable);
        final Component[] components = super.popupMenu.getComponents();
        for (Component component : components) {
            if (component instanceof JMenuItem) {
                component.setEnabled(currentIsModifiable);
                final String actionCommand = ((JMenuItem) component).getActionCommand();
                if (actionCommand.toLowerCase().contains("export")) {
                    component.setEnabled(egCourbe != null);
                }
            }
        }
    }
}
