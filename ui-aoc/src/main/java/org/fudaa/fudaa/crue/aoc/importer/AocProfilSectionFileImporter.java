package org.fudaa.fudaa.crue.aoc.importer;

import org.fudaa.fudaa.crue.aoc.nodes.ParametrageProfilSectionNode;
import org.fudaa.fudaa.crue.common.helper.AbstractFileImporterProgressRunnable;
import org.fudaa.fudaa.crue.common.services.AbstractPerspectiveService;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

import java.io.File;
import java.util.List;

/**
 * Handler pour importer un fichier csv ou excel.
 */
public class AocProfilSectionFileImporter extends AbstractFileImporterProgressRunnable implements ProgressRunnable<List<ParametrageProfilSectionNode>> {
    /**
     * la perspective parente
     */
    private final AbstractPerspectiveService abstractPerspectiveService;
    /**
     * les sections valides du scenario sélectionné
     */
    private final String[] sections;

    /**
     * @param file le fichier à importer
     * @param abstractPerspectiveService la perspective parente ( utilisée par les noeuds pour savoir s'ils sont éditables ou pas).
     * @param sections les noms des sections
     */
    public AocProfilSectionFileImporter(File file, AbstractPerspectiveService abstractPerspectiveService, String[] sections) {
        super(file);
        this.abstractPerspectiveService = abstractPerspectiveService;
        this.sections = sections;
    }

    @Override
    public List<ParametrageProfilSectionNode> run(ProgressHandle progressHandle) {
        String[][] values = null;
        if (file != null) {
            values = readFile();
        }
        if (values == null) {
            return null;
        }
        return AocProfilSectionNodesImporter.extractProfilSectionNode(abstractPerspectiveService, sections, values);
    }
}
