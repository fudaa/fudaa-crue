package org.fudaa.fudaa.crue.aoc.service;

import gnu.trove.TDoubleArrayList;
import gnu.trove.TLongDoubleHashMap;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.aoc.exec.AocContentLoader;
import org.fudaa.dodico.crue.aoc.io.AocReadersHelper;
import org.fudaa.dodico.crue.aoc.validation.LHPTContentValidator;
import org.fudaa.dodico.crue.aoc.validation.LHPTLoiUpdater;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.aoc.AocEchellesSections;
import org.fudaa.dodico.crue.metier.aoc.LoiMesuresPost;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.LoiDF;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierLHPTSupport;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.result.TimeSimuConverter;
import org.fudaa.fudaa.crue.aoc.MessagesAoc;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.EMHProjetService;
import org.fudaa.fudaa.crue.loi.common.LoiConstanteCourbeModel;
import org.joda.time.LocalDateTime;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.lookup.ServiceProvider;
import org.openide.windows.WindowManager;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.*;

/**
 * Service gérant les données LHPT : chargement et sauvegarde
 * <table  border="1"  cellpadding="3" cellspacing="0"  summary="lookups">
 * <tr>
 * <th align="left">Lookup</th>
 * <th align="left">Commentaire</th>
 * <th align="left">Méthode utilisant le lookup</th>
 * </tr>
 * <tr>
 * <td></td>
 * <td></td>
 * <td></td>
 * </tr>
 * </table>
 *
 * @author deniger
 */
@ServiceProvider(service = LhptService.class)
public class LhptService implements LookupListener {
  /**
   * Constante pour les événements envoyés lorsque le projet est chargé
   */
  public static final String PROPERTY_PROJECT_LOADED = "projectLoaded";
  /**
   * Constante pour les événements envoyés lorsque le projet est chargé
   */
  private static final String PROPERTY_LHPT_MODIFIED = "lhptModified";
  /**
   * Interne: écoute lorsque le projet est chargé
   */
  @SuppressWarnings("FieldCanBeLocal")
  private final Lookup.Result<EMHProjet> resultat;
  /**
   * le service EMHProjetService
   */
  private final EMHProjetService service = Lookup.getDefault().lookup(EMHProjetService.class);
  /**
   * Support pour envoi d'evenements
   */
  private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
  /**
   * les données LoiMesure par scénario Id.
   */
  private final Map<String, LoiMesurePostState> dataByScenarioId = new HashMap<>();

  /**
   * enregistre listener sur le chargement de projet
   */
  public LhptService() {
    resultat = service.getLookup().lookupResult(EMHProjet.class);
    resultat.addLookupListener(this);
  }

  /**
   * @param scenario le scénario
   * @return la propriété à écouter pour les changement de Loi-Mesure concernant ce scénario
   */
  public static String getPropertyLoiMesure(ManagerEMHScenario scenario) {
    return getPropertyLoiMesure(scenario.getId());
  }

  /**
   * @param scenarioId l'id scénario
   * @return la propriété à écouter pour les changement de Loi-Mesure concernant ce scénario
   */
  private static String getPropertyLoiMesure(String scenarioId) {
    return "loiMesureChangedFor_" + scenarioId.toUpperCase();
  }

  public boolean isLoaded() {
    return !dataByScenarioId.isEmpty();
  }

  /**
   * @return le service EMHProjetService
   */
  public EMHProjetService getProjetService() {
    return service;
  }

  /**
   * @return la liste des LoiMesure modifiées
   */
  public List<LoiMesurePostState> getModifiedLoiMesurePostState() {
    List<LoiMesurePostState> res = new ArrayList<>();
    for (LoiMesurePostState loiMesurePostState : dataByScenarioId.values()) {
      if (loiMesurePostState.modified) {
        res.add(loiMesurePostState);
      }
    }
    return res;
  }

  public boolean isLoiMesureMesureModified() {
    for (LoiMesurePostState loiMesurePostState : dataByScenarioId.values()) {
      if (loiMesurePostState.modified) {
        return true;
      }
    }
    return false;
  }

  /**
   * Chargement si nécessaire des Loi-Mesures
   *
   * @param scenario le scenario
   * @param target si non null, sera inscrit en tant que listener des modifications sur les données loi-mesure du scénario <code>scenario</code>
   * @return les données chargées. Peut être null si erreur au chargement
   */
  public LoiMesurePostState loadLoiMesure(ManagerEMHScenario scenario, PropertyChangeListener target) {
    return loadLoiMesure(scenario, target, false);
  }

  /**
   * Chargement si nécessaire des Loi-Mesures
   *
   * @param scenario le scenario
   * @param target si non null, sera inscrit en tant que listener des modifications sur les données loi-mesure du scénario <code>scenario</code>
   * @param showValidation true si le résultat de validation doit être montrée
   * @return les données chargées. Peut être null si erreur au chargement
   */
  public LoiMesurePostState loadLoiMesure(ManagerEMHScenario scenario, PropertyChangeListener target, boolean showValidation) {
    if (target != null && !isAlreadyAListener(target, getPropertyLoiMesure(scenario))) {
      propertyChangeSupport.addPropertyChangeListener(getPropertyLoiMesure(scenario), target);
    }
    LoiMesurePostState res = getLoadedLoiMesure(scenario);
    if (res == null) {
      LoadMesurePostLoader loader = new LoadMesurePostLoader(scenario);
      final String actionTitle = MessagesAoc.getMessage("LhptAndSectionLoading", scenario.getNom());
      res = CrueProgressUtils
          .showProgressDialogAndRun(loader, actionTitle);

      if (showValidation) {
        validLoiMesure(res, actionTitle);
      }
      dataByScenarioId.put(scenario.getId(), res);
    }

    return res;
  }

  public LoiMesurePostState reloadFromDisk(ManagerEMHScenario scenario) {
    dataByScenarioId.remove(scenario.getId());
    LoiMesurePostState res = loadLoiMesure(scenario, null, true);
    propertyChangeSupport.firePropertyChange(getPropertyLoiMesure(scenario), true, false);
    fireLhptModifiedState();
    updateTopComponentLhptListener(scenario);
    return res;
  }

  /**
   * Valide les données LHPT.
   *
   * @param res les données
   * @param actionTitle le titre du dialog à utiliser si pas d'erreurs trouvées.
   */
  public void validLoiMesure(LoiMesurePostState res, String actionTitle) {
    if (res != null) {
      LHPTContentValidator validator = new LHPTContentValidator();
      final CtuluLogGroup logGroup = validator
          .valid(res.loiMesuresPost, new TreeSet<>(Arrays.asList(res.getSectionsSortedByName())), res.scenario.getNom(),
              getProjetService().getCcm());
      if (logGroup.containsSomething()) {
        LogsDisplayer.displayError(logGroup, actionTitle);
      } else {
        DialogHelper.showNotifyOperationTermine(MessagesAoc.getMessage("lhptValidation.NoErrorFound"));
      }
    }
  }

  /**
   * @param scenario le conteneur Scenaro
   * @return les données Loi-mesure chargées. Si pas de données renvoie null ( mais ne charge pas)
   */
  public LoiMesurePostState getLoadedLoiMesure(ManagerEMHScenario scenario) {
    if (scenario == null) {
      return null;
    }
    return dataByScenarioId.get(scenario.getId());
  }

  /**
   * Utile pour éviter d'inscrire plusieurs fois le même listener
   *
   * @param target la cible
   * @param propertyName le nom de la propriété ecoutée
   * @return true si <code>target</code> est déjà un listener
   */
  private boolean isAlreadyAListener(PropertyChangeListener target, String propertyName) {
    final PropertyChangeListener[] propertyChangeListeners = propertyChangeSupport.getPropertyChangeListeners(propertyName);
    for (PropertyChangeListener propertyChangeListener : propertyChangeListeners) {
      if (propertyChangeListener == target) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param scenario le scénario
   * @return les données loi-mesure chargée. Renvoi null si pas de projet chargé.
   */
  private LoiMesuresPost load(ManagerEMHScenario scenario) {
    if (service.getSelectedProject() == null) {
      return null;
    }
    final File lhptFile = FichierLHPTSupport.getLHPTFile(service.getSelectedProject(), scenario);
    if (lhptFile.exists()) {
      final CrueIOResu<LoiMesuresPost> read = AocContentLoader.loadLHPT(service.getSelectedProject(), scenario);
      if (read != null) {
        if (read.getAnalyse().isNotEmpty()) {
          LogsDisplayer.displayError(read.getAnalyse(), getMessageCantOpenLHPTFile());
        }
        final LoiMesuresPost metier = read.getMetier();
        if (metier == null) {
          return new LoiMesuresPost();
        }
        return metier;
      }
    }
    return new LoiMesuresPost();
  }

  private static String getMessageCantOpenLHPTFile() {
    return MessagesAoc.getMessage("CantOpenLHPTFile");
  }

  /**
   * Utilisé par les éditeurs lorsqu'un scénario n'est plus sélectionné.
   *
   * @param scenario le conteneur du scenario
   * @param target la cible
   */
  public void unloadLoiMesure(ManagerEMHScenario scenario, PropertyChangeListener target) {
    if (target != null) {
      final String propertyName = getPropertyLoiMesure(scenario);
      propertyChangeSupport.removePropertyChangeListener(propertyName, target);
    }
  }

  /**
   * Appelé par les éditeurs pour sauvegarder en mémoire les données modifiés-> modifie l'état modifié des lois-données et envoie un evt.
   *
   * @param scenario le scénario
   * @param echellesSections les échelles sections modifiées
   */
  public void saveLoiMesure(ManagerEMHScenario scenario, AocEchellesSections echellesSections) {
    final LoiMesurePostState loiMesuresPostState = getLoadedLoiMesure(scenario);
    if (loiMesuresPostState != null) {
      loiMesuresPostState.loiMesuresPost.setEchellesSections(echellesSections);
      new LHPTLoiUpdater().updateLoi(loiMesuresPostState.loiMesuresPost);
      loiMesuresPostState.modified = true;
      propertyChangeSupport.firePropertyChange(getPropertyLoiMesure(scenario), null, loiMesuresPostState);
    }
    //on doit mettre à jour les lois SectionTZ
  }

  /**
   * Sauve en mémoire la modification
   *
   * @param selectedScenario le scénario pour lequel la loi est ajoutée
   * @param currentLoi la loi à ajouter
   */
  public void saveLoiAdded(ManagerEMHScenario selectedScenario, AbstractLoi currentLoi) {
    final LoiMesurePostState loiMesure = getLoadedLoiMesure(selectedScenario);
    if (loiMesure != null) {
      loiMesure.modified = true;
      loiMesure.getLoiMesuresPost().addLoiAndSort(currentLoi);
      propertyChangeSupport.firePropertyChange(getPropertyLoiMesure(selectedScenario), null, loiMesure);
      fireLhptModifiedState();
      updateTopComponentLhptListener(selectedScenario);
    }
  }

  private void fireLhptModifiedState() {
    propertyChangeSupport.firePropertyChange(PROPERTY_LHPT_MODIFIED, true, false);
  }

  /**
   * Sauve en mémoire la modification
   *
   * @param selectedScenario le scenario
   */
  public void saveLoiChanged(ManagerEMHScenario selectedScenario) {
    final LoiMesurePostState loiMesure = getLoadedLoiMesure(selectedScenario);
    if (loiMesure != null) {
      loiMesure.modified = true;
      loiMesure.getLoiMesuresPost().loiUpdated();
      propertyChangeSupport.firePropertyChange(getPropertyLoiMesure(selectedScenario), null, loiMesure);
      updateTopComponentLhptListener(selectedScenario);
      fireLhptModifiedState();
    }
  }

  /**
   * permet de rafraichir les topcomponent ( profil en long) affichant des donnes lhpt
   */
  private void updateTopComponentLhptListener(ManagerEMHScenario scenario) {
    WindowManager.getDefault().getRegistry().getOpened().stream()
        .filter(topComponent -> topComponent instanceof LhptValuesDisplayer)
        .forEach(topComponent -> ((LhptValuesDisplayer) topComponent).lhptUpdated(scenario));
  }

  /**
   * Écoute les changements d'état du projet. Si ce n'est pas une action de rechargement ( fonctionnement interne)
   *
   * @param lookupEvent l'event
   */
  @Override
  public void resultChanged(LookupEvent lookupEvent) {
    //event interne-> on ignore
    if (service.isReloading()) {
      return;
    }
    //project loaded
    boolean isProjectLoaded = isProjectLoaded();
    propertyChangeSupport.firePropertyChange(PROPERTY_PROJECT_LOADED, !isProjectLoaded, isProjectLoaded);
    if (isProjectLoaded) {
      //rien a faire
    } else {
      for (String id : dataByScenarioId.keySet()) {
        String property = getPropertyLoiMesure(id);
        removeAllPropertyChangeListener(property);
      }
      dataByScenarioId.clear();
    }
    fireLhptModifiedState();
  }

  /**
   * @return la liste des scénarios actifs et de version crue 10 du projet courant
   */
  public List<ManagerEMHScenario> getListeScenarios() {
    if (service.getSelectedProject() == null) {
      return Collections.emptyList();
    }
    List<ManagerEMHScenario> scenarioCrue10 = new ArrayList<>();
    for (ManagerEMHScenario managerEMHScenario : service.getSelectedProject().getListeScenarios()) {
      if (managerEMHScenario.isCrue10() && managerEMHScenario.isActive()) {
        scenarioCrue10.add(managerEMHScenario);
      }
    }
    return scenarioCrue10;
  }

  /**
   * @param property la propriété pour laquelle on veut enlever tous les listeners
   */
  private void removeAllPropertyChangeListener(String property) {
    if (propertyChangeSupport.hasListeners(property)) {
      final PropertyChangeListener[] propertyChangeListeners = propertyChangeSupport.getPropertyChangeListeners(property);
      for (PropertyChangeListener prop : propertyChangeListeners) {
        propertyChangeSupport.removePropertyChangeListener(property, prop);
      }
    }
  }

  /**
   * @return true si projet chargés
   */
  public boolean isProjectLoaded() {
    return service.getSelectedProject() != null;
  }

  /**
   * @param listener un listener à ajouter sur l'état du projet
   * @see #PROPERTY_PROJECT_LOADED
   */
  public void addProjectListener(PropertyChangeListener listener) {
    if (isAlreadyAListener(listener, PROPERTY_PROJECT_LOADED)) {
      return;
    }
    propertyChangeSupport.addPropertyChangeListener(PROPERTY_PROJECT_LOADED, listener);
  }

  /**
   * @param listener un listener à ajouter sur l'état modifier de LHPT
   * @see #PROPERTY_LHPT_MODIFIED
   */
  public void addModifiedStateListener(PropertyChangeListener listener) {
    if (isAlreadyAListener(listener, PROPERTY_LHPT_MODIFIED)) {
      return;
    }
    propertyChangeSupport.addPropertyChangeListener(PROPERTY_LHPT_MODIFIED, listener);
  }

  /**
   * @param listener un listener à enlever sur l'état du projet
   * @see #PROPERTY_PROJECT_LOADED
   */
  public void removeProjectListener(PropertyChangeListener listener) {
    propertyChangeSupport.removePropertyChangeListener(PROPERTY_PROJECT_LOADED, listener);
  }

  public void reload() {
    List<String> loadedScenario = new ArrayList<>(dataByScenarioId.keySet());
    this.dataByScenarioId.clear();
    //on envoie des events à tous les listener afin qu'ils rechargent leur données
    for (String s : loadedScenario) {
      propertyChangeSupport.firePropertyChange(getPropertyLoiMesure(s), true, false);
    }
    fireLhptModifiedState();
  }

  public void close() {
    reload();
  }

  /**
   * sauvegarde dans un thread a part les données loi-mesure
   */
  public void saveOnFiles() {
    LhptSaver processor = new LhptSaver(this);
    CtuluLogGroup allLogs = null;
    final String action = MessagesAoc.getMessage("Lhpt.SaveAction");
    if (isLoiMesureMesureModified()) {
      allLogs = CrueProgressUtils.showProgressDialogAndRun(processor, action);
    }
    AocConfigService configService = Lookup.getDefault().lookup(AocConfigService.class);
    if (configService.isModified()) {
      CtuluLog configSaveLog = configService.save();
      if (configSaveLog != null) {
        if (allLogs == null) {
          allLogs = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
        }
        allLogs.addLog(configSaveLog);
      }
    }
    if (allLogs != null && allLogs.containsSomething()) {
      LogsDisplayer.displayError(allLogs, action);
    }
    fireLhptModifiedState();
  }

  public void removeLoiInScenario(AbstractLoi currentLoi, ManagerEMHScenario selectedScenario) {
    final LoiMesurePostState loadedLoiMesure = getLoadedLoiMesure(selectedScenario);
    if (loadedLoiMesure != null) {
      boolean removed = loadedLoiMesure.getLoiMesuresPost().removeLoi(currentLoi);
      if (removed) {
        loadedLoiMesure.modified = true;
        propertyChangeSupport.firePropertyChange(getPropertyLoiMesure(selectedScenario), null, loadedLoiMesure);
        fireLhptModifiedState();
        updateTopComponentLhptListener(selectedScenario);
      }
    }
  }

  /**
   * @param managerScenario le scenario
   * @return les noms des loiSectionZ disponibles
   */
  public List<String> getLoiSectionZFor(ManagerEMHScenario managerScenario) {
    List<String> res = new ArrayList<>();
    final LoiMesuresPost load = load(managerScenario);
    if (load != null) {
      load.getLois().stream().filter(loi -> EnumTypeLoi.LoiSectionsZ.equals(loi.getType())).forEach(loi -> res.add(loi.getNom()));
    }
    return res;
  }

  /**
   * @param managerScenario le scenario
   * @return les noms des loiT disponibles
   */
  public List<String> getLoiSectionTFor(ManagerEMHScenario managerScenario) {
    List<String> res = new ArrayList<>();
    final LoiMesuresPost load = load(managerScenario);
    if (load != null) {
      load.getLois().stream()
          .filter(loi -> EnumTypeLoi.LoiTQ.equals(loi.getType()) || EnumTypeLoi.LoiTZ.equals(loi.getType()))
          .forEach(loi -> res.add(loi.getNom()));
    }
    return res;
  }

  /**
   * @param key la clé de le ccourbe
   * @param managerScenario le scenario
   * @param loiName le nom de la courbe
   * @param timeByKey les pas de temps
   * @return la loi représentant les valeurs de la loiName pour les temps donnés
   */
  public LoiConstanteCourbeModel createLoiCourbeModel(EMHScenario scenario, Object key, ManagerEMHScenario managerScenario, String loiName,
                                                      List<Pair<ResultatTimeKey, Long>> timeByKey) {
    //chargement des fichiers si necessaire
    final LoiMesurePostState loiMesurePostState = loadLoiMesure(managerScenario, null);
    if (loiMesurePostState == null) {
      return null;
    }
    //recupération de la loi
    final LoiDF loiDF = loiMesurePostState.getLoiMesuresPost().getLoiDF(loiName);
    if (loiDF == null) {
      return null;
    }
    //le delta en millis entre le T0 du scenario et le T0 de la loi
    final long dateZeroInMillis = getDateZeroInMillis(scenario, loiDF);

    //et extraction des données
    TLongDoubleHashMap valuesByMillis = new TLongDoubleHashMap();
    loiDF.getEvolutionFF().getPtEvolutionFF().forEach(pt -> {
      final long timeInMillis = dateZeroInMillis + (long) (pt.getAbscisse() * 1000L);
      valuesByMillis.put(timeInMillis, pt.getOrdonnee());
    });
    TDoubleArrayList times = new TDoubleArrayList();
    TDoubleArrayList values = new TDoubleArrayList();
    timeByKey.forEach(entry -> {
      if (entry.first.isTransitoire() && valuesByMillis.contains(entry.second)) {
        times.add(entry.second);
        values.add(valuesByMillis.get(entry.second));
      }
    });
    //construction de la loi
    final ItemVariable variableOrdonnee = getProjetService().getSelectedProject().getPropDefinition().getLoiOrdonnee(loiDF.getType());
    return LoiConstanteCourbeModel.create(key, times, values, null, variableOrdonnee);
  }

  private long getDateZeroInMillis(EMHScenario scenario, LoiDF loiDF) {
    //prise en compte de DateZero
    long dateZeroInMillis = 0;
    final long datDebInMillisForScenario = TimeSimuConverter.getDatDebInMillis(scenario);
    if (loiDF.getDateZeroLoiDF() != null && datDebInMillisForScenario > 0) {
      dateZeroInMillis = loiDF.getDateZeroLoiDF().toDateTime().getMillis() - datDebInMillisForScenario;
    }
    return dateZeroInMillis;
  }

  /**
   * Classe contenant l'état des données loi mesure.
   */
  public static class LoiMesurePostState {
    /**
     * les données LoiMesures
     */
    protected final LoiMesuresPost loiMesuresPost;
    /**
     * true si les données sont modifiées
     */
    protected boolean modified;
    /**
     * les sections du scénario dans l'ordre "user" avec le xp correspondant.
     */
    protected LinkedHashMap<String, Double> sections;
    protected String[] sectionsSorted;
    /**
     * le scénario
     */
    protected final ManagerEMHScenario scenario;
    /**
     * information issue de pcal est utile pour les lois
     */
    protected LocalDateTime dateDebSce;

    /**
     * @param scenario le scénario
     * @param loiMesuresPost les données loi-mesures associées au scénario
     */
    protected LoiMesurePostState(ManagerEMHScenario scenario, LoiMesuresPost loiMesuresPost) {
      this.loiMesuresPost = loiMesuresPost;
      this.scenario = scenario;
    }

    public LocalDateTime getDateDebSce() {
      return dateDebSce;
    }

    protected void setDateDebSce(LocalDateTime dateDebSce) {
      this.dateDebSce = dateDebSce;
    }

    /**
     * @return les xp par section. Issue du scenario
     */
    public Map<String, Double> getSections() {
      return sections;
    }

    /**
     * @param sections noms des sections associées au scénario avec leur position
     */
    private void setSections(LinkedHashMap<String, Double> sections) {
      this.sections = sections;

      sectionsSorted = sections.keySet().toArray(new String[0]);
      Arrays.sort(sectionsSorted);
    }

    /**
     * @return les noms des sections associées au scénario
     */
    public String[] getSectionsSortedByName() {
      return sectionsSorted;
    }

    /**
     * @return les données loi-mesures
     */
    public LoiMesuresPost getLoiMesuresPost() {
      return loiMesuresPost;
    }
  }

  /**
   * Classe chargeant les donnes lhpt et les sections du scénario
   */
  private class LoadMesurePostLoader implements ProgressRunnable<LoiMesurePostState> {
    final ManagerEMHScenario scenario;

    protected LoadMesurePostLoader(ManagerEMHScenario scenario) {
      this.scenario = scenario;
    }

    /**
     * chargement des données Loi-Mesure
     *
     * @param progressHandle bar de progression
     * @return les données chargées. Peut être null
     */
    @Override
    public LoiMesurePostState run(ProgressHandle progressHandle) {
      LoiMesuresPost loiMesuresPost = load(scenario);
      LoiMesurePostState res = new LoiMesurePostState(scenario, loiMesuresPost);
      final AocReadersHelper aocReadersHelper = new AocReadersHelper(service.getSelectedProject());
      CrueIOResu<LinkedHashMap<String, Double>> readSection = aocReadersHelper.readSection(scenario);
      if (readSection.getAnalyse().containsErrors()) {
        LogsDisplayer.displayError(readSection.getAnalyse(), getMessageCantOpenLHPTFile());
      }

      res.setSections(readSection.getMetier());

      final CrueIOResu<LocalDateTime> readDateDebScenario = aocReadersHelper.readDateDebScenario(scenario);
      if (readDateDebScenario.getAnalyse().isNotEmpty()) {
        LogsDisplayer.displayError(readDateDebScenario.getAnalyse(), getMessageCantOpenLHPTFile());
      }
      res.setDateDebSce(readDateDebScenario.getMetier());

      return res;
    }
  }
}
