package org.fudaa.fudaa.crue.aoc.nodes;

import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculPermanent;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;

import java.util.Map;

/**
 * Node pour les donnees {@link AocLoiCalculPermanent}
 *
 * @author deniger
 */
public class AocLigneEauCalculPermanentNode extends AbstractAocLoiCalculNode<AocLoiCalculPermanent> {
    public AocLigneEauCalculPermanentNode(AocLoiCalculPermanent aocLoiCalculPermanent, String[] calculs, String[] lois, Map<String, String> commentByCalculId) {
        super(aocLoiCalculPermanent, calculs,lois,commentByCalculId);
    }

    /**
     * @return calculRef+TAB+loiRef+TAB+ponderation
     */
    @Override
    public String getDataAsString() {
        final AocLoiCalculPermanent echellesSection = getAocLoiCalculPermanent();
        return echellesSection.getCalculRef() + "\t" + echellesSection.getLoiRef() + "\t" + echellesSection.getPonderation();
    }

    @Override
    public HelpCtx getHelpCtx() {
        return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(AocLoiCalculPermanent.class));
    }

    @Override
    protected Sheet createSheet() {
        Sheet res = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        set.setName(AocLoiCalculPermanent.class.getSimpleName());
        set.setDisplayName(BusinessMessages.geti18nForClass(AocLoiCalculPermanent.class));
        AocLoiCalculPermanent aocLoiCalculPermanent = getAocLoiCalculPermanent();
        res.put(set);

        createCalculProperty(set, aocLoiCalculPermanent);
        AocLoiRefProperty.createLigneEauProperty(this, set, aocLoiCalculPermanent);
        createPonderationProperty(set, aocLoiCalculPermanent);

        return res;
    }

    public AocLoiCalculPermanent getAocLoiCalculPermanent() {
        return getLookup().lookup(AocLoiCalculPermanent.class);
    }

    @Override
    public AocLoiCalculPermanent getMainValue() {
        return getAocLoiCalculPermanent();
    }

    public void replaceValueBy(AocLigneEauCalculPermanentNode newValue) {
        if (newValue != null) {
            final AocLoiCalculPermanent loiCalculPermanent = newValue.getAocLoiCalculPermanent();
            getAocLoiCalculPermanent().setCalculRef(loiCalculPermanent.getCalculRef());
            getAocLoiCalculPermanent().setLoiRef(loiCalculPermanent.getLoiRef());
            getAocLoiCalculPermanent().setPonderation(loiCalculPermanent.getPonderation());
            firePropertyChange(AocNodeHelper.PROP_LOI, null, getAocLoiCalculPermanent().getLoiRef());
            firePropertyChange(AocNodeHelper.PROP_CALCUL, null, getAocLoiCalculPermanent().getCalculRef());
            firePropertyChange(AocNodeHelper.PROP_PONDERATION, null, getAocLoiCalculPermanent().getPonderation());
        }
    }
}
