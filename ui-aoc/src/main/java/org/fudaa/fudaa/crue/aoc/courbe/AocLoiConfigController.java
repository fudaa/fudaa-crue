/*
 GPL 2
 */
package org.fudaa.fudaa.crue.aoc.courbe;

import org.fudaa.ebli.courbe.EGAxe;
import org.fudaa.ebli.courbe.EGGrapheModelListener;
import org.fudaa.ebli.courbe.EGObject;
import org.fudaa.fudaa.crue.aoc.AocLoiTopComponentInterface;
import org.fudaa.fudaa.crue.aoc.service.AocConfigService;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiUiController;
import org.fudaa.fudaa.crue.loi.common.CourbeConfigUpdater;
import org.fudaa.fudaa.crue.options.config.CourbeConfig;
import org.openide.util.Lookup;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Permet de gérer la configuration des courbes effectuées dans les AOC et les sauvegarder en mémoire.
 * Le fichier est gérée par {@link org.fudaa.fudaa.crue.options.services.ConfigurationManagerService}
 *
 * @author Frederic Deniger
 */
public class AocLoiConfigController implements EGGrapheModelListener, PropertyChangeListener {
  private final AocLoiTopComponentInterface topComponent;
  private final AbstractLoiUiController loiUIController;
  private final AocConfigService aocConfigService = Lookup.getDefault().lookup(AocConfigService.class);
  private CourbeConfig current;
  private boolean updating;
  private boolean configModified;

  public void setUpdating(boolean updating) {
    this.updating = updating;
  }

  public boolean isUpdating() {
    return updating;
  }

  /**
   * @param topComponent le topComponent utilisant la configuration AOC (éditeur de loi)
   * @param loiUIController le controller de loi
   */
  public AocLoiConfigController(AocLoiTopComponentInterface topComponent, AbstractLoiUiController loiUIController) {
    this.topComponent = topComponent;
    this.loiUIController = loiUIController;
  }

  /**
   * @return true si la configuration a été modifiée dans le topcomponent
   */
  public boolean isConfigModified() {
    return configModified;
  }

  /**
   * @param configModified true si la config a été modifiée dans le topComponent
   */
  private void setConfigModified(boolean configModified) {
    this.configModified = configModified;
  }

  /**
   * Installe le service AocConfig: écoute les changements au niveau des paramétrages des courbes pour envoyer au service {@link AocConfigService} et écoute
   * les changements effectués par ce service ( si l'utilisateur décide de recharger les configuration par exemple).
   */
  public void install() {
    aocConfigService.loadIfNeeded();
    if (updating) {
      return;
    }
    removeListeners();
    loiUIController.getEGGrapheSimpleModel().addModelListener(this);
    aocConfigService.addPropertyChangeListener(topComponent.getDefaultTopComponentId(), this);
  }

  /**
   * Opération inverse de {@link #install()}: enlève les listener.s
   */
  public void uninstall() {
    if (updating) {
      return;
    }
    current = null;
    removeListeners();
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (aocConfigService.getAocLoiConfiguration() != null) {
      reloadCourbeConfig();
    }
  }

  @Override
  public void structureChanged() {
    //rien à faire
  }

  @Override
  public void courbeContentChanged(EGObject egObject, boolean mustRestore) {
    //rien a faire
  }

  /**
   * Envoie les evt de modifications
   *
   * @param egObject l'objet envoyant l'evt
   * @param visibil true si c'est une option de visibilité
   */
  @Override
  public void courbeAspectChanged(EGObject egObject, boolean visibil) {
    configIsModified();
  }

  @Override
  public void axeContentChanged(EGAxe egAxe) {
    //rien a faire
  }

  /**
   * Envoie les evt de modifications
   *
   * @param egAxe l'axe modifie
   */
  @Override
  public void axeAspectChanged(EGAxe egAxe) {
    configIsModified();
  }

  private void configIsModified() {
    if (!updating && topComponent.getAttachedPerspective().isInEditMode() && !topComponent.isUpdating()) {
      configModified = true;
      current = new CourbeConfigUpdater(loiUIController, false).create(current);
      topComponent.setConfigModified();
    }
  }

  public void reloadCourbeConfig() {
    if (updating) {
      return;
    }
    updating = true;
    current = null;
    applyConfig();
    updating = false;
    setConfigModified(false);
  }

  public boolean saveCourbeConfig() {
    boolean modified = configModified && current != null;
    if (modified) {
      updating = true;
      aocConfigService.setAocLoiConfigurationModified(topComponent.getDefaultTopComponentId(), current.createCopy());
      updating = false;
      setConfigModified(false);
    }
    return modified;
  }

  /**
   * Applique la configuration issue du service {@link AocConfigService}.
   */
  public void applyConfig() {
    boolean wasNotConfigured = false;
    if (current == null) {
      final CourbeConfig commonConfig = aocConfigService.getAocCourbeConfig(topComponent.getDefaultTopComponentId());
      if (commonConfig == null) {
        current = new CourbeConfig();
        wasNotConfigured = true;
      } else {
        current = commonConfig.createCopy();
      }
    }
    CourbeConfigUpdater updater = new CourbeConfigUpdater(loiUIController, false);
    updating = true;
    updater.apply(current);
    if (wasNotConfigured) {
      current = updater.create(current);
    }
    updating = false;
  }

  /**
   * enleve les listener
   */
  private void removeListeners() {
    loiUIController.getEGGrapheSimpleModel().removeModelListener(this);
    aocConfigService.removePropertyChangeListener(topComponent.getDefaultTopComponentId(), this);
  }
}
