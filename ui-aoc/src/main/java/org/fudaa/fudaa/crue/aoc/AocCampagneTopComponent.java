/*
 * OtfaCampagneTopComponent.java
 *
 * Created on 10 nov. 2011, 15:10:04
 */
package org.fudaa.fudaa.crue.aoc;

import com.jidesoft.swing.JideTabbedPane;
import com.jidesoft.swing.SimpleScrollPane;
import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.aoc.projet.AocParametreNumeriques;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageAlgorithme;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageType;
import org.fudaa.dodico.crue.metier.aoc.EnumAocTypeCalageCritere;
import org.fudaa.dodico.crue.metier.aoc.EnumAocTypeCalageDonnees;
import org.fudaa.fudaa.crue.aoc.action.*;
import org.fudaa.fudaa.crue.aoc.perspective.PerspectiveServiceAoc;
import org.fudaa.fudaa.crue.aoc.service.AocService;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.editor.DocumentListenerAdapter;
import org.fudaa.fudaa.crue.common.helper.PositiveIntegerValidator;
import org.fudaa.fudaa.crue.common.helper.ToStringInternationalizableCellRenderer;
import org.fudaa.fudaa.crue.common.helper.TopComponentHelper;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.ItemVariableView;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.windows.TopComponent;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ItemListener;
import java.util.*;
import java.util.List;

import static org.openide.util.NbBundle.getMessage;

/**
 * @author Deniger
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.aocService//AocCampagneTopComponent//EN",
    autostore = false)
@TopComponent.Description(preferredID = AocCampagneTopComponent.TOPCOMPONENT_ID,
    persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "aoc-editor", openAtStartup = false)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.aocService.AocCampagneTopComponent")
@TopComponent.OpenActionRegistration(displayName = "#CTL_AocCampagneTopComponentAction",
    preferredID = AocCampagneTopComponent.TOPCOMPONENT_ID)
@ActionReference(path = "Menu/Window/AOC", position = 1)
public class AocCampagneTopComponent extends TopComponent implements LookupListener {
  public static final String TOPCOMPONENT_ID = "AocCampagneTopComponent";
  private final transient Lookup.Result<AocCampagne> aocCampagneResult;
  private final JRadioButton rdTypePermanent = new JRadioButton(MessagesAoc.getMessage("TypeCalage.Permanent"));
  private final JRadioButton rdTypeTransitoireLimnigramme = new JRadioButton(MessagesAoc.getMessage("TypeCalage.TransitoireLimni"));
  private final JRadioButton rdTypeTransitoireHydrogramme = new JRadioButton(MessagesAoc.getMessage("TypeCalage.TransitoireHydro"));
  private final JRadioButton rdCalageSeul = new JRadioButton(MessagesAoc.getMessage("Calage.Seul"));
  private final JRadioButton rdCalageRepetabilite = new JRadioButton(MessagesAoc.getMessage("Calage.Repetabilite"));
  private final JRadioButton rdCalageAnalyse = new JRadioButton(MessagesAoc.getMessage("Calage.Analyse"));
  private final JRadioButton rdCalageValidation = new JRadioButton(MessagesAoc.getMessage("Calage.ValidationCroisee"));
  private final JComboBox cbCalageAlgorithme = new JComboBox();
  private final JComboBox cbCalageTransitoireCritere = new JComboBox();
  private final transient PerspectiveServiceAoc perspectiveServiceAoc = Lookup.getDefault().lookup(PerspectiveServiceAoc.class);
  private final NewCampaignAction newCampaignAction = new NewCampaignAction();
  private final OpenFileAction openFileAction = new OpenFileAction();
  private final CloseFileAction closeFileAction = new CloseFileAction();
  private final SaveFileAction saveFileAction = new SaveFileAction();
  private final LaunchCampagne launchCampagneAction = new LaunchCampagne();
  private final JTextField tfAocCampagneFile = new JTextField();
  private final JTextField tfEtudeFile = new JTextField();
  private final JCheckBox cbLog = new JCheckBox();
  private final JTextField tfScenario = new JTextField();
  private final JTextField tfCommentaire = new JTextField();
  private final JTextField tfAuteurCreation = new JTextField();
  private final JTextField tfDateCreation = new JTextField();
  private final JTextField tfAuteurModification = new JTextField();
  private final JTextField tfDateModification = new JTextField();
  private final BuTextField tfNbIterSeul = BuTextField.createIntegerField();
  private final BuTextField tfNbIterTir = BuTextField.createIntegerField();
  private final BuTextField tfNbTir = BuTextField.createIntegerField();
  private final BuTextField tfNbIterValCrois = BuTextField.createIntegerField();
  private final BuTextField tfPondApp = BuTextField.createDoubleField();
  private final BuTextField tfPondVal = BuTextField.createDoubleField();
  private final JLabel lbValidNbIterSeul = new JLabel();
  private final JLabel lbValidNbIterTir = new JLabel();
  private final JLabel lbValidNbTir = new JLabel();
  private final JLabel lbValidNbIterValCrois = new JLabel();
  private final JLabel lbValidPondApp = new JLabel();
  private final transient Lookup.Result<CtuluLogGroup> aocCampagneValidationResult;
  private final transient Lookup.Result<Boolean> aocCampagneModifiedResult;
  private final transient AocService aocService = Lookup.getDefault().lookup(AocService.class);
  private boolean updateAocFromUI;
  private boolean editableForTest;
  private boolean updatingFromModel;
  private JButton btShowValidation;
  private transient List<ItemVariableView> views;
  private JButton buttonInitParemetresNumeriques;

  /**
   * AocCampagneTopComponent
   */
  public AocCampagneTopComponent() {
    initComponents();

    TopComponentHelper.setMainViewProperties(this);

    this.setDisplayName(getMessage(AocCampagneTopComponent.class, "CTL_AocCampagneTopComponentAction"));
    this.setToolTipText(getMessage(AocCampagneTopComponent.class, "CTL_AocCampagneTopComponentAction"));

    buildButtonPanel();
    setBorder(BuBorders.EMPTY5555);

    JPanel mainParameters = new JPanel(new BuVerticalLayout(20));
    mainParameters.setBorder(BuBorders.EMPTY5555);
    configureComponents();
    buildMainInfoPanel(mainParameters);

    JideTabbedPane tabbedPane = new JideTabbedPane();
    mainParameters.add(tabbedPane);
    tabbedPane.add(getMessage(AocCampagneTopComponent.class, "AocCampagneTopComponent.MainParameters"), buildCalagesPanel());
    tabbedPane.add(getMessage(AocCampagneTopComponent.class, "AocCampagneTopComponent.NumericalParameters"), buildNumericalParameters());
    add(mainParameters, BorderLayout.CENTER);

    perspectiveServiceAoc.addStateListener(evt -> {
      updateAllEditableState();
      updateComponentStateWithBusinessRules();
    });
    updateAllEditableState();
    initValues();

    aocCampagneResult = aocService.getLookup().lookupResult(AocCampagne.class);
    aocCampagneValidationResult = aocService.getLookup().lookupResult(CtuluLogGroup.class);
    aocCampagneModifiedResult = aocService.getLookup().lookupResult(Boolean.class);
    aocCampagneResult.addLookupListener(this);
    aocCampagneModifiedResult.addLookupListener(this);
    aocCampagneValidationResult.addLookupListener(this);

    updateUIFromAoc();
    updateValidation();
    initEditionListeners();
  }

  private ItemVariableView getValue(String variableName) {
    return views.stream().filter(itemVariableView -> itemVariableView.getProperty().getNom().equals(variableName)).findFirst().get();
  }

  private JPanel buildNumericalParameters() {
    JPanel panel = new JPanel(new BuGridLayout(2, 5, 5, false, false, false, false));
    panel.setBorder(BuBorders.EMPTY5555);
    final CrueConfigMetier ccm = aocService.getCoeurConfigToUseForNumericalParameters().getCrueConfigMetier();
    try {

      views = ItemVariableView.create(DecimalFormatEpsilonEnum.COMPARISON, new AocParametreNumeriques(ccm), ccm,
          AocParametreNumeriques.PROP_AMPLITUDE,
          AocParametreNumeriques.PROP_TEMPERATURE_INITIALE,
          AocParametreNumeriques.PROP_TEMPERATURE_FINALE,
          AocParametreNumeriques.PROP_SEUIL_RMSEZ,
          AocParametreNumeriques.PROP_SEUIL_RMSEQ,
          AocParametreNumeriques.PROP_SEUIL_T,
          AocParametreNumeriques.PROP_SEUIL_ZMAX,
          AocParametreNumeriques.PROP_SEUIL_QMAX,
          AocParametreNumeriques.PROP_SEUIL_VOL
      );
    } catch (Throwable ex) {
      views = new ArrayList<>();
      panel.add(new JLabel(getMessage(AocCampagneTopComponent.class, "AocCampagneTopComponent.CrueConfigMetierNotUpdated")));
    }
    views.forEach(itemVariableView -> {
      panel.add(new JLabel(itemVariableView.getLabelName()));
      final JComponent panelComponent = itemVariableView.getPanelComponent();
      itemVariableView.setEditable(false);
      panel.add(panelComponent);
      if (AocParametreNumeriques.PROP_TEMPERATURE_FINALE.equals(itemVariableView.getProperty().getNom())) {
        panel.add(new JLabel());
        panel.add(new JLabel(getMessage(AocCampagneTopComponent.class, "AocCampagneTopComponent.TemperatureFinaleInfo")));
      }
    });

    buttonInitParemetresNumeriques = new JButton(getMessage(AocCampagneTopComponent.class, "AocCampagneTopComponent.ReinitValues"));
    panel.add(new JLabel());
    panel.add(buttonInitParemetresNumeriques);
    buttonInitParemetresNumeriques.addActionListener(e -> reinitValues());
    buttonInitParemetresNumeriques.setEnabled(false);

    return panel;
  }

  private void reinitValues() {
    final CrueConfigMetier ccm = aocService.getCcm();
    updateUIParametresNumeriques(new AocParametreNumeriques(ccm));
  }

  private List<JComponent> getEditableComponents() {
    List<JComponent> res = new ArrayList<>(Arrays.asList(tfCommentaire, cbLog, rdTypePermanent, rdTypeTransitoireHydrogramme, rdTypeTransitoireLimnigramme,
        rdCalageSeul, rdCalageRepetabilite, rdCalageAnalyse, rdCalageValidation, cbCalageTransitoireCritere, cbCalageAlgorithme,
        tfNbIterSeul, tfNbIterTir, tfNbTir, tfNbIterValCrois, tfPondApp, buttonInitParemetresNumeriques));
    views.forEach(itemVariableView -> res.add(itemVariableView.getPanelComponent()));
    return res;
  }

  private void configureComponents() {
    ButtonGroup groupTypeCalage = new ButtonGroup();
    groupTypeCalage.add(rdTypePermanent);
    groupTypeCalage.add(rdTypeTransitoireHydrogramme);
    groupTypeCalage.add(rdTypeTransitoireLimnigramme);

    ButtonGroup groupCalage = new ButtonGroup();
    groupCalage.add(rdCalageSeul);
    groupCalage.add(rdCalageRepetabilite);
    groupCalage.add(rdCalageAnalyse);
    groupCalage.add(rdCalageValidation);

    tfNbIterSeul.setColumns(10);
    tfNbTir.setColumns(10);
    tfNbIterTir.setColumns(10);

    tfNbIterSeul.setValueValidator(new PositiveIntegerValidator());
    tfNbIterTir.setValueValidator(new PositiveIntegerValidator());
    tfNbTir.setValueValidator(new PositiveIntegerValidator());
    tfNbIterValCrois.setValueValidator(new PositiveIntegerValidator());
    tfPondApp.setValueValidator(BuValueValidator.MINMAX(0, 1));

    tfDateCreation.setColumns(25);
    tfDateModification.setColumns(25);
    tfAuteurModification.setColumns(25);
    tfAuteurCreation.setColumns(25);

    //ces champs sont toujours en lecture seule
    tfAocCampagneFile.setEnabled(false);
    tfEtudeFile.setEnabled(false);
    cbLog.setEnabled(false);
    tfScenario.setEnabled(false);
    tfAuteurCreation.setEnabled(false);
    tfAuteurModification.setEnabled(false);
    tfDateModification.setEnabled(false);
    tfDateCreation.setEnabled(false);
    tfPondVal.setEnabled(false);

    cbCalageAlgorithme.setModel(new DefaultComboBoxModel<>(EnumAocCalageAlgorithme.values()));
    cbCalageAlgorithme.setRenderer(new ToStringInternationalizableCellRenderer());

    cbCalageTransitoireCritere.setModel(new DefaultComboBoxModel<>(EnumAocTypeCalageCritere.values()));
    cbCalageTransitoireCritere.setRenderer(new ToStringInternationalizableCellRenderer());
  }

  /**
   * Valide toutes les valeurs des champs textes.
   */
  private void validTextFieldsData() {

    lbValidPondApp.setIcon(null);
    lbValidPondApp.setToolTipText(null);

    if (aocService.getCurrentAOC() != null && tfPondApp.isEnabled() && tfPondApp.getValue() == null) {
      lbValidPondApp.setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.SEVERE));
      lbValidPondApp.setToolTipText(BusinessMessages.getString("aoc.validation.campagne.pondApp"));
    }
    if (tfPondApp.getValue() != null) {
      tfPondVal.setValue(1d - ((Double) tfPondApp.getValue()));
    }
    updateLabelTextField(BusinessMessages.getString("aoc.validation.campagne.nbIterSeul"), tfNbIterSeul, lbValidNbIterSeul);
    updateLabelTextField(BusinessMessages.getString("aoc.validation.campagne.nbIterTir"), tfNbIterTir, lbValidNbIterTir);
    updateLabelTextField(BusinessMessages.getString("aoc.validation.campagne.nbTir"), tfNbTir, lbValidNbTir);
    updateLabelTextField(BusinessMessages.getString("aoc.validation.campagne.nbIterationValidationCroisee"), tfNbIterValCrois, lbValidNbIterValCrois);
  }

  private void updateLabelTextField(String toolTipText, BuTextField textField, JLabel lbValidation) {
    lbValidation.setIcon(null);
    lbValidation.setToolTipText(null);
    if (aocService.getCurrentAOC() != null && textField.isEnabled() && textField.getValue() == null) {
      lbValidation.setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.SEVERE));
      lbValidation.setToolTipText(toolTipText);
    }
    textField.setToolTipText(toolTipText);
  }

  private List<Action> getAocAction() {
    return Arrays.asList(newCampaignAction, openFileAction, closeFileAction, saveFileAction, launchCampagneAction);
  }

  private void buildButtonPanel() {
    JPanel pnButton = new JPanel();
    pnButton.setLayout(new GridLayout(1, 5, 5, 5));
    pnButton.add(new JButton(newCampaignAction));
    pnButton.add(new JButton(openFileAction));
    pnButton.add(new JButton(saveFileAction));
    pnButton.add(new JButton(closeFileAction));
    pnButton.add(new JButton(launchCampagneAction));
    add(pnButton, BorderLayout.NORTH);
  }

  private JComponent buildCalagesPanel() {

    BuMultiFormLayout l = new BuMultiFormLayout
        (20, 5, 2, 2, new int[]{200, 0}, new int[]{0, 0});

    //Type calage:
    JPanel pnTypeCalagePermanent = new JPanel(l);
    pnTypeCalagePermanent.setBorder(createEtchedBorder());
    pnTypeCalagePermanent.add(rdTypePermanent, BuMultiFormLayout.constraint(0, 0, 1, 2, true, true, 0f, 0.5f));
    pnTypeCalagePermanent.add(new JLabel(MessagesAoc.getMessage("TypeCalage.Critere")), BuMultiFormLayout.constraint(1, 0, 1, 2, false, true, 1f, 0.5f));
    pnTypeCalagePermanent
        .add(new JLabel(MessagesAoc.getMessage("TypeCalage.ErreurQuadratique")), BuMultiFormLayout.constraint(2, 0, 1, 2, true, true, 0.2f, 0.5f));

    JPanel pnTypeCalageTransitoire = new JPanel(l);
    pnTypeCalageTransitoire.setBorder(createEtchedBorder());

    pnTypeCalageTransitoire.add(rdTypeTransitoireLimnigramme, BuMultiFormLayout.constraint(0, 0));
    pnTypeCalageTransitoire.add(rdTypeTransitoireHydrogramme, BuMultiFormLayout.constraint(0, 1));
    pnTypeCalageTransitoire.add(new JLabel(MessagesAoc.getMessage("TypeCalage.Critere")), BuMultiFormLayout.constraint(1, 0, 1, 2, false, true, 1f, 0.5f));
    pnTypeCalageTransitoire.add(cbCalageTransitoireCritere, BuMultiFormLayout.constraint(2, 0, 1, 2, true, false, 0.2f, 0.5f));

    JPanel pnTypeCalageTitle = new JPanel(new BuVerticalLayout(5));
    pnTypeCalageTitle.setBorder(createTitleBorder(MessagesAoc.getMessage("TypeCalage.Title")));

    pnTypeCalageTitle.add(pnTypeCalagePermanent);
    pnTypeCalageTitle.add(pnTypeCalageTransitoire);

    //calage:
    JPanel pnCalageAlgo = new JPanel(l);

    pnCalageAlgo.add(new JLabel(MessagesAoc.getMessage("Algorithme.Title")), BuMultiFormLayout.constraint(0, 0));
    pnCalageAlgo.add(cbCalageAlgorithme, BuMultiFormLayout.constraint(1, 0, 1, 1, true, false, 0, 0.5f));

    JPanel pnCalageSeul = new JPanel(l);
    pnCalageSeul.setBorder(createEtchedBorder());
    pnCalageSeul.add(rdCalageSeul, BuMultiFormLayout.constraint(0, 0, 1, 2));
    pnCalageSeul.add(new JLabel(MessagesAoc.getMessage("Calage.NombreIteration")), BuMultiFormLayout.constraint(1, 0, 1, 2, false, false, 1f, 0.5f));
    pnCalageSeul.add(tfNbIterSeul, BuMultiFormLayout.constraint(2, 0, 1, 2, true, false, 0.2f, 0.5f));
    pnCalageSeul.add(lbValidNbIterSeul, BuMultiFormLayout.constraint(3, 0, 1, 2, true, false, 0.5f, 0.5f));

    JPanel pnCalageRepetabilite = new JPanel(l);
    pnCalageRepetabilite.setBorder(createEtchedBorder());
    pnCalageRepetabilite.add(rdCalageRepetabilite, BuMultiFormLayout.constraint(0, 0, 1, 2));
    pnCalageRepetabilite
        .add(new JLabel(MessagesAoc.getMessage("Calage.NombreIterationTir")), BuMultiFormLayout.constraint(1, 0, 1, 1, false, false, 1f, 0.5f));
    pnCalageRepetabilite.add(tfNbIterTir, BuMultiFormLayout.constraint(2, 0, 1, 1, true, false, 0.2f, 0.5f));
    pnCalageRepetabilite.add(lbValidNbIterTir, BuMultiFormLayout.constraint(3, 0, 1, 1, true, false, 0.5f, 0.5f));
    pnCalageRepetabilite.add(new JLabel(MessagesAoc.getMessage("Calage.NombreTir")), BuMultiFormLayout.constraint(1, 1, 1, 1, false, false, 1f, 0.5f));
    pnCalageRepetabilite.add(tfNbTir, BuMultiFormLayout.constraint(2, 1, 1, 1, true, false, 0.2f, 0.5f));
    pnCalageRepetabilite.add(lbValidNbTir, BuMultiFormLayout.constraint(3, 1, 1, 1, true, false, 0.5f, 0.5f));

    JPanel pnCalageAnalyse = new JPanel(l);
    pnCalageAnalyse.setBorder(createEtchedBorder());
    pnCalageAnalyse.add(rdCalageAnalyse, BuMultiFormLayout.constraint(0, 0, 1, 2));

    JPanel pnCalageValidation = new JPanel(l);
    pnCalageValidation.setBorder(createEtchedBorder());
    pnCalageValidation.add(rdCalageValidation, BuMultiFormLayout.constraint(0, 0, 1, 3));
    pnCalageValidation.add(new JLabel(MessagesAoc.getMessage("Calage.NombreIteration")), BuMultiFormLayout.constraint(1, 0, 1, 1, false, false, 1f, 0.5f));
    pnCalageValidation
        .add(new JLabel(MessagesAoc.getMessage("Calage.PonderationApprentissage")), BuMultiFormLayout.constraint(1, 1, 1, 1, false, false, 1f, 0.5f));
    pnCalageValidation
        .add(new JLabel(MessagesAoc.getMessage("Calage.PonderationValidation")), BuMultiFormLayout.constraint(1, 2, 1, 1, false, false, 1f, 0.5f));

    pnCalageValidation.add(tfNbIterValCrois, BuMultiFormLayout.constraint(2, 0, 1, 1, true, false, 0.2f, 0.5f));
    pnCalageValidation.add(lbValidNbIterValCrois, BuMultiFormLayout.constraint(3, 0, 1, 1, true, false, 0.5f, 0.5f));
    pnCalageValidation.add(tfPondApp, BuMultiFormLayout.constraint(2, 1, 1, 1, true, false, 0.2f, 0.5f));
    pnCalageValidation.add(lbValidPondApp, BuMultiFormLayout.constraint(3, 1, 1, 1, true, false, 0.5f, 0.5f));
    pnCalageValidation.add(tfPondVal, BuMultiFormLayout.constraint(2, 2, 1, 1, true, false, 0.2f, 0.5f));

    JPanel pnCalageTitle = new JPanel(new BuVerticalLayout(5));
    pnCalageTitle.setBorder(createTitleBorder(MessagesAoc.getMessage("Calage.Title")));
    pnCalageTitle.add(pnCalageAlgo);
    pnCalageTitle.add(pnCalageSeul);
    pnCalageTitle.add(pnCalageRepetabilite);
    pnCalageTitle.add(pnCalageAnalyse);
    pnCalageTitle.add(pnCalageValidation);

    JPanel pnCalages = new JPanel(new BuVerticalLayout(10));
    pnCalages.add(pnTypeCalageTitle);
    pnCalages.add(pnCalageTitle);
    final JScrollPane scrollPane = new SimpleScrollPane();
    scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    scrollPane.setViewportBorder(BorderFactory.createEmptyBorder());
    scrollPane.setBorder(BorderFactory.createEmptyBorder());
    scrollPane.setViewportView(pnCalages);
    JPanel panel = new JPanel(new BorderLayout());
    panel.setBorder(BuBorders.EMPTY5555);
    panel.add(scrollPane);
    return panel;
  }

  private Border createEtchedBorder() {
    return BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(), BuBorders.EMPTY5555);
  }

  private Border createTitleBorder(String title) {
    return BorderFactory.createCompoundBorder(new BuTitledBorder(BorderFactory.createLineBorder(Color.GRAY), title), BuBorders.EMPTY5555);
  }

  private void buildMainInfoPanel(JPanel parentContainer) {

    JPanel pnMainInfos = new JPanel();
    pnMainInfos.setLayout(new BuGridLayout(2, 10, 10, true, true, true, true, true));
    pnMainInfos.add(new JLabel(MessagesAoc.getMessage("CampagneAOC.AocFile")));
    pnMainInfos.add(tfAocCampagneFile);
    pnMainInfos.add(new JLabel(MessagesAoc.getMessage("CampagneAOC.EtudeFile")));
    pnMainInfos.add(tfEtudeFile);
    pnMainInfos.add(new JLabel(MessagesAoc.getMessage("CampagneAOC.Scenario")));
    pnMainInfos.add(tfScenario);
    pnMainInfos.add(new JLabel(MessagesAoc.getMessage("CampagneAOC.Commentaire")));
    pnMainInfos.add(tfCommentaire);
    pnMainInfos.add(new JLabel(MessagesAoc.getMessage("CampagneAOC.Log")));
    pnMainInfos.add(cbLog);
    cbLog.setToolTipText(MessagesAoc.getMessage("CampagneAOC.Log.Tooltip"));
    JPanel creationModificationTitle = new JPanel(new BuVerticalLayout(10));
    creationModificationTitle.add(new JLabel(MessagesAoc.getMessage("CampagneAOC.AuteurCreation")));
    creationModificationTitle.add(new JLabel(MessagesAoc.getMessage("CampagneAOC.AuteurModification")));
    pnMainInfos.add(creationModificationTitle);
    JPanel pnAuthors = new JPanel(new BuVerticalLayout(10));
    pnAuthors.add(tfAuteurCreation);
    pnAuthors.add(tfAuteurModification);

    JPanel pnDates = new JPanel(new BuGridLayout(2, 10, 10, true, false, false, false));
    pnDates.add(new JLabel(MessagesAoc.getMessage("CampagneAOC.DateCreation")));
    pnDates.add(tfDateCreation);
    pnDates.add(new JLabel(MessagesAoc.getMessage("CampagneAOC.DateModification")));
    pnDates.add(tfDateModification);

    JPanel creationModificationData = new JPanel(new BuBorderLayout(30, 10));
    creationModificationData.add(pnAuthors, BorderLayout.WEST);
    pnDates.setBorder(BuBorders.EMPTY0505);
    creationModificationData.add(pnDates, BorderLayout.CENTER);
    pnMainInfos.add(creationModificationData);

    btShowValidation = new JButton();
    btShowValidation.addActionListener(e -> btShowValidation());
    pnMainInfos.add(new JLabel(MessagesAoc.getMessage("CampagneAOC.Validation")));
    pnMainInfos.add(btShowValidation);

    parentContainer.add(pnMainInfos);
  }

  private void btShowValidation() {
    LogsDisplayer.displayError(aocService.getCurrentValidationState(),
        MessagesAoc.getMessage("CampagneAOC.ShowErrorButtonLabel"));
  }

  @Override
  public Action[] getActions() {
    return TopComponentHelper.getMainViewActions();
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getTopComponentHelpCtxId("vueAOC", PerspectiveEnum.AOC));
  }

  public Map<Action, Boolean> calculLaunched() {
    updateAllComponentEnableState(false);
    final List<Action> aocAction = getAocAction();
    Map<Action, Boolean> res = new HashMap<>();
    for (Action action : aocAction) {
      res.put(action, action.isEnabled());
      action.setEnabled(false);
    }
    return res;
  }

  @Override
  public void resultChanged(LookupEvent ev) {
    if (ev == null || ev.getSource() == aocCampagneResult) {
      updateUIFromAoc();
      return;
    } else if (ev.getSource() == aocCampagneValidationResult) {
      updateValidation();
    }
    if (ev.getSource() == aocCampagneModifiedResult && !aocService.isModified() && !updatingFromModel && !updateAocFromUI) {
      updateUIFromAoc();
    }
  }

  @Override
  public void componentOpened() {
    super.componentOpened();
  }

  //utilisé par Netbeans RCP
  @SuppressWarnings("unused")
  void writeProperties(java.util.Properties p) {
    p.setProperty("version", "1.0");
  }

  //utilisé par Netbeans RCP
  @SuppressWarnings("unused")
  void readProperties(java.util.Properties p) {
    //nothing saved currently
  }

  private void initValues() {
    tfEtudeFile.setText(CtuluLibString.EMPTY_STRING);
    tfScenario.setText(CtuluLibString.EMPTY_STRING);
    tfAocCampagneFile.setText(CtuluLibString.EMPTY_STRING);
    tfAuteurModification.setText(CtuluLibString.EMPTY_STRING);
    tfAuteurCreation.setText(CtuluLibString.EMPTY_STRING);
    tfDateCreation.setText(CtuluLibString.EMPTY_STRING);
    tfDateModification.setText(CtuluLibString.EMPTY_STRING);

    tfCommentaire.setText(CtuluLibString.EMPTY_STRING);
    cbLog.setSelected(false);
    tfNbIterSeul.setText(CtuluLibString.EMPTY_STRING);
    tfNbTir.setText(CtuluLibString.EMPTY_STRING);
    tfNbIterTir.setText(CtuluLibString.EMPTY_STRING);
    tfNbIterValCrois.setText(CtuluLibString.EMPTY_STRING);
    tfPondApp.setText(CtuluLibString.EMPTY_STRING);
    tfPondVal.setText(CtuluLibString.EMPTY_STRING);

    cbCalageAlgorithme.setSelectedItem(EnumAocCalageAlgorithme.RECUIT);
    cbCalageTransitoireCritere.setSelectedItem(EnumAocTypeCalageCritere.ERREUR_QUADRATIQUE);

    rdTypePermanent.setSelected(true);
    rdCalageSeul.setSelected(true);

    views.forEach(itemVariableView -> ((JTextField) itemVariableView.getEditComponent()).setText(CtuluLibString.EMPTY_STRING));
  }

  private EnumAocTypeCalageDonnees getTypeCalageDonnesFromUI() {
    if (rdTypeTransitoireLimnigramme.isSelected()) {
      return EnumAocTypeCalageDonnees.TRANSITOIRE_LIMNIGRAMME;
    }
    if (rdTypeTransitoireHydrogramme.isSelected()) {
      return EnumAocTypeCalageDonnees.TRANSITOIRE_HYDROGRAMME;
    }
    return EnumAocTypeCalageDonnees.PERMANENT;
  }

  private void updateAocFromUI() {
    if (!updatingFromModel) {
      updateAocFromUI = true;
      try {
        final AocCampagne currentAOC = aocService.getCurrentAOC();
        if (currentAOC != null) {
          currentAOC.setCommentaire(tfCommentaire.getText());
          currentAOC.setLogAll(cbLog.isSelected());
          currentAOC.setTypeCalageDonnees(getTypeCalageDonnesFromUI());
          currentAOC.getCalage().setOperation(getOperationFromUI());
          currentAOC.setTypeCalageCritere((EnumAocTypeCalageCritere) cbCalageTransitoireCritere.getSelectedItem());
          currentAOC.getCalage().setAlgorithme((EnumAocCalageAlgorithme) cbCalageAlgorithme.getSelectedItem());
          if (tfNbIterSeul.getValue() != null) {
            currentAOC.getCalage().setNombreIterationSeul((Integer) tfNbIterSeul.getValue());
          }
          if (tfNbIterTir.getValue() != null) {
            currentAOC.getCalage().setNombreIterationTir(((Integer) tfNbIterTir.getValue()));
          }
          if (tfNbTir.getValue() != null) {
            currentAOC.getCalage().setNombreTir(((Integer) tfNbTir.getValue()));
          }
          if (tfNbIterValCrois.getValue() != null) {
            currentAOC.getCalage().setNombreIterationValidationCroisee((Integer) tfNbIterValCrois.getValue());
          }
          if (tfPondApp.getValue() != null) {
            currentAOC.getCalage().setPonderationApprentissage((Double) tfPondApp.getValue());
            currentAOC.getCalage().setPonderationValidation((Double) tfPondVal.getValue());
          }
          Object value = getValue(AocParametreNumeriques.PROP_AMPLITUDE).getValue();
          if (value != null) {
            currentAOC.getParametreNumeriques().setCoefAmplitude_Recuit((Double) value);
          }
          value = getValue(AocParametreNumeriques.PROP_TEMPERATURE_INITIALE).getValue();
          if (value != null) {
            currentAOC.getParametreNumeriques().setTemperatureInitiale_Recuit((Double) value);
          }
          value = getValue(AocParametreNumeriques.PROP_TEMPERATURE_FINALE).getValue();
          if (value != null) {
            currentAOC.getParametreNumeriques().setTemperatureFinale_Recuit((Double) value);
          }
          value = getValue(AocParametreNumeriques.PROP_SEUIL_RMSEZ).getValue();
          if (value != null) {
            currentAOC.getParametreNumeriques().setSeuilRMSEZ_CriterePermTransAOC((Double) value);
          }
          value = getValue(AocParametreNumeriques.PROP_SEUIL_RMSEQ).getValue();
          if (value != null) {
            currentAOC.getParametreNumeriques().setSeuilRMSEQ_CritereTransAOC((Double) value);
          }
          value = getValue(AocParametreNumeriques.PROP_SEUIL_T).getValue();
          if (value != null) {
            currentAOC.getParametreNumeriques().setSeuilTarriveeMax_CritereTransAOC((Double) value);
          }
          value = getValue(AocParametreNumeriques.PROP_SEUIL_QMAX).getValue();
          if (value != null) {
            currentAOC.getParametreNumeriques().setSeuilQmax_CritereTransAOC((Double) value);
          }
          value = getValue(AocParametreNumeriques.PROP_SEUIL_ZMAX).getValue();
          if (value != null) {
            currentAOC.getParametreNumeriques().setSeuilZmax_CritereTransAOC((Double) value);
          }
          value = getValue(AocParametreNumeriques.PROP_SEUIL_VOL).getValue();
          if (value != null) {
            currentAOC.getParametreNumeriques().setSeuilVol_CritereTransAOC((Double) value);
          }
          aocService.setModified();
        }
      } finally {
        updateAocFromUI = false;
      }
    }
  }

  private void updateUIFromAoc() {
    updatingFromModel = true;
    final AocCampagne currentAOC = aocService.getCurrentAOC();
    if (currentAOC != null) {
      cbCalageTransitoireCritere.setSelectedItem(currentAOC.getTypeCalageCritere());
      cbCalageAlgorithme.setSelectedItem(currentAOC.getCalage().getAlgorithme());
      tfNbIterSeul.setValue(currentAOC.getCalage().getNombreIterationSeul());
      tfNbIterTir.setValue(currentAOC.getCalage().getNombreIterationTir());
      tfNbTir.setValue(currentAOC.getCalage().getNombreTir());
      tfNbIterValCrois.setValue(currentAOC.getCalage().getNombreIterationValidationCroisee());
      tfPondApp.setValue(currentAOC.getCalage().getPonderationApprentissage());

      final EnumAocTypeCalageDonnees typeCalageDonnees = currentAOC.getTypeCalageDonnees();
      rdTypePermanent.setSelected(EnumAocTypeCalageDonnees.PERMANENT.equals(typeCalageDonnees));
      rdTypeTransitoireHydrogramme.setSelected(EnumAocTypeCalageDonnees.TRANSITOIRE_HYDROGRAMME.equals(typeCalageDonnees));
      rdTypeTransitoireLimnigramme.setSelected(EnumAocTypeCalageDonnees.TRANSITOIRE_LIMNIGRAMME.equals(typeCalageDonnees));

      final EnumAocCalageType operation = currentAOC.getCalage().getOperation();
      rdCalageSeul.setSelected(EnumAocCalageType.SEUL.equals(operation));
      rdCalageRepetabilite.setSelected(EnumAocCalageType.TEST_REPETABILITE.equals(operation));
      rdCalageAnalyse.setSelected(EnumAocCalageType.ANALYSE_SENSIBILITE.equals(operation));
      rdCalageValidation.setSelected(EnumAocCalageType.VALIDATION_CROISEE.equals(operation));

      tfAocCampagneFile.setText(currentAOC.getAocFile().getAbsolutePath());
      tfEtudeFile.setText(currentAOC.getEtudeChemin());
      tfCommentaire.setText(currentAOC.getCommentaire());
      cbLog.setSelected(currentAOC.isLogAll());
      tfScenario.setText(currentAOC.getNomScenario());
      tfAuteurCreation.setText(currentAOC.getAuteurCreation());
      tfAuteurModification.setText(currentAOC.getAuteurModification());
      if (currentAOC.getDateCreation() != null) {
        tfDateCreation.setText(currentAOC.getDateCreation().toString());
      }
      if (currentAOC.getDateModification() != null) {
        tfDateModification.setText(currentAOC.getDateModification().toString());
      }
      final AocParametreNumeriques parametreNumeriques = currentAOC.getParametreNumeriques();
      updateUIParametresNumeriques(parametreNumeriques);
    } else {
      initValues();
      validTextFieldsData();
    }

    updatingFromModel = false;
  }

  private void updateUIParametresNumeriques(AocParametreNumeriques parametreNumeriques) {
    getValue(AocParametreNumeriques.PROP_AMPLITUDE).setValue(parametreNumeriques.getCoefAmplitude_Recuit());
    getValue(AocParametreNumeriques.PROP_TEMPERATURE_INITIALE).setValue(parametreNumeriques.getTemperatureInitiale_Recuit());
    getValue(AocParametreNumeriques.PROP_TEMPERATURE_FINALE).setValue(parametreNumeriques.getTemperatureFinale_Recuit());
    getValue(AocParametreNumeriques.PROP_SEUIL_RMSEZ).setValue(parametreNumeriques.getSeuilRMSEZ_CriterePermTransAOC());
    getValue(AocParametreNumeriques.PROP_SEUIL_RMSEQ).setValue(parametreNumeriques.getSeuilRMSEQ_CritereTransAOC());
    getValue(AocParametreNumeriques.PROP_SEUIL_T).setValue(parametreNumeriques.getSeuilTarriveeMax_CritereTransAOC());
    getValue(AocParametreNumeriques.PROP_SEUIL_QMAX).setValue(parametreNumeriques.getSeuilQmax_CritereTransAOC());
    getValue(AocParametreNumeriques.PROP_SEUIL_ZMAX).setValue(parametreNumeriques.getSeuilZmax_CritereTransAOC());
    getValue(AocParametreNumeriques.PROP_SEUIL_VOL).setValue(parametreNumeriques.getSeuilVol_CritereTransAOC());
  }

  /**
   * Initialise les listeners sur les composants swing
   */
  private void initEditionListeners() {
    ItemListener listener = e -> {
      updateComponentStateWithBusinessRules();
      updateAocFromUI();
    };
    rdTypePermanent.addItemListener(listener);
    rdTypeTransitoireHydrogramme.addItemListener(listener);
    rdTypeTransitoireLimnigramme.addItemListener(listener);
    rdCalageSeul.addItemListener(listener);
    rdCalageRepetabilite.addItemListener(listener);
    rdCalageAnalyse.addItemListener(listener);
    rdCalageValidation.addItemListener(listener);

    DocumentListener documentListener = new DocumentListenerAdapter() {
      @Override
      protected void update(DocumentEvent e) {
        updateAocFromUI();
        validTextFieldsData();
      }
    };
    tfNbIterSeul.getDocument().addDocumentListener(documentListener);
    tfNbIterTir.getDocument().addDocumentListener(documentListener);
    tfNbTir.getDocument().addDocumentListener(documentListener);
    tfNbIterValCrois.getDocument().addDocumentListener(documentListener);
    tfPondApp.getDocument().addDocumentListener(documentListener);

    views.forEach(itemVariableView -> ((JTextField) itemVariableView.getEditComponent()).getDocument().addDocumentListener(documentListener));

    ItemListener listenerAocChangedFromUI = e -> updateAocFromUI();
    cbCalageAlgorithme.addItemListener(listenerAocChangedFromUI);
    tfCommentaire.getDocument().addDocumentListener(documentListener);
    cbCalageTransitoireCritere.addItemListener(listenerAocChangedFromUI);
    cbLog.addItemListener(listenerAocChangedFromUI);
  }

  public void updateComponentStateWithBusinessRules() {
    //pas d'edition en cours: on ne fait rien.
    if (!isInEditMode()) {
      return;
    }
    cbCalageTransitoireCritere.setEnabled(rdTypeTransitoireLimnigramme.isSelected() || rdTypeTransitoireHydrogramme.isSelected());
    cbCalageAlgorithme.setEnabled(rdCalageSeul.isSelected() || rdCalageRepetabilite.isSelected() || rdCalageValidation.isSelected());
    tfNbIterSeul.setEnabled(rdCalageSeul.isSelected());
    tfNbIterTir.setEnabled(rdCalageRepetabilite.isSelected());
    tfNbTir.setEnabled(rdCalageRepetabilite.isSelected());
    tfNbIterValCrois.setEnabled(rdCalageValidation.isSelected());
    tfPondApp.setEnabled(rdCalageValidation.isSelected());
    validTextFieldsData();
  }

  /**
   * met à jour l'état enable de tous les composants sans les règles métiers
   */
  public void updateAllEditableState() {
    final boolean modifiable = isInEditMode();
    updateAllComponentEnableState(modifiable);
  }

  private void updateAllComponentEnableState(boolean modifiable) {
    final List<JComponent> editableComponents = getEditableComponents();
    for (JComponent editableComponent : editableComponents) {
      editableComponent.setEnabled(modifiable);
    }
  }

  public void setEditableForTest(boolean editableForTest) {
    this.editableForTest = editableForTest;
  }

  private boolean isInEditMode() {
    return editableForTest || perspectiveServiceAoc.isInEditMode();
  }

  private void updateValidation() {
    CtuluLogGroup currentValidationState = aocService.getCurrentValidationState();
    btShowValidation.setForeground(UIManager.getColor("Button.foreground"));
    btShowValidation.setText(MessagesAoc.getMessage("State.NoError"));
    btShowValidation.setIcon(null);
    btShowValidation.setEnabled(currentValidationState != null && currentValidationState.containsSomething());
    if (btShowValidation.isEnabled() && currentValidationState != null) {
      if (currentValidationState.containsError() || currentValidationState.containsFatalError()) {
        btShowValidation.setForeground(Color.RED);
        btShowValidation.setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.SEVERE));
        btShowValidation.setText(MessagesAoc.getMessage("State.ContainsError"));
      } else if (currentValidationState.containsWarning()) {
        btShowValidation.setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.WARNING));
        btShowValidation.setForeground(Color.ORANGE);
        btShowValidation.setText(MessagesAoc.getMessage("State.ContainsWarning"));
      } else {
        btShowValidation.setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.INFO));
        btShowValidation.setText(MessagesAoc.getMessage("State.ContainsMessage"));
      }
    }
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout(0, 10));
  }// </editor-fold>//GEN-END:initComponents

  private EnumAocCalageType getOperationFromUI() {
    if (rdCalageRepetabilite.isSelected()) {
      return EnumAocCalageType.TEST_REPETABILITE;
    }
    if (rdCalageAnalyse.isSelected()) {
      return EnumAocCalageType.ANALYSE_SENSIBILITE;
    }
    if (rdCalageValidation.isSelected()) {
      return EnumAocCalageType.VALIDATION_CROISEE;
    }
    return EnumAocCalageType.SEUL;
  }
// Variables declaration - do not modify//GEN-BEGIN:variables
// End of variables declaration//GEN-END:variables
}
