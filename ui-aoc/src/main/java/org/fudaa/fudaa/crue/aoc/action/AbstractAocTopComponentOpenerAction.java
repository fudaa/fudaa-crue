package org.fudaa.fudaa.crue.aoc.action;

import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.fudaa.crue.aoc.service.AocService;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractPerspectiveAwareAction;
import org.fudaa.fudaa.crue.common.helper.TopComponentOpener;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Classe parente pour les actions ouvrant des topcomponents d'edition de la campagne AOC.
 *
 * @author f.deniger
 */
public abstract class AbstractAocTopComponentOpenerAction extends AbstractPerspectiveAwareAction {
  private final transient TopComponentOpener topComponentOpener;
  @SuppressWarnings("FieldCanBeLocal")
  private final transient Lookup.Result<AocCampagne> lookupResult;
  private final transient AocService aocService = Lookup.getDefault().lookup(AocService.class);

  protected AbstractAocTopComponentOpenerAction(String name, String mode, String topComponentId) {
    super(PerspectiveEnum.AOC, false);
    topComponentOpener = new TopComponentOpener(mode, topComponentId);
    lookupResult = aocService.getLookup().lookupResult(AocCampagne.class);
    lookupResult.addLookupListener(this);
    putValue(Action.NAME, NbBundle.getMessage(AbstractAocTopComponentOpenerAction.class, name));
    updateEnableState();
  }

  @Override
  protected boolean getEnableState() {
    return super.getEnableState() && aocService.getCurrentAOC() != null;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    topComponentOpener.open();
  }
}
