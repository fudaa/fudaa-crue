package org.fudaa.fudaa.crue.aoc.nodes;

import com.memoire.fu.FuEmptyArrays;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.dodico.crue.aoc.validation.AocProfilSectionValidator;
import org.fudaa.dodico.crue.metier.aoc.ParametrageProfilSection;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.aoc.MessagesAoc;
import org.fudaa.fudaa.crue.aoc.ParametrageEchelleSectionTopComponent;
import org.fudaa.fudaa.crue.aoc.importer.AocProfilSectionFileImporter;
import org.fudaa.fudaa.crue.aoc.importer.AocProfilSectionNodesImporter;
import org.fudaa.fudaa.crue.aoc.service.LhptService;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.view.DefaultEditableOutlineViewEditor;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Vue gérant les {@link ParametrageProfilSection}
 */
public class ParametrageProfilSectionView extends DefaultEditableOutlineViewEditor<ParametrageEchelleSectionTopComponent, ParametrageProfilSection, ParametrageProfilSectionNode> {
  private transient ManagerEMHScenario managerEMHScenario;

  public ParametrageProfilSectionView(ParametrageEchelleSectionTopComponent parent) {
    super(parent);
  }

  @Override
  public void setEditable(boolean editable) {
    super.setEditable(editable);
    btAdd.setEnabled(editable && managerEMHScenario != null);
    btPaste.setEnabled(btAdd.isEnabled());
    btImport.setEnabled(btPaste.isEnabled());
  }

  /**
   * Remplace tout le contenu du tableau par le presse-papier
   */
  protected void replaceAllByClipboardData() {
    if (!this.editable) {
      return;
    }
    final String[] availableSections = getAvailableSections();
    List<ParametrageProfilSectionNode> pastData = AocProfilSectionNodesImporter.getPastData(getTopComponentParent().getAttachedPerspective(), availableSections);
    if (!pastData.isEmpty()) {
      update(pastData);
    }
    DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
    getTopComponentParent().setModified(true);
  }

  /**
   * Remplace tout le contenu du tableau par le contenu d'un fichier
   */
  protected void importData() {
    if (!this.editable) {
      return;
    }

    CtuluFileChooser fileChooser = AocProfilSectionFileImporter.getImportXLSCSVFileChooser();

    final int res = fileChooser.showDialog(CtuluUIForNetbeans.DEFAULT.getParentComponent(), NbBundle.getMessage(
        ParametrageProfilSectionView.class, "Import.DisplayName"));
    if (res == JFileChooser.APPROVE_OPTION) {
      final List<ParametrageProfilSectionNode> result = CrueProgressUtils.showProgressDialogAndRun(
          new AocProfilSectionFileImporter(fileChooser.getSelectedFile(), getTopComponentParent().getAttachedPerspective(), getAvailableSections()),
          NbBundle.getMessage(
              ParametrageProfilSectionView.class, "Import.DisplayName"));
      if (CollectionUtils.isEmpty(result)) {
        DialogHelper.showWarn(NbBundle.getMessage(getClass(), "Import.NoDataFound"));
      } else {
        update(result);
        // on force la modif de la fenêtre pour pouvoir enregistrer
        getTopComponentParent().setModified(true);
      }
    }
  }

  /**
   * @return les sections disponibles pour le scénario
   */
  private String[] getAvailableSections() {
    if (getTopComponentParent().getSelectedScenario() == null) {
      return FuEmptyArrays.STRING0;
    }
    final LhptService.LoiMesurePostState loadedLoiMesure = getTopComponentParent().getService()
        .getLoadedLoiMesure(getTopComponentParent().getSelectedScenario());
    return loadedLoiMesure == null ? FuEmptyArrays.STRING0 : loadedLoiMesure.getSectionsSortedByName();
  }

  @Override
  protected void addElement() {
    if (getTopComponentParent().getSelectedScenario() == null) {
      return;
    }
    Children children = getExplorerManager().getRootContext().getChildren();
    //les sections deja utilisées dans l'editeur
    Set<String> usedSections = getUsedEchellesSection();

    //les sections disponibles
    final String[] availableSections = getAvailableSections();
    if (usedSections.size() == availableSections.length) {
      DialogHelper.showWarn(MessagesAoc.getMessage("CantAddEchellesSectionsAllUsed.Message"));
      return;
    }
    ParametrageProfilSection aocEchellesSection = new ParametrageProfilSection();
    aocEchellesSection.setSectionRef(getFirstUsable(availableSections, usedSections));
    aocEchellesSection.setPk(CtuluLibString.EMPTY_STRING);
    final ParametrageProfilSectionNode aocProfilSectionNode = new ParametrageProfilSectionNode(getTopComponentParent().getAttachedPerspective(), aocEchellesSection,
        availableSections);
    children.add(new Node[]{aocProfilSectionNode});
    DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
  }

  /**
   * @return liste des sections déjà définies dans le composant
   */
  private Set<String> getUsedEchellesSection() {
    Set<String> usedSections = new HashSet<>();
    for (Node otherNode : getExplorerManager().getRootContext().getChildren().getNodes()) {
      usedSections.add(((ParametrageProfilSectionNode) otherNode).getEchellesSection().getSectionRef());
    }
    return usedSections;
  }

  @Override
  protected void createOutlineView() {
    view = new OutlineView(FIRST_COLUMN_NAME_SELECTION);
    view.getOutline().setColumnHidingAllowed(false);
    view.getOutline().setRootVisible(false);
    view.setTreeSortable(false);
    view.setPropertyColumns(
        AocNodeHelper.PROP_PROFIL,
        AocNodeHelper.getEchelleDisplay(),
        AocNodeHelper.PROP_SECTION,
        AocNodeHelper.getSectionDisplay());
  }

  public void setScenarioSelected(ManagerEMHScenario emhScenario) {
    this.managerEMHScenario = emhScenario;
    setEditable(super.editable);
  }

  /**
   * Copie le contenu du presse-papier dans le tableau-> remplace le contenu des cellules sélectionnées et ajoute si nécessaire des entrées.
   */
  public void pasteInSelectedNodes() {
    Node[] selectedNodes = getExplorerManager().getSelectedNodes();
    final List<ParametrageProfilSectionNode> pastData = AocProfilSectionNodesImporter
        .getPastData(getTopComponentParent().getAttachedPerspective(), getAvailableSections());
    if (selectedNodes.length > 0) {
      final int nbSelectedNodeToModify = Math.min(pastData.size(), selectedNodes.length);
      for (int i = 0; i < nbSelectedNodeToModify; i++) {
        ((ParametrageProfilSectionNode) selectedNodes[i]).replaceValueBy(pastData.get(i));
      }
    }
    //on ajoute les autres noeuds
    List<ParametrageProfilSectionNode> newNodeToAdd = new ArrayList<>();
    for (int i = selectedNodes.length; i < pastData.size(); i++) {
      newNodeToAdd.add(pastData.get(i));
    }
    if (!newNodeToAdd.isEmpty()) {
      Children children = getExplorerManager().getRootContext().getChildren();
      children.add(newNodeToAdd.toArray(new Node[0]));
      DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
    }
    validateData();
  }

  public boolean validateData() {
    final CtuluLog log = getValidationResult();
    return !log.isNotEmpty();
  }

  public CtuluLog getValidationResult() {
    AocProfilSectionValidator validator = new AocProfilSectionValidator();
    return validator.validate(getAllNodesForValidation(), getAvailableSections());
  }
}
