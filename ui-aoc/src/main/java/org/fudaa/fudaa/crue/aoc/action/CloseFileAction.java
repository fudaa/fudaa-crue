package org.fudaa.fudaa.crue.aoc.action;

import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * @author Chris
 */
public class CloseFileAction extends AbstractAocAction {
    public CloseFileAction() {
        super("CloseFileAction.ActionName");
        putValue(Action.NAME, NbBundle.getMessage(CloseFileAction.class, "CloseFileAction.ActionName"));
        putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(CloseFileAction.class, "CloseFileAction.ShortDescription"));
    }

    @Override
    protected void updateEnabledState() {
        setEnabled(aocService.getCurrentAOC() != null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        aocService.closeCurrent();
    }
}
