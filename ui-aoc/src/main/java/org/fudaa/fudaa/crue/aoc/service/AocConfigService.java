/*
 GPL 2
 */
package org.fudaa.fudaa.crue.aoc.service;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.fudaa.crue.aoc.MessagesAoc;
import org.fudaa.fudaa.crue.common.config.ConfigDefaultValuesProvider;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.services.EMHProjetService;
import org.fudaa.fudaa.crue.options.config.ConfigAocController;
import org.fudaa.fudaa.crue.options.config.CourbeConfig;
import org.fudaa.fudaa.crue.options.config.OptionLoiConfiguration;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.lookup.ServiceProvider;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Service permettant de gérer la confguration AOC au niveau étude. Ecoute les chargements de l'étude pour charger/décharger le paramétrage AOC ( configuration
 * des courbes,...).
 *
 * @author Frederic Deniger
 */
@ServiceProvider(service = AocConfigService.class)
public final class AocConfigService implements LookupListener, ConfigDefaultValuesProvider {
  @SuppressWarnings("FieldCanBeLocal")
  private final Lookup.Result<EMHProjet> resultat;
  /**
   * le service EMHProjetService
   */
  private final EMHProjetService service = Lookup.getDefault().lookup(EMHProjetService.class);
  private final ConfigAocController configAocController = new ConfigAocController();
  private final PropertyChangeSupport propertyChangeSupport;
  private boolean modified = false;

  public AocConfigService() {
    resultat = service.getLookup().lookupResult(EMHProjet.class);
    resultat.addLookupListener(this);
    resultChanged(null);
    propertyChangeSupport = new PropertyChangeSupport(this);
  }

  public void removePropertyChangeListener(String topComponentId, PropertyChangeListener listener) {
    propertyChangeSupport.removePropertyChangeListener(topComponentId, listener);
  }

  public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
    propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
  }

  /**
   * @return true si projet chargés
   */
  private boolean isProjectLoaded() {
    return service.getSelectedProject() != null;
  }

  @Override
  public void resultChanged(LookupEvent ev) {
    //event interne-> on ignore
    if (service.isReloading()) {
      return;
    }
    //project loaded
    if (!isProjectLoaded()) {
      unloadStudy();
    }
  }

  public OptionLoiConfiguration getAocLoiConfiguration() {
    return configAocController.getAocLoiConfiguration();
  }

  public CourbeConfig getAocCourbeConfig(String topComponentId) {
    OptionLoiConfiguration modellingLoiConfiguration = configAocController.getAocLoiConfiguration();
    if (modellingLoiConfiguration != null) {
      return modellingLoiConfiguration.getCourbeConfig(topComponentId);
    }
    return new CourbeConfig();
  }

  public void setAocLoiConfigurationModified(String topComponentId, CourbeConfig config) {
    modified = true;
    OptionLoiConfiguration modellingLoiConfiguration = configAocController.getAocLoiConfiguration();
    if (modellingLoiConfiguration == null) {
      modellingLoiConfiguration = new OptionLoiConfiguration();
      configAocController.setAocLoiConfiguration(modellingLoiConfiguration);
    }
    modellingLoiConfiguration.setCourbeConfig(topComponentId, config);
    propertyChangeSupport.firePropertyChange(topComponentId, Boolean.FALSE, Boolean.TRUE);
  }

  /**
   * @return true si la configuration a été modifiée
   */
  public boolean isModified() {
    return modified;
  }

  private void unloadStudy() {
    configAocController.unload();
  }

  @Override
  public CreationDefaultValue getDefaultValues() {
    return null;
  }

  public void loadIfNeeded() {
    if (isProjectLoaded() && configAocController.getAocLoiConfiguration() == null) {
      configAocController.loadConfigEtude(service.getSelectedProject());
    }
  }

  /**
   * @return le projet ouvert dans Fudaa-Crue
   */
  public EMHProjet getSelectedProject() {
    return service.getSelectedProject();
  }

  /**
   * @return le résultat de la sauvegarde de la configuration AOC.
   */
  public CtuluLog save() {
    return CrueProgressUtils.showProgressDialogAndRun(new AocSaveStudyConfigCallable(this), MessagesAoc.getMessage("Save.Configuration"));
  }
}
