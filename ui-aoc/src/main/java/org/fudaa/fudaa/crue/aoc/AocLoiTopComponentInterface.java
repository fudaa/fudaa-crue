package org.fudaa.fudaa.crue.aoc;

import org.fudaa.fudaa.crue.common.services.AbstractPerspectiveService;

/**
 *
 */
public interface AocLoiTopComponentInterface {
    /**
     * @return la perspective liée
     */
    AbstractPerspectiveService getAttachedPerspective();

    String getDefaultTopComponentId();

    boolean isUpdating();

    void setConfigModified();
}
