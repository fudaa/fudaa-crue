package org.fudaa.fudaa.crue.aoc.nodes;

import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTableModelInterface;
import org.fudaa.dodico.crue.aoc.projet.AocParamCalc;
import org.fudaa.dodico.crue.common.BusinessMessages;

import java.util.Collections;
import java.util.List;

/**
 * @author deniger
 */
public class AocListeCalculsExportTableModel implements CtuluTableModelInterface {
    private final List<AocParamCalcNode> nodes;

    public AocListeCalculsExportTableModel(List<AocParamCalcNode> nodes) {
        this.nodes = nodes;
    }

    @Override
    public List<String> getComments() {
        return Collections.emptyList();
    }

    @Override
    public int getMaxCol() {
        return 5;
    }

    @Override
    public int getMaxRow() {
        return nodes.size();
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return AocNodeHelper.getCalculDisplay();
            case 1:
                return BusinessMessages.getString("EMH.shortName");
            case 2:
                return BusinessMessages.getString("TypeClimM.shortName");
            case 3:
                return AocNodeHelper.getValueDisplay();
            case 4:
                return AocNodeHelper.getDeltaQDisplay();
            default:
                return CtuluLibString.EMPTY_STRING;
        }
    }

    @Override
    public Object getValue(int rowIndex, int columnIndex) {
        if (rowIndex < 0 || rowIndex >= getMaxRow()) {
            return CtuluLibString.EMPTY_STRING;
        }
        AocParamCalcNode node = nodes.get(rowIndex);
        final AocParamCalc mainValue = node.getMainValue();
        switch (columnIndex) {
            case 0:
                return mainValue.getCalculRef();
            case 1:
                return mainValue.getEmhRef();
            case 2:
                return node.getDclmType();
            case 3:
                return node.getDonCLimMValue();
            case 4:
                return mainValue.getDeltaQ();
            default:
                return CtuluLibString.EMPTY_STRING;
        }
    }

    @Override
    public WritableCell getExcelWritable(int rowIndex, int columnIndex, int rowInXls, int colInXls) {
        final Object o = getValue(rowIndex, columnIndex);
        if (o == null) {
            return null;
        }
        String s = o.toString();
        if (s == null) {
            return null;
        }
        s = s.trim();
        if (s.length() == 0) {
            return null;
        }
        try {
            return new Number(colInXls, rowInXls, Double.parseDouble(s));
        } catch (final NumberFormatException e) {
            //rien a faire
        }
        return new Label(colInXls, rowInXls, s);
    }

    @Override
    public int[] getSelectedRows() {
        return new int[0];
    }
}
