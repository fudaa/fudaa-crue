package org.fudaa.fudaa.crue.aoc;

import com.memoire.dnd.DndConstants;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.metier.aoc.AocEchellesSections;
import org.fudaa.dodico.crue.metier.aoc.ParametrageProfilSection;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.aoc.nodes.ParametrageProfilSectionNode;
import org.fudaa.fudaa.crue.aoc.nodes.ParametrageProfilSectionView;
import org.fudaa.fudaa.crue.aoc.service.LhptService;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.AbstractPerspectiveService;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultTopComponentParent;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ParametrageEchelleSectionTopComponent extends AbstractLhptTopComponent implements ExplorerManager.Provider, DefaultTopComponentParent {
  private final ParametrageProfilSectionView outlineViewEditor;

  public ParametrageEchelleSectionTopComponent(AbstractPerspectiveService attachedPerspective) {
    super(attachedPerspective);
    initComponents();
    setName(MessagesAoc.getMessage("CTL_ParametrageEchelleSectionTopComponent"));
    setToolTipText(MessagesAoc.getMessage("HINT_ParametrageEchelleSectionTopComponent"));
    outlineViewEditor = new ParametrageProfilSectionView(this);
    ActionMap map = getActionMap();
    associateLookup(ExplorerUtils.createLookup(getExplorerManager(), map));
    add(createCbScenarioPanel(), BorderLayout.NORTH);
    add(outlineViewEditor, BorderLayout.CENTER);
    add(createCancelSaveButtons(), BorderLayout.SOUTH);
    outlineViewEditor.getView().setAllowedDragActions(DndConstants.NONE);
    setEditable(false);
  }

  public ExplorerManager getExplorerManager() {
    return outlineViewEditor.getExplorerManager();
  }

  @Override
  protected void currentDataChanged(ManagerEMHScenario emhScenario, LhptService.LoiMesurePostState loiMesuresPost) {
    outlineViewEditor.setScenarioSelected(emhScenario);
    List<ParametrageProfilSectionNode> nodes = new ArrayList<>();
    if (emhScenario != null && loiMesuresPost != null) {
      for (ParametrageProfilSection aocEchellesSection : loiMesuresPost.getLoiMesuresPost().getEchellesSections().getEchellesSectionList()) {
        ParametrageProfilSectionNode node = new ParametrageProfilSectionNode(attachedPerspective, aocEchellesSection.clone(), loiMesuresPost.getSectionsSortedByName());
        nodes.add(node);
      }
    }
    outlineViewEditor.update(nodes);
  }

  @Override
  protected void componentActivated() {
    super.componentActivated();
    ExplorerUtils.activateActions(getExplorerManager(), true);
  }

  @Override
  protected void componentDeactivated() {
    super.componentDeactivated();
    ExplorerUtils.activateActions(getExplorerManager(), false);
  }

  @Override
  protected void setEditable(boolean b) {
    super.setEditable(b);
    outlineViewEditor.setEditable(b);
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueParametrageProfilSectionTopComponent", PerspectiveEnum.REPORT);
  }

  @Override
  protected void componentOpenedHandler() {
    super.componentOpenedHandler();
    readPreferences();
  }

  @Override
  public boolean valideModification() {
    if (!isModified()) {
      return true;
    }
    CtuluLog validatingResult = outlineViewEditor.getValidationResult();
    if (validatingResult.isNotEmpty()) {
      LogsDisplayer.displayError(validatingResult, getName());
    }
    if (validatingResult.containsErrorOrSevereError()) {
      return false;
    }
    final List<ParametrageProfilSectionNode> echellesSectionsFromNodes = outlineViewEditor.getAllNodes();
    AocEchellesSections aocEchellesSections = new AocEchellesSections();
    for (ParametrageProfilSectionNode echellesSectionsFromNode : echellesSectionsFromNodes) {
      aocEchellesSections.getEchellesSectionList().add(echellesSectionsFromNode.getEchellesSection());
    }

    service.saveLoiMesure(getSelectedScenario(), aocEchellesSections);
    setModified(false);
    getAttachedPerspective().setDirty(true);
    return true;
  }

  @Override
  public void cancelModification() {
    super.selectedScenarioChanged(getSelectedScenario());
    setModified(false);
  }

  @Override
  protected void componentClosedDefinitlyHandler() {
    super.componentClosedDefinitlyHandler();
    writePreferences();
  }

  private void readPreferences() {
    DialogHelper.readInPreferences(outlineViewEditor.getView(), "outlineView", getClass());
  }

  private void writePreferences() {
    DialogHelper.writeInPreferences(outlineViewEditor.getView(), "outlineView", getClass());
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  @SuppressWarnings("unused")
  void writeProperties(java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
  }

  @SuppressWarnings("unused")
  void readProperties(java.util.Properties p) {
  }
}
