/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.aoc.action;

import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.fudaa.crue.aoc.perspective.PerspectiveServiceAoc;
import org.fudaa.fudaa.crue.aoc.service.AocService;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;

import javax.swing.*;

/**
 * Classe abstraite pour les actions Aoc prenant en compte la perspective
 *
 * @author deniger
 */
public abstract class AbstractAocAction extends AbstractAction implements LookupListener {
    protected final transient AocService aocService = Lookup.getDefault().lookup(AocService.class);
    protected final transient PerspectiveServiceAoc perspectiveServiceAoc = Lookup.getDefault().lookup(PerspectiveServiceAoc.class);
    @SuppressWarnings("FieldCanBeLocal")
    private final transient Lookup.Result<AocCampagne> lookupResult;

    protected AbstractAocAction(String name) {
        putValue(Action.NAME, NbBundle.getMessage(AbstractAocAction.class, name));
        lookupResult = aocService.getLookup().lookupResult(AocCampagne.class);
        lookupResult.addLookupListener(this);
        perspectiveServiceAoc.addStateListener(evt -> updateEnabledState());
        updateEnabledState();
    }

    @Override
    public void resultChanged(LookupEvent ev) {
        updateEnabledState();
    }

    protected void updateEnabledState() {
        setEnabled(aocService.getCurrentAOC() != null && perspectiveServiceAoc.isInEditMode());
    }
}
