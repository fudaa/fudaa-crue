package org.fudaa.fudaa.crue.aoc.action;

import org.fudaa.fudaa.crue.aoc.AocValidationCroiseeTopComponent;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;

import javax.swing.*;

/**
 * Ouverture de {@link AocValidationCroiseeTopComponent}
 */
@ActionID(category = "View",
        id = "org.fudaa.fudaa.crue.aocService.action.AocValidationCroiseeAction")
@ActionRegistration(displayName = "#CTL_AocValidationCroiseeAction")
@ActionReference(path = "Actions/AOC", position = 7)
public final class AocValidationCroiseeAction extends AbstractAocTopComponentOpenerAction {
    public AocValidationCroiseeAction() {
        super("CTL_AocValidationCroiseeAction", AocValidationCroiseeTopComponent.MODE, AocValidationCroiseeTopComponent.TOPCOMPONENT_ID);
    }

    @Override
    public Action createContextAwareInstance(Lookup actionContext) {
        return new AocValidationCroiseeAction();
    }
}
