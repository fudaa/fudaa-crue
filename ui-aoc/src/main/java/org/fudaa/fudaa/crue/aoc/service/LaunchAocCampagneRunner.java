package org.fudaa.fudaa.crue.aoc.service;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.crue.aoc.batch.AocBatchLineSaver;
import org.fudaa.dodico.crue.aoc.exec.AocExecContainer;
import org.fudaa.dodico.crue.aoc.exec.AocExecProcessor;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.fudaa.crue.aoc.AocCampagneTopComponent;
import org.fudaa.fudaa.crue.aoc.MessagesAoc;
import org.fudaa.fudaa.crue.aoc.perspective.PerspectiveServiceAoc;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.util.Cancellable;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author Fred Deniger
 */
public class LaunchAocCampagneRunner implements Cancellable, Runnable {
  private Thread thread;
  private final PerspectiveServiceAoc perspectiveServiceAoc = Lookup.getDefault().lookup(PerspectiveServiceAoc.class);
  private final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  private ProgressHandle ph;
  private Map<Action, Boolean> calculLaunched;
  private final AocExecProcessor executor;
  private final File aocFile;

  public LaunchAocCampagneRunner(AocExecProcessor executor, File aocFile) {
    this.executor = executor;
    this.aocFile = aocFile;
  }

  @Override
  public boolean cancel() {
    if (thread != null) {
      thread.interrupt();
      executor.stop();
    }
    perspectiveServiceAoc.setDirty(false);
    ph.finish();
    return true;
  }

  public void go() {
    ph = ProgressHandleFactory.createHandle(MessagesAoc.getMessage("computingCampaign"), this,null);
    ph.start();
    ph.switchToIndeterminate();
    perspectiveServiceAoc.setDirty(true);
    AocCampagneTopComponent findTopComponent = (AocCampagneTopComponent) WindowManager.getDefault().findTopComponent(
        AocCampagneTopComponent.TOPCOMPONENT_ID);
    calculLaunched = findTopComponent.calculLaunched();
    thread = new Thread(this);
    thread.start();
  }

  @Override
  public void run() {
    final AocBatchLineSaver saver = new AocBatchLineSaver();
    CtuluLog initLog = saver.start(false);
    try {
      ph.switchToDeterminate(100);//100%

      saver.setAocFile(aocFile);
      final CrueOperationResult<AocExecContainer> launch = executor.launch(this.aocFile, new ProgressAdapter());

      final CtuluLogGroup cleanGroup = launch.getLogs().createCleanGroup();
      saver.getGlobalValidation().addGroup(cleanGroup);
      saver.keepOnlyError();
      EventQueue.invokeLater(() -> {
        DialogHelper.showNotifyOperationTermine(MessagesAoc.getMessage("computingCampaignFinished"));
        if (calculLaunched != null) {
          for (Entry<Action, Boolean> entry : calculLaunched.entrySet()) {
            entry.getKey().setEnabled(entry.getValue());
          }
        }
        ph.finish();
        projetService.reload("AOC");
        perspectiveServiceAoc.setDirty(false);
        AocCampagneTopComponent findTopComponent = (AocCampagneTopComponent) WindowManager.getDefault().findTopComponent(
            AocCampagneTopComponent.TOPCOMPONENT_ID);
        if (findTopComponent != null) {
          findTopComponent.updateAllEditableState();
        }

        if (saver.getGlobalValidation().containsSomething()) {
          LogsDisplayer.displayError(saver.getGlobalValidation(), MessagesAoc.getMessage("computingCampaign"));
        }
      });
    } catch (Exception e) {
      Exceptions.printStackTrace(e);
      initLog.addSevereError(e.getMessage());
      EventQueue.invokeLater(() -> {
        projetService.reload("AOC");
        perspectiveServiceAoc.setDirty(false);
        ph.finish();
      });
    } finally {
      saver.close();
    }
  }

  private class ProgressAdapter implements ProgressionInterface {
    @Override
    public void setProgression(int value) {
      ph.progress(Math.min(100, value));
    }

    @Override
    public void setDesc(String description) {
      ph.progress(description);
    }

    @Override
    public void reset() {
      //rien a faire
    }
  }
}
