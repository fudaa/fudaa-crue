package org.fudaa.fudaa.crue.aoc.service;

import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

/**
 * A utiliser por les topcomponent qui affichent des données lhpt comme les vues longitudinales et temporell,
 * @author deniger
 */
public interface LhptValuesDisplayer {
  /**
   * @param scenario le scenario mise à jour
   */
  void lhptUpdated(ManagerEMHScenario scenario);
}
