package org.fudaa.fudaa.crue.aoc.courbe;

import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGHorizontalBanner;
import org.fudaa.ebli.courbe.EGRepere;
import org.fudaa.ebli.trace.TraceBox;
import org.fudaa.fudaa.crue.loi.loiff.LoiUiController;

import javax.swing.*;
import java.awt.*;

/**
 * POur affichier les graduations sous forme de noms des strickler
 *
 * @author deniger
 */
public class AocLoiSectionZBanner implements EGHorizontalBanner {
    private final LoiUiController loiUiController;
    private final TraceBox traceBox;

    public AocLoiSectionZBanner(LoiUiController loiUiController) {
        this.loiUiController = loiUiController;
        loiUiController.getAxeX().setSpecificFormat(null);
        loiUiController.getAxeX().setAxisIterator(new CustomTickIterator());
        loiUiController.getAxeX().setTitleModifiable(false);
        loiUiController.getAxeX().setTitre("");
        loiUiController.getAxeX().setTitreVisible(false);
        loiUiController.getAxeX().setUniteVisible(false);
        traceBox = new TraceBox();
        traceBox.setVertical(true);
        traceBox.setVPosition(SwingConstants.TOP);
        traceBox.setHPosition(SwingConstants.CENTER);
        traceBox.setDrawFond(false);
        traceBox.setDrawBox(false);
    }

    private void initBox() {
        EGAxeHorizontal axeHorizontal = loiUiController.getAxeX();
        traceBox.setColorText(axeHorizontal.getLineColor());
        traceBox.setDrawFond(false);
        traceBox.setDrawBox(false);
    }

    @Override
    public void dessine(Graphics2D graphics2D, EGRepere egRepere) {
        Font old = graphics2D.getFont();
        initBox();
        graphics2D.setFont(loiUiController.getAxeX().getFont());
        int yMin = 3 + egRepere.getH() - getHeightNeeded(graphics2D) - loiUiController.getAxeX().getBottomHeightNeeded(graphics2D);
        LHPTLoiSectionZCourbeModel modelCourbe = (LHPTLoiSectionZCourbeModel) loiUiController.getGraphe().getSelectedComponent().getModel();
        int nbValues = modelCourbe.getNbValues();
        final CtuluRange xRange = egRepere.getXAxe().getRange();
        for (int i = 0; i < nbValues; i++) {
            final AocLoiLine loiLine = modelCourbe.getLoiLine(i);
            final double abscisse = loiLine.getPtEvolutionFF().getAbscisse();
            if (xRange.isValueContained(abscisse)) {
                int iEcran = egRepere.getXEcran(abscisse);
                traceBox.paintBox(graphics2D, iEcran, yMin, loiLine.getProfil());
            }
        }
        graphics2D.setFont(old);
    }

    @Override
    public int getHeightNeeded(Graphics2D graphics2D) {
        Font old = graphics2D.getFont();
        Font font = loiUiController.getAxeX().getFont();
        if (font == null) {
            font = old;
        }
        FontMetrics fontMetrics = graphics2D.getFontMetrics(font);
        initBox();
        int h = 0;
        LHPTLoiSectionZCourbeModel modelCourbe = (LHPTLoiSectionZCourbeModel) loiUiController.getGraphe().getSelectedComponent().getModel();
        int nbValues = modelCourbe.getNbValues();
        for (int i = 0; i < nbValues; i++) {
            final AocLoiLine loiLine = modelCourbe.getLoiLine(i);
            h = Math.max(h, fontMetrics.stringWidth(loiLine.getProfil()));
        }
        graphics2D.setFont(old);
        return 3 + h + traceBox.getTotalHeightMargin();
    }
}
