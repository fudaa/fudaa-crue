package org.fudaa.fudaa.crue.aoc.service;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.io.conf.CrueAocConfigurationHelper;
import org.fudaa.dodico.crue.io.conf.CrueEtudeConfigurationHelper;
import org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager;
import org.fudaa.fudaa.crue.options.config.ModellingLoiConfigurationConverter;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

import java.io.File;
import java.util.HashMap;

/**
 * Sauvegarde de la config de niveau étude.
 *
 * @author deniger
 */
public class AocSaveStudyConfigCallable implements ProgressRunnable<CtuluLog> {
  private final AocConfigService aocConfigService;

  public AocSaveStudyConfigCallable(AocConfigService aocConfigService) {
    this.aocConfigService = aocConfigService;
  }

  @Override
  public CtuluLog run(ProgressHandle progressHandle) {
    if (!aocConfigService.getSelectedProject().getInfos().isDirOfConfigDefined()) {
      return null;
    }
    //fin
    File destDir = aocConfigService.getSelectedProject().getInfos().getDirOfConfig();
    CrueEtudeConfigurationHelper.InputData input = new CrueEtudeConfigurationHelper.InputData();
    input.globalOptions = new HashMap<>();
    input.loiOptions = aocConfigService.getAocLoiConfiguration().getConfigByTopComponentId();
    CrueAocConfigurationHelper.write(new File(destDir, GlobalOptionsManager.ETUDE_AOC_FILE), input, new ModellingLoiConfigurationConverter());
    CtuluLog gr = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    gr.setDesc("save.configEtu.done");
    return gr;
  }
}
