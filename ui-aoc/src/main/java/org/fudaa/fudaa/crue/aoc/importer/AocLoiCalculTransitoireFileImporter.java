package org.fudaa.fudaa.crue.aoc.importer;

import org.fudaa.fudaa.crue.aoc.nodes.AocLoiCalculTansitoireNode;
import org.fudaa.fudaa.crue.common.helper.AbstractFileImporterProgressRunnable;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Handler pour importer un fichier csv ou excel.
 */
public class AocLoiCalculTransitoireFileImporter extends AbstractFileImporterProgressRunnable implements ProgressRunnable<List<AocLoiCalculTansitoireNode>> {
  /**
   * les calculs du scenario sélectionné
   */
  private final String[] calculs;
  /**
   * les lois
   */
  private final String[] lois;
  /**
   * les sections actives
   */
  private final String[] sections;
  private final Map<String, String> commentByCalculId;

  /**
   * @param file le fichier à importer
   * @param calculs les calculs disponibles
   * @param lois les lois disponibles
   * @param sections les sections actives disponibles
   */
  public AocLoiCalculTransitoireFileImporter(File file, String[] calculs, String[] lois, String[] sections, Map<String, String> commentByCalculId) {
    super(file);
    this.calculs = calculs;
    this.lois = lois;
    this.sections = sections;
    this.commentByCalculId = commentByCalculId;
  }

  @Override
  public List<AocLoiCalculTansitoireNode> run(ProgressHandle progressHandle) {
    String[][] values = null;
    if (file != null) {
      values = readFile();
    }
    if (values == null) {
      return null;
    }
    return AocLoiCalculTransitoireNodesImporter.extractLoiCalculTransitoireNode(calculs, lois, sections, commentByCalculId, values);
  }
}
