package org.fudaa.fudaa.crue.aoc.nodes;

import org.fudaa.dodico.crue.metier.aoc.AocWithSectionRef;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;

import java.beans.PropertyEditor;
import java.beans.PropertyEditorSupport;

/**
 * Propriete pour les sections
 *
 * @author deniger
 */
public class AocSectionProperty extends PropertySupportReflection<String> {
    /**
     * @param node     doit etre de type {@link AbstractNodeFirable}
     * @param instance l'objet a modifier
     */
    public AocSectionProperty(NodeWithSectionsContrat node, AocWithSectionRef instance) throws NoSuchMethodException {
        super((AbstractNodeFirable) node, instance, String.class, "getSectionRef", "setSectionRef");
    }

    @Override
    public PropertyEditor getPropertyEditor() {
        return new AocSectionPropertyEditor();
    }

    private class AocSectionPropertyEditor extends PropertyEditorSupport {
        @Override
        public String getAsText() {
            return (String) getValue();
        }

        @Override
        public void setAsText(String string) {
            setValue(string);
        }

        @Override
        public String[] getTags() {
            return ((NodeWithSectionsContrat) AocSectionProperty.this.node).getSections();
        }
    }
}
