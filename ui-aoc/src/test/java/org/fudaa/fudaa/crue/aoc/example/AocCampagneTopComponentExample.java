package org.fudaa.fudaa.crue.aoc.example;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.fudaa.crue.aoc.AocCampagneTopComponent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

public class AocCampagneTopComponentExample {

    public static void display(final JComponent jc, final File toDelete) {
        final JFrame fr = new JFrame();
        fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fr.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                if (toDelete != null) {
                    CtuluLibFile.deleteDir(toDelete);
                }
            }
        });

        fr.setContentPane(jc);
        fr.setSize(500, 600);
        fr.setVisible(true);
    }

    public static void main(final String[] args) {

        EventQueue.invokeLater(() -> {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (final ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                ex.printStackTrace();
            }
            ParametrageProfilSectionTopComponentExample.setDefaultSize(12);
            final AocCampagneTopComponent topComponent = new AocCampagneTopComponent();
            topComponent.setEditableForTest(true);
            topComponent.componentOpened();
            display(topComponent, null);
            topComponent.updateAllEditableState();
            topComponent.updateComponentStateWithBusinessRules();
        });
    }

}
