package org.fudaa.fudaa.crue.aoc.example;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormatETU;
import org.fudaa.dodico.crue.metier.etude.EMHProjectInfos;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.fudaa.fudaa.crue.aoc.ParametrageEchelleSectionTopComponent;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.netbeans.core.startup.CoreBridge;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ParametrageProfilSectionTopComponentExample {
  public static void main(String[] args) {
    final EMHProjet projet = configureProject();

    EventQueue.invokeLater(() -> {
      try {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
      } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
        ex.printStackTrace();
      }
      setDefaultSize(24);
      ParametrageEchelleSectionTopComponent topComponent = new ParametrageEchelleSectionTopComponent(null);
      topComponent.componentOpened();
      topComponent.setEditableForTest();

      AocCampagneTopComponentExample.display(topComponent, projet.getParentDirOfEtuFile());
    });
  }

  public static EMHProjet readScM3_0_c10(final Class toLoadLHPT, final String folder) {
    File target = null;
    try {
      target = CtuluLibFile.createTempDir();
    } catch (final IOException ex) {
      Exceptions.printStackTrace(ex);
    }
    AbstractTestParent.exportZip(target, "/Sc_M3-0_c10.zip");
    final File etuFile = new File(target, "EtuEx.etu.xml");
    final EMHProjet projet = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE);
    if (toLoadLHPT != null) {
      String lhptFileName = "M3-0_c10.lhpt.xml";
      final File lhpt = new File(target, lhptFileName);
      if (StringUtils.isBlank(folder)) {
        lhptFileName = "/" + lhptFileName;
      } else {
        lhptFileName = folder + "/" + lhptFileName;
      }
      try {
        final FileOutputStream out = new FileOutputStream(lhpt);
        CtuluLibFile.copyStream(toLoadLHPT.getResourceAsStream(lhptFileName), out, true, true);
      } catch (final Exception e) {
        e.printStackTrace();
      }
    }
    projet.setEtuFile(etuFile);
    Logger.getLogger(ParametrageProfilSectionTopComponentExample.class.getName()).log(Level.INFO, "etude unzip in " + target.getAbsolutePath());

    return projet;
  }

  public static EMHProjet configureProject() {
    CoreBridge.getDefault().registerPropertyEditors();
    EMHProjet projet = readScM3_0_c10(ParametrageProfilSectionTopComponentExample.class,null);
    final EMHProjectInfos infos = projet.getInfos();
    infos.getDirectories().put(EMHProjectInfos.CONFIG, "nowhere");
    projet.setInfos(infos);

    projet.setPropDefinition(TestCoeurConfig.INSTANCE);
    EMHProjetServiceImpl service = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
    service.setForTest(projet);
    return projet;
  }

  public static void setDefaultSize(int size) {

    Set<Object> keySet = UIManager.getLookAndFeelDefaults().keySet();
    Object[] keys = keySet.toArray(new Object[0]);

    for (Object key : keys) {

      if (key != null && key.toString().toLowerCase().contains("font")) {
        Font font = UIManager.getDefaults().getFont(key);
        if (font != null) {
          font = font.deriveFont((float) size);
          UIManager.put(key, font);
        }
      }
    }
  }
}
