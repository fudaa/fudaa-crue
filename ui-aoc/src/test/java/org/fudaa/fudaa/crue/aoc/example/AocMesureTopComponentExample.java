package org.fudaa.fudaa.crue.aoc.example;

import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.fudaa.crue.aoc.ParametrageMesurePostTraitementTopComponent;

import javax.swing.*;
import java.awt.*;

public class AocMesureTopComponentExample {
    public static void main(final String[] args) {
        final EMHProjet projet = ParametrageProfilSectionTopComponentExample.configureProject();

        EventQueue.invokeLater(() -> {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (final ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                ex.printStackTrace();
            }
            ParametrageProfilSectionTopComponentExample.setDefaultSize(12);
            final ParametrageMesurePostTraitementTopComponent topComponent = new ParametrageMesurePostTraitementTopComponent(null);
            topComponent.componentOpened();
            topComponent.setEditableForTest();

            AocCampagneTopComponentExample.display(topComponent, projet.getParentDirOfEtuFile());
        });
    }
}
