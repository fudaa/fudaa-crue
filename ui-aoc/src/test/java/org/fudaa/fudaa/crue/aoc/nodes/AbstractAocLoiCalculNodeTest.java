package org.fudaa.fudaa.crue.aoc.nodes;

import com.memoire.fu.FuEmptyArrays;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * @author deniger
 */
public class AbstractAocLoiCalculNodeTest {
    @Test
    public void addEmptyString()  {
        assertNull(AbstractAocLoiCalculNode.addEmptyString(null));
        assertEquals(0, AbstractAocLoiCalculNode.addEmptyString(FuEmptyArrays.STRING0).length);
        String[] strings = new String[]{"one", "two"};
        String[] res = AbstractAocLoiCalculNode.addEmptyString(strings);
        assertEquals(3, res.length);
        assertEquals("", res[0]);
        assertEquals("one", res[1]);
        assertEquals("two", res[2]);
    }
}
