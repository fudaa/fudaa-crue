Copier le fichier fudaacrueDevExample.conf en fudaacrueDevE.conf
Il est possible d'utiliser un autre fichier de conf pour le dev soit le fichier fudaacrueDev.conf.

Exemple:

# ${HOME} will be replaced by user home directory according to platform
#Changer 0.52 avec la version qui va bien
default_userdir="${HOME}/.${APPNAME}/0.52/"
default_mac_userdir="${HOME}/Library/Application Support/${APPNAME}"

# options used by the launcher by default, can be overridden by explicit
# command line switches
#la propriété dev.etcDir permet de changer le répertoire etc
default_options="-J-Ddev.etcDir="C:\Data\etc" --branding fudaacrue -J-Xms24m -J-Xmx512m"
# for development purposes you may wish to append: -J-Dnetbeans.logger.console=true -J-ea

# default location of JDK/JRE, can be overridden by using --jdkhome <dir> switch
#jdkhome="/path/to/jdk"

# clusters' paths separated by path.separator (semicolon on Windows, colon on Unices)
#extra_clusters=
