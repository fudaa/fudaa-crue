/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.metier.etude.EMHInfosVersion;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author Frederic Deniger
 */
public class ComparaisonSelectorContainerTest {

  public ComparaisonSelectorContainerTest() {
  }

  @Test
  public void testRetrieveEnum() {
    ManagerEMHScenario sc10 = createScenario(CrueVersionType.CRUE10);
    ManagerEMHScenario sc9 = createScenario(CrueVersionType.CRUE9);
    assertEquals(ComparaisonSelectorEnum.C9_C9, ComparaisonSelectorContent.retrieveEnum(sc9, sc9));
    assertEquals(ComparaisonSelectorEnum.C9_C10, ComparaisonSelectorContent.retrieveEnum(sc9, sc10));
    assertEquals(ComparaisonSelectorEnum.C9_C10, ComparaisonSelectorContent.retrieveEnum(sc10, sc9));
    assertEquals(ComparaisonSelectorEnum.C10_C10, ComparaisonSelectorContent.retrieveEnum(sc10, sc10));
  }

  private ManagerEMHScenario createScenario(final CrueVersionType CRUE10) {
    ManagerEMHScenario sc10 = new ManagerEMHScenario();
    final EMHInfosVersion emhInfosVersion = new EMHInfosVersion();
    emhInfosVersion.setVersion(CRUE10);
    sc10.setInfosVersions(emhInfosVersion);
    return sc10;
  }
}
