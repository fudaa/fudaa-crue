/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.apache.commons.beanutils.LazyDynaMap;
import org.fudaa.dodico.crue.common.PropertyUtilsCache;
import org.fudaa.dodico.crue.comparaison.tester.EqualsTesterBean;
import org.fudaa.dodico.crue.comparaison.tester.FactoryEqualsTester;
import org.fudaa.dodico.crue.comparaison.tester.TestOption;
import org.fudaa.dodico.crue.comparaison.tester.TesterContextImpl;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.result.ResCalBrancheSaintVenant;
import org.junit.Test;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 *
 * @author deniger
 */
public class EqualsTesterResultTest {

  @Test
  public void testComparisonResult() {
    try {
      final FactoryEqualsTester factoryEqualsTester = new FactoryEqualsTester(CrueConfigMetierForTest.DEFAULT, -1, -1);
      final EqualsTesterBean equalsTester = new EqualsTesterBean(factoryEqualsTester);
      final ResCalBrancheSaintVenant test = new ResCalBrancheSaintVenant();
      test.setSplanAct(10);
      test.setSplanSto(101);
      test.setSplanTot(102);
      final ResCalBrancheSaintVenant test2 = new ResCalBrancheSaintVenant();
      test2.setSplanAct(10);
      test2.setSplanSto(101);
      test2.setSplanTot(102);
      assertEquals(new Double(10), test2.getRes().get("splanAct"));
      final LazyDynaMap bean = new LazyDynaMap(test2.getRes());
      PropertyUtilsCache.getInstance().setActive(true);
      assertEquals(new Double(10), PropertyUtilsCache.getInstance().getProperty(bean, "splanAct"));
      assertTrue(equalsTester.isSame(new LazyDynaMap(test.getRes()), new LazyDynaMap(test2.getRes()), null, new TesterContextImpl(null), Arrays.asList("splanAct", "splanSto", "splanTot")));
    } catch (final Exception ex) {
      Logger.getLogger(EqualsTesterResultTest.class.getName()).log(Level.SEVERE, null, ex);
      fail(ex.getMessage());
    }
  }

  /**
   * Test les comparaisons relatives
   */
  @Test
  public void testComparisonResultEpsilon() {
    try {
      final FactoryEqualsTester factoryEqualsTester = new FactoryEqualsTester(CrueConfigMetierForTest.DEFAULT, -1, -1);
      final EqualsTesterBean equalsTester = new EqualsTesterBean(factoryEqualsTester);

      final ResCalBrancheSaintVenant test = new ResCalBrancheSaintVenant();
      final double value = 1e10;
      test.setSplanAct(value);
      test.setSplanSto(101);
      test.setSplanTot(102);
      final ResCalBrancheSaintVenant test2 = new ResCalBrancheSaintVenant();
      test2.setSplanAct(value + 101d);
      test2.setSplanSto(101);
      test2.setSplanTot(102);
      final LazyDynaMap map1 = new LazyDynaMap(test.getRes());
      final LazyDynaMap map2 = new LazyDynaMap(test2.getRes());
      final TesterContextImpl context = new TesterContextImpl(null);
      final List<String> variables = Collections.singletonList("splanAct");
      //l'ecart est de 1
      assertFalse(equalsTester.isSame(map1, map2, null, context, variables));
      context.activate(TestOption.RELATIVE_FOR_DOUBLE);
      assertTrue(equalsTester.isSame(map1, map2, null, context, variables));
      context.unActivate(TestOption.RELATIVE_FOR_DOUBLE);
      assertFalse(equalsTester.isSame(map1, map2, null, context, variables));
    } catch (final Exception ex) {
      Logger.getLogger(EqualsTesterResultTest.class.getName()).log(Level.SEVERE, null, ex);
      fail(ex.getMessage());
    }
  }

  @Test
  public void testLazy() {
    try {
      final Map test = new HashMap();
      test.put("splanAct", 20);
      test.put("splanSto", 30);
      test.put("vol", 10);
      final LazyDynaMap bean = new LazyDynaMap(Collections.unmodifiableMap(test));
      final Object property = PropertyUtilsCache.getInstance().getProperty(bean, "vol");
      assertEquals(new Integer(10), property);
    } catch (final Exception ex) {
      Logger.getLogger(EqualsTesterResultTest.class.getName()).log(Level.SEVERE, null, ex);
      fail(ex.getMessage());
    }

  }
}
