/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.comparaison.config.ConfCompareListe;
import org.fudaa.dodico.crue.comparaison.config.ConfCompareObject;
import org.fudaa.dodico.crue.comparaison.config.ConfOption;
import org.fudaa.dodico.crue.comparaison.tester.EqualsTesterIsIncludeCollection;
import org.fudaa.dodico.crue.comparaison.tester.FactoryEqualsTester;
import org.fudaa.dodico.crue.comparaison.tester.TestOption;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Test sur la selection
 *
 * @author deniger
 */
public class ConvertCompareTest {
    /**
     * Test du converter pour EqualsTesterBean
     */
    @Test
    public void testCreateConfCompareObject() {
        final ConfCompareObject in = new ConfCompareObject();
        in.setOptions(Arrays.asList(
                new ConfOption(TestOption.IGNORE_COMMENT.getKey(), TestOption.IGNORE_COMMENT.getActive()),
                new ConfOption(TestOption.ONLY_LOIZ_FIRST_POINT.getKey(), TestOption.ONLY_LOIZ_FIRST_POINT.getActive()),
                new ConfOption(TestOption.RELATIVE_FOR_DOUBLE.getKey(), TestOption.RELATIVE_FOR_DOUBLE.getActive())));
        in.setDeep(23);
        final List<String> asList = Arrays.asList("a1", "a2");
        in.setAttributs(asList);
        final ConvertCompare builder = new ConvertCompare();
        builder.setFactory(new FactoryEqualsTester(CrueConfigMetierForTest.DEFAULT, -1, -1));
        final CtuluLog bilan = new CtuluLog();
        final ExecuteCompareActionOnObject res = (ExecuteCompareActionOnObject) builder.convert(in, bilan, null);
        Assert.assertEquals(23, res.tester.getContextFactory().getMaxDeep());
        final Collection<String> propToCompare2 = res.tester.getContextFactory().create().getPropToCompare();
        Assert.assertEquals(2, propToCompare2.size());
        final Iterator<String> iterator = propToCompare2.iterator();
        Assert.assertEquals("a1", iterator.next());
        Assert.assertEquals("a2", iterator.next());
        Assert.assertTrue(res.getTester().getContextFactory().isIgnoreComment());
        Assert.assertTrue(res.getTester().getContextFactory().isActivated(TestOption.IGNORE_COMMENT));
        Assert.assertTrue(res.getTester().getContextFactory().isActivated(TestOption.ONLY_LOIZ_FIRST_POINT));
        Assert.assertTrue(res.getTester().getContextFactory().isActivated(TestOption.RELATIVE_FOR_DOUBLE));
    }

    /**
     * Test du converter pour EqualsTesterIsIncludeCollection
     */
    @Test
    public void testCreateEqualsTesterIsIncludeCollection() {
        final ConfCompareListe in = new ConfCompareListe();
        in.setBidirect(true);
        in.setAttribut("toto");
        final ConvertCompare builder = new ConvertCompare();
        final CtuluLog bilan = new CtuluLog();
        final ExecuteCompareActionOnList res = (ExecuteCompareActionOnList) builder.convert(in, bilan, null);
        Assert.assertEquals(in.getAttribut(), ((EqualsTesterIsIncludeCollection) res.tester).getPropName());
        Assert.assertEquals(in.isBidirect(), ((EqualsTesterIsIncludeCollection) res.tester).isBidirect());
    }
}
