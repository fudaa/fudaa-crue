/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author Frederic Deniger
 */
public class EqualsTesterDoubleTest {

  public EqualsTesterDoubleTest() {
  }

  @Test
  public void testIsSameSafe() throws Exception {
    ItemVariable property = CrueConfigMetierForTest.DEFAULT.getProperty(CrueConfigMetierConstants.PROP_XT);
    EqualsTesterDouble tester = new EqualsTesterDouble(property, true);
    assertTrue(tester.isSame(Double.valueOf(1), Double.valueOf(1)));
    assertTrue(tester.isSame(Double.valueOf(1), Double.valueOf(1 + property.getEpsilon().getEpsilonComparaison() / 2d)));
    assertFalse(tester.isSame(Double.valueOf(1), Double.valueOf(1 + property.getEpsilon().getEpsilonComparaison())));
  }

  @Test
  public void testIsSameSafeRelative() throws Exception {
    final double relativeEps = 1e-6;
    ItemVariable property = CrueConfigMetierForTest.DEFAULT.getProperty(CrueConfigMetierConstants.PROP_XT);
    EqualsTesterDouble tester = new EqualsTesterDouble(property, true);
    Double v1 = 1e7;
    Double v2 = 1e7 + 1;
    assertFalse(tester.isSame(v1, v2));
    TesterContextImpl ctx = new TesterContextImpl(null);
    ctx.activate(TestOption.RELATIVE_FOR_DOUBLE);
    assertTrue(tester.isSame(v1, v2, null, ctx));
    //on teste le cas ou la différence absolue ne lève pas de différence mais par contre la différence relative le devrait.
    v1 = 1e-7;
    v2 = 1e-7 + property.getEpsilon().getEpsilonComparaison() / 2d;
    assertTrue(tester.isSame(v1, v2));//sans l'option
    assertTrue(tester.isSame(v1, v2, null, ctx));//avec l'option relative c'est bien egale
    assertTrue(tester.getDiffRelative(v1, v2) > relativeEps);//alors que la difference relative est supérieure à ce qui est attenu.

  }
}
