/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.apache.commons.collections4.PredicateUtils;
import org.apache.commons.jxpath.JXPathContext;
import org.fudaa.dodico.crue.common.CustomPropertyUtilsBeanInstaller;
import org.fudaa.dodico.crue.comparaison.config.ConfComparaisonConteneur;
import org.fudaa.dodico.crue.comparaison.io.ReaderConfig;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHFactory;
import org.fudaa.dodico.crue.metier.factory.EMHNoeudFactory;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * @author deniger
 */
public class RequeteTest {

  @Test
  public void testSelectActive() {
    final EMHNoeudFactory nodeFactory = new EMHNoeudFactory();

    final EMHSousModele modele = new EMHSousModele();
    final EMHNoeudNiveauContinu nd = nodeFactory.getNode("inactive");// new EMHNoeudNiveauContinu("inactive");
    final EMHBrancheOrifice nd2 = new EMHBrancheOrifice("inactive");
    nd2.setUserActive(false);
    EMHRelationFactory.addRelationContientEMH(modele, nd);
    EMHRelationFactory.addRelationContientEMH(modele, nd2);

    final JXPathContext path = JXPathContext.newContext(modele);
    final List selectNodes = path.selectNodes("allSimpleEMH[userActive='true']");
    Assert.assertEquals(1, selectNodes.size());
    Assert.assertEquals(nd, selectNodes.get(0));
  }

  @Test
  public void testValParam() {
    final EMHModeleBase model = new EMHModeleBase();
    final OrdPrtCIniModeleBase o = new OrdPrtCIniModeleBase(CrueConfigMetierForTest.DEFAULT);
    model.addInfosEMH(o);
    o.addValParam(new ValParamDouble("tiot", 1d));
    o.addValParam(new ValParamDouble("tiot", 2d));
    JXPathContext path = JXPathContext.newContext(model);
    List selectNodes = path.selectNodes("ordPrtCIniModeleBase");
    Assert.assertEquals(1, selectNodes.size());
    path = JXPathContext.newContext(model);
    selectNodes = path.selectNodes("ordPrtCIniModeleBase/valParam");
    Assert.assertEquals(2, selectNodes.size());

  }

  @Test
  public void testSelectAllDptgSection() {
    final JXPathContext path = JXPathContext.newContext(createModele());
    List nodes = path.selectNodes("allSimpleEMH/DPTGNommes[type=\"DonPrtGeoProfilSection\"]");
    Assert.assertEquals(2, nodes.size());
    nodes = path.selectNodes("allSimpleEMH/DPTGNommes[type=\"DonPrtGeoProfilSection\"]/litNumerote/nomLit");
    Assert.assertEquals(4, nodes.size());
  }

  @Test
  public void testSelectBrancheSeuil() {
    final JXPathContext path = JXPathContext.newContext(createModele());
    List nodes = path.selectNodes("branches[contains(type,'Seuil')]");
    Assert.assertEquals(2, nodes.size());
    nodes = path.selectNodes("branches[type='EMHBrancheSeuilTransversal'|'EMHBrancheSeuilLateral']");
    Assert.assertEquals(2, nodes.size());
  }

  private EMHSousModele createModele() {
    final EMHNoeudFactory nodeFactory = new EMHNoeudFactory();
    final EMHSousModele modele = new EMHSousModele();
    final EMHNoeudNiveauContinu nd = nodeFactory.getNode("inactive");// new EMHNoeudNiveauContinu("inactive");
    final RelationEMHCasierDansNoeud createCasierDansNoeud = EMHRelationFactory.createCasierDansNoeud(
            new EMHCasierProfil("casier"));
    nd.addRelationEMH(createCasierDansNoeud);
    EMHRelationFactory.addRelationContientEMH(modele, nd);
    final EMHSectionIdem idem = new EMHSectionIdem("idem");
    idem.addInfosEMH(new DonPrtGeoSectionIdem(CrueConfigMetierForTest.DEFAULT));
    EMHRelationFactory.addRelationContientEMH(modele, idem);
    EMHRelationFactory.addRelationContientEMH(modele, createBranche("inactive"));
    EMHRelationFactory.addRelationContientEMH(modele, createBranche("Br2"));
    final EMHBrancheSeuilLateral contenu = new EMHBrancheSeuilLateral("sl1");
    contenu.addInfosEMH(new DonCalcSansPrtBrancheSeuilLateral());
    EMHRelationFactory.addRelationContientEMH(modele, contenu);
    EMHRelationFactory.addRelationContientEMH(modele, new EMHBrancheSeuilTransversal("st1"));
    final EMHBrancheBarrageFilEau barrage = new EMHBrancheBarrageFilEau("filEau");
    final DonCalcSansPrtBrancheBarrageFilEau dcspFil = new DonCalcSansPrtBrancheBarrageFilEau(
            CrueConfigMetierForTest.DEFAULT);
    barrage.addInfosEMH(dcspFil);
    final CalcPseudoPermBrancheOrificeManoeuvre dclm = new CalcPseudoPermBrancheOrificeManoeuvre(
            CrueConfigMetierForTest.DEFAULT);
    final CalcPseudoPerm newCalculParent = new CalcPseudoPerm();
    newCalculParent.setNom("calcPseudoPerm");
    dclm.setCalculParent(newCalculParent);
    final CalcPseudoPermBrancheSaintVenantQruis dclmQruis = new CalcPseudoPermBrancheSaintVenantQruis(
            CrueConfigMetierForTest.DEFAULT);
    dclmQruis.setCalculParent(newCalculParent);
    barrage.addInfosEMH(dclm);
    barrage.addInfosEMH(dclmQruis);
    EMHRelationFactory.addRelationContientEMH(modele, barrage);
    return modele;
  }

  private EMHBrancheSaintVenant createBranche(final String nom) {
    final EMHNoeudFactory nodeFactory = new EMHNoeudFactory();

    final EMHBrancheSaintVenant branche = new EMHBrancheSaintVenant(nom);
    final DonPrtGeoProfilCasier casier = new DonPrtGeoProfilCasier(CrueConfigMetierForTest.DEFAULT);
    casier.setNom("casier");
    final LitUtile newLitUtile = new LitUtile();
    newLitUtile.setLimDeb(new PtProfil(3, 4));
    casier.setLitUtile(newLitUtile);
    branche.addInfosEMH(casier);
    final DonPrtGeoProfilSection dptgSection = new DonPrtGeoProfilSection();
    dptgSection.setNom("Sect_" + branche.getNom());
    dptgSection.setFente(new DonPrtGeoProfilSectionFenteData(CrueConfigMetierForTest.DEFAULT));
    dptgSection.addPtProfil(new PtProfil(23, 23));
    dptgSection.addLitNumerote(createLit(nom + "lit"));
    dptgSection.addLitNumerote(createLit(nom + "lit2"));
    branche.addInfosEMH(dptgSection);
    branche.addInfosEMH(new DonPrtCIniBranche(CrueConfigMetierForTest.DEFAULT));
    branche.addRelationEMH(EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(branche, true, new EMHSectionInterpolee(
            "section"), CrueConfigMetierForTest.DEFAULT));
    branche.setNoeudAmont(nodeFactory.getNode("titi")/*
             * new EMHNoeudNiveauContinu("titi")
             */);
    branche.setNoeudAval(nodeFactory.getNode("tito")/*
             * new EMHNoeudNiveauContinu("tito")
             */);
    branche.setUserActive(false);
    return branche;
  }

  private LitNumerote createLit(final String nom) {
    final LitNumerote lit = new LitNumerote();
    lit.setNomLit(new LitNomme(nom, 0));
    lit.setLimDeb(new PtProfil(1, 2));
    final DonFrtStrickler frt = EMHFactory.createDonFrtZFk("stri");
    frt.getLoi().getEvolutionFF().getPtEvolutionFF().add(new PtEvolutionFF(12, 1));
    lit.setFrot(frt);
    return lit;
  }

  private EMHScenario createScenario() {
    final EMHSousModele ss = createModele();
    final EMHModeleBase modele = new EMHModeleBase();
    modele.addRelationEMH(EMHRelationFactory.createRelationContient(ss));
    final EMHScenario res = new EMHScenario();
    EMHRelationFactory.addRelationContientEMH(res, modele);
    final OrdCalcScenario ord = new OrdCalcScenario();
    res.addInfosEMH(ord);
    return res;
  }

  @Test
  public void testFile() {
    CustomPropertyUtilsBeanInstaller.install();
    final ReaderConfig readerConfig = new ReaderConfig();
    final ConfComparaisonConteneur read = readerConfig.read(ReaderConfig.class.getResource("default-comparaison.xml"));
    final ExecuteComparaison comp = new ExecuteComparaison(read, CrueConfigMetierForTest.DEFAULT, null, -1, -1);
    final List<ExecuteComparaisonResult> launch = comp.launch(createScenario(), createScenario(), PredicateUtils.truePredicate());
    for (final ExecuteComparaisonResult compareResult : launch) {
      if (compareResult.getLog().containsErrorOrSevereError()) {
        compareResult.getLog().printResume();
        Assert.fail();
      }

    }
  }
}
