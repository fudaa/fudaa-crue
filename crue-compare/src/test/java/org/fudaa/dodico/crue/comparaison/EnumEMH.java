/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

/**
 * Les variables utilisable dans Crue
 */
public enum EnumEMH {
  /**
   * z
   */
  SCENARIO,
  /**
   * Temps
   */
  T,
  /**
   * Débit d'apport
   */
  Q_APP,
  /**
   * débit de ruissellement
   */
  Q_RUIS,
  /**
   * pourcentage ouverture
   */
  OUV, Z_AM, Z_AV, Q, D_Z, Q_PIL
}
