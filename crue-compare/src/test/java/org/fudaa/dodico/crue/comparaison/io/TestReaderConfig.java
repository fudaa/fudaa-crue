/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.io;

import com.thoughtworks.xstream.XStream;
import org.fudaa.dodico.crue.comparaison.config.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author deniger
 */
public class TestReaderConfig {
  @Test
  public void testRead() {
    final ReaderConfig readerConfig = new ReaderConfig();
    final ConfComparaisonConteneur read = readerConfig.read(TestReaderConfig.class.getResource("testComparaison.xml"));
    Assert.assertNotNull(read);
    Assert.assertEquals(1, read.getComparaisons().size());
    final ConfComparaison comparaison = read.getComparaisons().get(0);
    Assert.assertEquals("testId8", comparaison.getId());
    Assert.assertEquals("testIt", comparaison.getNom());
    Assert.assertEquals(1, comparaison.getCompare().size());
    final List<ConfSelectionItem> items = comparaison.getSelection().getItems();
    Assert.assertEquals(2, items.size());
    final ConfCompareObject compare = (ConfCompareObject) comparaison.getCompare().get(0);
    Assert.assertEquals(2, compare.getDeep());
    Assert.assertEquals(2, compare.getAttributs().size());
    Assert.assertEquals(2, compare.getOptions().size());
    Assert.assertEquals("titi", compare.getAttributs().get(0));
    Assert.assertEquals("titi2", compare.getAttributs().get(1));
    Assert.assertEquals("nomOPtion", compare.getOptions().get(0).getNom());
    Assert.assertEquals("valeurOPtion", compare.getOptions().get(0).getValeur());
    Assert.assertEquals("nomOPtion2", compare.getOptions().get(1).getNom());
    Assert.assertEquals("valeurOPtion2", compare.getOptions().get(1).getValeur());
    final ConfSelectionItemMerge selectionItem = (ConfSelectionItemMerge) items.get(0);
    Assert.assertEquals("attToMerge", selectionItem.getAttribut());
    Assert.assertEquals("Merge sur att", selectionItem.getDescription());
    Assert.assertEquals("select * from", selectionItem.getRequest().getRequest());
    Assert.assertEquals("{nom}", selectionItem.getRequest().getParamToPrint());
    final ConfSelectionItemRequete selectionItemRequete = (ConfSelectionItemRequete) items.get(1);
    Assert.assertEquals("select from merge", selectionItemRequete.getRequest());
    Assert.assertEquals("Select", selectionItemRequete.getDescription());
    Assert.assertEquals("{nom}", selectionItemRequete.getParamToPrint());
  }

  /**
   * Test de lecture
   */
  @Test
  public void testWrite() {

    final ConfComparaisonConteneur cont = new ConfComparaisonConteneur();
    cont.setComparaisons(new ArrayList<>());
    final ConfComparaison c1 = new ConfComparaison();
    cont.getComparaisons().add(c1);
    c1.setId("id");
    c1.setNom("test");
    final ConfCompareObject compare = new ConfCompareObject();

    final ArrayList<String> attributs = new ArrayList<>();
    attributs.add("titi");
    attributs.add("titi2");
    compare.setAttributs(attributs);

    compare.setOptions(new ArrayList<>());
    final ConfOption coucou = new ConfOption();
    coucou.setNom("nomOPtion");
    coucou.setValeur("valeurOPtion");
    final ConfOption coucou2 = new ConfOption();
    coucou2.setNom("nomOPtion2");
    coucou2.setValeur("valeurOPtion2");
    compare.getOptions().add(coucou);
    compare.getOptions().add(coucou2);
    final ArrayList<ConfCompare> compares = new ArrayList<>();
    compares.add(compare);
    c1.setCompare(compares);
    final ConfSelection selec = new ConfSelection();
    final ConfSelectionItemMerge merge = new ConfSelectionItemMerge();
    merge.setAttribut("attToMerge");

    final ConfSelectionItemRequete requ = new ConfSelectionItemRequete();
    requ.setRequest("select * from");
    merge.setRequest(requ);
    selec.setItems(new ArrayList<>());
    selec.getItems().add(merge);
    c1.setSelection(selec);
    final ReaderConfig readerConfig = new ReaderConfig();
    final XStream createXstream = readerConfig.createXstream();
    try {
      final String xml = createXstream.toXML(cont);
      createXstream.fromXML(xml);
    } catch (final Exception e) {
      e.printStackTrace();
      Assert.fail(e.getMessage());
    }
  }
}
