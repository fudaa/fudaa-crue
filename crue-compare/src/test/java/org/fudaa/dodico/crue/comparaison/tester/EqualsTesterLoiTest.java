/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.emh.EvolutionFF;
import org.fudaa.dodico.crue.metier.emh.LoiDF;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author deniger
 */
public class EqualsTesterLoiTest {

  /**
   * Tester le comparateur de lois.
   */
  @Test
  public void testEqualsTesterLoiFF() {
    final LoiFF createLoi1 = createLoi();
    final LoiFF createLoi2 = createLoi();
    final EqualsTesterLoi tester = new EqualsTesterLoi(new FactoryEqualsTester(CrueConfigMetierForTest.DEFAULT, -1, -1));
    final ResultatTest resTest = new ResultatTest();
    final TesterContext ctx = new TesterContextImpl(null);
    try {
      Assert.assertTrue(tester.isSameSafe(createLoi1, createLoi2, resTest, ctx));
      final PtEvolutionFF ptEvolutionFF = createLoi1.getEvolutionFF().getPtEvolutionFF().get(0);
      ptEvolutionFF.setAbscisse(ptEvolutionFF.getAbscisse() + 1E-6);
      // on reste dans le epsilon défini par les abscisse
      Assert.assertTrue(tester.isSameSafe(createLoi1, createLoi2, resTest, ctx));
      ptEvolutionFF.setAbscisse(ptEvolutionFF.getAbscisse() + 1.1);
      Assert.assertFalse(tester.isSameSafe(createLoi1, createLoi2, resTest, ctx));
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }
  }

  @Test
  public void testEqualsTesterLoiFFRelative() {
    final LoiFF loi1 = createLoi();
    final LoiFF loi2 = createLoi();
    //le delta relatif est <1.e-6
    loi1.getEvolutionFF().getPtEvolutionFF().get(0).setAbscisse(2000000.12);
    loi2.getEvolutionFF().getPtEvolutionFF().get(0).setAbscisse(2000000.13);
    final EqualsTesterLoi tester = new EqualsTesterLoi(new FactoryEqualsTester(CrueConfigMetierForTest.DEFAULT, -1, -1));
    final ResultatTest resTest = new ResultatTest();
    final TesterContextImpl ctx = new TesterContextImpl(null);
    try {
      Assert.assertFalse("no same same in absolute", tester.isSameSafe(loi1, loi2, resTest, ctx));//cas non relatif
      ctx.activate(TestOption.RELATIVE_FOR_DOUBLE);
      Assert.assertTrue("same in relatif", tester.isSameSafe(loi1, loi2, resTest, ctx));//cas
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }
  }

  @Test
  public void testEqualsTesterLoiZFirst() {
    final LoiFF createLoi1 = createLoi();
    final LoiFF createLoi2 = createLoi();
    final PtEvolutionFF pt1 = createLoi2.getEvolutionFF().getPtEvolutionFF().get(1);
    final PtEvolutionFF pt0 = createLoi2.getEvolutionFF().getPtEvolutionFF().get(0);
    pt1.setAbscisse(pt1.getAbscisse() + 3);
    final EqualsTesterLoi tester = new EqualsTesterLoi(new FactoryEqualsTester(CrueConfigMetierForTest.DEFAULT, -1, -1));
    ResultatTest resTest = new ResultatTest();
    final TesterContextImpl ctx = new TesterContextImpl(null);
    try {
      Assert.assertFalse(tester.isSameSafe(createLoi1, createLoi2, resTest, ctx));
      resTest = new ResultatTest();
      ctx.activate(TestOption.ONLY_LOIZ_FIRST_POINT);
      Assert.assertTrue(tester.isSameSafe(createLoi1, createLoi2, resTest, ctx));
      pt0.setAbscisse(pt0.getAbscisse() + 3);
      resTest = new ResultatTest();
      Assert.assertFalse(tester.isSameSafe(createLoi1, createLoi2, resTest, ctx));
      Assert.assertEquals("comparaison.loiZFirstPoint.difference", resTest.getFils().get(0).getFils().get(0).getMsg());
      //cas d'une loi d'un autre type.
      createLoi1.setType(EnumTypeLoi.LoiQDz);
      createLoi2.setType(EnumTypeLoi.LoiQDz);
      Assert.assertTrue(tester.isSameSafe(createLoi1, createLoi2, resTest, ctx));
      createLoi1.setType(EnumTypeLoi.LoiZDact);
      createLoi2.setType(EnumTypeLoi.LoiZDact);

      //on teste le cas ou une loi n'a pas de points
      resTest = new ResultatTest();
      createLoi1.getEvolutionFF().getPtEvolutionFF().clear();
      Assert.assertFalse(tester.isSameSafe(createLoi1, createLoi2, resTest, ctx));
      Assert.assertEquals("comparaison.loiZFirstPoint.empty", resTest.getFils().get(0).getFils().get(0).getMsg());
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }
  }

  @Test
  public void testEqualsTesterLoiZIgnoreFirst() {
    final LoiFF createLoi1 = createLoi();
    final LoiFF createLoi2 = createLoi();
    final PtEvolutionFF pt1 = createLoi2.getEvolutionFF().getPtEvolutionFF().get(1);
    final PtEvolutionFF pt0 = createLoi2.getEvolutionFF().getPtEvolutionFF().get(0);
    pt0.setAbscisse(pt1.getAbscisse() + 3);//on modifie le premier point
    final EqualsTesterLoi tester = new EqualsTesterLoi(new FactoryEqualsTester(CrueConfigMetierForTest.DEFAULT, -1, -1));
    ResultatTest resTest = new ResultatTest();
    final TesterContextImpl ctx = new TesterContextImpl(null);
    try {
      Assert.assertFalse(tester.isSameSafe(createLoi1, createLoi2, resTest, ctx));
      resTest = new ResultatTest();
      ctx.activate(TestOption.LOIZ_DONT_COMPARE_FIRST_POINT);
      Assert.assertTrue(tester.isSameSafe(createLoi1, createLoi2, resTest, ctx));
      pt1.setAbscisse(pt1.getAbscisse() + 3);//on modifie le point 1
      resTest = new ResultatTest();
      Assert.assertFalse(tester.isSameSafe(createLoi1, createLoi2, resTest, ctx));
      final ResultatTest diff = resTest.getFils().get(0).getFils().get(0);
      Assert.assertEquals("comparaison.collection.points", diff.getMsg());
      Assert.assertEquals("1", diff.getMsgArguments()[0].toString());

      //on teste le cas ou une loi n'a pas de points
      resTest = new ResultatTest();
      createLoi1.getEvolutionFF().getPtEvolutionFF().clear();
      Assert.assertFalse(tester.isSameSafe(createLoi1, createLoi2, resTest, ctx));
      Assert.assertEquals("comparaison.collection.size", resTest.getFils().get(0).getFils().get(0).getMsg());
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }
  }

  /**
   * Tester le comparateur de lois.
   */
  @Test
  public void testEqualsTesterLoiDF() {
    final LoiDF createLoi1 = createLoiDF();
    final LoiDF createLoi2 = createLoiDF();
    final EqualsTesterLoi tester = new EqualsTesterLoi(new FactoryEqualsTester(CrueConfigMetierForTest.DEFAULT, -1, -1));
    final ResultatTest resTest = new ResultatTest();
    final TesterContext ctx = new TesterContextImpl(null);
    try {
      Assert.assertTrue(tester.isSameSafe(createLoi1, createLoi2, resTest, ctx));
      final PtEvolutionFF ptEvolutionFF = createLoi1.getEvolutionFF().getPtEvolutionFF().get(0);
      ptEvolutionFF.setAbscisse(ptEvolutionFF.getAbscisse() + 0.0005);
      // on reste dans le epsilon défini par les abscisse
      Assert.assertTrue(tester.isSameSafe(createLoi1, createLoi2, resTest, ctx));
      createLoi1.setNom(createLoi2.getNom());
      // cette fois en dehors du epsilon
      ptEvolutionFF.setAbscisse(ptEvolutionFF.getAbscisse() + 0.001);
      Assert.assertFalse(tester.isSameSafe(createLoi1, createLoi2, resTest, ctx));
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }
  }

  private static LoiFF createLoi() {
    final LoiFF loi = new LoiFF();
    loi.setType(EnumTypeLoi.LoiZBeta);
    final EvolutionFF evol = new EvolutionFF();
    final List<PtEvolutionFF> pts = new ArrayList<>();
    evol.setPtEvolutionFF(pts);
    pts.add(new PtEvolutionFF(1, 2));
    pts.add(new PtEvolutionFF(3, 4));
    loi.setEvolutionFF(evol);
    loi.setNom("Titi");
    return loi;
  }

  private static LoiDF createLoiDF() {
    final LoiDF loi = new LoiDF();
    loi.setType(EnumTypeLoi.LoiTOuv);
    final EvolutionFF evol = new EvolutionFF();
    final List<PtEvolutionFF> pts = new ArrayList<>();
    evol.setPtEvolutionFF(pts);
    pts.add(new PtEvolutionFF(1, 2));
    pts.add(new PtEvolutionFF(3, 4));
    loi.setEvolutionFF(evol);
    loi.setNom("Titi");
    return loi;
  }
}
