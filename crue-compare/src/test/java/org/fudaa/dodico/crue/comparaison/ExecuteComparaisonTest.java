/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.apache.commons.collections4.PredicateUtils;
import org.fudaa.dodico.crue.comparaison.config.ConfComparaison;
import org.fudaa.dodico.crue.comparaison.config.ConfComparaisonConteneur;
import org.fudaa.dodico.crue.comparaison.config.ConfCompareListe;
import org.fudaa.dodico.crue.comparaison.config.ConfSelection;
import org.fudaa.dodico.crue.comparaison.tester.ResultatTest;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author deniger
 */
public class ExecuteComparaisonTest {
  /**
   * Test de ExecuteComparaison
   */
  @Test
  public void testExecuteComparaison() {
    final ConfSelection sel = new ConfSelection();
    sel.getItems().add(ConvertSelectOnTest.createMergeBrancheRequete());
    sel.getItems().add(ConvertSelectOnTest.getSelectSelectionRequete());
    final EMHSousModele m1 = ConvertSelectOnTest.createModeleWithRelation("titi", "tata");
    final EMHSousModele m2 = ConvertSelectOnTest.createModeleWithRelation("tata", "titi");
    final ConfComparaison comparaison = new ConfComparaison();
    comparaison.setSelection(sel);
    final ConfCompareListe compare = new ConfCompareListe();
    compare.setAttribut("emh.nom");
    compare.setBidirect(true);
    comparaison.setCompare(new ArrayList<>(Collections.singletonList(compare)));
    final ConfComparaisonConteneur conteneur = new ConfComparaisonConteneur();
    conteneur.getComparaisons().add(comparaison);
    final ExecuteComparaison cont = new ExecuteComparaison(conteneur, CrueConfigMetierForTest.DEFAULT, null, -1, -1);
    final List<ExecuteComparaisonResult> launch = cont.launch(m1, m2, PredicateUtils.truePredicate());
    final ExecuteComparaisonResult compareResult = launch.get(0);
    Assert.assertEquals(6, compareResult.getNbObjectTested());
    Assert.assertEquals(m1, compareResult.getRes().getObjetA());
    Assert.assertEquals(m2, compareResult.getRes().getObjetB());
    final List<ResultatTest> fils = compareResult.getRes().getFils();
    Assert.assertEquals(2, fils.size());
    Assert.assertTrue(fils.get(0).getObjetA() instanceof CatEMHBranche);
    Assert.assertTrue(fils.get(0).getObjetB() instanceof CatEMHBranche);
    Assert.assertTrue(fils.get(1).getObjetA() instanceof CatEMHBranche);
    Assert.assertTrue(fils.get(1).getObjetB() instanceof CatEMHBranche);
    Assert.assertEquals(3, fils.get(1).getNbObjectTested());
    Assert.assertEquals(3, fils.get(0).getNbObjectTested());
  }
}
