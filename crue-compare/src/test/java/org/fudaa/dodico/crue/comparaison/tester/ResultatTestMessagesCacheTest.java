package org.fudaa.dodico.crue.comparaison.tester;

import org.junit.Test;

import static org.junit.Assert.*;

public class ResultatTestMessagesCacheTest {
  @Test
  public void getMsg() {
    assertEquals(1,1);
    ResultatTestMessagesCache.getInstance().setUseCache(true);
    final String test = ResultatTestMessagesCache.getInstance().getMsg("conf.validCoeur");
    assertEquals(test,ResultatTestMessagesCache.getInstance().getInCache("conf.validCoeur"));
  }
}
