/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.PredicateUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.comparaison.io.SelectorItem;
import org.fudaa.dodico.crue.comparaison.io.SelectorItemContainer;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * @author Frederic Deniger
 */
public class ComparaisonSelectorReaderTest {
  private final Set<String> knownIds = new HashSet<>();

  public ComparaisonSelectorReaderTest() {
    knownIds.add("DCLM1");
    knownIds.add("DCLM2");
    knownIds.add("DCLM3");
  }

  @Test
  public void testGetFiles() {
    ComparaisonSelectorReader reader = new ComparaisonSelectorReader(knownIds);
    assertEquals(ComparaisonSelectorReader.C9C9_FILE, reader.getFiles().get(ComparaisonSelectorEnum.C9_C9));
    assertEquals(ComparaisonSelectorReader.C9C10_FILE, reader.getFiles().get(ComparaisonSelectorEnum.C9_C10));
    assertEquals(ComparaisonSelectorReader.C10C10_FILE, reader.getFiles().get(ComparaisonSelectorEnum.C10_C10));
  }

  @Test
  public void testCreate() {
    ComparaisonSelectorReader reader = new ComparaisonSelectorReader(knownIds);
    SelectorItemContainer container = new SelectorItemContainer();
    container.getItems().add(new SelectorItem("Test1"));
    container.getItems().add(new SelectorItem("Test2"));
    testPredicate(reader.create(container), "Test1", "Test2");
  }

  private void testPredicate(Predicate predicate, String... expectedArray) {
    ComparaisonSelectorPredicate create = (ComparaisonSelectorPredicate) predicate;
    Set<String> rejectedIds = create.getRejectedIds();
    assertEquals(expectedArray.length, rejectedIds.size());
    for (String expected : expectedArray) {
      assertTrue(rejectedIds.contains(expected));
    }
  }

  @Test
  public void testReadAndValid_File() {
    ComparaisonSelectorReader reader = new ComparaisonSelectorReader(knownIds);
    //on extrait les fichiers
    File siteDir = null;
    try {
      siteDir = extractDir(reader);
      CrueOperationResult<ComparaisonSelectorContent> readAndValid = reader.readAndValid(siteDir);
      ComparaisonSelectorContent result = readAndValid.getResult();
      testPredicate(result.getPredicate(ComparaisonSelectorEnum.C9_C9), "DCLM1", "DCLM2");
      testPredicate(result.getPredicate(ComparaisonSelectorEnum.C9_C10), "DCLM3");
      testPredicate(result.getPredicate(ComparaisonSelectorEnum.C10_C10), "DCLM1", "DCLM2", "DCLM4");
      //valid
      assertTrue(readAndValid.getLogs().containsSomething());
      CtuluLog lastLog = readAndValid.getLogs().getLastLog();
      assertEquals(1, lastLog.getRecords().size());
      CtuluLogRecord record = lastLog.getRecords().get(0);
      assertEquals(CtuluLogLevel.WARNING, record.getLevel());
      assertEquals("testSelector.readTest.unknown", record.getMsg());
      assertEquals(1, record.getArgs().length);
      assertEquals("DCLM4", record.getArgs()[0]);
    } catch (IOException iOException) {
      fail(iOException.getMessage());
    } finally {
      CtuluLibFile.deleteDir(siteDir);
    }
  }

  @Test
  public void testReadAndValid_File_ComparaisonSelectorEnum() {
    ComparaisonSelectorReader reader = new ComparaisonSelectorReader(knownIds);
    //on extrait les fichiers
    File siteDir = null;
    try {
      siteDir = extractDir(reader);
      CrueOperationResult<ComparaisonSelectorContent> readAndValid = reader.readAndValid(siteDir, ComparaisonSelectorEnum.C9_C9);
      ComparaisonSelectorContent result = readAndValid.getResult();
      testPredicate(result.getPredicate(ComparaisonSelectorEnum.C9_C9), "DCLM1", "DCLM2");
      assertEquals(PredicateUtils.truePredicate(), result.getPredicate(ComparaisonSelectorEnum.C9_C10));
      assertEquals(PredicateUtils.truePredicate(), result.getPredicate(ComparaisonSelectorEnum.C10_C10));
      assertFalse(readAndValid.getLogs().containsSomething());
    } catch (IOException iOException) {
      fail(iOException.getMessage());
    } finally {
      CtuluLibFile.deleteDir(siteDir);
    }
  }

  private File extractDir(ComparaisonSelectorReader reader) throws IOException {
    File siteDir = CtuluLibFile.createTempDir();
    File comparaisonDir = new File(siteDir, "comparaisons");
    assertTrue(comparaisonDir.mkdirs());
    File file = CtuluLibFile.getFileFromJar("/comparaisons/c9c9.xml", new File(siteDir, reader.getFiles().get(ComparaisonSelectorEnum.C9_C9)));
    assertTrue(file.exists());
    file = CtuluLibFile.getFileFromJar("/comparaisons/c9c10.xml", new File(siteDir, reader.getFiles().get(ComparaisonSelectorEnum.C9_C10)));
    assertTrue(file.exists());
    file = CtuluLibFile.getFileFromJar("/comparaisons/c10c10.xml", new File(siteDir, reader.getFiles().get(ComparaisonSelectorEnum.C10_C10)));
    assertTrue(file.exists());
    return siteDir;
  }
}
