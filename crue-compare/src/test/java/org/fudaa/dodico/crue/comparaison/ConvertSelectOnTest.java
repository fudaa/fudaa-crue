/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.PropertyUtilsCache;
import org.fudaa.dodico.crue.comparaison.config.ConfSelection;
import org.fudaa.dodico.crue.comparaison.config.ConfSelectionItemMerge;
import org.fudaa.dodico.crue.comparaison.config.ConfSelectionItemRequete;
import org.fudaa.dodico.crue.comparaison.tester.EqualsTesterIsIncludeCollection;
import org.fudaa.dodico.crue.comparaison.tester.ResultatTest;
import org.fudaa.dodico.crue.comparaison.tester.TesterContext;
import org.fudaa.dodico.crue.comparaison.tester.TesterContextImpl;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Test sur la selection
 *
 * @author deniger
 */
public class ConvertSelectOnTest {
  @Test
  public void testSelectionSimple() {
    final ConfSelection sel = new ConfSelection();
    final ConfSelectionItemMerge merge = createMergeBrancheRequete();
    sel.getItems().add(merge);
    final EMHSousModele m1 = createSousModele("br1", "br2");
    final EMHSousModele m2 = createSousModele("br2", "br1");
    final ConvertSelectOn builder = new ConvertSelectOn();
    builder.setA(m1);
    builder.setB(m2);
    builder.setSelect(sel);
    try {
      final List<ComparaisonNodeFinal> res = builder.doSelection();
      Assert.assertEquals(1, res.size());
      final ComparaisonNodeFinal mp = res.get(0);
      Assert.assertEquals(m1, mp.objetA);
      Assert.assertEquals(m2, mp.objetB);
      //m1 a 2 branches
      Assert.assertEquals(2, mp.listObjectA.size());
      //m2 a 2 branches
      Assert.assertEquals(2, mp.listObjectB.size());
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }
  }

  @Test
  public void testSelectionMulti() {

    final ConfSelection sel = new ConfSelection();
    sel.getItems().add(createMergeBrancheRequete());
    sel.getItems().add(getSelectSelectionRequete());
    final EMHSousModele m1 = createModeleWithRelation("titi", "tata");
    final EMHSousModele m2 = createModeleWithRelation("tata", "titi");
    final ConvertSelectOn builder = new ConvertSelectOn();
    builder.setA(m1);
    builder.setB(m2);
    builder.setSelect(sel);
    try {
      final List<ComparaisonNodeFinal> res = builder.doSelection();
      Assert.assertEquals(2, res.size());
      final ComparaisonNodeFinal mp = res.get(0);
      Assert.assertTrue(mp.objetA instanceof EMHBrancheOrifice);
      Assert.assertTrue(mp.objetB instanceof EMHBrancheOrifice);
      Assert.assertTrue(mp.parent.objetA instanceof EMHSousModele);
      Assert.assertTrue(mp.parent.objetB instanceof EMHSousModele);
      final List objectA = mp.listObjectA;
      Assert.assertEquals(3, objectA.size());
      int firstIdx = 1;
      if ("titi".equals(((EMHBrancheOrifice) mp.objetA).getNom())) {
      } else if ("tata".equals(((EMHBrancheOrifice) mp.objetA).getNom())) {
        firstIdx = 4;
      } else {
        Assert.fail("nom inconnu " + ((EMHBrancheOrifice) mp.objetA).getNom());
      }
      for (final Object object : objectA) {
        Assert.assertEquals("SECT" + (firstIdx++), PropertyUtilsCache.getInstance().getProperty(object, mp.printForFils));
      }

      Assert.assertEquals(3, mp.listObjectB.size());
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }
  }

  @Test
  public void testComparaisonIn() {
    final EMHSousModele sm1 = createSousModele("1", "2");
    final EMHSousModele sm2 = createSousModele("1", "2");
    EqualsTesterIsIncludeCollection include = new EqualsTesterIsIncludeCollection("nom", true);
    final TesterContext contexte = new TesterContextImpl(null);
    try {
      ResultatTest compRes = new ResultatTest();
      Assert.assertTrue(include.isSame(sm1.getBranches(), sm2.getBranches(), compRes, contexte));
      addBranche("3", "4", sm2);
      compRes = new ResultatTest();
      Assert.assertFalse(include.isSame(sm1.getBranches(), sm2.getBranches(), compRes, contexte));
      Assert.assertEquals(1, compRes.getFils().size());
      final ResultatTest comparaisonResult = compRes.getFils().get(0);
      Assert.assertEquals(BusinessMessages.getString("diff.include.dansB.pasDansA"), comparaisonResult.getMsg());
      compRes = new ResultatTest();
      include = new EqualsTesterIsIncludeCollection("nom", false);
      Assert.assertTrue(include.isSame(sm1.getBranches(), sm2.getBranches(), compRes, contexte));
    } catch (final Exception e) {
      e.printStackTrace();
      Assert.fail(e.getMessage());
    }
  }

  public static ConfSelectionItemRequete getSelectSelectionRequete() {
    // pour chaque branche on compare les section
    final ConfSelectionItemRequete requete = new ConfSelectionItemRequete();
    requete.setParamToPrint("emh.nom");
    requete.setRequest("listeSections");
    return requete;
  }

  public static ConfSelectionItemMerge createMergeBrancheRequete() {
    // une requete qui recupere les branches
    final ConfSelectionItemRequete re = new ConfSelectionItemRequete();
    re.setRequest("branches");
    // on les merges selon leur nom
    final ConfSelectionItemMerge merge = new ConfSelectionItemMerge();
    merge.setRequest(re);
    merge.setAttribut("nom");
    return merge;
  }

  private static EMHSousModele createSousModele(final String n1, final String n2) {
    final EMHSousModele sm1 = new EMHSousModele();
    addBranche(n1, n2, sm1);
    return sm1;
  }

  private static void addBranche(final String n1, final String n2, final EMHSousModele sm1) {
    final List<EMH> emhs = new ArrayList<>();
    final EMHBrancheOrifice b1 = new EMHBrancheOrifice(n1);
    emhs.add(b1);
    final EMHBrancheOrifice b2 = new EMHBrancheOrifice(n2);
    emhs.add(b2);
    EMHRelationFactory.addRelationsContientEMH(sm1, emhs);
  }

  public static EMHSousModele createModeleWithRelation(final String n1, final String n2) {
    final EMHSousModele sm1 = new EMHSousModele();
    final List<EMH> emhs = new ArrayList<>();

    final EMHBrancheOrifice b1 = new EMHBrancheOrifice(n1);
    b1.addRelationEMH(createRelationSectionDansBranche("SECT1"));
    b1.addRelationEMH(createRelationSectionDansBranche("SECT2"));
    b1.addRelationEMH(createRelationSectionDansBranche("SECT3"));
    emhs.add(b1);
    final EMHBrancheOrifice b2 = new EMHBrancheOrifice(n2);
    b2.addRelationEMH(createRelationSectionDansBranche("SECT4"));
    b2.addRelationEMH(createRelationSectionDansBranche("SECT5"));
    b2.addRelationEMH(createRelationSectionDansBranche("SECT6"));
    emhs.add(b2);
    EMHRelationFactory.addRelationsContientEMH(sm1, emhs);
    return sm1;
  }

  private static RelationEMHSectionDansBranche createRelationSectionDansBranche(final String nom) {
    final RelationEMHSectionDansBranche sect1 = new RelationEMHSectionDansBranche();
    final EMHSectionInterpolee s1 = new EMHSectionInterpolee(nom);
    sect1.setEmh(s1);
    return sect1;
  }
}
