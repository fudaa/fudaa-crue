/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.comparaison.tester.*;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.emh.ParamNumCalcPseudoPerm;
import org.fudaa.dodico.crue.metier.emh.ParamNumModeleBase;
import org.joda.time.LocalDateTime;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.fudaa.dodico.crue.test.AbstractTestParent.assertDoubleEquals;

/**
 * Test unitaire sur le chargeur des valeurs de comparaison
 *
 * @author deniger
 */
public class ComparaisonFactoryTest {
  /**
   * Test le chargement des valeurs par defaut
   */
  @SuppressWarnings("unchecked")
  @Test
  public void testFactory() {
    final FactoryEqualsTester factory = buildFactory();
    final CtuluLog log = new CtuluLog();
    EqualsTester tester = factory.buildTesterFor(null, String.class, null, log);
    Assert.assertNotNull(tester);
    // test pour l'objet simple
    try {
      Assert.assertEquals(EqualsTesterString.class, tester.getClass());
      Assert.assertTrue(tester.isSame("1", "1", null, new TesterContextImpl(null)));
      tester = factory.buildTesterFor(null, Double.class, "coefD", log);
      Assert.assertEquals(EqualsTesterDouble.class, tester.getClass());
      // pour les double
      assertDoubleEquals(1e-5, ((EqualsTesterDouble) tester).getEps().getEpsilonComparaison());
      Assert.assertTrue(tester.isSame(Double.valueOf(1.0), Double.valueOf(1.00000000001), null, new TesterContextImpl(null)));
      Assert.assertFalse(tester.isSame(Double.valueOf(1.0), Double.valueOf(1.01), null, new TesterContextImpl(null)));
      // float
      tester = factory.buildTesterFor(null, Float.class, "coefD", log);
      Assert.assertEquals(EqualsTesterDouble.class, tester.getClass());
      assertDoubleEquals(1e-5, ((EqualsTesterDouble) tester).getEps().getEpsilonComparaison());
      Assert.assertTrue(tester.isSame(Double.valueOf(1.0), Double.valueOf(1.00000000001), null, new TesterContextImpl(null)));
      // date
      tester = factory.buildTesterFor(null, LocalDateTime.class, "dateDebSce", log);
      Assert.assertNotNull(tester);
      Assert.assertEquals(EqualsTesterDate.class, tester.getClass());
      final LocalDateTime d1 = new LocalDateTime(2009, 5, 5, 5, 5, 0, 4);
      LocalDateTime d2 = new LocalDateTime(2009, 5, 5, 5, 5, 0);
      Assert.assertTrue(tester.isSame(d1, d2, null, new TesterContextImpl(null)));
      d2 = new LocalDateTime(2009, 5, 5, 5, 5, 3);
      Assert.assertFalse(tester.isSame(d1, d2, null, new TesterContextImpl(null)));

      // enum
      tester = factory.buildTesterFor(null, EnumEMH.class, null, log);
      Assert.assertTrue(tester.isSame(EnumEMH.D_Z, EnumEMH.D_Z, null, new TesterContextImpl(null)));
      Assert.assertFalse(tester.isSame(EnumEMH.D_Z, EnumEMH.OUV, null, new TesterContextImpl(null)));

      tester = factory.buildTesterFor(null, List.class, "enum", log);
      Assert.assertEquals(EqualsTesterCollection.class, tester.getClass());
      final Collection coll1 = Arrays.asList(EnumEMH.D_Z, EnumEMH.OUV);
      final Collection coll2 = Arrays.asList(EnumEMH.D_Z, EnumEMH.OUV);

      final ResultatTest resu = new ResultatTest();
      Assert.assertTrue(tester.isSame(coll1, coll2, resu, new TesterContextImpl(null)));
      Assert.assertFalse(tester.isSame(coll1, Collections.singletonList(EnumEMH.D_Z), resu, new TesterContextImpl(null)));
      Assert.assertEquals(1, resu.getFils().size());
      Assert.assertFalse(tester.isSame(coll1, Arrays.asList(EnumEMH.D_Z, null), resu, new TesterContextImpl(null)));
      Assert.assertEquals(2, resu.getFils().size());
      final double[] array1 = new double[23];
      double[] array2 = new double[22];
      tester = factory.buildTesterFor(null, double[].class, "coefD", log);
      Assert.assertEquals(EqualsTesterArrayDouble.class, tester.getClass());
      Assert.assertFalse(tester.isSame(array1, array2, null, new TesterContextImpl(null)));
      array2 = new double[23];
      Assert.assertTrue(tester.isSame(array1, array2, null, new TesterContextImpl(null)));
      array2[22] = 0.5;
      Assert.assertFalse(tester.isSame(array1, array2, null, new TesterContextImpl(null)));
      array2[22] = 0.000001;
      Assert.assertTrue(tester.isSame(array1, array2, null, new TesterContextImpl(null)));
    } catch (final Exception e) {
      e.printStackTrace();
      Assert.fail(e.getMessage());
    }
  }

  private FactoryEqualsTester buildFactory() {
    return new FactoryEqualsTester(CrueConfigMetierForTest.DEFAULT, -1, -1);
  }

  @Test
  public void testCompareBean() {
    final FactoryEqualsTester factory = buildFactory();
    final ParamNumModeleBase pnum1 = new ParamNumModeleBase(CrueConfigMetierForTest.DEFAULT);
    pnum1.setParamNumCalcPseudoPerm(null);
    pnum1.setFrLinInf(0.1);
    final ParamNumModeleBase pnum2 = new ParamNumModeleBase(CrueConfigMetierForTest.DEFAULT);
    pnum2.setParamNumCalcPseudoPerm(new ParamNumCalcPseudoPerm(CrueConfigMetierForTest.DEFAULT));
    pnum2.setFrLinInf(0.1);
    final EqualsTesterBean beanTester = new EqualsTesterBean(factory);
    try {
      Assert.assertFalse(beanTester.isSame(pnum1, pnum2, null, new TesterContextImpl(null)));
      // on compare 2 niveau : dans ce cas cela doit renvoye false: car pnum2 a un ParamNumCalcPseudoPerm
      TesterContext ctx2 = new TesterContextImpl(2, null);
      Assert.assertFalse(beanTester.isSame(pnum1, pnum2, null, ctx2));
      // on met pnum1 dans le même cadre:
      pnum1.setParamNumCalcPseudoPerm(new ParamNumCalcPseudoPerm(CrueConfigMetierForTest.DEFAULT));
      ctx2 = new TesterContextImpl(2, null);
      // et tout rentre dans l'ordre
      Assert.assertTrue(beanTester.isSame(pnum1, pnum2, null, ctx2));
      pnum2.setFrLinInf(0.10001);
      Assert.assertTrue(beanTester.isSame(pnum1, pnum2, null, new TesterContextImpl(null)));
      pnum2.setFrLinInf(0.2);
      Assert.assertFalse(beanTester.isSame(pnum1, pnum2, null, new TesterContextImpl(null)));
      Assert.assertTrue(beanTester.isSame(pnum1, pnum2, null, new TesterContextImpl(null), Collections.singletonList("frLinSup")));
      pnum2.setFrLinSup(0.000001);
      Assert.assertFalse(beanTester.isSame(pnum1, pnum2, null, new TesterContextImpl(null)));
      pnum2.setFrLinSup(0.1);
      Assert.assertFalse(beanTester.isSame(pnum1, pnum2, null, new TesterContextImpl(null)));
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }
}
