/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.fudaa.ctulu.CsvWriter;
import org.fudaa.dodico.crue.comparaison.config.ConfComparaison;
import org.fudaa.dodico.crue.comparaison.config.ConfComparaisonConteneur;
import org.fudaa.dodico.crue.comparaison.io.ReaderConfig;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Utiliser pour écrire la liste des conparaison dans un fichier CSV.
 * 
 * @author Frederic Deniger
 */
public class ComparaisonWriteToCsv {
  
  public static void main(String[] args) {
    ConfComparaisonConteneur read = new ReaderConfig().read(ReaderConfig.class.getResource("default-comparaison.xml"));
    File out = new File("C:\\data\\Fudaa-Crue\\comparaison.csv");
    try {
      CsvWriter writer = new CsvWriter(out);
      writer.setSepChar('\t');
      List<ConfComparaison> comparaisons = read.getComparaisons();
      boolean first = true;
      for (ConfComparaison confComparaison : comparaisons) {
        if (first) {
          first = false;
        } else {
          writer.newLine();
        }
        writer.appendString(confComparaison.getId());
        writer.appendString(confComparaison.getNomi18n());
      }
      writer.close();
    } catch (IOException iOException) {
    }
    
  }
}
