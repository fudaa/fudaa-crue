/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.fudaa.dodico.crue.common.PropertyUtilsCache;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPermNoeudQapp;
import org.junit.Assert;
import org.junit.Test;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Fred Deniger
 */
public class PropertyUtilsCacheTest {
  /**
   * Test of isActive method, of class PropertyUtilsCache.
   */
  @Test
  public void testIsActive() {
    final PropertyUtilsCache instance = new PropertyUtilsCache();
    final boolean result = instance.isActive();
    Assert.assertFalse(result);
  }

  /**
   * Test of setActive method, of class PropertyUtilsCache.
   */
  @Test
  public void testSetActive() {
    final PropertyUtilsCache instance = new PropertyUtilsCache();
    instance.setActive(true);
    Assert.assertTrue(instance.isActive());
  }

  /**
   * Test of getValue method, of class PropertyUtilsCache.
   */
  @Test
  public void testGetValueWithoutActive() throws Exception {

    final PropertyUtilsCache instance = new PropertyUtilsCache();
    testGetValues(instance);
  }

  private void testGetValues(final PropertyUtilsCache instance) throws InvocationTargetException, IllegalAccessException, IntrospectionException, NoSuchMethodException {
    final List test = new ArrayList();
    test.add("test");
    final Object result = instance.getProperty(test, "size");
    Assert.assertEquals(Integer.valueOf(1), result);

    final CalcPseudoPermNoeudQapp item = new CalcPseudoPermNoeudQapp(CrueConfigMetierForTest.DEFAULT);
    item.setQapp(10.3);
    final Object value = instance.getProperty(item, "qapp");
    Assert.assertEquals(Double.valueOf(10.3).doubleValue(), ((Double) value).doubleValue(), 1e-10);
  }

  @Test
  public void testGetValueWithActive() throws Exception {
    final PropertyUtilsCache instance = new PropertyUtilsCache();
    instance.setActive(true);
    testGetValues(instance);
  }
}
