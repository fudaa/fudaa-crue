/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.io;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author deniger
 */
public class TestReaderSelector {
  @Test
  public void testRead() {
    final ReaderSelector readerConfig = new ReaderSelector();
    final SelectorItemContainer read = readerConfig.read(TestReaderSelector.class.getResource("/comparaisons/c9c9.xml"));
    Assert.assertNotNull(read);
    Assert.assertEquals(2, read.getItems().size());
    Assert.assertEquals("DCLM1", read.getItems().get(0).getTestId());
    Assert.assertEquals("DCLM2", read.getItems().get(1).getTestId());
  }
}
