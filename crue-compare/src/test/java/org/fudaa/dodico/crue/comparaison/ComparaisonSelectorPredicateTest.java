/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.fudaa.dodico.crue.comparaison.config.ConfComparaison;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 *
 * @author Frederic Deniger
 */
public class ComparaisonSelectorPredicateTest {
  
  public ComparaisonSelectorPredicateTest() {
  }
  
  @Test
  public void testSomeMethod() {
    HashSet<String> rejected = new HashSet<>();
    rejected.add("DCLM1");
    ComparaisonSelectorPredicate predicate = new ComparaisonSelectorPredicate(rejected);
    assertFalse(predicate.evaluate(null));
    ConfComparaison cmp=new ConfComparaison();
    cmp.setId("DCLM1");
    assertFalse(predicate.evaluate(cmp));
    cmp.setId("DCLM2");
    assertTrue(predicate.evaluate(cmp));
  }
}
