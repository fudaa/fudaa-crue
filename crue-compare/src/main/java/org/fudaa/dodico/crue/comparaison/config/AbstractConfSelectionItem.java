/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.config;

import org.apache.commons.lang3.StringUtils;

public class AbstractConfSelectionItem implements ConfSelectionItem {

  private String description;

  /**
   * @return the description
   */
  @Override
  public final String getDescription() {
    return description;
  }

  /**
   * @param description the description to set
   */
  public final void setDescription(String description) {
    this.description = StringUtils.trim(description);
  }

}
