/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.fudaa.dodico.crue.common.CustomPropertyUtilsBeanInstaller;
import org.fudaa.dodico.crue.common.PropertyUtilsCache;

/**
 * @author deniger
 */
public class EqualsTesterBean extends AbstractEqualsTesterBean<Object> {

  static {
    CustomPropertyUtilsBeanInstaller.install();
  }

  /**
   * @param factory la factory
   */
  public EqualsTesterBean(final FactoryEqualsTester factory) {
    super(factory,true);
  }

  @Override
  protected Object getPropertyValue(final Object o1, final String name) throws Exception {
    return PropertyUtilsCache.getInstance().getProperty(o1, name);
  }
}
