/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.fudaa.dodico.crue.metier.helper.DynamicPropertyFilter;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author deniger
 */
public class TesterContextFactory {

  private final Set<TestOption> activatedOption = new HashSet<>();
  private boolean isMaxDeepSet;
  private boolean trimString;
  private boolean ignoreCase;
  private int maxDeep;
  private DynamicPropertyFilter resFilter;
  private ToStringTransformer toStringTransfomer;

  public TesterContext create() {
    TesterContextImpl testerContextImpl = isMaxDeepSet ? new TesterContextImpl(maxDeep, resFilter, activatedOption,
            toStringTransfomer) : new TesterContextImpl(resFilter, toStringTransfomer);
    testerContextImpl.setIgnoreCaseForStringComparison(ignoreCase);
    testerContextImpl.setTrimStringForComparison(trimString);
    if (this.propsToCompare != null) {
      testerContextImpl.setPropToCompare(new HashSet<>(propsToCompare));
    }
    if (this.propsToIgnore != null) {
      testerContextImpl.setPropToIgnore(new HashSet<>(propsToIgnore));
    }
    return testerContextImpl;
  }

  /**
   * @return the maxDeep
   */
  public int getMaxDeep() {
    return maxDeep;
  }


  /**
   * @return the ignoreComment
   */
  public boolean isIgnoreComment() {
    return activatedOption.contains(TestOption.IGNORE_COMMENT);
  }

  public boolean isActivated(TestOption option) {
    return activatedOption.contains(option);
  }

  /**
   * @param ignoreComment the ignoreComment to set
   */
  public void setIgnoreComment(boolean ignoreComment) {
    if (ignoreComment) {
      this.activatedOption.add(TestOption.IGNORE_COMMENT);
    } else {
      this.activatedOption.remove(TestOption.IGNORE_COMMENT);
    }
  }

  public void activeOption(TestOption testOption) {
    activatedOption.add(testOption);
  }


  /**
   * @param maxDeep the maxDeep to set
   */
  public void setMaxDeep(final int maxDeep) {
    this.maxDeep = maxDeep;
    isMaxDeepSet = true;
  }

  /**
   * @param resFilter the resFilter to set
   */
  public void setResFilter(DynamicPropertyFilter resFilter) {
    this.resFilter = resFilter;
  }


  /**
   * @param toStringTransfomer the toStringTransfomer to set
   */
  public void setToStringTransfomer(ToStringTransformer toStringTransfomer) {
    this.toStringTransfomer = toStringTransfomer;
  }


  /**
   * @param trimString the trimString to set
   */
  public void setTrimString(boolean trimString) {
    this.trimString = trimString;
  }


  /**
   * @param ignoreCase the ignoreCase to set
   */
  public void setIgnoreCase(boolean ignoreCase) {
    this.ignoreCase = ignoreCase;
  }
  private List<String> propsToCompare;
  private List<String> propsToIgnore;

  public void setPropertiesToCompare(List<String> attributs) {
    propsToCompare = attributs;

  }

  public void setPropertiesToIgnore(List<String> attributsToIgnore) {
    propsToIgnore = attributsToIgnore;

  }
}
