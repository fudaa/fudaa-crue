/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import java.util.ArrayList;
import java.util.List;

public class ComparaisonGroup extends ComparaisonNode {

  final List<ComparaisonNode> fils = new ArrayList<>();

  @Override
  public List<ComparaisonNode> getFils() {
    return fils;
  }

  @Override
  public boolean isFeuille() {
    return false;
  }

}
