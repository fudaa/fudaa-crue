/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.config;

import org.apache.commons.lang3.StringUtils;

public class ConfCompareListe extends AbstractConfCompare {
  private String attribut;
  //si non null doit contenir le prefix pour l'id du message contenant la traduction.
  private String diffTranslation;
  private boolean bidirect;

  /**
   * @return the attribut
   */
  public String getAttribut() {
    return attribut;
  }

  @SuppressWarnings("unused")
  public String getDiffTranslation() {
    return diffTranslation;
  }

  /**
   * utilise par Xstream (Xml)
   *
   * @param diffTranslation
   */
  @SuppressWarnings("unused")
  public void setDiffTranslation(final String diffTranslation) {
    this.diffTranslation = diffTranslation;
  }

  /**
   * @param attribut the attribut to set
   */
  public void setAttribut(final String attribut) {
    this.attribut = StringUtils.trim(attribut);
  }

  /**
   * @return the bidirect
   */
  public boolean isBidirect() {
    return bidirect;
  }

  /**
   * @param bidirect the bidirect to set
   */
  public void setBidirect(final boolean bidirect) {
    this.bidirect = bidirect;
  }
}
