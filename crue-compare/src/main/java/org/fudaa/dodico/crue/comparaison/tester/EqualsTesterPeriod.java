/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.joda.time.Period;

/**
 * Comparateur de duration.
 * 
 * @author deniger
 */
public class EqualsTesterPeriod extends AbstractTemplateEqualsTesterDouble<Period> {

  @Override
  public boolean isSameSafe(final Period o1, final Period o2, ResultatTest res, TesterContext context) {
    return isSameDouble(o1.toStandardDuration().getMillis() / 1000d, o2.toStandardDuration().getMillis() / 1000d);
  }

  @Override
  protected boolean mustTestAlreadyDone() {
    return false;
  }

}
