/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.fudaa.dodico.crue.comparaison.config.ConfSelection;
import org.fudaa.dodico.crue.comparaison.config.ConfSelectionItem;
import org.fudaa.dodico.crue.comparaison.config.ConfSelectionItemMerge;

import java.util.ArrayList;
import java.util.List;

public class ConvertSelectOn {

  private Object a;
  private Object b;

  private ConfSelection select;

  public Object getA() {
    return a;
  }

  public void setA(final Object a) {
    this.a = a;
  }

  public Object getB() {
    return b;
  }

  public void setB(final Object b) {
    this.b = b;
  }

  public ConfSelection getSelect() {
    return select;
  }

  public void setSelect(final ConfSelection select) {
    this.select = select;
  }

  List<ComparaisonNodeFinal> doSelection() throws Exception {
    List<ComparaisonNodeFinal> init = new ArrayList<>();
    final ComparaisonNodeFinal node = new ComparaisonNodeFinal(true);
    init.add(node);
    node.listObjectA = new ArrayList();
    node.listObjectA.add(a);
    node.objetA = a;
    node.listObjectB = new ArrayList();
    node.listObjectB.add(b);
    node.objetB = b;
    final List<ConfSelectionItem> selects = select.getItems();
    final ConvertSelectOnItemRequest request = new ConvertSelectOnItemRequest();
    final ConvertSelectOnItemMerge merge = new ConvertSelectOnItemMerge();
    for (final ConfSelectionItem selectionItem : selects) {
      ConvertSelectOnItem doer;
      if (ConfSelectionItemMerge.class.equals(selectionItem.getClass())) {
        doer = merge;
      } else {
        doer = request;
      }
      init = doer.compute(init, selectionItem);

    }
    return init;
  }

}
