/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.apache.commons.collections4.Predicate;
import org.fudaa.dodico.crue.comparaison.config.ConfComparaison;

import java.util.Collections;
import java.util.Set;

/**
 *
 * @author Frederic Deniger
 */
public class ComparaisonSelectorPredicate implements Predicate {

  private final Set<String> rejectedIds;

  public ComparaisonSelectorPredicate(final Set<String> rejectedIds) {
    this.rejectedIds = rejectedIds == null ? Collections.emptySet() : rejectedIds;
  }

  Set<String> getRejectedIds() {
    return Collections.unmodifiableSet(rejectedIds);
  }

  @Override
  public boolean evaluate(final Object object) {
    if (object == null) {
      return false;
    }
    final ConfComparaison comp = (ConfComparaison) object;
    return !rejectedIds.contains(comp.getId());
  }
}
