/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.comparaison.tester.*;
import org.fudaa.dodico.crue.metier.emh.RelationEMH;

import java.util.List;

public class ExecuteCompareActionOnObject implements CompareActionBuilder {
    final EqualsTester tester;
    private final FactoryEqualsTester factory;

    /**
     * @param tester  le tester
     * @param factory la factory
     */
    public ExecuteCompareActionOnObject(final EqualsTester tester, final FactoryEqualsTester factory) {
        super();
        this.tester = tester;
        this.factory = factory;
    }

    @Override
    public boolean isDiffOnOrder() {
        return false;
    }

    public EqualsTester getTester() {
        return tester;
    }

    private boolean needResultatTestFils(final Object a, final Object b) {
        return a instanceof RelationEMH || b instanceof RelationEMH;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean launch(final ComparaisonNodeFinal target, final ResultatTest parent, final CtuluLog ctuluLog) {
        parent.setNbObjectTested(0);
        final List a = target.listObjectA;
        final List b = target.listObjectB;
        if (a == b) {
            return true;
        }
        if (a == null) {
            parent.addDiff(new ResultatTest(a, b, "list.a.isNull"));
            return false;
        }
        if (b == null) {
            parent.addDiff(new ResultatTest(a, b, "list.b.isNull"));
            return false;
        }
        final int size = a.size();
        if (size != b.size()) {
            parent.addDiff(new ResultatTest(size, b.size(), EqualsTesterCollection.TAILLE_DIFF));
            return false;
        }
        parent.setNbObjectTested(size);
        boolean res = true;
        try {
            for (int i = 0; i < size; i++) {
                final TesterContext create = tester.getContextFactory().create();
                create.setError(ctuluLog);
                EqualsTester toUse = tester;
                final Object ai = a.get(i);
                EqualsTester other = factory.getMainKnownTester(ai);
                final Object bi = b.get(i);
                if (other == null) {
                    other = factory.getMainKnownTester(bi);
                }
                if (other != null) {
                    toUse = other;
                }
                ResultatTest resTest = parent;
                // a voir si intelligent: ne faudrait il pas etendre ce cas.
                // sans cela, le test DRSO6 n'affiche pas les relation dans les résultats de comparaisons.
                final boolean needResultatTestFils = needResultatTestFils(ai, bi);
                if (needResultatTestFils) {
                    resTest = createResultatTest(ai, bi);
                    parent.addGroup(resTest);
                }
                final boolean same = toUse.isSame(ai, bi, resTest, create);
                if (!same && needResultatTestFils) {
                    if (CollectionUtils.isNotEmpty(resTest.getFils()) && resTest.getFils().size() == 1) {
                        final ResultatTest filsResultat = resTest.getFils().get(0);
                        if (filsResultat.getObjetA() == ai && filsResultat.getObjetB() == bi) {
                            resTest.replaceDiffFilsOf(filsResultat);
                        }
                    }
                    resTest.setSame(false);
                }
                res = res && same;
            }
        } catch (final Exception e) {
            ctuluLog.manageException(e);
        }
        return res;
    }

    private ResultatTest createResultatTest(final Object ai, final Object bi) {
        ResultatTest resTest;
        boolean useOverviewPrint = false;
        String txt = BusinessMessages.getString("relation.node");
        if (ai == null) {
            useOverviewPrint = true;
            txt = bi.getClass().getSimpleName();
        } else if (bi == null) {
            useOverviewPrint = true;
            txt = ai.getClass().getSimpleName();
        } else if (ai.getClass().equals(bi.getClass())) {
            useOverviewPrint = true;
            txt = ai.getClass().getSimpleName();
        }
        resTest = new ResultatTest(ai, bi, txt);
        if (useOverviewPrint) {
            if (ai != null) {
                resTest.setPrintA(((RelationEMH) ai).getEmh().getNom());
            }
            if (bi != null) {
                resTest.setPrintB(((RelationEMH) bi).getEmh().getNom());
            }
        }
        return resTest;
    }
}
