/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.fudaa.dodico.crue.comparaison.config.ConfSelectionItem;

import java.util.List;

public interface ConvertSelectOnItem {

  List<ComparaisonNodeFinal> compute(final List<ComparaisonNodeFinal> in, final ConfSelectionItem requete) throws Exception;

}
