/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.io;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueXmlReaderWriterImpl;
import org.fudaa.dodico.crue.comparaison.config.*;

import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReaderConfig {

  private final static Logger LOGGER = Logger.getLogger(ReaderConfig.class.getName());
  private CtuluLog analyze;

  public CtuluLog getAnalyze() {
    return analyze;
  }

  public ConfComparaisonConteneur read(final URL file) {
    analyze = new CtuluLog();
    analyze.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    analyze.setDesc("read.comparaison.file");
    if (file == null) {
      analyze.addSevereError("file.read.null");
      return null;
    }
    analyze.setDescriptionArgs(file.getFile());

    InputStream in = null;
    ConfComparaisonConteneur res = null;
    try {
      in = file.openStream();
      final XStream xstream = createXstream();
      final Object fromXML = xstream.fromXML(in);
      if (fromXML instanceof ConfComparaisonConteneur) {
        res = (ConfComparaisonConteneur) fromXML;
      } else {
        analyze.addSevereError("file.read.badContent.error", file);
      }
    } catch (final Exception e) {
      analyze.addSevereError("file.read.error", file, e.getMessage());
      LOGGER.log(Level.SEVERE, "read " + file, e);
    } finally {
      CtuluLibFile.close(in);
    }
    return res;

  }

  protected XStream createXstream() {
    final XStream xstream = new XStream(new DomDriver());
    CrueXmlReaderWriterImpl.initXstreamSecurity(xstream);
    xstream.alias("Comparaisons", ConfComparaisonConteneur.class);

    configureComparaison(xstream);
    configureSelection(xstream);
    configureOption(xstream);
    xstream.registerConverter(new StringConverter());
    return xstream;
  }

  private void configureSelection(final XStream xstream) {

    xstream.addImplicitCollection(ConfSelection.class, "items");
    xstream.alias("Requete", ConfSelectionItemRequete.class);
    xstream.alias("Merge", ConfSelectionItemMerge.class);
    xstream.aliasAttribute(ConfSelectionItemMerge.class, "attribut", "Attribut");
    xstream.aliasAttribute(ConfSelectionItemMerge.class, "errorIfDifferentSize", "ErrorIfDifferentSize");
    xstream.aliasField("Requete", ConfSelectionItemMerge.class, "request");
    xstream.aliasAttribute(AbstractConfSelectionItem.class, "description", "Description");
    xstream.registerConverter(createSingleValueConverterRequete());
  }

  private void configureComparaison(final XStream xstream) {
    xstream.alias("Comparaison", ConfComparaison.class);
    xstream.aliasField("Id", ConfComparaison.class, "id");
    xstream.aliasField("Nom", ConfComparaison.class, "nom");

    xstream.addImplicitCollection(ConfComparaisonConteneur.class, "comparaisons");
    xstream.addImplicitCollection(ConfComparaison.class, "compare");
    xstream.alias("Compare-Liste", ConfCompareListe.class);
    xstream.aliasAttribute(ConfCompareListe.class, "attribut", "Attribut");
    xstream.aliasAttribute(ConfCompareListe.class, "bidirect", "Bidirect");
    xstream.aliasAttribute(ConfCompareListe.class, "diffTranslation", "DiffTranslation");
    xstream.alias("Compare-Liste-Order", ConfCompareListeOrder.class);
    xstream.aliasAttribute(ConfCompareListeOrder.class, "attribut", "Attribut");
    xstream.aliasAttribute(ConfCompareListeOrder.class, "onlyIfSameList", "OnlyIfSameList");
    xstream.alias("Compare-Objet", ConfCompareObject.class);
    xstream.aliasAttribute(ConfCompareObject.class, "deep", "Deep");
    xstream.aliasAttribute(ConfCompareObject.class, "trimString", "TrimString");
    xstream.aliasAttribute(ConfCompareObject.class, "ignoreCase", "IgnoreCase");
    xstream.aliasField("Selection", ConfComparaison.class, "selection");
    xstream.alias("Compare-Specific", ConfCompareSpecific.class);
    xstream.addImplicitCollection(ConfCompareObject.class, "options", ConfOption.class);
    xstream.addImplicitCollection(ConfCompareObject.class, "attributs", "Attribut", String.class);
    xstream.addImplicitCollection(ConfCompareObject.class, "attributsToIgnore", "AttributToIgnore", String.class);
  }

  private static class StringConverter extends AbstractSingleValueConverter {

    @Override
    public boolean canConvert(Class arg0) {
      return String.class.equals(arg0);
    }

    @Override
    public Object fromString(String arg0) {
      return StringUtils.trim(arg0);
    }
  }

  private Converter createSingleValueConverterRequete() {
    return new Converter() {
      @Override
      public boolean canConvert(final Class arg0) {
        return ConfSelectionItemRequete.class.equals(arg0);
      }

      @Override
      public Object unmarshal(final HierarchicalStreamReader reader, final UnmarshallingContext arg1) {
        ConfSelectionItemRequete item = new ConfSelectionItemRequete();
        item.setParamToPrint(reader.getAttribute("ParamToPrint"));
        item.setDescription(reader.getAttribute("Description"));
        item.setRequest(reader.getValue());
        return item;
      }

      @Override
      public void marshal(final Object arg0, final HierarchicalStreamWriter writer, final MarshallingContext arg2) {
        final ConfSelectionItemRequete it = (ConfSelectionItemRequete) arg0;
        writer.startNode("Requete");
        if (StringUtils.isNotBlank(it.getParamToPrint())) {
          writer.addAttribute("ParamToPrint", it.getParamToPrint());
        }
        if (StringUtils.isNotBlank(it.getDescription())) {
          writer.addAttribute("Description", it.getDescription());
        }
        writer.setValue(it.getRequest());
        writer.endNode();

      }
    };
  }

  private static void configureOption(final XStream xstream) {
    xstream.alias("Option", ConfOption.class);
    xstream.aliasAttribute(ConfOption.class, "nom", "Nom");
    xstream.aliasAttribute(ConfOption.class, "valeur", "Valeur");
  }
}
