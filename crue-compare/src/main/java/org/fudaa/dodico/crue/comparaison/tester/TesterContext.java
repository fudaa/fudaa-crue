/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.metier.helper.DynamicPropertyFilter;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;

import java.util.Collection;

/**
 * @author deniger
 */
public interface TesterContext {
  boolean isIgnoreComment();

  /**
   * @return le filtre a utiliser pour les comparaisons de resultats.
   */
  DynamicPropertyFilter getResultatFilter();

  /**
   * @return the only properties to compare
   */
  Collection<String> getPropToCompare();

  Collection<String> getPropToIgnore();

  /**
   * @param error the error to set
   */
  void setError(CtuluLog error);

  void addError(final String msg);

  TesterContext createSubTesterContext();

  double getDoubleEpsilonRelative();

  void setDoubleEpsilonRelative(double doubleEpsilonRelative);

  ToStringTransformer getToStringTransformer();

  void addError(final String msg, final Object... data);

  void addError(final String msg, final Throwable e);

  void addErrorThrown(final String msg, final Throwable e, final Object... data);

  void addFatalError(final String m);

  void addFatalError(final String m, final Object... arg);

  void addInfo(final String msg);

  void addInfo(final String msg, final Object... args);

  void addWarn(final String msg);

  void addWarn(final String msg, final Object... args);

  boolean canDigg();

  /**
   * Indique au contexte que l'on a creuse la
   */
  void diggDone();

  /**
   * @return the error
   */
  CtuluLog getError();

  void stopDigg();

  boolean isObjectAlreadyTested(final Object o);

  void manageException(final Exception e);

  void manageException(final Exception e, final String msg);

  /**
   * @return true if string comparison ignore case.
   */
  boolean isIgnoreCaseForStringComparison();

  /**
   * @return true to trim string before comparison.
   */
  boolean isTrimStringForComparison();

  boolean isActivated(TestOption option);
}
