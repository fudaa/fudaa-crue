/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.config;

/**
 * @author deniger
 */
public interface ConfSelectionItem {

  String getDescription();

}
