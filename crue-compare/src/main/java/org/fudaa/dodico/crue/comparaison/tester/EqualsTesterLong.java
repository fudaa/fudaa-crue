/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.fudaa.dodico.crue.config.ccm.ItemVariable;

/**
 * @author deniger
 */
public class EqualsTesterLong extends AbstractEqualsTester<Number> {
    private ItemVariable property;

    /**
     * @return l'epsilon utilise pour les comparaison
     */
    public ItemVariable getProperty() {
        return property;
    }

    /**
     * @param eps l'epsilon utilise pour les comparaison
     */
    public void setProperty(final ItemVariable eps) {
        this.property = eps;
    }

    @Override
    protected boolean mustTestAlreadyDone() {
        return false;
    }

    /**
     * @param d1 le double a comparer
     * @param d2 le double a comparer
     * @return true si egaux a eps pres.
     */
    public boolean isSameLong(final long d1, final long d2) {
        if (property == null) {
            return d1 == d2;
        }
        if (property.isInfiniPositif(d1) && property.isInfiniPositif(d2)) {
            return true;
        }
        if (property.isInfiniNegatif(d1) && property.isInfiniNegatif(d2)) {
            return true;
        }
        return property.getEpsilon().isSame(d1, d2);
    }

    @Override
    public boolean isSameSafe(final Number o1, final Number o2, ResultatTest res, TesterContext context) {
        return isSameLong(o1.longValue(), o2.longValue());
    }
}
