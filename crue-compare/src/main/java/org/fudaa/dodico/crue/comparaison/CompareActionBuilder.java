/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.comparaison.tester.ResultatTest;

/**
 * @author deniger
 */
public interface CompareActionBuilder {
    /**
     * @param target   la cible
     * @param parent   le resultat paernt
     * @param ctuluLog les logs
     * @return true si identique...
     */
    boolean launch(ComparaisonNodeFinal target, ResultatTest parent, CtuluLog ctuluLog);

    /**
     * @return si difference sur l'ordre
     */
    boolean isDiffOnOrder();
}
