/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.metier.helper.DynamicPropertyFilter;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.comparaison.config.*;
import org.fudaa.dodico.crue.comparaison.tester.*;

import java.util.List;
import java.util.Map;

/**
 * @author deniger
 */
public class ConvertCompare {
  private FactoryEqualsTester factory;
  private DynamicPropertyFilter resPropertyFilter;

  /**
   * @param resPropertyFilter the resPropertyFilter to set
   */
  public void setResPropertyFilter(final DynamicPropertyFilter resPropertyFilter) {
    this.resPropertyFilter = resPropertyFilter;
  }

  public static int getSize(final List<?> a) {
    return a == null ? 0 : a.size();
  }

  /**
   * @param factory the factory to set
   */
  public void setFactory(final FactoryEqualsTester factory) {
    this.factory = factory;
  }

  private final Map<String, TestOption> optionByKey = TestOption.getByKey();

  private void computeOptions(final AbstractConfCompare inObject, final TesterContextFactory context, final CtuluLog analyze) {
    final List<ConfOption> options = inObject.getOptions();
    boolean unknownOption = false;
    if (CollectionUtils.isNotEmpty(options)) {
      for (final ConfOption confOption : options) {
        final TestOption option = optionByKey.get(confOption.getNom());
        if (option != null) {
          if (option.isActive(confOption.getValeur())) {
            context.activeOption(option);
          }
        } else {
          unknownOption = true;
        }
      }
    }
    if (unknownOption) {
      analyze.addWarn("options.notManaged");
    }
  }

  CompareActionBuilder convert(final ConfCompare in, final CtuluLog analyze, final ToStringTransformer toStringTransformer) {
    if (in == null) {
      return null;
    }
    final Class<?> inClass = in.getClass();
    if (ConfCompareObject.class.equals(inClass)) {
      final ConfCompareObject inObject = (ConfCompareObject) in;

      final EqualsTesterBean res = new EqualsTesterBean(factory);
      res.getContextFactory().setMaxDeep(inObject.getDeep());
      res.getContextFactory().setIgnoreCase(inObject.isIgnoreCase());
      res.getContextFactory().setTrimString(inObject.isTrimString());
      res.getContextFactory().setMaxDeep(inObject.getDeep());
      res.getContextFactory().setToStringTransfomer(toStringTransformer);
      res.getContextFactory().setResFilter(resPropertyFilter);
      res.getContextFactory().setPropertiesToCompare(inObject.getAttributs());
      res.getContextFactory().setPropertiesToIgnore(inObject.getAttributsToIgnore());
      computeOptions(inObject, res.getContextFactory(), analyze);
      return new ExecuteCompareActionOnObject(res, factory);
    } else if (ConfCompareListe.class.equals(inClass)) {
      final ConfCompareListe inObject = (ConfCompareListe) in;
      final EqualsTesterIsIncludeCollection res = new EqualsTesterIsIncludeCollection(inObject.getAttribut(), inObject
        .isBidirect());
      res.setPrefixDiffMessageId(inObject.getDiffTranslation());
      computeOptions(inObject, res.getContextFactory(), analyze);
      return new ExecuteCompareActionOnList(res, false);
    } else if (ConfCompareListeOrder.class.equals(inClass)) {
      final ConfCompareListeOrder inObject = (ConfCompareListeOrder) in;
      final EqualsTesterOrderListe res = new EqualsTesterOrderListe(inObject.getAttribut(), inObject.isOnlyIfSameList(), factory
        .getPropertyDefinition());
      computeOptions(inObject, res.getContextFactory(), analyze);
      return new ExecuteCompareActionOnList(res, true);
    }
    analyze.addSevereError("compare.notRecognized", inClass);
    return null;
  }
}
