/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.io;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueXmlReaderWriterImpl;

import java.io.InputStream;
import java.net.URL;

public class ReaderSelector {

  private CtuluLog analyze;

  public CtuluLog getAnalyze() {
    return analyze;
  }

  public SelectorItemContainer read(final URL file) {
    analyze = new CtuluLog();
    analyze.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    analyze.setDesc("read.selector.file");
    if (file == null) {
      analyze.addSevereError("file.read.null");
      return null;
    }
    analyze.setDescriptionArgs(file.getFile());

    InputStream in = null;
    SelectorItemContainer res = null;
    try {
      in = file.openStream();
      final XStream xstream = createXstream();
      final Object fromXML = xstream.fromXML(in);
      if (fromXML instanceof SelectorItemContainer) {
        res = (SelectorItemContainer) fromXML;
      } else {
        analyze.addSevereError("file.read.badContent.error", file);
      }
    } catch (final Exception e) {
      analyze.addSevereError("file.read.error", file, e.getMessage());
    } finally {
      CtuluLibFile.close(in);
    }
    return res;

  }

  private XStream createXstream() {
    final XStream xstream = new XStream(new StaxDriver());
    CrueXmlReaderWriterImpl.initXstreamSecurity(xstream);
    xstream.alias("Tests", SelectorItemContainer.class);
    xstream.addImplicitCollection(SelectorItemContainer.class, "items");
    xstream.alias("Ignore", SelectorItem.class);
    xstream.registerConverter(new ItemConverter());
    return xstream;
  }

  protected static class ItemConverter implements Converter {

    @Override
    public boolean canConvert(Class type) {
      return type.equals(SelectorItem.class);
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
      //rien a faire
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
      SelectorItem res = new SelectorItem();
      res.testId = reader.getValue();
      return res;
    }
  }
}
