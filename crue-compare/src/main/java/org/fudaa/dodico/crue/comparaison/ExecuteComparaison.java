/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.PropertyUtilsCache;
import org.fudaa.dodico.crue.metier.helper.DynamicPropertyFilter;
import org.fudaa.dodico.crue.common.io.CacheFileInputStream;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.comparaison.config.ConfComparaison;
import org.fudaa.dodico.crue.comparaison.config.ConfComparaisonConteneur;
import org.fudaa.dodico.crue.comparaison.config.ConfCompare;
import org.fudaa.dodico.crue.comparaison.tester.FactoryEqualsTester;
import org.fudaa.dodico.crue.comparaison.tester.ResultatTest;
import org.fudaa.dodico.crue.comparaison.tester.ResultatTestMessagesCache;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransfomerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author deniger
 */
public class ExecuteComparaison {

  private final ConfComparaisonConteneur in;
  private final ConvertCompare convertCompare;
  private final ToStringTransformer transformer;

  public ExecuteComparaison(final ConfComparaisonConteneur in, final CrueConfigMetier props, final DynamicPropertyFilter resFilter, final int maxReadCrLine, final int maxDiffByEMHOnResultat) {
    super();
    this.in = in;
    convertCompare = new ConvertCompare();
    convertCompare.setFactory(new FactoryEqualsTester(props, maxReadCrLine, maxDiffByEMHOnResultat));
    convertCompare.setResPropertyFilter(resFilter);
    transformer = ToStringTransfomerFactory.create(props, DecimalFormatEpsilonEnum.COMPARISON);
  }

  public static int getNbDifferences(Collection<ExecuteComparaisonResult> result) {
    int sum = 0;
    if (result == null) {
      return sum;
    }
    for (ExecuteComparaisonResult executeComparaisonResult : result) {
      sum += executeComparaisonResult.getNbDifferences();
    }
    return sum;
  }

  public static String getString(final Object a) {
    if (a == null) {
      return null;
    }
    String traduct = null;
    final String className = a.getClass().getSimpleName();
    try {
      traduct = BusinessMessages.getString(className + ".comparaisonTypeName");
    } catch (final Exception e) {
    }
    return traduct == null ? className : traduct;
  }

  public List<ExecuteComparaisonResult> launch(final Object a, final Object b, Predicate comparaisonPredicate) {
    PropertyUtilsCache.getInstance().setActive(true);
    ResultatTestMessagesCache.getInstance().setUseCache(true);
    CacheFileInputStream.getINSTANCE().setActive(true);
    final List<ConfComparaison> comparaisons = in.getComparaisons();
    final List<ExecuteComparaisonResult> res = new ArrayList<>(comparaisons.size());
    try {
      for (final ConfComparaison cmp : comparaisons) {
        if (comparaisonPredicate.evaluate(cmp)) {
          final ExecuteComparaisonResult launch = launch(cmp, a, b);
          if (launch != null) {
            res.add(launch);
          }
        }
      }
    } finally {
      PropertyUtilsCache.getInstance().setActive(false);
      ResultatTestMessagesCache.getInstance().setUseCache(false);
      CacheFileInputStream.getINSTANCE().setActive(false);
    }
    return res;
  }

  private ExecuteComparaisonResult launch(final ConfComparaison cmp, final Object a, final Object b) {
    if (!cmp.isActive()) {
      return null;
    }
    final ExecuteComparaisonResult res = new ExecuteComparaisonResult(transformer);
    res.id = cmp.getId();
    res.msg = cmp.getNomi18n();
    res.log = new CtuluLog();
    res.log.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    final List<ConfCompare> compare = cmp.getCompare();
    final CompareActionBuilder tester = convertCompare.convert(compare.get(0), res.log, this.transformer);
    if (tester == null) {
      return res;
    }
    res.diffOnOrder = tester.isDiffOnOrder();
//    res.
    final ConvertSelectOn selector = new ConvertSelectOn();
    selector.setA(a);
    selector.setB(b);
    selector.setSelect(cmp.getSelection());
    List<ComparaisonNodeFinal> go = null;
    try {
      go = selector.doSelection();
    } catch (final Exception e) {
      res.log.manageException(e);
    }
    res.log.setDesc(cmp.getNom());
    if (CollectionUtils.isEmpty(go)) {
      return res;
    }
    for (final ComparaisonNodeFinal toTest : go) {
      final ResultatTest testEnCours = res.registerResultatTest(toTest);
      testEnCours.setIsOrderDiff(res.diffOnOrder);
      if (toTest.getResultatTestFoundOnSelection() != null) {
        testEnCours.addDiff(toTest.getResultatTestFoundOnSelection());
      }
      final boolean launch = tester.launch(toTest, testEnCours, res.log);
      res.nbObjectTested += testEnCours.getNbObjectTested();
      testEnCours.setSame(testEnCours.isSame() && launch);
    }
    res.res.updateSameInArbo();
    return res;

  }
}
