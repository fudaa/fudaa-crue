/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.PropertyUtilsCache;
import org.fudaa.dodico.crue.comparaison.config.ConfSelectionItem;
import org.fudaa.dodico.crue.comparaison.config.ConfSelectionItemMerge;
import org.fudaa.dodico.crue.comparaison.tester.ResultatTest;

import java.util.*;

public class ConvertSelectOnItemMerge implements ConvertSelectOnItem {

  @Override
  public List<ComparaisonNodeFinal> compute(final List<ComparaisonNodeFinal> in, final ConfSelectionItem requete)
          throws Exception {
    return computeOn(in, (ConfSelectionItemMerge) requete);
  }

  private List<ComparaisonNodeFinal> computeOn(final List<ComparaisonNodeFinal> in, final ConfSelectionItemMerge requete)
          throws Exception {
    final List<ComparaisonNodeFinal> selectedBy = new ConvertSelectOnItemRequest().computeOn(in, requete.getRequest());
    for (final ComparaisonNodeFinal nodeFinal : selectedBy) {
      computeOn(nodeFinal, requete);
    }
    return selectedBy;
  }

  private void computeOn(final ComparaisonNodeFinal in, final ConfSelectionItemMerge requete) throws Exception {
    List initA = new ArrayList(in.listObjectA);
    List initB = new ArrayList(in.listObjectB);

    String[] att = StringUtils.split(requete.getAttribut(), ",");
    final int min = Math.min(initA.size(), initB.size());
    final List newA = new ArrayList(min);
    final List newB = new ArrayList(min);
    merge(initA, initB, newA, newB, att);
    if (requete.isErrorIfDifferentSize() && initA.size() != initB.size()) {
      in.setResultatTestFoundOnSelection(new ResultatTest(initA.size(), initB.size(), "comparaison.collection.size"));
    }

    in.listObjectA = newA;
    in.listObjectB = newB;

  }

  private static class MultiKeyComparator implements Comparator<MultiKey> {

    @Override
    public int compare(MultiKey o1, MultiKey o2) {
      if (o1 == o2) {
        return 0;
      }
      if (o1 == null) {
        return -1;
      }
      if (o2 == null) {
        return 1;
      }
      int nb = Math.min(o1.size(), o2.size());
      //on compare les clés une a une utilisant la représentation toString.
      for (int i = 0; i < nb; i++) {
        Object obj1 = o1.getKey(i);
        Object obj2 = o2.getKey(i);
        int res = ObjectUtils.toString(obj1).compareTo(ObjectUtils.toString(obj2));
        if (res != 0) {
          return res;
        }
      }
      if (o1.size() > o2.size()) {
        return 1;
      }
      if (o2.size() > o1.size()) {
        return -1;
      }
      return 0;
    }
  }

  private void merge(final List listInitialA, final List listInitialB, final List targetListA, final List targetListB,
          String[] attribut)
          throws Exception {
    final LinkedHashMap propA = getMap(listInitialA, attribut);
    final LinkedHashMap propB = getMap(listInitialB, attribut);
    targetListA.clear();
    targetListB.clear();
    //on trie les clés de A:
    List keysInListA = new ArrayList(propA.keySet());
    //suite demande CRUE-394 on ne tri pas
    //a enlever si pas de retours...
//    if (!keysInListA.isEmpty()) {
//      Object first = keysInListA.get(0);
//      if (first instanceof Comparable) {
//        Collections.sort(keysInListA);
//      } else if (first instanceof MultiKey) {
//        Collections.sort(keysInListA, new MultiKeyComparator());
//      }
//    }
    //a optimiser si pas de retours sur le tri.
    for (Object key : keysInListA) {
      final Object inB = propB.get(key);
      if (inB != null) {
        targetListA.add(propA.get(key));
        targetListB.add(inB);
      }
    }
  }

  private static LinkedHashMap getMap(final Collection obj, final String[] keyProp) throws Exception {
    final LinkedHashMap res = new LinkedHashMap(obj.size());
    for (final Object object : obj) {
      if (keyProp.length == 1) {
        final Object property = PropertyUtilsCache.getInstance().getProperty(object, keyProp[0]);
        res.put(property, object);
      } else {
        res.put(createMultiKey(keyProp, object), object);
      }

    }
    return res;

  }

  private static MultiKey createMultiKey(final String[] keyProp, final Object object) throws Exception {
    Object[] multiKey = new Object[keyProp.length];
    for (int i = 0; i < keyProp.length; i++) {
      multiKey[i] = PropertyUtilsCache.getInstance().getProperty(object, keyProp[i]);
    }
    return new MultiKey(multiKey);
  }

  public static Set getSet(final Collection obj, final String[] prop) throws Exception {
    final Set res = new HashSet(obj.size());
    for (final Object object : obj) {
      if (prop == null) {
        res.add(object);
      } else if (prop.length == 1) {
        final Object property = PropertyUtils.getProperty(object, prop[0]);
        res.add(property);
      } else {
        res.add(createMultiKey(prop, object));
      }
    }
    return res;

  }
}
