/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.PropertyUtilsCache;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author deniger
 */
@SuppressWarnings("unchecked")
public class EqualsTesterOrderListe extends AbstractEqualsTester<List> {
    private final String propName;
    private final CrueConfigMetier crueProperties;
    private final boolean onlyIfSameList;

    public EqualsTesterOrderListe(final String propName, boolean onlyIfSameList, final CrueConfigMetier crueProperties) {
        super();
        this.propName = propName;
        this.crueProperties = crueProperties;
        this.onlyIfSameList = onlyIfSameList;
    }

    private void addDiff(Object o1, final Object property, final ResultatTest res, final Integer post1, final Integer pos2) {
        if (res != null) {
            String id = TransformerEMHHelper.transformToString(o1, crueProperties, EnumToString.COMPLETE, DecimalFormatEpsilonEnum.COMPARISON);
            if (StringUtils.isBlank(id)) {
                id = property.toString();
            }
            ResultatTest resultatTest = new ResultatTest(post1, pos2, "diff.orderIsDifferent", id);
            resultatTest.setIsOrderDiff(true);
            res.addDiff(resultatTest);
        }
    }

    private Map<Object, Integer> getOrder(List list) throws Exception {
        Map<Object, Integer> res = new HashMap<>();
        for (int i = 0; i < list.size(); i++) {
            Object o = list.get(i);
            res.put(PropertyUtilsCache.getInstance().getProperty(o, propName), Integer.valueOf(i + 1));// pour avoir la numerotation "Humaine".
        }
        return res;
    }

    @Override
    public boolean isSameSafe(final List o1, final List o2, final ResultatTest res, TesterContext context)
            throws Exception {
        Map<Object, Integer> positionsIno2 = getOrder(o2);
        // si onlyIfSameList vaut true, la comparaison ne sait que si les 2 listes sont identiques.
        if (onlyIfSameList) {
            if (o1.size() != o2.size()) {
                return true;
            }
            Map<Object, Integer> positionsIno1 = getOrder(o1);
            if (positionsIno1.size() != positionsIno2.size()) {
                return true;
            }
            for (Object object : positionsIno1.keySet()) {
                if (!positionsIno2.containsKey(object)) {
                    return true;
                }
            }
        }
        boolean resFinal = true;
        for (int i = 0; i < o1.size(); i++) {
            int position = i + 1;// pour avoir la numerotation "Humaine".
            Object object = o1.get(i);
            Object property = PropertyUtilsCache.getInstance().getProperty(object, propName);
            Integer inO2 = positionsIno2.get(property);
            if (inO2 != null && inO2.intValue() != position) {
                addDiff(object, property, res, position, inO2);
                resFinal = false;
            }
        }
        return resFinal;
    }

    @Override
    protected boolean mustTestAlreadyDone() {
        return false;
    }
}
