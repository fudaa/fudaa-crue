/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

/**
 * @author deniger
 */
public class EqualsTesterObject extends AbstractEqualsTester<Object> {
    @Override
    public boolean isSameSafe(final Object o1, final Object o2, ResultatTest res, TesterContext context) {
        return o1.equals(o2);
    }

    @Override
    protected boolean mustTestAlreadyDone() {
        return false;
    }
}
