package org.fudaa.dodico.crue.comparaison.tester;

/**
 * @author deniger
 */
public final class EqualsTesterItemBuilderDefaults {
  public static final EqualsTesterItemBuilder.LongTester LONG_TESTER = new EqualsTesterItemBuilder.LongTester();
  public static final EqualsTesterItemBuilder.IntegerTester INTEGER_TESTER = new EqualsTesterItemBuilder.IntegerTester();
  public static final EqualsTesterItemBuilder.DoubleTester DOUBLE_TESTER = new EqualsTesterItemBuilder.DoubleTester();

  private EqualsTesterItemBuilderDefaults() {
  }
}
