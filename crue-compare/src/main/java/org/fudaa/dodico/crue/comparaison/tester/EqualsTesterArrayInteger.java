/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;


/**
 * @author deniger
 */
public class EqualsTesterArrayInteger extends AbstractEqualsTester<int[]> {

  private final EqualsTesterInteger objTester;

  /**
   * @param objTester le tester
   */
  public EqualsTesterArrayInteger(final EqualsTesterInteger objTester) {
    super();
    this.objTester = objTester;
  }

  @Override
  protected boolean mustTestAlreadyDone() {
    return false;
  }

  @Override
  public boolean isSameSafe(final int[] o1, final int[] o2, final ResultatTest res, TesterContext context) {
    final int length = o1.length;
    if (length != o2.length) {
      if (res != null) {
        res.addDiff(new ResultatTest(length, o2.length, EqualsTesterCollection.TAILLE_DIFF));
      }
      return false;
    }
    for (int idx = 0; idx < length; idx++) {
      final boolean isSame = objTester == null ? (o1[idx] == o2[idx]) : (objTester.isSameInteger(o1[idx], o2[idx]));
      if (!isSame) {
        if (res != null) {
          res.addDiff(new ResultatTest(o1[idx], o2[idx], "comparaison.collection.item", idx));
        }
        return false;

      }

    }
    return true;
  }
}
