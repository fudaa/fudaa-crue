/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.config;

import org.apache.commons.lang3.StringUtils;

public class ConfCompareListeOrder extends AbstractConfCompare {

  private String attribut;
  private boolean onlyIfSameList;

  /**
   * @return the attribut
   */
  public String getAttribut() {
    return attribut;
  }

  /**
   * @param attribut the attribut to set
   */
  @SuppressWarnings("unused")
  public void setAttribut(final String attribut) {
    this.attribut = StringUtils.trim(attribut);
  }

  /**
   * @return the onlyIfSameList
   */
  public boolean isOnlyIfSameList() {
    return onlyIfSameList;
  }

  /**
   * @param onlyIfSameList the onlyIfSameList to set
   */
  @SuppressWarnings("unused")
  public void setOnlyIfSameList(final boolean onlyIfSameList) {
    this.onlyIfSameList = onlyIfSameList;
  }

}
