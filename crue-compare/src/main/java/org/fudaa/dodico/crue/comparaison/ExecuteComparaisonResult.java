/*
 GPL 2
 */
package org.fudaa.dodico.crue.comparaison;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.PropertyUtilsCache;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.comparaison.tester.ResultatTest;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Frederic Deniger
 */
public class ExecuteComparaisonResult implements Comparable<ExecuteComparaisonResult> {

  CtuluLog log;
  private final ToStringTransformer transformer;
  boolean diffOnOrder;

  public ExecuteComparaisonResult(ToStringTransformer transformer) {
    super();
    this.transformer = transformer;
  }

  public void setDiffOnOrder(boolean diffOnOrder) {
    this.diffOnOrder = diffOnOrder;
  }

  public boolean isDiffOnOrder() {
    return diffOnOrder;
  }

  public int getNbDifferences() {
    return res == null ? 0 : res.getNbDifferences();
  }

  /**
   * @return the analyze
   */
  public CtuluLog getLog() {
    return log;
  }

  /**
   * @param analyze the analyze to set
   */
  public void setLog(final CtuluLog analyze) {
    this.log = analyze;
  }
  int nbObjectTested;

  public void setNbObjectTested(int nbObjectTested) {
    this.nbObjectTested = nbObjectTested;
  }

  /**
   * @return the nbObjectTested
   */
  public int getNbObjectTested() {
    return nbObjectTested;
  }

  @Override
  public int compareTo(final ExecuteComparaisonResult o) {
    if (o == null) {
      return 1;
    }
    return id.compareTo(o.id);
  }
  ResultatTest res;
  String id;
  String msg;

  public void setRes(ResultatTest res) {
    this.res = res;
  }

  public String getId() {
    return id;
  }

  public void setId(final String id) {
    this.id = id;
  }

  /**
   * @return the msg
   */
  public String getMsg() {
    return msg;
  }

  /**
   * @param msg the msg to set
   */
  public void setMsg(final String msg) {
    this.msg = msg;
  }

  /**
   * @return the res
   */
  public ResultatTest getRes() {
    return res;
  }
  private final Map<ComparaisonNode, ResultatTest> map = new HashMap<>();

  ResultatTest registerResultatTest(final ComparaisonNode compared) {
    ResultatTest resTest = map.get(compared);
    if (resTest != null) {
      resTest.incrementeObjectTested();
      return resTest;
    }
    final ComparaisonNode parentNode = compared.parent;
    // c'est le root:
    if (parentNode == null) {
      final ResultatTest parentTest = createRoot(compared);
      parentTest.incrementeObjectTested();
      return parentTest;
    }
    final ResultatTest parentTest = registerResultatTest(parentNode);
    resTest = createResFor(compared);
    parentTest.addGroup(resTest);
    resTest.incrementeObjectTested();
    map.put(compared, resTest);
    return resTest;
  }

  private ResultatTest createRoot(final ComparaisonNode compared) {
    if (res == null) {
      res = createResFor(compared);
    }
    return res;
  }

  private ResultatTest createResFor(final ComparaisonNode node) {
    final Object objetB = node.objetB;
    final Object objetA = node.objetA;
    final ResultatTest resForNode = new ResultatTest();
    resForNode.setObjetA(objetA);
    resForNode.setObjetB(objetB);
    if (node.printForThisLevel != null) {
      try {
        resForNode.setPrintA(PropertyUtilsCache.getInstance().getProperty(objetA, node.printForThisLevel));
        resForNode.setPrintB(PropertyUtilsCache.getInstance().getProperty(objetB, node.printForThisLevel));
      } catch (final Exception e) {
        log.manageException(e);
      }
    }
    resForNode.updatePrint(transformer);
    if (StringUtils.isBlank(node.msg)) {
      String className = ExecuteComparaison.getString(objetA);
      if (className == null) {
        className = ExecuteComparaison.getString(objetB);
      }
      resForNode.setMsg(className);
    } else {
      resForNode.setMsg(node.msg);
    }
    map.put(node, resForNode);
    return resForNode;
  }
}
