/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.config;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class ConfCompareObject extends AbstractConfCompare {
  private int deep = 1;
  private List<String> attributs;
  private List<String> attributsToIgnore;
  private boolean trimString;
  private boolean ignoreCase;

  /**
   * @return the deep
   */
  public int getDeep() {
    return deep;
  }

  /**
   * @param deep the deep to set
   */
  public void setDeep(final int deep) {
    this.deep = deep;
  }

  /**
   * @return the attributs
   */
  public List<String> getAttributs() {
    return attributs;
  }

  /**
   * @param attributs the attributs to set
   */
  public void setAttributs(final List<String> attributs) {
    this.attributs = attributs;
    if (this.attributs != null) {
      CollectionUtils.transform(this.attributs, input -> StringUtils.trim((String) input));
    }
  }

  /**
   * Utilise par Xstream
   * @param attributs the attributs to set
   */
  @SuppressWarnings("unused")
  public void setAttributsToIgnore(final List<String> attributs) {
    this.attributsToIgnore = attributs;
    if (this.attributsToIgnore != null) {
      CollectionUtils.transform(this.attributsToIgnore, input -> StringUtils.trim((String) input));
    }
  }

  /**
   * @return the trimString
   */
  public boolean isTrimString() {
    return trimString;
  }

  /**
   * Utilise par Xstream
   * @param trimString the trimString to set
   */
  @SuppressWarnings("unused")
  public void setTrimString(final boolean trimString) {
    this.trimString = trimString;
  }

  /**
   * @return the ignoreCase
   */
  public boolean isIgnoreCase() {
    return ignoreCase;
  }

  /**
   * Utilise par Xstream
   * @param ignoreCase the ignoreCase to set
   */
  @SuppressWarnings("unused")
  public void setIgnoreCase(final boolean ignoreCase) {
    this.ignoreCase = ignoreCase;
  }

  /**
   * @return the attributsToIgnore
   */
  public List<String> getAttributsToIgnore() {
    return attributsToIgnore;
  }
}
