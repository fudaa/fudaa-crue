/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

/**
 * un template pour comparer des long.
 * 
 * @author deniger
 */
public class TemplateEqualsTesterLong {

  private long eps = 0;

  /**
   * @return l'epsilon utilise pour les comparaison
   */
  public long getEps() {
    return eps;
  }

  /**
   * @param eps l'epsilon utilise pour les comparaison
   */
  public void setEps(final long eps) {
    this.eps = Math.abs(eps);
  }

  /**
   * @param d1 le double a comparer
   * @param d2 le double a comparer
   * @return true si egaux a eps pres.
   */
  public boolean isSameLong(final long d1, final long d2) {
    if (eps > 0) { return Math.abs(d1 - d2) < eps; }
    return d1 == d2;
  }

}
