/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.config;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;

import java.util.List;

/**
 * @author deniger
 */
public class ConfComparaison implements ObjetNomme {
    private String nom;
    private String nomi18n;
    private String id;
    private ConfSelection selection;
    private List<ConfCompare> compare;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return true;
    }

    /**
     * @return le nom initial lu dans le fichier de configuration.
     */
    @Override
    public String getNom() {
        return nom;
    }

    @Override
    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNomi18n() {
        if (nomi18n == null) {
            nomi18n = BusinessMessages.getStringOrDefault(id + ".name", nom);
        }
        return nomi18n;
    }

    public ConfSelection getSelection() {
        return selection;
    }

    public void setSelection(ConfSelection selection) {
        this.selection = selection;
    }

    public List<ConfCompare> getCompare() {
        return compare;
    }

    public void setCompare(List<ConfCompare> compare) {
        this.compare = compare;
    }
}
