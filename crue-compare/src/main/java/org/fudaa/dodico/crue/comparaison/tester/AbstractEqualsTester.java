/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

/**
 * Tests de comparaison par défaut gérant les null.
 *
 * @param <T>
 * @author deniger
 */
public abstract class AbstractEqualsTester<T> implements EqualsTester<T> {
    private TesterContextFactory contextFactory;

    /**
     * @return the contextFactory
     */
    @Override
    public TesterContextFactory getContextFactory() {
        if (contextFactory == null) {
            contextFactory = new TesterContextFactory();
        }
        return contextFactory;
    }

    /**
     * @param contextFactory the contextFactory to set
     */
    @Override
    public void setContextFactory(TesterContextFactory contextFactory) {
        this.contextFactory = contextFactory;
    }

    public final boolean isSame(final T o1, final T o2) throws Exception {
        return isSame(o1, o2, null, new TesterContextImpl(null));
    }

    @Override
    public final boolean isSame(final T o1, final T o2, final ResultatTest res, final TesterContext context)
            throws Exception {
        if (o1 == o2) {
            return true;
        }
        if (o1 == null || o2 == null) {
            return false;
        }
        if (mustTestAlreadyDone() && context.isObjectAlreadyTested(o1)) {
            // on interdit de récuperer d'autre objet.
            context.stopDigg();
        }
        return isSameSafe(o1, o2, res, context);
    }

    /**
     * @return true si le tester doit verifier si l'objet a deja ete teste ou non
     */
    protected abstract boolean mustTestAlreadyDone();

    /**
     * @param o1      non null
     * @param o2      non null
     * @param res     recoit les resultat de la comparaison.
     * @param context le contexte de la comparaison: utilise si plusieurs niveaux de la grappe d'objets sont teste: permet
     *                de limiter la profondeur.
     * @return true si egaux.
     * @throws Exception exception introspection
     */
    public abstract boolean isSameSafe(T o1, T o2, ResultatTest res, TesterContext context) throws Exception;

    /**
     * @param o1  objet 1 a comparer
     * @param o2  objet 2 a comparer
     * @param res le resultat
     * @return true si identique
     * @throws Exception si erreur dans introspection
     */
    public boolean isSameSafe(T o1, T o2, ResultatTest res) throws Exception {
        return isSameSafe(o1, o2, res, getContextFactory().create());
    }
}
