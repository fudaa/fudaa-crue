/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.comparaison.ConvertSelectOnItemMerge;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

/**
 * @author deniger
 */
public class EqualsTesterIsIncludeCollection extends AbstractEqualsTester<Collection> {
    /**
     *
     */
    private static final String DIFF_INCLUDE_DANS_B_PAS_DANS_A = BusinessMessages
            .getString("diff.include.dansB.pasDansA");
    /**
     *
     */
    private static final String DIFF_INCLUDE_DANS_A_PAS_DANS_B = BusinessMessages
            .getString("diff.include.dansA.pasDansB");
    private final String propName;
    private final boolean bidirect;
    private final int nbMaxItemInDiff = 50;
    private String prefixDiffMessageId;
    private String diffMessageIdDansBPasDansA;
    private String diffMessageIdDansAPasDansB;

    public EqualsTesterIsIncludeCollection(final String propName, final boolean bidi) {
        super();
        this.propName = propName;
        this.bidirect = bidi;
    }

    /**
     * @return the propName
     */
    public String getPropName() {
        return propName;
    }

    private String getDansAPasDansB() {
        if (prefixDiffMessageId == null) {
            return DIFF_INCLUDE_DANS_A_PAS_DANS_B;
        }
        if (diffMessageIdDansAPasDansB == null) {
            diffMessageIdDansAPasDansB = BusinessMessages.getString(prefixDiffMessageId + ".dansA.pasDansB");
        }
        return diffMessageIdDansAPasDansB;
    }

    private String getDansBPasDansA() {
        if (prefixDiffMessageId == null) {
            return DIFF_INCLUDE_DANS_B_PAS_DANS_A;
        }
        if (diffMessageIdDansBPasDansA == null) {
            diffMessageIdDansBPasDansA = BusinessMessages.getString(prefixDiffMessageId + ".dansB.pasDansA");
        }
        return diffMessageIdDansBPasDansA;
    }

    /**
     * @return the bidirect
     */
    public boolean isBidirect() {
        return bidirect;
    }

    @Override
    protected boolean mustTestAlreadyDone() {
        return false;
    }

    private String toString(Collection diff) {
        if (diff.size() > nbMaxItemInDiff) {
            ArrayList list = (ArrayList) diff;
            return StringUtils.join(list.subList(0, nbMaxItemInDiff), ";") + "; ...";
        }
        return StringUtils.join(diff, ";");
    }

    @Override
    public boolean isSameSafe(final Collection o1, final Collection o2, final ResultatTest res, TesterContext context)
            throws Exception {
        final String[] prop = StringUtils.split(propName, ",");
        final Set propInA = ConvertSelectOnItemMerge.getSet(o1, prop);
        propInA.remove(null);
        final Set propInB = ConvertSelectOnItemMerge.getSet(o2, prop);
        propInB.remove(null);
        boolean resFinal = true;
        Collection subtract = CollectionUtils.subtract(propInA, propInB);
        if (subtract.size() > nbMaxItemInDiff) {
            ArrayList list = (ArrayList) subtract;
            list.subList(0, nbMaxItemInDiff);
        }
        if (CollectionUtils.isNotEmpty(subtract)) {
            addDiff(getDansAPasDansB(), res, toString(subtract), StringUtils.EMPTY);
            resFinal = false;
        }
        if (bidirect) {
            subtract = CollectionUtils.subtract(propInB, propInA);
            if (CollectionUtils.isNotEmpty(subtract)) {
                addDiff(getDansBPasDansA(), res, StringUtils.EMPTY, toString(subtract));
                resFinal = false;
            }
        }

        return resFinal;
    }

    private void addDiff(final String msg, final ResultatTest res, final Object obj1, final Object obj2) {
        if (res != null) {
            res.addDiff(new ResultatTest(obj1, obj2, msg, true));
        }
    }

    public void setPrefixDiffMessageId(String prefixDiffMessageId) {
        this.prefixDiffMessageId = prefixDiffMessageId;
    }
}
