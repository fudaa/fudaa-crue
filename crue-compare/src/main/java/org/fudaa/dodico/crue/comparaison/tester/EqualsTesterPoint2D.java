/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.fudaa.dodico.crue.metier.emh.Point2D;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class EqualsTesterPoint2D extends AbstractEqualsTester<Point2D> {

  private final EqualsTesterDouble abs;
  private final EqualsTesterDouble ord;

  /**
   * @param abs le tester pour l'abscisse
   * @param ord le tester pour l'ordonnée.
   */
  public EqualsTesterPoint2D(final EqualsTesterDouble abs, final EqualsTesterDouble ord) {
    super();
    this.abs = abs;
    this.ord = ord;
  }

  @Override
  protected boolean mustTestAlreadyDone() {
    return false;
  }

  @Override
  public boolean isSameSafe(final Point2D o1, final Point2D o2, final ResultatTest res, final TesterContext context) {
    boolean isSame = true;
    if (abs == null) {//cas des enums
      isSame = ((int) o1.getAbscisse()) == ((int) o2.getAbscisse());
    } else {
      try {
        isSame = abs.isSame(o1.getAbscisse(), o2.getAbscisse(), null, context);
      } catch (Exception exception) {
        Logger.getLogger(EqualsTesterPoint2D.class.getName()).log(Level.INFO, "message {0}", exception);
      }
    }
    if (!isSame) {
      return false;
    }
    if (ord == null) {//cas des enums
      isSame = ((int) o1.getOrdonnee()) == ((int) o2.getOrdonnee());
    } else {
      try {
        isSame = ord.isSame(o1.getOrdonnee(), o2.getOrdonnee(), null, context);
      } catch (Exception exception) {
        Logger.getLogger(EqualsTesterPoint2D.class.getName()).log(Level.INFO, "message {0}", exception);
      }
    }
    return isSame;

  }
}
