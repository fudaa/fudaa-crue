/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.fudaa.dodico.crue.comparaison.tester.ResultatTest;

import java.util.Collections;
import java.util.List;

public class ComparaisonNodeFinal extends ComparaisonNode {

  @SuppressWarnings("unchecked")
  List listObjectA;
  @SuppressWarnings("unchecked")
  List listObjectB;

  private ResultatTest resultatTestFoundOnSelection;

  public ComparaisonNodeFinal() {
    super(false);
  }

  public ComparaisonNodeFinal(boolean isRoot) {
    super(isRoot);
  }

  @Override
  public List<ComparaisonNode> getFils() {
    return Collections.emptyList();
  }

  /**
   * @return the resultatTestFoundOnSelection
   */
  public ResultatTest getResultatTestFoundOnSelection() {
    return resultatTestFoundOnSelection;
  }

  @Override
  public boolean isFeuille() {
    return true;
  }

  public void setResultatTestFoundOnSelection(ResultatTest resultatTest) {
    this.resultatTestFoundOnSelection = resultatTest;
  }

}
