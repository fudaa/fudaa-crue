/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.apache.commons.beanutils.LazyDynaMap;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.cr.CRReader;
import org.fudaa.dodico.crue.metier.emh.CompteRendu;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author f.deniger
 */
public class EqualsTesterCompteRendu extends AbstractEqualsTester<CompteRendu> {
    private final int maxRecordRead;
    private final FactoryEqualsTester factory;

    public EqualsTesterCompteRendu(final FactoryEqualsTester factory, int maxRecord) {
        this.factory = factory;
        this.maxRecordRead = maxRecord;
    }

    @Override
    protected boolean mustTestAlreadyDone() {
        return false;
    }

    @Override
    public boolean isSameSafe(final CompteRendu o1, final CompteRendu o2, final ResultatTest resultatTest,
                              final TesterContext context) {
        File o1File = o1.getLogFile();
        File o2File = o2.getLogFile();
        ResultatTest thisResult = new ResultatTest(o1.getId(), o2.getId(), StringUtils.EMPTY);
        boolean ok = true;
        if (!o1File.exists()) {
            thisResult.addDiff(new ResultatTest(o1File.getName(), null, "crFileNotExists", o1File.getAbsolutePath()));
            ok = false;
        }
        if (!o2File.exists()) {
            thisResult.addDiff(new ResultatTest(null, o2File.getName(), "crFileNotExists", o2File.getAbsolutePath()));
            ok = false;
        }
        if (!ok) {
            resultatTest.addDiff(thisResult);
            return false;
        }
        CRReader logReader = new CRReader(maxRecordRead);
        CrueIOResu<CtuluLog> read = logReader.read(o1File);
        if (read.getAnalyse().containsErrorOrSevereError()) {
            context.getError().addAllLogRecord(read.getAnalyse());
        }
        CtuluLog cr1 = read.getMetier();
        read = logReader.read(o2File);
        if (read.getAnalyse().containsErrorOrSevereError()) {
            context.getError().addAllLogRecord(read.getAnalyse());
        }
        CtuluLog cr2 = read.getMetier();
        if (cr1 == cr2) {//cas null
            return true;
        }
        if (cr1 == null) {
            thisResult.addDiff(new ResultatTest("null", "", ""));
            resultatTest.addDiff(thisResult);
            return false;
        }
        if (cr2 == null) {
            thisResult.addDiff(new ResultatTest("", "null", ""));
            resultatTest.addDiff(thisResult);
            return false;
        }
        List<CtuluLogRecord> records1 = cr1.getRecords();
        List<CtuluLogRecord> records2 = cr2.getRecords();
        boolean same = true;
        if (records1.size() != records2.size()) {
            same = false;
            thisResult.addDiff(new ResultatTest(records1.size(), records2.size(), "comparaison.collection.size"));
        }
        int max = Math.min(records1.size(), records2.size());
        EqualsTesterBean bean = new EqualsTesterBean(factory);
        for (int i = 0; i < max; i++) {
            boolean lineSame = false;
            ResultatTest lineResult = null;
            try {
                Map<String, Object> log1 = CRReader.getDataMapWithoutDate(records1.get(i));
                Map<String, Object> log2 = CRReader.getDataMapWithoutDate(records2.get(i));
                lineResult = new ResultatTest(StringUtils.EMPTY, StringUtils.EMPTY, "comparaison.collection.item", Integer.toString(i + 1));
                Set<String> props = new HashSet<>();
                props.addAll(log1.keySet());
                props.addAll(log2.keySet());
                //pour s'assurer que tout es present
                for (String string : props) {
                    if (!log1.containsKey(string)) {
                        log1.put(string, null);
                    }
                    if (!log2.containsKey(string)) {
                        log2.put(string, null);
                    }
                }
                LazyDynaMap toCompare1 = new LazyDynaMap(log1);
                toCompare1.setReturnNull(true);//bizarre: ne fonctionne pas
                LazyDynaMap toCompare2 = new LazyDynaMap(log2);
                toCompare2.setReturnNull(true);//bizarre: ne fonctionne pas: leve une exception
                lineSame = bean.isSame(toCompare1, toCompare2, lineResult, context, props);
            } catch (Exception exception) {
                Logger.getLogger(EqualsTesterCompteRendu.class.getName()).log(Level.INFO, "message {0}", exception);
            }
            if (!lineSame) {
                thisResult.addDiff(lineResult);
                same = false;
                break;
            }
        }
        if (!same) {
            resultatTest.addDiff(thisResult);
        }

        return same;
    }
}
