/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;

/**
 * un template pour comparer des long.
 *
 * @author deniger
 * @param <T> le type d'objet a comparer.
 */
public abstract class AbstractTemplateEqualsTesterDouble<T> extends AbstractEqualsTester<T> {

  private PropertyNature property;
  /**
   * valeur pouvant être utilisée pour des comparaisons relatives.
   */
  boolean useRelativeEpsilonValue = true;

  /**
   * @return l'epsilon utilise pour les comparaison
   */
  public PropertyEpsilon getEps() {
    return property.getEpsilon();
  }

  @Override
  protected boolean mustTestAlreadyDone() {// NOPMD
    return false;
  }

  /**
   * @param variable l'epsilon utilise pour les comparaison
   */
  public final void setPropertyDefinition(final PropertyNature variable, boolean useRelativeValue) {
    assert variable != null;
    this.property = variable;
    this.useRelativeEpsilonValue = useRelativeValue;
  }

  /**
   * @param d1 le double a comparer
   * @param d2 le double a comparer
   * @return true si egaux a eps pres.
   */
  public boolean isSameDouble(final double d1, final double d2) {
    // le cas des infini:
    if (property.isInfiniPositif(d1) && property.isInfiniPositif(d2)) {
      return true;
    }
    if (property.isInfiniNegatif(d1) && property.isInfiniNegatif(d2)) {
      return true;
    }
    if (property != null) {
      if (property.isEnum()) {
        return ((int) d1) == ((int) d2);
      }
      return getEps().isSame(d1, d2);
    }
    return d1 == d2;
  }
}
