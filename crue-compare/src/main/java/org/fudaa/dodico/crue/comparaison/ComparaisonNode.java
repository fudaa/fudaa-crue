/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import java.util.List;

public abstract class ComparaisonNode {
  ComparaisonNode parent;
  String printForThisLevel;
  String printForFils;
  String msg;
  Object objetA;
  Object objetB;
  final boolean isRoot;

  /**
   * @return the isRoot
   */
  public boolean isRoot() {
    return isRoot;
  }

  protected ComparaisonNode(boolean isRoot) {
    super();
    this.isRoot = isRoot;
  }

  protected ComparaisonNode() {
    this(false);
  }

  public abstract boolean isFeuille();

  public abstract List<ComparaisonNode> getFils();
}
