/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.apache.commons.beanutils.LazyDynaMap;
import org.fudaa.dodico.crue.comparaison.ComparaisonSelectorContent;
import org.fudaa.dodico.crue.metier.Crue9TypeBranche;
import org.fudaa.dodico.crue.metier.emh.ResPrtGeo;

import java.util.*;

/**
 * @author deniger
 */
@SuppressWarnings("unchecked")
public class EqualsTesterRPTG extends AbstractEqualsTester<ResPrtGeo> {
    private final EqualsTesterBeanMap beanTester;
    private final Set<String> variablesC9 = new HashSet<>(Arrays.asList(
            "zLimGLitSto",
            "zLimDLitSto",
            "zLimGLitMaj",
            "zLimDLitMaj",
            "zLimGLitMin",
            "zLimDLitMin",
            "coefKextrapol",
            "loiZPmin",
            "loiZSmaj",
            "loiZPmaj",
            "loiZLmaj",
            "loiZCoefW1",
            "loiZCoefW2"));

    /**
     * @param equalsTester la factory pour créer les testeurs
     */
    public EqualsTesterRPTG(FactoryEqualsTester equalsTester) {
        super();
        this.beanTester = new EqualsTesterBeanMap(equalsTester);
        beanTester.setUseNameForLoi(false);
    }

    @Override
    protected boolean mustTestAlreadyDone() {
        return false;
    }

    @Override
    public boolean isSameSafe(final ResPrtGeo o1, final ResPrtGeo o2, final ResultatTest res,
                              final TesterContext context) {
        boolean isEquals = true;
        try {
            Map<String, Object> values1 = o1.getValues();
            Map<String, Object> values2 = o2.getValues();
            //certaines variables ne sont calculées que dans C9: il faut les enlever dans le cas des comparaison c9c10.
            //CRUE-376
            boolean values1IsC9 = o1.isC9();
            boolean values2IsC9 = o2.isC9();
            if (values1IsC9 != values2IsC9) {
                values1 = new HashMap<>(values1);
                values2 = new HashMap<>(values2);
                for (String varC9ToRemove : variablesC9) {
                    if (values1IsC9) {
                        values1.remove(varC9ToRemove);
                    }
                    if (values2IsC9) {
                        values2.remove(varC9ToRemove);
                    }
                }
                //voir CRUE-417:
                ResPrtGeo rptgToUseToFindTypBra = values1IsC9 ? o1 : o2;
                int typBra = rptgToUseToFindTypBra.getTypBra();
                //LoiZBeta pour les sections des branches 2, 6, 15,
                //LstLitLinf pour les sections des branches 2, 6, 15.
                // LstLitPsup et LstLitSsup
                if (typBra == Crue9TypeBranche.BRA_2 || typBra == Crue9TypeBranche.STRICKLER || typBra == Crue9TypeBranche.BRA_15) {
                    removeKey(values1, values2, "loiZBeta");
                    removeKey(values1, values2, "lstLitLinf");
                    //voir Dictionnaire de données et STO_STR_RPTG
                    if (!rptgToUseToFindTypBra.isStricklerWith1Lit()) {
                        removeKey(values1, values2, "lstLitLsup");
                        removeKey(values1, values2, "lstLitPosBordure");
                        removeKey(values1, values2, "lstLitPsup");
                        removeKey(values1, values2, "lstLitSsup");
                    }
                } else {
                    //  LoiZLmin et LoiZSmin pour les sections des branches autres que 2, 6, 15,
                    removeKey(values1, values2, "loiZLmin");
                    removeKey(values1, values2, "loiZSmin");
                }
                //LoiZDact pour les sections des branches 2 et 15,
                if (typBra == Crue9TypeBranche.BRA_2 || typBra == Crue9TypeBranche.BRA_15) {
                    removeKey(values1, values2, "loiZDact");
                }
            }
            Set<String> props = new HashSet<>();
            props.addAll(values1.keySet());
            props.addAll(values2.keySet());
            ResultatTest tmp = new ResultatTest();
            List<String> orderedProp = new ArrayList<>(props);
            Collections.sort(orderedProp);
            double epsilonRelative = ComparaisonSelectorContent.getEpsilonRelativeRPTValue(values1IsC9, values2IsC9, beanTester.getCCM());
            context.setDoubleEpsilonRelative(epsilonRelative);
            isEquals = beanTester.isSame(values1, values2, tmp, context, orderedProp);
            if (!isEquals) {
                final List<ResultatTest> fils = tmp.getFils();
                if (!fils.get(0).getObjetA().getClass().equals(LazyDynaMap.class)) {
                    res.addDiffs(fils);
                } else {
                    res.addDiffs(fils.get(0).getFils());
                }
            }
        } catch (Exception e) {
            context.addError(e.getMessage(), e);
        }

        return isEquals;
    }

    private void removeKey(Map<String, Object> values1, Map<String, Object> values2, final String loiZLmin) {
        values1.remove(loiZLmin);
        values2.remove(loiZLmin);
    }
}
