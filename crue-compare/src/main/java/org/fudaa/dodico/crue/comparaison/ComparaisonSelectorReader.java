/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.PredicateUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.comparaison.io.ReaderSelector;
import org.fudaa.dodico.crue.comparaison.io.SelectorItem;
import org.fudaa.dodico.crue.comparaison.io.SelectorItemContainer;

import java.io.File;
import java.net.MalformedURLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Frederic Deniger
 */
public class ComparaisonSelectorReader {

  public static final String C9C9_FILE = "comparaisons/c9c9.xml";
  public static final String C9C10_FILE = "comparaisons/c9c10.xml";
  public static final String C10C10_FILE = "comparaisons/c10c10.xml";
  private final Set<String> knownIds;

  public ComparaisonSelectorReader(Set<String> knownIds) {
    this.knownIds = knownIds;
  }

  public Map<ComparaisonSelectorEnum, String> getFiles() {
    Map<ComparaisonSelectorEnum, String> res = new EnumMap<>(ComparaisonSelectorEnum.class);
    res.put(ComparaisonSelectorEnum.C9_C9, C9C9_FILE);
    res.put(ComparaisonSelectorEnum.C9_C10, C9C10_FILE);
    res.put(ComparaisonSelectorEnum.C10_C10, C10C10_FILE);
    return res;
  }

  Predicate create(SelectorItemContainer content) {
    if (content == null || content.getItems() == null) {
      return null;
    }
    Set<String> rejectedIds = new HashSet<>();
    List<SelectorItem> items = content.getItems();
    for (SelectorItem selectorItem : items) {
      rejectedIds.add(selectorItem.getTestId());
    }
    return new ComparaisonSelectorPredicate(rejectedIds);

  }

  public CrueOperationResult<ComparaisonSelectorContent> readAndValid(File siteDir) {
    ComparaisonSelectorContent container = new ComparaisonSelectorContent();
    CtuluLogGroup groupe = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    groupe.setDescription("comparaisonFilters.log");
    if (!siteDir.exists()) {
      addLogForDirNotExisting(siteDir, groupe);
    } else {
      read(siteDir, ComparaisonSelectorEnum.C9_C9, groupe, container);
      read(siteDir, ComparaisonSelectorEnum.C9_C10, groupe, container);
      read(siteDir, ComparaisonSelectorEnum.C10_C10, groupe, container);
    }
    return new CrueOperationResult<>(container, groupe);
  }

  public CrueOperationResult<ComparaisonSelectorContent> readAndValid(File siteDir, ComparaisonSelectorEnum type) {
    ComparaisonSelectorContent container = new ComparaisonSelectorContent();
    CtuluLogGroup groupe = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    groupe.setDescription("comparaisonFilters.log");
    if (!siteDir.exists()) {
      addLogForDirNotExisting(siteDir, groupe);
    } else {
      read(siteDir, type, groupe, container);
    }
    return new CrueOperationResult<>(container, groupe);
  }

  private void read(File siteDir, ComparaisonSelectorEnum type, CtuluLogGroup target, ComparaisonSelectorContent container) {
    CrueIOResu<Predicate> io = read(siteDir, type);
    if (io.getAnalyse() != null) {
      target.addLog(io.getAnalyse());
    }
    if (io.getMetier() != null) {
      container.addPredicate(type, io.getMetier());
    }

  }

  private CrueIOResu<Predicate> read(File siteDir, ComparaisonSelectorEnum type) {
    String file = getFiles().get(type);
    File targetFile = new File(siteDir, file);
    if (targetFile.exists()) {
      try {
        final ReaderSelector readerSelector = new ReaderSelector();
        SelectorItemContainer read = readerSelector.read(targetFile.toURI().toURL());
        if (read != null) {
          List<SelectorItem> items = read.getItems();
          if (items != null) {
            for (SelectorItem selectorItem : items) {
              if (!knownIds.contains(selectorItem.getTestId())) {
                readerSelector.getAnalyze().addWarn("testSelector.readTest.unknown", selectorItem.getTestId());
              }
            }
          }
        }
        return new CrueIOResu<>(create(read), readerSelector.getAnalyze());
      } catch (MalformedURLException malformedURLException) {
        Logger.getLogger(ComparaisonSelectorReader.class.getName()).log(Level.INFO, "message {0}", malformedURLException);
      }
    }
    CrueIOResu<Predicate> res = new CrueIOResu<>();
    res.setMetier(PredicateUtils.truePredicate());
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc(targetFile.getName());
    log.addWarn("FileEtcComparaisonNotFound.warning", targetFile.getAbsolutePath());
    res.setAnalyse(log);
    return res;
  }

  private void addLogForDirNotExisting(File siteDir, CtuluLogGroup groupe) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("FolderEtcComparaisonNotFound.log");
    log.addWarn("FolderEtcComparaisonNotFound.warning", siteDir == null ? "?" : siteDir.getAbsolutePath());
    groupe.addLog(log);
  }
}
