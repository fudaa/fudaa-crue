/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

public class TesterContextDigg {

  /**
   * Le nombre de beanObjet à parcourir pour déterminer l'égalite de 2 objets. Une valeur de 1 indique que l'on ne suit pas les objet références par
   * l'objet en cours.
   */
  private final int maxDeep;
  private int actualDeep;
  private boolean continueDigg = true;

  protected TesterContextDigg(int maxDeep) {
    this.maxDeep = maxDeep;
    this.actualDeep = maxDeep;
  }

  protected TesterContextDigg(TesterContextDigg parent) {
    this.maxDeep = parent.maxDeep;
    this.actualDeep = parent.actualDeep;
  }


  public final boolean isObjectAlreadyTested(final Object o) {
    return false;
  }

  /**
   * @return true si le comparateur peut continuer à creuser les objets
   */
  public final boolean canDigg() {
    return continueDigg && (maxDeep < 0 || actualDeep > 0);
  }

  /**
   * Indique au contexte que l'on a creuse la
   */
  public final void diggDone() {
    actualDeep--;
  }

  /**
   * Force l'arret du parcourt de l'arbre d'objet: a utiliser si cycle dans les objets
   */
  public void stopDigg() {
    continueDigg = false;
  }
}
