package org.fudaa.dodico.crue.comparaison.config;

import java.util.ArrayList;
import java.util.List;

public class ConfSelection {

  private List<ConfSelectionItem> items = new ArrayList<>();

  public List<ConfSelectionItem> getItems() {
    return items;
  }

  public void setItems(List<ConfSelectionItem> items) {
    this.items = items;
  }

}
