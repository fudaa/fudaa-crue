/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.config;

import java.util.List;

public class AbstractConfCompare implements ConfCompare {
  private List<ConfOption> options;

  /**
   * @return the options
   */
  public List<ConfOption> getOptions() {
    return options;
  }

  /**
   * @param options the options to set
   */
  public void setOptions(List<ConfOption> options) {
    this.options = options;
  }
}
