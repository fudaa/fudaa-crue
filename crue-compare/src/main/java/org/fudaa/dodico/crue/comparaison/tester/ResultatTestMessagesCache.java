/*
 GPL 2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Frederic Deniger
 */
public class ResultatTestMessagesCache {
  private final static ResultatTestMessagesCache INSTANCE = new ResultatTestMessagesCache();

  public static ResultatTestMessagesCache getInstance() {
    return INSTANCE;
  }

  private Map<String, String> cache = new HashMap<>();

  public void setUseCache(boolean useCache) {
    if (useCache) {
      if (cache == null) {
        cache = new HashMap<>();
      }
    } else {
      cache = null;
    }
  }

  protected String getInCache(String key) {
    return cache.get(key);
  }

  public final String getMsg(String msg, final Object... msgArguments) {
    if (cache == null || ArrayUtils.isNotEmpty(msgArguments)) {
      return BusinessMessages.getString(msg, msgArguments);
    }
    return cache.computeIfAbsent(msg, s -> BusinessMessages.getStringOrDefault(msg, msg));
  }
}
