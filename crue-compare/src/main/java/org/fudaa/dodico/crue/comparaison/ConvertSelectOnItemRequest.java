/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.apache.commons.jxpath.JXPathContext;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.comparaison.config.ConfSelectionItem;
import org.fudaa.dodico.crue.comparaison.config.ConfSelectionItemRequete;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConvertSelectOnItemRequest implements ConvertSelectOnItem {

  @Override
  public List<ComparaisonNodeFinal> compute(final List<ComparaisonNodeFinal> in, final ConfSelectionItem requete)
      throws Exception {
    return computeOn(in, (ConfSelectionItemRequete) requete);
  }

  List<ComparaisonNodeFinal> computeOn(final List<ComparaisonNodeFinal> in, final ConfSelectionItemRequete requete) {
    final List<ComparaisonNodeFinal> res = new ArrayList<>();
    for (final ComparaisonNodeFinal nodeFinal : in) {
      computeOn(nodeFinal, requete, res);
    }
    return res;
  }

  private final static Logger LOGGER = Logger.getLogger(ConvertSelectOnItemRequest.class.getName());

  private void computeOn(final ComparaisonNodeFinal in, final ConfSelectionItemRequete requete,
      final List<ComparaisonNodeFinal> res) {
    ComparaisonGroup gr = null;
    // si c'est le noeud root on ne peut pas lui créer un noeud parent.
    if (!in.isRoot) {
      gr = new ComparaisonGroup();
      gr.parent = in.parent;
      gr.objetA = in.objetA;
      gr.objetB = in.objetB;
      gr.printForThisLevel = in.printForThisLevel;
      gr.printForFils = in.printForFils;
    }
    final List lA = in.listObjectA;
    final List lB = in.listObjectB;
    if (lA.size() != lB.size()) {
      final ComparaisonNodeFinal newCompar = new ComparaisonNodeFinal();
      newCompar.listObjectA = lA;
      newCompar.listObjectB = lB;
      newCompar.parent = gr;
      newCompar.objetA = lA.size();
      newCompar.objetB = lB.size();
      newCompar.printForThisLevel = null;
      newCompar.printForFils = requete.getParamToPrint();
      newCompar.msg=BusinessMessages.getString("compare.size.isDifferent", lA.size(), lB.size());
      res.add(newCompar);
      return;
    }
    final int taille = lA.size();
    for (int i = 0; i < taille; i++) {
      final Object oa = lA.get(i);
      final Object ob = lB.get(i);
      JXPathContext xPath;
      List selectNodesA = Collections.emptyList();
      List selectNodesB = Collections.emptyList();
      try {
        xPath = JXPathContext.newContext(oa);
        selectNodesA = xPath.selectNodes(requete.getRequest());
        xPath = JXPathContext.newContext(ob);
        selectNodesB = xPath.selectNodes(requete.getRequest());
      } catch (Exception e) {
        LOGGER.log(Level.SEVERE,"computeOn", e);
      }

      final ComparaisonNodeFinal newCompar = new ComparaisonNodeFinal();
      newCompar.listObjectA = selectNodesA;
      newCompar.listObjectB = selectNodesB;
      newCompar.parent = gr;
      newCompar.objetA = oa;
      newCompar.objetB = ob;
      newCompar.printForThisLevel = in.printForFils;
      newCompar.printForFils = requete.getParamToPrint();
      res.add(newCompar);
      if (gr != null) {
        gr.fils.add(newCompar);
      }
    }

  }
}
