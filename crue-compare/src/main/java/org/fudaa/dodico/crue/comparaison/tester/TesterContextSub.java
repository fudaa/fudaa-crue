/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.metier.helper.DynamicPropertyFilter;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;

import java.util.Collection;

public class TesterContextSub extends TesterContextDigg implements TesterContext {

  private final TesterContextImpl parent;

  public TesterContextSub(TesterContextImpl parent) {
    super(parent);
    this.parent = parent;
  }

  private TesterContextSub(TesterContextImpl parent, TesterContextSub current) {
    super(current);
    this.parent = parent;
  }

  @Override
  public double getDoubleEpsilonRelative() {
    return parent.getDoubleEpsilonRelative();
  }

  @Override
  public void setDoubleEpsilonRelative(double doubleEpsilonRelative) {
    parent.setDoubleEpsilonRelative(doubleEpsilonRelative);
  }
  
  

  @Override
  public void addError(String msg) {
    parent.addError(msg);
  }

  @Override
  public void addError(String msg, Object... data) {
    parent.addError(msg, data);
  }

  @Override
  public void addError(String msg, Throwable e) {
    parent.addError(msg, e);
  }

  @Override
  public void addErrorThrown(String msg, Throwable e, Object... data) {
    parent.addErrorThrown(msg, e, data);
  }

  @Override
  public void addFatalError(String m) {
    parent.addFatalError(m);
  }

  @Override
  public void addFatalError(String m, Object... arg) {
    parent.addFatalError(m, arg);
  }

  @Override
  public void addInfo(String msg) {
    parent.addInfo(msg);
  }

  @Override
  public void addInfo(String msg, Object... args) {
    parent.addInfo(msg, args);
  }

  @Override
  public void addWarn(String msg) {
    parent.addWarn(msg);
  }

  @Override
  public void addWarn(String msg, Object... args) {
    parent.addWarn(msg, args);
  }

  @Override
  public TesterContext createSubTesterContext() {
    return new TesterContextSub(parent, this);
  }

  @Override
  public boolean equals(Object obj) {
    return parent.equals(obj);
  }

  @Override
  public CtuluLog getError() {
    return parent.getError();
  }

  @Override
  public DynamicPropertyFilter getResultatFilter() {
    return parent.getResultatFilter();
  }

  @Override
  public ToStringTransformer getToStringTransformer() {
    return parent.getToStringTransformer();
  }

  @Override
  public int hashCode() {
    return parent.hashCode();
  }

  /**
   * @return @see org.fudaa.dodico.crue.comparaison.tester.TesterContextImpl#isIgnoreCaseForStringComparison()
   */
  @Override
  public boolean isIgnoreCaseForStringComparison() {
    return parent.isIgnoreCaseForStringComparison();
  }

  @Override
  public boolean isIgnoreComment() {
    return parent.isIgnoreComment();
  }

  @Override
  public boolean isActivated(TestOption option) {
    return parent.isActivated(option);
  }

  /**
   * @return @see org.fudaa.dodico.crue.comparaison.tester.TesterContextImpl#isTrimStringForComparison()
   */
  @Override
  public boolean isTrimStringForComparison() {
    return parent.isTrimStringForComparison();
  }

  @Override
  public void manageException(Exception e) {
    parent.manageException(e);
  }

  @Override
  public void manageException(Exception e, String msg) {
    parent.manageException(e, msg);
  }

  @Override
  public void setError(CtuluLog error) {
    parent.setError(error);
  }

  @Override
  public String toString() {
    return parent.toString();
  }

  /**
   * Do not use properties form parent context.
   */
  @Override
  public Collection<String> getPropToCompare() {
    return null;
  }

  /**
   * Do not use properties form parent context.
   */
  @Override
  public Collection<String> getPropToIgnore() {
    return null;
  }
}
