/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.fudaa.dodico.crue.comparaison.ComparaisonSelectorContent;
import org.fudaa.dodico.crue.metier.emh.AbstractResPrtCIni;

/**
 * redefini pour utilise l'epsilon relative associé.
 *
 * @author deniger
 */
@SuppressWarnings("unchecked")
public class EqualsTesterRPTI extends AbstractEqualsTester<AbstractResPrtCIni> {
    private final EqualsTesterBean beanTester;

    public EqualsTesterRPTI(FactoryEqualsTester equalsTester) {
        super();
        this.beanTester = new EqualsTesterBean(equalsTester);
    }

    @Override
    protected boolean mustTestAlreadyDone() {
        return false;
    }

    @Override
    public boolean isSameSafe(final AbstractResPrtCIni o1, final AbstractResPrtCIni o2, final ResultatTest res,
                              final TesterContext context) {
        boolean isEquals = true;
        try {
            boolean values1IsC9 = o1.isC9();
            boolean values2IsC9 = o2.isC9();
            double epsilonRelative = ComparaisonSelectorContent.getEpsilonRelativeRPTValue(values1IsC9, values2IsC9, beanTester.getCCM());
            context.setDoubleEpsilonRelative(epsilonRelative);
            isEquals = beanTester.isSame(o1, o2, res, context);
        } catch (Exception e) {
            context.addError(e.getMessage(), e);
        }
        return isEquals;
    }
}
