/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

/**
 *
 * @author Frederic Deniger
 */
public enum ComparaisonSelectorEnum {

  /**
   * dans le cas de comparaison C9_C9*
   */
  C9_C9,
  /**
   * dans le cas de comparaison C9_C10*
   */
  C9_C10,
  /**
   * dans le cas de comparaison C10_C10*
   */
  C10_C10
}
