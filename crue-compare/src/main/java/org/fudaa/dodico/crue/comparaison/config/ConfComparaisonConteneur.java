/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.config;

import java.util.ArrayList;
import java.util.List;

public class ConfComparaisonConteneur {

  private List<ConfComparaison> comparaisons = new ArrayList<>();

  public void setComparaisons(List<ConfComparaison> comparaisons) {
    this.comparaisons = comparaisons;
  }

  public List<ConfComparaison> getComparaisons() {
    return comparaisons;
  }

}
