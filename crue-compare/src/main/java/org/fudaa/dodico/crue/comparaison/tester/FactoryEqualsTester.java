/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.metier.emh.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author deniger
 */
@SuppressWarnings("unchecked")
public class FactoryEqualsTester {
    private static final EqualsTesterObject BASIC_OBJECT_TESTER = new EqualsTesterObject();
    private static final EqualsTesterString STRING_TESTER = new EqualsTesterString();
    private final CrueConfigMetier propertyDefinition;
    private final Map<Class, EqualsTesterItemBuilder> knownTesterBuilder;
    private final Map<Class, EqualsTester> knownTester;
    private final Map<Class, EqualsTester> mainKnownTester;
    private final int maxRecordToRead;
    private final int maxDiffByEMHOnResultat;
    private final Map<String, String> propToString = new HashMap<>();

    /**
     * @param propEps la correspondance propertyName->Epsilon
     */
    public FactoryEqualsTester(final CrueConfigMetier propEps, int maxRecordToRead, int maxDiffByEMHOnResultat) {
        super();
        this.maxRecordToRead = maxRecordToRead;
        this.maxDiffByEMHOnResultat = maxDiffByEMHOnResultat;
        this.propertyDefinition = propEps;
        knownTesterBuilder = new HashMap<>();
        knownTester = createKnownEqualsTester(this);
        mainKnownTester = createMainKnownEqualsTester(this);
        knownTester.putAll(mainKnownTester);
        EqualsTesterItemBuilder.addDefault(knownTesterBuilder);
    }

    private static Map<Class, EqualsTester> createKnownEqualsTester(final FactoryEqualsTester factory) {

        final Map<Class, EqualsTester> res = new HashMap<>();
        res.put(String.class, STRING_TESTER);
        res.put(Boolean.class, BASIC_OBJECT_TESTER);

        final EqualsTesterDouble abs = new EqualsTesterDouble();
        abs.setPropertyDefinition(factory.getProperty("dx").getNature(), true);
        final EqualsTesterDouble ord = new EqualsTesterDouble();
        ord.setPropertyDefinition(factory.getProperty("dz").getNature(), true);
        res.put(PtProfil.class, new EqualsTesterPoint2D(abs, ord));
        final EqualsTesterFente fente = new EqualsTesterFente(new EqualsTesterDouble(factory.getProperty("largeurFente"), true),
                new EqualsTesterDouble(factory.getProperty("profondeurFente"), true));
        res.put(DonPrtGeoProfilSectionFenteData.class, fente);

        return res;
    }

    /**
     * @param clazz la classe a tester
     * @return true si du type enum.
     */
    private static boolean isEnumType(final Class<?> clazz) {
        return (clazz.isEnum() || (clazz.getSuperclass() != null && clazz.getSuperclass().isEnum()));
    }

    public CrueConfigMetier getPropertyDefinition() {
        return propertyDefinition;
    }

    public String getStringOrDefault(String propName) {
        if (propName == null) {
            return null;
        }
        String res = propToString.get(propName);
        if (res == null) {
            res = BusinessMessages.getStringOrDefault(propName, StringUtils.capitalize(propName));
            propToString.put(propName, res);
        }
        return res;
    }

    private Map<Class, EqualsTester> createMainKnownEqualsTester(final FactoryEqualsTester factory) {
        final Map<Class, EqualsTester> res = new HashMap<>();
        final EqualsTesterLoi loiTester = new EqualsTesterLoi(factory);
        res.put(LoiDF.class, loiTester);
        res.put(LoiFF.class, loiTester);
        res.put(ResultatPasDeTemps.class, new EqualsTesterResultatCrue9PasDeTemps(factory.getEpsFor("pdt"),
                new EqualsTesterDouble(factory.getProperty("qruis"), true),
                factory.getPropertyDefinition()));
        res.put(ResultatCalcul.class, new EqualsTesterResultatCalcul(factory, maxDiffByEMHOnResultat));
        res.put(ResPrtGeo.class, new EqualsTesterRPTG(factory));
        res.put(ResPrtCIniCasier.class, new EqualsTesterRPTI(factory));
        res.put(ResPrtCIniSection.class, new EqualsTesterRPTI(factory));

        res.put(ValParamDouble.class, new EqualsTesterValParamDouble(factory));
        res.put(ValParamEntier.class, new EqualsTesterValParamEntier(factory));
        res.put(ValParamBooleen.class, new EqualsTesterValParamBoolean(factory));
        res.put(CompteRendu.class, new EqualsTesterCompteRendu(factory, maxRecordToRead));
        return res;
    }

    /**
     * Used to test directly objet.
     *
     * @param o l'objet a tester
     */
    public EqualsTester getMainKnownTester(Object o) {
        if (o == null) {
            return null;
        }
        return mainKnownTester.get(o.getClass());
    }

    /**
     * Used to test directly objet.
     *
     * @return le testeur pour {@link LoiDF}
     */
    public EqualsTester getLoiDFTester() {
        return mainKnownTester.get(LoiDF.class);
    }

    /**
     * @param id l'identifiant du parametre
     * @return l'epsilon cherche
     */
    private PropertyEpsilon getEpsFor(final String id) {
        return (propertyDefinition == null || !propertyDefinition.isPropertyDefined(id)) ? PropertyEpsilon.DEFAULT
                : propertyDefinition.getProperty(id).getEpsilon();
    }

    /**
     * @param id l'identifiant du parametre
     * @return l'epsilon cherche
     */
    public ItemVariable getProperty(final String id) {
        return propertyDefinition.getProperty(id);
    }

    private EqualsTester getCollectionTester(final Class parentClass, final Class propClass, final String prop, CtuluLog log) {
        if (propClass.isArray()) {
            if (propClass.isAssignableFrom(double[].class)) {
                return new EqualsTesterArrayDouble(
                        EqualsTesterItemBuilderDefaults.DOUBLE_TESTER.build(this, prop, log));
            }
            if (propClass.isAssignableFrom(int[].class)) {
                return new EqualsTesterArrayInteger(
                        EqualsTesterItemBuilderDefaults.INTEGER_TESTER.build(this, prop, log));
            }
            if (propClass.isAssignableFrom(int[].class)) {
                return new EqualsTesterArrayLong(
                        EqualsTesterItemBuilderDefaults.LONG_TESTER.build(this, prop, log));
            }
            return new EqualsTesterArray(this, prop);
        } else if (Collection.class.isAssignableFrom(propClass)) {
            return new EqualsTesterCollection(this, prop);
        }
        return null;
    }

    /**
     * @param parentClass classe parente
     * @param propClass   la classe de la propriete
     * @param prop        nom de la propriété
     * @return le tester demande. Ne renvoie jamais null.
     */
    public EqualsTester buildTesterFor(final Class parentClass, final Class propClass, final String prop, CtuluLog log) {
        EqualsTester res = getKnownEqualsTester(propClass);
        if (res != null) {
            return res;
        }
        res = getCollectionTester(parentClass, propClass, prop, log);
        if (res != null) {
            return res;
        }
        if (isEnumType(propClass)) {
            return FactoryEqualsTester.BASIC_OBJECT_TESTER;
        }
        final EqualsTesterItemBuilder builder = knownTesterBuilder.get(propClass);
        if (builder != null) {
            return builder.build(this, prop, log);
        }
        return new EqualsTesterBean(this);
    }

    /**
     * @param propClass LoiDF,LoiFF,String et PtProfil
     * @return le tester d'egalité associe
     */
    public EqualsTester getKnownEqualsTester(final Class propClass) {
        return knownTester.get(propClass);
    }
}
