/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author deniger
 */
public class ResultatTest {
  private List<ResultatTest> fils;
  private List<ResultatTest> filsExt;
  private String msg;
  private Object[] msgArguments;
  private int nbObjectTested;
  private Object objetA;
  private Object objetB;
  private Object printA;
  private Object printB;
  private String propertyTested;
  private boolean same = true;
  private String traductedMsg;
  private boolean isOrderDiff;

  public ResultatTest() {
    super();
  }

  public ResultatTest(final Object a, final Object b, final String msg) {
    objetA = a;
    objetB = b;
    setMsg(msg);
  }

  public ResultatTest(final Object a, final Object b, final String msg, final Object... args) {
    objetA = a;
    objetB = b;
    setMsg(msg, args);
  }

  public void addDiff(final ResultatTest item) {
    addFils(item);
    item.same = false;
  }

  public void replaceDiffFilsOf(final ResultatTest other) {
    if (fils != null) {
      fils.clear();
    }
    for (final ResultatTest newFils : other.getFils()) {
      addDiff(newFils);
    }
  }

  public void addDiffs(final Collection<ResultatTest> items) {
    if (items == null) {
      return;
    }
    for (final ResultatTest newFils : items) {
      addFils(newFils);
      newFils.same = false;
    }
  }

  public int getNbDifferences() {
    int res = 0;
    final List<ResultatTest> filsTest = getFils();
    if (filsTest.isEmpty()) {
      return isSame() ? 0 : 1;
    }
    for (final ResultatTest resultatTest : filsTest) {
      res += resultatTest.getNbDifferences();
    }
    return res;
  }

  public void addDiffsAndUpdatePrint(final Collection<ResultatTest> items, final ToStringTransformer toStringTransformer) {
    addDiffs(items);
    for (final ResultatTest resultatTest : items) {
      resultatTest.updatePrint(toStringTransformer);
    }
  }

  public void addFils(final ResultatTest item) {
    if (fils == null) {
      fils = new ArrayList<>();
    }
    fils.add(item);
  }

  public void addGroup(final ResultatTest item) {
    addFils(item);
  }

  public List<ResultatTest> getFils() {
    if (fils == null) {
      return Collections.emptyList();
    }
    if (filsExt == null) {
      filsExt = Collections.unmodifiableList(fils);
    }
    return filsExt;
  }

  public String getMsg() {
    if (msg == null) {
      updateTraduction();
    }
    return msg;
  }

  public Object[] getMsgArguments() {
    return msgArguments;
  }

  /**
   * @return the nbObjectTested
   */
  public int getNbObjectTested() {
    return nbObjectTested;
  }

  public Object getObjetA() {
    return objetA;
  }

  public Object getObjetB() {
    return objetB;
  }

  /**
   * @return the printA
   */
  public Object getPrintA() {
    return printA;
  }

  /**
   * @return the printB
   */
  public Object getPrintB() {
    return printB;
  }

  /**
   * @return the propertyTested
   */
  public String getPropertyTested() {
    return propertyTested;
  }

  /**
   * @return the traductedMsg
   */
  public String getTraductedMsg() {
    updateTraduction();
    return traductedMsg;
  }

  public void incrementeObjectTested() {
    nbObjectTested++;
  }

  public boolean isEmpty() {
    return CollectionUtils.isEmpty(fils);
  }

  /**
   * @return the same
   */
  public boolean isSame() {
    return same;
  }

  private boolean isWellKnownType(final Class c) {
    return String.class.equals(c) || Double.class.equals(c) || Integer.class.equals(c);
  }

  public final void setMsg(final String msg) {
    setMsg(msg, (Object[]) null);
  }

  public final void setMsg(final String msg, final Object... msgArguments) {
    this.msg = msg;
    this.msgArguments = msgArguments;
  }

  private void updateTraduction() {
    if (msg != null) {
      this.traductedMsg = ResultatTestMessagesCache.getInstance().getMsg(msg, msgArguments);
    } else {
      traductedMsg = StringUtils.EMPTY;
    }
  }

  /**
   * @param nbObjectTested the nbObjectTested to set
   */
  public void setNbObjectTested(final int nbObjectTested) {
    this.nbObjectTested = nbObjectTested;
  }

  public void setObjetA(final Object objetA) {
    this.objetA = objetA;
  }

  public void setObjetB(final Object objetB) {
    this.objetB = objetB;
  }

  /**
   * @param printA the printA to set
   */
  public void setPrintA(final Object printA) {
    this.printA = printA;
  }

  /**
   * @param printB the printB to set
   */
  public void setPrintB(final Object printB) {
    this.printB = printB;
  }

  /**
   * @param propertyTested the propertyTested to set
   */
  public void setPropertyTested(final String propertyTested) {
    this.propertyTested = propertyTested;
  }

  /**
   * @param same the same to set
   */
  public void setSame(final boolean same) {
    this.same = same;
  }

  public void updatePrint(final ToStringTransformer toStringTransformer) {
    if (getPrintA() == null) {
      setPrintA(objectToString(toStringTransformer, objetA));
    }
    if (getPrintB() == null) {
      setPrintB(objectToString(toStringTransformer, objetB));
    }
  }

  private String objectToString(final ToStringTransformer toStringTransformer, final Object objetB2) {
    if (objetB2 instanceof ObjetWithID) {
      return (((ObjetNomme) objetB2).getNom());
    } else if (toStringTransformer != null) {
      return (toStringTransformer.transform(objetB2));
    } else if (objetB2 != null) {
      return (String) (isWellKnownType(objetB2.getClass()) ? objetB2 : objetB2.getClass().getSimpleName()
          + ": " + objetB2);
    }
    return StringUtils.EMPTY;
  }

  public boolean updateSameInArbo() {
    if (!isSame()) {
      return false;
    }
    if (fils == null) {
      return true;
    }
    for (final ResultatTest sousRes : fils) {
      if (!sousRes.updateSameInArbo()) {
        same = false;
      }
    }
    return same;
  }

  /**
   * @return the isOrderDiff
   */
  public boolean isOrderDiff() {
    return isOrderDiff;
  }

  /**
   * @param isOrderDiff the isOrderDiff to set
   */
  public void setIsOrderDiff(final boolean isOrderDiff) {
    this.isOrderDiff = isOrderDiff;
  }

  void clearTraduction() {
    traductedMsg = null;
  }

  private boolean comparaisonStopped;

  public void setComparaisonStopped() {
    comparaisonStopped = true;
  }

  public boolean isComparaisonStopped() {
    return comparaisonStopped;
  }
}
