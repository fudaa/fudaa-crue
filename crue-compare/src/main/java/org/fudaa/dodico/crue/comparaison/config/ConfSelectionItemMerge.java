/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.config;

import org.apache.commons.lang3.StringUtils;

public class ConfSelectionItemMerge extends AbstractConfSelectionItem {

  private String attribut;
  private boolean errorIfDifferentSize;

  public String getAttribut() {
    return attribut;
  }

  public void setAttribut(String attribut) {
    this.attribut = StringUtils.trim(attribut);
  }

  private ConfSelectionItemRequete request;

  public ConfSelectionItemRequete getRequest() {
    return request;
  }

  public void setRequest(ConfSelectionItemRequete request) {
    this.request = request;
  }

  /**
   * @return the errorIfDifferentSize
   */
  public boolean isErrorIfDifferentSize() {
    return errorIfDifferentSize;
  }

  /**
   * @param errorIfDifferentSize the errorIfDifferentSize to set
   */
  public void setErrorIfDifferentSize(boolean errorIfDifferentSize) {
    this.errorIfDifferentSize = errorIfDifferentSize;
  }

}
