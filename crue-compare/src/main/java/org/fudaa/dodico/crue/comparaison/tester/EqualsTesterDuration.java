/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.joda.time.Duration;

/**
 * Comparateur de duration.
 * 
 * @author deniger
 */
public class EqualsTesterDuration extends AbstractTemplateEqualsTesterDouble<Duration> {

  @Override
  public boolean isSameSafe(final Duration o1, final Duration o2, ResultatTest res, TesterContext context) {
    return isSameDouble(o1.getMillis() / 1000d, o2.getMillis() / 1000d);
  }

}
