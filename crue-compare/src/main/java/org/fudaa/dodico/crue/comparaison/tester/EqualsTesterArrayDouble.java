/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;


/**
 * @author deniger
 */
public class EqualsTesterArrayDouble extends AbstractEqualsTester<double[]> {

  private final EqualsTesterDouble objTester;

  /**
   * @param objTester le tester
   */
  public EqualsTesterArrayDouble(final EqualsTesterDouble objTester) {
    super();
    this.objTester = objTester;
  }

  @Override
  protected boolean mustTestAlreadyDone() {
    return false;
  }

  @Override
  public boolean isSameSafe(final double[] o1, final double[] o2, final ResultatTest res, TesterContext context) {
    final int length = o1.length;
    if (length != o2.length) {
      if (res != null) {
        res.addDiff(new ResultatTest(length, o2.length, EqualsTesterCollection.TAILLE_DIFF));
      }
      return false;
    }
    for (int idx = 0; idx < length; idx++) {
      if (!objTester.isSameDouble(o1[idx], o2[idx])) {
        if (res != null) {
          res.addDiff(new ResultatTest(o1[idx], o2[idx], "comparaison.collection.item", idx));
        }
        return false;

      }

    }

    return true;

  }

}
