/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;

import java.util.Collection;
import java.util.Iterator;

/**
 * Methode utilitaire pour comparer des données
 *
 * @author deniger
 */
public final class TesterHelper {
    private TesterHelper() {
    }

    /**
     * @param toCompare la liste d'objets a comparer
     * @param ccm       le {@link CrueConfigMetier}
     * @return true si null, de taille 1 ou si tous les elements sont egaux.
     */
    public static boolean isSame(final Collection toCompare, final CrueConfigMetier ccm, int maxRecordToRead, int maxDiffByEMHOnResultat) {
        if (CollectionUtils.isEmpty(toCompare) || toCompare.size() == 1) {
            return true;
        }
        final Iterator it = toCompare.iterator();
        final Object first = it.next();
        EqualsTesterBean tester = new EqualsTesterBean(new FactoryEqualsTester(ccm, maxRecordToRead, maxDiffByEMHOnResultat));
        while (it.hasNext()) {
            final Object obji = it.next();
            try {
                if (!tester.isSame(first, obji)) {
                    return false;
                }
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }
}
