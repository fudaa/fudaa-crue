/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.PredicateUtils;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.comparaison.config.ConfComparaisonConteneur;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

import java.io.File;

/**
 * @author Frederic Deniger
 */
public class ComparaisonSelectorLoader {
  private final File siteDir;
  private final ConfComparaisonConteneur comparisons;

  public ComparaisonSelectorLoader(File baseDir, ConfComparaisonConteneur comparisons) {
    this.siteDir = baseDir;
    this.comparisons = comparisons;
  }

  public CrueOperationResult<ComparaisonSelectorContent> read() {
    return createReader().readAndValid(siteDir);
  }

  public CrueOperationResult<Predicate> getPredicateToUse(ManagerEMHScenario scenarioA, ManagerEMHScenario scenarioB) {
    ComparaisonSelectorEnum comparaisonType = ComparaisonSelectorContent.retrieveEnum(scenarioA, scenarioB);
    CrueOperationResult<ComparaisonSelectorContent> selector = createReader().readAndValid(siteDir, comparaisonType);
    ComparaisonSelectorContent selectorContainer = selector.getResult();
    if (selectorContainer == null) {
      selectorContainer = new ComparaisonSelectorContent();
    }
    Predicate predicate = selectorContainer.getPredicate(comparaisonType);
    if (predicate == null) {
      predicate = PredicateUtils.truePredicate();
    }
    return new CrueOperationResult<>(predicate, selector.getLogs());
  }

  private ComparaisonSelectorReader createReader() {
    return new ComparaisonSelectorReader(TransformerHelper.toSetOfId(comparisons.getComparaisons()));
  }
}
