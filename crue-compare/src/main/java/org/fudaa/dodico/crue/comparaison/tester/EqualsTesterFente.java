/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSectionFenteData;

/**
 * @author deniger
 */
public class EqualsTesterFente extends AbstractEqualsTester<DonPrtGeoProfilSectionFenteData> {

  private final EqualsTesterDouble largeurFente;
  private final EqualsTesterDouble profondeurFente;

  /**
   * @param largeurFente le tester pour l'abscisse
   * @param profondeurFente le tester pour l'ordonnée.
   */
  public EqualsTesterFente(final EqualsTesterDouble largeurFente, final EqualsTesterDouble profondeurFente) {
    super();
    this.largeurFente = largeurFente;
    this.profondeurFente = profondeurFente;
  }

  @Override
  protected boolean mustTestAlreadyDone() {
    return false;
  }

  @Override
  public boolean isSameSafe(final DonPrtGeoProfilSectionFenteData o1, final DonPrtGeoProfilSectionFenteData o2,
      final ResultatTest res, final TesterContext context) {
    boolean bool = true;
    if (!largeurFente.isSameDouble(o1.getLargeurFente(), o2.getLargeurFente())) {
      bool = false;
      if (res != null) {
        res.addDiff(new ResultatTest(o1.getLargeurFente(), o2.getLargeurFente(), "compare.property.diff",
            "largeurFente"));
      }
    }
    if (!profondeurFente.isSameDouble(o1.getProfondeurFente(), o2.getProfondeurFente())) {
      bool = false;
      if (res != null) {
        res.addDiff(new ResultatTest(o1.getProfondeurFente(), o2.getProfondeurFente(), "compare.property.diff",
            "profondeurFente"));
      }
    }
    return bool;
  }
}
