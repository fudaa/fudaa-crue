/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.PredicateUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.ItemConstant;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

import java.util.EnumMap;
import java.util.Map;

/**
 *
 * @author Frederic Deniger
 */
public class ComparaisonSelectorContent {

  public static final double DEFAULT_RELATIVE_VALUE = 1e-6;
  private final Map<ComparaisonSelectorEnum, Predicate> predicates = new EnumMap<>(ComparaisonSelectorEnum.class);

  public Predicate getPredicate(ComparaisonSelectorEnum in) {
    final Predicate predicate = predicates.get(in);
    return predicate == null ? PredicateUtils.truePredicate() : predicate;
  }

  void addPredicate(ComparaisonSelectorEnum in, Predicate predicate) {
    predicates.put(in, predicate);
  }

  private static ItemConstant getEpsilonRelativeRPT(boolean projetReferenceIsC9, boolean projetCibleIsC9, CrueConfigMetier ccm) {
    if (projetCibleIsC9 && projetReferenceIsC9) {
      return ccm.getConstante(CrueConfigMetierConstants.EPSCOMPREL_RPT_C9C9);
    }
    if (!projetCibleIsC9 && !projetReferenceIsC9) {
      return ccm.getConstante(CrueConfigMetierConstants.EPSCOMPREL_RPT_C10C10);
    }
    return ccm.getConstante(CrueConfigMetierConstants.EPSCOMPREL_RPT_C9C10);

  }

  public static double getEpsilonRelativeRPTValue(boolean projetReferenceIsC9, boolean projetCibleIsC9, CrueConfigMetier ccm) {
    ItemConstant cst = getEpsilonRelativeRPT(projetReferenceIsC9, projetCibleIsC9, ccm);
    if (cst == null || cst.getConstantValue() == null) {
      return 1e-6;
    }
    return cst.getConstantValue().doubleValue();
  }

  public static double getEpsilonRelativeRCALValue(boolean projetReferenceIsC9, boolean projetCibleIsC9, CrueConfigMetier ccm) {
    ItemConstant cst = getEpsilonRelativeRCAL(projetReferenceIsC9, projetCibleIsC9, ccm);
    if (cst == null || cst.getConstantValue() == null) {
      return DEFAULT_RELATIVE_VALUE;
    }
    return cst.getConstantValue().doubleValue();
  }

  private static ItemConstant getEpsilonRelativeRCAL(boolean projetReferenceIsC9, boolean projetCibleIsC9, CrueConfigMetier ccm) {
    if (projetCibleIsC9 && projetReferenceIsC9) {
      return ccm.getConstante(CrueConfigMetierConstants.EPSCOMPREL_RCAL_C9C9);
    }
    if (!projetCibleIsC9 && !projetReferenceIsC9) {
      return ccm.getConstante(CrueConfigMetierConstants.EPSCOMPREL_RCAL_C10C10);
    }
    return ccm.getConstante(CrueConfigMetierConstants.EPSCOMPREL_RCAL_C9C10);

  }

  public static ComparaisonSelectorEnum retrieveEnum(ManagerEMHScenario projetCible, ManagerEMHScenario projetReference) {
    if (projetCible.isCrue9() && projetReference.isCrue9()) {
      return ComparaisonSelectorEnum.C9_C9;
    }
    if (projetCible.isCrue10() && projetReference.isCrue10()) {
      return ComparaisonSelectorEnum.C10_C10;
    }
    return ComparaisonSelectorEnum.C9_C10;
  }
}
