/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.config;

import org.apache.commons.lang3.StringUtils;

public class ConfSelectionItemRequete extends AbstractConfSelectionItem {
  private String paramToPrint;
  private String request;

  public String getParamToPrint() {
    return paramToPrint;
  }

  public void setParamToPrint(String print) {
    this.paramToPrint = StringUtils.trim(print);
  }

  public String getRequest() {
    return request;
  }

  public void setRequest(String request) {
    this.request = StringUtils.trim(request);
  }

}
