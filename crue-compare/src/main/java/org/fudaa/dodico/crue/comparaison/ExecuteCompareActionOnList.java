/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.comparaison.tester.EqualsTester;
import org.fudaa.dodico.crue.comparaison.tester.ResultatTest;
import org.fudaa.dodico.crue.comparaison.tester.TesterContext;

/**
 * @author deniger
 */
public class ExecuteCompareActionOnList implements CompareActionBuilder {
  
  final EqualsTester tester;
  private final boolean diffOnOrder;

  public ExecuteCompareActionOnList(EqualsTester tester,boolean diffOnOrder) {
    super();
    this.tester = tester;
    this.diffOnOrder=diffOnOrder;
  }

  @Override
  public boolean isDiffOnOrder() {
    return diffOnOrder;
  }
  
  
  

  @Override
  public boolean launch(ComparaisonNodeFinal target, ResultatTest parent, CtuluLog ctuluLog) {
    TesterContext create = tester.getContextFactory().create();
    create.setError(ctuluLog);
    try {
      parent.setNbObjectTested(Math.max(ConvertCompare.getSize(target.listObjectA), ConvertCompare
          .getSize(target.listObjectB)));
      return tester.isSame(target.listObjectA, target.listObjectB, parent, create);

    } catch (Exception e) {
      ctuluLog.manageException(e);
    }
    return false;

  }
}
