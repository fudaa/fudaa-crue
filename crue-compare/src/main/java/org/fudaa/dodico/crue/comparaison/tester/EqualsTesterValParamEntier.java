/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.dodico.crue.metier.emh.ValParamEntier;

/**
 * @author deniger
 */
public class EqualsTesterValParamEntier extends AbstractEqualsTester<ValParamEntier> {

  private final FactoryEqualsTester factory;

  public EqualsTesterValParamEntier(final FactoryEqualsTester factory) {
    this.factory = factory;
  }

  @Override
  protected boolean mustTestAlreadyDone() {
    return false;
  }

  @Override
  public boolean isSameSafe(final ValParamEntier o1, final ValParamEntier o2, final ResultatTest res,
      final TesterContext context) {
    if (!ObjectUtils.equals(o1.getId(), o2.getId())) {
      if (res != null) {
        res.addDiff(new ResultatTest(o1.getNom(), o2.getNom(), "compare.type.diff"));
      }
      return false;
    }
    final EqualsTesterDouble build = EqualsTesterItemBuilderDefaults.DOUBLE_TESTER.build(factory, o1.getNom(), context
        .getError());
    if (context.getError().containsSevereError()) { return false; }
    
    boolean isSame = build.isSameDouble(o1.getValeur(), o2.getValeur());
    if (!isSame && res!=null) {
      final ResultatTest item = new ResultatTest(o1, o2, o1.getNom());
      item.setPrintA(context.getToStringTransformer().transform(o1));
      item.setPrintB(context.getToStringTransformer().transform(o2));
      res.addDiff(item);
    }
    return isSame;

  }
}
