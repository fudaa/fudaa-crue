/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.LoiDF;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformerPoint2d;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author deniger
 */
public class EqualsTesterLoi extends AbstractEqualsTester<Loi> {
  private final FactoryEqualsTester factory;

  public EqualsTesterLoi(final FactoryEqualsTester factory) {
    this.factory = factory;
  }

  @Override
  protected boolean mustTestAlreadyDone() {
    return false;
  }

  @Override
  public boolean isSameSafe(final Loi o1, final Loi o2, final ResultatTest resultatTest, final TesterContext context)
      throws Exception {
    Collection<String> propToCompare = context.getPropToCompare();
    ResultatTest thisResult;
    if (StringUtils.equals(o1.getNom(), o2.getNom())) {
      thisResult = new ResultatTest("", "", o1.getNom());
    } else {
      thisResult = new ResultatTest(o1.getNom(), o2.getNom(), o1.getType().toString());
    }

    final EqualsTesterBean bean = new EqualsTesterBean(factory);
    List<String> props = new ArrayList<>();
    // by default it's true
    boolean testPointAskByUser = true;
    if (propToCompare != null) {
      props.addAll(propToCompare);
      testPointAskByUser = props.contains("evolutionFF");
      //this test is specific for evolutionFF.
      if (testPointAskByUser) {
        props.remove("evolutionFF");
      }
    } else {
      if (o1.getClass().equals(LoiDF.class)) {
        props.add("dateZeroLoiDF");
      }
      props.add("type");
      if (!context.isIgnoreComment()) {
        props.add("commentaire");
      }
    }

    boolean same = bean.isSameSafe(o1, o2, thisResult, context, props);
    // on ne va pas plus loin si types sont différents
    if (!ObjectUtils.equals(o1.getType(), o2.getType())) {
      resultatTest.addDiff(thisResult);
      return false;
    }
    if (testPointAskByUser) {
      final CrueConfigMetier crueProperties = factory.getPropertyDefinition();
      final String nomAbs = crueProperties.getLoiAbscisse(o1).getNom();
      final String nomOrd = crueProperties.getLoiOrdonnee(o1).getNom();
      final ItemVariable propertyAbs = factory.getProperty(nomAbs);
      final ItemVariable propertyOrd = factory.getProperty(nomOrd);
      EqualsTesterDouble abs = null;
      boolean abscisseIsEnum = propertyAbs != null && propertyAbs.getNature().isEnum();
      boolean ordonneeIsEnum = propertyOrd != null && propertyOrd.getNature().isEnum();
      if (!abscisseIsEnum) {
        abs = EqualsTesterItemBuilderDefaults.DOUBLE_TESTER.build(factory, nomAbs, context.getError());
      }
      if (context.getError().containsSevereError()) {
        return false;
      }
      EqualsTesterDouble ord = null;
      if (!ordonneeIsEnum) {
        ord = EqualsTesterItemBuilderDefaults.DOUBLE_TESTER.build(factory, nomOrd, context.getError());
      }
      if (context.getError().containsSevereError()) {
        return false;
      }
      final ToStringTransformerPoint2d toStringTransformerPoint2d = new ToStringTransformerPoint2d(nomAbs, nomOrd, crueProperties,
          DecimalFormatEpsilonEnum.COMPARISON);
      final EqualsTesterPoint2D equalsTesterPoint2D = new EqualsTesterPoint2D(abs, ord);
      if (context.isActivated(TestOption.ONLY_LOIZ_FIRST_POINT)) {//on ne doit comparer que le premier point des loiZ:
        if (o1.getType().isLoiZ()) {
          int nbPt1 = o1.getEvolutionFF().getPtEvolutionFF().size();
          int nbPt2 = o2.getEvolutionFF().getPtEvolutionFF().size();
          //comparaison que si des points sont présents.
          if (nbPt1 > 0 || nbPt2 > 0) {
            if (nbPt1 == 0 || nbPt2 == 0) {
              thisResult.addDiff(new ResultatTest(nbPt1, nbPt2, "comparaison.loiZFirstPoint.empty"));
              same = false;
            } else {
              final PtEvolutionFF pt1 = o1.getEvolutionFF().getPtEvolutionFF().get(0);
              final PtEvolutionFF pt2 = o2.getEvolutionFF().getPtEvolutionFF().get(0);
              final boolean isSame = equalsTesterPoint2D.isSame(pt1, pt2, thisResult, context);
              if (!isSame) {
                same = false;
                thisResult.addDiff(new ResultatTest(toStringTransformerPoint2d.transform(pt1), toStringTransformerPoint2d.transform(pt2),
                    "comparaison.loiZFirstPoint.difference"));
              }
            }
          }
        }
      } else {
        final EqualsTesterCollection ptTester = new EqualsTesterCollection(equalsTesterPoint2D);
        ptTester.setDiffMsg("comparaison.collection.points");
        ptTester.setToStringTransformer(toStringTransformerPoint2d);
        boolean ignoreFirstPoint = context.isActivated(TestOption.LOIZ_DONT_COMPARE_FIRST_POINT) && o1.getType().isLoiZ();
        final boolean isPointEquals = ptTester.isSameSafe(o1.getEvolutionFF().getPtEvolutionFF(), o2.getEvolutionFF().getPtEvolutionFF(),
            thisResult, context, ignoreFirstPoint);
        same = same && isPointEquals;
      }
    }
    if (!same && resultatTest != null) {
      resultatTest.addDiff(thisResult);
    }
    return same;
  }
}
