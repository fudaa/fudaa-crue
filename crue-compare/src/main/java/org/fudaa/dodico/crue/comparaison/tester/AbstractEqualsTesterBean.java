/*
 GPL 2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.emh.InfosEMH;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.Pdt;
import org.fudaa.dodico.crue.metier.emh.PdtCst;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Frederic Deniger
 */
public abstract class AbstractEqualsTesterBean<T> extends AbstractEqualsTester<T> {
  private static final Logger LOGGER = Logger.getLogger(EqualsTesterBean.class.getName());
  private static final String TYPE_DIFF = "compare.type.diff";
  private final FactoryEqualsTester factory;
  private final boolean compareClass;
  private String notDefined;
  private final Collection<String> propToIgnore = new HashSet<>(Arrays.asList("class", "value"));
  private boolean useNameForLoi = true;
  private final Map<Class, String> simpleNames = new HashMap<>();

  AbstractEqualsTesterBean(final FactoryEqualsTester factory, final boolean compareClass) {
    this.factory = factory;
    this.compareClass = compareClass;
  }

  public FactoryEqualsTester getFactory() {
    return factory;
  }

  private static boolean isPropAccepted(final String name, final Collection<String> props) {
    return props == null || !props.contains(name);
  }

  protected ResultatTest addDiff(final Object o1, final ResultatTest res, final String propName, final Object a, final Object b,
                                 final TesterContext context) {
    if (res != null) {
      final CrueConfigMetier crueProperties = factory.getPropertyDefinition();
      final ResultatTest item = new ResultatTest(
          TransformerEMHHelper.formatFromPropertyName(propName, a, crueProperties, DecimalFormatEpsilonEnum.COMPARISON),
          TransformerEMHHelper.formatFromPropertyName(propName, b, crueProperties, DecimalFormatEpsilonEnum.COMPARISON), "compare.property.diff",
          getPrefix(o1), factory.getStringOrDefault(propName));
      item.setPropertyTested(propName);
      item.updatePrint(context.getToStringTransformer());
      res.addDiff(item);
      return item;
    }
    return null;
  }

  @SuppressWarnings(value = "unchecked")
  private boolean compareProp(final Object o1, final ResultatTest res, final String name, final Object a, final Object b,
                              final TesterContext context) throws Exception {
    final boolean isLoi = a instanceof Loi || (a == null && b instanceof Loi);
    if (a == null) {
      if (b != null) {
        final ResultatTest addDiff = addDiff(o1, res, name, null, b, context);
        if (isLoi) {
          addDiff.setPrintA(getPrintForLoi(null, context));
          addDiff.setPrintB(getPrintForLoi(b, context));
        }
        return false;
      }
    } else {
      final EqualsTester tester = getTesterToUse(o1, name, context, a);
      // an error occured:
      if (context.getError().containsSevereError()) {
        return false;
      }
      if (tester == null) {
        return true; // nothing to test.
        // nothing to test.
      }
      // on cree un nouveau contexte pour cette propriete afin de ne pas modifier les compteur pour les autres props.
      ResultatTest subResultat = null;
      if (!(a instanceof PdtCst)) {
        subResultat = new ResultatTest();
      }
      final boolean resB = tester.isSame(a, b, subResultat, context.createSubTesterContext());
      if (!resB) {
        final ResultatTest addDiff = addDiff(o1, res, name, a, b, context);
        // pour les lois, on enleve le premier noeud qui reprend le titre.
        if (isLoi) {
          addDiff.setPrintA(getPrintForLoi(a, context));
          addDiff.setPrintB(getPrintForLoi(b, context));
          if (subResultat!=null && CollectionUtils.isNotEmpty(subResultat.getFils())) {
            subResultat = subResultat.getFils().get(0);
          }
        }
        if (addDiff != null && subResultat != null && subResultat.getFils() != null) {
          addDiff.addDiffsAndUpdatePrint(subResultat.getFils(), context.getToStringTransformer());
        }
        return false;
      }
    }
    return true;
  }

  public CrueConfigMetier getCCM() {
    return factory.getPropertyDefinition();
  }

  private String getNotDefinedMsg() {
    if (notDefined == null) {
      notDefined = BusinessMessages.getString("notDefined.message");
    }
    return notDefined;
  }

  protected String getPrefix(final Object o1) {
    final String string = toString(o1);
    return string.length() == 0 ? string : (string + ": ");
  }

  private String getPrintForLoi(final Object a, final TesterContext context) {
    if (a == null) {
      return getNotDefinedMsg();
    }
    final Loi loi = (Loi) a;
    if (useNameForLoi) {
      return loi.getNom();
    }
    final String i18nKey = "loi.description";
    if (loi.getType() != null && loi.getType().isLoiZ()) {
      final boolean firstPointIgnored = context.isActivated(TestOption.LOIZ_DONT_COMPARE_FIRST_POINT);
      final boolean onlyFirstPoint = context.isActivated(TestOption.ONLY_LOIZ_FIRST_POINT);
      if (firstPointIgnored) {
        return BusinessMessages.getString(i18nKey, loi.getSize() - 1);
      } else if (onlyFirstPoint) {
        return BusinessMessages.getString(i18nKey, 1);
      }
    }
    return BusinessMessages.getString(i18nKey, loi.getSize());
  }

  private Collection<String> getPropToCompare(final Object o1, final Object o2, final TesterContext context) {
    if (context != null && CollectionUtils.isNotEmpty(context.getPropToCompare())) {
      return context.getPropToCompare();
    }
    final PropertyDescriptor[] propertyDescriptors = PropertyUtils.getPropertyDescriptors(o1);
    final List<String> res = new ArrayList<>(propertyDescriptors.length);
    final Collection<String> propToIgnoreInContext = context == null ? null : context.getPropToIgnore();
    for (final PropertyDescriptor propertyDescriptor : propertyDescriptors) {
      final String name = propertyDescriptor.getName();
      if ("class".equals(name) || propertyDescriptor.getReadMethod() == null) {
        continue;
      }
      final UsedByComparison annotation = propertyDescriptor.getReadMethod().getAnnotation(UsedByComparison.class);
      boolean usable = isPropAccepted(name) && isPropAccepted(name, propToIgnoreInContext);
      if (usable) {
        usable = annotation == null || !annotation.ignoreInComparison();
      }
      if (usable) {
        res.add(name);
      }
    }
    return res;
  }

  /**
   * @param parentObject l'objet pareent
   * @param name le nom de la variable
   * @param context le contexte
   * @param init la valeur a tester
   * @return null si le test n'est pas demande par l'utilisateur
   */
  @SuppressWarnings(value = "unchecked")
  private EqualsTester getTesterToUse(final Object parentObject, final String name, final TesterContext context, final Object init) {
    if ("emh".equals(name)) {
      if (parentObject instanceof InfosEMH) {
        return null;
      }
    }
    final EqualsTester tester = factory.buildTesterFor(parentObject.getClass(), init.getClass(), name, context.getError());
    if (context.getError().containsSevereError()) {
      return null;
    }
    final boolean isComplexTest = TesterContextImpl.isComplexEqualsTester(tester);
    if (isComplexTest) {
      // la comparaison a été demandée explicitement par le user.
      final boolean isAskedByUser = isPropertyAskedByUser(name, context);
      // on ne creuse pas plus loin la comparaison si non demande.
      if (!isAskedByUser && !context.canDigg()) {
        return null;
      }
    }
    return tester;
  }

  private List<String> getUsableProperties(final Object o1) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
    final Map m = PropertyUtils.describe(o1);
    final List<String> prop = new ArrayList<>(m.size());
    for (final Object o : m.keySet()) {
      prop.add(ObjectUtils.toString(o));
    }
    return prop;
  }

  private boolean isPropAccepted(final String name) {
    return isPropAccepted(name, propToIgnore);
  }

  private boolean isPropertyAskedByUser(final String name, final TesterContext context) {
    return CollectionUtils.isNotEmpty(context.getPropToCompare()) && context.getPropToCompare().contains(name);
  }

  public final boolean isSame(final T o1, final T o2, final ResultatTest res, final TesterContext context,
                              final Collection<String> propToUse) throws Exception {
    if (o1 == o2) {
      return true;
    }
    if (o1 == null || o2 == null) {
      return false;
    }
    if (mustTestAlreadyDone() && context.isObjectAlreadyTested(o1)) {
      // on interdit de récuperer d'autre objet.
      context.stopDigg();
    }
    return isSameSafe(o1, o2, res, context, propToUse);
  }

  @Override
  public boolean isSameSafe(final T o1, final T o2, final ResultatTest res, final TesterContext context) {
    return isSameSafe(o1, o2, res, context, getPropToCompare(o1, o2, context));
  }

  protected abstract Object getPropertyValue(final T o1, final String name) throws Exception;

  public boolean isSameSafe(final T o1, final T o2, final ResultatTest res, final TesterContext context, final Collection<String> propToUse) {

    final boolean classEquals = o1.getClass().equals(o2.getClass());
    if (compareClass && CollectionUtils.isEmpty(context.getPropToCompare()) && !classEquals) {
      if (res != null) {
        String prefix1 = toString(o1);
        if (o1 instanceof Pdt) {
          prefix1 = StringUtils.EMPTY;
        }
        res.addDiff(new ResultatTest(getClassSimpleName(o1), getClassSimpleName(o2), TYPE_DIFF, prefix1));
      }
      return false;
    }
    // on a fait un niveau de comparaison.
    if (context != null) {
      context.diggDone();
    }
    boolean same = true;
    final ResultatTest tempResultatTest = new ResultatTest(o1, o2, getClassSimpleName(o1));

    for (final String name : propToUse) {
      try {
        Object a = null;
        try {
          a = getPropertyValue(o1, name);
        } catch (final NestedNullException e) {
          //rien a faire
        }
        Object b = null;
        try {
          b = getPropertyValue(o2, name);
        } catch (final NestedNullException e) {
          //rien a faire
        }
        if (!compareProp(o1, tempResultatTest, name, a, b, context)) {
          same = false;
        }
      } catch (final Exception e) {
        if (context != null) {
          context.addErrorThrown("comparaison.error", e, e.getMessage());
        }
        LOGGER.log(Level.WARNING, "comparaison.error", e);
        try {
          if (context != null) {
            context.addWarn("usable.properties", StringUtils.join(getUsableProperties(o1), "; "));
          }
        } catch (final Exception e1) {
          LOGGER.log(Level.SEVERE, "isSameSafe", e1);
        }
      }
    }
    if (!same && res != null) {
      tempResultatTest.setPrintA(getPrefixForObject(o1));
      tempResultatTest.setPrintB(getPrefixForObject(o2));
      final List<ResultatTest> fils = tempResultatTest.getFils();
      if (fils.size() == 1) {
        res.addDiff(fils.get(0));
      } else {
        res.addDiff(tempResultatTest);
        for (final ResultatTest resultatTest : fils) {
          final Object[] msgArguments = resultatTest.getMsgArguments();
          if (ArrayUtils.isNotEmpty(msgArguments)) {
            msgArguments[0] = StringUtils.EMPTY;
            resultatTest.clearTraduction();
          }
        }
      }
    }
    return same;
  }

  @Override
  protected boolean mustTestAlreadyDone() {
    return true;
  }

  public void setUseNameForLoi(final boolean useNameForLoi) {
    this.useNameForLoi = useNameForLoi;
  }

  private String toString(final Object o1) {
    if (o1 != null && ("UnmodifiableMap".equals(getClassSimpleName(o1)) || o1.getClass().equals(HashMap.class))) {
      return "";
    }
    return TransformerEMHHelper.transformToStringOverview(o1, factory.getPropertyDefinition(), DecimalFormatEpsilonEnum.COMPARISON);
  }

  private String getClassSimpleName(final Object o1) {
    if (o1 == null) {
      return null;
    }
    final Class<?> aClass = o1.getClass();
    return simpleNames.computeIfAbsent(aClass, key -> aClass.getSimpleName());
  }

  protected String getPrefixForObject(final T o1) {
    final String prefix1 = toString(o1);
    return StringUtils.isEmpty(prefix1) ? o1.toString() : prefix1;
  }
}
