/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.helper.DynamicPropertyFilter;
import org.fudaa.dodico.crue.comparaison.ComparaisonSelectorContent;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.metier.result.OrdResDynamicPropertyFilter;
import org.fudaa.dodico.crue.metier.result.ResultKeyFormatter;
import org.fudaa.dodico.crue.metier.result.TimeSimuConverter;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
@SuppressWarnings("unchecked")
public class EqualsTesterResultatCalcul extends AbstractEqualsTester<ResultatCalcul> {
  private static final Logger LOGGER = Logger.getLogger(EqualsTesterResultatCalcul.class.getName());
  private final EqualsTesterBeanMap mapTester;
  private final int maxDiffByEMHOnResultat;
  private final Map<ResultatTimeKey, String> stringByTimeKey = new HashMap<>();

  /**
   * @param factoryEqualsTester    la factory des testeurs
   * @param maxDiffByEMHOnResultat nombre max de resultat
   */
  public EqualsTesterResultatCalcul(FactoryEqualsTester factoryEqualsTester, final int maxDiffByEMHOnResultat) {
    super();
    this.mapTester = new EqualsTesterBeanMap(factoryEqualsTester);
    mapTester.setUseNameForLoi(false);
    this.maxDiffByEMHOnResultat = maxDiffByEMHOnResultat;
  }

  @Override
  protected boolean mustTestAlreadyDone() {
    return false;
  }

  @Override
  public boolean isSameSafe(final ResultatCalcul o1, final ResultatCalcul o2, final ResultatTest res,
                            final TesterContext context) {
    boolean bool = true;
    //l'epsilon relatif pour rcal:
    double epsilonRelative = ComparaisonSelectorContent.getEpsilonRelativeRCALValue(o1.isC9(), o2.isC9(), mapTester.getCCM());
    context.setDoubleEpsilonRelative(epsilonRelative);
    Collection<ResultatTimeKey> resultatKeysIn1 = o1.getResultatPasDeTemps().getResultatKeys();
    final EMHScenario scenarioRef = getEmhModeleBase(o1).getParent();
    EMHModeleBase parentAlternatif = getEmhModeleBase(o2);
    final EMHScenario scenarioCible = parentAlternatif.getParent();
    TimeSimuConverter tempSimuConverter = TimeSimuConverter.create(scenarioRef, scenarioCible);
    int nbDiff = 0;
    ResultKeyFormatter formatter = new ResultKeyFormatter();
    formatter.setCcmAndScenario(mapTester.getCCM(), scenarioRef);
    DynamicPropertyFilter resultatFilter = context.getResultatFilter();
    Set<String> props = null;
    if (resultatFilter != null) {
      props = resultatFilter.getPropertyToUse(o1.getEmh(), o2.getEmh());
    }
    if (props == null) {
      LOGGER.log(Level.SEVERE, "compare.noOrdresFound for {0}", o1.getEMHType());
      context.addError("compare.noOrdresFound", o1.getEMHType());
      return false;
    }

    if (OrdResDynamicPropertyFilter.isModeleType(o1.getEMHType())) {
      String qVariables1 = StringUtils.join(o1.getQRegulVariablesName(),",");
      String qVariables2 = StringUtils.join(o2.getQRegulVariablesName(),",");
      if(!qVariables1.equals(qVariables2)){
        res.addDiff(new ResultatTest(qVariables1, qVariables2,"comparaison.resultat.VariableResQregul.diff"));
      }
      String zVariables1 = StringUtils.join(o1.getZRegulVariablesName(),",");
      String zVariables2 = StringUtils.join(o2.getZRegulVariablesName(),",");
      if(!zVariables1.equals(zVariables2)){
        res.addDiff(new ResultatTest(zVariables1, zVariables2,"comparaison.resultat.VariableResZregul.diff"));
      }
    }

    List<String> orderedProp = new ArrayList<>(props);
    Collections.sort(orderedProp);
    orderedProp = Collections.unmodifiableList(orderedProp);
    for (ResultatTimeKey resultatKey : resultatKeysIn1) {
      ResultatTimeKey o2ResultatKey = tempSimuConverter.getEquivalentTempsSimuRunAlternatif(resultatKey, parentAlternatif);
      if (o2ResultatKey == null || !o2.getResultatPasDeTemps().containsResultsFor(o2ResultatKey)) {
        continue;
      }
      try {
        final Map<String, Object> read1 = o1.read(resultatKey);
        final Map<String, Object> read2 = o2.read(o2ResultatKey);

        ResultatTest resLocal = new ResultatTest("", "", "compare.resultat.diff", "<NONE>");//sera remplacé...

        if (!mapTester.isSame(read1, read2, resLocal, context, orderedProp)) {
          if (maxDiffByEMHOnResultat > 0 && nbDiff >= maxDiffByEMHOnResultat) {
            res.addDiff(new ResultatTest("", "", "compare.resultatCalcul.Truncated"));
            res.setComparaisonStopped();
            break;
          }
          final List<ResultatTest> firstChildren = resLocal.getFils();
          List<ResultatTest> fils = firstChildren.get(0).getFils();
          //s'il y a une seule différence, elle est ajoutée directement dans le conteneur parentAlternatif.
          //on prend en compte ce comportement:
          if (firstChildren.get(0).getFils().isEmpty()) {
            fils = firstChildren;
          }
          String resultatKeyToString = resultatTimeKeyToString(resultatKey, formatter);
          resLocal = new ResultatTest("", "", "compare.resultat.diff", resultatKeyToString);
          resLocal.addDiffs(fils);
          if (maxDiffByEMHOnResultat > 0) {
            nbDiff += resLocal.getNbDifferences();
          }
          res.addDiff(resLocal);
        }
      } catch (Exception e) {
        context.addError(e.getMessage(), e);
      }
    }
    return bool;
  }

  private static EMHModeleBase getEmhModeleBase(ResultatCalcul resultatCalcul) {
    if (EnumCatEMH.MODELE.equals(resultatCalcul.getEmh().getCatType())) {
      return (EMHModeleBase) resultatCalcul.getEmh();
    }
    return EMHHelper.getParent(resultatCalcul.getEmh()).getParent();
  }

  private String resultatTimeKeyToString(ResultatTimeKey resultatKey, ResultKeyFormatter formatter) {
    return stringByTimeKey.computeIfAbsent(resultatKey, timeKey -> formatter.getResulatKeyTempsSceToStringTransformer().transform(timeKey));
  }
}
