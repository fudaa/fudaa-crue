/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Frederic Deniger
 */
public enum TestOption {

  IGNORE_COMMENT("IgnoreCommentaire", "True"), RELATIVE_FOR_DOUBLE("CompareDoubleRelativeMethod", "True"),
  ONLY_LOIZ_FIRST_POINT("CompareLoiZFirstPointOnly", "True"),
  LOIZ_DONT_COMPARE_FIRST_POINT("LoiZDontCompareFirstPoint", "True");
  private final String key;
  private final String active;

  TestOption(String key, String active) {
    this.key = key;
    this.active = active;
  }

  public String getActive() {
    return active;
  }

  public String getKey() {
    return key;
  }

  public boolean isActive(String value) {
    return active.equalsIgnoreCase(value);
  }

  public static Map<String, TestOption> getByKey() {
    Map<String, TestOption> res = new HashMap<>();
    TestOption[] values = TestOption.values();
    for (TestOption testOption : values) {
      res.put(testOption.key, testOption);
    }
    return res;
  }
}
