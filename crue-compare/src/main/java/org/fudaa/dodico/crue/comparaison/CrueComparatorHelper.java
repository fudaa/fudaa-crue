/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.comparaison.tester.EqualsTester;
import org.fudaa.dodico.crue.metier.comparator.Point2dAbscisseComparator;
import org.fudaa.dodico.crue.metier.emh.PtProfil;

import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Frederic Deniger
 */
public class CrueComparatorHelper {
    /**
     * @param pt     le point recherche
     * @param listPt la liste a parcourir
     * @return l'indice du PtProfil egal a epsilon pres
     */
    public static int getEqualsPtProfil(final PtProfil pt, final List<PtProfil> listPt, EqualsTester knownEqualsTester) {
        if (pt == null || CollectionUtils.isEmpty(listPt)) {
            return -1;
        }
        final int nb = listPt.size();
        for (int i = 0; i < nb; i++) {
            try {
                if (knownEqualsTester.isSame(pt, listPt.get(i), null, null)) {
                    return i;
                }
            } catch (final Exception e) {
                Logger.getLogger(CrueComparatorHelper.class.getName()).log(Level.INFO, "getEqualsPtProfil", e);
            }
        }
        return -1;
    }

    /**
     * Attention: la liste doit être triée.
     *
     * @param pt         le point profil
     * @param listPt     liste triée de points
     * @param comparator le comparateur d'abscisse
     * @return l'indice du profil issue de {@link Collections#binarySearch(List, Object)}
     */
    public static int getEqualsPtProfilSorted(final PtProfil pt, final List<PtProfil> listPt, Point2dAbscisseComparator comparator) {
        if (pt == null || CollectionUtils.isEmpty(listPt)) {
            return -1;
        }
        return Collections.binarySearch(listPt, pt, comparator);
    }

    /**
     * @param pt     le point de base
     * @param listPt la liste de point a parcourir dans laquelle on va recherche le point le plus pres.
     * @param min    true si on veut récupérer le point le plus près d'indice min
     * @return -1 si un des 2 argument est null ou si liste vide. l'indice sinon
     */
    public static int getNearestPtProfilEnY(final PtProfil pt, final List<PtProfil> listPt, final boolean min) {
        if (pt == null || CollectionUtils.isEmpty(listPt)) {
            return -1;
        }
        int res = 0;
        final int size = listPt.size();
        double minDistanceFound = -1;
        for (int i = 0; i < size; i++) {
            final PtProfil ptInList = listPt.get(i);
            final double distance = Math.abs(ptInList.getXt() - pt.getXt());
            if (minDistanceFound < 0 || (min ? (distance < minDistanceFound) : (distance <= minDistanceFound))) {
                res = i;
                minDistanceFound = distance;
            }
        }
        return res;
    }
}
