/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.fudaa.dodico.crue.config.ccm.ItemVariable;

/**
 * @author deniger
 */
public class EqualsTesterInteger extends AbstractEqualsTester<Number> {

  private ItemVariable propertyDefinition;

  @Override
  protected boolean mustTestAlreadyDone() {
    return false;
  }

  /**
   * @param eps l'epsilon utilise pour les comparaison
   */
  public void setPropertyDefinition(final ItemVariable eps) {
    this.propertyDefinition = eps;
  }

  /**
   * @param d1 le double a comparer
   * @param d2 le double a comparer
   * @return true si egaux a eps pres.
   */
  public boolean isSameInteger(final int d1, final int d2) {
    if (propertyDefinition != null) {
      if (propertyDefinition.isInfiniPositif(d1) && propertyDefinition.isInfiniPositif(d2)) { return true; }
      if (propertyDefinition.isInfiniNegatif(d1) && propertyDefinition.isInfiniNegatif(d2)) { return true; }
      return propertyDefinition.getEpsilon().isSame(d1, d2);
    }
    return d1==d2;
  }

  @Override
  public boolean isSameSafe(final Number o1, final Number o2, final ResultatTest res, TesterContext context) {
    return isSameInteger(o1.intValue(), o2.intValue());
  }

}
