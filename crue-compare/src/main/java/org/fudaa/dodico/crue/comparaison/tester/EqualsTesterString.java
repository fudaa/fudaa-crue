/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.apache.commons.lang3.StringUtils;

/**
 * @author deniger
 */
public class EqualsTesterString extends AbstractEqualsTester<String> {
    @Override
    public boolean isSameSafe(final String o1, final String o2, ResultatTest res, TesterContext context) {
        boolean trim = context.isTrimStringForComparison();
        String s1 = trim ? StringUtils.trim(o1) : o1;
        String s2 = trim ? StringUtils.trim(o2) : o2;
        if (context.isIgnoreCaseForStringComparison()) {
            return s1.equalsIgnoreCase(s2);
        }
        return s1.equals(s2);
    }

    @Override
    protected boolean mustTestAlreadyDone() {
        return false;
    }
}
