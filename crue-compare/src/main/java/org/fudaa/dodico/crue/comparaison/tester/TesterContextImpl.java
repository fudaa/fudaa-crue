/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.helper.DynamicPropertyFilter;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.comparaison.ComparaisonSelectorContent;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author deniger
 */
public class TesterContextImpl extends TesterContextDigg implements TesterContext {

  /**
   * @param tester a tester
   * @return true si le tester est complexe cad du type EqualsTesterBean.
   */
  public static boolean isComplexEqualsTester(final EqualsTester tester) {
    return tester != null
            && (EqualsTesterBean.class.equals(tester.getClass()) || EqualsTesterLoi.class.equals(tester.getClass()) || EqualsTesterCollection.class
            .equals(tester.getClass()));
  }
  private CtuluLog error = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
  private boolean ignoreCaseForStringComparison;
  private double doubleEpsilonRelative=ComparaisonSelectorContent.DEFAULT_RELATIVE_VALUE;
  private final Set<TestOption> activatedOption = new HashSet<>();
  private Collection<String> propToCompare;
  private Collection<String> propToIgnore;
  private final DynamicPropertyFilter resFilter;
  private final ToStringTransformer toStringTransformer;
  private boolean trimStringForComparison;

  /**
   * Constructeur par défaut ou on ne compare que les bean du premier niveau.
   */
  public TesterContextImpl(final DynamicPropertyFilter resFilter, Set<TestOption> activatedOption,
          final ToStringTransformer toStringTransformer) {
    this(1, resFilter, activatedOption, toStringTransformer);
  }

  /**
   * Constructeur par défaut ou on ne compare que les bean du premier niveau.
   */
  public TesterContextImpl(final DynamicPropertyFilter resFilter, final ToStringTransformer toStringTransformer) {
    this(1, resFilter, null, toStringTransformer);
  }

  /**
   * si maxDeep vaut 1, on ne compare que le niveau courant. si 2, on compare les objet de niveau 2 : ceux références par l'objet teste. si -1, on
   * compare tous les niveaux.
   *
   * @param maxDeep Le nombre de beanObjet à parcourir pour déterminer l'égalite de 2 objets.
   */
  public TesterContextImpl(final int maxDeep, final DynamicPropertyFilter resFilter, Set<TestOption> activatedOption,
          final ToStringTransformer toStringTransformer) {
    super(maxDeep);
    if (activatedOption != null) {
      this.activatedOption.addAll(activatedOption);
    }
    this.resFilter = resFilter;
    this.toStringTransformer = toStringTransformer;
  }

  /**
   * si maxDeep vaut 1, on ne compare que le niveau courant. si 2, on compare les objet de niveau 2 : ceux références par l'objet teste. si -1, on
   * compare tous les niveaux.
   *
   * @param maxDeep Le nombre de beanObjet à parcourir pour déterminer l'égalite de 2 objets.
   */
  public TesterContextImpl(final int maxDeep, final ToStringTransformer toStringTransformer) {
    this(maxDeep, null, null, toStringTransformer);
  }

  public TesterContextImpl(final ToStringTransformer toStringTransformer) {
    this(1, null, null, toStringTransformer);
  }

  @Override
  public void addError(final String msg) {
    error.addError(msg);
  }

  @Override
  public void addError(final String msg, final Object... data) {
    error.addError(msg, data);
  }
  @Override
  public void addError(final String msg, final Throwable e) {
    error.addError(msg, e);
  }

  @Override
  public void addErrorThrown(final String msg, final Throwable e, final Object... data) {
    error.addErrorThrown(msg, e, data);
  }

  @Override
  public void addFatalError(final String m) {
    error.addSevereError(m);
  }

  @Override
  public void addFatalError(final String m, final Object... arg) {
    error.addSevereError(m, arg);
  }

  @Override
  public void addInfo(final String msg) {
    error.addInfo(msg);
  }

  @Override
  public void addInfo(final String msg, final Object... args) {
    error.addInfo(msg, args);
  }

  @Override
  public void addWarn(final String msg) {
    error.addWarn(msg);
  }

  @Override
  public void addWarn(final String msg, final Object... args) {
    error.addWarn(msg, args);
  }

  @Override
  public TesterContext createSubTesterContext() {
    return new TesterContextSub(this);
  }

  @Override
  public CtuluLog getError() {
    return error;
  }

  @Override
  public Collection<String> getPropToCompare() {
    return propToCompare;
  }

  @Override
  public DynamicPropertyFilter getResultatFilter() {
    return resFilter;
  }

  @Override
  public ToStringTransformer getToStringTransformer() {
    return toStringTransformer;
  }

  @Override
  public double getDoubleEpsilonRelative() {
    return doubleEpsilonRelative;
  }

  @Override
  public void setDoubleEpsilonRelative(double doubleEpsilonRelative) {
    this.doubleEpsilonRelative = doubleEpsilonRelative;
  }
  
  

  /**
   * @return the ignoreCaseComparison
   */
  @Override
  public boolean isIgnoreCaseForStringComparison() {
    return ignoreCaseForStringComparison;
  }

  /**
   * @return the ignoreComment
   */
  @Override
  public boolean isIgnoreComment() {
    return isActivated(TestOption.IGNORE_COMMENT);
  }

  @Override
  public boolean isActivated(TestOption option) {
    return activatedOption.contains(option);
  }

  public void activate(TestOption option) {
    activatedOption.add(option);
  }

  public void unActivate(TestOption option) {
    activatedOption.remove(option);
  }

  /**
   * @return the trimStringForComparison
   */
  @Override
  public boolean isTrimStringForComparison() {
    return trimStringForComparison;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.fudaa.dodico.crue.comparaison.tester.TesterContext#manageException(java.lang.Exception)
   */
  @Override
  public void manageException(final Exception e) {
    error.manageException(e);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.fudaa.dodico.crue.comparaison.tester.TesterContext#manageException(java.lang.Exception, java.lang.String)
   */
  @Override
  public void manageException(final Exception e, final String msg) {
    error.manageException(e, msg);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.fudaa.dodico.crue.comparaison.tester.TesterContext#setError(org.fudaa.ctulu.CtuluAnalyze)
   */
  @Override
  public void setError(CtuluLog error) {
    this.error = error;
    error.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
  }

  /**
   * @param ignoreCaseComparison the ignoreCaseComparison to set
   */
  public void setIgnoreCaseForStringComparison(boolean ignoreCaseComparison) {
    this.ignoreCaseForStringComparison = ignoreCaseComparison;
  }

  public void setPropToCompare(final Collection<String> propToCompare) {
    this.propToCompare = propToCompare;
  }

  public void setPropToIgnore(Set<String> propToIgnore) {
    this.propToIgnore = propToIgnore;
  }

  /**
   * @param trimStringForComparison the trimStringForComparison to set
   */
  public void setTrimStringForComparison(boolean trimStringForComparison) {
    this.trimStringForComparison = trimStringForComparison;
  }

  /**
   * @return the propToIgnore
   */
  @Override
  public Collection<String> getPropToIgnore() {
    return propToIgnore;
  }
}
