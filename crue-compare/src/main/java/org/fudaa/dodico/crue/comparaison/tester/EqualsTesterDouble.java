/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.fudaa.dodico.crue.config.ccm.ItemVariable;

/**
 * @author deniger
 */
public class EqualsTesterDouble extends AbstractTemplateEqualsTesterDouble<Number> {
    public EqualsTesterDouble() {
        super();
    }

    public EqualsTesterDouble(ItemVariable eps, boolean useRelativeValue) {
        setPropertyDefinition(eps.getNature(), useRelativeValue);
    }

    @Override
    public boolean isSameSafe(final Number o1, final Number o2, ResultatTest res, TesterContext context) {
        boolean same = isSameDouble(o1.doubleValue(), o2.doubleValue());
        if (!same && context != null && useRelativeEpsilonValue && context.isActivated(TestOption.RELATIVE_FOR_DOUBLE)) {
            double diffRelative = getDiffRelative(o1.doubleValue(), o2.doubleValue());
            same = (diffRelative <= context.getDoubleEpsilonRelative());
        }
        return same;
    }

    public double getDiffRelative(double d1, double d2) {
        double moy = (Math.abs(d1) + Math.abs(d2)) / 2d;
        if (moy == 0) {
            return Math.abs(d1 - d2);
        } else {
            return Math.abs((d1 - d2) / moy);
        }
    }
}
