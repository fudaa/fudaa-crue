/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.io;

/**
 *
 * @author Frederic Deniger
 */
public class SelectorItem {

  String testId;

  public SelectorItem() {
  }

  public SelectorItem(String testId) {
    this.testId = testId;
  }
  
  

  public String getTestId() {
    return testId;
  }
  
  
}
