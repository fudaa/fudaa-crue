/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.config;

import org.apache.commons.lang3.StringUtils;

public class ConfOption {

  private String nom;
  private String valeur;

  public ConfOption() {
  }

  public ConfOption(String nom, String valeur) {
    this.nom = nom;
    this.valeur = valeur;
  }


  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = StringUtils.trim(nom);
  }

  public String getValeur() {
    return valeur;
  }

  public void setValeur(String valeur) {
    this.valeur = StringUtils.trim(valeur);
  }

}
