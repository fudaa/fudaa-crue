/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author deniger
 */
public class EqualsTesterArray extends AbstractEqualsTester<Object> {
    private final EqualsTesterCollection tester;

    /**
     * @param factory  la factory des testes
     * @param propName le nom de la propriete
     */
    public EqualsTesterArray(final FactoryEqualsTester factory, final String propName) {
        this.tester = new EqualsTesterCollection(factory, propName);
    }

    @Override
    protected boolean mustTestAlreadyDone() {
        return true;
    }

    @Override
    public boolean isSameSafe(final Object o1, final Object o2, final ResultatTest res, TesterContext context)
            throws Exception {
        final int length = Array.getLength(o1);
        if (length != Array.getLength(o2)) {
            if (res != null) {
                res.addDiff(new ResultatTest(length, Array.getLength(o2), EqualsTesterCollection.TAILLE_DIFF));
            }
            return false;
        }
        return tester.isSame(toCollection(o1, length), toCollection(o2, length), res, context);
    }

    private Collection toCollection(final Object array, final int length) {
        final Collection res = new ArrayList(length);
        for (int i = 0; i < length; i++) {
            res.add(Array.get(array, i));
        }
        return res;
    }
}
