/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.CustomPropertyUtilsBeanInstaller;

import java.util.Map;

/**
 * @author deniger
 */
public class EqualsTesterBeanMap extends AbstractEqualsTesterBean<Map> {
    static {
        CustomPropertyUtilsBeanInstaller.install();
    }

    /**
     * @param factory la factory
     */
    public EqualsTesterBeanMap(final FactoryEqualsTester factory) {
        super(factory, false);
    }

    @Override
    protected Object getPropertyValue(final Map o1, final String name) {
        return o1 == null ? null : o1.get(name);
    }

    @Override
    protected String getPrefixForObject(final Map o1) {
        return StringUtils.EMPTY;
    }
}
