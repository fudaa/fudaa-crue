/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.joda.time.Duration;
import org.joda.time.LocalDateTime;
import org.joda.time.Period;

import java.util.Map;

/**
 * @param <T>
 * @author deniger
 */
@SuppressWarnings("unchecked")
public abstract class EqualsTesterItemBuilder<T extends EqualsTester> {
  protected static void addDefault(final Map<Class, EqualsTesterItemBuilder> dest) {
        final DoubleTester d = EqualsTesterItemBuilderDefaults.DOUBLE_TESTER;
        dest.put(Double.class, d);
        dest.put(Float.class, d);
        dest.put(Integer.class, EqualsTesterItemBuilderDefaults.INTEGER_TESTER);
        dest.put(Long.class, EqualsTesterItemBuilderDefaults.LONG_TESTER);
        dest.put(Duration.class, new DurationTester());
        dest.put(Period.class, new PeriodTester());
        dest.put(LocalDateTime.class, new DateTester());
    }

    protected final T build(FactoryEqualsTester factory, String propertyName, CtuluLog log) {
        ItemVariable property = factory.getProperty(propertyName);
        if (isPropertyRequired() && property == null) {
            if (log != null) {
                log.addSevereError("propertyUseToCompareIsNotDefined.error", propertyName);
            }
            return null;
        }
        return internBuild(factory, property, log);
    }

    protected abstract T internBuild(FactoryEqualsTester factory, ItemVariable property, CtuluLog log);

    protected boolean isPropertyRequired() {
        return true;
    }

    protected static class DateTester extends EqualsTesterItemBuilder<EqualsTesterDate> {
        @Override
        protected EqualsTesterDate internBuild(final FactoryEqualsTester factory, final ItemVariable property,
                                               CtuluLog log) {
            final EqualsTesterDate res = new EqualsTesterDate();
            res.setPropertyDefinition(property.getNature(), false);
            return res;
        }
    }

    public static class DoubleTester extends EqualsTesterItemBuilder<EqualsTesterDouble> {
        @Override
        protected EqualsTesterDouble internBuild(final FactoryEqualsTester factory, final ItemVariable property,
                                                 CtuluLog log) {
            final EqualsTesterDouble res = new EqualsTesterDouble();
            res.setPropertyDefinition(property.getNature(), true);
            return res;
        }
    }

    public static class DurationTester extends EqualsTesterItemBuilder<EqualsTesterDuration> {
        @Override
        protected EqualsTesterDuration internBuild(final FactoryEqualsTester factory, final ItemVariable property,
                                                   CtuluLog log) {
            final EqualsTesterDuration res = new EqualsTesterDuration();
            res.setPropertyDefinition(property.getNature(), false);
            return res;
        }
    }

    public static class IntegerTester extends EqualsTesterItemBuilder<EqualsTesterInteger> {
        @Override
        protected EqualsTesterInteger internBuild(final FactoryEqualsTester factory, final ItemVariable property,
                                                  CtuluLog log) {
            final EqualsTesterInteger res = new EqualsTesterInteger();
            res.setPropertyDefinition(property);
            return res;
        }

        @Override
        protected boolean isPropertyRequired() {
            return false;
        }
    }

    public static class LongTester extends EqualsTesterItemBuilder<EqualsTesterLong> {
        @Override
        protected EqualsTesterLong internBuild(final FactoryEqualsTester factory, final ItemVariable property,
                                               CtuluLog log) {
            final EqualsTesterLong res = new EqualsTesterLong();
            res.setProperty(property);
            return res;
        }

        @Override
        protected boolean isPropertyRequired() {
            return false;
        }
    }

    public static class PeriodTester extends EqualsTesterItemBuilder<EqualsTesterPeriod> {
        @Override
        protected EqualsTesterPeriod internBuild(final FactoryEqualsTester factory, final ItemVariable property,
                                                 CtuluLog log) {
            final EqualsTesterPeriod res = new EqualsTesterPeriod();
            res.setPropertyDefinition(property.getNature(), false);
            return res;
        }
    }
}
