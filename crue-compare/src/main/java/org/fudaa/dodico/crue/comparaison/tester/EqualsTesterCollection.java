/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;

import java.util.Collection;
import java.util.Iterator;

/**
 * @author deniger
 */
public class EqualsTesterCollection extends AbstractEqualsTester<Collection> {
  public static final String TAILLE_DIFF = "comparaison.collection.size";
  private final FactoryEqualsTester factory;
  private final String propName;
  private final EqualsTester equalTester;
  private ToStringTransformer toStringTransformer;
  private String diffMsg = "comparaison.collection.item";

  public EqualsTesterCollection(final FactoryEqualsTester factory, final String propName) {
    super();
    this.factory = factory;
    this.propName = propName;
    this.equalTester = null;
  }

  public EqualsTesterCollection(final EqualsTester tester) {
    super();
    this.factory = null;
    this.propName = null;
    this.equalTester = tester;
  }

  @Override
  protected boolean mustTestAlreadyDone() {
    return true;
  }

  @Override
  public boolean isSameSafe(final Collection o1, final Collection o2, final ResultatTest res,
                            final TesterContext context) throws Exception {
    return isSameSafe(o1, o2, res, context, false);
  }

  /**
   * @param o1 collection 1 a comparer
   * @param o2 collection 1 a comparer
   * @param res resultat du test
   * @param context le context
   * @param ignoreFirstPoint si true la comparaison du premier point n'est pas effectuée.
   * @return true si identique
   * @throws Exception si probleme lors récuperation valeur par introspection par exemple
   */
  public boolean isSameSafe(final Collection o1, final Collection o2, final ResultatTest res,
                            final TesterContext context, final boolean ignoreFirstPoint) throws Exception {
    if (o1.size() != o2.size()) {
      if (res != null) {
        res.addDiff(new ResultatTest(o1.size(), o2.size(), TAILLE_DIFF));
      }
      return false;
    }
    if (o1.isEmpty()) {
      return true;
    }
    final Iterator it1 = o1.iterator();
    final Iterator it2 = o2.iterator();
    int idx = 0;
    EqualsTester tester = equalTester;
    // true si on peut comparer les objets complexes de cette liste.
    final boolean canDigg = context.canDigg();
    boolean isSame = true;
    if (ignoreFirstPoint) {
      it1.next();
      it2.next();
    }
    while (it1.hasNext()) {
      idx++;
      final Object obj1 = it1.next();
      final Object obj2 = it2.next();
      if (obj1 != obj2) {
        if (obj1 == null) {
          addDiff(res, idx, null, obj2);
          isSame = false;
        } else {
          if (tester == null) {
            tester = factory.buildTesterFor(null, obj1.getClass(), propName, context.getError());
            if (context.getError().containsSevereError()) {
              return false;
            }
            if (TesterContextImpl.isComplexEqualsTester(tester) && !canDigg) {
              return true;
            }
          }
          final ResultatTest itemDifferences = new ResultatTest(obj1, obj2, null);
          if (!tester.isSame(obj1, obj2, itemDifferences, context)) {
            final ResultatTest addDiff = addDiff(res, idx, obj1, obj2);
            if (addDiff != null) {
              addDiff.addDiffsAndUpdatePrint(itemDifferences.getFils(), context.getToStringTransformer());
            }
            isSame = false;
          }
        }
      }
    }

    return isSame;
  }

  private ResultatTest addDiff(final ResultatTest res, final int idx, final Object obj1, final Object obj2) {
    if (res != null) {
      final ResultatTest item = new ResultatTest(obj1, obj2, diffMsg, idx);
      if (toStringTransformer != null) {
        item.setPrintA(toStringTransformer.transform(obj1));
        item.setPrintB(toStringTransformer.transform(obj2));
      }
      res.addDiff(item);
      return item;
    }
    return null;
  }

  /**
   * @return the diffMsg
   */
  public String getDiffMsg() {
    return diffMsg;
  }

  /**
   * @param diffMsg the diffMsg to set
   */
  public void setDiffMsg(final String diffMsg) {
    this.diffMsg = diffMsg;
  }

  /**
   * @return the toStringTransformer
   */
  private ToStringTransformer getToStringTransformer() {
    return toStringTransformer;
  }

  /**
   * @param toStringTransformer the toStringTransformer to set
   */
  public void setToStringTransformer(final ToStringTransformer toStringTransformer) {
    this.toStringTransformer = toStringTransformer;
  }
}
