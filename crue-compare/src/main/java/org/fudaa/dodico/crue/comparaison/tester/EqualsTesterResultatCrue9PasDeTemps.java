/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.config.comparator.DoubleComparator;
import org.fudaa.dodico.crue.metier.emh.ResultatPasDeTemps;
import org.fudaa.dodico.crue.metier.emh.ResultatPasDeTempsDelegate;
import org.fudaa.dodico.crue.metier.result.Crue9ResultatCalculPasDeTemps;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * Comparateur spécifique pour les résultats Crue 9 prenant en compte ruinou,...
 *
 * @author deniger
 */
public class EqualsTesterResultatCrue9PasDeTemps extends AbstractEqualsTester<ResultatPasDeTemps> {
    private final EqualsTesterDouble ruinouTester;
    private final PropertyEpsilon epsPdt;
    private final CrueConfigMetier crueProperties;
    private NumberFormat pdtFormater;

    /**
     * @param epsPdt       valeur de epsilon
     * @param ruinouTester le tester de double
     * @param ccm          le {@link CrueConfigMetier}
     */
    public EqualsTesterResultatCrue9PasDeTemps(final PropertyEpsilon epsPdt, final EqualsTesterDouble ruinouTester,
                                               final CrueConfigMetier ccm) {
        super();
        this.epsPdt = epsPdt;
        this.crueProperties = ccm;
        this.ruinouTester = ruinouTester;
    }

    @Override
    protected boolean mustTestAlreadyDone() {
        return false;
    }

    private NumberFormat getNumberFormatForPdt() {
        if (pdtFormater == null) {
            pdtFormater = crueProperties.getProperty("pdt").getFormatter(DecimalFormatEpsilonEnum.COMPARISON);
        }
        return pdtFormater;
    }

    @Override
    public boolean isSameSafe(final ResultatPasDeTemps o1, final ResultatPasDeTemps o2,
                              final ResultatTest res, final TesterContext context) {
        final ResultatPasDeTempsDelegate delegate1 = o1.getDelegate();
        final ResultatPasDeTempsDelegate delegate2 = o2.getDelegate();
        if (Crue9ResultatCalculPasDeTemps.class.equals(delegate1.getClass())
                && Crue9ResultatCalculPasDeTemps.class.equals(delegate2.getClass())) {
            final Crue9ResultatCalculPasDeTemps times1 = (Crue9ResultatCalculPasDeTemps) delegate1;
            final Crue9ResultatCalculPasDeTemps times2 = (Crue9ResultatCalculPasDeTemps) delegate2;
            final DoubleComparator cmp = new DoubleComparator(epsPdt);
            final List<String> pdtWithDifferentRegul = new ArrayList<>();
            final List<String> pdtWithDifferentRuinou = new ArrayList<>();
            final int nbPdt1 = times2.getNbResultats();
            final int nbPdt2 = times2.getNbResultats();
            final TreeMap<Double, Integer> pdtIn1 = new TreeMap<>(cmp);
            for (int i = 0; i < nbPdt1; i++) {
                pdtIn1.put(Double.valueOf(times1.getPdt(i)), Integer.valueOf(i));
            }
            for (int i = 0; i < nbPdt2; i++) {
                final double pdt = times2.getPdt(i);
                final Integer pos = pdtIn1.get(Double.valueOf(pdt));
                if (pos != null) {
                    final int pos1 = pos;
                    final int pos2 = i;
                    final boolean isRegu2 = times2.isRegulation(pos2);
                    final boolean isRegu1 = times1.isRegulation(pos1);
                    if (isRegu1 != isRegu2) {
                        pdtWithDifferentRegul.add(getNumberFormatForPdt().format(pdt));
                    } else if (!isRegu1) {
                        final double rui1 = times1.getRuinou(pos1);
                        final double rui2 = times2.getRuinou(pos2);
                        if (!ruinouTester.isSameDouble(rui1, rui2)) {
                            pdtWithDifferentRuinou.add(getNumberFormatForPdt().format(pdt));
                        }
                    }
                }
            }
            if (pdtWithDifferentRegul.size() > 0) {
                res.addDiff(new ResultatTest(StringUtils.EMPTY, StringUtils.join(pdtWithDifferentRegul, "; "),
                        "compare.pdt.withDifferentRegu"));
            }
            if (pdtWithDifferentRuinou.size() > 0) {
                res.addDiff(new ResultatTest(StringUtils.EMPTY, StringUtils.join(pdtWithDifferentRuinou, "; "),
                        "compare.pdt.withDifferentRuinou"));
            }
        }
        res.setNbObjectTested(o1.getNbResultats());

        return true;
    }
}
