/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.joda.time.LocalDateTime;

/**
 * Comparateur de date.
 * 
 * @author deniger
 */
public class EqualsTesterDate extends AbstractTemplateEqualsTesterDouble<LocalDateTime> {

  @Override
  public boolean isSameSafe(final LocalDateTime o1, final LocalDateTime o2, ResultatTest res, TesterContext context) {
    return isSameDouble(o1.toDateTime().getMillis() / 1000d, o2.toDateTime().getMillis() / 1000d);

  }

}
