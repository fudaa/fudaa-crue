/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.io;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Frederic Deniger
 */
public class SelectorItemContainer {

  private final List<SelectorItem> items=new ArrayList<>();

  public List<SelectorItem> getItems() {
    return items;
  }
}
