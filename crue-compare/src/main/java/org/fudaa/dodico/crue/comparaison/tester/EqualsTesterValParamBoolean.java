/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.dodico.crue.metier.emh.ValParamBooleen;

/**
 * @author deniger
 */
public class EqualsTesterValParamBoolean extends AbstractEqualsTester<ValParamBooleen> {
    public EqualsTesterValParamBoolean(final FactoryEqualsTester factory) {
    }

    @Override
    protected boolean mustTestAlreadyDone() {
        return false;
    }

    @Override
    public boolean isSameSafe(final ValParamBooleen o1, final ValParamBooleen o2, final ResultatTest res,
                              final TesterContext context) {
        if (!ObjectUtils.equals(o1.getId(), o2.getId())) {
            if (res != null) {
                res.addDiff(new ResultatTest(o1.getNom(), o2.getNom(), "compare.type.diff"));
                return false;
            }
        }
        final boolean same = o2.getValeur() == o1.getValeur();
        if (!same && res != null) {
            final ResultatTest item = new ResultatTest(o1, o2, o1.getNom());
            item.setPrintA(context.getToStringTransformer().transform(o1));
            item.setPrintB(context.getToStringTransformer().transform(o2));
            res.addDiff(item);
        }
        return same;
    }
}
