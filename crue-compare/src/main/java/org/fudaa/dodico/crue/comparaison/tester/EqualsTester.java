/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.comparaison.tester;

/**
 * @param <T> l'objet a tester
 * @author deniger
 */
public interface EqualsTester<T> {
    /**
     * @return la factory de contexte
     */
    TesterContextFactory getContextFactory();

    /**
     * @param contextFactory the contextFactory to set
     */
    void setContextFactory(TesterContextFactory contextFactory);

    /**
     * @param o1      l'objet 1 a comparer
     * @param o2      l'objet 2 a comparer
     * @param res     resultat du test
     * @param context le context du test
     * @return true si les 2 objets sont les mêmes
     * @throws Exception si erreur d'introspection
     */
    boolean isSame(T o1, T o2, ResultatTest res, TesterContext context) throws Exception;
}
