/*
 GPL 2
 */
package org.fudaa.fudaa.crue.options.tools;

import org.fudaa.fudaa.crue.options.services.OpenFileProcess;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.modules.Places;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collections;

@ActionID(category = "Tools", id = "org.fudaa.fudaa.crue.options.tools.issue.OpenLogAction")
@ActionRegistration(
    displayName = "#CTL_OpenLogAction")
@ActionReference(path = "Menu/Tools", position = 11)
@Messages("CTL_OpenLogAction=Ouvrir le log de l'application")
public final class OpenLogAction implements ActionListener {

  @Override
  public void actionPerformed(ActionEvent e) {
    File userDirectory = Places.getUserDirectory();
    File log = new File(userDirectory, "/var/log/messages.log");
    try {
      new OpenFileProcess().openFiles(Collections.singletonList(log));
//      Desktop.getDesktop().
    } catch (Exception ex) {
      Exceptions.printStackTrace(ex);
    }
  }
}
