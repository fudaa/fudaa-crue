/*
 GPL 2
 */
package org.fudaa.fudaa.crue.options.tools;

import java.awt.Desktop;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URI;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import static org.fudaa.fudaa.crue.options.tools.Bundle.*;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.modules.Places;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;

@ActionID(category = "Tools", id = "org.fudaa.fudaa.crue.options.tools.issue.ReportBugAction")
@ActionRegistration(
        displayName = "#CTL_ReportBugAction")
@ActionReference(path = "Menu/Tools", position = 10, separatorBefore = 9)
@Messages("CTL_ReportBugAction=Déclarer un bogue")
public final class ReportBugAction implements ActionListener {

  @Override
  @Messages({"BugReportUrl=https://fudaa-project.atlassian.net/secure/CreateIssue!default.jspa?selectedProjectId=10021&issuetype=1",
    "PathToLogFileCopiedInClipboard=Le chemin vers le fichier de log a \u00e9t\u00e9 copi\u00e9 dans le presse-papier. Vous pouvez le coller dans la zone attachement de la page de report de bug"})
  public void actionPerformed(ActionEvent e) {
    String message = BugReportUrl();
    try {
      File userDirectory = Places.getUserDirectory();
      File log = new File(userDirectory, "/var/log/messages.log");
      Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
      clipboard.setContents(new StringSelection(log.getAbsolutePath()), null);
      DialogHelper.showNotifyOperationTermine(PathToLogFileCopiedInClipboard());
      Desktop.getDesktop().browse(new URI(message));
//      Desktop.getDesktop().
    } catch (Exception ex) {
      Exceptions.printStackTrace(ex);
    }
  }
}
