package org.fudaa.fudaa.crue.options.node;

import java.lang.reflect.InvocationTargetException;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport.ReadOnly;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class CoeurNode extends AbstractNode {

  public static final String PROP_ID = "id";
  public static final String PROP_XSD = "xsd";
  public static final String PROP_ISDEFAULT = "isDefault";
  public static final String PROP_FOLDERPATH = "folderPath";

  public static String getPropIdDisplayName() {
    return NbBundle.getMessage(CoeurNode.class, "ProprieteCoeur.Id.Name");
  }

  public static String getPropIdDescription() {
    return NbBundle.getMessage(CoeurNode.class, "ProprieteCoeur.Id.Desc");
  }

  public static String getPropXsdDisplayName() {
    return NbBundle.getMessage(CoeurNode.class, "ProprieteCoeur.Xsd.Name");
  }

  public static String getPropXsdDescription() {
    return NbBundle.getMessage(CoeurNode.class, "ProprieteOption.Xsd.Desc");
  }

  public static String getPropIsDefaultDisplayName() {
    return NbBundle.getMessage(CoeurNode.class, "ProprieteCoeur.IsDefault.Name");
  }

  public static String getPropIsDefaultDescription() {
    return NbBundle.getMessage(CoeurNode.class, "ProprieteCoeur.IsDefault.Desc");
  }

  public static String getPropFolderPathDisplayName() {
    return NbBundle.getMessage(CoeurNode.class, "ProprieteCoeur.FolderPath.Name");
  }

  public static String getPropFolderPathDescription() {
    return NbBundle.getMessage(CoeurNode.class, "ProprieteCoeur.FolderPath.Desc");
  }

  public CoeurNode(CoeurConfig coeurConfig) {
    super(Children.LEAF, Lookups.singleton(coeurConfig));
    setDisplayName(coeurConfig.getName());
    setShortDescription(coeurConfig.getComment());
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    CoeurConfig option = getCoeurConfig();
    try {
      set.put(new PropertyId(option));
      set.put(new PropertyGrammaire(option));
      set.put(new PropertyIsDefault(option));
      set.put(new PropertyFolderPath(option));
    } catch (Exception ex) {
      Exceptions.printStackTrace(ex);
    }
    sheet.put(set);
    return sheet;
  }

  public CoeurConfig getCoeurConfig() {
    return getLookup().lookup(CoeurConfig.class);
  }


  private static class PropertyId extends ReadOnly {

    private final CoeurConfig option;

    public PropertyId(final CoeurConfig option) {
      super(PROP_ID, String.class, getPropIdDisplayName(), getPropIdDescription());
      this.option = option;
      PropertyCrueUtils.configureNoCustomEditor(this);
    }

    @Override
    public Object getValue() throws IllegalAccessException, InvocationTargetException {
      return option.getName();
    }
  }

  private static class PropertyIsDefault extends ReadOnly {

    private final CoeurConfig option;

    public PropertyIsDefault(final CoeurConfig option) {
      super(PROP_ISDEFAULT, Boolean.class, getPropIsDefaultDisplayName(), getPropIsDefaultDescription());
      this.option = option;
      PropertyCrueUtils.configureNoCustomEditor(this);
    }

    @Override
    public Object getValue() throws IllegalAccessException, InvocationTargetException {
      return option.isUsedByDefault();
    }
  }

  private static class PropertyGrammaire extends ReadOnly {

    private final CoeurConfig value;

    public PropertyGrammaire(final CoeurConfig option) {
      super(PROP_XSD, String.class, getPropXsdDisplayName(), getPropXsdDescription());
      this.value = option;
      PropertyCrueUtils.configureNoCustomEditor(this);
    }

    @Override
    public Object getValue() throws IllegalAccessException, InvocationTargetException {
      return value.getXsdVersion();
    }
  }

  private static class PropertyFolderPath extends ReadOnly {

    private final CoeurConfig value;

    public PropertyFolderPath(final CoeurConfig option) {
      super(PROP_FOLDERPATH, String.class, getPropFolderPathDisplayName(), getPropFolderPathDescription());
      this.value = option;
      PropertyCrueUtils.configureNoCustomEditor(this);
    }

    @Override
    public Object getValue() throws IllegalAccessException, InvocationTargetException {
      return value.getCoeurFolderPath();
    }
  }
}
