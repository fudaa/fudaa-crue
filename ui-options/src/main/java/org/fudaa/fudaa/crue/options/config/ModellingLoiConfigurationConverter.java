/*
 GPL 2
 */
package org.fudaa.fudaa.crue.options.config;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueDaoStructure;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.fudaa.ebli.courbe.convert.EGAxePersistConverter;
import org.fudaa.ebli.courbe.convert.EGCourbePersistConverter;

/**
 *
 * @author Frederic Deniger
 */
public class ModellingLoiConfigurationConverter implements CrueDaoStructure {

  @Override
  public void configureXStream(XStream xstream, CtuluLog analyze) {
    TraceToStringConverter traceToStringConverter = new TraceToStringConverter();
    EGCourbePersistConverter courbConverter = new EGCourbePersistConverter(traceToStringConverter);
    courbConverter.initXstream(xstream);
    EGAxePersistConverter axeConverter = new EGAxePersistConverter(traceToStringConverter);
    axeConverter.initXstream(xstream);
    xstream.processAnnotations(CourbeConfig.class);

  }
}
