package org.fudaa.fudaa.crue.options;

import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.openide.modules.ModuleInfo;
import org.openide.modules.ModuleInstall;
import org.openide.util.Lookup;

import java.util.logging.Logger;

/**
 * Module appelé au demarrage permettant de tester la presence du dossier <i>etc</i> et des fichiers de configuration.
 *
 * @author deniger
 * @see org.fudaa.fudaa.crue.options.services.InstallationService
 */
public class CrueConfigurationInstaller extends ModuleInstall {
  final InstallationService service = Lookup.getDefault().lookup(InstallationService.class);

  @Override
  public void restored() {
    Lookup.Template<ModuleInfo> template = new Lookup.Template<>(ModuleInfo.class,
        "Module[org.fudaa.soft.fudaa.crue.ui.options", null);
    Lookup.Item<ModuleInfo> modules = Lookup.getDefault().lookupItem(template);
    if (modules != null) {
      ModuleInfo instance = modules.getInstance();
      if (instance != null) {
        Logger.getLogger(getClass().getName()).warning("Fudaa-Crue Version: " + instance.getImplementationVersion());
        System.setProperty("netbeans.buildnumber", instance.getImplementationVersion());
      }
    }
    service.load();
  }
}
