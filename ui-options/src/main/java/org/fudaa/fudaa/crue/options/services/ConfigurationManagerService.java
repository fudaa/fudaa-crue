package org.fudaa.fudaa.crue.options.services;

import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuResource;
import com.memoire.fu.FuResource;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.io.CrueXmlReaderWriterImpl;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierLoader;
import org.fudaa.dodico.crue.config.coeur.*;
import org.fudaa.dodico.crue.io.aide.*;
import org.fudaa.dodico.crue.io.conf.*;
import org.fudaa.dodico.crue.projet.ConfigurationReader;
import org.fudaa.dodico.crue.projet.conf.*;
import org.fudaa.fudaa.crue.common.helper.ChooserView;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.options.config.ModellingLoiConfigurationConverter;
import org.fudaa.fudaa.crue.options.node.CoeurNode;
import org.fudaa.fudaa.crue.options.node.CoeurNodeChildFactory;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ServiceProvider;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.*;
import java.util.List;

/**
 * Service permettant de charger les fichiers de configuration du site soit les options
 * {@link org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager} et les
 * definition des coeurs
 * {@link org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager}. Ce
 * service utilise le service {@link org.fudaa.fudaa.crue.options.services.InstallationService} pour récupérer les URL des
 * fichiers de configuration.
 * <br><br>
 * <table  border="1"  cellpadding="3" cellspacing="0"  summary="lookups">
 * <tr>
 * <th align="left">Lookup</th>
 * <th align="left">Commentaire</th>
 * <th align="left">Méthode utilisant le lookup</th>
 * </tr>
 * <tr>
 * <td> {@link org.fudaa.dodico.crue.config.coeur.CoeurManager org.fudaa.dodico.crue.config.coeur.CoeurManager}</td>
 * <td>
 * Le manager des coeurs de calcul
 * </td>
 * <td>{@link #getCoeurManager()}</td>
 * </tr>
 * <tr>
 * <td>{@link org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager}</td>
 * <td>	Manager des options site et etude.</td>
 * <td><code>{@link #getOptionsManager()}</code></td>
 * </tr>
 * </table>
 *
 * @author Christophe CANEL
 */
@ServiceProvider(service = ConfigurationManagerService.class)
public class ConfigurationManagerService implements Lookup.Provider, LookupListener {
  /**
   * Service donnant les informations sur l'installation site.
   */
  private final InstallationService installationService = Lookup.getDefault().lookup(InstallationService.class);
  /**
   * Resultat du lookup effectue sur le service
   * {@link org.fudaa.fudaa.crue.options.services.InstallationService InstallationService}
   */
  private final Result<File> lookupResult;
  /**
   * Le contenu du lookup fourni par ce service
   */
  private final InstanceContent dynamicContent = new InstanceContent();
  /**
   * Le lookup fourni par ce service
   */
  private final Lookup lookup = new AbstractLookup(dynamicContent);
  private boolean isCoeurValid = false;
  private boolean isOptionsValid = false;

  /**
   * true si on peut créer des projets ou migrer vers des projets en version 1.3 uniquement
   */
  private boolean useOnlyHighestGrammar = true;

  @SuppressWarnings("LeakingThisInConstructor")
  public ConfigurationManagerService() {
    //on ecoute les modifications sur les fichiers de configuration pour recharger les managers ( options et coeurs) dès que modif.
    lookupResult = installationService.getLookup().lookupResult(File.class);
    lookupResult.addLookupListener(this);
    //chargement initial des managers.
    resultChanged(null);
  }

  /**
   * @return les informations de connexion locale.
   * @see InstallationService
   */
  public ConnexionInformation getConnexionInformation() {
    return installationService.getConnexionInformation();
  }

  /**
   * @return la locale (fr_FR par exemple) définie dans les options.
   */
  public String getCurrentLocale() {
    return getOptionsManager().getOption(OptionsEnum.USER_LANGUAGE).getValeur();
  }

  /**
   * Ecoute les modifications dans les fichiers d'installation pour recharger les managers si modif.
   *
   * @param ev event ignore.
   */
  @Override
  public final void resultChanged(final LookupEvent ev) {
    final File lookupFile = installationService.getLookup().lookup(File.class);
    if (lookupFile != null) {
      reloadAll();
    }
  }

  /**
   * @return lookup du service
   */
  @Override
  public Lookup getLookup() {
    return lookup;
  }

  /**
   * Recharge si necessaire les fichiers coeurs.
   *
   * @return le CoeurManager.
   */
  public CoeurManager getCoeurManager() {
    final CoeurManager coeurManager = lookup.lookup(CoeurManager.class);
    if (coeurManager == null) {
      reloadCoeur();
    }
    return lookup.lookup(CoeurManager.class);
  }

  /**
   * Recharge si necessaire les fichiers d'options.
   *
   * @return le GlobalOptionsManager contenant les options site et utilisateur.
   */
  public GlobalOptionsManager getOptionsManager() {
    final GlobalOptionsManager coeurManager = lookup.lookup(GlobalOptionsManager.class);
    if (coeurManager == null) {
      reloadOptions();
    }
    return lookup.lookup(GlobalOptionsManager.class);
  }

  /**
   * Recharge les coeurs (utilise par le panneau de configuration).
   */
  public void reloadCoeur() {
    reloadCoeur(null, null);
  }

  /**
   * @return les differents blocs de configuration d'un etude ( affichage lois, planimetrie,....) contenu par le fichier site
   * FudaaCrue_Etude.xml
   * @see org.fudaa.fudaa.crue.options.services.InstallationService#getEtudeConfigFile
   */
  public CrueEtudeDaoConfiguration readDefaultEtudeConfig() {
    //le fichier de config niveau etude
    final File etudeConfigFile = installationService.getEtudeConfigFile();
    if (etudeConfigFile == null) {
      return null;
    }
    //chargement
    final CrueIOResu<CrueEtudeDaoConfiguration> read = CrueEtudeConfigurationHelper.read(etudeConfigFile, new ModellingLoiConfigurationConverter());
    //gestion des erreurs
    if (read.getAnalyse().containsErrorOrSevereError()) {
      LogsDisplayer.displayError(read.getAnalyse(), NbBundle.getMessage(ConfigurationManagerService.class,
          "ReadDefaultEtudeConfiguration.BilanError"));
    }
    //retour resultat.
    return read.getMetier();
  }

  /**
   * @return les differents blocs de configuration d'un etude ( affichage lois, planimetrie,....) contenu par le fichier site
   * FudaaCrue_Etude_Aoc.xml
   * @see org.fudaa.fudaa.crue.options.services.InstallationService#getEtudeConfigFile
   */
  public CrueAocDaoConfiguration readDefaultAocConfig() {
    //le fichier de config niveau etude
    final File etudeConfigFile = installationService.getEtudeAocConfigFile();

    //chargement
    final CrueIOResu<CrueAocDaoConfiguration> read = CrueAocConfigurationHelper.read(etudeConfigFile, new ModellingLoiConfigurationConverter());
    //gestion des erreurs
    if (read.getAnalyse().containsErrorOrSevereError()) {
      LogsDisplayer.displayError(read.getAnalyse(), NbBundle.getMessage(ConfigurationManagerService.class,
          "ReadDefaultAocConfiguration.BilanError"));
    }
    //retour resultat.
    return read.getMetier();
  }

  /**
   * Recharge et valide les coeurs.
   *
   * @param conf   la configuration site et utilisateur
   * @param parent receveur de logs
   */
  private void reloadCoeur(final Configuration conf, final CtuluLogGroup parent) {
    //reinitialisation des variables
    isCoeurValid = false;
    CoeurManager coeurManager = lookup.lookup(CoeurManager.class);
    if (coeurManager != null) {
      dynamicContent.remove(coeurManager);
    }
    //lecture de la conf
    Configuration readConfiguration = conf;
    if (conf == null) {
      readConfiguration = read(installationService.getSiteConfigFile());
    }
    if (readConfiguration == null) {
      return;
    }
    //validation de la conf principal
    coeurManager = new CoeurManager(installationService.getSiteDir(), readConfiguration.getSite().getCoeurs());
    final CoeurManagerValidator validator = new CoeurManagerValidator();
    final CtuluLogGroup logGroup = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);

    final CtuluLog validate = validator.validate(coeurManager);
    logGroup.setDescription(validate.getDesc());
    logGroup.setDescriptionArgs(validate.getDescriptionArgs());
    logGroup.addLog(validate);
    isCoeurValid = !validate.containsErrorOrSevereError();
    //si valide, validation des coeurs:
    if (isCoeurValid) {
      final List<CoeurConfig> allCoeurConfigs = coeurManager.getAllCoeurConfigs();
      for (final CoeurConfig coeurConfig : allCoeurConfigs) {
        //pour chaque coeur, on charge le CrueConfigMetier
        final File crueConfigMetierURL = new File(coeurConfig.getCrueConfigMetierURL());
        final CrueConfigMetierLoader loader = new CrueConfigMetierLoader();
        final CrueIOResu<CrueConfigMetier> load = loader.load(crueConfigMetierURL, coeurConfig);
        //et on traite les logs:
        final CtuluLog log = load.getAnalyse();
        if (log.isNotEmpty()) {
          log.setDesc(coeurConfig.getName() + ": " + log.getDesci18n());
          logGroup.addLog(log);
        }
        if (load.getAnalyse().containsErrorOrSevereError() || load.getMetier() == null) {
          isCoeurValid = false;
        }
        if (load.getMetier() != null) {
          coeurConfig.loadCrueConfigMetier(load.getMetier());
          final Crue9DecimalFormatBuilder crue9DecimalFormatBuilder = new Crue9DecimalFormatBuilder();
          crue9DecimalFormatBuilder.intialize(readConfiguration, load.getMetier());
        }
      }
    }
    //maj des logs
    if (parent != null) {
      parent.addGroup(logGroup);
    } else if (logGroup.containsSomething()) {
      LogsDisplayer.displayError(logGroup, NbBundle.getMessage(ConfigurationManagerService.class,
          "ValidationCoeurs.BilanDialogTitle"));
    }
    //ajout au lookup
    dynamicContent.add(coeurManager);
  }

  /**
   * @param confFile le fichier à charger.
   * @return les options site et utilisateur. renvoie null si des erreurs surviennent lors de la lecture.
   */
  private Configuration read(final File confFile) {
    final ConfigurationReader reader = new ConfigurationReader(null);
    final CrueIOResu<Configuration> result = reader.read(confFile);
    final CtuluLog analyse = result.getAnalyse();
    if (analyse.isNotEmpty()) {
      LogsDisplayer.displayError(analyse, NbBundle.getMessage(ConfigurationManagerService.class,
          "LoadConfigurationFile.BilanDialogTitle"));
    }
    return analyse.containsErrorOrSevereError() ? null : result.getMetier();
  }

  /**
   * @return liste des noms des coeurs de calculs.
   */
  public List<String> getCoeursNames() {
    final List<CoeurConfig> coeurs = getCoeurManager().getAllCoeurConfigs();
    final List<String> names = new ArrayList<>(coeurs.size());

    for (final CoeurConfig coeur : coeurs) {
      names.add(coeur.getName());
    }
    return names;
  }

  /**
   * @return la version xsd choisie par l'utilisateur parmi les xsd disponbible dans l'installation courante. renvoie null si
   * l'utilisateur annule son choix.
   */
  public String chooseXsdVersion() {
    List<String> allXsd = null;
    if (useOnlyHighestGrammar) {
      allXsd = Arrays.asList(getCoeurManager().getHigherGrammaire());
    } else {
      allXsd = getCoeurManager().getAllXsd();
    }
    if (allXsd.isEmpty()) {
      return null;
    }
    if (allXsd.size() == 1) {
      return allXsd.get(0);
    }
    final JComboBox cb = new JComboBox(allXsd.toArray(new String[0]));
    final JPanel pn = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
    pn.add(new JLabel(NbBundle.getMessage(ConfigurationManagerService.class, "ChooseXsdVersion.DialogLabel")));
    pn.add(cb);
    cb.setSelectedIndex(allXsd.size() - 1);
    final boolean ok = DialogHelper.showQuestionOkCancel(NbBundle.getMessage(ConfigurationManagerService.class,
        "ChooseXsdVersion.DialogTitle"), pn);
    if (ok) {
      return (String) cb.getSelectedItem();
    }
    return null;
  }

  /**
   * Permet de choisir le coeur à utiliser en fonction d'une version de grammaire (xsd).
   *
   * @param xsdVersion la version xsd voulue.
   * @return la version par défaut ou choisie par l'utilisateur si plusieurs.
   */
  public CoeurConfigContrat chooseCoeur(final String xsdVersion) {
    final CoeurConfig coeurConfig = getCoeurManager().getCoeurConfigDefault(xsdVersion);
    //un coeur par defaut est defini pour la version: on le retourne
    if (coeurConfig != null) {
      return coeurConfig;
    }
    //sinon on va demander à l'utilisateur de choisir.
    final List<CoeurConfig> coeurConfigDefault = new ArrayList<>(getCoeurManager().getAllCoeurConfig(xsdVersion));
    //psa de coeurs trouvés ->  message d'erreur:
    if (coeurConfigDefault.isEmpty()) {
      DialogHelper.showError(NbBundle.getMessage(ConfigurationManagerService.class, "ChooseCoeur.NoCoeurFound.DialogTitle"),
          NbBundle.getMessage(ConfigurationManagerService.class, "ChooseCoeur.NoCoeurFound.DialogMessage",
              xsdVersion));
      return null;
    }
//un seul coeur trouvé: on l'utilise
    if (coeurConfigDefault.size() == 1) {
      return coeurConfigDefault.iterator().next();
    } else {
      //on doit demander à l'utilisateur de choisir.
      //on tri les coeurs et on crée un NodeChildFacture ( netbeans RCP):
      Collections.sort(coeurConfigDefault, new CoeurConfigComparator());
      final CoeurNodeChildFactory childFactory = new CoeurNodeChildFactory((getCoeurManager()));
      final Node rootNode = new AbstractNode(Children.create(childFactory, false), Lookup.EMPTY);
      //la vue de sélection:
      final ChooserView chooser = new ChooserView(rootNode);
      //on affiche le dialogue:
      final NotifyDescriptor nd = new NotifyDescriptor(chooser, NbBundle.getMessage(ConfigurationManagerService.class,
          "ChooseCoeurDialog.Title"),
          NotifyDescriptor.OK_CANCEL_OPTION, NotifyDescriptor.QUESTION_MESSAGE, null,
          NotifyDescriptor.OK_OPTION);
      final Object notify = DialogDisplayer.getDefault().notify(nd);
      //si ok, on renvoie le coeur sélectionné
      if (NotifyDescriptor.OK_OPTION.equals(notify)) {
        final Node selectedNodes = chooser.getSelectedNode();
        if (selectedNodes != null) {
          return ((CoeurNode) selectedNodes).getCoeurConfig();
        }
      }
    }
    //sinon rien....
    return null;
  }

  public void reloadAll() {
    final Configuration siteRead = read(installationService.getSiteConfigFile());
    if (siteRead == null) {
      return;
    }
    final CtuluLogGroup log = new CtuluLogGroup(null);
    reloadCoeur(siteRead, log);
    reloadOptions(siteRead, log);

    if (log.containsSomething()) {
      WindowManager.getDefault().invokeWhenUIReady(() -> LogsDisplayer.displayError(log,
          NbBundle.getMessage(ConfigurationManagerService.class, "ValidationConfigSite.BilanDialogTitle")));
    }
    final String currentLocale = getCurrentLocale();
    if (currentLocale != null && currentLocale.toLowerCase().contains("fr")) {
      BuResource.setDefaultToEnglish(false);
      FuResource.setDefaultToEnglish(false);
      BuPreferences.BU.applyLanguage("fr");
      BuPreferences.BU.applyLanguage("fr");
    }
  }

  public boolean isConfigValidShowMessage() {
    if (!isConfigValid()) {
      DialogHelper.showError(NbBundle.getMessage(ConfigurationManagerService.class, "ConfigurationInvalid.ErrorTitle"),
          NbBundle.getMessage(ConfigurationManagerService.class, "ConfigurationInvalid.ErrorMessage"));
      return false;
    }
    return true;
  }

  public boolean isConfigValid() {
    return isCoeurValid && isOptionsValid;
  }

  public void reloadOptions() {
    reloadOptions(null, null);
  }

  private void reloadOptions(final Configuration conf, final CtuluLogGroup parent) {
    isOptionsValid = false;
    GlobalOptionsManager optionsManager = lookup.lookup(GlobalOptionsManager.class);

    if (optionsManager != null) {
      dynamicContent.remove(optionsManager);
    }
    Configuration siteRead = conf;

    if (siteRead == null) {
      siteRead = read(installationService.getSiteConfigFile());
    }

    if (siteRead == null) {
      return;
    }
    Configuration userRead = null;
    final File userConfigFile = installationService.getUserConfigFile();
    if (userConfigFile.exists()) {
      userRead = read(installationService.getUserConfigFile());
    }
    UserConfiguration user = null;

    if (userRead != null) {
      user = userRead.getUser();
    }
    if (user == null) {
      user = new UserConfiguration();
    }
    optionsManager = new GlobalOptionsManager();
    final CtuluLogGroup init = optionsManager.init(siteRead, user);

    if (parent
        != null) {
      parent.addGroup(init);
    } else if (init.containsSomething()) {
      LogsDisplayer.displayError(init,
          NbBundle.getMessage(ConfigurationManagerService.class, "ValidationOptions.BilanDialogTitle"));
    }
    isOptionsValid = !init.containsFatalError();

    if (isOptionsValid) {
      dynamicContent.add(optionsManager);
    }
  }

  public void saveUserOptions(final UserConfiguration userConfiguration) {
    final CtuluLogGroup reloadUserConfiguration = getOptionsManager().validUserConfiguration(userConfiguration);
    if (reloadUserConfiguration.containsSomething()) {
      LogsDisplayer.displayError(reloadUserConfiguration, NbBundle.getMessage(ConfigurationManagerService.class,
          "ValidationOptions.BilanDialogTitle"));
    }
    if (!reloadUserConfiguration.containsFatalError()) {
      getOptionsManager().reloadUserConfiguration(userConfiguration);
      final CrueCONFReaderWriter reader = new CrueCONFReaderWriter("1.2");
      final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
      final Configuration conf = new Configuration();
      conf.setUser(userConfiguration);
      reader.writeXMLMetier(new CrueIOResu<>(conf), installationService.getUserConfigFile(), log);
      if (log.isNotEmpty()) {
        LogsDisplayer.displayError(log, NbBundle.getMessage(ConfigurationManagerService.class, "UserOptionsSave.BilanDialogTitle"));
      }
    }
  }

  /**
   * Lecture du fichier de mappage de l'aide
   *
   * @return Map avec cle=idFudaaCrue et valeur=chemin help
   */
  public HashMap<String, String> readAide() {
    final CrueXmlReaderWriterImpl<CrueDaoAide, CrueDaoAideContents> rw = new CrueXmlReaderWriterImpl<>("Lien", "1.0", new CrueConverterAide(),
        new CrueDaoStructureAide());

    final CtuluLog log = new CtuluLog();
    final CrueIOResu<CrueDaoAideContents> in = rw.readXML(new File(installationService.getSiteDir(), GlobalOptionsManager.HELP_FILE), log);
    final HashMap<String, String> mapAide = new HashMap<>();

    if (in.getMetier() != null) {
      final List<CrueAide> lstAide = in.getMetier().getLstAide();
      for (final CrueAide crueAide : lstAide) {
        mapAide.put(crueAide.getIdFudaaCrue(), crueAide.getHelp());
      }
    }
    return mapAide;
  }

  /**
   * @return le chemin par defaut pour les données (C:\\DATA).
   */
  public File getDefaultDataHome() {
    //TODO a changer:
    return new File("C:\\DATA");
  }
}
