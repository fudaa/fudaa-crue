package org.fudaa.fudaa.crue.options.node;

import java.util.Collection;
import java.util.List;
import org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager;
import org.fudaa.dodico.crue.projet.conf.Option;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;

/**
 *
 * @author deniger
 */
public class OptionNodeChildFactory extends ChildFactory<Option> {

  private final GlobalOptionsManager optionsMananger;

  public OptionNodeChildFactory(GlobalOptionsManager optionsMananger) {
    this.optionsMananger = optionsMananger;
  }

  @Override
  protected boolean createKeys(List<Option> toPopulate) {
    if (toPopulate == null || optionsMananger == null) {
      return true;
    }
    final Collection<Option> visibleOptionsCloned = optionsMananger.getVisibleOptionsCloned();
    if (visibleOptionsCloned == null) {
      return true;
    }
    toPopulate.addAll(
            visibleOptionsCloned);
    return true;
  }

  @Override
  protected Node createNodeForKey(Option key) {
    return new OptionNode(key, optionsMananger.getSiteValue(key), optionsMananger.getOptionEnum(key));
  }
}
