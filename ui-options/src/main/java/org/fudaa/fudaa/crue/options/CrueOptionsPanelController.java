/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.options;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import static org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder.getDialogHelpCtxId;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

@OptionsPanelController.TopLevelRegistration(categoryName = "#OptionsCategory_Name_CrueOption",
iconBase = "org/fudaa/fudaa/crue/options/icons/option32.png",
keywords = "#OptionsCategory_Keywords_CrueOption",
keywordsCategory = "Options", position = 1)
public final class CrueOptionsPanelController extends OptionsPanelController {

  private CrueOptionsPanel panel;
  private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
  private boolean changed;

  @Override
  public void update() {
    getPanel().load();
    changed = false;
  }

  @Override
  public void applyChanges() {
    if (getPanel().valid(true)) {
      getPanel().store();
      changed = false;
    }
  }

  @Override
  public void cancel() {
    // need not do anything special, if no changes have been persisted yet
  }

  @Override
  public boolean isValid() {
    return getPanel().valid(false);
  }

  @Override
  public boolean isChanged() {
    return changed;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(getDialogHelpCtxId("bdlOptions", null)); // new HelpCtx("...ID") if you have a help set
  }

  @Override
  public JComponent getComponent(Lookup masterLookup) {
    return getPanel();
  }

  @Override
  public void addPropertyChangeListener(PropertyChangeListener l) {
    pcs.addPropertyChangeListener(l);
  }

  @Override
  public void removePropertyChangeListener(PropertyChangeListener l) {
    pcs.removePropertyChangeListener(l);
  }

  private CrueOptionsPanel getPanel() {
    if (panel == null) {
      panel = new CrueOptionsPanel(this);
      String id = getDialogHelpCtxId("bdlOptions", null);
      SysdocUrlBuilder.installHelpShortcut(panel,id);
    }
    return panel;
  }
  
  Boolean oldValid=Boolean.TRUE;

  void changed() {
    if (!changed) {
      changed = true;
      pcs.firePropertyChange(OptionsPanelController.PROP_CHANGED, false, true);
    }
    Boolean newValid=Boolean.valueOf(getPanel().valid(true));
    pcs.firePropertyChange(OptionsPanelController.PROP_VALID, oldValid, newValid);
    oldValid=newValid;
  }
}
