package org.fudaa.fudaa.crue.options.node;

import java.util.List;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;
import org.fudaa.dodico.crue.config.coeur.CoeurManager;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;

/**
 *
 * @author deniger
 */
public class CoeurNodeChildFactory extends ChildFactory<CoeurConfig> {
  
  private final CoeurManager coeurManager;
  
  public CoeurNodeChildFactory(CoeurManager optionsMananger) {
    this.coeurManager = optionsMananger;
  }
  
  @Override
  protected boolean createKeys(List<CoeurConfig> toPopulate) {
    toPopulate.addAll(
            coeurManager.getAllCoeurConfigs());
    return true;
  }
  
  @Override
  protected Node createNodeForKey(CoeurConfig key) {
    return new CoeurNode(key);
  }
}
