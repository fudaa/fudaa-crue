/*
 GPL 2
 */
package org.fudaa.fudaa.crue.options.config;

import java.util.Map;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;
import org.fudaa.fudaa.crue.common.config.ConfigDefaultValuesProvider;

/**
 * @author Frederic Deniger
 */
public class ModellingGlobalConfiguration implements ConfigDefaultValuesProvider {

  public static final String PROP_DEFAULT_LITMINEUR = "defaultLongueurLitMineur";
  public static final String SEUIL_SIMPLIF_PROFIL_SECTION = "seuilSimplifProfilSection";
  public static final String SEUIL_SIMPLIF_PROFIL_CASIER = "seuilSimplifProfilCasier";
  Double defaultLongueurLitMineur = null;//par défaut vaut null.
  Double seuilSimplifProfilSection = null;//par défaut vaut null.
  Double seuilSimplifProfilCasier = null;//par défaut vaut null.

  public ModellingGlobalConfiguration() {
  }

  public ModellingGlobalConfiguration(ModellingGlobalConfiguration from) {
    if (from != null) {
      defaultLongueurLitMineur = from.defaultLongueurLitMineur;
      seuilSimplifProfilSection = from.seuilSimplifProfilSection;
      seuilSimplifProfilCasier = from.seuilSimplifProfilCasier;
    }
  }

  public ModellingGlobalConfiguration copy(){
    return new ModellingGlobalConfiguration(this);
  }

  @Override
  public CreationDefaultValue getDefaultValues() {
    CreationDefaultValue res = new CreationDefaultValue();
    res.setDefaultLitMineurLength(defaultLongueurLitMineur);
    return res;
  }

  public Double getDefaultLongueurLitMineur() {
    return defaultLongueurLitMineur;
  }

  public Double getSeuilSimplifProfilSection() {
    return seuilSimplifProfilSection;
  }

  public void setSeuilSimplifProfilSection(Double seuilSimplifProfilSection) {
    if (seuilSimplifProfilSection > 0) {
      this.seuilSimplifProfilSection = seuilSimplifProfilSection;
    }
  }

  public Double getSeuilSimplifProfilCasier() {
    return seuilSimplifProfilCasier;
  }

  public void setSeuilSimplifProfilCasier(Double seuilSimplifProfilCasier) {
    if (seuilSimplifProfilCasier > 0) {
      this.seuilSimplifProfilCasier = seuilSimplifProfilCasier;
    }
  }

  public static void replaceValuesIfNeeded(Map<String, String> props, CrueConfigMetier ccm) {
    String initValue = props.get(PROP_DEFAULT_LITMINEUR);
    if (initValue == null) {
      initValue = ccm.getDefaultValue(CrueConfigMetierConstants.PROP_DEFAULT_DX_XT).toString();
      props.put(PROP_DEFAULT_LITMINEUR, initValue);
    }
    initValue = props.get(SEUIL_SIMPLIF_PROFIL_SECTION);
    if (initValue == null && ccm.getDefaultValue(CrueConfigMetierConstants.PROP_DEFAULT_SEUILSIMPLIFPROFILSECTION) != null) {
      initValue = ccm.getDefaultValue(CrueConfigMetierConstants.PROP_DEFAULT_SEUILSIMPLIFPROFILSECTION).toString();
      props.put(SEUIL_SIMPLIF_PROFIL_SECTION, initValue);
    }
    initValue = props.get(SEUIL_SIMPLIF_PROFIL_CASIER);
    if (initValue == null && ccm.getDefaultValue(CrueConfigMetierConstants.PROP_DEFAULT_SEUILSIMPLIFPROFILCASIER) != null) {
      initValue = ccm.getDefaultValue(CrueConfigMetierConstants.PROP_DEFAULT_SEUILSIMPLIFPROFILCASIER).toString();
      props.put(SEUIL_SIMPLIF_PROFIL_CASIER, initValue);
    }
  }

  public void setDefaultLongueurLitMineur(Double defaultLongueurLitMineur) {
    if (defaultLongueurLitMineur > 0) {
      this.defaultLongueurLitMineur = defaultLongueurLitMineur;
    }
  }

  void initValues(CrueConfigMetier propDefinition) {
    defaultLongueurLitMineur = propDefinition.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_DEFAULT_DX_XT);
    seuilSimplifProfilCasier = propDefinition.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_DEFAULT_SEUILSIMPLIFPROFILCASIER);
    seuilSimplifProfilSection = propDefinition.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_DEFAULT_SEUILSIMPLIFPROFILSECTION);
  }
}
