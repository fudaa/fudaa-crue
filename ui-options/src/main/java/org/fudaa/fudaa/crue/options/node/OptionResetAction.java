package org.fudaa.fudaa.crue.options.node;

import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;

/**
 *
 * @author deniger
 */
public class OptionResetAction extends NodeAction {

  final String name = NbBundle.getMessage(OptionResetAction.class, "OptionResetAction.Name");

  @Override
  protected boolean enable(Node[] activatedNodes) {
    if (activatedNodes.length > 0) {
      for (Node node : activatedNodes) {
        OptionNode optionNode = (OptionNode) node;
        if (optionNode.getOption().isEditable()) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    for (Node node : activatedNodes) {
      OptionNode optionNode = (OptionNode) node;
      if (optionNode.getOption().isEditable()) {
        optionNode.setOptionValue(optionNode.getDefaultValue());
      }
    }
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  public String getName() {
    return name;
  }
}
