/*
 GPL 2
 */
package org.fudaa.fudaa.crue.options.config;

import org.fudaa.fudaa.crue.common.config.ConfigurationInfoHelper;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ModellingGlobalConfigurationInfo {

  public static Sheet.Set createSetDefaultLongeur(ModellingGlobalConfiguration in) {
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setName("profil");//attention, utilise pour la persistence donc ne pas changer sans vérifier les tests.
    set.setDisplayName(NbBundle.getMessage(ModellingGlobalConfigurationInfo.class, "ModellingGlobalConfiguration.Profil.DisplayName"));

    try {
      set.put(createDefaultLongueurProfil(in));
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return set;
  }

  public static Sheet.Set createSetAll(ModellingGlobalConfiguration in) {
    Sheet.Set set = createSetDefaultLongeur(in);
    try {
      set.put(ConfigurationInfoHelper.create(ModellingGlobalConfiguration.SEUIL_SIMPLIF_PROFIL_SECTION,
              Double.class, in, ModellingGlobalConfiguration.class));
      set.put(ConfigurationInfoHelper.create(ModellingGlobalConfiguration.SEUIL_SIMPLIF_PROFIL_CASIER,
              Double.class, in, ModellingGlobalConfiguration.class));
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return set;
  }

  private static PropertySupport.Reflection createDefaultLongueurProfil(ModellingGlobalConfiguration in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(ModellingGlobalConfiguration.PROP_DEFAULT_LITMINEUR,
            Double.class, in, ModellingGlobalConfiguration.class);
    return res;
  }
}
