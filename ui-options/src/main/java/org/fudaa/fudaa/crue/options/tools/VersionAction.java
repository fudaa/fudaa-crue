/*
 GPL 2
 */
package org.fudaa.fudaa.crue.options.tools;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;

@ActionID(category = "Tools", id = "org.fudaa.fudaa.crue.options.tools.issue.VersionAction")
@ActionRegistration(
    displayName = "#CTL_VersionAction")
@ActionReference(path = "Menu/Tools", position = 12)
@Messages("CTL_VersionAction=Historique des Versions")
public final class VersionAction implements ActionListener {

  @Override
  public void actionPerformed(ActionEvent e) {
    File siteDirectory = Lookup.getDefault().lookup(InstallationService.class).getSiteDir();
    File version = new File(siteDirectory, "/historique/versions.xml");
    try {        
        if(version.exists())
        {
            URL versionUrl =  new URL("file:///" + version);  
            Desktop.getDesktop().browse(versionUrl.toURI());
        }
    } catch (Exception ex) {
      Exceptions.printStackTrace(ex);
    }
  }
}
