package org.fudaa.fudaa.crue.options.services;

import java.awt.EventQueue;
import java.io.File;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.calcul.CalculExec;
import org.fudaa.dodico.calcul.CalculExecDefault;
import org.fudaa.dodico.crue.projet.conf.Option;
import org.fudaa.dodico.crue.projet.conf.OptionsEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.UiContext;
import static org.fudaa.fudaa.crue.options.services.Bundle.*;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;

/**
 *
 * @author Fred Deniger
 */
public class OpenFileProcess {

  final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

  @Messages({
    "OpenFichier.EditorFileIsNotConfigured=Vous devez ouvrir les options ( Menu Outils) pour configurer l'\u00e9diteur \u00e0 utilisé",
    "ExterneEditorFailed=<html><body>L''\u00e9diteur externe ne peut pas \u00eatre lanc\u00e9. L''\u00e9diteur utilis\u00e9 est:<br><ul><li>{0}</li></ul></body></html>", 
    "OpenFile.Error=Erreur: ouverture d'un fichier"})
  public void openFiles(final List<File> files)   {
    final Option option = configurationManagerService.getOptionsManager().getOption(OptionsEnum.EDITOR);
    if (option == null || StringUtils.isEmpty(option.getValeur())) {
      DialogHelper.showError(OpenFile_Error(), OpenFichier_EditorFileIsNotConfigured());
      return;
    }
    final CalculExec exec = new CalculExecDefault(new String[]{option.getValeur()});
    exec.setLauchInNewTerm(false);
    exec.setStartCmdUse(false);
    exec.setShowErrorInUI(false);
    exec.setUI(new UiContext());
    new Thread() {
      @Override
      public void run() {
        boolean launch = true;
        for (File file : files) {
          launch = exec.launch(file, null);
          if (!launch) {
            break;
          }
        }
        if (!launch) {
          EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
              DialogHelper.showError(ExterneEditorFailed(option.getValeur()));
            }
          });

        }
      }
    }.start();
  }
}
