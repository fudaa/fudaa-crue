/*
 GPL 2
 */
package org.fudaa.fudaa.crue.options.config;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.fudaa.ebli.courbe.EGAxeHorizontalPersist;
import org.fudaa.ebli.courbe.EGAxeVerticalPersist;
import org.fudaa.ebli.courbe.EGCourbePersist;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Frederic Deniger
 */
@XStreamAlias("Courbe-Configuration")
public class CourbeConfig {

  @XStreamAlias("Courbe-Configs")
  private final Map<String, EGCourbePersist> courbeByKey = new HashMap<>();
  @XStreamAlias("Axe-Horizontal")
  private EGAxeHorizontalPersist horizontalPersist;
  @XStreamAlias("Axes-Verticaux")
  private Map<String, EGAxeVerticalPersist> axeByNature = new HashMap<>();

  public void reinitContent() {
    if (axeByNature == null) {
      axeByNature = new HashMap<>();
    }
  }

  public EGCourbePersist getCourbeConfig(Object key) {
    if (key != null) {
      return courbeByKey.get(key.toString());
    }
    return null;
  }

  public void saveAxeH(EGAxeHorizontalPersist horizontalPersist) {
    this.horizontalPersist = horizontalPersist;
  }

  public EGAxeHorizontalPersist getHorizontalPersist() {
    return horizontalPersist;
  }

  public void saveConfig(Object key, EGCourbePersist persist) {
    if (key != null) {
      courbeByKey.put(key.toString(), persist);
    }
  }

  public EGAxeVerticalPersist getAxeVConfig(Object key) {
    if (key == null) {
      return null;
    }
    return axeByNature.get(key.toString());
  }

  public void saveAxeVConfig(Object key, EGAxeVerticalPersist persist) {
    if (key == null) {
      return;
    }
    axeByNature.put(key.toString(), persist);
  }

  /**
   * @return a copy of CourbConfig by cloning all Persist data.
   */
  public CourbeConfig createCopy() {
    CourbeConfig copy = new CourbeConfig();
    if (horizontalPersist != null) {
      copy.horizontalPersist = horizontalPersist.copy();
    }
    axeByNature.forEach((key, value) -> copy.axeByNature.put(key, value.copy()));
    courbeByKey.forEach((key, value) -> copy.courbeByKey.put(key, value.duplicate()));
    return copy;
  }
}
