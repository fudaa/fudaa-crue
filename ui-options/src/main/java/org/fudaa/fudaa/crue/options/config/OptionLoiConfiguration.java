/*
 GPL 2
 */
package org.fudaa.fudaa.crue.options.config;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Frederic Deniger
 */
public class OptionLoiConfiguration {

  Map<String, CourbeConfig> configByTopComponentId = new HashMap<>();

  public CourbeConfig getCourbeConfig(String topComponentId) {
    return configByTopComponentId.get(topComponentId);
  }

  public Map<String, CourbeConfig> getConfigByTopComponentId() {
    return Collections.unmodifiableMap(configByTopComponentId);
  }

  public void setCourbeConfig(String topComponentId, CourbeConfig config) {
    configByTopComponentId.put(topComponentId, config);
  }
}
