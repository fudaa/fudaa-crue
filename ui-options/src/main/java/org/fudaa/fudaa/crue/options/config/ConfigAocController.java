/*
 GPL 2
 */
package org.fudaa.fudaa.crue.options.config;

import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.conf.CrueAocConfigurationHelper;
import org.fudaa.dodico.crue.io.conf.CrueAocDaoConfiguration;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Un controlleur gerant les configurations des lois aoc.
 *
 * @author Frederic Deniger
 */
public class ConfigAocController {
    OptionLoiConfiguration aocLoiConfiguration;
    final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

    public static void replaceLoiGlobalValuesBySpecific(Map loiProperties, CrueAocDaoConfiguration studySpecificConfiguration) {
        Map<String, ?> customLoiProperties = studySpecificConfiguration.getCourbeConfiguration();
        for (Map.Entry<String, ?> entry : customLoiProperties.entrySet()) {
            final Object value = entry.getValue();
            loiProperties.put(entry.getKey(), value);
        }
    }

    public OptionLoiConfiguration getAocLoiConfiguration() {
        return aocLoiConfiguration;
    }

    public void setAocLoiConfiguration(OptionLoiConfiguration aocLoiConfiguration) {
        this.aocLoiConfiguration = aocLoiConfiguration;
    }

    public void loadConfigEtude(EMHProjet projet) {
        aocLoiConfiguration = new OptionLoiConfiguration();
        try {

            //on recupère les données par defaut
            final File dirOfConfig = projet.getInfos().getDirOfConfig();
            CrueAocDaoConfiguration readDefaultEtudeConfig = configurationManagerService.readDefaultAocConfig();
            Map loiProperties = new HashMap();
            if (readDefaultEtudeConfig != null) {
                loiProperties = new HashMap(readDefaultEtudeConfig.getCourbeConfiguration());
            }
            if (projet.getInfos().isDirOfConfigDefined()) {
                File configEtu = new File(dirOfConfig, GlobalOptionsManager.ETUDE_AOC_FILE);
                if (configEtu.exists()) {
                    final CrueAocDaoConfiguration studySpecificConfiguration = readSpecificConfigurationFile(configEtu);
                    replaceLoiGlobalValuesBySpecific(loiProperties, studySpecificConfiguration);
                }
            }
            aocLoiConfiguration.configByTopComponentId = ConfigEtudeController.createCourbeConfigById(loiProperties);
        } catch (Exception e) {
            Exceptions.printStackTrace(e);
        }
    }

    public CrueAocDaoConfiguration readSpecificConfigurationFile(File configEtu) {
        CrueIOResu<CrueAocDaoConfiguration> read = CrueAocConfigurationHelper.read(configEtu, new ModellingLoiConfigurationConverter());
        if (read.getAnalyse().containsErrorOrSevereError()) {
            LogsDisplayer.displayError(read.getAnalyse(), configEtu.getAbsolutePath());
        }
        return read.getMetier();
    }

    public void unload() {
        aocLoiConfiguration = null;

    }
}
