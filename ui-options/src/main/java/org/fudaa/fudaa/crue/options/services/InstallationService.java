package org.fudaa.fudaa.crue.options.services;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.common.ConnexionInformationDefault;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ServiceProvider;
import org.openide.windows.WindowManager;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Service donnant les informations de l'installation: les repertoires, les fichiers de configuration...<br>
 * Service appelé au démarrage de l'application Fudaa-Crue par {@link org.fudaa.fudaa.crue.options.CrueConfigurationInstaller}
 * <br>
 * Le lookup n'est utilisé que pour envoyer un evenement au demarrage de l'application indiquant que le dossier <i>etc</i> et les fichiers de configuration
 * ont été trouves. Si les fichiers de configuration ne sont pas trouvés, les fichiers par défaut sont extraits de l'application.
 * Voir la fonction {@link #load() }.
 * <br><br>
 * <table  border="1"  cellpadding="3" cellspacing="0"  summary="lookups">
 * <tr>
 * <th align="left">Lookup</th>
 * <th align="left">Commentaire</th>
 * <th align="left">Méthode utilisant le lookup</th>
 * </tr>
 * <tr>
 * <td>
 * {@link java.io.File}</td>
 * <td>
 * Le manager des coeurs de calcul. Contien le resultat de {@link #getSiteConfigFile()}.
 * Ce lookup est utilise pour envoyer des events au chargement mais pas utilisé directement.
 * </td>
 * <td>{@link #load()}</td>
 * </table>
 *
 * @author Christophe CANEL
 * @see org.fudaa.fudaa.crue.options.CrueConfigurationInstaller
 */
@ServiceProvider(service = InstallationService.class)
public class InstallationService implements Lookup.Provider {
    private static final Logger LOGGER = Logger.getLogger(InstallationService.class.getName());
    /**
     * Information de connexion par defaut.
     */
    private final ConnexionInformation connexionInformation = ConnexionInformationDefault.getDefault();
    /**
     * le contenu du lookup de ce service
     */
    private final InstanceContent dynamicContent = new InstanceContent();
    /**
     * Lookup de ce service
     */
    private final Lookup lookup = new AbstractLookup(dynamicContent);
    private File etcDir = null;

    /**
     * @param source recupération de lastModified
     * @param target la propriete lastModified sera initialisee avec celle de source
     */
    private static void preserveLastTime(FileObject source, FileObject target) {
        if (source != null && target != null) {
            Date d = source.lastModified();
            File toFile = FileUtil.toFile(target);
            if (toFile != null && d != null) {
                CrueFileHelper.setLastModified(toFile,d.getTime(),InstallationService.class);
            }
        }
    }

    /**
     * @return Information de connexion par defaut. Jamais null
     */
    public ConnexionInformation getConnexionInformation() {
        return connexionInformation;
    }

    /**
     * Contient le fichier FudaaCrue_Site.xml
     */
    @Override
    public Lookup getLookup() {
        return lookup;
    }

    /**
     * @return le repertoire de configuration de l'utilisateur. La facon la plus "normale" selon netbeans ?
     */
    public File getConfigUserDir() {
        final FileObject configRoot = FileUtil.getConfigRoot();
        return FileUtil.toFile(configRoot);
    }

    public File getUserDir() {
        return new File(System.getProperty("netbeans.user")).getParentFile();
    }

    public void load() {
        File crueSiteFile = lookup.lookup(File.class);
        if (crueSiteFile != null) {
            dynamicContent.remove(crueSiteFile);
        }
        File siteDir = getSiteDir();
        if (!CtuluLibFile.exists(siteDir)) {
            LOGGER.log(Level.SEVERE, "fudaacrue.conf not found. Must be in the <INSTALL>/etc folder");
            WindowManager.getDefault().invokeWhenUIReady(() -> DialogHelper.showError(NbBundle.getMessage(InstallationService.class, "Installation.NoFudaaCrueConfError")));
        }
        final File siteConfig = getSiteConfigFile();
        final File etuConfig = getEtudeConfigFile();
        final File etuAocConfig = getEtudeAocConfigFile();
        final File coeurDir = new File(getSiteDir(), "coeurs");

        if (!etuConfig.exists()) {
            createDefaultConfigEtude();
        }
        if (!etuAocConfig.exists()) {
            createDefaultConfigEtudeAoc();
        }
        if (!siteConfig.exists()) {
            createDefaultFiles();
            WindowManager.getDefault().invokeWhenUIReady(() -> DialogHelper.showNotifyOperationTermine(NbBundle.getMessage(InstallationService.class,
                    "Installation.CreateDefaultSiteAndCoeur.DialogTitle"),
                    NbBundle.getMessage(InstallationService.class,
                            "Installation.CreateDefaultSiteAndCoeur.DialogContent",
                            siteConfig.getAbsolutePath(), coeurDir.getAbsoluteFile())));
        }

        dynamicContent.add(siteConfig);
    }

    public File getSiteConfigFile() {
        return new File(getSiteDir(), GlobalOptionsManager.SITE_FILE);
    }

    public File getEtudeConfigFile() {
        return new File(getSiteDir(), GlobalOptionsManager.ETUDE_FILE);
    }

    public File getEtudeAocConfigFile() {
        return new File(getSiteDir(), GlobalOptionsManager.ETUDE_AOC_FILE);
    }

    public File getUserConfigFile() {
        File userDir = getConfigUserDir();
        if (userDir == null) {
            String property = System.getProperty("dev.userDir");
            if (property != null) {
                userDir = new File(property);
            }
        }
        return new File(userDir, GlobalOptionsManager.USER_FILE);
    }

    public File getSiteDir() {
        if (etcDir == null) {
            String property = System.getProperty("dev.etcDir");
            if (property != null) {
                Logger.getLogger(InstallationService.class.getName()).log(Level.WARNING, "custom etc dir used: {0} ", property);
                etcDir = new File(property);
            } else {
                final String netbeansHome = System.getProperty("netbeans.home");
                if (netbeansHome != null) {
                    File platformDir = new File(netbeansHome);
                    etcDir = new File(platformDir.getParentFile(), "etc");
                }
            }
        }
        return etcDir;
    }

    private void createDefaultFiles() {
        //voir layer.xml pour la définition
        FileObject fo = FileUtil.getConfigFile("DefaultConfig/FudaaCrue_Site.xml");
        final File siteDir = getSiteDir();
        if (fo != null) {
            try {
                FileObject copy = fo.copy(FileUtil.createFolder(siteDir), "FudaaCrue_Site", "xml");
                preserveLastTime(fo, copy);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
        fo = FileUtil.getConfigFile("DefaultConfig/FudaaCrue_Aide.xml");
        if (fo != null) {
            try {
                FileObject copy = fo.copy(FileUtil.createFolder(siteDir), "FudaaCrue_Aide", "xml");
                preserveLastTime(fo, copy);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
        fo = FileUtil.getConfigFile("DefaultConfig/default-coeurs.zip");
        if (fo != null) {
            try {
                FileObject copy = fo.copy(FileUtil.createFolder(siteDir), "default-coeurs", "zip");
                File destFile = FileUtil.toFile(copy);
                CtuluLibFile.unzip(destFile, new File(siteDir, "coeurs"), null);
                CrueFileHelper.deleteFile(destFile,getClass());
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
        fo = FileUtil.getConfigFile("DefaultConfig/comparaisons.zip");
        if (fo != null) {
            try {
                FileObject copy = fo.copy(FileUtil.createFolder(siteDir), "comparaisons", "zip");
                File destFile = FileUtil.toFile(copy);
                CtuluLibFile.unzip(destFile, siteDir, null);
                CrueFileHelper.deleteFile(destFile,getClass());
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
        fo = FileUtil.getConfigFile("DefaultConfig/report-templates.zip");
        if (fo != null) {
            try {
                FileObject copy = fo.copy(FileUtil.createFolder(siteDir), "report-templates", "zip");
                File destFile = FileUtil.toFile(copy);
                CtuluLibFile.unzip(destFile, siteDir, null);
                CrueFileHelper.deleteFile(destFile,getClass());
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
        //voir layer.xml pour la définition
        fo = FileUtil.getConfigFile("DefaultConfig/default-colors.xlsx");
        if (fo != null) {
            try {
                FileObject copy = fo.copy(FileUtil.createFolder(siteDir), "default-colors", "xlsx");
                preserveLastTime(fo, copy);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    private void createDefaultConfigEtude() {
        //voir layer.xml pour la définition
        FileObject fo = FileUtil.getConfigFile("DefaultConfig/FudaaCrue_Etude.xml");
        final File siteDir = getSiteDir();
        if (fo != null) {
            try {
                FileObject copy = fo.copy(FileUtil.createFolder(siteDir), "FudaaCrue_Etude", "xml");
                preserveLastTime(fo, copy);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    private void createDefaultConfigEtudeAoc() {
        //voir layer.xml pour la définition
        FileObject fo = FileUtil.getConfigFile("DefaultConfig/FudaaCrue_Etude_Aoc.xml");
        final File siteDir = getSiteDir();
        if (fo != null) {
            try {
                FileObject copy = fo.copy(FileUtil.createFolder(siteDir), "FudaaCrue_Etude_Aoc", "xml");
                preserveLastTime(fo, copy);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }
}
