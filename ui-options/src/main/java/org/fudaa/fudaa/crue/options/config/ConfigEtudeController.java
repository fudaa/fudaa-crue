/*
 GPL 2
 */
package org.fudaa.fudaa.crue.options.config;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.conf.CrueEtudeConfigurationHelper;
import org.fudaa.dodico.crue.io.conf.CrueEtudeDaoConfiguration;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager;
import org.fudaa.fudaa.crue.common.config.ConfigSaverHelper;
import org.fudaa.fudaa.crue.common.config.ConfigurationInfoHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Frederic Deniger
 */
public class ConfigEtudeController {
    ModellingGlobalConfiguration modellingGlobalConfiguration;
    OptionLoiConfiguration modellingLoiConfiguration;
    final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

    public static Map createCourbeConfigById(Map loiProperties) {
        Map loiConfigByValues = new HashMap();
        Set<Map.Entry> entrySet = loiProperties.entrySet();
        for (Map.Entry entry : entrySet) {
            Object value = entry.getValue();
            if (value instanceof CourbeConfig) {
                loiConfigByValues.put(entry.getKey(), value);
            }
        }
        return loiConfigByValues;
    }

    public static void replaceLoiGlobalValuesBySpecific(Map loiProperties, CrueEtudeDaoConfiguration studySpecificConfiguration) {
        Map<String, ?> customLoiProperties = studySpecificConfiguration.getCourbeConfiguration();
        for (Map.Entry<String, ?> entry : customLoiProperties.entrySet()) {
            final Object value = entry.getValue();
            loiProperties.put(entry.getKey(), value);
        }
    }

    public ModellingGlobalConfiguration getModellingGlobalConfiguration() {
        return modellingGlobalConfiguration;
    }

    public void setModellingGlobalConfiguration(ModellingGlobalConfiguration modellingGlobalConfiguration) {
        this.modellingGlobalConfiguration = modellingGlobalConfiguration;
    }

    public OptionLoiConfiguration getModellingLoiConfiguration() {
        return modellingLoiConfiguration;
    }

    public void setModellingLoiConfiguration(OptionLoiConfiguration modellingLoiConfiguration) {
        this.modellingLoiConfiguration = modellingLoiConfiguration;
    }

    public void loadConfigEtude(EMHProjet projet) {
        modellingGlobalConfiguration = new ModellingGlobalConfiguration();
        modellingGlobalConfiguration.initValues(projet.getPropDefinition());
        modellingLoiConfiguration = new OptionLoiConfiguration();
        try {

            //on recupère les données par defaut
            CrueEtudeDaoConfiguration readDefaultEtudeConfig = configurationManagerService.readDefaultEtudeConfig();
            Map<String, String> globalProperties = CrueEtudeConfigurationHelper.getProperties(readDefaultEtudeConfig.getGlobalConfigurationOptions());
            Map loiProperties = new HashMap(readDefaultEtudeConfig.getCourbeConfiguration());
            if (projet.getInfos().isDirOfConfigDefined()) {
                final File dirOfConfig = projet.getInfos().getDirOfConfig();
                File configEtu = new File(dirOfConfig, GlobalOptionsManager.ETUDE_FILE);
                if (configEtu.exists()) {
                    final CrueEtudeDaoConfiguration studySpecificConfiguration = readSpecificConfigurationFile(configEtu);
                    replaceGlobalValuesBySpecificValues(globalProperties, studySpecificConfiguration);
                    replaceLoiGlobalValuesBySpecific(loiProperties, studySpecificConfiguration);
                }
            }
            CtuluLog logGlobal = configureGlobalConfiguration(projet, globalProperties);
            modellingLoiConfiguration.configByTopComponentId = createCourbeConfigById(loiProperties);

            if (logGlobal.containsErrorOrSevereError()) {
                LogsDisplayer.displayError(logGlobal, NbBundle.getMessage(getClass(), "configEtude.error"));
            }
        } catch (Exception e) {
            Exceptions.printStackTrace(e);
        }
    }

    public CtuluLog configureGlobalConfiguration(EMHProjet projet, Map<String, String> globalProperties) {
        ModellingGlobalConfiguration.replaceValuesIfNeeded(globalProperties, projet.getPropDefinition());
        Sheet.Set globalSet = ModellingGlobalConfigurationInfo.createSetAll(modellingGlobalConfiguration);
        LinkedHashMap<String, Node.Property> globalMap = ConfigSaverHelper.getProperties(globalSet);
        return ConfigurationInfoHelper.setAndValidReadProperties(globalProperties, globalMap, CtuluLogLevel.WARNING);
    }

    public void replaceGlobalValuesBySpecificValues(Map<String, String> globalProperties, CrueEtudeDaoConfiguration studySpecificConfiguration) {
        Map<String, String> customGlobalProperties = CrueEtudeConfigurationHelper.getProperties(studySpecificConfiguration.getGlobalConfigurationOptions());
        //on remplace la valeur
        for (Map.Entry<String, String> entry : customGlobalProperties.entrySet()) {
            globalProperties.put(entry.getKey(), entry.getValue());
        }
    }

    public CrueEtudeDaoConfiguration readSpecificConfigurationFile(File configEtu) {
        CrueIOResu<CrueEtudeDaoConfiguration> read = CrueEtudeConfigurationHelper.read(configEtu, new ModellingLoiConfigurationConverter());
        if (read.getAnalyse().containsErrorOrSevereError()) {
            LogsDisplayer.displayError(read.getAnalyse(), configEtu.getAbsolutePath());
        }
        return read.getMetier();
    }

    public Map<String, String> getGlobalOptions() {
        LinkedHashMap<String, Node.Property> properties = ConfigSaverHelper.getProperties(ModellingGlobalConfigurationInfo.createSetAll(
                modellingGlobalConfiguration));
        return ConfigSaverHelper.transform(properties);
    }


    public void unload() {
        modellingGlobalConfiguration = null;
        modellingLoiConfiguration = null;
    }
}
