Si le fichier default-coeurs.zip est généré avec 7Zip ou Windows Zip il n'est pas "ouvrable" par Java.

Pour générer ce fichier utiliser la classe GenerateZip qui est dans test: cette classe attend que le dossier 
`ui-options/src/main/resources/default-coeurs` existe. Il va générer le zip `ui-options/src/main/resources/default-coeurs.zip`


Le contenu de cette archives sera utilisé pour générer le dossier `C:\data\Fudaa-Crue\etc` si ce dernier n'exist pas.
Voir le fichier `ui-application/src/main/resources/etc/fudaacrueDev.conf` (copié depuis fudaacrueDevExample.conf) pour 
la définition de ce dossier.



Si vous voulez utiliser le contenu du zip `default-coeurs.zip` pour l'application Fudaa-Crue, il faudra effacer le dossier
`C:\data\Fudaa-Crue\etc` et redémarrer Fudaa-Crue. Ce dossier sera automatiquement recréé avec le contenu de l'archive.