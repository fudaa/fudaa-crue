import org.fudaa.ctulu.CtuluLibFile;

import java.io.File;
import java.io.IOException;

public class GenerateZip {


  public static void main(String[] args) throws IOException {

    File moduleDir=new File(GenerateZip.class.getResource(".").getFile()).getParentFile().getParentFile();
    File folderWithCoeursZip=new File(moduleDir,"src"+File.separator+"main"+File.separator+"resources").getCanonicalFile();
    System.out.println("Will work in the folder "+folderWithCoeursZip);

    File sourceFolder=new File(folderWithCoeursZip,"default-coeurs");
    File target=new File(folderWithCoeursZip,"default-coeurs.zip");
    if(!CtuluLibFile.exists(sourceFolder) || !sourceFolder.isDirectory()){
      System.err.println("The folder "+sourceFolder+" must exist");
      System.exit(1);
    }
    if(!CtuluLibFile.exists(target)){
      System.err.println("The targetFile "+target+" must exist");
      System.exit(1);
    }
    System.out.println("The zip file "+target+" will be replaced by the zip archive of the folder "+sourceFolder);
    CtuluLibFile.zip(sourceFolder,target,null);
    System.out.println("Done");

  }
}
