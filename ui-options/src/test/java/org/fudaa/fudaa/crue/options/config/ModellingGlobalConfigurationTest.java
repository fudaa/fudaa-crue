package org.fudaa.fudaa.crue.options.config;

import org.junit.Assert;
import org.junit.Test;
public class ModellingGlobalConfigurationTest  {

  @Test
  public void testCopy() {
    ModellingGlobalConfiguration init=new ModellingGlobalConfiguration();
    init.setDefaultLongueurLitMineur(2d);
    init.setSeuilSimplifProfilCasier(3d);
    init.setSeuilSimplifProfilSection(4d);

    ModellingGlobalConfiguration copy=init.copy();
    Assert.assertNotSame(copy,init);
    Assert.assertEquals(init.getDefaultLongueurLitMineur().intValue(),copy.getDefaultLongueurLitMineur().intValue());
    Assert.assertEquals(init.getSeuilSimplifProfilSection().intValue(),copy.getSeuilSimplifProfilSection().intValue());
    Assert.assertEquals(init.getSeuilSimplifProfilCasier().intValue(),copy.getSeuilSimplifProfilCasier().intValue());


  }
}