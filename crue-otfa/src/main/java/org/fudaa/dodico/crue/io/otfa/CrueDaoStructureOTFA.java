package org.fudaa.dodico.crue.io.otfa;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureOptions;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;

/**
 * @author CANEL Christophe
 *
 */
public class CrueDaoStructureOTFA implements CrueDataDaoStructure {

  /**
   * {@inheritDoc}
   */
  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    xstream.alias("OTFA", CrueDaoOTFA.class);
    xstream.aliasAttribute(CrueDaoOTFA.class, "ReferenceOptions", "Reference-Options");
    xstream.aliasAttribute(CrueDaoOTFA.class, "CibleOptions", "Cible-Options");
    xstream.omitField(CrueDaoOTFA.class, "Comparaison-Options");//compatibilité avec versions précédentes
    new CrueDaoStructureOptions().configureXStream(xstream, ctuluLog, props);

  }
}
