package org.fudaa.dodico.crue.io.rtfa;

import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;

/**
 * @author CANEL Christophe
 *
 */
public class CrueRTFAReaderWriter extends CrueDataXmlReaderWriterImpl<CrueDaoRTFA, RTFAResultLines> {

  public static final String LAST_VERSION = "1.3";

  public CrueRTFAReaderWriter(final String version) {
    super("rtfa", version, new CrueConverterRTFA(), new CrueDaoStructureRTFA());
  }
}
