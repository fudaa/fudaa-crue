/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.line;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.loi.LoiTypeContainer;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureLog;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;

/**
 *
 * @author Frederic Deniger
 */
public class CrueDaoStructureLineLog implements CrueDataDaoStructure {

  @Override
  public void configureXStream(XStream xstream, CtuluLog ctuluLog, LoiTypeContainer props) {
    new CrueDaoStructureLog().configureXStream(xstream, ctuluLog, props);
    xstream.alias("CTFA", org.fudaa.dodico.crue.io.line.CrueDaoLineLog.class);
  }
}
