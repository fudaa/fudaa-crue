package org.fudaa.dodico.crue.io.rtfa;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.io.common.CrueConverterCommonLog;
import org.fudaa.dodico.crue.io.common.CrueConverterCommonParameter;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureLog;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.rtfa.CrueDaoStructureRTFA.LigneRTFA;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneResultHeader;

/**
 * @author CANEL Christophe
 */
public class CrueConverterRTFA implements CrueDataConverter<CrueDaoRTFA, RTFAResultLines> {

  private static OtfaCampagneResultHeader convertDaoToMetierHeader(final CrueDaoRTFA dao, final CtuluLog analyser) {
    final OtfaCampagneResultHeader metier = new OtfaCampagneResultHeader();

    metier.setCommentaire(dao.getCommentaire());
    if (dao.Header != null) {
      metier.setDate(DateDurationConverter.getDate(dao.Header.Date));
      metier.setGlobalValidation(CrueConverterCommonLog.convertDaoToMetier(dao.Header.LoggerGroup));
    }
    return metier;
  }

  private static RTFAResultLine convertDaoToMetierLine(final LigneRTFA dao, final CtuluLog analyser) {
    final RTFAResultLine metier = new RTFAResultLine();

    metier.setInitialLine(CrueConverterCommonParameter.convertCampagneLineDaoToMetier(dao.LigneCampagne));
    metier.setNbDifferences(dao.nbDifferences);
    metier.setNbSevereError(dao.nbSevereError);
    metier.setNbError(dao.nbError);
    metier.setNbWarn(dao.nbWarn);
    metier.setNbInfo(dao.nbInfo);
    return metier;
  }

  private static void convertMetierToDaoHeader(final CrueDaoRTFA target, final RTFAResultLines metier, final CtuluLog analyser) {
    target.Header = new CrueDaoStructureLog.Header();
    if (metier.getHeader() != null) {
      target.setCommentaire(metier.getHeader().getCommentaire());
      target.Header.Date = DateDurationConverter.dateToXsd(metier.getHeader().getDate());
      target.Header.LoggerGroup = CrueConverterCommonLog.convertMetierToDao(metier.getHeader().getGlobalValidation());
    }
  }

  private static LigneRTFA convertMetierToDaoLine(final RTFAResultLine metier, final CtuluLog analyser) {
    final LigneRTFA dao = new LigneRTFA();

    dao.LigneCampagne = CrueConverterCommonParameter.convertCampagneLineMetierToDao(metier.getInitialLine());
    dao.nbDifferences = metier.getNbDifferences();
    dao.nbSevereError = metier.getNbSevereError();
    dao.nbError = metier.getNbError();
    dao.nbWarn = metier.getNbWarn();
    dao.nbInfo = metier.getNbInfo();
    return dao;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public RTFAResultLines convertDaoToMetier(final CrueDaoRTFA dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
    final RTFAResultLines metier = new RTFAResultLines();
    metier.setHeader(convertDaoToMetierHeader(dao, ctuluLog));
    final List<RTFAResultLine> lines = new ArrayList<>();

    for (final LigneRTFA ligne : dao.Lignes) {
      lines.add(convertDaoToMetierLine(ligne, ctuluLog));
    }

    metier.addLines(lines);

    return metier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public RTFAResultLines getConverterData(final CrueData in) {
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CrueDaoRTFA convertMetierToDao(final RTFAResultLines metier, final CtuluLog ctuluLog) {
    final CrueDaoRTFA dao = new CrueDaoRTFA();
    convertMetierToDaoHeader(dao, metier, ctuluLog);
    dao.Lignes = new ArrayList<>();

    for (final RTFAResultLine line : metier.getLines()) {
      dao.Lignes.add(convertMetierToDaoLine(line, ctuluLog));
    }

    return dao;
  }
}
