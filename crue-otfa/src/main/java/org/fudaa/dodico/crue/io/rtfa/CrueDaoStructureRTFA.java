package org.fudaa.dodico.crue.io.rtfa;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureLog;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureOptions;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureOptions.LigneCampagne;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;

/**
 * @author CANEL Christophe
 */
public class CrueDaoStructureRTFA implements CrueDataDaoStructure {

  /**
   * {@inheritDoc}
   */
  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    xstream.alias("OTFA-Resultats", CrueDaoRTFA.class);
    xstream.alias("OTFA-Resultat-Line", LigneRTFA.class);
    xstream.aliasField("OTFA-Resultats", CrueDaoRTFA.class, "Lignes");
    new CrueDaoStructureOptions().configureXStream(xstream, ctuluLog, props);
    new CrueDaoStructureLog().configureXStream(xstream, ctuluLog, props);
  }

  protected static class LigneRTFA {

    public LigneCampagne LigneCampagne;
    public int nbDifferences;
    public int nbSevereError;
    public int nbError;
    public int nbWarn;
    public int nbInfo;
  }
}
