package org.fudaa.dodico.crue.io.line;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.comparaison.ExecuteComparaisonResult;
import org.fudaa.dodico.crue.comparaison.tester.ResultatTest;
import org.fudaa.dodico.crue.io.common.CrueConverterCommonLog;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.line.CrueDaoStructureLineResult.Comparaison;
import org.fudaa.dodico.crue.io.line.CrueDaoStructureLineResult.LineResult;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLineResultComparaisons;

/**
 * @author CANEL Christophe
 */
public class CrueConverterLineResult implements CrueDataConverter<CrueDaoLineResult, OtfaCampagneLineResultComparaisons> {

  private static ResultatTest convertDaoToMetier(final Comparaison dao) {
    final ResultatTest metier = new ResultatTest();
    metier.setPropertyTested(dao.PropertyTested);
    if (dao.ComparisonStopped) {
      metier.setComparaisonStopped();
    }
    final List<String> arguments = dao.Arguments;
    if (arguments != null) {
      final Object[] strings = arguments.toArray(new String[0]);
      metier.setMsg(dao.Message, strings);
    } else {
      metier.setMsg(dao.Message);
    }
    metier.setPrintA(dao.A);
    metier.setObjetA(dao.A);
    metier.setPrintB(dao.B);
    metier.setObjetB(dao.B);
    metier.setSame(dao.Same);
    for (final CrueDaoStructureLineResult.Comparaison result : dao.SubComparaisons) {
      final ResultatTest filsResult = convertDaoToMetier(result);
      if (filsResult.isSame()) {
        metier.addFils(filsResult);
      } else {
        metier.addDiff(filsResult);
      }
    }
    return metier;
  }

  private static Comparaison convertMetierToDao(final ResultatTest metier) {
    final CrueDaoStructureLineResult.Comparaison dao = new CrueDaoStructureLineResult.Comparaison();
    if (metier == null) {
      dao.Same = true;
      return dao;
    }
    dao.Message = metier.getMsg();
    dao.ComparisonStopped = metier.isComparaisonStopped();
    dao.Arguments = new ArrayList<>();
    final Object[] msgArguments = metier.getMsgArguments();
    if (msgArguments != null) {
      for (final Object object : msgArguments) {
        dao.Arguments.add(ObjectUtils.toString(object));
      }
    }
    dao.PropertyTested = metier.getPropertyTested();
    dao.Same = metier.isSame();
    dao.NbDifferences = metier.getNbDifferences();
    final Object printA = metier.getPrintA();
    dao.A = (printA == null) ? metier.getObjetA().toString() : printA.toString();
    final Object printB = metier.getPrintB();
    dao.B = (printB == null) ? metier.getObjetB().toString() : printB.toString();
    for (final ResultatTest resultat : metier.getFils()) {
      dao.SubComparaisons.add(convertMetierToDao(resultat));
    }
    return dao;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public OtfaCampagneLineResultComparaisons convertDaoToMetier(final CrueDaoLineResult dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
    final OtfaCampagneLineResultComparaisons metier = new OtfaCampagneLineResultComparaisons();
    if (dao.ComparaisonResultats != null) {
      final List<ExecuteComparaisonResult> resultats = new ArrayList<>();
      for (final LineResult executeComparaisonResult : dao.ComparaisonResultats) {
        resultats.add(convertDaoToMetier(executeComparaisonResult));
      }
      metier.setComparisonResult(resultats);
    }
    return metier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public OtfaCampagneLineResultComparaisons getConverterData(final CrueData in) {
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CrueDaoLineResult convertMetierToDao(final OtfaCampagneLineResultComparaisons metier, final CtuluLog ctuluLog) {
    final CrueDaoLineResult dao = new CrueDaoLineResult();
    final List<ExecuteComparaisonResult> comparisonResult = metier.getComparisonResult();
    for (final ExecuteComparaisonResult executeComparaisonResult : comparisonResult) {
      dao.ComparaisonResultats.add(convertMetierToDao(executeComparaisonResult));

    }
    return dao;
  }

  private CrueDaoStructureLineResult.LineResult convertMetierToDao(final ExecuteComparaisonResult metier) {
    final CrueDaoStructureLineResult.LineResult dao = new CrueDaoStructureLineResult.LineResult();
    dao.Id = metier.getId();
    dao.Message = metier.getMsg();
    dao.NbDifferences = metier.getNbDifferences();
    dao.Comparaisons = convertMetierToDao(metier.getRes());
    dao.ResultatLog = CrueConverterCommonLog.convertMetierToDao(metier.getLog());
    dao.DiffOnOrder = metier.isDiffOnOrder();
    return dao;
  }

  private ExecuteComparaisonResult convertDaoToMetier(final CrueDaoStructureLineResult.LineResult dao) {
    final ExecuteComparaisonResult metier = new ExecuteComparaisonResult(null);
    metier.setId(dao.Id);
    metier.setMsg(dao.Message);
    metier.setDiffOnOrder(dao.DiffOnOrder);
    metier.setRes(convertDaoToMetier(dao.Comparaisons));
    metier.setLog(CrueConverterCommonLog.convertDaoToMetier(dao.ResultatLog));
    return metier;
  }
}
