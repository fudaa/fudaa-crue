package org.fudaa.dodico.crue.projet.otfa;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.*;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.comparaison.ComparaisonSelectorEnum;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.CoeurManager;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.config.cr.CRReader;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory.VersionResult;
import org.fudaa.dodico.crue.io.common.FileLocker;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLines;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLinesSaver;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.CompteRendu;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.projet.ScenarioLoader;
import org.fudaa.dodico.crue.projet.ScenarioLoaderOperation;
import org.fudaa.dodico.crue.projet.calcul.*;
import org.fudaa.dodico.crue.projet.create.InfosCreation;
import org.fudaa.dodico.crue.projet.create.RunCalculOption;
import org.fudaa.dodico.crue.projet.create.RunCreatorOptions;
import org.joda.time.LocalDateTime;

import java.io.File;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Execute actions.
 */
public class OtfaExecutor {
    private final CoeurManager coeurManager;
    private final ConnexionInformation user;
    private final CalculCrueRunnerManager calculRunner;
    private final RTFAResultLinesSaver lineSaver;
    private final OtfaComparaisonExecutor executor;
    private CalculCrueContrat currentRunner;
    private boolean batch;
    private volatile boolean stoppedByUser;

    /**
     * @param lineSaver    le process de sauvegarde des ligns
     * @param coeurManager le manager des coeurs
     * @param user         le user courant
     * @param calculRunner le lanceur de run
     */
    public OtfaExecutor(RTFAResultLinesSaver lineSaver, CoeurManager coeurManager, ConnexionInformation user, final CalculCrueRunnerManager calculRunner) {
        super();
        this.coeurManager = coeurManager;
        executor = new OtfaComparaisonExecutor(coeurManager.getSiteDir());
        this.user = user;
        this.calculRunner = calculRunner;
        this.lineSaver = lineSaver;
    }

    public RTFAResultLines launch(OtfaCampagne in, ProgressionInterface progress) {
        RTFAResultLines result = null;
        OutOfMemoryError oomError = null;
        FileLocker locker = new FileLocker();
        Set<File> allEtu = new HashSet<>();
        if (progress != null) {
            progress.reset();
        }
        ProgressionUpdater updater = new ProgressionUpdater(progress, false);
        try {
            if (!lineSaver.isStarted()) {
                lineSaver.start(false);
            }
            result = lineSaver.getLines();

            result.getHeader().setCommentaire(in.getCommentaire());
            result.getHeader().setDate(new LocalDateTime());
            updater.majProgessionStateOnly(BusinessMessages.getString("otfa.avancement.validation"));
            OtfaContentValidator validator = new OtfaContentValidator(coeurManager);
            CtuluLogGroup campagneLogs = validator.valid(in, true);
            result.setGlobalValidation(campagneLogs);
            CtuluLogGroup preloadSelector = executor.preloadSelector();
            if (preloadSelector != null && preloadSelector.containsSomething()) {
                campagneLogs.addGroup(preloadSelector);
            }

            if (campagneLogs.containsFatalError()) {
                return result;
            }
            if (stoppedAskedByUser()) {
                return result;
            }

            List<OtfaCampagneLine> lines = in.getLines();

            updater.majProgessionStateOnly(BusinessMessages.getString("otfa.avancement.lock"));
            for (OtfaCampagneLine line : lines) {

                allEtu.add(line.getReference().getEtuFile(in.getOtfaDir()));
                if (!line.isCibleAConversion() && !line.getCible().isEmpty()) {
                    allEtu.add(line.getFinalCible().getEtuFile(in.getOtfaDir()));
                }
                if (stoppedAskedByUser()) {
                    return result;
                }
            }
            try {
                for (File file : allEtu) {
                    locker.lock(file, user);
                }
                processAllLines(in, campagneLogs, updater);
            } catch (Exception ex) {
                Logger.getLogger(OtfaExecutor.class.getName()).log(Level.INFO, "message", ex);
            } finally {
                updater.majProgessionStateOnly(BusinessMessages.getString("otfa.avancement.unlock"));
                for (File file : allEtu) {
                    locker.unlock(file);
                }
            }
            if (stoppedAskedByUser()) {
                return result;
            }
            CtuluLogGroup saveRtfaCtfalogs = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
            saveRtfaCtfalogs.setDescription("otfa.executor.saveResultFiles");
            campagneLogs.addGroup(saveRtfaCtfalogs);
            if (progress != null) {
                progress.setProgression(100);
            }
        } catch (OutOfMemoryError error) {
            Logger.getLogger(OtfaExecutor.class.getName()).log(Level.WARNING, "message");
            oomError = error;
        } catch (Exception ex) {
            Logger.getLogger(OtfaExecutor.class.getName()).log(Level.INFO, "message", ex);
        } finally {
            for (File file : allEtu) {
                locker.unlock(file);
            }
            lineSaver.close();
        }
        if (oomError
                != null) {
            throw oomError;
        }
        return result;
    }

    private void processAllLines(OtfaCampagne in, CtuluLogGroup campagneLogs, ProgressionUpdater updater) {
        if (in.getReferenceOptions().isEffacerRunAvant() || in.getCibleOptions().isEffacerRunAvant()) {
            CtuluLogGroup logsBefore = campagneLogs.createGroup("otfa.deleteRunBefore.logs");
            updater.majProgessionStateOnly(BusinessMessages.getString("otfa.avancement.deleteRunAvant"));
            deleteRuns(in, in.getReferenceOptions().isEffacerRunAvant(), in.getCibleOptions().isEffacerRunAvant(), logsBefore);
        }
        int size = in.getLines().size();
        updater.setValue(Math.max(10, size), size);
        for (OtfaCampagneLine line : in.getLines()) {
            updater.majProgessionStateOnly(BusinessMessages.getString("otfa.avancement.line", Integer.toString(line.getIndice()), size));
            lineSaver.addResult(this.launch(in, line));
            final boolean isStopped = stoppedAskedByUser();
            if (isStopped) {
                return;
            }
            updater.majAvancement();
        }
        if (in.getReferenceOptions().isEffacerRunApres() || in.getCibleOptions().isEffacerRunApres()) {
            updater.majProgessionStateOnly(BusinessMessages.getString("otfa.avancement.deleteRunApres"));
            CtuluLogGroup logsAfter = campagneLogs.createGroup("otfa.deleteRunAfter.logs");
            deleteRuns(in, in.getReferenceOptions().isEffacerRunApres(), in.getCibleOptions().isEffacerRunApres(), logsAfter);
        }
    }

    private OtfaCampagneLineResult launch(OtfaCampagne campagne, OtfaCampagneLine line) {
        final OtfaCampagneLineResult result = new OtfaCampagneLineResult();
        final CtuluLogGroup logs = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
        logs.setDescription("otfa.launchLine");
        logs.setDescriptionArgs(Integer.toString(line.getIndice()));

        result.setInitialLine(line);
        result.setLogs(logs);

        final CrueOperationResult<EMHScenario> referenceResult = this.launchItem(campagne, line.getReference(), null);
        final CtuluLogGroup refLogs = referenceResult.getLogs();
        refLogs.setDescription("otfa.launchRefItem");
        logs.addGroup(refLogs);
        final CrueOperationResult<EMHScenario> cibleResult = this.launchItem(campagne, line.getFinalCible(),
                line.isCibleAConversion() ? line.getReference() : null);
        final CtuluLogGroup logsCible = cibleResult.getLogs();
        logsCible.setDescription("otfa.launchCibleItem");
        logs.addGroup(logsCible);
        if (stoppedAskedByUser()) {
            return result;
        }
        if (line.isLancerComparaison()) {
            final CoeurConfigContrat coeurForItem = getCoeurForItem(campagne, line.getReference());

            //la config est celle de la référence.
            CrueConfigMetier config = coeurForItem.getCrueConfigMetier();
            final EMHScenario refScenario = referenceResult.getResult();
            final EMHScenario cibleScenario = cibleResult.getResult();

            if ((refScenario != null) && (cibleScenario != null)) {
                result.setComparisonResult(executor.compare(config, refScenario, cibleScenario, getSelectorType(line)));
            }
        }

        return result;
    }

    private ComparaisonSelectorEnum getSelectorType(OtfaCampagneLine line) {
        if (line.getReference().isCrue9() && line.getFinalCible().isCrue9()) {
            return ComparaisonSelectorEnum.C9_C9;
        }
        if (line.getReference().isNotCrue9() && line.getFinalCible().isNotCrue9()) {
            return ComparaisonSelectorEnum.C10_C10;
        }
        return ComparaisonSelectorEnum.C9_C10;
    }

    private CoeurConfigContrat getCoeurForItem(OtfaCampagne campagne, OtfaCampagneItem item) {
        File etuFile = item.getEtuFile(campagne.getOtfaDir());
        final CoeurConfigContrat coeur;
        if (item.isCrue9()) {
            VersionResult findVersion = Crue10FileFormatFactory.findVersion(etuFile);
            coeur = coeurManager.getCoeurConfigDefault(findVersion.getVersion());
        } else {
            coeur = this.coeurManager.getCoeurConfig(item.getCoeurName());
        }
        return coeur;
    }

    public void setBatch(boolean batch) {
        this.batch = batch;
    }

    /**
     * @param campagne la campagne
     * @param item     l'item a lancer
     * @param ref      la référence est utilisée dans le cas cible si une conversion est demandée.
     * @return resultat de l'opération
     */
    private CrueOperationResult<EMHScenario> launchItem(OtfaCampagne campagne, OtfaCampagneItem item, OtfaCampagneItem ref) {
        final CtuluLogGroup logs = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
        if (item.isEmpty()) {
            return new CrueOperationResult<>(null, logs.createCleanGroup());
        }

        // Chargement du projet.
        final File etuFile = item.getEtuFile(campagne.getOtfaDir());
        final CoeurConfigContrat coeurForItem = getCoeurForItem(campagne, item);
        CrueOperationResult<EMHProjet> projetResult = this.loadProjet(etuFile, coeurForItem);
        logs.addGroup(projetResult.getLogs().createCleanGroup());
        if (projetResult.getResult() == null || stoppedAskedByUser()) {
            return new CrueOperationResult<>(null, logs.createCleanGroup());
        }

        EMHProjet projet = projetResult.getResult();

        EMHProjetController controller = new EMHProjetController(projet, user);

        ManagerEMHScenario managerScenario = projet.getScenario(item.getScenarioNom());
        // Conversion
        if (ref != null) {
            final InfosCreation infos = new InfosCreation(user);
            infos.setCrueVersion(item.isCrue9() ? CrueVersionType.CRUE9 : CrueVersionType.CRUE10);
            // Conversion du scénario
            final CrueOperationResult<EMHScenarioContainer> conversionResult = controller.convertScenario(ref.getScenarioNom(), infos,
                    ref.getEtuFile(
                            campagne.getOtfaDir()));

            logs.addGroup(conversionResult.getLogs().createCleanGroup());
            EMHProjetController.writeProjet(etuFile, projet, logs, user);
            projetResult = this.loadProjet(etuFile, getCoeurForItem(campagne, item));
            final CtuluLogGroup cleanLogGroup = projetResult.getLogs().createCleanGroup();

            logs.addGroup(cleanLogGroup);
            if (projetResult.getResult() == null || cleanLogGroup.containsFatalError()) {
                return new CrueOperationResult<>(null, (CtuluLog) null);
            }
            //on reprend les données:
            projet = projetResult.getResult();
            managerScenario = projet.getScenario(item.getScenarioNom());
            controller = new EMHProjetController(projet, user);//important de faire cela pour la reference a projet.
        }
        if (managerScenario == null) {
            logs.createNewLog("otfa.scenarioNotFound").addError("otfa.scenarionNotFound.detail", item.getScenarioNom());
            return new CrueOperationResult<>(null, logs.createCleanGroup());
        }

        if (item.isLaunchCompute()) {
            // Création du run et sauvegarde dans l'étude
            //pour créer le runs on utilise des options sans run afin de forcer le lancement complet des calculs.
            //fait suite à la demande:CRUE-349
            final RunCreatorOptions runOptions = new RunCreatorOptions(null);
            runOptions.setComputeAll(RunCalculOption.FORCE_EXCUTE);
            CrueOperationResult<EMHRun> runResult = controller.createRun(managerScenario.getNom(), runOptions,
                    item.getEtuFile(campagne.getOtfaDir()));
            logs.addGroup(runResult.getLogs().createCleanGroup());
            if (runResult.getResult() != null) {
                if (managerScenario.isCrue9()) {
                    currentRunner = calculRunner.getCrue9Runner().createRunner(new ExecInputDefault(projet, managerScenario,
                            runResult.getResult()), null);
                    currentRunner.run();
                } else {
                    final ExecInputDefault execInput = new ExecInputDefault(projet, managerScenario,
                            runResult.getResult());
                    Crue10OptionBuilder optionsBuilder = new Crue10OptionBuilder(calculRunner.getExecOptions());
                    ExecConfigurer defaultConfigurer = optionsBuilder.createConfigurer(execInput, RunCreatorOptions.createMap(RunCalculOption.FORCE_EXCUTE));
                    currentRunner = calculRunner.getCrue10Runner(projet.getCoeurConfig()).createRunner(execInput, defaultConfigurer);
                    currentRunner.run();
                }
            }
        }
        ScenarioLoader loader = new ScenarioLoader(managerScenario, projet, projet.getCoeurConfig());

        ScenarioLoaderOperation load = loader.load(managerScenario.getRunCourant());//on charge le run courant:

        logs.addGroup(load.getLogs().createCleanGroup());
        EMHScenario emhScenario = null;
        if (!load.getLogs().containsFatalError()) {
            emhScenario = load.getResult();
            //on cherche les erreurs dans les CR.
            List<EMHModeleBase> modeles = emhScenario.getModeles();
            CtuluLogGroup logCheckCr = logs.createGroup("otfa.checkComputeCr");
            for (EMHModeleBase modele : modeles) {
                CtuluLog logForModel = logCheckCr.createNewLog(modele.getNom());
                for (CrueFileType fileType : CrueFileType.values()) {
                    if (fileType.isResultFileType()) {
                        CompteRendu compteRendu = modele.getCompteRendu(fileType);
                        if (compteRendu != null) {
                            File logFile = compteRendu.getLogFile();
                            if (logFile.isFile()) {
                                CRReader reader = new CRReader(0);//pas d'importance
                                CrueIOResu<CtuluLogLevel> higherLevel = reader.getHigherLevel(logFile);
                                if (higherLevel != null && (CtuluLogLevel.SEVERE.equals(higherLevel.getMetier()) || CtuluLogLevel.FATAL.
                                        equals(higherLevel.getMetier()))) {
                                    logForModel.addRecord(higherLevel.getMetier(), "otfa.errorLevelFoundInCr", logFile.getName());
                                }
                            }
                        }
                    }
                }
            }
            if (StringUtils.isNotEmpty(item.getRapport())) {
                final CtuluLogGroup reportGroup = logs.createGroup("otfa.reportCreation");
                reportGroup.setDescriptionArgs(item.getRapport());

                OtfaReportExecutor report = new OtfaReportExecutor(projet, managerScenario, emhScenario, item, campagne);
                report.exportCSV(reportGroup);
            }
        }

        return new CrueOperationResult<>(emhScenario, logs.createCleanGroup());
    }

    private CrueOperationResult<EMHProjet> loadProjet(File etuFile, CoeurConfigContrat version) {
        return EMHProjetController.readProjet(etuFile, version,true);
    }

    private void deleteRuns(OtfaCampagne in, boolean reference, boolean cible, CtuluLogGroup logs) {
        //rien a faire:
        if (!reference && !cible) {
            return;
        }
        for (OtfaCampagneLine otfaCampagneLine : in.getLines()) {
            if (Thread.currentThread().isInterrupted()) {
                return;
            }
            if (reference) {
                final OtfaCampagneItem item = otfaCampagneLine.getReference();
                File referenceFile = item.getEtuFile(in.getOtfaDir());
                deleteRuns(referenceFile, otfaCampagneLine.getReference().getScenarioNom(), in, item, logs);
            }
            if (cible && !otfaCampagneLine.getCible().isEmpty()) {
                final OtfaCampagneItem item = otfaCampagneLine.getFinalCible();
                File cibleEtu = item.getEtuFile(in.getOtfaDir());
                deleteRuns(cibleEtu, otfaCampagneLine.getFinalCible().getScenarioNom(), in, item, logs);
            }
        }
    }

    private void deleteRuns(File referenceFile, String scenarioNom, OtfaCampagne campagne,
                            OtfaCampagneItem item, CtuluLogGroup logs) {
        CrueOperationResult<EMHProjet> projetResult = this.loadProjet(referenceFile, getCoeurForItem(campagne, item));
        final EMHProjet projet = projetResult.getResult();
        CtuluLog mainLog = logs.createNewLog("otfa.deleteRuns.logs");
        mainLog.setDescriptionArgs(scenarioNom);
        if (projet != null) {
            EMHProjetController controller = new EMHProjetController(projet, user);
            ManagerEMHScenario scenario = projet.getScenario(scenarioNom);
            if (scenario != null && CollectionUtils.isNotEmpty(scenario.getListeRuns())) {
                Map<RunScenarioPair, FileDeleteResult> map = new LinkedHashMap<>();
                controller.deleteAllRuns(scenario, map);
                if (!map.isEmpty()) {
                    for (Map.Entry<RunScenarioPair, FileDeleteResult> entry : map.entrySet()) {
                        RunScenarioPair runScenarioPair = entry.getKey();
                        FileDeleteResult fileDeleteResult = entry.getValue();
                        mainLog.addWarn("deleteRunFolder.Failed", runScenarioPair.getScenario().getNom(), runScenarioPair.getRun().getNom());
                        if (!fileDeleteResult.getFilesNotDeleted().isEmpty()) {
                            for (File fileNotDelete : fileDeleteResult.getFilesNotDeleted()) {
                                mainLog.addWarn("deleteRunFolder.FailedForFile", fileNotDelete.getAbsolutePath());
                            }
                        }
                        if (!fileDeleteResult.getDirNotDeleted().isEmpty()) {
                            for (File dirNotDeleted : fileDeleteResult.getDirNotDeleted()) {
                                mainLog.addWarn("deleteRunFolder.FailedForDir", dirNotDeleted.getAbsolutePath());
                            }
                        }
                    }
                } else {
                    scenario.setListeRuns(null);
                    scenario.setRunCourant(null);
                }
                controller.saveProjet(referenceFile, logs);
            }
        }
    }

    /**
     */
    public synchronized void stop() {
        stoppedByUser = true;
        if (currentRunner != null) {
            currentRunner.stop();
        }
    }

    private boolean stoppedAskedByUser() {
        return Thread.currentThread().isInterrupted() || stoppedByUser;
    }
}
