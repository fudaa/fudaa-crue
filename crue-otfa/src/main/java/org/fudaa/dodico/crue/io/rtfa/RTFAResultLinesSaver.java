/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.rtfa;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.batch.BatchLogSaver;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.comparaison.ExecuteComparaison;
import org.fudaa.dodico.crue.io.otfa.OtfaFileUtils;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLineResult;
import org.joda.time.LocalDateTime;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Frederic Deniger
 */
public class RTFAResultLinesSaver implements BatchLogSaver {
  private final File rtfaFile;
  private final File rtfaZipFile;
  ExecutorService executorService;
  boolean started;
  File tempDir;
  private RTFAResultLines lines;
  private CtuluLog mainLog;

  public RTFAResultLinesSaver(final File otfa) {
    rtfaFile = OtfaFileUtils.getRtfaFile(otfa);
    rtfaZipFile = OtfaFileUtils.getRtfaZipFile(otfa);
  }

  public static boolean deleteZipRtfa(final File ctfaFile, final CtuluLog log) {
    if (ctfaFile.exists()) {
      final boolean deleted = ctfaFile.delete();
      if (!deleted) {
        log.addSevereError("otfaBatch.rtfaZipCantBeDeleted", ctfaFile.getAbsolutePath());
        return false;
      }
    }
    return true;
  }

  public static boolean deleteRtfa(final File rtfaFile, final CtuluLog log) {
    if (rtfaFile.exists()) {
      final boolean deleted = rtfaFile.delete();
      if (!deleted) {
        log.addSevereError("otfaBatch.rtfaCantBeDeleted", rtfaFile.getAbsolutePath());
        return false;
      }
    }
    return true;
  }

  public String getRtfaFilPath() {
    return rtfaFile.getAbsolutePath();
  }

  public boolean isStarted() {
    return started;
  }

  public RTFAResultLines getLines() {
    return lines;
  }

  @Override
  public CtuluLog start(final boolean batch) {
    assert !started;
    started = true;
    lines = new RTFAResultLines();
    lines.getHeader().setDate(new LocalDateTime());
    mainLog = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    if (batch) {
      mainLog.setDesc("otfaBatch.launch");
      lines.setBatch(true);
    }

    deleteRtfa(rtfaFile, mainLog);
    deleteZipRtfa(rtfaZipFile, mainLog);
    try {
      tempDir = CtuluLibFile.createTempDir(rtfaFile.getName() + "_temp", rtfaFile.getParentFile());
    } catch (final IOException ex) {
      mainLog.addSevereError(ex.getMessage());
    }
    lines.getHeader().getGlobalValidation().addLog(mainLog);
    executorService = Executors.newFixedThreadPool(2);
    return mainLog;
  }

  public CtuluLog getMainLog() {
    return mainLog;
  }

  public void addGlobalMainLog(final CtuluLog log) {
    lines.getHeader().getGlobalValidation().addLog(log);
  }

  @Override
  public void close() {
    lines.getHeader().cleanGlobalValidation();
    final CrueRTFAReaderWriter writer = new CrueRTFAReaderWriter(CrueRTFAReaderWriter.LAST_VERSION);
    final CrueIOResu<RTFAResultLines> resu = new CrueIOResu<>(lines);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    writer.writeXMLMetier(resu, rtfaFile, log, null);
    final File[] listFiles = tempDir.listFiles();
    CtuluLibFile.zip(listFiles, rtfaZipFile);
    CtuluLibFile.deleteDir(tempDir);
  }

  public void addResult() {
    assert started;
  }

  public void addGlobalMainLog(final CtuluLogGroup init) {
    lines.getHeader().getGlobalValidation().addGroup(init);
  }

  protected void createFiles(final OtfaCampagneLineResult launch) {
    final CTFAWriteResultCallable ctfaCallable = new CTFAWriteResultCallable(launch, tempDir);
    final RTFAWriteResultCallable rtfaCallable = new RTFAWriteResultCallable(launch, tempDir);
    try {
      final List<Future<CtuluLog>> invokeAll = executorService.invokeAll(Arrays.asList(ctfaCallable, rtfaCallable));
      for (final Future<CtuluLog> future : invokeAll) {
        final CtuluLog log = future.get();
        if (log != null && log.isNotEmpty()) {
          addGlobalMainLog(log);
        }
      }
    } catch (final Exception ex) {
      Logger.getLogger(RTFAResultLinesSaver.class.getName()).log(Level.INFO, null, ex);
    }
  }

  public void addResult(final OtfaCampagneLineResult launch) {
    final RTFAResultLine line = new RTFAResultLine();
    line.setInitialLine(launch.getInitialLine());
    final CtuluLogGroup logs = launch.getLogs();
    line.setNbSevereError(logs.getNbOccurence(CtuluLogLevel.SEVERE));
    line.setNbError(logs.getNbOccurence(CtuluLogLevel.ERROR));
    line.setNbWarn(logs.getNbOccurence(CtuluLogLevel.WARNING));
    line.setNbInfo(logs.getNbOccurence(CtuluLogLevel.INFO));
    line.setNbDifferences(ExecuteComparaison.getNbDifferences(launch.getComparisonResults()));
    lines.addLine(line);
    createFiles(launch);
  }
}
