/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.otfa;

import java.io.File;
import java.io.IOException;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.result.ModeleResultatTimeKeyContent;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.ReportIndexReaderHelper;
import org.fudaa.dodico.crue.projet.report.ReportViewLineInfoAndType;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.dodico.crue.projet.report.persist.ReportCourbeConfigReader;
import org.fudaa.dodico.crue.projet.report.transformer.KeysToStringConverter;
import org.fudaa.ebli.converter.TraceToStringConverter;

/**
 *
 * @author Frederic Deniger
 */
public class OtfaReportExecutor {

  private final EMHProjet projet;
  private final ManagerEMHScenario managerScenario;
  private final EMHScenario emhScenario;
  private final OtfaCampagneItem item;
  private final OtfaCampagne campagne;

  OtfaReportExecutor(EMHProjet projet, ManagerEMHScenario managerScenario, EMHScenario emhScenario, OtfaCampagneItem item,
          OtfaCampagne campagne) {
    this.projet = projet;
    this.emhScenario = emhScenario;
    this.managerScenario = managerScenario;
    this.item = item;
    this.campagne = campagne;
  }

  public EMHProjet getProjet() {
    return projet;
  }

  public ManagerEMHScenario getManagerScenario() {
    return managerScenario;
  }

  public EMHScenario getEmhScenario() {
    return emhScenario;
  }

  private OtfaReportGlobalService createService(CtuluLogGroup logs) {
    CtuluLog log = logs.createNewLog("report.result.queries");
    final EMHRun run = managerScenario.getRunCourant();

    OtfaReportResultService resultService = new OtfaReportResultService(projet.getCoeurConfig().getCrueConfigMetier(), log, run, managerScenario,
            emhScenario);
    OtfaReportGlobalService globalService = new OtfaReportGlobalService(resultService);
    OtfaReportFormuleServiceLoader formuleLoader = new OtfaReportFormuleServiceLoader(projet);
    final CrueIOResu<OtfaReportFormuleService> loadFormuleService = formuleLoader.loadFormuleService(globalService);
    if (loadFormuleService.getAnalyse().isNotEmpty()) {
      logs.addLog(loadFormuleService.getAnalyse());
    }
    resultService.setFormuleService(loadFormuleService.getMetier());
    return globalService;
  }

  void exportCSV(CtuluLogGroup logs) {
    final EMHRun run = managerScenario.getRunCourant();
    CtuluLog log = logs.createNewLog("otfa.report.loading");
    if (run == null) {
      log.addError("otfa.report.notGenerated.NoCurrentRun");
      return;
    }
    ReportIndexReaderHelper helper = new ReportIndexReaderHelper(projet);
    final ReportContentType contentType = ReportViewLineInfoAndType.getContentType(item.getRapport());
    if (contentType == null) {
      log.addSevereError("otfa.report.typeUnknown", item.getRapport());
      return;
    }
    final ReportViewLineInfoAndType reportInfoAndType = helper.containsReportWithIdOrDescription(contentType, item.getRapport());
    if (reportInfoAndType == null) {
      log.addSevereError("otfa.report.notFound", item.getRapport());
      return;
    }
    File dir = new File(projet.getInfos().getDirOfRapports(), reportInfoAndType.getType().getFolderName());
    File xml = new File(dir, reportInfoAndType.getInfo().getFilename());
    if (!xml.exists()) {
      log.addSevereError("otfa.report.fileNotFound", xml.getAbsolutePath());
      return;
    }
    final OtfaReportGlobalService reportGlobalService = createService(logs);
    final EMHModeleBase defaultModele = emhScenario.getModeles().get(0);
    final ModeleResultatTimeKeyContent timeContent = ModeleResultatTimeKeyContent.extract(defaultModele);
    reportGlobalService.setPermanentTimeKeys(timeContent.getPermanentsTimes());
    reportGlobalService.setTransitoireTimeKeys(timeContent.getTransitoiresTimes());
    if (item.isRapportOnTransitoire()) {
      reportGlobalService.setRangeSelectedTimeKeys(timeContent.getTransitoiresTimes());
    } else {
      reportGlobalService.setRangeSelectedTimeKeys(timeContent.getPermanentsTimes());
    }

    KeysToStringConverter keysToString = new KeysToStringConverter(reportGlobalService.getResultService().getRunCourant().getRunKey());
    ReportCourbeConfigReader reader = new ReportCourbeConfigReader(new TraceToStringConverter(), keysToString);
    final CrueIOResu<ReportConfigContrat> read = reader.read(xml);
    if (read.getAnalyse().isNotEmpty()) {
      logs.addLog(read.getAnalyse());
    }
    final ReportConfigContrat report = read.getMetier();
    if (report == null) {
      return;
    }

    String targetCsvFile = item.getCheminCSV();
    File targetFile = null;
    File modeleDir = projet.getDirForRunModele(managerScenario, managerScenario.getManagerFils(defaultModele.getId()), run);
    if (CtuluLibString.isEmpty(targetCsvFile)) {
      targetCsvFile = report.getType().name().toLowerCase() + "_" + CtuluLibFile.getSansExtension(xml.getName()) + ".csv";
      targetFile = new File(modeleDir, targetCsvFile).getAbsoluteFile();
    } else {
      targetFile = new File(targetCsvFile);
      if (!targetFile.isAbsolute()) {
        try {
          targetFile = new File(modeleDir, targetCsvFile).getCanonicalFile();
        } catch (IOException ex) {
          log.addSevereError(ex.getMessage());
        }
      }
    }
    if (log.containsSevereError()) {
      return;
    }
    targetFile = CtuluLibFile.appendExtensionIfNeeded(targetFile, "csv");

    if (targetFile.getParentFile() == null || !targetFile.getParentFile().exists()) {
      final boolean mkdirs = targetFile.getParentFile().mkdirs();
      if (!mkdirs) {
        log.addSevereError("otfa.report.cantCreateDir", targetFile.getParentFile().toString());
      }
    }
    final String error = CtuluLibFile.canWrite(targetFile);
    if (error != null) {
      log.addSevereError(error);
      return;
    }
    OtfaReportExecutorBuilder builder = new OtfaReportExecutorBuilder(this);
    final OtfaReportExecutorDelegate delegate = builder.createDelegate(report, reportInfoAndType, reportGlobalService);

    if (delegate == null) {
      log.addSevereError("otfa.report.unknown", report.getType().geti18n());
    } else {
      delegate.exportTo(targetFile, log);
    }

  }

}
