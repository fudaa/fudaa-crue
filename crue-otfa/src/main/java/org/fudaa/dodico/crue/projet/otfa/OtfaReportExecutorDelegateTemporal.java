/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.otfa;

import com.Ostermiller.util.CSVPrinter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportViewLineInfoAndType;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableEmhKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableHelper;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.dodico.crue.projet.report.persist.ReportTemporalConfig;

/**
 *
 * @author Frederic Deniger
 */
public class OtfaReportExecutorDelegateTemporal extends OtfaReportExecutorDelegate {

  public OtfaReportExecutorDelegateTemporal(OtfaReportExecutor parent, ReportViewLineInfoAndType reportInfo, ReportConfigContrat config,
          OtfaReportGlobalService reportGlobalService) {
    super(parent, reportInfo, config, reportGlobalService);
  }

  @Override
  protected void exportTo(CSVPrinter printer, CtuluLog log) throws IOException {
    ReportTemporalConfig config = (ReportTemporalConfig) super.config;
    StringBuilder builder = new StringBuilder();
    builder.append(quote).append(BusinessMessages.getString("Export.Calcul.ColumnName")).append(quote).append(super.delimiter);
    builder.append(quote).append(BusinessMessages.getString("Export.Temps.ColumnName")).append(quote);
    final List<ReportRunVariableEmhKey> variablesToExport = getVariablesToExport(config, log);
    for (ReportRunVariableEmhKey reportRunVariableEmhKey : variablesToExport) {
      builder.append(delimiter).append(quote).append(reportRunVariableEmhKey.getRunVariableKey().getVariable().getVariableDisplayName()).append(
              BusinessMessages.ENTITY_SEPARATOR).
              append(reportRunVariableEmhKey.
                      getEmhName()).append(quote);
    }
    printer.writelnComment(builder.toString());

    final List<ResultatTimeKey> times = reportGlobalService.getTimeKeysToUse();
    for (ResultatTimeKey resultatTimeKey : times) {
      printer.write(resultatTimeKey.getNomCalcul());
      printer.write(resultatTimeKey.getTemps());
      for (ReportRunVariableEmhKey reportRunVariableEmhKey : variablesToExport) {
        Double val = reportGlobalService.getResultService().getValue(resultatTimeKey, reportRunVariableEmhKey.getRunVariableKey(),
                reportRunVariableEmhKey.getEmhName(), getSelectedTimeStepInReport());
        printVariable(printer, val, reportRunVariableEmhKey.getVariableName());
      }
      //suite
      printer.writeln();

    }

  }

  private List<ReportRunVariableEmhKey> getVariablesToExport(ReportTemporalConfig config, CtuluLog log) {
    Map<String, List<ReportRunKey>> keyByVar = ReportRunVariableHelper.getByVarName(config.getTemporalVariables());
    boolean containNonCurrent = false;
    Set<String> toExport = new HashSet<>();
    for (Map.Entry<String, List<ReportRunKey>> entry : keyByVar.entrySet()) {
      String var = entry.getKey();
      if (containsCurrent(entry.getValue())) {
        toExport.add(var);
      }
      if (containsAlternatif(entry.getValue())) {
        containNonCurrent = true;
      }
    }
    if (containNonCurrent) {
      log.addWarn(BusinessMessages.getString("Report.RunNonCurrentIgnored"));
    }
    ReportRunKey current = new ReportRunKey();
    List<ReportRunVariableEmhKey> keys = new ArrayList<>();
    for (String variableName : config.getVariables()) {
      if (toExport.contains(variableName)) {
        ReportVariableKey variableKey = super.reportGlobalService.getResultService().createVariableKey(variableName);
        for (String emhName : config.getEmhs()) {
          keys.add(new ReportRunVariableEmhKey(current, variableKey, emhName));

        }
      }
    }
    return keys;
  }

}
