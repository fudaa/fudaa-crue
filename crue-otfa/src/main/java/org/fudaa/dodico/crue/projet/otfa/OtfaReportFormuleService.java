/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.otfa;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.FormuleContentManager;
import org.fudaa.dodico.crue.projet.report.ReportFormuleServiceContrat;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.formule.FormuleContent;
import org.fudaa.dodico.crue.projet.report.formule.FormuleServiceContent;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * @author Frederic Deniger
 */
public class OtfaReportFormuleService implements FormuleContentManager, ReportFormuleServiceContrat {

    private final FormuleServiceContent serviceContent;

    public OtfaReportFormuleService(FormuleServiceContent serviceContent) {
        this.serviceContent = serviceContent;
    }

    @Override
    public List<FormuleContent> getContents() {
        return getServiceContent().getVariables();
    }

    @Override
    public List<String> getVariablesKeys() {
        return serviceContent == null ? Collections.emptyList() : serviceContent.getVariablesKeys();
    }

    public FormuleServiceContent getServiceContent() {
        return serviceContent;
    }

    Double getValue(ResultatTimeKey selectedTime, ReportRunVariableKey key, String emhNom, Collection<ResultatTimeKey> selectedTimesInRapport) {
        if (serviceContent == null) {
            return null;
        }
        FormuleContent formuleContent = serviceContent.getContent(key);
        if (formuleContent == null || formuleContent.getCalculator() == null) {
            return null;
        }
        return formuleContent.getCalculator().getValue(selectedTime, key, emhNom, selectedTimesInRapport);
    }

    public String getPropertyNature(ReportVariableKey key) {
        FormuleContent content = serviceContent.getContent(key);
        if (content == null) {
            return null;
        }
        return content.getPropertyNature();
    }

    PropertyNature getPropertyNature(ReportVariableKey key, CrueConfigMetier ccm) {
        String variableName = getPropertyNature(key);
        if (variableName != null) {
            return ccm.getNature(variableName);
        }
        return null;
    }

    PropertyNature getPropertyNature(String varName, CrueConfigMetier ccm) {
        if (serviceContent == null) {
            return null;
        }
        FormuleContent content = serviceContent.getContent(varName);
        if (content == null || content.getPropertyNature() == null) {
            return null;
        }
        return ccm.getNature(content.getPropertyNature());
    }

    public boolean isFormule(String varName) {
        return this.serviceContent != null && serviceContent.getContent(varName) != null;
    }

    @Override
    public Set<String> getDirectUsedVariable(String var) {
        FormuleContent content = serviceContent.getContent(var);
        if (content == null || content.getCalculator() == null) {
            return Collections.emptySet();
        }
        return content.getCalculator().getDirectUsedVariables();
    }

    @Override
    public boolean containsTimeDependantVariable(String var) {
        FormuleContent content = serviceContent.getContent(var);
        if (content == null || content.getCalculator() == null) {
            return false;
        }
        return content.getCalculator().containsTimeDependantVariable();
    }

    @Override
    public boolean isExpressionValid(String var, Collection<ResultatTimeKey> selectedTimes) {
        FormuleContent content = serviceContent.getContent(var);
        if (content == null || content.getCalculator() == null) {
            //valide car pas de forumule
            return true;
        }
        return content.getCalculator().isExpressionValid(selectedTimes);
    }
}
