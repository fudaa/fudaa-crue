package org.fudaa.dodico.crue.projet.otfa;

import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.commun.ProgressionTestAdapter;
import org.fudaa.dodico.crue.batch.BatchData;
import org.fudaa.dodico.crue.batch.BatchHelper;
import org.fudaa.dodico.crue.batch.BatchLauncher;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformationDefault;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.otfa.CrueOTFAReaderWriter;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLinesSaver;
import org.fudaa.dodico.crue.projet.calcul.CalculCrueRunnerManagerImpl;
import org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager;
import org.fudaa.dodico.crue.projet.conf.OptionsEnum;
import org.fudaa.dodico.crue.projet.conf.UserConfiguration;

import java.io.File;

/**
 * @author deniger
 */
public class OtfaBatch {
  public static void main(String[] args) {
    try {
      CtuluLog runOtfa = runOtfa(args);
      runOtfa.printResume();
    } catch (Exception e) {
      e.printStackTrace();//mode console
    }
    System.exit(0);
  }

  public static CtuluLog runOtfa(String[] args) {
    CtuluLog initLog = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    initLog.setDesc("otfaBatch.launch");
    if (ArrayUtils.isEmpty(args)) {
      initLog.addSevereError("otfaBatch.noArgument");
      return initLog;
    }
    File otfaFile = new File(args[0]).getAbsoluteFile();
    final String otfaFileAbsolutePath = otfaFile.getAbsolutePath();
    if (!otfaFile.exists()) {
      initLog.addSevereError("otfaBatch.fileNotExists", otfaFileAbsolutePath);
      return initLog;
    }
    final String messageFileLoaded = "otfaBatch.fileLoaded";

    RTFAResultLinesSaver saver = new RTFAResultLinesSaver(otfaFile);
    BatchData data = BatchLauncher.prepare(saver, messageFileLoaded, otfaFileAbsolutePath);

    if (data.getLog().containsErrorOrSevereError()) {
      return data.getLog();
    }

    CtuluLog logToUse = data.getLog();
    GlobalOptionsManager optionsManager = new GlobalOptionsManager();
    CtuluLogGroup init = optionsManager.init(data.getConfiguration(), new UserConfiguration());
    if (init.containsFatalError()) {
      logToUse.addSevereError("otfaBatch.siteConfigNotLoadable");
      saver.addGlobalMainLog(init);
      saver.close();
      return logToUse;
    }

    OtfaCampagne campagne = readCampagne(otfaFile, saver);
    if (campagne == null) {
      return logToUse;
    }
    final ConnexionInformationDefault connexionInformation = new ConnexionInformationDefault();
    CalculCrueRunnerManagerImpl runner = new CalculCrueRunnerManagerImpl();
    runner.setCrue9Exec(optionsManager.getOption(OptionsEnum.CRUE9_EXE).getValeur());
    runner.setExecOptions(BatchHelper.getExecOptions(optionsManager));
    OtfaExecutor exec = new OtfaExecutor(saver, data.getCoeurManager(), connexionInformation, runner);
    exec.setBatch(true);
    logToUse.addInfo("otfaBatch.otfaLaunched", otfaFileAbsolutePath);
    exec.launch(campagne, new ProgressionTestAdapter());
    logToUse.addInfo("otfaBatch.otfaTerminated", otfaFileAbsolutePath);
    return logToUse;
  }

  public static OtfaCampagne readCampagne(File otfaFile, RTFAResultLinesSaver ctfaFile) {
    final CrueOTFAReaderWriter otfaReader = new CrueOTFAReaderWriter("1.2", otfaFile);
    final CtuluLog logLoadOtfa = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueIOResu<OtfaCampagne> otfaResult = otfaReader.readXML(otfaFile, logLoadOtfa);
    if (otfaResult.getMetier() == null) {
      if (logLoadOtfa.isEmpty()) {
        ctfaFile.getMainLog().addSevereError("otfaBatch.fileCantBeLoadedButNoLog", otfaFile.getAbsolutePath());
      } else {
        ctfaFile.addGlobalMainLog(logLoadOtfa);
        ctfaFile.getMainLog().addSevereError("otfaBatch.fileCantBeLoaded", otfaFile.getAbsolutePath());
      }
    }
    return otfaResult.getMetier();
  }
}
