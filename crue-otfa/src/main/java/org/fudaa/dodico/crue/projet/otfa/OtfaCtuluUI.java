/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.otfa;

import java.awt.Component;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluUIAbstract;
import org.fudaa.ctulu.ProgressionInterface;

/**
 *
 * @author Frederic Deniger
 */
public class OtfaCtuluUI extends CtuluUIAbstract {

  private final CtuluLog log;

  public OtfaCtuluUI(CtuluLog log) {
    this.log = log;
  }

  public CtuluLog getLog() {
    return log;
  }


  @Override
  public void error(String _titre, String _msg, boolean _tempo) {
    log.addError(_msg);
  }

  @Override
  public void message(String _titre, String _msg, boolean _tempo) {
    log.addInfo(_msg);
  }

  @Override
  public void warn(String _titre, String _msg, boolean _tempo) {
    log.addWarn(_msg);
  }

  @Override
  public Component getParentComponent() {
    throw new UnsupportedOperationException("Not implemented.");
  }

  @Override
  public ProgressionInterface getMainProgression() {
    throw new UnsupportedOperationException("Not implemented."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void clearMainProgression() {
  }

  @Override
  public boolean question(String _titre, String _text) {
    throw new UnsupportedOperationException("Not implemented."); //To change body of generated methods, choose Tools | Templates.
  }

}
