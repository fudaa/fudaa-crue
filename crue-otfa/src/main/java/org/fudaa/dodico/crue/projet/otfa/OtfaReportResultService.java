/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.otfa;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.metier.result.TimeSimuConverter;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;
import org.fudaa.dodico.crue.projet.report.data.ReportRunContent;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;

/**
 *
 * @author Frederic Deniger
 */
public class OtfaReportResultService implements ReportResultProviderServiceContrat {

  private final OtfaCtuluUI ui;
  private final ReportRunContent runCourant;
  private OtfaReportFormuleService formuleService;
  private final CrueConfigMetier ccm;

  OtfaReportResultService(CrueConfigMetier ccm, CtuluLog log, EMHRun run, ManagerEMHScenario managerScenario, EMHScenario scenario) {
    ui = new OtfaCtuluUI(log);
    this.ccm = ccm;
    runCourant = new ReportRunContent(run, managerScenario, new ReportRunKey());
    runCourant.setLoaded(true);
    runCourant.setScenario(scenario);

  }

  public void setFormuleService(OtfaReportFormuleService formuleService) {
    this.formuleService = formuleService;
  }

  @Override
  public ReportVariableKey createVariableKey(String varName) {
    if (formuleService.isFormule(varName)) {
      return new ReportVariableKey(ReportVariableTypeEnum.FORMULE, varName);
    }
    return new ReportVariableKey(ReportVariableTypeEnum.READ, StringUtils.uncapitalize(varName));
  }

  @Override
  public Double getValue(ReportRunVariableKey key, String emhNom, Collection<ResultatTimeKey> selectedTimesInRapport) {
    throw new UnsupportedOperationException("Not supported: time must be given."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public Double getValue(ResultatTimeKey selectedTime, ReportRunVariableKey key, String emhNom, Collection<ResultatTimeKey> selectedTimesInRapport) {

    ReportVariableTypeEnum variableType = key.getVariable().getVariableType();
    switch (variableType) {
      case TIME:
        return null;
      case FORMULE:
        return formuleService.getValue(selectedTime, key, emhNom, selectedTimesInRapport);
      case READ:
        return getReadValue(selectedTime, key, emhNom);
    }
    throw new IllegalAccessError("unknown " + variableType);
  }

  private ResultatTimeKey getInitialTimeKey(ResultatTimeKey timeKey, ReportRunKey key, EMH emh) {
    ResultatTimeKey toUse = timeKey;
    //pour les runs non courants, on doit chercher l'équivalent
    if (!key.isCourant()) {
      ReportRunContent runContent = getRunCourant();
      TimeSimuConverter tempSimuConverter = TimeSimuConverter.create(getRunCourant().getScenario(), runContent.getScenario());
      EMHModeleBase parent = EMHHelper.getParent(emh).getParent();
      toUse = tempSimuConverter.getEquivalentTempsSimuRunAlternatif(toUse, parent);
    }
    return toUse;
  }

  public Double getReadValue(ResultatTimeKey selectedTime, ReportRunVariableKey key, String emhNom) {
    if (selectedTime == null) {
      return null;
    }
    EMH emh = getRunCourant().getEMH(emhNom);
    if (emh == null || emh.getResultatCalcul() == null) {
      return null;
    }
    try {
      ResultatTimeKey toUse = getInitialTimeKey(selectedTime, key.getReportRunKey(), emh);
      if (toUse == null) {
        return null;
      }
      final Map<String, Object> read = emh.getResultatCalcul().read(toUse);
      if (read == null) {
        return null;
      }
      return (Double) read.get(key.getVariable().getVariableName());
    } catch (IOException ex) {
      ui.getLog().addSevereError(ex.getMessage());
    }
    return null;
  }

  @Override
  public ReportRunContent getRunCourant() {
    return runCourant;
  }

  @Override
  public CtuluUI getUI() {
    return ui;
  }

  @Override
  public CrueConfigMetier getCcm() {
    return ccm;
  }

  @Override
  public EMH getEMH(String nom, ReportRunKey key) {
    if (!key.isCourant()) {
      return null;
    }
    return getRunCourant().getEMH(nom);
  }

  public String getVariableName(String varName) {
    if (formuleService.isFormule(varName)) {
      return varName;
    }
    ItemVariable property = getCcm().getProperty(varName);
    return property == null ? varName : property.getDisplayNom();
  }

  @Override
  public PropertyNature getPropertyNature(String varName) {
    if (formuleService.isFormule(varName)) {
      return formuleService.getPropertyNature(varName, ccm);
    }
    ItemVariable property = ccm.getProperty(varName);
    if (property == null) {
      return PropertyNature.DEFAULT;
    }
    return property.getNature();
  }

}
