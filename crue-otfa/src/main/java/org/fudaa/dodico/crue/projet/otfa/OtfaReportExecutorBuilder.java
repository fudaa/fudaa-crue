/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.otfa;

import org.fudaa.dodico.crue.projet.report.ReportViewLineInfoAndType;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;

/**
 *
 * @author Frederic Deniger
 */
public class OtfaReportExecutorBuilder {

  private final OtfaReportExecutor parent;

  public OtfaReportExecutorBuilder(OtfaReportExecutor parent) {
    this.parent = parent;
  }

  protected OtfaReportExecutorDelegate createDelegate(ReportConfigContrat config, ReportViewLineInfoAndType reportInfo,
          OtfaReportGlobalService reportGlobalService) {
    switch (config.getType()) {
      case TEMPORAL:
        return new OtfaReportExecutorDelegateTemporal(parent, reportInfo, config, reportGlobalService);
      case LONGITUDINAL:
        return new OtfaReportExecutorDelegateLongitudinal(parent, reportInfo, config, reportGlobalService);
      case MULTI_VAR:
        return new OtfaReportExecutorDelegateMultiVar(parent, reportInfo, config, reportGlobalService);
      case RPTG:
        return new OtfaReportExecutorDelegateRPTG(parent, reportInfo, config, reportGlobalService);
    }
    return null;
  }

}
