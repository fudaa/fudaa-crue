package org.fudaa.dodico.crue.projet.otfa;

import java.io.File;
import java.util.List;
import org.joda.time.LocalDateTime;

/**
 * Une campagne de tests
 *
 * @author deniger
 *
 */
public class OtfaCampagne {

  public static class OtfaRunOptions {

    private boolean effacerRunAvant;
    private boolean effacerRunApres;

    public boolean isEffacerRunAvant() {
      return effacerRunAvant;
    }

    public void setEffacerRunAvant(boolean effacerRunAvant) {
      this.effacerRunAvant = effacerRunAvant;
    }

    public boolean isEffacerRunApres() {
      return effacerRunApres;
    }

    public void setEffacerRunApres(boolean effacerRunApres) {
      this.effacerRunApres = effacerRunApres;
    }
  }
  private String commentaire;
  private String auteurCreation;
  private LocalDateTime dateCreation;
  private String auteurModification;
  private LocalDateTime dateModification;
  private OtfaRunOptions referenceOptions;
  private OtfaRunOptions cibleOptions;
  private List<OtfaCampagneLine> lines;
  private File otfaFile;

  public File getOtfaDir() {
    return otfaFile == null ? null : otfaFile.getParentFile();
  }

  public File getOtfaFile() {
    return otfaFile;
  }

  public void setOtfaFile(File otfaFile) {
    this.otfaFile = otfaFile;
  }

  /**
   * @return the lines
   */
  public List<OtfaCampagneLine> getLines() {
    return lines;
  }

  /**
   * @param lines the lines to set
   */
  public void setLines(List<OtfaCampagneLine> lines) {
    this.lines = lines;
  }

  public String getCommentaire() {
    return commentaire;
  }

  public void setCommentaire(String commentaire) {
    this.commentaire = commentaire;
  }

  public String getAuteurCreation() {
    return auteurCreation;
  }

  public void setAuteurCreation(String auteurCreation) {
    this.auteurCreation = auteurCreation;
  }

  public LocalDateTime getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(LocalDateTime dateCreation) {
    this.dateCreation = dateCreation;
  }

  public String getAuteurModification() {
    return auteurModification;
  }

  public void setAuteurModification(String auteurModification) {
    this.auteurModification = auteurModification;
  }

  public LocalDateTime getDateModification() {
    return dateModification;
  }

  public void setDateModification(LocalDateTime dateModification) {
    this.dateModification = dateModification;
  }

  public OtfaRunOptions getReferenceOptions() {
    return referenceOptions;
  }

  public void setReferenceOptions(OtfaRunOptions referenceOptions) {
    this.referenceOptions = referenceOptions;
  }

  public OtfaRunOptions getCibleOptions() {
    return cibleOptions;
  }

  public void setCibleOptions(OtfaRunOptions cibleOptions) {
    this.cibleOptions = cibleOptions;
  }
}
