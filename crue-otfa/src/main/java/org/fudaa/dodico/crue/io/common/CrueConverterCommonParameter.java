/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.common;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureOptions.LigneCampagne;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureOptions.RunOptions;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureOptions.RunParameters;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne.OtfaRunOptions;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneItem;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;

/**
 *
 * @author Frederic Deniger
 */
public class CrueConverterCommonParameter {

  public static RunParameters convertRunParametersMetierToDao(OtfaCampagneItem metier) {
    CrueDaoStructureOptions.RunParameters dao = new CrueDaoStructureOptions.RunParameters();
    dao.Etude = metier.getEtuPath();
    dao.Scenario = metier.getScenarioNom();
    dao.Coeur = metier.getCoeurName();
    dao.LancerCalcul = metier.isLaunchCompute();
    dao.Transitoire = metier.isRapportOnTransitoire();
    dao.CheminCSV = metier.getCheminCSV();
    dao.Rapport = metier.getRapport();
    return dao;
  }

  public static OtfaCampagneItem convertRunParametersDaoToMetier(RunParameters dao) {
    OtfaCampagneItem metier = new OtfaCampagneItem();
    metier.setEtuPath(dao.Etude);
    metier.setScenarioNom(dao.Scenario);
    metier.setCoeurName(dao.Coeur);
    metier.setLaunchCompute(dao.LancerCalcul);
    metier.setRapportOnTransitoire(dao.Transitoire);
    metier.setCheminCSV(dao.CheminCSV);
    metier.setRapport(dao.Rapport);
    return metier;
  }

  public static OtfaRunOptions convertRunOptionsDaoToMetier(RunOptions dao) {
    OtfaCampagne.OtfaRunOptions metier = new OtfaCampagne.OtfaRunOptions();
    metier.setEffacerRunAvant(dao.EffacerRunAvant);
    metier.setEffacerRunApres(dao.EffacerRunApres);
    return metier;
  }

  public static RunOptions convertRunOptionsMetierToDao(OtfaRunOptions metier) {
    CrueDaoStructureOptions.RunOptions dao = new CrueDaoStructureOptions.RunOptions();
    dao.EffacerRunAvant = metier.isEffacerRunAvant();
    dao.EffacerRunApres = metier.isEffacerRunApres();
    return dao;
  }

  public static List<LigneCampagne> convertCampagneLinesMetierToDao(List<OtfaCampagneLine> metier) {
    List<CrueDaoStructureOptions.LigneCampagne> dao = new ArrayList<>();
    for (OtfaCampagneLine line : metier) {
      dao.add(convertCampagneLineMetierToDao(line));
    }
    return dao;
  }

  public static OtfaCampagneLine convertCampagneLineDaoToMetier(LigneCampagne dao) {
    OtfaCampagneLine metier = new OtfaCampagneLine();
    metier.setCommentaire(dao.Commentaire);
    metier.setIndice(dao.Numero);
    if (dao.LancerComparaison != null) {
      metier.setLancerComparaison(dao.LancerComparaison);
    } else {
      metier.setLancerComparaison(true);
    }
    metier.setReference(CrueConverterCommonParameter.convertRunParametersDaoToMetier(dao.Reference));
    metier.setCible(CrueConverterCommonParameter.convertRunParametersDaoToMetier(dao.Cible));
    return metier;
  }

  public static LigneCampagne convertCampagneLineMetierToDao(OtfaCampagneLine metier) {
    CrueDaoStructureOptions.LigneCampagne dao = new CrueDaoStructureOptions.LigneCampagne();
    dao.Commentaire = metier.getCommentaire();
    dao.Numero = metier.getIndice();
    dao.LancerComparaison = Boolean.valueOf(metier.isLancerComparaison());
    dao.Reference = CrueConverterCommonParameter.convertRunParametersMetierToDao(metier.getReference());
    dao.Cible = CrueConverterCommonParameter.convertRunParametersMetierToDao(metier.getCible());
    return dao;
  }

  public static List<OtfaCampagneLine> convertCampagneLinesDaoToMetier(List<LigneCampagne> dao) {
    List<OtfaCampagneLine> metier = new ArrayList<>();
    for (CrueDaoStructureOptions.LigneCampagne line : dao) {
      metier.add(convertCampagneLineDaoToMetier(line));
    }
    return metier;
  }
}
