/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.otfa;

import com.Ostermiller.util.CSVPrinter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportMultiVarConfig;
import org.fudaa.dodico.crue.projet.report.ReportViewLineInfoAndType;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableEmhKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableHelper;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;

/**
 *
 * @author Frederic Deniger
 */
public class OtfaReportExecutorDelegateMultiVar extends OtfaReportExecutorDelegate {

  public OtfaReportExecutorDelegateMultiVar(OtfaReportExecutor parent, ReportViewLineInfoAndType reportInfo, ReportConfigContrat config,
          OtfaReportGlobalService reportGlobalService) {
    super(parent, reportInfo, config, reportGlobalService);
  }

  @Override
  protected void exportTo(CSVPrinter printer, CtuluLog log) throws IOException {
    ReportMultiVarConfig content = (ReportMultiVarConfig) super.config;
    Map<String, List<ReportRunKey>> keyByVar = ReportRunVariableHelper.getByEmh(content.getReportRunEmhs());
    List<ReportRunVariableEmhKey> keysToUse = new ArrayList<>();
    boolean containsNonCurrent = false;
    ReportRunKey current = new ReportRunKey();
    final ReportVariableKey horizontalVariableKey = super.reportGlobalService.getResultService().createVariableKey(content.getHorizontalVar());
    for (String variableName : content.getVariables()) {
      final ReportVariableKey verticalVariableKey = super.reportGlobalService.getResultService().createVariableKey(variableName);
      for (String emhName : content.getEmhs()) {
        List<ReportRunKey> keys = keyByVar.get(emhName);
        if (containsAlternatif(keys)) {
          containsNonCurrent = true;
        }
        if (containsCurrent(keys)) {
          keysToUse.add(new ReportRunVariableEmhKey(current, horizontalVariableKey, emhName));
          keysToUse.add(new ReportRunVariableEmhKey(current, verticalVariableKey, emhName));

        }
      }
    }
    if (containsNonCurrent) {
      log.addWarn(BusinessMessages.getString("Report.RunNonCurrentIgnored"));
    }
    StringBuilder builder = new StringBuilder();
    boolean addDelim = false;
    for (ReportRunVariableEmhKey key : keysToUse) {
      if (addDelim) {
        builder.append(super.delimiter);
      }
      addDelim = true;
      builder.append(quote).append(key.getRunVariableKey().getVariable().getVariableDisplayName()).append(BusinessMessages.ENTITY_SEPARATOR).append(
              key.getEmhName()).
              append(quote);

    }
    printer.printlnComment(builder.toString());
    final List<ResultatTimeKey> times = reportGlobalService.getTimeKeysToUse();
    for (ResultatTimeKey time : times) {
      for (ReportRunVariableEmhKey key : keysToUse) {
        super.printVariable(printer, reportGlobalService.getResultService().getValue(time, key.getRunVariableKey(), key.getEmhName(),
                getSelectedTimeStepInReport()), key.
                        getVariableName());
      }
      printer.println();
    }
  }

}
