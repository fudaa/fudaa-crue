/**
 *
 */
package org.fudaa.dodico.crue.projet.otfa;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.joda.time.LocalDateTime;

/**
 * @author CANEL Christophe
 *
 */
public class OtfaCampagneResultHeader {

  private String commentaire;
  private LocalDateTime date;
  private CtuluLogGroup globalValidation;

  /**
   * @return the commentaire
   */
  public String getCommentaire() {
    return commentaire;
  }

  public void cleanGlobalValidation() {
    if (globalValidation != null) {
      globalValidation = globalValidation.createCleanGroup();
    }
  }

  /**
   * @param commentaire the commentaire to set
   */
  public void setCommentaire(String commentaire) {
    this.commentaire = commentaire;
  }

  /**
   * @return the date
   */
  public LocalDateTime getDate() {
    return date;
  }

  /**
   * @param date the date to set
   */
  public void setDate(LocalDateTime date) {
    this.date = date;
  }

  /**
   * @return the globalValidation
   */
  public CtuluLogGroup getGlobalValidation() {
    if (globalValidation == null) {
      globalValidation = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    }
    return globalValidation;
  }

  /**
   * @param globalValidation the globalValidation to set
   */
  public void setGlobalValidation(CtuluLogGroup globalValidation) {
    this.globalValidation = globalValidation;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((commentaire == null) ? 0 : commentaire.hashCode());
    result = prime * result + ((date == null) ? 0 : date.hashCode());
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    OtfaCampagneResultHeader other = (OtfaCampagneResultHeader) obj;
    if (commentaire == null) {
      if (other.commentaire != null) {
        return false;
      }
    } else if (!commentaire.equals(other.commentaire)) {
      return false;
    }
    if (date == null) {
      if (other.date != null) {
        return false;
      }
    } else if (!date.equals(other.date)) {
      return false;
    }
    return true;
  }
}
