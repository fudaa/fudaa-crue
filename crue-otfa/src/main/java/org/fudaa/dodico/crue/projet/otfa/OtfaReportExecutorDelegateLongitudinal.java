/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.otfa;

import com.Ostermiller.util.CSVPrinter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.config.lit.LitNommeLimite;
import org.fudaa.dodico.crue.config.lit.ReportLongitudinalLimitHelper;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.EMHSectionProfil;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.metier.helper.ReportProfilHelper;
import org.fudaa.dodico.crue.projet.report.ReportViewLineInfoAndType;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.export.ReportExportUtils;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBrancheCartouche;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBrancheConfig;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalConfig;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalPositionBrancheContent;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalPositionBuilder;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalPositionSection;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalPositionSectionByRun;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;

/**
 *
 * @author Frederic Deniger
 */
public class OtfaReportExecutorDelegateLongitudinal extends OtfaReportExecutorDelegate {

  public OtfaReportExecutorDelegateLongitudinal(OtfaReportExecutor parent, ReportViewLineInfoAndType reportInfo, ReportConfigContrat config,
          OtfaReportGlobalService reportGlobalService) {
    super(parent, reportInfo, config, reportGlobalService);
  }

  @Override
  protected void exportTo(CSVPrinter printer, CtuluLog log) throws IOException {
    ReportLongitudinalConfig content = (ReportLongitudinalConfig) super.config;
    final CrueConfigMetier ccm = reportGlobalService.getResultService().getCcm();
    ReportLongitudinalLimitHelper limitHelper = new ReportLongitudinalLimitHelper(ccm);
    final Map<String, String> displayNameById = limitHelper.getDisplayNameById();
    final ItemVariable propertyXp = ccm.getProperty(CrueConfigMetierConstants.PROP_XP);
    StringBuilder builder = new StringBuilder();
    builder.append(quote).append(BusinessMessages.getString("otfa.report.longitudinalFirstColName")).append(quote).append(super.delimiter);
    builder.append(quote).append(propertyXp.getDisplayNom()).append(quote);
    final List<ResultatTimeKey> profilTimes = content.getTimes();
    final List<ReportRunVariableKey> variables = content.getProfilVariables();
    for (ReportRunVariableKey variable : variables) {
      if (ReportVariableTypeEnum.LIMIT_PROFIL.equals(variable.getVariable().getVariableType())) {
        String displayName = displayNameById.get(variable.getVariableName());
        if (StringUtils.isEmpty(displayName)) {
          displayName = variable.getVariableName();
        }
        builder.append(super.delimiter).append(quote).append(displayName).append(quote);
      }
    }
    boolean containsNonCurrent = false;
    for (ResultatTimeKey time : profilTimes) {
      for (ReportRunVariableKey variable : variables) {
        if (variable.getReportRunKey().isAlternatifRun()) {
          containsNonCurrent = true;
        }
        if (variable.isReadOrVariable()) {
          builder.append(super.delimiter).append(quote).append(variable.getVariable().getVariableDisplayName()).append(
                  BusinessMessages.ENTITY_SEPARATOR).append(ResultatTimeKey.timeToString(time)).append(quote);
        }
      }
    }
    if (containsNonCurrent) {
      log.addWarn(BusinessMessages.getString("Report.RunNonCurrentIgnored"));
    }
    printer.writelnComment(builder.toString());
    final List<ReportLongitudinalBrancheConfig> branchesConfig = content.getBranchesConfig();
    ReportLongitudinalPositionBuilder positionBuilder = new ReportLongitudinalPositionBuilder(reportGlobalService.getResultService(), ccm);
    final ReportRunKey runCourant = new ReportRunKey();
    Set<ReportRunKey> keys = Collections.singleton(runCourant);
    double xpInit = 0;
    ReportLongitudinalPositionBrancheContent result = new ReportLongitudinalPositionBrancheContent();
    for (ReportLongitudinalBrancheConfig brancheConfig : branchesConfig) {
      xpInit = xpInit + brancheConfig.getDecalXp();
      positionBuilder.buildPositions(xpInit, brancheConfig, result, keys);
      xpInit = xpInit + brancheConfig.getLength();
    }
    final List<ReportLongitudinalBrancheCartouche> cartouches = result.getCartouches();

    for (ReportLongitudinalBrancheCartouche cartouche : cartouches) {
      final ReportLongitudinalPositionSectionByRun sectionPositions = cartouche.getSectionPositions(runCourant);
      final List<ReportLongitudinalPositionSection> sectionDisplayPositions = sectionPositions.getSectionDisplayPositions();
      for (ReportLongitudinalPositionSection reportLongitudinalPositionSection : sectionDisplayPositions) {
        final CatEMHSection section = reportLongitudinalPositionSection.getName();
        printer.write(section.getBranche().getNom() + " / " + section.getNom());
        printer.write(propertyXp.format(reportLongitudinalPositionSection.getXpDisplay(), DecimalFormatEpsilonEnum.COMPARISON));
        final PropertyNature nature = reportGlobalService.getResultService().getPropertyNature(CrueConfigMetierConstants.PROP_Z);

        for (ReportRunVariableKey variable : variables) {
          if (ReportVariableTypeEnum.LIMIT_PROFIL.equals(variable.getVariable().getVariableType())) {
            ItemEnum etiquette = limitHelper.getEtiquette(variable.getVariable().getVariableName());
            LitNommeLimite limite = limitHelper.getLimite(variable.getVariable().getVariableName());
            if (etiquette != null || limite != null) {
              EMHSectionProfil retrieveProfil = ReportProfilHelper.retrieveProfil(section);
              if (retrieveProfil != null) {
                DonPrtGeoProfilSection profil = DonPrtHelper.getProfilSection(retrieveProfil);
                Double z = ReportExportUtils.computeZ(etiquette, limite, profil, ccm);
                printVariable(printer, z, variable);
              }
            }
          }
        }
        for (ResultatTimeKey time : profilTimes) {
          for (ReportRunVariableKey variable : variables) {
            if (!variable.getReportRunKey().isAlternatifRun() && variable.isReadOrVariable()) {
              Double value = reportGlobalService.getResultService().getValue(time, variable, section.getNom(), content.getSelectedTimeStepInReport());
              printVariable(printer, value, variable);
            }
          }
        }
        printer.println();
      }
    }
  }

}
