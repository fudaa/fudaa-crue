package org.fudaa.dodico.crue.projet.otfa;

import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Permet d'extraire dans l'ordre les fichiers etu utilisés dans une campagne
 *
 * @author deniger
 */
public class OtfaEtuFileCoeurExtractor {
  public OtfaEtuFileCoeurExtractor() {
  }

  public List<String> extract(OtfaCampagne campagne) {
    Set<String> otfaEtuFileCoeurs = new HashSet<>();
    campagne.getLines().forEach(otfaCampagneLine ->
    {
      add(otfaCampagneLine.getReference(), otfaEtuFileCoeurs);
      add(otfaCampagneLine.getCible(), otfaEtuFileCoeurs);
    });
    return otfaEtuFileCoeurs.stream().sorted().collect(Collectors.toList());
  }

  private void add(OtfaCampagneItem item, Set<String> target) {
    if (item != null && StringUtils.isNotEmpty(item.getEtuPath())) {
      target.add(item.getEtuPath());
    }
  }
}
