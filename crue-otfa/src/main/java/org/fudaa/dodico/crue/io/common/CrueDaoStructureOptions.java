package org.fudaa.dodico.crue.io.common;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;

/**
 * @author CANEL Christophe
 *
 */
public class CrueDaoStructureOptions implements CrueDataDaoStructure {

  /**
   * {@inheritDoc}
   */
  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {

    xstream.alias("LigneCampagne", LigneCampagne.class);
    xstream.useAttributeFor(LigneCampagne.class, "Numero");
  }

  public static class RunOptions {

    public boolean EffacerRunAvant;
    public boolean EffacerRunApres;
  }

  public static class LigneCampagne {

    public int Numero;
    public String Commentaire;
    public Boolean LancerComparaison;
    public RunParameters Reference;
    public RunParameters Cible;
  }

  public static class RunParameters {

    public String Etude;
    public String Scenario;
    public String Coeur;
    public boolean LancerCalcul;
    public boolean Transitoire;
    public String Rapport;
    public String CheminCSV;
  }
}
