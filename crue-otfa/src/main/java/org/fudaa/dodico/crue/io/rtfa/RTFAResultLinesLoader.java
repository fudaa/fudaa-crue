/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.rtfa;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.line.CrueLineLogReaderWriter;
import org.fudaa.dodico.crue.io.line.CrueLineResultReaderWriter;
import org.fudaa.dodico.crue.io.otfa.OtfaFileUtils;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLineResultComparaisons;

/**
 *
 * @author Frederic Deniger
 */
public class RTFAResultLinesLoader {

  private final File rtfaZipFile;

  public RTFAResultLinesLoader(File rtfaZipFile) {
    this.rtfaZipFile = OtfaFileUtils.getRtfaZipFile(rtfaZipFile);
  }

  public CrueIOResu<CtuluLogGroup> loadLogs(OtfaCampagneLine line) {
    CrueIOResu<CtuluLogGroup> res = null;
    CrueLineLogReaderWriter reader = new CrueLineLogReaderWriter(CrueRTFAReaderWriter.LAST_VERSION);
    ZipFile file = null;
    InputStream inputStream = null;
    try {
      file = new ZipFile(rtfaZipFile);
      inputStream = file.getInputStream(new ZipEntry(RTFAResultLines.getCtfaFileName(line)));
      CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
      res = reader.readXML(inputStream, log, null);
    } catch (Exception e) {
      Logger.getLogger(RTFAResultLinesLoader.class.getName()).log(Level.INFO, "message {0}", e);
    } finally {
      if (file != null) {
        try {
          file.close();
        } catch (IOException ex) {
          Logger.getLogger(RTFAResultLinesLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }

    return res;

  }

  public CrueIOResu<OtfaCampagneLineResultComparaisons> loadResults(OtfaCampagneLine line) {
    CrueLineResultReaderWriter reader = new CrueLineResultReaderWriter(CrueRTFAReaderWriter.LAST_VERSION);
    CrueIOResu<OtfaCampagneLineResultComparaisons> res = null;
    ZipFile file = null;
    InputStream inputStream = null;
    try {
      file = new ZipFile(rtfaZipFile);
      inputStream = file.getInputStream(new ZipEntry(RTFAResultLines.getRtfaFileName(line)));
      CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
      res = reader.readXML(inputStream, log, null);
    } catch (Exception e) {
      Logger.getLogger(RTFAResultLinesLoader.class.getName()).log(Level.INFO, "message {0}", e);
    } finally {
      if (file != null) {
        try {
          file.close();
        } catch (IOException ex) {
          Logger.getLogger(RTFAResultLinesLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }

    return res;
  }
}
