package org.fudaa.dodico.crue.io.otfa;

import org.fudaa.dodico.crue.common.CrueFileHelper;

import java.io.File;

/**
 *
 * @author deniger
 */
public class OtfaFileUtils {

  /**
   * otfa.xml
   */
  public static final String OTFA_EXTENSION = "otfa.xml";
  /**
   * ctfa.xml
   */
  public static final String RTFA_ZIP_EXTENSION = "rtfa.zip";
  /**
   * rtfa.xml
   */
  public static final String RTFA_EXTENSION = "rtfa.xml";

  public static File getRtfaZipFile(File otfaFile) {
    return CrueFileHelper.changeExtension(otfaFile, OTFA_EXTENSION, RTFA_ZIP_EXTENSION);
  }

  public static File addOtfaExtensionIfNeeded(File otfa) {
    if (otfa == null || otfa.getName() == null) {
      return otfa;
    }
    if (!otfa.getName().endsWith(OTFA_EXTENSION)) {
      return new File(otfa.getParentFile(), otfa.getName() + "." + OTFA_EXTENSION);
    }
    return otfa;
  }

  public static File getRtfaFile(File otfaFile) {
    return CrueFileHelper.changeExtension(otfaFile, OTFA_EXTENSION, RTFA_EXTENSION);
  }
}
