/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.rtfa;

import java.io.File;
import java.util.concurrent.Callable;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.line.CrueLineResultReaderWriter;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLineResult;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLineResultComparaisons;

/**
 *
 * @author Frederic Deniger
 */
public class RTFAWriteResultCallable implements Callable<CtuluLog> {

  private final OtfaCampagneLineResult launch;
  private final File targetDir;

  public RTFAWriteResultCallable(OtfaCampagneLineResult launch, File targetDir) {
    this.launch = launch;
    this.targetDir = targetDir;
  }

  @Override
  public CtuluLog call() throws Exception {
    final CrueLineResultReaderWriter writer = new CrueLineResultReaderWriter(CrueRTFAReaderWriter.LAST_VERSION);
    final CrueIOResu<OtfaCampagneLineResultComparaisons> resu = new CrueIOResu<>(launch.getComparisonResult());
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    File target = new File(targetDir, RTFAResultLines.getRtfaFileName(launch.getInitialLine()));
    writer.writeXMLMetier(resu, target, log, null);
    return log;
  }
}
