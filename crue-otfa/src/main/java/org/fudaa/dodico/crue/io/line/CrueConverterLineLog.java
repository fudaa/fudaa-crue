package org.fudaa.dodico.crue.io.line;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.io.common.CrueConverterCommonLog;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.metier.CrueData;

/**
 * @author CANEL Christophe
 */
public class CrueConverterLineLog implements CrueDataConverter<CrueDaoLineLog, CtuluLogGroup> {

  /**
   * {@inheritDoc}
   */
  @Override
  public CtuluLogGroup convertDaoToMetier(final CrueDaoLineLog dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
    return CrueConverterCommonLog.convertDaoToMetier(dao.Logs);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CtuluLogGroup getConverterData(final CrueData in) {
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CrueDaoLineLog convertMetierToDao(final CtuluLogGroup metier, final CtuluLog ctuluLog) {
    final CrueDaoLineLog dao = new CrueDaoLineLog();
    dao.Logs = CrueConverterCommonLog.convertMetierToDao(metier);
    return dao;
  }
}
