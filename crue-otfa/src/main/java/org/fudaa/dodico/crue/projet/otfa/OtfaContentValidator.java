package org.fudaa.dodico.crue.projet.otfa;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.CoeurManager;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory.VersionResult;
import org.fudaa.dodico.crue.io.common.FileLocker;
import org.fudaa.dodico.crue.io.common.FileLocker.Data;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.ReportIndexReaderHelper;
import org.fudaa.dodico.crue.projet.report.ReportViewLineInfoAndType;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class OtfaContentValidator {
    private final CoeurManager coeurManager;
    private final FileLocker fileLocker = new FileLocker();

    /**
     * @param coeurManager le manager de coeurs
     */
    public OtfaContentValidator(final CoeurManager coeurManager) {
        super();
        this.coeurManager = coeurManager;
    }

    private CoeurConfigContrat getCoeurForItem(final OtfaCampagneItem item, final File etuFile) {
        final CoeurConfigContrat coeur;
        if (item.isCrue9()) {
            final VersionResult findVersion = Crue10FileFormatFactory.findVersion(etuFile);
            coeur = coeurManager.getCoeurConfigDefault(findVersion.getVersion());
        } else {
            coeur = this.coeurManager.getCoeurConfig(item.getCoeurName());
        }
        return coeur;
    }

    public CtuluLogGroup valid(final OtfaCampagne campagne, final boolean beforeExecution) {
        return valid(campagne, true, beforeExecution);
    }

    /**
     * @param campagne        la campagne
     * @param complete        true si termin&eacute; completement
     * @param beforeExecution si true verifie que toutes etudes ne sont pas bloqu&eacute;s.
     * @return un CtuluLogGroup avec un CtuluLog par ligne de la campagne
     */
    public CtuluLogGroup valid(final OtfaCampagne campagne, final boolean complete, final boolean beforeExecution) {
        final CtuluLogGroup logGroup = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
        logGroup.setDescription("otfa.validator.log");
        final CtuluLog prevalidation = logGroup.createNewLog("otfa.validator.prevalidation.log");
        final List<OtfaCampagneLine> lines = new ArrayList(campagne.getLines());
        final Map<Integer, CtuluLog> logsByLineId = new HashMap<>();
        for (final OtfaCampagneLine otfaCampagneLine : lines) {
            final CtuluLog logi = logGroup.createNewLog("otfa.validator.logDescription");
            logi.setDescriptionArgs(Integer.toString(otfaCampagneLine.getIndice()));
            final Integer indice = otfaCampagneLine.getIndice();
            if (logsByLineId.containsKey(indice)) {
                prevalidation.addSevereError("otfa.validator.indiceNotUnique", indice);
            }
            logi.setDescriptionArgs(Integer.toString(indice));
            logsByLineId.put(indice, logi);
        }
        //erreur fatale dans les tests basiques on arrete la validation.
        if (prevalidation.containsSevereError()) {
            return logGroup;
        }
        validBasics(campagne, logsByLineId);
        validAllEtuExist(campagne, logsByLineId);
        if (beforeExecution) {
            validAllEtuNotLocked(campagne, logsByLineId);
        }

        validAllCoeurExist(campagne, logsByLineId);
        validateXsdCompatible(campagne, logsByLineId);
        if (complete) {
            validScenarioExist(campagne, logsByLineId);
        }
        if (complete) {
            validReportExist(campagne, logsByLineId);
        }

        return logGroup;
    }

    private void validBasics(final OtfaCampagne campagne, final Map<Integer, CtuluLog> logsByLineId) {
        for (final OtfaCampagneLine line : campagne.getLines()) {
            final CtuluLog log = logsByLineId.get(line.getIndice());
            if (StringUtils.isEmpty(line.getReference().getEtuPath())) {
                log.addSevereError("otfa.validator.referenceEtuNoSet");
            }
            if (StringUtils.isEmpty(line.getReference().getScenarioNom())) {
                log.addSevereError("otfa.validator.referenceScenarioNoSet");
            }
            if (StringUtils.isEmpty(line.getReference().getCoeurName())) {
                log.addSevereError("otfa.validator.referenceCoeurNoSet");
            }
            final boolean cibleEtuEmpty = StringUtils.isEmpty(line.getCible().getEtuPath());
            final boolean cibleScenarioEmpty = StringUtils.isEmpty(line.getCible().getScenarioNom());
            final boolean coeurEmpty = StringUtils.isEmpty(line.getCible().getCoeurName());
            if (line.isLancerComparaison() || !coeurEmpty) {
                if ((cibleEtuEmpty && !cibleScenarioEmpty) || (!cibleEtuEmpty && cibleScenarioEmpty)) {
                    log.addSevereError("otfa.validator.cibleConversionMisconfigured");
                }
            }
            if (line.isLancerComparaison() && coeurEmpty) {
                log.addSevereError("otfa.validator.cibleCoeurNoSet");
            }
            if (!line.isLancerComparaison() && coeurEmpty && (!cibleEtuEmpty || !cibleScenarioEmpty)) {
                log.addSevereError("otfa.validator.cibleCoeurEmptyButNotScenarioEtude");
            }
        }
    }

    private void validAllCoeurExist(final OtfaCampagne campagne, final Map<Integer, CtuluLog> logsByLineId) {

        for (final OtfaCampagneLine line : campagne.getLines()) {
            final CtuluLog log = logsByLineId.get(line.getIndice());
            String coeurName = line.getReference().getCoeurName();
            if (line.getReference().isNotCrue9() && !this.coeurExist(coeurName)) {
                log.addSevereError("otfa.validator.referenceCoeurNotFound", coeurName);
            }
            coeurName = line.getCible().getCoeurName();
            final boolean coeurEmpty = StringUtils.isEmpty(coeurName);
            if (!coeurEmpty && line.getCible().isNotCrue9() && !this.coeurExist(coeurName)) {
                log.addSevereError("otfa.validator.cibleCoeurNotFound", coeurName);
            }
        }
    }

    private boolean coeurExist(final String name) {
        return (this.coeurManager.getCoeurConfig(name) != null);
    }

    /**
     * Test que tous les fichiers etu de la campagne existe.
     *
     * @param campagne la campagne
     */
    private void validAllEtuExist(final OtfaCampagne campagne, final Map<Integer, CtuluLog> logsByLineId) {
        final List<OtfaCampagneLine> lines = new ArrayList<>(campagne.getLines());
        for (final OtfaCampagneLine line : lines) {
            final CtuluLog log = logsByLineId.get(line.getIndice());
            OtfaCampagneItem item = line.getReference();
            final File etuFile = item.getEtuFile(campagne.getOtfaDir());

            if (etuFile == null || !etuFile.isFile()) {
                log.addSevereError("otfa.validator.refEtuFileNotFound", etuFile == null ? "<?>" : etuFile.getAbsolutePath());
            }
            if (StringUtils.isNotEmpty(line.getCible().getEtuPath())) {
                item = line.getCible();
                final File cibleFile = item.getEtuFile(campagne.getOtfaDir());
                if (!cibleFile.isFile()) {
                    log.addSevereError("otfa.validator.cibleEtuFileNotFound", item.getEtuPath());
                }
            }
        }
    }

    private void validAllEtuNotLocked(final OtfaCampagne campagne, final Map<Integer, CtuluLog> logsByLineId) {
        for (final OtfaCampagneLine line : campagne.getLines()) {
            final CtuluLog log = logsByLineId.get(line.getIndice());
            OtfaCampagneItem item = line.getReference();
            final File etuFile = item.getEtuFile(campagne.getOtfaDir());

            if (etuFile != null && etuFile.isFile() && fileLocker.isLock(etuFile)) {
                final Data lockedBy = fileLocker.isLockedBy(etuFile);
                if (lockedBy == null) {
                    log.addSevereError("otfa.referenceFileEtu.isLockedByUnknown", item.getEtuPath());
                } else {
                    log.addSevereError("otfa.referenceFileEtu.isLocked", item.getEtuPath(), lockedBy.getUserName(), lockedBy.getTime());
                }
            }
            if (StringUtils.isNotEmpty(line.getCible().getEtuPath())) {
                item = line.getCible();
                final File cibleFile = item.getEtuFile(campagne.getOtfaDir());
                if (cibleFile != null && cibleFile.isFile() && fileLocker.isLock(cibleFile)) {
                    final Data lockedBy = fileLocker.isLockedBy(cibleFile);
                    if (lockedBy == null) {
                        log.addSevereError("otfa.cibleFileEtu.isLockedByUnknown", cibleFile.getAbsoluteFile());
                    } else {
                        log.addSevereError("otfa.cibleFileEtu.isLocked", item.getEtuPath(), lockedBy.getUserName(), lockedBy.getTime());
                    }
                }
            }
        }
    }

    /**
     * une campagne est valide si pour une étude donnée, la même version de xsd est utilisée.
     *
     * @param campagne la campagne
     */
    private void validateXsdCompatible(final OtfaCampagne campagne, final Map<Integer, CtuluLog> logsByLineId) {
        final Map<File, String> xsdVersionByEtuFile = new HashMap<>();
        final List<OtfaCampagneLine> lines = new ArrayList(campagne.getLines());

        for (final OtfaCampagneLine line : lines) {
            final CtuluLog log = logsByLineId.get(line.getIndice());
            this.validateXsdCompatible(campagne, line.getFinalReference(), xsdVersionByEtuFile, log, true);
            this.validateXsdCompatible(campagne, line.getFinalCible(), xsdVersionByEtuFile, log, false);
        }
    }

    private void validateXsdCompatible(final OtfaCampagne campagne, final OtfaCampagneItem item, final Map<File, String> xsdVersionByEtuFile,
                                       final CtuluLog log, final boolean isRef) {
        if (item.isCrue9()) {
            return;
        }
        final CoeurConfigContrat coeurConfig = this.coeurManager.getCoeurConfig(item.getCoeurName());
        //la validation du cas pas de coeur config deja traite:
        if (coeurConfig == null) {
            return;
        }
        final String xsdVersion = coeurConfig.getXsdVersion();
        final File etuFile = item.getEtuFile(campagne.getOtfaDir());
        //la validation du cas etuFile est deja géré
        if (etuFile != null && etuFile.exists()) {
            final String etuFileXsdVersion = xsdVersionByEtuFile.get(etuFile);
            if (etuFileXsdVersion == null) {
                final VersionResult findVersion = Crue10FileFormatFactory.findVersion(etuFile);
                if (findVersion.getLog().containsErrorOrSevereError()) {
                    log.addAllLogRecord(findVersion.getLog());
                } else if (!StringUtils.equals(xsdVersion, findVersion.getVersion())) {

                    log.addSevereError(isRef ? "otfa.validator.referenceXsdNotCompatible" : "otfa.validator.cibleXsdNotCompatible",
                            xsdVersion, findVersion.getVersion());
                }
            }
        }
    }

    private void validScenarioExist(final OtfaCampagne campagne, final Map<Integer, CtuluLog> logsByLineId) {
        final Map<String, Map<String, CrueVersionType>> scenariosByEtudes = new HashMap<>();

        for (final OtfaCampagneLine line : campagne.getLines()) {
            final CtuluLog log = logsByLineId.get(line.getIndice());
            final OtfaCampagneItem reference = line.getReference();
            //ce tests est deja effectue en amont.
            if (StringUtils.isBlank(reference.getEtuPath()) || StringUtils.isBlank(reference.getScenarioNom())) {
                continue;
            }
            final OtfaCampagneItem cible = line.getCible();
            this.validScenarioExist(campagne, reference, scenariosByEtudes, log);

            if (line.isCibleAConversion() && scenariosByEtudes.containsKey(reference.getEtuPath())) {
                final Map<String, CrueVersionType> scenarios = scenariosByEtudes.get(reference.getEtuPath());
                if (scenarios != null) {
                    scenarios.put(line.getFinalCible().getScenarioNom(),
                            line.getCible().isCrue9() ? CrueVersionType.CRUE9 : CrueVersionType.CRUE10);
                }
            } else {
                this.validScenarioExist(campagne, cible, scenariosByEtudes, log);
            }
        }
    }

    private boolean rapportIsBlankOrExists(final OtfaCampagne campagne, final OtfaCampagneItem item) {
        if (StringUtils.isNotBlank(item.getRapport())) {
            final File etuFile = item.getEtuFile(campagne.getOtfaDir());
            if (etuFile != null && etuFile.exists()) {
                final ReportContentType contentType = ReportViewLineInfoAndType.getContentType(item.getRapport());
                if (contentType == null) {
                    return false;
                }
                final CoeurConfigContrat coeur = getCoeurForItem(item, etuFile);
                //validation effectuée auparavant:
                if (coeur == null) {
                    return false;
                }
                final CrueOperationResult<EMHProjet> result = EMHProjetController.readProjet(etuFile, coeur,false);
                if (result == null || result.getResult() == null) {
                    return false;
                }
                final ReportIndexReaderHelper helper = new ReportIndexReaderHelper(result.getResult());
                return helper.containsReportWithIdOrDescription(contentType, item.getRapport()) != null;
            }
        }
        return true;
    }

    private void validReportExist(final OtfaCampagne campagne, final Map<Integer, CtuluLog> logsByLineId) {

        for (final OtfaCampagneLine line : campagne.getLines()) {
            final CtuluLog log = logsByLineId.get(line.getIndice());
            if (!rapportIsBlankOrExists(campagne, line.getReference())) {
                log.addSevereError("otfa.validator.reportNotFoundReference", line.getReference().getRapport());
            }
            if (!rapportIsBlankOrExists(campagne, line.getFinalCible())) {
                log.addSevereError("otfa.validator.reportNotFoundCible", line.getFinalCible().getRapport());
            }
            if (StringUtils.isNotEmpty(line.getCible().getRapport()) && line.getCible().isEmpty()) {
                log.addSevereError("otfa.validator.reportNotEmptyButCibleEmpty");
            }
        }
    }

    private void validScenarioExist(final OtfaCampagne campagne, final OtfaCampagneItem item,
                                    final Map<String, Map<String, CrueVersionType>> scenariosByEtudes, final CtuluLog log) {
        final File etuFile = item.getEtuFile(campagne.getOtfaDir());
        //validation effectuée auparavant:
        if (etuFile == null || !etuFile.exists()) {
            return;
        }
        if (!scenariosByEtudes.containsKey(item.getEtuPath())) {
            final CoeurConfigContrat coeur = getCoeurForItem(item, etuFile);
            //validation effectuée auparavant:
            if (coeur == null) {
                return;
            }
            final CrueOperationResult<EMHProjet> result = EMHProjetController.readProjet(etuFile, coeur,false);
            if (result.getResult() == null) {
                scenariosByEtudes.put(item.getEtuPath(), null);
            } else {

                scenariosByEtudes.put(item.getEtuPath(), this.getAllScenarios(result.getResult()));
            }
        }

        final Map<String, CrueVersionType> scenarios = scenariosByEtudes.get(item.getEtuPath());

        final String name = item.getScenarioNom();
        if (scenarios != null) {
//le cas null permet d'éviter d'afficher de doubler les messages d'erreur.
            //si un projet ne peut pas être chargé (le cas null), un test doit l'avoir detecté en amont.
            if (!scenarios.containsKey(name)) {
                log.addSevereError("otfa.validator.scenarioNotFound", name);
            } else {
                final CrueVersionType versionItem = item.isCrue9() ? CrueVersionType.CRUE9 : CrueVersionType.CRUE10;

                final CrueVersionType scenarioVersion = scenarios.get(name);

                if (!versionItem.equals(scenarioVersion)) {
                    //attention, il faut également prendre en compte le cas ou le coeur crue10 n'existe pas :
                    //erreur relevée par ailleurs: il faut eviter les remontées multiples
                    if (versionItem.equals(CrueVersionType.CRUE10) && !coeurExist(item.getCoeurName())) {
                        Logger.getLogger(OtfaContentValidator.class
                                .getName()).log(Level.INFO,
                                "pas d''erreur le scenario {0} car deja remonté dans un test precedent",
                                scenarioVersion);
                    } else {
                        log.addSevereError("otfa.validator.crueVersionNonCompatible", name, scenarioVersion.getDisplayName(),
                                versionItem.getDisplayName());
                    }
                }
            }
        }
    }

    private Map<String, CrueVersionType> getAllScenarios(final EMHProjet projet) {
        final Map<String, CrueVersionType> scenarios = new HashMap<>();
        if (projet == null) {
            return scenarios;
        }
        for (final ManagerEMHScenario scenario : projet.getListeScenarios()) {
            scenarios.put(scenario.getNom(), scenario.getInfosVersions().getCrueVersion());
        }

        return scenarios;
    }
}
