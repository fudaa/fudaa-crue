package org.fudaa.dodico.crue.projet.otfa;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.projet.create.ConversionNameConverter;

/**
 * Correspond a une ligne d'une campagne.
 *
 * @author deniger
 */
public class OtfaCampagneLine {
  
  private OtfaCampagneItem reference;
  private OtfaCampagneItem cible;
  private String commentaire;
  private boolean lancerComparaison = true;

  /**
   * correspond à l'indice de la ligne dans la liste de {@link OtfaCampagne}
   */
  private int indice;
  
  public OtfaCampagneLine() {
  }
  
  public boolean isLancerComparaison() {
    return lancerComparaison;
  }
  
  public void setLancerComparaison(boolean lancerComparaison) {
    this.lancerComparaison = lancerComparaison;
  }
  
  public OtfaCampagneLine(OtfaCampagneLine toCopy) {
    if (toCopy != null) {
      if (toCopy.reference != null) {
        reference = new OtfaCampagneItem(toCopy.reference);
      }
      if (toCopy.cible != null) {
        cible = new OtfaCampagneItem(toCopy.cible);
      }
      this.commentaire = toCopy.commentaire;
      this.indice = toCopy.indice;
    }
  }

  /**
   * @return the reference
   */
  public OtfaCampagneItem getReference() {
    return reference;
  }

  /**
   * @param reference the reference to set
   */
  public void setReference(OtfaCampagneItem reference) {
    this.reference = reference;
  }

  /**
   *
   * @return true si la cible correspond a une conversion d'etude
   */
  public boolean isCibleAConversion() {
    if (cible.isEmpty()) {
      return false;
    }
    return (StringUtils.isBlank(cible.getScenarioNom()) && StringUtils.isBlank(cible.getEtuPath()));
  }
  
  public OtfaCampagneItem getFinalReference() {
    return reference;
  }
  

  /**
   * @return if the cible is a conversion return the final etu file and scenario
   */
  public OtfaCampagneItem getFinalCible() {
    if (isCibleAConversion()) {
      OtfaCampagneItem newCible = new OtfaCampagneItem();
      newCible.setLaunchCompute(cible.isLaunchCompute());
      newCible.setEtuPath(reference.getEtuPath());
      newCible.setCoeurName(cible.getCoeurName());
      newCible.initRapportDataFrom(cible);

      ConversionNameConverter converter = new ConversionNameConverter(cible.isCrue9() ? CrueVersionType.CRUE9 : CrueVersionType.CRUE10);
      newCible.setScenarioNom(converter.convert(reference.getScenarioNom()));
      
      return newCible;
    }
    return cible;
    
  }

  /**
   * @return the cible
   */
  public OtfaCampagneItem getCible() {
    return cible;
  }

  /**
   * @param cible the cible to set
   */
  public void setCible(OtfaCampagneItem cible) {
    this.cible = cible;
  }

  /**
   * @return the commentaire
   */
  public String getCommentaire() {
    return commentaire;
  }

  /**
   * @param commentaire the commentaire to set
   */
  public void setCommentaire(String commentaire) {
    this.commentaire = commentaire;
  }

  /**
   * @return the indice
   */
  public int getIndice() {
    return indice;
  }

  /**
   * @param indice the indice to set
   */
  public void setIndice(int indice) {
    this.indice = indice;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cible == null) ? 0 : cible.hashCode());
    result = prime * result + ((commentaire == null) ? 0 : commentaire.hashCode());
    result = prime * result + indice;
    result = prime * result + ((reference == null) ? 0 : reference.hashCode());
    if (lancerComparaison) {
      result++;
    }
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    OtfaCampagneLine other = (OtfaCampagneLine) obj;
    if (cible == null) {
      if (other.cible != null) {
        return false;
      }
    } else if (!cible.equals(other.cible)) {
      return false;
    }
    if (commentaire == null) {
      if (other.commentaire != null) {
        return false;
      }
    } else if (!commentaire.equals(other.commentaire)) {
      return false;
    }
    if (indice != other.indice) {
      return false;
    }
    if (lancerComparaison != other.lancerComparaison) {
      return false;
    }
    if (reference == null) {
      if (other.reference != null) {
        return false;
      }
    } else if (!reference.equals(other.reference)) {
      return false;
    }
    return true;
  }
}
