package org.fudaa.dodico.crue.projet.otfa;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;

import java.io.File;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class OtfaCampagneItem {
  public static final String CRUE9 = CrueVersionType.CRUE9.getTypeId().toUpperCase();
  private String etuFile;
  private String scenarioNom;
  private boolean launchCompute;
  private String coeurName;
  private boolean rapportOnTransitoire;
  private String rapport;
  private String cheminCSV;

  public OtfaCampagneItem() {
  }


  OtfaCampagneItem(OtfaCampagneItem toCopy) {
    if (toCopy != null) {
      this.etuFile = toCopy.etuFile;
      this.scenarioNom = toCopy.scenarioNom;
      this.coeurName = toCopy.coeurName;
      this.launchCompute = toCopy.launchCompute;
    }
  }

  public boolean isRapportOnTransitoire() {
    return rapportOnTransitoire;
  }

  public void setRapportOnTransitoire(boolean rapportOnTransitoire) {
    this.rapportOnTransitoire = rapportOnTransitoire;
  }

  public String getRapport() {
    return rapport;
  }

  public void setRapport(String rapport) {
    this.rapport = rapport;
  }

  public String getCheminCSV() {
    return cheminCSV;
  }

  public void setCheminCSV(String cheminCSV) {
    this.cheminCSV = cheminCSV;
  }

  public boolean isEmpty() {
    return StringUtils.isBlank(coeurName) && StringUtils.isBlank(scenarioNom) && StringUtils.isBlank(etuFile);
  }

  /**
   * @return true si la version utilisee est Crue9.
   */
  public boolean isCrue9() {
    return CRUE9.equals(coeurName);
  }

  public boolean isNotCrue9() {
    return !isCrue9();
  }

  /**
   * @return the etuFile
   */
  public String getEtuPath() {
    return etuFile;
  }

  /**
   * @param otfaDir répertoire contenant le fichier otfa.
   * @return le fichier en prenant en compte le répertoire de otfa
   */
  public File getEtuFile(File otfaDir) {
    if (StringUtils.isBlank(etuFile)) {
      return null;
    }
    if (otfaDir == null && CrueFileHelper.isRelative(etuFile)) {
      Logger.getLogger(OtfaCampagneItem.class.getName()).log(Level.SEVERE, "otfa dir is not set");
      return null;
    }
    return CrueFileHelper.getCanonicalBaseDir(otfaDir, etuFile);
  }

  /**
   * @param etuFile the etuFile to set
   */
  public void setEtuPath(String etuFile) {
    this.etuFile = etuFile;
  }

  /**
   * @return the scenarioNom
   */
  public String getScenarioNom() {
    return scenarioNom;
  }

  /**
   * @param scenarioNom the scenarioNom to set
   */
  public void setScenarioNom(String scenarioNom) {
    this.scenarioNom = scenarioNom;
  }

  /**
   * @return the launchCompute
   */
  public boolean isLaunchCompute() {
    return launchCompute;
  }

  /**
   * @param launchCompute the launchCompute to set
   */
  public void setLaunchCompute(boolean launchCompute) {
    this.launchCompute = launchCompute;
  }

  /**
   * @return the coeurName
   */
  public String getCoeurName() {
    return coeurName;
  }

  /**
   * @param coeurName the coeurName to set
   */
  public void setCoeurName(String coeurName) {
    this.coeurName = coeurName;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    int hash = 7;
    hash = 19 * hash + Objects.hashCode(this.etuFile);
    hash = 19 * hash + Objects.hashCode(this.scenarioNom);
    hash = 19 * hash + (this.launchCompute ? 1 : 0);
    hash = 19 * hash + Objects.hashCode(this.coeurName);
    hash = 19 * hash + (this.rapportOnTransitoire ? 1 : 0);
    hash = 19 * hash + Objects.hashCode(this.rapport);
    hash = 19 * hash + Objects.hashCode(this.cheminCSV);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final OtfaCampagneItem other = (OtfaCampagneItem) obj;
    if (!Objects.equals(this.etuFile, other.etuFile)) {
      return false;
    }
    if (!Objects.equals(this.scenarioNom, other.scenarioNom)) {
      return false;
    }
    if (this.launchCompute != other.launchCompute) {
      return false;
    }
    if (!Objects.equals(this.coeurName, other.coeurName)) {
      return false;
    }
    if (this.rapportOnTransitoire != other.rapportOnTransitoire) {
      return false;
    }
    if (!Objects.equals(this.rapport, other.rapport)) {
      return false;
    }
    if (!Objects.equals(this.cheminCSV, other.cheminCSV)) {
      return false;
    }
    return true;
  }

  void initRapportDataFrom(OtfaCampagneItem cible) {
    if (cible == null) {
      return;
    }
    this.rapportOnTransitoire = cible.rapportOnTransitoire;
    this.rapport = cible.rapport;
    this.cheminCSV = cible.cheminCSV;
  }
}
