package org.fudaa.dodico.crue.projet.otfa;

import java.io.File;
import java.util.List;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.comparaison.ComparaisonSelectorContent;
import org.fudaa.dodico.crue.comparaison.ComparaisonSelectorEnum;
import org.fudaa.dodico.crue.comparaison.ComparaisonSelectorLoader;
import org.fudaa.dodico.crue.comparaison.ExecuteComparaison;
import org.fudaa.dodico.crue.comparaison.ExecuteComparaisonResult;
import org.fudaa.dodico.crue.comparaison.config.ConfComparaisonConteneur;
import org.fudaa.dodico.crue.comparaison.io.ReaderConfig;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.result.OrdResDynamicPropertyFilter;
import org.fudaa.dodico.crue.projet.ConfigurationReader;
import org.fudaa.dodico.crue.projet.conf.Configuration;
import org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager;
import org.fudaa.dodico.crue.projet.conf.Option;
import org.fudaa.dodico.crue.projet.conf.OptionsEnum;

/**
 *
 * Compare 2 scénarios.
 *
 * @author deniger
 *
 */
public class OtfaComparaisonExecutor {

  private ConfComparaisonConteneur comparaisonConteneur;
  private final File siteDir;
  ComparaisonSelectorContent selector;
  private int maxReadCRLine = -1;
  private int maxDiffByEMHOnResultat = -1;

  public OtfaComparaisonExecutor(File siteDir) {
    this.siteDir = siteDir;
  }

  private void buildComparison() {
    if (comparaisonConteneur != null) {
      return;
    }
    final ReaderConfig readerConfig = new ReaderConfig();
    comparaisonConteneur = readerConfig.read(ReaderConfig.class.getResource("default-comparaison.xml"));
  }

  public CtuluLogGroup preloadSelector() {
    buildComparison();
    ComparaisonSelectorLoader loader = new ComparaisonSelectorLoader(siteDir, comparaisonConteneur);
    CrueOperationResult<ComparaisonSelectorContent> read = loader.read();
    selector = read.getResult();

    ConfigurationReader reader = new ConfigurationReader(siteDir);
    CrueIOResu<Configuration> readConfigSite = reader.readConfigSite();
    if (readConfigSite.getAnalyse().isNotEmpty()) {
      read.getLogs().addLog(readConfigSite.getAnalyse());
    }
    Configuration metier = readConfigSite.getMetier();
    GlobalOptionsManager manager = new GlobalOptionsManager();
    manager.init(metier, null);
    Option option = manager.getOption(OptionsEnum.MAX_LOG_ITEM);
    if (option != null) {
      try {
        maxReadCRLine = Integer.parseInt(option.getValeur());
      } catch (NumberFormatException numberFormatException) {
      }
    }
    option = manager.getOption(OptionsEnum.MAX_RESULTAT_DIFF_BY_EMH);
    if (option != null) {
      try {
        maxDiffByEMHOnResultat = Integer.parseInt(option.getValeur());
      } catch (NumberFormatException numberFormatException) {
      }
    }



    return read.getLogs();

  }

  public List<ExecuteComparaisonResult> compare(CrueConfigMetier config, EMHScenario reference, EMHScenario cible, ComparaisonSelectorEnum enumSelector) {
    if (comparaisonConteneur == null) {
      throw new IllegalStateException("call preload before");
    }

    final OrdResDynamicPropertyFilter filter = new OrdResDynamicPropertyFilter(reference.getOrdResScenario(), cible
            .getOrdResScenario());
    final ExecuteComparaison comp = new ExecuteComparaison(comparaisonConteneur, config, filter, maxReadCRLine, maxDiffByEMHOnResultat);
    return comp.launch(reference, cible, selector.getPredicate(enumSelector));

  }
}
