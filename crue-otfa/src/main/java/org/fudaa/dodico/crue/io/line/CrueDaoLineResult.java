package org.fudaa.dodico.crue.io.line;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.line.CrueDaoStructureLineResult.LineResult;

/**
 * @author CANEL Christophe
 *
 */
public class CrueDaoLineResult extends AbstractCrueDao {

  public final List<LineResult> ComparaisonResultats = new ArrayList<>();
}
