package org.fudaa.dodico.crue.projet.otfa;

import java.util.List;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.comparaison.ExecuteComparaisonResult;

/**
 * Result for a line of OTFA.
 *
 * @author deniger
 *
 */
public class OtfaCampagneLineResult {

  private OtfaCampagneLineResultComparaisons comparisonResult = new OtfaCampagneLineResultComparaisons();
  private CtuluLogGroup logs;//contains all logs from read, run,...
  private OtfaCampagneLine initialLine;

  public OtfaCampagneLineResult() {
  }

  public OtfaCampagneLineResult(OtfaCampagneLine line) {
    this.initialLine = line;
  }

  public void initForm(OtfaCampagneLineResult other) {
    this.initialLine = other.initialLine;
    this.logs = other.logs;
    this.comparisonResult = other.comparisonResult;
  }

  public OtfaCampagneLineResultComparaisons getComparisonResult() {
    return comparisonResult;
  }

  /**
   * @return the comparisonResult
   */
  public List<ExecuteComparaisonResult> getComparisonResults() {
    return comparisonResult.getComparisonResult();
  }

  /**
   * @param comparisonResult the comparisonResult to set
   */
  public void setComparisonResult(List<ExecuteComparaisonResult> comparisonResult) {
    this.comparisonResult.setComparisonResult(comparisonResult);
  }

  /**
   * @return the logs
   */
  public CtuluLogGroup getLogs() {
    return logs;
  }

  /**
   * @param logs the logs to set
   */
  public void setLogs(CtuluLogGroup logs) {
    this.logs = logs;
  }

  /**
   * @return the initialLine
   */
  public OtfaCampagneLine getInitialLine() {
    return initialLine;
  }

  /**
   * @param initialLine the initialLine to set
   */
  public void setInitialLine(OtfaCampagneLine initialLine) {
    this.initialLine = initialLine;
  }
}
