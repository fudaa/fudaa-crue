/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.otfa;

import com.Ostermiller.util.CSVPrinter;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportViewLineInfoAndType;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Frederic Deniger
 */
public abstract class OtfaReportExecutorDelegate {
  protected final ReportConfigContrat config;
  final OtfaReportGlobalService reportGlobalService;
  protected final OtfaReportExecutor parent;
  private final ReportViewLineInfoAndType reportInfo;
  final char delimiter = ';';
  final char quote = '"';

  public OtfaReportExecutorDelegate(OtfaReportExecutor parent, ReportViewLineInfoAndType reportInfo, ReportConfigContrat config,
                                    OtfaReportGlobalService reportGlobalService) {
    this.config = config;
    this.reportInfo = reportInfo;
    this.parent = parent;
    this.reportGlobalService = reportGlobalService;
  }

  void printVariable(CSVPrinter print, Double value, ReportRunVariableKey prop) {
    if (ReportVariableTypeEnum.LIMIT_PROFIL.equals(prop.getVariable().getVariableType())) {
      printVariable(print, value, CrueConfigMetierConstants.PROP_Z);
    } else {
      printVariable(print, value, prop.getVariableName());
    }
  }

  public Collection<ResultatTimeKey> getSelectedTimeStepInReport() {
    return config.getSelectedTimeStepInReport();
  }

  void printVariable(CSVPrinter print, Double value, String prop) {
    if (value == null) {
      print.print(StringUtils.EMPTY);
    } else {
      final PropertyNature nature = reportGlobalService.getResultService().getPropertyNature(prop);
      if (nature == null) {
        print.print(value.toString());
      } else {
        print.print(nature.format(value, DecimalFormatEpsilonEnum.COMPARISON));
      }
    }
  }

  boolean containsCurrent(List<ReportRunKey> keys) {
    for (ReportRunKey reportRunKey : keys) {
      if (reportRunKey.isCourant()) {
        return true;
      }
    }
    return false;
  }

  boolean containsAlternatif(List<ReportRunKey> keys) {
    for (ReportRunKey reportRunKey : keys) {
      if (!reportRunKey.isCourant()) {
        return true;
      }
    }
    return false;
  }

  void exportTo(File targetFile, CtuluLog log) {
    log.addInfo("otfa.report.exportTo", reportInfo.getInfo().getNom(), targetFile.getAbsolutePath());
    try (FileOutputStream out = new FileOutputStream(targetFile)) {
      CSVPrinter writer = new CSVPrinter(out);
      writer.changeQuote(quote);
      writer.changeDelimiter(delimiter);

      writer.writelnComment(
          BusinessMessages.getString("ExportFichierEtu") + ": " + parent.getProjet().getInfos().getEtuFile().getAbsolutePath());
      writer.writelnComment(BusinessMessages.getString("ExportScenario") + ": " + parent.getManagerScenario().getNom());
      writer.writelnComment(BusinessMessages.getString("ExportRun") + ": " + parent.getManagerScenario().getRunCourant().getNom());
      writer.writelnComment(
          BusinessMessages.getString("ExportReport") + ": " + config.getType().geti18n() + " (" + reportInfo.getInfo().getNom() + ")");

      exportTo(writer, log);
      writer.flush();
    } catch (Exception e) {
      log.addSevereError(e.getMessage());
      Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getMessage(), e);
    }
    log.printResume();
  }

  protected abstract void exportTo(CSVPrinter printer, CtuluLog log) throws IOException;
}
