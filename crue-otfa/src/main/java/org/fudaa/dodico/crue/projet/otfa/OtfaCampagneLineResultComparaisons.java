package org.fudaa.dodico.crue.projet.otfa;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.comparaison.ExecuteComparaisonResult;

/**
 * Result for a line of OTFA.
 *
 * @author deniger
 *
 */
public class OtfaCampagneLineResultComparaisons {

  private List<ExecuteComparaisonResult> comparisonResult = new ArrayList<>();

  public OtfaCampagneLineResultComparaisons() {
  }

  public void initForm(OtfaCampagneLineResultComparaisons other) {
    this.comparisonResult = other.comparisonResult;
  }

  /**
   * @return the comparisonResult
   */
  public List<ExecuteComparaisonResult> getComparisonResult() {
    return comparisonResult;
  }

  /**
   * @param comparisonResult the comparisonResult to set
   */
  public void setComparisonResult(List<ExecuteComparaisonResult> comparisonResult) {
    this.comparisonResult = comparisonResult;
  }
}
