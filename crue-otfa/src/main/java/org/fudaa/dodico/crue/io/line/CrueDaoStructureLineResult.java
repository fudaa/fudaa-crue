/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.line;

import com.thoughtworks.xstream.XStream;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.loi.LoiTypeContainer;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureLog;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;

/**
 *
 * @author Frederic Deniger
 */
public class CrueDaoStructureLineResult implements CrueDataDaoStructure {

  @Override
  public void configureXStream(XStream xstream, CtuluLog ctuluLog, LoiTypeContainer props) {
    xstream.alias("RTFA-Comparaisons", CrueDaoLineResult.class);
    xstream.alias("ComparaisonLineResultats", LineResult.class);
    xstream.alias("Comparaison", Comparaison.class);
    new CrueDaoStructureLog().configureXStream(xstream, ctuluLog, props);
  }

  public static class Comparaison {

    public String PropertyTested;
    public String Message;
    public List<String> Arguments = new ArrayList<>();
    public boolean Same;
    public boolean ComparisonStopped;
    public int NbDifferences;
    public String A;
    public String B;
    public final List<Comparaison> SubComparaisons = new ArrayList<>();
  }

  public static class LineResult {

    public String Id;
    public String Message;
    public int NbDifferences;
    public boolean DiffOnOrder;
    public CrueDaoStructureLineResult.Comparaison Comparaisons;
    public CrueDaoStructureLog.Logger ResultatLog;
  }
}
