/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.rtfa;

import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;

/**
 *
 * @author Frederic Deniger
 */
public class RTFAResultLine {

  private OtfaCampagneLine initialLine;
  private int nbDifferences = -1;
  private int nbSevereError;
  private int nbError;
  private int nbWarn;
  private int nbInfo;

  public RTFAResultLine() {
  }

  public RTFAResultLine(OtfaCampagneLine initialLine) {
    this.initialLine = initialLine;
  }

  public OtfaCampagneLine getInitialLine() {
    return initialLine;
  }

  public void setInitialLine(OtfaCampagneLine initialLine) {
    this.initialLine = initialLine;
  }

  public int getNbDifferences() {
    return nbDifferences;
  }

  public void setNbDifferences(int nbDifferences) {
    this.nbDifferences = nbDifferences;
  }

  public int getNbSevereError() {
    return nbSevereError;
  }

  public boolean isSameCount(RTFAResultLine other) {
    if (other == null) {
      return false;
    }
    return nbDifferences == other.nbDifferences
            && nbSevereError == other.nbSevereError
            && nbError == other.nbError
            && nbWarn == other.nbWarn
            && nbInfo == other.nbInfo;
  }

  public void setNbSevereError(int nbSevereError) {
    this.nbSevereError = nbSevereError;
  }

  public int getNbError() {
    return nbError;
  }

  public void setNbError(int nbError) {
    this.nbError = nbError;
  }

  public boolean containsErrorOrSevereError() {
    return nbError > 0 || nbSevereError > 0;
  }

  public boolean containsWarning() {
    return nbWarn > 0;
  }

  public int getNbWarn() {
    return nbWarn;
  }

  public void setNbWarn(int nbWarn) {
    this.nbWarn = nbWarn;
  }

  public int getNbInfo() {
    return nbInfo;
  }

  public void setNbInfo(int nbInfo) {
    this.nbInfo = nbInfo;
  }

  public boolean hasLog() {
    return nbSevereError > 0 || nbError > 0 || nbWarn > 0 || nbInfo > 0;
  }

  public boolean hasDifferences() {
    return nbDifferences > 0;
  }

  public void initForm(RTFAResultLine result) {
    if (result == null) {
      return;
    }
    initialLine = new OtfaCampagneLine(result.getInitialLine());
    nbDifferences = result.nbDifferences;
    nbSevereError = result.nbSevereError;
    nbError = result.nbError;
    nbWarn = result.nbWarn;
    nbInfo = result.nbInfo;
  }

  public int getNbLogs() {
    return nbSevereError + nbError + nbWarn + nbInfo;
  }

  public boolean hasComparaison() {
    return nbDifferences >= 0;
  }
}
