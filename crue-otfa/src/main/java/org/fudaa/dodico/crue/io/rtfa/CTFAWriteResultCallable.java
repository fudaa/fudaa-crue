/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.rtfa;

import java.io.File;
import java.util.concurrent.Callable;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.line.CrueLineLogReaderWriter;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLineResult;

/**
 *
 * @author Frederic Deniger
 */
public class CTFAWriteResultCallable implements Callable<CtuluLog> {

  private final OtfaCampagneLineResult launch;
  private final File targetDir;

  public CTFAWriteResultCallable(OtfaCampagneLineResult launch, File targetDir) {
    this.launch = launch;
    this.targetDir = targetDir;
  }

  @Override
  public CtuluLog call() throws Exception {
    final CrueLineLogReaderWriter writer = new CrueLineLogReaderWriter(CrueRTFAReaderWriter.LAST_VERSION);
    final CrueIOResu<CtuluLogGroup> resu = new CrueIOResu<>(launch.getLogs() == null ? null : launch.getLogs().createCleanGroup());
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    File target = new File(targetDir, RTFAResultLines.getCtfaFileName(launch.getInitialLine()));
    writer.writeXMLMetier(resu, target, log, null);
    return log;
  }
}
