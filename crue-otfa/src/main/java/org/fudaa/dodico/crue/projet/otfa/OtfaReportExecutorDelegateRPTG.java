/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.otfa;

import com.Ostermiller.util.CSVPrinter;
import java.io.IOException;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.emh.ResPrtGeo;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.dodico.crue.projet.report.ReportRPTGConfig;
import org.fudaa.dodico.crue.projet.report.ReportViewLineInfoAndType;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;

/**
 *
 * @author Frederic Deniger
 */
public class OtfaReportExecutorDelegateRPTG extends OtfaReportExecutorDelegate {

  public static LoiFF findLoi(List<LoiFF> lois, String varX, String varY, CrueConfigMetier ccm) {
    for (LoiFF loiFF : lois) {
      ConfigLoi config = ccm.getConfLoi().get(loiFF.getType());
      if (config.getVarAbscisse().getNom().equals(varX) && config.getVarOrdonnee().getNom().equals(varY)) {
        return loiFF;
      } else if (config.getVarAbscisse().getNom().equals(varY) && config.getVarOrdonnee().getNom().equals(varX)) {
        return LoiHelper.switchAbscOrdonnee(loiFF);
      }
    }
    return null;
  }

  public OtfaReportExecutorDelegateRPTG(OtfaReportExecutor parent, ReportViewLineInfoAndType reportInfo, ReportConfigContrat config,
          OtfaReportGlobalService reportGlobalService) {
    super(parent, reportInfo, config, reportGlobalService);
  }

  @Override
  protected void exportTo(CSVPrinter printer, CtuluLog log) throws IOException {
    ReportRPTGConfig content = (ReportRPTGConfig) super.config;
    EMH emh = reportGlobalService.getResultService().getRunCourant().getEMH(content.getEmh());
    if (emh == null) {
      log.addError("otfa.report.emhNotFound", content.getEmh());
      return;
    }
    ResPrtGeo rptg = emh.getRPTG();
    if (rptg == null) {
      log.addError("otfa.report.emhNoRPTG", content.getEmh());
      return;
    }
    printer.writelnComment(BusinessMessages.getString("otfa.report.exportRPTG.on") + ": " + emh.getNom());
    List<LoiFF> lois = rptg.getLois();
    final String var1 = content.getVar1();
    final String var2 = content.getVar2();
    final String varH = content.getVarHorizontal();
    LoiFF lois1 = null;
    LoiFF lois2 = null;
    if (var1 != null) {
      lois1 = findLoi(lois, varH, var1, reportGlobalService.getResultService().getCcm());
    }
    if (var2 != null) {
      lois2 = findLoi(lois, varH, var2, reportGlobalService.getResultService().getCcm());
    }
    int max1 = 0;
    int max2 = 0;
    StringBuilder builder = new StringBuilder();
    if (lois1 != null) {
      max1 = lois1.getNombrePoint();
      builder.append(super.quote).append(reportGlobalService.getResultService().getVariableName(varH)).append(super.quote);
      builder.append(super.delimiter).append(super.quote).append(reportGlobalService.getResultService().getVariableName(var1)).append(super.quote);
    }
    if (lois2 != null) {
      max2 = lois2.getNombrePoint();
      if (builder.length() > 0) {
        builder.append(super.delimiter);
      }
      builder.append(super.quote).append(reportGlobalService.getResultService().getVariableName(varH)).append(super.quote);
      builder.append(super.delimiter).append(super.quote).append(reportGlobalService.getResultService().getVariableName(var2)).append(super.quote);
    }
    int maxSize = Math.max(max1, max2);
    printer.printlnComment(builder.toString());
    for (int i = 0; i < maxSize; i++) {
      if (lois1 != null) {
        if (i >= max1) {
          printer.print(StringUtils.EMPTY);
          printer.print(StringUtils.EMPTY);
        }
        printVariable(printer, lois1.getAbscisse(i), varH);
        printVariable(printer, lois1.getOrdonnee(i), var1);
      }
      if (lois2 != null) {
        if (i >= max2) {
          printer.print(StringUtils.EMPTY);
          printer.print(StringUtils.EMPTY);
        }
        printVariable(printer, lois2.getAbscisse(i), varH);
        printVariable(printer, lois2.getOrdonnee(i), var2);
      }
      printer.println();
    }

  }

}
