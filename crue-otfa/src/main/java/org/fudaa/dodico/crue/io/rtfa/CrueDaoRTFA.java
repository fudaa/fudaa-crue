package org.fudaa.dodico.crue.io.rtfa;

import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureLog;
import org.fudaa.dodico.crue.io.rtfa.CrueDaoStructureRTFA.LigneRTFA;

/**
 * @author CANEL Christophe
 *
 */
public class CrueDaoRTFA extends AbstractCrueDao {

  public CrueDaoStructureLog.Header Header;
  public List<LigneRTFA> Lignes;
}
