/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.otfa;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;
import org.fudaa.dodico.crue.projet.report.formule.function.AggregationCacheKey;
import org.netbeans.api.progress.ProgressRunnable;

/**
 *
 * @author Frederic Deniger
 */
public class OtfaReportGlobalService implements ReportGlobalServiceContrat {

  private final OtfaReportResultService resultService;
  private final Map<AggregationCacheKey, Double> cache = new HashMap<>();
  private List<ResultatTimeKey> rangeSelectedTimeKeys = Collections.emptyList();
  private List<ResultatTimeKey> permanentTimeKeys = Collections.emptyList();
  private List<ResultatTimeKey> transitoireTimeKeys = Collections.emptyList();

  public OtfaReportGlobalService(OtfaReportResultService resultService) {
    this.resultService = resultService;
  }

  @Override
  public OtfaReportResultService getResultService() {
    return resultService;
  }

  @Override
  public Double getCachedValue(AggregationCacheKey cacheKey) {
    if (cacheKey.getStatKey().getTimeSelection().isCachable()) {
      return cache.get(cacheKey);
    }
    return null;
  }

  @Override
  public void putCachedValue(AggregationCacheKey cacheKey, Double resValue) {
    if (cacheKey.getStatKey().getTimeSelection().isCachable()) {
      cache.put(cacheKey, resValue);
    }
  }

  public List<ResultatTimeKey> getTimeKeysToUse() {
    return rangeSelectedTimeKeys;
  }

  @Override
  public List<ResultatTimeKey> getAllPermanentTimeKeys() {
    return permanentTimeKeys;
  }

  @Override
  public List<ResultatTimeKey> getAllTransientTimeKeys() {
    return transitoireTimeKeys;
  }

  public void setRangeSelectedTimeKeys(List<ResultatTimeKey> rangeSelectedTimeKeys) {
    this.rangeSelectedTimeKeys = rangeSelectedTimeKeys;
  }

  public void setPermanentTimeKeys(List<ResultatTimeKey> permanentTimeKeys) {
    this.permanentTimeKeys = permanentTimeKeys;
  }

  public void setTransitoireTimeKeys(List<ResultatTimeKey> transitoireTimeKeys) {
    this.transitoireTimeKeys = transitoireTimeKeys;
  }

  @Override
  public <T> T showProgressDialogAndRun(ProgressRunnable<T> operation, String displayName) {
    return operation.run(null);
  }

}
