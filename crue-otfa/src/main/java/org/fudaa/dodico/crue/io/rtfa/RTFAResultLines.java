/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.rtfa;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneResultHeader;

/**
 *
 * @author Frederic Deniger
 */
public class RTFAResultLines {

  private OtfaCampagneResultHeader header = new OtfaCampagneResultHeader();
  private boolean batch;
  private final List<RTFAResultLine> lines = new ArrayList<>();

  /**
   * @return the globalValidation
   */
  public CtuluLogGroup getGlobalValidation() {
    return header.getGlobalValidation();
  }

  public static String getRtfaFileName(OtfaCampagneLine line) {
    return line.getIndice() + ".rtfa-line.xml";
  }

  public static String getCtfaFileName(OtfaCampagneLine line) {
    return line.getIndice() + ".ctfa-line.xml";
  }

  public List<RTFAResultLine> getLines() {
    return Collections.unmodifiableList(lines);
  }

  public void addLines(Collection<RTFAResultLine> lines) {
    if (lines != null) {
      this.lines.addAll(lines);
    }
  }

  public void addLine(RTFAResultLine line) {
    if (line != null) {
      this.lines.add(line);
    }
  }

  public boolean isBatch() {
    return batch;
  }

  public void setBatch(boolean batch) {
    this.batch = batch;
  }

  public OtfaCampagneResultHeader getHeader() {
    return header;
  }

  /**
   * @param globalValidation the globalValidation to set
   */
  public void setGlobalValidation(CtuluLogGroup globalValidation) {
    this.header.setGlobalValidation(globalValidation);
  }

  public void setHeader(OtfaCampagneResultHeader header) {
    this.header = header;
  }
}
