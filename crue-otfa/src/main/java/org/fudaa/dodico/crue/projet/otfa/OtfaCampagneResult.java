package org.fudaa.dodico.crue.projet.otfa;

import java.util.List;
import org.fudaa.ctulu.CtuluLogGroup;
import org.joda.time.LocalDateTime;

/**
 * Container d'une opération OTFA
 *
 * @author deniger
 */
public class OtfaCampagneResult {

  private OtfaCampagneResultHeader header = new OtfaCampagneResultHeader();
  
  boolean batch;
  private List<OtfaCampagneLineResult> results;

  public boolean isBatch() {
    return batch;
  }

  public void setBatch(boolean batch) {
    this.batch = batch;
  }
  
  

  /**
   * @return the commentaire
   */
  public String getCommentaire() {
    return header.getCommentaire();
  }

  /**
   * @return the date
   */
  public LocalDateTime getDate() {
    return header.getDate();
  }

  /**
   * @return the globalValidation
   */
  public CtuluLogGroup getGlobalValidation() {
    return header.getGlobalValidation();
  }

  public OtfaCampagneResultHeader getHeader() {
    return header;
  }

  /**
   * @return the results
   */
  public List<OtfaCampagneLineResult> getResults() {
    return results;
  }

  /**
   * @param commentaire the commentaire to set
   */
  public void setCommentaire(String commentaire) {
    this.header.setCommentaire(commentaire);
  }

  /**
   * @param date the date to set
   */
  public void setDate(LocalDateTime date) {
    this.header.setDate(date);
  }

  /**
   * @param globalValidation the globalValidation to set
   */
  public void setGlobalValidation(CtuluLogGroup globalValidation) {
    this.header.setGlobalValidation(globalValidation);
  }

  public void setHeader(OtfaCampagneResultHeader header) {
    this.header = header;
  }

  /**
   * @param results the results to set
   */
  public void setResults(List<OtfaCampagneLineResult> results) {
    this.results = results;
  }
}
