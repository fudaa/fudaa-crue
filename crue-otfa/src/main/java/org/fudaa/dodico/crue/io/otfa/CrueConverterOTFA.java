package org.fudaa.dodico.crue.io.otfa;

import java.io.File;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.io.common.CrueConverterCommonParameter;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;

/**
 * @author CANEL Christophe
 */
public class CrueConverterOTFA implements CrueDataConverter<CrueDaoOTFA, OtfaCampagne> {

  private final File otfaFile;

  public CrueConverterOTFA(final File otfaFile) {
    this.otfaFile = otfaFile;
  }

  public CrueConverterOTFA() {
    this(null);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public OtfaCampagne convertDaoToMetier(final CrueDaoOTFA dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
    final OtfaCampagne metier = new OtfaCampagne();

    metier.setCommentaire(dao.getCommentaire());
    metier.setAuteurCreation(dao.AuteurCreation);
    metier.setDateCreation(DateDurationConverter.getDate(dao.DateCreation));
    metier.setAuteurModification(dao.AuteurDerniereModif);
    if (dao.DateDerniereModif != null) {
      metier.setDateModification(DateDurationConverter.getDate(dao.DateDerniereModif));
    }
    metier.setReferenceOptions(CrueConverterCommonParameter.convertRunOptionsDaoToMetier(dao.ReferenceOptions));
    metier.setCibleOptions(CrueConverterCommonParameter.convertRunOptionsDaoToMetier(dao.CibleOptions));
    metier.setLines(CrueConverterCommonParameter.convertCampagneLinesDaoToMetier(dao.LignesCampagne));
    metier.setOtfaFile(otfaFile);
    return metier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public OtfaCampagne getConverterData(final CrueData in) {
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CrueDaoOTFA convertMetierToDao(final OtfaCampagne metier, final CtuluLog ctuluLog) {
    final CrueDaoOTFA dao = new CrueDaoOTFA();

    dao.setCommentaire(metier.getCommentaire());
    dao.AuteurCreation = metier.getAuteurCreation();
    if (metier.getDateCreation() != null) {
      dao.DateCreation = DateDurationConverter.dateToXsd(metier.getDateCreation());
    }
    dao.AuteurDerniereModif = metier.getAuteurModification();
    if (metier.getDateModification() != null) {
      dao.DateDerniereModif = DateDurationConverter.dateToXsd(metier.getDateModification());
    }
    dao.ReferenceOptions = CrueConverterCommonParameter.convertRunOptionsMetierToDao(metier.getReferenceOptions());
    dao.CibleOptions = CrueConverterCommonParameter.convertRunOptionsMetierToDao(metier.getCibleOptions());
    dao.LignesCampagne = CrueConverterCommonParameter.convertCampagneLinesMetierToDao(metier.getLines());

    return dao;
  }
}
