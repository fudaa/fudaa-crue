package org.fudaa.dodico.crue.io.line;

import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureLog.LoggerGroup;

/**
 * @author CANEL Christophe
 *
 */
public class CrueDaoLineLog extends AbstractCrueDao {

  public LoggerGroup Logs;
}
