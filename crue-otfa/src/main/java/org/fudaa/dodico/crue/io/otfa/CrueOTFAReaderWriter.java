package org.fudaa.dodico.crue.io.otfa;

import java.io.File;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;

/**
 * @author CANEL Christophe
 *
 */
public class CrueOTFAReaderWriter extends CrueDataXmlReaderWriterImpl<CrueDaoOTFA, OtfaCampagne> {
  public CrueOTFAReaderWriter(String version)
  {
    this(version, null);
  }
/**
   * 
   * @param version la version attendu
   * @param otfaFile on l'utilise pour l'initialiser dans OtfaCampagne.
   */
  public CrueOTFAReaderWriter(String version, File otfaFile)
  {
    super("otfa", version, new CrueConverterOTFA(otfaFile), new CrueDaoStructureOTFA());
  }
}
