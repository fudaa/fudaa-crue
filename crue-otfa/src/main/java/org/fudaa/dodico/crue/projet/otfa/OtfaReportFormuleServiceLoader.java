/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.otfa;

import java.io.File;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;
import org.fudaa.dodico.crue.projet.report.formule.FormuleContent;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDependenciesContent;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDependenciesProcessor;
import org.fudaa.dodico.crue.projet.report.formule.FormuleReader;
import org.fudaa.dodico.crue.projet.report.formule.FormuleServiceContent;

/**
 *
 * @author Frederic Deniger
 */
public class OtfaReportFormuleServiceLoader {

  private final EMHProjet project;

  public OtfaReportFormuleServiceLoader(EMHProjet project) {
    this.project = project;
  }

  public CrueIOResu<OtfaReportFormuleService> loadFormuleService(ReportGlobalServiceContrat service) {
    final CrueIOResu<FormuleServiceContent> loadContent = loadContent(service);
    OtfaReportFormuleService formuleService = new OtfaReportFormuleService(loadContent.getMetier());
    CtuluLog log = loadContent.getAnalyse();
    if (log == null) {
      log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    }
    detectCycle(formuleService, log);
    return new CrueIOResu<>(formuleService, log);

  }

  public CrueIOResu<FormuleServiceContent> loadContent(ReportGlobalServiceContrat service) {
    FormuleReader saver = new FormuleReader();
    final File targetFile = getTargetFile();
    if (targetFile == null || !targetFile.exists()) {
      return new CrueIOResu<>(new FormuleServiceContent());
    }
    CrueIOResu<FormuleServiceContent> read = saver.readContent(targetFile, service);
    return read;

  }

  private File getTargetFile() {
    final File dirOfRapports = project.getInfos().getDirOfRapports();
    if (dirOfRapports == null) {
      return null;
    }
    File target = new File(dirOfRapports, "VariablesFC.xml");
    return target;
  }

  public void detectCycle(OtfaReportFormuleService formuleService, CtuluLog log) {
    FormuleDependenciesProcessor processor = new FormuleDependenciesProcessor(formuleService);
    FormuleDependenciesContent compute = processor.compute(formuleService.getServiceContent().getVariablesKeys());
    List<String> cycliqueVar = compute.getCycliqueVar();
    if (!cycliqueVar.isEmpty()) {
      List<FormuleContent> contents = formuleService.getContents();
      if (contents != null) {
        for (FormuleContent formuleContent : contents) {
          if (cycliqueVar.contains(formuleContent.getId())) {
            formuleContent.setCyclique(true);
          }
        }
      }
      for (String var : cycliqueVar) {
        log.addSevereError("cycleDetected.Error", var);
      }

    }
  }

}
