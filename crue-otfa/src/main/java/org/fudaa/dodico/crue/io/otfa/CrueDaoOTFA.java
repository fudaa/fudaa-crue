package org.fudaa.dodico.crue.io.otfa;

import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureOptions.LigneCampagne;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureOptions.RunOptions;

/**
 * @author CANEL Christophe
 *
 */
public class CrueDaoOTFA extends AbstractCrueDao {
  public String AuteurCreation;
  public String DateCreation;
  public String AuteurDerniereModif;
  public String DateDerniereModif;
  public RunOptions ReferenceOptions;
  public RunOptions CibleOptions;
  public List<LigneCampagne> LignesCampagne;
}
