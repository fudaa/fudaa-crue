package org.fudaa.dodico.crue.io.line;

import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLineResultComparaisons;

/**
 * @author CANEL Christophe
 *
 */
public class CrueLineResultReaderWriter extends CrueDataXmlReaderWriterImpl<CrueDaoLineResult, OtfaCampagneLineResultComparaisons> {

  public CrueLineResultReaderWriter(final String version) {
    super("rtfa-line", version, new CrueConverterLineResult(), new CrueDaoStructureLineResult());
  }
}
