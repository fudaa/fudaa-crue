package org.fudaa.dodico.crue.io.line;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;

/**
 * @author CANEL Christophe
 *
 */
public class CrueLineLogReaderWriter extends CrueDataXmlReaderWriterImpl<CrueDaoLineLog, CtuluLogGroup> {

  public CrueLineLogReaderWriter(final String version) {
    super("ctfa-line", version, new CrueConverterLineLog(), new CrueDaoStructureLineLog());
  }
}
