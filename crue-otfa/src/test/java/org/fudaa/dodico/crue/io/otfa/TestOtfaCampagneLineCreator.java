/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.otfa;

import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneItem;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;

/**
 *
 * @author Frederic Deniger
 */
public class TestOtfaCampagneLineCreator {

  public static OtfaCampagneLine createCampagneLine1() {
    final OtfaCampagneLine line = new OtfaCampagneLine();
    line.setIndice(1);
    line.setCommentaire("Line 1");
    line.setReference(createCampagneItemRef());
    line.setCible(createCampagneItemCible());
    return line;
  }

  public static OtfaCampagneLine createCampagneLine2() {
    final OtfaCampagneLine line = new OtfaCampagneLine();
    line.setIndice(2);
    line.setCommentaire("Line 2");
    line.setReference(createCampagneItemRef());
    line.setCible(createCampagneItemCible());
    return line;
  }

  public static OtfaCampagneItem createCampagneItemRef() {
    final OtfaCampagneItem ref = new OtfaCampagneItem();
    ref.setEtuPath("refFile1.etu.xml");
    ref.setScenarioNom("refScenario1");
    ref.setCoeurName("refCoeur1");
    ref.setLaunchCompute(true);
    return ref;
  }

  public static OtfaCampagneItem createCampagneItemCible() {
    final OtfaCampagneItem cible = new OtfaCampagneItem();
    cible.setEtuPath("cibleFile1.etu.xml");
    cible.setScenarioNom("cibleScenario1");
    cible.setCoeurName("cibleCoeur1");
    cible.setLaunchCompute(false);
    return cible;
  }
}
