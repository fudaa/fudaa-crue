/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.otfa;

import java.util.List;
import junit.framework.Assert;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;

/**
 *
 * @author Frederic Deniger
 */
public class TestLogCreator {

  public static CtuluLog createMainLog() {
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("Main log");
    log.addError("Main log error 1.");
    log.addError("Main log error 2.");
    return log;
  }

  public static CtuluLog createLog2() {
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("Log 2");
    log.addError("Log2 error 1.");
    log.addError("Log2 error 2.");
    return log;
  }

  public static CtuluLogGroup createLogGroupSimple() {
    final CtuluLogGroup group = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    group.getLogs().clear();
    group.addLog(createMainLog());
    group.addLog(createLog1());
    group.addLog(createLog2());
    return group;
  }

  public static CtuluLogGroup createLogGroup() {
    final CtuluLogGroup group = createLogGroupSimple();
    group.addGroup(createLogGroupSimple());
    group.addGroup(createLogGroupSimple());
    return group;
  }

  public static CtuluLog createLog1() {
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("Log 1");
    log.addError("Log1 error 1.");
    log.addError("Log1 error 2.");
    return log;
  }

  public static void assertCorrectLogGroupSimple(CtuluLogGroup group) {
    final List<CtuluLog> logs = group.getLogs();
    Assert.assertEquals(3, logs.size());
    assertCorrectMainLog(group.getLogs().get(0));
    assertCorrectLog1(logs.get(1));
    assertCorrectLog2(logs.get(2));
  }

  public static void assertCorrectLogGroup(CtuluLogGroup group) {
    final List<CtuluLogGroup> groups = group.getGroups();
    assertCorrectLogGroupSimple(group);
    Assert.assertEquals(2, groups.size());
    assertCorrectLogGroupSimple(groups.get(0));
    assertCorrectLogGroupSimple(groups.get(1));
  }

  public static void assertCorrectMainLog(CtuluLog log) {
    final CtuluLogRecord[] logs = log.getRecords().toArray(new CtuluLogRecord[0]);
    Assert.assertEquals("Main log", log.getDesc());
    Assert.assertEquals(2, logs.length);
    Assert.assertEquals(CtuluLogLevel.ERROR, logs[0].getLevel());
    Assert.assertEquals("Main log error 1.", logs[0].getMsg());
    Assert.assertEquals(CtuluLogLevel.ERROR, logs[1].getLevel());
    Assert.assertEquals("Main log error 2.", logs[1].getMsg());
  }

  public static void assertCorrectLog1(CtuluLog log) {
    final CtuluLogRecord[] logs = log.getRecords().toArray(new CtuluLogRecord[0]);
    Assert.assertEquals("Log 1", log.getDesc());
    Assert.assertEquals(2, logs.length);
    Assert.assertEquals(CtuluLogLevel.ERROR, logs[0].getLevel());
    Assert.assertEquals("Log1 error 1.", logs[0].getMsg());
    Assert.assertEquals(CtuluLogLevel.ERROR, logs[1].getLevel());
    Assert.assertEquals("Log1 error 2.", logs[1].getMsg());
  }

  public static void assertCorrectLog2(CtuluLog log) {
    final CtuluLogRecord[] logs = log.getRecords().toArray(new CtuluLogRecord[0]);
    Assert.assertEquals("Log 2", log.getDesc());
    Assert.assertEquals(2, logs.length);
    Assert.assertEquals(CtuluLogLevel.ERROR, logs[0].getLevel());
    Assert.assertEquals("Log2 error 1.", logs[0].getMsg());
    Assert.assertEquals(CtuluLogLevel.ERROR, logs[1].getLevel());
    Assert.assertEquals("Log2 error 2.", logs[1].getMsg());
  }
}
