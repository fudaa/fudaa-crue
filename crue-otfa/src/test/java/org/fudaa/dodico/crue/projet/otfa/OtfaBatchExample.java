/*
 * GPL v2
 */
package org.fudaa.dodico.crue.projet.otfa;

import org.fudaa.dodico.crue.batch.BatchHelper;

/**
 *
 * @author deniger
 */
public class OtfaBatchExample {

  public static void main(String[] args) {
    System.setProperty(BatchHelper.SITE_CONFIG_PROP, "C:\\data\\Fudaa-Crue\\etc");
    OtfaBatch.main(new String[]{"C:\\data\\Fudaa-Crue\\Data\\otfa-conversion.otfa.xml"});
  }
}
