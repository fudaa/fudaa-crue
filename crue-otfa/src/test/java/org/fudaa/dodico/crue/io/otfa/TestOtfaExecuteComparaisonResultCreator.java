/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.otfa;

import java.util.ArrayList;
import java.util.List;
import static junit.framework.Assert.assertEquals;
import org.fudaa.dodico.crue.comparaison.ExecuteComparaisonResult;
import org.fudaa.dodico.crue.comparaison.tester.ResultatTest;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLineResultComparaisons;

/**
 *
 * @author Frederic Deniger
 */
public class TestOtfaExecuteComparaisonResultCreator {

  public static OtfaCampagneLineResultComparaisons createComparaisonResult() {
    OtfaCampagneLineResultComparaisons res = new OtfaCampagneLineResultComparaisons();
    List<ExecuteComparaisonResult> results = new ArrayList<>();
    results.add(createComparaisonResult1());
    results.add(createComparaisonResult2());
    res.setComparisonResult(results);
    return res;
  }

  public static void assertCorrect(OtfaCampagneLineResultComparaisons result) {
    assertEquals(2, result.getComparisonResult().size());
    assertCorrectCompResult1(result.getComparisonResult().get(0));
    assertCorrectCompResult2(result.getComparisonResult().get(1));

  }

  public static void assertCorrectCompResult1(ExecuteComparaisonResult result) {
    assertEquals("Comp1", result.getId());
    assertEquals("Comparaison 1", result.getMsg());
    ResultatTest res = result.getRes();
    assertEquals("ref1", res.getPrintA());
    assertEquals("cible1", res.getPrintB());
    assertEquals("msg1", res.getMsg());
    TestLogCreator.assertCorrectMainLog(result.getLog());
  }

  public static void assertCorrectCompResult2(ExecuteComparaisonResult result) {
    assertEquals("Comp2", result.getId());
    assertEquals("Comparaison 2", result.getMsg());
    ResultatTest res = result.getRes();
    assertEquals("ref2", res.getPrintA());
    assertEquals("cible2", res.getPrintB());
    assertEquals("msg2", res.getMsg());
    TestLogCreator.assertCorrectMainLog(result.getLog());
  }

  public static ExecuteComparaisonResult createComparaisonResult2() {
    ExecuteComparaisonResult result = new ExecuteComparaisonResult(null);

    result.setId("Comp2");
    result.setMsg("Comparaison 2");
    result.setLog(TestLogCreator.createMainLog());
    ResultatTest resTest = new ResultatTest("ref2", "cible2", "msg2");
    resTest.setPrintA("ref2");
    resTest.setPrintB("cible2");
    result.setRes(resTest);
    return result;
  }

  public static ExecuteComparaisonResult createComparaisonResult1() {
    ExecuteComparaisonResult result = new ExecuteComparaisonResult(null);

    result.setId("Comp1");
    result.setMsg("Comparaison 1");
    result.setLog(TestLogCreator.createMainLog());
    ResultatTest resTest = new ResultatTest("ref1", "cible1", "msg1");
    resTest.setPrintA("ref1");
    resTest.setPrintB("cible1");
    result.setRes(resTest);
    return result;
  }
}
