/*
 * GPL v2
 */
package org.fudaa.dodico.crue.io.otfa;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.AbstractIOParentTest;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneItem;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;
import org.joda.time.LocalDateTime;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;

import static org.junit.Assert.*;

/**
 * @author CANEL Christophe
 */
public class TestCrueOTFA extends AbstractIOParentTest {
    public static final String FICHIER_TEST_XML = "/otfa/campagne.otfa.xml";
    private File testFile;

    public TestCrueOTFA() {
        super(FICHIER_TEST_XML);
    }

    @After
    public void deleteFile() {
        if (testFile != null && testFile.exists()) {
            testFile.delete();
        }
    }

    @Test
    public void testLecture() {
        final File otfaFile = this.getTestFile();
        final OtfaCampagne campagne = read(otfaFile);

        assertOtfaCampagne(campagne, otfaFile.getParentFile());
    }

    @Test
    public void testEcriture() {
        final File otfaFile = this.getTestFile();
        final File newOtfaFile = new File(otfaFile.getParentFile(), "campagne.new.otfa.xml");

        OtfaCampagne campagne = read(otfaFile);

        final CrueOTFAReaderWriter writer = new CrueOTFAReaderWriter("1.2");
        final CrueIOResu<OtfaCampagne> resu = new CrueIOResu<>(campagne);
        final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);

        assertTrue(writer.writeXMLMetier(resu, newOtfaFile, log, CrueConfigMetierForTest.DEFAULT));

        testAnalyser(log);

        campagne = read(newOtfaFile);

        assertOtfaCampagne(campagne, newOtfaFile.getParentFile());
    }

    private File getTestFile() {
        if (testFile != null && testFile.exists()) {
            return testFile;
        }
        final URL url = TestCrueOTFA.class.getResource(FICHIER_TEST_XML);
        testFile = new File(createTempDir(), "campagne.otfa.xml");
        try {
            CtuluLibFile.copyStream(url.openStream(), new FileOutputStream(testFile), true, true);
        } catch (final Exception e) {
            e.printStackTrace();

            fail();
        }

        return testFile;
    }

    public static OtfaCampagne read(final File otfaFile) {
        final CrueOTFAReaderWriter reader = new CrueOTFAReaderWriter("1.2", otfaFile);
        final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
        final CrueIOResu<OtfaCampagne> result = reader.readXML(otfaFile, log, new CrueDataImpl(CrueConfigMetierForTest.DEFAULT));

        testAnalyser(log);

        return result.getMetier();
    }

    private static void assertOtfaCampagne(final OtfaCampagne campagne, final File otfaDir) {
        Assert.assertEquals("Commentaire du fichier OTFA", campagne.getCommentaire());
        Assert.assertEquals("User", campagne.getAuteurCreation());
        Assert.assertEquals(new LocalDateTime(2011, 4, 20, 8, 45, 0), campagne.getDateCreation());
        Assert.assertEquals("User modif", campagne.getAuteurModification());
        Assert.assertEquals(new LocalDateTime(2011, 4, 22, 8, 45, 0), campagne.getDateModification());
        assertTrue(campagne.getReferenceOptions().isEffacerRunAvant());
        assertFalse(campagne.getReferenceOptions().isEffacerRunApres());
        assertFalse(campagne.getCibleOptions().isEffacerRunAvant());
        assertTrue(campagne.getCibleOptions().isEffacerRunApres());

        assertEquals(getLine1(), campagne.getLines().get(0));
        assertEquals(getLine2(otfaDir), campagne.getLines().get(1));
    }

    private static OtfaCampagneLine getLine1() {
        final OtfaCampagneLine line = new OtfaCampagneLine();

        line.setIndice(1);
        line.setCommentaire("Commentaire de la LigneCampagne ");
        line.setLancerComparaison(true);
        OtfaCampagneItem item = new OtfaCampagneItem();
        // /v10.1 est un path absolu:
        item.setEtuPath("/v10.1/Etu3-0");
        item.setScenarioNom("Sc_M3-0_c9");
        item.setCoeurName("Crue9");
        item.setLaunchCompute(true);
        item.setCheminCSV("cheminCsv1");
        item.setRapport("rapport1/test.xml");
        item.setRapportOnTransitoire(true);

        line.setReference(item);

        item = new OtfaCampagneItem();

        item.setEtuPath("C:/etudes/v10.2/Etu3-0");
        item.setScenarioNom("Sc_M3-0_c9");
        item.setCoeurName("Crue10.1");
        item.setLaunchCompute(true);
        item.setCheminCSV("cheminCsv2");
        item.setRapport("rapport2/test.xml");
        item.setRapportOnTransitoire(false);

        line.setCible(item);

        return line;
    }

    private static OtfaCampagneLine getLine2(final File otfaDir) {
        final OtfaCampagneLine line = new OtfaCampagneLine();

        line.setIndice(2);
        line.setCommentaire("Commentaire de la LigneCampagne ");
        line.setLancerComparaison(false);
        OtfaCampagneItem item = new OtfaCampagneItem();

        item.setEtuPath("/v10.1/Etu3-0");
        item.setScenarioNom("Sc_M3-0_c9c10");
        item.setCoeurName("Crue10.1");
        item.setLaunchCompute(false);

        line.setReference(item);

        item = new OtfaCampagneItem();

        item.setEtuPath("C:/etudes/v10.2/Etu3-0");
        item.setScenarioNom("Sc_M3-0_c9c10");
        item.setCoeurName("Crue10.2");
        item.setLaunchCompute(false);

        line.setCible(item);

        return line;
    }

    private static void assertEquals(final OtfaCampagneLine expected, final OtfaCampagneLine actual) {
        Assert.assertEquals(expected.getIndice(), actual.getIndice());
        Assert.assertEquals(expected.getCommentaire(), actual.getCommentaire());
        assertEquals(expected.getReference(), actual.getReference());
        assertEquals(expected.getCible(), actual.getCible());
        Assert.assertEquals(expected.isLancerComparaison(), actual.isLancerComparaison());
    }

    private static void assertEquals(final OtfaCampagneItem expected, final OtfaCampagneItem actual) {
        Assert.assertEquals(expected.getEtuPath(), actual.getEtuPath());
        Assert.assertEquals(expected.getScenarioNom(), actual.getScenarioNom());
        Assert.assertEquals(expected.getCoeurName(), actual.getCoeurName());
        Assert.assertEquals(expected.isLaunchCompute(), actual.isLaunchCompute());
        Assert.assertEquals(expected.getCheminCSV(), actual.getCheminCSV());
        Assert.assertEquals(expected.getRapport(), actual.getRapport());
        Assert.assertEquals(expected.isRapportOnTransitoire(), actual.isRapportOnTransitoire());
    }

    @Test
    @Override
    public void testValide() {
    }
}
