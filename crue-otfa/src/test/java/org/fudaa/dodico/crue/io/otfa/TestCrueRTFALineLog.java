/*
 * GPL v2
 */
package org.fudaa.dodico.crue.io.otfa;

import com.memoire.fu.FuLib;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.AbstractIOParentTest;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.io.line.CrueLineLogReaderWriter;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneResultHeader;
import org.joda.time.LocalDateTime;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

/**
 * @author CANEL Christophe
 */
public class TestCrueRTFALineLog extends AbstractIOParentTest {
  private static final String FICHIER_TEST_XML = "/otfa/campagne.ctfa-line.xml";

  public TestCrueRTFALineLog() {
    super(FICHIER_TEST_XML);
  }

  @Test
  public void testLecture() {
    assertCorrect(read(this.getTestFile()));
  }

  private static void assertCorrect(final CtuluLogGroup logsPart) {
    TestLogCreator.assertCorrectLogGroupSimple(logsPart);
  }

  @Test
  public void testEcriture() {
    final File ctfaFile = this.getTestFile();
    File newCtfaFile = null;

    try {
      newCtfaFile = File.createTempFile("Test", "CrueCTFA");
    } catch (final IOException e) {
      e.printStackTrace();
      Assert.fail();
    }

    CtuluLogGroup logsPart = read(ctfaFile);

    final CrueLineLogReaderWriter writer = new CrueLineLogReaderWriter("1.2");
    final CrueIOResu<CtuluLogGroup> resu = new CrueIOResu<>(logsPart);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);

    Assert.assertTrue(writer.writeXMLMetier(resu, newCtfaFile, log, CrueConfigMetierForTest.DEFAULT));

    testAnalyser(log);

    logsPart = read(newCtfaFile);

    assertCorrect(logsPart);
  }

  private File testFile;

  @After
  public void deleteFile() {
    if (testFile != null && testFile.exists()) {
      testFile.delete();
    }
  }

  private File getTestFile() {
    if (testFile != null && testFile.exists()) {
      return testFile;
    }
    final URL url = getClass().getResource(FICHIER_TEST_XML);
    testFile = new File(createTempDir(), "reaultat.ctfa.xml");

    try {
      CtuluLibFile.copyStream(url.openStream(), new FileOutputStream(testFile), true, true);
    } catch (final Exception e) {
      e.printStackTrace();

      Assert.fail();
    }

    return testFile;
  }

  private static CtuluLogGroup read(final File ctfaFile) {
    final CrueLineLogReaderWriter reader = new CrueLineLogReaderWriter("1.2");
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueIOResu<CtuluLogGroup> result = reader.readXML(ctfaFile, log, new CrueDataImpl(CrueConfigMetierForTest.DEFAULT));

    testAnalyser(log);

    return result.getMetier();
  }

  @Test
  @Override
  public void testValide() {
  }

  public static OtfaCampagneResultHeader createHeaderPart() {
    final OtfaCampagneResultHeader header = new OtfaCampagneResultHeader();

    header.setCommentaire("Une header.");
    header.setDate(new LocalDateTime(2011, 11, 24, 9, 25));
    header.setGlobalValidation(TestLogCreator.createLogGroup());

    return header;
  }

  public static void main(final String[] args) {
    final CtuluLogGroup part = TestLogCreator.createLogGroupSimple();
    final File newRtfaFile = new File(FuLib.getUserHome(), "resultat.ctfa-line.xml");
    System.err.println(newRtfaFile.getAbsolutePath());
    final CrueLineLogReaderWriter writer = new CrueLineLogReaderWriter("1.3");
    final CrueIOResu<CtuluLogGroup> resu = new CrueIOResu<>(part);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    Assert.assertTrue(writer.writeXMLMetier(resu, newRtfaFile, log, CrueConfigMetierForTest.DEFAULT));
  }
}
