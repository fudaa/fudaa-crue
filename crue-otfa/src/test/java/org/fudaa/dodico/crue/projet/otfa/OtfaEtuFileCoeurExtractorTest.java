package org.fudaa.dodico.crue.projet.otfa;

import org.fudaa.dodico.crue.io.otfa.TestCrueOTFA;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;

public class OtfaEtuFileCoeurExtractorTest extends AbstractTestParent {
  public OtfaEtuFileCoeurExtractorTest() {
  }

  @Test
  public void testExtract() {
    final File file = getFile(TestCrueOTFA.FICHIER_TEST_XML);
    final OtfaCampagne read = TestCrueOTFA.read(file);
    Assert.assertNotNull(read);
    final OtfaEtuFileCoeurExtractor extractor = new OtfaEtuFileCoeurExtractor();
    final List<String> extract = extractor.extract(read);
    Assert.assertEquals(2, extract.size());
    Assert.assertEquals("/v10.1/Etu3-0", extract.get(0));
    Assert.assertEquals("C:/etudes/v10.2/Etu3-0", extract.get(1));
  }
}
