/*
 * GPL v2
 */
package org.fudaa.dodico.crue.projet.otfa;

import junit.framework.TestCase;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.ConnexionInformationDefault;
import org.fudaa.dodico.crue.config.coeur.CoeurManagerForTest;
import org.fudaa.dodico.crue.io.common.FileLocker;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.*;

/**
 * @author CANEL Christophe
 */
public class TestOtfaContentValidator extends AbstractTestParent {
  private static File etudesDir;

  @Test
  public void testValidation() {
    etudesDir = exportZipInTempDir("/v1_1_1/otfa.zip");

    final OtfaContentValidator validator = new OtfaContentValidator(CoeurManagerForTest.create(null));
    final OtfaCampagne campagne = createCampagneWithInvalidEtuFile();
    CtuluLogGroup logGroup = validator.valid(campagne, false);
    Assert.assertEquals(campagne.getLines().size() + 1, logGroup.getLogs().size());//+1 because of the main log.
    Assert.assertTrue(logGroup.containsFatalError());

    Collection<CtuluLogRecord> records = getRecords(logGroup);

    Assert.assertEquals(6, records.size());//+1 because of the main log.
    List<String> message = Arrays.asList("otfa.validator.cibleEtuFileNotFound", "otfa.validator.referenceXsdNotCompatible",
        "otfa.validator.refEtuFileNotFound", "otfa.validator.cibleEtuFileNotFound", "otfa.validator.cibleEtuFileNotFound",
        "otfa.validator.referenceXsdNotCompatible");
    int idx = 0;
    for (final CtuluLogRecord record : records) {
      Assert.assertEquals("idx= " + idx, message.get(idx++), record.getMsg());
    }

    logGroup = validator.valid(createCampagneWithInvalidCoeurName(), false);

    Assert.assertTrue(logGroup.containsFatalError());

    records = getRecords(logGroup);

    Assert.assertEquals(4, records.size());

    message = Arrays.asList("otfa.validator.cibleCoeurNotFound", "otfa.validator.referenceCoeurNotFound",
        "otfa.validator.crueVersionNonCompatible", "otfa.validator.referenceCoeurNotFound");
    idx = 0;
    for (final CtuluLogRecord record : records) {
      Assert.assertEquals("idx= " + idx, message.get(idx++), record.getMsg());
    }

    logGroup = validator.valid(createCampagneWithXsdNotCorresponding(), false);

    Assert.assertTrue(logGroup.containsFatalError());

    records = getRecords(logGroup);

    Assert.assertEquals(5, records.size());

    message = Arrays.asList("otfa.validator.referenceXsdNotCompatible", "otfa.validator.cibleXsdNotCompatible",
        "otfa.validator.referenceXsdNotCompatible", "otfa.validator.referenceXsdNotCompatible", "otfa.validator.cibleXsdNotCompatible");
    idx = 0;
    for (final CtuluLogRecord record : records) {
      Assert.assertEquals("idx= " + idx, message.get(idx++), record.getMsg());
    }

    logGroup = validator.valid(createCampagneWithInvalidScenario(), false);

    Assert.assertTrue(logGroup.containsFatalError());

    records = getRecords(logGroup);

    Assert.assertEquals(3, records.size());

    for (final CtuluLogRecord record : records) {
      Assert.assertEquals("otfa.validator.scenarioNotFound", record.getMsg());
    }
    final OtfaCampagne validCompagne = createValidCampagne();

    logGroup = validator.valid(validCompagne, false);
    Assert.assertFalse(logGroup.containsFatalError());
    final OtfaCampagneLine line = validCompagne.getLines().get(0);
    testLockEtuFiles(campagne, line, validator);
  }

  private void testLockEtuFiles(final OtfaCampagne campagne, final OtfaCampagneLine line, final OtfaContentValidator validator) {
    CtuluLogGroup logGroup;
    final Collection<CtuluLogRecord> records;
    final OtfaCampagne campagneUniqueWithLock = new OtfaCampagne();
    campagneUniqueWithLock.setOtfaFile(campagne.getOtfaFile());
    campagneUniqueWithLock.setLines(Collections.singletonList(line));
    final File etuFileRef = line.getReference().getEtuFile(campagne.getOtfaDir());
    final File etuFileCible = line.getFinalCible().getEtuFile(campagne.getOtfaDir());
    final FileLocker locker = new FileLocker();
    locker.lock(etuFileRef, ConnexionInformationDefault.getDefault());
    locker.lock(etuFileCible, ConnexionInformationDefault.getDefault());
    logGroup = validator.valid(campagneUniqueWithLock, true);
    Assert.assertTrue(logGroup.containsFatalError());
    records = getRecords(logGroup);
    Assert.assertEquals(2, records.size());
    final Iterator<CtuluLogRecord> iterator = records.iterator();
    CtuluLogRecord next = iterator.next();
    Assert.assertEquals("otfa.referenceFileEtu.isLocked", next.getMsg());
    next = iterator.next();
    Assert.assertEquals("otfa.cibleFileEtu.isLocked", next.getMsg());
    locker.unlock(etuFileRef);
    locker.unlock(etuFileCible);
    logGroup = validator.valid(campagneUniqueWithLock, true);
    Assert.assertFalse(logGroup.containsFatalError());
  }

  private static Collection<CtuluLogRecord> getRecords(final CtuluLogGroup logGroup) {
    final Collection<CtuluLogRecord> records = new ArrayList<>();

    for (final CtuluLog log : logGroup.getLogs()) {
      records.addAll(log.getRecords());
    }

    return records;
  }

  private static OtfaCampagne createValidCampagne() {
    final OtfaCampagne campagne = new OtfaCampagne();
    final List<OtfaCampagneLine> lines = new ArrayList<>();
    campagne.setLines(lines);

    OtfaCampagneItem reference = createCampagneItem("Etu3-0", "Sc_M3-0_c9", OtfaCampagneItem.CRUE9);
    OtfaCampagneItem cible = createCampagneItem("Etu3-1", "Sc_M3-1_c9", OtfaCampagneItem.CRUE9);
    OtfaCampagneLine line = createCampagneLine(reference, cible, 0);
    lines.add(line);

    reference = createCampagneItem("Etu3-0", "Sc_M3-0_c9", OtfaCampagneItem.CRUE9);
    cible = createCampagneItem("Etu6-0", "Sc_M6-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 1);
    lines.add(line);

    reference = createCampagneItem("Etu4-0", "Sc_M4-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    cible = createCampagneItem("Etu5-0", "Sc_M5-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 2);
    lines.add(line);

    // Doit créer un scénario M5-0_c9c10 qui est la conversion du scénario M5-0_c9.
    reference = createCampagneItem("Etu5-0", "Sc_M5-0_c9", OtfaCampagneItem.CRUE9);
    cible = createCampagneItem(null, null, CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 3);
    lines.add(line);

    // Utilise le scénario M5-0_c9c10 créé à la ligne précédente.
    reference = createCampagneItem("Etu6-0", "Sc_M6-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    cible = createCampagneItem("Etu5-0", "Sc_M5-0_c9c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 4);
    lines.add(line);

    reference = createCampagneItem("Etu7-0", "Sc_M7-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    cible = createCampagneItem("Etu7-1", "Sc_M7-1_c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 5);
    lines.add(line);

    reference = createCampagneItem("Etu4-0", "Sc_M4-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    cible = createCampagneItem("Etu4-1", "Sc_M4-1_c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 6);
    lines.add(line);

    return campagne;
  }

  private static OtfaCampagne createCampagneWithInvalidEtuFile() {
    final OtfaCampagne campagne = new OtfaCampagne();
    final List<OtfaCampagneLine> lines = new ArrayList<>();
    campagne.setLines(lines);

    OtfaCampagneItem reference = createCampagneItem("Etu3-0", "Sc_M3-0_c9", OtfaCampagneItem.CRUE9);
    OtfaCampagneItem cible = createCampagneItem("Etu3-1", "Sc_M3-1_c9", OtfaCampagneItem.CRUE9);
    OtfaCampagneLine line = createCampagneLine(reference, cible, 0);
    lines.add(line);

    reference = createCampagneItem("Etu3-0", "Sc_M3-0_c9", OtfaCampagneItem.CRUE9);
    cible = createCampagneItem("Etu6-0", "Sc_M6-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 1);
    lines.add(line);

    // Le fichier etu de la référence n'existe pas.
    reference = createCampagneItem("Etu4-0", "Sc_M4-0_c10", CoeurManagerForTest.CRUE10_2B_NAME);
    cible = createCampagneItem("Etu5-1", "Sc_M5-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 2);
    lines.add(line);

    // Doit créer un scénario M5-0_c9c10 qui est la conversion du scénario M5-0_c9.
    reference = createCampagneItem("Etu5-0", "Sc_M5-0_c9", OtfaCampagneItem.CRUE9);
    cible = createCampagneItem(null, null, CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 3);
    lines.add(line);

    // Utilise le scénario M5-0_c9c10 créé à la ligne précédente.
    reference = createCampagneItem("Etu6-0", "Sc_M6-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    cible = createCampagneItem("Etu5-0", "Sc_M5-0_c9c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 4);
    lines.add(line);

    // Les fichiers etu de la cible et de la référence n'existe pas.
    reference = createCampagneItem("Etu8-0", "Sc_M7-0_c10", CoeurManagerForTest.CRUE10_2_NAME);
    cible = createCampagneItem("Etu8-1", "Sc_M7-1_c10", CoeurManagerForTest.CRUE10_2_NAME);
    line = createCampagneLine(reference, cible, 5);
    lines.add(line);

    // Le fichier etu de la référence n'existe pas.
    reference = createCampagneItem("Etu4-0", "Sc_M4-0_c10", CoeurManagerForTest.CRUE10_2_NAME);
    cible = createCampagneItem("Etu5-1", "Sc_M4-1_c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 6);
    lines.add(line);

    return campagne;
  }

  private static OtfaCampagne createCampagneWithInvalidCoeurName() {
    final OtfaCampagne campagne = new OtfaCampagne();
    final List<OtfaCampagneLine> lines = new ArrayList<>();
    campagne.setLines(lines);

    OtfaCampagneItem reference = createCampagneItem("Etu3-0", "Sc_M3-0_c9", OtfaCampagneItem.CRUE9);
    OtfaCampagneItem cible = createCampagneItem("Etu3-1", "Sc_M3-1_c9", OtfaCampagneItem.CRUE9);
    OtfaCampagneLine line = createCampagneLine(reference, cible, 0);
    lines.add(line);

    // Le coeur de la cible n'existe pas.
    reference = createCampagneItem("Etu3-0", "Sc_M3-0_c9", OtfaCampagneItem.CRUE9);
    cible = createCampagneItem("Etu6-0", "Sc_M6-0_c10", "Crue7");
    line = createCampagneLine(reference, cible, 1);
    lines.add(line);

    // Le coeur de la référence n'existe pas.
    reference = createCampagneItem("Etu4-0", "Sc_M4-0_c10", "Crue9.2");
    cible = createCampagneItem("Etu5-0", "Sc_M5-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 2);
    lines.add(line);

    //la cible propose un coeur c10 pour un scenario c9.
    reference = createCampagneItem("Etu5-0", "Sc_M5-0_c9", OtfaCampagneItem.CRUE9);
    cible = createCampagneItem("Etu5-0", "Sc_M5-0_c9", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 3);
    lines.add(line);

    // Utilise le scénario M5-0_c9c10 créé à la ligne précédente.
    reference = createCampagneItem("Etu6-0", "Sc_M6-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    cible = createCampagneItem("Etu5-0", "Sc_M5-0_c9c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 4);
    lines.add(line);

    // Le coeur de la référence n'existe pas.
    reference = createCampagneItem("Etu7-0", "Sc_M7-0_c10", "Crue7");
    cible = createCampagneItem("Etu7-1", "Sc_M7-1_c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 5);
    lines.add(line);

    reference = createCampagneItem("Etu4-0", "Sc_M4-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    cible = createCampagneItem("Etu4-1", "Sc_M4-1_c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 6);
    lines.add(line);

    return campagne;
  }

  private static OtfaCampagne createCampagneWithXsdNotCorresponding() {
    final OtfaCampagne campagne = new OtfaCampagne();
    final List<OtfaCampagneLine> lines = new ArrayList<>();
    campagne.setLines(lines);

    OtfaCampagneItem reference = createCampagneItem("Etu3-0", "Sc_M3-0_c9", OtfaCampagneItem.CRUE9);
    OtfaCampagneItem cible = createCampagneItem("Etu3-1", "Sc_M3-1_c9", OtfaCampagneItem.CRUE9);
    OtfaCampagneLine line = createCampagneLine(reference, cible, 0);
    lines.add(line);

    reference = createCampagneItem("Etu3-0", "Sc_M3-0_c9", OtfaCampagneItem.CRUE9);
    cible = createCampagneItem("Etu6-0", "Sc_M6-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 1);
    lines.add(line);

    //"otfa.validator.referenceXsdNotCompatible"
    reference = createCampagneItem("Etu4-0", "Sc_M4-0_c10", CoeurManagerForTest.CRUE10_2B_NAME);
    //"otfa.validator.cibleXsdNotCompatible"
    cible = createCampagneItem("Etu5-0", "Sc_M5-0_c10", CoeurManagerForTest.CRUE10_2_NAME);
    line = createCampagneLine(reference, cible, 2);
    lines.add(line);

    reference = createCampagneItem("Etu5-0", "Sc_M5-0_c9", OtfaCampagneItem.CRUE9);
    cible = createCampagneItem("Etu5-0", "Sc_M5-0_c9", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 3);
    lines.add(line);

    // Utilise le scénario M5-0_c9c10 créé à la ligne précédente.
    // La Xsd de la référence est incompatible la version du fichier etu
    //"otfa.validator.referenceXsdNotCompatible"
    reference = createCampagneItem("Etu6-0", "Sc_M6-0_c10", CoeurManagerForTest.CRUE10_2_NAME);
    cible = createCampagneItem("Etu5-0", "Sc_M5-0_c9c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 4);
    lines.add(line);

    reference = createCampagneItem("Etu7-0", "Sc_M7-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    cible = createCampagneItem("Etu7-1", "Sc_M7-1_c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 5);
    lines.add(line);
//"otfa.validator.referenceXsdNotCompatible"
    reference = createCampagneItem("Etu4-0", "Sc_M4-0_c10", CoeurManagerForTest.CRUE10_2_NAME);
    cible = createCampagneItem("Etu4-1", "Sc_M4-1_c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 6);
    lines.add(line);

    //"otfa.validator.referenceXsdNotCompatible" cas de conversion
    reference = createCampagneItem("Etu4-0", "Sc_M4-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    cible = createCampagneItem(null, null, CoeurManagerForTest.CRUE10_2_NAME);
    line = createCampagneLine(reference, cible, 7);
    lines.add(line);

    return campagne;
  }

  private static OtfaCampagne createCampagneWithInvalidScenario() {
    final OtfaCampagne campagne = new OtfaCampagne();
    final List<OtfaCampagneLine> lines = new ArrayList<>();
    campagne.setLines(lines);

    // Le scénario de la référence n'existe pas.
    OtfaCampagneItem reference = createCampagneItem("Etu3-0", "Sc_M3-34_c9", OtfaCampagneItem.CRUE9);
    OtfaCampagneItem cible = createCampagneItem("Etu3-1", "Sc_M3-1_c9", OtfaCampagneItem.CRUE9);
    OtfaCampagneLine line = createCampagneLine(reference, cible, 0);
    lines.add(line);

    // Le scénario de la référence n'existe pas.
    reference = createCampagneItem("Etu3-0", "Sc_M3-34_c9", OtfaCampagneItem.CRUE9);
    cible = createCampagneItem("Etu6-0", "Sc_M6-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 1);
    lines.add(line);

    reference = createCampagneItem("Etu4-0", "Sc_M4-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    cible = createCampagneItem("Etu5-0", "Sc_M5-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 2);
    lines.add(line);

    // Utilise le scénario M5-0_c9c10 créé à la ligne suivante et qui donc n'existe pas encore.
    reference = createCampagneItem("Etu6-0", "Sc_M6-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    cible = createCampagneItem("Etu5-0", "Sc_M5-0_c9c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 4);
    lines.add(line);

    // Doit créer un scénario M5-0_c9c10 qui est la conversion du scénario M5-0_c9.
    reference = createCampagneItem("Etu5-0", "Sc_M5-0_c9", OtfaCampagneItem.CRUE9);
    cible = createCampagneItem(null, null, CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 3);
    lines.add(line);

    reference = createCampagneItem("Etu7-0", "Sc_M7-0_c10", CoeurManagerForTest.CRUE10_1_NAME);
    cible = createCampagneItem("Etu7-1", "Sc_M7-1_c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 5);
    lines.add(line);

    // Le scénario de la référence n'existe pas.
    reference = createCampagneItem("Etu4-0", "Sc_M4-21_c10", CoeurManagerForTest.CRUE10_1_NAME);
    cible = createCampagneItem("Etu4-1", "Sc_M4-1_c10", CoeurManagerForTest.CRUE10_1_NAME);
    line = createCampagneLine(reference, cible, 6);
    lines.add(line);

    return campagne;
  }

  private static OtfaCampagneLine createCampagneLine(final OtfaCampagneItem reference, final OtfaCampagneItem cible, final int indice) {
    final OtfaCampagneLine line = new OtfaCampagneLine();

    line.setReference(reference);
    line.setCible(cible);
    line.setIndice(indice);

    return line;
  }

  private static OtfaCampagneItem createCampagneItem(final String etude, final String scenario, final String coeur) {
    final OtfaCampagneItem item = new OtfaCampagneItem();

    if (etude != null) {
      item.setEtuPath(TestOtfaContentValidator.getEtuFile(etude).getAbsolutePath());
    } else {
      item.setEtuPath(null);
    }

    item.setScenarioNom(scenario);
    item.setCoeurName(coeur);
    item.setLaunchCompute(false);

    return item;
  }

  private static File getEtuFile(final String etude) {
    return getEtuFileUpdated(etude, new File(etudesDir, etude));
  }

  public static File getEtuFileUpdated(final String etuId, final File dir) {
    final File etuFile = new File(dir, StringUtils.capitalize(etuId) + ".etu.xml");
    final File destFile = new File(dir, StringUtils.capitalize(etuId) + ".new.etu.xml");

    if (etuFile.isFile()) {
      CtuluLibFile.replaceAndCopyFile(etuFile, destFile, "@dir@", dir.getAbsolutePath() + "/", "UTF-8");
      TestCase.assertTrue(destFile.isFile());
    }

    return destFile;
  }
}
