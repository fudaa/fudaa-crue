/*
 * GPL v2
 */
package org.fudaa.dodico.crue.io.otfa;

import com.memoire.fu.FuLib;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.AbstractIOParentTest;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.io.line.CrueLineResultReaderWriter;
import org.fudaa.dodico.crue.io.rtfa.CrueRTFAReaderWriter;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLineResultComparaisons;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Logger;

/**
 * @author CANEL Christophe
 */
public class TestCrueRTFALineResult extends AbstractIOParentTest {
  private static final String FICHIER_TEST_XML = "/otfa/campagne.rtfa-line.xml";

  public TestCrueRTFALineResult() {
    super(FICHIER_TEST_XML);
  }

  @Test
  public void testLecture() {
    assertCorrect(read(this.getTestFile()));
  }

  private static void assertCorrect(final OtfaCampagneLineResultComparaisons logsPart) {
    TestOtfaExecuteComparaisonResultCreator.assertCorrect(logsPart);
  }

  @Test
  public void testEcriture() {
    final File ctfaFile = this.getTestFile();
    File newCtfaFile = null;

    try {
      newCtfaFile = File.createTempFile("Test", "CrueCTFA");
    } catch (final IOException e) {
      e.printStackTrace();
      Assert.fail();
    }

    OtfaCampagneLineResultComparaisons logsPart = read(ctfaFile);

    final CrueLineResultReaderWriter writer = new CrueLineResultReaderWriter(CrueRTFAReaderWriter.LAST_VERSION);
    final CrueIOResu<OtfaCampagneLineResultComparaisons> resu = new CrueIOResu<>(logsPart);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);

    Assert.assertTrue(writer.writeXMLMetier(resu, newCtfaFile, log, CrueConfigMetierForTest.DEFAULT));
    testAnalyser(log);
    logsPart = read(newCtfaFile);
    assertCorrect(logsPart);
  }

  private File testFile;

  @After
  public void deleteFile() {
    if (testFile != null && testFile.exists()) {
      final boolean deleted = testFile.delete();
      if (!deleted) {
        Logger.getLogger(getClass().getName()).warning(testFile.getAbsolutePath() + " not deleted");
      }
    }
  }

  private File getTestFile() {
    if (testFile != null && testFile.exists()) {
      return testFile;
    }
    final URL url = getClass().getResource(FICHIER_TEST_XML);
    testFile = new File(createTempDir(), "reaultat.rtfa-line.xml");

    try {
      CtuluLibFile.copyStream(url.openStream(), new FileOutputStream(testFile), true, true);
    } catch (final Exception e) {
      e.printStackTrace();

      Assert.fail();
    }

    return testFile;
  }

  private static OtfaCampagneLineResultComparaisons read(final File ctfaFile) {
    final CrueLineResultReaderWriter reader = new CrueLineResultReaderWriter("1.2");
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueIOResu<OtfaCampagneLineResultComparaisons> result = reader.readXML(ctfaFile, log, new CrueDataImpl(CrueConfigMetierForTest.DEFAULT));
    testAnalyser(log);
    return result.getMetier();
  }

  @Test
  @Override
  public void testValide() {
  }

  public static void main(final String[] args) {
    final OtfaCampagneLineResultComparaisons part = TestOtfaExecuteComparaisonResultCreator.createComparaisonResult();
    final File newRtfaFile = new File(FuLib.getUserHome(), "resultat.rtfa-line.xml");
    System.err.println(newRtfaFile.getAbsolutePath());
    final CrueLineResultReaderWriter writer = new CrueLineResultReaderWriter("1.3");
    final CrueIOResu<OtfaCampagneLineResultComparaisons> resu = new CrueIOResu<>(part);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    Assert.assertTrue(writer.writeXMLMetier(resu, newRtfaFile, log, CrueConfigMetierForTest.DEFAULT));
  }
}
