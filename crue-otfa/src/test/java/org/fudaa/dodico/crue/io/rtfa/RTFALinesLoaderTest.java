/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.rtfa;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.otfa.OtfaFileUtils;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLineResultComparaisons;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

/**
 * @author Frederic Deniger
 */
public class RTFALinesLoaderTest extends AbstractTestParent {
  public RTFALinesLoaderTest() {
  }

  @Test
  public void testRead() {
    final File targetDir = createTempDir();
    final File targetFile = OtfaFileUtils.getRtfaZipFile(new File(targetDir, "test.otfa.xml"));
    CtuluLibFile.getFileFromJar("/rtfa/otfa-conversion.rtfa.zip", targetFile);
    assertTrue(targetFile.exists());
    assertTrue(targetFile.length() > 0);
    final RTFAResultLinesLoader loader = new RTFAResultLinesLoader(targetFile);
    final OtfaCampagneLine line = new OtfaCampagneLine();
    line.setIndice(1);
    CrueIOResu<CtuluLogGroup> loadLogs = loader.loadLogs(line);
    assertFalse(loadLogs.getAnalyse().containsErrorOrSevereError());
    assertNotNull(loadLogs.getMetier());
    assertTrue(loadLogs.getMetier().containsError());
    assertEquals("valid.loi.notStrictlyIncreasing",loadLogs.getMetier().getGroups().get(0).getGroups().get(0).getGroups().get(0).getLogs().get(0).getRecords().get(0).getMsg());
    CrueIOResu<OtfaCampagneLineResultComparaisons> loadResultat = loader.loadResults(line);
    assertFalse(loadResultat.getAnalyse().containsErrorOrSevereError());

    assertNotNull(loadResultat.getMetier());

    line.setIndice(10);
    loadLogs = loader.loadLogs(line);
    assertTrue(loadLogs.getAnalyse().containsErrorOrSevereError());
    assertNull(loadLogs.getMetier());
    loadResultat = loader.loadResults(line);
    assertTrue(loadResultat.getAnalyse().containsErrorOrSevereError());
    assertNull(loadResultat.getMetier());
  }
}
