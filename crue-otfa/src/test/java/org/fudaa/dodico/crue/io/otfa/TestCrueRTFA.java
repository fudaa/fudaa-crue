/*
 * GPL v2
 */
package org.fudaa.dodico.crue.io.otfa;

import com.memoire.fu.FuLib;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.AbstractIOParentTest;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.io.rtfa.CrueRTFAReaderWriter;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLine;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLines;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneResultHeader;
import org.joda.time.LocalDateTime;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author CANEL Christophe
 */
public class TestCrueRTFA extends AbstractIOParentTest {
  protected static final String FICHIER_TEST_XML = "/otfa/campagne.rtfa.xml";

  public TestCrueRTFA() {
    super(FICHIER_TEST_XML);
  }

  @Test
  public void testLecture() {
    final RTFAResultLines compPart = read(this.getTestFile());
    assertCorrect(compPart);
  }

  private static void assertCorrect(final RTFAResultLines compPart) {
    final List<RTFAResultLine> lines = compPart.getLines();

    assertHeaderCorrect(compPart.getHeader());
    Assert.assertEquals(2, lines.size());
    assertCorrectCompPartLine1(lines.get(0));
    assertCorrectCompPartLine2(lines.get(1));
  }

  private static void assertCorrectCompPartLine1(final RTFAResultLine line) {

    final RTFAResultLine initLine1 = createResultCompPartLine1();
    Assert.assertTrue(line.getInitialLine().equals(initLine1.getInitialLine()));
    Assert.assertTrue(initLine1.isSameCount(line));
  }

  private static void assertCorrectCompPartLine2(final RTFAResultLine line) {

    final RTFAResultLine initLine2 = createResultCompPartLine2();
    Assert.assertTrue(line.getInitialLine().equals(initLine2.getInitialLine()));
    Assert.assertTrue(initLine2.isSameCount(line));
  }

  @Test
  public void testEcriture() {
    final File rtfaFile = this.getTestFile();
    File newRtfaFile = null;

    try {
      newRtfaFile = File.createTempFile("Test", "CrueRTFA");
    } catch (final IOException e) {
      e.printStackTrace();
      Assert.fail();
    }

    RTFAResultLines compPart = read(rtfaFile);
    final CrueRTFAReaderWriter writer = new CrueRTFAReaderWriter("1.3");
    final CrueIOResu<RTFAResultLines> resu = new CrueIOResu<>(compPart);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    Assert.assertTrue(writer.writeXMLMetier(resu, newRtfaFile, log, CrueConfigMetierForTest.DEFAULT));
    testAnalyser(log);
    compPart = read(newRtfaFile);
    assertCorrect(compPart);
  }

  private File testFile;

  @After
  public void deleteFile() {
    if (testFile != null && testFile.exists()) {
      testFile.delete();
    }
  }

  private File getTestFile() {
    if (testFile != null && testFile.exists()) {
      return testFile;
    }
    final URL url = getClass().getResource(FICHIER_TEST_XML);
    testFile = new File(createTempDir(), "resultat.rtfa.xml");

    try {
      CtuluLibFile.copyStream(url.openStream(), new FileOutputStream(testFile), true, true);
    } catch (final Exception e) {
      e.printStackTrace();

      Assert.fail();
    }

    return testFile;
  }

  private static RTFAResultLines read(final File rtfaFile) {
    final CrueRTFAReaderWriter reader = new CrueRTFAReaderWriter("1.3");
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueIOResu<RTFAResultLines> result = reader.readXML(rtfaFile, log, new CrueDataImpl(CrueConfigMetierForTest.DEFAULT));

    testAnalyser(log);

    return result.getMetier();
  }

  public static void main(final String[] args) {
    final RTFAResultLines part = createResultCompPart();
    final File newRtfaFile = new File(FuLib.getUserHome(), "resultat.rtfa.xml");
    System.err.println(newRtfaFile.getAbsolutePath());
    final CrueRTFAReaderWriter writer = new CrueRTFAReaderWriter("1.3");
    final CrueIOResu<RTFAResultLines> resu = new CrueIOResu<>(part);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);

    Assert.assertTrue(writer.writeXMLMetier(resu, newRtfaFile, log, CrueConfigMetierForTest.DEFAULT));
  }

  @Test
  @Override
  public void testValide() {
  }

  public static RTFAResultLines createResultCompPart() {
    final RTFAResultLines part = new RTFAResultLines();

    part.setHeader(TestCrueRTFALineLog.createHeaderPart());
    part.addLines(createResultLogsCompLines());

    return part;
  }

  private static List<RTFAResultLine> createResultLogsCompLines() {
    final List<RTFAResultLine> lines = new ArrayList<>();

    lines.add(createResultCompPartLine1());
    lines.add(createResultCompPartLine2());

    return lines;
  }

  private static RTFAResultLine createResultCompPartLine1() {
    final RTFAResultLine line = new RTFAResultLine();
    line.setInitialLine(TestOtfaCampagneLineCreator.createCampagneLine1());
    line.setNbDifferences(10);
    line.setNbSevereError(9);
    line.setNbError(8);
    line.setNbWarn(7);
    line.setNbInfo(6);
    return line;
  }

  private static RTFAResultLine createResultCompPartLine2() {
    final RTFAResultLine line = new RTFAResultLine();
    line.setInitialLine(TestOtfaCampagneLineCreator.createCampagneLine2());
    line.setNbDifferences(5);
    line.setNbSevereError(4);
    line.setNbError(3);
    line.setNbWarn(2);
    line.setNbInfo(1);
    return line;
  }

  private static void assertHeaderCorrect(final OtfaCampagneResultHeader header) {
    Assert.assertEquals("Une header.", header.getCommentaire());
    Assert.assertEquals(new LocalDateTime(2011, 11, 24, 9, 25), header.getDate());
    TestLogCreator.assertCorrectLogGroup(header.getGlobalValidation());
  }
}
