/*
 GPL 2
 */
package org.fudaa.fudaa.crue.views;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.IdRegistry;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.fudaa.fudaa.crue.common.helper.LoadEtuHelper;
import org.openide.util.Exceptions;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author Frederic Deniger
 */
public class DClimMExample {
  public static Pair<EMHScenarioContainer, File> readScenarioAndRun() {
    File target = null;
    try {
      target = CtuluLibFile.createTempDir();
    } catch (final IOException ex) {
      Exceptions.printStackTrace(ex);
    }
    AbstractTestParent.exportZip(target, "/Etu3-0.zip");
    final File file = new File(new File(target, "Etu3-0"), "Etu3-0.etu.xml");
    final EMHProjet projet = LoadEtuHelper.loadEtu(file, TestCoeurConfig.INSTANCE_1_1_1);
    final EMHScenarioContainer runv8 = LoadEtuHelper.loadRun("Sc_M3-0_v8c9", projet, 0);
    return new Pair<>(runv8, target);
  }
  public static void main(String[] args) {
    Pair<EMHScenarioContainer, File> readScenarioAndRun = readScenarioAndRun();
    DonCLimMLineBuilder builder = new DonCLimMLineBuilder();
    EMHScenario emhScenario = readScenarioAndRun.first.getEmhScenario();
    IdRegistry.install(emhScenario);
    JScrollPane createScrollPane = builder.createScrollPane(TestCoeurConfig.INSTANCE.getCrueConfigMetier(), emhScenario, true, null, null).first;
    JFrame frame = new JFrame();
    frame.setContentPane(createScrollPane);
    frame.setPreferredSize(new Dimension(500, 500));
    frame.setSize(new Dimension(500, 500));
    frame.setLocation(0, 0);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);
  }
}
