package org.fudaa.fudaa.crue.options.config;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ebli.courbe.EGAxeHorizontalPersist;
import org.fudaa.ebli.courbe.EGAxeVerticalPersist;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.junit.Assert;
import org.junit.Test;

import java.awt.*;

public class CourbeConfigTest {

  private static EGCourbePersist createEGCourbePersist() {
    EGCourbePersist init = new EGCourbePersist();
    init.setTitle("title");
    init.setDisplayTitle(false);
    init.setIdgroup(555);
    init.setColor("red",Color.RED);
    init.setColor("blue",Color.BLUE);
    init.setDisplayLabels(true);
    return init;
  }
  private static EGAxeHorizontalPersist createEGAxeHorizontalPersist() {
    EGAxeHorizontalPersist init = new EGAxeHorizontalPersist();
    init.setTitre("title");
    init.setTitreVisible(false);
    init.setUnite("unit");
    init.setVisible(true);
    init.setFont(new Font("test", 1, 2));
    init.setGraduations(true);
    init.setGrille(new TraceLigne(2, 4, Color.RED));
    init.setIsIteratorUptodate(true);
    init.setLineColor(Color.WHITE);
    init.setRange(new CtuluRange(1, 2));
    init.setTraceGrille(true);
    init.setTraceSousGrille(true);
    init.setTraceGraduations(new TraceLigneModel(5, 6, Color.RED));
    init.setTraceSousGraduations(new TraceLigneModel(0, 1, Color.RED));
    init.setNbPas(222);
    init.setLongueurPas(1.223);
    init.setModeGraduations(100);
    init.setNbSousGraduations(8);
    init.setIsExtremiteDessinee(true);
    return init;
  }
  private static EGAxeVerticalPersist createEGAxeVerticalPersist() {
    EGAxeVerticalPersist init = new EGAxeVerticalPersist();
    init.setDroite(true);
    init.setTitreVertical(true);
    init.setTitreVerticalDroite(true);
    init.setTitre("title");
    init.setTitreVisible(false);
    init.setUnite("unit");
    init.setVisible(true);
    init.setFont(new Font("test",1,2));
    init.setGraduations(true);
    init.setGrille(new TraceLigne(2,4, Color.RED));
    init.setIsIteratorUptodate(true);
    init.setLineColor(Color.WHITE);
    init.setRange(new CtuluRange(1, 2));
    init.setTraceGrille(true);
    init.setTraceSousGrille(true);
    init.setTraceGraduations(new TraceLigneModel(5,6, Color.RED));
    init.setTraceSousGraduations(new TraceLigneModel(0,1, Color.RED));
    init.setNbPas(222);
    init.setLongueurPas(1.223);
    init.setModeGraduations(100);
    init.setNbSousGraduations(8);
    init.setIsExtremiteDessinee(true);
    return init;
  }

  @Test
  public void testCreateCopy() {

    CourbeConfig courbeConfig = new CourbeConfig();
    String keyAxeVerticalOne = "test1";
    courbeConfig.saveAxeVConfig(keyAxeVerticalOne,createEGAxeVerticalPersist());
    courbeConfig.saveAxeVConfig("test2",createEGAxeVerticalPersist());
    courbeConfig.saveAxeH(createEGAxeHorizontalPersist());
    String courbeConfigKey = "test3";
    courbeConfig.saveConfig(courbeConfigKey,createEGCourbePersist());

    CourbeConfig copy = courbeConfig.createCopy();
    Assert.assertNotSame(courbeConfig.getAxeVConfig(keyAxeVerticalOne),copy.getAxeVConfig(keyAxeVerticalOne));
    Assert.assertNotSame(courbeConfig.getHorizontalPersist(),copy.getHorizontalPersist());
    Assert.assertNotSame(courbeConfig.getCourbeConfig(courbeConfigKey),copy.getCourbeConfig(courbeConfigKey));

    XStream xstream = createXstream();
    Assert.assertEquals(xstream.toXML(courbeConfig),xstream.toXML(copy));

  }

  private static XStream createXstream() {
    XStream xstream=new XStream();
    xstream.processAnnotations(CourbeConfig.class);
    xstream.processAnnotations(EGAxeVerticalPersist.class);
    xstream.processAnnotations(EGAxeHorizontalPersist.class);
    xstream.processAnnotations(EGCourbePersist.class);
    return xstream;
  }

}