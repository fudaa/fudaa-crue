/*
 GPL 2
 */
package org.fudaa.fudaa.crue.views;

import gnu.trove.TLongObjectHashMap;
import org.fudaa.ctulu.gui.CtuluPopupListener;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.bean.DonCLimMLineContent;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.fudaa.crue.common.swing.TableRowHeaderInstaller;
import org.fudaa.fudaa.crue.emh.node.HierarchyChildFactory;
import org.openide.util.Exceptions;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Frederic Deniger
 */
public class DonCLimMLineBuilder {
    private static final int TABLE_COLUMN_HEIGHT = 20;

    /**
     * @param scenario le scenario
     * @param clone    true si on veut cloner les {@link DonCLimMCommonItem}
     * @return un model de dclms clonées
     */
    private DonClimMTableModel build(EMHScenario scenario, boolean clone) {
        List<OrdCalc> ordCalcs = scenario.getOrdCalcScenario().getOrdCalc();
        List<DonCLimMLineContent> lines = new ArrayList<>();
        Set<Long> emhs = new HashSet<>();
        for (OrdCalc ordCalc : ordCalcs) {
            List<DonCLimM> listeDCLMUserActive = ordCalc.getCalc().getlisteDCLMUserActive();
            TLongObjectHashMap<List<DonCLimM>> dclmsByEMHId = new TLongObjectHashMap<>();
            for (DonCLimM donCLimM : listeDCLMUserActive) {
                try {
                    if (!donCLimM.getActuallyActive()) {//EMH non active...
                        continue;
                    }
                    DonCLimM toUse = clone ? ((DonCLimMCommonItem) donCLimM).clone() : donCLimM;
                    final Long uiId = donCLimM.getEmh().getUiId();
                    emhs.add(uiId);
                    List<DonCLimM> existing = dclmsByEMHId.get(uiId);
                    if (existing == null) {
                        existing = new ArrayList<>();
                        dclmsByEMHId.put(uiId, existing);
                    }
                    existing.add(toUse);
                } catch (CloneNotSupportedException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
            lines.add(new DonCLimMLineContent(ordCalc.getCalc(), dclmsByEMHId));
        }
        List<EMH> allSimpleEMH = HierarchyChildFactory.getEMHInNetworkOrder(null, scenario);
        List<EMH> emhToDisplay = new ArrayList<>();
        for (EMH emh : allSimpleEMH) {
            if (emhs.contains(emh.getUiId())) {
                emhToDisplay.add(emh);
            }
        }
        DonLoiHYConteneur loiConteneur = scenario.getLoiConteneur();
        if (clone) {
            loiConteneur = loiConteneur.cloneWithoutPoints();
        }
        return new DonClimMTableModel(lines, emhToDisplay, loiConteneur);
    }

    public Pair<JScrollPane, DonClimMTableModel> createScrollPane(CrueConfigMetier ccm, EMHScenario scenario, boolean editable,
                                                                  final LoiDisplayer loiDisplayer, CtuluPopupListener.PopupReceiver rowPopupReceiver) {
        //on clone les dclm si editable
        DonClimMTableModel res = build(scenario, editable);
        res.setEditable(true);

        return createScrollPane(res, ccm, scenario, editable, loiDisplayer, rowPopupReceiver);
    }

    public Pair<JScrollPane, DonClimMTableModel> createScrollPane(DonClimMTableModel res, CrueConfigMetier ccm,
                                                                  EMHScenario scenario, boolean editable, final LoiDisplayer loiDisplayer,
                                                                  CtuluPopupListener.PopupReceiver rowPopupReceiver) {
        List<String> rows = new ArrayList<>();
        List<DonCLimMLineContent> dclims = res.getDclims();
        for (DonCLimMLineContent donCLimMLine : dclims) {
            rows.add(donCLimMLine.getCalc().getNom());
        }
        final DonCLimCellRenderer renderer = new DonCLimCellRenderer(ccm);
        final DonCLimCellEditor editor = editable ? new DonCLimCellEditor(ccm, res.getDonLoiHYConteneur(), loiDisplayer) : null;
        final JTable jTable = new JTable(res) {
            @Override
            public TableCellRenderer getCellRenderer(int row, int column) {
                return renderer;
            }

            @Override
            public TableCellEditor getCellEditor(int row, int column) {
                return editor;
            }
        };
        jTable.setRowHeight(TABLE_COLUMN_HEIGHT * res.getMaxDclmByEMH());
        jTable.setCellSelectionEnabled(true);
        //permet d'ouvrir l'éditeur de loi si double-clic sur la table.
        jTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() >= 2) {
                    int selectedColumn = jTable.getSelectedColumn();
                    int selectedRow = jTable.getSelectedRow();
                    if (selectedColumn >= 0 && selectedRow >= 0) {
                        DonCLimM climM = (DonCLimM) jTable.getValueAt(selectedRow, selectedColumn);
                        if (climM instanceof CalcTransItem) {
                            DonCLimCellRenderer cellRenderer = (DonCLimCellRenderer) jTable.getCellRenderer(selectedRow, selectedColumn);
                            cellRenderer.getTableCellRendererComponent(jTable, climM, true, true, selectedRow, selectedColumn);
                        }
                    }
                }
            }
        });
        jTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        final JScrollPane jscrollpane = TableRowHeaderInstaller.install(jTable, rows, "", rowPopupReceiver);
        TableColumnModel columnModel = jTable.getColumnModel();
        for (int i = 0; i < columnModel.getColumnCount(); i++) {
            columnModel.getColumn(i).setWidth(DonCLimCellRenderer.getCellRendererWidth());
            columnModel.getColumn(i).setPreferredWidth(DonCLimCellRenderer.getCellRendererWidth());
        }
        jscrollpane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        jscrollpane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        return new Pair(jscrollpane, res);
    }
}
