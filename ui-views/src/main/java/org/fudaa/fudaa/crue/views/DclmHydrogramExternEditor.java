package org.fudaa.fudaa.crue.views;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLib;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.CalcTransNoeudQappExt;
import org.fudaa.dodico.crue.metier.emh.DonLoiHYConteneur;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.fudaa.crue.common.swing.DialogPanelDescriptor;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.util.function.BooleanSupplier;

public class DclmHydrogramExternEditor {

  private final DonLoiHYConteneur loiConteneur;
  final CalcTransNoeudQappExt dclm;

  JTextField jtextNomFic;
  JTextField jtextSection;
  JTextField jtextCalc;

  public DclmHydrogramExternEditor(final CalcTransNoeudQappExt dclm, final DonLoiHYConteneur loiConteneur) {
    this.loiConteneur = loiConteneur;
    this.dclm = dclm;
  }



  public String getNomFic() {
    return jtextNomFic.getText();
  }
  public String getNomSection() {
    return jtextSection.getText();
  }
  public String getNomCalc() {
    return jtextCalc.getText();
  }

  public DialogPanelDescriptor create() {
    DialogPanelDescriptor p = new DialogPanelDescriptor(dclm.getTypeAndEMH());
    p.setBorder(BorderFactory.createEmptyBorder(5, 3, 3, 5));
    p.setLayout(new BuGridLayout(2, 5, 5));
    p.addLabel(BusinessMessages.getString("nomFic.property"));
    jtextNomFic = p.addStringText();

    p.addLabel(BusinessMessages.getString("calcTrans.property"));
    jtextCalc = p.addStringText();
    p.addLabel("");
    BuLabel label = p.addLabel(NbBundle.getMessage(DonCLimCellRenderer.class, "calcTrans.validation"));
    Font font = BuLib.deriveFont(label.getFont(), -2);
    label.setFont(font);

    p.addLabel(BusinessMessages.getString("section.property"));
    jtextSection = p.addStringText();
    p.addLabel("");
    p.addLabel(NbBundle.getMessage(DonCLimCellRenderer.class, "section.validation")).setFont(font);

    jtextNomFic.setText(dclm.getNomFic());
    jtextSection.setText(dclm.getSection());
    jtextCalc.setText(dclm.getResCalcTrans());

    BooleanSupplier validator = () -> StringUtils.isNotEmpty(jtextNomFic.getText())
        && StringUtils.isNotEmpty(jtextSection.getText())
        && jtextSection.getText().startsWith(CruePrefix.P_SECTION)
        && jtextSection.getText().length() > CruePrefix.P_SECTION.length()
        && (StringUtils.isBlank(jtextCalc.getText()) || (jtextCalc.getText().startsWith(CruePrefix.P_CALCUL) && jtextCalc.getText().length() > CruePrefix.P_CALCUL.length()));

    p.addTextListener(jtextNomFic, validator);
    p.addTextListener(jtextSection, validator);
    p.addTextListener(jtextCalc, validator);

    return p;
  }

  public void setEditable(boolean canWrite) {
    jtextCalc.setEditable(canWrite);
    jtextSection.setEditable(canWrite);
    jtextNomFic.setEditable(canWrite);
  }
}
