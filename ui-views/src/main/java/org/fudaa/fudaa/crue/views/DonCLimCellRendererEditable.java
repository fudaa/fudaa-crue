/*
 GPL 2
 */
package org.fudaa.fudaa.crue.views;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuGridLayout;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyValidator;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.DclmFactory;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.fudaa.crue.common.helper.ComboBoxHelper;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.helper.ToStringInternationalizableCellRenderer;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Frederic Deniger
 */
public final class DonCLimCellRendererEditable extends JPanel implements TableCellRenderer {

  protected static final String NAME_CB_LOIS = "CB_LOIS";
  protected static final String NAME_TF_VALUE = "TF_VALUE";
  protected static final String NAME_CB_TYPE = "CB_TYPE";
  protected static final String NAME_BUTTON_REGUL_TYPE = "CALCTRANSBRANCHEORIFICEMANOEUVREREGUL_TYPE";
  protected static final String NAME_BUTTON_HYDROGRAMME_EXTERN_TYPE = "CALCTRANSNOEUDQAPPEXT_TYPE";
  protected static final String CLIENT_PROPERTY_VALIDATOR = "VALIDATOR";
  protected static final String CLIENT_PROPERTY_CAN_BE_EDITABLE = "CAN_BE_EDITABLE";
  protected static final String CLIENT_PROPERTY_INDEX = "INDEX";

  private static void updateComponentsSize(JComponent jc, int width) {
    Dimension preferredSize = jc.getPreferredSize();
    preferredSize.width = width;
    jc.setPreferredSize(preferredSize);
    jc.setSize(preferredSize);
  }

  private final CrueConfigMetier ccm;
  private final DonLoiHYConteneur loiConteneur;
  private final LoiDisplayer loiDisplayer;
  public final String defaultTooltip;
  final ToStringInternationalizableCellRenderer renderer = new ToStringInternationalizableCellRenderer();

  public DonCLimCellRendererEditable(CrueConfigMetier ccm, DonLoiHYConteneur loiConteneur, LoiDisplayer loiDisplayer) {
    defaultTooltip = NbBundle.getMessage(DonCLimCellRendererEditable.class, "sensManoeuvre.Tooltip");
    this.ccm = ccm;
    this.loiDisplayer = loiDisplayer;
    this.loiConteneur = loiConteneur;

    setLayout(new BuGridLayout(3, 1, 0, true, true));
    setBorder(BuBorders.EMPTY0000);

  }

  public DonLoiHYConteneur getLoiConteneur() {
    return loiConteneur;
  }

  private JComboBox createCbType() {
    JComboBox cbType = new JComboBox();
    cbType.setName(NAME_CB_TYPE);
    cbType.setRenderer(renderer);
    return cbType;
  }

  private JTextField createTextFieldValue(final JLabel lbError) {
    final JTextField tfValue = new JTextField();
    tfValue.setName(NAME_TF_VALUE);
    tfValue.setEditable(false);
    tfValue.setHorizontalAlignment(JTextField.RIGHT);
    tfValue.getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void insertUpdate(DocumentEvent e) {
        updateValidation(tfValue, lbError);
      }

      @Override
      public void removeUpdate(DocumentEvent e) {
        updateValidation(tfValue, lbError);
      }

      @Override
      public void changedUpdate(DocumentEvent e) {
        updateValidation(tfValue, lbError);
      }
    });
    return tfValue;
  }

  private JComboBox createComboBoxLoi() {
    final JComboBox cbLois = new JComboBox();
    cbLois.setName(NAME_CB_LOIS);
    if (loiDisplayer != null) {
      cbLois.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
          if (e.getClickCount() >= 2 && cbLois.getSelectedItem() != null) {
            DonCLimCellRendererEditable.this.loiDisplayer.display((Loi) cbLois.getSelectedItem());
          }
        }
      });
    }
    cbLois.setRenderer(new ObjetNommeCellRenderer());
    return cbLois;
  }

  private JLabel createLabelError() {
    JLabel lbError = new JLabel();
    lbError.setText(" ");
    return lbError;
  }

  public void mouseDoubleClickedOnTable(MouseEvent evt) {
  }

  public CrueConfigMetier getCcm() {
    return ccm;
  }


  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    List<DonCLimM> climList = (List<DonCLimM>) value;
    removeAll();

    setToolTipText(null);
    if (climList != null) {
      StringBuilder builder = new StringBuilder();
      builder.append("<html><body>");
      int idx = 0;
      for (DonCLimM clim : climList) {
        if (idx > 0) {
          builder.append("<br>");
        }

        List<DclmFactory.CalcBuilder> creators = new ArrayList<>(DclmFactory.getCreatorsSens(clim.getCalculParent()));
        EMH emh = clim.getEmh();
        for (Iterator<DclmFactory.CalcBuilder> it = creators.iterator(); it.hasNext(); ) {
          DclmFactory.CalcBuilder calcBuilder = it.next();
          if (!calcBuilder.isAccepted(emh)) {
            it.remove();
          }
        }
        JComboBox cbType = createCbType();
        updateComponentsSize(cbType, DonCLimCellRenderer.COL_WIDTH_ONE);
        ComboBoxHelper.setDefaultModel(cbType, creators);
        DonCLimMCommonItem item = (DonCLimMCommonItem) clim;
        cbType.setSelectedItem(DclmFactory.findCreator(creators, clim));
        if (cbType.getSelectedItem() != null) {
          builder.append("<b>").append(DonCLimCellRenderer.MSG_TYPE).
              append("</b>");
          builder.append(cbType.getSelectedItem().toString()).append("<br>");
          cbType.setToolTipText(((ObjetNomme) cbType.getSelectedItem()).getNom());

        } else {
          cbType.setToolTipText(NbBundle.getMessage(DonCLimCellRendererEditable.class, "Type.Tooltip"));

        }
        JLabel lbError = createLabelError();
        updateComponentsSize(lbError, DonCLimCellRenderer.COL_WIDTH_ERROR);
        cbType.putClientProperty(CLIENT_PROPERTY_INDEX, idx);
        lbError.putClientProperty(CLIENT_PROPERTY_INDEX, idx);
        add(cbType);
        if (item instanceof DonCLimMWithManoeuvreRegulData) {
          DonCLimMWithManoeuvreRegulData regul = (DonCLimMWithManoeuvreRegulData) item;
          final JButton buttonRegul = new JButton();
          buttonRegul.setHorizontalTextPosition(SwingConstants.RIGHT);
          buttonRegul.setName(NAME_BUTTON_REGUL_TYPE);
          updateComponentsSize(buttonRegul, DonCLimCellRenderer.COL_WIDTH_TWO);
          add(buttonRegul);
          buttonRegul.putClientProperty(CLIENT_PROPERTY_INDEX, idx);
          add(lbError);
          buttonRegul.setText(regul.getDesc());
          builder.append(DonCLimCellRenderer.MSG_VALUE);
          builder.append(DonCLimCellRenderer.clean(regul.getDesc())).append("<br>");
        } else if (item instanceof CalcPseudoPermItem) {
          JTextField tfValue = createTextFieldValue(lbError);
          add(tfValue);
          tfValue.putClientProperty(CLIENT_PROPERTY_INDEX, idx);
          updateComponentsSize(tfValue, DonCLimCellRenderer.COL_WIDTH_TWO);
          add(lbError);
          decorePermanent(item, tfValue, lbError);
          builder.append(DonCLimCellRenderer.MSG_VALUE);
          builder.append(DonCLimCellRenderer.clean(tfValue.getToolTipText())).append("<br>");
        } else if (item instanceof CalcTransNoeudQappExt) {
          CalcTransNoeudQappExt hydroExtern = (CalcTransNoeudQappExt) item;
          final JButton buttonHydroExtern = new JButton();
          buttonHydroExtern.setHorizontalTextPosition(SwingConstants.RIGHT);
          buttonHydroExtern.setName(NAME_BUTTON_HYDROGRAMME_EXTERN_TYPE);
          updateComponentsSize(buttonHydroExtern, DonCLimCellRenderer.COL_WIDTH_TWO);
          add(buttonHydroExtern);
          buttonHydroExtern.putClientProperty(CLIENT_PROPERTY_INDEX, idx);
          add(lbError);
          buttonHydroExtern.setText(hydroExtern.getDesc());
          builder.append(DonCLimCellRenderer.MSG_VALUE);
          builder.append(DonCLimCellRenderer.clean(hydroExtern.getDesc())).append("<br>");
        } else {
          JComboBox cbLois = createComboBoxLoi();
          updateComponentsSize(cbLois, DonCLimCellRenderer.COL_WIDTH_TWO);
          add(cbLois);
          cbLois.putClientProperty(CLIENT_PROPERTY_INDEX, idx);
          add(lbError);
          decoreTransitoire(item, cbLois);
          builder.append(DonCLimCellRenderer.MSG_VALUE);
          builder.append(DonCLimCellRenderer.clean(cbLois.getToolTipText())).append("<br>");
        }

        if (StringUtils.isNotBlank(lbError.getToolTipText())) {
          builder.append(DonCLimCellRenderer.MSG_VALIDATION);
          builder.append(DonCLimCellRenderer.clean(lbError.getToolTipText()));
        }
        idx++;
      }
      setToolTipText(builder.toString());
    }
    return this;
  }

  public void decorePermanent(DonCLimMCommonItem item, JTextField tfValue, JLabel lbError) {
    CalcPseudoPermItem permItem = (CalcPseudoPermItem) item;
    tfValue.putClientProperty(CLIENT_PROPERTY_CAN_BE_EDITABLE, permItem.containValue());
    if (permItem.containValue()) {
      String ccmProp = permItem.getCcmVariableName();
      ItemVariable property = ccm.getProperty(ccmProp);
      final Double dclmValue = Double.valueOf(permItem.getValue());
      if (property != null) {
        PropertyValidator validator = property.getValidator();
        tfValue.putClientProperty(CLIENT_PROPERTY_VALIDATOR, validator);
        tfValue.setText(property.format(dclmValue, DecimalFormatEpsilonEnum.PRESENTATION));
        final String valueTltip = property.format(dclmValue, DecimalFormatEpsilonEnum.COMPARISON)
            + property.getNature().getUniteSuffixe();
        tfValue.setToolTipText(validator.getHtmlDesc(valueTltip));
        validate(dclmValue, validator, lbError);
      } else {
        tfValue.setText(dclmValue.toString());
        tfValue.setToolTipText(tfValue.getText());

      }
    } else {
      tfValue.setText(CtuluLibString.EMPTY_STRING);
      tfValue.setToolTipText(null);
    }

  }

  protected void updateValidation(JTextField tfValue, JLabel lbError) {
    PropertyValidator validator = (PropertyValidator) tfValue.getClientProperty(CLIENT_PROPERTY_VALIDATOR);
    if (validator != null) {
      lbError.setText(null);
      lbError.setToolTipText(null);
      lbError.setIcon(null);
      try {
        Double value = Double.parseDouble(tfValue.getText());
        validate(value, validator, lbError);
      } catch (NumberFormatException numberFormatException) {
        Logger.getLogger(DonCLimCellRendererEditable.class.getName()).log(Level.FINEST, "message {0}", numberFormatException);
      }
    }
  }

  public void decoreTransitoire(DonCLimMCommonItem item, JComboBox cbLois) {
    boolean editable = false;
    if (item instanceof CalcTransItem) {
      editable = true;
      decoreTransitoire(loiConteneur, (CalcTransItem) item, cbLois);
    }
    cbLois.putClientProperty(DonCLimCellRendererEditable.CLIENT_PROPERTY_CAN_BE_EDITABLE, editable);
  }

  public static void decoreTransitoire(DonLoiHYConteneur loiConteneur, DonCLimMWithLoi item, JComboBox cbLois) {
    DonCLimMWithLoi transItem = item;
    final Loi loiInit = transItem.getLoi();
    List<Loi> filterDLHY = LoiHelper.filterDLHY(loiConteneur, transItem.getTypeLoi());
    ComboBoxHelper.setDefaultModel(cbLois, filterDLHY);
    if (loiInit != null) {
      final Loi loi = LoiHelper.findByReference(loiInit.getNom(), filterDLHY);
      cbLois.setSelectedItem(loi);
    }
  }

  private void validate(final Double dclmValue, PropertyValidator validator, JLabel lbError) {
    lbError.setIcon(null);
    lbError.setToolTipText(null);
    if (validator == null) {
      return;
    }
    CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    validator.validateNumber(StringUtils.EMPTY, dclmValue, res, false);
    if (res.isNotEmpty()) {
      CtuluLogRecord record = res.getRecords().get(0);
      BusinessMessages.updateLocalizedMessage(record);
      lbError.setToolTipText(record.getLocalizedMessage());
      lbError.setIcon(LogIconTranslationProvider.getIcon(record.getLevel()));
    }
  }
}
