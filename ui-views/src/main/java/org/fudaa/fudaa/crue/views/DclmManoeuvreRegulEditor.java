package org.fudaa.fudaa.crue.views;

import com.memoire.bu.BuGridLayout;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.swing.DialogPanelDescriptor;

import javax.swing.*;
import java.util.function.BooleanSupplier;

public class DclmManoeuvreRegulEditor {

  private final DonLoiHYConteneur loiConteneur;
  final DonCLimMWithManoeuvreRegulData dclm;

  JComboBox cbLois;
  JTextField jtextParam;

  public DclmManoeuvreRegulEditor(final DonCLimMWithManoeuvreRegulData dclm, final DonLoiHYConteneur loiConteneur) {
    this.loiConteneur = loiConteneur;
    this.dclm = dclm;
  }


  public Loi getSelectedLoi() {
    return (Loi) cbLois.getSelectedItem();
  }

  public String getEditedParam() {
    return jtextParam.getText();
  }

  public DialogPanelDescriptor create() {
    cbLois = new JComboBox();
    cbLois.setRenderer(new ObjetNommeCellRenderer());
    DonCLimCellRendererEditable.decoreTransitoire(loiConteneur, dclm, cbLois);


    DialogPanelDescriptor p = new DialogPanelDescriptor(dclm.getTypeAndEMH());
    p.setBorder(BorderFactory.createEmptyBorder(5, 3, 3, 5));
    p.setLayout(new BuGridLayout(2, 5, 5));
    p.addLabel(BusinessMessages.getString("manoeuvreRegul.property"));
    p.add(cbLois);
    p.addLabel(BusinessMessages.getString("param.property"));
    jtextParam = p.addStringText();
    jtextParam.setText(dclm.getParam());
    BooleanSupplier validator = () -> StringUtils.isNotEmpty(jtextParam.getText()) && cbLois.getSelectedItem() != null;
    p.addTextListener(jtextParam, validator);
    p.addComboListner(cbLois, validator);

    return p;
  }

  public void setEditable(boolean canWrite) {
    cbLois.setEnabled(canWrite);
    jtextParam.setEditable(canWrite);
  }
}
