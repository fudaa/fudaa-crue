/*
 GPL 2
 */
package org.fudaa.fudaa.crue.views;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuGridLayout;

import java.awt.Component;
import java.awt.Dimension;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellRenderer;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.openide.util.NbBundle;

/**
 * @author Frederic Deniger
 */
public class DonCLimCellRenderer extends JPanel implements TableCellRenderer {

  public static final String MSG_TYPE = NbBundle.getMessage(DonCLimCellRenderer.class, "dclim.Type");
  public static final String MSG_VALUE = NbBundle.getMessage(DonCLimCellRenderer.class, "dclim.Value");
  public static final String MSG_VALIDATION = NbBundle.getMessage(DonCLimCellRenderer.class, "dclim.Validation");
  public static final String COMPONENT_VALUE_NAME = "dclmValue";
  final JLabel firstLabelType;
  final JTextField firstTextFieldValue;
  final JLabel firstLabelError;
  private final CrueConfigMetier ccm;
  protected static final int COL_WIDTH_ONE = 160;
  protected static final int COL_WIDTH_TWO = 140;
  protected static final int COL_WIDTH_ERROR = 20;
  final DonClimMTypeToString typeToString;

  public static int getCellRendererWidth() {
    return COL_WIDTH_ERROR + COL_WIDTH_ONE + COL_WIDTH_TWO + 3;
  }

  public DonCLimCellRenderer(CrueConfigMetier ccm) {
    typeToString = new DonClimMTypeToString(ccm);
    this.ccm = ccm;
    setLayout(new BuGridLayout(3, 1, 0, true, true));
    setBorder(BuBorders.EMPTY0000);
    firstLabelType = createLabelType();
    firstLabelError = createLabelError();
    firstTextFieldValue = createTfValue();
  }

  private JLabel createLabelType() {
    JLabel lbType = new JLabel();
    lbType.setBorder(BuBorders.EMPTY0200);
    return lbType;
  }

  private JTextField createTfValue() {
    JTextField tfValue = new JTextField();
    tfValue.setName(COMPONENT_VALUE_NAME);
    tfValue.setEditable(false);
    tfValue.setHorizontalAlignment(JTextField.RIGHT);
    return tfValue;
  }

  private JLabel createLabelError() {
    JLabel lbError = new JLabel();
    lbError.setText(" ");
    return lbError;
  }

  private static void updateComponentsSize(JComponent jc, int width) {
    Dimension preferredSize = jc.getPreferredSize();
    preferredSize.width = width;
    jc.setPreferredSize(preferredSize);
    jc.setSize(preferredSize);
  }

  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    List<DonCLimM> listOfClim = (List<DonCLimM>) value;
    removeAll();
    firstLabelType.setText(StringUtils.EMPTY);
    firstLabelError.setIcon(null);
    firstTextFieldValue.setText(StringUtils.EMPTY);
    firstLabelType.setToolTipText(null);
    firstLabelError.setToolTipText(null);
    firstTextFieldValue.setToolTipText(null);
    setToolTipText(null);

    updateComponentsSize(firstLabelType, COL_WIDTH_ONE);
    updateComponentsSize(firstTextFieldValue, COL_WIDTH_TWO);
    updateComponentsSize(firstLabelError, COL_WIDTH_ERROR);
    if (listOfClim != null) {
      StringBuilder builder = new StringBuilder();
      builder.append("<html><body>");
      for (int i = 0; i < listOfClim.size(); i++) {
        DonCLimMCommonItem item = (DonCLimMCommonItem) listOfClim.get(i);
        JLabel lbType = i == 0 ? firstLabelType : createLabelType();
        JLabel lbError = i == 0 ? firstLabelError : createLabelError();
        JTextField tfValue = i == 0 ? firstTextFieldValue : createTfValue();
        add(lbType);
        add(tfValue);
        add(lbError);
        lbType.setText(typeToString.getAsText(item));
        lbType.setToolTipText(lbType.getText());
        if (item instanceof DonCLimMWithManoeuvreRegulData) {
          decoreManoeuvreRegulData((DonCLimMWithManoeuvreRegulData) item, lbType, lbError, tfValue);
        }
        else if (item instanceof CalcPseudoPermItem) {
          decorePermanent(item, lbType, lbError, tfValue);
        } else {
          decoreTransitoire(item, lbType, lbError, tfValue);
        }
        if (i > 0) {
          builder.append("<br>");
        }
        builder.append("<b>").append(DonCLimCellRenderer.MSG_TYPE).
            append("</b>");
        builder.append(clean(lbType.getToolTipText())).append("<br>");
        builder.append(MSG_VALUE);
        builder.append(clean(tfValue.getToolTipText())).append("<br>");
        if (StringUtils.isNotBlank(lbError.getToolTipText())) {
          builder.append(MSG_VALIDATION);
          builder.append(clean(lbError.getToolTipText()));
        }
      }
      builder.append("</body></html>");
      setToolTipText(builder.toString());
    }
    return this;
  }

  protected static String clean(String in) {
    String res = StringUtils.remove(in, "<html><body>");
    res = StringUtils.remove(res, "</body></html>");
    return res;
  }

  public void decorePermanent(DonCLimMCommonItem item, JLabel lbType, JLabel lbError, JTextField tfValue) {
    CalcPseudoPermItem permItem = (CalcPseudoPermItem) item;
    if (permItem.containValue()) {
      String ccmProp = permItem.getCcmVariableName();
      ItemVariable property = ccm.getProperty(ccmProp);
      final Double dclmValue = Double.valueOf(permItem.getValue());
      if (property != null) {
        tfValue.setText(property.format(dclmValue, DecimalFormatEpsilonEnum.PRESENTATION));
        final String tooltip = property.format(dclmValue, DecimalFormatEpsilonEnum.COMPARISON)
            + property.getNature().getUniteSuffixe();
        tfValue.setToolTipText(property.getValidator().getHtmlDesc(tooltip));
        CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
        property.getValidator().validateNumber(StringUtils.EMPTY, dclmValue, res, false);
        if (res.isNotEmpty()) {
          CtuluLogRecord record = res.getRecords().get(0);
          BusinessMessages.updateLocalizedMessage(record);
          lbError.setToolTipText(record.getLocalizedMessage());
          lbError.setIcon(LogIconTranslationProvider.getIcon(record.getLevel()));
        }
      } else {
        tfValue.setText(dclmValue.toString());
        tfValue.setToolTipText(tfValue.getText());

      }
    } else {
      tfValue.setText(StringUtils.EMPTY);
      tfValue.setEditable(false);
    }

  }

  public void decoreManoeuvreRegulData(DonCLimMWithManoeuvreRegulData item, JLabel lbType, JLabel lbError, JTextField tfValue) {
    tfValue.setText(item.getDesc());
    tfValue.setToolTipText(tfValue.getText());
  }

  public void decoreTransitoire(DonCLimMCommonItem item, JLabel lbType, JLabel lbError, JTextField tfValue) {
    if (item instanceof CalcTransItem) {
      CalcTransItem permItem = (CalcTransItem) item;
      final Loi loi = permItem.getLoi();
      tfValue.setText(loi == null ? StringUtils.EMPTY : loi.getNom());
      tfValue.setToolTipText(tfValue.getText());
    } else if (item instanceof CalcTransNoeudQappExt) {
      CalcTransNoeudQappExt transNoeudQappExt = (CalcTransNoeudQappExt) item;
      tfValue.setText(transNoeudQappExt.getDesc());
      tfValue.setToolTipText(transNoeudQappExt.getDesc() + " " + transNoeudQappExt.getNomFic());
    } else {
      tfValue.setToolTipText(item.getTypei18n());
    }


  }
}
