/*
 GPL 2
 */
package org.fudaa.fudaa.crue.views.export;

import com.memoire.fu.FuEmptyArrays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jxl.write.Label;
import jxl.write.WritableCell;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.table.CtuluTableModelInterface;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.bean.DonCLimMLineContent;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPermItem;
import org.fudaa.dodico.crue.metier.emh.CalcTransItem;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;
import org.fudaa.dodico.crue.metier.emh.DonCLimMCommonItem;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.factory.DclmFactory;
import org.fudaa.fudaa.crue.views.DonClimMTableModel;
import org.fudaa.fudaa.crue.views.DonClimMTypeToString;
import org.openide.util.NbBundle;

/**
 * @author Frederic Deniger
 */
public class DonClimMExportTableModel implements CtuluTableModelInterface {

  private final DonClimMTableModel tableModel;
  private final DonClimMTypeToString typeToString;
  private final CrueConfigMetier ccm;
  private final List<Pair<DonCLimMLineContent, Integer>> lines = new ArrayList<>();

  public DonClimMExportTableModel(DonClimMTableModel tableModel, CrueConfigMetier ccm) {
    this.tableModel = tableModel;
    typeToString = new DonClimMTypeToString(ccm);
    this.ccm = ccm;
    //on créé les lignes
    final List<DonCLimMLineContent> dclims = tableModel.getDclims();
    for (DonCLimMLineContent dclim : dclims) {
      int nb = dclim.getMaxDclmByEMH();
      for (int i = 0; i < nb; i++) {
        lines.add(new Pair<>(dclim, i));
      }
    }
  }

  @Override
  public int getMaxRow() {
    return lines.size();
  }

  @Override
  public int getMaxCol() {
    return tableModel.getColumnCount() * 2 + 1;
  }

  @Override
  public int[] getSelectedRows() {
    return FuEmptyArrays.INT0;
  }

  @Override
  public List<String> getComments() {
    return Collections.emptyList();
  }

  @Override
  public String getColumnName(int _i) {
    if (_i == 0) {
      return NbBundle.getMessage(DonClimMExportTableModel.class, "exportClimMs.column.calcul");
    }
    int colIdx = _i - 1;
    //si typeDonnee
    //0=type de DclimM
    //1= sens manoeuve
    //2= valeur
    int typeDonnee = colIdx % 2;
    int emhIdx = (colIdx - typeDonnee) / 2;
    String emhName = tableModel.getEmhs().get(emhIdx).getNom();
    if (typeDonnee == 0) {
      return NbBundle.getMessage(DonClimMExportTableModel.class, "exportClimMs.column.type", emhName);
    }
    return NbBundle.getMessage(DonClimMExportTableModel.class, "exportClimMs.column.value", emhName);
  }

  @Override
  public String getValue(int _row, int _col) {
    if (_col == 0) {
      return lines.get(_row).first.getCalc().getNom();
    }
    int colIdx = _col - 1;
    //si typeDonnee
    //0=type de DclimM
    //1= sens manoeuve
    //2= valeur
    int typeDonnee = colIdx % 2;
    int emhIdx = (colIdx - typeDonnee) / 2;
    Long uid = tableModel.getEmhs().get(emhIdx).getUiId();
    final Pair<DonCLimMLineContent, Integer> dclmAndIdx = lines.get(_row);
    final List<DonCLimM> listOfValues = dclmAndIdx.first.getCLimM(uid);
    if (listOfValues == null || dclmAndIdx.second >= listOfValues.size()) {
      return StringUtils.EMPTY;
    }
    DonCLimM content = listOfValues.get(dclmAndIdx.second);
    if (content == null || DclmFactory.getNonExportableDclm().contains(content.getClass())) {
      return StringUtils.EMPTY;
    }
    boolean isPermanent = content instanceof CalcPseudoPermItem;
    if (typeDonnee == 0) {
      return typeToString.getAsText((DonCLimMCommonItem) content);
    }

    if (typeDonnee == 1) {
      if (isPermanent) {
        CalcPseudoPermItem permItem = (CalcPseudoPermItem) content;
        String ccmProp = permItem.getCcmVariableName();
        ItemVariable property = ccm.getProperty(ccmProp);
        final Double dclmValue = Double.valueOf(permItem.getValue());
        if (property != null) {
          return property.format(dclmValue, DecimalFormatEpsilonEnum.PRESENTATION);
        }
      } else if(content instanceof CalcTransItem){
        CalcTransItem permItem = (CalcTransItem) content;
        final Loi loi = permItem.getLoi();
        if (loi != null) {
          return loi.getNom();
        }
      }
    }
    return StringUtils.EMPTY;
  }

  @Override
  public WritableCell getExcelWritable(int _rowInModel, int _colInModel, int _rowInXls, int _colInXls) {
    String value = getValue(_rowInModel, _colInModel);
    return new Label(_colInXls, _rowInXls, value);
  }

}
