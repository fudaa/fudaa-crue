/*
 GPL 2
 */
package org.fudaa.fudaa.crue.views;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.function.Predicate;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableCellEditor;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLib;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluOptionPane;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.io.common.CrueHelper;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.DclmFactory;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.ComboBoxHelper;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.swing.DialogPanelDescriptor;
import org.fudaa.fudaa.crue.loader.EtudeAndScenarioChooser;
import org.openide.DialogDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

/**
 * @author Frederic Deniger
 */
public class DonCLimCellEditor extends AbstractCellEditor implements TableCellEditor {

  private final DonCLimCellRendererEditable cellRenderer;
  private boolean updating;
  private List<DonCLimM> value;
  private boolean cloned;

  public DonCLimCellEditor(CrueConfigMetier ccm, DonLoiHYConteneur loiConteneur, LoiDisplayer loiDisplayer) {
    cellRenderer = new DonCLimCellRendererEditable(ccm, loiConteneur, loiDisplayer);
  }

  private void updateListener(JComponent parent, Object value, int row, int column) {
    final Component[] components = parent.getComponents();
    for (final Component component : components) {
      if (DonCLimCellRendererEditable.NAME_CB_TYPE.equals(component.getName())) {
        final JComboBox cbType = (JComboBox) component;
        cbType.addItemListener(e -> {
          if (!updating && e.getStateChange() == ItemEvent.SELECTED) {
            typeChanged(cbType);
          }
        });

      }
      if (DonCLimCellRendererEditable.NAME_CB_LOIS.equals(component.getName())) {
        final JComboBox cbLoi = (JComboBox) component;
        cbLoi.setEditable(isComponentEditable(cbLoi));
        cbLoi.addItemListener(e -> {
          if (!updating && e.getStateChange() == ItemEvent.SELECTED) {
            loiChanged(cbLoi);
          }
        });
      } else if (DonCLimCellRendererEditable.NAME_BUTTON_HYDROGRAMME_EXTERN_TYPE.equals(component.getName())) {
        JButton bt = (JButton) component;
        bt.addActionListener(e -> {
          editHydrogramExtern((JComponent) component);
        });
      } else if (DonCLimCellRendererEditable.NAME_BUTTON_REGUL_TYPE.equals(component.getName())) {
        JButton bt = (JButton) component;
        bt.addActionListener(e -> {
          editOrificeManoeuvreRegul((JComponent) component);
        });
      } else if (DonCLimCellRendererEditable.NAME_TF_VALUE.equals(component.getName())) {
        final JTextField tfValue = (JTextField) component;
        tfValue.setEditable(isComponentEditable(tfValue));
        tfValue.getDocument().addDocumentListener(new DocumentListener() {
          @Override
          public void insertUpdate(DocumentEvent e) {
            valueChanged(tfValue);
          }

          @Override
          public void removeUpdate(DocumentEvent e) {
            valueChanged(tfValue);
          }

          @Override
          public void changedUpdate(DocumentEvent e) {
            valueChanged(tfValue);
          }
        });

      }

    }

  }

  private static boolean isComponentEditable(JComponent cbLoi) {
    return Boolean.TRUE.equals(cbLoi.getClientProperty(DonCLimCellRendererEditable.CLIENT_PROPERTY_CAN_BE_EDITABLE));
  }

  private void editOrificeManoeuvreRegul(JComponent component) {
    int idx = (Integer) component.getClientProperty(DonCLimCellRendererEditable.CLIENT_PROPERTY_INDEX);
    final DonCLimMWithManoeuvreRegulData dclm = (DonCLimMWithManoeuvreRegulData) this.value.get(idx);

    DclmManoeuvreRegulEditor editor = new DclmManoeuvreRegulEditor(dclm, cellRenderer.getLoiConteneur());

    DialogPanelDescriptor p = editor.create();
    boolean validated = DialogHelper.showQuestion(p.getDialogDescriptor(), "editDonCLimMWithManoeuvreRegulData", false);
    if (validated) {
      cloneIfNeeded();
      DonCLimMWithManoeuvreRegulData dclmAfterClone = (DonCLimMWithManoeuvreRegulData) this.value.get(idx);
      dclmAfterClone.setParam(editor.getEditedParam());
      dclmAfterClone.setLoi(editor.getSelectedLoi());
    }
    fireEditingStopped();
  }

  private void editHydrogramExtern(JComponent component) {
    int idx = (Integer) component.getClientProperty(DonCLimCellRendererEditable.CLIENT_PROPERTY_INDEX);
    final CalcTransNoeudQappExt dclm = (CalcTransNoeudQappExt) this.value.get(idx);


    DclmHydrogramExternEditor editor = new DclmHydrogramExternEditor(dclm, cellRenderer.getLoiConteneur());
    DialogPanelDescriptor p = editor.create();

    boolean validated = DialogHelper.showQuestion(p.getDialogDescriptor(), "editCalcTransNoeudQappExt", false);
    if (validated) {
      cloneIfNeeded();
      CalcTransNoeudQappExt dclmAfterClone = (CalcTransNoeudQappExt) this.value.get(idx);
      dclmAfterClone.setNomFic(editor.getNomFic());
      dclmAfterClone.setResCalcTrans(editor.getNomCalc());
      dclmAfterClone.setSection(editor.getNomSection());
    }
    fireEditingStopped();
  }

  protected void valueChanged(JTextField tfValue) {
    if (!isComponentEditable(tfValue)) {
      return;
    }
    if (updating) {
      return;
    }
    cloneIfNeeded();
    try {
      int idx = (Integer) tfValue.getClientProperty(DonCLimCellRendererEditable.CLIENT_PROPERTY_INDEX);
      double newValue = Double.parseDouble(tfValue.getText());
      ((CalcPseudoPermItem) value.get(idx)).setValue(newValue);
    } catch (NumberFormatException numberFormatException) {
    }
  }

  @Override
  public Object getCellEditorValue() {
    return value;
  }

  protected void loiChanged(JComboBox cbLoi) {
    cloneIfNeeded();
    int idx = (Integer) cbLoi.getClientProperty(DonCLimCellRendererEditable.CLIENT_PROPERTY_INDEX);
    DonCLimM dclm = value.get(idx);
    if (dclm instanceof CalcTransItem) {
      ((CalcTransItem) dclm).setLoi((Loi) cbLoi.getSelectedItem());
    }
    fireEditingStopped();
  }

  @Override
  public boolean isCellEditable(EventObject e) {
    if (e instanceof MouseEvent && ((MouseEvent) e).getClickCount() >= 2) {
      return false;
    }
    return super.isCellEditable(e);
  }

  protected void typeChanged(JComboBox cbType) {
    DclmFactory.CalcBuilder selectedItem = (DclmFactory.CalcBuilder) cbType.getSelectedItem();
    if (value == null) {
      return;
    }
    int idx = (Integer) cbType.getClientProperty(DonCLimCellRendererEditable.CLIENT_PROPERTY_INDEX);
    DonCLimM dclm = value.get(idx);
    Calc calculParent = dclm.getCalculParent();
    EMH emh = dclm.getEmh();
    dclm = selectedItem.create(cellRenderer.getCcm());
    dclm.setCalculParent(calculParent);
    dclm.setEmh(emh);
    value.set(idx, dclm);
    fireEditingStopped();
  }

  @Override
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
    this.value = new ArrayList((List<DonCLimM>) value);
    cloned = false;
    updating = true;
    final Component res = cellRenderer.getTableCellRendererComponent(table, value, isSelected, isSelected, row, column);
    updateListener((JComponent) res, value, row, column);
    updating = false;
    return res;
  }

  public void cloneIfNeeded() {
    if (!cloned && value != null) {
      cloned = true;
      try {
        for (int i = 0; i < value.size(); i++) {
          DonCLimM dclm = value.get(i);
          if (dclm != null) {
            value.set(i, ((DonCLimMCommonItem) dclm).clone());
          }
        }
      } catch (CloneNotSupportedException ex) {
        Exceptions.printStackTrace(ex);
      }
    }
  }
}
