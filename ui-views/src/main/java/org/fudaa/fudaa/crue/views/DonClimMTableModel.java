/*
 GPL 2
 */
package org.fudaa.fudaa.crue.views;

import gnu.trove.TLongObjectHashMap;
import gnu.trove.TLongObjectIterator;
import org.fudaa.dodico.crue.edition.bean.DonCLimMLineContent;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;
import org.fudaa.dodico.crue.metier.emh.DonLoiHYConteneur;
import org.fudaa.dodico.crue.metier.emh.EMH;

import javax.swing.table.AbstractTableModel;
import java.util.Collections;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public class DonClimMTableModel extends AbstractTableModel {
    private final DonLoiHYConteneur donLoiHYConteneur;
    private final List<DonCLimMLineContent> dclims;
    private final List<EMH> emhs;
    private boolean editable;

    /**
     * @param dclims            les contenuus des clm
     * @param emhList           la liste des EMHs
     * @param donLoiHYConteneur Attention peut être un clone du conteneur de lois
     */
    public DonClimMTableModel(List<DonCLimMLineContent> dclims, List<EMH> emhList, DonLoiHYConteneur donLoiHYConteneur) {
        this.dclims = Collections.unmodifiableList(dclims);
        this.emhs = Collections.unmodifiableList(emhList);
        this.donLoiHYConteneur = donLoiHYConteneur;
    }

    /**
     * @return le nombre max de dclm affecté à une EMH
     */
    public int getMaxDclmByEMH() {
        int nb = 1;
        for (DonCLimMLineContent dclim : dclims) {
            final TLongObjectHashMap<List<DonCLimM>> dclmsByEMHId = dclim.getDclmsByEMHId();
            for (TLongObjectIterator<List<DonCLimM>> it = dclmsByEMHId.iterator(); it.hasNext(); ) {
                it.advance();
                if (it.value() != null) {
                    nb = Math.max(nb, it.value().size());
                }
            }
        }
        return nb;
    }

    public DonLoiHYConteneur getDonLoiHYConteneur() {
        return donLoiHYConteneur;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return editable && getValueAt(rowIndex, columnIndex) != null;
    }

    @Override
    public int getRowCount() {
        return dclims.size();
    }

    @Override
    public int getColumnCount() {
        return emhs.size();
    }

    @Override
    public String getColumnName(int column) {
        return emhs.get(column).getNom();
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (rowIndex < 0 || columnIndex < 0) {
            return;
        }
        if (isEditable()) {
            List values = (List) aValue;
            boolean done = false;
            for (int i = 0; i < values.size(); i++) {
                done |= dclims.get(rowIndex).setCLimM(emhs.get(columnIndex).getUiId(), (DonCLimM) values.get(i), i);
            }
            if (done) {
                fireTableCellUpdated(rowIndex, columnIndex);
            }
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return dclims.get(rowIndex).getCLimM(emhs.get(columnIndex).getUiId());
    }

    public List<DonCLimMLineContent> getDclims() {
        return dclims;
    }

    public List<EMH> getEmhs() {
        return emhs;
    }
}
