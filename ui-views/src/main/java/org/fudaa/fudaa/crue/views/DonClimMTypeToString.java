/*
 GPL 2
 */
package org.fudaa.fudaa.crue.views;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.DclmFactory;

/**
 * @author Frederic Deniger
 */
public class DonClimMTypeToString {

  final Map<Class, DclmFactory.CalcBuilder> builderByClass;

  public DonClimMTypeToString(CrueConfigMetier ccm) {
    builderByClass = new HashMap<>();
    for (DclmFactory.CalcBuilder object : DclmFactory.getPseudoPerm()) {
      builderByClass.put(object.getDclmClass(), object);
    }
    for (DclmFactory.CalcBuilder object : DclmFactory.getTrans()) {
      builderByClass.put(object.getDclmClass(), object);
    }
  }

  public String getAsText(DonCLimMCommonItem item) {
    if (item == null) {
      return StringUtils.EMPTY;
    }
    String i18n = builderByClass.get(item.getClass()).geti18n();
    if (item instanceof DonCLimMWithSensOuv) {
      i18n = i18n + " " + ((DonCLimMWithSensOuv) item).getSensOuv().geti18n();
    }

    return i18n;
  }
}
