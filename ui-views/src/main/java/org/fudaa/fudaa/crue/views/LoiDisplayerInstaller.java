/*
 GPL 2
 */
package org.fudaa.fudaa.crue.views;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import org.fudaa.ctulu.gui.CtuluPopupListener;
import org.fudaa.ctulu.gui.CtuluPopupMenu;
import org.fudaa.dodico.crue.metier.emh.CalcTransItem;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class LoiDisplayerInstaller implements CtuluPopupListener.PopupReceiver {

  private final LoiDisplayer loiDisplayer;

  public LoiDisplayerInstaller(LoiDisplayer loiDisplayer) {
    this.loiDisplayer = loiDisplayer;
  }

  public void install(JScrollPane scrollpane) {
    final JTable table = (JTable) scrollpane.getViewport().getView();
    new CtuluPopupListener(this, table);
  }

  protected void fillWithCustomItems(JPopupMenu menu) {
  }

  @Override
  public void popup(MouseEvent _evt) {
    JTable table = (JTable) _evt.getSource();
    Point point = _evt.getPoint();
    int column = table.columnAtPoint(point);
    int row = table.rowAtPoint(point);
    final Object valueAt = table.getModel().getValueAt(row, column);
    CtuluPopupMenu menu = new CtuluPopupMenu();
    if (valueAt instanceof CalcTransItem) {
      final String loiName = ((CalcTransItem) valueAt).getLoi().getNom();
      menu.add(NbBundle.getMessage(LoiDisplayerInstaller.class, "DisplayLoi.ActionName", loiName)).addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          loiDisplayer.display(((CalcTransItem) valueAt).getLoi());
        }
      });
    }
    fillWithCustomItems(menu);
    if (menu.getComponentCount() > 0) {
      menu.show(table, _evt.getX(), _evt.getY());
    }
  }
}
