/*
 GPL 2
 */
package org.fudaa.fudaa.crue.views;

import org.fudaa.dodico.crue.metier.emh.Loi;

/**
 *
 * @author Frederic Deniger
 */
public interface LoiDisplayer {

  void display(Loi loi);
}
