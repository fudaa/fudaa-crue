package org.fudaa.fudaa.crue.uicommandline;

import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.windows.WindowManager;

import java.io.*;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class MultiNetbeansInstanceUtils {
  private static final String INSTANCE_LOCKER_FILENAME = "frame-title.txt";
  private static final String USER_DIR_PREFIX = "userDir";
  private static final String IS_COPY_PROPERTY = "thisInstanceIsACopy";

  static boolean isCopiedInstance() {
    return Boolean.TRUE.toString().equals(System.getProperty(IS_COPY_PROPERTY));
  }

  static void storedCopiedInstance() {
    System.setProperty(IS_COPY_PROPERTY, Boolean.TRUE.toString());
  }

  private static File createTempDir(final String prefix, File parent) throws IOException {
    return Files.createTempDirectory(parent.toPath(),prefix).toFile();
  }

  static List<File> listUserDir() {
    List<File> userDirs = new ArrayList<>();
    File f = getUserDir();
    File[] listFiles = f.listFiles();
    for (File file : listFiles) {
      if (isSubalternUserDir(file)) {
        userDirs.add(file);
      }
    }
    return userDirs;
  }

  private static boolean isSubalternUserDir(File file) {
    return file.isDirectory() && file.getName().startsWith(USER_DIR_PREFIX);
  }

  static File createNewUserDir() throws IOException {
    File userDir = getUserDir();
    File newUserDir = new File(userDir, USER_DIR_PREFIX + new SimpleDateFormat("yyyy-MM-dd-HH'h'mm'm'ss's'").format(new Date()));
    if (newUserDir.exists()) {
      newUserDir = createTempDir(USER_DIR_PREFIX, userDir);
    }
    newUserDir.mkdirs();
    //copy of the userDirFiles
    FileObject newUserDirFileObject = FileUtil.toFileObject(newUserDir);
    File[] listFiles = userDir.listFiles();
    for (File file : listFiles) {
      if (!isSubalternUserDir(file) && !"lock".equals(file.getName()) && !INSTANCE_LOCKER_FILENAME.equals(file.getName())) {
        FileObject source = FileUtil.toFileObject(file);
        source.copy(newUserDirFileObject, source.getName(), source.getExt());
      }
    }
    return newUserDir;
  }

  static final boolean isWindows() {
    return (System.getProperty("os.name").startsWith("Win"));
  }

  static File getUserDir() {
    final FileObject configRoot = FileUtil.getConfigRoot();
    return FileUtil.toFile(configRoot).getParentFile();
  }

  static File getFrameName() {
    return getFrameName(getUserDir());
  }

  static File getFrameName(File userDir) {
    return new File(userDir, INSTANCE_LOCKER_FILENAME);
  }

  static String getMainWindowTitle() {
    return WindowManager.getDefault().getMainWindow().getTitle();
  }

  static String readFrameName(File frameTitleFile) {
    String res = null;

    try (FileReader reader = new FileReader(frameTitleFile)) {
      BufferedReader buffReadder = new BufferedReader(reader);
      res = buffReadder.readLine();
    } catch (IOException ex) {
      Logger.getLogger(MultiNetbeansInstanceUtils.class.getName()).log(Level.SEVERE, null, ex);
    }
    return res;
  }

  static void cleanUserDirs() {
    List<File> userDirOpened = MultiNetbeansInstanceUtils.listUserDir();
    for (File userDir : userDirOpened) {
      File frameName = MultiNetbeansInstanceUtils.getFrameName(userDir);
      if (!frameName.exists()) {
        try {
          FileUtil.toFileObject(userDir).delete();
        } catch (IOException ex) {
          Exceptions.printStackTrace(ex);
        }
      }
    }
  }

  static synchronized void storeFrameName() {
    File f = getFrameName();
    try (FileWriter w = new FileWriter(f, false)) {
      final String mainWindowTitle = getMainWindowTitle();
      w.append(mainWindowTitle);
    } catch (IOException ex) {
      Exceptions.printStackTrace(ex);
      Logger.getLogger(MultiNetbeansInstanceUtils.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
}
