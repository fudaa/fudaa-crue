package org.fudaa.fudaa.crue.uicommandline;

import java.io.File;

/**
 *
 * @author deniger
 */
class OpenedNetbeansInstance implements Comparable<OpenedNetbeansInstance> {
  private final File userDir;
  private final String frameName;
  private final boolean current;

  public OpenedNetbeansInstance(File userDir, String frameName, boolean current) {
    this.userDir = userDir;
    this.frameName = frameName;
    this.current = current;
  }

  public String getFrameName() {
    return frameName;
  }

  public File getUserDir() {
    return userDir;
  }

  public boolean isCurrent() {
    return current;
  }

  @Override
  public int compareTo(OpenedNetbeansInstance o) {
    if (o == null) {
      return 1;
    }
    if (o == this) {
      return 0;
    }
    int idx = frameName.compareTo(o.frameName);
    if (idx == 0) {
      idx = userDir.compareTo(o.userDir);
    }
    return idx;
  }

}
