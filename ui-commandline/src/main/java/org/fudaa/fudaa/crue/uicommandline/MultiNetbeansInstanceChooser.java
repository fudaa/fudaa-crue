package org.fudaa.fudaa.crue.uicommandline;

import java.awt.Component;
import java.util.List;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class MultiNetbeansInstanceChooser {

  public static class Result {

    private final static int CANCEL = 0;
    private final static int ACTIVE = 1;
    private final static int NEW = 2;
    private final int action;
    private final OpenedNetbeansInstance instance;

    public OpenedNetbeansInstance getInstance() {
      return instance;
    }

    public Result(int action, OpenedNetbeansInstance instance) {
      this.action = action;
      this.instance = instance;
    }

    public static Result createCancel() {
      return new Result(CANCEL, null);
    }

    public static Result createNewInstance() {
      return new Result(NEW, null);
    }

    public static Result createActivate(OpenedNetbeansInstance instance) {
      if (instance == null) {
        return createCancel();
      }
      return new Result(ACTIVE, instance);
    }

    public boolean isCancel() {
      return CANCEL == action;
    }

    public boolean isCreateNew() {
      return NEW == action;
    }

    public boolean isActivateExisting() {
      return ACTIVE == action;
    }
  }

  private static class InstanceRenderer extends DefaultListCellRenderer {

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
      Object newValue = value;
      if (value != null) {
        newValue = ((OpenedNetbeansInstance) value).getFrameName();
      }
      return super.getListCellRendererComponent(list, newValue, index, isSelected, cellHasFocus);
    }
  }

  public static Result choose(List<OpenedNetbeansInstance> in) {

    final JList list = new JList(in.toArray());
    list.setCellRenderer(new InstanceRenderer());
    list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);


    String title = NbBundle.getMessage(MultiNetbeansInstanceChooser.class, "ChooseInstance.DialogTitle");
    String optionActive = NbBundle.getMessage(MultiNetbeansInstanceChooser.class, "ChooseInstance.ActiveSelected");
    String optionNewInstance = NbBundle.getMessage(MultiNetbeansInstanceChooser.class, "ChooseInstance.CreateNewInstance");
    String cancel = NbBundle.getMessage(MultiNetbeansInstanceChooser.class, "ChooseInstance.Cancel");
    final NotifyDescriptor nd = new NotifyDescriptor.Confirmation(new JScrollPane(list), title);
    nd.setValid(false);
    list.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

      @Override
      public void valueChanged(ListSelectionEvent e) {
        nd.setValid(!list.getSelectionModel().isSelectionEmpty());
      }
    });

    nd.setOptions(new Object[]{optionActive, optionNewInstance, cancel});
    Object showQuestion = DialogDisplayer.getDefault().notify(nd);
    if (showQuestion.equals(optionNewInstance)) {
      return Result.createNewInstance();
    } else if (showQuestion.equals(optionActive)) {
      return Result.createActivate((OpenedNetbeansInstance) list.getSelectedValue());
    }

    return Result.createCancel();

  }
}
