package org.fudaa.fudaa.crue.uicommandline;

import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;
import java.util.*;
import org.fudaa.fudaa.crue.uicommandline.MultiNetbeansInstanceChooser.Result;
import org.netbeans.api.sendopts.CommandException;
import org.netbeans.spi.sendopts.Env;
import org.netbeans.spi.sendopts.Option;
import org.netbeans.spi.sendopts.OptionProcessor;
import org.openide.util.Exceptions;
import org.openide.util.lookup.ServiceProvider;
import org.openide.windows.WindowManager;

/**
 * Ne contient pas de lookup. Un service utilise par Netbeans RCP pour traiter les arguements de lancement de l'application.
 * Dans le cas de Fudaa-Crue permet de gérer le multi-instance de Fudaa-Crue: demnader de remettre en avant-plan l'application Fudaa-Crue ou ouvrir une nouvelle
 * instance qui ne sera pas persistée.
 *
 * @author deniger
 */
@ServiceProvider(service = OptionProcessor.class)
public class ArgumentLineAnalyser extends OptionProcessor {

  /**
   * Clé de l'option pour remettre en avant-plan l'application Fudaa-Crue ( l'utilisateur clique plusieurs fois sur l'icone)
   */
  private static final String TO_FRONT_OPTION = "toFront";
  /**
   * Clé de l'option lancer une nouvelle instance qui contiendra une copie temporaire des fichiers de configuration de Nebeans RCP.
   */
  private static final String IS_COPY_OPTION = "isCopy";
  /**
   * ouverture multi-instance
   */
  private final Option openMultiInstances = Option.withoutArgument(Option.NO_SHORT_NAME, "multi-instances");
  /**
   * Reactiver la fenetre Fudaa-Crue
   */
  private final Option toFront = Option.withoutArgument(Option.NO_SHORT_NAME, TO_FRONT_OPTION);
  /**
   * nouvelle instance avec copie des fichiers de configuration.
   */
  private final Option isCopy = Option.withoutArgument(Option.NO_SHORT_NAME, IS_COPY_OPTION);

  /**
   * Un cas de lancements mutliples est detecté: gestion de ces cas de figure
   */
  private void activateOrCreateNewInstance() {
    //propose un choix à u l'utilisateur
    final MultiNetbeansInstanceChooser.Result res = MultiNetbeansInstanceChooser.choose(getOpenedInstances(true));
    //il veut créer une nouvelle instance
    if (res.isCreateNew()) {
      launchNewInstance();
      //il veut activer une instance existante
    } else if (res.isActivateExisting()) {
      final OpenedNetbeansInstance instance = res.getInstance();
      //c'est l'instance en cours: simple on bouge met le focus sur la fenetre principale
      if (instance.isCurrent()) {
        moveMainWindowToFront();
      } else {
        //sinon on relance Fudaa-crue.exe dans le bon dossier userDir en demande d'activer l'autre instance...
        activateInstance(instance.getUserDir());
      }
    }
  }

  private void firstInstanceLaunched() {
    //for the main instance we clean subaltern userDir and we test if other instances already launched
    if (!MultiNetbeansInstanceUtils.isCopiedInstance()) {
      //first call of the main window
      //we clean old userDir
      MultiNetbeansInstanceUtils.cleanUserDirs();
      //if not null, the main instance has been closed before subalterns.
      final List<OpenedNetbeansInstance> openedInstances = getOpenedInstances(false);
      //what can we do ?
      if (!openedInstances.isEmpty()) {
        final Result choose = MultiNetbeansInstanceChooser.choose(openedInstances);
        if (choose.isActivateExisting()) {
          activateInstance(choose.getInstance().getUserDir());
          System.exit(0);
        } else if (choose.isCancel()) {
          System.exit(0);
        }
      }
    }
  }

  /**
   * Une instance est déjà lancée: il faut demander à l'utilisateur ce qu'il veut faire.
   */
  private void instanceAlreadyLaunched() {
    EventQueue.invokeLater(() -> activateOrCreateNewInstance());
  }

  /**
   * Création d'une nouvelle instance en copiant les fichiers de configuration userDir.
   */
  private void launchNewInstance() {
    try {
      final File newUserDir = MultiNetbeansInstanceUtils.createNewUserDir();
      launchAppli(newUserDir, "--" + IS_COPY_OPTION);
    } catch (final IOException ex) {
      Exceptions.printStackTrace(ex);
    }
  }

  /**
   *
   * @param userDir permet d'identifier l'instance à activer
   */
  private void activateInstance(final File userDir) {
    launchAppli(userDir, "--" + TO_FRONT_OPTION);

  }

  /**
   * Lancement de Fudaa-Crue dans le userDir fourni et avec l'option donnée
   *
   * @param newUserDir le userDir a utiliser
   * @param option     l'option
   */
  private void launchAppli(final File newUserDir, final String option) {
    final File platformDir = new File(System.getProperty("netbeans.home"));
    final File binDir = new File(platformDir.getParentFile(), "bin");
    //on relance l'exe avec la bonne option:
    final File exe = new File(binDir, MultiNetbeansInstanceUtils.isWindows() ? "fudaacrue.exe" : "fudaacrue");
    final ProcessBuilder builder = new ProcessBuilder(exe.getAbsolutePath(), "--userdir", newUserDir.getAbsolutePath(), option);
    try {
      builder.start();
    } catch (final IOException ex) {
      Exceptions.printStackTrace(ex);
    }
  }

  /**
   * Envoie la fenetre de l'application courante en avant-plan.
   */
  private void moveMainWindowToFront() {
    EventQueue.invokeLater(() -> WindowManager.getDefault().getMainWindow().toFront());
  }

  /**
   *
   * @return les 3 options: multi-instances, toFront et nouvelle instance.
   */
  @Override
  protected Set<Option> getOptions() {
    final HashSet set = new HashSet();
    set.add(openMultiInstances);
    set.add(toFront);
    set.add(isCopy);
    return set;
  }

  /**
   *
   * @param includeCurrent si true, l'instance courante doit être ajouté à la liste
   * @return liste des instances de Netbeans ouvertes.
   */
  private List<OpenedNetbeansInstance> getOpenedInstances(final boolean includeCurrent) {
    //le résultats
    final List<OpenedNetbeansInstance> openedIstance = new ArrayList<>();
    if (includeCurrent) {
      final OpenedNetbeansInstance thisInstance = new OpenedNetbeansInstance(MultiNetbeansInstanceUtils.getUserDir(), MultiNetbeansInstanceUtils.getMainWindowTitle(), true);
      openedIstance.add(thisInstance);
    }
    //on parcourt la listes des instances ouvertes:
    final List<File> userDirOpened = MultiNetbeansInstanceUtils.listUserDir();
    for (final File userDir : userDirOpened) {
      final File frameName = MultiNetbeansInstanceUtils.getFrameName(userDir);
      if (frameName.exists()) {
        final String frame = MultiNetbeansInstanceUtils.readFrameName(frameName);
        if (frame != null) {
          openedIstance.add(new OpenedNetbeansInstance(userDir, frame, false));
        }
      }
    }
    Collections.sort(openedIstance);
    return openedIstance;
  }

  /**
   * traitement des arguments de l'application pour réactiver une instance (toFront) ou créer une nouvelle instance.
   *
   * @param env  non utilisé
   * @param maps les options passées en parametres
   * @throws CommandException exception si commande non connue
   */
  @Override
  protected void process(final Env env, final Map<Option, String[]> maps) throws CommandException {
    //for copied instances we just move to front.
    if (MultiNetbeansInstanceUtils.isCopiedInstance() || maps.containsKey(toFront)) {
      moveMainWindowToFront();
      return;
    }
    if (maps.containsKey(isCopy)) {
      MultiNetbeansInstanceUtils.storedCopiedInstance();
    }
    if (MultiNetbeansInstanceUtils.getFrameName().exists()) {
      instanceAlreadyLaunched();
    } else {
      firstInstanceLaunched();
    }
  }
}
