package org.fudaa.fudaa.crue.uicommandline;

import org.openide.modules.ModuleInstall;
import org.openide.windows.WindowManager;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
@SuppressWarnings("unused")
public class MultiNetbeansInstanceInstaller extends ModuleInstall implements Runnable, PropertyChangeListener {
  @Override
  public void close() {
    File frameName = MultiNetbeansInstanceUtils.getFrameName();
    if (frameName.exists()) {
      try {
        Files.delete(frameName.toPath());
      } catch (IOException e) {
        Logger.getLogger(getClass().getName()).log(Level.WARNING, "can't delete file " + frameName.getAbsolutePath(), e);
      }
    }
    //we clean for the main instance only:
    if (!MultiNetbeansInstanceUtils.isCopiedInstance()) {
      MultiNetbeansInstanceUtils.cleanUserDirs();
    }
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    MultiNetbeansInstanceUtils.storeFrameName();
  }

  @Override
  public void restored() {
    WindowManager.getDefault().invokeWhenUIReady(this);
  }

  @Override
  public void run() {
    MultiNetbeansInstanceUtils.storeFrameName();
    WindowManager.getDefault().getMainWindow().addPropertyChangeListener("title", this);
  }
}
