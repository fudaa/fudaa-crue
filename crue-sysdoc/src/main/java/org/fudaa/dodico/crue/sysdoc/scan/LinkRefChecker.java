/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.scan;

import java.io.File;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FilenameUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.dodico.crue.sysdoc.common.SysdocMessages;
import org.fudaa.dodico.crue.sysdoc.scan.LinkAndSourceProcessor.Result;

/**
 * Verifie que tous les liens des fichiers html d'un dossier sont bien présent et inclus dans le dossier.
 *
 * @author Frederic Deniger
 */
public class LinkRefChecker {

  private final File basedir;

  public LinkRefChecker(File basedir) {
    this.basedir = basedir;
  }

  List<File> getHtmlFiles() {
    FolderProcessor folderProcessor = new FolderProcessor();
    DocScanEntry process = folderProcessor.process(basedir);
    List<File> htmlFiles = new ArrayList<>();
    fillListWithHtmlFiles(process, htmlFiles);
    return htmlFiles;
  }

  public CtuluLogResult<CtuluLogGroup> checkLink() {

    CtuluLogResult<CtuluLogGroup> res = new CtuluLogResult<>();

    CtuluLogGroup group = new CtuluLogGroup(SysdocMessages.RESOURCE_BUNDLE);
    group.setDescription("brokenLinksOverview");

    CtuluLog log = new CtuluLog(SysdocMessages.RESOURCE_BUNDLE);
    log.setDesc("logCheckLinksProcess");

    res.setLog(log);
    res.setResultat(group);

    List<File> htmlFiles = getHtmlFiles();
    Collections.sort(htmlFiles);
    List<LinkFinder> linkedFinders = new ArrayList<>();
    for (File htmlFile : htmlFiles) {
      linkedFinders.add(new LinkFinder(htmlFile, basedir));
    }
    ExecutorService executor = null;
    try {
      executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
      List<Future<CtuluLogResult<CtuluLog>>> invokeAll = executor.invokeAll(linkedFinders);
      for (Future<CtuluLogResult<CtuluLog>> future : invokeAll) {
        CtuluLogResult<CtuluLog> logForLink = future.get();
        if (logForLink.getResultat().isNotEmpty()) {
          group.addLog(logForLink.getResultat());
        }
        if (logForLink.getLog().isNotEmpty()) {
          log.addAllLogRecord(logForLink.getLog());
        }
      }
    } catch (InterruptedException ex) {
      Logger.getLogger(LinkRefChecker.class.getName()).log(Level.INFO, "message {0}", ex);
      log.addSevereError("checkLinksInterrupted");
    } catch (ExecutionException e) {
      Logger.getLogger(LinkRefChecker.class.getName()).log(Level.INFO, "message {0}", e);
      log.addSevereError("checkLinksTaskAborted", e.getMessage());
    } finally {
      if (executor != null) {
        executor.shutdownNow();
      }
    }
    return res;
  }

  private void fillListWithHtmlFiles(DocScanEntry entry, List<File> target) {
    if (entry.getHtmlFile() != null) {
      target.add(entry.getHtmlFile());
    }
    if (!entry.isLeaf()) {
      List<DocScanEntry> children = entry.getChildren();
      for (DocScanEntry child : children) {
        fillListWithHtmlFiles(child, target);
      }
    }
  }

  /**
   * Le callable effectuant le test.
   */
  private static class LinkFinder implements Callable<CtuluLogResult<CtuluLog>> {

    private final File htmlFile;
    private final File baseDir;

    public LinkFinder(File htmlFile, File baseDir) {
      this.htmlFile = htmlFile;
      this.baseDir = baseDir;
    }

    @Override
    public CtuluLogResult<CtuluLog> call() throws Exception {
      CtuluLog res = new CtuluLog(SysdocMessages.RESOURCE_BUNDLE);
      CtuluLog log = new CtuluLog(SysdocMessages.RESOURCE_BUNDLE);
      LinkAndSourceProcessor processor = new LinkAndSourceProcessor();
      final Result allHrefAndSourceLinks = processor.getAllHrefAndSourceLinks(htmlFile, log);
      res.setDesc(htmlFile.getCanonicalPath() + " (" + allHrefAndSourceLinks.getTitle() + ")");
      res.setResource(htmlFile.getCanonicalPath());
      Set<String> links = new LinkedHashSet<>(allHrefAndSourceLinks.getRefs());
      File dir = htmlFile.getParentFile();
      final String baseDirCanonicalPath = baseDir.getCanonicalPath();
      boolean signetInfoAdded = false;
      for (String link : links) {
        //pas http: ftp:,...
        if (link.indexOf(':') < 0) {
          final int indexOfSignet = link.indexOf("#");
          if (indexOfSignet >= 0 && !signetInfoAdded) {
            signetInfoAdded = true;
            res.addInfo("anchorNotTested");
          }
          if (indexOfSignet == 0) {
            continue;
          } else if (indexOfSignet > 0) {
            link = link.substring(0, indexOfSignet);
          }
          String decode = URLDecoder.decode(link, allHrefAndSourceLinks.getEncoding());
          String targetPath = new File(dir, decode).getAbsolutePath();
          targetPath = FilenameUtils.separatorsToSystem(targetPath);
          targetPath = FilenameUtils.normalize(targetPath);
          File target = new File(targetPath).getAbsoluteFile();
          if (!target.exists()) {
            res.addError("linkNotFound", link);
          } else if (!FilenameUtils.directoryContains(baseDirCanonicalPath, target.getCanonicalPath())) {
            res.addWarn("linkNotInBaseDir", link);
          }
        }
      }
      return new CtuluLogResult<>(res, log);
    }
  }
}
