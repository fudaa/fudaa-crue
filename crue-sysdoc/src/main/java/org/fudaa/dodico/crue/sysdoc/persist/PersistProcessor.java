/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.persist;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.naming.NameCoder;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.xml.UnicodeInputStream;
import org.fudaa.ctulu.xml.XmlVersionFinder;
import org.fudaa.dodico.crue.sysdoc.common.DocEntry;
import org.fudaa.dodico.crue.sysdoc.common.SysdocMessages;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Frederic Deniger
 */
public class PersistProcessor {
  final String version = "1.0";

  public CtuluLog write(File file, DocEntry entry) {
    CtuluLog log = new CtuluLog(SysdocMessages.RESOURCE_BUNDLE);
    PersistContent content = new PersistContent();
    content.setXsdName("sydoc-" + version + ".xsd");
    content.setEntry(entry);
    try (FileOutputStream out = new FileOutputStream(file)) {
      final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, XmlVersionFinder.ENCODING), 8192 * 4);
      writer.write(XmlVersionFinder.ENTETE_XML + CtuluLibString.LINE_SEP);
      final XStream parser = createXstream();
      parser.marshal(content, new CustomPrettyPrintWriter(writer, new char[]{' ', ' '},
          createNameCoder()));
    } catch (final IOException e) {
      log.addSevereError("file.write.error");
      Logger.getLogger(PersistProcessor.class.getName()).log(Level.SEVERE, "message {0}", e);
    }
    return log;
  }

  public CtuluIOResult<PersistContent> read(File file) {
    CtuluLog log = new CtuluLog(SysdocMessages.RESOURCE_BUNDLE);

    PersistContent content = null;
    try (FileInputStream in = new FileInputStream(file)) {
      // The unicodeInputStream is used to eliminate the BOM bug from java:
      UnicodeInputStream unicodeStream = new UnicodeInputStream(in, XmlVersionFinder.ENCODING);
      unicodeStream.init();
      try (BufferedReader contentRead = new BufferedReader(new InputStreamReader(unicodeStream, XmlVersionFinder.ENCODING))) {
        content = (PersistContent) createXstream().fromXML(contentRead);// we not that is a D object.
      }
    } catch (final Exception e) {
      log.addSevereError("file.read.error");
      Logger.getLogger(PersistProcessor.class.getName()).log(Level.SEVERE, "message {0}", e);
    }
    CtuluIOResult<PersistContent> res = new CtuluIOResult<>();
    res.setAnalyze(log);
    res.setSource(content);
    return res;
  }

  private XStream createXstream() {
    final StaxDriver staxDriver = new StaxDriver(createNameCoder());
    final XStream xstream = new XStream(staxDriver);
    XStream.setupDefaultSecurity(xstream);
    xstream.allowTypesByWildcard(new String[] {
      "org.fudaa.**"
    });
    xstream.setMode(XStream.NO_REFERENCES);
    // -- creation des alias pour que ce soit + parlant dans le xml file --//
    // -- alias pour les entete xsd --//
    xstream.aliasAttribute("xmlns:xsi", "xmlnsxsi");
    xstream.aliasAttribute("xsi:schemaLocation", "xsischemaLocation");
    xstream.useAttributeFor(PersistContent.class, "xmlns");
    xstream.useAttributeFor(PersistContent.class, "xmlnsxsi");
    xstream.useAttributeFor(PersistContent.class, "xsischemaLocation");
    xstream.processAnnotations(PersistContent.class);
    xstream.processAnnotations(DocEntry.class);
    return xstream;
  }

  private XmlFriendlyNameCoder createNameCoder() {
    return new XmlFriendlyNameCoder("#", "_");
  }

  protected class CustomPrettyPrintWriter extends PrettyPrintWriter {
    public CustomPrettyPrintWriter(Writer writer, char[] lineIndenter, NameCoder nameCoder) {
      super(writer, XML_QUIRKS, lineIndenter, nameCoder);
    }

    @Override
    protected String getNewLine() {
      return CtuluLibString.LINE_SEP;
    }
  }
}
