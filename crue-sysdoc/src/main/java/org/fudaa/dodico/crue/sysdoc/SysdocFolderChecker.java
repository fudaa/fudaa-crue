/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc;

import java.io.File;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.sysdoc.common.SysdocFolder;
import org.fudaa.dodico.crue.sysdoc.common.SysdocMessages;

/**
 * Permet de vérifier que les dossier de base
 *
 * @author Frederic Deniger
 */
public class SysdocFolderChecker {

  private final File baseDir;

  public SysdocFolderChecker(File baseDir) {
    this.baseDir = baseDir;
  }

  /**
   *
   * @return le log non null. Contien des erreurs severe si les dossier de sysdoc ne peuvent être créés.
   */
  public CtuluLog checkSysdocFolder() {
    CtuluLog log = new CtuluLog(SysdocMessages.RESOURCE_BUNDLE);
    if (!baseDir.exists()) {
      boolean created = baseDir.mkdirs();
      if (!created) {
        log.addSevereError("RebuildToc.CantCreateBaseFolder.Error");
      }
    }
    SysdocFolder folder = new SysdocFolder(baseDir);
    File sysdocDir = folder.getSysdocDir();
    if (!sysdocDir.exists()) {
      boolean created = sysdocDir.mkdirs();
      if (!created) {
        log.addSevereError("RebuildToc.CantCreateSysdocFolder.Error");
      }
    }
    return log;
  }
}
