/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("DocItem")
public class DocEntry {

  @XStreamAlias("Path")
  private String path;
  @XStreamAlias("Title")
  private String title;
  @XStreamAlias("Description")
  private String description;
  @XStreamAlias("Folder")
  private boolean folder;
  @XStreamImplicit(itemFieldName = "SubDocItem")
  private List<DocEntry> children = new ArrayList<>();

  public String getPath() {
    return path;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public boolean isFolder() {
    return folder;
  }

  public void setFolder(boolean folder) {
    this.folder = folder;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public List<DocEntry> getChildren() {
    return children == null ? Collections.emptyList() : children;
  }

  public void addChild(DocEntry entry) {
    children.add(entry);
  }

  public void setChildren(List<DocEntry> children) {
    this.children = children;
  }
}
