/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc;

import java.io.File;
import java.io.IOException;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.sysdoc.common.DocEntry;
import org.fudaa.dodico.crue.sysdoc.common.DocEntryBuilder;
import org.fudaa.dodico.crue.sysdoc.common.SysdocFolder;
import org.fudaa.dodico.crue.sysdoc.common.SysdocMessages;
import org.fudaa.dodico.crue.sysdoc.persist.PersistProcessor;
import org.fudaa.dodico.crue.sysdoc.scan.DocScanEntry;
import org.fudaa.dodico.crue.sysdoc.scan.FolderProcessor;
import org.fudaa.dodico.crue.sysdoc.scan.TitleProcessor;

/**
 *
 * @author Frederic Deniger
 */
public class SysdocBuilder {

  private final File baseDir;

  public SysdocBuilder(File baseDir) {
    this.baseDir = baseDir;
  }

  public CtuluLogGroup build() {
    SysdocFolderChecker folderChecker = new SysdocFolderChecker(baseDir);
    CtuluLogGroup group = new CtuluLogGroup(SysdocMessages.RESOURCE_BUNDLE);
    CtuluLog log = folderChecker.checkSysdocFolder();
    log.setDesc("RebuildToc.Log");
    group.addLog(log);
    if (log.containsSevereError()) {
      return group;
    }
    SysdocFolder sysdocFolder = new SysdocFolder(baseDir);
    CtuluLibFile.getFileFromJar("/sydoc.html", sysdocFolder.getSysdocHelpFile());
    File tocFile = sysdocFolder.getTocFile();
    String error = CtuluLibFile.canWrite(tocFile);
    if (error != null) {
      log.addSevereError(error);
      return group;
    }
    File copied = null;
    if (tocFile.exists()) {
      try {
        copied = File.createTempFile("sydoc-toc", ".xml");
        CtuluLibFile.copyFile(tocFile, copied);
      } catch (IOException iOException) {
        log.addSevereError(iOException.getMessage());
      }
    }
    FolderProcessor folderProcessor = new FolderProcessor();
    DocScanEntry process = folderProcessor.process(baseDir);
    if (process.getChildren().isEmpty()) {
      log.addWarn("RebuildToc.NoEntryFound");
      log.addWarn("RebuildToc.NoEntryFound.Info", baseDir.getAbsolutePath());
    }

    TitleProcessor titleProcessor = new TitleProcessor();
    CtuluLogGroup groupTitle = titleProcessor.process(process);
    if (groupTitle.containsSomething()) {
      group.addGroup(groupTitle);
    }

    DocEntryBuilder builder = new DocEntryBuilder();
    DocEntry entry = builder.create(process);
    PersistProcessor persist = new PersistProcessor();
    CtuluLog write = persist.write(tocFile, entry);
    if (write.isNotEmpty()) {
      log.addAllLogRecord(write);
    }
    if (write.containsSevereError() && copied != null && copied.exists()) {
      CtuluLibFile.copyFile(copied, tocFile);
      log.addWarn("RebuildToc.OldTocKept");
    }
    //on récupère les titres
    return group;
  }
}
