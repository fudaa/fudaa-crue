/*
 * Licence GPLv2
 */
package org.fudaa.dodico.crue.sysdoc.scan;

import java.io.File;
import java.util.*;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author deniger
 */
public class DocScanEntry {

  private String path;
  private String title;
  private String description;
  private File file;
  private File htmlFile;
  private final List<DocScanEntry> children = new ArrayList<>();

  public String getPath() {
    return path;
  }

  /**
   *
   * @return pour un dossier peut être index.html si existe.
   */
  public File getHtmlFile() {
    return htmlFile;
  }

  public void setHtmlFile(File htmlFile) {
    this.htmlFile = htmlFile;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Map<File, DocScanEntry> getDocEntryByFile() {
    Map<File, DocScanEntry> res = new HashMap<>();
    getDocEntryByFile(res);
    return res;
  }

  private void getDocEntryByFile(Map<File, DocScanEntry> in) {
    in.put(file, this);
    for (DocScanEntry docEntry : children) {
      docEntry.getDocEntryByFile(in);
    }
  }

  @Override
  public String toString() {
    return print(StringUtils.EMPTY);
  }

  private String print(String prefix) {
    StringBuilder builder = new StringBuilder();
    builder.append(prefix);
    builder.append(title);
    builder.append("\t;");
    builder.append(path);
    builder.append("\t;");
    builder.append(file);
    if (!isLeaf()) {
      for (DocScanEntry docEntry : children) {
        builder.append('\n');
        builder.append(docEntry.print(prefix + "  "));
      }
    }
    return builder.toString();
  }

  public File getFile() {
    return file;
  }

  public void setFile(File file) {
    this.file = file;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public List<DocScanEntry> getChildren() {
    return Collections.unmodifiableList(children);
  }

  public boolean isLeaf() {
    return children.isEmpty();
  }

  public void addEntry(DocScanEntry fileEntry) {
    children.add(fileEntry);
  }
}
