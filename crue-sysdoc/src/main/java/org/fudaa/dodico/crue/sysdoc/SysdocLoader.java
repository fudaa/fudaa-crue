/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc;

import java.io.File;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.sysdoc.common.SysdocFolder;
import org.fudaa.dodico.crue.sysdoc.common.SysdocMessages;
import org.fudaa.dodico.crue.sysdoc.persist.PersistContent;
import org.fudaa.dodico.crue.sysdoc.persist.PersistProcessor;

/**
 *
 * @author Frederic Deniger
 */
public class SysdocLoader {

  private final SysdocFolder sysdocFolders;

  public SysdocLoader(File baseDir) {
    this.sysdocFolders = new SysdocFolder(baseDir);
  }

  public CtuluIOResult<Sysdoc> load() {
    CtuluIOResult<Sysdoc> res = new CtuluIOResult<>();
    CtuluLog log = new CtuluLog(SysdocMessages.RESOURCE_BUNDLE);
    res.setAnalyze(log);
    File toc = sysdocFolders.getTocFile();
    if (!toc.exists()) {
      log.addSevereError("tocNotFound.Error", toc.getAbsolutePath());
      log.addInfo("tocNotFound.Info", toc.getAbsolutePath());
      return res;
    }
    PersistProcessor persist = new PersistProcessor();
    CtuluIOResult<PersistContent> read = persist.read(toc);
    if (read.getSource() == null) {
      log.addSevereError("tocContentInvalid.Error", toc.getAbsolutePath());
    }
    if (read.getSource() != null) {
      Sysdoc sysdoc = new Sysdoc(sysdocFolders, read.getSource().getEntry());
      res.setSource(sysdoc);
    }
    return res;

  }
}
