/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.sysdoc.scan;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.Comparator;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.sysdoc.common.SysdocFolder;

/**
 * FolderProcessor​ permet de construire la table des matières en analysant le répertoire (Folder) principal de Sydoc
 *
 * @author deniger
 */
public class FolderProcessor {

  private final String extension = ".html";
  private FileFilter extensionFileFilter;
  private Comparator<File> fileComparator;

  public DocScanEntry process(File dir) {
    extensionFileFilter = new ExtensionFileFilter(extension);
    fileComparator = new FileNameComparator();
    DocScanEntry res = new DocScanEntry();
    res.setFile(dir);
    res.setTitle(dir.getName());
    res.setPath(StringUtils.EMPTY);
    File htmlFile = new File(dir, SysdocFolder.INDEXHTML_FILENAME);
    if (htmlFile.isFile()) {
      res.setHtmlFile(htmlFile);
    }
    processFile(res);



    return res;
  }

  private void processFile(DocScanEntry res) {
    File[] listFiles = res.getFile().listFiles(extensionFileFilter);
    Arrays.sort(listFiles, fileComparator);
    String path = res.getPath();
    for (File file : listFiles) {
      if (file.isDirectory() && SysdocFolder.SYSDOC_FOLDER_NAME.equals(file.getName())) {
        continue;
      }
      DocScanEntry fileEntry = new DocScanEntry();
      fileEntry.setFile(file);
      fileEntry.setTitle(file.getName());
      if (file.isFile()) {
        fileEntry.setPath(path + file.getName());
        fileEntry.setHtmlFile(file);
      } else {
        File htmlFile = new File(file, SysdocFolder.INDEXHTML_FILENAME);
        if (htmlFile.isFile()) {
          fileEntry.setHtmlFile(htmlFile);
        }
        fileEntry.setPath(path + file.getName() + "/");
        //on fait un parcours récursif
        processFile(fileEntry);
      }
      //pour ne pas ajouter les folders vide
      if (fileEntry.getFile().isFile() || !fileEntry.isLeaf() || fileEntry.getHtmlFile() != null) {
        res.addEntry(fileEntry);
      }
    }
  }
}
