/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.common;

import java.io.File;

/**
 *
 * @author Frederic Deniger
 */
public class SysdocFolder {

  public static final String INDEXHTML_FILENAME = "index.html";
  public static final String SYSDOC_FOLDER_NAME = ".sydoc";
  private final File baseDir;
  private final File sysdocDir;
  private final File sysdocSearchIndexDir;
  private final File tocFile;

  public SysdocFolder(File baseDir) {
    this.baseDir = baseDir;
    sysdocDir = new File(baseDir, SYSDOC_FOLDER_NAME);
    sysdocSearchIndexDir = new File(sysdocDir, "index");
    tocFile = new File(sysdocDir, "toc.xml");
  }

  public File getSysdocHelpFile() {
    return new File(sysdocDir, "sydoc.html");
  }

  public File getSysdocSearchIndexDir() {
    return sysdocSearchIndexDir;
  }

  public File getBaseDir() {
    return baseDir;
  }

  public File getHomeDir() {
    return new File(baseDir, "index.html");
  }

  public File getSysdocDir() {
    return sysdocDir;
  }

  public File getTocFile() {
    return tocFile;
  }
}
