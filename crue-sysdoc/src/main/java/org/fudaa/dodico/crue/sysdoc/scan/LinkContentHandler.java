/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.scan;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Frederic Deniger
 */
public class LinkContentHandler extends DefaultHandler {

  final List<String> urls = new ArrayList<>();

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
    if ("img".equalsIgnoreCase(localName)) {
      String value = attributes.getValue("SRC");
      if (value == null) {
        value = attributes.getValue("src");
      }
      if (StringUtils.isNotEmpty(value)) {
        urls.add(value);
      }

    } else {
      String value = attributes.getValue("HREF");
      if (value == null) {
        value = attributes.getValue("href");
      }
      if (StringUtils.isNotEmpty(value)) {
        urls.add(value);
      }
    }

  }

  public List<String> getUrls() {
    return urls;
  }

  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    if ("body".equals(qName)) {
      throw new SAXException("OK title found");
    }
  }
}
