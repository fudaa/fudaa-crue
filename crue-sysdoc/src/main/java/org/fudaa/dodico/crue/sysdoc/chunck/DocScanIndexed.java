/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.chunck;

import org.fudaa.dodico.crue.sysdoc.common.DocEntry;

/**
 *
 * @author Frederic Deniger
 */
public class DocScanIndexed {

  private final int idx;
  private final DocEntry entry;

  public DocScanIndexed(int idx, DocEntry entry) {
    this.idx = idx;
    this.entry = entry;
  }

  public int getIdx() {
    return idx;
  }

  public DocEntry getEntry() {
    return entry;
  }
}
