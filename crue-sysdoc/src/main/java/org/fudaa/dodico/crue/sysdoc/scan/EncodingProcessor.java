/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.scan;

import java.io.File;
import java.io.FileReader;
import java.nio.charset.Charset;
import org.apache.commons.lang3.StringUtils;
import org.ccil.cowan.tagsoup.Parser;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.xml.sax.InputSource;

/**
 *
 * @author Frederic Deniger
 */
public class EncodingProcessor {

  public String getEncoding(File htmlFile, CtuluLog log) {
    Parser parser = new Parser();
    final EncodingContentHandler encodingContentHandler = new EncodingContentHandler();
    parser.setContentHandler(encodingContentHandler);
    FileReader reader = null;
    try {
      reader = new FileReader(htmlFile);
      parser.parse(new InputSource(reader));
    } catch (Exception exception) {
    } finally {
      CtuluLibFile.close(reader);
    }
    String encoding = encodingContentHandler.encoding;
    if (StringUtils.isBlank(encoding)) {
      log.addWarn("encoding.notFound");
    } else {
      try {
        Charset.forName(encoding);
      } catch (Exception e) {
        log.addWarn("encoding.notSupported", encoding);
        encoding = "UTF-8";
      }
    }
    return encoding;

  }
}
