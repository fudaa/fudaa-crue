/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.sysdoc.scan;

import java.io.File;
import java.util.Comparator;

/**
 *
 * @author deniger
 */
public class FileNameComparator implements Comparator<File> {

  @Override
  public int compare(File o1, File o2) {
    return o1.getName().compareTo(o2.getName());
  }
}
