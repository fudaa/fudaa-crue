/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.index;

import java.util.TreeSet;
import org.apache.commons.lang3.StringUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Frederic Deniger
 */
public class HtmlContentHandler extends DefaultHandler {

  private final StringBuilder builder = new StringBuilder();
  boolean ignore = false;
  final TreeSet<String> toIgnore = new TreeSet<>();
  private final char sepChar;

  public HtmlContentHandler() {
    sepChar = '\n';
    toIgnore.add("style");
  }

  public String getContent() {
    return builder.toString();
  }
  boolean lineSepAdded;

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

    ignore = toIgnore.contains(qName.toLowerCase());
    if ("meta".equals(qName)) {
      String name = attributes.getValue("name");
      if (name != null) {
        name = name.toLowerCase();
        if ("description".equals(name) || "keywords".equals(name)) {
          String content = attributes.getValue("content").trim();
          if (StringUtils.isNotBlank(content)) {
            if (builder.length() > 0) {
              builder.append(sepChar);
            }
            builder.append(content);
          }
        }
      }
      ignore = true;
    }
    lineSepAdded = false;

  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    super.endElement(uri, localName, qName);
  }

  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
    if (!ignore && length > 0) {

      String string = new String(ch, start, length).trim();
      if (StringUtils.isNotBlank(string)) {
        if (!lineSepAdded) {
          if (builder.length() > 0) {
            builder.append(sepChar);
          }
          lineSepAdded = true;
        }
        builder.append(string);
      }
    }
  }
}
