/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.scan;

import org.apache.commons.lang3.StringUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Frederic Deniger
 */
public class TitleContentHandler extends DefaultHandler {

  final StringBuilder title = new StringBuilder();
  String description;
  boolean titleStarted;

  public String getTitle() {
    return title.toString();
  }

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
    if ("title".equalsIgnoreCase(localName)) {
      titleStarted = true;
    } else if ("meta".equalsIgnoreCase(localName)) {
      String name = attributes.getValue("name");
      if (name != null) {
        name = name.toLowerCase();
        if ("description".equals(name)) {
          String content = attributes.getValue("content").trim();
          if (StringUtils.isNotBlank(content)) {
            description = content;
            if (description.length() > 50) {
              description = description.substring(0, 47) + "...";
            }
          }
        }
      }
    }
  }

  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
    if (titleStarted) {
      title.append(ch, start, length);
    }
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    if (titleStarted) {
      titleStarted = false;
    }
    if ("head".equals(qName)) {
      throw new SAXException("OK title found");
    }
  }
}
