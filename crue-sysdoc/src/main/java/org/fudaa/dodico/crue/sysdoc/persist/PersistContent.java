/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.persist;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.fudaa.dodico.crue.sysdoc.common.DocEntry;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("DocContent")
public class PersistContent {

  protected String xmlns = "http://www.fudaa.fr/xsd/crue";
  protected String xmlnsxsi = "http://www.w3.org/2001/XMLSchema-instance";
  protected String xsischemaLocation;

  protected void setXsdName(final String xsdFile) {
    xsischemaLocation = "http://www.fudaa.fr/xsd/crue http://www.fudaa.fr/xsd/crue/" + xsdFile;
  }

  public void updateXmlns() {
    xmlns = "http://www.fudaa.fr/xsd/crue";
    xmlnsxsi = "http://www.w3.org/2001/XMLSchema-instance";
  }
  @XStreamAlias("Doc")
  private DocEntry entry = new DocEntry();

  public DocEntry getEntry() {
    return entry;
  }

  public void setEntry(DocEntry entry) {
    this.entry = entry;
  }
}
