/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc;

import org.fudaa.dodico.crue.sysdoc.common.DocEntry;
import org.fudaa.dodico.crue.sysdoc.common.SysdocFolder;

/**
 *
 * @author Frederic Deniger
 */
public class Sysdoc {

  private final SysdocFolder sysdocFolders;
  private final DocEntry entry;

  public Sysdoc(SysdocFolder sysdocFolders, DocEntry entry) {
    this.sysdocFolders = sysdocFolders;
    this.entry = entry;
  }

  public DocEntry getEntry() {
    return entry;
  }

  public SysdocFolder getSysdocFolders() {
    return sysdocFolders;
  }
}
