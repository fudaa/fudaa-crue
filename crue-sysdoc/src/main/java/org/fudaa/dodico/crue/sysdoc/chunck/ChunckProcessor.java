/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.chunck;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.ccil.cowan.tagsoup.Parser;
import org.ccil.cowan.tagsoup.XMLWriter;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.sysdoc.common.DocEntry;
import org.fudaa.dodico.crue.sysdoc.common.SysdocMessages;
import org.fudaa.dodico.crue.sysdoc.scan.EncodingProcessor;
import org.fudaa.dodico.crue.sysdoc.scan.LinkRefChecker;
import org.xml.sax.InputSource;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Frederic Deniger
 */
public class ChunckProcessor {
  private List<DocScanIndexed> getEntries(DocEntry entry) {
    return fillWithEntries(0, entry, new ArrayList<>());
  }

  private List<DocScanIndexed> fillWithEntries(int idx, DocEntry entry, List<DocScanIndexed> target) {
    target.add(new DocScanIndexed(idx, entry));
    List<DocEntry> children = entry.getChildren();
    for (DocEntry docEntry : children) {
      fillWithEntries(idx + 1, docEntry, target);
    }
    return target;
  }

  private static String getAnchorName(final DocEntry entry) {
    String name = entry.getPath();
    if (entry.getPath() == null) {
      return null;
    }
    name = name.replace('/', '_');
    name = name.replace('\\', '_');
    name = name.replace('.', '_');
    return name;
  }

  public CtuluLog chunck(File baseDir, DocEntry entry, File targetFile) {
    CtuluLog log = new CtuluLog(SysdocMessages.RESOURCE_BUNDLE);
    List<DocScanIndexed> entries = getEntries(entry);
    List<ChunkHtmlCallable> callables = new ArrayList<>();
    for (DocScanIndexed docScanEntryIndexed : entries) {
      callables.add(new ChunkHtmlCallable(docScanEntryIndexed, baseDir));
    }
    ExecutorService executor = null;
    try {
      executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
      List<Future<ChunckResult>> invokeAll = executor.invokeAll(callables);
      List<ChunckResult> chunks = new ArrayList<>();
      for (Future<ChunckResult> future : invokeAll) {
        chunks.add(future.get());
      }
      writeAllHtmlFile(targetFile, chunks);
    } catch (InterruptedException ex) {
      Logger.getLogger(LinkRefChecker.class.getName()).log(Level.INFO, "message {0}", ex);
      log.addSevereError("chunckHtmlInterrupted");
      Thread.currentThread().interrupt();
    } catch (ExecutionException e) {
      Logger.getLogger(LinkRefChecker.class.getName()).log(Level.INFO, "message {0}", e);
      log.addSevereError("chunckHtmlTaskAborted", e.getMessage());
    } catch (IOException e) {
      Logger.getLogger(LinkRefChecker.class.getName()).log(Level.INFO, "message {0}", e);
      log.addSevereError("chunckHtmlIOException", e.getMessage());
    } finally {

      if (executor != null) {
        executor.shutdownNow();
      }
    }
    return log;
  }

  private void writeAllHtmlFile(File targetFile, List<ChunckResult> chunks) throws IOException {
    FileOutputStream fileOutput = new FileOutputStream(targetFile);
    OutputStreamWriter writer = new OutputStreamWriter(fileOutput, ENCODING_CONSTANTE);
    writeHead(writer, chunks, fileOutput);
    writer.write("<body>");
    writer.write(CtuluLibString.LINE_SEP);
    if (chunks.size() > 1) {
      writer.write("<h1>");
      writer.write(SysdocMessages.getString("html.summary"));
      writer.write("</h1>");
      writer.write("<ul>");
      for (ChunckResult chunckResult : chunks) {
        final String anchorName = getAnchorName(chunckResult.entry.getEntry());
        if (anchorName != null) {
          writer.write("<li>");
          writer.write("<a href=\"#" + anchorName + "\">");
          writer.write(chunckResult.entry.getEntry().getTitle());
          writer.write("</a>");
          writer.write("</li>");
        }
      }
      writer.write("</ul>");
      writePageBreak(writer);
    }
    writer.flush();
    boolean first = true;
    for (ChunckResult chunckResult : chunks) {
      if (writeChunkBody(first, chunckResult, writer, fileOutput)) {
        first = false;
      }
    }

    writer.write("</body>");
    writer.write(CtuluLibString.LINE_SEP);
    writer.write("</html>");
    writer.flush();
    CtuluLibFile.close(fileOutput);
  }

  private void writeHead(OutputStreamWriter writer, List<ChunckResult> chunks, FileOutputStream fileOutput) throws IOException {
    writer.write("<html>");
    writer.write(CtuluLibString.LINE_SEP);
    writer.write("<meta http-equiv=Content-Type content=\"text/html;" + ENCODING_CONSTANTE + "\">");
    writer.write("<meta charset=\"" + ENCODING_CONSTANTE + "\">");
    writer.write(CtuluLibString.LINE_SEP);
    writer.write("<style>\n<!--\n");
    writer.write(
        "@media all { .page-break  { display: block;clear: both;height: 0;margin: 40px 0 80px;padding: 0;border: 0;font-family: arial;text-align: center;font-size: 30px;line-height: 1;}}\n");
    writer.write("@media print { .page-break  { display: none; }}\n");
    writer.write("@media print { .page-break-print  { display: block; page-break-before: always; }}\n");
    writer.write("hr:after {content: \"\\273D \\273D \\273D \\273D \\273D\";height: 0;letter-spacing: 1em;color: #aaa;}");
    writer.write("-->\n</style>");
    writer.write(CtuluLibString.LINE_SEP);
    writer.flush();
    for (ChunckResult chunckResult : chunks) {
      File styleFile = chunckResult.getStyleFile();
      if (styleFile != null && styleFile.length() > 0) {
        FileUtils.copyFile(styleFile, fileOutput);
        fileOutput.flush();
        writer.write(CtuluLibString.LINE_SEP);
        writer.flush();
      }
      deleteFile(styleFile);
    }
  }

  private boolean writeChunkBody(boolean first, ChunckResult chunckResult, OutputStreamWriter writer, FileOutputStream fileOutput) throws IOException {
    File bodyFile = chunckResult.getBodyFile();
    DocScanIndexed docScanEntry = chunckResult.getEntry();
    final DocEntry docEntry = docScanEntry.getEntry();
    String name = getAnchorName(docEntry);
    if (name != null) {
      writer.write(CtuluLibString.LINE_SEP);
      writer.write("<a name=\"" + name + "\">");
      writer.write(CtuluLibString.LINE_SEP);
    }
    writer.flush();
    boolean written = false;
    if (bodyFile != null && bodyFile.length() > 0) {
      if (!first) {
        writePageBreak(writer);
      }
      written = true;
      writer.flush();
      FileUtils.copyFile(bodyFile, fileOutput);
      fileOutput.flush();
      writer.write(CtuluLibString.LINE_SEP);
      writer.write("<br>");
      writer.write(CtuluLibString.LINE_SEP);
      writer.flush();
    } else if (docScanEntry.getEntry().getTitle() != null) {
      writer.write(CtuluLibString.LINE_SEP);
      writer.write("<h1 class=\"level" + docScanEntry.getIdx() + "\">");
      writer.write(docScanEntry.getEntry().getTitle());
      writer.write("</h1>");
      writer.write(CtuluLibString.LINE_SEP);
      writer.flush();
    }
    deleteFile(bodyFile);
    return written;
  }

  private void deleteFile(File fileToDelete) {
    if (fileToDelete != null) {
      try {
        Files.delete(fileToDelete.toPath());
      } catch (IOException ex) {
        Logger.getLogger(getClass().getName()).log(Level.WARNING, "Can't delete file " + fileToDelete.getAbsolutePath() + ": " + ex.getMessage());
      }
    }
  }

  private void writePageBreak(OutputStreamWriter writer) throws IOException {
    writer.write("<span class=\"page-break-print\"></span>");
    writer.write("<hr class=\"page-break\">");
  }

  public static class ChunckResult {
    DocScanIndexed entry;
    File bodyFile;
    File styleFile;

    public DocScanIndexed getEntry() {
      return entry;
    }

    public File getBodyFile() {
      return bodyFile;
    }

    public File getStyleFile() {
      return styleFile;
    }
  }

  public static final String ENCODING_CONSTANTE = "UTF-8";

  public static class ChunkHtmlCallable implements Callable<ChunckResult> {
    private final DocScanIndexed entry;
    private final File baseDir;

    public ChunkHtmlCallable(DocScanIndexed entry, File baseDir) {
      this.entry = entry;
      this.baseDir = baseDir;
    }

    @Override
    public ChunckResult call() throws Exception {
      ChunckResult res = new ChunckResult();
      res.entry = entry;
      if (entry.getEntry().getPath() == null) {
        return res;
      }
      File htmlFile = new File(baseDir, entry.getEntry().getPath());
      if (htmlFile.exists()) {
        FileInputStream fileInputStream = null;
        FileWriterWithEncoding out = null;
        FileWriterWithEncoding outStyle = null;
        try {
          res.bodyFile = File.createTempFile("body", ".html");
          res.styleFile = File.createTempFile("css", ".html");
          out = new FileWriterWithEncoding(res.bodyFile, ENCODING_CONSTANTE);
          outStyle = new FileWriterWithEncoding(res.styleFile, ENCODING_CONSTANTE);
          WriterContentHandler writer = new WriterContentHandler(htmlFile.getParentFile(), new XMLWriter(out), new XMLWriter(outStyle));
          Parser parser = new Parser();
          parser.setContentHandler(writer);
          EncodingProcessor encodingProcessor = new EncodingProcessor();
          CtuluLog log = new CtuluLog();
          String encoding = encodingProcessor.getEncoding(htmlFile, log);
          fileInputStream = new FileInputStream(htmlFile);
          parser.parse(new InputSource(new InputStreamReader(fileInputStream, Charset.forName(encoding))));
        } finally {
          CtuluLibFile.close(fileInputStream);
          CtuluLibFile.close(out);
          CtuluLibFile.close(outStyle);
        }
      }
      return res;
    }
  }
}
