/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.scan;

import org.apache.commons.lang3.StringUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Frederic Deniger
 */
public class EncodingContentHandler extends DefaultHandler {

  String encoding;

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
    if ("meta".equalsIgnoreCase(localName)) {
      String name = attributes.getValue("http-equiv");
      if ("content-type".equalsIgnoreCase(name)) {
        String content = attributes.getValue("content");
        encoding = StringUtils.substringAfterLast(content, "=");
        throw new SAXException("OK title found");
      }
    }
  }

  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    if ("head".equals(qName)) {
      throw new SAXException("OK title found");
    }
  }
}
