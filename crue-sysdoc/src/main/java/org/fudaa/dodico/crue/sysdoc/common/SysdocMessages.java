/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.crue.sysdoc.common;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.ctulu.MessageFormatHelper;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author deniger
 */
public final class SysdocMessages {
    public static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("org.fudaa.dodico.crue.sysdoc.common.sysdocMessages");

    private SysdocMessages() {
    }

    public static String getString(String s) {
        return RESOURCE_BUNDLE.getString(s);
    }

    public static String geti18nForClass(Class c, boolean longName) {
        return getStringOrDefault(c.getSimpleName() + (longName ? ".longName" : ".shortName"), c.getSimpleName());
    }

    public static String getString(String s, Object... args) {
        return MessageFormatHelper.getS(RESOURCE_BUNDLE, s, args);
    }

    private static String getStringOrDefault(String s, String defaultString) {
        try {
            return RESOURCE_BUNDLE.getString(s);
        } catch (MissingResourceException e) {
        }
        return defaultString;
    }

    public static void updateLocalizedMessage(CtuluLogRecord record) {
        updateLocalizedMessage(record, RESOURCE_BUNDLE);
    }

    public static void updateLocalizedMessage(CtuluLogRecord record, ResourceBundle resourceBundle) {
        final Object[] args = record.getArgs();
        String msg = null;
        if (resourceBundle != null) {
            try {
                msg = resourceBundle.getString(record.getMsg());
            } catch (java.util.MissingResourceException e) {
                msg = record.getMsg();
            }

            if (!ArrayUtils.isEmpty(args)) {
                msg = MessageFormat.format(msg, args);
            }
        }
        if (msg == null) {
            if (!ArrayUtils.isEmpty(args)) {
                msg = record.getMsg() + " " + StringUtils.join(args);
            } else {
                msg = record.getMsg();
            }
        }
        record.setLocalizedMessage(msg);
    }
}
