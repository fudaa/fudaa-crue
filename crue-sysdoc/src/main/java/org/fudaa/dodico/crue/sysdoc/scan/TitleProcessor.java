/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.scan;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.ccil.cowan.tagsoup.Parser;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.sysdoc.common.SysdocMessages;
import org.xml.sax.InputSource;

/**
 *
 * @author Frederic Deniger
 */
public class TitleProcessor {

  public CtuluLogGroup process(DocScanEntry entry) {
    ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    List<Callable<CtuluLog>> callables = new ArrayList<>();
    CtuluLogGroup group = new CtuluLogGroup(SysdocMessages.RESOURCE_BUNDLE);
    group.setDescription("titleFinding.Action");
    addCallable(callables, entry);
    try {
      List<Future<CtuluLog>> invokeAll = service.invokeAll(callables);
      for (Future<CtuluLog> future : invokeAll) {
        CtuluLog log = future.get();
        if (log.isNotEmpty()) {
          group.addLog(log);
        }
      }
    } catch (Exception ex) {
      Logger.getLogger(TitleProcessor.class.getName()).log(Level.SEVERE, null, ex);
    }
    service.shutdown();
    return group;
  }

  private void addCallable(List<Callable<CtuluLog>> callables, DocScanEntry docScanEntry) {
    callables.add(new UpdateTitleCallable(docScanEntry));
    List<DocScanEntry> children = docScanEntry.getChildren();
    for (DocScanEntry child : children) {
      addCallable(callables, child);

    }
  }

  private static class UpdateTitleCallable implements Callable<CtuluLog> {

    private final DocScanEntry entry;

    public UpdateTitleCallable(DocScanEntry entry) {
      this.entry = entry;
    }

    @Override
    public CtuluLog call() {
      CtuluLog log = new CtuluLog(SysdocMessages.RESOURCE_BUNDLE);
      try {
        log.setResource(entry.getPath());
        log.setDesc(entry.getPath());
        File htmlFile = entry.getHtmlFile();
        if (htmlFile != null && htmlFile.isFile()) {
          EncodingProcessor encodingProcessor = new EncodingProcessor();
          String encoding = encodingProcessor.getEncoding(htmlFile, log);
          TitleContentHandler titleContentHandler = getTitle(htmlFile, encoding);
          String title = StringUtils.trim(titleContentHandler.title.toString());
          if (StringUtils.isNotEmpty(title)) {
            entry.setTitle(title);
          } else {
            entry.setTitle(entry.getFile().isFile() ? StringUtils.substringBeforeLast(entry.getFile().getName(), ".") : entry.getFile().getName());
          }
          entry.setDescription(titleContentHandler.description);

        }
        log.updateLocalizedMessage(SysdocMessages.RESOURCE_BUNDLE);
      } catch (Exception e) {
        Logger.getLogger(UpdateTitleCallable.class.getName()).log(Level.SEVERE, "message {0}", e);
      }
      return log;
    }
  }

  public static TitleContentHandler getTitle(File htmlFile, String encoding) {
    Parser parser = new Parser();
    final TitleContentHandler titleContentHandler = new TitleContentHandler();
    parser.setContentHandler(titleContentHandler);
    FileInputStream fileInputStream = null;
    try {
      fileInputStream = new FileInputStream(htmlFile);
      parser.parse(new InputSource(new InputStreamReader(fileInputStream, Charset.forName(encoding))));
    } catch (Exception exception) {
//          Logger.getLogger(UpdateTitleCallable.class.getName()).log(Level.WARNING, "message {0}", exception);
    } finally {
      CtuluLibFile.close(fileInputStream);
    }
    return titleContentHandler;
  }
}
