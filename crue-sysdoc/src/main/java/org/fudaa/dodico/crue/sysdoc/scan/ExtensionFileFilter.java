/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.sysdoc.scan;

import java.io.File;
import java.io.FileFilter;
import org.fudaa.dodico.crue.sysdoc.common.SysdocFolder;

/**
 *
 * @author deniger
 */
class ExtensionFileFilter implements FileFilter {

  private final String extension;

  public ExtensionFileFilter(String extension) {
    this.extension = extension;
  }

  @Override
  public boolean accept(File pathname) {
    if (pathname.isDirectory() && !SysdocFolder.SYSDOC_FOLDER_NAME.equals(pathname.getName())) {
      return true;
    }
    if (SysdocFolder.INDEXHTML_FILENAME.equals(pathname.getName())) {
      return false;
    }
    return pathname.getName().endsWith(extension);
  }
}
