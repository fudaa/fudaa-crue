/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.scan;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;
import org.ccil.cowan.tagsoup.Parser;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.xml.sax.InputSource;

/**
 * Liste toutes les références vers des liens.
 *
 * @author Frederic Deniger
 */
public class LinkAndSourceProcessor {

  public static class Result {

    private final String encoding;
    private final List<String> refs;
    private final String title;

    public Result(String encoding, List<String> refs, String title) {
      this.encoding = encoding;
      this.refs = refs;
      this.title = title;
    }

    public String getTitle() {
      return title;
    }

    public String getEncoding() {
      return encoding;
    }

    public List<String> getRefs() {
      return refs;
    }
  }

  public Result getAllHrefAndSourceLinks(File htmlFile, CtuluLog log) {
    EncodingProcessor encodingProcessor = new EncodingProcessor();
    String encoding = encodingProcessor.getEncoding(htmlFile, log);
    Parser parser = new Parser();
    final LinkContentHandler refsContentHandler = new LinkContentHandler();
    parser.setContentHandler(refsContentHandler);
    FileInputStream fileInputStream = null;
    try {
      fileInputStream = new FileInputStream(htmlFile);
      parser.parse(new InputSource(new InputStreamReader(fileInputStream, Charset.forName(encoding))));
    } catch (Exception exception) {
//          Logger.getLogger(UpdateTitleCallable.class.getName()).log(Level.WARNING, "message {0}", exception);
    } finally {
      CtuluLibFile.close(fileInputStream);
    }
    return new Result(encoding, refsContentHandler.getUrls(), TitleProcessor.getTitle(htmlFile, encoding).getTitle());

  }
}
