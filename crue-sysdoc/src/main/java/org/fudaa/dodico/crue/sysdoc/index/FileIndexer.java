/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.index;

import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.ccil.cowan.tagsoup.Parser;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.sysdoc.common.SysdocFolder;
import org.fudaa.dodico.crue.sysdoc.common.SysdocMessages;
import org.fudaa.dodico.crue.sysdoc.scan.EncodingProcessor;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Frederic Deniger
 */
public class FileIndexer {
  public static final String PATH_ID = "path";
  public static final String CONTENTS_ID = "contents";
  private final File baseDir;
  private final SysdocFolder sysdocFolder;

  public FileIndexer(File baseDir, String language) {
    this.baseDir = baseDir;
    sysdocFolder = new SysdocFolder(baseDir);
  }

  public CtuluLog index() {
    CtuluLog res = new CtuluLog(SysdocMessages.RESOURCE_BUNDLE);
    File indexDir = sysdocFolder.getSysdocSearchIndexDir();
    CtuluLibFile.deleteDir(indexDir);
    try (Directory dir = FSDirectory.open(indexDir)) {
      Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
      IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_40, analyzer);
      iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
      IndexWriter writer = new IndexWriter(dir, iwc);
      indexDocs(writer, baseDir);
      writer.close();
    } catch (Exception e) {
      res.addSevereError(e.getMessage());
    }
    return res;
  }

  protected String getContent(File file) {
    HtmlContentHandler contentHandler = new HtmlContentHandler();
    Parser parser = new Parser();
    parser.setContentHandler(contentHandler);
    try(FileInputStream  inputStream = new FileInputStream(file)) {
      EncodingProcessor encodingProcessor = new EncodingProcessor();
      String encoding = encodingProcessor.getEncoding(file, new CtuluLog());
      parser.parse(new InputSource(new InputStreamReader(inputStream, Charset.forName(encoding))));
    } catch (Exception exception) {
      Logger.getLogger(FileIndexer.class.getName()).log(Level.WARNING, "message {0}", exception);
    }
    return contentHandler.getContent();
  }

  void indexDocs(IndexWriter writer, File file) throws IOException {
    // do not try to index files that cannot be read
    if (file.canRead()) {
      if (file.isDirectory() && !file.equals(sysdocFolder.getSysdocDir())) {
        String[] files = file.list();
        // an IO error could occur
        if (files != null) {
          for (int i = 0; i < files.length; i++) {
            indexDocs(writer, new File(file, files[i]));
          }
        }
      } else if (file.getName().endsWith(".html")) {
        // make a new, empty document
        Document doc = new Document();
        // Add the path of the file as a field named "path".  Use a
        // field that is indexed (i.e. searchable), but don't tokenize 
        // the field into separate words and don't index term frequency
        // or positional information:
        String path = StringUtils.removeStart(file.getPath(), baseDir.getPath());
        path = StringUtils.replaceChars(path, '\\', '/');
        if (path.length() > 0 && path.charAt(0) == '/') {
          path = path.substring(1);
        }
        Field pathField = new StringField(PATH_ID, path, Field.Store.YES);
        doc.add(pathField);

        doc.add(new LongField("modified", file.lastModified(), Field.Store.NO));
        final String content = getContent(file);
        doc.add(new TextField(CONTENTS_ID, content, Field.Store.YES));
        writer.addDocument(doc);
      }
    }
  }
}
