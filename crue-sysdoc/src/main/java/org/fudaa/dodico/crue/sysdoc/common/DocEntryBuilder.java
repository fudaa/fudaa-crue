/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.common;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.sysdoc.scan.DocScanEntry;

/**
 * Transforme les objets issus du scan en objet pour la table des matières.
 *
 * @author Frederic Deniger
 */
public class DocEntryBuilder {

  public List<DocEntry> createEntries(List<DocScanEntry> entry) {
    List<DocEntry> res = new ArrayList<>();
    if (entry != null) {
      for (DocScanEntry scan : entry) {
        res.add(create(scan));
      }
    }
    return res;
  }

  public DocEntry create(DocScanEntry scan) {
    DocEntry entry = new DocEntry();
    entry.setPath(scan.getPath());
    entry.setTitle(scan.getTitle());
    entry.setDescription(scan.getDescription());
    entry.setFolder(scan.getFile() != null && scan.getFile().isDirectory());
    if (entry.isFolder() && scan.getHtmlFile() != null) {
      entry.setPath(scan.getPath() + scan.getHtmlFile().getName());
    }
    if (!scan.isLeaf()) {
      List<DocScanEntry> children = scan.getChildren();
      for (DocScanEntry docScanEntry : children) {
        entry.addChild(create(docScanEntry));
      }
    }
    return entry;
  }
}
