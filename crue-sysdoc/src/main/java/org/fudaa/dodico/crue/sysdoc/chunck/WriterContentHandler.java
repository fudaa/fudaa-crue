/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.chunck;

import java.io.File;
import java.net.MalformedURLException;
import org.ccil.cowan.tagsoup.AttributesImpl;
import org.ccil.cowan.tagsoup.XMLWriter;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 *
 * @author Frederic Deniger
 */
public class WriterContentHandler implements ContentHandler {

  private final XMLWriter xmlWriter;
  private final XMLWriter styleWriter;
  private final File baseDir;
  private boolean styleStarted;
  private boolean bodystarted = false;

  public WriterContentHandler(File baseDir, XMLWriter xmlWriter, XMLWriter styleWriter) {
    this.xmlWriter = xmlWriter;
    this.styleWriter = styleWriter;
    this.baseDir = baseDir;
  }

  private XMLWriter getXMLWriter() {
    if (styleStarted) {
      return styleWriter;
    }
    return xmlWriter;
  }

  @Override
  public void skippedEntity(String name) throws SAXException {
    getXMLWriter().skippedEntity(name);
  }

  @Override
  public void endPrefixMapping(String prefix) throws SAXException {
    getXMLWriter().endPrefixMapping(prefix);
  }

  @Override
  public void setDocumentLocator(Locator locator) {
    getXMLWriter().setDocumentLocator(locator);
  }

  @Override
  public void endDocument() throws SAXException {
//    getXMLWriter().endDocument();
  }

  @Override
  public void startPrefixMapping(String prefix, String uri) throws SAXException {
    if (canWrite()) {
      getXMLWriter().startPrefixMapping(prefix, uri);
    }
  }

  @Override
  public void startDocument() throws SAXException {
//    getXMLWriter().startDocument();
  }

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
    if ("img".equalsIgnoreCase(localName)) {
      String value = attributes.getValue("src");
      if (value != null) {
        AttributesImpl imp = (AttributesImpl) attributes;
        int index = imp.getIndex("src");
        if (value.indexOf(':') < 0) {
          try {
            imp.setValue(index, new File(baseDir, value).toURI().toURL().toString());
          } catch (MalformedURLException malformedURLException) {//que faire ?
          }
        }
      }
    } else {
      String value = attributes.getValue("href");
      if (value != null) {
        AttributesImpl imp = (AttributesImpl) attributes;
        int index = imp.getIndex("href");
        if (value.indexOf(':') < 0) {
          try {
            imp.setValue(index, new File(baseDir, value).toURI().toURL().toString());
          } catch (MalformedURLException malformedURLException) {//que faire ?
          }
        }
      }
    }
    if ("style".equals(qName)) {
      styleStarted = true;
      bodystarted = false;
    }
    if (canWrite()) {
      getXMLWriter().startElement(uri, localName, qName, attributes);
    }
    if ("body".equals(qName)) {//on le fait après pour ne pas imprimer le body.
      bodystarted = true;
      styleStarted = false;
    }
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    if ("body".equals(qName)) {
      bodystarted = false;
    }
    if (canWrite()) {
      getXMLWriter().endElement(uri, localName, qName);
    }
    if ("style".equals(qName)) {//on le fait après pour écrire la balise fermant.
      styleStarted = false;
    }
  }

  @Override
  public void characters(char[] ch, int start, int len) throws SAXException {
    if (canWrite()) {
      getXMLWriter().characters(ch, start, len);
    }
  }

  @Override
  public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
    if (bodystarted) {
      getXMLWriter().ignorableWhitespace(ch, start, length);
    }
  }

  @Override
  public void processingInstruction(String target, String data) throws SAXException {
    if (canWrite()) {
      getXMLWriter().processingInstruction(target, data);
    }
  }

  private boolean canWrite() {
    return bodystarted || styleStarted;
  }
}
