/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.sysdoc.scan;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.fudaa.ctulu.CtuluLibFile;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author deniger
 */
public class FolderProcessorTest {

  public FolderProcessorTest() {
  }

  public static void main(String[] args) {
    File dir = new File("/home/deniger/temp/sysdoc/");
    FolderProcessor instance = new FolderProcessor();
    DocScanEntry result = instance.process(dir);
    System.err.println(result);
  }

  /**
   * Test of process method, of class FolderProcessor.
   */
  @Test
  public void testProcess() throws IOException {
    File dir = CtuluLibFile.createTempDir();
    //on cree les fichier a,z dans le folder
    File ahtml = new File(dir, "a.html");
    File zhtml = new File(dir, "z.html");
    ahtml.createNewFile();
    zhtml.createNewFile();
    //sera ignore a cause de l'extension
    new File(dir, "toIgnore.htm").createNewFile();
    //le folder b
    File subFolderB = new File(dir, "b");
    //contient un sous folder BB
    File subFolderBB = new File(subFolderB, "bb");
    subFolderBB.mkdirs();
    //qui contient un fichier bbHtml
    File bzHtml = new File(subFolderBB, "z.html");
    bzHtml.createNewFile();
    //le folder c
    File subFolderC = new File(dir, "c");
    subFolderC.mkdirs();
    File cHtml = new File(subFolderC, "c.html");
    cHtml.createNewFile();
    cHtml.mkdirs();
    //empty folder
    File subFolderD = new File(dir, "d");
    subFolderD.mkdirs();
    //a.html
    //+b
    //++bb
    //+++z.html
    //+c
    //++c.html
    //d ( vide sera ignore)
    //z.html

    FolderProcessor processor = new FolderProcessor();
    DocScanEntry process = processor.process(dir);
    assertNotNull(process);
    assertEquals(dir, process.getFile());
    final List<DocScanEntry> children = process.getChildren();
    assertEquals(4, children.size());
    assertEquals(ahtml, children.get(0).getFile());
    assertEquals(ahtml.getName(), children.get(0).getPath());
    assertEquals(zhtml, children.get(3).getFile());
    assertEquals(zhtml.getName(), children.get(3).getPath());
    DocScanEntry bzEntry = process.getDocEntryByFile().get(bzHtml);
    assertNotNull(bzEntry);
    assertEquals(bzHtml.getName(), bzEntry.getTitle());
    assertEquals("b/bb/z.html", bzEntry.getPath());


    CtuluLibFile.deleteDir(dir);




  }
}
