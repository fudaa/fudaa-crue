/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.scan;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.fudaa.ctulu.CtuluLibFile;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class TitleProcessorTest {

  public TitleProcessorTest() {
  }

  @Test
  public void testUpdateTitle() throws IOException {
    File fileFromJar = CtuluLibFile.getFileFromJar("/sydoc.zip", null);
    File dir = CtuluLibFile.createTempDir();
    CtuluLibFile.unzip(fileFromJar, dir, null);
    FolderProcessor processor = new FolderProcessor();
    DocScanEntry process = processor.process(dir);
    List<DocScanEntry> folders = process.getChildren();
    assertEquals(2, folders.size());
    TitleProcessor titleProcessor = new TitleProcessor();
    titleProcessor.process(process);
    assertEquals("Titre du dossier avec éè", folders.get(0).getTitle());
    assertEquals("Dossier 1/", folders.get(0).getPath());
    DocScanEntry fichierOne = folders.get(0).getChildren().get(0);
    assertEquals("Titre", fichierOne.getTitle());
    assertEquals("Dossier 1/fichier.html", fichierOne.getPath());
    CtuluLibFile.deleteDir(dir);

  }
}
