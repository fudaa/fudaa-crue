/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.persist;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.sysdoc.common.DocEntry;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class PersistProcessorTest {

  public PersistProcessorTest() {
  }

  @Test
  public void testWrite() throws IOException {
    CtuluIOResult<PersistContent> read = read();
    PersistProcessor persistProcessor = new PersistProcessor();
    File f = File.createTempFile("test", ".xml");
    persistProcessor.write(f, read.getSource().getEntry());
    read = read(f);
    f.delete();
    testEntries(read.getSource().getEntry().getChildren());
  }

  @Test
  public void testRead() {
    CtuluIOResult<PersistContent> read = read();
    final List<DocEntry> docEntrys = read.getSource().getEntry().getChildren();
    testEntries(docEntrys);
  }

  private void testEntries(final List<DocEntry> docEntrys) {
    assertEquals(2, docEntrys.size());
    DocEntry entry = docEntrys.get(0);
    assertEquals("title", entry.getTitle());
    assertEquals("path", entry.getPath());
    List<DocEntry> children = entry.getChildren();
    assertEquals(1, children.size());
    assertEquals("childTitle", children.get(0).getTitle());
    assertEquals("childPath", children.get(0).getPath());
    entry = docEntrys.get(1);
    assertEquals("titleBis", entry.getTitle());
    assertEquals("pathBis", entry.getPath());
  }

  private CtuluIOResult<PersistContent> read() {
    File fileFromJar = CtuluLibFile.getFileFromJar("/sydoc-toc.xml", null);
    return read(fileFromJar);
  }

  private CtuluIOResult<PersistContent> read(File fileFromJar) {
    PersistProcessor persistProcessor = new PersistProcessor();
    assertTrue(fileFromJar.exists());
    CtuluIOResult<PersistContent> read = persistProcessor.read(fileFromJar);
    fileFromJar.delete();
    return read;
  }
}
