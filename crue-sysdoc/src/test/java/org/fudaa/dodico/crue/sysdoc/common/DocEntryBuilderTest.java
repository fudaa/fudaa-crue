/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.common;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.sysdoc.scan.DocScanEntry;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class DocEntryBuilderTest {

  public DocEntryBuilderTest() {
  }

  @Test
  public void testCreate() {
    List<DocScanEntry> entries = new ArrayList<>();
    DocScanEntry entryOne = createScan("pathOne", "TitleOne");
    entryOne.addEntry(createScan("pathOneOne", "TitleOneOne"));
    entryOne.addEntry(createScan("pathOneTwo", "TitleOneTwo"));
    entries.add(entryOne);
    entries.add(createScan("pathTwo", "TitleTwo"));
    DocEntryBuilder builder = new DocEntryBuilder();
    List<DocEntry> create = builder.createEntries(entries);
    assertEquals(2, create.size());
    DocEntry docOne = create.get(0);
    assertEquals(entryOne.getTitle(), docOne.getTitle());
    assertEquals(entryOne.getPath(), docOne.getPath());
    assertEquals(2, docOne.getChildren().size());
    assertEquals("pathOneOne", docOne.getChildren().get(0).getPath());
    assertEquals("TitleOneOne", docOne.getChildren().get(0).getTitle());
    assertEquals("pathOneTwo", docOne.getChildren().get(1).getPath());
    assertEquals("TitleOneTwo", docOne.getChildren().get(1).getTitle());
    DocEntry docTwo = create.get(1);
    assertEquals("TitleTwo", docTwo.getTitle());
    assertEquals("pathTwo", docTwo.getPath());
    assertEquals(0, docTwo.getChildren().size());

  }

  private DocScanEntry createScan(final String pathOne, final String titleOne) {
    DocScanEntry entryOne = new DocScanEntry();
    entryOne.setPath(pathOne);
    entryOne.setTitle(titleOne);
    return entryOne;

  }
}
