/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc;

import java.io.File;
import java.io.IOException;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class SysdocLoaderTest {

  public SysdocLoaderTest() {
  }

  @Test
  public void testCreateWrong() throws IOException {
    final File createTempDir = CtuluLibFile.createTempDir();
    SysdocLoader builder = new SysdocLoader(createTempDir);
    CtuluLibFile.deleteDir(createTempDir);
    CtuluIOResult<Sysdoc> load = builder.load();
    assertNotNull(load);
    CtuluLog analyze = load.getAnalyze();
    assertTrue(analyze.containsSevereError());
    assertEquals(2, analyze.getRecords().size());
    assertEquals("tocNotFound.Error", analyze.getRecords().get(0).getMsg());
    assertEquals("tocNotFound.Info", analyze.getRecords().get(1).getMsg());
  }

  @Test
  public void testCreateGood() throws IOException {
    File fileFromJar = CtuluLibFile.getFileFromJar("/sydoc.zip", null);
    final File createTempDir = CtuluLibFile.createTempDir();
    CtuluLibFile.unzip(fileFromJar, createTempDir, null);
    fileFromJar.delete();
    SysdocLoader builder = new SysdocLoader(createTempDir);
    CtuluIOResult<Sysdoc> load = builder.load();
    CtuluLibFile.deleteDir(createTempDir);
    assertNotNull(load);
    CtuluLog analyze = load.getAnalyze();
    assertFalse(analyze.containsSevereError());
    Sysdoc source = load.getSource();
    assertEquals(2, source.getEntry().getChildren().size());
    CtuluLibFile.deleteDir(createTempDir);
  }
}
