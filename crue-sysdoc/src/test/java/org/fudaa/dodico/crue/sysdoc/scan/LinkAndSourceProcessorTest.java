/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.scan;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.ccil.cowan.tagsoup.Parser;
import org.ccil.cowan.tagsoup.XMLWriter;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.sysdoc.chunck.WriterContentHandler;
import org.fudaa.dodico.crue.sysdoc.scan.LinkAndSourceProcessor.Result;
import static org.junit.Assert.*;
import org.junit.Test;
import org.xml.sax.InputSource;

/**
 *
 * @author Frederic Deniger
 */
public class LinkAndSourceProcessorTest {

  public LinkAndSourceProcessorTest() {
  }

  @Test
  public void testFindHrefAnsSources() throws IOException {
    File target = File.createTempFile("tmp", ".html");
    CtuluLibFile.getFileFromJar("/fichierWithLinks.html", target);
    LinkAndSourceProcessor processor = new LinkAndSourceProcessor();
    CtuluLog log = new CtuluLog();
    Result allHrefAndSourceLinks = processor.getAllHrefAndSourceLinks(target, log);
    assertTrue(log.isEmpty());
    assertEquals(3, allHrefAndSourceLinks.getRefs().size());
    assertEquals("http://www.domain.com", allHrefAndSourceLinks.getRefs().get(0));
    assertEquals("../../../../Users/User%201/Documents/test.html", allHrefAndSourceLinks.getRefs().get(1));
    assertEquals("fichier1_html_m2613bf30.jpg", allHrefAndSourceLinks.getRefs().get(2));
    target.delete();
  }

  public static void main(String[] args) throws Exception {
    File target = new File("C:\\data\\Fudaa-Crue\\Data\\tmp\\file.xhtml");
    File target1 = new File("C:\\data\\Fudaa-Crue\\Data\\tmp\\file.html");
    File pdfFile = new File("C:\\data\\Fudaa-Crue\\Data\\tmp\\all.html");
    File styleFile = new File("C:\\data\\Fudaa-Crue\\Data\\tmp\\style.html");
    EncodingProcessor encodingProcessor = new EncodingProcessor();
    CtuluLog log = new CtuluLog();

    Parser parser = new Parser();
    final FileWriterWithEncoding out = new FileWriterWithEncoding(pdfFile, "UTF-8");
    final FileWriterWithEncoding outStyle = new FileWriterWithEncoding(styleFile, "UTF-8");
    XMLWriter writer = new XMLWriter(out);
    XMLWriter stylewriter = new XMLWriter(outStyle);
    final WriterContentHandler writerHandler = new WriterContentHandler(target.getParentFile(), writer, stylewriter);
    parser.setContentHandler(writerHandler);
    FileInputStream fileInputStream = null;
    try {
      fileInputStream = new FileInputStream(target);
      String encoding = encodingProcessor.getEncoding(target, log);
      parser.parse(new InputSource(new InputStreamReader(fileInputStream, Charset.forName(encoding))));
      fileInputStream.close();
      fileInputStream = new FileInputStream(target1);
      encoding = encodingProcessor.getEncoding(target1, log);
      parser.parse(new InputSource(new InputStreamReader(fileInputStream, Charset.forName(encoding))));

    } catch (Exception exception) {
      exception.printStackTrace();
//          Logger.getLogger(UpdateTitleCallable.class.getName()).log(Level.WARNING, "message {0}", exception);
    } finally {
      CtuluLibFile.close(fileInputStream);
      CtuluLibFile.close(out);
      CtuluLibFile.close(outStyle);
    }
  }
}
