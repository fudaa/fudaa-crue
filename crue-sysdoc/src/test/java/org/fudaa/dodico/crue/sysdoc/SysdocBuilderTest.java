/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.sysdoc.common.DocEntry;
import org.fudaa.dodico.crue.sysdoc.common.SysdocFolder;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class SysdocBuilderTest {

  public SysdocBuilderTest() {
  }

  @Test
  public void testGenerate() throws IOException {
    File fileFromJar = CtuluLibFile.getFileFromJar("/sydoc.zip", null);
    final File tmpDir = CtuluLibFile.createTempDir();
    CtuluLibFile.unzip(fileFromJar, tmpDir, null);
    fileFromJar.delete();
    SysdocFolder sysdocFolder = new SysdocFolder(tmpDir);
    File sysdocDir = sysdocFolder.getSysdocDir();
    assertTrue(sysdocDir.exists());
    CtuluLibFile.deleteDir(sysdocDir);
    assertFalse(sysdocDir.exists());
    SysdocBuilder builder = new SysdocBuilder(tmpDir);
    CtuluLogGroup build = builder.build();
    assertFalse(build.containsSomething());
    assertTrue(sysdocDir.exists());
    File tocFile = sysdocFolder.getTocFile();
    assertTrue(tocFile.exists());

    SysdocLoader loader = new SysdocLoader(tmpDir);
    CtuluIOResult<Sysdoc> load = loader.load();
    assertTrue(load.getAnalyze().isEmpty());
    Sysdoc source = load.getSource();
    List<DocEntry> entries = source.getEntry().getChildren();
    assertEquals(2, entries.size());
    assertEquals("Titre du dossier avec éè", entries.get(0).getTitle());
    assertTrue(entries.get(0).isFolder());
    assertEquals("Dossier 2", entries.get(1).getTitle());
    assertTrue(entries.get(1).isFolder());
    CtuluLibFile.deleteDir(tmpDir);
  }
}
