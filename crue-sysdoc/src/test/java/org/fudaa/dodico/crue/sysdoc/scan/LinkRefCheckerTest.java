/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.scan;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogResult;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class LinkRefCheckerTest {
  
  public LinkRefCheckerTest() {
  }
  private static File dir;
  
  @BeforeClass
  public static void exportSysdocDir() throws IOException {
    File fileFromJar = CtuluLibFile.getFileFromJar("/sydoc.zip", null);
    dir = CtuluLibFile.createTempDir();
    CtuluLibFile.unzip(fileFromJar, dir, null);
  }
  
  @AfterClass
  public static void deleteDir() {
    CtuluLibFile.deleteDir(dir);
  }
  
  @Test
  public void testGetHtmlFiles() {
    LinkRefChecker checker = new LinkRefChecker(dir);
    List<File> htmlFiles = checker.getHtmlFiles();
    assertEquals(3, htmlFiles.size());
    Collections.sort(htmlFiles);//pour les mettres dans l'ordre alphabetique.
    assertEquals(new File(new File(dir, "Dossier 1"), "fichier.html"), htmlFiles.get(0));
    assertEquals(new File(new File(dir, "Dossier 1"), "index.html"), htmlFiles.get(1));
    assertEquals(new File(new File(dir, "Dossier 2"), "fichier2.html"), htmlFiles.get(2));
  }
  
  @Test
  public void testCheckLink() throws IOException {
    LinkRefChecker checker = new LinkRefChecker(dir);
    CtuluLogResult<CtuluLogGroup> htmlFiles = checker.checkLink();
    assertTrue(htmlFiles.getLog().isEmpty());
    CtuluLogGroup resultat = htmlFiles.getResultat();
    List<CtuluLog> logs = resultat.getLogs();
    assertEquals(2, logs.size());
    CtuluLog log = logs.get(0);
    testFirstLogRecord(log);
    assertEquals(new File(new File(dir, "Dossier 1"), "fichier.html (Titre)").getCanonicalPath(), log.getDesc());
    log = logs.get(1);
    assertEquals(new File(new File(dir, "Dossier 1"), "index.html (Titre du dossier avec éè)").getCanonicalPath(), log.getDesc());
    testFirstLogRecord(log);
  }

  private void testFirstLogRecord(CtuluLog log) {
    assertEquals(1,log.getRecords().size());
    assertEquals("linkNotFound", log.getRecords().get(0).getMsg());
    assertEquals(1, log.getRecords().get(0).getArgs().length);
    assertEquals("fichier1_html_m2613bf30.jpg", log.getRecords().get(0).getArgs()[0]);
  }
}
