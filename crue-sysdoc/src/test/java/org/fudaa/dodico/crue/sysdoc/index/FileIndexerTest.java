/*
 GPL 2
 */
package org.fudaa.dodico.crue.sysdoc.index;

import java.io.File;
import java.io.IOException;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class FileIndexerTest {

  public FileIndexerTest() {
  }

  @Test
  public void testIndexer() throws IOException {
    File fileFromJar = CtuluLibFile.getFileFromJar("/sydoc.zip", null);
    final File tmpDir = CtuluLibFile.createTempDir();
    CtuluLibFile.unzip(fileFromJar, tmpDir, null);
    fileFromJar.delete();
    FileIndexer filerIndexer = new FileIndexer(tmpDir, "fr");
    CtuluLog index = filerIndexer.index();
    assertTrue(index.isEmpty());
    CtuluLibFile.deleteDir(tmpDir);
  }
}
