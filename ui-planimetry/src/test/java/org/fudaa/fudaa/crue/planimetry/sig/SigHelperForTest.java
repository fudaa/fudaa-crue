/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig;

import java.io.File;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper;
import org.fudaa.fudaa.crue.planimetry.sig.traceprofil.GGPointToTraceProfilProcessor;
import org.fudaa.fudaa.sig.wizard.FSigFileLoadResult;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderI;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author Frederic Deniger
 */
public class SigHelperForTest {

  public static CtuluLogResult<GISZoneCollectionLigneBrisee> loadProfilTrace(File shapeFile) {
    Map<SigFormatHelper.EnumFormat, FSigFileLoaderI> mapOfLoader = SigFormatHelper.getLoader();
    FSigFileLoaderI loader = mapOfLoader.get(SigFormatHelper.EnumFormat.SHP);
    assertNotNull(loader);
    FSigFileLoadResult result = new FSigFileLoadResult();
    CtuluAnalyze analyse = new CtuluAnalyze();
    loader.setInResult(result, shapeFile, shapeFile.getAbsolutePath(), null,
            analyse);
    assertTrue(analyse.isEmpty());
    GISZoneCollectionPoint buildPoints = SigLoaderHelper.buildPoints(result.createData());
    assertEquals(2433, buildPoints.getNbGeometries());
    GGPointToTraceProfilProcessor toTrace = new GGPointToTraceProfilProcessor(buildPoints, TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    CtuluLogResult<GISZoneCollectionLigneBrisee> toProfil = toTrace.toProfil(GGPointToTraceProfilProcessor.MODEL_Z_KEY_ORTHO);
    return toProfil;
  }

  public static GISZoneCollectionLigneBrisee loadCasiers(File shapeFile) {
    Map<SigFormatHelper.EnumFormat, FSigFileLoaderI> mapOfLoader = SigFormatHelper.getLoader();
    FSigFileLoaderI loader = mapOfLoader.get(SigFormatHelper.EnumFormat.SHP);
    assertNotNull(loader);
    FSigFileLoadResult result = new FSigFileLoadResult();
    CtuluAnalyze analyse = new CtuluAnalyze();
    loader.setInResult(result, shapeFile, shapeFile.getAbsolutePath(), null,
            analyse);
    assertTrue(analyse.isEmpty());
    return SigLoaderHelper.buildLine(result.createData());
  }
}
