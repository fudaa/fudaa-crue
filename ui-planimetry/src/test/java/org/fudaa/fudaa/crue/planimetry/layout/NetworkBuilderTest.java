package org.fudaa.fudaa.crue.planimetry.layout;

import org.locationtech.jts.geom.Coordinate;
import java.util.List;
import org.fudaa.ctulu.CtuluUIDefault;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.IdRegistry;
import org.fudaa.ebli.calque.edition.ZModelePointEditable;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.ScenarioBuilderForTests;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.controller.LayerBrancheController;
import org.fudaa.fudaa.crue.planimetry.controller.LayerNodeController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 *
 * @author deniger
 */
@Category(org.fudaa.dodico.crue.test.TestUI.class)
public class NetworkBuilderTest {

  public NetworkBuilderTest() {
  }

  @Test
  public void testBuildNewGisData() {
    EMHScenario scenario = ScenarioBuilderForTests.buildDefaultScenario();
    IdRegistry.install(scenario);
    NetworkBuilder builder = new NetworkBuilder();
    NetworkGisPositionnerResult gisData = builder.buildNewGisData(scenario, new VisuConfiguration(),
            CrueConfigMetierForTest.DEFAULT, 0, 0);
    GISZoneCollectionLigneBrisee edges = gisData.getBranches();
    assertEquals(5, edges.getNbGeometries());
    testIdsAndUids(edges, scenario.getIdRegistry());
    GISZoneCollectionPoint nodes = gisData.getNodes();
    assertEquals(7, nodes.getNbGeometries());
    testIdsAndUids(nodes, scenario.getIdRegistry());
  }

  @Test
  public void testBuildCalque() {
    EMHScenario scenario = ScenarioBuilderForTests.buildDefaultScenario();
    IdRegistry.install(scenario);
    NetworkBuilder builder = new NetworkBuilder();
    PlanimetryVisuPanel panel = builder.buildCalque(new CtuluUIDefault(), scenario, new VisuConfiguration(),
            CrueConfigMetierForTest.DEFAULT, true);
    final PlanimetryController planimetryCalqueContext = panel.getPlanimetryController();
    LayerBrancheController brancheController = planimetryCalqueContext.getBrancheController();
    GISZoneCollectionLigneBrisee brancheCollection = brancheController.getBrancheCollection();
    for (int i = 0; i < brancheCollection.getNumGeometries(); i++) {
      Long uid = planimetryCalqueContext.getPlanimetryGisModelContainer().getUid(brancheCollection, i);
      assertEquals(brancheController.getBranchePosition(uid), i);
    }
  }

  /**
   * Si on modifie un noeud, les branches doivent être modifiées.
   */
  @Test
  public void testMoveNode() {
    EMHScenario scenario = ScenarioBuilderForTests.buildDefaultScenario();
    IdRegistry.install(scenario);
    NetworkBuilder builder = new NetworkBuilder();
    PlanimetryVisuPanel panel = builder.buildCalque(new CtuluUIDefault(), scenario, new VisuConfiguration(),
            CrueConfigMetierForTest.DEFAULT, true);
    final PlanimetryController planimetryCalqueContext = panel.getPlanimetryController();
    //on modifie le noeud aval de la branche 0:
    int position = 1;
    final PlanimetryControllerHelper helper = planimetryCalqueContext.getHelper();
    LayerBrancheController brancheController = planimetryCalqueContext.getBrancheController();
    LayerNodeController nodeController = planimetryCalqueContext.getNodeController();
    Long avalUid = brancheController.getAmontNodeUid(position);
    int positionInModel = nodeController.getNodePosition(avalUid);
    ZModelePointEditable noeudsModeles = (ZModelePointEditable) planimetryCalqueContext.getCqNoeud().modele();
    GISPoint oldPoint = noeudsModeles.getGeomData().get(positionInModel);
    final double newX = oldPoint.getX() + 1000;
    final double newY = oldPoint.getY() + 1000;
    GISPoint newPoint = new GISPoint(newX, newY, 0);
    noeudsModeles.getGeomData().setGeomModifiable(true);
    noeudsModeles.getGeomData().set(positionInModel, newX, newY, null);
    CatEMHNoeud nd = (CatEMHNoeud) scenario.getIdRegistry().getEmh(avalUid);
    List<CatEMHBranche> branches = nd.getBranches();
    assertEquals(4, branches.size());
    for (CatEMHBranche catEMHBranche : branches) {
      long brancheUid = catEMHBranche.getUiId();
      assertTrue(brancheUid >= 0);
      int branchePosition = brancheController.getBranchePosition(brancheUid);
      assertTrue(branchePosition >= 0);
      long amont = brancheController.getAmontNodeUid(branchePosition);
      long aval = brancheController.getAvalNodeUid(branchePosition);
      assertTrue(amont == avalUid || aval == avalUid);//la branche doit avoir un des 2 noeuds extremes egale au noeud modifie.
      Coordinate amontCoordinate = brancheController.getAmontCoordinate(branchePosition);
      Coordinate avalCoordinate = brancheController.getAvalCoordinate(branchePosition);
      if (amont == avalUid) {
        assertTrue(helper.isSame(amontCoordinate, newPoint));
        assertTrue(helper.isNotSame(avalCoordinate, newPoint));
      } else {
        assertTrue(helper.isSame(avalCoordinate, newPoint));
        assertTrue(helper.isNotSame(amontCoordinate, newPoint));
      }
    }

  }

  private void testIdsAndUids(GISZoneCollection collection, IdRegistry registry) {
    GISAttributeModel uids = collection.getDataModel(0);
    GISAttributeModel ids = collection.getDataModel(1);
    assertEquals(collection.getNumGeometries(), uids.getSize());
    assertEquals(collection.getNumGeometries(), ids.getSize());
    for (int i = 0; i < uids.getSize(); i++) {
      Long uid = (Long) uids.getObjectValueAt(i);
      assertNotNull(uid);
      EMH emh = registry.getEmh(uid);
      String id = (String) ids.getObjectValueAt(i);
      assertEquals(emh.getNom(), id);
    }
  }
}
