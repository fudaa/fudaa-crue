/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.action;

import org.fudaa.dodico.crue.metier.factory.ScenarioBuilderForTest;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.operation.distance.GeometryLocation;
import java.io.File;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluUIDefault;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.IdRegistry;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper.EnumFormat;
import org.fudaa.fudaa.crue.common.ContainerActivityVisibility;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.controller.GroupeExternSIGType;
import org.fudaa.fudaa.crue.planimetry.controller.LayerBrancheController;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLigneBriseeExternUnmodifiableLayer;
import org.fudaa.fudaa.crue.planimetry.layout.NetworkBuilder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class ProjectBrancheOnLineProcessTest {

  PlanimetryVisuPanel panel;
  GISZoneCollectionLigneBrisee ligne;
  PlanimetryLigneBriseeExternUnmodifiableLayer lineLayer;

  public ProjectBrancheOnLineProcessTest() {
    System.setProperty("java.awt.headless", "true");//sur le moteur d'intégration continue c'est le cas.
  }

  /**
   * test le cas d'intersection sur une ligne
   */
  @Test
  public void testFindPositionOnLine() {
    ProjectBrancheOnLineProcess process = new ProjectBrancheOnLineProcess(panel);
    CoordinateSequence seq = GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(
            new Coordinate[]{new Coordinate(0, 0), new Coordinate(0, 50), new Coordinate(0, 100)});
    Coordinate c = new Coordinate(50, -1);//intersection point 0
    GeometryLocation findPositionOnLine = process.findPositionOnLine(seq, c);
    assertNotNull(findPositionOnLine);
    assertEquals(0, findPositionOnLine.getSegmentIndex());
    assertEquals(0, findPositionOnLine.getCoordinate().x, 1e-3);
    assertEquals(0, findPositionOnLine.getCoordinate().y, 1e-3);
    c = new Coordinate(50, 101);//intersection dernier point
    findPositionOnLine = process.findPositionOnLine(seq, c);
    assertNotNull(findPositionOnLine);
    assertEquals(2, findPositionOnLine.getSegmentIndex());
    assertEquals(0, findPositionOnLine.getCoordinate().x, 1e-3);
    assertEquals(100, findPositionOnLine.getCoordinate().y, 1e-3);
    c = new Coordinate(50, 0);
    findPositionOnLine = process.findPositionOnLine(seq, c);
    assertNotNull(findPositionOnLine);
    assertEquals(0, findPositionOnLine.getSegmentIndex());
    assertEquals(0, findPositionOnLine.getCoordinate().x, 1e-3);
    assertEquals(0, findPositionOnLine.getCoordinate().y, 1e-3);

    c = new Coordinate(50, 100);
    findPositionOnLine = process.findPositionOnLine(seq, c);
    assertNotNull(findPositionOnLine);
    assertEquals(1, findPositionOnLine.getSegmentIndex());
    assertEquals(0, findPositionOnLine.getCoordinate().x, 1e-3);
    assertEquals(100, findPositionOnLine.getCoordinate().y, 1e-3);
  }

  /**
   * dans ce cas, la projection ne fonctionne pas car la branche est verticale alors que la ligne est horizontale
   */
  @Test
  public void testProcessWithError() {
    LayerBrancheController brancheController = panel.getPlanimetryController().getBrancheController();
    checkAmontAvalNoeuds();
    brancheController.setEditable(true);
    GISPolyligne horizontalLine = GISGeometryFactory.INSTANCE.createLineString(new Coordinate[]{new Coordinate(10, 10), new Coordinate(0, 10)});
    ProjectBrancheOnLineProcess process = new ProjectBrancheOnLineProcess(panel);
    panel.getPlanimetryController().getCqBranche().setSelection(new int[]{0});
    CtuluLog log = process.process(horizontalLine);
    assertTrue(log.containsErrors());
    //on s'assure que les points n'ont pas été modifiés:
    checkAmontAvalNoeuds();
  }

  private void checkAmontAvalNoeuds() {
    LayerBrancheController brancheController = panel.getPlanimetryController().getBrancheController();
    Coordinate oldAmontCoordinate = brancheController.getAmontCoordinate(0);
    assertNotNull(oldAmontCoordinate);
    assertEquals(6, oldAmontCoordinate.x, 1e-3);
    assertEquals(200, oldAmontCoordinate.y, 1e-3);
    Coordinate oldAvalCoordinate = brancheController.getAvalCoordinate(0);
    assertNotNull(oldAvalCoordinate);
    assertEquals(6, oldAvalCoordinate.x, 1e-3);
    assertEquals(0, oldAvalCoordinate.y, 1e-3);
  }

  @Test
  public void testProcess() {
    LayerBrancheController brancheController = panel.getPlanimetryController().getBrancheController();
    brancheController.setEditable(true);
    checkAmontAvalNoeuds();
    Coordinate oldAmontCoordinate = brancheController.getAmontCoordinate(0);
    Coordinate oldAvalCoordinate = brancheController.getAvalCoordinate(0);
    //la sélection
    panel.getPlanimetryController().getCqBranche().setSelection(new int[]{0});
    lineLayer.setSelection(new int[]{0});
    ProjectBrancheOnLineProcess process = new ProjectBrancheOnLineProcess(panel);
    process.process();
    Coordinate amontCoordinate = brancheController.getAmontCoordinate(0);
    Coordinate avalCoordinate = brancheController.getAvalCoordinate(0);
    assertEquals(0, amontCoordinate.x, 1e-3);
    assertEquals(oldAmontCoordinate.y, amontCoordinate.y, 1e-3);
    assertEquals(0, avalCoordinate.x, 1e-3);
    assertEquals(oldAvalCoordinate.y, avalCoordinate.y, 1e-3);
    GISPolyligne brancheGis = brancheController.getBrancheGis(0);
    assertEquals(3, brancheGis.getNumPoints());
    Coordinate coordinateN = brancheGis.getCoordinateN(1);
    assertEquals(0, coordinateN.x, 1e-3);
    assertEquals(50, coordinateN.y, 1e-3);
  }

  @Test
  public void testIsEnable() {
    panel.clearSelection();
    ProjectBrancheOnLineProcess process = new ProjectBrancheOnLineProcess(panel);
    assertFalse(process.isEnable());
    lineLayer.setSelection(new int[]{0});
    assertFalse(process.isEnable());
    panel.getPlanimetryController().getCqBranche().setSelection(new int[]{0});
    assertTrue("selection ok :1 branche and 1 line", process.isEnable());
  }

  @Before
  public void createPanel() {
    EMHScenario scenario = ScenarioBuilderForTest.createDefaultScenario(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    assertEquals(1, scenario.getBranches().size());
    NetworkBuilder builder = new NetworkBuilder();
    IdRegistry.install(scenario);
    panel = builder.buildCalque(new CtuluUIDefault(), scenario, new VisuConfiguration(),
            TestCoeurConfig.INSTANCE.getCrueConfigMetier(), true);
    ContainerActivityVisibility activity = new ContainerActivityVisibility();
    activity.initialize(scenario);
    panel.getPlanimetryController().setContainerActivityVisibility(activity);
    ligne = new GISZoneCollectionLigneBrisee();
    ligne.addPolyligne(GISGeometryFactory.INSTANCE.createLineString(new Coordinate[]{new Coordinate(0, 0), new Coordinate(0, 50), new Coordinate(0,
      200)}), null);
    lineLayer = (PlanimetryLigneBriseeExternUnmodifiableLayer) panel.getPlanimetryController().getGroupExternController().addLayerLignesUnmodifiable(
            ligne, new File("test.txt"), EnumFormat.SHP, GroupeExternSIGType.UNMODIFIABLE);
  }
}
