/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import gnu.trove.TIntObjectHashMap;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class LimGeoHelperTest {

  public LimGeoHelperTest() {
  }

  @Test
  public void testCreateMap() {
    TIntObjectHashMap<String> createMap = LimGeoHelper.createMap();
    assertEquals(LimGeoHelper.AXE, createMap.get(4));
    assertEquals(LimGeoHelper.THALWEG, createMap.get(3));
  }
}