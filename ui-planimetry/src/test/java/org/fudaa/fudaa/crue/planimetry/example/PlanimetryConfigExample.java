package org.fudaa.fudaa.crue.planimetry.example;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.fudaa.fudaa.crue.planimetry.ConfigurationUI;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;

/**
 *
 * @author deniger
 */
public class PlanimetryConfigExample {

  public static void main(String[] args) {
    JDialog dial = new JDialog();
    dial.setContentPane(new JLabel("Testttttttttttt"));
    dial.pack();
    dial.setModal(false);
    dial.setLocation(0, 0);
    dial.setVisible(true);
    JFrame fr = new JFrame();
    ConfigurationUI ui = new ConfigurationUI();
    ui.setVisuConfiguration(new VisuConfiguration(),null,null);
    fr.setContentPane(ui);
    fr.setSize(300, 500);
    fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    fr.setVisible(true);
  }
}
