package org.fudaa.fudaa.crue.planimetry.example;

import com.memoire.fu.FuLib;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluUIDefault;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.edition.EditionDelete;
import org.fudaa.dodico.crue.io.test.ReadHelperForTest;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanelBuilder;
import org.fudaa.fudaa.crue.common.ContainerActivityVisibility;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.layout.NetworkBuilder;
import org.fudaa.fudaa.crue.planimetry.layout.NetworkGisPositionnerResult;
import org.fudaa.fudaa.crue.planimetry.save.PlanimetryGisLoader;
import org.fudaa.fudaa.crue.planimetry.save.PlanimetryGisSaver;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.List;

/**
 *
 * @author deniger
 */
public class PlanimetryLauncherExample {

  public static void main(String[] args) {
    final EMHScenario scenario = new ReadHelperForTest(TestCoeurConfig.INSTANCE).readModeleAnsSetRelation(new CtuluLog(),
        "/M3-0_c9.dc", "/M3-0_c9.dh");
    NetworkBuilder builder = new NetworkBuilder();
    IdRegistry.install(scenario);
    final PlanimetryVisuPanel panel = builder.buildCalque(new CtuluUIDefault(), scenario, new VisuConfiguration(),
            TestCoeurConfig.INSTANCE.getCrueConfigMetier(), true);
    ContainerActivityVisibility activity = new ContainerActivityVisibility();
    activity.initialize(scenario);
    panel.getPlanimetryController().setContainerActivityVisibility(activity);
    Calc calc = scenario.getDonCLimMScenario().getCalc().get(0);
    panel.getPlanimetryController().setEditedCalcul(calc.getUiId());
    final JFrame frame = new JFrame();

    JComponent buildInfosCreationComposant = panel.createGisEditionPalette();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    JPanel cp = new JPanel(new BorderLayout());
    cp.add(PlanimetryVisuPanelBuilder.buildTopComponent(panel), BorderLayout.NORTH);
    cp.add(buildInfosCreationComposant, BorderLayout.WEST);
    final JComponent createOverview = PlanimetryVisuPanelBuilder.createOverview(panel);
    cp.add(createOverview, BorderLayout.EAST);
    final PlanimetryController planimetryController = panel.getPlanimetryController();
    final JCheckBox cb = new JCheckBox("Editable");
    cb.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        planimetryController.setEditable(cb.isSelected());
        planimetryController.getGroupExternController().setArbreEditable(cb.isSelected());
      }
    });
    JButton save = new JButton("save");
    save.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        planimetryController.saveDone();
      }
    });

    JButton cancel = new JButton("Cancel");
    cancel.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        planimetryController.cancel();
      }
    });
    JButton export = new JButton("Export and loadLHPT");
    export.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        PlanimetryGisSaver saver = new PlanimetryGisSaver(planimetryController);
        File f = new File(FuLib.getJavaTmp());
        saver.save(f);
        System.err.println("file= " + f);
        PlanimetryGisLoader loader = new PlanimetryGisLoader();
        NetworkGisPositionnerResult load1 = loader.load(f, planimetryController.getScenario());
        planimetryController.validLoading(load1);
      }
    });
    JButton modify = new JButton("modify");
    modify.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        EMHScenario scenario1 = planimetryController.getScenario();
        List<CatEMHBranche> branches = scenario1.getBranches();
        if (branches.isEmpty()) {
          return;
        }
        CatEMHBranche branche = branches.get(0);
        EditionDelete.removeBranche(branche);
        scenario1.getIdRegistry().unregister(branche);
        branches = scenario1.getBranches();
        if (!branches.isEmpty()) {
          branche = branches.get(0);
          branche.setNom("Br_New");
        }
        CatEMHNoeud ndAmont = new EMHNoeudNiveauContinu("test_amont");
        scenario1.getIdRegistry().register(ndAmont);
        CatEMHNoeud ndAval = new EMHNoeudNiveauContinu("test_aval");
        scenario1.getIdRegistry().register(ndAval);
        EMHBrancheEnchainement newBranche = new EMHBrancheEnchainement("Br_test");
        scenario1.getIdRegistry().register(newBranche);
        newBranche.setNoeudAmont(ndAmont);
        newBranche.setNoeudAval(ndAval);
        final EMHSousModele ssModele = scenario1.getModeles().get(0).getSousModeles().get(0);
        EMHRelationFactory.addRelationContientEMH(ssModele, ndAmont);
        EMHRelationFactory.addRelationContientEMH(ssModele, ndAval);
        EMHRelationFactory.addRelationContientEMH(ssModele, newBranche);
        planimetryController.reload(scenario1);
      }
    });
    JPanel bottom = new JPanel(new FlowLayout(FlowLayout.LEFT));
    bottom.add(panel.getEditZonePanel().getPanel());
    bottom.add(export);
    bottom.add(modify);
    bottom.add(cb);
    bottom.add(save);
    bottom.add(cancel);

    final JLabel lb = new JLabel();
    planimetryController.getState().addModifiedListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        lb.setText(planimetryController.getState().isModified() ? "Modified" : "Not modifed");
      }
    });
    lb.setText("State");
    bottom.add(lb);
    cp.add(bottom, BorderLayout.SOUTH);
    panel.setPreferredSize(new Dimension(600, 700));
    cp.add(panel);
    frame.setContentPane(cp);
    frame.setPreferredSize(new Dimension(1000, 800));
    frame.setSize(new Dimension(1000, 800));
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        frame.setVisible(true);
      }
    });
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        panel.setEditable(false);
        panel.restaurer();
      }
    });

  }
}
