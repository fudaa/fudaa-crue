package org.fudaa.fudaa.crue.planimetry.example;

import java.util.List;
import org.fudaa.fudaa.crue.planimetry.configuration.BrancheConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.configuration.CasierConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.configuration.NodeConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.configuration.SectionConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.configuration.TraceConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfigurationInfo;
import org.openide.nodes.Node.Property;
import org.openide.nodes.Sheet.Set;

/**
 *
 * @author deniger
 */
public class PlanimetryEtudeConfigurationExample {

  private static void printSet(Set set) {
    Property<?>[] properties = set.getProperties();
    for (Property<?> property : properties) {
      System.err.println("  " + set.getName() + "." + property.getName());
    }
  }

  private static void printSet(List<Set> set) {
    for (Set set1 : set) {
      printSet(set1);
    }
  }

  public static void main(String[] args) {
    final VisuConfiguration visuConfiguration = new VisuConfiguration();
    Set generalConfig = VisuConfigurationInfo.createSet(visuConfiguration);
    List<Set> sectionConfig = SectionConfigurationInfo.createSet(visuConfiguration.getSectionConfiguration());
    List<Set> traceConfig = TraceConfigurationInfo.createSet(visuConfiguration.getTraceConfiguration());
    List<Set> nodeConfig = NodeConfigurationInfo.createSet(visuConfiguration.getNodeConfiguration());
    List<Set> brancheConfig = BrancheConfigurationInfo.createSet(visuConfiguration.getBrancheConfiguration());
    List<Set> casierConfig = CasierConfigurationInfo.createSet(visuConfiguration.getCasierConfiguration());
//    System.err.println("-----------------------");
//    System.err.println("General config");
    printSet(generalConfig);
//    System.err.println("-----------------------");
//    System.err.println("Section Config");
    printSet(sectionConfig);
//    System.err.println("-----------------------");
//    System.err.println("Trace Config");
    printSet(traceConfig);
//    System.err.println("-----------------------");
//    System.err.println("Node Config");
    printSet(nodeConfig);
//    System.err.println("-----------------------");
//    System.err.println("Branche Config");
    printSet(brancheConfig);
//    System.err.println("-----------------------");
//    System.err.println("Casier Config");
    printSet(casierConfig);

  }
}
