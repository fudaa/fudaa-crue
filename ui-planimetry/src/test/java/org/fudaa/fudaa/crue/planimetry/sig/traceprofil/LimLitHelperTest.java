/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import gnu.trove.TIntObjectHashMap;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class LimLitHelperTest {

  public LimLitHelperTest() {
  }

  @Test
  public void testCreateMap() {
    TIntObjectHashMap<String> createMap = LimLitHelper.createMap();
    assertEquals(LimLitHelper.RD_STOD, createMap.get(1));
    assertEquals(LimLitHelper.STOD_MAJD, createMap.get(2));
    assertEquals(LimLitHelper.MAJD_MIN, createMap.get(3));
    assertEquals(LimLitHelper.MIN_MAJG, createMap.get(4));
    assertEquals(LimLitHelper.MAJG_STOG, createMap.get(5));
    assertEquals(LimLitHelper.STOG_RG, createMap.get(6));
  }
}