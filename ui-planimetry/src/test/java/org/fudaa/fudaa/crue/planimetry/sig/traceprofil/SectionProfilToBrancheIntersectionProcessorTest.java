/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import org.locationtech.jts.geom.Coordinate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class SectionProfilToBrancheIntersectionProcessorTest {

  public SectionProfilToBrancheIntersectionProcessorTest() {
  }

  @Test
  public void testIntesection() {
    assertEquals(this, this);
    SectionProfilToBrancheProcessorIntersection processor = new SectionProfilToBrancheProcessorIntersection();
    List<SectionProfilToBrancheParametersSection> sectionList = new ArrayList<>();
    SectionProfilToBrancheParametersSection section1 = new SectionProfilToBrancheParametersSection("St_1");
    section1.setLitMineur(new GrSegment(new GrPoint(-1, 2, 0), new GrPoint(1, 2, 0)));
    sectionList.add(section1);

    SectionProfilToBrancheParametersSection section2 = new SectionProfilToBrancheParametersSection("St_2");
    section2.setLitMineur(new GrSegment(new GrPoint(-1, 9, 0), new GrPoint(1, 9, 0)));
    sectionList.add(section2);

    //ne sera pas prise en compte
    SectionProfilToBrancheParametersSection section3 = new SectionProfilToBrancheParametersSection("St_NotUsed");
    section3.setLitMineur(new GrSegment(new GrPoint(-1, 11, 0), new GrPoint(1, 11, 0)));
    sectionList.add(section3);

    SectionProfilToBrancheParametersBranche branches = new SectionProfilToBrancheParametersBranche();
    Coordinate[] branchesPt = new Coordinate[]{new Coordinate(0, 0), new Coordinate(0, 5), new Coordinate(0, 10)};
    GISPolyligne branche = GISGeometryFactory.INSTANCE.createLineString(branchesPt);
    final String brancheName = "Br_1";
    branches.add(brancheName, branche);

    Coordinate[] branchesPtNotUsed = new Coordinate[]{new Coordinate(-5, 0), new Coordinate(-5, 5), new Coordinate(-5, 10)};
    GISPolyligne branchesNotUsed = GISGeometryFactory.INSTANCE.createLineString(branchesPtNotUsed);
    branches.add("Br_NotUsed", branchesNotUsed);

    SectionProfilToBrancheParametersComputed res = processor.process(branches, sectionList);
    Map<SectionProfilToBrancheParametersSection, SectionProfilToBrancheParametersComputed.IntersectedBranche> targetsBrancheByParameter = res.
            getTargetsBrancheByParameter();
    assertEquals(2, targetsBrancheByParameter.size());
    final Iterator<Map.Entry<SectionProfilToBrancheParametersSection, SectionProfilToBrancheParametersComputed.IntersectedBranche>> iterator = targetsBrancheByParameter.
            entrySet().iterator();
    Map.Entry<SectionProfilToBrancheParametersSection, SectionProfilToBrancheParametersComputed.IntersectedBranche> first = iterator.next();
    assertEquals(section1.getSectionName(), first.getKey().getSectionName());
    SectionProfilToBrancheParametersComputed.IntersectedBranche value = first.getValue();
    assertEquals(1, value.getBrancheNames().size());
    assertEquals(brancheName, value.getBrancheNames().get(0).getBrancheName());
    assertEquals(2d, value.getBrancheNames().get(0).getXpSchematique(), 1e-10);

    Map.Entry<SectionProfilToBrancheParametersSection, SectionProfilToBrancheParametersComputed.IntersectedBranche> second = iterator.next();
    assertEquals(section2.getSectionName(), second.getKey().getSectionName());
    value = second.getValue();
    assertEquals(1, value.getBrancheNames().size());
    assertEquals(brancheName, value.getBrancheNames().get(0).getBrancheName());
    assertEquals(9d, value.getBrancheNames().get(0).getXpSchematique(), 1e-10);

  }
}
