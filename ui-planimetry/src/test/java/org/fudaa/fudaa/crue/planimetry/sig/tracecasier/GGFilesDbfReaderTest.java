/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.tracecasier;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class GGFilesDbfReaderTest {

  public GGFilesDbfReaderTest() {
  }
  private static File targetDir;
  private static File dbfFile;

  @BeforeClass
  public static void extractFile() throws IOException {
    targetDir = CtuluLibFile.createTempDir();
    dbfFile = new File(targetDir, "CalculCasier.dbf");
    CtuluLibFile.getFileFromJar("/ggcrue/CalculCasier.dbf", dbfFile);
  }

  @AfterClass
  public static void deleteFile() {
    CtuluLibFile.deleteDir(targetDir);
  }

  @Test
  public void testRead() {
    GGFilesDbfReader reader = new GGFilesDbfReader(dbfFile, PropertyEpsilon.DEFAULT, PropertyEpsilon.DEFAULT);
    CtuluLogResult<Map<String, TraceCasierProfilContent>> read = reader.read();
    assertFalse(read.getLog().containsErrorOrSevereError());
    final Map<String, TraceCasierProfilContent> resultat = read.getResultat();
    assertNotNull(resultat);
    assertEquals(3, resultat.size());
    assertTrue(resultat.containsKey("Ca_PCUSINE"));
    assertTrue(resultat.containsKey("Ca_PC13"));
    assertTrue(resultat.containsKey("Ca_PC14"));

    //Ca_PCUSINE
    TraceCasierProfilContent content = resultat.get("Ca_PCUSINE");
    assertEquals(818.209468284, content.getDistance(), 1e-10);
    List<TraceCasierProfilContentItem> sortedItems = content.getSortedItems();
    assertEquals(1, sortedItems.size());
    TraceCasierProfilContentItem item = sortedItems.get(0);
    assertEquals(1.24659938417, item.getXt(), 1e-10);
    assertEquals(210.559997559, item.getZ(), 1e-10);

    content = resultat.get("Ca_PC13");
    assertEquals(942.304836813, content.getDistance(), 1e-10);
    sortedItems = content.getSortedItems();
    assertEquals(6, sortedItems.size());
    item = sortedItems.get(0);
    assertEquals(9.05393368385, item.getXt(), 1e-10);
    assertEquals(210.220001221, item.getZ(), 1e-10);
    item = sortedItems.get(sortedItems.size() - 1);
    assertEquals(801.100231248, item.getXt(), 1e-10);
    assertEquals(215.220001221, item.getZ(), 1e-10);

    content = resultat.get("Ca_PC14");
    assertEquals(607.67759522, content.getDistance(), 1e-10);
    sortedItems = content.getSortedItems();
    assertEquals(8, sortedItems.size());
    item = sortedItems.get(0);
    assertEquals(0, item.getXt(), 1e-10);
    assertEquals(211.509994507, item.getZ(), 1e-10);
    item = sortedItems.get(sortedItems.size() - 1);
    assertEquals(606.663306354, item.getXt(), 1e-10);
    assertEquals(218.509994507, item.getZ(), 1e-10);
  }
}