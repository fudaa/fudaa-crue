package org.fudaa.fudaa.crue.planimetry.configuration;

import junit.framework.TestCase;
import org.junit.Test;
import org.locationtech.jts.util.Assert;

public class DefaultConfigurationTest {
  @Test
  public void testCopy() {

    DefaultConfiguration init=new DefaultConfiguration();
    init.setTransparenceAlpha(34);
    Assert.equals(init.getTransparenceAlpha(),init.copy().getTransparenceAlpha());
  }
}