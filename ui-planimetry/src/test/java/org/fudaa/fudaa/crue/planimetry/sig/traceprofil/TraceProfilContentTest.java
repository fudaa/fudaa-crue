/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class TraceProfilContentTest {

  public TraceProfilContentTest() {
  }

  private static TraceProfilContentItem create(List<TraceProfilContentItem> items, double x) {
    TraceProfilContentItem item1 = new TraceProfilContentItem();
    item1.x = x;
    items.add(item1);
    return item1;
  }

  /**
   * ON teste que la transposition de coordonnées fonctionne.
   */
  @Test
  public void testTranspose() {
    List<TraceProfilContentItem> items = new ArrayList<>();
    TraceProfilContentItem item1 = create(items, -1);
    TraceProfilContentItem item2 = create(items, 2);
    TraceProfilContentItem item3 = create(items, 10);
    TraceProfilContentItem item4 = create(items, 500);
    List<TraceProfilContentItem> transpose = TraceProfilContent.transpose(items);
    assertFalse(transpose == items);
    assertEquals(transpose.size(), items.size());
    for (int i = 0; i < transpose.size(); i++) {
      assertTrue(transpose.get(i) == items.get(transpose.size() - i - 1));
    }
    assertEquals(-1, item4.getX(), 1e-5);
    assertEquals(-1 + 490, item3.getX(), 1e-5);
    assertEquals(-1 + 490 + 8, item2.getX(), 1e-5);
    assertEquals(500, item1.getX(), 1e-5);
  }
}