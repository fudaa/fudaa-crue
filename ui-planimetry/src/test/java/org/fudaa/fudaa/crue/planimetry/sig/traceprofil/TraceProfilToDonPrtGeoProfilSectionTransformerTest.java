/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import java.io.File;
import java.io.IOException;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.IdRegistry;
import org.fudaa.fudaa.crue.planimetry.ScenarioBuilderForTests;
import org.fudaa.fudaa.crue.planimetry.sig.SigHelperForTest;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class TraceProfilToDonPrtGeoProfilSectionTransformerTest {

  private static File targetDir;
  private static File shapeFile;

  @BeforeClass
  public static void extractFile() throws IOException {
    targetDir = CtuluLibFile.createTempDir();
    shapeFile = new File(targetDir, "EMG_Profil_point_canal_de_fuite.shp");
    File dbfFile = new File(targetDir, "EMG_Profil_point_canal_de_fuite.dbf");
    CtuluLibFile.getFileFromJar("/ggcrue/EMG_Profil_point_canal_de_fuite.shp", shapeFile);
    CtuluLibFile.getFileFromJar("/ggcrue/EMG_Profil_point_canal_de_fuite.dbf", dbfFile);
  }

  @AfterClass
  public static void deleteFile() {
    CtuluLibFile.deleteDir(targetDir);
  }

  @Test
  public void testPreload() {
    EMHScenario scenario = ScenarioBuilderForTests.buildDefaultScenario();
    IdRegistry.install(scenario);
    EMHSousModele sousModele = scenario.getModeles().get(0).getSousModeles().get(0);
    assertNotNull(sousModele);
    CtuluLogResult<GISZoneCollectionLigneBrisee> loadProfilTrace = SigHelperForTest.loadProfilTrace(shapeFile);
    TraceProfilToSectionPreloadProcessor processor = new TraceProfilToSectionPreloadProcessor(scenario);
    if (loadProfilTrace.getLog().containsErrorOrSevereError()) {
      loadProfilTrace.getLog().printResume();
    }
    TraceProfilSousModeleContent preload = processor.preload(loadProfilTrace.getResultat(), null, null);
    assertEquals(loadProfilTrace.getResultat().getNumGeometries(), preload.traceProfilSousModeles.size());
    assertEquals(0, preload.traceProfilPositionByName.get((String) loadProfilTrace.getResultat().getModel(0).getObjectValueAt(0)));
    assertNull(preload.traceProfilSousModeles.get(0).getSousModeleName());

  }

}