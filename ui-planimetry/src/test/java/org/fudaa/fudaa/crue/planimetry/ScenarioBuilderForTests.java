package org.fudaa.fudaa.crue.planimetry;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.EMHBranchePdc;
import org.fudaa.dodico.crue.metier.emh.EMHCasierMNT;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHNoeudNiveauContinu;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSectionProfil;
import org.fudaa.dodico.crue.metier.emh.EMHSectionSansGeometrie;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumPosSection;
import org.fudaa.dodico.crue.metier.emh.LitNumerote;
import org.fudaa.dodico.crue.metier.emh.PtProfil;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.factory.EMHNoeudFactory;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;

/**
 *
 * @author deniger
 */
public class ScenarioBuilderForTests {

  public static EMHScenario buildDefaultScenario() {
    EMHScenario scenario = new EMHScenario();
    EMHModeleBase modele = new EMHModeleBase();
    EMHSousModele sousModele = new EMHSousModele();
    EMHRelationFactory.addRelationContientEMH(scenario, modele);
    EMHRelationFactory.addRelationContientEMH(modele, sousModele);
    EMHNoeudFactory fac = new EMHNoeudFactory();
    List<EMHNoeudNiveauContinu> nds = new ArrayList<>();
    //nd1---Br_1:100----nd2--Br_2:500-------------nd3
    //                   |
    //                   |---Br_3:200--nd4
    //                   |
    //                   |---Br_4:300------nd5
    //
    //nd6---Br_5:250---nd7
    CatEMHNoeud nd1 = createNode(fac, "Nd_1", sousModele);
    CatEMHNoeud nd2 = createNode(fac, "Nd_2", sousModele);
    CatEMHNoeud nd3 = createNode(fac, "Nd_3", sousModele);
    CatEMHNoeud nd4 = createNode(fac, "Nd_4", sousModele);
    CatEMHNoeud nd5 = createNode(fac, "Nd_5", sousModele);
    CatEMHNoeud nd6 = createNode(fac, "Nd_6", sousModele);
    CatEMHNoeud nd7 = createNode(fac, "Nd_7", sousModele);
    createCasier(nd1, sousModele);
    createCasier(nd3, sousModele);
    createCasier(nd5, sousModele);
    addBranche("Br_1", nd1, nd2, 100, sousModele, 2);
    addBranche("Br_2", nd2, nd3, 500, sousModele, 10);
    addBranche("Br_3", nd2, nd4, 200, sousModele, 0);
    addBranche("Br_4", nd2, nd5, 300, sousModele, 4);
    addBranche("Br_5", nd6, nd7, 300, sousModele, 20);
    return scenario;
  }

  protected static void addSection(EMHSectionProfil section) {
    DonPrtGeoProfilSection profil = new DonPrtGeoProfilSection();
    profil.setNom("Prof_" + section.getNom());
    section.addInfosEMH(profil);
    List<PtProfil> profils = new ArrayList<>();
    List<LitNumerote> litNumerotes = new ArrayList<>();
    double min = Math.random() * 10;
    double max = 100 - Math.random() * 10;
    int nbInterval = 20;
    double delta = (max - min) / nbInterval;
    profils.add(new PtProfil(min, Math.random() * 50));
    for (int i = 0; i < nbInterval; i++) {
      double x = min + Math.random() * delta + (i * delta);
      profils.add(new PtProfil(x, Math.random() * 50));
    }
    profils.add(new PtProfil(max, Math.random() * 50));
    litNumerotes.add(create(profils, 0, 2, false, "Stockage droit"));
    litNumerotes.add(create(profils, 2, 4, false, "Majeur droit"));
    litNumerotes.add(create(profils, 4, 10, true, "mineur droit"));
    litNumerotes.add(create(profils, 10, 14, true, "mineur gauche"));
    litNumerotes.add(create(profils, 14, 16, false, "majeur gauche"));
    litNumerotes.add(create(profils, 14, profils.size() - 1, false, "Stockage gauche"));
    profil.setPtProfil(profils);
    profil.setLitNumerote(litNumerotes);
  }

  protected static LitNumerote create(List<PtProfil> profils, int idxDeb, int idxFin, boolean mineure, String nom) {
    LitNumerote stoDroit = new LitNumerote();
    stoDroit.setLimDeb(profils.get(idxDeb));
    stoDroit.setLimFin(profils.get(idxFin));
    stoDroit.setIsLitMineur(mineure);
    stoDroit.setNomLit(new LitNomme(nom, 0));
    return stoDroit;

  }

  private static void addBranche(final String brName, CatEMHNoeud nd1, CatEMHNoeud nd2, double length, EMHSousModele sousModele,
          int nbSection) {
    CatEMHBranche br = new EMHBranchePdc(brName);
    br.setUserActive(true);
    br.setNoeudAmont(nd1);
    br.setNoeudAval(nd2);
    addSections(sousModele, br, length, nbSection);
    EMHRelationFactory.addRelationContientEMH(sousModele, br);
  }

  private static void addSections(EMHSousModele sousModele, CatEMHBranche br, double length, int nbSection) {
    EMHSectionSansGeometrie sectionAmont = new EMHSectionSansGeometrie("St_" + br.getNom() + "_Am");
    EMHSectionSansGeometrie sectionAval = new EMHSectionSansGeometrie("St_" + br.getNom() + "_Av");
    RelationEMHSectionDansBranche sectionAmontRelation = EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(br,
            false, sectionAmont,
            CrueConfigMetierForTest.DEFAULT);
    RelationEMHSectionDansBranche sectionAvalRelation = EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(br,
            false, sectionAval,
            CrueConfigMetierForTest.DEFAULT);
    sectionAmontRelation.setPos(EnumPosSection.AMONT);
    sectionAmontRelation.setXp(0);
    sectionAvalRelation.setPos(EnumPosSection.AVAL);
    sectionAvalRelation.setXp(length);
    List<RelationEMHSectionDansBranche> sections = new ArrayList<>();
    sections.add(sectionAmontRelation);
    if (nbSection > 0) {
      double delta = length / (nbSection + 1);
      double x = delta;
      for (int i = 0; i < nbSection; i++) {
        EMHSectionProfil section = new EMHSectionProfil("St_" + br.getNom() + "_" + i);
        addSection(section);
        RelationEMHSectionDansBranche sectionRelation = EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(br,
                false, section,
                CrueConfigMetierForTest.DEFAULT);
        sectionRelation.setPos(EnumPosSection.INTERNE);
        sectionRelation.setXp(x);
        sections.add(sectionRelation);
        if (i == 3) {
          section = new EMHSectionProfil("St_" + br.getNom() + "_" + i + "bis");
          addSection(section);
          sectionRelation = EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(br,
                  false, section,
                  CrueConfigMetierForTest.DEFAULT);
          sectionRelation.setPos(EnumPosSection.INTERNE);
          sectionRelation.setXp(x);
          sections.add(sectionRelation);
        }

        x = x + delta;
      }
    }
    sections.add(sectionAvalRelation);
    for (RelationEMHSectionDansBranche relationEMHSectionDansBranche : sections) {
      EMHRelationFactory.addRelationContientEMH(sousModele, relationEMHSectionDansBranche.getEmh());
    }
    br.addListeSections(sections);
  }

  private static CatEMHNoeud createNode(EMHNoeudFactory fac, final String noeudName, EMHSousModele sousModele) {
    CatEMHNoeud nd1 = fac.getCatEMHNoeud(noeudName);
    EMHRelationFactory.addRelationContientEMH(sousModele, nd1);
    return nd1;
  }

  private static CatEMHCasier createCasier(CatEMHNoeud noeud, EMHSousModele sousModele) {
    EMHCasierMNT casier = new EMHCasierMNT("Ca_" + noeud.getNom());
    casier.setNoeud(noeud);
    casier.setUserActive(true);
    EMHRelationFactory.addRelationContientEMH(sousModele, casier);
    return casier;
  }
}
