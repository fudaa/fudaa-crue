/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.configuration;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPerm;
import org.fudaa.dodico.crue.metier.emh.CalcTrans;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class EnumTypeIconTest {

  public EnumTypeIconTest() {
  }

  @Test
  public void testValues() {
    EnumTypeIcon[] values = EnumTypeIcon.values();
    Set<Class> supported = new HashSet<>();
    for (EnumTypeIcon enumTypeIcon : values) {
      supported.addAll(enumTypeIcon.getDclmClasses());
    }
    Set<Class> defined = getDclmClasses();
    assertEquals(defined.size(), supported.size());
    for (Class dclmClass : defined) {
      assertTrue(supported.contains(dclmClass));

    }
  }

  @Test
  public void testgetEnumByDclm() {
    Map<Class, EnumTypeIcon> values = EnumTypeIcon.getEnumByDclm();
    Set<Class> defined = getDclmClasses();
    assertEquals(defined.size(), values.size());
    for (Class dclmClass : defined) {
      assertTrue(values.containsKey(dclmClass));

    }
  }

  public static Set<Class> getDclmClasses() {
    Set<Class> defined = new HashSet<>();
    defined.addAll(CalcPseudoPerm.getDclmClassInOrder());
    defined.addAll(CalcTrans.getDclmClassInOrder());
    return defined;
  }
}
