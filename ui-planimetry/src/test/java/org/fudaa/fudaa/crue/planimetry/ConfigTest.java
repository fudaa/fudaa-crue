/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.planimetry;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import org.fudaa.dodico.crue.projet.planimetry.LayerVisibility;
import org.fudaa.fudaa.crue.common.config.ConfigSaverHelper;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.save.ConfigSaverHdydaulicAndVisibility;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.openide.nodes.Node;

/**
 *
 * @author deniger
 */
public class ConfigTest {

  public ConfigTest() {
  }

  public List<String> getExpectedProperties() {
    List<String> res = new ArrayList<>();
    res.add("general.minimalDistanceBetweenNode");
    res.add("general.minimalDistanceBeetweenParallelEdges");
    res.add("general.defaultCasierRealDimension");
    res.add("general.inactiveEMHVisible");
    res.add("general.inactiveEMHColor");
    res.add("sections.visible");
    res.add("sections.transparenceAlpha");
    res.add("sections.icon.type");
    res.add("sections.icon.taille");
    res.add("sections.icon.epaisseurLigne");
    res.add("sections.icon.couleur");
    res.add("sections.icon.backgroundColor");
    res.add("sections.icon.backgroundColorPainted");
    res.add("sections.label.displayLabels");
    res.add("sections.label.labelDistance");
    res.add("sections.label.labelFont");
    res.add("sections.label.labelAlignment");
    res.add("sections.label.labelColor");
    res.add("sections.colors.EMHSectionIdem");
    res.add("sections.colors.EMHSectionInterpolee");
    res.add("sections.colors.EMHSectionProfil");
    res.add("sections.colors.EMHSectionSansGeometrie");
    res.add("traces.visible");
    res.add("traces.transparenceAlpha");
    res.add("traces.profilVueDeAmont");
    res.add("traces.line.typeTrait");
    res.add("traces.line.epaisseur");
    res.add("traces.line.couleur");
    res.add("traces.label.sectionLabelPosition");
    res.add("traces.label.displayLabels");
    res.add("traces.label.labelDistance");
    res.add("traces.label.labelFont");
    res.add("traces.label.labelAlignment");
    res.add("traces.label.labelColor");
    res.add("traces.lits.icon.type");
    res.add("traces.lits.icon.taille");
    res.add("traces.lits.icon.epaisseurLigne");
    res.add("traces.lits.icon.couleur");
    res.add("traces.lits.icon.backgroundColor");
    res.add("traces.lits.icon.backgroundColorPainted");

    res.add("traces.ptProfil.icon.type");
    res.add("traces.ptProfil.icon.taille");
    res.add("traces.ptProfil.icon.epaisseurLigne");
    res.add("traces.ptProfil.icon.couleur");
    res.add("traces.ptProfil.icon.backgroundColor");
    res.add("traces.ptProfil.icon.backgroundColorPainted");

    res.add("traces.lits.label.displayLabels");
    res.add("traces.lits.label.labelDistance");
    res.add("traces.lits.label.labelFont");
    res.add("traces.lits.label.labelAlignment");
    res.add("traces.lits.label.labelColor");
    res.add("noeuds.visible");
    res.add("noeuds.transparenceAlpha");
    res.add("noeuds.icon.type");
    res.add("noeuds.icon.taille");
    res.add("noeuds.icon.epaisseurLigne");
    res.add("noeuds.icon.couleur");
    res.add("noeuds.icon.backgroundColor");
    res.add("noeuds.icon.backgroundColorPainted");
    res.add("noeuds.label.displayCasierLabel");
    res.add("noeuds.label.displayLabels");
    res.add("noeuds.label.labelDistance");
    res.add("noeuds.label.labelFont");
    res.add("noeuds.label.labelAlignment");
    res.add("noeuds.label.labelColor");
    res.add("branches.visible");
    res.add("branches.transparenceAlpha");
    res.add("branches.displaySens");
    res.add("branches.flecheSize");
    res.add("branches.line.typeTrait");
    res.add("branches.line.epaisseur");
    res.add("branches.mainIcon.icon.type");
    res.add("branches.mainIcon.icon.taille");
    res.add("branches.mainIcon.icon.epaisseurLigne");
    res.add("branches.atomic.icon.type");
    res.add("branches.atomic.icon.taille");
    res.add("branches.atomic.icon.epaisseurLigne");
    res.add("branches.atomic.icon.couleur");
    res.add("branches.atomic.icon.backgroundColor");
    res.add("branches.atomic.icon.backgroundColorPainted");
    res.add("branches.label.displayLabels");
    res.add("branches.label.labelDistance");
    res.add("branches.label.labelFont");
    res.add("branches.label.labelAlignment");
    res.add("branches.label.labelColor");
    res.add("branches.colors.EMHBrancheBarrageFilEau");
    res.add("branches.colors.EMHBrancheBarrageGenerique");
    res.add("branches.colors.EMHBrancheNiveauxAssocies");
    res.add("branches.colors.EMHBrancheOrifice");
    res.add("branches.colors.EMHBranchePdc");
    res.add("branches.colors.EMHBrancheSaintVenant");
    res.add("branches.colors.EMHBrancheSeuilLateral");
    res.add("branches.colors.EMHBrancheSeuilTransversal");
    res.add("branches.colors.EMHBrancheStrickler");
    res.add("casiers.visible");
    res.add("casiers.transparenceAlpha");
    res.add("casiers.fondPainted");
    res.add("casiers.fond");
    res.add("casiers.line.typeTrait");
    res.add("casiers.line.epaisseur");
    res.add("casiers.line.couleur");
    res.add("casiers.atomic.icon.type");
    res.add("casiers.atomic.icon.taille");
    res.add("casiers.atomic.icon.epaisseurLigne");
    res.add("casiers.atomic.icon.couleur");
    res.add("casiers.atomic.icon.backgroundColor");
    res.add("casiers.atomic.icon.backgroundColorPainted");
    res.add("casiers.label.displayLabels");
    res.add("casiers.label.labelDistance");
    res.add("casiers.label.labelFont");
    res.add("casiers.label.labelAlignment");
    res.add("casiers.label.labelColor");
    res.add("condLimite.visible");
    res.add("condLimite.transparenceAlpha");
    res.add("condLimite.iconDistance");
    res.add("condLimite.positions.qapp");
    res.add("condLimite.positions.zimp");
    res.add("condLimite.positions.tarage");
    res.add("condLimite.positions.manoeuvre");
    res.add("condLimite.positions.qruis");
    res.add("condLimite.positions.manoeuvre-regul");
    res.add("condLimite.positions.usine");
    res.add("condLimite.positions.bg1");
    res.add("condLimite.positions.bg1av");
    res.add("condLimite.positions.bg2");
    res.add("condLimite.positions.bg2av");
    res.add("condLimite.qapp.label.displayLabels");
    res.add("condLimite.qapp.label.labelDistance");
    res.add("condLimite.qapp.label.labelFont");
    res.add("condLimite.qapp.label.labelAlignment");
    res.add("condLimite.qapp.label.labelColor");
    res.add("condLimite.zimp.label.displayLabels");
    res.add("condLimite.zimp.label.labelDistance");
    res.add("condLimite.zimp.label.labelFont");
    res.add("condLimite.zimp.label.labelAlignment");
    res.add("condLimite.zimp.label.labelColor");
    res.add("condLimite.tarage.label.displayLabels");
    res.add("condLimite.tarage.label.labelDistance");
    res.add("condLimite.tarage.label.labelFont");
    res.add("condLimite.tarage.label.labelAlignment");
    res.add("condLimite.tarage.label.labelColor");
    res.add("condLimite.manoeuvre.label.displayLabels");
    res.add("condLimite.manoeuvre.label.labelDistance");
    res.add("condLimite.manoeuvre.label.labelFont");
    res.add("condLimite.manoeuvre.label.labelAlignment");
    res.add("condLimite.manoeuvre.label.labelColor");
    res.add("condLimite.qruis.label.displayLabels");
    res.add("condLimite.qruis.label.labelDistance");
    res.add("condLimite.qruis.label.labelFont");
    res.add("condLimite.qruis.label.labelAlignment");
    res.add("condLimite.qruis.label.labelColor");
    res.add("condLimite.manoeuvre-regul.label.displayLabels");
    res.add("condLimite.manoeuvre-regul.label.labelDistance");
    res.add("condLimite.manoeuvre-regul.label.labelFont");
    res.add("condLimite.manoeuvre-regul.label.labelAlignment");
    res.add("condLimite.manoeuvre-regul.label.labelColor");
    res.add("condLimite.usine.label.displayLabels");
    res.add("condLimite.usine.label.labelDistance");
    res.add("condLimite.usine.label.labelFont");
    res.add("condLimite.usine.label.labelAlignment");
    res.add("condLimite.usine.label.labelColor");
    res.add("condLimite.bg1.label.displayLabels");
    res.add("condLimite.bg1.label.labelDistance");
    res.add("condLimite.bg1.label.labelFont");
    res.add("condLimite.bg1.label.labelAlignment");
    res.add("condLimite.bg1.label.labelColor");
    res.add("condLimite.bg1av.label.displayLabels");
    res.add("condLimite.bg1av.label.labelDistance");
    res.add("condLimite.bg1av.label.labelFont");
    res.add("condLimite.bg1av.label.labelAlignment");
    res.add("condLimite.bg1av.label.labelColor");
    res.add("condLimite.bg2.label.displayLabels");
    res.add("condLimite.bg2.label.labelDistance");
    res.add("condLimite.bg2.label.labelFont");
    res.add("condLimite.bg2.label.labelAlignment");
    res.add("condLimite.bg2.label.labelColor");
    res.add("condLimite.bg2av.label.displayLabels");
    res.add("condLimite.bg2av.label.labelDistance");
    res.add("condLimite.bg2av.label.labelFont");
    res.add("condLimite.bg2av.label.labelAlignment");
    res.add("condLimite.bg2av.label.labelColor");
    res.add("groupeDessin.visible");
    res.add("groupHydraulique.visible");
    res.add("groupeImage.visible");
    res.add("groupeAutre.visible");
    res.add("groupeFichierSIG.visible");
    res.add("groupeTraceProfils.visible");
    res.add("groupeProfilsCasier.visible");
    return res;
  }

  @Test
  public void testGetProperties() {
    ConfigSaverHdydaulicAndVisibility saver = new ConfigSaverHdydaulicAndVisibility();
    final LayerVisibility layerVisibility = new LayerVisibility();
    layerVisibility.setCasierVisible(false);
    LinkedHashMap<String, Node.Property> properties = saver.getProperties(new VisuConfiguration(), layerVisibility);
    List<String> keySet = new ArrayList<>(properties.keySet());
    List<String> expected = getExpectedProperties();
//    assertEquals(expected.size(), keySet.size());
    for (int i = 0; i < keySet.size(); i++) {
      assertEquals("i= " + i, expected.get(i), keySet.get(i));
    }
    try {
      assertEquals(Boolean.FALSE, properties.get("casiers.visible").getValue());
      assertEquals(Boolean.TRUE, properties.get("noeuds.visible").getValue());
    } catch (Exception illegalAccessException) {
      fail(illegalAccessException.getMessage());
    }
  }

  @Test
  public void testTransform() {
    ConfigSaverHdydaulicAndVisibility saver = new ConfigSaverHdydaulicAndVisibility();
    final LayerVisibility layerVisibility = new LayerVisibility();
    layerVisibility.setCasierVisible(false);
    LinkedHashMap<String, Node.Property> properties = saver.getProperties(new VisuConfiguration(), layerVisibility);
    LinkedHashMap<String, String> transform = ConfigSaverHelper.transform(properties);
    assertEquals(properties.size(), transform.size());
    assertEquals("SansSerif,1,12", transform.get("casiers.label.labelFont"));
    assertEquals("0,255,255", transform.get("branches.colors.EMHBranchePdc"));
  }
}
