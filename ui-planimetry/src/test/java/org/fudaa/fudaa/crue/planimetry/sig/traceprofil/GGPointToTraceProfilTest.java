/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.ctulu.collection.CtuluCollection;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.fudaa.crue.planimetry.sig.SigHelperForTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

/**
 * @author Frederic Deniger
 */
public class GGPointToTraceProfilTest {
  private static File targetDir;
  private static File shapeFile;

  public GGPointToTraceProfilTest() {
  }

  @BeforeClass
  public static void extractFile() throws IOException {
    targetDir = CtuluLibFile.createTempDir();
    shapeFile = new File(targetDir, "EMG_Profil_point_canal_de_fuite.shp");
    final File dbfFile = new File(targetDir, "EMG_Profil_point_canal_de_fuite.dbf");
    CtuluLibFile.getFileFromJar("/ggcrue/EMG_Profil_point_canal_de_fuite.shp", shapeFile);
    CtuluLibFile.getFileFromJar("/ggcrue/EMG_Profil_point_canal_de_fuite.dbf", dbfFile);
  }

  @AfterClass
  public static void deleteFile() {
    CtuluLibFile.deleteDir(targetDir);
  }

  @Test
  public void testToProfil() {
    //NOM, XT, Z, LIM_LIT, LIM_GEOM, PlanimetryExternDrawLayerGroup.DESSINS_LABEL_LIGNES
    final int nomIdx = 0;
    final int xtIdx = 1;
    final int zIdx = 2;
    final int limLitIdx = 3;
    final int limGeomIdx = 4;
    final int labelIdx = 5;
    final CtuluLogResult<GISZoneCollectionLigneBrisee> toProfil = SigHelperForTest.loadProfilTrace(shapeFile);
    assertNotNull(toProfil);
//    assertTrue("contient des doublons donc warning", toProfil.getLog().containsWarnings());//a cous
    assertFalse(toProfil.getLog().containsErrorOrSevereError());
    assertNotNull(toProfil.getResultat());
    final int nbProfil = 16;
    assertEquals(nbProfil, toProfil.getResultat().getNbGeometries());
    final GISAttributeModel modelNom = toProfil.getResultat().getModel(nomIdx);
    assertEquals(GGTraceProfilSectionAttributesController.NOM, modelNom.getAttribute());
    final Object[] initValues = modelNom.getObjectValues();
    final String[] objectValues = new String[initValues.length];
    for (int i = 0; i < objectValues.length; i++) {
      objectValues[i] = (String) initValues[i];
    }
    assertEquals("1", objectValues[0]);
    assertEquals("218.260", objectValues[nbProfil - 1]);

    final GISAttributeModel modelXt = toProfil.getResultat().getModel(xtIdx);
    assertNotNull(toProfil.getResultat().getModel(zIdx));
    assertNotNull(toProfil.getResultat().getModel(limLitIdx));
    assertNotNull(toProfil.getResultat().getModel(limGeomIdx));
    assertNotNull(toProfil.getResultat().getModel(labelIdx));

    //c'est trié dans l'ordre
    final String nom1 = (String) modelNom.getObjectValueAt(0);
    assertEquals("1", nom1);

    Double xt = getValueAt(modelXt, 0, 0);
    assertEquals(-12.02, xt, 1e-3);
    //on teste la première frontière:
    final int firstLimMinIdx = 7;
    xt = getValueAt(modelXt, 0, firstLimMinIdx);
    assertEquals(39.98, xt, 1e-3);
//    String limLit = getValueAt(modelLimLit, 0, firstLimMinIdx);
//    String limGeom = getValueAt(modelLimGeo, 0, firstLimMinIdx);
//    String label = getValueAt(modelLabel, 0, firstLimMinIdx);
////    assertEquals(LimLitHelper.MAJD_MIN, limLit);
//    assertEquals("", limGeom);
////    assertEquals(LimLitHelper.MAJD_MIN, label);
//
//    int thalwegIdx = 12;
//    xt = getValueAt(modelXt, 0, thalwegIdx);
//    limLit = getValueAt(modelLimLit, 0, thalwegIdx);
//    limGeom = getValueAt(modelLimGeo, 0, thalwegIdx);
//    label = getValueAt(modelLabel, 0, thalwegIdx);
//    assertEquals(10.93, xt, 1e-3);
//    assertEquals("", limLit);
////    assertEquals(LimGeoHelper.THALWEG, limGeom);
////    assertEquals(LimGeoHelper.THALWEG, label);
//
//    int lastLimMinIdx = 40;
//    xt = getValueAt(modelXt, 0, lastLimMinIdx);
//    limLit = getValueAt(modelLimLit, 0, lastLimMinIdx);
//    limGeom = getValueAt(modelLimGeo, 0, lastLimMinIdx);
//    label = getValueAt(modelLabel, 0, lastLimMinIdx);
//    assertEquals(114.03, xt, 1e-3);
//    assertEquals("", limGeom);
////    assertEquals(LimLitHelper.MIN_MAJG, limLit);
////    assertEquals(LimLitHelper.MIN_MAJG, label);
//
//    String nom2 = (String) modelNom.getObjectValueAt(1);
//    assertEquals("98.600", nom2);
//
//    int first = 1;
//    xt = getValueAt(modelXt, 1, first);
//    limLit = getValueAt(modelLimLit, 1, first);
//    limGeom = getValueAt(modelLimGeo, 1, first);
//    label = getValueAt(modelLabel, 1, first);
//    assertEquals(-18, xt, 1e-3);
//    assertEquals("", limGeom);
//    assertEquals(LimLitHelper.RD_STOD, limLit);
//    assertEquals(LimLitHelper.RD_STOD, label);
//
//    first = 7;
//    xt = getValueAt(modelXt, 1, first);
//    limLit = getValueAt(modelLimLit, 1, first);
//    limGeom = getValueAt(modelLimGeo, 1, first);
//    label = getValueAt(modelLabel, 1, first);
//    assertEquals(0.54, xt, 1e-3);
//    final String limLitResult = LimLitHelper.STOD_MAJD + ";" + LimLitHelper.MAJD_MIN;
//    final String litGeoResult = LimGeoHelper.AXE + ";" + LimGeoHelper.THALWEG;
//    assertEquals(litGeoResult, limGeom);
//    assertEquals(limLitResult, limLit);
//    assertEquals(limLitResult + "; " + litGeoResult, label);
  }

  private static <T> T getValueAt(final GISAttributeModel model, final int geoIdx, final int attIdx) {
    return (T) ((CtuluCollection) model.getObjectValueAt(geoIdx)).getObjectValueAt(attIdx);
  }
}
