/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.planimetry;

import gnu.trove.TObjectDoubleHashMap;
import org.fudaa.fudaa.crue.planimetry.layout.NodeYFinder;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class NodeYFinderTest {

  public NodeYFinderTest() {
  }

  @Test
  public void testNodeFinder() {
    NodeYFinder yFinder = new NodeYFinder();
    DirectedWeightedMultigraph<Long, BrancheEdge> graph = new DirectedWeightedMultigraph<>(
            BrancheEdge.class);
    //               |----------------200--------n5
    //n1-----100----n2-------150-----n3
    //               |----------------------300-----------n4
    graph.addVertex(1L);
    graph.addVertex(2L);
    graph.addVertex(3L);
    graph.addVertex(4L);
    graph.addVertex(5L);
    final BrancheEdge b1 = new BrancheEdge(10L);
    final BrancheEdge b2 = new BrancheEdge(20L);
    final BrancheEdge b3 = new BrancheEdge(30L);
    final BrancheEdge b4 = new BrancheEdge(40L);
    graph.addEdge(1L, 2L, b1);
    graph.addEdge(2L, 3L, b2);
    graph.addEdge(2L, 4L, b3);
    graph.addEdge(2L, 5L, b4);
    graph.setEdgeWeight(b1, 100d);
    graph.setEdgeWeight(b2, 150d);
    graph.setEdgeWeight(b3, 300d);
    graph.setEdgeWeight(b4, 200d);
    TObjectDoubleHashMap<Long> findYs = yFinder.findYs(graph);
    assertEquals(5, findYs.size());
    assertEquals(0, findYs.get(4L), 1e-3);
    assertEquals(300, findYs.get(2L), 1e-3);
    assertEquals(150, findYs.get(3L), 1e-3);
    assertEquals(400, findYs.get(1L), 1e-3);
    assertEquals(100, findYs.get(5L), 1e-3);
  }
}
