/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.tracecasier;

import java.io.File;
import java.io.IOException;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.fudaa.crue.planimetry.sig.SigHelperForTest;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import org.junit.BeforeClass;
import org.junit.Test;
import org.locationtech.jts.geom.Geometry;

/**
 *
 * @author Frederic Deniger
 */
public class GGFileToTraceCasierProcessorTest {

  public GGFileToTraceCasierProcessorTest() {
  }
  private static File targetDir;
  private static File dbfFile;
  private static File shapeFile;

  @BeforeClass
  public static void extractFile() throws IOException {
    targetDir = CtuluLibFile.createTempDir();
    dbfFile = new File(targetDir, "CalculCasier.dbf");
    CtuluLibFile.getFileFromJar("/ggcrue/CalculCasier.dbf", dbfFile);

    File shapeDbfFile = new File(targetDir, "EMG_Casier.dbf");
    shapeFile = new File(targetDir, "EMG_Casier.shp");
    CtuluLibFile.getFileFromJar("/ggcrue/EMG_Casier.shp", shapeFile);
    CtuluLibFile.getFileFromJar("/ggcrue/EMG_Casier.dbf", shapeDbfFile);
  }

  @AfterClass
  public static void deleteFile() {
    CtuluLibFile.deleteDir(targetDir);
  }

  @Test
  public void testToCasier() {
    GISZoneCollectionLigneBrisee casiers = SigHelperForTest.loadCasiers(shapeFile);
    assertEquals(31, casiers.getNumGeometries());
    GGFileToTraceCasierProcessor processor = new GGFileToTraceCasierProcessor(casiers, dbfFile, TestCoeurConfig.INSTANCE.
            getCrueConfigMetier());
    CtuluLogResult<GISZoneCollectionLigneBrisee> toCasier = processor.toCasier();
    assertFalse(toCasier.getLog().containsErrorOrSevereError());
    final GISZoneCollectionLigneBrisee resultat = toCasier.getResultat();
    assertNotNull(resultat);
    assertEquals(31, resultat.getNumGeometries());
    GISAttributeModel noms = resultat.getModel(GGTraceCasierAttributesController.NOM);
    GISAttributeModel distances = resultat.getModel(GGTraceCasierAttributesController.DISTANCE);
    GISAttributeModel arrayXts = resultat.getModel(GGTraceCasierAttributesController.ARRAY_XT);
    GISAttributeModel arrayZs = resultat.getModel(GGTraceCasierAttributesController.ARRAY_Z);
    assertEquals("Ca_PC1_3", noms.getObjectValueAt(0));
    assertEquals(0, (Double) distances.getObjectValueAt(0), 1e-5);
    assertEquals("", arrayXts.getObjectValueAt(0));
    assertEquals("", arrayZs.getObjectValueAt(0));
    int pc13Idx = 9;
    assertEquals("Ca_PC13", noms.getObjectValueAt(pc13Idx));
    assertEquals(942.304836813, (Double) distances.getObjectValueAt(pc13Idx), 1e-5);
    assertEquals("9.05393368385;124.329849979;587.190836789;737.931719415;792.709033843;801.100231248", arrayXts.getObjectValueAt(pc13Idx));
    assertEquals("210.220001221;211.220001221;212.220001221;213.220001221;214.220001221;215.220001221", arrayZs.getObjectValueAt(pc13Idx));
  }
}