/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.save;

import java.io.File;
import java.io.IOException;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessourceInfos;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternDrawLayerGroup;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class LayerLigneBriseeModifiableControllerSaverTest {

  private static File targetDir;

  public LayerLigneBriseeModifiableControllerSaverTest() {
  }

  @BeforeClass
  public static void extractFile() throws IOException {
    targetDir = CtuluLibFile.createTempDir();
    File dessinsDir = ConfigSaverExtern.getDessinDir(targetDir);
    dessinsDir.mkdirs();
    CtuluLibFile.getFileFromJar("/dessins/1.shp", new File(dessinsDir, "1.shp"));
    CtuluLibFile.getFileFromJar("/dessins/1.dbf", new File(dessinsDir, "1.dbf"));
    CtuluLibFile.getFileFromJar("/dessins/1.shx", new File(dessinsDir, "1.shx"));
  }

  @AfterClass
  public static void deleteFile() {
    CtuluLibFile.deleteDir(targetDir);
  }

  @Test
  public void testLoad() {
    LayerLigneBriseeModifiableControllerSaver saver = new LayerLigneBriseeModifiableControllerSaver();
    CtuluLog log = new CtuluLog();
    CrueEtudeExternRessourceInfos ressource = new CrueEtudeExternRessourceInfos();
    ressource.setNom("test");
    ressource.setId("1");
    GISZoneCollectionLigneBrisee loadCollection = saver.loadCollection(ressource, log, targetDir);
    assertFalse(log.containsErrorOrSevereError());
    assertEquals(1, loadCollection.getNumGeometries());
    GISAttributeModel model = loadCollection.getModel(PlanimetryExternDrawLayerGroup.getDefaultAttributesLignesModifiable()[0]);
    assertEquals(1, model.getSize());
    GISAttributeModel modelLabels = (GISAttributeModel) model.getObjectValueAt(0);
    assertEquals(5, modelLabels.getSize());
    assertEquals("test", modelLabels.getObjectValueAt(0));
    assertEquals("testttt", modelLabels.getObjectValueAt(1));
    assertNull(modelLabels.getObjectValueAt(2));
    assertNull(modelLabels.getObjectValueAt(3));
    assertNull(modelLabels.getObjectValueAt(4));
  }
}