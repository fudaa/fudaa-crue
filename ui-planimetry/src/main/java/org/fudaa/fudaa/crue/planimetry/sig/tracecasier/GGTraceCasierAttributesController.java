/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.tracecasier;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeString;
import org.fudaa.fudaa.crue.planimetry.sig.GGAttributeDouble;
import org.fudaa.fudaa.crue.planimetry.sig.GGAttributeString;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class GGTraceCasierAttributesController {

  public final static GISAttributeString NOM = new GGAttributeString(CtuluLib.getS("Nom"), "CASIER", false);
  public final static GGAttributeDouble DISTANCE = new GGAttributeDouble(NbBundle.getMessage(GGTraceCasierAttributesController.class,
          "attribute.Distance"), "DISTANCE", false);
  public final static GGAttributeString ARRAY_XT = new GGAttributeString("ARRAY_XT", "ARRAY_XT", false);
  public final static GGAttributeString ARRAY_Z = new GGAttributeString("ARRAY_Z", "ARRAY_Z", false);
  public final static GGAttributeString ARRAY_LIMLIT = new GGAttributeString("LIMLIT", "LIMLIT", false);

  static {
    ARRAY_XT.setUserVisible(false);
    ARRAY_XT.setInternEditor(null);
    ARRAY_Z.setUserVisible(false);
    ARRAY_Z.setInternEditor(null);
    ARRAY_LIMLIT.setUserVisible(false);
    ARRAY_LIMLIT.setInternEditor(null);
    DISTANCE.setInternEditor(new CtuluValueEditorDouble());
    DISTANCE.getEditor().setEditable(false);
  }

  public static boolean isLimLitUtile(String lit) {
    return "LimLitUtile".equals(lit);
  }

  GISAttributeInterface[] createCasierAttributes() {
    //Attention l'ordre doit être identique a createObject
    return new GISAttributeInterface[]{NOM, DISTANCE, ARRAY_XT, ARRAY_Z, ARRAY_LIMLIT};
  }

  Object[] createObject(String nom, TraceCasierProfilContent content) {
    if (content == null) {
      return new Object[]{nom, Double.valueOf(0), StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY};
    }
    return new Object[]{nom, content.getDistance(), content.getXtAsString(), content.getZAsString(), content.getLimLitAsString()};
  }
}
