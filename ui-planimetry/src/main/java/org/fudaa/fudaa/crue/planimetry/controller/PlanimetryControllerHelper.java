package org.fudaa.fudaa.crue.planimetry.controller;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Point;
import java.util.List;
import org.fudaa.ctulu.gis.GISZoneCollectionGeometry;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.EditionDelete.RemoveBilan;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.ContainerActivityVisibility;
import org.fudaa.fudaa.crue.planimetry.layer.LayerController;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryNodeLayerModel;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class PlanimetryControllerHelper {

  private final PlanimetryController controller;
  private final CreationEMHSelectedType creationEMH = new CreationEMHSelectedType();

  public CreationEMHSelectedType getCreationEMH() {
    return creationEMH;
  }

  public PlanimetryControllerHelper(PlanimetryController context) {
    this.controller = context;
  }
  ItemVariable xyVariable;

  public boolean isSame(Coordinate c, Point p) {
    return isSame(c.x, c.y, p.getX(), p.getY());
  }
  public boolean isSame(Coordinate c, Coordinate p) {
    return isSame(c.x, c.y, p.getX(), p.getY());
  }

  public PlanimetryController getController() {
    return controller;
  }

  public boolean canEMHBeAddedAndDisplayError() {
    EMHSousModele sousModele = getDefaultSousModele();
    if (sousModele == null) {
      return false;
    }
    if (!isVisible(sousModele)) {
      DialogHelper.showWarn(NbBundle.getMessage(PlanimetryNodeLayerModel.class, "AddEMH.SousModeleParDefautNotVisible"));
      return false;
    }
    return true;
  }

  /**
   *
   * @return le sousModele par defaut. Attention ne jamais le stocker (clone de scenario).
   */
  public EMHSousModele getDefaultSousModele() {
    ContainerActivityVisibility containerActivityVisibility = controller.getContainerActivityVisibility();
    if (containerActivityVisibility == null) {
      return null;
    }
    Long sousModeleActif = containerActivityVisibility.getSousModeleActifUid();
    if (sousModeleActif == null) {
      return null;
    }
    return (EMHSousModele) getPlanimetryGisModelContainer().getScenario().getIdRegistry().getEmh(sousModeleActif);
  }

  public boolean isVisible(EMHSousModele sousModele) {
    ContainerActivityVisibility containerActivityVisibility = controller.getContainerActivityVisibility();
    if (containerActivityVisibility == null) {
      return true;
    }
    return containerActivityVisibility.isSousModeleVisible(sousModele);
  }

  public void scenarioEditionStarted() {
    controller.getState().setScenarioModified();//scenario modifie
    controller.getPlanimetryGisModelContainer().scenarioEditionStarted();
  }

  public boolean isNotSame(Coordinate c, Point p) {
    return !isSame(c, p);
  }

  public boolean isSame(double x, double y, double x1, double y1) {
    if (xyVariable == null) {
      xyVariable = controller.getCrueConfigMetier().getProperty("xp");
    }
    return xyVariable.getEpsilon().isSame(x, x1) && xyVariable.getEpsilon().isSame(y, y1);
  }

  public <T extends EMH> T getEMH(Long uid) {
    return getPlanimetryGisModelContainer().getEMH(uid);
  }

  public Long getUid(GISZoneCollectionGeometry zone, int idx) {
    return controller.getPlanimetryGisModelContainer().getUid(zone, idx);
  }

  /**
   * ne doit etre appele que pour les sections ou trace de section
   *
   * @param zone zone
   * @param idx du de la section
   * @return uid de la section
   */
  public Long getBrancheUidForSection(GISZoneCollectionGeometry zone, int idx) {
    return controller.getPlanimetryGisModelContainer().getBrancheUid(zone, idx);
  }

  /**
   * ne doit etre appele que pour les sections ou trace de section
   *
   * @param zone zone
   * @param idx de la relation
   * @return la relation
   */
  public RelationEMHSectionDansBranche getRelationSectionDansBranche(GISZoneCollectionGeometry zone, int idx) {
    return controller.getPlanimetryGisModelContainer().getRelationSectionDansBranche(zone, idx);
  }

  public PlanimetryGisModelContainer getPlanimetryGisModelContainer() {
    return controller.getPlanimetryGisModelContainer();
  }

  public LayerNodeController getNodeController() {
    return controller.getNodeController();
  }

  public LayerBrancheController getBrancheController() {
    return controller.getBrancheController();
  }

  public LayerCasierController getCasierController() {
    return controller.getCasierController();
  }

  public LayerCLimMController getcondLimiteController() {
    return controller.getcondLimiteController();
  }

  public LayerSectionController getSectionController() {
    return controller.getSectionController();
  }

  public LayerTraceController getTraceController() {
    return controller.getTraceController();
  }

  public void changeDone(LayerController layerController) {
    controller.changeDone(layerController);
  }

  public void changeDoneInExternLayer(LayerController layerController) {
    controller.changeDoneInExternData(layerController);
  }

  boolean isEditable() {
    return controller.isEditable();
  }

  /**
   *
   * @param branche
   * @return true si le sous-modele parent est visible
   */
  public boolean isVisible(CatEMHBranche branche) {
    return branche != null && isVisible(branche.getParent());
  }

  /**
   *
   * @param casier
   * @return true si le sous-modele parent est visible
   */
  public boolean isVisible(CatEMHCasier casier) {
    return casier != null && isVisible(casier.getParent());
  }

  /**
   *
   * @param section
   * @return true si le sous-modele parent est visible
   */
  public boolean isVisible(CatEMHSection section) {
    return section != null && isVisible(section.getParent());
  }

  /**
   *
   * @param noeud
   * @return true si un sous-modele parent est visible
   */
  public boolean isVisible(CatEMHNoeud noeud) {
    if (noeud == null) {
      return false;
    }
    List<EMHSousModele> parents = noeud.getParents();
    for (EMHSousModele parent : parents) {
      if (isVisible(parent)) {
        return true;
      }
    }
    return false;
  }

  public void displayBilan(RemoveBilan deleted) {
    LogsDisplayer.displayBilan(deleted);
  }

  public CrueConfigMetier getCcm() {
    return getController().getCrueConfigMetier();
  }
}
