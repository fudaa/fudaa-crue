/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.save;

import java.io.File;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.planimetry.LayerVisibility;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;
import org.fudaa.fudaa.crue.planimetry.layout.NetworkBuilder;
import org.fudaa.fudaa.crue.planimetry.layout.NetworkGisPositionnerResult;

/**
 *
 * @author Frederic Deniger
 */
public class VisuLoadingResult {

  public static VisuLoadingResult loadVisuConfig(final EMHProjet selectedProjet, final EMHScenario scenario) {
    CrueConfigMetier crueConfigMetier = selectedProjet.getPropDefinition();
    ConfigLoaderHydraulic.Result loadConfigEtude = ConfigLoaderHydraulic.loadConfigEtude(selectedProjet);
    VisuConfiguration visuConfiguration = null;
    LayerVisibility visibility = null;
    if (loadConfigEtude != null) {
      visuConfiguration = loadConfigEtude.visuConfiguration;
      visibility = loadConfigEtude.layerVisibility;
    }
    if (visuConfiguration == null) {
      visuConfiguration = new VisuConfiguration();
    }
    if (visibility == null) {
      visibility = new LayerVisibility();
    }
    PlanimetryGisLoader loader = new PlanimetryGisLoader();
    NetworkBuilder builder = new NetworkBuilder();
    final File dirOfConfig = selectedProjet.getInfos().getDirOfConfig();
    NetworkGisPositionnerResult load = loader.load(dirOfConfig, scenario);
    VisuLoadingResult result = new VisuLoadingResult();
    if (load == null) {
      load = builder.buildNewGisData(scenario, visuConfiguration, crueConfigMetier, 0, 0);
    }
    if (load == null) {
      load = new NetworkGisPositionnerResult(scenario,false);
    }
    load.setLayerVisibility(visibility);
    load.setVisuConfiguration(visuConfiguration);
    load.setCcm(crueConfigMetier);
    result.networkGisPositionnerResult = load;
    ConfigLoaderExtern.ResultExtern loadConfigEtudeExtern = ConfigLoaderExtern.loadConfigEtudeExtern(selectedProjet);
    if (loadConfigEtudeExtern != null) {
      result.additionnalLayer = loadConfigEtudeExtern.additionnalLayers;
    }
    result.log = load.getError();
    return result;
  }
  public NetworkGisPositionnerResult networkGisPositionnerResult;
  public List<PlanimetryAdditionalLayerContrat> additionnalLayer;
  public CtuluLog log;
}
