/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.find;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Frederic Deniger
 */
public class FindQuery {

  private final Set types = new HashSet();
  private String textToFind;

  String getTextToFind() {
    return textToFind;
  }

  Set getTypes() {
    return types;
  }

  void setTextToFind(final String textToFind) {
    this.textToFind = textToFind;
  }


  void addTypes(final Object[] types) {
    if (types != null) {
      this.types.addAll(Arrays.asList(types));
    }
  }
}
