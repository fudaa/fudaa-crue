package org.fudaa.fudaa.crue.planimetry.configuration;

import java.awt.Color;

import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryPointLayerModel;

/**
 * @author deniger
 */
public class PointConfiguration extends DefaultConfiguration {

  TraceIconModel iconModel = new TraceIconModel(TraceIcon.CERCLE, 6, Color.BLACK);
  LabelConfiguration labelConfiguration = new LabelConfiguration();

  public PointConfiguration() {
    iconModel.setBackgroundColor(Color.WHITE);
  }

  public PointConfiguration(PointConfiguration from) {
    super(from);
    if (from != null) {
      iconModel = new TraceIconModel(from.iconModel);
      labelConfiguration = labelConfiguration.copy();
    }
  }

  @Override
  public PointConfiguration copy() {
    return new PointConfiguration(this);
  }


  public Color getIconColor() {
    return iconModel.getCouleur();
  }

  public void setIconColor(Color c) {
    iconModel.setCouleur(c);
  }

  public LabelConfiguration getLabelConfiguration() {
    return labelConfiguration;
  }

  public void initTraceIcon(TraceIconModel _ligne, int _idxPoint, PlanimetryPointLayerModel model) {
    _ligne.updateData(iconModel);
  }

  public int getDiameter() {
    return iconModel.getTaille();
  }

  public double getRayon() {
    return iconModel.getTaille() / 2d;
  }
}
