package org.fudaa.fudaa.crue.planimetry.configuration.node;

import java.util.List;
import org.fudaa.fudaa.crue.planimetry.configuration.NodeConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.NodeConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerBuilder;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class NodeConfigurationNode extends AbstractNode {
  
  public NodeConfigurationNode(NodeConfiguration nodeConfiguration) {
    super(Children.LEAF, Lookups.singleton(nodeConfiguration));
    setDisplayName(NbBundle.getMessage(PlanimetryControllerBuilder.class, PlanimetryController.NAME_CQ_NODE));
  }
  
  @Override
  protected Sheet createSheet() {
    Sheet sheet = new Sheet();
    NodeConfiguration lookup = getLookup().lookup(NodeConfiguration.class);
    List<Set> createSet = NodeConfigurationInfo.createSet(lookup);
    for (Set set : createSet) {
      sheet.put(set);
    }
    return sheet;
  }
}
