package org.fudaa.fudaa.crue.planimetry.controller;

import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.EnumNoeudType;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.ebli.calque.edition.ZModelePointEditable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.crue.planimetry.GeometryIndexer;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.action.OpenEMHAction;
import org.fudaa.fudaa.crue.planimetry.configuration.NodeConfigurationExtra;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.layer.LayerControllerEMH;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryNodeLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryNodeLayerModel;
import org.fudaa.fudaa.crue.planimetry.listener.NodeUpdaterFromBranche;
import org.fudaa.fudaa.crue.planimetry.listener.NodeUpdaterFromCasier;
import org.openide.util.NbBundle;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author deniger
 */
public class LayerNodeController extends LayerModelControllerDefault<PlanimetryNodeLayer> implements LayerControllerEMH {

  GeometryIndexer nodeIndexer;
  NodeConfigurationExtra nodeConfigurationExtra;

  public NodeConfigurationExtra getNodeConfigurationExtra() {
    return nodeConfigurationExtra;
  }

  public void setNodeConfigurationExtra(NodeConfigurationExtra nodeConfigurationExtra) {
    this.nodeConfigurationExtra = nodeConfigurationExtra;
  }

  protected static LayerNodeController install(PlanimetryController ctx, PlanimetryVisuPanel res,
          VisuConfiguration configuration) {
    LayerNodeController nodeController = new LayerNodeController();
    nodeController.layer = new PlanimetryNodeLayer(new PlanimetryNodeLayerModel(ctx.gisModel.createNodeCollection(),
            nodeController),
            res.getEditor());
    nodeController.layer.setLayerConfiguration(configuration.getNodeConfiguration());
    nodeController.layer.setName(PlanimetryController.NAME_CQ_NODE);
    nodeController.layer.setTitle(NbBundle.getMessage(PlanimetryControllerBuilder.class, PlanimetryController.NAME_CQ_NODE));
    nodeController.layer.setActions(new EbliActionInterface[]{
      new OpenEMHAction.Reopen(ctx),
      new OpenEMHAction.NewFrame(ctx),
      null,
      res.getSigEditAction(),
      null,
      res.getEditor().getExportWithNameAction(),
      null,
      res.getEditor().getActionDelete(),
      res.getPlanimetryVisuController().getDeleteCascade(),
      null,
      res.createFindAction(nodeController.layer)});
    ctx.controllersByName.put(PlanimetryController.NAME_CQ_NODE, nodeController);
    return nodeController;
  }

  @Override
  public void changeWillBeDone() {
    //modif defa effectuée
    if (isModified()) {
      return;
    }
    super.changeWillBeDone();
    //on sauvegarde égaleemnt les autres données hydrau
    getHelper().getBrancheController().changeWillBeDone();
    getHelper().getCasierController().changeWillBeDone();
  }
  NodeUpdaterFromBranche nodeUpdaterFromBranche;
  NodeUpdaterFromCasier nodeUpdaterFromCasier;

  @Override
  public void init(PlanimetryControllerHelper helper) {
    super.init(helper);
    nodeIndexer = new GeometryIndexer(layer.modeleDonnees(), helper.getPlanimetryGisModelContainer());
    listenerDispatcher.addListener(nodeIndexer);

    //pour que les noeuds suivent la branche
    nodeUpdaterFromBranche = new NodeUpdaterFromBranche(this);
    //on écoute les changement dans les branches.
    helper.getBrancheController().addGISListener(nodeUpdaterFromBranche);

    nodeUpdaterFromCasier = new NodeUpdaterFromCasier(this);
    helper.getCasierController().addGISListener(nodeUpdaterFromCasier);
  }

  @Override
  public List<ToStringInternationalizable> getCategories() {
    return Arrays.asList(EnumNoeudType.values());
  }

  @Override
  public void setVisuConfiguration(VisuConfiguration cloned) {
    getLayer().setLayerConfiguration(cloned.getNodeConfiguration());
  }

  @Override
  protected void geometryDataUpdated() {
    if (nodeIndexer != null) {
      nodeIndexer.reindex();
    }
  }

  public GISZoneCollectionPoint getNodeCollection() {
    return ((ZModelePointEditable) layer.modele()).getGeomData();
  }

  public GISPoint getNoeudGis(int idx) {
    return getNodeCollection().get(idx);
  }

  public GeometryIndexer getNodeIndexer() {
    return nodeIndexer;
  }

  public int getNodePosition(Long uid) {
    return nodeIndexer.getPosition(uid);
  }

  public CatEMHNoeud getNoeud(int position) {
    Long uid = helper.getUid(getNodeCollection(), position);
    return getEMH(uid);
  }

  @Override
  public EMH getEMHFromLayerPosition(int position) {
    return getNoeud(position);
  }

  @Override
  public PlanimetryControllerHelper getHelper() {
    return helper;
  }

  public void globalChangedFinished() {
    nodeUpdaterFromBranche.updateIfNeeded();
    nodeUpdaterFromCasier.updateIfNeeded();
  }

  public boolean setSelectedEMHs(List<EMH> selectedEMH) {
    List<EMH> selectEMHS = EMHHelper.selectEMHS(selectedEMH, EnumCatEMH.NOEUD);
    TIntArrayList positions = new TIntArrayList();
    for (EMH emh : selectEMHS) {
      int nodePosition = getNodePosition(emh.getUiId());
      if (nodePosition >= 0) {
        positions.add(nodePosition);
      }
    }
    layer.setSelection(positions.toNativeArray());
    return !positions.isEmpty();
  }

  public boolean setSelectedEMHUids(Collection<Long> selectedEMHUids) {
    TIntArrayList positions = new TIntArrayList();
    for (Long uid : selectedEMHUids) {
      int nodePosition = getNodePosition(uid);
      if (nodePosition >= 0) {
        positions.add(nodePosition);
      }
    }
    layer.setSelection(positions.toNativeArray());
    return !positions.isEmpty();
  }

  public void getSelectedEMHs(Collection<Long> uids) {
    if (!layer.isVisible() || layer.isSelectionEmpty()) {
      return;
    }
    int[] selectedIndex = layer.getSelectedIndex();
    if (selectedIndex != null) {
      for (int i = 0; i < selectedIndex.length; i++) {
        uids.add(helper.getUid(getNodeCollection(), selectedIndex[i]));
      }
    }
  }
}
