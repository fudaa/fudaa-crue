/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry;

import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuResource;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.gis.exporter.GISExportDataStoreFactory;
import org.fudaa.ctulu.gis.exporter.GISFileFormat;
import org.fudaa.dodico.rubar.io.RubarStCnFileFormat;
import org.fudaa.dodico.telemac.io.SinusxFileFormat;
import org.fudaa.dodico.zesri.io.ZESRiFileFormat;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.ZModeleGeometry;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.fudaa.fudaa.sig.layer.FSigLayerExporterI;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Frederic Deniger
 */
public class PlanimetryGisEditor extends FSigEditor {
    private EbliActionSimple exportWithNameAction;

    public PlanimetryGisEditor(ZEbliCalquesPanel ebliCalquesPanel) {
        super(ebliCalquesPanel, new PlanimetrySceneEditor(ebliCalquesPanel.getScene(), ebliCalquesPanel));
        setExportWithId(true);
    }

    @Override
    protected void editSingleObject(ZCalqueEditable _target) {
        super.editSingleObject(_target);
    }

    @Override
    protected void configureEditorPanel(JComponent jc) {
        super.configureEditorPanel(jc);
        SysdocUrlBuilder.installHelpShortcut(jc, SysdocUrlBuilder.getDialogHelpCtxId("bdlEditerDonneesGeolocalisation", null));
    }

    /**
     * @return une action qui utilise le nom de l'attribut dans l'export. Permet d'etre compatible avec les anciennes versions de GeoGama
     */
    public EbliActionSimple getExportWithNameAction() {
        if (exportWithNameAction == null) {
            exportWithNameAction = new ExportWithNameAction();
        }
        return exportWithNameAction;
    }


    public Map<BuFileFilter, GISFileFormat> generateSupportedFileFilter() {
        final Map filterExporter = new HashMap(5);// pour l'instant .....
        final Map<BuFileFilter, GISFileFormat> dataStores = GISExportDataStoreFactory.buildFileFilterMapExcludeGML();
        if (dataStores.size() == 0) {
            if (FuLog.isTrace()) {
                FuLog.trace("no gis format to export");
            }
            return null;
        }
        for (final Map.Entry<BuFileFilter, GISFileFormat> e : dataStores.entrySet()) {
            filterExporter.put(e.getKey(), new FSigLayerExporterI.DataStore(e.getValue()));
        }
        return filterExporter;
    }

    private class ExportWithNameAction extends EbliActionSimple {
        protected ExportWithNameAction() {
            super(org.openide.util.NbBundle.getMessage(PlanimetryGisEditor.class, "exportLayerWithName.actionName"), BuResource.BU.getIcon("exporter"),
                    "GIS_NAME_EXPORT");
            setDefaultToolTip(org.openide.util.NbBundle.getMessage(PlanimetryGisEditor.class, "exportLayerWithName.tooltip"));
        }

        @Override
        public void actionPerformed(final ActionEvent _e) {
            exportSelectedLayer(false);
        }

        @Override
        public void updateStateBeforeShow() {
            boolean canExport = canExportSelectedLayer();
            //action activée que si des données sont présentes
            if (canExport) {
                canExport = false;
                final BCalque[] parent = getPanel().getArbreCalqueModel().getSelection();
                if (parent != null) {
                    for (BCalque bCalque : parent) {
                        if (bCalque instanceof ZCalqueAffichageDonneesInterface
                                && ((ZCalqueAffichageDonneesInterface) bCalque).modeleDonnees() instanceof ZModeleGeometry) {
                            ZModeleGeometry modeleGeometry = (ZModeleGeometry) ((ZCalqueAffichageDonneesInterface) bCalque).modeleDonnees();
                            if (modeleGeometry.getNombre() > 0) {
                                canExport = true;
                            }
                        }
                    }
                }
            }
            super.setEnabled(canExport);
        }

        @Override
        public String getEnableCondition() {
            return FudaaLib.getS("Sélectionner un calque non vide");
        }
    }
}
