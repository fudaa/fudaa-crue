package org.fudaa.fudaa.crue.planimetry.action;

import org.apache.commons.collections4.EnumerationUtils;
import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.action.TreeDeleteCalqueAction;

import javax.swing.tree.TreePath;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * @author deniger
 */
public class DeleteCalqueAction extends TreeDeleteCalqueAction {
  private final BArbreCalque arbreCalque;

  public DeleteCalqueAction(BArbreCalqueModel treeModel, BArbreCalque arbreCalque) {
    super(treeModel);
    this.arbreCalque = arbreCalque;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    final BCalque[] c = treeModel_.getSelection();
    if (c.length == 0) {
      return;
    }

    TreePath rootPath = arbreCalque.getPathForRow(0).getParentPath();
    final List expandedDescendants =new ArrayList(EnumerationUtils.toList(arbreCalque.getExpandedDescendants(rootPath)));
    super.actionPerformed(_e);
    for (Object treePath : expandedDescendants) {
      arbreCalque.expandPath((TreePath) treePath);
    }
  }
}
