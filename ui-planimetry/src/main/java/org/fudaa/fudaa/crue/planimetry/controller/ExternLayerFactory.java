/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.controller;

import java.io.File;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper.EnumFormat;
import org.fudaa.fudaa.crue.planimetry.configuration.LigneBriseeConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.PointConfiguration;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternTraceCasierLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternTraceProfilLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLigneBriseeExternModifiableLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLigneBriseeExternUnmodifiableLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLigneBriseeLayerModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryPointExternModifiableLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryPointExternUnmodifiableLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryPointLayerModel;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.joda.time.LocalDateTime;

/**
 *
 * @author Frederic Deniger
 */
public class ExternLayerFactory {

  public static PlanimetryPointExternModifiableLayer createLayerPointsModifiable(GISZoneCollectionPoint pt, FSigEditor editor) {
    final LayerPointExternController layerPointExternController = new LayerPointExternController(true);
    PlanimetryPointLayerModel model = new PlanimetryPointLayerModel(pt, layerPointExternController);
    PlanimetryPointExternModifiableLayer newLayer = new PlanimetryPointExternModifiableLayer(model, editor);
    layerPointExternController.setLayer(newLayer);
    return newLayer;
  }

  public static PlanimetryLigneBriseeExternModifiableLayer createLayerLignesModifiable(GISZoneCollectionLigneBrisee lignes, final FSigEditor editor) {
    final LayerLigneBriseeExternController layerLigneBriseeExternController = new LayerLigneBriseeExternController(true);
    PlanimetryLigneBriseeLayerModel model = new PlanimetryLigneBriseeLayerModel(lignes, layerLigneBriseeExternController);
    PlanimetryLigneBriseeExternModifiableLayer newLayer = new PlanimetryLigneBriseeExternModifiableLayer(model, editor);
    layerLigneBriseeExternController.setLayer(newLayer);
    return newLayer;
  }

  public static PlanimetryLigneBriseeExternUnmodifiableLayer createLayerLignesUnmodifiable(GISZoneCollectionLigneBrisee lignes, File file,
          EnumFormat fmt, final FSigEditor editor) {
    lignes.setGeomModifiable(false);
    final LayerLigneBriseeExternController layerLigneBriseeExternController = new LayerLigneBriseeExternController(false);
    PlanimetryLigneBriseeLayerModel model = new PlanimetryLigneBriseeLayerModel(lignes, layerLigneBriseeExternController);
    PlanimetryLigneBriseeExternUnmodifiableLayer newLayer = new PlanimetryLigneBriseeExternUnmodifiableLayer(model, editor, file, fmt);
    newLayer.setTitle(file.getName());
    layerLigneBriseeExternController.setLayer(newLayer);
    return newLayer;
  }

  /**
   * Attention, le nom du calque sert d'identifiant. Toutes les méthodes appelantes doivent l'initialiser par la suite.
   *
   * @param lignes la collection de lignes
   * @param file le fichier
   * @param fmt le format
   * @param editor l'editeur de calque SIG
   * @return le calque
   */
  public static PlanimetryExternTraceProfilLayer createLayerTraceProfil(GISZoneCollectionLigneBrisee lignes, File file, EnumFormat fmt,
          final FSigEditor editor) {
    lignes.setGeomModifiable(false);
    final LayerLigneBriseeExternController layerLigneBriseeExternController = new LayerLigneBriseeExternController(false);
    PlanimetryLigneBriseeLayerModel model = new PlanimetryLigneBriseeLayerModel(lignes, layerLigneBriseeExternController);
    PlanimetryExternTraceProfilLayer newLayer = new PlanimetryExternTraceProfilLayer(model, editor, null);
    newLayer.setTitle(file.getName() + BusinessMessages.ENTITY_SEPARATOR + DateDurationConverter.dateToXsdUI(new LocalDateTime(file.lastModified())));
    layerLigneBriseeExternController.setLayer(newLayer);

    return newLayer;
  }

  public static PlanimetryExternTraceCasierLayer createLayerTraceCasier(GISZoneCollectionLigneBrisee lignes, File file, EnumFormat fmt,
          final FSigEditor editor) {
    lignes.setGeomModifiable(false);
    final LayerLigneBriseeExternController layerLigneBriseeExternController = new LayerLigneBriseeExternController(false);
    PlanimetryLigneBriseeLayerModel model = new PlanimetryLigneBriseeLayerModel(lignes, layerLigneBriseeExternController);
    PlanimetryExternTraceCasierLayer newLayer = new PlanimetryExternTraceCasierLayer(model, editor, null);
    newLayer.setTitle(file.getName() + BusinessMessages.ENTITY_SEPARATOR + DateDurationConverter.dateToXsdUI(new LocalDateTime(file.lastModified())));
    layerLigneBriseeExternController.setLayer(newLayer);
    return newLayer;
  }

  public static PlanimetryPointExternUnmodifiableLayer createLayerPointUnmodifiable(GISZoneCollectionPoint pt, File file, EnumFormat fmt,
          FSigEditor editor) {
    pt.setGeomModifiable(false);
    final LayerPointExternController layerPointExternController = new LayerPointExternController(false);
    PlanimetryPointLayerModel model = new PlanimetryPointLayerModel(pt, layerPointExternController);
    PlanimetryPointExternUnmodifiableLayer newLayer = new PlanimetryPointExternUnmodifiableLayer(model, editor, file, fmt);
    newLayer.setTitle(file.getName());
    layerPointExternController.setLayer(newLayer);
    return newLayer;
  }

  public static PlanimetryAdditionalLayerContrat cloneModifiableLayer(PlanimetryLigneBriseeExternModifiableLayer toClone) {
    LayerLigneBriseeExternController res = new LayerLigneBriseeExternController(true);
    PlanimetryLigneBriseeExternModifiableLayer layer = new PlanimetryLigneBriseeExternModifiableLayer(toClone.modeleDonnees().cloneModel(res), null);
    res.setLayer(layer);
    layer.initFrom(toClone.saveUIProperties());
    layer.setTitle(toClone.getTitle());
    layer.setName(toClone.getName());
    layer.setLayerConfigurationNoUndo((LigneBriseeConfiguration) toClone.getLayerConfiguration().copy());
    return layer;
  }

  public static PlanimetryAdditionalLayerContrat cloneLayerUnmodifiable(PlanimetryLigneBriseeExternUnmodifiableLayer toClone) {
    LayerLigneBriseeExternController res = new LayerLigneBriseeExternController(false);
    PlanimetryLigneBriseeExternUnmodifiableLayer layer = new PlanimetryLigneBriseeExternUnmodifiableLayer(toClone.modeleDonnees().cloneModel(res),
            null, toClone.getFile(), toClone.getFmt());
    res.setLayer(layer);
    layer.initFrom(toClone.saveUIProperties());
    layer.setTitle(toClone.getTitle());
    layer.setName(toClone.getName());
    layer.setLayerConfigurationNoUndo((LigneBriseeConfiguration) toClone.getLayerConfiguration().copy());
    return layer;
  }

  public static PlanimetryAdditionalLayerContrat cloneTraceProfil(PlanimetryExternTraceProfilLayer toClone) {
    LayerLigneBriseeExternController res = new LayerLigneBriseeExternController(false);
    PlanimetryExternTraceProfilLayer layer = new PlanimetryExternTraceProfilLayer(toClone.modeleDonnees().cloneModel(res), (FSigEditor) toClone.getEditor(),
            toClone.getTraceProfilId());
    res.setLayer(layer);
    layer.initFrom(toClone.saveUIProperties());
    layer.setTitle(toClone.getTitle());
    layer.setName(toClone.getName());
    layer.setLayerConfigurationNoUndo((LigneBriseeConfiguration) toClone.getLayerConfiguration().copy());

    return layer;
  }

  public static PlanimetryAdditionalLayerContrat cloneTraceCasier(PlanimetryExternTraceCasierLayer toClone) {
    LayerLigneBriseeExternController res = new LayerLigneBriseeExternController(false);
    PlanimetryExternTraceCasierLayer layer = new PlanimetryExternTraceCasierLayer(toClone.modeleDonnees().cloneModel(res), (FSigEditor) toClone.getEditor(),
            toClone.getTraceProfilId());
    res.setLayer(layer);
    layer.initFrom(toClone.saveUIProperties());
    layer.setTitle(toClone.getTitle());
    layer.setName(toClone.getName());
    layer.setLayerConfigurationNoUndo((LigneBriseeConfiguration) toClone.getLayerConfiguration().copy());

    return layer;
  }

  public static PlanimetryAdditionalLayerContrat cloneLayerPointModifiable(PlanimetryPointExternModifiableLayer toClone) {
    LayerPointExternController res = new LayerPointExternController(true);
    PlanimetryPointExternModifiableLayer layer = new PlanimetryPointExternModifiableLayer(toClone.modeleDonnees().cloneModel(res), null);
    res.setLayer(layer);
    layer.initFrom(toClone.saveUIProperties());
    layer.setTitle(toClone.getTitle());
    layer.setName(toClone.getName());
    layer.setLayerConfigurationNoUndo((PointConfiguration) toClone.getLayerConfiguration().copy());
    return layer;
  }

  public static PlanimetryAdditionalLayerContrat cloneLayerPointUnmodifiable(PlanimetryPointExternUnmodifiableLayer toClone) {
    LayerPointExternController res = new LayerPointExternController(false);
    PlanimetryPointExternUnmodifiableLayer layer = new PlanimetryPointExternUnmodifiableLayer(toClone.modeleDonnees().cloneModel(res), null, toClone.
            getFile(), toClone.getFmt());
    res.setLayer(layer);
    layer.initFrom(toClone.saveUIProperties());
    layer.setTitle(toClone.getTitle());
    layer.setName(toClone.getName());
    layer.setLayerConfigurationNoUndo((PointConfiguration) toClone.getLayerConfiguration().copy());
    return layer;
  }
}
