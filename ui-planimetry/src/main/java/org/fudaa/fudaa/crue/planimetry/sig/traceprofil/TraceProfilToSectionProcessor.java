/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import gnu.trove.TObjectIntHashMap;
import java.awt.EventQueue;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.EditionChangeSection;
import org.fudaa.dodico.crue.edition.EditionCreateEMH;
import org.fudaa.dodico.crue.edition.EditionRename;
import org.fudaa.dodico.crue.edition.bean.SectionEditionContent;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSectionProfil;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternTraceProfilLayer;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class TraceProfilToSectionProcessor implements ProgressRunnable<CtuluLogGroup> {

  private final PlanimetryControllerHelper helper;
  private final List<TraceProfilSousModele> todos;
  private final PlanimetryExternTraceProfilLayer layer;
  private final TObjectIntHashMap<String> traceProfilPositionByName;
  private final TraceProfilAnglesProcessor traceProfilAnglesProcessor;

  public TraceProfilToSectionProcessor(PlanimetryControllerHelper helper, List<TraceProfilSousModele> todos,
          PlanimetryExternTraceProfilLayer layer, TObjectIntHashMap<String> traceProfilPositionByName) {
    this.helper = helper;
    this.todos = todos;
    this.layer = layer;
    this.traceProfilPositionByName = traceProfilPositionByName;
    traceProfilAnglesProcessor = new TraceProfilAnglesProcessor(helper);
  }

  @Override
  public CtuluLogGroup run(ProgressHandle handle) {
    handle.switchToDeterminate(todos.size());
    helper.scenarioEditionStarted();
    helper.getTraceController().changeWillBeDone();
    final EMHScenario scenario = helper.getPlanimetryGisModelContainer().getScenario();
    List<EMHSousModele> sousModeles = scenario.getSousModeles();
    Map<String, EMH> emhByNom = scenario.getIdRegistry().getEmhByNom();
    Map<String, EMHSousModele> sousModeleByNom = TransformerHelper.toMapOfNom(sousModeles);
    CtuluLogGroup res = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    for (TraceProfilSousModele traceProfilSousModele : todos) {
      handle.progress(traceProfilSousModele.getSectionName(), 1);
      process(traceProfilSousModele, sousModeleByNom, emhByNom, res);
    }
    propagateModification();

    return res;
  }

  public void propagateModification() {
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        helper.getTraceController().reset();
        helper.getTraceController().changeDone();
        helper.getTraceController().rebuildGis();
        helper.getTraceController().getLayer().repaint(0);
      }
    });
  }

  private void process(TraceProfilSousModele traceProfilSousModele, Map<String, EMHSousModele> sousModeleByNom, Map<String, EMH> emhByNom,
          CtuluLogGroup res) {
    EMHSousModele targetSousModele = sousModeleByNom.get(traceProfilSousModele.getSousModeleName());
    if (targetSousModele == null || !traceProfilPositionByName.containsKey(traceProfilSousModele.getTraceProfilName())) {
      return;
    }
    int traceIdx = traceProfilPositionByName.get(traceProfilSousModele.getTraceProfilName());
    final CrueConfigMetier ccm = helper.getController().getCrueConfigMetier();
    TraceProfilToDonPrtGeoProfilSectionTransformer toSection = new TraceProfilToDonPrtGeoProfilSectionTransformer(ccm, targetSousModele);
    CtuluLogResult<DonPrtGeoProfilSection> donPrtGeoProfilSection = toSection.getDonPrtGeoProfilSection(layer.getModelEditable().getGeomData(),
            traceIdx);
    final DonPrtGeoProfilSection newDPTG = donPrtGeoProfilSection.getResultat();
    if (newDPTG == null || donPrtGeoProfilSection.getLog().containsErrorOrSevereError()) {
      CtuluLog log = donPrtGeoProfilSection.getLog();
      log.setDesc(getLogDesc(traceProfilSousModele));
      res.addLog(log);
    } else {
      //on ajoute ou modifie la section
      if (traceProfilSousModele.isSectionExists()) {
        CatEMHSection oldSection = (CatEMHSection) emhByNom.get(traceProfilSousModele.getSectionName());
        if (!oldSection.getSectionType().equals(EnumSectionType.EMHSectionProfil)) {
          EditionChangeSection change = new EditionChangeSection(helper.getTraceController().getDefaultValues());
          SectionEditionContent newContent = new SectionEditionContent();
          newContent.setNewType(EnumSectionType.EMHSectionProfil);
          newContent.setNewName(oldSection.getNom());
          newContent.setComment(oldSection.getCommentaire());
          CtuluLog changeSection = change.changeSection(oldSection, newContent, ccm);
          if (changeSection.containsErrorOrSevereError()) {
            changeSection.setDesc(getLogDesc(traceProfilSousModele));
            res.addLog(changeSection);
            return;
          }
          oldSection = (CatEMHSection) targetSousModele.getParent().getParent().getIdRegistry().getEmhByNom().get(oldSection.getNom());
        }
        DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection(oldSection);
        profilSection.initDataFrom(newDPTG);
        traceProfilAnglesProcessor.propagateAngleChange(traceIdx, oldSection, layer.getModelEditable().getGeomData());
      } else {
        EditionCreateEMH creator = new EditionCreateEMH(helper.getTraceController().getDefaultValues());
        String targetName = CruePrefix.getNomAvecPrefixFor(traceProfilSousModele.getSectionName(), EnumCatEMH.SECTION);
        EMHSectionProfil section = new EMHSectionProfil(targetName);
        section.addInfosEMH(newDPTG);
        EditionRename.propagateSectionRename(section, targetName);
        creator.addSection(section, targetSousModele, null, null, ccm);
      }
      CtuluLog success = new CtuluLog();
      success.setDesc(NbBundle.getMessage(TraceProfilToSectionProcessor.class, "traceProfil.sectionCreated", traceProfilSousModele.getSectionName(),
              traceProfilSousModele.getTraceProfilName()));
      res.addLog(success);
    }
  }

  public String getLogDesc(TraceProfilSousModele traceProfilSousModele) throws MissingResourceException {
    return NbBundle.getMessage(TraceProfilToSectionProcessor.class, "traceProfil.noModificationDone", traceProfilSousModele.getSectionName());
  }
}
