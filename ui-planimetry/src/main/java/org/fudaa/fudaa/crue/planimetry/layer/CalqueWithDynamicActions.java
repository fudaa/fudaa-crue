/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.layer;

import com.memoire.bu.BuPopupMenu;

/**
 *
 * @author Frederic Deniger
 */
public interface CalqueWithDynamicActions {

  void fillPopup(BuPopupMenu popup);
}
