/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.save;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessourceInfos;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper;
import org.fudaa.fudaa.sig.FSigGeomSrcData;

/**
 *
 * @author Frederic Deniger
 */
public class LayerControllerSaverModifiableHelper {

  protected FSigGeomSrcData loadCollection(CrueEtudeExternRessourceInfos ressource, CtuluLog log, File baseDir) {
    SigFormatHelper.EnumFormat fmt = SigFormatHelper.EnumFormat.GML;
    File file = new File(ConfigSaverExtern.getDessinDir(baseDir), ressource.getId() + PlanimetryGISGMLZoneExporter.GML_EXTENSION);
    if (!file.exists()) {
      file = new File(ConfigSaverExtern.getDessinDir(baseDir), ressource.getId() + PlanimetryGISGMLZoneExporter.SHP_EXTENSION);
      fmt = SigFormatHelper.EnumFormat.SHP;
    }
    if (!file.exists()) {
      return null;
    }
    Map<String, String> optionsBykey = new HashMap<>();
    optionsBykey.put(LayerControllerSaverHelper.GEO_FORMAT, fmt.name());
    Pair<FSigGeomSrcData, SigFormatHelper.EnumFormat> pair = new LayerControllerSaverHelper().loadGisData(file, optionsBykey, ressource.getNom(), log);
    if (pair == null) {
      return null;
    }
    FSigGeomSrcData result = pair.first;
    if (result == null) {
      log.addError("externConfig.geo.NoDataFound.error", ressource.getNom(), file.getName());
      return null;
    }
    return result;
  }
}
