/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import java.util.List;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frederic Deniger
 */
public class SectionProfilToBrancheParametersChangeNode extends AbstractNodeFirable {

  public SectionProfilToBrancheParametersChangeNode(SectionProfilToBrancheParametersChange object) {
    super(Children.LEAF, Lookups.fixed(object));
    setName(object.getSectionName());
    setDisplayName(object.getSectionName());
  }

  @Override
  public boolean isEditMode() {
    return true;
  }

  @Override
  protected Sheet createSheet() {
    SectionProfilToBrancheParametersChange tc = getLookup().lookup(SectionProfilToBrancheParametersChange.class);
    Sheet res = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    res.put(set);
    PropertyNodeBuilder builder = new PropertyNodeBuilder();
    List<PropertySupportReflection> createFromPropertyDesc = builder.createFromPropertyDesc(DecimalFormatEpsilonEnum.COMPARISON, tc, this);
    for (PropertySupportReflection prop : createFromPropertyDesc) {
      set.put(prop);
    }
    return res;
  }
}
