/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.configuration;

import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLigneBriseeLayerModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryTraceLayerModel;

/**
 *
 * @author Frederic Deniger
 */
public interface TraceConfigurationExtra {

  String getDisplayName(DonPrtGeoProfilSection profilSection, PlanimetryTraceLayerModel modeleDonnees, TraceConfiguration aThis, boolean selected);

  void decoreTraceLigne(CatEMHSection section, TraceLigneModel _ligne, PlanimetryLigneBriseeLayerModel model, TraceConfiguration aThis);
}
