package org.fudaa.fudaa.crue.planimetry.configuration;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.fudaa.crue.common.config.ConfigurationInfoHelper;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 ** Attention. Les name des set sont utilisés pour la persistence dans le xml. Donc, ne pas changer sans rejouer les tests unitaires et faire des
 * reprises
 *
 * @author deniger
 */
public class NodeConfigurationInfo {

  public static final String PREFIX_NODES = "noeuds";

  public static List<Sheet.Set> createSet(NodeConfiguration in) {
    List<Sheet.Set> res = new ArrayList<>();
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setName(PREFIX_NODES);
    set.put(VisuConfigurationInfo.createTransparency(in));
    set.setDisplayName(NbBundle.getMessage(VisuConfigurationInfo.class, "NodeConfiguration.DisplayName"));
    set.setShortDescription(set.getDisplayName());
    res.add(set);
    res.add(TraceIconModelConfigurationInfo.createSet(in.iconModel, PREFIX_NODES));
    try {
      PropertySupport.Reflection displayCasierName = ConfigurationInfoHelper.create(NodeConfiguration.PROP_DISPLAY_CASIER_NAME,
              Boolean.TYPE, in,
              "NodeConfiguration.DisplayCasierName", VisuConfigurationInfo.class);
      res.add(LabelConfigurationInfo.createSet(in.getLabelConfiguration(), PREFIX_NODES, displayCasierName, true));
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return res;
  }
}
