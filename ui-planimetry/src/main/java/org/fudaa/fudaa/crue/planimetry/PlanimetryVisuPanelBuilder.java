package org.fudaa.fudaa.crue.planimetry;

import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.BCalquePaletteInfo;
import org.fudaa.ebli.calque.ZCalqueGeometry;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.commun.*;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerBuilder;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryGisModelContainer;
import si.uom.SI;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;
import java.util.List;

/**
 * @author deniger
 */
public class PlanimetryVisuPanelBuilder {
  private PlanimetryVisuPanelBuilder() {
  }

  public static PlanimetryVisuPanel build(final CtuluUI ui, final PlanimetryGisModelContainer context, final CrueConfigMetier crueConfigMetier,
                                          final VisuConfiguration configuration, final boolean modelling) {
    final PlanimetryVisuPanel res = new PlanimetryVisuPanel(null, ui);
    res.getScene().setRestrictedToCalqueActif(false);
    res.getScene().setSelectionMode(ZCalqueGeometry.SelectionMode.NORMAL);
    final PlanimetryController calqueContext = PlanimetryControllerBuilder.build(res, context, crueConfigMetier, configuration, modelling);
    res.init(calqueContext);
    final EbliCoordinateDefinition[] xy = new EbliCoordinateDefinition[2];
    final EbliFormatter fmt = new EbliFormatter();
    fmt.setFormatter(CtuluNumberFormatDefault.DEFAULT_FMT_3DIGITS);
    fmt.setUnit(SI.METRE);
    xy[0] = new EbliCoordinateDefinition("X", fmt);
    xy[1] = new EbliCoordinateDefinition("Y", fmt);
    res.setCoordinateDefinitions(xy);
    return res;
  }

  public static JComponent buildTopComponent(final PlanimetryVisuPanel panel) {
    return buildTopComponent(panel, (JComponent[]) null);
  }

  public static JComponent buildTopComponent(final PlanimetryVisuPanel panel, final JComponent... otherSpecificEditButtons) {
    final List<EbliActionInterface> actions = panel.getController().getActions();
    final List tools = EbliLib.updateToolButtons(actions, null);
    final JToolBar bar = new JToolBar();
    bar.setFloatable(false);
    for (final Object object : tools) {
      if (object == null) {
        bar.addSeparator();
      } else {
        bar.add((JComponent) object);
      }
    }
    final JPanel top = new JPanel(new BuVerticalLayout());
    top.add(bar);

    final JToolBar componentBar = new JToolBar();
    componentBar.setFloatable(false);
    panel.getConfigurer().addSpecificButtonsInToolbar(panel, componentBar);
    if (otherSpecificEditButtons != null) {
      for (final JComponent jComponent : otherSpecificEditButtons) {
        componentBar.add(jComponent);
        panel.getPlanimetryVisuController().addEditionComponent(jComponent);
      }
    }
    top.add(componentBar);
    return top;
  }

  public static JComponent createOverview(final ZEbliCalquesPanel visuPanel) {
    final JScrollPane scrollPane = new JScrollPane(new BArbreCalque(visuPanel.getArbreCalqueModel()));
    scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
    scrollPane.setPreferredSize(new Dimension(200, 500));
    scrollPane.setSize(new Dimension(200, 500));
    final Collection<EbliActionPaletteAbstract> acts = visuPanel.getController().getTabPaletteAction();

    final BuTabbedPane tb = new BuTabbedPane();
    for (final EbliActionPaletteAbstract palette : acts) {
      final JComponent component = palette.getPaletteContent();
      if (component instanceof BCalquePaletteInfo) {
        ((BCalquePaletteInfo) component).setAvailable(true);
        ((BCalquePaletteInfo) component).updateState();
      }
      palette.setEnabled(true);
      palette.updateBeforeShow();
      tb.addTab(palette.getTitle(), palette.getIcon(), component);
    }

    // -- insert avec infos + arbre des calques --//
    final JSplitPane splitInfosArbre = new JSplitPane(JSplitPane.VERTICAL_SPLIT);

    final BuPanel scrollInfos = visuPanel.buildInfosCreationComposant();
    scrollInfos.setMinimumSize(new Dimension((int) scrollInfos.getSize().getWidth(), 100));

    splitInfosArbre.setTopComponent(scrollInfos);
    splitInfosArbre.setBottomComponent(scrollPane);
    splitInfosArbre.setDividerLocation(0.3D);

    final JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
    splitPane.setTopComponent(tb);
    splitPane.setOneTouchExpandable(true);
    splitPane.setBottomComponent(splitInfosArbre);
    splitPane.setDividerLocation(0.3D);
    return splitPane;
  }
}
