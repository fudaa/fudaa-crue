/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import java.util.List;

/**
 *
 * @author Frederic Deniger
 */
public class SectionProfilToBrancheParametersInput {

  SectionProfilToBrancheParametersBranche branchesParameters;
  List<SectionProfilToBrancheParametersSection> sectionParameters;

  public SectionProfilToBrancheParametersBranche getBranchesParameters() {
    return branchesParameters;
  }

  public void setBranchesParameters(SectionProfilToBrancheParametersBranche branchesParameters) {
    this.branchesParameters = branchesParameters;
  }

  public List<SectionProfilToBrancheParametersSection> getSectionParameters() {
    return sectionParameters;
  }

  public void setSectionParameters(List<SectionProfilToBrancheParametersSection> sectionParameters) {
    this.sectionParameters = sectionParameters;
  }

  public boolean isEmpty() {
    return branchesParameters == null || branchesParameters.isEmpty() || sectionParameters == null || sectionParameters.isEmpty();
  }
}
