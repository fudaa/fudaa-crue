/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JToolBar;
import org.fudaa.ebli.calque.action.SceneShowDistanceAction;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class PlanimetryVisuPanelConfigurer {
  
  public void addApplicationActions(final PlanimetryVisuPanel panel, List<EbliActionInterface> arrayList) {
    arrayList.add(null);
    final SceneShowDistanceAction distanceAction = new SceneShowDistanceAction(panel.getEditor(), panel.getEbliFormatter());
    distanceAction.setDialogIsModal(false);
    arrayList.add(distanceAction);
    arrayList.add(new ConfigureUIAction(panel));
  }
  
  public JCheckBox createOneLayerCheckBox(final PlanimetryVisuPanel panel) {
    final JCheckBox oneLayer = new JCheckBox(NbBundle.getMessage(PlanimetryVisuPanelBuilder.class, "OneLayer.Action"));
    oneLayer.setToolTipText(NbBundle.getMessage(PlanimetryVisuPanelBuilder.class, "OneLayer.Tooltip"));
    oneLayer.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        panel.setWokOnOneLayer(oneLayer.isSelected());
      }
    });
    return oneLayer;
  }
  
  public void addSpecificButtonsInToolbar(final PlanimetryVisuPanel panel, JToolBar componentBar) {
    JButton btRedoLayout = new JButton(NbBundle.getMessage(PlanimetryVisuPanelBuilder.class, "RebuildLayout.Action"));
    btRedoLayout.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        panel.getPlanimetryController().rebuildLayout();
      }
    });
    JButton btCheckMiniDistance = new JButton(NbBundle.getMessage(PlanimetryVisuPanelBuilder.class, "CheckMinDistance.Action"));
    btCheckMiniDistance.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        panel.getPlanimetryController().checkMinimalDistance();
      }
    });
    panel.getPlanimetryVisuController().addEditionComponent(btRedoLayout);
    panel.getPlanimetryVisuController().addEditionComponent(btCheckMiniDistance);
    componentBar.add(btRedoLayout);
    componentBar.add(btCheckMiniDistance);
  }
}
