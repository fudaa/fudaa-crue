package org.fudaa.fudaa.crue.planimetry.layer;

import java.io.File;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessource;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper.EnumFormat;
import org.fudaa.fudaa.crue.planimetry.configuration.PointConfiguration;
import org.fudaa.fudaa.crue.planimetry.controller.ExternLayerFactory;
import org.fudaa.fudaa.crue.planimetry.save.ConfigExternIds;
import org.fudaa.fudaa.crue.planimetry.save.LayerPointUnmodifiableControllerSaver;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 *
 * @author deniger
 */
public class PlanimetryPointExternUnmodifiableLayer extends AbstractPlanimetryPointExternLayer {

  final File file;
  private final SigFormatHelper.EnumFormat fmt;

  public PlanimetryPointExternUnmodifiableLayer(PlanimetryPointLayerModel _modele, FSigEditor _editor, File file,
          SigFormatHelper.EnumFormat fmt) {
    super(_modele, _editor);
    this.fmt = fmt;
    this.file = file;
    this.layerConfiguration = new PointConfiguration();
  }

  @Override
  public PlanimetryAdditionalLayerContrat cloneLayer(FSigEditor editor) {
    return ExternLayerFactory.cloneLayerPointUnmodifiable(this);
  }

  

  public File getFile() {
    return file;
  }

  public EnumFormat getFmt() {
    return fmt;
  }

  @Override
  public boolean isDestructible() {
    return true;
  }

  @Override
  public CrueEtudeExternRessource getSaveRessource(File baseDir, String id) {
    LayerPointUnmodifiableControllerSaver save = new LayerPointUnmodifiableControllerSaver();
    return save.getSaveRessource(id, getLayerController(), baseDir);
  }

  @Override
  public String getTypeId() {
    return ConfigExternIds.LAYER_POINT_UNMODIFIABLE_SAVE_ID;
  }
}
