package org.fudaa.fudaa.crue.planimetry.configuration;

import java.awt.Color;
import java.util.Arrays;
import javax.swing.SpinnerNumberModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.common.config.ConfigurationInfoHelper;
import org.fudaa.fudaa.crue.common.editor.SpinnerInplaceEditor;
import org.fudaa.fudaa.crue.common.editor.TypeLineIconPropertyEditorSupport;
import org.fudaa.fudaa.crue.common.editor.TypeLineJComboBoxInplaceEditor;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class TraceLineModelConfigurationInfo {

  public static Sheet.Set createSet(TraceLigneModel in, String prefix, boolean useColor) {
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setDisplayName(NbBundle.getMessage(LabelConfigurationInfo.class, "LineConfiguration.DisplayName"));
    set.setShortDescription(set.getDisplayName());
    set.setName(prefix + ".line");
    try {
      set.put(createTypeLigne(in));
      set.put(createEpaisseurLigne(in));
      if (useColor) {
        set.put(createForegroundColor(in));
      }
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return set;
  }

  private static PropertySupport.Reflection createTypeLigne(TraceLigneModel in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(TraceLigneModel.PROPERTY_TYPE,
            Integer.TYPE, in, "lineType", VisuConfigurationInfo.class);
    res.setPropertyEditorClass(TypeLineIconPropertyEditorSupport.class);
    final TypeLineJComboBoxInplaceEditor typeIconJComboBoxInplaceEditor = new TypeLineJComboBoxInplaceEditor(
            Arrays.asList(TraceLigne.LISSE, TraceLigne.MIXTE,
            TraceLigne.POINTILLE, TraceLigne.TIRETE,
            TraceLigne.INVISIBLE));
    res.setValue("inplaceEditor", typeIconJComboBoxInplaceEditor);
    return res;
  }

  private static PropertySupport.Reflection createEpaisseurLigne(TraceLigneModel in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(TraceLigneModel.PROPERTY_EPAISSEUR,
            Float.TYPE, in, "lineEpaisseurLigne", VisuConfigurationInfo.class);
    SpinnerInplaceEditor inplaceEditor = new SpinnerInplaceEditor(new SpinnerNumberModel(Float.valueOf(1.5f), Float.valueOf(1f),
            Float.valueOf(100f), Float.valueOf(0.5f)));
    res.setValue("inplaceEditor", inplaceEditor);
    return res;
  }

  private static PropertySupport.Reflection createForegroundColor(TraceLigneModel in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(TraceLigneModel.PROPERTY_COULEUR,
            Color.class, in, "lineCouleurForeground", VisuConfigurationInfo.class);
    return res;
  }
}
