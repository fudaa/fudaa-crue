package org.fudaa.fudaa.crue.planimetry.configuration;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import org.fudaa.dodico.crue.config.ccm.NumberRangeValidator;
import org.fudaa.fudaa.crue.common.config.ConfigurationInfoHelper;
import org.fudaa.fudaa.crue.common.editor.SliderTransparencyPropertyEditor;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.PropertySupport.Reflection;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class VisuConfigurationInfo {

  private static final Map<String, NumberRangeValidator> VALIDATOR_BY_PROPS = new HashMap<>();

  public static PropertySupport.Reflection createTransparency(DefaultConfiguration in) {
    try {
      PropertySupport.Reflection res = ConfigurationInfoHelper.create(NodeConfiguration.PROP_TRANSPARENCE,
              int.class, in, VisuConfigurationInfo.class);
      res.setPropertyEditorClass(SliderTransparencyPropertyEditor.class);
      res.setValue("suppressCustomEditor", Boolean.TRUE);
      return res;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return null;
  }

  static {
    VALIDATOR_BY_PROPS.put(VisuConfiguration.PROP_DEFAULT_CASIER_DIMENSION, new NumberRangeValidator(new Double(1), null));
    VALIDATOR_BY_PROPS.put(VisuConfiguration.PROP_MINIMAL_DISTANCE_BEETWEEN_NODE, new NumberRangeValidator(new Double(1), null));
    VALIDATOR_BY_PROPS.put(VisuConfiguration.PROP_MINIMAL_DISTANCE_BEETWEEN_PARALLEL_EDGE, new NumberRangeValidator(new Double(1),
            null));
  }

  public static boolean isValide(String props, double value) {
    NumberRangeValidator get = VALIDATOR_BY_PROPS.get(props);
    if (get == null) {
      return true;
    }
    return get.containsNumber(value);
  }

  public static Sheet.Set createSet(VisuConfiguration in) {
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setName("general");//attention, utilise pour la persistence donc ne pas changer sans vérifier les tests.
    set.setDisplayName(NbBundle.getMessage(VisuConfigurationInfo.class, "VisuConfigration.DisplayName"));

    try {

      set.put(createMinimalDistanceBeetweenNodes(in));
      set.put(createMinimalDistanceBeetweenParallelEdges(in));
      set.put(createDefaultCasierDimension(in));
      set.put(createInactiveEMHProperty(in));
      set.put(createInactiveEMHColor(in));
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return set;
  }

  private static Reflection createMinimalDistanceBeetweenNodes(VisuConfiguration in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(VisuConfiguration.PROP_MINIMAL_DISTANCE_BEETWEEN_NODE,
            Double.TYPE, in, VisuConfigurationInfo.class);
    return res;
  }

  private static Reflection createInactiveEMHColor(VisuConfiguration in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(VisuConfiguration.PROP_INACTIVE_EMH_COLOR,
            Color.class, in, VisuConfigurationInfo.class);
    return res;
  }

  private static Reflection createMinimalDistanceBeetweenParallelEdges(VisuConfiguration in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(VisuConfiguration.PROP_MINIMAL_DISTANCE_BEETWEEN_PARALLEL_EDGE,
            Double.TYPE, in, VisuConfigurationInfo.class);
    return res;
  }

  private static Reflection createDefaultCasierDimension(VisuConfiguration in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(VisuConfiguration.PROP_DEFAULT_CASIER_DIMENSION,
            Double.TYPE, in, VisuConfigurationInfo.class);
    return res;
  }

  private static Reflection createInactiveEMHProperty(VisuConfiguration in) throws NoSuchMethodException {
    PropertySupport.Reflection showInactiveEMH = ConfigurationInfoHelper.create(VisuConfiguration.PROP_INACTIVE_EMH_VISIBLE,
            Boolean.TYPE, in, VisuConfigurationInfo.class);
    return showInactiveEMH;
  }
}
