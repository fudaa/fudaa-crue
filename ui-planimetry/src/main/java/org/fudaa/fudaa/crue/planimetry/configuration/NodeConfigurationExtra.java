/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.configuration;

import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryPointLayerModel;

/**
 *
 * @author Frederic Deniger
 */
public interface NodeConfigurationExtra {

  void decoreTraceIcon(TraceIconModel ligne, CatEMHNoeud noeud, PlanimetryPointLayerModel model);

  String getDisplayedLabel(CatEMHNoeud noeud, NodeConfiguration aThis, boolean isSelected);
}
