package org.fudaa.fudaa.crue.planimetry.action;

import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class DeleteEMHCascadeAction extends EbliActionSimple {

  private final FSigEditor editor;

  public DeleteEMHCascadeAction(FSigEditor editor) {
    super(NbBundle.getMessage(DeleteEMHCascadeAction.class, "DeleteEMHCascade.Name"), BuResource.BU.getIcon("enlever"),
          "DeleteEMHCascade");
    this.editor = editor;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    if (editor.cancelEdition()) {
      return;
    }
    final ZScene scene = editor.getSceneEditor().getScene();
    ZCalqueEditable[] cqs = scene.getEditableLayers();
    for (int i = 0; i < cqs.length; i++) {
      if (cqs[i] instanceof LayerWithCascadeDeleteAction) {
        ((LayerWithCascadeDeleteAction) cqs[i]).removeCascadeSelectedObjects();
      }
    }
  }

  @Override
  public void updateStateBeforeShow() {
    super.setEnabled(editor.getTarget() instanceof LayerWithCascadeDeleteAction && !editor.getTarget().isSelectionEmpty()
                     && ((LayerWithCascadeDeleteAction) editor.getTarget()).isEditable());
  }
}
