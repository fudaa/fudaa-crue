/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

/**
 * Les paramètres avec les intersections déterminées.
 *
 * @author Frederic Deniger
 */
public class SectionProfilToBrancheParametersComputed {

  public static class BrancheIntersection implements ObjetWithID {

    private final double xpSchematique;
    private final String brancheName;
    private final double longueurSchematique;

    public BrancheIntersection(double xpSchematique, String brancheName, double longueurSchematique) {
      this.xpSchematique = xpSchematique;
      this.longueurSchematique = longueurSchematique;
      this.brancheName = brancheName;
    }

    public double getLongueurSchematique() {
      return longueurSchematique;
    }

    @Override
    public String getId() {
      return getNom();
    }

    @Override
    public String getNom() {
      return brancheName;
    }

    public double getXpSchematique() {
      return xpSchematique;
    }

    public String getBrancheName() {
      return brancheName;
    }
  }

  public static class IntersectedBranche {

    private final List<BrancheIntersection> brancheNames;

    public IntersectedBranche(
            List<BrancheIntersection> brancheNames) {
      this.brancheNames = brancheNames;
    }

    public List<BrancheIntersection> getBrancheNames() {
      return brancheNames;
    }
  }
  private final LinkedHashMap<SectionProfilToBrancheParametersSection, IntersectedBranche> targetsBrancheByParameter = new LinkedHashMap<>();

  public void addIntersection(SectionProfilToBrancheParametersSection param, List<BrancheIntersection> intersectedBranche) {
    targetsBrancheByParameter.put(param, new IntersectedBranche(intersectedBranche));
  }

  public Map<SectionProfilToBrancheParametersSection, IntersectedBranche> getTargetsBrancheByParameter() {
    return Collections.unmodifiableMap(targetsBrancheByParameter);
  }
}
