package org.fudaa.fudaa.crue.planimetry.layer;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.edition.EditionDelete;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.planimetry.configuration.SectionConfigurationExtra;
import org.fudaa.fudaa.crue.planimetry.controller.LayerSectionController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author deniger
 */
public class PlanimetrySectionLayerModel extends PlanimetryPointLayerModel<LayerSectionController> {

  SectionConfigurationExtra sectionConfigurationExtra;

  public PlanimetrySectionLayerModel(GISZoneCollectionPoint _zone, LayerSectionController layerModelController) {
    super(_zone, layerModelController);
  }

  public SectionConfigurationExtra getSectionConfigurationExtra() {
    return sectionConfigurationExtra;
  }

  public void setSectionConfigurationExtra(SectionConfigurationExtra sectionConfigurationExtra) {
    this.sectionConfigurationExtra = sectionConfigurationExtra;
  }

  @Override
  public boolean isGeometryVisible(int idx) {
    CatEMHSection  section = getLayerController().getEMHFromPositionInModel(idx);
    PlanimetryControllerHelper helper = getLayerController().getHelper();
    return helper.isVisible(section);
  }

  @Override
  public boolean removePoint(int[] _idx, CtuluCommandContainer _cmd) {
    return deleteSections(_idx, false);
  }

  public boolean removePointCascade(int[] _idx, CtuluCommandContainer _cmd) {
    return deleteSections(_idx, true);
  }

  public boolean deleteSections(int[] _idx, final boolean cascade) {
    PlanimetryControllerHelper helper = super.getLayerController().getHelper();
    if (!helper.canEMHBeAddedAndDisplayError()) {
      return false;
    }
    helper.scenarioEditionStarted();
    List<EMH> sections = new ArrayList<>();
    for (int i = 0; i < _idx.length; i++) {
      sections.add(getLayerController().getEMHFromPositionInModel(_idx[i]));
    }
    final EMHScenario scenario = helper.getPlanimetryGisModelContainer().getScenario();
    //a utiliser
    EditionDelete.RemoveBilan deleted = EditionDelete.delete(sections, scenario, cascade);
    if (deleted.isSomethingRemoved()) {
      getLayerController().getHelper().getSectionController().rebuildGis();
      getLayerController().getHelper().getTraceController().rebuildGis();
    }
    helper.displayBilan(deleted);
    return deleted.isSomethingRemoved();
  }
}
