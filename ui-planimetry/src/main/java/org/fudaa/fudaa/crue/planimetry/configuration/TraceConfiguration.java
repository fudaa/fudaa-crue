package org.fudaa.fudaa.crue.planimetry.configuration;

import java.awt.Color;
import javax.swing.SwingConstants;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLigneBriseeLayerModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryTraceLayerModel;

/**
 *
 * @author deniger
 */
public class TraceConfiguration extends AbstractLigneBriseeConfiguration {

  public static final String PROPERTY_PROFIL_VUE_DE_AMONT = "profilVueDeAmont";
  public static final String PROPERTY_SECTION_LABEL_POSITION = "sectionLabelPosition";
  TraceLigneModel lineModel = new TraceLigneModel(TraceLigne.LISSE, 1.5f, Color.BLACK);
  TraceIconModel iconModelLimiteLit = new TraceIconModel(TraceIcon.CROIX, 4, Color.RED);
  TraceIconModel iconModelPtProfil = new TraceIconModel(TraceIcon.CROIX, 4, Color.RED);
  LabelConfiguration litLabelConfiguration = new LabelConfiguration();
  LabelConfiguration sectionLabelConfiguration = new LabelConfiguration();
  private final VisuConfiguration parent;
  boolean profilVueDeAmont = false;
  private int angleInDegree = 0;
  int sectionLabelPosition = SwingConstants.RIGHT;

  public TraceConfiguration(VisuConfiguration parent) {
    litLabelConfiguration.setLabelAlignment(SwingConstants.NORTH);
    sectionLabelConfiguration.setLabelAlignment(SwingConstants.EAST);
    iconModelLimiteLit.setTypeLigne(TraceLigne.LISSE);
    iconModelLimiteLit.setEpaisseurLigne(1f);

    iconModelPtProfil.setTypeLigne(TraceLigne.LISSE);
    iconModelPtProfil.setEpaisseurLigne(1.5f);

    litLabelConfiguration.setLabelDistance(5);
    this.parent = parent;
  }

  public TraceConfiguration(TraceConfiguration from, final VisuConfiguration parent) {
    super(from);
    if (from != null) {
      this.lineModel = new TraceLigneModel(lineModel);
      this.iconModelLimiteLit = new TraceIconModel(from.iconModelLimiteLit);
      this.iconModelPtProfil = new TraceIconModel(from.iconModelPtProfil);
      this.litLabelConfiguration = from.litLabelConfiguration.copy();
      this.sectionLabelConfiguration = from.sectionLabelConfiguration.copy();
      this.profilVueDeAmont = from.profilVueDeAmont;
      this.angleInDegree = from.angleInDegree;
    }
    this.parent = parent;
  }

  @Override
  public TraceConfiguration copy() {
    return new TraceConfiguration(this, parent);
  }

  public boolean isProfilVueDeAmont() {
    return profilVueDeAmont;
  }

  public void setProfilVueDeAmont(boolean profilVueDeAmont) {
    this.profilVueDeAmont = profilVueDeAmont;
  }

  public int getAngleInDegree() {
    return angleInDegree;
  }

  public VisuConfiguration getParent() {
    return parent;
  }

  public int getSectionLabelPosition() {
    return sectionLabelPosition;
  }

  public void setSectionLabelPosition(int sectionLabelPosition) {
    this.sectionLabelPosition = sectionLabelPosition;
  }

  public LabelConfiguration getLitLabelConfiguration() {
    return litLabelConfiguration;
  }

  public LabelConfiguration getSectionLabelConfiguration() {
    return sectionLabelConfiguration;
  }

  @Override
  public void initTraceLigne(TraceLigneModel ligne, int idxPoly, PlanimetryLigneBriseeLayerModel model) {
    ligne.updateData(lineModel);
    final PlanimetryTraceLayerModel traceModel = (PlanimetryTraceLayerModel) model;
    if ((traceModel).getTraceConfigurationExtra() != null) {
      CatEMHSection section = traceModel.getLayerController().getSection(idxPoly);
      ((PlanimetryTraceLayerModel) model).getTraceConfigurationExtra().decoreTraceLigne(section, ligne, model, this);
    }
  }

  @Override
  public void initTraceAtomicIcon(TraceIconModel traceIconModel, int idxPoly, PlanimetryLigneBriseeLayerModel model) {
    traceIconModel.setType(TraceIcon.RIEN);
  }

  public boolean isInactiveEMHVisible() {
    return parent.isInactiveEMHVisible();
  }

  public void initTraceIconLimiteLitOrEtiquette(TraceIconModel icon, int idxOfGeom) {
    icon.updateData(iconModelLimiteLit);
  }
  public void initTraceIconPtProfil(TraceIconModel icon, int idxOfGeom) {
    icon.updateData(iconModelPtProfil);
  }

  /**
   * @return angle en radians.
   */
  public double getAngleInRadians() {
    return Math.toRadians(angleInDegree);
  }
  boolean litLabelsPainted = true;
  boolean sectionLabelPainted = true;

  public boolean isLitLabelsPainted() {
    return getLitLabelConfiguration().isDisplayLabels();
  }

  public boolean isSectionLabelPainted() {
    return getSectionLabelConfiguration().isDisplayLabels();
  }

  public String getDisplayName(DonPrtGeoProfilSection profilSection, PlanimetryTraceLayerModel modeleDonnees, boolean selected) {
    String nom = null;
    if (modeleDonnees.getTraceConfigurationExtra() != null) {
      nom = modeleDonnees.getTraceConfigurationExtra().getDisplayName(profilSection, modeleDonnees, this, selected);
    }
    if (nom == null) {
      nom = profilSection.getNom();

    }
    return nom;
  }
}
