package org.fudaa.fudaa.crue.planimetry.action;

import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuGridLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.sig.SigLoaderHelper;
import org.fudaa.fudaa.crue.planimetry.sig.tracecasier.GGFileToTraceCasierProcessor;
import org.fudaa.fudaa.sig.wizard.FSigFileLoadResult;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderGIS;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.DialogDescriptor;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class TraceCasierImporterAction extends EbliActionSimple {

  private final PlanimetryController controller;

  public TraceCasierImporterAction(PlanimetryController controller) {
    super(NbBundle.getMessage(TraceCasierImporterAction.class, "TraceCasierImporterAction.ActionName"), null, TraceCasierImporterAction.class.
            getSimpleName());
    this.controller = controller;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    ChooseInputFiles panel = new ChooseInputFiles();
    panel.setPreferredSize(new Dimension(450, 350));
    DialogDescriptor dialogDescriptor = new DialogDescriptor(panel, getTitle());
    panel.setDialogDescriptor(dialogDescriptor);
    boolean accepted = DialogHelper.showQuestion(dialogDescriptor, getClass(), "importTraceCasiers", PerspectiveEnum.MODELLING,
            DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    if (accepted) {
      final File shapeFile = panel.getShapeFile();
      final File profilFile = panel.getProfilDbfFile();
      if (shapeFile != null && shapeFile.isFile()) {
        ProgressRunnable<CtuluLogResult<GISZoneCollectionLigneBrisee>> runnable = new ProgressRunnable<CtuluLogResult<GISZoneCollectionLigneBrisee>>() {
          @Override
          public CtuluLogResult<GISZoneCollectionLigneBrisee> run(ProgressHandle handle) {
            return loadFiles(shapeFile, profilFile, handle);
          }
        };
        final String actionDescription = getTitle() + ": " + shapeFile.getName();
        CtuluLogResult<GISZoneCollectionLigneBrisee> result = CrueProgressUtils.
                showProgressDialogAndRun(runnable, actionDescription);
        if (result.getLog().isNotEmpty()) {
          LogsDisplayer.displayError(result.getLog(), actionDescription);
        }
        final GISZoneCollectionLigneBrisee casiersZone = result.getResultat();
        if (casiersZone != null) {
          controller.getGroupExternController().addTraceCasiers(casiersZone, shapeFile, SigFormatHelper.EnumFormat.SHP);
          DialogHelper.showNotifyOperationTermine(getTitle(), NbBundle.getMessage(TraceCasierImporterAction.class,
                  "TraceCasierImporterAction.Result", casiersZone.getNumGeometries()));

        }
      }
    }
  }

  private CtuluLogResult<GISZoneCollectionLigneBrisee> loadFiles(File shapeFile, File profilFile, ProgressHandle progress) {
    progress.switchToIndeterminate();
    FSigFileLoadResult result = new FSigFileLoadResult();
    FSigFileLoaderGIS loader = new FSigFileLoaderGIS.Shape();
    CtuluAnalyze analyse = new CtuluAnalyze();
    loader.setInResult(result, shapeFile, shapeFile.getAbsolutePath(), null,
            analyse);
    GISZoneCollectionLigneBrisee casiers = SigLoaderHelper.buildLine(result.createData());
    GGFileToTraceCasierProcessor processor = new GGFileToTraceCasierProcessor(casiers, profilFile, controller.getCrueConfigMetier());
    CtuluLogResult<GISZoneCollectionLigneBrisee> toCasier = processor.toCasier();
    return toCasier;
  }

  private static class ChooseInputFiles extends CtuluDialogPanel {

    private final CtuluFileChooserPanel chooseShpFile;
    private final CtuluFileChooserPanel chooseDbfFile;
    DialogDescriptor dialogDescriptor;

    public ChooseInputFiles() {
      setLayout(new BuGridLayout(2, 5, 10));
      chooseShpFile = addFileChooserPanel(this, NbBundle.getMessage(TraceCasierImporterAction.class, "importTraceCasier.ChooseShapefile.Label"), false,
              false);
      chooseShpFile.setAllFileFilter(true);
      chooseShpFile.setFilter(new FileFilter[]{new BuFileFilter("shp")});
      chooseShpFile.getTf().getDocument().addDocumentListener(new DocumentListener() {
        @Override
        public void insertUpdate(DocumentEvent e) {
          updateValidState();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
          updateValidState();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
          updateValidState();
        }
      });
      chooseDbfFile = addFileChooserPanel(this, NbBundle.getMessage(TraceCasierImporterAction.class, "importTraceCasier.ChooseProfilDbf.Label"), false,
              false);
      chooseDbfFile.setAllFileFilter(true);
      chooseDbfFile.setFilter(new FileFilter[]{new BuFileFilter("dbf")});
    }

    public void setDialogDescriptor(DialogDescriptor dialogDescriptor) {
      this.dialogDescriptor = dialogDescriptor;
      updateValidState();
    }

    protected void updateValidState() {
      if (getShapeFile() != null && chooseDbfFile.getFile() == null) {
        chooseDbfFile.setInitDir(getShapeFile().getParentFile());
      }
      if (dialogDescriptor != null) {
        dialogDescriptor.setValid(getShapeFile() != null && getShapeFile().isFile());
      }
    }

    protected File getShapeFile() {
      return chooseShpFile.getFile();
    }

    protected File getProfilDbfFile() {
      return chooseDbfFile.getFile();
    }
  }
}
