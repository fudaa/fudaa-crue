package org.fudaa.fudaa.crue.planimetry.controller;

import org.locationtech.jts.geom.Coordinate;
import gnu.trove.TIntArrayList;
import gnu.trove.TLongDoubleHashMap;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.crue.planimetry.GeometryIndexer;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.action.OpenEMHAction;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.layer.LayerControllerEMH;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryBrancheLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryBrancheModel;
import org.fudaa.fudaa.crue.planimetry.listener.BrancheUpdaterFromNode;
import org.openide.util.NbBundle;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author deniger
 */
public class LayerBrancheController extends LayerModelControllerDefault<PlanimetryBrancheLayer> implements LayerControllerEMH {

  protected static LayerBrancheController install(PlanimetryController ctx, PlanimetryVisuPanel res,
          VisuConfiguration configuration) {

    LayerBrancheController brancheController = new LayerBrancheController();
    brancheController.layer = new PlanimetryBrancheLayer(new PlanimetryBrancheModel(ctx.gisModel.createEdgeCollection(),
            brancheController),
            res.getEditor());
    brancheController.layer.setLayerConfiguration(configuration.getBrancheConfiguration());
    brancheController.layer.setName(PlanimetryController.NAME_CQ_BRANCHE);
    brancheController.layer.setTitle(NbBundle.getMessage(PlanimetryControllerBuilder.class, PlanimetryController.NAME_CQ_BRANCHE));

    ctx.controllersByName.put(PlanimetryController.NAME_CQ_BRANCHE, brancheController);
    return brancheController;
  }
  GeometryIndexer brancheIndexer;

  public int getBranchePosition(Long uid) {
    return brancheIndexer.getPosition(uid);
  }

  @Override
  public void setVisuConfiguration(VisuConfiguration cloned) {
    getLayer().setLayerConfiguration(cloned.getBrancheConfiguration());
  }

  @Override
  protected void geometryDataUpdated() {
    if (brancheIndexer != null) {
      brancheIndexer.reindex();
    }
  }

  @Override
  public List<ToStringInternationalizable> getCategories() {
    return Arrays.asList(EnumBrancheType.values());
  }

  public TLongDoubleHashMap getDistanceByBrancheUid() {
    TLongDoubleHashMap res = new TLongDoubleHashMap();
    final GISZoneCollectionLigneBrisee brancheCollection = getBrancheCollection();
    int nb = brancheCollection.getNumGeometries();
    for (int i = 0; i < nb; i++) {
      GISPolyligne brancheGis = (GISPolyligne) brancheCollection.getGeometry(i);
      Long uid = helper.getUid(brancheCollection, i);
      res.put(uid.longValue(), brancheGis.getLength());
    }
    return res;
  }

  @Override
  public void changeWillBeDone() {
    //modif deja effectuée
    if (isModified()) {
      return;
    }
    super.changeWillBeDone();
    //on sauvegarde également les autres données hydrau
    getHelper().getNodeController().changeWillBeDone();
    getHelper().getCasierController().changeWillBeDone();
  }
  BrancheUpdaterFromNode brancheUpdaterFromNode;

  @Override
  public void init(PlanimetryControllerHelper helper) {
    super.init(helper);
    PlanimetryVisuPanel res = helper.getController().getVisuPanel();
    layer.setActions(new EbliActionInterface[]{
      new OpenEMHAction.Reopen(helper.getController()),
      new OpenEMHAction.NewFrame(helper.getController()),
      null,
      res.getSigEditAction(),
      null,
      res.getEditor().getExportWithNameAction(),
//      res.getEditor().getExportAction(),
      null,
      helper.getController().getProjectEMHOnSIGAction(),
      helper.getController().getTraceProfilAttachedToBrancheAction(),
      null,
      res.getEditor().getActionDelete(),
      res.getPlanimetryVisuController().getDeleteCascade(),
      null,
      res.createFindAction(layer)});
    brancheIndexer = new GeometryIndexer(layer.modeleDonnees(), helper.getPlanimetryGisModelContainer());
    listenerDispatcher.addListener(brancheIndexer);

    //pour que les noeuds suivent la branche
    brancheUpdaterFromNode = new BrancheUpdaterFromNode(this);
    //on écoute les noeuds:
    helper.getNodeController().addGISListener(brancheUpdaterFromNode);
  }

  public CatEMHBranche getBranche(int position) {
    Long uid = helper.getUid(getBrancheCollection(), position);
    return getEMH(uid);
  }

  @Override
  public EMH getEMHFromLayerPosition(int position) {
    return getBranche(position);
  }

  public Long getAvalNodeUid(int branchePosition) {
    CatEMHNoeud noeud = getBranche(branchePosition).getNoeudAval();
    return noeud == null ? null : noeud.getUiId();
  }

  public void updateBrancheFromNode() {
    brancheUpdaterFromNode.updateAll();
  }

  /**
   *
   * @param branchePosition la position de la branche
   * @return l'uid du noeud amont
   */
  public Long getAmontNodeUid(int branchePosition) {
    CatEMHNoeud noeud = getBranche(branchePosition).getNoeudAmont();
    return noeud == null ? null : noeud.getUiId();
  }

  public GISZoneCollectionLigneBrisee getBrancheCollection() {
    return layer.modeleDonnees().getGeomData();
  }

  public Coordinate getAvalCoordinate(int branche) {
    GISPolyligne geometry = getBrancheGis(branche);
    return geometry.getCoordinateN(getAvalCoordinatePosition(geometry.getNumPoints()));
  }

  public GISPolyligne getBrancheGis(int idx) {
    return (GISPolyligne) getBrancheCollection().getGeometry(idx);
  }

  public Coordinate getAmontCoordinate(int branchePosition) {
    GISPolyligne geometry = getBrancheGis(branchePosition);
    return geometry.getCoordinateN(getAmontCoordinatePosition());
  }

  public static int getAmontCoordinatePosition() {
    return 0;
  }

  public static int getAvalCoordinatePosition(int nbPoint) {
    return nbPoint - 1;
  }

  public CrueConfigMetier getCrueConfigMetier() {
    return helper.getController().getCrueConfigMetier();
  }

  /**
   * Appele des qu'une modification global est terminee
   */
  public void globalChangedFinished() {
    this.brancheUpdaterFromNode.updateIfNeeded();
  }

  public boolean setSelectedEMHs(List<EMH> selectedEMH) {
    List<EMH> selectEMHS = EMHHelper.selectEMHS(selectedEMH, EnumCatEMH.BRANCHE);
    TIntArrayList positions = new TIntArrayList();
    for (EMH emh : selectEMHS) {
      int nodePosition = getBranchePosition(emh.getUiId());
      if (nodePosition >= 0) {
        positions.add(nodePosition);
      }
    }
    layer.setSelection(positions.toNativeArray());
    return !positions.isEmpty();
  }

  public boolean setSelectedEMHUids(Collection<Long> selectedUid) {
    TIntArrayList positions = new TIntArrayList();
    for (Long uid : selectedUid) {
      int nodePosition = getBranchePosition(uid);
      if (nodePosition >= 0) {
        positions.add(nodePosition);
      }
    }
    layer.setSelection(positions.toNativeArray());
    return !positions.isEmpty();
  }

  public void getSelectedEMHs(Collection<Long> uids) {
    if (!layer.isVisible() || layer.isSelectionEmpty()) {
      return;
    }
    int[] selectedIndex = layer.getSelectedIndex();
    if (selectedIndex != null) {
      for (int i = 0; i < selectedIndex.length; i++) {
        uids.add(helper.getUid(getBrancheCollection(), selectedIndex[i]));
      }
    }
  }
  private boolean brancheUpdatingFromPoint;

  public void unsetBrancheUidUpdatingFromPoint() {
    brancheUpdatingFromPoint = false;
  }

  public boolean isBrancheUpdatingFromPoint() {
    return brancheUpdatingFromPoint;
  }

  public void setBrancheUidUpdatingFromPoint() {
    brancheUpdatingFromPoint = true;
  }
}
