package org.fudaa.fudaa.crue.planimetry;

import org.jgrapht.graph.DefaultWeightedEdge;

/**
 *
 * @author deniger
 */
public class BrancheEdge extends DefaultWeightedEdge {

  private final Long id;

  public BrancheEdge(final Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final BrancheEdge other = (BrancheEdge) obj;
    return (this.id == null) ? (other.id == null) : this.id.equals(other.id);
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }
}
