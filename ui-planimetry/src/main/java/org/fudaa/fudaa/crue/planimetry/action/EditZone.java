package org.fudaa.fudaa.crue.planimetry.action;

import javax.swing.JComponent;

/**
 *
 * @author deniger
 */
public interface EditZone {
  
  void active(JComponent jc);
  void unactive(JComponent jc);

}
