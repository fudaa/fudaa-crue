/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.action;

import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.JFileChooser;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluImageContainer;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.gis.GisJGWFileReader;
import org.fudaa.ctulu.image.CtuluImageFileChooser;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZModeleImageRaster;
import org.fudaa.ebli.calque.ZModeleStatiqueImageRaster;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternImagesLayerGroup;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryImageRasterLayer;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class PlanimetryImageJGWImportAction extends EbliActionSimple {

  private final PlanimetryExternImagesLayerGroup target;

  public PlanimetryImageJGWImportAction(final PlanimetryExternImagesLayerGroup target) {
    super(NbBundle.getMessage(PlanimetryImageJGWImportAction.class, "importJGW.action"), null, "PlanimetryImageJGWImportAction");
    this.target = target;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    CtuluImageFileChooser chooserFile = new CtuluImageFileChooser(true, null);
    if (JFileChooser.APPROVE_OPTION == chooserFile.showOpenDialog(null)) {
      File file = chooserFile.getSelectedFile();
      boolean isJpeg = StringUtils.equalsIgnoreCase("jpg", CtuluLibFile.getExtension(file.getName())) || StringUtils.equalsIgnoreCase("jpeg",
              CtuluLibFile.
              getExtension(file.getName()));
      File ref = null;
      if (isJpeg) {
        ref = CtuluLibFile.changeExtension(file, "jgw");
      } else {
        ref = CtuluLibFile.changeExtension(file, "tfw");
      }
      if (!ref.exists()) {
        DialogHelper.showError(NbBundle.getMessage(PlanimetryImageJGWImportAction.class, "jgwFileNotFound.text", ref.getName()));
        return;
      }
      GisJGWFileReader read = new GisJGWFileReader();
      CtuluIOResult<GisJGWFileReader.ReadResult> result = read.read(ref);
      if (result.getAnalyze().containsErrorOrSevereError()) {
        LogsDisplayer.displayError(result.getAnalyze(), getTitle());
        return;
      }
      GisJGWFileReader.ReadResult source = result.getSource();
      ZModeleImageRaster model = create(source, file);
      if (model != null) {
        PlanimetryImageRasterLayer createCalqueImage = target.createCalqueImage(model);
        String findUniqueChildName = BGroupeCalque.findUniqueChildName(target);
        createCalqueImage.setName(findUniqueChildName);
        createCalqueImage.setTitle(file.getName());
        target.enDernier(createCalqueImage);
      }


    }
  }

  private ZModeleImageRaster create(GisJGWFileReader.ReadResult source, File file) {
    CtuluImageContainer image = new CtuluImageContainer(file);
    ZModeleStatiqueImageRaster raster = new ZModeleStatiqueImageRaster(image);
    GisJGWFileReader.TransformResult transformPoints = GisJGWFileReader.getTransformPoints(source, image);
    raster.setProj(transformPoints.imagePts, transformPoints.reelPts);
    return raster;


  }
}
