package org.fudaa.fudaa.crue.planimetry.action;

import java.awt.event.ActionEvent;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.fudaa.dodico.crue.edition.SimplificationProcessor;
import org.fudaa.dodico.crue.edition.SimplificationSeuils;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumCasierType;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class SimplifyProfilsAction extends EbliActionSimple {

  private final PlanimetryController controller;

  public SimplifyProfilsAction(PlanimetryController controller) {
    super(NbBundle.getMessage(SimplifyProfilsAction.class, "SimplifyProfils.Name"), null,
            "SimplifyProfilsAction");
    this.controller = controller;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    SimplificationSeuils seuils = controller.getSimplificationSeuilsProvider().getSeuils();
    if (seuils == null) {
      return;
    }
    controller.getHelper().scenarioEditionStarted();
    Set<Long> uids = new HashSet<>();
    controller.getSectionController().getSelectedEMHs(uids);
    controller.getTraceController().getSelectedEMHs(uids);
    controller.getCasierController().getSelectedEMHs(uids);
    List<EMH> emhs = controller.getVisuPanel().getEmhs(uids);
    new SimplificationProcessor(controller.getCrueConfigMetier(), seuils).simpify(emhs);
  }

  @Override
  public void updateStateBeforeShow() {
    boolean enable = controller.isEditable();
    if (enable) {
      List<EMH> selectedEMHs = controller.getVisuPanel().getSelectedEMHs();
      enable = EMHHelper.containsEMHWithSubCatTypes(selectedEMHs, EnumSectionType.EMHSectionProfil, EnumCasierType.EMHCasierProfil);
    }

    super.setEnabled(enable);
  }
}
