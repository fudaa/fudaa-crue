/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.layer;

import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessource;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.crue.planimetry.action.SectionProfilAttachedToBrancheAction;
import org.fudaa.fudaa.crue.planimetry.action.TraceProfilToSectionAction;
import org.fudaa.fudaa.crue.planimetry.controller.ExternLayerFactory;
import org.fudaa.fudaa.crue.planimetry.save.ConfigExternIds;
import org.fudaa.fudaa.crue.planimetry.save.LayerTraceProfilsControllerSaver;
import org.fudaa.fudaa.sig.layer.FSigEditor;

import java.io.File;

/**
 * @author Frederic Deniger
 */
public class PlanimetryExternTraceProfilLayer extends AbstractPlanimetryLigneBriseeExternLayer {
    /**
     * constructeur appelé lors du rechargement.
     *
     * @param briseeLayerModel le modele
     * @param fSigEditor       l'editeur
     * @param traceProfilId    id du profil
     */
    public PlanimetryExternTraceProfilLayer(PlanimetryLigneBriseeLayerModel briseeLayerModel, FSigEditor fSigEditor, String traceProfilId) {
        super(briseeLayerModel, fSigEditor);
        setName(traceProfilId);
        setAttributForLabels(PlanimetryExternDrawLayerGroup.DESSINS_LABEL_LIGNES);
        getLayerConfiguration().getLabelConfiguration().setDisplayLabels(false);
    }

    public String getTraceProfilId() {
        return getName();
    }

    @Override
    public PlanimetryAdditionalLayerContrat cloneLayer(FSigEditor editor) {
        return ExternLayerFactory.cloneTraceProfil(this);
    }

    @Override
    public CrueEtudeExternRessource getSaveRessource(File baseDir, String id) {
        LayerTraceProfilsControllerSaver save = new LayerTraceProfilsControllerSaver();
        return save.getSaveRessource(id, getLayerController(), baseDir);
    }

    @Override
    public void installed() {
        SectionProfilAttachedToBrancheAction action = getLayerController().getHelper().getController().getTraceProfilAttachedToBrancheAction();
        setActions(new EbliActionInterface[]{new TraceProfilToSectionAction(this), action, null, ((FSigEditor) getEditor()).getExportAction()});
    }

    @Override
    public String getTypeId() {
        return ConfigExternIds.LAYER_TRACES_PROFILS_SAVE_ID;
    }
}
