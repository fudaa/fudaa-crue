package org.fudaa.fudaa.crue.planimetry.listener;

import org.locationtech.jts.geom.Coordinate;
import java.util.Iterator;
import java.util.List;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISCoordinateSequence;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.fudaa.crue.planimetry.controller.LayerBrancheController;
import org.fudaa.fudaa.crue.planimetry.controller.LayerNodeController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryBrancheModel;

/**
 * Utilise pour déplacer les branhces si un noeud amon/aval est modifié.
 * @author deniger
 */
public class BrancheUpdaterFromNode extends AbstractGeomUpdater {

  private final LayerBrancheController brancheController;

  public BrancheUpdaterFromNode(LayerBrancheController ctx) {
    this.brancheController = ctx;
  }

  @Override
  public void attributeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _action) {
  }

  @Override
  public void attributeValueChangeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _indexObj,
                                         Object _newValue) {
  }

  @Override
  protected PlanimetryController getController() {
    return brancheController.getHelper().getController();
  }

  @Override
  public void updateAll() {
    final LayerNodeController nodeController = brancheController.getHelper().getNodeController();
    final GISZoneCollectionPoint nodeCollection = nodeController.getNodeCollection();
    for (int i = 0; i < nodeCollection.size(); i++) {
      update(i);
    }
  }

  @Override
  protected void updateAfterRemove() {
    PlanimetryBrancheModel model = brancheController.getLayer().modeleDonnees();
    model.updateAfterRemove();
  }

  @Override
  protected void update(int indexOfPoint) {
    final LayerNodeController nodeController = brancheController.getHelper().getNodeController();
    final GISZoneCollectionPoint nodeCollection = nodeController.getNodeCollection();
    GISPoint newPoint = (GISPoint) nodeCollection.getGeometry(indexOfPoint);
    Long nodeUid = nodeController.getUid(indexOfPoint);
    CatEMHNoeud nd = nodeController.getEMH(nodeUid);
    List<CatEMHBranche> branches = nd.getBranches();
    brancheController.setBrancheUidUpdatingFromPoint();
    for (Iterator<CatEMHBranche> it = branches.iterator(); it.hasNext();) {
      final CatEMHBranche branche = it.next();
      Long brancheUid = branche.getUiId();

      int branchePosition = brancheController.getBranchePosition(brancheUid);
      if (branchePosition < 0) {
        continue;
      }
      GISPolyligne brancheLine = brancheController.getBrancheGis(branchePosition);
      boolean isAmont = nodeUid.equals(branche.getNoeudAmont().getUiId());
      if (isAmont) {
        Coordinate amontCoordinate = brancheController.getAmontCoordinate(branchePosition);
        if (brancheController.getHelper().isNotSame(amontCoordinate, newPoint)) {
          final GISCoordinateSequence newSeq = (GISCoordinateSequence) brancheLine.getCoordinateSequence().copy();
          int idx = LayerBrancheController.getAmontCoordinatePosition();
          newSeq.setX(idx, newPoint.getX());
          newSeq.setY(idx, newPoint.getY());
          brancheController.getBrancheCollection().setCoordinateSequence(branchePosition, newSeq, null);
        }
      } else {
        Coordinate avalCoordinate = brancheController.getAvalCoordinate(branchePosition);
        if (brancheController.getHelper().isNotSame(avalCoordinate, newPoint)) {
          final GISCoordinateSequence newSeq = (GISCoordinateSequence) brancheLine.getCoordinateSequence().copy();
          int idx = LayerBrancheController.getAvalCoordinatePosition(newSeq.size());
          newSeq.setX(idx, newPoint.getX());
          newSeq.setY(idx, newPoint.getY());
          brancheController.getBrancheCollection().setCoordinateSequence(branchePosition, newSeq, null);
        }
      }
    }
    brancheController.unsetBrancheUidUpdatingFromPoint();
  }
}
