/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;

/**
 *
 * @author Frederic Deniger
 */
public class TraceProfilToSectionNameTransformer {

  public String getSectionName(String traceProfilName) {
    return CruePrefix.P_SECTION + traceProfilName;
  }

  public String getSectionName(GISZoneCollection traceProfil, int idx) {
    String name = (String) traceProfil.getModel(GGTraceProfilSectionAttributesController.NOM).getObjectValueAt(idx);
    return getSectionName(name);
  }
}
