/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig;

import org.fudaa.ctulu.gis.GISAttributeString;

/**
 *
 * @author Frederic Deniger
 */
public class GGAttributeString extends GISAttributeString {

  public GGAttributeString(String name, String id, boolean atomic) {
    super(name, atomic);
    super.setID(id);
    getEditor().setEditable(false);
  }



  @Override
  public boolean isUserVisible() {
    return true;
  }
}
