package org.fudaa.fudaa.crue.planimetry.configuration.editor;

import org.fudaa.fudaa.crue.common.editor.CodeTranslationEditorSupport;
import org.fudaa.fudaa.crue.planimetry.configuration.TraceConfigurationInfo;

/**
 *
 * @author deniger
 */
public class SectionPositionPropertyEditorSupport extends CodeTranslationEditorSupport {

  public SectionPositionPropertyEditorSupport() {
    super(TraceConfigurationInfo.createCodes());
  }
}
