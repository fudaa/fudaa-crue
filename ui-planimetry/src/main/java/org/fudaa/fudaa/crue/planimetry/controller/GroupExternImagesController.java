/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.controller;

import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternImagesLayerGroup;

/**
 *
 * @author Frederic Deniger
 */
public class GroupExternImagesController extends AbstractGroupExternController<PlanimetryExternImagesLayerGroup> {

  public GroupExternImagesController(PlanimetryController controller, PlanimetryExternImagesLayerGroup groupLayer) {
    super(controller, groupLayer);
  }
  
}
