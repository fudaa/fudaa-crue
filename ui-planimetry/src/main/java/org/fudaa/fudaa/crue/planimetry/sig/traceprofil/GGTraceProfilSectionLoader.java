/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import java.util.Arrays;
import java.util.List;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelFilterAdapter;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.fudaa.sig.FSigGeomSrcData;

/**
 * utiliser pour recharger un fichier persisté de TraceProfil
 *
 * @author Frederic Deniger
 */
public class GGTraceProfilSectionLoader {

  public static GISZoneCollectionLigneBrisee buildLine(FSigGeomSrcData result) {

    GISZoneCollectionLigneBrisee lignes = new GISZoneCollectionLigneBrisee();
    GGTraceProfilSectionAttributesController attributesController = new GGTraceProfilSectionAttributesController();
    GISAttributeInterface[] attributes = attributesController.createSectionAttributes();
    lignes.setAttributes(attributes, null);
    if (result != null) {
      result.preload(attributes, null);
      final List<GISDataModel> polylignes = Arrays.asList(result.getPolylignes());
      if (polylignes != null) {
        for (GISDataModel model : polylignes) {
          lignes.addAll(GISDataModelFilterAdapter.buildAdapter(model, attributes), null, false);
        }
      }
    }
    return lignes;
  }
}
