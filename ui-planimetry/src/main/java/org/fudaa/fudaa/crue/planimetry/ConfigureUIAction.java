package org.fudaa.fudaa.crue.planimetry;

import com.memoire.bu.BuResource;
import org.fudaa.ctulu.action.ActionsInstaller;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.node.AdditionalConfigurationNode;
import org.fudaa.fudaa.crue.planimetry.controller.AbstractGroupExternController;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.List;

/**
 *
 * @author deniger
 */
public class ConfigureUIAction extends EbliActionSimple {

  public static final String CONFIGURE_ACTION_KEY = "CONFIGURE_UI";
  private final PlanimetryVisuPanel panel;

  public ConfigureUIAction(final PlanimetryVisuPanel panel) {
    super(NbBundle.getMessage(ConfigureUIAction.class, "Configure.Action"), BuResource.BU.getToolIcon("configurer"),
            CONFIGURE_ACTION_KEY);
    this.panel = panel;
  }

  @Override
  public void actionPerformed(final ActionEvent e) {
    final ConfigurationUI ui = new ConfigurationUI();
    final VisuConfiguration initVisuConfiguration = panel.getPlanimetryController().getVisuConfiguration();
    final VisuConfiguration cloned = initVisuConfiguration.copy();
    final List<AbstractGroupExternController> subGroups = panel.getPlanimetryController().getGroupExternController().getSubGroups();
    ui.setVisuConfiguration(cloned, panel.getCalqueActif(), subGroups);
    final String apply = NbBundle.getMessage(ConfigureUIAction.class, "ConfigurationUI.Apply.Name");
    final Object[] options = new Object[]{NotifyDescriptor.OK_OPTION, apply, NotifyDescriptor.CANCEL_OPTION};
    final DialogDescriptor dialogDescriptor = new DialogDescriptor(ui, (String) getValue(NAME), true, options,
            NotifyDescriptor.OK_OPTION,
            DialogDescriptor.DEFAULT_ALIGN,
            HelpCtx.DEFAULT_HELP,
            null);    
    SysdocUrlBuilder.installDialogHelpCtx(dialogDescriptor, "bdlConfigurerAffichage",null,false);
    dialogDescriptor.setClosingOptions(new Object[]{NotifyDescriptor.OK_OPTION, NotifyDescriptor.CANCEL_OPTION});
    dialogDescriptor.setButtonListener(actionEvent -> {
      ui.stopEdition();
      final Object value = dialogDescriptor.getValue();
      final boolean isApply = apply.equals(value);
      if (NotifyDescriptor.OK_OPTION.equals(value) || isApply) {
        VisuConfiguration toUse = cloned;
        final List<AdditionalConfigurationNode> additionalNode = ui.getAdditionalNode();
        if (additionalNode != null) {
          for (final AdditionalConfigurationNode additionalConfigurationNode : additionalNode) {
            additionalConfigurationNode.applyModification();
          }
        }
        if (isApply) {
          toUse = toUse.copy();
        }
        panel.getPlanimetryController().modify(toUse);
      }
    });
    final Dialog dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
    ActionsInstaller.install((JDialog) dialog);
    dialog.pack();
    dialog.setLocationRelativeTo(panel.getCtuluUI().getParentComponent());
    dialog.setVisible(true);
  }
}
