package org.fudaa.fudaa.crue.planimetry.configuration;

/**
 * @author deniger
 */
public class DefaultConfiguration {
  public static final String PROP_TRANSPARENCE = "transparenceAlpha";
  int transparenceAlpha = 0;

  public DefaultConfiguration() {

  }

  public DefaultConfiguration(DefaultConfiguration init) {
    if (init != null) {
      transparenceAlpha = init.transparenceAlpha;
    }
  }

  public int getTransparenceAlpha() {
    return transparenceAlpha;
  }

  public int getTransparenceAlphaForLayer() {
    return 255 - transparenceAlpha;
  }

  public void setTransparenceAlpha(int transparenceAlpha) {
    this.transparenceAlpha = transparenceAlpha;
  }


  public DefaultConfiguration copy() {
    return new DefaultConfiguration(this);
  }
}
