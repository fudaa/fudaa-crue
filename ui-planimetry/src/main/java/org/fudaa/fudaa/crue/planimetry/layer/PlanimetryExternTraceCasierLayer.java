/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.layer;

import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessource;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.crue.planimetry.action.TraceCasierToCasierAction;
import org.fudaa.fudaa.crue.planimetry.controller.ExternLayerFactory;
import org.fudaa.fudaa.crue.planimetry.save.ConfigExternIds;
import org.fudaa.fudaa.crue.planimetry.save.LayerTraceCasiersControllerSaver;
import org.fudaa.fudaa.crue.planimetry.sig.tracecasier.GGTraceCasierAttributesController;
import org.fudaa.fudaa.sig.layer.FSigEditor;

import java.io.File;

/**
 * @author Frederic Deniger
 */
public class PlanimetryExternTraceCasierLayer extends AbstractPlanimetryLigneBriseeExternLayer {
    /**
     * constructeur appelé lors du rechargement.
     *
     * @param _modele       le modele
     * @param _editor       l'editeur
     * @param traceProfilId l'id du traceprofil
     */
    public PlanimetryExternTraceCasierLayer(PlanimetryLigneBriseeLayerModel _modele, FSigEditor _editor, String traceProfilId) {
        super(_modele, _editor);
        setName(traceProfilId);
        setAttributForLabels(GGTraceCasierAttributesController.NOM);
        getLayerConfiguration().getLabelConfiguration().setDisplayLabels(false);
    }

    public String getTraceProfilId() {
        return getName();
    }

    @Override
    public PlanimetryAdditionalLayerContrat cloneLayer(FSigEditor editor) {
        return ExternLayerFactory.cloneTraceCasier(this);
    }

    @Override
    public CrueEtudeExternRessource getSaveRessource(File baseDir, String id) {
        LayerTraceCasiersControllerSaver save = new LayerTraceCasiersControllerSaver();
        return save.getSaveRessource(id, getLayerController(), baseDir);
    }

    @Override
    public void installed() {
        setActions(new EbliActionInterface[]{new TraceCasierToCasierAction(this), null, ((FSigEditor) getEditor()).getExportAction()});
    }

    @Override
    public String getTypeId() {
        return ConfigExternIds.LAYER_TRACES_CASIERS_SAVE_ID;
    }
}
