package org.fudaa.fudaa.crue.planimetry.save;

import java.io.File;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper;
import org.fudaa.fudaa.crue.planimetry.controller.ExternLayerFactory;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.layer.AbstractPlanimetryLigneBriseeExternLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternTraceCasierLayer;
import org.fudaa.fudaa.crue.planimetry.sig.tracecasier.GGTraceCasierLoader;
import org.fudaa.fudaa.sig.FSigGeomSrcData;

/**
 * Pour etre pris compte doit être ajoute dans la liste AdditionalLayerLoaderProcess
 *
 * @author deniger
 */
public class LayerTraceCasiersControllerSaver extends AbstractLayerTraceControllerSaver {

  public LayerTraceCasiersControllerSaver() {
    super(PlanimetryController.NAME_GC_EXTERN_TRACES_CASIERS, ConfigExternIds.LAYER_TRACES_CASIERS_SAVE_ID, "TraceCasiers");
  }

  @Override
  protected AbstractPlanimetryLigneBriseeExternLayer createLayer(FSigGeomSrcData result, File file,
          Pair<FSigGeomSrcData, SigFormatHelper.EnumFormat> pair) {
    GISZoneCollectionLigneBrisee lignes = GGTraceCasierLoader.buildLine(result);
    PlanimetryExternTraceCasierLayer layer = ExternLayerFactory.createLayerTraceCasier(lignes, file,
            pair.second,
            null);
    return layer;
  }
}
