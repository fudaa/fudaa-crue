package org.fudaa.fudaa.crue.planimetry.layer;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.calque.edition.ZModelePointEditable;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.crue.planimetry.controller.LayerPointExternController;

/**
 * Un modele que tous les modeles de calques éditables doivent utilsées. Permet de gérer les modifications, en faisant une copie des données avant
 * toutes modifications via l'UI
 *
 * @author deniger
 */
public class PlanimetryPointLayerModel<C extends LayerController> extends ZModelePointEditable {

  private final C layerController;

  public PlanimetryPointLayerModel(GISZoneCollectionPoint _zone, final C layerModelController) {
    super(_zone);
    this.layerController = layerModelController;
  }

  @Override
  public void modificationWillBeDone() {
    getLayerController().changeWillBeDone();
  }

  public C getLayerController() {
    return layerController;
  }

  @Override
  public boolean copyGlobal(CtuluListSelectionInterface _selection, double _dx, double _dy, CtuluCommandContainer _cmd) {
    layerController.changeWillBeDone();
    final boolean done = super.copyGlobal(_selection, _dx, _dy, null);
    layerController.changeDone();
    return done;
  }

  @Override
  public void addPoint(GrPoint _p, ZEditionAttributesDataI _data, CtuluCommandContainer _cmd) {
    layerController.changeWillBeDone();
    super.addPoint(_p, _data, null);
    layerController.changeDone();
  }

  @Override
  public boolean setPoint(int _idx, double _newX, double _newY, CtuluCommandContainer _cmd) {
    layerController.changeWillBeDone();
    boolean done = super.setPoint(_idx, _newX, _newY, null);
    layerController.changeDone();
    return done;
  }

  @Override
  public boolean removePoint(int[] _idx, CtuluCommandContainer _cmd) {
    layerController.changeWillBeDone();
    final boolean done = super.removePoint(_idx, null);
    layerController.changeDone();
    return done;
  }

  public PlanimetryPointLayerModel cloneModel(LayerPointExternController res) {
    return new PlanimetryPointLayerModel((GISZoneCollectionPoint) pts_.clone(), res);
  }
}
