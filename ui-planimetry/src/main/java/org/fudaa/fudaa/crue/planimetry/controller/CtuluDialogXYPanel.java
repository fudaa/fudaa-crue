package org.fudaa.fudaa.crue.planimetry.controller;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class CtuluDialogXYPanel extends CtuluDialogPanel implements KeyListener {

  final BuTextField xMin;
  final BuTextField yMin;

  public CtuluDialogXYPanel() {
    super(true);
    setLayout(new BuGridLayout(2, 5, 5));
    xMin = addLabelDoubleText(org.openide.util.NbBundle.getMessage(CtuluDialogXYPanel.class,
                                                                   "RebuildLayout.MinX.Text"));
    yMin = addLabelDoubleText(org.openide.util.NbBundle.getMessage(CtuluDialogXYPanel.class,
                                                                   "RebuildLayout.MinY.Text"));
    updateError();
    xMin.addKeyListener(this);
    yMin.addKeyListener(this);
  }

  @Override
  public void keyPressed(KeyEvent e) {
    updateError();
  }

  @Override
  public void keyReleased(KeyEvent e) {
    updateError();
  }

  @Override
  public void keyTyped(KeyEvent e) {
    updateError();
  }

  private void updateError() {
    if (xMin == null || yMin == null) {
      setErrorText(NbBundle.getMessage(CtuluDialogXYPanel.class, "XYSaisie.Error.Text"));
    } else {
      setErrorText(null);
    }
  }

  public Double getXmin() {
    return (Double) xMin.getValue();
  }

  public Double getYmin() {
    return (Double) yMin.getValue();
  }

  @Override
  public boolean isDataValid() {
    return xMin.getValue() != null && yMin.getValue() != null;
  }
}
