/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.tracecasier;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.gis.*;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Frederic Deniger
 */
public class TraceCasierToCasierPreloadProcessor {
  private final EMHScenario scenario;
  final PlanimetryControllerHelper helper;

  public TraceCasierToCasierPreloadProcessor(EMHScenario scenario, final PlanimetryControllerHelper helper) {
    this.scenario = scenario;
    this.helper = helper;
  }

  public TraceCasierSousModeleContent preload(GISZoneCollection traceCasiers, CtuluListSelection selection, String defaultSousModele) {
    TraceCasierSousModeleContent content = new TraceCasierSousModeleContent();
    int nb = traceCasiers.getNumGeometries();
    GISAttributeModelObjectInterface model = (GISAttributeModelObjectInterface) traceCasiers.getModel(GGTraceCasierAttributesController.NOM);
    GISAttributeModelObjectInterface modelXt = (GISAttributeModelObjectInterface) traceCasiers.getModel(GGTraceCasierAttributesController.ARRAY_XT);
    TraceCasierToNoeudNameTransformer transformer = new TraceCasierToNoeudNameTransformer();
    List<EMH> allSimpleEMH = scenario.getIdRegistry().getAllSimpleEMH();
    Map<String, EMH> toMapOfId = TransformerHelper.toMapOfId(allSimpleEMH);
    for (int i = 0; i < nb; i++) {
      if (selection != null && !selection.isSelected(i)) {
        continue;
      }
      String casierName = (String) model.getObjectValueAt(i);
      if (casierName == null) {
        continue;
      }
      content.addCasierPosition(casierName, i);
      final String noeudName = transformer.getNoeudName(casierName);
      String noeudId = noeudName.toUpperCase();
      CatEMHNoeud noeud = (CatEMHNoeud) toMapOfId.get(noeudId);
      CatEMHCasier casier = null;
      if (noeud != null) {
        casier = noeud.getCasier();
      }
      TraceCasierSousModele tp = new TraceCasierSousModele();
      tp.setAddOrModifiy(true);
      tp.casierName = casierName;
      tp.noeudName = noeudName;
      tp.setNoeudExists(noeud != null);
      String xts = (String) modelXt.getObjectValueAt(i);
      tp.setContainProfil(StringUtils.isNotBlank(xts));
      tp.setCasierExists(casier != null);
      if (casier != null) {
        tp.sousModeleName = casier.getParent().getNom();
      } else if (noeud != null) {
        tp.sousModeleName = noeud.getParents().get(0).getNom();
      } else {
        tp.sousModeleName = defaultSousModele;
      }
      if (noeud != null) {
        int nodePosition = helper.getNodeController().getNodePosition(noeud.getUiId());
        GISPoint noeudGis = helper.getNodeController().getNoeudGis(nodePosition);
        LineString line = (LineString) traceCasiers.getGeometry(i);
        LinearRing linearRing = GISGeometryFactory.INSTANCE.transformToLinearRing(line);
        boolean inside = GISLib.isInside(new IndexedPointInAreaLocator(linearRing), noeudGis.getCoordinate());
        if (!inside) {
          inside = linearRing.distance(noeudGis) < 1e-3;
        }
        tp.setNoeudInFutureCasier(linearRing.contains(noeudGis) || inside);
      }
      content.traceCasierSousModeles.add(tp);
    }
    Collections.sort(content.traceCasierSousModeles);
    return content;
  }
}
