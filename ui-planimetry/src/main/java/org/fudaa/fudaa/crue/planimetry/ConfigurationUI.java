package org.fudaa.fudaa.crue.planimetry;

import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.fudaa.crue.common.node.NodeChildrenHelper;
import org.fudaa.fudaa.crue.common.view.DefaultConfigurationPanel;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.node.AdditionalConfigurationNode;
import org.fudaa.fudaa.crue.planimetry.configuration.node.BrancheConfigurationNode;
import org.fudaa.fudaa.crue.planimetry.configuration.node.CasierConfigurationNode;
import org.fudaa.fudaa.crue.planimetry.configuration.node.CondLimitConfigurationNode;
import org.fudaa.fudaa.crue.planimetry.configuration.node.NodeConfigurationNode;
import org.fudaa.fudaa.crue.planimetry.configuration.node.SectionConfigurationNode;
import org.fudaa.fudaa.crue.planimetry.configuration.node.TraceConfigurationNode;
import org.fudaa.fudaa.crue.planimetry.configuration.node.VisuConfigurationNode;
import org.fudaa.fudaa.crue.planimetry.controller.AbstractGroupExternController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

/**
 *
 * @author deniger
 */
public class ConfigurationUI extends DefaultConfigurationPanel {

  public ConfigurationUI() {
  }
  List<AdditionalConfigurationNode> additionalNode;

  public List<AdditionalConfigurationNode> getAdditionalNode() {
    return additionalNode;
  }

  public void setVisuConfiguration(VisuConfiguration configuration, BCalque selectedLayer,
          List<AbstractGroupExternController> subGroups) {
    Children.Array children = new Children.Array();
    final SectionConfigurationNode sectionConfigurationNode = new SectionConfigurationNode(configuration.getSectionConfiguration());
    final TraceConfigurationNode traceConfigurationNode = new TraceConfigurationNode(configuration.getTraceConfiguration());
    final NodeConfigurationNode nodeConfigurationNode = new NodeConfigurationNode(configuration.getNodeConfiguration());
    final BrancheConfigurationNode brancheConfigurationNode = new BrancheConfigurationNode(configuration.getBrancheConfiguration());
    final CasierConfigurationNode casierConfigurationNode = new CasierConfigurationNode(configuration.getCasierConfiguration());
    final CondLimitConfigurationNode condLimiteConfigurationNode = new CondLimitConfigurationNode(configuration.getcondLimiteConfiguration());
    List<Node> nodes = new ArrayList<>();
    nodes.add(new VisuConfigurationNode(configuration));
    nodes.add(condLimiteConfigurationNode);
    nodes.add(sectionConfigurationNode);
    nodes.add(traceConfigurationNode);
    nodes.add(nodeConfigurationNode);
    nodes.add(brancheConfigurationNode);
    nodes.add(casierConfigurationNode);
    Node selectedNode = null;
    List<AbstractNode> subGroupNodes = null;
    if (CollectionUtils.isNotEmpty(subGroups)) {
      subGroupNodes = new ArrayList<>();
      additionalNode = new ArrayList<>();
      for (AbstractGroupExternController subGroup : subGroups) {
        ArrayList<AdditionalConfigurationNode> groupNodes = new ArrayList<>();
        final List<PlanimetryAdditionalLayerContrat> additionnalLayers = subGroup.getAdditionnalLayers();
        for (PlanimetryAdditionalLayerContrat object : additionnalLayers) {
          AdditionalConfigurationNode addedNode = object.createConfigurationNode();
          additionalNode.add(addedNode);
          groupNodes.add(addedNode);
          if (selectedLayer == object) {
            selectedNode = addedNode;
          }
        }
        Children createChildren = NodeChildrenHelper.createChildren(groupNodes);
        AbstractNode subGroupNode = new AbstractNode(createChildren);
        subGroupNode.setName(subGroup.getGroupLayer().getName());
        subGroupNode.setDisplayName(subGroup.getGroupLayer().getTitle());
        nodes.add(subGroupNode);
      }

    }
    children.add(nodes.toArray(new Node[0]));

    if (selectedLayer != null && selectedNode == null) {
      String name = selectedLayer.getName();
      if (PlanimetryController.NAME_CQ_BRANCHE.equals(name)) {
        selectedNode = brancheConfigurationNode;
      }
      if (PlanimetryController.NAME_CQ_CASIER.equals(name)) {
        selectedNode = casierConfigurationNode;
      }
      if (PlanimetryController.NAME_CQ_NODE.equals(name)) {
        selectedNode = nodeConfigurationNode;
      }
      if (PlanimetryController.NAME_CQ_SECTION.equals(name)) {
        selectedNode = sectionConfigurationNode;
      }
      if (PlanimetryController.NAME_CQ_TRACE.equals(name)) {
        selectedNode = traceConfigurationNode;
      }

    }

    getExplorerManager().setRootContext(new AbstractNode(children));
    try {
      if (subGroupNodes != null) {
        for (AbstractNode abstractNode : subGroupNodes) {
          listView.expandNode(abstractNode);
        }
      }
      if (selectedNode != null) {
        getExplorerManager().setSelectedNodes(new Node[]{selectedNode});
      }
    } catch (PropertyVetoException propertyVetoException) {
      Exceptions.printStackTrace(propertyVetoException);
    }
  }
}
