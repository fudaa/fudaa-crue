/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.configuration;

import java.text.NumberFormat;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPermItem;
import org.fudaa.dodico.crue.metier.emh.CalcTransItem;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;
import org.fudaa.dodico.crue.metier.emh.DonCLimMWithManoeuvreRegulData;

/**
 * Permet de donner un label à partir d'une DonCLimM
 *
 * @author Frederic Deniger
 */
public class CondLimitConfigurationLabelBuilder {

  public String getLabelFor(DonCLimM donCLimM, CrueConfigMetier ccm, boolean longText) {
    if (donCLimM == null) {
      return null;
    }
    if(donCLimM instanceof DonCLimMWithManoeuvreRegulData){
      return ((DonCLimMWithManoeuvreRegulData) donCLimM).getDesc();
    }
    if (donCLimM.getCalculParent().isPermanent()) {
      CalcPseudoPermItem item = (CalcPseudoPermItem) donCLimM;
      if (item.containValue()) {
        final double permValue = item.getValue();
        return getFormattedValue(ccm, item.getCcmVariableName(), permValue, longText);
      }

    } else if (donCLimM instanceof CalcTransItem) {
      CalcTransItem item = (CalcTransItem) donCLimM;
      if (item.getLoi() != null) {
        return item.getLoi().getNom();
      }
    }
    return donCLimM.getTypei18n();
  }

  public String getFormattedValue(CrueConfigMetier ccm, String variableName, final double permValue, boolean longText) {
    NumberFormat formatter = ccm.getFormatter(variableName, DecimalFormatEpsilonEnum.PRESENTATION);
    final String value = formatter == null ? Double.toString(permValue) : formatter.format(permValue);
    if (longText) {
      ItemVariable property = ccm.getProperty(variableName);
      return property.getDisplayNom() + "= " + value + property.getNature().getUniteSuffixe();
    }
    return value;
  }
}
