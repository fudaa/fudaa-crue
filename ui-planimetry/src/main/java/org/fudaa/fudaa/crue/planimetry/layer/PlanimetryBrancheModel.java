package org.fudaa.fudaa.crue.planimetry.layer;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import gnu.trove.TIntArrayList;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISCoordinateSequence;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.EditionCreateEMH;
import org.fudaa.dodico.crue.edition.EditionDelete;
import org.fudaa.dodico.crue.edition.EditionDelete.RemoveBilan;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.ebli.calque.ZCalqueSelectionInteractionAbstract;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.planimetry.configuration.BrancheConfigurationExtra;
import org.fudaa.fudaa.crue.planimetry.controller.LayerBrancheController;
import org.fudaa.fudaa.crue.planimetry.controller.LayerNodeController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class PlanimetryBrancheModel extends PlanimetryLigneBriseeLayerModel<LayerBrancheController> {

  BrancheConfigurationExtra brancheConfigurationExtra;

  public PlanimetryBrancheModel(GISZoneCollectionLigneBrisee _zone, LayerBrancheController modelController) {
    super(_zone, modelController);
  }

  public BrancheConfigurationExtra getBrancheConfigurationExtra() {
    return brancheConfigurationExtra;
  }

  public void setBrancheConfigurationExtra(BrancheConfigurationExtra brancheConfigurationExtra) {
    this.brancheConfigurationExtra = brancheConfigurationExtra;
  }

  @Override
  public boolean addGeometry(GrPolygone _p, CtuluCommandContainer _cmd, CtuluUI _ui, ZEditionAttributesDataI _d) {
    return false;
  }

  @Override
  public boolean addGeometry(Geometry _p, CtuluCommandContainer _cmd, CtuluUI _ui, ZEditionAttributesDataI _d) {
    return false;
  }

  public void setGeometry(int i, GISPolyligne line) {
    super.getLayerController().changeWillBeDone();
    geometries_.setGeometry(i, line, null);
    super.getLayerController().changeDone();
  }

  @Override
  public boolean isGeometryVisible(int idx) {
    CatEMHBranche branche = getLayerController().getEMHFromPositionInModel(idx);
    PlanimetryControllerHelper helper = getLayerController().getHelper();
    return helper.isVisible(branche);
  }

  @Override
  public boolean removeLigneBrisee(int[] _idx, CtuluCommandContainer _cmd) {
    return removeBranches(_idx, false);
  }

  public boolean removeLigneBriseeCascade(int[] _idx, CtuluCommandContainer _cmd) {
    return removeBranches(_idx, true);
  }

  public boolean removeBranches(int[] _idx, final boolean cascade) {
    PlanimetryControllerHelper helper = super.getLayerController().getHelper();
    if (!helper.canEMHBeAddedAndDisplayError()) {
      return false;
    }
    helper.scenarioEditionStarted();
    List<EMH> branchesToRemove = new ArrayList<>();
    for (int i = 0; i < _idx.length; i++) {
      branchesToRemove.add(getLayerController().getEMHFromPositionInModel(_idx[i]));
    }
    final EMHScenario scenario = helper.getPlanimetryGisModelContainer().getScenario();
    //a utiliser
    RemoveBilan deleted = EditionDelete.delete(branchesToRemove, scenario, cascade);
    helper.displayBilan(deleted);
    return updateAfterRemove();
  }

  public boolean updateAfterRemove() {
    PlanimetryControllerHelper helper = super.getLayerController().getHelper();
    final EMHScenario scenario = helper.getPlanimetryGisModelContainer().getScenario();
    TIntArrayList toRemove = new TIntArrayList();
    GISAttributeModel uidModel = helper.getPlanimetryGisModelContainer().getUidModel(geometries_);
    int nb = uidModel.getSize();
    for (int i = 0; i < nb; i++) {
      Long uid = (Long) uidModel.getObjectValueAt(i);
      if (!scenario.getIdRegistry().containEMH(uid)) {
        toRemove.add(i);
      }
    }
    if (!toRemove.isEmpty()) {
      return super.removeLigneBrisee(toRemove.toNativeArray(), null);

    }
    return false;
  }

  public int findPoint(Coordinate coordinate, double distance) {
    final LayerNodeController nodeController = getLayerController().getHelper().getNodeController();
    GISZoneCollectionPoint nodeCollection = nodeController.getNodeCollection();
    int nb = nodeCollection.getNumGeometries();
    for (int i = 0; i < nb; i++) {
      GISPoint noeudCoordinate = nodeCollection.get(i);
      if (CtuluLibGeometrie.getDistance(noeudCoordinate.getX(), noeudCoordinate.getY(), coordinate.x, coordinate.y) <= distance) {
        return i;
      }
    }
    return -1;
  }

  @Override
  public boolean addGeometry(GrPolyligne _p, CtuluCommandContainer _cmd, CtuluUI _ui, ZEditionAttributesDataI _d) {
    if (_p.nombre() < 2) {
      return false;
    }
    PlanimetryControllerHelper helper = super.getLayerController().getHelper();
    if (!helper.canEMHBeAddedAndDisplayError()) {
      return false;
    }
    EnumBrancheType brancheType = helper.getCreationEMH().getBrancheType();
    if (brancheType == null) {
      return false;
    }

    GISPolyligne polyligne = _p.toGIS();
    double length = polyligne.getLength();
    CrueConfigMetier crueConfigMetier = helper.getController().getCrueConfigMetier();
    final ItemVariable property = crueConfigMetier.getProperty("xp");
    double minValue = property.getValidator().getRangeValidate().getMin().doubleValue();
    if (length < minValue) {
      DialogHelper.showError(NbBundle.getMessage(PlanimetryBrancheModel.class, "AddBrancheMinLongueurNotRespected.error",
              property.getFormatter(DecimalFormatEpsilonEnum.COMPARISON).format(minValue)));
      return false;
    }
    //la distance en pixel pour etre cohérent avec la sélection on utilise ce qui est utilisé pour la sélection
    double distance = ZCalqueSelectionInteractionAbstract.DEFAULT_TOLERANCE_PIXEL;
    //la distance en réelle:
    distance = GrMorphisme.convertDistanceXY(getLayerController().getLayer().getVersReel(), distance);
    Coordinate amont = polyligne.getCoordinateN(LayerBrancheController.getAmontCoordinatePosition());
    int noeudAmontIdx = findPoint(amont, distance);
    Coordinate aval = polyligne.getCoordinateN(LayerBrancheController.getAvalCoordinatePosition(polyligne.getNumPoints()));
    int noeudAvalIdx = findPoint(aval, distance);
    if (noeudAmontIdx >= 0 && noeudAvalIdx >= 0 && noeudAmontIdx == noeudAvalIdx) {
      DialogHelper.showError(NbBundle.getMessage(PlanimetryBrancheModel.class, "AddBrancheNotValidSameNoeuds.error"));
      return false;
    }
//une modification va être effectuée. A faire maintenant avant de récupérer des pointeurs vers des noeuds car 
    //le scenario va être cloné ( si pas deja fait).
    getLayerController().changeWillBeDone();
    helper.scenarioEditionStarted();
    final LayerNodeController nodeController = helper.getController().getNodeController();

    CatEMHNoeud noeudAmont = noeudAmontIdx >= 0 ? (CatEMHNoeud) nodeController.getEMHFromPositionInModel(noeudAmontIdx) : null;
    CatEMHNoeud noeudAval = noeudAvalIdx >= 0 ? (CatEMHNoeud) nodeController.getEMHFromPositionInModel(noeudAvalIdx) : null;

    if (noeudAmontIdx >= 0) {
      updateNoeudAmontPosition(nodeController, noeudAmontIdx, polyligne);
    }
    if (noeudAvalIdx >= 0) {
      updateNoeudAvalPosition(nodeController, noeudAvalIdx, polyligne);
    }

    CatEMHBranche createBranche = new EditionCreateEMH(getLayerController().getDefaultValues()).createBranche(helper.getDefaultSousModele(),
            brancheType, noeudAmont,
            noeudAval, crueConfigMetier, polyligne.getLength());
    if (createBranche != null) {
      StringBuilder info = new StringBuilder();
      info.append("<html><body>");
      if (noeudAmont == null) {
        noeudAmont = createBranche.getNoeudAmont();
        PlanimetryNodeLayerModel nodeModel = nodeController.getLayer().modeleDonnees();
        nodeModel.addNoeud(noeudAmont, new GISPoint(amont));
        info.append(NbBundle.getMessage(PlanimetryBrancheModel.class, "Info.CreatedBranche.CreateNoeudAmont", noeudAmont.getNom()));
      } else {
        info.append(NbBundle.getMessage(PlanimetryBrancheModel.class, "Info.CreatedBranche.WithNoeudAmont", noeudAmont.getNom()));
      }
      info.append("<br>");
      if (noeudAval == null) {
        noeudAval = createBranche.getNoeudAval();
        PlanimetryNodeLayerModel nodeModel = nodeController.getLayer().modeleDonnees();
        nodeModel.addNoeud(noeudAval, new GISPoint(aval));
        info.append(NbBundle.getMessage(PlanimetryBrancheModel.class, "Info.CreatedBranche.CreateNoeudAval", noeudAval.getNom()));
      } else {
        info.append(NbBundle.getMessage(PlanimetryBrancheModel.class, "Info.CreatedBranche.WithNoeudAval", noeudAval.getNom()));
      }
      Object[] data = helper.getPlanimetryGisModelContainer().buildEdgeData(createBranche);
      getGeometries().addGeometry(polyligne, data, null);
      DialogHelper.showNotifyOperationTermine(NbBundle.getMessage(PlanimetryBrancheModel.class, "Info.CreatedBranche", createBranche.getNom()), info.
              toString());
    }
    return true;
  }

  public void updateNoeudAvalPosition(final LayerNodeController nodeController, int noeudAvalIdx, GISPolyligne polyligne) {
    GISZoneCollectionPoint nodeCollection = nodeController.getNodeCollection();
    GISPoint realAvalPoint = nodeCollection.get(noeudAvalIdx);
    GISCoordinateSequence coordinateSequence = (GISCoordinateSequence) polyligne.getCoordinateSequence();
    int pos = LayerBrancheController.getAvalCoordinatePosition(coordinateSequence.size());
    coordinateSequence.setX(pos, realAvalPoint.getX());
    coordinateSequence.setY(pos, realAvalPoint.getY());
  }

  public void updateNoeudAmontPosition(final LayerNodeController nodeController, int noeudAmontIdx, GISPolyligne polyligne) {
    GISZoneCollectionPoint nodeCollection = nodeController.getNodeCollection();
    GISPoint realAmontPoint = nodeCollection.get(noeudAmontIdx);
    GISCoordinateSequence coordinateSequence = (GISCoordinateSequence) polyligne.getCoordinateSequence();
    int pos = LayerBrancheController.getAmontCoordinatePosition();
    coordinateSequence.setX(pos, realAmontPoint.getX());
    coordinateSequence.setY(pos, realAmontPoint.getY());
  }
}
