package org.fudaa.fudaa.crue.planimetry.layer;

import java.io.File;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessource;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper;
import org.fudaa.fudaa.crue.planimetry.controller.ExternLayerFactory;
import org.fudaa.fudaa.crue.planimetry.save.ConfigExternIds;
import org.fudaa.fudaa.crue.planimetry.save.LayerLigneBriseeUnmodifiableControllerSaver;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 *
 * @author deniger
 */
public class PlanimetryLigneBriseeExternUnmodifiableLayer extends AbstractPlanimetryLigneBriseeExternLayer {

  final File file;
  private final SigFormatHelper.EnumFormat fmt;

  public PlanimetryLigneBriseeExternUnmodifiableLayer(PlanimetryLigneBriseeLayerModel _modele, FSigEditor _editor, File file,
          SigFormatHelper.EnumFormat fmt) {
    super(_modele, _editor);
    this.file = file;
    this.fmt = fmt;
  }

  public File getFile() {
    return file;
  }

  @Override
  public PlanimetryAdditionalLayerContrat cloneLayer(FSigEditor editor) {
    return ExternLayerFactory.cloneLayerUnmodifiable(this);
  }

  public SigFormatHelper.EnumFormat getFmt() {
    return fmt;
  }

  @Override
  public void installed() {
    setActions(new EbliActionInterface[]{((FSigEditor) getEditor()).getImportAction(), ((FSigEditor) getEditor()).getExportAction()});
  }

  @Override
  public boolean isDestructible() {
    return true;
  }

  @Override
  public CrueEtudeExternRessource getSaveRessource(File baseDir, String id) {
    LayerLigneBriseeUnmodifiableControllerSaver saver = new LayerLigneBriseeUnmodifiableControllerSaver();
    return saver.getSaveRessource(id, getLayerController(), baseDir);
  }

  @Override
  public String getTypeId() {
    return ConfigExternIds.LAYER_LINE_UNMODIFIABLE_SAVE_ID;
  }
}
