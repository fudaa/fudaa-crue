/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.services;

import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;

/**
 * Un listener permettant d'écouter les modifications sur les calques additionnels.
 *
 * @author Frederic Deniger
 */
public interface AdditionalLayerSaveStateListener {
    String FROM_REPORT = "FROM_REPORT";
    String FROM_MODELLING = "FROM_MODELLING";

    /**
     * Evenement indiquant que les calques additionnels ( dessins, images) ont été sauvegardés sur disque
     *
     * @param source     la source de l'evenement: soit {@link #FROM_MODELLING} ou soit {@link #FROM_REPORT}
     * @param controller le controller à l'origine de l'evenement
     */
    void additionalLayerSavedBy(String source, PlanimetryController controller);
}
