package org.fudaa.fudaa.crue.planimetry.configuration;


import javax.swing.*;
import java.awt.*;

/**
 * @author deniger
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class VisuConfiguration {
  static final String PROP_MINIMAL_DISTANCE_BEETWEEN_NODE = "minimalDistanceBetweenNode";
  static final String PROP_MINIMAL_DISTANCE_BEETWEEN_PARALLEL_EDGE = "minimalDistanceBeetweenParallelEdges";
  static final String PROP_DEFAULT_CASIER_DIMENSION = "defaultCasierRealDimension";
  static final String PROP_INACTIVE_EMH_VISIBLE = "inactiveEMHVisible";
  static final String PROP_INACTIVE_EMH_COLOR = "inactiveEMHColor";
  /**
   * Attention, il faut maintenir les correspondances entre les noms des propriétés et les constantes: accès par introspection.
   */
  private double minimalDistanceBetweenNode = 200;
  private double minimalDistanceBeetweenParallelEdges = 400;
  private double defaultCasierRealDimension = 4;
  boolean inactiveEMHVisible = true;
  private Color inactiveEMHColor = Color.LIGHT_GRAY;
  final NodeConfiguration nodeConfiguration;
  final BrancheConfiguration brancheConfiguration;
  final SectionConfiguration sectionConfiguration;
  final TraceConfiguration traceConfiguration;
  final CasierConfiguration casierConfiguration;
  final CondLimitConfiguration condLimiteConfiguration;
  public static final Font DEFAULT_FONT = createDefaultFont();

  private static Font createDefaultFont() {
    // Theme.font
    return new Font("SansSerif", Font.PLAIN, 12);
  }

  public double getDefaultCasierRealDimension() {
    return defaultCasierRealDimension;
  }


  public void setMinimalDistanceBeetweenParallelEdges(final double minimalDistanceBeetweenParallelEdges) {
    if (VisuConfigurationInfo.isValide(PROP_MINIMAL_DISTANCE_BEETWEEN_PARALLEL_EDGE, minimalDistanceBeetweenParallelEdges)) {
      this.minimalDistanceBeetweenParallelEdges = minimalDistanceBeetweenParallelEdges;
    }
  }

  public Color getInactiveEMHColor() {
    return inactiveEMHColor;
  }

  public void setInactiveEMHColor(final Color inactiveEMHColor) {
    this.inactiveEMHColor = inactiveEMHColor;
  }

  public void setMinimalDistanceBetweenNode(final double minimalDistanceBetweenNode) {
    if (VisuConfigurationInfo.isValide(PROP_MINIMAL_DISTANCE_BEETWEEN_NODE, minimalDistanceBetweenNode)) {
      this.minimalDistanceBetweenNode = minimalDistanceBetweenNode;
    }
  }

  public void setDefaultCasierRealDimension(final double defaultCasierRealDimension) {
    if (VisuConfigurationInfo.isValide(PROP_DEFAULT_CASIER_DIMENSION, defaultCasierRealDimension)) {
      this.defaultCasierRealDimension = defaultCasierRealDimension;
    }
  }

  public VisuConfiguration() {
    this(null);
  }

  public VisuConfiguration(VisuConfiguration from) {
    if (from != null) {
      nodeConfiguration = new NodeConfiguration(from.nodeConfiguration, this);
      brancheConfiguration = new BrancheConfiguration(from.brancheConfiguration, this);
      sectionConfiguration = new SectionConfiguration(from.sectionConfiguration, this);
      casierConfiguration = new CasierConfiguration(from.casierConfiguration, this);
      traceConfiguration = new TraceConfiguration(from.traceConfiguration, this);
      condLimiteConfiguration = new CondLimitConfiguration(from.condLimiteConfiguration, this);
      this.minimalDistanceBetweenNode = from.minimalDistanceBetweenNode;
      this.minimalDistanceBeetweenParallelEdges = from.minimalDistanceBeetweenParallelEdges;
      this.defaultCasierRealDimension = from.defaultCasierRealDimension;
      this.inactiveEMHVisible = from.inactiveEMHVisible;
      this.inactiveEMHColor = from.inactiveEMHColor;
    } else {
      nodeConfiguration = new NodeConfiguration(this);
      brancheConfiguration = new BrancheConfiguration(this);
      sectionConfiguration = new SectionConfiguration(this);
      casierConfiguration = new CasierConfiguration(this);
      traceConfiguration = new TraceConfiguration(this);
      condLimiteConfiguration = new CondLimitConfiguration(this);
    }

  }

  public VisuConfiguration copy() {
    return new VisuConfiguration(this);
  }


  public TraceConfiguration getTraceConfiguration() {
    return traceConfiguration;
  }

  public NodeConfiguration getNodeConfiguration() {
    return nodeConfiguration;
  }

  public void setInactiveEMHVisible(final boolean inactiveEMHVisible) {
    this.inactiveEMHVisible = inactiveEMHVisible;
  }

  public boolean isInactiveEMHVisible() {
    return inactiveEMHVisible;
  }

  /**
   * To get a font of the same family than an other but a little differente in size or style.
   *
   * @return a derivated font
   */
  public static Font deriveFont(final String _key, final int _style, final int _deltasize) {
    return getFont(_style, _deltasize, UIManager.getFont(_key + ".font"));
  }

  private static Font getFont(int style, int deltasize, Font initFont) {
    Font ft = initFont;
    if (ft == null) {
      ft = DEFAULT_FONT;
    }
    final int sz = Math.max(ft.getSize() + deltasize, 10);
    // UIResource
    return new Font(ft.getFamily(), style, sz);
  }

  public static Font deriveFont(final Font _ft, final int _style, final int _deltasize) {
    return getFont(_style, _deltasize, _ft);
  }

  public CasierConfiguration getCasierConfiguration() {
    return casierConfiguration;
  }

  public double getMinimalDistanceBeetweenParallelEdges() {
    return minimalDistanceBeetweenParallelEdges;
  }

  public double getMinimalDistanceBetweenNode() {
    return minimalDistanceBetweenNode;
  }

  public SectionConfiguration getSectionConfiguration() {
    return sectionConfiguration;
  }

  public BrancheConfiguration getBrancheConfiguration() {
    return brancheConfiguration;
  }

  public CondLimitConfiguration getcondLimiteConfiguration() {
    return condLimiteConfiguration;
  }
}
