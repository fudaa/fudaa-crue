/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.gis.GISAttributeModelObjectInterface;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;

/**
 *
 * @author Frederic Deniger
 */
public class TraceProfilToSectionPreloadProcessor {

  private final EMHScenario scenario;

  public TraceProfilToSectionPreloadProcessor(EMHScenario scenario) {
    this.scenario = scenario;
  }

  public TraceProfilSousModeleContent preload(GISZoneCollection traceProfils, CtuluListSelection selection, String defaultSousModele) {
    TraceProfilSousModeleContent content = new TraceProfilSousModeleContent();
    int nb = traceProfils.getNumGeometries();
    GISAttributeModelObjectInterface model = (GISAttributeModelObjectInterface) traceProfils.getModel(GGTraceProfilSectionAttributesController.NOM);
    TraceProfilToSectionNameTransformer transformer = new TraceProfilToSectionNameTransformer();
    List<EMH> allSimpleEMH = scenario.getIdRegistry().getAllSimpleEMH();
    Map<String, EMH> toMapOfId = TransformerHelper.toMapOfId(allSimpleEMH);
    for (int i = 0; i < nb; i++) {
      if (selection != null && !selection.isSelected(i)) {
        continue;
      }
      String traceName = (String) model.getObjectValueAt(i);
      if (traceName == null) {
        continue;
      }
      content.addTracePosition(traceName, i);
      final String sectionName = transformer.getSectionName(traceName);
      String sectionId = sectionName.toUpperCase();
      CatEMHSection section = (CatEMHSection) toMapOfId.get(sectionId);

      TraceProfilSousModele tp = new TraceProfilSousModele();
      tp.setAddOrModifiy(true);
      tp.traceProfilName = traceName;
      tp.setSousModeleName(defaultSousModele);
      if (section != null) {
        tp.setSectionExists(true);
        tp.sectionName = section.getNom();
        tp.setSousModeleName(section.getParent().getNom());
      } else {
        tp.sectionName = sectionName;
      }
      content.traceProfilSousModeles.add(tp);
    }
    Collections.sort(content.traceProfilSousModeles);
    return content;
  }
}
