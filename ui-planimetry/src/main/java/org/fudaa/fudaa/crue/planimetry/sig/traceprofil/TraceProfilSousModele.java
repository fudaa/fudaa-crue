/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class TraceProfilSousModele implements Comparable<TraceProfilSousModele> {

  public static final String PROP_TRACE_PROFIL_NAME = "traceProfilName";
  public static final String PROP_SECTION_NAME = "sectionName";
  public static final String PROP_SOUS_MODELE = "sousModeleName";
  public static final String PROP_ACCEPT_MODIFICATION = "addOrModifiy";
  public static final String PROP_ACTION_BILAN = "actionName";
  public static final String I18N_ACTION_BILAN = "actionBilan.property";
  public static final String I18N_TRACE_PROFIL_NAME = "traceProfilName.property";
  public static final String I18N_SECTION_NAME = "sectionName.property";
  public static final String I18N_TARGET_SOUS_MODELE = "targetSousModele.property";
  public static final String I18N_ACCEPT_MODIFICATION = "acceptModification.property";
  /**
   * true si l'utilisateur accepte la modification
   */
  boolean addOrModifiy;
  /**
   * true si la section cible existe déjà
   */
  boolean sectionExists;
  /**
   * le nom du traceProfil
   */
  String traceProfilName;
  /**
   * le nom de la section cible
   */
  String sectionName;
  /**
   * le sous-modele cible. Si la section existe déjà, ne peut être modifiée
   */
  String sousModeleName;

  @PropertyDesc(i18n = I18N_ACTION_BILAN, i18nBundleBaseName = "org/fudaa/fudaa/crue/planimetry/sig/traceprofil/Bundle.properties", readOnly = true)
  public String getActionName() {
    if (addOrModifiy) {
      if (sectionExists) {
        return NbBundle.getMessage(TraceProfilSousModele.class, "createSection.replace");
      } else if (sousModeleName != null) {
        return NbBundle.getMessage(TraceProfilSousModele.class, "createSection.create");
      }

    }
    return NbBundle.getMessage(TraceProfilSousModele.class, "createSection.doNothing");
  }

  Double getTraceNameAsDouble() {
    try {
      return Double.parseDouble(traceProfilName);
    } catch (NumberFormatException numberFormatException) {
    }
    return null;
  }

  @Override
  public int compareTo(TraceProfilSousModele o) {
    if (o == null) {
      return 1;
    }
    Double thisAsDouble = getTraceNameAsDouble();
    Double oAsDouble = o.getTraceNameAsDouble();
    if (thisAsDouble != null && oAsDouble != null) {
      return SafeComparator.compareComparable(thisAsDouble, oAsDouble);
    }
    return SafeComparator.compareString(traceProfilName, o.traceProfilName);
  }

  @PropertyDesc(i18n = I18N_ACCEPT_MODIFICATION, i18nBundleBaseName = "org/fudaa/fudaa/crue/planimetry/sig/traceprofil/Bundle.properties")
  public boolean isAddOrModifiy() {
    return addOrModifiy;
  }

  public void setAddOrModifiy(boolean addOrModifiy) {
    this.addOrModifiy = addOrModifiy;
  }

  public boolean isSectionExists() {
    return sectionExists;
  }

  public void setSectionExists(boolean sectionExists) {
    this.sectionExists = sectionExists;
  }

  @PropertyDesc(i18n = I18N_TRACE_PROFIL_NAME, i18nBundleBaseName = "org/fudaa/fudaa/crue/planimetry/sig/traceprofil/Bundle.properties",
          readOnly = true)
  public String getTraceProfilName() {
    return traceProfilName;
  }

  @PropertyDesc(i18n = I18N_SECTION_NAME, i18nBundleBaseName = "org/fudaa/fudaa/crue/planimetry/sig/traceprofil/Bundle.properties", readOnly = true)
  public String getSectionName() {
    return sectionName;
  }

  @PropertyDesc(i18n = I18N_TARGET_SOUS_MODELE, i18nBundleBaseName = "org/fudaa/fudaa/crue/planimetry/sig/traceprofil/Bundle.properties")
  public String getSousModeleName() {
    return sousModeleName;
  }

  public void setSousModeleName(String sousModeleName) {
    this.sousModeleName = sousModeleName;
  }
}
