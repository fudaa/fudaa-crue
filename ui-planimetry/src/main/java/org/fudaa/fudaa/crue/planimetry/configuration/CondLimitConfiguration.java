/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.configuration;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * @author Frederic Deniger
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class CondLimitConfiguration extends DefaultConfiguration {

  public final static String PROP_DISTANCE = "iconDistance";
  private final VisuConfiguration parent;
  final Map<String, Integer> iconPositionByType = new HashMap<>();
  final Map<String, LabelConfiguration> labelConfigurationByType = new HashMap<>();
  final Map<Class, EnumTypeIcon> enumByDclm = EnumTypeIcon.getEnumByDclm();
  private int iconDistance = 5;
  private final CondLimitConfigurationLabelBuilder labelBuilder = new CondLimitConfigurationLabelBuilder();

  public CondLimitConfiguration(VisuConfiguration parent) {
    this.parent = parent;
    EnumTypeIcon[] values = EnumTypeIcon.values();
    for (EnumTypeIcon value : values) {
      iconPositionByType.put(value.getConfigId(), value.getDefaultPosition());
      final LabelConfiguration labelConfiguration = new LabelConfiguration();
      labelConfiguration.setDisplayLabels(false);
      labelConfigurationByType.put(value.getConfigId(), labelConfiguration);
    }
  }

  public CondLimitConfiguration(CondLimitConfiguration from, final VisuConfiguration parent) {
    super(from);
    if (from != null) {
      this.iconPositionByType.putAll(from.iconPositionByType);
      from.labelConfigurationByType.forEach((key, value) -> labelConfigurationByType.put(key, value.copy()));
      this.enumByDclm.putAll(from.enumByDclm);
      this.iconDistance = from.iconDistance;
    }
    this.parent = parent;
  }

  @Override
  public CondLimitConfiguration copy() {
    return new CondLimitConfiguration(this,parent);
  }

  public Map<String, Integer> getIconPosition() {
    return Collections.unmodifiableMap(iconPositionByType);
  }

  public Map<String, LabelConfiguration> getLabelPosition() {
    return Collections.unmodifiableMap(labelConfigurationByType);
  }

  LabelConfiguration getLabelConfiguration(EnumTypeIcon enumTypeIcon) {
    return labelConfigurationByType.get(enumTypeIcon.getConfigId());
  }

  public LabelConfiguration getLabelPosition(Class<? extends DonCLimM> dclmClass) {
    EnumTypeIcon type = enumByDclm.get(dclmClass);
    return labelConfigurationByType.get(type.getConfigId());
  }

  public int getIconDistance() {
    return iconDistance;
  }

  public void setIconDistance(int iconDistance) {
    this.iconDistance = iconDistance;
  }

  public int getIconPosition(Class dclmClass) {
    EnumTypeIcon type = enumByDclm.get(dclmClass);
    return iconPositionByType.get(type.getConfigId());
  }

  public int getIconPosition(String name) {
    return iconPositionByType.get(name);
  }

  public int setPosition(String name, Integer position) {
    return iconPositionByType.put(name, position);
  }

  public VisuConfiguration getParent() {
    return parent;
  }

  public boolean isInactiveEMHVisible() {
    return parent.isInactiveEMHVisible();
  }

  public String getLabelFor(DonCLimM donCLimM, CrueConfigMetier ccm, boolean longText, CondLimitConfigurationExtra extra) {
    if (extra != null) {
      return extra.getLabelFor(donCLimM, ccm, longText);
    }
    return labelBuilder.getLabelFor(donCLimM, ccm, longText);
  }
}
