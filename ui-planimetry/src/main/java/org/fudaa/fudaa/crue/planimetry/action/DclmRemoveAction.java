/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.action;

import java.awt.event.ActionEvent;
import org.fudaa.dodico.crue.edition.EditionUpdateCalc;
import org.fudaa.dodico.crue.metier.emh.DonCLimMCommonItem;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;

/**
 *
 * @author Frederic Deniger
 */
public class DclmRemoveAction extends EbliActionSimple {

  private final Long emhUid;
  private final Class dclmClass;
  private final PlanimetryControllerHelper helper;

  public DclmRemoveAction(PlanimetryControllerHelper controller, EMH emh, DonCLimMCommonItem dclm) {
    super(dclm.getShortName(), null, "REMOVE_DCLM");
    this.emhUid = emh.getUiId();
    this.dclmClass = dclm.getClass();
    this.helper = controller;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    helper.scenarioEditionStarted();//attention des données vont être clonées !
    EMHScenario scenario = helper.getController().getScenario();
    EMH emh = scenario.getIdRegistry().getEmh(emhUid);
    new EditionUpdateCalc().removeActiveDclm(emh, dclmClass);
    helper.getcondLimiteController().getLayer().repaint(0);
  }
}
