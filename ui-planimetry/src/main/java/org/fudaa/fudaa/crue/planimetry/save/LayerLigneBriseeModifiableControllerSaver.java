package org.fudaa.fudaa.crue.planimetry.save;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessource;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessourceInfos;
import org.fudaa.dodico.crue.io.conf.Option;
import org.fudaa.fudaa.crue.common.config.ConfigSaverHelper;
import org.fudaa.fudaa.crue.planimetry.configuration.LigneBriseeConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.controller.ExternLayerFactory;
import org.fudaa.fudaa.crue.planimetry.controller.LayerLigneBriseeExternController;
import org.fudaa.fudaa.crue.planimetry.layer.AbstractPlanimetryLigneBriseeExternLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternDrawLayerGroup;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLigneBriseeExternModifiableLayer;
import org.fudaa.fudaa.crue.planimetry.sig.SigLoaderHelper;
import org.fudaa.fudaa.sig.FSigGeomSrcData;
import org.openide.nodes.Sheet;

/**
 * Pour etre pris compte doit être ajoute dans la liste AdditionalLayerLoaderProcess
 *
 * @author deniger
 */
public class LayerLigneBriseeModifiableControllerSaver implements AdditionalLayerLoaderProcess.Loader {

  public CrueEtudeExternRessource getSaveRessource(String id, LayerLigneBriseeExternController layerController, File baseDir) {
    LayerControllerSaverHelper helper = new LayerControllerSaverHelper();
    CrueEtudeExternRessourceInfos ressource = new CrueEtudeExternRessourceInfos();
    final AbstractPlanimetryLigneBriseeExternLayer layer = layerController.getLayer();
    ressource.setNom(layer.getTitle());
    ressource.setId(id);
    ressource.setLayerId(layer.getName());
    ressource.setType(ConfigExternIds.LAYER_LINE_MODIFIABLE_SAVE_ID);
    helper.addVisibleProperty(ressource, layerController.getLayer());
    LinkedHashMap<String, String> transform = ConfigSaverHelper.getProperties(LigneBriseeConfigurationInfo.createSet(
            layer.getLayerConfiguration()));
    for (Map.Entry<String, String> entry : transform.entrySet()) {
      ressource.getOptions().add(new Option(entry.getKey(), entry.getValue()));
    }

    return new CrueEtudeExternRessource(ressource, new GISAdditionalFileSaver(ressource, layer.modeleDonnees().getGeomData()));
  }

  @Override
  public String getType() {
    return ConfigExternIds.LAYER_LINE_MODIFIABLE_SAVE_ID;
  }

  protected GISZoneCollectionLigneBrisee loadCollection(CrueEtudeExternRessourceInfos ressource, CtuluLog log, File baseDir) {
    FSigGeomSrcData result = new LayerControllerSaverModifiableHelper().loadCollection(ressource, log, baseDir);
    if (result == null) {
      return null;
    }
    return SigLoaderHelper.buildLine(result, PlanimetryExternDrawLayerGroup.getDefaultAttributesLignesModifiable());
  }

  @Override
  public PlanimetryAdditionalLayerContrat load(CrueEtudeExternRessourceInfos ressource, CtuluLog log, File baseDir) {

    GISZoneCollectionLigneBrisee lignes = loadCollection(ressource, log, baseDir);
    if (lignes == null) {
      return null;
    }
    PlanimetryLigneBriseeExternModifiableLayer layer = ExternLayerFactory.createLayerLignesModifiable(lignes,
            null);
    layer.setTitle(ressource.getNom());
    layer.setName(ressource.getLayerId());

    LayerControllerSaverHelper helper = new LayerControllerSaverHelper();
    Map<String, String> optionsBykey = helper.toMap(ressource);
    helper.addVisibleProperty(ressource, layer);
    List<Sheet.Set> propertySet = LigneBriseeConfigurationInfo.createSet(layer.getLayerConfiguration());
    LayerControllerSaverHelper.restoreVisuProperties(optionsBykey, layer, propertySet, log);
    return layer;
  }
}
