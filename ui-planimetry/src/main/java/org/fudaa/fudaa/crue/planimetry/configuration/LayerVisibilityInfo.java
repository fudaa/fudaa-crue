package org.fudaa.fudaa.crue.planimetry.configuration;

import org.fudaa.dodico.crue.projet.planimetry.LayerVisibility;
import org.fudaa.fudaa.crue.common.config.ConfigurationInfoHelper;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport.Reflection;
import org.openide.util.Exceptions;

/**
 *
 * @author deniger
 */
public class LayerVisibilityInfo {

  private static final  String VISIBILITY_GROUP_HYDRAULIC = "groupHydraulique";
  private static final  String VISIBILITY_GROUP_OTHER = "groupeAutre";
  private static final  String VISIBILITY_GROUP_DESSIN = "groupeDessin";
  private static final  String VISIBILITY_GROUP_SIG_FILE = "groupeFichierSIG";
  private static final  String VISIBILITY_GROUP_IMAGE_FILE = "groupeImage";
  private static final  String VISIBILITY_GROUP_TRACE_PROFILS = "groupeTraceProfils";
  private static final  String VISIBILITY_GROUP_TRACE_PROFILS_CASIERS = "groupeProfilsCasier";

  private LayerVisibilityInfo(){}

  public static Node.Property createGrTraceProfilsProperty(LayerVisibility in) {
    try {
      Reflection create = ConfigurationInfoHelper.create("traceProfilsVisible", Boolean.TYPE, in, LayerVisibilityInfo.class);
      create.setName(VISIBILITY_GROUP_TRACE_PROFILS + ".visible");
      return create;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return null;
  }

  public static Node.Property createGrTraceProfilsPropertyCasiers(LayerVisibility in) {
    try {
      Reflection create = ConfigurationInfoHelper.create("traceCasiersVisible", Boolean.TYPE, in, LayerVisibilityInfo.class);
      create.setName(VISIBILITY_GROUP_TRACE_PROFILS_CASIERS + ".visible");
      return create;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return null;
  }

  public static Node.Property createGrHydraulicProperty(LayerVisibility in) {
    try {
      Reflection create = ConfigurationInfoHelper.create("grHydraulicVisible", Boolean.TYPE, in, LayerVisibilityInfo.class);
      create.setName(VISIBILITY_GROUP_HYDRAULIC + ".visible");
      return create;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return null;
  }

  public static Node.Property createGrOtherProperty(LayerVisibility in) {
    try {
      Reflection create = ConfigurationInfoHelper.create("grOtherObjectVisible", Boolean.TYPE, in, LayerVisibilityInfo.class);
      create.setName(VISIBILITY_GROUP_OTHER + ".visible");
      return create;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return null;
  }

  public static Node.Property createGrDessinProperty(LayerVisibility in) {
    try {
      Reflection create = ConfigurationInfoHelper.create("grDrawVisible", Boolean.TYPE, in, LayerVisibilityInfo.class);
      create.setName(VISIBILITY_GROUP_DESSIN + ".visible");
      return create;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return null;
  }

  public static Node.Property createGrSIGFIleProperty(LayerVisibility in) {
    try {
      Reflection create = ConfigurationInfoHelper.create("grSIGFileVisible", Boolean.TYPE, in, LayerVisibilityInfo.class);
      create.setName(VISIBILITY_GROUP_SIG_FILE + ".visible");
      return create;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return null;
  }

  public static Node.Property createGrImageProperty(LayerVisibility in) {
    try {
      Reflection create = ConfigurationInfoHelper.create("grImagesVisible", Boolean.TYPE, in, LayerVisibilityInfo.class);
      create.setName(VISIBILITY_GROUP_IMAGE_FILE + ".visible");
      return create;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return null;
  }

  public static Node.Property createTraceProperty(LayerVisibility in) {
    try {
      Reflection create = ConfigurationInfoHelper.create("traceVisible", Boolean.TYPE, in, LayerVisibilityInfo.class);
      create.setName(TraceConfigurationInfo.PREFIX_TRACES + ".visible");
      return create;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return null;
  }

  public static Node.Property createSectionProperty(LayerVisibility in) {
    try {
      Reflection create = ConfigurationInfoHelper.create("sectionVisible", Boolean.TYPE, in, LayerVisibilityInfo.class);
      create.setName(SectionConfigurationInfo.PREFIX_SECTIONS + ".visible");
      return create;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return null;
  }

  public static Node.Property createNodeProperty(LayerVisibility in) {
    try {
      Reflection create = ConfigurationInfoHelper.create("nodeVisible", Boolean.TYPE, in, LayerVisibilityInfo.class);
      create.setName(NodeConfigurationInfo.PREFIX_NODES + ".visible");
      return create;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return null;
  }

  public static Node.Property createBrancheProperty(LayerVisibility in) {
    try {
      Reflection create = ConfigurationInfoHelper.create("brancheVisible", Boolean.TYPE, in, LayerVisibilityInfo.class);
      create.setName(BrancheConfigurationInfo.PREFIX_BRANCHES + ".visible");
      return create;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return null;
  }

  public static Node.Property createCasierProperty(LayerVisibility in) {
    try {
      Reflection create = ConfigurationInfoHelper.create("casierVisible", Boolean.TYPE, in, LayerVisibilityInfo.class);
      create.setName(CasierConfigurationInfo.PREFIX_CASIERS + ".visible");
      return create;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return null;
  }

  public static Node.Property createcondLimiteProperty(LayerVisibility in) {
    try {
      Reflection create = ConfigurationInfoHelper.create("condLimitVisible", Boolean.TYPE, in, LayerVisibilityInfo.class);
      create.setName(CondLimitConfigurationInfo.PREFIX_CONDLIMITE + ".visible");
      return create;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return null;
  }
}
