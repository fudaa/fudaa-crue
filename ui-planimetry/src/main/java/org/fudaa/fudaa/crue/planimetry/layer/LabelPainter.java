package org.fudaa.fudaa.crue.planimetry.layer;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceBox;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.fudaa.crue.planimetry.configuration.LabelConfiguration;

/**
 *
 * @author deniger
 */
public class LabelPainter {

  final TraceBox traceBox = new TraceBox();
  final TraceLigne traceBoxLigne = new TraceLigne(TraceLigne.INVISIBLE, 0, Color.BLACK);

  protected void paintLabels(final Graphics2D g2d, GrPoint pEcran, String txt, LabelConfiguration configuration, int alpha) {
    Font old = g2d.getFont();
    final Font font = configuration.getLabelFont();
    g2d.setFont(font);
    int delta = configuration.getLabelDistance();
    int position = configuration.getLabelAlignment();
    Point pt = new Point((int) pEcran.x_, (int) pEcran.y_);
    traceBox.configureFor(position, delta, pt);
    traceBox.setColorText(EbliLib.getAlphaColor(configuration.getLabelColor(), alpha));
    traceBox.setDrawBox(false);
    traceBox.setDrawFond(false);
    traceBox.setTraceLigne(traceBoxLigne);
    traceBox.paintBox(g2d, pt.x, pt.y, txt);
    g2d.setFont(old);
  }
}
