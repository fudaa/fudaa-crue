package org.fudaa.fudaa.crue.planimetry.controller;

import org.fudaa.fudaa.crue.planimetry.configuration.DefaultConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.layer.LayerController;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryImageRasterLayer;

/**
 * @author deniger
 */
public class LayerImageController implements LayerController {

  PlanimetryImageRasterLayer layer;

  public PlanimetryImageRasterLayer getLayer() {
    return layer;
  }

  public void setLayer(PlanimetryImageRasterLayer layer) {
    this.layer = layer;
  }

  @Override
  public void cancel() {
    if (cloned != null) {
      layer.setLayerConfigurationNoUndo(cloned);
      cloned = null;
    }
  }

  @Override
  public void changeDone() {
  }

  DefaultConfiguration cloned;

  @Override
  public void changeWillBeDone() {
    if (cloned == null) {
      DefaultConfiguration layerConfiguration = layer.getLayerConfiguration();
      cloned = layerConfiguration.copy();
    }
  }

  @Override
  public void init(PlanimetryControllerHelper helper) {
  }

  @Override
  public boolean isEditable() {
    return layer.getModelEditable().getGeomData().isGeomModifiable();
  }

  @Override
  public void saveDone() {
    cloned = null;
  }

  @Override
  public void setEditable(boolean editable) {
    layer.getModelEditable().getGeomData().setGeomModifiable(editable);
  }

  @Override
  public void setVisuConfiguration(VisuConfiguration cloned) {
  }
}
