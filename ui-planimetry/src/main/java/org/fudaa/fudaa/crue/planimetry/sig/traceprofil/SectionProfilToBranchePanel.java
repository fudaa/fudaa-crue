/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JPanel;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.view.DefaultOutlineViewEditor;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class SectionProfilToBranchePanel extends DefaultOutlineViewEditor {

  public SectionProfilToBranchePanel(List<SectionProfilToBrancheParametersChange> changes) {
    final List<SectionProfilToBrancheParametersChangeNode> nodes = new ArrayList<>();
    for (SectionProfilToBrancheParametersChange change : changes) {
      nodes.add(new SectionProfilToBrancheParametersChangeNode(change));
    }
    JPanel top = new JPanel(new BorderLayout());
    JPanel selectAllPanel = createSelectAllPanel();
    top.add(selectAllPanel, BorderLayout.EAST);
    add(top, BorderLayout.NORTH);
    Node createNode = NodeHelper.createNode(nodes, "All");
    getExplorerManager().setRootContext(createNode);
  }

  public List<SectionProfilToBrancheParametersChange> getAcceptedModification() {
    List<SectionProfilToBrancheParametersChange> res = new ArrayList<>();
    Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    for (Node node : nodes) {
      SectionProfilToBrancheParametersChange change = node.getLookup().lookup(SectionProfilToBrancheParametersChange.class);
      if (change.isAcceptOperation()) {
        res.add(change);
      }
    }
    return res;
  }

  protected void selectAll(boolean newOne) {
    Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    for (Node node : nodes) {
      try {
        AbstractNodeFirable myNode = (AbstractNodeFirable) node;
        myNode.find(SectionProfilToBrancheParametersChange.PROP_ACCEPT_OPERATION).setValue(newOne);
      } catch (Exception ex) {
        Logger.getLogger(SectionProfilToBranchePanel.class.getName()).log(Level.INFO, "message {0}", ex);
      }
    }
  }

  @Override
  protected void initActions() {
  }

  @Override
  protected void addElement() {
  }

  @Override
  protected void createOutlineView() {
    view = new OutlineView(NbBundle.getMessage(SectionProfilToBranchePanel.class, "section.property"));
    view.getOutline().setRootVisible(false);
    view.setPropertyColumns(
            SectionProfilToBrancheParametersChange.PROP_OLD_BRANCHE_NAME, NbBundle.getMessage(SectionProfilToBrancheParametersChange.class,
            SectionProfilToBrancheParametersChange.I18N_OLD_BRANCHE_NAME),
            SectionProfilToBrancheParametersChange.PROP_NEW_BRANCHE_NAME, NbBundle.getMessage(SectionProfilToBrancheParametersChange.class,
            SectionProfilToBrancheParametersChange.I18N_NEW_BRANCHE_NAME),
            SectionProfilToBrancheParametersChange.PROP_OLD_XP, NbBundle.getMessage(SectionProfilToBrancheParametersChange.class,
            SectionProfilToBrancheParametersChange.I18N_OLD_XP),
            SectionProfilToBrancheParametersChange.PROP_NEW_XP, NbBundle.getMessage(SectionProfilToBrancheParametersChange.class,
            SectionProfilToBrancheParametersChange.I18N_NEW_XP),
            SectionProfilToBrancheParametersChange.PROP_ACCEPT_OPERATION, NbBundle.getMessage(SectionProfilToBrancheParametersChange.class,
            SectionProfilToBrancheParametersChange.I18N_ACCEPT_OPERATION));
  }

  private JPanel createSelectAllPanel() {
    JPanel pnActiveUnactive = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    JButton btActive = new JButton(NbBundle.getMessage(SectionProfilToBranchePanel.class, "AcceptAll.Button"));
    btActive.setToolTipText(NbBundle.getMessage(SectionProfilToBranchePanel.class, "AcceptAll.Tooltip"));
    JButton btUnActive = new JButton(NbBundle.getMessage(SectionProfilToBranchePanel.class, "RefuseAll.Button"));
    btUnActive.setToolTipText(NbBundle.getMessage(SectionProfilToBranchePanel.class, "RefuseAll.Tooltip"));
    btActive.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        selectAll(true);
      }
    });
    btUnActive.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        selectAll(false);
      }
    });

    pnActiveUnactive.add(btActive);
    pnActiveUnactive.add(btUnActive);
    return pnActiveUnactive;
  }
}
