/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig;

/**
 *
 * @author Frederic Deniger
 */
public interface PointContainer {

  double getX();

  double getY();
}
