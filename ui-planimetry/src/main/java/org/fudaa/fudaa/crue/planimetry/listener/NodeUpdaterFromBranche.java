package org.fudaa.fudaa.crue.planimetry.listener;

import org.locationtech.jts.geom.Coordinate;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.fudaa.crue.planimetry.controller.LayerBrancheController;
import org.fudaa.fudaa.crue.planimetry.controller.LayerNodeController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryNodeLayerModel;

/**
 * Utilise pour déplacer les noeuds si la branche est modifiée.
 * @author deniger
 */
public class NodeUpdaterFromBranche extends AbstractGeomUpdater {

  final LayerNodeController nodeController;

  public NodeUpdaterFromBranche(LayerNodeController controller) {
    this.nodeController = controller;
  }

  @Override
  public void attributeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _action) {
  }

  @Override
  public void attributeValueChangeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _indexObj,
                                         Object _newValue) {
  }

  @Override
  protected void updateAfterRemove() {
    PlanimetryNodeLayerModel model = nodeController.getLayer().modeleDonnees();
    model.updateAfterRemove();
  }

  @Override
  protected PlanimetryController getController() {
    return nodeController.getHelper().getController();
  }

  @Override
  protected void updateAll() {
    LayerBrancheController brancheController = nodeController.getHelper().getBrancheController();
    GISZoneCollectionLigneBrisee brancheCollection = brancheController.getBrancheCollection();
    for (int i = 0; i < brancheCollection.getNumGeometries(); i++) {
      update(i);
    }
  }

  @Override
  protected void update(int indexOfBranches) {
    LayerBrancheController brancheController = nodeController.getHelper().getBrancheController();
    if (brancheController.isBrancheUpdatingFromPoint()) {
      return;
    }
    final PlanimetryControllerHelper helper = nodeController.getHelper();
    Long amontUid = brancheController.getAmontNodeUid(indexOfBranches);
    int amontPosition = nodeController.getNodePosition(amontUid);
    GISZoneCollectionPoint nodeCollection = nodeController.getNodeCollection();
    if (amontPosition >= 0) {
      Coordinate newAmontCoordinate = brancheController.getAmontCoordinate(indexOfBranches);
      GISPoint old = nodeCollection.get(amontPosition);
      if (helper.isNotSame(newAmontCoordinate, old)) {
        nodeCollection.set(amontPosition, newAmontCoordinate.x, newAmontCoordinate.y, null);
      }
    }
    Coordinate newAvalCoordinate = brancheController.getAvalCoordinate(indexOfBranches);
    Long avalUid = brancheController.getAvalNodeUid(indexOfBranches);
    int avalPosition = nodeController.getNodePosition(avalUid);
    if (avalPosition >= 0) {
      GISPoint old = nodeCollection.get(avalPosition);
      if (helper.isNotSame(newAvalCoordinate, old)) {
        nodeCollection.set(avalPosition, newAvalCoordinate.x, newAvalCoordinate.y, null);
      }
    }
  }
}
