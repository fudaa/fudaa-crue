/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.tracecasier;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.EditionProfilCreator;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilCasier;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.LitUtile;
import org.fudaa.dodico.crue.metier.emh.PtProfil;
import org.openide.util.NbBundle;

/**
 * Permet de créer des DonPrtGeoProfilCasier à partir des données SIG.
 *
 * @author Frederic Deniger
 */
public class TraceCasierToDonPrtGeoProfilCasierTransformer {

  private final EditionProfilCreator profilCreator;
  private final CrueConfigMetier ccm;
  private final EMHSousModele sousModele;
  private final ItemVariable propXt;
  private final ItemVariable propZ;
  private final ItemVariable propDistance;

  public TraceCasierToDonPrtGeoProfilCasierTransformer(CrueConfigMetier ccm, final EMHSousModele sousModele) {
    profilCreator = new EditionProfilCreator(new UniqueNomFinder(), new CreationDefaultValue());
    this.sousModele = sousModele;
    this.ccm = ccm;
    propXt = ccm.getProperty(CrueConfigMetierConstants.PROP_XT);
    propZ = ccm.getProperty(CrueConfigMetierConstants.PROP_Z);
    propDistance = ccm.getProperty(CrueConfigMetierConstants.PROP_DISTANCE);
  }

  private <T> T getValueAt(GISZoneCollection zone, GISAttributeInterface att, int idxGeo) {
    return (T) zone.getModel(att).getObjectValueAt(idxGeo);
  }

  private List<PtProfil> getPtProfil(String xt, String z) {
    String[] xts = splitString(xt);
    String[] zs = splitString(z);
    List<PtProfil> pt = new ArrayList<>();
    final int size = Math.min(xts.length, zs.length);
    for (int i = 0; i < size; i++) {
      try {
        pt.add(new PtProfil(propXt.getNormalizedValue(Double.parseDouble(xts[i])),
                propZ.getNormalizedValue(Double.parseDouble(zs[i]))));
      } catch (NumberFormatException numberFormatException) {
        Logger.getLogger(TraceCasierToDonPrtGeoProfilCasierTransformer.class.getName()).log(Level.WARNING, "message {0}", numberFormatException);
      }
    }
    return pt;
  }

  public DonPrtGeoProfilCasier getDonPrtGeoProfilCasier(GISZoneCollection traceCasier, int idx) {

    String name = getValueAt(traceCasier, GGTraceCasierAttributesController.NOM, idx);
    String xt = getValueAt(traceCasier, GGTraceCasierAttributesController.ARRAY_XT, idx);
    if (StringUtils.isBlank(xt)) {
      return null;
    }
    String z = getValueAt(traceCasier, GGTraceCasierAttributesController.ARRAY_Z, idx);
    if (StringUtils.isBlank(z)) {
      return null;
    }
    String lits = getValueAt(traceCasier, GGTraceCasierAttributesController.ARRAY_LIMLIT, idx);

    Double distance = getValueAt(traceCasier, GGTraceCasierAttributesController.DISTANCE, idx);
    List<PtProfil> pts = getPtProfil(xt, z);
    if (pts.isEmpty()) {
      return null;
    }
    DonPrtGeoProfilCasier profilCasier = profilCreator.createProfilCasier(ccm, name, sousModele.getParent().getParent(), pts);
    if (distance != null) {
      profilCasier.setDistance(propDistance.getNormalizedValue(distance));
    }
    String[] listAsArray = splitString(lits);
    if (listAsArray.length > 0) {
      int first = -1;
      int second = -1;
      final int length = Math.min(listAsArray.length, pts.size());
      for (int i = 0; i < length; i++) {
        String string = listAsArray[i];
        if (GGTraceCasierAttributesController.isLimLitUtile(string)) {
          if (first < 0) {
            first = i;
          } else {
            second = i;
            break;
          }
        }
      }
      if (first >= 0 && second > first) {
        LitUtile litUtile = profilCasier.getLitUtile();
        litUtile.setLimDeb(pts.get(first));
        litUtile.setLimFin(pts.get(second));
      }

    }
    profilCasier.setCommentaire(NbBundle.getMessage(TraceCasierToDonPrtGeoProfilCasierTransformer.class, "traceCasier.buildFrom", name));
    profilCreator.addEmptyZFkStoIfNeeded(sousModele, ccm);
    return profilCasier;
  }


  protected String[] splitString(String lits) {
    return StringUtils.split(lits, ';');
  }
}
