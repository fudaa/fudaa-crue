package org.fudaa.fudaa.crue.planimetry.layer;

import com.memoire.bu.BuPopupMenu;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.crue.planimetry.action.LayerWithCascadeDeleteAction;
import org.fudaa.fudaa.crue.planimetry.action.PlanimetryDclmController;
import org.fudaa.fudaa.crue.planimetry.configuration.LabelConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.NodeConfiguration;
import org.fudaa.fudaa.crue.planimetry.controller.LayerNodeController;
import org.fudaa.fudaa.crue.planimetry.find.PlanimetryFindEMHLayerAction;
import org.fudaa.fudaa.sig.layer.FSigEditor;

import java.awt.*;

/**
 *
 * Calque pour les noeuds du réseau.
 *
 * @author deniger
 */
public class PlanimetryNodeLayer extends PlanimetryPointLayer<NodeConfiguration>
        implements LayerWithCascadeDeleteAction, PlanimetryLayerWithEMHContrat, CalqueWithDynamicActions {

  public PlanimetryNodeLayer(PlanimetryNodeLayerModel _modele, FSigEditor _editor) {
    super(_modele, _editor);
  }

  LayerNodeController getNoeudController() {
    return (LayerNodeController) (((PlanimetryPointLayerModel) modeleDonnees()).getLayerController());
  }

  @Override
  public PlanimetryNodeLayerModel modeleDonnees() {
    return (PlanimetryNodeLayerModel) super.modeleDonnees();
  }

  @Override
  public EbliFindActionInterface getFinder() {
    return new PlanimetryFindEMHLayerAction(this);
  }

  @Override
  public String editSelected() {
    getActions()[0].actionPerformed(null);
    return null;
  }

  @Override
  public LayerControllerEMH getLayerControllerEMH() {
    return (LayerControllerEMH) getLayerController();
  }

  @Override
  protected boolean clearSelectionIfLayerNonVisible() {
    return false;
  }

  private boolean userVisible = true;

  @Override
  public boolean isUserVisible() {
    return userVisible;
  }

  @Override
  public void setUserVisible(boolean userVisible) {
    if (this.userVisible != userVisible) {
      super.setUserVisible(userVisible);
      this.userVisible = userVisible;
      clearCacheAndRepaint();
    }
  }

  @Override
  public boolean isVisible() {
    return !isSelectionEmpty() || isUserVisible();
  }

  @Override
  public boolean isMovable() {
    return false;
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  @Override
  public void fillPopup(BuPopupMenu popup) {
    if (isEditable() && isOnlyOneObjectSelected()) {
      int idx = getSelectedIndex()[0];
      EMH emh = getLayerControllerEMH().getEMHFromLayerPosition(idx);
      PlanimetryDclmController dclmController = new PlanimetryDclmController(getNoeudController().getHelper());
      dclmController.fillPopupMenu(emh, popup);
    }
  }

  @Override
  public GrBoite getDomaine() {
    GrBoite boite = super.getDomaine();
    return boite;
  }

  @Override
  public void removeCascadeSelectedObjects() {
    if (!isSelectionEmpty()) {
      final boolean r = ((PlanimetryNodeLayerModel) modele_).removePointCascade(getSelectedIndex(), null);
      if (r) {
        clearSelection();
      }
    }
  }

  @Override
  protected boolean isPainted(int idx) {

    if (!isUserVisible()||!modeleDonnees().isGeometryVisible(idx)){
      return false;
    }
    if (getLayerSelection().isSelected(idx)) {
      return true;
    }
    if (!layerConfiguration.isInactiveEMHVisible()) {
      CatEMHNoeud noeud = getNoeudController().getEMHFromPositionInModel(idx);
      return  noeud != null && noeud.getActuallyActive();
    }
    return true;
  }

  @Override
  public boolean canAddForme(int _typeForme) {
    return isEditable() && _typeForme == DeForme.POINT;
  }
  final LabelPainter painter = new LabelPainter();

  @Override
  protected void paintLabels(final Graphics2D g2d, int idx, GrPoint pEcran) {
    LabelConfiguration labelConfiguration = layerConfiguration.getLabelConfiguration();
    if (!labelConfiguration.isDisplayLabels()) {
      return;
    }
    CatEMHNoeud noeud = getNoeudController().getNoeud(idx);
    String nom = layerConfiguration.getDisplayedLabel(noeud, modeleDonnees(), selection_ != null && selection_.isSelected(
            idx));
    painter.paintLabels(g2d, pEcran, nom, labelConfiguration, alpha_);
  }
}
