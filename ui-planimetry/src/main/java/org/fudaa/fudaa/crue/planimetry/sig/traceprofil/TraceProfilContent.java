/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import gnu.trove.TObjectIntHashMap;
import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.config.ccm.DoubleEpsilonComparator;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

/**
 * @author Frederic Deniger
 */
public class TraceProfilContent implements Comparable<TraceProfilContent> {
  final String name;
  private final TreeMap<Double, TraceProfilContentItem> xtSet;
  private List<TraceProfilContentItem> items = new ArrayList<>();

  public TraceProfilContent(String name, final PropertyEpsilon xtEpsilon) {
    this.name = name;
    this.xtSet = new TreeMap<>(new DoubleEpsilonComparator(xtEpsilon));
  }

  /**
   * Transpositions des points
   *
   * @param items les items a transposer
   * @return les items transposés
   */
  static List<TraceProfilContentItem> transpose(List<TraceProfilContentItem> items) {
    List<TraceProfilContentItem> transposed = new ArrayList<>();
    double x0 = items.get(0).getX();
    double delta = 0;
    for (int i = items.size() - 1; i >= 0; i--) {
      TraceProfilContentItem current = items.get(i);
      x0 = x0 + delta;
      if (i > 0) {
        delta = current.x - items.get(i - 1).x;
      }
      current.x = x0;
      transposed.add(current);
    }
    return transposed;
  }

  private int find(int limLit) {
    for (int i = 0; i < items.size(); i++) {
      TraceProfilContentItem traceProfilContentItem = items.get(i);
      if (traceProfilContentItem.containsLit(limLit)) {
        return i;
      }
    }
    return -1;
  }

  List<TraceProfilContentItem> getSortedItems() {
    Collections.sort(items);
    TObjectIntHashMap<String> createMapInverse = LimLitHelper.createMapInverse();
    int first = find(createMapInverse.get(LimLitHelper.MAJD_MIN));
    int second = find(createMapInverse.get(LimLitHelper.MIN_MAJG));
    if (first >= 0 && second >= 0) {
      if (first > second) {
        items = transpose(items);
      }
      final int idRd = createMapInverse.get(LimLitHelper.RD_STOD);
      final int idRg = createMapInverse.get(LimLitHelper.STOG_RG);
      if (find(idRd) < 0) {
        items.get(0).addLimLit(idRd);
      }
      if (find(idRg) < 0) {
        items.get(items.size() - 1).addLimLit(idRg);
      }
    }

    return items;
  }

  /**
   * @param newItem l'item a ajouter
   * @return false si le xt est déjà présent.
   */
  public boolean add(TraceProfilContentItem newItem) {
    TraceProfilContentItem item = xtSet.get(newItem.xt);
    if (item == null) {
      xtSet.put(newItem.xt, newItem);
      items.add(newItem);
      return true;
    } else {
      item.copyLimFrom(newItem);
    }
    return false;
  }

  private Double getAsDouble() {
    try {
      return Double.parseDouble(name);
    } catch (Exception ex) {
      //rien a faire:
    }
    return null;
  }

  @Override
  public int compareTo(TraceProfilContent o) {
    if (o == null) {
      return 1;
    }
    Double asDouble = getAsDouble();
    Double asDoubleO = o.getAsDouble();
    if (asDouble != null && asDoubleO != null) {
      return SafeComparator.compareComparable(asDouble, asDoubleO);
    }
    return SafeComparator.compareString(name, o.name);
  }
}
