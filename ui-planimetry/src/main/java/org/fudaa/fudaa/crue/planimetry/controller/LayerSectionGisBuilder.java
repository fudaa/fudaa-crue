package org.fudaa.fudaa.crue.planimetry.controller;

import gnu.trove.TObjectDoubleHashMap;
import gnu.trove.TObjectDoubleIterator;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.gis.GisAbscCurviligneToCoordinate;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EnumPosSection;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.factory.PredicateFactory;

/**
 *
 * @author deniger
 */
public class LayerSectionGisBuilder {
  
  public static TObjectDoubleHashMap<RelationEMHSectionDansBranche> getRatio(int position, double minRealDistance,
          LayerBrancheController brancheController) {
    TObjectDoubleHashMap<RelationEMHSectionDansBranche> res = new TObjectDoubleHashMap<>();
    if (position < 0) {
      return res;
    }
    CatEMHBranche branche = brancheController.getBranche(position);
    List<RelationEMHSectionDansBranche> listeSections = branche.getListeSections();
    RelationEMHSectionDansBranche amontRelation = (RelationEMHSectionDansBranche) CollectionUtils.find(listeSections,
            new PredicateFactory.PredicateRelationEMHSectionDansBranche(
            EnumPosSection.AMONT));
    RelationEMHSectionDansBranche avalRelation = (RelationEMHSectionDansBranche) CollectionUtils.find(listeSections,
            new PredicateFactory.PredicateRelationEMHSectionDansBranche(
            EnumPosSection.AVAL));
    if (amontRelation == null || avalRelation == null) {
      return res;
    }
    GISPolyligne brancheGis = brancheController.getBrancheGis(position);
    double visuLength = brancheGis.getLength();
    PropertyEpsilon epsilon = brancheController.getCrueConfigMetier().getEpsilon(CrueConfigMetierConstants.PROP_XP);
    final boolean isVisuLengthZero = epsilon.isZero(visuLength);
    if (isVisuLengthZero) {
      return res;
    }
    double avalRatio = 1d;
    double amontRatio = 0d;
    final double length = branche.getLength();
    
    if (visuLength > minRealDistance) {
      amontRatio = minRealDistance / visuLength;
      avalRatio = (visuLength - minRealDistance) / visuLength;
    }
    if (!epsilon.isZero(length)) {
      for (RelationEMHSectionDansBranche relationEMHSectionDansBranche : listeSections) {
        //on ne retraite pas les 2 sections extreme
        if (relationEMHSectionDansBranche.getPos().equals(EnumPosSection.AMONT)) {
          continue;
        }
        if (relationEMHSectionDansBranche.getPos().equals(EnumPosSection.AVAL)) {
          continue;
        }
        double ratio = relationEMHSectionDansBranche.getXp() / length;
        //pour s'assurer que 
        ratio = Math.min(avalRatio, ratio);
        ratio = Math.max(amontRatio, ratio);
        res.put(relationEMHSectionDansBranche, ratio);
      }
    }
    res.put(amontRelation, amontRatio);
    res.put(avalRelation, avalRatio);
    return res;
  }
  private final PlanimetryController controller;
  
  public LayerSectionGisBuilder(PlanimetryController controller) {
    this.controller = controller;
  }
  
  protected GISZoneCollectionPoint rebuild(double offsetForAmonAvalSection) {
    final GISZoneCollectionPoint newSectionCollection = controller.getPlanimetryGisModelContainer().createSectionCollection();
    newSectionCollection.setGeomModifiable(true);
    final LayerBrancheController brancheController = controller.getBrancheController();
    GisAbscCurviligneToCoordinate.Result tmpResult = new GisAbscCurviligneToCoordinate.Result();
    double realDistance = offsetForAmonAvalSection;
    GISZoneCollectionLigneBrisee brancheCollection = brancheController.getBrancheCollection();
    int nb = brancheCollection.getNumGeometries();
    for (int i = 0; i < nb; i++) {
      CatEMHBranche branche = brancheController.getBranche(i);
      GISPolyligne brancheGis = brancheController.getBrancheGis(i);
      TObjectDoubleHashMap<RelationEMHSectionDansBranche> ratiosBySection = getRatio(i, realDistance,
              brancheController);
      for (TObjectDoubleIterator it = ratiosBySection.iterator(); it.hasNext();) {
        it.advance();
        double ratio = it.value();
        RelationEMHSectionDansBranche sectionDansBranche = (RelationEMHSectionDansBranche) it.key();
        GisAbscCurviligneToCoordinate.find(brancheGis, ratio, tmpResult);
        Object[] buildSectionData = controller.getPlanimetryGisModelContainer().buildSectionData(
                sectionDansBranche, branche);
        newSectionCollection.add(tmpResult.getCoordinate().x, tmpResult.getCoordinate().y, 0, Arrays.asList(buildSectionData),
                null);
      }
    }
    newSectionCollection.setGeomModifiable(false);
    return newSectionCollection;
  }
}
