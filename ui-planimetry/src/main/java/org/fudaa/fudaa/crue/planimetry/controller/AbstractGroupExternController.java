/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.controller;

import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.layer.AbstractPlanimetryExternLayerGroup;
import org.fudaa.fudaa.crue.planimetry.layer.LayerController;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 *
 * @author Frederic Deniger
 */
public abstract class AbstractGroupExternController<T extends AbstractPlanimetryExternLayerGroup> extends Observable implements LayerController,
        PropertyChangeListener {

  public static final String PROPERTY_LAYER_ADDED = "LayerAdded";
  public static final String PROPERTY_LAYER_REMOVED = "LayerRemoved";
  CalqueAddRemoveListener calquesListener;
  boolean editable;
  protected final PlanimetryController controller;
  private final T groupLayer;
  private boolean initializing;

  public AbstractGroupExternController(PlanimetryController controller, T groupLayer) {
    this.controller = controller;
    this.groupLayer = groupLayer;
    createCalqueListener();
  }

  public final List<String> getUsedNames() {
    BCalque[] calques = getGroupLayer().getCalques();
    List<String> names = new ArrayList<>();
    for (BCalque bCalque : calques) {
      names.add(bCalque.getName());
    }
    return names;
  }

  private boolean nameUsed(String name) {
    if (name == null) {
      return true;
    }
    final BCalque[] cqs = groupLayer.getCalques();
    for (BCalque bCalque : cqs) {
      if (name.equals(bCalque.getName())) {
        return true;
      }
    }
    return false;
  }

  public BCalque addAdditionalLayer(PlanimetryAdditionalLayerContrat layer) {
    BCalque calque = (BCalque) layer;
    //nom unique
    if (layer.getName() != null) {
      if (nameUsed(layer.getName())) {
        final String uniqueName = BGroupeCalque.findUniqueChildName(groupLayer, layer.getName());
        calque.setName(uniqueName);
      }
    } else {
      String newName = layer.getTypeId() + "_" + System.currentTimeMillis();
      if (nameUsed(newName)) {
        newName = BGroupeCalque.findUniqueChildName(groupLayer, layer.getTypeId());
      }
      calque.setName(newName);
    }
    if (calque instanceof ZCalquePointEditable) {
      ((ZCalquePointEditable) calque).setEditor(controller.getVisuPanel().getEditor());
    } else if (calque instanceof ZCalqueLigneBriseeEditable) {
      ((ZCalqueLigneBriseeEditable) calque).setEditor(controller.getVisuPanel().getEditor());
    }
    layer.getLayerController().init(getHelper());
    layer.installed();
    groupLayer.enDernier(calque);
    return calque;
  }

  public void addAdditionnalLayer(List<PlanimetryAdditionalLayerContrat> additionnalLayer) {
    for (PlanimetryAdditionalLayerContrat layer : additionnalLayer) {
      addAdditionalLayer(layer);
    }

  }

  /**
   *
   * @return les layers dans l'ordre d'affichage.
   */
  public List<PlanimetryAdditionalLayerContrat> getAdditionnalLayers() {
    BCalque[] calques = getGroupLayer().getCalques();

    List<PlanimetryAdditionalLayerContrat> res = new ArrayList<>();
    for (BCalque bCalque : calques) {
      res.add((PlanimetryAdditionalLayerContrat) bCalque);
    }
    return res;
  }

  @Override
  public void cancel() {
  }

  @Override
  public void changeDone() {
  }

  @Override
  public void changeWillBeDone() {
  }

  public T getGroupLayer() {
    return groupLayer;
  }

  @Override
  public boolean isEditable() {
    return editable;
  }

  @Override
  public void saveDone() {
  }

  protected final void createCalqueListener() {
    if (calquesListener == null) {
      calquesListener = new CalqueAddRemoveListener();
      getGroupLayer().addContainerListener(calquesListener);
    }
    getGroupLayer().addPropertyChangeListener(this);
  }

  public void setArbreEditable(boolean editable) {
    this.editable = editable;
    getGroupLayer().setArbreEditable(editable);
    List<PlanimetryAdditionalLayerContrat> additionnalLayers = getAdditionnalLayers();
    for (PlanimetryAdditionalLayerContrat planimetryAdditionalLayerContrat : additionnalLayers) {
      planimetryAdditionalLayerContrat.getLayerController().setEditable(editable);
    }
  }

  public PlanimetryControllerHelper getHelper() {
    return controller.getHelper();
  }

  @Override
  public void init(PlanimetryControllerHelper helper) {
    List<PlanimetryAdditionalLayerContrat> additionnalLayers = getAdditionnalLayers();
    for (PlanimetryAdditionalLayerContrat planimetryAdditionalLayerContrat : additionnalLayers) {
      planimetryAdditionalLayerContrat.getLayerController().init(helper);
    }
  }

  //ne fait rien car les actions de ce groupe sont controllees par l'arbre.
  @Override
  public void setEditable(boolean editable) {
  }

  @Override
  public void setVisuConfiguration(VisuConfiguration cloned) {
  }

  /**
   * initialise les calques externes depuis ceux de other.
   *
   * @param other l'objet a copier
   */
  void resetFrom(AbstractGroupExternController other) {
    initializing = true;
    getGroupLayer().removeAll();
    List<PlanimetryAdditionalLayerContrat> additionnalLayers = other.getAdditionnalLayers();
    FSigEditor editor = controller.getVisuPanel().getEditor();
    for (PlanimetryAdditionalLayerContrat planimetryAdditionalLayerContrat : additionnalLayers) {
      addAdditionalLayer(planimetryAdditionalLayerContrat.cloneLayer(editor));
    }
    try {
    } finally {
      initializing = false;
    }

  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (initializing) {
      return;
    }
    AbstractGroupExternController.this.setChanged();
    AbstractGroupExternController.this.notifyObservers(evt.getPropertyName());
  }

  private class CalqueAddRemoveListener implements ContainerListener {

    public CalqueAddRemoveListener() {
    }

    @Override
    public void componentAdded(ContainerEvent e) {

      final BCalque c = (BCalque) e.getChild();
      c.addPropertyChangeListener(AbstractGroupExternController.this);
      controller.calqueAdded(c);
      if (initializing) {
        return;
      }
      AbstractGroupExternController.this.setChanged();
      AbstractGroupExternController.this.notifyObservers(PROPERTY_LAYER_ADDED);
    }

    @Override
    public void componentRemoved(ContainerEvent e) {

      final BCalque c = (BCalque) e.getChild();
      c.removePropertyChangeListener(AbstractGroupExternController.this);
      controller.calqueRemoved(c);
      if (initializing) {
        return;
      }
      AbstractGroupExternController.this.setChanged();
      AbstractGroupExternController.this.notifyObservers(PROPERTY_LAYER_REMOVED);
    }
  }
}
