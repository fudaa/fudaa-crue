package org.fudaa.fudaa.crue.planimetry.configuration.node;

import org.fudaa.fudaa.crue.planimetry.configuration.LigneBriseeConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.LigneBriseeConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLigneBriseeLayer;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.lookup.Lookups;

import java.util.List;

/**
 *
 * @author deniger
 */
public class LigneBriseeConfigurationNode extends AdditionalConfigurationNode {

  public LigneBriseeConfigurationNode(LigneBriseeConfiguration ligneBriseeConfiguration, PlanimetryLigneBriseeLayer layer) {
    super(Children.LEAF, Lookups.fixed(ligneBriseeConfiguration, layer));
    setDisplayName(layer.getTitle());
  }

  PlanimetryLigneBriseeLayer getLayer() {
    return getLookup().lookup(PlanimetryLigneBriseeLayer.class);
  }

  @Override
  public void applyModification() {
    LigneBriseeConfiguration config = getLookup().lookup(LigneBriseeConfiguration.class);
    PlanimetryLigneBriseeLayer layer = getLookup().lookup(PlanimetryLigneBriseeLayer.class);
    layer.setLayerConfiguration(config.copy());
    layer.repaint();

  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = new Sheet();
    LigneBriseeConfiguration lookup = getLookup().lookup(LigneBriseeConfiguration.class);
    List<Set> createSet = LigneBriseeConfigurationInfo.createSet(lookup);
    for (Set set : createSet) {
      sheet.put(set);
    }
    return sheet;
  }
}
