package org.fudaa.fudaa.crue.planimetry.save;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.conf.CrueEtudeConfigurationHelper;
import org.fudaa.dodico.crue.io.conf.CrueEtudeDaoConfiguration;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager;
import org.fudaa.dodico.crue.projet.planimetry.LayerVisibility;
import org.fudaa.fudaa.crue.common.config.ConfigurationInfoHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.options.config.ModellingLoiConfigurationConverter;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.openide.nodes.Node.Property;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

/**
 *
 * @author deniger
 */
public class ConfigLoaderHydraulic {

  public static ConfigLoaderHydraulic.Result loadConfigEtude(EMHProjet projet) {
    ConfigLoaderHydraulic.Result load = null;
    try {
      if (!projet.getInfos().isDirOfConfigDefined()) {
        return null;
      }
      final File dirOfConfig = projet.getInfos().getDirOfConfig();
      ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);
      Map<String, String> etudeProperties = ConfigLoaderExtern.readDefaultPanimetryConfig(configurationManagerService);
      File configEtu = new File(dirOfConfig, GlobalOptionsManager.ETUDE_FILE);
      if (configEtu.exists()) {
        CrueIOResu<CrueEtudeDaoConfiguration> read = CrueEtudeConfigurationHelper.read(configEtu, new ModellingLoiConfigurationConverter());
        if (read.getAnalyse().containsErrorOrSevereError()) {
          LogsDisplayer.displayError(read.getAnalyse(), configEtu.getAbsolutePath());
        }
        Map<String, String> planimetryProperties = CrueEtudeConfigurationHelper.getPlanimetryProperties(read.getMetier());
        for (Map.Entry<String, String> entry : planimetryProperties.entrySet()) {
          etudeProperties.put(entry.getKey(), entry.getValue());
        }
      }
      ConfigLoaderHydraulic loader = new ConfigLoaderHydraulic();
      load = loader.load(etudeProperties, CtuluLogLevel.WARNING);
      if (load.log.containsErrorOrSevereError()) {
        LogsDisplayer.displayError(load.log, configEtu.getAbsolutePath());
      }
    } catch (Exception e) {
      Exceptions.printStackTrace(e);
    }
    return load;
  }

  public static class Result {

    public VisuConfiguration visuConfiguration;
    public LayerVisibility layerVisibility;
    public CtuluLog log;

    public Result() {
    }
  }

  public Result load(Map<String, String> properties, CtuluLogLevel level) {
    Result res = new Result();
    res.visuConfiguration = new VisuConfiguration();
    res.layerVisibility = new LayerVisibility();
    ConfigSaverHdydaulicAndVisibility saver = new ConfigSaverHdydaulicAndVisibility();
    LinkedHashMap<String, Property> initProperties = saver.getProperties(res.visuConfiguration, res.layerVisibility);
    CtuluLog log = ConfigurationInfoHelper.setAndValidReadProperties(properties, initProperties, level);
    res.log = log;
    return res;
  }
}
