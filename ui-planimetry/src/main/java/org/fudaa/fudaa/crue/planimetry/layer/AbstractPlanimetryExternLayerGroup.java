/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.layer;

import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;
import org.fudaa.fudaa.sig.layer.FSigVisuPanel;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Frederic Deniger
 */
public class AbstractPlanimetryExternLayerGroup extends FSigLayerGroup {
    private final Set<String> idSupported;
    private boolean editable;

    /**
     * @param fSigVisuPanel          la paneau
     * @param gisAttributeInterfaces les attributs
     * @param ids                    valeur parmi les LayerLigneBriseeUnmodifiableControllerSaver.SAVE_ID,...
     */
    public AbstractPlanimetryExternLayerGroup(FSigVisuPanel fSigVisuPanel, GISAttributeInterface[] gisAttributeInterfaces, String[] ids) {
        super(fSigVisuPanel, gisAttributeInterfaces);
        setDestructible(false);
        setArbreEditable(false);
        if (ids == null) {
            idSupported = Collections.emptySet();
        } else {
            idSupported = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(ids)));
        }
    }

    public Set<String> getTypeIdSupported() {
        return idSupported;
    }

    @Override
    public boolean canAddNewLayer() {
        return editable;
    }

    @Override
    public boolean isMovable() {
        return false;
    }

    @Override
    public boolean isTitleModifiable() {
        return false;
    }

    @Override
    public void setActionsEnable(boolean _b) {
        super.setActionsEnable(_b && editable);
    }

    public final void setArbreEditable(boolean editable) {
        this.editable = editable;
        if (getActions() != null) {
            EbliLib.setActionEnable(getActions(), editable);
        }
    }
}
