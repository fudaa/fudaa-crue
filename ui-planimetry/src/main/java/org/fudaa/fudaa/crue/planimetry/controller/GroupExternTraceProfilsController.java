/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.controller;

import java.io.File;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternTraceProfilLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternTraceProfilsLayerGroup;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.joda.time.format.ISODateTimeFormat;

/**
 *
 * @author Frederic Deniger
 */
public class GroupExternTraceProfilsController extends AbstractGroupExternController<PlanimetryExternTraceProfilsLayerGroup> {

  public GroupExternTraceProfilsController(PlanimetryController controller, PlanimetryExternTraceProfilsLayerGroup groupLayer) {
    super(controller, groupLayer);
  }

  public PlanimetryExternTraceProfilLayer addTraceProfil(GISZoneCollectionLigneBrisee lignes, File file, SigFormatHelper.EnumFormat fmt) {
    final FSigEditor editor = controller.getVisuPanel().getEditor();
    final PlanimetryExternTraceProfilLayer layer = ExternLayerFactory.createLayerTraceProfil(lignes, file, fmt, editor);
    addAdditionalLayer(layer);
    final String date = ISODateTimeFormat.dateHourMinuteSecond().print(System.currentTimeMillis());
    String name = "TraceProfil-" + StringUtils.replaceChars(date, ":.", "__");
    List<String> usedNames = getUsedNames();
    UniqueNomFinder finder = new UniqueNomFinder();
    name = finder.findUniqueName(usedNames, name);
    layer.setName(name);
    return layer;
  }
}
