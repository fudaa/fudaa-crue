package org.fudaa.fudaa.crue.planimetry.controller;

import gnu.trove.TIntArrayList;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISZoneCollectionGeometry;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.ebli.calque.edition.ZModelePointEditable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.action.OpenEMHAction;
import org.fudaa.fudaa.crue.planimetry.action.SimplifyProfilsAction;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.layer.LayerControllerEMH;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetrySectionLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetrySectionLayerModel;
import org.openide.util.NbBundle;

/**
 * @author deniger
 */
public class LayerSectionController extends AbstractLayerSectionController<PlanimetrySectionLayer> implements LayerControllerEMH {

  protected static LayerSectionController install(PlanimetryController ctx, PlanimetryVisuPanel res,
                                                  VisuConfiguration configuration, LayerBrancheController brancheController) {
    LayerSectionController sectionController = new LayerSectionController();
    final PlanimetrySectionLayerModel layerModel = new PlanimetrySectionLayerModel(ctx.gisModel.createSectionCollection(),
        sectionController);
    PlanimetrySectionLayer sectionLayer = new PlanimetrySectionLayer(layerModel, res.getEditor());
    sectionLayer.setLayerConfiguration(configuration.getSectionConfiguration());
    sectionLayer.setName(PlanimetryController.NAME_CQ_SECTION);
    sectionLayer.setTitle(NbBundle.getMessage(PlanimetryControllerBuilder.class, PlanimetryController.NAME_CQ_SECTION));
    sectionController.layer = sectionLayer;
    sectionController.layer.setActions(new EbliActionInterface[]{
        new OpenEMHAction.Reopen(ctx),
        new OpenEMHAction.NewFrame(ctx),
        null,
        res.getSigEditAction(),
        null,
        res.getEditor().getExportWithNameAction(),
        null,
        res.getEditor().getActionDelete(),
        res.getPlanimetryVisuController().getDeleteCascade(),
        null,
        new SimplifyProfilsAction(ctx),
        null,
        res.createFindAction(sectionController.layer)});
    ctx.controllersByName.put(sectionLayer.getName(), sectionController);
    return sectionController;
  }

  @Override
  protected GISZoneCollectionPoint getGISCollection() {
    return ((ZModelePointEditable) layer.modele()).getGeomData();
  }

  @Override
  public void changeWillBeDone() {
    //rien a faire: le modele SIG est reconstruit.
  }

  @Override
  public void cancel() {
    reset();
  }

  @Override
  public void setVisuConfiguration(VisuConfiguration cloned) {
    getLayer().setLayerConfiguration(cloned.getSectionConfiguration());
    rebuildGis();
  }

  @Override
  public List<ToStringInternationalizable> getCategories() {
    return Arrays.asList(EnumSectionType.values());
  }

  @Override
  protected VisuConfiguration getVisuConfiguration() {
    return layer.getVisuConfiguration();
  }

  @Override
  protected void setGISCollection(GISZoneCollectionGeometry newGis) {
    ((ZModelePointEditable) layer.modele()).setGeometries(newGis);
  }

  @Override
  protected GISZoneCollectionPoint rebuild(double offsetForAmonAvalSection) {
    LayerSectionGisBuilder builder = new LayerSectionGisBuilder(super.helper.getController());
    return builder.rebuild(offsetForAmonAvalSection);
  }

  public boolean setSelectedEMHs(List<EMH> selectedEMH) {
    List<EMH> selectEMHS = EMHHelper.selectEMHS(selectedEMH, EnumCatEMH.SECTION);
    TIntArrayList positions = new TIntArrayList();
    if (!selectEMHS.isEmpty()) {
      Set<Long> uids = new HashSet<>();
      for (EMH emh : selectEMHS) {
        uids.add(emh.getUiId());
      }
      GISZoneCollectionPoint gisCollection = getGISCollection();
      int nb = gisCollection.size();
      for (int i = 0; i < nb; i++) {
        RelationEMHSectionDansBranche relationSectionDansBranche = getHelper().getPlanimetryGisModelContainer().getRelationSectionDansBranche(
            gisCollection, i);
        if (uids.contains(relationSectionDansBranche.getEmh().getUiId())) {
          positions.add(i);
        }
      }
    }
    layer.setSelection(positions.toNativeArray());
    return !positions.isEmpty();
  }

  /**
   * @param uid the uid of the section
   * @return the GisPoint
   */
  public GISPoint getPoint(Long uid) {
    if (uid == null) {
      return null;
    }
    GISZoneCollectionPoint gisCollection = getGISCollection();
    int nb = gisCollection.size();
    for (int i = 0; i < nb; i++) {
      RelationEMHSectionDansBranche relationSectionDansBranche = getHelper().getPlanimetryGisModelContainer().getRelationSectionDansBranche(
          gisCollection, i);
      if (uid.equals(relationSectionDansBranche.getEmh().getUiId())) {
        return gisCollection.get(i);
      }
    }
    return null;
  }

  public boolean setSelectedEMHUids(Collection<Long> uids) {
    TIntArrayList positions = new TIntArrayList();
    if (!uids.isEmpty()) {
      GISZoneCollectionPoint gisCollection = getGISCollection();
      int nb = gisCollection.size();
      for (int i = 0; i < nb; i++) {
        RelationEMHSectionDansBranche relationSectionDansBranche = getHelper().getPlanimetryGisModelContainer().getRelationSectionDansBranche(
            gisCollection, i);
        if (uids.contains(relationSectionDansBranche.getEmh().getUiId())) {
          positions.add(i);
        }
      }
    }
    layer.setSelection(positions.toNativeArray());
    return !positions.isEmpty();
  }

  @Override
  public EMH getEMHFromLayerPosition(int position) {
    GISZoneCollectionPoint gisCollection = getGISCollection();
    RelationEMHSectionDansBranche relationSectionDansBranche = getHelper().getPlanimetryGisModelContainer().getRelationSectionDansBranche(
        gisCollection, position);
    return relationSectionDansBranche.getEmh();
  }

  public void getSelectedEMHs(Collection<Long> uids) {
    if (!layer.isVisible() || layer.isSelectionEmpty()) {
      return;
    }
    int[] selectedIndex = layer.getSelectedIndex();

    if (selectedIndex != null) {
      GISZoneCollectionPoint gisCollection = getGISCollection();
      for (int i = 0; i < selectedIndex.length; i++) {
        RelationEMHSectionDansBranche relationSectionDansBranche = getHelper().getPlanimetryGisModelContainer().getRelationSectionDansBranche(
            gisCollection, selectedIndex[i]);
        uids.add(relationSectionDansBranche.getEmh().getUiId());
      }
    }
  }
}
