package org.fudaa.fudaa.crue.planimetry;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JSeparator;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.edition.BPaletteEdition;

/**
 *
 * @author deniger
 */
public class PlanimetryGisPaletteEdition extends BPaletteEdition {

  public PlanimetryGisPaletteEdition(ZScene _scene) {
    super(_scene);
  }

  @Override
  public void buildComponents() {
    setLayout(new BuVerticalLayout(0, true, false));
    setBorder(BorderFactory.createEmptyBorder(5, 2, 0, 2));
    buildGlobalsActions(this);
    editorPanel_ = new BuPanel();
    editorPanel_.setLayout(new BuBorderLayout());
    editorPanel_.setBorder(BorderFactory.createEtchedBorder());
    editorPanel_.setVisible(false);
//    add(editorPanel_);
  }

  private Set<String> getHiddenAction() {
    return new HashSet<>(Arrays.asList(ATOM_ACTION, SEGMENT_ACTION, ADD_MULTIPOINT_ACTION, ADD_ELLIPSE_ACTION, ADD_ATOM_ACTION));
  }

  @Override
  protected void buildGlobalsActions(final BuPanel _dest) {
    if (globalButtons_ == null) {
      globalButtons_ = new ArrayList();
      fillGlobalActions(globalButtons_);
    }
    Set<String> hiddenAction = getHiddenAction();
    for (Object object : globalButtons_) {
      if (object == null) {
        _dest.add(new JSeparator());
      } else {
        AbstractButton bt = (AbstractButton) object;
        if (!hiddenAction.contains(bt.getActionCommand())) {
          _dest.add((JComponent) object);
        }
      }
    }
  }
}
