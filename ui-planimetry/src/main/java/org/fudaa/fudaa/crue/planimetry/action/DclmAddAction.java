/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.action;

import java.awt.event.ActionEvent;
import java.util.List;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.CalcTransItem;
import org.fudaa.dodico.crue.metier.emh.DonCLimMCommonItem;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.factory.DclmFactory;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.view.ChooseInListHelper;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class DclmAddAction extends EbliActionSimple {
  
  private final Long emhUid;
  private final PlanimetryControllerHelper helper;
  private final DclmFactory.CalcBuilder builder;
  
  public DclmAddAction(PlanimetryControllerHelper controller, EMH emh, DclmFactory.CalcBuilder builder) {
    super(builder.getNom(), null, "ADD");
    this.emhUid = emh.getUiId();
    this.helper = controller;
    this.builder = builder;
  }
  
  @Override
  public void actionPerformed(ActionEvent _e) {
    helper.scenarioEditionStarted();//attention des données vont être clonées !
    EMHScenario scenario = helper.getController().getScenario();
    EMH emh = scenario.getIdRegistry().getEmh(emhUid);
    Calc editedCalc = helper.getcondLimiteController().getSelectedCalc();
    
    DonCLimMCommonItem dclm = DclmFactory.create(builder, helper.getController().getCrueConfigMetier(), emh, editedCalc);
    //on doit vérifier les lois.
    if (editedCalc.isTransitoire() && (dclm instanceof CalcTransItem)) {
      CalcTransItem transItem = (CalcTransItem) dclm;
      List<Loi> filterDLHY = LoiHelper.filterDLHY(scenario, transItem.getTypeLoi());
      if (filterDLHY.isEmpty()) {
        DialogHelper.showError(getTitle(), NbBundle.getMessage(DclmAddAction.class, "AddTransitoireDclm.NoLoi"));
        return;
      }
      ChooseInListHelper<Loi> chooser = new ChooseInListHelper<>();
      chooser.setDialogTitle(getTitle());
      chooser.setLabel(NbBundle.getMessage(DclmAddAction.class, "AddTransitoireDclm.ChooseLoi.Label"));
      chooser.setRenderer(new ObjetNommeCellRenderer());
      Loi choose = chooser.choose(filterDLHY, null);
      if (choose == null) {
        return;
      }
      transItem.setLoi(choose);
    }
    
    emh.addInfosEMH(dclm);
    editedCalc.addDclm(dclm);
    helper.getcondLimiteController().getLayer().repaint(0);
  }
}
