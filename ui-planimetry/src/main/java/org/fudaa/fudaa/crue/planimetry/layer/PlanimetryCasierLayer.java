package org.fudaa.fudaa.crue.planimetry.layer;

import com.memoire.bu.BuPopupMenu;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.gis.GISCoordinateSequenceContainerInterface;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceSurface;
import org.fudaa.ebli.trace.TraceSurfaceModel;
import org.fudaa.fudaa.crue.planimetry.action.LayerWithCascadeDeleteAction;
import org.fudaa.fudaa.crue.planimetry.action.PlanimetryDclmController;
import org.fudaa.fudaa.crue.planimetry.configuration.CasierConfiguration;
import org.fudaa.fudaa.crue.planimetry.controller.LayerCasierController;
import org.fudaa.fudaa.crue.planimetry.find.PlanimetryFindEMHLayerAction;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LinearRing;

import java.awt.*;

/**
 * @author deniger
 */
public class PlanimetryCasierLayer extends PlanimetryLigneBriseeLayer<CasierConfiguration>
    implements LayerWithCascadeDeleteAction, PlanimetryLayerWithEMHContrat, CalqueWithDynamicActions {
    private boolean userVisible = true;
    private final LabelPainter labelPainter = new LabelPainter();

    public PlanimetryCasierLayer(PlanimetryCasierLayerModel _modele,
                                 FSigEditor _editor) {
        super(_modele, _editor);
    }

    public static Coordinate getCenter(Geometry pts) {
        if (pts == null) {
            return null;
        }

        final Coordinate coordinate = pts.getCentroid().getCoordinate();
        if (pts instanceof LinearRing) {
            IndexedPointInAreaLocator inRing = new IndexedPointInAreaLocator(pts);
            if (!GISLib.isInside(inRing, coordinate)) {
                return pts.getInteriorPoint().getCoordinate();
            }
        }
        return coordinate;
    }

    @Override
    public boolean isMovable() {
        return false;
    }

    @Override
    public LayerControllerEMH getLayerControllerEMH() {
        return (LayerControllerEMH) getLayerController();
    }

    @Override
    public void fillPopup(BuPopupMenu popup) {
        if (isEditable() && isOnlyOneObjectSelected()) {
            int idx = getSelectedIndex()[0];
            EMH emh = getLayerControllerEMH().getEMHFromLayerPosition(idx);
            PlanimetryDclmController dclmController = new PlanimetryDclmController(getCasierController().getHelper());
            dclmController.fillPopupMenu(emh, popup);
        }
    }

    @Override
    public String editSelected() {
        getActions()[0].actionPerformed(null);
        return null;
    }

    @Override
    public EbliFindActionInterface getFinder() {
        return new PlanimetryFindEMHLayerAction(this);
    }

    @Override
    public boolean isTitleModifiable() {
        return false;
    }

    @Override
    public void removeCascadeSelectedObjects() {
        if (!isSelectionEmpty()) {
            final boolean r = ((PlanimetryCasierLayerModel) modele_).removeLigneBriseeCascade(getSelectedIndex(), null);
            if (r) {
                clearSelection();
            }
        }
    }

    @Override
    public boolean isSelected(int idxCasier) {
        if (this.getSelectionMode() == SelectionMode.NORMAL && selection_ != null) {
            return selection_.isSelected(idxCasier);
        }
        if (selectionMulti_ != null) {
            final CtuluListSelection selection = selectionMulti_.get(idxCasier);
            return selection != null && !selection.isEmpty();
        }
        return false;
    }

    private PlanimetryCasierLayerModel getCasierModel() {
        return modeleDonnees();
    }

    private LayerCasierController getCasierController() {
        return getCasierModel().getLayerController();
    }

    /**
     * @param idx l'indice de l'objet
     * @return true si l'emh active/inactive peut être dessinée
     */
    private boolean isActivityPainted(int idx) {
        if (!isUserVisible()||!this.modele_.isGeometryVisible(idx)) {
            return false;
        }
        if (isSelected(idx)) {
            return true;
        }
        if (!layerConfiguration.isInactiveEMHVisible()) {
            CatEMHCasier casier = getCasierController().getEMHFromPositionInModel(idx);
            return casier != null && casier.getActuallyActive();
        }
        return true;
    }

    @Override
    public boolean isUserVisible() {
        return userVisible;
    }

    @Override
    public void setUserVisible(boolean userVisible) {
        if (this.userVisible != userVisible) {
            super.setUserVisible(userVisible);
            this.userVisible = userVisible;
            clearCacheAndRepaint();
        }
    }

    @Override
    public boolean isVisible() {
        return !isSelectionEmpty() || isUserVisible();
    }

    @Override
    protected boolean isPainted(int idx, GrMorphisme _versEcran) {
        return isActivityPainted(idx) && isPaintedAsPolyligne(idx, _versEcran);
    }

    private boolean isPaintedAsPolyligne(int idx, GrMorphisme _versEcran) {
        if (!modele_.isGeometryReliee(idx)) {
            return false;
        }
        Envelope envelope = modele_.getGeomData().getGeometry(idx).getEnvelopeInternal();
        double taille = Math.max(envelope.getHeight(), envelope.getWidth());
        double convertDistanceXY = GrMorphisme.convertDistanceXY(_versEcran, taille);
        return convertDistanceXY > layerConfiguration.getMinimumSize();
    }

    @Override
    public boolean canAddForme(int _typeForme) {
        final boolean ok = _typeForme == DeForme.RECTANGLE || _typeForme == DeForme.ELLIPSE || _typeForme == DeForme.POLYGONE;
        return ok && isEditable();
    }

    @Override
    protected boolean clearSelectionIfLayerNonVisible() {
        return false;
    }

    @Override
    protected Color getSurfaceSpecColor(int i, Color initColor) {
        CatEMHCasier casier = getCasierController().getCasier(i);
        Color res = layerConfiguration.getFond(casier, modeleDonnees());
        if (res != null) {
            return EbliLib.getAlphaColor(res, alpha_);
        }
        return initColor;
    }

    @Override
    public void paintDonnees(Graphics2D _g, GrMorphisme _versEcran, GrMorphisme _versReel, GrBoite _clipReel) {
        if (!layerConfiguration.isFondPainted()) {
            surfModel_ = null;
        } else {
            if (surfModel_ == null) {
                surfModel_ = new TraceSurfaceModel(TraceSurface.UNIFORME, layerConfiguration.getFond(null, modeleDonnees()), Color.WHITE);
            } else {
                surfModel_.setFgColor(layerConfiguration.getFond(null, modeleDonnees()));
            }
        }
        super.paintDonnees(_g, _versEcran, _versReel, _clipReel);
        final int nombre = modele_.getNombre();
        GrPoint pt = new GrPoint();
        TraceIcon icon = new TraceIcon();
        for (int i = nombre - 1; i >= 0; i--) {
            // il n'y a pas de points pour cette ligne
            if (modele_.getNbPointForGeometry(i) <= 0) {
                continue;
            }
            // La géometrie n'est pas visible car inactive et la configuration fait que l'on affiche pas
            if (!isActivityPainted(i)) {
                continue;
            }
            //les casiers dessine comme icone
            if (!isPaintedAsPolyligne(i, _versEcran)) {
                GISCoordinateSequenceContainerInterface geometry = (GISCoordinateSequenceContainerInterface) modele_.getGeomData().getGeometry(
                    i);
                if (geometry.getCoordinateSequence().size() == 1) {
                    pt.x_ = geometry.getCoordinateSequence().getX(0);
                    pt.y_ = geometry.getCoordinateSequence().getY(0);
                } else {
                    Coordinate center = getCenter((Geometry) geometry);
                    pt.initialiseAvec(center);
                }
                if (_clipReel.contientXY(pt)) {
                    pt.autoApplique(_versEcran);
                    layerConfiguration.initCasierAsIcon(icon.getModel(), i, getCasierModel());
                    initTraceWithAlpha(icon.getModel(), alpha_);
                    icon.paintIconCentre(_g, pt.x_, pt.y_);
                }
            }

            if (layerConfiguration.getLabelConfiguration().isDisplayLabels()) {
                Coordinate centerOfCasier = getCenter(getCasierController().getCasierCollection().getGeometry(
                    i));
                pt.initialiseAvec(centerOfCasier);
                if (_clipReel.contientXY(pt)) {
                    pt.autoApplique(_versEcran);
                    CatEMHCasier casier = getCasierController().getCasier(i);
                    labelPainter.paintLabels(_g, pt, layerConfiguration.getDisplayName(casier, modeleDonnees(), isSelected(i)), layerConfiguration.
                        getLabelConfiguration(), alpha_);
                }
            }
        }
    }

    @Override
    public PlanimetryCasierLayerModel modeleDonnees() {
        return (PlanimetryCasierLayerModel) super.modeleDonnees();
    }
}
