/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModelDoubleInterface;
import org.fudaa.ctulu.gis.GISAttributeModelObjectInterface;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.edition.EditionProfilCreator;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilEtiquette;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.LitNumerote;
import org.fudaa.dodico.crue.metier.emh.PtProfil;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.validation.ValidateAndRebuildProfilSection;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class TraceProfilToDonPrtGeoProfilSectionTransformer {

  private final EditionProfilCreator profilCreator;
  private final CrueConfigMetier ccm;
  private final EMHSousModele sousModele;
  private final ItemVariable propXt;
  private final ItemVariable propZ;

  public TraceProfilToDonPrtGeoProfilSectionTransformer(CrueConfigMetier ccm, final EMHSousModele sousModele) {
    profilCreator = new EditionProfilCreator(new UniqueNomFinder(), new CreationDefaultValue());
    this.sousModele = sousModele;
    this.ccm = ccm;
    propXt = ccm.getProperty(CrueConfigMetierConstants.PROP_XT);
    propZ = ccm.getProperty(CrueConfigMetierConstants.PROP_Z);
  }

  private <T> T getValueAt(GISZoneCollection zone, GISAttributeInterface att, int idxGeo) {
    return (T) zone.getModel(att).getObjectValueAt(idxGeo);
  }

  protected List<PtProfil> getPtProfil(GISAttributeModelDoubleInterface xt, GISAttributeModelDoubleInterface z) {

    List<PtProfil> pt = new ArrayList<>();
    if (xt == null || z == null) {
      return pt;
    }
    final int size = xt.getSize();
    for (int i = 0; i < size; i++) {
      pt.add(new PtProfil(propXt.getNormalizedValue(xt.getValue(i)), propZ.getNormalizedValue(z.getValue(i))));
    }
    return pt;
  }

  private List<DonPrtGeoProfilEtiquette> getEtiquettes(List<PtProfil> pts, GISAttributeModelObjectInterface limGeo) {
    List<DonPrtGeoProfilEtiquette> etiquettes = new ArrayList<>();
    if (limGeo == null || pts == null) {
      return etiquettes;
    }
    final int size = limGeo.getSize();
    for (int i = 0; i < size; i++) {
      String nomLitInGG = (String) limGeo.getObjectValueAt(i);
      if (StringUtils.isNotBlank(nomLitInGG)) {
        ItemEnum[] etiquetteNames = LimGeoHelper.getEtiquettes(nomLitInGG, ccm.getLitNomme());
        if (etiquetteNames != null) {
          for (ItemEnum etiquetteName : etiquetteNames) {
            DonPrtGeoProfilEtiquette etiquette = new DonPrtGeoProfilEtiquette();
            etiquette.setTypeEtiquette(etiquetteName);
            etiquette.setPoint(pts.get(i));
            etiquettes.add(etiquette);
          }
        }
      }
    }

    return etiquettes;

  }

  protected List<LitNumerote> getLitNumerote(List<PtProfil> profil, GISAttributeModelObjectInterface limLit) {

    List<LitNumerote> profils = new ArrayList<>();
    if (profil == null || limLit == null) {
      return profils;
    }
    final int size = limLit.getSize();
    LitNumerote enCours = null;
    for (int i = 0; i < size; i++) {
      String nomLitInGG = (String) limLit.getObjectValueAt(i);
      if (StringUtils.isNotBlank(nomLitInGG)) {
        if (enCours != null) {
          enCours.setLimFin(profil.get(i));
          profils.add(enCours);
          enCours = null;
        }
        LitNomme[] litNommes = findLit(nomLitInGG);
        if (litNommes != null) {
          for (LitNomme litNomme : litNommes) {
            if (enCours != null) {
              enCours.setLimFin(profil.get(i));
              profils.add(enCours);
              enCours = null;
            }
            final boolean isMineur = ccm.getLitNomme().getMineur().equals(litNomme);
            if (isMineur) {
              enCours = profilCreator.createLitMineur(sousModele, ccm, profil.get(i), profil.get(i));
            } else {
              enCours = profilCreator.createLitMajeur(sousModele, ccm, profil.get(i), profil.get(i), litNomme);
            }
            enCours.setIsLitActif(ccm.getLitNomme().isActif(litNomme));
            if (!enCours.getIsLitActif()) {
              enCours.setFrot(profilCreator.addEmptyZFkStoIfNeeded(sousModele, ccm));
            }
            enCours.setIsLitMineur(isMineur);
          }
        }
      }

    }
    if (enCours != null) {
      enCours.setLimFin(profil.get(profil.size() - 1));
      profils.add(enCours);
    }
    return profils;
  }

  public CtuluLogResult<DonPrtGeoProfilSection> getDonPrtGeoProfilSection(GISZoneCollection traceProfil, int idx) {

    String name = getValueAt(traceProfil, GGTraceProfilSectionAttributesController.NOM, idx);
    GISAttributeModelDoubleInterface xt = getValueAt(traceProfil, GGTraceProfilSectionAttributesController.XT, idx);
    GISAttributeModelDoubleInterface z = getValueAt(traceProfil, GGTraceProfilSectionAttributesController.Z, idx);
    GISAttributeModelObjectInterface limLit = getValueAt(traceProfil, GGTraceProfilSectionAttributesController.LIM_LIT, idx);
    GISAttributeModelObjectInterface limGeo = getValueAt(traceProfil, GGTraceProfilSectionAttributesController.LIM_GEOM, idx);
    List<PtProfil> pts = getPtProfil(xt, z);
    List<LitNumerote> lits = getLitNumerote(pts, limLit);
    DonPrtGeoProfilSection profilSection = new DonPrtGeoProfilSection();
    profilSection.setPtProfil(pts);
    profilSection.setLitNumerote(lits);
    profilSection.setEtiquettes(getEtiquettes(pts, limGeo));
    profilSection.setNom(CruePrefix.P_PROFIL_SECTION + name);
    profilSection.setCommentaire(NbBundle.getMessage(TraceProfilToDonPrtGeoProfilSectionTransformer.class, "traceProfil.buildFrom", name));
    profilCreator.addEmptyZFkStoIfNeeded(sousModele, ccm);
    ValidateAndRebuildProfilSection rebuilder = new ValidateAndRebuildProfilSection(ccm, null);
    CtuluLog log = rebuilder.validateAndRebuildDonPrtGeoProfilSection(profilSection, sousModele);
    return new CtuluLogResult<>(profilSection, log);
  }

  private LitNomme[] findLit(String limLit) {
    return LimLitHelper.getLitNommes(limLit, ccm.getLitNomme());
  }
}
