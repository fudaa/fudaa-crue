package org.fudaa.fudaa.crue.planimetry.layer;

import org.fudaa.ebli.calque.ZCalqueImageRaster;
import org.fudaa.ebli.calque.ZModeleImageRaster;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.crue.planimetry.action.PlanimetryImageJGWImportAction;
import org.fudaa.fudaa.crue.planimetry.save.ConfigExternIds;
import org.fudaa.fudaa.sig.layer.FSigImageImportAction;
import org.fudaa.fudaa.sig.layer.FSigVisuPanel;
import org.openide.util.NbBundle;

/**
 * Controlleur pour les images.
 *
 * @author deniger
 */
public class PlanimetryExternImagesLayerGroup extends AbstractPlanimetryExternLayerGroup {

  public PlanimetryExternImagesLayerGroup(FSigVisuPanel _impl, String name) {
    super(_impl, null, new String[]{ConfigExternIds.LAYER_IMAGE_SAVE_ID});
    setName(name);
    setTitle(NbBundle.getMessage(PlanimetryExternImagesLayerGroup.class, name));
    FSigImageImportAction importImage = new FSigImageImportAction(visu_, this) {
      @Override
      public ZCalqueImageRaster createCalque(ZModeleImageRaster _model) {
        return PlanimetryExternImagesLayerGroup.this.createCalqueImage(_model);
      }
    };
    importImage.setAddEditGeo(false);
    setActions(new EbliActionInterface[]{new PlanimetryImageJGWImportAction(this), importImage});
  }

  public PlanimetryImageRasterLayer createCalqueImage(ZModeleImageRaster _model) {
    return new PlanimetryImageRasterLayer(_model);
  }
}
