package org.fudaa.fudaa.crue.planimetry.configuration;

import org.fudaa.ctulu.gui.CtuluLibSwing;

import javax.swing.*;
import java.awt.*;

/**
 * Cette classe est utlise
 *
 * @author deniger
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class LabelConfiguration implements Cloneable {
  static final String PROP_FONT = "labelFont";
  static final String PROP_DISTANCE = "labelDistance";
  static final String PROP_ALIGMNENT = "labelAlignment";
  static final String PROP_DISPLAYLABEL = "displayLabels";
  static final String PROP_TEXT_COLOR = "labelColor";
  private Font labelFont = CtuluLibSwing.getMiniFont();
  private int labelDistance = 2;
  private int labelAlignment = SwingConstants.NORTH_EAST;
  private boolean displayLabels = true;
  private Color labelColor = Color.BLACK;


  public LabelConfiguration() {

  }

  public LabelConfiguration(LabelConfiguration from) {
    if (from != null) {
      labelFont = from.labelFont;
      labelDistance = from.labelDistance;
      labelAlignment = from.labelAlignment;
      displayLabels = from.displayLabels;
      labelColor = from.labelColor;
    }
  }

  public LabelConfiguration copy() {
    return new LabelConfiguration(this);
  }

  public void setLabelAlignment(int labelAlignment) {
    this.labelAlignment = labelAlignment;
  }

  @SuppressWarnings("WeakerAccess")
  public void setLabelFont(Font labelFont) {
    this.labelFont = labelFont;
  }

  @SuppressWarnings("unused")
  public void setLabelColor(Color textColor) {
    this.labelColor = textColor;
  }

  @SuppressWarnings("WeakerAccess")
  public void setLabelDistance(int labelDistance) {
    if (LabelConfigurationInfo.isValide(PROP_ALIGMNENT, labelDistance)) {
      this.labelDistance = labelDistance;
    }
  }

  public void setDisplayLabels(boolean displayLabels) {
    this.displayLabels = displayLabels;
  }

  public boolean isDisplayLabels() {
    return displayLabels;
  }

  public static int getNortherAlignment(int in) {
    if (in == SwingConstants.SOUTH) {
      return SwingConstants.CENTER;
    }
    if (in == SwingConstants.CENTER) {
      return SwingConstants.NORTH;
    }
    if (in == SwingConstants.EAST) {
      return SwingConstants.NORTH_EAST;
    }
    if (in == SwingConstants.WEST) {
      return SwingConstants.NORTH_WEST;
    }

    if (in == SwingConstants.SOUTH_EAST) {
      return SwingConstants.EAST;
    }
    if (in == SwingConstants.SOUTH_WEST) {
      return SwingConstants.WEST;
    }
    return in;
  }

  public static int getSoutherAlignment(int in) {
    if (in == SwingConstants.NORTH) {
      return SwingConstants.CENTER;
    }
    if (in == SwingConstants.CENTER) {
      return SwingConstants.SOUTH;
    }
    if (in == SwingConstants.EAST) {
      return SwingConstants.SOUTH_EAST;
    }
    if (in == SwingConstants.WEST) {
      return SwingConstants.SOUTH_WEST;
    }
    if (in == SwingConstants.NORTH_EAST) {
      return SwingConstants.EAST;
    }
    if (in == SwingConstants.NORTH_WEST) {
      return SwingConstants.WEST;
    }
    return in;
  }

  @Override
  public LabelConfiguration clone() {
    try {
      return (LabelConfiguration) super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    return null;
  }

  public int getLabelDistance() {
    return labelDistance;
  }

  /**
   * SwingConstants.NORTH_WEST|SwingConstants.NORTH|SwingConstants.NORTH_EAST
   * SwingConstants.WEST|SwingConstants.CENTER|SwingConstants.EAST
   * SwingConstants.SOUTH_WEST|SwingConstants.SOUTH|SwingConstants.SOUTH_EAST
   *
   * @return l'alignement
   */
  public int getLabelAlignment() {
    return labelAlignment;
  }

  public Font getLabelFont() {
    return labelFont;
  }

  public Color getLabelColor() {
    return labelColor;
  }
}
