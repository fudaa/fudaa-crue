/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.tracecasier;

import java.util.Arrays;
import java.util.List;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelFilterAdapter;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.fudaa.sig.FSigGeomSrcData;

/**
 * utiliser pour recharger un fichier persisté de TraceCasier
 *
 * @author Frederic Deniger
 */
public class GGTraceCasierLoader {

  public static GISZoneCollectionLigneBrisee buildLine(FSigGeomSrcData result) {

    GISZoneCollectionLigneBrisee lignes = new GISZoneCollectionLigneBrisee();
    GGTraceCasierAttributesController attributesController = new GGTraceCasierAttributesController();
    GISAttributeInterface[] attributes = attributesController.createCasierAttributes();
    lignes.setAttributes(attributes, null);
    if (result != null) {
      result.preload(attributes, null);
      final List<GISDataModel> polylignes = Arrays.asList(result.getPolylignes());
      if (polylignes != null) {
        for (GISDataModel model : polylignes) {
          lignes.addAll(GISDataModelFilterAdapter.buildLigneFermeeCommonAdapter(model, attributes), null, false);
        }
      }
      final List<GISDataModel> polygones = Arrays.asList(result.getPolygones());
      if (polygones != null) {
        for (GISDataModel model : polygones) {
          lignes.addAll(GISDataModelFilterAdapter.buildLigneFermeeCommonAdapter(model, attributes), null, false);
        }
      }
    }
    return lignes;
  }
}
