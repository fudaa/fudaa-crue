package org.fudaa.fudaa.crue.planimetry.action;

import java.awt.EventQueue;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteraction;
import org.fudaa.ebli.commun.EbliActionChangeState;
import org.fudaa.fudaa.crue.planimetry.PlanimetryGisPaletteEdition;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;

/**
 * Classe abstraite pour les actions d'ajout d'EMH: Branche, Casier et Noeud.
 *
 * @author deniger
 */
public abstract class AbstractAddAction extends EbliActionChangeState implements TreeSelectionListener, PropertyChangeListener {

  final PlanimetryController controller;
  final EditZone zone;
  final PlanimetryGisPaletteEdition palette;

  public AbstractAddAction(PlanimetryGisPaletteEdition palette, PlanimetryController controller, EditZone zone, String name,
          Icon ic, String cmd) {
    super(name, ic, cmd);
    this.controller = controller;
    this.palette = palette;
    this.zone = zone;
    setEnabled(false);
    controller.getVisuPanel().getArbreCalqueModel().addTreeSelectionListener(this);
    controller.getVisuPanel().getEditor().getCalqueDessin().addPropertyChangeListener("gele", this);
  }

  public abstract String getDefaultPaletteAction();

  public boolean isSelectedLayerOk() {
    return controller.getVisuPanel().getArbreCalqueModel().getSelectedCalque() == getLayer();
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (isUpdating || controller.getVisuPanel().getEditor().isStateChanging()) {
      return;
    }
    final boolean gele = getCalqueDessin().isGele();
    final boolean selected = isSelected();
    if (selected && gele) {
      changeAll();
    }
    if (!selected && !gele && isSelectedLayerOk()) {
      changeAll();
    }
  }

  public ZCalqueEditionInteraction getCalqueDessin() {
    return controller.getVisuPanel().getEditor().getCalqueDessin();
  }

  @Override
  public void valueChanged(TreeSelectionEvent e) {
    if (isUpdating) {
      return;
    }
    if (isSelected() && !isSelectedLayerOk()) {
      changeAll();
    }
  }

  protected abstract ZCalqueAffichageDonnees getLayer();

  protected abstract JComponent getEditionComponent();
  boolean isUpdating = false;

  @Override
  public void changeAction() {
    isUpdating = true;
    if (isSelected()) {
      if (!isSelectedLayerOk()) {
        controller.getVisuPanel().getArbreCalqueModel().setSelectionCalque(getLayer());
      }
      zone.active(getEditionComponent());
      if (getCalqueDessin().isGele()) {
        getCalqueDessin().setGele(false);
        palette.setButtonSelected(getDefaultPaletteAction());
      }
    } else {
      zone.unactive(getEditionComponent());
      getCalqueDessin().setGele(true);
      //plus tard on remet la sélection en route.
      EventQueue.invokeLater(() -> resetToSelection());
    }
    isUpdating = false;
  }

  public void resetToSelection() {
    if (!controller.getVisuPanel().getPlanimetryVisuController().isCalqueInterationEnabled()) {
      controller.getVisuPanel().setCalqueSelectionActif();
    }
  }
}
