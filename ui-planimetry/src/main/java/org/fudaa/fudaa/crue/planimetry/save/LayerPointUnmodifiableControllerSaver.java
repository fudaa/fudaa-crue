package org.fudaa.fudaa.crue.planimetry.save;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessource;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessourceInfos;
import org.fudaa.dodico.crue.io.conf.Option;
import org.fudaa.fudaa.crue.common.config.ConfigSaverHelper;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper.EnumFormat;
import org.fudaa.fudaa.crue.planimetry.configuration.PointConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.controller.ExternLayerFactory;
import org.fudaa.fudaa.crue.planimetry.controller.LayerPointExternController;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryPointExternUnmodifiableLayer;
import org.fudaa.fudaa.crue.planimetry.sig.SigLoaderHelper;
import org.fudaa.fudaa.sig.FSigGeomSrcData;
import org.openide.nodes.Sheet.Set;

/**
 * Pour etre pris compte doit être ajoute dans la liste AdditionalLayerLoaderProcess
 *
 * @author deniger
 */
public class LayerPointUnmodifiableControllerSaver implements AdditionalLayerLoaderProcess.Loader {

  public CrueEtudeExternRessource getSaveRessource(String id, LayerPointExternController layerController, File baseDir) {
    LayerControllerSaverHelper helper = new LayerControllerSaverHelper();
    CrueEtudeExternRessourceInfos ressource = new CrueEtudeExternRessourceInfos();
    final PlanimetryPointExternUnmodifiableLayer layer = (PlanimetryPointExternUnmodifiableLayer) layerController.getLayer();
    ressource.setNom(layer.getTitle());
    ressource.setType(ConfigExternIds.LAYER_POINT_UNMODIFIABLE_SAVE_ID);
    ressource.setId(id);
    ressource.setLayerId(layer.getName());
    File file = layer.getFile();
    helper.putRelativeAndAbsolutePaths(ressource, file, baseDir);
    helper.addOption(ressource, LayerControllerSaverHelper.GEO_FORMAT, layer.getFmt().toString());
    helper.addVisibleProperty(ressource, layerController.getLayer());
    LinkedHashMap<String, String> transform = ConfigSaverHelper.getProperties(PointConfigurationInfo.createSet(
            layer.getLayerConfiguration()));
    for (Map.Entry<String, String> entry : transform.entrySet()) {
      ressource.getOptions().add(new Option(entry.getKey(), entry.getValue()));
    }

    return new CrueEtudeExternRessource(ressource);
  }

  @Override
  public String getType() {
    return ConfigExternIds.LAYER_POINT_UNMODIFIABLE_SAVE_ID;
  }

  @Override
  public PlanimetryAdditionalLayerContrat load(CrueEtudeExternRessourceInfos ressource, CtuluLog log, File baseDir) {
    LayerControllerSaverHelper helper = new LayerControllerSaverHelper();
    Map<String, String> optionsBykey = helper.toMap(ressource);
    File file = helper.retrieveFiles(optionsBykey, ressource, baseDir, log);
    if (file == null) {
      return null;
    }
    Pair<FSigGeomSrcData, EnumFormat> pair = helper.loadGisData(file, optionsBykey, ressource.getNom(), log);
    if (pair == null) {
      return null;
    }
    FSigGeomSrcData result = pair.first;
    if (result == null) {
      log.addError("externConfig.geo.NoDataFound.error", ressource.getNom(), file.getName());
    }
    GISZoneCollectionPoint buildPoints = SigLoaderHelper.buildPoints(result);
    PlanimetryPointExternUnmodifiableLayer layer = ExternLayerFactory.createLayerPointUnmodifiable(buildPoints, file,
            pair.second,
            null);
    layer.setName(ressource.getLayerId());
    layer.setTitle(ressource.getNom());
    helper.addVisibleProperty(ressource, layer);
    List<Set> propertySet = PointConfigurationInfo.createSet(layer.getLayerConfiguration());
    LayerControllerSaverHelper.restoreVisuProperties(optionsBykey, layer, propertySet, log);
    return layer;
  }
}
