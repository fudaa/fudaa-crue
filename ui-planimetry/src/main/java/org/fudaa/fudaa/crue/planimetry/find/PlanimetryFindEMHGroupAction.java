/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.find;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.find.EbliFindComponent;
import org.fudaa.ebli.find.EbliFindable;
import org.fudaa.ebli.find.EbliFindableItem;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLayerWithEMHContrat;

/**
 *
 * @author Frederic Deniger
 */
public class PlanimetryFindEMHGroupAction implements EbliFindActionInterface {

  final BGroupeCalque groupe;

  public PlanimetryFindEMHGroupAction(BGroupeCalque layer) {
    this.groupe = layer;
  }

  @Override
  public boolean find(Object _s, String _findId, int _selOption, EbliFindable _parent) {
    if (!PlanimetryFindComponent.NAME_AND_OPTIONS.equals(_findId)) {
      return false;
    }
    String name = ((FindQuery) _s).getTextToFind();
    if (StringUtils.isBlank(name)) {
      return false;
    }
    name = name.toUpperCase();
    BCalque[] calques = groupe.getCalques();
    boolean found = false;
    boolean selectionChanged = false;
    for (BCalque bCalque : calques) {
      if (bCalque instanceof PlanimetryLayerWithEMHContrat) {
        PlanimetryLayerWithEMHContrat emhLayer = (PlanimetryLayerWithEMHContrat) bCalque;
        CtuluListSelection findSelection = PlanimetryFindEMHLayerAction.findSelection(emhLayer, name, null);
        selectionChanged |= ((ZCalqueAffichageDonnees) emhLayer).changeSelection(findSelection, _selOption);
        if (findSelection != null && !findSelection.isEmpty()) {
          found = true;
        }
      }
    }
    if (selectionChanged) {
      ((PlanimetryVisuPanel) _parent).getScene().fireSelectionEvent();
    }
    return found;

  }

  @Override
  public String erreur(Object _s, String _findId, EbliFindable _parent) {
    return null;
  }

  @Override
  public EbliFindComponent createFindComponent(EbliFindable _parent) {
    PlanimetryFindComponent res = new PlanimetryFindComponent();
    res.setTxtLabel(org.openide.util.NbBundle.getMessage(PlanimetryFindEMHGroupAction.class, "find.AllLayer"));
    res.setTxtTooltip(org.openide.util.NbBundle.getMessage(PlanimetryFindEMHGroupAction.class, "find.AllLayer.Tooltip"));
    return res;
  }

  @Override
  public boolean isEditableEnable(String _searchId, EbliFindable _parent) {
    return false;
  }

  @Override
  public GrBoite getZoomOnSelected() {
    GrBoite res = null;
    BCalque[] calques = groupe.getCalques();
    for (BCalque bCalque : calques) {
      if (bCalque instanceof PlanimetryLayerWithEMHContrat) {
        PlanimetryLayerWithEMHContrat emhLayer = (PlanimetryLayerWithEMHContrat) bCalque;
        final GrBoite zoomOnSelected = emhLayer.getZoomOnSelected();
        if (res == null) {
          res = zoomOnSelected;
        } else if (zoomOnSelected != null) {
          res.ajuste(zoomOnSelected);
        }
      }
    }
    return res;
  }

  @Override
  public EbliFindableItem getCalque() {
    return (EbliFindableItem) groupe;
  }

  @Override
  public String editSelected(EbliFindable _parent) {
    return null;
  }
}
