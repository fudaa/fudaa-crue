/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.action;

import com.memoire.bu.BuResource;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogGroupResult;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryGisModelContainer;
import org.fudaa.fudaa.crue.planimetry.services.PlanimetryModellingController;
import org.fudaa.fudaa.crue.planimetry.sig.traceprofil.*;
import org.openide.DialogDescriptor;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

import java.awt.event.ActionEvent;
import java.util.List;

/**
 * Action qui permet d'attacher des sections a des branches a partir d'une selection de traceProfil.
 *
 * @author Frederic Deniger
 */
public class SectionProfilAttachedToBrancheAction extends AbstractEnablingAction {
  private final transient PlanimetryControllerHelper helper;
  private final transient SectionProfilToBrancheSelectionController selection;

  public SectionProfilAttachedToBrancheAction(PlanimetryController controller) {
    super(NbBundle.getMessage(SectionProfilAttachedToBrancheAction.class, "AttachedTraceProfilToBranche.ActionName"),
        BuResource.BU.getIcon("crystal_generer"), "ATTACHED_TO_BRANCHE", controller.getVisuPanel());
    this.helper = controller.getHelper();
    this.selection = new SectionProfilToBrancheSelectionController(this.helper);
  }

  @Override
  protected boolean isProcessorEnable() {
    return selection != null && selection.isActionEnable();
  }

  @Override
  public void actionPerformed(ActionEvent actionEvent) {
    final TopComponent targetTc = WindowManager.getDefault().findTopComponent(PlanimetryModellingController.TOPCOMPONENT_ID);
    if (targetTc == null) {
      return;
    }
    targetTc.requestActive();
    final PlanimetryGisModelContainer planimetryGisModelContainer = helper.getPlanimetryGisModelContainer();
    EMHScenario scenario = planimetryGisModelContainer.getScenario();
    SectionProfilToBrancheParametersInput inputs = getSelection();
    if (inputs == null || inputs.isEmpty()) {
      CtuluUIForNetbeans.DEFAULT.message(getTitle(), NbBundle.getMessage(SectionProfilAttachedToBrancheAction.class, "noOperation.information"), true);
      return;
    }

    SectionProfilToBrancheProcessorPrepareInputs processor = new SectionProfilToBrancheProcessorPrepareInputs(scenario, helper.getController().
        getCrueConfigMetier());
    CtuluLogGroupResult<List<SectionProfilToBrancheParametersChange>, Object> process = processor.
        process(inputs.getBranchesParameters(), inputs.getSectionParameters());
    if (process.getLog().containsSomething()) {
      LogsDisplayer.displayError(process.getLog(), getTitle());
    }
    if (process.getLog().containsFatalError() || process.getResultat() == null) {
      return;
    }
    displayModifications(process.getResultat());
  }

  private SectionProfilToBrancheParametersInput getSelection() {

    CtuluLogResult<SectionProfilToBrancheParametersInput> inputs = selection.createParametersFromSelection(true);
    if (inputs.getLog().containsErrorOrSevereError()) {
      LogsDisplayer.displayError(inputs.getLog(), getTitle());
      return null;
    }
    return inputs.getResultat();
  }

  private void displayModifications(
      List<SectionProfilToBrancheParametersChange> resultat) {
    if (resultat.isEmpty()) {
      return;
    }
    SectionProfilToBranchePanel panel = new SectionProfilToBranchePanel(resultat);
    DialogHelper.readInPreferences(panel.getView(), "outlineView", getClass());
    DialogDescriptor descriptor = new DialogDescriptor(panel, getTitle());
    SysdocUrlBuilder.installDialogHelpCtx(descriptor, "affecteSectionsBranches", PerspectiveEnum.MODELLING, false);
    boolean accepted = DialogHelper.showQuestion(descriptor, getClass(), DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    DialogHelper.writeInPreferences(panel.getView(), "outlineView", getClass());
    if (accepted) {
      final List<SectionProfilToBrancheParametersChange> acceptedModification = panel.getAcceptedModification();
      if (!acceptedModification.isEmpty()) {
        doModifications(acceptedModification);
      }
    }
  }

  private void doModifications(
      List<SectionProfilToBrancheParametersChange> acceptedModification) {
    SectionProfilToBrancheProcessor processor = new SectionProfilToBrancheProcessor(helper, acceptedModification);
    CtuluLogGroup logs = CrueProgressUtils.showProgressDialogAndRun(processor, getTitle());
    if (logs.containsSomething()) {
      LogsDisplayer.displayError(logs, getTitle());
    } else {
      DialogHelper.showNotifyOperationTermine(getTitle(), NbBundle.getMessage(SectionProfilAttachedToBrancheAction.class, "profilAttachedToBranche.completed"));
    }
  }
}
