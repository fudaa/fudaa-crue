/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.save;

/**
 * Les ids utilisés pour les sauvegardes de données externes ( images, dessins et importer SIG).
 *
 * @author Frederic Deniger
 */
public class ConfigExternIds {

  public static final String LAYER_LINE_MODIFIABLE_SAVE_ID = "LINES_MODIFIABLE";
  public static final String LAYER_LINE_UNMODIFIABLE_SAVE_ID = "LINES_EXTERN_UNMODIFIABLE";
  public static final String LAYER_TRACES_PROFILS_SAVE_ID = "TRACES_PROFILS";
  public static final String LAYER_TRACES_CASIERS_SAVE_ID = "TRACES_CASIERS";
  public static final String LAYER_POINT_MODIFIABLE_SAVE_ID = "POINTS_MODIFIABLE";
  public static final String LAYER_POINT_UNMODIFIABLE_SAVE_ID = "POINTS_EXTERN_UNMODIFIABLE";
  public static final String LAYER_IMAGE_SAVE_ID = "IMAGE";
}
