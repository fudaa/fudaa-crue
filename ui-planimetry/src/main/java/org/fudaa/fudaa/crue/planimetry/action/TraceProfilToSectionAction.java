/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.action;

import java.awt.event.ActionEvent;
import java.util.List;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryGisModelContainer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternTraceProfilLayer;
import org.fudaa.fudaa.crue.planimetry.services.PlanimetryModellingController;
import org.fudaa.fudaa.crue.planimetry.sig.traceprofil.TraceProfilSousModele;
import org.fudaa.fudaa.crue.planimetry.sig.traceprofil.TraceProfilSousModeleContent;
import org.fudaa.fudaa.crue.planimetry.sig.traceprofil.TraceProfilToSectionPanel;
import org.fudaa.fudaa.crue.planimetry.sig.traceprofil.TraceProfilToSectionPreloadProcessor;
import org.fudaa.fudaa.crue.planimetry.sig.traceprofil.TraceProfilToSectionProcessor;
import org.openide.DialogDescriptor;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 *
 * @author Frederic Deniger
 */
public class TraceProfilToSectionAction extends EbliActionSimple {

  private final PlanimetryExternTraceProfilLayer layer;
  final PlanimetryModellingController planimetryModellingController = new PlanimetryModellingController();

  public TraceProfilToSectionAction(PlanimetryExternTraceProfilLayer layer) {
    super(NbBundle.getMessage(TraceProfilToSectionAction.class, "CreateSectionFromTraceProfil.ActionName"), null, "CREATE_SECTION");
    this.layer = layer;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    final TopComponent targetTc = WindowManager.getDefault().findTopComponent(PlanimetryModellingController.TOPCOMPONENT_ID);
    if (targetTc == null) {
      return;
    }
    targetTc.requestActive();
    final PlanimetryControllerHelper helper = layer.getLayerController().getHelper();
    final PlanimetryGisModelContainer planimetryGisModelContainer = helper.getPlanimetryGisModelContainer();
    EMHScenario scenario = planimetryGisModelContainer.getScenario();
    TraceProfilToSectionPreloadProcessor preloader = new TraceProfilToSectionPreloadProcessor(scenario);
    final String defaultSousModele = helper.getDefaultSousModele().getNom();
    TraceProfilSousModeleContent preload = preloader.preload(layer.getModelEditable().getGeomData(), null, defaultSousModele);
    final List<TraceProfilSousModele> tcs = preload.getTraceProfilSousModeles();
    TraceProfilToSectionPanel panel = new TraceProfilToSectionPanel(scenario, tcs, defaultSousModele);
    DialogDescriptor des = new DialogDescriptor(panel, getTitle());
    SysdocUrlBuilder.installDialogHelpCtx(des, "creationSectionsDepuisTraceProfils", PerspectiveEnum.MODELLING, false);
    DialogHelper.readInPreferences(panel.getView(), "outlineView", getClass());
    boolean accepted = DialogHelper.showQuestion(des, TraceProfilToSectionPanel.class, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    DialogHelper.writeInPreferences(panel.getView(), "outlineView", getClass());
    if (accepted) {
      List<TraceProfilSousModele> acceptedModification = panel.getAcceptedModification();
      if (!acceptedModification.isEmpty()) {
        final String actionName = getTitle() + " / " + layer.getTitle();
        CtuluLogGroup logs = CrueProgressUtils.showProgressDialogAndRun(new TraceProfilToSectionProcessor(helper, acceptedModification, layer,
                preload.getTraceProfilPositionByName()), actionName);
        LogsDisplayer.displayError(logs, actionName);
      }
    }
  }

  @Override
  public void updateStateBeforeShow() {
    setEnabled(planimetryModellingController.isModellingPerspectiveInEditMode());
  }
}
