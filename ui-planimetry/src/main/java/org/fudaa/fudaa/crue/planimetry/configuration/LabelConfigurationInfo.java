package org.fudaa.fudaa.crue.planimetry.configuration;

import java.awt.Color;
import java.awt.Font;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import org.fudaa.dodico.crue.config.ccm.NumberRangeValidator;
import org.fudaa.fudaa.crue.common.config.ConfigurationInfoHelper;
import org.fudaa.fudaa.crue.common.editor.CodeTranslation;
import org.fudaa.fudaa.crue.common.editor.JcomboBoxIntegerInplaceEditor;
import org.fudaa.fudaa.crue.common.editor.SpinnerInplaceEditor;
import org.fudaa.fudaa.crue.planimetry.configuration.editor.LabelAlignmentPropertyEditorSupport;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.PropertySupport.Reflection;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class LabelConfigurationInfo {

  private static final Map<String, NumberRangeValidator> VALIDATOR_BY_PROPS = new HashMap<>();

  static {
    VALIDATOR_BY_PROPS.put(LabelConfiguration.PROP_DISTANCE, new NumberRangeValidator(Integer.valueOf(0), null));
  }

  public static CodeTranslation createCodes() throws MissingResourceException {
    CodeTranslation codes = new CodeTranslation();
    codes.intValues.add(SwingConstants.NORTH_WEST);
    codes.strings.add(NbBundle.getMessage(LabelConfigurationInfo.class, "label.position.northWest"));
    codes.intValues.add(SwingConstants.NORTH);
    codes.strings.add(NbBundle.getMessage(LabelConfigurationInfo.class, "label.position.north"));
    codes.intValues.add(SwingConstants.NORTH_EAST);
    codes.strings.add(NbBundle.getMessage(LabelConfigurationInfo.class, "label.position.northEast"));
    codes.intValues.add(SwingConstants.WEST);
    codes.strings.add(NbBundle.getMessage(LabelConfigurationInfo.class, "label.position.west"));
    codes.intValues.add(SwingConstants.CENTER);
    codes.strings.add(NbBundle.getMessage(LabelConfigurationInfo.class, "label.position.center"));
    codes.intValues.add(SwingConstants.EAST);
    codes.strings.add(NbBundle.getMessage(LabelConfigurationInfo.class, "label.position.east"));
    codes.intValues.add(SwingConstants.SOUTH_WEST);
    codes.strings.add(NbBundle.getMessage(LabelConfigurationInfo.class, "label.position.southWest"));
    codes.intValues.add(SwingConstants.SOUTH);
    codes.strings.add(NbBundle.getMessage(LabelConfigurationInfo.class, "label.position.south"));
    codes.intValues.add(SwingConstants.SOUTH_EAST);
    codes.strings.add(NbBundle.getMessage(LabelConfigurationInfo.class, "label.position.southEast"));
    return codes;
  }

  public static boolean isValide(String props, double value) {
    NumberRangeValidator get = VALIDATOR_BY_PROPS.get(props);
    if (get == null) {
      return true;
    }
    return get.containsNumber(value);
  }

  public static Sheet.Set createSet(LabelConfiguration in, String prefix, boolean addColor) {
    return createSet(in, prefix, null, addColor);
  }

  public static Sheet.Set createSet(LabelConfiguration in, String prefix, Node.Property<?> first, boolean addColor) {
    Sheet.Set set = Sheet.createPropertiesSet();
    if (first != null) {
      set.put(first);
    }
    set.setDisplayName(NbBundle.getMessage(LabelConfigurationInfo.class, "LabelConfiguration.DisplayName"));
    set.setShortDescription(set.getDisplayName());
    set.setName(prefix + ".label");
    try {
      set.put(createDisplayLabels(in));
      set.put(createDistance(in));
      set.put(createFont(in));
      set.put(createLabelAlignment(in));
      if (addColor) {
        set.put(createTextColor(in));
      }
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return set;
  }

  private static Reflection createDistance(LabelConfiguration in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(LabelConfiguration.PROP_DISTANCE,
            Integer.TYPE, in, VisuConfigurationInfo.class);
    SpinnerInplaceEditor inplaceEditor = new SpinnerInplaceEditor(new SpinnerNumberModel(1, 1, 100, 1));
    res.setValue("inplaceEditor", inplaceEditor);
    return res;
  }

  private static Reflection createFont(LabelConfiguration in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(LabelConfiguration.PROP_FONT,
            Font.class, in, VisuConfigurationInfo.class);
    res.setValue("canEditAsText", Boolean.FALSE);
    return res;
  }

  private static Reflection createDisplayLabels(LabelConfiguration in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(LabelConfiguration.PROP_DISPLAYLABEL,
            Boolean.TYPE, in, VisuConfigurationInfo.class);
    return res;
  }

  private static Reflection createTextColor(LabelConfiguration in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(LabelConfiguration.PROP_TEXT_COLOR,
            Color.class, in, VisuConfigurationInfo.class);
    return res;
  }

  private static Reflection createLabelAlignment(LabelConfiguration in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(LabelConfiguration.PROP_ALIGMNENT,
            Integer.TYPE, in, VisuConfigurationInfo.class);
    CodeTranslation codes = createCodes();
    JcomboBoxIntegerInplaceEditor inplaceEditor = new JcomboBoxIntegerInplaceEditor(codes);
    res.setPropertyEditorClass(LabelAlignmentPropertyEditorSupport.class);
    res.setValue("inplaceEditor", inplaceEditor);
    return res;
  }
}
