package org.fudaa.fudaa.crue.planimetry.configuration;

import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryBrancheModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLigneBriseeLayerModel;

import javax.swing.*;
import java.awt.*;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * @author deniger
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class BrancheConfiguration extends AbstractLigneBriseeConfiguration {
  public static final String PROPERTY_SENS_PAINTED = "displaySens";
  public static final String PROPERTY_FLECHE_SIZE = "flecheSize";
  TraceLigneModel lineForBranche = new TraceLigneModel(TraceLigne.LISSE, 4f, Color.CYAN);
  TraceIconModel iconForLinePoints = new TraceIconModel(TraceIcon.CARRE_PLEIN, 4, Color.BLACK);
  TraceIconModel iconModelTypeBranche = new TraceIconModel(TraceIcon.LOSANGE, 5, Color.BLACK);
  private LabelConfiguration labelConfiguration = new LabelConfiguration();
  private boolean displaySens = false;
  private int flecheSize = 11;
  private final VisuConfiguration parent;
  private final Map<EnumBrancheType, Color> colorByBrancheType = new EnumMap<>(EnumBrancheType.class);

  public BrancheConfiguration(final VisuConfiguration parent) {
    labelConfiguration.setLabelFont(VisuConfiguration.deriveFont(labelConfiguration.getLabelFont(), Font.BOLD, 2));
    labelConfiguration.setLabelAlignment(SwingConstants.EAST);
    this.parent = parent;
    fillDefaultColor();
  }

  public BrancheConfiguration(BrancheConfiguration from, final VisuConfiguration parent) {
    super(from);
    if (from != null) {
      this.lineForBranche = new TraceLigneModel(lineForBranche);
      this.iconForLinePoints = new TraceIconModel(from.iconForLinePoints);
      this.iconModelTypeBranche = new TraceIconModel(from.iconModelTypeBranche);
      this.labelConfiguration = from.labelConfiguration.copy();
      this.flecheSize = from.flecheSize;
      this.displaySens = from.displaySens;
      this.colorByBrancheType.putAll(from.colorByBrancheType);
    }
    this.parent = parent;
  }

  @Override
  public BrancheConfiguration copy() {
    return new BrancheConfiguration(this, parent);
  }

  public int getFlecheSize() {
    return flecheSize;
  }

  public void setFlecheSize(final int flecheSize) {
    this.flecheSize = flecheSize;
  }

  private void fillDefaultColor() {
    final List<EnumBrancheType> availablesBrancheType = EnumBrancheType.getAvailablesBrancheType();
    for (final EnumBrancheType enumBrancheType : availablesBrancheType) {
      colorByBrancheType.put(enumBrancheType, Color.CYAN);
    }
  }

  public Map<EnumBrancheType, Color> getColorByBrancheType() {
    return Collections.unmodifiableMap(colorByBrancheType);
  }

  public Color getColor(final EnumBrancheType type) {
    return colorByBrancheType.get(type);
  }

  public void setColor(final EnumBrancheType type, final Color color) {
    colorByBrancheType.put(type, color);
  }

  public boolean isInactiveEMHVisible() {
    return parent.isInactiveEMHVisible();
  }

  public LabelConfiguration getLabelConfiguration() {
    return labelConfiguration;
  }

  @SuppressWarnings("unused")
  public float getEpaisseur() {
    return lineForBranche.getEpaisseur();
  }

  public boolean isDisplaySens() {
    return displaySens;
  }

  public void setDisplaySens(final boolean displaySens) {
    this.displaySens = displaySens;
  }

  public TraceLigneModel getLigneModel() {
    return lineForBranche;
  }

  public TraceIconModel getIconForLinePoints() {
    return iconForLinePoints;
  }

  public Color getColor(final CatEMHBranche branche, final PlanimetryBrancheModel model) {
    Color color = null;
    if (model.getBrancheConfigurationExtra() != null) {
      color = model.getBrancheConfigurationExtra().getColor(branche, model, this);
    }
    if (color != null) {
      return color;
    }
    if (!branche.getActuallyActive()) {
      return parent.getInactiveEMHColor();
    }
    return colorByBrancheType.get(branche.getBrancheType());
  }

  @Override
  public void initTraceAtomicIcon(final TraceIconModel iconModel, final int idxPoly, final PlanimetryLigneBriseeLayerModel model) {
    iconModel.updateData(iconForLinePoints);
  }

  @Override
  public void initTraceLigne(final TraceLigneModel ligne, final int idxPoly, final PlanimetryLigneBriseeLayerModel model) {
    final PlanimetryBrancheModel brancheModel = (PlanimetryBrancheModel) model;
    final CatEMHBranche branche = (brancheModel).getLayerController().getBranche(idxPoly);
    ligne.updateData(lineForBranche);
    ligne.setColor(getColor(branche, brancheModel));
    if (brancheModel.getBrancheConfigurationExtra() != null) {
      brancheModel.getBrancheConfigurationExtra().decoreTraceLigne(ligne, model, this);
    }
  }

  public TraceIcon getMiddleIcon(final CatEMHBranche branche, final PlanimetryBrancheModel model, final GrSegment direction) {
    TraceIcon traceIcon = iconModelTypeBranche.buildCopy();
    traceIcon.setCouleur(getColor(branche, model));
    if (model.getBrancheConfigurationExtra() != null) {
      traceIcon = model.getBrancheConfigurationExtra().decoreMiddleIcon(branche, traceIcon, model, this, direction);
    }
    return traceIcon;
  }

  /**
   * utilise pour determiner le clip ecran.
   *
   * @param model le modele des branches
   * @return taille de l'icone a utiliser pour le milieu de la branche
   */
  public int getMiddleIconWidth(final PlanimetryBrancheModel model) {
    int res = iconModelTypeBranche.getTaille();
    if (model.getBrancheConfigurationExtra() != null) {
      res = model.getBrancheConfigurationExtra().getMiddleIconWidth(res, model, this);
    }
    return res;
  }

  public String getDisplayName(final CatEMHBranche branche, final PlanimetryBrancheModel model, final boolean isSelected) {
    if (model.getBrancheConfigurationExtra() != null) {
      final String extraName = model.getBrancheConfigurationExtra().getDisplayName(branche, model, this, isSelected);
      if (extraName != null) {
        return extraName;
      }
    }
    return branche.getNom();
  }
}
