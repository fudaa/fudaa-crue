package org.fudaa.fudaa.crue.planimetry.configuration.node;

import java.util.List;
import org.fudaa.fudaa.crue.planimetry.configuration.CondLimitConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.CondLimitConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerBuilder;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class CondLimitConfigurationNode extends AbstractNode {
  
  public CondLimitConfigurationNode(CondLimitConfiguration condLimiteConfiguration) {
    super(Children.LEAF, Lookups.singleton(condLimiteConfiguration));
    setDisplayName(NbBundle.getMessage(PlanimetryControllerBuilder.class, PlanimetryController.NAME_CQ_CLIMM));
    setShortDescription(NbBundle.getMessage(CondLimitConfigurationNode.class, "CondLimit.shortDescription"));
  }
  
  @Override
  protected Sheet createSheet() {
    Sheet sheet = new Sheet();
    CondLimitConfiguration lookup = getLookup().lookup(CondLimitConfiguration.class);
    List<Set> createSet = CondLimitConfigurationInfo.createSet(lookup);
    for (Set set : createSet) {
      sheet.put(set);
    }
    return sheet;
  }
}
