/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.tracecasier;

import gnu.trove.TObjectIntHashMap;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Frederic Deniger
 */
public class TraceCasierSousModeleContent {

  TObjectIntHashMap<String> traceCasierPositionByName = new TObjectIntHashMap<>();
  List<TraceCasierSousModele> traceCasierSousModeles = new ArrayList<>();

  void addCasierPosition(String traceName, int i) {
    traceCasierPositionByName.put(traceName, i);
  }

  public TObjectIntHashMap<String> getTraceCasierPositionByName() {
    return traceCasierPositionByName;
  }

  public void setTraceCasierPositionByName(
          TObjectIntHashMap<String> traceCasierPositionByName) {
    this.traceCasierPositionByName = traceCasierPositionByName;
  }

  public List<TraceCasierSousModele> getTraceCasierSousModeles() {
    return traceCasierSousModeles;
  }

  public void setTraceCasierSousModeles(
          List<TraceCasierSousModele> traceCasierSousModeles) {
    this.traceCasierSousModeles = traceCasierSousModeles;
  }
}
