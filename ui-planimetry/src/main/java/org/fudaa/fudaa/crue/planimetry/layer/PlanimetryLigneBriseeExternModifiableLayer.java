package org.fudaa.fudaa.crue.planimetry.layer;

import java.io.File;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessource;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.planimetry.controller.ExternLayerFactory;
import org.fudaa.fudaa.crue.planimetry.save.ConfigExternIds;
import org.fudaa.fudaa.crue.planimetry.save.LayerLigneBriseeModifiableControllerSaver;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 *
 * @author deniger
 */
public class PlanimetryLigneBriseeExternModifiableLayer extends AbstractPlanimetryLigneBriseeExternLayer {

  public PlanimetryLigneBriseeExternModifiableLayer(PlanimetryLigneBriseeLayerModel _modele, FSigEditor _editor) {
    super(_modele, _editor);
    setAttributForLabels(PlanimetryExternDrawLayerGroup.DESSINS_LABEL_LIGNES);
  }

  @Override
  public CrueEtudeExternRessource getSaveRessource(File baseDir, String id) {
    LayerLigneBriseeModifiableControllerSaver saver = new LayerLigneBriseeModifiableControllerSaver();
    return saver.getSaveRessource(id, getLayerController(), baseDir);
  }

  @Override
  public void installed() {
    final FSigEditor editor = (FSigEditor) getEditor();
    final EbliActionSimple exportAction = editor.getExportAction();
    setActions(new EbliActionInterface[]{exportAction});
  }

  @Override
  public PlanimetryAdditionalLayerContrat cloneLayer(FSigEditor editor) {
    return ExternLayerFactory.cloneModifiableLayer(this);
  }

  @Override
  public String getTypeId() {
    return ConfigExternIds.LAYER_LINE_MODIFIABLE_SAVE_ID;
  }
}
