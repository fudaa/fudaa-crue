/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.tracecasier;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.fudaa.crue.planimetry.sig.traceprofil.GGPointToTraceProfilProcessor;
import org.openide.util.NbBundle;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Transforme un semis de points en TraceProfils
 *
 * @author Frederic Deniger
 */
public class GGFileToTraceCasierProcessor {
  public static final String ATTRIBUTE_NAME_NOM_CASIER = "Nom_casier";
  private final GISZoneCollectionLigneBrisee lignes;
  private final File dbfFile;
  private final PropertyEpsilon xtEpsilon;
  private final PropertyEpsilon distanceEpsilon;

  public GGFileToTraceCasierProcessor(GISZoneCollectionLigneBrisee lignes, File dbfFile, CrueConfigMetier ccm) {
    this.lignes = lignes;
    this.dbfFile = dbfFile;
    xtEpsilon = ccm.getEpsilon(CrueConfigMetierConstants.PROP_XT);
    distanceEpsilon = ccm.getEpsilon(CrueConfigMetierConstants.PROP_DISTANCE);
  }

  private void validCasiersModels(Map<String, GISAttributeModel> models, CtuluLog log, int nbPoint) {
    for (Map.Entry<String, GISAttributeModel> entry : models.entrySet()) {
      String key = entry.getKey();
      GISAttributeModel model = entry.getValue();
      if (model == null) {
        log.addSevereError(NbBundle.getMessage(GGFileToTraceCasierProcessor.class, "traceCasier.AttributNotFound", key));
        return;
      }
      Class dataClass = model.getAttribute().getDataClass();
      final String name = model.getAttribute().getName();
      if (name.equalsIgnoreCase(ATTRIBUTE_NAME_NOM_CASIER)) {
        Class expectedDataClass = String.class;
        if (!dataClass.equals(expectedDataClass)) {
          log.addSevereError(NbBundle.getMessage(GGFileToTraceCasierProcessor.class, "traceCasier.AttributeWithWrongType", key, dataClass.toString(),
              expectedDataClass.toString()));
        }
        for (int i = 0; i < nbPoint; i++) {
          if (model.getObjectValueAt(i) == null) {
            log.
                addSevereError(NbBundle.getMessage(GGPointToTraceProfilProcessor.class, "traceCasier.AttributeWithEmptyValue", key, Integer.
                    toString(i)));
          }
        }
      }
    }
  }

  private GISAttributeModel findModelLignes(String name) {
    for (int i = lignes.getNbAttributes() - 1; i >= 0; i--) {
      GISAttributeModel attribute = lignes.getModel(i);
      if (name.equalsIgnoreCase(attribute.getAttribute().getName())) {
        return attribute;
      }
    }
    return null;
  }

  public CtuluLogResult<GISZoneCollectionLigneBrisee> toCasier() {
    CtuluLogResult<GISZoneCollectionLigneBrisee> res = new CtuluLogResult<>();
    Map<String, TraceCasierProfilContent> contents = null;
    CtuluLog log = new CtuluLog();
    if (dbfFile.exists()) {
      GGFilesDbfReader dbfReader = new GGFilesDbfReader(dbfFile, xtEpsilon, distanceEpsilon);
      CtuluLogResult<Map<String, TraceCasierProfilContent>> read = dbfReader.read();
      contents = read.getResultat();
      if (read.getLog() != null) {
        log.addAllLogRecord(read.getLog());
      }
    }
    if (contents == null) {
      contents = new HashMap<>();
    }

    Map<String, GISAttributeModel> models = new HashMap<>();
    final String modelNomProfilKey = ATTRIBUTE_NAME_NOM_CASIER;
    GISAttributeModel modelNomCasier = findModelLignes(modelNomProfilKey);
    assert  modelNomCasier!=null;
    models.put(modelNomProfilKey, modelNomCasier);
    res.setLog(log);
    final int nbLines = lignes.getNumGeometries();
    validCasiersModels(models, log, nbLines);
    if (log.containsErrorOrSevereError()) {
      return res;
    }
    GGTraceCasierAttributesController attributesHelper = new GGTraceCasierAttributesController();
    GISZoneCollectionLigneBrisee zone = new GISZoneCollectionLigneBrisee();
    zone.setAttributes(attributesHelper.createCasierAttributes(), null);
    for (int i = 0; i < nbLines; i++) {
      String nom = CruePrefix.addPrefix((String) modelNomCasier.getObjectValueAt(i), EnumCatEMH.CASIER);
      //on ignore les lignes vides.
      if (StringUtils.isBlank(nom)) {
        continue;
      }
      TraceCasierProfilContent content = contents.get(nom);
      zone.addCoordinateSequence(lignes.getCoordinateSequence(i),
          attributesHelper.createObject(nom, content), null);
    }
    res.setResultat(zone);
    return res;
  }
}
