/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.controller;

/**
 *
 * @author Frederic Deniger
 */
public enum GroupeExternSIGType {

  MODIFIABLE,
  UNMODIFIABLE
}
