/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.action;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineSegment;
import org.locationtech.jts.operation.distance.GeometryLocation;
import gnu.trove.TIntObjectHashMap;
import gnu.trove.TIntObjectIterator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLigneBrisee;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.controller.LayerBrancheController;
import org.fudaa.fudaa.crue.planimetry.layer.AbstractPlanimetryLigneBriseeExternLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryBrancheLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryBrancheModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryCasierLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryCasierLayerModel;
import org.openide.util.NbBundle;

/**
 * processor pour projeter la sélection des branches sur une ligne.
 *
 * @author Frederic Deniger
 */
public class ProjectBrancheOnLineProcess {

  private final PlanimetryVisuPanel visuPanel;

  public ProjectBrancheOnLineProcess(PlanimetryVisuPanel visuPanel) {
    this.visuPanel = visuPanel;
  }

  public static GeometryLocation findPositionOnLine(CoordinateSequence line, Coordinate point) {
    Coordinate position = null;
    int idx = -1;
    int size = line.size();
    for (int k = 0; k < size - 1; k++) {
      Coordinate coordinateA = line.getCoordinate(k);
      Coordinate coordinateB = line.getCoordinate(k + 1);
      Coordinate foundPosition = getPosition(point, coordinateA, coordinateB);
      if (foundPosition != null) {
        if (position == null) {
          position = foundPosition;
          idx = k;
        } else {
          double current = position.distance(point);
          double newDistance = foundPosition.distance(point);
          if (newDistance < current) {
            position = foundPosition;
            idx = k;
          }
        }
      }
    }
    //on prend le point le plus proche dans ce cas:
    for (int k = 0; k < size; k++) {
      Coordinate coordinateA = line.getCoordinate(k);
      if (position == null) {
        position = coordinateA;
        idx = 0;
      } else {
        double current = position.distance(point);
        double newDistance = coordinateA.distance(point);
        if (newDistance < current) {
          position = coordinateA;
          idx = k;
        }
      }
    }
    return position == null ? null : new GeometryLocation(null, idx, position);
  }

  public CtuluLog process() {
    if (isBrancheSelected()) {
      GISLigneBrisee selectedLine = getSelectedLine();
      return process(selectedLine);
    } else if (isOnlyOneCasierSelected()) {
      int idx = getCasierLayer().getSelectedIndex()[0];
      final Geometry selectedPolygone = getSelectedPolygone();
      GISPolygone poly = null;
      if (selectedPolygone instanceof GISPolygone) {
        poly = (GISPolygone) selectedPolygone;
      } else if (selectedPolygone instanceof GISLigneBrisee) {
        poly = GISGeometryFactory.INSTANCE.createLinearRing(((GISLigneBrisee) selectedPolygone).getCoordinateSequence());
      }
      ((PlanimetryCasierLayerModel) getCasierLayer().getModelEditable()).setGeometry(idx, poly);
    }
    return new CtuluLog();
  }

  private static Coordinate getPosition(Coordinate p, Coordinate A, Coordinate B) {
    // if start==end, then use pt distance
    if (A.equals(B)) {
      return null;
    }
    // otherwise use comp.graphics.algorithms Frequently Asked Questions method
    /*(1)     	      AC dot AB
     r = ---------
     ||AB||^2
     r has the following meaning:
     r=0 P = A
     r=1 P = B
     r<0 P is on the backward extension of AB
     r>1 P is on the forward extension of AB
     0<r<1 P is interior to AB
     */

    double r = ((p.x - A.x) * (B.x - A.x) + (p.y - A.y) * (B.y - A.y))
            / ((B.x - A.x) * (B.x - A.x) + (B.y - A.y) * (B.y - A.y));

    if (r >= 0 && r <= 1) {
      LineSegment seg = new LineSegment(A, B);
      Coordinate segClosestPoint = seg.closestPoint(p);
      return segClosestPoint;

    }

    return null;
  }

  private GISLigneBrisee getSelectedLine() {
    ZCalqueAffichageDonneesInterface[] targetLayers = visuPanel.getEditor().getSceneEditor().getScene().getTargetLayers();
    for (ZCalqueAffichageDonneesInterface layer : targetLayers) {
      if (layer.getLayerSelection() != null && layer.getLayerSelection().isOnlyOnIndexSelected() && layer instanceof AbstractPlanimetryLigneBriseeExternLayer) {
        int idx = layer.getLayerSelection().getMinIndex();
        AbstractPlanimetryLigneBriseeExternLayer lineLayer = (AbstractPlanimetryLigneBriseeExternLayer) layer;
        GISLigneBrisee res = (GISLigneBrisee) lineLayer.getModelEditable().getGeomData().getGeometry(idx);
        if (!res.isClosed()) {
          return res;
        }
      }
    }
    return null;
  }

  private Geometry getSelectedPolygone() {
    ZCalqueAffichageDonneesInterface[] targetLayers = visuPanel.getEditor().getSceneEditor().getScene().getTargetLayers();
    for (ZCalqueAffichageDonneesInterface layer : targetLayers) {
      if (layer.getLayerSelection() != null && layer.getLayerSelection().isOnlyOnIndexSelected() && layer instanceof AbstractPlanimetryLigneBriseeExternLayer) {
        int idx = layer.getLayerSelection().getMinIndex();
        AbstractPlanimetryLigneBriseeExternLayer lineLayer = (AbstractPlanimetryLigneBriseeExternLayer) layer;
        final Geometry geometry = lineLayer.getModelEditable().getGeomData().getGeometry(idx);
        if (geometry instanceof GISPolygone) {
          return geometry;
        }
        GISPolyligne res = (GISPolyligne) geometry;

        if (res.isClosed()) {
          return res;
        }
      }
    }
    return null;
  }

  protected boolean isEnable() {
    if (isBrancheSelected()) {
      return getCasierLayer().isSelectionEmpty() && getSelectedLine() != null;
    } else {
      return isOnlyOneCasierSelected() && getSelectedPolygone() != null;
    }
  }

  protected CtuluLog process(GISLigneBrisee selectedLine) {
    final LayerBrancheController brancheController = visuPanel.getPlanimetryController().getBrancheController();
    PlanimetryBrancheLayer brancheLayer = brancheController.getLayer();
    visuPanel.getPlanimetryController().getSceneListener().setGlobalChangeStarted(true);
    CtuluLog log = new CtuluLog();
    log.setDesc(NbBundle.getMessage(ProjectBrancheOnLineProcess.class, "ProjectLineOnBranche.LogTitle"));
    try {
      int[] selectedIndex = brancheLayer.getSelectedIndex();
      if (selectedIndex == null) {
        return log;
      }
      TIntObjectHashMap<GISPolyligne> newCoordinateByBrancheIdx = new TIntObjectHashMap<>();
      for (int i = 0; i < selectedIndex.length; i++) {
        int brancheIdx = selectedIndex[i];
        Coordinate amontCoordinate = brancheController.getAmontCoordinate(brancheIdx);
        Coordinate avalCoordinate = brancheController.getAvalCoordinate(brancheIdx);
        CoordinateSequence coordinateSequence = selectedLine.getCoordinateSequence();
        GeometryLocation amontPosition = findPositionOnLine(coordinateSequence, amontCoordinate);
        GeometryLocation avalPosition = findPositionOnLine(coordinateSequence, avalCoordinate);
        if (amontPosition != null && avalPosition != null) {
          List<Coordinate> cs = new ArrayList<>();
          int min = Math.min(amontPosition.getSegmentIndex(), avalPosition.getSegmentIndex());
          int max = Math.max(amontPosition.getSegmentIndex(), avalPosition.getSegmentIndex());
          for (int k = min; k < max; k++) {
            cs.add(new Coordinate(coordinateSequence.getCoordinate(k + 1)));
          }
          if (amontPosition.getSegmentIndex() > avalPosition.getSegmentIndex()) {
            Collections.reverse(cs);
          }
          cs.add(0, new Coordinate(amontPosition.getCoordinate()));
//a verifier que données non pourries:
          cs.add(new Coordinate(avalPosition.getCoordinate()));
          if (cs.size() < 2) {
            log.addError(NbBundle.getMessage(ProjectBrancheOnLineProcess.class, "ProjectrBrancheOnLine.OnePoint.Error", brancheController.getBranche(
                    brancheIdx).getNom()));
          } else {
            CoordinateSequence newSeq = GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(cs.toArray(new Coordinate[0]));
            GISPolyligne newLine = (GISPolyligne) GISGeometryFactory.INSTANCE.createGeometry(GISPolyligne.class, newSeq);
            if (newLine.getLength() < 1e-3) {
              log.addError(NbBundle.getMessage(ProjectBrancheOnLineProcess.class, "ProjectrBrancheOnLine.EmptyLine.Error", brancheController.
                      getBranche(brancheIdx).getNom()));
            } else {
              newCoordinateByBrancheIdx
                      .put(brancheIdx, newLine);
            }
          }
        } else {
          log.addWarn(NbBundle.getMessage(ProjectBrancheOnLineProcess.class, "ProjectBrancheOnLine.CantBeProjected.Warn", brancheController.
                  getBranche(brancheIdx).getNom()));
        }
      }
      if (!log.containsErrorOrSevereError()) {
        TIntObjectIterator<GISPolyligne> iterator = newCoordinateByBrancheIdx.iterator();
        for (int l = newCoordinateByBrancheIdx.size(); l-- > 0;) {
          iterator.advance();
          ((PlanimetryBrancheModel) brancheController.getLayer().getModelEditable()).setGeometry(iterator.key(), iterator.value());
        }
      }
    } finally {
      visuPanel.getPlanimetryController().getSceneListener().setGlobalChangeFinished();
    }
    return log;
  }

  public boolean isBrancheSelected() {
    return !getBrancheLayer().isSelectionEmpty();
  }

  public boolean isOnlyOneCasierSelected() {
    return getCasierLayer().isOnlyOneObjectSelected();
  }

  public PlanimetryCasierLayer getCasierLayer() {
    return visuPanel.getPlanimetryController().getCasierController().getLayer();
  }

  public PlanimetryBrancheLayer getBrancheLayer() {
    return visuPanel.getPlanimetryController().getBrancheController().getLayer();
  }
}
