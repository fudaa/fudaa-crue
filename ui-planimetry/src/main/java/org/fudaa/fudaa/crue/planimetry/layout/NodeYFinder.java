package org.fudaa.fudaa.crue.planimetry.layout;

import gnu.trove.TObjectDoubleHashMap;
import gnu.trove.TObjectDoubleIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.fudaa.fudaa.crue.planimetry.BrancheEdge;
import org.jgrapht.graph.DirectedWeightedMultigraph;

/**
 *
 * @author deniger
 */
public class NodeYFinder {

  public TObjectDoubleHashMap<Long> findYs(DirectedWeightedMultigraph<Long, BrancheEdge> graph) {
    TObjectDoubleHashMap<Long> ys = new TObjectDoubleHashMap<>();
    List<BrancheEdge> edgeSet = new ArrayList<>(graph.edgeSet());
    BrancheEdge firstEdge = edgeSet.get(0);
    Long currentVertex = graph.getEdgeSource(firstEdge);
    ys.put(currentVertex, 0);
    ComputeResult result = new ComputeResult();
    result.ys = ys;
    compute(0, currentVertex, result, graph);
    //on remets a zero
    for (TObjectDoubleIterator it = ys.iterator(); it.hasNext();) {
      it.advance();
      it.setValue(it.value() - result.lower);
    }
    return result.ys;


  }

  private static class ComputeResult {

    TObjectDoubleHashMap<Long> ys;
    double lower;
  }

  private void compute(double currentYs, Long vertex, ComputeResult result,
                       DirectedWeightedMultigraph<Long, BrancheEdge> graph) {
    Set<BrancheEdge> edgesOf = graph.edgesOf(vertex);
    for (BrancheEdge edge : edgesOf) {
      Long vertexAmont = graph.getEdgeSource(edge);
      Long vertexAval = graph.getEdgeTarget(edge);
      double distance = graph.getEdgeWeight(edge);
      if (!vertexAmont.equals(vertex) && !result.ys.contains(vertexAmont)) {
        double newYs = currentYs + distance;
        result.lower = Math.min(newYs, result.lower);
        result.ys.put(vertexAmont, newYs);
        compute(newYs, vertexAmont, result, graph);
      }
      if (!vertexAval.equals(vertex) && !result.ys.contains(vertexAval)) {
        double newYs = currentYs - distance;
        result.lower = Math.min(newYs, result.lower);
        result.ys.put(vertexAval, newYs);
        compute(newYs, vertexAval, result, graph);
      }

    }
  }
}
