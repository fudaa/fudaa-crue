/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.tracecasier;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.geotools.data.shapefile.dbf.DbaseFileReader;
import org.openide.util.NbBundle;

import java.io.File;
import java.io.FileInputStream;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Frederic Deniger
 */
public class GGFilesDbfReader {
  final File dbfFile;
  final PropertyEpsilon xtEpsilon;
  final PropertyEpsilon distanceEpsilon;

  public GGFilesDbfReader(File dbfFile, PropertyEpsilon xtEpsilon, PropertyEpsilon distanceEpsilon) {
    this.dbfFile = dbfFile;
    this.xtEpsilon = xtEpsilon;
    this.distanceEpsilon = distanceEpsilon;
  }

  int getPosition(DbaseFileReader reader, String name, CtuluLog log) {
    int numFields = reader.getHeader().getNumFields();
    for (int i = 0; i < numFields; i++) {
      if (name.equalsIgnoreCase(reader.getHeader().getFieldName(i))) {
        Class readClass = reader.getHeader().getFieldClass(i);
        Class expectedClass = Double.class;
        if (GGFileToTraceCasierProcessor.ATTRIBUTE_NAME_NOM_CASIER.equals(name)) {
          expectedClass = String.class;
        }
        if (Long.class.equals(readClass)) {
          readClass = Double.class;
        }
        if (!expectedClass.equals(readClass)) {
          log.addSevereError(NbBundle.getMessage(GGFileToTraceCasierProcessor.class, "traceCasier.AttributeWithWrongType", name, readClass.toString(),
              expectedClass.toString()));
        }
        return i;
      }
    }
    log.addSevereError(NbBundle.getMessage(GGFilesDbfReader.class, "traceCasier.AttributNotFound", name));
    return -1;
  }

  public CtuluLogResult<Map<String, TraceCasierProfilContent>> read() {
    Map<String, TraceCasierProfilContent> contentByName = new HashMap<>();
    CtuluLog log = new CtuluLog();
    FileInputStream in = null;
    final CtuluLogResult<Map<String, TraceCasierProfilContent>> res = new CtuluLogResult<>(contentByName, log);
    try {
      in = new FileInputStream(dbfFile);
      FileChannel channel = in.getChannel();
      DbaseFileReader r = new DbaseFileReader(channel, true, Charset.forName("UTF-8"));
      int zIdx = getPosition(r, "Cote", log);
      int xtIdx = getPosition(r, "Xt", log);
      int nomIdx = getPosition(r, GGFileToTraceCasierProcessor.ATTRIBUTE_NAME_NOM_CASIER, log);
      int distanceIdx = getPosition(r, "Distance", log);
      if (log.containsErrorOrSevereError()) {
        return res;
      }
      Set<String> warnDoneOnDouble = new HashSet<>();
      while (r.hasNext()) {
        DbaseFileReader.Row row = r.readRow();
        String nom = CruePrefix.addPrefix((String) row.read(nomIdx), EnumCatEMH.CASIER);
        TraceCasierProfilContent content = contentByName.get(nom);
        double distance = ((Number) row.read(distanceIdx)).doubleValue();
        double xt = ((Number) row.read(xtIdx)).doubleValue();
        double z = ((Number) row.read(zIdx)).doubleValue();
        if (content == null) {
          content = new TraceCasierProfilContent(nom, xtEpsilon);
          content.setDistance(distance);
          contentByName.put(nom, content);
        } else if (!distanceEpsilon.isSame(distance, content.getDistance())) {
          log.addWarn(NbBundle.getMessage(GGFilesDbfReader.class, "traceProfilCasier.distanceNotConstant"));
        }
        boolean added = content.add(new TraceCasierProfilContentItem(xt, z));
        if (!added && !warnDoneOnDouble.contains(nom)) {
          warnDoneOnDouble.add(nom);
          log.addWarn(NbBundle.getMessage(GGFilesDbfReader.class, "traceProfilCasier.xtDoublonDetected", nom));
        }
      }
    } catch (Exception fileNotFoundException) {
      log.manageException(fileNotFoundException);
    } finally {
      CtuluLibFile.close(in);
    }
    return res;
  }
}
