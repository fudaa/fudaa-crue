/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import gnu.trove.TIntHashSet;
import org.fudaa.fudaa.crue.planimetry.sig.PointContainer;

/**
 *
 * @author Frederic Deniger
 */
class TraceProfilContentItem implements Comparable<TraceProfilContentItem>, PointContainer {

  double x;
  double y;
  double z;
  double xt;
  //limite maj, sto,...
  //Bord-StoD=1;StoD-MajD	2;MajD-Min	3;Min-MajG	4;MajG-StoG	5;StoG-Bord	6
  private final TIntHashSet limLit = new TIntHashSet();
  //axe geom
  //thalweg	3;axe hydraulique	4
  private final TIntHashSet limGeom = new TIntHashSet();

  @Override
  public int compareTo(TraceProfilContentItem o) {
    if (o == null) {
      return -1;
    }
    return xt > o.xt ? 1 : -1;
  }

  public void copyLimFrom(TraceProfilContentItem other) {
    if (other != null) {
      limLit.addAll(other.getLimLits());
      limGeom.addAll(other.getLimGeom());
    }
  }

  public int[] getLimLits() {
    return limLit.toArray();
  }

  public int[] getLimGeom() {
    return limGeom.toArray();
  }

  @Override
  public double getX() {
    return x;
  }

  @Override
  public double getY() {
    return y;
  }

  public void addLimLit(int limLitIdx) {
    limLit.add(limLitIdx);
  }

  public void addLimGeom(int limGeoIdx) {
    limGeom.add(limGeoIdx);
  }

  boolean containsLit(int limLitIdx) {
    return limLit.contains(limLitIdx);
  }
}
