package org.fudaa.fudaa.crue.planimetry.configuration.node;

import java.util.List;
import org.fudaa.fudaa.crue.planimetry.configuration.SectionConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.SectionConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerBuilder;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class SectionConfigurationNode extends AbstractNode {

  public SectionConfigurationNode(SectionConfiguration sectionConfiguration) {
    super(Children.LEAF, Lookups.singleton(sectionConfiguration));
    setDisplayName(NbBundle.getMessage(PlanimetryControllerBuilder.class, PlanimetryController.NAME_CQ_SECTION));
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = new Sheet();
    SectionConfiguration lookup = getLookup().lookup(SectionConfiguration.class);
    List<Set> createSet = SectionConfigurationInfo.createSet(lookup);
    for (Set set : createSet) {
      sheet.put(set);
    }
    return sheet;
  }
}
