/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.find;

import com.jidesoft.swing.CheckBoxList;
import com.memoire.bu.BuGridLayout;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;
import org.fudaa.dodico.crue.metier.comparator.ToStringInternationalizableComparator;
import org.fudaa.ebli.find.EbliFindComponent;
import org.fudaa.fudaa.crue.common.helper.CheckBoxListPopupListener;
import org.fudaa.fudaa.crue.common.helper.ToStringInternationalizableCellRenderer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.List;

/**
 * composant de recherche
 *
 * @author Frederic Deniger
 */
public class PlanimetryFindComponent implements EbliFindComponent {
  public static final String NAME_AND_OPTIONS = "NAME_AND_OPTIONS";
  private final JTextField txtNomEMH = new JTextField(10);
  private String txtLabel = NbBundle.getMessage(PlanimetryFindComponent.class, "FindEMH.LabelName");
  private String txtTooltip = NbBundle.getMessage(PlanimetryFindComponent.class, "FindEMH.LabelTooltip");
  private final String txtOptions = NbBundle.getMessage(PlanimetryFindComponent.class, "FindEMH.Type");
  private List<ToStringInternationalizable> options;
  private CheckBoxList listOptions;
  private JPanel panel;

  public List<ToStringInternationalizable> getOptions() {
    return options;
  }

  public void setOptions(final List<ToStringInternationalizable> options) {
    this.options = options;
  }

  public void setTxtLabel(final String txtLabel) {
    this.txtLabel = txtLabel;
  }

  public void setTxtTooltip(final String txtTooltip) {
    this.txtTooltip = txtTooltip;
  }

  private String getFindText() {
    return txtNomEMH.getText();
  }

  @Override
  public Object getFindQuery() {
    final FindQuery query = new FindQuery();
    query.setTextToFind(getFindText());
    if (listOptions != null) {
      query.addTypes(listOptions.getCheckBoxListSelectedValues());
    }
    return query;
  }

  @Override
  public JComponent getComponent() {
    if (panel == null) {
      createPanel();
    }
    return panel;
  }

  @Override
  public String getSearchId() {
    return NAME_AND_OPTIONS;
  }

  private void createPanel() {
    panel = new JPanel();
    final BuGridLayout buGridLayout = new BuGridLayout(2, 5, 5, true, false, true, false);
    buGridLayout.setYAlign(0f);
    panel.setLayout(buGridLayout);
    final JLabel jLabel = new JLabel(txtLabel);
    jLabel.setToolTipText(txtTooltip);
    panel.add(jLabel);
    panel.add(txtNomEMH);
    if (options != null) {
      final JLabel lbOption = new JLabel(txtOptions);
      panel.add(lbOption);
      final ToStringInternationalizable[] optionArray = options.toArray(new ToStringInternationalizable[0]);
      Arrays.sort(optionArray, ToStringInternationalizableComparator.INSTANCE);
      listOptions = new CheckBoxList(optionArray);
      new CheckBoxListPopupListener(listOptions);
      final ToStringInternationalizableCellRenderer renderer = new ToStringInternationalizableCellRenderer() {
        @Override
        public Component getListCellRendererComponent(final JList jList, final Object value, final int index, final boolean isSelected, final boolean cellHasFocus) {
          final JComponent res = (JComponent) super.getListCellRendererComponent(jList, value, index, isSelected, cellHasFocus);
          res.setOpaque(false);
          return res;
        }
      };
      renderer.setOpaque(false);
      listOptions.setCellRenderer(renderer);
      listOptions.setOpaque(false);
      panel.add(listOptions);
      listOptions.selectAll();
    }
    SysdocUrlBuilder.installHelpShortcut(panel, SysdocUrlBuilder.getDialogHelpCtxId("bdlRechercher", null));
  }
}
