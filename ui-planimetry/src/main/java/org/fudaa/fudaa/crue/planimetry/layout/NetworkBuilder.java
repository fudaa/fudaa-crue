package org.fudaa.fudaa.crue.planimetry.layout;

import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.model.mxICell;
import com.mxgraph.util.mxPoint;
import com.mxgraph.view.mxGraph;
import gnu.trove.TObjectDoubleHashMap;
import gnu.trove.TObjectIntHashMap;
import org.fudaa.ctulu.CtuluTroveProcedure;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.*;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.planimetry.BrancheEdge;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanelBuilder;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryGisModelContainer;
import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.graph.DirectedMultigraph;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;

import javax.swing.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class NetworkBuilder {
  public PlanimetryVisuPanel buildCalque(final CtuluUI ui, final EMHScenario scenario,
                                         final VisuConfiguration configuration, final CrueConfigMetier configMetier, final boolean modelling) {
    final NetworkGisPositionnerResult buildNewGisData = buildNewGisData(scenario, configuration, configMetier, 0, 0);
    return buildVisuPanel(ui, buildNewGisData, configMetier, configuration, modelling);
  }

  private PlanimetryVisuPanel buildVisuPanel(final CtuluUI ui, final NetworkGisPositionnerResult buildNewGisData,
                                             final CrueConfigMetier configMetier, final VisuConfiguration configuration, final boolean modelling) {
    final PlanimetryVisuPanel res = PlanimetryVisuPanelBuilder.build(ui, buildNewGisData.context, configMetier, configuration, modelling);
    res.getPlanimetryController().setModeles(buildNewGisData.nodes, buildNewGisData.edges, buildNewGisData.casiers);
    res.getPlanimetryController().getTraceController().setInitAngleFromInitData(buildNewGisData.getAnglesBySectionUid());
    return res;
  }

  public PlanimetryVisuPanel loadCalqueFrom(final CtuluUI ui, final EMHScenario scenario, final NetworkGisPositionnerResult loadedData,
                                            final VisuConfiguration configuration, final CrueConfigMetier configMetier, final boolean modelling) {
    checkAllEMHPresent(scenario, loadedData, configuration);
    return buildVisuPanel(ui, loadedData, configMetier, configuration, modelling);
  }

  /**
   *
   * @param scenario le scenario
   * @param visuConfiguration configuration d'affichage
   * @return le resultat de position geoloc des EMH
   */
  public NetworkGisPositionnerResult buildNewGisData(final EMHScenario scenario, final VisuConfiguration visuConfiguration,
                                                     final CrueConfigMetier configMetier, final double xMin, final double yMin) {
    final List<CatEMHBranche> branches = scenario.getBranches();
    final NetworkGisPositionnerResult network = buildNetworkForBranches(branches, scenario, visuConfiguration, xMin, yMin);
    manageNoeudsWithoutBranches(scenario, network, visuConfiguration);
    addCasierIfNeeded(scenario, network, visuConfiguration);
    network.setCcm(configMetier);
    return network;
  }

  /**
   * Construit uniquement les branches et noeuds appartenant à une branche.
   *
   * @param branches liste des branches
   * @param scenario le scenario
   * @param visuConfiguration la configuration pour la création du réseau
   * @param initX le xmin
   * @param initY le ymin
   * @return le reseau construit
   */
  private NetworkGisPositionnerResult buildNetworkForBranches(final List<CatEMHBranche> branches, final EMHScenario scenario,
                                                              final VisuConfiguration visuConfiguration, final double initX, final double initY) {
    final DirectedMultigraph<Long, Long> initGraphe = new DirectedMultigraph<>(Long.class);
    final Set<Long> addedNode = new HashSet<>();
    for (final CatEMHBranche branche : branches) {
      final Long parentNode = branche.getNoeudAmont().getUiId();
      final Long childNode = branche.getNoeudAval().getUiId();
      if (!addedNode.contains(parentNode)) {
        addedNode.add(parentNode);
        initGraphe.addVertex(parentNode);
      }
      if (!addedNode.contains(childNode)) {
        addedNode.add(childNode);
        initGraphe.addVertex(childNode);
      }
      initGraphe.addEdge(parentNode, childNode, branche.getUiId());
    }
    final ConnectivityInspector subGraphes = new ConnectivityInspector(initGraphe);
    final List<Set<Long>> connectedVertices = subGraphes.connectedSets();
    final ConnectedGraphBuilder graphesBuilder = new ConnectedGraphBuilder(connectedVertices, scenario);
    for (final CatEMHBranche branche : branches) {
      graphesBuilder.processBranche(branche, visuConfiguration.getMinimalDistanceBetweenNode());
    }
    final NetworkGisPositionnerResult res = new NetworkGisPositionnerResult(scenario,false);
    final int bigger = graphesBuilder.getBigger();
    if (graphesBuilder.getNb() > 0) {
      final DirectedWeightedMultigraph<Long, BrancheEdge> graph = graphesBuilder.getGraph(bigger);
      Exception ex = buildGisData(graph, res, initX, initY, visuConfiguration);
      if (ex != null) {
        res.getError().addError(org.openide.util.NbBundle.getMessage(NetworkBuilder.class, "buildNetworkFail"));
        Logger.getLogger(NetworkBuilder.class.getName()).log(Level.INFO, "Error while layout", ex);
      }

      final int nb = graphesBuilder.getNb();
      for (int i = 0; i < nb; i++) {
        if (i == bigger) {
          continue;
        }
        final Envelope envelopeInternal = res.edges.getEnvelopeInternal();
        final double xOffset = envelopeInternal.getMaxX() + visuConfiguration.getMinimalDistanceBeetweenParallelEdges();
        ex = buildGisData(graphesBuilder.getGraph(i), res, xOffset, 0, visuConfiguration);
        if (ex != null) {
          res.getError().addError(org.openide.util.NbBundle.getMessage(NetworkBuilder.class, "buildNetworkFail"));
          Logger.getLogger(NetworkBuilder.class.getName()).log(Level.INFO, "Error while layout", ex);
        }
      }
    }
    //les
    return res;
  }

  /**
   * on met à jour les noeuds qui n'ont pas de branches. Cette methode doit toujours être appelée après l'ajout des branches.
   *
   * @param scenario le scenario
   * @param networkGisPositionnerResult resultat de positionnement des EMHS
   * @param visuConfiguration la configuration
   * @return true si modifié
   */
  private boolean manageNoeudsWithoutBranches(final EMHScenario scenario, final NetworkGisPositionnerResult networkGisPositionnerResult,
                                              final VisuConfiguration visuConfiguration) {
    final Set<Long> doneNode = networkGisPositionnerResult.getPresentNodeUid();
    final Envelope envelopeInternal = networkGisPositionnerResult.edges.getEnvelopeInternal();
    final double yMin = envelopeInternal.getMinY();
    final double yMax = Math.max(envelopeInternal.getMaxY(), yMin + 3 * visuConfiguration.getMinimalDistanceBetweenNode());
    double xOffset = envelopeInternal.getMaxX() + visuConfiguration.getMinimalDistanceBetweenNode();

    double yOffset = yMin;
    final List<CatEMHNoeud> noeuds = scenario.getNoeuds();
    boolean modified = false;
    for (final CatEMHNoeud catEMHNoeud : noeuds) {
      if (!doneNode.contains(catEMHNoeud.getUiId())) {
        modified = true;
        if (yOffset > yMax) {
          xOffset += visuConfiguration.getMinimalDistanceBetweenNode();
          yOffset = yMin;
        }
        networkGisPositionnerResult.addNode(xOffset, yOffset, catEMHNoeud);
        yOffset = yOffset + visuConfiguration.getMinimalDistanceBetweenNode();
      }
    }
    return modified;
  }

  /**
   * Verifie que toutes les EMH du scenarios sont dessinées dans le res.
   *
   * @param scenario le scenario
   * @param gisPositionnerResult le resultat de positionnement
   * @param visuConfiguration la configuration
   * @return true si modification
   */
  @SuppressWarnings("Duplicates")
  public boolean checkAllEMHPresent(final EMHScenario scenario, final NetworkGisPositionnerResult gisPositionnerResult, final VisuConfiguration visuConfiguration) {
    //toutes les branches déjà présentes:
    final Set<Long> presentBrancheUid = gisPositionnerResult.getPresentBrancheUid();
    //les noeuds présents
    TObjectIntHashMap<Long> nodePositionByUid = gisPositionnerResult.getNodePositionByUid();
    final List<CatEMHBranche> newBranches = new ArrayList<>();
    boolean modified = false;
    for (final CatEMHBranche branche : scenario.getBranches()) {
      if (presentBrancheUid.contains(branche.getUiId())) {
        continue;
      }
      modified = true;
      final CatEMHNoeud noeudAmont = branche.getNoeudAmont();
      final Long noeudAmontUid = noeudAmont.getUiId();
      final CatEMHNoeud noeudAval = branche.getNoeudAval();
      final Long noeudAvalUid = noeudAval.getUiId();
      final int posAmont = nodePositionByUid.contains(noeudAmontUid) ? nodePositionByUid.get(noeudAmontUid) : -1;
      final int posAval = nodePositionByUid.contains(noeudAvalUid) ? nodePositionByUid.get(noeudAvalUid) : -1;
      final boolean amontExists = posAmont >= 0;
      final boolean avalExists = posAval >= 0;
      //les 2 points existent: on ajoute la branche

      if (amontExists && avalExists) {
        final GISPoint amont = gisPositionnerResult.getNodes().get(posAmont);
        final GISPoint aval = gisPositionnerResult.getNodes().get(posAval);
        gisPositionnerResult.addEdge(Arrays.asList((Coordinate) amont.getCoordinate().clone(), (Coordinate) aval.getCoordinate().clone()), branche);
      } else if (avalExists) {
        final double length = Math.max(branche.getLength(), visuConfiguration.getMinimalDistanceBetweenNode());
        final GISPoint aval = gisPositionnerResult.getNodes().get(posAval);
        double xAmont = aval.getX();
        double yAmont = aval.getY() + length;
        //si le nouveau noeud amont est une insertion entre 2 branches, on le place au milieu
        if (!noeudAmont.getBranchesAmont().isEmpty()) {
          //on prend la première branche
          final CatEMHNoeud noeudAmontAmont = noeudAmont.getBranchesAmont().get(0).getNoeudAmont();
          if (noeudAmontAmont != null && nodePositionByUid.contains(noeudAmontAmont.getUiId())) {
            final GISPoint amontAmont = gisPositionnerResult.getNodes().get(nodePositionByUid.get(noeudAmontAmont.getUiId()));
            if (amontAmont != null) {
              xAmont = (aval.getX() + amontAmont.getX()) / 2d;
              yAmont = (aval.getY() + amontAmont.getY()) / 2d;
            }
          }
        }
        gisPositionnerResult.addNode(xAmont, yAmont, noeudAmont);
        gisPositionnerResult.addEdge(Arrays.asList(new Coordinate(xAmont, yAmont), (Coordinate) aval.getCoordinate().clone()), branche);
        //on met à jour les positions des noeuds:
        nodePositionByUid = gisPositionnerResult.getNodePositionByUid();
      } else if (amontExists) {
        final double length = Math.max(branche.getLength(), visuConfiguration.getMinimalDistanceBetweenNode());
        final GISPoint amont = gisPositionnerResult.getNodes().get(posAmont);
        double xAval = amont.getX();
        double yAval = amont.getY() - length;
        //si le nouveau noeud aval est une insertion entre 2 branches, on le place au milieu
        if (!noeudAval.getBranchesAval().isEmpty()) {
          final CatEMHNoeud noeudAvalAval = noeudAval.getBranchesAval().get(0).getNoeudAval();
          if (noeudAvalAval != null && nodePositionByUid.contains(noeudAvalAval.getUiId())) {
            final GISPoint avalAval = gisPositionnerResult.getNodes().get(nodePositionByUid.get(noeudAvalAval.getUiId()));
            if (avalAval != null) {
              xAval = (amont.getX() + avalAval.getX()) / 2d;
              yAval = (amont.getY() + avalAval.getY()) / 2d;
            }
          }
        }
        gisPositionnerResult.addNode(xAval, yAval, noeudAval);
        gisPositionnerResult.addEdge(Arrays.asList((Coordinate) amont.getCoordinate().clone(), new Coordinate(xAval, yAval)), branche);
        //on met à jour les positions des noeuds:
        nodePositionByUid = gisPositionnerResult.getNodePositionByUid();
      } else {
        //les nouvelles branches s'appuyant sur des nouveaux noeud sont traitées plus tards.
        newBranches.add(branche);
      }
    }
    final Envelope envelopeInternal = gisPositionnerResult.getNodes().getEnvelopeInternal();
    final double initX = envelopeInternal.getMaxX() + visuConfiguration.getMinimalDistanceBeetweenParallelEdges();
    final double initY = envelopeInternal.getMinY();
    if (!newBranches.isEmpty()) {
      final NetworkGisPositionnerResult buildNetworkForBranches = buildNetworkForBranches(newBranches, scenario, visuConfiguration, initX,
        initY);
      gisPositionnerResult.mergeFrom(buildNetworkForBranches);
    }

    //on finit avec les 2 dernieres verification:
    modified |= manageNoeudsWithoutBranches(scenario, gisPositionnerResult, visuConfiguration);
    modified |= addCasierIfNeeded(scenario, gisPositionnerResult, visuConfiguration);
    return modified;
  }

  /**
   * On ajoute les casiers non encore présents.
   *
   * @param scenario le scenario
   * @param networkGisPositionnerResult le resultat du positionnent
   * @param visuConfiguration la configuration pour l'affichage
   * @return true si modification
   */
  private boolean addCasierIfNeeded(final EMHScenario scenario, final NetworkGisPositionnerResult networkGisPositionnerResult, final VisuConfiguration visuConfiguration) {
    final GISZoneCollectionPoint nodes = networkGisPositionnerResult.getNodes();
    final Set<Long> presentCasierUid = networkGisPositionnerResult.getPresentCasierUid();
    final Set<Long> casierDone = new HashSet<>();
    boolean modified = false;
    casierDone.addAll(presentCasierUid);
    //on parcourt les points pour ajouter les casiers:
    for (int i = nodes.getNumGeometries() - 1; i >= 0; i--) {
      final Long uid = networkGisPositionnerResult.context.getUid(nodes, i);
      final GISPoint pt = nodes.get(i);
      final CatEMHNoeud nd = networkGisPositionnerResult.context.getEMH(uid);
      final CatEMHCasier casier = nd.getCasier();
      if (casier != null && !casierDone.contains(casier.getUiId())) {
        casierDone.add(casier.getUiId());
        modified = true;
        final double defaultCasierRealDimension = visuConfiguration.getDefaultCasierRealDimension();
        final double xMin = pt.getX() - defaultCasierRealDimension / 2;
        final double yMin = pt.getY() - defaultCasierRealDimension / 2;
        final double xMax = pt.getX() + defaultCasierRealDimension / 2;
        final double yMax = pt.getY() + defaultCasierRealDimension / 2;
        final GISPolygone rectangle = GISGeometryFactory.INSTANCE.createLinearRing(xMin, xMax, yMin, yMax);
        networkGisPositionnerResult.addCasierAsPolygone(rectangle, casier);
      }
    }

    //un casier a toujours un noeud.
    return modified;
  }

  private Exception buildGisData(final DirectedWeightedMultigraph<Long, BrancheEdge> graph, final NetworkGisPositionnerResult res,
                                 final double globalXOffset,
                                 final double globalYOffset, final VisuConfiguration configuration) {
    //on construit un mxGraph pour lui faire faire le layout:
    Exception exceptionCaught = null;
    final mxGraph mxGraphe = new mxGraph();
    final NodeYFinder ys = new NodeYFinder();
    final TObjectDoubleHashMap<Long> findYs = ys.findYs(graph);
    final Set<Long> vertexSet = graph.vertexSet();
    final Map<Long, mxICell> mxCellByNoeudId = new HashMap<>();
    final Map<Long, mxICell> mxCellByBrancheId = new HashMap<>();
    for (final Iterator<Long> it = vertexSet.iterator(); it.hasNext(); ) {
      final Long vertexId = it.next();
      final double y = findYs.get(vertexId);
      final mxICell insertVertex = (mxICell) mxGraphe.insertVertex(mxGraphe.getDefaultParent(), vertexId.toString(), vertexId, 0, y, 1,
        1,
        null);
      mxCellByNoeudId.put(vertexId, insertVertex);
    }
    final Set<BrancheEdge> edgeSet = graph.edgeSet();
    for (final BrancheEdge brancheEdge : edgeSet) {
      final Long id = brancheEdge.getId();
      final mxICell insertEdge = (mxICell) mxGraphe.insertEdge(mxGraphe.getDefaultParent(), id.toString(), id,
        mxCellByNoeudId.get(graph.getEdgeSource(
          brancheEdge)),
        mxCellByNoeudId.get(graph.getEdgeTarget(brancheEdge)),
        null);
      insertEdge.getGeometry().setHeight(graph.getEdgeWeight(brancheEdge));
      mxCellByBrancheId.put(id, insertEdge);
    }
    final mxHierarchicalLayout layout = new mxHierarchicalLayout(mxGraphe);
    layout.setOrientation(SwingConstants.NORTH);
    layout.setParallelEdgeSpacing(configuration.getMinimalDistanceBeetweenParallelEdges());
    layout.setInterHierarchySpacing(configuration.getMinimalDistanceBeetweenParallelEdges());
    layout.setIntraCellSpacing(configuration.getMinimalDistanceBetweenNode());
    layout.setInterRankCellSpacing(configuration.getMinimalDistanceBetweenNode());
    layout.setResizeParent(true);
    try {
      layout.execute(mxGraphe.getDefaultParent());
    } catch (final Exception ex) {
      exceptionCaught = ex;
    }

    final CtuluTroveProcedure.DoubleMaxValue procedure = new CtuluTroveProcedure.DoubleMaxValue();
    findYs.forEachValue(procedure);

    final Map<Long, GISPoint> node = new HashMap<>();
    for (final Entry<Long, mxICell> elem : mxCellByNoeudId.entrySet()) {
      final double x = globalXOffset + elem.getValue().getGeometry().getCenterX();
      final double y = globalYOffset + findYs.get(elem.getKey());
      final GISPoint addNode = res.addNode(x, y, (CatEMHNoeud) res.idRegistry.getEmh(elem.getKey()));
      node.put(elem.getKey(), addNode);
    }

    for (final BrancheEdge brancheEdge : edgeSet) {
      final Long id = brancheEdge.getId();
      final mxICell brancheCell = mxCellByBrancheId.get(id);
      final Long sourceVertex = graph.getEdgeSource(brancheEdge);
      final Long targetVertex = graph.getEdgeTarget(brancheEdge);
      final mxGeometry geometry = brancheCell.getGeometry();
      final List<Coordinate> finalCoordinate = new ArrayList<>();
      final Coordinate amontRealCoordinate = node.get(sourceVertex).getCoordinate();
      final Coordinate avalRealCoordinate = node.get(targetVertex).getCoordinate();
      finalCoordinate.add(amontRealCoordinate);
      final List<mxPoint> points = geometry.getPoints();
      if (points != null) {
        final mxPoint targetPoint = points.get(points.size() - 1);
        final mxPoint sourcePoint = points.get(0);
        final double yRatio = (avalRealCoordinate.y - amontRealCoordinate.y) / (targetPoint.getY() - sourcePoint.getY());
        for (int i = 1; i < points.size() - 1; i++) {
          final mxPoint pt = points.get(i);
          final double y = amontRealCoordinate.y + (pt.getY() - sourcePoint.getY()) * yRatio;
          final Coordinate c = new Coordinate(pt.getX(), y);
          finalCoordinate.add(c);
        }
      }
      finalCoordinate.add(avalRealCoordinate);
      res.addEdge(finalCoordinate, (CatEMHBranche) res.idRegistry.getEmh(id));
    }
    return exceptionCaught;
  }

  public boolean applyLocalChanges(NetworkGisPositionnerResult target, NetworkGisPositionnerResult source, Collection<String> emhNameToUpdate) {
    if (source == null || emhNameToUpdate == null) {
      return false;
    }
    applyLocalChanges(target.getCasiers(), source.getCasiers(), emhNameToUpdate);
    applyLocalChanges(target.getBranches(), source.getBranches(), emhNameToUpdate);
    applyLocalChanges(target.getNodes(), source.getNodes(), emhNameToUpdate);
    return true;
  }

  private boolean applyLocalChanges(GISZoneCollectionGeometry target, GISZoneCollectionGeometry source, Collection<String> emhNameToUpdate) {
    final Map<String, Geometry> geometryToUpdate = getGeometryToUpdate(source, emhNameToUpdate);
    GISAttributeModel names = target.getModel(PlanimetryGisModelContainer.NOM_EMH_POSITION);
    boolean modified = false;
    for (int i = 0; i < names.getSize(); i++) {
      final Object name = names.getObjectValueAt(i);
      Geometry geom = geometryToUpdate.get(name);
      if (geom != null) {
        target.setGeometry(i, geom, null);
        modified = true;
      }
    }
    return modified;
  }

  private Map<String, Geometry> getGeometryToUpdate(GISZoneCollectionGeometry collectionGeometry, Collection<String> emhNameToUpdate) {
    Map<String, Geometry> res = new HashMap<>();
    GISAttributeModel names = collectionGeometry.getModel(PlanimetryGisModelContainer.NOM_EMH_POSITION);
    for (int i = 0; i < names.getSize(); i++) {
      final Object name = names.getObjectValueAt(i);
      if (emhNameToUpdate.contains(name)) {
        res.put((String) name, collectionGeometry.getGeometry(i));
      }
    }
    return res;
  }

  private static class ConnectedGraphBuilder {
    private final List<Set<Long>> connectedVertices;
    private final EMHScenario scenario;
    final List<DirectedWeightedMultigraph<Long, BrancheEdge>> graphes;

    public ConnectedGraphBuilder(final List<Set<Long>> connectedVertices, final EMHScenario scenario) {
      this.connectedVertices = connectedVertices;
      this.scenario = scenario;
      graphes = new ArrayList<>();
      for (final Set<Long> vertices : connectedVertices) {
        final DirectedWeightedMultigraph<Long, BrancheEdge> graph = new DirectedWeightedMultigraph<>(
          BrancheEdge.class);
        graphes.add(graph);
        for (final Long id : vertices) {
          graph.addVertex(id);
        }
      }
    }

    public DirectedWeightedMultigraph<Long, BrancheEdge> getGraph(final int i) {
      return graphes.get(i);
    }

    public int getNb() {
      return graphes.size();
    }

    public int getBigger() {
      if (graphes.isEmpty()) {
        return -1;
      }
      int res = 0;
      int maxNbEdges = graphes.get(0).edgeSet().size();
      for (int i = 1; i < graphes.size(); i++) {
        final int nbEdges = graphes.get(i).edgeSet().size();
        if (nbEdges > maxNbEdges) {
          maxNbEdges = nbEdges;
          res = i;
        }
      }
      return res;
    }

    void processBranche(final CatEMHBranche branche, final double minLength) {
      final DirectedWeightedMultigraph<Long, BrancheEdge> graph = findGraph(branche.getNoeudAmont().getUiId());
      if (graph == null) {
        return;
      }
      final BrancheEdge edge = new BrancheEdge(branche.getUiId());
      graph.addEdge(branche.getNoeudAmont().getUiId(), branche.getNoeudAval().getUiId(), edge);
      final double w = Math.max(minLength, branche.getLength());
      graph.setEdgeWeight(edge, w);
    }

    private DirectedWeightedMultigraph<Long, BrancheEdge> findGraph(final Long noeudAmontId) {
      final int nb = connectedVertices.size();
      for (int i = 0; i < nb; i++) {
        if (connectedVertices.get(i).contains(noeudAmontId)) {
          return graphes.get(i);
        }
      }
      return null;
    }
  }
}
