package org.fudaa.fudaa.crue.planimetry.configuration.node;

import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.Lookup;

/**
 *
 * @author deniger
 */
public abstract class AdditionalConfigurationNode extends AbstractNode {

  public AdditionalConfigurationNode(final Children children, final Lookup lookup) {
    super(children, lookup);
  }


  public abstract void applyModification();
}
