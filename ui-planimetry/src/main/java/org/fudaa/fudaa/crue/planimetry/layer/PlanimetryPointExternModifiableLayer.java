package org.fudaa.fudaa.crue.planimetry.layer;

import java.io.File;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessource;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.crue.planimetry.controller.ExternLayerFactory;
import org.fudaa.fudaa.crue.planimetry.save.ConfigExternIds;
import org.fudaa.fudaa.crue.planimetry.save.LayerPointModifiableControllerSaver;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 *
 * @author deniger
 */
public class PlanimetryPointExternModifiableLayer extends AbstractPlanimetryPointExternLayer {

  public PlanimetryPointExternModifiableLayer(PlanimetryPointLayerModel _modele, FSigEditor _editor) {
    super(_modele, _editor);
    setAttributForLabels(PlanimetryExternDrawLayerGroup.DESSINS_LABEL_POINTS);
  }

  @Override
  public PlanimetryAdditionalLayerContrat cloneLayer(FSigEditor editor) {
    return ExternLayerFactory.cloneLayerPointModifiable(this);
  }

  @Override
  public boolean isDestructible() {
    return true;
  }

  @Override
  public void installed() {
    setActions(new EbliActionInterface[]{((FSigEditor) getEditor()).getImportAction(), ((FSigEditor) getEditor()).getExportAction()});
  }

  @Override
  public CrueEtudeExternRessource getSaveRessource(File baseDir, String id) {
    LayerPointModifiableControllerSaver save = new LayerPointModifiableControllerSaver();
    return save.getSaveRessource(id, getLayerController(), baseDir);
  }

  @Override
  public String getTypeId() {
    return ConfigExternIds.LAYER_POINT_MODIFIABLE_SAVE_ID;
  }
}
