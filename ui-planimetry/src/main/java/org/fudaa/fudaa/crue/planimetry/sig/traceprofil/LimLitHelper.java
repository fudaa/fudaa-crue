/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import gnu.trove.TIntObjectHashMap;
import gnu.trove.TIntObjectIterator;
import gnu.trove.TObjectIntHashMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierLitNomme;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.config.lit.LitNommeComparator;

/**
 *
 * @author Frederic Deniger
 */
public class LimLitHelper {

  public static final String RD_STOD = "RD-StoD";//1
  public static final String STOD_MAJD = "StoD-MajD";//2
  public static final String MAJD_MIN = "MajD-Min";//3
  public static final String MIN_MAJG = "Min-MajG";//4
  public static final String MAJG_STOG = "MajG-StoG";//5
  public static final String STOG_RG = "StoG-RG";//6

  public static TObjectIntHashMap<String> createMapInverse() {
    TIntObjectHashMap<String> createMap = createMap();
    TObjectIntHashMap<String> res = new TObjectIntHashMap<>();
    for (TIntObjectIterator<String> it = createMap.iterator();
            it.hasNext();) {
      it.advance();
      res.put(it.value(), it.key());
    }
    return res;

  }

  public static TIntObjectHashMap<String> createMap() {
    TIntObjectHashMap<String> keys = new TIntObjectHashMap<>();
    keys.put(1, RD_STOD);
    keys.put(2, STOD_MAJD);
    keys.put(3, MAJD_MIN);
    keys.put(4, MIN_MAJG);
    keys.put(5, MAJG_STOG);
    keys.put(6, STOG_RG);
    return keys;
  }

  public static String getString(int[] idx, TIntObjectHashMap<String> translator) {
    StringBuilder stringBuilder = new StringBuilder();
    for (int i = 0; i < idx.length; i++) {
      int j = idx[i];
      String lit = translator.get(j);
      if (lit != null) {
        if (stringBuilder.length() > 0) {
          stringBuilder.append(';');
        }
        stringBuilder.append(lit);
      }
    }
    return stringBuilder.toString();
  }

  public static LitNomme[] getLitNommes(String id, CrueConfigMetierLitNomme litNommeConfig) {
    List<LitNomme> res = new ArrayList<>();
    String[] split = StringUtils.split(id, ';');
    for (String string : split) {
      LitNomme litNomme = getLitNomme(string, litNommeConfig);
      if (litNomme != null) {
        res.add(litNomme);
      }
    }
    Collections.sort(res, new LitNommeComparator());
    return res.toArray(new LitNomme[0]);

  }

  private static LitNomme getLitNomme(String id, CrueConfigMetierLitNomme litNommeConfig) {
    if (RD_STOD.equals(id)) {
      return litNommeConfig.getStockageStart();
    }
    if (STOD_MAJD.equals(id)) {
      return litNommeConfig.getMajeurStart();
    }
    if (MAJD_MIN.equals(id)) {
      return litNommeConfig.getMineur();
    }
    if (MIN_MAJG.equals(id)) {
      return litNommeConfig.getMajeurEnd();
    }
    if (MAJG_STOG.equals(id)) {
      return litNommeConfig.getStockageEnd();
    }
    //STOG_RG
    return null;
  }
}
