/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import org.locationtech.jts.geom.CoordinateSequence;
import gnu.trove.TLongIntHashMap;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gis.GISAttributeModelObjectInterface;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.ebli.geometrie.GrVecteur;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;

/**
 *
 * @author Frederic Deniger
 */
public class TraceProfilAnglesProcessor {

  private final PlanimetryControllerHelper helper;
  private final TLongIntHashMap positionBySectionUid;

  public TraceProfilAnglesProcessor(PlanimetryControllerHelper helper) {
    this.helper = helper;
    positionBySectionUid = helper.getTraceController().getPositionBySectionUid();
  }

  protected void propagateAngleChange(int traceProfilIdx, CatEMHSection section, GISZoneCollectionLigneBrisee traceProfilGeomData) {

    GISPolyligne traceProfilGeometry = (GISPolyligne) traceProfilGeomData.getGeometry(traceProfilIdx);
    GISAttributeModelObjectInterface limLits = (GISAttributeModelObjectInterface) traceProfilGeomData.getModel(
            GGTraceProfilSectionAttributesController.LIM_LIT).getObjectValueAt(traceProfilIdx);

    double angleStart = 0;
    double angleEnd = 0;
    int nb = limLits.getSize();
    int debLitMineur = LimLitFinder.getDebLimLit(limLits);
    int finLitMineur = LimLitFinder.getEndLimLit(limLits);
    if (debLitMineur > 0 && finLitMineur > 0) {
      int debIdx = -1;
      int finIdx = -1;
      for (int i = debLitMineur - 1; i >= 0; i--) {
        if (StringUtils.isNotBlank((String) limLits.getValue(i))) {
          debIdx = i;
          break;
        }
      }
      for (int i = finLitMineur + 1; i < nb; i++) {
        if (StringUtils.isNotBlank((String) limLits.getValue(i))) {
          finIdx = i;
          break;
        }
      }
      final CoordinateSequence coordinateSequence = traceProfilGeometry.getCoordinateSequence();
      double xDebLitMineur = coordinateSequence.getX(debLitMineur);
      double yDebLitMineur = coordinateSequence.getY(debLitMineur);
      double xFinLitMineur = coordinateSequence.getX(finLitMineur);
      double yFinLitMineur = coordinateSequence.getY(finLitMineur);
      if (debIdx >= 0) {
        GrVecteur vecMinMax = new GrVecteur(-xFinLitMineur + xDebLitMineur, -yFinLitMineur + yDebLitMineur, 0);
        GrVecteur dev = new GrVecteur(-xDebLitMineur + coordinateSequence.getX(debIdx), -yDebLitMineur + coordinateSequence.getY(debIdx), 0);
        angleStart = Math.toDegrees(vecMinMax.getAngleXY(dev));
      }
      if (finIdx >= 0) {
        GrVecteur vecMinMax = new GrVecteur(-xFinLitMineur + xDebLitMineur, -yFinLitMineur + yDebLitMineur, 0);
        GrVecteur dev = new GrVecteur(xFinLitMineur - coordinateSequence.getX(finIdx), yFinLitMineur - coordinateSequence.getY(finIdx), 0);
        angleEnd = Math.toDegrees(vecMinMax.getAngleXY(dev));
      }
    }
    if (positionBySectionUid.contains(section.getUiId())) {
      int traceIdxInLayer = positionBySectionUid.get(section.getUiId());
      helper.getPlanimetryGisModelContainer().setAngleBegin(helper.getTraceController().getGeomData(), traceIdxInLayer, angleStart);
      helper.getPlanimetryGisModelContainer().setAngleEnd(helper.getTraceController().getGeomData(), traceIdxInLayer, angleEnd);
    }

  }
}
