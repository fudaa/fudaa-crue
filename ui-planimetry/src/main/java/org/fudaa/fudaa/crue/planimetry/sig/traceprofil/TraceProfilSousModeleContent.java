/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import gnu.trove.TObjectIntHashMap;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Frederic Deniger
 */
public class TraceProfilSousModeleContent {

  TObjectIntHashMap<String> traceProfilPositionByName = new TObjectIntHashMap<>();
  List<TraceProfilSousModele> traceProfilSousModeles = new ArrayList<>();

  void addTracePosition(String traceName, int i) {
    traceProfilPositionByName.put(traceName, i);
  }

  public TObjectIntHashMap<String> getTraceProfilPositionByName() {
    return traceProfilPositionByName;
  }

  public void setTraceProfilPositionByName(
          TObjectIntHashMap<String> traceProfilPositionByName) {
    this.traceProfilPositionByName = traceProfilPositionByName;
  }

  public List<TraceProfilSousModele> getTraceProfilSousModeles() {
    return traceProfilSousModeles;
  }

  public void setTraceProfilSousModeles(
          List<TraceProfilSousModele> traceProfilSousModeles) {
    this.traceProfilSousModeles = traceProfilSousModeles;
  }
}
