/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.listener;

import java.util.HashSet;
import java.util.Set;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLayerWithEMHContrat;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetrySectionLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryTraceLayer;

/**
 *
 * @author Frederic Deniger
 */
public class SectionTraceSelectionSynchronizer implements ZSelectionListener {

  private final PlanimetryTraceLayer trace;
  private final PlanimetrySectionLayer section;
  private final ZScene scene;
  private boolean updating;

  public SectionTraceSelectionSynchronizer(ZScene scene, PlanimetryTraceLayer trace, PlanimetrySectionLayer section) {
    this.trace = trace;
    this.section = section;
    this.scene = scene;
  }

  public void install() {
    trace.addSelectionListener(this);
    section.addSelectionListener(this);
  }

  @Override
  public void selectionChanged(ZSelectionEvent _evt) {
    if (updating) {
      return;
    }
    updating = true;
    PlanimetryLayerWithEMHContrat source = (PlanimetryLayerWithEMHContrat) _evt.getSource();
    Set<Long> uidSelected = new HashSet<>();
    if (source == section) {
      section.getLayerController().getSelectedEMHs(uidSelected);
    } else {
      trace.getLayerController().getSelectedEMHs(uidSelected);
    }
    if (scene.isRestrictedToCalqueActif()) {
      scene.setCalqueActif(trace);
      trace.getLayerController().setSelectedEMHUids(uidSelected);
    } else {
      if (source == section) {
        trace.getLayerController().setSelectedEMHUids(uidSelected);
      } else {
        section.getLayerController().setSelectedEMHUids(uidSelected);
      }
    }
    scene.fireSelectionEvent();
    updating = false;
  }

}
