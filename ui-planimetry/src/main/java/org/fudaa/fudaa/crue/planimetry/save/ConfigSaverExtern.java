/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.save;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternAdditionalFileSaver;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternDaoConfiguration;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessource;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessourceInfos;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternSaveContent;
import org.fudaa.fudaa.crue.planimetry.controller.AbstractGroupExternController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;

/**
 *
 * @author Frederic Deniger
 */
public class ConfigSaverExtern {

  public static File getDessinDir(File baseDir) {
    return new File(baseDir, "dessins");
  }

  public CrueEtudeExternSaveContent getExternProperties(PlanimetryController planimetryController, File baseDir) {
    List<CrueEtudeExternRessourceInfos> ressources = new ArrayList<>();
    List<AbstractGroupExternController> subGroups = planimetryController.getGroupExternController().getSubGroups();
    int idx = 1;
    List<CrueEtudeExternAdditionalFileSaver> additionnalFileSavers = new ArrayList<>();
    for (AbstractGroupExternController subGroup : subGroups) {
      List<PlanimetryAdditionalLayerContrat> additionnalLayers = subGroup.getAdditionnalLayers();
      for (PlanimetryAdditionalLayerContrat layer : additionnalLayers) {
        final CrueEtudeExternRessource saveRessource = layer.getSaveRessource(baseDir, Integer.toString(idx++));
        ressources.add(saveRessource.getInfos());
        CrueEtudeExternAdditionalFileSaver fileSaver = saveRessource.getFileSaver();
        if (fileSaver != null) {
          additionnalFileSavers.add(fileSaver);
        }
      }
    }
    CrueEtudeExternDaoConfiguration configuration = new CrueEtudeExternDaoConfiguration();
    configuration.setRessources(ressources);
    CrueEtudeExternSaveContent res = new CrueEtudeExternSaveContent();
    res.setAdditionalFileSaver(additionnalFileSavers);
    res.setConfig(configuration);
    return res;
  }
}
