package org.fudaa.fudaa.crue.planimetry.configuration.editor;

import org.fudaa.fudaa.crue.common.editor.CodeTranslationEditorSupport;
import org.fudaa.fudaa.crue.planimetry.configuration.LabelConfigurationInfo;

/**
 *
 * @author deniger
 */
public class LabelAlignmentPropertyEditorSupport extends CodeTranslationEditorSupport {


  public LabelAlignmentPropertyEditorSupport() {
    super(LabelConfigurationInfo.createCodes());
  }

}
