package org.fudaa.fudaa.crue.planimetry.configuration.node;

import org.fudaa.fudaa.crue.planimetry.configuration.PointConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.PointConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryPointLayer;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.lookup.Lookups;

import java.util.List;

/**
 *
 * @author deniger
 */
public class PointConfigurationNode extends AdditionalConfigurationNode {

  public PointConfigurationNode(PointConfiguration imageLayer, PlanimetryPointLayer layer) {
    super(Children.LEAF, Lookups.fixed(imageLayer, layer));
    setDisplayName(layer.getTitle());
  }

  PlanimetryPointLayer getLayer() {
    return getLookup().lookup(PlanimetryPointLayer.class);
  }

  @Override
  public void applyModification() {
    PointConfiguration config = getLookup().lookup(PointConfiguration.class);
    PlanimetryPointLayer layer = getLookup().lookup(PlanimetryPointLayer.class);
    layer.setLayerConfiguration(config.copy());
    layer.repaint();

  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = new Sheet();
    PointConfiguration lookup = getLookup().lookup(PointConfiguration.class);
    List<Set> createSet = PointConfigurationInfo.createSet(lookup);
    for (Set set : createSet) {
      sheet.put(set);
    }
    return sheet;
  }
}
