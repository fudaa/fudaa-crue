package org.fudaa.fudaa.crue.planimetry.controller;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.dodico.crue.metier.emh.EnumCasierType;

import javax.swing.*;
import java.util.List;

/**
 * @author deniger
 */
public class CreationEMHSelectedType {
  private EnumCasierType casierType = EnumCasierType.EMHCasierProfil;
  private EnumBrancheType brancheType = EnumBrancheType.EMHBrancheSaintVenant;

  public EnumBrancheType getBrancheType() {
    return brancheType;
  }

  private void setBrancheType(final EnumBrancheType brancheType) {
    this.brancheType = brancheType;
  }

  public EnumCasierType getCasierType() {
    return casierType;
  }

  private void setCasierType(final EnumCasierType casierType) {
    this.casierType = casierType;
  }

  private JComboBox cbCasier;
  private JComboBox cbBranche;

  public JComboBox getCasierComboBox() {
    if (cbCasier == null) {
      cbCasier = new JComboBox(new Object[]{EnumCasierType.EMHCasierProfil});
      cbCasier.setRenderer(renderer);
      cbCasier.setSelectedItem(casierType);
      cbCasier.addItemListener(e -> setCasierType((EnumCasierType) cbCasier.getSelectedItem()));
    }
    return cbCasier;
  }

  public JComboBox getBrancheComboBox() {
    if (cbBranche == null) {
      final List<EnumBrancheType> values = EnumBrancheType.getAvailablesBrancheType();
      cbBranche = new JComboBox(values.toArray());
      cbBranche.setRenderer(renderer);
      cbBranche.setSelectedItem(brancheType);
      cbBranche.addItemListener(e -> setBrancheType((EnumBrancheType) cbBranche.getSelectedItem()));
    }
    return cbBranche;
  }

  private final CtuluCellTextRenderer renderer = new CtuluCellTextRenderer() {
    @Override
    protected void setValue(final Object value) {
      if (value == null) {
        super.setValue(StringUtils.EMPTY);
      } else {
        super.setValue(((ToStringInternationalizable) value).geti18n());
      }
    }
  };
}
