/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import org.locationtech.jts.geom.LineString;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.ctulu.gis.GISAttributeModelObjectInterface;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.view.MultiEntriesChooser;
import org.fudaa.fudaa.crue.planimetry.controller.AbstractGroupExternController;
import org.fudaa.fudaa.crue.planimetry.controller.LayerBrancheController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternTraceProfilLayer;
import org.openide.util.NbBundle;

/**
 *
 * Permet de controller si la sélection courante est correcte.
 *
 * @author Frederic Deniger
 */
public class SectionProfilToBrancheSelectionController {

  private final PlanimetryControllerHelper helper;

  public SectionProfilToBrancheSelectionController(PlanimetryControllerHelper helper) {
    this.helper = helper;
  }

  /**
   *
   * @return les branches sélectionnées.
   */
  public SectionProfilToBrancheParametersBranche getBrancheSelected() {
    int[] selectedIndex = helper.getBrancheController().getLayer().getSelectedIndex();
    return getBrancheParameterFromSelection(selectedIndex);
  }

  public boolean isActionEnable() {
    BCalque[] traceProfilsCalques = getTraceProfilLayers();
    for (BCalque bCalque : traceProfilsCalques) {
      PlanimetryExternTraceProfilLayer calque = (PlanimetryExternTraceProfilLayer) bCalque;
      if (!calque.isSelectionEmpty()) {
        return true;
      }
    }
    return false;
  }

  protected SectionProfilToBrancheParametersBranche chooseBranche() {
    LayerBrancheController layerController = helper.getBrancheController();
    int nbBranches = layerController.getGeomData().getNumGeometries();
    LinkedHashMap<String, Integer> positionByBranche = new LinkedHashMap<>();
    for (int i = 0; i < nbBranches; i++) {
      CatEMHBranche branche = layerController.getBranche(i);
      if (EnumBrancheType.EMHBrancheSaintVenant.equals(branche.getBrancheType())) {
        positionByBranche.put(branche.getNom(), i);
      }
    }
    MultiEntriesChooser<String> chooser = new MultiEntriesChooser<>(new ArrayList<>(positionByBranche.keySet()));
    boolean ok = DialogHelper.showQuestionOkCancel(NbBundle.getMessage(SectionProfilToBrancheSelectionController.class,
            "traceProfil.ChooseBranche.DialogTitle"), chooser.getPanel());
    if (!ok) {
      return new SectionProfilToBrancheParametersBranche();
    }
    List<String> selected = chooser.getSelectedEntries();
    int[] selectedIdx = new int[selected.size()];
    for (int i = 0; i < selected.size(); i++) {
      selectedIdx[i] = positionByBranche.get(selected.get(i));
    }
    return getBrancheParameterFromSelection(selectedIdx);
  }

  public CtuluLogResult<SectionProfilToBrancheParametersInput> createParametersFromSelection(boolean chooseBrancheIfEmpty) {
    SectionProfilToBrancheParametersInput input = new SectionProfilToBrancheParametersInput();
    SectionProfilToBrancheParametersBranche brancheSelected = getBrancheSelected();
    if (chooseBrancheIfEmpty && brancheSelected.getLigneByBrancheName().isEmpty()) {
      brancheSelected = chooseBranche();
    }
    input.setBranchesParameters(brancheSelected);

    Map<String, List<SectionProfilToBrancheParametersSection>> traceProfilSelected = getTraceProfilSelectedByLayerName();
    Map<String, TreeSet<String>> layersByTrace = new LinkedHashMap<>();
    List<SectionProfilToBrancheParametersSection> sectionParameters = new ArrayList<>();
    for (Map.Entry<String, List<SectionProfilToBrancheParametersSection>> entry : traceProfilSelected.entrySet()) {
      String layerName = entry.getKey();
      List<SectionProfilToBrancheParametersSection> list = entry.getValue();
      sectionParameters.addAll(list);
      for (SectionProfilToBrancheParametersSection item : list) {
        TreeSet<String> layers = layersByTrace.get(item.getTraceName());
        if (layers == null) {
          layers = new TreeSet<>();
          layersByTrace.put(item.getTraceName(), layers);
        }
        layers.add(layerName);
      }
    }
    CtuluLog log = new CtuluLog();
    for (Map.Entry<String, TreeSet<String>> entry : layersByTrace.entrySet()) {
      String traceName = entry.getKey();
      TreeSet<String> treeSet = entry.getValue();
      if (treeSet.size() > 1) {
        log.addSevereError(NbBundle.getMessage(SectionProfilToBrancheSelectionController.class, "traceProfil.sameTraceInDifferentLayer", traceName,
                StringUtils.join(treeSet, "; ")));
      }
    }
    if (log.containsErrorOrSevereError()) {
      return new CtuluLogResult<>(null, log);
    }



    input.setSectionParameters(sectionParameters);
    return new CtuluLogResult<>(input, log);

  }

  /**
   *
   * @return la liste des traceProfil sélectionnées par nom de calque.
   */
  private Map<String, List<SectionProfilToBrancheParametersSection>> getTraceProfilSelectedByLayerName() {
    BCalque[] traceProfilsCalques = getTraceProfilLayers();
    Map<String, List<SectionProfilToBrancheParametersSection>> resMap = new LinkedHashMap<>();
    TraceProfilToSectionNameTransformer transformer = new TraceProfilToSectionNameTransformer();
    for (BCalque bCalque : traceProfilsCalques) {
      PlanimetryExternTraceProfilLayer calque = (PlanimetryExternTraceProfilLayer) bCalque;
      if (!calque.isSelectionEmpty()) {
        List<SectionProfilToBrancheParametersSection> res = new ArrayList<>();
        resMap.put(calque.getTitle(), res);
        final GISZoneCollectionLigneBrisee traceProfilGeomData = calque.getModelEditable().getGeomData();
        GISAttributeModelObjectInterface model = (GISAttributeModelObjectInterface) traceProfilGeomData.getModel(
                GGTraceProfilSectionAttributesController.NOM);
        int[] selectedIndex = calque.getSelectedIndex();
        for (int i = 0; i < selectedIndex.length; i++) {
          int selectedTrace = selectedIndex[i];
          String traceName = (String) model.getObjectValueAt(selectedTrace);
          if (traceName == null) {
            continue;
          }
          final String sectionName = transformer.getSectionName(traceName);
          SectionProfilToBrancheParametersSection param = new SectionProfilToBrancheParametersSection(sectionName);
          param.setTraceName(traceName);
          param.setZone(traceProfilGeomData);
          param.setIdxInCalque(selectedTrace);
          int debLimLit = LimLitFinder.getDebLimLit(selectedTrace, traceProfilGeomData);
          int endLimLit = LimLitFinder.getEndLimLit(selectedTrace, traceProfilGeomData);
          if (debLimLit >= 0 && endLimLit >= 0) {
            final LineString line = (LineString) traceProfilGeomData.getGeometry(selectedTrace);
            GrPoint deb = new GrPoint(line.getCoordinateSequence().getX(debLimLit), line.getCoordinateSequence().getY(debLimLit), 0);
            GrPoint fin = new GrPoint(line.getCoordinateSequence().getX(endLimLit), line.getCoordinateSequence().getY(endLimLit), 0);
            param.setLitMineur(new GrSegment(deb, fin));
            res.add(param);
          }
        }
      }
    }
    return resMap;
  }

  public BCalque[] getTraceProfilLayers() {
    AbstractGroupExternController subController = helper.getController().getGroupExternController().getSubController(
            PlanimetryController.NAME_GC_EXTERN_TRACES);
    BCalque[] traceProfilsCalques = subController.getGroupLayer().getCalques();
    return traceProfilsCalques;
  }

  public SectionProfilToBrancheParametersBranche getBrancheParameterFromSelection(int[] selectedIndex) {
    SectionProfilToBrancheParametersBranche selected = new SectionProfilToBrancheParametersBranche();
    if (selectedIndex != null) {
      for (int i = 0; i < selectedIndex.length; i++) {
        int branchePosition = selectedIndex[i];
        selected.add(helper.getBrancheController().getBranche(branchePosition).getNom(), helper.getBrancheController().getBrancheGis(branchePosition));
      }
    }
    return selected;
  }
}
