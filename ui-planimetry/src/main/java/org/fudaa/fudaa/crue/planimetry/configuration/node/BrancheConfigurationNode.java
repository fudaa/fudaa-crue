package org.fudaa.fudaa.crue.planimetry.configuration.node;

import java.util.List;
import org.fudaa.fudaa.crue.planimetry.configuration.BrancheConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.BrancheConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerBuilder;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class BrancheConfigurationNode extends AbstractNode {

  public BrancheConfigurationNode(BrancheConfiguration brancheConfiguration) {
    super(Children.LEAF, Lookups.singleton(brancheConfiguration));
    setDisplayName(NbBundle.getMessage(PlanimetryControllerBuilder.class, PlanimetryController.NAME_CQ_BRANCHE));
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = new Sheet();
    BrancheConfiguration lookup = getLookup().lookup(BrancheConfiguration.class);
    List<Set> createSet = BrancheConfigurationInfo.createSet(lookup);
    for (Set set : createSet) {
      sheet.put(set);
    }
    return sheet;
  }
}
