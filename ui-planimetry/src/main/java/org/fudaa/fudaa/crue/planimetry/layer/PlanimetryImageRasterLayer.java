package org.fudaa.fudaa.crue.planimetry.layer;

import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessource;
import org.fudaa.ebli.calque.ZCalqueImageRaster;
import org.fudaa.ebli.calque.ZModeleImageRaster;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.fudaa.crue.planimetry.configuration.DefaultConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.node.AdditionalConfigurationNode;
import org.fudaa.fudaa.crue.planimetry.configuration.node.ImageConfigurationNode;
import org.fudaa.fudaa.crue.planimetry.controller.LayerImageController;
import org.fudaa.fudaa.crue.planimetry.save.ConfigExternIds;
import org.fudaa.fudaa.crue.planimetry.save.LayerImageControllerSaver;
import org.fudaa.fudaa.sig.layer.FSigEditor;

import java.awt.*;
import java.io.File;

/**
 *
 * @author deniger
 */
public class PlanimetryImageRasterLayer extends ZCalqueImageRaster implements PlanimetryAdditionalLayerContrat<DefaultConfiguration> {

  DefaultConfiguration layerConfiguration;
  final LayerImageController imageController = new LayerImageController();

  public PlanimetryImageRasterLayer(ZModeleImageRaster _modele) {
    super(_modele);
    layerConfiguration = new DefaultConfiguration();
    imageController.setLayer(this);
  }

  @Override
  public PlanimetryAdditionalLayerContrat cloneLayer(FSigEditor editor) {
    final PlanimetryImageRasterLayer layer = new PlanimetryImageRasterLayer(getModelImage().cloneModel());
    layer.layerConfiguration=layerConfiguration.copy();
    layer.setTitle(getTitle());
    layer.setName(getName());
    return layer;
  }

  public PlanimetryImageRasterLayer() {
  }

  @Override
  public void installed() {
  }

  @Override
  public String getTypeId() {
    return ConfigExternIds.LAYER_IMAGE_SAVE_ID;
  }

  @Override
  public boolean isDestructible() {
    return super.isEditable();
  }

  @Override
  public LayerController getLayerController() {
    return imageController;
  }

  @Override
  public DefaultConfiguration getLayerConfiguration() {
    return layerConfiguration;
  }

  @Override
  public CrueEtudeExternRessource getSaveRessource(File baseDir, String id) {
    return new LayerImageControllerSaver().getSaveRessource(id, imageController, baseDir);
  }

  @Override
  public AdditionalConfigurationNode createConfigurationNode() {
    return new ImageConfigurationNode(layerConfiguration.copy(), this);

  }

  public void setLayerConfigurationNoUndo(DefaultConfiguration layerConfiguration) {
    this.layerConfiguration = layerConfiguration;
    firePropertyChange(PROPERTY_LAYER_CONFIGURATION, null, layerConfiguration);
    repaint();
  }

  public void setLayerConfiguration(DefaultConfiguration layerConfiguration) {
    getLayerController().changeWillBeDone();
    setLayerConfigurationNoUndo(layerConfiguration);
  }

  @Override
  public void paintDonnees(Graphics2D _g, GrMorphisme _versEcran, GrMorphisme _versReel, GrBoite _clipReel) {
    alpha_ = layerConfiguration.getTransparenceAlphaForLayer();
    super.paintDonnees(_g, _versEcran, _versReel, _clipReel);
  }
}
