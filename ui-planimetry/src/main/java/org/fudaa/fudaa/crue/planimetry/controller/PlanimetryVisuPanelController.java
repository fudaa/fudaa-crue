package org.fudaa.fudaa.crue.planimetry.controller;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.Action;
import javax.swing.JComponent;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueInteraction;
import org.fudaa.ebli.calque.BCalqueLegendeCustomizer;
import org.fudaa.ebli.calque.ZCalqueSelectionInteractionSimple;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.edition.SelectionMoveInteraction;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.repere.BControleRepereTexte;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.crue.common.action.ExportImageAction;
import org.fudaa.fudaa.crue.common.action.ExportImageToClipboardAction;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import static org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder.getDialogHelpCtxId;
import org.fudaa.fudaa.crue.planimetry.ConfigureUIAction;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.action.DeleteEMHCascadeAction;
import org.fudaa.fudaa.sig.layer.FSigVisuPanelController;

/**
 *
 * @author deniger
 */
public class PlanimetryVisuPanelController extends FSigVisuPanelController implements ZCalqueSelectionInteractionSimple.EditorDelegate {

  final Collection<String> useAsPalette = Arrays.asList("CHANGE_REFERENCE", "PALETTE_DISTANCE");
  final Collection<String> useAsTab = Arrays.asList("INFOS", "PALETTE_EDTION");
  Collection<EbliActionPaletteAbstract> tabPalettes_ = null;
  final Map<String, EbliActionPaletteAbstract> mapTabPalettes = new HashMap<>(2);

  public PlanimetryVisuPanelController(CtuluUI _ui) {
    super(_ui);
  }

  @Override
  public void setCalqueInteractionActif(BCalqueInteraction _b) {
    super.setCalqueInteractionActif(_b);
    getView().getVueCalque().requestFocus();
  }


  @Override
  public void setView(final ZEbliCalquesPanel view) {
    super.setView(view);
    view.getVueCalque().removeKeyListener(cqCatchI_);
    cqCatchI_.setGele(true);
    final PropertyChangeListener[] propertyChangeListeners = cqCatchI_.getPropertyChangeListeners("gele");
    if (propertyChangeListeners != null) {
      for (PropertyChangeListener propertyChangeListener : propertyChangeListeners) {
        cqCatchI_.removePropertyChangeListener("gele", propertyChangeListener);
      }
    }
    view.getVueCalque().addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent e) {
        super.keyPressed(e);
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
          getCqSelectionI().setGele(false);
        } else if (e.getKeyCode() == KeyEvent.VK_A) {
          cqCatchI_.setGele(false);
        }
      }

      @Override
      public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_A) {
          cqCatchI_.setGele(true);
        }
      }
    });
    new PlanimetryAccrocheController((PlanimetryVisuPanel) view).install();
    SelectionMoveInteraction selection = (SelectionMoveInteraction) getCalqueInteraction("cqSELECTION");
    selection.setEditorDelegate(this);
  }

  @Override
  protected void createCqSelection() {
    cqSelectionI_ = new SelectionMoveInteraction(pn_);
  }

  @Override
  public String edit() {
    BCalque calqueActif = getView().getScene().getCalqueActif();
    PlanimetryVisuPanel panel = (PlanimetryVisuPanel) getView();
    if (panel.getPlanimetryController().isEMHLayer(calqueActif) && panel.getPlanimetryController().getEmhEditor() != null) {
      List<EMH> selectedEMHs = panel.getSelectedEMHs();
      if (selectedEMHs != null && selectedEMHs.size() == 1) {
        panel.getPlanimetryController().getEmhEditor().edit(selectedEMHs.get(0), true);
      }
      return null;
    }

    return getVisuPanel().getEditor().edit();
  }
  DeleteEMHCascadeAction deleteCascadeAction;

  public DeleteEMHCascadeAction getDeleteCascade() {
    if (deleteCascadeAction == null) {
      deleteCascadeAction = new DeleteEMHCascadeAction(getVisuPanel().getEditor());
    }
    return deleteCascadeAction;
  }

  @Override
  public void restaurer() {
    final BCalqueInteraction cq = unsetCurrentInteractifCalque();
    final GrBoite b = pn_.getVueCalque().getCalque().getDomaine();
    if ((b == null) || b.isIndefinie()) {
      return;
    }
    pn_.getVueCalque().changeRepere(this, b, 0, 100, 30, false, false);
    setCalqueInteractionActif(cq);
  }

  @Override
  public Collection<EbliActionPaletteAbstract> getTabPaletteAction() {
    // si deja fait on retourne de suite
    if (tabPalettes_ != null) {
      return tabPalettes_;
    }
    initSpecificActions();
    tabPalettes_ = new ArrayList<>(3);
    for (final String str : useAsTab) {
      final EbliActionPaletteAbstract o = mapTabPalettes.get(str);
      if (o != null) {
        tabPalettes_.add(o);
      }
    }
    return tabPalettes_;
  }

  @Override
  protected void buildButtonGroupNavigation() {
    if (navigationActionGroup_ != null) {
      return;
    }
    super.buildButtonGroupNavigation();
    final EbliActionInterface[] actions = navigationActionGroup_;
    navigationActionGroup_ = removePalettes(actions);
    EbliLib.updateMapKeyStroke(pn_, navigationActionGroup_);
  }

  @Override
  protected void buildButtonGroupSelection() {
    // pas de palette dans la selection
    super.buildButtonGroupSelection();
    EbliLib.updateMapKeyStroke(pn_, selectedActionGroup_);
  }

  @Override
  protected void buildButtonGroupStandard() {
    PlanimetryVisuPanel panel = (PlanimetryVisuPanel) getPn();
    if (standardActionGroup_ != null || panel.getCqLegend() == null) {
      return;
    }
    final ArrayList l = new ArrayList(6);
    EbliActionPaletteAbstract ebliActionPaletteAbstract = new EbliActionPaletteAbstract(EbliLib.getS("Légende"),
            EbliResource.EBLI.getToolIcon("legend"),
            "EDIT_LEGEND", true) {
              @Override
              public JComponent buildContentPane() {
                return new BCalqueLegendeCustomizer(getView().getCqLegend());
              }
            };
    String id = getDialogHelpCtxId("bdlLegende", null);
    SysdocUrlBuilder.installHelpShortcut(ebliActionPaletteAbstract.getPaletteContent(),id);
    ebliActionPaletteAbstract.setParent(pn_);
    l.add(ebliActionPaletteAbstract);
    standardActionGroup_ = new EbliActionInterface[l.size()];
    l.toArray(standardActionGroup_);
  }

  @Override
  public List<EbliActionInterface> getActions() {
    final List<EbliActionInterface> actions = super.getActions();
    actions.add(null);
    actions.add(new ExportImageAction(pn_));
    actions.add(new ExportImageToClipboardAction(pn_));
    return actions;
  }

  @Override
  protected EbliActionInterface[] getApplicationActions() {
    return removePalettes(pn_.getApplicationActions());
  }

  /**
   * @param actions les actions a trier
   * @return la listes des actions a ajouter dans les menus et autres. Les autres ( les palettes) seront visible tout le temps dans des dock.
   */
  private EbliActionInterface[] removePalettes(final EbliActionInterface[] actions) {

    final List<EbliActionInterface> acts = new ArrayList<>(actions.length);

    for (int i = 0; i < actions.length; i++) {
      final EbliActionInterface action = actions[i];
      if (action == null) {
        continue;
      }
      final String value = (String) action.getValue(Action.ACTION_COMMAND_KEY);
      final boolean isPalette = action instanceof EbliActionPaletteAbstract;
      if (ConfigureUIAction.CONFIGURE_ACTION_KEY.equals(value)) {
        ConfigureUIAction configureUI = (ConfigureUIAction) action;
        addEditAction(configureUI);
        configureUI.setEnabled(false);
      }
      if (!isPalette || useAsPalette.contains(value)) {
        acts.add(action);
      }
      if (isPalette && useAsTab.contains(value)) {
        mapTabPalettes.put(value, (EbliActionPaletteAbstract) action);
        ((EbliActionPaletteAbstract) action).setUsedAsTab(true);
      }
    }
    return acts.toArray(new EbliActionInterface[0]);
  }
  final List<JComponent> editionComponents = new ArrayList<>();
  final Map<String, Action> editionAction = new HashMap<>();

  public void addEditionComponent(JComponent jc) {
    editionComponents.add(jc);
    jc.setEnabled(editable);
  }

  @Override
  protected EbliActionInterface createRepereAction() {
    final EbliActionPaletteAbstract plAction = new EbliActionPaletteAbstract(EbliLib.getS("Repère"),
            EbliResource.EBLI.getIcon("repere"),
            "CHANGE_REFERENCE") {
              @Override
              public JComponent buildContentPane() {
                final BControleRepereTexte crt = new BControleRepereTexte(pn_.getVueCalque());
                crt.addRepereEventListener(pn_.getVueCalque());
                SysdocUrlBuilder.installHelpShortcut(crt, SysdocUrlBuilder.getDialogHelpCtxId("bdlRepere", null));
                return crt;
              }
            };
    plAction.setParent(pn_);
    plAction.putValue(Action.SHORT_DESCRIPTION, EbliLib.getS("Transformations du repère"));
    return plAction;
  }


  public void addEditAction(Action action) {
    final String name = (String) action.getValue(Action.ACTION_COMMAND_KEY);
    assert name != null;
    assert !editionAction.containsKey(name);
    editionAction.put(name, action);
  }
  private boolean editable;

  public void setEditable(boolean editable) {
    this.editable = editable;
    for (JComponent jc : editionComponents) {
      jc.setEnabled(editable);
    }
    Collection<Action> values = editionAction.values();
    for (Action action : values) {
      action.setEnabled(editable);
    }
  }
}
