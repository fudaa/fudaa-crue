package org.fudaa.fudaa.crue.planimetry.layer;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeString;
import org.fudaa.ctulu.gis.GISZoneAttributeFactory;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.crue.planimetry.action.SigLoaderAction;
import org.fudaa.fudaa.crue.planimetry.controller.ExternLayerFactory;
import org.fudaa.fudaa.crue.planimetry.controller.GroupeExternSIGType;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.save.ConfigExternIds;
import org.fudaa.fudaa.sig.layer.FSigVisuPanel;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class PlanimetryExternDrawLayerGroup extends AbstractPlanimetryExternLayerGroup {
  public final static GISAttributeString DESSINS_LABEL_LIGNES = new GISAttributeString(CtuluLib.getS("Label"), true) {
    @Override
    public String getID() {
      return "LABEL";
    }
  };
  public final static GISAttributeString DESSINS_LABEL_POINTS = new GISAttributeString(CtuluLib.getS("Label"), false) {
    @Override
    public String getID() {
      return "LABEL";
    }
  };

  public static GISAttributeInterface[] getDefaultAttributesLignesModifiable() {
    return new GISAttributeInterface[]{DESSINS_LABEL_LIGNES};
  }

  public static GISAttributeInterface[] getDefaultAttributesPointsModifiable() {
    return new GISAttributeInterface[]{DESSINS_LABEL_POINTS};
  }
  final PlanimetryController controller;

  public PlanimetryExternDrawLayerGroup(FSigVisuPanel _impl, PlanimetryController controller, String name) {
    super(_impl, null, new String[]{ConfigExternIds.LAYER_LINE_MODIFIABLE_SAVE_ID, ConfigExternIds.LAYER_POINT_MODIFIABLE_SAVE_ID});
    setName(name);
    setTitle(NbBundle.getMessage(PlanimetryExternDrawLayerGroup.class, name));
    this.controller = controller;
    super.buildAddPoint();
    super.buildAddPoly();
    setActions(new EbliActionInterface[]{actionAddPoint_, actionAddPoly_,
      null, new SigLoaderAction(controller, GroupeExternSIGType.MODIFIABLE)});
  }

  @Override
  public GISZoneCollectionLigneBrisee createLigneBriseeZone() {
    return GISZoneAttributeFactory.createPolyligne(visu_.getEditor(), getDefaultAttributesLignesModifiable());
  }

  @Override
  public GISZoneCollectionPoint createPointZone() {
    return GISZoneAttributeFactory.createPoint(visu_.getEditor(), getDefaultAttributesPointsModifiable());
  }

  @Override
  public ZCalquePointEditable addPointLayerAct(final String _title, final GISZoneCollectionPoint _zone,
          final CtuluCommandContainer _cmd) {
    final PlanimetryPointExternModifiableLayer layer = ExternLayerFactory.createLayerPointsModifiable(_zone, getGisEditor());
    layer.setName(BGroupeCalque.findUniqueChildName(this, "point"));
    layer.getLayerController().init(controller.getHelper());
    layer.setTitle(_title);
    finishActionCreation(this, _cmd, layer, false);
    return layer;
  }

  @Override
  public ZCalqueLigneBrisee addLigneBriseeLayerAct(String _title, GISZoneCollectionLigneBrisee _zone, CtuluCommandContainer _cmd) {
    final PlanimetryLigneBriseeExternModifiableLayer layer = ExternLayerFactory.createLayerLignesModifiable(_zone, getGisEditor());
    layer.setName(BGroupeCalque.findUniqueChildName(this, "poly"));
    layer.setTitle(_title);
    layer.getLayerController().init(controller.getHelper());
    finishActionCreation(this, _cmd, layer, false);
    return layer;
  }
}
