package org.fudaa.fudaa.crue.planimetry.layer;

import org.fudaa.fudaa.sig.layer.FSigVisuPanel;
import org.openide.util.NbBundle;

/**
 * Controlleur pour les images.
 *
 * @author deniger
 */
public class PlanimetryExternMainLayerGroup extends AbstractPlanimetryExternLayerGroup {

  public PlanimetryExternMainLayerGroup(FSigVisuPanel _impl, String name) {
    super(_impl, null,null);
    setName(name);
    setTitle(NbBundle.getMessage(PlanimetryExternMainLayerGroup.class, name));
  }
}
