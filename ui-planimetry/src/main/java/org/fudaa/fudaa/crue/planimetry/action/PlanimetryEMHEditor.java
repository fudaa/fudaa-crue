package org.fudaa.fudaa.crue.planimetry.action;

import org.fudaa.dodico.crue.metier.emh.EMH;

/**
 *
 * @author deniger
 */
public interface PlanimetryEMHEditor {

  void edit(EMH emh, boolean reopenFrame);
}
