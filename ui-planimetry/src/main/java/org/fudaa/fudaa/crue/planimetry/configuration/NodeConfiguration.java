package org.fudaa.fudaa.crue.planimetry.configuration;

import java.awt.*;
import javax.swing.SwingConstants;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryNodeLayerModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryPointLayerModel;

/**
 *
 * @author deniger
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class NodeConfiguration extends PointConfiguration {

  final VisuConfiguration parent;
  public static final String PROP_DISPLAY_CASIER_NAME = "displayCasierLabel";
  boolean displayCasierLabel;

  public NodeConfiguration(VisuConfiguration parent) {
    labelConfiguration.setLabelFont(VisuConfiguration.deriveFont(labelConfiguration.getLabelFont(), Font.BOLD, 1));
    labelConfiguration.setLabelAlignment(SwingConstants.EAST);
    labelConfiguration.setLabelDistance(6);
    iconModel.setBackgroundColor(Color.WHITE);
    iconModel.setBackgroundColorPainted(true);
    iconModel.setTypeLigne(TraceLigne.LISSE);
    iconModel.setEpaisseurLigne(1.5f);
    this.parent = parent;
  }

  public NodeConfiguration(NodeConfiguration from, final VisuConfiguration parent) {
    super(from);
    if (from != null) {
      this.displayCasierLabel=from.displayCasierLabel;
    }
    this.parent = parent;
  }

  @Override
  public NodeConfiguration copy() {
    return new NodeConfiguration(this, parent);
  }

  public void setDisplayCasierLabel(boolean displayCasier) {
    this.displayCasierLabel = displayCasier;
  }

  public boolean isDisplayCasierLabel() {
    return displayCasierLabel;
  }

  public String getDisplayedLabel(CatEMHNoeud noeud, PlanimetryNodeLayerModel model, boolean isSelected) {
    String nom = null;
    if (model.getNodeConfigurationExtra() != null) {
      nom = model.getNodeConfigurationExtra().getDisplayedLabel(noeud, this, isSelected);
    }
    if (nom != null) {
      return nom;
    }
    nom = noeud.getNom();
    if (isDisplayCasierLabel()) {
      if (noeud.getCasier() != null) {
        if (noeud.getCasier().getActuallyActive() || isInactiveEMHVisible()) {
          nom = nom + "\n" + noeud.getCasier().getNom();
        }
      }
    }
    return nom;
  }

  public boolean isInactiveEMHVisible() {
    return parent.isInactiveEMHVisible();
  }

  @Override
  public void initTraceIcon(TraceIconModel _ligne, int _idxPoint, PlanimetryPointLayerModel model) {
    PlanimetryNodeLayerModel nodeModel = (PlanimetryNodeLayerModel) model;
    super.initTraceIcon(_ligne, _idxPoint, model);
    CatEMHNoeud noeud = nodeModel.getLayerController().getNoeud(_idxPoint);
    if (!noeud.getActuallyActive()) {
      _ligne.setCouleur(parent.getInactiveEMHColor());
    }
    if (nodeModel.getNodeConfigurationExtra() != null) {
      nodeModel.getNodeConfigurationExtra().decoreTraceIcon(_ligne, noeud, model);
    }
  }

  public VisuConfiguration getParent() {
    return parent;
  }
}
