package org.fudaa.fudaa.crue.planimetry.layer;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.crue.edition.EditionDelete;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.planimetry.configuration.TraceConfigurationExtra;
import org.fudaa.fudaa.crue.planimetry.controller.LayerTraceController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * @author deniger
 */
public class PlanimetryTraceLayerModel extends PlanimetryLigneBriseeLayerModel<LayerTraceController> {
    TraceConfigurationExtra traceConfigurationExtra;

    public PlanimetryTraceLayerModel(GISZoneCollectionLigneBrisee _zone, LayerTraceController modelController) {
        super(_zone, modelController);
    }

    public TraceConfigurationExtra getTraceConfigurationExtra() {
        return traceConfigurationExtra;
    }

    public void setTraceConfigurationExtra(TraceConfigurationExtra traceConfigurationExtra) {
        this.traceConfigurationExtra = traceConfigurationExtra;
    }

    boolean modificationDone;

    @Override
    public void attributeValueChangeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _indexGeom, Object _newValue) {
        super.attributeValueChangeAction(_source, _indexAtt, _att, _indexGeom, _newValue);
        modificationDone = true;
    }

    @Override
    public void modificationDone() {
        if (modificationDone) {
            getLayerController().changeWillBeDone();
            PlanimetryControllerHelper helper = super.getLayerController().getHelper();
            helper.scenarioEditionStarted();
            getLayerController().reset();
            getLayerController().changeDone();
            getLayerController().getLayer().repaint(0);
            modificationDone = false;
        }
    }

    @Override
    public boolean isGeometryVisible(int idx) {
        CatEMHSection section = getLayerController().getEMHFromPositionInModel(idx);
        PlanimetryControllerHelper helper = getLayerController().getHelper();
        return helper.isVisible(section);
    }

    @Override
    public void attributeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _action) {
        super.attributeAction(_source, _indexAtt, _att, _action);
    }

    @Override
    public void objectAction(Object _source, int _idx, Object _obj, int _action) {
        super.objectAction(_source, _idx, _obj, _action);
    }

    @Override
    public boolean removeLigneBrisee(int[] _idx, CtuluCommandContainer _cmd) {
        return removeTraces(_idx, false);
    }

    public boolean removeLigneBriseeCascade(int[] _idx, CtuluCommandContainer _cmd) {
        return removeTraces(_idx, true);
    }

    public boolean removeTraces(int[] _idx, final boolean cascade) {
        PlanimetryControllerHelper helper = super.getLayerController().getHelper();
        if (!helper.canEMHBeAddedAndDisplayError()) {
            return false;
        }
        helper.scenarioEditionStarted();
        List<EMH> sections = new ArrayList<>();
        for (int i = 0; i < _idx.length; i++) {
            sections.add(getLayerController().getEMHFromPositionInModel(_idx[i]));
        }
        final EMHScenario scenario = helper.getPlanimetryGisModelContainer().getScenario();
        //a utiliser
        EditionDelete.RemoveBilan deleted = EditionDelete.delete(sections, scenario, cascade);
        if (deleted.isSomethingRemoved()) {
            getLayerController().getHelper().getSectionController().rebuildGis();
            getLayerController().getHelper().getTraceController().rebuildGis();
        }
        helper.displayBilan(deleted);
        return deleted.isSomethingRemoved();
    }
}
