package org.fudaa.fudaa.crue.planimetry.configuration.editor;

import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import org.fudaa.fudaa.crue.planimetry.configuration.CondLimitConfiguration;
import org.openide.nodes.PropertySupport;

/**
 *
 * @author deniger
 */
public class CondLimitPositionPropertySupport extends PropertySupport.ReadWrite<Integer> {

  final String climType;
  final CondLimitConfiguration config;

  public CondLimitPositionPropertySupport(String climType, String displayName,
          CondLimitConfiguration config) {
    super(climType, Integer.TYPE, displayName, displayName);
    this.climType = climType;
    this.config = config;
  }

  @Override
  public Integer getValue() throws IllegalAccessException, InvocationTargetException {
    return config.getIconPosition(climType);
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    return new LabelAlignmentPropertyEditorSupport();
  }

  @Override
  public void setValue(Integer val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    config.setPosition(climType, val);
  }
}
