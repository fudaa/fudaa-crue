package org.fudaa.fudaa.crue.planimetry.controller;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.gis.GISZoneCollectionGeometry;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.listener.SectionUpdaterFromBranche;

import java.beans.PropertyChangeListener;

/**
 * Un controleur commun pour les traces et section
 *
 * @author deniger
 */
public abstract class AbstractLayerSectionController<L extends ZCalqueAffichageDonnees> extends LayerModelControllerDefault<L> {
  private SectionUpdaterFromBranche sectionUpdaterFromBranche;
  private final PropertyChangeListener zoomChangedListener = evt -> updateIfZoomChanged();
  private double lastUsedOffset = -1;

  /**
   * Ne plus mettre à jour les SIG pendant un changement majeur de géometrie
   */
  private boolean globalChangedOnGoing;

  public void globalChangedFinished() {
    globalChangedOnGoing=false;
    reset();
  }

  public void globalChangedStarted() {
    globalChangedOnGoing=true;
  }

  @Override
  protected void geometryDataUpdated() {
//rien a faire: le modele SIG est reconstruit.
  }

  public void reset() {
    //pour forcer le recalcul
    lastUsedOffset = -1;
  }

  public boolean isBuild() {
    return lastUsedOffset >= 0;
  }

  protected abstract VisuConfiguration getVisuConfiguration();

  protected abstract GISZoneCollectionGeometry getGISCollection();

  protected abstract void setGISCollection(GISZoneCollectionGeometry newGis);

  protected void updateIfZoomChanged() {
    if (lastUsedOffset < 0) {
      rebuildGis();
    } else {
      final GrMorphisme versReel = layer.getVersReel();
      final double rayon = getVisuConfiguration().getNodeConfiguration().getRayon();
      final double newOffset = GrMorphisme.convertDistanceXY(versReel, rayon);
      if (!CtuluLib.isEquals(newOffset, lastUsedOffset, 1e-3)) {
        rebuildGis();
      }
    }
  }

  public CatEMHBranche getBranche(final int position) {
    final Long uid = helper.getBrancheUidForSection(getGISCollection(), position);
    return getEMH(uid);
  }

  public CatEMHSection getSection(final int position) {
    final Long uid = helper.getUid(getGISCollection(), position);
    return getEMH(uid);
  }

  public RelationEMHSectionDansBranche getRelationEMHSectionDansBranche(final int position) {
    return helper.getRelationSectionDansBranche(getGISCollection(), position);
  }

  public void rebuildGis() {
    //on ne reconstruit pas pendant un changement majeur
    if(globalChangedOnGoing){
      return;
    }
    final GrMorphisme versReel = layer.getVersReel();
    final double rayon = getVisuConfiguration().getNodeConfiguration().getRayon();
    lastUsedOffset = GrMorphisme.convertDistanceXY(versReel, rayon);
    final GISZoneCollectionGeometry rebuild = rebuild(lastUsedOffset);
    rebuild.setGeomModifiable(false);
    setGISCollection(rebuild);
  }

  protected abstract GISZoneCollectionGeometry rebuild(double offsetForAmonAvalSection);

  @Override
  public void init(final PlanimetryControllerHelper helper) {
    super.init(helper);
    //pour que les noeuds suivent la branche
    layer.addPropertyChangeListener("versReel", zoomChangedListener);
    sectionUpdaterFromBranche = new SectionUpdaterFromBranche(this);
    helper.getBrancheController().addGISListener(sectionUpdaterFromBranche);
  }
}
