package org.fudaa.fudaa.crue.planimetry.configuration.editor;

import java.awt.Color;
import java.lang.reflect.InvocationTargetException;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.fudaa.crue.planimetry.configuration.BrancheConfiguration;
import org.openide.nodes.PropertySupport;

/**
 *
 * @author deniger
 */
public class BrancheColorPropertySupport extends PropertySupport.ReadWrite<Color> {

  final EnumBrancheType brancheType;
  final BrancheConfiguration config;

  public BrancheColorPropertySupport(EnumBrancheType name,
                                     BrancheConfiguration config) {
    super(name.name(), Color.class, name.geti18n(), name.geti18n());
    this.brancheType = name;
    this.config = config;
  }

  @Override
  public Color getValue() throws IllegalAccessException, InvocationTargetException {
    return config.getColor(brancheType);
  }

  @Override
  public void setValue(Color val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    config.setColor(brancheType, val);
  }
}
