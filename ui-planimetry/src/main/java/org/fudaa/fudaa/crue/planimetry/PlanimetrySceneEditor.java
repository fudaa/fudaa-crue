/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.calque.ZCalqueGeometry;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.calque.edition.ZSceneEditor;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.planimetry.action.LayerWithCascadeDeleteAction;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class PlanimetrySceneEditor extends ZSceneEditor {

  public PlanimetrySceneEditor(ZScene _scene, ZEbliCalquesPanel _calquePanel) {
    super(_scene, _calquePanel);
  }

  @Override
  public void removeSelectedObjects(CtuluCommandContainer _cmd) {
    int nb = getScene().getSelectionHelper().getNbTotalSelectedObject();
    if (nb == 0) {
      return;
    }
    if (isCascadeRemoveActionQuestion()) {
      String delete = NbBundle.getMessage(PlanimetrySceneEditor.class, "DeleteEMH.Name");
      String cancel = NbBundle.getMessage(PlanimetrySceneEditor.class, "Cancel.Name");
      String deleteCascade = NbBundle.getMessage(PlanimetrySceneEditor.class, "DeleteEMHCascade.Name");
      Object showQuestion = DialogHelper.showQuestion(
              NbBundle.getMessage(PlanimetrySceneEditor.class, "DeleteEMH.DialogTitle"),
              NbBundle.getMessage(PlanimetrySceneEditor.class, "DeleteEMH.DialogContent"),
              new Object[]{delete, deleteCascade, cancel});
      if (delete.equals(showQuestion)) {
        super.removeSelectedObjects(_cmd);
      } else if (deleteCascade.equals(showQuestion)) {
        ZCalqueEditable[] cqs = scene_.getEditableLayers();
        final CtuluCommandComposite cmp = new CtuluCommandComposite(
                EbliLib.getS("Supprimer sélection"));
        boolean done = false;
        for (int i = 0; i < cqs.length; i++) {
          if (cqs[i] instanceof LayerWithCascadeDeleteAction && !cqs[i].isSelectionEmpty()) {
            ((LayerWithCascadeDeleteAction) cqs[i]).removeCascadeSelectedObjects();
            done = true;
          }
          done |= cqs[i].removeSelectedObjects(cmp, ui_);
        }
        if (done) {
          final String msg = EbliLib.getS("{0} objet(s) supprimé(s)", Integer.toString(nb));
          FuLog.warning("MOD:" + msg);
          getUI().message(null, msg, true);
        }
        if (_cmd != null) {
          _cmd.addCmd(cmp.getSimplify());
        }
      }

    } else {
      super.removeSelectedObjects(_cmd);
    }
  }

  private boolean isCascadeRemoveActionQuestion() {
    ZCalqueEditable[] cqs = scene_.getEditableLayers();
    if (getScene().getSelectionMode() != ZCalqueGeometry.SelectionMode.NORMAL) {
      return false;
    }
    for (ZCalqueEditable zCalqueEditable : cqs) {
      boolean cascadeLayer = zCalqueEditable.isEditable()
              && !zCalqueEditable.isSelectionEmpty()
              && zCalqueEditable instanceof LayerWithCascadeDeleteAction;
      if (cascadeLayer) {
        return true;
      }

    }
    return false;
  }
}
