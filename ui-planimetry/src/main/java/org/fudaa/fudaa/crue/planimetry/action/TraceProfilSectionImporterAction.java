package org.fudaa.fudaa.crue.planimetry.action;

import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper.EnumFormat;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.sig.traceprofil.GGPointToTraceProfilProcessor;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.NbBundle;

import java.io.File;

/**
 * Action "Importer des TraceProfils"
 *
 * @author deniger
 */
public class TraceProfilSectionImporterAction extends AbstractSigLoaderAction {
  public TraceProfilSectionImporterAction(PlanimetryController controller) {
    super(NbBundle.getMessage(TraceProfilSectionImporterAction.class, "TraceProfilImporterAction.ActionName"), controller);
  }

  @Override
  protected void finishImportData(final Pair<GISZoneCollectionPoint, GISZoneCollectionLigneBrisee> result, File file, EnumFormat fmt) {
    //pas de points à importer: message à l'utilisateur et sortie
    boolean hasPoint = hasPoint(result);
    if (!hasPoint) {
      DialogHelper.showError(getTitle(), getNoResultMessage());
      return;
    }
    final GISZoneCollectionPoint pt = result.first;

    //on va afficher une boite de dialog pour choisir entre Z_IGN et Z_ORTHO
    boolean hasIGN = hasIGNColumn(pt);
    boolean hasORTHO = hasORTHOColumn(pt);

    String attributeForZtoUse = GGPointToTraceProfilProcessor.MODEL_Z_KEY_ORTHO;

    String messageToAdd = null;
    //le cas pas de z du tout est traité par GGPointToTraceProfilProcessor

    //cas ou pas de choix:
    if (hasIGN && !hasORTHO) {
      attributeForZtoUse = GGPointToTraceProfilProcessor.MODEL_Z_KEY_IGN;
      messageToAdd = getMessage("TraceProfilImporterAction.onlyIGN");
    }
    if (hasORTHO && !hasIGN) {
      messageToAdd = getMessage("TraceProfilImporterAction.onlyOrhto");
    }
    //choix possibles:
    if (hasORTHO && hasIGN) {
      attributeForZtoUse = chooseColumnForZ();
      //l'utilisateur a annulé:
      if (attributeForZtoUse == null) {
        return;
      }
    }
    final String titleAction = getMessage("TraceProfilImporterAction.ActionName");

    CtuluLogResult<GISZoneCollectionLigneBrisee> action = convertToTraceProfil(pt, attributeForZtoUse, titleAction);
    if (action == null) {
      DialogHelper.showError(getTitle(), getNoResultMessage());
      return;
    }
    if (action.getLog() != null && action.getLog().isNotEmpty()) {
      LogsDisplayer.displayError(action.getLog(), titleAction);
    }
    if (action.getResultat() != null) {
      controller.getGroupExternController().addTraceProfils(action.getResultat(), file, fmt);
      String message = NbBundle.getMessage(TraceProfilSectionImporterAction.class,
          "TraceProfilImporterAction.Result", action.getResultat().getNumGeometries());
      if (messageToAdd != null) {
        message = "<html><body>" + message + "<br> " + messageToAdd + "</body></html>";
      }
      DialogHelper.showNotifyOperationTermine(
          NbBundle.getMessage(TraceProfilSectionImporterAction.class,
              "TraceProfilImporterAction.ResultTitle", file.getName()),
          message);
    } else {
      DialogHelper.showError(getTitle(), getNoResultMessage());
    }
  }

  private boolean hasPoint(Pair<GISZoneCollectionPoint, GISZoneCollectionLigneBrisee> result) {
    return result != null && result.first != null && result.first.getNumGeometries() > 0;
  }

  private CtuluLogResult<GISZoneCollectionLigneBrisee> convertToTraceProfil(GISZoneCollectionPoint pt, String attributeForZtoUse, String titleAction) {
    final String attributeForZ = attributeForZtoUse == null ? GGPointToTraceProfilProcessor.MODEL_Z_KEY_ORTHO : attributeForZtoUse;
    ProgressRunnable<CtuluLogResult<GISZoneCollectionLigneBrisee>> worker = ph -> {
      ph.switchToIndeterminate();
      return new GGPointToTraceProfilProcessor(pt, controller.getCrueConfigMetier()).toProfil(attributeForZ);
    };

    return CrueProgressUtils.showProgressDialogAndRun(worker, titleAction);
  }

  private String getMessage(String key) {
    return NbBundle.getMessage(TraceProfilSectionImporterAction.class, key);
  }

  private String getNoResultMessage() {
    return NbBundle.getMessage(TraceProfilSectionImporterAction.class,
        "TraceProfilImporterAction.NoResult");
  }

  private boolean hasORTHOColumn(GISZoneCollectionPoint pt) {
    return GGPointToTraceProfilProcessor.findModel(GGPointToTraceProfilProcessor.MODEL_Z_KEY_ORTHO, pt) != null;
  }

  private boolean hasIGNColumn(GISZoneCollectionPoint pt) {
    return GGPointToTraceProfilProcessor.findModel(GGPointToTraceProfilProcessor.MODEL_Z_KEY_IGN, pt) != null;
  }

  /**
   * @return GGPointToTraceProfilProcessor.MODEL_Z_KEY_IGN ou GGPointToTraceProfilProcessor.MODEL_Z_KEY_ORTHO ou null si l'utilisateur choisit "Annuler"
   */
  private String chooseColumnForZ() {
    String title = NbBundle.getMessage(TraceProfilSectionImporterAction.class,
        "TraceProfilImporterAction.chooseAltitudeTitle");
    String message = NbBundle.getMessage(TraceProfilSectionImporterAction.class,
        "TraceProfilImporterAction.chooseAltitudeMessage");
    String chooseOrtho = NbBundle.getMessage(TraceProfilSectionImporterAction.class,
        "TraceProfilImporterAction.chooseAltitudeOrtho");
    String chooseIgn = NbBundle.getMessage(TraceProfilSectionImporterAction.class,
        "TraceProfilImporterAction.chooseAltitudeIGN");
    String chooseNone = NbBundle.getMessage(TraceProfilSectionImporterAction.class,
        "TraceProfilImporterAction.chooseAltitudeNone");

    String selection = (String) DialogHelper.showQuestion(title, message, new String[]{chooseOrtho, chooseIgn, chooseNone});
    if (chooseNone.equals(selection)) {
      return null;
    } else if (chooseIgn.equals(selection)) {
      return GGPointToTraceProfilProcessor.MODEL_Z_KEY_IGN;
    }
    return GGPointToTraceProfilProcessor.MODEL_Z_KEY_ORTHO;
  }
}
