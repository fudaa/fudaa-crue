/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import org.fudaa.ctulu.gis.GISPolyligne;

/**
 *
 * @author Frederic Deniger
 */
public class SectionProfilToBrancheParametersBranche {

  private final Map<String, GISPolyligne> ligneByBrancheName = new LinkedHashMap<>();

  public void add(String brancheName, GISPolyligne ligne) {
    ligneByBrancheName.put(brancheName, ligne);
  }

  public Map<String, GISPolyligne> getLigneByBrancheName() {
    return Collections.unmodifiableMap(ligneByBrancheName);
  }

  boolean isEmpty() {
    return ligneByBrancheName.isEmpty();
  }
}
