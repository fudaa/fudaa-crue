package org.fudaa.fudaa.crue.planimetry.action;

import java.awt.Color;
import javax.swing.Icon;
import javax.swing.JComponent;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.edition.BPaletteEdition;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.crue.planimetry.PlanimetryGisPaletteEdition;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class AddCasierAction extends AbstractAddAction {

  private static Icon createIcon() {
    TraceIconModel model = new TraceIconModel(TraceIcon.CARRE, 10, Color.BLACK);
    model.setBackgroundColor(Color.ORANGE.darker());
    model.setBackgroundColorPainted(true);
    return new TraceIcon(model);
  }

  public AddCasierAction(PlanimetryGisPaletteEdition palette, PlanimetryController controller, EditZone zone) {
    super(palette, controller, zone, NbBundle.getMessage(AddCasierAction.class, "AddCasierAction.Name"), createIcon(),
          "ADD_CASIER");
  }

  @Override
  public String getDefaultPaletteAction() {
    return BPaletteEdition.ADD_RECTANGLE_ACTION;
  }

  @Override
  protected JComponent getEditionComponent() {
    return controller.getHelper().getCreationEMH().getCasierComboBox();
  }

  @Override
  protected ZCalqueAffichageDonnees getLayer() {
    return controller.getCasierController().getLayer();
  }
}
