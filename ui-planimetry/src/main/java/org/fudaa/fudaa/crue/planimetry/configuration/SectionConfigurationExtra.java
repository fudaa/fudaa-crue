/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.configuration;

import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetrySectionLayerModel;

/**
 *
 * @author Frederic Deniger
 */
public interface SectionConfigurationExtra {

  void decorateTraceIcon(CatEMHSection emh, TraceIconModel _ligne, PlanimetrySectionLayerModel sectionModel, SectionConfiguration aThis);

  String getDisplayName(CatEMHSection emh, PlanimetrySectionLayerModel modeleDonnees, SectionConfiguration aThis, boolean selected);

  boolean canLabelsBeAggregate();
}
