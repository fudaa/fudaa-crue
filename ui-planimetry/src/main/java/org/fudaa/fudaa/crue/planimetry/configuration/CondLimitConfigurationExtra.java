/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.configuration;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;

/**
 *
 * @author Frederic Deniger
 */
public interface CondLimitConfigurationExtra {

  String getLabelFor(DonCLimM donCLimM, CrueConfigMetier ccm, boolean longText);
}
