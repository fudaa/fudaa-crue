/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.configuration;

import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.SwingConstants;

import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.openide.util.NbBundle;

/**
 * @author Frederic Deniger
 */
public enum EnumTypeIcon implements ToStringInternationalizable {

  QAPP("qapp", "qapp.png", SwingConstants.NORTH_EAST, CalcTransNoeudQapp.class, CalcPseudoPermNoeudQapp.class, CalcTransNoeudQappExt.class),
  ZIMP("zimp", "zimp.png", SwingConstants.SOUTH_EAST, CalcTransNoeudNiveauContinuLimnigramme.class, CalcPseudoPermNoeudNiveauContinuZimp.class),
  TARAGE("tarage", "tarage.png", SwingConstants.NORTH_WEST, CalcTransNoeudNiveauContinuTarage.class),
  MANOEUVRE("manoeuvre", "manoeuvre.png", SwingConstants.EAST, CalcTransBrancheOrificeManoeuvre.class, CalcPseudoPermBrancheOrificeManoeuvre.class),
  QRUIS("qruis", "qruis.png", SwingConstants.NORTH_EAST, CalcTransBrancheSaintVenantQruis.class, CalcTransCasierProfilQruis.class,
      CalcPseudoPermBrancheSaintVenantQruis.class, CalcPseudoPermCasierProfilQruis.class),
  MANOEUVRE_REGUL("manoeuvre-regul", "manoeuvreregul.png", SwingConstants.EAST, CalcTransBrancheOrificeManoeuvreRegul.class, CalcPseudoPermBrancheOrificeManoeuvreRegul.class),

  USINE("usine", "usine.png", SwingConstants.SOUTH_WEST, CalcTransNoeudUsine.class, CalcPseudoPermNoeudUsi.class),
  BG1("bg1", "bg1.png", SwingConstants.SOUTH_WEST, CalcPseudoPermNoeudBg1.class, CalcTransNoeudBg1.class),
  BG1AV("bg1av", "bg1av.png", SwingConstants.SOUTH_WEST, CalcPseudoPermNoeudBg1Av.class, CalcTransNoeudBg1Av.class),
  BG2("bg2", "bg2.png", SwingConstants.SOUTH_WEST, CalcPseudoPermNoeudBg2.class, CalcTransNoeudBg2.class),
  BG2AV("bg2av", "bg2av.png", SwingConstants.SOUTH_WEST, CalcPseudoPermNoeudBg2Av.class, CalcTransNoeudBg2Av.class);

  private final String iconFile;
  private final String configId;
  private final Set<Class> dclmClasses;
  private final int defaultPosition;

  EnumTypeIcon(String configId, String iconFile, int defaultPosition, Class... classes) {
    this.iconFile = iconFile;
    this.configId = configId;
    this.defaultPosition = defaultPosition;
    dclmClasses = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(classes)));
  }

  @Override
  public String geti18n() {
    return NbBundle.getMessage(getClass(), "condLimite." + configId + ".name");
  }

  @Override
  public String geti18nLongName() {
    return geti18n();
  }

  public int getDefaultPosition() {
    return defaultPosition;
  }

  public Set<Class> getDclmClasses() {
    return dclmClasses;
  }

  public String getConfigId() {
    return configId;
  }

  public static Map<Class, Icon> getIconsByDclm() {
    Map<EnumTypeIcon, Icon> iconsByType = getIconsByType();
    Map<Class, Icon> iconsByDclm = new HashMap<>();
    EnumTypeIcon[] values = EnumTypeIcon.values();
    for (EnumTypeIcon enumTypeIcon : values) {
      for (Class dclmClass : enumTypeIcon.getDclmClasses()) {
        iconsByDclm.put(dclmClass, iconsByType.get(enumTypeIcon));
      }
    }
    return iconsByDclm;
  }

  public static Map<Class, EnumTypeIcon> getEnumByDclm() {
    Map<Class, EnumTypeIcon> iconsByDclm = new HashMap<>();
    EnumTypeIcon[] values = EnumTypeIcon.values();
    for (EnumTypeIcon enumTypeIcon : values) {
      for (Class dclmClass : enumTypeIcon.getDclmClasses()) {
        iconsByDclm.put(dclmClass, enumTypeIcon);
      }
    }
    return iconsByDclm;
  }

  public static Map<EnumTypeIcon, Icon> getIconsByType() {
    Map<EnumTypeIcon, Icon> iconsByType = new EnumMap<>(EnumTypeIcon.class);
    EnumTypeIcon[] values = EnumTypeIcon.values();
    for (EnumTypeIcon enumTypeIcon : values) {
      iconsByType.put(enumTypeIcon, getIcon(enumTypeIcon.iconFile));

    }
    return iconsByType;
  }

  public static Icon getIcon(String name) {
    return CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/planimetry/icons/" + name);
  }
}
