package org.fudaa.fudaa.crue.planimetry.configuration;

import java.util.ArrayList;
import java.util.List;
import javax.swing.SpinnerNumberModel;
import org.fudaa.fudaa.crue.common.config.ConfigurationInfoHelper;
import org.fudaa.fudaa.crue.common.editor.CodeTranslation;
import org.fudaa.fudaa.crue.common.editor.JcomboBoxIntegerInplaceEditor;
import org.fudaa.fudaa.crue.common.editor.SpinnerInplaceEditor;
import org.fudaa.fudaa.crue.planimetry.configuration.editor.CondLimitPositionPropertySupport;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 ** Attention. Les name des set sont utilisés pour la persistence dans le xml. Donc, ne pas changer sans rejouer les tests unitaires et faire des
 * reprises
 *
 * @author deniger
 */
public class CondLimitConfigurationInfo {

  public static final String PREFIX_CONDLIMITE = "condLimite";

  public static List<Sheet.Set> createSet(CondLimitConfiguration in) {
    List<Sheet.Set> res = new ArrayList<>();
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setName(PREFIX_CONDLIMITE);
    set.setDisplayName(NbBundle.getMessage(CondLimitConfigurationInfo.class, "condLimiteConfiguration.DisplayName"));
    set.setShortDescription(NbBundle.getMessage(CondLimitConfigurationInfo.class, "condLimiteConfiguration.Description"));
    set.put(VisuConfigurationInfo.createTransparency(in));
    set.put(createDistance(in));
    res.add(set);
    res.add(createColorSheet(in));
    addLabels(in, res);
    return res;
  }

  private static PropertySupport.Reflection createDistance(CondLimitConfiguration in) {
    try {
      PropertySupport.Reflection res = ConfigurationInfoHelper.create(CondLimitConfiguration.PROP_DISTANCE,
              Integer.TYPE, in, CondLimitConfiguration.class);
      SpinnerInplaceEditor inplaceEditor = new SpinnerInplaceEditor(new SpinnerNumberModel(1, 1, 100, 1));
      res.setValue("inplaceEditor", inplaceEditor);
      return res;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    throw new IllegalAccessError("??");
  }

  public static Sheet.Set createColorSheet(CondLimitConfiguration in) {
    Sheet.Set set = new Set();
    set.setName(PREFIX_CONDLIMITE + ".positions");
    set.setDisplayName(NbBundle.getMessage(CondLimitConfiguration.class, "condLimiteConfiguration.Position"));
    set.setShortDescription(set.getDisplayName());
    EnumTypeIcon[] values = EnumTypeIcon.values();
    CodeTranslation codes = LabelConfigurationInfo.createCodes();
    JcomboBoxIntegerInplaceEditor inplaceEditor = new JcomboBoxIntegerInplaceEditor(codes);

    for (EnumTypeIcon enumTypeIcon : values) {
      CondLimitPositionPropertySupport res = new CondLimitPositionPropertySupport(enumTypeIcon.getConfigId(), enumTypeIcon.geti18n(), in);
      res.setValue("inplaceEditor", inplaceEditor);
      set.put(res);
    }
    return set;
  }

  public static void addLabels(CondLimitConfiguration in, List<Sheet.Set> res) {
    EnumTypeIcon[] values = EnumTypeIcon.values();
    for (EnumTypeIcon enumTypeIcon : values) {

      final String prefix = PREFIX_CONDLIMITE + "." + enumTypeIcon.getConfigId();
      Sheet.Set set = LabelConfigurationInfo.createSet(in.getLabelConfiguration(enumTypeIcon), prefix, true);
      set.setDisplayName(NbBundle.getMessage(CondLimitConfiguration.class, "condLimiteConfiguration.Labels") + " " + enumTypeIcon.geti18n());
      set.setShortDescription(set.getDisplayName());
      res.add(set);
    }
  }
}
