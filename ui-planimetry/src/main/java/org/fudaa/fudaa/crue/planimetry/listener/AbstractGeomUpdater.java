package org.fudaa.fudaa.crue.planimetry.listener;

import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISZoneListener;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;

/**
 * Utilise pour déplacer les branhces si un noeud amon/aval est modifié.
 * @author deniger
 */
public abstract class AbstractGeomUpdater implements GISZoneListener {

  boolean mustUpdate;

  @Override
  public void attributeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _action) {
  }

  @Override
  public void attributeValueChangeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _indexObj,
                                         Object _newValue) {
  }

  protected abstract PlanimetryController getController();
  final TIntArrayList modifiedIdx = new TIntArrayList();

  @Override
  public final void objectAction(Object _source, int indexOfEMH, Object _obj, int _action) {
    if (GISZoneListener.OBJECT_ACTION_MODIFY == _action) {
      if (isMassiveModification()) {
        mustUpdate = true;
        modifiedIdx.add(indexOfEMH);
      } else {
        update(indexOfEMH);
      }
    } else if (GISZoneListener.OBJECT_ACTION_REMOVE == _action) {
      updateAfterRemove();
    } else if (GISZoneListener.OBJECT_ACTION_ADD == _action) {
      updateAfterAdd();

    }
  }

  protected void updateAfterAdd() {
  }

  protected abstract void updateAfterRemove();

  public void updateIfNeeded() {
    if (isMassiveModification()) {
      return;
    }
    if (mustUpdate) {
      updateAll();
      mustUpdate = false;
      modifiedIdx.clear();
    }
  }

  /**
   *
   * @return true si une modification massive est en cours.
   */
  protected boolean isMassiveModification() {
    return getController().getSceneListener().isGlobalChangedStarted();
  }

  protected abstract void updateAll();

  protected abstract void update(int idx);
}
