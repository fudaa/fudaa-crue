package org.fudaa.fudaa.crue.planimetry.action;

import com.memoire.bu.BuFileFilter;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper.EnumFormat;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.sig.SigLoaderHelper;
import org.fudaa.fudaa.sig.wizard.FSigFileLoadResult;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderI;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.windows.WindowManager;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author deniger
 */
public abstract class AbstractSigLoaderAction extends EbliActionSimple {
  final PlanimetryController controller;

  public AbstractSigLoaderAction(final String name, final PlanimetryController controller) {
    super(name, null, name.toUpperCase());
    assert controller != null;
    this.controller = controller;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final Map<EnumFormat, FSigFileLoaderI> mapOfLoader = SigFormatHelper.getLoader();
    final EnumFormat[] values = EnumFormat.values();
    final Map<BuFileFilter, FSigFileLoaderI> loaders = new HashMap<>();
    final Map<BuFileFilter, EnumFormat> fmtByFileFilter = new HashMap<>();
    final List<BuFileFilter> filters = new ArrayList<>();
    for (final EnumFormat enumFormat : values) {
      final FSigFileLoaderI sigFileLoader = mapOfLoader.get(enumFormat);
      final BuFileFilter fileFilter = sigFileLoader.getFileFilter();
      filters.add(fileFilter);
      loaders.put(fileFilter, sigFileLoader);
      fmtByFileFilter.put(fileFilter, enumFormat);
    }
    final Pair<File, BuFileFilter> file = chooseFile(filters);
    if (file != null) {
      final FSigFileLoaderI loader = loaders.get(file.second);
      final EnumFormat fmt = fmtByFileFilter.get(file.second);
      if (loader != null) {
        loadData(loader, file, fmt);
      }
    }
  }

  protected final void loadData(final FSigFileLoaderI loader,
                                final Pair<File, BuFileFilter> file, final EnumFormat fmt) {

    final ProgressRunnable<Pair<GISZoneCollectionPoint, GISZoneCollectionLigneBrisee>> worker = ph -> {
      ph.switchToIndeterminate();
      final FSigFileLoadResult result = new FSigFileLoadResult();
      final CtuluAnalyze analyse = new CtuluAnalyze();
      try {
        loader.setInResult(result, file.first, file.first.getAbsolutePath(), CtuluUIForNetbeans.DEFAULT.getMainProgression(),
            analyse);
      } catch (final IllegalArgumentException ex) {
        analyse.addFatalError(org.openide.util.NbBundle.getMessage(AbstractSigLoaderAction.class, "sig.import.error", ex.getMessage()));
      }

      if (CtuluUIForNetbeans.DEFAULT.manageAnalyzeAndIsFatal(analyse)) {
        return null;
      }
      final boolean hasPoint = result.nbPoint_ > 0;
      final boolean hasLigne = result.nbPolygones_ > 0 || result.nbPolylignes_ > 0;
      GISZoneCollectionPoint pt = null;
      GISZoneCollectionLigneBrisee lignes = null;
      if (hasPoint) {
        pt = SigLoaderHelper.buildPoints(result.createData());
      }
      if (hasLigne) {
        lignes = SigLoaderHelper.buildLine(result.createDataWithAtomic());
      }
      return new Pair<>(pt, lignes);
    };
    final Pair<GISZoneCollectionPoint, GISZoneCollectionLigneBrisee> showProgressDialogAndRun = CrueProgressUtils.showProgressDialogAndRun(worker,
        getTitle());
    finishImportData(showProgressDialogAndRun, file.first, fmt);
  }

  public Pair<File, BuFileFilter> chooseFile(final List<BuFileFilter> fileFilters) {
    final FileChooserBuilder builder = new FileChooserBuilder(AbstractSigLoaderAction.class).setTitle(getTitle());
    builder.setFilesOnly(
        true);
    //a changer pour ne plus dépendre de cela:
    for (final FileFilter fileFilter : fileFilters) {
      builder.addFileFilter(fileFilter);
    }

    builder.setFileFilter(fileFilters.get(0));

    final JFileChooser chooser = builder.createFileChooser();

    chooser.setAcceptAllFileFilterUsed(
        false);
    chooser.setFileFilter(fileFilters.get(0));
    chooser.setMultiSelectionEnabled(
        false);
    final int dlgResult = chooser.showOpenDialog(WindowManager.getDefault().getMainWindow());
    if (JFileChooser.APPROVE_OPTION == dlgResult) {
      File result = chooser.getSelectedFile();
      if (result != null && !result.exists()) {
        result = null;
      }
      return new Pair<>(result, (BuFileFilter) chooser.getFileFilter());
    } else {
      return null;
    }
  }

  protected abstract void finishImportData(Pair<GISZoneCollectionPoint, GISZoneCollectionLigneBrisee> showProgressDialogAndRun, File file,
                                           EnumFormat fmt);
}
