package org.fudaa.fudaa.crue.planimetry.controller;

import gnu.trove.TIntArrayList;
import gnu.trove.TLongIntHashMap;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fudaa.ctulu.gis.GISZoneCollectionGeometry;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.action.OpenEMHAction;
import org.fudaa.fudaa.crue.planimetry.action.SimplifyProfilsAction;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.layer.LayerControllerEMH;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryTraceLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryTraceLayerModel;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class LayerTraceController extends AbstractLayerSectionController<PlanimetryTraceLayer> implements LayerControllerEMH {

  protected static LayerTraceController install(PlanimetryController ctx, PlanimetryVisuPanel res,
          VisuConfiguration configuration, LayerBrancheController brancheController) {
    LayerTraceController traceController = new LayerTraceController();
    PlanimetryTraceLayer traceLayer = new PlanimetryTraceLayer(new PlanimetryTraceLayerModel(
            ctx.gisModel.createTraceCollection(), traceController), res.getEditor());
    traceLayer.setName(PlanimetryController.NAME_CQ_TRACE);
    traceLayer.setTitle(NbBundle.getMessage(PlanimetryControllerBuilder.class, PlanimetryController.NAME_CQ_TRACE));
    traceLayer.setLayerConfiguration(configuration.getTraceConfiguration());
    traceController.layer = traceLayer;
    traceController.layer.setActions(new EbliActionInterface[]{
      new OpenEMHAction.Reopen(ctx),
      new OpenEMHAction.NewFrame(ctx),
      null,
      res.getSigEditAction(),
      null,
      res.getEditor().getExportWithNameAction(),
//      res.getEditor().getExportAction(),
      null,
      res.getEditor().getActionDelete(),
      res.getPlanimetryVisuController().getDeleteCascade(),
      null,
      new SimplifyProfilsAction(ctx),
      null,
      res.createFindAction(traceController.layer)});
    ctx.controllersByName.put(traceLayer.getName(), traceController);
    return traceController;
  }

  @Override
  public List<ToStringInternationalizable> getCategories() {
    return Arrays.asList(EnumSectionType.values());
  }

  @Override
  protected GISZoneCollectionLigneBrisee getGISCollection() {
    return layer.getModelEditable().getGeomData();
  }

  @Override
  public void changeWillBeDone() {
    super.changeWillBeDone();
  }

  @Override
  public void setVisuConfiguration(VisuConfiguration cloned) {
    getLayer().setLayerConfiguration(cloned.getTraceConfiguration());
    rebuildGis();
  }

  @Override
  protected VisuConfiguration getVisuConfiguration() {
    return layer.getVisuConfiguration();
  }

  @Override
  protected void setGISCollection(GISZoneCollectionGeometry newGis) {
    layer.getModelEditable().setGeometries(newGis);
  }

  @Override
  public void cancel() {
    reset();
    super.cancel();
  }

  @Override
  protected GISZoneCollectionLigneBrisee rebuild(double offsetForAmonAvalSection) {
    LayerTraceGisBuilder builder = new LayerTraceGisBuilder(super.helper.getController());
    final GISZoneCollectionLigneBrisee rebuild = builder.rebuild(offsetForAmonAvalSection, anglesBySectionNames);
    anglesBySectionNames = null;
    return rebuild;
  }

  public boolean setSelectedEMHs(List<EMH> selectedEMH) {
    List<EMH> selectEMHS = EMHHelper.selectEMHS(selectedEMH, EnumCatEMH.SECTION);
    TIntArrayList positions = new TIntArrayList();
    if (!selectEMHS.isEmpty()) {
      Set<Long> uids = new HashSet<>();
      for (EMH emh : selectEMHS) {
        uids.add(emh.getUiId());
      }
      GISZoneCollectionLigneBrisee gisCollection = getGISCollection();
      int nb = gisCollection.getNumGeometries();
      for (int i = 0; i < nb; i++) {
        RelationEMHSectionDansBranche relationSectionDansBranche = getHelper().getPlanimetryGisModelContainer().getRelationSectionDansBranche(
                gisCollection, i);
        if (uids.contains(relationSectionDansBranche.getEmh().getUiId())) {
          positions.add(i);
        }
      }
    }
    layer.setSelection(positions.toNativeArray());
    return !positions.isEmpty();
  }

  public boolean setSelectedEMHUids(Collection<Long> uids) {
    TIntArrayList positions = new TIntArrayList();
    if (!uids.isEmpty()) {
      GISZoneCollectionLigneBrisee gisCollection = getGISCollection();
      int nb = gisCollection.getNumGeometries();
      for (int i = 0; i < nb; i++) {
        RelationEMHSectionDansBranche relationSectionDansBranche = getHelper().getPlanimetryGisModelContainer().getRelationSectionDansBranche(
                gisCollection, i);
        if (uids.contains(relationSectionDansBranche.getEmh().getUiId())) {
          positions.add(i);
        }
      }
    }
    layer.setSelection(positions.toNativeArray());
    return !positions.isEmpty();
  }

  @Override
  public EMH getEMHFromLayerPosition(int position) {
    GISZoneCollectionLigneBrisee gisCollection = getGISCollection();
    RelationEMHSectionDansBranche relationSectionDansBranche = getHelper().getPlanimetryGisModelContainer().getRelationSectionDansBranche(
            gisCollection, position);
    return relationSectionDansBranche.getEmh();
  }

  public TLongIntHashMap getPositionBySectionUid() {
    final TLongIntHashMap res = new TLongIntHashMap();
    final GISZoneCollectionLigneBrisee gisCollection = getGISCollection();
    final int size = gisCollection.getNumGeometries();
    for (int i = 0; i < size; i++) {
      RelationEMHSectionDansBranche relationSectionDansBranche = getHelper().getPlanimetryGisModelContainer().getRelationSectionDansBranche(
              gisCollection, i);
      res.put(relationSectionDansBranche.getEmh().getUiId(), i);
    }
    return res;
  }

  @Override
  public void setEditable(boolean editable) {
    this.editable = editable;
    getHelper().getPlanimetryGisModelContainer().setAnglesEditable(editable);
    getLayer().setActionsEnable(editable);
  }

  public void getSelectedEMHs(Collection<Long> uids) {
    if (!layer.isVisible() || layer.isSelectionEmpty()) {
      return;
    }
    int[] selectedIndex = layer.getSelectedIndex();

    if (selectedIndex != null) {
      GISZoneCollectionLigneBrisee gisCollection = getGISCollection();
      for (int i = 0; i < selectedIndex.length; i++) {
        RelationEMHSectionDansBranche relationSectionDansBranche = getHelper().getPlanimetryGisModelContainer().getRelationSectionDansBranche(
                gisCollection, selectedIndex[i]);
        uids.add(relationSectionDansBranche.getEmh().getUiId());
      }
    }
  }
  Map<Long, Pair<Double, Double>> anglesBySectionNames = null;

  public void setInitAngleFromInitData(Map<Long, Pair<Double, Double>> anglesBySectionNames) {
    this.anglesBySectionNames = anglesBySectionNames;
    reset();
  }
}
