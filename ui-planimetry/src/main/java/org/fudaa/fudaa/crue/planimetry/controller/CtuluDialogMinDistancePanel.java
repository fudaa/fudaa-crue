package org.fudaa.fudaa.crue.planimetry.controller;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class CtuluDialogMinDistancePanel extends CtuluDialogPanel implements KeyListener {
  
  final BuTextField distanceMini;
  
  public CtuluDialogMinDistancePanel(double initValue) {
    super(true);
    setLayout(new BuGridLayout(2, 5, 5));
    distanceMini = addLabelDoubleText(org.openide.util.NbBundle.getMessage(CtuluDialogMinDistancePanel.class,
                                                                           "DistanceMin.Distance.Text"));
    distanceMini.setValue(initValue);
    distanceMini.addKeyListener(this);
  }
  
  @Override
  public void keyPressed(KeyEvent e) {
    updateError();
  }
  
  @Override
  public void keyReleased(KeyEvent e) {
    updateError();
  }
  
  @Override
  public void keyTyped(KeyEvent e) {
    updateError();
  }
  
  private void updateError() {
    if (distanceMini.getValue() == null) {
      setErrorText(NbBundle.getMessage(PlanimetryController.class, "DistanceMini.Error.Text"));
    } else {
      setErrorText(null);
    }
  }
  
  public Double getDistanceMini() {
    return (Double) distanceMini.getValue();
  }
  
  @Override
  public boolean isDataValid() {
    return distanceMini.getValue() != null;
  }
}
