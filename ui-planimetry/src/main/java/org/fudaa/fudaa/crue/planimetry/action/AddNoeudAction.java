package org.fudaa.fudaa.crue.planimetry.action;

import java.awt.Color;
import javax.swing.Icon;
import javax.swing.JComponent;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.edition.BPaletteEdition;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.crue.planimetry.PlanimetryGisPaletteEdition;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class AddNoeudAction extends AbstractAddAction {

  private static Icon createIcon() {
    TraceIconModel model = new TraceIconModel(TraceIcon.CERCLE, 10, Color.BLACK);
    model.setBackgroundColor(Color.WHITE);
    model.setBackgroundColorPainted(true);
    return new TraceIcon(model);
  }

  public AddNoeudAction(PlanimetryGisPaletteEdition palette, PlanimetryController controller, EditZone zone) {
    super(palette, controller, zone, NbBundle.getMessage(AddNoeudAction.class, "AddNoeudAction.Name"), createIcon(),
          "ADD_NOEUD");
  }

  @Override
  public String getDefaultPaletteAction() {
    return BPaletteEdition.ADD_POINT_ACTION;
  }

  @Override
  protected JComponent getEditionComponent() {
    return null;
  }

  @Override
  protected ZCalqueAffichageDonnees getLayer() {
    return controller.getNodeController().getLayer();
  }
}
