package org.fudaa.fudaa.crue.planimetry.configuration;

import java.awt.Color;

import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLigneBriseeLayerModel;

/**
 * @author deniger
 */
public final class LigneBriseeConfiguration extends AbstractLigneBriseeConfiguration {

  TraceLigneModel line = new TraceLigneModel(TraceLigne.LISSE, 2f, Color.BLACK);
  TraceIconModel icon = new TraceIconModel(TraceIcon.CARRE, 4, Color.BLACK);
  LabelConfiguration labelConfiguration = new LabelConfiguration();

  public LigneBriseeConfiguration() {
    icon.setBackgroundColor(Color.WHITE);
  }

  public LigneBriseeConfiguration(LigneBriseeConfiguration from) {
    super(from);
    if (from != null) {
      line = new TraceLigneModel(from.line);
      icon = new TraceIconModel(from.icon);
      labelConfiguration = from.labelConfiguration.clone();
    }
  }

  @Override
  public LigneBriseeConfiguration copy() {
    return new LigneBriseeConfiguration(this);
  }


  public LabelConfiguration getLabelConfiguration() {
    return labelConfiguration;
  }

  @Override
  public void initTraceAtomicIcon(TraceIconModel traceIconModel, int idxPoly, PlanimetryLigneBriseeLayerModel model) {
    traceIconModel.updateData(icon);
  }

  @Override
  public void initTraceLigne(TraceLigneModel traceLigneModel, int idxPoly, PlanimetryLigneBriseeLayerModel model) {
    traceLigneModel.updateData(line);
  }
}
