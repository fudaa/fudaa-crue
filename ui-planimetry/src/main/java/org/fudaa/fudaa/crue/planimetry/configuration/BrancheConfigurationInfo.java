package org.fudaa.fudaa.crue.planimetry.configuration;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SpinnerNumberModel;
import org.fudaa.dodico.crue.metier.comparator.ToStringInternationalizableComparator;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.fudaa.crue.common.config.ConfigurationInfoHelper;
import org.fudaa.fudaa.crue.common.editor.SpinnerInplaceEditor;
import org.fudaa.fudaa.crue.planimetry.configuration.editor.BrancheColorPropertySupport;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.NbBundle;

/**
 ** Attention. Les name des set sont utilisés pour la persistence dans le xml. Donc, ne pas changer sans rejouer les tests unitaires et faire des
 * reprises
 *
 * @author deniger
 */
public class BrancheConfigurationInfo {

  public static final String PREFIX_BRANCHES = "branches";

  public static List<Sheet.Set> createSet(BrancheConfiguration in) {
    List<Sheet.Set> res = new ArrayList<>();
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setName(PREFIX_BRANCHES);
    set.put(VisuConfigurationInfo.createTransparency(in));
    try {
      set.put(ConfigurationInfoHelper.create(BrancheConfiguration.PROPERTY_SENS_PAINTED, Boolean.TYPE, in,
              "BrancheConfiguration.SensPainted", BrancheConfiguration.class));
      final PropertySupport.Reflection sizeProperty = ConfigurationInfoHelper.create(BrancheConfiguration.PROPERTY_FLECHE_SIZE, Integer.TYPE, in,
              "BrancheConfiguration.FlecheSize", BrancheConfiguration.class);
      set.put(sizeProperty);
      SpinnerInplaceEditor inplaceEditor = new SpinnerInplaceEditor(new SpinnerNumberModel(1, 1, 100, 1));
      sizeProperty.setValue("inplaceEditor", inplaceEditor);
    } catch (NoSuchMethodException noSuchMethodException) {
      Logger.getLogger(BrancheConfigurationInfo.class.getName()).log(Level.SEVERE, "message {0}", noSuchMethodException);
    }
    set.setDisplayName(NbBundle.getMessage(BrancheConfigurationInfo.class, "BrancheConfiguration.DisplayName"));
    set.setShortDescription(set.getDisplayName());
    res.add(set);
    res.add(TraceLineModelConfigurationInfo.createSet(in.lineForBranche, PREFIX_BRANCHES, false));
    res.add(TraceIconModelConfigurationInfo.createSet(in.iconModelTypeBranche, PREFIX_BRANCHES + ".mainIcon", false));
    final Set iconForAtomic = TraceIconModelConfigurationInfo.createSet(in.iconForLinePoints, PREFIX_BRANCHES + ".atomic");
    iconForAtomic.setDisplayName(NbBundle.getMessage(BrancheConfigurationInfo.class,
            "BrancheConfiguration.AtomicPoint.DisplayName"));
    res.add(iconForAtomic);
    res.add(LabelConfigurationInfo.createSet(in.getLabelConfiguration(), PREFIX_BRANCHES, true));
    res.add(createColorSheet(in));
    return res;
  }

  public static Sheet.Set createColorSheet(BrancheConfiguration in) {
    Sheet.Set set = new Set();
    set.setName(PREFIX_BRANCHES + ".colors");
    set.setDisplayName(NbBundle.getMessage(BrancheConfigurationInfo.class, "BrancheConfiguration.BrancheColors"));
    Map<EnumBrancheType, Color> colorByBrancheType = in.getColorByBrancheType();
    List<EnumBrancheType> keySet = new ArrayList<>(colorByBrancheType.keySet());
    Collections.sort(keySet, ToStringInternationalizableComparator.INSTANCE);
    for (EnumBrancheType enumBrancheType : keySet) {
      BrancheColorPropertySupport res = new BrancheColorPropertySupport(enumBrancheType, in);
      set.put(res);
    }
    return set;
  }
}
