/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.action;

import com.memoire.bu.BuPopupMenu;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JMenu;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;
import org.fudaa.dodico.crue.metier.emh.DonCLimMCommonItem;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.factory.DclmFactory;
import org.fudaa.dodico.crue.metier.factory.DclmFactory.CalcBuilder;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class PlanimetryDclmController {

  private final PlanimetryControllerHelper helper;

  public PlanimetryDclmController(PlanimetryControllerHelper helper) {
    this.helper = helper;
  }

  /**
   *
   * @param emh l'EMH
   * @param activatedDclm les dclm deja active.
   * @return les builder utilisable pour l'EMH
   */
  private List<CalcBuilder> getBuilder(EMH emh, List<DonCLimM> activatedDclm) {
    final Calc editedCalc = helper.getcondLimiteController().getSelectedCalc();
    if (editedCalc == null) {
      return Collections.emptyList();
    }
    List<CalcBuilder> initBuilder = DclmFactory.getCreators(editedCalc);
    List<CalcBuilder> filteredBuilder = new ArrayList<>();
    Set<Class> usedDclm = new HashSet<>();
    for (DonCLimM donCLimM : activatedDclm) {
      usedDclm.add(donCLimM.getClass());
    }

    for (CalcBuilder calcBuilder : initBuilder) {
      if (calcBuilder.isAccepted(emh) && !usedDclm.contains(calcBuilder.getDclmClass())) {
        filteredBuilder.add(calcBuilder);
      }
    }
    Collections.sort(filteredBuilder, ObjetNommeByNameComparator.INSTANCE);
    return filteredBuilder;
  }

  public void fillPopupMenu(EMH emh, BuPopupMenu popup) {
    if (!helper.getcondLimiteController().isDclmEditable()) {
      return;
    }
    boolean enable = helper.getcondLimiteController().isCalcSelected();
    List<DonCLimM> dclmForEditedCalc = helper.getcondLimiteController().getDclmForEditedCalc(emh);
    List<CalcBuilder> filteredBuilder = getBuilder(emh, dclmForEditedCalc);
    //add
    boolean sepAdded = false;
    if (!filteredBuilder.isEmpty()) {
      popup.addSeparator();
      sepAdded = true;
      JMenu menu = new JMenu(NbBundle.getMessage(PlanimetryDclmController.class, "AddDclmAction.DisplayName"));
      menu.setIcon(CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/planimetry/icons/add.png"));
      popup.add(menu);
      for (CalcBuilder calcBuilder : filteredBuilder) {
        menu.add(new DclmAddAction(helper, emh, calcBuilder)).setEnabled(enable);
      }
    }
    if (!dclmForEditedCalc.isEmpty()) {
      if (!sepAdded) {
        popup.addSeparator();
        sepAdded = true;
      }
      JMenu menu = new JMenu(NbBundle.getMessage(PlanimetryDclmController.class, "RemoveDclmAction.DisplayName"));
      menu.setIcon(CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/planimetry/icons/remove.png"));
      popup.add(menu);
      for (DonCLimM donCLimM : dclmForEditedCalc) {
        menu.add(new DclmRemoveAction(helper, emh, (DonCLimMCommonItem) donCLimM)).setEnabled(enable);
      }
    }

    if (sepAdded) {
      popup.addSeparator();
    }
  }
}
