package org.fudaa.fudaa.crue.planimetry.controller;

import org.locationtech.jts.geom.Coordinate;
import gnu.trove.TObjectDoubleHashMap;
import gnu.trove.TObjectDoubleIterator;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GisAbscCurviligneToCoordinate;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.fudaa.crue.planimetry.configuration.TraceConfiguration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author deniger
 */
public class LayerTraceGisBuilder {
    private final PlanimetryController controller;

    public LayerTraceGisBuilder(PlanimetryController controller) {
        this.controller = controller;
    }

    public static double findXmaxLit(List<LitNumerote> litNumerotes, boolean onlyActif) {
        double xmax = 0;
        boolean first = true;
        for (LitNumerote litNumerote : litNumerotes) {
            if (onlyActif && !litNumerote.getIsLitActif()) {
                continue;
            }
            if (first) {
                xmax = litNumerote.getLimFin().getAbscisse();
                first = false;
            } else {
                xmax = Math.max(xmax, litNumerote.getLimFin().getAbscisse());
            }
        }
        return xmax;
    }

    public static double findXminLit(List<LitNumerote> litNumerotes, boolean onlyActif) {
        double xmin = 0;
        boolean first = true;
        for (LitNumerote litNumerote : litNumerotes) {
            if (onlyActif && !litNumerote.getIsLitActif()) {
                continue;
            }
            if (first) {
                xmin = litNumerote.getLimDeb().getAbscisse();
                first = false;
            } else {
                xmin = Math.min(xmin, litNumerote.getLimDeb().getAbscisse());
            }
        }
        return xmin;
    }

    /**
     * Construit les lignes en partant toujours de la rive droite (premier point = rive droite).
     *
     * @param offsetForAmonAvalSection l'offset pour l'affichage
     * @return la collection
     */
    public GISZoneCollectionLigneBrisee rebuild(double offsetForAmonAvalSection, Map<Long, Pair<Double, Double>> anglesBySectionNames) {
        final GISZoneCollectionLigneBrisee oldSectionCollection = controller.getTraceController().getGISCollection();
        Map<Long, Pair<Double, Double>> oldAngle = new HashMap<>();
        final PlanimetryGisModelContainer planimetryGisModelContainer = controller.getPlanimetryGisModelContainer();
        if (anglesBySectionNames != null) {
            oldAngle.putAll(anglesBySectionNames);
        } else {
            if (oldSectionCollection != null && oldSectionCollection.getNumGeometries() > 0) {
                int nb = oldSectionCollection.getNumGeometries();
                for (int i = 0; i < nb; i++) {
                    Long uid = planimetryGisModelContainer.getUid(oldSectionCollection, i);
                    if (uid != null) {
                        Double startAngleInDegree = planimetryGisModelContainer.getAngleBegin(oldSectionCollection, i);
                        Double endAngleInDegree = planimetryGisModelContainer.getAngleEnd(oldSectionCollection, i);
                        oldAngle.put(uid, new Pair<>(startAngleInDegree, endAngleInDegree));
                    }
                }
            }
        }

        final GISZoneCollectionLigneBrisee newSectionCollection = planimetryGisModelContainer.createTraceCollection();
        newSectionCollection.setGeomModifiable(true);
        final LayerBrancheController brancheController = controller.getBrancheController();
        GISZoneCollectionLigneBrisee brancheCollection = brancheController.getBrancheCollection();
        int nb = brancheCollection.getNumGeometries();
        GisAbscCurviligneToCoordinate.Result tmpResult = new GisAbscCurviligneToCoordinate.Result();
        double realDistance = offsetForAmonAvalSection;
        double defaultAngleInRadians = getTraceConfiguration().getAngleInRadians();
        double defaultAngleInDegree = getTraceConfiguration().getAngleInDegree();
        for (int i = nb - 1; i >= 0; i--) {
            CatEMHBranche branche = brancheController.getBranche(i);
            GISPolyligne brancheGis = brancheController.getBrancheGis(i);

            TObjectDoubleHashMap<RelationEMHSectionDansBranche> ratiosBySection = LayerSectionGisBuilder.getRatio(i, realDistance,
                    brancheController);
            for (TObjectDoubleIterator it = ratiosBySection.iterator(); it.hasNext(); ) {
                it.advance();
                double ratio = it.value();
                RelationEMHSectionDansBranche sectionDansBranche = (RelationEMHSectionDansBranche) it.key();
                List<DonPrtGeo> dptg = sectionDansBranche.getEmh().getDPTG();
                if (CollectionUtils.isEmpty(dptg)) {
                    continue;
                }
                DonPrtGeoProfilSection profilSection = EMHHelper.selectFirstOfClass(dptg,
                        DonPrtGeoProfilSection.class);
                if (profilSection == null) {
                    continue;
                }
                GisAbscCurviligneToCoordinate.find(brancheGis, ratio, tmpResult);
                if (!tmpResult.isFound()) {
                    continue;
                }
                double startAngleInRadians = defaultAngleInRadians;
                double startAngleInDegree = defaultAngleInDegree;
                double endAngleInRadians = defaultAngleInRadians;
                double endAngleInDegree = defaultAngleInDegree;
                Pair<Double, Double> angleInDegrees = oldAngle.get(sectionDansBranche.getEmh().getUiId());
                if (angleInDegrees != null) {
                    startAngleInDegree = angleInDegrees.first;
                    startAngleInRadians = Math.toRadians(startAngleInDegree);
                    endAngleInDegree = angleInDegrees.second;
                    endAngleInRadians = Math.toRadians(endAngleInDegree);
                }
                GISPolyligne traceGIS = createLine(profilSection, brancheGis, tmpResult, startAngleInRadians, endAngleInRadians);
                if (traceGIS != null) {
                    newSectionCollection.addPolyligne(traceGIS, planimetryGisModelContainer.buildTraceData(
                            sectionDansBranche, branche, startAngleInDegree, endAngleInDegree), null);
                }
            }
        }
        newSectionCollection.setGeomModifiable(false);
        return newSectionCollection;
    }

    private TraceConfiguration getTraceConfiguration() {
        return controller.getVisuConfiguration().getTraceConfiguration();
    }

    private GISPolyligne createLine(DonPrtGeoProfilSection profilSection, GISPolyligne brancheGis,
                                    GisAbscCurviligneToCoordinate.Result tmpResult, double angleStartInRadians, double angleEndInRadians) {
        if (profilSection == null) {
            return null;
        }
        List<PtProfil> ptProfil = profilSection.getPtProfil();
        List<LitNumerote> litNumerotes = profilSection.getLitNumerote();
        if (CollectionUtils.isEmpty(litNumerotes) || CollectionUtils.isEmpty(ptProfil)) {
            return null;
        }
        double xmin = findXminLit(litNumerotes, false);
        double xminActif = findXminLit(litNumerotes, true);
        double xmax = findXmaxLit(litNumerotes, false);
        double xmaxActif = findXmaxLit(litNumerotes, true);
        boolean mineurFound = false;
        double minMineur = 0;
        double maxMineur = 0;
        for (LitNumerote litNumerote : litNumerotes) {
            if (litNumerote.getIsLitMineur()) {
                double xDeb = litNumerote.getLimDeb().getAbscisse();
                double xFin = litNumerote.getLimFin().getAbscisse();
                double currentMinMineur = Math.min(xDeb, xFin);
                double currentMaxMineur = Math.max(xDeb, xFin);
                if (!mineurFound) {
                    mineurFound = true;
                    minMineur = currentMinMineur;
                    maxMineur = currentMaxMineur;
                } else {
                    minMineur = Math.min(minMineur, currentMinMineur);
                    maxMineur = Math.max(maxMineur, currentMaxMineur);
                }
            }
        }

        int segmentidx = tmpResult.getSegmentidx();
        Coordinate amont = brancheGis.getCoordinateN(segmentidx);
        Coordinate aval = brancheGis.getCoordinateN(segmentidx + 1);
        double norm = amont.distance(aval);
        if (norm == 0) {
            if (segmentidx > 0) {
                amont = brancheGis.getCoordinateN(segmentidx - 1);
                norm = amont.distance(aval);
                if (norm == 0) {
                    return null;
                }
            }
            return null;
        }
        double vx = aval.x - amont.x;
        double vy = aval.y - amont.y;
        double vxTrace = (vy) / norm;
        double vyTrace = -(vx) / norm;
        if (getTraceConfiguration().isProfilVueDeAmont()) {
            vxTrace = -vxTrace;
            vyTrace = -vyTrace;
        }
        Map<ItemEnum, DonPrtGeoProfilEtiquette> createMap = DonPrtGeoProfilEtiquette.createMap(profilSection.getEtiquettes());
        DonPrtGeoProfilEtiquette axeHyd = createMap.get(controller.getCrueConfigMetier().getLitNomme().getEtiquetteAxeHyd());

        double xAxeHydraulique = (maxMineur + minMineur) / 2;//retrouver la notion d'axe hydraulique...
        if (axeHyd != null) {
            xAxeHydraulique = axeHyd.getPoint().getXt();
        }
        final Coordinate accrochePt = tmpResult.getCoordinate();
        Coordinate rd = new Coordinate();
        Coordinate rdActif = new Coordinate();
        Coordinate rdMineur = new Coordinate();
        Coordinate rg = new Coordinate();
        Coordinate rgActif = new Coordinate();
        Coordinate rgMineur = new Coordinate();
        compute(accrochePt, vxTrace, vyTrace, (xAxeHydraulique - xmin), rd);
        compute(accrochePt, vxTrace, vyTrace, (xAxeHydraulique - xminActif), rdActif);
        compute(accrochePt, vxTrace, vyTrace, (xAxeHydraulique - minMineur), rdMineur);
        compute(accrochePt, vxTrace, vyTrace, (xAxeHydraulique - xmax), rg);
        compute(accrochePt, vxTrace, vyTrace, (xAxeHydraulique - xmaxActif), rgActif);
        compute(accrochePt, vxTrace, vyTrace, (xAxeHydraulique - maxMineur), rgMineur);
        if (angleStartInRadians != 0) {
            final double sinAlpha = Math.sin(angleStartInRadians);
            final double cosAlpha = Math.cos(angleStartInRadians);
            if (!rd.equals2D(rdMineur)) {
                rotate(rd, rdMineur, cosAlpha, sinAlpha);
                rotate(rdActif, rdMineur, cosAlpha, sinAlpha);
            }
        }
        if (angleEndInRadians != 0) {
            final double sinAlpha = Math.sin(angleEndInRadians);
            final double cosAlpha = Math.cos(angleEndInRadians);
            if (!rg.equals2D(rgMineur)) {
                rotate(rg, rgMineur, cosAlpha, sinAlpha);
                rotate(rgActif, rgMineur, cosAlpha, sinAlpha);
            }
        }

        return GISGeometryFactory.INSTANCE.createLineString(
                new Coordinate[]{rd, rdActif, rdMineur, rgMineur, rgActif, rg});
    }

    protected void compute(Coordinate base, double vx, double vy, double norm, Coordinate pt) {
        pt.x = base.x + vx * norm;
        pt.y = base.y + vy * norm;
    }

    private void rotate(Coordinate ptToRotate, Coordinate reference, final double cosAlpha, final double sinAlpha) {
        double vx = ptToRotate.x - reference.x;
        double vy = ptToRotate.y - reference.y;
        //on tourne le vecteur de x radians:
        double vxAfterRotation = vx * cosAlpha - vy * sinAlpha;
        double vyAfterRotation = vy * cosAlpha + vx * sinAlpha;
        ptToRotate.x = reference.x + vxAfterRotation;
        ptToRotate.y = reference.y + vyAfterRotation;
    }
}
