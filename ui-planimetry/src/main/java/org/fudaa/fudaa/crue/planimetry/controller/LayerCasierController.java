package org.fudaa.fudaa.crue.planimetry.controller;

import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumCasierType;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.crue.planimetry.GeometryIndexer;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.action.OpenEMHAction;
import org.fudaa.fudaa.crue.planimetry.action.SimplifyProfilsAction;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.layer.LayerControllerEMH;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryCasierLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryCasierLayerModel;
import org.fudaa.fudaa.crue.planimetry.listener.CasierUpdaterFromNode;
import org.openide.util.NbBundle;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author deniger
 */
public class LayerCasierController extends LayerModelControllerDefault<PlanimetryCasierLayer> implements LayerControllerEMH {

  GeometryIndexer casierIndexer;

  protected static LayerCasierController install(PlanimetryController ctx, PlanimetryVisuPanel res,
          VisuConfiguration configuration) {
    LayerCasierController casierController = new LayerCasierController();
    casierController.layer = new PlanimetryCasierLayer(
            new PlanimetryCasierLayerModel(ctx.gisModel.createCasierCollection(), casierController),
            res.getEditor());
    casierController.layer.setLayerConfiguration(configuration.getCasierConfiguration());
    casierController.layer.setName(PlanimetryController.NAME_CQ_CASIER);
    casierController.layer.setTitle(NbBundle.getMessage(PlanimetryControllerBuilder.class, PlanimetryController.NAME_CQ_CASIER));
    casierController.layer.setActions(new EbliActionInterface[]{
      new OpenEMHAction.Reopen(ctx),
      new OpenEMHAction.NewFrame(ctx),
      null,
      res.getSigEditAction(),
      null,
      res.getEditor().getExportWithNameAction(),
//      res.getEditor().getExportAction(),
      null,
      res.getEditor().getActionDelete(),
      res.getPlanimetryVisuController().getDeleteCascade(),
      null,
      new SimplifyProfilsAction(ctx),
      null,
      res.createFindAction(casierController.layer)});
    ctx.controllersByName.put(casierController.layer.getName(), casierController);
    return casierController;
  }

  @Override
  public void changeWillBeDone() {
    //modif defa effectuée
    if (isModified()) {
      return;
    }
    super.changeWillBeDone();
    getHelper().getNodeController().changeWillBeDone();
    getHelper().getBrancheController().changeWillBeDone();
  }

  @Override
  public List<ToStringInternationalizable> getCategories() {
    return Arrays.asList(EnumCasierType.values());
  }

  @Override
  public void setVisuConfiguration(VisuConfiguration cloned) {
    getLayer().setLayerConfiguration(cloned.getCasierConfiguration());
  }

  @Override
  protected void geometryDataUpdated() {
    if (casierIndexer != null) {
      casierIndexer.reindex();
    }
  }

  public int getCasierPosition(Long uid) {
    return casierIndexer.getPosition(uid);
  }

  public CatEMHCasier getCasier(int position) {
    Long uid = helper.getUid(getCasierCollection(), position);
    return getEMH(uid);
  }

  @Override
  public EMH getEMHFromLayerPosition(int position) {
    return getCasier(position);
  }

  public void updateCasierFromNode() {
    casierUpdaterFromNode.updateAll();
  }

  public GISZoneCollectionLigneBrisee getCasierCollection() {
    return layer.modeleDonnees().getGeomData();
  }
  CasierUpdaterFromNode casierUpdaterFromNode;

  @Override
  public void init(PlanimetryControllerHelper helper) {
    super.init(helper);
    casierIndexer = new GeometryIndexer(layer.modeleDonnees(), helper.getPlanimetryGisModelContainer());
    listenerDispatcher.addListener(casierIndexer);

    //pour que les noeuds suivent la branche
    casierUpdaterFromNode = new CasierUpdaterFromNode(this);
    helper.getNodeController().addGISListener(casierUpdaterFromNode);
  }

  public void globalChangedFinished() {
    casierUpdaterFromNode.updateIfNeeded();
  }

  public boolean setSelectedEMHs(List<EMH> selectedEMH) {
    List<EMH> selectEMHS = EMHHelper.selectEMHS(selectedEMH, EnumCatEMH.CASIER);
    TIntArrayList positions = new TIntArrayList();
    for (EMH emh : selectEMHS) {
      int nodePosition = getCasierPosition(emh.getUiId());
      if (nodePosition >= 0) {
        positions.add(nodePosition);
      }
    }
    layer.setSelection(positions.toNativeArray());
    return !positions.isEmpty();
  }

  public boolean setSelectedEMHUids(Collection<Long> selectedUids) {
    TIntArrayList positions = new TIntArrayList();
    for (Long uid : selectedUids) {
      int nodePosition = getCasierPosition(uid);
      if (nodePosition >= 0) {
        positions.add(nodePosition);
      }
    }
    layer.setSelection(positions.toNativeArray());
    return !positions.isEmpty();
  }

  public void getSelectedEMHs(Collection<Long> uids) {
    if (!layer.isVisible() || layer.isSelectionEmpty()) {
      return;
    }
    int[] selectedIndex = layer.getSelectedIndex();
    if (selectedIndex != null) {
      for (int i = 0; i < selectedIndex.length; i++) {
        uids.add(helper.getUid(getCasierCollection(), selectedIndex[i]));
      }
    }
  }
}
