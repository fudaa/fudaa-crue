package org.fudaa.fudaa.crue.planimetry.listener;

import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISZoneListener;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;

/**
 * Pour mettre a jour l'affichage lorsque les calques branche, casier, noeud se mette à jour
 *
 * @author deniger
 */
public class PlanimetryVisuUpdater implements GISZoneListener {

  private final PlanimetryVisuPanel visuPanel;

  public PlanimetryVisuUpdater(PlanimetryVisuPanel visuPanel) {
    this.visuPanel = visuPanel;
    visuPanel.getPlanimetryController().getBrancheController().addGISListener(this);
    visuPanel.getPlanimetryController().getCasierController().addGISListener(this);
    visuPanel.getPlanimetryController().getNodeController().addGISListener(this);
  }

  @Override
  public void attributeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _action) {
    repaint();
  }

  protected void repaint() {
    visuPanel.getPlanimetryController().getTraceController().getLayer().repaint();
    visuPanel.getPlanimetryController().getSectionController().getLayer().repaint();
    visuPanel.repaint();
  }

  @Override
  public void attributeValueChangeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _indexObj,
                                         Object _newValue) {
    repaint();
  }

  @Override
  public void objectAction(Object _source, int _indexObj, Object _obj, int _action) {
    repaint();
  }
}
