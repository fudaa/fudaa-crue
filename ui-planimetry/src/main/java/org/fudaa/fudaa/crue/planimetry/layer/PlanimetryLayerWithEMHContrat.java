/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.layer;

/**
 *
 * @author Frederic Deniger
 */
public interface PlanimetryLayerWithEMHContrat extends PlanimetryLayerContrat{
  
  LayerControllerEMH getLayerControllerEMH();
  
}
