package org.fudaa.fudaa.crue.planimetry.layout;

import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.IdRegistry;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryGisModelContainer;

import java.util.Set;

/**
 * @author deniger
 */
public class NetworkUpdater {
  private final PlanimetryController controller;

  public NetworkUpdater(PlanimetryController controller) {
    this.controller = controller;
  }

  public boolean reload(EMHScenario loaded) {
    return reload(loaded, null, null);
  }

  /**
   * Annule les modifications en cours. Cette methode ne gere pas le fait que des EMHS aient changées de Sous-Modeles parents (
   * dans ce cas il faut enregistrer les geoloc). Ce sont les actions le faisant qui doivent forwarder l'evt.
   *
   * @param loaded a recharger. Reconstruit le layout si nécessaire.
   * @return true if modified
   */
  public boolean reload(EMHScenario loaded, NetworkGisPositionnerResult newPosition, Set<String> emhNameToUpdate) {
    final PlanimetryGisModelContainer modelContainer = controller.getPlanimetryGisModelContainer();
    //on met à jour le scenario:
    modelContainer.setScenario(loaded);
    GISZoneCollectionLigneBrisee currentBrancheModele = (GISZoneCollectionLigneBrisee) controller.getBrancheController().getGeomData();
    GISZoneCollectionLigneBrisee currentCasier = (GISZoneCollectionLigneBrisee) controller.getCasierController().getGeomData();
    GISZoneCollectionPoint currentNode = (GISZoneCollectionPoint) controller.getNodeController().getGeomData();
    currentBrancheModele.setGeomModifiable(true);
    currentCasier.setGeomModifiable(true);
    currentNode.setGeomModifiable(true);
    //des modeles vides:
    controller.getCasierController().setGeometryData(modelContainer.createCasierCollection());
    controller.getNodeController().setGeometryData(modelContainer.createNodeCollection());
    controller.getBrancheController().setGeometryData(modelContainer.createEdgeCollection());
    //on enleve les EMH supprimées
    boolean modified = removedDeletedEMH(currentBrancheModele, loaded, currentNode, currentCasier);
    //on met à jour les noms:
    updateEMHNom(currentNode, loaded.getIdRegistry());
    updateEMHNom(currentCasier, loaded.getIdRegistry());
    updateEMHNom(currentBrancheModele, loaded.getIdRegistry());

    NetworkGisPositionnerResult result = new NetworkGisPositionnerResult(modelContainer, currentNode, currentBrancheModele,
      currentCasier);
    NetworkBuilder builder = new NetworkBuilder();
    modified |= builder.checkAllEMHPresent(loaded, result, controller.getVisuConfiguration());
    modified |=builder.applyLocalChanges(result,newPosition,emhNameToUpdate);

    controller.getCasierController().setGeometryData(currentCasier);
    controller.getBrancheController().setGeometryData(currentBrancheModele);
    controller.getNodeController().setGeometryData(currentNode);

    try {
      //pour eviter de reconstruire les sections et les traces pendant l'update majeur du modele
      controller.getSectionController().globalChangedStarted();
      controller.getTraceController().globalChangedStarted();

      //si les association branche/noeud ont été modifiées on les refait ici:
      controller.getBrancheController().updateBrancheFromNode();
      //si les association casier/noeud ont été modifiées on les refait ici:
      controller.getCasierController().updateCasierFromNode();
    }
    finally {
      controller.getSectionController().globalChangedFinished();
      controller.getTraceController().globalChangedFinished();
    }
    return modified;
  }

  private void updateEMHNom(GISZoneCollection collection, IdRegistry idRegistry) {
    GISAttributeModel uids = collection.getModel(PlanimetryGisModelContainer.UID_POSITION);
    GISAttributeModel names = collection.getModel(PlanimetryGisModelContainer.NOM_EMH_POSITION);
    int nb = uids.getSize();
    for (int i = 0; i < nb; i++) {
      Long uid = (Long) uids.getObjectValueAt(i);
      EMH emh = idRegistry.getEmh(uid);
      names.setObject(i, emh.getNom(), null);
    }
  }

  private int[] findPositionToRemove(GISZoneCollection collection, IdRegistry registry) {
    GISAttributeModel uids = collection.getModel(PlanimetryGisModelContainer.UID_POSITION);
    int nb = uids.getSize();
    TIntArrayList idx = new TIntArrayList();
    for (int i = 0; i < nb; i++) {
      Long uid = (Long) uids.getObjectValueAt(i);
      if (!registry.containEMH(uid)) {
        idx.add(i);
      }
    }
    return idx.toNativeArray();
  }

  private boolean removedDeletedEMH(GISZoneCollectionLigneBrisee currentBrancheModele, EMHScenario loaded,
                                    GISZoneCollectionPoint currentNode, GISZoneCollectionLigneBrisee currentCasier) {
    //suppression des emh enlevées:
    int[] brancheToRemove = findPositionToRemove(currentBrancheModele, loaded.getIdRegistry());

    boolean modified = false;
    if (brancheToRemove.length > 0) {
      modified = true;
      currentBrancheModele.removeGeometries(brancheToRemove, null);
    }
    int[] noeudsToRemove = findPositionToRemove(currentNode, loaded.getIdRegistry());
    if (noeudsToRemove.length > 0) {
      modified = true;
      currentNode.removeGeometries(noeudsToRemove, null);
    }
    int[] casiersToRemove = findPositionToRemove(currentCasier, loaded.getIdRegistry());
    if (casiersToRemove.length > 0) {
      modified = true;
      currentCasier.removeGeometries(casiersToRemove, null);
    }
    return modified;
  }
}
