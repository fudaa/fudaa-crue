package org.fudaa.fudaa.crue.planimetry.layer;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.crue.planimetry.configuration.LigneBriseeConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.node.AdditionalConfigurationNode;
import org.fudaa.fudaa.crue.planimetry.configuration.node.LigneBriseeConfigurationNode;
import org.fudaa.fudaa.crue.planimetry.controller.LayerLigneBriseeExternController;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 *
 * @author deniger
 */
public abstract class AbstractPlanimetryLigneBriseeExternLayer extends PlanimetryLigneBriseeLayer<LigneBriseeConfiguration>
        implements PlanimetryAdditionalLayerContrat<LigneBriseeConfiguration> {

  private final LabelPainter labelPainter = new LabelPainter();

  public AbstractPlanimetryLigneBriseeExternLayer(PlanimetryLigneBriseeLayerModel _modele, FSigEditor _editor) {
    super(_modele, _editor);
    this.layerConfiguration = new LigneBriseeConfiguration();
  }

  @Override
  public void installed() {
    final FSigEditor editor = (FSigEditor) getEditor();
    final EbliActionSimple exportAction = editor.getExportAction();
    setActions(new EbliActionInterface[]{exportAction});
  }

  @Override
  protected void paintLabelsOnAtomics(Graphics2D _g, GrMorphisme _versEcran, GrMorphisme _versReel, GrBoite _clipReel) {
    if (layerConfiguration.getLabelConfiguration().isDisplayLabels()) {
      super.paintLabelsOnAtomics(_g, _versEcran, _versReel, _clipReel);
    }
  }

  @Override
  protected void drawLabelOnAtomic(final FontMetrics fm, String s, final GrPoint ptDest, final Color bgColor, final Graphics2D _g, final Color fgColor) {
    labelPainter.paintLabels(_g, ptDest, s, layerConfiguration.getLabelConfiguration(), alpha_);
  }

  @Override
  public LayerLigneBriseeExternController getLayerController() {
    return (LayerLigneBriseeExternController) super.getLayerController();
  }

  @Override
  public boolean isDestructible() {
    return true;
  }

  @Override
  public AdditionalConfigurationNode createConfigurationNode() {
    return new LigneBriseeConfigurationNode(layerConfiguration, this);
  }

  @Override
  public void setLayerConfiguration(LigneBriseeConfiguration layerConfiguration) {
    getLayerController().changeWillBeDone();
    setLayerConfigurationNoUndo(layerConfiguration);
  }

  public void setLayerConfigurationNoUndo(LigneBriseeConfiguration layerConfiguration) {
    this.layerConfiguration = layerConfiguration;
    firePropertyChange(PROPERTY_LAYER_CONFIGURATION, null, layerConfiguration);
    repaint();
  }
}
