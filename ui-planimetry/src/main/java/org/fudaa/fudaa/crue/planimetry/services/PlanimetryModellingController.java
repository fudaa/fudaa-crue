/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.services;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class PlanimetryModellingController {

  public static final String TOPCOMPONENT_ID = "ModellingVisualTopComponent";
  final SelectedPerspectiveService perspectiveService = Lookup.getDefault().lookup(SelectedPerspectiveService.class);

  public boolean isModellingPerspectiveInEditMode() {
    return perspectiveService.isCurrentPerspectiveInEditMode() && perspectiveService.isActivated(PerspectiveEnum.MODELLING);
  }
}
