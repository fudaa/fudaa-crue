package org.fudaa.fudaa.crue.planimetry.controller;

import com.rits.cloning.Cloner;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.gis.*;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.lit.LitNommeLimite;
import org.fudaa.dodico.crue.metier.emh.*;
import org.openide.util.NbBundle;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author deniger
 */
public class PlanimetryGisModelContainer {

  public static final int UID_POSITION = 0;
  public static final int NOM_EMH_POSITION = 1;
  /**
   * pour les sections et les traces, permet de connaître l'UID de la branche parente
   */
  private static final int BRANCHE_UID_POSITION = 2;
  private static final int RELATION_SECTION_DANS_BRANCHE_POSITION = 4;
  public static final int TRACE_ANGLE_BEGIN = 5;
  public static final int TRACE_ANGLE_END = 6;
  final GISAttributeLong uid = new GISAttributeLong("uid");
  final GISAttributeString id = new GISAttributeString(org.openide.util.NbBundle.getMessage(PlanimetryGisModelContainer.class,
          "attributeEMH.Name"));
  final GISAttributeDouble beginAngle = new GISAttributeDouble(org.openide.util.NbBundle.getMessage(PlanimetryGisModelContainer.class,
          "BeginAngle.Name"), false);
  final GISAttributeDouble endAngle = new GISAttributeDouble(org.openide.util.NbBundle.getMessage(PlanimetryGisModelContainer.class,
          "EndAngle.Name"), false);
  /**
   * utilisé par section et trace
   */
  private final GISAttributeLong brancheUid = new GISAttributeLong("branche_uid");
  /**
   * utilisé par section et trace
   */
  private final GISAttributeString brancheId = new GISAttributeString(NbBundle.getMessage(PlanimetryGisModelContainer.class,
          "attributeBranche.Name"));
  /**
   * utilise pour recuperer la relation utilisee entre la branche et la section.
   */
  private final GISAttribute sectionDansBranche = new GISAttribute(null, "section_dans_branche");
  private EMHScenario scenario;
  private EMHScenario scenarioInitial;

  public PlanimetryGisModelContainer(EMHScenario scenario) {
    this.scenario = scenario;
    uid.setUserVisible(false);
    brancheUid.setUserVisible(false);
    sectionDansBranche.setUserVisible(false);
    id.getEditor().setEditable(false);
    brancheId.getEditor().setEditable(false);
    beginAngle.setInternEditor(new CtuluValueEditorDouble());
    endAngle.setInternEditor(new CtuluValueEditorDouble());
    setAnglesEditable(false);

    //Attention ne pas changer ces id: utilisés à la relecture des données de geoloc:
    id.setID("EMH_NAME");
    uid.setID("EMH_UID");
    beginAngle.setID("ANGLE_START");
    endAngle.setID("ANGLE_END");
    brancheId.setID("BRANCHE_NAME");
    brancheUid.setID("BRANCHE_UID");
    sectionDansBranche.setID("SECTION_OBJECT");


  }

  void updateNames(CrueConfigMetier crueConfigMetier) {
    final List<LitNommeLimite> limites = crueConfigMetier.getLitNomme().getLimites();
    beginAngle.setName(NbBundle.getMessage(PlanimetryGisModelContainer.class, "Angle.AtributeName", limites.get(0).geti18n()));
    endAngle.setName(NbBundle.getMessage(PlanimetryGisModelContainer.class, "Angle.AtributeName", limites.get(limites.size() - 1).geti18n()));
  }

  public final void setAnglesEditable(boolean editable) {
    beginAngle.getEditor().setEditable(editable);
    endAngle.getEditor().setEditable(editable);

  }

  public void setScenario(EMHScenario scenario) {
    this.scenario = scenario;
    scenarioInitial = null;
  }

  protected void scenarioEditionStarted() {
    if (scenarioInitial == null) {
      scenarioInitial = scenario;
      Cloner cloner = new Cloner();
      scenario = cloner.deepClone(scenario);
    }
  }

  public boolean isScenarioEdited() {
    return scenarioInitial != null;
  }

  public void saveDone() {
    //on oublie simplement le scenario initial
    scenarioInitial = null;
  }

  public void cancel() {
    if (scenarioInitial != null) {
      scenario = scenarioInitial;
      scenarioInitial = null;
    }
  }

  public EMHScenario getScenario() {
    return scenario;
  }

  public <T extends EMH> T getEMH(Long uid) {
    return (T) scenario.getIdRegistry().getEmh(uid);
  }

  public boolean isUid(GISAttributeInterface attribute) {
    return uid == attribute;
  }

  public GISAttributeModel getUidModel(GISZoneCollectionGeometry zone) {
    return zone.getDataModel(UID_POSITION);
  }

  public Long getUid(GISZoneCollectionGeometry zone, int idx) {
    return (Long) zone.getDataModel(UID_POSITION).getObjectValueAt(idx);
  }

  public Double getAngleBegin(GISZoneCollectionGeometry zone, int idx) {
    return (Double) zone.getDataModel(TRACE_ANGLE_BEGIN).getObjectValueAt(idx);
  }

  public void setAngleBegin(GISZoneCollectionGeometry zone, int idx, Double angleInDegree) {
    zone.getDataModel(TRACE_ANGLE_BEGIN).setObject(idx, angleInDegree, null);
  }

  public void setAngleEnd(GISZoneCollectionGeometry zone, int idx, Double angleInDegree) {
    zone.getDataModel(TRACE_ANGLE_END).setObject(idx, angleInDegree, null);
  }

  public Double getAngleEnd(GISZoneCollectionGeometry zone, int idx) {
    return (Double) zone.getDataModel(TRACE_ANGLE_END).getObjectValueAt(idx);
  }

  /**
   * ne doit etre appele que pour les sections ou trace de section
   *
   * @param zone la zone
   * @param idx index de la branche
   * @return l'uid de la branche
   */
  public Long getBrancheUid(GISZoneCollectionGeometry zone, int idx) {
    return (Long) zone.getDataModel(BRANCHE_UID_POSITION).getObjectValueAt(idx);
  }

  /**
   * ne doit etre appele que pour les sections ou trace de section
   *
   * @param zone la zone
   * @param idx index de la relation
   * @return la relation
   */
  public RelationEMHSectionDansBranche getRelationSectionDansBranche(GISZoneCollectionGeometry zone, int idx) {
    return (RelationEMHSectionDansBranche) zone.getDataModel(RELATION_SECTION_DANS_BRANCHE_POSITION).getObjectValueAt(idx);
  }

  public GISZoneCollectionPoint createNodeCollection() {
    GISZoneCollectionPoint res = new GISZoneCollectionPoint();
    //attention, l'ordre de ce tableau doit respecter les indices *_POSITION de cette classe.
    res.setAttributes(new GISAttributeInterface[]{uid, id}, null);
    return res;
  }

  public GISZoneCollectionPoint createSectionCollection() {
    GISZoneCollectionPoint res = new GISZoneCollectionPoint();
    //attention, l'ordre de ce tableau doit respecter les indices *_POSITION de cette classe.
    res.setAttributes(new GISAttributeInterface[]{uid, id, brancheUid, brancheId, sectionDansBranche}, null);
    return res;
  }

  public GISZoneCollectionLigneBrisee createTraceCollection() {
    GISZoneCollectionLigneBrisee res = new GISZoneCollectionLigneBrisee();
    //attention, l'ordre de ce tableau doit respecter les indices *_POSITION de cette classe.
    res.setAttributes(new GISAttributeInterface[]{uid, id, brancheUid, brancheId, sectionDansBranche, beginAngle, endAngle}, null);
    return res;
  }

  public List buildNodeData(CatEMHNoeud nd) {
    List data = new ArrayList();
    data.add(nd.getUiId());
    data.add(nd.getNom());
    return data;
  }

  public GISZoneCollectionLigneBrisee createEdgeCollection() {
    GISZoneCollectionLigneBrisee res = new GISZoneCollectionLigneBrisee();
    //attention, l'ordre de ce tableau doit respecter les indices *_POSITION de cette classe.
    res.setAttributes(new GISAttributeInterface[]{uid, id}, null);
    return res;
  }

  public GISZoneCollectionLigneBrisee createCasierCollection() {
    GISZoneCollectionLigneBrisee res = new GISZoneCollectionLigneBrisee();
    res.setAttributes(new GISAttributeInterface[]{uid, id}, null);
    return res;
  }

  public Object[] buildEdgeData(CatEMHBranche branche) {
    List data = new ArrayList();
    data.add(branche.getUiId());
    data.add(branche.getNom());
    return data.toArray();
  }

  public Object[] buildTraceData(RelationEMHSectionDansBranche relation,
          CatEMHBranche branche, double angleInDegreeBegin, double angleInDegreeEnd) {
    List data = new ArrayList();
    CatEMHSection section = relation.getEmh();
    data.add(section.getUiId());
    data.add(section.getNom());
    data.add(branche.getUiId());
    data.add(branche.getNom());
    data.add(relation);
    data.add(angleInDegreeBegin);
    data.add(angleInDegreeEnd);
    return data.toArray();
  }

  public Object[] buildSectionData(RelationEMHSectionDansBranche relation,
          CatEMHBranche branche) {
    List data = new ArrayList();
    CatEMHSection section = relation.getEmh();
    data.add(section.getUiId());
    data.add(section.getNom());
    data.add(branche.getUiId());
    data.add(branche.getNom());
    data.add(relation);
    return data.toArray();
  }

  public Object[] buildCasierData(CatEMHCasier casier) {
    List data = new ArrayList();
    data.add(casier.getUiId());
    data.add(casier.getNom());
    return data.toArray();
  }
}
