/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.action;

import java.awt.event.ActionEvent;
import java.util.List;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryGisModelContainer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternTraceCasierLayer;
import org.fudaa.fudaa.crue.planimetry.services.PlanimetryModellingController;
import org.fudaa.fudaa.crue.planimetry.sig.tracecasier.TraceCasierSousModele;
import org.fudaa.fudaa.crue.planimetry.sig.tracecasier.TraceCasierSousModeleContent;
import org.fudaa.fudaa.crue.planimetry.sig.tracecasier.TraceCasierToCasierPanel;
import org.fudaa.fudaa.crue.planimetry.sig.tracecasier.TraceCasierToCasierPreloadProcessor;
import org.fudaa.fudaa.crue.planimetry.sig.tracecasier.TraceCasierToCasierProcessor;
import org.fudaa.fudaa.crue.planimetry.sig.traceprofil.TraceProfilToSectionPanel;
import org.openide.DialogDescriptor;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 *
 * @author Frederic Deniger
 */
public class TraceCasierToCasierAction extends EbliActionSimple {

  private final PlanimetryExternTraceCasierLayer layer;
  final PlanimetryModellingController planimetryModellingController = new PlanimetryModellingController();

  public TraceCasierToCasierAction(PlanimetryExternTraceCasierLayer layer) {
    super(NbBundle.getMessage(TraceCasierToCasierAction.class, "CreateCasierFromTraceCasier.ActionName"), null, "CREATE_CASIER");
    this.layer = layer;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    final TopComponent targetTc = WindowManager.getDefault().findTopComponent(PlanimetryModellingController.TOPCOMPONENT_ID);
    if (targetTc == null) {
      return;
    }
    targetTc.requestActive();
    final PlanimetryControllerHelper helper = layer.getLayerController().getHelper();
    final PlanimetryGisModelContainer planimetryGisModelContainer = helper.getPlanimetryGisModelContainer();
    EMHScenario scenario = planimetryGisModelContainer.getScenario();
    TraceCasierToCasierPreloadProcessor preloader = new TraceCasierToCasierPreloadProcessor(scenario, helper);
    final String defaultSousModele = helper.getDefaultSousModele().getNom();
    TraceCasierSousModeleContent preload = preloader.preload(layer.getModelEditable().getGeomData(), null, defaultSousModele);
    final List<TraceCasierSousModele> tcs = preload.getTraceCasierSousModeles();
    TraceCasierToCasierPanel panel = new TraceCasierToCasierPanel(scenario, tcs, defaultSousModele);
    DialogDescriptor des = new DialogDescriptor(panel, getTitle());
    SysdocUrlBuilder.installDialogHelpCtx(des, "creationCasiersDepuisTraceCasiers", PerspectiveEnum.MODELLING,false);
    DialogHelper.readInPreferences(panel.getView(), "outlineView", getClass());
    boolean accepted = DialogHelper.showQuestion(des, TraceProfilToSectionPanel.class, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    DialogHelper.writeInPreferences(panel.getView(), "outlineView", getClass());
    if (accepted) {
      List<TraceCasierSousModele> acceptedModification = panel.getAcceptedModification();
      if (!acceptedModification.isEmpty()) {
        final String actionName = getTitle() + " / " + layer.getTitle();
        CtuluLogGroup logs = CrueProgressUtils.showProgressDialogAndRun(new TraceCasierToCasierProcessor(helper, acceptedModification, layer,
                preload.getTraceCasierPositionByName()), actionName);
        LogsDisplayer.displayError(logs, actionName);
      }
    }
  }

  @Override
  public void updateStateBeforeShow() {
    setEnabled(planimetryModellingController.isModellingPerspectiveInEditMode());
  }
}
