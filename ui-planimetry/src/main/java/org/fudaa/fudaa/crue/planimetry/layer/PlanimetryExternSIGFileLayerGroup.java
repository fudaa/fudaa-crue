package org.fudaa.fudaa.crue.planimetry.layer;

import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.crue.planimetry.action.SigLoaderAction;
import org.fudaa.fudaa.crue.planimetry.controller.GroupeExternSIGType;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.save.ConfigExternIds;
import org.fudaa.fudaa.sig.layer.FSigVisuPanel;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class PlanimetryExternSIGFileLayerGroup extends AbstractPlanimetryExternLayerGroup {

  final PlanimetryController controller;

  public PlanimetryExternSIGFileLayerGroup(FSigVisuPanel _impl, PlanimetryController controller, String name) {
    super(_impl, null, new String[]{ConfigExternIds.LAYER_LINE_UNMODIFIABLE_SAVE_ID, ConfigExternIds.LAYER_POINT_UNMODIFIABLE_SAVE_ID});
    setName(name);
    setTitle(NbBundle.getMessage(PlanimetryExternSIGFileLayerGroup.class, name));
    this.controller = controller;
    setActions(new EbliActionInterface[]{
              new SigLoaderAction(controller, GroupeExternSIGType.UNMODIFIABLE)});
  }
}
