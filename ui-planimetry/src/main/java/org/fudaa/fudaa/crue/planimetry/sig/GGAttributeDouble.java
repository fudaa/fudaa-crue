/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig;

import org.fudaa.ctulu.gis.GISAttributeDouble;

/**
 *
 * @author Frederic Deniger
 */
public class GGAttributeDouble extends GISAttributeDouble {

  public GGAttributeDouble(String name, String id, boolean atomic) {
    super(name, atomic);
    super.setID(id);
  }


  @Override
  public boolean isUserVisible() {
    return true;
  }
}
