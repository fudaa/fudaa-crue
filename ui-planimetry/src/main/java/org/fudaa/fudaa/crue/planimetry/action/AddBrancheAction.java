package org.fudaa.fudaa.crue.planimetry.action;

import java.awt.Color;
import javax.swing.Icon;
import javax.swing.JComponent;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.edition.BPaletteEdition;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.crue.planimetry.PlanimetryGisPaletteEdition;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class AddBrancheAction extends AbstractAddAction {

  private static Icon createIcon() {
    TraceIconModel model = new TraceIconModel(TraceIcon.LIGNE_OBLIQUE, 10, Color.CYAN);
    model.setEpaisseurLigne(2);
    return new TraceIcon(model);
  }

  public AddBrancheAction(PlanimetryGisPaletteEdition palette, PlanimetryController controller, EditZone zone) {
    super(palette, controller, zone, NbBundle.getMessage(AddBrancheAction.class, "AddBrancheAction.Name"), createIcon(),
          "ADD_BRANCHE");
  }

  @Override
  public String getDefaultPaletteAction() {
    return BPaletteEdition.ADD_POLYLINE_ACTION;
  }

  @Override
  protected JComponent getEditionComponent() {
    return controller.getHelper().getCreationEMH().getBrancheComboBox();
  }

  @Override
  protected ZCalqueAffichageDonnees getLayer() {
    return controller.getBrancheController().getLayer();
  }
}
