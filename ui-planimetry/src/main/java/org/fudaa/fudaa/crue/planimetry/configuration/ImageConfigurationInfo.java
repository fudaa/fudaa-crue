package org.fudaa.fudaa.crue.planimetry.configuration;

import java.util.ArrayList;
import java.util.List;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class ImageConfigurationInfo {

  public static List<Sheet.Set> createSet(DefaultConfiguration in) {
    List<Sheet.Set> res = new ArrayList<>();
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setDisplayName(NbBundle.getMessage(ImageConfigurationInfo.class, "ImageConfiguration.DisplayName"));
    set.setShortDescription(set.getDisplayName());
    res.add(set);
    try {
      set.put(VisuConfigurationInfo.createTransparency(in));
    } catch (Exception exception) {
      Exceptions.printStackTrace(exception);
    }
    return res;
  }
}
