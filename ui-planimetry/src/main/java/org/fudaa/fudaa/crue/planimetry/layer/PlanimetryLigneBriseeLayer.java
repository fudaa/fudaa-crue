package org.fudaa.fudaa.crue.planimetry.layer;

import java.awt.Graphics2D;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.calque.ZSceneSelection;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.commun.EbliListeSelectionMulti;
import org.fudaa.ebli.commun.EbliSelectionState;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.planimetry.configuration.AbstractLigneBriseeConfiguration;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.fudaa.fudaa.sig.layer.FSigLayerLineEditable;

/**
 *
 * @author deniger
 */
public class PlanimetryLigneBriseeLayer<T extends AbstractLigneBriseeConfiguration> extends FSigLayerLineEditable implements PlanimetryLayerContrat {

  T layerConfiguration;

  public PlanimetryLigneBriseeLayer(PlanimetryLigneBriseeLayerModel _modele, FSigEditor _editor) {
    super(_modele, _editor);
    setSelectClosedLineInterior(true);
  }

  @Override
  public PlanimetryLigneBriseeLayerModel modeleDonnees() {
    return (PlanimetryLigneBriseeLayerModel) super.modeleDonnees();
  }

  @Override
  public LayerController getLayerController() {
    return modeleDonnees().getLayerController();
  }

  @Override
  public boolean isEditable() {
    return modeleDonnees().getLayerController().isEditable();
  }


  @Override
  public boolean canAddForme(int _typeForme) {
    return isEditable() && _typeForme != DeForme.POINT && _typeForme != DeForme.MULTI_POINT;
  }

  @Override
  public void paintDonnees(Graphics2D _g, GrMorphisme _versEcran, GrMorphisme _versReel, GrBoite _clipReel) {
    alpha_ = layerConfiguration.getTransparenceAlphaForLayer();
    super.paintDonnees(_g, _versEcran, _versReel, _clipReel);
  }

  @Override
  public boolean addAtome(final GrPoint _ptReel, final CtuluCommandContainer _cmd, final CtuluUI _ui) {
    if (getNbSelected() != 1) {
      return false;
    }
    // En mode non atomique, le point ajouté est obligatoirement sur la polyligne.
    final GrMorphisme versEcran = getVersEcran();
    final GrMorphisme versReel = getVersReel();
    final GrBoite bClip = getDomaine();
    bClip.autoApplique(versEcran);
    final int tolerance = 5;
    // Point à projeter
    // final GrPoint pt = new GrPoint(_xEcran, _yEcran, 0);
    final GrPoint ptEcran = _ptReel.applique(versEcran);
    if ((!bClip.contientXY(ptEcran)) && (bClip.distanceXY(ptEcran) > tolerance)) {
      return false;
    }

    final double distanceReel = GrMorphisme.convertDistanceXY(versReel, tolerance);
    // final GrPoint _ptReel = pt.applique(versReel);
    final GrSegment seg = new GrSegment(new GrPoint(), new GrPoint());
    final GrBoite bPoly = new GrBoite(new GrPoint(), new GrPoint());

    int i = selection_.getMinIndex();
    modele_.getDomaineForGeometry(i, bPoly);
    if (bPoly.contientXY(_ptReel) || bPoly.distanceXY(_ptReel) < distanceReel) {
      for (int j = modele_.getNbPointForGeometry(i) - 1; j > 0; j--) {
        modele_.point(seg.e_, i, j);
        modele_.point(seg.o_, i, j - 1);
        if (seg.distanceXY(_ptReel) < distanceReel) {
          GrPoint ptOnSeg = seg.pointPlusProcheXY(_ptReel);
          modeleDonnees().addPoint(i, j - 1, ptOnSeg.x_, ptOnSeg.y_, null, _cmd);
          atomAdded(i, j - 1);
          return true;
        }
      }
      if (modele_.isGeometryFermee(i)) {
        modele_.point(seg.e_, i, 0);
        modele_.point(seg.o_, i, modele_.getNbPointForGeometry(i) - 1);
        if (seg.distanceXY(_ptReel) < distanceReel) {
          GrPoint ptOnSeg = seg.pointPlusProcheXY(_ptReel);
          final int idxAdded = modele_.getNbPointForGeometry(i) - 1;
          modeleDonnees().addPoint(i, idxAdded, ptOnSeg.x_, ptOnSeg.y_, null, _cmd);
          atomAdded(i, idxAdded);
          return true;
        }
      }
    }
    return false;
  }

  @Override
  protected void atomAdded(int geomIdx, int idxOneLine) {
    FSigEditor editor = (FSigEditor) getEditor();
    editor.getSceneEditor().getScene().setSelectionMode(SelectionMode.ATOMIC);
    editor.getSceneEditor().getScene().clearSelection();
    ZSceneSelection select = new ZSceneSelection(editor.getSceneEditor().getScene());
    EbliListeSelectionMulti multi = new EbliListeSelectionMulti(1);
    multi.add(geomIdx, idxOneLine + 1);
    select.addLayerListSelectionMulti(this, multi);
    editor.getSceneEditor().getScene().changeSelection(select, EbliSelectionState.ACTION_REPLACE);
  }

  @Override
  protected void initTrace(TraceIconModel _icon, int _idxPoly) {
    if (!isSelected(_idxPoly) && getSelectionMode() != SelectionMode.ATOMIC) {
      _icon.setType(TraceIcon.RIEN);
      return;
    }
    layerConfiguration.initTraceAtomicIcon(_icon, _idxPoly, modeleDonnees());
    initTraceWithAlpha(_icon, layerConfiguration.getTransparenceAlphaForLayer());
  }

  @Override
  public boolean setSelectionMode(SelectionMode mode) {
    boolean res = super.setSelectionMode(mode);
    if (res) {
      repaint();
    }
    return res;
  }

  @Override
  protected void initTrace(final TraceLigneModel _ligne, final int _idxPoly) {
    layerConfiguration.initTraceLigne(_ligne, _idxPoly, modeleDonnees());
    initTraceWithAlpha(_ligne, layerConfiguration.getTransparenceAlphaForLayer());
  }

  protected boolean isSelected(int idxGeometry) {
    if (this.getSelectionMode() == SelectionMode.NORMAL && selection_ != null) {
      return selection_.isSelected(idxGeometry);
    }
    if (selectionMulti_ != null) {
      final CtuluListSelection selection = selectionMulti_.get(idxGeometry);
      return selection != null && !selection.isEmpty();
    }
    return false;

  }

  public void setLayerConfiguration(T brancheConfiguration) {
    this.layerConfiguration = brancheConfiguration;
  }

  public T getLayerConfiguration() {
    return layerConfiguration;
  }
}
