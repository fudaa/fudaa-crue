/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.openide.util.NbBundle;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Frederic Deniger
 */
public class SectionProfilToBrancheProcessorValidator {
  private final Map<String, EMH> emhByNom;
  private final CrueConfigMetier ccm;

  public SectionProfilToBrancheProcessorValidator(EMHScenario scenario, CrueConfigMetier ccm) {
    emhByNom = scenario.getIdRegistry().getEmhByNom();
    this.ccm = ccm;
  }

  CtuluLogResult<List<SectionProfilToBrancheParametersChange>> validate(SectionProfilToBrancheParametersComputed in) {
    CtuluLog log = new CtuluLog();
    log.setDesc(NbBundle.getMessage(SectionProfilToBrancheProcessorValidator.class, "traceProfil.attacheToBrancheValidation.logName"));
    Map<SectionProfilToBrancheParametersSection, SectionProfilToBrancheParametersComputed.IntersectedBranche> brancheByParameter = in.
        getTargetsBrancheByParameter();
    if (brancheByParameter.isEmpty()) {
      log.addInfo(NbBundle.getMessage(
          SectionProfilToBrancheProcessorValidator.class, "traceProfil.attacheToBrancheValidation.noBranchesFound"));
    }
    List<SectionProfilToBrancheParametersChange> params = new ArrayList<>();
    for (Map.Entry<SectionProfilToBrancheParametersSection, SectionProfilToBrancheParametersComputed.IntersectedBranche> entry : brancheByParameter.
        entrySet()) {
      SectionProfilToBrancheParametersSection section = entry.getKey();
      SectionProfilToBrancheParametersComputed.IntersectedBranche targetBranche = entry.getValue();
      final boolean severalIntersections = targetBranche.getBrancheNames().size() > 1;
      if (severalIntersections) {
        final Collection<String> names = TransformerHelper.toId(targetBranche.getBrancheNames());
        log.addWarn(
            NbBundle.getMessage(
                SectionProfilToBrancheProcessorValidator.class, "traceProfil.attacheToBrancheValidation.tooManyBranches",
                section.getSectionName(), StringUtils.join(names, "; ")));
      } else if (targetBranche.getBrancheNames().size() == 1) {
        final SectionProfilToBrancheParametersComputed.BrancheIntersection brancheIntersection = targetBranche.getBrancheNames().get(0);
        final String newBranche = brancheIntersection.getBrancheName();

        CatEMHSection emhSection = (CatEMHSection) emhByNom.get(section.getSectionName());
        CatEMHBranche branche = (CatEMHBranche) emhByNom.get(newBranche);
        if (EMHHelper.getParent(branche) != EMHHelper.getParent(emhSection)) {
          log.addWarn(
              NbBundle.getMessage(
                  SectionProfilToBrancheProcessorValidator.class, "traceProfil.attacheToBrancheValidation.notSameSousModele",
                  section.getSectionName(), newBranche));
        } else {
          String oldBranche = StringUtils.EMPTY;
          if (emhSection.getBranche() != null) {
            oldBranche = emhSection.getBranche().getNom();
          }
          Double oldXp = getXp(emhSection);
          String oldXpAsString = StringUtils.EMPTY;
          NumberFormat formatter = ccm.getFormatter(CrueConfigMetierConstants.PROP_XP, DecimalFormatEpsilonEnum.COMPARISON);
          if (oldXp != null) {
            oldXpAsString = formatter.format(oldXp);
          }
          SectionProfilToBrancheParametersChange paramChange = new SectionProfilToBrancheParametersChange(
              emhSection.getNom(), oldBranche, oldXpAsString);
          paramChange.setIdxTraceInGeom(section.getIdxInCalque());
          paramChange.setTraceGisModel(section.getZone());
          final double newXp = branche.getLength() * brancheIntersection.getXpSchematique() / brancheIntersection.getLongueurSchematique();
          paramChange.setNewXp(newXp, formatter.format(newXp));
          paramChange.setNewBrancheName(newBranche);
          params.add(paramChange);
        }
      }
    }
    return new CtuluLogResult<>(params, log);
  }

  public Double getXp(CatEMHSection emhSection) {
    if (emhSection.getBranche() != null) {
      List<RelationEMHSectionDansBranche> listeSections = emhSection.getBranche().getListeSections();
      for (RelationEMHSectionDansBranche relation : listeSections) {
        if (relation.getEmhNom().equals(emhSection.getNom())) {
          return relation.getXp();
        }
      }
    }
    return null;
  }
}
