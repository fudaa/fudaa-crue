package org.fudaa.fudaa.crue.planimetry.configuration.node;

import org.fudaa.fudaa.crue.planimetry.configuration.DefaultConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.ImageConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryImageRasterLayer;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.lookup.Lookups;

import java.util.List;

/**
 *
 * @author deniger
 */
public class ImageConfigurationNode extends AdditionalConfigurationNode {

  public ImageConfigurationNode(DefaultConfiguration imageLayer, PlanimetryImageRasterLayer layer) {
    super(Children.LEAF, Lookups.fixed(imageLayer, layer));
    setDisplayName(layer.getTitle());
  }

  PlanimetryAdditionalLayerContrat getLayer() {
    return getLookup().lookup(PlanimetryImageRasterLayer.class);
  }

  @Override
  public void applyModification() {
    DefaultConfiguration config = getLookup().lookup(DefaultConfiguration.class);
    PlanimetryImageRasterLayer layer = getLookup().lookup(PlanimetryImageRasterLayer.class);
    layer.setLayerConfiguration(config.copy());
    layer.repaint();

  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = new Sheet();
    DefaultConfiguration lookup = getLookup().lookup(DefaultConfiguration.class);
    List<Set> createSet = ImageConfigurationInfo.createSet(lookup);
    for (Set set : createSet) {
      sheet.put(set);
    }
    return sheet;
  }
}
