/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.save;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.conf.CrueEtudeConfigurationHelper;
import org.fudaa.dodico.crue.io.conf.CrueEtudeDaoConfiguration;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternDaoConfiguration;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;
import org.openide.util.Exceptions;

/**
 *
 * @author Frederic Deniger
 */
public class ConfigLoaderExtern {

  public static class ResultExtern {

    public CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    public List<PlanimetryAdditionalLayerContrat> additionnalLayers;

    public ResultExtern() {
      log.setDesc("configEtuExtern.load");
    }
  }

  public static ResultExtern loadConfigEtudeExtern(EMHProjet projet) {
    ResultExtern load = new ResultExtern();
    try {
      if (!projet.getInfos().isDirOfConfigDefined()) {
        return null;
      }
      final File dirOfConfig = projet.getInfos().getDirOfConfig();
      File configEtuExtern = new File(dirOfConfig, GlobalOptionsManager.ETUDE_FILE_EXTERNE);
      if (configEtuExtern.exists()) {
        loadConfigEtudeExterrn(configEtuExtern, load, dirOfConfig);
      }
    } catch (Exception e) {
      Exceptions.printStackTrace(e);
    }
    return load;
  }

  private static void loadConfigEtudeExterrn(File configEtuExtern, ResultExtern load, final File dirOfConfig) {
    CrueIOResu<CrueEtudeExternDaoConfiguration> read = CrueEtudeConfigurationHelper.readExtern(configEtuExtern);
    if (read.getAnalyse().containsErrorOrSevereError()) {
      LogsDisplayer.displayError(read.getAnalyse(), configEtuExtern.getAbsolutePath());
      load.log = read.getAnalyse();
      return;
    }
    AdditionalLayerLoaderProcess process = new AdditionalLayerLoaderProcess();
    CrueIOResu<List<PlanimetryAdditionalLayerContrat>> loaded = process.load(read.getMetier().getRessources(), dirOfConfig);
    load.log = loaded.getAnalyse();
    load.additionnalLayers = loaded.getMetier();
    if (loaded.getAnalyse().isNotEmpty()) {
      LogsDisplayer.displayError(loaded.getAnalyse(), configEtuExtern.getAbsolutePath());
    }
  }

  public static Map<String, String> readDefaultPanimetryConfig(ConfigurationManagerService configurationManagerService) {
    CrueEtudeDaoConfiguration readDefaultEtudeConfig = configurationManagerService.readDefaultEtudeConfig();
    if(readDefaultEtudeConfig==null){
      return Collections.emptyMap();
    }
    return CrueEtudeConfigurationHelper.getPlanimetryProperties(readDefaultEtudeConfig);
  }
}
