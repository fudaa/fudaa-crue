package org.fudaa.fudaa.crue.planimetry.listener;

import org.fudaa.ebli.calque.edition.ZSceneEditorListener;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;

/**
 *
 * @author deniger
 */
public class PlanimetrySceneEditorListener implements ZSceneEditorListener {

  boolean globalChangedStarted;
  final PlanimetryController controller;

  public PlanimetrySceneEditorListener(PlanimetryController controller) {
    this.controller = controller;
  }

  public boolean isGlobalChangedStarted() {
    return globalChangedStarted;
  }

  /**
   * Permet aux calques de se recalculer qu'a la fin d'une mise à jour massive. Attenion: il faut s'assurer que
   * setGlobalChangeFinished soit appelé à la fin.
   * @param stopUpdatingTraceSection si true, les positions des traces sections ne seront plus recalculées. Elles le seront lors de l'appel à {@link #setGlobalChangeFinished()}
   */
  public void setGlobalChangeStarted(boolean stopUpdatingTraceSection) {
    globalChangedStarted = true;
    if(stopUpdatingTraceSection){
      controller.getSectionController().globalChangedStarted();
      controller.getTraceController().globalChangedStarted();
    }
  }

  public void setGlobalChangeFinished() {
    globalChangedStarted = false;
    controller.getBrancheController().globalChangedFinished();
    controller.getCasierController().globalChangedFinished();
    controller.getNodeController().globalChangedFinished();
    controller.getSectionController().globalChangedFinished();
    controller.getTraceController().globalChangedFinished();
  }

  @Override
  public void globalMovedEnd() {
    setGlobalChangeFinished();

  }

  @Override
  public void globalMovedStart() {
    setGlobalChangeStarted(true);
  }

  @Override
  public void globalRotationEnd() {
    setGlobalChangeFinished();
  }

  @Override
  public void globalRotationStart() {
    setGlobalChangeStarted(true);
  }
}
