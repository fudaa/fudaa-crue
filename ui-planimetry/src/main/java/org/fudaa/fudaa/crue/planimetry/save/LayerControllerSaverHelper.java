package org.fudaa.fudaa.crue.planimetry.save;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.ctulu.converter.ToStringTransformerFactory;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessourceInfos;
import org.fudaa.dodico.crue.io.conf.Option;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.fudaa.crue.common.config.ConfigSaverHelper;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;
import org.fudaa.fudaa.sig.FSigGeomSrcData;
import org.fudaa.fudaa.sig.wizard.FSigFileLoadResult;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderI;
import org.openide.nodes.Node.Property;
import org.openide.nodes.Sheet.Set;
import org.openide.util.Exceptions;

/**
 *
 * @author deniger
 */
public class LayerControllerSaverHelper {

  public static final String SAVE_ID = "IMAGE";
  public static final String PATH_ABSOLUTE_ID = "FileAbsolutePath";
  public static final String PATH_RELATIVE_ID = "FileRelativePath";
  public static final String GEO_FORMAT = "Format";
  public static final String TRACE_FILE_ID = "TraceFileId";
  public static final String VISIBLE = "Visible";

  public static void restorVisibleProperty(Map<String, String> optionsBykey, BCalque layer) {
    //on sauvegarde les propriétés graphiques:
    layer.setVisible((Boolean) ToStringTransformerFactory.createTransformers().get(Boolean.TYPE).fromString(optionsBykey.get(
            VISIBLE)));
  }

  public void addVisibleProperty(CrueEtudeExternRessourceInfos ressource, BCalque layer) {
    //on sauvegarde les propriétés graphiques:
    ressource.getOptions().add(new Option(VISIBLE, ToStringTransformerFactory.createTransformers().get(Boolean.TYPE).toString(
            layer.isVisible())));
  }

  /**
   *
   * @param optionsBykey les options par clés
   * @param ressourceName nom de la ressourrce
   * @param file le fichier
   * @param log le log
   * @return null si erreur
   */
  public Pair<FSigGeomSrcData, SigFormatHelper.EnumFormat> loadGisData(File file, Map<String, String> optionsBykey, String ressourceName, CtuluLog log) {
    String fmtAsString = optionsBykey.remove(LayerControllerSaverHelper.GEO_FORMAT);
    if (fmtAsString == null) {
      log.addError("externConfig.geo.NoFormatFound.error", ressourceName);
      return null;
    }
    SigFormatHelper.EnumFormat fmt = SigFormatHelper.isKnown(fmtAsString);
    return loadGisData(file, fmt, ressourceName, log);
  }

  public static Pair<FSigGeomSrcData, SigFormatHelper.EnumFormat> loadFile(File file, CtuluLog log) {
    File toDo = file;
    if (!file.exists()) {
      toDo = CtuluLibFile.changeExtension(file, "gml");
    }
    if (!toDo.exists()) {
      return null;
    }
    SigFormatHelper.EnumFormat fmt = SigFormatHelper.EnumFormat.SHP;
    if (toDo.getName().toLowerCase().endsWith(PlanimetryGISGMLZoneExporter.GML_EXTENSION)) {
      fmt = SigFormatHelper.EnumFormat.GML;
    }
    FSigFileLoaderI loader = SigFormatHelper.getLoader(fmt);
    FSigFileLoadResult result = new FSigFileLoadResult();
    CtuluAnalyze analyse = new CtuluAnalyze();
    loader.setInResult(result, toDo, toDo.getAbsolutePath(), CtuluUIForNetbeans.DEFAULT.getMainProgression(),
            analyse);
    FSigGeomSrcData createDataWithAtomic = result.createDataWithAtomic(false);
    return new Pair<>(createDataWithAtomic, fmt);
  }

  public File retrieveFiles(Map<String, String> optionsBykey, CrueEtudeExternRessourceInfos ressource, File baseDir, CtuluLog log) {
    String relative = optionsBykey.remove(PATH_RELATIVE_ID);
    String absolute = optionsBykey.remove(PATH_ABSOLUTE_ID);
    if (relative == null) {
      log.addError("externConfig.propertyNotFound.error", PATH_RELATIVE_ID, ressource.getNom());
      return null;
    }
    if (absolute == null) {
      log.addError("externConfig.propertyNotFound.error", PATH_ABSOLUTE_ID, ressource.getNom());
      return null;
    }
    File relativeFile = new File(baseDir, relative).getAbsoluteFile();
    File image = relativeFile;
    if (!image.exists()) {
      image = new File(absolute);
    }
    if (!image.exists()) {
      log.addError("externConfig.fileNotFound.error", ressource.getNom(), relativeFile.getAbsolutePath(),
              image.getAbsolutePath());
      return null;
    }
    return image;
  }

  public void putRelativeAndAbsolutePaths(CrueEtudeExternRessourceInfos ressource, File file, File baseDir) {
    //les liens vers l'image
    addOption(ressource, PATH_ABSOLUTE_ID, file.getAbsolutePath());
    String relativeFile = CtuluLibFile.getRelativeFile(file, baseDir, 10);
    addOption(ressource, PATH_RELATIVE_ID, relativeFile);
  }

  public void addOption(CrueEtudeExternRessourceInfos ressource, String key, String value) {
    //les liens vers l'image
    ressource.getOptions().add(new Option(key, value));
  }

  public Map<String, String> toMap(CrueEtudeExternRessourceInfos ressource) {
    Map<String, String> optionsBykey = new HashMap<>();
    List<Option> options = ressource.getOptions();
    for (Option option : options) {
      optionsBykey.put(option.getNom(), option.getValeur());
    }
    return optionsBykey;
  }

  public static void restoreVisuProperties(Map<String, String> optionsBykey, PlanimetryAdditionalLayerContrat layer,
          List<Set> createSet,
          CtuluLog log) {
    restorVisibleProperty(optionsBykey, (BCalque) layer);
    optionsBykey.remove(VISIBLE);
    LinkedHashMap<String, Property> nodeProperties = ConfigSaverHelper.getNodeProperties(createSet);
    Map<Class, AbstractPropertyToStringTransformer> createTransformers = ToStringTransformerFactory.createTransformers();
    for (Map.Entry<String, String> readProperties : optionsBykey.entrySet()) {
      String key = readProperties.getKey();
      String value = readProperties.getValue();
      Property property = nodeProperties.get(key);
      if (property == null) {
        log.addWarn("configEtu.unknownProperty.Error", key);
      } else {
        AbstractPropertyToStringTransformer transformer = createTransformers.get(property.getValueType());
        if (transformer == null) {
          log.addWarn("configEtu.unknownType.Error", property.getValueType());
        } else if (!transformer.isValid(value)) {
          log.addWarn("configEtu.badValue.Error", key, value);
        } else {
          try {
            property.setValue(transformer.fromString(value));
          } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
            log.addWarn("configEtu.valueCantBeSet.Error", key, value);
          }
        }
      }
    }
  }

  public Pair<FSigGeomSrcData, SigFormatHelper.EnumFormat> loadGisData(File file, SigFormatHelper.EnumFormat fmt,
          String ressourceName, CtuluLog log) {
    if (fmt == null) {
      log.addError("externConfig.geo.NoFormatFound.error", ressourceName);
      return null;
    }
    FSigFileLoaderI loader = SigFormatHelper.getLoader(fmt);
    FSigFileLoadResult result = new FSigFileLoadResult();
    CtuluAnalyze analyse = new CtuluAnalyze();
    loader.setInResult(result, file, file.getAbsolutePath(), CtuluUIForNetbeans.DEFAULT.getMainProgression(),
            analyse);
    FSigGeomSrcData createDataWithAtomic = result.createDataWithAtomic(false);

    return new Pair<>(createDataWithAtomic, fmt);
  }
}
