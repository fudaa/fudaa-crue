package org.fudaa.fudaa.crue.planimetry.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;
import javax.swing.SwingConstants;
import org.fudaa.fudaa.crue.common.config.ConfigurationInfoHelper;
import org.fudaa.fudaa.crue.common.editor.CodeTranslation;
import org.fudaa.fudaa.crue.common.editor.JcomboBoxIntegerInplaceEditor;
import org.fudaa.fudaa.crue.planimetry.configuration.editor.SectionPositionPropertyEditorSupport;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.PropertySupport.Reflection;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 * Attention. Les name des set sont utilisés pour la persistence dans le xml. Donc, ne pas changer sans rejouer les tests unitaires et faire des
 * reprises
 *
 * @author deniger
 */
public class TraceConfigurationInfo {

  private TraceConfigurationInfo(){}

  static final String PREFIX_TRACES = "traces";

  public static List<Sheet.Set> createSet(TraceConfiguration in) {
    List<Sheet.Set> res = new ArrayList<>();
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setName(PREFIX_TRACES);
    set.put(VisuConfigurationInfo.createTransparency(in));
    try {
      set.put(ConfigurationInfoHelper.create(TraceConfiguration.PROPERTY_PROFIL_VUE_DE_AMONT, Boolean.TYPE, in,
              "TraceConfiguration.profilVueDeAmont", VisuConfigurationInfo.class));
      set.setDisplayName(NbBundle.getMessage(TraceConfigurationInfo.class, "TraceConfiguration.DisplayName"));
      set.setShortDescription(set.getDisplayName());
      res.add(set);

      res.add(TraceLineModelConfigurationInfo.createSet(in.lineModel, PREFIX_TRACES, true));

      //label nom de section
      final Reflection createSectionNamePosition = createSectionNamePosition(in);
      final Set labelSection = LabelConfigurationInfo.createSet(in.getSectionLabelConfiguration(), PREFIX_TRACES,
              createSectionNamePosition, true);
      labelSection.put(createSectionNamePosition);
      labelSection.setDisplayName(NbBundle.getMessage(TraceConfigurationInfo.class, "TraceConfiguration.LabelSection.DisplayName", VisuConfigurationInfo.class));
      labelSection.setShortDescription(labelSection.getDisplayName());
      res.add(labelSection);

      //limite de lits
      final Set setLitIcones = TraceIconModelConfigurationInfo.createSet(in.iconModelLimiteLit, PREFIX_TRACES + ".lits", true);
      setLitIcones.setDisplayName(NbBundle.getMessage(TraceConfigurationInfo.class, "TraceConfiguration.IconLits.DisplayName", VisuConfigurationInfo.class));
      res.add(setLitIcones);

      final Set setLitIconesEtiquette = TraceIconModelConfigurationInfo.createSet(in.iconModelPtProfil, PREFIX_TRACES + ".ptProfil", true);
      setLitIconesEtiquette.setDisplayName(NbBundle.getMessage(TraceConfigurationInfo.class, "TraceConfiguration.IconPtProfil.DisplayName", VisuConfigurationInfo.class));
      res.add(setLitIconesEtiquette);



      //label des lits
      final Set labelLits = LabelConfigurationInfo.createSet(in.getLitLabelConfiguration(), PREFIX_TRACES + ".lits", true);
      labelLits.setDisplayName(NbBundle.getMessage(TraceConfigurationInfo.class, "TraceConfiguration.LabelLits.DisplayName", VisuConfigurationInfo.class));
      res.add(labelLits);
    } catch (NoSuchMethodException ex) {
      Exceptions.printStackTrace(ex);
    }
    return res;
  }

  private static Reflection createSectionNamePosition(TraceConfiguration in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(TraceConfiguration.PROPERTY_SECTION_LABEL_POSITION,
            Integer.TYPE, in, "TraceConfiguration.SectionLabelPosition", VisuConfigurationInfo.class);
    CodeTranslation codes = createCodes();

    JcomboBoxIntegerInplaceEditor inplaceEditor = new JcomboBoxIntegerInplaceEditor(codes);
    res.setPropertyEditorClass(SectionPositionPropertyEditorSupport.class);
    res.setValue("inplaceEditor", inplaceEditor);
    return res;
  }

  public static CodeTranslation createCodes() throws MissingResourceException {
    CodeTranslation codes = new CodeTranslation();
    codes.intValues.add(SwingConstants.RIGHT);
    codes.strings.add(NbBundle.getMessage(TraceConfigurationInfo.class, "label.position.right"));
    codes.intValues.add(SwingConstants.LEFT);
    codes.strings.add(NbBundle.getMessage(TraceConfigurationInfo.class, "label.position.left"));
    return codes;
  }
}
