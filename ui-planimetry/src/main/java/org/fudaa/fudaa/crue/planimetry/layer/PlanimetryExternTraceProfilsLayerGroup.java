package org.fudaa.fudaa.crue.planimetry.layer;

import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.crue.planimetry.action.TraceProfilSectionImporterAction;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.save.ConfigExternIds;
import org.fudaa.fudaa.sig.layer.FSigVisuPanel;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class PlanimetryExternTraceProfilsLayerGroup extends AbstractPlanimetryExternLayerGroup {

  final PlanimetryController controller;

  public PlanimetryExternTraceProfilsLayerGroup(FSigVisuPanel _impl, PlanimetryController controller, String name) {
    super(_impl, null, new String[]{ConfigExternIds.LAYER_TRACES_PROFILS_SAVE_ID});
    setName(name);
    setTitle(NbBundle.getMessage(PlanimetryExternTraceProfilsLayerGroup.class, name));
    this.controller = controller;
    setActions(new EbliActionInterface[]{
      new TraceProfilSectionImporterAction(controller)});
  }
}
