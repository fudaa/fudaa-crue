/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.tracecasier;

import java.util.List;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.planimetry.sig.traceprofil.TraceProfilSousModele;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frederic Deniger
 */
public class TraceCasierSousModeleNode extends AbstractNodeFirable {

  public TraceCasierSousModeleNode(TraceCasierSousModele object, List<String> sousModeles) {
    super(Children.LEAF, Lookups.fixed(object, sousModeles));
    setName(object.getCasierName());
    setDisplayName(object.getCasierName());
    if (object.isNoeudOut()) {
      setIconBaseWithExtension(LogIconTranslationProvider.getIconBase(CtuluLogLevel.WARNING));
    }
  }

  @Override
  public boolean isEditMode() {
    return true;
  }

  @Override
  protected void fireObjectChange(String property, Object oldValue, Object newValue) {
    super.fireObjectChange(property, oldValue, newValue);
    super.fireObjectChange(TraceCasierSousModele.PROP_ACTION_BILAN, oldValue, newValue);

  }

  @Override
  protected Sheet createSheet() {
    TraceCasierSousModele tc = getLookup().lookup(TraceCasierSousModele.class);
    List<String> sousModele = getLookup().lookup(List.class);
    Sheet res = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    res.put(set);
    PropertyNodeBuilder builder = new PropertyNodeBuilder();
    List<PropertySupportReflection> createFromPropertyDesc = builder.createFromPropertyDesc(DecimalFormatEpsilonEnum.COMPARISON, tc, this);
    for (PropertySupportReflection prop : createFromPropertyDesc) {
      set.put(prop);
      if (TraceProfilSousModele.PROP_SOUS_MODELE.equals(prop.getName())) {
        if (!tc.cansSousModeleBeChanged()) {
          prop.setCanWrite(false);
        } else {
          try {
            prop.setValue(sousModele.get(0));
            prop.setTags(sousModele);
          } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
          }
        }
      } else if (TraceProfilSousModele.PROP_ACCEPT_MODIFICATION.equals(prop.getName())) {
        prop.setCanWrite(tc.canActionBeModified());
      }

    }
    return res;
  }
}
