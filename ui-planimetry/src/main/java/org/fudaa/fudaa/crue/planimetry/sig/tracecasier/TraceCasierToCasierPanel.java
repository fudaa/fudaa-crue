/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.tracecasier;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.view.DefaultOutlineViewEditor;
import org.fudaa.fudaa.crue.planimetry.sig.traceprofil.TraceProfilToSectionPanel;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class TraceCasierToCasierPanel extends DefaultOutlineViewEditor {

  public TraceCasierToCasierPanel(final EMHScenario scenario, final List<TraceCasierSousModele> tcs, final String defaultSousModele) {
    final List<TraceCasierSousModeleNode> nodes = new ArrayList<>();
    final List<String> sousModelesName = TransformerHelper.toNom(scenario.getSousModeles());
    for (final TraceCasierSousModele traceCasier : tcs) {
      nodes.add(new TraceCasierSousModeleNode(traceCasier, sousModelesName));
    }
    final JPanel top = new JPanel(new BorderLayout());
    final JPanel changeSousModelPanel = createChangeSousModelePanel(sousModelesName, defaultSousModele);
    final JPanel selectAllPanel = createSelectAllPanel();

    top.add(changeSousModelPanel, BorderLayout.WEST);
    top.add(selectAllPanel, BorderLayout.EAST);
    add(top, BorderLayout.NORTH);
    final Node createNode = NodeHelper.createNode(nodes, "All");
    getExplorerManager().setRootContext(createNode);
  }

  public List<TraceCasierSousModele> getAcceptedModification() {
    final List<TraceCasierSousModele> res = new ArrayList<>();
    final Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    for (final Node node : nodes) {
      final TraceCasierSousModele tc = node.getLookup().lookup(TraceCasierSousModele.class);
      if (tc.getSousModeleName() != null && tc.isAddOrModifiy() && tc.canActionBeModified()) {
        res.add(tc);
      }
    }
    return res;
  }

  protected void changeAllSousModel(final String newOne) {
    final Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    for (final Node node : nodes) {
      final TraceCasierSousModele lookup = node.getLookup().lookup(TraceCasierSousModele.class);
      if (lookup.cansSousModeleBeChanged()) {
        final AbstractNodeFirable myNode = (AbstractNodeFirable) node;
        try {
          myNode.find(TraceCasierSousModele.PROP_SOUS_MODELE).setValue(newOne);
        } catch (final Exception ex) {
          Logger.getLogger(TraceProfilToSectionPanel.class.getName()).log(Level.INFO, "message {0}", ex);
        }
      }
    }
  }

  protected void selectAll(final boolean newOne) {
    final Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    for (final Node node : nodes) {
      try {
        final AbstractNodeFirable myNode = (AbstractNodeFirable) node;
        final TraceCasierSousModele lookup = node.getLookup().lookup(TraceCasierSousModele.class);
        if (lookup.canActionBeModified()) {
          myNode.find(TraceCasierSousModele.PROP_ACCEPT_MODIFICATION).setValue(newOne);
        }
      } catch (final Exception ex) {
        Logger.getLogger(TraceProfilToSectionPanel.class.getName()).log(Level.INFO, "message {0}", ex);
      }
    }
  }

  @Override
  protected void initActions() {
  }

  @Override
  protected void addElement() {
  }

  @Override
  protected void createOutlineView() {
    view = new OutlineView(NbBundle.getMessage(TraceCasierSousModele.class, TraceCasierSousModele.I18N_CASIER_NAME));
    view.getOutline().setRootVisible(false);
    view.setPropertyColumns(
            //            TraceCasierSousModele.PROP_CASIER_NAME, NbBundle.getMessage(TraceCasierSousModele.class, TraceCasierSousModele.I18N_CASIER_NAME),
            TraceCasierSousModele.PROP_SOUS_MODELE, NbBundle.getMessage(TraceCasierSousModele.class, TraceCasierSousModele.I18N_TARGET_SOUS_MODELE),
            TraceCasierSousModele.PROP_CONTAIN_PROFIL, NbBundle.getMessage(TraceCasierSousModele.class, TraceCasierSousModele.I18N_CONTAIN_PROFIL),
            TraceCasierSousModele.PROP_ACCEPT_MODIFICATION, NbBundle.getMessage(TraceCasierSousModele.class,
            TraceCasierSousModele.I18N_ACCEPT_MODIFICATION),
            TraceCasierSousModele.PROP_ACTION_BILAN, NbBundle.getMessage(TraceCasierSousModele.class, TraceCasierSousModele.I18N_ACTION_BILAN));
  }

  private JPanel createChangeSousModelePanel(final List<String> sousModelesName, final String defaultSelected) {
    final JComboBox cb = new JComboBox(sousModelesName.toArray());
    final JButton bt = new JButton(NbBundle.getMessage(TraceProfilToSectionPanel.class, "MassiveChangeSousModele.ActionName"));
    bt.setToolTipText(NbBundle.getMessage(TraceProfilToSectionPanel.class, "MassiveChangeSousModele.Tooltip"));
    final JPanel changeSousModel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    changeSousModel.add(cb);
    if (defaultSelected != null) {
      cb.setSelectedItem(defaultSelected);
    }
    changeSousModel.add(bt);
    bt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        changeAllSousModel((String) cb.getSelectedItem());
      }
    });
    return changeSousModel;
  }

  private JPanel createSelectAllPanel() {
    final JPanel pnActiveUnactive = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    final JButton btActive = new JButton(NbBundle.getMessage(TraceProfilToSectionPanel.class, "AcceptAll.Button"));
    btActive.setToolTipText(NbBundle.getMessage(TraceProfilToSectionPanel.class, "AcceptAll.Tooltip"));
    final JButton btUnActive = new JButton(NbBundle.getMessage(TraceProfilToSectionPanel.class, "RefuseAll.Button"));
    btUnActive.setToolTipText(NbBundle.getMessage(TraceProfilToSectionPanel.class, "RefuseAll.Tooltip"));
    btActive.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        selectAll(true);
      }
    });
    btUnActive.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        selectAll(false);
      }
    });

    pnActiveUnactive.add(btActive);
    pnActiveUnactive.add(btUnActive);
    return pnActiveUnactive;
  }
}
