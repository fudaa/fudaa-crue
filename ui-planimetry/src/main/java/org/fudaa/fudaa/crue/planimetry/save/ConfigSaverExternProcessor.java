/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.save;

import java.io.File;
import java.util.List;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.io.conf.CrueEtudeConfigurationHelper;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternAdditionalFileSaver;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternSaveContent;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;

/**
 *
 * @author Frederic Deniger
 */
public class ConfigSaverExternProcessor {

  private final EMHProjet project;

  public ConfigSaverExternProcessor(EMHProjet project) {
    this.project = project;
  }

  public CtuluLogGroup saveAdditionalLayers(PlanimetryController planimetryController) {
    ConfigSaverExtern saverExtern = new ConfigSaverExtern();
    File destDir = project.getInfos().getDirOfConfig();
    CrueEtudeExternSaveContent externConf = saverExtern.getExternProperties(planimetryController, destDir);
    //on nettoie le dossier des dessins
    File dessinsDir = ConfigSaverExtern.getDessinDir(destDir);
    if (dessinsDir.exists()) {
      CtuluLibFile.deleteDir(dessinsDir);
    }
    dessinsDir.mkdirs();
    //on nettoie le dossier des trace profils:
    new LayerTraceProfilsControllerSaver().clearDirTraceProfil(destDir, planimetryController);
    //on nettoie le dossier des trace casiers:
    new LayerTraceCasiersControllerSaver().clearDirTraceProfil(destDir, planimetryController);
    //on écrit les fichiers
    CrueEtudeConfigurationHelper.writeExtern(new File(destDir, GlobalOptionsManager.ETUDE_FILE_EXTERNE), externConf.getConfig());
    List<CrueEtudeExternAdditionalFileSaver> additionalFileSaver = externConf.getAdditionalFileSaver();
    for (CrueEtudeExternAdditionalFileSaver additional : additionalFileSaver) {
      additional.save(destDir, dessinsDir);
    }
    CtuluLogGroup gr = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    gr.setDescription("save.configEtuExtern.done");
    return gr;
  }
}
