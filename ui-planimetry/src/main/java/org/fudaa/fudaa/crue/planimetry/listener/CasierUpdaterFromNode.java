package org.fudaa.fudaa.crue.planimetry.listener;

import org.fudaa.ctulu.gis.*;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.fudaa.crue.planimetry.controller.LayerCasierController;
import org.fudaa.fudaa.crue.planimetry.controller.LayerNodeController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryCasierLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryCasierLayerModel;
import org.locationtech.jts.algorithm.locate.PointOnGeometryLocator;
import org.locationtech.jts.geom.Coordinate;

/**
 * Utilise pour déplacer les branhces si un noeud amon/aval est modifié.
 *
 * @author deniger
 */
public class CasierUpdaterFromNode extends AbstractGeomUpdater {
  private final LayerCasierController casierController;

  public CasierUpdaterFromNode(LayerCasierController ctx) {
    this.casierController = ctx;
  }

  @Override
  public void attributeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _action) {
  }

  @Override
  public void attributeValueChangeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _indexObj,
                                         Object _newValue) {
  }

  @Override
  protected PlanimetryController getController() {
    return casierController.getHelper().getController();
  }

  @Override
  protected void updateAfterRemove() {
    PlanimetryCasierLayerModel model = casierController.getLayer().modeleDonnees();
    model.updateAfterRemove();
  }

  @Override
  public void updateAll() {
    final LayerNodeController nodeController = casierController.getHelper().getNodeController();
    final GISZoneCollectionPoint nodeCollection = nodeController.getNodeCollection();
    for (int i = 0; i < nodeCollection.size(); i++) {
      update(i);
    }
  }

  @Override
  protected void update(int indexOfPoint) {
    final LayerNodeController nodeController = casierController.getHelper().getNodeController();
    final GISZoneCollectionPoint nodeCollection = nodeController.getNodeCollection();
    GISPoint newPoint = (GISPoint) nodeCollection.getGeometry(indexOfPoint);
    Long nodeUid = nodeController.getUid(indexOfPoint);
    CatEMHNoeud nd = nodeController.getEMH(nodeUid);
    CatEMHCasier casier = nd.getCasier();
    if (casier == null) {
      return;
    }
    int casierPosition = casierController.getCasierPosition(casier.getUiId());
    if (casierPosition >= 0) {
      GISPolygone geometry = (GISPolygone) casierController.getCasierCollection().getGeometry(casierPosition);
      PointOnGeometryLocator tester = GISLib.createPolygoneTester(geometry);
      if (!GISLib.isInside(tester, newPoint.getCoordinate())) {
        Coordinate oldCenter = PlanimetryCasierLayer.getCenter(geometry);
        double dx = newPoint.getX() - oldCenter.x;
        double dy = newPoint.getY() - oldCenter.y;
        if (Math.abs(dx) < 1e-2 && Math.abs(dy) < 1e-2) {
          return;
        }
        GISPolygone translated = geometry.createTranslateGeometry(new GISPoint(dx, dy, 0));
        casierController.getCasierCollection().setCoordinateSequence(casierPosition,
            translated.getCoordinateSequence(), null);
      }
    }
  }
}
