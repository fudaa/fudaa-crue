package org.fudaa.fudaa.crue.planimetry;

import java.beans.PropertyChangeListener;
import org.fudaa.fudaa.crue.common.services.ModificationStateContainer;

/**
 *
 * @author deniger
 */
public class PlanimetryCalqueState {

  /**
   * les données de niveau scenario sont modifiées
   */
  public static final String PROPERTY_SCENARIO_MODIFIED = "scenarioModified";
  /**
   * si une donnée de géoloc est modifiée
   */
  public static final String PROPERTY_GEOLOC_MODIFIED = "geoLocModified";
  /**
   * si une donnée de niveau étude est modifiée
   */
  public static final String PROPERTY_STUDYCONFIG_MODIFIED = "studyConfigModified";
  /**
   * si une donnée de niveau étude est modifiée
   */
  public static final String PROPERTY_EXTERN_DATA_MODIFIED = "externDataModified";
  final ModificationStateContainer modificationStateContainer = new ModificationStateContainer();

  public void addModifiedListener(PropertyChangeListener listener) {
    modificationStateContainer.addModifiedListener(listener);
  }

  public void removeModifiedListener(PropertyChangeListener listener) {
    modificationStateContainer.removeModifiedSateListener(listener);
  }

  public boolean isModified() {
    return modificationStateContainer.isModified();
  }

  /**
   * appele si la geo loc d'une EMH est modifié
   */
  public void setGeoLocModified() {
    modificationStateContainer.setModified(PROPERTY_GEOLOC_MODIFIED);
  }

  public void setScenarioModified() {
    modificationStateContainer.setModified(PROPERTY_SCENARIO_MODIFIED);
  }

  public void setStudyConfigModified() {
    modificationStateContainer.setModified(PROPERTY_STUDYCONFIG_MODIFIED);
  }

  public void setExternDataModified() {
    modificationStateContainer.setModified(PROPERTY_EXTERN_DATA_MODIFIED);
  }

  public boolean isScenarioModified() {
    return modificationStateContainer.isModified(PROPERTY_SCENARIO_MODIFIED);
  }

  public boolean isGeoLocModified() {
    return modificationStateContainer.isModified(PROPERTY_GEOLOC_MODIFIED);
  }

  public boolean isExternDataModified() {
    return modificationStateContainer.isModified(PROPERTY_EXTERN_DATA_MODIFIED);
  }

  public boolean isStudyConfigModified() {
    return modificationStateContainer.isModified(PROPERTY_STUDYCONFIG_MODIFIED);
  }

  public void clear() {
    modificationStateContainer.clear();
  }
}
