/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.controller;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.calque.ZCalqueSelectionInteractionAbstract;
import org.fudaa.ebli.calque.ZEbliCalquePanelController;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryCLimMLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryNodeLayer;

/**
 *
 * @author Frederic Deniger
 */
public class PlanimetryAccrocheController {

  private final PlanimetryVisuPanel view;

  PlanimetryAccrocheController(PlanimetryVisuPanel view) {
    this.view = view;
  }

  public void install() {
    view.getVueCalque().addMouseMotionListener(new MouseAdapter() {
      @Override
      public void mouseMoved(MouseEvent e) {
        PlanimetryAccrocheController.this.mouseMoved(e);
      }
    });
  }

  void mouseMoved(MouseEvent e) {
    final PlanimetryNodeLayer layer = view.getPlanimetryController().getNodeController().getLayer();
    if (layer == null) {
      return;
    }
    GrPoint pt = new GrPoint(e.getPoint());
    pt.autoApplique(layer.getVersReel());
    final CtuluListSelection selection = layer.selection(pt, ZCalqueSelectionInteractionAbstract.DEFAULT_TOLERANCE_PIXEL);
    Cursor newCursor = null;
    if (selection == null || selection.isEmpty()) {
      if (!view.getController().getCqCatchI().isSomethingCatched()) {
        newCursor = view.getController().getDefautCursor();
        view.unsetInfoText();
      }
      PlanimetryCLimMLayer climM = view.getPlanimetryController().getcondLimiteController().getLayer();
      final CtuluListSelection selectionClimM = climM.selection(pt, ZCalqueSelectionInteractionAbstract.DEFAULT_TOLERANCE_PIXEL);
      if (selectionClimM != null && !selectionClimM.isEmpty()) {
        view.setInfoText(climM.getInfoClimMSelected(selectionClimM));
      }

    } else {
      newCursor = ZEbliCalquePanelController.CURSOR_ACC;
      int[] selectedIndex = selection.getSelectedIndex();
      String[] emhs = new String[selectedIndex.length];
      for (int i = 0; i < selectedIndex.length; i++) {
        emhs[i] = layer.getLayerControllerEMH().getEMHFromLayerPosition(selectedIndex[i]).getNom();
      }
      view.setInfoText(StringUtils.join(emhs, "; "));
    }
    if (newCursor != null && !newCursor.equals(view.getVueCalque().getCursor())) {
      view.getVueCalque().setCursor(newCursor);
    }

  }
}
