/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import org.fudaa.ctulu.gis.GISAttributeModelObjectInterface;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;

/**
 * Permet de trouver les positions des lits dans un TraceProfil
 *
 * @author Frederic Deniger
 */
public class LimLitFinder {
  public static int getEndLimLit(GISAttributeModelObjectInterface limLits) {
    return findIdx(limLits, LimLitHelper.MIN_MAJG);
  }

  public static int getEndLimLit(int traceProfilIdx, final GISZoneCollectionLigneBrisee traceProfilGeomData) {
    GISAttributeModelObjectInterface limLits = (GISAttributeModelObjectInterface) traceProfilGeomData.getModel(
        GGTraceProfilSectionAttributesController.LIM_LIT).
        getObjectValueAt(traceProfilIdx);
    return getEndLimLit(limLits);
  }

  public static int getDebLimLit(int traceProfilIdx, final GISZoneCollectionLigneBrisee traceProfilGeomData) {
    GISAttributeModelObjectInterface limLits = (GISAttributeModelObjectInterface) traceProfilGeomData.getModel(
        GGTraceProfilSectionAttributesController.LIM_LIT).
        getObjectValueAt(traceProfilIdx);
    return getDebLimLit(limLits);
  }

  public static int getDebLimLit(GISAttributeModelObjectInterface limLits) {
    return findIdx(limLits, LimLitHelper.MAJD_MIN);
  }

  static int findIdx(GISAttributeModelObjectInterface limLits, String key) {
    int nb = limLits.getSize();
    int debLitMineur = -1;
    for (int i = 0; i < nb; i++) {
      String value = (String) limLits.getValue(i);
      if (debLitMineur < 0) {
        if (value != null && value.indexOf(key) >= 0) {
          return i;
        }
      }
    }
    return -1;
  }
}
