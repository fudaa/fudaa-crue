package org.fudaa.fudaa.crue.planimetry.action;

import org.fudaa.ctulu.gis.*;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.controller.LayerBrancheController;
import org.fudaa.fudaa.crue.planimetry.controller.LayerSectionController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryBrancheModel;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.operation.distance.GeometryLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui permet de gerer le split d'une branche sur une section et construit les 2 supports des branches crées par ce split.
 */
public class SplitBrancheProcess {


    private final LayerBrancheController layerBrancheController;
    private final LayerSectionController layerSectionController;
    private final PlanimetryControllerHelper planimetryControllerHelper;

    public SplitBrancheProcess(PlanimetryController planimetryController) {
        layerBrancheController = planimetryController.getBrancheController();
        layerSectionController = planimetryController.getSectionController();
        planimetryControllerHelper = layerSectionController.getHelper();
    }


    /**
     * construit les geometries des 2 branches issues du split de la branche brancheUid à la sectionUid
     *
     * @param brancheUid la branche a splitter sur la section. Deviendra la branche amont
     * @param sectionUid la section support du split
     * @return les geometries des branches amont/aval
     */
    public Result split(Long brancheUid, Long sectionUid) {
        Result result = new Result();
        int branchePosition = layerBrancheController.getBranchePosition(brancheUid);
        CoordinateSequence line = layerBrancheController.getBrancheGis(branchePosition).getCoordinateSequence();
        GISPoint point = layerSectionController.getPoint(sectionUid);
        GeometryLocation positionOnLine = ProjectBrancheOnLineProcess.findPositionOnLine(line, point.getCoordinate());
        if (positionOnLine == null) {
            return result;
        }
        //on recupère la coordonnées d'un point existant pour éviter d'ajouter un nouveau point
        Coordinate pointToAdd = positionOnLine.getCoordinate().copy();
        if (planimetryControllerHelper.isSame(line.getCoordinate(positionOnLine.getSegmentIndex()), pointToAdd)) {
            pointToAdd = line.getCoordinate(positionOnLine.getSegmentIndex()).copy();
        } else if (planimetryControllerHelper.isSame(line.getCoordinate(positionOnLine.getSegmentIndex() + 1), pointToAdd)) {
            pointToAdd = line.getCoordinate(positionOnLine.getSegmentIndex() + 1).copy();
        }

        List<Coordinate> amont = new ArrayList<>();
        for (int k = 0; k <= positionOnLine.getSegmentIndex(); k++) {
            amont.add(line.getCoordinate(k).copy());
        }
        amont.add(pointToAdd);
        List<Coordinate> aval = new ArrayList<>();
        aval.add(pointToAdd);
        for (int k = positionOnLine.getSegmentIndex() + 1; k < line.size(); k++) {
            aval.add(line.getCoordinate(k).copy());
        }
        //on enleve les doublons si nécessaires.
        int size = amont.size();
        if (size > 2) {
            Coordinate last = amont.get(size - 1);
            Coordinate beforeLast = amont.get(size - 2);
            if (planimetryControllerHelper.isSame(last, beforeLast)) {
                amont.remove(size - 1);
            }
        }
        if (aval.size() > 2) {
            Coordinate first = aval.get(0);
            Coordinate second = aval.get(1);
            if (planimetryControllerHelper.isSame(first, second)) {
                aval.remove(0);
            }
        }
        result.newBrancheAmont = GISGeometryFactory.INSTANCE.createLineString(amont.toArray(new Coordinate[0]));
        result.newBrancheAval = GISGeometryFactory.INSTANCE.createLineString(aval.toArray(new Coordinate[0]));
        return result;
    }

    public static class Result {
        /**
         * Geometrie de la branche Amont
         */
        public GISPolyligne newBrancheAmont;
        /**
         * Geometrie de la branche Aval
         */
        public GISPolyligne newBrancheAval;

    }
}
