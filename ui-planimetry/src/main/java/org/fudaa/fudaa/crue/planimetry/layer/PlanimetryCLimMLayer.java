package org.fudaa.fudaa.crue.planimetry.layer;

import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.ZSelectionTrace;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceImage;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.fudaa.crue.planimetry.configuration.CondLimitConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.EnumTypeIcon;
import org.fudaa.fudaa.crue.planimetry.configuration.LabelConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.controller.LayerCLimMController;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.algorithm.locate.PointOnGeometryLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Map;

/**
 * @author deniger
 */
public class PlanimetryCLimMLayer extends ZCalqueAffichageDonnees implements PlanimetryLayerContrat {
  CondLimitConfiguration layerConfiguration;
  final PlanimetryCLimMLayerModel modele;
  final Map<Class, Icon> iconsByDclm;
  private final LabelPainter labelPainter = new LabelPainter();

  public PlanimetryCLimMLayer(PlanimetryCLimMLayerModel modele) {
    this.modele = modele;
    iconsByDclm = EnumTypeIcon.getIconsByDclm();
  }

  @Override
  public boolean canBeUsedForAccroche() {
    return false;
  }

  @Override
  public CtuluListSelection selection(GrPoint _pt, int _tolerance) {
    if (modele == null || !isVisible()) {
      return null;
    }
    CtuluListSelection res = creeSelection();
    final GrMorphisme versEcran = getVersEcran();
    GrPoint ptEcran = _pt.applique(versEcran);
    GrPoint pt = new GrPoint();
    Point ptInt = new Point();
    TraceImage traceImage = new TraceImage();
    GrBoite iconBoite = new GrBoite();
    int idx = 0;
    for (EMH emh : modele.getEMHs()) {
      modele.getPosition(emh, pt);
      pt.autoApplique(versEcran);
      List<DonCLimM> dclm = modele.getDclm(emh);
      for (DonCLimM donCLimM : dclm) {
        Icon icon = iconsByDclm.get(donCLimM.getClass());
        ptInt.x = (int) pt.x_;
        ptInt.y = (int) pt.y_;
        int position = layerConfiguration.getIconPosition(donCLimM.getClass());
        traceImage.configureFor(position, layerConfiguration.getIconDistance(), ptInt);
        traceImage.getBoite(iconBoite, icon, ptInt.x, ptInt.y);
        if (iconBoite.contientXY(ptEcran) || iconBoite.distanceXY(ptEcran) <= _tolerance) {
          res.add(idx);
        }
        idx++;
      }
    }
    if (res.isEmpty()) {
      return null;
    }
    return res;
  }

  @Override
  public CtuluListSelection selection(LinearRing _poly, int _mode) {
    final GrBoite domaineBoite = getDomaine();
    if (modele == null || !isVisible()||domaineBoite==null) {
      return null;
    }
    final Envelope polyEnv = _poly.getEnvelopeInternal();

    final Envelope domaine = new Envelope(domaineBoite.e_.x_, domaineBoite.o_.x_, domaineBoite.e_.y_,
        domaineBoite.o_.y_);
    // si l'envelop du polygone n'intersect pas le domaine, il n'y a pas de selection
    if (!polyEnv.intersects(domaine)) {
      return null;
    }
    final GrMorphisme versEcran = getVersEcran();
    final CtuluListSelection r = creeSelection();
    GrPoint pt = new GrPoint();
    Point ptInt = new Point();
    TraceImage traceImage = new TraceImage();
    GrBoite iconBoite = new GrBoite();
    final PointOnGeometryLocator tester = new IndexedPointInAreaLocator(_poly);
    int idx = 0;
    final Coordinate c = new Coordinate();
    for (EMH emh : modele.getEMHs()) {
      modele.getPosition(emh, pt);
      pt.autoApplique(versEcran);
      List<DonCLimM> dclm = modele.getDclm(emh);
      for (DonCLimM donCLimM : dclm) {
        Icon icon = iconsByDclm.get(donCLimM.getClass());
        ptInt.x = (int) pt.x_;
        ptInt.y = (int) pt.y_;
        int position = layerConfiguration.getIconPosition(donCLimM.getClass());
        traceImage.configureFor(position, layerConfiguration.getIconDistance(), ptInt);
        traceImage.getBoite(iconBoite, icon, ptInt.x, ptInt.y);
        iconBoite.autoApplique(getVersReel());
        iconBoite.barycentreXY(pt);
        c.x = pt.x_;
        c.y = pt.y_;
        if (GISLib.isSelectedEnv(c, _poly, polyEnv, tester)) {
          r.add(idx);
        }
        idx++;
      }
    }
    if (r.isEmpty()) {
      return null;
    }
    return r;
  }

  public String getInfoClimMSelected(CtuluListSelection selection) {
    int idx = 0;
    CrueConfigMetier ccm = modele.getCcm();
    final Coordinate c = new Coordinate();
    StringBuilder builder = new StringBuilder();
    for (EMH emh : modele.getEMHs()) {
      List<DonCLimM> dclm = modele.getDclm(emh);
      for (DonCLimM donCLimM : dclm) {
        if (selection.isSelected(idx)) {
          String value = layerConfiguration.getLabelFor(donCLimM, ccm, true, modele.getCondLimitConfigurationExtra());
          if (builder.length() > 0) {
            builder.append("; ");
          }
          builder.append(value);
        }
        idx++;
      }
    }
    return builder.toString();
  }

  @Override
  public GrBoite getDomaineOnSelected() {
    return null;
  }

  @Override
  public LineString getSelectedLine() {
    return null;
  }

  @Override
  public void doPaintSelection(Graphics2D _g, ZSelectionTrace _trace, GrMorphisme _versEcran, GrBoite _clipReel) {
    if (isSelectionEmpty()) {
      return;
    }
    int idx = 0;
    GrPoint pt = new GrPoint();
    Point ptInt = new Point();
    TraceImage traceImage = new TraceImage();
    GrBoite iconBoite = new GrBoite();
    GrBoite clipEcran = _clipReel.applique(getVersEcran());
    final TraceLigne tlSelection = _trace.getLigne();
    for (EMH emh : modele.getEMHs()) {
      List<DonCLimM> dclm = modele.getDclm(emh);
      for (DonCLimM donCLimM : dclm) {
        if (selection_.isSelected(idx)) {
          modele.getPosition(emh, pt);
          pt.autoApplique(getVersEcran());
          Icon icon = iconsByDclm.get(donCLimM.getClass());
          ptInt.x = (int) pt.x_;
          ptInt.y = (int) pt.y_;
          int position = layerConfiguration.getIconPosition(donCLimM.getClass());
          traceImage.configureFor(position, layerConfiguration.getIconDistance(), ptInt);
          traceImage.getBoite(iconBoite, icon, ptInt.x, ptInt.y);
          if (clipEcran.intersectXY(iconBoite)) {
            tlSelection.dessineRectangle(_g, iconBoite.getMinX(), iconBoite.getMinY(), iconBoite.getDeltaX() + 1, iconBoite.getDeltaY() + 1);
          }
        }

        idx++;
      }
    }
  }

  public CondLimitConfiguration getLayerConfiguration() {
    return layerConfiguration;
  }

  public void setLayerConfiguration(CondLimitConfiguration layerConfiguration) {
    this.layerConfiguration = layerConfiguration;
  }

  @Override
  public PlanimetryCLimMLayerModel modeleDonnees() {
    return modele;
  }

  @Override
  public boolean isMovable() {
    return false;
  }

  @Override
  public String editSelected() {
    getActions()[0].actionPerformed(null);
    return null;
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  @Override
  public boolean isEditable() {
    return false;
  }

  @Override
  public LayerCLimMController getLayerController() {
    return modeleDonnees().getLayerController();
  }

  public VisuConfiguration getVisuConfiguration() {
    return layerConfiguration.getParent();
  }

  @Override
  public void paintDonnees(Graphics2D _g, GrMorphisme _versEcran, GrMorphisme _versReel, GrBoite _clipReel) {
    if (!isVisible()) {
      return;
    }

    GrPoint pt = new GrPoint();
    Point ptInt = new Point();
    TraceImage traceImage = new TraceImage();
    alpha_ = layerConfiguration.getTransparenceAlphaForLayer();
    CrueConfigMetier ccm = modele.getCcm();
    for (EMH emh : modele.getEMHs()) {
      modele.getPosition(emh, pt);
      if (_clipReel.contientXY(pt)) {
        pt.autoApplique(_versEcran);
        List<DonCLimM> dclm = modele.getDclm(emh);
        for (DonCLimM donCLimM : dclm) {
          Icon icon = iconsByDclm.get(donCLimM.getClass());
          ptInt.x = (int) pt.x_;
          ptInt.y = (int) pt.y_;
          int position = layerConfiguration.getIconPosition(donCLimM.getClass());
          traceImage.configureFor(position, layerConfiguration.getIconDistance(), ptInt);
          if (EbliLib.isAlphaChanged(alpha_) && icon instanceof ImageIcon) {
            final AlphaComposite composite = AlphaComposite.getInstance(AlphaComposite.DST_OVER, ((float) alpha_) / 255);
            BufferedImage imgAlpha = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
            final Graphics2D g2D = (Graphics2D) imgAlpha.getGraphics();
            g2D.setComposite(composite);
            g2D.drawImage(((ImageIcon) icon).getImage(), 0, 0, this);
            traceImage.paintImage(_g, ptInt.x, ptInt.y, imgAlpha, null, this);
          } else {
            traceImage.paintIcon(_g, ptInt.x, ptInt.y, icon, null);
          }
          LabelConfiguration labelPosition = layerConfiguration.getLabelPosition(donCLimM.getClass());
          if (labelPosition != null && labelPosition.isDisplayLabels()) {

            String value = layerConfiguration.getLabelFor(donCLimM, ccm, false, modele.getCondLimitConfigurationExtra());
            if (value != null) {
              int xTopLeft = traceImage.getXTopLeft(ptInt.x, icon.getIconWidth(), null);
              int yTopLeft = traceImage.getYTopLeft(ptInt.y, icon.getIconHeight(), null);
              final GrPoint grPoint = getPoint(xTopLeft, yTopLeft, icon.getIconWidth(), icon.getIconHeight(), labelPosition.getLabelAlignment());
              labelPainter.paintLabels(_g, grPoint, value, labelPosition, alpha_);
            }
          }
        }
      }
    }
  }

  /**
   * <pre>
   * SwingConstants.NORTH_WEST|SwingConstants.NORTH|SwingConstants.NORTH_EAST
   * SwingConstants.WEST|SwingConstants.CENTER|SwingConstants.EAST
   * SwingConstants.SOUTH_WEST|SwingConstants.SOUTH|SwingConstants.SOUTH_EAST
   * </pre>
   */
  private GrPoint getPoint(int xTopLeft, int yToLeft, int width, int height, int labelPosition) {
    switch (labelPosition) {
      case SwingConstants.NORTH_WEST:
        return new GrPoint(xTopLeft, yToLeft, 0);
      case SwingConstants.NORTH:
        return new GrPoint(xTopLeft + width / 2, yToLeft, 0);
      case SwingConstants.NORTH_EAST:
        return new GrPoint(xTopLeft + width, yToLeft, 0);
      case SwingConstants.WEST:
        return new GrPoint(xTopLeft, yToLeft + height / 2, 0);
      case SwingConstants.CENTER:
        return new GrPoint(xTopLeft + width / 2, yToLeft + height / 2, 0);
      case SwingConstants.EAST:
        return new GrPoint(xTopLeft + width, yToLeft + height / 2, 0);
      case SwingConstants.SOUTH_WEST:
        return new GrPoint(xTopLeft, yToLeft + height, 0);
      case SwingConstants.SOUTH:
        return new GrPoint(xTopLeft + width / 2, yToLeft + height, 0);
      case SwingConstants.SOUTH_EAST:
        return new GrPoint(xTopLeft + width, yToLeft + height, 0);
    }
    return new GrPoint(xTopLeft, yToLeft, 0);
  }
}
