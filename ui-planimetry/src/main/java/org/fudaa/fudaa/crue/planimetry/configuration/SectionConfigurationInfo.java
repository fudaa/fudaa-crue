package org.fudaa.fudaa.crue.planimetry.configuration;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.metier.comparator.ToStringInternationalizableComparator;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.fudaa.crue.planimetry.configuration.editor.SectionColorPropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.NbBundle;

/**
 ** Attention. Les name des set sont utilisés pour la persistence dans le xml. Donc, ne pas changer sans rejouer les tests unitaires et faire des
 * reprises
 *
 * @author deniger
 */
public class SectionConfigurationInfo {

  public static final String PREFIX_SECTIONS = "sections";//attention, utilise par la persistence donc ne pas changer

  public static List<Sheet.Set> createSet(SectionConfiguration in) {
    List<Sheet.Set> res = new ArrayList<>();
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setName(PREFIX_SECTIONS);
    set.put(VisuConfigurationInfo.createTransparency(in));
    set.setDisplayName(NbBundle.getMessage(VisuConfigurationInfo.class, "SectionConfiguration.DisplayName"));
    set.setShortDescription(set.getDisplayName());
    res.add(set);
    res.add(TraceIconModelConfigurationInfo.createSet(in.iconModel, PREFIX_SECTIONS));
    res.add(LabelConfigurationInfo.createSet(in.getLabelConfiguration(), PREFIX_SECTIONS, true));
    res.add(createColorSheet(in));
    return res;
  }

  public static Sheet.Set createColorSheet(SectionConfiguration in) {
    Sheet.Set set = new Sheet.Set();
    set.setName(PREFIX_SECTIONS + ".colors");
    set.setDisplayName(NbBundle.getMessage(BrancheConfigurationInfo.class, "SectionConfiguration.SectionColors"));
    set.setShortDescription(set.getDisplayName());
    Map<EnumSectionType, Color> colorByBrancheType = in.getColorBySectionType();
    List<EnumSectionType> keySet = new ArrayList<>(colorByBrancheType.keySet());
    Collections.sort(keySet, ToStringInternationalizableComparator.INSTANCE);
    for (EnumSectionType enumSectionType : keySet) {
      SectionColorPropertySupport res = new SectionColorPropertySupport(enumSectionType, in);
      set.put(res);
    }
    return set;


  }
}
