/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.tracecasier;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Frederic Deniger
 */
class TraceCasierProfilContentItem implements Comparable<TraceCasierProfilContentItem> {

  private final double z;
  private final double xt;
  private String limLit = "";

  public TraceCasierProfilContentItem(double xt, double z) {
    this.z = z;
    this.xt = xt;
  }

  public double getZ() {
    return z;
  }

  public void setLimLit(String limLit) {
    this.limLit = StringUtils.defaultString(limLit);
  }

  public double getXt() {
    return xt;
  }

  @Override
  public int compareTo(TraceCasierProfilContentItem o) {
    if (o == null) {
      return -1;
    }
    return xt > o.xt ? 1 : -1;
  }

  /**
   * Non utilise pour l'instant
   *
   * @return limite de lit.
   */
  String getLimLit() {
    return limLit;
  }
}
