package org.fudaa.fudaa.crue.planimetry.save;

import java.util.LinkedHashMap;
import java.util.List;
import org.fudaa.dodico.crue.projet.planimetry.LayerVisibility;
import org.fudaa.fudaa.crue.common.config.ConfigSaverHelper;
import org.fudaa.fudaa.crue.planimetry.configuration.BrancheConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.configuration.CasierConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.configuration.CondLimitConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.configuration.LayerVisibilityInfo;
import org.fudaa.fudaa.crue.planimetry.configuration.NodeConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.configuration.SectionConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.configuration.TraceConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfigurationInfo;
import org.openide.nodes.Node.Property;
import org.openide.nodes.Sheet;

/**
 * Permet de sauvegarder les données propres aux données hydraulique ( branches,noeuds,...) et les options de visibilités
 *
 * @author deniger
 */
public class ConfigSaverHdydaulicAndVisibility {

  public LinkedHashMap<String, Property> getProperties(VisuConfiguration in, LayerVisibility visibility) {
    LinkedHashMap<String, Property> res = new LinkedHashMap<>();
    if (in == null) {
      return res;
    }
    Sheet.Set generalConfig = VisuConfigurationInfo.createSet(in);
    ConfigSaverHelper.addProperties(res, generalConfig);
    List<Sheet.Set> sectionConfig = SectionConfigurationInfo.createSet(in.getSectionConfiguration());
    ConfigSaverHelper.addVisibility(res, LayerVisibilityInfo.createSectionProperty(visibility));
    ConfigSaverHelper.addProperties(res, sectionConfig);

    List<Sheet.Set> traceConfig = TraceConfigurationInfo.createSet(in.getTraceConfiguration());
    ConfigSaverHelper.addVisibility(res, LayerVisibilityInfo.createTraceProperty(visibility));
    ConfigSaverHelper.addProperties(res, traceConfig);

    List<Sheet.Set> nodeConfig = NodeConfigurationInfo.createSet(in.getNodeConfiguration());
    ConfigSaverHelper.addVisibility(res, LayerVisibilityInfo.createNodeProperty(visibility));
    ConfigSaverHelper.addProperties(res, nodeConfig);

    List<Sheet.Set> brancheConfig = BrancheConfigurationInfo.createSet(in.getBrancheConfiguration());
    ConfigSaverHelper.addVisibility(res, LayerVisibilityInfo.createBrancheProperty(visibility));
    ConfigSaverHelper.addProperties(res, brancheConfig);

    List<Sheet.Set> casierConfig = CasierConfigurationInfo.createSet(in.getCasierConfiguration());
    ConfigSaverHelper.addVisibility(res, LayerVisibilityInfo.createCasierProperty(visibility));
    ConfigSaverHelper.addProperties(res, casierConfig);

    List<Sheet.Set> condLimiteConfig = CondLimitConfigurationInfo.createSet(in.getcondLimiteConfiguration());
    ConfigSaverHelper.addVisibility(res, LayerVisibilityInfo.createcondLimiteProperty(visibility));
    ConfigSaverHelper.addProperties(res, condLimiteConfig);

    ConfigSaverHelper.addVisibility(res, LayerVisibilityInfo.createGrDessinProperty(visibility));
    ConfigSaverHelper.addVisibility(res, LayerVisibilityInfo.createGrHydraulicProperty(visibility));
    ConfigSaverHelper.addVisibility(res, LayerVisibilityInfo.createGrImageProperty(visibility));
    ConfigSaverHelper.addVisibility(res, LayerVisibilityInfo.createGrOtherProperty(visibility));
    ConfigSaverHelper.addVisibility(res, LayerVisibilityInfo.createGrSIGFIleProperty(visibility));
    ConfigSaverHelper.addVisibility(res, LayerVisibilityInfo.createGrTraceProfilsProperty(visibility));
    ConfigSaverHelper.addVisibility(res, LayerVisibilityInfo.createGrTraceProfilsPropertyCasiers(visibility));

    return res;
  }
}
