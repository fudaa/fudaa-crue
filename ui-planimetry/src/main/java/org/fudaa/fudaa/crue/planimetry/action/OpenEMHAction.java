/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.planimetry.action;

import java.awt.event.ActionEvent;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.openide.util.NbBundle;

/**
 *
 * @author Frédéric Deniger
 */
public abstract class OpenEMHAction extends EbliActionSimple {
  
   boolean reopen;
  private final PlanimetryController controller;
  
  private OpenEMHAction(String name, String actionName, PlanimetryController controller) {
    super(name, null, actionName);
    this.controller = controller;
  }
  
  @Override
  public void updateStateBeforeShow() {
    List<EMH> selectedEMHs = controller.getVisuPanel().getSelectedEMHs();
    setEnabled(CollectionUtils.isNotEmpty(selectedEMHs) && selectedEMHs.size() == 1);
  }
  
  @Override
  public void actionPerformed(ActionEvent _e) {
    if (controller.getEmhEditor() == null) {//arrive en test.
      DialogHelper.showError("no editor");
      return;
    }
    List<EMH> selectedEMHs = controller.getVisuPanel().getSelectedEMHs();
    if (CollectionUtils.isNotEmpty(selectedEMHs) && selectedEMHs.size() == 1) {
      controller.getEmhEditor().edit(selectedEMHs.get(0), reopen);
    }
  }
  
  public static class Reopen extends OpenEMHAction {
    
    public Reopen(PlanimetryController controller) {
      super(NbBundle.getMessage(OpenEMHAction.class, "OpenEMH.DisplayName"), "OPEN_EMH_SAME_FRAME", controller);
      reopen = true;
    }
  }
  
  public static class NewFrame extends OpenEMHAction {
    
    public NewFrame(PlanimetryController controller) {
      super(NbBundle.getMessage(OpenEMHAction.class, "OpenEMH.NewFrame.DisplayName"), "OPEN_EMH_NEWFRAME", controller);
      reopen = false;
    }
  }
}
