package org.fudaa.fudaa.crue.planimetry.controller;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryCLimMLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetrySectionLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryTraceLayer;
import org.fudaa.fudaa.crue.planimetry.listener.SectionTraceSelectionSynchronizer;

/**
 *
 * @author deniger
 */
public class PlanimetryControllerBuilder {

  public static PlanimetryController build(PlanimetryVisuPanel panel, PlanimetryGisModelContainer modelContext,
          CrueConfigMetier crueConfigMetier, VisuConfiguration configuration, boolean modelling) {
    PlanimetryController res = new PlanimetryController(modelContext, crueConfigMetier, configuration);
    res.helper = new PlanimetryControllerHelper(res);
    modelContext.updateNames(crueConfigMetier);
    GroupHydraulicController.build(res, panel);
    GroupExterneMainController.build(res, panel);
    buildGroupHydraulic(res, panel, configuration, modelling);
    res.setVisuPanel(panel);
    res.initControllers();
    return res;
  }

  private static void buildGroupHydraulic(PlanimetryController ctx, PlanimetryVisuPanel res, VisuConfiguration configuration, boolean modelling) {
    LayerNodeController nodeController = LayerNodeController.install(ctx, res, configuration);
    LayerBrancheController brancheController = LayerBrancheController.install(ctx, res, configuration);
    LayerModelControllerDefault<PlanimetryTraceLayer> traceController = LayerTraceController.install(ctx, res, configuration,
            brancheController);
    LayerModelControllerDefault<PlanimetrySectionLayer> sectionController = LayerSectionController.install(ctx, res,
            configuration,
            brancheController);
    LayerModelControllerDefault<PlanimetryCLimMLayer> condLimiteController = LayerCLimMController.install(ctx, res,
            configuration,
            brancheController, modelling);
    LayerCasierController casierController = LayerCasierController.install(ctx, res, configuration);
    final BGroupeCalque groupHydraulic = ctx.groupHydraulicController.groupHydraulic;
    //on ajouter les traces en premier qui seront finalement en dernier.
    groupHydraulic.enPremier(traceController.getLayer());
    groupHydraulic.enPremier(casierController.getLayer());
    groupHydraulic.enPremier(brancheController.getLayer());
    groupHydraulic.enPremier(nodeController.getLayer());
    groupHydraulic.enPremier(sectionController.getLayer());
    groupHydraulic.enPremier(condLimiteController.getLayer());
    new SectionTraceSelectionSynchronizer(res.getScene(), traceController.getLayer(), sectionController.getLayer()).install();

  }
}
