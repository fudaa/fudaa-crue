/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import gnu.trove.TDoubleArrayList;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.fudaa.crue.planimetry.sig.PointContainerToSequenceProcessor;
import org.openide.util.NbBundle;

import java.util.*;

/**
 * Transforme un semis de points en TraceProfils
 *
 * @author Frederic Deniger
 */
public class GGPointToTraceProfilProcessor {
  private static final String ATTRIBUTE_NAME_NOM_PROFIL = "NOM_PROFIL";
  private static final String ATTRIBUTE_NAME_NOM_PROFIL_OLD = "PROFIL_PRE";
  private static final String ATTRIBUTE_LIM_LIT = "Lim_lit";
  private static final String ATTRIBUTE_LIM_GEOM = "Lim_geom";
  public static final String MODEL_Z_KEY_ORTHO = "Z_ORTHO";
  public static final String MODEL_Z_KEY_IGN = "Z_IGN69";
  private final GISZoneCollectionPoint pt;
  private final PropertyEpsilon epsilon;

  public GGPointToTraceProfilProcessor(GISZoneCollectionPoint pt, CrueConfigMetier ccm) {
    this.pt = pt;
    epsilon = ccm.getEpsilon(CrueConfigMetierConstants.PROP_XT);
  }

  private void valideModels(Map<String, GISAttributeModel> models, CtuluLog log, int nbPoint) {
    for (Map.Entry<String, GISAttributeModel> entry : models.entrySet()) {
      String key = entry.getKey();
      GISAttributeModel model = entry.getValue();
      if (model == null) {
        log.addSevereError(NbBundle.getMessage(GGPointToTraceProfilProcessor.class, "traceProfil.AttributNotFound", key));
        return;
      } else if (model.getSize() != nbPoint) {
        log.addSevereError(NbBundle.getMessage(GGPointToTraceProfilProcessor.class, "traceProfil.AttributeSizeNotCorrect", key, Integer.toString(
            model.getSize()), Integer.toString(nbPoint)));
      }
      Class dataClass = model.getAttribute().getDataClass();
      Class expectedDataClass = Double.class;
      final String name = model.getAttribute().getName();
      if (name.equalsIgnoreCase(ATTRIBUTE_NAME_NOM_PROFIL) || name.equalsIgnoreCase(ATTRIBUTE_NAME_NOM_PROFIL_OLD)) {
        expectedDataClass = String.class;
      } else if (name.equalsIgnoreCase(ATTRIBUTE_LIM_GEOM) || name.equalsIgnoreCase(ATTRIBUTE_LIM_LIT)) {
        expectedDataClass = Integer.class;
      }
      if (!dataClass.equals(expectedDataClass)) {
        log.addSevereError(NbBundle.getMessage(GGPointToTraceProfilProcessor.class, "traceProfil.AttributeWithWrongType", key, dataClass.toString(),
            expectedDataClass.toString()));
      }
      for (int i = 0; i < nbPoint; i++) {
        if (model.getObjectValueAt(i) == null) {
          log.
              addSevereError(NbBundle.getMessage(GGPointToTraceProfilProcessor.class, "traceProfil.AttributeWithEmptyValue", key, Integer.
                  toString(i)));
        }
      }
    }
  }

  private GISAttributeModel findModel(String name) {
    return findModel(name, pt);
  }

  public static GISAttributeModel findModel(String name, GISZoneCollection zoneCollection) {
    for (int i = zoneCollection.getNbAttributes() - 1; i >= 0; i--) {
      GISAttributeModel attribute = zoneCollection.getModel(i);
      if (name.equalsIgnoreCase(attribute.getAttribute().getName())) {
        return attribute;
      }
    }
    return null;
  }

  /**
   * @param attributeForZName le nom de l'attribut à utiliser pour retrouver les valeurs de l'altitude.
   */
  public CtuluLogResult<GISZoneCollectionLigneBrisee> toProfil(final String attributeForZName) {
    CtuluLogResult<GISZoneCollectionLigneBrisee> res = new CtuluLogResult<>();
    CtuluLog log = new CtuluLog();
    res.setLog(log);
    Map<String, GISAttributeModel> models = new HashMap<>();
    final String modelXtKey = "Absc_curv";
    GISAttributeModel modelXt = findModel(modelXtKey);

    models.put(modelXtKey, modelXt);
    GISAttributeModel modelZ = findModel(attributeForZName);
    models.put(attributeForZName, modelZ);
    GISAttributeModel modelLimLit = findModel(ATTRIBUTE_LIM_LIT);
    models.put(ATTRIBUTE_LIM_LIT, modelLimLit);
    GISAttributeModel modelLimGeo = findModel(ATTRIBUTE_LIM_GEOM);
    models.put(ATTRIBUTE_LIM_GEOM, modelLimGeo);
    String modelNomProfilKey = ATTRIBUTE_NAME_NOM_PROFIL;
    GISAttributeModel modelNomProfil = findModel(modelNomProfilKey);
    //on prend en charge l'ancien nom de l'attribut pour le nom du profil
    if (modelNomProfil == null) {
      modelNomProfil = findModel(ATTRIBUTE_NAME_NOM_PROFIL_OLD);
      if (modelNomProfil != null) {
        modelNomProfilKey = ATTRIBUTE_NAME_NOM_PROFIL_OLD;
      }
    }
    models.put(modelNomProfilKey, modelNomProfil);

    if (modelXt == null) {
      log.addError(NbBundle.getMessage(GGPointToTraceProfilProcessor.class, "traceProfil.noXtAttributes"));
    }
    if (attributeForZName == null) {
      log.addError(NbBundle.getMessage(GGPointToTraceProfilProcessor.class, "traceProfil.noZAttributes"));
    }
    if (modelNomProfil == null) {
      log.addError(NbBundle.getMessage(GGPointToTraceProfilProcessor.class, "traceProfil.noNameAttributes"));
    }
    if (modelLimLit == null) {
      log.addError(NbBundle.getMessage(GGPointToTraceProfilProcessor.class, "traceProfil.noLimitLitAttributes"));
    }
    if (modelLimGeo == null) {
      log.addError(NbBundle.getMessage(GGPointToTraceProfilProcessor.class, "traceProfil.noLimitGeoAttributes"));
    }
    if (log.containsErrorOrSevereError()) {
      return res;
    }

    final int nbPts = pt.getNumGeometries();
    valideModels(models, log, nbPts);
    if (log.containsErrorOrSevereError()) {
      return res;
    }
    boolean listOfDoublonDetected = false;
    Map<String, TraceProfilContent> contentByName = new HashMap<>();
    TDoubleArrayList detectedDoublons = new TDoubleArrayList();
    int nbIgnoredLinesBecauseNameIsEmpty = 0;
    int nbIgnoredLinesBecauseXtIsEmpty = 0;
    int nbIgnoredLinesBecauseZIsEmpty = 0;
    int nbIgnoredLinesBecauseLimLitIsEmpty = 0;
    int nbIgnoredLinesBecauseLimGeoIsEmpty = 0;
    for (int i = 0; i < nbPts; i++) {
      String nom = (String) modelNomProfil.getObjectValueAt(i);
      final Number xtValue = (Number) modelXt.getObjectValueAt(i);
      final Number zValue = (Number) modelZ.getObjectValueAt(i);
      final Number limLitValue = (Number) modelLimLit.getObjectValueAt(i);
      final Number limGeoValue = (Number) modelLimGeo.getObjectValueAt(i);
      //on ignore les lignes vides.
      if (StringUtils.isBlank(nom)) {
        nbIgnoredLinesBecauseNameIsEmpty++;
        continue;
      }
      if (xtValue == null) {
        nbIgnoredLinesBecauseXtIsEmpty++;
        continue;
      }
      if (zValue == null) {
        nbIgnoredLinesBecauseZIsEmpty++;
        continue;
      }
      if (limLitValue == null) {
        nbIgnoredLinesBecauseLimLitIsEmpty++;
        continue;
      }
      if (limGeoValue == null) {
        nbIgnoredLinesBecauseLimGeoIsEmpty++;
        continue;
      }

      TraceProfilContentItem item = new TraceProfilContentItem();
      item.x = pt.getX(i);
      item.y = pt.getY(i);
      item.xt = xtValue.doubleValue();
      item.z = zValue.doubleValue();
      item.addLimLit(limLitValue.intValue());
      item.addLimGeom(limGeoValue.intValue());
      TraceProfilContent content = contentByName.get(nom);
      if (content == null) {
        content = new TraceProfilContent(nom, epsilon);
        contentByName.put(nom, content);
      }
      if (!content.add(item)) {
        detectedDoublons.add(item.xt);
        listOfDoublonDetected = true;
      }
    }
    List<TraceProfilContent> values = new ArrayList<>(contentByName.values());
    Collections.sort(values);
    GISZoneCollectionLigneBrisee zone = new GISZoneCollectionLigneBrisee();
    GGTraceProfilSectionAttributesController attributesHelper = new GGTraceProfilSectionAttributesController();
    zone.setAttributes(attributesHelper.createSectionAttributes(), null);
    PointContainerToSequenceProcessor toSequence = new PointContainerToSequenceProcessor();
    for (TraceProfilContent traceProfilContent : values) {
      String nom = traceProfilContent.name;
      List<TraceProfilContentItem> sortedItems = traceProfilContent.getSortedItems();
      zone.addCoordinateSequence(toSequence.create(sortedItems),
          attributesHelper.createObject(nom, sortedItems), null);
    }
    if (listOfDoublonDetected) {
      //doublons contient les xt trouvés en double.
      String doublons = CtuluLibString.arrayToString(detectedDoublons.toNativeArray());
      //on limite a 40 caractères le message:
      doublons = StringUtils.abbreviate(doublons, 40);
      log.addWarn(NbBundle.getMessage(GGPointToTraceProfilProcessor.class, "traceProfil.xtDoublonDetected", doublons));
    }
    if (nbIgnoredLinesBecauseNameIsEmpty > 0) {
      log.addWarn(NbBundle.getMessage(GGPointToTraceProfilProcessor.class, "traceProfil.ligneIgnoredEmptyName", nbIgnoredLinesBecauseNameIsEmpty));
    }
    if (nbIgnoredLinesBecauseXtIsEmpty > 0) {
      log.addWarn(NbBundle.getMessage(GGPointToTraceProfilProcessor.class, "traceProfil.ligneIgnoredEmptyXt", nbIgnoredLinesBecauseXtIsEmpty));
    }
    if (nbIgnoredLinesBecauseZIsEmpty > 0) {
      log.addWarn(NbBundle.getMessage(GGPointToTraceProfilProcessor.class, "traceProfil.ligneIgnoredEmptyZ", nbIgnoredLinesBecauseZIsEmpty));
    }
    if (nbIgnoredLinesBecauseLimLitIsEmpty > 0) {
      log.addWarn(NbBundle.getMessage(GGPointToTraceProfilProcessor.class, "traceProfil.ligneIgnoredEmptyLimLit", nbIgnoredLinesBecauseLimLitIsEmpty));
    }
    if (nbIgnoredLinesBecauseLimGeoIsEmpty > 0) {
      log.addWarn(NbBundle.getMessage(GGPointToTraceProfilProcessor.class, "traceProfil.ligneIgnoredEmptyLimGeo", nbIgnoredLinesBecauseLimGeoIsEmpty));
    }
    res.setResultat(zone);

    return res;
  }
}
