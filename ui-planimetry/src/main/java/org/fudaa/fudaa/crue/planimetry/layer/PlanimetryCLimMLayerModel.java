package org.fudaa.fudaa.crue.planimetry.layer;

import com.memoire.bu.BuTable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GisAbscCurviligneToCoordinate;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;
import org.fudaa.dodico.crue.metier.emh.DonCLimMCommonItem;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModeleDonnees;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.crue.planimetry.configuration.CondLimitConfigurationExtra;
import org.fudaa.fudaa.crue.planimetry.controller.LayerCLimMController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;

/**
 *
 * @author deniger
 */
public class PlanimetryCLimMLayerModel implements ZModeleDonnees {

  private final LayerCLimMController layerController;
  private Long editedCalculUid;
  private boolean inactiveEMHVisible;
  private CondLimitConfigurationExtra condLimitConfigurationExtra;

  public CondLimitConfigurationExtra getCondLimitConfigurationExtra() {
    return condLimitConfigurationExtra;
  }

  public void setCondLimitConfigurationExtra(CondLimitConfigurationExtra condLimitConfigurationExtra) {
    this.condLimitConfigurationExtra = condLimitConfigurationExtra;
  }

  public PlanimetryCLimMLayerModel(LayerCLimMController layerModelController) {
    this.layerController = layerModelController;
  }

  public Long getEditedCalculUid() {
    return editedCalculUid;
  }

  public void setEditedCalculUid(Long editedCalculUid) {
    this.editedCalculUid = editedCalculUid;
  }

  public LayerCLimMController getLayerController() {
    return layerController;
  }

  protected boolean isPainted(EMH emh) {
    if (!layerController.getLayer().getVisuConfiguration().isInactiveEMHVisible() && !emh.getActuallyActive()) {
      return false;
    }
    PlanimetryControllerHelper helper = layerController.getHelper();
    return helper.isVisible(EMHHelper.getParent(emh));
  }

  public List<DonCLimM> getDclm(EMH emh) {
    List<DonCLimM> dclM = emh.getDCLM();
    List<DonCLimM> res = new ArrayList<>();
    if (editedCalculUid == null) {
      return res;
    }
    for (DonCLimM donCLimM : dclM) {
      if (editedCalculUid.equals(donCLimM.getCalculParent().getUiId()) && ((DonCLimMCommonItem) donCLimM).getUserActive()) {
        res.add(donCLimM);
      }
    }
    return res;
  }

  public void getPosition(EMH emh, GrPoint pt) {
    if (EnumCatEMH.BRANCHE.equals(emh.getCatType())) {
      getPositionForBranche(emh, pt);
    } else if (EnumCatEMH.CASIER.equals(emh.getCatType())) {
      CatEMHCasier casier = (CatEMHCasier) emh;
      getPositionForNoeud(casier.getNoeud(), pt);
    } else if (EnumCatEMH.NOEUD.equals(emh.getCatType())) {
      getPositionForNoeud(emh, pt);
    }
  }

  public boolean hasDclmForCalcul(EMH emh) {
    List<DonCLimM> dclM = emh.getDCLM();
    for (DonCLimM donCLimM : dclM) {
      if (((DonCLimMCommonItem) donCLimM).getUserActive() && editedCalculUid.equals(donCLimM.getCalculParent().getUiId())) {
        return true;
      }
    }
    return false;
  }

  protected List<EMH> getEMHs() {
    if (editedCalculUid == null) {
      return Collections.emptyList();
    }
    List<EMH> allSimpleEMH = layerController.getHelper().getController().getScenario().getAllSimpleEMH();
    List<EMH> filtered = new ArrayList<>();
    inactiveEMHVisible = getLayerController().getLayer().getVisuConfiguration().isInactiveEMHVisible();
    for (EMH emh : allSimpleEMH) {
      if (isPainted(emh) && hasDclmForCalcul(emh)) {
        filtered.add(emh);
      }
    }
    return filtered;
  }

  @Override
  public BuTable createValuesTable(ZCalqueAffichageDonneesInterface _layer) {
    return null;
  }

  @Override
  public void fillWithInfo(InfoData _d, ZCalqueAffichageDonneesInterface _layer) {
  }

  @Override
  public boolean isValuesTableAvailable() {
    return false;
  }

  @Override
  public GrBoite getDomaine() {
    GrBoite domaine = getLayerController().getHelper().getBrancheController().getLayer().getBrancheModel().getDomaine();
    if (domaine != null) {
      domaine.ajuste(getLayerController().getHelper().getNodeController().getLayer().getDomaine());
    } else {
      domaine = getLayerController().getHelper().getNodeController().getLayer().getDomaine();
    }
    return domaine;
  }

  @Override
  public int getNombre() {
    return 0;
  }

  @Override
  public Object getObject(int _ind) {
    return null;
  }

  @Override
  public GrPoint getVertexForObject(int _ind, int _idVertex) {
    return null;
  }

  public void getPositionForNoeud(EMH emh, GrPoint pt) {
    final PlanimetryControllerHelper helper = layerController.getHelper();
    int position = helper.getNodeController().getNodePosition(emh.getUiId());
    GISPoint noeudGis = helper.getNodeController().getNoeudGis(position);
    pt.x_ = noeudGis.getX();
    pt.y_ = noeudGis.getY();
  }

  public void getPositionForBranche(EMH emh, GrPoint pt) {
    final PlanimetryControllerHelper helper = layerController.getHelper();
    int branchePosition = helper.getBrancheController().getBranchePosition(emh.getUiId());
    GISPolyligne brancheGis = helper.getBrancheController().getBrancheGis(branchePosition);
    GisAbscCurviligneToCoordinate.Result res = new GisAbscCurviligneToCoordinate.Result();
    GisAbscCurviligneToCoordinate.find(brancheGis, 0.5, res);
    pt.x_ = res.getCoordinate().x;
    pt.y_ = res.getCoordinate().y;
  }

  CrueConfigMetier getCcm() {
    return layerController.getHelper().getController().getCrueConfigMetier();
  }
}
