/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.action;

import javax.swing.Icon;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.services.PlanimetryModellingController;

/**
 *
 * @author Frederic Deniger
 */
public abstract class AbstractEnablingAction extends EbliActionSimple implements ZSelectionListener {

  final PlanimetryModellingController planimetryModellingController = new PlanimetryModellingController();

  public AbstractEnablingAction(String name, Icon icon, String actionCommand, PlanimetryVisuPanel visuPanel) {
    super(name, icon, actionCommand);
    setEnabled(false);
    visuPanel.getEditor().getSceneEditor().getScene().addSelectionListener(this);
  }

  @Override
  public final void setEnabled(boolean _newValue) {
    updateStateBeforeShow();
  }

  public void setEnabledParent(boolean _newValue) {
    super.setEnabled(_newValue);
  }

  protected abstract boolean isProcessorEnable();

  @Override
  public void updateStateBeforeShow() {
    setEnabledParent(planimetryModellingController.isModellingPerspectiveInEditMode() && isProcessorEnable());
  }

  @Override
  public void selectionChanged(ZSelectionEvent _evt) {
    updateStateBeforeShow();
  }
}
