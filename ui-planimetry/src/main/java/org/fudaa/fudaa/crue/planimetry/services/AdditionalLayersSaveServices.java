/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.services;

import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.openide.util.lookup.ServiceProvider;

import java.util.HashSet;
import java.util.Set;

/**
 * Service permettant de gérer l'état des calques additionnels: dessins, images géoreferencees,...
 *
 * @author Frederic Deniger
 */
@ServiceProvider(service = AdditionalLayersSaveServices.class)
public class AdditionalLayersSaveServices {
    final Set<AdditionalLayerSaveStateListener> listeners = new HashSet<>();

    /**
     * appelee lorsque la config des calques de dessins est sauvegardée depuis la perspective {@link org.fudaa.fudaa.crue.common.PerspectiveEnum#REPORT}.
     */
    public void setAdditionalLayerSavedFromReport(PlanimetryController planimetryController) {
        for (AdditionalLayerSaveStateListener additionalLayerSaveStateListener : listeners) {
            additionalLayerSaveStateListener.additionalLayerSavedBy(AdditionalLayerSaveStateListener.FROM_REPORT, planimetryController);
        }
    }

    /**
     * appelee lorsque la config des calques de dessins est sauvegardée depuis la perspective {@link org.fudaa.fudaa.crue.common.PerspectiveEnum#MODELLING}.
     */
    public void setAdditionalLayerSavedFromModelling(PlanimetryController planimetryController) {
        for (AdditionalLayerSaveStateListener additionalLayerSaveStateListener : listeners) {
            additionalLayerSaveStateListener.additionalLayerSavedBy(AdditionalLayerSaveStateListener.FROM_MODELLING, planimetryController);
        }
    }

    /**
     * @param listener listener ecoutant les modifications sur les sauvegardes des calques additionnels
     */
    public void addListener(AdditionalLayerSaveStateListener listener) {
        listeners.add(listener);
    }

    /**
     * @param listener listener n'ecoutant plus les modifications sur les sauvegardes des calques additionnels
     */
    public void removeListener(AdditionalLayerSaveStateListener listener) {
        listeners.add(listener);
    }
}
