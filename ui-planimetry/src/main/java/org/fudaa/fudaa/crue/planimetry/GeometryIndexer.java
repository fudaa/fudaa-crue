package org.fudaa.fudaa.crue.planimetry;

import gnu.trove.TObjectIntHashMap;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISZoneListener;
import org.fudaa.ebli.calque.edition.ZModeleGeometryDefault;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryGisModelContainer;

/**
 * Permet de donner la position de l'objet dans le modele d'affichage à partir de son Uid.
 * @author deniger
 */
public class GeometryIndexer implements GISZoneListener {

  private final ZModeleGeometryDefault modele;
  private final PlanimetryGisModelContainer gisContext;
  TObjectIntHashMap<Long> positionByUid = null;

  public GeometryIndexer(ZModeleGeometryDefault modele, PlanimetryGisModelContainer gisContext) {
    this.modele = modele;
    this.gisContext = gisContext;
  }

  @Override
  public void attributeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _action) {
  }

  @Override
  public void attributeValueChangeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _indexObj,
                                         Object _newValue) {
    if (gisContext.isUid(_att)) {
      clear();
    }
  }

  @Override
  public void objectAction(Object _source, int _indexObj, Object _obj, int _action) {
    if (_action == GISZoneListener.OBJECT_ACTION_ADD || _action == GISZoneListener.OBJECT_ACTION_REMOVE) {
      clear();
    }
  }

  public int getPosition(Long uid) {
    if (positionByUid == null) {
      reindex();
    }
    if (positionByUid.containsKey(uid)) {
      return positionByUid.get(uid);
    }
    return -1;
  }

  private void clear() {
    positionByUid = null;
  }

  public void reindex() {
    positionByUid = new TObjectIntHashMap<>();
    GISAttributeModel uidModel = gisContext.getUidModel(modele.getGeometries());
    int nb = uidModel.getSize();
    for (int i = 0; i < nb; i++) {
      positionByUid.put((Long) uidModel.getObjectValueAt(i), i);
    }
  }
}
