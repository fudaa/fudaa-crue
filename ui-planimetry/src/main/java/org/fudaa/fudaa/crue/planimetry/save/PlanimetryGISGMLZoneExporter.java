/*
 * @creation 19 mai 2005
 * 
 * @modification $Date: 2008-03-28 14:59:28 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.crue.planimetry.save;

import org.fudaa.ctulu.gis.exporter.GISDataStoreZoneExporter;

/**
 * @author Fred Deniger
 * @version $Id: GISGMLZoneExporter.java,v 1.1.6.1 2008-03-28 14:59:28 bmarchan Exp $
 */
public class PlanimetryGISGMLZoneExporter extends GISDataStoreZoneExporter {
  
  public static final String GML_EXTENSION = ".gml";
  public static final String SHP_EXTENSION = ".shp";
  
  public PlanimetryGISGMLZoneExporter() {
    super();
    setUseIdAsName(true);
  }
  
}
