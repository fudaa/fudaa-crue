package org.fudaa.fudaa.crue.planimetry.controller;

import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryHydraulicGroup;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class GroupHydraulicController {

  public static GroupHydraulicController build(PlanimetryController context,PlanimetryVisuPanel panel) {
    assert context.groupHydraulicController == null;
    GroupHydraulicController groupController = new GroupHydraulicController();
    groupController.groupHydraulic = new PlanimetryHydraulicGroup();
    groupController.groupHydraulic.setName(PlanimetryController.NAME_GC_HYDRO);
    groupController.groupHydraulic.setTitle(NbBundle.getMessage(PlanimetryControllerBuilder.class,
            PlanimetryController.NAME_GC_HYDRO));
    context.groupHydraulicController = groupController;
    return groupController;
  }
  PlanimetryHydraulicGroup groupHydraulic;

  public PlanimetryHydraulicGroup getGroupHydraulic() {
    return groupHydraulic;
  }
}
