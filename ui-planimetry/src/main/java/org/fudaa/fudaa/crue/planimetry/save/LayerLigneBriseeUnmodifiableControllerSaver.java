package org.fudaa.fudaa.crue.planimetry.save;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessource;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessourceInfos;
import org.fudaa.dodico.crue.io.conf.Option;
import org.fudaa.fudaa.crue.common.config.ConfigSaverHelper;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper;
import org.fudaa.fudaa.crue.planimetry.configuration.LigneBriseeConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.controller.ExternLayerFactory;
import org.fudaa.fudaa.crue.planimetry.controller.LayerLigneBriseeExternController;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLigneBriseeExternUnmodifiableLayer;
import org.fudaa.fudaa.crue.planimetry.sig.SigLoaderHelper;
import org.fudaa.fudaa.sig.FSigGeomSrcData;
import org.openide.nodes.Sheet;

/**
 * Pour etre pris compte doit être ajoute dans la liste AdditionalLayerLoaderProcess
 *
 * @author deniger
 */
public class LayerLigneBriseeUnmodifiableControllerSaver implements AdditionalLayerLoaderProcess.Loader {

  public CrueEtudeExternRessource getSaveRessource(String id, LayerLigneBriseeExternController layerController, File baseDir) {
    LayerControllerSaverHelper helper = new LayerControllerSaverHelper();
    CrueEtudeExternRessourceInfos ressource = new CrueEtudeExternRessourceInfos();
    final PlanimetryLigneBriseeExternUnmodifiableLayer layer = (PlanimetryLigneBriseeExternUnmodifiableLayer) layerController.getLayer();
    ressource.setNom(layer.getTitle());
    ressource.setType(ConfigExternIds.LAYER_LINE_UNMODIFIABLE_SAVE_ID);
    ressource.setId(id);
    ressource.setLayerId(layer.getName());
    File file = layer.getFile();
    helper.putRelativeAndAbsolutePaths(ressource, file, baseDir);
    helper.addOption(ressource, LayerControllerSaverHelper.GEO_FORMAT, layer.getFmt().toString());
    helper.addVisibleProperty(ressource, layerController.getLayer());
    LinkedHashMap<String, String> transform = ConfigSaverHelper.getProperties(LigneBriseeConfigurationInfo.createSet(
            layer.getLayerConfiguration()));
    for (Map.Entry<String, String> entry : transform.entrySet()) {
      ressource.getOptions().add(new Option(entry.getKey(), entry.getValue()));
    }
    return new CrueEtudeExternRessource(ressource);
  }

  @Override
  public String getType() {
    return ConfigExternIds.LAYER_LINE_UNMODIFIABLE_SAVE_ID;
  }

  @Override
  public PlanimetryAdditionalLayerContrat load(CrueEtudeExternRessourceInfos ressource, CtuluLog log, File baseDir) {
    LayerControllerSaverHelper helper = new LayerControllerSaverHelper();
    Map<String, String> optionsBykey = helper.toMap(ressource);
    File file = helper.retrieveFiles(optionsBykey, ressource, baseDir, log);
    if (file == null) {
      return null;
    }
    Pair<FSigGeomSrcData, SigFormatHelper.EnumFormat> pair = helper.loadGisData(file, optionsBykey, ressource.getNom(), log);
    if (pair == null) {
      return null;
    }
    FSigGeomSrcData result = pair.first;
    if (result == null) {
      log.addError("externConfig.geo.NoDataFound.error", ressource.getNom(), file.getName());
    }
    GISZoneCollectionLigneBrisee lignes = SigLoaderHelper.buildLine(result);
    PlanimetryLigneBriseeExternUnmodifiableLayer layer = ExternLayerFactory.createLayerLignesUnmodifiable(lignes, file,
            pair.second,
            null);
    layer.setTitle(ressource.getNom());
    layer.setName(ressource.getLayerId());
    helper.addVisibleProperty(ressource, layer);
    List<Sheet.Set> propertySet = LigneBriseeConfigurationInfo.createSet(layer.getLayerConfiguration());
    LayerControllerSaverHelper.restoreVisuProperties(optionsBykey, layer, propertySet, log);
    return layer;
  }
}
