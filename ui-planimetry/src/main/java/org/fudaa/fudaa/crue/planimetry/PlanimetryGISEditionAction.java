package org.fudaa.fudaa.crue.planimetry;

import java.beans.PropertyChangeEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.TreeSelectionModel;
import org.fudaa.ebli.calque.BCalqueInteraction;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.action.CalqueGISEditionAction;
import org.fudaa.ebli.calque.edition.BPaletteEdition;
import org.fudaa.ebli.calque.edition.ZEditorDefault;
import org.fudaa.ebli.commun.BPalettePanelInterface;

/**
 *
 * @author deniger
 */
public class PlanimetryGISEditionAction extends CalqueGISEditionAction {

  public PlanimetryGISEditionAction(TreeSelectionModel _m, ZEditorDefault _editor, ZScene _scene) {
    super(_m, _editor, _scene);
  }

  PlanimetryGisPaletteEdition getPalette() {
    return (PlanimetryGisPaletteEdition) super.palette_;
  }

  @Override
  protected BPalettePanelInterface buildPaletteContent() {
    final PlanimetryGisPaletteEdition bPaletteEdition = new PlanimetryGisPaletteEdition(super.getScene());
    bPaletteEdition.buildComponents();
    return bPaletteEdition;
  }

  @Override
  public void propertyChange(PropertyChangeEvent _evt) {
    if (_evt.getSource() != calqueEdition_
        && !((Boolean) _evt.getNewValue()).booleanValue() && palette_ != null && ((BPaletteEdition) palette_).isEditionOnGoing()) {
      unactivePanel();
    }
  }

  /**
   * Appelée quand il y a un changement de selection dans l'arbre des calques.
   */
  @Override
  public void valueChanged(final TreeSelectionEvent _e) {
    unactivePaletteCreation();
    BCalqueInteraction calqueInteraction = editor_.getPanel().getController().getCalqueInteraction("cqDeplacement");
    if (calqueInteraction.isGele() && editor_.getPanel().getController().getCqSelectionI() != null) {
      editor_.getPanel().getController().getCqSelectionI().setGele(false);
    }
    setTarget(getTarget((TreeSelectionModel) _e.getSource()));
  }
}
