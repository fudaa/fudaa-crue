package org.fudaa.fudaa.crue.planimetry.save;

import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.gis.GISDataModelFilterAdapter;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.projet.planimetry.GisLayerId;
import org.fudaa.fudaa.crue.planimetry.controller.*;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Point;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class PlanimetryGisSaver {
  private final PlanimetryController controller;
  static final String NODE_NAME = GisLayerId.NOEUDS.getShpFilename();
  static final String BRANCHE_NAME = GisLayerId.BRANCHES.getShpFilename();
  static final String TRACE_NAME = GisLayerId.TRACES_SECTIONS.getShpFilename();
  static final String CASIER_NAME = GisLayerId.CASIERS.getShpFilename();

  public PlanimetryGisSaver(PlanimetryController controller) {
    this.controller = controller;
  }

  public void save(File dirOfConfig) {
    deleteGML(new File(dirOfConfig, NODE_NAME));
    deleteGML(new File(dirOfConfig, BRANCHE_NAME));
    deleteGML(new File(dirOfConfig, TRACE_NAME));
    deleteGML(new File(dirOfConfig, CASIER_NAME));
    saveNodes(dirOfConfig);
    saveBranches(dirOfConfig);
    saveCasiers(dirOfConfig);
    saveTraces(dirOfConfig);
  }

  private int[] getAttributesToSave() {
    return new int[]{PlanimetryGisModelContainer.NOM_EMH_POSITION};
  }

  private int[] getTraceAttributesToSave() {
    return new int[]{PlanimetryGisModelContainer.NOM_EMH_POSITION,
      PlanimetryGisModelContainer.TRACE_ANGLE_BEGIN,
      PlanimetryGisModelContainer.TRACE_ANGLE_END};
  }

  private void saveNodes(File dirOfConfig) {
    Map<String, TIntArrayList> nodeIdxBySousModeleId = new HashMap<>();
    GISZoneCollectionPoint nodeCollection = controller.getNodeController().getNodeCollection();
    int size = nodeCollection.getNbGeometries();
    for (int i = 0; i < size; i++) {
      CatEMHNoeud emhFromPositionInModel = controller.getNodeController().getEMHFromPositionInModel(i);
      List<EMHSousModele> parents = emhFromPositionInModel.getParents();
      for (EMHSousModele eMHSousModele : parents) {
        final String ssModeleId = eMHSousModele.getId();
        TIntArrayList positions = nodeIdxBySousModeleId.get(ssModeleId);
        if (positions == null) {
          positions = new TIntArrayList();
          nodeIdxBySousModeleId.put(ssModeleId, positions);
        }
        positions.add(i);
      }
    }
    int[] att = getAttributesToSave();
    for (Map.Entry<String, TIntArrayList> entry : nodeIdxBySousModeleId.entrySet()) {
      String string = entry.getKey();
      TIntArrayList positions = entry.getValue();
      File destDir = new File(dirOfConfig, string);
      destDir.mkdirs();
      GISDataModelFilterAdapter adapter = new GISDataModelFilterAdapter(nodeCollection, att, positions.toNativeArray());
      PlanimetryGISGMLZoneExporter exporter = new PlanimetryGISGMLZoneExporter();
      File destFile = new File(destDir, NODE_NAME);
      try {
        exporter.processStore(null, adapter, new ShapefileDataStoreFactory().createDataStore(destFile.toURI().toURL()), Point.class);
      } catch (Exception ex) {
        Logger.getLogger(getClass().getName()).log(Level.WARNING, "saveNodes", ex);
      }
    }
  }

  private void saveBranches(File dirOfConfig) {
    Map<String, TIntArrayList> brancheIdxBySousModeleId = new HashMap<>();
    final LayerBrancheController brancheController = controller.getBrancheController();
    GISZoneCollectionLigneBrisee brancheCollection = brancheController.getBrancheCollection();
    int size = brancheCollection.getNbGeometries();
    for (int i = 0; i < size; i++) {
      CatEMHBranche emhFromPositionInModel = brancheController.getEMHFromPositionInModel(i);
      EMHSousModele parent = emhFromPositionInModel.getParent();
      final String ssModeleId = parent.getId();
      TIntArrayList positions = brancheIdxBySousModeleId.get(ssModeleId);
      if (positions == null) {
        positions = new TIntArrayList();
        brancheIdxBySousModeleId.put(ssModeleId, positions);
      }
      positions.add(i);
    }
    int[] att = getAttributesToSave();
    for (Map.Entry<String, TIntArrayList> entry : brancheIdxBySousModeleId.entrySet()) {
      String string = entry.getKey();
      TIntArrayList positions = entry.getValue();
      File destDir = new File(dirOfConfig, string);
      destDir.mkdirs();
      GISDataModelFilterAdapter adapter = new GISDataModelFilterAdapter(brancheCollection, att, positions.toNativeArray());
      PlanimetryGISGMLZoneExporter exporter = new PlanimetryGISGMLZoneExporter();
      File destFile = new File(destDir, BRANCHE_NAME);
      try {
        exporter.processStore(null, adapter, new ShapefileDataStoreFactory().createDataStore(destFile.toURI().toURL()), LineString.class);
      } catch (Exception ex) {
        Logger.getLogger(getClass().getName()).log(Level.WARNING, "saveBranches", ex);
      }
    }
  }

  private void saveTraces(File dirOfConfig) {
    Map<String, TIntArrayList> traceIdxBySousModele = new HashMap<>();
    final LayerTraceController traceController = controller.getTraceController();
    GISZoneCollectionLigneBrisee traceCollection = (GISZoneCollectionLigneBrisee) traceController.getGeomData();
    int size = traceCollection.getNbGeometries();
    for (int i = 0; i < size; i++) {
      CatEMHSection emhFromPositionInModel = traceController.getEMHFromPositionInModel(i);
      EMHSousModele parent = emhFromPositionInModel.getParent();
      final String ssModeleId = parent.getId();
      TIntArrayList positions = traceIdxBySousModele.get(ssModeleId);
      if (positions == null) {
        positions = new TIntArrayList();
        traceIdxBySousModele.put(ssModeleId, positions);
      }
      positions.add(i);
    }
    int[] att = getTraceAttributesToSave();
    for (Map.Entry<String, TIntArrayList> entry : traceIdxBySousModele.entrySet()) {
      String string = entry.getKey();
      TIntArrayList positions = entry.getValue();
      File destDir = new File(dirOfConfig, string);
      boolean mkdirs = destDir.mkdirs();
      if (!mkdirs) {
        Logger.getLogger(getClass().getName()).warning("Can't create directory " + destDir.getAbsolutePath());
      }
      GISDataModelFilterAdapter adapter = new GISDataModelFilterAdapter(traceCollection, att, positions.toNativeArray());
      PlanimetryGISGMLZoneExporter exporter = new PlanimetryGISGMLZoneExporter();
      File destFile = new File(destDir, TRACE_NAME);
      try {
        exporter.processStore(null, adapter, new ShapefileDataStoreFactory().createDataStore(destFile.toURI().toURL()), LineString.class);
      } catch (Exception ex) {
        Logger.getLogger(getClass().getName()).log(Level.WARNING, "saveTraces", ex);
      }
    }
  }

  private void saveCasiers(File dirOfConfig) {
    Map<String, TIntArrayList> casierIdxBySousModeleId = new HashMap<>();
    final LayerCasierController casierController = controller.getCasierController();
    GISZoneCollectionLigneBrisee casierCollection = casierController.getCasierCollection();
    int size = casierCollection.getNbGeometries();
    for (int i = 0; i < size; i++) {
      CatEMHCasier emhFromPositionInModel = casierController.getEMHFromPositionInModel(i);
      EMHSousModele parent = emhFromPositionInModel.getParent();
      final String ssModeleId = parent.getId();
      TIntArrayList positions = casierIdxBySousModeleId.get(ssModeleId);
      if (positions == null) {
        positions = new TIntArrayList();
        casierIdxBySousModeleId.put(ssModeleId, positions);
      }
      positions.add(i);
    }
    int[] att = getAttributesToSave();
    for (Map.Entry<String, TIntArrayList> entry : casierIdxBySousModeleId.entrySet()) {
      String string = entry.getKey();
      TIntArrayList positions = entry.getValue();
      File destDir = new File(dirOfConfig, string);
      final boolean mkdirs = destDir.mkdirs();
      if (!mkdirs) {
        Logger.getLogger(getClass().getName()).warning("Can't create directory " + destDir.getAbsolutePath());
      }
      GISDataModelFilterAdapter adapter = new GISDataModelFilterAdapter(casierCollection, att, positions.toNativeArray());
      PlanimetryGISGMLZoneExporter exporter = new PlanimetryGISGMLZoneExporter();
      File destFile = new File(destDir, CASIER_NAME);
      try {
        exporter.processStore(null, adapter, new ShapefileDataStoreFactory().createDataStore(destFile.toURI().toURL()), LinearRing.class);
      } catch (Exception ex) {
        Logger.getLogger(getClass().getName()).log(Level.WARNING, "saveCasiers", ex);
      }
    }
  }

  private void deleteGML(File file) {
    File gmlFile = CtuluLibFile.changeExtension(file, "gml");
    if (gmlFile.exists()) {
      CrueFileHelper.deleteFile(gmlFile, getClass());
    }
  }
}
