package org.fudaa.fudaa.crue.planimetry.listener;

import org.fudaa.ctulu.gis.*;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.fudaa.crue.planimetry.controller.LayerCasierController;
import org.fudaa.fudaa.crue.planimetry.controller.LayerNodeController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryCasierLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryNodeLayerModel;
import org.locationtech.jts.algorithm.locate.PointOnGeometryLocator;
import org.locationtech.jts.geom.Coordinate;

/**
 * Utilise pour déplacer les noeuds si la branche est modifiée.
 *
 * @author deniger
 */
public class NodeUpdaterFromCasier extends AbstractGeomUpdater {
  final LayerNodeController nodeController;

  public NodeUpdaterFromCasier(LayerNodeController controller) {
    this.nodeController = controller;
  }

  @Override
  public void attributeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _action) {
  }

  @Override
  public void attributeValueChangeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _indexObj,
                                         Object _newValue) {
  }

  @Override
  protected void updateAfterRemove() {
    PlanimetryNodeLayerModel model = nodeController.getLayer().modeleDonnees();
    model.updateAfterRemove();
  }

  @Override
  protected void updateAll() {
    LayerCasierController casierController = nodeController.getHelper().getCasierController();
    GISZoneCollectionLigneBrisee casierCollection = casierController.getCasierCollection();
    for (int i = 0; i < casierCollection.getNumGeometries(); i++) {
      update(i);
    }
  }

  @Override
  protected PlanimetryController getController() {
    return nodeController.getHelper().getController();
  }

  @Override
  protected void update(int indexOfCasier) {
    LayerCasierController casierController = nodeController.getHelper().getCasierController();
    CatEMHCasier casier = casierController.getEMHFromPositionInModel(indexOfCasier);
    if (casier == null) {
      return;
    }
    CatEMHNoeud noeud = casier.getNoeud();
    if (noeud == null) {
      return;
    }
    int nodePosition = nodeController.getNodePosition(noeud.getUiId());
    if (nodePosition >= 0) {
      final GISPolygone name = (GISPolygone) casierController.getCasierCollection().getGeometry(
          indexOfCasier);
      GISZoneCollectionPoint nodeCollection = nodeController.getNodeCollection();
      GISPoint point = nodeCollection.get(nodePosition);
      PointOnGeometryLocator createPolygoneTester = GISLib.createPolygoneTester(name);
      if (!GISLib.isInside(createPolygoneTester, point.getCoordinate())) {
        Coordinate centerOfCasier = PlanimetryCasierLayer.getCenter(name);
        nodeCollection.set(nodePosition, centerOfCasier.x, centerOfCasier.y, null);
      }
    }
  }
}
