package org.fudaa.fudaa.crue.planimetry.save;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper;
import org.fudaa.fudaa.crue.planimetry.layout.NetworkGisPositionnerResult;
import org.fudaa.fudaa.sig.FSigGeomSrcData;
import org.openide.util.Exceptions;

import java.io.File;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class PlanimetryGisLoader {
    public PlanimetryGisLoader() {
    }

    /**
     * @param dir      le scenario de config du projet.
     * @param scenario le scenario
     * @return null si aucun projet trouvé dans le repertoire
     */
    public NetworkGisPositionnerResult load(File dir, EMHScenario scenario) {
        NetworkGisPositionnerResult resultat = new NetworkGisPositionnerResult(scenario,true);
        List<EMHModeleBase> modeles = scenario.getModeles();
        boolean found = false;
        for (EMHModeleBase eMHModeleBase : modeles) {
            List<EMHSousModele> sousModeles = eMHModeleBase.getSousModeles();
            for (EMHSousModele eMHSousModele : sousModeles) {
                found |= loadSousModele(eMHSousModele, dir, resultat);
            }
        }
        if (!found) {
            return null;
        }
        return resultat;
    }

    private File getBrancheFile(File destDir) {
        return new File(destDir, PlanimetryGisSaver.BRANCHE_NAME);
    }

    private File getTraceFile(File destDir) {
        return new File(destDir, PlanimetryGisSaver.TRACE_NAME);
    }

    private File getCasierFile(File destDir) {
        return new File(destDir, PlanimetryGisSaver.CASIER_NAME);
    }

    private File getNodeFile(File destDir) {
        return new File(destDir, PlanimetryGisSaver.NODE_NAME);
    }

    public boolean loadSousModeleFromDestDir(EMHSousModele sousModele, File destDir, NetworkGisPositionnerResult resultat) {
        if (!destDir.exists()) {
            return false;
        }

        boolean res = loadNoeuds(sousModele, destDir, resultat);
        res |= loadBranches(sousModele, destDir, resultat);
        res |= loadCasiers(sousModele, destDir, resultat);
        res |= loadTraces(destDir, resultat);
        return res;
    }

    public boolean loadSousModele(EMHSousModele sousModele, File dir, NetworkGisPositionnerResult resultat) {
        return loadSousModeleFromDestDir(sousModele,new File(dir, sousModele.getId()),resultat);
    }

    private boolean loadNoeuds(EMHSousModele sousModele, File destDir, NetworkGisPositionnerResult resultat) {
        List<CatEMHNoeud> noeuds = sousModele.getNoeuds();
        Map<String, CatEMHNoeud> noeudsByName = new HashMap<>();
        for (CatEMHNoeud catEMHNoeud : noeuds) {
            noeudsByName.put(catEMHNoeud.getNom(), catEMHNoeud);
        }
        try {
            final File file = getNodeFile(destDir);
            Pair<FSigGeomSrcData, SigFormatHelper.EnumFormat> loadFile = LayerControllerSaverHelper.loadFile(file, new CtuluLog());
            if (loadFile == null || loadFile.first == null || ArrayUtils.isEmpty(loadFile.first.getPoints())) {
                return false;
            }
            GISDataModel load = loadFile.first.getPoints()[0];
            GISLib.preloadAttributes(load);
            int numGeometries = load.getNumGeometries();
            Set<String> alreadyDoneNodes=new HashSet<>();
            for (int i = 0; i < numGeometries; i++) {
                Geometry geometry = load.getGeometry(i);
                Coordinate[] coordinates = geometry.getCoordinates();
                if (coordinates == null || coordinates.length == 0) {
                    continue;
                }
                String name = ObjectUtils.toString(load.getValue(0, i));
                if(alreadyDoneNodes.contains(name)){
                    resultat.setModifiedWhileLoading(true);
                    Logger.getLogger(PlanimetryGisLoader.class.getName()).log(Level.INFO, "Node defined several times in Gis Data");
                    continue;
                }
                alreadyDoneNodes.add(name);
                CatEMHNoeud noeud = noeudsByName.get(name);
                if (noeud == null) {
                    continue;
                }
                resultat.addNode(coordinates[0].x, coordinates[0].y, noeud);
            }
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
        return true;
    }

    private boolean loadBranches(EMHSousModele sousModele, File destDir, NetworkGisPositionnerResult resultat) {
        List<CatEMHBranche> branches = sousModele.getBranches();
        Map<String, CatEMHBranche> noeudsByName = new HashMap<>();
        for (CatEMHBranche branche : branches) {
            noeudsByName.put(branche.getNom(), branche);
        }
        try {
            final File file = getBrancheFile(destDir);
            Pair<FSigGeomSrcData, SigFormatHelper.EnumFormat> loadFile = LayerControllerSaverHelper.loadFile(file, new CtuluLog());
            if (loadFile == null || loadFile.first == null || ArrayUtils.isEmpty(loadFile.first.getPolylignes())) {
                return false;
            }
            GISDataModel load = loadFile.first.getPolylignes()[0];
            GISLib.preloadAttributes(load);
            int numGeometries = load.getNumGeometries();
            for (int i = 0; i < numGeometries; i++) {
                Geometry geometry = load.getGeometry(i);
                Coordinate[] coordinates = geometry.getCoordinates();
                if (coordinates == null || coordinates.length == 0) {
                    continue;
                }
                String name = ObjectUtils.toString(load.getValue(0, i));
                CatEMHBranche branche = noeudsByName.get(name);
                if (branche == null) {
                    continue;
                }
                resultat.addEdge(Arrays.asList(coordinates), branche);
            }
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
        return true;
    }

    private boolean loadTraces(File destDir, NetworkGisPositionnerResult resultat) {
        try {
            final File file = getTraceFile(destDir);
            if (!file.exists()) {
                return false;
            }
            Pair<FSigGeomSrcData, SigFormatHelper.EnumFormat> loadFile = LayerControllerSaverHelper.loadFile(file, new CtuluLog());
            if (loadFile == null || loadFile.first == null || ArrayUtils.isEmpty(loadFile.first.getPolylignes())) {
                return false;
            }
            GISDataModel load = loadFile.first.getPolylignes()[0];
            GISLib.preloadAttributes(load);
            int numGeometries = load.getNumGeometries();
            for (int i = 0; i < numGeometries; i++) {
                Geometry geometry = load.getGeometry(i);
                Coordinate[] coordinates = geometry.getCoordinates();
                if (coordinates == null || coordinates.length == 0 || load.getNbAttributes() < 3) {
                    continue;
                }
                String name = ObjectUtils.toString(load.getValue(0, i));
                double beginAngle = load.getDoubleValue(1, i);
                double endAngle = load.getDoubleValue(2, i);
                resultat.addTraceAngle(name, beginAngle, endAngle);
            }
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
        return true;
    }

    private boolean loadCasiers(EMHSousModele sousModele, File destDir, NetworkGisPositionnerResult resultat) {
        List<CatEMHCasier> casiers = sousModele.getCasiers();
        Map<String, CatEMHCasier> casierByName = new HashMap<>();
        for (CatEMHCasier casier : casiers) {
            casierByName.put(casier.getNom(), casier);
        }
        try {
            final File file = getCasierFile(destDir);
            if (!file.exists()) {
                return false;
            }
            Pair<FSigGeomSrcData, SigFormatHelper.EnumFormat> loadFile = LayerControllerSaverHelper.loadFile(file, new CtuluLog());
            if (loadFile == null || loadFile.first == null) {
                return false;
            }
            if (ArrayUtils.isNotEmpty(loadFile.first.getPolygones())) {
                GISDataModel load = loadFile.first.getPolygones()[0];
                loadCasierFromModel(load, casierByName, resultat);
            }
            if (ArrayUtils.isNotEmpty(loadFile.first.getPolylignes())) {
                GISDataModel load = loadFile.first.getPolylignes()[0];
                loadCasierFromModel(load, casierByName, resultat);
            }
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
        return true;
    }

    private void loadCasierFromModel(GISDataModel load,
                                     Map<String, CatEMHCasier> casierByName, NetworkGisPositionnerResult resultat) {
        GISLib.preloadAttributes(load);
        int numGeometries = load.getNumGeometries();
        for (int i = 0; i < numGeometries; i++) {
            Geometry geometry = load.getGeometry(i);
            Coordinate[] coordinates = geometry.getCoordinates();
            if (coordinates == null || coordinates.length == 0 || !(((LineString) geometry).isClosed())) {
                continue;
            }
            String name = ObjectUtils.toString(load.getValue(0, i));
            CatEMHCasier casier = casierByName.get(name);
            if (casier == null) {
                continue;
            }
            GISPolygone polygone = GISGeometryFactory.INSTANCE.createLinearRing(coordinates);
            resultat.addCasierAsPolygone(polygone, casier);
        }
    }
}
