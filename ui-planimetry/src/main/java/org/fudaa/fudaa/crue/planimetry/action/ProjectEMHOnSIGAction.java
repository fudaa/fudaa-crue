/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.action;

import java.awt.event.ActionEvent;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ProjectEMHOnSIGAction extends AbstractEnablingAction {

  private final ProjectBrancheOnLineProcess process;

  public ProjectEMHOnSIGAction(PlanimetryController controller) {
    super(NbBundle.getMessage(ProjectEMHOnSIGAction.class, "ProjectBrancheOnLine.ActionName"), EbliResource.EBLI.getIcon("ligne.gif"),
            "PROJECT_ON_LINE", controller.getVisuPanel());
    this.process = new ProjectBrancheOnLineProcess(controller.getVisuPanel());
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    CtuluLog log = process.process();
    if (log.isNotEmpty()) {
      LogsDisplayer.displayError(log, getTitle());
    }
  }

  @Override
  protected boolean isProcessorEnable() {
    return process != null && process.isEnable();
  }
}
