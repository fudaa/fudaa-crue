package org.fudaa.fudaa.crue.planimetry.action;

import java.awt.GridLayout;
import java.io.File;
import javax.swing.Action;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper.EnumFormat;
import org.fudaa.fudaa.crue.planimetry.controller.GroupeExternSIGType;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class SigLoaderAction extends AbstractSigLoaderAction {

  private final GroupeExternSIGType type;

  public SigLoaderAction(PlanimetryController controller, GroupeExternSIGType type) {
    super(NbBundle.getMessage(SigLoaderAction.class, "SigLoaderAction.AddExternalFile"), controller);
    if (GroupeExternSIGType.MODIFIABLE.equals(type)) {
      putValue(Action.NAME, NbBundle.getMessage(SigLoaderAction.class, "SigLoaderAction.ImportFile"));
    }
    this.type = type;
  }

  @Override
  public void finishImportData(final Pair<GISZoneCollectionPoint, GISZoneCollectionLigneBrisee> collections, File file,
          EnumFormat fmt) {
    if (collections == null) {
      return;
    }
    final GISZoneCollectionPoint points = collections.first;
    final GISZoneCollectionLigneBrisee lignes = collections.second;
    int nbPoints = points == null ? 0 : points.getNbGeometries();
    int nbLignes = lignes == null ? 0 : lignes.getNbGeometries();
    boolean importPoint = false;
    boolean importLigne = false;
    if (nbLignes > 0 && nbPoints > 0) {
      JCheckBox cbLignes = new JCheckBox(NbBundle.getMessage(SigLoaderAction.class,
              "SigLoaderAction.ImportLignes.CheckBox",
              Integer.toString(nbLignes)));
      cbLignes.setSelected(true);
      JCheckBox cbPoints = new JCheckBox(NbBundle.getMessage(SigLoaderAction.class,
              "SigLoaderAction.ImportPoints.CheckBox",
              Integer.toString(nbLignes)));
      cbPoints.setSelected(true);
      JPanel panel = new JPanel(new GridLayout(2, 1));
      panel.add(cbLignes);
      panel.add(cbPoints);
      boolean accepted = DialogHelper.showQuestion(getTitle(), panel);
      importLigne = accepted && cbLignes.isSelected();
      importPoint = accepted && cbPoints.isSelected();

    } else if (nbLignes > 0) {
      importLigne = DialogHelper.showQuestion(getTitle(), NbBundle.getMessage(SigLoaderAction.class,
              "SigLoaderAction.ImportLignes.Confirmation",
              Integer.toString(nbLignes)));

    } else if (nbPoints > 0) {
      importPoint = DialogHelper.showQuestion(getTitle(), NbBundle.getMessage(SigLoaderAction.class,
              "SigLoaderAction.ImportPoints.Confirmation",
              Integer.toString(nbPoints)));

    }
    if (importPoint) {
      GISAttributeInterface[] attributes = points.getAttributes();
      setNotEditable(attributes);
      controller.getGroupExternController().addLayerPointUnmodifiable(points, file, fmt, type);
    }
    if (importLigne) {
      GISAttributeInterface[] attributes = lignes.getAttributes();
      setNotEditable(attributes);
      controller.getGroupExternController().addLayerLignesUnmodifiable(lignes, file, fmt, type);
    }
  }

  protected void setNotEditable(GISAttributeInterface[] attributes) {
    if (attributes != null) {
      for (GISAttributeInterface attribute : attributes) {
        if (attribute != null) {
          if (Double.class.equals(attribute.getDataClass())) {
            ((GISAttribute) attribute).setInternEditor(new CtuluValueEditorDouble(false));
          }
          attribute.getEditor().setEditable(false);
        }

      }
    }
  }

}
