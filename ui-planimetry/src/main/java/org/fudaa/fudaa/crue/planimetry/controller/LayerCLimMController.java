package org.fudaa.fudaa.crue.planimetry.controller;

import java.util.List;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryCLimMLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryCLimMLayerModel;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class LayerCLimMController extends LayerModelControllerDefault<PlanimetryCLimMLayer> {

  protected static LayerCLimMController install(PlanimetryController ctx, PlanimetryVisuPanel res,
          VisuConfiguration configuration, LayerBrancheController brancheController, boolean dclmEditable) {
    LayerCLimMController condLimiteController = new LayerCLimMController(dclmEditable);
    final PlanimetryCLimMLayerModel layerModel = new PlanimetryCLimMLayerModel(condLimiteController);
    PlanimetryCLimMLayer condLimiteLayer = new PlanimetryCLimMLayer(layerModel);
    condLimiteLayer.setLayerConfiguration(configuration.getcondLimiteConfiguration());
    condLimiteLayer.setName(PlanimetryController.NAME_CQ_CLIMM);
    condLimiteLayer.setTitle(NbBundle.getMessage(PlanimetryControllerBuilder.class, PlanimetryController.NAME_CQ_CLIMM));
    condLimiteController.layer = condLimiteLayer;
    ctx.controllersByName.put(condLimiteLayer.getName(), condLimiteController);
    return condLimiteController;
  }
  private final boolean dclmEditable;

  public LayerCLimMController(boolean dclmEditable) {
    this.dclmEditable = dclmEditable;
  }

  void setEditedCalcul(Long editedCalculUid) {
    getLayer().modeleDonnees().setEditedCalculUid(editedCalculUid);
    getLayer().repaint(0);
  }

  @Override
  public void setVisuConfiguration(VisuConfiguration cloned) {
    getLayer().setLayerConfiguration(cloned.getcondLimiteConfiguration());
  }

  public List<DonCLimM> getDclmForEditedCalc(EMH emh) {
    return getLayer().modeleDonnees().getDclm(emh);
  }

  public boolean isCalcSelected() {
    return getLayer().modeleDonnees().getEditedCalculUid() != null;
  }

  public boolean isDclmEditable() {
    return dclmEditable;
  }

  public Calc getSelectedCalc() {
    Long editedCalculUid = getLayer().modeleDonnees().getEditedCalculUid();
    if (editedCalculUid == null) {
      return null;
    }
    List<Calc> calcs = getHelper().getController().getScenario().getDonCLimMScenario().getCalc();
    for (Calc calc : calcs) {
      if (editedCalculUid.equals(calc.getUiId())) {
        return calc;
      }
    }
    return null;
  }
}
