/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.controller;

import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.fudaa.crue.planimetry.layer.AbstractPlanimetryLigneBriseeExternLayer;
import org.fudaa.fudaa.crue.planimetry.layer.AbstractPlanimetryPointExternLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternDrawLayerGroup;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class GroupExternDrawController extends AbstractGroupExternController<PlanimetryExternDrawLayerGroup> {

  private int pointIdx = 1;

  public GroupExternDrawController(final PlanimetryController controller, final PlanimetryExternDrawLayerGroup groupLayer) {
    super(controller, groupLayer);
  }

  public AbstractPlanimetryPointExternLayer addLayerPoint(final GISZoneCollectionPoint pt) {
    final AbstractPlanimetryPointExternLayer layer = (AbstractPlanimetryPointExternLayer) addAdditionalLayer(ExternLayerFactory.createLayerPointsModifiable(pt, controller.getVisuPanel().getEditor()));
    layer.setTitle(NbBundle.getMessage(GroupExternDrawController.class, "PointLayerName", Integer.toString(pointIdx++)));
    return layer;
  }

  public AbstractPlanimetryLigneBriseeExternLayer addLayerLignes(final GISZoneCollectionLigneBrisee lignes) {
    final FSigEditor editor = controller.getVisuPanel().getEditor();
    final AbstractPlanimetryLigneBriseeExternLayer layer = ExternLayerFactory.createLayerLignesModifiable(lignes, editor);
    addAdditionalLayer(layer);
    layer.setTitle(NbBundle.getMessage(GroupExternDrawController.class, "LineLayerName", Integer.toString(pointIdx++)));
    return layer;
  }
}
