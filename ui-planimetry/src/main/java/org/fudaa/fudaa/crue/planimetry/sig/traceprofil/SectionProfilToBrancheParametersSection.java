/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.geometrie.GrSegment;

/**
 *
 * @author Frederic Deniger
 */
public class SectionProfilToBrancheParametersSection {

  GrSegment litMineur;
  private final String sectionName;
  String traceName;
  int idxInCalque;
  GISZoneCollectionLigneBrisee zone;

  public SectionProfilToBrancheParametersSection(String sectionName) {
    this.sectionName = sectionName;
  }

  public int getIdxInCalque() {
    return idxInCalque;
  }

  public void setIdxInCalque(int idxInCalque) {
    this.idxInCalque = idxInCalque;
  }

  public GISZoneCollectionLigneBrisee getZone() {
    return zone;
  }

  public void setZone(GISZoneCollectionLigneBrisee zone) {
    this.zone = zone;
  }

  public GrSegment getLitMineur() {
    return litMineur;
  }

  public void setLitMineur(GrSegment litMineur) {
    this.litMineur = litMineur;
  }

  public String getTraceName() {
    return traceName;
  }

  public void setTraceName(String traceName) {
    this.traceName = traceName;
  }

  public String getSectionName() {
    return sectionName;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 71 * hash + (this.sectionName != null ? this.sectionName.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final SectionProfilToBrancheParametersSection other = (SectionProfilToBrancheParametersSection) obj;
    if ((this.sectionName == null) ? (other.sectionName != null) : !this.sectionName.equals(other.sectionName)) {
      return false;
    }
    return true;
  }
}
