package org.fudaa.fudaa.crue.planimetry.layer;

import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;

/**
 *
 * @author deniger
 */
public interface PlanimetryLayerContrat extends ZCalqueAffichageDonneesInterface {

  LayerController getLayerController();
  

}
