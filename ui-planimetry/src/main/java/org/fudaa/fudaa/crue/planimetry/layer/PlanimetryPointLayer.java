package org.fudaa.fudaa.crue.planimetry.layer;

import java.awt.Graphics2D;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.crue.planimetry.configuration.PointConfiguration;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.fudaa.fudaa.sig.layer.FSigLayerPointEditable;

/**
 * Calque pour les points dessinés.
 *
 * @author deniger
 */
public class PlanimetryPointLayer<T extends PointConfiguration> extends FSigLayerPointEditable implements PlanimetryLayerContrat {

  protected T layerConfiguration;

  public PlanimetryPointLayer(PlanimetryPointLayerModel _modele, FSigEditor _editor) {
    super(_modele, _editor);
  }

  public void setLayerConfiguration(T layerConfiguration) {
    this.layerConfiguration = layerConfiguration;
  }

  public T getLayerConfiguration() {
    return layerConfiguration;
  }

  @Override
  public boolean isEditable() {
    return getLayerController().isEditable();
  }

  @Override
  public LayerController getLayerController() {
    return modeleDonnees().getLayerController();
  }

  @Override
  public PlanimetryPointLayerModel modeleDonnees() {
    return (PlanimetryPointLayerModel) super.modeleDonnees();
  }

  @Override
  public void paintDonnees(Graphics2D _g, GrMorphisme _versEcran, GrMorphisme _versReel, GrBoite _clipReel) {
    alpha_ = layerConfiguration.getTransparenceAlphaForLayer();
    super.paintDonnees(_g, _versEcran, _versReel, _clipReel);
  }

  @Override
  public boolean removeSelectedObjects(final CtuluCommandContainer _cmd, final CtuluUI _ui) {
    if (isSelectionEmpty()) {
      return false;
    }
    getLayerController().changeWillBeDone();
    return super.removeSelectedObjects(_cmd, _ui);
  }

  @Override
  public boolean rotateSelectedObjects(double _angRad, double _xreel0, double _yreel0, CtuluCommandContainer _cmd, CtuluUI _ui) {
    if (isSelectionEmpty()) {
      return false;
    }
    getLayerController().changeWillBeDone();
    return super.rotateSelectedObjects(_angRad, _xreel0, _yreel0, _cmd, _ui);
  }

  @Override
  public boolean moveSelectedObjects(double _dx, double _dy, double _dz, CtuluCommandContainer _cmd, CtuluUI _ui) {
    if (isSelectionEmpty()) {
      return false;
    }
    getLayerController().changeWillBeDone();
    return super.moveSelectedObjects(_dx, _dy, _dz, _cmd, _ui);
  }

  @Override
  public boolean canAddForme(int _typeForme) {
    return isEditable() && _typeForme == DeForme.POINT;
  }

  @Override
  protected void initTrace(TraceIconModel model, int i, double z) {
    layerConfiguration.initTraceIcon(model, i, modeleDonnees());
    ZCalqueLigneBrisee.initTraceWithAlpha(model, layerConfiguration.getTransparenceAlphaForLayer());
  }
}
