/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import gnu.trove.TIntObjectHashMap;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierLitNomme;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;

/**
 *
 * @author Frederic Deniger
 */
public class LimGeoHelper {

  public static final String AXE = "AxeHyd";//1
  public static final String THALWEG = "Thalweg";//2

  public static TIntObjectHashMap<String> createMap() {
    TIntObjectHashMap<String> keys = new TIntObjectHashMap<>();
    keys.put(3, THALWEG);
    keys.put(4, AXE);
    return keys;
  }

  static ItemEnum[] getEtiquettes(String nomEtiquetteInGG, CrueConfigMetierLitNomme litNomme) {
    List<ItemEnum> res = new ArrayList<>();
    String[] split = StringUtils.split(nomEtiquetteInGG, ';');
    for (String string : split) {
      ItemEnum etiquette = getEtiquette(string, litNomme);
      if (etiquette != null) {
        res.add(etiquette);
      }
    }
    return res.toArray(new ItemEnum[0]);
  }

  private static ItemEnum getEtiquette(String nomLitInGG, CrueConfigMetierLitNomme litNomme) {
    if (AXE.equals(nomLitInGG)) {
      return litNomme.getEtiquetteAxeHyd();
    }
    if (THALWEG.equals(nomLitInGG)) {
      return litNomme.getEtiquetteThalweg();
    }
    return null;
  }
}
