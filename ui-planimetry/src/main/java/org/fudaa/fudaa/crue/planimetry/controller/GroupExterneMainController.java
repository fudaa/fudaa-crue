package org.fudaa.fudaa.crue.planimetry.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Observer;
import java.util.Set;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.projet.planimetry.LayerVisibility;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.layer.AbstractPlanimetryLigneBriseeExternLayer;
import org.fudaa.fudaa.crue.planimetry.layer.AbstractPlanimetryPointExternLayer;
import org.fudaa.fudaa.crue.planimetry.layer.LayerController;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternDrawLayerGroup;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternImagesLayerGroup;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternMainLayerGroup;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternSIGFileLayerGroup;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternTraceCasiersLayerGroup;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternTraceProfilsLayerGroup;

/**
 *
 * @author deniger
 */
public class GroupExterneMainController implements LayerController {

  PlanimetryExternMainLayerGroup groupExternLayer;
  final LinkedHashMap<String, AbstractGroupExternController> subControllers = new LinkedHashMap<>();

  public static GroupExterneMainController build(PlanimetryController controller, PlanimetryVisuPanel panel) {
    assert controller.groupExternController == null;
    GroupExterneMainController groupController = new GroupExterneMainController();
    groupController.groupExternLayer = new PlanimetryExternMainLayerGroup(panel, PlanimetryController.NAME_GC_EXTERN);

    PlanimetryExternSIGFileLayerGroup groupSIGFilesLayer = new PlanimetryExternSIGFileLayerGroup(panel, controller,
            PlanimetryController.NAME_GC_EXTERN_SIG);
    PlanimetryExternTraceProfilsLayerGroup tracesSectionsFilesLayer = new PlanimetryExternTraceProfilsLayerGroup(panel, controller,
            PlanimetryController.NAME_GC_EXTERN_TRACES);
    PlanimetryExternTraceCasiersLayerGroup tracesCasiersFilesLayer = new PlanimetryExternTraceCasiersLayerGroup(panel, controller,
            PlanimetryController.NAME_GC_EXTERN_TRACES_CASIERS);
    PlanimetryExternImagesLayerGroup groupImagesLayer = new PlanimetryExternImagesLayerGroup(panel, PlanimetryController.NAME_GC_EXTERN_IMAGES);
    PlanimetryExternDrawLayerGroup groupDrawLayer = new PlanimetryExternDrawLayerGroup(panel, controller, PlanimetryController.NAME_GC_EXTERN_DRAW);

    groupController.subControllers.put(PlanimetryController.NAME_GC_EXTERN_DRAW, new GroupExternDrawController(controller, groupDrawLayer));
    groupController.subControllers.put(PlanimetryController.NAME_GC_EXTERN_TRACES, new GroupExternTraceProfilsController(controller,
            tracesSectionsFilesLayer));
    groupController.subControllers.put(PlanimetryController.NAME_GC_EXTERN_TRACES_CASIERS, new GroupExternTraceCasiersController(controller,
            tracesCasiersFilesLayer));
    groupController.subControllers.put(PlanimetryController.NAME_GC_EXTERN_SIG, new GroupExternSIGFileController(controller, groupSIGFilesLayer));
    groupController.subControllers.put(PlanimetryController.NAME_GC_EXTERN_IMAGES, new GroupExternImagesController(controller, groupImagesLayer));
    Collection<AbstractGroupExternController> values = groupController.subControllers.values();

    for (AbstractGroupExternController abstractGroupExternController : values) {
      groupController.groupExternLayer.add(abstractGroupExternController.getGroupLayer());
    }

    controller.groupExternController = groupController;
    controller.controllersByName.put(PlanimetryController.NAME_GC_EXTERN, groupController);

    return groupController;
  }

  @Override
  public void setVisuConfiguration(VisuConfiguration cloned) {
    Collection<AbstractGroupExternController> values = subControllers.values();
    for (AbstractGroupExternController value : values) {
      value.setVisuConfiguration(cloned);
    }
  }

  public List<AbstractGroupExternController> getSubGroups() {
    return new ArrayList<>(subControllers.values());
  }

  public <T extends AbstractGroupExternController> T getSubController(String id) {
    return (T) subControllers.get(id);
  }

  public AbstractPlanimetryPointExternLayer addLayerPointUnmodifiable(GISZoneCollectionPoint pt, File file, SigFormatHelper.EnumFormat fmt,
          GroupeExternSIGType type) {
    if (GroupeExternSIGType.UNMODIFIABLE.equals(type)) {
      GroupExternSIGFileController sig = getSubController(PlanimetryController.NAME_GC_EXTERN_SIG);
      return sig.addLayerPointUnmodifiable(pt, file, fmt);
    } else {
      GroupExternDrawController sig = getSubController(PlanimetryController.NAME_GC_EXTERN_DRAW);
      return sig.addLayerPoint(pt);
    }
  }

  public AbstractPlanimetryLigneBriseeExternLayer addLayerLignesUnmodifiable(GISZoneCollectionLigneBrisee lignes, File file,
          SigFormatHelper.EnumFormat fmt, GroupeExternSIGType type) {
    if (GroupeExternSIGType.UNMODIFIABLE.equals(type)) {
      GroupExternSIGFileController sig = getSubController(PlanimetryController.NAME_GC_EXTERN_SIG);
      return sig.addLayerLignesUnmodifiable(lignes, file, fmt);
    } else {
      GroupExternDrawController sig = getSubController(PlanimetryController.NAME_GC_EXTERN_DRAW);
      return sig.addLayerLignes(lignes);
    }
  }

  public AbstractPlanimetryLigneBriseeExternLayer addTraceProfils(GISZoneCollectionLigneBrisee lignes, File file, SigFormatHelper.EnumFormat fmt) {
    GroupExternTraceProfilsController sig = getSubController(PlanimetryController.NAME_GC_EXTERN_TRACES);
    return sig.addTraceProfil(lignes, file, fmt);
  }

  public AbstractPlanimetryLigneBriseeExternLayer addTraceCasiers(GISZoneCollectionLigneBrisee lignes, File file, SigFormatHelper.EnumFormat fmt) {
    GroupExternTraceCasiersController sig = getSubController(PlanimetryController.NAME_GC_EXTERN_TRACES_CASIERS);
    return sig.addTraceCasiers(lignes, file, fmt);
  }

  public void initWithAdditionnalLayers(List<PlanimetryAdditionalLayerContrat> additionnalLayer) {
    Map<String, AbstractGroupExternController> controllerById = new HashMap<>();
    List<AbstractGroupExternController> subGroups = getSubGroups();
    for (AbstractGroupExternController subGroup : subGroups) {
      Set<String> idSupported = subGroup.getGroupLayer().getTypeIdSupported();
      subGroup.getGroupLayer().removeAll();
      for (String string : idSupported) {
        controllerById.put(string, subGroup);
      }
    }
    for (PlanimetryAdditionalLayerContrat layer : additionnalLayer) {
      if (controllerById.get(layer.getTypeId()) != null) {
        controllerById.get(layer.getTypeId()).addAdditionalLayer(layer);
      }
    }
  }

  @Override
  public boolean isEditable() {
    Collection<AbstractGroupExternController> values = subControllers.values();
    for (AbstractGroupExternController value : values) {
      if (value.isEditable()) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void changeDone() {
    Collection<AbstractGroupExternController> values = subControllers.values();
    for (AbstractGroupExternController value : values) {
      value.changeDone();
    }
  }

  @Override
  public void changeWillBeDone() {
    Collection<AbstractGroupExternController> values = subControllers.values();
    for (AbstractGroupExternController value : values) {
      value.changeWillBeDone();
    }
  }

  public void setArbreEditable(boolean editable) {
    Collection<AbstractGroupExternController> values = subControllers.values();
    for (AbstractGroupExternController value : values) {
      value.setArbreEditable(editable);
    }
  }

  @Override
  public void setEditable(boolean editable) {
    Collection<AbstractGroupExternController> values = subControllers.values();
    for (AbstractGroupExternController value : values) {
      value.setEditable(editable);
    }
  }

  @Override
  public void cancel() {
    Collection<AbstractGroupExternController> values = subControllers.values();
    for (AbstractGroupExternController value : values) {
      value.cancel();
    }
  }

  public PlanimetryExternMainLayerGroup getGroupExtern() {
    return groupExternLayer;
  }

  @Override
  public void saveDone() {
    Collection<AbstractGroupExternController> values = subControllers.values();
    for (AbstractGroupExternController value : values) {
      value.saveDone();
    }
  }
  PlanimetryControllerHelper helper;

  @Override
  public void init(PlanimetryControllerHelper helper) {
    this.helper = helper;
    Collection<AbstractGroupExternController> values = subControllers.values();
    for (AbstractGroupExternController value : values) {
      value.init(helper);
    }
  }

  public void addObserver(Observer observer) {
    Collection<AbstractGroupExternController> values = subControllers.values();
    for (AbstractGroupExternController value : values) {
      value.addObserver(observer);
    }
  }

  public void deleteObserver(Observer observer) {
    Collection<AbstractGroupExternController> values = subControllers.values();
    for (AbstractGroupExternController value : values) {
      value.deleteObserver(observer);
    }
  }

  void apply(LayerVisibility layerVisibility) {
    getGroupExtern().setVisible(layerVisibility.isGrOtherObjectVisible());
    getSubController(PlanimetryController.NAME_GC_EXTERN_SIG).getGroupLayer().setVisible(layerVisibility.isGrSIGFileVisible());
    getSubController(PlanimetryController.NAME_GC_EXTERN_DRAW).getGroupLayer().setVisible(layerVisibility.isGrDrawVisible());
    getSubController(PlanimetryController.NAME_GC_EXTERN_IMAGES).getGroupLayer().setVisible(layerVisibility.isGrImagesVisible());
    getSubController(PlanimetryController.NAME_GC_EXTERN_TRACES).getGroupLayer().setVisible(layerVisibility.isTraceProfilsVisible());
    getSubController(PlanimetryController.NAME_GC_EXTERN_TRACES_CASIERS).getGroupLayer().setVisible(layerVisibility.isTraceCasiersVisible());
  }

  void fill(LayerVisibility res) {
    res.setGrOtherObjectVisible(getGroupExtern().isVisible());
    res.setGrSIGFileVisible(getSubController(PlanimetryController.NAME_GC_EXTERN_SIG).getGroupLayer().isVisible());
    res.setGrDrawVisible(getSubController(PlanimetryController.NAME_GC_EXTERN_DRAW).getGroupLayer().isVisible());
    res.setGrImagesVisible(getSubController(PlanimetryController.NAME_GC_EXTERN_IMAGES).getGroupLayer().isVisible());
    res.setTraceProfilsVisible(getSubController(PlanimetryController.NAME_GC_EXTERN_TRACES).getGroupLayer().isVisible());
    res.setTraceCasiersVisible(getSubController(PlanimetryController.NAME_GC_EXTERN_TRACES_CASIERS).getGroupLayer().isVisible());
  }

  Map<String, Boolean> getVisibilityForExternLayer() {
    Map<String, Boolean> res = new HashMap<>();
    BCalque[] tousCalques = getGroupExtern().getTousCalques();
    for (BCalque bCalque : tousCalques) {
      if (!bCalque.isGroupeCalque()) {
        res.put(bCalque.getName(), bCalque.isVisible());
      }
    }
    return res;
  }

  void applyExternLayerVisibilty(Map<String, Boolean> visibilityByExternLayerName) {
    if (visibilityByExternLayerName == null) {
      return;
    }
    BCalque[] tousCalques = getGroupExtern().getTousCalques();
    for (BCalque bCalque : tousCalques) {
      if (!bCalque.isGroupeCalque()) {
        boolean visible = true;
        if (visibilityByExternLayerName.containsKey(bCalque.getName())) {
          visible = !Boolean.FALSE.equals(visibilityByExternLayerName.get(bCalque.getName()));
        }
        bCalque.setVisible(visible);
      }
    }
  }
}
