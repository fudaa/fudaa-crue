package org.fudaa.fudaa.crue.planimetry.layer;

import java.awt.Graphics2D;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EnumPosSection;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.crue.planimetry.action.LayerWithCascadeDeleteAction;
import org.fudaa.fudaa.crue.planimetry.configuration.LabelConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.SectionConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.controller.LayerSectionController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.fudaa.fudaa.crue.planimetry.find.PlanimetryFindEMHLayerAction;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 *
 * @author deniger
 */
public class PlanimetrySectionLayer extends PlanimetryPointLayer<SectionConfiguration> implements LayerWithCascadeDeleteAction,
        PlanimetryLayerWithEMHContrat {

  public PlanimetrySectionLayer(PlanimetrySectionLayerModel _modele, FSigEditor _editor) {
    super(_modele, _editor);
  }

  protected LayerSectionController getController() {
    return modeleDonnees().getLayerController();
  }

  @Override
  public boolean isMovable() {
    return false;
  }

  @Override
  public PlanimetrySectionLayerModel modeleDonnees() {
    return (PlanimetrySectionLayerModel) super.modeleDonnees();
  }

  @Override
  public String editSelected() {
    getActions()[0].actionPerformed(null);
    return null;
  }

  @Override
  public EbliFindActionInterface getFinder() {
    return new PlanimetryFindEMHLayerAction(this);
  }

  @Override
  public LayerControllerEMH getLayerControllerEMH() {
    return getLayerController();
  }

  @Override
  public void removeCascadeSelectedObjects() {
    if (!isSelectionEmpty()) {
      final boolean r = ((PlanimetrySectionLayerModel) modele_).removePointCascade(getSelectedIndex(), null);
      if (r) {
        clearSelection();
      }
    }
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  @Override
  public boolean isEditable() {
    return getLayerController().isEditable();
  }

  @Override
  public LayerSectionController getLayerController() {
    return (LayerSectionController) super.getLayerController();
  }
  final LabelPainter labelPainter = new LabelPainter();

  public VisuConfiguration getVisuConfiguration() {
    return super.layerConfiguration.getParent();
  }

  @Override
  protected boolean clearSelectionIfLayerNonVisible() {
    return false;
  }

  @Override
  protected boolean isPainted(int idx) {

    if (!isUserVisible()||!modeleDonnees().isGeometryVisible(idx)) {
      return false;
    }
    if (getLayerSelection().isSelected(idx)) {
      return true;
    }
    if (!getVisuConfiguration().isInactiveEMHVisible()) {
      CatEMHSection section = getController().getEMHFromPositionInModel(idx);
      return section != null && section.getActuallyActive();
    }
    return true;
  }

  private boolean userVisible = true;

  @Override
  public boolean isUserVisible() {
    return userVisible;
  }

  @Override
  public void setUserVisible(boolean userVisible) {
    if (this.userVisible != userVisible) {
      super.setUserVisible(userVisible);
      this.userVisible = userVisible;
      clearCacheAndRepaint();
    }
  }

  @Override
  public boolean isVisible() {
    return !isSelectionEmpty() || isUserVisible();
  }

  @Override
  public void paintDonnees(Graphics2D _g, GrMorphisme _versEcran, GrMorphisme _versReel, GrBoite _clipReel) {
    if (!isVisible()) {
      return;
    }
    if (!getLayerController().isBuild()) {
      getLayerController().rebuildGis();
    }
    super.paintDonnees(_g, _versEcran, _versReel, _clipReel);
    Set<Long> uidDone = new HashSet<>();
    final int nombre = modele_.getNombre();
    GrPoint pt = new GrPoint();
    for (int i = 0; i < nombre; i++) {
      if (!isPainted(i)) {
        continue;
      }
      RelationEMHSectionDansBranche sectionDansBranche = getController().getRelationEMHSectionDansBranche(i);
      final String nom = getLayerConfiguration().getDisplayName(sectionDansBranche.getEmh(), modeleDonnees(), selection_ != null && selection_.
              isSelected(i));
      if (!uidDone.contains(sectionDansBranche.getEmh().getUiId())) {
        modele_.point(pt, i, true);
        if (!_clipReel.contientXY(pt)) {
          continue;
        }
        pt.autoApplique(_versEcran);
        boolean aggregateLabels = getLayerConfiguration().canLabelsBeAggregate(modeleDonnees());
        paintLabel(uidDone, sectionDansBranche, nom, getController().getBranche(i), _g, pt, aggregateLabels);
      }
    }
  }

  protected void paintLabel(Set<Long> uidDone, RelationEMHSectionDansBranche sectionDansBranche, final String nom,
          CatEMHBranche branche, Graphics2D _g, GrPoint pt, boolean aggregateLabels) {
    if (!layerConfiguration.getLabelConfiguration().isDisplayLabels()) {
      return;
    }

    String nameToUse = nom;
    LabelConfiguration current = manageAmontVisuConfiguration(sectionDansBranche, branche);
    current = manageAvalVisuConfiugration(sectionDansBranche, branche, current);
    if (aggregateLabels) {
      uidDone.add(sectionDansBranche.getEmh().getUiId());
      String newName = manageAmont(sectionDansBranche, branche, uidDone);
      if (newName == null) {
        newName = manageAval(sectionDansBranche, branche, uidDone);
      }
      if (newName != null) {
        nameToUse = newName;
      }
    }
    labelPainter.paintLabels(_g, pt, nameToUse, current, alpha_);
  }

  private String manageAmont(RelationEMHSectionDansBranche sectionDansBranche, CatEMHBranche branche, Set<Long> uidDone) {
    String nameToUse = null;
    if (sectionDansBranche.getPos().equals(EnumPosSection.AMONT)) {
      CatEMHNoeud noeudAmont = branche.getNoeudAmont();
      if (noeudAmont != null) {
        List<CatEMHBranche> branchesAval = noeudAmont.getBranchesAval();
        if (branchesAval.size() > 1) {
          StringBuilder names = new StringBuilder();
          for (CatEMHBranche brancheAval : branchesAval) {
            if (names.length() > 0) {
              names.append(", ");
            }
            final RelationEMHSectionDansBranche sectionAmont = brancheAval.getSectionAmont();
            names.append(sectionAmont.getEmh().getNom());
            uidDone.add(sectionAmont.getEmh().getUiId());
          }
          nameToUse = names.toString();
        }
      }
    }
    return nameToUse;
  }

  private String manageAval(RelationEMHSectionDansBranche sectionDansBranche, CatEMHBranche branche, Set<Long> uidDone) {
    String nameToUse = null;
    if (sectionDansBranche.getPos().equals(EnumPosSection.AVAL)) {
      CatEMHNoeud noeudAval = branche.getNoeudAval();
      List<CatEMHBranche> branchesAmont = noeudAval.getBranchesAmont();
      if (branchesAmont.size() > 1) {
        StringBuilder names = new StringBuilder();
        for (CatEMHBranche brancheAmont : branchesAmont) {
          if (names.length() > 0) {
            names.append(", ");
          }
          final RelationEMHSectionDansBranche sectionAmont = brancheAmont.getSectionAval();
          names.append(sectionAmont.getEmh().getNom());
          uidDone.add(sectionAmont.getEmh().getUiId());
        }
        nameToUse = names.toString();
      }
    }
    return nameToUse;
  }

  private LabelConfiguration manageAmontVisuConfiguration(RelationEMHSectionDansBranche sectionDansBranche, CatEMHBranche branche) {
    if (sectionDansBranche.getPos().equals(EnumPosSection.AMONT)) {
      CatEMHNoeud noeudAmont = branche.getNoeudAmont();
      if (noeudAmont != null) {
        if (noeudAmont.getBranchesAmont().size() > 1) {
          LabelConfiguration labelConfiguration = layerConfiguration.getLabelConfiguration();
          LabelConfiguration current = labelConfiguration.clone();
          current.setLabelAlignment(LabelConfiguration.getSoutherAlignment(
                  layerConfiguration.getLabelConfiguration().getLabelAlignment()));
          return current;
        }
      }
    }
    return layerConfiguration.getLabelConfiguration();
  }

  private LabelConfiguration manageAvalVisuConfiugration(RelationEMHSectionDansBranche sectionDansBranche, CatEMHBranche branche,
          LabelConfiguration in) {
    if (sectionDansBranche.getPos().equals(EnumPosSection.AVAL)) {
      CatEMHNoeud noeudAval = branche.getNoeudAval();
      if (noeudAval != null && noeudAval.getBranchesAval().size() > 1) {
        LabelConfiguration current = in.clone();
        current.setLabelAlignment(LabelConfiguration.getNortherAlignment(in.getLabelAlignment()));
        return current;
      }
    }
    return in;
  }
}
