package org.fudaa.fudaa.crue.planimetry.layer;

import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;

/**
 *
 * @author deniger
 */
public interface LayerController {

  /**
   * Appele avant chaque modif
   */
  void changeWillBeDone();

  /**
   * Appele apres modificaiton
   */
  void changeDone();

  boolean isEditable();

  /**
   * appele des qu'une opération de sauvegarde a été effectuée.
   */
  void saveDone();

  void cancel();

  void setEditable(boolean editable);

  void init(PlanimetryControllerHelper helper);

  void setVisuConfiguration(VisuConfiguration cloned);
}
