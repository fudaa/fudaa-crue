package org.fudaa.fudaa.crue.planimetry.save;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessourceInfos;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;

/**
 *
 * @author deniger
 */
public class AdditionalLayerLoaderProcess {

  public interface Loader {

    String getType();

    PlanimetryAdditionalLayerContrat load(CrueEtudeExternRessourceInfos ressource, CtuluLog log, File baseDir);
  }
  private final Map<String, Loader> loaderByType = new HashMap<>();

  private List<Loader> createLoaders() {
    return Arrays.asList(new LayerImageControllerSaver(),
            new LayerLigneBriseeUnmodifiableControllerSaver(),
            new LayerPointUnmodifiableControllerSaver(),
            new LayerLigneBriseeModifiableControllerSaver(),
            new LayerPointModifiableControllerSaver(),
            new LayerTraceProfilsControllerSaver(),
            new LayerTraceCasiersControllerSaver());
  }

  public CrueIOResu<List<PlanimetryAdditionalLayerContrat>> load(List<CrueEtudeExternRessourceInfos> ressources,
          File baseDir) {
    List<Loader> createLoaders = createLoaders();
    for (Loader loader : createLoaders) {
      loaderByType.put(loader.getType(), loader);
    }
    CrueIOResu<List<PlanimetryAdditionalLayerContrat>> resu = new CrueIOResu<>();
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    List<PlanimetryAdditionalLayerContrat> additionalLayers = new ArrayList<>();
    resu.setMetier(additionalLayers);
    resu.setAnalyse(log);
    int idx = 1;
    if (ressources != null) {
      for (CrueEtudeExternRessourceInfos ressource : ressources) {

        Loader loader = loaderByType.get(ressource.getType());
        if (loader == null) {
          log.addError("externConfig.typeUnknown.error", ressource.getType(), Integer.toString(idx));
          return null;
        }
        PlanimetryAdditionalLayerContrat load = loader.load(ressource, log, baseDir);
        if (load != null) {
          additionalLayers.add(load);
        }
        idx++;
      }
    }
    return resu;

  }
}
