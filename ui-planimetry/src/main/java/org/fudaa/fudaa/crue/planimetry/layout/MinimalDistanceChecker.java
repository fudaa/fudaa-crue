package org.fudaa.fudaa.crue.planimetry.layout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;

/**
 *
 * @author deniger
 */
public class MinimalDistanceChecker {

  private final PlanimetryController controller;

  public MinimalDistanceChecker(PlanimetryController controller) {
    this.controller = controller;
  }

  public void check(double minimalDistance) {
    controller.getSceneListener().setGlobalChangeStarted(true);
    try {
      GISZoneCollectionPoint nodeCollection = controller.getNodeController().getNodeCollection();
      List<NodeContent> contents = new ArrayList<>();
      for (int i = 0; i < nodeCollection.getNumPoints(); i++) {
        contents.add(new NodeContent(nodeCollection, i));
      }
      Collections.sort(contents, new NodeContentYComparator());
      modifyYTop(contents, 0, minimalDistance);
      Collections.sort(contents);
      modifyXTop(contents, 0, minimalDistance);
    } finally {
      controller.getBrancheController().updateBrancheFromNode();
      controller.getCasierController().updateCasierFromNode();
      controller.getSceneListener().setGlobalChangeFinished();
    }
  }

  private void modifyXTop(List<NodeContent> nodes, int start, double minimalDistance) {
    for (int i = start; i < nodes.size() - 1; i++) {
      NodeContent current = nodes.get(i);
      boolean goOn = true;
      for (int j = start + 1; j < nodes.size() - 1 && goOn; j++) {
        NodeContent next = nodes.get(j);
        double distance = CtuluLibGeometrie.getDistance(current.getX(), current.getY(), next.getX(), next.getY());
        if (distance < minimalDistance) {
          //on deplace sur la ligne current ->next
          double diffX = Math.abs(next.getX() - current.getX());
          double diffY = Math.abs(next.getY() - current.getY());
          double deltaX = Math.sqrt(Math.abs(minimalDistance * minimalDistance - diffY * diffY)) - diffX;
          double deltaY = 0;
          propagateTop(nodes, j, deltaX, deltaY, true, current.getX());
        } else if (Math.abs(current.getX() - next.getX()) > minimalDistance) {
          goOn = false;
        }
      }
    }
  }

  /**
   * on modifie les y sur la même ligne
   *
   * @param nodes liste des nodes
   * @param start indice de depart
   * @param minimalDistance distance minimale
   */
  private void modifyYTop(List<NodeContent> nodes, int start, double minimalDistance) {
    for (int i = start; i < nodes.size() - 1; i++) {
      NodeContent current = nodes.get(i);
      boolean goOn = true;
      for (int j = start + 1; j < nodes.size() - 1 && goOn; j++) {
        NodeContent next = nodes.get(j);
        if (Math.abs(current.getX() - next.getX()) < 0.1) {
          double distance = CtuluLibGeometrie.getDistance(current.getX(), current.getY(), next.getX(), next.getY());
          if (distance < minimalDistance) {
            //on deplace sur la ligne current ->next
            double diffX = Math.abs(next.getX() - current.getX());
            double diffY = Math.abs(next.getY() - current.getY());
            double deltaY = Math.sqrt(Math.abs(minimalDistance * minimalDistance - diffX * diffX)) - diffY;
            double deltaX = 0;
            propagateTop(nodes, j, deltaX, deltaY, false, current.getX());
          } else if (Math.abs(current.getY() - next.getY()) > minimalDistance) {
            goOn = false;
          }
        }
      }
    }
  }

  private void propagateTop(List<NodeContent> nodes, int start, double dx, double dy, boolean notSame, double x) {
    for (int i = start; i < nodes.size(); i++) {
      final NodeContent node = nodes.get(i);
      boolean skip = notSame && Math.abs(node.getX() - x) < 1e-2;
      if (!skip) {
        node.setXY(node.getX() + dx, node.getY() + dy);
      }
    }
  }

  private static class NodeContentYComparator extends SafeComparator< NodeContent> {

    @Override
    protected int compareSafe(NodeContent o1, NodeContent o2) {
      if (o1.idx == o2.idx) {
        return 0;
      }
      if (o1.getY() > o2.getY()) {
        return 1;
      }
      if (o1.getY() < o2.getY()) {
        return -1;
      }
      if (o1.getX() > o2.getX()) {
        return 1;
      }
      if (o1.getX() < o2.getX()) {
        return -1;
      }

      return o1.idx > o2.idx ? 1 : -1;
    }
  }

  private static class NodeContent implements Comparable<NodeContent> {

    private final GISZoneCollectionPoint points;
    private final int idx;

    public NodeContent(GISZoneCollectionPoint points, int idx) {
      this.points = points;
      this.idx = idx;
    }

    public int getIdx() {
      return idx;
    }

    @Override
    public int compareTo(NodeContent o) {
      if (o == null) {
        return 1;
      }
      if (o == this || o.idx == idx) {
        return 0;
      }
      if (getX() > o.getX()) {
        return 1;
      }
      if (getX() < o.getX()) {
        return -1;
      }
      if (getY() > o.getY()) {
        return 1;
      }
      if (getY() < o.getY()) {
        return -1;
      }
      return idx > o.idx ? 1 : -1;
    }

    public double getX() {
      return points.getX(idx);
    }

    public void setXY(double x, double y) {
      points.set(idx, x, y, null);
    }

    public double getY() {
      return points.getY(idx);
    }
  }
}
