/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.tracecasier;

import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import gnu.trove.TObjectIntHashMap;
import java.awt.EventQueue;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilCasier;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHCasierProfil;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.planimetry.controller.LayerCasierController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryCasierLayerModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternTraceCasierLayer;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class TraceCasierToCasierProcessor implements ProgressRunnable<CtuluLogGroup> {

  private final PlanimetryControllerHelper helper;
  private final List<TraceCasierSousModele> todos;
  private final PlanimetryExternTraceCasierLayer layer;
  private final TObjectIntHashMap<String> traceCasierByName;

  public TraceCasierToCasierProcessor(PlanimetryControllerHelper helper, List<TraceCasierSousModele> todos,
          PlanimetryExternTraceCasierLayer layer, TObjectIntHashMap<String> traceCasierPositionByName) {
    this.helper = helper;
    this.todos = todos;
    this.layer = layer;
    this.traceCasierByName = traceCasierPositionByName;
  }

  @Override
  public CtuluLogGroup run(ProgressHandle handle) {
    handle.switchToDeterminate(todos.size());
    helper.scenarioEditionStarted();
    helper.getCasierController().changeWillBeDone();
    final EMHScenario scenario = helper.getPlanimetryGisModelContainer().getScenario();
    List<EMHSousModele> sousModeles = scenario.getSousModeles();
    Map<String, EMH> emhByNom = scenario.getIdRegistry().getEmhByNom();
    Map<String, EMHSousModele> sousModeleByNom = TransformerHelper.toMapOfNom(sousModeles);
    CtuluLogGroup res = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    for (TraceCasierSousModele TraceCasierSousModele : todos) {
      handle.progress(TraceCasierSousModele.getCasierName(), 1);
      process(TraceCasierSousModele, sousModeleByNom, emhByNom, res);
    }
    propagateModification();

    return res;
  }

  public void propagateModification() {
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        helper.getCasierController().changeDone();
        helper.getCasierController().getLayer().repaint(0);
      }
    });
  }

  private void process(TraceCasierSousModele traceCasierSousModele, Map<String, EMHSousModele> sousModeleByNom, Map<String, EMH> emhByNom,
          CtuluLogGroup res) {
    EMHSousModele targetSousModele = sousModeleByNom.get(traceCasierSousModele.getSousModeleName());
    if (targetSousModele == null || !traceCasierByName.containsKey(traceCasierSousModele.getCasierName())) {
      return;
    }
    int casierIdx = traceCasierByName.get(traceCasierSousModele.getCasierName());
    LineString lineString = (LineString) layer.getModelEditable().getGeometries().getGeometry(casierIdx);
    final String noeudName = traceCasierSousModele.getNoeudName();
    CatEMHNoeud noeud = (CatEMHNoeud) emhByNom.get(noeudName);
    LinearRing transformToLinearRing = GISGeometryFactory.INSTANCE.transformToLinearRing(lineString);
    final GISPolygone polygone = GISGeometryFactory.INSTANCE.createLinearRing(transformToLinearRing.getCoordinateSequence());
    final LayerCasierController casierController = layer.getLayerController().getHelper().getCasierController();
    PlanimetryCasierLayerModel casiersModel = (PlanimetryCasierLayerModel) casierController.getLayer().getModelEditable();
    CatEMHCasier casier = null;
    if (!traceCasierSousModele.isNoeudExists() || !traceCasierSousModele.isCasierExists()) {
      //on créé l'ensemble
      casier = casiersModel.addCasier(helper, noeud, polygone, traceCasierSousModele.getCasierName());
    } else {
      //le casier existe: on le modifie
      casier = (CatEMHCasier) emhByNom.get(traceCasierSousModele.getCasierName());
      int casierPosition = casierController.getCasierPosition(casier.getUiId());
      casiersModel.setGeometry(casierPosition, polygone);
    }
    if (casier instanceof EMHCasierProfil && traceCasierSousModele.containProfil) {
      Collection<DonPrtGeoProfilCasier> oldDPTG = DonPrtHelper.getDonPrtGeoProfilCasier(casier);
      TraceCasierToDonPrtGeoProfilCasierTransformer toDPTGTransformer = new TraceCasierToDonPrtGeoProfilCasierTransformer(helper.getCcm(),
              targetSousModele);
      DonPrtGeoProfilCasier newDPTG = toDPTGTransformer.getDonPrtGeoProfilCasier(layer.getModelEditable().getGeometries(), casierIdx);
      if (newDPTG != null) {
        casier.removeInfosEMH(oldDPTG);
        casier.addInfosEMH(newDPTG);
      }
    }
    CtuluLog success = new CtuluLog();
    success.setDesc(NbBundle.getMessage(TraceCasierToCasierProcessor.class, "traceCasier.casierCreated", traceCasierSousModele.getCasierName()));
    res.addLog(success);
  }

  public String getLogDesc(TraceCasierSousModele TraceCasierSousModele) throws MissingResourceException {
    return NbBundle.getMessage(TraceCasierToCasierProcessor.class, "traceProfil.noModificationDone", TraceCasierSousModele.getCasierName());
  }
}
