/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.tracecasier;

import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.fudaa.crue.planimetry.sig.traceprofil.GGTraceProfilSectionAttributesController;

/**
 *
 * @author Frederic Deniger
 */
public class TraceCasierToNoeudNameTransformer {

  public String getNoeudName(String traceProfilName) {
    return CruePrefix.changePrefix(traceProfilName, EnumCatEMH.NOEUD);
  }

  public String getNoeudName(GISZoneCollection traceCasier, int idx) {
    String name = (String) traceCasier.getModel(GGTraceProfilSectionAttributesController.NOM).getObjectValueAt(idx);
    return getNoeudName(name);
  }
}
