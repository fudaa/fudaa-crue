package org.fudaa.fudaa.crue.planimetry.layout;

import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.locationtech.jts.geom.Coordinate;

import java.util.HashSet;
import java.util.Set;

/**
 * Contient les données geometriques à modifier lors de la reconstruction du reseau geometrique
 */
public class NetworkPresetData {

    NetworkGisPositionnerResult newPosition;
    Set<String> emhNameToUpdate=new HashSet<>();

    public NetworkPresetData(EMHScenario emhScenario){
        newPosition=new NetworkGisPositionnerResult(emhScenario,false);
    }


    /**
     * Ajouter une association entre une branche et son support geographique
     * @param emh la branche
     * @param polyligne la polyligne support la branche emh
     */
    public void addBranche(CatEMHBranche emh, GISPolyligne polyligne){
        emhNameToUpdate.add(emh.getNom());
        newPosition.addEdge(polyligne,emh);
    }

    /**
     * Ajouter une association entre un noeud et son support geographique
     * @param emh le noeud
     * @param pt le support geographique
     */
    public void addNoeud(CatEMHNoeud  emh, Coordinate pt){
        //ne pas ajouter le noeud si déjà présent
        if(emhNameToUpdate.add(emh.getNom())) {
            newPosition.addNode(pt.x, pt.y, emh);
        }
    }

    public NetworkGisPositionnerResult getNewPosition() {
        return newPosition;
    }

    public Set<String> getEmhNameToUpdate() {
        return emhNameToUpdate;
    }
}
