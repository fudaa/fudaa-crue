/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.validation.util.ValidationContentSectionDansBrancheExecutor;
import org.openide.util.NbBundle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Permet de filtrer les branches/sections en entrée afin de ne pas traiter les cas simples.
 *
 * @author Frederic Deniger
 */
public class SectionProfilToBrancheProcessorFilter {
    private final Map<String, EMH> emhByNom;

    public SectionProfilToBrancheProcessorFilter(EMHScenario scenario) {
        emhByNom = scenario.getIdRegistry().getEmhByNom();
    }

    /**
     * @param branchesParameters les parametres de branches
     * @return les paramètres sans les branches incompatibles.
     */
    public CtuluLogResult<SectionProfilToBrancheParametersBranche> filter(SectionProfilToBrancheParametersBranche branchesParameters) {
        Map<EnumBrancheType, Collection<EnumSectionType>> sectionsTypeByBrancheType = ValidationContentSectionDansBrancheExecutor.
                createSectionInterneTypeDansBrancheContents();
        SectionProfilToBrancheParametersBranche res = new SectionProfilToBrancheParametersBranche();
        CtuluLog notCompatibleBranche = new CtuluLog();
        notCompatibleBranche.setDesc(NbBundle.getMessage(SectionProfilToBrancheProcessorFilter.class, "traceProfil.brancheNotCompatible.LogName"));
        Map<String, GISPolyligne> ligneByBrancheName = branchesParameters.getLigneByBrancheName();
        for (Map.Entry<String, GISPolyligne> entry : ligneByBrancheName.entrySet()) {
            String brancheName = entry.getKey();
            CatEMHBranche branche = (CatEMHBranche) emhByNom.get(brancheName);
            Collection<EnumSectionType> authorizedSection = sectionsTypeByBrancheType.get(branche.getBrancheType());
            if (!authorizedSection.contains(EnumSectionType.EMHSectionProfil)) {
                notCompatibleBranche.addWarn(NbBundle.getMessage(SectionProfilToBrancheProcessorFilter.class, "traceProfil.brancheNotCompatible"));
            } else {
                res.add(brancheName, entry.getValue());
            }
        }
        return new CtuluLogResult<>(res, notCompatibleBranche);
    }

    /**
     * Filtre les traces qui n'ont pas de sections et les sections qui ne pourront pas être modifiées car amont/aval d'une branche.
     *
     * @param sectionsParameters les parametres de sections.
     */
    public CtuluLogResult<List<SectionProfilToBrancheParametersSection>> filterSection(
            List<SectionProfilToBrancheParametersSection> sectionsParameters) {
        CtuluLog log = new CtuluLog();
        log.setDesc(NbBundle.getMessage(SectionProfilToBrancheProcessorFilter.class, "traceProfil.sectionAmontAval.LogName"));
        List<SectionProfilToBrancheParametersSection> resList = new ArrayList<>();
        for (SectionProfilToBrancheParametersSection param : sectionsParameters) {
            CatEMHSection section = (CatEMHSection) emhByNom.get(param.getSectionName());
            //si une branche parente, on vérifie que la section n'est ni l'avale ni l'amont.
            if (section == null) {
                log.addWarn(NbBundle.getMessage(SectionProfilToBrancheProcessorFilter.class, "traceProfil.traceIgnored.noSection", param.getTraceName()));
            } else {
                if (section.getBranche() != null) {
                    boolean isAmont = section == section.getBranche().getSectionAmont().getEmh();
                    boolean isAval = section == section.getBranche().getSectionAval().getEmh();
                    if (isAmont) {
                        log.addWarn(NbBundle.getMessage(SectionProfilToBrancheProcessorFilter.class, "traceProfil.sectionIgnored.sectionAmont", param.
                                getSectionName(), section.getBranche().getNom()));
                    } else if (isAval) {
                        log.addWarn(NbBundle.getMessage(SectionProfilToBrancheProcessorFilter.class, "traceProfil.sectionIgnored.sectionAval", param.
                                getSectionName(), section.getBranche().getNom()));
                    } else {
                        resList.add(param);
                    }
                } else {
                    resList.add(param);
                }
            }
        }
        return new CtuluLogResult<>(resList, log);
    }
}
