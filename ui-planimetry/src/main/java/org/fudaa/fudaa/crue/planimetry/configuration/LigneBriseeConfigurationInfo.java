package org.fudaa.fudaa.crue.planimetry.configuration;

import java.util.ArrayList;
import java.util.List;
import org.openide.nodes.Sheet;
import org.openide.util.NbBundle;

/**
 ** Attention. Les name des set sont utilisés pour la persistence dans le xml. Donc, ne pas changer sans rejouer les tests unitaires et faire des
 * reprises
 *
 * @author deniger
 */
public class LigneBriseeConfigurationInfo {

  public static final String PREFIX_LINE = "lines";

  public static List<Sheet.Set> createSet(LigneBriseeConfiguration in) {
    List<Sheet.Set> res = new ArrayList<>();
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setName(PREFIX_LINE);
    set.put(VisuConfigurationInfo.createTransparency(in));
    set.setDisplayName(NbBundle.getMessage(VisuConfigurationInfo.class, "LineConfiguration.DisplayName"));
    set.setShortDescription(set.getDisplayName());
    res.add(set);
    res.add(TraceIconModelConfigurationInfo.createSet(in.icon, PREFIX_LINE));
    res.add(TraceLineModelConfigurationInfo.createSet(in.line, PREFIX_LINE, true));
    res.add(LabelConfigurationInfo.createSet(in.labelConfiguration, PREFIX_LINE, true));
    return res;
  }
}
