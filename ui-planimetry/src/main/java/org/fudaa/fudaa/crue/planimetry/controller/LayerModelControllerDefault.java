package org.fudaa.fudaa.crue.planimetry.controller;

import org.fudaa.ctulu.gis.GISZoneCollectionGeometry;
import org.fudaa.ctulu.gis.GISZoneListener;
import org.fudaa.ctulu.gis.GISZoneListenerDispatcher;
import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.ZModeleDonnees;
import org.fudaa.ebli.calque.ZModeleGeometry;
import org.fudaa.ebli.calque.edition.ZModeleEditable;
import org.fudaa.ebli.calque.edition.ZModeleGeometryDefault;
import org.fudaa.fudaa.crue.planimetry.layer.LayerController;

/**
 *
 * @author deniger
 */
public abstract class LayerModelControllerDefault<L extends ZCalqueAffichageDonnees> implements LayerController {

  boolean modified;
  boolean editable;
  GISZoneCollectionGeometry clonedData;
  final GISZoneListenerDispatcher listenerDispatcher = new GISZoneListenerDispatcher();
  L layer;
  PlanimetryControllerHelper helper;
  public static final int MODIFICATION_DONE = 100;

  @Override
  public void changeWillBeDone() {
    cloneDataIfNeeded();
    helper.changeDone(this);
  }

  protected void cloneDataIfNeeded() {
    //a copy must be done to undo changes
    if (!modified) {
      modified = true;
      clonedData = (GISZoneCollectionGeometry) getLayerModel().getGeomData().clone();
    }
  }

  public CreationDefaultValue getDefaultValues() {
    return helper.getController().getDefaultValues();
  }

  /**
   * Envoie un evt avec l'id MODIFICATION_DONE
   */
  @Override
  public void changeDone() {
    listenerDispatcher.objectAction(layer, -1, null, MODIFICATION_DONE);

  }

  public boolean isModified() {
    return modified;
  }

  @Override
  public void saveDone() {
    modified = false;
    clonedData = null;
  }

  public void resetModification() {
    modified = false;
    clonedData = null;
  }

  @Override
  public void cancel() {
    if (modified) {
      clonedData.setListener(listenerDispatcher);
      getLayerModel().setGeometries(clonedData);
      modified = false;
      getLayer().repaint();
    }

  }

  protected ZModeleGeometryDefault getLayerModel() {
    return (ZModeleGeometryDefault) layer.modeleDonnees();
  }

  @Override
  public void setEditable(boolean editable) {
    this.editable = editable;
    ZModeleDonnees modeleDonnees = layer.modeleDonnees();
    if (modeleDonnees instanceof ZModeleEditable) {
      ((ZModeleEditable) modeleDonnees).getGeomData().setGeomModifiable(editable);
    }
    getLayer().setActionsEnable(editable);
  }

  @Override
  public boolean isEditable() {
    return editable;
  }

  public PlanimetryControllerHelper getHelper() {
    return helper;
  }
  
  public void openFindDialog() {
    getHelper().getController().getVisuPanel().findSelectedLayer();
  }

  /**
   *
   * @param listener listener sur les chahgement dans les données geoloc.
   */
  public void addGISListener(GISZoneListener listener) {
    listenerDispatcher.addListener(listener);
  }

  public L getLayer() {
    return layer;
  }

  @Override
  public void init(PlanimetryControllerHelper helper) {
    this.helper = helper;
  }

  public void setGeometryData(GISZoneCollectionGeometry loaded) {
    ZModeleGeometryDefault modele = (ZModeleGeometryDefault) layer.modeleDonnees();
    modele.getGeomData().setListener(null);
    modele.setGeometries(loaded);
    modele.getGeomData().setListener(listenerDispatcher);
    geometryDataUpdated();
  }

  /**
   * Appele lors la liste des geometries a été modifiée.
   */
  protected void geometryDataUpdated() {
  }

  public <T extends EMH> T getEMH(Long uid) {
    return helper.getPlanimetryGisModelContainer().getEMH(uid);
  }

  public GISZoneCollectionGeometry getGeomData() {
    return (GISZoneCollectionGeometry) ((ZModeleGeometry) layer.modeleDonnees()).getGeomData();
  }

  public Long getUid(int positionInModel) {
    return getHelper().getUid(getGeomData(), positionInModel);
  }

  public <T extends EMH> T getEMHFromPositionInModel(int positionInModel) {
    Long uid = getUid(positionInModel);
    return uid == null ? null : (T) getEMH(uid);
  }
}
