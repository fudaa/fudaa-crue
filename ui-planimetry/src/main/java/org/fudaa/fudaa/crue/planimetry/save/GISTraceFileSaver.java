/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.save;

import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternAdditionalFileSaver;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessourceInfos;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.openide.util.Exceptions;

import java.io.File;

/**
 * Sauvegarde des fichiers contenants des informations relative au TraceProfil
 *
 * @author Frederic Deniger
 */
public class GISTraceFileSaver implements CrueEtudeExternAdditionalFileSaver {
  protected final CrueEtudeExternRessourceInfos infos;
  private final GISZoneCollection zone;
  private final AbstractLayerTraceControllerSaver saver;

  public GISTraceFileSaver(CrueEtudeExternRessourceInfos infos, GISZoneCollection zone, AbstractLayerTraceControllerSaver saver) {
    this.infos = infos;
    this.zone = zone;
    this.saver = saver;
  }

  @Override
  public void save(File configDir, File dessinDir) {
    File targetFile = saver.getTargetFile(configDir, infos.getLayerId());
    if (!targetFile.exists()) {
      PlanimetryGISGMLZoneExporter exporter = new PlanimetryGISGMLZoneExporter();
//
      try {
        final ShapefileDataStoreFactory fac = new ShapefileDataStoreFactory();
        exporter.process(null, zone, fac.createDataStore(targetFile.toURI().toURL()));
      } catch (Exception ex) {
        Exceptions.printStackTrace(ex);
      }
    }
  }
}
