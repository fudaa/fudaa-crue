package org.fudaa.fudaa.crue.planimetry.layer;

import com.memoire.bu.BuPopupMenu;
import org.fudaa.ctulu.CtuluListSelection;
import org.locationtech.jts.geom.Coordinate;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Area;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GisAbscCurviligneToCoordinate;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.fudaa.crue.planimetry.action.LayerWithCascadeDeleteAction;
import org.fudaa.fudaa.crue.planimetry.action.PlanimetryDclmController;
import org.fudaa.fudaa.crue.planimetry.configuration.BrancheConfiguration;
import org.fudaa.fudaa.crue.planimetry.controller.LayerBrancheController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.fudaa.fudaa.crue.planimetry.find.PlanimetryFindEMHLayerAction;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 *
 * @author deniger
 */
public class PlanimetryBrancheLayer extends PlanimetryLigneBriseeLayer<BrancheConfiguration>
        implements LayerWithCascadeDeleteAction, PlanimetryLayerWithEMHContrat, CalqueWithDynamicActions {

  public PlanimetryBrancheLayer(PlanimetryBrancheModel _modele, FSigEditor _editor) {
    super(_modele, _editor);
  }

  PlanimetryBrancheModel getBrancheModel() {
    return modeleDonnees();
  }

  @Override
  public PlanimetryBrancheModel modeleDonnees() {
    return (PlanimetryBrancheModel) super.modeleDonnees();
  }

  @Override
  public EbliFindActionInterface getFinder() {
    return new PlanimetryFindEMHLayerAction(this);
  }

  @Override
  public void removeCascadeSelectedObjects() {
    if (!isSelectionEmpty()) {
      final boolean r = ((PlanimetryBrancheModel) modele_).removeLigneBriseeCascade(getSelectedIndex(), null);
      if (r) {
        clearSelection();
      }
    }
  }

  @Override
  public void fillPopup(BuPopupMenu popup) {
    if (isEditable() && isOnlyOneObjectSelected()) {
      int idx = getSelectedIndex()[0];
      EMH emh = getLayerControllerEMH().getEMHFromLayerPosition(idx);
      PlanimetryDclmController dclmController = new PlanimetryDclmController(getBrancheController().getHelper());
      dclmController.fillPopupMenu(emh, popup);
    }
  }

  @Override
  public LayerBrancheController getLayerController() {
    return (LayerBrancheController) super.getLayerController();
  }

  @Override
  public String editSelected() {
    getActions()[0].actionPerformed(null);
    return null;
  }

  LayerBrancheController getBrancheController() {
    return getBrancheModel().getLayerController();
  }

  @Override
  public boolean isMovable() {
    return false;
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  @Override
  protected boolean showOrientation(int idxPoly) {
    return layerConfiguration.isDisplaySens() || isSelected(idxPoly);
  }

  @Override
  public int getFlecheSize() {
    return layerConfiguration.getFlecheSize();
  }

  private boolean userVisible = true;

  @Override
  public boolean isUserVisible() {
    return userVisible;
  }

  @Override
  public void setUserVisible(boolean userVisible) {
    if (this.userVisible != userVisible) {
      super.setUserVisible(userVisible);
      this.userVisible = userVisible;
      clearCacheAndRepaint();
    }
  }

  @Override
  public boolean isVisible() {
    return !isSelectionEmpty() || isUserVisible();
  }

  @Override
  protected boolean clearSelectionIfLayerNonVisible() {
    return false;
  }

  @Override
  protected boolean isPainted(int idx, GrMorphisme _versEcran) {
    if (!modeleDonnees().isGeometryVisible(idx)||!isUserVisible()) {
      return false;
    }
    if (isSelected(idx)) {
      return true;
    }
    if (!layerConfiguration.isInactiveEMHVisible()) {
      CatEMHBranche branche = getBrancheController().getEMHFromPositionInModel(idx);
      return branche != null && branche.getActuallyActive();
    }
    return true;
  }


  @Override
  public boolean canAddForme(int _typeForme) {
    return isEditable() && _typeForme == DeForme.LIGNE_BRISEE;
  }
  private final LabelPainter labelPainter = new LabelPainter();

  @Override
  public void paintDonnees(Graphics2D _g, GrMorphisme versEcran, GrMorphisme _versReel, GrBoite clipReel) {
    super.paintDonnees(_g, versEcran, _versReel, clipReel);
    final int nombre = modele_.getNombre();
    GisAbscCurviligneToCoordinate.Result result = new GisAbscCurviligneToCoordinate.Result();
    GrPoint pt = new GrPoint();
    GrSegment segment = new GrSegment(new GrPoint(), new GrPoint());
    for (int i = nombre - 1; i >= 0; i--) {
      // il n'y a pas de points pour cette ligne
      if (modele_.getNbPointForGeometry(i) <= 0) {
        continue;
      }
      // La géometrie n'est pas visible
      if (!isPainted(i, versEcran)) {
        continue;
      }
      GISPolyligne ligne = (GISPolyligne) modele_.getGeomData().getGeometry(i);
      GisAbscCurviligneToCoordinate.find(ligne, 0.5, result);
      if (result.isFound()) {
        //trouve la direction !

        pt.x_ = result.getCoordinate().x;
        pt.y_ = result.getCoordinate().y;
        if (clipReel.contientXY(pt)) {
          pt.autoApplique(versEcran);
          int segmentidx = result.getSegmentidx();
          Coordinate coordinateN = ligne.getCoordinateN(segmentidx);
          segment.o_.x_ = coordinateN.x;
          segment.o_.y_ = coordinateN.y;
          coordinateN = ligne.getCoordinateN(segmentidx + 1);
          segment.e_.x_ = coordinateN.x;
          segment.e_.y_ = coordinateN.y;
          segment.applique(versEcran);
          CatEMHBranche branche = getBrancheController().getBranche(i);
          TraceIcon icon = super.layerConfiguration.getMiddleIcon(branche, modeleDonnees(), segment);
          icon.paintIconCentre(_g, pt.x_, pt.y_);
          if (layerConfiguration.getLabelConfiguration().isDisplayLabels()) {
            labelPainter.paintLabels(_g, pt, layerConfiguration.getDisplayName(branche, modeleDonnees(), isSelected(i)), layerConfiguration.
                    getLabelConfiguration(), alpha_);
          }
        }
      }
    }
  }

  @Override
  protected void updateClip(Graphics2D _g, Shape oldClip, GrMorphisme _versEcran, int idx) {
    _g.setClip(oldClip);
    GisAbscCurviligneToCoordinate.Result result = new GisAbscCurviligneToCoordinate.Result();
    GISPolyligne ligne = (GISPolyligne) modele_.getGeomData().getGeometry(idx);
    GisAbscCurviligneToCoordinate.find(ligne, 0.5, result);
    if (result.isFound()) {
      int middleIconWidth = layerConfiguration.getMiddleIconWidth(modeleDonnees());
      Area newClip = new Area(oldClip);
      GrPoint pt = new GrPoint(result.getCoordinate().x, result.getCoordinate().y, 0);
      pt.autoApplique(_versEcran);
      Rectangle rectangle = new Rectangle((int) pt.x_ - middleIconWidth / 2, (int) pt.y_ - middleIconWidth / 2, middleIconWidth,
              middleIconWidth);
      newClip.subtract(new Area(rectangle));
      _g.setClip(newClip);

    }
  }

  @Override
  public LayerControllerEMH getLayerControllerEMH() {
    return getLayerController();
  }
}
