/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import gnu.trove.TLongDoubleHashMap;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.EditionChangeRelationSection;
import org.fudaa.dodico.crue.edition.ListRelationSectionContentFactory;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent;
import org.fudaa.dodico.crue.edition.bean.ValidationMessage;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.NbBundle;

/**
 * Effectue les modifications. En entrée, les données sont prévalidées par les autres processor.
 *
 * @author Frederic Deniger
 */
public class SectionProfilToBrancheProcessor implements ProgressRunnable<CtuluLogGroup> {

  private final PlanimetryControllerHelper helper;
  private final List<SectionProfilToBrancheParametersChange> acceptedModification;

  public SectionProfilToBrancheProcessor(PlanimetryControllerHelper helper,
          List<SectionProfilToBrancheParametersChange> acceptedModification) {
    this.helper = helper;
    this.acceptedModification = acceptedModification;
  }

  private Map<String, List<SectionProfilToBrancheParametersChange>> getBySousModeleNames(Map<String, EMH> emhByNom) {
    Map<String, List<SectionProfilToBrancheParametersChange>> res = new HashMap<>();
    for (SectionProfilToBrancheParametersChange change : acceptedModification) {
      CatEMHBranche branche = (CatEMHBranche) emhByNom.get(change.getNewBrancheName());
      String sousModelNom = branche.getParent().getNom();
      List<SectionProfilToBrancheParametersChange> list = res.get(sousModelNom);
      if (list == null) {
        list = new ArrayList<>();
        res.put(sousModelNom, list);
      }
      list.add(change);

    }
    return res;
  }

  @Override
  public CtuluLogGroup run(ProgressHandle handle) {
    helper.scenarioEditionStarted();
    helper.getTraceController().changeWillBeDone();
    Map<String, EMH> emhByNom = helper.getPlanimetryGisModelContainer().getScenario().getIdRegistry().getEmhByNom();
    CtuluLogGroup res = new CtuluLogGroup(null);
    Map<String, List<SectionProfilToBrancheParametersChange>> bySousModeleNames = getBySousModeleNames(emhByNom);
    for (Map.Entry<String, List<SectionProfilToBrancheParametersChange>> entry : bySousModeleNames.entrySet()) {
      String sousModeleName = entry.getKey();
      List<SectionProfilToBrancheParametersChange> changes = entry.getValue();
      ValidationMessage<ListRelationSectionContent> doModificationBySousModele = doModificationBySousModele(sousModeleName, changes, emhByNom);
      if (doModificationBySousModele.log.isNotEmpty()) {
        final List<ValidationMessage.ResultItem<ListRelationSectionContent>> messages = doModificationBySousModele.messages;
        for (ValidationMessage.ResultItem<ListRelationSectionContent> resultItem : messages) {
          if (resultItem.messages != null && !resultItem.messages.isEmpty()) {
            String nom = resultItem.bean.getNom();
            CtuluLog log = new CtuluLog();
            log.setDesc(NbBundle.getMessage(SectionProfilToBrancheProcessor.class, "traceProfil.SectionNotChanged", nom));
            for (String errorMessage : resultItem.messages) {
              log.addError(errorMessage);
            }
            res.addLog(log);
          }
        }
      }
    }


    helper.getTraceController().reset();
    helper.getTraceController().changeDone();
    helper.getTraceController().rebuildGis();
    TraceProfilAnglesProcessor traceProfilAnglesProcessor = new TraceProfilAnglesProcessor(helper);
    applyAnglesChange(emhByNom, traceProfilAnglesProcessor);
    propagateModification();
    return res;
  }

  public void propagateModification() {
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        helper.getTraceController().reset();
        helper.getTraceController().changeDone();
        helper.getTraceController().rebuildGis();
        helper.getTraceController().getLayer().repaint(0);
      }
    });
  }

  private void applyAnglesChange(
          Map<String, EMH> emhByNom, TraceProfilAnglesProcessor traceProfilAnglesProcessor) {
    for (SectionProfilToBrancheParametersChange change : acceptedModification) {
      CatEMHSection section = (CatEMHSection) emhByNom.get(change.getSectionName());
      if (section != null) {
        traceProfilAnglesProcessor.propagateAngleChange(change.getIdxTraceInGeom(), section, change.getTraceGisModel());
      }

    }
  }

  private ValidationMessage<ListRelationSectionContent> doModificationBySousModele(String sousModeleName,
          List<SectionProfilToBrancheParametersChange> changes,
          Map<String, EMH> emhByNom) {
    ListRelationSectionContentFactory factory = new ListRelationSectionContentFactory();
    EMHSousModele sousModele = (EMHSousModele) emhByNom.get(sousModeleName);
    final CrueConfigMetier ccm = helper.getController().
            getCrueConfigMetier();
    List<ListRelationSectionContent> sectionContents = factory.createNodes(sousModele, new TLongDoubleHashMap(), ccm);
    Map<String, SectionProfilToBrancheParametersChange> toMapOfNom = TransformerHelper.toMapOfNom(changes);
    for (ListRelationSectionContent sectionContent : sectionContents) {
      SectionProfilToBrancheParametersChange change = toMapOfNom.get(sectionContent.getNom());
      if (change != null) {
        sectionContent.setBrancheNom(change.getNewBrancheName());
        sectionContent.setAbscisseHydraulique(change.getNewXp());
      }
    }
    EditionChangeRelationSection changeProcess = new EditionChangeRelationSection();
    return changeProcess.apply(sectionContents, sousModele, ccm);
  }
}
