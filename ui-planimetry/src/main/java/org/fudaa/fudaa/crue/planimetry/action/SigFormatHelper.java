package org.fudaa.fudaa.crue.planimetry.action;

import java.util.EnumMap;
import java.util.Map;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderGIS;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderI;

/**
 *
 * @author deniger
 */
public class SigFormatHelper {

  public enum EnumFormat {
    SHP, GML
  }

  public static EnumFormat isKnown(final String in) {
    try {
      return EnumFormat.valueOf(in);
    } catch (final Exception e) {
    }
    return null;
  }

  public static String getExtension(final EnumFormat in) {
    switch (in) {
      case SHP:
        return ".shp";
      case GML:
        return ".gml";
      default:
        return null;
    }
  }

  public static FSigFileLoaderI getLoader(final EnumFormat in) {
    switch (in) {
      case SHP:
        return new FSigFileLoaderGIS.Shape();
      case GML:
        return new FSigFileLoaderGIS.GML();
      default:
        return null;
    }

  }

  public static Map<EnumFormat, FSigFileLoaderI> getLoader() {
    final Map<EnumFormat, FSigFileLoaderI> res = new EnumMap<>(
            SigFormatHelper.EnumFormat.class);
    final EnumFormat[] values = EnumFormat.values();
    for (final EnumFormat enumFormat : values) {
      res.put(enumFormat, getLoader(enumFormat));
    }
    return res;
  }
}
