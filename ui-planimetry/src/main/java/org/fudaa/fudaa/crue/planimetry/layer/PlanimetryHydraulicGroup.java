/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.layer;

import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.find.EbliFindableItem;
import org.fudaa.fudaa.crue.planimetry.find.PlanimetryFindEMHGroupAction;

/**
 *
 * @author Frederic Deniger
 */
public class PlanimetryHydraulicGroup extends BGroupeCalque implements EbliFindableItem {

  @Override
  public EbliFindActionInterface getFinder() {
    return new PlanimetryFindEMHGroupAction(this);
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return null;
  }

  @Override
  public boolean isSelectionEmpty() {
    BCalque[] calques = getCalques();
    for (BCalque bCalque : calques) {
      if (!((ZCalqueAffichageDonnees) bCalque).isSelectionEmpty()) {
        return false;
      }
    }
    return true;

  }

  @Override
  public void setUserVisible(boolean _b) {
    super.setUserVisible(_b);

    final BCalque[] c = getCalques();
    for (int i = 0; i < c.length; i++) {
      c[i].setUserVisible(_b);
    }
  }
}
