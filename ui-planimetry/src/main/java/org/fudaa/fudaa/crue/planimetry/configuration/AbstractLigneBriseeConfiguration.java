package org.fudaa.fudaa.crue.planimetry.configuration;

import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLigneBriseeLayerModel;

/**
 *
 * @author deniger
 */
public abstract class AbstractLigneBriseeConfiguration extends DefaultConfiguration {

  public AbstractLigneBriseeConfiguration(){

  }
  public AbstractLigneBriseeConfiguration(AbstractLigneBriseeConfiguration from){
    super(from);
  }


  /**
   * Appele par le calque pour intialiser le trace de la ligne
   * @param _ligne le modele à modifier
   * @param _idxPoly l'indice dans le model de calque.
   */
  public abstract void initTraceLigne(final TraceLigneModel _ligne, final int _idxPoly, PlanimetryLigneBriseeLayerModel model);

  public abstract void initTraceAtomicIcon(final TraceIconModel _ligne, final int _idxPoly, PlanimetryLigneBriseeLayerModel model);
}
