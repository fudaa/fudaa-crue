package org.fudaa.fudaa.crue.planimetry.layer;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import javax.swing.SwingConstants;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GisAbscCurviligneToCoordinate;
import org.fudaa.dodico.crue.config.comparator.XtComparator;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeo;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilEtiquette;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.LitNumerote;
import org.fudaa.dodico.crue.metier.emh.PtProfil;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.fudaa.crue.planimetry.action.LayerWithCascadeDeleteAction;
import org.fudaa.fudaa.crue.planimetry.configuration.TraceConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.controller.LayerTraceController;
import org.fudaa.fudaa.crue.planimetry.controller.LayerTraceGisBuilder;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.fudaa.fudaa.crue.planimetry.find.PlanimetryFindEMHLayerAction;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 *
 * @author deniger
 */
public class PlanimetryTraceLayer extends PlanimetryLigneBriseeLayer<TraceConfiguration> implements LayerWithCascadeDeleteAction,
        PlanimetryLayerWithEMHContrat {

  public PlanimetryTraceLayer(PlanimetryTraceLayerModel _modele, FSigEditor _editor) {
    super(_modele, _editor);
  }

  @Override
  public void removeCascadeSelectedObjects() {
    if (!isSelectionEmpty()) {
      final boolean r = ((PlanimetryTraceLayerModel) modele_).removeLigneBriseeCascade(getSelectedIndex(), null);
      if (r) {
        clearSelection();
      }
    }
  }

  @Override
  public PlanimetryTraceLayerModel modeleDonnees() {
    return (PlanimetryTraceLayerModel) super.modeleDonnees();
  }

  @Override
  protected boolean clearSelectionIfLayerNonVisible() {
    return false;
  }

  private boolean userVisible = true;

  @Override
  public boolean isUserVisible() {
    return userVisible;
  }

  @Override
  public void setUserVisible(boolean userVisible) {
    if (this.userVisible != userVisible) {
      super.setUserVisible(userVisible);
      this.userVisible = userVisible;
      clearCacheAndRepaint();
    }
  }

  @Override
  public boolean isVisible() {
    return !isSelectionEmpty() || isUserVisible();
  }

  @Override
  public String editSelected() {
    getActions()[0].actionPerformed(null);
    return null;
  }

  @Override
  public EbliFindActionInterface getFinder() {
    return new PlanimetryFindEMHLayerAction(this);
  }

  @Override
  public LayerControllerEMH getLayerControllerEMH() {
    return getLayerController();
  }

  public VisuConfiguration getVisuConfiguration() {
    return super.layerConfiguration.getParent();
  }
  final LabelPainter labelPainter = new LabelPainter();

  @Override
  public boolean canBeUsedForAccroche() {
    return false;
  }

  @Override
  protected boolean isPainted(int idx, GrMorphisme _versEcran) {

    if (!isUserVisible()||!modeleDonnees().isGeometryVisible(idx)) {
      return false;
    }
    if (isSelected(idx)) {
      return true;
    }
    if (!getVisuConfiguration().isInactiveEMHVisible()) {
      CatEMHSection section = getLayerController().getEMHFromPositionInModel(idx);
      return section != null && section.getActuallyActive();
    }
    return true;
  }

  @Override
  public boolean isEditable() {
    return getLayerController().isEditable();
  }

  @Override
  public boolean canAddForme(int _typeForme) {
    return false;
  }

  @Override
  public LayerTraceController getLayerController() {
    return (LayerTraceController) super.getLayerController();
  }

  @Override
  public boolean canUseAtomicMode() {
    return false;
  }

  @Override
  public void paintDonnees(Graphics2D _g, GrMorphisme _versEcran, GrMorphisme _versReel, GrBoite _clipReel) {
    if (!isVisible()) {
      return;
    }
    if (!getLayerController().isBuild()) {
      getLayerController().rebuildGis();
    }
    super.paintDonnees(_g, _versEcran, _versReel, _clipReel);

    GrPoint pt = new GrPoint();
    int nb = modeleDonnees().getNombre();
    GisAbscCurviligneToCoordinate.Result tmpResult = new GisAbscCurviligneToCoordinate.Result();
    XtComparator xtComparator = new XtComparator(getLayerController().getHelper().getController().getCrueConfigMetier());
    TraceIcon iconPtProfil = new TraceIcon();
    TraceIcon iconEtiquette = new TraceIcon();
    for (int i = 0; i < nb; i++) {
      if (!isPainted(i, _versEcran)) {
        continue;
      }
      CatEMHSection section = getLayerController().getSection(i);
      List<DonPrtGeo> dptg = section.getDPTG();
      if (CollectionUtils.isEmpty(dptg)) {
        continue;
      }
      DonPrtGeoProfilSection profilSection = EMHHelper.selectFirstOfClass(dptg,
              DonPrtGeoProfilSection.class);
      if (profilSection == null) {
        continue;
      }
      GISPolyligne traceGIS = (GISPolyligne) modele_.getGeomData().getGeometry(i);
      double xmin = LayerTraceGisBuilder.findXminLit(profilSection.getLitNumerote(), false);
      double xmax = LayerTraceGisBuilder.findXmaxLit(profilSection.getLitNumerote(), false);
      double length = xmax - xmin;

      super.layerConfiguration.initTraceIconLimiteLitOrEtiquette(iconEtiquette.getModel(), i);
      //les limites de lits
      TreeSet<Double> done = new TreeSet<>(xtComparator);
      final boolean iconEtiquettePainted = iconEtiquette.getTaille() > 0 && iconEtiquette.getType() != TraceIcon.RIEN;
      if (iconEtiquettePainted) {
        List<LitNumerote> litNumerotes = profilSection.getLitNumerote();
        List<DonPrtGeoProfilEtiquette> etiquettes = profilSection.getEtiquettes();

        if (iconEtiquettePainted) {
          for (LitNumerote litNumerote : litNumerotes) {
            double x = litNumerote.getLimDeb().getXt();
            done.add(x);
            GisAbscCurviligneToCoordinate.find(traceGIS, (x - xmin) / length, tmpResult);
            if (tmpResult.isFound()) {
              pt.initialiseAvec(tmpResult.getCoordinate());
              if (_clipReel.contientXY(pt)) {
                pt.autoApplique(_versEcran);
                iconEtiquette.paintIconCentre(_g, pt.x_, pt.y_);
              }
            }
            x = litNumerote.getLimFin().getXt();
            done.add(x);
            GisAbscCurviligneToCoordinate.find(traceGIS, (x - xmin) / length, tmpResult);
            if (tmpResult.isFound()) {
              pt.initialiseAvec(tmpResult.getCoordinate());
              if (_clipReel.contientXY(pt)) {
                pt.autoApplique(_versEcran);
                iconEtiquette.paintIconCentre(_g, pt.x_, pt.y_);
              }
            }

          }
          for (DonPrtGeoProfilEtiquette etiquette : etiquettes) {
            double x = etiquette.getPoint().getXt();
            if (!done.contains(x)) {
              done.add(x);
              GisAbscCurviligneToCoordinate.find(traceGIS, (x - xmin) / length, tmpResult);
              if (tmpResult.isFound()) {
                pt.initialiseAvec(tmpResult.getCoordinate());
                if (_clipReel.contientXY(pt)) {
                  pt.autoApplique(_versEcran);
                  iconEtiquette.paintIconCentre(_g, pt.x_, pt.y_);
                }
              }
            }
          }
        }
      }
      super.layerConfiguration.initTraceIconPtProfil(iconPtProfil.getModel(), i);
      final boolean iconPtProfilPainted = iconPtProfil.getTaille() > 0 && iconPtProfil.getType() != TraceIcon.RIEN;
      if (iconPtProfilPainted) {
        List<PtProfil> ptProfils = profilSection.getPtProfil();
        for (PtProfil ptProfil : ptProfils) {
          double x = ptProfil.getXt();
          if (!done.contains(x)) {
            done.add(x);
            GisAbscCurviligneToCoordinate.find(traceGIS, (x - xmin) / length, tmpResult);
            if (tmpResult.isFound()) {
              pt.initialiseAvec(tmpResult.getCoordinate());
              if (_clipReel.contientXY(pt)) {
                pt.autoApplique(_versEcran);
                iconPtProfil.paintIconCentre(_g, pt.x_, pt.y_);
              }
            }
          }
        }
      }
      if (layerConfiguration.isLitLabelsPainted()) {
        List<LitNumerote> litNumerotes = profilSection.getLitNumerote();
        Map<Double, List<String>> nameByXt = new TreeMap<>(xtComparator);
        for (LitNumerote litNumerote : litNumerotes) {
          if (litNumerote.getNomLit() == null || litNumerote.getNomLit().getNom() == null) {
            continue;
          }
          Double xMiddle = (litNumerote.getLimDeb().getXt() + litNumerote.getLimFin().getXt()) / 2;
          List<String> names = nameByXt.get(xMiddle);
          if (names == null) {
            names = new ArrayList<>();
            nameByXt.put(xMiddle, names);
          }
          names.add(litNumerote.getNomLit().getNom());
        }
        for (Map.Entry<Double, List<String>> entry : nameByXt.entrySet()) {
          Double xLabel = entry.getKey();
          GisAbscCurviligneToCoordinate.find(traceGIS, (xLabel - xmin) / length, tmpResult);
          if (tmpResult.isFound()) {
            List<String> list = entry.getValue();
            String nom = null;
            if (list.size() == 1) {
              nom = list.get(0);
            } else {
              Collections.sort(list);
              nom = StringUtils.join(list, '\n');
            }
            pt.initialiseAvec(tmpResult.getCoordinate());
            if (_clipReel.contientXY(pt)) {
              pt.autoApplique(_versEcran);
              labelPainter.paintLabels(_g, pt, nom, layerConfiguration.getLitLabelConfiguration(), alpha_);
            }
          }

        }
      }
      if (layerConfiguration.isSectionLabelPainted()) {
        int position = layerConfiguration.getSectionLabelPosition();
        if (SwingConstants.LEFT == position) {
          int nbPt = traceGIS.getNumPoints();
          pt.x_ = traceGIS.getCoordinateN(nbPt - 1).x;
          pt.y_ = traceGIS.getCoordinateN(nbPt - 1).y;
        } else if (SwingConstants.RIGHT == position) {
          pt.x_ = traceGIS.getCoordinateN(0).x;
          pt.y_ = traceGIS.getCoordinateN(0).y;
        }
        if (_clipReel.contientXY(pt)) {
          pt.autoApplique(_versEcran);
          labelPainter.paintLabels(_g, pt, layerConfiguration.getDisplayName(profilSection, modeleDonnees(), isSelected(i)), layerConfiguration.
                  getSectionLabelConfiguration(), alpha_);
        }
      }

    }
  }
}
