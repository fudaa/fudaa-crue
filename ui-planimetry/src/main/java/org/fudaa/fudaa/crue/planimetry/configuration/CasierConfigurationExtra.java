/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.configuration;

import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryCasierLayerModel;

import java.awt.*;

/**
 * @author Frederic Deniger
 */
public interface CasierConfigurationExtra {
    void decoreTraceLigne(TraceLigneModel ligne, CatEMHCasier casier, PlanimetryCasierLayerModel casierModel, CasierConfiguration aThis);

    void decoreTraceIcon(TraceIconModel modelIcon, CatEMHCasier casier, PlanimetryCasierLayerModel casierModel, CasierConfiguration aThis);

    /**
     * @param casier              casier
     * @param casierModel         le modele de calque
     * @param selected            true si selected
     * @param casierConfiguration la configuration
     * @return si null, le nom par defaut est utilise.
     */
    String getDisplayName(CatEMHCasier casier, PlanimetryCasierLayerModel casierModel, boolean selected, CasierConfiguration casierConfiguration);

    Color getFond(CatEMHCasier casier, PlanimetryCasierLayerModel casierModel);
}
