package org.fudaa.fudaa.crue.planimetry.listener;

import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.fudaa.crue.planimetry.controller.AbstractLayerSectionController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;

/**
 * Utilise pour reconstruire les sectionla branche est modifiée.
 * @author deniger
 */
public class SectionUpdaterFromBranche extends AbstractGeomUpdater {

  final AbstractLayerSectionController sectionController;

  public SectionUpdaterFromBranche(AbstractLayerSectionController controller) {
    this.sectionController = controller;
  }

  @Override
  public void attributeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _action) {
  }

  @Override
  public void attributeValueChangeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _indexObj,
                                         Object _newValue) {
  }

  @Override
  protected PlanimetryController getController() {
    return sectionController.getHelper().getController();
  }
  
  @Override
  protected void updateAfterAdd() {
    sectionController.rebuildGis();
  }

  @Override
  protected void updateAfterRemove() {
    sectionController.rebuildGis();
  }

  @Override
  protected void updateAll() {
    sectionController.rebuildGis();
  }

  @Override
  protected void update(int indexOfBranches) {
    sectionController.rebuildGis();
  }
}
