/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogGroupResult;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;

import java.util.List;

/**
 * Filtre les résultats et recherches les intersections pour les sections passées en paramètres.
 *
 * @author Frederic Deniger
 */
public class SectionProfilToBrancheProcessorPrepareInputs {
    private final SectionProfilToBrancheProcessorFilter filterProcessor;
    private final SectionProfilToBrancheProcessorIntersection intersectionProcessor;
    private final SectionProfilToBrancheProcessorValidator validatorProcessor;

    public SectionProfilToBrancheProcessorPrepareInputs(EMHScenario scenario, CrueConfigMetier ccm) {
        filterProcessor = new SectionProfilToBrancheProcessorFilter(scenario);
        validatorProcessor = new SectionProfilToBrancheProcessorValidator(scenario, ccm);
        intersectionProcessor = new SectionProfilToBrancheProcessorIntersection();
    }

    /**
     * Filtre les résultats et recherches les intersections pour les sections passées en paramètres. Finalement valide les résultats avec
     * SectionProfilToBrancheProcessorValidator: une seule intersection par trace et la section et la branche doivent appartenir au meme sous-modele.
     *
     * @param branchesParameters les parametres de branches
     * @param sectionParameters  les parametres de secions
     * @return le resultats des recherches d'intersection
     */
    public CtuluLogGroupResult<List<SectionProfilToBrancheParametersChange>, Object> process(SectionProfilToBrancheParametersBranche branchesParameters,
                                                                                             List<SectionProfilToBrancheParametersSection> sectionParameters) {
        CtuluLogResult<SectionProfilToBrancheParametersBranche> filterBranche = filterProcessor.filter(branchesParameters);
        CtuluLogResult<List<SectionProfilToBrancheParametersSection>> filterSection = filterProcessor.filterSection(sectionParameters);
        CtuluLogGroup logs = new CtuluLogGroup(null);
        if (filterBranche.getLog().isNotEmpty()) {
            logs.addLog(filterBranche.getLog());
        }
        if (filterSection.getLog().isNotEmpty()) {
            logs.addLog(filterSection.getLog());
        }
        SectionProfilToBrancheParametersComputed processed = intersectionProcessor.process(filterBranche.getResultat(), filterSection.getResultat());
        CtuluLogResult<List<SectionProfilToBrancheParametersChange>> validate = validatorProcessor.validate(processed);
        if (validate.getLog().isNotEmpty()) {
            logs.addLog(validate.getLog());
        }
        return new CtuluLogGroupResult<>(validate.getResultat(), logs);
    }
}
