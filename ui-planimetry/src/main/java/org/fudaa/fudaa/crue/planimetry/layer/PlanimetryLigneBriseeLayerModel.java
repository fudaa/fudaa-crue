package org.fudaa.fudaa.crue.planimetry.layer;

import org.locationtech.jts.geom.Geometry;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;

/**
 * Un modele que tous les modeles de calques éditables doivent utilsées. Permet de gérer les modifications, en faisant une copie des données avant
 * toutes modifications via l'UI
 *
 * @author deniger
 */
public class PlanimetryLigneBriseeLayerModel<C extends LayerController> extends ZModeleLigneBriseeEditable {

  private C layerController;

  public PlanimetryLigneBriseeLayerModel(GISZoneCollectionLigneBrisee _zone, C modelController) {
    super(_zone);
    this.layerController = modelController;
  }

  public C getLayerController() {
    return layerController;
  }

  @Override
  public void modificationWillBeDone() {
    getLayerController().changeWillBeDone();
  }

  public PlanimetryLigneBriseeLayerModel() {
  }

  @Override
  public void addPoint(int _ligneIdx, int _idxBefore, double _x, double _y, Double _z, CtuluCommandContainer _cmd) {
    if (!getGeomData().isGeomModifiable()) {
      return;
    }
    layerController.changeWillBeDone();
    super.addPoint(_ligneIdx, _idxBefore, _x, _y, _z, null);
    layerController.changeDone();
  }

  @Override
  protected boolean removeAtomicObjects(EbliListeSelectionMultiInterface _selection, CtuluCommandContainer _cmd, CtuluUI _ui) {
    layerController.changeWillBeDone();
    final boolean done = super.removeAtomicObjects(_selection, null, _ui);
    layerController.changeDone();
    return done;
  }

  @Override
  public boolean addGeometry(Geometry _p, CtuluCommandContainer _cmd, CtuluUI _ui, ZEditionAttributesDataI _d) {
    layerController.changeWillBeDone();
    final boolean done = super.addGeometry(_p, null, _ui, _d);
    layerController.changeDone();
    return done;
  }

  @Override
  public boolean addGeometry(GrPolygone _p, CtuluCommandContainer _cmd, CtuluUI _ui, ZEditionAttributesDataI _d) {
    layerController.changeWillBeDone();
    final boolean done = super.addGeometry(_p, null, _ui, _d);
    layerController.changeDone();
    return done;
  }

  @Override
  public boolean addGeometry(GrPolyligne _p, CtuluCommandContainer _cmd, CtuluUI _ui, ZEditionAttributesDataI _d) {
    layerController.changeWillBeDone();
    final boolean done = super.addGeometry(_p, null, _ui, _d);
    layerController.changeDone();
    return done;
  }

  @Override
  public boolean closeGeometry(int _ligneIdx, CtuluCommandContainer _cmd) {
    layerController.changeWillBeDone();
    final boolean done = super.closeGeometry(_ligneIdx, null);
    layerController.changeDone();
    return done;
  }

  @Override
  public void invertGeometry(int _idxGeom, CtuluCommandContainer _cmd) {
    layerController.changeWillBeDone();
    super.invertGeometry(_idxGeom, null);
    layerController.changeDone();
  }

  @Override
  public void modifyProperty(String _key, Object _value, int[] _index, CtuluCommandContainer _cmd) {
    layerController.changeWillBeDone();
    super.modifyProperty(_key, _value, _index, null);
    layerController.changeDone();
  }

  @Override
  public boolean moveAtomic(EbliListeSelectionMultiInterface _selection, double _dx, double _dy, double _dz,
          CtuluCommandContainer _cmd, CtuluUI _ui) {
    layerController.changeWillBeDone();
    final boolean done = super.moveAtomic(_selection, _dx, _dy, _dz, null, _ui);
    layerController.changeDone();
    return done;
  }

  @Override
  public boolean moveGlobal(CtuluListSelectionInterface _selection, double _dx, double _dy, double _dz, CtuluCommandContainer _cmd) {
    layerController.changeWillBeDone();
    boolean res = super.moveGlobal(_selection, _dx, _dy, _dz, null);
    layerController.changeDone();
    return res;
  }

  @Override
  public boolean removeLigneBrisee(int[] _idx, CtuluCommandContainer _cmd) {
    layerController.changeWillBeDone();
    final boolean done = super.removeLigneBrisee(_idx, null);
    layerController.changeDone();
    return done;
  }

  @Override
  public boolean rotateAtomic(EbliListeSelectionMultiInterface _selection, double _angRad, double _xreel0, double _yreel0,
          CtuluCommandContainer _cmd, CtuluUI _ui) {
    layerController.changeWillBeDone();
    final boolean done = super.rotateAtomic(_selection, _angRad, _xreel0, _yreel0, null, _ui);
    layerController.changeDone();
    return done;
  }

  @Override
  public boolean rotateGlobal(CtuluListSelectionInterface _selection, double _angRad, double _xreel0, double _yreel0,
          CtuluCommandContainer _cmd) {
    layerController.changeWillBeDone();
    final boolean done = super.rotateGlobal(_selection, _angRad, _xreel0, _yreel0, null);
    layerController.changeDone();
    return done;
  }

  public PlanimetryLigneBriseeLayerModel cloneModel(C modelController) {
    final GISZoneCollectionLigneBrisee name = (GISZoneCollectionLigneBrisee) super.getGeomData().clone();
    final PlanimetryLigneBriseeLayerModel res = new PlanimetryLigneBriseeLayerModel(name, modelController);
    return res;
  }
}
