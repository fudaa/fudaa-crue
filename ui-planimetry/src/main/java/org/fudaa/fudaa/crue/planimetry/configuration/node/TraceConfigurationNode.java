package org.fudaa.fudaa.crue.planimetry.configuration.node;

import java.util.List;
import org.fudaa.fudaa.crue.planimetry.configuration.TraceConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.TraceConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerBuilder;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class TraceConfigurationNode extends AbstractNode {

  public TraceConfigurationNode(TraceConfiguration traceConfiguration) {
    super(Children.LEAF, Lookups.singleton(traceConfiguration));
    setDisplayName(NbBundle.getMessage(PlanimetryControllerBuilder.class, PlanimetryController.NAME_CQ_TRACE));
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = new Sheet();
    TraceConfiguration lookup = getLookup().lookup(TraceConfiguration.class);
    List<Set> createSet = TraceConfigurationInfo.createSet(lookup);
    for (Set set : createSet) {
      sheet.put(set);
    }
    return sheet;
  }
}
