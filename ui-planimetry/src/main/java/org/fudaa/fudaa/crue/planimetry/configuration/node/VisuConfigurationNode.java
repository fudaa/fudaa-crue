package org.fudaa.fudaa.crue.planimetry.configuration.node;

import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfigurationInfo;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class VisuConfigurationNode extends AbstractNode {
  
  public VisuConfigurationNode(VisuConfiguration visuConfig) {
    super(Children.LEAF, Lookups.singleton(visuConfig));
    setDisplayName(NbBundle.getMessage(VisuConfiguration.class, "VisuConfigration.DisplayName"));
  }
  
  @Override
  protected Sheet createSheet() {
    Sheet sheet = new Sheet();
    VisuConfiguration lookup = getLookup().lookup(VisuConfiguration.class);
    sheet.put(VisuConfigurationInfo.createSet(lookup));
    return sheet;
  }
}
