package org.fudaa.fudaa.crue.planimetry.layer;

import gnu.trove.TIntArrayList;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.edition.EditionCreateEMH;
import org.fudaa.dodico.crue.edition.EditionDelete;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.crue.planimetry.configuration.NodeConfigurationExtra;
import org.fudaa.fudaa.crue.planimetry.controller.LayerNodeController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryGisModelContainer;

/**
 *
 * @author deniger
 */
public class PlanimetryNodeLayerModel extends PlanimetryPointLayerModel<LayerNodeController> {

  private NodeConfigurationExtra nodeConfigurationExtra;

  public PlanimetryNodeLayerModel(GISZoneCollectionPoint _zone, LayerNodeController layerModelController) {
    super(_zone, layerModelController);
  }

  public void setNodeConfigurationExtra(NodeConfigurationExtra nodeConfigurationExtra) {
    this.nodeConfigurationExtra = nodeConfigurationExtra;
  }

  @Override
  public boolean removePoint(int[] _idx, CtuluCommandContainer _cmd) {
    return removeNoeuds(_idx, false);
  }

  public boolean removePointCascade(int[] _idx, CtuluCommandContainer _cmd) {
    return removeNoeuds(_idx, true);
  }

  public boolean removeNoeuds(int[] _idx, final boolean cascade) {
    PlanimetryControllerHelper helper = super.getLayerController().getHelper();
    if (!helper.canEMHBeAddedAndDisplayError()) {
      return false;
    }
    helper.scenarioEditionStarted();
    List<EMH> noeuds = new ArrayList<>();
    for (int i = 0; i < _idx.length; i++) {
      noeuds.add(getLayerController().getEMHFromPositionInModel(_idx[i]));
    }
    final EMHScenario scenario = helper.getPlanimetryGisModelContainer().getScenario();
    //a utiliser
    EditionDelete.RemoveBilan deleted = EditionDelete.delete(noeuds, scenario, cascade);
    helper.displayBilan(deleted);
    return updateAfterRemove();
  }

  public boolean updateAfterRemove() {
    PlanimetryControllerHelper helper = super.getLayerController().getHelper();
    final EMHScenario scenario = helper.getPlanimetryGisModelContainer().getScenario();
    TIntArrayList toRemove = new TIntArrayList();
    GISAttributeModel uidModel = helper.getPlanimetryGisModelContainer().getUidModel(geometries_);
    int nb = uidModel.getSize();
    for (int i = 0; i < nb; i++) {
      Long uid = (Long) uidModel.getObjectValueAt(i);
      if (!scenario.getIdRegistry().containEMH(uid)) {
        toRemove.add(i);
      }
    }
    if (!toRemove.isEmpty()) {
      return super.removePoint(toRemove.toNativeArray(), null);
    }
    return false;
  }

  @Override
  public boolean isGeometryVisible(int idx) {
    CatEMHNoeud noeud = getLayerController().getEMHFromPositionInModel(idx);
    PlanimetryControllerHelper helper = getLayerController().getHelper();
    return helper.isVisible(noeud);
  }

  @Override
  public void addPoint(GrPoint _p, ZEditionAttributesDataI _data, CtuluCommandContainer _cmd) {
    PlanimetryControllerHelper helper = super.getLayerController().getHelper();
    if (!helper.canEMHBeAddedAndDisplayError()) {
      return;
    }
    getLayerController().changeWillBeDone();
    helper.scenarioEditionStarted();
    final PlanimetryGisModelContainer planimetryGisAttributeContext = helper.getPlanimetryGisModelContainer();
    EditionCreateEMH creationHelper = new EditionCreateEMH(getLayerController().getDefaultValues());

    CatEMHNoeud nd = creationHelper.createNoeud(helper.getDefaultSousModele(), EnumNoeudType.EMHNoeudNiveauContinu, helper.getController().getCrueConfigMetier());
    GISPoint gisPt = new GISPoint(_p.x_, _p.y_, 0);
    addNoeud(planimetryGisAttributeContext, nd, gisPt);
    getLayerController().changeDone();
  }

  public void addNoeud(CatEMHNoeud nd, GISPoint gisPt) {
    getLayerController().changeWillBeDone();
    PlanimetryControllerHelper helper = super.getLayerController().getHelper();
    helper.scenarioEditionStarted();
    addNoeud(helper.getPlanimetryGisModelContainer(), nd, gisPt);
    getLayerController().changeDone();
  }

  private void addNoeud(final PlanimetryGisModelContainer planimetryGisAttributeContext, CatEMHNoeud nd, GISPoint gisPt) {
    List data = planimetryGisAttributeContext.buildNodeData(nd);
    getGeometries().addGeometry(gisPt, data.toArray(), null);
  }

  public NodeConfigurationExtra getNodeConfigurationExtra() {
    return nodeConfigurationExtra;
  }
}
