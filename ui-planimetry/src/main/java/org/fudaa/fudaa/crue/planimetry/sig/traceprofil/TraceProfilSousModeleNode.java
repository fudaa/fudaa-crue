/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import java.util.List;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frederic Deniger
 */
public class TraceProfilSousModeleNode extends AbstractNodeFirable {

  public TraceProfilSousModeleNode(TraceProfilSousModele object, List<String> sousModeles) {
    super(Children.LEAF, Lookups.fixed(object, sousModeles));
    setName(object.getTraceProfilName());
    setDisplayName(object.getTraceProfilName());
  }

  @Override
  public boolean isEditMode() {
    return true;
  }

  @Override
  protected void fireObjectChange(String property, Object oldValue, Object newValue) {
    super.fireObjectChange(property, oldValue, newValue);
    super.fireObjectChange(TraceProfilSousModele.PROP_ACTION_BILAN, oldValue, newValue);

  }

  @Override
  protected Sheet createSheet() {
    TraceProfilSousModele tc = getLookup().lookup(TraceProfilSousModele.class);
    List<String> sousModele = getLookup().lookup(List.class);
    Sheet res = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    res.put(set);
    PropertyNodeBuilder builder = new PropertyNodeBuilder();
    List<PropertySupportReflection> createFromPropertyDesc = builder.createFromPropertyDesc(DecimalFormatEpsilonEnum.COMPARISON, tc, this);
    for (PropertySupportReflection prop : createFromPropertyDesc) {
      set.put(prop);
      if (TraceProfilSousModele.PROP_SOUS_MODELE.equals(prop.getName())) {
        if (tc.isSectionExists()) {
          prop.setCanWrite(false);
        } else {
          try {
            prop.setValue(sousModele.get(0));
            prop.setTags(sousModele);
          } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
          }
        }
      }

    }
    return res;
  }
}
