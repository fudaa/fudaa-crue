package org.fudaa.fudaa.crue.planimetry.save;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessource;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessourceInfos;
import org.fudaa.dodico.crue.io.conf.Option;
import org.fudaa.fudaa.crue.common.config.ConfigSaverHelper;
import org.fudaa.fudaa.crue.planimetry.configuration.PointConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.controller.ExternLayerFactory;
import org.fudaa.fudaa.crue.planimetry.controller.LayerPointExternController;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternDrawLayerGroup;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryPointExternModifiableLayer;
import org.fudaa.fudaa.crue.planimetry.sig.SigLoaderHelper;
import org.fudaa.fudaa.sig.FSigGeomSrcData;
import org.openide.nodes.Sheet.Set;

/**
 * Pour etre pris compte doit être ajoute dans la liste AdditionalLayerLoaderProcess
 *
 * @author deniger
 */
public class LayerPointModifiableControllerSaver implements AdditionalLayerLoaderProcess.Loader {

  public CrueEtudeExternRessource getSaveRessource(String id, LayerPointExternController layerController, File baseDir) {
    LayerControllerSaverHelper helper = new LayerControllerSaverHelper();
    CrueEtudeExternRessourceInfos ressource = new CrueEtudeExternRessourceInfos();
    final PlanimetryPointExternModifiableLayer layer = (PlanimetryPointExternModifiableLayer) layerController.getLayer();
    ressource.setNom(layer.getTitle());
    ressource.setType(ConfigExternIds.LAYER_POINT_MODIFIABLE_SAVE_ID);
    ressource.setId(id);
    ressource.setLayerId(layer.getName());
    helper.addVisibleProperty(ressource, layerController.getLayer());
    LinkedHashMap<String, String> transform = ConfigSaverHelper.getProperties(PointConfigurationInfo.createSet(
            layer.getLayerConfiguration()));
    for (Map.Entry<String, String> entry : transform.entrySet()) {
      ressource.getOptions().add(new Option(entry.getKey(), entry.getValue()));
    }
    return new CrueEtudeExternRessource(ressource, new GISAdditionalFileSaver(ressource, layer.modeleDonnees().getGeomData()));
  }

  @Override
  public String getType() {
    return ConfigExternIds.LAYER_POINT_MODIFIABLE_SAVE_ID;
  }

  @Override
  public PlanimetryAdditionalLayerContrat load(CrueEtudeExternRessourceInfos ressource, CtuluLog log, File baseDir) {
    FSigGeomSrcData result = new LayerControllerSaverModifiableHelper().loadCollection(ressource, log, baseDir);
    if (result == null) {
      return null;
    }
    GISZoneCollectionPoint buildPoints = SigLoaderHelper.buildPoints(result, PlanimetryExternDrawLayerGroup.getDefaultAttributesPointsModifiable());
    PlanimetryPointExternModifiableLayer layer = ExternLayerFactory.createLayerPointsModifiable(buildPoints,
            null);
    layer.setTitle(ressource.getNom());
    layer.setName(ressource.getLayerId());
    LayerControllerSaverHelper helper = new LayerControllerSaverHelper();
    Map<String, String> optionsBykey = helper.toMap(ressource);
    helper.addVisibleProperty(ressource, layer);
    List<Set> propertySet = PointConfigurationInfo.createSet(layer.getLayerConfiguration());
    LayerControllerSaverHelper.restoreVisuProperties(optionsBykey, layer, propertySet, log);
    return layer;
  }
}
