/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.view.DefaultOutlineViewEditor;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class TraceProfilToSectionPanel extends DefaultOutlineViewEditor {

  public TraceProfilToSectionPanel(final EMHScenario scenario, final List<TraceProfilSousModele> tcs, final String defaultSousModele) {
    final List<TraceProfilSousModeleNode> nodes = new ArrayList<>();
    final List<String> sousModelesName = TransformerHelper.toNom(scenario.getSousModeles());
    for (final TraceProfilSousModele traceProfilSousModele : tcs) {
      nodes.add(new TraceProfilSousModeleNode(traceProfilSousModele, sousModelesName));
    }
    final JPanel top = new JPanel(new BorderLayout());
    final JPanel changeSousModelPanel = createChangeSousModelePanel(sousModelesName, defaultSousModele);
    final JPanel selectAllPanel = createSelectAllPanel();

    top.add(changeSousModelPanel, BorderLayout.WEST);
    top.add(selectAllPanel, BorderLayout.EAST);
    add(top, BorderLayout.NORTH);
    final Node createNode = NodeHelper.createNode(nodes, "All");
    getExplorerManager().setRootContext(createNode);
  }

  public List<TraceProfilSousModele> getAcceptedModification() {
    final List<TraceProfilSousModele> res = new ArrayList<>();
    final Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    for (final Node node : nodes) {
      final TraceProfilSousModele tc = node.getLookup().lookup(TraceProfilSousModele.class);
      if (tc.getSousModeleName() != null && tc.isAddOrModifiy()) {
        res.add(tc);
      }
    }
    return res;
  }

  protected void changeAllSousModel(final String newOne) {
    final Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    for (final Node node : nodes) {
      final TraceProfilSousModele lookup = node.getLookup().lookup(TraceProfilSousModele.class);
      if (!lookup.isSectionExists()) {
        final AbstractNodeFirable myNode = (AbstractNodeFirable) node;
        try {
          myNode.find(TraceProfilSousModele.PROP_SOUS_MODELE).setValue(newOne);
        } catch (final Exception ex) {
          Logger.getLogger(TraceProfilToSectionPanel.class.getName()).log(Level.INFO, "message {0}", ex);
        }
      }
    }
  }

  protected void selectAll(final boolean newOne) {
    final Node[] nodes = getExplorerManager().getRootContext().getChildren().getNodes();
    for (final Node node : nodes) {
      try {
        final AbstractNodeFirable myNode = (AbstractNodeFirable) node;
        myNode.find(TraceProfilSousModele.PROP_ACCEPT_MODIFICATION).setValue(newOne);
      } catch (final Exception ex) {
        Logger.getLogger(TraceProfilToSectionPanel.class.getName()).log(Level.INFO, "message {0}", ex);
      }
    }
  }

  @Override
  protected void initActions() {
  }

  @Override
  protected void addElement() {
  }

  @Override
  protected void createOutlineView() {
    view = new OutlineView(NbBundle.getMessage(TraceProfilSousModele.class, TraceProfilSousModele.I18N_TRACE_PROFIL_NAME));
    view.getOutline().setRootVisible(false);
    view.setPropertyColumns(
            TraceProfilSousModele.PROP_SECTION_NAME, NbBundle.getMessage(TraceProfilSousModele.class, TraceProfilSousModele.I18N_SECTION_NAME),
            TraceProfilSousModele.PROP_SOUS_MODELE, NbBundle.getMessage(TraceProfilSousModele.class, TraceProfilSousModele.I18N_TARGET_SOUS_MODELE),
            TraceProfilSousModele.PROP_ACCEPT_MODIFICATION, NbBundle.getMessage(TraceProfilSousModele.class,
            TraceProfilSousModele.I18N_ACCEPT_MODIFICATION),
            TraceProfilSousModele.PROP_ACTION_BILAN, NbBundle.getMessage(TraceProfilSousModele.class, TraceProfilSousModele.I18N_ACTION_BILAN));
  }

  private JPanel createChangeSousModelePanel(final List<String> sousModelesName, final String defaultSelected) {
    final JComboBox cb = new JComboBox(sousModelesName.toArray());
    final JButton bt = new JButton(NbBundle.getMessage(TraceProfilToSectionPanel.class, "MassiveChangeSousModele.ActionName"));
    bt.setToolTipText(NbBundle.getMessage(TraceProfilToSectionPanel.class, "MassiveChangeSousModele.Tooltip"));
    final JPanel changeSousModel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    changeSousModel.add(cb);
    if (defaultSelected != null) {
      cb.setSelectedItem(defaultSelected);
    }
    changeSousModel.add(bt);
    bt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        changeAllSousModel((String) cb.getSelectedItem());
      }
    });
    return changeSousModel;
  }

  private JPanel createSelectAllPanel() {
    final JPanel pnActiveUnactive = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    final JButton btActive = new JButton(NbBundle.getMessage(TraceProfilToSectionPanel.class, "AcceptAll.Button"));
    btActive.setToolTipText(NbBundle.getMessage(TraceProfilToSectionPanel.class, "AcceptAll.Tooltip"));
    final JButton btUnActive = new JButton(NbBundle.getMessage(TraceProfilToSectionPanel.class, "RefuseAll.Button"));
    btUnActive.setToolTipText(NbBundle.getMessage(TraceProfilToSectionPanel.class, "RefuseAll.Tooltip"));
    btActive.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        selectAll(true);
      }
    });
    btUnActive.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        selectAll(false);
      }
    });

    pnActiveUnactive.add(btActive);
    pnActiveUnactive.add(btUnActive);
    return pnActiveUnactive;
  }
}
