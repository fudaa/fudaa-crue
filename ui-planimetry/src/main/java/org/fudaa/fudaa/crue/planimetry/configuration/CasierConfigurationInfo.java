package org.fudaa.fudaa.crue.planimetry.configuration;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.fudaa.crue.common.config.ConfigurationInfoHelper;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 ** Attention. Les name des set sont utilisés pour la persistence dans le xml. Donc, ne pas changer sans rejouer les tests unitaires et faire des
 * reprises
 *
 * @author deniger
 */
public class CasierConfigurationInfo {

  private CasierConfigurationInfo(){}

  static final String PREFIX_CASIERS = "casiers";

  public static List<Sheet.Set> createSet(CasierConfiguration in) {
    List<Sheet.Set> res = new ArrayList<>();
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setName(PREFIX_CASIERS);
    set.setDisplayName(NbBundle.getMessage(CasierConfigurationInfo.class, "CasierConfiguration.DisplayName"));
    try {
      set.put(VisuConfigurationInfo.createTransparency(in));
      set.put(ConfigurationInfoHelper.create(CasierConfiguration.PROPERTY_FOND_PAINTED, Boolean.TYPE, in,
              "CasierConfiguration.FondPainted", VisuConfigurationInfo.class));
      set.put(ConfigurationInfoHelper.create(CasierConfiguration.PROPERTY_FOND, Color.class, in,
              "CasierConfiguration.Fond", VisuConfigurationInfo.class));
      res.add(set);
      res.add(TraceLineModelConfigurationInfo.createSet(in.lineForCasier, PREFIX_CASIERS, true));
      final Set iconForAtomic = TraceIconModelConfigurationInfo.createSet(in.iconForAtomicIcon, PREFIX_CASIERS + ".atomic", true);
      res.add(iconForAtomic);
      iconForAtomic.setDisplayName(NbBundle.getMessage(CasierConfigurationInfo.class,
              "CasierConfiguration.AtomicPoint.DisplayName"));
      res.add(LabelConfigurationInfo.createSet(in.getLabelConfiguration(), PREFIX_CASIERS, true));
    } catch (Exception exception) {
      Exceptions.printStackTrace(exception);
    }
    return res;
  }
}
