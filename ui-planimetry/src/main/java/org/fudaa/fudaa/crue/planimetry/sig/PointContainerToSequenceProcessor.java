/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import java.util.List;
import org.fudaa.ctulu.gis.GISGeometryFactory;

/**
 * Transforme une liste de points en CoordinateSequence
 *
 * @author Frederic Deniger
 */
public class PointContainerToSequenceProcessor {

  public CoordinateSequence create(List<? extends PointContainer> container) {
    Coordinate[] res = new Coordinate[container.size()];
    for (int i = 0; i < container.size(); i++) {
      res[i] = new Coordinate(container.get(i).getX(), container.get(i).getY());
    }
    return GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(res);
  }
}
