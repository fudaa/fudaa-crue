/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.layer;

import java.util.List;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;
import org.fudaa.dodico.crue.metier.emh.EMH;

/**
 *
 * @author Frederic Deniger
 */
public interface LayerControllerEMH extends LayerController {

  EMH getEMHFromLayerPosition(int position);

  List<ToStringInternationalizable> getCategories();
}
