package org.fudaa.fudaa.crue.planimetry.save;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessource;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessourceInfos;
import org.fudaa.dodico.crue.io.conf.Option;
import org.fudaa.fudaa.crue.common.config.ConfigSaverHelper;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper;
import org.fudaa.fudaa.crue.planimetry.configuration.LigneBriseeConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.controller.AbstractGroupExternController;
import org.fudaa.fudaa.crue.planimetry.controller.LayerLigneBriseeExternController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.layer.AbstractPlanimetryLigneBriseeExternLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;
import org.fudaa.fudaa.sig.FSigGeomSrcData;
import org.openide.nodes.Sheet;

import java.io.File;
import java.util.*;
import java.util.logging.Logger;

/**
 * Pour etre pris compte doit être ajoute dans la liste AdditionalLayerLoaderProcess
 *
 * @author deniger
 */
public abstract class AbstractLayerTraceControllerSaver implements AdditionalLayerLoaderProcess.Loader {
  private final String subControllerId;
  private final String configExternId;
  private final String folderName;

  public AbstractLayerTraceControllerSaver(final String subControllerId,
                                           final String configExternId,
                                           final String folderName) {
    this.configExternId = configExternId;
    this.subControllerId = subControllerId;
    this.folderName = folderName;
  }

  private File getTraceDir(final File baseDir) {
    return new File(baseDir, folderName);
  }

  final File getTargetFile(final File baseDir, final String layerId) {
    return new File(getTraceDir(baseDir), layerId + SigFormatHelper.getExtension(SigFormatHelper.EnumFormat.GML));
  }

  /**
   * Nettoie le répertoire contenant les TraceProfils
   *
   * @param baseDir le repertoire de base
   * @param planimetryController le controller
   */
  public final void clearDirTraceProfil(final File baseDir, final PlanimetryController planimetryController) {
    final File traceProfilsDir = getTraceDir(baseDir);
    if (traceProfilsDir.isDirectory()) {
      final AbstractGroupExternController controller = planimetryController.getGroupExternController().
          getSubController(subControllerId);
      final Set<String> usedNames = new HashSet<>(controller.getUsedNames());
      final File[] listFiles = traceProfilsDir.listFiles();
      for (final File file : listFiles) {
        final String sansExtension = CtuluLibFile.getSansExtension(file.getName());
        if (!usedNames.contains(sansExtension)) {
          CrueFileHelper.deleteFile(file, getClass());
        }
      }
    } else {
      final boolean mkdirs = traceProfilsDir.mkdirs();
      if (!mkdirs) {
        Logger.getLogger(getClass().getName()).warning("Can't create directory " + traceProfilsDir.getAbsolutePath());
      }
    }
  }

  /**
   * @param id l'id
   * @param layerController le controller de calque
   * @param baseDir représente le répertoire CONFIG
   * @return la ressources externe
   */
  public final CrueEtudeExternRessource getSaveRessource(final String id, final LayerLigneBriseeExternController layerController, final File baseDir) {
    final LayerControllerSaverHelper helper = new LayerControllerSaverHelper();
    final CrueEtudeExternRessourceInfos ressource = new CrueEtudeExternRessourceInfos();
    final AbstractPlanimetryLigneBriseeExternLayer layer = layerController.getLayer();
    ressource.setNom(layer.getTitle());
    ressource.setType(configExternId);
    ressource.setId(id);
    ressource.setLayerId(layer.getName());
    helper.addVisibleProperty(ressource, layerController.getLayer());
    final LinkedHashMap<String, String> transform = ConfigSaverHelper.getProperties(LigneBriseeConfigurationInfo.createSet(
        layer.getLayerConfiguration()));
    for (final Map.Entry<String, String> entry : transform.entrySet()) {
      ressource.getOptions().add(new Option(entry.getKey(), entry.getValue()));
    }
    return createExternRessource(ressource, layer);
  }

  @Override
  public final String getType() {
    return configExternId;
  }

  @Override
  public final PlanimetryAdditionalLayerContrat load(final CrueEtudeExternRessourceInfos ressource, final CtuluLog log, final File baseDir) {
    final LayerControllerSaverHelper helper = new LayerControllerSaverHelper();
    final Map<String, String> optionsBykey = helper.toMap(ressource);
    final File file = getTargetFile(baseDir, ressource.getLayerId());
    final Pair<FSigGeomSrcData, SigFormatHelper.EnumFormat> pair = helper.loadGisData(file, SigFormatHelper.EnumFormat.GML, ressource.getNom(), log);
    if (pair == null) {
      return null;
    }
    final FSigGeomSrcData result = pair.first;
    if (result == null) {
      log.addError("externConfig.geo.NoDataFound.error", ressource.getNom(), file.getName());
    }
    final AbstractPlanimetryLigneBriseeExternLayer layer = createLayer(result, file, pair);
    layer.setTitle(ressource.getNom());
    layer.setName(ressource.getLayerId());
    helper.addVisibleProperty(ressource, layer);
    final List<Sheet.Set> propertySet = LigneBriseeConfigurationInfo.createSet(layer.getLayerConfiguration());
    LayerControllerSaverHelper.restoreVisuProperties(optionsBykey, layer, propertySet, log);
    return layer;
  }

  private CrueEtudeExternRessource createExternRessource(final CrueEtudeExternRessourceInfos ressource,
                                                         final AbstractPlanimetryLigneBriseeExternLayer layer) {
    return new CrueEtudeExternRessource(ressource, new GISTraceFileSaver(ressource, layer.getModelEditable().getGeomData(), this));
  }

  protected abstract AbstractPlanimetryLigneBriseeExternLayer createLayer(FSigGeomSrcData result, File file,
                                                                          Pair<FSigGeomSrcData, SigFormatHelper.EnumFormat> pair);
}
