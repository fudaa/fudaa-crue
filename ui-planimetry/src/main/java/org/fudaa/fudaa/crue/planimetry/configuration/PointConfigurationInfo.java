package org.fudaa.fudaa.crue.planimetry.configuration;

import java.util.ArrayList;
import java.util.List;
import org.openide.nodes.Sheet;
import org.openide.util.NbBundle;

/**
 ** Attention. Les name des set sont utilisés pour la persistence dans le xml. Donc, ne pas changer sans rejouer les tests unitaires et faire des
 * reprises
 *
 * @author deniger
 */
public class PointConfigurationInfo {

  public static final String PREFIX_POINTS = "points";

  public static List<Sheet.Set> createSet(PointConfiguration in) {
    List<Sheet.Set> res = new ArrayList<>();
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setName(PREFIX_POINTS);
    set.put(VisuConfigurationInfo.createTransparency(in));
    set.setDisplayName(NbBundle.getMessage(VisuConfigurationInfo.class, "PointConfiguration.DisplayName"));
    set.setShortDescription(set.getDisplayName());
    res.add(set);
    res.add(TraceIconModelConfigurationInfo.createSet(in.iconModel, PREFIX_POINTS));
    res.add(LabelConfigurationInfo.createSet(in.labelConfiguration, PREFIX_POINTS, true));

    return res;
  }
}
