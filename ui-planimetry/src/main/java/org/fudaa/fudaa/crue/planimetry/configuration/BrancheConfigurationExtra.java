/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.configuration;

import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryBrancheModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLigneBriseeLayerModel;

import java.awt.*;

/**
 * @author Frederic Deniger
 */
public interface BrancheConfigurationExtra {
    /**
     * @param branche la branche
     * @param model le model
     * @param brancheConfiguration la configuration de branche
     * @return la couleur a utiliser. Si renvoie null, la couleur par defaut sera utilisée.
     */
    Color getColor(CatEMHBranche branche, PlanimetryBrancheModel model, BrancheConfiguration brancheConfiguration);

    /**
     * Decore la ligne utilisee pour le tracé de la branche. Appele apres que la ligne soit correctement initialisée par la config. Attention, la
     * couleur est ensuite appliqué (voir #getColor).
     *
     * @param ligne le tracé de ligne
     * @param model le modele de calque
     * @param brancheConfiguration la configuration de branche
     */
    void decoreTraceLigne(TraceLigneModel ligne, PlanimetryLigneBriseeLayerModel model, BrancheConfiguration brancheConfiguration);

    /**
     * Decore l'icone utilise pour le milieu de la branche. La couleur est appliquée ensuite.
     *
     * @param traceIcon le tracé d'icone
     * @param model  le modele de calque
     * @param brancheConfiguration la configuration de la branche
     */
    TraceIcon decoreMiddleIcon(CatEMHBranche branche, TraceIcon traceIcon, PlanimetryBrancheModel model, BrancheConfiguration brancheConfiguration,
                               GrSegment direction);

    /**
     * @param initTaille la taille initiale
     * @param model le modele de calque
     * @param brancheConfiguration  la configuration de la branche
     */
    int getMiddleIconWidth(int initTaille, PlanimetryBrancheModel model, BrancheConfiguration brancheConfiguration);

    /**
     * @param branche la branche
     * @param planimetryBrancheModel le model de calque
     * @param brancheConfiguration la configuration de brance
     * @param isSelected true si le casier est selectionne.
     * @return le nom à afficher. si renvoie null ,le nom par défaut est utilisé
     */
    String getDisplayName(CatEMHBranche branche, PlanimetryBrancheModel planimetryBrancheModel, BrancheConfiguration brancheConfiguration, boolean isSelected);
}
