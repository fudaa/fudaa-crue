package org.fudaa.fudaa.crue.planimetry.layer;

import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessource;
import org.fudaa.fudaa.crue.planimetry.configuration.DefaultConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.node.AdditionalConfigurationNode;
import org.fudaa.fudaa.sig.layer.FSigEditor;

import java.io.File;

/**
 * @author deniger
 */
public interface PlanimetryAdditionalLayerContrat<T extends DefaultConfiguration> extends PlanimetryLayerContrat {
    /**
     * nom de la propriété de l'evt envoye si la configuration du calque est modifié.
     */
    String PROPERTY_LAYER_CONFIGURATION = "layerConfiguration";

    AdditionalConfigurationNode createConfigurationNode();

    String getTypeId();

    T getLayerConfiguration();

    /**
     * appelé lorsque le calque est ajouté au groupe.
     */
    void installed();

    /**
     * @param baseDir utilise pour les path relatifs.
     * @param id      l'id de la ressources
     * @return la ressource
     */
    CrueEtudeExternRessource getSaveRessource(File baseDir, String id);

    PlanimetryAdditionalLayerContrat cloneLayer(FSigEditor editor);
}
