/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.tracecasier;

import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class TraceCasierSousModele implements Comparable<TraceCasierSousModele> {

  public static final String PROP_CASIER_NAME = "casierName";
  public static final String PROP_NOEUD_NAME = "noeudName";
  public static final String PROP_CONTAIN_PROFIL = "containProfil";
  public static final String PROP_SOUS_MODELE = "sousModeleName";
  public static final String PROP_ACCEPT_MODIFICATION = "addOrModifiy";
  public static final String PROP_ACTION_BILAN = "actionName";
  public static final String I18N_ACTION_BILAN = "actionBilan.property";
  public static final String I18N_CASIER_NAME = "casierName.property";
  public static final String I18N_CONTAIN_PROFIL = "containProfil.property";
  public static final String I18N_TARGET_SOUS_MODELE = "targetSousModele.property";
  public static final String I18N_ACCEPT_MODIFICATION = "acceptModification.property";
  /**
   * true si l'utilisateur accepte la modification
   */
  boolean addOrModifiy;
  /**
   * true si le casier cible existe déjà
   */
  boolean casierExists;
  /**
   * true si le casier cible existe déjà
   */
  boolean noeudExists;
  boolean noeudInFutureCasier;
  boolean containProfil;
  /**
   * le nom du casier
   */
  String casierName;
  String noeudName;
  /**
   * le sous-modele cible. Si la section existe déjà, ne peut être modifiée
   */
  String sousModeleName;

  @PropertyDesc(i18n = I18N_ACTION_BILAN, i18nBundleBaseName = "org/fudaa/fudaa/crue/planimetry/sig/tracecasier/Bundle.properties", readOnly = true)
  public String getActionName() {
    if (isNoeudOut()) {
      return NbBundle.getMessage(TraceCasierSousModele.class, "createCasier.noeudNotIncludedInCasier");
    }
    if (addOrModifiy) {
      if (!noeudExists) {
        return NbBundle.getMessage(TraceCasierSousModele.class, "createCasier.createNoeudAndCasier");
      } else if (!casierExists) {
        return NbBundle.getMessage(TraceCasierSousModele.class, "createCasier.createCasier");
      } else {
        return NbBundle.getMessage(TraceCasierSousModele.class, "createCasier.modifyCasier");
      }
    }
    return NbBundle.getMessage(TraceCasierSousModele.class, "createCasier.doNothing");
  }

  public void setContainProfil(boolean containProfil) {
    this.containProfil = containProfil;
  }

  public boolean cansSousModeleBeChanged() {
    return !casierExists && !noeudExists;
  }

  @Override
  public int compareTo(TraceCasierSousModele o) {
    if (o == null) {
      return 1;
    }
    return SafeComparator.compareString(casierName, o.casierName);
  }

  @PropertyDesc(i18n = I18N_CONTAIN_PROFIL, i18nBundleBaseName = "org/fudaa/fudaa/crue/planimetry/sig/tracecasier/Bundle.properties", readOnly = true)
  public boolean isContainProfil() {
    return containProfil;
  }

  @PropertyDesc(i18n = I18N_ACCEPT_MODIFICATION, i18nBundleBaseName = "org/fudaa/fudaa/crue/planimetry/sig/tracecasier/Bundle.properties")
  public boolean isAddOrModifiy() {
    return addOrModifiy;
  }

  public void setAddOrModifiy(boolean addOrModifiy) {
    if (isNoeudOut()) {
      return;
    }
    this.addOrModifiy = addOrModifiy;
  }

  public boolean isCasierExists() {
    return casierExists;
  }

  public boolean isNoeudExists() {
    return noeudExists;
  }

  public void setCasierExists(boolean exists) {
    this.casierExists = exists;
  }

  public void setNoeudExists(boolean exists) {
    this.noeudExists = exists;
  }

  @PropertyDesc(i18n = I18N_CASIER_NAME, i18nBundleBaseName = "org/fudaa/fudaa/crue/planimetry/sig/tracecasier/Bundle.properties",
          readOnly = true)
  public String getCasierName() {
    return casierName;
  }

  public String getNoeudName() {
    return noeudName;
  }

  @PropertyDesc(i18n = I18N_TARGET_SOUS_MODELE, i18nBundleBaseName = "org/fudaa/fudaa/crue/planimetry/sig/tracecasier/Bundle.properties")
  public String getSousModeleName() {
    return sousModeleName;
  }

  public void setSousModeleName(String sousModeleName) {
    this.sousModeleName = sousModeleName;
  }

  void setNoeudInFutureCasier(boolean inside) {
    this.noeudInFutureCasier = inside;
    if (!inside) {
      addOrModifiy = false;
    }
  }

  boolean isNoeudOut() {
    return noeudExists && !noeudInFutureCasier;
  }

  boolean canActionBeModified() {
    return !isNoeudOut();
  }
}
