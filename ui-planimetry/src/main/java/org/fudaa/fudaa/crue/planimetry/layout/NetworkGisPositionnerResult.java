package org.fudaa.fudaa.crue.planimetry.layout;

import org.fudaa.ctulu.gis.*;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import gnu.trove.TObjectIntHashMap;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.IdRegistry;
import org.fudaa.dodico.crue.projet.planimetry.LayerVisibility;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryGisModelContainer;

/**
 * @author deniger
 */
public class NetworkGisPositionnerResult {

    private boolean modifiedWhileLoading;

    final IdRegistry idRegistry;
    final PlanimetryGisModelContainer context;
    final GISZoneCollectionPoint nodes;
    final GISZoneCollectionLigneBrisee edges;
    final GISZoneCollectionLigneBrisee casiers;
    private final Map<String, Pair<Double, Double>> anglesBySectionNames = new HashMap<>();
    private VisuConfiguration visuConfiguration;
    private LayerVisibility layerVisibility;
    private CrueConfigMetier ccm;
    private final CtuluLog error = new CtuluLog();

    private Map<String, GISPoint> avoidDuplicateNodesSet;


    public CtuluLog getError() {
        return error;
    }

    public NetworkGisPositionnerResult(PlanimetryGisModelContainer container, GISZoneCollectionPoint nodes,
                                       GISZoneCollectionLigneBrisee edges, GISZoneCollectionLigneBrisee casiers) {
        this.context = container;
        this.idRegistry = container.getScenario().getIdRegistry();
        this.nodes = nodes;
        this.edges = edges;
        this.casiers = casiers;
    }

    public CrueConfigMetier getCcm() {
        return ccm;
    }

    public void setCcm(CrueConfigMetier ccm) {
        this.ccm = ccm;
    }


    /**
     *
     * @param scenario le scenario non null
     * @param avoidDuplicateNodes si true, un test sera effectué pour éviter les doublons des noeuds ( utile pour les multi sous-modeles)
     */
    public NetworkGisPositionnerResult(EMHScenario scenario, boolean avoidDuplicateNodes) {
        this.idRegistry = scenario.getIdRegistry();
        context = new PlanimetryGisModelContainer(scenario);
        nodes = context.createNodeCollection();
        edges = context.createEdgeCollection();
        casiers = context.createCasierCollection();
        if (avoidDuplicateNodes) {
            avoidDuplicateNodesSet = new HashMap<>();
        }
    }

    public LayerVisibility getLayerVisibility() {
        return layerVisibility;
    }

    public void setLayerVisibility(LayerVisibility layerVisibility) {
        this.layerVisibility = layerVisibility;
    }

    public VisuConfiguration getVisuConfiguration() {
        return visuConfiguration;
    }

    public void setVisuConfiguration(VisuConfiguration visuConfiguration) {
        this.visuConfiguration = visuConfiguration;
    }

    Set<Long> getPresentNodeUid() {
        final GISZoneCollection collection = getNodes();
        return getDone(collection);
    }

    private Set<Long> getDone(final GISZoneCollection collection) {
        GISAttributeModel dataModel = collection.getDataModel(PlanimetryGisModelContainer.UID_POSITION);
        Set<Long> doneNode = new HashSet<>();
        int size = dataModel.getSize();
        for (int i = 0; i < size; i++) {
            doneNode.add((Long) dataModel.getObjectValueAt(i));
        }
        return doneNode;
    }

    private GISAttributeModel getUids(final GISZoneCollection collection) {
        return collection.getDataModel(PlanimetryGisModelContainer.UID_POSITION);
    }

    Set<Long> getPresentCasierUid() {
        return getDone(getCasiers());
    }

    Set<Long> getPresentBrancheUid() {
        return getDone(getBranches());
    }

    void mergeFrom(NetworkGisPositionnerResult other) {
        mergeBranches(other);
        mergeNoeuds(other);
        mergeCasiers(other);
    }

    private void mergeBranches(NetworkGisPositionnerResult other) {
        Set<Long> presentBrancheUid = getPresentBrancheUid();
        GISAttributeModel otherBranchesUids = other.getUids(other.getBranches());
        int nb = otherBranchesUids.getSize();
        for (int i = 0; i < nb; i++) {
            Long otherBrancheId = (Long) otherBranchesUids.getObjectValueAt(i);
            if (!presentBrancheUid.contains(otherBrancheId)) {
                Geometry geometry = other.getBranches().getGeometry(i);
                CatEMHBranche emh = (CatEMHBranche) idRegistry.getEmh(otherBrancheId);
                addEdge(Arrays.asList(geometry.getCoordinates()), emh);
            }
        }
    }

    private void mergeNoeuds(NetworkGisPositionnerResult other) {
        Set<Long> presentNoeudUid = getPresentNodeUid();
        GISAttributeModel otherNodeUids = other.getUids(other.getNodes());
        int nb = otherNodeUids.getSize();
        for (int i = 0; i < nb; i++) {
            Long otherNodeId = (Long) otherNodeUids.getObjectValueAt(i);
            if (!presentNoeudUid.contains(otherNodeId)) {
                Geometry geometry = other.getNodes().getGeometry(i);
                CatEMHNoeud emh = (CatEMHNoeud) idRegistry.getEmh(otherNodeId);
                Coordinate coordinate = geometry.getCoordinate();
                addNode(coordinate.x, coordinate.y, emh);
            }
        }
    }

    private void mergeCasiers(NetworkGisPositionnerResult other) {
        Set<Long> presentCasierUid = getPresentCasierUid();
        GISAttributeModel otherCasierUids = other.getUids(other.getCasiers());
        int nb = otherCasierUids.getSize();
        for (int i = 0; i < nb; i++) {
            Long otherCasierId = (Long) otherCasierUids.getObjectValueAt(i);
            if (!presentCasierUid.contains(otherCasierId)) {
                Geometry geometry = other.getCasiers().getGeometry(i);
                CatEMHCasier emh = (CatEMHCasier) idRegistry.getEmh(otherCasierId);
                addCasierAsPolygone(GISGeometryFactory.INSTANCE.createLinearRing(geometry.getCoordinates()), emh);
            }
        }
    }

    TObjectIntHashMap<Long> getNodePositionByUid() {
        TObjectIntHashMap<Long> res = new TObjectIntHashMap<>();
        GISAttributeModel dataModel = getNodes().getDataModel(PlanimetryGisModelContainer.UID_POSITION);
        int nb = dataModel.getSize();
        for (int i = 0; i < nb; i++) {
            res.put((Long) dataModel.getObjectValueAt(i), i);
        }
        return res;
    }

    public GISZoneCollectionLigneBrisee getCasiers() {
        return casiers;
    }

    public GISZoneCollectionLigneBrisee getBranches() {
        return edges;
    }

    public GISPoint addNode(double x, double y, CatEMHNoeud nd) {
        //si on filtre les doublons et si présent, on ne fait rien:
        if (avoidDuplicateNodesSet != null && avoidDuplicateNodesSet.containsKey(nd.getNom())) {
            return avoidDuplicateNodesSet.get(nd.getNom());
        }
        nodes.add(x, y, 0, context.buildNodeData(nd), null);
        GISPoint gisPoint = nodes.get(nodes.getNbGeometries() - 1);
        if (avoidDuplicateNodesSet != null) {
            avoidDuplicateNodesSet.put(nd.getNom(), gisPoint);
        }
        return gisPoint;
    }

    public void addEdge(List<Coordinate> finalCoordinate, CatEMHBranche branche) {
        edges.addPolyligne(GISGeometryFactory.INSTANCE.createLineString(
                        finalCoordinate.toArray(new Coordinate[0])),
                context.buildEdgeData(branche), null);
    }

    public void addEdge(GISPolyligne finalCoordinate, CatEMHBranche branche) {
        edges.addPolyligne(finalCoordinate,
                context.buildEdgeData(branche), null);
    }

    public Map<String, Pair<Double, Double>> getAnglesBySectionNames() {
        return anglesBySectionNames;
    }

    Map<Long, Pair<Double, Double>> getAnglesBySectionUid() {
        Map<Long, Pair<Double, Double>> res = new HashMap<>();
        List<EMH> sections = idRegistry.getEMHs(EnumCatEMH.SECTION);
        Map<String, EMH> toMapOfNom = TransformerHelper.toMapOfNom(sections);
        for (Map.Entry<String, Pair<Double, Double>> entry : anglesBySectionNames.entrySet()) {
            String emhNom = entry.getKey();
            Pair<Double, Double> pair = entry.getValue();
            EMH emh = toMapOfNom.get(emhNom);
            if (emh != null) {
                res.put(emh.getUiId(), pair);
            }
        }
        return res;
    }

    public void addTraceAngle(String sectionName, double beginAngle, double endAngle) {
        anglesBySectionNames.put(sectionName, new Pair<>(beginAngle, endAngle));

    }

    public void addCasierAsPolygone(GISPolygone finalCoordinate, CatEMHCasier casier) {
        casiers.addPolygone(finalCoordinate, context.buildCasierData(casier), null);
    }

    public GISZoneCollectionPoint getNodes() {
        return nodes;
    }

    public boolean isModifiedWhileLoading() {
        return modifiedWhileLoading;
    }

    public void setModifiedWhileLoading(boolean modifiedWhileLoading) {
        this.modifiedWhileLoading = modifiedWhileLoading;
    }
}
