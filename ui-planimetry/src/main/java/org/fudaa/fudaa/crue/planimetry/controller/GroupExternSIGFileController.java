/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.controller;

import java.io.File;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper;
import org.fudaa.fudaa.crue.planimetry.layer.AbstractPlanimetryPointExternLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternSIGFileLayerGroup;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLigneBriseeExternUnmodifiableLayer;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 *
 * @author Frederic Deniger
 */
public class GroupExternSIGFileController extends AbstractGroupExternController<PlanimetryExternSIGFileLayerGroup> {

  public GroupExternSIGFileController(PlanimetryController controller, PlanimetryExternSIGFileLayerGroup groupLayer) {
    super(controller, groupLayer);
  }

  public AbstractPlanimetryPointExternLayer addLayerPointUnmodifiable(GISZoneCollectionPoint pt, File file, SigFormatHelper.EnumFormat fmt) {
    return (AbstractPlanimetryPointExternLayer) addAdditionalLayer(ExternLayerFactory.createLayerPointUnmodifiable(pt, file, fmt, controller.getVisuPanel().getEditor()));
  }

  public PlanimetryLigneBriseeExternUnmodifiableLayer addLayerLignesUnmodifiable(GISZoneCollectionLigneBrisee lignes, File file, SigFormatHelper.EnumFormat fmt) {
    final FSigEditor editor = controller.getVisuPanel().getEditor();
    final PlanimetryLigneBriseeExternUnmodifiableLayer layer = ExternLayerFactory.createLayerLignesUnmodifiable(lignes, file, fmt, editor);
    addAdditionalLayer(layer);
    return layer;
  }
}
