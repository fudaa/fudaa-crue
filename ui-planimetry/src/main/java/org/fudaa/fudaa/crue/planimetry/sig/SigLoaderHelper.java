/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig;

import org.fudaa.ctulu.gis.*;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.fudaa.sig.FSigGeomSrcData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public class SigLoaderHelper {
  private SigLoaderHelper() {
  }

  private static GISAttributeInterface[] filterAttributes(GISDataModel in) {
    List<GISAttributeInterface> res = new ArrayList<>();
    int size = in.getNbAttributes();
    for (int i = 0; i < size; i++) {
      final GISAttributeInterface attribute = in.getAttribute(i);
      //pour ignorer les attibuts créés par défaut:
      if (attribute == GISAttributeConstants.ETAT_GEOM || attribute == GISAttributeConstants.INDEX_GEOM || "ATTRIBUTE_INDEX_GEOM".equals(
          attribute.getID())) {
        continue;
      }
      attribute.getEditor().setEditable(false);
      res.add(attribute);
    }
    return res.toArray(new GISAttributeInterface[0]);
  }

  private static Pair<GISAttributeInterface[], GISDataModel> getAttributes(GISDataModel in) {
    if (in == null) {
      return null;
    }
    GISAttributeInterface[] attributes = filterAttributes(in);
    GISDataModelFilterAdapter filter = GISDataModelFilterAdapter.buildAdapter(in, attributes);
    return new Pair<>(attributes, filter);
  }

  public static GISZoneCollectionPoint buildPoints(FSigGeomSrcData result) {

    GISZoneCollectionPoint pt = new GISZoneCollectionPoint();
    if (result.getNbPoints() > 0) {
      GISDataModel pointsModel = result.getPoints()[0];
      Pair<GISAttributeInterface[], GISDataModel> attributes = SigLoaderHelper.getAttributes(pointsModel);
      if (attributes != null) {
        pointsModel.preload(attributes.first, null);
        pt.setAttributes(attributes.first, null);
        pt.addAll(attributes.second, null, false);
      }
    }
    return pt;
  }

  public static GISZoneCollectionPoint buildPoints(FSigGeomSrcData result, GISAttributeInterface[] iniAttributes) {
    GISDataModel pointsModel = result.getPoints()[0];
    GISZoneCollectionPoint pt = new GISZoneCollectionPoint();
    pointsModel.preload(iniAttributes, null);
    GISDataModelFilterAdapter filter = buildAdapterAndPreload(pointsModel, iniAttributes);
    pt.setAttributes(iniAttributes, null);
    pt.addAll(filter, null, false);
    return pt;
  }

  public static GISZoneCollectionLigneBrisee buildLine(FSigGeomSrcData result) {
    GISZoneCollectionLigneBrisee lignes = new GISZoneCollectionLigneBrisee();
    GISAttributeInterface[] attributes = null;
    final List<GISDataModel> polygones = Arrays.asList(result.getPolygones());
    if (polygones != null) {
      for (GISDataModel model : polygones) {
        if (attributes == null) {
          attributes = SigLoaderHelper.filterAttributes(model);
          lignes.setAttributes(attributes, null);
        }
        lignes.addAll(buildAdapterAndPreload(model, attributes), null, false);
      }
    }
    final List<GISDataModel> polylignes = Arrays.asList(result.getPolylignes());
    if (polylignes != null) {
      for (GISDataModel model : polylignes) {
        if (attributes == null) {
          attributes = SigLoaderHelper.filterAttributes(model);
          lignes.setAttributes(attributes, null);
        }
        lignes.addAll(buildAdapterAndPreload(model, attributes), null, false);
      }
    }
    return lignes;
  }

  public static GISZoneCollectionLigneBrisee buildLine(FSigGeomSrcData result, GISAttributeInterface[] attributes) {
    GISZoneCollectionLigneBrisee lignes = new GISZoneCollectionLigneBrisee();
    lignes.setAttributes(attributes, null);
    if (result == null) {
      return lignes;
    }
    final GISDataModel[] polygones = result.getPolygones();
    if (polygones != null) {
      for (GISDataModel model : polygones) {
        lignes.addAll(buildAdapterAndPreload(model, attributes), null, false);
      }
    }
    final GISDataModel[] polylignes = result.getPolylignes();
    if (polylignes != null) {
      for (GISDataModel model : polylignes) {
        lignes.addAll(buildAdapterAndPreload(model, attributes), null, false);
      }
    }
    return lignes;
  }

  private static GISDataModelFilterAdapter buildAdapterAndPreload(GISDataModel model, GISAttributeInterface[] attributes) {
    final GISDataModelFilterAdapter buildAdapter = GISDataModelFilterAdapter.buildAdapter(model, attributes);
    buildAdapter.preload(attributes, null);
    return buildAdapter;
  }
}
