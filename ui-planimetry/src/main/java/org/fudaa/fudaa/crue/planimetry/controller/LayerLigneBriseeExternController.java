package org.fudaa.fudaa.crue.planimetry.controller;

import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.fudaa.crue.planimetry.configuration.LigneBriseeConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.layer.AbstractPlanimetryLigneBriseeExternLayer;

/**
 *
 * @author deniger
 */
public class LayerLigneBriseeExternController<T extends AbstractPlanimetryLigneBriseeExternLayer> extends LayerModelControllerDefault<T> {

  final boolean modifiable;

  public LayerLigneBriseeExternController(boolean modifiable) {
    this.modifiable = modifiable;
  }

  public void setLayer(T layer) {
    this.layer = layer;
  }

  @Override
  public T getLayer() {
    return super.getLayer();
  }

  @Override
  public void cancel() {
    super.cancel();
    if (cloned != null) {
      layer.setLayerConfigurationNoUndo(cloned);
      cloned = null;
    }
  }

  @Override
  public void resetModification() {
    super.resetModification();
    cloned = null;
  }
  

  @Override
  public void changeDone() {
  }
  
  LigneBriseeConfiguration cloned;

  @Override
  public void changeWillBeDone() {
    cloneDataIfNeeded();
    helper.changeDoneInExternLayer(this);
    if (cloned == null) {
      LigneBriseeConfiguration layerConfiguration = layer.getLayerConfiguration();
      cloned = layerConfiguration.copy();
    }
  }

  @Override
  public void saveDone() {
    super.saveDone();
    cloned = null;
  }

  @Override
  public void setVisuConfiguration(VisuConfiguration cloned) {
  }

  @Override
  public void setEditable(boolean editable) {
    if (modifiable) {
      super.setEditable(editable);
      GISAttributeConstants.LABEL.getEditor().setEditable(editable);
    }
  }

  @Override
  public boolean isEditable() {
    return modifiable && super.isEditable();
  }
}
