package org.fudaa.fudaa.crue.planimetry.save;

import java.awt.geom.Point2D.Double;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import org.fudaa.ctulu.CtuluImageContainer;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.ctulu.converter.ToStringTransformerFactory;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessource;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessourceInfos;
import org.fudaa.dodico.crue.io.conf.Option;
import org.fudaa.ebli.calque.ZModeleStatiqueImageRaster;
import org.fudaa.fudaa.crue.common.config.ConfigSaverHelper;
import org.fudaa.fudaa.crue.planimetry.configuration.ImageConfigurationInfo;
import org.fudaa.fudaa.crue.planimetry.controller.LayerImageController;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryImageRasterLayer;

/**
 * Pour etre pris compte doit être ajoute dans la liste AdditionalLayerLoaderProcess
 *
 * @author deniger
 */
public class LayerImageControllerSaver implements AdditionalLayerLoaderProcess.Loader {
  private static final String POINTS_IMAGE_ID = "PointsImage";
  private static final String POINTS_REELS_ID = "PointsReels";

  public CrueEtudeExternRessource getSaveRessource(String id, LayerImageController layerController, File baseDir) {
    LayerControllerSaverHelper helper = new LayerControllerSaverHelper();
    CrueEtudeExternRessourceInfos ressource = new CrueEtudeExternRessourceInfos();
    final PlanimetryImageRasterLayer layer = layerController.getLayer();
    ressource.setNom(layer.getTitle());
    ressource.setLayerId(layer.getName());
    ressource.setType(ConfigExternIds.LAYER_IMAGE_SAVE_ID);
    ressource.setId(id);
    ZModeleStatiqueImageRaster model = (ZModeleStatiqueImageRaster) layer.getModelImage();
    File file = model.getData().getImgFile();
    helper.putRelativeAndAbsolutePaths(ressource, file, baseDir);
    //point image
    Double[] ptImg = model.getPtImg();
    Double[] ptReel = model.getPtReel();
    final AbstractPropertyToStringTransformer<Double[]> point2dArrayTransformer = ToStringTransformerFactory.getPoint2dArrayTransformer();
    ressource.getOptions().add(new Option(POINTS_IMAGE_ID,
            point2dArrayTransformer.toString(ptImg)));
    ressource.getOptions().add(new Option(POINTS_REELS_ID, point2dArrayTransformer.toString(
            ptReel)));
    helper.addVisibleProperty(ressource, layerController.getLayer());
    LinkedHashMap<String, String> transform = ConfigSaverHelper.getProperties(ImageConfigurationInfo.createSet(
            layer.getLayerConfiguration()));
    for (Map.Entry<String, String> entry : transform.entrySet()) {
      ressource.getOptions().add(new Option(entry.getKey(), entry.getValue()));
    }
    return new CrueEtudeExternRessource(ressource);
  }

  @Override
  public String getType() {
    return ConfigExternIds.LAYER_IMAGE_SAVE_ID;
  }

  @Override
  public PlanimetryAdditionalLayerContrat load(CrueEtudeExternRessourceInfos ressource, CtuluLog log, File baseDir) {
    LayerControllerSaverHelper helper = new LayerControllerSaverHelper();
    Map<String, String> optionsBykey = helper.toMap(ressource);
    File image = helper.retrieveFiles(optionsBykey, ressource, baseDir, log);
    if (image == null) {
      return null;
    }
    final AbstractPropertyToStringTransformer<Double[]> point2dArrayTransformer = ToStringTransformerFactory.getPoint2dArrayTransformer();
    Double[] ptImg = point2dArrayTransformer.fromString(optionsBykey.remove(POINTS_IMAGE_ID));
    if (ptImg == null || ptImg.length < 2) {
      log.addError("externConfig.image.pointImage.error", ressource.getNom());
      return null;
    }
    Double[] ptReel = point2dArrayTransformer.fromString(optionsBykey.remove(POINTS_REELS_ID));
    if (ptReel == null || ptReel.length < 2) {
      log.addError("externConfig.image.pointReals.error", ressource.getNom());
      return null;
    }
    if (ptReel.length != ptImg.length) {
      log.addError("externConfig.image.pointIncoherent.error", ressource.getNom());
      return null;
    }
    ZModeleStatiqueImageRaster model = new ZModeleStatiqueImageRaster(new CtuluImageContainer(image));
    model.setProj(ptImg, ptReel);
    PlanimetryImageRasterLayer layer = new PlanimetryImageRasterLayer(model);
    layer.setTitle(ressource.getNom());
    layer.setName(ressource.getLayerId());
    LayerControllerSaverHelper.restoreVisuProperties(optionsBykey, layer, ImageConfigurationInfo.createSet(layer.getLayerConfiguration()), log);
    return layer;
  }
}
