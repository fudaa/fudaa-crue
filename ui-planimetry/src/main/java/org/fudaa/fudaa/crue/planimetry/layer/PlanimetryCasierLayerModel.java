package org.fudaa.fudaa.crue.planimetry.layer;

import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.*;
import org.fudaa.dodico.crue.edition.EditionCreateEMH;
import org.fudaa.dodico.crue.edition.EditionDelete;
import org.fudaa.dodico.crue.edition.EditionRename;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.crue.planimetry.configuration.CasierConfigurationExtra;
import org.fudaa.fudaa.crue.planimetry.controller.LayerCasierController;
import org.fudaa.fudaa.crue.planimetry.controller.LayerNodeController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryControllerHelper;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryGisModelContainer;
import org.locationtech.jts.algorithm.locate.PointOnGeometryLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;

import java.util.ArrayList;
import java.util.List;

/**
 * @author deniger
 */
public class PlanimetryCasierLayerModel extends PlanimetryLigneBriseeLayerModel<LayerCasierController> {
  private CasierConfigurationExtra casierConfigurationExtra;

  public PlanimetryCasierLayerModel(GISZoneCollectionLigneBrisee _zone, LayerCasierController modelController) {
    super(_zone, modelController);
  }

  public PlanimetryCasierLayerModel() {
  }

  public CasierConfigurationExtra getCasierConfigurationExtra() {
    return casierConfigurationExtra;
  }

  public void setCasierConfigurationExtra(CasierConfigurationExtra casierConfigurationExtra) {
    this.casierConfigurationExtra = casierConfigurationExtra;
  }

  @Override
  public boolean isGeometryReliee(int _idxGeom) {
    return true;
  }

  /**
   *
   * @param idx
   * @return true si le sous-modele parent est visible
   */
  @Override
  public boolean isGeometryVisible(int idx) {
    CatEMHCasier casier = getLayerController().getEMHFromPositionInModel(idx);
    PlanimetryControllerHelper helper = getLayerController().getHelper();
    return helper.isVisible(casier);
  }

  @Override
  public boolean addGeometry(Geometry _p, CtuluCommandContainer _cmd, CtuluUI _ui, ZEditionAttributesDataI _d) {
    return false;
  }

  public void setGeometry(int i, GISPolygone polygon) {
    super.getLayerController().changeWillBeDone();
    geometries_.setGeometry(i, polygon, null);
    super.getLayerController().changeDone();
  }

  public int findPointWithoutCasier(GISPolygone polygone) {
    final LayerNodeController nodeController = getLayerController().getHelper().getNodeController();
    GISZoneCollectionPoint nodeCollection = nodeController.getNodeCollection();
    PointOnGeometryLocator tester = GISLib.createPolygoneTester(polygone);
    int nb = nodeCollection.getNumGeometries();
    for (int i = 0; i < nb; i++) {
      if (GISLib.isInside(tester, nodeCollection.get(i).getCoordinate())) {
        CatEMHNoeud noeud = nodeController.getEMHFromPositionInModel(i);
        if (noeud.getCasier() == null) {
          return i;
        }
      }
    }
    return -1;
  }

  @Override
  public boolean removeLigneBrisee(int[] _idx, CtuluCommandContainer _cmd) {
    return removeCasier(_idx, false);
  }

  public boolean removeLigneBriseeCascade(int[] _idx, CtuluCommandContainer _cmd) {
    return removeCasier(_idx, true);
  }

  public boolean removeCasier(int[] _idx, final boolean cascade) {
    PlanimetryControllerHelper helper = super.getLayerController().getHelper();
    if (!helper.canEMHBeAddedAndDisplayError()) {
      return false;
    }
    helper.scenarioEditionStarted();
    List<EMH> casier = new ArrayList<>();
    for (int i = 0; i < _idx.length; i++) {
      casier.add(getLayerController().getEMHFromPositionInModel(_idx[i]));
    }
    final EMHScenario scenario = helper.getPlanimetryGisModelContainer().getScenario();
    //a utiliser
    EditionDelete.RemoveBilan deleted = EditionDelete.delete(casier, scenario, cascade);
    helper.displayBilan(deleted);
    return updateAfterRemove();
  }

  public boolean updateAfterRemove() {
    PlanimetryControllerHelper helper = super.getLayerController().getHelper();
    final EMHScenario scenario = helper.getPlanimetryGisModelContainer().getScenario();
    TIntArrayList toRemove = new TIntArrayList();
    GISAttributeModel uidModel = helper.getPlanimetryGisModelContainer().getUidModel(geometries_);
    int nb = uidModel.getSize();
    for (int i = 0; i < nb; i++) {
      Long uid = (Long) uidModel.getObjectValueAt(i);
      if (!scenario.getIdRegistry().containEMH(uid)) {
        toRemove.add(i);
      }
    }
    if (!toRemove.isEmpty()) {
      return super.removeLigneBrisee(toRemove.toNativeArray(), null);
    }
    return false;
  }

  @Override
  public boolean addGeometry(GrPolygone _p, CtuluCommandContainer _cmd, CtuluUI _ui, ZEditionAttributesDataI _d) {
    if (_p.sommets_.nombre() < 3) {
      return false;
    }
    PlanimetryControllerHelper helper = super.getLayerController().getHelper();
    if (!helper.canEMHBeAddedAndDisplayError()) {
      return false;
    }
    EnumCasierType casierType = helper.getCreationEMH().getCasierType();
    if (casierType == null) {
      return false;
    }
    getLayerController().changeWillBeDone();
    helper.scenarioEditionStarted();
    GISPolygone polygone = _p.toGIS();
    //est-ce qu'il y a un noeud ?
    int point = findPointWithoutCasier(polygone);
    final LayerNodeController nodeController = getLayerController().getHelper().getNodeController();
    CatEMHNoeud initialNoeud = point >= 0 ? (CatEMHNoeud) nodeController.getEMHFromPositionInModel(point) : null;
    CatEMHCasier newCasier = addCasier(helper, casierType, initialNoeud, polygone);
    getLayerController().changeDone();
    return newCasier != null;
  }

  @Override
  public boolean addGeometry(GrPolyligne _p, CtuluCommandContainer _cmd, CtuluUI _ui, ZEditionAttributesDataI _d) {
    return false;
  }

  public CatEMHCasier addCasier(PlanimetryControllerHelper helper, CatEMHNoeud initialNoeud, GISPolygone polygone, String casierName) {
    CatEMHCasier addCasier = addCasier(helper, EnumCasierType.EMHCasierProfil, initialNoeud, polygone);
    if (addCasier == null) {
      return null;
    }
    new EditionRename().rename(addCasier, casierName);
    //on doit aussi mettre à jour le nom du noeud dans les données geographiques:
    final LayerNodeController nodeController = getLayerController().getHelper().getNodeController();
    final int nodePosition = nodeController.getNodePosition(addCasier.getNoeud().getUiId());
    GISAttributeModel nodeNames = nodeController.getGeomData().getModel(PlanimetryGisModelContainer.NOM_EMH_POSITION);
    nodeNames.setObject(nodePosition, addCasier.getNoeud().getNom(), null);
    //on doit aussi mettre à jour le nom du casier dans les données geographiques:
    final LayerCasierController casierController = getLayerController().getHelper().getCasierController();
    final int casierPosition = casierController.getCasierPosition(addCasier.getUiId());
    GISAttributeModel casierNames = casierController.getGeomData().getModel(PlanimetryGisModelContainer.NOM_EMH_POSITION);
    casierNames.setObject(casierPosition, addCasier.getNom(), null);
    return addCasier;
  }

  private CatEMHCasier addCasier(PlanimetryControllerHelper helper, EnumCasierType casierType, CatEMHNoeud initialNoeud, GISPolygone polygone) {
    final LayerNodeController nodeController = getLayerController().getHelper().getNodeController();
    EditionCreateEMH creator = new EditionCreateEMH(getLayerController().getDefaultValues());
    CatEMHCasier newCasier = creator.createCasier(helper.getDefaultSousModele(), casierType, initialNoeud,
        helper.getController().getCrueConfigMetier());
    if (newCasier != null) {
      //un noeud a été ajoute
      if (initialNoeud == null) {
        Coordinate center = PlanimetryCasierLayer.getCenter(polygone);
        PlanimetryNodeLayerModel nodeModel = nodeController.getLayer().modeleDonnees();
        nodeModel.addNoeud(newCasier.getNoeud(), new GISPoint(center));
      }
      Object[] data = helper.getPlanimetryGisModelContainer().buildCasierData(newCasier);
      getGeometries().addGeometry(polygone, data, null);
    }
    return newCasier;
  }
}
