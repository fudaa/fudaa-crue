package org.fudaa.fudaa.crue.planimetry;

import com.jidesoft.swing.TitledSeparator;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuPopupMenu;
import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.IdRegistry;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCatchEvent;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.calque.action.AddVertexAction;
import org.fudaa.ebli.calque.action.CalqueSelectNormalSelectionAction;
import org.fudaa.ebli.calque.action.SceneSelectNextAction;
import org.fudaa.ebli.calque.action.SceneSelectPreviousAction;
import org.fudaa.ebli.calque.edition.ZEditorDefault;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionMap;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.find.EbliFindDialog;
import org.fudaa.ebli.find.EbliFindableItem;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.crue.planimetry.action.*;
import org.fudaa.fudaa.crue.planimetry.controller.LayerBrancheController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryVisuPanelController;
import org.fudaa.fudaa.crue.planimetry.layer.CalqueWithDynamicActions;
import org.fudaa.fudaa.crue.planimetry.listener.PlanimetryVisuUpdater;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;
import org.fudaa.fudaa.sig.layer.FSigVisuPanel;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class PlanimetryVisuPanel extends FSigVisuPanel {

  public static void addSelectionAction(final BuMenu _menu, final boolean _enable) {
    Action act;
    if ((act = EbliActionMap.getInstance().getAction("TOUTSELECTIONNER")) != null) {
      _menu.add(act);
    }
    if ((act = EbliActionMap.getInstance().getAction("INVERSESELECTION")) != null) {
      _menu.add(act);
    }
    if ((act = EbliActionMap.getInstance().getAction("CLEARSELECTION")) != null) {
      _menu.add(act);
    }
  }

  public static void addSelectionAction(final BuMenu _menu, final boolean _enable, final ActionListener _l) {
    BuMenuItem it = _menu.addMenuItem(BuResource.BU.getString("Tout sélectionner"), "TOUTSELECTIONNER", false, KeyEvent.VK_A);
    it.setEnabled(_enable);
    if (_l != null) {
      it.addActionListener(_l);
    }
    it = _menu.addMenuItem(CtuluLib.getS("Inverser la sélection"), "INVERSESELECTION", BuResource.BU.getIcon("aucun"),
            false, KeyEvent.VK_I);
    it.setEnabled(_enable);
    if (_l != null) {
      it.addActionListener(_l);
    }
    it = _menu.addMenuItem(CtuluLib.getS("Effacer la sélection"), "CLEARSELECTION", BuResource.BU.getIcon("effacer"),
            false, 0);
    it.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
    it.setEnabled(_enable);
    if (_l != null) {
      it.addActionListener(_l);
    }

  }
  PlanimetryVisuPanelConfigurer configurer;
  private PlanimetryController planimetryCalqueContext;
  EbliActionSimple findAction;
  JMenuItem itemAtomicActivation;
  JMenuItem itemAddAtomic;
  private final JMenuItem selectNext;
  private final JMenuItem selectPrevious;
  EditZonePanel editZonePanel;
  JComponent gisEditionPalette;

  public PlanimetryVisuPanel(final BGroupeCalque _gcMain, final CtuluUI _impl) {
    super(_gcMain, new PlanimetryVisuPanelController(_impl));
    ((FSigEditor) gisEditor_).getEditAction()[0].putValue(Action.NAME, NbBundle.getMessage(PlanimetryVisuPanel.class, "EditGisData.DisplayName"));
    configurer = new PlanimetryVisuPanelConfigurer();
    createFindActions();
    getVueCalque().setCheckTtranslation(false);
    gisEditor_.setDisplayInfo(false);
    final SceneSelectPreviousAction sceneSelectPreviousAction = new SceneSelectPreviousAction(gisEditor_.getSceneEditor());
    final SceneSelectNextAction sceneSelectNextAction = new SceneSelectNextAction(gisEditor_.getSceneEditor());
    final CalqueSelectNormalSelectionAction normalSelection = new CalqueSelectNormalSelectionAction(gisEditor_.getSupport());
    final KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.ALT_MASK);
    getScene().getAtomicSelectionAction().setKey(keyStroke);
    EbliLib.updateMapKeyStroke(this, new Action[]{sceneSelectPreviousAction, sceneSelectNextAction, normalSelection, getScene().
      getAtomicSelectionAction()});
    installSelectionShortcuts();
    selectNext = (JMenuItem) sceneSelectNextAction.buildMenuItem(EbliComponentFactory.INSTANCE);
    selectPrevious = (JMenuItem) sceneSelectPreviousAction.buildMenuItem(EbliComponentFactory.INSTANCE);
    FSigEditor.activeDeferredModificationAsDefault();
    getScene().setUseSelectedLayerFirstForSelection(false);
    setAddNbSelectionInInfo(true);
  }

  private Action createAction(final String cmd, final KeyStroke ks) {
    final Action action = new AbstractAction(cmd) {
      @Override
      public void actionPerformed(final ActionEvent e) {
        PlanimetryVisuPanel.this.actionPerformed(new ActionEvent(PlanimetryVisuPanel.this, 0, cmd));
      }
    };
    action.putValue(Action.ACTION_COMMAND_KEY, cmd);
    action.putValue(Action.ACCELERATOR_KEY, ks);
    return action;
  }

  @Override
  public PlanimetryGisEditor getEditor() {
    return (PlanimetryGisEditor) super.getEditor();
  }

  public final void installSelectionShortcuts() {
    final Action selectAll = createAction("TOUTSELECTIONNER", KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
    final Action invAction = createAction("INVERSESELECTION", KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_MASK));
    final Action clearAction = createAction("CLEARSELECTION", KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
    EbliLib.updateMapKeyStroke(this, new Action[]{selectAll, invAction, clearAction});
  }

  public PlanimetryVisuPanelConfigurer getConfigurer() {
    return configurer;
  }

  @Override
  public void setAccrocheText(final ZCatchEvent _evt) {
    final ZCalqueAffichageDonneesInterface layer = _evt.selection.getScene().getLayerForId(_evt.idxGeom);
    final int idx = _evt.selection.getScene().sceneId2LayerId(_evt.idxGeom);
    final int vertex = _evt.idxVertex;
    final PlanimetryController planimetryController = getPlanimetryController();
    if (layer == planimetryController.getCqNoeud()) {
      final EMH emh = planimetryController.getNodeController().getEMHFromLayerPosition(idx);
      setInfoText(NbBundle.getMessage(PlanimetryVisuPanel.class, "accroche.node", emh.getNom()));
    } else if (layer == planimetryController.getCqBranche()) {
      setAccrocheTextBranche(planimetryController, idx, vertex);
    } else if (layer == planimetryController.getCqCasier()) {
      final EMH emh = planimetryController.getCasierController().getEMHFromLayerPosition(idx);
      setInfoText(NbBundle.getMessage(PlanimetryVisuPanel.class, "accroche.casier", emh.getNom(), vertex));
    } else if (layer == planimetryController.getCqSection()) {
      final EMH emh = planimetryController.getSectionController().getEMHFromLayerPosition(idx);
      setInfoText(NbBundle.getMessage(PlanimetryVisuPanel.class, "accroche.section", emh.getNom()));
    } else {
      super.setAccrocheText(_evt);
    }
  }

  private void setAccrocheTextBranche(final PlanimetryController planimetryController, final int idx, final int vertex) throws MissingResourceException {
    final CatEMHBranche branche = (CatEMHBranche) planimetryController.getBrancheController().getEMHFromLayerPosition(idx);
    final GISPolyligne brancheGis = planimetryController.getBrancheController().getBrancheGis(idx);
    if (vertex == LayerBrancheController.getAmontCoordinatePosition()) {
      final EMH emh = branche.getNoeudAmont();
      setInfoText(NbBundle.getMessage(PlanimetryVisuPanel.class, "accroche.node", emh.getNom()));
    } else if (vertex == LayerBrancheController.getAvalCoordinatePosition(brancheGis.getNumPoints())) {
      final EMH emh = branche.getNoeudAval();
      setInfoText(NbBundle.getMessage(PlanimetryVisuPanel.class, "accroche.node", emh.getNom()));
    } else {
      setInfoText(NbBundle.getMessage(PlanimetryVisuPanel.class, "accroche.branche", branche.getNom(), vertex + 1));
    }
  }

  @Override
  public void setAccrocheText(final String[] vars) {
    final String text
            = EbliResource.EBLI.getString(
                    "Accroche : Calque={0} Geom={1} Sommet={2}", (Object[]) vars);
    setInfoText(text);
  }

  @Override
  public void unsetAcrrocheText() {
    super.unsetAcrrocheText();
  }

  @Override
  protected ZEditorDefault createGisEditor() {
    return new PlanimetryGisEditor(this);
  }

  public void setConfigurer(final PlanimetryVisuPanelConfigurer configurer) {
    this.configurer = configurer;
  }

  public EbliActionInterface getSigEditAction() {
    return getEditor().getEditAction()[0];
  }

  public EbliActionInterface createFindAction(final BCalque cq) {
    final EbliActionSimple res = new EbliActionSimple(cq.getTitle() + ": " + BuResource.BU.getString("Rechercher..."), BuResource.BU.getIcon("rechercher"),
            "RECHERCHER") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        findSelectedLayer();
      }

              @Override
      public void setEnabled(final boolean _newValue) {
              }
            };
    return res;
  }

  @Override
  public void setActive(final boolean _b) {
  }

  public PlanimetryController getPlanimetryController() {
    return planimetryCalqueContext;
  }

  @Override
  protected void fillCmdContextuelles(final BuPopupMenu _menu) {
    createFindActions();
    final BCalque cqActif = getScene().getCalqueActif();
    if (cqActif != null) {
      fillMenuWithCalqueActions(_menu, cqActif);
      if (cqActif instanceof CalqueWithDynamicActions) {
        ((CalqueWithDynamicActions) cqActif).fillPopup(_menu);
      }
    }

    if (getScene() != null) {
      fillMenuWithSceneActions(_menu);
    }
    _menu.addMenuItem(BuResource.BU.getString("Rechercher..."), "RECHERCHER", BuResource.BU.getIcon("rechercher"), true, KeyEvent.VK_F).
            addActionListener(this);
    _menu.addSeparator();
    if (itemAddAtomic == null) {
      final AddVertexAction action = new AddVertexAction(getScene());
      itemAddAtomic = new JMenuItem(action);
    }
    final AddVertexAction action = (AddVertexAction) itemAddAtomic.getAction();
    if (action.isActivatedFor(cqActif)) {
      _menu.add(itemAddAtomic);
    }
    action.updateStateBeforeShow();
    if (itemAtomicActivation == null) {
      getScene().getAtomicSelectionAction().setKey(KeyStroke.getKeyStroke("Alt s"));
      getScene().getAtomicSelectionAction().updateStateBeforeShow();
      itemAtomicActivation = (JMenuItem) getScene().getAtomicSelectionAction().buildMenuItem(EbliComponentFactory.INSTANCE);
    }
    _menu.add(itemAtomicActivation);
    _menu.addSeparator();
    _menu.add(getMenuSelectionPath());
    if (contextTools_ == null) {
      buildTools();
    }
    _menu.add(contextTools_);
  }

  protected final void createFindActions() {
    if (findAction == null) {
      findAction = new EbliActionSimple(BuResource.BU.getString("Rechercher..."), BuResource.BU.getIcon("rechercher"), "RECHERCHER") {
        @Override
        public void actionPerformed(final ActionEvent _e) {
          find();
        }
      };
      findAction.setKey(KeyStroke.getKeyStroke("ctrl F"));
    }
  }

  public EbliActionSimple getFindAction() {
    return findAction;
  }

  @Override
  public void find() {
    createFindDialog();
    findDialog_.setSelectedFindAction(getPlanimetryController().getGroupHydraulicController().getGroupHydraulic());
    findDialog_.affiche(this);
  }

  public void findSelectedLayer() {
    createFindDialog();
    final BCalque cq = getCalqueActif();
    findDialog_.setSelectedFindAction((cq instanceof EbliFindableItem) ? (EbliFindableItem) cq : null);
    findDialog_.affiche(this);
  }

  @Override
  public JMenu getMenuSelectionPath() {
    final BuMenu selection = new BuMenu(EbliLib.getS("Sélection"), "SELECTION");
    getController().addSelectionActionTo(selection);
    selection.add(selectPrevious);
    selection.add(selectNext);

    selection.addSeparator();
    if (!EbliActionMap.getInstance().isEmpty()) {
      addSelectionAction(selection, true);
    } else {
      addSelectionAction(selection, true, this);
    }
    return selection;
  }

  public final PlanimetryVisuPanelController getPlanimetryVisuController() {
    return (PlanimetryVisuPanelController) getController();
  }

  boolean isWorkOnOneLayer() {
    return getScene().isRestrictedToCalqueActif();
  }

  public void setWokOnOneLayer(final boolean restricted) {
    getScene().setRestrictedToCalqueActif(restricted);
    getArbreCalqueModel().setSelectionMultiCalques(!restricted);
    getScene().clearSelection();
  }

  protected void init(final PlanimetryController ctx) {
    planimetryCalqueContext = ctx;
    getDonneesCalque().enPremier(ctx.getGroupExternController().getGroupExtern());
    getDonneesCalque().enPremier(ctx.getGroupHydraulicController().getGroupHydraulic());
    final ObserverCalque observerCalque = new ObserverCalque();
    getArbreCalqueModel().getObservable().addObserver(observerCalque);
    observerCalque.addListeners();
    new PlanimetryVisuUpdater(this);
  }

  public void setSelectedEMHs(final List<EMH> selectedEMH) {
    getScene().clearSelection();
    boolean selectionChanged = getPlanimetryController().getNodeController().setSelectedEMHs(selectedEMH);
    selectionChanged |= getPlanimetryController().getBrancheController().setSelectedEMHs(selectedEMH);
    selectionChanged |= getPlanimetryController().getCasierController().setSelectedEMHs(selectedEMH);
    selectionChanged |= getPlanimetryController().getSectionController().setSelectedEMHs(selectedEMH);
    selectionChanged |= getPlanimetryController().getTraceController().setSelectedEMHs(selectedEMH);
    if (selectionChanged) {
      getEditor().getSceneEditor().zoomControlledOnSelected();
    }
  }

  public void setSelectedEMHFromUids(final Collection<Long> selectedEMHUids) {
    getScene().clearSelection();
    boolean selectionChanged = getPlanimetryController().getNodeController().setSelectedEMHUids(selectedEMHUids);
    selectionChanged |= getPlanimetryController().getBrancheController().setSelectedEMHUids(selectedEMHUids);
    selectionChanged |= getPlanimetryController().getCasierController().setSelectedEMHUids(selectedEMHUids);
    selectionChanged |= getPlanimetryController().getSectionController().setSelectedEMHUids(selectedEMHUids);
    if (selectionChanged) {
      final GrBoite zoomOnSelected = getEditor().getSceneEditor().getDomaineOnSelected();
      if (zoomOnSelected != null) {
        final GrBoite currentZoom = getPlanimetryVisuController().getVisuPanel().getVueCalque().getViewBoite();
        if (!currentZoom.contientXY(zoomOnSelected)) {
          getEditor().getSceneEditor().zoomControlledOnSelected();
        }
      }
      getScene().fireSelectionEvent();
    }
  }

  public List<EMH> getSelectedEMHs() {
    final Set<Long> uids = new HashSet<>();
    getPlanimetryController().getNodeController().getSelectedEMHs(uids);
    getPlanimetryController().getBrancheController().getSelectedEMHs(uids);
    getPlanimetryController().getCasierController().getSelectedEMHs(uids);
    getPlanimetryController().getSectionController().getSelectedEMHs(uids);
    getPlanimetryController().getTraceController().getSelectedEMHs(uids);
    return getEmhs(uids);
  }

  private void createFindDialog() {
    if (findDialog_ == null) {
      findDialog_ = new EbliFindDialog(this);
    } else {
      findDialog_.updateBeforeShow();
    }
  }

  @Override
  public void clearListeners() {
    super.clearListeners();
    EbliLib.cleanListener(findAction);
  }

  @Override
  public FSigLayerGroup getGroupGIS() {
    return planimetryCalqueContext.getGroupExternController().getGroupExtern();
  }

  public EditZonePanel getEditZonePanel() {
    return editZonePanel;
  }

  public JComponent createGisEditionPalette() {
    return createGisEditionPalette(true);
  }

  public JComponent createGisEditionPalette(final boolean editHydraulic1d) {
    if (gisEditionPalette != null) {
      return gisEditionPalette;
    }
    final PlanimetryGISEditionAction action = new PlanimetryGISEditionAction(getArbreCalqueModel().getTreeSelectionModel(),
            gisEditor_, getScene());
    action.setUsedAsTab(true);
    gisEditionPalette = action.buildContentPane();
    action.updateBeforeShow();
    editZonePanel = new EditZonePanel();
    final PlanimetryGisPaletteEdition palette = action.getPalette();
    if (editHydraulic1d) {
      final AddNoeudAction addNoeud = new AddNoeudAction(palette, planimetryCalqueContext, editZonePanel);
      final AddCasierAction addCasier = new AddCasierAction(palette, planimetryCalqueContext, editZonePanel);
      final AddBrancheAction addBranche = new AddBrancheAction(palette, planimetryCalqueContext, editZonePanel);
      getPlanimetryVisuController().addEditAction(addNoeud);
      getPlanimetryVisuController().addEditAction(addCasier);
      getPlanimetryVisuController().addEditAction(addBranche);
      getPlanimetryVisuController().addEditAction(getPlanimetryController().getProjectEMHOnSIGAction());
      getPlanimetryVisuController().addEditAction(getPlanimetryController().getTraceProfilAttachedToBrancheAction());

      TitledSeparator titleSeparator = new TitledSeparator(org.openide.util.NbBundle.getMessage(PlanimetryVisuPanel.class, "Toolbar.Separator"));
      final Border emptyBorder = BorderFactory.createEmptyBorder(5, 1, 1, 1);

      titleSeparator.setBorder(emptyBorder);

      gisEditionPalette.add(titleSeparator);

      gisEditionPalette.add(addNoeud.buildToolButton(EbliComponentFactory.INSTANCE));
      gisEditionPalette.add(addBranche.buildToolButton(EbliComponentFactory.INSTANCE));
      gisEditionPalette.add(addCasier.buildToolButton(EbliComponentFactory.INSTANCE));
      titleSeparator = new TitledSeparator(org.openide.util.NbBundle.getMessage(PlanimetryVisuPanel.class, "Project.Separator"));

      titleSeparator.setBorder(emptyBorder);

      gisEditionPalette.add(titleSeparator);

      gisEditionPalette.add(getPlanimetryController().getProjectEMHOnSIGAction().buildToolButton(EbliComponentFactory.INSTANCE));
      gisEditionPalette.add(getPlanimetryController().getTraceProfilAttachedToBrancheAction().buildToolButton(EbliComponentFactory.INSTANCE));
    }
    return gisEditionPalette;
  }

  @Override
  public EbliActionInterface[] getApplicationActions() {
    final List<EbliActionInterface> arrayList = new ArrayList<>(4);
    initButtonGroupSpecific(arrayList, getController());
    configurer.addApplicationActions(this, arrayList);
    return arrayList.toArray(new EbliActionInterface[0]);
  }

  public void setEditable(final boolean editable) {
    getPlanimetryVisuController().setEditable(editable);
    final FSigEditor editor = getEditor();
    editor.updatePalette();
  }

  public List<EMH> getEmhs(final Set<Long> uids) {
    if (uids.isEmpty()) {
      return Collections.emptyList();
    }
    final List<EMH> emhs = new ArrayList<>();
    final IdRegistry idRegistry = getPlanimetryController().getScenario().getIdRegistry();
    for (final Long uid : uids) {
      final EMH emh = idRegistry.getEmh(uid);
      if (emh != null) {
        emhs.add(emh);
      }
    }
    return emhs;

  }

  private class ObserverCalque implements Observer {

    final SelectionUpdater listener = new SelectionUpdater();
    ZCalqueAffichageDonneesInterface[] targetLayers;

    protected void addListeners() {
      targetLayers = getScene().getTargetLayers();
      for (final ZCalqueAffichageDonneesInterface layer : targetLayers) {
        layer.addSelectionListener(listener);
      }
    }

    @Override
    public void update(final Observable o, final Object arg) {
      if (BArbreCalqueModel.LAYER_ADDED.equals(arg) || BArbreCalqueModel.LAYER_REMOVED.equals(arg)) {
        if (targetLayers != null) {
          for (final ZCalqueAffichageDonneesInterface layer : targetLayers) {
            layer.removeSelectionListener(listener);
          }
          addListeners();
        }
      }
    }
  }

  private class SelectionUpdater implements ZSelectionListener {

    @Override
    public void selectionChanged(final ZSelectionEvent _evt) {
      final ZCalqueAffichageDonneesInterface source = _evt.getSource();
      if (!source.isSelectionEmpty()) {
        getArbreCalqueModel().setSelectionCalque((BCalque) source);
      }
    }
  }
}
