/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import org.locationtech.jts.geom.CoordinateSequence;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.fudaa.crue.planimetry.sig.traceprofil.SectionProfilToBrancheParametersComputed.BrancheIntersection;

/**
 * Permet de récupérer les branches intersectées par les sections à affecter.
 *
 * @author Frederic Deniger
 */
public class SectionProfilToBrancheProcessorIntersection {

  public SectionProfilToBrancheParametersComputed process(SectionProfilToBrancheParametersBranche branchesParameters,
          List<SectionProfilToBrancheParametersSection> sectionParameters) {
    SectionProfilToBrancheParametersComputed res = new SectionProfilToBrancheParametersComputed();
    for (SectionProfilToBrancheParametersSection sectionParameter : sectionParameters) {
      List<BrancheIntersection> intersectedBranche = getIntersectedBranche(sectionParameter, branchesParameters);
      if (!intersectedBranche.isEmpty()) {
        res.addIntersection(sectionParameter, intersectedBranche);
      }
    }
    return res;
  }

  private List<BrancheIntersection> getIntersectedBranche(SectionProfilToBrancheParametersSection sectionParameter,
          SectionProfilToBrancheParametersBranche branchesParameters) {
    List<BrancheIntersection> branchesName = new ArrayList<>();
    Map<String, GISPolyligne> ligneByBrancheName = branchesParameters.getLigneByBrancheName();
    for (Map.Entry<String, GISPolyligne> entry : ligneByBrancheName.entrySet()) {
      String brancheKey = entry.getKey();
      GISPolyligne brancheLine = entry.getValue();
      BrancheIntersection intersect = intersect(brancheKey, sectionParameter, brancheLine);
      if (intersect != null) {
        branchesName.add(intersect);
      }
    }
    return branchesName;

  }

  private BrancheIntersection intersect(String brancheKey, SectionProfilToBrancheParametersSection sectionParameter, GISPolyligne brancheLine) {
    CoordinateSequence coordinateSequence = brancheLine.getCoordinateSequence();
    int nbPoint = coordinateSequence.size();
    GrPoint pt1 = new GrPoint();
    GrPoint pt2 = new GrPoint();
    GrSegment brancheSegment = new GrSegment(pt1, pt2);
    double xp = 0;
    for (int i = 0; i < nbPoint - 1; i++) {
      pt1.setCoordonnees(coordinateSequence.getX(i), coordinateSequence.getY(i), 0);
      pt2.setCoordonnees(coordinateSequence.getX(i + 1), coordinateSequence.getY(i + 1), 0);
      GrPoint intersectionXY = brancheSegment.intersectionXY(sectionParameter.getLitMineur());
      if (intersectionXY != null) {
        xp = xp + intersectionXY.distance(pt1);
        return new BrancheIntersection(xp, brancheKey, brancheLine.getLength());
      }
      xp = xp + brancheSegment.longueurXY();
    }
    return null;
  }
}
