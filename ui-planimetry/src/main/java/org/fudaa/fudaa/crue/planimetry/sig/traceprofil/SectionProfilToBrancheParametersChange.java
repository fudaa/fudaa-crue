/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

/**
 * Les données nécessaires pour la modifications des sections.
 *
 * @author Frederic Deniger
 */
public class SectionProfilToBrancheParametersChange implements ObjetWithID {

  public static final String I18N_ACCEPT_OPERATION = "acceptModification.property";
  public static final String I18N_NEW_BRANCHE_NAME = "newBrancheName.property";
  public static final String I18N_OLD_BRANCHE_NAME = "oldBrancheName.property";
  public static final String I18N_OLD_XP = "oldXp.property";
  public static final String I18N_NEW_XP = "newXp.property";
  public static final String PROP_ACCEPT_OPERATION = "acceptOperation";
  public static final String PROP_NEW_BRANCHE_NAME = "newBrancheName";
  public static final String PROP_OLD_BRANCHE_NAME = "oldBrancheName";
  public static final String PROP_OLD_XP = "oldXpAsString";
  public static final String PROP_NEW_XP = "newXpAsString";
  private boolean acceptOperation = true;
  private final String sectionName;
  private final String oldBrancheName;
  private String newBrancheName;
  private final String oldXpAsString;
  private Double newXp;
  private String newXpAsString;
  int idxTraceInGeom;
  GISZoneCollectionLigneBrisee traceModel;

  public SectionProfilToBrancheParametersChange(String sectionName, String oldBrancheName, String oldXpAsString) {
    this.sectionName = sectionName;
    this.oldBrancheName = oldBrancheName;
    this.oldXpAsString = oldXpAsString;
  }

  public int getIdxTraceInGeom() {
    return idxTraceInGeom;
  }

  @Override
  public String getId() {
    return getSectionName();
  }

  @Override
  public String getNom() {
    return getSectionName();
  }

  public void setIdxTraceInGeom(int idxTraceInGeom) {
    this.idxTraceInGeom = idxTraceInGeom;
  }

  public GISZoneCollectionLigneBrisee getTraceGisModel() {
    return traceModel;
  }

  public void setTraceGisModel(GISZoneCollectionLigneBrisee traceModel) {
    this.traceModel = traceModel;
  }

  @PropertyDesc(i18n = I18N_OLD_XP, i18nBundleBaseName = "org/fudaa/fudaa/crue/planimetry/sig/traceprofil/Bundle.properties")
  public boolean isAcceptOperation() {
    return acceptOperation;
  }

  public void setAcceptOperation(boolean acceptOperation) {
    this.acceptOperation = acceptOperation;
  }

  @PropertyDesc(i18n = I18N_NEW_BRANCHE_NAME, i18nBundleBaseName = "org/fudaa/fudaa/crue/planimetry/sig/traceprofil/Bundle.properties",
          readOnly = true)
  public String getNewBrancheName() {
    return newBrancheName;
  }

  public void setNewBrancheName(String newBrancheName) {
    this.newBrancheName = newBrancheName;
  }

  public Double getNewXp() {
    return newXp;
  }

  @PropertyDesc(i18n = I18N_NEW_XP, i18nBundleBaseName = "org/fudaa/fudaa/crue/planimetry/sig/traceprofil/Bundle.properties",
          readOnly = true)
  public String getNewXpAsString() {
    return newXpAsString;
  }

  public void setNewXp(Double newXp, String newXpAsString) {
    this.newXp = newXp;
    this.newXpAsString = newXpAsString;
  }

  public String getSectionName() {
    return sectionName;
  }

  @PropertyDesc(i18n = I18N_OLD_BRANCHE_NAME, i18nBundleBaseName = "org/fudaa/fudaa/crue/planimetry/sig/traceprofil/Bundle.properties",
          readOnly = true)
  public String getOldBrancheName() {
    return oldBrancheName;
  }

  @PropertyDesc(i18n = I18N_OLD_XP, i18nBundleBaseName = "org/fudaa/fudaa/crue/planimetry/sig/traceprofil/Bundle.properties",
          readOnly = true)
  public String getOldXpAsString() {
    return oldXpAsString;
  }
}
