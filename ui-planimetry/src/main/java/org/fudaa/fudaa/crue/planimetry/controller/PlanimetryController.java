package org.fudaa.fudaa.crue.planimetry.controller;

import gnu.trove.TLongDoubleHashMap;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.SimplificationSeuilsProvider;
import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.projet.planimetry.LayerVisibility;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.fudaa.crue.common.ContainerActivityVisibility;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.config.ConfigDefaultValuesProvider;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.planimetry.PlanimetryCalqueState;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.action.PlanimetryEMHEditor;
import org.fudaa.fudaa.crue.planimetry.action.ProjectEMHOnSIGAction;
import org.fudaa.fudaa.crue.planimetry.action.SectionProfilAttachedToBrancheAction;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.layer.*;
import org.fudaa.fudaa.crue.planimetry.layout.*;
import org.fudaa.fudaa.crue.planimetry.listener.PlanimetrySceneEditorListener;

import java.awt.*;
import java.util.List;
import java.util.*;

/**
 * @author deniger
 */
public class PlanimetryController {
    public static final String NAME_GC_EXTERN = "gcExtern";
    public static final String NAME_GC_EXTERN_SIG = "gcSig";
    public static final String NAME_GC_EXTERN_IMAGES = "gcImages";
    public static final String NAME_GC_EXTERN_DRAW = "gcDraw";
    public static final String NAME_GC_EXTERN_TRACES = "gcTrace";
    public static final String NAME_GC_EXTERN_TRACES_CASIERS = "gcTraceCasier";
    public static final String NAME_GC_HYDRO = "gcHydro";
    public static final String NAME_CQ_NODE = "cqNode";
    public static final String NAME_CQ_BRANCHE = "cqBranche";
    public static final String NAME_CQ_TRACE = "cqTrace";
    public static final String NAME_CQ_SECTION = "cqSection";
    public static final String NAME_CQ_CLIMM = "cqCondLimite";
    public static final String NAME_CQ_CASIER = "cqCasier";
    final PlanimetryGisModelContainer gisModel;
    final CrueConfigMetier crueConfigMetier;
    PlanimetryControllerHelper helper;
    GroupHydraulicController groupHydraulicController;
    GroupExterneMainController groupExternController;
    PlanimetryVisuPanel visuPanel;
    final Map<String, LayerController> controllersByName = new HashMap<>();
    final PlanimetryCalqueState state = new PlanimetryCalqueState();
    PlanimetryEMHEditor emhEditor;
    ConfigDefaultValuesProvider defaultValuesProvider;
    SimplificationSeuilsProvider simplificationSeuilsProvider;
    ProjectEMHOnSIGAction projectBrancheOnLineAction;
    SectionProfilAttachedToBrancheAction sectionProfilToBrancheAction;
    VisuConfiguration visuConfiguration;
    /**
     * pour revenir en arriere si l'utilisateur fait cancel
     */
    VisuConfiguration saveConfiguration;
    boolean editable;
    PlanimetrySceneEditorListener sceneListener;
    ContainerActivityVisibility containerActivityVisibility;

    private NetworkPresetData networkPresetData;

    public PlanimetryController(PlanimetryGisModelContainer gisModelcontext, CrueConfigMetier crueConfigMetier,
                                VisuConfiguration configuration) {
        this.gisModel = gisModelcontext;
        this.crueConfigMetier = crueConfigMetier;
        this.visuConfiguration = configuration;
    }

    public CreationDefaultValue getDefaultValues() {
        return defaultValuesProvider == null ? new CreationDefaultValue() : defaultValuesProvider.getDefaultValues();
    }

    public SimplificationSeuilsProvider getSimplificationSeuilsProvider() {
        return simplificationSeuilsProvider;
    }

    public void setSimplificationSeuilsProvider(SimplificationSeuilsProvider simplificationSeuilsProvider) {
        this.simplificationSeuilsProvider = simplificationSeuilsProvider;
    }

    public ProjectEMHOnSIGAction getProjectEMHOnSIGAction() {
        if (projectBrancheOnLineAction == null) {
            projectBrancheOnLineAction = new ProjectEMHOnSIGAction(this);
        }
        return projectBrancheOnLineAction;
    }

    public SectionProfilAttachedToBrancheAction getTraceProfilAttachedToBrancheAction() {
        if (sectionProfilToBrancheAction == null) {
            sectionProfilToBrancheAction = new SectionProfilAttachedToBrancheAction(this);
        }
        return sectionProfilToBrancheAction;
    }

    public void setDefaultValuesProvider(ConfigDefaultValuesProvider defaultValuesProvider) {
        this.defaultValuesProvider = defaultValuesProvider;
    }

    public PlanimetryEMHEditor getEmhEditor() {
        return emhEditor;
    }

    public void setEmhEditor(PlanimetryEMHEditor emhEditor) {
        this.emhEditor = emhEditor;
    }

    public void apply(VisuConfiguration cloned) {
        visuConfiguration = cloned;
        Collection<LayerController> values = controllersByName.values();
        for (LayerController layerModelController : values) {
            layerModelController.setVisuConfiguration(cloned);
        }
    }

    public TLongDoubleHashMap getDistanceByBrancheUid() {
        return getBrancheController().getDistanceByBrancheUid();
    }

    public PlanimetryControllerHelper getHelper() {
        return helper;
    }

    public PlanimetryVisuPanel getVisuPanel() {
        return visuPanel;
    }

    void setVisuPanel(PlanimetryVisuPanel panel) {
        this.visuPanel = panel;

        sceneListener = new PlanimetrySceneEditorListener(this);
        visuPanel.getEditor().getSceneEditor().setEditorListener(sceneListener);
    }

    public GroupExterneMainController getGroupExternController() {
        return groupExternController;
    }

    public GroupHydraulicController getGroupHydraulicController() {
        return groupHydraulicController;
    }

    public LayerCasierController getCasierController() {
        return (LayerCasierController) controllersByName.get(NAME_CQ_CASIER);
    }

    public LayerCLimMController getcondLimiteController() {
        return (LayerCLimMController) controllersByName.get(NAME_CQ_CLIMM);
    }

    public LayerBrancheController getBrancheController() {
        return (LayerBrancheController) controllersByName.get(NAME_CQ_BRANCHE);
    }

    public LayerSectionController getSectionController() {
        return (LayerSectionController) controllersByName.get(NAME_CQ_SECTION);
    }

    public LayerTraceController getTraceController() {
        return (LayerTraceController) controllersByName.get(NAME_CQ_TRACE);
    }

    public LayerNodeController getNodeController() {
        return (LayerNodeController) controllersByName.get(NAME_CQ_NODE);
    }

    public PlanimetryBrancheLayer getCqBranche() {
        return getBrancheController().getLayer();
    }

    public PlanimetryNodeLayer getCqNoeud() {
        return getNodeController().getLayer();
    }

    public PlanimetryCasierLayer getCqCasier() {
        return getCasierController().getLayer();
    }

    public PlanimetryTraceLayer getCqTrace() {
        return getTraceController().getLayer();
    }

    public PlanimetrySectionLayer getCqSection() {
        return getSectionController().getLayer();
    }

    /**
     * ne doit etre appele qu'une fois
     */
    void initControllers() {
        Collection<LayerController> values = controllersByName.values();
        for (LayerController layerModelController : values) {
            layerModelController.init(helper);
            layerModelController.setEditable(false);
        }
    }

    public void saveDone() {
        getPlanimetryGisModelContainer().saveDone();
        Collection<LayerController> values = controllersByName.values();
        for (LayerController layerModelController : values) {
            layerModelController.saveDone();
        }
        saveConfiguration = null;
        visuPanel.repaint();
        state.clear();
    }

    public void cancel() {
        getPlanimetryGisModelContainer().cancel();
        visuPanel.clearSelection();
        if (saveConfiguration != null) {
            apply(saveConfiguration);
        }
        Collection<LayerController> values = controllersByName.values();
        for (LayerController layerModelController : values) {
            layerModelController.cancel();
        }
        visuPanel.getVueCalque().repaint(0);
        state.clear();
    }

    public VisuConfiguration getVisuConfiguration() {
        return visuConfiguration;
    }

    public void modify(VisuConfiguration cloned) {
        if (saveConfiguration == null) {
            saveConfiguration =visuConfiguration.copy();
        }
        apply(cloned);
        visuPanel.getVueCalque().repaint(0);
        visuPanel.repaint();
        getState().setStudyConfigModified();
    }

    public CrueConfigMetier getCrueConfigMetier() {
        return crueConfigMetier;
    }

    public PlanimetryGisModelContainer getPlanimetryGisModelContainer() {
        return gisModel;
    }

    public EMHScenario getScenario() {
        return gisModel.getScenario();
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        setEditable(editable, editable);
    }

    /**
     * @param editable    true si les calques autres que EMH doivent être editable
     * @param emhEditable true si les calques EMH doivent être editable
     */
    public void setEditable(boolean editable, boolean emhEditable) {
        this.editable = editable;
        Collection<LayerController> values = controllersByName.values();
        for (LayerController controller : values) {
            if (isEMHController(controller)) {
                controller.setEditable(emhEditable);
            } else {
                controller.setEditable(editable);
            }
        }
        visuPanel.setEditable(editable);
        getPlanimetryGisModelContainer().setAnglesEditable(emhEditable);
    }

    public PlanimetrySceneEditorListener getSceneListener() {
        return sceneListener;
    }

    public PlanimetryCalqueState getState() {
        return state;
    }

    void changeDone(LayerController controller) {
        state.setGeoLocModified();
    }

    void changeDoneInExternData(LayerController layerController) {
        state.setExternDataModified();
    }

    /**
     * Attention: les collections données doivent être cohérentes. Cette méthode ne fait pas de controles.
     */
    public void setModeles(GISZoneCollectionPoint nodes, GISZoneCollectionLigneBrisee branches, GISZoneCollectionLigneBrisee casiers) {
        setModeles(nodes, branches, casiers, true);
    }

    /**
     * Attention: les collections données doivent être cohérentes. Cette méthode ne fait pas de controles.
     */
    public void setModeles(GISZoneCollectionPoint nodes, GISZoneCollectionLigneBrisee branches, GISZoneCollectionLigneBrisee casiers,
                           boolean resetModifications) {
        if (resetModifications) {
            getNodeController().resetModification();
            getBrancheController().resetModification();
            getCasierController().resetModification();
        }
        nodes.setGeomModifiable(editable);
        getNodeController().setGeometryData(nodes);
        branches.setGeomModifiable(editable);
        getBrancheController().setGeometryData(branches);
        casiers.setGeomModifiable(editable);
        getCasierController().setGeometryData(casiers);
        getSectionController().reset();
        getTraceController().reset();
        visuPanel.repaint(0);
    }

    public void rebuildLayout() {
        CtuluDialogXYPanel pn = new CtuluDialogXYPanel();
        SysdocUrlBuilder.installHelpShortcut(pn, SysdocUrlBuilder.getDialogHelpCtxId("reconstruireReseau", PerspectiveEnum.MODELLING));
        boolean afficheModaleOk = pn.afficheModaleOk(null);
        if (afficheModaleOk) {
            redoLayout(pn.getXmin(), pn.getYmin());
        }
    }

    /**
     * Reconstruit le layout. Une sauvegarde est effectuée avant.
     *
     * @param xMin le xmin
     * @param yMin le ymin
     * @see NetworkBuilder#buildNewGisData(EMHScenario, VisuConfiguration, CrueConfigMetier, double, double)
     */
    public void redoLayout(double xMin, double yMin) {
        NetworkBuilder builder = new NetworkBuilder();
        //pour faire une sauvegarde:
        getNodeController().changeWillBeDone();
        getBrancheController().changeWillBeDone();
        getCasierController().changeWillBeDone();
        NetworkGisPositionnerResult buildNewGisData = builder.buildNewGisData(getScenario(), visuConfiguration, getCrueConfigMetier(), xMin, yMin);
        setModeles(buildNewGisData.getNodes(), buildNewGisData.getBranches(), buildNewGisData.getCasiers(), false);
        visuPanel.restaurer();
    }

    public void checkMinimalDistance() {
        CtuluDialogMinDistancePanel pn = new CtuluDialogMinDistancePanel(visuConfiguration.getMinimalDistanceBetweenNode());
        SysdocUrlBuilder.installHelpShortcut(pn, SysdocUrlBuilder.getDialogHelpCtxId("assurerDistanceMinimale", PerspectiveEnum.MODELLING));
        if (pn.afficheModaleOk(null)) {
            checkMinimalDistance(pn.getDistanceMini());
        }
    }

    private void checkMinimalDistance(double distanceMin) {
        //pour faire une sauvegarde:
        getNodeController().changeWillBeDone();
        getBrancheController().changeWillBeDone();
        getCasierController().changeWillBeDone();
        MinimalDistanceChecker checker = new MinimalDistanceChecker(this);
        checker.check(distanceMin);
        visuPanel.repaint(0);
    }

    /**
     * Annule les modifications en cours.
     *
     * @param loaded a recharger. Reconstruit le layout si nécessaire.
     */
    public void reload(EMHScenario loaded) {
        if(networkPresetData!=null){
            reload(loaded,networkPresetData.getNewPosition(),networkPresetData.getEmhNameToUpdate());
            networkPresetData=null;
            return;
        }
        boolean modified = new NetworkUpdater(this).reload(loaded);
        if (modified) {
            //les données de geolocalisation ont été modifiée:
            state.setGeoLocModified();
            visuPanel.restaurer();
        }
        visuPanel.getVueCalque().repaint(0);
    }

    /**
     * Annule les modifications en cours.
     *
     * @param loaded a recharger. Reconstruit le layout si nécessaire.
     */
    public void reload(EMHScenario loaded, NetworkGisPositionnerResult newPosition, Set<String> emhNameToUpdate) {
        final boolean modified = new NetworkUpdater(this).reload(loaded, newPosition, emhNameToUpdate);

        Runnable runnable = () -> {
            if (modified) {
                //les données de geolocalisation ont été modifiée:
                state.setGeoLocModified();
                visuPanel.restaurer();
            }
            visuPanel.getVueCalque().repaint(0);
            visuPanel.restaurer();
        };
        if (EventQueue.isDispatchThread()) {
            runnable.run();
        } else {
            EventQueue.invokeLater(runnable);
        }
    }

    /**
     * Ne pas utiliser. Simplement utilisé pour les test et PlanimetryLauncher
     *
     * @param loaded a recharger
     */
    public void validLoading(NetworkGisPositionnerResult loaded) {
        NetworkBuilder builder = new NetworkBuilder();
        boolean checkAllEMHPresent = builder.checkAllEMHPresent(getScenario(), loaded, visuConfiguration);
        getBrancheController().setGeometryData(loaded.getBranches());
        getCasierController().setGeometryData(loaded.getCasiers());
        getNodeController().setGeometryData(loaded.getNodes());
        visuPanel.repaint();
        visuPanel.restaurer();
    }

    public void calqueAdded(BCalque c) {
        controllersByName.put(c.getName(), ((PlanimetryAdditionalLayerContrat) c).getLayerController());
    }

    public void calqueRemoved(BCalque c) {
        controllersByName.remove(c.getName());
    }

    public LayerVisibility getLayerVisibility() {
        LayerVisibility res = new LayerVisibility();
        res.setBrancheVisible(getBrancheController().getLayer().isUserVisible());
        res.setCasierVisible(getCasierController().getLayer().isUserVisible());
        res.setNodeVisible(getNodeController().getLayer().isUserVisible());
        res.setSectionVisible(getSectionController().getLayer().isUserVisible());
        res.setTraceVisible(getTraceController().getLayer().isUserVisible());
        res.setCondLimitVisible(getcondLimiteController().getLayer().isUserVisible());
        getGroupExternController().fill(res);
        return res;
    }

    public void apply(LayerVisibility layerVisibility) {
        getBrancheController().getLayer().setUserVisible(layerVisibility.isBrancheVisible());
        getCasierController().getLayer().setUserVisible(layerVisibility.isCasierVisible());
        getNodeController().getLayer().setUserVisible(layerVisibility.isNodeVisible());
        getSectionController().getLayer().setUserVisible(layerVisibility.isSectionVisible());
        getTraceController().getLayer().setUserVisible(layerVisibility.isTraceVisible());
        getcondLimiteController().getLayer().setUserVisible(layerVisibility.isCondLimitVisible());
        getGroupExternController().apply(layerVisibility);
    }

    public boolean isEMHLayer(BCalque layer) {
        return getBrancheController().getLayer() == layer
                || getCasierController().getLayer() == layer
                || getNodeController().getLayer() == layer
                || getSectionController().getLayer() == layer
                || getTraceController().getLayer() == layer;
    }

    public List<BCalque> getMainLayers() {
        List<BCalque> res = new ArrayList<>();
        res.add(getBrancheController().getLayer());
        res.add(getSectionController().getLayer());
        res.add(getTraceController().getLayer());
        res.add(getNodeController().getLayer());
        res.add(getcondLimiteController().getLayer());
        res.add(getGroupExternController().groupExternLayer);
        res.add(getGroupHydraulicController().groupHydraulic);
        return res;
    }

    public ContainerActivityVisibility getContainerActivityVisibility() {
        return containerActivityVisibility;
    }

    public void setContainerActivityVisibility(ContainerActivityVisibility containerActivityVisibility) {
        this.containerActivityVisibility = containerActivityVisibility;
        getVisuPanel().getVueCalque().repaint();
    }

    public void setEditedCalcul(Long editedCalculUid) {
        getcondLimiteController().setEditedCalcul(editedCalculUid);
    }

    public void initWithAdditionnalLayers(List<PlanimetryAdditionalLayerContrat> additionnalLayer) {
        getGroupExternController().initWithAdditionnalLayers(additionnalLayer);
    }

    public void reloadAdditionLayersFrom(PlanimetryController controller) {
        Map<String, Boolean> visibilityForExternLayer = getVisibilityForExternLayer();
        //les 2 groupes doivent avoir la même structure.
        List<AbstractGroupExternController> otherGroups = controller.getGroupExternController().getSubGroups();
        List<AbstractGroupExternController> myGroups = getGroupExternController().getSubGroups();
        assert otherGroups.size() == myGroups.size();
        for (int i = 0; i < otherGroups.size(); i++) {
            myGroups.get(i).resetFrom(otherGroups.get(i));
        }
        //la visibilité n'est pas partagée:
        applyExternLayerVisibilty(visibilityForExternLayer);
    }

    public Map<String, Boolean> getVisibilityForExternLayer() {
        return getGroupExternController().getVisibilityForExternLayer();
    }

    public void applyExternLayerVisibilty(Map<String, Boolean> visibilityByExternLayerName) {
        getGroupExternController().applyExternLayerVisibilty(visibilityByExternLayerName);
    }

    private boolean isEMHController(LayerController controller) {
        return controller instanceof LayerControllerEMH || controller instanceof LayerCLimMController;
    }

    public void savePreset(NetworkPresetData presetData) {
        this.networkPresetData = presetData;
    }
}
