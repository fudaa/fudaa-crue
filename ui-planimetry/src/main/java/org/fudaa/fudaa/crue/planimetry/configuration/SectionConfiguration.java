package org.fudaa.fudaa.crue.planimetry.configuration;

import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryPointLayerModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetrySectionLayerModel;

import javax.swing.*;
import java.awt.*;
import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;

/**
 * @author deniger
 */
public class SectionConfiguration extends PointConfiguration {

  private final VisuConfiguration parent;
  final Map<EnumSectionType, Color> colorBySectionType = new EnumMap<>(EnumSectionType.class);

  private void fillDefaultColor() {
    EnumSectionType[] values = EnumSectionType.values();
    for (EnumSectionType enumSectionType : values) {
      colorBySectionType.put(enumSectionType, Color.BLACK);
    }
  }

  public Map<EnumSectionType, Color> getColorBySectionType() {
    return Collections.unmodifiableMap(colorBySectionType);
  }

  public Color getColor(EnumSectionType type) {
    return colorBySectionType.get(type);
  }

  public void setColor(EnumSectionType type, Color color) {
    colorBySectionType.put(type, color);
  }

  public SectionConfiguration(VisuConfiguration parent) {
    labelConfiguration.setLabelAlignment(SwingConstants.WEST);
    iconModel.setTypeLigne(TraceLigne.LISSE);
    iconModel.setEpaisseurLigne(1f);
    iconModel.setType(TraceIcon.CARRE_ARRONDI);
    iconModel.setBackgroundColor(Color.WHITE);
    iconModel.setTaille(4);
    labelConfiguration.setLabelDistance(5);
    this.parent = parent;
    fillDefaultColor();
  }

  public SectionConfiguration(SectionConfiguration from, final VisuConfiguration parent) {
    super(from);
    if (from != null) {
      colorBySectionType.putAll(from.colorBySectionType);
    }
    this.parent = parent;
  }

  @Override
  public SectionConfiguration copy() {
    return new SectionConfiguration(this, parent);
  }

  public VisuConfiguration getParent() {
    return parent;
  }

  public boolean isInactiveEMHVisible() {
    return parent.isInactiveEMHVisible();
  }

  private Color getColor(CatEMHSection section) {
    if (!section.getActuallyActive()) {
      return parent.getInactiveEMHColor();
    }
    return colorBySectionType.get(section.getSectionType());
  }

  @Override
  public void initTraceIcon(TraceIconModel ligne, int idxPoint, PlanimetryPointLayerModel model) {
    PlanimetrySectionLayerModel sectionModel = (PlanimetrySectionLayerModel) model;
    super.initTraceIcon(ligne, idxPoint, model);
    CatEMHSection section = sectionModel.getLayerController().getSection(idxPoint);
    ligne.setCouleur(getColor(section));
    if (sectionModel.getSectionConfigurationExtra() != null) {
      sectionModel.getSectionConfigurationExtra().decorateTraceIcon(section, ligne, sectionModel, this);

    }
  }

  public String getDisplayName(CatEMHSection emh, PlanimetrySectionLayerModel modeleDonnees, boolean isSelected) {
    String nom = null;
    if (modeleDonnees.getSectionConfigurationExtra() != null) {
      nom = modeleDonnees.getSectionConfigurationExtra().getDisplayName(emh, modeleDonnees, this, isSelected);
    }
    if (nom == null) {
      nom = emh.getNom();
    }
    return nom;
  }

  public boolean canLabelsBeAggregate(PlanimetrySectionLayerModel modeleDonnees) {
    if (modeleDonnees.getSectionConfigurationExtra() != null) {
      return modeleDonnees.getSectionConfigurationExtra().canLabelsBeAggregate();
    }
    return true;
  }
}
