/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.save;

import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternAdditionalFileSaver;
import org.fudaa.dodico.crue.io.conf.CrueEtudeExternRessourceInfos;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.openide.util.Exceptions;

import java.io.File;

/**
 * @author Frederic Deniger
 */
public class GISAdditionalFileSaver implements CrueEtudeExternAdditionalFileSaver {
  private final CrueEtudeExternRessourceInfos infos;
  private final GISZoneCollection zone;

  public GISAdditionalFileSaver(CrueEtudeExternRessourceInfos infos, GISZoneCollection zone) {
    this.infos = infos;
    this.zone = zone;
  }

  @Override
  public void save(File configDir, File dessinDir) {
    if (zone.getNumGeometries() == 0) {
      return;
    }
    File targetFile = new File(dessinDir, infos.getId() + SigFormatHelper.getExtension(SigFormatHelper.EnumFormat.SHP));
    PlanimetryGISGMLZoneExporter exporter = new PlanimetryGISGMLZoneExporter();
    final ShapefileDataStoreFactory fac = new ShapefileDataStoreFactory();
    try {
      exporter.process(null, zone, fac.createDataStore(targetFile.toURI().toURL()));
    } catch (Exception ex) {
      Exceptions.printStackTrace(ex);
    } finally {
    }
  }
}
