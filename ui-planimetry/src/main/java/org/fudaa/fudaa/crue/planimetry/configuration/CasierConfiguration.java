package org.fudaa.fudaa.crue.planimetry.configuration;


import java.awt.*;
import javax.swing.SwingConstants;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryCasierLayerModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLigneBriseeLayerModel;

/**
 *
 * @author deniger
 */
public class CasierConfiguration extends AbstractLigneBriseeConfiguration {

  public static final String PROPERTY_FOND_PAINTED = "fondPainted";
  public static final String PROPERTY_FOND = "fond";
  TraceLigneModel lineForCasier = new TraceLigneModel(TraceLigne.LISSE, 1f, Color.BLACK);//la couleur pour le contour
  //dessin des point du casier si présent.
  TraceIconModel iconForAtomicIcon = new TraceIconModel(TraceIcon.CARRE, 4, Color.BLACK);
  TraceIconModel icon = new TraceIconModel(TraceIcon.CARRE, 7, Color.BLACK);
  LabelConfiguration labelConfiguration = new LabelConfiguration();
  Color fond = Color.ORANGE.darker();
  boolean fondPainted = true;
  private final VisuConfiguration parent;

  public CasierConfiguration(final VisuConfiguration parent) {
    labelConfiguration.setLabelFont(VisuConfiguration.deriveFont(labelConfiguration.getLabelFont(), Font.BOLD, 2));
    labelConfiguration.setLabelAlignment(SwingConstants.EAST);
    transparenceAlpha = 64;
    this.parent = parent;
  }

  public CasierConfiguration(CasierConfiguration from, final VisuConfiguration parent) {
    super(from);
    if (from != null) {
      this.lineForCasier = new TraceLigneModel(lineForCasier);
      this.iconForAtomicIcon = new TraceIconModel(from.iconForAtomicIcon);
      this.icon = new TraceIconModel(from.icon);
      this.labelConfiguration = from.labelConfiguration.copy();
      this.fond = from.fond;
      this.fondPainted = from.fondPainted;
    }
    this.parent = parent;
  }

  @Override
  public CasierConfiguration copy() {
    return new CasierConfiguration(this,parent);
  }

  public boolean isInactiveEMHVisible() {
    return parent.isInactiveEMHVisible();
  }

  public void setFond(Color fond) {
    this.fond = fond;
  }

  public boolean isFondPainted() {
    return fondPainted;
  }

  public void setFondPainted(boolean fondPainted) {
    this.fondPainted = fondPainted;
  }

  public Color getFond() {
    return fond;
  }

  public Color getFond(CatEMHCasier casier, PlanimetryCasierLayerModel casierModel) {
    if (casierModel.getCasierConfigurationExtra() != null) {
      Color res = casierModel.getCasierConfigurationExtra().getFond(casier, casierModel);
      if (res != null) {
        return res;
      }
    }
    return fond;
  }

  @Override
  public void initTraceAtomicIcon(TraceIconModel traceIconModel, int idxPoly, PlanimetryLigneBriseeLayerModel model) {
    traceIconModel.updateData(iconForAtomicIcon);
  }

  @Override
  public void initTraceLigne(TraceLigneModel ligne, int idxPoly, PlanimetryLigneBriseeLayerModel model) {
    CatEMHCasier casier = ((PlanimetryCasierLayerModel) model).getLayerController().getCasier(idxPoly);
    ligne.updateData(lineForCasier);
    if (!casier.getActuallyActive()) {
      ligne.setCouleur(parent.getInactiveEMHColor());
    }
    PlanimetryCasierLayerModel casierModel = (PlanimetryCasierLayerModel) model;
    if (casierModel.getCasierConfigurationExtra() != null) {
      casierModel.getCasierConfigurationExtra().decoreTraceLigne(ligne, casier, casierModel, this);
    }
  }

  public LabelConfiguration getLabelConfiguration() {
    return labelConfiguration;
  }

  public int getMinimumSize() {
    return parent.getNodeConfiguration().getDiameter() + 5;
  }

  public void initCasierAsIcon(TraceIconModel modelIcon, int idxPoly, PlanimetryLigneBriseeLayerModel model) {
    final PlanimetryCasierLayerModel casierModel = (PlanimetryCasierLayerModel) model;
    CatEMHCasier casier = (casierModel).getLayerController().getCasier(idxPoly);
    modelIcon.updateData(icon);
    modelIcon.setEpaisseurLigne(1f);
    modelIcon.setTypeLigne(lineForCasier.getTypeTrait());
    modelIcon.setCouleur(lineForCasier.getCouleur());
    modelIcon.setBackgroundColor(fond);
    modelIcon.setBackgroundColorPainted(fondPainted);
    if (!casier.getActuallyActive()) {
      modelIcon.setCouleur(parent.getInactiveEMHColor());
    }
    if (casierModel.getCasierConfigurationExtra() != null) {
      casierModel.getCasierConfigurationExtra().decoreTraceIcon(modelIcon, casier, casierModel, this);
    }

  }

  public String getDisplayName(CatEMHCasier casier, PlanimetryCasierLayerModel casierModel, boolean isSelected) {
    String nom = null;
    if (casierModel.getCasierConfigurationExtra() != null) {
      nom = casierModel.getCasierConfigurationExtra().getDisplayName(casier, casierModel, isSelected, this);
    }
    if (nom == null) {
      nom = casier.getNom();
    }
    return nom;
  }
}
