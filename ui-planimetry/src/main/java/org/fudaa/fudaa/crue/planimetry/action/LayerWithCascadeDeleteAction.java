package org.fudaa.fudaa.crue.planimetry.action;

/**
 *
 * @author deniger
 */
public interface LayerWithCascadeDeleteAction {
  
  void removeCascadeSelectedObjects();
  
  boolean isEditable();

}
