package org.fudaa.fudaa.crue.planimetry.configuration;

import java.awt.Color;
import java.util.Arrays;
import javax.swing.SpinnerNumberModel;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.crue.common.config.ConfigurationInfoHelper;
import org.fudaa.fudaa.crue.common.editor.SpinnerInplaceEditor;
import org.fudaa.fudaa.crue.common.editor.TypeIconJcomboBoxInplaceEditor;
import org.fudaa.fudaa.crue.common.editor.TypeIconPropertyEditorSupport;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class TraceIconModelConfigurationInfo {

  public static Sheet.Set createSet(TraceIconModel in, String prefix) {
    return createSet(in, prefix, true);
  }

  public static Sheet.Set createSet(TraceIconModel in, String prefix, boolean addColor) {
    Sheet.Set set = Sheet.createPropertiesSet();
    set.setDisplayName(NbBundle.getMessage(LabelConfigurationInfo.class, "IconConfiguration.DisplayName"));
    set.setShortDescription(set.getDisplayName());
    set.setName(prefix + ".icon");
    try {
      set.put(createType(in));
      set.put(createTaille(in));
      set.put(createEpaisseurLigne(in));
      if (addColor) {
        set.put(createForegroundColor(in));
        set.put(createBackgroundColor(in));
        set.put(createBackgroundColorPainted(in));
      }
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return set;
  }

  private static PropertySupport.Reflection createType(TraceIconModel in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(TraceIconModel.PROPERTY_TYPE,
            Integer.TYPE, in, "iconType", VisuConfigurationInfo.class);
    res.setPropertyEditorClass(TypeIconPropertyEditorSupport.class);
    final TypeIconJcomboBoxInplaceEditor typeIconJComboBoxInplaceEditor = new TypeIconJcomboBoxInplaceEditor(
            Arrays.asList(TraceIcon.RIEN, TraceIcon.CARRE, TraceIcon.CARRE_ARRONDI,
            TraceIcon.CERCLE, TraceIcon.LOSANGE,
            TraceIcon.CROIX));
    res.setValue("inplaceEditor", typeIconJComboBoxInplaceEditor);
    return res;
  }

  private static PropertySupport.Reflection createTaille(TraceIconModel in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(TraceIconModel.PROPERTY_TAILLE,
            Integer.TYPE, in, "iconTaille", VisuConfigurationInfo.class);
    SpinnerInplaceEditor inplaceEditor = new SpinnerInplaceEditor(new SpinnerNumberModel(1, 1, 100, 1));
    res.setValue("inplaceEditor", inplaceEditor);
    return res;
  }

  private static PropertySupport.Reflection createEpaisseurLigne(TraceIconModel in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(TraceIconModel.PROPERTY_EPAISSEUR_LIGNE,
            Float.TYPE, in, "iconEpaisseurLigne", VisuConfigurationInfo.class);
    SpinnerInplaceEditor inplaceEditor = new SpinnerInplaceEditor(new SpinnerNumberModel(Float.valueOf(1.5f), Float.valueOf(1f),
            Float.valueOf(100f), Float.valueOf(0.5f)));
    res.setValue("inplaceEditor", inplaceEditor);
    return res;
  }

  private static PropertySupport.Reflection createForegroundColor(TraceIconModel in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(TraceIconModel.PROPERTY_COULEUR,
            Color.class, in, "iconCouleurForeground", VisuConfigurationInfo.class);
    return res;
  }

  private static PropertySupport.Reflection createBackgroundColor(TraceIconModel in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(TraceIconModel.PROPERTY_BACKGROUND_COLOR,
            Color.class, in, "iconCouleurBackground", VisuConfigurationInfo.class);
    return res;
  }

  private static PropertySupport.Reflection createBackgroundColorPainted(TraceIconModel in) throws NoSuchMethodException {
    PropertySupport.Reflection res = ConfigurationInfoHelper.create(TraceIconModel.PROPERTY_BACKGROUND_COLOR_PAINTED,
            Boolean.TYPE, in, "iconCouleurBackgroundPainted", VisuConfigurationInfo.class);
    return res;
  }
}
