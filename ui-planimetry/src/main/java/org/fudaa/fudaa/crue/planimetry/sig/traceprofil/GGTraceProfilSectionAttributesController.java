/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.traceprofil;

import gnu.trove.TIntObjectHashMap;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeString;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternDrawLayerGroup;
import org.fudaa.fudaa.crue.planimetry.sig.GGAttributeDouble;
import org.fudaa.fudaa.crue.planimetry.sig.GGAttributeString;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class GGTraceProfilSectionAttributesController {

  public final static GISAttributeString NOM = new GGAttributeString(CtuluLib.getS("Nom"), "PROFIL", false);
  public final static GGAttributeDouble XT = new GGAttributeDouble(NbBundle.getMessage(GGTraceProfilSectionAttributesController.class,
          "attribute.AbsCurviligneName"), "ABS_CURV", true);
  public final static GGAttributeDouble Z = new GGAttributeDouble(NbBundle.getMessage(GGTraceProfilSectionAttributesController.class,
          "attribute.ZName"), "Z", true);
  public final static GISAttributeString LIM_LIT = new GGAttributeString(NbBundle.getMessage(GGTraceProfilSectionAttributesController.class,
          "attribute.LimLit"), "LIM_LIT", true);
  public final static GISAttributeString LIM_GEOM = new GGAttributeString(NbBundle.getMessage(GGTraceProfilSectionAttributesController.class,
          "attribute.LimGeo"), "LIM_GEO", true);
  final TIntObjectHashMap<String> limLitById = LimLitHelper.createMap();
  final TIntObjectHashMap<String> limGeoById = LimGeoHelper.createMap();

  GISAttributeInterface[] createSectionAttributes() {
    //Attention l'ordre doit être identique a createObject
    return new GISAttributeInterface[]{NOM, XT, Z, LIM_LIT, LIM_GEOM, PlanimetryExternDrawLayerGroup.DESSINS_LABEL_LIGNES};
  }

  Object[] createObject(String nom, List<TraceProfilContentItem> item) {
    final int size = item.size();
    Double[] xts = new Double[size];
    Double[] zs = new Double[size];
    String[] limLit = new String[size];
    String[] limGeom = new String[size];
    String[] labels = new String[size];
    for (int i = 0; i < size; i++) {
      xts[i] = item.get(i).xt;
      zs[i] = item.get(i).z;
      limLit[i] = limitLitsToString(item.get(i).getLimLits());
      limGeom[i] = limitGeosToString(item.get(i).getLimGeom());
      String label = limLit[i];
      if (StringUtils.isNotBlank(limGeom[i])) {
        if (StringUtils.isNotBlank(label)) {
          label = label + "; " + limGeom[i];
        } else {
          label = limGeom[i];
        }
      }
      labels[i] = label;

    }
    //Attention l'ordre doit être identique a createSectionAttributes
    return new Object[]{nom, xts, zs, limLit, limGeom, labels};
  }

  private String limitLitsToString(int[] limLit) {
    return LimLitHelper.getString(limLit, limLitById);
  }

  private String limitGeosToString(int[] limGeom) {
    return LimLitHelper.getString(limGeom, limGeoById);
  }
}
