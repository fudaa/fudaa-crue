/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.find;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.commun.EbliSelectionState;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.find.EbliFindComponent;
import org.fudaa.ebli.find.EbliFindable;
import org.fudaa.ebli.find.EbliFindableItem;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLayerWithEMHContrat;

import java.util.List;
import java.util.Set;

/**
 * @author Frederic Deniger
 */
public class PlanimetryFindEMHLayerAction implements EbliFindActionInterface {
    final PlanimetryLayerWithEMHContrat layer;

    public PlanimetryFindEMHLayerAction(PlanimetryLayerWithEMHContrat layer) {
        this.layer = layer;
    }

    /**
     * @param layer         le calque
     * @param initName      si  vide, ne sera pas pris en compte dans la sélection
     * @param subCategories si null, ne sera pas pris en compte dans la sélection
     * @return la selection
     */
    protected static CtuluListSelection findSelection(PlanimetryLayerWithEMHContrat layer, String initName, Set subCategories) {
        int nombre = layer.modeleDonnees().getNombre();
        String name = initName;
        if (name != null) {
            name = name.toUpperCase();
        }
        CtuluListSelection newSelection = new CtuluListSelection();
        for (int i = 0; i < nombre; i++) {
            EMH emhFromLayerPosition = layer.getLayerControllerEMH().getEMHFromLayerPosition(i);
            final String nom = emhFromLayerPosition.getNom().toUpperCase();
            boolean isSelectedByName = true;
            if (StringUtils.isNotBlank(name)) {
                isSelectedByName = nom.contains(name);
            }
            boolean add = false;
            if (subCategories == null) {
                add = isSelectedByName;
            } else {
                add = isSelectedByName && subCategories.contains(emhFromLayerPosition.getSubCatType());
            }
            if (add) {
                newSelection.add(i);
            }
        }
        return newSelection;
    }

    @Override
    public boolean find(Object _s, String _findId, int _selOption, EbliFindable _parent) {
        if (_selOption == EbliSelectionState.ACTION_AND || _selOption == EbliSelectionState.ACTION_REPLACE) {
            PlanimetryVisuPanel pn = (PlanimetryVisuPanel) _parent;
            BCalque[] tousCalques = pn.getDonneesCalque().getTousCalques();
            for (BCalque bCalque : tousCalques) {
                if (bCalque != layer && bCalque instanceof ZCalqueAffichageDonneesInterface) {
                    ((ZCalqueAffichageDonneesInterface) bCalque).clearSelection();
                }
            }
        }
        FindQuery query = (FindQuery) _s;
        CtuluListSelection newSelection = findSelection(layer, query.getTextToFind(), query.getTypes());
        ((ZCalqueAffichageDonnees) layer).changeSelection(newSelection, _selOption);
        ((PlanimetryVisuPanel) _parent).getScene().fireSelectionEvent();
        return newSelection != null && !newSelection.isEmpty();
    }

    @Override
    public String erreur(Object _s, String _findId, EbliFindable _parent) {
        return null;
    }

    @Override
    public EbliFindComponent createFindComponent(EbliFindable _parent) {
        List<ToStringInternationalizable> options = layer.getLayerControllerEMH().getCategories();
        PlanimetryFindComponent res = new PlanimetryFindComponent();
        res.setOptions(options);
        return res;
    }

    @Override
    public boolean isEditableEnable(String _searchId, EbliFindable _parent) {
        return layer.getLayerController().isEditable();
    }

    @Override
    public GrBoite getZoomOnSelected() {
        return layer.getZoomOnSelected();
    }

    @Override
    public EbliFindableItem getCalque() {
        return layer;
    }

    @Override
    public String editSelected(EbliFindable _parent) {
        return layer.editSelected();
    }
}
