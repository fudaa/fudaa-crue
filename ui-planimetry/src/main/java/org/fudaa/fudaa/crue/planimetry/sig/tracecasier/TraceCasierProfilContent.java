/*
 GPL 2
 */
package org.fudaa.fudaa.crue.planimetry.sig.tracecasier;

import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.config.ccm.DoubleEpsilonComparator;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

/**
 * @author Frederic Deniger
 */
public class TraceCasierProfilContent implements Comparable<TraceCasierProfilContent> {
    final String name;
    private final TreeMap<Double, TraceCasierProfilContentItem> xtSet;
    double distance;
    private final List<TraceCasierProfilContentItem> items = new ArrayList<>();

    public TraceCasierProfilContent(String name, final PropertyEpsilon xtEpsilon) {
        this.name = name;
        this.xtSet = new TreeMap<>(new DoubleEpsilonComparator(xtEpsilon));
    }

    List<TraceCasierProfilContentItem> getSortedItems() {
        Collections.sort(items);
        return Collections.unmodifiableList(items);
    }

    /**
     * @param newItem le nouvel item
     * @return false si le xt est déjà présent.
     */
    public boolean add(TraceCasierProfilContentItem newItem) {
        TraceCasierProfilContentItem item = xtSet.get(newItem.getXt());
        if (item == null) {
            xtSet.put(newItem.getXt(), newItem);
            items.add(newItem);
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(TraceCasierProfilContent o) {
        if (o == null) {
            return 1;
        }
        return SafeComparator.compareString(name, o.name);
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    String getXtAsString() {
        StringBuilder builder = new StringBuilder();
        for (TraceCasierProfilContentItem traceCasierProfilContentItem : items) {
            if (builder.length() > 0) {
                builder.append(';');
            }
            builder.append(traceCasierProfilContentItem.getXt());
        }
        return builder.toString();
    }

    String getZAsString() {
        StringBuilder builder = new StringBuilder();
        for (TraceCasierProfilContentItem traceCasierProfilContentItem : items) {
            if (builder.length() > 0) {
                builder.append(';');
            }
            builder.append(traceCasierProfilContentItem.getZ());
        }
        return builder.toString();
    }

    Object getLimLitAsString() {
        StringBuilder builder = new StringBuilder();
        for (TraceCasierProfilContentItem traceCasierProfilContentItem : items) {
            if (builder.length() > 0) {
                builder.append(';');
            }
            builder.append(traceCasierProfilContentItem.getLimLit());
        }
        return builder.toString();
    }
}
