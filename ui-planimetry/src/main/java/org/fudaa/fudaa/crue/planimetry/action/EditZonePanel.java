package org.fudaa.fudaa.crue.planimetry.action;

import java.awt.Component;
import java.awt.FlowLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluLibArray;

/**
 *
 * @author deniger
 */
public class EditZonePanel implements EditZone {

  final JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));

  public JPanel getPanel() {
    return panel;
  }

  @Override
  public void active(JComponent jc) {
    Component[] components = panel.getComponents();
      if (CtuluLibArray.containsObject(components, jc)) {
      return;
    }
    if (jc != null) {
      panel.add(jc);
    }
    panel.revalidate();
  }

  @Override
  public void unactive(JComponent jc) {
    if (jc == null) {
      return;
    }
    Component[] components = panel.getComponents();
    if (!CtuluLibArray.containsObject(components, jc)) {
      return;
    }
    panel.remove(jc);
    panel.revalidate();
  }
}
