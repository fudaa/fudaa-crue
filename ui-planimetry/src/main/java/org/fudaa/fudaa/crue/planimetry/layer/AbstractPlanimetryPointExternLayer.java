package org.fudaa.fudaa.crue.planimetry.layer;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.crue.planimetry.configuration.PointConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.node.AdditionalConfigurationNode;
import org.fudaa.fudaa.crue.planimetry.configuration.node.PointConfigurationNode;
import org.fudaa.fudaa.crue.planimetry.controller.LayerPointExternController;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 *
 * @author deniger
 */
public abstract class AbstractPlanimetryPointExternLayer extends PlanimetryPointLayer<PointConfiguration> implements PlanimetryAdditionalLayerContrat<PointConfiguration> {

  private final LabelPainter labelPainter = new LabelPainter();

  public AbstractPlanimetryPointExternLayer(PlanimetryPointLayerModel _modele, FSigEditor _editor) {
    super(_modele, _editor);
    this.layerConfiguration = new PointConfiguration();
  }

  @Override
  public void installed() {
    final FSigEditor editor = (FSigEditor) getEditor();
    final EbliActionSimple exportAction = editor.getExportAction();
    setActions(new EbliActionInterface[]{exportAction});
  }

  @Override
  public LayerPointExternController getLayerController() {
    return (LayerPointExternController) super.getLayerController();
  }

  @Override
  public AdditionalConfigurationNode createConfigurationNode() {
    return new PointConfigurationNode(layerConfiguration, this);
  }

  public void setLayerConfigurationNoUndo(PointConfiguration layerConfiguration) {
    this.layerConfiguration = layerConfiguration;
    firePropertyChange(PROPERTY_LAYER_CONFIGURATION, null, layerConfiguration);
    repaint();
  }

  @Override
  protected void paintLabelsOnAtomics(Graphics2D _g, GrMorphisme _versEcran, GrMorphisme _versReel, GrBoite _clipReel) {
    if (layerConfiguration.getLabelConfiguration().isDisplayLabels()) {
      super.paintLabelsOnAtomics(_g, _versEcran, _versReel, _clipReel);
    }
  }

  @Override
  protected void drawLabel(FontMetrics fm, String s, GrPoint p, Color bgColor, Graphics2D _g, Color fgColor) {
    labelPainter.paintLabels(_g, p, s, layerConfiguration.getLabelConfiguration(), alpha_);
  }

  @Override
  public void setLayerConfiguration(PointConfiguration layerConfiguration) {
    getLayerController().changeWillBeDone();
    setLayerConfigurationNoUndo(layerConfiguration);
  }
}
