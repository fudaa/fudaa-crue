package org.fudaa.fudaa.crue.planimetry.save;

import java.io.File;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.fudaa.crue.planimetry.action.SigFormatHelper;
import org.fudaa.fudaa.crue.planimetry.controller.ExternLayerFactory;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.layer.AbstractPlanimetryLigneBriseeExternLayer;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryExternTraceProfilLayer;
import org.fudaa.fudaa.crue.planimetry.sig.traceprofil.GGTraceProfilSectionLoader;
import org.fudaa.fudaa.sig.FSigGeomSrcData;

/**
 * Pour etre pris compte doit être ajoute dans la liste AdditionalLayerLoaderProcess
 *
 * @author deniger
 */
public class LayerTraceProfilsControllerSaver extends AbstractLayerTraceControllerSaver {

  public LayerTraceProfilsControllerSaver() {
    super(PlanimetryController.NAME_GC_EXTERN_TRACES, ConfigExternIds.LAYER_TRACES_PROFILS_SAVE_ID, "TraceProfils");
  }

  @Override
  protected AbstractPlanimetryLigneBriseeExternLayer createLayer(FSigGeomSrcData result, File file,
          Pair<FSigGeomSrcData, SigFormatHelper.EnumFormat> pair) {
    GISZoneCollectionLigneBrisee lignes = GGTraceProfilSectionLoader.buildLine(result);
    PlanimetryExternTraceProfilLayer layer = ExternLayerFactory.createLayerTraceProfil(lignes, file,
            pair.second,
            null);
    return layer;
  }
}
