/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;

/**
 * La description d'une expression: nom et id.
 *
 * @author Frederic Deniger
 */
public abstract class FormuleParameters implements Cloneable, ObjetWithID {
    @XStreamAlias("Nom")
    private String name;

    public abstract String getDescription();

    @Override
    public String getId() {
        return name;
    }

    public void setId(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FormuleParameters other = (FormuleParameters) obj;
        return StringUtils.equals(name, other.name) && isContentEqual(other);
    }

    @Override
    public String getNom() {
        return name;
    }

    public void setDisplayName(String displayName) {
        setId(displayName);
    }

    @Override
    public FormuleParameters clone() {
        try {
            return (FormuleParameters) super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        throw new IllegalAccessError("why ?");
    }

    public abstract boolean isContentEqual(FormuleParameters other);

    public abstract FormuleContent createContent(ReportGlobalServiceContrat service);
}
