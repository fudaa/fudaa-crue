/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.calcul;

import java.util.Map;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.projet.create.RunCreatorOptions;

/**
 *
 * @author deniger
 */
public class CalculCrue10Runner implements CalculCrueRunner {

  private final CoeurConfigContrat coeurConfig;
  private final Map<CrueFileType, String> execOptions;

  public CalculCrue10Runner(final CoeurConfigContrat coeurConfig, final Map<CrueFileType, String> execOptions) {
    this.coeurConfig = coeurConfig;
    this.execOptions = execOptions;
  }

  @Override
  public CalculCrueContrat createRunner(final ExecInput execInput, final ExecConfigurer configurer) {
    final Crue10Exec exec = new Crue10Exec(coeurConfig, execInput);
    if (configurer != null) {
      configurer.configure(exec);
    } else {
      final Crue10OptionBuilder optionsBuilder = new Crue10OptionBuilder(execOptions);
      final ExecConfigurer defaultConfigurer = optionsBuilder.createConfigurer(execInput, RunCreatorOptions.createDefaultMap());
      defaultConfigurer.configure(exec);
    }
    return exec;
  }
}
