package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateSourceTargetData;

import java.util.ArrayList;
import java.util.List;

public class SmUpdateDataLostInBrancheIdentifier {
  private final CrueConfigMetier ccm;

  public SmUpdateDataLostInBrancheIdentifier(CrueConfigMetier ccm) {
    this.ccm = ccm;
  }

  public CtuluIOResult<List<SmUpdateSourceTargetData.Line<CatEMHBranche>>> generateLogsAndExtractNotLastData(List<SmUpdateSourceTargetData.Line<CatEMHBranche>> sourcesToImport) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("sm.updater.BrancheslostLog");
    List<SmUpdateSourceTargetData.Line<CatEMHBranche>> notModified = new ArrayList<>();
    sourcesToImport.forEach(emh -> checkBranche(emh, log, notModified));
    return new CtuluIOResult<>(log, notModified);
  }

  private void checkBranche(SmUpdateSourceTargetData.Line<CatEMHBranche> brancheLine, CtuluLog log, List<SmUpdateSourceTargetData.Line<CatEMHBranche>> notModified) {
    final CatEMHBranche targetToModify = brancheLine.getTargetToModify();
    final EnumBrancheType targetBrancheType = targetToModify.getBrancheType();

    final EnumBrancheType sourceBrancheType = brancheLine.getSourceToImport().getBrancheType();
    boolean lostData = false;
    if (SmUpdateSeuilHelper.isSeuil(targetBrancheType) && sourceBrancheType.equals(targetBrancheType)) {
      SmUpdateSeuilHelper helper = new SmUpdateSeuilHelper(ccm);
      if (helper.willCoefBeChanged(brancheLine)) {
        lostData = true;
        log.addWarn("sm.updater.brancheSeuilLost", targetToModify.getNom(), targetBrancheType.geti18n());
      }
    }
    if (!lostData) {
      notModified.add(brancheLine);
    }
  }
}
