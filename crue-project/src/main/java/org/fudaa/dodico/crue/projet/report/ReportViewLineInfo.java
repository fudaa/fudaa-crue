/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.joda.time.LocalDateTime;

/**
 * Les informations au sujet d'une ligne
 * @author Frederic Deniger
 */
@XStreamAlias("VueConfig")
public class ReportViewLineInfo implements Cloneable {

  @XStreamAlias("Nom")
  private String nom;
  @XStreamAlias("Commentaire")
  private String commentaire;
  @XStreamAlias("Filename")
  private String filename;
  @XStreamAlias("AuteurCreation")
  private String auteurCreation;
  @XStreamAlias("DateCreation")
  private LocalDateTime dateCreation;
  @XStreamAlias("AuteurDerniereModif")
  private String auteurDerniereModif;
  @XStreamAlias("DateDerniereModif")
  private LocalDateTime dateDerniereModif;

  public String getFilename() {
    return filename;
  }

  public String getNom() {
    return nom;
  }


  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getAuteurCreation() {
    return auteurCreation;
  }

  public String getCommentaire() {
    return commentaire;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public void setAuteurCreation(String auteurCreation) {
    this.auteurCreation = auteurCreation;
  }

  public void setDateCreation(LocalDateTime dateCreation) {
    this.dateCreation = dateCreation;
  }

  public void setAuteurDerniereModif(String auteurDerniereModif) {
    this.auteurDerniereModif = auteurDerniereModif;
  }

  public void setDateDerniereModif(LocalDateTime dateDerniereModif) {
    this.dateDerniereModif = dateDerniereModif;
  }

  @Override
  protected ReportViewLineInfo clone() {
    try {
      return (ReportViewLineInfo) super.clone();
    } catch (CloneNotSupportedException ex) {
//      Exceptions.printStackTrace(ex);
    }
    throw new IllegalAccessError("why ?");
  }

  public void updateEdited(ConnexionInformation connexionInformation) {
    String currentUser = connexionInformation.getCurrentUser();
    LocalDateTime currentDate = connexionInformation.getCurrentDate();
    if (dateCreation == null) {
      dateCreation = currentDate;
    }
    if (auteurCreation == null) {
      auteurCreation = currentUser;
    }
    auteurDerniereModif = currentUser;
    dateDerniereModif = currentDate;
  }

  public ReportViewLineInfo copy(ConnexionInformation connexionInformation) {
    ReportViewLineInfo res = clone();
    res.auteurCreation = null;
    res.dateCreation = null;
    res.filename = null;//le filename doit être unique
    res.updateEdited(connexionInformation);
    return res;
  }

  public void setCommentaire(String commentaire) {
    this.commentaire = commentaire;
  }

  public LocalDateTime getDateCreation() {
    return dateCreation;
  }

  public String getAuteurDerniereModif() {
    return auteurDerniereModif;
  }

  public LocalDateTime getDateDerniereModif() {
    return dateDerniereModif;
  }
}
