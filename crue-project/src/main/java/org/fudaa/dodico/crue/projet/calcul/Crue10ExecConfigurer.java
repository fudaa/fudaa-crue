/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.calcul;

import java.util.List;

/**
 *
 * @author deniger
 */
public class Crue10ExecConfigurer implements ExecConfigurer<Crue10Exec> {

  final List<String> optionToAdd;

  public Crue10ExecConfigurer(final List<String> optionToAdd) {
    this.optionToAdd = optionToAdd;
  }

  @Override
  public void configure(final Crue10Exec calculExec) {
    calculExec.setOptions(optionToAdd);
  }
}
