/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.persist;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.projet.report.formule.FormuleParametersExpr;
import org.joda.time.LocalDateTime;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("Variable-FCs")
public class FormulesDao extends AbstractCrueDao {

  @XStreamAlias("AuteurDerniereModif")
  private String auteurDerniereModif;
  @XStreamAlias("DateDerniereModif")
  private LocalDateTime dateDerniereModif;
  @XStreamImplicit(itemFieldName = "VariableFC")
  private List<FormuleParametersExpr> formules;

  public String getAuteurDerniereModif() {
    return auteurDerniereModif;
  }

  public void setAuteurDerniereModif(String auteurDerniereModif) {
    this.auteurDerniereModif = auteurDerniereModif;
  }

  public LocalDateTime getDateDerniereModif() {
    return dateDerniereModif;
  }

  public void setDateDerniereModif(LocalDateTime dateDerniereModif) {
    this.dateDerniereModif = dateDerniereModif;
  }

  public List<FormuleParametersExpr> getFormules() {
    return formules;
  }

  public void setFormules(List<FormuleParametersExpr> runAlternatifs) {
    this.formules = runAlternatifs;
  }
}
