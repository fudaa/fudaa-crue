/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("ReportRunVariableEmhKey")
public class ReportRunVariableEmhKey implements ReportKeyContract {

  public static final ReportRunVariableEmhKey NULL = new ReportRunVariableEmhKey(ReportRunVariableKey.NULL_VALUE, StringUtils.EMPTY);
  final ReportRunVariableKey runVariableKey;
  final String emhName;

  public ReportRunVariableEmhKey(ReportRunKey runKey, ReportVariableKey variable, String emhName) {
    this(new ReportRunVariableKey(runKey, variable), emhName);
  }

  public ReportRunVariableEmhKey(ReportRunVariableKey runKey, String emhName) {
    this.runVariableKey = runKey;
    this.emhName = emhName;
    assert runKey != null;
    assert emhName != null;
  }

  @Override
  public String getVariableName() {
    return runVariableKey.getVariableName();

  }

  @Override
  public boolean isExtern() {
    return false;
  }

  @Override
  public ReportRunKey getReportRunKey() {
    return runVariableKey.getRunKey();
  }

  public String getDisplayName() {
    return getRunVariableKey().getVariable().getVariableDisplayName() + BusinessMessages.ENTITY_SEPARATOR + getEmhName() + BusinessMessages.ENTITY_SEPARATOR + getRunVariableKey().
            getRunKey().
            getDisplayName();
  }

  public String getDisplayName(String other) {
    return getRunVariableKey().getVariable().getVariableDisplayName() + other + BusinessMessages.ENTITY_SEPARATOR + getEmhName() + BusinessMessages.ENTITY_SEPARATOR + getRunVariableKey().
            getRunKey().
            getDisplayName();
  }

  @Override
  public ReportRunVariableEmhKey duplicateWithNewVar(String newVarName) {
    return new ReportRunVariableEmhKey(runVariableKey.duplicateWith(newVarName), emhName);
  }

  public ReportRunVariableKey getRunVariableKey() {
    return runVariableKey;
  }

  public String getEmhName() {
    return emhName;
  }

  @Override
  public int hashCode() {
    return 5 + runVariableKey.hashCode() * 7 + emhName.hashCode() * 13;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ReportRunVariableEmhKey other = (ReportRunVariableEmhKey) obj;
    if (this.runVariableKey != other.runVariableKey && (this.runVariableKey == null || !this.runVariableKey.equals(other.runVariableKey))) {
      return false;
    }
    if ((this.emhName == null) ? (other.emhName != null) : !this.emhName.equals(other.emhName)) {
      return false;
    }
    return true;
  }
}
