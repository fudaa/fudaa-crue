/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.planimetry;

import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;

/**
 *
 * @author Frederic Deniger
 */
public class ReportPlanimetryCasierExtraData extends AbstractReportPlanimetryConfigData {

  public ReportPlanimetryCasierExtraData() {

  }

  protected ReportPlanimetryCasierExtraData(ReportPlanimetryCasierExtraData from) {
    super(from);
  }

  @Override
  public ReportPlanimetryCasierExtraData copy() {
    return new ReportPlanimetryCasierExtraData(this);
  }

  @Override
  protected boolean specificVariableUpdated(FormuleDataChanges changes) {
    return false;
  }
}
