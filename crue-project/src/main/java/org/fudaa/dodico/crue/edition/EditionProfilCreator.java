/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.EMHFactory;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.dodico.crue.validation.ValidateAndRebuildProfilSection;

import java.util.ArrayList;
import java.util.List;

/**
 * @author deniger
 */
public class EditionProfilCreator {
    private final CreationDefaultValue defaultValues;
    private final UniqueNomFinder uniqueNomFinder;

    public EditionProfilCreator(UniqueNomFinder uniqueNomFinder, CreationDefaultValue defaultValues) {
        this.uniqueNomFinder = uniqueNomFinder;
        this.defaultValues = defaultValues;
    }

    protected UniqueNomFinder getUniqueNomFinder() {
        return uniqueNomFinder;
    }

    private DonFrt getDefaultFrtMineur(EMHSousModele sousModele) {
        String prefix = getFrtMineurName();
        DonFrt res = sousModele.getFrtConteneur().getDefaultFrtMineur();
        if (res == null) {
            List<DonFrt> listFrt = sousModele.getFrtConteneur().getListFrt();
            for (DonFrt donFrt : listFrt) {
                if (donFrt.getNom().startsWith(prefix)) {
                    res = donFrt;
                    sousModele.getFrtConteneur().setDefaultFrtMineur(res);
                }
            }
        }
        return res;
    }

    private DonFrt getDefaultFrtMajeur(EMHSousModele sousModele) {
        String prefix = getFrtMajeurName();
        DonFrt res = sousModele.getFrtConteneur().getDefaultFrtMajeur();
        if (res == null) {
            List<DonFrt> listFrt = sousModele.getFrtConteneur().getListFrt();
            for (DonFrt donFrt : listFrt) {
                if (donFrt.getNom().startsWith(prefix)) {
                    res = donFrt;
                    sousModele.getFrtConteneur().setDefaultFrtMajeur(res);
                }
            }
        }
        return res;
    }

    /**
     * cree si nécessaire le frottement
     *
     * @param sousModele le {@link EMHSousModele}
     * @param ccm        le {@link CrueConfigMetier}
     * @return un DonFrt ( utilise celui par defaut si possible)
     */
    private DonFrt getOrCreateFrtMineur(EMHSousModele sousModele, CrueConfigMetier ccm) {
        DonFrt defaultFrt = getDefaultFrtMineur(sousModele);
        if (defaultFrt != null) {
            return defaultFrt;
        }
        DonFrtStrickler newFrt = createDonFrtStrickler(getFrtMineurName(), sousModele, ccm);
        sousModele.getFrtConteneur().setDefaultFrtMineur(newFrt);
        sousModele.getFrtConteneur().addFrt(newFrt);
        return newFrt;
    }

    /**
     * cree si nécessaire le frottement
     *
     * @param sousModele le {@link EMHSousModele}
     * @param ccm        le {@link CrueConfigMetier}
     */
    public DonFrt getOrCreateFrtMajeur(EMHSousModele sousModele, CrueConfigMetier ccm) {
        DonFrt defaultFrt = getDefaultFrtMajeur(sousModele);
        if (defaultFrt != null) {
            return defaultFrt;
        }
        DonFrtStrickler newFrt = createDonFrtStrickler(getFrtMajeurName(), sousModele, ccm);
        sousModele.getFrtConteneur().setDefaultFrtMajeur(newFrt);
        sousModele.getFrtConteneur().addFrt(newFrt);
        return newFrt;
    }

    /**
     * Cree si nécessaire une frottement vide FkSto
     *
     * @param sousModele le {@link EMHSousModele}
     * @param ccm        le {@link CrueConfigMetier}
     * @return le frottement FkSto vide.
     */
    public DonFrt addEmptyZFkStoIfNeeded(EMHSousModele sousModele, CrueConfigMetier ccm) {
        DonFrt emptyDonFrt = sousModele.getFrtConteneur().getEmptyDonFrtFkSto(ccm);
        if (emptyDonFrt == null) {
            String newName = uniqueNomFinder.findNewNameDonFrtStricklerStockageEmpty(sousModele.getParent().getParent());
            emptyDonFrt = EMHFactory.createDonFrtZFkSto(newName);
            emptyDonFrt.getLoi().getEvolutionFF().getPtEvolutionFF().add(new PtEvolutionFF());
            sousModele.getFrtConteneur().addFrt(emptyDonFrt);
        }
        return emptyDonFrt;
    }

    public DonFrtStrickler createDonFrtStrickler(String name, EMHSousModele sousModele, CrueConfigMetier ccm) {
        String frtName = name;
        final EMHScenario scenario = sousModele.getParent().getParent();
        if (StringUtils.isEmpty(frtName)) {
            frtName = uniqueNomFinder.findNewNameDonFrtStrickler(scenario);
        } else {
            frtName = CruePrefix.getNomAvecPrefixe(CruePrefix.P_FROTT_STRICKLER, name);
            frtName = uniqueNomFinder.findUniqueNameDonFrt(frtName, scenario);
        }
        DonFrtStrickler newFrt = EMHFactory.createDonFrtZFk(frtName);
        ConfigLoi configLoi = ccm.getConfLoi().get(EnumTypeLoi.LoiZFK);
        PtEvolutionFF point = new PtEvolutionFF();
        point.setAbscisse(configLoi.getVarAbscisse().getValidator().getRangeNormalite().getMin().doubleValue());
        point.setOrdonnee(configLoi.getVarOrdonnee().getValidator().getRangeNormalite().getMin().doubleValue());
        newFrt.getLoi().getEvolutionFF().getPtEvolutionFF().add(point);
        return newFrt;
    }

    public DonPrtGeoProfilCasier createProfilCasier(CrueConfigMetier ccm, String nameOfCasier, EMHScenario scenario, List<PtProfil> ptProfils) {
        DonPrtGeoProfilCasier profileCasier = new DonPrtGeoProfilCasier(ccm);
        String profilName = CruePrefix.changePrefix(nameOfCasier, CruePrefix.P_CASIER, CruePrefix.P_PROFIL_CASIER);
        profilName = new UniqueNomFinder().findUniqueNameProfilCasier(profilName, scenario);
        profileCasier.setNom(profilName);
        profileCasier.setPtProfil(ptProfils);
        LitUtile litUtile = new LitUtile();
        litUtile.setLimDeb(ptProfils.get(0));
        litUtile.setLimFin(ptProfils.get(ptProfils.size() - 1));
        profileCasier.setLitUtile(litUtile);
        return profileCasier;
    }

    protected DonPrtGeoProfilCasier createProfilCasier(CrueConfigMetier ccm, String nameOfCasier, EMHScenario scenario) {
        final List<PtProfil> ptProfils = createDefaultPtProfil(ccm);
        return createProfilCasier(ccm, nameOfCasier, scenario, ptProfils);
    }

    protected DonPrtGeoProfilSection createProfilSection(CrueConfigMetier ccm, String name, EMHSousModele ssModele) {
        DonPrtGeoProfilSection section = new DonPrtGeoProfilSection();
        String nom = CruePrefix.getNomAvecPrefixe(CruePrefix.P_PROFIL_SECTION, name);
        //on s'assure de l'unicite du nom:
        nom = uniqueNomFinder.findUniqueNameProfilSection(nom, ssModele.getParent().getParent());
        section.setNom(nom);
        List<PtProfil> ptProfils = createDefaultPtProfil(ccm);
        section.setPtProfil(ptProfils);
        assert ptProfils.size() == 3;
        final PtProfil start = ptProfils.get(0);
        final PtProfil end = ptProfils.get(2);
        List<LitNumerote> lits = new ArrayList<>();
        lits.add(createLitMajeur(ssModele, ccm, start, start, ccm.getLitNomme().getMajeurStart()));
        lits.add(createLitMineur(ssModele, ccm, start, end));
        lits.add(createLitMajeur(ssModele, ccm, end, end, ccm.getLitNomme().getMajeurEnd()));
        section.setLitNumerote(lits);
        ValidateAndRebuildProfilSection rebuilder = new ValidateAndRebuildProfilSection(ccm, null);
        addEmptyZFkStoIfNeeded(ssModele, ccm);
        CtuluLog log = rebuilder.validateAndRebuildDonPrtGeoProfilSection(section, ssModele);
        assert !log.containsErrorOrSevereError();
        return section;
    }

    private List<PtProfil> createDefaultPtProfil(CrueConfigMetier ccm) {
        defaultValues.checkValues(ccm);
        List<PtProfil> res = new ArrayList<>();
        PtProfil first = new PtProfil(ccm.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_XT),
                ccm.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_Z));
        res.add(first);
        PtProfil middle = new PtProfil(first.getXt() + defaultValues.getDefaultLitMineurLength() / 2,
                first.getZ());
        res.add(middle);
        PtProfil last = new PtProfil(first.getXt() + defaultValues.getDefaultLitMineurLength(),
                first.getZ());
        res.add(last);
        return res;
    }

    private String getFrtMineurName() {
        return CruePrefix.P_FROTT_STRICKLER + "DefautMin";
    }

    private String getFrtMajeurName() {
        return CruePrefix.P_FROTT_STRICKLER + "DefautMaj";
    }

    public LitNumerote createLitMineur(EMHSousModele ssModele, CrueConfigMetier ccm, final PtProfil start, final PtProfil end) {
        DonFrt frt = getOrCreateFrtMineur(ssModele, ccm);
        LitNumerote litMineur = new LitNumerote();
        litMineur.setIsLitActif(true);
        litMineur.setIsLitMineur(true);
        litMineur.setFrot(frt);
        litMineur.setLimDeb(start);
        litMineur.setLimFin(end);
        litMineur.setNomLit(ccm.getLitNomme().getMineur());
        return litMineur;
    }

    public LitNumerote createLitMajeur(EMHSousModele ssModele, CrueConfigMetier ccm, final PtProfil start, final PtProfil end, LitNomme litNomme) {
        DonFrt frt = getOrCreateFrtMajeur(ssModele, ccm);
        LitNumerote litMajeur = new LitNumerote();
        litMajeur.setIsLitActif(true);
        litMajeur.setIsLitMineur(false);
        litMajeur.setFrot(frt);
        litMajeur.setLimDeb(start);
        litMajeur.setLimFin(end);
        litMajeur.setNomLit(litNomme);
        return litMajeur;
    }
}
