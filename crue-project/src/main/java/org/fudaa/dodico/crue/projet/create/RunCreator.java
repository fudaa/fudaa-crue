/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.create;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.cr.CRReader;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.projet.ScenarioLoaderCrue10FilesFinder;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RunCreator {
    private static long lastDateUsed = 0;
    private final String runNamePattern = "yyyy-MM-dd-HH'h'mm'm'ss's'";
    private final EMHProjet projet;
    private final ConnexionInformation connexionInformation;

    /**
     * @param projet le projet
     */
    public RunCreator(EMHProjet projet, ConnexionInformation connexionInformation) {
        super();
        this.projet = projet;
        this.connexionInformation = connexionInformation;
        assert connexionInformation != null;
    }

    protected RunCreatorOptions createDefault(ManagerEMHScenario scenario) {
        return new RunCreatorOptions(scenario.getRunCourant());
    }

    private void copyLinkedCrue9Files(ManagerEMHScenario scenario, CtuluLog log, EMHRun newRun) {
        if (!projet.getCoeurConfig().isCrue9Dependant()) {
            return;
        }
        if (scenario.isCrue10() && scenario.getLinkedscenarioCrue9() != null) {
            ManagerEMHScenario linkedScenarioCrue9 = projet.getScenario(scenario.getLinkedscenarioCrue9());
            if (linkedScenarioCrue9 == null) {
                log.addWarn("create.runCreator.linkedScenarioCrue9.cantFind", scenario.getLinkedscenarioCrue9());
            } else if (linkedScenarioCrue9.getRunCourant() == null) {
                log.addWarn("create.runCreator.linkedScenarioCrue9.noRunCourant", scenario.getLinkedscenarioCrue9());
            } else {
                File dirForRunModeleCrue9 = projet.getDirForRunModeleCrue9(linkedScenarioCrue9, linkedScenarioCrue9.getRunCourant());
                if (!dirForRunModeleCrue9.exists()) {
                    log.addWarn("create.runCreator.linkedScenarioCrue9.runDirNotExisting", dirForRunModeleCrue9.getAbsolutePath());
                } else {
                    File xxcprovx = new File(dirForRunModeleCrue9, "xxcprovx.dat");
                    boolean copyXxprovx = true;
                    if (!xxcprovx.exists()) {
                        copyXxprovx = false;
                        log.addWarn("create.runCreator.linkedScenarioCrue9.xxcprovxNotExisting", dirForRunModeleCrue9.getAbsolutePath());
                    } else {
                        testxxprovxContent(xxcprovx, log);
                    }
                    Map<String, RunFile> runFilesResultat = projet.getRunFilesResultat(linkedScenarioCrue9,
                            linkedScenarioCrue9.getRunCourant());
                    //un scenario crue9 a forcément un seul modele.
                    final ManagerEMHModeleBase modeleCrue9 = linkedScenarioCrue9.getFils().get(0);
                    final RunFile stoRunFile = runFilesResultat.get(ManagerEMHScenario.getResKey(modeleCrue9, CrueFileType.STO));
                    boolean copySto = true;
                    boolean copyStr = true;
                    if (stoRunFile == null || !stoRunFile.getFile().exists()) {
                        copySto = false;
                        final boolean undefined = stoRunFile == null;
                        if (undefined) {
                            log.addWarn("create.runCreator.linkedScenarioCrue9.stoNotDefined");
                        } else {
                            log.addWarn("create.runCreator.linkedScenarioCrue9.stoNotExisting", stoRunFile.getFile().getAbsolutePath());
                        }
                    }
                    final RunFile strRunFile = runFilesResultat.get(ManagerEMHScenario.getResKey(modeleCrue9, CrueFileType.STR));
                    if (strRunFile == null || !strRunFile.getFile().exists()) {
                        copyStr = false;
                        final boolean strUndefined = strRunFile == null;
                        if (strUndefined) {
                            log.addWarn("create.runCreator.linkedScenarioCrue9.strNotDefined");
                        } else {
                            log.addWarn("create.runCreator.linkedScenarioCrue9.strNotExisting", strRunFile.getFile().getAbsolutePath());
                        }
                    }
                    for (ManagerEMHModeleBase targetModele : scenario.getFils()) {
                        File destDir = projet.getDirForRunModele(scenario, targetModele, newRun);
                        destDir.mkdirs();
                        if (copyXxprovx) {
                            CrueFileHelper.copyFileWithSameLastModifiedDate(xxcprovx, new File(destDir, xxcprovx.getName()));
                        }
                        if (copySto) {
                            CrueFileHelper.copyFileWithSameLastModifiedDate(stoRunFile.getFile(), new File(destDir,
                                    stoRunFile.getFile().getName()));
                        }
                        if (copyStr) {
                            CrueFileHelper.copyFileWithSameLastModifiedDate(strRunFile.getFile(), new File(destDir,
                                    strRunFile.getFile().getName()));
                        }
                    }
                }
            }
        }
    }

    private boolean isAbsolute(String fileName) {
        return fileName.indexOf('\\') >= 0 || fileName.indexOf('/') >= 0;
    }

    public CrueOperationResult<EMHRun> create(ManagerEMHScenario scenario) {
        return create(scenario, createDefault(scenario));
    }

    /**
     * @param scenario le scenario
     * @param options les options
     */
    public CrueOperationResult<EMHRun> create(ManagerEMHScenario scenario, RunCreatorOptions options) {
        CtuluLog log = new CtuluLog();
        log.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
        if (log.containsSevereError()) {
            return new CrueOperationResult<>(null, log);
        }

        Map<String, File> inputFiles = projet.getInputFiles(scenario);

        EMHRun newRun = createRun();
        //on copie les fichiers d'entrées:
        //dest inputFile contains all input files
        Map<String, File> destInputFiles = projet.getInputFiles(scenario, newRun);
        //ne pas oublier de faire des File.getParent.mkdirs() si nécessaire ( si le dossier parent n'existe pas)

        for (Entry<String, File> entry : inputFiles.entrySet()) {
            final String name = entry.getKey();

            File destFile = destInputFiles.get(name);

            if (destFile == null) {
                log.addSevereError("create.runCreator.fileNotFound", name);
            }else {
                destFile.getParentFile().mkdirs();
            }
            if (!CrueFileHelper.copyFileWithSameLastModifiedDate(entry.getValue(), destFile)) {
                log.addSevereError("create.runCreator.cannotCopy", name);
            }
        }

        //options.getSelectedRunToStartFrom() donne le run à partir duquel on va copier si possible les fichier de résultats ( si pas obsolètes)
        //on copie si possible les fichiers de résultats.

        if (options.getSelectedRunToStartFrom() != null) {
            Map<String, RunFile> resultFilesToCopy = projet.getRunFilesResultat(scenario, options.getSelectedRunToStartFrom());
            Map<String, RunFile> destResultFiles = projet.getRunFilesResultat(scenario, newRun);
            RunPredecessorFileFinder finder = new RunPredecessorFileFinder(projet.getCoeurConfig(),scenario, destResultFiles.values());

            Map<String, File> filesToCopyById = new HashMap<>(destInputFiles);
            for (Entry<String, RunFile> entry : resultFilesToCopy.entrySet()) {
                filesToCopyById.put(entry.getKey(), entry.getValue().getFile());
            }
            Map<CrueFileType, RunCalculOption> optionsByCompute = options.getOptionsByCompute();
            for (RunFile targetFile : destResultFiles.values()) {
                RunCalculOption calcOption = optionsByCompute.get(targetFile.getType());
                //CRUE-294 ne copiés que les fichiers que si " Exécuter que si nécessaire" est choisi.
                if (RunCalculOption.EXECUTE_IF_NEEDED.equals(calcOption)) {
                    boolean obsolete = isObsolete(targetFile, finder, filesToCopyById);
                    //il faut copier les c*
                    final File srcFile = resultFilesToCopy.get(targetFile.getKey()).getFile();
                    final File logFile = ScenarioLoaderCrue10FilesFinder.retrieveLogName(srcFile, targetFile.getType());
                    if (!obsolete) {
                        //si le fichier de log contient une erreur fatale les fichier ne sont pas copiés
                        obsolete = containsFatalError(logFile);
                    }
                    if (obsolete) {//si obsolète on ne fait pas la copie
                        continue;
                    }

                    Set<File> binFiles = ScenarioLoaderCrue10FilesFinder.retrieveBinName(srcFile, targetFile.getType(), projet.getCoeurConfig(),
                            log);

                    final File destFile = targetFile.getFile();
                    if (!CrueFileHelper.copyFileWithSameLastModifiedDate(srcFile, destFile)) {
                        log.addWarn("create.runCreator.cannotCopy", srcFile.getName());
                    }
                    if (logFile.exists()) {
                        final File destLogFile = new File(destFile.getParentFile(), logFile.getName());
                        if (!CrueFileHelper.copyFileWithSameLastModifiedDate(logFile, destLogFile)) {
                            log.addWarn("create.runCreator.cannotCopy", destLogFile.getName());
                        }
                    }
                    if (!binFiles.isEmpty()) {
                        for (File binfileToCopy : binFiles) {
                            File destBinFile = new File(destFile.getParentFile(), binfileToCopy.getName());
                            if (!CrueFileHelper.copyFileWithSameLastModifiedDate(binfileToCopy, destBinFile)) {
                                log.addWarn("create.runCreator.cannotCopy", destBinFile.getName());
                            }
                        }
                    }
                }
            }
        }

        copyLinkedCrue9Files(scenario, log, newRun);

        return new CrueOperationResult<>(newRun, log);
    }

    /**
     * @param fileInNewRun    do not exist: the target of the copy
     * @param finder          process pou retrouver lesfichiers
     * @param filesToCopyById les fichiers a copier par id
     * @return true si données obsoletes
     */
    private boolean isObsolete(RunFile fileInNewRun, RunPredecessorFileFinder finder, Map<String, File> filesToCopyById) {
        //le fichier a copier n'existe pas: obsolte
        File testedRunFile = filesToCopyById.get(fileInNewRun.getKey());//doit etre le plus recente
        if (testedRunFile == null || !testedRunFile.exists()) {
            return true;
        }
        //on recherche les fichiers en amont
        final Set<String> predecessorFiles = finder.getPredecessorFile(fileInNewRun);
        final long testedLastModified = testedRunFile.lastModified();
        for (String key : predecessorFiles) {
            final File predecessorFile = filesToCopyById.get(key);
            if (predecessorFile == null || !predecessorFile.exists()) {
                return true;
            }
            final long predecessorLastModified = predecessorFile.lastModified();
            if (predecessorLastModified >= testedLastModified) {
                return true;
            }
            RunFile runFile = finder.getRunFile(key);
            //si un prédecesseur est un fichier résultat, on le teste également.
            if (runFile != null) {
                if (isObsolete(runFile, finder, filesToCopyById)) {
                    return true;
                }
            }
        }
        return false;
    }

    public Date getDateCreation(String run) {
        String remove = StringUtils.remove(run, CruePrefix.P_RUN);
        SimpleDateFormat fmt = new SimpleDateFormat(runNamePattern);
        try {
            return fmt.parse(remove);
        } catch (ParseException ex) {
            Logger.getLogger(RunCreator.class.getName()).log(Level.INFO, null, ex);
        }
        return null;
    }

    private EMHRun createRun() {
        long date = System.currentTimeMillis();
        //pour les tests unitaires qui peuvent créer des runs à la chaine...
        if (date <= lastDateUsed + 1000) {
            date = lastDateUsed + 2000;
        }
        lastDateUsed = date;
        String name = CruePrefix.P_RUN + new SimpleDateFormat(runNamePattern).format(new Date(date));
        final EMHRun run = new EMHRun(name);
        run.setInfosVersion(new EMHInfosVersion());
        run.getInfosVersion().updateForm(connexionInformation);

        return run;
    }

    private void testxxprovxContent(File xxcprovx, CtuluLog log) {
        List<String> litFichierLineByLine = CtuluLibFile.litFichierLineByLine(xxcprovx);
        if (litFichierLineByLine.size() != 4) {
            log.addError("create.runCreator.linkedScenarioCrue9.xxcprovx.NotValid");
        } else {
            if (isAbsolute(litFichierLineByLine.get(2)) || isAbsolute(litFichierLineByLine.get(3))) {
                log.addWarn("create.runCreator.linkedScenarioCrue9.xxcprovx.containsAbsolutePath");
            }
        }
    }

    private boolean containsFatalError(File logFile) {
        if (logFile.exists()) {
            CRReader reader = new CRReader(-1);
            CrueIOResu<CtuluLogLevel> read = reader.getHigherLevel(logFile);
            if (read.getMetier() != null) {
                CtuluLogLevel higherLevel = read.getMetier();
                //SEVERE -> ERRBLK et FATAL -> FATAL
                return higherLevel.equals(CtuluLogLevel.SEVERE) || higherLevel.equals(CtuluLogLevel.FATAL);
            }
            if (read.getAnalyse() != null && read.getAnalyse().containsSevereError()) {//que faire dans ce cas
                Logger.getLogger(RunCreator.class.getName()).log(Level.SEVERE, "La lecture du fichier de log {0} a échoué", read);
                return true;
            }
        }
        return false;
    }
}
