/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.persist;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.fudaa.dodico.crue.projet.report.ReportMultiVarConfig;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("MultiVarConfig")
public class ReportConfigMultiVarDao extends AbstractReportConfigDao<ReportMultiVarConfig> {

  @XStreamAlias("MultiVar")
  private ReportMultiVarConfig config;

  public ReportConfigMultiVarDao() {
  }

  public ReportConfigMultiVarDao(ReportMultiVarConfig config) {
    this.config = config;
  }

  @Override
  public ReportMultiVarConfig getConfig() {
    return config;
  }

  @Override
  public void setConfig(ReportMultiVarConfig config) {
    this.config = config;
  }
}
