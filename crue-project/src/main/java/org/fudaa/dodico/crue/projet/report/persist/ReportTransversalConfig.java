/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.persist;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.data.ReportKeyContract;
import org.fudaa.dodico.crue.projet.report.data.ReportKeyContratCleaner;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableConfig;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;
import org.fudaa.dodico.crue.projet.report.loi.ViewCourbeConfig;
import org.fudaa.ebli.courbe.EGCourbePersist;

/**
 * @author Frederic Deniger
 */
@XStreamAlias("Transversal")
public class ReportTransversalConfig extends AbstractReportCourbeConfig {

  @XStreamAlias("Section")
  private String sectionName;
  /**
   * la liste des profils à dessiner sur le graphe
   */
  @XStreamImplicit(itemFieldName = "ProfilXtZ-Run")
  protected List<ReportRunKey> profilXtZ = new ArrayList<>();
  /**
   * la listes des variables à dessiner sur le graphe
   */
  @XStreamImplicit(itemFieldName = "Variable-RunVariable")
  protected List<ReportRunVariableKey> profilVariables = new ArrayList<>();
  /**
   * la config pour les courbes:
   */
  @XStreamAlias("Courbe-Configs")
  protected final Map<ReportRunVariableKey, EGCourbePersist> courbeconfigs = new HashMap<>();
  @XStreamAlias("Vue-Config")
  private ViewCourbeConfig loiLegendConfig = new ViewCourbeConfig();

  public ReportTransversalConfig() {

  }

  public ReportTransversalConfig(ReportTransversalConfig from) {
    super(from);
    if (from == null) return;
    sectionName = from.sectionName;
    if (from.profilXtZ != null) {
      profilXtZ.addAll(from.profilXtZ);
    }
    if (from.profilVariables != null) {
      profilVariables.addAll(from.profilVariables);
    }
    from.courbeconfigs.forEach((key, value) -> courbeconfigs.put(key, value.duplicate()));
    if (from.loiLegendConfig != null) {
      loiLegendConfig = from.loiLegendConfig.dpulicate();
    }
  }

  @Override
  public ReportTransversalConfig duplicate() {
    return new ReportTransversalConfig(this);
  }

  @Override
  public void reinitContent() {
    super.reinitContent();
    if (profilXtZ == null) {
      profilXtZ = new ArrayList<>();
    }
    if (profilVariables == null) {
      profilVariables = new ArrayList<>();
    }
    if (loiLegendConfig == null) {
      loiLegendConfig = new ViewCourbeConfig();
    }
    loiLegendConfig.reinit();
    profilXtZ.remove(ReportRunKey.NULL_VALUE);
    profilVariables.remove(ReportRunVariableKey.NULL_VALUE);
  }

  public Map<ReportRunVariableKey, EGCourbePersist> getCourbeconfigs() {
    return courbeconfigs;
  }

  @Override
  protected void cleanUnusedCourbesConfig(Set<ReportKeyContract> usedKeys) {
    for (Iterator<ReportRunVariableKey> it = courbeconfigs.keySet().iterator(); it.hasNext(); ) {
      if (!usedKeys.contains(it.next())) {
        it.remove();
      }
    }
  }

  @Override
  public ReportConfigContrat createTemplateConfig() {
    final ReportTransversalConfig duplicate = (ReportTransversalConfig) duplicate();
    duplicate.sectionName = null;
    duplicate.clearExternFiles();
    duplicate.loiLegendConfig.clearLabelTxtTooltip();
    duplicate.clearZooms();
    ReportKeyContratCleaner.cleanNotCurrent(duplicate.courbeconfigs);
    ReportKeyContratCleaner.cleanNotCurrent(duplicate.profilVariables);
    ReportKeyContratCleaner.cleanNotCurrent(duplicate.profilXtZ);
    return duplicate;
  }

  @Override
  public boolean variablesUpdated(FormuleDataChanges changes) {
    boolean modified = super.axeVariablesUpdated(changes);
    modified |= VariableUpdateHelper.updateViewCourbeConfig(loiLegendConfig, changes);
    Pair<List<ReportRunVariableKey>, Boolean> res = VariableUpdateHelper.variablesUpdatedVariable(profilVariables, changes);
    if (Boolean.TRUE.equals(res.second)) {
      profilVariables = res.first;
      modified = true;
    }
    modified |= VariableUpdateHelper.updateCourbeConfig(courbeconfigs, changes);
    return modified;
  }

  @Override
  public ViewCourbeConfig getLoiLegendConfig() {
    return loiLegendConfig;
  }

  public void put(ReportRunVariableKey key, EGCourbePersist courbePersist) {
    courbeconfigs.put(key, courbePersist);
  }

  public List<ReportRunVariableKey> getProfilVariables() {
    return profilVariables;
  }

  public List<ReportRunKey> getProfilXtZ() {
    return profilXtZ;
  }

  @Override
  public ReportContentType getType() {
    return ReportContentType.TRANSVERSAL;
  }

  public boolean isProfilDrawn(ReportRunKey key) {
    if (key.isCourant()) {
      return true;
    }
    return profilXtZ.contains(key);
  }

  public List<String> getVariablesDrawn(ReportRunKey key) {
    List<String> res = new ArrayList<>();
    for (ReportRunVariableKey vKey : profilVariables) {
      if (key.equals(vKey.getRunKey())) {
        res.add(vKey.getVariable().getVariableName());
      }
    }
    return res;
  }

  public String getSectionName() {
    return sectionName;
  }

  @Override
  public String getMainEMHName() {
    return sectionName;
  }

  public void setSectionName(String sectionName) {
    this.sectionName = sectionName;
  }

  public ReportRunVariableConfig create(ReportRunVariableKey key) {
    ReportRunVariableConfig res = new ReportRunVariableConfig();
    res.setReportRunVariableKey(key);
    return res;
  }

  public void addProfilVariable(ReportRunVariableKey... keys) {
    profilVariables.addAll(Arrays.asList(keys));
  }

  public void addProfilYtZ(ReportRunKey... keys) {
    profilXtZ.addAll(Arrays.asList(keys));
  }
}
