/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.create;

import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.projet.create.LevelDataFactory.NameConverter;

public final class ConversionNameConverter implements NameConverter {
  private final CrueVersionType version;

  public ConversionNameConverter(CrueVersionType version) {
    this.version = version;
  }

  @Override
  public String convert(String name) {
    return name + version.getSuffix();
  }
}
