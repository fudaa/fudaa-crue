/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition.bean;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.emh.EnumCasierType;

/**
 *
 * @author deniger
 */
public class ListCasierContent extends AbstractListContent {

  public static final String PROP_NOEUD = "noeud";
  String noeud;
  EnumCasierType type = EnumCasierType.EMHCasierProfil;
  String commentaire = StringUtils.EMPTY;
  boolean active;

  public ListCasierContent(final String nom) {
    this.nom = nom;
  }

  public ListCasierContent() {
  }

  public EnumCasierType getType() {
    return type;
  }

  public String getNoeud() {
    return noeud;
  }

  public void setNoeud(final String noeud) {
    this.noeud = noeud;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(final boolean active) {
    this.active = active;
  }

  public void setType(final EnumCasierType type) {
    this.type = type;
  }

  public String getCommentaire() {
    return commentaire;
  }

  public void setCommentaire(final String commentaire) {
    this.commentaire = commentaire;
  }
}
