package org.fudaa.dodico.crue.projet.copy;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.dodico.crue.projet.rename.RenameManager;

import java.io.File;
import java.util.*;

/**
 * Classe permettant de mémoriser des informations pour une copie d'EMH
 *
 * @author Yoan GRESSIER
 */
public class CopyInformations {
  /**
   * Manager du conteneur racine (scénario) à copier
   */
  public final ManagerEMHContainerBase rootContainer;
  /**
   * Fichier racine à copier
   */
  public final FichierCrue rootFile;
  /**
   * Faut-il faire une copie profonde de l'EMH ?
   */
  public final boolean deepCopy;
  /**
   * Étude source à partir de laquelle seront copiés les conteneurs/fichiers à
   * créer
   */
  public final EMHProjet sourceProject;
  /**
   * Étude destination dans laquelle seront copiés les conteneurs/fichiers à
   * créer (peut-être différente de la source)
   */
  public final EMHProjet destProject;
  /**
   * Collection des conteneurs à créer sous la forme "Ancien
   * ManagerEMHContainerBase/Nouveau Nom"
   */
  public final Map<ManagerEMHContainerBase, String> containersToCopy = new HashMap<>();
  /**
   * Liste des fichiers à créer sous la forme "Ancien FichierCrue/Nouveau nom
   * fichier"
   */
  public final Map<FichierCrue, String> filesToCopy = new HashMap<>();

  /**
   * Constucteur pour la copie d'un conteneur (scénario)
   *
   * @param root le conteneur à copier
   * @param infos informations de renommage (nouveaux noms des différents
   *     fichiers associés au conteneur à copier)
   * @param deepCopy true si copie profonde, false sinon
   * @param project l'étude dans laquelle se passe la copie (même source et
   *     cible)
   */
  public CopyInformations(ManagerEMHContainerBase root, RenameManager.NewNameInformations infos, boolean deepCopy, EMHProjet project) {
    this(root, infos, deepCopy, project, project);
  }

  /**
   * Constucteur pour la copie d'un conteneur (scénario)
   *
   * @param root le conteneur à copier
   * @param infos informations de renommage (nouveaux noms des différents
   *     fichiers associés au conteneur à copier)
   * @param deepCopy true si copie profonde, false sinon
   * @param sourceProject l'étude dans laquelle se trouve root
   * @param destProject l'étude dans laquelle il faut copier root
   */
  public CopyInformations(ManagerEMHContainerBase root, RenameManager.NewNameInformations infos, boolean deepCopy, EMHProjet sourceProject, EMHProjet destProject) {
    this.rootContainer = root;
    this.rootFile = null;
    this.deepCopy = deepCopy;
    this.sourceProject = sourceProject;
    this.destProject = destProject;
    // génération des conteneurs et fichiers à créer
    this.fillMaps(infos);
  }

  /**
   * Constructeur pour la copie d'un fichier de l'étude
   *
   * @param root le fichier à copier
   * @param infos informations de renommage (nouveau nom)
   * @param project l'étude dans laquelle se passe la copie (même source et
   *     cible)
   */
  public CopyInformations(FichierCrue root, RenameManager.NewNameInformations infos, EMHProjet project) {
    this.rootContainer = null;
    this.rootFile = root;
    this.deepCopy = false;
    this.sourceProject = project;
    this.destProject = project;
    // génération des conteneurs et fichiers à créer
    this.fillMaps(infos);
  }

  public Map<FichierCrue, String> getFilesToCopy() {
    return Collections.unmodifiableMap(filesToCopy);
  }

  /**
   * Dresse la liste des fichiers qui seront écrasés dans l'étude de
   * destination
   *
   * @return la liste des fichiers
   */
  public List<String> getOverwrittenFiles() {
    List<String> path = new ArrayList<>();
    for (Map.Entry<FichierCrue, String> entry : filesToCopy.entrySet()) {
      String newName = entry.getValue();
      if (!StringUtils.equals(entry.getKey().getNom(), newName)) {
        FichierCrue destFile = entry.getKey().clone();
        destFile.setNom(newName);
        final File newFile = destFile.getProjectFile(sourceProject);
        if (newFile.isFile()) {
          path.add(newFile.getAbsolutePath());
        }
      }
    }
    return path;
  }

  /**
   * Remplie les deux collections containersToCopy et filesToCopy à partir des
   * infos de renommage
   *
   * @param infos les informations de renommage
   */
  private void fillMaps(RenameManager.NewNameInformations infos) {
    for (Map.Entry<String, ManagerEMHContainerBase> newName : infos.newNameContainers.entrySet()) {
      this.containersToCopy.put(newName.getValue(), newName.getKey());
    }

    for (Map.Entry<String, FichierCrue> newName : infos.newNameFiles.entrySet()) {
      this.filesToCopy.put(newName.getValue(), newName.getKey());
    }
  }
}
