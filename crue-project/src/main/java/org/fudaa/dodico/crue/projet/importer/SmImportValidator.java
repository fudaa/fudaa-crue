package org.fudaa.dodico.crue.projet.importer;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.Pair;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.projet.OrdonnanceurCrue10;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Teste si un fichier de sous-modele peut être importé
 */
public class SmImportValidator {
  private final EMHProjet projet;
  private final Set<String> filenames;
  private final boolean createSousModele;

  /**
   * @param projet le projet dans lequel le sous-modele sera créé ou mis à jour
   */
  public SmImportValidator(EMHProjet projet) {
    this(projet, true);
  }

  /**
   *
   * @param projet le projet dans lequel le sous-modele sera créé ou mis à jour
   * @param createSousModele true si le sous-modele doit être créé.
   */
  public SmImportValidator(EMHProjet projet, boolean createSousModele) {
    this.createSousModele = createSousModele;
    this.projet = projet;
    if (projet != null) {
      filenames = projet.getInfos().getBaseFichiersProjets().stream().map(FichierCrue::getNom).collect(Collectors.toSet());
    } else {
      filenames = Collections.emptySet();
    }
  }

  /**
   * @param sousModeleFileToImport le fichier du sous-modele à importer
   * @param validGrammar true si la grammaire des fichiers doit être validés
   * @return resultat de validation
   */
  public Pair<Boolean, String> canImportSousModele(File sousModeleFileToImport, boolean validGrammar) {
    //on teste la validité des fichiers/dossiers
    Pair<Boolean, String> firstValidation = validateFileLocation(sousModeleFileToImport);
    if (firstValidation != null) {
      return firstValidation;
    }

    //on teste que le fichier est bien connu
    final List<CrueFileType> sousModeleFileTypes = new OrdonnanceurCrue10().getSousModele();
    final Optional<CrueFileType> optionalCrueFileType = sousModeleFileTypes.stream().filter(fileType -> fileType.hasExtension(sousModeleFileToImport)).findFirst();
    if (!optionalCrueFileType.isPresent()) {
      return new Pair<>(false, BusinessMessages.getString("sm.import.fileNotSousModeleFile", sousModeleFileToImport.getName()));
    }
    CrueFileType type = optionalCrueFileType.get();
    String sousModeleRadical = getSousModeleRadical(sousModeleFileToImport, type);
    final Pair<Boolean, String> allFileArePresentValidation = testAllFilesArePresent(sousModeleFileToImport, sousModeleFileTypes, sousModeleRadical);
    if (allFileArePresentValidation != null) {
      return allFileArePresentValidation;
    }

    if (createSousModele) {
      final Pair<Boolean, String> canImportSousModeleName = canCreateNewSousModeleName(sousModeleRadical);
      if (!canImportSousModeleName.firstValue) {
        return canImportSousModeleName;
      }
    }

    if (validGrammar) {
      for (CrueFileType fileType : sousModeleFileTypes) {
        File file = fileType.getFile(sousModeleFileToImport.getParentFile(), sousModeleRadical);
        if (file.exists()) {
          CtuluLog log = new CtuluLog();
          Crue10FileFormatFactory.getVersion(fileType, projet.getCoeurConfig()).isValide(file, log);
          if (log.containsErrorOrSevereError()) {
            return new Pair<>(false, BusinessMessages.getString("sm.import.fileNotValid", file.getName()));
          }
        }
      }
    }
    return new Pair<>(true, CtuluLibString.EMPTY_STRING);
  }

  public static String getSousModeleRadical(File sousModeleFileToImport, CrueFileType type) {
    return StringUtils.removeEndIgnoreCase(sousModeleFileToImport.getName(), "." + type.getExtension());
  }

  private Pair<Boolean, String> testAllFilesArePresent(File sousModeleFileToImport, List<CrueFileType> sousModeleFileTypes, String sousModeleRadical) {
    File folder = sousModeleFileToImport.getParentFile();
    final List<String> notExistingFilenames = sousModeleFileTypes.stream().map(crueFileType -> new File(folder, sousModeleRadical + "." + crueFileType.getExtension()))
      .filter(file -> !file.exists())
      .map(File::getName).collect(Collectors.toList());
    if (!notExistingFilenames.isEmpty()) {
      if (notExistingFilenames.size() == 1) {
        return new Pair<>(false, BusinessMessages.getString("sm.import.fileNotExisting", notExistingFilenames.get(0)));
      }
      return new Pair<>(false, BusinessMessages.getString("sm.import.filesAreMissing", StringUtils.join(notExistingFilenames, ", ")));
    }
    return null;
  }

  /**
   * Teste si les données ne sont pas nulles et si le fichier n'est pas inclus dans les fichiers de l'étude
   *
   * @param sousModeleFileToImport un des fichiers à importer
   * @return resultat de validation
   */
  protected Pair<Boolean, String> validateFileLocation(File sousModeleFileToImport) {
    if (projet == null) {
      return new Pair<>(false, BusinessMessages.getString("sm.import.etuNotSet"));
    }
    if (projet.getDirOfFichiersEtudes() == null) {
      return new Pair<>(false, BusinessMessages.getString("sm.import.etuFolderNotSet"));
    }
    if (sousModeleFileToImport == null || !sousModeleFileToImport.exists()) {
      return new Pair<>(false, BusinessMessages.getString("sm.import.fileNotExisting", sousModeleFileToImport==null?"?":sousModeleFileToImport.getAbsolutePath()));
    }
    if (sousModeleFileToImport.isDirectory()) {
      return new Pair<>(false, BusinessMessages.getString("sm.import.fileIsDirectory", sousModeleFileToImport.getAbsolutePath()));
    }
    try {
      String canonicalParent = projet.getDirOfFichiersEtudes().getCanonicalPath()+File.separator;
      String canonicalChild = sousModeleFileToImport.getCanonicalPath();
      if (FilenameUtils.directoryContains(canonicalParent, canonicalChild)) {
        return new Pair<>(false, BusinessMessages.getString("sm.import.fileIsIncludedInEtuFolder", sousModeleFileToImport.getAbsolutePath()));
      }
    } catch (IOException e) {
      return new Pair<>(false, BusinessMessages.getString(e.getMessage(), sousModeleFileToImport.getAbsolutePath()));
    }
    return null;
  }

  protected Pair<Boolean, String> canCreateNewSousModeleName(String sousModeleRadical) {
    if (!ValidationPatternHelper.isRadicalValideForEMHContainer(sousModeleRadical)) {
      return new Pair<>(false, BusinessMessages.getString("sm.import.radicalNotValid", sousModeleRadical));
    }
    String completeName = CruePrefix.addPrefix(CruePrefix.P_SS_MODELE, sousModeleRadical);
    if (projet.getSousModele(completeName) != null) {
      return new Pair<>(false, BusinessMessages.getString("sm.import.existingSousModele", completeName));
    }
    //les types de fichiers attendus
    final List<String> extensions = new OrdonnanceurCrue10().getSousModele().stream().map(CrueFileType::getExtension).collect(Collectors.toList());
    //peut etre importé si aucun nom de fichier deja existant
    boolean valid = extensions.stream().noneMatch(extension -> filenames.contains(sousModeleRadical + "." + extension));
    if (valid) {
      return new Pair<>(true, CtuluLibString.EMPTY_STRING);
    }
    return new Pair<>(false, BusinessMessages.getString("sm.import.existingFiles", sousModeleRadical));
  }
}
