package org.fudaa.dodico.crue.edition;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.FileDeleteResult;
import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.projet.calcul.RunScenarioPair;
import org.fudaa.dodico.crue.projet.select.SelectableFinder;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Permet de supprimer des containes
 *
 * @author deniger
 */
public class DeleteContainersCallable implements Callable<Boolean> {
  private final SelectableFinder.SelectedItems items;
  private final EMHProjetController emhProjetController;
  List<String> notDeletableFiles;
  Map<RunScenarioPair, FileDeleteResult> runsNotCleaned;

  public DeleteContainersCallable(SelectableFinder.SelectedItems itemsToDelete, EMHProjetController project) {
    this.items = itemsToDelete;
    this.emhProjetController = project;
  }

  /**
   *
   * @return
   */
  public List<String> getNotDeletableFiles() {
    return notDeletableFiles;
  }

  /**
   * @return les runs non nettoyes
   */
  public Map<RunScenarioPair, FileDeleteResult> getRunsNotCleaned() {
    return runsNotCleaned;
  }

  @Override
  public Boolean call() throws Exception {
    boolean res = true;
    runsNotCleaned = new HashMap<>();
    notDeletableFiles = new ArrayList<>();
    for (FichierCrue file : items.getSelectedFiles()) {
      final File fichier = file.getProjectFile(emhProjetController.getProjet());
      if (fichier != null && fichier.exists() && !fichier.canWrite()) {
        notDeletableFiles.add(fichier.getAbsolutePath());
      }
    }
    if (!notDeletableFiles.isEmpty()) {
      return Boolean.FALSE;
    }
    for (ManagerEMHContainerBase container : items.getSelectedContainers()) {
      if (container instanceof ManagerEMHScenario) {
        emhProjetController.deleteAllRuns((ManagerEMHScenario) container, runsNotCleaned);
      }
      final boolean removed = emhProjetController.getProjet().remove(container);
      if (removed && CrueLevelType.SOUS_MODELE.equals(container.getLevel())) {
        //on efface le repertoire de configuration.
        ManagerEMHSousModele ssModele = (ManagerEMHSousModele) container;
        File configDir = emhProjetController.getProjet().getInfos().getDirOfConfig(ssModele);
        if (CtuluLibFile.exists(configDir)) {
          CtuluLibFile.deleteDir(configDir);
        }
      }
      res &= removed;
    }

    for (FichierCrue file : items.getSelectedFiles()) {
      if (this.emhProjetController.getProjet().removeFile(file)) {
        final File fichier = file.getProjectFile(emhProjetController.getProjet());
        if (fichier != null && fichier.exists()) {
          boolean deleted = fichier.delete();
          if (!deleted) {
            notDeletableFiles.add(fichier.getAbsolutePath());
          }
        }
      } else {
        res = false;
      }
    }

    return Boolean.valueOf(res);
  }
}
