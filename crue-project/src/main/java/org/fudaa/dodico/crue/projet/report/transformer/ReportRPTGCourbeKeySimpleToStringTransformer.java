/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.transformer;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.ctulu.converter.BooleanToStringTransformer;
import org.fudaa.dodico.crue.projet.report.data.ReportRPTGCourbeKey;

/**
 * Nomme Simple car ne contient qu'un niveau.
 *
 * @author Frederic Deniger
 */
public class ReportRPTGCourbeKeySimpleToStringTransformer extends AbstractPropertyToStringTransformer<ReportRPTGCourbeKey> {

  final BooleanToStringTransformer booleanTransformer = new BooleanToStringTransformer();

  public ReportRPTGCourbeKeySimpleToStringTransformer() {
    super(ReportRPTGCourbeKey.class);
  }

  @Override
  public String toStringSafe(ReportRPTGCourbeKey in) {
    return in.getVariable() + KeysToStringConverter.MINOR_SEPARATOR + booleanTransformer.toStringSafe(in.isMain());
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }

  @Override
  public ReportRPTGCourbeKey fromStringSafe(String in) {
    String[] split = StringUtils.split(in, KeysToStringConverter.MINOR_SEPARATOR);
    if (split.length == 2) {
      return new ReportRPTGCourbeKey(split[0], Boolean.TRUE.equals(booleanTransformer.fromString(split[1])));
    }
    return ReportRPTGCourbeKey.NULL_VALUE;
  }
}
