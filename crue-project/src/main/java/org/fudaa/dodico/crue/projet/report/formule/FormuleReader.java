/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.io.CrueXmlReaderWriterImpl;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.common.LocalDateTimeConverter;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;
import org.fudaa.dodico.crue.projet.report.ReportXstreamReaderWriter;
import org.fudaa.dodico.crue.projet.report.persist.FormulesDao;
import org.fudaa.ebli.converter.TraceToStringConverter;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleReader implements CrueDataDaoStructure {
  
  public static XStream initXstream(TraceToStringConverter traceToStringConverter) {
    final XmlFriendlyNameCoder replacer = new XmlFriendlyNameCoder("#", "_");
    final StaxDriver staxDriver = new StaxDriver(replacer);
    final XStream xstream = new XStream(staxDriver);
    CrueXmlReaderWriterImpl.initXstreamSecurity(xstream);
    return xstream;
  }
  
  public CrueIOResu<List<FormuleParametersExpr>> read(File targetFile) {
    ReportXstreamReaderWriter<FormulesDao> readerWriter = new ReportXstreamReaderWriter<>("Variable-FCs", ReportXstreamReaderWriter.VERSION,
            this);
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("formule.read.file");
    FormulesDao readDao = readerWriter.readDao(targetFile, log, null);
    new FormuleServiceContantTransformer().process(readDao);
    CrueIOResu<List<FormuleParametersExpr>> resuFinal = new CrueIOResu<>();
    resuFinal.setAnalyse(log);
    resuFinal.setMetier(readDao == null ? Collections.emptyList() : readDao.getFormules());
    return resuFinal;
  }
  
  public CrueIOResu<FormuleServiceContent> readContent(File targetFile, ReportGlobalServiceContrat reportGlobalServiceContrat) {
    CrueIOResu<List<FormuleParametersExpr>> read = read(targetFile);
    final CtuluLog log = read.getAnalyse();
    if (log.containsErrorOrSevereError()) {
      return new CrueIOResu<>(new FormuleServiceContent(), log);
    }
    Collection<String> allVariableNames = TransformerHelper.toId(read.getMetier());
    List<FormuleContent> contents = new ArrayList<>();
    List<FormuleParametersExpr> metier = read.getMetier();
    if (metier != null) {
      for (FormuleParametersExpr param : metier) {
        FormuleContent createContent = param.createContent(reportGlobalServiceContrat);
        CtuluLog setParameter = createContent.setParameter(param, allVariableNames);
        if (setParameter.isNotEmpty()) {
          log.addAllLogRecord(setParameter);
        }
        contents.add(createContent);
      }
    }
    return new CrueIOResu<>(new FormuleServiceContent(contents), log);
  }
  
  @Override
  public void configureXStream(XStream xstream, CtuluLog ctuluLog, org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    xstream.processAnnotations(FormulesDao.class);
    xstream.processAnnotations(FormuleParametersExpr.class);
    xstream.registerConverter(new LocalDateTimeConverter());
  }
}
