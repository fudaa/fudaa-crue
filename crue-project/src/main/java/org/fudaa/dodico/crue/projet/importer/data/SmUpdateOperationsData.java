package org.fudaa.dodico.crue.projet.importer.data;

import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;

import java.util.*;

public class SmUpdateOperationsData {
  private final List<EMH> sourceEmhsToAdd;
  private final SmUpdateSourceTargetData sourceTargetData;
  private final EMHSousModele sourceSousModele;
  private final EMHSousModele targetSousModele;

  public SmUpdateOperationsData(final EMHSousModele sourceSousModele,final EMHSousModele targetSousModele) {
    this(sourceSousModele,targetSousModele,Collections.emptyList(),new SmUpdateSourceTargetData());
  }

  public SmUpdateOperationsData(final EMHSousModele sourceSousModele,final EMHSousModele targetSousModele,List<EMH> sourceEmhsToAdd, SmUpdateSourceTargetData update) {
    this.sourceSousModele=sourceSousModele;
    this.targetSousModele=targetSousModele;
    this.sourceEmhsToAdd = build(sourceEmhsToAdd);
    this.sourceTargetData = update;
  }

  public SmUpdateSourceTargetData getSourceTargetData() {
    return sourceTargetData;
  }

  public EMHSousModele getSourceSousModele() {
    return sourceSousModele;
  }

  public EMHSousModele getTargetSousModele() {
    return targetSousModele;
  }


  public List<EMH> getSourceEmhsToAdd() {
    return sourceEmhsToAdd;
  }

  public List<EMH> getSourceEmhsToUpdate() {
    return sourceTargetData.getSourceEMHs();
  }

  public Set<String> getEmhNameToAddOrUpdate() {
    Set<String> res = new HashSet<>();
    res.addAll(TransformerHelper.toNom(sourceEmhsToAdd));
    res.addAll(TransformerHelper.toNom(getSourceEmhsToUpdate()));
    return res;
  }

  private static List<EMH> build(List<EMH> in) {
    if (in == null) {
      return Collections.emptyList();
    }
    List<EMH> out = new ArrayList<>(in);
    out.sort(ObjetNommeByNameComparator.INSTANCE);
    return Collections.unmodifiableList(out);
  }
}
