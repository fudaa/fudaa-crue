/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;

/**
 *
 * @author deniger
 */
public class EditionChecker {

  public static boolean isEditable(final String crue10Version) {
    return Crue10VersionConfig.getLastVersion().equals(crue10Version);
  }
}
