/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.transformer;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableTimeKey;

/**
 *
 * @author Frederic Deniger
 */
public class ReportRunVariableTimeKeyMultiToStringTransformer extends AbstractPropertyToStringTransformer<ReportRunVariableTimeKey> {

  private final AbstractPropertyToStringTransformer<ReportRunVariableKey> reportRunVariableKeyToStringTransformer;
  private final AbstractPropertyToStringTransformer<ResultatTimeKey> resultatKeyToStringTransformer;

  public ReportRunVariableTimeKeyMultiToStringTransformer(
          AbstractPropertyToStringTransformer<ReportRunVariableKey> reportRunVariableKeyToStringTransformer,
          AbstractPropertyToStringTransformer<ResultatTimeKey> resultatKeyToStringTransformer) {
    super(ReportRunVariableTimeKey.class);
    this.reportRunVariableKeyToStringTransformer = reportRunVariableKeyToStringTransformer;
    this.resultatKeyToStringTransformer = resultatKeyToStringTransformer;
  }

  @Override
  public String toStringSafe(ReportRunVariableTimeKey in) {
    return reportRunVariableKeyToStringTransformer.toString(in.getRunVariableKey()) + KeysToStringConverter.MAIN_SEPARATOR
            + resultatKeyToStringTransformer.toString(in.getResultatKey());
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }

  @Override
  public ReportRunVariableTimeKey fromStringSafe(String in) {
    String resultatKeyAsString = StringUtils.substringAfterLast(in, KeysToStringConverter.MAIN_SEPARATOR);
    ResultatTimeKey resultatKey = resultatKeyToStringTransformer.fromString(resultatKeyAsString);
    ReportRunVariableKey reportRunVariableKey = reportRunVariableKeyToStringTransformer.fromString(StringUtils.substringBeforeLast(in,
            KeysToStringConverter.MAIN_SEPARATOR));
    if (reportRunVariableKey != null) {
      return new ReportRunVariableTimeKey(reportRunVariableKey, resultatKey);
    }
    return ReportRunVariableTimeKey.NULL;
  }
}
