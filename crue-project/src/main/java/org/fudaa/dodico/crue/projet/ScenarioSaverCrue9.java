/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.io.Crue9FileFormatFactory;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.validation.ValidationHelper;
import org.fudaa.dodico.crue.validation.ValidatorForCrue9Export;
import org.fudaa.dodico.crue.validation.ValidatorForIDCrue9;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Se charge de sauvegarder un scenario. Cette premiere version utilisee dans le lot 0 uniqument pour la generation de crue 9 en
 * crue 10. Utilise un fichier modele et genere tous les fichier avec les bons suffixes.
 *
 * @author Adrien Hadoux
 */
public class ScenarioSaverCrue9 {
    private final CtuluLogGroup errorManager;
    private final ManagerEMHScenario scenario;
    private final EMHScenario scenarioToSave;
    private final CrueConfigMetier configMetier;

    protected ScenarioSaverCrue9(final EMHScenario scenarioToSave, final ManagerEMHScenario scenarioToPersist,
                                 final CtuluLogGroup errorManager, CrueConfigMetier configMetier) {
        super();
        this.scenarioToSave = scenarioToSave;
        this.scenario = scenarioToPersist;
        this.errorManager = errorManager;
        this.configMetier = configMetier;
    }

    /**
     * @param scenario    le {@link EMHScenario} à valider
     * @param scenarioLog les logs de la validations
     */
    private static void validScenario(final EMHScenario scenario, CtuluLog scenarioLog) {
        // -- recherche des fichiers sous modeles --//
        final List<EMHModeleBase> fils = scenario.getModeles();
        if (fils.isEmpty()) {
            scenarioLog.addSevereError("noCurrentModele.error");
        } else if (fils.size() > 1) {
            scenarioLog.addSevereError("crue9.oneModele.authorized");
        }
    }

    private CrueData validOperation(CtuluLog analyzer) {
        ScenarioSaverCrue9.validScenario(scenarioToSave, analyzer);
        if (analyzer.isNotEmpty()) {
            if (analyzer.containsSevereError()) {
                return null;
            }
        }
        final CrueData data = CrueDataImpl.buildConcatFor(scenarioToSave, configMetier);
        validateCrueDataForCrue9(errorManager, data);
        return errorManager.containsFatalError() ? null : data;
    }

    /**
     * @param files la liste id->Dile
     * @return true si sauvegarde effectuée.
     */
    protected boolean save(final Map<String, File> files) {
        final CtuluLog analyzer = errorManager.createNewLog("crueSaver.validation");
        analyzer.setDescriptionArgs(scenarioToSave.getNom());
        final CrueData data = validOperation(analyzer);
        if (data == null) {
            return false;
        }

        final ManagerEMHModeleBase modele = scenario.getFils().get(0);
        if (!writeFile(files, modele, data, CrueFileType.DC, analyzer)) {
            return false;
        }
        return writeFile(files, modele, data, CrueFileType.DH, analyzer);
    }

    protected boolean export(final Map<CrueFileType, File> files) {
        final CtuluLog analyzer = errorManager.createNewLog("crueSaver.validation");
        analyzer.setDescriptionArgs(scenarioToSave.getNom());
        final CrueData data = validOperation(analyzer);
        if (data == null) {
            return false;
        }
        final ManagerEMHModeleBase modele = scenario.getFils().get(0);
        if (!writeFile(modele, data, CrueFileType.DC, files.get(CrueFileType.DC), analyzer)) {
            return false;
        }
        return writeFile(modele, data, CrueFileType.DH, files.get(CrueFileType.DH), analyzer);
    }

    private String getCommentaire(final CrueFileType crueFileType) {
        boolean isDC = (crueFileType == CrueFileType.DC);
        String commentaire;
        if (isDC) {
            EMHSousModele managerEMHSousModele = scenarioToSave.getModeles().get(0).getSousModeles().get(0);
            commentaire = managerEMHSousModele.getCommentairesManager().getCommentaire(CrueFileType.DRSO);
        } else {
            commentaire = scenarioToSave.getCommentairesManager().getCommentaire(CrueFileType.DCLM);
        }
        return commentaire;
    }

    private boolean writeFile(final Map<String, File> files, final ManagerEMHModeleBase modele, final CrueData data,
                              final CrueFileType fileType, CtuluLog prevalidation) {
        final String id = modele.getListeFichiers().getFile(fileType).getNom();
        final File file = files.get(id);
        return writeFile(modele, data, fileType, file, prevalidation);
    }

    private boolean writeFile(final ManagerEMHModeleBase modele, final CrueData data, final CrueFileType fileType, final File file,
                              CtuluLog prevalidation) {
        if (file == null) {
            prevalidation.addSevereError("loader.crue9.noDefined", fileType.toString(), modele.getNom());
            return false;
        }
        final CrueIOResu<CrueData> in = new CrueIOResu<>(data);
        in.setCrueCommentaire(getCommentaire(fileType));
        Crue9FileFormatFactory.getFileFormat(fileType).write(in, file, errorManager.createLog());
        return true;
    }

    protected void validateCrueDataForCrue9(CtuluLogGroup error, CrueData crueData) {
        ValidationHelper.validateAll(error, crueData.getScenarioData(), new ValidatorForCrue9Export(crueData.getCrueConfigMetier(),
                        true),
                new ValidatorForIDCrue9());
    }

    /**
     * @return the errorManager
     */
    public CtuluLogGroup getErrorManager() {
        return errorManager;
    }
}
