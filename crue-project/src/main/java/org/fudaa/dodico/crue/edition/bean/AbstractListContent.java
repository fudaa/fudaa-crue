/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition.bean;

import java.util.Collection;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

/**
 *
 * @author deniger
 */
public class AbstractListContent implements ObjetWithID {

  public static boolean nameUsed(final AbstractListContent toTest, final Collection<? extends AbstractListContent> others) {
    for (final AbstractListContent other : others) {
      if (other != toTest && other.getNom().equalsIgnoreCase(toTest.getNom())) {
        return true;
      }
    }
    return false;
  }
  protected String nom = StringUtils.EMPTY;

  @Override
  public String getNom() {
    return nom;
  }

  @Override
  public String getId() {
    return getNom();
  }

  public void setNom(final String nom) {
    this.nom = nom;
  }
}
