/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.transformer;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.dodico.crue.projet.report.data.ExternFileKey;

/**
 *
 * @author Frederic Deniger
 */
public class ExternFileKeyToStringTransformer extends AbstractPropertyToStringTransformer<ExternFileKey> {

  public ExternFileKeyToStringTransformer() {
    super(ExternFileKey.class);
  }

  @Override
  public String toStringSafe(ExternFileKey in) {
    return in.getFile() + ";" + in.getxVar() + ";" + in.getyVar();
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }

  @Override
  public ExternFileKey fromStringSafe(String in) {
    String[] split = StringUtils.split(in, ";");
    if (split.length == 3) {
      return new ExternFileKey(split[0], split[1], split[2]);
    }
    return ExternFileKey.NULL;
  }
}
