/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule.function;

import java.awt.EventQueue;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.editor.PostfixMathCommandEnhancedI;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;
import org.fudaa.dodico.crue.projet.report.data.ReportRunEmhKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.nfunk.jep.ParseException;

/**
 *
 * @author Frederic Deniger
 */
public abstract class AbstractAggregationFunction implements FormuleMathCommand, PostfixMathCommandEnhancedI {

  private ReportRunKey runKey;
  private String emhNom;
  private Collection<ResultatTimeKey> selectedTimeInRapport;

  private final EnumFormuleStat statKey;

  private final String errorStackNull = BusinessMessages.getString("aggregation.internError");
  private final String errorNotRun = BusinessMessages.getString("aggregation.internError");
  private final String errorNotEmh = BusinessMessages.getString("aggregation.internError");
  private final String errorNotVariable = BusinessMessages.getString("aggregation.variableNotCorrect");

  private final ReportGlobalServiceContrat reportGlobalService;

  public AbstractAggregationFunction(final EnumFormuleStat statKey, final ReportGlobalServiceContrat reportGlobalService) {
    this.statKey = statKey;
    this.reportGlobalService = reportGlobalService;
  }

  /**
   *
   * @param onlyTransient true si on doit recuperer les pas de temps transient. Si false, seulement les permanents.
   * @return la liste des ResultatTimeKey du rapport
   */
  private List<ResultatTimeKey> filterSelectedTimeInRapport(final boolean onlyTransient) {
    final List<ResultatTimeKey> res = new ArrayList<>();
    if (selectedTimeInRapport != null) {
      for (final ResultatTimeKey resultatTimeKey : selectedTimeInRapport) {
        if (resultatTimeKey.isTransitoire() == onlyTransient) {
          res.add(resultatTimeKey);
        }
      }
    }
    return res;
  }

  @Override
  public boolean needQuotes() {
    return true;
  }

  @Override
  public boolean setConfig(final ResultatTimeKey selectedTime, final ReportRunKey runKey, final String emhNom, final Collection<ResultatTimeKey> selectedTimeInRapport) {
    this.runKey = runKey;
    this.emhNom = emhNom;
    this.selectedTimeInRapport = selectedTimeInRapport;
    return true;
  }

  @Override
  public void run(final Stack aStack) throws ParseException {
    if (null == aStack) {
      throw new ParseException(errorStackNull);
    }
    if (runKey == null) {
      throw new ParseException(errorNotRun);
    }
    if (emhNom == null) {
      throw new ParseException(errorNotEmh);
    }
    final Object variable = aStack.pop();
    if (!(variable instanceof String)) {
      throw new ParseException(errorNotVariable);
    }
    final String var = StringUtils.uncapitalize((String) variable);

    final AggregationCacheKey cacheKey = new AggregationCacheKey(new ReportRunEmhKey(runKey, emhNom), statKey, var);
    Double resValue = reportGlobalService.getCachedValue(cacheKey);
    if (resValue == null) {
      resValue = computeValue(var);
      reportGlobalService.putCachedValue(cacheKey, resValue);
    }
    aStack.push(resValue);
  }

  @Override
  public int getNumberOfParameters() {
    return 1;
  }

  @Override
  public void setCurNumberOfParameters(final int n) {
//    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean checkNumberOfParameters(final int n) {
    return n == getNumberOfParameters();
  }

  public Double computeValue(final String var) {
    if (EventQueue.isDispatchThread()) {
      final ProgressRunnable<Double> runnable = handle -> computeValue(var, handle);
      return reportGlobalService.showProgressDialogAndRun(runnable, statKey.getNom() + " " + StringUtils.capitalize(var) + ": " + emhNom);
    } else {
      return computeValue(var, null);
    }
  }

  private Double computeValue(final String var, final ProgressHandle handle) {
    final Double resValue;
    final EnumSelectionTime timeSelection = statKey.getTimeSelection();
    List<ResultatTimeKey> selectedTimes = null;
    if (null != timeSelection) {
      switch (timeSelection) {
        case ALL_PERMANENT:
          selectedTimes = reportGlobalService.getAllPermanentTimeKeys();
          break;
        case ALL_TRANSIENT:
          selectedTimes = reportGlobalService.getAllTransientTimeKeys();
          break;
        case SELECTION_PERMANENT:
          selectedTimes = filterSelectedTimeInRapport(false);
          break;
        case SELECTION_TRANSIENT:
          selectedTimes = filterSelectedTimeInRapport(true);
          break;
        case ALL:
          selectedTimes = new ArrayList<>();
          selectedTimes.addAll(reportGlobalService.getAllPermanentTimeKeys());
          selectedTimes.addAll(reportGlobalService.getAllTransientTimeKeys());
          break;
      }
    }
    final StatComputer statComputer = StatComputer.createFrom(statKey);
    assert selectedTimes!=null;
    assert statComputer!=null;
    final ReportRunVariableKey key = new ReportRunVariableKey(runKey, reportGlobalService.getResultService().createVariableKey(var));
    for (final ResultatTimeKey resultatTimeKey : selectedTimes) {
      if (handle != null) {
        handle.setDisplayName(statKey.getNom() + " " + StringUtils.capitalize(var) + ": " + emhNom + " / " + resultatTimeKey.toString());
      }
      final Double value = reportGlobalService.getResultService().getValue(resultatTimeKey, key, emhNom, selectedTimeInRapport);
      if (value != null) {
        statComputer.addValue(resultatTimeKey, value);
      }
    }
    return Double.valueOf(statComputer.getFinalValue());
  }
}
