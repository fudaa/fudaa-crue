/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report;

import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.formule.function.AggregationCacheKey;
import org.netbeans.api.progress.ProgressRunnable;

import java.util.List;

/**
 * Un contrat pour un service global de gestion de post-traitement
 *
 * @author Frederic Deniger
 */
public interface ReportGlobalServiceContrat {
    /**
     * @return le contrat (interface) du service permettant de fournir les résultats demandés
     */
    ReportResultProviderServiceContrat getResultService();

    /**
     * @param cacheKey la clé du cache demandé
     * @return la valeur cachée. Null si non trouvée
     */
    Double getCachedValue(AggregationCacheKey cacheKey);

    /**
     * @param cacheKey la clé du cache demandé
     * @param resValue la valeur à cacher
     */
    void putCachedValue(AggregationCacheKey cacheKey, Double resValue);

    /**
     * @return liste de tous les pas de temps permanents
     */
    List<ResultatTimeKey> getAllPermanentTimeKeys();

    /**
     * @return liste de tous les pas de temps transitoires
     */
    List<ResultatTimeKey> getAllTransientTimeKeys();

    /**
     * @param operation   l'opération a lancer
     * @param displayName le nom de l'opération
     * @param <T>         le type du résultat
     * @return le résultat obtenu
     */
    <T> T showProgressDialogAndRun(final ProgressRunnable<T> operation, final String displayName);
}
