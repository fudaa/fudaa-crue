/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("ReportVerticalTimeKey")
public class ReportVerticalTimeKey implements ReportKeyContract {

  public static final ReportVerticalTimeKey INSTANCE = new ReportVerticalTimeKey();

  private ReportVerticalTimeKey() {
  }

  public static boolean isTimeKey(Object o) {
    return o != null && ReportVerticalTimeKey.class.equals(o.getClass());
  }

  @Override
  public String getVariableName() {
    return null;
  }

  @Override
  public ReportKeyContract duplicateWithNewVar(String newName) {
    return this;
  }

  @Override
  public boolean isExtern() {
    return false;
  }

  @Override
  public ReportRunKey getReportRunKey() {
    return null;
  }
}
