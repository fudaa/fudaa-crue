/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.export;

import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportMultiVarConfig;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableEmhKey;
import org.fudaa.dodico.crue.projet.report.excel.ReportExcelFormatData;

/**
 *
 * @author Frederic Deniger
 */
public class ReportExportMultiVar extends ReportExportAbstract<ReportMultiVarConfig, ReportRunVariableEmhKey> {

  public ReportExportMultiVar(ReportMultiVarConfig config, ReportResultProviderServiceContrat resultService) {
    super(config, resultService);
  }

  @Override
  protected void writeInBook(Workbook wb, List<ReportRunVariableEmhKey> variablesToExport, List<ResultatTimeKey> times) {

    String sheetName
            = CtuluResource.CTULU.getString("Feuille {0}", CtuluLibString.getString(1));
    ReportExcelFormatData formater = new ReportExcelFormatData(wb);
    Sheet currentSheet = wb.createSheet(sheetName);
    //Titres
    int rowIdx = 0;
    Row row = currentSheet.createRow(rowIdx++);
    int idx = 0;
    for (ReportRunVariableEmhKey variable : variablesToExport) {
      row.createCell(idx++).setCellValue(
              variable.getRunVariableKey().getVariable().getVariableDisplayName() + BusinessMessages.ENTITY_SEPARATOR + variable.getEmhName() + BusinessMessages.ENTITY_SEPARATOR + variable.
              getReportRunKey().
              getDisplayName());
    }
    for (ResultatTimeKey time : times) {
      idx = 0;
      row = currentSheet.createRow(rowIdx++);
      for (ReportRunVariableEmhKey variable : variablesToExport) {
        Double val = resultService.getValue(time, variable.getRunVariableKey(), variable.getEmhName(), getSelectedTimeStepInReport());
        final Cell cell = row.createCell(idx++);
        if (val != null) {
          cell.setCellValue(val.doubleValue());
          final CellStyle style = formater.getStyle(resultService.getPropertyNature(variable.getVariableName()), DecimalFormatEpsilonEnum.COMPARISON);
          if (style != null) {
            cell.setCellStyle(style);
          }
        }
      }

    }
  }

}
