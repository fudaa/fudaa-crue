/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.calcul;

import org.fudaa.dodico.calcul.CalculExec;

/**
 *
 * @author deniger
 */
public interface ExecConfigurer<T extends CalculExec> {

  void configure(T calculExec);
}
