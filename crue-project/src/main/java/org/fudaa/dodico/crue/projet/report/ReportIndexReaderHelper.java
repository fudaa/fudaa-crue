/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report;

import java.io.File;
import java.util.List;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;

/**
 *
 * @author Frederic Deniger
 */
public class ReportIndexReaderHelper {

  private final EMHProjet projet;

  public ReportIndexReaderHelper(EMHProjet projet) {
    this.projet = projet;
  }

  public CrueIOResu<List<ReportViewLineInfo>> readType(ReportContentType type) {
    File dir = new File(projet.getInfos().getDirOfRapports(), type.getFolderName());
    File index = new File(dir, ReportIndexReader.INDEX_FILENAME);
    ReportIndexReader reader = new ReportIndexReader();
    return reader.read(index);
  }

  public ReportViewLineInfoAndType containsReportWithIdOrDescription(ReportContentType type, String descriptionOrId) {
    final CrueIOResu<List<ReportViewLineInfo>> readType = readType(type);
    if (readType == null) {
      return null;
    }
    for (ReportViewLineInfo info : readType.getMetier()) {
      ReportViewLineInfoAndType infoAndType = new ReportViewLineInfoAndType(type, info);
      if (infoAndType.isSameId(descriptionOrId)) {
        return infoAndType;
      }
    }
    return null;
  }

}
