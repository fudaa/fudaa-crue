/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.bean.BrancheEditionContent;
import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.metier.helper.PropertyFinder;
import org.fudaa.dodico.crue.metier.helper.PropertyFinder.DescriptionIndexed;
import org.fudaa.dodico.crue.validation.ValidateInitialConditions;
import org.fudaa.dodico.crue.validation.ValidationHelper;
import org.fudaa.dodico.crue.validation.util.DelegateValidatorBranche;
import org.fudaa.dodico.crue.validation.util.ValidationContentSectionDansBrancheExecutor;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe a utiliser pour modifier une branche.
 *
 * @author Frédéric Deniger
 */
public class EditionChangeBranche {
  private final CreationDefaultValue defaultValues;
  private final boolean updateBrancheTypeOnly;

  public EditionChangeBranche(final CreationDefaultValue defaultValues) {
    this(defaultValues, false);
  }

  public EditionChangeBranche(final CreationDefaultValue defaultValues, boolean updateBrancheTypeOnly) {
    this.defaultValues = defaultValues;
    this.updateBrancheTypeOnly = updateBrancheTypeOnly;
  }

  public CtuluLog changeBranche(final CatEMHBranche initBranche, final BrancheEditionContent newContent, final CrueConfigMetier ccm, final boolean renameSection) {
    final Map<String, CatEMHNoeud> noeudsByNoms = TransformerHelper.toMapOfNom(initBranche.getParent().getNoeuds());
    final CatEMHNoeud noeudAmont = noeudsByNoms.get(newContent.getNoeudAmont());
    final CatEMHNoeud noeudAval = noeudsByNoms.get(newContent.getNoeudAval());
    final CtuluLog log = updateBrancheTypeOnly ? new CtuluLog(BusinessMessages.RESOURCE_BUNDLE) : prevalidate(newContent, initBranche, noeudAmont, noeudAval, ccm, renameSection);
    //on s'arrête:
    if (log.containsErrorOrSevereError()) {
      return log;
    }
    //Toutes ces relations sont reaffectées par la suite:
    //on enleve toutes les relations avant l'eventuel changement de type:
    EMHRelationFactory.removeRelationBidirect(initBranche, initBranche.getNoeudAval());
    EMHRelationFactory.removeRelationBidirect(initBranche, initBranche.getNoeudAmont());
    //on enleve la section pilote
    final CatEMHSection sectionPilote = EMHHelper.getSectionPilote(initBranche);
    if (sectionPilote != null) {
      EMHRelationFactory.removeSectionPilote(initBranche);
    }

    //changement de type:attention l'instance sera changée
    CatEMHBranche branche = initBranche;
    final EnumBrancheType newType = newContent.getType();
    final EMHSousModele sousModele = branche.getParent();
    if (!newType.equals(branche.getBrancheType())) {
      branche = changeType(ccm, sousModele, initBranche, newContent, newType);
    }
    if (!updateBrancheTypeOnly) {
      applyModifications(branche, newContent, sousModele, ccm, noeudAmont, noeudAval, renameSection);
    }

    return log;
  }

  private void applyModifications(final CatEMHBranche branche, final BrancheEditionContent newContent, final EMHSousModele sousModele, final CrueConfigMetier ccm,
                                  final CatEMHNoeud noeudAmont, final CatEMHNoeud noeudAval, final boolean renameSection) {
    final EnumBrancheType newType = branche.getBrancheType();
    branche.setCommentaire(newContent.getCommentaire());

    new EditionRename().rename(branche, newContent.getNom(), renameSection);
    if (newContent.isActive() != branche.getUserActive()) {
      branche.setUserActive(newContent.isActive());
      final EditionChangeActivity activity = new EditionChangeActivity();
      activity.propagateActivityChanged(sousModele.getParent(), branche, ccm);
    }

    branche.setNoeudAmont(noeudAmont);
    branche.setNoeudAval(noeudAval);

    if (requireDcsp(newType)) {
      final DonCalcSansPrt dcsp = EMHHelper.selectFirstInstanceOf(branche.getInfosEMH(), DonCalcSansPrt.class);
      applyModification(newContent.getDcsp(), dcsp);
    }
    if (EnumBrancheType.EMHBrancheSaintVenant.equals(newType)) {
      final DonPrtGeoBrancheSaintVenant dptg = EMHHelper.selectFirstOfClass(branche.getInfosEMH(), DonPrtGeoBrancheSaintVenant.class);
      applyModification(newContent.getDptgSaintVenant(), dptg);
      final List<RelationEMHSectionDansBrancheSaintVenant> saintVenantCoeff = newContent.getSaintVenantCoeff();
      final int size = saintVenantCoeff.size();
      final List<RelationEMHSectionDansBranche> currentSections = branche.getSections();
      assert size == currentSections.size() - 1;
      for (int i = 0; i < size; i++) {
        applyModification(saintVenantCoeff.get(i), currentSections.get(i));
      }
    }

    if (newContent.getSectionPilote() != null) {
      EMHRelationFactory.addSectionPilote(branche, newContent.getSectionPilote());
    }
    final List<EnumBrancheType> brancheTypeWithDistanceNotNull = DelegateValidatorBranche.getBrancheTypeWithDistanceNotNull();
    if (brancheTypeWithDistanceNotNull.contains(newType)) {
      final RelationEMHSectionDansBranche sectionAval = branche.getSectionAval();
      sectionAval.setXp(newContent.getDistance());
    }
  }

  private CatEMHBranche changeType(final CrueConfigMetier ccm, final EMHSousModele sousModele, final CatEMHBranche initBranche, final BrancheEditionContent newContent,
                                   final EnumBrancheType newType) {
    //on enleve les relations
    final List<RelationEMHSectionDansBranche> currentSections = initBranche.getListeSectionsSortedXP(ccm);
    for (final RelationEMHSectionDansBranche relationEMHSectionDansBranche : currentSections) {
      EMHRelationFactory.removeRelationBidirect(initBranche, relationEMHSectionDansBranche.getEmh());
    }
    final List<RelationEMH> sousModeleContientBranche = EMHRelationFactory.findRelationWithTarget(sousModele, initBranche);
    //pour la création on enlève l'ancienne branche
    sousModele.getParent().getParent().getIdRegistry().unregister(initBranche);
    //l'ancienne branche n'est pas attachée au sous-modele.
    //par contre, la relation sous-modele contient branche est conservée afin de garder l'ordre.
    EMHRelationFactory.removeRelation(initBranche, sousModele);
    //branche contient désormais la nouvelle instance
    final CatEMHBranche branche = EditionBrancheCreator.createSimpleBranche(newContent.getNom(), newType);
    //on reinitialise l'uid:
    branche.setUiId(initBranche.getUiId());
    sousModele.getParent().getParent().getIdRegistry().register(branche);
    assert sousModeleContientBranche.size() == 1;
    //pour garder l'ordre initial:
    sousModeleContientBranche.get(0).setEmh(branche);
    //on initialise la relation inverse:
    branche.addRelationEMH(new RelationEMHDansSousModele(sousModele));
    if (updateBrancheTypeOnly) {
      return branche;
    }
    //dcsp
    final EditionBrancheDcspCreator dcspCreator = new EditionBrancheDcspCreator();
    final DonCalcSansPrt newDcsp = dcspCreator.createDcsp(ccm, branche);
    if (newDcsp != null) {//par exemple, pour les strickler, il est possible que le dcsp soit null 
      branche.addInfosEMH(newDcsp);
    }
    final boolean moveToSaintVenant = EnumBrancheType.EMHBrancheSaintVenant.equals(newType);
    //dptg pour Saint-Venant
    if (moveToSaintVenant) {
      branche.addInfosEMH(new DonPrtGeoBrancheSaintVenant(ccm));
    }
    //les conditions initiales.
    final List<DonPrtCIni> oldDptis = initBranche.getDPTI();
    if (CollectionUtils.isNotEmpty(oldDptis)) {
      //un seul
      assert oldDptis.size() == 1;
      final DonPrtCIniBranche oldDpti = (DonPrtCIniBranche) oldDptis.get(0);
      final DonPrtCIniBranche newDpti = ValidateInitialConditions.createDefaultDonPrtCiniBranche(newType, ccm);
      newDpti.initFrom(oldDpti);
      branche.addInfosEMH(newDpti);
    }
    //les sections...
    CatEMHSection amont = currentSections.get(0).getEmh();
    CatEMHSection aval = currentSections.get(currentSections.size() - 1).getEmh();
    final Collection<EnumSectionType> authorizedType = ValidationContentSectionDansBrancheExecutor.createSectionInterneTypeDansBrancheContents().
      get(newType);
    final EditionCreateEMH creator = new EditionCreateEMH(defaultValues);
    if (!authorizedType.contains(amont.getSectionType())) {
      if (authorizedType.contains(EnumSectionType.EMHSectionSansGeometrie)) {
        amont = creator.sectionCreator.createEMHSectionSansGeometrieAmont(branche, ccm);
      } else {
        amont = creator.sectionCreator.createEMHSectionProfilAmont(branche, ccm);
      }
    }
    if (!authorizedType.contains(aval.getSectionType())) {
      if (authorizedType.contains(EnumSectionType.EMHSectionSansGeometrie)) {
        aval = creator.sectionCreator.createEMHSectionSansGeometrieAval(branche, ccm);
      } else {
        aval = creator.sectionCreator.createEMHSectionProfilAval(branche, ccm);
      }
    }
    final RelationEMHSectionDansBranche relationAmont = EMHRelationFactory.createSectionDansBrancheAndSetRelation(branche, amont, 0, ccm);
    relationAmont.setPos(EnumPosSection.AMONT);
    final RelationEMHSectionDansBranche relationAval = EMHRelationFactory.createSectionDansBrancheAndSetRelation(branche, aval, 0, ccm);
    relationAval.setPos(EnumPosSection.AVAL);
    branche.addRelationEMH(relationAmont);
    branche.addRelationEMH(relationAval);
    return branche;
  }

  /**
   * Copie les propriétés (avec l'annotation
   * \@PropertyDesc) <b>hormis les lois</b>.
   *
   * @param source l'objet source
   * @param target l'objet modifie
   */
  private static void applyModification(final Object source, final Object target) {
    assert source.getClass().equals(target.getClass());
    final PropertyFinder finder = new PropertyFinder();
    final List<DescriptionIndexed> availablePropertiesNotSorted = finder.getAvailablePropertiesNotSorted(source.getClass());
    for (final DescriptionIndexed descriptionIndexed : availablePropertiesNotSorted) {
      final String name = descriptionIndexed.description.property;
      //les lois ne sont pas modifiées:
      if (Loi.class.isAssignableFrom(descriptionIndexed.description.propertyClass)) {
        continue;
      }
      try {
        final Object newValue = PropertyUtils.getProperty(source, name);
        PropertyUtils.setSimpleProperty(target, name, newValue);
      } catch (final Exception exception) {
        Logger.getLogger(EditionChangeBranche.class.getName()).log(Level.SEVERE, "applyModification", exception);
      }
    }
    if (DonCalcSansPrtBrancheBarrageFilEau.class.equals(source.getClass())) {
      final DonCalcSansPrtBrancheBarrageFilEau dcspSource = (DonCalcSansPrtBrancheBarrageFilEau) source;
      final DonCalcSansPrtBrancheBarrageFilEau dcspTarget = (DonCalcSansPrtBrancheBarrageFilEau) target;
      dcspTarget.setElemBarrage(dcspSource.getElemBarrage());
    }
    if (DonCalcSansPrtBrancheSeuilLateral.class.equals(source.getClass())) {
      final DonCalcSansPrtBrancheSeuilLateral dcspSource = (DonCalcSansPrtBrancheSeuilLateral) source;
      final DonCalcSansPrtBrancheSeuilLateral dcspTarget = (DonCalcSansPrtBrancheSeuilLateral) target;
      dcspTarget.setElemSeuilAvecPdc(dcspSource.getElemSeuilAvecPdc());
      dcspTarget.setFormulePdc(dcspSource.getFormulePdc());
    }
    if (DonCalcSansPrtBrancheSeuilTransversal.class.equals(source.getClass())) {
      final DonCalcSansPrtBrancheSeuilTransversal dcspSource = (DonCalcSansPrtBrancheSeuilTransversal) source;
      final DonCalcSansPrtBrancheSeuilTransversal dcspTarget = (DonCalcSansPrtBrancheSeuilTransversal) target;
      dcspTarget.setElemSeuilAvecPdc(dcspSource.getElemSeuilAvecPdc());
      dcspTarget.setFormulePdc(dcspSource.getFormulePdc());
    }
    if (DonCalcSansPrtBrancheOrifice.class.equals(source.getClass())) {
      applyModification(((DonCalcSansPrtBrancheOrifice) source).getElemOrifice(), ((DonCalcSansPrtBrancheOrifice) target).getElemOrifice());
    }
  }

  private CtuluLog prevalidate(final BrancheEditionContent newContent, final CatEMHBranche branche, final CatEMHNoeud noeudAmont, final CatEMHNoeud noeudAval,
                               final CrueConfigMetier ccm, final boolean renameSections) {
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final EditionRename rename = new EditionRename();
    if (!newContent.getNom().equals(branche.getNom())) {
      final String nameValid = rename.isNameValid(branche, newContent.getNom(), renameSections);
      if (nameValid != null) {
        log.addSevereError(nameValid);
      }
    }
    if (noeudAmont == null) {
      log.addSevereError("AddEMH.BrancheNoeudAmontUnknown", newContent.getNom(), newContent.getNoeudAmont());
    }
    if (noeudAval == null) {
      log.addSevereError("AddEMH.BrancheNoeudAvalUnknown", newContent.getNom(), newContent.getNoeudAval());
    } else if (noeudAval.equals(noeudAmont)) {
      log.addSevereError("AddEMH.BrancheNoeudAmontSameThanAval", newContent.getNom());
    }
    final EnumBrancheType newType = newContent.getType();

    if (ValidationContentSectionDansBrancheExecutor.requireSectionPilote(newType)) {
      if (newContent.getSectionPilote() == null) {
        log.addSevereError("ChangeEMHBranche.SectionPiloteRequired");
      }
    }
    final Pair<String, Object[]> distanceValidMessage = EditionBrancheCreator.isDistanceValidMessage(newType, newContent.getDistance(), ccm);
    if (distanceValidMessage != null) {
      log.addSevereError(distanceValidMessage.first, distanceValidMessage.second);
    }
    if (newContent.getDcsp() != null) {
      ValidationHelper.validateObject(StringUtils.EMPTY, log, newContent.getDcsp(), ccm);
    }
    if (newContent.getDptgSaintVenant() != null) {
      ValidationHelper.validateObject(StringUtils.EMPTY, log, newContent.getDptgSaintVenant(), ccm);
    }
    if (newContent.getDcsp() == null && requireDcsp(newType)) {
      log.addSevereError("ChangeEMHBranche.DcspRequired");
    }
    if (EnumBrancheType.EMHBrancheSaintVenant.equals(newType)) {
      if (newContent.getDptgSaintVenant() == null) {
        log.addSevereError("ChangeEMHBrancheSaintVenant.DptgRequired");
      }
      final List<RelationEMHSectionDansBrancheSaintVenant> saintVenantCoeff = newContent.getSaintVenantCoeff();
      if (CollectionUtils.isEmpty(saintVenantCoeff)) {
        log.addSevereError("ChangeEMHBrancheSaintVenant.RelationEMHSectionDansBrancheSaintVenantRequired");
      }
      int idx = 0;
      for (final RelationEMHSectionDansBrancheSaintVenant relationEMHSectionDansBrancheSaintVenant : saintVenantCoeff) {
        ValidationHelper.validateObject(BusinessMessages.getString("ChangeEMHBrancheSaintVenant.ValidationRelation", Integer.toString(++idx)), log,
          relationEMHSectionDansBrancheSaintVenant, ccm);
      }
    }

    return log;
  }

  private boolean requireDcsp(final EnumBrancheType newType) {
    //pour PDC, le dcsp contient uniquement une loi qui n'est pas modifiable ici.
    return !EnumBrancheType.EMHBranchePdc.equals(newType) && !EnumBrancheType.EMHBrancheStrickler.equals(newType);
  }
}
