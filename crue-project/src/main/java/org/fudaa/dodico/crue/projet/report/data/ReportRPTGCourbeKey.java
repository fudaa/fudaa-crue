/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("ReportRPTGCourbeKey")
public class ReportRPTGCourbeKey implements ReportKeyContract {

  final String variable;
  final boolean main;
  public static final ReportRPTGCourbeKey NULL_VALUE = new ReportRPTGCourbeKey(StringUtils.EMPTY, false);

  public ReportRPTGCourbeKey(String variable, boolean main) {
    this.variable = variable;
    this.main = main;
  }

  @Override
  public String getVariableName() {
    return variable;
  }

  @Override
  public ReportKeyContract duplicateWithNewVar(String newName) {
    return new ReportRPTGCourbeKey(newName, isMain());
  }

  public String getVariable() {
    return variable;
  }

  @Override
  public ReportRunKey getReportRunKey() {
    return null;
  }

  @Override
  public boolean isExtern() {
    return false;
  }

  @Override
  public int hashCode() {
    return 18 + variable.hashCode() * 7 + (main ? 1 : -1);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ReportRPTGCourbeKey other = (ReportRPTGCourbeKey) obj;
    if ((this.variable == null) ? (other.variable != null) : !this.variable.equals(other.variable)) {
      return false;
    }
    if (this.main != other.main) {
      return false;
    }
    return true;
  }

  public boolean isMain() {
    return main;
  }
}
