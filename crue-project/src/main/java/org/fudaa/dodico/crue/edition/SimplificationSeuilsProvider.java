/*
 GPL 2
 */
package org.fudaa.dodico.crue.edition;

/**
 *
 * @author Frederic Deniger
 */
public interface SimplificationSeuilsProvider {

  SimplificationSeuils getSeuils();
}
