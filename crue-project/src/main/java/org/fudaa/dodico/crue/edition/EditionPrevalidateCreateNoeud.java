/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.edition.bean.AbstractListContent;
import org.fudaa.dodico.crue.edition.bean.ListNoeudContent;
import org.fudaa.dodico.crue.edition.bean.ValidationMessage;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;

/**
 *
 * @author Frédéric Deniger
 */
public class EditionPrevalidateCreateNoeud {

  public ValidationMessage<ListNoeudContent> validate(final EMHSousModele sousModele, final Collection<ListNoeudContent> contents) {
    final ValidationMessage<ListNoeudContent> res = new ValidationMessage<>();
    final CtuluLog log = res.log;
    final List<EMH> noeuds = sousModele.getParent().getParent().getIdRegistry().getEMHs(EnumCatEMH.NOEUD);
    final Set<String> ids = TransformerHelper.toSetOfId(noeuds);
    for (final ListNoeudContent content : contents) {
      final String nom = content.getNom();
      final List<CtuluLogRecord> msgs = new ArrayList<>();

      if (!ValidationPatternHelper.isNameValide(nom, EnumCatEMH.NOEUD)) {
        msgs.add(log.addSevereError("AddEMH.NameNotValid", nom));
      }

      if (AbstractListContent.nameUsed(content, contents)) {
        msgs.add(log.addSevereError("AddEMH.NameAlreadyUsedInNewEMH", nom));
      }

      if (ids.contains(nom.toUpperCase())) {
        msgs.add(log.addSevereError("AddEMH.NameAlreadyUsedByExistingEMH", nom));
      }
      if (!msgs.isEmpty()) {
        res.accepted = false;
        final ValidationMessage.ResultItem<ListNoeudContent> item = new ValidationMessage.ResultItem<>();
        item.bean = content;
        res.messages.add(item);
        for (final CtuluLogRecord ctuluLogRecord : msgs) {
          BusinessMessages.updateLocalizedMessage(ctuluLogRecord);
          item.messages.add(ctuluLogRecord.getLocalizedMessage());
        }
      }
    }

    return res;
  }
}
