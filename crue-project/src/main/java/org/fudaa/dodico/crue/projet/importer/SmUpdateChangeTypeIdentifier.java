package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateSourceTargetData;

public class SmUpdateChangeTypeIdentifier {
  public SmUpdateChangeTypeIdentifier() {
  }

  public CtuluIOResult<SmUpdateSourceTargetData> extractChange(SmUpdateSourceTargetData sourcesToImport) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    SmUpdateSourceTargetData data = new SmUpdateSourceTargetData();
    log.setDesc("sm.updater.typeChangedLog");
    sourcesToImport.getCasiers().forEach(casierLine -> {
      checkCasierChanged(casierLine, log, data);
    });
    sourcesToImport.getBranches().forEach(brancheLine -> {
      checkBrancheChanged(brancheLine, log, data);
    });
    sourcesToImport.getNoeuds().forEach(noeudLine -> {
      checkNoeudChanged(noeudLine, log, data);
    });
    sourcesToImport.getSections().forEach(sectionLine -> {
      checkSectionChanged(sectionLine, log, data);
    });
    return new CtuluIOResult<>(log, data);
  }

  private void checkBrancheChanged(SmUpdateSourceTargetData.Line<CatEMHBranche> emh, CtuluLog newLog, SmUpdateSourceTargetData data) {
    if (!emh.getSourceToImport().getBrancheType().equals(emh.getTargetToModify().getBrancheType())) {
      newLog.addWarn("sm.updater.typeChangedBranchesLogItem", emh.getSourceToImport().getNom(), emh.getTargetToModify().getBrancheType().geti18n(),
        emh.getSourceToImport().getBrancheType().geti18n());
    } else {
      data.addBrancheLine(emh);
    }
  }

  private void checkSectionChanged(SmUpdateSourceTargetData.Line<CatEMHSection> emh, CtuluLog newLog, SmUpdateSourceTargetData data) {
    if (!emh.getSourceToImport().getSectionType().equals(emh.getTargetToModify().getSectionType())) {
      newLog.addWarn("sm.updater.typeChangedSectionsLogItem", emh.getSourceToImport().getNom(), emh.getTargetToModify().getSectionType().geti18n(),
        emh.getSourceToImport().getSectionType().geti18n());
    } else {
      data.addSectionLine(emh);
    }
  }

  private void checkCasierChanged(SmUpdateSourceTargetData.Line<CatEMHCasier> emh, CtuluLog newLog, SmUpdateSourceTargetData data) {
    if (!emh.getSourceToImport().getCasierType().equals(emh.getTargetToModify().getCasierType())) {
      newLog.addWarn("sm.updater.typeChangedCasiersLogItem", emh.getSourceToImport().getNom(), emh.getTargetToModify().getCasierType().geti18n(),
        emh.getSourceToImport().getCasierType().geti18n());
    } else {
      data.addCasierLine(emh);
    }
  }

  private void checkNoeudChanged(SmUpdateSourceTargetData.Line<CatEMHNoeud> emh, CtuluLog newLog, SmUpdateSourceTargetData data) {
    if (!emh.getSourceToImport().getNoeudType().equals(emh.getTargetToModify().getNoeudType())) {
      newLog.addWarn("sm.updater.typeChangedNoeudsLogItem", emh.getSourceToImport().getNom(), emh.getTargetToModify().getNoeudType().geti18n(),
        emh.getSourceToImport().getNoeudType().geti18n());
    } else {
      data.addNoeudLine(emh);
    }
  }
}
