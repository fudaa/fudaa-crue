/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.export;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;
import org.fudaa.dodico.crue.projet.report.data.ReportKeyContract;
import org.fudaa.dodico.crue.projet.report.persist.AbstractReportCourbeConfig;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Frederic Deniger
 */
public abstract class ReportExportAbstract<T extends AbstractReportCourbeConfig, K extends ReportKeyContract> {

  protected final T config;
  protected final ReportResultProviderServiceContrat resultService;

  public ReportExportAbstract(T config, ReportResultProviderServiceContrat resultService) {
    this.config = config;
    this.resultService = resultService;
  }

  public Collection<ResultatTimeKey> getSelectedTimeStepInReport() {
    return config.getSelectedTimeStepInReport();
  }

  public void export(List<K> variablesToExport, List<ResultatTimeKey> times, File target) {
    Workbook wb = new XSSFWorkbook();
    writeInBook(wb, variablesToExport, times);
    writeToFile(wb, target);

  }

  protected abstract void writeInBook(Workbook wb, List<K> variablesToExport, List<ResultatTimeKey> times);

  protected void writeToFile(Workbook wb, File target) {
    FileOutputStream fileOut = null;
    try {
      fileOut = new FileOutputStream(target);
      wb.write(fileOut);
      fileOut.close();
    } catch (IOException ex) {
      Logger.getLogger(ReportExportAbstract.class.getName()).log(Level.SEVERE, null, ex);
    } finally {
      CtuluLibFile.close(fileOut);
    }
  }
}
