/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

/**
 * @author Frederic Deniger
 */
public enum ReportVariableTypeEnum implements ObjetWithID {

  READ("read"),//lue directement dans le fichier de résultats (RCAL)
  TIME("time"),//issue du pas de temps sélectionné
  EXPR("expr"),//issue d'une expression (nom, commentaire, ou nom du profil)
  FORMULE("formule"),//issue d'une variable
  LIMIT_PROFIL("limit"),//utilisé uniquement pour les profils en long et pour retrouver les limite des lits/étiquettes
  RESULTAT_RPTI("resultat_rpti"),//résultats issus du fichier RPTI (Zini et/ou Qini)
  LHPT("lhpt");//les lois lhpt
  final String persistId;

  ReportVariableTypeEnum(String persistId) {
    this.persistId = persistId;
  }

  public String getPersistId() {
    return persistId;
  }

  @Override
  public String getId() {
    return getPersistId();
  }

  @Override
  public String getNom() {
    return getId();
  }
}
