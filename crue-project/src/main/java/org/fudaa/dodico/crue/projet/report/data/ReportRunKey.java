/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("ReportRunKey")
public class ReportRunKey implements ObjetNomme, ReportKeyContract {

  final ReportRunEnum runType;
  final String runAlternatifId;
  final String managerNom;
  String displayName;
  public static final ReportRunKey NULL_VALUE = new ReportRunKey();

  public ReportRunKey(String managerNom, String runAlternatifId) {
    this.runAlternatifId = runAlternatifId;
    this.runType = ReportRunEnum.ALTERNATIF;
    this.managerNom = managerNom;
    displayName = managerNom + " / " + runAlternatifId;
  }

  public String getManagerNom() {
    return managerNom;
  }

  @Override
  public String getVariableName() {
    return null;
  }

  @Override
  public ReportKeyContract duplicateWithNewVar(String newName) {
    return this;
  }

  @Override
  public ReportRunKey getReportRunKey() {
    return this;
  }

  @Override
  public boolean isExtern() {
    return false;
  }

  public boolean isAlternatifRun() {
    return !isExtern() && !isCourant();
  }

  public void updateNom(ManagerEMHScenario managerScenario, EMHRun run) {
    displayName = managerScenario.getNom() + " / " + run.getNom();
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  @Override
  public String getId() {
    return getDisplayName();
  }

  @Override
  public String getNom() {
    return getDisplayName();
  }

  @Override
  public void setNom(String newNom) {
    setDisplayName(newNom);
  }

  public ReportRunKey() {
    this.runType = ReportRunEnum.CURRENT;
    runAlternatifId = null;
    managerNom = null;
  }

  public String getRunAlternatifId() {
    return runAlternatifId;
  }

  public String getDisplayName() {
    if (displayName != null) {
      return displayName;
    }
    if (isCourant()) {
      return BusinessMessages.getString("RunCourant.DisplayName");
    }
    return runAlternatifId;
  }

  @Override
  public int hashCode() {
    int hash = 3 + runType.hashCode() * 13;
    if (runAlternatifId != null) {
      hash = hash + runAlternatifId.hashCode() * 7;
    }
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ReportRunKey other = (ReportRunKey) obj;
    if (this.runType != other.runType) {
      return false;
    }
    if ((this.runAlternatifId == null) ? (other.runAlternatifId != null) : !this.runAlternatifId.equals(other.runAlternatifId)) {
      return false;
    }
    return true;
  }

  public ReportRunEnum getRunType() {
    return runType;
  }

  public boolean isCourant() {
    return ReportRunEnum.CURRENT == runType;

  }
}
