package org.fudaa.dodico.crue.edition;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.bean.BrancheSplitContent;
import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.dodico.crue.validation.ValidateInitialConditions;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Permet de découper une branche
 *
 * @author deniger
 */
public class EditionBrancheSaintVenantInternSplit {
    private final CrueConfigMetier crueConfigMetier;

    public EditionBrancheSaintVenantInternSplit(final CrueConfigMetier crueConfigMetier) {
        this.crueConfigMetier = crueConfigMetier;
    }

    /**
     * @param branche la branche a decouper ( qui deviendra la branche amont)
     * @param section la section qui sera le point de découpage ( deviendra la section aval de la branche amont)
     * @return le contenu permettant à l'utilisateur de prévisualiser les modifications
     */
    public CrueOperationResult<BrancheSplitContent> createContent(final CatEMHBranche branche, final CatEMHSection section) {

        final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
        log.setDesc("EditionBrancheSplit.CreateContent.LogContent");
        log.setDescriptionArgs(branche.getNom());
        //on valide d'abord la faisabilité
        validSplit(branche, section, log);
        if (log.containsErrorOrSevereError()) {
            return new CrueOperationResult<>(null, log);
        }
        final BrancheSplitContent content = new BrancheSplitContent();
        content.brancheNameAmontToSplit = branche.getNom();
        content.sectionNameToSplitOn = section.getNom();
        content.brancheToSplitUid =branche.getUiId();
        content.sectionToSplitOnUid =section.getUiId();
        final EMHScenario scenario = branche.getParent().getParent().getParent();
        final UniqueNomFinder uniqueNomFinder = new UniqueNomFinder();
        content.newBrancheNameAval = uniqueNomFinder.findNewName(EnumCatEMH.BRANCHE, scenario);
        content.newNoeudName = uniqueNomFinder.findNewName(EnumCatEMH.NOEUD, scenario);

        final List<RelationEMHSectionDansBranche> listeSections = branche.getListeSections();
        //la relation contenant la section utilisée pour le découpage
        RelationEMHSectionDansBranche toSplitFound = null;
        double xp = 0;
        for (final RelationEMHSectionDansBranche emhSectionDansBranche : listeSections) {
            if (toSplitFound != null) {
                content.addSectionContent(emhSectionDansBranche.getEmhNom(), emhSectionDansBranche.getEmh().getSectionType(), emhSectionDansBranche.getXp() - xp);
            }
            if (toSplitFound == null && emhSectionDansBranche.getEmh().getId().equals(section.getId())) {
                toSplitFound = emhSectionDansBranche;
                xp = emhSectionDansBranche.getXp();
            }
        }

        return new CrueOperationResult<>(content, log);
    }

    private void validSplit(final CatEMHBranche branche, final CatEMHSection section, final CtuluLog log) {
        if (branche.getBrancheType() != EnumBrancheType.EMHBrancheSaintVenant) {
            log.addError("EditionBrancheSplit.BrancheIsNotSaintVenant");
        }
        final RelationEMHSectionDansBranche sectionAval = branche.getSectionAval();
        final RelationEMHSectionDansBranche sectionAmont = branche.getSectionAmont();
        if (section.getId().equals(sectionAmont.getEmhId())) {
            log.addError("EditionBrancheSplit.SectionIsSectionAmont", branche.getNom(), section.getNom());
        }
        if (section.getId().equals(sectionAval.getEmhId())) {
            log.addError("EditionBrancheSplit.SectionIsSectionAval", branche.getNom(), section.getNom());
        }
        if (!section.getBranche().getId().equals(branche.getId())) {
            log.addError("EditionBrancheSplit.SectionNotInBranche", branche.getNom(), section.getNom());
        }
    }

    /**
     * @param scenario le scenario concerné
     * @return le resultat de l'operaiont
     */
    public CtuluLog split(final EMHScenario scenario, final BrancheSplitContent splitContent) {
        final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);

        //on valide
        //la branche amont est la branche a spliter au niveau de splitContent
        //une nouvelle branche avale va être créée
        final EMHBrancheSaintVenant brancheAmont = (EMHBrancheSaintVenant) scenario.getIdRegistry().findByName(splitContent.brancheNameAmontToSplit);
        final double initLength=brancheAmont.getLength();
        final CatEMHSection section = (CatEMHSection) scenario.getIdRegistry().findByName(splitContent.sectionNameToSplitOn);




        validSplitAction(scenario, brancheAmont, section, splitContent, log);
        if (log.containsErrorOrSevereError()) {
            return log;
        }
        final List<RelationEMHSectionDansBranche> listeSections = new ArrayList<>(brancheAmont.getListeSections());
        final List<RelationEMHSectionDansBranche> listeToMoveToNewSection = new ArrayList<>();

        //la relation contenant la section utilisée pour le découpage
        RelationEMHSectionDansBrancheSaintVenant relationToSplitOn = null;
        double xp = 0;
        for (RelationEMHSectionDansBranche listeSection : listeSections) {
            final RelationEMHSectionDansBrancheSaintVenant emhSectionDansBranche = (RelationEMHSectionDansBrancheSaintVenant) listeSection;
            if (relationToSplitOn != null) {
                EMHRelationFactory.removeRelationBidirect(brancheAmont, emhSectionDansBranche.getEmh());
                emhSectionDansBranche.setXp(emhSectionDansBranche.getXp() - xp);
                listeToMoveToNewSection.add(emhSectionDansBranche);
            }
            if (relationToSplitOn == null && emhSectionDansBranche.getEmh().getId().equals(section.getId())) {
                relationToSplitOn = emhSectionDansBranche;
                xp = emhSectionDansBranche.getXp();
            }
        }
        if (relationToSplitOn == null) {
            return log;
        }

        //la section utilisée pour découper la branche est la nouvelle section aval de la branche amont
        relationToSplitOn.setPos(EnumPosSection.AVAL);

        final EditionCreateEMH creator = new EditionCreateEMH(new CreationDefaultValue());
        final EMHSousModele sousModele = section.getParent();


        final CatEMHNoeud noeudAval = brancheAmont.getNoeudAval();

        //on créé le nouveau noeud et on l'affecte
        final CatEMHNoeud newNoeudAmont = creator.createNoeud(splitContent.newNoeudName, sousModele, EnumNoeudType.EMHNoeudNiveauContinu, crueConfigMetier);


        //pour la branche amont on reaffecte le noeud aval
        EMHRelationFactory.removeRelationBidirect(brancheAmont, noeudAval);
        //le nouveau noeud devient le noeud aval de la branche amont
        brancheAmont.setNoeudAval(newNoeudAmont);
        //on crée la branche de saint-venant
        final EMHBrancheSaintVenant newBrancheSaintVenant = new EMHBrancheSaintVenant(splitContent.newBrancheNameAval);
        newBrancheSaintVenant.setNoeudAmont(newNoeudAmont);
        newBrancheSaintVenant.setNoeudAval(noeudAval);
        newBrancheSaintVenant.setUserActive(brancheAmont.getUserActive());

        //la nouvelle section
        final CatEMHSection newSection = createNewSectionIdemDetached(scenario, creator, section, splitContent.newBrancheNameAval);
        EditionCreateEMH.addInScenario(sousModele, newSection, scenario);
        //la nouvelle relation emh
        final RelationEMHSectionDansBrancheSaintVenant firstSectionRelation = new RelationEMHSectionDansBrancheSaintVenant(crueConfigMetier);
        firstSectionRelation.setPos(EnumPosSection.AMONT);
        firstSectionRelation.setEmh(newSection);
        firstSectionRelation.setCoefConv(relationToSplitOn.getCoefConv());
        firstSectionRelation.setCoefDiv(relationToSplitOn.getCoefDiv());
        firstSectionRelation.setCoefPond(relationToSplitOn.getCoefPond());
        firstSectionRelation.setXp(0);

        listeToMoveToNewSection.add(0, firstSectionRelation);
        //on affecte toutes les relations
        newBrancheSaintVenant.addAllRelations(listeToMoveToNewSection);
        listeToMoveToNewSection.forEach(relationEMHSectionDansBranche ->
                EMHRelationFactory.addBrancheContientSection(newBrancheSaintVenant, relationEMHSectionDansBranche.getEmh())
        );

        //les infos emh sont dupliquées de la branche amont:
        final DonPrtGeoBrancheSaintVenant amontDptg = EMHHelper.selectFirstOfClass(brancheAmont.getInfosEMH(), DonPrtGeoBrancheSaintVenant.class);
        final DonCalcSansPrtBrancheSaintVenant amontDcsp = EMHHelper.selectFirstOfClass(brancheAmont.getInfosEMH(), DonCalcSansPrtBrancheSaintVenant.class);
        if (amontDptg != null) {
            newBrancheSaintVenant.addInfosEMH(new DonPrtGeoBrancheSaintVenant(amontDptg));
        }
        if (amontDcsp != null) {
            newBrancheSaintVenant.addInfosEMH(new DonCalcSansPrtBrancheSaintVenant(amontDcsp));
        }
        final InfosEMH dpti = EMHHelper.selectInfoEMH(brancheAmont, EnumInfosEMH.DON_PRT_CINI);
        if (dpti != null) {
            newBrancheSaintVenant.addInfosEMH(((DonPrtCIni) dpti).deepClone());
        }

        //le zini pour ce nouveau noeud
        DonPrtCIni ciniAmont = ValidateInitialConditions.getCini(brancheAmont.getNoeudAmont());
        DonPrtCIni ciniAval = ValidateInitialConditions.getCini(noeudAval);
        DonPrtCIni ciniForNewNoeud = ValidateInitialConditions.getCini(newNoeudAmont);

        if(ciniAmont!=null && ciniAval!=null && ciniForNewNoeud!=null){
            double ziniAmont=((DonPrtCIniNoeudNiveauContinu)ciniAmont).getZini();
            double ziniAval=((DonPrtCIniNoeudNiveauContinu)ciniAval).getZini();
            double alpha=xp/initLength;
            ((DonPrtCIniNoeudNiveauContinu)ciniForNewNoeud).setZini((1-alpha)*ziniAmont+alpha*ziniAval);
        }

        EditionCreateEMH.addInScenario(sousModele, newBrancheSaintVenant, scenario);

        return log;
    }

    /**
     * Permet de valider l'action de découpage.
     *
     * @param scenario     le scenario
     * @param branche      la branche decoupe
     * @param section      la section support du decoupage
     * @param splitContent les données du découpage
     * @param log          le log qui contiendra les résultats de validation
     */
    private void validSplitAction(final EMHScenario scenario, final EMHBrancheSaintVenant branche, final CatEMHSection section, final BrancheSplitContent splitContent,
                                  final CtuluLog log) {
        if (branche == null) {
            log.addError("EditionBrancheSplit.BrancheToSplitNotFound", splitContent.newBrancheNameAval);
        }
        if (section == null) {
            log.addError("EditionBrancheSplit.SectionToSplitNotFound", splitContent.sectionNameToSplitOn);
        }
        if (!isSectionUsableForSplit(section)) {
            log.addError("EditionBrancheSplit.SectionToSplitMustBeProfilOrIdem");
        }
        final String newBrancheNameValidation = ValidationPatternHelper.isNameValidei18nMessage(splitContent.newBrancheNameAval, EnumCatEMH.BRANCHE);
        if (newBrancheNameValidation != null) {
            log.addError(newBrancheNameValidation);
        }
        final String newNoeudNameValidation = ValidationPatternHelper.isNameValidei18nMessage(splitContent.newNoeudName, EnumCatEMH.NOEUD);
        if (newNoeudNameValidation != null) {
            log.addError(newNoeudNameValidation);
        }

        if (scenario.getIdRegistry().findByName(splitContent.newBrancheNameAval) != null) {
            log.addError("EditionBrancheSplit.BrancheNameAlreadyExists", splitContent.newBrancheNameAval);
        }
        if (scenario.getIdRegistry().findByName(splitContent.newNoeudName) != null) {
            log.addError("EditionBrancheSplit.NoeudNameAlreadyExists", splitContent.newNoeudName);
        }

        if (branche != null && section != null) {
            validSplit(branche, section, log);
        }
    }

    /**
     * @param section la section a tester
     * @return true si la section peut être support d'un decoupage d'une branche de Saint-Venant
     */
    public static boolean isSectionUsableForSplit(CatEMHSection section) {
        return section != null && (section.getSectionType() == EnumSectionType.EMHSectionIdem || section.getSectionType() == EnumSectionType.EMHSectionProfil);
    }

    private EMHSectionIdem createNewSectionIdemDetached(final EMHScenario scenario, final EditionCreateEMH creator, final CatEMHSection section, final String brancheName) {
        final EditionSectionCreator sectionCreator = creator.sectionCreator;
        final String sectionName = getNewSectionName(scenario, creator.nomFinder, brancheName);

        final EnumSectionType sectionType = section.getSectionType();
        switch (sectionType) {
            case EMHSectionIdem:
                final EMHSectionIdem idem = (EMHSectionIdem) section;
                final DonPrtGeoSectionIdem dptg = EMHHelper.getDPTGSectionIdem(idem);
                final double dz = dptg.getDz();
                return sectionCreator.createSectionIdemDetached(sectionName, crueConfigMetier, dz, idem.getSectionRef());
            case EMHSectionProfil:
                return sectionCreator.createSectionIdemDetached(sectionName, crueConfigMetier, 0, section);
        }
        return null;
    }

    /**
     * @param scenario    le scenario parent
     * @param nomFinder   le fonction de recherche de nouveau nom
     * @param brancheName le nom de la branche
     */
    public static String getNewSectionName(final EMHScenario scenario, final UniqueNomFinder nomFinder, final String brancheName) {
        String sectionName = BusinessMessages.getString("create.SectionAmont", CruePrefix.getNomAvecPrefixFor(brancheName, EnumCatEMH.SECTION));
        sectionName = nomFinder.findUniqueName(EnumCatEMH.SECTION, scenario, sectionName);
        return sectionName;
    }
}
