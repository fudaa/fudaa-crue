/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule.function;

import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.nfunk.jep.function.PostfixMathCommandI;

import java.util.Collection;

/**
 * @author Frederic Deniger
 */
public interface FormuleMathCommand extends PostfixMathCommandI {
    /**
     * @param currentTime           le temps courant
     * @param runKey                la clé du run
     * @param emhNom                le nom de l'EMH
     * @param selectedTimeInRapport les temps selectionnés
     * @return true si un résultat est possible
     */
    boolean setConfig(ResultatTimeKey currentTime, ReportRunKey runKey, String emhNom, Collection<ResultatTimeKey> selectedTimeInRapport);
}
