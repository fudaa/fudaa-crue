/*
 GPL 2
 */
package org.fudaa.dodico.crue.edition.bean;

import gnu.trove.TLongObjectHashMap;
import gnu.trove.TLongObjectIterator;
import java.util.List;
import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;

/**
 *
 * @author Frederic Deniger
 */
public class DonCLimMLineContent {

  private final Calc calc;
  protected final TLongObjectHashMap<List<DonCLimM>> dclmsByEMHId;

  public DonCLimMLineContent(Calc calc, TLongObjectHashMap<List<DonCLimM>> dclmsByEMHId) {
    this.calc = calc;
    this.dclmsByEMHId = dclmsByEMHId;
  }

  public Calc getCalc() {
    return calc;
  }

  public List<DonCLimM> getCLimM(long uid) {
    return dclmsByEMHId.get(uid);
  }

  public TLongObjectHashMap<List<DonCLimM>> getDclmsByEMHId() {
    return dclmsByEMHId;
  }

  /**
   *
   * @return le nombre max de dclm affecté à une EMH
   */
  public int getMaxDclmByEMH() {
    int nb = 1;
    for (TLongObjectIterator<List<DonCLimM>> it = dclmsByEMHId.iterator(); it.hasNext();) {
      it.advance();
      if (it.value() != null) {
        nb = Math.max(nb, it.value().size());
      }
    }
    return nb;

  }

  public boolean setCLimM(long uid, DonCLimM clim, int idx) {
    DonCLimM oldClimM = getCLimM(uid).get(idx);
    if (oldClimM == null || clim == oldClimM || ObjectUtils.equals(oldClimM, clim)) {
      return false;
    }
    clim.setEmh(oldClimM.getEmh());
    clim.setCalculParent(oldClimM.getCalculParent());
    getCLimM(uid).set(idx, clim);
    return true;
  }
}
