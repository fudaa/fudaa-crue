/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.FileDeleteResult;
import org.fudaa.dodico.crue.common.*;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory.VersionResult;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.*;
import org.fudaa.dodico.crue.projet.calcul.RunScenarioPair;
import org.fudaa.dodico.crue.projet.create.InfosCreation;
import org.fudaa.dodico.crue.projet.create.RunCreator;
import org.fudaa.dodico.crue.projet.create.RunCreatorOptions;
import org.fudaa.dodico.crue.projet.create.ScenarioConverterProcess;

import java.io.File;
import java.util.*;
import java.util.Map.Entry;

/**
 * @author CANEL Christophe
 */
public class EMHProjetController {
  private final EMHProjet projet;
  private final ConnexionInformation connexionInformation;

  public EMHProjetController(final EMHProjet projet, final ConnexionInformation connexionInformation) {
    super();
    this.projet = projet;
    this.connexionInformation = connexionInformation;
  }

  public ConnexionInformation getConnexionInformation() {
    return connexionInformation;
  }

  /**
   * Efface tous les runs de tous les scenarios de l'etude
   *
   * @return la liste des noms des scenarios pour lesquels l'effacement n'a pas été complet.
   */
  public Map<RunScenarioPair, FileDeleteResult> deleteAllRunsOfEtu() {
    final List<ManagerEMHScenario> listeScenarios = projet.getListeScenarios();
    final Map<RunScenarioPair, FileDeleteResult> scenarioWithRunNotCleaned = new HashMap<>();
    for (final ManagerEMHScenario managerEMHScenario : listeScenarios) {
      deleteAllRuns(managerEMHScenario, scenarioWithRunNotCleaned);
    }
    return scenarioWithRunNotCleaned;
  }

  /**
   * Parcours tous les runs du scénario et efface les répertoires. Supprime tous les répertoires des runs du scenario
   *
   * @param scenario le {@link EMHScenario}
   * @param map      contiendra l'état de la suppression par Run/Scenario
   * @return true si le dossier des runs est bien supprimé.
   */
  public boolean deleteAllRuns(final ManagerEMHScenario scenario, final Map<RunScenarioPair, FileDeleteResult> map) {
    final List<EMHRun> listeRuns = new ArrayList<>(scenario.getListeRuns());
    if (CollectionUtils.isNotEmpty(listeRuns)) {
      deleteRuns(scenario, listeRuns, map);
    } else {
      return true;
    }
    return cleanRunDir(scenario);
  }

  /**
   * @param scenario  le scenario
   * @param listeRuns les runs a supprimer
   * @param map       la map qui contiendra l'état de la suppression par Run/Scenario
   */
  public void deleteRuns(final ManagerEMHScenario scenario, final List<EMHRun> listeRuns, final Map<RunScenarioPair, FileDeleteResult> map) {
    for (final EMHRun run : listeRuns) {
      final FileDeleteResult fileResult = new FileDeleteResult();
      deleteRun(scenario, run, fileResult);
      if (fileResult.isNotEmpty()) {
        if (map != null) {
          map.put(new RunScenarioPair(scenario, run), fileResult);
        }
      }
    }
  }

  /**
   * si le scenario contient des runs ne fait rien. S'il ne contient pas de run et si le dossier des runs existe, le supprime s'il est vide.
   *
   * @param scenario le {@link EMHScenario}
   * @return true si le dossier a pu etre completement supprimé.
   */
  public boolean cleanRunDir(final ManagerEMHScenario scenario) {
    final List<EMHRun> listeRuns = scenario.getListeRuns();
    //contient des run: on ne fait rien:
    if (CollectionUtils.isNotEmpty(listeRuns)) {
      return true;
    }
    final File mainDirOfRuns = projet.getMainDirOfRuns(scenario);
    if (!mainDirOfRuns.exists()) {
      return true;
    }
    return CtuluLibFile.deleteDir(mainDirOfRuns);
  }

  public EMHProjet getProjet() {
    return projet;
  }

  /**
   * Appele #cleanRunDir après afin de nettoyer le dossier des runs.
   *
   * @param scenario   le {@link EMHScenario}
   * @param run        le {@link EMHRun}
   * @param fileResult resultat de l'operation
   * @return true si suppression effective
   */
  public boolean deleteRun(final ManagerEMHScenario scenario, final EMHRun run, final FileDeleteResult fileResult) {
    if (run == null) {
      return false;
    }
    final EMHRun runInScenario = scenario.getRunFromScenar(run.getId());
    if (runInScenario == null) {
      return false;
    }
    if (scenario.containRun(runInScenario)) {
      scenario.getInfosVersions().updateForm(connexionInformation);
      final File dirForRun = projet.getDirForRun(scenario, run);
      if (dirForRun.exists()) {
        CtuluLibFile.deleteDir(dirForRun, fileResult);
      }
      final boolean removed = scenario.removeRun(runInScenario);
      cleanRunDir(scenario);
      return removed;
    }
    return false;
  }

  public CrueOperationResult<EMHScenarioContainer> convertScenario(final String name, final InfosCreation infos, final File etuFile) {
    final CtuluLogGroup errors = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    errors.setDescription("project.convertScenario");
    errors.setDescriptionArgs(name);
    final ManagerEMHScenario sourceScenario = projet.getScenario(name);

    if (sourceScenario == null) {
      errors.createNewLog("project.convertScenario.mainLog").addSevereError("projet.controller.scenarioNameNotExist", name);

      return new CrueOperationResult<>(null, errors);
    }

    final ScenarioConverterProcess converter = new ScenarioConverterProcess(projet);
    final CrueOperationResult<EMHScenarioContainer> result = converter.convert(sourceScenario, infos);

    errors.addGroup(result.getLogs());

    if (!errors.containsFatalError()) {
      //c'est ce qui va supprimer les runs
      this.addScenarioToProjectAndSave(result.getResult(), etuFile, errors);
      //on copie les config dans ce cas:
      if (sourceScenario.isCrue10() && result.getResult().getManagerEMHScenario().isCrue10()) {
        //copie des fichier dreg
        if (Crue10VersionConfig.isUpperThan_V1_2(projet.getCoeurConfig())) {
          copyDregFiles(sourceScenario, result);
        }
        copyConfigFiles(sourceScenario, result);
      }
    }

    return new CrueOperationResult<>(result.getResult(), errors.createCleanGroup());
  }

  private void copyConfigFiles(ManagerEMHScenario sourceScenario, CrueOperationResult<EMHScenarioContainer> result) {
    final List<ManagerEMHSousModele> initSousModeles = sourceScenario.getAllSousModeles();
    final List<ManagerEMHSousModele> targetSousModeles = result.getResult().getManagerEMHScenario().getAllSousModeles();
    assert (initSousModeles.size() == targetSousModeles.size());
    for (int i = 0; i < initSousModeles.size(); i++) {
      final ManagerEMHSousModele iniSousModele = initSousModeles.get(i);
      final ManagerEMHSousModele targetSousModele = targetSousModeles.get(i);
      final File config = projet.getInfos().getDirOfConfig(iniSousModele);
      if (CtuluLibFile.exists(config)) {
        final File target = projet.getInfos().getDirOfConfig(targetSousModele);
        CrueFileHelper.copyDirWithSameLastDate(config, target);
      }
    }
  }

  /**
   * Copie les fichier dreg de la source pour chaque modele cible.
   * @param sourceScenario le scenario source
   * @param result la cible
   */
  private void copyDregFiles(ManagerEMHScenario sourceScenario, CrueOperationResult<EMHScenarioContainer> result) {
    final List<ManagerEMHModeleBase> initModeles = sourceScenario.getFils();
    final List<ManagerEMHModeleBase> targetModeles = result.getResult().getManagerEMHScenario().getFils();
    assert (initModeles.size() == targetModeles.size());
    for (int i = 0; i < initModeles.size(); i++) {
      final ManagerEMHModeleBase initModele = initModeles.get(i);
      final ManagerEMHModeleBase targetModele = targetModeles.get(i);
      File initDregFile = initModele.getFinalFile(CrueFileType.DREG, projet);
      File targetDregFile = targetModele.getFinalFile(CrueFileType.DREG, projet);
      if (CtuluLibFile.exists(initDregFile) && targetDregFile != null) {
        CrueFileHelper.copyFileWithSameLastModifiedDate(initDregFile, targetDregFile);
      }
    }
  }

  private void addScenarioToProjectAndSave(final EMHScenarioContainer emhScenarioContainer, final File etuFile, final CtuluLogGroup errors) {
    final ManagerEMHScenario managerScenario = emhScenarioContainer.getManagerEMHScenario();
    final ManagerEMHScenario existantScenario = projet.getScenario(managerScenario.getNom());
    //on nettoie les runs:
    if (existantScenario != null && existantScenario.getListeRuns() != null) {
      final List<EMHRun> listeRuns = existantScenario.getListeRuns();
      for (final EMHRun eMHRun : listeRuns) {
        final File dirForRun = projet.getDirForRun(existantScenario, eMHRun);
        CtuluLibFile.deleteDir(dirForRun);
      }
      existantScenario.setRunCourant(null);
      existantScenario.setListeRuns(Collections.emptyList());
      if (!cleanRunDir(existantScenario)) {
        final File mainDirOfRuns = projet.getMainDirOfRuns(existantScenario);
        errors.createNewLog("cleanRunDirs.logDescription").addInfo("cleanRunDirs.logItem", mainDirOfRuns.getAbsolutePath());
      }
    }
    projet.addScenario(managerScenario);

    for (final ManagerEMHModeleBase modele : managerScenario.getFils()) {
      projet.addBaseModele(modele);
    }

    for (final ManagerEMHSousModele sousModele : managerScenario.getAllSousModeles()) {
      projet.addBaseSousModele(sousModele);
    }

    for (final FichierCrueParModele fichier : managerScenario.getAllFileUsed().values()) {
      projet.getInfos().addCrueFileToProject(fichier.getFichier());
    }

    this.saveScenario(emhScenarioContainer.getEmhScenario(), managerScenario, etuFile, errors);
  }

  private void saveScenario(final EMHScenario emhScenario, final ManagerEMHScenario scenario, final File etuFile, final CtuluLogGroup errors) {
    final ScenarioSaverProcess saver = new ScenarioSaverProcess(emhScenario, scenario, projet);
    errors.addGroup(saver.saveWithScenarioType());

    writeProjet(etuFile, this.projet, errors, connexionInformation);
  }

  public void saveProjet(final File etuFile, final CtuluLogGroup errors) {
    writeProjet(etuFile, this.projet, errors, connexionInformation);
  }

  public static void writeProjet(final File etuFile, final EMHProjet projet, final CtuluLogGroup errors, final ConnexionInformation connexionInformation) {
    final EMHProjectInfos infos = projet.getInfos();
    infos.getInfosVersions().setAuteurDerniereModif(connexionInformation.getCurrentUser());
    infos.getInfosVersions().setDateDerniereModif(connexionInformation.getCurrentDate());
    final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getVersion(CrueFileType.ETU, projet.getCoeurConfig());
    fileFormat.writeMetierDirect(projet, etuFile, errors.createLog(), projet.getPropDefinition());
  }

  public CrueOperationResult<EMHRun> createRun(final String scenarioName, final File etuFile) {
    return this.createRun(scenarioName, new RunCreatorOptions(projet.getScenario(scenarioName).getRunCourant()), etuFile);
  }

  public CrueIOResu<Boolean> reloadRun(final ManagerEMHScenario scenario, final EMHRun run) {
    final Map<String, File> toOveridde = projet.getInputFiles(scenario);
    final Map<String, File> copyFrom = projet.getInputFiles(scenario, run);
    final FileIntegrityManager mng = new FileIntegrityManager(toOveridde.values());
    mng.start();
    final Set<Entry<String, File>> entrySet = toOveridde.entrySet();
    boolean ok = true;
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    for (final Entry<String, File> entry : entrySet) {
      final File from = copyFrom.get(entry.getKey());
      if (from.exists()) {
        final boolean copyOk = CrueFileHelper.copyFileWithSameLastModifiedDate(from, entry.getValue());
        if (!copyOk) {
          ok = false;
          log.addError("reloadRun.copyFailed", from.getName());
          break;
        }
      } else {
        log.addError("reloadRun.runFileNotFound", from.getAbsolutePath());
        ok = false;
      }
    }

    mng.finish(ok);
    if (ok) {
      scenario.setRunCourant(run);
    }
    return new CrueIOResu<>(Boolean.valueOf(ok), log);
  }

  public CrueOperationResult<EMHRun> createRun(final String scenarioName, final RunCreatorOptions options, final File etuFile) {
    final RunCreator creator = new RunCreator(projet, connexionInformation);
    final ManagerEMHScenario scenario = projet.getScenario(scenarioName);
    final CrueOperationResult<EMHRun> result = creator.create(scenario, options);
    final CtuluLogGroup errors = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    errors.setDescription("project.createRun");
    errors.setDescriptionArgs(scenarioName);
    EMHRun run = result.getResult();

    errors.addGroup(result.getLogs());

    final File runDir = projet.getDirForRun(scenario, run);
    if (errors.containsFatalError()) {

      if (runDir.isDirectory()) {
        if (CtuluLibFile.deleteDir(runDir)) {
          errors.createNewLog("run.runCleanerIfCreationFailed").addSevereError("run.runRemover.cannotDeleteFile", runDir.getName());
        }
      }

      run = null;
    } else {
      scenario.addRunToScenario(run);
      scenario.setRunCourant(run);
      scenario.getInfosVersions().updateForm(connexionInformation);
      //le scenario courant doit être mis à jour :

      writeProjet(etuFile, this.projet, errors, connexionInformation);
      if (scenario.isCrue10()) {
        final ManagerEMHScenario oldScenario = projet.getScenarioCourant();
        this.projet.setScenarioCourant(scenario);
        try {
          writeProjet(new File(runDir, etuFile.getName()), this.projet, errors, connexionInformation);
        } catch (final Exception e) {
        } finally {
          this.projet.setScenarioCourant(oldScenario);
        }
      }
    }

    return new CrueOperationResult<>(run, errors);
  }

  public static CrueOperationResult<EMHProjet> readProjet(final File fichier, final CoeurConfigContrat config, final boolean validation) {
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("load.project");
    log.setDescriptionArgs(fichier.getAbsolutePath());

    //test versions:
    final VersionResult findVersion = Crue10FileFormatFactory.findVersion(fichier);
    if (findVersion.getLog().containsErrorOrSevereError()) {
      return new CrueOperationResult<>(null, findVersion.getLog());
    }
    if (!findVersion.getVersion().equals(config.getXsdVersion())) {
      log.addSevereError("etude.versionDifferentFromCoeur", fichier.getName(), findVersion.getVersion(), config.getXsdVersion());
      return new CrueOperationResult<>(null, log);
    }
    //fin
    final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ETU, config);
    final boolean ok = validation ? fileFormat.isValide(fichier, log) : true;
    if (ok) {
      final EMHProjet projet = (EMHProjet) fileFormat.read(fichier, log, null).getMetier();

      if (log.containsSevereError()) {
        return new CrueOperationResult<>(null, log);
      }

      projet.setPropDefinition(config);
      return new CrueOperationResult<>(projet, log);
    }
    return new CrueOperationResult<>(null, log);
  }
}
