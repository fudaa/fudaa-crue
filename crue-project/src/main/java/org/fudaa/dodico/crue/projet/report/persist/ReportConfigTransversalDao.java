/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.persist;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("TransversalConfig")
public class ReportConfigTransversalDao extends AbstractReportConfigDao<ReportTransversalConfig> {

  @XStreamAlias("Transversal")
  private ReportTransversalConfig config;

  public ReportConfigTransversalDao() {
  }

  public ReportConfigTransversalDao(ReportTransversalConfig config) {
    this.config = config;
  }

  @Override
  public ReportTransversalConfig getConfig() {
    return config;
  }

  @Override
  public void setConfig(ReportTransversalConfig config) {
    this.config = config;
  }
}
