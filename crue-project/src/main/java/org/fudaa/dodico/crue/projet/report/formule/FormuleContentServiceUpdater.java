/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogGroupResult;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleContentServiceUpdater {

  private final FormuleServiceContent currentContent;
  private final ReportGlobalServiceContrat service;

  public FormuleContentServiceUpdater(FormuleServiceContent currentContent, ReportGlobalServiceContrat service) {
    this.currentContent = currentContent == null ? new FormuleServiceContent() : currentContent;
    this.service = service;
  }

  public CtuluLogGroupResult<FormuleServiceContent, FormuleDataChanges> computeChanges(FormuleDataEdited edited) {
    List<FormuleContent> oldVariables = currentContent.getVariables();
    FormuleDataChanges changes = new FormuleDataChanges();
    //les nouvelles variables
    List<FormuleContent> newVariables = new ArrayList<>();
    List<FormuleParameters> newParameters = edited.getNewParameters();
    CtuluLogGroup group = new CtuluLogGroup(null);
    Collection<String> newVariableNames = TransformerHelper.toId(edited.getAllParameters());
    for (FormuleParameters formuleParameters : newParameters) {
      FormuleContent content = formuleParameters.createContent(service);
      CtuluLog log = content.setParameter(formuleParameters, newVariableNames);
      if (log != null && log.containsErrorOrSevereError()) {
        log.setDesc(formuleParameters.getId());
        group.addLog(log);
      }
      newVariables.add(content);
    }
    //les variables modifiées
    Map<String, FormuleParameters> modifiedParametersByOldName = edited.getModifiedParametersByOldName();
    Map<String, FormuleContent> toMapOfNom = TransformerHelper.toMapOfId(oldVariables);
    for (Map.Entry<String, FormuleParameters> entry : modifiedParametersByOldName.entrySet()) {
      String oldName = entry.getKey();
      FormuleParameters newFormuleParameters = entry.getValue();
      if (!oldName.equals(newFormuleParameters.getId())) {
        changes.renamedVariables.put(oldName, newFormuleParameters.getId());
      }

      FormuleContent oldFormuleContent = toMapOfNom.get(oldName);
      if (oldFormuleContent.getParameters().isContentEqual(newFormuleParameters)) {
        oldFormuleContent.getParameters().setId(newFormuleParameters.getId());
        newVariables.add(oldFormuleContent);
      } else {
        changes.modifiedVariables.add(oldName);
        FormuleContent content = newFormuleParameters.createContent(service);
        CtuluLog log = content.setParameter(newFormuleParameters, newVariableNames);
        if (log != null && log.containsErrorOrSevereError()) {
          log.setDesc(newFormuleParameters.getId());
          group.addLog(log);
        } else {
          newVariables.add(content);
        }

      }
    }

    //les variables supprimées:
    List<FormuleParameters> allEditedParameters = edited.getAllParameters();
    Set<String> newNames = TransformerHelper.toSetOfId(allEditedParameters);
    for (FormuleContent oldContent : oldVariables) {
      if (!changes.renamedVariables.containsKey(oldContent.getParameters().getId())
              && !newNames.contains(oldContent.getParameters().getId())) {
        changes.removedVariables.add(oldContent.getParameters().getId());
      }
    }

    FormuleServiceContent newContent = new FormuleServiceContent(newVariables);
    return new CtuluLogGroupResult<>(newContent, changes, group);
  }
}
