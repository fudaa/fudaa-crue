/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition.bean;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.emh.EnumNoeudType;

/**
 *
 * @author deniger
 */
public class ListNoeudContent extends AbstractListContent {

  String commentaire = StringUtils.EMPTY;
  EnumNoeudType type=EnumNoeudType.EMHNoeudNiveauContinu;

  public ListNoeudContent() {
  }

  public ListNoeudContent(String nom) {
    this.nom = nom;
  }

  public EnumNoeudType getType() {
    return type;
  }

  public void setType(EnumNoeudType type) {
    this.type = type;
  }

  public String getCommentaire() {
    return commentaire;
  }

  public void setCommentaire(String commentaire) {
    this.commentaire = commentaire;
  }
}
