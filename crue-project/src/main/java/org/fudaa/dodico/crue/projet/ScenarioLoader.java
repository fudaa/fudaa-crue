/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.Crue10FileFormatOCAL;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.IdRegistry;
import org.fudaa.dodico.crue.metier.emh.ui.OrdCalcScenarioUiState;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.etude.RunFile;
import org.fudaa.dodico.crue.validation.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Classe qui se charge de charger en memoire un scénario donné.
 *
 * @author Adrien Hadoux
 */
public class ScenarioLoader {
  private final ManagerEMHScenario scenario;
  private final EMHProjet projet;
  /**
   * il s'agit du coeur utilise pour le chargement. Le projet possède un coeur par défaut qui n'est pas utilisé dans le chargement.
   */
  private final CoeurConfigContrat coeur;

  /**
   * @param scenario le scenario
   * @param projet le projet contenant
   */
  public ScenarioLoader(final ManagerEMHScenario scenario, final EMHProjet projet, final CoeurConfigContrat coeur) {
    super();
    this.scenario = scenario;
    this.projet = projet;
    this.coeur = coeur;
  }

  public static CtuluLogGroup validateScenario(EMHScenario emhScenario, CoeurConfigContrat coeur, boolean crue9,
                                               ScenarioAutoModifiedState modifiedState) {
    final CtuluLogGroup validationBilan = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    validationBilan.setDescription("loader.valid.group");
    final CtuluLog analyseValidationCOnnexite = ValidateConnectionModele.validateConnexite(emhScenario);
    validationBilan.addLog(analyseValidationCOnnexite);
    List<CrueValidator> validators = new ArrayList<>();
    validators.add(new ValidatorForIDCrue10());
    validators.add(new ValidatorForIDCrue9());
    final CrueConfigMetier crueConfigMetier = coeur.getCrueConfigMetier();
    validators.add(new ValidatorForValuesAndContents(crueConfigMetier));
    validators.add(new ValidateAndRebuildProfilSection(crueConfigMetier, modifiedState));
    validators.add(new ValidatorInitialConditions(crueConfigMetier, modifiedState));
    validators.add(new ValidatorNumberOfData(crueConfigMetier));
    validators.add(new ValidatorSousModeleContent());
    validators.add(new ValidatorNomCasierContent());
    validators.add(new ValidatorNomProfilCasierContent());
    validators.add(new ValidatorNomSectionContent());
    validators.add(new ValidatorORES(coeur, modifiedState));

    //cette validation doit être effectuée après la reconstruction des profil section
    if (!crue9) {
      validators.add(new ValidatorForCrue9Export(crueConfigMetier, false));
    }
    validators.add(new ValidatorWarnForInactiveContent());

    ValidationHelper.validateAll(validationBilan, emhScenario, validators);
    return validationBilan;
  }

  public static boolean isOptr(File f) {
    return f != null && f.getName().endsWith("." + CrueFileType.OPTR.getExtension());
  }

  public static boolean isLhpt(File f) {
    return f != null && f.getName().endsWith("." + CrueFileType.LHPT.getExtension());
  }

  /**
   * Realise la lecture.
   *
   * @return true si ok
   */
  public ScenarioLoaderOperation loadRunCourant() {
    final EMHRun runCourant = scenario.getRunCourant();
    return load(runCourant);
  }

  /**
   * @param run le run a charger. Sinon, seul le scenario est chargé.
   * @return resultat de l'operation
   */
  public ScenarioLoaderOperation load(final EMHRun run) {
    final CtuluLogGroup errorMng = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    ScenarioLoaderOperation res = loadScenario(run, errorMng);
    if (res == null) {
      return new ScenarioLoaderOperation(null, errorMng);
    }
    return res;
  }

  private ScenarioLoaderOperation loadScenario(final EMHRun run, final CtuluLogGroup errorMng) {
    errorMng.setDescription("loader.fichiers.scenario.chargt");
    CtuluLog prevalidation = errorMng.createNewLog("loader.fichiers.scenario.chargt.prevalidation");
    if (scenario == null) {
      prevalidation.addSevereError("not.scenario.error", "<unknown>");
      return new ScenarioLoaderOperation(null, errorMng);
    } else {
      errorMng.setDescriptionArgs(scenario.getNom());
    }

    final CtuluLogGroup loadScenarioBilan = errorMng.createGroup("loader.loadScenario.group");
    loadScenarioBilan.setDescriptionArgs(scenario.getNom());
    final CtuluLogGroup testFileExistBilan = loadScenarioBilan.createGroup("loader.fileExist.test");
    if (projet.getScenario(scenario.getNom()) == null) {
      prevalidation.addSevereError("not.scenario.error", scenario.getNom());
    }
    final boolean ok = validFiles(testFileExistBilan, run);
    if (!ok) {
      return null;
    }

    Map<String, File> files = projet.getInputFiles(scenario, run);
    Map<String, File> resFiles = null;
    if (run != null) {
      Map<String, RunFile> runFilesResultat = projet.getRunFilesResultat(scenario, run);
      if (runFilesResultat != null) {
        resFiles = new HashMap<>();
        for (Entry<String, RunFile> entry : runFilesResultat.entrySet()) {
          resFiles.put(entry.getKey(), entry.getValue().getFile());
        }
      }
    }
    EMHScenario emhScenario = null;
    boolean crue9 = false;
    ScenarioAutoModifiedState modifiedState = new ScenarioAutoModifiedState();
    if (scenario.getInfosVersions() != null && scenario.getInfosVersions().getType() != null) {
      crue9 = scenario.isCrue9();
      final CtuluLogGroup readFileBilan = loadScenarioBilan.createGroup("loader.readFile");

      if (crue9) {
        emhScenario = new ScenarioLoaderCrue9(scenario, readFileBilan, coeur, prevalidation).compute(files, resFiles, modifiedState);
      } else {
        File runDir = run == null ? null : projet.getDirForRun(scenario, run);
        emhScenario = new ScenarioLoaderCrue10(projet, scenario, readFileBilan, prevalidation,modifiedState).compute(files, resFiles,
            runDir, run);
      }
      final File uiOcalFile = projet.getInfos().getUiOCALFile(scenario);
      //si le fichier ocal de persistance des données ocal ui existe on le charge
      if (emhScenario != null && uiOcalFile != null && uiOcalFile.exists()) {
        final CtuluLog uiOcalLog = readFileBilan.createNewLog("loader.readUiOcal");
        final Crue10FileFormat fmt = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.OCAL, coeur);
        Crue10FileFormatOCAL ocalFmt = (Crue10FileFormatOCAL) fmt;
        final CrueIOResu<OrdCalcScenarioUiState> readUiState = ocalFmt.readUiState(uiOcalFile, uiOcalLog, coeur.getCrueConfigMetier());
        if (readUiState != null) {
          scenario.setUiOCalData(readUiState.getMetier());
        } else {
          final Crue10FileFormatFactory.VersionResult formatFinderResult = Crue10FileFormatFactory.findVersion(uiOcalFile);
          String version = formatFinderResult.getVersion();
          if (formatFinderResult.getLog().containsErrorOrSevereError()) {
            uiOcalLog.addWarn("loader.readUiOcal.errorInVersion", uiOcalFile.getName());
          } else if (!coeur.getXsdVersion().equals(formatFinderResult.getVersion())) {
            uiOcalLog.addWarn("crue10.scenarioVersionDifferentFromEtude", uiOcalFile.getName(), version, coeur.getXsdVersion());
          }
        }
      }
    }
    if (emhScenario == null) {
      return null;
    }
    if (emhScenario.sort()) {
      modifiedState.setReorderedDone();
    }

    emhScenario.setNom(scenario.getNom());

    CtuluLogGroup validationBilan = validateScenario(emhScenario, projet.getCoeurConfig(), crue9, modifiedState);
    errorMng.addGroup(validationBilan);

    final ScenarioLoaderOperation scenarioLoaderResult = new ScenarioLoaderOperation(emhScenario, errorMng);
    scenarioLoaderResult.setAutoModifiedState(modifiedState);
    IdRegistry.install(emhScenario);
    return scenarioLoaderResult;
  }

  private boolean validFiles(final CtuluLogGroup errorMng, EMHRun run) {
    // on teste la presence de fichiers
    Map<String, File> fileToLoad = projet.getInputFiles(scenario);
    boolean ok = validFileExists(errorMng, fileToLoad, "loader.fichier.etude.exist");
    if (run != null) {
      final Map<String, File> runFiles = projet.getInputFiles(scenario, run);
      ok = ok && validFileExists(errorMng, runFiles, "loader.fichier.run.exist");
      final CtuluLog analyze = warnIfResFileNotExists(projet.getRunFilesResultat(scenario, run));
      if (analyze != null && !analyze.isEmpty()) {
        errorMng.getLogs().add(analyze);
      }
    }
    return ok;
  }

  private boolean validFileExists(final CtuluLogGroup error, final Map<String, File> allFileUsed, final String title) {
    CtuluLog fileNotExist = null;
    boolean valid = true;
    for (final File f : allFileUsed.values()) {
      //en 1.1.1 le fichier optr n'existe pas.
      if (Crue10VersionConfig.V_1_1_1.equals(coeur.getXsdVersion()) && isOptr(f)) {
        continue;
      }
      if (isLhpt(f)) {
        continue;
      }
      if (f == null || !f.exists()) {
        valid = false;
        if (fileNotExist == null) {
          fileNotExist = error.createLog();
          fileNotExist.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
          fileNotExist.setDesc(title);
        }
        fileNotExist.addSevereError("loader.fichier.notExist.error", f == null ? "?" : f.toString());
      }
    }
    return valid;
  }

  private CtuluLog warnIfResFileNotExists(final Map<String, RunFile> allFileUsed) {
    CtuluLog fileNotExist = null;
    for (final RunFile runFile : allFileUsed.values()) {
      if (!CtuluLibFile.exists(runFile.getFile())) {
        if (fileNotExist == null) {
          fileNotExist = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
          fileNotExist.setDesc("loader.resFichier.exist");
        }
        fileNotExist.addWarn("loader.resFichier.notExist.error", runFile.getFile().toString());
      }
    }
    return fileNotExist;
  }
}
