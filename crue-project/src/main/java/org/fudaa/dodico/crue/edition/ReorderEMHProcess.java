/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.RelationEMHContient;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;

import java.util.*;

/**
 * Permet de trier des EMHs
 *
 * @author deniger
 */
public class ReorderEMHProcess {
    /**
     * @param sousModele le {@link EMHSousModele}
     * @param ordered    les EMHS triées dans l'ordre voulue
     * @param catEmh     la categorie a trie
     * @return true si modification effectuée.
     */
    public static boolean reorder(EMHSousModele sousModele, List<? extends EMH> ordered, EnumCatEMH catEmh) {
        List<Long> newOrderedUids = TransformerEMHHelper.toUId(ordered);
        Set<Long> setOfUids = new HashSet<>(newOrderedUids);
        assert setOfUids.size() == newOrderedUids.size();
        List<RelationEMHContient> toRemove = new ArrayList<>();
        Collection<RelationEMHContient> selectRelationOfType = EMHHelper.selectRelationOfType(sousModele, RelationEMHContient.class);

        for (RelationEMHContient relationEMHContient : selectRelationOfType) {
            if (setOfUids.contains(relationEMHContient.getEmh().getUiId())) {
                assert catEmh.equals(relationEMHContient.getEmh().getCatType());
                toRemove.add(relationEMHContient);
            }
        }
        assert toRemove.size() == ordered.size();
        boolean modified = false;
        int nb = toRemove.size();
        for (int i = 0; i < nb; i++) {
            Long currentUid = toRemove.get(i).getEmh().getUiId();
            Long newUid = newOrderedUids.get(i);
            if (!currentUid.equals(newUid)) {
                modified = true;
                break;
            }
        }
        if (!modified) {
            return false;
        }
        sousModele.removeAllRelations(toRemove);
        for (EMH node : ordered) {
            sousModele.addRelationEMH(new RelationEMHContient(node));
        }
        return true;
    }
}
