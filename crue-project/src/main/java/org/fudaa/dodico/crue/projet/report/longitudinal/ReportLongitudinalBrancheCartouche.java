/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.longitudinal;

import java.util.HashMap;
import java.util.Map;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;

/**
 * Contient les informations nécessaire pour afficher le cartouche
 *
 * @author Frederic Deniger
 */
public class ReportLongitudinalBrancheCartouche {

  private final String brancheName;//utilise au cas ou la branche n'existe pas
  /**
   * le xp min de la branche dans l'affichage
   */
  private final double xpMinDisplay;
  private final boolean negativeDecal;
  /**
   * le xp max de la branche dans l'affichage
   */
  private final double xpMaxDisplay;
  final Map<ReportRunKey, ReportLongitudinalPositionSectionByRun> sections = new HashMap<>();

  public ReportLongitudinalBrancheCartouche(String brancheName, double xpMin, double xpMax, boolean negativeDecal) {
    this.xpMinDisplay = xpMin;
    this.brancheName = brancheName;
    this.xpMaxDisplay = xpMax;
    this.negativeDecal = negativeDecal;
  }

  public boolean isNegativeDecal() {
    return negativeDecal;
  }

  public String getBrancheName() {
    return brancheName;
  }

  public double getXpMinDisplay() {
    return xpMinDisplay;
  }

  public CtuluRange getDisplayRange() {
    return new CtuluRange(xpMinDisplay, xpMaxDisplay);
  }

  public double getXpMaxDisplay() {
    return xpMaxDisplay;
  }

  public void add(ReportLongitudinalPositionSectionByRun byRun) {
    sections.put(byRun.getRunKey(), byRun);
  }

  public boolean containsSection() {
    return !sections.isEmpty();
  }

  public ReportLongitudinalPositionSectionByRun getSectionPositions(ReportRunKey key) {
    return sections.get(key);
  }
}
