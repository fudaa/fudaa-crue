/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.bean.AbstractListContent;
import org.fudaa.dodico.crue.edition.bean.ListCasierContent;
import org.fudaa.dodico.crue.edition.bean.ValidationMessage;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;

/**
 *
 * @author Frédéric Deniger
 */
public class EditionPrevalidateCreateCasier {

  public ValidationMessage<ListCasierContent> validate(final EMHSousModele sousModele, final Collection<ListCasierContent> contents, final CrueConfigMetier ccm) {
    final ValidationMessage<ListCasierContent> res = new ValidationMessage<>();
    final CtuluLog log = res.log;
    final List<EMH> casiers = sousModele.getParent().getParent().getIdRegistry().getEMHs(EnumCatEMH.CASIER);
    final List<CatEMHNoeud> noeuds = sousModele.getNoeuds();
    final Map<String, CatEMHNoeud> noeudsByNom = TransformerHelper.toMapOfNom(noeuds);
    final Set<String> ids = TransformerHelper.toSetOfId(casiers);
    for (final ListCasierContent content : contents) {
      final String nom = content.getNom();
      final List<CtuluLogRecord> msgs = new ArrayList<>();

      if (!ValidationPatternHelper.isNameValide(nom, EnumCatEMH.CASIER)) {
        msgs.add(log.addSevereError("AddEMH.NameNotValid", nom));
      }

      if (AbstractListContent.nameUsed(content, contents)) {
        msgs.add(log.addSevereError("AddEMH.NameAlreadyUsedInNewEMH", nom));
      }

      if (ids.contains(nom.toUpperCase())) {
        msgs.add(log.addSevereError("AddEMH.NameAlreadyUsedByExistingEMH", nom));
      }
      final String noeudNom = content.getNoeud();
      if (StringUtils.isBlank(noeudNom)) {
        msgs.add(log.addSevereError("AddEMH.CasierWithoutNoeud", nom));
      } else {
        final CatEMHNoeud noeud = noeudsByNom.get(noeudNom);
        if (noeud == null) {
          msgs.add(log.addSevereError("AddEMH.CasierNoeudUnknown", nom));
        }
      }
      if (noeudNom != null && nom != null) {
        final String casierNameForNoeud = CruePrefix.getNomAvecPrefixFor(noeudNom, EnumCatEMH.CASIER);
        if (!casierNameForNoeud.equals(nom)) {
          msgs.add(log.addSevereError("AddEMH.CasierNameNotCorrespondingToNoeud", nom, casierNameForNoeud));

        }
      }
      if (!msgs.isEmpty()) {
        res.accepted = false;
        final ValidationMessage.ResultItem<ListCasierContent> item = new ValidationMessage.ResultItem<>();
        item.bean = content;
        res.messages.add(item);
        for (final CtuluLogRecord ctuluLogRecord : msgs) {
          BusinessMessages.updateLocalizedMessage(ctuluLogRecord);
          item.messages.add(ctuluLogRecord.getLocalizedMessage());
        }
      }
    }

    return res;
  }
}
