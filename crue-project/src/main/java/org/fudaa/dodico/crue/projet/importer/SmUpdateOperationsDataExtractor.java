package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOperationsData;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOptions;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Permet de valider et d'extraire les operations qui seront effectuées pour mettre à jour un sous-modèle
 */
public class SmUpdateOperationsDataExtractor {
  private final Map<String, EMH> emhByNomInTarget;
  EMHScenario targetScenario;

  /**
   * @param target le scenario cible
   */
  public SmUpdateOperationsDataExtractor(EMHScenario target) {
    emhByNomInTarget = target.getIdRegistry().getEmhByNom();
    this.targetScenario = target;
  }

  /**
   * @param sourceSousModele la source
   * @param updateOptions les options ( toutes les sections, les casiers, branches)
   * @param targetSousModeleName le nom du sous-modele cible
   * @return les données de la mise à jour
   */
  SmUpdateOperationsData extract(EMHSousModele sourceSousModele, SmUpdateOptions updateOptions, String targetSousModeleName) {

    EMHSousModele targetSousModele = (EMHSousModele) emhByNomInTarget.get(targetSousModeleName);
    if (targetSousModele == null) {
      //rien à importer
      return new SmUpdateOperationsData(sourceSousModele, null);
    }

    //les donnees à importer
    final SmUpdateOptionsDataExtractor.SmUpdateOptionsData sourceDataToImport = new SmUpdateOptionsDataExtractor(sourceSousModele).extract(updateOptions);
    //Bilan des opérations
    final List<EMH> toUpdate = sourceDataToImport.getEmhToAddOrUpdate().stream().filter(emh -> emhByNomInTarget.containsKey(emh.getNom())).collect(Collectors.toList());
    final List<EMH> toAdd = sourceDataToImport.getEmhToAddOrUpdate().stream().filter(emh -> !emhByNomInTarget.containsKey(emh.getNom())).collect(Collectors.toList());
    SmUpdateSourceTargetBuilder builder = new SmUpdateSourceTargetBuilder(targetScenario);
    return new SmUpdateOperationsData(sourceSousModele, targetSousModele, toAdd, builder.build(toUpdate));
  }
}
