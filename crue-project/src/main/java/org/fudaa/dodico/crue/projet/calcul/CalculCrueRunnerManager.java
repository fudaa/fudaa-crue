/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.calcul;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.metier.CrueFileType;

import java.util.Map;

/**
 * L'interface pour le lancement des runs.
 *
 * @author deniger
 */
public interface CalculCrueRunnerManager {
    /**
     * @return le runner Crue9
     */
    CalculCrueRunner getCrue9Runner();

    /**
     * @return le runner Crue10
     */
    CalculCrueRunner getCrue10Runner(CoeurConfigContrat coeur);

    /**
     * Exemple: CrueFileType.RPTR -> -r CrueFileType.RPTG -> -g
     *
     * @return map contenant l'association CrueFileType de resultat -> options a utiliser.
     */
    Map<CrueFileType, String> getExecOptions();
}
