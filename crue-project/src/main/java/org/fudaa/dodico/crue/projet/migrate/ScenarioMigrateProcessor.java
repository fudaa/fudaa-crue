/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.migrate;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.common.ConnexionInformationDefault;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;

import java.io.File;

/**
 * Process pour migrer un projet. Seul la dernière version est supportée.
 */
public class ScenarioMigrateProcessor {
  private final EMHProjet initial;
  private ConnexionInformation connexionInformation;

  /**
   * @param emhProjet            le projet
   * @param connexionInformation les infos pour l'utilisateur/heure courante
   */
  public ScenarioMigrateProcessor(EMHProjet emhProjet, ConnexionInformation connexionInformation) {
    super();
    this.initial = emhProjet;
    this.connexionInformation = connexionInformation;
    if (this.connexionInformation == null) {
      this.connexionInformation = ConnexionInformationDefault.getDefault();
    }
  }

  /**
   * @param emhProjet le projet
   */
  public ScenarioMigrateProcessor(EMHProjet emhProjet) {
    this(emhProjet, null);
  }

  public CtuluLogGroup exportTo(File targetDir, String etuFileName, CoeurConfigContrat version) {
    CtuluLogGroup logs = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    logs.setDescription("exportTo.log");
    logs.setDescriptionArgs(etuFileName);
    CtuluLog log = logs.createNewLog("exportTo.validation.log");

    if (!Crue10VersionConfig.isLastVerion(version)) {
      log.addSevereError("migrate.canOnlyMigrateToLastVersion", Crue10VersionConfig.getLastVersion(), version.getXsdVersion());
      return logs;
    }
    // step 1: on valide
    MigrateTargetDirValidator validator = new MigrateTargetDirValidator(this.initial.getParentDirOfEtuFile());
    validator.valid(targetDir, log, new File(targetDir, etuFileName));
    if (log.containsSevereError()) {
      return logs;
    }

    final boolean targetDirExists = targetDir.exists();


    // step 2: construire le projet final
    FilteredProjectCreator creator = new FilteredProjectCreator(initial, connexionInformation);
    EMHProjet targetProject = creator.create(new File(targetDir, etuFileName), version, log);

    if (log.containsSevereError()) {
      return logs;
    }

    // step 3: on copie les scenarios Crue 9 et on transforme les scenarios Crue 10
    ScenarioCopyProcessor scenarioCopier = new ScenarioCopyProcessor(initial);
    scenarioCopier.copy(targetProject, logs);
    String filename = etuFileName;
//    Copie du fichier etu:
    Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getVersion(CrueFileType.ETU, version);
    if (!filename.endsWith(fileFormat.getExtension())) {
      filename = filename + "." + fileFormat.getExtension();
    }
    fileFormat.writeMetierDirect(targetProject,
        new File(targetDir, filename), logs.createLog(), initial.getPropDefinition());

    if (logs.containsFatalError()) {
      this.deleteChildren(targetDir);
      if (!targetDirExists) {
        CrueFileHelper.deleteFile(targetDir, getClass());
      }
    } else {
      //copie de config
      File dirOfConfig = initial.getInfos().getDirOfConfig();
      if (dirOfConfig != null && dirOfConfig.exists()) {
        CrueFileHelper.copyDirWithSameLastDate(dirOfConfig, targetProject.getInfos().getDirOfConfig());
      }
      //copie de rapport
      File dirOfRapport = initial.getInfos().getDirOfRapports();
      if (dirOfRapport != null && dirOfRapport.exists()) {
        CrueFileHelper.copyDirWithSameLastDate(dirOfRapport, targetProject.getInfos().getDirOfRapports());
      }
    }

    return logs;
  }

  private void deleteChildren(File directory) {
    if (directory != null) {
      for (File file : directory.listFiles()) {
        CtuluLibFile.deleteDir(file);
      }
    }
  }
}
