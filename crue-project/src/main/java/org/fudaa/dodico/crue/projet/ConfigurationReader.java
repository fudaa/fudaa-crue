/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.conf.CrueCONFReaderWriter;
import org.fudaa.dodico.crue.projet.conf.Configuration;
import org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager;

/**
 *
 * @author Frederic Deniger
 */
public class ConfigurationReader {

  private final File siteDir;

  public ConfigurationReader(File siteDir) {
    this.siteDir = siteDir;
  }

  public CrueIOResu<Configuration> readConfigSite() {
    return read(new File(siteDir, GlobalOptionsManager.SITE_FILE));
  }

  public CrueIOResu<Configuration> readConfigUser() {
    return read(new File(siteDir, GlobalOptionsManager.USER_FILE));
  }

  public CrueIOResu<Configuration> read(File confFile) {
    final CrueCONFReaderWriter reader = new CrueCONFReaderWriter("1.2");
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    CrueIOResu<Configuration> result = null;
    try {
      result = reader.readXML(confFile, log);
    } catch (Throwable exception) {
      Logger.getLogger(ConfigurationReader.class.getName()).log(Level.SEVERE, "erreur lors de l'analyse", exception);
    }
    return result;
  }
}
