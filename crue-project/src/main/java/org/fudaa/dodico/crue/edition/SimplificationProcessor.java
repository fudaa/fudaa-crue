/*
 GPL 2
 */
package org.fudaa.dodico.crue.edition;

import java.util.Collection;
import java.util.List;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilCasier;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHCasierProfil;
import org.fudaa.dodico.crue.metier.emh.EnumCasierType;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;

/**
 *
 * @author Frederic Deniger
 */
public class SimplificationProcessor {

  private final DonPrtGeoProfilSimplifier sectionUpdater;
  private final DonPrtGeoProfilSimplifier casierUpdater;

  public SimplificationProcessor(CrueConfigMetier ccm, SimplificationSeuils choose) {
    sectionUpdater = new DonPrtGeoProfilSimplifier(choose.sectionSeuil, ccm);
    casierUpdater = new DonPrtGeoProfilSimplifier(choose.casierSeuil, ccm);
  }

  public void simpify(Collection<EMH> emhs) {
    for (EMH emh : emhs) {
      if (EnumSectionType.EMHSectionProfil.equals(emh.getSubCatType())) {
        final DonPrtGeoProfilSection toSimplify = DonPrtHelper.getProfilSection(emh);
        toSimplify.initDataFrom(sectionUpdater.simplify(toSimplify));
      } else if (EnumCasierType.EMHCasierProfil.equals(emh.getSubCatType())) {
        List<DonPrtGeoProfilCasier> profilCasiers = DonPrtHelper.getProfilCasier((EMHCasierProfil) emh);
        for (DonPrtGeoProfilCasier profilCasier : profilCasiers) {
          profilCasier.initFrom(casierUpdater.simplify(profilCasier));
        }
      }
    }
  }
}
