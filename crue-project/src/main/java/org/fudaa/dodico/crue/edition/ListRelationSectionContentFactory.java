package org.fudaa.dodico.crue.edition;

import gnu.trove.TLongDoubleHashMap;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent.SectionData;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoSectionIdem;
import org.fudaa.dodico.crue.metier.emh.EMHSectionIdem;
import org.fudaa.dodico.crue.metier.emh.EMHSectionProfil;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

/**
 *
 * @author deniger
 */
public class ListRelationSectionContentFactory {
  
  public List<ListRelationSectionContent> createNodes(EMHSousModele sousModele, TLongDoubleHashMap distanceByBrancheUid, CrueConfigMetier ccm) {
    List<ListRelationSectionContent> res = new ArrayList<>();
    Set<CatEMHSection> done = new HashSet<>();
    List<CatEMHBranche> branches = sousModele.getBranches();
    
    NumberFormat dzFormatter = ccm.getFormatter(CrueConfigMetierConstants.PROP_DZ, DecimalFormatEpsilonEnum.COMPARISON);
    ItemVariable xpFormatter = ccm.getProperty(CrueConfigMetierConstants.PROP_XP);
    for (CatEMHBranche branche : branches) {
      List<RelationEMHSectionDansBranche> sections = branche.getSections();
      double distanceSchematique = distanceByBrancheUid == null ? 0 : distanceByBrancheUid.get(branche.getUiId());
      double distanceHydraul = branche.getLength();
      for (RelationEMHSectionDansBranche relationSection : sections) {
        CatEMHSection section = relationSection.getEmh();
        ListRelationSectionContent sectionContent = new ListRelationSectionContent();
        sectionContent.setAbscisseHydraulique(relationSection.getXp());
        sectionContent.setAdditionalDataRelation(relationSection.getAdditionalData());
        double xSchematique = 0;
        if (!xpFormatter.getEpsilon().isZero(distanceHydraul)) {//pas de div par zero !
          xSchematique = distanceSchematique * relationSection.getXp() / distanceHydraul;
        }
        sectionContent.setAbscisseSchematique(xpFormatter.format(xSchematique, DecimalFormatEpsilonEnum.COMPARISON));
        sectionContent.setBrancheNom(branche.getNom());
        fillContentWithSectionData(sectionContent, section, dzFormatter);
        res.add(sectionContent);
        done.add(section);
      }
    }
    List<CatEMHSection> sections = sousModele.getSections();
    for (CatEMHSection section : sections) {
      if (!done.contains(section)) {
        done.add(section);
        ListRelationSectionContent sectionContent = new ListRelationSectionContent();
        sectionContent.setBrancheNom(StringUtils.EMPTY);
        fillContentWithSectionData(sectionContent, section, dzFormatter);
        res.add(sectionContent);
      }
      
    }
    return res;
  }
  
  public static void fillContentWithSectionData(ListRelationSectionContent relationSection, CatEMHSection section, NumberFormat dzFormatter) {
    relationSection.setNom(section.getNom());
    SectionData sectionData = relationSection.getSection();
    sectionData.setSectionType(section.getSectionType());
    sectionData.setCommentaire(section.getCommentaire());
    if (section.getSectionType().equals(EnumSectionType.EMHSectionProfil)) {
      DonPrtGeoProfilSection profil = DonPrtHelper.getProfilSection((EMHSectionProfil) section);
      sectionData.setProfilSection(profil.getNom());
    } else if (section.getSectionType().equals(EnumSectionType.EMHSectionIdem)) {
      CatEMHSection sectionRef = EMHHelper.getSectionRef((EMHSectionIdem) section);
      if (sectionRef != null) {
        sectionData.setReferenceSectionIdem(sectionRef.getNom());
      }
      DonPrtGeoSectionIdem dptg = EMHHelper.getDPTGSectionIdem((EMHSectionIdem) section);
      sectionData.setDz(dptg.getDz());
    }
  }
}
