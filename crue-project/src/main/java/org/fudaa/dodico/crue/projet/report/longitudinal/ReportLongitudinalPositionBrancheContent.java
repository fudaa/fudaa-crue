/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.longitudinal;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLongitudinalPositionBrancheContent {

  private final List<ReportLongitudinalBrancheCartouche> cartouches = new ArrayList<>();

  public List<ReportLongitudinalBrancheCartouche> getCartouches() {
    return cartouches;
  }
  
}
