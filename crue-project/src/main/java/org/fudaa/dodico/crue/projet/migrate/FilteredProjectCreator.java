/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.migrate;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Créer des projets etu vierges ou en filtrant un projet existant. Utile pour réaliser une migration.
 *
 * @author CANEL Christophe
 */
public class FilteredProjectCreator {
  private final EMHProjet initialProject;
  private final ConnexionInformation userConnected;

  public FilteredProjectCreator(EMHProjet initialProject, ConnexionInformation userConnected) {
    super();

    this.initialProject = initialProject;
    this.userConnected = userConnected;

    assert (initialProject != null);
  }

  public static EMHProjet createEmptyProject(CoeurConfigContrat coeurConfigContrat, ConnexionInformation connexionInformation,
                                             File etuFile) {
    EMHProjet filteredProject = new EMHProjet();

    filteredProject.setPropDefinition(coeurConfigContrat);

    Map<String, String> directories = new HashMap<>();
    directories.put(EMHProjectInfos.FICHETUDES, ".");
    directories.put(EMHProjectInfos.RAPPORTS, "Rapports");
    directories.put(EMHProjectInfos.RUNS, "Runs");
    directories.put(EMHProjectInfos.CONFIG, "Config");

    EMHProjectInfos targetInfos = new EMHProjectInfos();
    targetInfos.setBaseFichiersProjets(new ArrayList<>());
    targetInfos.setXmlVersion(coeurConfigContrat.getXsdVersion());
    targetInfos.setInfosVersions(createNewInfoVersionWith(connexionInformation));
    targetInfos.setEtuFile(etuFile);//on initialise correctement le répertoire de l'étude.
    targetInfos.setDirectories(directories);
    filteredProject.setInfos(targetInfos);
    return filteredProject;
  }

  private static EMHInfosVersion createNewInfoVersionWith(ConnexionInformation userConnected) {
    EMHInfosVersion targetInfoVersion = new EMHInfosVersion();
    targetInfoVersion.setAuteurCreation(userConnected.getCurrentUser());
    targetInfoVersion.setDateCreation(userConnected.getCurrentDate());
    targetInfoVersion.setAuteurDerniereModif(targetInfoVersion.getAuteurCreation());
    targetInfoVersion.setDateDerniereModif(targetInfoVersion.getDateCreation());
    return targetInfoVersion;
  }

  /**
   * Controle en premier que la cible est bien la dernière version de la grammaire
   * Cree un projet à partir de base mais en enlevant tous les scenarios inactifs et en mettant à jour toutes les versions. L'idée
   * est de créer un projet cible filteredProject qui sera rempli à partir des listes targetScenarios,
   * targetModeles,targetSousModeles, listFileAvailable Ces listes seront créées en parcourant leur équivalent du projet initial,
   * en ignorant les scenarios, modele, ss-modele non actifs, les runs... La liste de fichiers listFileAvailable sera crée au fur
   * et mesure: si un scenario, modele, ss.modele est actif on ajoute le fichier-crue dans la map.
   *
   * @param etuFile       le fichier etu.
   * @param targetVersion la version cible
   * @param log           le log
   */
  public EMHProjet create(File etuFile, CoeurConfigContrat targetVersion, CtuluLog log) {


//dans la version 1.1.1, le fichier OPTR est absent. Il faut créer des fichiers
    boolean mustCreateOptr = Crue10VersionConfig.isV1_1_1(initialProject.getCoeurConfig())
        && !Crue10VersionConfig.isV1_1_1(targetVersion);
    boolean mustCreateDreg = Crue10VersionConfig.isLowerOrEqualsTo_V1_2(initialProject.getCoeurConfig())
        && Crue10VersionConfig.isDregSupported(targetVersion);

    // le projet a modifier
    Map<String, FichierCrue> filesById = new HashMap<>();
    List<FichierCrue> optrFiles = new ArrayList<>();
    List<FichierCrue> dregFiles = new ArrayList<>();
    List<ManagerEMHScenario> targetScenarios = new ArrayList<>();
    //maps afin de ne pas creer plusieur fois le meme modele ou sous-modele.
    Map<String, ManagerEMHModeleBase> targetModelesMap = new HashMap<>();
    Map<String, ManagerEMHSousModele> targetSousModelesMap = new HashMap<>();
    List<ManagerEMHModeleBase> targetModeles = new ArrayList<>();
    List<ManagerEMHSousModele> targetSousModeles = new ArrayList<>();
    // Pour tous les scenario, ne prend que les actifs.
    List<ManagerEMHScenario> listeScenarios = initialProject.getListeScenarios();
    for (ManagerEMHScenario intialEMHScenario : listeScenarios) {
      if (intialEMHScenario.isActive()) {
        ManagerEMHScenario scenario = new ManagerEMHScenario(intialEMHScenario.getNom());
        // mettre à jour la date de sauvegarde et autres... On verra plus tard...
        copyInfoVersion(intialEMHScenario, scenario);
        copyFiles(intialEMHScenario, scenario, filesById);

        // Pour tous les modèles du scénario.
        List<ManagerEMHModeleBase> initialModeles = intialEMHScenario.getFils();

        for (ManagerEMHModeleBase initialModele : initialModeles) {
          if (initialModele.isActive()) {
            ManagerEMHModeleBase targetModele = targetModelesMap.get(initialModele.getId());
            if (targetModele == null) {
              targetModele = new ManagerEMHModeleBase(initialModele.getNom());
              // Ajouter les modèles actifs au scenario créé et à la liste de targetModeles
              targetModelesMap.put(targetModele.getId(), targetModele);
              targetModeles.add(targetModele);
              copyInfoVersion(initialModele, targetModele);
              copyFiles(initialModele, targetModele, filesById);
              if (mustCreateOptr && !initialModele.isCrue9()) {
                String nom = targetModele.getNom();
                nom = StringUtils.removeStart(nom, CruePrefix.P_MODELE);

                final FichierCrue optrFichierCrue = new FichierCrue(nom + "." + CrueFileType.OPTR.getExtension(), FichierCrue.CURRENT_PATH,
                    CrueFileType.OPTR);
                targetModele.getListeFichiers().addFile(optrFichierCrue);
                optrFiles.add(optrFichierCrue);
              }
              //doit on créer un ficher dreg depuis un fichier vierge ? Oui si pas Crue9 et si pas de Dreg déjà présent
              if (mustCreateDreg && !initialModele.isCrue9() && initialModele.getFile(CrueFileType.DREG) == null) {
                String nom = targetModele.getNom();
                nom = StringUtils.removeStart(nom, CruePrefix.P_MODELE);
                final FichierCrue dregFichierCrue = new FichierCrue(nom + "." + CrueFileType.DREG.getExtension(), FichierCrue.CURRENT_PATH,
                    CrueFileType.DREG);
                targetModele.getListeFichiers().addFile(dregFichierCrue);
                dregFiles.add(dregFichierCrue);
              }

              for (ManagerEMHSousModele initialSousModele : initialModele.getFils()) {
                if (initialSousModele.isActive()) {
                  ManagerEMHSousModele targetSousModele = targetSousModelesMap.get(initialModele.getId());
                  if (targetSousModele == null) {
                    targetSousModele = new ManagerEMHSousModele(initialSousModele.getNom());
                    // Ajouter les sous-modèles actifs au modèle créé et à la liste
                    // de targetModeles
                    targetSousModeles.add(targetSousModele);
                    targetSousModelesMap.put(targetSousModele.getId(), targetSousModele);
                    copyInfoVersion(initialSousModele, targetSousModele);
                    copyFiles(initialSousModele, targetSousModele, filesById);
                  }
                  targetModele.addManagerFils(targetSousModele);
                }
              }
            }
            scenario.addManagerFils(targetModele);
          }
        }

        targetScenarios.add(scenario);
      }
    }

    List<File> files = new ArrayList<>();

    List<FichierCrue> targetFiles = new ArrayList<>();
    //we do it like that to keep the initial order of files
    for (FichierCrue fichierCrue : initialProject.getInfos().getBaseFichiersProjets()) {
      FichierCrue ficCrue = filesById.get(fichierCrue.getNom());
      if (ficCrue != null) {
        files.add(fichierCrue.getProjectFile(initialProject));
        targetFiles.add(ficCrue);
      }
    }
    targetFiles.addAll(optrFiles);
    targetFiles.addAll(dregFiles);
    File projectBaseDir = initialProject.getParentDirOfEtuFile();
    //we test that all initial files belong to the folder of the etu file.
    ChildrenFilesTester tester = new ChildrenFilesTester(projectBaseDir);
    tester.test(files, log);
    if (log.containsSevereError()) {
      return null;
    }

    EMHProjet filteredProject = new EMHProjet();

    filteredProject.setPropDefinition(targetVersion);
    EMHProjectInfos initialInfos = initialProject.getInfos();

    Map<String, String> directories = new HashMap<>(initialInfos.getDirectories());
    directories.put(EMHProjectInfos.FICHETUDES, ".");
    directories.put(EMHProjectInfos.RAPPORTS, "Rapports");
    directories.put(EMHProjectInfos.RUNS, "Runs");
    directories.put(EMHProjectInfos.CONFIG, "Config");

    EMHProjectInfos targetInfos = new EMHProjectInfos();
    targetInfos.setXmlVersion(targetVersion.getXsdVersion());
    targetInfos.setInfosVersions(createNewInfoVersionWith(initialInfos.getInfosVersions()));
    targetInfos.setBaseFichiersProjets(targetFiles);
    targetInfos.setEtuFile(etuFile);//on initialise correctement le répertoire de l'étude.
    targetInfos.setDirectories(directories);

    filteredProject.setInfos(targetInfos);

    for (ManagerEMHScenario scenario : targetScenarios) {
      filteredProject.addScenario(scenario);
    }

    for (ManagerEMHModeleBase modele : targetModeles) {
      filteredProject.addBaseModele(modele);
    }

    for (ManagerEMHSousModele sousModele : targetSousModeles) {
      filteredProject.addBaseSousModele(sousModele);
    }
    if (initialProject.getScenarioCourant() != null) {
      ManagerEMHScenario scenario = filteredProject.getScenario(initialProject.getScenarioCourant().getNom());
      if (scenario != null) {
        filteredProject.setScenarioCourant(scenario);
      }
    }
    if (!dregFiles.isEmpty()) {
      final URL defaultFileDREG = targetVersion.getDefaultFile(CrueFileType.DREG);
      for (FichierCrue dregFileToCreate : dregFiles) {
        File dregFileProjectFile = dregFileToCreate.getProjectFile(filteredProject);
        boolean copyDone = CrueFileHelper.copyFile(defaultFileDREG, dregFileProjectFile);
        if (!copyDone) {
          log.addError("migrate.cantCreateDregFile", dregFileProjectFile.getName());
        }
      }
    }

    return filteredProject;
  }

  private void copyFiles(ManagerEMHContainerBase initial, ManagerEMHContainerBase target, Map<String, FichierCrue> listing) {
    if (initial.getListeFichiers() == null) {
      return;
    }
    boolean canDregBeCopied = Crue10VersionConfig.isLowerOrEqualsTo_V1_2(initialProject.getCoeurConfig());
    List<FichierCrue> initFiles = initial.getListeFichiers().getFichiers();
    List<FichierCrue> targetFiles = new ArrayList<>(initFiles.size());

    // si le scenario est actif ajouter tous ces fichiers au target et a la map listFileAvailable
    for (FichierCrue fichier : initFiles) {
      if (!canDregBeCopied && fichier.getType().equals(CrueFileType.DREG)) {
        //on ne copie pas le fichier dreg si la version est <=1.2
        continue;
      }
      FichierCrue targetFichier = listing.get(fichier.getNom());
      if (targetFichier == null) {
        targetFichier = fichier.clone();
        //warn if the
        String path = targetFichier.getPath();
        if (path != null) {
          File filePath = new File(path);
          if (filePath.isAbsolute()) {
            String relativeFile = CtuluLibFile.getRelativeFile(filePath, this.initialProject.getDirOfFichiersEtudes(), 7);
            if (new File(relativeFile).isAbsolute()) {
              targetFichier.setPath(FichierCrue.CURRENT_PATH);
            } else {
              targetFichier.setPath(relativeFile);
            }
          }
        }
        listing.put(targetFichier.getNom(), targetFichier);
      }
      targetFiles.add(targetFichier);
    }

    target.setListeFichiers(targetFiles);
  }

  private void copyInfoVersion(ManagerEMHContainerBase initial, ManagerEMHContainerBase target) {
    EMHInfosVersion infosVersions = initial.getInfosVersions();
    EMHInfosVersion targetInfoVersion = createNewInfoVersionWith(infosVersions);
    target.setInfosVersions(targetInfoVersion);
  }

  private EMHInfosVersion createNewInfoVersionWith(EMHInfosVersion infosVersions) {
    EMHInfosVersion targetInfoVersion = (EMHInfosVersion) infosVersions.clone();
    targetInfoVersion.setAuteurDerniereModif(userConnected.getCurrentUser());
    targetInfoVersion.setDateDerniereModif(userConnected.getCurrentDate());
    return targetInfoVersion;
  }
}
