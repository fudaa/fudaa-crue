/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent.SectionData;
import org.fudaa.dodico.crue.edition.bean.ValidationMessage;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.validation.util.DelegateValidatorBranche;

/**
 *
 * @author Frédéric Deniger
 */
public class EditionChangeRelationSection {

  /**
   * Modification a appliquer à toutes relation sections-branches du sous-modele. Attention, l'ordre est important les ListRelationSectionContent
   * doivent être dans l'ordre amont/aval pour les branches de longueur nulle.
   *
   * @param contents le contenu a editer
   * @param emhSousModele le sous-modele
   * @param ccm le {@link CrueConfigMetier}
   * @return validation de l'operation
   */
  public ValidationMessage<ListRelationSectionContent> apply(List<ListRelationSectionContent> contents, EMHSousModele emhSousModele,
          CrueConfigMetier ccm) {
    EditionPrevalidateModifyRelationSection prevalide = new EditionPrevalidateModifyRelationSection();
    ValidationMessage<ListRelationSectionContent> validateMessages = prevalide.validate(contents, emhSousModele, ccm);
    if (!validateMessages.accepted) {
      return validateMessages;
    }
    //on peut effectuer les modifications:
    //on met a jour les oldSections
    Map<String, CatEMHSection> sectionsByNom = TransformerHelper.toMapOfNom(emhSousModele.getSections());
    updateSections(contents, sectionsByNom);
    //on met a jour les relations
    Map<String, CatEMHBranche> branchesByNom = TransformerHelper.toMapOfNom(emhSousModele.getBranches());
    Map<String, List<ListRelationSectionContent>> newContentByBrancheName = EditionPrevalidateModifyRelationSection.getNewContentByBrancheName(
            contents);
    List<EnumBrancheType> brancheTypeWithDistanceNotNull = DelegateValidatorBranche.getBrancheTypeWithDistanceNotNull();
    for (Map.Entry<String, List<ListRelationSectionContent>> entry : newContentByBrancheName.entrySet()) {
      String brancheNom = entry.getKey();
      if (StringUtils.isBlank(brancheNom)) {
        continue;
      }
      CatEMHBranche branche = branchesByNom.get(brancheNom);
      List<ListRelationSectionContent> newContentsForBranche = entry.getValue();
      List<RelationEMHSectionDansBranche> oldSections = branche.getSections();
      //on enlève toutes les anciennes relations et on persiste les anciennes données.
      for (RelationEMHSectionDansBranche relation : oldSections) {
        CatEMHSection section = relation.getEmh();
        List<RelationEMHBrancheContientSection> toRemove = EMHHelper.selectClass(section.getRelationEMH(), RelationEMHBrancheContientSection.class);
        if (!toRemove.isEmpty()) {
          assert toRemove.size() == 1;
          section.removeAllRelations(toRemove);
        }
      }
      branche.removeAllRelations(oldSections);
      
      final boolean brancheWithDistanceNotNull = brancheTypeWithDistanceNotNull.contains(branche.getBrancheType());
      for (ListRelationSectionContent newContent : newContentsForBranche) {
        CatEMHSection newSection = sectionsByNom.get(newContent.getNom());
        //la section appartenait a une autre branche: on enlève les relations.
        if (newSection.getBranche() != null) {
          EMHRelationFactory.removeRelationBrancheDansSectionBidirect(newSection);
        }
        RelationEMHSectionDansBranche relationToUse = EMHRelationFactory.createSectionDansBrancheAndSetRelation(branche, newSection, 0, ccm);
        branche.addRelationEMH(relationToUse);
        //dans ce cas, on doit initialiser les xp
        relationToUse.initAdditionnalData(newContent.getAdditionalDataRelation());
        if (brancheWithDistanceNotNull) {
          relationToUse.setXp(newContent.getAbscisseHydraulique());
        } else {
          relationToUse.setXp(0);
        }
      }
      if (brancheWithDistanceNotNull) {
        branche.sortRelationSectionDansBranche(ccm);
      }
      List<RelationEMHSectionDansBranche> currentSections = branche.getSections();
      currentSections.get(0).setPos(EnumPosSection.AMONT);
      currentSections.get(currentSections.size() - 1).setPos(EnumPosSection.AVAL);
      for (int i = 1; i < currentSections.size() - 1; i++) {
        currentSections.get(i).setPos(EnumPosSection.INTERNE);
      }
      
    }
    
    
    return validateMessages;
    
  }
  
  private void updateSections(List<ListRelationSectionContent> contents, Map<String, CatEMHSection> sectionsByNom) {
    //on commence par modifier les oldSections commentaire + idem
    for (ListRelationSectionContent content : contents) {
      SectionData sectionData = content.getSection();
      CatEMHSection section = sectionsByNom.get(content.getNom());
      section.setCommentaire(sectionData.getCommentaire());
      if (EnumSectionType.EMHSectionIdem.equals(sectionData.getSectionType())) {
        EMHSectionIdem idem = (EMHSectionIdem) section;
        CatEMHSection newRef = sectionsByNom.get(sectionData.getReferenceSectionIdem());
        CatEMHSection oldRef = EMHHelper.getSectionRef(idem);
        EMHRelationFactory.removeRelationBidirect(idem, oldRef);
        EMHRelationFactory.addSectionRef(idem, newRef);
        DonPrtGeoSectionIdem dptg = EMHHelper.selectFirstOfClass(idem.getInfosEMH(), DonPrtGeoSectionIdem.class);
        dptg.setDz(sectionData.getDz());
      }
    }
  }
}
