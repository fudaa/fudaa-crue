/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.calcul;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.calcul.CalculExec;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.projet.ScenarioLoaderCrue10Result;
import org.fudaa.dodico.objet.CExecListener;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class Crue10Exec extends CalculExec implements CalculCrueContrat, CExecListener {
  private final CoeurConfigContrat coeurConfig;
  private final ExecInput input;
  boolean stopFileWritten;
  List<String> options;
  private boolean execFinishWithoutError;
  private Process process;

  public Crue10Exec(final CoeurConfigContrat coeurConfig, final ExecInput input) {
    this.coeurConfig = coeurConfig;
    this.input = input;
  }

  @Override
  public boolean isComputeFinishedCorrectly() {
    return execFinishWithoutError;
  }

  /**
   * @return false car non appplication Crue9
   */
  @Override
  public boolean hasNoOptions() {
    return CollectionUtils.isEmpty(options);
  }

  @Override
  public boolean isStopFileWritten() {
    return stopFileWritten;
  }

  @Override
  public synchronized void stop() {
    if (stopFileWritten) {
      if (process != null) {
        process.destroy();
      }
    } else {
      stopFileWritten = true;
      final File run = getRunDir();
      final String nom = this.input.getScenario().getNom();
      final File stop = new File(run, nom + ".stop");
      Logger.getLogger(Crue10Exec.class.getName()).log(Level.WARNING, "fichier stop ecrit {0}", stop.getAbsolutePath());

      try (FileWriter writer = new FileWriter(stop)) {
        writer.write("1");
        writer.flush();
      } catch (final IOException iOException) {
        Logger.getLogger(Crue10Exec.class.getName()).log(Level.SEVERE, "impossible d'écrire dans le fichier: {0}", iOException);
      }
    }
  }

  @Override
  public synchronized void setProcess(final Process process) {
    this.process = process;
  }

  @Override
  public void run() {
    stopFileWritten = false;
    process = null;
    execFinishWithoutError = true;
    final File exec = new File(coeurConfig.getExecFile());
    if (!exec.exists()) {
      Logger.getLogger(Crue10Exec.class.getName()).log(Level.SEVERE, "L''executable crue 10 {0} n''existe pas",
          exec.getAbsolutePath());
    } else {
      final File dir = getRunDir();
      final File stdout = new File(dir, ScenarioLoaderCrue10Result.STDOUTCSV_NAME);
      createFile(stdout);

      try (PrintStream stream = new PrintStream(stdout)) {
        setOutError(stream);
        setOutStandard(stream);
        execFinishWithoutError = super.launch(dir, this);
        if (!execFinishWithoutError) {
          final File stderr = new File(dir, ScenarioLoaderCrue10Result.STDERRCSV_NAME);
          createFile(stderr);
        }
      } catch (final FileNotFoundException fileNotFoundException) {
        Logger.getLogger(Crue10Exec.class.getName()).log(Level.WARNING, "cant open PrintStream", fileNotFoundException);
      }
    }
  }

  private void createFile(final File stdout) {
    try {
      boolean created = stdout.createNewFile();
      if (!created) {
        Logger.getLogger(Crue10Exec.class.getName()).log(Level.WARNING, "cant create file");
      }
    } catch (final IOException iOException) {
      Logger.getLogger(Crue10Exec.class.getName()).log(Level.WARNING, "cant create file", iOException);
    }
  }

  public List<String> getOptions() {
    return options;
  }

  public void setOptions(final List<String> options) {
    this.options = options;
  }

  @Override
  public String getExecTitle() {
    return "Crue 10";
  }

  @Override
  public String[] getLaunchCmd(final File paramsFile, final CtuluUI ctuluUI) {
    final List<String> cmd = new ArrayList<>();
    cmd.add(coeurConfig.getExecFile());
    if (CollectionUtils.isNotEmpty(options)) {
      cmd.addAll(options);
    }
    //le fichier etu se trouve à la racine du run:
    final File etuFile = new File(getRunDir(), input.getProjet().getInfos().getEtuFile().getName());
    cmd.add(etuFile.getAbsolutePath());
    return cmd.toArray(new String[0]);
  }

  private File getRunDir() {
    return input.getProjet().getDirForRun(input.getScenario(), input.getRun());
  }
}
