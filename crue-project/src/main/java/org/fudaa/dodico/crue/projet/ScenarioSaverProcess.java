/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.FileIntegrityManager;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

/**
 * Se charge de sauvegarder un scenario. Cette premiere version utilisee dans le lot 0 uniqument pour la generation de crue 9 en crue 10. Utilise un
 * fichier modele et genere tous les fichier avec les bons suffixes.
 *
 * @author Adrien Hadoux
 */
public class ScenarioSaverProcess {

  private final ManagerEMHScenario scenario;
  private final EMHScenario scenarioLoaded;
  /**
   * Indique si on sauvegarde dans des nouveaux fichiers en utilisant fichierModele comme nom commun de fichier.
   */
  private final EMHProjet projet;

  /**
   * Sauvegarder dans les fichiers deja existants:Fonction sauvegarder
   *
   * @param scenarioToPersist {@link EMHScenario} a persister
   * @param emhProjet le projet parent
   */
  public ScenarioSaverProcess(final EMHScenario scenarioLoaded, final ManagerEMHScenario scenarioToPersist, final EMHProjet emhProjet) {
    super();

    this.scenarioLoaded = scenarioLoaded;
    this.scenario = scenarioToPersist;
    this.projet = emhProjet;
  }
  /**
   * un autre répertoire de sauvegarde qui remplace le dossier des fichiers etu du projet
   */
  private File otherDir;

  /**
   *
   * @param otherDir un autre répertoire de sauvegarde qui remplace le dossier des fichiers etu du projet
   */
  public void setOtherDir(File otherDir) {
    this.otherDir = otherDir;
  }

  /**
   * S'occupe d'enregistrer en appelant le bon generateur crue 9 ou 10 selon le type du projet.
   *
   * @return true si réussite
   */
  public CtuluLogGroup saveWithScenarioType() {
    final CtuluLogGroup errorMng = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    errorMng.setDescription("write.scenario.action");
    errorMng.setDescriptionArgs(scenario.getNom());
    CtuluLog analyzer = null;

    if (projet.getScenario(scenario.getNom()) == null) {
      analyzer = errorMng.createNewLog("write.scenario.action.prevalidation");
      analyzer.addSevereError("not.scenario.error", scenario.getNom());
    }
    final Map<String, File> files = otherDir == null ? projet.getInputFiles(scenario) : projet.getInputFiles(scenario, otherDir);
    scenarioLoaded.sort();
    if (scenario.getInfosVersions() != null && scenario.getInfosVersions().getType() != null) {

      final String type = scenario.getInfosVersions().getType().toUpperCase();
      final List<File> writedFiles = new ArrayList<>();
      if (files != null) {
        writedFiles.addAll(files.values());
      }
      final FileIntegrityManager backupManager = new FileIntegrityManager(writedFiles);
      backupManager.start();
      try {
        if (type.contains("9")) {
          boolean res = new ScenarioSaverCrue9(scenarioLoaded, scenario, errorMng, projet.getPropDefinition()).save(files);
          if (!res) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "ScenarioSaverCrue9 save return false");
          }

        } else {
          boolean res = new ScenarioSaverCrue10(projet,scenarioLoaded, projet.getCoeurConfig(), scenario, errorMng).save(files, true);
          if (!res) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "ScenarioSaverCrue10 save return false");
          }
        }
      } catch (Exception e) {
        if (analyzer == null) {
          analyzer = errorMng.createNewLog("write.scenario.action.prevalidation");
        }
        analyzer.addSevereError("io.xml.erreur");
        Logger.getLogger(getClass().getName()).log(Level.SEVERE, "saveWithScenarioType", e);
      } finally {
        finishBackup(errorMng, backupManager);
      }
      return errorMng;
    }
    return errorMng;
  }

  private void finishBackup(final CtuluLogGroup errorMng, final FileIntegrityManager backupManager) {
    backupManager.finish(!errorMng.containsFatalError());
    if (backupManager.getBackupLog().isNotEmpty()) {
      errorMng.addLog(backupManager.getBackupLog());
    }
  }
}
