/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report;

import org.fudaa.dodico.crue.projet.report.formule.FormuleContent;

import java.util.List;

/**
 * Contrat pour les services contenant des formules
 *
 * @author Frederic Deniger
 */
public interface ReportFormuleServiceContrat {
    /**
     * @return la liste des noms de variables définies
     */
    List<String> getVariablesKeys();

    /**
     * @return le contenu des exrpressions.
     */
    List<FormuleContent> getContents();
}
