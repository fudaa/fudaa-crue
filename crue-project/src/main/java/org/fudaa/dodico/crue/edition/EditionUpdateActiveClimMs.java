/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import gnu.trove.TLongObjectHashMap;
import gnu.trove.TLongObjectIterator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.bean.DonCLimMLineContent;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.DclmFactory;
import org.fudaa.dodico.crue.metier.factory.OrdCalcFactory;
import org.fudaa.dodico.crue.validation.ValidationHelper;

/**
 * Met à jour les ClimM active d'un calcul.
 *
 * @author Frederic Deniger
 */
public class EditionUpdateActiveClimMs {

  private final CrueConfigMetier ccm;

  public EditionUpdateActiveClimMs(final CrueConfigMetier ccm) {
    this.ccm = ccm;
  }

  public CtuluLogGroup updateCLimMs(final List<DonCLimMLineContent> newCalcsAndClimMs, final EMHScenario parent) {
    final Map<Class, DclmFactory.CalcBuilder> builderByClass = new HashMap<>();
    for (final DclmFactory.CalcBuilder calcBuilder : DclmFactory.getPseudoPerm()) {
      builderByClass.put(calcBuilder.getDclmClass(), calcBuilder);
    }
    for (final DclmFactory.CalcBuilder calcBuilder : DclmFactory.getTrans()) {
      builderByClass.put(calcBuilder.getDclmClass(), calcBuilder);
    }
    final List<OrdCalc> currentOrdCalcs = parent.getOrdCalcScenario().getOrdCalc();
    final Map<String, OrdCalc> currentOrdCalcsByCalcName = new HashMap<>();
    for (final OrdCalc currentOrdCalc : currentOrdCalcs) {
      currentOrdCalcsByCalcName.put(currentOrdCalc.getCalc().getNom(), currentOrdCalc);
    }
    final CtuluLogGroup group = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    final List<Calc> newCalcs = new ArrayList<>();
    final List<OrdCalc> newOrdCalcs = new ArrayList<>();
    final Set<String> newCalculNames = new HashSet<>();
    final CtuluLog log = group.createLog();
    for (final DonCLimMLineContent donCLimMLineContent : newCalcsAndClimMs) {
      final TLongObjectHashMap<List<DonCLimM>> dclmsByEMHId = donCLimMLineContent.getDclmsByEMHId();
      final Calc calcul = donCLimMLineContent.getCalc().cloneWithNameComment();
      newCalculNames.add(calcul.getNom());
      newCalcs.add(calcul);
      final OrdCalc currentOcal = currentOrdCalcsByCalcName.get(calcul.getNom());
      if (currentOcal == null) {
        final OrdCalc newOcal = OrdCalcFactory.createDefaultOcal(calcul, ccm);
        newOrdCalcs.add(newOcal);
      } else {
        final OrdCalc newOcal = currentOcal.deepCloneButNotCalc(calcul);
        newOrdCalcs.add(newOcal);
      }
      for (final TLongObjectIterator<List<DonCLimM>> it = dclmsByEMHId.iterator(); it.hasNext(); ) {
        it.advance();
        if (it.value() == null) {
          continue;
        }
        for (final DonCLimM donCLimM : it.value()) {
          final DonCLimMCommonItem dclm = (DonCLimMCommonItem) donCLimM;
          if (dclm == null) {
            continue;
          }
          final EMH emh = parent.getIdRegistry().getEmh(it.key());
          final DclmFactory.CalcBuilder builder = builderByClass.get(dclm.getClass());
          if (!builder.isAccepted(emh)) {
            log.addSevereError("dclim.dclmNotAccepted", dclm.getCalculParent().getNom(), emh.getNom(), builder.getNom());
          }
          if (dclm instanceof CalcPseudoPermItem) {
            final CalcPseudoPermItem perm = (CalcPseudoPermItem) dclm;
            if (perm.containValue()) {
              ccm.getValidator(perm.getCcmVariableName()).validateNumber(perm.getCalculParent().getNom() + " / " + emh.getNom(), perm.getValue(), log);
            }
          }
          if (dclm instanceof DonCLimMWithLoi) {
            if (((DonCLimMWithLoi) dclm).getLoi() == null) {
              log.addSevereError("dclim.loiNotDefined", dclm.getCalculParent().getNom(), emh.getNom());
            }
          }
          dclm.setCalculParent(calcul);
          dclm.setUserActive(true);
          calcul.addDclm(dclm);
        }
      }
    }
    final List<Calc> currentCalculs = parent.getDonCLimMScenario().getCalc();
    for (final Calc currentCalcul : currentCalculs) {
      if (!newCalculNames.contains(currentCalcul.getNom())) {
        newCalcs.add(currentCalcul.deepClone());
      }

    }
    final OrdCalcScenario ocalTemp = new OrdCalcScenario();
    final DonCLimMScenario dclmTemp = new DonCLimMScenario();
    for (final OrdCalc ordCalc : newOrdCalcs) {
      ocalTemp.addOrdCalc(ordCalc);
    }
    for (final Calc calc : newCalcs) {
      dclmTemp.addCalc(calc);
    }

    group.addLog(ValidationHelper.validateDonCLimMScenarioAndClim(dclmTemp, ccm));
    group.addLog(ValidationHelper.validateOrdCalcScenario(ocalTemp, ccm));
    if (group.containsFatalError()) {
      return group;
    }

    final EditionUpdateCalc updater = new EditionUpdateCalc();
    updater.updateCalcs(newCalcs, newOrdCalcs, parent);
    return group;
  }

  protected DonCLimM getActiveClim(final EMH emh, final String calculNom) {
    final List<DonCLimM> dclM = emh.getDCLM();
    for (final DonCLimM donCLimM : dclM) {
      if (donCLimM.getActuallyActive() && calculNom.equals(donCLimM.getCalculParent().getNom())) {
        return donCLimM;
      }
    }
    return null;

  }
}
