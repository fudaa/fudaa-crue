/*
 GPL 2
 */
package org.fudaa.dodico.crue.edition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierLitNomme;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilEtiquette;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.LitNumerote;
import org.fudaa.dodico.crue.metier.emh.PtProfil;

/**
 *
 * @author Frederic Deniger
 */
public class AlgoDonPrtGeoProfilSectionInverser {

  private final CrueConfigMetier ccm;

  public static class XtInverser {

    private final double xPivot;

    public XtInverser(double xPivot) {
      this.xPivot = xPivot;
    }

    public double getNewXt(double old) {
      return xPivot - old;
    }
  }

  public AlgoDonPrtGeoProfilSectionInverser(CrueConfigMetier ccm) {
    this.ccm = ccm;
  }

  private double getXPivot(DonPrtGeoProfilSection in) {
    List<LitNumerote> litNumerote = in.getLitNumerote();
    for (int i = litNumerote.size() - 1; i >= 0; i--) {
      if (litNumerote.get(i).getNomLit() != null) {
        return litNumerote.get(i).getLimFin().getAbscisse();
      }
    }
    return 0;
  }

  public DonPrtGeoProfilSection inverse(DonPrtGeoProfilSection in) {
    if (in == null) {
      return null;
    }
    double xPivot = getXPivot(in);
    XtInverser inverser = new XtInverser(xPivot);
    List<PtProfil> targetProfil = createTargetPtProfils(in, inverser);
    DonPrtGeoProfilSection res = new DonPrtGeoProfilSection();
    res.setEtiquettes(createTargetEtiquettes(in, inverser));
    res.setPtProfil(targetProfil);
    res.setLitNumerote(createTargetLitNommes(in, inverser));
    if (in.getFente() != null) {
      res.setFente(in.getFente().clone());
    }
    res.setNom(in.getNom());
    res.setCommentaire(in.getCommentaire());
    return res;
  }

  protected List<LitNumerote> createTargetLitNommes(DonPrtGeoProfilSection in,
          XtInverser inverser) {
    Map<LitNomme, LitNomme> newLitNommeByOld = new HashMap<>();
    CrueConfigMetierLitNomme litNomme = ccm.getLitNomme();
    newLitNommeByOld.put(litNomme.getMajeurEnd(), litNomme.getMajeurStart());
    newLitNommeByOld.put(litNomme.getMajeurStart(), litNomme.getMajeurEnd());
    newLitNommeByOld.put(litNomme.getStockageStart(), litNomme.getStockageEnd());
    newLitNommeByOld.put(litNomme.getStockageEnd(), litNomme.getStockageStart());
    newLitNommeByOld.put(litNomme.getMineur(), litNomme.getMineur());
    List<LitNumerote> initLits = in.getLitNumerote();
    List<LitNumerote> targetLits = new ArrayList<>(initLits.size());
    for (int i = initLits.size() - 1; i >= 0; i--) {
      LitNumerote initLit = initLits.get(i);
      LitNumerote newLit = initLit.clone();
      newLit.setNomLit(newLitNommeByOld.get(initLit.getNomLit()));
      PtProfil limDeb = newLit.getLimDeb();
      PtProfil limFin = newLit.getLimFin();
      limDeb.setXt(inverser.getNewXt(limDeb.getXt()));
      limFin.setXt(inverser.getNewXt(limFin.getXt()));
      //il faut inverser
      newLit.setLimDeb(limFin);
      newLit.setLimFin(limDeb);
      targetLits.add(newLit);
    }
    return targetLits;
  }

  protected List<DonPrtGeoProfilEtiquette> createTargetEtiquettes(DonPrtGeoProfilSection in,
          XtInverser inverser) {
    List<DonPrtGeoProfilEtiquette> etiquettes = in.getEtiquettes();
    List<DonPrtGeoProfilEtiquette> newEtiquettes = new ArrayList<>(etiquettes.size());
    for (DonPrtGeoProfilEtiquette etiquette : etiquettes) {
      DonPrtGeoProfilEtiquette newEtiquette = etiquette.clone();
      newEtiquette.getPoint().setXt(inverser.getNewXt(etiquette.getPoint().getAbscisse()));
      newEtiquettes.add(newEtiquette);
    }
    return newEtiquettes;
  }

  protected List<PtProfil> createTargetPtProfils(DonPrtGeoProfilSection in,
          XtInverser inverser) {
    List<PtProfil> initPtProfil = in.getPtProfil();
    List<PtProfil> targetProfil = new ArrayList<>();
    final int nbPt = initPtProfil.size();
    for (int i = nbPt - 1; i >= 0; i--) {
      final PtProfil oldPt = initPtProfil.get(i);
      final double oldX = oldPt.getAbscisse();
      final PtProfil newPt = new PtProfil(inverser.getNewXt(oldX), oldPt.getOrdonnee());
      targetProfil.add(newPt);
    }
    return targetProfil;
  }
}
