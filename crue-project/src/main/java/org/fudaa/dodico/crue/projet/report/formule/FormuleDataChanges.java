/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * L'ensemble des modifications apportées par la dernière edition.
 *
 * @author Frederic Deniger
 */
public class FormuleDataChanges {

  final Set<String> removedVariables = new HashSet<>();
  final Set<String> modifiedVariables = new HashSet<>();
  final Map<String, String> renamedVariables = new HashMap<>();

  public boolean isSomethingModified() {
    return !removedVariables.isEmpty() || !modifiedVariables.isEmpty() || !renamedVariables.isEmpty();
  }

  public boolean isRemoved(String oldVar) {
    return removedVariables.contains(oldVar);
  }

  public boolean isRenamed(String oldVar) {
    return renamedVariables.containsKey(oldVar);
  }

  public String getNewName(String oldVar) {
    return renamedVariables.get(oldVar);
  }

  public boolean isModified(String oldVar) {
    return modifiedVariables.contains(oldVar);
  }
}
