/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluParser;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.formule.function.EnumFormuleStat;
import org.fudaa.dodico.crue.projet.report.formule.function.FormuleMathCommand;
import org.nfunk.jep.Variable;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Frederic Deniger
 */
public class FormuleCalculatorExpr implements FormuleCalculator {

    private final CtuluParser parser;
    private final String displayName;
    private final Variable[] findUsedVar;
    private final ReportResultProviderServiceContrat service;

    FormuleCalculatorExpr(final ReportResultProviderServiceContrat service, final CtuluParser parser, final String displayName) {
        this.parser = parser;
        this.service = service;
        this.displayName = displayName;
        findUsedVar = this.parser.findUsedVar();
        parser.clearUnusedVar();
    }

    private boolean cyclique;

    @Override
    public void setCyclique(final boolean b) {
        this.cyclique = b;
    }

    @Override
    public boolean containsTimeDependantVariable() {
        return ArrayUtils.isNotEmpty(findUsedVar);
    }


    @Override
    public boolean isExpressionValid(final Collection<ResultatTimeKey> selectedTimes) {
        if (CollectionUtils.isEmpty(selectedTimes)) {
            if (parser.getLastExpr().contains(EnumFormuleStat.FORMULE_WITH_SELECTION_TIME)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Set<String> getDirectUsedVariables() {
        final Set<String> directUsedVariable = new HashSet<>();
        for (final Variable res : findUsedVar) {
            directUsedVariable.add(res.getName());
        }
        final String lastExpr = parser.getLastExpr();
        findFunctionReferencedVariable(lastExpr, directUsedVariable);
        return directUsedVariable;
    }


    @Override
    //pour l'instant non thread safe...
    public synchronized Double getValue(final ResultatTimeKey selectedTime, final ReportRunVariableKey key, final String emhNom,
                                        final Collection<ResultatTimeKey> selectedTimesInRapport) {
        if (cyclique) {
            displayError(BusinessMessages.getString("notComputedBecauseCycle", displayName));
            return null;
        }
        if (parser.hasError()) {
            return null;
        }
        final Collection values = this.parser.getFunctionTable().values();
        for (final Object func : values) {
            if (func instanceof FormuleMathCommand) {
                final boolean ok = ((FormuleMathCommand) func).setConfig(selectedTime, key.getRunKey(), emhNom, selectedTimesInRapport);
                if (!ok) {
                    return null;
                }
            }
        }

        for (final Variable variable : findUsedVar) {
            final ReportRunVariableKey usedVar = new ReportRunVariableKey(key.getReportRunKey(), service.createVariableKey(variable.getName()));
            final Double val = service.getValue(selectedTime, usedVar, emhNom, selectedTimesInRapport);
            if (val == null) {
                return null;
            } else {
                variable.setValue(val);
            }
        }
        Double val = null;
        try {
            val = parser.getValue();
            if (parser.hasError()) {
                final String errorInfo = parser.getErrorInfo();
                val = null;
                displayError(errorInfo);
            }
        } catch (final Exception e) {
        }
        return val;
    }

    long lastTime = 0;

    private void displayError(final String errorInfo) {
        final long to = System.currentTimeMillis();
        if (to - lastTime > 10000) {
            service.getUI().error(displayName, StringUtils.substringAfter(errorInfo, ":").trim(), true);
            lastTime = to;
        }

    }

    public static Set<String> findFunctionReferencedVariable(final String lastExpr) {
        return findFunctionReferencedVariable(lastExpr, new HashSet<>());

    }

    private static Set<String> findFunctionReferencedVariable(final String lastExpr, final Set<String> directUsedVariable) {
        if (lastExpr.indexOf('"') >= 0) {
            final String[] splitPreserveAllTokens = StringUtils.splitPreserveAllTokens(lastExpr, '"');
            for (int i = 1; i < splitPreserveAllTokens.length; i += 2) {
                directUsedVariable.add(StringUtils.trim(splitPreserveAllTokens[i]));
            }
        }
        return directUsedVariable;
    }
}
