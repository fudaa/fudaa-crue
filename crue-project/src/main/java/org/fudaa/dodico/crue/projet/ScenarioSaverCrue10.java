/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.CommentaireContainer;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.etude.*;

import java.io.File;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Se charge de sauvegarder un scenario. Cette premiere version utilisee dans le lot 0 uniqument pour la generation de crue 9 en
 * crue 10. Utilise un fichier modele et genere tous les fichier avec les bons suffixes.
 *
 * @author Adrien Hadoux
 */
public class ScenarioSaverCrue10 {
  private final EMHProjet projet;
  private final EMHScenario scenarioToSave;
  private final ManagerEMHScenario scenario;
  private final CtuluLogGroup error;
  private final CoeurConfigContrat coeur;
  private final OrdonnanceurCrue10 ordonnanceur = new OrdonnanceurCrue10();
  private final Crue10FileFormatFactory factory = Crue10FileFormatFactory.getInstance();

  /**
   * Sauvegarder dans les fichiers deja existants:Fonction sauvegarder
   *
   * @param scenarioToSave    scenario a sauver
   * @param version           la version
   * @param scenarioToPersist le manager de scenario
   * @param error             pour stocker les erreurs de validation
   */
  protected ScenarioSaverCrue10(final EMHProjet projet, final EMHScenario scenarioToSave, final CoeurConfigContrat version,
                                final ManagerEMHScenario scenarioToPersist,
                                final CtuluLogGroup error) {
    super();
    this.error = error;
    this.projet = projet;
    this.scenarioToSave = scenarioToSave;
    this.scenario = scenarioToPersist;
    this.coeur = version;
  }

  /**
   * S'occupe de persister le scenario en crue 10
   *
   * @return true si réussite
   */
  protected boolean save(final Map<String, File> files, boolean valid) {
    final CtuluLog prevalidationLog = error.createNewLog("write.scenario.action.prevalidation");
    try {
      // -- recherche des fichiers sous modeles --//
      if (scenario.getFils().isEmpty()) {
        prevalidationLog.addError("noCurrentModele.error");
        return false;
      }
      final List<CrueFileType> typesSsModele = ordonnanceur.getSousModele();
      final List<CrueFileType> typesModele = ordonnanceur.getModele();
      final List<CrueFileType> typesScenario = ordonnanceur.getScenario();
      CrueConfigMetier crueProperties = coeur.getCrueConfigMetier();
      for (final EMHModeleBase modele : scenarioToSave.getModeles()) {
        final ManagerEMHModeleBase managerModele = scenario.getManagerFils(modele.getId());
        if (managerModele == null) {
          prevalidationLog.addSevereError("write.modele.noFound", modele.getNom(), scenario.getId());
          continue;
        }
        final List<EMHSousModele> fils = modele.getSousModeles();
        if (fils.isEmpty()) {
          prevalidationLog.addError("noCurrentSousModele.error", modele.getNom());
          return false;
        }
        for (final EMHSousModele emhSousModele : fils) {
          final ManagerEMHSousModele managerSousModele = managerModele.getManagerFils(emhSousModele.getId());
          if (managerSousModele == null) {
            prevalidationLog.addSevereError("write.sousModele.noFound", emhSousModele.getNom(), modele.getNom());
          }
          writeFile(typesSsModele, files, managerSousModele, CrueDataImpl.buildFor(emhSousModele, crueProperties), emhSousModele,
              prevalidationLog);
          // erreur fatale on arrete
          if (error.containsFatalError()) {
            return false;
          }
        }
        writeFile(typesModele, files, managerModele, CrueDataImpl.buildFor(modele, crueProperties), modele, prevalidationLog);
        if (error.containsFatalError()) {
          return false;
        }
      }
      CrueData crueData = CrueDataImpl.buildFor(scenarioToSave, crueProperties);
      writeFile(typesScenario, files, scenario, crueData, scenarioToSave, prevalidationLog);
      if (valid && crueData != null) {
        ScenarioExporterCrue10.validateCrueDataForCrue10(error, crueData);
      }
      if (error.containsFatalError()) {
        return false;
      }
    } catch (final Exception e) {
      Logger.getLogger(getClass().getName()).log(Level.SEVERE, "save", e);
      prevalidationLog.addSevereError("io.xml.erreur", e.getMessage());
    }
    return true;
  }

  private void writeFile(final List<CrueFileType> typesToWrite, final Map<String, File> idFile,
                         final ManagerEMHContainerBase content,
                         final CrueData dest, CommentaireContainer coms, CtuluLog prevalidationLog) {

    for (final CrueFileType type : typesToWrite) {
      //on ne s'occupe pas de ce fichier en version 1.1.1
      if (coeur.getXsdVersion().equals(Crue10VersionConfig.V_1_1_1) && CrueFileType.OPTR.equals(type)) {
        continue;
      }
      final boolean isDreg = CrueFileType.DREG.equals(type);
      if (isDreg && Crue10VersionConfig.isLowerOrEqualsTo_V1_2(coeur)) {
        continue;
      }
      final Crue10FileFormat fmt = factory.getFileFormat(type, coeur);


      FichierCrue file = content.getListeFichiers().getFile(type);
      if (file != null) {
        final String id = file.getNom();
        File targetFile = idFile.get(id);
        if (isDreg) {
          File initFile = file.getProjectFile(projet);
          if (initFile == null || !initFile.exists()) {
            try {
              initFile = new File(coeur.getDefaultFile(CrueFileType.DREG).toURI());
            } catch (URISyntaxException e) {
              throw new RuntimeException(e);
            }
          }
          if (!initFile.equals(targetFile)) {
            CrueFileHelper.copyFileWithSameLastModifiedDate(initFile, targetFile);
          }
        } else {
          final CrueIOResu<CrueData> out = new CrueIOResu<>(dest);
          out.setCrueCommentaire(coms.getCommentairesManager().getCommentaire(type));
          if (fmt == null) {
            prevalidationLog.addError("ihm.filetype.unknown.error", type);
            return;
          }

          fmt.write(out, targetFile, error.createLog());
        }
      } else {
        prevalidationLog.addWarn("write.fileTypeNotSaved.warn", type.toString());
      }
    }
  }
}
