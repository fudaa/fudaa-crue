/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.io.rcal.Crue10ResultatPasDeTempsDelegate;
import org.fudaa.dodico.crue.io.rcal.CrueDaoRCAL;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.ItemResDao;
import org.fudaa.dodico.crue.io.rdao.StructureResultatsDaoXstream;
import org.fudaa.dodico.crue.io.res.*;
import org.fudaa.dodico.crue.io.res.RCalTimeStepBuilder.Result;
import org.fudaa.dodico.crue.io.rptg.CrueDaoRPTG;
import org.fudaa.dodico.crue.io.rptg.ResPrtGeoDao;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.etude.RunFile;
import org.fudaa.dodico.crue.projet.create.RunPredecessorFileFinder;
import org.fudaa.dodico.crue.validation.*;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class ScenarioLoaderCrue10Result {
    public static final String STDOUTCSV_NAME = "stdout.csv";
    public static final String STDERRCSV_NAME = CompteRendusInfoEMH.STDERRCSV_NAME;
    final CtuluLog prevalidationLog;
    private final CtuluLogGroup logsGroup;
    private final Map<String, File> resFiles;
    private final EMHScenario emhScenario;
    private final CoeurConfigContrat coeur;
    private final ScenarioLoaderCrue10 crue10Loader;
    private final RunPredecessorFileFinder predecessorFinder;
    private final Map<String, RunFile> runFileById;
    private final Map<String, File> inputFiles;

    public ScenarioLoaderCrue10Result(final ScenarioLoaderCrue10 crue10Loader, final EMHScenario emhScenario,
                                      final CtuluLogGroup logsGroup, final Map<String, File> resFiles, final EMHRun run) {
        this.logsGroup = logsGroup;
        prevalidationLog = logsGroup.createNewLog("load.crue10.resultFiles.prevalidation");
        this.resFiles = resFiles;
        this.emhScenario = emhScenario;
        this.coeur = crue10Loader.getCoeur();
        this.crue10Loader = crue10Loader;
        runFileById = crue10Loader.getProjet().getRunFilesResultat(crue10Loader.getScenario(), run);
        inputFiles = crue10Loader.getProjet().getInputFiles(crue10Loader.getScenario(), run);
        predecessorFinder = new RunPredecessorFileFinder(crue10Loader.getCoeur(),crue10Loader.getScenario(), runFileById.values());
    }

    private static void addLog(final CtuluLog log, final CtuluLogGroup toAddIn) {
        if (log.isNotEmpty()) {
            toAddIn.addLog(log);
        }
    }

    public void loadResultats(final EMHModeleBase modele) {
        final CrueData crueData = CrueDataImpl.buildConcatFor(emhScenario, modele, coeur.getCrueConfigMetier());

        loadRPTR(modele, crueData);
        loadRPTG(modele, crueData);
        loadRPTI(modele, crueData);
        loadRCAL(modele, crueData);
        loadLogs(modele);
    }

    private long getLastModifiedPredecessor(final EMHModeleBase modele, final CrueFileType type) {
        final String resKey = ManagerEMHScenario.getResKey(modele, type);
        final RunFile runFile = runFileById.get(resKey);
        final Set<String> predecessorFile = predecessorFinder.getPredecessorFile(runFile);
        long res = -1;
        for (final String fileId : predecessorFile) {
            final File file = inputFiles.get(fileId);
            if (file != null && file.exists()) {
                res = Math.max(res, file.lastModified());
            }
            final RunFile inputRunFile = predecessorFinder.getRunFile(fileId);
            if (inputRunFile != null && inputRunFile.getFile() != null && inputRunFile.getFile().exists()) {
                res = Math.max(res, inputRunFile.getFile().lastModified());
            }
        }
        return res;
    }

    void loadRPTR(final EMHModeleBase modele, final CrueData crueData) {
        final String resKey = ManagerEMHScenario.getResKey(modele, CrueFileType.RPTR);
        final File rptr = resFiles.get(resKey);
        if (CtuluLibFile.exists(rptr)) {
            final long lastModification = getLastModifiedPredecessor(modele, CrueFileType.RPTR);
            if (rptr.lastModified() < lastModification) {
                prevalidationLog.addSevereError("load.rptrFile.cancelledBecauseTooOld");
            } else {
                Crue10FileFormatFactory.getVersion(CrueFileType.RPTR, coeur).read(rptr, logsGroup.createLog(), crueData);
                new ValidatorResultatCrue10Prt().validateResultatPretraitement(emhScenario, modele, logsGroup);
            }
        }
    }

    void loadRCAL(final EMHModeleBase modele, final CrueData crueData) {
        final File rcal = resFiles.get(ManagerEMHScenario.getResKey(modele, CrueFileType.RCAL));
        if (CtuluLibFile.exists(rcal)) {
            final long lastModification = getLastModifiedPredecessor(modele, CrueFileType.RCAL);
            if (rcal.lastModified() < lastModification) {
                prevalidationLog.addSevereError("load.rcalFile.cancelledBecauseTooOld");
            } else {
                final CtuluLogGroup rcalGroup = logsGroup.createGroup("load.rcal.operations");
                rcalGroup.setDescriptionArgs(rcal.getName());
                final Crue10FileFormat<Object> fmt = Crue10FileFormatFactory.getVersion(CrueFileType.RCAL, coeur);
                //le fichier xml n'est pas valide on annule le chargement:
                if (!fmt.isValide(rcal, rcalGroup.createLog())) {
                    return;
                }
                final CrueDaoRCAL res = (CrueDaoRCAL) fmt.read(rcal, logsGroup.createLog(), crueData).getMetier();
                if (res == null) {
                    return;
                }
                //extraction
                final ResTypeEMHBuilder typeBuilder = new ResTypeEMHBuilder(res);
                final CrueIOResu<List<ResCatEMHContent>> extract = typeBuilder.extract();
                addLog(extract.getAnalyse(), rcalGroup);
                final RCalTimeStepBuilder timeStepBuilder = new RCalTimeStepBuilder();
                final Result timeStepExtracted = timeStepBuilder.extract(res, rcal.getParentFile());
                addLog(timeStepExtracted.getLog(), rcalGroup);
                //validation resultat sur calcul actif
                final ValidatorResultatCrue10CalculInactif calcActif = new ValidatorResultatCrue10CalculInactif(emhScenario);
                addLog(calcActif.validCalculActif(timeStepExtracted.getOrderedKey()), rcalGroup);

                final ValidatorResultatCrue10VariablesDescriptor validator = new ValidatorResultatCrue10VariablesDescriptor();
                final List<ResCatEMHContent> categories = extract.getMetier();
                final CtuluLog validResLoiLog = validator.validVariables(categories, crueData.getCrueConfigMetier());
                addLog(validResLoiLog, rcalGroup);
                //toutes les EMHS actives doivent être présentes dans RCAL
                final ValidatorResultatCrue10EMHActive validatorEMHActive = new ValidatorResultatCrue10EMHActive();
                final CtuluLog validOnlyEMHActiveAreActiveLog = validatorEMHActive.validActiveEMHPresentInRes(categories, modele,
                        CtuluLogLevel.SEVERE);
                addLog(validOnlyEMHActiveAreActiveLog, rcalGroup);
                final ValidatorResultatCrue10BinaryFile validatorBinaryFile = new ValidatorResultatCrue10BinaryFile();
                final Crue10ResultatPasDeTempsDelegate timeDelegate = new Crue10ResultatPasDeTempsDelegate(timeStepExtracted.getOrderedKey(),
                        timeStepExtracted.getEntries());
                final CtuluLog validBinaryFile = validatorBinaryFile.validBinaryFile(timeDelegate.getEntries().values(), categories,
                        timeStepExtracted.getDelimiteurs());

                addLog(validBinaryFile, logsGroup);
                final CtuluLog logAssociation = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
                logAssociation.setDesc("load.rcal.associationEMH");
                final Map<String, EMH> emhByNom = crueData.getEMHByNom();
                //fin validation.
                for (final ResCatEMHContent categorie : categories) {
                    final List<ResTypeEMHContent> types = categorie.getResTypeEMHContent();
                    for (final ResTypeEMHContent type : types) {
                        final ResVariablesContent typeVariables = type.getResVariablesContent();
                        final List<ItemResDao> emhs = type.getItemResDao();
                        for (final ItemResDao emhDef : emhs) {
                            if (emhDef.getNbrMot() == 0) {
                                continue;
                            }
                            final String nom = emhDef.getNomRef();
                            final EMH emh = emhByNom.get(nom);
                            if (emh != null) {
                                boolean isRegul=EnumCatEMH.MODELE.equals(emh.getCatType()) && StructureResultatsDaoXstream.MODELE_REGUL.equals(type.getEmhType());
                                if (!isRegul && !emh.getEmhType().equals(type.getEmhType())) {
                                    final String emhType = emh.getEmhType();
                                    final String typeDefined = type.getEmhType();
                                    logAssociation.addError("load.rcal.emhTypeNonCompatible.error", emhType,
                                            typeDefined);
                                    continue;
                                }
                                if (!emh.getActuallyActive()) {
                                    //seules les EMHs actives doivent être definies dans RCAL.
                                    logAssociation.addError("res.validate.refEMHDefinedInResButNotActive", emh.getNom());
                                }
                                ResVariablesContent emhVariables = typeVariables;
                                if(isRegul){
                                    crueData.getCrueConfigMetier().addZRegulVariables(emhVariables.getZRegulVariablesId());
                                    crueData.getCrueConfigMetier().addQRegulVariables(emhVariables.getQRegulVariablesId());
                                }
                                if (CollectionUtils.isNotEmpty(emhDef.getVariableResLoiNbrPt())) {
                                    emhVariables = typeVariables.clone();
                                    emhVariables.addVariableResLoiNbrPtDao(emhDef.getVariableResLoiNbrPt());
                                }
                                final int length = emhDef.getNbrMot() * res.getParametrage().getNbrOctetMot();
                                final ResultatCalculCrue10 resultatCalcul = new ResultatCalculCrue10(type.getItemPositionInBytes(nom),
                                        length, emhVariables, timeDelegate,
                                        emh.getEmhType(), emh.getId());
                                emh.addInfosEMH(new ResultatCalcul(emh, resultatCalcul, false));
                            }
                        }
                    }
                }
                modele.addInfosEMH(new ResultatPasDeTemps(timeDelegate));
                addLog(logAssociation, logsGroup);
            }
        }
    }

    void loadLogs(final EMHModeleBase modele) {
        final Map<File, CrueFileType> logs = getLogFiles(modele, CrueFileType.RCAL, CrueFileType.RPTG, CrueFileType.RPTI, CrueFileType.RPTR);
        final CompteRendusInfoEMH info = new CompteRendusInfoEMH();
        modele.addInfosEMH(info);
        if (logs != null) {
            for (final Map.Entry<File, CrueFileType> entry : logs.entrySet()) {
                if (entry.getKey() != null && entry.getKey().exists()) {
                    info.addCompteRendu(new CompteRendu(entry.getKey(), entry.getValue()));
                }
            }
        }
        info.sort();
    }

    private Map<File, CrueFileType> getLogFiles(final EMHModeleBase modele, final CrueFileType... types) {
        final Map<File, CrueFileType> res = new HashMap<>();
        for (final CrueFileType type : types) {
            final File resFile = resFiles.get(ManagerEMHScenario.getResKey(modele, type));
            res.put(ScenarioLoaderCrue10FilesFinder.retrieveLogName(resFile, type), type);
        }
        return res;
    }

    void loadRPTG(final EMHModeleBase modele, final CrueData crueData) {
        final File rptg = resFiles.get(ManagerEMHScenario.getResKey(modele, CrueFileType.RPTG));
        if (CtuluLibFile.exists(rptg)) {
            final long lastModification = getLastModifiedPredecessor(modele, CrueFileType.RPTG);
            if (rptg.lastModified() < lastModification) {
                prevalidationLog.addSevereError("load.rptgFile.cancelledBecauseTooOld");
            } else {
                final CtuluLogGroup rptgGroup = logsGroup.createGroup("load.rptg.operations");
                rptgGroup.setDescriptionArgs(rptg.getName());
                final Crue10FileFormat<Object> fmt = Crue10FileFormatFactory.getVersion(CrueFileType.RPTG, coeur);
                //le fichier xml n'est pas valide on annule le chargement:
                if (!fmt.isValide(rptg, rptgGroup.createLog())) {
                    return;
                }
                final CrueDaoRPTG res = (CrueDaoRPTG) fmt.read(rptg, logsGroup.createLog(), crueData).getMetier();
                if (res == null) {
                    return;
                }
                rptgGroup.setDescriptionArgs(rptg.getName());
                //extraction
                final ResTypeEMHBuilder typeBuilder = new ResTypeEMHBuilder(res);
                final CrueIOResu<List<ResCatEMHContent>> extract = typeBuilder.extract();
                addLog(extract.getAnalyse(), rptgGroup);
                if (CollectionUtils.isEmpty(res.getResPrtGeos())) {
                    prevalidationLog.addSevereError("load.rptg.noResPrtGeoasFound.error", rptg.getName());
                    return;
                }
                final ResPrtGeoDao firstPrtGeo = res.getResPrtGeos().get(0);
                //on construit une clé temporaire:
                final ResultatTimeKey key = new ResultatTimeKey(ResPrtGeoDao.class.getSimpleName());
                final ResultatEntry entry = new ResultatEntry(key);
                entry.setBinFile(new File(rptg.getParentFile(), firstPrtGeo.getHref()));
                entry.setOffsetInBytes(res.getParametrage().getBytes(firstPrtGeo.getOffsetMot()));
                final Map<ResultatTimeKey, String> delimiteurs = new HashMap<>();
                delimiteurs.put(key, res.getParametrage().getDelimiteurChaine("ResPrtGeo"));
                final Map<ResultatTimeKey, ResultatEntry> pasDeTemps = new HashMap<>();
                pasDeTemps.put(key, entry);
                final Crue10ResultatPasDeTempsDelegate rptgPasDeTemps = new Crue10ResultatPasDeTempsDelegate(Collections.singletonList(key), pasDeTemps);
                //validation
                final ValidatorResultatCrue10VariablesDescriptor validator = new ValidatorResultatCrue10VariablesDescriptor();
                final List<ResCatEMHContent> categories = extract.getMetier();
                final CtuluLog validResLoiLog = validator.validVariables(categories, crueData.getCrueConfigMetier());
                addLog(validResLoiLog, rptgGroup);
                final ValidatorResultatCrue10EMHActive validatorEMHActive = new ValidatorResultatCrue10EMHActive();
                //toutes les EMHs doivent être déclarées dans le fichier RPTR:
                final CtuluLog validOnlyEMHActiveAreActiveLog = validatorEMHActive.validAllEMHPresentInRes(categories, modele,
                        CtuluLogLevel.SEVERE);
                addLog(validOnlyEMHActiveAreActiveLog, rptgGroup);
                final ValidatorResultatCrue10BinaryFile validatorBinaryFile = new ValidatorResultatCrue10BinaryFile();
                final CtuluLog validBinaryFile = validatorBinaryFile.validBinaryFile(Collections.singletonList(entry), categories,
                        delimiteurs);
                addLog(validBinaryFile, logsGroup);
                final CtuluLog logAssociation = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
                logAssociation.setDesc("load.rptg.associationEMH");
                final Map<String, EMH> simpleEMHByNom = crueData.getSimpleEMHByNom();
                //fin validation.
                for (final ResCatEMHContent categorie : categories) {
                    final List<ResTypeEMHContent> types = categorie.getResTypeEMHContent();
                    for (final ResTypeEMHContent type : types) {
                        final ResVariablesContent typeVariables = type.getResVariablesContent();
                        final List<ItemResDao> emhs = type.getItemResDao();
                        for (final ItemResDao emhDef : emhs) {
                            if (emhDef.getNbrMot() == 0) {
                                continue;
                            }
                            final String nom = emhDef.getNomRef();
                            final EMH emh = simpleEMHByNom.get(nom);
                            if (emh != null) {
                                if (!emh.getEmhType().equals(type.getEmhType())) {
                                    final String emhType = emh.getEmhType();
                                    final String typeDefined = type.getEmhType();
                                    logAssociation.addSevereError("load.emhTypeNonCompatible.error", emh.getNom(), emhType,
                                            typeDefined);
                                    continue;
                                }
                                ResVariablesContent emhVariables = typeVariables;
                                if (CollectionUtils.isNotEmpty(emhDef.getVariableResLoiNbrPt())) {
                                    emhVariables = typeVariables.clone();
                                    emhVariables.addVariableResLoiNbrPtDao(emhDef.getVariableResLoiNbrPt());
                                }
                                final int length = emhDef.getNbrMot() * res.getParametrage().getNbrOctetMot();

                                final ResultatCalculCrue10 resultatCalcul = new ResultatCalculCrue10(type.getItemPositionInBytes(nom),
                                        length, emhVariables, rptgPasDeTemps,
                                        emh.getEmhType(), emh.getId());
                                Map<String, Object> readData = null;
                                try {
                                    readData = resultatCalcul.read(key);
                                } catch (final IOException ex) {
                                    Logger.getLogger(ScenarioLoaderCrue10Result.class.getName()).log(Level.SEVERE, null, ex);
                                    prevalidationLog.addSevereError(ex.getMessage());
                                }
                                if (readData != null) {
                                    emh.addInfosEMH(new ResPrtGeo(readData, emh.getNom(), false, -1, false));
                                }
                            }
                        }
                    }
                }
                addLog(logAssociation, logsGroup);
            }
        }
    }

    private CrueData loadRPTI(final EMHModeleBase modele, final CrueData crueData) {
        final File rpti = resFiles.get(ManagerEMHScenario.getResKey(modele, CrueFileType.RPTI));
        if (CtuluLibFile.exists(rpti)) {
            final long lastModification = getLastModifiedPredecessor(modele, CrueFileType.RPTI);
            if (rpti.lastModified() < lastModification) {
                prevalidationLog.addError("load.rptiFile.cancelledBecauseTooOld");
            } else {
                Crue10FileFormatFactory.getVersion(CrueFileType.RPTI, coeur).read(rpti, logsGroup.createLog(), crueData);
            }
        }
        return crueData;
    }
}
