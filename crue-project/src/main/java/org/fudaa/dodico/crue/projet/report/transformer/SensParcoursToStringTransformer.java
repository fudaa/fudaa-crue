/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.transformer;

import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBrancheConfig;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBrancheConfig.SensParcours;

/**
 *
 * @author Frederic Deniger
 */
public class SensParcoursToStringTransformer extends AbstractPropertyToStringTransformer<ReportLongitudinalBrancheConfig.SensParcours> {

  public static final String POSITIF = "+";
  public static final String NEGATIF = "-";

  public SensParcoursToStringTransformer() {
    super(SensParcours.class);
  }

  @Override
  public String toStringSafe(SensParcours in) {
    return SensParcours.AVAL_AMONT.equals(in) ? NEGATIF : POSITIF;
  }

  @Override
  public boolean isValidSafe(String in) {
    return POSITIF.equals(in) || NEGATIF.equals(in);
  }

  @Override
  public SensParcours fromStringSafe(String in) {
    if (NEGATIF.equals(in)) {
      return SensParcours.AVAL_AMONT;
    }
    return SensParcours.AMONT_AVAL;
  }
}
