package org.fudaa.dodico.crue.projet.importer;

import org.apache.commons.io.filefilter.PrefixFileFilter;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.Pair;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.common.ConnexionInformationDefault;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.etude.EMHInfosVersion;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.projet.OrdonnanceurCrue10;
import org.fudaa.dodico.crue.projet.planimetry.GisLayerId;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Importe un sous-modèle a partir d'un dossier
 */
public class SmImportFromFolder {
  private final EMHProjet project;
  private final List<CrueFileType> crueFileTypes;
  private final ConnexionInformation connexionInfos;

  public SmImportFromFolder(EMHProjet project, ConnexionInformation connexionInfos) {
    this.project = project;
    crueFileTypes = new OrdonnanceurCrue10().getSousModele();
    this.connexionInfos = connexionInfos;
  }

  public CtuluLog importSousModele(File drsoFile, String comment) {
    return importSousModele(drsoFile, comment, true);
  }

  protected CtuluLog importSousModele(File drsoFile, String comment, boolean validGrammar) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    SmImportValidator validator = new SmImportValidator(project);
    final Pair<Boolean, String> valid = validator.canImportSousModele(drsoFile, validGrammar);
    if (!valid.firstValue) {
      log.addSevereError(valid.secondValue);
      return log;
    }
    String radical = SmImportValidator.getSousModeleRadical(drsoFile, CrueFileType.DRSO);
    copySousModeleFiles(drsoFile, radical, log);
    if (log.containsErrorOrSevereError()) {
      return log;
    }
    final ManagerEMHSousModele managerEMHSousModele = addSousModele(radical, comment, log);
    copyGisFiles(managerEMHSousModele, drsoFile, log);
    return log;
  }

  private ManagerEMHSousModele addSousModele(String radical, String comment, CtuluLog log) {
    final ManagerEMHSousModele sousModele = createSousModele(radical, comment);

    final List<FichierCrue> listeFiles = new ArrayList<>();
    for (CrueFileType fileType : crueFileTypes) {
      FichierCrue fichierCrue = new FichierCrue(radical + "." + fileType.getExtension(), null, fileType);
      final boolean added = project.getInfos().addCrueFileToProject(fichierCrue);
      listeFiles.add(fichierCrue);
      if (!added) {
        log.addSevereError("sm.import.cantAddFileEtu", fichierCrue.getNom());
      }
    }
    sousModele.setListeFichiers(listeFiles);
    project.addBaseSousModele(sousModele);
    return sousModele;
  }

  /**
   * @param radical le radical du sous-modele
   * @param comment le commentaire
   * @return le sous-modele
   */
  private ManagerEMHSousModele createSousModele(String radical, String comment) {
    final ManagerEMHSousModele sousModele = new ManagerEMHSousModele();
    sousModele.setNom(CruePrefix.addPrefix(radical, EnumCatEMH.SOUS_MODELE));
    final EMHInfosVersion infosVersion = new EMHInfosVersion();
    infosVersion.updateForm(connexionInfos == null ? ConnexionInformationDefault.getDefault() : connexionInfos);
    infosVersion.setVersion(CrueVersionType.CRUE10);
    if (comment != null) {
      infosVersion.setCommentaire(comment);
    }
    sousModele.setActive(true);
    sousModele.setInfosVersions(infosVersion);
    return sousModele;
  }

  private void copySousModeleFiles(File drsoFile, String radical, CtuluLog log) {
    File dir = drsoFile.getParentFile();
    for (CrueFileType fileType : crueFileTypes) {
      File source = new File(dir, radical + "." + fileType.getExtension());
      File target = new File(project.getDirOfFichiersEtudes(), source.getName());
      if (!CtuluLibFile.copyFile(source, target)) {
        log.addSevereError("sm.import.cantCopyFile", target.getName());
      }
    }
  }

  public static File getConfigDir(File drsoFile) {
    return new File(drsoFile.getParent(), "Config");
  }

  private void copyGisFiles(ManagerEMHSousModele sousModele, File drsoFile, CtuluLog log) {
    final File dirOfConfig = project.getInfos().getDirOfConfig(sousModele);
    File dirToImport = new File(getConfigDir(drsoFile), File.separator + sousModele.getId());
    if (dirToImport.exists()) {
      //on peut créer le dossier
      if (!dirOfConfig.exists() && !dirOfConfig.mkdirs()) {
        log.addSevereError("sm.import.cantCreateFolder", dirOfConfig.getAbsolutePath());
        return;
      }
      Arrays.stream(GisLayerId.values()).forEach(gisLayerId -> copyFile(gisLayerId, dirToImport, dirOfConfig, log));
    }
  }

  /**
   * @param id le type de fichier a copier
   * @param sourceDir le répertoire initial
   * @param targetDir la cible
   * @param log les logs
   */
  private void copyFile(GisLayerId id, File sourceDir, File targetDir, CtuluLog log) {
    final File[] files = sourceDir.listFiles((FilenameFilter) new PrefixFileFilter(id.getFilename() + "."));
    if (files != null) {
      for (File file : files) {
        if (!CtuluLibFile.copyFile(file, new File(targetDir, file.getName()))) {
          log.addWarn("sm.import.cantCopyFile", file.getAbsolutePath());
        }
      }
    }
  }
}
