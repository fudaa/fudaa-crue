/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("ReportIndex")
public class IndexDao extends AbstractCrueDao {

  @XStreamAlias("Type")
  String type;
  @XStreamImplicit(itemFieldName = "VueConfig")
  private List<ReportViewLineInfo> infoLines = new ArrayList<>();

  public String getType() {
    return type;
  }

  public List<ReportViewLineInfo> getLines() {
    return infoLines;
  }

  public void setInfoLines(List<ReportViewLineInfo> infoLines) {
    this.infoLines = infoLines;
  }

  public void setType(String type) {
    this.type = type;
  }
}
