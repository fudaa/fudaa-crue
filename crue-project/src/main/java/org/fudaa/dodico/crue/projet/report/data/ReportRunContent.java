/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

import java.util.Map;

/**
 * Conteneur complet d'un run
 *
 * @author Frederic Deniger
 */
public class ReportRunContent {
    private final EMHRun run;
    private final ManagerEMHScenario managerScenario;
    private final ReportRunKey runKey;
    private EMHScenario scenario;
    private Map<String, EMH> emhByName;
    private boolean loaded;

    /**
     * @param run             le run
     * @param managerScenario le scenario parent
     * @param currentKey      la clé du run
     */
    public ReportRunContent(EMHRun run, ManagerEMHScenario managerScenario, ReportRunKey currentKey) {
        this.run = run;
        this.managerScenario = managerScenario;
        assert ReportRunEnum.CURRENT.equals(currentKey.getRunType());
        this.runKey = currentKey;
    }

    /**
     * @param run             le run
     * @param managerScenario le scenario parent
     */
    public ReportRunContent(EMHRun run, ManagerEMHScenario managerScenario) {
        this.run = run;
        this.managerScenario = managerScenario;
        this.runKey = new ReportRunKey(managerScenario.getNom(), run.getNom());
    }

    /**
     * @return true si loaded
     */
    public boolean isLoaded() {
        return loaded;
    }

    /**
     * @param loaded nouvel état loaded du run
     */
    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    /**
     * @param nom nom de l'EMH
     * @return l'EMH correspondante
     */
    public EMH getEMH(String nom) {
        if (emhByName == null) {
            return null;
        }
        return emhByName.get(nom);
    }

    /**
     * @return la clé du run
     */
    public ReportRunKey getRunKey() {
        return runKey;
    }

    /**
     * Pour les runs alternatifs, le scénario n'est pas chargé de suite.
     *
     * @return le scenario si charge
     */
    public EMHScenario getScenario() {
        return scenario;
    }

    /**
     * @param scenario le scenario chargé
     */
    public void setScenario(EMHScenario scenario) {
        this.scenario = scenario;
        emhByName = scenario.getIdRegistry().getEmhByNom();
    }

    /**
     * @return le run
     */
    public EMHRun getRun() {
        return run;
    }

    /**
     * @return le scenario parent
     */
    public ManagerEMHScenario getManagerScenario() {
        return managerScenario;
    }
}
