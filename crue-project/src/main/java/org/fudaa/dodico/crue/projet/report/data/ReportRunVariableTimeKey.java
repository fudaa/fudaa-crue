/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;

/**
 * Une clé pour run / variable / pas de temps.
 *
 * @author Frederic Deniger
 */
@XStreamAlias("ReportRunVariableTimeKey")
public class ReportRunVariableTimeKey implements ReportKeyContract {
  public static final ReportRunVariableTimeKey NULL = new ReportRunVariableTimeKey(ReportRunVariableKey.NULL_VALUE, ResultatTimeKey.NULL_VALUE);
  final ReportRunVariableKey runVariableKey;
  final ResultatTimeKey resultatKey;

  public ReportRunVariableTimeKey(ReportRunKey runKey, ReportVariableTypeEnum type, String variable, ResultatTimeKey resultatKey) {
    this(new ReportRunVariableKey(runKey, type, variable), resultatKey);
  }

  public ReportRunVariableTimeKey(ReportRunVariableKey runKey, ResultatTimeKey resultatKey) {
    this.runVariableKey = runKey;
    this.resultatKey = resultatKey;
    assert runKey != null;
  }

  @Override
  public boolean isExtern() {
    return false;
  }

  @Override
  public ReportRunKey getReportRunKey() {
    return runVariableKey.getReportRunKey();
  }

  public boolean isDefinedOnSpecifiedTimeStep() {
    return resultatKey != null;
  }

  public boolean isDefinedOnSelectedTimeStep() {
    return !isDefinedOnSpecifiedTimeStep();
  }

  public boolean isNotTimeDependant() {
    return isLimitProfilVariable() || isLHPTVariable() || isRPTIVariable();
  }

  public boolean isLimitProfilVariable() {
    return ReportVariableTypeEnum.LIMIT_PROFIL.equals(runVariableKey.getVariable().getVariableType());
  }

  public boolean isRPTIVariable() {
    return ReportVariableTypeEnum.RESULTAT_RPTI.equals(runVariableKey.getVariable().getVariableType());
  }

  public boolean isLHPTVariable() {
    return ReportVariableTypeEnum.LHPT.equals(runVariableKey.getVariable().getVariableType());
  }

  public boolean isReadVariable() {
    return ReportVariableTypeEnum.READ.equals(runVariableKey.getVariable().getVariableType());
  }

  public String getDisplayName(ToStringTransformer timeToStringTransformer) {
    if (resultatKey == null) {
      return getRunVariableKey().getVariable().getVariableDisplayName() + BusinessMessages.ENTITY_SEPARATOR + getRunVariableKey().getRunKey().getDisplayName();
    }
    return getRunVariableKey().getVariable().getVariableDisplayName() + BusinessMessages.ENTITY_SEPARATOR + timeToString(
        timeToStringTransformer) + BusinessMessages.ENTITY_SEPARATOR + getRunVariableKey().
        getRunKey().getDisplayName();
  }

  public String getDisplayName(String other, ToStringTransformer timeToStringTransformer) {
    if (resultatKey == null) {
      return getRunVariableKey().getVariable().getVariableDisplayName() + other + BusinessMessages.ENTITY_SEPARATOR + getRunVariableKey().getRunKey().getDisplayName();
    }
    return getRunVariableKey().getVariable().getVariableDisplayName() + other + BusinessMessages.ENTITY_SEPARATOR + timeToString(
        timeToStringTransformer) + BusinessMessages.ENTITY_SEPARATOR + getRunVariableKey().
        getRunKey().getDisplayName();
  }

  @Override
  public ReportRunVariableTimeKey duplicateWithNewVar(String newVarName) {
    return new ReportRunVariableTimeKey(runVariableKey.duplicateWith(newVarName), resultatKey);
  }

  @Override
  public String getVariableName() {
    return runVariableKey.getVariableName();
  }

  public ReportRunVariableKey getRunVariableKey() {
    return runVariableKey;
  }

  public ResultatTimeKey getResultatKey() {
    return resultatKey;
  }

  @Override
  public int hashCode() {
    return 5 + runVariableKey.hashCode() * 7 + (resultatKey == null ? 0 : resultatKey.hashCode() * 13);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ReportRunVariableTimeKey other = (ReportRunVariableTimeKey) obj;
    if (this.runVariableKey != other.runVariableKey && (this.runVariableKey == null || !this.runVariableKey.equals(other.runVariableKey))) {
      return false;
    }
    if ((this.resultatKey == null) ? (other.resultatKey != null) : !this.resultatKey.equals(other.resultatKey)) {
      return false;
    }
    return true;
  }

  private Comparable<? extends Comparable<?>> timeToString(ToStringTransformer timeToStringTransformer) {
    return timeToStringTransformer == null ? getResultatKey() : timeToStringTransformer.transform(getResultatKey());
  }
}
