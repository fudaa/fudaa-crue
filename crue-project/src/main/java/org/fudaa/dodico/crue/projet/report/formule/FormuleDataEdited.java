/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * Les données issues d'une modifications de l'UI sans traitement.
 *
 * @author Frederic Deniger
 */
public class FormuleDataEdited {
    private final List<FormuleParameters> newParameters = new ArrayList<>();
    private final Map<String, FormuleParameters> renamedParametersByOldId = new HashMap<>();

    /**
     * @param oldId      si oldId vaut null ou vide, le parameter est considéré comme nouveau
     * @param parameters le nouveau parameters ou le renommé
     */
    public void addParameter(String oldId, FormuleParameters parameters) {
        if (StringUtils.isBlank(oldId)) {
            newParameters.add(parameters);
        } else {
            renamedParametersByOldId.put(oldId, parameters);
        }
    }

    public List<FormuleParameters> getNewParameters() {
        return Collections.unmodifiableList(newParameters);
    }

    public List<FormuleParameters> getAllParameters() {
        List<FormuleParameters> all = new ArrayList<>();
        all.addAll(newParameters);
        all.addAll(renamedParametersByOldId.values());
        return all;
    }

    /**
     * Ne vérifie pas si le contenu est égale
     *
     * @param oldId l'ancien id
     * @return true si la oldId est probablement modifié
     */
    public boolean isProbablyModified(String oldId) {
        return renamedParametersByOldId.containsKey(oldId);
    }

    public String getNewId(String oldName) {
        FormuleParameters get = renamedParametersByOldId.get(oldName);
        return get == null ? null : get.getId();
    }

    public Map<String, FormuleParameters> getModifiedParametersByOldName() {
        return Collections.unmodifiableMap(renamedParametersByOldId);
    }
}
