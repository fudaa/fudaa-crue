/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.migrate;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;

public class ChildrenFilesTester {

  private File baseDir;

  public ChildrenFilesTester(final File baseDir) {
    this.baseDir = baseDir.getAbsoluteFile();
    try {
      this.baseDir = baseDir.getCanonicalFile();
    } catch (final IOException iOException) {
    }
    assert (baseDir != null) && baseDir.isDirectory();
  }

  public boolean test(final List<File> files, final CtuluLog log) {
    boolean allChildren = true;
    log.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    for (final File file : files) {
      if (!this.isChild(file)) {
        log.addSevereError("migrate.isChildrenFilesTester.isNotChild", file.getName());

        allChildren = false;
      }
    }

    return allChildren;
  }

  /**
   * Test si un fichier est fils du baseDir (fichier et répertoire on la même signification dans ce cas).
   *
   * @param file Le fichier à tester.
   * @return True si toTest est fils de parent.
   */
  public boolean isChild(final File file) {
    if (file == null) {
      return false;
    }

    if (baseDir.equals(file)) {
      return true;
    }

    return this.isChild(file.getParentFile());
  }
}
