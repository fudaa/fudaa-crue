/*
GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule.function;

import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;

/**
 *
 * @author Frederic Deniger
 */
public class AggregationFunctionDefault extends AbstractAggregationFunction {

  public AggregationFunctionDefault(EnumFormuleStat statKey, ReportGlobalServiceContrat reportGlobalService) {
    super(statKey, reportGlobalService);
  }

}
