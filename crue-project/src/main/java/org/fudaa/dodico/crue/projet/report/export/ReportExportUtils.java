/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.export;

import java.util.List;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.config.lit.LitNommeLimite;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilEtiquette;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.helper.EtiquetteIndexed;
import org.fudaa.dodico.crue.metier.helper.LitNommeIndexed;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalHelper;
import org.fudaa.dodico.crue.validation.ValidateAndRebuildProfilSection;

/**
 *
 * @author Frederic Deniger
 */
public class ReportExportUtils {

  public static Double computeZ(ItemEnum etiquette, LitNommeLimite limite, DonPrtGeoProfilSection profil,
          CrueConfigMetier ccm) {
    if (etiquette != null) {
      CrueIOResu<List<EtiquetteIndexed>> etiquettesAsIndexed = new ValidateAndRebuildProfilSection(ccm, null).getEtiquettesAsIndexed(profil);
      DonPrtGeoProfilEtiquette foundPt = EtiquetteIndexed.find(etiquettesAsIndexed.getMetier(), etiquette);
      if (foundPt != null) {
        return Double.valueOf(foundPt.getPoint().getZ());
      }
    } else {
      if (limite != null) {
        CrueIOResu<List<LitNommeIndexed>> asIndexedData = new ValidateAndRebuildProfilSection(ccm, null).getAsIndexedData(profil);
        if (asIndexedData != null && asIndexedData.getMetier() != null) {
          int ptIdx = ReportLongitudinalHelper.finPtIdx(asIndexedData, limite);
          if (ptIdx >= 0) {
            return Double.valueOf(profil.getPtProfil().get(ptIdx).getZ());
          }
        }
      }
    }
    return null;
  }

}
