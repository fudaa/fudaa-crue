/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.select;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.helper.DonFrtHelper;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author deniger
 */
public class UniqueNomFinder {
    private static final String SEPARATOR = "_";
    private final int minDigits = 4;

    /**
     * Il est suppose que l'IdRegistry est à jour.
     *
     * @param emhCat   la categorie d'EMH
     * @param scenario le scenario
     * @param nb       nombre de nom a trouver
     * @return liste des noms
     */
    public List<String> findNewNames(EnumCatEMH emhCat, EMHScenario scenario, int nb) {
        List<EMH> emhs = scenario.getIdRegistry().getEMHs(emhCat);
        String prefix = CruePrefix.getPrefix(emhCat);
        Collection<String> names = TransformerHelper.toNom(emhs);
        return findNewNames(names, prefix, nb);
    }

    private Collection<String> getExistingDonFrtNames(EMHScenario scenario) {
        List<DonFrt> allDonFrt = DonFrtHelper.getDonFrt(scenario);
        return TransformerHelper.toSetNom(allDonFrt);
    }

    private String addSuffixeIdentifier(Collection<String> names, String initName, int nbDigits) {
        int maxId = findMaxIdentifier(names, initName);
        String id = getIdAsString(maxId, nbDigits);
        if (initName.isEmpty()) {
            return id;
        }
        if (initName.charAt(initName.length() - 1) == '_') {
            return initName + id;
        }
        return initName + SEPARATOR + id;
    }

    /**
     * Ne modifie pas et ne teste pas le nom initUniqueName. Ajoute simplement _XXX pour avoir un nom unique.
     *
     * @param existingNames  les noms existants
     * @param initUniqueName doit etre du style Pc_ABC
     * @return le nom unique
     */
    protected String findUniqueNameProfilCasier(Collection<String> existingNames, String initUniqueName) {
        String newName = initUniqueName;
        if (!newName.endsWith(SEPARATOR)) {
            newName = newName + SEPARATOR;
        }
        return addSuffixeIdentifier(existingNames, newName, 3);
    }

    /**
     * @param maxCurrentId le currentId max trouvé
     */
    private String getIdAsString(int maxCurrentId) {
        return getIdAsString(maxCurrentId, minDigits);
    }

    private String getIdAsString(int maxCurrentId, int nbDigits) {
        String id = Integer.toString(maxCurrentId + 1);
        if (id.length() < nbDigits) {
            id = CtuluLibString.adjustSizeBefore(nbDigits, id, '0');
        }
        return id;
    }

    public boolean isUsed(EnumCatEMH emhCat, EMHScenario scenario, String name) {
        List<EMH> emhs = scenario.getIdRegistry().getEMHs(emhCat);
        Collection<String> names = TransformerHelper.toNom(emhs);
        return names.contains(name);
    }

    private int findMaxIdentifier(EMHScenario scenario, EnumCatEMH emhCat) {
        List<EMH> emhs = scenario.getIdRegistry().getEMHs(emhCat);
        String prefix = CruePrefix.getPrefix(emhCat);
        Collection<String> names = TransformerHelper.toNom(emhs);
        return findMaxIdentifier(names, prefix);
    }

    public int findMaxIdentifier(EMHScenario scenario, EnumCatEMH... emhCat) {
        int max = 0;
        for (EnumCatEMH enumCatEMH : emhCat) {
            int value = findMaxIdentifier(scenario, enumCatEMH);
            max = Math.max(max, value);
        }
        return max;
    }

    public String findNewName(EnumCatEMH emhCat, EMHScenario scenario) {
        List<EMH> emhs = scenario.getIdRegistry().getEMHs(emhCat);
        String prefix = CruePrefix.getPrefix(emhCat);
        Collection<String> names = TransformerHelper.toNom(emhs);
        return findNewName(names, prefix);
    }

    /**
     * @param emhCategory la categorie d'EMH
     * @param scenario    le scenario
     * @param initName    doit commencer par le prefix de la catégorie.
     * @return un nom unique
     */
    public String findUniqueName(EnumCatEMH emhCategory, EMHScenario scenario, String initName) {
        List<EMH> emhs = scenario.getIdRegistry().getEMHs(emhCategory);
        String prefix = CruePrefix.getPrefix(emhCategory);
        assert initName.startsWith(prefix);
        Collection<String> names = TransformerHelper.toNom(emhs);
        return findUniqueName(names, initName);
    }

    private boolean isInteger(String in) {
        try {
            Integer.parseInt(in);
            return true;
        } catch (NumberFormatException numberFormatException) {
        }
        return false;
    }

    /**
     * Pour le profil casier, on doit ajouter _XXX
     *
     * @param initUniqueName nom unique du casier
     * @param scenario le scenario
     */
    public String findUniqueNameProfilCasier(String initUniqueName, EMHScenario scenario) {
        Set<DonPrtGeoProfilCasier> allProfilCasier = DonPrtHelper.getAllProfilCasier(scenario);
        Collection<String> toNom = TransformerHelper.toSetNom(allProfilCasier);
        return findUniqueNameProfilCasier(toNom, initUniqueName);
    }

    public String findNewNameDonFrtStrickler(EMHScenario scenario) {
        Collection<String> toNom = getExistingDonFrtNames(scenario);
        return findNewName(toNom, CruePrefix.P_FROTT_STRICKLER);
    }

    public String findNewNameDonFrtStricklerStockageEmpty(EMHScenario scenario) {
        Collection<String> toNom = getExistingDonFrtNames(scenario);
        return findNewName(toNom, CruePrefix.P_FROTT_STRICKLER_STO + "K0_");
    }

    /**
     * Attention pas de tests sur le initUniqueName pour voir s'il commencer par les préfixes
     */
    public String findUniqueNameDonFrt(String initUniqueName, EMHScenario scenario) {
        Collection<String> toNom = getExistingDonFrtNames(scenario);
        assert initUniqueName.startsWith(CruePrefix.P_FROTT_MANNING)
                || initUniqueName.startsWith(CruePrefix.P_FROTT_STRICKLER_STO)
                || initUniqueName.startsWith(CruePrefix.P_FROTT_STRICKLER);
        return findUniqueName(toNom, initUniqueName);
    }

    public String findUniqueNameProfilSection(String initUniqueName, EMHScenario scenario) {
        Set<DonPrtGeoProfilSection> allProfilCasier = DonPrtHelper.getAllProfilSection(scenario);
        Collection<String> toNom = TransformerHelper.toSetNom(allProfilCasier);
        return findUniqueName(toNom, initUniqueName);
    }

    /**
     * @param names les noms existants
     * @param initUniqueName le nom intiale
     * @return nom unique. Si initUniqueName n'est pas compris dans la liste des noms existants, il sera renvoyé en l'état
     */
    public String findUniqueName(Collection<String> names, String initUniqueName) {
        return findUniqueName(names, initUniqueName, minDigits);
    }

    /**
     * @param names les noms existants
     * @param initUniqueName le nom intiale
     * @param nbDigits       le nombre de chiffres à utiliser pour construire l'index.
     * @return nom unique. Si initUniqueName n'est pas compris dans la liste des noms existants, il sera renvoyé en l'état
     */
    public String findUniqueName(Collection<String> names, String initUniqueName, int nbDigits) {
        String initName = initUniqueName;
        if (!names.contains(initName) && !initName.isEmpty()) {
            return initName;
        }
        //se termine par un chiffre ? Dans ce cas, on va le remplacer.
        if (initName.indexOf('_') > 0) {
            String last = StringUtils.substringAfterLast(initName, SEPARATOR);
            if (isInteger(last)) {
                initName = StringUtils.substringBeforeLast(initName, SEPARATOR) + SEPARATOR;
            }
        }
        return addSuffixeIdentifier(names, initName, nbDigits);
    }

    public List<String> findNewNames(Collection<String> names, String prefix, int nb) {
        int maxId = findMaxIdentifier(names, prefix);
        List<String> res = new ArrayList<>();
        for (int i = 0; i < nb; i++) {
            String id = Integer.toString(maxId + i + 1);
            if (id.length() < minDigits) {
                id = CtuluLibString.adjustSizeBefore(minDigits, id, '0');
            }
            res.add(prefix + id);
        }
        return res;
    }

    public String findNewName(Collection<String> names, String prefix) {
        int maxId = findMaxIdentifier(names, prefix);
        String id = getIdAsString(maxId);
        return prefix + id;
    }

    public String getName(EnumCatEMH cat, int currentMaxId) {
        return CruePrefix.getPrefix(cat) + getIdAsString(currentMaxId);
    }

    public int findMaxIdentifier(List<EMH> emhs, String prefix) {
        return findMaxIdentifier(TransformerHelper.toNom(emhs), prefix);
    }

    public int findMaxIdentifier(Collection<String> noms, String prefix) {
        int res = 0;
        for (String nom : noms) {
            if (!nom.equals(prefix) && nom.startsWith(prefix)) {
                String suffix = nom.substring(prefix.length());
                if (suffix.length() > 0 && suffix.charAt(0) == '_') {
                    suffix = suffix.substring(1);
                }
                int id = 0;
                try {
                    id = Integer.parseInt(suffix);
                } catch (NumberFormatException numberFormatException) {
                }
                res = Math.max(id, res);
            }
        }
        return res;
    }
}
