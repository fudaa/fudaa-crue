/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.common.LocalDateTimeConverter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public class ReportIndexReader implements CrueDataDaoStructure {
  public static final String INDEX_FILENAME = "index.xml";

  public CrueIOResu<List<ReportViewLineInfo>> read(final File targetFile) {
    final ReportXstreamReaderWriter<IndexDao> readerWriter = new ReportXstreamReaderWriter<>("report-index", ReportXstreamReaderWriter.VERSION, this);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final IndexDao readDao = readerWriter.readDao(targetFile, log, null);
    final List<ReportViewLineInfo> res = new ArrayList<>();
    if (readDao != null && readDao.getLines() != null) {
      res.addAll(readDao.getLines());
    }
    final CrueIOResu<List<ReportViewLineInfo>> resuFinal = new CrueIOResu<>();
    resuFinal.setAnalyse(log);
    resuFinal.setMetier(res);
    return resuFinal;
  }

  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    xstream.processAnnotations(IndexDao.class);
    xstream.processAnnotations(ReportViewLineInfo.class);
    xstream.registerConverter(new LocalDateTimeConverter());
  }
}
