package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOperationsData;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateSourceTargetData;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Genere les logs pour les sections detachees
 */
public class SmUpdateDataDetachedSectiondentifier {
  private final CrueConfigMetier ccm;

  public SmUpdateDataDetachedSectiondentifier(CrueConfigMetier ccm) {
    this.ccm = ccm;
  }

  public CtuluLog generateLogForDetachedSection(SmUpdateOperationsData data) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("sm.updater.sectionNotAttachedLog");
    final List<SmUpdateSourceTargetData.Line<CatEMHBranche>> modifiedBranches = data.getSourceTargetData().getBranches().stream()
      .filter(new SmUpdateBrancheSameRelationSectionPredicate(ccm).negate()).collect(Collectors.toList());
    final Map<String, EMH> sourceEMHByName = TransformerHelper.toMapOfNom(data.getSourceEmhsToUpdate());
    modifiedBranches.forEach(modifiedBranche -> {
      final CatEMHBranche targetToModify = modifiedBranche.getTargetToModify();
      targetToModify.getSections().forEach(targetSection -> {
        final CatEMHSection emhInSource = (CatEMHSection) sourceEMHByName.get(targetSection.getEmhNom());
        if (emhInSource == null || emhInSource.getBranche() == null) {
          log.addWarn("sm.updater.sectionNotAttachedLogItem", targetSection.getEmhNom(), targetSection.getEmh().getSectionType().geti18n());
        }
      });
    });

    return log;
  }
}
