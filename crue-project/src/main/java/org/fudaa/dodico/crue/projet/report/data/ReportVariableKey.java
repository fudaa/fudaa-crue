/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

/**
 * @author Frederic Deniger
 */
public class ReportVariableKey implements ToStringInternationalizable, ReportKeyContract {
  public static final ReportVariableKey EMPTY = new ReportVariableKey(ReportVariableTypeEnum.EXPR, " ");
  public static final ReportVariableKey NULL = new ReportVariableKey(ReportVariableTypeEnum.EXPR, "null");
  final ReportVariableTypeEnum variableType;
  final String variableName;
  String displayName;

  public ReportVariableKey(ReportVariableTypeEnum type, String varName) {
    this.variableName = varName;
    this.variableType = type;
    displayName = varName;
    if (ReportVariableTypeEnum.READ.equals(variableType)
        || variableType.equals(ReportVariableTypeEnum.LHPT)
        || variableType.equals(ReportVariableTypeEnum.RESULTAT_RPTI)) {
      displayName = StringUtils.capitalize(variableName);
    }
  }

  public static String toString(ReportVariableKey key) {
    if (key == EMPTY) {
      return " ";
    }
    String name = key.getVariableName();
    final ReportVariableTypeEnum variableType = key.getVariableType();
    if (variableType.equals(ReportVariableTypeEnum.TIME)) {
      return BusinessMessages.getString("Variable.Time." + name);
    } else if (variableType.equals(ReportVariableTypeEnum.READ) || variableType.equals(ReportVariableTypeEnum.RESULTAT_RPTI)) {
      return StringUtils.capitalize(name);
    } else if (variableType.equals(ReportVariableTypeEnum.LHPT)) {
      return StringUtils.capitalize(name);
    } else if (variableType.equals(ReportVariableTypeEnum.EXPR)) {
      return ReportExpressionHelper.geti18n(name);
    } else if (variableType.equals(ReportVariableTypeEnum.FORMULE)) {
      return key.getVariableDisplayName();
    }
    return name;
  }

  @Override
  public ReportKeyContract duplicateWithNewVar(String newName) {
    return new ReportVariableKey(variableType, newName);
  }

  @Override
  public ReportRunKey getReportRunKey() {
    return null;
  }

  @Override
  public boolean isExtern() {
    return false;
  }

  @Override
  public String geti18n() {
    return toString(this);
  }

  @Override
  public String geti18nLongName() {
    return toString(this);
  }

  public ReportVariableKey duplicateWith(String newVarName) {
    return new ReportVariableKey(variableType, newVarName);
  }

  public ReportVariableTypeEnum getVariableType() {
    return variableType;
  }

  @Override
  public String getVariableName() {
    return variableName;
  }

  public String getVariableDisplayName() {
    if (displayName != null) {
      return displayName;
    }
    if (ReportVariableTypeEnum.READ.equals(variableType)) {
      displayName = StringUtils.capitalize(variableName);
      return displayName;
    }
    return variableName;
  }

  @Override
  public int hashCode() {
    int hash = 3 + variableType.hashCode() * 13;
    if (variableName != null) {
      hash = hash + variableName.hashCode() * 7;
    }
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ReportVariableKey other = (ReportVariableKey) obj;
    if (this.variableType != other.variableType) {
      return false;
    }
    if ((this.variableName == null) ? (other.variableName != null) : !this.variableName.equals(other.variableName)) {
      return false;
    }
    return true;
  }
}
