package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateSourceTargetData;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Renvoie true si les relations sections change
 */
public class SmUpdateBrancheSameRelationSectionPredicate implements Predicate<SmUpdateSourceTargetData.Line<CatEMHBranche>> {
  private final CrueConfigMetier ccm;

  public SmUpdateBrancheSameRelationSectionPredicate(CrueConfigMetier ccm) {
    this.ccm = ccm;
  }

  @Override
  public boolean test(SmUpdateSourceTargetData.Line<CatEMHBranche> brancheLine) {
    return isSameSectionsRelations(brancheLine);
  }

  public boolean isSameSectionsRelations(SmUpdateSourceTargetData.Line<CatEMHBranche> brancheLine) {
    //type different
    final CatEMHBranche sourceToImport = brancheLine.getSourceToImport();
    final CatEMHBranche targetToModify = brancheLine.getTargetToModify();
    return isSameSectionsRelations(sourceToImport, targetToModify);
  }

  public boolean isSameSectionsRelations(CatEMHBranche sourceToImport, CatEMHBranche targetToModify) {
    if (!sourceToImport.getBrancheType().equals(targetToModify.getBrancheType())) {
      return false;
    }

    //distance differente
    double distanceSource = sourceToImport.getLength();
    double distanceTarget = targetToModify.getLength();
    final PropertyEpsilon xpEpsilon = ccm.getEpsilon(CrueConfigMetierConstants.PROP_XP);
    if (!xpEpsilon.isSame(distanceSource, distanceTarget)) {
      return false;
    }
    final List<RelationEMHSectionDansBranche> sourceSections = sourceToImport.getSections();
    final List<RelationEMHSectionDansBranche> targetSections = targetToModify.getSections().stream()
      .filter(relation -> !EnumSectionType.EMHSectionInterpolee.equals(
        relation.getEmh().getSectionType())).collect(Collectors.toList());

    //taille différente
    if (sourceSections.size() != targetSections.size()) {
      return false;
    }

    final Map<String, RelationEMHSectionDansBranche> relationByEMhNom = TransformerHelper.toMapOfNom(targetSections);

    return sourceSections.stream().allMatch(source -> {
      RelationEMHSectionDansBranche targetRelation = relationByEMhNom.get(source.getEmhNom());
      return targetRelation != null && xpEpsilon.isSame(targetRelation.getXp(), source.getXp());
    });
  }
}
