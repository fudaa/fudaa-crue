package org.fudaa.dodico.crue.projet.clean;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.projet.EMHProjetController;

import java.io.File;
import java.util.concurrent.Callable;

/**
 * Adapter pour Callable
 *
 * @author deniger
 */
public class EMHProjectCleanerCallableAdapter implements Callable<Boolean> {
  private final EMHProjetController projetController;
  private final File etuFile;
  private final ScenarioCleanData cleanData;
  private final boolean saveProject;
  private CtuluLogGroup logs;

  public EMHProjectCleanerCallableAdapter(EMHProjetController projetController, File etuFile, ScenarioCleanData cleanData, boolean saveProject) {
    this.projetController = projetController;
    this.etuFile = etuFile;
    this.cleanData = cleanData;
    this.saveProject = saveProject;
  }

  public CtuluLogGroup getLogs() {
    return logs;
  }

  @Override
  public Boolean call() throws Exception {
    EMHProjectCleaner cleaner = new EMHProjectCleaner(projetController, etuFile);
    logs = cleaner.cleanProject(cleanData, saveProject);
    return Boolean.TRUE;
  }
}
