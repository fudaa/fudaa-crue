/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

/**
 *
 * @author Frederic Deniger
 */
public interface ReportKeyContract {

  String getVariableName();

  ReportKeyContract duplicateWithNewVar(String newName);

  /**
   *
   * @return si null, cela signifie que les résultats de cette clé porte sur le run courant.
   */
  ReportRunKey getReportRunKey();

  /**
   *
   * @return true si fichier externe
   */
  boolean isExtern();
}
