/*
 GPL 2
 */
package org.fudaa.dodico.crue.edition;

/**
 *
 * @author Frederic Deniger
 */
public class SimplificationSeuils {

  public final double casierSeuil;
  public final double sectionSeuil;

  public SimplificationSeuils(double casierSeuil, double sectionSeuil) {
    this.casierSeuil = casierSeuil;
    this.sectionSeuil = sectionSeuil;
  }
}
