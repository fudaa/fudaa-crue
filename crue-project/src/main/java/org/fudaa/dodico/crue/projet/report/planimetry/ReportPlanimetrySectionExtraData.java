/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.planimetry;

import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;

/**
 *
 * @author Frederic Deniger
 */
public class ReportPlanimetrySectionExtraData extends AbstractReportPlanimetryConfigData {

  public ReportPlanimetrySectionExtraData() {

  }

  protected ReportPlanimetrySectionExtraData(ReportPlanimetrySectionExtraData from) {
    super(from);
  }

  @Override
  public ReportPlanimetrySectionExtraData copy() {
    return new ReportPlanimetrySectionExtraData(this);
  }

  @Override
  protected boolean specificVariableUpdated(FormuleDataChanges changes) {
    return false;

  }
}
