/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * @author deniger
 */
public class OrdonnanceurCrue9 {

  private final static List<CrueFileType> SCENARIO = Collections.unmodifiableList(Arrays.asList(CrueFileType.DC,
      CrueFileType.DH));

  /**
   * @return the sCENARIO
   */
  public List<CrueFileType> getScenario() {
    return SCENARIO;
  }

  /**
   * @return dc,dh
   */
  public static List<String> getAllExtensions() {
    return Arrays.asList("dc", "dh");

  }

}
