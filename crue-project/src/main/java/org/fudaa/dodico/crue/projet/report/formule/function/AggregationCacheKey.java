/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule.function;

import org.fudaa.dodico.crue.projet.report.data.ReportRunEmhKey;

/**
 * Permet d'identifier completement un calcul d'aggrégation  (somme,min,max,moy) pour un run et une variable donnée
 * Utiliser pour le cache entre autre.
 *
 * @author Frederic Deniger
 */
public final class AggregationCacheKey {
    private final ReportRunEmhKey runKey;
    private final EnumFormuleStat statKey;
    private final String var;

    /**
     * @param runKey  la clé du run/emh
     * @param statKey l'opération d'aggrégation
     * @param var     la varible
     */
    public AggregationCacheKey(ReportRunEmhKey runKey, EnumFormuleStat statKey, String var) {
        this.runKey = runKey;
        this.statKey = statKey;
        this.var = var;
    }

    /**
     * @return l'opération d'aggrégation
     */
    public EnumFormuleStat getStatKey() {
        return statKey;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + (this.runKey != null ? this.runKey.hashCode() : 0);
        hash = 23 * hash + (this.statKey != null ? this.statKey.hashCode() : 0);
        hash = 23 * hash + (this.var != null ? this.var.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AggregationCacheKey other = (AggregationCacheKey) obj;
        if (this.runKey != other.runKey && (this.runKey == null || !this.runKey.equals(other.runKey))) {
            return false;
        }
        if (this.statKey != other.statKey) {
            return false;
        }
        if ((this.var == null) ? (other.var != null) : !this.var.equals(other.var)) {
            return false;
        }
        return true;
    }
}
