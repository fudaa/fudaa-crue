/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.migrate;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.ScenarioLoader;
import org.fudaa.dodico.crue.projet.ScenarioLoaderCrue9;
import org.fudaa.dodico.crue.projet.ScenarioLoaderOperation;
import org.fudaa.dodico.crue.projet.ScenarioSaverProcess;
import org.fudaa.dodico.crue.validation.ValidatorORES;

/**
 * @author CANEL Christophe
 */
public class ScenarioCopyProcessor {

  private final EMHProjet initialProject;

  public ScenarioCopyProcessor(EMHProjet initialProject) {
    super();
    this.initialProject = initialProject;
    assert (initialProject != null);
  }

  public void copy(EMHProjet targetProject, CtuluLogGroup log) {
    final List<ManagerEMHScenario> scenarios = targetProject.getListeScenarios();
    for (ManagerEMHScenario targetScenario : scenarios) {
      if (targetScenario.isCrue9()) {
        CtuluLog analyser = log.createLog();
        analyser.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
        analyser.setDesc("migrate.scenarioCopier.copieCrue9");
        analyser.setDescriptionArgs(targetScenario.getNom());
        this.copyScenarioCrue9(targetProject, targetScenario, analyser);
      } else {
        this.copyScenarioCrue10(targetProject, targetScenario, log);
      }
    }
  }

  public boolean copyScenarioCrue9(EMHProjet targetProject, ManagerEMHScenario targetScenario, CtuluLog log) {
    Map<String, File> etudeInitialScenarioFiles = initialProject.getInputFiles(targetScenario);
    Map<String, File> etudeTargetScenarioFiles = targetProject.getInputFiles(targetScenario);

    ManagerEMHScenario initialScenario = initialProject.getScenario(targetScenario.getNom());

    File initialFileDc = ScenarioLoaderCrue9.findFileDc(initialScenario, etudeInitialScenarioFiles);
    File initialFileDh = ScenarioLoaderCrue9.findFileDh(initialScenario, etudeInitialScenarioFiles);

    if (initialFileDc == null || initialFileDh == null || (!initialFileDc.exists()) || (!initialFileDh.exists())) {
      log.addSevereError("migrate.scenarioCopier.dhDcNotExist");

      return false;
    }

    File targetFileDc = ScenarioLoaderCrue9.findFileDc(targetScenario, etudeTargetScenarioFiles);
    File targetFileDh = ScenarioLoaderCrue9.findFileDh(targetScenario, etudeTargetScenarioFiles);

    if (!CtuluLibFile.copyFile(initialFileDc, targetFileDc) || !CtuluLibFile.copyFile(initialFileDh, targetFileDh)) {
      log.addSevereError("migrate.scenarioCopier.dhDcNotCopied");

      return false;
    }

    return true;
  }

  public void copyScenarioCrue10(EMHProjet targetProject, ManagerEMHScenario targetScenario,
                                 CtuluLogGroup log) {

    ManagerEMHScenario initialScenario = initialProject.getScenario(targetScenario.getNom());

    // pour charger le scenario de base:
    ScenarioLoader loader = new ScenarioLoader(initialScenario, initialProject, initialProject.getCoeurConfig());
    ScenarioLoaderOperation load = loader.load(null);
    log.addGroup(load.getLogs());
    if (!load.getLogs().containsFatalError()) {
      EMHScenario scenarioLoaded = load.getResult();
      //on valide ORES afin d'ajouter toutes les DDEs nécessaires.
      new ValidatorORES(targetProject.getCoeurConfig(),new ScenarioAutoModifiedState()).validateScenario(scenarioLoaded);


      // enfin la sauvegarder
      ScenarioSaverProcess saver = new ScenarioSaverProcess(scenarioLoaded, targetScenario, targetProject);
      CtuluLogGroup saveLogs = saver.saveWithScenarioType();
      log.addGroup(saveLogs);
    }
  }
}