/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;

import java.util.Arrays;
import java.util.List;

/**
 * @author deniger
 */
public class EditionSectionCreator {
  final EditionProfilCreator profilCreator;

  public EditionSectionCreator(final EditionProfilCreator profilCreator) {
    this.profilCreator = profilCreator;
  }

  protected UniqueNomFinder getUniqueNomFinder() {
    return profilCreator.getUniqueNomFinder();
  }

  /**
   * @param ccm le {@link CrueConfigMetier}
   * @param brancheName le nom de de la branche
   * @param ssModele le sous-modele
   * @return list avec 2 sections: amont et aval
   */
  protected List<EMHSectionProfil> createEMHSectionProfils(final CrueConfigMetier ccm, final String brancheName, final EMHSousModele ssModele) {
    final EMHScenario scenario = ssModele.getParent().getParent();
    final String sectionName = CruePrefix.getNomAvecPrefixFor(brancheName, EnumCatEMH.SECTION);
    final EMHSectionProfil sectionAmont = createEMHSectionProfilAmont(sectionName, scenario, ccm, ssModele);
    final EMHSectionProfil sectionAval = createEMHSectionProfilAval(sectionName, scenario, ccm, ssModele);
    return Arrays.asList(sectionAmont, sectionAval);
  }

  /**
   * Crée la section a partir du nom de la branche: n'ajoute pas la section a la branhe
   *
   * @param branche la branche
   * @param ccm le {@link CrueConfigMetier}
   */
  public EMHSectionProfil createEMHSectionProfilAval(final CatEMHBranche branche, final CrueConfigMetier ccm) {
    final String sectionName = CruePrefix.getNomAvecPrefixFor(branche.getNom(), EnumCatEMH.SECTION);
    final EMHSousModele sousModele = branche.getParent();
    return createEMHSectionProfilAval(sectionName, sousModele.getParent().getParent(), ccm, sousModele);
  }

  /**
   * Crée la section a partir du nom de la branche: n'ajoute pas la section a la branhe
   *
   * @param branche la banche
   * @param ccm le {@link CrueConfigMetier}
   */
  public EMHSectionSansGeometrie createEMHSectionSansGeometrieAval(final CatEMHBranche branche, final CrueConfigMetier ccm) {
    final String sectionName = CruePrefix.getNomAvecPrefixFor(branche.getNom(), EnumCatEMH.SECTION);
    final EMHSousModele sousModele = branche.getParent();
    return createEMHSectionSansGeometrieAval(sectionName, sousModele.getParent().getParent(), sousModele, ccm);
  }

  /**
   * Crée la section a partir du nom de la branche: n'ajoute pas la section a la branhe
   *
   * @param branche la branche
   * @param ccm le {@link CrueConfigMetier}
   */
  public EMHSectionProfil createEMHSectionProfilAmont(final CatEMHBranche branche, final CrueConfigMetier ccm) {
    final String sectionName = CruePrefix.getNomAvecPrefixFor(branche.getNom(), EnumCatEMH.SECTION);
    final EMHSousModele sousModele = branche.getParent();
    return createEMHSectionProfilAmont(sectionName, sousModele.getParent().getParent(), ccm, sousModele);
  }

  /**
   * Crée la section a partir du nom de la branche: n'ajoute pas la section a la branhe
   *
   * @param branche la branch
   * @param ccm le {@link CrueConfigMetier}
   */
  public EMHSectionSansGeometrie createEMHSectionSansGeometrieAmont(final CatEMHBranche branche, final CrueConfigMetier ccm) {
    final String sectionName = CruePrefix.getNomAvecPrefixFor(branche.getNom(), EnumCatEMH.SECTION);
    final EMHSousModele sousModele = branche.getParent();
    return createEMHSectionSansGeometrieAmont(sectionName, sousModele.getParent().getParent(), sousModele, ccm);
  }

  private EMHSectionProfil createEMHSectionProfilAval(final String sectionName, final EMHScenario scenario, final CrueConfigMetier ccm, final EMHSousModele ssModele) {
    String sectionAvalName = BusinessMessages.getString("create.SectionAval", sectionName);
    sectionAvalName = profilCreator.getUniqueNomFinder().findUniqueName(EnumCatEMH.SECTION, scenario, sectionAvalName);
    return createEMHSectionProfil(sectionAvalName, ccm, ssModele, scenario);
  }

  private EMHSectionProfil createEMHSectionProfilAmont(final String sectionName, final EMHScenario scenario, final CrueConfigMetier ccm, final EMHSousModele ssModele) {
    String sectionAmontName = BusinessMessages.getString("create.SectionAmont", sectionName);
    //on s'assure de l'unicité:
    sectionAmontName = profilCreator.getUniqueNomFinder().findUniqueName(EnumCatEMH.SECTION, scenario, sectionAmontName);
    return createEMHSectionProfil(sectionAmontName, ccm, ssModele, scenario);
  }

  protected List<EMHSectionSansGeometrie> createEMHSectionSansGeometrie(final CrueConfigMetier ccm, final String brancheName,
                                                                        final EMHSousModele ssModele) {
    final EMHScenario scenario = ssModele.getParent().getParent();
    final String sectionName = CruePrefix.getNomAvecPrefixFor(brancheName, EnumCatEMH.SECTION);
    final EMHSectionSansGeometrie sectionAmont = createEMHSectionSansGeometrieAmont(sectionName, scenario, ssModele, ccm);
    final EMHSectionSansGeometrie sectionAval = createEMHSectionSansGeometrieAval(sectionName, scenario, ssModele, ccm);
    return Arrays.asList(sectionAmont, sectionAval);
  }

  private EMHSectionSansGeometrie createEMHSectionSansGeometrieAval(final String sectionName, final EMHScenario scenario, final EMHSousModele ssModele,
                                                                    final CrueConfigMetier ccm) {
    //on s'assure de l'unicité:
    String sectionAvalName = BusinessMessages.getString("create.SectionAval", sectionName);
    sectionAvalName = profilCreator.getUniqueNomFinder().findUniqueName(EnumCatEMH.SECTION, scenario, sectionAvalName);
    return createSectionSansGeometrie(sectionAvalName, ssModele, scenario, ccm);
  }

  private EMHSectionSansGeometrie createEMHSectionSansGeometrieAmont(final String sectionName, final EMHScenario scenario, final EMHSousModele ssModele,
                                                                     final CrueConfigMetier ccm) {
    String sectionAmontName = BusinessMessages.getString("create.SectionAmont", sectionName);
    sectionAmontName = profilCreator.getUniqueNomFinder().findUniqueName(EnumCatEMH.SECTION, scenario, sectionAmontName);
    return createSectionSansGeometrie(sectionAmontName, ssModele, scenario, ccm);
  }

  protected EMHSectionProfil createEMHSectionProfil(final String name, final CrueConfigMetier ccm, final EMHSousModele ssModele, final EMHScenario scenario) {
    final EMHSectionProfil section = createEMHSectionProfilDetached(name, ccm, ssModele, true);
    EditionCreateEMH.addInScenario(ssModele, section, scenario);//toujours faire cela à la fin de la creation pour assure des noms uniques
    return section;
  }

  protected EMHSectionSansGeometrie createSectionSansGeometrie(final String name, final EMHSousModele ssModele, final EMHScenario scenario, final CrueConfigMetier ccm) {
    final EMHSectionSansGeometrie section = createSectionSansGeometrieDetached(name);
    EditionCreateEMH.addInScenario(ssModele, section, scenario);//toujours faire cela à la fin de la creation pour assure des noms uniques
    return section;
  }

  protected EMHSectionInterpolee createSectionInterpolee(final String name, final EMHSousModele ssModele, final EMHScenario scenario, final CrueConfigMetier ccm) {
    final EMHSectionInterpolee section = createSectionInterpoleeDetached(name);
    EditionCreateEMH.addInScenario(ssModele, section, scenario);//toujours faire cela à la fin de la creation pour assure des noms uniques
    return section;
  }

  protected EMHSectionIdem createSectionIdem(final String name, final EMHSousModele ssModele, final EMHScenario scenario, final CatEMHSection sectionRef, final double dz,
                                             final CrueConfigMetier ccm) {
    final EMHSectionIdem section = createSectionIdemDetached(name, ccm, dz, sectionRef);
    EditionCreateEMH.addInScenario(ssModele, section, scenario);//toujours faire cela à la fin de la creation pour assure des noms uniques
    return section;
  }

  /**
   * cree une sectionProfil mais ne l'ajoute pas au sousModele
   *
   * @param ccm le {@link CrueConfigMetier}
   * @param ssModele le {@link EMHSousModele}
   */
  public EMHSectionProfil createEMHSectionProfilDetached(final CrueConfigMetier ccm, final EMHSousModele ssModele) {
    final UniqueNomFinder uniqueNomFinder = profilCreator.getUniqueNomFinder();
    final String newName = uniqueNomFinder.findNewName(EnumCatEMH.SECTION, ssModele.getParent().getParent());
    return createEMHSectionProfilDetached(newName, ccm, ssModele, true);
  }

  /**
   * cree une sectionProfil mais ne l'ajoute pas au sousModele
   *
   * @param name le nom de la section
   * @param ccm le {@link CrueConfigMetier}
   * @param ssModele le {@link EMHSousModele}
   */
  public EMHSectionProfil createEMHSectionProfilDetached(final String name, final CrueConfigMetier ccm, final EMHSousModele ssModele, boolean createProfilSection) {
    final EMHSectionProfil section = new EMHSectionProfil(name);
    if (createProfilSection) {
      final DonPrtGeoProfilSection profil = profilCreator.createProfilSection(ccm, name, ssModele);
      section.addInfosEMH(profil);
    }
    return section;
  }

  public EMHSectionInterpolee createSectionInterpoleeDetached(final String name) {
    return new EMHSectionInterpolee(name);
  }

  public EMHSectionIdem createSectionIdemDetached(final String name, final CrueConfigMetier ccm, final double dz, final CatEMHSection sectionRef) {
    final EMHSectionIdem section = new EMHSectionIdem(name);
    final DonPrtGeoSectionIdem dptg = new DonPrtGeoSectionIdem(ccm);
    dptg.setDz(dz);
    section.addInfosEMH(dptg);
    EMHRelationFactory.addSectionRef(section, sectionRef);
    return section;
  }

  public EMHSectionSansGeometrie createSectionSansGeometrieDetached(final String name) {
    return new EMHSectionSansGeometrie(name);
  }
}
