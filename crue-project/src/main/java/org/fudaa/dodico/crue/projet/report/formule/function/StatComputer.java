/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule.function;

import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;

/**
 *
 * @author Frederic Deniger
 */
public abstract class StatComputer {

  public abstract double getFinalValue();

  public abstract void addValue(ResultatTimeKey time, double val);

  public static StatComputer createFrom(EnumFormuleStat state) {
    switch (state.getFormuleSelection()) {
      case MIN:
        return new StatComputer.Min();
      case MAX:
        return new StatComputer.Max();
      case MOY:
        return new StatComputer.Moy();
      case SUM:
        return new StatComputer.Somme();
    }
    return null;
  }

  public static class Somme extends StatComputer {

    private double value;

    @Override
    public double getFinalValue() {
      return value;
    }

    @Override
    public void addValue(ResultatTimeKey time, double val) {
      value = val + value;
    }
  }

  public static class Min extends StatComputer {

    private double min;
    private boolean first = true;

    @Override
    public double getFinalValue() {
      return min;
    }

    @Override
    public void addValue(ResultatTimeKey time, double val) {
      if (first) {
        first = false;
        min = val;
      } else {
        min = Math.min(val, min);
      }
    }
  }

  public static class Max extends StatComputer {

    private double max;
    private boolean first = true;

    @Override
    public double getFinalValue() {
      return max;
    }

    @Override
    public void addValue(ResultatTimeKey time, double val) {
      if (first) {
        first = false;
        max = val;
      } else {
        max = Math.max(val, max);
      }
    }
  }

  public static class Moy extends StatComputer {

    private double total;
    private int nb;

    @Override
    public double getFinalValue() {
      return total / (double) nb;
    }

    @Override
    public void addValue(ResultatTimeKey time, double val) {
      nb++;
      total = total + val;
    }
  }
}
