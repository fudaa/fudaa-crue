/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.longitudinal;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLongitudinalPositionSectionByRun {

  private final ReportRunKey runKey;
  private final List<ReportLongitudinalPositionSection> sectionDisplayPositions = new ArrayList<>();

  public ReportLongitudinalPositionSectionByRun(ReportRunKey runKey) {
    this.runKey = runKey;
  }

  public ReportRunKey getRunKey() {
    return runKey;
  }

  public List<ReportLongitudinalPositionSection> getSectionDisplayPositions() {
    return sectionDisplayPositions;
  }

  public void addSection(double xpDisplay, CatEMHSection name) {
    sectionDisplayPositions.add(new ReportLongitudinalPositionSection(xpDisplay, name));
  }
}
