/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

import org.fudaa.ebli.courbe.EGCourbePersist;

/**
 *
 * @author Frederic Deniger
 */
public class ReportRunVariableConfig {
  
  ReportRunVariableKey reportRunVariableKey;
  EGCourbePersist courbeConfig;

  public ReportRunVariableKey getReportRunVariableKey() {
    return reportRunVariableKey;
  }

  public void setReportRunVariableKey(ReportRunVariableKey reportRunVariableKey) {
    this.reportRunVariableKey = reportRunVariableKey;
  }

  public EGCourbePersist getCourbeConfig() {
    return courbeConfig;
  }

  public void setCourbeConfig(EGCourbePersist courbeConfig) {
    this.courbeConfig = courbeConfig;
  }
  
  
          
  
}
