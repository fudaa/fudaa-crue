/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.calcul;

import org.fudaa.ctulu.CtuluUI;

/**
 * @author deniger
 */
public interface CalculCrueContrat extends Runnable {
    /**
     * @param ctuluUI
     */
    void setUI(CtuluUI ctuluUI);

    /**
     * demande d'arret du calcul.
     */
    void stop();

    /**
     * @return true si le fichier de stop est écrit
     */
    boolean isStopFileWritten();

    /**
     * dans le cas de crue 10, des options sont necessaires pour le lancement des services. Or dans le cas d'un run rapide
     * il se peut qu'aucun service ne soit lancé. Cette méthode détecte ce cas
     * @return true si le calcul sera lancé sans options et donc conduira à une erreur
     */
    boolean hasNoOptions();

    /**
     * @return true si le calcul a été executé normalement ( pas d'"abnomarl exit").
     */
    boolean isComputeFinishedCorrectly();
}
