package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.EMHNoeudFactory;
import org.fudaa.dodico.crue.projet.OrdonnanceurCrue10;

import java.io.File;
import java.util.List;

public class SmUpdateSourceDrsoLoader {
  private final CoeurConfigContrat coeur;

  public SmUpdateSourceDrsoLoader(CoeurConfigContrat coeurConfigContrat) {
    this.coeur = coeurConfigContrat;
  }

  public CrueOperationResult<EMHSousModele> load(File drsoFile) {
    CtuluLogGroup ctuluLogGroup = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    ctuluLogGroup.setDescription("sm.updater.LoadLog");
    ctuluLogGroup.setDescriptionArgs(drsoFile.getName());

    final EMHNoeudFactory nodeFactory = new EMHNoeudFactory();
    final Crue10FileFormatFactory factory = Crue10FileFormatFactory.getInstance();
    final CrueDataImpl crueData = new CrueDataImpl(nodeFactory, coeur.getCrueConfigMetier());

    final List<CrueFileType> sousModeleFileTypes = new OrdonnanceurCrue10().getSousModele();
    String radical = SmImportValidator.getSousModeleRadical(drsoFile, CrueFileType.DRSO);
    final File parentFile = drsoFile.getParentFile();
    CtuluLog mainLog = ctuluLogGroup.createNewLog("loader.readFile");
    for (CrueFileType crueFileType : sousModeleFileTypes) {
      final Crue10FileFormat fmt = factory.getFileFormat(crueFileType, coeur);
      if (fmt == null) {
        mainLog.addSevereError("filetype.unknown.error", crueFileType);
      } else {
        File file = new File(parentFile, radical + "." + crueFileType.getExtension());
        if (file.exists()) {
          fmt.read(file, ctuluLogGroup.createLog(), crueData);
        } else {
          mainLog.addSevereError("io.FileNotFoundException.error", file.getAbsolutePath());
        }
      }
    }
    crueData.getSousModele().setNom(CruePrefix.P_SS_MODELE+radical);
    return new CrueOperationResult<>(crueData.getSousModele(), ctuluLogGroup);
  }
}
