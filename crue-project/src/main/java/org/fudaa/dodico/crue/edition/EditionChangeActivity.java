/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import java.util.Collection;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.validation.ValidateInitialConditions;

/**
 *
 * @author Frédéric Deniger
 */
public class EditionChangeActivity {

  public CtuluLog propagateActivityChanged(final EMHModeleBase modele, final EMH emh, final CrueConfigMetier ccm) {
    final ValidateInitialConditions changeCini = new ValidateInitialConditions();
    final CtuluLog res = changeCini.propagateChange(modele, ccm);
    return res;

  }

  public CtuluLog propagateActivityChanged(final EMHModeleBase modele, final Collection<? extends EMH> emhs, final CrueConfigMetier ccm) {
    final ValidateInitialConditions changeCini = new ValidateInitialConditions();
    final CtuluLog res = changeCini.propagateChange(modele, ccm);
    return res;

  }
}
