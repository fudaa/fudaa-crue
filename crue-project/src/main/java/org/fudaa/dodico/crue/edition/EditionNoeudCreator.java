/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;

/**
 * @author deniger
 */
public class EditionNoeudCreator {
  private final UniqueNomFinder uniqueNomFinder;

  public EditionNoeudCreator(final UniqueNomFinder uniqueNomFinder) {
    this.uniqueNomFinder = uniqueNomFinder;
  }

  public CatEMHNoeud createNoeud(final EMHSousModele sousModele, final EnumNoeudType noeudType, final CrueConfigMetier ccm) {
    final EMHScenario scenario = sousModele.getParent().getParent();
    final String newName = this.uniqueNomFinder.findNewName(EnumCatEMH.NOEUD, scenario);
    return createNoeud(newName, noeudType, sousModele, scenario, ccm);
  }

  public CatEMHNoeud createNoeud(final String newName, final EnumNoeudType noeudType, final EMHSousModele sousModele, final CrueConfigMetier ccm) {
    return createNoeud(newName, noeudType, sousModele, sousModele.getParent().getParent(), ccm);
  }

  public CatEMHNoeud createNoeud(final String newName, final EnumNoeudType noeudType, final EMHSousModele sousModele, final EMHScenario scenario, final CrueConfigMetier ccm) {
    final CatEMHNoeud nd = createNoeud(newName, noeudType);
    EditionCreateEMH.addInScenario(sousModele, nd, scenario);
    return nd;
  }

  private CatEMHNoeud createNoeud(final String name, final EnumNoeudType noeudType) {
    switch (noeudType) {
      case EMHNoeudNiveauContinu:
        return new EMHNoeudNiveauContinu(name);
    }
    throw new IllegalAccessError("noeudType not supported" + noeudType);
  }
}
