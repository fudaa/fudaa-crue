/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleParametersExpr extends FormuleParameters {

  @XStreamAlias("Expression")
  private String formule;
  @XStreamAlias("Nature")
  private String nature;

  @Override
  public String getDescription() {
    return formule;
  }

  @Override
  public FormuleParameters clone() {
    FormuleParametersExpr cloned = (FormuleParametersExpr) super.clone();
    return cloned;
  }

  @Override
  public boolean isContentEqual(FormuleParameters other) {
    if (other instanceof FormuleParametersExpr) {
      FormuleParametersExpr otherExp = (FormuleParametersExpr) other;
      return StringUtils.equals(formule, otherExp.formule) && StringUtils.equals(nature, otherExp.nature);
    } else {
      throw new IllegalAccessError("cant compare different parameters");
    }
  }

  @Override
  public FormuleContent createContent(ReportGlobalServiceContrat service) {
    return new FormuleContentExpr(service);
  }

  public String getNature() {
    return nature;
  }

  public void setNature(String nature) {
    this.nature = nature;
  }

  public String getFormule() {
    return formule;
  }

  public void setFormule(String formule) {
    this.formule = formule;
  }
}
