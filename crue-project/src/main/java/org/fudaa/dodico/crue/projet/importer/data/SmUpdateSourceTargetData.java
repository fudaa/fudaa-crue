package org.fudaa.dodico.crue.projet.importer.data;

import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SmUpdateSourceTargetData {
  private final List<Line<CatEMHNoeud>> noeuds = new ArrayList<>();
  private final List<Line<CatEMHCasier>> casiers = new ArrayList<>();
  private final List<Line<CatEMHSection>> sections = new ArrayList<>();
  private final List<Line<CatEMHBranche>> branches = new ArrayList<>();

  public SmUpdateSourceTargetData() {
  }

  public SmUpdateSourceTargetData(List<Line<CatEMHNoeud>> noeuds, List<Line<CatEMHCasier>> casiers, List<Line<CatEMHSection>> sections, List<Line<CatEMHBranche>> branches) {
    if (noeuds != null) {
      this.noeuds.addAll(noeuds);
    }
    if (casiers != null) {
      this.casiers.addAll(casiers);
    }
    if (sections != null) {
      this.sections.addAll(sections);
    }
    if (branches != null) {
      this.branches.addAll(branches);
    }
  }

  public void sort() {
    Collections.sort(casiers);
    Collections.sort(branches);
    Collections.sort(noeuds);
    Collections.sort(sections);
  }

  public List<EMH> getSourceEMHs() {
    List<EMH> res = new ArrayList<>();
    sections.forEach(line -> res.add(line.getSourceToImport()));
    noeuds.forEach(line -> res.add(line.getSourceToImport()));
    casiers.forEach(line -> res.add(line.getSourceToImport()));
    branches.forEach(line -> res.add(line.getSourceToImport()));
    res.sort(ObjetNommeByNameComparator.INSTANCE);
    return res;
  }

  public List<EMH> getTargetEMHs() {
    List<EMH> res = new ArrayList<>();
    sections.forEach(line -> res.add(line.getTargetToModify()));
    noeuds.forEach(line -> res.add(line.getTargetToModify()));
    casiers.forEach(line -> res.add(line.getTargetToModify()));
    branches.forEach(line -> res.add(line.getTargetToModify()));
    return res;
  }

  public void addCasierLine(Line<CatEMHCasier> in) {
    casiers.add(in);
  }

  public void addBrancheLine(Line<CatEMHBranche> in) {
    branches.add(in);
  }

  public void addSectionLine(Line<CatEMHSection> in) {
    sections.add(in);
  }

  public void addNoeudLine(Line<CatEMHNoeud> in) {
    noeuds.add(in);
  }

  public List<Line<CatEMHBranche>> getBranches() {
    return branches;
  }

  public List<Line<CatEMHCasier>> getCasiers() {
    return casiers;
  }

  public List<Line<CatEMHNoeud>> getNoeuds() {
    return noeuds;
  }

  public List<Line<CatEMHSection>> getSections() {
    return sections;
  }

  public void add(EMH source, EMH target) {
    switch (source.getCatType()) {
      case CASIER:
        casiers.add(new Line<>((CatEMHCasier) source, (CatEMHCasier) target));
        return;
      case NOEUD:
        noeuds.add(new Line<>((CatEMHNoeud) source, (CatEMHNoeud) target));
        return;
      case SECTION:
        sections.add(new Line<>((CatEMHSection) source, (CatEMHSection) target));
        return;
      case BRANCHE:
        branches.add(new Line<>((CatEMHBranche) source, (CatEMHBranche) target));
        return;
    }
  }

  public static class Line<T extends EMH> implements Comparable<Line<T>> {
    private final T sourceToImport;
    private final T targetToModify;

    public Line(T source, T data) {
      this.sourceToImport = source;
      this.targetToModify = data;
    }

    public T getSourceToImport() {
      return sourceToImport;
    }

    public T getTargetToModify() {
      return targetToModify;
    }

    @Override
    public int compareTo(Line o) {
      return ObjetNommeByNameComparator.INSTANCE.compare(sourceToImport, o.sourceToImport);
    }
  }
}
