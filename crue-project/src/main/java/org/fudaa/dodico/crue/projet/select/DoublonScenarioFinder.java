/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.select;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Permet de selectionner les scenarios qui sont en doublons: s'il existe un scenario nommé Sc_scenarioc10 et un Sc_scenario, le premier est un doublon.
 */
public class DoublonScenarioFinder {
  private EMHProjet project;
  private final String suffixe = CrueVersionType.CRUE10.getSuffix();

  public DoublonScenarioFinder(EMHProjet project) {
    this.project = project;
  }

  public DoublonScenarioFinder() {
  }

  public EMHProjet getProject() {
    return project;
  }

  public String getSuffixe() {
    return suffixe;
  }


  public List<ManagerEMHScenario> getDoublons() {
    final Set<String> scenarioNames = project.getListeScenarios().stream().map(ManagerEMHScenario::getNom).collect(Collectors.toSet());
    return project.getListeScenarios().stream().filter(managerEMHScenario -> {
      if (StringUtils.endsWith(managerEMHScenario.getNom(), suffixe)) {
        String name = StringUtils.removeEnd(managerEMHScenario.getNom(), suffixe);
        return scenarioNames.contains(name);
      }
      return false;
    }).collect(Collectors.toList());
  }

  public void setProject(EMHProjet project) {
    this.project = project;
  }
}
