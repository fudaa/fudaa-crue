/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.transformer;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;

/**
 *
 * @author Frederic Deniger
 */
public class ReportRunKeySimpleToStringTransformer extends AbstractPropertyToStringTransformer<ReportRunKey> {

  public static final String CURRENT_ID = "CURRENT";
  private final ReportRunKey currentKey;

  public ReportRunKeySimpleToStringTransformer(ReportRunKey courantKey) {
    super(ReportRunKey.class);
    this.currentKey = courantKey;
  }

  @Override
  public String toStringSafe(ReportRunKey in) {
    if (in.isCourant()) {
      return CURRENT_ID;
    }
    return in.getManagerNom() + KeysToStringConverter.MINOR_SEPARATOR + in.getRunAlternatifId();
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }

  @Override
  public ReportRunKey fromStringSafe(String in) {
    if (CURRENT_ID.equals(in)) {
      return currentKey;
    }
    String[] split = StringUtils.split(in, KeysToStringConverter.MINOR_SEPARATOR);
    if (split.length == 2) {
      return new ReportRunKey(split[0], split[1]);
    }
    return ReportRunKey.NULL_VALUE;
  }
}
