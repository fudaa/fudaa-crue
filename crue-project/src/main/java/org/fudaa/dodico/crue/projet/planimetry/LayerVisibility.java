package org.fudaa.dodico.crue.projet.planimetry;

/**
 *
 * @author deniger
 */
public class LayerVisibility {

  boolean sectionVisible = true;
  boolean traceVisible = true;
  boolean nodeVisible = true;
  boolean brancheVisible = true;
  boolean casierVisible = true;
  boolean condLimitVisible = true;
  boolean grHydraulicVisible = true;
  boolean grOtherObjectVisible = true;
  boolean grDrawVisible = true;
  boolean grSIGFileVisible = true;
  boolean grImagesVisible = true;
  boolean traceCasiersVisible = true;
  boolean traceProfilsVisible = true;

  public boolean isTraceCasiersVisible() {
    return traceCasiersVisible;
  }

  public void setTraceCasiersVisible(boolean traceCasiersVisible) {
    this.traceCasiersVisible = traceCasiersVisible;
  }

  public boolean isTraceProfilsVisible() {
    return traceProfilsVisible;
  }

  public void setTraceProfilsVisible(boolean traceProfilsVisible) {
    this.traceProfilsVisible = traceProfilsVisible;
  }

  public boolean isGrHydraulicVisible() {
    return grHydraulicVisible;
  }

  public void setGrHydraulicVisible(boolean grHydraulicVisible) {
    this.grHydraulicVisible = grHydraulicVisible;
  }

  public boolean isGrOtherObjectVisible() {
    return grOtherObjectVisible;
  }

  public void setGrOtherObjectVisible(boolean grOtherObjectVisible) {
    this.grOtherObjectVisible = grOtherObjectVisible;
  }

  public boolean isGrDrawVisible() {
    return grDrawVisible;
  }

  public void setGrDrawVisible(boolean grDrawVisible) {
    this.grDrawVisible = grDrawVisible;
  }

  public boolean isGrSIGFileVisible() {
    return grSIGFileVisible;
  }

  public void setGrSIGFileVisible(boolean grSIGFileVisible) {
    this.grSIGFileVisible = grSIGFileVisible;
  }

  public boolean isGrImagesVisible() {
    return grImagesVisible;
  }

  public void setGrImagesVisible(boolean grImagesVisible) {
    this.grImagesVisible = grImagesVisible;
  }

  public boolean isBrancheVisible() {
    return brancheVisible;
  }

  public boolean isCondLimitVisible() {
    return condLimitVisible;
  }

  public void setCondLimitVisible(boolean condLimitVisible) {
    this.condLimitVisible = condLimitVisible;
  }

  public void setBrancheVisible(boolean brancheVisible) {
    this.brancheVisible = brancheVisible;
  }

  public boolean isCasierVisible() {
    return casierVisible;
  }

  public void setCasierVisible(boolean casierVisible) {
    this.casierVisible = casierVisible;
  }

  public boolean isNodeVisible() {
    return nodeVisible;
  }

  public void setNodeVisible(boolean nodeVisible) {
    this.nodeVisible = nodeVisible;
  }

  public boolean isSectionVisible() {
    return sectionVisible;
  }

  public void setSectionVisible(boolean sectionVisible) {
    this.sectionVisible = sectionVisible;
  }

  public boolean isTraceVisible() {
    return traceVisible;
  }

  public void setTraceVisible(boolean traceVisible) {
    this.traceVisible = traceVisible;
  }
}
