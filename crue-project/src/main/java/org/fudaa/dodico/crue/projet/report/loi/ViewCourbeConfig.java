/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.loi;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.SwingConstants;

/**
 * @author Frederic Deniger
 */
public class ViewCourbeConfig {

  @XStreamAlias("Legende-Visible")
  boolean displayLegend = true;
  @XStreamAlias("Legende-Position")
  int legendPosition = SwingConstants.NORTH;
  @XStreamAlias("Title-Visible")
  boolean displayTitle = true;
  @XStreamAlias("Title-Position")
  int titlePosition = SwingConstants.SOUTH;
  @XStreamAlias("Title-Config")
  LabelConfig titleConfig = LabelConfig.createCenteredLabel();
  @XStreamAlias("Labels-Visible")
  boolean displayLabels = true;
  @XStreamAlias("Labels-Position")
  int labelsPosition = SwingConstants.SOUTH;
  @XStreamImplicit(itemFieldName = "Label-Config")
  List<LabelConfig> labels = new ArrayList<>();

  public ViewCourbeConfig() {
  }

  public void reinit() {
    if (labels == null) {
      labels = new ArrayList<>();
    }
  }

  public static boolean isEast(int position) {
    return position == SwingConstants.NORTH_EAST || position == SwingConstants.SOUTH_EAST || position == SwingConstants.EAST;
  }

  public void setLabels(List<LabelConfig> labels) {
    this.labels.clear();
    this.labels.addAll(labels);
  }

  public List<LabelConfig> getLabels() {
    return Collections.unmodifiableList(labels);
  }

  public void clearLabelTxtTooltip() {
    if (titleConfig != null && titleConfig.getKey() != null) {
      titleConfig.setText(null);
      titleConfig.setTooltipText(null);
    }
    for (LabelConfig labelConfig : labels) {
      if (labelConfig.getKey() != null) {
        labelConfig.setText(null);
        labelConfig.setTooltipText(null);
      }
    }
  }

  public static boolean isNorth(int position) {
    return position == SwingConstants.NORTH || position == SwingConstants.NORTH_EAST || position == SwingConstants.NORTH_WEST;
  }

  public int getLegendPosition() {
    return legendPosition;
  }

  public void setLegendPosition(int legendPosition) {
    this.legendPosition = legendPosition;
  }

  public boolean isDisplayLegend() {
    return displayLegend;
  }

  public boolean isTitlePositionTop() {
    return titlePosition == SwingConstants.NORTH;
  }

  public void setTitlePositionTop(boolean top) {
    titlePosition = top ? SwingConstants.NORTH : SwingConstants.SOUTH;
  }

  public void setDisplayLegend(boolean displayLegend) {
    this.displayLegend = displayLegend;
  }

  public int getLabelsPosition() {
    return labelsPosition;
  }

  public void setLabelsPosition(int labelsPosition) {
    this.labelsPosition = labelsPosition;
  }

  public boolean isDisplayLabels() {
    return displayLabels;
  }

  public void setDisplayLabels(boolean displayLabels) {
    this.displayLabels = displayLabels;
  }

  public int getTitlePosition() {
    return titlePosition;
  }

  public void setTitlePosition(int titlePosition) {
    this.titlePosition = titlePosition;
  }

  public boolean isDisplayTitle() {
    return displayTitle;
  }

  public void setDisplayTitle(boolean displayTitle) {
    this.displayTitle = displayTitle;
  }

  public LabelConfig getTitleConfig() {
    return titleConfig;
  }

  public void setTitleConfig(LabelConfig titleConfig) {
    this.titleConfig = titleConfig;
  }

  public ViewCourbeConfig dpulicate() {
    ViewCourbeConfig copy = new ViewCourbeConfig();
    copy.titlePosition = titlePosition;
    copy.displayLegend = displayLegend;
    copy.legendPosition = legendPosition;
    copy.displayTitle = displayTitle;
    if (titleConfig != null) {
      copy.titleConfig = titleConfig.duplicate();
    }
    copy.displayLabels = displayLabels;
    copy.labelsPosition = labelsPosition;
    if (labels != null)
      labels.forEach(l -> copy.labels.add(l.duplicate()));
    return copy;
  }
}
