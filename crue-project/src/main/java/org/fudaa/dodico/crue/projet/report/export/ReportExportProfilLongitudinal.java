/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.export;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.config.lit.LitNommeLimite;
import org.fudaa.dodico.crue.config.lit.ReportLongitudinalLimitHelper;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.EMHSectionProfil;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.metier.helper.ReportProfilHelper;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.excel.ReportExcelFormatData;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBrancheCartouche;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBrancheConfig;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalConfig;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalPositionBrancheContent;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalPositionBuilder;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalPositionSection;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalPositionSectionByRun;

/**
 *
 * @author Frederic Deniger
 */
public class ReportExportProfilLongitudinal extends ReportExportAbstract<ReportLongitudinalConfig, ReportRunVariableKey> {
  
  public ReportExportProfilLongitudinal(ReportLongitudinalConfig config, ReportResultProviderServiceContrat resultService) {
    super(config, resultService);
  }
  
  private static class RowCreator {
    
    int rowIdx = 0;
    int colOffset = 0;
    int colIdx = 0;
    int maxCol = 0;
    final Sheet currentSheet;
    Row currentRow;
    
    public RowCreator(Sheet currentSheet) {
      this.currentSheet = currentSheet;
    }
    
    public void getRowAndIncrement() {
      currentRow = currentSheet.getRow(rowIdx);
      if (currentRow == null) {
        currentRow = currentSheet.createRow(rowIdx);
      }
      colIdx = colOffset;
      rowIdx++;
    }
    
    public Cell createNextCell() {
      Cell res = currentRow.createCell(colIdx++);
      maxCol = Math.max(maxCol, colIdx);
      return res;
    }
    
    public void nextBlock() {
      rowIdx = 0;
      colOffset = maxCol;
    }
    
  }
  
  @Override
  protected void writeInBook(Workbook wb, List<ReportRunVariableKey> variablesToExportInit, List<ResultatTimeKey> times) {
    
    String sheetName = CtuluResource.CTULU.getString("Feuille {0}", CtuluLibString.getString(1));
    ReportExcelFormatData formater = new ReportExcelFormatData(wb);
    Sheet currentSheet = wb.createSheet(sheetName);
    //la config
    ReportLongitudinalConfig content = super.config;
    final CrueConfigMetier ccm = resultService.getCcm();
    ReportLongitudinalLimitHelper limitHelper = new ReportLongitudinalLimitHelper(ccm);
    final Map<String, String> displayNameById = limitHelper.getDisplayNameById();
    final ItemVariable propertyXp = ccm.getProperty(CrueConfigMetierConstants.PROP_XP);
    Set<ReportRunKey> runs = new LinkedHashSet<>();
    for (ReportRunVariableKey reportRunVariableKey : variablesToExportInit) {
      runs.add(reportRunVariableKey.getReportRunKey());
    }
    RowCreator rowCreator = new RowCreator(currentSheet);
    for (ReportRunKey runKey : runs) {
      //ecriture des titres
      rowCreator.getRowAndIncrement();
      String runName = runKey.getDisplayName();
      rowCreator.createNextCell().setCellValue(BusinessMessages.getString("prefix.branches") + BusinessMessages.ENTITY_SEPARATOR + runName);
      rowCreator.createNextCell().setCellValue(BusinessMessages.getString("prefix.sections") + BusinessMessages.ENTITY_SEPARATOR + runName);
      rowCreator.createNextCell().setCellValue(propertyXp.getDisplayNom() + BusinessMessages.ENTITY_SEPARATOR + runName);
      final List<ReportRunVariableKey> profilVariables = content.getProfilVariables();
      //les titres des limites de profils
      for (ReportRunVariableKey variable : profilVariables) {
        if (ReportVariableTypeEnum.LIMIT_PROFIL.equals(variable.getVariable().getVariableType())) {
          String displayName = displayNameById.get(variable.getVariableName());
          if (StringUtils.isEmpty(displayName)) {
            displayName = variable.getVariableName();
          }
          rowCreator.createNextCell().setCellValue(displayName + BusinessMessages.ENTITY_SEPARATOR + runName);
        }
      }
      
      List<ReportRunVariableKey> variablesToExport = new ArrayList<>();
      List<ReportRunVariableKey> variablesRPIToExport = new ArrayList<>();

      //pour les valeur RPTI, il ne faut exporter qu'une seul fois par runKey.
      for (ReportRunVariableKey variable : variablesToExportInit) {
        if (variable.getRunKey().equals(runKey)) {
          if (variable.isRPTIVariable()) {
            variablesRPIToExport.add(variable);
          } else {
            variablesToExport.add(variable);
          }
        }
      }
      for (ReportRunVariableKey variable : variablesRPIToExport) {
        rowCreator.createNextCell().setCellValue(variable.getVariable().getVariableDisplayName() + BusinessMessages.ENTITY_SEPARATOR + runName);
      }
      for (ResultatTimeKey time : times) {
        for (ReportRunVariableKey variable : variablesToExport) {
          rowCreator.createNextCell().setCellValue(
                  variable.getVariable().getVariableDisplayName() + BusinessMessages.ENTITY_SEPARATOR + ResultatTimeKey.timeToString(time) + BusinessMessages.ENTITY_SEPARATOR + runName);
        }
      }
      final List<ReportLongitudinalBrancheConfig> branchesConfig = content.getBranchesConfig();
      ReportLongitudinalPositionBuilder positionBuilder = new ReportLongitudinalPositionBuilder(this.resultService, ccm);
      Set<ReportRunKey> keys = Collections.singleton(runKey);

      //construction des cartouches
      double xpInit = 0;
      ReportLongitudinalPositionBrancheContent result = new ReportLongitudinalPositionBrancheContent();
      for (ReportLongitudinalBrancheConfig brancheConfig : branchesConfig) {
        xpInit = xpInit + brancheConfig.getDecalXp();
        positionBuilder.buildPositions(xpInit, brancheConfig, result, keys);
        xpInit = xpInit + brancheConfig.getLength();
      }

      //ecritures des valeurs
      final List<ReportLongitudinalBrancheCartouche> cartouches = result.getCartouches();
      //les styles commun
      final CellStyle xpStyle = formater.getStyle(propertyXp.getNature(), DecimalFormatEpsilonEnum.COMPARISON);
      final PropertyNature natureForZ = resultService.getCcm().getProperty(CrueConfigMetierConstants.PROP_Z).getNature();
      final CellStyle zStyle = formater.
              getStyle(natureForZ, DecimalFormatEpsilonEnum.COMPARISON);
      for (ReportLongitudinalBrancheCartouche cartouche : cartouches) {
        final ReportLongitudinalPositionSectionByRun sectionPositions = cartouche.getSectionPositions(runKey);
        final List<ReportLongitudinalPositionSection> sectionDisplayPositions = sectionPositions.getSectionDisplayPositions();
        for (ReportLongitudinalPositionSection reportLongitudinalPositionSection : sectionDisplayPositions) {
          rowCreator.getRowAndIncrement();
          final CatEMHSection section = reportLongitudinalPositionSection.getName();
          rowCreator.createNextCell().setCellValue(section.getBranche().getNom());
          rowCreator.createNextCell().setCellValue(section.getNom());
          Cell cell = rowCreator.createNextCell();
          cell.setCellValue(reportLongitudinalPositionSection.getXpDisplay());
          cell.setCellStyle(xpStyle);
          for (ReportRunVariableKey variable : profilVariables) {
            if (ReportVariableTypeEnum.LIMIT_PROFIL.equals(variable.getVariable().getVariableType())) {
              ItemEnum etiquette = limitHelper.getEtiquette(variable.getVariable().getVariableName());
              LitNommeLimite limite = limitHelper.getLimite(variable.getVariable().getVariableName());
              cell = rowCreator.createNextCell();
              if (etiquette != null || limite != null) {
                EMHSectionProfil retrieveProfil = ReportProfilHelper.retrieveProfil(section);
                if (retrieveProfil != null) {
                  DonPrtGeoProfilSection profil = DonPrtHelper.getProfilSection(retrieveProfil);
                  Double z = ReportExportUtils.computeZ(etiquette, limite, profil, ccm);
                  if (z != null) {
                    cell.setCellValue(z.doubleValue());
                    cell.setCellStyle(zStyle);
                    
                  }
                }
              }
            }
          }
          
          for (ReportRunVariableKey variableInit : variablesRPIToExport) {
            ReportRunVariableKey variable = new ReportRunVariableKey(runKey, variableInit.getVariable());
            Double value = resultService.getValue(variable, section.getNom(), getSelectedTimeStepInReport());
            cell = rowCreator.createNextCell();
            if (value != null) {
              cell.setCellValue(value.doubleValue());
              final CellStyle style = formater.getStyle(resultService.getPropertyNature(variable.getVariableName()),
                      DecimalFormatEpsilonEnum.COMPARISON);
              cell.setCellStyle(style);
              
            }
          }
          for (ResultatTimeKey time : times) {
            for (ReportRunVariableKey variableInit : variablesToExport) {
              ReportRunVariableKey variable = new ReportRunVariableKey(runKey, variableInit.getVariable());
              Double value = resultService.getValue(time, variable, section.getNom(), getSelectedTimeStepInReport());
              cell = rowCreator.createNextCell();
              if (value != null) {
                cell.setCellValue(value.doubleValue());
                final CellStyle style = formater.getStyle(resultService.getPropertyNature(variable.getVariableName()),
                        DecimalFormatEpsilonEnum.COMPARISON);
                cell.setCellStyle(style);
                
              }
            }
          }
        }
      }
      rowCreator.nextBlock();
    }
  }
  
}
