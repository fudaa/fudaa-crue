package org.fudaa.dodico.crue.projet.importer.data;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

import java.util.*;

/**
 * Contient les résultats de validation de la mise à jour d'un sous-modele.
 * si le sous-modele cible n'existe pas, des EMH cibles sont dans un autre sous-modele ou des sections requises ne sont pas présentes -> erreur
 */
public class SmUpdateValidationData {
  private final List<String> requiredSectionsNotInTarget = new ArrayList<>();
  private final Map<String, String> targetEmhInAnotherSousModele = new TreeMap<>();
  private final Map<String, String> targetDonFrtInAnotherSousModele = new TreeMap<>();
  private final boolean targetSousModeleNotExisting;
  private final String targetSousModele;

  public SmUpdateValidationData(final String targetSousModele) {
    this(false, targetSousModele);
  }

  public SmUpdateValidationData(boolean targetSousModeleNotExisting, final String targetSousModele) {
    this.targetSousModeleNotExisting = targetSousModeleNotExisting;
    this.targetSousModele = targetSousModele;
  }

  /**
   * @return le nom du sous-modele cible
   */
  public String getTargetSousModele() {
    return targetSousModele;
  }

  /**
   * @return true si la cible n'est pas trouvée
   */
  public boolean isTargetSousModeleNotExisting() {
    return targetSousModeleNotExisting;
  }

  /**
   * @return true si la cible est trouvée
   */
  public boolean isTargetSousModeleExisting() {
    return !isTargetSousModeleNotExisting();
  }


  /**
   * @return les sections qui devraient être dans la cible. Si non présentes -> erreur
   */
  public List<String> getRequiredSectionsNotInTarget() {
    return Collections.unmodifiableList(requiredSectionsNotInTarget);
  }

  /**
   * @return emh-> sous-modele. Toutes les emhs qui existent dans un autre sous-modele
   */
  public Map<String, String> getTargetEmhInAnotherSousModele() {
    return Collections.unmodifiableMap(targetEmhInAnotherSousModele);
  }

  public Map<String, String> getTargetDonFrtInAnotherSousModele() {
    return Collections.unmodifiableMap(targetDonFrtInAnotherSousModele);
  }

  public void addRequiredNotInTarget(String emhNameInTarget) {
    requiredSectionsNotInTarget.add(emhNameInTarget);
  }

  public void addDonFrtInAnotherSousModele(EMH targetSimpleEMH) {
    final EMHSousModele parent = EMHHelper.getParent(targetSimpleEMH);
    if (parent != null) {
      targetEmhInAnotherSousModele.put(targetSimpleEMH.getNom(), parent.getNom());
    } else {
      targetEmhInAnotherSousModele.put(targetSimpleEMH.getNom(), StringUtils.EMPTY);
    }
  }

  public void addDonFrtInAnotherSousModele(String dontFrt, String emhSousModeleName) {
    targetDonFrtInAnotherSousModele.put(dontFrt, emhSousModeleName);
  }
}
