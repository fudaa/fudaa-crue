/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition.bean;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author deniger
 */
public class ListRelationSectionContent extends AbstractListContent implements Comparable<ListRelationSectionContent> {
  public static final String PROP_BRANCHE_NOM = "brancheNom";
  public static final String PROP_PROFIL_SECTION = "profilSection";
  public static final String PROP_SECTION_TYPE_NOM = "sectionType";
  public static final String PROP_REFERENCE_SECTION_IDEM = "referenceSectionIdem";
  public static final String PROP_DZ = "dz";
  public static final String PROP_ABSCISSE_HYDRAULIQUE = "abscisseHydraulique";
  public static final String PROP_ABSCISSE_SCHEMATIQUE = "abscisseSchematique";
  public static final List<String> SECTION_DATA_PROPERTIES = Collections.unmodifiableList(Arrays.asList(PROP_PROFIL_SECTION,
      PROP_SECTION_TYPE_NOM,
      PROP_REFERENCE_SECTION_IDEM,
      PROP_DZ,
      ListCommonProperties.PROP_COMMENTAIRE));
  private String brancheNom;
  private double abscisseHydraulique;
  private String abscisseSchematique = StringUtils.EMPTY;
  private SectionData section = new SectionData();
  private Map additionalDataRelation;

  @Override
  public int compareTo(ListRelationSectionContent o) {
    if (o == null) {
      return 1;
    }
    if (o == this) {
      return 0;
    }
    if (abscisseHydraulique > o.abscisseHydraulique) {
      return 1;
    }
    return -1;
  }

  public static class SectionData {
    String profilSection = StringUtils.EMPTY;
    String commentaire = StringUtils.EMPTY;
    EnumSectionType sectionType;
    String referenceSectionIdem = StringUtils.EMPTY;
    double dz;

    public String getProfilSection() {
      return profilSection;
    }

    public void setProfilSection(String profilSection) {
      this.profilSection = profilSection;
    }

    public double getDz() {
      return dz;
    }

    public void setDz(double dz) {
      this.dz = dz;
    }

    public String getReferenceSectionIdem() {
      return referenceSectionIdem;
    }

    public void setReferenceSectionIdem(String referenceSectionIdem) {
      this.referenceSectionIdem = referenceSectionIdem;
    }

    public EnumSectionType getSectionType() {
      return sectionType;
    }

    public void setSectionType(EnumSectionType sectionType) {
      this.sectionType = sectionType;
    }

    public String getCommentaire() {
      return commentaire;
    }

    public void setCommentaire(String commentaire) {
      this.commentaire = commentaire;
    }
  }

  public ListRelationSectionContent() {
  }

  public ListRelationSectionContent(String nom) {
    this.nom = nom;
  }

  public double getAbscisseHydraulique() {
    return abscisseHydraulique;
  }

  public void setAbscisseHydraulique(double abscisseHydraulique) {
    this.abscisseHydraulique = abscisseHydraulique;
  }

  public String getAbscisseSchematique() {
    return abscisseSchematique;
  }

  public Map getAdditionalDataRelation() {
    return additionalDataRelation;
  }

  public void setAdditionalDataRelation(Map additionalDataRelation) {
    this.additionalDataRelation = additionalDataRelation;
  }

  public void setAbscisseSchematique(String abscisseSchematique) {
    this.abscisseSchematique = abscisseSchematique;
  }

  public String getBrancheNom() {
    return brancheNom;
  }

  public void setBrancheNom(String brancheNom) {
    this.brancheNom = brancheNom;
  }

  public SectionData getSection() {
    return section;
  }

  public void setSection(SectionData section) {
    this.section = section;
  }
}
