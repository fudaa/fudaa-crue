/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import java.util.Collection;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

/**
 * @author deniger
 */
public class EditionChangeUtils {

  private static void removeCasier(final CatEMHNoeud noeud) {
    final CatEMHCasier casier = noeud.getCasier();
    if (casier != null) {
      assert casier.getNoeud() == noeud;
      EMHRelationFactory.removeRelation(noeud, casier);
      EMHRelationFactory.removeRelation(casier, noeud);
      assert casier.getNoeud() == null;
      assert noeud.getCasier() == null;
    }
  }

  public static void setNoeud(final CatEMHCasier casier, final CatEMHNoeud noeud) {
    final CatEMHNoeud oldNoeud = casier.getNoeud();
    if (oldNoeud != null) {
      removeCasier(oldNoeud);
    }
    EMHRelationFactory.addNoeudCasier(casier, noeud);
  }
  
  public static void toggleStatus(Collection<? extends EMH> toToggle, boolean status) {
    List<CatEMHBranche> branchesToToggle = EMHHelper.selectEMHS(toToggle, EnumCatEMH.BRANCHE);
    if (CollectionUtils.isNotEmpty(branchesToToggle)) {
      // Activation ou désactivation des branches
      for (CatEMHBranche branche : branchesToToggle) {
        branche.setUserActive(status);
      }
    }

    List<CatEMHCasier> casiersToToggle = EMHHelper.selectEMHS(toToggle, EnumCatEMH.CASIER);
    if (CollectionUtils.isNotEmpty(casiersToToggle)) {
      // Activation ou désactivation des casiers
      for (CatEMHCasier casier : casiersToToggle) {
        casier.setUserActive(status);
      }
    }
  }

}
