/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition.bean;

/**
 *
 * @author deniger
 */
public class ListCommonProperties {

  public static final String PROP_ORDRE = "ordre";
  public static final String PROP_NOM = "nom";
  public static final String PROP_COMMENTAIRE = "commentaire";
  public static final String PROP_TYPE = "type";
  public static final String PROP_ACTIVE = "active";
  
}
