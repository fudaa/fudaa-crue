/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

/**
 *
 * @author Frederic Deniger
 */
public enum ReportRunEnum {

  CURRENT, ALTERNATIF
}
