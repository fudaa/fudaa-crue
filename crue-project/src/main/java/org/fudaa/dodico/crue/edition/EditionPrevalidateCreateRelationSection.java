/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DoubleEpsilonComparator;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.edition.bean.AbstractListContent;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent;
import org.fudaa.dodico.crue.edition.bean.ValidationMessage;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.dodico.crue.validation.util.DelegateValidatorSection;
import org.fudaa.dodico.crue.validation.util.ValidationContentSectionDansBrancheExecutor;

/**
 *
 * @author Frédéric Deniger
 */
public class EditionPrevalidateCreateRelationSection {

  public static Map<String, List<String>> getFinalSectionNameByBranche(final Map<String, List<ListRelationSectionContent>> newXpByBrancheName, final EMHSousModele sousModele, final CrueConfigMetier ccm) {
    final Map<String, List<String>> res = new HashMap<>();
    final Map<String, CatEMHBranche> branchesByNoms = TransformerHelper.toMapOfNom(sousModele.getBranches());
    final PropertyEpsilon epsilon = ccm.getEpsilon(CrueConfigMetierConstants.PROP_XP);
    final DoubleEpsilonComparator comparator = new DoubleEpsilonComparator(epsilon);
    for (final Map.Entry<String, List<ListRelationSectionContent>> entry : newXpByBrancheName.entrySet()) {
      final TreeMap<Double, String> maps = new TreeMap<>(comparator);
      final List<ListRelationSectionContent> values = entry.getValue();
      for (final ListRelationSectionContent listRelationSectionContent : values) {
        maps.put(listRelationSectionContent.getAbscisseHydraulique(), listRelationSectionContent.getNom());
      }
      final String brancheName = entry.getKey();
      final CatEMHBranche branche = branchesByNoms.get(brancheName);
      if (branche != null) {
        final List<RelationEMHSectionDansBranche> sections = branche.getSections();
        for (final RelationEMHSectionDansBranche relationEMHSectionDansBranche : sections) {
          maps.put(relationEMHSectionDansBranche.getXp(), relationEMHSectionDansBranche.getEmhNom());
        }
      }
      res.put(entry.getKey(), new ArrayList<>(maps.values()));
    }
    return res;
  }

  DuplicateXpCreateSectionContainer findDuplicateXp(final Collection<ListRelationSectionContent> newSections, final EMHSousModele sousModele, final CrueConfigMetier ccm) {
    final DuplicateXpCreateSectionContainer res = new DuplicateXpCreateSectionContainer();
    final Map<String, EMHBrancheSaintVenant> branchesSaintVenantByNoms = TransformerHelper.toMapOfNom(sousModele.getBranchesSaintVenant());
    final Map<String, List<ListRelationSectionContent>> newXpByBrancheName = EditionPrevalidateModifyRelationSection.getNewContentByBrancheName(newSections);
    final PropertyEpsilon epsilon = ccm.getEpsilon(CrueConfigMetierConstants.PROP_XP);
    final DoubleEpsilonComparator comparator = new DoubleEpsilonComparator(epsilon);
    res.sectionsNameByBranche = getFinalSectionNameByBranche(newXpByBrancheName, sousModele, ccm);
    for (final Map.Entry<String, List<ListRelationSectionContent>> entry : newXpByBrancheName.entrySet()) {
      final String brancheName = entry.getKey();
      final EMHBrancheSaintVenant branche = branchesSaintVenantByNoms.get(brancheName);
      if (branche == null) {
        continue;//cette erreur est détecté après
      }
      final TreeSet<Double> existingXp = new TreeSet<>(comparator);
      final List<RelationEMHSectionDansBranche> sections = branche.getSections();
      for (final RelationEMHSectionDansBranche relation : sections) {
        existingXp.add(relation.getXp());
      }
      final TreeSet<Double> newXp = new TreeSet<>(comparator);
      final List<ListRelationSectionContent> list = entry.getValue();
      for (final ListRelationSectionContent listRelationSectionContent : list) {
        final Double xp = Double.valueOf(listRelationSectionContent.getAbscisseHydraulique());
        if (existingXp.contains(xp)) {
          //duplique un xp existant.
          res.duplicateExistingRelation.add(listRelationSectionContent);
        }
        if (newXp.contains(xp)) {
          res.duplicateNewRelation.add(listRelationSectionContent);
        }
        newXp.add(xp);
      }
    }
    return res;

  }

  ValidationMessage<ListRelationSectionContent> validate(final Collection<ListRelationSectionContent> contents, final EMHSousModele modele, final CrueConfigMetier ccm) {
    final ValidationMessage<ListRelationSectionContent> res = new ValidationMessage<>();
    final CtuluLog log = res.log;
    final Set<String> allIdsUsedInScenarios = TransformerHelper.toSetOfId(modele.getParent().getParent().getIdRegistry().getEMHs(EnumCatEMH.SECTION));
    final Map<String, EMHBrancheSaintVenant> branchesByNoms = TransformerHelper.toMapOfNom(modele.getBranchesSaintVenant());
    final Map<String, CatEMHSection> sectionsByNoms = TransformerHelper.toMapOfNom(modele.getSections());
    final ItemVariable xpProperty = ccm.getProperty(CrueConfigMetierConstants.PROP_XP);
    final ItemVariable dzProperty = ccm.getProperty(CrueConfigMetierConstants.PROP_DZ);
    final DuplicateXpCreateSectionContainer findDuplicateXp = findDuplicateXp(contents, modele, ccm);
    final Collection<EnumSectionType> authorizedSectionInterneForSaintVenant = ValidationContentSectionDansBrancheExecutor.createSectionInterneTypeDansBrancheContents().
            get(EnumBrancheType.EMHBrancheSaintVenant);
    final Collection<EnumSectionType> authorizedSectionAmontAvalForSaintVenant = ValidationContentSectionDansBrancheExecutor.createSectionTypeDansBrancheContents(true).
            get(EnumBrancheType.EMHBrancheSaintVenant);
    for (final ListRelationSectionContent content : contents) {
      final String nom = content.getNom();
      final List<CtuluLogRecord> msgs = new ArrayList<>();
      //on valide le nom de la section:
      if (!ValidationPatternHelper.isNameValide(nom, EnumCatEMH.SECTION)) {
        msgs.add(log.addSevereError("AddEMH.NameNotValid", nom));
      }
      if (AbstractListContent.nameUsed(content, contents)) {
        msgs.add(log.addSevereError("AddEMH.NameAlreadyUsedInNewEMH", nom));
      }
      if (allIdsUsedInScenarios.contains(nom.toUpperCase())) {
        msgs.add(log.addSevereError("AddEMH.NameAlreadyUsedByExistingEMH", nom));
      }

      //le type de la section:
      final EnumSectionType sectionType = content.getSection().getSectionType();
      if (sectionType == null) {
        msgs.add(log.addSevereError("AddEMH.SectionTypeUnknown", nom));
      }

      //pour section idem, il faut vérifier dz et la référence
      if (EnumSectionType.EMHSectionIdem.equals(sectionType)) {
        //dz
        final double dz = content.getSection().getDz();
        final Pair<String, Object[]> valueError = dzProperty.getValidator().getValidateErrorParamForValue(dz);
        if (valueError != null) {
          msgs.add(log.addSevereError(valueError.first, valueError.second));
        }
        //la référence
        final String referenceSectionIdem = content.getSection().getReferenceSectionIdem();
        final CatEMHSection referencedSection = sectionsByNoms.get(referenceSectionIdem);
        if (referencedSection == null) {
          msgs.add(log.addSevereError("AddEMH.SectionReferencedUnknown", nom, referenceSectionIdem));
        } else {
          final List<EnumSectionType> authorizedSectionType = DelegateValidatorSection.getAuthorizedSectionRefType();
          if (!authorizedSectionType.contains(referencedSection.getSectionType())) {
            msgs.add(log.addSevereError("AddEMH.SectionReferencedInvalid", nom, referencedSection.getSectionType().geti18n()));
            //on teste l'activité
          } else if (!referencedSection.getActuallyActive()) {
            final String brancheNom = content.getBrancheNom();
            if (StringUtils.isNotBlank(brancheNom)) {
              final EMHBrancheSaintVenant branche = branchesByNoms.get(brancheNom);
              if (branche != null && branche.getActuallyActive()) {//la section idem est active mais la reference non...
                msgs.add(log.addSevereError("validation.sectionRefNotActive", nom, referenceSectionIdem));
              }
            }

          }
        }


      }
      //la branche
      if (StringUtils.isNotBlank(content.getBrancheNom())) {
        if (!branchesByNoms.containsKey(content.getBrancheNom())) {
          msgs.add(log.addSevereError("AddEMH.BrancheSaintVenantUnknown", nom, content.getBrancheNom()));
        }
        if (sectionType != null && !authorizedSectionInterneForSaintVenant.contains(sectionType)) {
          msgs.add(log.addSevereError("AddEMH.BrancheSaintVenantNotSupportThisSection", nom, content.getBrancheNom(), sectionType.geti18n()));
        }
        if (sectionType != null) {
          final List<String> sectionNames = findDuplicateXp.sectionsNameByBranche.get(content.getBrancheNom());
          if (CollectionUtils.isNotEmpty(sectionNames)) {
            final boolean avalOrAmont = nom.equals(sectionNames.get(0)) || nom.equals(sectionNames.get(sectionNames.size() - 1));
            if (avalOrAmont && !authorizedSectionAmontAvalForSaintVenant.contains(sectionType)) {
              msgs.add(log.addSevereError("AddEMH.BrancheSaintVenantNotSupportThisSectionAvalAmont", nom, content.getBrancheNom(), sectionType.geti18n()));
            }
          }
        }
        final double abscisseHydraulique = content.getAbscisseHydraulique();
        final Pair<String, Object[]> valueError = xpProperty.getValidator().getValidateErrorParamForValue(abscisseHydraulique);
        if (valueError != null) {
          msgs.add(log.addSevereError(valueError.first, valueError.second));
        }
        //il faut aussi tester que le xp n'est pas en doublons....
        if (findDuplicateXp.duplicateExistingRelation.contains(content)) {
          msgs.add(log.addSevereError("AddEMH.XpAlreadyUsedInExistingRelation", nom));
        }
        if (findDuplicateXp.duplicateNewRelation.contains(content)) {
          msgs.add(log.addSevereError("AddEMH.XpAlreadyUsedInNewRelation", nom));
        }

      }
      if (!msgs.isEmpty()) {
        res.accepted = false;
        final ValidationMessage.ResultItem<ListRelationSectionContent> item = new ValidationMessage.ResultItem<>();
        item.bean = content;
        for (final CtuluLogRecord ctuluLogRecord : msgs) {
          BusinessMessages.updateLocalizedMessage(ctuluLogRecord);
          item.messages.add(ctuluLogRecord.getLocalizedMessage());
        }
        res.messages.add(item);
      }

    }
    return res;

  }

  public static class DuplicateXpCreateSectionContainer {

    /**
     * Pour chaque branche cont
     */
    Map<String, List<String>> sectionsNameByBranche = new HashMap<>();
    /**
     * Contient les contenus qui ont un xp qui duplique une relation existante
     */
    final List<ListRelationSectionContent> duplicateExistingRelation = new ArrayList<>();
    /**
     * Contient les contenus qui ont un xp qui duplique une relation en creation.
     */
    final List<ListRelationSectionContent> duplicateNewRelation = new ArrayList<>();
  }
}
