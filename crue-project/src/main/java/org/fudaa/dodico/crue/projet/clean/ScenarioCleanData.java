package org.fudaa.dodico.crue.projet.clean;

import org.fudaa.dodico.crue.projet.ProjectNormalizableCallable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Contient les données a nettoyer/normaliser
 *
 * @author deniger
 */
public class ScenarioCleanData {
  public final List<ProjectNormalizableCallable.Result> scenarioToNormalizeOrWithErrors = new ArrayList<>();
  /**
   * Doit contenir les scenario en double ( c10...)
   */
  public final List<String> scenarioNamesToDelete = new ArrayList<>();

  public final Map<String, ProjectNormalizableCallable.Result> getResultByScenarioName() {
    return scenarioToNormalizeOrWithErrors.stream().collect(Collectors.toMap(ProjectNormalizableCallable.Result::getScenarioName, Function.identity()));
  }

  public boolean isNotEmpty() {
    return !scenarioNamesToDelete.isEmpty() || !scenarioToNormalizeOrWithErrors.isEmpty();
  }
}
