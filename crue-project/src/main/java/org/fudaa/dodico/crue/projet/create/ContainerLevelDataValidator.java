/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.create;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;

/**
 * @author CANEL Christophe
 *
 */
public class ContainerLevelDataValidator {
  private final EMHProjet projet;

  public ContainerLevelDataValidator(final EMHProjet projet) {
    super();
    this.projet = projet;
  }

  public CtuluLog valid(final ContainerLevelData data, final boolean addWarn)
  {
    
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("create.levelDataValidator.desc");
    //si pas de warn demande on ne fait rien
    if(!addWarn){
      return log;
    }
    
    final Set<String> files = this.getAllProjectFiles();

    List<ContainerLevelData> datas = data.getLevel(CrueLevelType.SCENARIO);
    
    for (final ContainerLevelData scenario : datas)
    {
      if (this.projet.getScenario(scenario.getName()) != null) {
        log.addWarn("create.levelDataValidator.scenarioNameExists", scenario.getName());
        this.validFiles(files, scenario.getFiles(), log);
      }
    }
    
    datas = data.getLevel(CrueLevelType.MODELE);

    for (final ContainerLevelData modele : datas)
    {
      if (this.projet.getModele(modele.getName()) != null) {
        log.addWarn("create.levelDataValidator.modeleNameExists", modele.getName());
        this.validFiles(files, modele.getFiles(), log);
      }
    }

    datas = data.getLevel(CrueLevelType.SOUS_MODELE);

    for (final ContainerLevelData sousModele : datas)
    {
      if (this.projet.getSousModele(sousModele.getName()) != null) {
        log.addWarn("create.levelDataValidator.sousModeleNameExists", sousModele.getName());
        this.validFiles(files, sousModele.getFiles(), log);
      }
    }

    return log;
  }
  
  private Set<String> getAllProjectFiles()
  {
    final List<FichierCrue> fichiersProjets = this.projet.getInfos().getBaseFichiersProjets();
    final Set<String> fichiers = new HashSet<>(fichiersProjets.size());
    
    for (final FichierCrue fichier : fichiersProjets)
    {
      fichiers.add(fichier.getNom().toUpperCase());
    }
    
    return fichiers;
  }
  
  private void validFiles(final Set<String> projectFiles, final List<FichierCrue> filesToCreated, final CtuluLog log)
  {
    for (final FichierCrue file : filesToCreated)
    {
      final String nom = file.getNom();
      
      if (projectFiles.contains(nom.toUpperCase()))
      {
        log.addWarn("create.levelDataValidator.fichierExists", nom);
      }
    }
  }
}
