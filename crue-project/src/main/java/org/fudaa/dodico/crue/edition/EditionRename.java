/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;

import java.util.*;

/**
 * @author deniger
 */
public class EditionRename {
    public static void propagateSectionRename(CatEMHSection section, String newName) {
        if (section.getSectionType().equals(EnumSectionType.EMHSectionProfil)) {
            EMHSectionProfil sectionProfil = (EMHSectionProfil) section;
            DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection(sectionProfil);
            profilSection.setNom(CruePrefix.changePrefix(newName, CruePrefix.P_SECTION, CruePrefix.P_PROFIL_SECTION));
        }
    }

    /**
     * @param emhToRename l'emh a renommer
     * @param newName     le nouveau nom
     * @return l'erreur si le nom est invalide ou déjà utilisé.
     */
    public String rename(EMH emhToRename, String newName) {
        String error = isNameValid(emhToRename, newName);
        if (error != null) {
            return error;
        }
        emhToRename.setNom(newName);
        propagateName(emhToRename);
        return null;
    }

    public String rename(CatEMHBranche emh, String newName, boolean renameSection) {
        //si section renommée, on vérifie d'abord qu'on peut le faire:
        Map<EMH, String> map = new HashMap<>();
        if (renameSection) {
            String oldSectionPrefix = CruePrefix.changePrefix(emh.getNom(), EnumCatEMH.SECTION);
            String newSectionPref = CruePrefix.changePrefix(newName, EnumCatEMH.SECTION);
            final List<RelationEMHSectionDansBranche> sections = emh.getSections();
            for (RelationEMHSectionDansBranche relationEMHSectionDansBranche : sections) {
                final CatEMHSection section = relationEMHSectionDansBranche.getEmh();
                if (section.getNom().startsWith(oldSectionPrefix)) {
                    String newSectionName = newSectionPref + section.getNom().substring(oldSectionPrefix.length());
                    String newSectioNameValid = isNameValid(section, newSectionName);
                    if (newSectioNameValid != null) {
                        return newSectioNameValid;
                    }
                    map.put(section, newSectionName);
                }
            }
        }
        String error = rename(emh, newName);
        if (error != null) {
            return error;
        }
        if (renameSection) {
            for (Map.Entry<EMH, String> entry : map.entrySet()) {
                rename(entry.getKey(), entry.getValue());
            }
        }
        return null;
    }

    private void propagateCasierRenamed(CatEMHCasier casier, String newCasierName) {
        Collection<DonPrtGeoProfilCasier> profilsCasier = EMHHelper.selectClass(casier.getInfosEMH(), DonPrtGeoProfilCasier.class);
        if (profilsCasier != null) {
            for (DonPrtGeoProfilCasier profilCasier : profilsCasier) {
                String suffix = StringUtils.substringAfterLast(profilCasier.getNom(), "_");
                profilCasier.setNom(CruePrefix.changePrefix(newCasierName, CruePrefix.P_CASIER, CruePrefix.P_PROFIL_CASIER) + "_" + suffix);
            }
        }
        DonPrtGeoBatiCasier batiCasier = EMHHelper.selectFirstOfClass(casier.getInfosEMH(), DonPrtGeoBatiCasier.class);
        if (batiCasier != null) {
            batiCasier.setNom(CruePrefix.changePrefix(newCasierName, CruePrefix.P_CASIER, CruePrefix.P_BATI_CASIER));
        }
    }

    /**
     * @param emh           emh a teser
     * @param newName       le nouveau nom
     * @param renameSection true si les sections doivent être renommées.
     * @return une chaine non vide si erreur
     */
    public String isNameValid(CatEMHBranche emh, String newName, boolean renameSection) {
        final String nameValid = isNameValid(emh, newName);
        if (nameValid != null) {
            return nameValid;
        }
        if (renameSection) {
            String oldSectionPrefix = CruePrefix.changePrefix(emh.getNom(), EnumCatEMH.SECTION);
            String newSectionPref = CruePrefix.changePrefix(newName, EnumCatEMH.SECTION);
            final List<RelationEMHSectionDansBranche> sections = emh.getSections();
            for (RelationEMHSectionDansBranche relationEMHSectionDansBranche : sections) {
                final CatEMHSection section = relationEMHSectionDansBranche.getEmh();
                if (section.getNom().startsWith(oldSectionPrefix)) {
                    String newSectionName = newSectionPref + section.getNom().substring(oldSectionPrefix.length());
                    String newSectioNameValid = isNameValid(section, newSectionName);
                    if (newSectioNameValid != null) {
                        return newSectioNameValid;
                    }
                }
            }
        }
        return null;
    }

    public String isNameValid(EMH emh, String newName) {
        String nameValidei18nMessage = ValidationPatternHelper.isNameValidei18nMessage(newName, emh.getCatType());
        if (nameValidei18nMessage != null) {
            return nameValidei18nMessage;
        }
        EMHScenario scenario = EMHHelper.getScenario(emh);
        List<EMH> emhs = scenario.getIdRegistry().getEMHs(emh.getCatType());
        emhs.remove(emh);
        Set<String> toSetOfId = TransformerHelper.toSetOfId(emhs);
        if (toSetOfId.contains(newName.toUpperCase())) {
            return BusinessMessages.getString("validation.name.alreadyUsed.error", newName);
        }
        return null;
    }

    public void propagateName(EMH emh) {
        final EnumCatEMH catType = emh.getCatType();
        if (EnumCatEMH.NOEUD.equals(catType)) {
            CatEMHNoeud nd = (CatEMHNoeud) emh;
            if (nd.getCasier() != null) {
                final String newCasierName = CruePrefix.changePrefix(emh.getNom(), EnumCatEMH.CASIER);
                nd.getCasier().setNom(newCasierName);
                propagateCasierRenamed(nd.getCasier(), newCasierName);
            }
        } else if (EnumCatEMH.CASIER.equals(catType)) {
            CatEMHCasier casier = (CatEMHCasier) emh;
            casier.getNoeud().setNom(CruePrefix.changePrefix(emh.getNom(), EnumCatEMH.NOEUD));
            propagateCasierRenamed(casier, emh.getNom());
        } else if (EnumCatEMH.SECTION.equals(catType)) {
            CatEMHSection section = (CatEMHSection) emh;
            propagateSectionRename(section, emh.getNom());
        } else if (EnumCatEMH.BRANCHE.equals(catType)) {
            List<DonCalcSansPrt> dcsP = emh.getDCSP();
            List<Loi> lois = new ArrayList<>();
            for (DonCalcSansPrt donCalcSansPrt : dcsP) {
                donCalcSansPrt.fillWithLoi(lois);
            }
            if (!lois.isEmpty()) {
                CatEMHBranche branche = (CatEMHBranche) emh;
                EditionLoiCreator loiCreator = new EditionLoiCreator(new UniqueNomFinder());
                EMHScenario scenario = branche.getParent().getParent().getParent();
                for (Loi loi : lois) {
                    loi.setNom(loiCreator.findLoiName(scenario, loi.getType(), emh.getNom()));
                }
            }
        }
    }
}
