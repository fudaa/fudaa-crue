/*
  License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.Pair;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author deniger
 */
public class EditionDelete {
  public static void removeBranche(CatEMHBranche branche) {
    EMHRelationFactory.removeRelationBidirect(branche.getParent(), branche);
    EMHRelationFactory.removeRelationBidirect(branche.getNoeudAmont(), branche);
    EMHRelationFactory.removeRelationBidirect(branche.getNoeudAval(), branche);
    List<RelationEMHSectionDansBranche> sections = branche.getSections();
    for (RelationEMHSectionDansBranche section : sections) {
      EMHRelationFactory.removeRelationBidirect(section.getEmh(), branche);
    }
    CatEMHSection sectionPilote = EMHHelper.getSectionPilote(branche);
    if (sectionPilote != null) {
      EMHRelationFactory.removeRelationBidirect(branche, sectionPilote);
    }
  }

  /**
   * pas de tests pour l'instant mais il faut vérifier qu'une section n'est pas en position amont/aval dans une branche que ce n'est pas la référence
   * d'une section idem.
   *
   * @param section la section
   * @param forceDelete true si forceDelete
   * @return true si la section est complètement supprimée
   */
  private static boolean detachOrRemoveSection(CatEMHSection section, boolean forceDelete) {
    //si c'est une section idem on enleve les relations avec la reference:
    if (section.getSectionType().equals(EnumSectionType.EMHSectionIdem)) {
      RelationEMHSectionDansSectionIdem relationSectionRef = EMHHelper.getRelationSectionRef((EMHSectionIdem) section);
      if (relationSectionRef != null) {
        EMHRelationFactory.removeRelationBidirect(relationSectionRef.getEmh(), section);
      }
    }
    //on enleve les relations avec la branche

    CatEMHBranche branche = section.getBranche();
    boolean completelyDeleted = false;
    if (forceDelete || branche == null) {
      //on l'enleve complètement
      EMHRelationFactory.removeRelationBidirect(section.getParent(), section);
      if (section.getSectionType().equals(EnumSectionType.EMHSectionProfil)) {
        deleteProfil((EMHSectionProfil) section);
      }
      completelyDeleted = true;
    }
    if (branche != null) {
      EMHRelationFactory.removeRelationBidirect(branche, section);
    }
    return completelyDeleted;
  }

  /**
   * n'enleve pas les profil casiers !
   *
   * @param casier le casier a enlever
   */
  private static void removeCasier(CatEMHCasier casier) {
    CatEMHNoeud noeud = casier.getNoeud();
    EMHRelationFactory.removeRelationBidirect(noeud, casier);
    EMHRelationFactory.removeRelationBidirect(casier.getParent(), casier);
  }

  /**
   * Les frottements gardent em memoire les lits numerotes les utilisés. Lors de la suppression, on doit les enlever explicitement.
   *
   * @param sectionProfil la sectionProfil a enlever
   */
  private static void deleteProfil(EMHSectionProfil sectionProfil) {
    DonPrtGeoProfilSection profilSection = EMHHelper.selectFirstOfClass(sectionProfil.getInfosEMH(), DonPrtGeoProfilSection.class);
    sectionProfil.removeInfosEMH(profilSection);
    List<LitNumerote> litNumerotes = profilSection.getLitNumerote();
    for (LitNumerote litNumerote : litNumerotes) {
      //le frottement ne sera plus utilisée.
      litNumerote.setFrot(null);
    }
  }

  /**
   * @param sousModele le sous-modele parent
   * @return les sections interpolees qui n'ont pas de branche associée.
   */
  public static List<CatEMHSection> findSectionInterpoleeToDelete(EMHSousModele sousModele) {
    if (sousModele == null) {
      return Collections.emptyList();
    }
    return sousModele.getSections().stream()
      .filter(catEMHSection -> catEMHSection.getBranche() == null && catEMHSection.getSectionType().equals(EnumSectionType.EMHSectionInterpolee))
      .collect(
        Collectors.toList());
  }

  /**
   * @param scenario le sous-modele parent
   * @return les sections interpolees qui n'ont pas de branche associée.
   */
  public static List<CatEMHSection> findSectionInterpoleeToDelete(EMHScenario scenario) {
    if (scenario == null) {
      return Collections.emptyList();
    }
    return scenario.getSections().stream()
      .filter(catEMHSection -> catEMHSection.getBranche() == null && catEMHSection.getSectionType().equals(EnumSectionType.EMHSectionInterpolee))
      .collect(
        Collectors.toList());
  }

  /**
   * si on supprime une branche, en mode cascade les sections sont enlevées les noeuds seront supprimés si non utilisés par ailleurs.
   * casier: si le noeud n'est pas utilisé, il sera supprimé
   * noeud: le casier sera supprimé.
   * Section profil: rien.
   * section casier : les profil seront supprimés.
   *
   * @param toDelete les EMHs a supprimer
   * @param cascade si true, tous éléments attenant sont enlevés
   */
  public static RemoveBilan delete(Collection<? extends EMH> toDelete, EMHScenario scenario, boolean cascade) {
    RemoveBilan res = new RemoveBilan();
    List<CatEMHBranche> branchesToDelete = EMHHelper.selectEMHS(toDelete, EnumCatEMH.BRANCHE);
    Set<CatEMHSection> sectionToRemoveCascade = new HashSet<>();
    Set<CatEMHNoeud> noeudToRemoveCascade = new HashSet<>();
    if (CollectionUtils.isNotEmpty(branchesToDelete)) {
      //on les enleves
      for (CatEMHBranche branche : branchesToDelete) {
        List<RelationEMHSectionDansBranche> sections = branche.getSections();
        if (cascade) {
          final List<CatEMHSection> brancheSections = EMHHelper.collectEMHInRelations(sections);
          sectionToRemoveCascade.addAll(brancheSections);
          noeudToRemoveCascade.add(branche.getNoeudAmont());
          noeudToRemoveCascade.add(branche.getNoeudAval());
        }
        removeBranche(branche);
        removeEMH(scenario, branche);
        res.nbBrancheRemoved++;
      }
    }
    detachOrRemoveSections(scenario, toDelete, sectionToRemoveCascade, res, cascade);
    deleteCasiersAndNoeuds(toDelete, cascade, noeudToRemoveCascade, scenario, res);

    return res;
  }

  private static void deleteCasiersAndNoeuds(Collection<? extends EMH> toDelete, boolean cascade,
                                             Set<CatEMHNoeud> noeudToRemoveCascade, EMHScenario scenario, RemoveBilan res) {
    List<CatEMHCasier> casiersToDelete = EMHHelper.selectEMHS(toDelete, EnumCatEMH.CASIER);
    //si cascade, ces listes seront utilisées pour enlever les profils
//    Set<DonPrtGeoProfilCasier> profilToDeleteCascade = new HashSet<>();
//    Set<DonPrtGeoBatiCasier> batiToDeleteCascade = new HashSet<>();
    //on enleve les casiers
    for (CatEMHCasier casier : casiersToDelete) {
      if (cascade) {
        noeudToRemoveCascade.add(casier.getNoeud());
      }
      removeCasier(casier);
      removeEMH(scenario, casier);
      res.nbCasierRemoved++;
    }
    //on enleve les noeuds
    Set<CatEMHNoeud> allNoeudToDelete = new HashSet<>();
    allNoeudToDelete.addAll(EMHHelper.selectEMHS(toDelete, EnumCatEMH.NOEUD));
    allNoeudToDelete.addAll(noeudToRemoveCascade);
    for (CatEMHNoeud noeud : allNoeudToDelete) {
      if (!noeud.getBranches().isEmpty()) {
        res.noeudNotRemovedBranche.add(noeud.getNom());
        removeNoeudFromSousModeleIfNotUsed(noeud,res);
      } else if (!cascade && noeud.getCasier() != null) {
        removeNoeudFromSousModeleIfNotUsed(noeud,res);
        res.noeudNotRemovedCasier.add(noeud.getNom());
      } else {
        //si casier et on est ici: forcement cascade
        if (noeud.getCasier() != null) {
          assert cascade;
          final CatEMHCasier casier = noeud.getCasier();
          removeCasier(casier);
          removeEMH(scenario, casier);
          res.nbCasierRemoved++;
        }
        removeNoeudFromSousModele(noeud);
        removeEMH(scenario, noeud);
        res.nbNoeudRemoved++;
      }
    }
  }

  private static void removeNoeudFromSousModeleIfNotUsed(CatEMHNoeud noeud,RemoveBilan removeBilan) {
    final List<EMHSousModele> noeudParent = noeud.getParents();
    final Set<String> ssModeleUsingNoeud = TransformerHelper.toSetNom(EMHHelper.getParents(noeud.getBranches()));
    if (noeud.getCasier() != null) {
      ssModeleUsingNoeud.add(noeud.getCasier().getParent().getNom());
    }
    noeudParent.stream().filter(ssModele -> !ssModeleUsingNoeud.contains(ssModele.getNom())).forEach(ssModele -> {
      EMHRelationFactory.removeRelationBidirect(ssModele, noeud);
      removeBilan.noeudRemovedSousModele.add(new Pair<>(noeud.getNom(),ssModele.getNom()));

    });
  }

  private static void removeNoeudFromSousModele(CatEMHNoeud noeud) {
    List<EMHSousModele> parents = noeud.getParents();
    for (EMHSousModele parent : parents) {
      EMHRelationFactory.removeRelationBidirect(parent, noeud);
    }
  }

  private static void detachOrRemoveSections(EMHScenario scenario, Collection<? extends EMH> toDelete,
                                             Set<CatEMHSection> sectionToRemoveCascade, RemoveBilan res,
                                             boolean cascade) {
    List<CatEMHSection> initialSectionToRemove = EMHHelper.selectEMHS(toDelete, EnumCatEMH.SECTION);

    Set<CatEMHSection> removed = new HashSet<>(sectionToRemoveCascade);

    //ces sections ne doivent pas être utilisé par la branche
    for (CatEMHSection section : initialSectionToRemove) {
      CatEMHBranche branche = section.getBranche();
      if (branche != null) {
        final String sectionName = section.getNom();
        if (branche.getSectionAmont().getEmh().getNom().equals(sectionName)) {
          res.sectionNotRemovedUsedByBranche.add(sectionName);
        } else if (branche.getSectionAval().getEmh().getNom().equals(sectionName)) {
          res.sectionNotRemovedUsedByBranche.add(sectionName);
        } else {
          //elle peut être enlevée.
          removed.add(section);
        }
      } else {//pas de branche parent:
        removed.add(section);
      }
    }
    //on fait un tri pour les sections pilotes et de reference:
    Collection<String> removedSectionNames = TransformerHelper.toSetNom(removed);
    for (Iterator<CatEMHSection> it = removed.iterator(); it.hasNext(); ) {
      CatEMHSection section = it.next();
      //la section est referencee par une sectionIdem qui ne sera pas enlevée:
      List<RelationEMHSectionIdemContientSection> relationSectionsIdemUsing = EMHHelper.getRelationSectionIdemUsing(section);
      for (RelationEMHSectionIdemContientSection relationSectionIdemUsing : relationSectionsIdemUsing) {
        if (relationSectionIdemUsing != null && !removedSectionNames.contains(relationSectionIdemUsing.getEmh().getNom())) {
          res.sectionNotRemovedReference.add(section.getNom());
          it.remove();
        }
      }
      //c'est une section pilote:
      CatEMHBranche branchePilotedBy = EMHHelper.getBranchePilotedBy(section);
      if (branchePilotedBy != null) {
        res.sectionNotRemovedPilote.add(section.getNom());
        it.remove();
      }
    }
    for (CatEMHSection section : removed) {
      boolean completelyDeleted = detachOrRemoveSection(section, cascade);
      if (completelyDeleted) {
        scenario.getIdRegistry().unregister(section);
      }
      scenario.getDonCLimMScenario().removeEMHFromCalcul(section);
      res.nbSectionRemoved++;
    }
  }

  private static void removeEMH(EMHScenario scenario, EMH toRemove) {
    scenario.getIdRegistry().unregister(toRemove);
    scenario.getDonCLimMScenario().removeEMHFromCalcul(toRemove);
  }

  public static class RemoveBilan {
    public final List<String> sectionNotRemovedUsedByBranche = new ArrayList<>();
    public final List<String> sectionNotRemovedPilote = new ArrayList<>();
    public final List<String> sectionNotRemovedReference = new ArrayList<>();
    public final List<String> noeudNotRemovedBranche = new ArrayList<>();
    public final List<String> noeudNotRemovedCasier = new ArrayList<>();
    public final List<Pair<String,String>> noeudRemovedSousModele = new ArrayList<>();

    public int nbCasierRemoved;
    public int nbSectionRemoved;
    public int nbBrancheRemoved;
    public int nbNoeudRemoved;

    public boolean isSomethingRemoved() {
      return nbCasierRemoved > 0 || nbSectionRemoved > 0 || nbBrancheRemoved > 0 || nbNoeudRemoved > 0 || noeudRemovedSousModele.size()>0;
    }
  }
}
