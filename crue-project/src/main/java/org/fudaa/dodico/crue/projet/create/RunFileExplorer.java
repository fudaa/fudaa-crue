/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.create;

import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.RunFile;

/**
 * 
 * @author Frederic Deniger
 */
public class RunFileExplorer {
  
  public static class ModeleContent{
    private final Map<CrueFileType, String> resFileIdByType=new EnumMap<>(CrueFileType.class);
    
    public String getResId(CrueFileType type){
      return resFileIdByType.get(type);
    }
  }
  
  private final Map<String,ModeleContent> runFileByModele=new HashMap<>();
  private final Map<String,RunFile> runFileById=new HashMap<>();
  
  public RunFileExplorer(Collection<RunFile> runFiles) {
    for (RunFile runFile : runFiles) {
      runFileById.put(runFile.getKey(), runFile);
      String id = runFile.getContainer().getNom();
      ModeleContent modeleContent = runFileByModele.get(id);

      if(modeleContent==null){
        modeleContent = new ModeleContent();
        
        runFileByModele.put(id, modeleContent);
      }
      
      modeleContent.resFileIdByType.put(runFile.getType(), runFile.getKey());
    }
  }
  
  public RunFile getRunFile(String key){
    return runFileById.get(key);
  }
  
  
  public ModeleContent getResContent(String modeleName){
    return runFileByModele.get(modeleName);
  }
  
  

}
