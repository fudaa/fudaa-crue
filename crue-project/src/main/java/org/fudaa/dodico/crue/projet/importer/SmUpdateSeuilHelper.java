package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateSourceTargetData;

import java.util.Collections;
import java.util.List;

public class SmUpdateSeuilHelper {
  private final CrueConfigMetier ccm;

  public SmUpdateSeuilHelper(CrueConfigMetier ccm) {
    this.ccm = ccm;
  }

  public static boolean isSeuil(EnumBrancheType targetBrancheType) {
    return EnumBrancheType.EMHBrancheSeuilLateral.equals(targetBrancheType) || EnumBrancheType.EMHBrancheSeuilTransversal.equals(targetBrancheType);
  }

  public static List<ElemSeuilAvecPdc> getSeuilInBrancheSeuil(CatEMHBranche branche) {
    if (EnumBrancheType.EMHBrancheSeuilTransversal.equals(branche.getBrancheType())) {
      DonCalcSansPrtBrancheSeuilTransversal dcsp = EMHHelper.selectFirstOfClass(branche.getInfosEMH(), DonCalcSansPrtBrancheSeuilTransversal.class);
      return dcsp.getElemSeuilAvecPdc();
    }
    if (EnumBrancheType.EMHBrancheSeuilLateral.equals(branche.getBrancheType())) {
      DonCalcSansPrtBrancheSeuilLateral dcsp = EMHHelper.selectFirstOfClass(branche.getInfosEMH(), DonCalcSansPrtBrancheSeuilLateral.class);
      return dcsp.getElemSeuilAvecPdc();
    }
    return Collections.emptyList();
  }

  public boolean willCoefBeChanged(SmUpdateSourceTargetData.Line<CatEMHBranche> brancheLine) {
    if (!brancheLine.getSourceToImport().getBrancheType().equals(brancheLine.getTargetToModify().getBrancheType())) {
      return true;
    }
    final List<ElemSeuilAvecPdc> seuilInSource = getSeuilInBrancheSeuil(brancheLine.getSourceToImport());
    final List<ElemSeuilAvecPdc> seuilInTarget = getSeuilInBrancheSeuil(brancheLine.getTargetToModify());
    final int size = seuilInSource.size();
    if (size != seuilInTarget.size()) {
      return true;
    }
    for (int i = 0; i < size; i++) {
      if (!seuilInSource.get(i).isSameLargeurAndCote(seuilInTarget.get(i), ccm)) {
        return true;
      }
    }

    return false;
  }
}
