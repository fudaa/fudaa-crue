package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateSourceTargetData;

import java.util.List;

public class SmUpdateSourceTargetBuilder {
  private final EMHScenario targetScenario;

  public SmUpdateSourceTargetBuilder(EMHScenario target) {
    this.targetScenario = target;
  }

  public SmUpdateSourceTargetData build(List<EMH> emhs) {
    SmUpdateSourceTargetData data = new SmUpdateSourceTargetData();
    if (emhs != null) {
      emhs.forEach(source -> {
        EMH target = targetScenario.getIdRegistry().findByName(source.getNom());
        if (target != null) {
          data.add(source, target);
        }
      });
    }
    data.sort();
    return data;
  }
}
