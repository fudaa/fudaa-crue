/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.planimetry;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;

/**
 * @author Frederic Deniger
 */
public class ReportPlanimetryExtraContainer {

  @XStreamAlias("BrancheConfiguration")
  private ReportPlanimetryBrancheExtraData brancheConfigurationExtraData;
  @XStreamAlias("NoeudConfiguration")
  private ReportPlanimetryNodeExtraData nodeConfigurationExtraData;
  @XStreamAlias("CasierConfiguration")
  private ReportPlanimetryCasierExtraData casierConfigurationExtraData;
  @XStreamAlias("SectionConfiguration")
  private ReportPlanimetrySectionExtraData sectionConfigurationExtraData;
  @XStreamAlias("TraceConfiguration")
  private ReportPlanimetryTraceExtraData traceConfigurationExtraData;

  public ReportPlanimetryExtraContainer() {
    this(true);
  }

  public ReportPlanimetryExtraContainer(boolean init) {
    if (init) {
      brancheConfigurationExtraData = new ReportPlanimetryBrancheExtraData();
      nodeConfigurationExtraData = new ReportPlanimetryNodeExtraData();
      casierConfigurationExtraData = new ReportPlanimetryCasierExtraData();
      sectionConfigurationExtraData = new ReportPlanimetrySectionExtraData();
      traceConfigurationExtraData = new ReportPlanimetryTraceExtraData();
    }
  }

  protected ReportPlanimetryExtraContainer(ReportPlanimetryExtraContainer from) {
    brancheConfigurationExtraData = from.brancheConfigurationExtraData.copy();
    nodeConfigurationExtraData = from.nodeConfigurationExtraData.copy();
    casierConfigurationExtraData = from.casierConfigurationExtraData.copy();
    sectionConfigurationExtraData = from.sectionConfigurationExtraData.copy();
    traceConfigurationExtraData = from.traceConfigurationExtraData.copy();
  }

  public ReportPlanimetryExtraContainer copy() {
    return new ReportPlanimetryExtraContainer(this);
  }

  public void reinitContent() {
    if (brancheConfigurationExtraData == null) {
      brancheConfigurationExtraData = new ReportPlanimetryBrancheExtraData();
    }
    if (nodeConfigurationExtraData == null) {
      nodeConfigurationExtraData = new ReportPlanimetryNodeExtraData();
    }
    if (casierConfigurationExtraData == null) {
      casierConfigurationExtraData = new ReportPlanimetryCasierExtraData();
    }
    if (sectionConfigurationExtraData == null) {
      sectionConfigurationExtraData = new ReportPlanimetrySectionExtraData();
    }
    if (traceConfigurationExtraData == null) {
      traceConfigurationExtraData = new ReportPlanimetryTraceExtraData();
    }
    brancheConfigurationExtraData.reinitContent();
    nodeConfigurationExtraData.reinitContent();
    casierConfigurationExtraData.reinitContent();
    sectionConfigurationExtraData.reinitContent();
    traceConfigurationExtraData.reinitContent();
    //appele pour des raisons de compatibilités avec d'anciennes versions
    traceConfigurationExtraData.clearLabels();
  }


  public ReportPlanimetryBrancheExtraData getBrancheConfigurationExtraData() {
    return brancheConfigurationExtraData;
  }

  public void setBrancheConfigurationExtraData(ReportPlanimetryBrancheExtraData brancheConfigurationExtraData) {
    this.brancheConfigurationExtraData = brancheConfigurationExtraData;
  }

  public ReportPlanimetryNodeExtraData getNodeConfigurationExtraData() {
    return nodeConfigurationExtraData;
  }

  public void setNodeConfigurationExtraData(ReportPlanimetryNodeExtraData nodeConfigurationExtraData) {
    this.nodeConfigurationExtraData = nodeConfigurationExtraData;
  }

  public ReportPlanimetryCasierExtraData getCasierConfigurationExtraData() {
    return casierConfigurationExtraData;
  }

  public void setCasierConfigurationExtraData(ReportPlanimetryCasierExtraData casierConfigurationExtraData) {
    this.casierConfigurationExtraData = casierConfigurationExtraData;
  }

  public ReportPlanimetrySectionExtraData getSectionConfigurationExtraData() {
    return sectionConfigurationExtraData;
  }

  public void setSectionConfigurationExtraData(ReportPlanimetrySectionExtraData sectionConfigurationExtraData) {
    this.sectionConfigurationExtraData = sectionConfigurationExtraData;
  }

  public ReportPlanimetryTraceExtraData getTraceConfigurationExtraData() {
    return traceConfigurationExtraData;
  }

  public void setTraceConfigurationExtraData(ReportPlanimetryTraceExtraData traceConfigurationExtraData) {
    this.traceConfigurationExtraData = traceConfigurationExtraData;
  }

  public boolean variablesUpdated(FormuleDataChanges changes) {
    return brancheConfigurationExtraData.variablesUpdated(changes)
        | nodeConfigurationExtraData.variablesUpdated(changes)
        | casierConfigurationExtraData.variablesUpdated(changes)
        | sectionConfigurationExtraData.variablesUpdated(changes)
        | traceConfigurationExtraData.variablesUpdated(changes);
  }
}
