/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("ReportRunEmhKey")
public class ReportRunEmhKey implements ReportKeyContract {

  public static final ReportRunEmhKey NULL_VALUE = new ReportRunEmhKey(ReportRunKey.NULL_VALUE, StringUtils.EMPTY);
  final ReportRunKey runKey;
  final String emhName;

  public ReportRunEmhKey(ReportRunKey runKey, String emhName) {
    this.runKey = runKey;
    this.emhName = emhName;
  }

  @Override
  public String getVariableName() {
    return null;
  }

  @Override
  public ReportKeyContract duplicateWithNewVar(String newName) {
    return null;
  }

  public String getEmhName() {
    return emhName;
  }

  @Override
  public ReportRunKey getReportRunKey() {
    return runKey;
  }

  @Override
  public boolean isExtern() {
    return false;
  }

  @Override
  public int hashCode() {
    return 3 + (runKey == null ? 0 : runKey.hashCode()) * 7 + (emhName == null ? 0 : emhName.hashCode()) * 13;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ReportRunEmhKey other = (ReportRunEmhKey) obj;
    if (this.runKey != other.runKey && (this.runKey == null || !this.runKey.equals(other.runKey))) {
      return false;
    }
    if ((this.emhName == null) ? (other.emhName != null) : !this.emhName.equals(other.emhName)) {
      return false;
    }
    return true;
  }
}
