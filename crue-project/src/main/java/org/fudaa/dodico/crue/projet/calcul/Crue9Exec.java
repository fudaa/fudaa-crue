/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.calcul;

import com.memoire.fu.FuLib;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.calcul.CalculExec;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.projet.ScenarioLoaderCrue9;
import org.fudaa.dodico.objet.CExecListener;

import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

public class Crue9Exec extends CalculExec implements CExecListener, CalculCrueContrat {
    private final String crue9Exe;
    private final String datFileName = "xxcrue8w.dat";
    /**
     * chemin vers la ressource interne contenant un template pour le fichier .dat
     */
    private final String datFileSourceFile = "/crue9/" + datFileName;
    private final ExecInput inputs;
    boolean calculFinishedCorrectly;
    private Process process;

    /**
     * @param crue9Exe exe crue9
     * @param inputs   donn&eacute;es en entr&eacute;es
     */
    public Crue9Exec(final String crue9Exe, final ExecInput inputs) {
        super();
        this.crue9Exe = crue9Exe;
        this.inputs = inputs;
        setChangeWorkingDirectory(true);
    }

    /**
     *
     * @return false car non appplication Crue9
     */
    @Override
    public boolean hasNoOptions() {
        return false;
    }

    @Override
    public synchronized void setProcess(Process _p) {
        this.process = _p;
    }

    @Override
    public void stop() {
        if (process != null) {
            process.destroy();
        }
    }

    @Override
    public boolean isStopFileWritten() {
        return false;
    }

    public String getFile(String sansExtension) {
        String ext = FuLib.isWindows() ? ".EXE" : ".sh";
        return sansExtension + ext;
    }

    private boolean createDatFile(final File datFile) {
        File crue9ExeFile = new File(this.crue9Exe);
        File emodcal = new File(crue9ExeFile.getParentFile(), getFile("EMODCAL"));
        Reader template = null;
        try {
            template = new InputStreamReader(getClass().getResourceAsStream(datFileSourceFile));
            final String error = CtuluLibFile.canWrite(datFile);
            if (error != null) {
                getUI().error(error);
                return false;
            }
            final Map<String, File> files = inputs.getProjet().getInputFiles(inputs.getScenario(), inputs.getRun());
            final File dc = ScenarioLoaderCrue9.findFileDc(inputs.getScenario(), files);
            final File dh = ScenarioLoaderCrue9.findFileDh(inputs.getScenario(), files);
            if (!CtuluLibFile.exists(dc)) {
                getUI().error("exec.crue9.dcFileNotFoundError");
                return false;
            }
            if (!CtuluLibFile.exists(dh)) {
                getUI().error("exec.crue9.dhFileNotFoundError");
                return false;
            }

            final Map<String, String> replace = new HashMap<>();
            replace.put("@EXE@", emodcal.getAbsolutePath());
            replace.put("@DH@", getSansExtensionAndUpperCase(dh));
            replace.put("@DC@", getSansExtensionAndUpperCase(dc));
            if (!CtuluLibFile.replaceAndCopyFile(template, datFile, replace, null)) {
                getUI().error(DodicoLib.getS("Copie de fichier"),
                        DodicoLib.getS("Erreur lors de la création du fichier {0} ", datFile.getAbsolutePath()), false);
                return false;
            }
        } finally {
            CtuluLibFile.close(template);
        }
        return true;
    }

    @Override
    public String getExecTitle() {
        return "Crue 9";
    }

    @Override
    public String[] getLaunchCmd(final File paramsFile, final CtuluUI ui) {
        return new String[]{crue9Exe};
    }

    private String getSansExtensionAndUpperCase(final File file) {
        return CtuluLibFile.getSansExtension(file.getName()).toUpperCase();
    }

    @Override
    public void run() {
        launch(getDirOfRun(), this, null);
    }

    @Override
    public boolean isComputeFinishedCorrectly() {
        return calculFinishedCorrectly;
    }

    @Override
    public boolean launch(final File f, final CExecListener l, final Runnable nextProcess) {
        calculFinishedCorrectly = true;
        process = null;
        final File dirForRun = getDirOfRun();
        if (!CtuluLibFile.exists(dirForRun)) {
            getUI().error(BusinessMessages.getString("exec.launchCrue9.noFolder"));
            return false;
        }
        final File datFile = new File(dirForRun, datFileName);
        if (!datFile.exists()) {
            if (!createDatFile(datFile)) {
                return false;
            }
        }

        calculFinishedCorrectly = super.launch(f, l, nextProcess);
        return calculFinishedCorrectly;
    }

    private File getDirOfRun() {
        return inputs.getProjet().getDirForRunModeleCrue9(inputs.getScenario(), inputs.getRun());
    }
}
