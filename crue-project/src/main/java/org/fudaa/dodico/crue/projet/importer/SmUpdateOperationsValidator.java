package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.DonFrtHelper;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOptions;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateValidationData;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Permet de valider et d'extraire les operations qui seront effectuées pour mettre à jour un sous-modèle
 */
public class SmUpdateOperationsValidator {
  private final Map<String, EMH> emhByNomInTarget;
  private final EMHScenario target;

  /**
   * @param target le scenario cible
   */
  public SmUpdateOperationsValidator(EMHScenario target) {
    emhByNomInTarget = target.getIdRegistry().getEmhByNom();
    this.target = target;
  }

  /**
   * @param sourceSousModele la source
   * @param updateOptions les options ( toutes les sections, les casiers, branches)
   * @param targetSousModeleName le nom du sous-modele cible
   * @return les données de la mise à jour
   */
  SmUpdateValidationData validate(EMHSousModele sourceSousModele, SmUpdateOptions updateOptions, String targetSousModeleName) {

    EMHSousModele targetSousModele = (EMHSousModele) emhByNomInTarget.get(targetSousModeleName);
    if (targetSousModele == null) {
      return new SmUpdateValidationData(true, targetSousModeleName);
    }
    SmUpdateValidationData smUpdateValidatorData = new SmUpdateValidationData(targetSousModeleName);


    //erreur: des emhs sont presentes dans un autre sous-modele cible
    fillTargetEmhInAnotherSousModele(sourceSousModele, targetSousModele, smUpdateValidatorData);

    //les donnees à importer
    final SmUpdateOptionsDataExtractor.SmUpdateOptionsData sourceDataToImport = new SmUpdateOptionsDataExtractor(sourceSousModele).extract(updateOptions);
    //erreur avec des sections requises mais non presentes dans la cible
    fillWithRequiredSectionsNotInTarget(sourceDataToImport, targetSousModele, smUpdateValidatorData);
    fillWithDontFrtNotInTarget(sourceDataToImport, targetSousModele, smUpdateValidatorData);
    return smUpdateValidatorData;
  }

  /**
   * On détecte les DonFrt qui sont définies dans un autre sous-modèle.
   *
   * @param sourceDataToImport les donnnees à importer
   * @param targetSousModele le sous-modele cible
   * @param smUpdateValidatorData le result
   */
  private void fillWithDontFrtNotInTarget(SmUpdateOptionsDataExtractor.SmUpdateOptionsData sourceDataToImport, EMHSousModele targetSousModele, SmUpdateValidationData smUpdateValidatorData) {
    final List<EMH> emhToAddOrUpdate = sourceDataToImport.getEmhToAddOrUpdate();
    final List<EMHSectionProfil> emhSectionProfils = EMHHelper.selectInstanceOf(emhToAddOrUpdate, EMHSectionProfil.class);
    final Set<DonFrt> usedDonFrt = DonFrtHelper.getUsedDonFrt(emhSectionProfils);
    final Map<String, String> sousModeleNameByDonFrtNameInTarget = DonFrtHelper.getSousModeleNameByDonFrtName(target);
    final String targetSousModeleNom = targetSousModele.getNom();
    usedDonFrt.forEach(donFrt -> {
      final String donFrtSousModeleName = sousModeleNameByDonFrtNameInTarget.get(donFrt.getNom());
      if (donFrtSousModeleName != null && !donFrtSousModeleName.equals(targetSousModeleNom)) {
        smUpdateValidatorData.addDonFrtInAnotherSousModele(donFrt.getNom(), donFrtSousModeleName);
      }
    });
  }



  /**
   * Les EMHs dans la cible qui appartiennent à un autre sous-modele -> erreur
   *
   * @param sourceSousModele sous-modele source
   * @param targetSousModele sous-modele cible
   * @param smUpdateValidatorData les donnees
   */
  private void fillTargetEmhInAnotherSousModele(EMHSousModele sourceSousModele, EMHSousModele targetSousModele, SmUpdateValidationData smUpdateValidatorData) {
    final Set<String> sourceNames = TransformerHelper.toSetNom(sourceSousModele.getAllSimpleEMH(EnumCatEMH.CASIER,EnumCatEMH.BRANCHE,EnumCatEMH.SECTION));
    final Set<String> targetNames = TransformerHelper.toSetNom(targetSousModele.getAllSimpleEMH(EnumCatEMH.CASIER,EnumCatEMH.BRANCHE,EnumCatEMH.SECTION));
    sourceNames.stream().map(emhByNomInTarget::get).filter(emh -> (emh != null && !targetNames.contains(emh.getNom())))
      .sorted(ObjetNommeByNameComparator.INSTANCE).forEach(smUpdateValidatorData::addDonFrtInAnotherSousModele);
  }

  /**
   * @param sourceDataToImport les données a importer
   * @param targetSousModele le sous-modele cible
   * @param smUpdateValidatorData les donnees
   */
  private void fillWithRequiredSectionsNotInTarget(final SmUpdateOptionsDataExtractor.SmUpdateOptionsData sourceDataToImport, EMHSousModele targetSousModele, SmUpdateValidationData smUpdateValidatorData) {
    final Set<String> targetNames = TransformerHelper.toSetNom(targetSousModele.getAllSimpleEMH());
    sourceDataToImport.getRequiredEmh().stream().filter(emh -> !targetNames.contains(emh.getNom()))
      .sorted(ObjetNommeByNameComparator.INSTANCE).forEach(emh -> smUpdateValidatorData.addRequiredNotInTarget(emh.getNom()));
  }
}
