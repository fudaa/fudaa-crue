/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import java.io.File;
import java.util.EnumMap;
import java.util.Map;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.FileIntegrityManager;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.io.AbstractCrue9FileFormat;
import org.fudaa.dodico.crue.io.Crue9FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.validation.util.Crue9ExportIgnoreContentValidation;

/**
 * Exporte les données au format crue neuf. Il est possible de définir les formats à exporter.
 *
 * @author Frederic Deniger
 */
public class ScenarioExporterCrue9 extends ScenarioSaverCrue9 {

  /**
   * Sauvegarder a partir d'un fichier modele:Fonction sauvegarder sous
   * @param scenario        le {@link EMHScenario} a persister
   * @param managerScenario le manager
   * @param ccm le {@link CrueConfigMetier}
   */
  public ScenarioExporterCrue9(final EMHScenario scenario, final ManagerEMHScenario managerScenario, CrueConfigMetier ccm) {
    super(scenario, managerScenario, new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE), ccm);
  }

  public void exportFor(String fichierDest) {
    Map<CrueFileType, File> files = new EnumMap<>(CrueFileType.class);
    addFileFor(files, fichierDest, CrueFileType.DC);
    addFileFor(files, fichierDest, CrueFileType.DH);
    FileIntegrityManager fileBackups = new FileIntegrityManager(files.values());
    fileBackups.start();
    try {
      super.export(files);
    } finally {
      fileBackups.finish(!super.getErrorManager().containsFatalError());
    }
    if (fileBackups.getBackupLog().isNotEmpty()) {
      super.getErrorManager().addLog(fileBackups.getBackupLog());
    }
  }

  @Override
  protected void validateCrueDataForCrue9(CtuluLogGroup error, CrueData crueData) {
    super.validateCrueDataForCrue9(error, crueData);
    CtuluLog log = Crue9ExportIgnoreContentValidation.validToExportInCrue9(crueData);
    if (!log.isEmpty()) {
      error.addLog(log);
    }
  }

  private void addFileFor(Map<CrueFileType, File> files, String fichierDest, CrueFileType fileType) {
    final AbstractCrue9FileFormat format = Crue9FileFormatFactory.getFileFormat(fileType);
    files.put(fileType, CtuluLibFile.changeExtension(new File(fichierDest), format.getExtensions()[0]));
  }
}
