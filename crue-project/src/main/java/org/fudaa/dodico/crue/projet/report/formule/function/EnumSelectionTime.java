/*
GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule.function;

/**
 *
 * Enum pour définir une sélection de pas de temps. Utilise dans EnumFormuleStat par exemple
 *
 * @author Frederic Deniger
 */
public enum EnumSelectionTime {

  ALL(true), // Tous les pas de temps
  ALL_PERMANENT(true),// Tous les pas de temps permanents
  ALL_TRANSIENT(true),// Tous les pas de temps transitoires
  SELECTION_PERMANENT(false),// Tous les pas de temps permanents
  SELECTION_TRANSIENT(false);// Tous les pas de temps transitoires

  private final boolean cachable;

  EnumSelectionTime(boolean cachable) {
    this.cachable = cachable;
  }

  public boolean isCachable() {
    return cachable;
  }


}
