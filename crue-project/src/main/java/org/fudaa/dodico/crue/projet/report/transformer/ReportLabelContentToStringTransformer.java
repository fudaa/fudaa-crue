/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.transformer;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.ctulu.converter.BooleanToStringTransformer;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLabelContentToStringTransformer extends AbstractPropertyToStringTransformer<ReportLabelContent> {

  private final AbstractPropertyToStringTransformer<ReportVariableKey> reportVariableKeyTransformer;
  final BooleanToStringTransformer booleanToStringTransformer = new BooleanToStringTransformer();

  public ReportLabelContentToStringTransformer(AbstractPropertyToStringTransformer<ReportVariableKey> reportVariableKeyTransformer) {
    super(ReportLabelContent.class);
    this.reportVariableKeyTransformer = reportVariableKeyTransformer;
  }

  @Override
  public String toStringSafe(ReportLabelContent in) {
    return in.getPrefix() + KeysToStringConverter.MAIN_SEPARATOR
            + reportVariableKeyTransformer.toString(in.getContent()) + KeysToStringConverter.MAIN_SEPARATOR
            + booleanToStringTransformer.toString(in.isUnit());
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }

  @Override
  public ReportLabelContent fromStringSafe(String in) {
    String[] split = StringUtils.splitPreserveAllTokens(in, KeysToStringConverter.MAIN_SEPARATOR);
    if (split.length == 3) {
      String prefix = split[0];
      ReportVariableKey variableKey = reportVariableKeyTransformer.fromString(split[1]);
      Boolean unit = booleanToStringTransformer.fromString(split[2]);
      final ReportLabelContent reportLabelContent = new ReportLabelContent();
      reportLabelContent.setPrefix(prefix);
      reportLabelContent.setUnit(Boolean.TRUE.equals(unit));
      reportLabelContent.setContent(variableKey);
      return reportLabelContent;
    }
    return ReportLabelContent.NULL;
  }
}
