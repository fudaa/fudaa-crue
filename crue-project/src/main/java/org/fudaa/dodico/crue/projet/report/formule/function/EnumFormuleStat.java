/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule.function;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

/**
 *
 * @author Frederic Deniger
 */
public enum EnumFormuleStat implements ObjetWithID {

  MOY_ALL_PERMANENT("moySurTousCP", "MoySurTousC_P", EnumSelectionTime.ALL_PERMANENT, EnumFormuleFunction.MOY),
  MOY_ALL_TRANSIENT("moySurTousCT", "MoySurTousC_T", EnumSelectionTime.ALL_TRANSIENT, EnumFormuleFunction.MOY),
  MOY_SELECTION_PERMANENT("moySurSelectionCP", "MoySurSelectionC_P", EnumSelectionTime.SELECTION_PERMANENT, EnumFormuleFunction.MOY),
  MOY_SELECTION_TRANSIENT("moySurSelectionCT", "MoySurSelectionC_T", EnumSelectionTime.SELECTION_TRANSIENT, EnumFormuleFunction.MOY),
  MAX_ALL_PERMANENT("maxSurTousCP", "MaxSurTousC_P", EnumSelectionTime.ALL_PERMANENT, EnumFormuleFunction.MAX),
  MAX_ALL_TRANSIENT("maxSurTousCT", "MaxSurTousC_T", EnumSelectionTime.ALL_TRANSIENT, EnumFormuleFunction.MAX),
  MAX_SELECTION_PERMANENT("maxSurSelectionCP", "MaxSurSelectionC_P", EnumSelectionTime.SELECTION_PERMANENT, EnumFormuleFunction.MAX),
  MAX_SELECTION_TRANSIENT("maxSurSelectionCT", "MaxSurSelectionC_T", EnumSelectionTime.SELECTION_TRANSIENT, EnumFormuleFunction.MAX),
  MIN_ALL_PERMANENT("minSurTousCP", "MinSurTousC_P", EnumSelectionTime.ALL_PERMANENT, EnumFormuleFunction.MIN),
  MIN_ALL_TRANSIENT("minSurTousCT", "MinSurTousC_T", EnumSelectionTime.ALL_TRANSIENT, EnumFormuleFunction.MIN),
  MIN_SELECTION_PERMANENT("minSurSelectionCP", "MinSurSelectionC_P", EnumSelectionTime.SELECTION_PERMANENT, EnumFormuleFunction.MIN),
  MIN_SELECTION_TRANSIENT("minSurSelectionCT", "MinSurSelectionC_T", EnumSelectionTime.SELECTION_TRANSIENT, EnumFormuleFunction.MIN),
  SUM_ALL_PERMANENT("sommeSurTousCP", "SommeSurTousC_P", EnumSelectionTime.ALL_PERMANENT, EnumFormuleFunction.SUM),
  SUM_ALL_TRANSIENT("sommeSurTousCT", "SommeSurTousC_T", EnumSelectionTime.ALL_TRANSIENT, EnumFormuleFunction.SUM),
  SUM_SELECTION_PERMANENT("sommeSurSelectionCP", "SommeSurSelectionC_P", EnumSelectionTime.SELECTION_PERMANENT, EnumFormuleFunction.SUM),
  SUM_SELECTION_TRANSIENT("sommeSurSelectionCT", "SommeSurSelectionC_T", EnumSelectionTime.SELECTION_TRANSIENT, EnumFormuleFunction.SUM);
  /**
   * Utilise pour savoir si une formule utilise une sélection sur les pas de temps sélectionné dans la vue courante.
   */
  public static final String FORMULE_WITH_SELECTION_TIME = "SurSelectionC_";
  private final String persistId;
  private final String expression;
  private final String nom;
  private final EnumSelectionTime timeSelection;
  private final EnumFormuleFunction formuleSelection;

  EnumFormuleStat(String persistId, String expression, EnumSelectionTime timeSelection, EnumFormuleFunction formuleSelection) {
    this.persistId = persistId;
    this.expression = expression;
    this.timeSelection = timeSelection;
    this.nom = geti8n(persistId);
    this.formuleSelection = formuleSelection;
  }

  public EnumFormuleFunction getFormuleSelection() {
    return formuleSelection;
  }


  public EnumSelectionTime getTimeSelection() {
    return timeSelection;
  }

  protected static String geti8n(String name) {
    try {
      return BusinessMessages.getString("formule.stat." + name);
    } catch (Exception e) {
      Logger.getLogger(EnumFormuleStat.class.getName()).log(Level.SEVERE, "i18n not found for {0}", name);
    }
    return name;
  }

  public String getPersistId() {
    return persistId;
  }

  public String getExpression() {
    return expression;
  }

  @Override
  public String getId() {
    return getPersistId();
  }

  @Override
  public String getNom() {
    return nom;
  }

  public static List<String> getExpressions() {
    List<String> expressions = new ArrayList<>();
    for (EnumFormuleStat value : EnumFormuleStat.values()) {
      expressions.add(value.getExpression());
    }
    return expressions;
  }
}
