/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleDependenciesContent {

  private final Map<String, Set<String>> usedVariables = new HashMap<>();

  public boolean isSet(String var) {
    return usedVariables.containsKey(var);
  }

  public UsedBy createUsedBy() {
    Map<String, Set<String>> usedBy = new HashMap<>();
    for (Map.Entry<String, Set<String>> entry : usedVariables.entrySet()) {
      String var = entry.getKey();
      Set<String> usedVars = entry.getValue();
      for (String usedVar : usedVars) {
        Set<String> used = usedBy.get(usedVar);
        if (used == null) {
          used = new HashSet<>();
          usedBy.put(usedVar, used);
        }

        used.add(var);
      }
    }
    return new UsedBy(usedBy);
  }

  public boolean isCylcique(String var) {
    Set<String> usedVariable = getUsedVariables(var);
    return usedVariable != null && usedVariable.contains(var);
  }

  public List<String> getCycliqueVar() {
    List<String> res = new ArrayList<>();
    for (String var : usedVariables.keySet()) {
      if (isCylcique(var)) {
        res.add(var);
      }
    }
    return res;
  }

  public Set<String> getUsedVariables(String var) {
    return usedVariables.get(var);
  }

  public void setUsedBy(String var, Set<String> used) {
    usedVariables.put(var, used);
  }

  public static class UsedBy {

    private final Map<String, Set<String>> usedBy;

    public UsedBy(Map<String, Set<String>> usedBy) {
      this.usedBy = usedBy;
    }

    public boolean isUsed(String var) {
      return CollectionUtils.isNotEmpty(usedBy.get(var));
    }

    public Set<String> usedBy(String var) {
      return usedBy.get(var);
    }
  }
}
