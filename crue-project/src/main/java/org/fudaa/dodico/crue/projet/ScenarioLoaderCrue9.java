/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.collections4.MapUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.Crue9FileFormatFactory;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.io.neuf.FCBSequentialReader;
import org.fudaa.dodico.crue.io.neuf.ResPrtReseauCrue9Adapter;
import org.fudaa.dodico.crue.io.neuf.STOReader;
import org.fudaa.dodico.crue.io.neuf.STOSequentialReader;
import org.fudaa.dodico.crue.io.neuf.STRFactory;
import org.fudaa.dodico.crue.io.neuf.STRSequentialReader;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.emh.CompteRendu;
import org.fudaa.dodico.crue.metier.emh.CompteRendusInfoEMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.OrdPrtGeoModeleBase;
import org.fudaa.dodico.crue.metier.emh.OrdPrtReseau;
import org.fudaa.dodico.crue.metier.emh.PlanimetrageNbrPdzCst;
import org.fudaa.dodico.crue.metier.emh.ResPrtReseau;
import org.fudaa.dodico.crue.metier.emh.Sorties;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.projet.validation.Crue9PostLoadEditionValidation;
import org.fudaa.dodico.crue.validation.ValidatorAndLoaderResultatCrue9;

/**
 * @author deniger
 */
public class ScenarioLoaderCrue9 {

  private final CtuluLog analyzer;
  private final CtuluLogGroup errorMng;
//  private final CtuluLog validationLog;
  private final ManagerEMHScenario scenario;
  private final CoeurConfigContrat coeur;

  public ScenarioLoaderCrue9(final ManagerEMHScenario scenario, final CtuluLogGroup errorMng, final CoeurConfigContrat coeur, final CtuluLog analyzer) {
    super();
    this.scenario = scenario;
    this.errorMng = errorMng;
//    validationLog = errorMng.createNewLog("load.crue9.resultFiles.prevalidation");
    this.analyzer = analyzer;
    this.coeur = coeur;
  }

  public static File findFileOfType(final ManagerEMHScenario scenarioCrue9, CrueFileType type, final Map<String, File> files) {
    // -- recherche des fichiers sous modeles --//
    final List<ManagerEMHModeleBase> fils = scenarioCrue9.getFils();
    if (fils.isEmpty()) {
      return null;
    } else if (fils.size() > 1) {
      return null;
    }
    final ManagerEMHModeleBase modele = fils.get(0);
    return files.get(modele.getListeFichiers().getFile(type).getNom());
  }

  public static File findFileDc(final ManagerEMHScenario scenarioCrue9, final Map<String, File> files) {
    return findFileOfType(scenarioCrue9, CrueFileType.DC, files);
  }

  public static File findFileDh(final ManagerEMHScenario scenarioCrue9, final Map<String, File> files) {
    return findFileOfType(scenarioCrue9, CrueFileType.DH, files);
  }

  /**
   * Ordonnance le chargement d'un projet crue 10: sous modeles puis modeles puis scenarios
   */
  protected EMHScenario compute(final Map<String, File> files, final Map<String, File> resFiles, ScenarioAutoModifiedState modifiedState) {

    // -- recherche des fichiers sous modeles --//
    final List<ManagerEMHModeleBase> fils = scenario.getFils();
    if (fils.isEmpty()) {
      analyzer.addSevereError("noCurrentModele.error");
      return null;
    } else if (fils.size() > 1) {
      analyzer.addSevereError("crue9.oneModele.authorized");
      return null;
    }
    final ManagerEMHModeleBase modele = fils.get(0);

    final File fichierDC = findFileDc(scenario, files);
    final File fichierDH = findFileDh(scenario, files);

    if (fichierDC == null) {
      analyzer.addSevereError("loader.crue9.noDefined", "DC", modele.getNom());
      return null;
    }
    if (fichierDH == null) {
      analyzer.addSevereError("loader.crue9.noDefined", "DH", modele.getNom());
      return null;
    }
    CtuluLog log = errorMng.createLog();
    final CrueIOResu<CrueData> ioResuDC = Crue9FileFormatFactory.getDCFileFormat().read(fichierDC, log,
            new CrueDataImpl(coeur.getCrueConfigMetier()));
    if (log.containsSevereError()) {
      return null;
    }
    // DH
    log = errorMng.createLog();
    final CrueIOResu<CrueData> ioResuDH = Crue9FileFormatFactory.getDHFileFormat().read(fichierDH, log, ioResuDC.getMetier());
    if (log.containsSevereError()) {
      return null;
    }

    final CrueData res = ioResuDH.getMetier();
    final CtuluLog ores = new CtuluLog();
    Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ORES, coeur).read(coeur.getDefaultFile(CrueFileType.ORES), ores, res);
    if (ores.isNotEmpty()) {
      errorMng.getLogs().add(ores);
    }
    if (ores.containsSevereError()) {
      return null;
    }
    final CtuluLog optr = new CtuluLog();
    //le fichier dc initialise optr:
    OrdPrtReseau readOptrFromDc = res.getOrCreateOPTR();
    Sorties savedSortie = new Sorties(readOptrFromDc.getSorties());
    Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.OPTR, coeur).read(coeur.getDefaultFile(CrueFileType.OPTR), optr, res);
    if (optr.isNotEmpty()) {
      errorMng.getLogs().add(optr);
    }
    if (optr.containsSevereError()) {
      return null;
    }
    res.getOPTR().getSorties().initWith(savedSortie);
    final EMHScenario emhScenario = res.getScenarioData();
    res.getModele().setNom(modele.getNom());
    res.getSousModele().setNom(CruePrefix.changePrefix(modele.getNom(), CruePrefix.P_MODELE, CruePrefix.P_SS_MODELE));
    EMHRelationFactory.addRelationContientEMH(emhScenario, res.getModele());
    EMHRelationFactory.addRelationContientEMH(res.getModele(), res.getSousModele());
    // on verifie que l'on peut renommer les noms correctement.
    final CtuluLog verifiePrefixeNomDonneesCrue9 = Crue9PostLoadEditionValidation.verifiePrefixeNomDonneesCrue9(res, modifiedState);
    if (verifiePrefixeNomDonneesCrue9.isNotEmpty()) {
      errorMng.getLogs().add(verifiePrefixeNomDonneesCrue9);
    }
    if (verifiePrefixeNomDonneesCrue9.containsSevereError()) {
      return null;
    }


    emhScenario.getCommentairesManager().setCommentaire(CrueFileType.DCLM, ioResuDH.getCrueCommentaire());
    emhScenario.getModeles().get(0).getSousModeles().get(0).getCommentairesManager().setCommentaire(CrueFileType.DRSO, ioResuDC.getCrueCommentaire());
    // initPlanimetrage(res, 50);
    loadRes(emhScenario, res, fichierDC, fichierDH, resFiles);
    return emhScenario;
  }
  private final static Logger LOGGER = Logger.getLogger(ScenarioLoaderCrue9.class.getName());

  private long getMax(final File... in) {
    long res = 0;
    if (in == null) {
      return res;
    }
    for (final File f : in) {
      res = Math.max(res, f.lastModified());
    }
    return res;
  }

  protected void loadRes(final EMHScenario emh, final CrueData data, final File dcFile, final File dhFile, final Map<String, File> resFiles) {
    if (MapUtils.isEmpty(resFiles) || dcFile == null || dhFile == null) {
      return;
    }
    File stdout = new File(dcFile.getParentFile().getParentFile(), ScenarioLoaderCrue10Result.STDOUTCSV_NAME);
    if (stdout.exists()) {
      CompteRendusInfoEMH info = new CompteRendusInfoEMH();
      info.addCompteRendu(new CompteRendu(stdout, null));
      emh.addInfosEMH(info);
    }
    final CtuluLogGroup logs = errorMng.createGroup("load.resFile");
    CtuluLog validationResLog = errorMng.createNewLog("load.crue9.resultFiles.prevalidation");
    validationResLog.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    if (data.isCrue9ContientDistmax()) {
      validationResLog.addSevereError("crue9.resNotLoad.becauseOfDistmax");
      return;
    }

    try {
      final EMHModeleBase modele = emh.getModeles().get(0);
      final File sto = resFiles.get(ManagerEMHScenario.getResKey(modele, CrueFileType.STO));
      final File str = resFiles.get(ManagerEMHScenario.getResKey(modele, CrueFileType.STR));
      final File fcb = resFiles.get(ManagerEMHScenario.getResKey(modele, CrueFileType.FCB));
      // default value for Crue9 Is it ok ?

      loadStrSto(emh, data, dcFile, logs, validationResLog, sto, str);
      if (!logs.containsFatalError()) {
        loadFcb(emh, data, dhFile, logs, validationResLog, sto, str, fcb);
      }
    } catch (final Exception e) {
      validationResLog.manageException(e);
      LOGGER.log(Level.SEVERE, "loadRes", e);
    }

  }

  private void loadStrSto(final EMHScenario emh, final CrueData data, final File dcFile, final CtuluLogGroup loadResFile, final CtuluLog prevalidLog, final File sto,
          final File str) throws Exception {// NOPMD ok
    final long timeEtudeDC = dcFile.lastModified();
    if (!CtuluLibFile.exists(sto)) {
      prevalidLog.addSevereError("load.stoFile.noExist", sto == null ? "?" : sto.getName());
      if (!CtuluLibFile.exists(str)) {
        prevalidLog.addSevereError("load.strFile.noExist", str == null ? "?" : str.getName());
      }
      return;
    } else if (!CtuluLibFile.exists(str)) {
      prevalidLog.addSevereError("load.strFile.noExist", str == null ? "?" : str.getName());
    }
    if (CtuluLibFile.exists(sto)) {
      final long stoTime = sto.lastModified();
      if (stoTime < timeEtudeDC) {
        prevalidLog.addSevereError("load.stoFile.cancelledBecauseTooOld");
        return;
      }
      final CrueIOResu<STOSequentialReader> stoRes = Crue9FileFormatFactory.getSTOFileFormat().read(sto, data);
      loadResFile.addLog(stoRes.getAnalyse());
      ResPrtReseauCrue9Adapter prt = null;
      if (stoRes != null && stoRes.getMetier() != null) {
        data.setSto(stoRes.getMetier());
        prt = STOReader.alimenteObjetsMetier(stoRes.getMetier());
        final int nbhaut = stoRes.getMetier().getParametresGeneraux().getNbhaut();
        initPlanimetrage(data, nbhaut);
      }

      if (!stoRes.getAnalyse().containsSevereError() && CtuluLibFile.exists(str)) {
        final long strTime = str.lastModified();
        if (strTime < timeEtudeDC) {
          prevalidLog.addSevereError("load.strFile.cancelledBecauseTooOld");
          return;
        }
        final CrueIOResu<STRSequentialReader> read = Crue9FileFormatFactory.getSTRFileFormat().read(str, data);
        loadResFile.addLog(read.getAnalyse());
        if (read != null) {
          if(prt!=null) {
            prt.setReader(read.getMetier());
          }
          data.getModele().addInfosEMH(new ResPrtReseau());
          // on remplit les données pour les sections appartenant a des branches de type 6:
          read.getMetier().setLoiZDactByIdBranch6(STRFactory.getLoiZDactForBranche6((STOSequentialReader) data.getSto(), data));
          read.getMetier().setCoefSinuoBySection(STRFactory.getCoefSinuoBySection((STOSequentialReader) data.getSto(), data));
          // ensuite on valid les résultats
          ValidatorAndLoaderResultatCrue9.loadAndValidStr(prt, data, emh, loadResFile);
        }
      } else {
        prevalidLog.addSevereError("load.strFile.notDone");
      }
    }
  }

  private void initPlanimetrage(final CrueData data, final int nbhaut) {
    OrdPrtGeoModeleBase ordPrtGeoModeleBase = data.getModele().getOrdPrtGeoModeleBase();
    if (ordPrtGeoModeleBase == null) {
      ordPrtGeoModeleBase = new OrdPrtGeoModeleBase(data.getCrueConfigMetier());
      data.getModele().addInfosEMH(ordPrtGeoModeleBase);
    }
    final PlanimetrageNbrPdzCst pdz = new PlanimetrageNbrPdzCst(data.getCrueConfigMetier());
    pdz.setNbrPdz(nbhaut);
    ordPrtGeoModeleBase.setPlanimetrage(pdz);
  }

  private void loadFcb(final EMHScenario emh, final CrueData data, final File dhFile, final CtuluLogGroup loadResFile, final CtuluLog prevalidLog, final File sto,
          final File str, final File fcb) throws Exception {// NOPMD ok
    if (!CtuluLibFile.exists(fcb)) {
      prevalidLog.addError("load.fcbFile.noExist", fcb == null ? "?" : fcb.getName());
    } else if (CtuluLibFile.exists(sto) && CtuluLibFile.exists(str)) {
      final long fcbTime = fcb.lastModified();
      if (fcbTime < getMax(sto, str, dhFile)) {
        prevalidLog.addError("load.fcbFile.cancelledBecauseTooOld");
        return;
      }
      final CrueIOResu<FCBSequentialReader> read = Crue9FileFormatFactory.getFCBFileFormat().read(fcb, data);
      if(read!=null) {
        loadResFile.addLog(read.getAnalyse());
      }
      if (read != null && read.getMetier() != null) {
        ValidatorAndLoaderResultatCrue9.validFcb(read.getMetier(), data, emh, loadResFile);
      }
    }
  }
}
