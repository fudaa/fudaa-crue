/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.select;

import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.etude.*;

import java.util.*;

/**
 * @author Chris
 */
public class SelectableFinder {
    private EMHProjet project;

    public EMHProjet getProject() {
        return project;
    }

    public void setProject(EMHProjet project) {
        this.project = project;
    }

    private void fillSelectedItems(List<? extends ManagerEMHContainerBase> currentSelected, Set<ManagerEMHContainerBase> selectedContainers,
                                   Set<FichierCrue> selectedFiles) {
        for (ManagerEMHContainerBase container : currentSelected) {
            selectedContainers.add(container);

            if (container instanceof ManagerEMHContainer<?>) {
                this.fillSelectedItems(((ManagerEMHContainer<ManagerEMHContainerBase>) container).getFils(), selectedContainers, selectedFiles);
            }

            final FichierCrueManager filesManager = container.getListeFichiers();

            if (filesManager != null) {
                selectedFiles.addAll(filesManager.getFichiers());
            }
        }
    }

    private void removeChildren(List<ManagerEMHContainerBase> children, Set<ManagerEMHContainerBase> selectedContainers, Set<FichierCrue> selectedFiles,
                                Collection<? extends ManagerEMHContainerBase> toAvoid) {
        for (ManagerEMHContainerBase child : children) {
            //on n'inclus pas
            if (toAvoid.contains(child)) {
                continue;
            }
            selectedContainers.remove(child);
            final FichierCrueManager filesManager = child.getListeFichiers();
            if (filesManager != null) {
                selectedFiles.removeAll(filesManager.getFichiers());
            }
            if (child instanceof ManagerEMHContainer<?>) {
                this.removeChildren(((ManagerEMHContainer<ManagerEMHContainerBase>) child).getFils(), selectedContainers, selectedFiles, toAvoid);
            }
        }
    }

    /**
     * @param containerToFilter les conteneurs a filtrer
     * @return une liste non vide des conteneur non utilisé par un conteneur parent.
     */
    private List<ManagerEMHContainerBase> getNotUsed(List<ManagerEMHContainerBase> containerToFilter) {
        List<ManagerEMHContainerBase> notUsed = new ArrayList<>();
        for (ManagerEMHContainerBase toTest : containerToFilter) {
            CrueLevelType parent = toTest.getLevel().getParent();
            if (parent == null || CrueLevelType.PROJET.equals(parent)) {
                notUsed.add(toTest);
            } else {
                for (ManagerEMHContainerBase parentManager : project.getListOfManager(parent)) {
                    if (!((ManagerEMHContainer) parentManager).getFils().contains(toTest)) {
                        notUsed.add(toTest);
                    }
                }
            }
        }
        return notUsed;
    }

    /**
     * @return la liste de roots et enfants non utilisés par un autre conteneur.
     */
    public SelectedItems getNotUsed(List<ManagerEMHContainerBase> roots, boolean deep) {
        return getIndependantSelection(getNotUsed(roots), deep);
    }

    /**
     * @param roots les conteneurs de haut-niveau
     * @param deep  ce parametre semble inutile car dans le cas non deep, seuls les roots sont ramenés.
     * @return la liste des rootToSelect plus la liste de ses fils non utilisés par un autre conteneur.
     */
    public SelectedItems getIndependantSelection(List<? extends ManagerEMHContainerBase> roots, boolean deep) {
        final List<ManagerEMHContainerBase> others = new ArrayList<>();
        final Set<ManagerEMHContainerBase> selectedContainers = new HashSet<>();
        final Set<FichierCrue> selectedFiles = new HashSet<>();

        if (deep) {
            this.fillSelectedItems(roots, selectedContainers, selectedFiles);
        } else {
            selectedContainers.addAll(roots);
        }

        others.addAll(this.project.getListeScenarios());
        others.addAll(this.project.getListeModeles());
        others.addAll(this.project.getListeSousModeles());

        others.removeAll(selectedContainers);

        for (ManagerEMHContainerBase container : others) {
            final FichierCrueManager filesManager = container.getListeFichiers();

            if (filesManager != null) {
                selectedFiles.removeAll(filesManager.getFichiers());
            }

            if (container instanceof ManagerEMHContainer<?>) {
                this.removeChildren(((ManagerEMHContainer<ManagerEMHContainerBase>) container).getFils(), selectedContainers, selectedFiles, roots);
            }
        }

        final SelectedItems selectedItems = new SelectedItems();

        selectedItems.selectedContainers.addAll(selectedContainers);
        selectedItems.selectedFiles.addAll(selectedFiles);

        return selectedItems;
    }

    public SelectedItems getItems(List<ManagerEMHContainerBase> rootToSelect, boolean deep) {
        final Set<ManagerEMHContainerBase> selectedContainers = new HashSet<>();
        final Set<FichierCrue> selectedFiles = new HashSet<>();

        if (deep) {
            this.fillSelectedItems(rootToSelect, selectedContainers, selectedFiles);
        } else {
            selectedContainers.addAll(rootToSelect);
        }

        final SelectedItems selectedItems = new SelectedItems();

        selectedItems.selectedContainers.addAll(selectedContainers);
        selectedItems.selectedFiles.addAll(selectedFiles);

        return selectedItems;
    }

    public static class SelectedItems {
        private final List<ManagerEMHContainerBase> selectedContainers = new ArrayList<>();
        private final List<FichierCrue> selectedFiles = new ArrayList<>();

        public List<ManagerEMHContainerBase> getSelectedContainers() {
            return selectedContainers;
        }

        public List<FichierCrue> getSelectedFiles() {
            return selectedFiles;
        }

        public boolean isEmpty() {
            return (this.selectedContainers.size() + this.selectedFiles.size()) == 0;
        }
    }
}
