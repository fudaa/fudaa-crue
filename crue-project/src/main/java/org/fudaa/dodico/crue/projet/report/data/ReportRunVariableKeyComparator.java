/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

import org.fudaa.dodico.crue.common.SafeComparator;

/**
 *
 * @author Frederic Deniger
 */
public class ReportRunVariableKeyComparator extends SafeComparator<ReportRunKey> {

  @Override
  protected int compareSafe(ReportRunKey o1, ReportRunKey o2) {
    if (o1.isCourant() && !o2.isCourant()) {
      return -1;
    }
    if (!o1.isCourant() && o2.isCourant()) {
      return 1;
    }
    return o1.getDisplayName().compareTo(o2.getDisplayName());
  }
}
