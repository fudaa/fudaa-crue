/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.FormuleContentManager;

import java.util.*;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleContentManagerDefault implements FormuleContentManager {

  private final Map<String, Set<String>> usedVar = new HashMap<>();
  private final Set<String> timeDependant = new HashSet<>();

  public void add(String var, boolean timeDependant, String... dependant) {
    usedVar.put(var, Collections.unmodifiableSet(new HashSet<>(Arrays.asList(dependant))));
    if (timeDependant) {
      this.timeDependant.add(var);
    }
  }

  public void add(String var, boolean timeDependant, Collection<String> dependant) {
    usedVar.put(var, Collections.unmodifiableSet(new HashSet<>(dependant)));
    if (timeDependant) {
      this.timeDependant.add(var);
    }
  }

  @Override
  public Set<String> getDirectUsedVariable(String var) {
    Set<String> get = usedVar.get(var);
    return get == null ? Collections.emptySet() : get;
  }

  @Override
  public boolean containsTimeDependantVariable(String var) {
    return timeDependant.contains(var);
  }

  /**
   *
   * @param var           la variable à tester
   * @param selectedTimes les temps sélectionnées
   * @return true par defaut. pas d'intéret ici
   */
  @Override
  public boolean isExpressionValid(String var, Collection<ResultatTimeKey> selectedTimes) {
    return true;
  }
}
