/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet;

import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

import java.util.concurrent.Callable;

/**
 * @author Frederic Deniger
 */
public class ProjectLoadCallable implements Callable<ScenarioLoaderOperation> {
  final EMHProjet projet;
  final ManagerEMHScenario scenario;
  final EMHRun run;

  public ProjectLoadCallable(EMHProjet projet, ManagerEMHScenario scenario, EMHRun run) {
    this.projet = projet;
    this.scenario = scenario;
    this.run = run;
  }

  @Override
  public ScenarioLoaderOperation call() throws Exception {
    ScenarioLoader loader = new ScenarioLoader(scenario, projet, projet.getCoeurConfig());
    return loader.load(run);
  }
}
