/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;

/**
 * Class containing result of a load action.
 *
 * @author deniger
 */
public class ScenarioLoaderOperation extends CrueOperationResult<EMHScenario> {

  ScenarioAutoModifiedState autoModifiedState = new ScenarioAutoModifiedState();

  public ScenarioLoaderOperation(EMHScenario scenario, CtuluLogGroup logs) {
    super(scenario, logs);
  }

  public ScenarioAutoModifiedState getAutoModifiedState() {
    return autoModifiedState;
  }



  public void setAutoModifiedState(ScenarioAutoModifiedState autoModifiedState) {
    this.autoModifiedState = autoModifiedState;
  }
  
  
}
