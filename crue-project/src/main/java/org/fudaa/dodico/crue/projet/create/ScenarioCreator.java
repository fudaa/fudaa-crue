/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.create;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;

public class ScenarioCreator {

  private final EMHProjet projet;

  public ScenarioCreator(EMHProjet projet) {
    super();
    this.projet = projet;
  }

  public CrueOperationResult<ManagerEMHScenario> create(ContainerLevelData data, InfosCreation infos) {
    ContainerLevelDataValidator validator = new ContainerLevelDataValidator(projet);
    CtuluLog log = validator.valid(data, false);
    ManagerEMHScenario scenario = null;
    if (!log.containsSevereError()) {
      scenario = this.createScenario(data, infos);
    }
    CrueOperationResult<ManagerEMHScenario> res = new CrueOperationResult<>(scenario, log);
    res.getLogs().setDescription("create.createScenario.desc");
    res.getLogs().setDescriptionArgs(data.getName());
    return res;
  }

  public CrueOperationResult<ManagerEMHScenario> createNew(String name, InfosCreation infos) {
    return this.create(LevelDataFactory.createScenarioLevelData(name, infos.getCrueVersion()), infos);
  }

  private ManagerEMHScenario createScenario(ContainerLevelData data, InfosCreation infos) {
    assert (data.getLevelType() == CrueLevelType.SCENARIO);

    ManagerEMHScenario scenario = new ManagerEMHScenario(data.getName());

    for (ContainerLevelData child : data.getChildren()) {
      scenario.addManagerFils(this.createModele(child, infos));
    }

    for (FichierCrue fichier : data.getFiles()) {
      scenario.addFichierDonnees(fichier);
    }

    scenario.setInfosVersions(infos.createInfosVersion());

    return scenario;
  }

  private ManagerEMHModeleBase createModele(ContainerLevelData data, InfosCreation infos) {
    assert (data.getLevelType() == CrueLevelType.MODELE);

    ManagerEMHModeleBase modele = new ManagerEMHModeleBase(data.getName());

    //pas de sous-modele pour le type crue9:
    if (!CrueVersionType.CRUE9.equals(infos.getCrueVersion())) {
      for (ContainerLevelData child : data.getChildren()) {
        modele.addManagerFils(this.createSousModele(child, infos));
      }
    }

    for (FichierCrue fichier : data.getFiles()) {
      modele.addFichierDonnees(fichier);
    }

    modele.setInfosVersions(infos.createInfosVersion());

    return modele;
  }

  private ManagerEMHSousModele createSousModele(ContainerLevelData data, InfosCreation infos) {
    assert (data.getLevelType() == CrueLevelType.SOUS_MODELE);

    ManagerEMHSousModele sousModele = new ManagerEMHSousModele(data.getName());

    for (FichierCrue fichier : data.getFiles()) {
      sousModele.addFichierDonnees(fichier);
    }

    sousModele.setInfosVersions(infos.createInfosVersion());

    return sousModele;
  }
}
