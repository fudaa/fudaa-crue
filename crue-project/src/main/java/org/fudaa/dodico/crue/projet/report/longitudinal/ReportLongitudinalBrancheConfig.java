/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.longitudinal;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

/**
 * Contient les éléments de configuration d'une branche participant à la vue longitudinale.
 *
 * @author Frederic Deniger
 */
@XStreamAlias("BrancheConfig")
public class ReportLongitudinalBrancheConfig {

  public static final String PROP_NAME = "name";
  public static final String PROP_SENS = "sensParcours";
  public static final String PROP_LENGTH = "length";
  public static final String PROP_LENGTH_HYD = "lengthHyd";
  public static final String PROP_DECAL = "decalXp";

  public enum SensParcours implements ToStringInternationalizable {

    AMONT_AVAL, AVAL_AMONT;

    @Override
    public String geti18n() {
      return BusinessMessages.getString(name().toLowerCase() + ".name");
    }

    @Override
    public String geti18nLongName() {
      return geti18n();
    }
  }
  @XStreamAlias("Branche-Nom")
  String name;
  @XStreamAlias("Sens")
  SensParcours sensParcours = SensParcours.AMONT_AVAL;
  @XStreamAlias("Longueur")
  double length;
  @XStreamAlias("DecalXp")
  double decalXp;

  @PropertyDesc(i18n = "name.property", i18nBundleBaseName = "org/fudaa/fudaa/crue/report/longitudinal/Bundle.properties")
  public String getName() {
    return name;
  }


  public ReportLongitudinalBrancheConfig(){

  }

  public ReportLongitudinalBrancheConfig(ReportLongitudinalBrancheConfig from){
    if(from==null) return;
    this.name=from.name;
    this.decalXp=from.decalXp;
    this.length=from.length;
    this.sensParcours=from.sensParcours;


  }

  public ReportLongitudinalBrancheConfig duplicate(){
    return new ReportLongitudinalBrancheConfig(this);
  }


  public void setName(String name) {
    this.name = name;
  }

  @PropertyDesc(i18n = "sens.property", i18nBundleBaseName = "org/fudaa/fudaa/crue/report/longitudinal/Bundle.properties")
  public SensParcours getSensParcours() {
    return sensParcours;
  }

  public void setSensParcours(SensParcours sensParcours) {
    this.sensParcours = sensParcours;
  }

  @PropertyDesc(i18n = "length.property", i18nBundleBaseName = "org/fudaa/fudaa/crue/report/longitudinal/Bundle.properties")
  public double getLength() {
    return length;
  }

  public void setLength(double length) {
    this.length = length;
  }

  @PropertyDesc(i18n = "decalXp.property", i18nBundleBaseName = "org/fudaa/fudaa/crue/report/longitudinal/Bundle.properties")
  public double getDecalXp() {
    return decalXp;
  }

  public void setDecalXp(double decalXp) {
    this.decalXp = decalXp;
  }
}
