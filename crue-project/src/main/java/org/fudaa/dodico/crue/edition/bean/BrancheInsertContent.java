package org.fudaa.dodico.crue.edition.bean;

import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;

/**
 * Le contenu des parametres pour découper une branche en 2: a partir d'une branche sélectionée et d'une section, l'objectif est de créer une nouvelle branche
 * aval qui contiendra toutes les sections
 *
 * @author deniger
 */
public class BrancheInsertContent {
  /**
   * Le nom du noeud qui sera utilisée pour faire l'insertion
   */
  public String neoudNameToSplitOn;
  /**
   * le nom de la branche a créer.
   */
  public String newBrancheName;

  /**
   * le nom du noeud à créer.
   */
  public String newNoeudName;
  /**
   * Le type de la branche à créer
   */
  public EnumBrancheType newBrancheType;
  /**
   * A utiliser pour les branches saint-venant ou strickler
   */
  public double distance;
  /**
   * la branche doit être insérée à l'aval ou à l'amont
   */
  public boolean addAval=true;
}
