package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.validation.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SmUpdateSourceLoader {
  private final CoeurConfig coeurConfigDefault;

  public SmUpdateSourceLoader(CoeurConfig coeurConfigDefault) {
    this.coeurConfigDefault = coeurConfigDefault;
  }

  public CrueOperationResult<EMHSousModele> updateFrom(File drsoFile) {
    CtuluLogGroup logGroup = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    logGroup.setDescription("sm.updater.LoadValidationLog");
    logGroup.setDescriptionArgs(drsoFile.getName());
    SmUpdateSourceDrsoLoader loader = new SmUpdateSourceDrsoLoader(coeurConfigDefault);
    final CrueOperationResult<EMHSousModele> load = loader.load(drsoFile);
    logGroup.addGroup(load.getLogs());
    if (logGroup.containsError() || load.getResult() == null) {
      return new CrueOperationResult<>(null, logGroup.createCleanGroup());
    }
    final EMHSousModele sourceSousModele = load.getResult();

    //on doit valider le sous-modele
    logGroup.addGroup(validateScenario(sourceSousModele));
    return new CrueOperationResult<>(sourceSousModele, logGroup);
  }

  private CtuluLogGroup validateScenario(EMHSousModele sourceSousModele) {
    EMHModeleBase modeleBase = new EMHModeleBase();
    modeleBase.setNom(CruePrefix.P_MODELE + "1");
    modeleBase.addInfosEMH(new ParamNumModeleBase(coeurConfigDefault.getCrueConfigMetier()));
    modeleBase.addInfosEMH(new OrdPrtGeoModeleBase(coeurConfigDefault.getCrueConfigMetier()));
    final OrdPrtCIniModeleBase ordPrtCIniModeleBase = new OrdPrtCIniModeleBase(coeurConfigDefault.getCrueConfigMetier());
    ordPrtCIniModeleBase.setMethodeInterpol(EnumMethodeInterpol.LINEAIRE);
    modeleBase.addInfosEMH(ordPrtCIniModeleBase);
    modeleBase.addInfosEMH(new OrdPrtReseau(coeurConfigDefault.getCrueConfigMetier()));

    EMHScenario emhScenario = new EMHScenario();
    emhScenario.setNom(CruePrefix.P_SCENARIO + "1");
    emhScenario.addInfosEMH(new ParamCalcScenario(coeurConfigDefault.getCrueConfigMetier()));
    emhScenario.addInfosEMH(new OrdResScenario());
    emhScenario.addInfosEMH(new OrdCalcScenario());
    emhScenario.addInfosEMH(new DonCLimMScenario());
    emhScenario.addInfosEMH(new DonLoiHYConteneur());

    EMHRelationFactory.addRelationContientEMH(emhScenario, modeleBase);
    EMHRelationFactory.addRelationContientEMH(modeleBase, sourceSousModele);
    IdRegistry.install(emhScenario);
    final CtuluLogGroup validationBilan = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    validationBilan.setDescription("loader.valid.group");
    final CtuluLog analyseValidationCOnnexite = ValidateConnectionModele.validateConnexite(emhScenario);
    validationBilan.addLog(analyseValidationCOnnexite);
    List<CrueValidator> validators = new ArrayList<>();
    validators.add(new ValidatorForIDCrue10());
    final CrueConfigMetier crueConfigMetier = coeurConfigDefault.getCrueConfigMetier();
    ScenarioAutoModifiedState modifiedState = new ScenarioAutoModifiedState();
    validators.add(new ValidateAndRebuildProfilSection(crueConfigMetier, modifiedState));
    validators.add(new ValidatorSousModeleContent());
    validators.add(new ValidatorNomCasierContent());
    validators.add(new ValidatorNomProfilCasierContent());
    validators.add(new ValidatorNomSectionContent());
    ValidationHelper.validateAll(validationBilan, emhScenario, validators);

    EMHRelationFactory.removeRelationBidirect(modeleBase, sourceSousModele);
    return validationBilan;
  }
}
