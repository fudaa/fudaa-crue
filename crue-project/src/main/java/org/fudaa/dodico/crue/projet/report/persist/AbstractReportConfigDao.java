/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.persist;

import org.fudaa.dodico.crue.common.io.AbstractCrueDao;

/**
 *
 * @author Frederic Deniger
 */
public abstract class AbstractReportConfigDao<T extends ReportConfigContrat> extends AbstractCrueDao {

  public AbstractReportConfigDao() {
  }

  public abstract T getConfig();

  public abstract void setConfig(T config);
}
