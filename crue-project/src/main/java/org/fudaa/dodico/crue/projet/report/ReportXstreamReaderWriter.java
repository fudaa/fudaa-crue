/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report;

import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.common.CrueConverterIdentity;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;

/**
 *
 * @author Frederic Deniger
 */
public class ReportXstreamReaderWriter<D extends AbstractCrueDao> extends CrueDataXmlReaderWriterImpl<D, D> {

  public static final String VERSION = "1.0";

  public ReportXstreamReaderWriter(String fileType, String version, CrueDataDaoStructure daoConfigurer) {
    super(fileType, VERSION, new CrueConverterIdentity<>(), daoConfigurer);
  }
}
