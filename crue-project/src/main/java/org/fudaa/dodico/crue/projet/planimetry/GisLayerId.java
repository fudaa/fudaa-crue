package org.fudaa.dodico.crue.projet.planimetry;

/**
 * les identifiants des  fichiers SIG portés par Fudaa-Crue
 */
public enum GisLayerId {
  NOEUDS("noeuds"),
  BRANCHES("branches"),
  TRACES_SECTIONS("tracesSections"),
  CASIERS("casiers");
  private final String filename;

  GisLayerId(String filename) {
    this.filename = filename;
  }

  public String getFilename() {
    return filename;
  }

  public String getShpFilename() {
    return getFilename("shp");
  }

  public String getFilename(String extension) {
    return filename + "." + extension;
  }
}
