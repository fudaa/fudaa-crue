/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.rename;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.etude.*;
import org.fudaa.dodico.crue.projet.select.SelectableFinder.SelectedItems;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.joda.time.LocalDateTime;

import java.io.File;
import java.text.NumberFormat;
import java.util.*;
import java.util.Map.Entry;

/**
 * @author Chris
 */
public class RenameManager {

    public static final NumberFormat FORMATTER = NumberFormat.getIntegerInstance();

    public ConnexionInformation getConnexionInformation() {
        return connexionInformation;
    }

    public void setConnexionInformation(ConnexionInformation connexionInformation) {
        this.connexionInformation = connexionInformation;
    }


    public static class NewNameInformations {

        public final Map<String, ManagerEMHContainerBase> newNameContainers = new HashMap<>();
        public final Map<String, FichierCrue> newNameFiles = new HashMap<>();

        public boolean isEmpty() {
            return (this.newNameContainers.size() + this.newNameFiles.size()) == 0;
        }
    }

    private EMHProjet project;

    private ConnexionInformation connexionInformation;

    static {
        FORMATTER.setMinimumIntegerDigits(2);
    }

    public EMHProjet getProject() {
        return project;
    }

    public void setProject(EMHProjet project) {
        this.project = project;
    }

    public NewNameInformations getNewNames(ManagerEMHContainerBase container, SelectedItems selectedItems, String newRadical,
                                           boolean deep) {
        final NewNameInformations infos = new NewNameInformations();

        if (selectedItems.getSelectedContainers().contains(container)) {
            infos.newNameContainers.put(container.getLevel().getPrefix() + newRadical, container);
        }

        if (deep) {
            if (container.getListeFichiers() != null) {
                for (FichierCrue file : container.getListeFichiers().getFichiers()) {
                    if (selectedItems.getSelectedFiles().contains(file)) {
                        infos.newNameFiles.put(newRadical + "." + file.getType().getExtension(), file);
                    }
                }
            }

            this.computeNewChildrenNames(container, newRadical, infos, selectedItems);
        }

        return infos;
    }

    public NewNameInformations getNewNames(FichierCrue file, String newRadical) {
        final NewNameInformations infos = new NewNameInformations();

        infos.newNameFiles.put(newRadical + "." + file.getType().getExtension(), file);

        return infos;
    }

    private void computeNewChildrenNames(ManagerEMHContainerBase container, String newRadical, NewNameInformations informations,
                                         SelectedItems selectedItems) {
        final List<ManagerEMHContainerBase> children = (container instanceof ManagerEMHContainer<?>) ? (List<ManagerEMHContainerBase>) ((ManagerEMHContainer<?>) container).getFils() : null;
        boolean uniqueChild = children == null || children.size() == 1;
        int currentIdx = 1;

        if (children != null) {
            for (ManagerEMHContainerBase child : children) {
                if (selectedItems.getSelectedContainers().contains(child)) {
                    String newName = newRadical;

                    if (!uniqueChild) {
                        newName += FORMATTER.format(currentIdx++);
                    }

                    informations.newNameContainers.put(child.getLevel().getPrefix() + newName, child);

                    this.computeNewChildrenNames(child, newName, informations, selectedItems);
                }
            }
        }

        if (container.getListeFichiers() != null) {
            for (FichierCrue file : container.getListeFichiers().getFichiers()) {
                if (selectedItems.getSelectedFiles().contains(file)) {
                    informations.newNameFiles.put(newRadical + "." + file.getType().getExtension(), file);
                }
            }
        }
    }

    /**
     * Renomme le run et deplace le dossier de run.
     *
     * @param newRunName
     * @param scenario
     * @param run
     * @return
     */
    public boolean renameRun(String newRunName, ManagerEMHScenario scenario, EMHRun run) {
        String oldName = run.getNom();
        File oldRunDir = project.getDirForRun(scenario, run);
        String auteurDerniereModif = run.getInfosVersion().getAuteurDerniereModif();
        LocalDateTime dateDerniereModif = run.getInfosVersion().getDateDerniereModif();
        //on renomme dans le fichier etu
        if (!project.renameRun(newRunName, run, connexionInformation)) return false;
        File newRunDir = project.getDirForRun(scenario, run);

        //le nouveau dossier est le meme que l'initial, on ne fait rien.
        //le nouveau dossier existe déjà
        boolean canFolderBeCopied = !newRunDir.equals(oldRunDir) && !newRunDir.exists();
        //copie du dossier
        boolean isRenamed = canFolderBeCopied && CrueFileHelper.copyDirWithSameLastDate(oldRunDir, newRunDir);
        if (!isRenamed) {
            //si échec dans la copie on annule le déplacement du dossier
            if (newRunDir.exists())
                CtuluLibFile.deleteDir(newRunDir);
            project.renameRun(oldName, run, connexionInformation);
            run.getInfosVersion().setAuteurDerniereModif(auteurDerniereModif);
            run.getInfosVersion().setDateDerniereModif(dateDerniereModif);
        } else {
            CtuluLibFile.deleteDir(oldRunDir);
        }
        return isRenamed;
    }


    public CtuluLog canUseNewRunName(String newRunName) {
        CtuluLog log = new CtuluLog();
        String err = ValidationPatternHelper.isRunNameValidei18nMessage(newRunName);
        if (err != null) {
            log.addError(err);
        }
        Set<String> allRunNameIds = project.getAllRunNameIds();
        if (allRunNameIds.contains(newRunName.toUpperCase())) {
            log.addError("validation.name.alreadyUsedShort.error");
        }
        return log;
    }

    /**
     * Vérifie la validité des nouveaux noms.
     *
     * @param newNameInformations les nouveaux noms
     * @return rapport d'erreur
     */
    public CtuluLog canUseNewNames(NewNameInformations newNameInformations) {
        CtuluLog log = new CtuluLog();

        for (Map.Entry<String, ManagerEMHContainerBase> newName : newNameInformations.newNameContainers.entrySet()) {
            if (project.getContainer(newName.getKey(), newName.getValue().getLevel()) != null) {
                log.addError(BusinessMessages.getString("RenameManager.ManagerNameUsed", newName.getKey()));
            } else {
                EnumCatEMH catEMH = CrueLevelType.getCatEMH(newName.getValue().getLevel());
                String err = ValidationPatternHelper.isNameValidei18nMessage(newName.getKey(), catEMH);
                if (err != null) {
                    log.addError(newName.getKey() + ": " + err);
                }
            }
        }

        for (Map.Entry<String, FichierCrue> newName : newNameInformations.newNameFiles.entrySet()) {
            if ((project.getInfos().getFileFromBase(newName.getKey()) != null)) {
                log.addError(BusinessMessages.getString("RenameManager.FileNameUsed", newName.getKey()));
            } else {
                String err = ValidationPatternHelper.isFilenameValidei18nErrorMessage(newName.getKey(), newName.getValue().getType());
                if (err != null) {
                    log.addError(newName.getKey() + ": " + err);
                }
            }
        }

        return log;
    }

    public List<String> getOverwrittenFiles(NewNameInformations infos) {
        List<String> path = new ArrayList<>();
        for (Entry<String, FichierCrue> entry : infos.newNameFiles.entrySet()) {
            FichierCrue old = entry.getValue();
            final String newName = entry.getKey();
            if (!newName.equals(old.getNom())) {
                FichierCrue newFile = old.clone();
                newFile.setNom(newName);
                final File file = newFile.getProjectFile(project);
                if (file.isFile()) {
                    path.add(file.getAbsolutePath());
                }
            }
        }
        return path;
    }

    public <T extends ManagerEMHContainerBase> boolean renameManager(EnumCatEMH type, ManagerEMHContainerBase manager,
                                                                     String newName) {
        if (newName.equals(manager.getNom())) {
            return true;
        }
        String oldId = manager.getId();
        boolean res = project.renameManager(type, manager, newName);
        if (res && type.equals(EnumCatEMH.SOUS_MODELE)) {
            File dirOfConfig = project.getInfos().getDirOfConfig(oldId);
            if (CtuluLibFile.exists(dirOfConfig)) {
                File dest = project.getInfos().getDirOfConfig(manager.getId());
                CtuluLibFile.deleteDir(dest);
                CrueFileHelper.copyDirWithSameLastDate(dirOfConfig, dest);
                CtuluLibFile.deleteDir(dirOfConfig);
            }
        }
        return res;
    }

    public boolean rename(NewNameInformations infos, boolean overwriteFilesIfExists) {
        boolean result = true;
        Map<String, String> newNameByOldId = new HashMap<>();
        for (Map.Entry<String, ManagerEMHContainerBase> entry : infos.newNameContainers.entrySet()) {
            String newNameOfContainer = entry.getKey();
            ManagerEMHContainerBase renamedManager = entry.getValue();
            if (renamedManager.getLevel().equals(CrueLevelType.SOUS_MODELE)) {
                newNameByOldId.put(renamedManager.getId(), newNameOfContainer.toUpperCase());
            }
        }

        for (Entry<String, ManagerEMHContainerBase> newName : infos.newNameContainers.entrySet()) {
            final ManagerEMHContainerBase manager = newName.getValue();
            final boolean renamed = this.project.renameManager(RenameManager.convert(manager.getLevel()), manager, newName.getKey());
            result &= renamed;
        }

        for (Entry<String, FichierCrue> newName : infos.newNameFiles.entrySet()) {
            result &= this.project.renameFile(newName.getValue(), newName.getKey(), overwriteFilesIfExists);
        }
        if (result) {
            for (Map.Entry<String, String> entry : newNameByOldId.entrySet()) {
                String oldFolderName = entry.getKey();
                String newFolrderName = entry.getValue();
                File dirOfConfig = project.getInfos().getDirOfConfig(oldFolderName);
                if (CtuluLibFile.exists(dirOfConfig)) {
                    File dest = project.getInfos().getDirOfConfig(newFolrderName);
                    CtuluLibFile.deleteDir(dest);
                    CrueFileHelper.copyDirWithSameLastDate(dirOfConfig, dest);
                    CtuluLibFile.deleteDir(dirOfConfig);
                }
            }
        }

        return result;
    }

    private static EnumCatEMH convert(CrueLevelType type) {
        switch (type) {
            case SCENARIO: {
                return EnumCatEMH.SCENARIO;
            }
            case MODELE: {
                return EnumCatEMH.MODELE;
            }
            case SOUS_MODELE: {
                return EnumCatEMH.SOUS_MODELE;
            }
        }

        return null;
    }
}
