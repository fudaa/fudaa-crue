/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule.function;

/**
 *
 * @author Frederic Deniger
 */
public interface ReportAggregationCacheServiceContrat {

  Double getCachedValue(AggregationCacheKey key);

  void putCachedValue(AggregationCacheKey key, Double val);
}
