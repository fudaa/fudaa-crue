/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import java.util.List;
import java.util.Set;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.projet.report.FormuleContentManager;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleContentManagerDefaultBuilder {

  private final List<FormuleParametersExpr> expressionList;
  private final ReportGlobalServiceContrat resultService;

  public FormuleContentManagerDefaultBuilder(List<FormuleParametersExpr> expr, ReportGlobalServiceContrat service) {
    this.expressionList = expr;
    this.resultService = service;
  }

  public CtuluLogResult<FormuleContentManager> create() {
    FormuleContentManagerDefault res = new FormuleContentManagerDefault();
    CtuluLog log = new CtuluLog();
    for (FormuleParametersExpr formuleParametersExpr : expressionList) {
      FormuleContent createContent = formuleParametersExpr.createContent(resultService);
      CtuluLogResult<FormuleCalculator> initCalculator = createContent.initCalculator(formuleParametersExpr, TransformerHelper.toId(expressionList));
      if (initCalculator.getLog().isNotEmpty()) {
        log.addAllLogRecord(initCalculator.getLog());
      }
      if (initCalculator.getResultat() != null) {
        Set<String> directUsedVariable = initCalculator.getResultat().getDirectUsedVariables();
        res.add(formuleParametersExpr.getNom(), initCalculator.getResultat().containsTimeDependantVariable(), directUsedVariable);
      }
    }
    if (log.isNotEmpty()) {
      log.setDesc(BusinessMessages.getString("formule.buildContent"));
    }
    return new CtuluLogResult<>(res, log);

  }
}
