/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.create;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;
import org.fudaa.dodico.crue.metier.etude.RunFile;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Utilisé pour récupérer les prédecesseur d'un fichier résultat.
 *
 * @author CANEL Christophe
 */
public class RunPredecessorFileFinder {
  private final RunFileExplorer runExplorer;
  private final ManagerEMHScenario scenario;
  private final CoeurConfigContrat coeur;

  public RunPredecessorFileFinder(CoeurConfigContrat coeur, ManagerEMHScenario scenario, Collection<RunFile> runFiles) {
    this.scenario = scenario;
    runExplorer = new RunFileExplorer(runFiles);
    this.coeur = coeur;
  }

  private void fillWithTargetFiles(CrueFileType type, ManagerEMHModeleBase modele, Set<String> dest) {
    if (type.isResultFileType()) {
      dest.add(runExplorer.getResContent(modele.getNom()).getResId(type));
      return;
    } else {
      if (type.isModeleLevel()) {
        if (CrueFileType.DREG.equals(type) && Crue10VersionConfig.isLowerOrEqualsTo_V1_2(coeur)) {
          return;
        }
        dest.add(modele.getListeFichiers().getFile(type).getNom());
        return;
      }
      if (type.isScenarioLevel()) {
        dest.add(scenario.getListeFichiers().getFile(type).getNom());
        return;
      }
      List<ManagerEMHSousModele> fils = modele.getFils();
      for (ManagerEMHSousModele managerEMHSousModele : fils) {
        dest.add(managerEMHSousModele.getListeFichiers().getFile(type).getNom());
      }
      return;
    }
  }

  public Set<String> getPredecessorFile(RunFile in) {
    Set<String> ids = new HashSet<>();
    Set<CrueFileType> inputTypeForRes = in.getType().getInputTypeForRes();

    for (CrueFileType type : inputTypeForRes) {
      fillWithTargetFiles(type, in.getContainer(), ids);
    }
    return ids;
  }

  /**
   * @param key l'identifiant du ficher
   * @return non null si l'identifiant est bien un fichier de run.
   */
  public RunFile getRunFile(String key) {
    return runExplorer.getRunFile(key);
  }
}
