/*
 GPL 2
 */
package org.fudaa.dodico.crue.edition;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;

import java.util.List;

/**
 * @author Frederic Deniger
 */
public class AlgoEMHSectionProfilEMHSectionIdemInverser {
  private final CrueConfigMetier ccm;

  public AlgoEMHSectionProfilEMHSectionIdemInverser(CrueConfigMetier ccm) {
    this.ccm = ccm;
  }

  public CtuluLog inverseSectionIdem(EMHSectionIdem sectionIdem) {
    CtuluLog ctuluLog = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CatEMHSection sectionProfil = sectionIdem.getSectionRef();

    if (sectionProfil.getSectionType() != EnumSectionType.EMHSectionProfil) {
      ctuluLog.addError(BusinessMessages.getString("validation.sectionIdem.RefTypeWrong", sectionProfil.getNom()));
      return ctuluLog;
    }

//    EMHSectionProfil
    CatEMHBranche brancheSectionIdem = sectionIdem.getBranche();
    //cette section va etre désolidarée dans la branche
    CatEMHBranche brancheSectionProfil = sectionProfil.getBranche();

    final List<EMHSectionIdem> sectionIdemList = EMHHelper.getSectionReferencing(sectionProfil);

    double xSectionIdem = EMHRelationFactory.getXP(sectionIdem);
    double xSectionRef = EMHRelationFactory.getXP(sectionProfil);

    //on fait une copie
    EMHSectionProfil copiedSectionProfil = new EditionSectionCloner(new UniqueNomFinder()).clone((EMHSectionProfil) sectionProfil);
    for (EMHSectionIdem emhSectionIdem : sectionIdemList) {
      emhSectionIdem.setSectionRef(copiedSectionProfil);
    }

    //on desolidarise les sections:
    EMHRelationFactory.removeRelationBrancheDansSectionBidirect(sectionIdem);
    EMHRelationFactory.removeRelationBrancheDansSectionBidirect(sectionProfil);

    //on inverse les appartenances:
    if (brancheSectionIdem != null) {
      final RelationEMHSectionDansBranche sectionDansBrancheAndSetRelation = EMHRelationFactory
          .createSectionDansBrancheAndSetRelation(brancheSectionIdem, copiedSectionProfil, xSectionIdem, ccm);
      brancheSectionIdem.addRelationEMH(sectionDansBrancheAndSetRelation);
    }
    if (brancheSectionProfil != null) {
      final RelationEMHSectionDansBranche sectionDansBrancheAndSetRelation = EMHRelationFactory
          .createSectionDansBrancheAndSetRelation(brancheSectionProfil, sectionIdem, xSectionRef, ccm);
      brancheSectionProfil.addRelationEMH(sectionDansBrancheAndSetRelation);
    }

    DonPrtGeoSectionIdem dptg = EMHHelper.getDPTGSectionIdem(sectionIdem);
    double dz = dptg.getDz();
    //si différent de 0, on doit faire un correctif:
    if (!ccm.getProperty(CrueConfigMetierConstants.PROP_DZ).getEpsilon().isZero(dz)) {
      for (EMHSectionIdem emhSectionIdem : sectionIdemList) {
        final DonPrtGeoSectionIdem prtGeoSectionIdem = EMHHelper.getDPTGSectionIdem(emhSectionIdem);
        prtGeoSectionIdem.setDz(prtGeoSectionIdem.getDz() - dz);
      }

      //on le refait pour la section echangée
      final DonPrtGeoSectionIdem prtGeoSectionIdem = EMHHelper.getDPTGSectionIdem(sectionIdem);
      prtGeoSectionIdem.setDz(prtGeoSectionIdem.getDz() - dz);

      final DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection(copiedSectionProfil);
      final List<PtProfil> ptProfil = profilSection.getPtProfil();
      for (PtProfil profil : ptProfil) {
        profil.setZ(profil.getZ() + dz);
      }
    }

    //on réordonne les sections dans les branches.
    if (brancheSectionIdem != null) {
      AlgoEMHSectionSorter.reorderSections(brancheSectionIdem, ccm);
    }
    if (brancheSectionProfil != null) {
      AlgoEMHSectionSorter.reorderSections(brancheSectionProfil, ccm);
    }

    return ctuluLog;
  }
}
