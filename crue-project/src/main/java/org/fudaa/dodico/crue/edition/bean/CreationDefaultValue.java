/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition.bean;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.validation.ValidationHelperProfil;

/**
 *
 * @author Frederic Deniger
 */
public class CreationDefaultValue {

  private double defaultLitMineurLength = -1;

  public double getDefaultLitMineurLength() {
    return defaultLitMineurLength;
  }

  public void setDefaultLitMineurLength(final double defaultLitMineurLength) {
    this.defaultLitMineurLength = defaultLitMineurLength;
  }

  public void checkValues(final CrueConfigMetier ccm) {
    defaultLitMineurLength = Math.max(defaultLitMineurLength, ValidationHelperProfil.getMinimalXtDelta(ccm)*2);
  }
}
