/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.persist;

import java.util.Collection;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;

/**
 *
 * @author Frederic Deniger
 */
public interface ReportConfigContrat {

  ReportConfigContrat duplicate();

  ReportConfigContrat createTemplateConfig();

  ReportContentType getType();

  Collection<ResultatTimeKey> getSelectedTimeStepInReport();

  void reinitContent();

  /**
   *
   * @param changes les modifications sur les variables.
   * @return true si modifications apportées.
   */
  boolean variablesUpdated(FormuleDataChanges changes);
}
