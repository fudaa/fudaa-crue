/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition.bean;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;

/**
 *
 * @author deniger
 */
public class ListBrancheContent extends AbstractListContent {

  public static final String PROP_NOEUD_AMONT = "noeudAmont";
  public static final String PROP_NOEUD_AVAL = "noeudAval";
  public static final String PROP_LONGUEUR = "longueur";
  String commentaire = StringUtils.EMPTY;
  String noeudAmont;
  String noeudAval;
  EnumBrancheType type;
  boolean active;
  double longueur;

  public ListBrancheContent() {
  }

  public ListBrancheContent(final String nom) {
    this.nom = nom;
  }

  public EnumBrancheType getType() {
    return type;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(final boolean active) {
    this.active = active;
  }

  public double getLongueur() {
    return longueur;
  }

  public void setLongueur(final double longueur) {
    this.longueur = longueur;
  }

  public String getNoeudAmont() {
    return noeudAmont;
  }

  public void setNoeudAmont(final String noeudAmont) {
    this.noeudAmont = noeudAmont;
  }

  public String getNoeudAval() {
    return noeudAval;
  }

  public void setNoeudAval(final String noeudAval) {
    this.noeudAval = noeudAval;
  }

  public void setType(final EnumBrancheType type) {
    this.type = type;
  }

  public String getCommentaire() {
    return commentaire;
  }

  public void setCommentaire(final String commentaire) {
    this.commentaire = commentaire;
  }
}
