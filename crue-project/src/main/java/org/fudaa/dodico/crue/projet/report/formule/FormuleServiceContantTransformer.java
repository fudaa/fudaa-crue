/*
GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.projet.report.formule.function.EnumFormuleStat;
import org.fudaa.dodico.crue.projet.report.persist.FormulesDao;

/**
 * Utiliser pour transformer des anciennes formules ( MaxSurT,...) en nouvelles.
 *
 * @author Frederic Deniger
 */
public class FormuleServiceContantTransformer {

  final List<Pair<String, String>> replacement = new ArrayList<>();

  public FormuleServiceContantTransformer() {
    addReplacement("MoySurT", EnumFormuleStat.MOY_ALL_TRANSIENT);
    addReplacement("MaxSurT", EnumFormuleStat.MAX_ALL_TRANSIENT);
    addReplacement("MinSurT", EnumFormuleStat.MIN_ALL_TRANSIENT);
    addReplacement("SommeSurT", EnumFormuleStat.SUM_ALL_TRANSIENT);
  }

  private void addReplacement(String old, EnumFormuleStat newExpr) {
    replacement.add(new Pair<>(old + "(", newExpr.getExpression() + "("));
  }

  public void process(FormulesDao dao) {
    if (dao == null) {
      return;
    }
    final List<FormuleParametersExpr> formules = dao.getFormules();
    if (formules == null) {
      return;
    }
    for (FormuleParametersExpr formule : formules) {
      formule.setFormule(processFormule(formule.getFormule()));
    }

  }

  protected String processFormule(String formule) {
    String result = formule;
    for (Pair<String, String> pair : replacement) {
      result = StringUtils.replace(result, pair.first, pair.second);
    }
    return result;

  }

}
