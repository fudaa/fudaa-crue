/*
GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule.function;

/**
 *
 * @author Frederic Deniger
 */
public enum EnumFormuleFunction {

  MOY,
  MIN,
  MAX,
  SUM

}
