/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.loi;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;

/**
 * Conteneur permettant de définir complètement un label ( unité, préfixe...)
 *
 * @author Frederic Deniger
 */
@XStreamAlias("ReportLabelContent")
public class ReportLabelContent {
  public static final ReportLabelContent NULL = new ReportLabelContent();
  /**
   * Attention, si vous modifiez le nom de la variable prefix: il faut modifier cette constante Idem pour les autres.
   */
  public static final String PROP_PREFIX = "prefix";
  public static final String PROP_CONTENT = "content";
  public static final String PROP_UNIT = "unit";
  String prefix = StringUtils.EMPTY;
  ReportVariableKey content;
  boolean unit;

  public ReportLabelContent() {

  }

  public ReportLabelContent(ReportLabelContent from) {
    if (from != null) {
      prefix = from.prefix;
      content = from.content;
      unit=from.unit;
    }
  }

  public static String geti18nPrefix() {
    return BusinessMessages.getString("labelPrefix.property");
  }

  public static String geti18nContent() {
    return BusinessMessages.getString("labelVariable.property");
  }

  public static String geti18nUnit() {
    return BusinessMessages.getString("labelUnit.property");
  }

  /**
   * @return le préfixe a utiliser
   */
  @PropertyDesc(i18n = "labelPrefix.property")
  public String getPrefix() {
    return prefix;
  }

  /**
   * @param prefix le préfixe
   */
  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  /**
   * @return le label complet avec le prefix et l'unité ( si demandé)
   */
  public String getDisplayName() {
    return StringUtils.defaultString(prefix) + (content == null ? StringUtils.EMPTY : content.geti18n()) + (unit ? " [unit]" : StringUtils.EMPTY);
  }

  public ReportLabelContent copy() {
    return new ReportLabelContent(this);
  }

  /**
   * @return la clé de la variable correspondante
   */
  @PropertyDesc(i18n = "labelVariable.property")
  public ReportVariableKey getContent() {
    return content;
  }

  /**
   * @param content la clé de la variable correspondante
   */
  public void setContent(ReportVariableKey content) {
    this.content = content;
  }

  /**
   * @return true si affichage des unités
   */
  @PropertyDesc(i18n = "labelUnit.property")
  public boolean isUnit() {
    return unit;
  }

  /**
   * @param unit true si affichage des unités
   */
  public void setUnit(boolean unit) {
    this.unit = unit;
  }
}
