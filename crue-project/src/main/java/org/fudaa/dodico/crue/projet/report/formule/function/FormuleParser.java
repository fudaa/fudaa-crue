/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule.function;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluParser;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.nfunk.jep.Node;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleParser extends CtuluParser {

  public FormuleParser() {
    addStandardFunctions();
    addStandardConstants();
    setImplicitMul(true);
    getFunctionTable().remove("sum");
  }

  @Override
  public Node parseExpression(String _expression) {
    Node res = super.parseExpression(_expression);
    if (!hasError()) {
      String lastExpr = getLastExpr();
      List<String> aggregations = EnumFormuleStat.getExpressions();
      boolean error = false;
      for (String string : aggregations) {
        if (lastExpr.contains(string + "(")) {
          String[] splitByWholeSeparatorPreserveAllTokens = StringUtils.splitByWholeSeparatorPreserveAllTokens(lastExpr, string);
          if (splitByWholeSeparatorPreserveAllTokens.length > 1) {
            for (int i = 1; i < splitByWholeSeparatorPreserveAllTokens.length; i++) {
              String token = splitByWholeSeparatorPreserveAllTokens[i].trim();
              if (!token.startsWith("(\"")) {
                error = true;
                break;
              }
            }
          }
        }
      }
      if (error) {
        super.errorList.add(BusinessMessages.getString("aggregation.variableNotCorrect"));
      }
    }
    return res;
  }
}
