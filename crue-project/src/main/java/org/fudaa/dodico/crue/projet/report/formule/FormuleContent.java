/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

import java.util.Collection;

/**
 * Classe stockant les paramètes {@link FormuleParameters} d'une expression et le calculateur {@link FormuleCalculator}
 *
 * @author Frederic Deniger
 */
public abstract class FormuleContent<P extends FormuleParameters, C extends FormuleCalculator> implements Comparable<FormuleContent>, ObjetWithID {
    private P parameters;
    private C calculator;

    /**
     * @param parameters les {@link FormuleParameters}
     * @param variableFC les noms des variables
     * @return le {@link FormuleCalculator} correspondant au @{@link FormuleParameters} donné
     */
    public abstract CtuluLogResult<C> initCalculator(P parameters, Collection<String> variableFC);

    /**
     * @param parameters les nouveaux paramètres
     * @param variableFC les variables disponibles dans le run
     * @return résultat de l'opération
     */
    public CtuluLog setParameter(P parameters, Collection<String> variableFC) {
        this.parameters = parameters;
        calculator = null;
        CtuluLogResult<C> initCalculator = initCalculator(parameters, variableFC);
        if (initCalculator.getResultat() != null) {
            calculator = initCalculator.getResultat();
        }
        return initCalculator.getLog();
    }

    @Override
    public String getId() {
        return getParameters().getId();
    }

    @Override
    public String getNom() {
        return getParameters().getNom();
    }

    @Override
    public int compareTo(FormuleContent o) {
        if (o == this) {
            return 0;
        }
        if (o == null) {
            return 1;
        }
        return SafeComparator.compareString(parameters.getNom(), o.getParameters().getNom());
    }

    /**
     * @return le calculateur
     */
    public C getCalculator() {
        return calculator;
    }

    /**
     * @return les parametres
     */
    public P getParameters() {
        return parameters;
    }

    /**
     * @return le type de variable qui doit être défini dans le CCM.
     */
    public abstract String getPropertyNature();

    /**
     * @see FormuleParameters#getDescription()
     */
    String getDescription() {
        return parameters == null ? "?"
                : parameters.getDescription();
    }

    /**
     * @param cyclique true si l'expression devient cyclique.
     */
    public void setCyclique(boolean cyclique) {
        getCalculator().setCyclique(cyclique);
    }
}
