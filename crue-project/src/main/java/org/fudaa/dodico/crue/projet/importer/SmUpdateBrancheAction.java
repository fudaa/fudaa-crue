package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.EditionBrancheCreator;
import org.fudaa.dodico.crue.edition.EditionChangeBranche;
import org.fudaa.dodico.crue.edition.EditionCreateEMH;
import org.fudaa.dodico.crue.edition.bean.BrancheEditionContent;
import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateSourceTargetData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SmUpdateBrancheAction {
  private final CrueConfigMetier ccm;
  private final EMHSousModele targetSousModele;
  private final EMHScenario targetScenario;

  public SmUpdateBrancheAction(CrueConfigMetier ccm, EMHSousModele targetSousModele, EMHScenario targetScenario) {
    this.ccm = ccm;
    this.targetSousModele = targetSousModele;
    this.targetScenario = targetScenario;
  }

  CtuluLog addBranches(List<CatEMHBranche> sourceEmhsToAdd) {
    CtuluLog logAddBranches = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    logAddBranches.setDesc("sm.updater.action.addBrancheLog");
    final Map<String, EMH> targetEmhByNom = targetScenario.getIdRegistry().getEmhByNom();
    sourceEmhsToAdd.forEach(sourceBranche -> {
      final CatEMHBranche targetBranche = EditionBrancheCreator.createSimpleBranche(sourceBranche.getNom(), sourceBranche.getBrancheType());
      synchronizeInfosEMH(sourceBranche, targetBranche);
      updateBrancheRelations(targetEmhByNom, sourceBranche, targetBranche);
      targetBranche.setUserActive(sourceBranche.getUserActive());
      EditionCreateEMH.addInScenario(targetSousModele, targetBranche, targetScenario);
    });
    return logAddBranches;
  }

  private void synchronizeInfosEMH(CatEMHBranche sourceBranche, CatEMHBranche targetBranche) {
    targetBranche.removeInfosEMH(targetBranche.getDCSP());
    targetBranche.removeInfosEMH(targetBranche.getDPTG());
    sourceBranche.getDCSP().forEach(dcsp -> targetBranche.addInfosEMH(dcsp.cloneDCSP()));
    sourceBranche.getDPTG().forEach(donCalcSansPrt -> targetBranche.addInfosEMH(donCalcSansPrt.cloneDPTG()));
  }

  CtuluLog updateBranches(List<SmUpdateSourceTargetData.Line<CatEMHBranche>> branchesLines) {
    CtuluLog logUpdateBranches = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    logUpdateBranches.setDesc("sm.updater.action.updateBrancheLog");
    final Map<String, EMH> targetEmhByNom = targetScenario.getIdRegistry().getEmhByNom();
    branchesLines.forEach(brancheLine -> {
      CatEMHBranche targetBranche = brancheLine.getTargetToModify();
      final CatEMHBranche sourceBranche = brancheLine.getSourceToImport();
      if (sourceBranche.getBrancheType().equals(targetBranche.getBrancheType())) {
        if (SmUpdateSeuilHelper.isSeuil(targetBranche.getBrancheType())) {
          //si les coeff ont changé on récupère ceux de la source
          if (new SmUpdateSeuilHelper(ccm).willCoefBeChanged(brancheLine)) {
            targetBranche.removeInfosEMH(targetBranche.getDCSP());
            CatEMHBranche finalTargetBranche = targetBranche;
            sourceBranche.getDCSP().forEach(dcsp -> finalTargetBranche.addInfosEMH(dcsp.cloneDCSP()));
          }
        }
      } else {
        EditionChangeBranche changeBranche = new EditionChangeBranche(new CreationDefaultValue(), true);
        BrancheEditionContent newContent = new BrancheEditionContent();
        newContent.setDistance(sourceBranche.getLength());
        newContent.setType(sourceBranche.getBrancheType());
        newContent.setCommentaire(targetBranche.getCommentaire());
        newContent.setNoeudAmont(sourceBranche.getNoeudAmontNom());
        newContent.setNoeudAval(sourceBranche.getNoeudAvalNom());
        newContent.setActive(sourceBranche.getUserActive());
        newContent.setNom(targetBranche.getNom());
        //change type
        changeBranche.changeBranche(targetBranche, newContent, ccm, false);
        targetBranche = (CatEMHBranche) targetScenario.getIdRegistry().getEmh(targetBranche.getUiId());
        synchronizeInfosEMH(sourceBranche, targetBranche);
      }
      targetBranche.setUserActive(sourceBranche.getUserActive());
      updateBrancheRelations(targetEmhByNom, sourceBranche, targetBranche);
    });
    return logUpdateBranches;
  }

  private void updateBrancheRelations(Map<String, EMH> targetEmhByNom, CatEMHBranche sourceBranche, CatEMHBranche targetBranche) {

    // on supprime toutes les relations existantes
    if (targetBranche.getNoeudAmont() != null) {
      EMHRelationFactory.removeRelationBidirect(targetBranche, targetBranche.getNoeudAmont());
    }
    if (targetBranche.getNoeudAval() != null) {
      EMHRelationFactory.removeRelationBidirect(targetBranche, targetBranche.getNoeudAval());
    }
    //on met à jour les relations
    final EMH noeudAmont = targetEmhByNom.get(sourceBranche.getNoeudAmontNom());
    final EMH noeudAval = targetEmhByNom.get(sourceBranche.getNoeudAvalNom());
    targetBranche.setNoeudAmont((CatEMHNoeud) noeudAmont);
    targetBranche.setNoeudAval((CatEMHNoeud) noeudAval);

    boolean sectionToBeChanged = !new SmUpdateBrancheSameRelationSectionPredicate(ccm).isSameSectionsRelations(sourceBranche, targetBranche);

    if (sectionToBeChanged) {
      final List<RelationEMHSectionDansBranche> oldTargetBrancheSections = targetBranche.getSections();
      oldTargetBrancheSections.forEach(relationEMHSectionDansBranche -> EMHRelationFactory.removeRelationBidirect(targetBranche, relationEMHSectionDansBranche.getEmh()));
      final Map<String, RelationEMHSectionDansBranche> oldRelationBySectionName = TransformerHelper.toMapOfNom(oldTargetBrancheSections);
      //EMHSectionPilote
      CatEMHSection targetSectionPilote = EMHRelationFactory.removeSectionPilote(targetBranche);
      //si la source contient une section pilote, on prend
      if(targetSectionPilote==null && EMHHelper.getSectionPilote(sourceBranche)!=null){
        targetSectionPilote= (CatEMHSection) targetEmhByNom.get(EMHHelper.getSectionPilote(sourceBranche).getNom());
      }

      final List<RelationEMHSectionDansBranche> sourceListSection = sourceBranche.getListeSections();
      final List<RelationEMHSectionDansBranche> targetListSection = new ArrayList<>();
      sourceListSection.forEach(sourceRelationEMH -> {
        RelationEMHSectionDansBranche cloned = sourceRelationEMH.cloneValuesOnly();
        final CatEMHSection catEMHSection = (CatEMHSection) targetEmhByNom.get(sourceRelationEMH.getEmhNom());
        final RelationEMHSectionDansBranche oldRelation = oldRelationBySectionName.get(catEMHSection.getNom());
        if (oldRelation != null) {
          cloned.initAdditionnalData(oldRelation.getAdditionalData());
        }
        if (catEMHSection.getBranche() != null) {
          EMHRelationFactory.removeRelationBidirect(catEMHSection, catEMHSection.getBranche());
        }
        EMHRelationFactory.addBrancheContientSection(targetBranche, catEMHSection);
        cloned.setEmh(catEMHSection);
        targetListSection.add(cloned);
      });
      targetBranche.addListeSections(targetListSection);
      targetBranche.sortRelationSectionDansBranche(ccm);

      //on
      if (targetSectionPilote != null) {
        EMHRelationFactory.addSectionPilote(targetBranche, targetSectionPilote);
      }
    }
  }
}
