/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.persist;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;
import org.fudaa.dodico.crue.projet.report.data.ExternFileKey;
import org.fudaa.dodico.crue.projet.report.data.ReportKeyContract;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;
import org.fudaa.dodico.crue.projet.report.loi.ViewCourbeConfig;
import org.fudaa.ebli.courbe.EGAxeHorizontalPersist;
import org.fudaa.ebli.courbe.EGAxeVerticalPersist;
import org.fudaa.ebli.courbe.EGCourbePersist;

/**
 * @author Frederic Deniger
 */
public abstract class AbstractReportCourbeConfig implements ReportConfigContrat {

  @XStreamAlias("Axe-Horizontal")
  private EGAxeHorizontalPersist horizontalPersist;
  @XStreamAlias("Axes-Verticaux")
  private Map<String, EGAxeVerticalPersist> axeByVariable = new HashMap<>();
  @XStreamImplicit(itemFieldName = "Fichier-Externe")
  private List<String> externFiles = new ArrayList<>();
  @XStreamAlias("Fichier-Externe-Configs")
  private Map<ExternFileKey, EGCourbePersist> externFileCourbeConfig = new HashMap<>();

  public AbstractReportCourbeConfig(){

  }

  public AbstractReportCourbeConfig(AbstractReportCourbeConfig from){
    if(from==null){
      return;
    }

    if (from.externFiles != null) {
      externFiles.addAll(from.externFiles);
    }
    horizontalPersist = null;
    if (from.horizontalPersist != null) {
      horizontalPersist = from.horizontalPersist.copy();
    }
    if (from.axeByVariable != null) {
      from.axeByVariable.forEach((key, value) -> axeByVariable.put(key, value.copy()));
    }
    if (from.externFileCourbeConfig != null) {
      from.externFileCourbeConfig.forEach((key, value) -> externFileCourbeConfig.put(key, value.duplicate()));
    }
  }
  public void cleanUnusedConfig(Set<ReportKeyContract> usedKeys, Set<String> usedAxeV) {
    cleanUnusedExternConfig(usedKeys);
    for (Iterator<String> it = axeByVariable.keySet().iterator(); it.hasNext(); ) {
      String string = it.next();
      if (!usedAxeV.contains(string)) {
        it.remove();
      }
    }
    cleanUnusedExternConfig(usedKeys);
    cleanUnusedCourbesConfig(usedKeys);
  }

  @Override
  public void reinitContent() {
    if (externFiles == null) {
      externFiles = new ArrayList<>();
    }
    if (axeByVariable == null) {
      axeByVariable = new HashMap<>();
    }
    if (externFileCourbeConfig == null) {
      externFileCourbeConfig = new HashMap<>();
    }
    externFiles.remove(null);
    axeByVariable.remove(null);
    externFileCourbeConfig.remove(ExternFileKey.NULL);
  }

  protected abstract void cleanUnusedCourbesConfig(Set<ReportKeyContract> usedKeys);

  protected void cleanUnusedExternConfig(Set<ReportKeyContract> usedKeys) {
    for (Iterator<ExternFileKey> it = externFileCourbeConfig.keySet().iterator(); it.hasNext(); ) {
      if (!usedKeys.contains(it.next())) {
        it.remove();
      }
    }
  }

  public boolean isNoZoomSet() {
    return !isZoomSet();
  }

  public boolean isZoomSet() {
    if (horizontalPersist != null && horizontalPersist.getRange() != null) {
      return true;
    }
    if (axeByVariable != null) {
      Collection<EGAxeVerticalPersist> values = axeByVariable.values();
      for (EGAxeVerticalPersist eGAxeVerticalPersist : values) {
        if (eGAxeVerticalPersist.getRange() != null && !eGAxeVerticalPersist.getRange().isNill()) {
          return true;
        }
      }
    }
    return false;
  }

  protected void clearExternFiles() {
    if (externFiles != null) {
      externFiles.clear();
    }
  }

  protected void clearZooms() {
    if (horizontalPersist != null) {
      horizontalPersist.setRange(null);
    }
    Collection<EGAxeVerticalPersist> values = axeByVariable.values();
    for (EGAxeVerticalPersist eGAxeVerticalPersist : values) {
      eGAxeVerticalPersist.setRange(null);

    }
  }

  public boolean axeVariablesUpdated(FormuleDataChanges changes) {
    boolean modified = false;
    Map<String, EGAxeVerticalPersist> toAdd = new HashMap<>();
    for (Iterator<Entry<String, EGAxeVerticalPersist>> it = axeByVariable.entrySet().iterator();
         it.hasNext(); ) {
      Entry<String, EGAxeVerticalPersist> entry = it.next();
      String varName = entry.getKey();
      EGAxeVerticalPersist axePersist = entry.getValue();
      if (changes.isRenamed(varName)) {
        it.remove();
        toAdd.put(changes.getNewName(varName), axePersist);
        modified = true;
      } else if (changes.isRemoved(varName)) {
        it.remove();
        modified = true;
      }

    }
    axeByVariable.putAll(toAdd);
    return modified;
  }

  public EGCourbePersist getExternFileCourbeConfig(ExternFileKey key) {
    return externFileCourbeConfig.get(key);
  }

  public void saveAxeH(EGAxeHorizontalPersist horizontalPersist) {
    this.horizontalPersist = horizontalPersist;
  }

  public EGAxeHorizontalPersist getHorizontalPersist() {
    return horizontalPersist;
  }

  public void saveExternKeyConfig(ExternFileKey key, EGCourbePersist persist) {
    externFileCourbeConfig.put(key, persist);
  }

  public List<String> getExternFiles() {
    return Collections.unmodifiableList(externFiles);
  }

  public EGAxeVerticalPersist getAxeVConfig(ItemVariable property) {
    return getAxeVConfig(property.getNature());
  }

  public EGAxeVerticalPersist getAxeVConfig(PropertyNature nature) {
    return axeByVariable.get(nature.getNom());
  }

  public EGAxeVerticalPersist getAxeVConfig(String natureName) {
    return axeByVariable.get(natureName);
  }

  public void saveAxeVConfig(ItemVariable property, EGAxeVerticalPersist persist) {
    saveAxeVConfig(property.getNature(), persist);
  }

  public void saveAxeVConfig(PropertyNature nature, EGAxeVerticalPersist persist) {
    axeByVariable.put(nature.getNom(), persist);
  }

  public void saveAxeVConfig(String natureName, EGAxeVerticalPersist persist) {
    axeByVariable.put(natureName, persist);
  }

  @Override
  public abstract ReportContentType getType();

  public abstract String getMainEMHName();

  @Override
  public Collection<ResultatTimeKey> getSelectedTimeStepInReport() {
    return ReportResultProviderServiceContrat.NO_SELECTED_TIMES_REPORT;
  }

  public abstract ViewCourbeConfig getLoiLegendConfig();


  public void setExternalFiles(List<String> files) {
    externFiles.clear();
    if (files != null) {
      externFiles.addAll(files);
    }
  }
}
