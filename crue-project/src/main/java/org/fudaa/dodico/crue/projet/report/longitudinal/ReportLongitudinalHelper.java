/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.longitudinal;

import java.util.List;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.lit.LitNommeLimite;
import org.fudaa.dodico.crue.metier.helper.LitNommeIndexed;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLongitudinalHelper {

  public static int finPtIdx(
          CrueIOResu<List<LitNommeIndexed>> asIndexedData, LitNommeLimite limite) {
    List<LitNommeIndexed> metier = asIndexedData.getMetier();
    if (limite.getIdx() < metier.size()) {
      return metier.get(limite.getIdx()).getFirstIdx();
    } else {
      return metier.get(metier.size() - 1).getlastIdx();
    }
  }

}
