/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.create;

import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;

import java.util.ArrayList;
import java.util.List;

/**
 * @author deniger
 */
public class ContainerLevelData {
    private final List<ContainerLevelData> children = new ArrayList<>();
    private final List<FichierCrue> files = new ArrayList<>();
    private final CrueLevelType levelType;
    private final String name;

    /**
     * @param name      nom du conteneur
     * @param levelType le level
     */
    public ContainerLevelData(String name, CrueLevelType levelType) {
        super();
        this.name = name;
        this.levelType = levelType;
    }

    public void addChildren(ContainerLevelData data) {
        children.add(data);
    }

    /**
     * @param crueLevelType le level
     * @return tous les levelData dont le type est type
     */
    public List<ContainerLevelData> getLevel(CrueLevelType crueLevelType) {
        List<ContainerLevelData> levels = new ArrayList<>();

        this.addDataToList(this, levels, crueLevelType);

        return levels;
    }

    private void addDataToList(ContainerLevelData data, List<ContainerLevelData> levels, CrueLevelType type) {
        if (data.levelType == type) {
            levels.add(data);
        }

        for (ContainerLevelData child : data.children) {
            this.addDataToList(child, levels, type);
        }
    }

    public void addFichierCrue(FichierCrue fichierCrue) {
        files.add(fichierCrue);
    }

    /**
     * @return the levelType
     */
    public CrueLevelType getLevelType() {
        return levelType;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the children
     */
    public List<ContainerLevelData> getChildren() {
        return children;
    }

    /**
     * @return the files
     */
    public List<FichierCrue> getFiles() {
        return files;
    }
}
