package org.fudaa.dodico.crue.edition;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.edition.bean.BrancheInsertContent;
import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.dodico.crue.validation.util.DelegateValidatorBranche;

import java.util.List;

/**
 * Permet d'insérer une branche
 *
 * @author deniger
 */
public class EditionBrancheInsert {
  private final CrueConfigMetier crueConfigMetier;

  public EditionBrancheInsert(final CrueConfigMetier crueConfigMetier) {
    this.crueConfigMetier = crueConfigMetier;
  }

  /**
   * @param section la section qui sera le point de découpage ( deviendra la section aval de la branche amont)
   * @return le contenu permettant à l'utilisateur de prévisualiser les modifications
   */
  public CrueOperationResult<BrancheInsertContent> createContent(final CatEMHSection section) {

    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("EditionBrancheInsert.CreateContent.LogContent");
    //on valide d'abord la faisabilité
    final CatEMHNoeud noeud = validSplit(section, log);
    if (log.containsErrorOrSevereError() || noeud == null) {
      return new CrueOperationResult<>(null, log);
    }
    return createContent(noeud);
  }

  public CrueOperationResult<BrancheInsertContent> createContent(final CatEMHNoeud noeud) {
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("EditionBrancheInsert.CreateContent.LogContent");
    final BrancheInsertContent content = new BrancheInsertContent();
    final UniqueNomFinder uniqueNomFinder = new UniqueNomFinder();
    final EMHScenario scenario = EMHHelper.getScenario(noeud);

    content.newBrancheName = uniqueNomFinder.findNewName(EnumCatEMH.BRANCHE, scenario);
    content.newNoeudName = uniqueNomFinder.findNewName(EnumCatEMH.NOEUD, scenario);
    content.neoudNameToSplitOn = noeud.getNom();
    return new CrueOperationResult<>(content, log);
  }

  private CatEMHNoeud validSplit(final CatEMHSection section, final CtuluLog log) {
    final CatEMHBranche branche = section.getBranche();
    if (branche == null) {
      log.addError("EditionBrancheInsert.SectionMustBeAvalOrAmont", section.getNom());
      return null;
    }
    final RelationEMHSectionDansBranche sectionAval = branche.getSectionAval();
    final RelationEMHSectionDansBranche sectionAmont = branche.getSectionAmont();
    if (section.getId().equals(sectionAmont.getEmhId())) {
      return branche.getNoeudAmont();
    }
    if (section.getId().equals(sectionAval.getEmhId())) {
      return branche.getNoeudAval();
    }
    log.addError("EditionBrancheInsert.SectionMustBeAvalOrAmont", section.getNom());
    return null;
  }

  /**
   * @param scenario le scenario concerné
   * @param insertContent les données d'insertion
   * @return le resultat de l'operaiont
   */
  public CtuluLog insert(final EMHScenario scenario, final BrancheInsertContent insertContent) {
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);

    //on valide
    final CatEMHNoeud noeud = (CatEMHNoeud) scenario.getIdRegistry().findByName(insertContent.neoudNameToSplitOn);

    validSplitAction(scenario, noeud, insertContent, log);
    if (log.containsErrorOrSevereError()) {
      return log;
    }
    final EMHSousModele emhSousModele = EMHHelper.getParent(noeud);

    final EditionCreateEMH creator = new EditionCreateEMH(new CreationDefaultValue());
    final CatEMHNoeud createdNoeud = creator.createNoeud(insertContent.newNoeudName, emhSousModele, EnumNoeudType.EMHNoeudNiveauContinu, crueConfigMetier);
    if (insertContent.addAval) {
      final List<CatEMHBranche> branchesAval = noeud.getBranchesAval();
      final CatEMHBranche catEMHBranche = creator
          .createBranche(insertContent.newBrancheName, emhSousModele, insertContent.newBrancheType, noeud, createdNoeud, crueConfigMetier, insertContent.distance);
      catEMHBranche.setUserActive(noeud.getActuallyActive());
      branchesAval.forEach(branche -> {
        EMHRelationFactory.removeRelationBidirect(branche, branche.getNoeudAmont());
        branche.setNoeudAmont(createdNoeud);
      });
    } else {
      final List<CatEMHBranche> branchesAmont = noeud.getBranchesAmont();
      final CatEMHBranche catEMHBranche = creator
          .createBranche(insertContent.newBrancheName, emhSousModele, insertContent.newBrancheType, createdNoeud, noeud, crueConfigMetier, insertContent.distance);
      catEMHBranche.setUserActive(noeud.getActuallyActive());
      branchesAmont.forEach(branche -> {
        EMHRelationFactory.removeRelationBidirect(branche, branche.getNoeudAval());
        branche.setNoeudAval(createdNoeud);
      });
    }

    return log;
  }

  /**
   * Permet de valider l'action de découpage.
   *
   * @param scenario le scenario
   * @param emhNoeud la emhNoeud support de l'insertion
   * @param insertContent les données du découpage
   * @param log le log qui contiendra les résultats de validation
   */
  private void validSplitAction(final EMHScenario scenario, final CatEMHNoeud emhNoeud, final BrancheInsertContent insertContent,
                                final CtuluLog log) {
    if (emhNoeud == null) {
      log.addError("BrancheInsertContent.NoeudToInsertNotFound", insertContent.neoudNameToSplitOn);
    }
    final String newBrancheNameValidation = ValidationPatternHelper.isNameValidei18nMessage(insertContent.newBrancheName, EnumCatEMH.BRANCHE);
    if (newBrancheNameValidation != null) {
      log.addError(newBrancheNameValidation);
    }
    final String newNoeudNameValidation = ValidationPatternHelper.isNameValidei18nMessage(insertContent.newNoeudName, EnumCatEMH.NOEUD);
    if (newNoeudNameValidation != null) {
      log.addError(newNoeudNameValidation);
    }

    if (scenario.getIdRegistry().findByName(insertContent.newBrancheName) != null) {
      log.addError("EditionBrancheSplit.BrancheNameAlreadyExists", insertContent.newBrancheName);
    }
    if (scenario.getIdRegistry().findByName(insertContent.newNoeudName) != null) {
      log.addError("EditionBrancheSplit.NoeudNameAlreadyExists", insertContent.newNoeudName);
    }

    if (insertContent.newBrancheType == null) {
      log.addError("BrancheInsertContent.BrancheTypeNotDefined");
    }
    final List<EnumBrancheType> brancheTypeWithDistanceNotNull = DelegateValidatorBranche.getBrancheTypeWithDistanceNotNull();
    final PropertyEpsilon epsilon = crueConfigMetier.getEpsilon(CrueConfigMetierConstants.PROP_XP);
    if (brancheTypeWithDistanceNotNull.contains(insertContent.newBrancheType)) {
      if (epsilon.isZero(insertContent.distance)) {
        log.addError("validation.branche.distanceMustNotBeNull", insertContent.newBrancheType.geti18n());
      }
    } else if (!epsilon.isZero(insertContent.distance)) {
      log.addError("validation.branche.distanceMustBeNull", insertContent.newBrancheType.geti18n());
    }
  }
}
