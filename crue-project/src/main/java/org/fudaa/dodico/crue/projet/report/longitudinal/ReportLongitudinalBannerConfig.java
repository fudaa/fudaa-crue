/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.longitudinal;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.trace.TraceBox;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;

import javax.swing.*;
import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("BrancheConfig-Affichage")
public class ReportLongitudinalBannerConfig {

  @XStreamAlias("Branche-Visible")
  private boolean brancheVisible = true;
  @XStreamAlias("Branche-Boite")
  private TraceBox brancheBox = new TraceBox();
  private Font brancheFont = CtuluLibSwing.getMiniFont();
  @XStreamAlias("Section-Visible")
  private boolean sectionVisible = true;
  @XStreamAlias("Section-Police")
  private Font sectionFont = CtuluLibSwing.getMiniFont();
  @XStreamAlias("Section-Boite")
  private TraceBox sectionBox = new TraceBox();
  @XStreamAlias("Espace-Vertical")
  private int verticalSpace = 5;
  @XStreamAlias("Section-Amont-Visible-Only")
  private boolean sectionAmontAvalOnly = false;

  public ReportLongitudinalBannerConfig() {
    createBrancheBox();
    createSectionBox();
  }

  public void reinit() {
    if (sectionFont == null) {
      sectionFont = CtuluLibSwing.getMiniFont();
    }
    if (brancheFont == null) {
      brancheFont = CtuluLibSwing.getMiniFont();
    }
    try {
      if (brancheBox == null) {
        createBrancheBox();
      }
      if (sectionBox == null) {
        createSectionBox();
      }
    } catch (Exception e) {
      Logger.getLogger(ReportLongitudinalBannerConfig.class.getName()).log(Level.SEVERE, "message {0}", e);
    }
  }

  public Color getSectionFontColor() {
    return sectionBox.getColorText();
  }

  public TraceLigneModel getBrancheBoxLine() {
    return brancheBox.getTraceLigne().getModel();
  }

  public void setBrancheBoxLine(TraceLigneModel model) {
    brancheBox.getTraceLigne().setModel(model);
  }

  public TraceLigneModel getSectionBoxLine() {
    return sectionBox.getTraceLigne().getModel();
  }

  public void setSectionBoxLine(TraceLigneModel model) {
    sectionBox.getTraceLigne().setModel(model);
  }

  public void setSectionFontColor(Color c) {
    sectionBox.setColorText(c);
  }

  public Color getBrancheFontColor() {
    return brancheBox.getColorText();
  }

  public void setBrancheFontColor(Color c) {
    brancheBox.setColorText(c);
  }

  public ReportLongitudinalBannerConfig duplicate() {

    ReportLongitudinalBannerConfig copy=new ReportLongitudinalBannerConfig();
    copy.brancheVisible=brancheVisible;
    if(brancheBox!=null){
      copy.brancheBox=new TraceBox(brancheBox);
    }
    if(sectionBox!=null){
      copy.sectionBox=new TraceBox(sectionBox);
    }
    copy.brancheFont=brancheFont;
    copy.sectionVisible=sectionVisible;
    copy.sectionFont=sectionFont;
    copy.verticalSpace=verticalSpace;
    copy.sectionAmontAvalOnly=sectionAmontAvalOnly;

    return copy;
  }

  public Font getBrancheFont() {
    return brancheFont;
  }

  public void setBrancheFont(Font brancheFont) {
    this.brancheFont = brancheFont;
  }

  public Font getSectionFont() {
    return sectionFont;
  }

  public void setSectionFont(Font sectionFont) {
    this.sectionFont = sectionFont;
  }

  public int getVerticalSpace() {
    return verticalSpace;
  }

  public boolean isSectionAmontAvalOnly() {
    return sectionAmontAvalOnly;
  }

  public void setSectionAmontAvalOnly(boolean sectionAmontAvalOnly) {
    this.sectionAmontAvalOnly = sectionAmontAvalOnly;
  }

  public void setVerticalSpace(int verticalSpace) {
    this.verticalSpace = verticalSpace;
  }

  public TraceBox getBrancheBox() {
    return brancheBox;
  }

  public void setBrancheBox(TraceBox brancheBox) {
    this.brancheBox = brancheBox;
  }

  public TraceBox getSectionBox() {
    return sectionBox;
  }

  public void setSectionBox(TraceBox sectionBox) {
    this.sectionBox = sectionBox;
  }

  public boolean isBrancheVisible() {
    return brancheVisible;
  }

  public void setBrancheVisible(boolean brancheVisible) {
    this.brancheVisible = brancheVisible;
  }

  public boolean isSectionVisible() {
    return sectionVisible;
  }

  public void setSectionVisible(boolean sectionVisible) {
    this.sectionVisible = sectionVisible;
  }

  private void createBrancheBox() {
    if (brancheBox == null) {
      brancheBox = new TraceBox();
    }
    brancheBox.setTraceLigne(new TraceLigne(TraceLigne.LISSE, 1, Color.GRAY));
    brancheBox.setVPosition(SwingConstants.TOP);
    brancheBox.setVMargin(2);
    brancheBox.setHPosition(SwingConstants.CENTER);
    brancheBox.setDrawBox(false);
    brancheBox.setDrawFond(false);
  }

  private void createSectionBox() {
    if (sectionBox == null) {
      sectionBox = new TraceBox();
    }
    sectionBox.setTraceLigne(new TraceLigne(TraceLigne.LISSE, 1, Color.LIGHT_GRAY));
    sectionBox.setColorBoite(Color.LIGHT_GRAY);
    sectionBox.setVPosition(SwingConstants.TOP);
    sectionBox.setHPosition(SwingConstants.CENTER);
    sectionBox.setVMargin(3);
    sectionBox.setDrawBox(true);
    sectionBox.setDrawFond(false);
    sectionBox.setVertical(true);
  }
}
