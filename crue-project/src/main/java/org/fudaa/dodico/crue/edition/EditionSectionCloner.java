package org.fudaa.dodico.crue.edition;

import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;

import java.util.List;

/**
 * Permet de cloner des sections.
 * @author deniger
 */
public class EditionSectionCloner {
  private final UniqueNomFinder uniqueNomFinder;

  public EditionSectionCloner(UniqueNomFinder uniqueNomFinder) {
    this.uniqueNomFinder = uniqueNomFinder;
  }
  /**
   * Clone le profil donne en paramètre avec le prefix _Copie et l'ajoute au meme sous-modele
   *
   * @param profil le profile a cloner
   * @return le profil cloné
   */
  public  EMHSectionProfil clone(EMHSectionProfil profil) {
    return clone(profil, "_Copie");
  }

  /**
   * Clone le profil donne en paramètre et l'ajoute au meme sous-modele
   *
   * @param profil le profile a cloner
   * @param suffixe le suffixe pour le nouveau nom
   * @return le profil cloné
   */
  public EMHSectionProfil clone(EMHSectionProfil profil, String suffixe) {
    final EMHSousModele emhSousModele = profil.getParent();
    final EMHScenario scenario = emhSousModele.getParent().getParent();
    final String newName = uniqueNomFinder.findUniqueName(EnumCatEMH.SECTION, scenario, profil.getNom() + suffixe);
    EMHSectionProfil cloned = new EMHSectionProfil(newName);
    final List<InfosEMH> infosEMH = profil.getInfosEMH();
    DonPrtGeoProfilSection profilSection = EMHHelper.selectFirstOfClass(infosEMH, DonPrtGeoProfilSection.class);
    String newProfilName = uniqueNomFinder.findUniqueNameProfilSection(profilSection.getNom() + suffixe, scenario);
    DonPrtGeoProfilSection clonedProfil = new DonPrtGeoProfilSection();
    clonedProfil.initDataFrom(profilSection);
    clonedProfil.setNom(newProfilName);
    cloned.addInfosEMH(clonedProfil);
    DonPrtCIniSection cini = EMHHelper.selectFirstOfClass(infosEMH, DonPrtCIniSection.class);
    if (cini != null) {
      cloned.addInfosEMH(cini.deepClone());
    }
    EditionCreateEMH.addInScenario(emhSousModele, cloned, scenario);
    return cloned;
  }
}
