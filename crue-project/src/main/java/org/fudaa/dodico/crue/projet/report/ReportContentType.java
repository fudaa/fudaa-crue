/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

/**
 * @author Frederic Deniger
 */
public enum ReportContentType implements ToStringInternationalizable {

    TRANSVERSAL("Transveral.Type", "transversal"), LONGITUDINAL("ProfilEnLong.Type", "longitudinal"), TEMPORAL("Temporel.Type", "temporel"), MULTI_VAR(
            "MultiVar.Type", "multivar"), RPTG("RPTG.Type", "pretraitement"),
    PLANIMETRY("Planimetry.Type", "planimetrie");
    private final String i18n;
    private final String folderName;

    ReportContentType(String i18n, String folderName) {
        this.i18n = i18n;
        this.folderName = folderName;
    }

    /**
     *
     * @return true si la vuee est de type planimetrique
     */
    public boolean isPlanimetry() {
        return PLANIMETRY.folderName.equals(folderName);
    }

    /**
     *
     * @return le nom du folder contenant les fichiers de configuration
     */
    public String getFolderName() {
        return folderName;
    }

    @Override
    public String geti18n() {
        return BusinessMessages.getStringOrDefault(i18n, i18n);
    }

    @Override
    public String geti18nLongName() {
        return geti18n();
    }
}
