/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;

import java.util.Collection;
import java.util.Set;

/**
 * Permet de calculer la valeur d'une expression
 * @author Frederic Deniger
 */
public interface FormuleCalculator {

  Double getValue(ResultatTimeKey currentSelectedTime, ReportRunVariableKey key, String emhNom, Collection<ResultatTimeKey> selectedTimeInRapport);

  /**
   * @return les variables utilisées par ce calcul
   */
  Set<String> getDirectUsedVariables();

  /**
   *
   * @return true si des variables dépendent du temps sélectionné
   */
  boolean containsTimeDependantVariable();

  /**
   * @param selectedTimes les temps sélectionnées
   * @return true si la formule est valide. Si pas une formule renvoie true
   */
  boolean isExpressionValid(Collection<ResultatTimeKey> selectedTimes);

  /**
   *
   * @param b si true, le calcul ne doit pas être effectuée.
   */
  void setCyclique(boolean b);
}
