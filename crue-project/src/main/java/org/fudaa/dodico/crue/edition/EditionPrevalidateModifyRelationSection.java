/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import java.util.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DoubleEpsilonComparator;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent;
import org.fudaa.dodico.crue.edition.bean.ValidationMessage;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.dodico.crue.validation.util.DelegateValidatorBranche;
import org.fudaa.dodico.crue.validation.util.DelegateValidatorSection;
import org.fudaa.dodico.crue.validation.util.ValidationContentSectionDansBrancheExecutor;

/**
 *
 * Vérifie que toutes les nouvelles relations section<-> brancge pour un sous-modele sont correctes.
 *
 * @author Frédéric Deniger
 */
public class EditionPrevalidateModifyRelationSection {

  public static Map<String, List<ListRelationSectionContent>> getNewContentByBrancheName(Collection<ListRelationSectionContent> newSections) {
    Map<String, List<ListRelationSectionContent>> res = new HashMap<>();
    for (ListRelationSectionContent content : newSections) {
      final String brancheNom = content.getBrancheNom();
      if (StringUtils.isNotBlank(brancheNom)) {
        List<ListRelationSectionContent> xps = res.get(brancheNom);
        if (xps == null) {
          xps = new ArrayList<>();
          res.put(brancheNom, xps);
        }
        xps.add(content);
      }
    }
    return res;
  }

  protected CtuluLog removeDuplicateLines(ValidationMessage<ListRelationSectionContent> res) {
    CtuluLog withoutDuplicate = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    List<CtuluLogRecord> records = res.log.getRecords();
    MultiKeyMap map = new MultiKeyMap();
    for (CtuluLogRecord record : records) {
      String args = StringUtils.join(record.getArgs());
      if (!map.containsKey(record.getMsg(), args)) {
        withoutDuplicate.addRecord(record);
        map.put(record.getMsg(), args, record);
      }
    }
    return withoutDuplicate;
  }

  public void validateBrancheNewContent(
          Map<String, CatEMHBranche> branchesByNoms,
          Map<String, List<ListRelationSectionContent>> sectionByBrancheName, CtuluLog log) {
    HashSet<String> brancheNamesNotDefined = new HashSet<>(branchesByNoms.keySet());
    brancheNamesNotDefined.removeAll(sectionByBrancheName.keySet());
    if (!brancheNamesNotDefined.isEmpty()) {
      log.addSevereError("ChangeEMH.branchesNotDefinedInEdition", StringUtils.join(brancheNamesNotDefined, "; "));
    }
    for (Map.Entry<String, List<ListRelationSectionContent>> entry : sectionByBrancheName.entrySet()) {
      String brancheName = entry.getKey();
      List<ListRelationSectionContent> relations = entry.getValue();
      if (relations.size() < 2) {
        log.addSevereError("ChangeEMH.branchesWithout2Sections", brancheName);

      }
    }
  }

  public void validateNewSectionNames(
          Map<String, CatEMHSection> sectionsByNoms,
          Map<String, ListRelationSectionContent> sectionsContentByNoms, CtuluLog log) {
    HashSet<String> sectionNamesNotDefined = new HashSet<>(sectionsByNoms.keySet());
    sectionNamesNotDefined.removeAll(sectionsContentByNoms.keySet());
    if (!sectionNamesNotDefined.isEmpty()) {
      log.addSevereError("ChangeEMH.sectionNotDefinedInEdition", StringUtils.join(sectionNamesNotDefined, "; "));
    }
  }

  private static class BadXpContainer {

    final List<String> branchesDistanceNullButXpIsNot = new ArrayList<>();
    final List<String> branchesDistanceNotNullAndAmontNotZero = new ArrayList<>();
  }

  /**
   * seules branches strickler et saint-venant sont concernées.
   *
   * @param newXpByBrancheName les données triées
   * @param sousModele le sous-modele
   * @param ccm le {@link CrueConfigMetier}
   * @return les Xp dupliquées
   */
  DuplicateXpContainer findDuplicateXp(Map<String, List<ListRelationSectionContent>> newXpByBrancheName, EMHSousModele sousModele, CrueConfigMetier ccm) {
    DuplicateXpContainer res = new DuplicateXpContainer();
    PropertyEpsilon epsilon = ccm.getEpsilon(CrueConfigMetierConstants.PROP_XP);
    DoubleEpsilonComparator comparator = new DoubleEpsilonComparator(epsilon);
    List<EnumBrancheType> brancheTypeWithDistanceNotNull = DelegateValidatorBranche.getBrancheTypeWithDistanceNotNull();
    res.sectionsNameByBranche = EditionPrevalidateCreateRelationSection.getFinalSectionNameByBranche(newXpByBrancheName, sousModele, ccm);
    Map<String, CatEMHBranche> branchesByNoms = TransformerHelper.toMapOfNom(sousModele.getBranches());
    for (Map.Entry<String, List<ListRelationSectionContent>> entry : newXpByBrancheName.entrySet()) {
      CatEMHBranche branche = branchesByNoms.get(entry.getKey());
      //pour les branches de dimensions nulles, on ne teste pas:
      if (!brancheTypeWithDistanceNotNull.contains(branche.getBrancheType())) {
        continue;
      }
      TreeSet<Double> newXp = new TreeSet<>(comparator);
      List<ListRelationSectionContent> list = entry.getValue();
      for (ListRelationSectionContent listRelationSectionContent : list) {
        Double xp = Double.valueOf(listRelationSectionContent.getAbscisseHydraulique());
        if (newXp.contains(xp)) {
          res.duplicateNewRelation.add(listRelationSectionContent);
        }
        newXp.add(xp);
      }
    }
    return res;
  }

  private boolean containsOneZero(List<ListRelationSectionContent> contents, PropertyEpsilon epsilon) {
    for (ListRelationSectionContent content : contents) {
      if (epsilon.isZero(content.getAbscisseHydraulique())) {
        return true;
      }
    }
    return false;
  }

  private boolean containsOnlyZero(List<ListRelationSectionContent> contents, PropertyEpsilon epsilon) {
    for (ListRelationSectionContent content : contents) {
      if (!epsilon.isZero(content.getAbscisseHydraulique())) {
        return false;
      }
    }
    return true;
  }

  BadXpContainer findBadXp(Map<String, List<ListRelationSectionContent>> newXpByBrancheName, EMHSousModele modele, CrueConfigMetier ccm) {
    BadXpContainer res = new BadXpContainer();
    PropertyEpsilon epsilon = ccm.getEpsilon(CrueConfigMetierConstants.PROP_XP);
    List<EnumBrancheType> brancheTypeWithDistanceNotNull = DelegateValidatorBranche.getBrancheTypeWithDistanceNotNull();
    Map<String, CatEMHBranche> branchesByNoms = TransformerHelper.toMapOfNom(modele.getBranches());
    for (Map.Entry<String, List<ListRelationSectionContent>> entry : newXpByBrancheName.entrySet()) {
      CatEMHBranche branche = branchesByNoms.get(entry.getKey());
      //pour les branches de dimensions nulles, on ne teste pas:
      if (brancheTypeWithDistanceNotNull.contains(branche.getBrancheType())) {
        boolean containsZero = containsOneZero(entry.getValue(), epsilon);
        if (!containsZero) {
          res.branchesDistanceNotNullAndAmontNotZero.add(branche.getNom());
        }
      } else {
        boolean onlyZero = containsOnlyZero(entry.getValue(), epsilon);
        if (!onlyZero) {
          res.branchesDistanceNullButXpIsNot.add(branche.getNom());
        }
      }
    }
    return res;
  }

  /**
   * Contient pour chaque branche, les sections dans l'ordre existant
   *
   * @param contents les relations a valider
   * @param sousModele le {@link EMHSousModele}
   * @param ccm le {@link CrueConfigMetier}
   * @return validation
   */
  ValidationMessage<ListRelationSectionContent> validate(Collection<ListRelationSectionContent> contents, EMHSousModele sousModele,
          CrueConfigMetier ccm) {
    ValidationMessage<ListRelationSectionContent> res = new ValidationMessage<>();
    CtuluLog log = res.log;
    Map<String, CatEMHBranche> branchesByNoms = TransformerHelper.toMapOfNom(sousModele.getBranches());
    Map<String, CatEMHSection> sectionsByNoms = TransformerHelper.toMapOfNom(sousModele.getSections());
    Map<String, ListRelationSectionContent> sectionsContentByNoms = TransformerHelper.toMapOfNom(contents);
    Map<String, List<ListRelationSectionContent>> sectionByBrancheName = getNewContentByBrancheName(contents);

    validateNewSectionNames(sectionsByNoms, sectionsContentByNoms, log);
    validateBrancheNewContent(branchesByNoms, sectionByBrancheName, log);

    ItemVariable xpProperty = ccm.getProperty(CrueConfigMetierConstants.PROP_XP);
    ItemVariable dzProperty = ccm.getProperty(CrueConfigMetierConstants.PROP_DZ);

    DuplicateXpContainer findDuplicateXp = findDuplicateXp(sectionByBrancheName, sousModele, ccm);
    BadXpContainer badXp = findBadXp(sectionByBrancheName, sousModele, ccm);
    Map<EnumBrancheType, Collection<EnumSectionType>> authorizedSection = ValidationContentSectionDansBrancheExecutor.
            createSectionInterneTypeDansBrancheContents();
    Map<EnumBrancheType, Collection<EnumSectionType>> authorizedSectionAmonAval = ValidationContentSectionDansBrancheExecutor.
            createSectionTypeDansBrancheContents(true);
    for (ListRelationSectionContent content : contents) {
      String nom = content.getNom();
      List<CtuluLogRecord> msgs = new ArrayList<>();

      if (!sectionsByNoms.containsKey(nom)) {
        msgs.add(log.addSevereError("ChangeEMH.SectionUnknown", nom));
      }
      //le type de la section:
      EnumSectionType sectionType = content.getSection().getSectionType();
      if (sectionType == null) {
        msgs.add(log.addSevereError("AddEMH.SectionTypeUnknown", nom));
      }

      //pour section idem, il faut vérifier dz et la référence
      if (EnumSectionType.EMHSectionIdem.equals(sectionType)) {
        //dz
        double dz = content.getSection().getDz();
        Pair<String, Object[]> valueError = dzProperty.getValidator().getValidateErrorParamForValue(dz);
        if (valueError != null) {
          msgs.add(log.addSevereError(valueError.first, valueError.second));
        }
        //la référence
        String referenceSectionIdem = content.getSection().getReferenceSectionIdem();
        CatEMHSection referencedSection = sectionsByNoms.get(referenceSectionIdem);
        if (referencedSection == null) {
          msgs.add(log.addSevereError("AddEMH.SectionReferencedUnknown", nom, referenceSectionIdem));
        } else {
          List<EnumSectionType> authorizedSectionType = DelegateValidatorSection.getAuthorizedSectionRefType();
          if (!authorizedSectionType.contains(referencedSection.getSectionType())) {
            msgs.add(log.addSevereError("AddEMH.SectionReferencedInvalid", nom, referencedSection.getSectionType().geti18n()));
          } //si la section idem est active, la section de ref doit l'etre
          else {
            final String brancheNom = content.getBrancheNom();
            if (StringUtils.isNotBlank(brancheNom)) {
              CatEMHBranche branche = branchesByNoms.get(brancheNom);
              boolean sectionIdemIsActive = branche != null && branche.getActuallyActive();
              boolean sectionRefIsActive = false;
              if (sectionIdemIsActive) {
                ListRelationSectionContent refContent = sectionsContentByNoms.get(referenceSectionIdem);
                if (refContent != null && StringUtils.isNotBlank(refContent.getBrancheNom())) {
                  CatEMHBranche brancheRef = branchesByNoms.get(refContent.getBrancheNom());
                  sectionRefIsActive = brancheRef != null && brancheRef.getActuallyActive();
                }
              }

              if (sectionIdemIsActive && !sectionRefIsActive) {
                msgs.add(log.addSevereError("validation.sectionRefNotActive", nom, referenceSectionIdem));
              }
            }
          }
        }
      }
      final String brancheNom = content.getBrancheNom();
      //la branche
      if (StringUtils.isNotBlank(brancheNom)) {
        CatEMHBranche branche = branchesByNoms.get(brancheNom);
        if (branche == null) {
          msgs.add(log.addSevereError("ChangeEMH.BrancheUnknown", nom, brancheNom));
        } else if (sectionType != null) {
          if (!authorizedSection.get(branche.getBrancheType()).contains(sectionType)) {
            msgs.add(log.addSevereError("ChangeEMH.BrancheNotSupportThisSection", nom, brancheNom, sectionType.geti18n()));
          } else {
            List<String> sectionNames = findDuplicateXp.sectionsNameByBranche.get(brancheNom);
            if (CollectionUtils.isNotEmpty(sectionNames)) {
              boolean avalOrAmont = nom.equals(sectionNames.get(0)) || nom.equals(sectionNames.get(sectionNames.size() - 1));
              if (avalOrAmont && !authorizedSectionAmonAval.get(branche.getBrancheType()).contains(sectionType)) {
                msgs.add(log.addSevereError("ChangeEMH.BrancheNotSupportThisSectionAvalAmont", nom, content.getBrancheNom(), sectionType.geti18n()));
              }
            }
          }
        }
        double abscisseHydraulique = content.getAbscisseHydraulique();
        Pair<String, Object[]> valueError = xpProperty.getValidator().getValidateErrorParamForValue(abscisseHydraulique);
        if (valueError != null) {
          msgs.add(log.addSevereError(valueError.first, valueError.second));
        }
        //il faut aussi tester que le xp n'est pas en doublons....
        if (findDuplicateXp.duplicateExistingRelation.contains(content)) {
          msgs.add(log.addSevereError("AddEMH.XpAlreadyUsedInExistingRelation", nom));
        }
        if (findDuplicateXp.duplicateNewRelation.contains(content)) {
          msgs.add(log.addSevereError("AddEMH.XpAlreadyUsedInNewRelation", nom));
        }
        if (badXp.branchesDistanceNullButXpIsNot.contains(brancheNom)) {
          msgs.add(log.addSevereError("ChangeEMH.XpIsNotNullForBrancheWithNullDistance", brancheNom));
        }
        if (badXp.branchesDistanceNotNullAndAmontNotZero.contains(brancheNom)) {
          msgs.add(log.addSevereError("ChangeEMH.NoZeroXpForBranche", brancheNom));
        }
      }
      if (!msgs.isEmpty()) {
        res.accepted = false;
        ValidationMessage.ResultItem<ListRelationSectionContent> item = new ValidationMessage.ResultItem<>();
        item.bean = content;
        for (CtuluLogRecord ctuluLogRecord : msgs) {
          BusinessMessages.updateLocalizedMessage(ctuluLogRecord);
          item.messages.add(ctuluLogRecord.getLocalizedMessage());
        }
        res.messages.add(item);
      }

    }
    res.log = removeDuplicateLines(res);
    return res;

  }

  public static class DuplicateXpContainer {

    /**
     * Pour chaque branche cont
     */
    Map<String, List<String>> sectionsNameByBranche = new HashMap<>();
    /**
     * Contient les contenus qui ont un xp qui duplique une relation existante
     */
    final List<ListRelationSectionContent> duplicateExistingRelation = new ArrayList<>();
    /**
     * Contient les contenus qui ont un xp qui duplique une relation en creation.
     */
    final List<ListRelationSectionContent> duplicateNewRelation = new ArrayList<>();
  }
}
