/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.calcul;

import org.fudaa.ctulu.CtuluUI;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author deniger
 */
public class CalculCrueRunnerEmpty implements CalculCrueRunner {

  public static class EmptyRunner implements CalculCrueContrat {

    @Override
    public boolean isStopFileWritten() {
      return false;
    }

    /**
     * non application ici.
     * @return
     */
    @Override
    public boolean hasNoOptions() {
      return false;
    }

    @Override
    public void run() {
    }

    @Override
    public void stop() {
    }

    @Override
    public void setUI(final CtuluUI ctuluUI) {
      //nothing to do here...
    }

    @Override
    public boolean isComputeFinishedCorrectly() {
      return true;
    }
  }

  @Override
  public CalculCrueContrat createRunner(final ExecInput execInput, final ExecConfigurer configurer) {
    Logger.getLogger(CalculCrueRunnerEmpty.class.getName()).log(Level.SEVERE, "No compute done");
    return new EmptyRunner();
  }
}
