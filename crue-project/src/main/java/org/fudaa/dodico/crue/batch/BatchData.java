package org.fudaa.dodico.crue.batch;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.CoeurManager;
import org.fudaa.dodico.crue.projet.conf.Configuration;

/**
 * @author deniger
 */
public class BatchData {
  private Configuration configuration;
  private CoeurManager coeurManager;
  private CtuluLog log;

  public Configuration getConfiguration() {
    return configuration;
  }

  public void setConfiguration(Configuration configuration) {
    this.configuration = configuration;
  }

  public CoeurManager getCoeurManager() {
    return coeurManager;
  }

  public void setCoeurManager(CoeurManager coeurManager) {
    this.coeurManager = coeurManager;
  }

  public CtuluLog getLog() {
    return log;
  }

  public void setLog(CtuluLog log) {
    this.log = log;
  }
}
