package org.fudaa.dodico.crue.projet.clean;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformationDefault;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.edition.DeleteContainersCallable;
import org.fudaa.dodico.crue.edition.EditionDeleteContainer;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.projet.ProjectNormalizableCallable;
import org.fudaa.dodico.crue.projet.SaveScenariosCallable;
import org.fudaa.dodico.crue.projet.select.SelectableFinder;
import org.openide.util.Exceptions;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Action permettant de nettoyer une etude.
 */
public class EMHProjectCleaner {
  private final File etuFile;
  private final EMHProjetController projetController;

  /**
   * @param project le projet a nettoyer
   * @param etuFile le fichier etu
   */
  public EMHProjectCleaner(EMHProjet project, File etuFile) {
    this(new EMHProjetController(project, new ConnexionInformationDefault()), etuFile);
  }

  /**
   * @param emhProjetController le controlleur
   * @param etuFile le fichier etu
   */
  public EMHProjectCleaner(EMHProjetController emhProjetController, File etuFile) {
    projetController = emhProjetController;
    this.etuFile = etuFile;
  }

  /**
   * Supprimer tous les scenarios en question
   *
   * @param scenarioNamesToDelete noms des scenarios a supprimer
   * @param logGroup aggrege les logs
   * @return scenarioNamesToDelete
   */
  private Set<String> doDelete(List<String> scenarioNamesToDelete, CtuluLogGroup logGroup) throws Exception {
    Set<String> deleted = new HashSet<>();
    if (CollectionUtils.isNotEmpty(scenarioNamesToDelete)) {
      deleted.addAll(scenarioNamesToDelete);
      final SelectableFinder.SelectedItems items = getSelectedItems(projetController.getProjet(), scenarioNamesToDelete);
      DeleteContainersCallable deleteCallable = new DeleteContainersCallable(items, projetController);
      deleteCallable.call();
      final CtuluLogGroup bilan = EditionDeleteContainer.createBilan(deleteCallable, "projectSimplication.deleteDuplicated");
      if (bilan.containsSomething()) {
        logGroup.addGroup(bilan);
      }
    }
    return deleted;
  }

  /**
   * @param projet le projet
   * @param scenarioNamesToDelete le noms des scenarios a supprimer completement
   * @return liste des elements a supprimer dans le cadre de suppression profonde.
   */
  public static SelectableFinder.SelectedItems getSelectedItems(EMHProjet projet, List<String> scenarioNamesToDelete) {
    if (CollectionUtils.isEmpty(scenarioNamesToDelete)) {
      return new SelectableFinder.SelectedItems();
    }
    Map<String, ManagerEMHScenario> scenarioByName = TransformerHelper.toMapOfNom(projet.getListeScenarios());
    List<ManagerEMHScenario> scenarioToDelete = scenarioNamesToDelete.stream().map(name -> scenarioByName.get(name)).collect(Collectors.toList());
    final SelectableFinder finder = new SelectableFinder();
    finder.setProject(projet);
    return finder.getIndependantSelection(scenarioToDelete, true);
  }

  /**
   * @param deleted les scenarios deja supprimé dans l'opération précédente. Pour eviter de normaliser un scenario supprimé
   * @param allScenario tous les scenarios avec erreur ou a normaliser. Seuls ceux avec le {@link ProjectNormalizableCallable.Status} TO_NORMALIZE le seront
   * @param ctuluLogGroup les logs
   */
  private void doNormalize(Set<String> deleted, List<ProjectNormalizableCallable.Result> allScenario, boolean saveProject, CtuluLogGroup ctuluLogGroup) throws Exception {
    if (CollectionUtils.isEmpty(allScenario)) {
      return;
    }

    final List<String> scenarioToNormalize = getScenarioToNormalize(allScenario);
    //ne pas normaliser les scénarios supprimés:
    scenarioToNormalize.removeAll(deleted);

    if (CollectionUtils.isNotEmpty(scenarioToNormalize)) {
      //on appelle l'operation de sauvegarde en masse:
      final SaveScenariosCallable saveScenariosCallable = new SaveScenariosCallable(projetController, scenarioToNormalize, etuFile);
      saveScenariosCallable.setSaveProject(saveProject);
      saveScenariosCallable.call();
      final CtuluLogGroup logGroup = saveScenariosCallable.getLogs();
      if (logGroup.containsError()) {
        logGroup.setDescription("projectSimplication.normalization");
        ctuluLogGroup.addGroup(logGroup);
      }
    }
  }

  /**
   * @param allScenario liste initiale
   * @return scenario dont le statut est TO_NORMALIZE
   */
  public static List<String> getScenarioToNormalize(List<ProjectNormalizableCallable.Result> allScenario) {
    return filterScenario(ProjectNormalizableCallable.Status.TO_NORMALIZE, allScenario);
  }

  public static CtuluLogGroup getLogs(List<ProjectNormalizableCallable.Result> allScenario) {
    CtuluLogGroup logs = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    allScenario.stream().filter(result -> ProjectNormalizableCallable.Status.ERROR.equals(result.state)).forEach(result -> {
      logs.addGroup(result.logs);
      logs.setDescription(result.getScenarioName());
    });
    return logs;
  }

  private static List<String> filterScenario(final ProjectNormalizableCallable.Status state, List<ProjectNormalizableCallable.Result> allScenario) {
    if (allScenario == null) {
      return Collections.emptyList();
    }
    return allScenario.stream()
        .filter(stringStateEntry -> state.equals(stringStateEntry.state))
        .map(stringStateEntry -> stringStateEntry.managerEMHScenario.getNom())
        .collect(Collectors.toList());
  }

  /**
   * @param cleanData les données a supprimer
   * @param saveProject true si le projet doit être sauvegarder par la suite
   * @return resultats des opérations.
   */
  public CtuluLogGroup cleanProject(ScenarioCleanData cleanData, boolean saveProject) {
    CtuluLogGroup lastLog = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    lastLog.setDescription("projectSimplication.title");
    lastLog.setDescriptionArgs(etuFile.getAbsolutePath());
    if (cleanData != null && cleanData.isNotEmpty()) {
      try {
        //on supprime les scenarios demandés
        final Set<String> deleteScenario = doDelete(cleanData.scenarioNamesToDelete, lastLog);
        //on normalise
        doNormalize(deleteScenario, cleanData.scenarioToNormalizeOrWithErrors, saveProject, lastLog);
        //si sauvegarde demandee
        if (saveProject) {
          projetController.saveProjet(etuFile, lastLog);
        }
      } catch (Exception ex) {
        Exceptions.printStackTrace(ex);
      }
    }
    return lastLog;
  }
}
