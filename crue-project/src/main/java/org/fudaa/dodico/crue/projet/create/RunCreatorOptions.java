/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.create;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.EMHRun;

import java.util.*;

/**
 * @author deniger
 */
public class RunCreatorOptions {
    /**
     * Si null, aucun fichier de résultat ne sera copié dans le répertoire de run.
     */
    private final EMHRun runContainingResultFile;
    private Map<CrueFileType, RunCalculOption> optionsByCompute = new EnumMap<>(CrueFileType.class);

    /**
     * @param runContainingResultFile le run
     */
    public RunCreatorOptions(EMHRun runContainingResultFile) {
        super();
        this.runContainingResultFile = runContainingResultFile;
        optionsByCompute = createDefaultMap();
    }

    public static String geti18n(CrueFileType type) {
        return BusinessMessages.getString("runOption." + type.name().toLowerCase() + ".name");
    }

    public static List<CrueFileType> getRunFileType() {
        return Arrays.asList(CrueFileType.RPTR, CrueFileType.RPTG, CrueFileType.RPTI, CrueFileType.RCAL);
    }

    public static Map<CrueFileType, RunCalculOption> createDefaultMap() {
        return createMap(RunCalculOption.EXECUTE_IF_NEEDED);
    }

    public static Map<CrueFileType, RunCalculOption> createMap(RunCalculOption runCalculOption) {
        Map<CrueFileType, RunCalculOption> res = new EnumMap<>(CrueFileType.class);
        setComputeAll(res, runCalculOption);
        return res;
    }

    public static void setComputeAll(Map<CrueFileType, RunCalculOption> map, RunCalculOption runCalculOption) {
        map.put(CrueFileType.RPTR, runCalculOption);
        map.put(CrueFileType.RPTG, runCalculOption);
        map.put(CrueFileType.RPTI, runCalculOption);
        map.put(CrueFileType.RCAL, runCalculOption);
    }

    public Map<CrueFileType, RunCalculOption> getOptionsByCompute() {
        return Collections.unmodifiableMap(optionsByCompute);
    }

    public void setComputeAll(RunCalculOption calculOption) {
        setComputeAll(optionsByCompute, calculOption);
    }

    /**
     * @return the run containing result Files to copy in the new created run ( if not obsolete)
     */
    public EMHRun getSelectedRunToStartFrom() {
        return runContainingResultFile;
    }

    public void setCompute(CrueFileType crueFileType, RunCalculOption calculOption) {
        optionsByCompute.put(crueFileType, calculOption);
    }
}
