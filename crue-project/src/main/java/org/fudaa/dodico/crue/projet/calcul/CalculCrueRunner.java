/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.calcul;

/**
 * A completer
 *
 * @author deniger
 */
public interface CalculCrueRunner {

  /**
   * @param  configurer  si null configuration par défaut.
   */
  CalculCrueContrat createRunner(ExecInput execInput, ExecConfigurer configurer);
}
