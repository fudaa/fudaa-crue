/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report;

import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;

import java.util.Collection;
import java.util.Set;

/**
 * Une interface pour un service capable de donner les variables utilisées par une autre
 *
 * @author Frederic Deniger
 */
public interface FormuleContentManager {

    /**
     * @param var la variable
     * @return les variables utilisées par <code>var</code>.
     */
    Set<String> getDirectUsedVariable(String var);

    /**
     * @param var la variable
     * @return true si le formule en question est dépendante du temps
     */
    boolean containsTimeDependantVariable(String var);


    /**
     * @param var           la variable à tester
     * @param selectedTimes les temps sélectionnées
     * @return true si la formule est valide. Si pas une formule renvoie true
     */
    boolean isExpressionValid(String var, Collection<ResultatTimeKey> selectedTimes);
}
