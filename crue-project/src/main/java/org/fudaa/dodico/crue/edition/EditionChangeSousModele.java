/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluPair;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.DonFrtHelper;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;

import java.util.*;

/**
 * Permet de changer une EMH de sous-modele
 *
 * @author deniger
 */
public class EditionChangeSousModele {
  final UniqueNomFinder uniqueNomFinder = new UniqueNomFinder();

  public static Set<EMHSousModele> getUsedIn(final CatEMHNoeud noeudToMove) {
    final Set<EMHSousModele> res = new HashSet<>();
    final CatEMHCasier casier = noeudToMove.getCasier();
    if (casier != null) {
      res.add(casier.getParent());
    }
    final List<CatEMHBranche> branches = noeudToMove.getBranches();
    for (final CatEMHBranche branche : branches) {
      res.add(branche.getParent());
    }
    return res;
  }

  private static boolean allSectionWillBeMoved(final Collection<CatEMHSection> usedSections, final Collection<CatEMHSection> moved) {
    for (final CatEMHSection usedSection : usedSections) {
      if (!moved.contains(usedSection)) {
        return false;
      }
    }
    return true;
  }

  protected void changeSections(final Set<CatEMHSection> changedSection, final EMHSousModele target, final MoveToSousModeleResult res) {
    //pour les sections, il faudrait déplacer les frottements ou les cloner si utilisés.
    final Map<EMHSousModele, Set<CatEMHSection>> movedSectionsBySousModele = new HashMap<>();
    for (final CatEMHSection section : changedSection) {
      final EMHSousModele parent = section.getParent();
      Set<CatEMHSection> set = movedSectionsBySousModele.get(parent);
      if (set == null) {
        set = new HashSet<>();
        movedSectionsBySousModele.put(parent, set);
      }
      set.add(section);
    }
    for (final Map.Entry<EMHSousModele, Set<CatEMHSection>> entry : movedSectionsBySousModele.entrySet()) {
      final EMHSousModele sousModele = entry.getKey();
      final Set<CatEMHSection> movedSections = entry.getValue();
      final Set<DonFrt> movedDonFrts = DonFrtHelper.getUsedDonFrt(movedSections);
      final Map<DonFrt, Set<CatEMHSection>> usedBy = DonFrtHelper.getUsedBy(sousModele, movedDonFrts);
      for (final DonFrt movedDonFrt : movedDonFrts) {
        final Set<CatEMHSection> sectionsUsingDonFrt = usedBy.get(movedDonFrt);
        if (!allSectionWillBeMoved(sectionsUsingDonFrt, movedSections)) {
          //on doit cloner
          final DonFrt cloned = cloneAndReplaceDonFrtIn(movedDonFrt, sectionsUsingDonFrt, target);
          target.getFrtConteneur().addFrt(cloned);
          res.donFrtCloned.add(movedDonFrt.getNom());
        } else {
          //on peut déplacer le donFrt.
          sousModele.getFrtConteneur().removeFrt(movedDonFrt);
          target.getFrtConteneur().addFrt(movedDonFrt);
        }
      }
      //on deplace les sections:
      for (final CatEMHSection movedSection : movedSections) {
        EMHRelationFactory.removeRelationBidirect(sousModele, movedSection);
        EMHRelationFactory.addRelationContientEMH(target, movedSection);
        res.addEmhMoved(movedSection);
      }
    }
  }

  protected void copyNoeudIfNeed(final Set<CatEMHNoeud> noeudsInTarget, final CatEMHNoeud noeud, final EMHSousModele target, final MoveToSousModeleResult res) {
    //on doit copier le noeud
    if (!noeudsInTarget.contains(noeud)) {
      //on ajoute le noeuds dans la cible:
      EMHRelationFactory.addRelationContientEMH(target, noeud);
      noeudsInTarget.add(noeud);
      res.addEmhMoved(noeud);
    }
  }

  private DonFrt cloneAndReplaceDonFrtIn(final DonFrt movedDonFrt,
                                         final Set<CatEMHSection> sectionsUsingDonFrt, final EMHSousModele target) {

    final DonFrt cloned = movedDonFrt.deepClone();
    final String newName = uniqueNomFinder.findUniqueNameDonFrt(cloned.getNom(), target.getParent().getParent());
    cloned.setNom(newName);
    //on remplace dans toutes les sections:
    for (final CatEMHSection section : sectionsUsingDonFrt) {
      if (section.getSectionType().equals(EnumSectionType.EMHSectionProfil)) {
        final EMHSectionProfil sectionProfil = (EMHSectionProfil) section;
        final DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection(sectionProfil);
        final List<LitNumerote> litNumerotes = profilSection.getLitNumerote();
        for (final LitNumerote litNumerote : litNumerotes) {
          if (movedDonFrt.equals(litNumerote.getFrot())) {
            litNumerote.setFrot(cloned);
          }
        }
      }
    }
    return cloned;
  }

  public MoveToSousModeleResult changeSousModeleParent(final Collection<EMH> emhsToMoved, final EMHSousModele target) {
    final Set<CatEMHNoeud> noeudsToMoveIfNeeded = new HashSet<>();
    final Set<CatEMHNoeud> noeudsInTarget = new HashSet<>(target.getNoeuds());
    final MoveToSousModeleResult res = new MoveToSousModeleResult();

    //contient toutes les sections qui seront changées de modele.
    final Set<CatEMHSection> allMovedSections = new HashSet<>();
    final Set<CatEMHBranche> allMovedBranches = new HashSet<>();

    //permet de récupérer les branches et section modifiée
    collectTargetBranches(EMHHelper.selectSetOfEMHS(emhsToMoved, EnumCatEMH.BRANCHE), target, allMovedSections, allMovedBranches);
    //fait simplement un filtre sur les sections utilisées par des branches non déplacées:
    collectTargetSections(EMHHelper.selectSetOfEMHS(emhsToMoved, EnumCatEMH.SECTION), target, allMovedBranches, allMovedSections, res);
    boolean canContinueWithBrancheSection = true;
    //on teste les sections pilotes:
    for (final CatEMHBranche branche : allMovedBranches) {
      final CatEMHSection sectionPilote = EMHHelper.getSectionPilote(branche);
      //la section pilote ne va pas appartenir au même sous-modèle
      if (sectionPilote != null && !allMovedSections.contains(sectionPilote)) {
        canContinueWithBrancheSection = false;
        res.brancheNotMovedSectionPilote.add(new CtuluPair<>(branche.getNom(), sectionPilote.getNom()));
      }
    }

    for (final CatEMHSection section : allMovedSections) {
      //on teste les section idem:
      if (EnumSectionType.EMHSectionIdem.equals(section.getSectionType())) {
        final CatEMHSection sectionRef = EMHHelper.getSectionRef((EMHSectionIdem) section);
        if (!allMovedSections.contains(sectionRef)) {
          res.sectionIdemNotMoved.add(new CtuluPair<>(section.getNom(), sectionRef.getNom()));
          canContinueWithBrancheSection = false;
        }
        //inversement les section references ne peuvent être déplacées que si la section idem l'est
        final List<EMHSectionIdem> sectionReferencingList = EMHHelper.getSectionReferencing(section);
        if (CollectionUtils.isNotEmpty(sectionReferencingList)) {
          for (final EMHSectionIdem emhSectionIdem : sectionReferencingList) {
            if (!allMovedSections.contains(emhSectionIdem)) {
              res.sectionDeReferenceNotMoved.add(new CtuluPair<>(section.getNom(), emhSectionIdem.getNom()));
              canContinueWithBrancheSection = false;
            }
          }
        }
        //les section pilote ne doivent être déplacées que si la branche l'est
        final CatEMHBranche branchePiloteBy = EMHHelper.getBranchePilotedBy(section);
        if (branchePiloteBy != null && !allMovedBranches.contains(branchePiloteBy)) {
          res.sectionPiloteNotMoved.add(new CtuluPair<>(section.getNom(), branchePiloteBy.getNom()));
          canContinueWithBrancheSection = false;
        }
      }
    }

    changeCasiers(emhsToMoved, target, noeudsToMoveIfNeeded, noeudsInTarget, res);
    if (canContinueWithBrancheSection) {
      changeSections(allMovedSections, target, res);
      for (final CatEMHBranche branche : allMovedBranches) {
        final CatEMHNoeud noeudAmont = branche.getNoeudAmont();
        copyNoeudIfNeed(noeudsInTarget, noeudAmont, target, res);
        noeudsToMoveIfNeeded.add(noeudAmont);
        final CatEMHNoeud noeudAval = branche.getNoeudAval();
        copyNoeudIfNeed(noeudsInTarget, noeudAval, target, res);
        noeudsToMoveIfNeeded.add(noeudAval);
        EMHRelationFactory.removeRelationBidirect(branche.getParent(), branche);
        EMHRelationFactory.addRelationContientEMH(target, branche);
        res.addEmhMoved(branche);
      }
    }

    changeNoeuds(emhsToMoved, noeudsToMoveIfNeeded, noeudsInTarget, target, res);

    clearMovedNoeuds(noeudsToMoveIfNeeded, target, res);

    res.sort();
    return res;
  }

  protected void clearMovedNoeuds(final Set<CatEMHNoeud> noeudsToMoveIfNeeded, final EMHSousModele target, final MoveToSousModeleResult res) {
    for (final CatEMHNoeud noeudToMove : noeudsToMoveIfNeeded) {
      final Set<EMHSousModele> usedIn = getUsedIn(noeudToMove);
      final List<EMHSousModele> parents = noeudToMove.getParents();
      for (final EMHSousModele parent : parents) {
        if (parent != target && !usedIn.contains(parent)) {
          EMHRelationFactory.removeRelationBidirect(parent, noeudToMove);
        } else if (parent != target) {
          res.setNoeudDuplicatedToTarget(noeudToMove);
        }
      }
    }
  }

  protected void collectTargetSections(final Collection<CatEMHSection> initSectionsToMoved, final EMHSousModele target,
                                       final Set<CatEMHBranche> allBranchesToMove, final Set<CatEMHSection> allSectionsToMove, final MoveToSousModeleResult res) {
    for (final CatEMHSection section : initSectionsToMoved) {
      //on ignore les sections deja dans le sous-modele cible
      if (!section.getParent().equals(target)) {
        //une section qui appartient à une branche non modifiée ne peut pas être deplacee.
        if (section.getBranche() != null && !allBranchesToMove.contains(section.getBranche())) {
          res.sectionNotMovedUsedByBranche.add(new CtuluPair<>(section.getNom(), section.getBranche().getNom()));
        } else {
          allSectionsToMove.add(section);
        }
      }
    }
  }

  protected void collectTargetBranches(final Collection<CatEMHBranche> branchesToMoved, final EMHSousModele target,
                                       final Set<CatEMHSection> allMovedSections,
                                       final Set<CatEMHBranche> allMovedBranches) {
    for (final CatEMHBranche branche : branchesToMoved) {
      //déja dans la cible, on ne fait rien
      if (!branche.getParent().equals(target)) {
        final List<RelationEMHSectionDansBranche> relations = branche.getSections();
        for (final RelationEMHSectionDansBranche relation : relations) {
          allMovedSections.add(relation.getEmh());
        }
        final CatEMHSection sectionPilote = EMHHelper.getSectionPilote(branche);
        if (sectionPilote != null) {
          allMovedSections.add(sectionPilote);
        }
        allMovedBranches.add(branche);
      }
    }
  }

  protected void changeNoeuds(final Collection<EMH> emhsToChange,
                              final Set<CatEMHNoeud> noeudsToMoveIfNeeded,
                              final Set<CatEMHNoeud> noeudsInTarget, final EMHSousModele target, final MoveToSousModeleResult res) {

    //on change les noeuds:
    final List<CatEMHNoeud> noeuds = EMHHelper.selectEMHS(emhsToChange, EnumCatEMH.NOEUD);
    for (final CatEMHNoeud noeud : noeuds) {
      noeudsToMoveIfNeeded.add(noeud);
      if (!noeudsInTarget.contains(noeud)) {
        //pour les noeud
        EMHRelationFactory.addRelationContientEMH(target, noeud);
        noeudsInTarget.add(noeud);
        res.addEmhMoved(noeud);
      }
    }
  }

  protected void changeCasiers(final Collection<EMH> emh, final EMHSousModele target,
                               final Set<CatEMHNoeud> noeudsToMoveIfNeeded,
                               final Set<CatEMHNoeud> noeudsInTarget, final MoveToSousModeleResult res) {
    final Collection<CatEMHCasier> casiers = EMHHelper.selectSetOfEMHS(emh, EnumCatEMH.CASIER);
    for (final CatEMHCasier casier : casiers) {
      //on ignore:
      if (casier.getParent() == target) {
        continue;
      }
      final CatEMHNoeud noeud = casier.getNoeud();
      noeudsToMoveIfNeeded.add(noeud);
      copyNoeudIfNeed(noeudsInTarget, noeud, target, res);
      //on déplace le casier:
      EMHRelationFactory.removeRelationBidirect(casier.getParent(), casier);
      EMHRelationFactory.addRelationContientEMH(target, casier);
      res.addEmhMoved(casier);
    }
  }

  public static class MoveToSousModeleResult {
    public final List<CtuluPair<String>> sectionNotMovedUsedByBranche = new ArrayList<>();
    public final List<CtuluPair<String>> brancheNotMovedSectionPilote = new ArrayList<>();
    public final List<CtuluPair<String>> sectionIdemNotMoved = new ArrayList<>();
    public final List<CtuluPair<String>> sectionDeReferenceNotMoved = new ArrayList<>();
    public final List<CtuluPair<String>> sectionPiloteNotMoved = new ArrayList<>();
    public final List<String> donFrtCloned = new ArrayList<>();
    public final Collection<String> emhsMoved = new TreeSet<>();
    public final Collection<String> noeudsDuplicatedInTarget = new TreeSet<>();

    public void addEmhMoved(EMH emh) {
      if (emh != null) {
        emhsMoved.add(emh.getNom());
      }
    }

    public int getNbEMHsModified() {
      return emhsMoved.size() + noeudsDuplicatedInTarget.size();
    }

    public boolean containsMessage() {
      return !sectionNotMovedUsedByBranche.isEmpty()
        || !sectionDeReferenceNotMoved.isEmpty()
        || !brancheNotMovedSectionPilote.isEmpty()
        || !sectionPiloteNotMoved.isEmpty()
        || !sectionIdemNotMoved.isEmpty()
        || !donFrtCloned.isEmpty();
    }

    private void sort() {
      Collections.sort(sectionNotMovedUsedByBranche);
      Collections.sort(brancheNotMovedSectionPilote);
      Collections.sort(sectionPiloteNotMoved);
      Collections.sort(sectionIdemNotMoved);
      Collections.sort(sectionDeReferenceNotMoved);
      Collections.sort(donFrtCloned);
    }

    public void setNoeudDuplicatedToTarget(CatEMHNoeud noeudDuplicated) {
      if (noeudDuplicated != null) {
        noeudsDuplicatedInTarget.add(noeudDuplicated.getNom());
        emhsMoved.remove(noeudDuplicated.getNom());
      }
    }
  }
}
