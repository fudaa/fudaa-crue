/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.calcul;

/**
 *
 * @author deniger
 */
public class CalculCrue9Runner implements CalculCrueRunner {

  private final String crue9Exec;

  public CalculCrue9Runner(final String exec) {
    this.crue9Exec = exec;
  }

  @Override
  public CalculCrueContrat createRunner(final ExecInput execInput, final ExecConfigurer configurer) {
    final Crue9Exec exec = new Crue9Exec(crue9Exec, execInput);
    if (configurer != null) {
      configurer.configure(exec);
    }
    return exec;
  }
}
