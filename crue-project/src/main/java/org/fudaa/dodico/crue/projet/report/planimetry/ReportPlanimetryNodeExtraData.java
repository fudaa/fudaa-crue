/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.planimetry;

import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;

/**
 *
 * @author Frederic Deniger
 */
public class ReportPlanimetryNodeExtraData extends AbstractReportPlanimetryConfigData {

  public ReportPlanimetryNodeExtraData() {

  }

  protected ReportPlanimetryNodeExtraData(ReportPlanimetryNodeExtraData from) {
    super(from);
  }

  @Override
  public ReportPlanimetryNodeExtraData copy() {
    return new ReportPlanimetryNodeExtraData(this);
  }

  @Override
  protected boolean specificVariableUpdated(FormuleDataChanges changes) {
    return false;

  }
}
