/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.calcul;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.etude.RunFile;
import org.fudaa.dodico.crue.projet.create.RunCalculOption;

/**
 * Options principales : -r --ptr Demande de pre-traitement reseau.<br/> -g --ptg Demande de pre-traitement de la geometrie.<br/> -i --pti Demande de
 * pre-traitement des conditions initiales<br/> -c --calc Demande de calculs
 *
 * Attention: dans un run crue 10 , les fichiers obsolètes ne sont pas copiés.
 *
 * @see org.fudaa.dodico.crue.projet.create.RunCreator
 *
 * @author deniger
 */
public class Crue10OptionBuilder {

  private final Map<CrueFileType, String> commands;

  public Crue10OptionBuilder(final Map<CrueFileType, String> commands) {
    this.commands = commands;
  }

  public ExecConfigurer createConfigurer(final ExecInput input, final Map<CrueFileType, RunCalculOption> options) {
    final List<String> optionToAdd = new ArrayList<>();
    final Map<String, RunFile> runFilesResultat = input.getProjet().getRunFilesResultat(input.getScenario(), input.getRun());
    addIfRequired(CrueFileType.RPTR, input, options, optionToAdd, runFilesResultat);
    addIfRequired(CrueFileType.RPTG, input, options, optionToAdd, runFilesResultat);
    addIfRequired(CrueFileType.RPTI, input, options, optionToAdd, runFilesResultat);
    addIfRequired(CrueFileType.RCAL, input, options, optionToAdd, runFilesResultat);
    return new Crue10ExecConfigurer(optionToAdd);
  }

  private void addIfRequired(final CrueFileType fileType, final ExecInput input, final Map<CrueFileType, RunCalculOption> options,
                             final List<String> optionToAdd, final Map<String, RunFile> runFilesResultat) {
    if (mustBeAdded(fileType, input, options, runFilesResultat)) {
      final String option = commands.get(fileType);
      if (StringUtils.isNotBlank(option)) {
        optionToAdd.add(option);
      }
    }
  }

  private boolean mustBeAdded(final CrueFileType type, final ExecInput input, final Map<CrueFileType, RunCalculOption> options,
                              final Map<String, RunFile> runFilesResultat) {
    final RunCalculOption option = options.get(type);
    if (option.equals(RunCalculOption.FORCE_EXCUTE)) {
      return true;
    }
    if (option.equals(RunCalculOption.EXECUTE_IF_NEEDED)) {
      final List<ManagerEMHModeleBase> modeles = input.getScenario().getFils();
      for (final ManagerEMHModeleBase modele : modeles) {
        //pour tous les modèles actifs:
        if (modele.isActive()) {
          final String resKey = ManagerEMHScenario.getResKey(modele, type);
          final RunFile runFile = runFilesResultat.get(resKey);
          //dans un run, les fichiers obsolètes ne sont pas copiés donc, on teste simplement l'existence.
          if (!runFile.getFile().exists()) {
            return true;
          }
        }
      }
    }
    return false;
  }
}
