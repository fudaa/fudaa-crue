/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.projet.report.data.ReportKeyContract;
import org.fudaa.dodico.crue.projet.report.data.ReportRunEmhKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableConfig;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableEmhKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;
import org.fudaa.dodico.crue.projet.report.loi.ViewCourbeConfig;
import org.fudaa.dodico.crue.projet.report.persist.AbstractReportCourbeConfig;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.dodico.crue.projet.report.persist.VariableUpdateHelper;
import org.fudaa.ebli.courbe.EGCourbePersist;

/**
 * @author Frederic Deniger
 */
public class ReportMultiVarConfig extends AbstractReportCourbeConfig {

  String horizontalVar;
  @XStreamImplicit(itemFieldName = "Emh")
  private List<String> emhs = new ArrayList<>();
  @XStreamImplicit(itemFieldName = "Variable")
  List<String> variables = new ArrayList<>();
  /**
   * la liste des emh par run à dessiner sur le graphe
   */
  @XStreamImplicit(itemFieldName = "RunEmh")
  Set<ReportRunEmhKey> reportRunEmhs = new HashSet<>();
  /**
   * la config pour les courbes:
   */
  @XStreamAlias("Courbe-Configs")
  Map<ReportRunVariableEmhKey, EGCourbePersist> courbeconfigs = new HashMap<>();
  @XStreamAlias("Vue-Config")
  ViewCourbeConfig loiLegendConfig = new ViewCourbeConfig();

  public ReportMultiVarConfig() {
    super();
  }

  public ReportMultiVarConfig(ReportMultiVarConfig from) {
    super(from);
    if (from == null) {
      return;
    }
    horizontalVar=from.horizontalVar;
    if (from.emhs != null) {
      this.emhs.addAll(from.emhs);
    }
    if (from.variables != null) {
      this.variables.addAll(from.variables);
    }
    if (from.reportRunEmhs != null) {
      this.reportRunEmhs.addAll(from.reportRunEmhs);
    }
    if (from.courbeconfigs != null) {
      from.courbeconfigs.forEach((key, value) -> this.courbeconfigs.put(key, value.duplicate()));
    }
    if (loiLegendConfig != null) {
      this.loiLegendConfig = from.loiLegendConfig.dpulicate();
    }
  }

  @Override
  public ReportMultiVarConfig duplicate() {
    return new ReportMultiVarConfig(this);
  }

  @Override
  public ViewCourbeConfig getLoiLegendConfig() {
    return loiLegendConfig;
  }

  public Map<ReportRunVariableEmhKey, EGCourbePersist> getCourbeconfigs() {
    return courbeconfigs;
  }


  @Override
  protected void cleanUnusedCourbesConfig(Set<ReportKeyContract> usedKeys) {
    for (Iterator<ReportRunVariableEmhKey> it = courbeconfigs.keySet().iterator(); it.hasNext(); ) {
      if (!usedKeys.contains(it.next())) {
        it.remove();
      }
    }
  }

  @Override
  public ReportConfigContrat createTemplateConfig() {
    final ReportMultiVarConfig duplicate = (ReportMultiVarConfig) duplicate();
    duplicate.clearExternFiles();
    duplicate.emhs.clear();
    duplicate.loiLegendConfig.clearLabelTxtTooltip();
    duplicate.reportRunEmhs.clear();
    duplicate.courbeconfigs.clear();
    duplicate.clearZooms();
    return duplicate;
  }

  @Override
  public boolean variablesUpdated(FormuleDataChanges changes) {
    boolean modified = super.axeVariablesUpdated(changes);
    modified |= VariableUpdateHelper.updateViewCourbeConfig(loiLegendConfig, changes);
    if (changes.isRemoved(horizontalVar)) {
      horizontalVar = null;
      modified = true;
    } else if (changes.isRenamed(horizontalVar)) {
      horizontalVar = changes.getNewName(horizontalVar);
      modified = true;
    } else if (changes.isModified(horizontalVar)) {
      modified = true;
    }
    Pair<List<String>, Boolean> res = VariableUpdateHelper.variablesUpdated(variables, changes);
    if (Boolean.TRUE.equals(res.second)) {
      variables = res.first;
      modified = true;
    }
    modified |= VariableUpdateHelper.updateCourbeConfig(courbeconfigs, changes);
    return modified;
  }

  public void setHorizontalVar(String horizontalVar) {
    this.horizontalVar = horizontalVar;
  }

  public List<String> getEmhs() {
    return Collections.unmodifiableList(emhs);
  }

  public List<String> getVariables() {
    return variables;
  }

  public Set<ReportRunEmhKey> getReportRunEmhs() {
    return reportRunEmhs;
  }

  public String getHorizontalVar() {
    return horizontalVar;
  }

  @Override
  public void reinitContent() {
    super.reinitContent();
    if (emhs == null) {
      emhs = new ArrayList<>();
    }
    if (variables == null) {
      variables = new ArrayList<>();
    }
    if (reportRunEmhs == null) {
      reportRunEmhs = new HashSet<>();
    }
    if (courbeconfigs == null) {
      courbeconfigs = new HashMap<>();
    }
    if (loiLegendConfig == null) {
      loiLegendConfig = new ViewCourbeConfig();
    }
    loiLegendConfig.reinit();
    emhs.remove(null);
    emhs.remove(StringUtils.EMPTY);
    variables.remove(null);
    variables.remove(StringUtils.EMPTY);
    reportRunEmhs.remove(ReportRunEmhKey.NULL_VALUE);
    courbeconfigs.remove(ReportRunVariableEmhKey.NULL);
  }

  public void setEMHAndAddRunCourant(List<String> selectedEMH, ReportResultProviderServiceContrat reportService) {
    if (selectedEMH != null) {
      List<String> newEMHs = new ArrayList<>(selectedEMH);
      newEMHs.removeAll(emhs);
      emhs = selectedEMH;
      Collections.sort(emhs);
      if (!newEMHs.isEmpty()) {
        for (String emhName : newEMHs) {
          reportRunEmhs.add(new ReportRunEmhKey(reportService.getRunCourant().getRunKey(), emhName));
        }
      }
      for (Iterator<ReportRunEmhKey> it = reportRunEmhs.iterator(); it.hasNext(); ) {
        ReportRunEmhKey reportRunEmhKey = it.next();
        if (!emhs.contains(reportRunEmhKey.getEmhName())) {
          it.remove();

        }
      }
      Collections.sort(emhs);
    }
  }

  @Override
  public String getMainEMHName() {
    return null;
  }

  @Override
  public ReportContentType getType() {
    return ReportContentType.MULTI_VAR;
  }

  public ReportRunVariableConfig create(ReportRunVariableKey key) {
    ReportRunVariableConfig res = new ReportRunVariableConfig();
    res.setReportRunVariableKey(key);
    return res;
  }

  public void addEMH(String nom) {
    if (!emhs.contains(nom)) {
      emhs.add(nom);
    }
  }
}
