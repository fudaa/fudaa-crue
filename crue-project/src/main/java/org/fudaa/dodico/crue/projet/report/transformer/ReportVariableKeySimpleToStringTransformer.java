/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.transformer;

import java.util.Arrays;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;

/**
 * Nomme Simple car ne contient qu'un niveau.
 *
 * @author Frederic Deniger
 */
public class ReportVariableKeySimpleToStringTransformer extends AbstractPropertyToStringTransformer<ReportVariableKey> {

  Map<String, ReportVariableTypeEnum> typeById;

  public ReportVariableKeySimpleToStringTransformer() {
    super(ReportVariableKey.class);
  }

  @Override
  public String toStringSafe(ReportVariableKey in) {
    return in.getVariableName() + KeysToStringConverter.MINOR_SEPARATOR + in.getVariableType().getPersistId();
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }

  @Override
  public ReportVariableKey fromStringSafe(String in) {
    if (typeById == null) {
      typeById = TransformerHelper.toMapOfNom(Arrays.asList(ReportVariableTypeEnum.values()));
    }
    String[] split = StringUtils.split(in, KeysToStringConverter.MINOR_SEPARATOR);
    if (split.length == 2) {
      ReportVariableTypeEnum type = typeById.get(split[1]);
      if (type != null) {
        return new ReportVariableKey(type, split[0]);

      }
    }
    return ReportVariableKey.NULL;
  }
}
