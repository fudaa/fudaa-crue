package org.fudaa.dodico.crue.projet;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Permet de supprimer des containes
 *
 * @author deniger
 */
public class SaveScenariosCallable implements Callable<Boolean> {
  private final EMHProjetController emhProjetController;
  private boolean saveProject = true;
  private final List<String> scenarioNamesToSave;
  private final File etuFile;
  private CtuluLogGroup logs;

  public SaveScenariosCallable(EMHProjetController emhProjetController, List<String> scenarioIdToSave, File etuFile) {
    this.emhProjetController = emhProjetController;
    this.scenarioNamesToSave = scenarioIdToSave;
    this.etuFile = etuFile;
  }

  /**
   * @return les logs de l'opération
   */
  public CtuluLogGroup getLogs() {
    return logs;
  }

  /**
   * @return true si aucune erreur fatale
   */
  @Override
  public Boolean call() throws Exception {
    //Exectution en parallèle

    //on crée les Callable:
    List<ProjectLoadAndSaveCallable> callables = new ArrayList<>();
    for (String id : scenarioNamesToSave) {
      ManagerEMHScenario scenario = emhProjetController.getProjet().getScenario(id);
      if (scenario != null) {
        ProjectLoadAndSaveCallable callable = new ProjectLoadAndSaveCallable(emhProjetController.getProjet(), scenario, null, emhProjetController.getConnexionInformation());
        callables.add(callable);
      }
    }

    //on lance
    ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(Math.max(2, Runtime.getRuntime().availableProcessors() / 2));
    final List<Future<CtuluLogGroup>> futures = newFixedThreadPool.invokeAll(callables);

    //
    logs = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    for (Future<CtuluLogGroup> ctuluLogGroupFuture : futures) {
      final CtuluLogGroup logGroup = ctuluLogGroupFuture.get();
      if (logGroup != null && logGroup.containsSomething()) {
        logs.addGroup(logGroup);
      }
    }

    //on sauvegarde finalement les données dans le projet.
    if (saveProject) {
      emhProjetController.saveProjet(etuFile, logs);
    }
    return !logs.containsFatalError();
  }

  public void setSaveProject(boolean saveProject) {
    this.saveProject = saveProject;
  }

}
