/*
 GPL 2
 */
package org.fudaa.dodico.crue.edition.bean;

import org.fudaa.dodico.crue.metier.emh.EnumSectionType;

/**
 *
 * @author Frederic Deniger
 */
public class SectionEditionContent {

  private Double dz;
  private String comment;
  private String newName;
  private EnumSectionType newType;
  private Long sectionRefUid;

  public Long getSectionRefUid() {
    return sectionRefUid;
  }

  public void setSectionRefUid(Long sectionRef) {
    this.sectionRefUid = sectionRef;
  }

  public Double getDz() {
    return dz;
  }

  public void setDz(Double dz) {
    this.dz = dz;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getNewName() {
    return newName;
  }

  public void setNewName(String newName) {
    this.newName = newName;
  }

  public EnumSectionType getNewType() {
    return newType;
  }

  public void setNewType(EnumSectionType newType) {
    this.newType = newType;
  }
}
