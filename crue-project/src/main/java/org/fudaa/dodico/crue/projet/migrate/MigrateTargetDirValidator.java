/**
 * License GPL v2
 */

package org.fudaa.dodico.crue.projet.migrate;

import java.io.File;
import org.fudaa.ctulu.CtuluLog;

/**
 *
 * @author Frederic Deniger
 */
public class MigrateTargetDirValidator {

  private final File baseDir;

  public MigrateTargetDirValidator(File baseDir) {
    super();

    this.baseDir = baseDir;

    assert (baseDir != null) && baseDir.isDirectory();
  }

  public boolean valid(File targetDir, CtuluLog log) {
    return valid(targetDir, log, null);
  }

  public boolean valid(File targetDir, CtuluLog log, File destFile) {
    if (targetDir == null) {
      log.addSevereError("migrate.targetDir.isNull");

      return false;
    }

    //step 1: tester si dir file est correct. Ajouter message erreur dans mainAnalyse.
    //il est correct si non inclus dans repertoire de base
    if (targetDir.exists()) {
      if (!targetDir.isDirectory()) {
        log.addSevereError("migrate.targetDir.notDirectory");
      } else if (targetDir.listFiles().length > 0) {
        File[] listFiles = targetDir.listFiles();
        boolean ok = listFiles.length == 1 && listFiles[0].equals(destFile);
        if (!ok) {
          log.addSevereError("migrate.targetDir.notEmpty");
        }
      }
    }

    if (new ChildrenFilesTester(baseDir).isChild(targetDir)) {
      log.addSevereError("migrate.targetDir.isChild");
    }

    return !log.containsSevereError();
  }
}
