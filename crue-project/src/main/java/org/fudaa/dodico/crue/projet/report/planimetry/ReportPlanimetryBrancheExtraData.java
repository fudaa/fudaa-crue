/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.planimetry;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;
import org.fudaa.dodico.crue.projet.report.persist.VariableUpdateHelper;

/**
 * @author Frederic Deniger
 */
public class ReportPlanimetryBrancheExtraData extends AbstractReportPlanimetryConfigData {

  @XStreamAlias("FlecheVariable")
  private ReportVariableKey arrowVariable;
  @XStreamAlias("FlecheRatio")
  double arrowRatio = 1;

  public ReportPlanimetryBrancheExtraData() {

  }

  protected ReportPlanimetryBrancheExtraData(ReportPlanimetryBrancheExtraData from) {
    super(from);
    arrowVariable = from.arrowVariable;
    arrowRatio = from.arrowRatio;
  }

  @Override
  public ReportPlanimetryBrancheExtraData copy() {
    return new ReportPlanimetryBrancheExtraData(this);
  }

  public double getArrowRatio() {
    return arrowRatio;
  }

  @Override
  protected boolean specificVariableUpdated(FormuleDataChanges changes) {
    boolean res = false;
    Pair<ReportVariableKey, Boolean> variableUpdated = VariableUpdateHelper.variableUpdated(arrowVariable, changes);
    if (Boolean.TRUE.equals(variableUpdated.second)) {
      arrowVariable = variableUpdated.first;
      res = true;
    }
    return res;

  }

  public void setArrowRatio(double arrowRatio) {
    this.arrowRatio = arrowRatio;
  }

  public ReportVariableKey getArrowVariable() {
    return arrowVariable;
  }

  public void setArrowVariable(ReportVariableKey arrowVariable) {
    this.arrowVariable = arrowVariable;
  }
}
