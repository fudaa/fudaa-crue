package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateSourceTargetData;

import java.util.ArrayList;
import java.util.List;

public class SmUpdateDataLostInProfilIdentifier {
  private final CrueConfigMetier ccm;

  public SmUpdateDataLostInProfilIdentifier(CrueConfigMetier ccm) {
    this.ccm = ccm;
  }

  public CtuluIOResult<List<SmUpdateSourceTargetData.Line<CatEMHSection>>> generateLogsAndExtractNotLastData(List<SmUpdateSourceTargetData.Line<CatEMHSection>> sourcesToImport) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("sm.updater.ProfilSectionlostLog");
    List<SmUpdateSourceTargetData.Line<CatEMHSection>> notModified = new ArrayList<>();
    sourcesToImport.forEach(emh -> checkSection(emh, log, notModified));
    return new CtuluIOResult<>(log, notModified);
  }

  private void checkSection(SmUpdateSourceTargetData.Line<CatEMHSection> sectionLine, CtuluLog log, List<SmUpdateSourceTargetData.Line<CatEMHSection>> notModified) {
//    if (sectionLine.getSourceToImport().getBranche() == null && sectionLine.getTargetToModify().getBranche() != null) {
//      log.addWarn("sm.updater.sectionNotAttachedChanged", sectionLine.getSourceToImport().getNom());
//    }
    boolean lostData = false;
    if (sectionLine.getTargetToModify().getSectionType().equals(EnumSectionType.EMHSectionProfil) &&
      sectionLine.getSourceToImport().getSectionType().equals(EnumSectionType.EMHSectionProfil)) {
      final DonPrtGeoProfilSection sourceProfilSection = DonPrtHelper.getProfilSection((EMHSectionProfil) sectionLine.getSourceToImport());
      final DonPrtGeoProfilSection targetProfilSection = DonPrtHelper.getProfilSection((EMHSectionProfil) sectionLine.getTargetToModify());

      SmUpdateDonPrtEqualsHelper tester = new SmUpdateDonPrtEqualsHelper(ccm);
      if (!tester.isSame(sourceProfilSection, targetProfilSection)) {
        lostData = true;
        boolean hasFente = targetProfilSection.getFente() != null;
        final List<LitNumerote> sourceLitNumerotes = sourceProfilSection.getLitNumerote();
        final List<LitNumerote> targetLitNumerotes = targetProfilSection.getLitNumerote();

        if (targetLitNumerotes.size() > sourceLitNumerotes.size()) {
          String msg = hasFente ? "sm.updater.sectionProfilLitLostAndFente" : "sm.updater.sectionProfilLitLost";
          log.addWarn(msg, sectionLine.getSourceToImport().getNom(), targetLitNumerotes.size(), sourceLitNumerotes.size());
        } else {
          String msg = hasFente ? "sm.updater.sectionProfilChangedAndFente" : "sm.updater.sectionProfilChanged";
          log.addWarn(msg, sectionLine.getSourceToImport().getNom());
        }
      }
    }
    if (!lostData) {
      notModified.add(sectionLine);
    }
  }
}
