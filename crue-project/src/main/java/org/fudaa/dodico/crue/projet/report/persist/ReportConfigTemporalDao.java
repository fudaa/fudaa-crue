/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.persist;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("TemporelConfig")
public class ReportConfigTemporalDao extends AbstractReportConfigDao<ReportTemporalConfig> {

  @XStreamAlias("Temporel")
  private ReportTemporalConfig config;

  public ReportConfigTemporalDao() {
  }

  public ReportConfigTemporalDao(ReportTemporalConfig config) {
    this.config = config;
  }

  @Override
  public ReportTemporalConfig getConfig() {
    return config;
  }

  @Override
  public void setConfig(ReportTemporalConfig config) {
    this.config = config;
  }
}
