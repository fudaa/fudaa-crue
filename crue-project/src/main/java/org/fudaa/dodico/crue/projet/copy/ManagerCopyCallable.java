package org.fudaa.dodico.crue.projet.copy;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.metier.etude.*;

import java.io.File;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * @author deniger
 */
public class ManagerCopyCallable implements Callable<Boolean> {
  private final CopyInformations infos;
  private final boolean overwrite;
  private final ConnexionInformation connexionInformation;
  CtuluLog operationLog;

  public ManagerCopyCallable(CopyInformations infos, boolean overwrite, ConnexionInformation connexionInformation) {
    this.infos = infos;
    this.overwrite = overwrite;
    this.connexionInformation = connexionInformation;
  }

  public CtuluLog getOperationLog() {
    return operationLog;
  }

  @Override
  public Boolean call() throws Exception {
    operationLog = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);

    if (this.infos.rootContainer != null) {
      return this.copy(this.infos.rootContainer, overwrite) != null;
    }
    if (this.infos.rootFile != null) {
      return this.copy(this.infos.rootFile, overwrite) != null;
    }

    return true;
  }

  private ManagerEMHContainerBase copy(ManagerEMHContainerBase container, boolean overwriteFiles) {
    ManagerEMHContainerBase destContainer = null;

    if (this.infos.deepCopy) {
      destContainer = this.copyContainer(container, true);

      if (container instanceof ManagerEMHContainer<?>) {
        final List<ManagerEMHContainerBase> children = ((ManagerEMHContainer) container).getFils();

        for (ManagerEMHContainerBase child : children) {
          ManagerEMHContainerBase destChild = this.copy(child, overwriteFiles);

          if (destChild == null) {
            return null;
          }
          if (destContainer != null) {
            ((ManagerEMHContainer) destContainer).addManagerFils(destChild);
          }
        }
      }

      final FichierCrueManager files = container.getListeFichiers();

      if (files != null) {
        for (FichierCrue file : files.getFichiers()) {
          FichierCrue destFile = this.copy(file, overwriteFiles);
          if (destFile == null) {
            return null;
          }
          if (destContainer != null) {
            destContainer.addFichierDonnees(destFile);
          }
        }
      }

      if (destContainer != null) {
        this.addToProject(destContainer);
      }
    } else {
      destContainer = this.copyContainer(container, false);
    }

    if (destContainer != null) {
      this.addToProject(destContainer);
      //on copie ici les fichier de configuration de niveau sous-modele et/ou scenario
      File initConfig = infos.sourceProject.getInfos().getDirOfConfig(container);
      if (CtuluLibFile.exists(initConfig)) {
        File destConfig = infos.sourceProject.getInfos().getDirOfConfig(destContainer);
        CrueFileHelper.copyDirWithSameLastDate(initConfig, destConfig);
      }
    }

    return destContainer;
  }

  private void addToProject(ManagerEMHContainerBase container) {
    if (container instanceof ManagerEMHScenario) {
      this.infos.sourceProject.addScenario((ManagerEMHScenario) container);
    } else if (container instanceof ManagerEMHModeleBase) {
      this.infos.sourceProject.addBaseModele((ManagerEMHModeleBase) container);
    } else if (container instanceof ManagerEMHSousModele) {
      this.infos.sourceProject.addBaseSousModele((ManagerEMHSousModele) container);
    }
  }

  private ManagerEMHContainerBase copyContainer(ManagerEMHContainerBase srcContainer, boolean copyEmpty) {
    ManagerEMHContainerBase destContainer = null;

    if (srcContainer instanceof ManagerEMHScenario) {
      final ManagerEMHScenario destScenario = new ManagerEMHScenario(this.infos.containersToCopy.get(srcContainer));
      if (srcContainer.isCrue10() && infos.sourceProject.getCoeurConfig().isCrue9Dependant()) {
        destScenario.setLinkedscenarioCrue9(((ManagerEMHScenario) srcContainer).getLinkedscenarioCrue9());
      }

      if (!copyEmpty) {
        final List<ManagerEMHModeleBase> children = ((ManagerEMHScenario) srcContainer).getFils();

        for (ManagerEMHModeleBase child : children) {
          destScenario.addManagerFils(child);
        }
      }

      destContainer = destScenario;
    } else if (srcContainer instanceof ManagerEMHModeleBase) {
      final ManagerEMHModeleBase destModele = new ManagerEMHModeleBase(this.infos.containersToCopy.get(srcContainer));

      if (!copyEmpty) {
        final List<ManagerEMHSousModele> children = ((ManagerEMHModeleBase) srcContainer).getFils();

        for (ManagerEMHSousModele child : children) {
          destModele.addManagerFils(child);
        }
      }

      destContainer = destModele;
    } else if (srcContainer instanceof ManagerEMHSousModele) {
      final ManagerEMHSousModele destSousModele = new ManagerEMHSousModele(this.infos.containersToCopy.get(srcContainer));

      destContainer = destSousModele;
    }

    if (!copyEmpty) {
      final FichierCrueManager files = srcContainer.getListeFichiers();
      if (files != null && destContainer != null) {
        destContainer.setListeFichiers(files.getFichiers());
      }
    }

    final EMHInfosVersion infosVersion = new EMHInfosVersion();

    infosVersion.setVersion(srcContainer.getInfosVersions().getCrueVersion());
    infosVersion.setAuteurCreation(connexionInformation.getCurrentUser());
    infosVersion.setDateCreation(connexionInformation.getCurrentDate());
    infosVersion.setAuteurDerniereModif(infosVersion.getAuteurCreation());
    infosVersion.setDateDerniereModif(infosVersion.getDateCreation());
    infosVersion.setCommentaire(srcContainer.getInfosVersions().getCommentaire());
    if (destContainer != null) {
      destContainer.setInfosVersions(infosVersion);
    }

    return destContainer;
  }

  private FichierCrue copy(FichierCrue file, boolean overwriteFile) {
    return this.infos.sourceProject.copyFile(file, this.infos.filesToCopy.get(file), overwriteFile, operationLog);
  }
}
