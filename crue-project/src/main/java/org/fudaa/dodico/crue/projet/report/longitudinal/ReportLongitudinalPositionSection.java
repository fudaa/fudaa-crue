/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.longitudinal;

import org.fudaa.dodico.crue.metier.emh.CatEMHSection;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLongitudinalPositionSection {

  private final double xpDisplay;
  private final CatEMHSection name;

  public ReportLongitudinalPositionSection(double xp, CatEMHSection section) {
    this.xpDisplay = xp;
    this.name = section;
  }

  public double getXpDisplay() {
    return xpDisplay;
  }

  public CatEMHSection getName() {
    return name;
  }
}
