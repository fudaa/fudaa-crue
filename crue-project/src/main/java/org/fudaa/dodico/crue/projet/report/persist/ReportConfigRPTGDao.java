/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.persist;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.fudaa.dodico.crue.projet.report.ReportRPTGConfig;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("PretraitementConfig")
public class ReportConfigRPTGDao extends AbstractReportConfigDao<ReportRPTGConfig> {

  @XStreamAlias("Pretraitement")
  private ReportRPTGConfig config;

  public ReportConfigRPTGDao() {
  }

  public ReportConfigRPTGDao(ReportRPTGConfig config) {
    this.config = config;
  }

  @Override
  public ReportRPTGConfig getConfig() {
    return config;
  }

  @Override
  public void setConfig(ReportRPTGConfig config) {
    this.config = config;
  }
}
