/*
 GPL 2
 */
package org.fudaa.dodico.crue.edition;

import gnu.trove.TIntHashSet;
import org.fudaa.dodico.crue.comparaison.CrueComparatorHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.comparator.Point2dAbscisseComparator;
import org.fudaa.dodico.crue.metier.emh.*;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.simplify.DouglasPeuckerSimplifier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public class DonPrtGeoProfilSimplifier {
  private final double tolerance;
  private final CrueConfigMetier ccm;

  public DonPrtGeoProfilSimplifier(double tolerance, CrueConfigMetier ccm) {
    this.tolerance = tolerance;
    this.ccm = ccm;
  }

  TIntHashSet getIndexes(DonPrtGeoProfilSection profil) {
    PropertyEpsilon epsilon = ccm.getConfLoi().get(EnumTypeLoi.LoiPtProfil).getVarAbscisse().getEpsilon();
    Point2dAbscisseComparator ptProfilComparator = new Point2dAbscisseComparator(epsilon);
    TIntHashSet set = new TIntHashSet();
    set.add(0);
    set.add(profil.getPtProfilSize() - 1);
    for (DonPrtGeoProfilEtiquette etiquette : profil.getEtiquettes()) {
      set.add(CrueComparatorHelper.getEqualsPtProfilSorted(etiquette.getPoint(), profil.getPtProfil(), ptProfilComparator));
    }
    for (LitNumerote litNumerote : profil.getLitNumerote()) {
      set.add(CrueComparatorHelper.getEqualsPtProfilSorted(litNumerote.getLimDeb(), profil.getPtProfil(), ptProfilComparator));
      set.add(CrueComparatorHelper.getEqualsPtProfilSorted(litNumerote.getLimFin(), profil.getPtProfil(), ptProfilComparator));
    }

    return set;
  }

  TIntHashSet getIndexes(DonPrtGeoProfilCasier profil) {
    PropertyEpsilon epsilon = ccm.getConfLoi().get(EnumTypeLoi.LoiPtProfil).getVarAbscisse().getEpsilon();
    Point2dAbscisseComparator ptProfilComparator = new Point2dAbscisseComparator(epsilon);
    TIntHashSet set = new TIntHashSet();
    set.add(0);
    set.add(profil.getPtProfilSize() - 1);
    if (profil.getLitUtile() != null) {
      set.add(CrueComparatorHelper.getEqualsPtProfilSorted(profil.getLitUtile().getLimDeb(), profil.getPtProfil(), ptProfilComparator));
      set.add(CrueComparatorHelper.getEqualsPtProfilSorted(profil.getLitUtile().getLimFin(), profil.getPtProfil(), ptProfilComparator));
    }
    return set;
  }

  public DonPrtGeoProfilSection simplify(DonPrtGeoProfilSection init) {
    if (init == null) {
      return null;
    }
    DonPrtGeoProfilSection res = new DonPrtGeoProfilSection();
    res.initDataFrom(init);
    TIntHashSet indexes = getIndexes(init);
    int[] toArray = indexes.toArray();
    Arrays.sort(toArray);
    final List<PtProfil> initPts = init.getPtProfil();
    res.setPtProfil(createSimplifiedPtProfil(initPts, toArray));
    return res;
  }

  public DonPrtGeoProfilCasier simplify(DonPrtGeoProfilCasier init) {
    if (init == null) {
      return null;
    }
    DonPrtGeoProfilCasier res = init.cloneProfilCasier();
    TIntHashSet indexes = getIndexes(init);
    int[] toArray = indexes.toArray();
    Arrays.sort(toArray);
    final List<PtProfil> initPts = init.getPtProfil();
    res.setPtProfil(createSimplifiedPtProfil(initPts, toArray));
    return res;
  }

  protected List<PtProfil> createSimplifiedPtProfil(
      final List<PtProfil> initPts, int[] toArray) {
    List<PtProfil> pts = new ArrayList<>();
    //ajout du premier point
    pts.add(initPts.get(0).clone());
    for (int i = 1; i < toArray.length; i++) {
      int firstIdx = toArray[i - 1];
      int lastIdx = toArray[i];
      int nb = lastIdx - firstIdx + 1;
      Coordinate[] cs = new Coordinate[nb];
      for (int j = firstIdx; j <= lastIdx; j++) {
        final PtProfil initPt = initPts.get(j);
        cs[j - firstIdx] = new Coordinate(initPt.getAbscisse(), initPt.getOrdonnee());
      }

      Geometry simplifiedLine = DouglasPeuckerSimplifier.simplify(new GeometryFactory().createLineString(cs), tolerance);
      Coordinate[] simplifiedCoordinates = simplifiedLine.getCoordinates();
      //on commence de 1 car le premier point est ajouté par l'iteration précédente
      for (int j = 1; j < simplifiedCoordinates.length - 1; j++) {
        pts.add(new PtProfil(simplifiedCoordinates[j].x, simplifiedCoordinates[j].y));
      }
      //ajout du dernier point
      pts.add(initPts.get(lastIdx).clone());
    }
    return pts;
  }
}
