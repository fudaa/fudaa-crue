/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.*;

/**
 * @author deniger
 */
public class EditionCasierCreator {
  final EditionProfilCreator profilCreator;
  private final boolean createInfoEMHs;

  public EditionCasierCreator(final EditionProfilCreator profilCreator) {
    this.profilCreator = profilCreator;
    createInfoEMHs = true;
  }

  public EditionCasierCreator(final EditionProfilCreator profilCreator, boolean createInfoEMHs) {
    this.profilCreator = profilCreator;
    this.createInfoEMHs = createInfoEMHs;
  }

  public CatEMHCasier createCasier(final String name, final EnumCasierType casierType, final CrueConfigMetier ccm, final EMHSousModele sousModele) {

    final CatEMHCasier casier = createNewInstanceCasier(name, casierType, ccm, sousModele);
    EditionCreateEMH.addInScenario(sousModele, casier, sousModele.getParent().getParent());
    return casier;
  }

  private CatEMHCasier createNewInstanceCasier(final String name, final EnumCasierType casierType, final CrueConfigMetier ccm,
                                               final EMHSousModele sousModele) {
    switch (casierType) {
      case EMHCasierProfil:
        return createEMHCasierProfil(name, ccm, sousModele);
    }
    throw new IllegalAccessError("casierType not supported" + casierType);
  }

  private EMHCasierProfil createEMHCasierProfil(final String name, final CrueConfigMetier ccm, final EMHSousModele sousModele) {
    final EMHCasierProfil res = new EMHCasierProfil(name);
    if (createInfoEMHs) {
      res.addInfosEMH(new DonCalcSansPrtCasierProfil(ccm));
      res.addInfosEMH(new DonPrtCIniCasierProfil(ccm));
      final EMHScenario scenario = sousModele.getParent().getParent();
      final DonPrtGeoProfilCasier profileCasier = profilCreator.createProfilCasier(ccm, name, scenario);
      res.addInfosEMH(profileCasier);
    }
    return res;
  }
}
