package org.fudaa.dodico.crue.edition;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.FileDeleteResult;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.projet.calcul.RunScenarioPair;
import org.fudaa.dodico.crue.projet.select.SelectableFinder;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class EditionDeleteContainer {
  private final EMHProjetController projectController;

  public EditionDeleteContainer(EMHProjetController projectController) {
    this.projectController = projectController;
  }

  public static CtuluLogGroup createBilan(DeleteContainersCallable action, String title) {
    CtuluLogGroup logGroup = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    logGroup.setDescription(title);
    if (CollectionUtils.isNotEmpty(action.notDeletableFiles)) {
      final CtuluLog logFileNotDeleted = logGroup.createNewLog("deleteFile.bilanTitle");
      for (String notDeletableFile : action.notDeletableFiles) {
        logFileNotDeleted.addError(notDeletableFile);
      }
    }
    if (MapUtils.isNotEmpty(action.runsNotCleaned)) {
      logGroup.addGroup(createLogsIfRunNotDeleted(action.runsNotCleaned));
    }
    return logGroup.createCleanGroup();
  }

  public static CtuluLogGroup createLogsIfRunNotDeleted(Map<RunScenarioPair, FileDeleteResult> managers) {
    CtuluLogGroup log = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    log.setDescription("deleteRun.bilanTitle");
    for (Map.Entry<RunScenarioPair, FileDeleteResult> entry : managers.entrySet()) {
      CtuluLog createLog = log.createNewLog("deleteRun.cantDeleteRun");
      createLog.setDescriptionArgs(entry.getKey().getScenario().getNom(), entry.getKey().getRun().getNom());
      if (!entry.getValue().getDirNotDeleted().isEmpty()) {
        for (File dirNotDeleted : entry.getValue().getDirNotDeleted()) {
          createLog.addWarn(BusinessMessages.getString("deleteRunFolder.FailedForDir", dirNotDeleted.getAbsolutePath()));
        }
      }
      if (!entry.getValue().getFilesNotDeleted().isEmpty()) {
        for (File fileNotDeleted : entry.getValue().getFilesNotDeleted()) {
          createLog.addWarn(BusinessMessages.getString("deleteRunFolder.FailedForFile", fileNotDeleted.getAbsolutePath()));
        }
      }
    }
    return log;
  }

  /**
   * @param emhContainerBase le container a supprimer
   * @param deep true si en cascade
   * @return le resultat de l'opération
   */
  public CtuluLogGroup delete(ManagerEMHContainerBase emhContainerBase, boolean deep) {
    final SelectableFinder finder = new SelectableFinder();
    finder.setProject(projectController.getProjet());
    CtuluLogGroup res = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    final SelectableFinder.SelectedItems items = finder.getIndependantSelection(Collections.singletonList(emhContainerBase), deep);
    DeleteContainersCallable deleteCallable = new DeleteContainersCallable(items, projectController);
    CtuluLog log = res.createLog();
    log.setDesc("containerDelete.action");
    log.setDescriptionArgs(emhContainerBase.getNom());
    try {
      Boolean result = deleteCallable.call();
      if (Boolean.FALSE.equals(result)) {
        log.addError("containerDelete.fail", emhContainerBase.getNom());
      }
    } catch (Exception e) {
      log.addError("containerDelete.error", e.getMessage());
      Logger.getLogger(getClass().getName()).log(Level.SEVERE, "error while delete container", e);
    }
    final List<String> notDeletableFiles = deleteCallable.getNotDeletableFiles();
    if (CollectionUtils.isNotEmpty(notDeletableFiles)) {
      for (String notDeletableFile : notDeletableFiles) {
        log.addError("containerDelete.cantDeleteFile", notDeletableFile);
      }
    }
    if (MapUtils.isNotEmpty(deleteCallable.getRunsNotCleaned())) {
      res.addGroup(createLogsIfRunNotDeleted(deleteCallable.getRunsNotCleaned()));
    }
    return res;
  }
}
