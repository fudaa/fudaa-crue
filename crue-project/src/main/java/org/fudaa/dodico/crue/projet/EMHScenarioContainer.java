/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

/**
 * @author deniger
 */
public class EMHScenarioContainer {

  private final ManagerEMHScenario managerEMHScenario;
  private final EMHScenario emhScenario;

  public EMHScenarioContainer(final ManagerEMHScenario managerEMHScenario, final EMHScenario emhScenarioLoaded) {
    super();
    this.managerEMHScenario = managerEMHScenario;
    this.emhScenario = emhScenarioLoaded;
  }

  public ManagerEMHScenario getManagerEMHScenario() {
    return managerEMHScenario;
  }

  public EMHScenario getEmhScenario() {
    return emhScenario;
  }

}
