package org.fudaa.dodico.crue.batch;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.CoeurManager;
import org.fudaa.dodico.crue.projet.conf.Configuration;
import org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager;

import java.io.File;

/**
 * @author deniger
 */
public class BatchLauncher {
  public static BatchData prepare(BatchLogSaver saver, String messageFileLoaded, String otfaFileAbsolutePath) {
    CtuluLog logLoaderData = saver.start(true);
    BatchData data = new BatchData();
    data.setLog(logLoaderData);
    logLoaderData.addInfo(messageFileLoaded, otfaFileAbsolutePath);
    if (logLoaderData.containsErrorOrSevereError()) {
      return data;
    }
    String property = System.getProperty(BatchHelper.SITE_CONFIG_PROP);

    if (property == null) {
      logLoaderData.addSevereError("batch.siteConfigDirNotSet");
      saver.close();
      return data;
    }
    File etc = new File(property).getAbsoluteFile();
    if (!etc.exists()) {
      logLoaderData.addSevereError("batch.siteConfigDirNotExist", etc.getAbsolutePath());
      saver.close();
      return data;
    }

    File siteFile = new File(etc, GlobalOptionsManager.SITE_FILE);
    if (!siteFile.exists()) {
      logLoaderData.addSevereError("batch.siteConfigFileNotExist", siteFile.getAbsolutePath());
      saver.close();
      return data;
    }
    Configuration configuration = BatchHelper.readConfigSite(siteFile, logLoaderData);
    if (configuration == null || logLoaderData.containsSevereError()) {
      logLoaderData.addSevereError("batch.siteConfigFileCantBeRead", siteFile.getAbsolutePath());
      saver.close();
      return data;
    }
    CoeurManager coeurManager = BatchHelper.loadCoeur(etc, configuration, logLoaderData);
    if (coeurManager == null) {
      logLoaderData.addSevereError("batch.errorWhileReadingCoeurs", etc.getAbsolutePath());
      saver.close();
      return data;
    }

    data.setConfiguration(configuration);
    data.setCoeurManager(coeurManager);
    return data;
  }
}
