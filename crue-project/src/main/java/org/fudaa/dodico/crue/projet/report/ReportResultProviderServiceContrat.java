/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report;

import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunContent;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;

import java.util.Collection;
import java.util.Collections;

/**
 * Contrat du service permettant de fournir les résultats du run en cours
 *
 * @author Frederic Deniger
 */
public interface ReportResultProviderServiceContrat {
    Collection<ResultatTimeKey> NO_SELECTED_TIMES_REPORT = Collections.emptyList();

    /**
     * @param varName le nom de la variable
     * @return la variable initialisée correctement
     */
    ReportVariableKey createVariableKey(String varName);

    /**
     * @param key                    la clé de la variable ( nom +id du run)
     * @param emhNom                 le nom de l'EMH
     * @param selectedTimesInRapport les pas de temps sélectionné dans l'interface
     * @return la valeur demandée. null si non trouvée
     */
    Double getValue(ReportRunVariableKey key, String emhNom, Collection<ResultatTimeKey> selectedTimesInRapport);

    /**
     * @param selectedTime           le temps sélectionné dans la perspective
     * @param key                    la clé de la variable ( nom +id du run)
     * @param emhNom                 le nom de l'EMH support
     * @param selectedTimesInRapport les pas de temps sélectionné dans l'interface
     * @return la valeur demandée. null si non trouvée
     */
    Double getValue(ResultatTimeKey selectedTime, ReportRunVariableKey key, String emhNom, Collection<ResultatTimeKey> selectedTimesInRapport);

    /**
     * @return le run courant
     */
    ReportRunContent getRunCourant();

    /**
     * @param nom nom de l'EMH
     * @param key le run parent (donc scenario)
     * @return l'EMH demandé
     */
    EMH getEMH(String nom, ReportRunKey key);

    /**
     * @return le CCM courant
     */
    CrueConfigMetier getCcm();

    /**
     * @return l'ui ( text or Netbeans RCP)
     */
    CtuluUI getUI();

    /**
     * @param key l'identifiant de la nature
     * @return la nature correspondante ou null si non trouvée.
     */
    PropertyNature getPropertyNature(String key);
}
