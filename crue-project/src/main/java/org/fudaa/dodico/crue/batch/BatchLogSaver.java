package org.fudaa.dodico.crue.batch;

import org.fudaa.ctulu.CtuluLog;

/**
 * @author deniger
 */
public interface BatchLogSaver {
  void close();

  CtuluLog start(boolean isBatch);
}
