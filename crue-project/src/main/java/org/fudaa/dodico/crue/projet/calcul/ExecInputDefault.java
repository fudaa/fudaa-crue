/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.calcul;

import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

import java.io.File;

/**
 * Données nécessaire pour le lancement d'un run.
 *
 * @author deniger
 */
public class ExecInputDefault implements ExecInput {
  private final EMHProjet projet;
  private final ManagerEMHScenario scenario;
  private final EMHRun run;
  private final EMHRun runReference;

  /**
   * @param projet le projet parent
   * @param scenario le scenario parent
   * @param run le run
   */
  public ExecInputDefault(final EMHProjet projet, final ManagerEMHScenario scenario, final EMHRun run) {
    this(projet, scenario, run, null);
  }

  /**
   * @param projet le projet parent
   * @param scenario le scenario parent
   * @param run le run
   */
  public ExecInputDefault(final EMHProjet projet, final ManagerEMHScenario scenario, final EMHRun run, final EMHRun runReference) {
    this.projet = projet;
    this.scenario = scenario;
    this.run = run;
    this.runReference = runReference;
  }

  /**
   * Utiliser dans le cas ou le calcul ne lance aucun service
   * @return le run de reference utilise pour le lancement
   */
  public EMHRun getRunReference() {
    return runReference;
  }

  /**
   * @return le repertoire du run
   */
  public File getRunDir() {
    return getProjet().getDirForRun(getScenario(), getRun());
  }

  /**
   * @return le projet parent
   */
  @Override
  public EMHProjet getProjet() {
    return projet;
  }

  /**
   * @return le run
   */
  @Override
  public EMHRun getRun() {
    return run;
  }

  /**
   * @return le scenario parent
   */
  @Override
  public ManagerEMHScenario getScenario() {
    return scenario;
  }
}
