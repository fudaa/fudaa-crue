/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.create;

import java.util.List;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;

/**
 * @author CANEL Christophe
 *
 */
public class LevelDataFactory {
  public interface NameConverter
  {
    String convert(String name);
  }
  
  public static ContainerLevelData createScenarioLevelData(final String name, final CrueVersionType version)
  {
    final ContainerLevelData scenario   = LevelDataFactory.createLevelData(name, version, CrueLevelType.SOUS_MODELE);
    final ContainerLevelData modele     = LevelDataFactory.createLevelData(name, version, CrueLevelType.MODELE     );
    final ContainerLevelData sousModele = LevelDataFactory.createLevelData(name, version, CrueLevelType.SOUS_MODELE);
    
    modele.addChildren(sousModele);
    scenario.addChildren(modele);
    
    return scenario;
  }
  
  public static ContainerLevelData createCopyScenarioLevelData(final ManagerEMHScenario source, final CrueVersionType version, final NameConverter converter)
  {
    final ContainerLevelData scenario = LevelDataFactory.createLevelData(converter.convert(LevelDataFactory.extractName(source.getNom(), CrueLevelType.SCENARIO)), version, CrueLevelType.SCENARIO);
    final List<ManagerEMHModeleBase> modeles = source.getFils();

    if ((modeles.size() > 1) && (version == CrueVersionType.CRUE9))
    {
      return null;
    }
    
    for (final ManagerEMHModeleBase modele : modeles)
    {
      scenario.addChildren(LevelDataFactory.createCopyModeleLevelData(modele, version, converter));
    }
    
    return scenario;
  }
  
  public static ContainerLevelData createCopyModeleLevelData(final ManagerEMHModeleBase source, final CrueVersionType version, final NameConverter converter)
  {
    final String name = converter.convert(LevelDataFactory.extractName(source.getNom(), CrueLevelType.MODELE));
    final ContainerLevelData modele = LevelDataFactory.createLevelData(name, version, CrueLevelType.MODELE);
    final List<ManagerEMHSousModele> sousModeles = source.getFils();

    for (final ManagerEMHSousModele sousModele : sousModeles)
    {
      modele.addChildren(LevelDataFactory.createCopySousModeleLevelData(sousModele, version, converter));
    }

    if ((sousModeles.isEmpty()) && (version == CrueVersionType.CRUE10))
    {
      modele.addChildren(LevelDataFactory.createLevelData(name, version, CrueLevelType.SOUS_MODELE));
    }
    
    return modele;
  }

  public static ContainerLevelData createCopySousModeleLevelData(final ManagerEMHSousModele source, final CrueVersionType version, final NameConverter converter)
  {
    return LevelDataFactory.createLevelData(converter.convert(LevelDataFactory.extractName(source.getNom(), CrueLevelType.SOUS_MODELE)), version, CrueLevelType.SOUS_MODELE);
  }
  
  public static ContainerLevelData createLevelData(final String name, final CrueVersionType version, final CrueLevelType level)
  {
    final ContainerLevelData data = new ContainerLevelData(level.getPrefix() + name, level);

    for (final CrueFileType type : CrueFileType.values())
    {
      if ((type.getLevel()           == level  ) &&
          (type.getCrueVersionType() == version) &&
          (!type.isResultFileType()            )) {
        data.addFichierCrue(new FichierCrue(name + "." + type.getExtension(), null, type));
      }
    }
    
    return data;
  }
  
  private static String extractName(final String name, final CrueLevelType level)
  {
    return name.replaceFirst(level.getPrefix(), "");
  }
}
