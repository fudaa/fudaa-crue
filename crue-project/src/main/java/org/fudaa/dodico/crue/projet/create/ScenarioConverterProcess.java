/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.create;

import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.projet.ScenarioLoader;
import org.fudaa.dodico.crue.projet.ScenarioLoaderOperation;
import org.fudaa.dodico.crue.projet.create.LevelDataFactory.NameConverter;

/**
 * @author CANEL Christophe
 *
 */
public class ScenarioConverterProcess {

  private final EMHProjet projet;

  public ScenarioConverterProcess(EMHProjet projet) {
    super();
    this.projet = projet;
  }

  public CrueOperationResult<EMHScenarioContainer> convert(ManagerEMHScenario sourceScenario, final InfosCreation infos) {
    final NameConverter converter = new ConversionNameConverter(infos.getCrueVersion());

    final ContainerLevelData data = LevelDataFactory.createCopyScenarioLevelData(sourceScenario, infos.getCrueVersion(), converter);

    if (data == null) {
      CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);

      log.addSevereError("create.scenarioConverter.cannotConvert");

      return new CrueOperationResult<>(null, log);
    }

    final ScenarioCreator creator = new ScenarioCreator(projet);
    final CrueOperationResult<ManagerEMHScenario> resultManager = creator.create(data, infos);

    final CtuluLogGroup errors = resultManager.getLogs();
    EMHScenario emhScenario = null;
    if (!errors.containsFatalError()) {
      ManagerEMHScenario targetScenario = resultManager.getResult();
      if (targetScenario.isCrue10() && projet.getCoeurConfig().isCrue9Dependant()) {
        if (sourceScenario.isCrue9()) {
          targetScenario.setLinkedscenarioCrue9(sourceScenario.getNom());
        } else {
          targetScenario.setLinkedscenarioCrue9(sourceScenario.getLinkedscenarioCrue9());
        }
      }

      ScenarioLoader loader = new ScenarioLoader(sourceScenario, projet, projet.getCoeurConfig());
      ScenarioLoaderOperation load = loader.load(null);

      errors.addGroup(load.getLogs());

      if (!load.getLogs().containsFatalError()) {
        emhScenario = load.getResult();

        this.setEmhNom(targetScenario, emhScenario);
      }
    }
    EMHScenarioContainer res = new EMHScenarioContainer(resultManager.getResult(), emhScenario);
    return new CrueOperationResult<>(res, resultManager.getLogs());
  }

  private void setEmhNom(ManagerEMHScenario scenario, EMHScenario emh) {
    final List<ManagerEMHModeleBase> modeles = scenario.getFils();
    final List<EMHModeleBase> emhs = emh.getModeles();
    final int nbModeles = modeles.size();

    emh.setNom(scenario.getNom());

    if (nbModeles == emhs.size()) {
      for (int i = 0; i < nbModeles; i++) {
        this.setEmhNom(modeles.get(i), emhs.get(i));
      }
    }
  }

  private void setEmhNom(ManagerEMHModeleBase modele, EMHModeleBase emh) {
    final List<ManagerEMHSousModele> sousModeles = modele.getFils();
    final List<EMHSousModele> emhs = emh.getSousModeles();
    final int nbSousModeles = sousModeles.size();

    emh.setNom(modele.getNom());

    if (nbSousModeles == emhs.size()) {
      for (int i = 0; i < nbSousModeles; i++) {
        this.setEmhNom(sousModeles.get(i), emhs.get(i));
      }
    }
  }

  private void setEmhNom(ManagerEMHSousModele sousModele, EMHSousModele emh) {
    emh.setNom(sousModele.getNom());
  }
}
