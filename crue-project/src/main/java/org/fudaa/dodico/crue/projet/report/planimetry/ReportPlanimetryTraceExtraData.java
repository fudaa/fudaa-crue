/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.planimetry;

import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;

/**
 * @author Frederic Deniger
 */
public class ReportPlanimetryTraceExtraData extends AbstractReportPlanimetryConfigData {

  public ReportPlanimetryTraceExtraData() {

  }

  protected ReportPlanimetryTraceExtraData(ReportPlanimetryTraceExtraData from) {
    super(from);
  }

  @Override
  public ReportPlanimetryTraceExtraData copy() {
    return new ReportPlanimetryTraceExtraData(this);
  }

  @Override
  protected boolean specificVariableUpdated(FormuleDataChanges changes) {
    return false;
  }
}
