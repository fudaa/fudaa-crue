/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.*;

import java.util.List;
import java.util.Map;

/**
 * @author Frederic Deniger
 */
public class EditionUpdateCalc {
    public CtuluLog updateCalcs(final List<Calc> newCalcs, final List<OrdCalc> newOrdCalc, final EMHScenario parent) {
        final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
        final IdRegistry idRegistry = parent.getIdRegistry();
        for (final Calc calc : newCalcs) {
            idRegistry.register(calc);
        }
        final DonCLimMScenario dclm = parent.getDonCLimMScenario();
        dclm.removeAllCalcPseudoPerm();
        dclm.removeAllCalcTrans();
        final List<EMH> allSimpleEMH = parent.getAllSimpleEMH();
        for (final EMH emh : allSimpleEMH) {
            emh.removeInfosEMH(emh.getDCLM());
        }
        final Map<String, Loi> loiByNames = TransformerHelper.toMapOfNom(
                parent.getLoiConteneur().getLois());

        for (final Calc calc : newCalcs) {
            if (calc.isPermanent()) {
                dclm.addCalcPseudoPerm((CalcPseudoPerm) calc);
            } else {
                dclm.addCalcTrans((CalcTrans) calc);
            }
            final List<DonCLimM> listeDCLM = calc.getlisteDCLM();
            for (final DonCLimM emhDclm : listeDCLM) {
                assert emhDclm.getCalculParent() == calc;
                if (emhDclm instanceof CalcTransItem) {
                    final CalcTransItem transemhDclm = (CalcTransItem) emhDclm;
                    final Loi loi = loiByNames.get(transemhDclm.getLoi().getNom());
                    assert loi != null;
                    transemhDclm.setLoi(loi);
                }
                emhDclm.getEmh().addInfosEMH(emhDclm);
            }
        }
        final OrdCalcScenario ocal = parent.getOrdCalcScenario();
        ocal.removeAllOrdCalc();
        for (final OrdCalc ordCalc : newOrdCalc) {
            ocal.addOrdCalc(ordCalc);
        }
        return log;
    }

    public void removeActiveDclm(final EMH emh, final Class dclm) {
        final DonCLimM activeDclm = getActiveDclm(emh, dclm);
        if (activeDclm != null) {
            activeDclm.getCalculParent().removeDclm(activeDclm);
            emh.removeInfosEMH(activeDclm);
        }
    }

    /**
     * @param emh       l'EMH
     * @param dclmClass une classe fille de ObjetUserActivable
     * @return la dclm active dl'EMH emh
     */
    private DonCLimM getActiveDclm(final EMH emh, final Class dclmClass) {
        final List<DonCLimM> dclms = emh.getDCLM();
        for (final DonCLimM dclm : dclms) {
            if (dclmClass.equals(dclm.getClass())) {
                if (((ObjetUserActivable) dclm).getUserActive()) {
                    return dclm;
                }
            }
        }
        return null;
    }
}
