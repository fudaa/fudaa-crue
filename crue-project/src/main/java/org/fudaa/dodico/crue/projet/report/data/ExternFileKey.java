/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("ExternFileKey")
public class ExternFileKey implements ReportKeyContract {

  private final String file;
  private final String xVar;
  private final String yVar;
  public static final ExternFileKey NULL = new ExternFileKey(StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);

  public ExternFileKey(String file, String xVar, String yVar) {
    this.file = file;
    this.xVar = xVar;
    this.yVar = yVar;
  }

  @Override
  public String getVariableName() {
    return null;
  }

  @Override
  public ReportKeyContract duplicateWithNewVar(String newName) {
    return this;
  }

  public String getFile() {
    return file;
  }

  public String getxVar() {
    return xVar;
  }

  public String getyVar() {
    return yVar;
  }

  @Override
  public boolean isExtern() {
    return true;
  }

  @Override
  public ReportRunKey getReportRunKey() {
    return null;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 59 * hash + (this.file != null ? this.file.hashCode() : 0);
    hash = 59 * hash + (this.xVar != null ? this.xVar.hashCode() : 0);
    hash = 59 * hash + (this.yVar != null ? this.yVar.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ExternFileKey other = (ExternFileKey) obj;
    if ((this.file == null) ? (other.file != null) : !this.file.equals(other.file)) {
      return false;
    }
    if ((this.xVar == null) ? (other.xVar != null) : !this.xVar.equals(other.xVar)) {
      return false;
    }
    if ((this.yVar == null) ? (other.yVar != null) : !this.yVar.equals(other.yVar)) {
      return false;
    }
    return true;
  }
}
