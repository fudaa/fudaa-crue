/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.persist;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalConfig;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("LongitudinalConfig")
public class ReportConfigLongitudinalDao extends AbstractReportConfigDao<ReportLongitudinalConfig> {

  @XStreamAlias("Longitudinal")
  private ReportLongitudinalConfig config;

  public ReportConfigLongitudinalDao() {
  }

  public ReportConfigLongitudinalDao(ReportLongitudinalConfig config) {
    this.config = config;
  }

  @Override
  public ReportLongitudinalConfig getConfig() {
    return config;
  }

  @Override
  public void setConfig(ReportLongitudinalConfig config) {
    this.config = config;
  }
}
