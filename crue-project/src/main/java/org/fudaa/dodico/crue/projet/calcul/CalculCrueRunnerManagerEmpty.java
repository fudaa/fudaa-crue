/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.calcul;

import java.util.Collections;
import java.util.Map;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 *
 * @author deniger
 */
public class CalculCrueRunnerManagerEmpty implements CalculCrueRunnerManager {

  private final CalculCrueRunnerEmpty empty = new CalculCrueRunnerEmpty();

  @Override
  public CalculCrueRunner getCrue10Runner(final CoeurConfigContrat coeur) {
    return empty;
  }

  @Override
  public CalculCrueRunner getCrue9Runner() {
    return empty;
  }

  @Override
  public Map<CrueFileType, String> getExecOptions() {
    return Collections.emptyMap();
  }
}
