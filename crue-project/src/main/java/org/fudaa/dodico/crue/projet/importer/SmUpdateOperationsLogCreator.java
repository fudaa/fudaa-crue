package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOperationsData;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateSourceTargetData;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Construit les logs pour la mise à jour d'un sous-modele
 */
public class SmUpdateOperationsLogCreator {
  private final CrueConfigMetier ccm;

  public SmUpdateOperationsLogCreator(CrueConfigMetier ccm) {
    this.ccm = ccm;
  }

  protected CtuluLogGroup extractBrutLog(SmUpdateOperationsData data) {
    CtuluLogGroup logGroup = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    logGroup.setDescription("sm.updater.operationLog");

    //type changed
    SmUpdateChangeTypeIdentifier changeType = new SmUpdateChangeTypeIdentifier();
    final CtuluIOResult<SmUpdateSourceTargetData> changeTypeResult = changeType.extractChange(data.getSourceTargetData());

    //lost data
    final SmUpdateSourceTargetData withSameType = changeTypeResult.getSource();
    CtuluIOResult<List<SmUpdateSourceTargetData.Line<CatEMHSection>>> lostProfilChangeResult = new SmUpdateDataLostInProfilIdentifier(ccm)
      .generateLogsAndExtractNotLastData(withSameType.getSections());
    CtuluIOResult<List<SmUpdateSourceTargetData.Line<CatEMHBranche>>> lostBrancheChangeResult = new SmUpdateDataLostInBrancheIdentifier(ccm)
      .generateLogsAndExtractNotLastData(withSameType.getBranches());

    final SmUpdateSourceTargetData updated = new SmUpdateSourceTargetData(withSameType.getNoeuds(), withSameType.getCasiers(), lostProfilChangeResult.getSource(),
      lostBrancheChangeResult.getSource());

    logGroup.addLog(changeTypeResult.getAnalyze());
    logGroup.addLog(lostProfilChangeResult.getAnalyze());
    logGroup.addLog(lostBrancheChangeResult.getAnalyze());
    logGroup.addLog(new SmUpdateDataDetachedSectiondentifier(ccm).generateLogForDetachedSection(data));
    logGroup.addLog(generateLogUpdated(updated));
    //added
    logGroup.addLog(generateLogAddedEMH(data));
    //in target only
    logGroup.addLog(generateLogInTargetOnly(data));
    return logGroup;
  }

  private CtuluLog generateLogUpdated(SmUpdateSourceTargetData updated) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("sm.updater.emhToUpdateLogGroup");
    final List<EMH> targetEMHs = updated.getTargetEMHs();
    targetEMHs.sort(ObjetNommeByNameComparator.INSTANCE);
    targetEMHs.forEach(emh -> log.addInfo("sm.updater.emhToUpdateLog", emh.getNom()));
    return log;
  }

  public CtuluLogGroup extractLog(SmUpdateOperationsData data) {
    return extractBrutLog(data).createCleanGroup();
  }

  protected CtuluLog generateLogInTargetOnly(SmUpdateOperationsData data) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("sm.updater.emhInTargetOnlyLog");
    getEmhInTargetOnly(data.getSourceSousModele(), data.getTargetSousModele()).forEach(emhName -> log.addInfo("sm.updater.emhInTargetOnlyLogItem", emhName));
    return log;
  }

  protected CtuluLog generateLogAddedEMH(SmUpdateOperationsData data) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("sm.updater.emhToAddLogGroup");
    data.getSourceEmhsToAdd().forEach(emh -> log.addInfo("sm.updater.emhToAddLog", emh.getNom()));
    return log;
  }

  /**
   * Donne une information des EMH qui sont dans le sous-modèle cible uniquement
   *
   * @param sourceSousModele sous-modele source
   * @param targetSousModele sous-modele cible
   */
  private Collection<String> getEmhInTargetOnly(EMHSousModele sourceSousModele, EMHSousModele targetSousModele) {
    final Set<String> onlyInTarget = TransformerHelper.toSetNom(targetSousModele.getAllSimpleEMH());
    final Set<String> sourceNames = TransformerHelper.toSetNom(sourceSousModele.getAllSimpleEMH());
    onlyInTarget.removeAll(sourceNames);
    return onlyInTarget;
  }
}
