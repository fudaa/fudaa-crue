/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.validation.util.DelegateValidatorBranche;

import java.util.Arrays;
import java.util.List;

/**
 * @author deniger
 */
public class EditionBrancheCreator {
  private final EditionSectionCreator sectionCreator;
  private final EditionNoeudCreator noeudCreator;
  private final EditionBrancheDcspCreator dcspCreator;

  public EditionBrancheCreator(final EditionSectionCreator sectionCreator, final EditionNoeudCreator noeudCreator) {
    this.sectionCreator = sectionCreator;
    this.noeudCreator = noeudCreator;
    dcspCreator = new EditionBrancheDcspCreator(sectionCreator.getUniqueNomFinder());
  }

  public static boolean isDistanceValid(final EnumBrancheType brancheType, final double distance, final CrueConfigMetier ccm) {
    final List<EnumBrancheType> brancheTypeWithDistanceNotNull = DelegateValidatorBranche.getBrancheTypeWithDistanceNotNull();
    //il faudrait monter ce probleme dans l'UI:
    final PropertyEpsilon epsilon = ccm.getEpsilon(CrueConfigMetierConstants.PROP_XP);
    if (brancheTypeWithDistanceNotNull.contains(brancheType)) {
      if (epsilon.isZero(distance)) {
        return false;
      }
    } else if (!epsilon.isZero(distance)) {
      return false;
    }
    return true;
  }

  /**
   * @param brancheType type de branche
   * @param brancheDistance la distance de la branche
   * @param ccm le {@link CrueConfigMetier}
   * @return null si valide, le message traduit sinon
   */
  public static Pair<String, Object[]> isDistanceValidMessage(final EnumBrancheType brancheType, final double brancheDistance, final CrueConfigMetier ccm) {
    final List<EnumBrancheType> brancheTypeWithDistanceNotNull = DelegateValidatorBranche.getBrancheTypeWithDistanceNotNull();
    //il faudrait monter ce probleme dans l'UI:
    final PropertyEpsilon epsilon = ccm.getEpsilon(CrueConfigMetierConstants.PROP_XP);
    if (brancheTypeWithDistanceNotNull.contains(brancheType)) {
      if (epsilon.isZero(brancheDistance)) {
        return new Pair<>("validation.branche.distanceMustNotBeNull", new Object[]{brancheType.geti18n()});
      }
    } else if (!epsilon.isZero(brancheDistance)) {
      return new Pair<>("validation.branche.distanceMustBeNull", new Object[]{brancheType.geti18n()});
    }
    return null;
  }

  /**
   * ne crée que l'instance de la branche et ne fait aucun test.
   *
   * @param name le nom de la branche
   * @param brancheType le type voulu
   * @return la branche créée
   */
  public static CatEMHBranche createSimpleBranche(final String name, final EnumBrancheType brancheType) {
    switch (brancheType) {
      case EMHBrancheBarrageFilEau:
        return new EMHBrancheBarrageFilEau(name);
      case EMHBrancheBarrageGenerique:
        return new EMHBrancheBarrageGenerique(name);
      case EMHBrancheEnchainement:
        return new EMHBrancheEnchainement(name);
      case EMHBrancheNiveauxAssocies:
        return new EMHBrancheNiveauxAssocies(name);
      case EMHBrancheOrifice:
        return new EMHBrancheOrifice(name);
      case EMHBranchePdc:
        return new EMHBranchePdc(name);
      case EMHBrancheSaintVenant:
        return new EMHBrancheSaintVenant(name);
      case EMHBrancheSeuilLateral:
        return new EMHBrancheSeuilLateral(name);
      case EMHBrancheSeuilTransversal:
        return new EMHBrancheSeuilTransversal(name);
      case EMHBrancheStrickler:
        return new EMHBrancheStrickler(name);
    }
    throw new IllegalAccessError("brancheType not supported" + brancheType);
  }

  /**
   * @param initBrancheName si non null, sera utilisé. Dans ce cas, on suppose que le nom est unique.
   * @param sousModele le sous-modele
   * @param brancheType le type de branche
   * @param noeudAmont {@link CatEMHNoeud} amont
   * @param noeudAval {@link CatEMHNoeud} aval
   * @param ccm le {@link CrueConfigMetier}
   * @param distance de la branche
   */
  protected CatEMHBranche createBranche(final String initBrancheName, final EMHSousModele sousModele, final EnumBrancheType brancheType, final CatEMHNoeud noeudAmont,
                                        final CatEMHNoeud noeudAval,
                                        final CrueConfigMetier ccm, final double distance) {
    if (!isDistanceValid(brancheType, distance, ccm)) {
      return null;
    }
    final EMHScenario scenario = sousModele.getParent().getParent();
    String brancheName = initBrancheName;
    if (brancheName == null) {
      brancheName = sectionCreator.getUniqueNomFinder().findNewName(EnumCatEMH.BRANCHE, scenario);
    }
    final CatEMHBranche branche = createBrancheInstance(brancheName, brancheType, ccm, sousModele, distance);
    CatEMHNoeud noeudAmontToUse = noeudAmont;
    CatEMHNoeud noeudAvalToUse = noeudAval;
    if (noeudAmontToUse == null) {
      noeudAmontToUse = noeudCreator.createNoeud(sousModele, EnumNoeudType.EMHNoeudNiveauContinu, ccm);
    }
    if (noeudAvalToUse == null) {
      noeudAvalToUse = noeudCreator.createNoeud(sousModele, EnumNoeudType.EMHNoeudNiveauContinu, ccm);
    }
    assert !noeudAmontToUse.getNom().equals(noeudAvalToUse.getNom());
    branche.setNoeudAmont(noeudAmontToUse);
    branche.setNoeudAval(noeudAvalToUse);
    EditionCreateEMH.addInScenario(sousModele, branche, scenario);
    return branche;
  }


  public CatEMHBranche createBrancheInstance(final String name, final EnumBrancheType brancheType, final CrueConfigMetier ccm, final EMHSousModele sousModele,
                                             final double distance) {
    switch (brancheType) {
      case EMHBrancheBarrageFilEau:
        return createEMHBrancheBarrageFilEau(name, ccm, sousModele, 0);
      case EMHBrancheBarrageGenerique:
        return createEMHBrancheBarrageGenerique(name, ccm, sousModele, 0);
      case EMHBrancheEnchainement:
        return createEMHBrancheEnchainement(name, ccm, sousModele, 0);
      case EMHBrancheNiveauxAssocies:
        return createEMHBrancheNiveauxAssocies(name, ccm, sousModele, 0);
      case EMHBrancheOrifice:
        return createEMHBrancheOrifice(name, ccm, sousModele, 0);
      case EMHBranchePdc:
        return createEMHBranchePdc(name, ccm, sousModele, 0);
      case EMHBrancheSaintVenant:
        return createEMHBrancheSaintVenant(name, ccm, sousModele, distance);
      case EMHBrancheSeuilLateral:
        return createEMHBrancheSeuilLateral(name, ccm, sousModele, 0);
      case EMHBrancheSeuilTransversal:
        return createEMHBrancheSeuilTransversal(name, ccm, sousModele, 0);
      case EMHBrancheStrickler:
        return createEMHBrancheStrickler(name, ccm, sousModele, distance);
    }
    throw new IllegalAccessError("brancheType not supported" + brancheType);
  }

  private CatEMHBranche createEMHBrancheBarrageFilEau(final String name, final CrueConfigMetier ccm, final EMHSousModele sousModele,
                                                      final double distance) {
    final EMHBrancheBarrageFilEau branche = new EMHBrancheBarrageFilEau(name);
    addDefaultsSectionProfils(ccm, name, sousModele, branche, distance);
    final DonCalcSansPrtBrancheBarrageFilEau dcsp = dcspCreator.createDcspBrancheBarrageFilEau(ccm, name, sousModele.getParent().getParent());
    branche.addInfosEMH(dcsp);
    return branche;
  }

  private CatEMHBranche createEMHBrancheBarrageGenerique(final String name, final CrueConfigMetier ccm, final EMHSousModele sousModele,
                                                         final double distance) {
    final EMHBrancheBarrageGenerique branche = new EMHBrancheBarrageGenerique(name);
    addDefaultsSectionSansGeometrie(ccm, name, sousModele, branche, distance);
    final DonCalcSansPrtBrancheBarrageGenerique dcsp = dcspCreator.createDcspBrancheBarrageGenerique(ccm, name, sousModele.getParent().getParent());
    branche.addInfosEMH(dcsp);
    return branche;
  }

  private CatEMHBranche createEMHBrancheEnchainement(final String name, final CrueConfigMetier ccm, final EMHSousModele sousModele,
                                                     final double distance) {
    final EMHBrancheEnchainement branche = new EMHBrancheEnchainement(name);
    addDefaultsSectionSansGeometrie(ccm, name, sousModele, branche, 0);
    return branche;
  }

  private CatEMHBranche createEMHBrancheNiveauxAssocies(final String name, final CrueConfigMetier ccm, final EMHSousModele sousModele,
                                                        final double distance) {
    final EMHBrancheNiveauxAssocies branche = new EMHBrancheNiveauxAssocies(name);
    final DonCalcSansPrtBrancheNiveauxAssocies dcsp = dcspCreator.createDcspBrancheNiveauxAssocies(ccm, name, sousModele.getParent().getParent());
    branche.addInfosEMH(dcsp);
    addDefaultsSectionSansGeometrie(ccm, name, sousModele, branche, 0);//distance forcee a 0
    return branche;
  }

  private CatEMHBranche createEMHBrancheOrifice(final String name, final CrueConfigMetier ccm, final EMHSousModele sousModele,
                                                final double distance) {
    final EMHBrancheOrifice branche = new EMHBrancheOrifice(name);
    final DonCalcSansPrtBrancheOrifice donCalcSansPrtBrancheOrifice = dcspCreator.createDcspBrancheOrifice(ccm);
    branche.addInfosEMH(donCalcSansPrtBrancheOrifice);
    addDefaultsSectionSansGeometrie(ccm, name, sousModele, branche, 0);//distance forcee a 0
    return branche;
  }

  private CatEMHBranche createEMHBranchePdc(final String name, final CrueConfigMetier ccm, final EMHSousModele sousModele,
                                            final double distance) {
    final EMHBranchePdc branche = new EMHBranchePdc(name);
    final DonCalcSansPrtBranchePdc dcsp = dcspCreator.createDcspBranchePdc(ccm, name, sousModele.getParent().getParent());
    branche.addInfosEMH(dcsp);
    addDefaultsSectionSansGeometrie(ccm, name, sousModele, branche, 0);//distance forcee a 0
    return branche;
  }

  private CatEMHBranche createEMHBrancheSaintVenant(final String name, final CrueConfigMetier ccm, final EMHSousModele sousModele,
                                                    final double distance) {
    final EMHBrancheSaintVenant branche = new EMHBrancheSaintVenant(name);
    addDefaultsSectionProfils(ccm, name, sousModele, branche, distance);
    final DonPrtGeoBrancheSaintVenant dptg = new DonPrtGeoBrancheSaintVenant(ccm);
    final DonCalcSansPrtBrancheSaintVenant dcsp = dcspCreator.createDcspBrancheSaintVenant(ccm);
    branche.addInfosEMH(dptg);
    branche.addInfosEMH(dcsp);
    return branche;
  }

  private CatEMHBranche createEMHBrancheSeuilLateral(final String name, final CrueConfigMetier ccm, final EMHSousModele sousModele,
                                                     final double distance) {
    final EMHBrancheSeuilLateral branche = new EMHBrancheSeuilLateral(name);
    final DonCalcSansPrtBrancheSeuilLateral dcsp = dcspCreator.createDcspBrancheSeuilLateral(ccm);
    branche.addInfosEMH(dcsp);
    addDefaultsSectionSansGeometrie(ccm, name, sousModele, branche, 0);//distance forcee a 0
    return branche;
  }

  private CatEMHBranche createEMHBrancheSeuilTransversal(final String name, final CrueConfigMetier ccm, final EMHSousModele sousModele,
                                                         final double distance) {
    final EMHBrancheSeuilTransversal branche = new EMHBrancheSeuilTransversal(name);
    final DonCalcSansPrtBrancheSeuilTransversal dcsp = dcspCreator.createDcspBrancheSeuilTransversal(ccm);
    branche.addInfosEMH(dcsp);
    addDefaultsSectionProfils(ccm, name, sousModele, branche, 0);//distance forcee a 0
    return branche;
  }

  private CatEMHBranche createEMHBrancheStrickler(final String name, final CrueConfigMetier ccm, final EMHSousModele sousModele,
                                                  final double distance) {
    final EMHBrancheStrickler branche = new EMHBrancheStrickler(name);
    addDefaultsSectionProfils(ccm, name, sousModele, branche, distance);
    return branche;
  }

  private void addDefaultsSectionProfils(final CrueConfigMetier ccm, final String name, final EMHSousModele sousModele,
                                         final CatEMHBranche branche, final double distance) {
    final List<EMHSectionProfil> createEMHSectionProfil = sectionCreator.createEMHSectionProfils(ccm, name, sousModele);
    final EMHSectionProfil amont = createEMHSectionProfil.get(0);
    final EMHSectionProfil aval = createEMHSectionProfil.get(1);
    final RelationEMHSectionDansBranche relationAmont = EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(branche, amont, 0,
      EnumPosSection.AMONT, ccm);
    final RelationEMHSectionDansBranche relationAval = EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(branche, aval, distance,
      EnumPosSection.AVAL, ccm);
    branche.addListeSections(Arrays.asList(relationAmont, relationAval));
  }

  private void addDefaultsSectionSansGeometrie(final CrueConfigMetier ccm, final String name, final EMHSousModele sousModele,
                                               final CatEMHBranche branche, final double distance) {
    final List<EMHSectionSansGeometrie> newSections = sectionCreator.createEMHSectionSansGeometrie(ccm, name, sousModele);
    final EMHSectionSansGeometrie amont = newSections.get(0);
    final EMHSectionSansGeometrie aval = newSections.get(1);
    final RelationEMHSectionDansBranche relationAmont = EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(branche, amont, 0,
      EnumPosSection.AMONT, ccm);
    final RelationEMHSectionDansBranche relationAval = EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(branche, aval, distance,
      EnumPosSection.AVAL, ccm);
    branche.addListeSections(Arrays.asList(relationAmont, relationAval));
  }
}
