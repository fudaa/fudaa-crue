/*
 GPL 2
 */
package org.fudaa.dodico.crue.edition.bean;

import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EMHSectionIdem;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;

import java.util.List;

/**
 * @author Frederic Deniger
 */
public class SectionPrevalidationContent {
    private List<EMHSectionIdem> sectionReferencing;
    private CatEMHBranche branchePilotedBy;
    private CatEMHBranche branche;
    private boolean isAval;
    private boolean isAmont;
    /**
     * les types de sections pouvant être utilisées.
     */
    private List<EnumSectionType> authorizedSections;

    public List<EMHSectionIdem> getSectionReferencing() {
        return sectionReferencing;
    }

    public void setSectionReferencing(List<EMHSectionIdem> sectionReferencing) {
        this.sectionReferencing = sectionReferencing;
    }

    public CatEMHBranche getBranchePilotedBy() {
        return branchePilotedBy;
    }

    public void setBranchePilotedBy(CatEMHBranche branchePilotedBy) {
        this.branchePilotedBy = branchePilotedBy;
    }

    public CatEMHBranche getBranche() {
        return branche;
    }

    public void setBranche(CatEMHBranche branche) {
        this.branche = branche;
    }

    public boolean isIsAval() {
        return isAval;
    }

    public void setIsAval(boolean isAval) {
        this.isAval = isAval;
    }

    public boolean isIsAmont() {
        return isAmont;
    }

    public void setIsAmont(boolean isAmont) {
        this.isAmont = isAmont;
    }

    public List<EnumSectionType> getAuthorizedSections() {
        return authorizedSections;
    }

    public void setAuthorizedSections(List<EnumSectionType> authorizedSections) {
        this.authorizedSections = authorizedSections;
    }
}
