/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.projet.report.data.ReportKeyContract;
import org.fudaa.dodico.crue.projet.report.data.ReportKeyContratCleaner;
import org.fudaa.dodico.crue.projet.report.data.ReportRPTGCourbeKey;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;
import org.fudaa.dodico.crue.projet.report.loi.ViewCourbeConfig;
import org.fudaa.dodico.crue.projet.report.persist.AbstractReportCourbeConfig;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.dodico.crue.projet.report.persist.VariableUpdateHelper;
import org.fudaa.ebli.courbe.EGCourbePersist;

import java.util.*;

/**
 * @author Frederic Deniger
 */
public class ReportRPTGConfig extends AbstractReportCourbeConfig {

  /**
   * listes des emhs à afficher.
   */
  @XStreamAlias("Emh")
  private String emh;
  //l'emh precedente
  @XStreamAlias("Emh-Prec")
  private String previous;
  @XStreamAlias("Variable-Horizontal")
  private String varHorizontal;
  /**
   * liste des variables à afficher sur les axes verticaux.
   */
  @XStreamImplicit(itemFieldName = "Variable")
  List<String> variables = new ArrayList<>();
  /**
   * la config pour les courbes:
   */
  @XStreamAlias("Courbe-Configs")
  Map<ReportRPTGCourbeKey, EGCourbePersist> courbeconfigs = new HashMap<>();
  @XStreamAlias("Vue-Config")
  ViewCourbeConfig loiLegendConfig = new ViewCourbeConfig();

  public ReportRPTGConfig() {
  }

  public ReportRPTGConfig(ReportRPTGConfig from) {
    super(from);
    if(from==null){
      return;
    }
    this.emh = from.emh;
    this.previous = from.previous;
    this.varHorizontal = from.varHorizontal;
    variables.clear();
    variables.addAll(from.variables);
    if (from.courbeconfigs != null) {
      from.courbeconfigs.forEach((key, value) -> courbeconfigs.put(key, value.duplicate()));
    }
    if (from.loiLegendConfig != null) {
      loiLegendConfig = from.loiLegendConfig.dpulicate();
    }
  }

  @Override
  public ReportRPTGConfig duplicate() {
    return new ReportRPTGConfig(this);
  }

  public Map<ReportRPTGCourbeKey, EGCourbePersist> getCourbeconfigs() {
    return courbeconfigs;
  }


  @Override
  protected void cleanUnusedCourbesConfig(Set<ReportKeyContract> usedKeys) {
    for (Iterator<ReportRPTGCourbeKey> it = courbeconfigs.keySet().iterator(); it.hasNext(); ) {
      if (!usedKeys.contains(it.next())) {
        it.remove();
      }
    }
  }

  @Override
  public boolean variablesUpdated(FormuleDataChanges changes) {
    boolean modified = super.axeVariablesUpdated(changes);
    modified |= VariableUpdateHelper.updateViewCourbeConfig(loiLegendConfig, changes);

    //variable horizontal
    if (changes.isRemoved(varHorizontal)) {
      varHorizontal = null;
      modified = true;
    }
    if (changes.isModified(varHorizontal)) {
      modified = true;
    }
    if (changes.isRenamed(varHorizontal)) {
      varHorizontal = changes.getNewName(varHorizontal);
      modified = true;
    }

    //les variables
    Pair<List<String>, Boolean> variablesUpdated = VariableUpdateHelper.variablesUpdated(variables, changes);
    if (Boolean.TRUE.equals(variablesUpdated.second)) {
      modified = true;
      variables = variablesUpdated.first;
    }
    modified |= VariableUpdateHelper.updateCourbeConfig(courbeconfigs, changes);
    return modified;
  }

  public String getEmh() {
    return emh;
  }

  @Override
  public ReportConfigContrat createTemplateConfig() {
    final ReportRPTGConfig duplicate = (ReportRPTGConfig) duplicate();
    duplicate.clearExternFiles();
    duplicate.clearZooms();
    duplicate.loiLegendConfig.clearLabelTxtTooltip();
    duplicate.emh = null;
    ReportKeyContratCleaner.cleanNotCurrent(duplicate.courbeconfigs);
    return duplicate;
  }

  public List<String> getVariables() {
    return variables;
  }

  public void setEmh(String emh) {
    this.emh = emh;
  }

  public String getPrevious() {
    return previous;
  }

  public void setPrevious(String previous) {
    this.previous = previous;
  }

  public String getVarHorizontal() {
    return varHorizontal;
  }

  public void setVarHorizontal(String varHorizontal) {
    this.varHorizontal = varHorizontal;
  }

  @Override
  public void reinitContent() {
    super.reinitContent();
    if (variables == null) {
      variables = new ArrayList<>();
    }
    if (courbeconfigs == null) {
      courbeconfigs = new HashMap<>();
    }
    if (loiLegendConfig == null) {
      loiLegendConfig = new ViewCourbeConfig();
    }
    loiLegendConfig.reinit();
    variables.remove(StringUtils.EMPTY);
    variables.remove(null);
    courbeconfigs.remove(ReportRPTGCourbeKey.NULL_VALUE);
  }


  public String getVar1() {
    if (!variables.isEmpty()) {
      return variables.get(0);
    }
    return null;
  }

  public String getVar2() {
    if (variables.size() > 1) {
      return variables.get(1);
    }
    return null;
  }

  @Override
  public ViewCourbeConfig getLoiLegendConfig() {
    return loiLegendConfig;
  }

  @Override
  public ReportContentType getType() {
    return ReportContentType.RPTG;
  }

  @Override
  public String getMainEMHName() {
    return emh;
  }
}
