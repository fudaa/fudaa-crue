/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import java.util.List;
import java.util.Set;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.LoiFactory;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;

/**
 * @author deniger
 */
public class EditionLoiCreator {

  private final UniqueNomFinder uniqueNomFinder;

  public EditionLoiCreator(final UniqueNomFinder uniqueNomFinder) {
    this.uniqueNomFinder = uniqueNomFinder;
  }

  /**
   * Rappel: ces lois ne sont pas ajouté dans le dlhy.
   *
   * @param name nom de la loi
   * @param scenario scenario parent
   * @param typeLoi le type de loi
   * @param ccm le {@link CrueConfigMetier}
   * @return la loi créée.
   */
  public LoiFF createLoiFF(final String name, final EMHScenario scenario, final EnumTypeLoi typeLoi, final CrueConfigMetier ccm) {
    final String loiName = findLoiName(scenario, typeLoi, name);
    final LoiFF loi = new LoiFF();
    LoiFactory.alimenteDebutLoiFF(loi, loiName, typeLoi);
    final ConfigLoi configLoi = ccm.getConfLoi().get(typeLoi);
    double x = 0;
    double y = 0;
    try {
      x = configLoi.getVarAbscisse().getValidator().getRangeNormalite().getMin().doubleValue();
      y = configLoi.getVarOrdonnee().getValidator().getRangeNormalite().getMin().doubleValue();
    } catch (final Exception e) {
    }
    LoiFactory.alimenteEvolutionFF(loi, new double[]{x}, new double[]{y});
    return loi;
  }

  public String findLoiName(final EMHScenario scenario, final EnumTypeLoi typeLoi, final String name) {
    final List<Loi> lois = LoiHelper.getAllLois(scenario);
    final Set<String> toSetOfId = TransformerHelper.toSetOfId(lois);
    final String prefix = CruePrefix.getPrefix(typeLoi);
    String loiName = CruePrefix.getNomAvecPrefixe(prefix, name);
    loiName = uniqueNomFinder.findUniqueName(toSetOfId, loiName);
    return loiName;
  }
}
