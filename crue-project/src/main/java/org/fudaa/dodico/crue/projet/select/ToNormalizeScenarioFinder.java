package org.fudaa.dodico.crue.projet.select;

import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.ProjectNormalizableCallable;
import org.openide.util.Exceptions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Permet de trouver les scenarios à normaliser
 *
 * @author deniger
 */
public class ToNormalizeScenarioFinder {
  private final EMHProjet projet;

  public ToNormalizeScenarioFinder(EMHProjet projet) {
    this.projet = projet;
  }

  /**
   * @return nom du scenario -> etat du scenario
   */
  public List<ProjectNormalizableCallable.Result> findToNormalizeOrWithErrors() {

    final List<ManagerEMHScenario> scenariosAll = projet.getListeScenarios();

    //seuls les scenarios crue10 sont proposés pour etre normalisés
    final List<ManagerEMHScenario> scenariosCrue10 = new ArrayList<>();
    for (ManagerEMHScenario scenario : scenariosAll) {
      if (scenario.isCrue10()) {
        scenariosCrue10.add(scenario);
      }
    }

    final List<ProjectNormalizableCallable.Result> toNormalizeOrWithError = new ArrayList<>();
    try {

      //on charge les scenarios dans des threads différents pour gagner en temps
      ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(Math.max(2, Runtime.getRuntime().availableProcessors() / 2));
      CompletionService<ProjectNormalizableCallable.Result> ecs = new ExecutorCompletionService<>(newFixedThreadPool);
      for (ManagerEMHScenario listeScenario : scenariosCrue10) {
        ecs.submit(new ProjectNormalizableCallable(projet, listeScenario));
      }

      int nbScenario = scenariosCrue10.size();
      //on consomme les résultats de chaque Thread:
      for (int i = 0; i < nbScenario; ++i) {
        ProjectNormalizableCallable.Result normalizeResult = ecs.take().get();
        if (normalizeResult.state != ProjectNormalizableCallable.Status.NOTHING_TO_DO) {
          toNormalizeOrWithError.add(normalizeResult);
        }
      }
    } catch (Exception ex) {
      Exceptions.printStackTrace(ex);
    }
    return toNormalizeOrWithError;
  }
}
