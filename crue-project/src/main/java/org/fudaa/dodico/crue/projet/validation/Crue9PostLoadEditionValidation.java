/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.validation;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.edition.EditionRename;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.DonFrtConteneur;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeo;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoBatiCasier;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilCasier;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHCasierProfil;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.LitNumerote;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.emh.OrdPrtCIniModeleBase;
import org.fudaa.dodico.crue.metier.emh.ValParam;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.validation.ValidationHelper;

/**
 *
 * @author Frederic Deniger
 */
public class Crue9PostLoadEditionValidation {

  private static void computeNom(final ObjetNomme obj, final String prefixe, final CtuluLog analyze,
          final String codeErreur, final Map<String, Set<String>> newNameOldNames) {
    final String oldName = obj.getNom();
    final String newNom = CruePrefix.getNomAvecPrefixe(prefixe, oldName);
    boolean canRename = codeErreur == null ? true : addWarnErrorForRename(obj, newNom, oldName, analyze, codeErreur);
    if (canRename) {
      obj.setNom(newNom);
      if (newNameOldNames != null && !newNom.equals(oldName)) {
        Set<String> oldNames = newNameOldNames.get(newNom);
        if (oldNames == null) {
          oldNames = new HashSet<>(2);
          newNameOldNames.put(newNom, oldNames);
        }
        oldNames.add(oldName);
      }
    }
  }

  private static void ensureUniqueName(final List<EMH> listeEMHs, final EMH emh) {
    UniqueNomFinder uniqueNomFinder = new UniqueNomFinder();
    EditionRename rename = new EditionRename();
    List<String> noms = TransformerHelper.toNom(listeEMHs);
    final String oldName = emh.getNom();
    noms.remove(oldName);
    String newName = uniqueNomFinder.findUniqueName(noms, oldName);
    if (!oldName.equals(newName)) {
      emh.setNom(newName);
    }
    //pour toutes les emhs, on met à jours les noms.
    //pour les casiers, ce sont les noeuds qui prendront le dessus:
    if (!emh.getCatType().equals(EnumCatEMH.CASIER)) {
      rename.propagateName(emh);
    }
  }

  private static void ensureUniqueNameDonFrt(final List<DonFrt> listFrt, final DonFrt donFrt) {
    List<String> noms = TransformerHelper.toNom(listFrt);
    final String oldName = donFrt.getNom();
    noms.remove(oldName);
    UniqueNomFinder uniqueNomFinder = new UniqueNomFinder();
    String newName = uniqueNomFinder.findUniqueName(noms, oldName);
    if (!oldName.equals(newName)) {
      donFrt.setNom(newName);
    }
  }

  private static boolean addWarnErrorForRename(final ObjetNomme obj, final String newNom, final String oldName, final CtuluLog analyze, final String codeErreur) {
    if (StringUtils.equals(newNom, oldName)) {
      return true;
    }
    // il faut ajouter un warning uniquement
    final int length = newNom.length();
    if (length > CruePrefix.NB_CAR_MAX) {
      analyze.addSevereError(codeErreur, oldName, newNom);
      return false;
    } else {
      // cas des casier profil a part.
      if ((obj instanceof EMHCasierProfil && length > CruePrefix.NB_CAR_MAX_CRUE9_CASIER_PROFIL) || (length > CruePrefix.NB_CAR_MAX_CRUE9)) {
        analyze.addWarn(codeErreur + ".warn", oldName, newNom);
      }
    }
    return true;
  }

  /**
   * @param crueData les données Crue
   * @return Les messages d'erreur éventuels
   */
  public static CtuluLog verifiePrefixeNomDonneesCrue9(final CrueData crueData, ScenarioAutoModifiedState modifiedState) {
    final List<EMH> listeEMHs = crueData.getAllSimpleEMH();
    List<String> oldNoms = TransformerHelper.toNom(listeEMHs);
    final CtuluLog analyze = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    analyze.setDesc("crue9.normalizeName");
    for (final EMH emh : listeEMHs) {
      String old = emh.getNom();
      computeNom(emh, ValidationHelper.getPrefixFor(emh), analyze, "crue9.cant.rename.emh", null);
      String newName = emh.getNom();
      if (!old.equals(newName)) {
        ensureUniqueName(listeEMHs, emh);
        addWarnErrorForRename(emh, emh.getNom(), old, analyze, "crue9.cant.rename.emh");
      }
    }
    for (final EMH emh : listeEMHs) {
      String old = emh.getNom();
      ensureUniqueName(listeEMHs, emh);
      addWarnErrorForRename(emh, emh.getNom(), old, analyze, "crue9.cant.rename.emh");
    }
    List<String> newNoms = TransformerHelper.toNom(listeEMHs);
    if (!newNoms.equals(oldNoms)) {
      modifiedState.setRenamedDone();
    }
    final DonFrtConteneur frottements = crueData.getFrottements();
    if (frottements != null) {
      final List<DonFrt> listFrt = frottements.getListFrt();
      oldNoms = TransformerHelper.toNom(listFrt);
      for (final DonFrt donFrt : listFrt) {
        final LoiFF loi = donFrt.getLoi();
        final String pref = loi.getType().getNom() + CruePrefix.SEP;
        String oldName = donFrt.getNom();
        computeNom(donFrt, pref, analyze, "crue9.cant.rename.frt", null);
        String newName = donFrt.getNom();
        if (!oldName.equals(newName)) {
          ensureUniqueNameDonFrt(listFrt, donFrt);
          addWarnErrorForRename(donFrt, donFrt.getNom(), oldName, analyze, "crue9.cant.rename.frt");
        }
      }
      for (final DonFrt donFrt : listFrt) {
        String oldName = donFrt.getNom();
        ensureUniqueNameDonFrt(listFrt, donFrt);
        addWarnErrorForRename(donFrt, donFrt.getNom(), oldName, analyze, "crue9.cant.rename.frt");
      }
      newNoms = TransformerHelper.toNom(listFrt);
      if (!newNoms.equals(oldNoms)) {
        modifiedState.setRenamedDone();
      }
    }
    for (final EMH emh : listeEMHs) {
      if (emh.getDPTG() != null) {
        final List<DonPrtGeo> donnees = emh.getDPTG();
        for (final DonPrtGeo donnee : donnees) {
          if (donnee instanceof DonPrtGeoProfilSection) {
            final DonPrtGeoProfilSection donneeProfilSection = (DonPrtGeoProfilSection) donnee;
            String old = donneeProfilSection.getNom();
            computeNom(donneeProfilSection, CruePrefix.P_PROFIL_SECTION, analyze, null, null);
            String newName = donneeProfilSection.getNom();
            if (!old.equals(newName)) {
              modifiedState.setRenamedDone();
            }
            final List<LitNumerote> litsNumerotes = donneeProfilSection.getLitNumerote();
            if (litsNumerotes != null) {
              for (int i = 0, imax = litsNumerotes.size(); i < imax; i++) {
                final LitNomme nomLit = litsNumerotes.get(i).getNomLit();
                if (nomLit != null) {
                  computeNom(nomLit, CruePrefix.P_LIT, analyze, "crue9.cant.rename.lit", null);
                }
              }
            }
          } else if (donnee instanceof DonPrtGeoProfilCasier) {
            String old = ((ObjetNomme) donnee).getNom();
            computeNom((ObjetNomme) donnee, CruePrefix.P_PROFIL_CASIER, analyze, null, null);
            String newName = ((ObjetNomme) donnee).getNom();
            if (!old.equals(newName)) {
              modifiedState.setRenamedDone();
            }
          } else if (donnee instanceof DonPrtGeoBatiCasier) {
            String old = ((ObjetNomme) donnee).getNom();
            computeNom((ObjetNomme) donnee, CruePrefix.P_BATI_CASIER, analyze, null, null);
            String newName = ((ObjetNomme) donnee).getNom();
            if (!old.equals(newName)) {
              modifiedState.setRenamedDone();
            }
          }
        }
      }
    }
    final OrdPrtCIniModeleBase opti = crueData.getOPTI();
    if (opti != null) {
      for (ValParam param : opti.getValParam()) {
        if (ValidationHelper.addPrefixIfNeeded(param)) {
          modifiedState.setRenamedDone();
        }
      }
    }
    return analyze;
  }
}
