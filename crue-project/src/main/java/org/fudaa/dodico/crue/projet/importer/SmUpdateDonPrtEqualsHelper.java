package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.dodico.crue.comparaison.tester.EqualsTester;
import org.fudaa.dodico.crue.comparaison.tester.FactoryEqualsTester;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.LitNumerote;
import org.fudaa.dodico.crue.metier.emh.PtProfil;

import java.util.List;

public class SmUpdateDonPrtEqualsHelper {
  private final EqualsTester ptProfilEqualsTester;

  public SmUpdateDonPrtEqualsHelper(CrueConfigMetier crueConfigMetier) {
    ptProfilEqualsTester = new FactoryEqualsTester(crueConfigMetier, -1, -1).getKnownEqualsTester(PtProfil.class);
  }

  public boolean isSame(DonPrtGeoProfilSection source, DonPrtGeoProfilSection target) {
    return isSamePtProfil(source, target) && isSameLitNumerote(source, target);
  }

  public boolean isSamePtProfil(DonPrtGeoProfilSection source, DonPrtGeoProfilSection target) {
    final List<PtProfil> sourcePtProfils = source.getPtProfil();
    final List<PtProfil> targetPtProfils = target.getPtProfil();
    return isSame(sourcePtProfils, targetPtProfils);
  }

  public boolean isSame(List<PtProfil> sourcePtProfils, List<PtProfil> targetPtProfils) {
    if (sourcePtProfils.size() != targetPtProfils.size()) {
      return false;
    }
    for (int i = 0; i < sourcePtProfils.size(); i++) {
      PtProfil sourcePt = sourcePtProfils.get(i);
      PtProfil targetPt = targetPtProfils.get(i);
      if (!isSamePt(sourcePt, targetPt)) {
        return false;
      }
    }
    return true;
  }

  public boolean isSamePt(PtProfil sourcePt, PtProfil targetPt) {
    try {
      return ptProfilEqualsTester.isSame(sourcePt, targetPt, null, null);
    } catch (Exception e) {
    }
    return false;
  }

  public boolean isSameLitNumerote(DonPrtGeoProfilSection source, DonPrtGeoProfilSection target) {
    final List<LitNumerote> sourceLitNumerotes = source.getLitNumerote();
    final List<LitNumerote> targetLitNumerotes = target.getLitNumerote();
    final int size = sourceLitNumerotes.size();
    if (size != targetLitNumerotes.size()) {
      return false;
    }
    for (int i = 0; i < size; i++) {
      LitNumerote sourceLitNumerote = sourceLitNumerotes.get(i);
      LitNumerote targetLitNumerote = targetLitNumerotes.get(i);
      if (!isSamePt(sourceLitNumerote.getLimDeb(), targetLitNumerote.getLimDeb())) {
        return false;
      }
      if (!isSamePt(sourceLitNumerote.getLimFin(), targetLitNumerote.getLimFin())) {
        return false;
      }
    }
    return true;
  }
}
