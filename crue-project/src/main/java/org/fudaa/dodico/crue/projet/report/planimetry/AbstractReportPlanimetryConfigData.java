/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.planimetry;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.dodico.crue.projet.report.persist.VariableUpdateHelper;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.palette.BPalettePlageProperties;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public abstract class AbstractReportPlanimetryConfigData {

  @XStreamAlias("UseLabels")
  private boolean useLabels;
  @XStreamAlias("UsePalette")
  private boolean usePalette;
  @XStreamAlias("SameLabelsIfSelected")
  private boolean sameIfSelected = true;
  @XStreamImplicit(itemFieldName = "Label")
  private List<ReportLabelContent> labels = new ArrayList<>();
  @XStreamImplicit(itemFieldName = "SelectedLabel")
  private List<ReportLabelContent> labelsIfSelected = new ArrayList<>();
  @XStreamAlias("PaletteColors")
  private BPalettePlageProperties palette;
  @XStreamOmitField
  private BPalettePlage paletteSaved;
  @XStreamAlias("PaletteVariable")
  private ReportVariableKey paletteVariable;

  public AbstractReportPlanimetryConfigData() {

  }

  public AbstractReportPlanimetryConfigData(AbstractReportPlanimetryConfigData from) {
    this.useLabels = from.useLabels;
    this.usePalette = from.usePalette;
    this.sameIfSelected = from.sameIfSelected;
    this.paletteVariable = from.paletteVariable;
    if (from.palette != null) {
      this.palette = from.palette.copy();
    }
    if (from.labels != null) {
      from.labels.forEach(l -> this.labels.add(l.copy()));
    }
    if (from.labelsIfSelected != null) {
      from.labelsIfSelected.forEach(l -> this.labelsIfSelected.add(l.copy()));
    }
    if (from.paletteSaved != null) {
      this.paletteSaved = new BPalettePlage(from.paletteSaved);
    }

  }

  public abstract AbstractReportPlanimetryConfigData copy();


  final void clearLabels() {
    labels = null;
    labelsIfSelected = null;
  }

  public final boolean variablesUpdated(FormuleDataChanges changes) {
    boolean res = false;
    Pair<List<ReportLabelContent>, Boolean> updateReportLabelContent = VariableUpdateHelper.updateReportLabelContent(labels, changes);
    if (Boolean.TRUE.equals(updateReportLabelContent.second)) {
      res = true;
      labels = updateReportLabelContent.first;
    }
    updateReportLabelContent = VariableUpdateHelper.updateReportLabelContent(labelsIfSelected, changes);
    if (Boolean.TRUE.equals(updateReportLabelContent.second)) {
      res = true;
      labelsIfSelected = updateReportLabelContent.first;
    }
    Pair<ReportVariableKey, Boolean> variableUpdated = VariableUpdateHelper.variableUpdated(paletteVariable, changes);
    if (Boolean.TRUE.equals(variableUpdated.second)) {
      res = true;
      paletteVariable = variableUpdated.first;
      String newSousTitre = paletteVariable == null ? null : paletteVariable.getVariableDisplayName();
      palette.setSsTitre(newSousTitre);
      if (paletteSaved != null) {
        paletteSaved.setSousTitre(newSousTitre);
        paletteSaved.setSubTitleLabel(newSousTitre);

      }
    }

    return specificVariableUpdated(changes) | res;
  }

  protected abstract boolean specificVariableUpdated(FormuleDataChanges changes);

  public ReportVariableKey getPaletteVariable() {
    return paletteVariable;
  }

  public void reinitContent() {
    if (labels == null) {
      labels = new ArrayList<>();
    }
    if (labelsIfSelected == null) {
      labelsIfSelected = new ArrayList<>();
    }
  }

  public void setPaletteVariable(ReportVariableKey paletteVariable) {
    this.paletteVariable = paletteVariable;
  }

  public BPalettePlage getPalette() {
    if (paletteSaved == null && palette != null) {
      paletteSaved = new BPalettePlage(palette);
    }
    return paletteSaved;
  }

  public BPalettePlageProperties getPaletteProperties() {
    return palette;
  }

  public void setBPalettePlageProperties(BPalettePlage palette) {
    this.palette = palette == null ? null : palette.save();
    paletteSaved = palette;
  }

  public boolean isUsePalette() {
    return usePalette;
  }

  public void setUsePalette(boolean usePalette) {
    this.usePalette = usePalette;
  }

  public boolean isUseLabels() {
    return useLabels;
  }

  public void setUseLabels(boolean useLabels) {
    this.useLabels = useLabels;
  }

  public boolean isSameIfSelected() {
    return sameIfSelected;
  }

  public List<ReportLabelContent> getLabelsToDisplay(boolean selected) {
    if (sameIfSelected) {
      return labels;
    }
    return selected ? labelsIfSelected : labels;
  }

  public void setSameIfSelected(boolean sameIfSelected) {
    this.sameIfSelected = sameIfSelected;
  }

  public List<ReportLabelContent> getLabels() {
    return labels;
  }

  public void setLabels(List<ReportLabelContent> labels) {
    this.labels = labels == null ? Collections.emptyList() : new ArrayList<>(labels);
  }

  public List<ReportLabelContent> getLabelsIfSelected() {
    return labelsIfSelected;
  }

  public void setLabelsIfSelected(List<ReportLabelContent> labelsIfSelected) {
    this.labelsIfSelected = labelsIfSelected == null ? Collections.emptyList() : new ArrayList<>(labelsIfSelected);
  }
}
