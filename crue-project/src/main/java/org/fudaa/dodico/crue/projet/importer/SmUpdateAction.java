package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.comparaison.CrueComparatorHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.edition.EditionCasierCreator;
import org.fudaa.dodico.crue.edition.EditionChangeSection;
import org.fudaa.dodico.crue.edition.EditionCreateEMH;
import org.fudaa.dodico.crue.edition.EditionNoeudCreator;
import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent;
import org.fudaa.dodico.crue.edition.bean.SectionEditionContent;
import org.fudaa.dodico.crue.metier.comparator.Point2dAbscisseComparator;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.DonFrtHelper;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOperationsData;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateSourceTargetData;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.dodico.crue.validation.ValidateInitialConditions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SmUpdateAction {
  private final CrueConfigMetier ccm;
  private final EMHScenario targetScenario;
  private final EMHSousModele targetSousModele;

  public SmUpdateAction(EMHSousModele targetEmhSousModele, CrueConfigMetier ccm) {
    this.targetSousModele = targetEmhSousModele;
    this.targetScenario = targetEmhSousModele.getParent().getParent();
    this.ccm = ccm;
  }

  public CtuluLogGroup performImport(EMHSousModele sourceSousModele, SmUpdateOperationsData source) {
    IdRegistry registry = new IdRegistry();
    registry.registerAll(sourceSousModele);
    CtuluLogGroup logGroup = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    final List<EMH> sourceEmhsToAdd = source.getSourceEmhsToAdd();
    final List<CatEMHNoeud> catEMHNoeuds = EMHHelper.selectInstanceOf(sourceEmhsToAdd, CatEMHNoeud.class);
    EditionNoeudCreator noeudCreator = new EditionNoeudCreator(new UniqueNomFinder());
    final SmUpdateSourceTargetData build = source.getSourceTargetData();

    //noeud -> on s'assure que le noeud soit ajoute au sous-modele
    catEMHNoeuds.forEach(nd -> noeudCreator.createNoeud(nd.getNom(), nd.getNoeudType(), targetSousModele, ccm));
    build.getNoeuds().stream().map(SmUpdateSourceTargetData.Line::getTargetToModify).forEach(this::addNoeudToTargetToSousModele);

    //casiers
    logGroup.addLog(addCasiers(EMHHelper.selectInstanceOf(sourceEmhsToAdd, CatEMHCasier.class)));
    logGroup.addLog(updateCasiers(build.getCasiers()));

    //sections
    logGroup.addLog(addSections(EMHHelper.selectInstanceOf(sourceEmhsToAdd, CatEMHSection.class)));
    logGroup.addLog(updateSections(build.getSections()));

    //branches
    SmUpdateBrancheAction brancheAction = new SmUpdateBrancheAction(ccm, targetSousModele, targetScenario);
    logGroup.addLog(brancheAction.addBranches(EMHHelper.selectInstanceOf(sourceEmhsToAdd, CatEMHBranche.class)));
    logGroup.addLog(brancheAction.updateBranches(build.getBranches()));

    //mise à jour de conditions initiales
    new ValidateInitialConditions().propagateChange(targetSousModele.getParent(), ccm);
    return logGroup.createCleanGroup();
  }

  private void addNoeudToTargetToSousModele(CatEMHNoeud emhNoeud) {
    final Set<String> parentNames = TransformerHelper.toSetNom(emhNoeud.getParents());
    if (!parentNames.contains(targetSousModele.getNom())) {
      EMHRelationFactory.addRelationContientEMH(targetSousModele, emhNoeud);
    }
  }

  private CtuluLog updateSections(List<SmUpdateSourceTargetData.Line<CatEMHSection>> sections) {
    CtuluLog logUpdateSections = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    logUpdateSections.setDesc("sm.updater.action.updateSectionLog");
    Map<String, EMH> targetEmhsByNom = targetScenario.getIdRegistry().getEmhByNom();
    Point2dAbscisseComparator ptProfilComparator =
      new Point2dAbscisseComparator(ccm.getConfLoi().get(EnumTypeLoi.LoiPtProfil).getVarAbscisse().getEpsilon());
    EditionChangeSection changeSection = new EditionChangeSection(new CreationDefaultValue());
    sections.forEach(sectionLine -> {
      final CatEMHSection targetSection = sectionLine.getTargetToModify();
      final CatEMHSection sourceSection = sectionLine.getSourceToImport();
      final EnumSectionType targetSectionType = targetSection.getSectionType();
      final EnumSectionType sourceSectionType = sectionLine.getSourceToImport().getSectionType();
      if (targetSectionType.equals(sourceSectionType)) {
        if (EnumSectionType.EMHSectionIdem.equals(targetSectionType)) {
          updateSectionIdem(targetEmhsByNom, sectionLine);
        } else if (EnumSectionType.EMHSectionProfil.equals(targetSectionType)) {
          updateSectionProfil(logUpdateSections, ptProfilComparator, sectionLine);
        }
      } else {
        //changement de type
        SectionEditionContent newContent = new SectionEditionContent();
        newContent.setNewType(sourceSectionType);
        newContent.setComment(targetSection.getCommentaire());
        newContent.setNewName(sectionLine.getTargetToModify().getNom());
        if (EnumSectionType.EMHSectionIdem.equals(sourceSection.getSectionType())) {
          //on recupere les donnees sur la section idem
          final DonPrtGeoSectionIdem donPrtSectionIdem = DonPrtHelper.getDonPrtSectionIdem(sourceSection);
          newContent.setDz(donPrtSectionIdem.getDz());
          EMHSectionIdem sourceSectionIdem = (EMHSectionIdem) sourceSection;
          final Long sectionRefUid = targetEmhsByNom.get(sourceSectionIdem.getSectionRefNom()).getUiId();
          newContent.setSectionRefUid(sectionRefUid);
        }
        //changement de type
        changeSection.changeSection(targetSection, newContent, ccm, false);

        CatEMHSection newSection = (CatEMHSection) targetScenario.getIdRegistry().getEmh(targetSection.getUiId());
        DonPrtGeoProfilSection targetProfilSection = DonPrtHelper.getProfilSection(newSection);
        if (targetProfilSection != null) {
          DonPrtGeoProfilSection sourceProfile = DonPrtHelper.getProfilSection(sourceSection);
          targetProfilSection.initDataFrom(sourceProfile);
          updateDonFrt(targetProfilSection);
        }
      }
    });
    return logUpdateSections;
  }

  private void updateSectionProfil(CtuluLog logUpdateSections, Point2dAbscisseComparator ptProfilComparator, SmUpdateSourceTargetData.Line<CatEMHSection> sectionLine) {
    final DonPrtGeoProfilSection sourceProfilSection = DonPrtHelper.getProfilSection((EMHSectionProfil) sectionLine.getSourceToImport());
    final DonPrtGeoProfilSection targetProfilSection = DonPrtHelper.getProfilSection((EMHSectionProfil) sectionLine.getTargetToModify());
    SmUpdateDonPrtEqualsHelper tester = new SmUpdateDonPrtEqualsHelper(ccm);
    if (!tester.isSame(sourceProfilSection, targetProfilSection)) {
      final Map<ItemEnum, DonPrtGeoProfilEtiquette> oldEtiquettes = DonPrtGeoProfilEtiquette.createMap(targetProfilSection.getEtiquettes());
      targetProfilSection.initProfilLitEtiquettes(sourceProfilSection);
      updateDonFrt(targetProfilSection);
      final Map<ItemEnum, DonPrtGeoProfilEtiquette> newEtiquettes = DonPrtGeoProfilEtiquette.createMap(targetProfilSection.getEtiquettes());
      oldEtiquettes.forEach((key, oldEtiquette) -> {
        if (!newEtiquettes.containsKey(key)) {
          final int etiquettePositionInTarget = CrueComparatorHelper.getEqualsPtProfilSorted(oldEtiquette.getPoint(), targetProfilSection.getPtProfil(), ptProfilComparator);
          if (etiquettePositionInTarget >= 0) {
            DonPrtGeoProfilEtiquette newEtiquette = new DonPrtGeoProfilEtiquette();
            newEtiquette.setPoint(targetProfilSection.getPtProfil().get(etiquettePositionInTarget));
            newEtiquette.setTypeEtiquette(key);
            targetProfilSection.getEtiquettes().add(newEtiquette);
          } else {
            logUpdateSections.addError("sm.updater.action.etiquetteNotUpdate", sectionLine.getTargetToModify().getNom(), key.geti18n());
          }
        }
      });
      targetProfilSection.setFente(null);
    }
  }

  /**
   * Mise à jour d'une Section Idem
   *
   * @param targetEmhsByNom
   * @param sectionLine
   */
  private void updateSectionIdem(Map<String, EMH> targetEmhsByNom, SmUpdateSourceTargetData.Line<CatEMHSection> sectionLine) {
    EMHSectionIdem sourceSectionIdem = (EMHSectionIdem) sectionLine.getSourceToImport();
    EMHSectionIdem targetSectionIdem = (EMHSectionIdem) sectionLine.getTargetToModify();
    //mise à jour la reference si modifiée
    if (!sourceSectionIdem.getSectionRefNom().equals(targetSectionIdem.getSectionRefNom())) {
      targetSectionIdem.setSectionRef((CatEMHSection) targetEmhsByNom.get(sourceSectionIdem.getSectionRefNom()));
    }
    //le dz
    final DonPrtGeoSectionIdem targetDonPrtSectionIdem = DonPrtHelper.getDonPrtSectionIdem(targetSectionIdem);
    targetDonPrtSectionIdem.setDz(DonPrtHelper.getDonPrtSectionIdem(sourceSectionIdem).getDz());
  }

  private CtuluLog addSections(List<CatEMHSection> sectionsToAdd) {
    CtuluLog logAddSections = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    logAddSections.setDesc("sm.updater.action.addSectionLog");
    EditionCreateEMH creator = new EditionCreateEMH(new CreationDefaultValue());
    List<EMHSectionIdem> sectionIdems = new ArrayList<>();

    //ajout des sections hors section idem
    sectionsToAdd.forEach(sourceSection -> {
      if (EnumSectionType.EMHSectionIdem.equals(sourceSection.getSectionType())) {
        sectionIdems.add((EMHSectionIdem) sourceSection);
      } else {
        ListRelationSectionContent data = new ListRelationSectionContent();
        data.setNom(sourceSection.getNom());
        data.getSection().setSectionType(sourceSection.getSectionType());
        //create section sans ProfilSection
        final CatEMHSection targetSection = creator.createSectionDetached(data, targetSousModele, ccm, null, false);
        final DonPrtGeoProfilSection sourceProfilSection = DonPrtHelper.getProfilSection(sourceSection);
        if (sourceProfilSection != null) {
          DonPrtGeoProfilSection targetProfil = new DonPrtGeoProfilSection();
          targetProfil.initDataFrom(sourceProfilSection);
          updateDonFrt(targetProfil);
          targetSection.addInfosEMH(targetProfil);
        }
        EditionCreateEMH.addInScenario(targetSousModele, targetSection, targetScenario);
      }
    });
    //les sectionsIdem
    Map<String, CatEMHSection> sectionById = TransformerHelper.toMapOfId(targetSousModele.getSections());
    sectionIdems.forEach(sourceSectionIdem -> {
      final String sectionRefId = sourceSectionIdem.getSectionRefId();
      final CatEMHSection catEMHSection = sectionById.get(sectionRefId);
      if (catEMHSection == null) {
        logAddSections.addInfo("sm.updater.action.sectionReIdemNotFound", sourceSectionIdem.getNom(), sourceSectionIdem.getSectionRef().getNom());
      } else {
        final DonPrtGeoSectionIdem donPrtSectionIdem = DonPrtHelper.getDonPrtSectionIdem(sourceSectionIdem);
        ListRelationSectionContent data = new ListRelationSectionContent();
        data.setNom(sourceSectionIdem.getNom());
        data.getSection().setSectionType(sourceSectionIdem.getSectionType());
        data.getSection().setDz(donPrtSectionIdem.getDz());
        final CatEMHSection targetSection = creator.createSectionDetached(data, targetSousModele, ccm, catEMHSection, false);
        EditionCreateEMH.addInScenario(targetSousModele, targetSection, targetScenario);
      }
    });
    return logAddSections;
  }

  /**
   * @param targetProfilSection le ProfilSection a mettre a jour
   */
  private void updateDonFrt(DonPrtGeoProfilSection targetProfilSection) {
    final Set<DonFrt> usedDonFrtInSource = DonFrtHelper.collectDonFrt(targetProfilSection);
    final Map<String, DonFrt> donFrtByNameInTarget = TransformerHelper.toMapOfNom(DonFrtHelper.getDonFrt(targetSousModele));
    //on cree des DonFrt si non trouve dans le sous-modele cible
    usedDonFrtInSource.stream().filter(donFrt -> !donFrtByNameInTarget.containsKey(donFrt.getNom())).forEach(donFrt -> {
      final DonFrt deepClone = donFrt.deepClone();
      targetSousModele.getFrtConteneur().addFrt(deepClone);
      donFrtByNameInTarget.put(deepClone.getNom(), deepClone);
    });
    //on affecte les frottements dans la cible
    targetProfilSection.getLitNumerote().forEach(litNumerote -> {
      DonFrt frtInTarget = donFrtByNameInTarget.get(litNumerote.getFrot().getNom());
      litNumerote.setFrot(frtInTarget);
    });
  }

  public CtuluLog addCasiers(List<CatEMHCasier> casiersToAdd) {
    CtuluLog logAddCasier = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    logAddCasier.setDesc("sm.updater.action.addCasierLog");
    final Map<String, EMH> targetEmhsByNom = targetScenario.getIdRegistry().getEmhByNom();
    EditionCasierCreator casierCreator = new EditionCasierCreator(null, false);
    casiersToAdd.forEach(sourceCasier -> {
        final CatEMHCasier targetCasier = casierCreator.createCasier(sourceCasier.getNom(), sourceCasier.getCasierType(), ccm, targetSousModele);
        final String ndName = sourceCasier.getNoeud().getNom();
        final CatEMHNoeud targetNoeud = (CatEMHNoeud) targetEmhsByNom.get(ndName);
        if (targetNoeud.getCasier() != null) {
          logAddCasier.addError("sm.updater.action.incoherentCasier", targetNoeud.getNom());
        } else {
          targetCasier.setNoeud(targetNoeud);
          targetCasier.setUserActive(sourceCasier.getUserActive());
          targetCasier.setCommentaire(sourceCasier.getCommentaire());
          sourceCasier.getDCSP().forEach(dcsp -> targetCasier.addInfosEMH(dcsp.cloneDCSP()));
          sourceCasier.getDPTG().forEach(donCalcSansPrt -> targetCasier.addInfosEMH(donCalcSansPrt.cloneDPTG()));
        }
      }
    );
    return logAddCasier;
  }

  public CtuluLog updateCasiers(List<SmUpdateSourceTargetData.Line<CatEMHCasier>> sourceEmhsToAdd) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("sm.updater.action.updateCasierLog");
    sourceEmhsToAdd.forEach(line -> updateCasier(line.getSourceToImport(), line.getTargetToModify(), log));
    return log;
  }

  private void updateCasier(CatEMHCasier sourceCasier, CatEMHCasier targetCasier, CtuluLog log) {
    targetCasier.setUserActive(sourceCasier.getUserActive());
    final Map<String, DonPrtGeoProfilCasier> sourceProfilCasier = TransformerHelper.toMapOfNom(DonPrtHelper.getProfilCasier(sourceCasier));
    final Map<String, DonPrtGeoProfilCasier> targetProfilCasier = TransformerHelper.toMapOfNom(DonPrtHelper.getProfilCasier(targetCasier));
    SmUpdateDonPrtEqualsHelper helper = new SmUpdateDonPrtEqualsHelper(ccm);
    //pour chaque source, on enlève la cible si trouvée pour le log
    sourceProfilCasier.forEach((key, sourceProfile) -> {
      DonPrtGeoProfilCasier targetProfil = targetProfilCasier.remove(key);
      //le profil n'existe pas: on l'ajouter
      if (targetProfil == null) {
        targetCasier.addInfosEMH(sourceProfile);
        log.addInfo("sm.updater.action.casierProfilAdd", sourceCasier.getNom(), key);
      } else {
        //sinon mise à jour
        targetProfil.setDistance(sourceProfile.getDistance());
        if (!helper.isSame(targetProfil.getPtProfil(), sourceProfile.getPtProfil())) {
          targetProfil.setPtProfil(sourceProfile.getPtProfil());
          targetProfil.setLitUtile(sourceProfile.getLitUtile());
        }
      }
    });
    //les profils restants sont présents dans Fudaa-Crue et pas dans l'import
    targetProfilCasier.keySet().forEach(profileName -> log.addInfo("sm.updater.action.casierProfilInTargetAndNotInSource", sourceCasier.getNom(), profileName));
  }
}
