/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import java.io.File;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.rcal.CrueDaoRCAL;
import org.fudaa.dodico.crue.io.rcal.CrueDaoStructureRCAL;
import org.fudaa.dodico.crue.io.rptg.CrueDaoRPTG;
import org.fudaa.dodico.crue.io.rptg.ResPrtGeoDao;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 *
 * @author deniger
 */
public class ScenarioLoaderCrue10FilesFinder {

  public static File retrieveLogName(File resFile, CrueFileType type) {
    File log = null;
    if (resFile != null) {
      String ext = type.getExtension();
      String logName = StringUtils.removeEnd(resFile.getName(), ext);
      String newExt = "c" + StringUtils.substring(StringUtils.substringBefore(ext, "."), 1) + ".csv";
      log = new File(resFile.getParentFile(), logName + newExt);
    }
    return log;
  }

  public static Set<File> retrieveBinName(File rFile, CrueFileType type, CoeurConfigContrat coeur, CtuluLog log) {
    if (CrueFileType.RPTG.equals(type)) {
      Crue10FileFormat<Object> fmt = Crue10FileFormatFactory.getVersion(CrueFileType.RPTG, coeur);
      //le fichier xml n'est pas valide on annule le chargement:
      if (!fmt.isValide(rFile, log)) {
        return Collections.emptySet();
      }
      CrueDaoRPTG res = (CrueDaoRPTG) fmt.read(rFile, log, null).getMetier();
      if (res == null) {
        return Collections.emptySet();
      }
      if (CollectionUtils.isNotEmpty(res.getResPrtGeos())) {
        Set<File> files = new HashSet<>();
        File parent = rFile.getParentFile();
        List<ResPrtGeoDao> resPrtGeos = res.getResPrtGeos();
        for (ResPrtGeoDao resPrtGeoDao : resPrtGeos) {
          files.add(new File(parent, resPrtGeoDao.getHref()));
        }
        return files;
      }
    } else if (CrueFileType.RCAL.equals(type)) {
      Crue10FileFormat<Object> fmt = Crue10FileFormatFactory.getVersion(CrueFileType.RCAL, coeur);
      //le fichier xml n'est pas valide on annule le chargement:
      if (!fmt.isValide(rFile, log)) {
        return Collections.emptySet();
      }
      CrueDaoRCAL res = (CrueDaoRCAL) fmt.read(rFile, log, null).getMetier();
      if (res == null) {
        return Collections.emptySet();
      }
      Set<File> files = new HashSet<>();
      File parent = rFile.getParentFile();
      if (res.getResCalcPerms() != null) {
        for (CrueDaoStructureRCAL.ResultatsCalculPermanentDao elem : res.getResCalcPerms()) {
          files.add(new File(parent, elem.getHref()));
        }
      }
      if (res.getResCalcTranss() != null) {
        for (CrueDaoStructureRCAL.ResCalcTransDao resCalTransDao : res.getResCalcTranss()) {
          for (CrueDaoStructureRCAL.ResPdtDao resPdtDao : resCalTransDao.getResPdts()) {
            files.add(new File(parent, resPdtDao.getHref()));

          }
        }
      }
      return files;
    }
    return Collections.emptySet();

  }
}
