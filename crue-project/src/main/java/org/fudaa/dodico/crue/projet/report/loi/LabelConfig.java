/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.loi;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;

/**
 * @author Frederic Deniger
 */
public class LabelConfig {

  /**
   * Attention, si vous modifier le nom de la variable font: il faut modifier cette constante Idem pour les autres.
   */
  public static final String PROP_FONT = "font";
  public static final String PROP_FGCOLOR = "fgColor";
  public static final String PROP_HALIGNMENT = "horizontalAlignment";

  public static String geti18nFont() {
    return BusinessMessages.getString("font.property");
  }

  public static String geti18nFgColor() {
    return BusinessMessages.getString("foregroundColor.property");
  }

  public static String geti18nAlignment() {
    return BusinessMessages.getString("alignment.property");
  }

  @XStreamAlias("Police")
  Font font = CtuluLibSwing.getDefaultLabelFont();
  @XStreamAlias("Horizontal-Position")
  int horizontalAlignment = SwingConstants.LEFT;
  @XStreamAlias("Couleur")
  Color fgColor = Color.BLACK;//couleur avant-plan
  @XStreamAlias("Texte")
  String text;
  @XStreamAlias("InfoBulle")
  String tooltipText;
  @XStreamAlias("Cle")
  Object key;//pour retrouver un label

  public Object getKey() {
    return key;
  }


  public void setKey(Object key) {
    this.key = key;
  }

  public static LabelConfig createCenteredLabel() {
    LabelConfig res = new LabelConfig();
    res.horizontalAlignment = SwingConstants.CENTER;

    return res;
  }

  public LabelConfig() {
  }

  public LabelConfig(LabelConfig from) {
    if (from != null) {
      this.font = from.font;
      this.horizontalAlignment = from.horizontalAlignment;
      this.fgColor = from.fgColor;
      this.text = from.text;
      this.tooltipText = from.tooltipText;
      this.key = from.key;
    }
  }

  @PropertyDesc(i18n = "font.property", i18nBundleBaseName = "org/fudaa/fudaa/crue/loi/Bundle.properties")
  public Font getFont() {
    return font;
  }

  public void setFont(Font font) {
    this.font = font;
  }

  @PropertyDesc(i18n = "alignment.property", i18nBundleBaseName = "org/fudaa/fudaa/crue/loi/Bundle.properties")
  public int getHorizontalAlignment() {
    return horizontalAlignment;
  }

  public void setHorizontalAlignment(int horizontalAlignment) {
    this.horizontalAlignment = horizontalAlignment;
  }

  @PropertyDesc(i18n = "foregroundColor.property", i18nBundleBaseName = "org/fudaa/fudaa/crue/loi/Bundle.properties")
  public Color getFgColor() {
    return fgColor;
  }

  public void setFgColor(Color fgColor) {
    this.fgColor = fgColor;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getTooltipText() {
    return tooltipText;
  }

  public void setTooltipText(String tooltipText) {
    this.tooltipText = tooltipText;
  }

  public void apply(JLabel label) {
    if (font != null) {
      label.setFont(font);
    }
    if (fgColor != null) {
      label.setForeground(fgColor);
    }
    label.setHorizontalAlignment(horizontalAlignment);
    label.setText(StringUtils.defaultString(text));
    label.setToolTipText(tooltipText);
  }

  public LabelConfig duplicate() {
    return new LabelConfig(this);
  }
}
