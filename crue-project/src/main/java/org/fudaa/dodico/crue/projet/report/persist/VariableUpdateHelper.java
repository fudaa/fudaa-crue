/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.persist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.projet.report.data.ReportKeyContract;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.dodico.crue.projet.report.loi.ViewCourbeConfig;
import org.fudaa.ebli.courbe.EGCourbePersist;

/**
 *
 * @author Frederic Deniger
 */
public class VariableUpdateHelper {

  public static <T extends ReportKeyContract> boolean updateCourbeConfig(Map<T, EGCourbePersist> courbeconfigs, FormuleDataChanges changes) {
    boolean modified = false;

    Map<T, EGCourbePersist> toAdd = new HashMap<>();
    final Set<Entry<  T, EGCourbePersist>> entrySet = courbeconfigs.entrySet();
    for (Iterator<Entry<T, EGCourbePersist>> it = entrySet.iterator(); it.hasNext();) {
      Map.Entry<T, EGCourbePersist> entry = it.next();
      ReportKeyContract varKey = entry.getKey();
      EGCourbePersist axePersist = entry.getValue();
      final String varName = varKey.getVariableName();
      if (varName != null) {
        if (changes.isRemoved(varName)) {
          it.remove();
          modified = true;
        }

        if (changes.isRenamed(varName)) {
          it.remove();
          final String newName = changes.getNewName(varName);
          T key = (T) varKey.duplicateWithNewVar(newName);
          toAdd.put(key, axePersist);
          modified = true;
        }
      }
    }
    courbeconfigs.putAll(toAdd);
    return modified;
  }

  public static Pair<List<String>, Boolean> variablesUpdated(List<String> initVariables, FormuleDataChanges changes) {
    boolean modified = false;
    List<String> newVar = new ArrayList<>();
    for (String old : initVariables) {
      if (changes.isRemoved(old)) {
        modified = true;
      } else if (changes.isRenamed(old)) {
        newVar.add(changes.getNewName(old));
        modified = true;
      } else {
        if (changes.isModified(old)) {
          modified = true;
        }
        newVar.add(old);
      }
    }
    return new Pair<>(newVar, modified);
  }

  public static <T extends ReportKeyContract> Pair<List<T>, Boolean> variablesUpdatedVariable(List<T> initVariables, FormuleDataChanges changes) {
    boolean modified = false;
    List<T> newVar = new ArrayList<>();
    for (T old : initVariables) {
      final String oldVariableName = old.getVariableName();
      if (oldVariableName == null) {
        newVar.add(old);
      } else if (changes.isRemoved(oldVariableName)) {
        modified = true;
      } else if (changes.isRenamed(oldVariableName)) {
        final String newName = changes.getNewName(oldVariableName);
        T newKey = (T) old.duplicateWithNewVar(newName);
        newVar.add(newKey);
        modified = true;
      } else {
        if (changes.isModified(oldVariableName)) {
          modified = true;
        }
        newVar.add(old);
      }
    }
    return new Pair<>(newVar, modified);
  }

  public static <T extends ReportKeyContract> Pair<T, Boolean> variableUpdated(T initVariable, FormuleDataChanges changes) {
    if (initVariable == null) {
      return new Pair(initVariable, Boolean.FALSE);
    }
    final String oldVariableName = initVariable.getVariableName();
    if (oldVariableName == null) {
      return new Pair(initVariable, Boolean.FALSE);
    } else if (changes.isRemoved(oldVariableName)) {
      return new Pair(true, Boolean.TRUE);
    } else if (changes.isRenamed(oldVariableName)) {
      final String newName = changes.getNewName(oldVariableName);
      T newKey = (T) initVariable.duplicateWithNewVar(newName);
      return new Pair(newKey, Boolean.TRUE);
    } else if (changes.isModified(oldVariableName)) {
      return new Pair(initVariable, Boolean.TRUE);
    }
    return new Pair(initVariable, Boolean.FALSE);
  }

  public static boolean updateViewCourbeConfig(ViewCourbeConfig config, FormuleDataChanges changes) {
    boolean modified = false;
    List<LabelConfig> labels = config.getLabels();
    List<LabelConfig> newLabels = new ArrayList<>();
    for (LabelConfig labelConfig : labels) {
      if (labelConfig.getKey() instanceof ReportLabelContent) {
        ReportLabelContent reportLabelContent = (ReportLabelContent) labelConfig.getKey();
        String variableName = reportLabelContent.getContent().getVariableName();
        if (changes.isRemoved(variableName)) {
          modified = true;
        } else {
          //on change le nom si nécessaire:
          if (changes.isRenamed(variableName)) {
            reportLabelContent.setContent(reportLabelContent.getContent().duplicateWith(changes.getNewName(variableName)));
            modified = true;
          } else if (changes.isModified(variableName)) {
            modified = true;
          }
          newLabels.add(labelConfig);
        }
      }
    }
    if (modified) {
      config.setLabels(newLabels);
    }
    return modified;
  }

  public static Pair<List<ReportLabelContent>, Boolean> updateReportLabelContent(List<ReportLabelContent> init, FormuleDataChanges changes) {
    boolean modified = false;
    if (init == null) {
      return new Pair<>(init, Boolean.FALSE);
    }
    List<ReportLabelContent> newLabels = new ArrayList<>();
    for (ReportLabelContent reportLabelContent : init) {
      String variableName = reportLabelContent.getContent().getVariableName();
      if (changes.isRemoved(variableName)) {
        modified = true;
      } else {
        //on change le nom si nécessaire:
        if (changes.isRenamed(variableName)) {
          reportLabelContent.setContent(reportLabelContent.getContent().duplicateWith(changes.getNewName(variableName)));
          modified = true;
        } else if (changes.isModified(variableName)) {
          modified = true;
        }
        newLabels.add(reportLabelContent);
      }
    }
    if (modified) {
      return new Pair<>(newLabels, Boolean.TRUE);
    }
    return new Pair<>(init, Boolean.FALSE);
  }
}
