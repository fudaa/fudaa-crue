/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;

/**
 * Le contenu des variables gérées par le service.
 *
 * @author Frederic Deniger
 */
public class FormuleServiceContent {

  private final List<FormuleContent> variables = new ArrayList<>();
  private final Map<String, FormuleContent> variablesByName = new HashMap<>();

  public FormuleServiceContent() {
  }

  public FormuleServiceContent(List<FormuleContent> variables) {
    if (variables != null) {
      this.variables.addAll(variables);
    }
    init();
  }

  public FormuleContent getContent(ReportRunVariableKey key) {
    return variablesByName.get(key.getVariable().getVariableName());
  }

  public FormuleContent getContent(ReportVariableKey key) {
    return variablesByName.get(key.getVariableName());
  }

  public FormuleContent getContent(String varName) {
    return variablesByName.get(varName);
  }

  private void init() {
    variablesByName.clear();
    for (FormuleContent variableContent : variables) {
      variablesByName.put(variableContent.getParameters().getId(), variableContent);

    }
  }

  public List<FormuleContent> getVariables() {
    return variables;
  }

  public List<String> getVariablesKeys() {
    List<String> res = new ArrayList<>();
    res.addAll(variablesByName.keySet());
    return res;
  }

  public List<FormuleParametersExpr> getParameters() {
    List<FormuleParametersExpr> res = new ArrayList<>();
    if (variables != null) {
      for (FormuleContent formuleContent : variables) {
        res.add((FormuleParametersExpr) formuleContent.getParameters());
      }
    }
    return res;
  }
}
