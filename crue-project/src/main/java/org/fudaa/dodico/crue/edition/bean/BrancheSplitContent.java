package org.fudaa.dodico.crue.edition.bean;

import org.fudaa.dodico.crue.metier.emh.EnumSectionType;

import java.util.ArrayList;
import java.util.List;

/**
 * Le contenu des parametres pour découper une branche en 2: a partir d'une branche sélectionée et d'une section, l'objectif est de créer une nouvelle branche
 * aval qui contiendra toutes les sections
 *
 * @author deniger
 */
public class BrancheSplitContent {
  /**
   * Le nom de la branche qui sera découpée. Deviendra la branche amont
   */
  public String brancheNameAmontToSplit;
  /**
   * Le nom de la section qui sera utilisée pour faire la séparation
   */
  public String sectionNameToSplitOn;
  /**
   * le nom de la branche a créer.
   */
  public String newBrancheNameAval;
  /**
   * le nom du noeud qui sera le noeud aval de la branche amont et le noeud amont de la nouvelle branche (la branche aval)
   */
  public String newNoeudName;
  /**
   * Les sections à déplacer de la branche amont vers la nouvelle branche
   */
  public final List<SectionContent> sections = new ArrayList<>();
  /**
   * l'Uid de la branche à splitter
   */
  public Long brancheToSplitUid;
  /**
   * L'uid de la section support du split
   */
  public Long sectionToSplitOnUid;

  public void addSectionContent(final String emhNom, final EnumSectionType type, final double abscisseHydraulique) {
    sections.add(new SectionContent(emhNom, type, abscisseHydraulique));
  }

  public class SectionContent {

    public SectionContent(final String nom, final EnumSectionType sectionType, final double abscisseHydraulique) {
      this.nom = nom;
      this.sectionType = sectionType;
      this.abscisseHydraulique = abscisseHydraulique;
    }

    /**
     * le nom de section. Ne doit pas être modifiée par l'UI et correspondre a une section de la branche amont.
     */
    public String nom;
    public EnumSectionType sectionType;
    public double abscisseHydraulique;
  }
}
