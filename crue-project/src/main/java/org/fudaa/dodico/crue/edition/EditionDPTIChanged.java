/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.DonPrtCIni;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.validation.ValidationHelper;
import org.fudaa.dodico.crue.validation.ValidateInitialConditions;

/**
 * @author Frédéric Deniger
 */
public class EditionDPTIChanged {

  public CtuluLog apply(final List<Pair<EMH, DonPrtCIni>> toModify, final CrueConfigMetier ccm) {
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    for (final Pair<EMH, DonPrtCIni> pair : toModify) {
      final EMH emh = pair.first;
      final Class dptiClass = ValidateInitialConditions.getDptiClass(emh);
      if (!pair.second.getClass().equals(dptiClass)) {
        log.addError("dptiChanged.typeNotSupport", emh.getNom(), dptiClass.getSimpleName());
      } else {
        ValidationHelper.validateObject(emh.getNom(), log, pair.second, ccm);
      }
    }
    if (log.containsErrorOrSevereError()) {
      return log;
    }
    for (final Pair<EMH, DonPrtCIni> pair : toModify) {
      final EMH emh = pair.first;
      //normalement, il n'y a qu'un dpti
      final List<DonPrtCIni> dpti = emh.getDPTI();
      if (dpti.isEmpty()) {
        emh.addInfosEMH(pair.second.deepClone());
      } else {
        dpti.get(0).initFrom(pair.second);
      }
    }
    return log;


  }
}
