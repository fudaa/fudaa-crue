/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition.bean;

import java.util.List;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrt;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBrancheSaintVenant;

/**
 *
 * @author Frédéric Deniger
 */
public class BrancheEditionContent extends ListBrancheContent {

  /**
   * les nouvelles dcsp à appliquer. Ces dcsp ne contiennent pas les lois qui doivent être reprise de l'existant ou
   * créées
   */
  private DonCalcSansPrt dcsp;
  /**
   * pour les branches saint-venant uniquement
   */
  private DonPrtGeoBrancheSaintVenant dptgSaintVenant;
  /**
   * contient n-1 relation (la dernière n'est pas utile et pas editée).
   */
  private List<RelationEMHSectionDansBrancheSaintVenant> saintVenantCoeff;
  /**
   * Pour les branches barrages, permet de définir la section pilote.
   */
  CatEMHSection sectionPilote;
  /**
   * pour les branches strickler et saint-venant
   */
  double distance;

  public DonCalcSansPrt getDcsp() {
    return dcsp;
  }

  public void setDcsp(final DonCalcSansPrt dcsp) {
    this.dcsp = dcsp;
  }

  public DonPrtGeoBrancheSaintVenant getDptgSaintVenant() {
    return dptgSaintVenant;
  }

  public void setDptgSaintVenant(final DonPrtGeoBrancheSaintVenant dptgSaintVenant) {
    this.dptgSaintVenant = dptgSaintVenant;
  }

  public List<RelationEMHSectionDansBrancheSaintVenant> getSaintVenantCoeff() {
    return saintVenantCoeff;
  }

  public void setSaintVenantCoeff(final List<RelationEMHSectionDansBrancheSaintVenant> saintVenantCoeff) {
    this.saintVenantCoeff = saintVenantCoeff;
  }

  public CatEMHSection getSectionPilote() {
    return sectionPilote;
  }

  public void setSectionPilote(final CatEMHSection sectionPilote) {
    this.sectionPilote = sectionPilote;
  }

  public double getDistance() {
    return distance;
  }

  public void setDistance(final double distance) {
    this.distance = distance;
  }

}
