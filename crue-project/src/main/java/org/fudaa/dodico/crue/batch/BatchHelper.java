package org.fudaa.dodico.crue.batch;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierLoader;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;
import org.fudaa.dodico.crue.config.coeur.CoeurManager;
import org.fudaa.dodico.crue.config.coeur.CoeurManagerValidator;
import org.fudaa.dodico.crue.io.conf.CrueCONFReaderWriter;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.projet.conf.Configuration;
import org.fudaa.dodico.crue.projet.conf.Crue9DecimalFormatBuilder;
import org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager;
import org.fudaa.dodico.crue.projet.conf.OptionsEnum;

import java.io.File;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * Classe helper pour les lancements batchs.
 * @author deniger
 */
public class BatchHelper {
  /**
   * la propriété a utiliser au lancement pour le rep de configuration du site.
   */
  public static final String SITE_CONFIG_PROP = "SiteConfigDir";

  public static Configuration readConfigSite(File confFile, CtuluLog log) {
    final CrueCONFReaderWriter reader = new CrueCONFReaderWriter("1.2");
    try {
      return reader.readXML(confFile, log).getMetier();
    } catch (Throwable exception) {
    }
    return null;
  }

  public static CoeurManager loadCoeur(File siteDir, Configuration configuration, CtuluLog log) {
    CoeurManager coeurManager = new CoeurManager(siteDir, configuration.getSite().getCoeurs());
    CoeurManagerValidator validator = new CoeurManagerValidator();

    CtuluLog validate = validator.validate(coeurManager);
    log.addAllLogRecord(validate);
    boolean isCoeurValid = !validate.containsErrorOrSevereError();
    if (isCoeurValid) {
      List<CoeurConfig> allCoeurConfigs = coeurManager.getAllCoeurConfigs();
      for (CoeurConfig coeurConfig : allCoeurConfigs) {
        File crueConfigMetierURL = new File(coeurConfig.getCrueConfigMetierURL());
        CrueConfigMetierLoader loader = new CrueConfigMetierLoader();
        CrueIOResu<CrueConfigMetier> load = loader.load(crueConfigMetierURL, coeurConfig);
        log.addAllLogRecord(load.getAnalyse());
        if (load.getAnalyse().containsErrorOrSevereError() || load.getMetier() == null) {
          return null;
        }
        coeurConfig.loadCrueConfigMetier(load.getMetier());
        Crue9DecimalFormatBuilder crue9DecimalFormatBuilder = new Crue9DecimalFormatBuilder();
        crue9DecimalFormatBuilder.intialize(configuration, load.getMetier());
      }
      return coeurManager;
    }
    return null;
  }

  public static Map<CrueFileType, String> getExecOptions(GlobalOptionsManager optionsManager) {
    Map<CrueFileType, String> res = new EnumMap<>(CrueFileType.class);
    res.put(CrueFileType.RPTR, optionsManager.getOption(OptionsEnum.RPTR_EXE_OPTION).getValeur());
    res.put(CrueFileType.RPTG, optionsManager.getOption(OptionsEnum.RPTG_EXE_OPTION).getValeur());
    res.put(CrueFileType.RPTI, optionsManager.getOption(OptionsEnum.RPTI_EXE_OPTION).getValeur());
    res.put(CrueFileType.RCAL, optionsManager.getOption(OptionsEnum.RCAL_EXE_OPTION).getValeur());
    return res;
  }
}
