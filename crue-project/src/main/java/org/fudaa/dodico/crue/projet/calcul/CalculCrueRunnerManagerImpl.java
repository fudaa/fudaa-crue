/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.calcul;

import java.util.Map;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 *
 * @author deniger
 */
public class CalculCrueRunnerManagerImpl implements CalculCrueRunnerManager {

  private String crue9Exec;
  private Map<CrueFileType, String> execOptions;

  public String getCrue9Exec() {
    return crue9Exec;
  }



  public final void setExecOptions(final Map<CrueFileType, String> execOptions) {
    this.execOptions = execOptions;
  }

  @Override
  public final Map<CrueFileType, String> getExecOptions() {
    return execOptions;
  }

  public void setCrue9Exec(final String crue9Exec) {
    this.crue9Exec = crue9Exec;
  }

  @Override
  public CalculCrueRunner getCrue10Runner(final CoeurConfigContrat coeur) {
    final CalculCrue10Runner calculCrue10Runner = new CalculCrue10Runner(coeur, execOptions);
    return calculCrue10Runner;
  }

  @Override
  public CalculCrueRunner getCrue9Runner() {
    final CalculCrue9Runner calculCrue9Runner = new CalculCrue9Runner(crue9Exec);
    return calculCrue9Runner;
  }
}
