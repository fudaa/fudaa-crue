/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.transformer;

import com.thoughtworks.xstream.XStream;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.ctulu.converter.PropertyToStringCacheDecorator;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ExternFileKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRPTGCourbeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunEmhKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableEmhKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVerticalTimeKey;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBrancheConfig;
import org.fudaa.ebli.converter.ToStringTransfomerXstreamConverter;

/**
 *
 * @author Frederic Deniger
 */
public class KeysToStringConverter {

  public static final String MAIN_SEPARATOR = "|";
  public static final String MINOR_SEPARATOR = ";";
  private final AbstractPropertyToStringTransformer<ExternFileKey> externFileKeyConverter;
  private final AbstractPropertyToStringTransformer<ReportRunEmhKey> reportRunEmhKeyConverter;
  private final AbstractPropertyToStringTransformer<ReportRunKey> runKeyConverter;
  private final AbstractPropertyToStringTransformer<ReportRunVariableEmhKey> reportRunVariableEmhKeyConverter;
  private final AbstractPropertyToStringTransformer<ReportRunVariableKey> reportRunVariableKeyConverter;
  private final AbstractPropertyToStringTransformer<ReportRunVariableTimeKey> reportRunVariableTimeKeyConverter;
  private final AbstractPropertyToStringTransformer<ReportVariableKey> reportVariableKeyConverter;
  private final AbstractPropertyToStringTransformer<ReportVerticalTimeKey> reportVerticalTimeKeyConverter;
  private final AbstractPropertyToStringTransformer<ResultatTimeKey> resultatKeyConverter;
  private final AbstractPropertyToStringTransformer<ReportLabelContent> reportLabelKeyConverter;
  private final AbstractPropertyToStringTransformer<ReportLongitudinalBrancheConfig.SensParcours> sensParcoursConverter;
  private final AbstractPropertyToStringTransformer<ReportRPTGCourbeKey> reportRPTGCourbeKeyConverter;
  private final List<AbstractPropertyToStringTransformer> converters = new ArrayList<>();

  public KeysToStringConverter(ReportRunKey currentKey) {
    resultatKeyConverter = new ResultatTimeKeySimpleToStringTransformer();
    converters.add(resultatKeyConverter);

    runKeyConverter = new PropertyToStringCacheDecorator<>(new ReportRunKeySimpleToStringTransformer(currentKey));
    converters.add(runKeyConverter);

    reportVariableKeyConverter = new PropertyToStringCacheDecorator<>(new ReportVariableKeySimpleToStringTransformer());
    converters.add(reportVariableKeyConverter);

    externFileKeyConverter = new ExternFileKeyToStringTransformer();
    converters.add(externFileKeyConverter);

    reportRunEmhKeyConverter = new ReportRunEmhKeyMultiToStringTransformer(runKeyConverter);
    converters.add(reportRunEmhKeyConverter);

    reportRunVariableKeyConverter = new ReportRunVariableKeyMultiToStringTransformer(runKeyConverter, reportVariableKeyConverter);
    converters.add(reportRunVariableKeyConverter);

    reportVerticalTimeKeyConverter = new ReportVerticalTimeKeyToStringTransformer();
    converters.add(reportVerticalTimeKeyConverter);

    reportRunVariableEmhKeyConverter = new ReportRunVariableEmhKeyMultiToStringTransformer(reportRunVariableKeyConverter);
    converters.add(reportRunVariableEmhKeyConverter);

    reportRunVariableTimeKeyConverter = new ReportRunVariableTimeKeyMultiToStringTransformer(reportRunVariableKeyConverter, resultatKeyConverter);
    converters.add(reportRunVariableTimeKeyConverter);

    reportLabelKeyConverter = new ReportLabelContentToStringTransformer(reportVariableKeyConverter);
    converters.add(reportLabelKeyConverter);

    sensParcoursConverter = new SensParcoursToStringTransformer();
    converters.add(sensParcoursConverter);

    reportRPTGCourbeKeyConverter = new ReportRPTGCourbeKeySimpleToStringTransformer();
    converters.add(reportRPTGCourbeKeyConverter);
  }

  public static String getMAIN_SEPARATOR() {
    return MAIN_SEPARATOR;
  }

  public static String getMINOR_SEPARATOR() {
    return MINOR_SEPARATOR;
  }

  public AbstractPropertyToStringTransformer<ExternFileKey> getExternFileKey() {
    return externFileKeyConverter;
  }

  public AbstractPropertyToStringTransformer<ReportRunEmhKey> getReportRunEmhKey() {
    return reportRunEmhKeyConverter;
  }

  public AbstractPropertyToStringTransformer<ReportRunKey> getRunKeyConverter() {
    return runKeyConverter;
  }

  public AbstractPropertyToStringTransformer<ReportRunVariableEmhKey> getReportRunVariableEmhKeyConverter() {
    return reportRunVariableEmhKeyConverter;
  }

  public AbstractPropertyToStringTransformer<ReportRunVariableKey> getReportRunVariableKeyConverter() {
    return reportRunVariableKeyConverter;
  }

  public AbstractPropertyToStringTransformer<ReportRunVariableTimeKey> getReportRunVariableTimeKeyConverter() {
    return reportRunVariableTimeKeyConverter;
  }

  public AbstractPropertyToStringTransformer<ReportVariableKey> getReportVariableKeyConverter() {
    return reportVariableKeyConverter;
  }

  public AbstractPropertyToStringTransformer<ReportVerticalTimeKey> getReportVerticalTimeKeyConverter() {
    return reportVerticalTimeKeyConverter;
  }

  public AbstractPropertyToStringTransformer<ResultatTimeKey> getResultatKeyConverter() {
    return resultatKeyConverter;
  }

  public void registerDefaultConverters(XStream xstream) {
    for (AbstractPropertyToStringTransformer converter : converters) {
      xstream.registerConverter(new ToStringTransfomerXstreamConverter(converter, converter.getSupportedClass()));
    }
  }
}
