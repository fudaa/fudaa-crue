/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

import java.io.File;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Charge un scénario et le sauve ensuite pour le normaliser. Attention: ne modifie pas le fichier etu.
 *
 * @author Frederic Deniger
 */
public class ProjectLoadAndSaveCallable implements Callable<CtuluLogGroup> {
  private final EMHProjet projet;
  private final ManagerEMHScenario managerEMHScenario;
  private final EMHRun run;
  private final ConnexionInformation information;

  public ProjectLoadAndSaveCallable(EMHProjet projet, ManagerEMHScenario scenario, EMHRun run, ConnexionInformation information) {
    this.projet = projet;
    this.managerEMHScenario = scenario;
    this.run = run;
    this.information = information;
  }

  @Override
  public CtuluLogGroup call() throws Exception {
    //chargement du scenario
    ScenarioLoader loader = new ScenarioLoader(managerEMHScenario, projet, projet.getCoeurConfig());
    final ScenarioLoaderOperation load = loader.load(run);
    CtuluLogGroup logGroup = load.getLogs();

    //si pas d'erreur, sauvegarde
    if (!logGroup.containsFatalError() && load.getResult() != null) {
      managerEMHScenario.getInfosVersions().updateForm(information);
      save(load.getResult(), logGroup);
    }
    logGroup.setDescription(managerEMHScenario.getNom());
    return logGroup;
  }

  /**
   * Permet de conserver les dates de dernières modifications des fichiers: seuls les fichiers réellement modifiés sont remplacés
   *
   * @param scenario le scenario a persister
   * @param logGroup les logs
   */
  private void save(EMHScenario scenario, CtuluLogGroup logGroup) throws Exception {
    //on va vérifier si les fichiers vont être modifiés par la nouvelle grammaire...
    File tempDir = CtuluLibFile.createTempDir(scenario.getId());
    try {
      ScenarioSaverProcess saver = new ScenarioSaverProcess(scenario, managerEMHScenario, projet);
      saver.setOtherDir(tempDir);
      CtuluLogGroup logs = saver.saveWithScenarioType();
      if (logs.containsFatalError()) {
        logGroup.addGroup(logs);
        return;
      }
      //on copie les nouveaux fichiers ecrit dans un répertoire temp
      Map<String, File> savedFiles = projet.getInputFiles(managerEMHScenario, tempDir);
      Map<String, File> destFiles = projet.getInputFiles(managerEMHScenario);

      for (Map.Entry<String, File> entry : savedFiles.entrySet()) {
        String id = entry.getKey();
        File savedFile = entry.getValue();
        File destFile = destFiles.get(id);
        if (!destFile.exists() || CrueFileHelper.isDifferentFiles(savedFile, destFile)) {
          CrueFileHelper.copyFileWithSameLastModifiedDate(savedFile, destFile);
        }
      }
    } finally {
      CtuluLibFile.deleteDir(tempDir);
    }
  }
}
