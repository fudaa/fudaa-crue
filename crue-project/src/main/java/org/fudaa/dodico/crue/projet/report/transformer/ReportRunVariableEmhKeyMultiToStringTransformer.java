/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.transformer;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableEmhKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;

/**
 *
 * @author Frederic Deniger
 */
public class ReportRunVariableEmhKeyMultiToStringTransformer extends AbstractPropertyToStringTransformer<ReportRunVariableEmhKey> {

  private final AbstractPropertyToStringTransformer<ReportRunVariableKey> reportRunVariableKeyToStringTransformer;

  public ReportRunVariableEmhKeyMultiToStringTransformer(
          AbstractPropertyToStringTransformer<ReportRunVariableKey> reportRunVariableKeyToStringTransformer) {
    super(ReportRunVariableEmhKey.class);
    this.reportRunVariableKeyToStringTransformer = reportRunVariableKeyToStringTransformer;
  }

  @Override
  public String toStringSafe(ReportRunVariableEmhKey in) {
    return reportRunVariableKeyToStringTransformer.toString(in.getRunVariableKey()) + KeysToStringConverter.MAIN_SEPARATOR + in.getEmhName();
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }

  @Override
  public ReportRunVariableEmhKey fromStringSafe(String in) {
    String emhName = StringUtils.substringAfterLast(in, KeysToStringConverter.MAIN_SEPARATOR);
    if (StringUtils.isBlank(emhName)) {
      return null;
    }
    String key = StringUtils.substringBeforeLast(in, KeysToStringConverter.MAIN_SEPARATOR);
    ReportRunVariableKey reportRunVariableKey = reportRunVariableKeyToStringTransformer.fromString(key);
    if (reportRunVariableKey != null) {
      return new ReportRunVariableEmhKey(reportRunVariableKey, emhName);
    }
    return ReportRunVariableEmhKey.NULL;
  }
}
