/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

import java.io.File;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * @author Frederic Deniger
 */
public class ProjectNormalizableCallable implements Callable<ProjectNormalizableCallable.Result> {
  final EMHProjet projet;
  final ManagerEMHScenario scenario;

  public enum Status {
    ERROR,
    NOTHING_TO_DO,
    TO_NORMALIZE
  }

  public static class Result {
    public final Status state;
    public final CtuluLogGroup logs;
    public final ManagerEMHScenario managerEMHScenario;

    public Result(Status state, CtuluLogGroup logs, ManagerEMHScenario managerEMHScenario) {
      this.state = state;
      this.logs = logs;
      this.managerEMHScenario = managerEMHScenario;
    }

    public String getScenarioName() {
      return managerEMHScenario.getNom();
    }
  }

  public ProjectNormalizableCallable(EMHProjet projet, ManagerEMHScenario scenario) {
    this.projet = projet;
    this.scenario = scenario;
  }

  @Override
  public Result call() throws Exception {
    final ScenarioLoaderOperation loaderOperation = new ScenarioLoader(scenario, projet, projet.getCoeurConfig()).load(null);
    // on ne peut pas charger le scenario:
    if (loaderOperation.isErrorOrEmpty()) {
      return new Result(Status.ERROR, loaderOperation.getLogs(), scenario);
    }
    if (loaderOperation.getAutoModifiedState() != null && loaderOperation.getAutoModifiedState().isNormalized()) {
      return new Result(Status.TO_NORMALIZE, loaderOperation.getLogs(), scenario);
    }
    //on va vérifier si les fichiers vont être modifiés par la nouvelle grammaire...
    File tempDir = CtuluLibFile.createTempDir(scenario.getId());
    try {
      ScenarioSaverProcess saver = new ScenarioSaverProcess(loaderOperation.getResult(), scenario,
          projet);
      saver.setOtherDir(tempDir);
      CtuluLogGroup logs = saver.saveWithScenarioType();
      if (logs.containsFatalError()) {
        return new Result(Status.ERROR, loaderOperation.getLogs(), scenario);
      }
      //on copie les nouveaux fichiers ecrit dans un répertoire temp
      Map<String, File> savedFiles = projet.getInputFiles(scenario, tempDir);
      Map<String, File> destFiles = projet.getInputFiles(scenario);

      //ensuite on compare ancien et nouveau et si modification attendue ( fichier différent ou pas présent):
      for (Map.Entry<String, File> entry : savedFiles.entrySet()) {
        String id = entry.getKey();

        //fichier LHPT non concerné par cette comparaison
        if (ScenarioLoader.isLhpt(entry.getValue())) {
          continue;
        }
        File savedFile = entry.getValue();
        File destFile = destFiles.get(id);
        if (!destFile.exists() || CrueFileHelper.isDifferentFiles(savedFile, destFile)) {
          return new Result(Status.TO_NORMALIZE, logs, scenario);
        }
      }
    } finally {
      CtuluLibFile.deleteDir(tempDir);
    }
    return new Result(Status.NOTHING_TO_DO, null, scenario);
  }
}
