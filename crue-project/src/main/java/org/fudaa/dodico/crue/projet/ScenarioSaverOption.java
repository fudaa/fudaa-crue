/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import java.io.File;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;

/**
 * Used to configure the write phase of a project
 * 
 * @author deniger
 */
public class ScenarioSaverOption {

  private File baseDir;
  private CoeurConfigContrat crue10Version;

  /**
   * @return the baseDir used to determine the target files
   */
  public File getBaseDir() {
    return baseDir;
  }

  /**
   * @param baseDir the baseDir to set
   */
  public void setBaseDir(File baseDir) {
    this.baseDir = baseDir;
  }

  /**
   * @return the crue10Version
   */
  public CoeurConfigContrat getCrue10Version() {
    return crue10Version;
  }

  /**
   * @param crue10Version the crue10Version to set
   */
  public void setCrue10Version(CoeurConfigContrat crue10Version) {
    this.crue10Version = crue10Version;
  }

}
