/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.longitudinal;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLongitudinalPositionBuilder {

  private final ReportResultProviderServiceContrat service;
  private final CrueConfigMetier ccm;

  public ReportLongitudinalPositionBuilder(ReportResultProviderServiceContrat service, CrueConfigMetier ccm) {
    this.service = service;
    this.ccm = ccm;
  }

  public void buildPositions(double xpInit, ReportLongitudinalBrancheConfig brancheConfig, ReportLongitudinalPositionBrancheContent result,
          Collection<ReportRunKey> runKeys) {
    double xpMin = xpInit;
    //la longueur d'affichage de la branche
    final double getBrancheVisualLength = brancheConfig.getLength();
    double xpMax = xpMin + getBrancheVisualLength;
    ReportLongitudinalBrancheCartouche cartouche = new ReportLongitudinalBrancheCartouche(brancheConfig.getName(), xpMin, xpMax, brancheConfig.
            getDecalXp() < 0);
    result.getCartouches().add(cartouche);
    for (ReportRunKey reportRunKey : runKeys) {
      CatEMHBranche branche = (CatEMHBranche) service.getEMH(brancheConfig.getName(), reportRunKey);
      if (branche != null) {
        ReportLongitudinalPositionSectionByRun sectionsPosition = new ReportLongitudinalPositionSectionByRun(reportRunKey);
        cartouche.add(sectionsPosition);
        List<RelationEMHSectionDansBranche> sections = branche.getListeSectionsSortedXP(ccm);
        //la longueur reelle de la branche
        double realBrancheLength = sections.get(sections.size() - 1).getXp();
        if (ReportLongitudinalBrancheConfig.SensParcours.AVAL_AMONT.equals(brancheConfig.getSensParcours())) {
          Collections.reverse(sections);
        }
        double firstXp = sections.get(0).getXp();
        sectionsPosition.addSection(xpMin, sections.get(0).getEmh());
        if (realBrancheLength > 0) {
          for (int i = 1; i < sections.size() - 1; i++) {//le cas de branches de longueur nulle est ainsi réglée: elles ne possèdent que 2 sections
            double delta = Math.abs(sections.get(i).getXp() - firstXp);
            double visuDelta = delta / realBrancheLength * getBrancheVisualLength;
            sectionsPosition.addSection(xpMin + visuDelta, sections.get(i).getEmh());
          }
        }
        sectionsPosition.addSection(xpMax, sections.get(sections.size() - 1).getEmh());

      }
    }

  }

}
