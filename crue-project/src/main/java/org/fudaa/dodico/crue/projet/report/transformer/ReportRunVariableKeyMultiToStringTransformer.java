/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.transformer;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;

/**
 *
 * @author Frederic Deniger
 */
public class ReportRunVariableKeyMultiToStringTransformer extends AbstractPropertyToStringTransformer<ReportRunVariableKey> {

  private final AbstractPropertyToStringTransformer<ReportRunKey> reportRunKeyToStringTransformer;
  private final AbstractPropertyToStringTransformer<ReportVariableKey> reportVariableKeyToStringTransformer;

  public ReportRunVariableKeyMultiToStringTransformer(AbstractPropertyToStringTransformer<ReportRunKey> reportRunKeyToStringTransformer, AbstractPropertyToStringTransformer<ReportVariableKey> reportVariableKeyToStringTransformer) {
    super(ReportRunVariableKey.class);
    this.reportRunKeyToStringTransformer = reportRunKeyToStringTransformer;
    this.reportVariableKeyToStringTransformer = reportVariableKeyToStringTransformer;
  }

  @Override
  public String toStringSafe(ReportRunVariableKey in) {
    return reportRunKeyToStringTransformer.toString(in.getReportRunKey()) + KeysToStringConverter.MAIN_SEPARATOR + reportVariableKeyToStringTransformer.toString(in.getVariable());
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }

  @Override
  public ReportRunVariableKey fromStringSafe(String in) {
    String[] split = StringUtils.split(in, KeysToStringConverter.MAIN_SEPARATOR);
    if (split.length == 2) {
      final ReportRunKey reportRunKey = reportRunKeyToStringTransformer.fromString(split[0]);
      final ReportVariableKey reportVariableKey = reportVariableKeyToStringTransformer.fromString(split[1]);
      if (reportRunKey != null && reportVariableKey != null) {
        return new ReportRunVariableKey(reportRunKey, reportVariableKey);
      }
    }
    return ReportRunVariableKey.NULL_VALUE;
  }
}
