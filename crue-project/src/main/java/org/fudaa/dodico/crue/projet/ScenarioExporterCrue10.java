/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.common.FileIntegrityManager;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.CommentaireContainer;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.validation.ValidatorForCrue10ExportOldVersion;
import org.fudaa.dodico.crue.validation.ValidatorForIDCrue10;

import java.io.File;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Exporte les données au format crue 10. Il est possible de définir les formats à exporter.
 *
 * @author Frederic Deniger
 */
public class ScenarioExporterCrue10 {
  private final String fichierModele;
  private final EMHScenario scenarioToPersist;
  private final CoeurConfigContrat targetVersion;
  private final CrueData dataToExport;
  private final OrdonnanceurCrue10 ordonnanceur = new OrdonnanceurCrue10();
  private final Crue10FileFormatFactory factory = Crue10FileFormatFactory.getInstance();
  private File initDregFile;

  /**
   * Sauvegarder a partir d'un fichier modele:Fonction sauvegarder sous
   *
   * @param fichierModele     le fichier modele
   * @param scenarioToPersist le scenario a exporter
   * @param targetVersion     la version cible
   */
  public ScenarioExporterCrue10(final String fichierModele, final EMHScenario scenarioToPersist, CoeurConfigContrat targetVersion, File dregFile) {
    super();
    this.fichierModele = fichierModele;
    this.targetVersion = targetVersion;
    this.dataToExport = CrueDataImpl.buildConcatFor(scenarioToPersist, targetVersion.getCrueConfigMetier());
    this.scenarioToPersist = scenarioToPersist;
    this.initDregFile = dregFile;
  }

  protected static void validateCrueDataForCrue10(CtuluLogGroup error, CrueData crueData) {
    CtuluLog validateNomCrue10 = ValidatorForIDCrue10.validateNomsForCrue10(crueData.getScenarioData()).getRes();
    if (validateNomCrue10 != null && !validateNomCrue10.isEmpty()) {
      error.addLog(validateNomCrue10);
    }
  }

  /**
   * S'occupe de persister le scenario en crue 10 en mettant toutes les données dans un minimum de fichier: 1 fichier pour drso, 1
   * pour dfrt....
   *
   * @return true si réussite
   */
  public CtuluLogGroup export() {
    final CtuluLogGroup error = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    final boolean isV1_1_1 = Crue10VersionConfig.isV1_1_1(targetVersion);
    boolean isDregSupported = Crue10VersionConfig.isUpperThan_V1_2(targetVersion);
    File dregFile = null;
    if (Crue10VersionConfig.isUpperThan_V1_2(targetVersion)) {
      dregFile = initDregFile;
      if (dregFile == null) {
        try {
          dregFile = new File(targetVersion.getDefaultFile(CrueFileType.DREG).toURI());
        } catch (URISyntaxException e) {
          error.createLog().addSevereError(e.getMessage());
          return error;
        }
      }
    }
    //TODO: to remove
    if (isV1_1_1) {
      CtuluLog log = new ValidatorForCrue10ExportOldVersion().validate(scenarioToPersist);
      if (log != null && log.containsSevereError()) {
        error.addLog(log);
        return error;
      }
    }
    dataToExport.sort();

    // on recupere tous les fichiers a écrire
    final List<CrueFileType> allFiles = ordonnanceur.getAllFileType();
    validateCrueDataForCrue10(error, dataToExport);
    Map<Crue10FileFormat, File> filesByType = new HashMap<>();
    for (final CrueFileType crueFileType : allFiles) {

      //pas de optr en v1.1.1
      if (crueFileType.equals(CrueFileType.OPTR) && isV1_1_1) {
        continue;
      }
      //pour les fichier dreg, on copie le fichier pour les version >=1.3
      if (crueFileType.equals(CrueFileType.DREG)) {
        if (isDregSupported) {
          final File fileDest = CtuluLibFile.changeExtension(new File(fichierModele), CrueFileType.DREG.getExtension());
          CrueFileHelper.copyFileWithSameLastModifiedDate(dregFile, fileDest);
        }
        continue;
      }
      final Crue10FileFormat fileFormat = factory.getFileFormat(crueFileType, targetVersion);
      final File fileDest = CtuluLibFile.changeExtension(new File(fichierModele), fileFormat.getExtensions()[0]);
      filesByType.put(fileFormat, fileDest);
    }
    FileIntegrityManager backupManager = new FileIntegrityManager(filesByType.values());
    backupManager.start();
    try {
      for (Map.Entry<Crue10FileFormat, File> entry : filesByType.entrySet()) {
        final CrueIOResu<CrueData> out = new CrueIOResu<>(dataToExport);
        out.setCrueCommentaire(findCommentaire(entry.getKey().getFileType()));
        entry.getKey().write(out, entry.getValue(), error.createLog());
      }
    } finally {
      backupManager.finish(!error.containsFatalError());
      if (backupManager.getBackupLog().isNotEmpty()) {
        error.addLog(backupManager.getBackupLog());
      }
    }
    return error;
  }

  private String findCommentaire(CrueFileType file) {
    String res = findCommentaire(file, scenarioToPersist);
    if (res == null) {
      List<EMHModeleBase> modeles = scenarioToPersist.getModeles();
      for (EMHModeleBase model : modeles) {
        res = findCommentaire(file, model);
        if (res != null) {
          return res;
        }
        List<EMHSousModele> ssmodeles = model.getSousModeles();
        for (EMHSousModele ssModele : ssmodeles) {
          res = findCommentaire(file, ssModele);
          if (res != null) {
            return res;
          }
        }
      }
    }

    return res;
  }

  private String findCommentaire(CrueFileType type, CommentaireContainer container) {
    return container.getCommentairesManager().getCommentaire(type);
  }
}
