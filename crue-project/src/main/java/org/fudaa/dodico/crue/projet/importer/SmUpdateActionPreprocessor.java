package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOperationsData;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOptions;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateValidationData;

import java.io.File;

public class SmUpdateActionPreprocessor {
  private final EMHSousModele targetSousModele;
  private final CoeurConfig coeurConfigDefault;

  public SmUpdateActionPreprocessor(EMHSousModele targetSousModele, CoeurConfig coeurConfigDefault) {
    this.targetSousModele = targetSousModele;
    this.coeurConfigDefault = coeurConfigDefault;
  }

  public CrueOperationResult<SmUpdateOperationsData> process(File selectedDrsoFile, SmUpdateOptions options) {
    CtuluLogGroup logs = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    logs.setDescription("sm.updater.log");
    String targetSousModeleName = targetSousModele.getNom();
    logs.setDescriptionArgs(targetSousModeleName);
    final CrueOperationResult<EMHSousModele> res = new SmUpdateSourceLoader(coeurConfigDefault).updateFrom(selectedDrsoFile);
    logs.addGroup(res.getLogs());
    if (res.getLogs().containsError() || res.getResult() == null) {
      return new CrueOperationResult<>(null, logs);
    }

    EMHSousModele sourceSousModele = res.getResult();
    //on valide la mise à jour
    final EMHScenario targetScenario = targetSousModele.getParent().getParent();
    final SmUpdateValidationData smUpdateValidatorData = new SmUpdateOperationsValidator(targetScenario).validate(sourceSousModele, options, targetSousModeleName);
    final CtuluLogGroup validationLogs = new SmUpdateValidatorLogCreator().extractLog(smUpdateValidatorData);
    logs.addGroup(validationLogs);
    if (validationLogs.containsError()) {
      return new CrueOperationResult<>(null, logs);
    }
    final SmUpdateOperationsData smUpdateOperationsData = new SmUpdateOperationsDataExtractor(targetScenario).extract(sourceSousModele, options, targetSousModeleName);
    SmUpdateOperationsLogCreator operationsLogs = new SmUpdateOperationsLogCreator(coeurConfigDefault.getCrueConfigMetier());
    final CtuluLogGroup operationLogs = operationsLogs.extractLog(smUpdateOperationsData);
    logs.addGroup(operationLogs);
    return new CrueOperationResult<>(smUpdateOperationsData, logs);
  }
}
