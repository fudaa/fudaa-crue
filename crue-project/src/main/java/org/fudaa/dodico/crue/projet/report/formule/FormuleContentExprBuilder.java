/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluParser;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.metier.emh.OrdResScenario;
import org.fudaa.dodico.crue.metier.result.OrdResExtractor;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;
import org.fudaa.dodico.crue.projet.report.formule.function.AggregationFunctionDefault;
import org.fudaa.dodico.crue.projet.report.formule.function.EnumFormuleStat;
import org.fudaa.dodico.crue.projet.report.formule.function.FormuleParser;

import java.util.*;

/**
 * @author Frederic Deniger
 */
public class FormuleContentExprBuilder {
  private final ReportGlobalServiceContrat reportGlobalService;

  public FormuleContentExprBuilder(ReportGlobalServiceContrat reportGlobalService) {
    this.reportGlobalService = reportGlobalService;
    assert reportGlobalService != null;
  }

  public CtuluParser createParser(Collection<String> otherVar) {
    return createExpr(otherVar).getParser();
  }

  public CtuluExpr createExpr(Collection<String> otherVar) {
    final FormuleParser formuleParser = new FormuleParser();
    for (EnumFormuleStat value : EnumFormuleStat.values()) {
      formuleParser.addFunction(value.getExpression(), new AggregationFunctionDefault(value, reportGlobalService));
    }
    CtuluExpr expr = new CtuluExpr();
    expr.initVar();
    expr.setParser(formuleParser);
    fillWithVariables(expr, otherVar);
    return expr;
  }

  private void fillWithVariables(CtuluExpr expr, Collection<String> otherVar) {
    if (reportGlobalService.getResultService() == null || reportGlobalService.getResultService().getRunCourant() == null) {
      return;
    }
    OrdResScenario ordResScenario = reportGlobalService.getResultService().getRunCourant().getScenario().getOrdResScenario();
    OrdResExtractor extractor = new OrdResExtractor(ordResScenario);
    List<String> allSelectableVariables = extractor.getAllSelectableVariables(Collections.emptyList());
    final HashSet<String> all = new HashSet<>();
    for (String variablesRes : allSelectableVariables) {
      all.add(StringUtils.capitalize(variablesRes));
    }
    if (otherVar != null) {
      all.addAll(otherVar);
    }
    ArrayList<String> sortedList = CollectionCrueUtil.getSortedList(all);
    for (String var : sortedList) {
      expr.addVar(var, var);
    }
  }
}
