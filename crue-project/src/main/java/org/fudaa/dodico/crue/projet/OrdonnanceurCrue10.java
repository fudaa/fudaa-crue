/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Transformer;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * @author deniger
 */
public class OrdonnanceurCrue10 {

  private final static List<CrueFileType> SOUS_MODELE = Collections.unmodifiableList(Arrays.asList(CrueFileType.DRSO,
          CrueFileType.DFRT, CrueFileType.DPTG, CrueFileType.DCSP));
  private final static List<CrueFileType> MODELE = Collections.unmodifiableList(Arrays.asList(CrueFileType.OPTG,
          CrueFileType.OPTI, CrueFileType.PNUM, CrueFileType.DPTI, CrueFileType.OPTR,CrueFileType.DREG));
  private final static List<CrueFileType> SCENARIO = Collections.unmodifiableList(Arrays.asList(CrueFileType.DLHY,
          CrueFileType.DCLM, CrueFileType.PCAL, CrueFileType.OCAL, CrueFileType.ORES));

  /**
   * @return the sOUS_MODELE
   */
  public List<CrueFileType> getSousModele() {
    return SOUS_MODELE;
  }

  /**
   * @return toutes les extensions utilisées ( dcsp.xml par exemple)
   */
  public static List<String> getAllExtensions() {
    final List<String> allFiles = new ArrayList<>(20);
    final Transformer trans = new Transformer() {

      @Override
      public Object transform(final Object input) {
        return input.toString().toLowerCase() + ".xml";
      }
    };
    CollectionUtils.collect(SOUS_MODELE, trans, allFiles);
    CollectionUtils.collect(MODELE, trans, allFiles);
    CollectionUtils.collect(SCENARIO, trans, allFiles);
    return allFiles;

  }

  /**
   * @return tous les types supportes
   */
  public List<CrueFileType> getAllFileType() {
    final List<CrueFileType> allFiles = new ArrayList<>(20);
    allFiles.addAll(getSousModele());
    allFiles.addAll(getModele());
    allFiles.addAll(getScenario());
    return allFiles;
  }

  /**
   * @return the modele
   */
  public List<CrueFileType> getModele() {
    return MODELE;
  }

  /**
   * @return the scenario
   */
  public List<CrueFileType> getScenario() {
    return SCENARIO;
  }
}
