package org.fudaa.dodico.crue.projet.importer.data;

public class SmUpdateOptions {
  private final boolean allSections;
  private final boolean casiers;
  private final boolean branches;

  public SmUpdateOptions() {
    this(true, true, true);
  }

  public SmUpdateOptions(boolean allSections, boolean casiers, boolean branches) {
    this.allSections = allSections;
    this.casiers = casiers;
    this.branches = branches;
  }

  public boolean isAllSections() {
    return allSections;
  }

  public boolean isBranches() {
    return branches;
  }

  public boolean isCasiers() {
    return casiers;
  }

  public boolean nothingImported() {
    return !casiers && !branches && !allSections;
  }
}
