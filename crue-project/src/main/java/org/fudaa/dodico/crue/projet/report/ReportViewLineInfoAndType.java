/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;

/**
 *
 * @author Frederic Deniger
 */
public class ReportViewLineInfoAndType {

  private final ReportContentType type;
  private final ReportViewLineInfo info;
  private final String id;
  private final String description;

  public ReportViewLineInfoAndType(ReportContentType type, ReportViewLineInfo info) {
    this.type = type;
    this.info = info;
    id = type.getFolderName() + FOLDER_NAME_SEPARATOR + CtuluLibFile.getSansExtension(info.getFilename());
    description = id + NOM_SEPARATOR + info.getNom() + ")";
  }
  public static final String NOM_SEPARATOR = " (";
  public static final String FOLDER_NAME_SEPARATOR = "/";

  public boolean isSameId(String descriptionOrID) {
    String givenId = StringUtils.substringBefore(descriptionOrID, NOM_SEPARATOR);
    return id.equals(givenId);
  }

  public static ReportContentType getContentType(String descriptionOrID) {
    String folder = StringUtils.substringBefore(descriptionOrID, FOLDER_NAME_SEPARATOR);
    final ReportContentType[] values = ReportContentType.values();
    for (ReportContentType reportContentType : values) {
      if (reportContentType.getFolderName().equals(folder)) {
        return reportContentType;
      }
    }
    return null;
  }

  public String getId() {
    return id;
  }

  public String getDescription() {
    return description;
  }

  public ReportContentType getType() {
    return type;
  }

  public ReportViewLineInfo getInfo() {
    return info;
  }

}
