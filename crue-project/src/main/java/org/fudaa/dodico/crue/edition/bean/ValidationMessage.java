/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition.bean;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;

/**
 *
 * @author Frédéric Deniger
 */
public class ValidationMessage<T> {

  public static class ResultItem<T> {

    public T bean;
    public final List<String> messages = new ArrayList<>();
  }
  public boolean accepted = true;
  public CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
  public final List<ResultItem<T>> messages = new ArrayList<>();
}
