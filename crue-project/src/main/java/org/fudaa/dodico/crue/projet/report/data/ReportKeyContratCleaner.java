/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Frederic Deniger
 */
public class ReportKeyContratCleaner {

  public static void cleanNotCurrent(Collection<? extends ReportKeyContract> toClean) {
    for (Iterator<? extends ReportKeyContract> it = toClean.iterator(); it.hasNext();) {
      ReportKeyContract reportKeyContract = it.next();
      if (reportKeyContract.getReportRunKey() != null && !reportKeyContract.getReportRunKey().isCourant()) {
        it.remove();
      }
    }
  }

  public static void cleanNotCurrent(Map<? extends ReportKeyContract, ?> toClean) {
    cleanNotCurrent(toClean.keySet());
  }
}
