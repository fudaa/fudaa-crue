/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import java.util.Collection;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.ctulu.CtuluParser;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleContentExpr extends FormuleContent<FormuleParametersExpr, FormuleCalculatorExpr> {

  private final ReportGlobalServiceContrat globalService;

  public FormuleContentExpr(ReportGlobalServiceContrat service) {
    this.globalService = service;
    assert globalService != null;
  }

  @Override
  public CtuluLogResult<FormuleCalculatorExpr> initCalculator(FormuleParametersExpr parameters, Collection<String> variableFC) {
    FormuleContentExprBuilder builder = new FormuleContentExprBuilder(globalService);
    CtuluParser parser = builder.createParser(variableFC);
    parser.parseExpression(parameters.getFormule());
    CtuluLog log = new CtuluLog();
    if (parser.hasError()) {
      log.addSevereError(parser.getErrorInfo());
    }
    FormuleCalculatorExpr calculator = new FormuleCalculatorExpr(globalService.getResultService(), parser, parameters.getNom());
    return new CtuluLogResult<>(calculator, log);
  }

  @Override
  public String getPropertyNature() {
    return getParameters().getNature();
  }
}
