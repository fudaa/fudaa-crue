/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;

/**
 *
 * @author fred
 */
public class EditionBrancheDcspCreator {

  final EditionLoiCreator loiCreator;

  public EditionBrancheDcspCreator() {
    this(new UniqueNomFinder());
  }

  public EditionBrancheDcspCreator(final UniqueNomFinder nomFinder) {
    loiCreator = new EditionLoiCreator(nomFinder);
  }

  public DonCalcSansPrt createDcsp(final CrueConfigMetier ccm, final CatEMHBranche branche) {
    final EnumBrancheType brancheType = branche.getBrancheType();
    final String nom = branche.getNom();
    final EMHScenario scenario = branche.getParent().getParent().getParent();
    switch (brancheType) {
      case EMHBrancheBarrageFilEau:
        return createDcspBrancheBarrageFilEau(ccm, nom, scenario);
      case EMHBrancheBarrageGenerique:
        return createDcspBrancheBarrageGenerique(ccm, nom, scenario);
      case EMHBrancheEnchainement:
        return null;
      case EMHBrancheNiveauxAssocies:
        return createDcspBrancheNiveauxAssocies(ccm, nom, scenario);
      case EMHBrancheOrifice:
        return createDcspBrancheOrifice(ccm);
      case EMHBranchePdc:
        return createDcspBranchePdc(ccm, nom, scenario);
      case EMHBrancheSaintVenant:
        return createDcspBrancheSaintVenant(ccm);
      case EMHBrancheSeuilLateral:
        return createDcspBrancheSeuilLateral(ccm);
      case EMHBrancheSeuilTransversal:
        return createDcspBrancheSeuilTransversal(ccm);
      case EMHBrancheStrickler:
        return null;
    }
    throw new IllegalAccessError("type " + branche.getBrancheType() + " not supported");
  }

  protected DonCalcSansPrtBrancheBarrageFilEau createDcspBrancheBarrageFilEau(final CrueConfigMetier ccm, final String name, final EMHScenario scenario) {
    final DonCalcSansPrtBrancheBarrageFilEau dcsp = new DonCalcSansPrtBrancheBarrageFilEau(ccm);
    dcsp.setRegimeManoeuvrant(loiCreator.createLoiFF(name, scenario, EnumTypeLoi.LoiQpilZam, ccm));
    dcsp.addElemBarrage(new ElemBarrage(ccm));
    return dcsp;
  }

  public DonCalcSansPrtBrancheBarrageGenerique createDcspBrancheBarrageGenerique(final CrueConfigMetier ccm, final String name, final EMHScenario scenario) {
    final DonCalcSansPrtBrancheBarrageGenerique dcsp = new DonCalcSansPrtBrancheBarrageGenerique(ccm);
    dcsp.setRegimeDenoye(loiCreator.createLoiFF(name, scenario, EnumTypeLoi.LoiQpilZam, ccm));
    dcsp.setRegimeNoye(loiCreator.createLoiFF(name, scenario, EnumTypeLoi.LoiQDz, ccm));
    return dcsp;
  }

  public DonCalcSansPrtBrancheNiveauxAssocies createDcspBrancheNiveauxAssocies(final CrueConfigMetier ccm, final String name, final EMHScenario scenario) {
    final DonCalcSansPrtBrancheNiveauxAssocies dcsp = new DonCalcSansPrtBrancheNiveauxAssocies(ccm);
    dcsp.setZasso(loiCreator.createLoiFF(name, scenario, EnumTypeLoi.LoiZavZam, ccm));
    return dcsp;
  }

  public DonCalcSansPrtBrancheOrifice createDcspBrancheOrifice(final CrueConfigMetier ccm) {
    final DonCalcSansPrtBrancheOrifice donCalcSansPrtBrancheOrifice = new DonCalcSansPrtBrancheOrifice();
    donCalcSansPrtBrancheOrifice.setElemOrifice(new ElemOrifice(ccm));
    return donCalcSansPrtBrancheOrifice;
  }

  public DonCalcSansPrtBranchePdc createDcspBranchePdc(final CrueConfigMetier ccm, final String name, final EMHScenario scenario) {
    final DonCalcSansPrtBranchePdc dcsp = new DonCalcSansPrtBranchePdc();
    dcsp.setPdc(loiCreator.createLoiFF(name, scenario, EnumTypeLoi.LoiQPdc, ccm));
    return dcsp;
  }

  public DonCalcSansPrtBrancheSaintVenant createDcspBrancheSaintVenant(final CrueConfigMetier ccm) {
    final DonCalcSansPrtBrancheSaintVenant dcsp = new DonCalcSansPrtBrancheSaintVenant(ccm);
    return dcsp;
  }

  public DonCalcSansPrtBrancheSeuilLateral createDcspBrancheSeuilLateral(final CrueConfigMetier ccm) {
    final DonCalcSansPrtBrancheSeuilLateral dcsp = new DonCalcSansPrtBrancheSeuilLateral();
    dcsp.setFormulePdc(EnumFormulePdc.BORDA);
    dcsp.addElemSeuilAvecPdc(new ElemSeuilAvecPdc(ccm));
    return dcsp;
  }

  public DonCalcSansPrtBrancheSeuilTransversal createDcspBrancheSeuilTransversal(final CrueConfigMetier ccm) {
    final DonCalcSansPrtBrancheSeuilTransversal dcsp = new DonCalcSansPrtBrancheSeuilTransversal();
    dcsp.addElemSeuilAvecPdc(new ElemSeuilAvecPdc(ccm));
    dcsp.setFormulePdc(EnumFormulePdc.BORDA);
    return dcsp;
  }
}
