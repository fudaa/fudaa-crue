/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.longitudinal;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.metier.comparator.EMHByUserPositionComparator;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;
import org.fudaa.dodico.crue.projet.report.data.ReportKeyContract;
import org.fudaa.dodico.crue.projet.report.data.ReportKeyContratCleaner;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableHelper;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;
import org.fudaa.dodico.crue.projet.report.loi.ViewCourbeConfig;
import org.fudaa.dodico.crue.projet.report.persist.AbstractReportCourbeConfig;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.dodico.crue.projet.report.persist.VariableUpdateHelper;
import org.fudaa.ebli.courbe.EGCourbePersist;

/**
 * @author Frederic Deniger
 */
public class ReportLongitudinalConfig extends AbstractReportCourbeConfig {

  /**
   * la liste des variables ( z, etiquette, resultat) à dessiner sur le graphe
   */
  @XStreamImplicit(itemFieldName = "Variable-RunVariable")
  List<ReportRunVariableKey> profilVariables = new ArrayList<>();
  /**
   * la liste des branches avec leur configuration.
   */
  @XStreamImplicit(itemFieldName = "Branche")
  List<ReportLongitudinalBrancheConfig> branchesConfig = new ArrayList<>();
  /**
   * pas de temps à afficher ( hors pas de temps actif)
   */
  @XStreamImplicit(itemFieldName = "Temps")
  List<ResultatTimeKey> times = new ArrayList<>();
  /**
   * la config pour les courbes:
   */
  @XStreamAlias("Courbe-Configs")
  Map<ReportRunVariableTimeKey, EGCourbePersist> courbeconfigs = new HashMap<>();
  @XStreamAlias("Banniere-Config")
  ReportLongitudinalBannerConfig bannerConfig = new ReportLongitudinalBannerConfig();
  @XStreamAlias("Vue-Config")
  ViewCourbeConfig loiLegendConfig = new ViewCourbeConfig();

  @XStreamAlias("Temps-courant-affiche")
  Boolean currentTimeDisplayed = Boolean.TRUE;

  public ReportLongitudinalConfig() {

  }

  public ReportLongitudinalConfig(ReportLongitudinalConfig from) {
    super(from);
    if (from == null) {
      return;
    }
    if (from.profilVariables != null) {
      this.profilVariables.addAll(from.profilVariables);
    }
    if (from.branchesConfig != null) {
      from.branchesConfig.forEach(b -> this.branchesConfig.add(b.duplicate()));
    }
    if (from.times != null) {
      this.times.addAll(from.times);
    }
    if (from.courbeconfigs != null) {
      from.courbeconfigs.forEach((key, value) -> this.courbeconfigs.put(key, value.duplicate()));
    }
    if (from.bannerConfig != null) {
      this.bannerConfig = from.bannerConfig.duplicate();
    }
    if (from.loiLegendConfig != null) {
      this.loiLegendConfig = from.loiLegendConfig.dpulicate();
    }
    this.currentTimeDisplayed = from.currentTimeDisplayed;
  }

  @Override
  public ReportLongitudinalConfig duplicate() {
    return new ReportLongitudinalConfig(this);
  }

  @Override
  public ViewCourbeConfig getLoiLegendConfig() {
    return loiLegendConfig;
  }

  public Map<ReportRunVariableTimeKey, EGCourbePersist> getCourbeconfigs() {
    return courbeconfigs;
  }

  @Override
  protected void cleanUnusedCourbesConfig(Set<ReportKeyContract> usedKeys) {
    for (Iterator<ReportRunVariableTimeKey> it = courbeconfigs.keySet().iterator(); it.hasNext(); ) {
      if (!usedKeys.contains(it.next())) {
        it.remove();
      }
    }
  }

  @Override
  public boolean variablesUpdated(FormuleDataChanges changes) {
    boolean modified = super.axeVariablesUpdated(changes);
    modified |= VariableUpdateHelper.updateViewCourbeConfig(loiLegendConfig, changes);
    Pair<List<ReportRunVariableKey>, Boolean> res = VariableUpdateHelper.variablesUpdatedVariable(profilVariables, changes);
    if (Boolean.TRUE.equals(res.second)) {
      profilVariables = res.first;
      modified = true;
    }
    modified |= VariableUpdateHelper.updateCourbeConfig(courbeconfigs, changes);
    return modified;
  }

  public Boolean getCurrentTimeDisplayed() {
    return currentTimeDisplayed == null ? Boolean.TRUE : currentTimeDisplayed;
  }

  public void setCurrentTimeDisplayed(Boolean currentTimeDisplayed) {
    this.currentTimeDisplayed = currentTimeDisplayed;
  }

  public List<ResultatTimeKey> getTimes() {
    return times;
  }

  @Override
  public Collection<ResultatTimeKey> getSelectedTimeStepInReport() {
    return Collections.unmodifiableCollection(times);
  }

  @Override
  public ReportConfigContrat createTemplateConfig() {
    final ReportLongitudinalConfig duplicate = (ReportLongitudinalConfig) duplicate();
    duplicate.clearExternFiles();
    duplicate.loiLegendConfig.clearLabelTxtTooltip();
    duplicate.branchesConfig.clear();
    duplicate.times.clear();
    duplicate.clearZooms();
    duplicate.courbeconfigs.clear();
    ReportKeyContratCleaner.cleanNotCurrent(duplicate.profilVariables);
    return duplicate;
  }

  public boolean containsNegativeDecal() {
    for (ReportLongitudinalBrancheConfig reportLongitudinalBrancheConfig : branchesConfig) {
      if (reportLongitudinalBrancheConfig.getDecalXp() < 0) {
        return true;
      }
    }
    return false;
  }

  public List<ReportRunVariableKey> getProfilVariables() {
    return profilVariables;
  }

  @Override
  public void reinitContent() {
    super.reinitContent();
    if (profilVariables == null) {
      profilVariables = new ArrayList<>();
    }
    if (branchesConfig == null) {
      branchesConfig = new ArrayList<>();
    }
    if (times == null) {
      times = new ArrayList<>();
    }
    if (currentTimeDisplayed == null) {
      currentTimeDisplayed = Boolean.TRUE;
    }
    if (courbeconfigs == null) {
      courbeconfigs = new HashMap<>();
    }
    if (loiLegendConfig == null) {
      loiLegendConfig = new ViewCourbeConfig();
    }
    loiLegendConfig.reinit();
    if (bannerConfig == null) {
      bannerConfig = new ReportLongitudinalBannerConfig();
    }
    bannerConfig.reinit();
    profilVariables.remove(null);
    profilVariables.remove(ReportRunVariableKey.NULL_VALUE);
    times.remove(ResultatTimeKey.NULL_VALUE);
    branchesConfig.remove(null);
    this.courbeconfigs.remove(ReportRunVariableTimeKey.NULL);

  }

  public ReportLongitudinalBannerConfig getBannerConfig() {
    return bannerConfig;
  }

  @Override
  public ReportContentType getType() {
    return ReportContentType.LONGITUDINAL;
  }

  public String getVariableDrawnOnRightAxis(ReportResultProviderServiceContrat service) {
    for (ReportRunVariableKey vKey : profilVariables) {
      //pour eviter les limites -> vKey.isReadVariable()
      //uniquement Qini pour les RESULTAT_RPTI sur l'axe de droite
      if (vKey.getVariableName().equals(CrueConfigMetierConstants.PROP_QINI) || (vKey.isReadOrVariable() && ReportRunVariableHelper.isNotNatZVar(
          vKey.getVariable(), service))) {
        return vKey.getVariable().getVariableName();
      }
    }
    return null;
  }

  public void removeAllVariableDrawnOnRightAxis(ReportResultProviderServiceContrat service) {
    for (Iterator<ReportRunVariableKey> it = profilVariables.iterator(); it.hasNext(); ) {
      ReportRunVariableKey vKey = it.next();
      //pour eviter les limites -> vKey.isReadVariable()
      if (vKey.getVariableName().equals(CrueConfigMetierConstants.PROP_QINI) || (vKey.isReadOrVariable() && ReportRunVariableHelper.isNotNatZVar(
          vKey.getVariable(), service))) {
        it.remove();
      }
    }
  }

  public List<ReportRunVariableKey> getReportRunVariableKey(String varName) {
    List<ReportRunVariableKey> res = new ArrayList<>();
    for (ReportRunVariableKey vKey : profilVariables) {
      if (vKey.getVariable().getVariableName().equals(varName)) {
        res.add(vKey);
      }
    }
    return res;
  }

  public List<ReportRunVariableKey> getReportRunVariableRightAxis(ReportResultProviderServiceContrat service) {
    List<ReportRunVariableKey> res = new ArrayList<>();
    String var = getVariableDrawnOnRightAxis(service);
    if (var != null) {
      res.addAll(getReportRunVariableKey(var));
    }
    return res;
  }

  @Override
  public String getMainEMHName() {
    return null;
  }

  public List<String> getVariablesAsZDrawn(ReportRunKey runKey, ReportResultProviderServiceContrat service) {
    List<String> res = new ArrayList<>();
    for (ReportRunVariableKey vKey : profilVariables) {
      boolean profil = ReportVariableTypeEnum.LIMIT_PROFIL.equals(vKey.getVariable().getVariableType());
      boolean lhpt = ReportVariableTypeEnum.LHPT.equals(vKey.getVariable().getVariableType());
      if (runKey.equals(vKey.getRunKey()) && (lhpt || profil || ReportRunVariableHelper.isNatZVar(vKey.getVariable(), service))) {
        res.add(vKey.getVariable().getVariableName());
      }
    }
    return res;
  }

  public void addProfilVariable(ReportRunVariableKey reportRunVariableKey) {
    profilVariables.add(reportRunVariableKey);
  }

  public List<ReportLongitudinalBrancheConfig> getBranchesConfig() {
    return branchesConfig;
  }

  public void initBranchesWith(List<CatEMHBranche> branches) {
    if (CollectionUtils.isEmpty(branches)) {
      return;
    }
    EMHByUserPositionComparator comparator = new EMHByUserPositionComparator(branches.get(0).getParent().getParent().getParent());
    Collections.sort(branches, comparator);
    this.branchesConfig.clear();
    for (CatEMHBranche branche : branches) {
      ReportLongitudinalBrancheConfig conf = new ReportLongitudinalBrancheConfig();
      conf.setName(branche.getNom());
      conf.setLength(branche.getLength());
      this.branchesConfig.add(conf);
    }
  }
}
