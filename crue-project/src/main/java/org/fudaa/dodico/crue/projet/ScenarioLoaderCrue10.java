/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory.VersionResult;
import org.fudaa.dodico.crue.io.avct.AvctReader;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.etude.*;
import org.fudaa.dodico.crue.metier.factory.EMHNoeudFactory;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.validation.ValidateModeleScenarioWithSchema;

import java.io.File;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class ScenarioLoaderCrue10 {
  private static final Logger LOGGER = Logger.getLogger(ScenarioLoaderCrue10.class.getName());
  private final CtuluLog analyzer;
  private final CtuluLogGroup errorMng;
  private final Crue10FileFormatFactory factory = Crue10FileFormatFactory.getInstance();
  private final OrdonnanceurCrue10 ordonnanceurCrue10 = new OrdonnanceurCrue10();
  private final CoeurConfigContrat coeur;
  private final ManagerEMHScenario scenario;
  private final EMHProjet projet;
  private final Map<String, Long> lastModificationByEMHId = new HashMap<>();
  private final ScenarioAutoModifiedState modifiedState;

  /**
   * @param scenario      le {@link ManagerEMHScenario} a charger
   * @param ctuluLogGroup les groups de logs
   * @param ctuluLog      le log de cette operation
   */
  protected ScenarioLoaderCrue10(EMHProjet projet, final ManagerEMHScenario scenario, final CtuluLogGroup ctuluLogGroup,
                                 final CtuluLog ctuluLog, ScenarioAutoModifiedState modifiedState) {
    super();
    this.scenario = scenario;
    this.projet = projet;
    this.coeur = projet.getCoeurConfig();
    this.errorMng = ctuluLogGroup;
    this.analyzer = ctuluLog;
    this.modifiedState = modifiedState;
  }

  protected EMHProjet getProjet() {
    return projet;
  }

  protected ManagerEMHScenario getScenario() {
    return scenario;
  }

  public CoeurConfigContrat getCoeur() {
    return coeur;
  }

  protected EMHScenario compute(final Map<String, File> files, Map<String, File> resFiles, File runDir, EMHRun run) {
    Collection<File> values = files.values();
    for (File file : values) {
      if (ScenarioLoader.isLhpt(file)) {
        continue;
      }
      VersionResult findVersion = Crue10FileFormatFactory.findVersion(file);
      if (findVersion.getLog().containsErrorOrSevereError()) {
        errorMng.addLog(findVersion.getLog());
        return null;
      }
      String version = findVersion.getVersion();
      if (!version.equals(coeur.getXsdVersion())) {
        analyzer.addSevereError("crue10.scenarioVersionDifferentFromEtude", file.getName(), version, coeur.getXsdVersion());
      }
    }
    final Map<String, URL> urlFiles = CrueFileHelper.createFrom(files);
    final ValidateModeleScenarioWithSchema crue10Valid = new ValidateModeleScenarioWithSchema(urlFiles,
        scenario, coeur);
    crue10Valid.validate(errorMng);
    if (errorMng.containsError()) {
      return null;
    }
    EMHScenario emhScenario = ordonnanceChargementCrue10(files, coeur.getXsdVersion());
    if (emhScenario != null && !analyzer.containsSevereError() && MapUtils.isNotEmpty(resFiles)) {
      loadResFiles(resFiles, emhScenario, runDir, run);
    }
    return emhScenario;
  }

  private void loadResFiles(Map<String, File> resFiles, EMHScenario emhScenario, File runDir, EMHRun run) {
    // chargement de rptr et rpti par modele
    final CtuluLogGroup logs = errorMng.createGroup("load.resFile");
    ScenarioLoaderCrue10Result crue10ResultLoader = new ScenarioLoaderCrue10Result(this, emhScenario, logs, resFiles, run);
    for (EMHModeleBase modele : emhScenario.getModeles()) {
      if (modele.getActuallyActive()) {
        crue10ResultLoader.loadResultats(modele);
      }
    }
    if (runDir != null) {
      File stdout = new File(runDir, ScenarioLoaderCrue10Result.STDOUTCSV_NAME);
      if (stdout.exists()) {
        CompteRendusInfoEMH info = new CompteRendusInfoEMH();
        info.addCompteRendu(new CompteRendu(stdout, null));
        emhScenario.addInfosEMH(info);
      }
      //erreur dans l'execution
      File stderr = new File(runDir, ScenarioLoaderCrue10Result.STDERRCSV_NAME);
      if (stderr.exists()) {
        CompteRendusInfoEMH info = (CompteRendusInfoEMH) EMHHelper.selectInfoEMH(emhScenario, EnumInfosEMH.LOGS);
        if (info == null) {
          info = new CompteRendusInfoEMH();
          emhScenario.addInfosEMH(info);
        }
        info.addCompteRendu(new CompteRendu(stderr, null));
        emhScenario.addInfosEMH(info);
      }
      File avct = new File(runDir, AvctReader.getAvctName(scenario));
      if (avct.exists()) {
        AvctReader reader = new AvctReader();
        CrueIOResu<CompteRenduAvancement> read = reader.read(avct);
        if (read.getAnalyse() != null) {
          logs.addLog(read.getAnalyse());
        }
        if (read.getMetier() != null) {
          emhScenario.addInfosEMH(read.getMetier());
        }
      }
    }
  }

  /**
   * Ordonnance le chargement d'un projet crue 10: sous modeles puis modeles puis scenarios
   *
   * @param filesById les fichiers par leur id
   * @param version   la version
   * @return le scenario chargé ( null si erreur fatale).
   */
  private EMHScenario ordonnanceChargementCrue10(final Map<String, File> filesById, String version) {

    try {
      // -- recherche des fichiers sous modeles --//
      if (scenario.getFils().isEmpty()) {
        analyzer.addSevereError("noCurrentModele.error");
        return null;
      }
      final List<CrueFileType> typesSsModele = ordonnanceurCrue10.getSousModele();
      final List<CrueFileType> typesModele = ordonnanceurCrue10.getModele();
      final List<CrueFileType> typesScenario = ordonnanceurCrue10.getScenario();
      final EMHScenario emhScenario = new EMHScenario();
      emhScenario.setUserActive(scenario.isActive());
      final EMHNoeudFactory nodeFactory = new EMHNoeudFactory();
      for (final ManagerEMHModeleBase modele : scenario.getFils()) {
        final List<ManagerEMHSousModele> fils = modele.getFils();
        if (fils.isEmpty()) {
          analyzer.addSevereError("noCurrentSousModele.error", modele.getNom());
          return null;
        }

        // -- on charge en memoire le contenu des sous modeles --//
        final EMHModeleBase emhModele = new EMHModeleBase();
        emhModele.setNom(modele.getNom());
        emhModele.setUserActive(modele.isActive());

        for (final ManagerEMHSousModele managerSousModele : fils) {
          final CrueDataImpl tmp = new CrueDataImpl(nodeFactory, coeur.getCrueConfigMetier());
          tmp.withModifiedState(this.modifiedState);
          readFile(typesSsModele, filesById, managerSousModele, tmp, tmp.getSousModele(), version);
          // erreur fatale on arrete
          if (errorMng.containsFatalError()) {
            return null;
          }
          final EMHSousModele ssModele = tmp.getSousModele();
          ssModele.setNom(managerSousModele.getNom());
          ssModele.setUserActive(managerSousModele.isActive());
          EMHRelationFactory.addRelationContientEMH(emhModele, ssModele);
        }
        readFile(typesModele, filesById, modele, CrueDataImpl.buildFor(emhModele, coeur.getCrueConfigMetier()).withModifiedState(modifiedState), emhModele, version);
        if (errorMng.containsFatalError()) {
          return null;
        }

        EMHRelationFactory.addRelationContientEMH(emhScenario, emhModele);
      }
      readFile(typesScenario, filesById, scenario, CrueDataImpl.buildFor(emhScenario, coeur.getCrueConfigMetier()), emhScenario,
          version);
      if (errorMng.containsFatalError()) {
        return null;
      }
      return emhScenario;
    } catch (final Exception e) {
      analyzer.addError("io.xml.erreur", e.getMessage());
      LOGGER.log(Level.SEVERE, "io.xml.erreur", e);
    }
    return null;
  }

  private void readFile(final List<CrueFileType> typesToRead, final Map<String, File> filesById,
                        final ManagerEMHContainerBase content,
                        final CrueData dest, CommentaireContainer coms, String version) {

    long lastModification = 0;
    for (final CrueFileType type : typesToRead) {
      CrueIOResu read = null;
      if (CrueFileType.OPTR.equals(type) && Crue10VersionConfig.V_1_1_1.equals(version)) {
        final CtuluLog createLog = errorMng.createLog();
        createLog.setDesc("load.fichierViergeOptr");
        Crue10FileFormatFactory.getVersion(CrueFileType.OPTR, coeur).read(coeur.getDefaultFile(CrueFileType.OPTR), createLog, dest);
        coms.getCommentairesManager().setCommentaire(CrueFileType.OPTR, StringUtils.EMPTY);
      } else {
        FichierCrue file = content.getListeFichiers().getFile(type);
        if (CrueFileType.DREG.equals(type) && Crue10VersionConfig.isLowerOrEqualsTo_V1_2(version)) {
          //pour les version <=1.2 pas de fichier dreg
          continue;
        }
        final String id = file.getNom();
        File f = filesById.get(id);
        if (f.lastModified() > lastModification) {
          lastModification = f.lastModified();
        }
        final VersionResult formatFinderResult = Crue10FileFormatFactory.findVersion(f);
        if (formatFinderResult.getLog().containsErrorOrSevereError()) {
          errorMng.addLog(formatFinderResult.getLog());
          return;
        }
        if (!coeur.getXsdVersion().equals(formatFinderResult.getVersion())) {
          throw new IllegalAccessError("version are different");
        }
        if (CrueFileType.DREG.equals(type)) {
          continue;
        }
        final Crue10FileFormat fmt = factory.getFileFormat(type, coeur);

        if (fmt == null) {
          analyzer.addSevereError("filetype.unknown.error", type);
          return;
        }
        read = fmt.read(f, errorMng.createLog(), dest);
      }
      if (read != null) {
        coms.getCommentairesManager().setCommentaire(type, read.getCrueCommentaire());
      }
    }
    lastModificationByEMHId.put(content.getId(), Long.valueOf(lastModification));
  }
}
