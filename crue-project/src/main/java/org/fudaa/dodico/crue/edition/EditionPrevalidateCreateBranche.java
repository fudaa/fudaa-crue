/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.edition.bean.AbstractListContent;
import org.fudaa.dodico.crue.edition.bean.ListBrancheContent;
import org.fudaa.dodico.crue.edition.bean.ValidationMessage;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;

/**
 *
 * @author Frédéric Deniger
 */
public class EditionPrevalidateCreateBranche {

  public ValidationMessage<ListBrancheContent> validate(final EMHSousModele sousModele, final Collection<ListBrancheContent> contents, final CrueConfigMetier ccm) {
    final ValidationMessage<ListBrancheContent> res = new ValidationMessage<>();
    final CtuluLog log = res.log;
    final List<EMH> existingBranche = sousModele.getParent().getParent().getIdRegistry().getEMHs(EnumCatEMH.BRANCHE);
    final Set<String> ids = TransformerHelper.toSetOfId(existingBranche);
    final Map<String, CatEMHNoeud> noeudsByIds = TransformerHelper.toMapOfNom(sousModele.getNoeuds());
    final ItemVariable xpProperty = ccm.getProperty(CrueConfigMetierConstants.PROP_XP);

    for (final ListBrancheContent content : contents) {
      final String nom = content.getNom();
      final List<CtuluLogRecord> msgs = new ArrayList<>();
      if (!ValidationPatternHelper.isNameValide(nom, EnumCatEMH.BRANCHE)) {
        msgs.add(log.addSevereError("AddEMH.NameNotValid", nom));
      }
      if (AbstractListContent.nameUsed(content, contents)) {
        msgs.add(log.addSevereError("AddEMH.NameAlreadyUsedInNewEMH", nom));
      }
      if (ids.contains(nom.toUpperCase())) {
        msgs.add(log.addSevereError("AddEMH.NameAlreadyUsedByExistingEMH", nom));
      }
      final EnumBrancheType brancheType = content.getType();
      if (brancheType == null) {
        msgs.add(log.addSevereError("AddEMH.BrancheTypeUnknown", nom));
      }
      final CatEMHNoeud noeudAmont = noeudsByIds.get(content.getNoeudAmont());
      final CatEMHNoeud noeudAval = noeudsByIds.get(content.getNoeudAval());
      if (noeudAmont == null) {
        msgs.add(log.addSevereError("AddEMH.BrancheNoeudAmontUnknown", nom, content.getNoeudAmont()));
      }
      if (noeudAval == null) {
        msgs.add(log.addSevereError("AddEMH.BrancheNoeudAvalUnknown", nom, content.getNoeudAval()));
      } else if (noeudAval.equals(noeudAmont)) {
        msgs.add(log.addSevereError("AddEMH.BrancheNoeudAmontSameThanAval", nom));
      }
      Pair<String, Object[]> valueError = xpProperty.getValidator().getValidateErrorParamForValue(content.getLongueur());
      if (valueError != null) {
        msgs.add(log.addSevereError(valueError.first, valueError.second));
      }
      valueError = EditionBrancheCreator.isDistanceValidMessage(brancheType, content.getLongueur(), ccm);
      if (valueError != null) {
        msgs.add(log.addSevereError(valueError.first, valueError.second));
      }
      if (!msgs.isEmpty()) {
        res.accepted = false;
        final ValidationMessage.ResultItem<ListBrancheContent> item = new ValidationMessage.ResultItem<>();
        item.bean = content;
        res.messages.add(item);
        for (final CtuluLogRecord ctuluLogRecord : msgs) {
          BusinessMessages.updateLocalizedMessage(ctuluLogRecord);
          item.messages.add(ctuluLogRecord.getLocalizedMessage());
        }
      }
    }
    return res;
  }
}
