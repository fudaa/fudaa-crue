/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

import org.fudaa.dodico.crue.common.BusinessMessages;

/**
 *
 * @author Frederic Deniger
 */
public class ReportExpressionHelper {

  public final static String EXPR_NOM = "NOM";
  public final static String EXPR_COMMENTAIRE = "COMMENTAIRE";
  public final static String EXPR_PROFIL_NOM = "PROFIL.NOM";
  public final static String EXPR_PROFIL_COMMENTAIRE = "PROFIL.COMMENTAIRE";
  /**
   * Attention utilise par ReportVariableKeyCellRenderer pour la traduction
   */
  public static final String TIME_TEMPS_SCE = "TempsSce";
  /**
   * Attention utilise par ReportVariableKeyCellRenderer pour la traduction
   */
  public static final String TIME_TEMPS_SIMU = "TempsSimu";
  /**
   * Attention utilise par ReportVariableKeyCellRenderer pour la traduction
   */
  public static final String TIME_NOM_CALC = "NomCalc";

  public static String geti18n(String name) {
    if (EXPR_NOM.equals(name)) {
      return BusinessMessages.getString("Expression.Emh.Nom");
    }
    if (EXPR_COMMENTAIRE.equals(name)) {
      return BusinessMessages.getString("Expression.Emh.Commentaire");
    }
    if (EXPR_PROFIL_COMMENTAIRE.equals(name)) {
      return BusinessMessages.getString("Expression.Profil.Commentaire");
    }
    return name;
  }
}
