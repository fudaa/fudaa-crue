/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.edition.bean.*;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.dodico.crue.validation.ValidateInitialConditions;
import org.fudaa.dodico.crue.validation.util.DelegateValidatorBranche;

import java.util.*;

/**
 * @author deniger
 */
public class EditionCreateEMH {
  final EditionSectionCreator sectionCreator;
  private final EditionProfilCreator profilCreator;
  private final EditionCasierCreator casierCreator;
  private final EditionBrancheCreator brancheCreator;
  final UniqueNomFinder nomFinder = new UniqueNomFinder();
  private final EditionNoeudCreator noeudCreator = new EditionNoeudCreator(nomFinder);

  public EditionCreateEMH(final CreationDefaultValue defaultValues) {
    profilCreator = new EditionProfilCreator(nomFinder, defaultValues);
    casierCreator = new EditionCasierCreator(profilCreator);
    sectionCreator = new EditionSectionCreator(profilCreator);
    brancheCreator = new EditionBrancheCreator(sectionCreator, noeudCreator);
  }

  public static void addInScenario(final EMHSousModele sousModele, final EMH emh, final EMHScenario scenario) {
    EMHRelationFactory.addRelationContientEMH(sousModele, emh);
    scenario.getIdRegistry().register(emh);
  }

  /**
   * @param sousModele le sous-modele
   * @param casierType type de casier
   * @param initNoeud null ou un noeud sans casier
   * @return casier créé
   */
  public CatEMHCasier createCasier(final EMHSousModele sousModele, final EnumCasierType casierType, final CatEMHNoeud initNoeud,
                                   final CrueConfigMetier ccm) {
    CatEMHNoeud noeud = initNoeud;
    final EMHScenario parent = sousModele.getParent().getParent();
    String casierName;
    if (noeud == null) {
      final int currentMax = nomFinder.findMaxIdentifier(parent, EnumCatEMH.NOEUD, EnumCatEMH.CASIER);
      final String noeudName = nomFinder.getName(EnumCatEMH.NOEUD, currentMax);
      casierName = nomFinder.getName(EnumCatEMH.CASIER, currentMax);
      noeud = noeudCreator.createNoeud(noeudName, EnumNoeudType.EMHNoeudNiveauContinu, sousModele, parent, ccm);
    } else {
      assert initNoeud.getCasier() == null;
      casierName = CruePrefix.changePrefix(noeud.getNom(), EnumCatEMH.CASIER);
      //un casier porte deja le nom !
      if (nomFinder.isUsed(EnumCatEMH.CASIER, parent, casierName)) {
        final int currentMax = nomFinder.findMaxIdentifier(parent, EnumCatEMH.NOEUD, EnumCatEMH.CASIER);
        final String noeudName = nomFinder.getName(EnumCatEMH.NOEUD, currentMax);
        casierName = nomFinder.getName(EnumCatEMH.CASIER, currentMax);
        noeud.setNom(noeudName);
      }
    }
    final CatEMHCasier casier = casierCreator.createCasier(casierName, casierType, ccm, sousModele);
    casier.setNoeud(noeud);
    new ValidateInitialConditions().propagateChange(sousModele.getParent(), ccm);
    return casier;
  }

  public CatEMHBranche createBranche(final EMHSousModele defaultSousModele, final EnumBrancheType brancheType, final CatEMHNoeud noeudAmont,
                                     final CatEMHNoeud noeudAval, final CrueConfigMetier crueConfigMetier, final double distance) {
    return createBranche(null, defaultSousModele, brancheType, noeudAmont, noeudAval, crueConfigMetier, distance);
  }

  public CatEMHBranche createBranche(final String initBrancheName, final EMHSousModele defaultSousModele, final EnumBrancheType brancheType, final CatEMHNoeud noeudAmont,
                                     final CatEMHNoeud noeudAval, final CrueConfigMetier crueConfigMetier, final double distance) {
    final List<EnumBrancheType> brancheTypeWithDistanceNotNull = DelegateValidatorBranche.getBrancheTypeWithDistanceNotNull();
    double distanceToUse = distance;
    if (!brancheTypeWithDistanceNotNull.contains(brancheType)) {
      distanceToUse = 0;
    }
    final CatEMHBranche createBranche = brancheCreator
      .createBranche(initBrancheName, defaultSousModele, brancheType, noeudAmont, noeudAval, crueConfigMetier, distanceToUse);
    new ValidateInitialConditions().propagateChange(defaultSousModele.getParent(), crueConfigMetier);
    return createBranche;
  }

  public CatEMHNoeud createNoeud(final String noeudName, final EMHSousModele defaultSousModele, final EnumNoeudType enumNoeudType, final CrueConfigMetier ccm) {
    final CatEMHNoeud createNoeud = noeudCreator.createNoeud(noeudName, enumNoeudType, defaultSousModele, ccm);
    new ValidateInitialConditions().propagateChange(defaultSousModele.getParent(), ccm);
    return createNoeud;
  }

  public CatEMHNoeud createNoeud(final EMHSousModele defaultSousModele, final EnumNoeudType enumNoeudType, final CrueConfigMetier ccm) {
    final CatEMHNoeud createNoeud = noeudCreator.createNoeud(defaultSousModele, enumNoeudType, ccm);
    new ValidateInitialConditions().propagateChange(defaultSousModele.getParent(), ccm);
    return createNoeud;
  }

  public ValidationMessage<ListNoeudContent> createNoeuds(final EMHSousModele defaultSousModele, final Collection<ListNoeudContent> noeuds, final CrueConfigMetier ccm) {
    final EditionPrevalidateCreateNoeud validate = new EditionPrevalidateCreateNoeud();
    final ValidationMessage<ListNoeudContent> validateMessages = validate.validate(defaultSousModele, noeuds);
    if (!validateMessages.accepted) {
      return validateMessages;
    }
    //tout est ok:
    final EMHScenario scenario = defaultSousModele.getParent().getParent();
    for (final ListNoeudContent pair : noeuds) {
      final CatEMHNoeud createNoeud = noeudCreator.createNoeud(pair.getNom(), pair.getType(), defaultSousModele, scenario, ccm);
      createNoeud.setCommentaire(pair.getCommentaire());
    }
    new ValidateInitialConditions().propagateChange(defaultSousModele.getParent(), ccm);
    return validateMessages;
  }

  public ValidationMessage<ListCasierContent> createCasiers(final EMHSousModele defaultSousModele, final Collection<ListCasierContent> casiers, final CrueConfigMetier ccm) {
    final EditionPrevalidateCreateCasier validate = new EditionPrevalidateCreateCasier();
    final ValidationMessage<ListCasierContent> validateMessages = validate.validate(defaultSousModele, casiers, ccm);
    if (!validateMessages.accepted) {
      return validateMessages;
    }
    final Map<String, CatEMHNoeud> noeudsByNoms = TransformerHelper.toMapOfNom(defaultSousModele.getNoeuds());
    for (final ListCasierContent data : casiers) {
      final CatEMHNoeud nd = noeudsByNoms.get(data.getNoeud());
      final CatEMHCasier createCasier = casierCreator.createCasier(data.getNom(), data.getType(), ccm, defaultSousModele);
      createCasier.setCommentaire(data.getCommentaire());
      EMHRelationFactory.addNoeudCasier(createCasier, nd);
    }
    new ValidateInitialConditions().propagateChange(defaultSousModele.getParent(), ccm);
    return validateMessages;
  }

  /**
   * Attention, aucun test effectué. On s'assure uniquement que la section est nouvelle et que le nom n'est pas utilisé.
   *
   * @param section la section
   * @param sousModele le sous-modle
   * @param branche la branche de dest
   * @param xp le xp de la branche
   * @param ccm le {@link CrueConfigMetier}
   */
  public void addSection(final EMHSectionProfil section, final EMHSousModele sousModele, final EMHBrancheSaintVenant branche, final Double xp, final CrueConfigMetier ccm) {
    if (section.getUiId() != null) {
      return;
    }
    final EMHScenario parent = sousModele.getParent().getParent();
    if (nomFinder.isUsed(EnumCatEMH.SECTION, parent, section.getNom())) {
      return;
    }
    addInScenario(sousModele, section, parent);
    if (branche != null) {
      final RelationEMHSectionDansBranche relation = EMHRelationFactory.createSectionDansBrancheAndSetRelation(branche, section, xp, ccm);
      //la factory n'ajoute pas la relation branche -> section !
      branche.addRelationEMH(relation);
      AlgoEMHSectionSorter.reorderSections(branche, ccm);
    }
  }

  public ValidationMessage<ListRelationSectionContent> createSections(final EMHSousModele sousModele, final Collection<ListRelationSectionContent> sections,
                                                                      final CrueConfigMetier ccm) {
    final EditionPrevalidateCreateRelationSection validate = new EditionPrevalidateCreateRelationSection();
    final ValidationMessage<ListRelationSectionContent> validateMessages = validate.validate(sections, sousModele, ccm);
    if (!validateMessages.accepted) {
      return validateMessages;
    }
    final Map<String, EMHBrancheSaintVenant> branchesByNom = TransformerHelper.toMapOfNom(sousModele.getBranchesSaintVenant());
    final Map<String, CatEMHSection> sectionsByNom = TransformerHelper.toMapOfNom(sousModele.getSections());
    //on peut ajouter les sections.
    //dans un premier temps on créé les sections et ensuite on trie les branches:
    final Set<EMHBrancheSaintVenant> brancheToreorder = new HashSet<>();
    for (final ListRelationSectionContent sectionData : sections) {
      final CatEMHSection createdSection = createSection(sectionData, sousModele, ccm, sectionsByNom);
      if (StringUtils.isNotBlank(sectionData.getBrancheNom())) {
        final EMHBrancheSaintVenant branche = branchesByNom.get(sectionData.getBrancheNom());
        final RelationEMHSectionDansBranche relation = EMHRelationFactory
          .createSectionDansBrancheAndSetRelation(branche, createdSection, sectionData.getAbscisseHydraulique(), ccm);
        //la factory n'ajoute pas la relation branche -> section !
        branche.addRelationEMH(relation);
        brancheToreorder.add(branche);
      }
    }
    //trie des sections dans les branches
    for (final EMHBrancheSaintVenant branche : brancheToreorder) {
      AlgoEMHSectionSorter.reorderSections(branche, ccm);
    }
    new ValidateInitialConditions().propagateChange(sousModele.getParent(), ccm);
    return validateMessages;
  }

  /**
   * @param sectionData les données
   * @param sousModele le sous-modle
   * @param ccm le {@link CrueConfigMetier}
   * @param emhSectionRef la section de ref
   * @return la section demandée mais sans l'avoir ajoutée au scenario/sous-sousModele
   */
  public CatEMHSection createSectionDetached(final ListRelationSectionContent sectionData, final EMHSousModele sousModele, final CrueConfigMetier ccm,
                                             final CatEMHSection emhSectionRef) {
    return createSectionDetached(sectionData, sousModele, ccm, emhSectionRef, true);
  }

  /**
   * @param sectionData les données
   * @param sousModele le sous-modle
   * @param ccm le {@link CrueConfigMetier}
   * @param emhSectionRef la section de ref
   * @param createProfilSection true si le profilSection doit être créé
   * @return la section demandée mais sans l'avoir ajoutée au scenario/sous-sousModele
   */
  public CatEMHSection createSectionDetached(final ListRelationSectionContent sectionData, final EMHSousModele sousModele, final CrueConfigMetier ccm,
                                             final CatEMHSection emhSectionRef, boolean createProfilSection) {
    final EnumSectionType sectionType = sectionData.getSection().getSectionType();
    switch (sectionType) {
      case EMHSectionInterpolee:
        return sectionCreator.createSectionInterpoleeDetached(sectionData.getNom());
      case EMHSectionSansGeometrie:
        return sectionCreator.createSectionSansGeometrieDetached(sectionData.getNom());
      case EMHSectionIdem:
        return sectionCreator.createSectionIdemDetached(sectionData.getNom(), ccm, sectionData.getSection().getDz(), emhSectionRef);
      case EMHSectionProfil:
        return sectionCreator.createEMHSectionProfilDetached(sectionData.getNom(), ccm, sousModele, createProfilSection);
    }
    throw new IllegalAccessError("createSection type unknown: " + sectionType);
  }

  private CatEMHSection createSection(final ListRelationSectionContent sectionData, final EMHSousModele modele, final CrueConfigMetier ccm,
                                      final Map<String, CatEMHSection> sectionsByNom) {
    final EnumSectionType sectionType = sectionData.getSection().getSectionType();
    switch (sectionType) {
      case EMHSectionInterpolee:
        return sectionCreator.createSectionInterpolee(sectionData.getNom(), modele, modele.getParent().getParent(), ccm);
      case EMHSectionSansGeometrie:
        return sectionCreator.createSectionSansGeometrie(sectionData.getNom(), modele, modele.getParent().getParent(), ccm);
      case EMHSectionIdem:
        final CatEMHSection ref = sectionsByNom.get(sectionData.getSection().getReferenceSectionIdem());
        return sectionCreator
          .createSectionIdem(sectionData.getNom(), modele, modele.getParent().getParent(), ref, sectionData.getSection().getDz(), ccm);
      case EMHSectionProfil:
        return sectionCreator.createEMHSectionProfil(sectionData.getNom(), ccm, modele, modele.getParent().getParent());
    }
    throw new IllegalAccessError("createSection type unknown: " + sectionType);
  }

  public ValidationMessage<ListBrancheContent> createBranches(final EMHSousModele sousModele, final Collection<ListBrancheContent> branches, final CrueConfigMetier ccm) {
    final EditionPrevalidateCreateBranche validate = new EditionPrevalidateCreateBranche();
    final ValidationMessage<ListBrancheContent> validateMessages = validate.validate(sousModele, branches, ccm);
    if (!validateMessages.accepted) {
      return validateMessages;
    }
    final List<CatEMHNoeud> noeuds = sousModele.getNoeuds();
    final Map<String, CatEMHNoeud> noeudsByNoms = TransformerHelper.toMapOfNom(noeuds);
    for (final ListBrancheContent data : branches) {
      final CatEMHNoeud noeudAmont = noeudsByNoms.get(data.getNoeudAmont());
      final CatEMHNoeud noeudAval = noeudsByNoms.get(data.getNoeudAval());
      final CatEMHBranche branche = brancheCreator.createBranche(data.getNom(), sousModele, data.getType(), noeudAmont, noeudAval, ccm, data.getLongueur());
      branche.setCommentaire(data.getCommentaire());
    }
    new ValidateInitialConditions().propagateChange(sousModele.getParent(), ccm);
    return validateMessages;
  }
}
