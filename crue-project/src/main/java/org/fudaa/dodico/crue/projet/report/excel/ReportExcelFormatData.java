/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.excel;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Workbook;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.PropertyFormaterBuilder;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;

/**
 *
 * @author Frederic Deniger
 */
public class ReportExcelFormatData {

  private final Workbook wb;
  protected final DataFormat dataFormat;
  private final PropertyFormaterBuilder formaterBuilder = new PropertyFormaterBuilder();
  final Map<Integer, CellStyle> cellStyleByNbDecimal = new HashMap<>();

  public ReportExcelFormatData(Workbook wb) {
    this.wb = wb;
    dataFormat = wb.createDataFormat();
  }

 

  public CellStyle getStyle(PropertyNature nature, DecimalFormatEpsilonEnum presentionOrComparison) {
    int nbDec = formaterBuilder.getNbDecimal(nature, presentionOrComparison);
    CellStyle res = cellStyleByNbDecimal.get(nbDec);
    if (res == null) {
      final short format = dataFormat.getFormat("0." + StringUtils.repeat("0", nbDec));
      res = wb.createCellStyle();
      res.setDataFormat(format);
      cellStyleByNbDecimal.put(nbDec, res);
    }
    return res;

  }

}
