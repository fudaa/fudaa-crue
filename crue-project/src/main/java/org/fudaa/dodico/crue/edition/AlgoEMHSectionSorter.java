package org.fudaa.dodico.crue.edition;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EnumPosSection;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;

import java.util.List;

/**
 * @author deniger
 */
public class AlgoEMHSectionSorter {
    /**
     * Reorganise les sections d'une branche en mettant à jour les positions
     *
     * @param branche la branche
     * @param ccm     le {@link CrueConfigMetier}
     */
    public static void reorderSections(CatEMHBranche branche, CrueConfigMetier ccm) {
        List<RelationEMHSectionDansBranche> ordererListeSections = branche.getListeSectionsSortedXP(ccm);
        ordererListeSections.get(0).setPos(EnumPosSection.AMONT);
        final int size = ordererListeSections.size();
        for (int i = 1; i < size - 1; i++) {
            ordererListeSections.get(i).setPos(EnumPosSection.INTERNE);
        }
        ordererListeSections.get(size - 1).setPos(EnumPosSection.AVAL);
        //pour s'assurer que les relations sont dans le bon ordre:
        branche.sortRelationSectionDansBranche(ccm);
    }
}
