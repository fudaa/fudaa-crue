/*
 GPL 2
 */
package org.fudaa.dodico.crue.edition;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent;
import org.fudaa.dodico.crue.edition.bean.SectionEditionContent;
import org.fudaa.dodico.crue.edition.bean.SectionPrevalidationContent;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.validation.util.DelegateValidatorSection;
import org.fudaa.dodico.crue.validation.util.ValidationContentSectionDansBrancheExecutor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public class EditionChangeSection {
  private final CreationDefaultValue defaultValues;

  public EditionChangeSection(CreationDefaultValue defaultValues) {
    this.defaultValues = defaultValues;
  }

  public CtuluLog changeSection(CatEMHSection initSection, SectionEditionContent newContent, CrueConfigMetier ccm) {
    return changeSection(initSection, newContent, ccm, true);
  }

  public CtuluLog changeSection(CatEMHSection initSection, SectionEditionContent newContent, CrueConfigMetier ccm, boolean doPrevalidation) {
    CtuluLog log = doPrevalidation ? prevalidate(initSection, newContent, ccm) : new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    if (log.containsErrorOrSevereError()) {
      return log;
    }
    initSection.setCommentaire(newContent.getComment());
    new EditionRename().rename(initSection, newContent.getNewName());
    EnumSectionType newType = newContent.getNewType();
    final boolean isCurrentSectionIdem = EnumSectionType.EMHSectionIdem.equals(initSection.getSectionType());
    if (newType != null && !newType.equals(initSection.getSectionType())) {
      changeType(isCurrentSectionIdem, initSection, newContent, ccm);
    } else if (isCurrentSectionIdem) {
      final EMHSectionIdem sectionIdem = (EMHSectionIdem) initSection;
      DonPrtGeoSectionIdem dptg = EMHHelper.getDPTGSectionIdem(sectionIdem);
      dptg.setDz(newContent.getDz());
      CatEMHSection currentSectionRef = EMHHelper.getSectionRef(sectionIdem);
      if (newContent.getSectionRefUid() != null && !newContent.getSectionRefUid().equals(currentSectionRef.getUiId())) {
        CatEMHSection newRef = getSectionRef(EMHHelper.getScenario(initSection), newContent);
        EMHRelationFactory.removeRelationBidirect(currentSectionRef, initSection);
        EMHRelationFactory.addSectionRef(sectionIdem, newRef);
      }
    }

    return log;
  }

  private CatEMHSection getSectionRef(EMHScenario scenario, SectionEditionContent newContent) {
    if (newContent.getSectionRefUid() != null) {
      return (CatEMHSection) scenario.getIdRegistry().getEmh(newContent.getSectionRefUid());
    }
    return null;
  }

  public static SectionPrevalidationContent createPrevalidateContent(CatEMHSection section) {
    SectionPrevalidationContent res = new SectionPrevalidationContent();
    List<EMHSectionIdem> sectionReferencing = EMHHelper.getSectionReferencing(section);
    CatEMHBranche branche = section.getBranche();
    res.setBranche(branche);
    res.setBranchePilotedBy(EMHHelper.getBranchePilotedBy(section));
    res.setSectionReferencing(sectionReferencing);
    boolean isAval = branche != null && section == branche.getSectionAval().getEmh();
    boolean isAmont = branche != null && section == branche.getSectionAmont().getEmh();
    res.setIsAmont(isAmont);
    res.setIsAval(isAval);
    List<EnumSectionType> authorizedSections = new ArrayList<>();
    if (branche != null) {
      final boolean amontAval = isAmont || isAval;
      Collection<EnumSectionType> authorizedType = ValidationContentSectionDansBrancheExecutor.createSectionTypeDansBrancheContents(amontAval).get(branche.getBrancheType());
      authorizedSections.addAll(authorizedType);
    } else {
      EnumSectionType[] values = EnumSectionType.values();
      authorizedSections.addAll(Arrays.asList(values));
    }
    if (sectionReferencing != null && !sectionReferencing.isEmpty()) {
      List<EnumSectionType> authorizedSectionRefType = DelegateValidatorSection.getAuthorizedSectionRefType();
      authorizedSections.retainAll(authorizedSectionRefType);
    }
    res.setAuthorizedSections(authorizedSections);
    return res;
  }

  public CtuluLog prevalidate(CatEMHSection section, SectionEditionContent newContent, CrueConfigMetier ccm) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    EditionRename rename = new EditionRename();
    if (!newContent.getNewName().equals(section.getNom())) {
      String nameValid = rename.isNameValid(section, newContent.getNewName());
      if (nameValid != null) {
        log.addSevereError(nameValid);
      }
    }
    EnumSectionType newType = newContent.getNewType();
    if (EnumSectionType.EMHSectionIdem.equals(newType)) {
      Double dz = newContent.getDz();
      if (dz == null) {
        log.addSevereError(BusinessMessages.getString("validation.sectionIdem.NoDz"));
      } else {
        ccm.getProperty(CrueConfigMetierConstants.PROP_DZ).getValidator().validateNumber(null, dz, log);
      }
      Long sectionRefUid = newContent.getSectionRefUid();
      if (sectionRefUid == null) {
        log.addSevereError(BusinessMessages.getString("validation.sectionIdem.NoRef"));
      }
      EMHScenario scenario = EMHHelper.getScenario(section);
      CatEMHSection emhSectionRef = (CatEMHSection) scenario.getIdRegistry().getEmh(sectionRefUid);
      if (emhSectionRef == null) {
        log.addSevereError(BusinessMessages.getString("validation.sectionIdem.RefNotFound", sectionRefUid));
      } else {
        if (emhSectionRef.getUiId().equals(section.getUiId())) {
          log.addSevereError(BusinessMessages.getString("validation.sectionIdem.AutoReference"));
        }
        List<Class<? extends CatEMHSection>> authorizedSectionRef = DelegateValidatorSection.getAuthorizedSectionRef();
        if (!authorizedSectionRef.contains(emhSectionRef.getClass())) {
          log.addSevereError(BusinessMessages.getString("validation.sectionIdem.RefTypeWrong", emhSectionRef.getNom()));
        }
      }
    }
    //changement de section:
    if (newType != null && !newType.equals(section.getSectionType())) {
      SectionPrevalidationContent createPrevalidateContent = createPrevalidateContent(section);
      if (!createPrevalidateContent.getAuthorizedSections().contains(newType)) {
        log.addSevereError(BusinessMessages.getString("validation.section.newTypeIsWrong", newType.geti18n()));
      }
    }

    return log;
  }

  private void changeType(final boolean isCurrentSectionIdem, CatEMHSection initSection, SectionEditionContent newContent, CrueConfigMetier ccm) {
    //on enleve la section de ref:
    if (isCurrentSectionIdem) {
      final EMHSectionIdem sectionIdem = (EMHSectionIdem) initSection;
      CatEMHSection currentSectionRef = EMHHelper.getSectionRef(sectionIdem);
      EMHRelationFactory.removeRelationBidirect(currentSectionRef, initSection);
    }
    //branche pilote
    CatEMHBranche branchePilotedBy = EMHHelper.getBranchePilotedBy(initSection);
    if (branchePilotedBy != null) {
      EMHRelationFactory.removeSectionPilote(branchePilotedBy);
    }
    CatEMHBranche branche = initSection.getBranche();
    List<RelationEMH> sectionsDansBranches = null;
    if (branche != null) {
      //on enlève la relation branche dans section
      EMHRelationFactory.removeRelation(initSection, branche);
      sectionsDansBranches = EMHRelationFactory.findRelationWithTarget(branche, initSection);
    }

    final EMHSousModele sousModele = initSection.getParent();
    List<RelationEMH> sousModeleContientSection = EMHRelationFactory.findRelationWithTarget(sousModele, initSection);
    //pour la création on enlève l'ancienne section
    sousModele.getParent().getParent().getIdRegistry().unregister(initSection);
    //par contre, la relation sous-modele contient initSection est conservée afin de garder l'ordre.
    EMHRelationFactory.removeRelation(initSection, sousModele);
    CatEMHSection newSection = createNewSection(newContent, sousModele, ccm);
    //on reinitialise l'uid:
    newSection.setUiId(initSection.getUiId());
    sousModele.getParent().getParent().getIdRegistry().register(newSection);
    assert sousModeleContientSection.size() == 1;
    //pour garder l'ordre initial:
    sousModeleContientSection.get(0).setEmh(newSection);
    //on initialise la relation inverse:
    newSection.addRelationEMH(new RelationEMHDansSousModele(sousModele));
    //branche
    if (branche != null) {
      for (RelationEMH relationEMH : sectionsDansBranches) {
        relationEMH.setEmh(newSection);
      }
      newSection.addRelationEMH(new RelationEMHBrancheContientSection(branche));
    }
    //branche pilote
    if (branchePilotedBy != null) {
      EMHRelationFactory.addSectionPilote(branchePilotedBy, newSection);
    }

    //on copie les autre InfosEMH
    DonPrtCIniSection cini = EMHHelper.selectFirstOfClass(initSection.getInfosEMH(), DonPrtCIniSection.class);
    if (cini != null) {
      newSection.addInfosEMH(cini.deepClone());
    }
  }

  private CatEMHSection createNewSection(SectionEditionContent newContent, final EMHSousModele sousModele, CrueConfigMetier ccm) {
    CatEMHSection newRef = getSectionRef(sousModele.getParent().getParent(), newContent);
    ListRelationSectionContent content = new ListRelationSectionContent();
    content.setNom(newContent.getNewName());
    ListRelationSectionContent.SectionData data = new ListRelationSectionContent.SectionData();
    data.setDz(newContent.getDz() == null ? 0 : newContent.getDz().doubleValue());
    data.setCommentaire(newContent.getComment());
    data.setSectionType(newContent.getNewType());
    content.setSection(data);
    CatEMHSection newSection = new EditionCreateEMH(defaultValues).createSectionDetached(content, sousModele, ccm, newRef);
    newSection.setCommentaire(newContent.getComment());
    return newSection;
  }
}
