/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;

import java.util.*;

/**
 * @author Frederic Deniger
 */
public class ReportRunVariableHelper {

  public static boolean isNatZVar(ReportVariableKey key, ReportResultProviderServiceContrat service) {
    return ReportVariableTypeEnum.LHPT.equals(key.getVariableType()) || isFromNature(key, service, CrueConfigMetierConstants.NAT_Z);
  }

  public static boolean isNatZVar(String key, ReportResultProviderServiceContrat service) {
    return isFromNature(key, service, CrueConfigMetierConstants.NAT_Z);
  }

  public static boolean isNotNatZVar(ReportVariableKey key, ReportResultProviderServiceContrat service) {
    return !isNatZVar(key, service);
  }

  /**
   * @param variableKeys les variables en entrée
   * @return map variable Name -> list de ReportRun trié avec le courant en premier.
   */
  public static Map<String, List<ReportRunKey>> getByVarName(List<ReportRunVariableKey> variableKeys) {
    Map<String, List<ReportRunKey>> res = new HashMap<>();
    if (variableKeys != null) {
      for (ReportRunVariableKey reportRunVariableKey : variableKeys) {
        String variableName = reportRunVariableKey.getVariable().getVariableName();
        List<ReportRunKey> list = res.get(variableName);
        if (list == null) {
          list = new ArrayList<>();
          res.put(variableName, list);
        }
        list.add(reportRunVariableKey.getRunKey());
      }
      ReportRunVariableKeyComparator comparator = new ReportRunVariableKeyComparator();
      for (List<ReportRunKey> list : res.values()) {
        Collections.sort(list, comparator);
      }
    }
    return res;
  }

  public static Map<String, List<ReportRunKey>> getByEmh(Collection<ReportRunEmhKey> keys) {
    Map<String, List<ReportRunKey>> res = new HashMap<>();
    if (keys != null) {
      for (ReportRunEmhKey key : keys) {
        String emhName = key.getEmhName();
        List<ReportRunKey> list = res.get(emhName);
        if (list == null) {
          list = new ArrayList<>();
          res.put(emhName, list);
        }
        list.add(key.getReportRunKey());
      }
      ReportRunVariableKeyComparator comparator = new ReportRunVariableKeyComparator();
      for (List<ReportRunKey> list : res.values()) {
        Collections.sort(list, comparator);
      }
    }
    return res;
  }

  private static boolean isFromNature(ReportVariableKey key, ReportResultProviderServiceContrat service, final String natureToFind) {
    return isFromNature(key.getVariableName(), service, natureToFind);
  }

  private static boolean isFromNature(String key, ReportResultProviderServiceContrat service, final String natureToFind) {
    PropertyNature nature = service.getPropertyNature(key);
    return nature != null && natureToFind.equals(nature.getNom());
  }
}
