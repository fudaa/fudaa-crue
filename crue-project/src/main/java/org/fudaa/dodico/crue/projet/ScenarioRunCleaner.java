/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.FileDeleteResult;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

/**
 *
 * @author Frederic Deniger
 */
public class ScenarioRunCleaner {

  private final EMHProjet projet;

  public ScenarioRunCleaner(EMHProjet projet) {
    this.projet = projet;
  }

  public FileDeleteResult deleteUnusedDir() {
    List<File> dirToDelete = getDirToDelete();
    FileDeleteResult result = new FileDeleteResult();
    for (File file : dirToDelete) {
      CtuluLibFile.deleteDir(file, result);
    }
    return result;
  }

  public List<File> getDirToDelete() {
    List<File> toDelete = new ArrayList<>();
    Set<String> expectedScenarioDir = new HashSet<>();
    Set<String> expectedRunDir = new HashSet<>();
    try {
      fillWithExpectedDir(expectedScenarioDir, expectedRunDir);
    } catch (IOException ex) {
      Logger.getLogger(ScenarioRunCleaner.class.getName()).log(Level.SEVERE, null, ex);
      return toDelete;
    }
    File dirOfRuns = projet.getInfos().getDirOfRuns();
    if (dirOfRuns == null) {
      return toDelete;
    }
    File[] listFilesScenarios = dirOfRuns.listFiles();
    if (listFilesScenarios == null) {
      return toDelete;
    }
    for (File scenarioFile : listFilesScenarios) {
      if (scenarioFile.isDirectory()) {
        if (expectedScenarioDir.contains(scenarioFile.getAbsolutePath())) {
          File[] listFilesRuns = scenarioFile.listFiles();
          for (File dirRun : listFilesRuns) {
            if (dirRun.isDirectory()) {
              if (!expectedRunDir.contains(dirRun.getAbsolutePath())) {
                toDelete.add(dirRun);
              }
            }
          }
        } else {
          toDelete.add(scenarioFile);
        }

      }
    }

    return toDelete;

  }

  private void fillWithExpectedDir(Set<String> expectedScenarioDir, Set<String> expectedRunDir) throws IOException {
    List<ManagerEMHScenario> listeScenarios = projet.getListeScenarios();
    for (ManagerEMHScenario managerEMHScenario : listeScenarios) {
      if (managerEMHScenario.hasRun()) {
        expectedScenarioDir.add(projet.getMainDirOfRuns(managerEMHScenario).getCanonicalPath());
        List<EMHRun> listeRuns = managerEMHScenario.getListeRuns();
        for (EMHRun eMHRun : listeRuns) {
          expectedRunDir.add(projet.getDirForRun(managerEMHScenario, eMHRun).getCanonicalPath());

        }
      }
    }
  }

}
