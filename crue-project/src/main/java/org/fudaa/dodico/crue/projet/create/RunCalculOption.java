/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.create;

import org.fudaa.dodico.crue.common.BusinessMessages;

/**
 *
 * Permet de spécifier pour chaque calcul, s'il doit être ignoré, executé de force ou executé que si des changements son détectés
 * @author deniger
 */
public enum RunCalculOption {

  /**
   * Ne pas executer le caclul
   */
  DONT_EXECUTE,
  /**
   * Executer meme si pas de changement de détecté
   */
  FORCE_EXCUTE,
  /**
   * Executer qui si
   */
  EXECUTE_IF_NEEDED;

  /**
   *
   * @return traduction de l'option.
   */
  public String geti18n() {
    return BusinessMessages.getString(name().toLowerCase() + ".name");
  }
}
