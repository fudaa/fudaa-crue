/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.transformer;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;

/**
 *
 * @author Frederic Deniger
 */
public class ResultatTimeKeySimpleToStringTransformer extends AbstractPropertyToStringTransformer<ResultatTimeKey> {

  public static final String DAY_YES = "DAY";
  private static final String DAY_NO = "NO_DAY";

  public ResultatTimeKeySimpleToStringTransformer() {
    super(ResultatTimeKey.class);
  }

  private static String getDay(ResultatTimeKey key) {
    return key.isUseDay() ? DAY_YES : DAY_NO;
  }

  @Override
  public String toStringSafe(ResultatTimeKey in) {
    if (in.isPermanent()) {
      return in.getNomCalcul();
    }
    return in.getNomCalcul() + KeysToStringConverter.MINOR_SEPARATOR + in.getDuree() + KeysToStringConverter.MINOR_SEPARATOR + getDay(in);
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }

  @Override
  public ResultatTimeKey fromStringSafe(String in) {
    String[] split = StringUtils.split(in, KeysToStringConverter.MINOR_SEPARATOR);
    if (split.length == 0) {
      return null;
    }
    String nomCalcul = split[0];
    if (StringUtils.isBlank(nomCalcul)) {
      return null;
    }
    if (split.length == 1) {
      return new ResultatTimeKey(nomCalcul);
    }
    if (split.length >= 2) {
      boolean day = false;
      if (split.length == 3) {
        day = DAY_YES.equals(split[2]);
      }
      try {
        Long duree = Long.valueOf(split[1]);
        return new ResultatTimeKey(nomCalcul, duree, day);
      } catch (NumberFormatException numberFormatException) {
      }
    }
    return ResultatTimeKey.NULL_VALUE;
  }
}
