package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Extrait les emhs à ajouter/mettre à jour à partir des options de l'utilisateur
 */
public class SmUpdateOptionsDataExtractor {
  private final EMHSousModele sourceSousModele;

  public SmUpdateOptionsDataExtractor(EMHSousModele sourceSousModele) {
    this.sourceSousModele = sourceSousModele;
  }

  public SmUpdateOptionsData extract(SmUpdateOptions options) {
    SmUpdateOptionsData result = new SmUpdateOptionsData(!options.isAllSections());
    if (options.isAllSections()) {
      sourceSousModele.getSections().forEach(result::addSectionEMH);
    }
    if (options.isCasiers()) {
      sourceSousModele.getCasiers().forEach(result::addCasier);
    }
    if (options.isBranches()) {
      sourceSousModele.getBranches().forEach(result::addBranche);
    }
    return result;
  }

  public static class SmUpdateOptionsData {
    private Map<String, EMH> requiredEMH = new HashMap<>();
    private Map<String, EMH> emhToAddOrUpdate = new HashMap<>();
    private final boolean sectionRequired;

    public SmUpdateOptionsData(boolean sectionRequired) {
      this.sectionRequired = sectionRequired;
    }

    private void addRequiredEMH(EMH emh) {
      requiredEMH.put(emh.getNom(), emh);
    }

    public void addCasier(CatEMHCasier emh) {
      addEMH(emh);
      addEMH(emh.getNoeud());
    }

    public void addBranche(CatEMHBranche emh) {
      addEMH(emh);
      addEMH(emh.getNoeudAmont());
      addEMH(emh.getNoeudAval());
      emh.getSections().forEach(relation -> {
        addSectionEMH(relation.getEmh());
        if (sectionRequired) {
          addRequiredEMH(relation.getEmh());
        }
      });
    }

    public void addSectionEMH(CatEMHSection emh) {
      addEMH(emh);
    }

    private void addEMH(EMH emh) {
      emhToAddOrUpdate.put(emh.getNom(), emh);
    }

    public List<EMH> getEmhToAddOrUpdate() {
      return new ArrayList<>(emhToAddOrUpdate.values());
    }

    public List<EMH> getRequiredEmh() {
      return new ArrayList<>(requiredEMH.values());
    }
  }
}
