/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.persist;

import com.thoughtworks.xstream.XStream;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.conf.Option;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportXstreamReaderWriter;
import org.fudaa.dodico.crue.projet.report.data.ExternFileKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunEmhKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableEmhKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVerticalTimeKey;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.dodico.crue.projet.report.loi.ViewCourbeConfig;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBannerConfig;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBrancheConfig;
import org.fudaa.dodico.crue.projet.report.transformer.KeysToStringConverter;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.fudaa.ebli.courbe.convert.EGAxePersistConverter;
import org.fudaa.ebli.courbe.convert.EGCourbePersistConverter;
import org.fudaa.ebli.courbe.convert.EGPersistHelper;
import org.fudaa.ebli.palette.BPalettePlageProperties;
import org.fudaa.ebli.palette.BPlage;

/**
 *
 * @author Frederic Deniger
 */
public class ReportCourbeConfigReader implements CrueDataDaoStructure {

  private final TraceToStringConverter traceToStringConverter;
  private final KeysToStringConverter keysToStringConverter;

  public ReportCourbeConfigReader(TraceToStringConverter traceToStringConverter, KeysToStringConverter keysToStringConverter) {
    this.traceToStringConverter = traceToStringConverter;
    this.keysToStringConverter = keysToStringConverter;
  }

  public CrueIOResu<ReportConfigContrat> read(File targetFile) {
    ReportXstreamReaderWriter<AbstractReportConfigDao> readerWriter = new ReportXstreamReaderWriter<>("report-vue",
            ReportXstreamReaderWriter.VERSION, this);
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    AbstractReportConfigDao readDao = null;
    try {
      readDao = readerWriter.readDao(targetFile, log, null);
    } catch (Exception ex) {
      Logger.getLogger(ReportCourbeConfigReader.class.getName()).log(Level.SEVERE, "message {0}", ex);
    }
    CrueIOResu<ReportConfigContrat> resuFinal = new CrueIOResu<>();
    resuFinal.setAnalyse(log);
    final ReportConfigContrat readConfig = readDao == null ? null : readDao.getConfig();
    if (readConfig != null) {
      readConfig.reinitContent();
    }
    resuFinal.setMetier(readConfig);
    return resuFinal;
  }

  @Override
  public void configureXStream(XStream xstream, CtuluLog ctuluLog, org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    xstream.processAnnotations(AbstractReportCourbeConfig.class);
    xstream.processAnnotations(AbstractReportConfigDao.class);

    xstream.processAnnotations(ReportTransversalConfig.class);

    EGCourbePersistConverter courbConverter = new EGCourbePersistConverter(traceToStringConverter);
    courbConverter.initXstream(xstream);
    EGAxePersistConverter axeConverter = new EGAxePersistConverter(traceToStringConverter);
    axeConverter.initXstream(xstream);

    xstream.processAnnotations(ReportConfigTransversalDao.class);
    xstream.processAnnotations(ReportConfigLongitudinalDao.class);
    xstream.processAnnotations(ReportConfigMultiVarDao.class);
    xstream.processAnnotations(ReportConfigRPTGDao.class);
    xstream.processAnnotations(ReportConfigTemporalDao.class);

    xstream.processAnnotations(ExternFileKey.class);
    xstream.processAnnotations(ReportRunVariableKey.class);
    xstream.processAnnotations(ReportRunKey.class);
    xstream.processAnnotations(ResultatTimeKey.class);
    xstream.processAnnotations(ReportLabelContent.class);
    xstream.processAnnotations(ReportRunEmhKey.class);
    xstream.processAnnotations(ReportRunVariableEmhKey.class);
    xstream.processAnnotations(ReportRunVariableTimeKey.class);
    xstream.processAnnotations(ReportVerticalTimeKey.class);
    xstream.processAnnotations(ReportLongitudinalBrancheConfig.class);
    xstream.processAnnotations(ReportLongitudinalBannerConfig.class);

    xstream.processAnnotations(ViewCourbeConfig.class);
    xstream.processAnnotations(LabelConfig.class);

    xstream.processAnnotations(BPalettePlageProperties.class);
    xstream.processAnnotations(BPlage.class);
    xstream.processAnnotations(Option.class);
    xstream.processAnnotations(EbliUIProperties.class);

    EGPersistHelper.registerDefaultConverters(xstream, traceToStringConverter);
    keysToStringConverter.registerDefaultConverters(xstream);
    configureXStreamPlanimetry(xstream);
  }

  protected void configureXStreamPlanimetry(XStream xstream) {
  }
}
