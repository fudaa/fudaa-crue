package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateValidationData;

import java.util.List;
import java.util.Map;

/**
 * Construit des logs a partir des résultats de validation d'opération de mise à jour d'un sous-modele
 */
public class SmUpdateValidatorLogCreator {
  public CtuluLogGroup extractLog(SmUpdateValidationData data) {
    CtuluLogGroup res = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    res.setDescription("sm.updater.validatorLogTitle");
    res.setDescriptionArgs(data.getTargetSousModele());
    if (data.isTargetSousModeleNotExisting()) {
      final CtuluLog logTargetSousModeleNotExisting = res.createNewLog("sm.updater.targetSousModeleNotExistingLog");
      logTargetSousModeleNotExisting.addSevereError("sm.updater.targetSousModeleNotExistingLogItem", data.getTargetSousModele());
    }

    final Map<String, String> targetEmhInAnotherSousModele = data.getTargetEmhInAnotherSousModele();
    if (!targetEmhInAnotherSousModele.isEmpty()) {
      final CtuluLog logEMHInAnotherSousModele = res.createNewLog("sm.updater.emhInAnotherSousModeleLog");
      targetEmhInAnotherSousModele.forEach((key, value) -> logEMHInAnotherSousModele.addSevereError("sm.updater.emhInAnotherSousModeleLogItem", key, value));
    }
    final Map<String, String> targetDonFrtInAnotherSousModele = data.getTargetDonFrtInAnotherSousModele();
    if (!targetDonFrtInAnotherSousModele.isEmpty()) {
      final CtuluLog logDonFrtInAnotherSousModele = res.createNewLog("sm.updater.donFrtInAnotherSousModeleLog");
      targetDonFrtInAnotherSousModele.forEach((key, value) -> logDonFrtInAnotherSousModele.addSevereError("sm.updater.donFrtInAnotherSousModeleLogItem", key, value));
    }
    final List<String> requiredSectionsNotInTarget = data.getRequiredSectionsNotInTarget();
    if (!requiredSectionsNotInTarget.isEmpty()) {
      final CtuluLog logRequiredSectionNotInTarget = res.createNewLog("sm.updater.requiredSectionsNotInTargetLog");
      requiredSectionsNotInTarget.forEach(emhName -> logRequiredSectionNotInTarget.addSevereError("sm.updater.requiredSectionsNotInTargetLogItem", emhName));
    }
    return res;
  }
}
