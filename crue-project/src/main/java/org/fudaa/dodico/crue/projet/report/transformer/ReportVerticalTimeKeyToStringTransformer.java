/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.transformer;

import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.dodico.crue.projet.report.data.ReportVerticalTimeKey;

/**
 *
 * @author Frederic Deniger
 */
public class ReportVerticalTimeKeyToStringTransformer extends AbstractPropertyToStringTransformer<ReportVerticalTimeKey> {

  public static final String ID = "_TIME_";

  public ReportVerticalTimeKeyToStringTransformer() {
    super(ReportVerticalTimeKey.class);
  }

  @Override
  public String toStringSafe(ReportVerticalTimeKey in) {
    return ID;
  }

  @Override
  public boolean isValidSafe(String in) {
    return ID.equals(in);
  }

  @Override
  public ReportVerticalTimeKey fromStringSafe(String in) {
    return ReportVerticalTimeKey.INSTANCE;
  }
}
