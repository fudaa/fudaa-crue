/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.calcul;

import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

/**
 *
 * @author Frederic Deniger
 */
public class RunScenarioPair {

  private final ManagerEMHScenario scenario;
  private final EMHRun run;

  public RunScenarioPair(ManagerEMHScenario scenario, EMHRun run) {
    this.scenario = scenario;
    this.run = run;
  }

  public ManagerEMHScenario getScenario() {
    return scenario;
  }

  public EMHRun getRun() {
    return run;
  }

}
