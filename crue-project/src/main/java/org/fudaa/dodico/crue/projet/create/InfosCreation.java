/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.create;

import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.metier.etude.EMHInfosVersion;

/**
 * Containeur des informations minimales pour la création d'Infos.
 *
 * @author deniger
 *
 */
public class InfosCreation {

  private final ConnexionInformation connexionInformation;
  private String commentaire;
  private CrueVersionType crueVersion;

  public InfosCreation(final ConnexionInformation connexionInformation) {
    this.connexionInformation = connexionInformation;
    assert connexionInformation != null;

  }

  /**
   * @return the commentaire
   */
  public String getCommentaire() {
    return commentaire;
  }

  /**
   * @param commentaire the commentaire to set
   */
  public void setCommentaire(final String commentaire) {
    this.commentaire = commentaire;
  }

  public CrueVersionType getCrueVersion() {
    return crueVersion;
  }

  public void setCrueVersion(final CrueVersionType crueVersion) {
    this.crueVersion = crueVersion;
  }

  public EMHInfosVersion createInfosVersion() {
    final EMHInfosVersion version = new EMHInfosVersion();
    final String currentUser = connexionInformation.getCurrentUser();
    version.setAuteurCreation(currentUser);
    version.setAuteurDerniereModif(currentUser);
    version.setCommentaire(commentaire);
    version.setDateCreation(connexionInformation.getCurrentDate());
    version.setDateDerniereModif(connexionInformation.getCurrentDate());
    version.setVersion(crueVersion);

    return version;
  }
}
