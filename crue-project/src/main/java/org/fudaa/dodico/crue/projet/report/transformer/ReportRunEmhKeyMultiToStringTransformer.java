/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.transformer;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.dodico.crue.projet.report.data.ReportRunEmhKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;

/**
 *
 * @author Frederic Deniger
 */
public class ReportRunEmhKeyMultiToStringTransformer extends AbstractPropertyToStringTransformer<ReportRunEmhKey> {

  private final AbstractPropertyToStringTransformer<ReportRunKey> reportRunKeyToStringTransformer;

  public ReportRunEmhKeyMultiToStringTransformer(AbstractPropertyToStringTransformer<ReportRunKey> reportRunKeyToStringTransformer) {
    super(ReportRunEmhKey.class);
    this.reportRunKeyToStringTransformer = reportRunKeyToStringTransformer;
  }

  @Override
  public String toStringSafe(ReportRunEmhKey in) {
    return reportRunKeyToStringTransformer.toString(in.getReportRunKey()) + KeysToStringConverter.MAIN_SEPARATOR + in.getEmhName();
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }

  @Override
  public ReportRunEmhKey fromStringSafe(String in) {
    String[] split = StringUtils.split(in, KeysToStringConverter.MAIN_SEPARATOR);
    if (split.length == 2) {
      ReportRunKey reportRunKey = reportRunKeyToStringTransformer.fromString(split[0]);
      if (reportRunKey != null) {
        return new ReportRunEmhKey(reportRunKey, split[1]);
      }
    }
    return ReportRunEmhKey.NULL_VALUE;
  }
}
