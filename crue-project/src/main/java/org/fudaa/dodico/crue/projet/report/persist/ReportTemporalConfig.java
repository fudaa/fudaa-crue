/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.persist;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.data.ReportKeyContract;
import org.fudaa.dodico.crue.projet.report.data.ReportKeyContratCleaner;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableEmhKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;
import org.fudaa.dodico.crue.projet.report.loi.ViewCourbeConfig;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * @author Frederic Deniger
 */
public class ReportTemporalConfig extends AbstractReportCourbeConfig {

  /**
   * listes des emhs à afficher.
   */
  @XStreamImplicit(itemFieldName = "Emh")
  List<String> emhs = new ArrayList<>();

  /**
   * listes des emhs à afficher.
   */
  @XStreamImplicit(itemFieldName = "lhptLois")
  List<String> lhptLois = new ArrayList<>();

  /**
   * listes des variables à afficher
   */
  @XStreamImplicit(itemFieldName = "Variable")
  List<String> variables = new ArrayList<>();
  /**
   * la listes des runs à afficher sur le graphe
   */
  @XStreamImplicit(itemFieldName = "Run-Variable")
  List<ReportRunVariableKey> temporalVariables = new ArrayList<>();
  /**
   * la config pour les courbes:
   */
  @XStreamAlias("Temps-Config")
  EGCourbePersist timeConfig;
  @XStreamAlias("Courbe-Configs")
  Map<ReportRunVariableEmhKey, EGCourbePersist> courbeconfigs = new HashMap<>();
  @XStreamAlias("Vue-Config")
  ViewCourbeConfig loiLegendConfig = new ViewCourbeConfig();

  public ReportTemporalConfig() {
    createTimeConfig();
  }

  public ReportTemporalConfig(ReportTemporalConfig from) {
    super(from);
    if (from == null) {
      return;
    }
    if (from.emhs != null) {
      emhs.addAll(from.emhs);
    }
    if (from.lhptLois != null) {
      lhptLois.addAll(from.lhptLois);
    }
    if (from.variables != null) {
      variables.addAll(from.variables);
    }
    if (from.temporalVariables != null) {
      temporalVariables.addAll(from.temporalVariables);
    }
    if (from.timeConfig != null) {
      timeConfig = from.timeConfig.duplicate();
    }
    if (from.courbeconfigs != null) {
      from.courbeconfigs.forEach((key, value) -> courbeconfigs.put(key, value.duplicate()));
    }
    if (from.loiLegendConfig != null) {
      loiLegendConfig = from.loiLegendConfig.dpulicate();
    }
  }

  @Override
  protected void cleanUnusedCourbesConfig(Set<ReportKeyContract> usedKeys) {
    for (Iterator<ReportRunVariableEmhKey> it = courbeconfigs.keySet().iterator(); it.hasNext(); ) {
      if (!usedKeys.contains(it.next())) {
        it.remove();
      }
    }
  }

  @Override
  public ReportTemporalConfig duplicate() {
    return new ReportTemporalConfig(this);
  }

  public List<String> getLhptLois() {
    return lhptLois;
  }

  public void setLhptLois(List<String> lhptLois) {
    this.lhptLois = lhptLois;
  }

  public void setEmhs(List<String> emhs) {
    this.emhs = emhs;
  }

  public Map<ReportRunVariableEmhKey, EGCourbePersist> getCourbeconfigs() {
    return courbeconfigs;
  }

  @Override
  public ReportConfigContrat createTemplateConfig() {
    final ReportTemporalConfig duplicate = (ReportTemporalConfig) duplicate();
    duplicate.clearExternFiles();
    duplicate.loiLegendConfig.clearLabelTxtTooltip();
    duplicate.emhs.clear();
    duplicate.courbeconfigs.clear();
    ReportKeyContratCleaner.cleanNotCurrent(duplicate.temporalVariables);
    duplicate.clearZooms();
    return duplicate;
  }

  @Override
  public boolean variablesUpdated(FormuleDataChanges changes) {
    boolean modified = super.axeVariablesUpdated(changes);
    modified |= VariableUpdateHelper.updateViewCourbeConfig(loiLegendConfig, changes);
    Pair<List<String>, Boolean> resVariable = VariableUpdateHelper.variablesUpdated(variables, changes);
    if (Boolean.TRUE.equals(resVariable.second)) {
      variables = resVariable.first;
      modified = true;
    }
    Pair<List<ReportRunVariableKey>, Boolean> resRunVariable = VariableUpdateHelper.variablesUpdatedVariable(temporalVariables, changes);
    if (Boolean.TRUE.equals(resRunVariable.second)) {
      temporalVariables = resRunVariable.first;
      modified = true;
    }
    modified |= VariableUpdateHelper.updateCourbeConfig(courbeconfigs, changes);
    return modified;
  }

  @Override
  public void reinitContent() {
    super.reinitContent();
    if (emhs == null) {
      emhs = new ArrayList<>();
    }
    if (variables == null) {
      variables = new ArrayList<>();
    }
    if (temporalVariables == null) {
      temporalVariables = new ArrayList<>();
    }
    if (courbeconfigs == null) {
      courbeconfigs = new HashMap<>();
    }
    if (loiLegendConfig == null) {
      loiLegendConfig = new ViewCourbeConfig();
    }
    loiLegendConfig.reinit();
    if (timeConfig == null) {
      createTimeConfig();
    }
    emhs.remove(null);
    emhs.remove(StringUtils.EMPTY);
    variables.remove(null);
    variables.remove(StringUtils.EMPTY);
    temporalVariables.remove(ReportRunVariableKey.NULL_VALUE);
    courbeconfigs.remove(ReportRunVariableEmhKey.NULL);
  }


  @Override
  public ViewCourbeConfig getLoiLegendConfig() {
    return loiLegendConfig;
  }

  public void addEMH(String emh) {
    emhs.add(emh);
  }

  public List<String> getVariables() {
    return variables;
  }

  public List<ReportRunVariableKey> getTemporalVariables() {
    return temporalVariables;
  }

  public EGCourbePersist getTimeConfig() {
    return timeConfig;
  }

  public List<String> getEmhs() {
    return emhs;
  }

  public LinkedHashMap<String, Set<ReportRunKey>> getVariablesMap() {
    LinkedHashMap<String, Set<ReportRunKey>> runByVar = new LinkedHashMap<>();
    for (String string : variables) {
      runByVar.put(string, new HashSet<>());
    }
    for (ReportRunVariableKey reportRunVariableKey : temporalVariables) {
      final String var = reportRunVariableKey.getVariable().getVariableName();
      Set<ReportRunKey> set = runByVar.get(var);
      if (set != null) {
        set.add(reportRunVariableKey.getRunKey());
      }
    }
    return runByVar;
  }

  public void addReportRunVariableKey(ReportRunVariableKey key) {
    temporalVariables.add(key);
  }

  @Override
  public ReportContentType getType() {
    return ReportContentType.TEMPORAL;
  }

  @Override
  public String getMainEMHName() {
    return null;
  }

  public void addProfilVariable(ReportRunVariableKey... keys) {
    temporalVariables.addAll(Arrays.asList(keys));
  }

  private void createTimeConfig() {
    timeConfig = new EGCourbePersist();
    timeConfig.setDisplayLabels(true);
    timeConfig.setLineModel(new TraceLigneModel(TraceLigne.MIXTE, 2f, Color.MAGENTA));
  }

  public void setTimeConfig(EGCourbePersist timeConfig) {
    this.timeConfig = timeConfig;
  }

}
