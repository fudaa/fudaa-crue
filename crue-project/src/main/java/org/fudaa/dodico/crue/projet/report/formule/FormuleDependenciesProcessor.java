/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.fudaa.dodico.crue.projet.report.FormuleContentManager;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleDependenciesProcessor {

  private final FormuleContentManager manager;

  public FormuleDependenciesProcessor(FormuleContentManager manager) {
    this.manager = manager;
  }

  public FormuleDependenciesContent compute(Collection<String> formules) {
    FormuleDependenciesContent res = new FormuleDependenciesContent();
    for (String formule : formules) {
      computeDependencies(res, formule);
    }
    return res;
  }

  private void computeDependencies(FormuleDependenciesContent res, String var) {
    if (!res.isSet(var)) {
      Set<String> dependantVar = new HashSet<>();
      computeDependencies(var, res, var, dependantVar);
      res.setUsedBy(var, dependantVar);


    }
  }

  private void computeDependencies(String initVar, FormuleDependenciesContent res, String currentVar, Set<String> processed) {
    Set<String> dependantVar = manager.getDirectUsedVariable(currentVar);
    for (String var : dependantVar) {
      //on continue si la variable dépendante n'est pas celle d'origine ou si non déjà traitée
      boolean goOn = !var.equals(initVar) && !processed.contains(var);
      processed.add(var);
      if (goOn) {
        if (res.isSet(var)) {
          processed.addAll(res.getUsedVariables(var));
        } else {
          computeDependencies(initVar, res, var, processed);
        }
      }
    }
  }
}
