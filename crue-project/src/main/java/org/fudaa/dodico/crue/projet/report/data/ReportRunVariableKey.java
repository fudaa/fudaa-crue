/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("ReportRunVariableKey")
public class ReportRunVariableKey implements ReportKeyContract {

  public static final ReportRunVariableKey NULL_VALUE = new ReportRunVariableKey();
  final ReportRunKey runKey;
  final ReportVariableKey variable;

  private ReportRunVariableKey() {
    runKey = ReportRunKey.NULL_VALUE;
    variable = ReportVariableKey.NULL;
  }

  public ReportRunVariableKey(ReportRunKey runKey, ReportVariableTypeEnum type, String variable) {
    this(runKey, new ReportVariableKey(type, variable));
  }

  public ReportRunVariableKey(ReportRunKey runKey, ReportVariableKey variable) {
    this.runKey = runKey;
    this.variable = variable;
    assert runKey != null;
    assert variable != null;
  }

  public String getDisplayName() {
    if (this == NULL_VALUE) {
      return StringUtils.EMPTY;
    }
    return variable.getVariableDisplayName() + " -> " + runKey.getDisplayName();
  }

  @Override
  public String getVariableName() {
    if (this == NULL_VALUE) {
      return StringUtils.EMPTY;
    }
    return variable.getVariableName();
  }

  @Override
  public ReportKeyContract duplicateWithNewVar(String newName) {
    return new ReportRunVariableKey(runKey, variable.duplicateWith(newName));
  }

  @Override
  public ReportRunKey getReportRunKey() {
    return runKey;
  }

  @Override
  public boolean isExtern() {
    return false;
  }

  public ReportRunVariableKey duplicateWith(String newVarName) {
    return new ReportRunVariableKey(runKey, variable.duplicateWith(newVarName));
  }

  public boolean isReadVariable() {
    return ReportVariableTypeEnum.READ.equals(variable.getVariableType());
  }
  public boolean isRPTIVariable() {
    return ReportVariableTypeEnum.RESULTAT_RPTI.equals(variable.getVariableType());
  }
  public boolean isLhpt() {
    return ReportVariableTypeEnum.LHPT.equals(variable.getVariableType());
  }

  public boolean isReadOrVariable() {
    return isReadVariable() || ReportVariableTypeEnum.FORMULE.equals(variable.getVariableType());
  }

  public ReportRunKey getRunKey() {
    return runKey;
  }

  public ReportVariableKey getVariable() {
    return variable;
  }

  @Override
  public int hashCode() {
    return 5 + runKey.hashCode() * 7 + variable.hashCode() * 13;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ReportRunVariableKey other = (ReportRunVariableKey) obj;
    if (this.runKey != other.runKey && (this.runKey == null || !this.runKey.equals(other.runKey))) {
      return false;
    }
    if ((this.variable == null) ? (other.variable != null) : !this.variable.equals(other.variable)) {
      return false;
    }
    return true;
  }
}
