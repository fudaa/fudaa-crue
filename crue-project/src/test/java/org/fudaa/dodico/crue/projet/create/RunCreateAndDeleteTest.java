/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.create;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformationDefault;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatETU;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.etude.RunFile;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author CANEL Christophe
 */
@SuppressWarnings("ALL")
public class RunCreateAndDeleteTest extends AbstractTestParent {
    /**
     * Le dossier contenant des résultats crue10.
     */
    private static final String CRUE10_RESDIR = "crue10Res";

    private void create2Runs(final EMHProjet projet, final ManagerEMHScenario scenario) {
        final RunCreator creator = new RunCreator(projet, ConnexionInformationDefault.getDefault());
        CrueOperationResult<EMHRun> create = creator.create(scenario);
        Assert.assertFalse(create.getLogs().containsSomething());
        Assert.assertFalse(scenario.getListeRuns().isEmpty());
        scenario.addRunToScenario(create.getResult());
        create = creator.create(scenario);
        Assert.assertFalse(create.getLogs().containsSomething());
        Assert.assertFalse(scenario.getListeRuns().isEmpty());
        scenario.addRunToScenario(create.getResult());
    }

    @Test
    public void testRunCreator() {
        final Calendar instance = Calendar.getInstance();
        instance.set(Calendar.MILLISECOND, 0);//no milliseconds
        final Date startTest = instance.getTime();
        final File exportZipInTempDir = exportZipInTempDir("/projet/Etu3-0_1.1.2.zip");
        final File etuFile = getEtuFileUpdated("etu3-0", exportZipInTempDir);
        Assert.assertNotNull(etuFile);
        Assert.assertTrue(etuFile.exists());

        EMHProjet projet = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE_1_2);
        Assert.assertNotNull(projet);

        this.testIsRunUpToDate(projet);
        final EMHRun createdRun = this.testCreate(projet, startTest);
        final EMHProjetController controller = new EMHProjetController(projet, ConnexionInformationDefault.getDefault());
        final CtuluLogGroup group = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
        //on teste la sauvegarde
        controller.saveProjet(etuFile, group);
        saveAndValidFile(etuFile);

        //on teste la suppression
        ManagerEMHScenario scenario = projet.getScenario("Sc_M3-0_c10");
        scenario.addRunToScenario(createdRun);//le test n'ajoute pas le run au scenario.
        final EMHRun runToDelete = scenario.getRunFromScenar(createdRun.getId());
        Assert.assertNotNull(runToDelete);
        final File dirForRun = projet.getDirForRun(scenario, runToDelete);
        Assert.assertTrue(dirForRun.isDirectory());
        final boolean deleteRun = controller.deleteRun(scenario, runToDelete, null);
        Assert.assertTrue(deleteRun);
        Assert.assertFalse(dirForRun.isDirectory());
        saveAndValidFile(etuFile);
        projet = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE);
        Assert.assertFalse(dirForRun.isDirectory());
        scenario = projet.getScenario("Sc_M3_c9");
        Assert.assertNull(scenario.getRunFromScenar(runToDelete.getId()));
    }

    @Test
    public void testRunDelete() {
        final Calendar instance = Calendar.getInstance();
        instance.set(Calendar.MILLISECOND, 0);//no milliseconds
        final File exportZipInTempDir = exportZipInTempDir("/projet/Etu3-0_1.1.2.zip");
        final File etuFile = getEtuFileUpdated("etu3-0", exportZipInTempDir);
        Assert.assertNotNull(etuFile);
        Assert.assertTrue(etuFile.exists());

        EMHProjet projet = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE_1_2);
        Assert.assertNotNull(projet);
        ManagerEMHScenario scenario = projet.getScenario("Sc_M3-0_c10");
        final RunCreator creator = new RunCreator(projet, ConnexionInformationDefault.getDefault());
        final CrueOperationResult<EMHRun> create = creator.create(scenario);
        final EMHRun createdRun = create.getResult();
        final CtuluLog log = create.getLogs().getLastLog();
        Assert.assertFalse(log.containsSevereError());

        final EMHProjetController controller = new EMHProjetController(projet, ConnexionInformationDefault.getDefault());
        //on teste la suppression
        scenario.addRunToScenario(createdRun);//le test n'ajoute pas le run au scenario.
        final EMHRun runToDelete = scenario.getRunFromScenar(createdRun.getId());
        Assert.assertNotNull(runToDelete);
        final File dirForRun = projet.getDirForRun(scenario, runToDelete);
        Assert.assertTrue(dirForRun.isDirectory());
        boolean deleteRun = controller.deleteRun(scenario, runToDelete, null);
        Assert.assertTrue(deleteRun);
        final List<EMHRun> listeRuns = new ArrayList<>(scenario.getListeRuns());
        for (final EMHRun run : listeRuns) {
            deleteRun = controller.deleteRun(scenario, run, null);
            Assert.assertTrue(deleteRun);
        }
        Assert.assertTrue(scenario.getListeRuns().isEmpty());
        final boolean isClean = controller.cleanRunDir(scenario);
        Assert.assertTrue(isClean);
        Assert.assertFalse(dirForRun.isDirectory());
        Assert.assertFalse(projet.getMainDirOfRuns(scenario).exists());
        saveAndValidFile(etuFile);
        projet = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE_1_2);
        scenario = projet.getScenario("Sc_M3-0_c10");
        Assert.assertNull(scenario.getRunFromScenar(runToDelete.getId()));
    }

    @Test
    public void testDeleteRuns() {
        final Calendar instance = Calendar.getInstance();
        instance.set(Calendar.MILLISECOND, 0);//no milliseconds
        final File exportZipInTempDir = exportZipInTempDir("/projet/Etu3-0_1.1.2.zip");
        final File etuFile = getEtuFileUpdated("etu3-0", exportZipInTempDir);
        Assert.assertNotNull(etuFile);
        Assert.assertTrue(etuFile.exists());

        final EMHProjet projet = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE);
        Assert.assertNotNull(projet);
        final ManagerEMHScenario scenario = projet.getScenario("Sc_M3-0_c10");
        create2Runs(projet, scenario);

        final EMHProjetController controller = new EMHProjetController(projet, ConnexionInformationDefault.getDefault());
        //on teste la suppression
        Assert.assertTrue(controller.deleteAllRuns(scenario, null));
        Assert.assertFalse(projet.getMainDirOfRuns(scenario).exists());
    }

    /**
     * Le dossier des runs contient un fichier autre.
     */
    @Test
    public void testDeleteRunsButWithAFile() {
        final Calendar instance = Calendar.getInstance();
        instance.set(Calendar.MILLISECOND, 0);//no milliseconds
        final File exportZipInTempDir = exportZipInTempDir("/projet/Etu3-0_1.1.2.zip");
        final File etuFile = getEtuFileUpdated("etu3-0", exportZipInTempDir);
        Assert.assertNotNull(etuFile);
        Assert.assertTrue(etuFile.exists());

        final EMHProjet projet = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE_1_2);
        Assert.assertNotNull(projet);
        final ManagerEMHScenario scenario = projet.getScenario("Sc_M3-0_c10");
        create2Runs(projet, scenario);
        Assert.assertFalse(scenario.getListeRuns().isEmpty());

        final EMHProjetController controller = new EMHProjetController(projet, ConnexionInformationDefault.getDefault());
        //on teste la suppression
        final File mainDirOfRuns = projet.getMainDirOfRuns(scenario);
        Assert.assertTrue(mainDirOfRuns.exists());
        try {
            File.createTempFile("newFile", ".txt", mainDirOfRuns);
        } catch (final IOException ex) {
            Logger.getLogger(RunCreateAndDeleteTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        Assert.assertTrue(controller.deleteAllRuns(scenario, null));
        Assert.assertFalse(projet.getMainDirOfRuns(scenario).exists());
    }

    /**
     * Doit renvoyer true si tous les fichiers de résultats existent dans le répertoire du run et si tous les fichiers d'entrée présent dans le
     * répertoire du run sont plus récents que ceux de l'étude.
     *
     * @param runToTest le run a teser
     * @param scenario le scenario
     * @return true si a jour
     */
    private static boolean isRunUpToDate(final EMHProjet projet, final EMHRun runToTest, final ManagerEMHScenario scenario) {
        //tous les fichier d'entree dans le répertoire run
        final Map<String, File> inputFilesInRun = projet.getInputFiles(scenario, runToTest);
        //tous les fichier d'entree dans le répertoire etude
        final Map<String, File> inputFilesInStudy = projet.getInputFiles(scenario);
        //tous les fichier resultats
        final Map<String, RunFile> filesResultat = projet.getRunFilesResultat(scenario, runToTest);

        for (final RunFile fileResultat : filesResultat.values()) {
            if (!fileResultat.getFile().isFile()) {
                return false;
            }
        }

        if (inputFilesInRun.size() != inputFilesInStudy.size()) {
            return false;
        }

        for (final Map.Entry<String, File> entry : inputFilesInRun.entrySet()) {
            final File inputFileInStudy = inputFilesInStudy.get(entry.getKey());

            if (inputFileInStudy == null) {
                return false;
            }

            if (inputFileInStudy.lastModified() >= entry.getValue().lastModified()) {
                return false;
            }
        }

        return true;
    }

    private void saveAndValidFile(final File etuFile) {
        final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ETU,
            TestCoeurConfig.INSTANCE_1_2);
        final CtuluLog log = new CtuluLog();
        final boolean valide = fileFormat.isValide(etuFile, log);
        testAnalyser(log);
        Assert.assertTrue(valide);
    }

    private void testIsRunUpToDate(final EMHProjet projet) {
        ManagerEMHScenario scenario = projet.getScenario("Sc_M3_c9");
        EMHRun run = scenario.getRunFromScenar("R074512");
        Assert.assertFalse(isRunUpToDate(projet, run, scenario));
        this.createFiles(projet, scenario, run);
        Assert.assertTrue(isRunUpToDate(projet, run, scenario));
        scenario = projet.getScenario("Sc_M3-0_c10");
        run = scenario.getRunFromScenar("R20090205084210");
        Assert.assertFalse(isRunUpToDate(projet, run, scenario));
        this.createFiles(projet, scenario, run);
        Assert.assertTrue(isRunUpToDate(projet, run, scenario));
    }

    private void createFiles(final EMHProjet projet, final ManagerEMHScenario scenario, final EMHRun run) {
        final Map<String, File> inputFiles = projet.getInputFiles(scenario, run);

        for (final File file : inputFiles.values()) {
            file.getParentFile().mkdirs();

            try {
                file.createNewFile();
                file.setLastModified(System.currentTimeMillis() - 5000);
            } catch (final IOException e) {
                Logger.getLogger(RunCreateAndDeleteTest.class.getName()).log(Level.SEVERE, "createFiles", e);
                Assert.fail();
            }
        }
        //pour rcal et rptg on copie des fichiers qui vont bien

        final Map<String, RunFile> resultFiles = projet.getRunFilesResultat(scenario, run);
        final List<RunFile> toCreateAtEnd = new ArrayList<>();
        final File crue10Resultat = new File(projet.getDirOfFichiersEtudes(), CRUE10_RESDIR);
        for (final RunFile runFile : resultFiles.values()) {
            if (runFile.getType().equals(CrueFileType.FCB) || runFile.getType().equals(CrueFileType.RCAL) || runFile.getType().equals(
                CrueFileType.RPTI)) {
                toCreateAtEnd.add(runFile);
            }

            final File file = runFile.getFile();

            file.getParentFile().mkdirs();

            try {
                file.createNewFile();
                file.setLastModified(System.currentTimeMillis() - 2000);
            } catch (final IOException e) {
                Logger.getLogger(RunCreateAndDeleteTest.class.getName()).log(Level.SEVERE, "createFiles", e);
                Assert.fail();
            }
            final boolean isRcal = runFile.getType().equals(CrueFileType.RCAL);
            if (isRcal || runFile.getType().equals(CrueFileType.RPTG)) {
                final File destDir = runFile.getFile().getParentFile();
                final File initDir = new File(crue10Resultat, isRcal ? "rcal"
                    : "rptg");
                for (final File elem : initDir.listFiles()) {
                    CtuluLibFile.copyFile(elem, new File(destDir, elem.getName()));
                }
            }
        }

        for (final RunFile runFile : toCreateAtEnd) {
            if (runFile.getType().equals(CrueFileType.RPTI)) {
                runFile.getFile().setLastModified(System.currentTimeMillis() + 1000);
            } else {
                runFile.getFile().setLastModified(System.currentTimeMillis() + 2000);
            }
        }
    }

    private EMHRun testCreate(final EMHProjet projet, final Date startTest) {
        final RunCreator creator = new RunCreator(projet, ConnexionInformationDefault.getDefault());
        ManagerEMHScenario scenario = projet.getScenario("Sc_M3_c9");

        CrueOperationResult<EMHRun> result = creator.create(scenario);
        CtuluLog log = result.getLogs().getLastLog();

        Assert.assertFalse(log.containsSevereError());

        EMHRun run = result.getResult();
        Date dateRun = creator.getDateCreation(run.getNom());
        Assert.assertTrue(dateRun + ">" + startTest, dateRun.compareTo(startTest) >= 0);

        File runDir = projet.getDirForRunModeleCrue9(scenario, run);
        File[] runFiles = runDir.listFiles();

        Assert.assertEquals(2, runFiles.length);

        for (final File file : runFiles) {
            final String fileName = file.getName();
            final long fileSize = file.length();

            if (fileName.equals("M3_c9.dc")) {
                Assert.assertEquals(10052L, fileSize);
            } else if (fileName.equals("M3_c9.dh")) {
                Assert.assertEquals(2395L, fileSize);
            } else {
                Assert.fail("Fichier non attendu : " + fileName);
            }
        }
        final RunCreatorOptions runCreatorOptions = new RunCreatorOptions(scenario.getRunFromScenar("R074512"));
        runCreatorOptions.setCompute(CrueFileType.FCB, RunCalculOption.EXECUTE_IF_NEEDED);
        runCreatorOptions.setCompute(CrueFileType.STO, RunCalculOption.EXECUTE_IF_NEEDED);
        runCreatorOptions.setCompute(CrueFileType.STR, RunCalculOption.EXECUTE_IF_NEEDED);

        result = creator.create(scenario, runCreatorOptions);
        log = result.getLogs().getLastLog();
        if (log.containsSevereError()) {
            log.printResume();
        }

        Assert.assertFalse(log.containsSevereError());

        run = result.getResult();
        dateRun = creator.getDateCreation(run.getNom());

        Assert.assertTrue(dateRun.compareTo(startTest) >= 0);

        runDir = projet.getDirForRunModeleCrue9(scenario, run);
        runFiles = runDir.listFiles();

        Assert.assertEquals(5, runFiles.length);

        for (final File file : runFiles) {
            final String fileName = file.getName();
            final long fileSize = file.length();

            if (fileName.equals("M3_c9.dc")) {
                Assert.assertEquals(10052L, fileSize);
            } else if (fileName.equals("M3_c9.dh")) {
                Assert.assertEquals(2395L, fileSize);
            } else if (fileName.equals("M3_c9.STO") || fileName.equals("M3_c9.STR") || fileName.equals("M3_c9.FCB")) {
                Assert.assertEquals(0L, fileSize);
            } else {
                Assert.fail("Fichier non attendu : " + fileName);
            }
        }

        //attention dans ce cas les fichier de l'étude sont remplacés par les fichier du run qui sont vide:
        scenario = projet.getScenario("Sc_M3-0_c10");
        result = creator.create(scenario);
        log = result.getLogs().getLastLog();

        Assert.assertFalse(log.containsSevereError());

        run = result.getResult();
        dateRun = creator.getDateCreation(run.getNom());

        Assert.assertTrue(dateRun.compareTo(startTest) >= 0);

        runDir = projet.getDirForRun(scenario, run);
        runFiles = runDir.listFiles();

        Assert.assertEquals(6, runFiles.length);

        for (final File runFile : runFiles) {
            final String fileName = runFile.getName();
            final long fileSize = runFile.length();

            if (fileName.equals("M3-0_c10.dclm.xml")) {
                Assert.assertEquals(2277L, fileSize);
            } else if (fileName.equals("M3-0_c10.dlhy.xml")) {
                Assert.assertEquals(4175L, fileSize);
            } else if (fileName.equals("M3-0_c10.ocal.xml")) {
                Assert.assertEquals(2814L, fileSize);
            } else if (fileName.equals("M3-0_c10.ores.xml")) {
                Assert.assertEquals(3379L, fileSize);
            } else if (fileName.equals("M3-0_c10.pcal.xml")) {
                Assert.assertEquals(1252L, fileSize);
            } else if (fileName.equals("Mo_M3-0_c10")) {
                final File[] modeleFiles = runFile.listFiles();

                Assert.assertEquals(9, modeleFiles.length);

                for (final File modeleFile : modeleFiles) {
                    final String modeleFileName = modeleFile.getName();
                    final long modeleFileSize = modeleFile.length();

                    if (modeleFileName.equals("M3-0_c10.dcsp.xml")) {
                        Assert.assertEquals(2742L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.dfrt.xml")) {
                        Assert.assertEquals(6357L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.dptg.xml")) {
                        Assert.assertEquals(28151L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.dpti.xml")) {
                        Assert.assertEquals(3103L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.drso.xml")) {
                        Assert.assertEquals(12457L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.optg.xml")) {
                        Assert.assertEquals(2266L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.opti.xml")) {
                        Assert.assertEquals(1680L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.optr.xml")) {
                        Assert.assertEquals(2198L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.pnum.xml")) {
                        Assert.assertEquals(1922L, modeleFileSize);
                    } else {
                        Assert.fail("Fichier non attendu : " + modeleFileName);
                    }
                }
            } else {
                Assert.fail("Fichier non attendu : " + fileName);
            }
        }
        final EMHRun runFromScenar = scenario.getRunFromScenar("R20090205084210");

        result = creator.create(scenario, new RunCreatorOptions(runFromScenar));
        log = result.getLogs().getLastLog();
        Assert.assertFalse(log.containsSevereError());
        run = result.getResult();
        dateRun = creator.getDateCreation(run.getNom());
        Assert.assertTrue(dateRun.compareTo(startTest) >= 0);
        runDir = projet.getDirForRun(scenario, run);
        runFiles = runDir.listFiles();
        Assert.assertEquals(6, runFiles.length);

        for (final File runFile : runFiles) {
            final String fileName = runFile.getName();
            final long fileSize = runFile.length();

            if (fileName.equals("M3-0_c10.dclm.xml")) {
                Assert.assertEquals(2277L, fileSize);
            } else if (fileName.equals("M3-0_c10.dlhy.xml")) {
                Assert.assertEquals(4175L, fileSize);
            } else if (fileName.equals("M3-0_c10.ocal.xml")) {
                Assert.assertEquals(2814L, fileSize);
            } else if (fileName.equals("M3-0_c10.ores.xml")) {
                Assert.assertEquals(3379L, fileSize);
            } else if (fileName.equals("M3-0_c10.pcal.xml")) {
                Assert.assertEquals(1252L, fileSize);
            } else if (fileName.equals("Mo_M3-0_c10")) {
                final File[] modeleFiles = runFile.listFiles();

                Assert.assertEquals(15, modeleFiles.length);

                for (final File modeleFile : modeleFiles) {
                    final String modeleFileName = modeleFile.getName();
                    final long modeleFileSize = modeleFile.length();

                    if (modeleFileName.equals("M3-0_c10.dcsp.xml")) {
                        Assert.assertEquals(2742L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.dfrt.xml")) {
                        Assert.assertEquals(6357L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.dptg.xml")) {
                        Assert.assertEquals(28151L, modeleFileSize);//done
                    } else if (modeleFileName.equals("M3-0_c10.dpti.xml")) {
                        Assert.assertEquals(3103L, modeleFileSize);//done
                    } else if (modeleFileName.equals("M3-0_c10.drso.xml")) {
                        Assert.assertEquals(12457L, modeleFileSize);//done
                    } else if (modeleFileName.equals("M3-0_c10.optg.xml")) {
                        Assert.assertEquals(2266L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.opti.xml")) {
                        Assert.assertEquals(1680L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.optr.xml")) {
                        Assert.assertEquals(2198L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.pnum.xml")) {
                        Assert.assertEquals(1922L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.rptg.bin")) {
                        Assert.assertEquals(8976L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.rcal.xml")) {
                        Assert.assertEquals(6788L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.rcal_0001.bin")) {
                        Assert.assertEquals(8976L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.rpti.xml")
                        || modeleFileName.equals("M3-0_c10.rptr.xml")) {
                        Assert.assertEquals(0L, modeleFileSize);
                    } else if (modeleFileName.equals("M3-0_c10.rptg.xml")) {
                        Assert.assertEquals(39305L, modeleFileSize);
                    } else {
                        Assert.fail("Fichier non attendu : " + modeleFileName);
                    }
                }
            } else {
                Assert.fail("Fichier non attendu : " + fileName);
            }
        }
        //dans ce cas les fichiers rcals ne sont pas copies
        final RunCreatorOptions options = new RunCreatorOptions(runFromScenar);
        options.setCompute(CrueFileType.RCAL, RunCalculOption.DONT_EXECUTE);
        result = creator.create(scenario, options);
        log = result.getLogs().getLastLog();
        Assert.assertFalse(log.containsSevereError());
        final EMHRun newRun = result.getResult();
        runDir = projet.getDirForRun(scenario, newRun);
        final File modeleDir = new File(runDir, "Mo_M3-0_c10");
        Assert.assertEquals(13, modeleDir.listFiles().length);
        return run;
    }
}
