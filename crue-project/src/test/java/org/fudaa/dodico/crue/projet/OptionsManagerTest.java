/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.projet.conf.*;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * @author deniger
 */
public class OptionsManagerTest extends AbstractTestParent {
  private static final String DEFAULT_VALUE = "ok";

  @Test
  public void testSiteAllHere() {
    final GlobalOptionsManager manager = new GlobalOptionsManager();
    SiteConfiguration siteConfiguration = createSiteConfiguration();
    final UserConfiguration userConfiguration = createUserConfiguration();
    CtuluLogGroup init = manager.init(createConfiguration(siteConfiguration, userConfiguration), createUserConfiguration());
    Assert.assertTrue(init.containsError());
    final Collection<CtuluLogRecord> records = init.getLogs().get(0).getRecords();
    Assert.assertEquals(OptionsEnum.values().length, records.size());
    for (final CtuluLogRecord ctuluLogRecord : records) {
      Assert.assertEquals("conf.optionNotSet", ctuluLogRecord.getMsg());
      Assert.assertNotNull(ctuluLogRecord.getArgs()[0]);
    }
    siteConfiguration = createSiteConfigurationComplete();
    init = manager.init(createConfiguration(siteConfiguration, userConfiguration), createUserConfiguration());
    Assert.assertFalse(init.containsError());
    Assert.assertFalse(init.containsSomething());
    //we test with editor in editable par
    init = manager.init(createConfigWithEditorEditable(), createUserConfiguration());
    Assert.assertFalse(init.containsSomething());
  }

  @Test
  public void testGetOption() {
    final GlobalOptionsManager manager = new GlobalOptionsManager();
    manager.init(createConfigWithEditorEditable(), createUserConfiguration());
    Assert.assertEquals(DEFAULT_VALUE, manager.getOption(OptionsEnum.CRUE9_EXE).getValeur());
    Assert.assertEquals(DEFAULT_VALUE, manager.getOption(OptionsEnum.EDITOR).getValeur());
    final String newValue = "newValue";
    manager.init(createConfigWithEditorEditable(), createUserConfiguration(OptionsEnum.EDITOR, newValue));
    Assert.assertEquals(DEFAULT_VALUE, manager.getOption(OptionsEnum.CRUE9_EXE).getValeur());
    Assert.assertEquals(newValue, manager.getOption(OptionsEnum.EDITOR).getValeur());
  }

  private Configuration createConfigWithEditorEditable() {
    final SiteConfiguration siteConfiguration = createSiteConfiguration();
    final List<SiteOption> siteOption = new ArrayList<>();
    final OptionsEnum[] values = OptionsEnum.values();
    for (final OptionsEnum optionsEnum : values) {
      if (!optionsEnum.equals(OptionsEnum.EDITOR)) {
        siteOption.add(new SiteOption(optionsEnum.getId(), getDefaultValue(optionsEnum, DEFAULT_VALUE), null, true));
      }
    }
    siteConfiguration.setOptions(siteOption);
    final UserConfiguration userConfiguration = createUserConfiguration();
    final List<UserOption> userOption = new ArrayList<>();
    userOption.add(new UserOption(OptionsEnum.EDITOR.getId(), DEFAULT_VALUE, null));
    userConfiguration.setOptions(userOption);
    return createConfiguration(siteConfiguration, userConfiguration);
  }

  @Test
  public void testSiteUnknowSiteAllHere() {
    final GlobalOptionsManager manager = new GlobalOptionsManager();
    final SiteConfiguration siteConfiguration = createSiteConfigurationComplete();
    final UserConfiguration userConfiguration = createUserConfiguration();
    add(siteConfiguration, new SiteOption("test", "test", null, true));
    final CtuluLogGroup init = manager.init(createConfiguration(siteConfiguration, userConfiguration), createUserConfiguration());
    Assert.assertTrue(init.containsError());
    final Collection<CtuluLogRecord> records = init.getLogs().get(0).getRecords();
    Assert.assertEquals(1, records.size());
    final CtuluLogRecord ctuluLogRecord = records.iterator().next();
    Assert.assertEquals("conf.optionUnknown", ctuluLogRecord.getMsg());
    Assert.assertEquals("test", ctuluLogRecord.getArgs()[0]);
  }

  @Test
  public void testUnknownLanguage() {
    final GlobalOptionsManager manager = new GlobalOptionsManager();
    final SiteConfiguration siteConfiguration = createSiteConfigurationComplete();
    final List<SiteOption> options = siteConfiguration.getOptions();
    for (final Iterator<SiteOption> it = options.iterator(); it.hasNext(); ) {
      final SiteOption siteOption = it.next();
      if (siteOption.getId().equals(OptionsEnum.AVAILABLE_LANGUAGE.getId())) {
        it.remove();
      }
      if (siteOption.getId().equals(OptionsEnum.USER_LANGUAGE.getId())) {
        it.remove();
      }
    }
    siteConfiguration.getOptions().add(new SiteOption(OptionsEnum.AVAILABLE_LANGUAGE.getId(), "fr_FR;en_EN", null, true));
    final UserConfiguration initUserConfiguration = createUserConfiguration(OptionsEnum.USER_LANGUAGE, "fr_FR");
    //test avec erreur
    UserConfiguration userConfiguration = createUserConfiguration(OptionsEnum.USER_LANGUAGE, "nothing");
    CtuluLogGroup init = manager.init(createConfiguration(siteConfiguration, initUserConfiguration), userConfiguration);
    Assert.assertTrue(init.containsError());
    final Collection<CtuluLogRecord> records = init.getLogs().get(1).getRecords();
    Assert.assertEquals(1, records.size());
    final CtuluLogRecord ctuluLogRecord = records.iterator().next();
    Assert.assertEquals("option.LanguageUnknow.Error", ctuluLogRecord.getMsg());
    Assert.assertEquals("nothing", ctuluLogRecord.getArgs()[0]);

    //test ok
    userConfiguration = createUserConfiguration(OptionsEnum.USER_LANGUAGE, "en_EN");
    init = manager.init(createConfiguration(siteConfiguration, initUserConfiguration), userConfiguration);
    Assert.assertFalse(init.containsError());
  }

  @Test
  public void testSiteTwice() {
    final GlobalOptionsManager manager = new GlobalOptionsManager();
    final SiteConfiguration siteConfiguration = createSiteConfigurationComplete();
    final UserConfiguration userConfiguration = createUserConfiguration();
    add(siteConfiguration, new SiteOption(OptionsEnum.CRUE9_EXE.getId(), "test", null, true));
    final CtuluLogGroup init = manager.init(createConfiguration(siteConfiguration, userConfiguration), createUserConfiguration());
    Assert.assertTrue(init.containsError());
    final Collection<CtuluLogRecord> records = init.getLogs().get(0).getRecords();
    Assert.assertEquals(1, records.size());
    final CtuluLogRecord ctuluLogRecord = records.iterator().next();
    Assert.assertEquals("conf.defineTwice", ctuluLogRecord.getMsg());
    Assert.assertEquals(OptionsEnum.CRUE9_EXE.getId(), ctuluLogRecord.getArgs()[0]);
  }

  private Configuration createConfiguration(final SiteConfiguration site, final UserConfiguration user) {
    final Configuration conf = new Configuration();
    conf.setSite(site);
    conf.setUser(user);
    return conf;
  }

  @Test
  public void testSiteRequiredButNoSetAndNotEditable() {
    final GlobalOptionsManager manager = new GlobalOptionsManager();
    final SiteConfiguration siteConfiguration = createSiteConfigurationCompleteWillNullValues();
    final UserConfiguration userConfiguration = createUserConfiguration();
    final CtuluLogGroup init = manager.init(createConfiguration(siteConfiguration, userConfiguration), createUserConfiguration());
    Assert.assertTrue(init.containsError());
    final Collection<CtuluLogRecord> records = init.getLogs().get(0).getRecords();
    Assert.assertEquals(OptionsEnum.values().length, records.size());
    for (final CtuluLogRecord ctuluLogRecord : records) {
      Assert.assertEquals("conf.optionRequiredEmptyButNoEditable", ctuluLogRecord.getMsg());
      Assert.assertNotNull(ctuluLogRecord.getArgs()[0]);
    }
  }

  @Test
  public void testUserOverrideSite() {
    final GlobalOptionsManager manager = new GlobalOptionsManager();
    final SiteConfiguration siteConfiguration = createSiteConfigurationComplete();
    final UserConfiguration userConfiguration = createUserConfiguration(OptionsEnum.CRUE9_EXE, "value");
    final CtuluLogGroup init = manager.init(createConfiguration(siteConfiguration, userConfiguration), createUserConfiguration());
    Assert.assertTrue(init.containsError());
    final Collection<CtuluLogRecord> records = init.getLogs().get(0).getRecords();
    Assert.assertEquals(1, records.size());
    final CtuluLogRecord ctuluLogRecord = records.iterator().next();
    Assert.assertEquals("conf.userConfOverridesSiteConf", ctuluLogRecord.getMsg());
    Assert.assertEquals(OptionsEnum.CRUE9_EXE.getId(), ctuluLogRecord.getArgs()[0]);
  }

  protected SiteConfiguration add(final SiteConfiguration siteConfiguration, final SiteOption toAdd) {
    final List<SiteOption> options = new ArrayList<>(siteConfiguration.getOptions());
    options.add(toAdd);
    siteConfiguration.setOptions(options);
    return siteConfiguration;
  }

  private SiteConfiguration createSiteConfiguration() {
    final SiteConfiguration siteConfiguration = new SiteConfiguration();
    siteConfiguration.setOptions(new ArrayList<>());
    return siteConfiguration;
  }

  private UserConfiguration createUserConfiguration() {
    final UserConfiguration userConfiguration = new UserConfiguration();
    userConfiguration.setOptions(new ArrayList<>());
    return userConfiguration;
  }

  private SiteConfiguration createSiteConfigurationComplete() {
    return createSiteConfigurationComplete("ok");
  }

  private String getDefaultValue(final OptionsEnum optionsEnum, final String defaultVal) {
    if (optionsEnum.getDefaultValue() != null) {
      return optionsEnum.getDefaultValue();
    }
    return defaultVal;
  }

  private SiteConfiguration createSiteConfigurationComplete(final String value) {
    final SiteConfiguration res = createSiteConfiguration();
    final List<SiteOption> options = new ArrayList<>();
    final OptionsEnum[] all = OptionsEnum.values();
    for (final OptionsEnum optionsEnum : all) {
      options.add(new SiteOption(optionsEnum.getId(), getDefaultValue(optionsEnum, value), null, true));
    }
    res.setOptions(options);
    res.setAide(new SiteAide());
    return res;
  }

  private SiteConfiguration createSiteConfigurationCompleteWillNullValues() {
    final SiteConfiguration res = createSiteConfiguration();
    final List<SiteOption> options = new ArrayList<>();
    final OptionsEnum[] all = OptionsEnum.values();
    for (final OptionsEnum optionsEnum : all) {
      options.add(new SiteOption(optionsEnum.getId(), null, null, true));
    }
    res.setOptions(options);
    return res;
  }

  private UserConfiguration createUserConfiguration(final OptionsEnum opt1, final String value1) {
    return createUserConfiguration(opt1, value1, null, null);
  }

  private UserConfiguration createUserConfiguration(final OptionsEnum opt1, final String value1, final OptionsEnum opt2, final String value2) {
    final UserConfiguration siteConfiguration = createUserConfiguration();
    final List<UserOption> options = new ArrayList<>();
    options.add(new UserOption(opt1.getId(), value1, null));
    if (opt2 != null) {
      options.add(new UserOption(opt2.getId(), value2, null));
    }
    siteConfiguration.setOptions(options);
    return siteConfiguration;
  }
}
