package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.Pair;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.EMHProjectInfos;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SmImportValidatorTest {
  private static File dir;
  private static File etuDir;

  @BeforeClass
  public static void createMainTempDir() throws Exception {
    dir = CtuluLibFile.createTempDir();
    etuDir = new File(dir, "etu");
    etuDir.mkdir();
  }

  @AfterClass
  public static void deleteMainTempDir() throws Exception {
    CtuluLibFile.deleteDir(dir);
  }

  @Test
  public void isNotValidWithEtuNull() {
    Pair<Boolean, String> result = new SmImportValidator(null).canImportSousModele(null, false);
    assertIsErrorAndMessage(result, "sm.import.etuNotSet");
  }

  @Test
  public void isNotValidFileToImportNotExisting() {
    final File notExistingFile = new File(dir, "NotExisting");
    Pair<Boolean, String> result = new SmImportValidator(createProjet("test1")).canImportSousModele(notExistingFile, false);
    assertIsErrorAndMessage(result, "sm.import.fileNotExisting", notExistingFile.getAbsolutePath());
  }

  private void assertIsErrorAndMessage(Pair<Boolean, String> result, String code, Object... args) {
    Assert.assertFalse(result.firstValue);
    Assert.assertEquals(BusinessMessages.getString(code, args), result.secondValue);
  }

  @Test
  public void cannotImportADirectory() throws IOException {
    final SmImportValidator smImporterValidator = new SmImportValidator(createProjet("tes1"));
    Pair<Boolean, String> result = smImporterValidator.canImportSousModele(etuDir, false);
    assertIsErrorAndMessage(result, "sm.import.fileIsDirectory", etuDir.getAbsolutePath());
  }

  @Test
  public void cannotimportNotExistingFile() throws IOException {
    final SmImportValidator smImporterValidator = new SmImportValidator(createProjet("tes1"));
    File drsoFile = new File(etuDir, "file." + CrueFileType.DRSO.getExtension());
    Pair<Boolean, String> result = smImporterValidator.canImportSousModele(drsoFile, false);
    assertIsErrorAndMessage(result, "sm.import.fileNotExisting", drsoFile.getAbsolutePath());
  }

  @Test
  public void cannotImportFileContainedInEtuDirectory() throws IOException {
    final SmImportValidator smImporterValidator = new SmImportValidator(createProjet("tes1"));
    File drsoFile = new File(etuDir, "file." + CrueFileType.DRSO.getExtension());
    Assert.assertTrue(drsoFile.createNewFile());
    Pair<Boolean, String> result = smImporterValidator.canImportSousModele(drsoFile, false);
    assertIsErrorAndMessage(result, "sm.import.fileIsIncludedInEtuFolder", drsoFile.getAbsolutePath());
    CtuluLibFile.deleteDir(drsoFile);
  }

  @Test
  public void canImportFile() throws IOException {
    File smFolder = new File(dir, "canImportFile");
    Assert.assertTrue(smFolder.mkdir());
    String name = "testIsOk";
    final File drsoFile = new File(smFolder, name + ".drso.xml");
    Assert.assertTrue(drsoFile.createNewFile());
    new File(smFolder, name + ".dfrt.xml").createNewFile();
    new File(smFolder, name + ".dptg.xml").createNewFile();
    new File(smFolder, name + ".dcsp.xml").createNewFile();
    Pair<Boolean, String> result = new SmImportValidator(createProjet("sm1")).canImportSousModele(drsoFile, false);
    Assert.assertTrue(result.firstValue);
    CtuluLibFile.deleteDir(smFolder);
  }

  @Test
  public void cannotImportMissingFile() throws IOException {
    File smFolder = new File(dir, "scanFound");
    Assert.assertTrue(smFolder.mkdir());
    String name = "testIsOk";
    final File drsoFile = new File(smFolder, name + ".drso.xml");
    drsoFile.createNewFile();
    new File(smFolder, name + ".dfrt.xml").createNewFile();
    new File(smFolder, name + ".dcsp.xml").createNewFile();
    Pair<Boolean, String> result = new SmImportValidator(createProjet("sm1")).canImportSousModele(drsoFile, false);
    assertIsErrorAndMessage(result, "sm.import.fileNotExisting", name + ".dptg.xml");
    CtuluLibFile.deleteDir(smFolder);
  }

  @Test
  public void cannotImportFilenameNotValid() throws IOException {
    File smFolder = new File(dir, "scanNotFoundInvalidFile");
    Assert.assertTrue(smFolder.mkdir());
    String name = "name is invalid";
    final File drsoFile = new File(smFolder, name + ".drso.xml");
    Assert.assertTrue(drsoFile.createNewFile());
    new File(smFolder, name + ".dfrt.xml").createNewFile();
    new File(smFolder, name + ".dptg.xml").createNewFile();
    new File(smFolder, name + ".dcsp.xml").createNewFile();
    Pair<Boolean, String> result = new SmImportValidator(createProjet("sm1")).canImportSousModele(drsoFile, false);
    assertIsErrorAndMessage(result, "sm.import.radicalNotValid", name);
    CtuluLibFile.deleteDir(smFolder);
  }

  @Test
  public void canImport() {
    EMHProjet projet = createProjet("test1", "test2");
    Assert.assertTrue(new SmImportValidator(projet).canCreateNewSousModeleName("test").firstValue);
  }

  @Test
  public void cannotImport() {
    EMHProjet projet = createProjet("test1", "test2");
    Assert.assertFalse(new SmImportValidator(projet).canCreateNewSousModeleName("test1").firstValue);
    Assert.assertFalse(new SmImportValidator(projet).canCreateNewSousModeleName("test2").firstValue);
  }

  @Test
  public void cannotImportBecauseInvalidName() {
    EMHProjet projet = createProjet("test1", "test2");
    Assert.assertFalse(new SmImportValidator(projet).canCreateNewSousModeleName("test 1").firstValue);
  }

  private EMHProjet createProjet(String... sousModeleNames) {
    final EMHProjet projet = new EMHProjet();
    EMHProjectInfos targetInfos = new EMHProjectInfos();
    projet.setInfos(targetInfos);
    Map<String, String> directories = new HashMap<>();
    directories.put(EMHProjectInfos.FICHETUDES, etuDir.getAbsolutePath());
    projet.getInfos().setDirectories(directories);

    for (String sousModeleName : sousModeleNames) {
      final ManagerEMHSousModele sousModele = new ManagerEMHSousModele(sousModeleName);
      createFichierCrue(projet, sousModeleName, CrueFileType.DRSO, sousModele);
      createFichierCrue(projet, sousModeleName, CrueFileType.DCSP, sousModele);
      createFichierCrue(projet, sousModeleName, CrueFileType.DFRT, sousModele);
      createFichierCrue(projet, sousModeleName, CrueFileType.DPTG, sousModele);
      projet.addBaseSousModele(sousModele);
    }
    return projet;
  }

  private static FichierCrue createFichierCrue(EMHProjet projet, final String name, final CrueFileType type, final ManagerEMHSousModele sousModele) {
    final FichierCrue fichierCrue = new FichierCrue(name + "." + type.getExtension(), name + "." + type.getExtension(), type);
    sousModele.addFichierDonnees(fichierCrue);
    projet.getInfos().addCrueFileToProject(fichierCrue);
    return fichierCrue;
  }
}
