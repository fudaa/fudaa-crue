package org.fudaa.dodico.crue.projet.clean;

import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormatETU;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.ProjectNormalizableCallable;
import org.fudaa.dodico.crue.projet.select.DoublonScenarioFinder;
import org.fudaa.dodico.crue.projet.select.ToNormalizeScenarioFinder;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;

public class EMHProjectCleanerTest extends AbstractTestParent {
  /**
   * Test la normalisation/nettoyage d'une etude
   */
  @Test
  public void testCleanProject() {
    //chargement
    final File exportZipInTempDir = exportZipInTempDir("/clean/Etu3-6.zip");
    final File etuFile = getEtuFileUpdated("Etu3-6", exportZipInTempDir);
    EMHProjet projet = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE_1_2);
    final EMHProjectCleaner cleaner = new EMHProjectCleaner(projet, etuFile);

    //l'etude contient des données a nettoyer
    final ScenarioCleanData data = createCleanData(projet);
    Assert.assertEquals(1, data.scenarioToNormalizeOrWithErrors.size());
    Assert.assertEquals(1, data.scenarioNamesToDelete.size());

    //nettoyage du projet
    cleaner.cleanProject(data, true);

    //on recharge et on verifie que le projet est nettoye
    projet = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE_1_2);

    final ScenarioCleanData afterCleaning = createCleanData(projet);
    Assert.assertEquals(0, afterCleaning.scenarioToNormalizeOrWithErrors.size());
    Assert.assertEquals(0, afterCleaning.scenarioNamesToDelete.size());
  }

  private ScenarioCleanData createCleanData(final EMHProjet projet) {
    final ToNormalizeScenarioFinder finder = new ToNormalizeScenarioFinder(projet);

    final List<ProjectNormalizableCallable.Result> normalizeOrWithErrors = finder.findToNormalizeOrWithErrors();
    final ScenarioCleanData data = new ScenarioCleanData();
    data.scenarioToNormalizeOrWithErrors.addAll(normalizeOrWithErrors);

    final DoublonScenarioFinder doublonScenarioFinder = new DoublonScenarioFinder(projet);
    data.scenarioNamesToDelete.addAll(TransformerHelper.toNom(doublonScenarioFinder.getDoublons()));
    return data;
  }
}
