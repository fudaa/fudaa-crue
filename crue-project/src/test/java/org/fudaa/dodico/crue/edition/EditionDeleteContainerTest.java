package org.fudaa.dodico.crue.edition;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.common.ConnexionInformationDefault;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.integration.IntegrationHelper;
import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class EditionDeleteContainerTest {
  public static File target;

  @BeforeClass
  public static void setUpClass() throws Exception {
    target = CtuluLibFile.createTempDir();
  }

  @AfterClass
  public static void tearDownClass() {
    CtuluLibFile.deleteDir(target);
  }

  @Test
  public void delete() {
    AbstractTestParent.exportZip(target, "/integration/Etu201.zip");
    final File file = new File(target, "Etu201.etu.xml");
    assertTrue(file.exists());
    final EMHProjet projet = IntegrationHelper.loadEtu(file, TestCoeurConfig.INSTANCE_1_2);
    assertNotNull(projet);
    final String scenarioName = "Sc_M201-1_c9c9c10";
    final ManagerEMHContainerBase container = projet.getContainer(scenarioName, CrueLevelType.SCENARIO);
    assertNotNull(container);
    assertNotNull(projet.getContainer("Mo_M201-1_c9c9c10", CrueLevelType.MODELE));
    assertNotNull(projet.getContainer("Sm_M201-1_c9c9c10", CrueLevelType.SOUS_MODELE));
    final File dlhy = new File(target, "M201-1_c9c9c10.dlhy.xml");
    assertTrue(dlhy.exists());
    final File drso = new File(target, "M201-1_c9c9c10.drso.xml");
    assertTrue(drso.exists());
    final File runDir = new File(target, "RUNS" + File.separator + "Sc_M201-1_c9c9c10" + File.separator + "R2012-10-10-15h21m53s");
    assertTrue(runDir.exists());
    assertTrue(runDir.isDirectory());

    final EMHProjetController projectController = new EMHProjetController(projet, new ConnexionInformationDefault());
    final EditionDeleteContainer deleteContainer = new EditionDeleteContainer(projectController);
    deleteContainer.delete(container, true);
    assertFalse(dlhy.exists());
    assertFalse(drso.exists());
    assertFalse(runDir.exists());

    assertNull(projet.getContainer(scenarioName, CrueLevelType.SCENARIO));
    assertNull(projet.getContainer("Mo_M201-1_c9c9c10", CrueLevelType.MODELE));
    assertNull(projet.getContainer("Sm_M201-1_c9c9c10", CrueLevelType.SOUS_MODELE));
  }
}
