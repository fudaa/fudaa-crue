/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.test.ReadHelperForTest;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.projet.validation.Crue9PostLoadEditionValidation;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Frederic Deniger
 */
public class ChangeIdScenarioCrue9Test {
    @Test
    public void testRebuildOn201() {
        final CtuluLog log = new CtuluLog();
        final CrueIOResu<CrueData> read = new ReadHelperForTest(TestCoeurConfig.INSTANCE)
            .readModeleAndDefaultORES(log, "/ChangeIdScenarioCrue9Test/201.dc", "/ChangeIdScenarioCrue9Test/201.dh");
        Assert.assertFalse(log.containsErrorOrSevereError());
        final EMHScenario scenario = read.getMetier().getScenarioData();
        Assert.assertNotNull(scenario);
        final EMHSousModele sousModele = scenario.getModeles().get(0).getSousModeles().get(0);
        final ScenarioAutoModifiedState state = new ScenarioAutoModifiedState();
        Crue9PostLoadEditionValidation.verifiePrefixeNomDonneesCrue9(read.getMetier(), state);
        Assert.assertTrue(state.isRenamedDone());
        Assert.assertFalse(state.isReorderedDone());
        final DonFrtConteneur frtConteneur = sousModele.getFrtConteneur();
        final List<DonFrt> listFrt = frtConteneur.getListFrt();
        final Set<String> ids = new HashSet<>();
        for (final DonFrt donFrt : listFrt) {
            Assert.assertFalse(ids.contains(donFrt.getId()));
            ids.add(donFrt.getId());
        }
        final List<CatEMHSection> sections = sousModele.getSections();
        ids.clear();
        for (final CatEMHSection section : sections) {
            Assert.assertFalse(ids.contains(section.getId()));
            ids.add(section.getId());
            if (section.getClass().equals(EMHSectionProfil.class)) {
                final DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection((EMHSectionProfil) section);
                Assert.assertEquals(profilSection.getNom(), CruePrefix.changePrefix(section.getNom(), CruePrefix.P_SECTION, CruePrefix.P_PROFIL_SECTION));
            }
        }
    }
}
