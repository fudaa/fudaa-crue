/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.io.test.ReadHelperForTest;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Frederic Deniger
 */
public class Crue9PostLoadEditionValidationTest {
    public Crue9PostLoadEditionValidationTest() {
    }

    /**
     * Test de lecture du modele 3 complet.
     */
    @Test
    public void testLectureModele3() {
        final CrueData testLectureFichierDC = new ReadHelperForTest(TestCoeurConfig.INSTANCE).readModeleCrue9(new CtuluLog(), "/M3-0_c9.dc");
        DonFrt fko = EMHHelper.find(testLectureFichierDC.getFrottements().getListFrt(), "K0");
        assertNotNull(fko);
        assertEquals(EnumTypeLoi.LoiZFKSto, fko.getLoi().getType());
        final ScenarioAutoModifiedState state = new ScenarioAutoModifiedState();
        AbstractTestParent.testAnalyser(Crue9PostLoadEditionValidation.verifiePrefixeNomDonneesCrue9(testLectureFichierDC, state));
        assertTrue(state.isRenamedDone());
        fko = EMHHelper.find(testLectureFichierDC.getFrottements().getListFrt(), "FkSto_K0");
        assertNotNull(fko);
        assertEquals(EnumTypeLoi.LoiZFKSto, fko.getLoi().getType());
    }
}
