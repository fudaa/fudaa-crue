/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformationDefault;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormatETU;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * @author deniger
 */
public class RenameFileAndManagerTest extends AbstractTestParent {
  @Test
  public void testFileRenameFailed() {
    File exportZipInTempDir = exportZipInTempDir("/projet/Etu3-0_1.1.2.zip");
    File etuFile = getEtuFileUpdated("etu3-0", exportZipInTempDir);
    Assert.assertNotNull(etuFile);
    EMHProjet projet = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE);
    String fileToRename = "M3-0_c10.dfrt.xml";
    FichierCrue fileFromBase = projet.getInfos().getFileFromBase(fileToRename);
    String fileExisting = "Poub_c10.dfrt.xml";
    Assert.assertFalse(projet.renameFile(fileFromBase, fileExisting, true));
  }


  @Test
  public void testFileRenameSucceed() {
    File exportZipInTempDir = exportZipInTempDir("/projet/Etu3-0_1.1.2.zip");
    File etuFile = getEtuFileUpdated("etu3-0", exportZipInTempDir);
    Assert.assertNotNull(etuFile);
    EMHProjet projet = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE);
    String fileToRename = "M3-0_c10.dfrt.xml";
    FichierCrue fileFromBase = projet.getInfos().getFileFromBase(fileToRename);
    Assert.assertNotNull(fileFromBase);
    File old = fileFromBase.getProjectFile(projet);
    long size = old.length();
    Assert.assertTrue(old.exists());
    final String newFileName = "M3-0_c10Bis.dfrt.xml";
    projet.renameFile(fileFromBase, newFileName, true);
    File newFile = fileFromBase.getProjectFile(projet);
    Assert.assertFalse(old.exists());
    Assert.assertTrue(newFile.exists());
    Assert.assertEquals(size, newFile.length());
    EMHProjetController controller = new EMHProjetController(projet, ConnexionInformationDefault.getDefault());
    CtuluLogGroup logs = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    controller.saveProjet(etuFile, logs);
    Assert.assertFalse(logs.containsSomething());
    projet = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE);
    Assert.assertNotNull(projet);
    fileFromBase = projet.getInfos().getFileFromBase(fileToRename);
    Assert.assertNull(fileFromBase);
    fileFromBase = projet.getInfos().getFileFromBase(newFileName);
    Assert.assertNotNull(fileFromBase);
  }
}
