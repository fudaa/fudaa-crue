package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.LitNumerote;
import org.fudaa.dodico.crue.metier.emh.PtProfil;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SmUpdateDonPrtEqualsHelperTest {
  @Test
  public void isSame() {
    SmUpdateDonPrtEqualsHelper helper = new SmUpdateDonPrtEqualsHelper(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    DonPrtGeoProfilSection prtGeoProfilSection1 = create();
    DonPrtGeoProfilSection prtGeoProfilSection2 = create();
    helper.isSame(prtGeoProfilSection1, prtGeoProfilSection2);

    addLit(prtGeoProfilSection1, "test", 0, 5);
    addLit(prtGeoProfilSection2, "test", 0, 5);
    Assert.assertTrue(helper.isSame(prtGeoProfilSection1, prtGeoProfilSection2));

    prtGeoProfilSection1.getLitNumerote().get(0).setNomLit(new LitNomme("test 2", 1));
    Assert.assertTrue("Identique meme si nom du lit different", helper.isSame(prtGeoProfilSection1, prtGeoProfilSection2));
  }

  @Test
  public void isNotSameBecauseNotSameNumberOfPoints() {
    SmUpdateDonPrtEqualsHelper helper = new SmUpdateDonPrtEqualsHelper(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    DonPrtGeoProfilSection prtGeoProfilSection1 = createDonPrtGeoProfilSectionWithLit();
    DonPrtGeoProfilSection prtGeoProfilSection2 = createDonPrtGeoProfilSectionWithLit();
    helper.isSame(prtGeoProfilSection1, prtGeoProfilSection2);
    prtGeoProfilSection1.getPtProfil().add(new PtProfil(100, 100));
    Assert.assertFalse(helper.isSame(prtGeoProfilSection1, prtGeoProfilSection2));
  }

  @Test
  public void isNotSameBecausePtDifferent() {
    SmUpdateDonPrtEqualsHelper helper = new SmUpdateDonPrtEqualsHelper(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    DonPrtGeoProfilSection prtGeoProfilSection1 = createDonPrtGeoProfilSectionWithLit();
    DonPrtGeoProfilSection prtGeoProfilSection2 = createDonPrtGeoProfilSectionWithLit();
    prtGeoProfilSection1.getPtProfil().get(0).setZ(-1);
    Assert.assertFalse(helper.isSame(prtGeoProfilSection1, prtGeoProfilSection2));
  }

  @Test
  public void isNotSameBecauseLitDifferent() {
    SmUpdateDonPrtEqualsHelper helper = new SmUpdateDonPrtEqualsHelper(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    DonPrtGeoProfilSection prtGeoProfilSection1 = createDonPrtGeoProfilSectionWithLit();
    DonPrtGeoProfilSection prtGeoProfilSection2 = createDonPrtGeoProfilSectionWithLit();
    prtGeoProfilSection1.getLitNumerote().get(0).setLimDeb(prtGeoProfilSection1.getPtProfil().get(1));
    Assert.assertFalse(helper.isSame(prtGeoProfilSection1, prtGeoProfilSection2));
  }

  @Test
  public void isNotSameBecauseNumberOfLitDifferent() {
    SmUpdateDonPrtEqualsHelper helper = new SmUpdateDonPrtEqualsHelper(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    DonPrtGeoProfilSection prtGeoProfilSection1 = createDonPrtGeoProfilSectionWithLit();
    DonPrtGeoProfilSection prtGeoProfilSection2 = createDonPrtGeoProfilSectionWithLit();
    addLit(prtGeoProfilSection1, "test2", 6, 8);
    Assert.assertFalse(helper.isSame(prtGeoProfilSection1, prtGeoProfilSection2));
  }

  public DonPrtGeoProfilSection createDonPrtGeoProfilSectionWithLit() {
    DonPrtGeoProfilSection prtGeoProfilSection1 = create();
    addLit(prtGeoProfilSection1, "test", 0, 5);
    return prtGeoProfilSection1;
  }

  private DonPrtGeoProfilSection create() {
    DonPrtGeoProfilSection profil = new DonPrtGeoProfilSection();
    List<PtProfil> pts = new ArrayList<>();
    int idx = 0;
    for (int i = 0; i < 10; i++) {
      pts.add(new PtProfil(idx++, idx * 10));
    }
    profil.setPtProfil(pts);
    return profil;
  }

  private void addLit(DonPrtGeoProfilSection profil, String name, int idxDeb, int idxFin) {
    LitNumerote lit = new LitNumerote();
    lit.setNomLit(new LitNomme(name, 0));
    lit.setLimDeb(profil.getPtProfil().get(idxDeb));
    lit.setLimDeb(profil.getPtProfil().get(idxFin));
    profil.addLitNumerote(lit);
  }
}
