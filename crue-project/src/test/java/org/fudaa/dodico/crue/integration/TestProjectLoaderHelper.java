package org.fudaa.dodico.crue.integration;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.openide.util.Exceptions;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Une classe helper pour charger des projets de tests.
 *
 * @author deniger
 */
public class TestProjectLoaderHelper {
    public static final String INTEGRATION_SC_M3_0_C10_ZIP = "/integration/Sc_M3-0_c10.zip";

    public static EMHScenario loadScenarioScM3_0_c10() {
        return loadScenarioScM3_0_c10(false);
    }

    public static EMHScenario loadScenarioScM3_0_c10(final boolean loadRunCourant) {
        final EMHProjet emhProjet = TestProjectLoaderHelper.readScM3_0_c10();
        final EMHScenarioContainer load = IntegrationHelper.load("Sc_M3-0_c10", emhProjet, loadRunCourant);
        return load.getEmhScenario();
    }

    private static EMHProjet readScM3_0_c10() {
        File target = null;
        try {
            target = CtuluLibFile.createTempDir();
        } catch (final IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        AbstractTestParent.exportZip(target, INTEGRATION_SC_M3_0_C10_ZIP);
        final File etuFile = new File(target, "EtuEx.etu.xml");
        final EMHProjet projet = IntegrationHelper.loadEtu(etuFile, TestCoeurConfig.INSTANCE_1_2);
        projet.setEtuFile(etuFile);
        Logger.getLogger(TestProjectLoaderHelper.class.getName()).log(Level.INFO, "etude unzip in " + target.getAbsolutePath());

        return projet;
    }
}
