package org.fudaa.dodico.crue.projet.report;

import org.apache.commons.io.output.WriterOutputStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.persist.AbstractReportConfigDao;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigMultiVarDao;
import org.fudaa.dodico.crue.projet.report.persist.ReportCourbeConfigReader;
import org.fudaa.dodico.crue.projet.report.transformer.KeysToStringConverter;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.fudaa.ebli.converter.TraceToStringConverter;

import java.io.File;
import java.io.StringWriter;
import java.nio.charset.Charset;

import static org.junit.Assert.assertTrue;

public class ReportReaderWriterTestHelper<D extends AbstractReportConfigDao> {


  private final ReportXstreamReaderWriter<D> readerWriter = new ReportXstreamReaderWriter<>("report-vue",
      ReportXstreamReaderWriter.VERSION,
      new ReportCourbeConfigReader(new TraceToStringConverter(),
          new KeysToStringConverter(new ReportRunKey("test", "id"))));


  public D read(String fileAsString) {
    final File file = AbstractTestParent.getFile(fileAsString);
    assertTrue(file.exists());
    return (D) readerWriter.readDao(file, new CtuluLog(), null);
  }

  public String write(D in) {
    StringWriter stringWriter = new StringWriter();
    WriterOutputStream output = new WriterOutputStream(stringWriter, Charset.defaultCharset());
    readerWriter.writeDAO(output, in, new CtuluLog(), null);
    return stringWriter.toString();
  }
}
