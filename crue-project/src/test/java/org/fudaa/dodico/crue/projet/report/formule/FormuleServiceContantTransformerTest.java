/*
GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule;

import java.util.ArrayList;
import org.fudaa.dodico.crue.projet.report.persist.FormulesDao;
import org.junit.Test;
import static org.junit.Assert.*;
/**
 *
 * @author Frederic Deniger
 */
public class FormuleServiceContantTransformerTest {

  public FormuleServiceContantTransformerTest() {
  }

  @Test
  public void testProcess() {
    FormulesDao dao = new FormulesDao();
    //ne doit pas lever d'exceptin
    new FormuleServiceContantTransformer().process(dao);
    final ArrayList<FormuleParametersExpr> formules = new ArrayList<>();
    dao.setFormules(formules);
    FormuleParametersExpr expr = new FormuleParametersExpr();
    expr.setFormule("MoySurT(\"H\")+H");
    formules.add(expr);
    new FormuleServiceContantTransformer().process(dao);
    assertEquals(1, dao.getFormules().size());
    assertEquals("MoySurTousC_T(\"H\")+H", dao.getFormules().get(0).getFormule());
  }
  @Test
  public void testProcessFormule() {
    assertEquals("toto", new FormuleServiceContantTransformer().processFormule("toto"));
    assertEquals("MoySurTousC_T(\"H\")", new FormuleServiceContantTransformer().processFormule("MoySurT(\"H\")"));
    assertEquals("MaxSurTousC_T(\"H\")", new FormuleServiceContantTransformer().processFormule("MaxSurT(\"H\")"));
    assertEquals("MinSurTousC_T(\"H\")", new FormuleServiceContantTransformer().processFormule("MinSurT(\"H\")"));
    assertEquals("SommeSurTousC_T(\"H\")", new FormuleServiceContantTransformer().processFormule("SommeSurT(\"H\")"));
    assertEquals("MoySurTousC_T(\"H\")+MaxSurTousC_T(\"H\")+MinSurTousC_T(\"H\")+SommeSurTousC_T(\"H\")", new FormuleServiceContantTransformer().
            processFormule("MoySurT(\"H\")+MaxSurT(\"H\")+MinSurT(\"H\")+SommeSurT(\"H\")"));

  }

}
