/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.integration;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.PredicateUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.common.ErrorBilanTester;
import org.fudaa.dodico.crue.comparaison.ExecuteComparaison;
import org.fudaa.dodico.crue.comparaison.ExecuteComparaisonResult;
import org.fudaa.dodico.crue.comparaison.config.ConfComparaisonConteneur;
import org.fudaa.dodico.crue.comparaison.io.ReaderConfig;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.result.OrdResDynamicPropertyFilter;
import org.fudaa.dodico.crue.projet.*;
import org.fudaa.dodico.crue.test.AbstractTestParent;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 * @author deniger
 */
public class  IntegrationHelper {
  public final static Logger LOGGER = Logger.getLogger(IntegrationHelper.class.getName());

  public static EMHProjet loadEtu(final File etuFile, final CoeurConfigContrat crueVersion) {
    final Crue10FileFormatFactory.VersionResult fileFormatResult = Crue10FileFormatFactory.findVersion(etuFile);
    assertFalse(fileFormatResult.getLog().containsErrorOrSevereError());
    final Crue10FileFormat<EMHProjet> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ETU, crueVersion);
    final CtuluLog log = new CtuluLog();
    final boolean valide = fileFormat.isValide(etuFile, log);
    if (!valide) {
      log.printResume();
    }
    assertTrue(valide);
    assertNotNull(crueVersion.getCrueConfigMetier());
    final EMHProjet project = fileFormat.read(etuFile, log, new CrueDataImpl(crueVersion.getCrueConfigMetier())).getMetier();
    project.getInfos().setXmlVersion(fileFormat.getVersionName());
    project.setPropDefinition(crueVersion);
    assertEquals(crueVersion.getXsdVersion(), project.getInfos().getXmlVersion());
    return project;
  }

  public static EMHScenarioContainer load(final String nom, final EMHProjet projet, final boolean loadRunCourant) {
    LOGGER.log(Level.FINE, "loading scenario {0}", nom);
    final ManagerEMHScenario managerEMHScenario = projet.getScenario(nom);
    final CrueOperationResult<EMHScenarioContainer> loadScenario = loadScenario(managerEMHScenario, projet, loadRunCourant);
    testErrorsManager(loadScenario.getLogs());
    return loadScenario.getResult();
  }

  public static void exportToCrue10(final File targetDir, final EMHProjet projet, final String nomScenarioFrom, final String nomScenarioTo,
                                    final String nomFichierTo, final boolean compare,
                                    final int nbDiff) {
    LOGGER.log(Level.FINE, "Export '{''}' en Crue10 sous le nom '{''}'{0}, {1}", new Object[]{nomScenarioFrom, nomScenarioTo});
    final ManagerEMHScenario managerScenarioFrom = projet.getScenario(nomScenarioFrom);
    ScenarioLoader loader = new ScenarioLoader(managerScenarioFrom, projet, projet.getCoeurConfig());
    ScenarioLoaderOperation result = loader.load(managerScenarioFrom.getRunCourant());
    testErrorsManager(result.getLogs());
    final EMHScenario scenarioFrom = result.getResult();
    final ScenarioExporterCrue10 toCrue10 = new ScenarioExporterCrue10(new File(targetDir, nomFichierTo).getAbsolutePath(),
        result.getResult(),
        projet.getCoeurConfig(),null);
    assertFalse(toCrue10.export().containsFatalError());
    final ManagerEMHScenario scenarioTo = projet.getScenario(nomScenarioTo);
    loader = new ScenarioLoader(scenarioTo, projet, projet.getCoeurConfig());
    result = loader.loadRunCourant();
    testErrorsManager(result.getLogs());
    if (compare) {
      innerTestComparison(projet, nbDiff, scenarioFrom, result.getResult());
    }
  }

  public static Collection<String> compare(final EMHProjet projet, final EMHScenario scenario1, final EMHScenario scenario2) {
    final ReaderConfig readerConfig = new ReaderConfig();
    final ConfComparaisonConteneur read = readerConfig.read(ReaderConfig.class.getResource("default-comparaison.xml"));
    final OrdResDynamicPropertyFilter filter = new OrdResDynamicPropertyFilter(scenario1.getOrdResScenario(),
        scenario2.getOrdResScenario());
    final ExecuteComparaison comp = new ExecuteComparaison(read, projet.getPropDefinition(), filter, -1, -1);
    final List<ExecuteComparaisonResult> launch = comp.launch(scenario1, scenario2, PredicateUtils.truePredicate());
    Collections.sort(launch);
    final Collection<String> comparisonIds = new HashSet<>();
    for (final ExecuteComparaisonResult executeComparaisonResult : launch) {
      if (executeComparaisonResult.getLog() != null && executeComparaisonResult.getLog().containsErrorOrSevereError()) {
        final String resume = executeComparaisonResult.getLog().getResume();
        LOGGER.warning(resume);
        fail(StringUtils.abbreviate(resume, 160));
      }
      if (executeComparaisonResult.getRes() != null) {
        final boolean contains = CollectionUtils.isNotEmpty(executeComparaisonResult.getRes().getFils());
        if (contains && !executeComparaisonResult.getRes().isSame()) {
          comparisonIds.add(executeComparaisonResult.getId());
        }
      }
    }
    return comparisonIds;
  }

  public static List<ExecuteComparaisonResult> compareResult(final EMHProjet projet, final EMHScenario scenario1, final EMHScenario scenario2) {
    final ReaderConfig readerConfig = new ReaderConfig();
    final ConfComparaisonConteneur read = readerConfig.read(ReaderConfig.class.getResource("default-comparaison.xml"));
    final OrdResDynamicPropertyFilter filter = new OrdResDynamicPropertyFilter(scenario1.getOrdResScenario(),
        scenario2.getOrdResScenario());
    final ExecuteComparaison comp = new ExecuteComparaison(read, projet.getPropDefinition(), filter, -1, -1);
    final List<ExecuteComparaisonResult> launch = comp.launch(scenario1, scenario2, PredicateUtils.truePredicate());
    Collections.sort(launch);
    return launch;
  }

  public static CrueOperationResult<EMHScenarioContainer> loadScenario(final ManagerEMHScenario managerEMHScenario, final EMHProjet projet) {
    return loadScenario(managerEMHScenario, projet, true);
  }

  public static void exportToCrue9(final File targetDir, final EMHProjet projet, final String nomScenarioFrom, final String nomScenarioTo, final String nomFichierTo,
                                   final Collection<String> idWithDifferences) {
    final ManagerEMHScenario managerScenarioFrom = projet.getScenario(nomScenarioFrom);
    ScenarioLoader loader = new ScenarioLoader(managerScenarioFrom, projet, projet.getCoeurConfig());
    ScenarioLoaderOperation resultat = loader.load(managerScenarioFrom.getRunCourant());
    final EMHScenario scenarioFrom = resultat.getResult();
    testErrorsManager(resultat.getLogs());
    assertFalse(resultat.getLogs().containsFatalError());
    final ScenarioExporterCrue9 toCrue9 = new ScenarioExporterCrue9(resultat.getResult(), managerScenarioFrom,
        projet.getPropDefinition());
    toCrue9.exportFor(new File(targetDir, nomFichierTo).getAbsolutePath());
    testErrorsManager(toCrue9.getErrorManager());
    final ManagerEMHScenario scenarioTo = projet.getScenario(nomScenarioTo);
    loader = new ScenarioLoader(scenarioTo, projet, projet.getCoeurConfig());
    resultat = loader.loadRunCourant();
    testErrorsManager(resultat.getLogs());
    assertFalse(resultat.getLogs().containsFatalError());
    if (idWithDifferences != null) {
      innerTestComparison(projet, idWithDifferences, scenarioFrom, resultat.getResult());
    }
  }

  public static void exportToCrue9(final File targetDir, final EMHProjet projet, final String nomScenarioFrom, final String nomScenarioTo, final String nomFichierTo,
                                   final boolean compare,
                                   final int nbDiff) {
    if (LOGGER.isLoggable(Level.FINE)) {
      LOGGER.log(Level.FINE, "Export '{''}' en Crue9 sous le nom '{''}'{0}, {1}", new Object[]{nomScenarioFrom, nomScenarioTo});
    }
    final ManagerEMHScenario managerScenarioFrom = projet.getScenario(nomScenarioFrom);
    ScenarioLoader loader = new ScenarioLoader(managerScenarioFrom, projet, projet.getCoeurConfig());
    ScenarioLoaderOperation result = loader.load(managerScenarioFrom.getRunCourant());
    testErrorsManager(result.getLogs());
    final EMHScenario scenarioFrom = result.getResult();
    final ScenarioExporterCrue9 toCrue9 = new ScenarioExporterCrue9(result.getResult(), managerScenarioFrom, projet.getPropDefinition());
    toCrue9.exportFor(new File(targetDir, nomFichierTo).getAbsolutePath());
    assertFalse(toCrue9.getErrorManager().containsFatalError());
    final ManagerEMHScenario managerScenarioTo = projet.getScenario(nomScenarioTo);
    loader = new ScenarioLoader(managerScenarioTo, projet, projet.getCoeurConfig());
    result = loader.loadRunCourant();
    testErrorsManager(result.getLogs());
    assertFalse(result.getLogs().containsFatalError());
    if (compare) {
      innerTestComparison(projet, nbDiff, scenarioFrom, result.getResult());
    }
  }

  public static void exportToCrue10(final File targetDir, final EMHProjet projet, final String nomScenarioFrom, final String nomScenarioTo,
                                    final String nomFichierTo,
                                    final Collection<String> comparisonResult) {
    LOGGER.log(Level.FINE, "Export '{''}' en Crue10 sous le nom '{''}'{0}, {1}", new Object[]{nomScenarioFrom, nomScenarioTo});
    final ManagerEMHScenario managerScenarioFrom = projet.getScenario(nomScenarioFrom);
    ScenarioLoader loader = new ScenarioLoader(managerScenarioFrom, projet, projet.getCoeurConfig());
    ScenarioLoaderOperation result = loader.load(managerScenarioFrom.getRunCourant());
    testErrorsManager(result.getLogs());
    final EMHScenario scenarioFrom = result.getResult();
    final ScenarioExporterCrue10 toCrue10 = new ScenarioExporterCrue10(new File(targetDir, nomFichierTo).getAbsolutePath(),
        result.getResult(), projet.getCoeurConfig(),null);
    assertFalse(toCrue10.export().containsFatalError());
    final ManagerEMHScenario scenarioTo = projet.getScenario(nomScenarioTo);
    loader = new ScenarioLoader(scenarioTo, projet, projet.getCoeurConfig());
    result = loader.loadRunCourant();
    testErrorsManager(result.getLogs());
    if (comparisonResult != null) {
      innerTestComparison(projet, comparisonResult, scenarioFrom, result.getResult());
    }
  }

  public static void testScenario(final EMHProjet projet, final String scenarioNom, final ErrorBilanTester bilan) {
    final ManagerEMHScenario scenario = projet.getScenario(scenarioNom);
    assertNotNull(scenario);
    final CrueOperationResult<EMHScenarioContainer> result = loadScenario(scenario, projet);
    bilan.testResultat(result.getLogs(), scenarioNom);
  }

  public static EMHScenarioContainer loadRun(final String nom, final EMHProjet projet, final int idxOfRun) {
    LOGGER.log(Level.FINE, "loading scenario {0}", nom);
    final ManagerEMHScenario managerEMHScenario = projet.getScenario(nom);
    final ScenarioLoader loader = new ScenarioLoader(managerEMHScenario, projet, projet.getCoeurConfig());
    final EMHRun run = managerEMHScenario.getListeRuns().get(idxOfRun);
    assertNotNull(run);
    LOGGER.log(Level.FINE, "loading run '{''}' of scenario '{''}'{0}, {1}", new Object[]{run.getNom(), nom});
    final ScenarioLoaderOperation result = loader.load(run);
    testErrorsManager(result.getLogs());
    return new EMHScenarioContainer(managerEMHScenario, result.getResult());
  }

  public static CrueOperationResult<EMHScenarioContainer> loadScenario(final ManagerEMHScenario managerEMHScenario, final EMHProjet projet,
                                                                       final boolean loadRunCourant) {
    final ScenarioLoader loader = new ScenarioLoader(managerEMHScenario, projet, projet.getCoeurConfig());
    final EMHRun runCourant = managerEMHScenario.getRunCourant();
    ScenarioLoaderOperation result = null;
    if (loadRunCourant && runCourant != null) {
      if (LOGGER.isLoggable(Level.FINE)) {
        LOGGER.log(Level.FINE, "testAll loading current run of scenario {0}", managerEMHScenario.getNom());
      }
      result = loader.load(runCourant);
    } else {
      if (LOGGER.isLoggable(Level.FINE)) {
        LOGGER.log(Level.FINE, "testAll loading without run scenario {0}", managerEMHScenario.getNom());
      }
      result = loader.load(null);
    }
    return new CrueOperationResult<>(new EMHScenarioContainer(managerEMHScenario, result.getResult()),
        result.getLogs());
  }

  public static void innerTestComparison(final EMHProjet projet, final int nbDiff, final EMHScenario scenarioFrom, final EMHScenario scenarioTo) {
    LOGGER.log(
        Level.FINE, "La comparaison des scenarios '{''}' et '{''}' doit d\u00e9tecter diff\u00e9rences{0}, {1}", new Object[]{scenarioFrom.getNom(), scenarioTo.getNom()});
    LOGGER.log(Level.FINE, "doit d\u00e9tecter {0} diff\u00e9rences", nbDiff);
    final Collection<String> compare = compare(projet, scenarioFrom, scenarioTo);
    assertEquals(
        "comparaison de " + scenarioFrom.getNom() + " et de " + scenarioTo.getNom() + " Differences obtenues:" + StringUtils.join(
            compare, ", "),
        nbDiff, compare.size());
  }

  public static void innerTestComparison(final EMHProjet projet, final Collection<String> idWithDifferences, final EMHScenario scenarioFrom,
                                         final EMHScenario scenarioTo) {
    LOGGER.log(Level.FINE, "La comparaison des scenarios '{''}' et '{''}' doit d\u00e9tecter diff\u00e9rences{0}, {1}",
        new Object[]{scenarioFrom.getNom(), scenarioTo.getNom()});
    LOGGER.log(Level.FINE, "doit d\u00e9tecter '{''}' diff\u00e9rences{0}", idWithDifferences);
    final Collection<String> idWithDifferencesFound = compare(projet, scenarioFrom, scenarioTo);
    final boolean isSame = CollectionUtils.isEqualCollection(idWithDifferencesFound, idWithDifferences);
    if (!isSame) {
      LOGGER.log(Level.SEVERE, "expected={0}", StringUtils.join(idWithDifferences, ", "));
      LOGGER.log(Level.SEVERE, "found   ={0}", StringUtils.join(idWithDifferencesFound, ", "));
    }
    assertTrue("comparaison de " + scenarioFrom.getNom() + " et de " + scenarioTo.getNom(), isSame);
  }

  public static void testErrorsManager(final CtuluLogGroup manager) {
    if (manager.containsFatalError()) {
      final String description = manager.getDesci18n();
      AbstractTestParent.printResume(manager);
      final List<CtuluLogGroup> groups = manager.getGroups();
      for (final CtuluLogGroup ctuluLogGroup : groups) {
        AbstractTestParent.printResume(ctuluLogGroup);
        final List<CtuluLogGroup> ssgroups = ctuluLogGroup.getGroups();
        for (final CtuluLogGroup ctuluLogGroup2 : ssgroups) {
          AbstractTestParent.printResume(ctuluLogGroup2);
        }
      }
      LOGGER.fine(description);
      fail(StringUtils.abbreviate(description, 160));
    }
  }
}
