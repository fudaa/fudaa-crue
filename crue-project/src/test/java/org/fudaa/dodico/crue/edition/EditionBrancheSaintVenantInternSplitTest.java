package org.fudaa.dodico.crue.edition;

import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.edition.bean.BrancheSplitContent;
import org.fudaa.dodico.crue.integration.TestProjectLoaderHelper;
import org.fudaa.dodico.crue.metier.emh.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class EditionBrancheSaintVenantInternSplitTest {
  @Test
  public void testCreateContent() {
    final EMHScenario scenario = TestProjectLoaderHelper.loadScenarioScM3_0_c10();
    String brancheNameToSplit = "Br_B1";
    String sectionNameAmont = "St_Prof11";
    String sectionNameToSplit = "St_PROF7";
    EditionBrancheSaintVenantInternSplit split = new EditionBrancheSaintVenantInternSplit(TestCoeurConfig.INSTANCE.getCrueConfigMetier());

    final EMHBrancheSaintVenant branche = (EMHBrancheSaintVenant) scenario.getIdRegistry().findByName(brancheNameToSplit);
    final CatEMHSection sectionAmont = (CatEMHSection) scenario.getIdRegistry().findByName(sectionNameAmont);
    final CatEMHSection sectionToSplit = (CatEMHSection) scenario.getIdRegistry().findByName(sectionNameToSplit);

    final CrueOperationResult<BrancheSplitContent> contentWithErrorCantSplitOnSectionAmont = split.createContent(branche, sectionAmont);
    assertNull(contentWithErrorCantSplitOnSectionAmont.getResult());

    assertEquals("EditionBrancheSplit.CreateContent.LogContent", contentWithErrorCantSplitOnSectionAmont.getLogs().getLogs().get(0).getDesc());
    assertEquals(brancheNameToSplit, contentWithErrorCantSplitOnSectionAmont.getLogs().getLogs().get(0).getDescriptionArgs()[0]);
    assertEquals("EditionBrancheSplit.SectionIsSectionAmont", contentWithErrorCantSplitOnSectionAmont.getLogs().getLogs().get(0).getRecords().get(0).getMsg());

    final CrueOperationResult<BrancheSplitContent> contentCorrect = split.createContent(branche, sectionToSplit);
    assertFalse(contentCorrect.getLogs().containsSomething());
    final BrancheSplitContent brancheSplitContent = contentCorrect.getResult();
    assertEquals(brancheNameToSplit, brancheSplitContent.brancheNameAmontToSplit);
    assertEquals(sectionNameToSplit, brancheSplitContent.sectionNameToSplitOn);

    assertEquals("Br_0001", brancheSplitContent.newBrancheNameAval);
    assertEquals("Nd_0001", brancheSplitContent.newNoeudName);
    assertEquals(2, brancheSplitContent.sections.size());

    //les sections qui seront dans la nouvelle branche
    assertEquals("St_B1_00450", brancheSplitContent.sections.get(0).nom);
    assertEquals(50, brancheSplitContent.sections.get(0).abscisseHydraulique, 1e-3);
    assertEquals("St_PROF6A", brancheSplitContent.sections.get(1).nom);
    assertEquals(100, brancheSplitContent.sections.get(1).abscisseHydraulique, 1e-3);
  }

  @Test
  public void testSplit() {
    final EMHScenario scenario = TestProjectLoaderHelper.loadScenarioScM3_0_c10();
    String brancheNameToSplit = "Br_B1";
    String sectionNameToSplit = "St_PROF7";

    EditionBrancheSaintVenantInternSplit split = new EditionBrancheSaintVenantInternSplit(TestCoeurConfig.INSTANCE.getCrueConfigMetier());

    final EMHBrancheSaintVenant brancheAmont = (EMHBrancheSaintVenant) scenario.getIdRegistry().findByName(brancheNameToSplit);

    final CatEMHSection sectionToSplit = (CatEMHSection) scenario.getIdRegistry().findByName(sectionNameToSplit);
    final CrueOperationResult<BrancheSplitContent> contentCorrect = split.createContent(brancheAmont, sectionToSplit);
    assertFalse(contentCorrect.getLogs().containsSomething());

    split.split(scenario, contentCorrect.getResult());

    CatEMHNoeud newNodeCreated = (CatEMHNoeud) scenario.getIdRegistry().findByName("Nd_0001");
    assertNotNull(newNodeCreated);
    CatEMHBranche newBrancheCreated = (CatEMHBranche) scenario.getIdRegistry().findByName("Br_0001");
    assertNotNull(newBrancheCreated);
    EMHSectionIdem newSectionCreated = (EMHSectionIdem) scenario.getIdRegistry().findByName("St_0001_Am");
    assertNotNull(newSectionCreated);

    //test des noeuds
    assertTrue(brancheAmont.getNoeudAval() == newNodeCreated);
    assertEquals("Nd_0001", brancheAmont.getNoeudAvalNom());
    assertEquals("Nd_N1", brancheAmont.getNoeudAmontNom());

    assertEquals("Nd_0001", newBrancheCreated.getNoeudAmontNom());
    assertEquals("Nd_N2", newBrancheCreated.getNoeudAvalNom());

    //test des sections de la branche amont
    assertEquals(400, brancheAmont.getLength(), 1e-3);
    assertEquals(9, brancheAmont.getSections().size());
    assertEquals(sectionNameToSplit, brancheAmont.getSections().get(8).getEmhNom());
    assertEquals(EnumPosSection.AVAL, brancheAmont.getSections().get(8).getPos());

    //test des sections de la nouvelle branche
    assertEquals(100, newBrancheCreated.getLength(), 1e-3);
    assertEquals(3, newBrancheCreated.getSections().size());

    assertEquals("St_0001_Am", newBrancheCreated.getListeSections().get(0).getEmhNom());
    assertEquals(newBrancheCreated.getNom(), newBrancheCreated.getListeSections().get(0).getEmh().getBranche().getNom());
    assertEquals(0, newBrancheCreated.getListeSections().get(0).getXp(), 1e-3);
    assertEquals(EnumPosSection.AMONT, newBrancheCreated.getListeSections().get(0).getPos());
    assertEquals(0.5, ((RelationEMHSectionDansBrancheSaintVenant) newBrancheCreated.getListeSections().get(0)).getCoefPond(), 1e-3);
    assertEquals(0, ((RelationEMHSectionDansBrancheSaintVenant) newBrancheCreated.getListeSections().get(0)).getCoefConv(), 1e-3);
    assertEquals(0, ((RelationEMHSectionDansBrancheSaintVenant) newBrancheCreated.getListeSections().get(0)).getCoefDiv(), 1e-3);

    assertEquals("St_B1_00450", newBrancheCreated.getListeSections().get(1).getEmhNom());
    assertEquals(50, newBrancheCreated.getListeSections().get(1).getXp(), 1e-3);
    assertEquals(EnumPosSection.INTERNE, newBrancheCreated.getListeSections().get(1).getPos());
    assertEquals(newBrancheCreated.getNom(), newBrancheCreated.getListeSections().get(1).getEmh().getBranche().getNom());

    assertEquals("St_PROF6A", newBrancheCreated.getListeSections().get(2).getEmhNom());
    assertEquals(100, newBrancheCreated.getListeSections().get(2).getXp(), 1e-3);
    assertEquals(EnumPosSection.AVAL, newBrancheCreated.getListeSections().get(2).getPos());
    assertEquals(newBrancheCreated.getNom(), newBrancheCreated.getListeSections().get(2).getEmh().getBranche().getNom());
  }
}
