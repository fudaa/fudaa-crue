/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.select;

import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.*;
import org.fudaa.dodico.crue.projet.select.SelectableFinder.SelectedItems;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

/**
 * @author Chris
 */
public class SelectableFinderTest {
  private final static EMHProjet PROJET = new EMHProjet();
  private final static ManagerEMHScenario SCENARIO1 = new ManagerEMHScenario("Sc_Scénario 1");
  private final static ManagerEMHScenario SCENARIO2 = new ManagerEMHScenario("Sc_Scénario 2");
  private final static ManagerEMHScenario SCENARIO3 = new ManagerEMHScenario("Sc_Scénario 3");
  private final static ManagerEMHScenario SCENARIO4 = new ManagerEMHScenario("Sc_Scénario 4");
  private final static ManagerEMHModeleBase MODELE1 = new ManagerEMHModeleBase("Mo_Modèle 1");
  private final static ManagerEMHModeleBase MODELE2 = new ManagerEMHModeleBase("Mo_Modèle 2");
  private final static ManagerEMHModeleBase MODELE3 = new ManagerEMHModeleBase("Mo_Modèle 3");
  private final static ManagerEMHModeleBase MODELE4 = new ManagerEMHModeleBase("Mo_Modèle 4");
  private final static ManagerEMHSousModele SOUS_MODELE1 = new ManagerEMHSousModele("Sm_Sous-modèle 1");
  private final static ManagerEMHSousModele SOUS_MODELE2 = new ManagerEMHSousModele("Sm_Sous-modèle 2");
  private final static ManagerEMHSousModele SOUS_MODELE3 = new ManagerEMHSousModele("Sm_Sous-modèle 3");
  private final static FichierCrue FICHIER1 = new FichierCrue("Fichier 1", "Path 1", CrueFileType.DC);
  private final static FichierCrue FICHIER2 = new FichierCrue("Fichier 2", "Path 2", CrueFileType.DC);
  private final static FichierCrue FICHIER3 = new FichierCrue("Fichier 3", "Path 3", CrueFileType.DC);
  private final static FichierCrue FICHIER4 = new FichierCrue("Fichier 4", "Path 4", CrueFileType.DC);
  private final static FichierCrue FICHIER5 = new FichierCrue("Fichier 5", "Path 5", CrueFileType.DC);
  private final static FichierCrue FICHIER6 = new FichierCrue("Fichier 6", "Path 6", CrueFileType.DC);

  static {
    SCENARIO1.addManagerFils(MODELE1);
    SCENARIO2.addManagerFils(MODELE2);
    SCENARIO3.addManagerFils(MODELE3);
    SCENARIO4.addManagerFils(MODELE3);

    MODELE1.addManagerFils(SOUS_MODELE1);
    MODELE2.addManagerFils(SOUS_MODELE2);
    MODELE3.addManagerFils(SOUS_MODELE2);
    MODELE4.addManagerFils(SOUS_MODELE3);

    MODELE1.setListeFichiers(Arrays.asList(FICHIER1, FICHIER2));
    MODELE2.setListeFichiers(Collections.singletonList(FICHIER2));
    MODELE3.setListeFichiers(Collections.singletonList(FICHIER3));

    SOUS_MODELE1.setListeFichiers(Collections.singletonList(FICHIER4));
    SOUS_MODELE2.setListeFichiers(Collections.singletonList(FICHIER5));
    SOUS_MODELE3.setListeFichiers(Collections.singletonList(FICHIER6));

    PROJET.addScenario(SCENARIO1);
    PROJET.addScenario(SCENARIO2);
    PROJET.addScenario(SCENARIO3);
    PROJET.addScenario(SCENARIO4);

    PROJET.addBaseModele(MODELE1);
    PROJET.addBaseModele(MODELE2);
    PROJET.addBaseModele(MODELE3);
    PROJET.addBaseModele(MODELE4);

    PROJET.addBaseSousModele(SOUS_MODELE1);
    PROJET.addBaseSousModele(SOUS_MODELE2);
    PROJET.addBaseSousModele(SOUS_MODELE3);
  }

  @Test
  public void testNotUsed() {
    final SelectableFinder finder = new SelectableFinder();
    finder.setProject(PROJET);
    SelectedItems items = finder.getNotUsed(Collections.singletonList(MODELE4), true);
    assertContains(items, MODELE4, SOUS_MODELE3);
    assertContainsFile(items, FICHIER6);
    items = finder.getNotUsed(Collections.singletonList(MODELE4), false);
    assertContains(items, MODELE4);
    assertContainsFile(items);

    items = finder.getNotUsed(Collections.singletonList(SCENARIO1), true);
    assertContains(items, SCENARIO1, MODELE1, SOUS_MODELE1);
    assertContainsFile(items, FICHIER1, FICHIER4);

    items = finder.getNotUsed(Collections.singletonList(SCENARIO2), true);
    assertContains(items, SCENARIO2, MODELE2);
    assertContainsFile(items);

    //test tout enlever
    items = finder.getNotUsed(Arrays.asList(SCENARIO1, SCENARIO2, SCENARIO3, SCENARIO4, MODELE4), true);
    assertContains(items, SCENARIO1, SCENARIO2, SCENARIO3, SCENARIO4, MODELE1, MODELE2, MODELE3, MODELE4, SOUS_MODELE1, SOUS_MODELE2, SOUS_MODELE3);
    assertContainsFile(items, FICHIER1, FICHIER2, FICHIER3, FICHIER4, FICHIER5, FICHIER6);
  }

  private <T extends ManagerEMHContainerBase> void assertContains(final SelectedItems items, final T... container) {
    Assert.assertEquals(container.length, items.getSelectedContainers().size());
    for (final T tested : container) {
      Assert.assertTrue(items.getSelectedContainers().contains(tested));
    }
  }

  private void assertContainsFile(final SelectedItems items, final FichierCrue... container) {
    Assert.assertEquals(container.length, items.getSelectedFiles().size());
    for (final FichierCrue tested : container) {
      Assert.assertTrue(items.getSelectedFiles().contains(tested));
    }
  }

  @Test
  public void testIndependantSelectionNotDeep() {
    final SelectableFinder finder = new SelectableFinder();
    finder.setProject(PROJET);

    SelectedItems items = finder.getIndependantSelection(Arrays.asList((ManagerEMHContainerBase) SCENARIO1, SCENARIO2), false);
    assertContains(items, SCENARIO1, SCENARIO2);
    assertContainsFile(items);

    items = finder.getIndependantSelection(Arrays.asList((ManagerEMHContainerBase) MODELE2, MODELE3), false);
    assertContains(items, MODELE2, MODELE3);
    assertContainsFile(items);
  }

  @Test
  public void testIndependantSelectionDeep() {
    final SelectableFinder finder = new SelectableFinder();
    finder.setProject(PROJET);

    SelectedItems items = finder.getIndependantSelection(Arrays.asList((ManagerEMHContainerBase) SCENARIO1, SCENARIO2), true);
    assertContains(items, SCENARIO1, SCENARIO2, MODELE1, MODELE2, SOUS_MODELE1);
    assertContainsFile(items, FICHIER1, FICHIER2, FICHIER4);

    items = finder.getIndependantSelection(Arrays.asList((ManagerEMHContainerBase) SCENARIO2, SCENARIO3, SCENARIO4), true);
    assertContains(items, SCENARIO2, SCENARIO3, SCENARIO4, MODELE2, MODELE3, SOUS_MODELE2);
    assertContainsFile(items, FICHIER3, FICHIER5);

    items = finder.getIndependantSelection(Arrays.asList((ManagerEMHContainerBase) SCENARIO3, SCENARIO4), true);
    assertContains(items, SCENARIO3, SCENARIO4, MODELE3);
    assertContainsFile(items, FICHIER3);

    items = finder.getIndependantSelection(Arrays.asList((ManagerEMHContainerBase) MODELE1, MODELE2, MODELE3), true);
    assertContains(items, MODELE1, MODELE2, MODELE3, SOUS_MODELE1, SOUS_MODELE2);
    assertContainsFile(items, FICHIER1, FICHIER2, FICHIER3, FICHIER4, FICHIER5);

    items = finder.getIndependantSelection(Arrays.asList((ManagerEMHContainerBase) SOUS_MODELE1, SOUS_MODELE2), true);
    assertContains(items, SOUS_MODELE1, SOUS_MODELE2);
    assertContainsFile(items, FICHIER4, FICHIER5);
  }
}
