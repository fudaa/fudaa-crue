package org.fudaa.dodico.crue.projet.report;

import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalConfig;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigLongitudinalDao;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigMultiVarDao;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ReportLongitudinalConfigTest {

  private ReportReaderWriterTestHelper<ReportConfigLongitudinalDao> helper = new ReportReaderWriterTestHelper<>();


  @Test
  public void testDuplicate() {

    ReportConfigLongitudinalDao config = helper.read("/org/fudaa/dodico/crue/projet/report/longitudinal.xml");
    ReportLongitudinalConfig duplicate = config.getConfig().duplicate();

    String initString = helper.write(config);
    String duplicateString = helper.write(new ReportConfigLongitudinalDao(duplicate));

    assertEquals(initString, duplicateString);

  }
}