/*
GPL 2
 */
package org.fudaa.dodico.crue.projet.report.formule.function;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * On teste que la configuration des formules de stats sont logiques par rapport à leur identifiant
 *
 * @author Frederic Deniger
 */
public class EnumFormuleStatTest {

  public EnumFormuleStatTest() {
  }

  /**
   * si contient max doit etre de type MAX,...
   */
  @Test
  public void testSetUpFormuleSelection() {
    final EnumFormuleStat[] values = EnumFormuleStat.values();
    int nbTested = 0;
    for (EnumFormuleStat value : values) {
      if (value.getId().contains("max")) {
        nbTested++;
        assertEquals(EnumFormuleFunction.MAX, value.getFormuleSelection());
      }
      if (value.getId().contains("min")) {
        nbTested++;
        assertEquals(EnumFormuleFunction.MIN, value.getFormuleSelection());
      }
      if (value.getId().contains("moy")) {
        nbTested++;
        assertEquals(EnumFormuleFunction.MOY, value.getFormuleSelection());
      }
      if (value.getId().contains("somme")) {
        nbTested++;
        assertEquals(EnumFormuleFunction.SUM, value.getFormuleSelection());
      }
    }
    assertEquals(nbTested, values.length);
  }

  /**
   * si contient SurTousCP, doit sur tout permanent...
   */
  @Test
  public void testSetUpTimeSelection() {
    final EnumFormuleStat[] values = EnumFormuleStat.values();
    int nbTested = 0;
    for (EnumFormuleStat value : values) {
      if (value.getId().contains("SurTousCP")) {
        nbTested++;
        assertEquals(EnumSelectionTime.ALL_PERMANENT, value.getTimeSelection());
      }
      if (value.getId().contains("SurTousCT")) {
        nbTested++;
        assertEquals(EnumSelectionTime.ALL_TRANSIENT, value.getTimeSelection());
      }
      if (value.getId().contains("SurSelectionCP")) {
        nbTested++;
        assertEquals(EnumSelectionTime.SELECTION_PERMANENT, value.getTimeSelection());
      }
      if (value.getId().contains("SurSelectionCT")) {
        nbTested++;
        assertEquals(EnumSelectionTime.SELECTION_TRANSIENT, value.getTimeSelection());
      }
    }
    assertEquals(nbTested, values.length);
  }

}
