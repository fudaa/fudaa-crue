package org.fudaa.dodico.crue.projet.select;

import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class DoublonScenarioFinderTest {
  @Test
  public void getDoublons() {
    DoublonScenarioFinder finder = new DoublonScenarioFinder();

    EMHProjet projet = new EMHProjet();
    String name = "Sc_Scenario";
    projet.addScenario(new ManagerEMHScenario(name));
    projet.addScenario(new ManagerEMHScenario(name + "Other"));
    final String doublon1Name = name + finder.getSuffixe();
    final String doublon2Name = doublon1Name + finder.getSuffixe();
    projet.addScenario(new ManagerEMHScenario(doublon1Name));
    projet.addScenario(new ManagerEMHScenario(doublon2Name));

    finder.setProject(projet);
    final List<ManagerEMHScenario> doublons = finder.getDoublons();
    assertEquals(2, doublons.size());
    assertEquals(doublon1Name, doublons.get(0).getNom());
    assertEquals(doublon2Name, doublons.get(1).getNom());
  }
}
