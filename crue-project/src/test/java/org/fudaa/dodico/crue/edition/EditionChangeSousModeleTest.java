package org.fudaa.dodico.crue.edition;

import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class EditionChangeSousModeleTest {
  @Test
  public void changeCasiers() {
    EMHSousModele source = new EMHSousModele();
    source.setNom("Sm_Source");
    EMHSousModele target = new EMHSousModele();
    target.setNom("Sm_Target");
    EMHCasierProfil casier = new EMHCasierProfil("Ca_1");
    EMHNoeudNiveauContinu node = new EMHNoeudNiveauContinu("Nd_1");
    IdRegistry registry = new IdRegistry();
    registry.register(source);
    registry.register(target);
    registry.register(casier);
    registry.register(node);
    EMHRelationFactory.addNoeudCasier(casier, node);
    EMHRelationFactory.addRelationContientEMH(source, casier);
    EMHRelationFactory.addRelationContientEMH(source, node);
    final DonPrtGeoBatiCasier infosEMH = new DonPrtGeoBatiCasier(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    casier.addInfosEMH(infosEMH);
    EditionChangeSousModele changer = new EditionChangeSousModele();
    Set<CatEMHNoeud> noeudsToMoveIfNeeded = new HashSet<>();
    Set<CatEMHNoeud> noeudsInTarget = new HashSet<>();

    //action
    EditionChangeSousModele.MoveToSousModeleResult changeRes = new EditionChangeSousModele.MoveToSousModeleResult();
    changer.changeCasiers(Arrays.asList(casier, node), target, noeudsToMoveIfNeeded, noeudsInTarget, changeRes);

    //asserts
    Assert.assertTrue(source.getCasiers().isEmpty());
    Assert.assertSame(casier, target.getCasiers().get(0));
    //infosEMH
    Assert.assertTrue(target.getInfosEMH().isEmpty());
    Assert.assertEquals(1, casier.getInfosEMH().size());
    Assert.assertSame(infosEMH, casier.getInfosEMH().get(0));

    Assert.assertTrue(changeRes.emhsMoved.contains(casier.getNom()));
  }
}
