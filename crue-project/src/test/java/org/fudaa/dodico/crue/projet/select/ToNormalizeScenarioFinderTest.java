package org.fudaa.dodico.crue.projet.select;

import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormatETU;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.ProjectNormalizableCallable;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;

public class ToNormalizeScenarioFinderTest extends AbstractTestParent {
  @Test
  public void testFindToNormalizeOrWithErrors() {
    final File exportZipInTempDir = exportZipInTempDir("/clean/Etu3-6.zip");
    final File etuFile = getEtuFileUpdated("Etu3-6", exportZipInTempDir);
    final EMHProjet projet = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE_1_2);
    Assert.assertNotNull(projet);
    final ToNormalizeScenarioFinder finder = new ToNormalizeScenarioFinder(projet);
    final List<ProjectNormalizableCallable.Result> normalizeOrWithErrors = finder.findToNormalizeOrWithErrors();
    Assert.assertEquals(1, normalizeOrWithErrors.size());
    Assert.assertEquals("Sc_TestEmpy", normalizeOrWithErrors.get(0).managerEMHScenario.getNom());
    Assert.assertEquals(ProjectNormalizableCallable.Status.TO_NORMALIZE, normalizeOrWithErrors.get(0).state);
  }
}
