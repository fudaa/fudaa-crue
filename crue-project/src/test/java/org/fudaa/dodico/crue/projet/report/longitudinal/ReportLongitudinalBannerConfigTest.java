package org.fudaa.dodico.crue.projet.report.longitudinal;

import com.thoughtworks.xstream.XStream;
import junit.framework.TestCase;
import org.fudaa.ebli.trace.TraceBox;
import org.fudaa.ebli.trace.TraceLigneModel;

import java.awt.*;

public class ReportLongitudinalBannerConfigTest extends TestCase {


  private static ReportLongitudinalBannerConfig create() {
    ReportLongitudinalBannerConfig config = new ReportLongitudinalBannerConfig();
    TraceBox traceBox = new TraceBox();
    traceBox.setVertical(true);
    traceBox.setColorText(Color.RED);
    TraceBox sectionBox = new TraceBox();
    sectionBox.setVertical(false);
    sectionBox.setColorText(Color.BLACK);
    config.setBrancheBox(traceBox);
    config.setSectionBox(sectionBox);
    config.setBrancheFont(new Font("test", 1, 2));
    config.setBrancheVisible(true);
    config.setBrancheFontColor(Color.BLACK);
    config.setSectionAmontAvalOnly(true);
    config.setSectionFontColor(Color.RED);
    config.setSectionVisible(true);
    config.setBrancheBoxLine(new TraceLigneModel(1, 2, Color.CYAN));
    config.setVerticalSpace(555);
    return config;
  }

  private static XStream createXstream() {
    XStream xstream = new XStream();
    xstream.processAnnotations(ReportLongitudinalBannerConfig.class);
    return xstream;
  }

  public void testDuplicate() {

    ReportLongitudinalBannerConfig init = create();

    ReportLongitudinalBannerConfig copy = init.duplicate();

    XStream xStream = createXstream();
    assertEquals(xStream.toXML(init), xStream.toXML(copy));

    assertNotSame(copy.getSectionBox(),init.getSectionBox());
    assertNotSame(copy.getBrancheBox(),init.getBrancheBox());

  }
}