/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.migrate;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

public class MigrateTargetDirValidatorTest extends AbstractTestParent {
  @Test
  public void testValid() {
    File testDir = null;
    try {
      testDir = createTempDir();
      final File baseDir = CtuluLibFile.createTempDir("targetDirValidator", testDir);
      final MigrateTargetDirValidator validator = new MigrateTargetDirValidator(baseDir);

      final CtuluLog log = new CtuluLog();
      Assert.assertFalse(validator.valid(null, log));
      testIsFatalError(log, "migrate.targetDir.isNull");
      log.clear();

      File targetDir = File.createTempFile("test", "targetDirValidator", testDir);
      Assert.assertFalse(validator.valid(targetDir, log));
      testIsFatalError(log, "migrate.targetDir.notDirectory");
      targetDir = CtuluLibFile.createTempDir();
      File.createTempFile("test", "targetDirValidator", targetDir);
      log.clear();

      Assert.assertFalse(validator.valid(targetDir, log));
      testIsFatalError(log, "migrate.targetDir.notEmpty");
      log.clear();

      Assert.assertFalse(validator.valid(baseDir, log));
      testIsFatalError(log, "migrate.targetDir.isChild");
      log.clear();

      targetDir = new File(new File(baseDir, "toto"), "titi");
      Assert.assertFalse(validator.valid(targetDir, log));
      testIsFatalError(log, "migrate.targetDir.isChild");
      log.clear();

      Assert.assertTrue(targetDir.mkdirs());//on cree toto/titi
      File.createTempFile("test", "targetDirValidator", targetDir);
      Assert.assertFalse(validator.valid(targetDir, log));
      testIsFatalError(log, "migrate.targetDir.notEmpty", "migrate.targetDir.isChild");
      log.clear();

      targetDir = new File(testDir, "tata");
      Assert.assertTrue(validator.valid(targetDir, log));
    } catch (final IOException e) {
      e.printStackTrace();
      Assert.fail(e.getMessage());
    } finally {
      if (testDir != null) {
        CtuluLibFile.deleteDir(testDir);
      }
    }
  }

  private void testIsFatalError(final CtuluLog log, final String... fatalErrorExpected) {
    Assert.assertEquals(fatalErrorExpected.length, log.getNbOccurence(CtuluLogLevel.SEVERE));
    final Collection<CtuluLogRecord> records = log.getRecords();
    int idx = 0;
    for (final CtuluLogRecord logRecord : records) {
      Assert.assertEquals(fatalErrorExpected[idx++], logRecord.getMsg());
    }
  }
}
