/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.integration;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.common.ErrorBilanTester;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;
import java.util.Arrays;

import static org.junit.Assert.*;

/**
 *
 * @author deniger
 */
@Category(org.fudaa.dodico.crue.test.TestIntegration.class)
public class TestIntegrationEtu60 {

  static File target;
  static EMHProjet projet;

  public TestIntegrationEtu60() {
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
    target = CtuluLibFile.createTempDir();
    AbstractTestParent.exportZip(target, "/integration/v1_1_1/Etu6-0.zip");
    final File file = new File(new File(target, "Etu6-0"), "Etu6-0.etu.xml");
    assertTrue(file.exists());
    projet = IntegrationHelper.loadEtu(file, TestCoeurConfig.INSTANCE_1_1_1);
    assertNotNull(projet);
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
    CtuluLibFile.deleteDir(target);
  }

  @Test
  public void testSc_M60_c9c9() {
    IntegrationHelper.exportToCrue9(getEtuFile().getParentFile(), projet, "Sc_M6-0_c9", "Sc_M6-0_c9c9", "M6-0_c9c9", true, 0);
    IntegrationHelper.testScenario(projet, "Sc_M6-0_c9c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_Poub_c9() {
    IntegrationHelper.exportToCrue9(getEtuFile().getParentFile(), projet, "Sc_M6-0_c9", "Sc_Poub_c9", "Poub_c9", true, 0);
    IntegrationHelper.testScenario(projet, "Sc_Poub_c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M60_c10c9() {
    IntegrationHelper.exportToCrue9(getEtuFile().getParentFile(), projet, "Sc_M6-0_c10", "Sc_M6-0_c10c9", "M6-0_c10c9",
            Arrays.asList("COM5", "DPTG1.2", "DFRT4", "COM1", "COM3", "DLHY3", "OPTR1"));
    IntegrationHelper.testScenario(projet, "Sc_M6-0_c10c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M60_c9c10() {
    IntegrationHelper.exportToCrue10(getEtuFile().getParentFile(), projet, "Sc_M6-0_c9", "Sc_M6-0_c9c10", "M6-0_c9c10",
            Arrays.asList("COM1", "COM3",
            "COM5", "OCAL4",
            "OPTI6", "OPTG4",
            "OPTR1"));// différences
    IntegrationHelper.testScenario(projet, "Sc_M6-0_c9c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M60_c10c10() {
    IntegrationHelper.exportToCrue10(getEtuFile().getParentFile(), projet, "Sc_M6-0_c10", "Sc_M6-0_c10c10", "M6-0_c10c10", true, 0);
    IntegrationHelper.testScenario(projet, "Sc_M6-0_c10c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_Poub_c10() {
    IntegrationHelper.exportToCrue10(getEtuFile().getParentFile(), projet, "Sc_M6-0_c10", "Sc_Poub_c10", "Poub_c10", true, 0);
    IntegrationHelper.testScenario(projet, "Sc_Poub_c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M60_c9() {
    IntegrationHelper.testScenario(projet, "Sc_M6-0_c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M30_c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M60_v1c9() {
    IntegrationHelper.testScenario(projet, "Sc_M6-0_v1c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M60_v2c9() {
    IntegrationHelper.testScenario(projet, "Sc_M6-0_v2c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M60_c10() {
    IntegrationHelper.testScenario(projet, "Sc_M6-0_c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M60_v3c9() {
    IntegrationHelper.testScenario(projet, "Sc_M6-0_v3c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  private File getEtuFile() {
    return projet.getInfos().getEtuFile();
  }
}
