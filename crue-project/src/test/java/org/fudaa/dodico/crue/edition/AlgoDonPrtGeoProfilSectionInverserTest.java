/*
 GPL 2
 */
package org.fudaa.dodico.crue.edition;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.emh.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Frederic Deniger
 */
public class AlgoDonPrtGeoProfilSectionInverserTest {
  public AlgoDonPrtGeoProfilSectionInverserTest() {
  }

  @Test
  public void testXtInverser() {
    AlgoDonPrtGeoProfilSectionInverser.XtInverser inverser = new AlgoDonPrtGeoProfilSectionInverser.XtInverser(50);
    assertEquals(-10d, inverser.getNewXt(60), 1e-5);
    assertEquals(0, inverser.getNewXt(50), 1e-5);
    assertEquals(50, inverser.getNewXt(0), 1e-5);
  }

  @Test
  public void testInverse() {
    assertTrue(true);
    DonPrtGeoProfilSection section = new DonPrtGeoProfilSection();
    double z = 0;
    PtProfil startSto = new PtProfil(0, z++);
    PtProfil stoMajeur = new PtProfil(10, z++);
    PtProfil majeurMineur = new PtProfil(15, z++);
    PtProfil mineurMineur = new PtProfil(17, z++);
    PtProfil mineurMajeur = new PtProfil(20, z++);
    PtProfil majeurSto = new PtProfil(120, z++);
    PtProfil stoEnd = new PtProfil(1120, z++);
    PtProfil end = new PtProfil(11120, z++);
    List<PtProfil> profils = new ArrayList<>();
    profils.add(startSto);
    profils.add(stoMajeur);
    profils.add(majeurMineur);
    profils.add(mineurMineur);
    profils.add(mineurMajeur);
    profils.add(majeurSto);
    profils.add(stoEnd);
    profils.add(end);

    CrueConfigMetier ccm = CrueConfigMetierForTest.DEFAULT;
    LitNumerote litStoMaj = new LitNumerote();
    litStoMaj.setFrot(new DonFrtStrickler());
    litStoMaj.setNomLit(ccm.getLitNomme().getStockageStart());
    litStoMaj.setLimDeb(new PtProfil(startSto.getAbscisse(), z++));
    litStoMaj.setLimFin(new PtProfil(stoMajeur.getAbscisse(), z++));

    LitNumerote litMajMineur = new LitNumerote();
    litMajMineur.setFrot(new DonFrtStrickler());
    litMajMineur.setNomLit(ccm.getLitNomme().getMajeurStart());
    litMajMineur.setLimDeb(new PtProfil(stoMajeur.getAbscisse(), z++));
    litMajMineur.setLimFin(new PtProfil(majeurMineur.getAbscisse(), z++));

    LitNumerote litMineurMineur = new LitNumerote();
    litMineurMineur.setFrot(new DonFrtStrickler());
    litMineurMineur.setNomLit(ccm.getLitNomme().getMineur());
    litMineurMineur.setLimDeb(new PtProfil(majeurMineur.getAbscisse(), z++));
    litMineurMineur.setLimFin(new PtProfil(mineurMineur.getAbscisse(), z++));

    LitNumerote litMineurMajeur = new LitNumerote();
    litMineurMajeur.setFrot(new DonFrtStrickler());
    litMineurMajeur.setNomLit(ccm.getLitNomme().getMineur());
    litMineurMajeur.setLimDeb(new PtProfil(mineurMineur.getAbscisse(), z++));
    litMineurMajeur.setLimFin(new PtProfil(mineurMajeur.getAbscisse(), z++));

    LitNumerote litMajeurSto = new LitNumerote();
    litMajeurSto.setFrot(new DonFrtStrickler());
    litMajeurSto.setNomLit(ccm.getLitNomme().getMajeurEnd());
    litMajeurSto.setLimDeb(new PtProfil(mineurMajeur.getAbscisse(), z++));
    litMajeurSto.setLimFin(new PtProfil(majeurSto.getAbscisse(), z++));

    LitNumerote litStoEnd = new LitNumerote();
    litStoEnd.setFrot(new DonFrtStrickler());
    litStoEnd.setNomLit(ccm.getLitNomme().getStockageEnd());
    litStoEnd.setLimDeb(new PtProfil(majeurSto.getAbscisse(), z++));
    litStoEnd.setLimFin(new PtProfil(stoEnd.getAbscisse(), z++));

    section.setPtProfil(profils);
    section.setLitNumerote(Arrays.asList(litStoMaj, litMajMineur, litMineurMineur, litMineurMajeur, litMajeurSto, litStoEnd));
    String comment = "test";
    section.setCommentaire(comment);
    DonPrtGeoProfilEtiquette et = new DonPrtGeoProfilEtiquette();
    et.setPoint(new PtProfil(mineurMineur.getXt(), z++));
    section.setEtiquettes(Collections.singletonList(et));

    AlgoDonPrtGeoProfilSectionInverser updater = new AlgoDonPrtGeoProfilSectionInverser(ccm);
    DonPrtGeoProfilSection inversed = updater.inverse(section);
    //on fait les tests pour s'assurer que les anciennes données ne sont pas modifiées
    assertEquals(17d, mineurMineur.getAbscisse(), 1e-5);
    assertEquals(17d, litMineurMineur.getLimFin().getAbscisse(), 1e-5);
    assertEquals(ccm.getLitNomme().getMajeurEnd(), litMajeurSto.getNomLit());
    assertEquals(comment, inversed.getCommentaire());
    assertNotNull(inversed);
    final int size = profils.size();
    assertEquals(size, inversed.getPtProfilSize());
    final List<PtProfil> newPtProfil = inversed.getPtProfil();
    for (int i = 0; i < size; i++) {
      assertEquals(profils.get(size - i - 1).getOrdonnee(), newPtProfil.get(i).getOrdonnee(), 1e-5);
    }
    assertEquals(-10000, newPtProfil.get(0).getAbscisse(), 1e-5);
    assertEquals(0, newPtProfil.get(1).getAbscisse(), 1e-5);
    assertEquals(1000, newPtProfil.get(2).getAbscisse(), 1e-5);
    assertEquals(1100, newPtProfil.get(3).getAbscisse(), 1e-5);
    assertEquals(1103, newPtProfil.get(4).getAbscisse(), 1e-5);
    assertEquals(1105, newPtProfil.get(5).getAbscisse(), 1e-5);
    final List<LitNumerote> newLits = inversed.getLitNumerote();
    assertEquals(section.getLitNumerote().size(), newLits.size());
    assertEquals(ccm.getLitNomme().getStockageStart(), newLits.get(0).getNomLit());
    assertEquals(ccm.getLitNomme().getMajeurStart(), newLits.get(1).getNomLit());
    assertEquals(ccm.getLitNomme().getMineur(), newLits.get(2).getNomLit());
    assertEquals(ccm.getLitNomme().getMineur(), newLits.get(3).getNomLit());
    assertEquals(ccm.getLitNomme().getMajeurEnd(), newLits.get(4).getNomLit());
    assertEquals(ccm.getLitNomme().getStockageEnd(), newLits.get(5).getNomLit());
    final DonPrtGeoProfilEtiquette newEtiquette = inversed.getEtiquettes().get(0);
    assertEquals(1103, newEtiquette.getPoint().getAbscisse(), 1e-5);
    assertEquals(et.getPoint().getOrdonnee(), newEtiquette.getPoint().getOrdonnee(), 1e-5);
    assertEquals(et.getTypeEtiquette(), newEtiquette.getTypeEtiquette());

    assertEquals(0, newLits.get(0).getLimDeb().getAbscisse(), 1e-5);
    assertEquals(1000, newLits.get(0).getLimFin().getAbscisse(), 1e-5);
    assertTrue(litStoEnd.getFrot() == newLits.get(0).getFrot());
  }
}
