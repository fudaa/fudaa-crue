package org.fudaa.dodico.crue.projet.report;

import org.fudaa.dodico.crue.projet.report.data.ReportRPTGCourbeKey;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigRPTGDao;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ReportRPTGConfigTest {

  private ReportReaderWriterTestHelper<ReportConfigRPTGDao> helper = new ReportReaderWriterTestHelper<>();


  @Test
  public void testDuplicate() {

    ReportRPTGConfig config = createReportConfig();


    ReportRPTGConfig duplicate = config.duplicate();

    String initString = helper.write(new ReportConfigRPTGDao(config));
    String duplicateString = helper.write(new ReportConfigRPTGDao(duplicate));

    assertEquals(initString, duplicateString);

  }

  private static ReportRPTGConfig createReportConfig() {
    ReportRPTGConfig config = new ReportRPTGConfig();
    config.setEmh("emh");
    config.setPrevious("previous");
    LabelConfig titleConfig = new LabelConfig();
    titleConfig.setText("text");
    titleConfig.setKey("key");
    config.loiLegendConfig.setTitleConfig(titleConfig);
    config.variables.add("var1");
    config.variables.add("var1");
    config.courbeconfigs.put(new ReportRPTGCourbeKey("var", true), new EGCourbePersist());
    return config;
  }
}