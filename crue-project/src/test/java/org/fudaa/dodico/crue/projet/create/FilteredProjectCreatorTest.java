/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.create;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformationDefault;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatETU;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.*;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.projet.migrate.FilteredProjectCreator;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author CANEL Christophe
 */
public class FilteredProjectCreatorTest extends AbstractTestParent {
  @Test
  public void testCreate() {
    final File exportZipInTempDir = exportZipInTempDir("/migration/etu3-ok.zip");
    final File etuFile = getEtuFileUpdated("etu3-0", exportZipInTempDir);
    Assert.assertNotNull(etuFile);
    Assert.assertTrue(etuFile.exists());

    //pour charger le projet
    final EMHProjet initialProject = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE_1_1_1);
    Assert.assertNotNull(initialProject);

    final File targetDir = createTempDir();
    CtuluLog log = new CtuluLog();
    final FilteredProjectCreator creator = new FilteredProjectCreator(initialProject, ConnexionInformationDefault.getDefault());

    final EMHProjet targetProject = creator.create(new File(targetDir, etuFile.getName()), TestCoeurConfig.INSTANCE, log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    Assert.assertNotNull(targetProject);
    this.testSenario(targetProject);
    this.testModeles(targetProject);
    this.testSousModeles(targetProject);
    final EMHProjetController controller = new EMHProjetController(targetProject, ConnexionInformationDefault.getDefault());
    controller.saveProjet(etuFile, new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE));
    final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ETU,
        TestCoeurConfig.INSTANCE);
    log = new CtuluLog();
    final boolean valide = fileFormat.isValide(etuFile, log);
    testAnalyser(log);
    Assert.assertTrue(valide);
  }

  private void testNomsFichierCrue(final String[] noms, final List<FichierCrue> fichiers) {
    Assert.assertEquals(noms.length, fichiers.size());

    final List<String> nomsFichiers = new ArrayList<>();

    for (final FichierCrue fichier : fichiers) {
      nomsFichiers.add(fichier.getNom());
    }

    for (final String nom : noms) {
      Assert.assertTrue(nomsFichiers.contains(nom));
    }
  }

  private void testSenario(final EMHProjet targetProject) {
    final List<ManagerEMHScenario> scenarios = targetProject.getListeScenarios();
    Assert.assertEquals(2, scenarios.size());

    final ManagerEMHScenario scenario = scenarios.get(0);
    Assert.assertEquals("Sc_M3-0_c10", scenario.getNom());
    Assert.assertFalse(scenario.isCrue9());
    Assert.assertTrue(scenario.isActive());

    this.testNomsFichierCrue(
        new String[]{"M3-0_c10.ocal.xml", "M3-0_c10.ores.xml", "M3-0_c10.pcal.xml", "M3-0_c10.dclm.xml", "M3-0_c10.dlhy.xml"},
        scenario.getListeFichiers().getFichiers());

    final List<ManagerEMHModeleBase> scenarioModele = scenario.getFils();
    Assert.assertEquals(1, scenarioModele.size());
    Assert.assertEquals("Mo_M3-0_c10", scenarioModele.get(0).getNom());

    final EMHInfosVersion infosScenario = scenario.getInfosVersions();
    Assert.assertEquals("Crue10", infosScenario.getType());
    Assert.assertEquals("Scénario Crue10 normé", infosScenario.getCommentaire());
    Assert.assertEquals("Balayn_P", infosScenario.getAuteurCreation());
    Assert.assertEquals(DateDurationConverter.getDate("2009-01-22T14:22:00"), infosScenario.getDateCreation());
    Assert.assertEquals(ConnexionInformationDefault.getDefault().getCurrentUser(), infosScenario.getAuteurDerniereModif());
    Assert.assertNotSame(DateDurationConverter.getDate("2009-01-22T14:22:00"), infosScenario.getDateDerniereModif());

    Assert.assertTrue(scenario.getListeRuns().isEmpty());
  }

  private void testModeles(final EMHProjet targetProject) {
    final List<ManagerEMHModeleBase> modeles = targetProject.getListeModeles();
    Assert.assertEquals(2, modeles.size());

    final ManagerEMHModeleBase modele = modeles.get(0);
    Assert.assertEquals("Mo_M3-0_c10", modele.getNom());
    Assert.assertFalse(modele.isCrue9());
    Assert.assertTrue(modele.isActive());

    this.testNomsFichierCrue(new String[]{"M3-0_c10.optg.xml", "M3-0_c10.opti.xml", "M3-0_c10.pnum.xml", "M3-0_c10.dpti.xml","M3-0_c10.optr.xml","M3-0_c10.dreg.xml"},
        modele.getListeFichiers().getFichiers());

    final List<ManagerEMHSousModele> modeleSousModele = modele.getFils();
    Assert.assertEquals(1, modeleSousModele.size());
    Assert.assertEquals("Sm_M3-0_c10", modeleSousModele.get(0).getNom());

    final EMHInfosVersion infosModele = modele.getInfosVersions();
    Assert.assertEquals("Crue10", infosModele.getType());
    Assert.assertEquals("Commentaire ou description du modèle Mo_M3-0_c10", infosModele.getCommentaire());
    Assert.assertEquals("Balayn_P", infosModele.getAuteurCreation());
    Assert.assertEquals(DateDurationConverter.getDate("2009-01-22T14:22:00"), infosModele.getDateCreation());
    Assert.assertEquals(ConnexionInformationDefault.getDefault().getCurrentUser(), infosModele.getAuteurDerniereModif());
    Assert.assertNotSame(DateDurationConverter.getDate("2009-01-22T14:22:00"), infosModele.getDateDerniereModif());
  }

  private void testSousModeles(final EMHProjet targetProject) {
    final List<ManagerEMHSousModele> sousModeles = targetProject.getListeSousModeles();
    Assert.assertEquals(1, sousModeles.size());

    final ManagerEMHSousModele sousModele = sousModeles.get(0);
    Assert.assertEquals("Sm_M3-0_c10", sousModele.getNom());
    Assert.assertFalse(sousModele.isCrue9());
    Assert.assertTrue(sousModele.isActive());

    this.testNomsFichierCrue(new String[]{"M3-0_c10.drso.xml", "M3-0_c10.dcsp.xml", "M3-0_c10.dptg.xml", "M3-0_c10.dfrt.xml"},
        sousModele.getListeFichiers().getFichiers());

    final EMHInfosVersion infosSousModele = sousModele.getInfosVersions();
    Assert.assertEquals("Crue10", infosSousModele.getType());
    Assert.assertEquals("Commentaire du sous-modèle Sm_M3-0_c10", infosSousModele.getCommentaire());
    Assert.assertEquals("Balayn_P", infosSousModele.getAuteurCreation());
    Assert.assertEquals(DateDurationConverter.getDate("2009-01-22T14:22:00"), infosSousModele.getDateCreation());
    Assert.assertEquals(ConnexionInformationDefault.getDefault().getCurrentUser(), infosSousModele.getAuteurDerniereModif());
    Assert.assertNotSame(DateDurationConverter.getDate("2009-01-22T14:22:00"), infosSousModele.getDateDerniereModif());
  }
}
