/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import com.memoire.fu.FuLib;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.integration.IntegrationHelper;
import org.fudaa.dodico.crue.integration.TestProjectLoaderHelper;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 *
 * @author deniger
 */
@Category(org.fudaa.dodico.crue.test.TestIntegration.class)
public class ChargementResultatsTest {

  static File targetDir;

  @BeforeClass
  public static void extractZip() throws IOException {
    targetDir = CtuluLibFile.createTempDir();
    targetDir.mkdirs();
    AbstractTestParent.exportZip(targetDir, TestProjectLoaderHelper.INTEGRATION_SC_M3_0_C10_ZIP);
  }

  @AfterClass
  public static void clean() {
    CtuluLibFile.deleteDir(targetDir);
  }

  @Test
  public void testGetLogFile() {
    final File f = new File(FuLib.getJavaHome(), "toTo.rptg.xml");
    final File retrieveLogName = ScenarioLoaderCrue10FilesFinder.retrieveLogName(f, CrueFileType.RPTG);
    assertEquals(new File(FuLib.getJavaHome(), "toTo.cptg.csv").getAbsolutePath(), retrieveLogName.getAbsolutePath());
  }

  @Test
  public void testLoadRes() {
    final File etu = new File(targetDir, "EtuEx.etu.xml");
    final EMHProjet loadEtu = IntegrationHelper.loadEtu(etu, TestCoeurConfig.INSTANCE_1_2);
    assertNotNull(loadEtu);
    final ManagerEMHScenario scenario = loadEtu.getScenario("Sc_M3-0_c10");
    assertNotNull(scenario);
    final EMHRun run = scenario.getRunFromScenar("RUN20090313090000");
    assertNotNull(run);
    final ScenarioLoader crue10Loader = new ScenarioLoader(
            scenario, loadEtu, TestCoeurConfig.INSTANCE_1_2);
    final ScenarioLoaderOperation load = crue10Loader.load(run);
    final CtuluLogGroup logs = load.getLogs();
    IntegrationHelper.testErrorsManager(logs);
    assertFalse(logs.containsFatalError());
    assertNotNull(load);
    final EMHScenario result = load.getResult();
    final EMHModeleBase modele = result.getModeles().get(0);
    //les resultats sont présents:
    final EMH nd1 = EMHHelper.selectEMHInRelationEMHContientByRef(modele.getConcatSousModele(), "Nd_N1", EnumCatEMH.NOEUD);
    assertNotNull(nd1);
    final ResultatCalcul res = EMHHelper.selectFirstOfClass(nd1.getInfosEMH(), ResultatCalcul.class);
    assertNotNull(res);
    //les compte-rendu
    final CompteRendusInfoEMH crs = EMHHelper.selectFirstOfClass(modele.getInfosEMH(), CompteRendusInfoEMH.class);
    assertNotNull(crs);
    final List<CompteRendu> compteRendus = new ArrayList<>(crs.getCompteRendus());
    Collections.sort(compteRendus);
    assertEquals(2, compteRendus.size());
    assertEquals("M3-0_c10.cptr.csv", compteRendus.get(0).getLogFile().getName());
    assertEquals("M3-0_c10.ccal.csv", compteRendus.get(1).getLogFile().getName());
    //l'avancement
    final CompteRenduAvancement avct = EMHHelper.selectFirstOfClass(result.getInfosEMH(), CompteRenduAvancement.class);
    assertNotNull(avct);





  }
}
