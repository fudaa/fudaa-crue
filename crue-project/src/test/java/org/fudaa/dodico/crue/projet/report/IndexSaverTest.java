/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report;

import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.fudaa.dodico.crue.test.FileTest;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

/**
 *
 * @author Frederic Deniger
 */
public class IndexSaverTest extends FileTest {

  private final String auteurCreation = "AuteurCreation";
  private final String auteurDerniereModif = "AuteurDerniereModif";

  public IndexSaverTest() {
  }

  @Test
  public void testRead() {
    final File file = AbstractTestParent.getFile("/org/fudaa/dodico/crue/projet/report/index.xml");
    assertTrue(file.exists());
    final ReportIndexReader saver = new ReportIndexReader();
    final CrueIOResu<List<ReportViewLineInfo>> read = saver.read(file);
    final List<ReportViewLineInfo> metier = read.getMetier();
    testResult(metier,
            DateDurationConverter.getDate("2012-10-09T12:02:23.853").toDateTime().getMillis(),
            DateDurationConverter.getDate("2012-10-09T12:02:33.853").toDateTime().getMillis(), "fileName 1", "fileName 2");
    file.delete();

  }

  private void testGeneralInfos(final ReportViewLineInfo line, final long creationDate, final long modificationDate) {
    assertEquals(auteurCreation, line.getAuteurCreation());
    assertEquals(auteurDerniereModif, line.getAuteurDerniereModif());
    assertEquals(creationDate, line.getDateCreation().toDateTime().getMillis());
    assertEquals(modificationDate, line.getDateDerniereModif().toDateTime().getMillis());
  }

  private void testResult(final List<ReportViewLineInfo> metier, final long creationDate, final long modificationDate, final String fileNameOne,
                          final String fileNameTwo) {
    assertEquals(2, metier.size());
    ReportViewLineInfo line = metier.get(0);
    assertEquals("Vue 1", line.getNom());
    assertEquals(fileNameOne, line.getFilename());
    assertEquals(fileNameOne, line.getCommentaire());
    testGeneralInfos(line, creationDate, modificationDate);
    line = metier.get(1);
    assertEquals("Vue 2", line.getNom());
    assertEquals(fileNameTwo, line.getFilename());
    assertEquals(fileNameTwo, line.getCommentaire());
    testGeneralInfos(line, creationDate, modificationDate);
  }
}
