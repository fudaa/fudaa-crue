/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import org.fudaa.ctulu.FileDeleteResult;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormatETU;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author CANEL Christophe
 *
 */
public class ScenarioRunCleanerTest extends AbstractTestParent {

  @Test
  public void testRunRemover() throws IOException {
    final File exportZipInTempDir = exportZipInTempDir("/remover/Etu3-0_1.1.2.zip");
    final File etuFile = getEtuFileUpdated("etu3-0", exportZipInTempDir);
    Assert.assertNotNull(etuFile);
    Assert.assertTrue(etuFile.exists());

    final EMHProjet projet = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE);
    Assert.assertNotNull(projet);

    final ScenarioRunCleaner remover = new ScenarioRunCleaner(projet);
    final List<File> dirToDelete = remover.getDirToDelete();
    Assert.assertEquals(2, dirToDelete.size());
    final File d1 = new File(exportZipInTempDir, "To_DeleteScenario").getCanonicalFile();
    final File d2 = new File(new File(exportZipInTempDir, "Sc_M3-0_c10"), "To_DeleteRun").getCanonicalFile();
    Assert.assertTrue(dirToDelete.contains(d1));
    Assert.assertTrue(dirToDelete.contains(d2));
    //
    final FileDeleteResult deleteUnusedDir = remover.deleteUnusedDir();
    Assert.assertTrue(deleteUnusedDir.isEmpty());
    Assert.assertFalse(d1.exists());
    Assert.assertFalse(d2.exists());
  }
}
