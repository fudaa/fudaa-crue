package org.fudaa.dodico.crue.edition;

import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.edition.bean.BrancheInsertContent;
import org.fudaa.dodico.crue.integration.TestProjectLoaderHelper;
import org.fudaa.dodico.crue.metier.emh.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class EditionBrancheInsertTest {
  @Test
  public void testCreateContent() {
    final EMHScenario scenario = TestProjectLoaderHelper.loadScenarioScM3_0_c10();
    final String sectionNameAmont = "St_Prof11";
    final String sectionNameAval = "St_PROF6A";
    final String sectionNameIntern = "St_PROF7";
    final EditionBrancheInsert insert = new EditionBrancheInsert(TestCoeurConfig.INSTANCE.getCrueConfigMetier());

    final CatEMHSection sectionAmont = (CatEMHSection) scenario.getIdRegistry().findByName(sectionNameAmont);
    final CatEMHSection sectionAval = (CatEMHSection) scenario.getIdRegistry().findByName(sectionNameAval);
    final CatEMHSection sectionIntern = (CatEMHSection) scenario.getIdRegistry().findByName(sectionNameIntern);

    final CrueOperationResult<BrancheInsertContent> insertResultOnIntern = insert.createContent(sectionIntern);
    assertNull(insertResultOnIntern.getResult());
    assertEquals("EditionBrancheInsert.CreateContent.LogContent", insertResultOnIntern.getLogs().getLogs().get(0).getDesc());
    assertEquals("EditionBrancheInsert.SectionMustBeAvalOrAmont", insertResultOnIntern.getLogs().getLogs().get(0).getRecords().get(0).getMsg());

    final CrueOperationResult<BrancheInsertContent> contentCorrect = insert.createContent(sectionAmont);
    assertFalse(contentCorrect.getLogs().containsSomething());
    BrancheInsertContent brancheSplitContent = contentCorrect.getResult();
    assertEquals("Nd_N1", brancheSplitContent.neoudNameToSplitOn);
    assertEquals("Br_0001", brancheSplitContent.newBrancheName);
    assertEquals("Nd_0001", brancheSplitContent.newNoeudName);

    brancheSplitContent = insert.createContent(sectionAval).getResult();
    assertEquals("Nd_N2", brancheSplitContent.neoudNameToSplitOn);
  }

  @Test
  public void testInsertAval() {
    final EMHScenario scenario = TestProjectLoaderHelper.loadScenarioScM3_0_c10();
    final String sectionNameToInsert = "St_PROF6A";

    final EditionBrancheInsert insert = new EditionBrancheInsert(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    final CatEMHSection sectionToSplit = (CatEMHSection) scenario.getIdRegistry().findByName(sectionNameToInsert);
    final CrueOperationResult<BrancheInsertContent> contentCorrect = insert.createContent(sectionToSplit);
    assertFalse(contentCorrect.getLogs().containsSomething());

    contentCorrect.getResult().newBrancheType = EnumBrancheType.EMHBrancheOrifice;
    insert.insert(scenario, contentCorrect.getResult());

    //la branche Br_0001 sera entre Br_B1 et Br_B2 avec comme noeud amont Nd_N2 et noeud aval Nd_0001
    final CatEMHNoeud newNodeCreated = (CatEMHNoeud) scenario.getIdRegistry().findByName("Nd_0001");
    assertNotNull(newNodeCreated);

    //la branche amont
    final CatEMHBranche branche1 = (CatEMHBranche) scenario.getIdRegistry().findByName("Br_B1");
    assertEquals("Nd_N1", branche1.getNoeudAmontNom());
    assertEquals("Nd_N2", branche1.getNoeudAvalNom());

    //la branche inserée
    final CatEMHBranche newBrancheCreated = (CatEMHBranche) scenario.getIdRegistry().findByName("Br_0001");
    assertNotNull(newBrancheCreated);
    assertEquals("Nd_N2", newBrancheCreated.getNoeudAmontNom());
    assertEquals(newNodeCreated.getNom(), newBrancheCreated.getNoeudAvalNom());

    //la branche aval
    final CatEMHBranche branche2 = (CatEMHBranche) scenario.getIdRegistry().findByName("Br_B2");
    assertEquals(newNodeCreated.getNom(), branche2.getNoeudAmontNom());
    assertEquals("Nd_N3", branche2.getNoeudAvalNom());
  }

  @Test
  public void testInsertAmont() {
    final EMHScenario scenario = TestProjectLoaderHelper.loadScenarioScM3_0_c10();
    final String sectionNameToInsert = "St_PROF6A";

    final EditionBrancheInsert insert = new EditionBrancheInsert(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    final CatEMHSection sectionToSplit = (CatEMHSection) scenario.getIdRegistry().findByName(sectionNameToInsert);
    final CrueOperationResult<BrancheInsertContent> contentCorrect = insert.createContent(sectionToSplit);
    assertFalse(contentCorrect.getLogs().containsSomething());

    contentCorrect.getResult().newBrancheType = EnumBrancheType.EMHBranchePdc;
    contentCorrect.getResult().addAval = false;
    insert.insert(scenario, contentCorrect.getResult());

    //la branche Br_0001 sera entre Br_B1 et Br_B2 avec comme noeud amont Nd_0001 et noeud aval Nd_N2
    final CatEMHNoeud newNodeCreated = (CatEMHNoeud) scenario.getIdRegistry().findByName("Nd_0001");
    assertNotNull(newNodeCreated);

    //la branche amont
    final CatEMHBranche branche1 = (CatEMHBranche) scenario.getIdRegistry().findByName("Br_B1");
    assertEquals("Nd_N1", branche1.getNoeudAmontNom());
    assertEquals(newNodeCreated.getNom(), branche1.getNoeudAvalNom());

    //la branche inserée
    final CatEMHBranche newBrancheCreated = (CatEMHBranche) scenario.getIdRegistry().findByName("Br_0001");
    assertNotNull(newBrancheCreated);
    assertEquals(newNodeCreated.getNom(), newBrancheCreated.getNoeudAmontNom());
    assertEquals("Nd_N2", newBrancheCreated.getNoeudAvalNom());

    //la branche aval
    final CatEMHBranche branche2 = (CatEMHBranche) scenario.getIdRegistry().findByName("Br_B2");
    assertEquals("Nd_N2", branche2.getNoeudAmontNom());
    assertEquals("Nd_N3", branche2.getNoeudAvalNom());
  }
}
