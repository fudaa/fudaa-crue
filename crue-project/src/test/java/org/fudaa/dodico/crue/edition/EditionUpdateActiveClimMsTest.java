/*
 GPL 2
 */
package org.fudaa.dodico.crue.edition;

import gnu.trove.TLongObjectHashMap;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.edition.bean.DonCLimMLineContent;
import org.fudaa.dodico.crue.io.test.ReadHelperForTest;
import org.fudaa.dodico.crue.metier.emh.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Frederic Deniger
 */
public class EditionUpdateActiveClimMsTest {
    public static EMHScenario readScenario() {

        EMHScenario emhScenario = new ReadHelperForTest(TestCoeurConfig.INSTANCE).readModeleAnsSetRelation(new CtuluLog(), "/M3-0_c9.dc",
            "/M3-0_c9.dh");
        IdRegistry.install(emhScenario);
        return emhScenario;
    }

    public EditionUpdateActiveClimMsTest() {
    }

    @Test
    public void testUpdate() {
        EMHScenario emhScenario = readScenario();
        OrdCalc ocal = emhScenario.getOrdCalcScenario().getOrdCalc().get(0);
        DonCLimM oldClim = ocal.getCalc().getlisteDCLMUserActive().get(0);
        assertNotNull(oldClim);
        final EMH nd = oldClim.getEmh();
        assertEquals("N1", nd.getNom());
        assertEquals(CalcPseudoPermNoeudQapp.class, oldClim.getClass());
        final CrueConfigMetier ccm = TestCoeurConfig.INSTANCE.getCrueConfigMetier();
        EditionUpdateActiveClimMs update = new EditionUpdateActiveClimMs(ccm);
        DonCLimM found = update.getActiveClim(oldClim.getEmh(), oldClim.getNomCalculParent());
        assertTrue(found == oldClim);
        final CalcPseudoPermNoeudNiveauContinuZimp newDclm = new CalcPseudoPermNoeudNiveauContinuZimp(ccm);
        newDclm.setEmh(nd);
        newDclm.setCalculParent(ocal.getCalc());
        List<DonCLimMLineContent> newCalcs = createEntry(nd, newDclm, ocal);
        CtuluLogGroup updateCLimMs = update.updateCLimMs(newCalcs, emhScenario);
        assertFalse(updateCLimMs.containsError() || updateCLimMs.containsFatalError());
        List<InfosEMH> infosEMH = nd.getInfosEMH();
        ocal = emhScenario.getOrdCalcScenario().getOrdCalc().get(0);
        List<DonCLimM> listeDCLMUserActive = ocal.getCalc().getlisteDCLMUserActive();
        assertTrue(infosEMH.contains(newDclm));
        assertTrue(newDclm.getEmh() != null);
        assertTrue(listeDCLMUserActive.contains(newDclm));
        assertFalse(infosEMH.contains(oldClim));
        assertFalse(listeDCLMUserActive.contains(oldClim));

        //echec mise à jour car pas le bon type de ClimMs:
        final CalcPseudoPermBrancheOrificeManoeuvre newDclmFalse = new CalcPseudoPermBrancheOrificeManoeuvre(ccm);
        newDclmFalse.setEmh(nd);
        newDclmFalse.setCalculParent(ocal.getCalc());
        newCalcs = createEntry(nd, newDclmFalse, ocal);
        updateCLimMs = update.updateCLimMs(newCalcs, emhScenario);
        assertTrue(updateCLimMs.containsError() || updateCLimMs.containsFatalError());
        assertEquals(1, updateCLimMs.getLogs().get(0).getRecords().size());
    }

    public List<DonCLimMLineContent> createEntry(final EMH nd, final DonCLimM newDclm, OrdCalc ocal) {
        List<DonCLimMLineContent> newCalcs = new ArrayList<>();
        TLongObjectHashMap<List<DonCLimM>> newValues = new TLongObjectHashMap<>();
        newValues.put(nd.getUiId(), Collections.singletonList(newDclm));
        DonCLimMLineContent updated = new DonCLimMLineContent(ocal.getCalc(), newValues);
        newCalcs.add(updated);
        return newCalcs;
    }
}
