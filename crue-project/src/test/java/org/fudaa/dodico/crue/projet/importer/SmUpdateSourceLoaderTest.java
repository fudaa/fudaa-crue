package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class SmUpdateSourceLoaderTest {
  File sourceFolder;

  @Before
  public void createFolders() throws IOException {
    sourceFolder = CtuluLibFile.createTempDir();
  }

  @After
  public void deleteFolders() throws IOException {
    CtuluLibFile.deleteDir(sourceFolder);
  }

  @Test
  public void updateFrom() {
    AbstractTestParent.exportZip(sourceFolder, "/integration/Etu201.zip");
    SmUpdateSourceLoader preValidator = new SmUpdateSourceLoader(TestCoeurConfig.INSTANCE);
    final CrueOperationResult<EMHSousModele> smUpdaterData = preValidator
      .updateFrom(new File(sourceFolder, "M201-1_c9c9c10.drso.xml"));
    assertNotNull(smUpdaterData.getResult());
    assertNotNull(smUpdaterData.getLogs());
  }
}
