/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.create;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormatETU;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author CANEL Christophe
 */
public class ContainerLevelDataValidatorTest extends AbstractTestParent {
  @Test
  public void testValid() {
    final File exportZipInTempDir = exportZipInTempDir("/migration/etu3-ok.zip");
    final File etuFile = getEtuFileUpdated("etu3-0", exportZipInTempDir);
    Assert.assertNotNull(etuFile);
    Assert.assertTrue(etuFile.exists());

    final EMHProjet projet = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE_1_1_1);
    Assert.assertNotNull(projet);

    final ContainerLevelDataValidator validator = new ContainerLevelDataValidator(projet);

    CtuluLog log = validator.valid(this.getCorrectDataCrue9(), true);

    Assert.assertTrue(log.isEmpty());

    log = validator.valid(this.getIncorrectDataCrue9(), true);

    Assert.assertEquals(4, log.getNbOccurence(CtuluLogLevel.WARNING));

    List<CtuluLogRecord> records = new ArrayList<>(log.getRecords());

    Assert.assertEquals("create.levelDataValidator.scenarioNameExists", records.get(0).getMsg());
    Assert.assertEquals("create.levelDataValidator.modeleNameExists", records.get(1).getMsg());
    Assert.assertEquals("create.levelDataValidator.fichierExists", records.get(2).getMsg());
    Assert.assertEquals("create.levelDataValidator.fichierExists", records.get(3).getMsg());

    log = validator.valid(this.getCorrectDataCrue10(), true);

    Assert.assertFalse(log.containsSevereError());

    log = validator.valid(this.getIncorrectDataCrue10(), true);

    Assert.assertEquals(16, log.getNbOccurence(CtuluLogLevel.WARNING));

    records = new ArrayList<>(log.getRecords());

    Assert.assertEquals("create.levelDataValidator.scenarioNameExists", records.get(0).getMsg());
    Assert.assertEquals("create.levelDataValidator.fichierExists", records.get(1).getMsg());
    Assert.assertEquals("create.levelDataValidator.fichierExists", records.get(2).getMsg());
    Assert.assertEquals("create.levelDataValidator.fichierExists", records.get(3).getMsg());
    Assert.assertEquals("create.levelDataValidator.fichierExists", records.get(4).getMsg());
    Assert.assertEquals("create.levelDataValidator.fichierExists", records.get(5).getMsg());
    Assert.assertEquals("create.levelDataValidator.modeleNameExists", records.get(6).getMsg());
    Assert.assertEquals("create.levelDataValidator.fichierExists", records.get(7).getMsg());
    Assert.assertEquals("create.levelDataValidator.fichierExists", records.get(8).getMsg());
    Assert.assertEquals("create.levelDataValidator.fichierExists", records.get(9).getMsg());
    Assert.assertEquals("create.levelDataValidator.fichierExists", records.get(10).getMsg());
    Assert.assertEquals("create.levelDataValidator.sousModeleNameExists", records.get(11).getMsg());
    Assert.assertEquals("create.levelDataValidator.fichierExists", records.get(12).getMsg());
    Assert.assertEquals("create.levelDataValidator.fichierExists", records.get(13).getMsg());
    Assert.assertEquals("create.levelDataValidator.fichierExists", records.get(14).getMsg());
    Assert.assertEquals("create.levelDataValidator.fichierExists", records.get(15).getMsg());
  }

  private ContainerLevelData getCorrectDataCrue9() {
    final ContainerLevelData scenario = new ContainerLevelData("Sc_M2_c9", CrueLevelType.SCENARIO);
    final ContainerLevelData modele = new ContainerLevelData("Mo_M2_c9", CrueLevelType.MODELE);
    final ContainerLevelData sousModele = new ContainerLevelData("Sm_M2_c9", CrueLevelType.SOUS_MODELE);

    modele.addChildren(sousModele);

    modele.addFichierCrue(new FichierCrue("M2_c9.dc", null, CrueFileType.DC));
    modele.addFichierCrue(new FichierCrue("M2_c9.dh", null, CrueFileType.DH));

    scenario.addChildren(modele);

    return scenario;
  }

  private ContainerLevelData getIncorrectDataCrue9() {
    final ContainerLevelData scenario = new ContainerLevelData("Sc_M3_c9", CrueLevelType.SCENARIO);
    final ContainerLevelData modele = new ContainerLevelData("Mo_M3_c9", CrueLevelType.MODELE);
    final ContainerLevelData sousModele = new ContainerLevelData("Sm_M3_c9", CrueLevelType.SOUS_MODELE);

    modele.addChildren(sousModele);

    modele.addFichierCrue(new FichierCrue("M3_c9.dc", null, CrueFileType.DC));
    modele.addFichierCrue(new FichierCrue("M3_c9.dh", null, CrueFileType.DH));

    scenario.addChildren(modele);

    return scenario;
  }

  private ContainerLevelData getCorrectDataCrue10() {
    final ContainerLevelData scenario = new ContainerLevelData("Sc_M2-0_c10", CrueLevelType.SCENARIO);
    final ContainerLevelData modele = new ContainerLevelData("Mo_M2-0_c10", CrueLevelType.MODELE);
    final ContainerLevelData sousModele = new ContainerLevelData("Sm_M2-0_c10", CrueLevelType.SOUS_MODELE);

    sousModele.addFichierCrue(new FichierCrue("M2-0_c10.drso.xml", null, CrueFileType.DRSO));
    sousModele.addFichierCrue(new FichierCrue("M2-0_c10.dcsp.xml", null, CrueFileType.DCSP));
    sousModele.addFichierCrue(new FichierCrue("M2-0_c10.dptg.xml", null, CrueFileType.DPTG));
    sousModele.addFichierCrue(new FichierCrue("M2-0_c10.dfrt.xml", null, CrueFileType.DFRT));

    modele.addChildren(sousModele);

    modele.addFichierCrue(new FichierCrue("M2-0_c10.optg.xml", null, CrueFileType.OPTG));
    modele.addFichierCrue(new FichierCrue("M2-0_c10.opti.xml", null, CrueFileType.OPTI));
    modele.addFichierCrue(new FichierCrue("M2-0_c10.pnum.xml", null, CrueFileType.PNUM));
    modele.addFichierCrue(new FichierCrue("M2-0_c10.dpti.xml", null, CrueFileType.DPTI));

    scenario.addChildren(modele);

    scenario.addFichierCrue(new FichierCrue("M2-0_c10.ocal.xml", null, CrueFileType.OCAL));
    scenario.addFichierCrue(new FichierCrue("M2-0_c10.ores.xml", null, CrueFileType.ORES));
    scenario.addFichierCrue(new FichierCrue("M2-0_c10.pcal.xml", null, CrueFileType.PCAL));
    scenario.addFichierCrue(new FichierCrue("M2-0_c10.dclm.xml", null, CrueFileType.DCLM));
    scenario.addFichierCrue(new FichierCrue("M2-0_c10.dlhy.xml", null, CrueFileType.DLHY));

    return scenario;
  }

  private ContainerLevelData getIncorrectDataCrue10() {
    final ContainerLevelData scenario = new ContainerLevelData("Sc_M3-0_c10", CrueLevelType.SCENARIO);
    final ContainerLevelData modele = new ContainerLevelData("Mo_M3-0_c10", CrueLevelType.MODELE);
    final ContainerLevelData sousModele = new ContainerLevelData("Sm_M3-0_c10", CrueLevelType.SOUS_MODELE);

    sousModele.addFichierCrue(new FichierCrue("M3-0_c10.drso.xml", null, CrueFileType.DRSO));
    sousModele.addFichierCrue(new FichierCrue("M3-0_c10.dcsp.xml", null, CrueFileType.DCSP));
    sousModele.addFichierCrue(new FichierCrue("M3-0_c10.dptg.xml", null, CrueFileType.DPTG));
    sousModele.addFichierCrue(new FichierCrue("M3-0_c10.dfrt.xml", null, CrueFileType.DFRT));

    modele.addChildren(sousModele);

    modele.addFichierCrue(new FichierCrue("M3-0_c10.optg.xml", null, CrueFileType.OPTG));
    modele.addFichierCrue(new FichierCrue("M3-0_c10.opti.xml", null, CrueFileType.OPTI));
    modele.addFichierCrue(new FichierCrue("M3-0_c10.pnum.xml", null, CrueFileType.PNUM));
    modele.addFichierCrue(new FichierCrue("M3-0_c10.dpti.xml", null, CrueFileType.DPTI));

    scenario.addChildren(modele);

    scenario.addFichierCrue(new FichierCrue("M3-0_c10.ocal.xml", null, CrueFileType.OCAL));
    scenario.addFichierCrue(new FichierCrue("M3-0_c10.ores.xml", null, CrueFileType.ORES));
    scenario.addFichierCrue(new FichierCrue("M3-0_c10.pcal.xml", null, CrueFileType.PCAL));
    scenario.addFichierCrue(new FichierCrue("M3-0_c10.dclm.xml", null, CrueFileType.DCLM));
    scenario.addFichierCrue(new FichierCrue("M3-0_c10.dlhy.xml", null, CrueFileType.DLHY));

    return scenario;
  }
}
