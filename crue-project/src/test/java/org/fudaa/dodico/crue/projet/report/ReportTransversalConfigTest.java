package org.fudaa.dodico.crue.projet.report;

import org.fudaa.dodico.crue.projet.report.data.*;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigRPTGDao;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigTransversalDao;
import org.fudaa.dodico.crue.projet.report.persist.ReportTransversalConfig;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ReportTransversalConfigTest {

  private ReportReaderWriterTestHelper<ReportConfigTransversalDao> helper = new ReportReaderWriterTestHelper<>();


  @Test
  public void testDuplicate() {

    ReportTransversalConfig config = createReportConfig();


    ReportTransversalConfig duplicate = config.duplicate();

    String initString = helper.write(new ReportConfigTransversalDao(config));
    String duplicateString = helper.write(new ReportConfigTransversalDao(duplicate));

    assertEquals(initString, duplicateString);

  }

  private static ReportTransversalConfig createReportConfig() {
    ReportTransversalConfig config = new ReportTransversalConfig();
    config.setSectionName("section");
    config.addProfilYtZ(new ReportRunKey("nom1", "run1"), new ReportRunKey("nom2", "run2"));
    config.addProfilVariable(new ReportRunVariableKey(new ReportRunKey("manager", "run"), new ReportVariableKey(ReportVariableTypeEnum.EXPR, "varName")));
    config.getLoiLegendConfig().setDisplayLabels(true);
    config.getLoiLegendConfig().setLegendPosition(45);
    return config;
  }
}