package org.fudaa.dodico.crue.edition;

import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.integration.IntegrationHelper;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import static org.fudaa.ctulu.CtuluLibFile.createTempDir;
import static org.fudaa.dodico.crue.integration.IntegrationHelper.loadEtu;
import static org.junit.Assert.*;

public class EditionDeleteTest {
  @Test
  public void testDeleteCascade() throws IOException {
    File target = createTempDir();
    AbstractTestParent.exportZip(target, "/org/fudaa/dodico/crue/edition/Etu_from_scratch_2.zip");
    final File file = new File(target, "Etu_from_scratch.etu.xml");
    assertTrue(file.exists());
    EMHProjet projet = loadEtu(file, TestCoeurConfig.INSTANCE_1_2);
    assertNotNull(projet);
    final CrueOperationResult<EMHScenarioContainer> crueOperationResult = IntegrationHelper.loadScenario(projet.getScenario("Sc_merge_from_scratch"), projet);
    final EMHScenario emhScenario = crueOperationResult.getResult().getEmhScenario();
//    Br_15-BarrageFilEau
    Map<String, EMH> emhByNom = emhScenario.getIdRegistry().getEmhByNom();
    final CatEMHBranche branche = (CatEMHBranche) emhByNom.get("Br_15-BarrageFilEau");
    final CatEMHNoeud nd= (CatEMHNoeud) emhByNom.get("Nd_15");
    assertNotNull(branche);
    assertNotNull(nd);
    assertEquals(2,nd.getParents().size());
    assertEquals("Sm_aval",branche.getParent().getNom());

    final EditionDelete.RemoveBilan delete = EditionDelete.delete(Collections.singleton(branche), emhScenario, true);

    emhByNom = emhScenario.getIdRegistry().getEmhByNom();
    assertNull(emhByNom.get("Br_15-BarrageFilEau"));
    assertNotNull(emhByNom.get("Nd_15"));
    assertEquals(1,nd.getParents().size());
    assertEquals("Sm_amont",nd.getParents().get(0).getNom());

    assertEquals(1,delete.noeudRemovedSousModele.size());
    assertEquals("Nd_15",delete.noeudRemovedSousModele.get(0).firstValue);
    assertEquals("Sm_aval",delete.noeudRemovedSousModele.get(0).secondValue);
  }
}
