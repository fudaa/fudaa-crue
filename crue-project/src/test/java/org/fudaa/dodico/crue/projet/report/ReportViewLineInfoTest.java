/*
 GPL 2
 */
package org.fudaa.dodico.crue.projet.report;

import org.fudaa.dodico.crue.common.ConnexionInformationDefault;
import org.fudaa.dodico.crue.common.ConnexionInformationFixed;
import org.joda.time.LocalDateTime;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class ReportViewLineInfoTest {

  public ReportViewLineInfoTest() {
  }

  @Test
  public void testCloned() {
    ReportViewLineInfo info = new ReportViewLineInfo();
    info.setCommentaire("commentaire");
    info.setDateCreation(new LocalDateTime());
    info.setAuteurCreation("Tester");
    ReportViewLineInfo cloned = info.clone();
    assertEquals(info.getCommentaire(), cloned.getCommentaire());
    assertEquals(info.getDateCreation(), cloned.getDateCreation());

    ConnexionInformationFixed ci = new ConnexionInformationFixed(new ConnexionInformationDefault());
    info.updateEdited(ci);
    assertEquals(info.getAuteurDerniereModif(), ci.getCurrentUser());
    assertEquals(info.getDateDerniereModif(), ci.getCurrentDate());
  }

  @Test
  public void testCopy() {
    ReportViewLineInfo info = new ReportViewLineInfo();
    info.setCommentaire("commentaire");
    info.setDateCreation(new LocalDateTime(0));
    info.setAuteurCreation("Tester");
    info.setAuteurDerniereModif("modifier");
    info.setDateDerniereModif(new LocalDateTime(10));
    info.setFilename("filename");

    ConnexionInformationFixed ci = new ConnexionInformationFixed(new ConnexionInformationDefault());
    assertNotSame(info.getAuteurCreation(), ci.getCurrentUser());
    assertNotSame(info.getAuteurDerniereModif(), ci.getCurrentUser());
    ReportViewLineInfo copy = info.copy(ci);
    assertEquals(info.getCommentaire(), copy.getCommentaire());
    assertEquals(ci.getCurrentUser(), copy.getAuteurCreation());
    assertEquals(ci.getCurrentUser(), copy.getAuteurDerniereModif());
    assertEquals(ci.getCurrentDate(), copy.getDateCreation());
    assertEquals(ci.getCurrentDate(), copy.getDateDerniereModif());
    assertNull(copy.getFilename());
  }
}
