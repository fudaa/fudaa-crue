/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.integration;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.common.ErrorBilanTester;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;
import java.util.Arrays;

import static org.junit.Assert.*;

/**
 *
 * @author deniger
 */
@Category(org.fudaa.dodico.crue.test.TestIntegration.class)
public class TestIntegrationEtu50 {

  static File target;
  static EMHProjet projet;

  public TestIntegrationEtu50() {
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
    target = CtuluLibFile.createTempDir();
    AbstractTestParent.exportZip(target, "/integration/v1_1_1/Etu5-0.zip");
    final File file = new File(new File(target, "Etu5-0"), "Etu5-0.etu.xml");
    assertTrue(file.exists());
    projet = IntegrationHelper.loadEtu(file, TestCoeurConfig.INSTANCE_1_1_1);
    assertNotNull(projet);
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
    CtuluLibFile.deleteDir(target);
  }

  @Test
  public void testSc_M50_c10c9() {
    IntegrationHelper.exportToCrue9(getEtuFile().getParentFile(), projet, "Sc_M5-0_c10", "Sc_M5-0_c10c9", "M5-0_c10c9",
            Arrays.asList("COM1", "COM3", "COM5", "PNUM1", "OPTR1"));
    IntegrationHelper.testScenario(projet, "Sc_M5-0_c10c9", new ErrorBilanTester(0, 0, 5, 1));
  }

  @Test
  public void testSc_M50_c9c9() {
    IntegrationHelper.exportToCrue9(getEtuFile().getParentFile(), projet, "Sc_M5_c9", "Sc_M5_c9c9", "M5_c9c9", true, 0);
    IntegrationHelper.testScenario(projet, "Sc_M5_c9c9", new ErrorBilanTester(0, 0, 5, 1));
  }

  @Test
  public void testSc_M50_c9c10() {
    IntegrationHelper.exportToCrue10(getEtuFile().getParentFile(), projet, "Sc_M5-0_c9", "Sc_M5-0_c9c10", "M5-0_c9c10", Arrays.asList("COM1", "COM3",
            "COM5", "OCAL4",
            "OPTI6", "OPTG4",
            "OPTR1"));// différences
    IntegrationHelper.testScenario(projet, "Sc_M5-0_c9c10", new ErrorBilanTester(0, 0, 4, 1));
  }

  @Test
  public void testSc_M50_c10c10() {
    IntegrationHelper.exportToCrue10(getEtuFile().getParentFile(), projet, "Sc_M5-0_c10", "Sc_M5-0_c10c10", "M5-0_c10c10", true, 0);// différences
    IntegrationHelper.testScenario(projet, "Sc_M5-0_c10c10", new ErrorBilanTester(0, 0, 4, 1));
  }

  @Test
  public void testSc_Poub_c10() {
    IntegrationHelper.exportToCrue10(getEtuFile().getParentFile(), projet, "Sc_M5-0_c9", "Sc_Poub_c10", "Poub_c10", Arrays.asList("COM1", "COM3", "COM5",
            "OCAL4", "OPTI6",
            "OPTG4", "OPTR1"));// différences
    IntegrationHelper.testScenario(projet, "Sc_Poub_c10", new ErrorBilanTester(0, 0, 4, 1));
  }

  @Test
  public void testSc_M5_c9() {
    IntegrationHelper.testScenario(projet, "Sc_M5_c9", new ErrorBilanTester(0, 0, 5, 1));
  }

  @Test
  public void testSc_M50_c9() {
    IntegrationHelper.testScenario(projet, "Sc_M5-0_c9", new ErrorBilanTester(0, 0, 5, 1));
  }

  @Test
  public void testSc_M50_c10() {
    IntegrationHelper.testScenario(projet, "Sc_M5-0_c10", new ErrorBilanTester(0, 0, 4, 1));
  }

  private File getEtuFile() {
    return projet.getInfos().getEtuFile();
  }
}
