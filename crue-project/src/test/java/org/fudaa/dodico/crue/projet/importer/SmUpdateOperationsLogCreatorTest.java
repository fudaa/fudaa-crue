package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.edition.EditionCreateEMH;
import org.fudaa.dodico.crue.metier.emh.EMHNoeudNiveauContinu;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOperationsData;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOptions;
import org.junit.Assert;
import org.junit.Test;

public class SmUpdateOperationsLogCreatorTest {
  @Test
  public void extractBrutLog() {
    SmScenarioBuilderForTest source = new SmScenarioBuilderForTest("1");
    SmScenarioBuilderForTest target = new SmScenarioBuilderForTest("2");
    SmUpdateOperationsDataExtractor extractor = new SmUpdateOperationsDataExtractor(target.scenario);
    final SmUpdateOperationsData data = extractor.extract(source.getSousModele(0), new SmUpdateOptions(), target.getSousModele(0).getNom());

    final SmUpdateOperationsLogCreator smUpdateOperationsLogCreator = new SmUpdateOperationsLogCreator(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    final CtuluLogGroup ctuluLogGroup = smUpdateOperationsLogCreator.extractBrutLog(data);
    Assert.assertEquals(7,ctuluLogGroup.getLogs().size());
    int i=0;
    Assert.assertEquals("sm.updater.typeChangedLog",ctuluLogGroup.getLogs().get(i++).getDesc());
    Assert.assertEquals("sm.updater.ProfilSectionlostLog",ctuluLogGroup.getLogs().get(i++).getDesc());
    Assert.assertEquals("sm.updater.BrancheslostLog",ctuluLogGroup.getLogs().get(i++).getDesc());
    Assert.assertEquals("sm.updater.sectionNotAttachedLog",ctuluLogGroup.getLogs().get(i++).getDesc());
    Assert.assertEquals("sm.updater.emhToUpdateLogGroup",ctuluLogGroup.getLogs().get(i++).getDesc());
    Assert.assertEquals("sm.updater.emhToAddLogGroup",ctuluLogGroup.getLogs().get(i++).getDesc());
    Assert.assertEquals("sm.updater.emhInTargetOnlyLog",ctuluLogGroup.getLogs().get(i++).getDesc());
  }

  @Test
  public void extractInTargetOnly() {
    SmScenarioBuilderForTest source = new SmScenarioBuilderForTest("1");
    SmScenarioBuilderForTest target = new SmScenarioBuilderForTest("1");
    final EMHSousModele targetSousModele = target.getSousModele(0);
    EMHNoeudNiveauContinu inTargetOnly = new EMHNoeudNiveauContinu("Nd_InTargetOnly");
    EMHRelationFactory.addRelationContientEMH(targetSousModele, inTargetOnly);
    EditionCreateEMH.addInScenario(targetSousModele, inTargetOnly, target.scenario);
    SmUpdateOperationsDataExtractor extractor = new SmUpdateOperationsDataExtractor(target.scenario);
    final SmUpdateOperationsData data = extractor.extract(source.getSousModele(0), new SmUpdateOptions(), targetSousModele.getNom());

    final SmUpdateOperationsLogCreator smUpdateOperationsLogCreator = new SmUpdateOperationsLogCreator(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    final CtuluLog ctuluLog = smUpdateOperationsLogCreator.generateLogInTargetOnly(data);
    Assert.assertEquals("sm.updater.emhInTargetOnlyLog", ctuluLog.getDesc());
    Assert.assertEquals(1, ctuluLog.getRecords().size());
    Assert.assertEquals("sm.updater.emhInTargetOnlyLogItem", ctuluLog.getRecords().get(0).getMsg());
    Assert.assertEquals(inTargetOnly.getNom(), ctuluLog.getRecords().get(0).getArgs()[0]);
  }

  @Test
  public void generateLogAddedEMH() {
    SmScenarioBuilderForTest source = new SmScenarioBuilderForTest("1");
    SmScenarioBuilderForTest target = new SmScenarioBuilderForTest("2");
    SmUpdateOperationsDataExtractor extractor = new SmUpdateOperationsDataExtractor(target.scenario);
    final SmUpdateOperationsData data = extractor.extract(source.getSousModele(0), new SmUpdateOptions(), target.getSousModele(0).getNom());

    final CtuluLog ctuluLog = new SmUpdateOperationsLogCreator(TestCoeurConfig.INSTANCE.getCrueConfigMetier()).generateLogAddedEMH(data);
    Assert.assertEquals("sm.updater.emhToAddLogGroup", ctuluLog.getDesc());
    Assert.assertEquals(source.getEmhToAddOrUpdate(0).size(), ctuluLog.getRecords().size());
  }
}
