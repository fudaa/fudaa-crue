/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.migrate;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author CANEL Christophe
 */
public class ChildrenFilesTesterTest {
  @Test
  public void testValid() {
    File testDir = null;

    try {
      testDir = CtuluLibFile.createTempDir().getCanonicalFile();
      final File baseDir = CtuluLibFile.createTempDir("targetDirValidator", testDir);
      final List<File> files = new ArrayList<>();
      final CtuluLog log = new CtuluLog();
      final ChildrenFilesTester tester = new ChildrenFilesTester(baseDir);

      files.add(new File(baseDir, "titi"));
      files.add(new File(files.get(0), "tutu"));

      Assert.assertTrue(tester.test(files, log));
      Assert.assertEquals(0, log.getRecords().size());

      files.add(new File(testDir, "toto"));
      files.add(new File(files.get(2), "tata"));

      Assert.assertFalse(tester.test(files, log));
      Assert.assertEquals(2, log.getRecords().size());

      int index = 0;
      for (final CtuluLogRecord record : log.getRecords()) {
        Assert.assertEquals("migrate.isChildrenFilesTester.isNotChild", record.getMsg());

        if (index == 0) {
          Assert.assertEquals("toto", record.getArgs()[0]);
        } else {
          Assert.assertEquals("tata", record.getArgs()[0]);
        }

        index++;
      }
    } catch (final IOException e) {
      Assert.fail(e.getMessage());
    } finally {
      if (testDir != null) {
        CtuluLibFile.deleteDir(testDir);
      }
    }
  }
}
