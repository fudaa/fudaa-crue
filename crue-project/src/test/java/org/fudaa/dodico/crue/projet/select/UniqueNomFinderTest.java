/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.select;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.ScenarioBuilderForTest;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author deniger
 */
public class UniqueNomFinderTest {
    public UniqueNomFinderTest() {
    }

    @Test
    public void testFindProfifCasier() {
        UniqueNomFinder finder = new UniqueNomFinder();
        List<String> names = Arrays.asList("Pc_0001_001", "Pc_0001_002");
        assertEquals("Pc_0001_003", finder.findUniqueNameProfilCasier(names, "Pc_0001"));
        assertEquals("Pc_0001_003", finder.findUniqueNameProfilCasier(names, "Pc_0001_"));
    }

    @Test
    public void testFindMaxIdentifier() {
        UniqueNomFinder finder = new UniqueNomFinder();
        List<String> names = Arrays.asList("1", "002", "a100");
        assertEquals(2, finder.findMaxIdentifier(names, ""));
        names = Arrays.asList("Br_", "Br_1002", "Br_a100");
        assertEquals(1002, finder.findMaxIdentifier(names, "Br_"));
    }

    @Test
    public void testFindMaxIdentifierScenario() {
        UniqueNomFinder finder = new UniqueNomFinder();
        EMHScenario scenario = ScenarioBuilderForTest.createDefaultScenario(CrueConfigMetierForTest.DEFAULT);
        List<EMH> emhs = scenario.getAllSimpleEMH();
        assertEquals(1, finder.findMaxIdentifier(emhs, CruePrefix.P_BRANCHE));
        assertEquals(2, finder.findMaxIdentifier(emhs, CruePrefix.P_NOEUD));
        assertEquals(0, finder.findMaxIdentifier(emhs, CruePrefix.P_SECTION));
    }

    @Test
    public void testFindNewName() {
        UniqueNomFinder finder = new UniqueNomFinder();
        List<String> names = Arrays.asList("1", "002", "a100");
        assertEquals("0003", finder.findNewName(names, ""));
        names = Arrays.asList("Br_", "Br_1002", "Br_a100");
        assertEquals("Br_1003", finder.findNewName(names, "Br_"));
    }

    @Test
    public void testFindNewNameScenario() {
        UniqueNomFinder finder = new UniqueNomFinder();
        EMHScenario scenario = ScenarioBuilderForTest.createDefaultScenario(CrueConfigMetierForTest.DEFAULT);
        assertEquals("Br_0002", finder.findNewName(EnumCatEMH.BRANCHE, scenario));
        assertEquals("Nd_0003", finder.findNewName(EnumCatEMH.NOEUD, scenario));
        assertEquals("St_0001", finder.findNewName(EnumCatEMH.SECTION, scenario));
    }

    @Test
    public void testFindUniqueName() {
        UniqueNomFinder finder = new UniqueNomFinder();

        List<String> names = Arrays.asList("Br_", "Br_1002", "Br_a100");
        assertEquals("Br_1003", finder.findUniqueName(names, "Br_1002"));
        assertEquals("Br_1", finder.findUniqueName(names, "Br_1"));
        assertEquals("Br_1003", finder.findUniqueName(names, "Br_"));
        assertEquals("Br_a100_0001", finder.findUniqueName(names, "Br_a100"));

        names = Arrays.asList("1", "002", "a100");
        assertEquals("0003", finder.findUniqueName(names, "0003"));
        assertEquals("a100_0001", finder.findUniqueName(names, "a100"));
    }

    @Test
    public void testFindUniqueNameScenario() {
        UniqueNomFinder finder = new UniqueNomFinder();
        EMHScenario scenario = ScenarioBuilderForTest.createDefaultScenario(CrueConfigMetierForTest.DEFAULT);
        assertEquals("Br_3", finder.findUniqueName(EnumCatEMH.BRANCHE, scenario, "Br_3"));
        assertEquals("Br_0002", finder.findUniqueName(EnumCatEMH.BRANCHE, scenario, "Br_1"));
    }

    @Test
    public void testFindNewNames() {
        UniqueNomFinder finder = new UniqueNomFinder();
        List<String> names = Arrays.asList("1", "002", "a100", "3");
        List<String> findNewNames = finder.findNewNames(names, "", 5);
        testList(findNewNames, "0004", "0005", "0006", "0007", "0008");
        names = Arrays.asList("Br_", "Br_0002", "Br_a100");
        findNewNames = finder.findNewNames(names, "Br_", 5);
        testList(findNewNames, "Br_0003", "Br_0004", "Br_0005", "Br_0006", "Br_0007");
    }

    @Test
    public void testFindNewNamesScenario() {
        UniqueNomFinder finder = new UniqueNomFinder();
        EMHScenario scenario = ScenarioBuilderForTest.createDefaultScenario(CrueConfigMetierForTest.DEFAULT);
        List<String> findNewNames = finder.findNewNames(EnumCatEMH.BRANCHE, scenario, 5);
        testList(findNewNames, "Br_0002", "Br_0003", "Br_0004", "Br_0005", "Br_0006");
        findNewNames = finder.findNewNames(EnumCatEMH.NOEUD, scenario, 5);
        testList(findNewNames, "Nd_0003", "Nd_0004", "Nd_0005", "Nd_0006", "Nd_0007");
    }

    private void testList(List<String> names, String... expected) {
        assertEquals(expected.length, names.size());
        int idx = 0;
        for (String string : expected) {
            assertEquals("Line " + idx, string, names.get(idx));
            idx++;
        }
    }
}
