/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.edition;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.ScenarioBuilderForTest;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;

/**
 *
 * @author deniger
 */
public class EditionCreateEMHTest {

  public EditionCreateEMHTest() {
  }

  @Test
  public void testCreateBranche() {
    final CreationDefaultValue defaultValues = new CreationDefaultValue();
    final EditionCreateEMH creator = new EditionCreateEMH(defaultValues);
    final EMHScenario scenario = ScenarioBuilderForTest.createDefaultScenario(CrueConfigMetierForTest.DEFAULT);
    scenario.addInfosEMH(new DonCLimMScenario());
    final EMHSousModele ssModele = scenario.getModeles().get(0).getSousModeles().get(0);
    final CatEMHBranche branche = creator.createBranche(ssModele, EnumBrancheType.EMHBrancheSaintVenant, null, null,
            CrueConfigMetierForTest.DEFAULT, 100);
    assert branche != null;
    assertNotNull(branche);
    assertNotNull(branche.getSectionAmont());
    assertNotNull(branche.getSectionAval());
    assertNotNull(branche.getNoeudAmont());
    assertNotNull(branche.getNoeudAval());
    assertEquals("Nd_0003", branche.getNoeudAmont().getNom());
    assertEquals("Nd_0004", branche.getNoeudAval().getNom());
    final EMHSectionProfil sectionProfil = (EMHSectionProfil) branche.getSectionAmont().getEmh();
    final DonPrtGeoProfilSection dptg = EMHHelper.selectFirstOfClass(sectionProfil.getInfosEMH(), DonPrtGeoProfilSection.class);
    assertEquals(5, dptg.getLitNumerote().size());//modification temporaire
    final LitNumerote mineur = dptg.getLitNumerote().get(2);
    assertTrue(mineur.getIsLitMineur());
    assertNotNull(mineur.getFrot());
    assertEquals("Fk_DefautMin", mineur.getFrot().getNom());
    final LitNumerote maj = dptg.getLitNumerote().get(1);
    assertFalse(maj.getIsLitMineur());
    assertTrue(maj.getIsLitActif());
    assertNotNull(maj.getFrot());
    assertEquals("FkSto_0", maj.getFrot().getNom());

    //on teste la suppression:
    EditionDelete.delete(Collections.<EMH>singletonList(branche), scenario, true);
    assertTrue(dptg.getEmh() == null);
  }
}
