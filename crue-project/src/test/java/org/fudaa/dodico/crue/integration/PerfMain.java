/*
 GPL 2
 */
package org.fudaa.dodico.crue.integration;

import java.io.File;
import java.util.Collection;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;

/**
 * Une classe pour tester les performances d'une comparaison.
 *
 * @author Frederic Deniger
 */
public class PerfMain {

  public static void main(String[] args) {
    File etu = new File("C:\\data\\Fudaa-Crue\\Data\\EtuAV_TI.FCv0.61_crue10v10.0.10\\Etu_AV_TI.etu.xml");
    EMHProjet loadEtu = IntegrationHelper.loadEtu(etu, TestCoeurConfig.INSTANCE);
    EMHScenarioContainer loadRun1 = IntegrationHelper.loadRun("Sc_AV_SSNA_1TR_c9c9", loadEtu, 0);
    EMHScenarioContainer loadRun2 = IntegrationHelper.loadRun("Sc_AV_SSNA_1TR_c9c9", loadEtu, 1);
    long to = System.nanoTime();
    System.err.println("Comparaison commencée entre " + loadRun1.getEmhScenario().getNom() + " et " + loadRun2.getEmhScenario().getNom());
    Collection<String> compare = IntegrationHelper.compare(loadEtu, loadRun1.getEmhScenario(), loadRun2.getEmhScenario());
    System.err.println("Comparaison terminé: " + StringUtils.join(compare, ", "));
    System.err.println("Temps: " + (System.nanoTime() - to) / 1e6);
    for (int i = 0; i < 100000000; i++) {
      System.err.println("finish");

    }
  }
}
