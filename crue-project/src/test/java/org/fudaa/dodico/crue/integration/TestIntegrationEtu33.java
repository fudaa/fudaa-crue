/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.integration;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.common.ErrorBilanTester;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;
import java.util.Arrays;

import static org.junit.Assert.*;

/**
 *
 * @author deniger
 */
@Category(org.fudaa.dodico.crue.test.TestIntegration.class)
public class TestIntegrationEtu33 {

  static File target;
  static EMHProjet projet;

  public TestIntegrationEtu33() {
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
    target = CtuluLibFile.createTempDir();
    AbstractTestParent.exportZip(target, "/integration/v1_1_1/Etu3-3.zip");
    final File file = new File(new File(target, "Etu3-3"), "Etu3-3.etu.xml");
    assertTrue(file.exists());
    projet = IntegrationHelper.loadEtu(file, TestCoeurConfig.INSTANCE_1_1_1);
    assertNotNull(projet);
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
    CtuluLibFile.deleteDir(target);
  }

  @Test
  public void testSc_M33_c9c9() {
    IntegrationHelper.exportToCrue9(getEtuFile().getParentFile(), projet, "Sc_M3-3_c9", "Sc_M3-3_c9c9", "M3-3_c9c9", true, 0);
    IntegrationHelper.testScenario(projet, "Sc_M3-3_c9c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M33_c10c9() {
    IntegrationHelper.exportToCrue9(getEtuFile().getParentFile(), projet, "Sc_M3-3_c10", "Sc_M3-3_c10c9", "M3-3_c10c9",
            Arrays.asList("COM5", "DPTG1.2", "DFRT4", "COM1", "COM3", "OPTR1", "DLHY3"));

    IntegrationHelper.testScenario(projet, "Sc_M3-3_c10c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M33_c9c10() {
    IntegrationHelper.exportToCrue10(getEtuFile().getParentFile(), projet, "Sc_M3-3_c9", "Sc_M3-3_c9c10", "M3-3_c9c10", Arrays.asList("COM5", "OPTG4",
            "COM1", "OPTR1",
            "COM3", "OPTI6",
            "OCAL4"));
    IntegrationHelper.testScenario(projet, "Sc_M3-3_c9c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M33_c10c10() {
    IntegrationHelper.exportToCrue10(getEtuFile().getParentFile(), projet, "Sc_M3-3_c10", "Sc_M3-3_c10c10", "M3-3_c10c10", true, 0);
    IntegrationHelper.testScenario(projet, "Sc_M3-3_c10c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M33_c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-3_c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M33_v1c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-3_v1c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M33_c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-3_c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testCompare() {
    final EMHScenarioContainer scenario1 = IntegrationHelper.load("Sc_M3-3_c9", projet, false);
    final EMHScenarioContainer scenario2 = IntegrationHelper.load("Sc_M3-3_v1c9", projet, false);
    assertEquals(0, IntegrationHelper.compare(projet, scenario1.getEmhScenario(), scenario2.getEmhScenario()).size());
  }

  private File getEtuFile() {
    return projet.getInfos().getEtuFile();
  }
}
