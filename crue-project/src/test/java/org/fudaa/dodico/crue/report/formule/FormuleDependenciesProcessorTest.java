/*
 GPL 2
 */
package org.fudaa.dodico.crue.report.formule;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.fudaa.dodico.crue.projet.report.formule.FormuleContentManagerDefault;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDependenciesContent;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDependenciesProcessor;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleDependenciesProcessorTest {

  public FormuleDependenciesProcessorTest() {
  }

  @Test
  public void testCorrect() {
    FormuleContentManagerDefault mock = new FormuleContentManagerDefault();
    //A=B+C
    //B=C+D
    //Z=A+B+C+D+Q
    mock.add("A", false, "B", "C");
    mock.add("B", false, "C", "D");
    mock.add("Z", true, "A", "B", "C", "D", "Q");
    assertFalse(mock.containsTimeDependantVariable("A"));
    assertFalse(mock.containsTimeDependantVariable("B"));
    assertTrue(mock.containsTimeDependantVariable("Z"));
    FormuleDependenciesProcessor processor = new FormuleDependenciesProcessor(mock);
    FormuleDependenciesContent compute = processor.compute(Arrays.asList("A", "B", "Z"));
    assertContained("A", compute.getUsedVariables("A"), "B", "C", "D");

    assertContained("B", compute.getUsedVariables("B"), "C", "D");
    assertContained("Z", compute.getUsedVariables("Z"), "A", "B", "C", "D", "Q");
    assertTrue(compute.getCycliqueVar().isEmpty());

    FormuleDependenciesContent.UsedBy createUsedBy = compute.createUsedBy();

    assertFalse(createUsedBy.isUsed("Z"));
    assertTrue(createUsedBy.isUsed("Q"));
    assertContained("A usedBy", createUsedBy.usedBy("A"), "Z");
    assertContained("B usedBy", createUsedBy.usedBy("B"), "A", "Z");
    assertContained("Q usedBy", createUsedBy.usedBy("Q"), "Z");
  }

  @Test
  public void testCycle() {
    FormuleContentManagerDefault mock = new FormuleContentManagerDefault();
    //A=B+D
    //B=C+E
    //D=A+Q
    mock.add("A", true, "B", "D");
    mock.add("B", false, "C", "E");
    mock.add("D", true, "A", "Q");
    assertTrue(mock.containsTimeDependantVariable("A"));
    assertFalse(mock.containsTimeDependantVariable("B"));
    assertTrue(mock.containsTimeDependantVariable("D"));
    FormuleDependenciesProcessor processor = new FormuleDependenciesProcessor(mock);
    FormuleDependenciesContent compute = processor.compute(Arrays.asList("A", "B", "D"));
    assertContained("A", compute.getUsedVariables("A"), "A", "B", "D", "C", "E", "Q");
    assertContained("B", compute.getUsedVariables("B"), "C", "E");
    assertContained("D", compute.getUsedVariables("D"), "A", "Q", "B", "D", "C", "E");

    List<String> cycliqueVar = compute.getCycliqueVar();
    assertContained("cyclique", cycliqueVar, "A", "D");

    FormuleDependenciesContent.UsedBy createUsedBy = compute.createUsedBy();

    assertFalse(createUsedBy.isUsed("Z"));
    assertTrue(createUsedBy.isUsed("Q"));
    assertContained("A usedBy", createUsedBy.usedBy("A"), "A", "D");
    assertContained("B usedBy", createUsedBy.usedBy("B"), "A", "D");
    assertContained("D usedBy", createUsedBy.usedBy("D"), "A", "D");
    assertContained("E usedBy", createUsedBy.usedBy("E"), "A", "B", "D");
  }

  public static void assertContained(String msg, Collection<String> var, String... includes) {
    assertEquals(msg, includes.length, var.size());
    for (String string : includes) {
      assertTrue(msg, var.contains(string));
    }
  }
}