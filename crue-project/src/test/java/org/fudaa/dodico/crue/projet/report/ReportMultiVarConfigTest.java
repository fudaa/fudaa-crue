package org.fudaa.dodico.crue.projet.report;

import org.fudaa.dodico.crue.projet.report.persist.ReportConfigMultiVarDao;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class ReportMultiVarConfigTest {

  private ReportReaderWriterTestHelper<ReportConfigMultiVarDao> helper = new ReportReaderWriterTestHelper<>();


  @Test
  public void testDuplicate() {

    ReportConfigMultiVarDao config = helper.read("/org/fudaa/dodico/crue/projet/report/multivar.xml");
    assertFalse(config.getConfig().getEmhs().isEmpty());
    ReportMultiVarConfig duplicate = config.getConfig().duplicate();

    String initString = helper.write(config);
    String duplicateString = helper.write(new ReportConfigMultiVarDao(duplicate));

    assertEquals(initString, duplicateString);

  }
}