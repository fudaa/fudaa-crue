package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheOrifice;
import org.fudaa.dodico.crue.metier.emh.EnumPosSection;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOperationsData;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateSourceTargetData;
import org.junit.Test;

import java.util.Collections;

import static org.fudaa.dodico.crue.projet.importer.SmUpdateBrancheSameRelationSectionPredicateTest.addSection;
import static org.junit.Assert.*;

public class SmUpdateDataDetachedSectiondentifierTest {
  @Test
  public void NoDetachedSection() {
    SmUpdateSourceTargetData sourceTargetData = new SmUpdateSourceTargetData();
    EMHBrancheOrifice source = new EMHBrancheOrifice("Br_1");
    EMHBrancheOrifice target = new EMHBrancheOrifice("Br_1");
    sourceTargetData.addBrancheLine(new SmUpdateSourceTargetData.Line<>(source, target));

    final RelationEMHSectionDansBranche source1 = addSection(source, false, "St_1", 0, EnumPosSection.AMONT);
    final RelationEMHSectionDansBranche target1 = addSection(target, false, "St_1", 0, EnumPosSection.AMONT);
    sourceTargetData.addSectionLine(new SmUpdateSourceTargetData.Line<>(source1.getEmh(), target1.getEmh()));

    final RelationEMHSectionDansBranche source2 = addSection(source, false, "St_1.1", 1, EnumPosSection.INTERNE);
    final RelationEMHSectionDansBranche target2 = addSection(target, false, "St_1.1", 2, EnumPosSection.INTERNE);
    sourceTargetData.addSectionLine(new SmUpdateSourceTargetData.Line<>(source2.getEmh(), target2.getEmh()));

    final RelationEMHSectionDansBranche source3 = addSection(source, false, "St_2", 10, EnumPosSection.AVAL);
    final RelationEMHSectionDansBranche target3 = addSection(target, false, "St_2", 12, EnumPosSection.AVAL);
    sourceTargetData.addSectionLine(new SmUpdateSourceTargetData.Line<>(source3.getEmh(), target3.getEmh()));

    SmUpdateOperationsData data = new SmUpdateOperationsData(null, null, Collections.emptyList(), sourceTargetData);
    SmUpdateDataDetachedSectiondentifier identifier = new SmUpdateDataDetachedSectiondentifier(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    final CtuluLog ctuluLog = identifier.generateLogForDetachedSection(data);

    assertTrue(ctuluLog.isEmpty());
  }

  @Test
  public void detachedSection() {
    SmUpdateSourceTargetData sourceTargetData = new SmUpdateSourceTargetData();
    EMHBrancheOrifice source = new EMHBrancheOrifice("Br_1");
    EMHBrancheOrifice target = new EMHBrancheOrifice("Br_1");
    sourceTargetData.addBrancheLine(new SmUpdateSourceTargetData.Line<>(source, target));

    final RelationEMHSectionDansBranche source1 = addSection(source, false, "St_1", 0, EnumPosSection.AMONT);
    final RelationEMHSectionDansBranche target1 = addSection(target, false, "St_1", 0, EnumPosSection.AMONT);
    sourceTargetData.addSectionLine(new SmUpdateSourceTargetData.Line<>(source1.getEmh(), target1.getEmh()));

    final RelationEMHSectionDansBranche source2 = addSection(source, false, "St_1.1", 1, EnumPosSection.INTERNE);
    final RelationEMHSectionDansBranche target2 = addSection(target, false, "St_1.1Detached", 2, EnumPosSection.INTERNE);
    sourceTargetData.addSectionLine(new SmUpdateSourceTargetData.Line<>(source2.getEmh(), target2.getEmh()));

    final RelationEMHSectionDansBranche source3 = addSection(source, false, "St_2", 10, EnumPosSection.AVAL);
    final RelationEMHSectionDansBranche target3 = addSection(target, false, "St_2", 12, EnumPosSection.AVAL);
    sourceTargetData.addSectionLine(new SmUpdateSourceTargetData.Line<>(source3.getEmh(), target3.getEmh()));

    SmUpdateOperationsData data = new SmUpdateOperationsData(null, null, Collections.emptyList(), sourceTargetData);
    SmUpdateDataDetachedSectiondentifier identifier = new SmUpdateDataDetachedSectiondentifier(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    final CtuluLog ctuluLog = identifier.generateLogForDetachedSection(data);

    assertEquals("sm.updater.sectionNotAttachedLog", ctuluLog.getDesc());
    assertEquals(1, ctuluLog.getRecords().size());
    final CtuluLogRecord ctuluLogRecord = ctuluLog.getRecords().get(0);
    assertEquals("sm.updater.sectionNotAttachedLogItem", ctuluLogRecord.getMsg());
    assertEquals(target2.getEmhNom(), ctuluLogRecord.getArgs()[0]);
    assertEquals(EnumSectionType.EMHSectionProfil.geti18n(), ctuluLogRecord.getArgs()[1]);
  }
}
