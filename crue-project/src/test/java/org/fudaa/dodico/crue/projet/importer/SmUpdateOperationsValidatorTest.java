package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOptions;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateValidationData;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SmUpdateOperationsValidatorTest {
  @Test
  public void validateAllIsUpdated() {
    SmScenarioBuilderForTest source = new SmScenarioBuilderForTest("m1", "m2");
    SmScenarioBuilderForTest target = new SmScenarioBuilderForTest("m1", "m2");
    SmUpdateOperationsValidator validator = new SmUpdateOperationsValidator(target.scenario);
    final SmBuilderForTest sousModeleM1 = source.getSmBuilders().get(0);
    final SmUpdateValidationData validate = validator
      .validate(sousModeleM1.getSousModele(), new SmUpdateOptions(true, true, true), CruePrefix.P_SS_MODELE + "m1");
    Assert.assertTrue(validate.isTargetSousModeleExisting());
    Assert.assertTrue(validate.getRequiredSectionsNotInTarget().isEmpty());
    Assert.assertTrue(validate.getTargetEmhInAnotherSousModele().isEmpty());

    final List<EMH> allSimpleEMHInSource = sousModeleM1.getAllSimpleEMHUpdatedOrAdded();
    allSimpleEMHInSource.sort(ObjetNommeByNameComparator.INSTANCE);
  }

  @Test
  public void validateAllIsUpdatedOneNoeudInOtherSousModele() {
    SmScenarioBuilderForTest source = new SmScenarioBuilderForTest("m1", "m2");
    final SmBuilderForTest sousModeleM1 = source.getSmBuilders().get(0);
    final SmBuilderForTest sousModeleM2 = source.getSmBuilders().get(1);
    final EMH ndToAddInSousModele1 = sousModeleM2.getSousModele().getAllSimpleEMH(EnumCatEMH.NOEUD).get(0);
    EMHRelationFactory.addRelationContientEMH(sousModeleM1.getSousModele(), ndToAddInSousModele1);
    SmScenarioBuilderForTest target = new SmScenarioBuilderForTest("m1", "m2");
    SmUpdateOperationsValidator validator = new SmUpdateOperationsValidator(target.scenario);
    final SmUpdateValidationData validate = validator
      .validate(sousModeleM1.getSousModele(), new SmUpdateOptions(true, true, true), CruePrefix.P_SS_MODELE + "m1");
    Assert.assertTrue(validate.isTargetSousModeleExisting());
    Assert.assertTrue(validate.getRequiredSectionsNotInTarget().isEmpty());
    Assert.assertTrue(validate.getTargetEmhInAnotherSousModele().isEmpty());

    final List<EMH> allSimpleEMHInSource = sousModeleM1.getAllSimpleEMHUpdatedOrAdded();
    allSimpleEMHInSource.sort(ObjetNommeByNameComparator.INSTANCE);
  }

  @Test
  public void allTargetEMHInAnotherSousModele() {
    //on va tenter de mettre à jour le sous-modele m2 a partir de donnees issues du sous-modele m1
    //Hors la cible contient ces emhs dans le sous-modele m1
    SmScenarioBuilderForTest source = new SmScenarioBuilderForTest("m1", "m2");
    SmScenarioBuilderForTest target = new SmScenarioBuilderForTest("m1", "m2");
    SmUpdateOperationsValidator validator = new SmUpdateOperationsValidator(target.scenario);
    final EMHSousModele sourceSousModeleM1 = source.getSousModele(0);
    final SmUpdateValidationData validate = validator
      .validate(sourceSousModeleM1, new SmUpdateOptions(true, true, true), CruePrefix.P_SS_MODELE + "m2");

    Assert.assertTrue(validate.isTargetSousModeleExisting());
    Assert.assertTrue(validate.getRequiredSectionsNotInTarget().isEmpty());


    final List<String> allEMHInSource = TransformerHelper.toNom(sourceSousModeleM1.getAllSimpleEMH(EnumCatEMH.BRANCHE,EnumCatEMH.SECTION,EnumCatEMH.CASIER)).stream().sorted().collect(Collectors.toList());
    final List<String> targetEMHInAnotherSousModele = validate.getTargetEmhInAnotherSousModele().keySet().stream().sorted().collect(Collectors.toList());
    Assert.assertEquals("Toutes les emh sources sont dans un autre sous-modele", allEMHInSource, targetEMHInAnotherSousModele);

    validate.getTargetEmhInAnotherSousModele().values().forEach(name -> Assert.assertEquals(CruePrefix.P_SS_MODELE + "m1", name));
  }

  @Test
  public void allEMHToBeCreated() {
    SmScenarioBuilderForTest source = new SmScenarioBuilderForTest("m1");
    SmScenarioBuilderForTest target = new SmScenarioBuilderForTest("m2");
    //on va tenter de mettre à jour le sous-modele m2 a partir de donnees issues du sous-modele m1
    SmUpdateOperationsValidator validator = new SmUpdateOperationsValidator(target.scenario);
    final EMHSousModele sourceSousModeleM1 = source.getSousModele(0);
    final SmUpdateValidationData validate = validator
      .validate(sourceSousModeleM1, new SmUpdateOptions(true, true, true), CruePrefix.P_SS_MODELE + "m2");

    Assert.assertTrue(validate.getRequiredSectionsNotInTarget().isEmpty());
    Assert.assertTrue(validate.getTargetEmhInAnotherSousModele().isEmpty());

    final List<String> emhInTargetOnlyExpected = TransformerHelper.toNom(target.getSousModele(0).getAllSimpleEMH());
    Collections.sort(emhInTargetOnlyExpected);
  }

  @Test
  public void sectionRequiredButNotPresent() {
    SmScenarioBuilderForTest source = new SmScenarioBuilderForTest("m1");
    SmScenarioBuilderForTest target = new SmScenarioBuilderForTest("m2");
    //on va tenter de mettre à jour le sous-modele m2 a partir de donnees issues du sous-modele m1
    SmUpdateOperationsValidator validator = new SmUpdateOperationsValidator(target.scenario);
    final EMHSousModele sourceSousModeleM1 = source.getSousModele(0);
    //les sections ne sont pas toutes importées
    final SmUpdateValidationData validate = validator
      .validate(sourceSousModeleM1, new SmUpdateOptions(false, true, true), CruePrefix.P_SS_MODELE + "m2");

    Assert.assertTrue(validate.getTargetEmhInAnotherSousModele().isEmpty());

    final List<String> requiredSectionNotInTargetExpected = TransformerHelper.toNom(source.getSousModele(0).getSections());
    requiredSectionNotInTargetExpected.remove("St_HorBranchem1");
    Collections.sort(requiredSectionNotInTargetExpected);
    Assert.assertEquals(requiredSectionNotInTargetExpected, validate.getRequiredSectionsNotInTarget());

    final List<String> emhToAddExpected = TransformerHelper.toNom(source.getSmBuilders().get(0).getAllSimpleEMHUpdatedOrAdded());
    emhToAddExpected.remove("St_HorBranchem1");
  }
}
