package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.dodico.crue.metier.emh.IdRegistry;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateSourceTargetData;
import org.junit.Test;

import static org.junit.Assert.*;

public class SmUpdateSourceTargetBuilderTest {
  @Test
  public void build() {
    SmScenarioBuilderForTest builderSource = new SmScenarioBuilderForTest("Sm_1");
    SmScenarioBuilderForTest builderTarget = new SmScenarioBuilderForTest("Sm_1");
    SmUpdateSourceTargetBuilder builder = new SmUpdateSourceTargetBuilder(builderTarget.scenario);
    IdRegistry.install(builderTarget.scenario);
    final SmUpdateSourceTargetData build = builder.build(builderSource.scenario.getAllSimpleEMH());
    final SmBuilderForTest builder1 = builderSource.getSmBuilders().get(0);

    assertEquals(1, build.getBranches().size());
    assertEquals(builder1.brancheName,build.getBranches().get(0).getSourceToImport().getNom());

    assertEquals(1, build.getCasiers().size());
    assertEquals(builder1.casierName,build.getCasiers().get(0).getSourceToImport().getNom());

    assertEquals(3, build.getSections().size());
    assertEquals(builder1.section1Name,build.getSections().get(0).getSourceToImport().getNom());
    assertEquals(builder1.section2Name,build.getSections().get(1).getSourceToImport().getNom());
    assertEquals(builder1.sectionHorBrancheName,build.getSections().get(2).getSourceToImport().getNom());

    assertEquals(4, build.getNoeuds().size());
    assertEquals(builder1.nd1Name,build.getNoeuds().get(0).getSourceToImport().getNom());
    assertEquals(builder1.nd2Name,build.getNoeuds().get(1).getSourceToImport().getNom());
    assertEquals(builder1.ndUsedByCasierName,build.getNoeuds().get(2).getSourceToImport().getNom());
    assertEquals(builder1.ndNotUSedName,build.getNoeuds().get(3).getSourceToImport().getNom());

  }
}
