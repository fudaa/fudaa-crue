/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.integration;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.common.ErrorBilanTester;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;
import java.util.Arrays;

import static org.junit.Assert.*;

/**
 *
 * @author deniger
 */
@Category(org.fudaa.dodico.crue.test.TestIntegration.class)
public class TestIntegrationEtu32 {

  static File target;
  static EMHProjet projet;

  public TestIntegrationEtu32() {
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
    target = CtuluLibFile.createTempDir();
    AbstractTestParent.exportZip(target, "/integration/v1_1_1/Etu3-2.zip");
    final File file = new File(new File(target, "Etu3-2"), "Etu3-2.etu.xml");
    assertTrue(file.exists());
    projet = IntegrationHelper.loadEtu(file, TestCoeurConfig.INSTANCE_1_1_1);
    assertNotNull(projet);
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
    CtuluLibFile.deleteDir(target);
  }

  @Test
  public void testSc_M32_c9c9() {
    IntegrationHelper.exportToCrue9(getEtuFile().getParentFile(), projet, "Sc_M3-2_c9", "Sc_M3-2_c9c9", "M3-2_c9c9", true, 0);
    IntegrationHelper.testScenario(projet, "Sc_M3-2_c9c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_Poub_c9() {
    IntegrationHelper.exportToCrue9(getEtuFile().getParentFile(), projet, "Sc_M3-2_c9", "Sc_Poub_c9", "Poub_c9", true, 0);
    IntegrationHelper.testScenario(projet, "Sc_Poub_c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M32_c10c9() {
    IntegrationHelper.exportToCrue9(getEtuFile().getParentFile(), projet, "Sc_M3-2_c10", "Sc_M3-2_c10c9", "M3-2_c10c9",
                                    Arrays.asList("DFRT4", "COM5", "COM1", "DPTG1.2", "COM3", "OPTR1", "DLHY3"));// qruis
   
    // différent
    IntegrationHelper.testScenario(projet, "Sc_M3-2_c10c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M32_c9c10() {
    IntegrationHelper.exportToCrue10(getEtuFile().getParentFile(), projet, "Sc_M3-2_c9", "Sc_M3-2_c9c10", "M3-2_c9c10",
            Arrays.asList("COM5", "COM1", "COM3", "OPTR1", "OCAL4"));
    IntegrationHelper.testScenario(projet, "Sc_M3-2_c9c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M32_c10c10() {
    IntegrationHelper.exportToCrue10(getEtuFile().getParentFile(), projet, "Sc_M3-2_c10", "Sc_M3-2_c10c10", "M3-2_c10c10", true, 0);
    IntegrationHelper.testScenario(projet, "Sc_M3-2_c10c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_Poub_c10() {
    IntegrationHelper.exportToCrue10(getEtuFile().getParentFile(), projet, "Sc_M3-2_c10", "Sc_Poub_c10", "Poub_c10", true, 0);
    IntegrationHelper.testScenario(projet, "Sc_Poub_c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M32_c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-2_c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M32_e1c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-2_e1c9", new ErrorBilanTester(1, 0, 0, 0));
  }

  @Test
  public void testSc_M32_v1c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-2_v1c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M32_c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-2_c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testComparaison() {
    final EMHScenarioContainer scenario1 = IntegrationHelper.load("Sc_M3-2_c9", projet, false);
    final EMHScenarioContainer scenario2 = IntegrationHelper.load("Sc_M3-2_v1c9", projet, false);
    assertEquals(1, IntegrationHelper.compare(projet, scenario1.getEmhScenario(), scenario2.getEmhScenario()).size());
  }

  private File getEtuFile() {
    return projet.getInfos().getEtuFile();
  }
}
