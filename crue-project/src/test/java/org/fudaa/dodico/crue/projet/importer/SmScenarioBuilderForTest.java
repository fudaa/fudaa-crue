package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;

import java.util.ArrayList;
import java.util.List;

public class SmScenarioBuilderForTest {
  EMHScenario scenario;
  List<SmBuilderForTest> smBuilders = new ArrayList<>();

  public SmScenarioBuilderForTest(String... sousModeleSuffixe) {
    scenario = new EMHScenario();
    scenario.setNom(CruePrefix.P_SCENARIO+"1");
    final EMHModeleBase modele = new EMHModeleBase();
    modele.setNom(CruePrefix.P_MODELE+"1");
    EMHRelationFactory.addRelationContientEMH(scenario, modele);
    for (String suffix : sousModeleSuffixe) {
      SmBuilderForTest smBuilder = new SmBuilderForTest(suffix);
      smBuilder.getSousModele().setNom(CruePrefix.P_SS_MODELE + suffix);
      smBuilders.add(smBuilder);
      EMHRelationFactory.addRelationContientEMH(modele, smBuilder.getSousModele());
    }
    IdRegistry.install(scenario);
  }

  public EMHScenario getScenario() {
    return scenario;
  }

  public EMHSousModele getSousModele(int i){
    return smBuilders.get(i).getSousModele();
  }

  public List<SmBuilderForTest> getSmBuilders() {
    return smBuilders;
  }

  public  List<EMH> getEmhToAddOrUpdate(int i){
    return smBuilders.get(i).getAllSimpleEMHUpdatedOrAdded();
  }
}
