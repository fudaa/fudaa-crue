package org.fudaa.dodico.crue.metier;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.integration.IntegrationHelper;
import org.fudaa.dodico.crue.metier.cloner.EMHScenarioCloner;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.IdRegistry;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.Collection;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class EMHScenarioClonerTest {

  static File target;

  public EMHScenarioClonerTest() {
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
    target = CtuluLibFile.createTempDir();

  }

  @Test
  public void testCloneEtu5_0() {
    testClone("/integration/v1_1_1/Etu5-0.zip", "Etu5-0", "Etu5-0.etu.xml", "Sc_M5-0_c10");
  }

  public void testClone(String zipFile, String subFolder, String etuFile, String scenarioName) {
    File targetDir = new File(target, subFolder);
    targetDir.mkdir();
    AbstractTestParent.exportZip(targetDir, zipFile);
    final File file = new File(new File(targetDir, subFolder), etuFile);
    assertTrue(file.exists());
    EMHProjet projet = IntegrationHelper.loadEtu(file, TestCoeurConfig.INSTANCE_1_1_1);
    EMHScenario loadScenario = IntegrationHelper.load(scenarioName, projet, false).getEmhScenario();

    EMHScenario clonedScenario = new EMHScenarioCloner().copy(loadScenario);
    Assert.assertNull(clonedScenario.getIdRegistry());
    IdRegistry.install(clonedScenario);

    Map<Long, EMH> emhById = loadScenario.getIdRegistry().getEmhById();
    emhById.forEach((id, loadedEmh) -> {
      EMH clonedEMH=clonedScenario.getIdRegistry().getEmh(id);
      Assert.assertNotSame(loadedEmh,clonedEMH);
      testRelationsEMH(loadedEmh, clonedEMH);
//      testInfosEMH(loadedEmh, clonedEMH);

    });

    Collection<String> compare = IntegrationHelper.compare(projet, loadScenario, clonedScenario);
    Assert.assertEquals(0, compare.size());
    CtuluLibFile.deleteDir(targetDir);
  }

  private static void testInfosEMH(EMH loadedEmh, EMH clonedEMH) {
    int nbInfo = loadedEmh.getInfosEMH().size();
    Assert.assertEquals(nbInfo, clonedEMH.getInfosEMH().size());
    for (int i = 0; i < nbInfo; i++) {
      Assert.assertNotSame(loadedEmh.getInfosEMH().get(i), clonedEMH.getInfosEMH().get(i));
      Assert.assertNotSame(loadedEmh.getInfosEMH().get(i), clonedEMH.getInfosEMH().get(i));
      Assert.assertEquals(loadedEmh.getInfosEMH().get(i).getType(), clonedEMH.getInfosEMH().get(i).getType());
      Assert.assertEquals(loadedEmh.getInfosEMH().get(i).getCatType(), clonedEMH.getInfosEMH().get(i).getCatType());
      Assert.assertEquals(loadedEmh.getInfosEMH().get(i).getActuallyActive(), clonedEMH.getInfosEMH().get(i).getActuallyActive());
      Assert.assertSame(clonedEMH, clonedEMH.getInfosEMH().get(i).getEmh());

    }
  }
  private static void testRelationsEMH(EMH loadedEmh, EMH clonedEMH) {
    int nbRelations = loadedEmh.getRelationEMH().size();
    Assert.assertEquals(nbRelations, clonedEMH.getRelationEMH().size());
    for (int i = 0; i < nbRelations; i++) {
      Assert.assertNotSame(loadedEmh.getRelationEMH().get(i), clonedEMH.getRelationEMH().get(i));
      Assert.assertNotSame(loadedEmh.getRelationEMH().get(i), clonedEMH.getRelationEMH().get(i));
      Assert.assertEquals(loadedEmh.getRelationEMH().get(i).getType(), clonedEMH.getRelationEMH().get(i).getType());
      Assert.assertEquals(loadedEmh.getRelationEMH().get(i).getEmhId(), clonedEMH.getRelationEMH().get(i).getEmhId());
      Assert.assertEquals(loadedEmh.getRelationEMH().get(i).getEmh().getUiId(), clonedEMH.getRelationEMH().get(i).getEmh().getUiId());

    }
  }


  @AfterClass
  public static void tearDownClass() throws Exception {
    CtuluLibFile.deleteDir(target);
  }
}
