/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.rename;

import com.memoire.fu.FuLib;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.*;
import org.fudaa.dodico.crue.projet.rename.RenameManager.NewNameInformations;
import org.fudaa.dodico.crue.projet.select.SelectableFinder;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

/**
 * @author Chris
 */
public class RenameManagerTest {
  private final static EMHProjet PROJET = new EMHProjet();
  private final static ManagerEMHScenario SCENARIO1 = new ManagerEMHScenario("Sc_Scénario 1");
  private final static ManagerEMHScenario SCENARIO2 = new ManagerEMHScenario("Sc_Scénario 2");
  private final static ManagerEMHScenario SCENARIO3 = new ManagerEMHScenario("Sc_Scénario 3");
  private final static ManagerEMHModeleBase MODELE1 = new ManagerEMHModeleBase("Mo_Modèle 1");
  private final static ManagerEMHModeleBase MODELE2 = new ManagerEMHModeleBase("Mo_Modèle 2");
  private final static ManagerEMHModeleBase MODELE3 = new ManagerEMHModeleBase("Mo_Modèle 3");
  private final static ManagerEMHSousModele SOUS_MODELE1 = new ManagerEMHSousModele("Sm_Sous-modèle 1");
  private final static ManagerEMHSousModele SOUS_MODELE2 = new ManagerEMHSousModele("Sm_Sous-modèle 2");
  private final static ManagerEMHSousModele SOUS_MODELE3 = new ManagerEMHSousModele("Sm_Sous-modèle 3");
  private final static ManagerEMHSousModele SOUS_MODELE4 = new ManagerEMHSousModele("Sm_Sous-modèle 4");
  private final static FichierCrue FICHIER1 = new FichierCrue("Fichier 1.dc", "Path 1", CrueFileType.DC);
  private final static FichierCrue FICHIER2 = new FichierCrue("Fichier 2.dc", "Path 2", CrueFileType.DC);
  private final static FichierCrue FICHIER3 = new FichierCrue("Fichier 3.dc", "Path 3", CrueFileType.DC);
  private final static FichierCrue FICHIER4 = new FichierCrue("Fichier 4.drso.xml", "Path 4", CrueFileType.DRSO);
  private final static FichierCrue FICHIER5 = new FichierCrue("Fichier 5.drso.xml", "Path 5", CrueFileType.DRSO);
  private final static FichierCrue FICHIER6 = new FichierCrue("Fichier 6.drso.xml", "Path 6", CrueFileType.DRSO);
  private final static FichierCrue FICHIER7 = new FichierCrue("Fichier 7.drso.xml", "Path 7", CrueFileType.DRSO);
  private final static String NEW_RADICAL1 = "Nouveau";
  private final static String NEW_RADICAL2 = "Fichier 2";
  private final static String NEW_RADICAL3 = "Scénario 2";
  private final static String NEW_RADICAL4 = "Modèle 2";

  static {
    SCENARIO1.addManagerFils(MODELE1);
    SCENARIO2.addManagerFils(MODELE2);
    SCENARIO3.addManagerFils(MODELE3);

    MODELE1.addManagerFils(SOUS_MODELE1);
    MODELE1.addManagerFils(SOUS_MODELE3);
    MODELE2.addManagerFils(SOUS_MODELE2);
    MODELE3.addManagerFils(SOUS_MODELE2);
    MODELE3.addManagerFils(SOUS_MODELE4);

    MODELE1.setListeFichiers(Arrays.asList(FICHIER1, FICHIER2));
    MODELE2.setListeFichiers(Collections.singletonList(FICHIER2));
    MODELE3.setListeFichiers(Collections.singletonList(FICHIER3));

    SOUS_MODELE1.setListeFichiers(Collections.singletonList(FICHIER4));
    SOUS_MODELE2.setListeFichiers(Collections.singletonList(FICHIER5));
    SOUS_MODELE3.setListeFichiers(Collections.singletonList(FICHIER6));
    SOUS_MODELE4.setListeFichiers(Collections.singletonList(FICHIER7));

    PROJET.addScenario(SCENARIO1);
    PROJET.addScenario(SCENARIO2);
    PROJET.addScenario(SCENARIO3);

    PROJET.addBaseModele(MODELE1);
    PROJET.addBaseModele(MODELE2);
    PROJET.addBaseModele(MODELE3);

    PROJET.addBaseSousModele(SOUS_MODELE1);
    PROJET.addBaseSousModele(SOUS_MODELE2);

    PROJET.setEtuFile(new File(FuLib.getJavaTmp(), "etu.etu.xml"));

    PROJET.getInfos().setBaseFichiersProjets(Arrays.asList(FICHIER1, FICHIER2, FICHIER3, FICHIER4, FICHIER5));
  }

  @Test
  public void testRenameManager() {
    final RenameManager manager = new RenameManager();
    final SelectableFinder finder = new SelectableFinder();
    manager.setProject(PROJET);
    finder.setProject(PROJET);

    NewNameInformations infos = manager.getNewNames(FICHIER1, NEW_RADICAL1);

    assertEquals(0, infos.newNameContainers.size());
    assertEquals(1, infos.newNameFiles.size());
    assertTrue(infos.newNameFiles.containsKey(NEW_RADICAL1 + "." + FICHIER1.getType().getExtension()));
    assertEquals(FICHIER1, infos.newNameFiles.get(NEW_RADICAL1 + "." + FICHIER1.getType().getExtension()));

    assertFalse(manager.canUseNewNames(infos).containsErrors());

    infos = manager.getNewNames(FICHIER1, NEW_RADICAL2);

    assertEquals(0, infos.newNameContainers.size());
    assertEquals(1, infos.newNameFiles.size());
    assertTrue(infos.newNameFiles.containsKey(NEW_RADICAL2 + "." + FICHIER1.getType().getExtension()));
    assertEquals(FICHIER1, infos.newNameFiles.get(NEW_RADICAL2 + "." + FICHIER1.getType().getExtension()));

    assertTrue(manager.canUseNewNames(infos).containsErrors());

    infos = manager.getNewNames(SCENARIO3, finder.getIndependantSelection(Collections.singletonList((ManagerEMHContainerBase) SCENARIO3),
        false), NEW_RADICAL1, false);

    assertEquals(1, infos.newNameContainers.size());
    assertEquals(0, infos.newNameFiles.size());
    assertTrue(infos.newNameContainers.containsKey(SCENARIO3.getLevel().getPrefix() + NEW_RADICAL1));
    assertEquals(SCENARIO3, infos.newNameContainers.get(SCENARIO3.getLevel().getPrefix() + NEW_RADICAL1));

    assertFalse(manager.canUseNewNames(infos).containsErrors());

    infos = manager.getNewNames(SCENARIO3, finder.getIndependantSelection(Collections.singletonList((ManagerEMHContainerBase) SCENARIO3),
        false), NEW_RADICAL3, false);

    assertEquals(1, infos.newNameContainers.size());
    assertEquals(0, infos.newNameFiles.size());
    assertTrue(infos.newNameContainers.containsKey(SCENARIO3.getLevel().getPrefix() + NEW_RADICAL3));
    assertEquals(SCENARIO3, infos.newNameContainers.get(SCENARIO3.getLevel().getPrefix() + NEW_RADICAL3));

    assertTrue(manager.canUseNewNames(infos).containsErrors());

    infos = manager.getNewNames(SCENARIO1,
        finder.getIndependantSelection(Collections.singletonList((ManagerEMHContainerBase) SCENARIO1), true),
        NEW_RADICAL1, true);

    assertEquals(4, infos.newNameContainers.size());
    assertEquals(3, infos.newNameFiles.size());
    assertTrue(infos.newNameContainers.containsKey(SCENARIO1.getLevel().getPrefix() + NEW_RADICAL1));
    assertEquals(SCENARIO1, infos.newNameContainers.get(SCENARIO1.getLevel().getPrefix() + NEW_RADICAL1));
    assertTrue(infos.newNameContainers.containsKey(MODELE1.getLevel().getPrefix() + NEW_RADICAL1));
    assertEquals(MODELE1, infos.newNameContainers.get(MODELE1.getLevel().getPrefix() + NEW_RADICAL1));
    assertTrue(infos.newNameContainers.containsKey(SOUS_MODELE1.getLevel().getPrefix() + NEW_RADICAL1 + "01"));
    assertEquals(SOUS_MODELE1, infos.newNameContainers.get(SOUS_MODELE1.getLevel().getPrefix() + NEW_RADICAL1 + "01"));
    assertTrue(infos.newNameContainers.containsKey(SOUS_MODELE3.getLevel().getPrefix() + NEW_RADICAL1 + "02"));
    assertEquals(SOUS_MODELE3, infos.newNameContainers.get(SOUS_MODELE3.getLevel().getPrefix() + NEW_RADICAL1 + "02"));
    assertTrue(infos.newNameFiles.containsKey(NEW_RADICAL1 + "." + FICHIER1.getType().getExtension()));
    assertEquals(FICHIER1, infos.newNameFiles.get(NEW_RADICAL1 + "." + FICHIER1.getType().getExtension()));
    assertTrue(infos.newNameFiles.containsKey(NEW_RADICAL1 + "01." + FICHIER4.getType().getExtension()));
    assertEquals(FICHIER4, infos.newNameFiles.get(NEW_RADICAL1 + "01." + FICHIER4.getType().getExtension()));
    assertTrue(infos.newNameFiles.containsKey(NEW_RADICAL1 + "02." + FICHIER6.getType().getExtension()));
    assertEquals(FICHIER6, infos.newNameFiles.get(NEW_RADICAL1 + "02." + FICHIER6.getType().getExtension()));

    assertFalse(manager.canUseNewNames(infos).containsErrors());

    infos = manager.getNewNames(SCENARIO3,
        finder.getIndependantSelection(Collections.singletonList((ManagerEMHContainerBase) SCENARIO3), true),
        NEW_RADICAL1, true);

    assertEquals(3, infos.newNameContainers.size());
    assertEquals(2, infos.newNameFiles.size());
    assertTrue(infos.newNameContainers.containsKey(SCENARIO3.getLevel().getPrefix() + NEW_RADICAL1));
    assertEquals(SCENARIO3, infos.newNameContainers.get(SCENARIO3.getLevel().getPrefix() + NEW_RADICAL1));
    assertTrue(infos.newNameContainers.containsKey(MODELE3.getLevel().getPrefix() + NEW_RADICAL1));
    assertEquals(MODELE3, infos.newNameContainers.get(MODELE3.getLevel().getPrefix() + NEW_RADICAL1));
    assertTrue(infos.newNameContainers.containsKey(SOUS_MODELE4.getLevel().getPrefix() + NEW_RADICAL1 + "01"));
    assertEquals(SOUS_MODELE4, infos.newNameContainers.get(SOUS_MODELE4.getLevel().getPrefix() + NEW_RADICAL1 + "01"));
    assertTrue(infos.newNameFiles.containsKey(NEW_RADICAL1 + "." + FICHIER3.getType().getExtension()));
    assertEquals(FICHIER3, infos.newNameFiles.get(NEW_RADICAL1 + "." + FICHIER3.getType().getExtension()));
    assertTrue(infos.newNameFiles.containsKey(NEW_RADICAL1 + "01." + FICHIER7.getType().getExtension()));
    assertEquals(FICHIER7, infos.newNameFiles.get(NEW_RADICAL1 + "01." + FICHIER7.getType().getExtension()));

    assertFalse(manager.canUseNewNames(infos).containsErrors());

    infos = manager.getNewNames(SCENARIO1,
        finder.getIndependantSelection(Collections.singletonList((ManagerEMHContainerBase) SCENARIO1), true),
        NEW_RADICAL4, true);

    assertEquals(4, infos.newNameContainers.size());
    assertEquals(3, infos.newNameFiles.size());
    assertTrue(infos.newNameContainers.containsKey(SCENARIO1.getLevel().getPrefix() + NEW_RADICAL4));
    assertEquals(SCENARIO1, infos.newNameContainers.get(SCENARIO1.getLevel().getPrefix() + NEW_RADICAL4));
    assertTrue(infos.newNameContainers.containsKey(MODELE1.getLevel().getPrefix() + NEW_RADICAL4));
    assertEquals(MODELE1, infos.newNameContainers.get(MODELE1.getLevel().getPrefix() + NEW_RADICAL4));
    assertTrue(infos.newNameContainers.containsKey(SOUS_MODELE1.getLevel().getPrefix() + NEW_RADICAL4 + "01"));
    assertEquals(SOUS_MODELE1, infos.newNameContainers.get(SOUS_MODELE1.getLevel().getPrefix() + NEW_RADICAL4 + "01"));
    assertTrue(infos.newNameContainers.containsKey(SOUS_MODELE3.getLevel().getPrefix() + NEW_RADICAL4 + "02"));
    assertEquals(SOUS_MODELE3, infos.newNameContainers.get(SOUS_MODELE3.getLevel().getPrefix() + NEW_RADICAL4 + "02"));
    assertTrue(infos.newNameFiles.containsKey(NEW_RADICAL4 + "." + FICHIER1.getType().getExtension()));
    assertEquals(FICHIER1, infos.newNameFiles.get(NEW_RADICAL4 + "." + FICHIER1.getType().getExtension()));
    assertTrue(infos.newNameFiles.containsKey(NEW_RADICAL4 + "01." + FICHIER4.getType().getExtension()));
    assertEquals(FICHIER4, infos.newNameFiles.get(NEW_RADICAL4 + "01." + FICHIER4.getType().getExtension()));
    assertTrue(infos.newNameFiles.containsKey(NEW_RADICAL4 + "02." + FICHIER6.getType().getExtension()));
    assertEquals(FICHIER6, infos.newNameFiles.get(NEW_RADICAL4 + "02." + FICHIER6.getType().getExtension()));

    assertTrue(manager.canUseNewNames(infos).containsErrors());
  }
}
