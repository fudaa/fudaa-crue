package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.metier.emh.EMHNoeudNiveauContinu;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateValidationData;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class SmUpdateValidatorLogCreatorTest {
  @Test
  public void extractLogName() {
    SmUpdateValidatorLogCreator creator = new SmUpdateValidatorLogCreator();
    SmUpdateValidationData validatorData = new SmUpdateValidationData(true, "Sm_target");

    final CtuluLogGroup ctuluLogGroup = creator.extractLog(validatorData);
    Assert.assertEquals("sm.updater.validatorLogTitle", ctuluLogGroup.getDescription());
    Assert.assertEquals("Sm_target", ctuluLogGroup.getDescriptionArgs()[0]);
  }

  @Test
  public void extractLogInAnotherSousModele() {
    SmUpdateValidatorLogCreator creator = new SmUpdateValidatorLogCreator();
    SmUpdateValidationData validatorData = new SmUpdateValidationData("Sm_target");
    EMHNoeudNiveauContinu nd = new EMHNoeudNiveauContinu("Nd_InAnother");
    EMHSousModele sousModele = new EMHSousModele();
    sousModele.setNom("Sm_1");
    EMHRelationFactory.addRelationContientEMH(sousModele, nd);
    validatorData.addDonFrtInAnotherSousModele(nd);

    final CtuluLogGroup ctuluLogGroup = creator.extractLog(validatorData);
    final CtuluLog ctuluLog = ctuluLogGroup.getLogs().get(0);
    Assert.assertEquals("sm.updater.emhInAnotherSousModeleLog", ctuluLog.getDesc());
    Assert.assertEquals("sm.updater.emhInAnotherSousModeleLogItem", ctuluLog.getRecords().get(0).getMsg());
    Assert.assertEquals("Nd_InAnother", ctuluLog.getRecords().get(0).getArgs()[0]);
    Assert.assertEquals("Sm_1", ctuluLog.getRecords().get(0).getArgs()[1]);
  }

  @Test
  public void extractLogNotInTarget() {
    SmUpdateValidatorLogCreator creator = new SmUpdateValidatorLogCreator();
    SmUpdateValidationData validatorData = new SmUpdateValidationData("Sm_target");
    validatorData.addRequiredNotInTarget("St_NotInTarget");

    final CtuluLogGroup ctuluLogGroup = creator.extractLog(validatorData);
    final CtuluLog ctuluLog = ctuluLogGroup.getLogs().get(0);
    Assert.assertEquals("sm.updater.requiredSectionsNotInTargetLog", ctuluLog.getDesc());
    Assert.assertEquals(1, ctuluLog.getRecords().size());
    Assert.assertEquals("sm.updater.requiredSectionsNotInTargetLogItem", ctuluLog.getRecords().get(0).getMsg());
    Assert.assertEquals("St_NotInTarget", ctuluLog.getRecords().get(0).getArgs()[0]);
  }
}
