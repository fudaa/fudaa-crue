package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateSourceTargetData;
import org.junit.Assert;
import org.junit.Test;

public class SmUpdateChangeTypeIdentifierTest {
  @Test
  public void extractChangeWithBrancheSameType() {
    SmUpdateChangeTypeIdentifier typeIdentifier = new SmUpdateChangeTypeIdentifier();

    SmUpdateSourceTargetData sourceTargetData = new SmUpdateSourceTargetData();
    final SmUpdateSourceTargetData.Line<CatEMHBranche> emhBrancheLine = new SmUpdateSourceTargetData.Line<>(new EMHBrancheOrifice("Br_1"), new EMHBrancheOrifice("Br_1"));
    sourceTargetData.addBrancheLine(emhBrancheLine);

    final SmUpdateSourceTargetData.Line<CatEMHSection> emhSectionLine = new SmUpdateSourceTargetData.Line<>(new EMHSectionProfil("St_1"), new EMHSectionProfil("Br_1"));
    sourceTargetData.addSectionLine(emhSectionLine);

    final SmUpdateSourceTargetData.Line<CatEMHCasier> emhCasierLine = new SmUpdateSourceTargetData.Line<>(new EMHCasierProfil("Ca_1"), new EMHCasierProfil("Ca_1"));
    sourceTargetData.addCasierLine(emhCasierLine);

    final SmUpdateSourceTargetData.Line<CatEMHNoeud> emhNoeudLine = new SmUpdateSourceTargetData.Line<>(new EMHNoeudNiveauContinu("Nd_1"), new EMHNoeudNiveauContinu("Nd_1"));
    sourceTargetData.addNoeudLine(emhNoeudLine);

    final CtuluIOResult<SmUpdateSourceTargetData> ioResult = typeIdentifier.extractChange(sourceTargetData);
    Assert.assertTrue(ioResult.getAnalyze().getRecords().isEmpty());

    Assert.assertEquals(1, ioResult.getSource().getBranches().size());
    Assert.assertTrue(emhBrancheLine == ioResult.getSource().getBranches().get(0));

    Assert.assertEquals(1, ioResult.getSource().getSections().size());
    Assert.assertTrue(emhSectionLine == ioResult.getSource().getSections().get(0));

    Assert.assertEquals(1, ioResult.getSource().getCasiers().size());
    Assert.assertTrue(emhCasierLine == ioResult.getSource().getCasiers().get(0));

    Assert.assertEquals(1, ioResult.getSource().getNoeuds().size());
    Assert.assertTrue(emhNoeudLine == ioResult.getSource().getNoeuds().get(0));
  }

  @Test
  public void extractChangeWithBrancheDifferentType() {
    SmUpdateChangeTypeIdentifier typeIdentifier = new SmUpdateChangeTypeIdentifier();

    SmUpdateSourceTargetData sourceTargetData = new SmUpdateSourceTargetData();
    EMHBrancheOrifice source = new EMHBrancheOrifice("Br_1");
    EMHBrancheSeuilLateral target = new EMHBrancheSeuilLateral("Br_1");
    sourceTargetData.addBrancheLine(new SmUpdateSourceTargetData.Line<>(source, target));
    final CtuluIOResult<SmUpdateSourceTargetData> ioResult = typeIdentifier.extractChange(sourceTargetData);
    Assert.assertEquals(1, ioResult.getAnalyze().getRecords().size());
    Assert.assertEquals("sm.updater.typeChangedBranchesLogItem", ioResult.getAnalyze().getRecords().get(0).getMsg());
    Assert.assertTrue(ioResult.getSource().getBranches().isEmpty());
  }

  @Test
  public void extractChangeWithSectionDifferentType() {
    SmUpdateChangeTypeIdentifier typeIdentifier = new SmUpdateChangeTypeIdentifier();

    SmUpdateSourceTargetData sourceTargetData = new SmUpdateSourceTargetData();
    EMHSectionProfil source = new EMHSectionProfil("St_1");
    EMHSectionSansGeometrie target = new EMHSectionSansGeometrie("St_1");
    sourceTargetData.addSectionLine(new SmUpdateSourceTargetData.Line<>(source, target));
    final CtuluIOResult<SmUpdateSourceTargetData> ioResult = typeIdentifier.extractChange(sourceTargetData);
    Assert.assertEquals(1, ioResult.getAnalyze().getRecords().size());
    Assert.assertEquals("sm.updater.typeChangedSectionsLogItem", ioResult.getAnalyze().getRecords().get(0).getMsg());
    Assert.assertTrue(ioResult.getSource().getSections().isEmpty());
  }
}
