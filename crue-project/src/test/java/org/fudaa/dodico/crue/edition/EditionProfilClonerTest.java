package org.fudaa.dodico.crue.edition;

import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.integration.TestProjectLoaderHelper;
import org.fudaa.dodico.crue.metier.comparator.Point2dAbscisseComparator;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class EditionProfilClonerTest {
  @Test
  public void cloneProfil() throws Exception {

    final EMHScenario scenario = TestProjectLoaderHelper.loadScenarioScM3_0_c10();
    assertNotNull(scenario);
    final EMHSectionProfil initSection = (EMHSectionProfil) scenario.getIdRegistry().findByName("St_PROF3A");
    assertNotNull(initSection);

    DonPrtCIniSection cini = new DonPrtCIniSection(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    cini.setZini(0.123);
    initSection.addInfosEMH(cini);

    EditionSectionCloner cloner = new EditionSectionCloner(new UniqueNomFinder());
    final EMHSectionProfil clonedSectgionProfil = cloner.clone(initSection);
    assertTrue(clonedSectgionProfil.getParent() == initSection.getParent());
    assertTrue(clonedSectgionProfil.getParent().getParent() == initSection.getParent().getParent());
    assertEquals("St_PROF3A_Copie", clonedSectgionProfil.getNom());
    EMHSectionProfil fromRegistry = (EMHSectionProfil) scenario.getIdRegistry().findByName("St_PROF3A_Copie");
    assertTrue(clonedSectgionProfil == fromRegistry);

    //cini
    DonPrtCIniSection ciniCloned = EMHHelper.selectFirstOfClass(clonedSectgionProfil.getInfosEMH(), DonPrtCIniSection.class);
    assertFalse(ciniCloned == cini);
    assertEquals(cini.getZini(), ciniCloned.getZini(), 0.0001);

    //les profil
    DonPrtGeoProfilSection clonedProfil = EMHHelper.selectFirstOfClass(clonedSectgionProfil.getInfosEMH(), DonPrtGeoProfilSection.class);
    assertEquals("Ps_PROF3A_Copie", clonedProfil.getNom());

    DonPrtGeoProfilSection initProfil = EMHHelper.selectFirstOfClass(initSection.getInfosEMH(), DonPrtGeoProfilSection.class);
    assertFalse(initProfil == clonedProfil);

    final List<LitNumerote> initProfilLitNumerote = initProfil.getLitNumerote();
    final List<LitNumerote> clonedtProfilLitNumerote = clonedProfil.getLitNumerote();
    assertEquals(initProfilLitNumerote.size(), clonedtProfilLitNumerote.size());
    assertFalse(initProfilLitNumerote.get(0) == clonedtProfilLitNumerote.get(0));
    assertTrue(
        initProfilLitNumerote.get(0).isSame(clonedtProfilLitNumerote.get(0), new Point2dAbscisseComparator(TestCoeurConfig.INSTANCE.getCrueConfigMetier().getEpsilon("Xt"))));

    //les frottements ne sont pas dupliqués
    assertTrue(initProfilLitNumerote.get(0).getFrot() == clonedtProfilLitNumerote.get(0).getFrot());
  }
}
