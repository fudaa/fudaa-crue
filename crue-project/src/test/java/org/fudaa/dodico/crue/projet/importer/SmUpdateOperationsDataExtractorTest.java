package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOperationsData;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOptions;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class SmUpdateOperationsDataExtractorTest {
  @Test
  public void extractOperationAndAllIsUpdated() {
    SmScenarioBuilderForTest source = new SmScenarioBuilderForTest("m1", "m2");
    SmScenarioBuilderForTest target = new SmScenarioBuilderForTest("m1", "m2");
    SmUpdateOperationsDataExtractor dataExtractor = new SmUpdateOperationsDataExtractor(target.scenario);
    final SmBuilderForTest sousModeleM1 = source.getSmBuilders().get(0);
    final SmUpdateOperationsData smUpdateSourceData = dataExtractor
      .extract(sousModeleM1.getSousModele(), new SmUpdateOptions(true, true, true), CruePrefix.P_SS_MODELE + "m1");

    final List<EMH> allSimpleEMHInSource = sousModeleM1.getAllSimpleEMHUpdatedOrAdded();
    allSimpleEMHInSource.sort(ObjetNommeByNameComparator.INSTANCE);
    Assert.assertEquals("Toutes les EMHS seront mise à jour", allSimpleEMHInSource, smUpdateSourceData.getSourceEmhsToUpdate());
  }

  @Test
  public void extractOpearionOneNodeInOtherSousModele() {
    SmScenarioBuilderForTest source = new SmScenarioBuilderForTest("m1", "m2");
    final SmBuilderForTest sousModeleM1 = source.getSmBuilders().get(0);
    final SmBuilderForTest sousModeleM2 = source.getSmBuilders().get(1);
    final CatEMHNoeud ndToAddInSousModele1 = (CatEMHNoeud) sousModeleM2.getSousModele().getAllSimpleEMH(EnumCatEMH.NOEUD).get(0);
    EMHRelationFactory.addRelationContientEMH(sousModeleM1.getSousModele(), ndToAddInSousModele1);
    SmScenarioBuilderForTest target = new SmScenarioBuilderForTest("m1", "m2");
    //on remplace un noeud de la branche avec le noeud du sous-modele 2
    final CatEMHBranche branche = (CatEMHBranche) source.getScenario().getIdRegistry().getEmhByNom().get(sousModeleM1.brancheName);
    EMHRelationFactory.removeRelation(branche, branche.getNoeudAmont());
    EMHRelationFactory.addNoeudAmont(branche, ndToAddInSousModele1);

    SmUpdateOperationsDataExtractor dataExtractor = new SmUpdateOperationsDataExtractor(target.scenario);
    final SmUpdateOperationsData smUpdateSourceData = dataExtractor
      .extract(sousModeleM1.getSousModele(), new SmUpdateOptions(true, true, true), CruePrefix.P_SS_MODELE + "m1");

    Assert.assertTrue("Le noeud qui appartient au sous-modele 2 va etre mis à jour et appartenir au sous-modele 1",
      TransformerHelper.toSetNom(smUpdateSourceData.getSourceEmhsToUpdate()).contains(ndToAddInSousModele1.getNom()));
    Assert.assertFalse("Le noeud qui appartient au sous-modele 2 ne doit pas etre créé",
      TransformerHelper.toSetNom(smUpdateSourceData.getSourceEmhsToAdd()).contains(ndToAddInSousModele1.getNom()));
  }

  @Test
  public void extractOperationAndAllIsCreated() {
    SmScenarioBuilderForTest source = new SmScenarioBuilderForTest("m1");
    SmScenarioBuilderForTest target = new SmScenarioBuilderForTest("m2");
    //on va tenter de mettre à jour le sous-modele m2 a partir de donnees issues du sous-modele m1
    SmUpdateOperationsDataExtractor validator = new SmUpdateOperationsDataExtractor(target.scenario);
    final EMHSousModele sourceSousModeleM1 = source.getSousModele(0);
    final SmUpdateOperationsData smUpdateSourceData = validator
      .extract(sourceSousModeleM1, new SmUpdateOptions(true, true, true), CruePrefix.P_SS_MODELE + "m2");
    Assert.assertTrue("Pas de mise à jour", smUpdateSourceData.getSourceEmhsToUpdate().isEmpty());
    Assert.assertEquals("Toutes les EMHs doivent être créées dans le sous-modele m2", TransformerHelper.toNom(source.getSmBuilders().get(0).getAllSimpleEMHUpdatedOrAdded()),
      TransformerHelper.toNom(smUpdateSourceData.getSourceEmhsToAdd()));
  }
}
