/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformationDefault;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormatETU;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.etude.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.projet.create.InfosCreation;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * @author CANEL Christophe
 */
public class EMHProjetControllerTest extends AbstractTestParent {
  @Test
  public void testConversion() {
    final File exportZipInTempDir = exportZipInTempDir("/projet/Etu3-0_1.1.2.zip");
    final File etuFile = getEtuFileUpdated("etu3-0", exportZipInTempDir);
    Assert.assertNotNull(etuFile);
    Assert.assertTrue(etuFile.exists());

    this.testConversion10MonoVers10(etuFile);
    this.testConversion10MonoVers9(etuFile);
    this.testConversion9Vers9(etuFile);
    this.testConversion9Vers10(etuFile);
  }

  private void testConversion10MonoVers10(final File etuFile) {
    final File etuDir = etuFile.getParentFile();
    final InfosCreation infos = this.getInfos("Conversion Crue10 mono vers Crue10", CrueVersionType.CRUE10);

    EMHProjet projet = this.getConvertedProject(etuFile);
    ManagerEMHScenario scenario = this.getScenario("Sc_M3-0_v2c10", projet);
    final String sc9 = "Sc_c9";
    scenario.setLinkedscenarioCrue9(sc9);
    final EMHProjetController controller = new EMHProjetController(projet, ConnexionInformationDefault.getDefault());
    controller.saveProjet(etuFile, new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE));
    final CtuluLogGroup errors = this.executeConversion(etuFile, "Sc_M3-0_v2c10", infos);
    Assert.assertFalse(errors.containsFatalError());
    Assert.assertEquals(0, errors.getLogs().size());
    projet = this.getConvertedProject(etuFile);
    scenario = this.getScenario("Sc_M3-0_v2c10c10", projet);
    Assert.assertEquals(sc9, scenario.getLinkedscenarioCrue9());
    this.testScenario(etuDir, scenario, infos);

    final ManagerEMHModeleBase modele = this.getModele("Mo_M3-0_v2c10c10", scenario);
    this.testModele(etuDir, modele, infos);

    final ManagerEMHSousModele sousModele = this.getSousModele("Sm_M3-0_v2c10c10", modele);
    this.testSousModele(etuDir, sousModele, infos);
  }

  private void testConversion10MonoVers9(final File etuFile) {
    final File etuDir = etuFile.getParentFile();
    final InfosCreation infos = this.getInfos("Conversion Crue10 mono vers Crue9", CrueVersionType.CRUE9);

    final CtuluLogGroup errors = this.executeConversion(etuFile, "Sc_M3-0_v2c10", infos);

    Assert.assertFalse(errors.containsFatalError());

    final EMHProjet projet = this.getConvertedProject(etuFile);

    final ManagerEMHScenario scenario = this.getScenario("Sc_M3-0_v2c10c9", projet);
    this.testScenario(etuDir, scenario, infos);

    final ManagerEMHModeleBase modele = this.getModele("Mo_M3-0_v2c10c9", scenario);
    this.testModele(etuDir, modele, infos);
  }

  private void testConversion9Vers9(final File etuFile) {
    final File etuDir = etuFile.getParentFile();
    final InfosCreation infos = this.getInfos("Conversion Crue9 vers Crue9", CrueVersionType.CRUE9);

    final CtuluLogGroup errors = this.executeConversion(etuFile, "Sc_M3-0_v2c9", infos);

    Assert.assertFalse(errors.containsFatalError());

    final EMHProjet projet = this.getConvertedProject(etuFile);

    final ManagerEMHScenario scenario = this.getScenario("Sc_M3-0_v2c9c9", projet);
    this.testScenario(etuDir, scenario, infos);

    final ManagerEMHModeleBase modele = this.getModele("Mo_M3-0_v2c9c9", scenario);
    this.testModele(etuDir, modele, infos);
  }

  private void testConversion9Vers10(final File etuFile) {
    final File etuDir = etuFile.getParentFile();
    final InfosCreation infos = this.getInfos("Conversion Crue9 vers Crue10", CrueVersionType.CRUE10);

    final CtuluLogGroup errors = this.executeConversion(etuFile, "Sc_M3-0_v2c9", infos);

    Assert.assertFalse(errors.containsFatalError());

    final EMHProjet projet = this.getConvertedProject(etuFile);
//    this.testInfosVersion(projet.getInfos().getInfosVersions(), infos);

    final ManagerEMHScenario scenario = this.getScenario("Sc_M3-0_v2c9c10", projet);
    Assert.assertEquals("Sc_M3-0_v2c9", scenario.getLinkedscenarioCrue9());
    this.testScenario(etuDir, scenario, infos);

    final ManagerEMHModeleBase modele = this.getModele("Mo_M3-0_v2c9c10", scenario);
    this.testModele(etuDir, modele, infos);

    final ManagerEMHSousModele sousModele = this.getSousModele("Sm_M3-0_v2c9c10", modele);
    this.testSousModele(etuDir, sousModele, infos);
  }

  private void testSousModele(final File etuDir, final ManagerEMHSousModele sousModele, final InfosCreation infos) {
    this.testInfosVersion(sousModele.getInfosVersions(), infos);
    this.testFiles(etuDir, sousModele.getNom().replaceFirst(CruePrefix.P_SS_MODELE, ""), CrueLevelType.SOUS_MODELE,
        infos.getCrueVersion());
  }

  private ManagerEMHSousModele getSousModele(final String name, final ManagerEMHModeleBase modele) {
    final ManagerEMHSousModele sousModele = modele.getManagerFils(name.toUpperCase());
    Assert.assertNotNull(sousModele);

    return sousModele;
  }

  private void testModele(final File etuDir, final ManagerEMHModeleBase modele, final InfosCreation infos) {
    this.testInfosVersion(modele.getInfosVersions(), infos);
    this.testFiles(etuDir, modele.getNom().replaceFirst(CruePrefix.P_MODELE, ""), CrueLevelType.MODELE, infos.getCrueVersion());
  }

  private ManagerEMHModeleBase getModele(final String name, final ManagerEMHScenario scenario) {
    final ManagerEMHModeleBase modele = scenario.getManagerFils(name.toUpperCase());
    Assert.assertNotNull(modele);

    return modele;
  }

  private void testScenario(final File etuDir, final ManagerEMHScenario scenario, final InfosCreation infos) {
    this.testInfosVersion(scenario.getInfosVersions(), infos);
    this.testFiles(etuDir, scenario.getNom().replaceFirst(CruePrefix.P_SCENARIO, ""), CrueLevelType.SCENARIO,
        infos.getCrueVersion());
  }

  private ManagerEMHScenario getScenario(final String name, final EMHProjet projet) {
    final ManagerEMHScenario scenario = projet.getScenario(name);
    Assert.assertNotNull(scenario);

    return scenario;
  }

  private InfosCreation getInfos(final String commentaire, final CrueVersionType version) {
    final InfosCreation infos = new InfosCreation(ConnexionInformationDefault.getDefault());
    infos.setCommentaire(commentaire);
    infos.setCrueVersion(version);

    return infos;
  }

  private CtuluLogGroup executeConversion(final File etuFile, final String scenario, final InfosCreation infos) {
    final EMHProjet projet = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE_1_2);
    Assert.assertNotNull(projet);

    final EMHProjetController controller = new EMHProjetController(projet, ConnexionInformationDefault.getDefault());

    return controller.convertScenario(scenario, infos, etuFile).getLogs();
  }

  private EMHProjet getConvertedProject(final File etuFile) {
    final EMHProjet projet = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE_1_2);
    Assert.assertNotNull(projet);

    return projet;
  }

  private void testFiles(final File etuDir, final String name, final CrueLevelType level, final CrueVersionType version) {
    for (final CrueFileType type : CrueFileType.values()) {
      if (CrueFileType.LHPT.equals(type)) {
        continue;
      }
      if (CrueFileType.DREG.equals(type)) {
        continue;
      }
      if ((type.getCrueVersionType() == version) && (type.getLevel() == level) && (!type.isResultFileType())) {
        Assert.assertTrue(new File(etuDir, name + "." + type.getExtension()).isFile());
      }
    }
  }

  private void testInfosVersion(final EMHInfosVersion infosVersion, final InfosCreation infosCreation) {
    Assert.assertEquals(ConnexionInformationDefault.getDefault().getCurrentUser(), infosVersion.getAuteurDerniereModif());
    Assert.assertEquals(infosCreation.getCommentaire(), infosVersion.getCommentaire());
    Assert.assertEquals(infosCreation.getCrueVersion(), infosVersion.getCrueVersion());
  }
}
