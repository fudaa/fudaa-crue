/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.projet.migrate;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.common.ConnexionInformationDefault;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormatETU;
import org.fudaa.dodico.crue.metier.etude.*;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author deniger
 */
public class ScenarioMigrateProcessorTest extends AbstractTestParent {
  private final String userName;

  public ScenarioMigrateProcessorTest() {
    userName = ConnexionInformationDefault.getDefault().getCurrentUser();
  }


  @Test
  public void cantMigrateTo1_2() {
    final File exportZipInTempDir = exportZipInTempDir("/migration/etu3-ok.zip");
    File etuFile = getEtuFileUpdated("etu3-0", exportZipInTempDir);
    Assert.assertTrue(etuFile.exists());

    final EMHProjet readData = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE_1_1_1);
    Assert.assertNotNull(readData);

    final File targetDir = createTempDir();

    final ScenarioMigrateProcessor processor = new ScenarioMigrateProcessor(readData);

    final CtuluLogGroup errors = processor.exportTo(targetDir, "etu3-0.etu.xml", TestCoeurConfig.INSTANCE_1_2);
    Assert.assertTrue(errors.containsFatalError());
    Assert.assertEquals(1,errors.getNbOccurence(CtuluLogLevel.SEVERE));
    Assert.assertEquals("migrate.canOnlyMigrateToLastVersion",errors.getLastLog().getRecords().get(0).getMsg());
  }

  @Test
  public void testExtractCorrectProject() {
    final File exportZipInTempDir = exportZipInTempDir("/migration/etu3-ok.zip");
    File etuFile = getEtuFileUpdated("etu3-0", exportZipInTempDir);
    Assert.assertNotNull(etuFile);
    Assert.assertTrue(etuFile.exists());

    final EMHProjet readData = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE_1_1_1);
    Assert.assertNotNull(readData);

    final File targetDir = createTempDir();

    final ScenarioMigrateProcessor processor = new ScenarioMigrateProcessor(readData);

    final CtuluLogGroup errors = processor.exportTo(targetDir, "etu3-0.etu.xml", TestCoeurConfig.INSTANCE);
    if (errors.containsError() || errors.containsFatalError()) {
      AbstractTestParent.printResume(errors);
    }
    Assert.assertFalse(errors.containsError());
    Assert.assertFalse(errors.containsFatalError());

    etuFile = new File(targetDir, "etu3-0.etu.xml");

    final EMHProjet targetProject = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE);

    Assert.assertNotNull(targetProject);

    this.testSenario(targetProject);
    this.testModeles(targetProject);
    this.testSousModeles(targetProject);

    final String[] presentFiles = new String[]{"etu3-0.etu.xml",
        "M3-0_c10.dclm.xml",
        "M3-0_c10.dcsp.xml",
        "M3-0_c10.dfrt.xml",
        "M3-0_c10.dlhy.xml",
        "M3-0_c10.dptg.xml",
        "M3-0_c10.dpti.xml",
        "M3-0_c10.drso.xml",
        "M3-0_c10.ocal.xml",
        "M3-0_c10.optg.xml",
        "M3-0_c10.opti.xml",
        "M3-0_c10.ores.xml",
        "M3-0_c10.pcal.xml",
        "M3-0_c10.pnum.xml",
        "M3-0_c10.optr.xml",//ajoute automatiquement pour v1.1.1 ->v1.2 et sup
        "M3-0_c10.dreg.xml",//ajoute automatiquement pour v1.1.1 ->v1.3 et sup
        "M3_c9.dc",
        "M3_c9.dh",
        "Rapports",
        "Config"//repertoire de config

    };

    this.testFichiersPresent(presentFiles, targetDir.list());
  }

  private void testFichiersPresent(final String[] filesExpected, final String[] filesActual) {
    Assert.assertEquals(filesExpected.length, filesActual.length);

    final List<String> files = Arrays.asList(filesActual);

    for (final String aFilesExpected : filesExpected) {
      Assert.assertTrue(files.contains(aFilesExpected));
    }
  }

  private void testNomsFichierCrue(final String[] noms, final List<FichierCrue> fichiers) {
    Assert.assertEquals(noms.length, fichiers.size());

    final List<String> nomsFichiers = new ArrayList<>();

    for (final FichierCrue fichier : fichiers) {
      nomsFichiers.add(fichier.getNom());
    }

    for (final String nom : noms) {
      Assert.assertTrue("file: " + nom, nomsFichiers.contains(nom));
    }
  }

  private void testSenario(final EMHProjet targetProject) {
    final List<ManagerEMHScenario> scenarios = targetProject.getListeScenarios();
    Assert.assertEquals(2, scenarios.size());

    final ManagerEMHScenario scenario = scenarios.get(0);
    Assert.assertEquals("Sc_M3-0_c10", scenario.getNom());
    Assert.assertFalse(scenario.isCrue9());
    Assert.assertTrue(scenario.isActive());

    this.testNomsFichierCrue(new String[]{"M3-0_c10.ocal.xml", "M3-0_c10.ores.xml", "M3-0_c10.pcal.xml", "M3-0_c10.dclm.xml", "M3-0_c10.dlhy.xml"},
        scenario.getListeFichiers().getFichiers());

    final List<ManagerEMHModeleBase> scenarioModele = scenario.getFils();
    Assert.assertEquals(1, scenarioModele.size());
    Assert.assertEquals("Mo_M3-0_c10", scenarioModele.get(0).getNom());

    final EMHInfosVersion infosScenario = scenario.getInfosVersions();
    Assert.assertEquals("Crue10", infosScenario.getType());
    Assert.assertEquals("Scénario Crue10 normé", infosScenario.getCommentaire());
    Assert.assertEquals("Balayn_P", infosScenario.getAuteurCreation());
    Assert.assertEquals(DateDurationConverter.getDate("2009-01-22T14:22:00"), infosScenario.getDateCreation());
    Assert.assertEquals(userName, infosScenario.getAuteurDerniereModif());
    Assert.assertNotSame(DateDurationConverter.getDate("2009-01-22T14:22:00"), infosScenario.getDateDerniereModif());

    Assert.assertTrue(scenario.getListeRuns().isEmpty());
  }

  private void testModeles(final EMHProjet targetProject) {
    final List<ManagerEMHModeleBase> modeles = targetProject.getListeModeles();
    Assert.assertEquals(2, modeles.size());

    final ManagerEMHModeleBase modele = modeles.get(0);
    Assert.assertEquals("Mo_M3-0_c10", modele.getNom());
    Assert.assertFalse(modele.isCrue9());
    Assert.assertTrue(modele.isActive());
    final List<FichierCrue> fichiers = modele.getListeFichiers().getFichiers();
    this.testNomsFichierCrue(new String[]{"M3-0_c10.optg.xml", "M3-0_c10.opti.xml", "M3-0_c10.pnum.xml", "M3-0_c10.dpti.xml", "M3-0_c10.optr.xml", "M3-0_c10.dreg.xml"}, fichiers);

    final List<ManagerEMHSousModele> modeleSousModele = modele.getFils();
    Assert.assertEquals(1, modeleSousModele.size());
    Assert.assertEquals("Sm_M3-0_c10", modeleSousModele.get(0).getNom());

    final EMHInfosVersion infosModele = modele.getInfosVersions();
    Assert.assertEquals("Crue10", infosModele.getType());
    Assert.assertEquals("Commentaire ou description du modèle Mo_M3-0_c10", infosModele.getCommentaire());
    Assert.assertEquals("Balayn_P", infosModele.getAuteurCreation());
    Assert.assertEquals(DateDurationConverter.getDate("2009-01-22T14:22:00"), infosModele.getDateCreation());
    Assert.assertEquals(userName, infosModele.getAuteurDerniereModif());
    Assert.assertNotSame(DateDurationConverter.getDate("2009-01-22T14:22:00"), infosModele.getDateDerniereModif());
  }

  private void testSousModeles(final EMHProjet targetProject) {
    final List<ManagerEMHSousModele> sousModeles = targetProject.getListeSousModeles();
    Assert.assertEquals(1, sousModeles.size());

    final ManagerEMHSousModele sousModele = sousModeles.get(0);
    Assert.assertEquals("Sm_M3-0_c10", sousModele.getNom());
    Assert.assertFalse(sousModele.isCrue9());
    Assert.assertTrue(sousModele.isActive());

    this.testNomsFichierCrue(new String[]{"M3-0_c10.drso.xml", "M3-0_c10.dcsp.xml", "M3-0_c10.dptg.xml", "M3-0_c10.dfrt.xml"}, sousModele.getListeFichiers().getFichiers());

    final EMHInfosVersion infosSousModele = sousModele.getInfosVersions();
    Assert.assertEquals("Crue10", infosSousModele.getType());
    Assert.assertEquals("Commentaire du sous-modèle Sm_M3-0_c10", infosSousModele.getCommentaire());
    Assert.assertEquals("Balayn_P", infosSousModele.getAuteurCreation());
    Assert.assertEquals(DateDurationConverter.getDate("2009-01-22T14:22:00"), infosSousModele.getDateCreation());
    Assert.assertEquals(userName, infosSousModele.getAuteurDerniereModif());
    Assert.assertNotSame(DateDurationConverter.getDate("2009-01-22T14:22:00"), infosSousModele.getDateDerniereModif());
  }

  @Test
  public void testExtractProblemProject() {
    final File exportZipInTempDir = exportZipInTempDir("/migration/etu3-avecErreurs.zip");
    final File etuFile = getEtuFileUpdated("etu3-0", exportZipInTempDir);
    Assert.assertNotNull(etuFile);
    Assert.assertTrue(etuFile.exists());

    final EMHProjet readData = Crue10FileFormatETU.readData(etuFile, TestCoeurConfig.INSTANCE_1_1_1);
    Assert.assertNotNull(readData);

    final File targetDir = createTempDir();

    final ScenarioMigrateProcessor processor = new ScenarioMigrateProcessor(readData);
//    processor.setUserConnected("User");

    final CtuluLogGroup errors = processor.exportTo(targetDir, "etu3-0.etu.xml", TestCoeurConfig.INSTANCE);
    Assert.assertTrue(errors.containsError());
    Assert.assertTrue(errors.containsFatalError());

    Assert.assertEquals(0, targetDir.list().length);
  }
}
