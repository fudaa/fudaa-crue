package org.fudaa.dodico.crue.edition;

import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.edition.AlgoEMHSectionProfilEMHSectionIdemInverser;
import org.fudaa.dodico.crue.integration.TestProjectLoaderHelper;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class AlgoEMHSectionProfilEMHSectionIdemInverserTest {
  @Test
  public void testInverse() {

    final EMHScenario scenario = TestProjectLoaderHelper.loadScenarioScM3_0_c10();
    assertNotNull(scenario);
    final EMHSectionIdem sectionIdem = (EMHSectionIdem) scenario.getIdRegistry().findByName("St_PROF3AM");
    final EMHSectionProfil sectionRef = (EMHSectionProfil) sectionIdem.getSectionRef();

    final EMHSectionIdem sectionIdemOther = (EMHSectionIdem) scenario.getIdRegistry().findByName("St_PROF6B");

    assertNotNull(sectionIdem);
    assertNotNull(sectionIdemOther);
    assertNotNull(sectionRef);

    assertTrue(sectionRef == scenario.getIdRegistry().findByName("St_PROF3A"));

    //on initialise le dz
    DonPrtGeoSectionIdem dptgSectionIdem = EMHHelper.getDPTGSectionIdem(sectionIdem);
    final double dzSectionIdem = 1d;
    dptgSectionIdem.setDz(dzSectionIdem);

    //on fait en sorte que sectionIdemOther utilise aussi ref
    sectionIdemOther.setSectionRef(sectionRef);
    dptgSectionIdem = EMHHelper.getDPTGSectionIdem(sectionIdemOther);
    final double dzAutreSection = 10d;
    dptgSectionIdem.setDz(dzAutreSection);

    DonPrtGeoProfilSection profilSectionRef = DonPrtHelper.getProfilSection(sectionRef);

    PtProfil oldSectionStart = profilSectionRef.getPtProfil().get(0);
    PtProfil oldSectionEnd = profilSectionRef.getPtProfil().get(profilSectionRef.getPtProfilSize() - 1);

    final CatEMHBranche sectionIdemBranche = sectionIdem.getBranche();
    final CatEMHBranche sectionRefBranche = sectionRef.getBranche();

    double xSectionIdem = EMHRelationFactory.getXP(sectionIdem);
    double xSectionRef = EMHRelationFactory.getXP(sectionRef);

    AlgoEMHSectionProfilEMHSectionIdemInverser inverser = new AlgoEMHSectionProfilEMHSectionIdemInverser(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    inverser.inverseSectionIdem(sectionIdem);

    //test ancienne section
    assertNull(sectionRef.getBranche());
    assertTrue(EMHHelper.getSectionReferencing(sectionRef).isEmpty());
    //points non changés
    profilSectionRef = DonPrtHelper.getProfilSection(sectionRef);
    final List<PtProfil> ptProfil = profilSectionRef.getPtProfil();
    testEquals(oldSectionStart, ptProfil.get(0), 0);
    testEquals(oldSectionEnd, ptProfil.get(profilSectionRef.getPtProfilSize() - 1), 0);

    //la nouvelle section
    final EMHSectionProfil sectionCopied = (EMHSectionProfil) scenario.getIdRegistry().findByName("St_PROF3A_Copie");
    assertNotNull(sectionCopied);

    final DonPrtGeoProfilSection profilSectionCopied = DonPrtHelper.getProfilSection(sectionCopied);
    assertEquals("Ps_PROF3A_Copie",profilSectionCopied.getNom());
    final List<PtProfil> ptProfilCopied = profilSectionCopied.getPtProfil();
    testEquals( ptProfilCopied.get(0), oldSectionStart,dzSectionIdem);
    testEquals(ptProfilCopied.get(ptProfilCopied.size()-1),oldSectionEnd,  dzSectionIdem);


    //les branches sont inversees
    assertTrue(sectionCopied.getBranche() == sectionIdemBranche);
    assertTrue(sectionIdem.getBranche() == sectionRefBranche);

    assertEquals(xSectionIdem, EMHRelationFactory.getXP(sectionCopied), 0.001);
    assertEquals(xSectionRef, EMHRelationFactory.getXP(sectionIdem), 0.001);

    assertTrue(sectionIdem.getSectionRef() == sectionCopied);
    assertTrue(sectionIdemOther.getSectionRef() == sectionCopied);

    //on test les deltaZ
    dptgSectionIdem = EMHHelper.getDPTGSectionIdem(sectionIdem);
    //le dz de la section idem inversee est inversee
    assertEquals(-dzSectionIdem, dptgSectionIdem.getDz(), 0.0001);
    dptgSectionIdem = EMHHelper.getDPTGSectionIdem(sectionIdemOther);
    assertEquals(dzAutreSection - dzSectionIdem, dptgSectionIdem.getDz(), 0.0001);
  }

  private void testEquals(PtProfil pt1, PtProfil pt2, double toAddToZPt2) {
    assertEquals(pt1.getZ(), pt2.getZ() + toAddToZPt2, 0.000001);
    assertEquals(pt1.getXt(), pt2.getXt(), 0.000001);
  }
}
