package org.fudaa.dodico.crue.projet.report;

import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigMultiVarDao;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigTemporalDao;
import org.fudaa.dodico.crue.projet.report.persist.ReportTemporalConfig;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class ReportTemporalConfigTest {

  private ReportReaderWriterTestHelper<ReportConfigTemporalDao> helper = new ReportReaderWriterTestHelper<>();


  @Test
  public void testDuplicate() {

    ReportConfigTemporalDao config = helper.read("/org/fudaa/dodico/crue/projet/report/temporal.xml");
    assertFalse(config.getConfig().getEmhs().isEmpty());
    ReportTemporalConfig duplicate = config.getConfig().duplicate();

    String initString = helper.write(config);
    String duplicateString = helper.write(new ReportConfigTemporalDao(duplicate));

    assertEquals(initString, duplicateString);

  }
}