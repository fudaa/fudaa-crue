package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.edition.EditionCreateEMH;
import org.fudaa.dodico.crue.edition.EditionProfilCreator;
import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;
import org.fudaa.dodico.crue.edition.bean.ListRelationSectionContent;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.DonFrtHelper;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOperationsData;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOptions;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SmUpdateActionTest {
  private final CrueConfigMetier ccm = TestCoeurConfig.INSTANCE.getCrueConfigMetier();
  EditionCreateEMH creator = new EditionCreateEMH(new CreationDefaultValue());

  private EMHScenario createScenario(String brancheSuffix, EnumBrancheType brancheType) {
    EMHScenario scenario = new EMHScenario();
    scenario.setNom(CruePrefix.P_SCENARIO + "1");
    final EMHModeleBase modele = new EMHModeleBase();
    final OrdPrtCIniModeleBase newInfosEMH = new OrdPrtCIniModeleBase(ccm);
    newInfosEMH.setMethodeInterpol(EnumMethodeInterpol.BAIGNOIRE);
    modele.addInfosEMH(newInfosEMH);
    modele.setNom(CruePrefix.P_MODELE + "1");
    EMHSousModele sousModele = new EMHSousModele();
    sousModele.setNom(CruePrefix.P_SS_MODELE + "1");
    EMHRelationFactory.addRelationContientEMH(scenario, modele);
    EMHRelationFactory.addRelationContientEMH(modele, sousModele);
    IdRegistry.install(scenario);
    createBranche(brancheSuffix, brancheType, sousModele);
    Assert.assertEquals(1, scenario.getBranches().size());
    return scenario;
  }

  private void createBranche(String brancheSuffix, EnumBrancheType brancheType, EMHSousModele sousModele) {
    final CatEMHBranche brancheInstance = creator
      .createBranche(CruePrefix.P_BRANCHE + brancheSuffix, sousModele, brancheType, null, null, ccm, 20d);
    Assert.assertNotNull(brancheInstance.getNoeudAmont());
    Assert.assertNotNull(brancheInstance.getNoeudAmontNom());
    Assert.assertNotNull(brancheInstance.getNoeudAval());
    Assert.assertNotNull(brancheInstance.getNoeudAvalNom());
  }

  private CatEMHCasier createCasier(String noeudSuffixe, EnumCasierType casierType, EMHSousModele sousModele) {
    final CatEMHNoeud noeud = creator.createNoeud(CruePrefix.P_NOEUD + noeudSuffixe, sousModele, EnumNoeudType.EMHNoeudNiveauContinu, ccm);
    return creator.createCasier(sousModele, casierType, noeud, ccm);
  }

  @Test
  public void testNoeudInOtherSousModele() {
    final EMHScenario sourceScenario = createScenario("1", EnumBrancheType.EMHBrancheSaintVenant);
    final EMHScenario targetScenario = createScenario("1", EnumBrancheType.EMHBrancheSaintVenant);
    final EMHSousModele targetSousModele = targetScenario.getSousModeles().get(0);
    final EMHSousModele sourceSousModele = sourceScenario.getSousModeles().get(0);
    final CatEMHCasier casier = createCasier("WithCasier", EnumCasierType.EMHCasierProfil, sourceSousModele);

    //le noeud appartient au sous-modele 2 dans la cible
    EMHSousModele newSousModele = new EMHSousModele();
    newSousModele.setNom(CruePrefix.P_SS_MODELE + "2");
    EMHRelationFactory.addRelationContientEMH(targetSousModele.getParent(), newSousModele);
    creator.createNoeud(casier.getNoeud().getNom(), newSousModele, EnumNoeudType.EMHNoeudNiveauContinu, ccm);

    SmUpdateAction action = new SmUpdateAction(targetSousModele, ccm);
    SmUpdateOperationsData smUpdateOperationsData = new SmUpdateOperationsDataExtractor(targetScenario)
      .extract(sourceScenario.getSousModeles().get(0), new SmUpdateOptions(), targetSousModele.getNom());
    action.performImport(sourceScenario.getSousModeles().get(0), smUpdateOperationsData);

    Assert.assertEquals(3, targetScenario.getNoeuds().size());
    Assert.assertEquals(3, targetSousModele.getNoeuds().size());
    CatEMHNoeud noeud = (CatEMHNoeud) targetScenario.getIdRegistry().getEmhByNom().get(casier.getNoeud().getNom());
    Assert.assertEquals(2, noeud.getParents().size());

    testScenarioConsistant("source", sourceScenario);
    testScenarioConsistant("target", targetScenario);
  }

  @Test
  public void testCasierIsCreated() {
    final EMHScenario sourceScenario = createScenario("1", EnumBrancheType.EMHBrancheSaintVenant);
    final EMHScenario targetScenario = createScenario("1", EnumBrancheType.EMHBrancheSaintVenant);
    final EMHSousModele targetSousModele = targetScenario.getSousModeles().get(0);
    final EMHSousModele sourceSousModele = sourceScenario.getSousModeles().get(0);
    createCasier("WithCasier", EnumCasierType.EMHCasierProfil, sourceSousModele);

    DonPrtGeoProfilCasier sourceProfil = DonPrtHelper.getProfilCasier(sourceScenario.getCasiers().get(0)).get(0);
    sourceProfil.addPtProfil(new PtProfil(100, 100));
    final LitUtile litUtile = new LitUtile();
    litUtile.setLimDeb(sourceProfil.getPtProfil().get(0));
    litUtile.setLimFin(sourceProfil.getPtProfil().get(1));
    sourceProfil.setLitUtile(litUtile);

    Assert.assertTrue(sourceScenario.getIdRegistry().getEmhByNom().containsKey("Ca_WithCasier"));

    SmUpdateAction action = new SmUpdateAction(targetSousModele, ccm);
    SmUpdateOperationsData smUpdateOperationsData = new SmUpdateOperationsDataExtractor(targetScenario)
      .extract(sourceScenario.getSousModeles().get(0), new SmUpdateOptions(), targetSousModele.getNom());
    action.performImport(sourceScenario.getSousModeles().get(0), smUpdateOperationsData);

    Assert.assertEquals(1, targetScenario.getCasiers().size());
    Assert.assertEquals("Nd_WithCasier", targetScenario.getCasiers().get(0).getNoeud().getNom());
    DonPrtGeoProfilCasier targetProfilTarget = DonPrtHelper.getProfilCasier(targetScenario.getCasiers().get(0)).get(0);
    Assert.assertNotNull(targetProfilTarget.getLitUtile());
    Assert.assertEquals(sourceProfil.getPtProfil().get(0).getXt(), targetProfilTarget.getLitUtile().getLimDeb().getXt(), 1e-3);
    Assert.assertEquals(sourceProfil.getPtProfil().get(1).getXt(), targetProfilTarget.getLitUtile().getLimFin().getXt(), 1e-3);
    Assert.assertEquals(4, targetProfilTarget.getPtProfil().size());

    //lit utile non touche si Pt Profil identique
    targetProfilTarget.getLitUtile().setLimFin(targetProfilTarget.getPtProfil().get(3));
    smUpdateOperationsData = new SmUpdateOperationsDataExtractor(targetScenario)
      .extract(sourceScenario.getSousModeles().get(0), new SmUpdateOptions(), targetSousModele.getNom());
    action.performImport(sourceScenario.getSousModeles().get(0), smUpdateOperationsData);
    targetProfilTarget = DonPrtHelper.getProfilCasier(targetScenario.getCasiers().get(0)).get(0);
    Assert.assertEquals(sourceProfil.getPtProfil().get(3).getXt(), targetProfilTarget.getLitUtile().getLimFin().getXt(), 1e-3);

    //on touche un pt profil
    sourceProfil.getPtProfil().get(0).setXt(22);

    smUpdateOperationsData = new SmUpdateOperationsDataExtractor(targetScenario)
      .extract(sourceScenario.getSousModeles().get(0), new SmUpdateOptions(), targetSousModele.getNom());
    action.performImport(sourceScenario.getSousModeles().get(0), smUpdateOperationsData);
    targetProfilTarget = DonPrtHelper.getProfilCasier(targetScenario.getCasiers().get(0)).get(0);
    Assert.assertEquals(sourceProfil.getPtProfil().get(0).getXt(), targetProfilTarget.getPtProfil().get(0).getXt(), 1e-3);
    //le lit utile est reinitialise
    Assert.assertEquals(sourceProfil.getPtProfil().get(1).getXt(), targetProfilTarget.getLitUtile().getLimFin().getXt(), 1e-3);

    testScenarioConsistant("source", sourceScenario);
    testScenarioConsistant("target", targetScenario);
  }

  private void testScenarioConsistant(String type, EMHScenario scenario) {
    final List<EMH> allSimpleEMH = scenario.getIdRegistry().getAllSimpleEMH();
    Assert.assertEquals(allSimpleEMH.size(), scenario.getConcatSousModele().getAllSimpleEMH().size());
    allSimpleEMH.forEach(emh -> emh.getInfosEMH().forEach(infosEMH -> Assert
      .assertSame(type + ", " + emh.getNom() + ", " + infosEMH.getClass().getSimpleName(), emh, infosEMH.getEmh())));

    final Map<String, DonFrt> donFrtByName = TransformerHelper.toMapOfNom(scenario.getConcatSousModele().getFrtConteneur().getListFrt());
    scenario.getIdRegistry().getEMHs(EnumCatEMH.SECTION).forEach(section -> {
      final DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection(section);
      if (profilSection != null) {
        final Set<DonFrt> donFrts = DonFrtHelper.collectDonFrt(profilSection);
        donFrts.forEach(donFrt -> {
          Assert.assertSame(donFrt, donFrtByName.get(donFrt.getNom()));
        });
      }
    });
  }

  @Test
  public void testSectionProfilBranche() {
    final EMHScenario sourceScenario = createScenario("1", EnumBrancheType.EMHBrancheSaintVenant);
    final EMHScenario targetScenario = createScenario("1", EnumBrancheType.EMHBrancheSaintVenant);
    final EMHSousModele sourceSousModele = sourceScenario.getSousModeles().get(0);
    final EMHSousModele targetSousModele = targetScenario.getSousModeles().get(0);

    EditionProfilCreator profilCreator = new EditionProfilCreator(new UniqueNomFinder(), new CreationDefaultValue());
    DonFrt newFrtInSource = profilCreator.createDonFrtStrickler("New", sourceSousModele, TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    sourceSousModele.getFrtConteneur().addFrt(newFrtInSource);
    EMHBrancheSaintVenant sourceBranche = (EMHBrancheSaintVenant) sourceScenario.getBranches().get(0);
    EMHBrancheSaintVenant targetBranche = (EMHBrancheSaintVenant) targetScenario.getBranches().get(0);

    final DonPrtGeoProfilSection sourceProfilSectionAmont = DonPrtHelper.getProfilSection(sourceBranche.getSectionAmont().getEmh());
    final DonPrtGeoProfilSection targetProfilSectionAmont = DonPrtHelper.getProfilSection(targetBranche.getSectionAmont().getEmh());
    DonFrt initFrt = sourceProfilSectionAmont.getLitNumerote().get(0).getFrot();
    sourceProfilSectionAmont.getLitNumerote().get(0).setFrot(newFrtInSource);
    Assert.assertEquals("Fk_New", sourceProfilSectionAmont.getLitNumerote().get(0).getFrot().getNom());

    SmUpdateOperationsData smUpdateOperationsData = new SmUpdateOperationsDataExtractor(targetScenario)
      .extract(sourceScenario.getSousModeles().get(0), new SmUpdateOptions(), targetSousModele.getNom());
    new SmUpdateAction(targetSousModele, ccm).performImport(sourceScenario.getSousModeles().get(0), smUpdateOperationsData);
    Assert.assertEquals(initFrt.getNom(), targetProfilSectionAmont.getLitNumerote().get(0).getFrot().getNom());

    //change
    sourceProfilSectionAmont.getPtProfil().get(0).setZ(23);

    smUpdateOperationsData = new SmUpdateOperationsDataExtractor(targetScenario)
      .extract(sourceScenario.getSousModeles().get(0), new SmUpdateOptions(), targetSousModele.getNom());
    new SmUpdateAction(targetSousModele, ccm).performImport(sourceScenario.getSousModeles().get(0), smUpdateOperationsData);
    Assert.assertEquals("Fk_New", targetProfilSectionAmont.getLitNumerote().get(0).getFrot().getNom());
    Assert.assertEquals(23, (int)targetProfilSectionAmont.getPtProfil().get(0).getZ());

    testScenarioConsistant("source", sourceScenario);
    testScenarioConsistant("target", targetScenario);

  }

  @Test
  public void testSectionInterpoleeKeptBranche() {
    final EMHScenario sourceScenario = createScenario("1", EnumBrancheType.EMHBrancheSaintVenant);
    final EMHScenario targetScenario = createScenario("1", EnumBrancheType.EMHBrancheSaintVenant);
    final EMHSousModele targetSousModele = targetScenario.getSousModeles().get(0);

    createSectionInBr1(targetScenario, targetSousModele, "St_Interpolee", EnumSectionType.EMHSectionInterpolee);

    SmUpdateAction action = new SmUpdateAction(targetSousModele, ccm);
    final SmUpdateOperationsData smUpdateOperationsData = new SmUpdateOperationsDataExtractor(targetScenario)
      .extract(sourceScenario.getSousModeles().get(0), new SmUpdateOptions(), targetSousModele.getNom());
    action.performImport(sourceScenario.getSousModeles().get(0), smUpdateOperationsData);

    Assert.assertEquals(3, targetScenario.getSections().size());
    Assert.assertEquals("St_1_Am", targetScenario.getSections().get(0).getNom());
    Assert.assertEquals("St_Interpolee", targetScenario.getSections().get(1).getNom());
    Assert.assertEquals("St_1_Av", targetScenario.getSections().get(2).getNom());

    Assert.assertEquals(2, targetScenario.getNoeuds().size());
    Assert.assertEquals("Nd_0001", targetScenario.getNoeuds().get(0).getNom());
    Assert.assertEquals("Nd_0002", targetScenario.getNoeuds().get(1).getNom());

    Assert.assertEquals(1, targetScenario.getBranches().size());
    Assert.assertEquals("Br_1", targetScenario.getBranches().get(0).getNom());
    Assert.assertEquals(3, targetScenario.getBranches().get(0).getSections().size());
    Assert.assertEquals("St_1_Am", targetScenario.getBranches().get(0).getSections().get(0).getEmhNom());
    Assert.assertEquals("St_Interpolee", targetScenario.getBranches().get(0).getSections().get(1).getEmhNom());
    Assert.assertEquals("St_1_Av", targetScenario.getBranches().get(0).getSections().get(2).getEmhNom());

    testScenarioConsistant("source", sourceScenario);
    testScenarioConsistant("target", targetScenario);
  }

  @Test
  public void testBrancheSeuilLateral() {
    final EMHScenario sourceScenario = createScenario("1", EnumBrancheType.EMHBrancheSeuilLateral);
    final EMHScenario targetScenario = createScenario("1", EnumBrancheType.EMHBrancheSeuilLateral);

    final EMHBrancheSeuilLateral sourceBranche = (EMHBrancheSeuilLateral) sourceScenario.getBranches().get(0);
    final EMHBrancheSeuilLateral targetBranche = (EMHBrancheSeuilLateral) targetScenario.getBranches().get(0);

    final DonCalcSansPrtBrancheSeuilLateral soureDcsp = (DonCalcSansPrtBrancheSeuilLateral) sourceBranche.getDCSP().get(0);
    soureDcsp.getElemSeuilAvecPdc().get(0).setLargeur(23);
    int defaultCoefPdc = (int) soureDcsp.getElemSeuilAvecPdc().get(0).getCoefPdc();

    final EMHSousModele targetSousModele = targetScenario.getSousModeles().get(0);
    SmUpdateOperationsData smUpdateOperationsData = new SmUpdateOperationsDataExtractor(targetScenario)
      .extract(sourceScenario.getSousModeles().get(0), new SmUpdateOptions(), targetSousModele.getNom());
    new SmUpdateAction(targetSousModele, ccm).performImport(sourceScenario.getSousModeles().get(0), smUpdateOperationsData);

    DonCalcSansPrtBrancheSeuilLateral targetDcsp = (DonCalcSansPrtBrancheSeuilLateral) targetBranche.getDCSP().get(0);
    Assert.assertEquals(23, (int) targetDcsp.getElemSeuilAvecPdc().get(0).getLargeur());
    Assert.assertEquals(defaultCoefPdc, (int) targetDcsp.getElemSeuilAvecPdc().get(0).getCoefPdc());

    //coefPdc in
    targetDcsp.getElemSeuilAvecPdc().get(0).setCoefPdc(11);
    smUpdateOperationsData = new SmUpdateOperationsDataExtractor(targetScenario)
      .extract(sourceScenario.getSousModeles().get(0), new SmUpdateOptions(), targetSousModele.getNom());
    new SmUpdateAction(targetSousModele, ccm).performImport(sourceScenario.getSousModeles().get(0), smUpdateOperationsData);

    targetDcsp = (DonCalcSansPrtBrancheSeuilLateral) targetBranche.getDCSP().get(0);
    Assert.assertEquals(23, (int) targetDcsp.getElemSeuilAvecPdc().get(0).getLargeur());
    Assert.assertEquals("coef est gardé car meme nombre et largeur", 11, (int) targetDcsp.getElemSeuilAvecPdc().get(0).getCoefPdc());

    soureDcsp.getElemSeuilAvecPdc().get(0).setLargeur(25);
    smUpdateOperationsData = new SmUpdateOperationsDataExtractor(targetScenario)
      .extract(sourceScenario.getSousModeles().get(0), new SmUpdateOptions(), targetSousModele.getNom());
    new SmUpdateAction(targetSousModele, ccm).performImport(sourceScenario.getSousModeles().get(0), smUpdateOperationsData);
    targetDcsp = (DonCalcSansPrtBrancheSeuilLateral) targetBranche.getDCSP().get(0);
    Assert.assertEquals(25, (int) targetDcsp.getElemSeuilAvecPdc().get(0).getLargeur());
    Assert.assertEquals("coef changé car largeur changee", defaultCoefPdc, (int) targetDcsp.getElemSeuilAvecPdc().get(0).getCoefPdc());
    targetDcsp.getElemSeuilAvecPdc().get(0).setCoefPdc(34);

    final ElemSeuilAvecPdc elemSeuilAvecPdc = new ElemSeuilAvecPdc(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    elemSeuilAvecPdc.setCoefD(20);
    soureDcsp.getElemSeuilAvecPdc().add(elemSeuilAvecPdc);
    smUpdateOperationsData = new SmUpdateOperationsDataExtractor(targetScenario)
      .extract(sourceScenario.getSousModeles().get(0), new SmUpdateOptions(), targetSousModele.getNom());
    new SmUpdateAction(targetSousModele, ccm).performImport(sourceScenario.getSousModeles().get(0), smUpdateOperationsData);
    targetDcsp = (DonCalcSansPrtBrancheSeuilLateral) targetBranche.getDCSP().get(0);
    Assert.assertEquals(defaultCoefPdc, (int) targetDcsp.getElemSeuilAvecPdc().get(0).getCoefPdc());
    Assert.assertEquals(20, (int) targetDcsp.getElemSeuilAvecPdc().get(1).getCoefD());

    targetDcsp.getElemSeuilAvecPdc().get(0).setCoefPdc(34);
    smUpdateOperationsData = new SmUpdateOperationsDataExtractor(targetScenario)
      .extract(sourceScenario.getSousModeles().get(0), new SmUpdateOptions(), targetSousModele.getNom());
    new SmUpdateAction(targetSousModele, ccm).performImport(sourceScenario.getSousModeles().get(0), smUpdateOperationsData);
    targetDcsp = (DonCalcSansPrtBrancheSeuilLateral) targetBranche.getDCSP().get(0);
    Assert.assertEquals(34, (int) targetDcsp.getElemSeuilAvecPdc().get(0).getCoefPdc());

    testScenarioConsistant("source", sourceScenario);
    testScenarioConsistant("target", targetScenario);
  }

  @Test
  public void testBrancheTypeChanged() {
    final EMHScenario sourceScenario = createScenario("1", EnumBrancheType.EMHBrancheSeuilLateral);
    final EMHScenario targetScenario = createScenario("1", EnumBrancheType.EMHBrancheSaintVenant);
    final EMHSousModele targetSousModele = targetScenario.getSousModeles().get(0);
    SmUpdateAction action = new SmUpdateAction(targetSousModele, ccm);
    final SmUpdateOperationsData smUpdateOperationsData = new SmUpdateOperationsDataExtractor(targetScenario)
      .extract(sourceScenario.getSousModeles().get(0), new SmUpdateOptions(), targetSousModele.getNom());
    action.performImport(sourceScenario.getSousModeles().get(0), smUpdateOperationsData);

    Assert.assertEquals(2, targetScenario.getSections().size());
    Assert.assertEquals("St_1_Am", targetScenario.getSections().get(0).getNom());
    Assert.assertEquals("St_1_Av", targetScenario.getSections().get(1).getNom());

    Assert.assertEquals(2, targetScenario.getNoeuds().size());
    Assert.assertEquals("Nd_0001", targetScenario.getNoeuds().get(0).getNom());
    Assert.assertEquals("Nd_0002", targetScenario.getNoeuds().get(1).getNom());

    Assert.assertEquals(1, targetScenario.getBranches().size());
    Assert.assertEquals(EnumBrancheType.EMHBrancheSeuilLateral, targetScenario.getBranches().get(0).getBrancheType());
    Assert.assertEquals("Br_1", targetScenario.getBranches().get(0).getNom());
    Assert.assertEquals(2, targetScenario.getBranches().get(0).getSections().size());
    Assert.assertEquals("St_1_Am", targetScenario.getBranches().get(0).getSections().get(0).getEmhNom());
    Assert.assertEquals(EnumSectionType.EMHSectionSansGeometrie, targetScenario.getBranches().get(0).getSections().get(0).getEmh().getSectionType());
    Assert.assertEquals("St_1_Av", targetScenario.getBranches().get(0).getSections().get(1).getEmhNom());
    Assert.assertEquals(EnumSectionType.EMHSectionSansGeometrie, targetScenario.getBranches().get(0).getSections().get(1).getEmh().getSectionType());

    testScenarioConsistant("source", sourceScenario);
    testScenarioConsistant("target", targetScenario);
  }

  @Test
  public void testCoefSaintVenantIsKept() {
    final EMHScenario sourceScenario = createScenario("1", EnumBrancheType.EMHBrancheSaintVenant);
    final EMHScenario targetScenario = createScenario("1", EnumBrancheType.EMHBrancheSaintVenant);
    final EMHSousModele targetSousModele = targetScenario.getSousModeles().get(0);

    createSectionInBr1(sourceScenario, sourceScenario.getSousModeles().get(0), "St_Imported", EnumSectionType.EMHSectionProfil);
    createSectionInBr1(targetScenario, targetSousModele, "St_Ignored", EnumSectionType.EMHSectionProfil);

    ((RelationEMHSectionDansBrancheSaintVenant) targetScenario.getBranches().get(0).getSections().get(0)).setCoefConv(3d);
    ((RelationEMHSectionDansBrancheSaintVenant) targetScenario.getBranches().get(0).getSections().get(2)).setCoefDiv(4d);

    SmUpdateAction action = new SmUpdateAction(targetSousModele, ccm);
    final SmUpdateOperationsData smUpdateOperationsData = new SmUpdateOperationsDataExtractor(targetScenario)
      .extract(sourceScenario.getSousModeles().get(0), new SmUpdateOptions(), targetSousModele.getNom());
    action.performImport(sourceScenario.getSousModeles().get(0), smUpdateOperationsData);

    Assert.assertEquals(1, targetScenario.getBranches().size());
    Assert.assertEquals("Br_1", targetScenario.getBranches().get(0).getNom());
    Assert.assertEquals(3, targetScenario.getBranches().get(0).getSections().size());
    Assert.assertEquals("St_1_Am", targetScenario.getBranches().get(0).getSections().get(0).getEmhNom());
    Assert.assertEquals("St_Imported", targetScenario.getBranches().get(0).getSections().get(1).getEmhNom());
    Assert.assertEquals("St_1_Av", targetScenario.getBranches().get(0).getSections().get(2).getEmhNom());

    //Coefs conservés
    Assert.assertEquals(3, (int) ((RelationEMHSectionDansBrancheSaintVenant) targetScenario.getBranches().get(0).getSections().get(0)).getCoefConv());
    Assert.assertEquals(4, (int) ((RelationEMHSectionDansBrancheSaintVenant) targetScenario.getBranches().get(0).getSections().get(2)).getCoefDiv());

    testScenarioConsistant("source", sourceScenario);
    testScenarioConsistant("target", targetScenario);
  }

  public void createSectionInBr1(EMHScenario targetScenario, EMHSousModele targetSousModele, String sectionName, EnumSectionType type) {
    createSection(targetScenario, targetSousModele, "Br_1", sectionName, type);
  }

  public void createSection(EMHScenario targetScenario, EMHSousModele targetSousModele, String brancheName, String sectionName, EnumSectionType type) {
    ListRelationSectionContent content = new ListRelationSectionContent();
    content.setBrancheNom(brancheName);
    content.setNom(sectionName);
    content.setAbscisseHydraulique(10);
    final ListRelationSectionContent.SectionData section = new ListRelationSectionContent.SectionData();
    section.setSectionType(type);
    content.setSection(section);
    creator.createSections(targetSousModele, Collections.singleton(content), ccm);
    Assert.assertTrue(targetScenario.getIdRegistry().getEmhByNom().containsKey(content.getNom()));
  }

  @Test
  public void testAddBranche() {
    final EMHScenario sourceScenario = createScenario("2", EnumBrancheType.EMHBrancheSaintVenant);
    final EMHScenario targetScenario = createScenario("1", EnumBrancheType.EMHBrancheSaintVenant);
    final EMHSousModele targetSousModele = targetScenario.getSousModeles().get(0);

    ((DonCalcSansPrtBrancheSaintVenant) sourceScenario.getBranches().get(0).getDCSP().get(0)).setCoefRuisQdm(100);

    SmUpdateAction action = new SmUpdateAction(targetSousModele, ccm);
    final SmUpdateOperationsData smUpdateOperationsData = new SmUpdateOperationsDataExtractor(targetScenario)
      .extract(sourceScenario.getSousModeles().get(0), new SmUpdateOptions(), targetSousModele.getNom());
    action.performImport(sourceScenario.getSousModeles().get(0), smUpdateOperationsData);

    Assert.assertEquals(2, targetScenario.getBranches().size());
    Assert.assertEquals("Br_1", targetScenario.getBranches().get(0).getNom());
    Assert.assertEquals("Br_2", targetScenario.getBranches().get(1).getNom());
    Assert.assertEquals(1, targetScenario.getBranches().get(1).getDCSP().size());
    Assert.assertEquals(100, ((DonCalcSansPrtBrancheSaintVenant) targetScenario.getBranches().get(1).getDCSP().get(0)).getCoefRuisQdm(), 1e-3);

    Assert.assertEquals(4, targetScenario.getSections().size());
    Assert.assertEquals("St_1_Am", targetScenario.getSections().get(0).getNom());
    Assert.assertEquals("St_1_Av", targetScenario.getSections().get(1).getNom());
    Assert.assertEquals("St_2_Am", targetScenario.getSections().get(2).getNom());
    Assert.assertEquals("St_2_Av", targetScenario.getSections().get(3).getNom());

    Assert.assertEquals(2, targetScenario.getNoeuds().size());
    Assert.assertEquals("Nd_0001", targetScenario.getNoeuds().get(0).getNom());
    Assert.assertEquals("Nd_0002", targetScenario.getNoeuds().get(1).getNom());

    //les coefficient sont conserves
    ((DonCalcSansPrtBrancheSaintVenant) targetScenario.getBranches().get(1).getDCSP().get(0)).setCoefRuisQdm(200);
    action.performImport(sourceScenario.getSousModeles().get(0), smUpdateOperationsData);
    Assert.assertEquals(200, ((DonCalcSansPrtBrancheSaintVenant) targetScenario.getBranches().get(1).getDCSP().get(0)).getCoefRuisQdm(), 1e-3);

    testScenarioConsistant("source", sourceScenario);
    testScenarioConsistant("target", targetScenario);
  }
}
