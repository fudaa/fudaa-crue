package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateSourceTargetData;
import org.junit.Test;

import static org.junit.Assert.*;

public class SmUpdateBrancheSameRelationSectionPredicateTest {
  private final SmUpdateBrancheSameRelationSectionPredicate tester = new SmUpdateBrancheSameRelationSectionPredicate(
    TestCoeurConfig.INSTANCE.getCrueConfigMetier());

  @Test
  public void notSameTypeIsDifferent() {
    EMHBrancheOrifice source = new EMHBrancheOrifice("Br_1");
    EMHBranchePdc target = new EMHBranchePdc("Br_1");
    assertFalse(tester.test(new SmUpdateSourceTargetData.Line<>(source, target)));
  }

  @Test
  public void sameNoSections() {
    EMHBrancheOrifice source = new EMHBrancheOrifice("Br_1");
    EMHBrancheOrifice target = new EMHBrancheOrifice("Br_1");
    assertTrue(tester.test(new SmUpdateSourceTargetData.Line<>(source, target)));
  }

  @Test
  public void sameSectionInterpoleInTarget() {
    EMHBrancheOrifice source = new EMHBrancheOrifice("Br_1");
    addSection(source, false, "St_1", 0, EnumPosSection.AMONT);
    addSection(source, false, "St_1.1", 1, EnumPosSection.INTERNE);
    addSection(source, false, "St_2", 10, EnumPosSection.AVAL);

    EMHBrancheOrifice target = new EMHBrancheOrifice("Br_1");
    addSection(target, false, "St_1", 0, EnumPosSection.AMONT);
    addSection(target, false, "St_1.1", 1, EnumPosSection.INTERNE);
    addSection(target, true, "St_1Bis", 5, EnumPosSection.INTERNE);
    addSection(target, false, "St_2", 10, EnumPosSection.AVAL);

    assertTrue(tester.test(new SmUpdateSourceTargetData.Line<>(source, target)));
  }

  @Test
  public void notSameBecauseNotSameLength() {
    EMHBrancheOrifice source = new EMHBrancheOrifice("Br_1");
    addSection(source, false, "St_1", 0, EnumPosSection.AMONT);
    addSection(source, false, "St_2", 10, EnumPosSection.AVAL);

    EMHBrancheOrifice target = new EMHBrancheOrifice("Br_1");
    addSection(target, false, "St_1", 0, EnumPosSection.AMONT);
    addSection(target, false, "St_2", 12, EnumPosSection.AVAL);

    assertFalse(tester.test(new SmUpdateSourceTargetData.Line<>(source, target)));
  }

  @Test
  public void notSameBecauseNotSameSection() {
    EMHBrancheOrifice source = new EMHBrancheOrifice("Br_1");
    addSection(source, false, "St_1", 0, EnumPosSection.AMONT);
    addSection(source, false, "St_1.1", 1, EnumPosSection.INTERNE);
    addSection(source, false, "St_2", 10, EnumPosSection.AVAL);

    EMHBrancheOrifice target = new EMHBrancheOrifice("Br_1");
    addSection(target, false, "St_1", 0, EnumPosSection.AMONT);
    addSection(target, false, "St_1.1", 2, EnumPosSection.INTERNE);
    addSection(target, false, "St_2", 12, EnumPosSection.AVAL);

    assertFalse(tester.test(new SmUpdateSourceTargetData.Line<>(source, target)));
  }

  @Test
  public void notSameBecauseNotSameSectionName() {
    EMHBrancheOrifice source = new EMHBrancheOrifice("Br_1");
    addSection(source, false, "St_1", 0, EnumPosSection.AMONT);
    addSection(source, false, "St_1.1", 1, EnumPosSection.INTERNE);
    addSection(source, false, "St_2", 10, EnumPosSection.AVAL);

    EMHBrancheOrifice target = new EMHBrancheOrifice("Br_1");
    addSection(target, false, "St_1", 0, EnumPosSection.AMONT);
    addSection(target, false, "St_1.1b", 1, EnumPosSection.INTERNE);
    addSection(target, false, "St_2", 12, EnumPosSection.AVAL);

    assertFalse(tester.test(new SmUpdateSourceTargetData.Line<>(source, target)));
  }

  @Test
  public void notSameBecauseNotSameNumberOfSection() {
    EMHBrancheOrifice source = new EMHBrancheOrifice("Br_1");
    addSection(source, false, "St_1", 0, EnumPosSection.AMONT);
    addSection(source, false, "St_2", 10, EnumPosSection.AVAL);

    EMHBrancheOrifice target = new EMHBrancheOrifice("Br_1");
    addSection(source, false, "St_1", 0, EnumPosSection.AMONT);
    addSection(source, false, "St_1.1", 2, EnumPosSection.INTERNE);
    addSection(source, false, "St_2", 12, EnumPosSection.AVAL);

    assertFalse(tester.test(new SmUpdateSourceTargetData.Line<>(source, target)));
  }

  public static RelationEMHSectionDansBranche addSection(CatEMHBranche branche, boolean interpolee, String name, double xp, EnumPosSection pos) {
    final CatEMHSection section = interpolee ? new EMHSectionInterpolee(name) : new EMHSectionProfil(name);
    final RelationEMHSectionDansBranche relation = EMHRelationFactory
      .createSectionDansBrancheAndSetBrancheContientSection(branche, false, section, TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    relation.setXp(xp);
    relation.setPos(pos);
    branche.addRelationEMH(relation);
    return relation;
  }
}
