package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;

import java.util.List;
import java.util.stream.Collectors;

public class SmBuilderForTest {
  final String brancheName;
  final String nd1Name;
  final String nd2Name;
  final String ndUsedByCasierName;
  final String casierName;
  final String sectionHorBrancheName;
  final String section1Name;
  final String section2Name;
  final String ndNotUSedName;
  private final EMHSousModele sousModele;

  public SmBuilderForTest(String suffix) {
    brancheName = "Br_1" + suffix;
    nd1Name = "Nd_1" + suffix;
    nd2Name = "Nd_2" + suffix;
    ndUsedByCasierName = "Nd_3" + suffix;
    casierName = "Ca_1" + suffix;
    sectionHorBrancheName = "St_HorBranche" + suffix;
    section1Name = "St_1" + suffix;
    section2Name = "St_2" + suffix;
    ndNotUSedName = "Nd_4" + suffix;
    sousModele = createSousModele();
  }

  public List<EMH> getAllSimpleEMHUpdatedOrAdded() {
    final List<EMH> allSimpleEMH = getSousModele().getAllSimpleEMH();
    return allSimpleEMH.stream().filter(emh -> !emh.getNom().equals(ndNotUSedName)).sorted(ObjetNommeByNameComparator.INSTANCE).collect(Collectors.toList());
  }

  public EMHSousModele getSousModele() {
    return sousModele;
  }

  private EMHSousModele createSousModele() {
    EMHSousModele sousModele = new EMHSousModele();
    final EMHBrancheOrifice br = new EMHBrancheOrifice(brancheName);
    final CatEMHNoeud nd1 = new EMHNoeudNiveauContinu(nd1Name);
    final CatEMHNoeud nd2 = new EMHNoeudNiveauContinu(nd2Name);
    final CatEMHNoeud ndUsedByCasier = new EMHNoeudNiveauContinu(ndUsedByCasierName);

    final CatEMHNoeud nd4 = new EMHNoeudNiveauContinu(ndNotUSedName);
    EMHSectionSansGeometrie sectionHorsBranche = new EMHSectionSansGeometrie(sectionHorBrancheName);
    EMHSectionSansGeometrie section1 = new EMHSectionSansGeometrie(section1Name);
    EMHSectionSansGeometrie section2 = new EMHSectionSansGeometrie(section2Name);
    EMHCasierProfil casierProfil = new EMHCasierProfil(casierName);
    EMHRelationFactory.addRelationContientEMH(sousModele, sectionHorsBranche);
    EMHRelationFactory.addRelationContientEMH(sousModele, section1);
    EMHRelationFactory.addRelationContientEMH(sousModele, section2);
    EMHRelationFactory.addRelationContientEMH(sousModele, br);
    EMHRelationFactory.addRelationContientEMH(sousModele, nd1);
    EMHRelationFactory.addRelationContientEMH(sousModele, nd2);
    EMHRelationFactory.addRelationContientEMH(sousModele, ndUsedByCasier);
    EMHRelationFactory.addRelationContientEMH(sousModele, nd4);
    EMHRelationFactory.addRelationContientEMH(sousModele, casierProfil);

    EMHRelationFactory.addNoeudCasier(casierProfil, ndUsedByCasier);
    EMHRelationFactory.addNoeudAval(br, nd1);
    EMHRelationFactory.addNoeudAmont(br, nd2);
    EMHRelationFactory.addBrancheContientSection(br, section1);
    EMHRelationFactory.addBrancheContientSection(br, section2);
    br.addRelationEMH(EMHRelationFactory.createSectionDansBranche(br, TestCoeurConfig.INSTANCE.getCrueConfigMetier(), section1));
    br.addRelationEMH(EMHRelationFactory.createSectionDansBranche(br, TestCoeurConfig.INSTANCE.getCrueConfigMetier(), section2));
    return sousModele;
  }
}
