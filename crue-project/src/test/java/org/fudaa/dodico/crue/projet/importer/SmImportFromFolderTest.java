package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.EMHProjectInfos;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;
import org.fudaa.dodico.crue.projet.planimetry.GisLayerId;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SmImportFromFolderTest {
  private static File dir;
  private static File etuDir;

  @BeforeClass
  public static void createMainTempDir() throws Exception {
    dir = CtuluLibFile.createTempDir();
    etuDir = new File(dir, "etu");
    etuDir.mkdir();
  }

  @AfterClass
  public static void deleteMainTempDir() throws Exception {
    CtuluLibFile.deleteDir(dir);
  }

  @Test
  public void importSmModele() throws IOException {
    final EMHProjet emhProjet = createProjet("Sm_m1");
    String radical = "m2";
    File drsoFile = new File(dir, radical + "." + CrueFileType.DRSO.getExtension());
    File dptgFile = new File(dir, radical + "." + CrueFileType.DPTG.getExtension());
    File dfrtFile = new File(dir, radical + "." + CrueFileType.DFRT.getExtension());
    File dcspFile = new File(dir, radical + "." + CrueFileType.DCSP.getExtension());
    Assert.assertTrue(drsoFile.createNewFile());
    Assert.assertTrue(dptgFile.createNewFile());
    Assert.assertTrue(dfrtFile.createNewFile());
    Assert.assertTrue(dcspFile.createNewFile());

    //GIS File
    File source = new File(new File(dir, "Config"), "SM_M2");
    Assert.assertTrue(source.mkdirs());
    File noeuds = new File(source, GisLayerId.NOEUDS.getShpFilename());
    noeuds.createNewFile();

    new SmImportFromFolder(emhProjet,null).importSousModele(drsoFile, "comment",false);

    Assert.assertTrue(new File(etuDir, drsoFile.getName()).exists());
    Assert.assertTrue(new File(etuDir, dptgFile.getName()).exists());
    Assert.assertTrue(new File(etuDir, dfrtFile.getName()).exists());
    Assert.assertTrue(new File(etuDir, dcspFile.getName()).exists());

    final ManagerEMHSousModele sousModele = emhProjet.getSousModele("Sm_m2");
    Assert.assertNotNull(sousModele);
    Assert.assertEquals("comment", sousModele.getInfosVersions().getCommentaire());
    final List<FichierCrue> fichiers = sousModele.getListeFichiers().getFichiers();
    Assert.assertEquals(4, fichiers.size());
    Assert.assertNotNull(FichierCrue.findById("m2." + CrueFileType.DRSO.getExtension(), fichiers));
    Assert.assertNotNull(FichierCrue.findById("m2." + CrueFileType.DPTG.getExtension(), fichiers));
    Assert.assertNotNull(FichierCrue.findById("m2." + CrueFileType.DFRT.getExtension(), fichiers));
    Assert.assertNotNull(FichierCrue.findById("m2." + CrueFileType.DCSP.getExtension(), fichiers));

    Assert.assertTrue(new File(emhProjet.getInfos().getDirOfConfig(sousModele), GisLayerId.NOEUDS.getShpFilename()).exists());
  }

  private EMHProjet createProjet(String... sousModeleNames) {
    final EMHProjet projet = new EMHProjet();
    EMHProjectInfos targetInfos = new EMHProjectInfos();
    projet.setInfos(targetInfos);
    Map<String, String> directories = new HashMap<>();
    directories.put(EMHProjectInfos.FICHETUDES, etuDir.getAbsolutePath());
    directories.put(EMHProjectInfos.CONFIG, new File(etuDir, "Config").getAbsolutePath());
    projet.getInfos().setDirectories(directories);

    for (String sousModeleName : sousModeleNames) {
      final ManagerEMHSousModele sousModele = new ManagerEMHSousModele(sousModeleName);
      createFichierCrue(projet, sousModeleName, CrueFileType.DRSO, sousModele);
      createFichierCrue(projet, sousModeleName, CrueFileType.DCSP, sousModele);
      createFichierCrue(projet, sousModeleName, CrueFileType.DFRT, sousModele);
      createFichierCrue(projet, sousModeleName, CrueFileType.DPTG, sousModele);
      projet.addBaseSousModele(sousModele);
    }
    return projet;
  }

  private static FichierCrue createFichierCrue(EMHProjet projet, final String name, final CrueFileType type, final ManagerEMHSousModele sousModele) {
    final FichierCrue fichierCrue = new FichierCrue(name + "." + type.getExtension(), name + "." + type.getExtension(), type);
    sousModele.addFichierDonnees(fichierCrue);
    projet.getInfos().addCrueFileToProject(fichierCrue);
    return fichierCrue;
  }
}
