package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class SmUpdateSourceDrsoLoaderTest extends AbstractTestParent {
  @Test
  public void loadAllFilesExist() {
    File exportZipInTempDir = exportZipInTempDir("/projet/Etu3-0_1.1.2.zip");
    File drsoFile = new File(exportZipInTempDir, "M3-0_c10.drso.xml");
    Assert.assertTrue(drsoFile.exists());
    SmUpdateSourceDrsoLoader updater = new SmUpdateSourceDrsoLoader(TestCoeurConfig.INSTANCE);
    final CrueOperationResult<EMHSousModele> load = updater.load(drsoFile);
    Assert.assertNotNull(load.getResult());
    Assert.assertFalse(load.getLogs().containsFatalError());
  }

  @Test
  public void errorInLoadDfrtFileNotExists() {
    File exportZipInTempDir = exportZipInTempDir("/projet/Etu3-0_1.1.2.zip");
    File drsoFile = new File(exportZipInTempDir, "M3-0_c10-2.drso.xml");
    Assert.assertTrue(drsoFile.exists());
    SmUpdateSourceDrsoLoader updater = new SmUpdateSourceDrsoLoader(TestCoeurConfig.INSTANCE);
    final CrueOperationResult<EMHSousModele> load = updater.load(drsoFile);
    Assert.assertNotNull(load.getResult());
    Assert.assertEquals("io.FileNotFoundException.error", load.getLogs().getLogs().get(0).getRecords().get(0).getMsg());
    Assert.assertEquals(new File(exportZipInTempDir, "M3-0_c10-2.dfrt.xml").getAbsolutePath(), load.getLogs().getLogs().get(0).getRecords().get(0).getArgs()[0]);
  }
}
