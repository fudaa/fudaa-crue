/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.integration;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.common.ErrorBilanTester;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;
import java.util.Arrays;

import static org.junit.Assert.*;

/**
 *
 * @author deniger
 */
@Category(org.fudaa.dodico.crue.test.TestIntegration.class)
public class TestIntegrationEtu30 {

  static File target;
  static EMHProjet projet;

  public TestIntegrationEtu30() {
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
    target = CtuluLibFile.createTempDir();
    AbstractTestParent.exportZip(target, "/integration/v1_1_1/Etu3-0.zip");
    final File file = new File(new File(target, "Etu3-0"), "Etu3-0.etu.xml");
    assertTrue(file.exists());
    projet = IntegrationHelper.loadEtu(file, TestCoeurConfig.INSTANCE_1_1_1);
    assertNotNull(projet);
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
    CtuluLibFile.deleteDir(target);
  }

  @Test
  public void testSc_Poub_c10() {
    IntegrationHelper.exportToCrue10(getEtuFile().getParentFile(), projet, "Sc_Vierge_c10", "Sc_Poub_c10", "Poub_c10", true, 0);
    IntegrationHelper.testScenario(projet, "Sc_Poub_c10", new ErrorBilanTester(0, 0, 0, 6));
    IntegrationHelper.exportToCrue10(getEtuFile().getParentFile(), projet, "Sc_M3-0_v9c10", "Sc_Poub_c10", "Poub_c10", true, 0);
  }

  @Test
  public void testM30_c10c10() {
    IntegrationHelper.exportToCrue10(getEtuFile().getParentFile(), projet, "Sc_M3-0_c10", "Sc_M3-0_c10c10", "M3-0_c10c10", true, 0);
    IntegrationHelper.testScenario(projet, "Sc_M3-0_c10c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testRuns() {
    IntegrationHelper.exportToCrue9(getEtuFile().getParentFile(), projet, "Sc_M3-0_c9", "Sc_M3-0_c9c9", "M3-0_c9c9",
            Arrays.asList("RPTG1.1", "RPTG1.2", "RPTG1.3", "RPTG1.4", "RPTG2.1", "RPTG2.2", "RPTG2.3", "RPTG2.4",
            "RCAL1", "RCAL3", "RCAL4.1", "RCAL4.2", "RPTR2"));
    final EMHScenarioContainer runv8 = IntegrationHelper.loadRun("Sc_M3-0_v8c9", projet, 0);
    final EMHScenarioContainer runc9 = IntegrationHelper.loadRun("Sc_M3-0_c9c9", projet, 0);
    assertNotNull(runv8.getEmhScenario().getModeles().get(0).getResultatCalculPasDeTemps());
    assertEquals(25, runv8.getEmhScenario().getModeles().get(0).getResultatCalculPasDeTemps().getNbResultats());
    assertNotNull(runc9.getEmhScenario().getModeles().get(0).getResultatCalculPasDeTemps());
    assertTrue(IntegrationHelper.compare(projet, runv8.getEmhScenario(), runc9.getEmhScenario()).size() >= 1);
  }

  @Test
  public void testM30_c9c10() {
    IntegrationHelper.exportToCrue10(getEtuFile().getParentFile(), projet, "Sc_M3-0_c9", "Sc_M3-0_c9c10", "M3-0_c9c10",
            Arrays.asList("RPTG1.1", "RPTG1.2", "RPTG1.3", "RPTG1.4", "RPTG2.1", "RPTG2.2", "RPTG2.3", "RPTG2.4", "COM5", "OPTG4", "RPTR2", "COM1", "OPTR1", "COM3", "RCAL1",
            "OPTI6",
            "OCAL4", "RCAL3", "RCAL4.2", "RCAL4.1"));
    IntegrationHelper.testScenario(projet, "Sc_M3-0_c9c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testPoubc9() {
    IntegrationHelper.exportToCrue9(getEtuFile().getParentFile(), projet, "Sc_M3_v1c9", "Sc_Poub_c9", "Poub_c9", true, 0);
    IntegrationHelper.testScenario(projet, "Sc_Poub_c9", new ErrorBilanTester(0, 0, 0, 0));
    IntegrationHelper.exportToCrue9(getEtuFile().getParentFile(), projet, "Sc_M3-0_v9c10", "Sc_Poub_c9", "Poub_c9",
            Arrays.asList("COM5", "DPTG1.2", "DFRT4", "COM1", "COM3", "DLHY3", "OPTR1"));

  }

  @Test
  public void testSc_M30_c9c9() {
    IntegrationHelper.exportToCrue9(getEtuFile().getParentFile(), projet, "Sc_M3-0_c9", "Sc_M3-0_c9c9", "M3-0_c9c9",
            Arrays.asList("RPTG1.1", "RPTG1.2", "RPTG1.3", "RPTG1.4", "RPTG2.1", "RPTG2.2", "RPTG2.3", "RPTG2.4",
            "RCAL1", "RCAL3", "RCAL4.2", "RCAL4.1", "RPTR2"));
    IntegrationHelper.testScenario(projet, "Sc_M3-0_c9c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M3_c9c9() {
    IntegrationHelper.exportToCrue9(getEtuFile().getParentFile(), projet, "Sc_M3_c9", "Sc_M3_c9c9", "M3_c9c9", true, 0);
    IntegrationHelper.testScenario(projet, "Sc_M3_c9c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M30_c10c9() {
    IntegrationHelper.exportToCrue9(getEtuFile().getParentFile(), projet, "Sc_M3-0_c10", "Sc_M3-0_c10c9", "M3-0_c10c9",
            Arrays.asList("COM5", "DPTG1.2", "DFRT4", "COM1", "OPTR1", "COM3", "DLHY3"));

    IntegrationHelper.testScenario(projet, "Sc_M3-0_c10c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M30_v1c9c9() {
    IntegrationHelper.exportToCrue9(getEtuFile().getParentFile(), projet, "Sc_M3-0_v1c9", "Sc_M3-0_v1c9c9", "M3-0_v1c9c9", true, 0);
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v1c9c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_Vierge_c9() {
    IntegrationHelper.testScenario(projet, "Sc_Vierge_c9", new ErrorBilanTester(0, 0, 0, 6));
  }

  @Test
  public void testSc_M3_c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3_c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M3_v0c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3_v0c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M3_v1c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3_v1c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M3_v10c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3_v10c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M30_c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M30_v1c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v1c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M30_v2c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v2c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M30_v3c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v3c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M30_v4c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v4c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M30_v5c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v5c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M30_v6c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v6c9", new ErrorBilanTester(0, 0, 0, 6));
  }

  @Test
  public void testSc_M30_v7c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v7c9", new ErrorBilanTester(0, 0, 0, 1));
  }

  @Test
  public void testSc_M30_v8c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v8c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M30_v9c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v9c9", new ErrorBilanTester(0, 0, 2, 0));
  }

  @Test
  public void testSc_M30_v10c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v10c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M30_v11c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v11c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M30_v12c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v12c9", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_Vierge_c10() {
    IntegrationHelper.testScenario(projet, "Sc_Vierge_c10", new ErrorBilanTester(0, 0, 0, 6));
  }

  @Test
  public void testSc_M30_c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M30_v1c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v1c10", new ErrorBilanTester(0, 0, 5, 0));
  }

  @Test
  public void testSc_M30_v2c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v2c10", new ErrorBilanTester(0, 0, 1, 0));
  }

  @Test
  public void testSc_M30_v3c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v3c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M30_v4c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v4c10", new ErrorBilanTester(0, 0, 0, 6));
  }

  @Test
  public void testSc_M30_v6c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v6c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M30_v7c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v7c10", new ErrorBilanTester(0, 0, 3, 0));
  }

  @Test
  public void testSc_M30_v8c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v8c10", new ErrorBilanTester(0, 0, 1, 0));
  }

  @Test
  public void testSc_M30_v9c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v9c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M30_v10c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_v10c10", new ErrorBilanTester(0, 0, 0, 0));
  }

  @Test
  public void testSc_M3_e1c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3_e1c9", new ErrorBilanTester(1, 0, 1, 0));
  }

  @Test
  public void testSc_M3_e2c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3_e2c9", new ErrorBilanTester(6, 0, 0, 0));
  }

  @Test
  public void testSc_M30_e3c9() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_e3c9", new ErrorBilanTester(0, 10, 0, 0));
  }

  @Test
  public void testSc_M30_e1c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_e1c10", new ErrorBilanTester(1, 0, 0, 0));
  }

  @Test
  public void testSc_M30_e2c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_e2c10", new ErrorBilanTester(5, 0, 0, 0));
  }

  @Test
  public void testSc_M30_e3c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_e3c10", new ErrorBilanTester(4, 0, 0, 0));
  }

  @Test
  public void testSc_M30_e4c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_e4c10", new ErrorBilanTester(1, 0, 0, 0));
  }

  @Test
  public void testSc_M30_e5c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_e5c10", new ErrorBilanTester(1, 0, 0, 0));
  }

  @Test
  public void testSc_M30_e6c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_e6c10", new ErrorBilanTester(1, 0, 0, 0));
  }

  @Test
  public void testSc_M30_e7c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_e7c10", new ErrorBilanTester(2, 2, 0, 0));
  }

  @Test
  public void testSc_M30_e8c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_e8c10", new ErrorBilanTester(10, 0, 0, 0));
  }

  @Test
  public void testSc_M30_e9c10() {
    IntegrationHelper.testScenario(projet, "Sc_M3-0_e9c10", new ErrorBilanTester(19, 0, 2, 1));
  }

  private File getEtuFile() {
    return projet.getInfos().getEtuFile();
  }
}
