package org.fudaa.dodico.crue.projet.importer;

import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.projet.importer.data.SmUpdateOptions;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class SmUpdateOptionsDataExtractorTest {
  private final SmBuilderForTest smBuilder = new SmBuilderForTest("");

  @Test
  public void extractAllSections() {
    SmUpdateOptionsDataExtractor extract = new SmUpdateOptionsDataExtractor(smBuilder.getSousModele());
    final SmUpdateOptionsDataExtractor.SmUpdateOptionsData result = extract.extract(new SmUpdateOptions(true, false, false));
    Assert.assertEquals(0, result.getRequiredEmh().size());
    final List<String> sections = TransformerHelper.toNom(result.getEmhToAddOrUpdate());
    sections.sort(null);
    Assert.assertEquals(Arrays.asList(smBuilder.section1Name, smBuilder.section2Name, smBuilder.sectionHorBrancheName), sections);
  }

  @Test
  public void extractCasiers() {
    SmUpdateOptionsDataExtractor extract = new SmUpdateOptionsDataExtractor(smBuilder.getSousModele());
    final SmUpdateOptionsDataExtractor.SmUpdateOptionsData result = extract.extract(new SmUpdateOptions(false, true, false));
    Assert.assertEquals(0, result.getRequiredEmh().size());
    final List<String> emhs = TransformerHelper.toNom(result.getEmhToAddOrUpdate());
    emhs.sort(null);
    Assert.assertEquals(Arrays.asList(smBuilder.casierName, smBuilder.ndUsedByCasierName), emhs);
  }

  @Test
  public void extractBranches() {
    SmUpdateOptionsDataExtractor extract = new SmUpdateOptionsDataExtractor(smBuilder.getSousModele());
    final SmUpdateOptionsDataExtractor.SmUpdateOptionsData result = extract.extract(new SmUpdateOptions(false, false, true));
    final List<String> requiredEmhs = TransformerHelper.toNom(result.getRequiredEmh());
    requiredEmhs.sort(null);
    Assert.assertEquals(Arrays.asList(smBuilder.section1Name, smBuilder.section2Name), requiredEmhs);
    final List<String> emhs = TransformerHelper.toNom(result.getEmhToAddOrUpdate());
    emhs.sort(null);
    Assert.assertEquals(Arrays.asList(smBuilder.brancheName, smBuilder.nd1Name, smBuilder.nd2Name, smBuilder.section1Name, smBuilder.section2Name), emhs);
  }

  @Test
  public void extractSectionsBranches() {
    SmUpdateOptionsDataExtractor extract = new SmUpdateOptionsDataExtractor(smBuilder.getSousModele());
    final SmUpdateOptionsDataExtractor.SmUpdateOptionsData result = extract.extract(new SmUpdateOptions(true, false, true));
    Assert.assertEquals(0, result.getRequiredEmh().size());
    final List<String> emhs = TransformerHelper.toNom(result.getEmhToAddOrUpdate());
    emhs.sort(null);
    Assert.assertEquals(Arrays.asList(smBuilder.brancheName, smBuilder.nd1Name, smBuilder.nd2Name, smBuilder.section1Name, smBuilder.section2Name, smBuilder.sectionHorBrancheName),
      emhs);
  }

  @Test
  public void extractAll() {
    SmUpdateOptionsDataExtractor extract = new SmUpdateOptionsDataExtractor(smBuilder.getSousModele());
    final SmUpdateOptionsDataExtractor.SmUpdateOptionsData result = extract.extract(new SmUpdateOptions(true, true, true));
    Assert.assertEquals(0, result.getRequiredEmh().size());
    final List<String> emhs = TransformerHelper.toNom(result.getEmhToAddOrUpdate());
    emhs.sort(null);
    Assert.assertEquals(Arrays
      .asList(smBuilder.brancheName, smBuilder.casierName, smBuilder.nd1Name, smBuilder.nd2Name, smBuilder.ndUsedByCasierName, smBuilder.section1Name,
        smBuilder.section2Name, smBuilder.sectionHorBrancheName), emhs);
  }
}
