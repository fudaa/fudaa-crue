/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.integration;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.comparaison.ExecuteComparaisonResult;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

/**
 *
 * @author deniger
 */
@Category(org.fudaa.dodico.crue.test.TestIntegration.class)
public class TestIntegrationEtu201 {

  static File target;
  static EMHProjet projet;

  public TestIntegrationEtu201() {
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
    target = CtuluLibFile.createTempDir();
    AbstractTestParent.exportZip(target, "/integration/Etu201.zip");
    final File file = new File(target, "Etu201.etu.xml");
    assertTrue(file.exists());
    projet = IntegrationHelper.loadEtu(file, TestCoeurConfig.INSTANCE_1_2);
    assertNotNull(projet);
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
    CtuluLibFile.deleteDir(target);

  }

  public static ExecuteComparaisonResult find(final List<ExecuteComparaisonResult> compare, final String id) {
    for (final ExecuteComparaisonResult executeComparaisonResult : compare) {
      if (id.equals(executeComparaisonResult.getId())) {
        return executeComparaisonResult;
      }
    }
    return null;
  }

  @Test
  public void testRuns() {

    final EMHScenarioContainer runc9 = IntegrationHelper.loadRun("Sc_M201-1_c9c9", projet, 0);
    final EMHScenarioContainer runc10 = IntegrationHelper.loadRun("Sc_M201-1_c9c9c10", projet, 0);
    assertNotNull(runc9.getEmhScenario().getModeles().get(0).getResultatCalculPasDeTemps());
    assertNotNull(runc9.getEmhScenario().getModeles().get(0).getResultatCalculPasDeTemps());
    final List<ExecuteComparaisonResult> compare = IntegrationHelper.compareResult(projet, runc9.getEmhScenario(), runc10.getEmhScenario());
    ExecuteComparaisonResult find = find(compare, "RCAL4.1");
    assertTrue(find.getNbDifferences()+ "differences found and expect more thant 344", find.getNbDifferences()>=344);
    find = find(compare, "RPTG1.4");
    assertEquals(13, find.getNbDifferences());
  }
}
