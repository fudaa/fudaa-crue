package org.fudaa.dodico.crue.validation.util;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

import java.util.*;

/**
 * Utiliser pour tester les CatEMHSection utilisee dans une branche.
 *
 * @author deniger
 */
public class ValidationContentSectionDansBrancheExecutor extends ValidationContentExecutor<Class<? extends CatEMHSection>> {
    private final Collection<EnumSectionType> sectionAuthorizedInAmontAval = createSectionTypeBrancheSaintVenantAmonAval();

    /**
     * @param ccm le {@link CrueConfigMetier}
     */
    public ValidationContentSectionDansBrancheExecutor(CrueConfigMetier ccm) {
        super(createSectionDansBrancheContents());
        setMsgIfCantContain("validation.branche.cantContainsThisSection");
    }

    /**
     * La branche peut utiliser une section du type
     *
     * @param aClass la classe de la section
     * @return resultat de la validation
     */
    private static ValidationContentParamDelegate<Class<? extends CatEMHSection>> canContain(
            final Class<? extends CatEMHSection> aClass) {
        return create(aClass, -1, -1);
    }

    private static ValidationContentParamDelegate<Class<? extends CatEMHSection>> create(
            Class<? extends CatEMHSection> classOfSection, int minOccurence, int maxOccurence) {
        ValidationContentParamDelegate<Class<? extends CatEMHSection>> res = new ValidationContentParamDelegate<>(
            classOfSection, minOccurence, maxOccurence);
        res.setMsgNbOccurenceNotEqualsTo("validation.branche.nbSectionNotEqualsTo");
        res.setMsgNbOccurenceNotEqualsToOne("validation.branche.nbSectionNotEqualsTo");
        res.setMsgNbOccurenceNotIn("validation.branche.nbSectionNotIn");
        return res;
    }

    private static List<ValidationContentParamDelegate<Class<? extends CatEMHSection>>> createList(
            final ValidationContentParamDelegate<Class<? extends CatEMHSection>>... params) {
        return Arrays.asList(params);
    }

    private static Collection<EnumSectionType> createSectionTypeBrancheSaintVenantAmonAval() {
        return Arrays.asList(EnumSectionType.EMHSectionProfil, EnumSectionType.EMHSectionIdem);
    }

    public static Map<EnumBrancheType, Collection<EnumSectionType>> createSectionTypeDansBrancheContents(boolean isSectionAmontAval) {
        final Map<EnumBrancheType, Collection<EnumSectionType>> res = createSectionInterneTypeDansBrancheContents();
        if (isSectionAmontAval) {
            res.put(EnumBrancheType.EMHBrancheSaintVenant, createSectionTypeBrancheSaintVenantAmonAval());
        }
        return res;
    }

    /**
     * Attention, ne prend pas en compte les règles pour les branches amont/aval
     * Cette map duplique un peu la map obtenu avec {@link #createSectionDansBrancheContents()}
     *
     * @return map des sections autorisés pour des types de branches
     */
    public static Map<EnumBrancheType, Collection<EnumSectionType>> createSectionInterneTypeDansBrancheContents() {
        Map<EnumBrancheType, Collection<EnumSectionType>> res = new EnumMap<>(EnumBrancheType.class);
        res.put(EnumBrancheType.EMHBranchePdc, Collections.singleton(EnumSectionType.EMHSectionSansGeometrie));
        res.put(EnumBrancheType.EMHBrancheSeuilTransversal, Arrays.asList(EnumSectionType.EMHSectionProfil, EnumSectionType.EMHSectionIdem));
        res.put(EnumBrancheType.EMHBrancheSeuilLateral, Collections.singleton(EnumSectionType.EMHSectionSansGeometrie));
        res.put(EnumBrancheType.EMHBrancheNiveauxAssocies, Collections.singleton(EnumSectionType.EMHSectionSansGeometrie));
        res.put(EnumBrancheType.EMHBrancheOrifice, Collections.singleton(EnumSectionType.EMHSectionSansGeometrie));
        res.put(EnumBrancheType.EMHBrancheStrickler, Arrays.asList(EnumSectionType.EMHSectionProfil, EnumSectionType.EMHSectionIdem));
        res.put(EnumBrancheType.EMHBrancheBarrageGenerique, Collections.singleton(EnumSectionType.EMHSectionSansGeometrie));
        res.put(EnumBrancheType.EMHBrancheBarrageFilEau, Arrays.asList(EnumSectionType.EMHSectionProfil, EnumSectionType.EMHSectionIdem));
        res.put(EnumBrancheType.EMHBrancheSaintVenant, Arrays.asList(EnumSectionType.EMHSectionProfil, EnumSectionType.EMHSectionIdem,
                EnumSectionType.EMHSectionInterpolee));
        return res;
    }

    /**
     * Tenir en cohérence avec createSectionDansBrancheContents
     */
    private static Map<Class<? extends EMH>, List<ValidationContentParamDelegate<Class<? extends CatEMHSection>>>> createSectionDansBrancheContents() {
        final Map<Class<? extends EMH>, List<ValidationContentParamDelegate<Class<? extends CatEMHSection>>>> res = new HashMap<>();
        res.put(EMHBranchePdc.class, createList(create(EMHSectionSansGeometrie.class, 2, 2)));
        res.put(EMHBrancheSeuilTransversal.class, createList(create(EMHSectionProfil.class, 0, 2), create(
                EMHSectionIdem.class, 0, 2)));
        res.put(EMHBrancheSeuilLateral.class, createList(create(EMHSectionSansGeometrie.class, 2, 2)));
        res.put(EMHBrancheNiveauxAssocies.class, createList(create(EMHSectionSansGeometrie.class, 2, 2)));
        res.put(EMHBrancheOrifice.class, createList(create(EMHSectionSansGeometrie.class, 2, 2)));
        res.put(EMHBrancheStrickler.class, createList(create(EMHSectionProfil.class, 0, 2), create(EMHSectionIdem.class, 0,
                2)));
        res.put(EMHBrancheBarrageGenerique.class, createList(create(EMHSectionSansGeometrie.class, 2, 2)));
        res.put(EMHBrancheBarrageFilEau.class, createList(create(EMHSectionProfil.class, 0, 2), create(
                EMHSectionIdem.class, 0, 2)));
        res.put(EMHBrancheSaintVenant.class, createList(canContain(EMHSectionProfil.class),
                canContain(EMHSectionIdem.class), canContain(EMHSectionInterpolee.class)));
        return res;
    }

    public static boolean requireSectionPilote(EnumBrancheType brancheType) {
        return EnumBrancheType.EMHBrancheBarrageFilEau.equals(brancheType)
                || EnumBrancheType.EMHBrancheBarrageGenerique.equals(brancheType);
    }

    private static boolean requireSectionPilote(CatEMHBranche emh) {
        EnumBrancheType brancheType = emh.getBrancheType();
        return requireSectionPilote(brancheType);
    }

    @Override
    public void validContent(CtuluLog emhLog, EMH emhContenant, List objectsToTests) {
        super.validContent(emhLog, emhContenant, objectsToTests);
        final boolean isSaintVenant = emhContenant.getClass().equals(EMHBrancheSaintVenant.class);

        //pour toutes les branches hormis saint-venant, il ne doit y avoir que 2 sections:
        if (!isSaintVenant && emhContenant.getCatType().equals(EnumCatEMH.BRANCHE)) {
            CatEMHBranche branche = (CatEMHBranche) emhContenant;
            List<RelationEMHSectionDansBranche> listeSections = branche.getListeSections();
            if (listeSections.size() != 2) {
                emhLog.addSevereError("validation.branche.mustContain2Section", branche.getNom(), branche.getBrancheType().geti18n());
            }
        }
        if (isSaintVenant) {
            CatEMHBranche branche = (CatEMHBranche) emhContenant;
            RelationEMHSectionDansBranche relationSectionAmont = branche.getSectionAmont();
            CatEMHSection sectionAmont = relationSectionAmont == null ? null : relationSectionAmont.getEmh();
            if (sectionAmont != null && (!sectionAuthorizedInAmontAval.contains(sectionAmont.getSectionType()))) {
                emhLog.addSevereError("validation.brancheSaintVenant.sectionAmontTypeWrong", branche.getNom(), sectionAmont.getNom(),
                        sectionAmont.getSectionType().geti18n());
            }
            RelationEMHSectionDansBranche relationSectionAval = branche.getSectionAval();
            CatEMHSection sectionAval = relationSectionAval == null ? null : relationSectionAval.getEmh();
            if (sectionAval != null && (!sectionAuthorizedInAmontAval.contains(sectionAval.getSectionType()))) {
                emhLog.addSevereError("validation.brancheSaintVenant.sectionAvalTypeWrong", branche.getNom(), sectionAval.getNom(),
                        sectionAval.getSectionType().geti18n());
            }
        }
        if (requireSectionPilote((CatEMHBranche) emhContenant)) {
            CatEMHSection sectionPilote = EMHHelper.getSectionPilote(emhContenant);
            if (sectionPilote == null) {
                emhLog.addSevereError("validation.branche.mustContainSectionPilote", emhContenant.getNom());
            } else if (emhContenant.getActuallyActive() && !sectionPilote.getActuallyActive()) {
                emhLog.addSevereError("validation.branche.isActiveButSectionPiloteUnactive", emhContenant.getNom(), sectionPilote.getNom());
            }
        }
    }
}
