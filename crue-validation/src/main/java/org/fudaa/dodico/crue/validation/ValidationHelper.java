package org.fudaa.dodico.crue.validation;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.compress.utils.Sets;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.common.PropertyUtilsCache;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.PropertyValidator;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.helper.CalcHelper;

import java.beans.PropertyDescriptor;
import java.util.*;

public class ValidationHelper {
  protected static void addAnalyze(final List<CtuluLog> res, final CtuluLog ana) {
    if (ana != null && !ana.isEmpty()) {
      res.add(ana);
    }
  }

  public static void validateAll(final CtuluLogGroup logs, final EMHScenario scenario,
                                 final Collection<CrueValidator> validators) {
    validateAll(logs, scenario, false, validators);
  }

  public static void validateAll(final CtuluLogGroup logs, final EMHScenario scenario,
                                 final boolean addOnlyNotEmptyLog, final Collection<CrueValidator> validators) {
    if (validators == null) {
      return;
    }
    PropertyUtilsCache.getInstance().setActive(true);
    for (final CrueValidator validator : validators) {
      final List<CtuluLog> validateScenario = validator.validateScenario(scenario);
      if (validateScenario != null) {
        if (addOnlyNotEmptyLog) {
          logs.addNotEmptyLogs(validateScenario);
        } else {
          logs.addAll(validateScenario);
        }
      }
    }
    PropertyUtilsCache.getInstance().setActive(false);
  }

  public static void validateAll(final CtuluLogGroup logs, final EMHScenario scenario,
                                 final CrueValidator... validators) {
    validateAll(logs, scenario, false, Arrays.asList(validators));
  }

  /**
   * @param objectToTest l'objet a tester
   * @param classList    les classes visées
   * @return true si l'objet est instanciable depuis une classe de la liste.
   */
  public static boolean isInstanceof(final Object objectToTest, final List<Class<? extends InfosEMH>> classList) {
    if (classList != null) {
      for (final Class<? extends InfosEMH> class1 : classList) {
        if (class1.isInstance(objectToTest)) {
          return true;
        }
      }
    }
    return false;
  }

  public static void validateObject(final String prefixe, final CtuluLog analyze, final Object o,
                                    final CrueConfigMetier propsContainer) {
    final PropertyDescriptor[] propertyDescriptors = PropertyUtils.getPropertyDescriptors(o.getClass());
    for (int i = 0; i < propertyDescriptors.length; i++) {
      final PropertyDescriptor desc = propertyDescriptors[i];
      try {
        final PropertyValidator r = propsContainer.getValidator(StringUtils.uncapitalize(desc.getName()));
        if (r != null) {
          final Object resObjet = PropertyUtilsCache.getInstance().getProperty(o, desc.getName());
          if (isLoi(resObjet)) {
            // pour les DonCalcSansPrt, les lois sont locales et non partagées:
            if (o instanceof DonCalcSansPrt) {
              ValidationHelperLoi.validateLoiFF((LoiFF) resObjet, propsContainer);
            }
          } else if (resObjet instanceof PdtVar) {
            validatePdtVar(prefixe, analyze, (PdtVar) resObjet, propsContainer);
          } else {
            r.validateValue(prefixe, resObjet, analyze);
          }
        }
      } catch (final Exception e) {
        analyze.manageException(e);
      }
    }
    validElemSeuil(o, analyze, propsContainer);
    validLoiCalc(o, analyze);
  }

  private static void validLoiCalc(final Object o, final CtuluLog analyze) {
    if (o instanceof CalcTransItem) {
      final CalcTransItem item = (CalcTransItem) o;
      Loi loi = item.getLoi();
      if (loi == null) {
        String name = item.getNomCalculParent() + ": " + item.getEmh().getNom();
        if (item instanceof DonCLimMCommonItem) {
          name = name + " / " + ((DonCLimMCommonItem) item).getShortName();
        } else {
          name = name + item.getClass().getSimpleName();
        }
        analyze.addSevereError("valid.NoLoiForTransCalc", name);
      }
    }
  }

  private static void validElemSeuil(final Object o, final CtuluLog analyze, final CrueConfigMetier propsContainer) {
    if (o.getClass().equals(DonCalcSansPrtBrancheSeuilLateral.class)) {
      DonCalcSansPrtBrancheSeuilLateral dcsp = (DonCalcSansPrtBrancheSeuilLateral) o;
      List<ElemSeuilAvecPdc> elemSeuils = dcsp.getElemSeuilAvecPdc();
      if (elemSeuils != null) {
        int idx = 0;
        for (ElemSeuilAvecPdc elemSeuil : elemSeuils) {
          validateObject(BusinessMessages.getString("valid.ElemSeuilAvecPdc", Integer.toString(++idx)), analyze, elemSeuil, propsContainer);
        }
      }
      if (CollectionUtils.isEmpty(elemSeuils)) {
        analyze.addSevereError("valid.ElemSeuilAvecPdcEmpy.Error");
      }
    } else if (o.getClass().equals(DonCalcSansPrtBrancheSeuilTransversal.class)) {
      DonCalcSansPrtBrancheSeuilTransversal dcsp = (DonCalcSansPrtBrancheSeuilTransversal) o;
      List<ElemSeuilAvecPdc> elemSeuils = dcsp.getElemSeuilAvecPdc();
      if (elemSeuils != null) {
        int idx = 0;
        for (ElemSeuilAvecPdc elemSeuil : elemSeuils) {
          validateObject(BusinessMessages.getString("valid.ElemSeuilAvecPdc", Integer.toString(++idx)), analyze, elemSeuil, propsContainer);
        }
      }
      if (CollectionUtils.isEmpty(elemSeuils)) {
        analyze.addSevereError("valid.ElemSeuilAvecPdcEmpy.Error");
      }
    } else if (o.getClass().equals(DonCalcSansPrtBrancheBarrageFilEau.class)) {
      DonCalcSansPrtBrancheBarrageFilEau dcsp = (DonCalcSansPrtBrancheBarrageFilEau) o;
      Collection<ElemBarrage> elemSeuils = dcsp.getElemBarrage();
      if (elemSeuils != null) {
        int idx = 0;
        for (ElemBarrage elemSeuil : elemSeuils) {
          validateObject(BusinessMessages.getString("valid.ElemBarrage", Integer.toString(++idx)), analyze, elemSeuil, propsContainer);
        }
      }
      if (CollectionUtils.isEmpty(elemSeuils)) {
        analyze.addSevereError("valid.ElemBarrageEmpty.Error");
      }
    }
  }

  public static void validatePdt(String prefixe, CtuluLog log, Pdt pdt, CrueConfigMetier ccm) {
    if (pdt instanceof PdtVar) {
      validatePdtVar(prefixe, log, (PdtVar) pdt, ccm);
    } else if (pdt instanceof PdtCst) {
      validateObject(prefixe, log, pdt, ccm);
    }
  }

  private static void validatePdtVar(String prefixe, CtuluLog log, PdtVar var, CrueConfigMetier ccm) {
    final List<ElemPdt> elems = var.getElemPdt();
    if (elems != null) {
      final int nb = elems.size();
      for (int iElem = 0; iElem < nb; iElem++) {
        validateObject(prefixe + " PdtVar " + (iElem + 1), log, elems.get(iElem), ccm);
      }
    }
    if (elems == null || elems.isEmpty()) {
      log.addError("validation.pdtVar.OneElemIsRequired", prefixe + " PdtVar ");
    }
  }

  public static CtuluLog validatePNUM(final ParamNumModeleBase pnum, final CrueConfigMetier ccm) {
    final CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    res.setDesc("ParamNumModeleBase");
    validateObject(StringUtils.EMPTY, res, pnum, ccm);
    if (pnum.getParamNumCalcPseudoPerm() != null) {
      validateObject("ParamNumCalcPseudoPerm", res, pnum.getParamNumCalcPseudoPerm(), ccm);
    }
    if (pnum.getParamNumCalcTrans() != null) {
      validateObject("ParamNumCalcTrans", res, pnum.getParamNumCalcTrans(), ccm);
    }
    if (pnum.getParamNumCalcVraiPerm() != null) {
      validateObject("ParamNumCalcVraiPerm", res, pnum.getParamNumCalcVraiPerm(), ccm);
    }

    return res;
  }

  private static boolean isLoi(Object o) {
    return o != null && (o.getClass().equals(LoiDF.class) || o.getClass().equals(LoiFF.class));
  }

  public static CtuluLog validateOrdPrtGeoModeleBase(final OrdPrtGeoModeleBase cont, final CrueConfigMetier validator) {
    final CtuluLog res = new CtuluLog();
    res.setDesc("OrdPrtGeoModeleBase");
    final List<Regle> listRegle = cont.getRegles();
    for (final Regle regle : listRegle) {
      validateValParam(regle.getValParam(), validator, res);
    }
    Planimetrage planimetrage = cont.getPlanimetrage();
    if (planimetrage instanceof PlanimetrageNbrPdzCst) {
      validator.getProperty("nbrPdz").getValidator().validateNumber("", ((PlanimetrageNbrPdzCst) planimetrage).getNbrPdz(), res);
    }
    return res;
  }

  public static CtuluLog validateOrdPrtReseau(final OrdPrtReseau cont, final CrueConfigMetier validator) {
    final CtuluLog res = new CtuluLog();
    res.setDesc("OrdPrtReseau");
    final List<Regle> listRegle = cont.getRegles();
    for (final Regle regle : listRegle) {
      validateValParam(regle.getValParam(), validator, res);
    }
    return res;
  }

  public static CtuluLog validateOrdPrtCIniModeleBase(final OrdPrtCIniModeleBase cont, final CrueConfigMetier validator) {
    final CtuluLog res = new CtuluLog();
    res.setDesc("OrdPrtCIniModeleBase");
    if (cont.getMethodeInterpol() == null) {
      res.addSevereError("validation.OrdPrtCIniModeleBase.noInterpolationMethod.error");
    } else {
      Collection<EnumRegleInterpolation> requiredValParam = cont.getMethodeInterpol().getRequiredValParam();
      for (EnumRegleInterpolation valParam : requiredValParam) {
        if (cont.getValParam(valParam.getVariableName()) == null) {
          res.addSevereError("validation.OrdPrtCIniModeleBase.paramNotDefined.error", cont.getMethodeInterpol().geti18n(), valParam);
        }
      }
    }
    final List<Regle> listRegle = cont.getRegles();
    for (final Regle regle : listRegle) {
      validateValParam(regle.getValParam(), validator, res);
    }
    if (cont.getValParam() != null) {
      for (ValParam valParam : cont.getValParam()) {
        validateValParam(valParam, validator, res);
      }
    }
    return res;
  }

  private static void validateValParam(final ValParam cont, final CrueConfigMetier validator, final CtuluLog des) {
    if (cont == null) {
      return;
    }
    final Class<? extends ValParam> clazz = cont.getClass();
    // TOTO a ameliorer
    if (ValParamDouble.class.equals(clazz)) {
      final PropertyValidator val = validator.getValidator(StringUtils.uncapitalize(cont.getNom()));
      if (val != null) {
        val.validateValue(null, ((ValParamDouble) cont).getValeur(), des);
      }
    }
    if (ValParamEntier.class.equals(clazz)) {
      final PropertyValidator val = validator.getValidator(StringUtils.uncapitalize(cont.getNom()));
      if (val != null) {
        val.validateValue(null, ((ValParamEntier) cont).getValeur(), des);
      }
    }
  }

  public static CtuluLog validateDonFrt(final DonFrt cont, final CrueConfigMetier props) {
    final CtuluLog res = new CtuluLog();
    res.setDesc(cont.getNom());
    ValidationHelperLoi.valideLoi(cont.getLoi(), props, res, null);
    return res;
  }

  public static String getPrefixFor(final EMH emh) {
    return CruePrefix.getPrefix(emh.getCatType());
  }

  /**
   * @param param param a modifier
   * @return true si modification effectuée
   */
  public static boolean addPrefixIfNeeded(final ValParam param) {

    if (param == null) {
      return false;
    }
    String old = param.getNom();
    param.setNom(CruePrefix.getNomAvecPrefixe(CruePrefix.P_VAL_PARAM, param.getNom()));
    return !StringUtils.equals(old, param.getNom());
  }

  /**
   * un calcul marque inactif ou lie a une EMH non active ne doit pas être présent dans OCAL
   *
   * @param ordCalcScenario  l'ordCalc du scenario
   * @param crueConfigMetier le {@link CrueConfigMetier}
   * @return les logs de validation
   */
  public static CtuluLog validateOrdCalcScenario(OrdCalcScenario ordCalcScenario, CrueConfigMetier crueConfigMetier) {
    final CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    res.setDesc("OrdCalcScenario");
    List<OrdCalc> ordCalcs = ordCalcScenario.getOrdCalc();
    for (OrdCalc ordCalc : ordCalcs) {
      validateOcal(ordCalc, res, crueConfigMetier);
    }
    return res;
  }

  public static CtuluLog validatePCAL(ParamCalcScenario editedObject, CrueConfigMetier ccm) {
    final CtuluLog res = new CtuluLog();
    res.setDesc("ParamCalcScenario");
    validateObject(StringUtils.EMPTY, res, editedObject, ccm);
    return res;
  }

  public static CtuluLog validateDonCLimMScenario(DonCLimMScenario donCLimMScenario, CrueConfigMetier crueConfigMetier) {
    final CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    res.setDesc("DonCLimMScenario");
    List<Calc> calcs = donCLimMScenario.getCalc();
    Set<Class> uniqueDclmPerCalc = Sets.newHashSet(
        CalcTransNoeudBg1.class, CalcTransNoeudBg2.class, CalcTransNoeudUsine.class, CalcTransNoeudBg1Av.class, CalcTransNoeudBg2Av.class
        ,CalcPseudoPermNoeudBg1.class, CalcPseudoPermNoeudBg2.class, CalcPseudoPermNoeudUsi.class, CalcPseudoPermNoeudBg1Av.class, CalcPseudoPermNoeudBg2Av.class
    );


    for (Calc calc : calcs) {
      List<Pair<String, Class>> dclmActiveWithDoublons = CalcHelper.getDCLMActiveWithDoublons(calc);
      if (!dclmActiveWithDoublons.isEmpty()) {
        for (Pair<String, Class> pair : dclmActiveWithDoublons) {
          res.addSevereError("validation.dclm.emhDefinedSeveralTimesWithSameType", calc.getNom(), pair.first, pair.second.getSimpleName());
        }
      }
      List<DonCLimM> listeDCLM = calc.getlisteDCLM();
      for (DonCLimM donCLimM : listeDCLM) {
        if (!donCLimM.getActuallyActive()) {
          res.addWarn("validation.dclm.inactive", calc.getNom(), ((DonCLimMCommonItem) donCLimM).getShortName());
        }
        if (donCLimM instanceof CalcTransNoeudQappExt) {
          CalcTransNoeudQappExt qAppExt = (CalcTransNoeudQappExt) donCLimM;
          if (StringUtils.isBlank(qAppExt.getNomFic())) {
            res.addError("validation.dclm.nomFic.isEmpty", calc.getNom(), qAppExt.getShortName(), qAppExt.getEmh().getNom());
          }
          if (StringUtils.isBlank(qAppExt.getSection())) {
            res.addError("validation.dclm.section.isEmpty", calc.getNom(), qAppExt.getShortName(), qAppExt.getEmh().getNom());
          }
        } else if (donCLimM instanceof CalcTransBrancheOrificeManoeuvreRegul) {
          CalcTransBrancheOrificeManoeuvreRegul qAppExt = (CalcTransBrancheOrificeManoeuvreRegul) donCLimM;
          if (StringUtils.isBlank(qAppExt.getParam())) {
            res.addError("validation.dclm.param.isEmpty", calc.getNom(), qAppExt.getShortName(), qAppExt.getEmh().getNom());
          }
        }
      }
      uniqueDclmPerCalc.stream().forEach(dclm -> {
        if (calc.getDclmUserActiveCount(dclm) > 1) {
          res.addSevereError("validation.dclm.severalTimeSameDclm", calc.getNom(), dclm.getSimpleName());
        }
      });

    }
    return res;
  }

  public static CtuluLog validateDonCLimMScenarioAndClim(DonCLimMScenario donCLimMScenario, CrueConfigMetier props) {
    final CtuluLog res = validateDonCLimMScenario(donCLimMScenario, props);
    List<Calc> calcs = donCLimMScenario.getCalc();
    for (Calc calc : calcs) {
      for (DonCLimM dclm : calc.getlisteDCLMUserActive()) {
        validateObject(calc.getNom() + ": " + dclm.getEmh().getNom() + " / " + ((DonCLimMCommonItem) dclm).getShortName(), res, dclm, props);
      }
    }
    return res;
  }

  private static void validateOcal(OrdCalc ordCalc, final CtuluLog res, CrueConfigMetier props) {
    validateObject(ordCalc.getCalc().getNom(), res, ordCalc, props);
    List cliches = ordCalc.getCliches();
    for (Object object : cliches) {
      if (object != null) {
        validateObject(ordCalc.getCalc().getNom() + " / " + object.getClass().getSimpleName(), res, object, props);
      }
    }
  }
}
