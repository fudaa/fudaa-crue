/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.emh.*;

import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Permet de tester que toutes les ordres de calcul sont inclus dans le fichier ORES par défaut d'un coeur ( et vice-versa).
 *
 * @author Frederic Deniger
 */
public class ValidatorORES implements CrueValidator {

  private final CoeurConfigContrat coeurConfig;
  private final ScenarioAutoModifiedState modifiedState;

  public ValidatorORES(CoeurConfigContrat coeurConfig, final ScenarioAutoModifiedState modifiedState) {
    this.coeurConfig = coeurConfig;
    this.modifiedState = modifiedState;
  }

  public ScenarioAutoModifiedState getModifiedState() {
    return modifiedState;
  }

  private void setModified() {
    if (modifiedState != null) {
      modifiedState.setOrdResModified();
    }
  }

  @Override
  public List<CtuluLog> getLogs() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void validate(Set<Object> childs, EMH emh, EMHModeleBase modeleBase) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  private void compare(OrdRes reference, OrdRes other, CtuluLog log) {
    if (reference == null && other == null) {
      return;
    }
    if (reference == null) {
      log.addSevereError("validation.ores.referenceIsNullAndNotOres.error", other.getClass().getSimpleName());
      return;
    }
    if (other == null) {
      log.addSevereError("validation.ores.oresIsNullAndNotReference.error", reference.getClass().getSimpleName());
      return;
    }
    if (other.synchronizeValuesFrom(reference)) {
      setModified();
    }
  }

  @Override
  public List<CtuluLog> validateScenario(EMHScenario scenario) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final Crue10FileFormat<OrdResScenario> format = Crue10FileFormatFactory.getVersion(CrueFileType.ORES, coeurConfig);
    CrueIOResu<OrdResScenario> read = format.read(coeurConfig.getDefaultFile(CrueFileType.ORES), log, null);
    if (log.containsSevereError() || read.getMetier() == null) {
      return Collections.singletonList(log);
    }
    OrdResScenario reference = read.getMetier();
    OrdResScenario other = scenario.getOrdResScenario();
    compare(reference.getOrdResBrancheBarrageFilEau(), other.getOrdResBrancheBarrageFilEau(), log);
    compare(reference.getOrdResBrancheBarrageGenerique(), other.getOrdResBrancheBarrageGenerique(), log);
    compare(reference.getOrdResBrancheNiveauxAssocies(), other.getOrdResBrancheNiveauxAssocies(), log);
    compare(reference.getOrdResBrancheOrifice(), other.getOrdResBrancheOrifice(), log);
    compare(reference.getOrdResBranchePdc(), other.getOrdResBranchePdc(), log);
    compare(reference.getOrdResBrancheSaintVenant(), other.getOrdResBrancheSaintVenant(), log);
    compare(reference.getOrdResBrancheSeuilLateral(), other.getOrdResBrancheSeuilLateral(), log);
    compare(reference.getOrdResBrancheSeuilTransversal(), other.getOrdResBrancheSeuilTransversal(), log);
    compare(reference.getOrdResBrancheStrickler(), other.getOrdResBrancheStrickler(), log);
    compare(reference.getOrdResCasier(), other.getOrdResCasier(), log);
    compare(reference.getOrdResNoeudNiveauContinu(), other.getOrdResNoeudNiveauContinu(), log);
    compare(reference.getOrdResSection(), other.getOrdResSection(), log);
    compare(reference.getOrdResModeleRegul(), other.getOrdResModeleRegul(), log);
    return Collections.singletonList(log);
  }
}
