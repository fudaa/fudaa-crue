package org.fudaa.dodico.crue.validation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.io.rdao.CommonResDao;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.ItemResDao;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.VariableResDao;
import org.fudaa.dodico.crue.io.res.ResCatEMHContent;
import org.fudaa.dodico.crue.io.res.ResTypeEMHContent;
import org.fudaa.dodico.crue.io.res.ResVariablesContent;

/**
 * Valide le contenu du fichier XML et test si toute les VariablesResLoi sont correctement définies.
 *
 * @author deniger
 */
public class ValidatorResultatCrue10VariablesDescriptor {

  public CtuluLog validVariables(List<ResCatEMHContent> caCcontent, CrueConfigMetier ccm) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("res.validate.Variable.Name");
    validVariableResLoi(caCcontent, log);
    validDefinedVariable(caCcontent, ccm, log);
    return log;

  }

  private void validVariableResLoi(List<ResCatEMHContent> caCcontent, CtuluLog log) {
    for (ResCatEMHContent cat : caCcontent) {
      for (ResTypeEMHContent type : cat.getResTypeEMHContent()) {
        validate(type, log);
      }
    }
  }

  private void validDefinedVariable(List<ResCatEMHContent> caCcontent, CrueConfigMetier ccm, CtuluLog log) {
    Set<String> done = new HashSet<>();
    for (ResCatEMHContent cat : caCcontent) {
      for (ResTypeEMHContent type : cat.getResTypeEMHContent()) {
        validateVariableDefinition(type, ccm, log, done);
      }
    }
  }

  private void validateVariableDefinition(ResTypeEMHContent type, CrueConfigMetier ccm, CtuluLog log, Set<String> done) {
    ResVariablesContent resVariablesContent = type.getResVariablesContent();
    List<VariableResDao> variables = resVariablesContent.getVariables();
    for (VariableResDao variableResDao : variables) {
      if (!done.contains(variableResDao.getId())) {
        done.add(variableResDao.getId());
        if(variableResDao.isLoiDefinition()){
          if(!ccm.isKnownLoi(variableResDao.getNomRef())){//on garde les majuscules pour les lois.
            log.addSevereError("res.validate.UnknownLoi", variableResDao.getNomRef());
          }
        }
        // on ne valide pas les variables car elles ne sont pas toutes définies dans le CCM
//        Voir CRUE-844
//        else if (!ccm.isPropertyDefined(variableResDao.getId())) {
//          log.addSevereError("res.validate.UnknownVariable", variableResDao.getNomRef());
//
//        }
      }

    }
  }

  private void validate(ResTypeEMHContent type, CtuluLog log) {
    ResVariablesContent resVariablesContent = type.getResVariablesContent();
    //les definition de nbr de points par lois
    Set<String> loiNbrPtDefinition = resVariablesContent.getLoiNbrPtDefinition();
    //les definition des lois:
    Set<String> lois = new HashSet<>(CommonResDao.selectResLoiDaoId(resVariablesContent.getVariables()));

    //contient toutes les VariableResLoiNbrPtDao qui ne font pas références à une VariableResLoi
    Collection<String> nbrPtLoiNotUsed = CollectionUtils.subtract(loiNbrPtDefinition, lois);
    if (!nbrPtLoiNotUsed.isEmpty()) {
      ArrayList<String> nbrPtLoiNotUsedSorted = new ArrayList<>(nbrPtLoiNotUsed);
      Collections.sort(nbrPtLoiNotUsedSorted);
      log.addWarn("res.validate.VariableResLoiNbrPtDao.NotUsed", ToStringHelper.join(nbrPtLoiNotUsedSorted));
    }
    //contient toutes les VariableResLoiNbrPtDao qui devront être definies dans un ItemResDao.
    Collection<String> nbrPtLoiToBeDefinedInItem = new HashSet<>(CollectionUtils.subtract(lois, loiNbrPtDefinition));
    for (ItemResDao itemResDao : type.getItemResDao()) {
      Set<String> variableResLoiNbrPtId = new HashSet<>(CommonResDao.getId(itemResDao.getVariableResLoiNbrPt()));
      //ajoute erreurs pour les VariableResLoiNbrPtDao non définies
      if (!nbrPtLoiToBeDefinedInItem.isEmpty()) {
        for (String loiId : nbrPtLoiToBeDefinedInItem) {
          if (!variableResLoiNbrPtId.contains(loiId)) {
            log.addSevereError("res.validate.VariableResLoiNbrPtDaoInEMH.NotDefined", itemResDao.getNomRef(),
                              StringUtils.capitalize(
                    loiId));
          }
        }
      }
      //ajoute erreurs pour les VariableResLoiNbrPtDao definies en trop
      if (!variableResLoiNbrPtId.isEmpty()) {
        for (String id : variableResLoiNbrPtId) {
          if (!lois.contains(id)) {
            log.addWarn("res.validate.VariableResLoiNbrPtDaoInEMH.NotUsed", itemResDao.getNomRef(), StringUtils.capitalize(id));
          }

        }
      }
    }


  }
}
