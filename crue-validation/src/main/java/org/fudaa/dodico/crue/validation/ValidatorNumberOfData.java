package org.fudaa.dodico.crue.validation;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.factory.PredicateFactory;

import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Utiliser pour générer des warnings si le nombre d'EMH est vide ou dépasse le nombre donné dans le fichier de configuration.
 *
 * @author deniger
 */
public class ValidatorNumberOfData implements CrueValidator {

  private final CrueConfigMetier configMetier;

  public ValidatorNumberOfData(CrueConfigMetier configMetier) {
    this.configMetier = configMetier;
  }

  @Override
  public List<CtuluLog> getLogs() {
    return null;
  }

  @Override
  public void validate(Set<Object> childs, EMH emh, EMHModeleBase modeleBase) {
    throw new IllegalAccessError("not implemented");
  }

  @Override
  public List<CtuluLog> validateScenario(EMHScenario scenario) {
    List<EMH> allSimpleEMH = scenario.getAllSimpleEMH();
    Predicate predicate = new PredicateFactory.PredicateTypeEnum(EnumCatEMH.NOEUD);
    CollectionUtils.countMatches(allSimpleEMH, predicate);
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("valid.validationNumberOfData");
    validNbr(log, allSimpleEMH, "nbrCatEMHSection", EnumCatEMH.SECTION);
    validNbr(log, allSimpleEMH, "nbrCatEMHCasier", EnumCatEMH.CASIER);
    validNbr(log, allSimpleEMH, "nbrCatEMHBranche", EnumCatEMH.BRANCHE);
    validNbr(log, allSimpleEMH, "nbrCatEMHNoeud", EnumCatEMH.NOEUD);
    int countConteneur = countType(scenario.getAllEMH(), EnumCatEMH.SCENARIO, EnumCatEMH.MODELE,
                                   EnumCatEMH.MODELE_ENCHAINEMENT, EnumCatEMH.SOUS_MODELE, EnumCatEMH.SOUS_MODELE_ENCHAINEMENT);
    configMetier.getValidator("nbrCatEMHConteneur").validateValue(
            BusinessMessages.getString("valid.nbrCatEMHConteneur"), countConteneur, log);

    addLogIfZero(countType(allSimpleEMH, EnumCatEMH.NOEUD), "valid.noNodes", log);
    addLogIfZero(countType(allSimpleEMH, EnumCatEMH.BRANCHE), "valid.noBranches", log);
    addLogIfZero(countType(allSimpleEMH, EnumCatEMH.CASIER), "valid.noCasiers", log);
    addLogIfZero(countType(allSimpleEMH, EnumCatEMH.SECTION), "valid.noSection", log);
    addLogIfZero(scenario.getLoiConteneur().getLois().size(), "valid.noLois", log);
    addLogIfZero(scenario.getOrdCalcScenario().getOrdCalc().size(), "valid.noCalc", log);
    return Collections.singletonList(log);

    // NbrCatEMHBranche
    // NbrCatEMHConteneur
    // NbrCatEMHNoeud
    // NbrCatEMHSection
  }

  private void validNbr(CtuluLog log, List<EMH> allSimpleEMH, String property,
                        EnumCatEMH typeEMH) {
    configMetier.getValidator(property).validateValue(
            BusinessMessages.getString("valid.nbrCatEMH" + StringUtils.capitalize(typeEMH.toString().toLowerCase())), countType(
            allSimpleEMH, typeEMH), log);
  }

  private int countType(List<EMH> allSimpleEMH, EnumCatEMH type) {
    Predicate predicate = new PredicateFactory.PredicateTypeEnum(type);
    return CollectionUtils.countMatches(allSimpleEMH, predicate);
  }

  private int countType(List<EMH> allSimpleEMH, EnumCatEMH... type) {
    int res = 0;
    for (EnumCatEMH enumTypeEMH : type) {
      res += countType(allSimpleEMH, enumTypeEMH);
    }
    return res;
  }

  private void addLogIfZero(int i, String msg, CtuluLog log) {
    if (i == 0) {
      log.addInfo(msg);
    }
  }
}
