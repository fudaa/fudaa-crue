/**
 *
 */
package org.fudaa.dodico.crue.validation;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.comparaison.tester.*;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ValidatorForCrue9Export extends AbstractCrueValidator {

  private final CtuluLog res = new CtuluLog();
  private final List<CtuluLog> lst = Collections.singletonList(res);
  boolean isBetaFound;
  boolean isQdmDifferent;
  final double defaultBeta;
  final PropertyEpsilon epsBeta;
  final PropertyEpsilon epsQdm;
  final CrueConfigMetier props;
  final boolean export;
  boolean coefRuisQdmInit = false;
  double coefRuisQdm;

  public ValidatorForCrue9Export(final CrueConfigMetier props, final boolean export) {
    super();
    this.export = export;
    this.props = props;
    res.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    res.setDesc("validation.crue9");
    defaultBeta = props.getDefaultDoubleValue("coefBeta");
    epsQdm = props.getEpsilon("coefRuisQdm");
    epsBeta = props.getEpsilon("coefBeta");
  }

  private void addWarnOrError(final String m, final Object... params) {
    if (export) {
      res.addError(m, params);
    } else {
      res.addWarn(m, params);
    }
  }

  private void addWarnOrFatalError(final String m, final Object... params) {
    if (export) {
      res.addSevereError(m, params);
    } else {
      res.addWarn(m, params);
    }
  }

  private void computeQruis(final Object o) {
    if (!(o instanceof OrdCalcScenario)) {
      return;
    }
    final OrdCalcScenario ocal = (OrdCalcScenario) o;
    final List<CalcPseudoPerm> calcPseudoPerm = EMHHelper.collectCalcPseudoPerm(ocal.getOrdCalc());
    for (final CalcPseudoPerm calcPerm : calcPseudoPerm) {
      // Il faut au moins une branche saint venant pour considérer qu'il y a un débit de ruissellement (identique)
      final List<CalcPseudoPermBrancheSaintVenantQruis> calcQruis = calcPerm.getDclm(CalcPseudoPermBrancheSaintVenantQruis.class);
      final List<CalcPseudoPermCasierProfilQruis> calcCasierQruis = calcPerm.getDclm(CalcPseudoPermCasierProfilQruis.class);
      if (CollectionUtils.isNotEmpty(calcQruis) || CollectionUtils.isNotEmpty(calcCasierQruis)) {
        final Double qruis = EMHHelper.isQruisConstantInUserActive(calcPerm, props);
        if (qruis == null) {
          addWarnOrError("crue9.compatible.Qruis.NotConstant", calcPerm.getNom());
        }
      }
    }

  }

  private void computeQruisQDM(final Object o) {
    if (isQdmDifferent) {
      return;
    }
    if (o instanceof DonCalcSansPrtBrancheSaintVenant) {
      final DonCalcSansPrtBrancheSaintVenant donCalcSansPrtBrancheSaintVenant = (DonCalcSansPrtBrancheSaintVenant) o;
      if (!donCalcSansPrtBrancheSaintVenant.getActuallyActive()) {
        return;
      }
      final double value = donCalcSansPrtBrancheSaintVenant.getCoefRuisQdm();
      if (!coefRuisQdmInit) {
        coefRuisQdm = value;
        coefRuisQdmInit = true;
      } else if (!epsQdm.isSame(value, coefRuisQdm)) {
        isQdmDifferent = true;
        addWarnOrError("crue9.compatible.coefRuisQdm.notConstant");
      }
    }
  }

  public void findBeta(final Object o) {
    if (isBetaFound) {
      return;
    }
    if (o instanceof DonCalcSansPrtBrancheSaintVenant) {
      final DonCalcSansPrtBrancheSaintVenant dcsp = (DonCalcSansPrtBrancheSaintVenant) o;
      if (!epsBeta.isSame(defaultBeta, dcsp.getCoefBeta())) {
        isBetaFound = true;
        addWarnOrError("crue9.compatible.coefBeta");

      }
    }
  }

  @Override
  public List<CtuluLog> getLogs() {
    return lst;
  }

  private void validateDonPrtCIniBrancheOrifice(final EMH emh) {

    if (emh.getCatType().equals(EnumCatEMH.BRANCHE) && emh.getClass().equals(EMHBrancheOrifice.class)) {
      List<InfosEMH> listPseudoPermanent = EMHHelper.collectInfoEMH(emh, EnumInfosEMH.DON_CLIM);
      List<InfosEMH> donPrtCIniBrancheOrifice = EMHHelper.collectInfoEMH(emh, EnumInfosEMH.DON_PRT_CINI);
      // ici on ne fait pas attention au probleme de cardinalite:
      if (CollectionUtils.isNotEmpty(listPseudoPermanent) && CollectionUtils.isNotEmpty(donPrtCIniBrancheOrifice)) {
        CalcPseudoPermBrancheOrificeManoeuvre firstPseudoPerm = EMHHelper.selectFirstOfClass(listPseudoPermanent,
            CalcPseudoPermBrancheOrificeManoeuvre.class);
        // on a 1 CalcPseudoPermBrancheOrificeManoeuvre par branche:
        DonPrtCIniBrancheOrifice ciniBranche = (DonPrtCIniBrancheOrifice) donPrtCIniBrancheOrifice.get(0);
        if (firstPseudoPerm != null) {
          if (!ciniBranche.getSensOuv().equals(firstPseudoPerm.getSensOuv())
              || !props.getProperty("ouv").getEpsilon().isSame(ciniBranche.getOuv(), firstPseudoPerm.getOuv())) {
            addWarnOrError("crue9.compatible.donPrtCiniBrancheOrifice.differentFromFirstCalcPseudoPerm", emh.getNom(),
                firstPseudoPerm.getNomCalculParent());
          }

        }
      }

    }
  }

  @Override
  public void validate(final Set<Object> o, final EMH emh, final EMHModeleBase modeleParent) {
    boolean qRuisSaintVenantDone = false;
    boolean qRuisDonPrtCIniCasierProfilDone = false;
    PropertyEpsilon epsilonQruis = props.getEpsilon("qruis");
    for (final Object object : o) {
      findBeta(object);
      computeQruisQDM(object);
      computeQruis(object);
      if (!qRuisSaintVenantDone && DonPrtCIniBrancheSaintVenant.class.equals(object.getClass())) {
        if (!epsilonQruis.isZero(((DonPrtCIniBrancheSaintVenant) object).getQruis())) {
          qRuisSaintVenantDone = true;
          addWarnOrError("crue9.compatible.qRuisNotNull.DonPrtCIniBrancheSaintVenant", emh.getNom());
        }

      }
      if (!qRuisDonPrtCIniCasierProfilDone && DonPrtCIniCasierProfil.class.equals(object.getClass())) {
        if (!epsilonQruis.isZero(((DonPrtCIniCasierProfil) object).getQruis())) {
          qRuisDonPrtCIniCasierProfilDone = true;
          addWarnOrError("crue9.compatible.qRuisNotNull.DonPrtCIniCasierProfil", emh.getNom());
        }

      }
      if (object instanceof DonCLimMScenario) {
        validateDonCLimMScenario((DonCLimMScenario) object);
      }
      if (object instanceof OrdCalcScenario) {
        validateOrdCalcScenario((OrdCalcScenario) object);
      }
      if (object instanceof ParamCalcScenario) {
        validateParamCalcScenario((ParamCalcScenario) object);
      }
      if (DonPrtGeoProfilSection.class.equals(object.getClass())) {
        validateLitMineurName((DonPrtGeoProfilSection) object);
      }
    }
    if (isBranche2ou15(emh)) {
      validateBranche2ou15((CatEMHBranche) emh);
    }
    validateDonPrtCIniBrancheOrifice(emh);
    validateCLMPSeudoPermForScenario(emh);
    validateHydrogrammeQRuisIdentiqueForCalculTransitoire(emh);
  }

  private void validateDonCLimMScenario(DonCLimMScenario dclms) {
    List<Class> notSupportedDclms = Arrays.asList(
        CalcPseudoPermNoeudBg1.class, CalcPseudoPermNoeudBg2.class,
        CalcPseudoPermNoeudUsi.class, CalcPseudoPermNoeudBg1Av.class, CalcPseudoPermNoeudBg2Av.class, CalcPseudoPermBrancheOrificeManoeuvreRegul.class,
        CalcTransNoeudQappExt.class, CalcTransBrancheOrificeManoeuvreRegul.class, CalcTransNoeudBg1.class, CalcTransNoeudBg2.class,
        CalcTransNoeudUsine.class, CalcTransNoeudBg1Av.class, CalcTransNoeudBg2Av.class);
    Set<Class> warningDone = new HashSet<>();
    dclms.getCalc().forEach(c -> {
      c.getlisteDCLMUserActive().forEach(dclm -> {
        if (notSupportedDclms.contains(dclm.getClass())) {
          if (!warningDone.contains(dclm.getClass())) {
            res.addWarn("crue9.compatible.dclmNotSupported", dclm.getType());
            warningDone.add(dclm.getClass());
          }
        }
      });
    });
  }

  private void validateHydrogrammeQRuisIdentiqueForCalculTransitoire(EMH emh) {
    if (!emh.getCatType().equals(EnumCatEMH.SCENARIO)) {
      return;
    }
    EMHScenario scenario = (EMHScenario) emh;
    final List<CalcTrans> calcsTrans = EMHHelper.collectCalcTrans(scenario.getOrdCalcScenario().getOrdCalc());
    final EqualsTester loiDFTester = new FactoryEqualsTester(props, -1, -1).getLoiDFTester();
    TesterContextFactory factory = new TesterContextFactory();
    factory.setIgnoreComment(true);
    TesterContext testerContext = factory.create();
    testerContext.setError(new CtuluLog());
    for (CalcTrans calcTrans : calcsTrans) {
      validateHydrogrammeQRuisIdentique(calcTrans, loiDFTester, testerContext);
    }

  }

  @SuppressWarnings("unchecked")
  private void validateHydrogrammeQRuisIdentique(CalcTrans calcTrans, EqualsTester loiDFTester,
                                                 TesterContext testerContext) {
    try {
      LoiDF common = null;
      Collection<CalcTransBrancheSaintVenantQruis> listQruis = calcTrans.getDclm(CalcTransBrancheSaintVenantQruis.class);
      for (CalcTransBrancheSaintVenantQruis qruis : listQruis) {
        LoiDF loiTQruis = qruis.getHydrogrammeQruis();
        if (common == null) {
          common = loiTQruis;
        } else if (!loiDFTester.isSame(common, loiTQruis, null, testerContext)) {
          addWarnOrError("crue9.compatible.sameHydrogrammeQruisForCasierAndBranche", calcTrans.getNom());
          return;
        }
      }
      Collection<CalcTransCasierProfilQruis> listProfilQruis = calcTrans.getDclm(CalcTransCasierProfilQruis.class);
      for (CalcTransCasierProfilQruis qruis : listProfilQruis) {
        LoiDF loiTQruis = qruis.getHydrogrammeQruis();
        if (common == null) {
          common = loiTQruis;
        } else if (!loiDFTester.isSame(common, loiTQruis, null, testerContext)) {
          addWarnOrError("crue9.compatible.sameHydrogrammeQruisForCasierAndBranche", calcTrans.getNom());
          return;
        }
      }
    } catch (Exception e) {
      Logger.getLogger(getClass().getName()).log(Level.SEVERE, "validateHydrogrammeQRuisIdentique", e);
    }
  }

  private void validateCLMPSeudoPermForScenario(EMH emh) {
    if (!emh.getCatType().equals(EnumCatEMH.SCENARIO)) {
      return;
    }
    EMHScenario scenario = (EMHScenario) emh;
    List<CalcPseudoPerm> calcPseudoPerms = scenario.getDonCLimMScenario().getCalcPseudoPerm();

    validCalcPseudoPermBrancheOrificeManoeuvre(calcPseudoPerms);
    validCalcPseudoPermBrancheSaintVenantQruis(calcPseudoPerms);
    validCalcPseudoPermCasierProfilQruis(calcPseudoPerms);
    validCalcPseudoPermNoeudNiveauContinuQapp(calcPseudoPerms);
    validCalcPseudoPermNoeudNiveauContinuZimpose(calcPseudoPerms);

  }

  private void validCalcPseudoPermBrancheOrificeManoeuvre(List<CalcPseudoPerm> calcPseudoPerms) {
    Set<EMH> usedEMH = null;
    for (CalcPseudoPerm calcPseudoPerm : calcPseudoPerms) {
      Set<EMH> currentUsedEMH = getUsedEMH(calcPseudoPerm.getDclm(CalcPseudoPermBrancheOrificeManoeuvre.class));
      if (usedEMH == null) {
        usedEMH = currentUsedEMH;
      } else if (!isSame(usedEMH, currentUsedEMH)) {
        addWarnOrFatalError("crue9.compatible.CalcPseudoPermBrancheOrificeManoeuvre.nonConstant");
        return;
      }

    }
  }

  private void validCalcPseudoPermNoeudNiveauContinuZimpose(List<CalcPseudoPerm> calcPseudoPerms) {
    Set<EMH> usedEMH = null;
    for (CalcPseudoPerm calcPseudoPerm : calcPseudoPerms) {
      Set<EMH> currentUsedEMH = getUsedEMH(calcPseudoPerm.getDclm(CalcPseudoPermNoeudNiveauContinuZimp.class));
      if (usedEMH == null) {
        usedEMH = currentUsedEMH;
      } else if (!isSame(usedEMH, currentUsedEMH)) {
        addWarnOrFatalError("crue9.compatible.CalcPseudoPermNoeudNiveauContinuZimpose.nonConstant");
        return;
      }

    }
  }

  private void validCalcPseudoPermNoeudNiveauContinuQapp(List<CalcPseudoPerm> calcPseudoPerms) {
    Set<EMH> usedEMH = null;
    for (CalcPseudoPerm calcPseudoPerm : calcPseudoPerms) {
      Set<EMH> currentUsedEMH = getUsedEMH(calcPseudoPerm.getDclm(CalcPseudoPermNoeudQapp.class));
      if (usedEMH == null) {
        usedEMH = currentUsedEMH;
      } else if (!isSame(usedEMH, currentUsedEMH)) {
        addWarnOrFatalError("crue9.compatible.CalcPseudoPermNoeudNiveauContinuQapp.nonConstant");
        return;
      }

    }
  }

  private void validCalcPseudoPermBrancheSaintVenantQruis(List<CalcPseudoPerm> calcPseudoPerms) {
    Set<EMH> usedEMH = null;
    for (CalcPseudoPerm calcPseudoPerm : calcPseudoPerms) {
      Set<EMH> currentUsedEMH = getUsedEMH(calcPseudoPerm.getDclm(CalcPseudoPermBrancheSaintVenantQruis.class));
      if (usedEMH == null) {
        usedEMH = currentUsedEMH;
      } else if (!isSame(usedEMH, currentUsedEMH)) {
        addWarnOrFatalError("crue9.compatible.CalcPseudoPermBrancheSaintVenantQruis.nonConstant");
        return;
      }

    }
  }

  private void validCalcPseudoPermCasierProfilQruis(List<CalcPseudoPerm> calcPseudoPerms) {
    Set<EMH> usedEMH = null;
    for (CalcPseudoPerm calcPseudoPerm : calcPseudoPerms) {
      Set<EMH> currentUsedEMH = getUsedEMH(calcPseudoPerm.getDclm(CalcPseudoPermCasierProfilQruis.class));
      if (usedEMH == null) {
        usedEMH = currentUsedEMH;
      } else if (!isSame(usedEMH, currentUsedEMH)) {
        addWarnOrFatalError("crue9.compatible.CalcPseudoPermCasierProfilQruis.nonConstant");
        return;
      }

    }
  }

  private boolean isSame(Set setA, Set setB) {
    if (setA.size() != setB.size()) {
      return false;
    }
    for (Object object : setB) {
      if (!setA.contains(object)) {
        return false;
      }
    }
    for (Object object : setA) {
      if (!setB.contains(object)) {
        return false;
      }
    }
    return true;
  }

  private Set<EMH> getUsedEMH(Collection<? extends DonCLimMCommon> in) {
    if (CollectionUtils.isEmpty(in)) {
      return Collections.emptySet();
    }
    HashSet<EMH> res = new HashSet<>();
    for (DonCLimMCommon dclm : in) {
      res.add(dclm.getEmh());
    }
    return res;
  }

  private boolean isBranche2ou15(final EMH emh) {
    return EnumCatEMH.BRANCHE == emh.getCatType()
        && (EMHBrancheSeuilTransversal.class.equals(emh.getClass()) || EMHBrancheBarrageFilEau.class.equals(emh.getClass()));
  }

  private void validateBranche2ou15(CatEMHBranche emh) {
    List<RelationEMHSectionDansBranche> sections = emh.getSections();
    for (RelationEMHSectionDansBranche relationSection : sections) {
      CatEMHSection section = relationSection.getEmh();
      if (!containsFiveLit(section)) {
        addWarnOrError("crue9.compatible.branche2ou15ContainsWithoutFiveLit", emh.getNom(), section.getNom());
      }
    }

  }

  private boolean containsFiveLit(CatEMHSection section) {
    List<DonPrtGeoProfilSection> sections = EMHHelper.selectClass(section.getInfosEMH(), DonPrtGeoProfilSection.class);
    for (DonPrtGeoProfilSection donPrtGeoProfilSection : sections) {
      if (donPrtGeoProfilSection.getNombreLitNumerote() != 5) {
        return false;
      }
    }
    return true;
  }

  private void validateLitMineurName(DonPrtGeoProfilSection in) {
    for (LitNumerote litNumerote : in.getLitNumerote()) {
      if (litNumerote.getIsLitMineur()) {
        String errLit = isLtMineurName(litNumerote);
        if (errLit != null) {
          addWarnOrError("crue9.compatible.litMineurRenomme", in.getNom(), errLit);
        }

      }
    }

  }

  private String isLtMineurName(LitNumerote litNumerote) {
    LitNomme nomLit = litNumerote.getNomLit();
    if (nomLit == null) {
      return BusinessMessages.getString("common.notDefined");
    }
    // ce n'est pas case sensitive
    if (!"Lt_Mineur".equalsIgnoreCase(nomLit.getNom())) {
      return nomLit.getNom();
    }
    return null;
  }

  public void validateOrdCalcScenario(final OrdCalcScenario ordCalcScenario) {
    final List<CalcTrans> calcsTrans = EMHHelper.collectCalcTrans(ordCalcScenario.getOrdCalc());
    validateTQruis(calcsTrans);
  }

  private void validateTQruis(List<CalcTrans> calcTrans) {
    if (CollectionUtils.isEmpty(calcTrans)) {
      return;
    }
    final Collection<CalcTransBrancheSaintVenantQruis> listeBranchesSV = calcTrans.get(0).getDclm(CalcTransBrancheSaintVenantQruis.class);
    if (CollectionUtils.isEmpty(listeBranchesSV)) {
      return;
    }

    LoiDF loiCommun = null;
    EqualsTesterLoi tester = null;
    for (final CalcTransBrancheSaintVenantQruis venantQruis : listeBranchesSV) {
      if (loiCommun == null) {
        loiCommun = venantQruis.getHydrogrammeQruis();
      } // on ne lance pas la comparaison si les lois sont les meme
      else if (!venantQruis.getHydrogrammeQruis().getId().equals(loiCommun.getId())) {
        if (tester == null) {
          tester = new EqualsTesterLoi(new FactoryEqualsTester(props, -1, -1));
        }
        try {
          if (!tester.isSameSafe(loiCommun, venantQruis.getHydrogrammeQruis(), null, tester.getContextFactory().create())) {
            addWarnOrError("crue9.compatible.calcTransMoreThanLoiTQruis");
          }
        } catch (final Exception e) {
        }

      }

    }

  }

  private void validateParamCalcScenario(ParamCalcScenario paramCalcScenario) {
    if (paramCalcScenario.getDateDebSce() != null) {
      addWarnOrError("crue9.compatible.temporalScenarioNotSupported");

    }
  }
}
