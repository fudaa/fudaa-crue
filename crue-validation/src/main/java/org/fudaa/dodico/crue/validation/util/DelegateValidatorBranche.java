package org.fudaa.dodico.crue.validation.util;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;
import org.fudaa.dodico.crue.config.ccm.*;
import org.fudaa.dodico.crue.metier.comparator.ComparatorRelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.PredicateFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;

import java.text.NumberFormat;
import java.util.*;

/**
 * Utiliser pour valider les section dans les branches
 *
 * @author deniger
 */
public class DelegateValidatorBranche {
  private final CrueConfigMetier props;

  /**
   * @param crueConfigMetier le ccm
   */
  public DelegateValidatorBranche(CrueConfigMetier crueConfigMetier) {
    super();
    this.props = crueConfigMetier;
  }

  public static List<EnumBrancheType> getBrancheTypeWithDistanceNotNull() {
    return Arrays.asList(EnumBrancheType.EMHBrancheSaintVenant, EnumBrancheType.EMHBrancheStrickler);
  }

  /**
   * @param emhLog les logs
   * @param emh la branche a valider
   * @param modeleBase le modele parent
   * @param relationSectionDansBranche les relations de sections dans branche
   */
  public void validateBranche(final CtuluLog emhLog, final CatEMHBranche emh, final EMHModeleBase modeleBase,
                              ValidationContentSectionDansBrancheExecutor relationSectionDansBranche) {
    List<RelationEMHSectionDansBranche> sections = emh.getListeSections();
    ComparatorRelationEMHSectionDansBranche comparator = new ComparatorRelationEMHSectionDansBranche(props);
    if (sections == null) {
      sections = Collections.emptyList();
    }
    validSectionsDisposition(emhLog, emh, sections, comparator);
    validTypeOfSection(emhLog, emh, relationSectionDansBranche, sections);
    validUniqueSectionByBranche(emhLog, sections);
    //on valide avec DPTI
    OrdPrtCIniModeleBase selectInfoEMH = (OrdPrtCIniModeleBase) EMHHelper.selectInfoEMH(modeleBase,
      EnumInfosEMH.ORD_PRT_CINI_MODELE_BASE);

    if (selectInfoEMH != null && selectInfoEMH.getMethodeInterpol() != null && selectInfoEMH.getMethodeInterpol().isBrancheActiveRequireDPTI()) {
      InfosEMH dpti = EMHHelper.selectInfoEMH(emh, EnumInfosEMH.DON_PRT_CINI);
      if (dpti == null) {
        emhLog.addSevereError("valid.branche.mustContainDPTI", emh.getNom(), selectInfoEMH.getMethodeInterpol().name());
      }
    }
  }

  protected void validSectionsDisposition(final CtuluLog emhLog, final CatEMHBranche emh,
                                          List<RelationEMHSectionDansBranche> sections,
                                          ComparatorRelationEMHSectionDansBranche comparator) {
    int nbSection = sections.size();
    if (nbSection < 2) {
      emhLog.addSevereError("validation.brancheMustContain2Sections", emh.getNom());
    }
    sections.sort(comparator);
    checkAmontAval(sections, emhLog, emh, nbSection, comparator);

//on verifie la distance
    ItemVariable property = props.getProperty(CrueConfigMetierConstants.PROP_XP);
    if (nbSection == 2 && comparator.getEps().isZero(sections.get(0).getXp())
      && comparator.getEps().isZero(sections.get(1).getXp())) {
      return;
    }
    EnumBrancheType brancheType = emh.getBrancheType();
    List<EnumBrancheType> brancheTypeWithDistanceNotNull = getBrancheTypeWithDistanceNotNull();
    if (!brancheTypeWithDistanceNotNull.contains(brancheType)) {
      emhLog.addSevereError("validation.branche.distanceMustBeNull", emh.getBrancheType());
    } else {
      double length = CatEMHBranche.getLength(sections);
      if (property.getEpsilon().isZero(length)) {
        emhLog.addSevereError("validation.branche.distanceMustNotBeNull", emh.getBrancheType());
      }
    }
    checkDuplicatedXp(property, sections, comparator, emhLog);
  }

  private List<RelationEMHSectionDansBranche> getListFor(double xp,
                                                         Map<Double, List<RelationEMHSectionDansBranche>> map, PropertyEpsilon eps) {
    for (Map.Entry<Double, List<RelationEMHSectionDansBranche>> entry : map.entrySet()) {
      if (eps.isSame(xp, entry.getKey().doubleValue())) {
        return entry.getValue();
      }
    }
    List<RelationEMHSectionDansBranche> rels = new ArrayList<>();
    map.put(xp, rels);
    return rels;
  }

  private void validUniqueSectionByBranche(final CtuluLog emhLog, List<RelationEMHSectionDansBranche> sections) {
    Set<String> idDone = new HashSet<>();
    Map<String, Integer> count = new HashMap<>();
    for (RelationEMHSectionDansBranche section : sections) {
      String idOfSection = section.getEmh().getNom();
      if (idDone.contains(idOfSection)) {
        Integer sum = count.get(idOfSection);
        if (sum == null) {
          count.put(idOfSection, Integer.valueOf(2));
        } else {
          count.put(idOfSection, sum++);
        }
      } else {
        idDone.add(idOfSection);
      }
    }
    for (Map.Entry<String, Integer> entry : count.entrySet()) {
      emhLog.addSevereError("validation.branche.cantContainSameSection", entry.getKey(), entry.getValue());
    }
  }

  private void validTypeOfSection(final CtuluLog emhLog, final CatEMHBranche emh,
                                  ValidationContentSectionDansBrancheExecutor relationSectionDansBranche,
                                  List<RelationEMHSectionDansBranche> sections) {
    final List<CatEMHSection> usedSection = new ArrayList<>(sections.size());
    for (final RelationEMHSectionDansBranche rel : sections) {
      usedSection.add(rel.getEmh());
    }
    relationSectionDansBranche.validContent(emhLog, emh, usedSection);
  }

  private void checkDuplicatedXp(ItemVariable property, List<RelationEMHSectionDansBranche> sections, ComparatorRelationEMHSectionDansBranche comparator,
                                 final CtuluLog emhLog) {
    NumberFormat formatter = property.getFormatter(DecimalFormatEpsilonEnum.COMPARISON);
    Map<Double, List<RelationEMHSectionDansBranche>> sectionByXp = new HashMap<>();
    // on parcourt toutes les relations
    for (RelationEMHSectionDansBranche relationEMH : sections) {
      // on stocke dans sectionByXp, les section par xp en prenant bien en compte les epsilon.
      List<RelationEMHSectionDansBranche> listFor = getListFor(relationEMH.getXp(), sectionByXp, comparator.getEps());
      listFor.add(relationEMH);
    }
    // verdict: si plus de 2 sections sur un même xp, erreur.
    for (Map.Entry<Double, List<RelationEMHSectionDansBranche>> entry : sectionByXp.entrySet()) {
      if (entry.getValue().size() > 1) {
        emhLog.addSevereError("validation.section.severalSectionsWithSameXp", formatter.format(entry.getValue().get(0).getXp()),
          ToStringHelper.transformTo(entry.getValue(), new TransformerEMHHelper.TransformerRelationToEMHNom()));
      }
    }
  }

  private void checkAmontAval(List<RelationEMHSectionDansBranche> sections, final CtuluLog emhLog, final CatEMHBranche emh, int nbSection,
                              ComparatorRelationEMHSectionDansBranche comparator) {
    Collection relationsAmont = CollectionUtils.select(sections,
      new PredicateFactory.PredicateRelationEMHSectionDansBranche(
        EnumPosSection.AMONT));
    Collection relationAval = CollectionUtils.select(sections,
      new PredicateFactory.PredicateRelationEMHSectionDansBranche(
        EnumPosSection.AVAL));
    if (relationsAmont.size() > 1) {
      String param = StringUtils.join(CollectionUtils.collect(relationsAmont,
        new TransformerEMHHelper.TransformerRelationToEMHNom()), "; ");
      emhLog.addSevereError("validation.branche.mustContainOnlyOneSectionAmont", emh.getNom(), param);
    } else if (relationsAmont.isEmpty()) {
      emhLog.addSevereError("validation.branche.mustContainAtLeastOneSectionAmont");
    }
    if (relationAval.size() > 1) {
      String param = StringUtils.join(CollectionUtils.collect(relationAval,
        new TransformerEMHHelper.TransformerRelationToEMHNom()), "; ");
      emhLog.addSevereError("validation.branche.mustContainOnlyOneSectionAval", emh.getNom(), param);
    } else if (relationAval.isEmpty()) {
      emhLog.addSevereError("validation.branche.mustContainAtLeastOneSectionAval");
    }
    if (nbSection > 0
      && (sections.get(0).getPos() != EnumPosSection.AMONT || !comparator.getEps().isZero(
      sections.get(0).getXp()))) {
      emhLog.addSevereError("validation.branche.firstSectionMustBeAmont");
    }
    if (nbSection > 1 && (sections.get(nbSection - 1).getPos() != EnumPosSection.AVAL)) {
      emhLog.addSevereError("validation.branche.lastSectionMustBeAval");
    }
  }
}
