package org.fudaa.dodico.crue.validation.util;

import java.util.*;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHSectionIdem;
import org.fudaa.dodico.crue.metier.emh.EMHSectionProfil;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.EnumInfosEMH;
import org.fudaa.dodico.crue.metier.emh.EnumSectionType;
import org.fudaa.dodico.crue.metier.emh.InfosEMH;
import org.fudaa.dodico.crue.metier.emh.OrdPrtCIniModeleBase;
import org.fudaa.dodico.crue.metier.emh.RelationEMHBrancheContientSection;
import org.fudaa.dodico.crue.metier.factory.PredicateFactory;
import org.fudaa.dodico.crue.metier.factory.PredicateFactory.PredicateRelationEMHContientEMHOfClass;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;

/**
 * @author deniger
 */
public class DelegateValidatorSection {

  @SuppressWarnings("unchecked")
  public static void validateSection(final CtuluLog emhLog, final EMH emh, final EMHModeleBase modeleBase) {
    if (emh == null || emh.getCatType() != EnumCatEMH.SECTION) {
      return;
    }
    PredicateRelationEMHContientEMHOfClass predicateRelationEMHContientEMHOfClass = new PredicateFactory.PredicateRelationEMHContientEMHOfClass(
            RelationEMHBrancheContientSection.class);
    List<RelationEMHBrancheContientSection> relation = new ArrayList<>();
    CollectionCrueUtil.select(emh.getRelationEMH(), predicateRelationEMHContientEMHOfClass, relation);
    if (relation.isEmpty()) {
      emhLog.addInfo("validation.section.NotContainedByBranche", emh.getNom());
    } else if (relation.size() > 1) {
      Collection<String> names = CollectionUtils.collect(relation, new TransformerEMHHelper.TransformerRelationToEMHNom());
      emhLog.addSevereError("validation.section.containedByOneBranche", emh.getNom(), StringUtils.join(names,
              "; "));
    }
    if (emh.getClass().equals(EMHSectionIdem.class)) {
      EMHSectionIdem id = (EMHSectionIdem) emh;
      CatEMHSection sectionRef = id.getSectionRef();
      if (sectionRef == null || !DelegateValidatorSection.getAuthorizedSectionRef().contains(sectionRef.getClass())) {
        emhLog.addSevereError("validation.sectionRefWrong", emh.getNom());
      } else if (id.getActuallyActive() && !sectionRef.getActuallyActive()) {
        emhLog.addSevereError("validation.sectionRefNotActive", emh.getNom(), sectionRef.getNom());
      }
    }
    //on valide avec DPTI
    OrdPrtCIniModeleBase selectInfoEMH = (OrdPrtCIniModeleBase) EMHHelper.selectInfoEMH(modeleBase,
            EnumInfosEMH.ORD_PRT_CINI_MODELE_BASE);

    if (selectInfoEMH != null && selectInfoEMH.getMethodeInterpol()!=null && selectInfoEMH.getMethodeInterpol().isSectionActiveRequireDPTI()) {
      InfosEMH dpti = EMHHelper.selectInfoEMH(emh, EnumInfosEMH.DON_PRT_CINI);
      if (dpti == null) {
        emhLog.addSevereError("valid.section.mustContainDPTI", emh.getNom(), selectInfoEMH.getMethodeInterpol().name());
      }
    }
  }

  /**
   * Donne la liste des sections autorisées en tant que référence d'une section idem.
   */
  public static List<Class<? extends CatEMHSection>> getAuthorizedSectionRef() {
    return Collections.<Class<? extends CatEMHSection>>singletonList(EMHSectionProfil.class);
  }

  /**
   * Donne la liste des sections autorisées en tant que référence d'une section idem.
   */
  public static List<EnumSectionType> getAuthorizedSectionRefType() {
    return Collections.singletonList(EnumSectionType.EMHSectionProfil);
  }
}
