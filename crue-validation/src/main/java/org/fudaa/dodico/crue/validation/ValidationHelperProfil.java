package org.fudaa.dodico.crue.validation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemConstant;
import org.fudaa.dodico.crue.config.ccm.PropertyValidator;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.comparator.Point2dAbscisseComparator;
import org.fudaa.dodico.crue.metier.emh.LitNumerote;
import org.fudaa.dodico.crue.metier.emh.PtProfil;

/**
 *
 * @author deniger
 */
public class ValidationHelperProfil {

  public static void validePtProfil(final List<PtProfil> in, final CrueConfigMetier validator, final CtuluLog res,
          final String pref) {
    //les régles sont les suivantes:
    //il doit y a avoir au moins 2 points ( c'est valider par le CCM et la ligne ci-dessous):
    validator.getValidator("nbrPtProfil").validateNumber(BusinessMessages.getString("valid.nbrPtProfil"), in.size(),
            res);
    //les points doivent être espacé de 2 mm minimum
    ConfigLoi configLoi = validator.getConfLoi().get(EnumTypeLoi.LoiPtProfil);
    final PropertyValidator abs = configLoi.getVarAbscisse().getValidator();
    final PropertyValidator ord = configLoi.getVarOrdonnee().getValidator();
    ValidationHelperLoi.validateListPt2d(in, abs, ord, res, pref);
    ValidationHelperLoi.validIncreasing(pref, in, configLoi.getVarAbscisse().getEpsilon(), res,
            configLoi.getControleLoi().isStricltyIncreasing());
    //on vérifie que les points sont espacés de 2 mm minimum.
    List<PtProfil> sorted = new ArrayList<>();
    sorted.addAll(in);
    Collections.sort(sorted, new Point2dAbscisseComparator(validator.getEpsilon("xt")));
    double minDelta = getMinimalXtDelta(validator);
    for (int i = 0; i < sorted.size() - 1; i++) {
      double x0 = sorted.get(i).getAbscisse();
      double x1 = sorted.get(i + 1).getAbscisse();
      double delta = Math.abs(x1 - x0);
      if (delta < minDelta) {
        res.addError("valid.ptProfil.MinDelta.NotRespected", pref, Integer.toString(i + 1));
      }
    }

  }

  protected static String getNomLit(final LitNumerote it, CrueConfigMetier props) {
    String nomLit = null;
    if (it.getNomLit() != null) {
      nomLit = it.getNomLit().getNom();
    }
    if (StringUtils.isEmpty(nomLit)) {
      nomLit = it.toString(props, EnumToString.OVERVIEW, DecimalFormatEpsilonEnum.COMPARISON);
    }
    return nomLit;
  }

  public static double getMinimalXtDelta(CrueConfigMetier ccm) {
    ItemConstant constante = ccm.getConstante(CrueConfigMetierConstants.CONST_DELTAXT);
    if (constante == null) {
      return 0.002;
    }
    return constante.getConstantValue().doubleValue();
  }
}
