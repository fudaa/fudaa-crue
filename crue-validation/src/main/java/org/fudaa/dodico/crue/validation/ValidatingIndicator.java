package org.fudaa.dodico.crue.validation;

/**
 * @author deniger
 */
public interface ValidatingIndicator {
    void clearMessage();

    void setErrorMessage(String errorMessage);

    void setWarningMessage(String warningMessage);
}
