package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Vérifie au chargement les données DPTI.
 *
 * @author deniger
 * @see ValidateInitialConditions
 */
public class ValidatorInitialConditions implements CrueValidator {
  private final CrueConfigMetier crueConfigMetier;
  private final ScenarioAutoModifiedState modifiedState;

  public ValidatorInitialConditions(CrueConfigMetier crueConfigMetier, ScenarioAutoModifiedState modifiedState) {
    this.crueConfigMetier = crueConfigMetier;
    this.modifiedState = modifiedState;
  }

  @Override
  public List<CtuluLog> getLogs() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void validate(Set<Object> childs, EMH emh, EMHModeleBase modeleBase) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public List<CtuluLog> validateScenario(EMHScenario scenario) {
    List<CtuluLog> logs = new ArrayList<>();
    final ValidateInitialConditions validateInitialCondition = new ValidateInitialConditions();
    //on vérifie les DTPI pour tous les scenarios:
    scenario.getModeles().forEach(emhModeleBase -> {
      final CtuluLog log = validateInitialCondition.propagateChange(emhModeleBase, crueConfigMetier);
      log.setDesc("validation.dpti.content");
      log.setDescriptionArgs(emhModeleBase.getNom());
      if (log.isNotEmpty()) {
        modifiedState.setDptiModified(true);
        logs.add(log);
      }
    });

    return logs;
  }
}
