package org.fudaa.dodico.crue.validation;

import java.util.*;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.RelationEMH;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;

/**
 * Le validation des id.
 * 
 * @author deniger
 */
public class ValidatorForIDCrue10 extends AbstractCrueValidator {
  boolean isCrue10Compatible = true;

  private final CtuluLog res = new CtuluLog();

  private final List<CtuluLog> lst = Collections.singletonList(res);
  private final Map<String, Object> used = new HashMap<>();

  public ValidatorForIDCrue10() {
    super();
    res.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    res.setDesc("validation.noms");
  }

  @Override
  public List<CtuluLog> getLogs() {
    return lst;
  }

  /**
   * @return the res
   */
  public CtuluLog getRes() {
    return res;
  }

  /**
   * @return the isCrue10Compatible
   */
  public boolean isCrue10Compatible() {
    return isCrue10Compatible;
  }

  /**
   * @param objectToValidate l'objet a valider
   */
  public void validate(final Object objectToValidate) {
    // noeud,strickler,litNomme
    if (objectToValidate instanceof ObjetWithID && !(objectToValidate instanceof RelationEMH)) {
      final String id = ((ObjetWithID) objectToValidate).getId();
      final String nom = ((ObjetWithID) objectToValidate).getNom();
      final int length = id.length();
      if (length > CruePrefix.NB_CAR_MAX) {
        isCrue10Compatible = false;
        res.addSevereError("valid.nom.tooLong", nom);
      }
      final Object usedObject = used.get(id);
      if (usedObject == null) {
        used.put(id, objectToValidate);
      } else if (!usedObject.equals(objectToValidate)) {
        res.addSevereError("valid.nom.notUnique", nom);
      }

    }

  }

  @Override
  public void validate(final Set<Object> o, final EMH emh, final EMHModeleBase modeleContenant) {
    for (final Object object : o) {
      validate(object);
    }

  }

  public static ValidatorForIDCrue10 validateNomsForCrue10(final EMHScenario scenario) {
    final ValidatorForIDCrue10 validator = new ValidatorForIDCrue10();
    ValidateEMHTreeWalker walker = new ValidateEMHTreeWalker();
    walker.doValidate(scenario.getAllEMH(), null, validator);
    return validator;
  }
}
