package org.fudaa.dodico.crue.validation;

import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.OrdCalc;
import org.fudaa.dodico.crue.metier.emh.OrdCalcPseudoPermIniCliche;
import org.fudaa.dodico.crue.metier.emh.OrdCalcScenario;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTransIniCalcCI;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTransIniCalcCliche;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

/**
 *
 * Test que les données sont compatibles avec la version 1.1.1
 *
 * @author deniger
 */
public class ValidatorForCrue10ExportOldVersion {

  public CtuluLog validate(EMHScenario scenario) {
    CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    res.setDesc("export.oldCrue10.ordCalc");
    OrdCalcScenario ordCalcScenario = scenario.getOrdCalcScenario();
    if(ordCalcScenario==null){
      return res;
    }
    List<OrdCalc> ordCalc = ordCalcScenario.getOrdCalc();
    if(ordCalc==null){
      return res;
    }
    if (EMHHelper.containsClass(ordCalc, OrdCalcPseudoPermIniCliche.class))  {
      res.addSevereError("export.oldCrue10.containOrdCalcPseudoPermIniCliche.error");
    }
    if (EMHHelper.containsClass(ordCalc, OrdCalcTransIniCalcCI.class))  {
      res.addSevereError("export.oldCrue10.containOrdCalcTransIniCalcCI.error");
    }
    if (EMHHelper.containsClass(ordCalc, OrdCalcTransIniCalcCliche.class))  {
      res.addSevereError("export.oldCrue10.containOrdCalcTransIniCalcCliche.error");
    }
    return res;
  }
}
