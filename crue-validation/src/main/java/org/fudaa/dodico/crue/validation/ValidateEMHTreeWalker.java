package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.metier.emh.*;

import java.util.*;

/**
 * Permet de parcourir un ensemble d'EMH pour les valider.
 *
 * @author deniger
 */
public class ValidateEMHTreeWalker {
  private interface CollectObject<T> {
    void getObjects(T in, Set<Object> collector);
  }

  private final Map<Class, CollectObject> objects = new HashMap<>();

  public ValidateEMHTreeWalker() {
    objects.put(ParamNumModeleBase.class, createCollectorForParamNumModeleBase());
    objects.put(DonLoiHYConteneur.class, createCollectorForDonLoiHYConteneur());
  }

  private void add(final Set out, final Object o) {
    if (o != null) {
      out.add(o);
      final CollectObject allSousObject = objects.get(o.getClass());
      if (allSousObject != null) {
        allSousObject.getObjects(o, out);
      }
    }
  }

  private CollectObject<DonLoiHYConteneur> createCollectorForDonLoiHYConteneur() {
    final CollectObject<DonLoiHYConteneur> pnum = (in, collector) -> {
      if (in != null) {
        collector.addAll(in.getLois());
      }
    };
    return pnum;
  }

  private CollectObject<ParamNumModeleBase> createCollectorForParamNumModeleBase() {
    final CollectObject<ParamNumModeleBase> pnum = (in, collector) -> {
      collector.add(in.getParamNumCalcPseudoPerm());
      collector.add(in.getParamNumCalcTrans());
      collector.add(in.getParamNumCalcVraiPerm());
    };
    return pnum;
  }

  void doValidate(final List<? extends EMH> emhs, final EMHModeleBase modeleContenant, final CrueValidator validator) {
    final Set validatedObjects = new HashSet();
    final List<Class<? extends InfosEMH>> classToAvoid = Arrays.asList(ResultatCalcul.class,
        ResultatPasDeTemps.class, ResPrtGeo.class);
    for (final EMH emh : emhs) {
      if (!validatedObjects.contains(emh) && emh.getActuallyActive()) {
        final Set<Object> listToTest = getObjectsFor(emh, validatedObjects, classToAvoid);
        validator.validate(listToTest, emh, modeleContenant);
      }
    }
  }

  @SuppressWarnings("serial")
  private Set<Object> getObjectsFor(final EMH emh, final Set alreadyValidatedObjects,
                                    final List<Class<? extends InfosEMH>> classToAvoid) {
    final Set out = new HashSet() {
      @Override
      public boolean add(final Object element) {
        if (element == null || ValidationHelper.isInstanceof(element, classToAvoid)) {
          return false;
        }
        if (alreadyValidatedObjects.contains(element)) {
          return false;
        }
        alreadyValidatedObjects.add(element);
        return super.add(element);
      }
    };
    add(out, emh);
    if (emh != null) {
      if (emh.isInfosEMHNotEmpty()) {
        for (final InfosEMH infosEMH : emh.getInfosEMH()) {
          add(out, infosEMH);
        }
      }
      if (emh.isRelationsEMHNotEmpty()) {
        for (final RelationEMH relationEMH : emh.getRelationEMH()) {
          add(out, relationEMH);
        }
      }
    }
    return out;
  }

  private List<CtuluLog> validateModele(final List<EMH> emhs, final EMHModeleBase modele, final CrueValidator value) {
    final List<EMHSousModele> sousModele = modele.getSousModeles();
    final List<EMH> all = new ArrayList<>(emhs.size() + (sousModele == null ? 0 : sousModele.size()) + 1);
    all.add(modele);
    if (sousModele != null) {
      all.addAll(sousModele);
    }
    all.addAll(emhs);
    doValidate(all, modele, value);
    return value.getLogs();
  }

  /**
   * @param scenario le scenario
   * @param validator le process de alidation
   * @return la liste des log du validator
   */
  public List<CtuluLog> validateScenario(final EMHScenario scenario, final CrueValidator validator) {
    doValidate(Collections.singletonList(scenario), null, validator);
    final List<EMHModeleBase> modeles = scenario.getModeles();
    for (final EMHModeleBase emhModeleBase : modeles) {
      validateModele(emhModeleBase.getAllSimpleEMH(), emhModeleBase, validator);
    }
    return validator.getLogs();
  }
}
