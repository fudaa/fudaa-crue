package org.fudaa.dodico.crue.validation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.PredicateUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.factory.PredicateFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

/**
 *
 * @author deniger
 */
public class ValidatorWarnForInactiveContent implements CrueValidator {

  public void addWarnForInactiveEMHs(EMHScenario scenario,
          List<CtuluLog> res) {
    List<EMH> allSimpleEMH = scenario.getAllSimpleEMH();
    ArrayList<EMH> inactiveEMH = CollectionCrueUtil.select(allSimpleEMH,
            PredicateUtils.notPredicate(
            PredicateFactory.PREDICATE_EMH_ACTUALLY_ACTIVE));
    Collections.sort(inactiveEMH, ObjetNommeByNameComparator.INSTANCE);
    if (CollectionUtils.isNotEmpty(inactiveEMH)) {
      addWarning(res, EMHHelper.selectEMHS(inactiveEMH, EnumCatEMH.NOEUD), "listNoeudsInactive.Message", "listNoeudsInactive.Item");
      addWarning(res, EMHHelper.selectEMHS(inactiveEMH, EnumCatEMH.CASIER), "listCasiersInactive.Message",
              "listCasiersInactive.Item");
      addWarning(res, EMHHelper.selectEMHS(inactiveEMH, EnumCatEMH.BRANCHE), "listBranchesInactive.Message",
              "listBranchesInactive.Item");
      addWarning(res, EMHHelper.selectEMHS(inactiveEMH, EnumCatEMH.SECTION), "listSectionsInactive.Message",
              "listSectionsInactive.Item");
    }
  }

  public void addWarning(List<CtuluLog> res, List<EMH> inactiveNoeuds, final String title, final String item) {
    if (CollectionUtils.isNotEmpty(inactiveNoeuds)) {
      CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
      log.setDesc(title);
      res.add(log);
      for (EMH emh : inactiveNoeuds) {
        log.addWarn(item, emh.getNom());
      }
    }
  }

  @Override
  public List<CtuluLog> getLogs() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void validate(Set<Object> childs, EMH emh, EMHModeleBase modeleBase) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public List<CtuluLog> validateScenario(EMHScenario scenario) {
    List<CtuluLog> res = new ArrayList<>();
    addWarnForInactiveEMHs(scenario, res);
    return res;
  }
}
