package org.fudaa.dodico.crue.validation.util;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByIdComparator;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.InfosEMH;

/**
 * Valide le fait qu'une infosEMH appartient à une seule EMH
 *
 * @author deniger
 */
public class DelegateValidatorEMHInfoUnique {

  /**
   * @param scenario ne doit pas etre null
   * @return log rempli avec des erreurs si une meme InfoEMH appartiennent à plusieurs EMH
   */
  public CtuluLog valid(EMHScenario scenario) {
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("validation.infoEMHUnique");
    log.setResource(scenario.getClass().toString());
    Map<InfosEMH, Collection<EMH>> emhByInfoEMH = collectionInfosEMHByEMH(scenario);
    for (Map.Entry<InfosEMH, Collection<EMH>> entry : emhByInfoEMH.entrySet()) {
      Collection<EMH> value = entry.getValue();
       if (value.size() > 1) {
        InfosEMH infoEMH = entry.getKey();
        if (infoEMH instanceof ObjetNomme) {
          log.addSevereError("error.infoEMHNommeUsedSeveralTimes", infoEMH.getClass().getSimpleName(),
                             ((ObjetNomme) infoEMH).getNom(), ToStringHelper.transformToNom(value));
        } else {
          log.addSevereError("error.infoEMHUsedSeveralTimes", infoEMH.getClass().getSimpleName(), ToStringHelper.transformToNom(
                  value));
        }

      }
    }
    return log;
  }

  private Map<InfosEMH, Collection<EMH>> collectionInfosEMHByEMH(EMHScenario scenario) {
    Map<InfosEMH, Collection<EMH>> emhByInfoEMH = new HashMap<>();
    List<EMH> allEMH = scenario.getAllEMH();
    Collections.sort(allEMH, ObjetNommeByIdComparator.INSTANCE);
    for (EMH emh : allEMH) {
      if (emh.getActuallyActive()) {
        List<InfosEMH> infosEMHs = emh.getInfosEMH();
        for (InfosEMH infosEMH : infosEMHs) {
          Collection<EMH> existant = emhByInfoEMH.get(infosEMH);
          if (existant == null) {
            existant = new HashSet<>(4);
            emhByInfoEMH.put(infosEMH, existant);
          }
          existant.add(emh);
        }
      }
    }
    return emhByInfoEMH;
  }
}
