/*
 GPL 2
 */
package org.fudaa.dodico.crue.validation.util;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.CalcTrans;
import org.fudaa.dodico.crue.metier.emh.CatEMHActivable;
import org.fudaa.dodico.crue.metier.emh.DonCLimMScenario;
import org.fudaa.dodico.crue.metier.emh.DonLoiHYConteneur;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.OrdCalc;
import org.fudaa.dodico.crue.metier.emh.OrdCalcPseudoPermIniCliche;
import org.fudaa.dodico.crue.metier.emh.OrdCalcScenario;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTransIniCalcCliche;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

/**
 *
 * @author Frederic Deniger
 */
public class Crue9ExportIgnoreContentValidation {

  public static CtuluLog validToExportInCrue9(CrueData crueData) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    addWarn(crueData.getBranches(), log, "dc.brancheInactive");
    addWarn(crueData.getCasiers(), log, "dc.casierInactive");

    DonCLimMScenario conditionsLim = crueData.getConditionsLim();
    List<String> unused = new ArrayList<>();
    //ordre de calculs:
    OrdCalcScenario ordCalc = crueData.getOCAL();
    for (Calc calc : conditionsLim.getCalc()) {
      if (!ordCalc.isUsed(calc)) {
        unused.add(calc.getNom());
      }
    }
    if (!unused.isEmpty()) {
      log.addWarn("dh.dclm.unused", StringUtils.join(unused, "; "));
    }
    final List<OrdCalc> ordCalcs = ordCalc.getOrdCalc();
    final List<CalcTrans> calcsTrans = EMHHelper.collectCalcTrans(ordCalcs);
    if (calcsTrans.size() > 1) {
      log.addWarn("crue9.compatible.calcTransMoreThanOne");
    }
    if (EMHHelper.containsClass(ordCalcs, OrdCalcPseudoPermIniCliche.class)) {
      log.addWarn("crue9.compatible.ordCalcPseudoPermIniClicheIgnored");
    }
    if (EMHHelper.containsClass(ordCalcs, OrdCalcTransIniCalcCliche.class)) {
      log.addWarn("crue9.compatible.ordCalcTransIniCalcClicheIgnored");
    }
    //ical=0 doit lever une error
    if (!calcsTrans.isEmpty() && !EMHHelper.containsPermanent(ordCalc.getOrdCalc())) {
      log.addSevereError("repriseDh.error");
    }
    DonLoiHYConteneur loiConteneur = crueData.getLoiConteneur();
    unused.clear();
    for (Loi loi : loiConteneur.getLois()) {
      if (!loi.getUsed()) {
        unused.add(loi.getNom());
      }
    }
    if (!unused.isEmpty()) {
      log.addWarn("dh.loi.unused", StringUtils.join(unused, "; "));
    }
    if (!log.isEmpty()) {
      log.setDesc("test.inactif");
    }
    return log;
  }

  public static void addWarn(List<? extends CatEMHActivable> toTest, CtuluLog log, String err) {
    if (toTest == null) {
      return;
    }
    for (CatEMHActivable emh : toTest) {
      if (!emh.getUserActive()) {
        log.addWarn(err, emh.getNom());
      }
    }

  }
}
