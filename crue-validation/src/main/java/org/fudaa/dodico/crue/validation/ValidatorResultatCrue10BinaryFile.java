package org.fudaa.dodico.crue.validation;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.io.rcal.RCalBinaryReader;
import org.fudaa.dodico.crue.io.res.ResCatEMHContent;
import org.fudaa.dodico.crue.io.res.ResultatEntry;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;

/**
 * Valide le contenu du fichier binaire et teste les délimiteurs.
 *
 * @author deniger
 */
public class ValidatorResultatCrue10BinaryFile {

  public CtuluLog validBinaryFile(Collection<ResultatEntry> timeSteps, List<ResCatEMHContent> category,
          Map<ResultatTimeKey, String> delimiteur) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("res.validate.crue10TimeSteps");
    Set<File> noExist = new HashSet<>();
    Set<File> withError = new HashSet<>();
    for (ResultatEntry value : timeSteps) {
      File binFile = value.getBinFile();
      if (binFile.exists()) {
        if (!withError.contains(binFile)) {
          RCalBinaryReader binReader = new RCalBinaryReader(binFile);
          long pos = value.getOffsetInBytes();
          try {
            String readString = binReader.readString(pos);
            if (!StringUtils.equals(readString, delimiteur.get(value.getResultatKey()))) {
              log.addSevereError("res.validate.wrongDelimiteur", binFile.getAbsolutePath(), value.getResultatKey(), readString,
                      delimiteur);
              withError.add(binFile);
            } else {
              for (ResCatEMHContent cat : category) {
                testCategory(pos, cat, log, binReader, value.getResultatKey());
              }
            }
          } catch (IOException ex) {
            Logger.getLogger(ValidatorResultatCrue10BinaryFile.class.getName()).log(Level.INFO, null, ex);
            log.addSevereError("res.validate.binaryFileNotValid", binFile.getAbsolutePath(),
                    value.getResultatKey());
            withError.add(binFile);
          }
        }
      } else if (!noExist.contains(binFile)) {
        noExist.add(binFile);
        log.addSevereError("res.validation.crue10.fileNotExist", binFile.getAbsolutePath());
      }
    }
    return log;
  }

  private boolean testCategory(long offset, ResCatEMHContent cat, CtuluLog log, RCalBinaryReader binReader, ResultatTimeKey key) {
    try {
      String delimiteurCat = cat.getDelimiteur();
      String readString = binReader.readString(offset + cat.getPositionInBytes());
      boolean ok = StringUtils.equals(delimiteurCat, readString);
      if (!ok) {
        log.addSevereError("res.validate.wrongDelimiteur", binReader.getFile().getAbsolutePath(), key,
                readString,
                delimiteurCat);
      }
      return !ok;

    } catch (IOException iOException) {
      Logger.getLogger(ValidatorResultatCrue10BinaryFile.class.getName()).log(Level.INFO, null, iOException);
    }
    return false;

  }
}
