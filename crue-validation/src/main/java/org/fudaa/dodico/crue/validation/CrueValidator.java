package org.fudaa.dodico.crue.validation;

import java.util.List;
import java.util.Set;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;

/**
 * @author deniger
 */
public interface CrueValidator {

  List<CtuluLog> getLogs();

  /**
   * @param childs les objets de l'EMH a tester
   * @param emh l'emh a tester
   * @param modeleBase le modele contenant. null si l'emh a tester est un scenario
   */
  void validate(Set<Object> childs, EMH emh, EMHModeleBase modeleBase);

  List<CtuluLog> validateScenario(EMHScenario scenario);
}
