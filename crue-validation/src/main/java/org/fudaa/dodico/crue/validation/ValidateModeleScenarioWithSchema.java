/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.crue.validation;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe permettant de valider la cohérence des fichiers d'un modèle pour Crue 10.
 *
 * @author deniger
 */
public class ValidateModeleScenarioWithSchema {
  private final Map<String, URL> idFile;
  private final ManagerEMHScenario scenario;
  private final CoeurConfigContrat grammaireVersion;
  private static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
  private static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";
  private static final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";
  private File configToDelete;
  private static final Logger LOGGER = Logger.getLogger(ValidateModeleScenarioWithSchema.class.getName());
  private File dest;

  /**
   * @param destDir le répertoire de travail de cette validation
   * @param scenario le scénario à valider
   */
  public ValidateModeleScenarioWithSchema(final Map<String, URL> destDir, final ManagerEMHScenario scenario,
                                          CoeurConfigContrat version) {
    super();
    this.idFile = destDir;
    this.grammaireVersion = version;
    this.scenario = scenario;
  }

  private void close() {
    CrueFileHelper.deleteFile(dest, getClass());
    CrueFileHelper.deleteFile(configToDelete, getClass());
    dest = null;
  }

  /**
   * @return le fichier utilisé pour écrire le fichier concaténé
   */
  private File getScenarioFile() {
    if (dest == null) {
      try {
        dest = File.createTempFile("valide", "scenario.xml");
      } catch (final IOException e) {
        LOGGER.log(Level.SEVERE, "createTempFile", e);
      }
    }
    return dest;
  }

  public CtuluLogGroup validate() {
    return validate(null);
  }

  /**
   * @param in le conteneur d'erreur: peut etre null
   * @return le manager d'erreur.
   */
  public CtuluLogGroup validate(final CtuluLogGroup in) {
    final CtuluLogGroup res = in == null ? new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE) : in;
    if (res.getDescription() == null) {
      res.setDescription("validation.xml");
    }
    try {
      final CtuluLog analyze = writeScenario();
      if (analyze.containsErrorOrSevereError()) {
        res.getLogs().add(analyze);
        return res;
      }
      validateFile(res);
    } finally {
      close();//pour etre sur d'effacer le fichier.
    }
    return res;
  }

  private CtuluLogGroup validateFile(final CtuluLogGroup res) {

    final Map<String, CtuluLog> map = new HashMap<>();
    try {
      String xsdUrl = grammaireVersion.getXsdUrl("scenario-" + grammaireVersion.getXsdVersion() + ".xsd");
      File xsdFile = new File(xsdUrl);
      URL xsdURL = null;
      if (xsdFile.exists()) {
        try {
          xsdURL = xsdFile.toURI().toURL();
        } catch (Exception e) {
          Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
        }
      } else {
        xsdURL = getClass().getResource(xsdUrl);
      }
      final File scenarioFile = getScenarioFile();
      if (xsdURL == null) {
        res.createLog().addSevereError(BusinessMessages.RESOURCE_BUNDLE.getString("validation.mainXsdNotFound"));
        return res;
      }
      if (scenarioFile == null) {
        res.createLog().addSevereError(BusinessMessages.RESOURCE_BUNDLE.getString("validation.cantCreateTemporaryXmlFile"));
        return res;
      }

      final URL xml = scenarioFile.toURI().toURL();
      final SchemaFactory schemaFactory = SchemaFactory.newInstance(W3C_XML_SCHEMA);

      schemaFactory.newSchema(xsdURL);
      final DefaultHandler handler = new DefaultHandler() {
        @Override
        public void error(final SAXParseException e) throws SAXException {
          final String message = e.getMessage();
          final String msg = message.indexOf(':') > 0 ? StringUtils.substringAfter(message, ":") : message;
          if (msg.contains("no grammar found") || msg.contains("must match DOCTYPE root") || msg.contains("'xml:base'")) {
            return;
          }
          CtuluLog analyze = map.get(e.getSystemId());
          if (analyze == null) {
            analyze = res.createLog();
            analyze.setDefaultResourceBundle(null);//pas de traduction pour ces messages.
            analyze.setResource(StringUtils.substringAfterLast(e.getSystemId(), "/"));
            analyze.setDesc(StringUtils.substringAfterLast(e.getSystemId(), "/"));
            map.put(e.getSystemId(), analyze);
          }
          analyze.addSevereError(BusinessMessages.RESOURCE_BUNDLE.getString("valid.line") + " " + e.getLineNumber() + ": " + msg,
              e.getLineNumber());
        }

        @Override
        public void fatalError(final SAXParseException e) throws SAXException {
          error(e);
        }

        @Override
        public void warning(final SAXParseException e) throws SAXException {
        }
      };
      final SAXParserFactory parser = SAXParserFactory.newInstance();
      parser.setNamespaceAware(true);
      parser.setXIncludeAware(true);
      parser.setValidating(true);
      final SAXParser newSAXParser = parser.newSAXParser();
      newSAXParser.setProperty(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
      newSAXParser.setProperty(JAXP_SCHEMA_SOURCE, new InputSource(xsdURL.toString()));
      newSAXParser.parse(new InputSource(xml.toString()), handler);
    } catch (final Throwable e) {
      res.createLog().addRecord(CtuluLogLevel.ERROR, e.getMessage()).setThrown(e);
    }
    return res;
  }

  /**
   * Ecrit le fichier scenario.xml
   *
   * @return Les messages d'erreur éventuels
   */
  private CtuluLog writeScenario() {
    final ScenarioWriterForXSDValidation.InputFormatWriter out = new ScenarioWriterForXSDValidation.InputFormatWriter();
    out.scenario = scenario;
    out.version = grammaireVersion.getXsdVersion();
    out.idFile = idFile;
    out.crueProps = this.grammaireVersion.getCrueConfigMetier();
    final ScenarioWriterForXSDValidation writer = new ScenarioWriterForXSDValidation(grammaireVersion);
    CtuluLog analyze;
    writer.setFile(getScenarioFile());
    analyze = writer.write(out).getAnalyze();
    configToDelete = writer.getConfLoiFile();
    return analyze;
  }
}
