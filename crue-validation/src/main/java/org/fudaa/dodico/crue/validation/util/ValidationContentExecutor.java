package org.fudaa.dodico.crue.validation.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.metier.emh.EMH;

public class ValidationContentExecutor<C extends Class> {

  private final Map<Class<? extends EMH>, List<ValidationContentParamDelegate<C>>> contentsByEMHClass;

  /**
   * @param contentsByEMHClass contenu par type d'EMH
   */
  public ValidationContentExecutor(Map<Class<? extends EMH>, List<ValidationContentParamDelegate<C>>> contentsByEMHClass) {
    super();
    this.contentsByEMHClass = contentsByEMHClass;
  }

  String msgIfCantContain = "CantContain";

  /**
   * @param msgIfCantContain the msgIfCantContain to set
   */
  public void setMsgIfCantContain(String msgIfCantContain) {
    this.msgIfCantContain = msgIfCantContain;
  }

  public void validContent(final CtuluLog emhLog, final EMH emhContenant, List objectsToTests) {
    final Set<C> usedClass = new HashSet<>(objectsToTests.size());
    final List<C> allClass = new ArrayList<>(objectsToTests.size());
    for (final Object rel : objectsToTests) {
      usedClass.add((C) rel.getClass());
      allClass.add((C) rel.getClass());
    }
    List<ValidationContentParamDelegate<C>> auth = contentsByEMHClass.get(emhContenant.getClass());
    if (auth == null) {
      auth = Collections.emptyList();
    }
    final List<C> authClass = new ArrayList<>(auth.size());
    for (final ValidationContentParamDelegate<C> cp : auth) {
      authClass.add(cp.getRequiredType());
    }
    usedClass.removeAll(authClass);
    if (usedClass.size() > 0) {
      StringBuilder classes = new StringBuilder();
      boolean first = true;
      for (C class1 : usedClass) {
        if (first) {
          first = false;
        } else {
          classes.append(", ");
        }
        classes.append(class1.getSimpleName());

      }
      emhLog.addSevereError(msgIfCantContain, emhContenant.getNom(), emhContenant.getType(),classes);
    }
    for (final ValidationContentParamDelegate<C> cp : auth) {
      cp.addFatalErrorIfNeeded(allClass, emhLog, emhContenant);

    }
  }

}
