package org.fudaa.dodico.crue.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;

/**
 *
 * @author deniger
 */
public class ValidatorNomCasierContent implements CrueValidator {

  @Override
  public List<CtuluLog> getLogs() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  public void validCasier(CatEMHCasier casier, CtuluLog error) {
    CatEMHNoeud noeud = casier.getNoeud();
    if (noeud != null) {
      String expectedCasierName = CruePrefix.changePrefix(noeud.getNom(), CruePrefix.P_NOEUD, CruePrefix.P_CASIER);
      if(!expectedCasierName.equals(casier.getNom())){
       error.addSevereError("validation.casierCorrespondance.CasietNameNotCorresponding", casier.getNom(), noeud.getNom(), expectedCasierName);
      }

    }
  }

  @Override
  public void validate(Set<Object> childs, EMH emh, EMHModeleBase modeleBase) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public List<CtuluLog> validateScenario(EMHScenario scenario) {
    List<CtuluLog> res = new ArrayList<>();
    List<EMHSousModele> sousModeles = scenario.getSousModeles();
    for (EMHSousModele sousModele : sousModeles) {
      CtuluLog log = validateSousModele(sousModele);
      if (log != null) {
        res.add(log);
      }
    }
    return res;
  }

  private CtuluLog validateSousModele(EMHSousModele sousModele) {
    CtuluLog error = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    error.setDesc("validation.casierCorrespondance");
    error.setDescriptionArgs(sousModele.getNom());
    List<CatEMHCasier> casiers = sousModele.getCasiers();
    for (CatEMHCasier casier : casiers) {
      validCasier(casier, error);
    }

    if (error.isNotEmpty()) {
      return error;
    }
    return null;

  }
}
