package org.fudaa.dodico.crue.validation.util;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.metier.emh.EMH;

import java.util.*;

@SuppressWarnings("unchecked")
public class ValidationContentParamDelegate<T extends Class> {
    private final T requiredType;
    private int minOccurence = 1;
    private int maxOccurence = -1;
    private String msgNbOccurenceNotEqualsTo = "msgNbOccurenceMustBeEqualsTo";
    private String msgNbOccurenceNotEqualsToOne = "msgNbOccurenceMustBeEqualsToOne";
    private String msgNbOccurenceNotIn = "msgNbOccurenceNotIn";

    /**
     * @param requiredType le type requis
     * @param minOccurence nombre d'occurence min attendue
     * @param maxOccurence nombre d'occurence max attendue
     */
    public ValidationContentParamDelegate(final T requiredType, final int minOccurence, final int maxOccurence) {
        super();
        this.requiredType = requiredType;
        this.minOccurence = minOccurence;
        this.maxOccurence = maxOccurence;
    }

    public static List<ValidationContentParamDelegate> create(final ValidationContentParamDelegate... params) {
        return Arrays.asList(params);
    }

    private boolean isMaxRespected(final int nbToTest) {
        return maxOccurence < 0 || nbToTest <= maxOccurence;
    }

    private boolean isMinRespected(final int nbToTest) {
        return minOccurence < 0 || nbToTest >= minOccurence;
    }

    private void addFatalErrorIfNeeded(final int nbToTest, final CtuluLog log, final EMH emh) {
        if ((isMaxRespected(nbToTest) && isMinRespected(nbToTest))) {
            return;
        }
        if (minOccurence == maxOccurence) {
            log.addSevereError(minOccurence == 1 ? msgNbOccurenceNotEqualsToOne : msgNbOccurenceNotEqualsTo, emh.getNom(), emh.getTypei18n(),
                    requiredType.getSimpleName(), maxOccurence, nbToTest);
        } else {
            log.addSevereError(msgNbOccurenceNotIn, emh.getNom(), emh.getType(), requiredType.getSimpleName(), minOccurence,
                    maxOccurence, nbToTest);
        }
    }

    public void addFatalErrorIfNeeded(final Collection<T> allClass, final CtuluLog log, final EMH emh) {
        final int nbDansRelation = CollectionUtils.cardinality(requiredType, allClass);
        addFatalErrorIfNeeded(nbDansRelation, log, emh);
    }

    /**
     * @param msgNbOccurenceMustBeEqualsTo the msgNbOccurenceMustBeEqualsTo to set
     */
    protected void setMsgNbOccurenceNotEqualsTo(String msgNbOccurenceMustBeEqualsTo) {
        this.msgNbOccurenceNotEqualsTo = msgNbOccurenceMustBeEqualsTo;
    }

    /**
     * @param msgNbOccurenceNotIn the msgNbOccurenceNotIn to set
     */
    protected void setMsgNbOccurenceNotIn(String msgNbOccurenceNotIn) {
        this.msgNbOccurenceNotIn = msgNbOccurenceNotIn;
    }

    /**
     * @return the requiredType
     */
    protected T getRequiredType() {
        return requiredType;
    }

    /**
     * @param msgNbOccurenceNotEqualsToOne the msgNbOccurenceNotEqualsToOne to set
     */
    public void setMsgNbOccurenceNotEqualsToOne(String msgNbOccurenceNotEqualsToOne) {
        this.msgNbOccurenceNotEqualsToOne = msgNbOccurenceNotEqualsToOne;
    }
}
