package org.fudaa.dodico.crue.validation;

/**
 * Un item a valider
 */
public class ValidatingItem<O> {
    private final ValidatingIndicator indicator;
    private final O objectToValidate;

    public ValidatingItem( O objectToValidate,ValidatingIndicator indicator) {
        this.indicator = indicator;
        this.objectToValidate = objectToValidate;
    }

    public void clearMessage() {
        if (indicator != null) {
            indicator.clearMessage();
        }
    }

    public void setErrorMessage(String errorMessage) {
        if (indicator != null) {
            indicator.setErrorMessage(errorMessage);
        }
    }

    public O getObjectToValidate() {
        return objectToValidate;
    }

    public void setWarningMessage(String warningMessage) {
        if (indicator != null) {
            indicator.setWarningMessage(warningMessage);
        }
    }
}
