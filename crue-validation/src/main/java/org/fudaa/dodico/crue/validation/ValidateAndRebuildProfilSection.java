package org.fudaa.dodico.crue.validation;

import java.util.*;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.comparaison.CrueComparatorHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierLitNomme;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.config.ccm.PropertyValidator;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.comparator.Point2dAbscisseComparator;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilCasier;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilEtiquette;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.LitNumerote;
import org.fudaa.dodico.crue.metier.emh.LitUtile;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;
import org.fudaa.dodico.crue.metier.emh.PtProfil;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.metier.helper.EtiquetteIndexed;
import org.fudaa.dodico.crue.metier.helper.LitNommeIndexed;
import org.fudaa.dodico.crue.metier.helper.LitNumeroteIndex;
import org.fudaa.dodico.crue.metier.helper.LitNumeroteIndexComparator;

/**
 *
 * Attention: cette classe ne fait pas que de la validation mais reconstruit les lits nommé selon la spec.
 *
 *
 * @author deniger
 */
public class ValidateAndRebuildProfilSection implements CrueValidator {

  public static boolean containLitMineur(final List<LitNumerote> lit) {
    for (final LitNumerote litNumerote : lit) {
      if (litNumerote.getIsLitMineur()) {
        return true;
      }
    }
    return false;
  }

  public static List<LitNommeIndexed> createLitNommeIndexed(final CrueConfigMetierLitNomme ccm) {
    final List<LitNommeIndexed> litNommeIndexed = new ArrayList<>();
    for (final LitNomme litNomme : ccm.getLitNomme()) {
      litNommeIndexed.add(new LitNommeIndexed(litNomme));
    }
    return litNommeIndexed;
  }
  private Point2dAbscisseComparator ptProfilComparator;
  private final CrueConfigMetier props;
  private final ScenarioAutoModifiedState modifiedState;
  /**
   * contien le DonFrt Vide par sous-modele
   */
  final Map<String, DonFrt> emptyDonFrtBySousModeleName = new HashMap<>();
  private PropertyEpsilon epsilon;

  public ValidateAndRebuildProfilSection(final CrueConfigMetier props, final ScenarioAutoModifiedState modifiedState) {
    this.props = props;
    this.modifiedState = modifiedState;
  }

  public ScenarioAutoModifiedState getModifiedState() {
    return modifiedState;
  }

  private void setModified() {
    if (modifiedState != null) {
      modifiedState.setProfilModified();
    }
  }

  private void validCasier(final EMHSousModele concatSousModele, final List<CtuluLog> logs, final ScenarioAutoModifiedState modifiedState) {
    final List<CatEMHCasier> casiers = concatSousModele.getCasiers();
    final Set<DonPrtGeoProfilCasier> profilsCasier = new HashSet<>();
    //le sous-modele contient les DontPrtGeoProfilCasier (pour l'instant ?).
    profilsCasier.addAll(EMHHelper.selectClass(concatSousModele.getInfosEMH(), DonPrtGeoProfilCasier.class));
    for (final CatEMHCasier casier : casiers) {
      final List<DonPrtGeoProfilCasier> profils = EMHHelper.selectClass(casier.getInfosEMH(), DonPrtGeoProfilCasier.class);
      profilsCasier.addAll(profils);
    }
    final List<DonPrtGeoProfilCasier> sortedProfilCasier = new ArrayList<>(profilsCasier);
    Collections.sort(sortedProfilCasier, ObjetNommeByNameComparator.INSTANCE);
    for (final DonPrtGeoProfilCasier profilCasier : sortedProfilCasier) {
      final CtuluLog validation = validateDonPrtGeoProfilCasier(profilCasier);
      if (validation != null && validation.isNotEmpty()) {
        logs.add(validation);
      }

    }
  }

  private void validSections(final EMHSousModele concatSousModele,
                             final List<CtuluLog> logs, final ScenarioAutoModifiedState modifiedState) {
    final List<CatEMHSection> sections = concatSousModele.getSections();
    final Set<DonPrtGeoProfilSection> profilSections = new HashSet<>();
    for (final CatEMHSection section : sections) {
      profilSections.addAll(EMHHelper.selectClass(section.getInfosEMH(), DonPrtGeoProfilSection.class));
    }
    final List<DonPrtGeoProfilSection> sortedProfilSections = new ArrayList<>(profilSections);
    Collections.sort(sortedProfilSections, ObjetNommeByNameComparator.INSTANCE);


    for (final DonPrtGeoProfilSection profil : sortedProfilSections) {
      final CtuluLog validation = validateAndRebuildDonPrtGeoProfilSection(profil);
      if (validation != null && validation.isNotEmpty()) {
        logs.add(validation);
      }
    }
  }

  @Override
  public void validate(final Set<Object> childs, final EMH emh, final EMHModeleBase modeleBase) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public List<CtuluLog> getLogs() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public List<CtuluLog> validateScenario(final EMHScenario scenario) {
    final EMHSousModele concatSousModele = scenario.getConcatSousModele();
    final List<CtuluLog> logs = new ArrayList<>();
    validSections(concatSousModele, logs, modifiedState);
    validCasier(concatSousModele, logs, modifiedState);
    return logs;
  }

  public CtuluLog validateDonPrtGeoProfilCasier(final DonPrtGeoProfilCasier section) {
    createProfilEqualsTester();
    return validateDonPrtGeoProfilCasier(section, ptProfilComparator);
  }

  private CtuluLog validateDonPrtGeoProfilCasier(final DonPrtGeoProfilCasier section,
                                                 final Point2dAbscisseComparator ptProfilComparator) {
    final CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    res.setDesc(section.getNom());
    ValidationHelper.validateObject(null, res, section, props);
    ValidationHelperProfil.validePtProfil(section.getPtProfil(), props, res, section.getId());
    final LitUtile litUtile = section.getLitUtile();
    final PropertyEpsilon epsilonZ = props.getEpsilon(CrueConfigMetierConstants.PROP_Z);
    if (litUtile == null) {
      res.addSevereError("validate.litUtile.notDefined");
    } else {
      final int positionLimDeb = CrueComparatorHelper.getEqualsPtProfilSorted(litUtile.getLimDeb(), section.getPtProfil(), ptProfilComparator);
      final int positionLimFin = CrueComparatorHelper.getEqualsPtProfilSorted(litUtile.getLimFin(), section.getPtProfil(), ptProfilComparator);
      if (positionLimDeb < 0) {
        res.addSevereError("validate.litUtile.limDebNotFound", litUtile.toString(props, EnumToString.COMPLETE, DecimalFormatEpsilonEnum.PRESENTATION));
      } else {
        ensureZIsSame(section.getPtProfil(), positionLimDeb, epsilonZ, litUtile.getLimDeb());
      }
      if (positionLimFin < 0) {
        res.addSevereError("validate.litUtile.limFinNotFound", litUtile.toString(props, EnumToString.COMPLETE, DecimalFormatEpsilonEnum.PRESENTATION));
      } else {
        ensureZIsSame(section.getPtProfil(), positionLimFin, epsilonZ, litUtile.getLimFin());
      }
    }

    return res;
  }

  public CtuluLog validateAndRebuildDonPrtGeoProfilSection(final DonPrtGeoProfilSection section) {
    return validateAndRebuildDonPrtGeoProfilSection(section, section.getParent());
  }

  /**
   * Ce test valide les données de bases avant la reconstruction des lits.
   *
   * @param section la section
   * @param parent le sous-modele
   * @return les logs de validation
   */
  public CtuluLog validateAndRebuildDonPrtGeoProfilSection(final DonPrtGeoProfilSection section, final EMHSousModele parent) {
    final CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    createEpsilon();
    res.setDesc(section.getNom());
    //on valide le nombre de lit
    validatePtProfilAndNbrLit(section, res);

    //on ne va pas plus loin car sinon on ne peut pas continuer le traitement
    if (res.containsErrorOrSevereError()) {
      return res;
    }
    //si pas de lit mineur on va transformer les lits actifs en lits mineurs.

    final List<LitNumerote> litNumerotes = section.getLitNumerote();
    if (!containLitMineur(litNumerotes)) {
      boolean modificationDone = false;
      for (final LitNumerote litNumerote : litNumerotes) {
        if (litNumerote.getIsLitActif()) {
          litNumerote.setIsLitMineur(true);
          modificationDone = true;
        }
      }
      if (modificationDone) {
        setModified();
        res.addError("validate.litActifTransformToMineur", section.getNom());
      }
    }

    final List<LitNumeroteIndex> litNumeroteIndexed = createLitNumeroteIndex(section, res);

    //on ne va pas plus loin car sinon on ne peut pas continuer le traitement
    if (res.containsSevereError()) {
      return res;
    }
    litNumeroteIndexed.sort(new LitNumeroteIndexComparator(litNumeroteIndexed));
    rebuildLits(section, litNumeroteIndexed, res, parent);
    //on valide les frottements:
    validateFrottement(section, res);
    valideEtiquettes(section, res);
    return res;
  }

  private LitNumeroteIndex createNewLitNumerote(final DonPrtGeoProfilSection section, final int ptProfilIdx, final LitNommeIndexed lit, final int idForLitNomme, final CtuluLog log, final EMHSousModele parent) {
    final List<PtProfil> ptProfil = section.getPtProfil();
    final LitNumerote litNumerote = new LitNumerote();
    final DonFrt donFrt = findEmptyDonFrt(log, lit.getLitNomme().getNom(), parent);
    litNumerote.setFrot(donFrt);
    litNumerote.setLimDeb(ptProfil.get(ptProfilIdx));
    litNumerote.setLimFin(ptProfil.get(ptProfilIdx));
    litNumerote.setNomLit(lit.getLitNomme());
    litNumerote.setIsLitActif(idForLitNomme == CrueConfigMetierLitNomme.MAJEUR_END || idForLitNomme == CrueConfigMetierLitNomme.MAJEUR_START);
    final LitNumeroteIndex index = new LitNumeroteIndex();
    index.setLitNumerote(litNumerote);
    index.setIdxEnd(ptProfilIdx);
    index.setIdxStart(ptProfilIdx);
    lit.addLitNumerote(index);
    return index;
  }

  private DonFrt findEmptyDonFrt(final CtuluLog log, final String litNomme, final EMHSousModele parent) {
    DonFrt frt = emptyDonFrtBySousModeleName.get(parent.getNom());
    if (frt == null) {
      frt = parent.getFrtConteneur().getEmptyDonFrtFkSto(props);
      emptyDonFrtBySousModeleName.put(parent.getNom(), frt);
      if (frt == null) {
        log.addSevereError("validate.lit.cantCreateLitNumerote.EmptyDonFrtNotFound", litNomme);
      }
    }

    return frt;
  }

  public CrueIOResu<List<LitNommeIndexed>> getAsIndexedData(final DonPrtGeoProfilSection section) {
    final CrueIOResu<List<LitNommeIndexed>> res = new CrueIOResu<>();
    res.setAnalyse(new CtuluLog(BusinessMessages.RESOURCE_BUNDLE));
    final CtuluLog log = res.getAnalyse();
    final List<LitNumeroteIndex> litNumeroteIndexed = createLitNumeroteIndex(section, log);
    final List<LitNommeIndexed> litNommeIndexeds = buildLitNommeIndexed(litNumeroteIndexed, log);
    res.setMetier(litNommeIndexeds);
    return res;
  }

  public CrueIOResu<List<EtiquetteIndexed>> getEtiquettesAsIndexed(final DonPrtGeoProfilSection section) {
    final CrueIOResu<List<EtiquetteIndexed>> res = new CrueIOResu<>();
    res.setAnalyse(new CtuluLog(BusinessMessages.RESOURCE_BUNDLE));
    final CtuluLog log = res.getAnalyse();
    final List<EtiquetteIndexed> valideEtiquettes = valideEtiquettes(section, log);
    res.setMetier(valideEtiquettes);
    return res;
  }

  private boolean rebuildLits(final DonPrtGeoProfilSection section, final List<LitNumeroteIndex> litNumeroteIndexed, final CtuluLog res, final EMHSousModele parent) {
    final int idxMineur = getLitMineurFirstIndex(litNumeroteIndexed);
    if (idxMineur < 0) {
      res.addSevereError("validate.lit.litMineurNotFound");
      return true;
    }
    if (isLitMineurNull(litNumeroteIndexed, idxMineur)) {
      res.addSevereError("validate.lit.litMineurNull");
      return true;
    }
    final List<LitNommeIndexed> litNommeIndexeds = buildLitNommeIndexed(litNumeroteIndexed, res);
    if (res.containsSevereError()) {
      return false;
    }
    cleanEmptyLitNumerote(litNommeIndexeds, parent, res);

    final int litMineur = CrueConfigMetierLitNomme.MINEUR;
    //on part du lit mineur pour construire eventuellement les nouveaux Litnumerotes
    for (int i = litMineur + 1; i < litNommeIndexeds.size(); i++) {
      final LitNommeIndexed lit = litNommeIndexeds.get(i);
      //pas de lit on en construit un
      if (lit.getLitNumerotesSize() == 0) {
        final LitNommeIndexed previous = litNommeIndexeds.get(i - 1);
        final int idx = previous.getlastIdx();
        createNewLitNumerote(section, idx, lit, i, res, parent);
        if (res.containsSevereError()) {
          return false;
        }
      }
    }
    for (int i = litMineur - 1; i >= 0; i--) {
      final LitNommeIndexed lit = litNommeIndexeds.get(i);
      //pas de lit on en construit un
      if (lit.getLitNumerotesSize() == 0) {
        final LitNommeIndexed previous = litNommeIndexeds.get(i + 1);
        final int idx = previous.getFirstIdx();
        createNewLitNumerote(section, idx, lit, i, res, parent);
        if (res.containsSevereError()) {
          return false;
        }
      }
    }
    final List<LitNumerote> oldLitNumerotes = section.getLitNumerote();
    final List<LitNumerote> newLitNumerote = new ArrayList<>();
    for (final LitNommeIndexed lit : litNommeIndexeds) {
      lit.applyLitNomme();
      lit.fill(newLitNumerote);
    }
    //on valide que les lits nommés sont bien consecutifs:
    final int nb = litNommeIndexeds.size();
    for (int i = 1; i < nb; i++) {
      final int before = litNommeIndexeds.get(i - 1).getlastIdx();
      final int current = litNommeIndexeds.get(i).getFirstIdx();
      if (current != before) {
        res.addSevereError("validate.litNomme.notSuccessifs", litNommeIndexeds.get(i - 1).getLitNomme().getNom(), litNommeIndexeds.get(i).getLitNomme().getNom());
      }
    }
    if (!isSame(oldLitNumerotes, newLitNumerote)) {
      setModified();
    }
    section.setLitNumerote(newLitNumerote);
    return true;
  }

  private boolean isSame(final List<LitNumerote> l1, final List<LitNumerote> l2) {
    if (l1.size() != l2.size()) {
      return false;
    }
    final int nb = l1.size();
    for (int i = 0; i < nb; i++) {
      if (!l1.get(i).isSame(l2.get(i), ptProfilComparator)) {
        return false;
      }

    }
    return true;
  }

  private void validatePtProfilAndNbrLit(final DonPrtGeoProfilSection section, final CtuluLog res) {
    final List<PtProfil> ptProfils = section.getPtProfil();
    final int size = section.getLitNumerote().size();
    final PropertyValidator validator = props.getValidator("nbrLitMax");
    validator.validateNumber(section.getNom(), size, res);
    ValidationHelper.validateObject(null, res, section, props);
    if (section.getFente() != null) {
      ValidationHelper.validateObject(null, res, section.getFente(), props);
    }
    //valide les abscisses strictement croissantes
    ValidationHelperProfil.validePtProfil(ptProfils, props, res, BusinessMessages.getString(
            "validate.pref.profileName", section.getNom()));
  }

  private List<LitNumeroteIndex> createLitNumeroteIndex(final DonPrtGeoProfilSection section, final CtuluLog res) {
    final List<PtProfil> ptProfils = section.getPtProfil();
    // on valide les litNumerotes.
    final List<LitNumeroteIndex> litNumeroteIndexed = new ArrayList<>();
    final PropertyEpsilon epsilonZ = props.getEpsilon(CrueConfigMetierConstants.PROP_Z);
    for (final LitNumerote lit : section.getLitNumerote()) {
      createProfilEqualsTester();
      final int positionLimDeb = CrueComparatorHelper.getEqualsPtProfilSorted(lit.getLimDeb(), ptProfils, ptProfilComparator);
      final int positionLimFin = CrueComparatorHelper.getEqualsPtProfilSorted(lit.getLimFin(), ptProfils, ptProfilComparator);
      if (lit.getIsLitMineur() && !lit.getIsLitActif()) {
        res.addSevereError(BusinessMessages.getString("validate.frt.listMineurNonActif", ValidationHelperProfil.getNomLit(
                lit, props)));
      }
      if (positionLimDeb < 0) {
        res.addSevereError("validate.lit.limDebNotFound", ValidationHelperProfil.getNomLit(lit, props));
      } else {
        ensureZIsSame(section.getPtProfil(), positionLimDeb, epsilonZ, lit.getLimDeb());
      }
      if (positionLimFin < 0) {
        res.addSevereError("validate.lit.limFinNotFound", ValidationHelperProfil.getNomLit(lit, props));
      } else {
        ensureZIsSame(section.getPtProfil(), positionLimFin, epsilonZ, lit.getLimFin());
      }
      final LitNumeroteIndex index = new LitNumeroteIndex();
      index.setIdxEnd(positionLimFin);
      index.setIdxStart(positionLimDeb);
      index.setLitNumerote(lit);
      if (!index.isValid()) {
        res.addSevereError("validate.lit.limFinMustBeGreatedThanLitDeb", ValidationHelperProfil.getNomLit(lit, props));
      }
      litNumeroteIndexed.add(index);
    }
    return litNumeroteIndexed;
  }

  private void validateFrottement(final DonPrtGeoProfilSection section, final CtuluLog res) {
    // on valide les litNumerotes.
    for (final LitNumerote lit : section.getLitNumerote()) {
      // frottement null sur un lit: que faire ?
      if (lit.getFrot() == null) {
        res.addSevereError(BusinessMessages.getString("validate.frt.forLit.isNull", ValidationHelperProfil.getNomLit(lit,
                props)));
      } else {
        //toute
        ValidationHelperLoi.validateDonFrt(lit.getFrot(), props, res, BusinessMessages.getString("validate.frt.forLit",
                ValidationHelperProfil.getNomLit(
                lit,
                props),
                lit.getFrot().getNom()));
        //pour les lits non actifs, le frottement doit être null
        if (!lit.getIsLitActif()) {
          final DonFrt frot = lit.getFrot();
          if (!frot.getLoi().getType().equals(EnumTypeLoi.LoiZFKSto)) {
            res.addError(BusinessMessages.getString("validate.frt.forLitNonActif.noFkoStoUsed", frot.getNom(),
                    ValidationHelperProfil.getNomLit(lit, props)));
          }
          final boolean err = !isEmptyDonFrt(frot);
          if (err) {
            res.addError(BusinessMessages.getString("validate.frt.forLitNonActif.noNull", frot.getNom(),
                    ValidationHelperProfil.getNomLit(lit, props)));
          }
        }
      }
    }
  }

  public int getLitMineurFirstIndex(final List<LitNumeroteIndex> litNumeroteIndexed) {
    final int size = litNumeroteIndexed.size();
    for (int i = 0; i < size; i++) {
      if (litNumeroteIndexed.get(i).getLitNumerote().getIsLitMineur()) {
        return i;
      }
    }
    return -1;
  }

  public boolean isLitMineurNull(final List<LitNumeroteIndex> litNumeroteIndexed, final int start) {
    final int size = litNumeroteIndexed.size();
    for (int i = start; i < size; i++) {
      final LitNumerote litNumerote = litNumeroteIndexed.get(i).getLitNumerote();
      if (litNumerote.getIsLitMineur()) {
        final double delta = litNumerote.getLimFin().getXt() - litNumerote.getLimDeb().getXt();
        if (!epsilon.isZero(delta)) {
          return false;
        }
      }
    }
    return true;
  }

  private void createProfilEqualsTester() {
    if (ptProfilComparator == null) {
      createEpsilon();
      ptProfilComparator = new Point2dAbscisseComparator(epsilon);
    }
  }

  private void createEpsilon() {
    if (epsilon == null) {
      epsilon = getEpsilon();
    }
  }

  private PropertyEpsilon getEpsilon() {
    return props.getConfLoi().get(EnumTypeLoi.LoiPtProfil).getVarAbscisse().getEpsilon();
  }

  protected List<LitNommeIndexed> buildLitNommeIndexed(final List<LitNumeroteIndex> litNumeroteIndexed, final CtuluLog res) {
    final int idxMineur = getLitMineurFirstIndex(litNumeroteIndexed);
    if (idxMineur < 0) {
      return new ArrayList<>();
    }
    final int firstPtIdxmineur = litNumeroteIndexed.get(idxMineur).getIdxStart();
    //on reconstruit les lits:
    final CrueConfigMetierLitNomme litNomme = props.getLitNomme();

    final List<LitNommeIndexed> litNommeIndexeds = createLitNommeIndexed(litNomme);
    final int size = litNumeroteIndexed.size();
    for (int i = 0; i < size; i++) {
      final LitNumeroteIndex indexed = litNumeroteIndexed.get(i);
      final LitNumerote litNumerote = indexed.getLitNumerote();
      if (litNumerote.getIsLitMineur()) {
        litNommeIndexeds.get(CrueConfigMetierLitNomme.MINEUR).addLitNumerote(indexed);
      } else if (litNumerote.getIsLitActif()) {
        final int id = indexed.getIdxStart() <= firstPtIdxmineur ? CrueConfigMetierLitNomme.MAJEUR_START : CrueConfigMetierLitNomme.MAJEUR_END;
        litNommeIndexeds.get(id).addLitNumerote(indexed);
      } else {
        final int id = indexed.getIdxStart() <= firstPtIdxmineur ? CrueConfigMetierLitNomme.STO_START : CrueConfigMetierLitNomme.STO_END;
        litNommeIndexeds.get(id).addLitNumerote(indexed);
      }
    }
    for (final LitNommeIndexed litNommeIndexed : litNommeIndexeds) {
      final boolean ok = litNommeIndexed.isLitsSuccessifs();
      if (!ok) {
        res.addSevereError("validate.lit.notSuccessifs", litNommeIndexed.getLitNomme().getNom());
      }
    }
    return litNommeIndexeds;
  }

  private List<EtiquetteIndexed> valideEtiquettes(final DonPrtGeoProfilSection section, final CtuluLog res) {
    final List<LitNommeIndexed> asIndexedData = getAsIndexedData(section).getMetier();
    if (asIndexedData == null) {//dans ce cas l'erreur est déjà levée en amont
      return Collections.emptyList();
    }
    final LitNommeIndexed litMineurIndexed = getLitMineur(asIndexedData);
    if (litMineurIndexed == null) {
      return Collections.emptyList();
    }
    final List<EtiquetteIndexed> result = new ArrayList<>();
    final List<DonPrtGeoProfilEtiquette> etiquettes = new ArrayList<>();
    final List<DonPrtGeoProfilEtiquette> etiquettesInitiales = section.getEtiquettes();
    valideEtiquetteOccurences(etiquettesInitiales, res);
    final Map<ItemEnum, DonPrtGeoProfilEtiquette> etiquetteByType = DonPrtGeoProfilEtiquette.createMap(etiquettesInitiales);
    DonPrtGeoProfilEtiquette etiquette = etiquetteByType.get(props.getLitNomme().getEtiquetteThalweg());
    //on valide THALWEG
    if (etiquette != null) {
      final int position = CrueComparatorHelper.getEqualsPtProfilSorted(etiquette.getPoint(), section.getPtProfil(), ptProfilComparator);
      if (!isContained(position, litMineurIndexed)) {
        if (section.getFente() != null) {
          res.addSevereError("validate.thalweg.NotInLitMineur.Fente");
        } else {
          res.addWarn("validate.thalweg.NotInLitMineur");
        }
      }
      etiquettes.add(etiquette);
      final EtiquetteIndexed indexed = new EtiquetteIndexed();
      indexed.setEtiquette(etiquette);
      indexed.setIdx(position);
      result.add(indexed);
    } else {
      final EtiquetteIndexed indexed = createThalweg(section, asIndexedData);
      etiquettes.add(indexed.getEtiquette());
      result.add(indexed);
    }

    //on valide AXE_HYD
    etiquette = etiquetteByType.get(props.getLitNomme().getEtiquetteAxeHyd());
    //on valide
    if (etiquette != null) {
      final int position = CrueComparatorHelper.getEqualsPtProfilSorted(etiquette.getPoint(), section.getPtProfil(), ptProfilComparator);
      if (!isContained(position, litMineurIndexed)) {
        res.addSevereError("validate.axeHyd.NotInLitMineur");
      }
      etiquettes.add(etiquette);
      final EtiquetteIndexed indexed = new EtiquetteIndexed();
      indexed.setEtiquette(etiquette);
      indexed.setIdx(position);
      result.add(indexed);
    } else {
      final EtiquetteIndexed indexed = createAxeHyd(section, litMineurIndexed);
      etiquettes.add(indexed.getEtiquette());
      result.add(indexed);
    }
    section.setEtiquettes(etiquettes);
    return result;
  }

  private LitNommeIndexed getLitMineur(final List<LitNommeIndexed> litNommes) {
    final LitNomme mineur = props.getLitNomme().getMineur();
    for (final LitNommeIndexed litNommeIndexed : litNommes) {
      if (mineur.equals(litNommeIndexed.getLitNomme())) {
        return litNommeIndexed;
      }
    }
    return null;
  }

  private boolean isContained(final int position, final LitNommeIndexed litNomme) {
    return litNomme.contains(position);
  }

  private EtiquetteIndexed createThalweg(final DonPrtGeoProfilSection section, final List<LitNommeIndexed> litNommes) {
    int foundIdx = -1;
    double minZ = Double.MAX_VALUE;
    // tous les lits actifs sont parcourus dans l'ordre et le dernier point rencontré avec la cote la plus basse est choisi.
    final CrueConfigMetierLitNomme litNomme = props.getLitNomme();
    for (final LitNommeIndexed litNommeIndexed : litNommes) {
      if (litNommeIndexed.getLitNumerotesSize() > 0 && litNomme.isActif(litNommeIndexed.getLitNomme())) {
        for (int i = litNommeIndexed.getFirstIdx(); i <= litNommeIndexed.getlastIdx(); i++) {
          final double z = section.getPtProfil().get(i).getZ();
          if (foundIdx < 0 || z <= minZ) {
            foundIdx = i;
            minZ = z;
          }
        }
      }

    }
    final DonPrtGeoProfilEtiquette etiquette = new DonPrtGeoProfilEtiquette();
    etiquette.setTypeEtiquette(props.getLitNomme().getEtiquetteThalweg());
    etiquette.setPoint(new PtProfil(section.getPtProfil().get(foundIdx)));
    final EtiquetteIndexed indexed = new EtiquetteIndexed();
    indexed.setEtiquette(etiquette);
    indexed.setIdx(foundIdx);
    return indexed;
  }

  private EtiquetteIndexed createAxeHyd(final DonPrtGeoProfilSection section, final LitNommeIndexed litMineur) {
    final int firstIdx = litMineur.getFirstIdx();
    final int lastIdx = litMineur.getlastIdx();
    final double xMiddle = (section.getPtProfil().get(firstIdx).getXt() + section.getPtProfil().get(lastIdx).getXt()) / 2d;
    int foundIdx = firstIdx;
    double minDistance = Math.abs(section.getPtProfil().get(foundIdx).getXt() - xMiddle);
    for (int i = firstIdx + 1; i <= lastIdx; i++) {
      final double distance = Math.abs(section.getPtProfil().get(i).getXt() - xMiddle);
      if (distance < minDistance) {
        foundIdx = i;
        minDistance = distance;
      }
    }
    final DonPrtGeoProfilEtiquette etiquette = new DonPrtGeoProfilEtiquette();
    etiquette.setTypeEtiquette(props.getLitNomme().getEtiquetteAxeHyd());
    etiquette.setPoint(new PtProfil(section.getPtProfil().get(foundIdx)));
    final EtiquetteIndexed indexed = new EtiquetteIndexed();
    indexed.setEtiquette(etiquette);
    indexed.setIdx(foundIdx);
    return indexed;
  }

  private void valideEtiquetteOccurences(final List<DonPrtGeoProfilEtiquette> etiquettesInitiales, final CtuluLog res) {
    if (etiquettesInitiales != null) {
      final List<ItemEnum> typeUsed = new ArrayList<>();
      for (final DonPrtGeoProfilEtiquette et : etiquettesInitiales) {
        typeUsed.add(et.getTypeEtiquette());
      }
      final List<ItemEnum> etiquettesEnums = props.getLitNomme().getEtiquettesEnums();
      for (final ItemEnum etiquette : etiquettesEnums) {
        if (props.getLitNomme().getEtiquetteLimLit().equals(etiquette)) {
          continue;
        }
        final int nb = CollectionUtils.cardinality(etiquette, typeUsed);
        if (nb > 1) {
          res.addSevereError("validate.etiquette.definedSeveralTimes", etiquette.geti18n(), Integer.toString(nb));
        }

      }
    }
  }

  public void ensureZIsSame(final List<PtProfil> ptProfil, final int positionLimDeb, final PropertyEpsilon epsilonZ, final PtProfil pt) {
    final double z = ptProfil.get(positionLimDeb).getZ();
    if (!epsilonZ.isSame(z, pt.getZ())) {
      pt.setZ(z);
      if (modifiedState != null) {
        modifiedState.setProfilModified();
      }
    }
  }

  private LitNumeroteIndex findWithEmptyDonFrt(final List<LitNumeroteIndex> lits) {
    for (final LitNumeroteIndex litNumeroteIndex : lits) {
      final DonFrt frot = litNumeroteIndex.getLitNumerote().getFrot();
      if (isEmptyDonFrt(frot)) {
        return litNumeroteIndex;
      }
    }
    return null;
  }

  private void cleanEmptyLitNumerote(final List<LitNommeIndexed> litNommeIndexeds, final EMHSousModele sousModele, final CtuluLog log) {
    //nettoyage des lit numerotes vides: un lit numerote vide est utile que si c'est le seul lit d'un lit nommé.
    for (final LitNommeIndexed litNommeIndexed : litNommeIndexeds) {
      if (litNommeIndexed.getLitNumerotesSize() == 0) {//pas de lits numerotes: rien a faire.
        continue;
      }
      if (litNommeIndexed.containsAnEmptyLitNumerotes()) {
        final List<LitNumeroteIndex> litNumerotes = new ArrayList<>(litNommeIndexed.getLitNumerotes());
        if (litNommeIndexed.containsOnlyEmptyLitNumerotes()) {
          LitNumeroteIndex litWithEmptyDonFrt = findWithEmptyDonFrt(litNumerotes);
          if (litWithEmptyDonFrt == null) {
            litWithEmptyDonFrt = litNumerotes.get(0);
            litWithEmptyDonFrt.getLitNumerote().setFrot(findEmptyDonFrt(log, litNommeIndexed.getLitNomme().getNom(), sousModele));

          }
          litNumerotes.clear();
          litNumerotes.add(litWithEmptyDonFrt);
          //garder le premier lit avec un frottement vide
        } else {
          //on enlève les lits vides
          for (final Iterator<LitNumeroteIndex> it = litNumerotes.iterator(); it.hasNext();) {
            final LitNumeroteIndex litNumeroteIndex = it.next();
            if (litNumeroteIndex.isEmpty()) {
              it.remove();
            }

          }

        }
        litNommeIndexed.setLitNumerote(litNumerotes);
      }
    }
  }

  private boolean isEmptyDonFrt(final DonFrt frot) {
    if (frot == null || frot.getLoi() == null || frot.getLoi().getEvolutionFF() == null) {
      return true;
    }
    final List<PtEvolutionFF> ptEvolutionFF = frot.getLoi().getEvolutionFF().getPtEvolutionFF();
    if (ptEvolutionFF.size() > 1) {
      return false;
    } else if (ptEvolutionFF.size() == 1) {
      final PtEvolutionFF pt = ptEvolutionFF.get(0);
      if (!props.getLoiAbscisseEps(frot.getLoi()).isZero(pt.getAbscisse()) || !props.getLoiOrdonneeEps(frot.getLoi()).isZero(pt.getOrdonnee())) {
        return false;
      }
    }
    return true;
  }
}
