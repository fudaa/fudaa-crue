package org.fudaa.dodico.crue.validation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.DonCLimMScenario;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.OrdCalcScenario;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;

/**
 * Valide que les calcul trouve dans les fichiers de résultats sont tous actifs.
 *
 * @author deniger
 */
public class ValidatorResultatCrue10CalculInactif {

  private final EMHScenario scenario;

  public ValidatorResultatCrue10CalculInactif(EMHScenario scenario) {
    this.scenario = scenario;
  }

  public CtuluLog validCalculActif(List<ResultatTimeKey> keys) {
    OrdCalcScenario ordCalcScenario = scenario.getOrdCalcScenario();
    DonCLimMScenario donCLimMScenario = scenario.getDonCLimMScenario();
    List<Calc> calcs = donCLimMScenario.getCalc();
    Map<String, Calc> calcByNom = new HashMap<>();
    for (Calc calc : calcs) {
      calcByNom.put(calc.getNom(), calc);
    }

    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("res.validate.CalcActif");
    for (ResultatTimeKey resultatKey : keys) {
      Calc calc = calcByNom.get(resultatKey.getNomCalcul());
      if (calc == null) {
        log.addSevereError("res.validate.CalcUnknown.error", resultatKey.getNomCalcul());
      } else if (!ordCalcScenario.isUsed(calc)) {
        log.addError("res.validate.CalcNotInOcal.error", resultatKey.getNomCalcul());
      }
    }

    return log;

  }
}
