package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;

import java.util.*;

/**
 * Le validation des id.
 * 
 * @author deniger
 */
public class ValidatorForIDCrue9 extends AbstractCrueValidator {
  boolean isCrue9Compatible = true;

  private final CtuluLog res = new CtuluLog();

  private final List<CtuluLog> lst = Collections.singletonList(res);
  private final Map<String, Object> used = new HashMap<>();

  public ValidatorForIDCrue9() {
    super();
    res.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    res.setDesc("validation.noms.crue9");
  }

  @Override
  public List<CtuluLog> getLogs() {
    return lst;
  }

  /**
   * @return the res
   */
  public CtuluLog getRes() {
    return res;
  }

  /**
   * @return the isCrue9Compatible
   */
  public boolean isCrue9Compatible() {
    return isCrue9Compatible;
  }

  /**
   * @param o l'objet a tester
   * @return true si ecrit dans les fichier crue 9
   */
  private boolean isWrittenByCrue9(final Object o) {
    return (o instanceof LitNomme) || (o instanceof CatEMHNoeud) || (o instanceof CatEMHBranche)
        || (o instanceof CatEMHSection);

  }

  public void validate(final Object o) {
    // noeud,strickler,litNomme
    if (o instanceof ObjetWithID && !(o instanceof RelationEMH)) {
      final String id = ((ObjetWithID) o).getId();
      final String nom = ((ObjetWithID) o).getNom();
      final int length = id.length();
      if (length > CruePrefix.NB_CAR_MAX_CRUE9_CASIER_PROFIL && o instanceof EMHCasierProfil) {
        isCrue9Compatible = false;
        res.addWarn("valid.nomEMHCasierProfil.tooLong.crue9", nom);
      } else if (length > CruePrefix.NB_CAR_MAX_CRUE9 && isWrittenByCrue9(o)) {
        isCrue9Compatible = false;
        res.addWarn("valid.nom.tooLong.crue9", nom);

      }

      final Object usedObject = used.get(id);
      if (usedObject == null) {
        used.put(id, o);
      } else if (!usedObject.equals(o)) {
        res.addSevereError("valid.nom.notUnique", nom);
      }

    }

  }

  @Override
  public void validate(final Set<Object> o, final EMH emh, final EMHModeleBase modeleContenant) {
    for (final Object object : o) {
      validate(object);
    }

  }

  public static ValidatorForIDCrue9 validateNomsForCrue9(final EMHScenario scenario) {
    final ValidatorForIDCrue9 validator = new ValidatorForIDCrue9();
    ValidateEMHTreeWalker walker = new ValidateEMHTreeWalker();
    walker.doValidate(scenario.getAllSimpleEMH(), null, validator);
    return validator;
  }
}
