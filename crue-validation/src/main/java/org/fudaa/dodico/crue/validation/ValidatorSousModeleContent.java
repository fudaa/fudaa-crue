package org.fudaa.dodico.crue.validation;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author deniger
 */
public class ValidatorSousModeleContent implements CrueValidator {
    @Override
    public List<CtuluLog> getLogs() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void validCasier(CatEMHCasier casier,
                            Set<String> nodesId, CtuluLog error) {
        CatEMHNoeud noeud = casier.getNoeud();
        if (noeud != null && !nodesId.contains(noeud.getId())) {
            error.addSevereError("validation.sousModeleContent.NoeudForCasierNotContained", casier.getNom(), noeud.getNom());
        }
    }

    public void validSection(CatEMHSection section,
                             Set<String> branchesId, CtuluLog error) {
        CatEMHBranche branche = section.getBranche();
        if (branche != null && !branchesId.contains(branche.getId())) {
            error.addSevereError("validation.sousModeleContent.BrancheForSectionNotContained", section.getNom(),
                    branche.getNom());
        }
        List<EMHSectionIdem> sectionIdems = EMHHelper.getSectionReferencing(section);
        if (CollectionUtils.isNotEmpty(sectionIdems)) {
            String sectionSousModeleId = section.getParent().getId();
            for (EMHSectionIdem emhSectionIdem : sectionIdems) {
                String sectionReferencingSousModeleId = emhSectionIdem.getParent().getId();
                if (!StringUtils.equals(sectionSousModeleId, sectionReferencingSousModeleId)) {
                    error.addSevereError("validation.sousModeleContent.SectionIdemNotContained", section.getNom(),
                            emhSectionIdem.getNom());
                }
            }
        }
    }

    @Override
    public void validate(Set<Object> childs, EMH emh, EMHModeleBase modeleBase) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void validateBranche(CatEMHBranche branche,
                                Set<String> nodesId, CtuluLog error) {
        CatEMHNoeud noeudAmont = branche.getNoeudAmont();
        String noeudAmontId = noeudAmont.getId();
        if (!nodesId.contains(noeudAmontId)) {
            error.addSevereError("validation.sousModeleContent.NoeudAmontNotContained", branche.getNom(), noeudAmont.getNom());
        }
        CatEMHNoeud noeudAval = branche.getNoeudAval();
        String noeudAvalId = noeudAval.getId();
        if (!nodesId.contains(noeudAvalId)) {
            error.addSevereError("validation.sousModeleContent.NoeudAvalNotContained", branche.getNom(), noeudAval.getNom());
        }
        String sousModeleId = branche.getParent().getId();

        CatEMHSection sectionPilote = EMHHelper.getSectionPilote(branche);
        if (sectionPilote != null) {
            String sectionSousModeleId = sectionPilote.getParent().getId();
            if (!StringUtils.equals(sousModeleId, sectionSousModeleId)) {
                error.addSevereError("validation.sousModeleContent.sectionPiloteNotContained", branche.getNom(),
                        sectionPilote.getNom());
            }
        }

        List<RelationEMHSectionDansBranche> sections = branche.getSections();
        for (RelationEMHSectionDansBranche relationEMHSectionDansBranche : sections) {
            final CatEMHSection emh = relationEMHSectionDansBranche.getEmh();
            String sectionSousModeleId = emh.getParent().getId();
            if (!StringUtils.equals(sousModeleId, sectionSousModeleId)) {
                error.addSevereError("validation.sousModeleContent.sectionNotContained", branche.getNom(),
                        relationEMHSectionDansBranche.getEmh().getNom());
            }
        }
    }

    @Override
    public List<CtuluLog> validateScenario(EMHScenario scenario) {
        List<CtuluLog> res = new ArrayList<>();
        List<EMHSousModele> sousModeles = scenario.getSousModeles();
        for (EMHSousModele sousModele : sousModeles) {
            CtuluLog log = validateSousModele(sousModele);
            if (log != null) {
                res.add(log);
            }
        }
        return res;
    }

    private CtuluLog validateSousModele(EMHSousModele sousModele) {
        CtuluLog error = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
        error.setDesc("validation.sousModeleContent");
        error.setDescriptionArgs(sousModele.getNom());
        final List<CatEMHNoeud> noeuds = sousModele.getNoeuds();
        Set<String> nodesId = TransformerHelper.toSetOfId(noeuds);
        List<CatEMHBranche> branches = sousModele.getBranches();
        for (CatEMHBranche branche : branches) {
            validateBranche(branche, nodesId, error);
        }
        List<CatEMHCasier> casiers = sousModele.getCasiers();
        for (CatEMHCasier casier : casiers) {
            validCasier(casier, nodesId, error);
        }
        List<CatEMHSection> sections = sousModele.getSections();
        Set<String> branchesId = TransformerHelper.toSetOfId(branches);
        for (CatEMHSection section : sections) {
            validSection(section, branchesId, error);
        }
        for (CatEMHNoeud noeud : noeuds) {
            Set<String> parentIds = TransformerHelper.toSetOfId(noeud.getParents());
            List<CatEMHBranche> branchesOfNode = noeud.getBranches();
            for (CatEMHBranche branche : branchesOfNode) {
                if (!parentIds.contains(branche.getParent().getId())) {
                    error.addSevereError("validation.sousModeleContent.BrancheForNodeNotContained", noeud.getNom(), branche.getNom());
                }
            }
            CatEMHCasier casier = noeud.getCasier();
            if (casier != null && !parentIds.contains(casier.getParent().getId())) {
                error.addSevereError("validation.sousModeleContent.CasierForNodeNotContained", noeud.getNom(), casier.getNom());
            }
        }

        if (error.isNotEmpty()) {
            return error;
        }
        return null;
    }
}
