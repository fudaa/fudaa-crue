package org.fudaa.dodico.crue.validation.util;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

/**
 * Un validateur spécifique pour les noeuds.
 *
 * @author deniger
 */
public class DelegateValidatorNoeud {
  public static void validateNoeud(EMH emh, EMHModeleBase modeleBase, CtuluLog log) {
    //on valide avec DPTI
    OrdPrtCIniModeleBase selectInfoEMH = (OrdPrtCIniModeleBase) EMHHelper.selectInfoEMH(modeleBase,
      EnumInfosEMH.ORD_PRT_CINI_MODELE_BASE);

    if (selectInfoEMH != null && selectInfoEMH.getMethodeInterpol() != null && selectInfoEMH.getMethodeInterpol().isNoeudActiveRequireDPTI()) {
      InfosEMH dpti = EMHHelper.selectInfoEMH(emh, EnumInfosEMH.DON_PRT_CINI);
      if (dpti == null) {
        log.addSevereError("valid.noeud.mustContainDPTI", emh.getNom(), selectInfoEMH.getMethodeInterpol().name());
      }
    }
  }
}
