package org.fudaa.dodico.crue.validation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.io.res.ResCatEMHContent;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.factory.PredicateFactory;

/**
 * Valide que seules des EMH actives disposent de résultat.s
 *
 * @author deniger
 */
public class ValidatorResultatCrue10EMHActive {

  public CtuluLog validActiveEMHPresentInRes(List<ResCatEMHContent> caCcontent, EMHModeleBase modele,
                                             CtuluLogLevel levelForDefinedButInactiveEMH) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("res.validate.EMHDeclaration");
    ArrayList<EMH> activateEMH = CollectionCrueUtil.select(modele.getAllSimpleEMH(),
                                                           PredicateFactory.PREDICATE_EMH_ACTUALLY_ACTIVE_FOR_CALCUL);
    Collection<String> refEMHActive = new HashSet<>(TransformerHelper.toNom(activateEMH));
    Set<String> refEMHDefinedInRes = new HashSet<>();
    for (ResCatEMHContent cat : caCcontent) {
      cat.getAllRef(refEMHDefinedInRes);
    }

    Collection<String> activeEMHNotPresentInRes = CollectionUtils.subtract(refEMHActive, refEMHDefinedInRes);
    if (!activeEMHNotPresentInRes.isEmpty()) {
      List<String> sorted = CollectionCrueUtil.getSortedList(activeEMHNotPresentInRes);
      for (String emhRef : sorted) {
        log.addError("res.validate.activeEMHNotPresentInRes", emhRef);
      }
    }
    return log;

  }

  public CtuluLog validAllEMHPresentInRes(List<ResCatEMHContent> caCcontent, EMHModeleBase modele,
                                          CtuluLogLevel levelForDefinedButInactiveEMH) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("res.validate.EMHDeclaration");
    List<EMH> allEMH = modele.getAllSimpleEMH();
    Collection<String> refEMHActive = new HashSet<>(TransformerHelper.toNom(allEMH));
    Set<String> refEMHDefinedInRes = new HashSet<>();
    for (ResCatEMHContent cat : caCcontent) {
      cat.getAllRef(refEMHDefinedInRes);
    }

    Collection<String> activeEMHNotPresentInRes = CollectionUtils.subtract(refEMHActive, refEMHDefinedInRes);
    if (!activeEMHNotPresentInRes.isEmpty()) {
      List<String> sorted = CollectionCrueUtil.getSortedList(activeEMHNotPresentInRes);
      for (String emhRef : sorted) {
        log.addError("res.validate.EMHNotPresentInRes", emhRef);
      }
    }
    return log;

  }
}
