package org.fudaa.dodico.crue.validation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHConteneur;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;

/**
 * Algorithmes de validations du modele.
 *
 * @author Adrien Hadoux
 */
public final class ValidateConnectionModele {

  private ValidateConnectionModele() {
  }

  /**
   * Methode qui teste la connexité du modele et retourne true ou false,
   *
   * @param data le conteneur surlequel la validation porte
   * @param analyzer analyzer
   * @return true si reseau valide.
   */
  public static CtuluLog validateConnexite(final CatEMHConteneur data) {
    final CtuluLog analyzer = new CtuluLog();
    analyzer.setDesc("validation.connexite");
    analyzer.setDescriptionArgs(data.getNom());
    analyzer.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    boolean validate = false;

    // -- premeire verification: tous les noeuds sont affectées --//
    validate = isNoeudsTousAffectes(data, analyzer);

    if (validate) {

      // -- on verifie que toutes les branches ont un noeud --//.
      validate = isBranchesConnexes(data, analyzer);
    }

    return analyzer;
  }

  /**
   * Verifie que toutes les branches soient affectees.
   *
   * @param catEMHConteneur le conteneur
   * @param ctuluLog les logs
   * @return true si le graphe est connexe.
   */
  private static boolean isBranchesConnexes(final CatEMHConteneur catEMHConteneur, final CtuluLog ctuluLog) {

    final List<CatEMHBranche> branches = catEMHConteneur.getBranches();
    final List<CatEMHBranche> lstBranchesRestantes = remplirBranchesRestantesActives(branches);

    // 1) Choisir un nœud de départ connecté à au moins une branche active. Il est stocké dans lstNoeudsATester.
    final CatEMHNoeud findNoeudDep = findNoeudDepart(branches);

    if (findNoeudDep == null) {
      return false;
    }
    final List<CatEMHNoeud> lstNoeudsATester = new ArrayList<>(5);// on pourrait mettre 1
    lstNoeudsATester.add(findNoeudDep);

    // 2) Tant qu’il existe au moins un élément dans lstNoeudsATester :
    while (lstNoeudsATester.size() > 0) {
      // 2.1) Dépiler un élément de lstNoeudsATester (le supprimer de la liste) et le stocker dans NoeudCourant.
      final CatEMHNoeud noeudCourant = lstNoeudsATester.remove(0);

      // 2.2) Tant qu’il existe une branche connectée à NoeudCourant et qui figure dans lstBranchesRestantes :
      final List<CatEMHBranche> lstBranchesContenantNoeud = findAllBranchesConnectedToNode(lstBranchesRestantes,
              noeudCourant);

      while (lstBranchesContenantNoeud.size() > 0) {
        // 2.2.1) Récupérer une telle branche et la stocker dans BrancheCourante.
        final CatEMHBranche brancheCourante = lstBranchesContenantNoeud.remove(0);

        // 2.2.2) Récupérer le nœud situé à l’autre extrémité de BrancheCourante (par rapport à NoeudCourant) et le
        // stocker
        // dans lstNoeudsATester (s’il n’y figure pas déjà).
        CatEMHNoeud nouveauNoeud = brancheCourante.getNoeudAmont();
        if (nouveauNoeud != noeudCourant) {
          lstNoeudsATester.add(nouveauNoeud);
        } else {
          nouveauNoeud = brancheCourante.getNoeudAval();
          lstNoeudsATester.add(nouveauNoeud);
        }

        // 2.2.3) Supprimer BrancheCourante de lstBranchesRestantes.
        lstBranchesRestantes.remove(brancheCourante);

      }

    }

    if (lstBranchesRestantes.size() > 0) {
      ctuluLog.addError("error.validationModele.branchesNonAffectes",
              createMapBranchesNonAffectes(lstBranchesRestantes));
      return false;
    }

    return true;
  }

  /**
   * Cree la liste de branches Active a partir de la liste de branches totales.
   *
   * @param lstBranchesTotales les branches
   * @return liste des branches active.
   */
  private static List<CatEMHBranche> remplirBranchesRestantesActives(final List<CatEMHBranche> lstBranchesTotales) {
    final List<CatEMHBranche> listeBranches = new ArrayList<>();

    for (final CatEMHBranche branche : lstBranchesTotales) {
      if (branche.getActuallyActive()) {
        listeBranches.add(branche);
      }
    }
    return listeBranches;

  }

  /**
   * Retrouve toutes les branches connectees au noeud.
   *
   * @param lstBranchesRestantes les branches restantes
   * @param noeud le noeud
   * @return liste de branches
   */
  private static List<CatEMHBranche> findAllBranchesConnectedToNode(final List<CatEMHBranche> lstBranchesRestantes,
                                                                    final CatEMHNoeud noeud) {
    final List<CatEMHBranche> listeConnected = new ArrayList<>();

    for (final CatEMHBranche branche : lstBranchesRestantes) {
      if (branche.getUserActive()) {
        final CatEMHNoeud noeudAmont = branche.getNoeudAmont();
        if (noeudAmont == noeud) {

          listeConnected.add(branche);

        } else {
          final CatEMHNoeud noeudAval = branche.getNoeudAval();
          if (noeudAval == noeud) {

            listeConnected.add(branche);

          }
        }
      }
    }

    return listeConnected;
  }

  /**
   * Methode qui recherche dans la liste des branches le premier noeud d'une branche active.
   *
   * @param catEMHBranches les branches
   * @return le noeud de départ
   */
  private static CatEMHNoeud findNoeudDepart(final List<CatEMHBranche> catEMHBranches) {
    for (final CatEMHBranche catEMHBranche : catEMHBranches) {
      if (catEMHBranche.getUserActive()) {
        CatEMHNoeud findNoeudDep = catEMHBranche.getNoeudAmont();
        if (findNoeudDep != null) {
          return findNoeudDep;
        }
        findNoeudDep = catEMHBranche.getNoeudAval();
        if (findNoeudDep != null) {
          return findNoeudDep;
        }
      }
    }
    return null;
  }

  /**
   *
   * @param emhConteneur le conteneur d'EMH
   * @param ctuluLog les logs
   * @return true si tous les noeuds soient affectés a au moins une branche.
   */
  private static boolean isNoeudsTousAffectes(final CatEMHConteneur emhConteneur, final CtuluLog ctuluLog) {

    final List<CatEMHNoeud> noeuds = emhConteneur.getNoeuds();
    final List<CatEMHNoeud> listeNoeudsNonAffectes = new ArrayList<>(noeuds.size());

    for (final CatEMHNoeud node : emhConteneur.getNoeuds()) {
      if (node.getActuallyActive()) {
        listeNoeudsNonAffectes.add(node);
      }
    }
    boolean isBrancheComplete = true;
    for (final CatEMHBranche branche : emhConteneur.getBranches()) {
      if (branche.getUserActive()) {
        final CatEMHNoeud noeudAmont = branche.getNoeudAmont();
        if (noeudAmont != null) {

          listeNoeudsNonAffectes.remove(noeudAmont);

        } else {
          ctuluLog.addSevereError("error.validationModele.noeudAmontManquant", branche.getNom());
          isBrancheComplete = false;
        }
        final CatEMHNoeud noeudAval = branche.getNoeudAval();
        if (noeudAval != null) {

          listeNoeudsNonAffectes.remove(noeudAval);

        } else {
          ctuluLog.addSevereError("error.validationModele.noeudAvalManquant", branche.getNom());
          isBrancheComplete = false;
        }
        if (noeudAval != null && noeudAmont != null && noeudAmont.getId().equals(noeudAval.getId())) {
          ctuluLog.addSevereError("error.validationModele.noeudAvalSameAsNoeudAmont", branche.getNom());
        }
      }
    }
    final boolean isNoeudToutAffecte = listeNoeudsNonAffectes.isEmpty();
    if (!isNoeudToutAffecte) {
      ctuluLog.addError("error.validationModele.noeudNonAffectes", createMapNoeudsNonAffectes(listeNoeudsNonAffectes));
    }

    return isNoeudToutAffecte && isBrancheComplete;
  }

  /**
   * Creer la map avec la liste des noeuds non affectes.
   *
   * @param listeNoeudsNonAffectes les noeuds non affectés
   * @return  chaine de caracteres avec les noms separes par des ,
   */
  private static String createMapNoeudsNonAffectes(final List<CatEMHNoeud> listeNoeudsNonAffectes) {

    StringBuilder liste = new StringBuilder();
    boolean first = true;
    for (final CatEMHNoeud noeud : listeNoeudsNonAffectes) {
      if (!first) {
        liste.append(", ");
      }
      liste.append(noeud.getNom());
      first = false;
    }
    return liste.toString();

  }

  /**
   * Creer la map avec la liste des branches non affectes.
   *
   * @param listeBranchesNonAffectes les branches non affectés
   * @return map...
   */
  private static Map<String, Object> createMapBranchesNonAffectes(final List<CatEMHBranche> listeBranchesNonAffectes) {
    final String key = BusinessMessages.getString("prefix.branches");

    StringBuilder liste = new StringBuilder();

    for (final CatEMHBranche branche : listeBranchesNonAffectes) {
      liste.append(' ').append(branche.getNom());
    }
    final HashMap<String, Object> map = new HashMap<>();
    map.put(key, liste.toString());
    return map;

  }
}
