package org.fudaa.dodico.crue.validation;

import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;

/**
 * @author deniger
 */
public abstract class AbstractCrueValidator implements CrueValidator {

  @Override
  public List<CtuluLog> validateScenario(EMHScenario scenario) {
    ValidateEMHTreeWalker walker = new ValidateEMHTreeWalker();
    final List<CtuluLog> validateValues = walker.validateScenario(scenario, this);
    if (CollectionUtils.isEmpty(validateValues)) { return null; }
    return validateValues;
  }

}
