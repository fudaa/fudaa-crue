package org.fudaa.dodico.crue.validation;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.AllPredicate;
import org.apache.commons.collections4.functors.NotPredicate;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSectionSansGeometrie;
import org.fudaa.dodico.crue.metier.emh.ResPrtGeo;
import org.fudaa.dodico.crue.metier.factory.PredicateFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

/**
 * Utiliser pour valider les résultats de pre-traitement.
 * 
 * @author deniger
 */
public class ValidatorResultatPretraitementMinimum {

  final CtuluLogLevel levelForError = CtuluLogLevel.SEVERE;

  /**
   * Valide sur les resultats Casier et Section
   * 
   * @param scenario le scenario
   * @param log le log a completer
   * @return le log
   */
  @SuppressWarnings("unchecked")
  public CtuluLog validateResultat(final EMHScenario scenario, final CtuluLog log) {
    final Set<Class> classEMHWithResultat = new HashSet<>();
    classEMHWithResultat.add(CatEMHCasier.class);
    classEMHWithResultat.add(CatEMHSection.class);
    Predicate not = new NotPredicate(new PredicateFactory.PredicateClass(EMHSectionSansGeometrie.class));
    for (final Class emhClass : classEMHWithResultat) {
      final Predicate predicateClass = new PredicateFactory.PredicateInstanceOf(emhClass);
      final List<EMH> emhWithResult = CollectionCrueUtil.select(scenario.getAllSimpleEMH(), new AllPredicate(new Predicate[] {
          not, predicateClass, PredicateFactory.PREDICATE_EMH_ACTUALLY_ACTIVE }));
      for (final EMH emh : emhWithResult) {
        // on teste uniquement les emh activee
        if (!EMHHelper.containsInstanceOf(emh.getInfosEMH(), ResPrtGeo.class)) {
          log.addRecord(levelForError, "res.noResultatPretraitementForEMH", emh.getNom());
        }

      }
    }
    return log;
  }

}
