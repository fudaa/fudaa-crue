/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

import java.util.List;

/**
 * Parcourt toutes les EMHS d'un scenario pour valider les conditions initiales.
 *
 * @author Frédéric Deniger
 */
public class ValidateInitialConditions {
  public static DonPrtCIniBranche createDefaultDonPrtCiniBranche(EnumBrancheType type, CrueConfigMetier ccm) {
    if (EnumBrancheType.EMHBrancheOrifice.equals(type)) {
      return new DonPrtCIniBrancheOrifice(ccm);
    }
    if (EnumBrancheType.EMHBrancheSaintVenant.equals(type)) {
      return new DonPrtCIniBrancheSaintVenant(ccm);
    }
    return new DonPrtCIniBranche(ccm);
  }

  /**
   * @param emh
   * @return la class a utiliser pour les conditions initiales
   */
  public static Class getDptiClass(EMH emh) {
    EnumCatEMH catType = emh.getCatType();
    switch (catType) {
      case SECTION:
        return DonPrtCIniSection.class;
      case NOEUD:
        return DonPrtCIniNoeudNiveauContinu.class;
      case CASIER:
        return DonPrtCIniCasierProfil.class;
      case BRANCHE:
        return getDptiClassForBranche((CatEMHBranche) emh);
    }
    throw new IllegalAccessError("EMH not supported " + emh);
  }

  private static InfosEMH createDefaultDonPrtCiniSection(EnumSectionType type, CrueConfigMetier ccm) {
    return new DonPrtCIniSection(ccm);
  }

  private static InfosEMH createDefaultDonPrtCiniSection(EnumCasierType type, CrueConfigMetier ccm) {
    return new DonPrtCIniCasierProfil(ccm);
  }

  private static InfosEMH createDefaultDonPrtCiniNoeud(EnumNoeudType type, CrueConfigMetier ccm) {
    return new DonPrtCIniNoeudNiveauContinu(ccm);
  }

  public static DonPrtCIni getCini(EMH emh) {
    return EMHHelper.selectFirstInstanceOf(emh.getInfosEMH(), DonPrtCIni.class);
  }

  private static Class getDptiClassForBranche(CatEMHBranche in) {
    if (EnumBrancheType.EMHBrancheOrifice.equals(in.getBrancheType())) {
      return DonPrtCIniBrancheOrifice.class;
    }
    if (EnumBrancheType.EMHBrancheSaintVenant.equals(in.getBrancheType())) {
      return DonPrtCIniBrancheSaintVenant.class;
    }
    return DonPrtCIniBranche.class;
  }

  public CtuluLog propagateChange(EMHModeleBase modele, CrueConfigMetier ccm) {
    CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    OrdPrtCIniModeleBase ordPrtCIniModeleBase = modele.getOrdPrtCIniModeleBase();
    EnumMethodeInterpol methodeInterpol = ordPrtCIniModeleBase.getMethodeInterpol();
    final EMHSousModele concatSousModele = EMHHelper.concatInNotSorted(null, modele.getSousModeles());
    computeBranches(methodeInterpol, concatSousModele, ccm, res);
    computeNoeuds(methodeInterpol, concatSousModele, ccm, res);
    computeSections(methodeInterpol, concatSousModele, ccm, res);
    computeCasiers(methodeInterpol, concatSousModele, ccm, res);

    return res;
  }

  private void computeBranches(EnumMethodeInterpol methodeInterpol, final EMHSousModele concatSousModele, CrueConfigMetier ccm, CtuluLog res) {
    boolean dptiRequired = methodeInterpol.isBrancheActiveRequireDPTI();
    //on le fait pour toutes les branches... a voir
    List<CatEMHBranche> branches = concatSousModele.getBranches();
    for (CatEMHBranche branche : branches) {
      computeBranche(branche, dptiRequired, ccm, res);
    }
  }

  private void computeBranche(CatEMHBranche branche, boolean dptiRequired, CrueConfigMetier ccm, CtuluLog res) {
    DonPrtCIni cini = getCini(branche);
    if (dptiRequired) {
      if (cini == null) {
        branche.addInfosEMH(createDefaultDonPrtCiniBranche(branche.getBrancheType(), ccm));
        addCiniAddedLog(branche, res);
      }
    } else {
      if (cini != null) {
        branche.removeInfosEMH(cini);
        addCiniRemovedLog(branche, res);
      }
    }
  }

  private void addCiniRemovedLog(EMH emh, CtuluLog res) {
    if (res != null) {
      res.addInfo("donPrtCini.remove", emh.getNom());
    }
  }

  private void addCiniAddedLog(EMH emh, CtuluLog res) {
    if (res != null) {
      res.addInfo("donPrtCini.added", emh.getNom());
    }
  }

  private DonPrtCIni getFirst(List<CatEMHNoeud> noeuds) {
    for (CatEMHNoeud noeud : noeuds) {
      DonPrtCIni cini = getCini(noeud);
      if (cini != null) {
        return cini;
      }
    }
    return null;
  }

  private void computeNoeuds(EnumMethodeInterpol methodeInterpol, final EMHSousModele concatSousModele, CrueConfigMetier ccm, CtuluLog res) {
    boolean dptiRequired = methodeInterpol.isNoeudActiveRequireDPTI();
    List<CatEMHNoeud> noeuds = concatSousModele.getNoeuds();
    DonPrtCIni first = null;
    if (EnumMethodeInterpol.BAIGNOIRE.equals(methodeInterpol)) {
      first = getFirst(noeuds);
    }
    for (CatEMHNoeud noeud : noeuds) {
      computeNoeud(noeud, dptiRequired, ccm, res);
    }
    //on met tous les zini à la même valeur
    if (EnumMethodeInterpol.BAIGNOIRE.equals(methodeInterpol)) {
      if (!noeuds.isEmpty()) {
        if (first == null) {
          first = getFirst(noeuds);
        }
        if (first != null) {
          DonPrtCIniNoeudNiveauContinu val = (DonPrtCIniNoeudNiveauContinu) first;
          for (CatEMHNoeud noeud : noeuds) {
            DonPrtCIni dpti = getCini(noeud);
            if (dpti != null) {
              DonPrtCIniNoeudNiveauContinu toChange = (DonPrtCIniNoeudNiveauContinu) dpti;
              if (!ccm.getProperty(CrueConfigMetierConstants.PROP_ZINI).getEpsilon().isSame(val.getZini(), toChange.getZini())) {
                res.addInfo("donPrtCini.ziniChanged", noeud.getNom());
                toChange.setZini(val.getZini());
              }
            }
          }
        }
      }
    }
  }

  private void computeCasier(CatEMHCasier casier, final boolean dptiRequired, CrueConfigMetier ccm, CtuluLog res) {
    DonPrtCIni cini = getCini(casier);
    if (dptiRequired) {
      if (cini == null) {
        casier.addInfosEMH(createDefaultDonPrtCiniSection(casier.getCasierType(), ccm));
        addCiniAddedLog(casier, res);
      }
    } else {
      if (cini != null) {
        casier.removeInfosEMH(cini);
        addCiniRemovedLog(casier, res);
      }
    }
  }

  private void computeNoeud(CatEMHNoeud noeud, boolean dptiRequired, CrueConfigMetier ccm, CtuluLog res) {
    DonPrtCIni cini = getCini(noeud);
    if (dptiRequired) {
      if (cini == null) {
        noeud.addInfosEMH(createDefaultDonPrtCiniNoeud(noeud.getNoeudType(), ccm));
        addCiniAddedLog(noeud, res);
      }
    } else {
      if (cini != null) {
        noeud.removeInfosEMH(cini);
        addCiniRemovedLog(noeud, res);
      }
    }
  }

  private void computeSections(EnumMethodeInterpol methodeInterpol, final EMHSousModele concatSousModele, CrueConfigMetier ccm, CtuluLog res) {
    final boolean dptiRequired = methodeInterpol.isSectionActiveRequireDPTI();
    //on le fait pour toutes les branches... a voir
    List<CatEMHSection> sections = concatSousModele.getSections();
    for (CatEMHSection section : sections) {
      computeSection(section, dptiRequired, ccm, res);
    }
  }

  private void computeSection(CatEMHSection section, final boolean dptiRequired, CrueConfigMetier ccm, CtuluLog res) {
    DonPrtCIni cini = getCini(section);
    if (dptiRequired) {
      if (cini == null) {
        section.addInfosEMH(createDefaultDonPrtCiniSection(section.getSectionType(), ccm));
        addCiniAddedLog(section, res);
      }
    } else {
      if (cini != null) {
        section.removeInfosEMH(cini);
        addCiniRemovedLog(section, res);
      }
    }
  }

  private void computeCasiers(EnumMethodeInterpol methodeInterpol, final EMHSousModele concatSousModele, CrueConfigMetier ccm, CtuluLog res) {
    final boolean dptiRequired = methodeInterpol.isCasierActiveRequireDPTI();
    //on le fait pour toutes les branches... a voir
    List<CatEMHCasier> casiers = concatSousModele.getCasiers();
    for (CatEMHCasier casier : casiers) {
      computeCasier(casier, dptiRequired, ccm, res);
    }
  }
}
