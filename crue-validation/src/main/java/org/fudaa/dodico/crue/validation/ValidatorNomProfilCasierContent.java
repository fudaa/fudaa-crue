package org.fudaa.dodico.crue.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

/**
 *
 * @author deniger
 */
public class ValidatorNomProfilCasierContent implements CrueValidator {

  @Override
  public List<CtuluLog> getLogs() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  public void validCasier(CatEMHCasier casier, CtuluLog error) {
    List<DonPrtGeoProfilCasier> profils = EMHHelper.selectClass(casier.getInfosEMH(), DonPrtGeoProfilCasier.class);
    if (profils != null) {
      String expectedPrefix = createNomProfilCasierPrefix(casier);
      for (DonPrtGeoProfilCasier profil : profils) {
        String nom = profil.getNom();
        CtuluLogRecord record = validNomProfilCasier(nom, expectedPrefix, casier);
        if (record != null) {
          error.addRecord(record);
        }

      }
    }
    DonPrtGeoBatiCasier batiCasier = EMHHelper.selectFirstOfClass(casier.getInfosEMH(), DonPrtGeoBatiCasier.class);
    if (batiCasier != null) {
      String expectedName = CruePrefix.changePrefix(casier.getNom(), CruePrefix.P_CASIER, CruePrefix.P_BATI_CASIER);
      if (!batiCasier.getNom().equals(expectedName)) {
        error.addSevereError("validation.batiCasierCorrespondance.CasietNameNotCorresponding", batiCasier.getNom(), casier.getNom(), expectedName);
      }
    }
  }

  @Override
  public void validate(Set<Object> childs, EMH emh, EMHModeleBase modeleBase) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public List<CtuluLog> validateScenario(EMHScenario scenario) {
    List<CtuluLog> res = new ArrayList<>();
    List<EMHSousModele> sousModeles = scenario.getSousModeles();
    for (EMHSousModele sousModele : sousModeles) {
      CtuluLog log = validateSousModele(sousModele);
      if (log != null) {
        res.add(log);
      }
    }
    return res;
  }

  private CtuluLog validateSousModele(EMHSousModele sousModele) {
    CtuluLog error = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    error.setDesc("validation.casierProfilCorrespondance");
    error.setDescriptionArgs(sousModele.getNom());
    List<CatEMHCasier> casiers = sousModele.getCasiers();
    for (CatEMHCasier casier : casiers) {
      validCasier(casier, error);
    }

    if (error.isNotEmpty()) {
      return error;
    }
    return null;

  }

  /**
   *
   * @param nomProfilCasier nom du casier
   * @param casier le casier
   * @return un record si le nom du profil ne suit pas la règle de nommage
   */
  public static CtuluLogRecord validNomProfilCasier(String nomProfilCasier, CatEMHCasier casier) {
    return validNomProfilCasier(nomProfilCasier, createNomProfilCasierPrefix(casier), casier);

  }

  private static CtuluLogRecord validNomProfilCasier(String nom, String expectedPrefix, CatEMHCasier casier) {
    CtuluLogRecord record = null;
    if (!nom.startsWith(expectedPrefix)) {
      record = new CtuluLogRecord(CtuluLogLevel.SEVERE, "validation.casierProfilCorrespondance.CasietNameNotCorresponding");
      record.setArgs(new Object[]{nom, casier.getNom(), expectedPrefix + "XXX"});
    } else {
      String suffix = StringUtils.substringAfter(nom, expectedPrefix);
      if (suffix.length() != 3 || !StringUtils.isNumeric(suffix)) {
        record = new CtuluLogRecord(CtuluLogLevel.SEVERE, "validation.casierProfilCorrespondance.CasietNameNotCorresponding");
        record.setArgs(new Object[]{nom, casier.getNom(), expectedPrefix + "XXX"});
      }
    }
    return record;
  }

  private static String createNomProfilCasierPrefix(CatEMHCasier casier) {
    return CruePrefix.changePrefix(casier.getNom(), CruePrefix.P_CASIER, CruePrefix.P_PROFIL_CASIER) + "_";
  }
}
