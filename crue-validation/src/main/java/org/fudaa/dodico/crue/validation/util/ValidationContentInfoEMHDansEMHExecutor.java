package org.fudaa.dodico.crue.validation.util;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Utiliser pour tester les CatEMHSection utilisees dans une branche.
 *
 * @author deniger
 */
public class ValidationContentInfoEMHDansEMHExecutor extends ValidationContentExecutor<Class<? extends InfosEMH>> {
  /**
   *
   */
  public ValidationContentInfoEMHDansEMHExecutor() {
    super(createInfosEMHDansEMHContents());
    setMsgIfCantContain("validation.emh.cantContainsThisInfoEMH");
  }

  /**
   * L'EMH peut contenir des infos de type aClass
   *
   * @param aClass la classe a tester
   * @return resultat de validation
   */
  private static ValidationContentParamDelegate<Class<? extends InfosEMH>> canContain(final Class<? extends InfosEMH> aClass) {
    return create(aClass, -1, -1);
  }

  private static ValidationContentParamDelegate<Class<? extends InfosEMH>> canContainOne(final Class<? extends InfosEMH> c) {
    return create(c, 0, 1);
  }

  private static ValidationContentParamDelegate<Class<? extends InfosEMH>> canContainsBetween(
      final Class<? extends InfosEMH> c, int min, int max) {
    return create(c, min, max);
  }

  private static ValidationContentParamDelegate<Class<? extends InfosEMH>> create(
      Class<? extends InfosEMH> classOfSection, int minOccurence, int maxOccurence) {
    ValidationContentParamDelegate<Class<? extends InfosEMH>> res = new ValidationContentParamDelegate<>(
        classOfSection, minOccurence, maxOccurence);
    res.setMsgNbOccurenceNotEqualsTo("validation.emh.nbInfoEMHNotEqualsTo");
    res.setMsgNbOccurenceNotEqualsToOne("validation.emh.nbInfoEMHNotEqualsToOne");
    res.setMsgNbOccurenceNotIn("validation.emh.nbInfoEMHNotIn");
    return res;
  }

  private static List<ValidationContentParamDelegate<Class<? extends InfosEMH>>> createList(
      final ValidationContentParamDelegate<Class<? extends InfosEMH>>... params) {
    return Arrays.asList(params);
  }

  @SuppressWarnings("unchecked")
  private static Map<Class<? extends EMH>, List<ValidationContentParamDelegate<Class<? extends InfosEMH>>>> createInfosEMHDansEMHContents() {
    final Map<Class<? extends EMH>, List<ValidationContentParamDelegate<Class<? extends InfosEMH>>>> res = new HashMap<>();
    // on enregistre pour chaque classe d'EMH, les types d'info nécessaires.
    //Par exemple, un EMHScenaro requiert un unique ParamCalcScenario,OrdResScenario et peut contenir un Crue9ResultatCalculPasDeTemps
    res.put(EMHScenario.class, createList(
        canContain(CompteRenduAvancement.class),
        canContain(CompteRendusInfoEMH.class),
        requiredUnique(ParamCalcScenario.class),
        requiredUnique(OrdResScenario.class),
        requiredUnique(OrdCalcScenario.class),
        requiredUnique(DonCLimMScenario.class),
        requiredUnique(DonLoiHYConteneur.class)));

    res.put(EMHModeleBase.class, createList(
        requiredUnique(ParamNumModeleBase.class),
        requiredUnique(OrdPrtGeoModeleBase.class),
        requiredUnique(OrdPrtCIniModeleBase.class),
        requiredUnique(OrdPrtReseau.class),
        canContain(ResPrtReseau.class),
        canContain(ResultatCalcul.class),
        canContain(CompteRendusInfoEMH.class),
        canContain(ResultatPasDeTemps.class)));

    res.put(EMHSousModele.class, createList(
        requiredUnique(DonFrtConteneur.class)));

    res.put(EMHNoeudNiveauContinu.class, createList(
        canContainsBetween(DonPrtCIniNoeudNiveauContinu.class, 0, 1),
        canContain(CalcPseudoPermNoeudQapp.class),
        canContain(CalcPseudoPermNoeudBg1.class),
        canContain(CalcPseudoPermNoeudBg2.class),
        canContain(CalcPseudoPermNoeudUsi.class),
        canContain(CalcPseudoPermNoeudBg1Av.class),
        canContain(CalcPseudoPermNoeudBg2Av.class),
        canContain(CalcPseudoPermNoeudNiveauContinuZimp.class),
        canContain(CalcTransNoeudNiveauContinuTarage.class),
        canContain(CalcTransNoeudQapp.class),
        canContain(CalcTransNoeudQappExt.class),
        canContain(ResultatCalcul.class),
        canContain(CalcTransNoeudNiveauContinuLimnigramme.class),
        canContain(CalcTransNoeudBg1.class),
        canContain(CalcTransNoeudBg2.class),
        canContain(CalcTransNoeudUsine.class),
        canContain(CalcTransNoeudBg1Av.class),
        canContain(CalcTransNoeudBg2Av.class)
    ));

    res.put(EMHCasierProfil.class, createList(
        canContainOne(ResPrtCIniCasier.class),
        canContain(DonPrtGeoProfilCasier.class),
        canContainOne(DonPrtGeoBatiCasier.class),
        requiredUnique(DonCalcSansPrtCasierProfil.class),
        requiredUnique(DonPrtCIniCasierProfil.class),
        canContain(ResultatCalcul.class),
        canContain(ResPrtGeo.class),
        canContain(CalcTransCasierProfilQruis.class),
        canContain(CalcPseudoPermCasierProfilQruis.class)));
    // les branches
    res.put(EMHBranchePdc.class, createList(
        requiredUnique(DonCalcSansPrtBranchePdc.class),
        canContain(ResultatCalcul.class),
        canContainOne(DonPrtCIniBranche.class)));

    res.put(EMHBrancheSeuilLateral.class, createList(
        canContain(ResultatCalcul.class),
        requiredUnique(DonCalcSansPrtBrancheSeuilLateral.class),
        canContainOne(DonPrtCIniBranche.class)));

    res.put(EMHBrancheSeuilTransversal.class, createList(
        canContain(ResultatCalcul.class),
        requiredUnique(DonCalcSansPrtBrancheSeuilTransversal.class),
        canContainOne(DonPrtCIniBranche.class)));

    res.put(EMHBrancheOrifice.class, createList(
        canContain(ResultatCalcul.class),
        requiredUnique(DonCalcSansPrtBrancheOrifice.class),
        canContainOne(DonPrtCIniBrancheOrifice.class),
        canContain(CalcPseudoPermBrancheOrificeManoeuvre.class),
        canContain(CalcPseudoPermBrancheOrificeManoeuvreRegul.class),
        canContain(CalcTransBrancheOrificeManoeuvreRegul.class),
        canContain(CalcTransBrancheOrificeManoeuvre.class)));

    res.put(EMHBrancheStrickler.class, createList(
        canContain(ResultatCalcul.class),
        canContainOne(DonPrtCIniBranche.class),
        canContain(ResultatCalcul.class)));

    res.put(EMHBrancheBarrageGenerique.class, createList(
        canContain(ResultatCalcul.class),
        requiredUnique(DonCalcSansPrtBrancheBarrageGenerique.class),
        canContainOne(DonPrtCIniBranche.class)));

    res.put(EMHBrancheBarrageFilEau.class, createList(
        canContain(ResultatCalcul.class),
        requiredUnique(DonCalcSansPrtBrancheBarrageFilEau.class),
        canContainOne(DonPrtCIniBranche.class)));

    res.put(EMHBrancheSaintVenant.class, createList(
        requiredUnique(DonPrtGeoBrancheSaintVenant.class),
        requiredUnique(DonCalcSansPrtBrancheSaintVenant.class),
        canContainOne(DonPrtCIniBrancheSaintVenant.class),
        canContain(ResultatCalcul.class),
        canContain(CalcTransBrancheSaintVenantQruis.class),
        canContain(CalcPseudoPermBrancheSaintVenantQruis.class)));

    res.put(EMHBrancheNiveauxAssocies.class, createList(
        canContain(ResultatCalcul.class),
        requiredUnique(DonCalcSansPrtBrancheNiveauxAssocies.class),
        canContainOne(DonPrtCIniBranche.class)));

    // les sections
    res.put(EMHSectionProfil.class, createList(
        requiredUnique(DonPrtGeoProfilSection.class),
        canContainOne(DonPrtCIniSection.class),
        canContainOne(ResPrtCIniSection.class),
        canContain(ResPrtGeo.class),
        canContain(ResultatCalcul.class)));

    res.put(EMHSectionIdem.class, createList(
        requiredUnique(DonPrtGeoSectionIdem.class),
        canContainOne(DonPrtCIniSection.class),
        canContainOne(ResPrtCIniSection.class),
        canContain(ResPrtGeo.class),
        canContain(ResultatCalcul.class)));

    res.put(EMHSectionInterpolee.class, createList(
        canContainOne(DonPrtCIniSection.class),
        canContainOne(ResPrtCIniSection.class),
        canContain(ResPrtGeo.class),
        canContain(ResultatCalcul.class)));

    res.put(EMHSectionSansGeometrie.class, createList(
        canContainOne(DonPrtCIniSection.class),
        canContainOne(ResPrtCIniSection.class),
        canContain(ResPrtGeo.class),
        canContain(ResultatCalcul.class)));
    return res;
  }

  /**
   * Un validateur.
   *
   * @param aClass l'unique classe d'infoEMH de type données
   * @return resultat de validation
   */
  private static ValidationContentParamDelegate<Class<? extends InfosEMH>> requiredUnique(
      final Class<? extends InfosEMH> aClass) {
    return create(aClass, 1, 1);
  }

  @Override
  public void validContent(CtuluLog emhLog, EMH emhContenant, List objectsToTests) {
    super.validContent(emhLog, emhContenant, objectsToTests);
    final List<CalcTransItem> trans = EMHHelper.selectInstanceOf(emhContenant.getDCLM(), CalcTransItem.class);
    if (trans != null) {
      for (final CalcTransItem calcTransItem : trans) {
        if (calcTransItem.getLoi() == null) {
          emhLog.addSevereError("validation.emh.loi.notFound", emhContenant.getNom(), calcTransItem.getNomCalculParent());
        }
      }
    }
  }
}
