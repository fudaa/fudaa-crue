/**
 *
 */
package org.fudaa.dodico.crue.validation;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.validation.util.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Valide les valeurs et les relations
 *
 * @author deniger
 */
public class ValidatorForValuesAndContents extends AbstractCrueValidator {
  private final CrueConfigMetier props;
  final List<CtuluLog> all = new ArrayList<>();
  private ValidationContentSectionDansBrancheExecutor relationSectionDansBranche;

  public ValidatorForValuesAndContents(final CrueConfigMetier cruePropertyDefinitionContainer) {
    super();
    this.props = cruePropertyDefinitionContainer;
  }

  @Override
  public List<CtuluLog> getLogs() {
    return all;
  }

  private void validate(final Object o, final List<CtuluLog> all, final CtuluLog emhAnalyze,
                        final EMH emhContenante) {
    // a revoir
    final Class clazz = o.getClass();
    if (LoiDF.class.equals(clazz)) {
      ValidationHelper.addAnalyze(all, ValidationHelperLoi.validateLoiDF((LoiDF) o, props));
    } else if (LoiFF.class.equals(clazz)) {
      ValidationHelper.addAnalyze(all, ValidationHelperLoi.validateLoiFF((LoiFF) o, props));
    } else if (DonFrtStrickler.class.equals(clazz)) {
      ValidationHelper.addAnalyze(all, ValidationHelper.validateDonFrt((DonFrtStrickler) o, props));
    } else if (DonFrtManning.class.equals(clazz)) {
      ValidationHelper.addAnalyze(all, ValidationHelper.validateDonFrt((DonFrtManning) o, props));
    } else if (OrdPrtGeoModeleBase.class.equals(clazz)) {
      ValidationHelper.addAnalyze(all, ValidationHelper.validateOrdPrtGeoModeleBase((OrdPrtGeoModeleBase) o, props));
    } else if (OrdPrtCIniModeleBase.class.equals(clazz)) {
      ValidationHelper.addAnalyze(all, ValidationHelper.validateOrdPrtCIniModeleBase((OrdPrtCIniModeleBase) o, props));
    } else if (OrdPrtReseau.class.equals(clazz)) {
      ValidationHelper.addAnalyze(all, ValidationHelper.validateOrdPrtReseau((OrdPrtReseau) o, props));
    } else if (OrdCalcScenario.class.equals(clazz)) {
      ValidationHelper.addAnalyze(all, ValidationHelper.validateOrdCalcScenario((OrdCalcScenario) o, props));
    } else if (DonCLimMScenario.class.equals(clazz)) {
      ValidationHelper.addAnalyze(all, ValidationHelper.validateDonCLimMScenario((DonCLimMScenario) o, props));
    } else {
      String prefix = (o == emhContenante) ? null : StringUtils.substringAfterLast(o.getClass().toString(), ".");
      if (o != emhContenante && o instanceof RelationEMH) {
        prefix = BusinessMessages.getString("valide.relation", ((RelationEMH) o).getEmh().getNom());
      }
      ValidationHelper.validateObject(prefix, emhAnalyze, o, props);
    }
  }

  @Override
  public void validate(final Set<Object> o, final EMH emhContenante, final EMHModeleBase modeleBase) {
    final CtuluLog emhAnalyze = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    all.add(emhAnalyze);
    emhAnalyze.setDesc("common.validation");
    emhAnalyze.setDescriptionArgs(emhContenante.getNom());
    emhAnalyze.setResource(emhContenante.getClass().toString());
    final ValidationContentInfoEMHDansEMHExecutor contentValidator = new ValidationContentInfoEMHDansEMHExecutor();
    contentValidator.validContent(emhAnalyze, emhContenante, emhContenante.getInfosEMH());
    if (EnumCatEMH.BRANCHE.equals(emhContenante.getCatType())) {
      validateBranche(emhContenante, modeleBase, emhAnalyze);
    }
    if (EnumCatEMH.NOEUD.equals(emhContenante.getCatType())) {
      DelegateValidatorNoeud.validateNoeud(emhContenante, modeleBase, emhAnalyze);
    }
    if (EnumCatEMH.SECTION.equals(emhContenante.getCatType())) {
      DelegateValidatorSection.validateSection(emhAnalyze, emhContenante, modeleBase);
    }
    if (EnumCatEMH.CASIER.equals(emhContenante.getCatType())) {
      DelegateValidatorCasier.validateCasier(emhContenante, modeleBase, emhAnalyze);
    }
    for (final Object oToTest : o) {
      validate(oToTest, all, emhAnalyze, emhContenante);
    }

    if (emhAnalyze.isEmpty()) {
      all.remove(emhAnalyze);
    }
  }

  protected void validateBranche(final EMH emhContenante, final EMHModeleBase modeleBase, final CtuluLog emhAnalyze) {
    if (relationSectionDansBranche == null) {
      relationSectionDansBranche = new ValidationContentSectionDansBrancheExecutor(props);
    }
    new DelegateValidatorBranche(this.props).validateBranche(emhAnalyze, (CatEMHBranche) emhContenante, modeleBase,
        relationSectionDansBranche);
  }

  @Override
  public java.util.List<CtuluLog> validateScenario(final EMHScenario scenario) {
    List<CtuluLog> validateScenario = super.validateScenario(scenario);
    final CtuluLog logInfosEMHUNique = new DelegateValidatorEMHInfoUnique().valid(scenario);
    if (logInfosEMHUNique.containsErrorOrSevereError()) {
      if (validateScenario == null) {
        validateScenario = new ArrayList<>();
      }
      validateScenario.add(logInfosEMHUNique);
    }
    return validateScenario;
  }

  /**
   * @param scenario le scenario contenant pour savoir
   * @param crueConfigMetier le crueConfigMetier
   * @return liste des logs de validatins
   */
  public static List<CtuluLog> validateValues(final EMHScenario scenario, final CrueConfigMetier crueConfigMetier) {
    final ValidatorForValuesAndContents value = new ValidatorForValuesAndContents(crueConfigMetier);
    final ValidateEMHTreeWalker walker = new ValidateEMHTreeWalker();
    return walker.validateScenario(scenario, value);
  }
}
