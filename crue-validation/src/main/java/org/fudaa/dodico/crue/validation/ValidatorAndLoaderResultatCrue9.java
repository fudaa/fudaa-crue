package org.fudaa.dodico.crue.validation;

import gnu.trove.TIntObjectHashMap;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections4.functors.AndPredicate;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.io.neuf.FCBSequentialReader;
import org.fudaa.dodico.crue.io.neuf.FCBValueObject.EnteteBranche;
import org.fudaa.dodico.crue.io.neuf.FCBValueObject.EnteteCasier;
import org.fudaa.dodico.crue.io.neuf.FCBValueObject.EnteteProfil;
import org.fudaa.dodico.crue.io.neuf.ResPrtReseauCrue9Adapter;
import org.fudaa.dodico.crue.io.neuf.STOSequentialReader;
import org.fudaa.dodico.crue.io.neuf.STOSequentialReader.DonneesBranche;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.comparator.ComparatorRelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHCasierMNT;
import org.fudaa.dodico.crue.metier.emh.EMHCasierProfil;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSectionIdem;
import org.fudaa.dodico.crue.metier.emh.EMHSectionInterpolee;
import org.fudaa.dodico.crue.metier.emh.EMHSectionProfil;
import org.fudaa.dodico.crue.metier.emh.EMHSectionSansGeometrie;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.OrdRes;
import org.fudaa.dodico.crue.metier.emh.OrdResScenario;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.emh.ResPrtGeo;
import org.fudaa.dodico.crue.metier.emh.ResultatCalcul;
import org.fudaa.dodico.crue.metier.emh.ResultatPasDeTemps;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.PredicateFactory;
import org.fudaa.dodico.crue.metier.result.Crue9ResultatCalculPasDeTemps;

/**
 * Utiliser pour valider les résultats : sont-ils cohérents avec les ordre de calculs.
 *
 * @author deniger
 */
public class ValidatorAndLoaderResultatCrue9 {

  private final static Logger LOGGER = Logger.getLogger(ValidatorAndLoaderResultatCrue9.class.getName());

  /**
   * Utiliser pour valider les résultats : sont-ils cohérents avec les ordre de calculs.
   *
   * @param scenario le scenario
   * @param log le log a populer
   * @return le log
   */
  private CtuluLog validateResultatOrdreCalcul(EMHScenario scenario, CtuluLog log) {
    OrdResScenario ordResScenario = scenario.getOrdResScenario();
    Set<Class> classEMHWithResultat = new HashSet<>();
    try {
      Map describe = PropertyUtils.describe(ordResScenario);

      for (Object value : describe.values()) {
        if (value instanceof OrdRes && isActivated((OrdRes) value)) {
          String name = StringUtils.substringAfter(value.getClass().getSimpleName(), "OrdRes");
          if ("Section".equals(name)) {
            classEMHWithResultat.add(EMHSectionIdem.class);
            classEMHWithResultat.add(EMHSectionInterpolee.class);
            classEMHWithResultat.add(EMHSectionProfil.class);
            classEMHWithResultat.add(EMHSectionSansGeometrie.class);
          } else if ("Casier".equals(name)) {
            classEMHWithResultat.add(EMHCasierMNT.class);
            classEMHWithResultat.add(EMHCasierProfil.class);
          } else {
            Class c = Class.forName(CatEMHNoeud.class.getPackage().getName() + ".EMH" + name);
            classEMHWithResultat.add(c);
          }

        }
      }
    } catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }
    for (Class emhClass : classEMHWithResultat) {
      PredicateFactory.PredicateClass predicateClass = new PredicateFactory.PredicateClass(emhClass);
      List<EMH> emhWithResult = CollectionCrueUtil.select(scenario.getAllSimpleEMH(),
              new AndPredicate(predicateClass,
              PredicateFactory.PREDICATE_EMH_ACTUALLY_ACTIVE));
      for (EMH emh : emhWithResult) {
        // on teste uniquement les emh activee
        if (emh.getResultatCalcul() == null) {
          log.addError("res.noResultatForEMH", emh.getNom());
        }

      }
    }
    return log;
  }

  private boolean isActivated(OrdRes res) {
    try {
      Map describe = PropertyUtils.describe(res);
      for (Iterator it = describe.entrySet().iterator(); it.hasNext();) {
        Map.Entry entry = (Map.Entry) it.next();
        if (((String) entry.getKey()).startsWith("dde") && Boolean.TRUE.equals(entry.getValue())) {
          return true;
        }

      }
    } catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }
    return false;
  }

  public static void loadAndValidStr(final ResPrtReseauCrue9Adapter rptg, final CrueData data,
          final EMHScenario scenario, final CtuluLogGroup mng) {
    final CtuluLog analyze = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    analyze.setDesc("load.validStoStr");
    mng.addLog(analyze);
    loadAndValidStrCasier(data, analyze, rptg);
    final int nbProfil = rptg.getNbResOnProfil();
    for (int i = 0; i < nbProfil; i++) {
      final String nomCrue9 = rptg.getProfilNom(i);
      if (nomCrue9.indexOf('#') < 0) {//le cas avec # correspond au profil casier qui n'est plus traité actuellement.
        final CatEMHSection section = data.findSectionByReference(CruePrefix.addPrefix(nomCrue9.toUpperCase(),
                EnumCatEMH.SECTION).toUpperCase());
        if (section == null) {
          analyze.addSevereError("rptg.section.notFound", nomCrue9);
        } else {
          section.addInfosEMH(rptg.getResultatOnProfil(nomCrue9));
        }
      }
    }
    List<CatEMHSection> sections = data.getSections();
    for (CatEMHSection sectionSansGeometrie : sections) {
      if (EMHSectionSansGeometrie.class.equals(sectionSansGeometrie.getClass())) {
        Map<String, Object> values = new HashMap<>();
        values.put("coefSinuo", rptg.getCoefSinuo(sectionSansGeometrie.getId()));
        ResPrtGeo res = new ResPrtGeo(values, sectionSansGeometrie.getNom(), true, -1, false);
        sectionSansGeometrie.addInfosEMH(res);
      }

    }
    ValidatorResultatPretraitementMinimum minRes = new ValidatorResultatPretraitementMinimum();
    minRes.validateResultat(scenario, analyze);

  }

  private static boolean loadAndValidStrCasier(final CrueData data, final CtuluLog analyze,
                                               final ResPrtReseauCrue9Adapter rptg) {
    boolean error = false;
    final int nbCasier = rptg.getNbResOnCasier();
    for (int i = 0; i < nbCasier; i++) {
      final String nom = rptg.getCasierNom(i);
      final String id = CruePrefix.changePrefix(nom.toUpperCase(), CruePrefix.getPrefix(EnumCatEMH.CASIER),
              CruePrefix.getPrefix(EnumCatEMH.NOEUD)).toUpperCase();
      final CatEMHNoeud nd = data.findNoeudByReference(id);
      if (nd == null) {
        error = true;
        analyze.addError("rptg.noeud.notFound", nom);
      } else if (nd.getCasier() == null) {
        analyze.addSevereError("rptg.noeud.casierNotFound", nom);
      } else {
        nd.getCasier().addInfosEMH(rptg.getResultatOnCasier(nom));
      }
    }
    return error;
  }

  public static void validFcb(final FCBSequentialReader res, final CrueData data, final EMHScenario scenario,
          final CtuluLogGroup mng) {
    final CtuluLog logResultat = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    logResultat.setDesc("load.validFcb");
    mng.addLog(logResultat);
    // boolean error = false;
    validFcbBranches(res, data, logResultat);
    validFcbCasiers(res, data, logResultat);
    validFcbProfils(res, data, logResultat);
    ValidatorAndLoaderResultatCrue9 resMin = new ValidatorAndLoaderResultatCrue9();
    resMin.validateResultatOrdreCalcul(scenario, logResultat);
    if (logResultat.containsErrorOrSevereError()) {
      scenario.clearAllResultatCalcul();
    }

    final Crue9ResultatCalculPasDeTemps pdt = res.getPdt();
    if (pdt != null && pdt.getNbResultats() > 0 && scenario.getModeles().size() == 1) {
      scenario.getModeles().get(0).addInfosEMH(new ResultatPasDeTemps(pdt));
    }

  }

  public static void validFcbProfils(final FCBSequentialReader res, final CrueData data, final CtuluLog analyze) {

    STOSequentialReader sto = (STOSequentialReader) data.getSto();
    if (sto == null) {
      return;
    }
    TIntObjectHashMap sectionByNumAbsolu = new TIntObjectHashMap();
    List<DonneesBranche> donneesBranches = sto.getDonneesBranches();
    // WARN sto stocke les branches avec leurs nom initial: il faut les traduire en crue 10,
    for (DonneesBranche donneesBranche : donneesBranches) {
      String nomBr = CruePrefix.addPrefix(donneesBranche.getNomBr().trim(), EnumCatEMH.BRANCHE);
      CatEMHBranche branche = data.findBrancheByReference(nomBr);
      if (branche == null) {
        analyze.addError("res.branche.notFound", nomBr);

      } else {
        List<RelationEMHSectionDansBranche> sections = branche.getSections();
        Collections.sort(sections,
                new ComparatorRelationEMHSectionDansBranche(data.getCrueConfigMetier()));
        int[] n0Pr = donneesBranche.getN0Pr();
        // WARN dans les données les indices fortran commencent à 1:
        for (int i = 0; i < n0Pr.length; i++) {
          // on enleve 1 pour partir de 0:
          sectionByNumAbsolu.put(n0Pr[i] - 1, sections.get(i).getEmh());
        }
      }
    }
    int nbAbsolu = 0;
    for (final EnteteProfil prof : res.getContainerProfil().getListData()) {

      final String id = prof.getNom().toUpperCase();
      final CatEMHSection foundSection = (CatEMHSection) sectionByNumAbsolu.get(nbAbsolu);
      ResultatCalcul resultatCalcul = null;
      if (StringUtils.isEmpty(id)) {
        resultatCalcul = res.getContainerProfil().createInfoEMH(foundSection, nbAbsolu);
      } else {
        resultatCalcul = res.getContainerProfil().createInfoEMH(foundSection);
      }
      if (StringUtils.isNotEmpty(id) && foundSection == null) {
        analyze.addError("res.section.notFound", prof.getNom());
        // error = true;
      } else if (foundSection != null) {
        foundSection.addInfosEMH(resultatCalcul);
      }
      nbAbsolu++;
    }
  }

  private static void validFcbCasiers(final FCBSequentialReader res, final CrueData data, final CtuluLog analyze) {
    for (final EnteteCasier nd : res.getContainerCasiers().getListData()) {
      final String id = nd.getNom().toUpperCase();
      final CatEMHNoeud foundNoeud = data.findNoeudByReference(id);
      if (foundNoeud == null) {
        analyze.addError("res.noeud.notFound", nd.getNom());
        // error = true;
      } else if (foundNoeud.getCasier() != null) {
        int position = res.getContainerCasiers().getPosition(foundNoeud.getId());
        foundNoeud.getCasier().addInfosEMH(res.getContainerCasiers().createInfoEMH(foundNoeud.getCasier(), position));
      }
    }
  }

  public static void validFcbBranches(final FCBSequentialReader res, final CrueData data, final CtuluLog analyze) {
    for (final EnteteBranche br : res.getContainerBranches().getListData()) {
      final String id = br.getNom().toUpperCase();
      final CatEMHBranche foundBranche = data.findBrancheByReference(id);
      if (foundBranche == null) {
        analyze.addError("res.branche.notFound", br.getNom());
        // error = true;
      } else {
        String type = foundBranche.getType();
        if ("EMHBrancheSaintVenant".equals(type) || "EMHBrancheStrickler".equals(type)) {
          foundBranche.addInfosEMH(res.getContainerBranches().createInfoEMH(foundBranche));
        }
      }
    }
  }
}
