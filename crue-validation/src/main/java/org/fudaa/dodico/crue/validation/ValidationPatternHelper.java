package org.fudaa.dodico.crue.validation;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageAlgorithme;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * Note pour la taille max des fichiers et scnénario:<br>
 * <pre>
 * Un scenario nommé Sc_Radical contient des fichiers nommées Radical.dclm.xml.
 * Lors d'une campagne AOC, ces fichiers seront renommées Radical_AST.dcml.xml (.dclm.xml est l'extension et _AST le suffixe ajouté par la compagne AOC).
 * la taille des noms fichiers, de scenario max est 32.
 * Il faut compter 9 caractères pour l'extension .dclm.xml
 * Il faut compter 4 caractères pour le nouveau nom de scenario ( exemple:_AST)
 * La taille max pour "Radical" est 32-9 ( extension fichier ) - 4  ( suffixe AOC)= 19
 *
 * Donc la taille max d'un nom de scenario avec le prefixe Sc_ est:  19 + 3 = 22
 * </pre>
 *
 * @author deniger
 */
public class ValidationPatternHelper {
    private static Pattern rapportPattern;
    private static Pattern namePattern;
    private static Pattern filenamePattern;
    private static Pattern nameEMHSimplePattern;

    public static final int PREFIX_LENGTH = 3;
    public static final int TAILLE_MAX_NOM_SCENARIO_AOC = 22;
    public static final int SPECIFIC_MAX_TAILLE_FOR_BLANK_SCENARIO = 22;
    public static final int SPECIFIC_MAX_TAILLE_FOR_RUN = 32;
    /**
     * La taille max du radical du fichier (sans extension) pour assurer compatibilité avec AOC.
     */
    public static final int SPECIFIC_MAX_TAILLE_FOR_FILE_RADICAL = 19;

    public static final String MESSAGE_ERROR_BLANK_SCENARIO_NAME_TOO_LONG = "valid.nom.tooLong.blankScenario";
    public static final String MESSAGE_ERROR_FILE_NAME_TOO_LONG = "valid.nom.tooLong.file";
    public static final String MESSAGE_ERROR_DEEP_COPY_NAME_TOO_LONG = "valid.nom.tooLong.deepCopy";
    public static final String MESSAGE_ERROR_RENAME_NAME_TOO_LONG = "valid.nom.tooLong.rename";

    private ValidationPatternHelper() {
    }

    private static Pattern getNameContainerPattern() {
        if (namePattern == null) {
            namePattern = Pattern.compile("[a-zA-Z]{1}([\\w\\-\\_]|\\.)*");
        }
        return namePattern;
    }

    private static Pattern getFilenamePattern() {
        if (filenamePattern == null) {
            filenamePattern = Pattern.compile("[a-zA-Z]{1}([\\w\\-\\_])*");
        }
        return filenamePattern;
    }

    private static Pattern getNameEmhSimplePattern() {
        if (nameEMHSimplePattern == null) {
            nameEMHSimplePattern = Pattern.compile("([\\w\\-\\_]|\\.)*");
        }
        return nameEMHSimplePattern;
    }

    public static Pattern getRapportNamePattern() {
        if (rapportPattern == null) {
            //y a surement plus simple...
            rapportPattern = Pattern.compile("([\\w\\-\\_ éèêäàüòôïîiç]|\\.)*");
        }
        return rapportPattern;
    }

    public static String isRunNameValidei18nMessage(String name) {
        if (StringUtils.isBlank(name)) {
            return getBlankNameMessage();
        }
        if (name.length() > CruePrefix.NB_CAR_MAX) {
            return getTooLongMessage();
        }
        boolean res = getNameContainerPattern().matcher(name).matches();
        if (!res) {
            return BusinessMessages.getString("validation.contentInvalid.error");
        }
        return null;
    }

    public static boolean isNameValide(String name, EnumCatEMH type) {
        String prefix = CruePrefix.getPrefix(type);
        if (type.isContainer()) {
            return isNameValid(name, prefix, getNameContainerPattern());
        }
        return isNameValid(name, prefix, getNameEmhSimplePattern());
    }

    public static boolean isNameValide(String name, EnumTypeLoi type) {

        String prefix = CruePrefix.getPrefix(type);
        return isNameValid(name, prefix, getNameEmhSimplePattern());
    }

    /**
     * @param name nom a valider
     * @param type le type
     * @return null si ok sinon le message d'erreur.
     */
    public static String isNameValidei18nMessage(String name, EnumCatEMH type) {
        String prefix = CruePrefix.getPrefix(type);
        if (type.isContainer()) {
            return isNameValidi18nErrorMessage(name, prefix, getNameContainerPattern());
        }
        return isNameValidi18nErrorMessage(name, prefix, getNameEmhSimplePattern());
    }

    public static String isNameValidei18nMessage(String name, String prefix) {
        return isNameValidi18nErrorMessage(name, prefix, getNameEmhSimplePattern());
    }

    public static String isNameValidei18nMessage(String name, EnumTypeLoi type) {

        String prefix = CruePrefix.getPrefix(type);
        return isNameValidi18nErrorMessage(name, prefix, getNameEmhSimplePattern());
    }

    static boolean isNameValid(String name, String prefix, Pattern pattern) {
        if (StringUtils.isBlank(name)) {
            return false;
        }
        if (!name.startsWith(prefix)) {
            return false;
        }
        if (name.length() <= prefix.length()) {
            return false;
        }
        if (name.length() > CruePrefix.NB_CAR_MAX) {
            return false;
        }
        return pattern.matcher(StringUtils.removeStart(name, prefix)).matches();
    }

    public static String isNameValidi18nErrorMessage(String name, String prefix, Pattern pattern) {
        if (StringUtils.isBlank(name)) {
            return getBlankNameMessage();
        }
        if (!name.startsWith(prefix)) {
            return BusinessMessages.getString("validation.name.prefix.error", prefix);
        }
        if (name.length() > CruePrefix.NB_CAR_MAX) {
            return getTooLongMessage();
        }
        boolean res = pattern.matcher(StringUtils.removeStart(name, prefix)).matches();
        if (!res) {
            return BusinessMessages.getString("validation.contentInvalid.error");
        }
        return null;
    }

    private static String getTooLongMessage() {
        return BusinessMessages.getString("valid.nom.tooLong.short");
    }

    private static String getBlankNameMessage() {
        return BusinessMessages.getString("validation.name.isBlank.error");
    }

    public static String isFilenameValidei18nErrorMessage(String name, CrueFileType type) {
        if (StringUtils.isBlank(name)) {
            return getBlankNameMessage();
        }
        if (name.length() > CruePrefix.NB_CAR_MAX) {
            return getTooLongMessage();
        }
        if (!name.endsWith(type.getExtension())) {
            return BusinessMessages.getString("valid.filename.extension.error");
        }
        final String removeEnd = StringUtils.removeEnd(name, "." + type.getExtension());
        if (!getFilenamePattern().matcher(removeEnd).matches()) {
            return BusinessMessages.getString("validation.contentInvalid.error");
        }
        return null;
    }

    /**
     * @param radical le radical sans le prefix de 3 lettres
     * @return null si suit le pattern, non vide et en sorte que 3+ nbChars < 32. Le message d'erreur sinon
     */
    public static String isRadicalValideForEMHContainerMsg(String radical) {
        return isRadicalValideForEMHContainerMsg(radical, -1, null);
    }

    /**
     * Pour un scenario vierge on ne peut utiliser plus de 27 caractères
     *
     * @param radical le radical sans le prefix de 3 lettres
     * @return null si suit le pattern, non vide et en sorte que 3+ nbChars < 27. Le message d'erreur sinon
     */
    public static String isRadicalValideForBlankScenario(String radical) {
        return isRadicalValideForEMHContainerMsg(radical, ValidationPatternHelper.SPECIFIC_MAX_TAILLE_FOR_BLANK_SCENARIO, MESSAGE_ERROR_BLANK_SCENARIO_NAME_TOO_LONG);
    }

    /**
     * @param maxLength la taille max attendue. Si négative ou null prend {@link CruePrefix#NB_CAR_MAX}
     * @return la taille max pour le radical
     */
    public static int getRadicalMaxLength(int maxLength) {
        if (maxLength > 0) {
            return maxLength - PREFIX_LENGTH;
        }
        return CruePrefix.NB_CAR_MAX - PREFIX_LENGTH;
    }

    /**
     * @param radical         le radical a tester
     * @param maxCharExpected le nombre max de caractere en tout ( y compris le prefix de 3 lettres). Si <0, {@link CruePrefix#NB_CAR_MAX} est utilisé
     * @param message         le message a renvoyer si la taille max n'est pas respectée.
     * @return string|null si suit le pattern, non vide et en sorte que 3+ nbChars < maxCharExpected. Le message d'erreur sinon
     */
    public static String isRadicalValideForEMHContainerMsg(String radical, int maxCharExpected, String message) {
        return isRadicalValideForEMHContainerMsg(radical, maxCharExpected, message, true);
    }

    /**
     * @param radical         le radical a tester
     * @param maxCharExpected le nombre max de caractere en tout ( y compris le prefix de 3 lettres). Si <0, {@link CruePrefix#NB_CAR_MAX} est utilisé
     * @param message         le message a renvoyer si la taille max n'est pas respectée.
     * @param checkAocPrefix  true si on doit tester que le radical ne se termine pas par un suffixe de scénario AOC.
     * @return string|null si suit le pattern, non vide et en sorte que 3+ nbChars < maxCharExpected. Le message d'erreur sinon
     */
    public static String isRadicalValideForEMHContainerMsg(String radical, int maxCharExpected, String message, boolean checkAocPrefix) {
        if (StringUtils.isBlank(radical)) {
            return "validation.name.isBlank.error";
        }
        boolean res = getNameContainerPattern().matcher(radical).matches();
        if (!res) {
            return "validation.name.pattern.error";
        }
        Set<String> allPrefix = CruePrefix.getAllPrefix();
        for (String prefix : allPrefix) {
            if (radical.startsWith(prefix)) {
                return "validation.radical.prefix.error";
            }
        }
        if (isRadicalSizeInvalid(radical, maxCharExpected)) {
            return message == null ? "valid.nom.tooLong.short" : message;
        }
        if (checkAocPrefix && !getScenarioFilterForAoc().test(radical)) {
            return "validation.radical.aoc.suffix.error";
        }
        return null;
    }

    private static boolean isRadicalSizeInvalid(String radical, int nbCharMax) {
        int length = PREFIX_LENGTH + radical.length();
        int max = nbCharMax > 0 ? nbCharMax : CruePrefix.NB_CAR_MAX;
        return length > max;
    }

    public static boolean isRadicalValideForEMHContainer(String radical) {
        return isRadicalValideForEMHContainerMsg(radical) == null;
    }

    public static boolean isFilenameValide(String name, CrueFileType type) {
        if (StringUtils.isBlank(name)) {
            return false;
        }
        if (!name.endsWith(type.getExtension())) {
            return false;
        }
        if (name.length() > CruePrefix.NB_CAR_MAX) {
            return false;
        }
        final String removeEnd = StringUtils.removeEnd(name, "." + type.getExtension());
        return getFilenamePattern().matcher(removeEnd).matches();
    }

    /**
     * @return un filtre pour les noms des scenarios utilisables dans une campagne AOC
     */
    public static Predicate<String> getScenarioFilterForAoc() {
        final List<String> allPrefix = getAllAocSuffixes();
        return s -> {
            for (String prefix : allPrefix) {
                if (StringUtils.endsWith(s, "_" + prefix)) {
                    return false;
                }
            }
            return true;
        };
    }

    /**
     * @return tous les suffixes potentiellement utilisables par une compagne AOC.
     */
    private static List<String> getAllAocSuffixes() {
        List<String> all = new ArrayList<>();
        //analyse sensibilité permaent et transitoire
        Arrays.stream(EnumAocCalageAlgorithme.values()).forEach(algo ->
        {
            all.add(algo.getIdentifiantShort());
            all.add(algo.getIdentifiantShort() + 'T');
        });
        all.add("AS");
        all.add("AST");
        return all;
    }


}
