package org.fudaa.dodico.crue.validation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.OrdCalcScenario;
import org.fudaa.dodico.crue.metier.emh.ResPrtReseau;
import org.fudaa.dodico.crue.metier.emh.ResPrtReseauNoeud;
import org.fudaa.dodico.crue.metier.emh.ResPrtReseauNoeuds;

/**
 * @author deniger
 */
public class ValidatorResultatCrue10Prt {

  public boolean validateResultatPretraitement(final EMHScenario scenario, final EMHModeleBase modele, final CtuluLogGroup mng) {

    final ResPrtReseau reseau = modele.getResPrtReseau();

    final CtuluLog main = createMainAnalyze(mng, modele.getNom());
    if (reseau == null) {
      main.addSevereError("rptr.noResultat");
      return false;
    }
    boolean valide = true;
    final Set<String> nomOfCalcFound = new HashSet<>();
    List<ResPrtReseauNoeuds> listResPrtReseauNoeuds = reseau.getResPrtReseauNoeuds();
    if (listResPrtReseauNoeuds == null) {
      listResPrtReseauNoeuds = Collections.emptyList();
    }
    final List<CatEMHNoeud> noeuds = modele.getNoeuds();
    final OrdCalcScenario ordCalcScenario = scenario.getOrdCalcScenario();
    for (final ResPrtReseauNoeuds resPrtReseauNoeuds : listResPrtReseauNoeuds) {
      final Calc calc = resPrtReseauNoeuds.getCalc();
      if (!ordCalcScenario.isUsed(calc)) {
        main.addInfo("res.validate.CalcInactive.error", calc.getNom());
      }
      nomOfCalcFound.add(calc.getNom());
      final List<ResPrtReseauNoeud> listResPrtReseauNoeud = resPrtReseauNoeuds.getResPrtReseauNoeud();
      if (listResPrtReseauNoeud != null) {
        final List<String> findDoublonPrtReseauNoeud = findDoublonPrtReseauNoeud(listResPrtReseauNoeud);
        final List<String> notDefinedEMHNoeud = findEMHNoeudNotDefined(listResPrtReseauNoeud, noeuds);
        final List<String> unactiveDefinedEMHNoeud = findUnactiveEMHNoeudDefined(listResPrtReseauNoeud, noeuds);
        final List<String> unknownPrtNoeuds = findUnknownPrtNoeud(listResPrtReseauNoeud, noeuds);
        if (!findDoublonPrtReseauNoeud.isEmpty()) {
          main.addError("rptr.prtNoeudsDoublons.error", calc.getNom(), StringUtils.join(findDoublonPrtReseauNoeud, "; "));
          valide = false;
        }
        if (!notDefinedEMHNoeud.isEmpty()) {
          main.addSevereError("rptr.CatEMHNoeudNotDefined.error", calc.getNom(), StringUtils.join(notDefinedEMHNoeud, "; "));
          valide = false;
        }
        if (!unknownPrtNoeuds.isEmpty()) {
          main.addError("rptr.prtNoeudsUnknown.error", calc.getNom(), StringUtils.join(unknownPrtNoeuds, "; "));
          valide = false;
        }
        if (!unactiveDefinedEMHNoeud.isEmpty()) {
          main.addError("rptr.UnactiveEMHNoeud.error", calc.getNom(), StringUtils.join(unactiveDefinedEMHNoeud, "; "));
          valide = false;
        }
      }
    }
    final List<Calc> allCalcs = scenario.getDonCLimMScenario().getCalc();
    final Collection<String> toNom = TransformerHelper.toNom(allCalcs);
    toNom.removeAll(nomOfCalcFound);
    if (!toNom.isEmpty()) {
      main.addError("rptr.calcWithoutResPrtReseauNoeuds.error", StringUtils.join(toNom, "; "));
      valide = false;
    }

    return valide;
  }

  private CtuluLog createMainAnalyze(final CtuluLogGroup mng, final String modeleNom) {
    final CtuluLog analyze = new CtuluLog();
    analyze.setDesc("load.validRptr");
    analyze.setDescriptionArgs(modeleNom);
    analyze.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    mng.addLog(analyze);
    return analyze;
  }

  private List<String> findUnknownPrtNoeud(final List<ResPrtReseauNoeud> listResPrtReseauNoeud, final List<CatEMHNoeud> noeuds) {
    final Set<String> setOfIdEMHNoeud = TransformerHelper.toSetOfId(noeuds);
    final List<String> unknownPrtNoeuds = new ArrayList<>();
    for (final ResPrtReseauNoeud prtNoeud : listResPrtReseauNoeud) {
      if (!setOfIdEMHNoeud.contains(prtNoeud.getId())) {
        unknownPrtNoeuds.add(prtNoeud.getNom());
      }

    }
    return unknownPrtNoeuds;
  }

  private List<String> findDoublonPrtReseauNoeud(final List<ResPrtReseauNoeud> listResPrtReseauNoeud) {

    final Set<String> setOfNomPrtNoeud = new HashSet<>();
    final List<String> doublonNode = new ArrayList<>();
    for (final ResPrtReseauNoeud resPrtReseauNoeud : listResPrtReseauNoeud) {
      final String nom = resPrtReseauNoeud.getNom();
      if (setOfNomPrtNoeud.contains(nom)) {
        doublonNode.add(nom);
      } else {
        setOfNomPrtNoeud.add(nom);
      }

    }
    return doublonNode;
  }

  private List<String> findEMHNoeudNotDefined(final List<ResPrtReseauNoeud> listResPrtReseauNoeud, final List<CatEMHNoeud> noeuds) {
    final Set<String> setOfIdPrtNoeud = TransformerHelper.toSetOfId(listResPrtReseauNoeud);
    final List<String> notDefinedNoeuds = new ArrayList<>();
    for (final CatEMHNoeud catEMHNoeud : noeuds) {
      if (catEMHNoeud.getActuallyActive() && !setOfIdPrtNoeud.contains(catEMHNoeud.getId())) {
        notDefinedNoeuds.add(catEMHNoeud.getNom());
      }
    }
    return notDefinedNoeuds;
  }

  private List<String> findUnactiveEMHNoeudDefined(final List<ResPrtReseauNoeud> listResPrtReseauNoeud, final List<CatEMHNoeud> noeuds) {
    final Set<String> setOfIdPrtNoeud = TransformerHelper.toSetOfId(listResPrtReseauNoeud);
    final List<String> unactiveDefinedNoeuds = new ArrayList<>();
    for (final CatEMHNoeud catEMHNoeud : noeuds) {
      if (!catEMHNoeud.getActuallyActive() && setOfIdPrtNoeud.contains(catEMHNoeud.getId())) {
        unactiveDefinedNoeuds.add(catEMHNoeud.getNom());
      }
    }
    return unactiveDefinedNoeuds;
  }
}
