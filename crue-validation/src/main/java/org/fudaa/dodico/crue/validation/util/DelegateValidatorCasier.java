package org.fudaa.dodico.crue.validation.util;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EnumInfosEMH;
import org.fudaa.dodico.crue.metier.emh.InfosEMH;
import org.fudaa.dodico.crue.metier.emh.OrdPrtCIniModeleBase;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

/**
 * Un validateur spécifique pour les casiers.
 *
 * @author deniger
 */
public class DelegateValidatorCasier {

  public static void validateCasier(EMH emh, EMHModeleBase modeleBase, CtuluLog log) {
    //on valide avec DPTI
    OrdPrtCIniModeleBase selectInfoEMH = (OrdPrtCIniModeleBase) EMHHelper.selectInfoEMH(modeleBase,
            EnumInfosEMH.ORD_PRT_CINI_MODELE_BASE);

    if (selectInfoEMH != null && selectInfoEMH.getMethodeInterpol()!=null && selectInfoEMH.getMethodeInterpol().isCasierActiveRequireDPTI()) {
      InfosEMH dpti = EMHHelper.selectInfoEMH(emh, EnumInfosEMH.DON_PRT_CINI);
      if (dpti == null) {
        log.addSevereError("valid.casier.mustContainDPTI", emh.getNom(), selectInfoEMH.getMethodeInterpol().name());
      }
    }
  }
}
