/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.crue.validation;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueFileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class ScenarioWriterForXSDValidation extends CrueFileOpWriterCharSimpleAbstract {
  /**
   * Entête de tout fichier XML
   */
  public static final String ENTETE_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
  /**
   * Propriété XMLNS
   */
  public static final String XMLNS = "http://www.fudaa.fr/xsd/crue";
  /**
   * Propriété XMLNS:XSI
   */
  public static final String XMLNS_XSI = "http://www.w3.org/2001/XMLSchema-instance";
  /**
   * Propriété XMLNS:XI
   */
  public static final String XMLNS_XI = "http://www.w3.org/2001/XInclude";
  /**
   * Propriété XSI:SCHEMALOCATION auquel il faut concaténer le nom du fichier XSD
   */
  public static final String XSI_SCHEMALOCATION_SANS_NOM_XSD = XMLNS + " " + XMLNS;
  private final CoeurConfigContrat coeurConfigContrat;
  File confLoiFile;

  public ScenarioWriterForXSDValidation(CoeurConfigContrat coeurConfigContrat) {
    this.coeurConfigContrat = coeurConfigContrat;
  }

  /**
   * Forme la chaîne qui contient toutes les informations pour la validation d'un fichier XML (xmlns, xmlns:xsi, xmlns:xi, xsi:schemaLocation) et pour
   * accepter les inclusions de fichiers
   *
   * @param nomXSD le nom de la xsd.
   * @return la chaîne formée des attributs cités
   */
  public static String getChaineXMLAvecXmlnsXI(final String nomXSD) {

    return "xmlns=\"" + XMLNS + "\" xmlns:xsi=\"" + XMLNS_XSI + "\" xmlns:xi=\"" + XMLNS_XI
        + "\" xsi:schemaLocation=\"" + XSI_SCHEMALOCATION_SANS_NOM_XSD + "/" + nomXSD + ".xsd";
  }


  @Override
  protected void internalWrite(Object objectToWrite) {
    if (objectToWrite instanceof InputFormatWriter) {
      try {
        writeInput((InputFormatWriter) objectToWrite);
      } catch (IOException e) {
        Logger.getLogger(getClass().getName()).log(Level.SEVERE, "internalWrite", e);
        analyze_.manageException(e);
      }
    }
  }

  public File getConfLoiFile() {
    return confLoiFile;
  }

  /**
   * Ecriture du fichier XML de validation d'un scénario (qui contient l'ensemble des fichiers XML impactés dans le scénario qu'ils soient de niveau
   * scénario, modèle et sous-modèle)
   *
   * @param in Objet qui contient le scénario à valider et la version de la XSD globale de validation
   */
  private void writeInput(InputFormatWriter in) throws IOException {

    if (in == null || in.scenario == null || in.version == null || in.crueProps == null) {
      return;
    }

    // Ecriture de l'entête
    this.writelnToOut(ENTETE_XML);
    this.writelnToOut("<ValidationScenario " + getChaineXMLAvecXmlnsXI("scenario-" + in.version) + "\">");
    String crueConfigMetierURL = coeurConfigContrat.getCrueConfigMetierURL();
    File f = new File(crueConfigMetierURL);
    URL configURL = null;
    if (f.exists()) {
      configURL = f.toURI().toURL();
    } else {
      configURL = getClass().getResource(crueConfigMetierURL);
    }
    writeIncludeFichier(configURL);
    // Ecriture des fichiers du scénario
    List<FichierCrue> listeFichiersScenario = in.scenario.getListeFichiers().getFichiers();
    if (listeFichiersScenario != null) {
      this.writelnToOut("<ScenarioFichiers>");
      for (final FichierCrue fichierCrue : listeFichiersScenario) {
        if (fichierCrue.getType().equals(CrueFileType.LHPT)) {
          continue;
        }
        writeIncludeFichier(in.idFile.get(fichierCrue.getNom()));
      }
      this.writelnToOut("</ScenarioFichiers>");
    }

    // Ecriture des ValidationModele
    List<ManagerEMHModeleBase> modeles = in.scenario.getFils();

    if (CollectionUtils.isNotEmpty(modeles)) {
      for (final ManagerEMHModeleBase modeleBase : modeles) {

        this.writelnToOut("<ValidationModele>");

        // Ecriture des fichiers du modèle
        writeIncludeListeFichiers(modeleBase.getListeFichiers().getFichiers(), "ModeleFichiers", in.idFile);

        // Ecriture des fichiers des sous-modèles
        final List<ManagerEMHSousModele> ssModeles = modeleBase.getFils();
        if (!CollectionUtils.isEmpty(ssModeles)) {
          for (ManagerEMHSousModele ssModele : ssModeles) {
            writeIncludeListeFichiers(ssModele.getListeFichiers().getFichiers(), "SousModeleFichiers", in.idFile);
          }
        }
        this.writelnToOut("</ValidationModele>");
      }
    }

    this.writelnToOut("</ValidationScenario>");
  }

  /**
   * Ecriture d'une ligne xi:include
   *
   * @param nomFichier Nom du fichier à inclure
   */
  private void writeIncludeFichier(URL nomFichier) throws IOException {
    this.writelnToOut("<xi:include href=\"" + nomFichier.toString() + "\" />");
  }

  @Override
  public void setFile(File file) {
    analyze_ = new CtuluLog();
    analyze_.setResource(file.getAbsolutePath());
    try {
      out_ = new FileWriterWithEncoding(file, "UTF-8");
    } catch (final IOException exception) {
      analyze_.manageException(exception);
    }
    analyze_.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
  }

  /**
   * Ecriture d'un ensemble de fichiers à inclure
   *
   * @param listeFichiers liste des fichiers
   * @param nomBalise     Nom de la balise qui englobe la liste des fichiers à inclure
   */
  private void writeIncludeListeFichiers(final List<FichierCrue> listeFichiers, final String nomBalise,
                                         Map<String, URL> files) throws IOException {

    if (CollectionUtils.isEmpty(listeFichiers)) {
      return;
    }

    this.writelnToOut("<" + nomBalise + ">");
    boolean isVersion111 = Crue10VersionConfig.V_1_1_1.equals(coeurConfigContrat.getXsdVersion());
    for (int j = 0, jmax = listeFichiers.size(); j < jmax; j++) {
      //ignoré en version 1.1.1
      if (isVersion111 && CrueFileType.OPTR.equals(listeFichiers.get(j).getType())) {
        continue;
      }

      writeIncludeFichier(files.get(listeFichiers.get(j).getNom()));
    }
    this.writelnToOut("</" + nomBalise + ">");
  }

  protected static class InputFormatWriter {
    protected String version;
    protected ManagerEMHScenario scenario;
    protected CrueConfigMetier crueProps;
    protected Map<String, URL> idFile;
  }
}
