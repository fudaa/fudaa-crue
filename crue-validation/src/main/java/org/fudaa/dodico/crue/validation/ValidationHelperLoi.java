package org.fudaa.dodico.crue.validation;

import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.config.ccm.PropertyValidator;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.LoiDF;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.emh.Point2D;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;

/**
 *
 * @author deniger
 */
public class ValidationHelperLoi {

  public static String getIndexString(final int idx) {
    if (idx < 0) {
      return StringUtils.EMPTY;
    }
    return Integer.toString(idx + 1);
  }

  public static CtuluLog valideLoi(final Loi in, final CrueConfigMetier props) {
    final CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    valideLoi(in, props, res, null);
    return res;

  }

  protected static void valideLoi(final Loi in, final CrueConfigMetier ccm, final CtuluLog res, final String pref) {
    final List<PtEvolutionFF> ptEvolutionFF = in.getEvolutionFF().getPtEvolutionFF();
    final int size = ptEvolutionFF.size();
    ccm.getValidator("nbrPtLoi").validateNumber(BusinessMessages.getString("valid.nbrPtEvolutionFF"), size, res);
    if (size == 0) {
      res.addError("valid.nbrPtEvolutionFF.IsEmpty", in.getNom());
    } else if (LoiDF.class.equals(in.getClass())) {
      double firstAbs = ptEvolutionFF.get(0).getAbscisse();
      PropertyEpsilon loiAbscisseEps = ccm.getLoiAbscisseEps(in);
      if (!loiAbscisseEps.isZero(firstAbs)) {
        res.addError("valid.firstAbsLoiDf.mustBeZero", in.getNom());
      }
    }

    final PropertyValidator abscisseValidator = ccm.getLoiAbscisseValidator(in);
    validateListPt2d(ptEvolutionFF, abscisseValidator, ccm.getLoiOrdonneeValidator(in), res, pref);
    ConfigLoi configLoi = ccm.getConfLoi().get(in.getType());
    validControlLoi(configLoi, ccm, in, pref, ptEvolutionFF, res);
  }

  public static void validControlLoi(ConfigLoi configLoi, final CrueConfigMetier props, final Loi in, final String pref,
          final List<PtEvolutionFF> ptEvolutionFF, final CtuluLog res) {
    if (configLoi != null && configLoi.getControleLoi() != null) {
      PropertyEpsilon loiAbscisseEps = props.getLoiAbscisseEps(in);
      if (loiAbscisseEps == null) {
        loiAbscisseEps = PropertyEpsilon.DEFAULT;
      }
      if (configLoi.getControleLoi().isIncreasing()) {
        validIncreasing(StringUtils.defaultString(pref, in.getNom()), ptEvolutionFF, loiAbscisseEps, res, false);

      } else if (configLoi.getControleLoi().isStricltyIncreasing()) {
        validIncreasing(StringUtils.defaultString(pref, in.getNom()), ptEvolutionFF, loiAbscisseEps, res, true);
      }
    }
  }

  static void validateListPt2d(final List<? extends Point2D> pts, final PropertyValidator abs,
          final PropertyValidator ord, final CtuluLog res, final String pref) {
    final int max = pts.size();
    for (int i = 0; i < max; i++) {
      final Point2D pt = pts.get(i);
      valideAbsOrOrd(abs, res, pt.getAbscisse(), true, i, pref);
      valideAbsOrOrd(ord, res, pt.getOrdonnee(), false, i, pref);

    }

  }

  public static CtuluLog validate(PtEvolutionFF evol, ConfigLoi config) {
    CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    valideAbsOrOrd(config.getVarAbscisse().getValidator(), res, evol.getAbscisse(), true, -1, null);
    valideAbsOrOrd(config.getVarOrdonnee().getValidator(), res, evol.getOrdonnee(), false, -1, null);
    return res;
  }

  public static void valideAbsOrOrd(final PropertyValidator abs, final CtuluLog res, final double val,
          final boolean isAbs, final int idx, final String pref) {
    if (abs == null) {
      return;
    }
    String error = abs.getValidateErrorForValue(val);
    if (error != null) {
      final String msg = (isAbs) ? "validation.abs.NotValid" : "validation.ord.NotValid";
      res.addError((pref == null ? StringUtils.EMPTY : pref + " ") + BusinessMessages.getString(msg, getIndexString(idx)) + " "
              + error);
    } else {
      error = abs.getNormaliteWarningForValue(val);
      if (error != null) {
        final String msg = (isAbs) ? "validation.abs.NotNormal" : "validation.ord.NotNormal";
        res.addWarn((pref == null ? StringUtils.EMPTY : pref + " ") + BusinessMessages.getString(msg, getIndexString(idx)) + " "
                + error);
      }
    }
  }

  public static void validateDonFrt(final DonFrt cont, final CrueConfigMetier props, final CtuluLog res,
          final String prefix) {
    valideLoi(cont.getLoi(), props, res, prefix);
  }

  public static CtuluLog validateLoiDF(final LoiDF loi, final CrueConfigMetier ccm) {
    final CtuluLog res = new CtuluLog();
    res.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    res.setDesc(loi.getNom());
    valideLoi(loi, ccm, res, null);

    return res;
  }

  protected static CtuluLog validateLoiFF(final LoiFF loi, final CrueConfigMetier validator) {
    final CtuluLog res = new CtuluLog();
    res.setDesc(loi.getNom());
    res.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    valideLoi(loi, validator, res, null);
    return res;
  }

  public static void validIncreasing(String nom, List<? extends Point2D> ptEvolutionFF, PropertyEpsilon epsilon,
          CtuluLog res,
          boolean strict) {
    if (CollectionUtils.isEmpty(ptEvolutionFF)) {
      return;
    }
    int size = ptEvolutionFF.size();
    double x = ptEvolutionFF.get(0).getAbscisse();
    boolean errorAddedInLog = false;//pour ne pas ajouter trop de log.
    for (int i = 1; i < size; i++) {
      double xUp = ptEvolutionFF.get(i).getAbscisse();
      boolean hasError = false;
      if (strict) {
        hasError = epsilon.isSame(x, xUp) || xUp <= x;
      } else {
        hasError = (!epsilon.isSame(x, xUp)) && xUp <= x;
      }
      if (hasError && !errorAddedInLog) {
        errorAddedInLog = true;
        res.addError(strict ? "valid.loi.notStrictlyIncreasing" : "valid.loi.notIncreasing", StringUtils.defaultString(nom),
                Integer.toString(i));
      }

      x = xUp;
    }

  }
}
