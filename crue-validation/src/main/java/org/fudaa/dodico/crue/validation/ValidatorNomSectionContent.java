package org.fudaa.dodico.crue.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;

/**
 * Valide le nom des profil sections
 * @author deniger
 */
public class ValidatorNomSectionContent implements CrueValidator {

  @Override
  public List<CtuluLog> getLogs() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  public void validSection(CatEMHSection section, CtuluLog error) {
    if (section.getSectionType().equals(EnumSectionType.EMHSectionProfil)) {
      DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection((EMHSectionProfil) section);
      if (profilSection != null) {
        String expectedName = CruePrefix.changePrefix(section.getNom(), CruePrefix.P_SECTION, CruePrefix.P_PROFIL_SECTION);
        if (!expectedName.equals(profilSection.getNom())) {
          error.addSevereError("validation.sectionCorrespondance.ProfilNameNotCorresponding", profilSection.getNom(), section.getNom(), expectedName);
        }
      }

    }
  }

  @Override
  public void validate(Set<Object> childs, EMH emh, EMHModeleBase modeleBase) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public List<CtuluLog> validateScenario(EMHScenario scenario) {
    List<CtuluLog> res = new ArrayList<>();
    List<EMHSousModele> sousModeles = scenario.getSousModeles();
    for (EMHSousModele sousModele : sousModeles) {
      CtuluLog log = validateSousModele(sousModele);
      if (log != null) {
        res.add(log);
      }
    }
    return res;
  }

  private CtuluLog validateSousModele(EMHSousModele sousModele) {
    CtuluLog error = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    error.setDesc("validation.sectionCorrespondance");
    error.setDescriptionArgs(sousModele.getNom());
    List<CatEMHSection> sections = sousModele.getSections();
    for (CatEMHSection section : sections) {
      validSection(section, error);
    }

    if (error.isNotEmpty()) {
      return error;
    }
    return null;

  }
}
