package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.io.test.ReadHelperForTest;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

/**
 * Classe test de validation du modele.
 *
 * @author Adrien Hadoux
 */
public class CrueValidationConnexiteTest extends AbstractTestParent {
  /**
   * constructeur lambda.
   */
  public CrueValidationConnexiteTest() {

  }

  @Test
  public void testEtuDM() {
      CtuluLog log = new CtuluLog();
    final CrueIOResu<CrueData> readModele = new ReadHelperForTest(TestCoeurConfig.INSTANCE).readModele(log, "/CrueValidationConnexiteTest/Test_c9cor.dc", "/CrueValidationConnexiteTest/Test_c9cor.dh");
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    Assert.assertFalse(log.containsSevereError());
    log = ValidateConnectionModele.validateConnexite(readModele.getMetier().getSousModele());
    Assert.assertFalse(log.containsErrorOrSevereError());
  }

  /**
   * @param path le path du fichier
   * @param resAttendu le resultat attendu par la validation
   */
  private void testCrueValidationModeleGENERIQUE(final String path, final boolean resAttendu) {
    CtuluLog analyzer = new CtuluLog();
    final CrueData data = (CrueData) Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DRSO,
        TestCoeurConfig.INSTANCE).read(path, analyzer,new CrueDataImpl(CrueConfigMetierForTest.DEFAULT)).getMetier();
    testAnalyser(analyzer);

    analyzer = ValidateConnectionModele.validateConnexite(data.getSousModele());
    Assert.assertEquals(resAttendu, !analyzer.containsErrorOrSevereError());
  }

  /**
   * @param path du fichier
   */
  private void testCrueValidationModeleGENERIQUE(final String path) {
    testCrueValidationModeleGENERIQUE(path, true);
  }

  /**
   *
   */
  @Test
  public void testCrueValidationModele3() {
    testCrueValidationModeleGENERIQUE("/M3-0_c10.drso.xml");
  }
}
