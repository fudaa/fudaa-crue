/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.io.rcal.CrueDaoRCAL;
import org.fudaa.dodico.crue.io.res.ResCatEMHContent;
import org.fudaa.dodico.crue.io.res.ResTypeEMHBuilder;
import org.fudaa.dodico.crue.io.rptg.CrueDaoRPTG;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Test;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author deniger
 */
public class ValidatorResultatCrue10DescriptorXmlTest {
    private static final String FICHIER_TEST_RCAL_XML = "/rcal/M3-0_c10.rcal.xml";
    private static final String FICHIER_TEST_RPTG_XML = "/rcal/M3-0_c10.rptg.xml";
    private static final String FICHIER_TEST_RPTG_NBRLOI_ENTROP_XML = "/rcal/M3-0_c10_nbrLoiEnTrop.rptg.xml";
    private static final String FICHIER_TEST_RPTG_NBRLOI_MANQUANT_XML = "/rcal/M3-0_c10_nbrLoiManquant.rptg.xml";

    public ValidatorResultatCrue10DescriptorXmlTest() {
    }

    public static CrueDaoRCAL readData(final String fichier, final CoeurConfigContrat version) {
        final CtuluLog analyzer = new CtuluLog();
        final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.RCAL, version);
        final CrueDaoRCAL jeuDonneesLue = (CrueDaoRCAL) fileFormat.read(fichier, analyzer, new CrueDataImpl(CrueConfigMetierForTest.DEFAULT)).getMetier();
        AbstractTestParent.testAnalyser(analyzer);

        return jeuDonneesLue;
    }

    private static CrueDaoRPTG readDataRPTG(final String fichier, final CoeurConfigContrat version) {
        final CtuluLog analyzer = new CtuluLog();
        final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.RPTG, version);
        final CrueDaoRPTG jeuDonneesLue = (CrueDaoRPTG) fileFormat.read(fichier, analyzer, new CrueDataImpl(CrueConfigMetierForTest.DEFAULT)).getMetier();

        AbstractTestParent.testAnalyser(analyzer);

        return jeuDonneesLue;
    }

    @Test
    public void testRcalNoError() {
        final List<ResCatEMHContent> metier = readDataRCAL(FICHIER_TEST_RCAL_XML);

        final ValidatorResultatCrue10VariablesDescriptor v = new ValidatorResultatCrue10VariablesDescriptor();
        final CtuluLog validResLoi = v.validVariables(metier, TestCoeurConfig.INSTANCE.getCrueConfigMetier());
        if (validResLoi.isNotEmpty()) {
            validResLoi.printResume();
        }
        assertTrue(validResLoi.isEmpty());
    }

    private List<ResCatEMHContent> readDataRCAL(final String file) {
        final CrueDaoRCAL readData = readData(file, TestCoeurConfig.INSTANCE);
        assertNotNull(readData);
        final ResTypeEMHBuilder builder = new ResTypeEMHBuilder(readData);
        final CrueIOResu<List<ResCatEMHContent>> extract = builder.extract();
        AbstractTestParent.testAnalyser(extract.getAnalyse());
        final List<ResCatEMHContent> metier = extract.getMetier();
        assertNotNull(metier);
        return metier;
    }

    private List<ResCatEMHContent> readDataRPTG(final String file) {
        final CrueDaoRPTG readData = readDataRPTG(file, TestCoeurConfig.INSTANCE);
        assertNotNull(readData);
        final ResTypeEMHBuilder builder = new ResTypeEMHBuilder(readData);
        final CrueIOResu<List<ResCatEMHContent>> extract = builder.extract();
        AbstractTestParent.testAnalyser(extract.getAnalyse());
        final List<ResCatEMHContent> metier = extract.getMetier();
        assertNotNull(metier);
        return metier;
    }

    @Test
    public void testRptgNoError() {
        final List<ResCatEMHContent> metier = readDataRPTG(FICHIER_TEST_RPTG_XML);
        final ValidatorResultatCrue10VariablesDescriptor v = new ValidatorResultatCrue10VariablesDescriptor();
        final CtuluLog validResLoi = v.validVariables(metier, TestCoeurConfig.INSTANCE.getCrueConfigMetier());
        assertTrue(validResLoi.isEmpty());
    }

    @Test
    public void testRptgNbrLoiEnTrop() {
        final List<ResCatEMHContent> metier = readDataRPTG(FICHIER_TEST_RPTG_NBRLOI_ENTROP_XML);
        final ValidatorResultatCrue10VariablesDescriptor v = new ValidatorResultatCrue10VariablesDescriptor();
        final CtuluLog validResLoi = v.validVariables(metier, TestCoeurConfig.INSTANCE.getCrueConfigMetier());
        assertFalse(validResLoi.isEmpty());
        final Collection<CtuluLogRecord> records = validResLoi.getRecords();
        assertEquals(2, records.size());
        final Iterator<CtuluLogRecord> iterator = records.iterator();
        CtuluLogRecord next = iterator.next();
        assertEquals("res.validate.VariableResLoiNbrPtDao.NotUsed", next.getMsg());
        next = iterator.next();
        assertEquals("res.validate.VariableResLoiNbrPtDaoInEMH.NotUsed", next.getMsg());
        final Object[] args = next.getArgs();
        assertEquals(2, args.length);
        assertEquals("Ca_N6", args[0]);
        assertEquals("DansEMHEnTrop", args[1]);
    }

    @Test
    public void testRptgNbrLoiManquant() {
        final List<ResCatEMHContent> metier = readDataRPTG(FICHIER_TEST_RPTG_NBRLOI_MANQUANT_XML);
        final ValidatorResultatCrue10VariablesDescriptor v = new ValidatorResultatCrue10VariablesDescriptor();
        final CtuluLog validResLoi = v.validVariables(metier, TestCoeurConfig.INSTANCE.getCrueConfigMetier());
        assertFalse(validResLoi.isEmpty());
        final Collection<CtuluLogRecord> records = validResLoi.getRecords();
        assertEquals(2, records.size());
        final Iterator<CtuluLogRecord> iterator = records.iterator();
        CtuluLogRecord next = iterator.next();
        assertEquals("res.validate.VariableResLoiNbrPtDaoInEMH.NotDefined", next.getMsg());
        Object[] args = next.getArgs();
        assertEquals(2, args.length);
        assertEquals("Ca_N6", args[0]);
        assertEquals("LoiZSplan", args[1]);
        next = iterator.next();
        assertEquals("res.validate.VariableResLoiNbrPtDaoInEMH.NotDefined", next.getMsg());
        args = next.getArgs();
        assertEquals(2, args.length);
        assertEquals("St_PROF3AM", args[0]);
        assertEquals("LstLitKsup", args[1]);
    }
}
