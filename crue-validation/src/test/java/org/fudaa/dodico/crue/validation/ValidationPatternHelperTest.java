package org.fudaa.dodico.crue.validation;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test de ValidationPatternHelper
 *
 * @author deniger
 */
public class ValidationPatternHelperTest {

  @Test
  public void testWrongFileName() {
    Assert.assertFalse(ValidationPatternHelper.isFilenameValide(null, CrueFileType.ETU));
    Assert.assertFalse(ValidationPatternHelper.isFilenameValide(" ", CrueFileType.ETU));
    Assert.assertFalse(ValidationPatternHelper.isFilenameValide("toto", CrueFileType.ETU));
    Assert.assertFalse(ValidationPatternHelper.isFilenameValide("toto.etu", CrueFileType.ETU));
    Assert.assertFalse(ValidationPatternHelper.isFilenameValide("toto.dd.etu.xml", CrueFileType.ETU));
    Assert.assertFalse(ValidationPatternHelper.isFilenameValide("to/to.dd.etu.xml", CrueFileType.ETU));
    Assert.assertFalse(ValidationPatternHelper.isFilenameValide("to'to.dd.etu.xml", CrueFileType.ETU));
    Assert.assertFalse(ValidationPatternHelper.isFilenameValide("to?to.dd.etu.xml", CrueFileType.ETU));
    Assert.assertFalse(ValidationPatternHelper.isFilenameValide("to#to.etu.xml", CrueFileType.ETU));
  }

  @Test
  public void testIsRadicalValideForEMHContainerMsg(){
    Assert.assertNull(ValidationPatternHelper.isRadicalValideForEMHContainerMsg("Test"));
    Assert.assertEquals("validation.name.pattern.error", ValidationPatternHelper.isRadicalValideForEMHContainerMsg("1est"));
    Assert.assertEquals("toolong", ValidationPatternHelper.isRadicalValideForEMHContainerMsg("test", 6, "toolong"));
  }

  @Test
  public void testCorrectRapportName() {
    Assert.assertTrue(ValidationPatternHelper.isNameValid("toto toto", "", ValidationPatternHelper.getRapportNamePattern()));
    Assert.assertTrue(ValidationPatternHelper.isNameValid("toto tétè", "", ValidationPatternHelper.getRapportNamePattern()));
  }

  @Test
  public void testCorrectFileName() {
    Assert.assertTrue(ValidationPatternHelper.isFilenameValide("toto.etu.xml", CrueFileType.ETU));
    Assert.assertTrue(ValidationPatternHelper.isFilenameValide("etu.etu.xml", CrueFileType.ETU));
    Assert.assertTrue(ValidationPatternHelper.isFilenameValide("Etu124-34.etu.xml", CrueFileType.ETU));
  }

  @Test
  public void testCorrectEmhName() {
    Assert.assertTrue(ValidationPatternHelper.isRadicalValideForEMHContainer("a"));
    Assert.assertTrue(ValidationPatternHelper.isNameValide("Sc_FF_DD", EnumCatEMH.SCENARIO));
    Assert.assertTrue(ValidationPatternHelper.isNameValide("Mo_MO_Mo", EnumCatEMH.MODELE));
    Assert.assertTrue(ValidationPatternHelper.isNameValide("Mo_Modele.Q", EnumCatEMH.MODELE));
    Assert.assertTrue(ValidationPatternHelper.isNameValide("St_Test.-Test123", EnumCatEMH.SECTION));
  }
  @Test
  public void testCorrectRunName() {
    Assert.assertNull(ValidationPatternHelper.isRunNameValidei18nMessage("aaaa"));
    Assert.assertNull(ValidationPatternHelper.isRunNameValidei18nMessage("aa"));
  }
  @Test
  public void testWrongRunName() {
    Assert.assertNotNull(ValidationPatternHelper.isRunNameValidei18nMessage(""));
    Assert.assertNotNull(ValidationPatternHelper.isRunNameValidei18nMessage("éà"));
    Assert.assertNotNull(ValidationPatternHelper.isRunNameValidei18nMessage(StringUtils.repeat("a",33)));
  }

  @Test
  public void testWrongEmhName() {
    Assert.assertFalse(ValidationPatternHelper.isRadicalValideForEMHContainer("1"));
    Assert.assertFalse(ValidationPatternHelper.isNameValide("Sc_1", EnumCatEMH.SCENARIO));
    Assert.assertFalse(ValidationPatternHelper.isNameValide("Sc_#", EnumCatEMH.SCENARIO));
    Assert.assertFalse(ValidationPatternHelper.isNameValide("Sc_*", EnumCatEMH.SCENARIO));
    Assert.assertFalse(ValidationPatternHelper.isNameValide("Sc_", EnumCatEMH.SCENARIO));
    Assert.assertFalse(ValidationPatternHelper.isNameValide("Mo_MO_Mo", EnumCatEMH.SOUS_MODELE));
  }
}
