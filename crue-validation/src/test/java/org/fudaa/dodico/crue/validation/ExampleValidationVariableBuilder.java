/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.validation;

import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.ParamNumCalcPseudoPerm;
import org.fudaa.dodico.crue.metier.emh.ParamNumModeleBase;

/**
 * @author deniger
 */
public class ExampleValidationVariableBuilder {

  public static List<EMH> buildEMHs() {
    final ParamNumModeleBase num = new ParamNumModeleBase(CrueConfigMetierForTest.DEFAULT);
    final ParamNumCalcPseudoPerm paramNumCalcPseudoPerm = new ParamNumCalcPseudoPerm(
        CrueConfigMetierForTest.DEFAULT);
    paramNumCalcPseudoPerm.setCrMaxFlu(399);
    num.setParamNumCalcPseudoPerm(paramNumCalcPseudoPerm);
    num.setFrLinInf(-1);
    num.setFrLinSup(2);
    final EMH modele = new EMHModeleBase();
    modele.addInfosEMH(num);
    final List<EMH> asList = Collections.singletonList(modele);
    return asList;
  }

  public static void main(final String[] args) {
    final List<EMH> asList = buildEMHs();
    new VariableBuilder().build(asList);
  }

}
