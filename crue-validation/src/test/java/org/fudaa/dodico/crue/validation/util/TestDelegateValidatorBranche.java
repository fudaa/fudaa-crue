package org.fudaa.dodico.crue.validation.util;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.comparator.ComparatorRelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.emh.EMHSectionIdem;
import org.fudaa.dodico.crue.metier.emh.EnumPosSection;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestDelegateValidatorBranche {

  @Test
  public void testvalidSectionsDisposition() {
    final DelegateValidatorBranche validator = new DelegateValidatorBranche(CrueConfigMetierForTest.DEFAULT);
    final CtuluLog log = new CtuluLog();
    final EMHBrancheSaintVenant branche = new EMHBrancheSaintVenant("test");
    final ComparatorRelationEMHSectionDansBranche comparator = new ComparatorRelationEMHSectionDansBranche(
            CrueConfigMetierForTest.DEFAULT);
    final List<RelationEMHSectionDansBranche> sections = new ArrayList<>();
    validator.validSectionsDisposition(log, branche, sections, comparator);
    // TOUS: validation.brancheMustContain2Sections
    // TOUS: validation.branche.mustContainAtLeastOneSectionAmont
    // TOUS: validation.branche.mustContainAtLeastOneSectionAval
    //validation.branche.distanceMustBeNull
    Assert.assertEquals(4, log.getNbOccurence(CtuluLogLevel.SEVERE));
    final RelationEMHSectionDansBranche amont = create(0, EnumPosSection.AVAL);
    sections.add(amont);

    // TOUS: validation.brancheMustContain2Sections
    // TOUS: validation.branche.mustContainAtLeastOneSectionAmont
    // TOUS: validation.branche.firstSectionMustBeAmont
    log.clear();
    validator.validSectionsDisposition(log, branche, sections, comparator);
    Assert.assertEquals(4, log.getNbOccurence(CtuluLogLevel.SEVERE));

    amont.setPos(EnumPosSection.AMONT);
    final RelationEMHSectionDansBranche aval = create(0, EnumPosSection.INTERNE);
    sections.add(aval);

    // TOUS: validation.branche.mustContainAtLeastOneSectionAval
    // TOUS: validation.branche.lastSectionMustBeAval
    log.clear();
    validator.validSectionsDisposition(log, branche, sections, comparator);
    Assert.assertEquals(2, log.getNbOccurence(CtuluLogLevel.SEVERE));

    // branche vide doit etre ok
    aval.setPos(EnumPosSection.AVAL);
    log.clear();
    validator.validSectionsDisposition(log, branche, sections, comparator);
    Assert.assertTrue(log.isEmpty());

    sections.remove(aval);

    final RelationEMHSectionDansBranche middle1 = create(0.2, EnumPosSection.INTERNE);
    final RelationEMHSectionDansBranche middle2 = create(0.3, EnumPosSection.INTERNE);
    sections.add(middle1);
    sections.add(middle2);
    sections.add(aval);
    log.clear();
    // TOUS: validation.branche.lastSectionMustBeAval
    // TOUS: validation.section.severalSectionsWithSameXp
    validator.validSectionsDisposition(log, branche, sections, comparator);
    Assert.assertEquals(3, log.getNbOccurence(CtuluLogLevel.SEVERE));

    // aval est correct
    aval.setXp(0.4);
    log.clear();
    validator.validSectionsDisposition(log, branche, sections, comparator);

    Assert.assertTrue(log.isEmpty());

    // middle1 et amont = meme xp erreur
    // TOUS: validation.section.severalSectionsWithSameXp

    middle1.setXp(0);
    log.clear();
    validator.validSectionsDisposition(log, branche, sections, comparator);
    Assert.assertEquals(1, log.getNbOccurence(CtuluLogLevel.SEVERE));

  }

  private RelationEMHSectionDansBranche create(final double xp, final EnumPosSection pos) {
    final RelationEMHSectionDansBranche res = new RelationEMHSectionDansBranche();
    res.setPos(pos);
    res.setXp(xp);
    res.setEmh(new EMHSectionIdem("idem"));
    return res;
  }
}
