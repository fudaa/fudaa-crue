package org.fudaa.dodico.crue.validation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.emh.Dde;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.OrdResBrancheBarrageFilEau;
import org.fudaa.dodico.crue.metier.emh.OrdResScenario;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class ValidatorORESTest {

  final TestCoeurConfig coeurConfig = TestCoeurConfig.INSTANCE;

  @Test
  public void testOresWillOnlyNullValues() {
    ScenarioAutoModifiedState state = new ScenarioAutoModifiedState();
    ValidatorORES validator = new ValidatorORES(coeurConfig, state);
    OrdResScenario allNull = new OrdResScenario();
    EMHScenario scenario = new EMHScenario();
    scenario.addInfosEMH(allNull);
    List<CtuluLog> validateScenario = validator.validateScenario(scenario);
    CtuluLog log = validateScenario.get(0);
    assertTrue(log.containsSevereError());
    assertEquals(13, log.getRecords().size());
    assertEquals("validation.ores.oresIsNullAndNotReference.error", log.getRecords().get(0).getMsg());
  }

  @Test
  public void testIsAllDdesOk() {
    ScenarioAutoModifiedState state = new ScenarioAutoModifiedState();
    ValidatorORES validator = new ValidatorORES(coeurConfig, state);
    assertFalse(state.isOrdResModified());
  }

  private OrdResScenario loadDefault() {
    final Crue10FileFormat<OrdResScenario> format = Crue10FileFormatFactory.getVersion(CrueFileType.ORES, coeurConfig);
    CrueIOResu<OrdResScenario> read = format.read(coeurConfig.getDefaultFile(CrueFileType.ORES), new CtuluLog(), null);
    assertNotNull(read.getMetier());
    return read.getMetier();
  }

  @Test
  public void testInRefButNotInEtude() {
    ScenarioAutoModifiedState state = new ScenarioAutoModifiedState();
    ValidatorORES validator = new ValidatorORES(coeurConfig, state);
    OrdResScenario other = loadDefault();
    EMHScenario scenario = new EMHScenario();
    scenario.addInfosEMH(other);
    Collection<Dde> expected = other.getOrdResBrancheBarrageFilEau().getDdes();
    other.getOrdResBrancheBarrageFilEau().setValues(Collections.emptyList());
    //la validation va ajouter à nouveau les variables.
    List<CtuluLog> validateScenario = validator.validateScenario(scenario);
    assertTrue(state.isOrdResModified());
    CtuluLog log = validateScenario.get(0);
    assertFalse(log.containsSevereError());
    List<Dde> rebuild = other.getOrdResBrancheBarrageFilEau().getDdes();
    assertTrue(expected.equals(rebuild));

  }

  @Test
  public void testInEtudeButNotInRef() {
    ScenarioAutoModifiedState state = new ScenarioAutoModifiedState();
    ValidatorORES validator = new ValidatorORES(coeurConfig, state);
    OrdResScenario other = loadDefault();
    EMHScenario scenario = new EMHScenario();
    scenario.addInfosEMH(other);
    final OrdResBrancheBarrageFilEau ordRes = other.getOrdResBrancheBarrageFilEau();
    final String name = ordRes.getDdes().get(0).getNom();
    ordRes.setDdeValue(name, true);
    List<Dde> initial = new ArrayList<>(ordRes.getDdes());
    List<Dde> modified = new ArrayList<>(ordRes.getDdes());
    modified.add(new Dde("test", true));
    ordRes.setValues(modified);
    //la validation va enlever la variable inconnue:
    List<CtuluLog> validateScenario = validator.validateScenario(scenario);
    assertTrue(state.isOrdResModified());
    CtuluLog log = validateScenario.get(0);
    assertFalse(log.containsSevereError());
    assertTrue(initial.equals(ordRes.getDdes()));


  }
}
