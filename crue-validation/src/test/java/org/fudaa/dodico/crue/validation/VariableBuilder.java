package org.fudaa.dodico.crue.validation;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.PropertyUtilsCache;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.InfosEMH;
import org.fudaa.dodico.crue.metier.emh.ParamNumModeleBase;

/**
 * Construit les variables utilisés par le fichier de validation.
 * 
 * @author deniger
 */
@Deprecated //unused
public class VariableBuilder {

  private static String getPropName(final String init) {
    if (init == null) {
      return null;
    }
    if (init.indexOf('.') < 0) {
      return init;
    }
    final String[] props = StringUtils.split(init, '.');
    return props[props.length - 1];
  }

  private final Map<Class, List<String>> classProps = new HashMap<>();

  private final Set<String> props = new HashSet<>(10);

  public VariableBuilder() {
    final List<String> propsByClass = Arrays.asList("frLinInf", "frLinSup", "paramNumCalcPseudoPerm.crMaxFlu");
    classProps.put(ParamNumModeleBase.class, propsByClass);
    for (final List<String> params : classProps.values()) {
      for (final String prop : params) {
        props.add(getPropName(prop));
      }
    }
  }

  private final static Logger LOGGER = Logger.getLogger(VariableBuilder.class.getName());

  private void fillLimitProperties(final Object o, final Map<String, Number> values) {
    final List<String> props = classProps.get(o.getClass());
    if (props != null) {
      for (final String prop : props) {
        try {

          final Object value = PropertyUtilsCache.getInstance().getProperty(o, prop);
          values.put(getPropName(prop), (Number) value);
        } catch (final Exception e) {
          LOGGER.log(Level.SEVERE, "fillLimitProperties " + prop, e);
        }
      }
    }
  }

  public Map<String, Number> build(final List<? extends EMH> all) {
    final Map<String, Number> values = new HashMap<>();
    for (final EMH emh : all) {
      fillLimitProperties(emh, values);
      for (final InfosEMH info : emh.getInfosEMH()) {
        fillLimitProperties(info, values);
      }
    }
    return values;
  }

  /**
   * @param s le nome de la variable officiel: commencant par une majuscule
   * @return true si c'est une variable
   */
  public boolean isVariable(final String s) {
    return props.contains(s);
  }

}
