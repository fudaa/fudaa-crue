package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ValidatorForCrue9ExportTest {


  @Test
  public void testConvertToCrue9ForNewDclm() throws IOException {
    CrueData data = readData("/16_DM2013.dclm.xml", "/16_DM2013.drso.xml", "/16_DM2013.dlhy.xml", TestCoeurConfig.INSTANCE);
    EMHScenario scenarioData = data.getScenarioData();
    CalcTrans calcTrans = scenarioData.getDonCLimMScenario().getCalcTrans().get(1);
    Assert.assertEquals(CalcTransNoeudBg2Av.class, calcTrans.getlisteDCLM().get(7).getClass());

    ValidatorForCrue9Export validator = new ValidatorForCrue9Export(TestCoeurConfig.INSTANCE.getCrueConfigMetier(), true);
    Set<Object> toValide=new HashSet<>();
    toValide.add(scenarioData.getDonCLimMScenario());
    OrdCalcScenario ordCalcScenario = new OrdCalcScenario();
    scenarioData.addInfosEMH(ordCalcScenario);

    validator.validate(toValide,scenarioData,null);
    List<CtuluLog> logs = validator.getLogs();
    Assert.assertNotNull(logs);
    Assert.assertEquals(1,logs.size());
    CtuluLog ctuluLog = logs.get(0);
    ctuluLog.updateLocalizedMessage(BusinessMessages.RESOURCE_BUNDLE);
    Assert.assertEquals(5,ctuluLog.getRecords().size());
    Assert.assertEquals("crue9.compatible.dclmNotSupported",ctuluLog.getRecords().get(0).getMsg());
    Assert.assertEquals("CalcTransBrancheOrificeManoeuvreRegul",ctuluLog.getRecords().get(0).getArgs()[0]);
    Assert.assertEquals("crue9.compatible.dclmNotSupported",ctuluLog.getRecords().get(1).getMsg());
    Assert.assertEquals("CalcTransNoeudBg1",ctuluLog.getRecords().get(1).getArgs()[0]);
    Assert.assertEquals("crue9.compatible.dclmNotSupported",ctuluLog.getRecords().get(2).getMsg());
    Assert.assertEquals("CalcTransNoeudUsine",ctuluLog.getRecords().get(2).getArgs()[0]);
    Assert.assertEquals("crue9.compatible.dclmNotSupported",ctuluLog.getRecords().get(3).getMsg());
    Assert.assertEquals("CalcTransNoeudBg2",ctuluLog.getRecords().get(3).getArgs()[0]);
    Assert.assertEquals("crue9.compatible.dclmNotSupported",ctuluLog.getRecords().get(4).getMsg());
    Assert.assertEquals("CalcTransNoeudBg2Av",ctuluLog.getRecords().get(4).getArgs()[0]);


  }


  public static CrueData readData(String dclmFile, String drsoFile, String dlhyFile, CoeurConfigContrat version) {
    final CtuluLog analyzer = new CtuluLog();
    // -- lecture drso --//
    final CrueData data = (CrueData) Crue10FileFormatFactory.getVersion(CrueFileType.DRSO, version)
        .read(drsoFile, analyzer, new CrueDataImpl(version.getCrueConfigMetier())).getMetier();

    // -- lecture dlhy --//
    final List<Loi> listeLois = (List<Loi>) Crue10FileFormatFactory.getVersion(CrueFileType.DLHY, version)
        .read(dlhyFile, analyzer, data).getMetier();
    Assert.assertNotNull(listeLois);

    // -- lecture de dclm --//
    Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DCLM, version).read(dclmFile, analyzer, data)
        .getMetier();
    return data;
  }
}
