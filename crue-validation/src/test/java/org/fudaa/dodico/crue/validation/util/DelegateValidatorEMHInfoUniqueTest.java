/*
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.crue.validation.util;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author deniger
 */
public class DelegateValidatorEMHInfoUniqueTest extends AbstractTestParent {

  @Test
  public void testValidatorInfoEMH() {
    final CtuluLog analyzer = new CtuluLog();
    final CrueData data = (CrueData) Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DRSO, TestCoeurConfig.INSTANCE)
        .read("/ValidateModeleScenarioTest/Modele3-3-erreur.drso.xml", analyzer,new CrueDataImpl(CrueConfigMetierForTest.DEFAULT)).getMetier();
    testAnalyser(analyzer);
    final EMHScenario scenario = data.getScenarioData();
    EMHRelationFactory.addRelationContientEMH(scenario, data.getModele());
    EMHRelationFactory.addRelationContientEMH(data.getModele(), data.getSousModele());
    final CtuluLog valid = new DelegateValidatorEMHInfoUnique().valid(scenario);
    Assert.assertTrue(valid.containsErrorOrSevereError());
  }
}
