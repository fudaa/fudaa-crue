package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.test.ReadHelperForTest;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EnumInfosEMH;
import org.fudaa.dodico.crue.metier.emh.InfosEMH;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe test de validation du modele.
 *
 * @author Deniger
 */
public class CrueValidationContentTest extends AbstractTestParent {
  /**
   * constructeur lambda.
   */
  public CrueValidationContentTest() {
  }

  @Test
  public void testValidationEtuBYCor() {
    final CtuluLog analyzer = new CtuluLog();
    final CrueIOResu<CrueData> ioResu = new ReadHelperForTest(TestCoeurConfig.INSTANCE).readModeleAndDefaultORES(analyzer, "/CrueValidationContentTest/Test_c9cor.dc",
        "/CrueValidationContentTest/Test_c9cor.dh");
    final CrueData data = ioResu.getMetier();
    final EMHScenario scenarioData = data.getScenarioData();
    final List<InfosEMH> collectInfoEMH = EMHHelper.collectInfoEMH(data.getSousModele(), EnumInfosEMH.DON_FRT_CONTENEUR);
    Assert.assertEquals(1, collectInfoEMH.size());
    scenarioData.setNom("Test");
    final CtuluLogGroup doValidate = doValidate(scenarioData, data.getCrueConfigMetier());
    final int nbOccurence = doValidate.getNbOccurence(CtuluLogLevel.ERROR);
    Assert.assertEquals(44, nbOccurence);//TOUS: Aucun DonFrt affecté au LitNumeroté STOCKG + 38 profils sans lit mineur.
  }

  private CtuluLogGroup doValidate(final EMHScenario scenarioData, final CrueConfigMetier configMetier) {
    final CtuluLogGroup mng = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    final List<CrueValidator> validators = new ArrayList<>();
    validators.add(new ValidatorForValuesAndContents(configMetier));
    validators.add(new ValidateAndRebuildProfilSection(configMetier, null));
    ValidationHelper.validateAll(mng, scenarioData, validators);
    return mng;
  }
}
