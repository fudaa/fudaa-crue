/*
 GPL 2
 */
package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.io.test.ReadHelperForTest;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHNoeudFactory;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.fudaa.dodico.crue.validation.util.DelegateValidatorNoeud;
import org.fudaa.dodico.crue.validation.util.DelegateValidatorSection;
import org.fudaa.dodico.crue.validation.util.ValidationContentSectionDansBrancheExecutor;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * @author Frederic Deniger
 */
public class ValidatorForValuesAndContentsTest {
  private static final String FICHIER_ERRORS_DCLM_XML_V1_1_1 = "/ValidatorForValuesAndContentsTest/M3-0_c10_withErrors.dclm.xml";
  private static final String FICHIER_ERRORS_DLHY_XML_V1_1_1 = "/ValidatorForValuesAndContentsTest/M3-0_c10_withErrors.dlhy.xml";

  @Test
  public void testValidationSectionDansBranche() {
    final ValidatorForValuesAndContents contents = new ValidatorForValuesAndContents(CrueConfigMetierForTest.DEFAULT);
    final EMHBranchePdc pdc = createValidBranche();
    final CtuluLog log = new CtuluLog();

    Assert.assertFalse(log.containsErrorOrSevereError());
    final ValidationContentSectionDansBrancheExecutor executor = new ValidationContentSectionDansBrancheExecutor(CrueConfigMetierForTest.DEFAULT);
    executor.validContent(log, pdc, Arrays.asList(new EMHSectionSansGeometrie("Test_Am"), new EMHSectionSansGeometrie(
        "Test_Av")));
    contents.validateBranche(pdc, new EMHModeleBase(), log);
    Assert.assertFalse(log.containsErrorOrSevereError());
    executor.validContent(log, pdc, Arrays.asList(new EMHSectionSansGeometrie("Test_Am"), new EMHSectionSansGeometrie(
            "Test_Am"),
        new EMHSectionSansGeometrie("Test_Av")));
    Assert.assertTrue(log.containsErrorOrSevereError());
    Assert.assertEquals(1, log.getRecords().size());
    Assert.assertEquals("validation.branche.nbSectionNotEqualsTo", log.getRecords().iterator().next().getMsg());
    log.clear();
    executor.validContent(log, pdc, Arrays.asList(new EMHSectionIdem("titi"), new EMHSectionSansGeometrie("Test_Am"),
        new EMHSectionSansGeometrie(
            "Test_Am"), new EMHSectionSansGeometrie("Test_Av")));
    Assert.assertTrue(log.containsErrorOrSevereError());
    Assert.assertEquals(2, log.getRecords().size());
    final Iterator<CtuluLogRecord> iterator = log.getRecords().iterator();
    Assert.assertEquals("validation.branche.cantContainsThisSection", iterator.next().getMsg());
    Assert.assertEquals("validation.branche.nbSectionNotEqualsTo", iterator.next().getMsg());
  }

  private EMHBranchePdc createValidBranche() {
    final EMHScenario scenario = new EMHScenario();
    final EMHModeleBase modele = new EMHModeleBase();
    EMHRelationFactory.addRelationContientEMH(scenario, modele);
    final EMHSousModele sousModele = new EMHSousModele();
    EMHRelationFactory.addRelationContientEMH(modele, sousModele);
    final EMHBranchePdc pdc = new EMHBranchePdc("test");
    EMHRelationFactory.addRelationContientEMH(sousModele, pdc);
    pdc.setUserActive(true);
    final EMHSectionSansGeometrie amont = new EMHSectionSansGeometrie("Test_Am");
    final EMHSectionSansGeometrie aval = new EMHSectionSansGeometrie("Test_Av");
    pdc.addListeSections(Arrays.asList(
        EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(pdc, amont, 0, EnumPosSection.AMONT,
            CrueConfigMetierForTest.DEFAULT),
        EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(pdc, aval, 0, EnumPosSection.AVAL,
            CrueConfigMetierForTest.DEFAULT)));
    return pdc;
  }

  @Test
  public void testBrancheWithDpti() {
    final EMHBranchePdc pdc = createValidBranche();
    pdc.setUserActive(true);
    Assert.assertTrue(pdc.getActuallyActive());
    final EMHModeleBase modele = new EMHModeleBase();
    final OrdPrtCIniModeleBase opti = new OrdPrtCIniModeleBase(CrueConfigMetierForTest.DEFAULT);
    opti.setMethodeInterpol(EnumMethodeInterpol.INTERPOL_ZIMP_AUX_SECTIONS);
    modele.addInfosEMH(opti);
    final ValidatorForValuesAndContents contents = new ValidatorForValuesAndContents(CrueConfigMetierForTest.DEFAULT);
    CtuluLog log = new CtuluLog();
    contents.validateBranche(pdc, modele, log);
    Assert.assertTrue(log.isNotEmpty());
    final CtuluLogRecord get = log.getRecords().get(0);
    Assert.assertEquals("valid.branche.mustContainDPTI", get.getMsg());
    Assert.assertEquals("test", get.getArgs()[0]);
    Assert.assertEquals(EnumMethodeInterpol.INTERPOL_ZIMP_AUX_SECTIONS.name(), get.getArgs()[1]);
    pdc.addInfosEMH(new DonPrtCIniBranche(CrueConfigMetierForTest.DEFAULT));
    log = new CtuluLog();
    contents.validateBranche(pdc, modele, log);
    Assert.assertTrue(log.isEmpty());
  }

  @Test
  public void testSectionWithDpti() {
    final EMHBranchePdc pdc = createValidBranche();
    final CatEMHSection section = pdc.getSections().get(0).getEmh();
    section.setNom("test");
    Assert.assertTrue(section.getActuallyActive());
    final EMHModeleBase modele = new EMHModeleBase();
    final OrdPrtCIniModeleBase opti = new OrdPrtCIniModeleBase(CrueConfigMetierForTest.DEFAULT);
    opti.setMethodeInterpol(EnumMethodeInterpol.SAINT_VENANT);//cette méthode ne demande pas de DPTI aux sections
    modele.addInfosEMH(opti);
    CtuluLog log = new CtuluLog();
    DelegateValidatorSection.validateSection(log, section, modele);
    Assert.assertTrue(log.isEmpty());
    opti.setMethodeInterpol(EnumMethodeInterpol.LINEAIRE);
    DelegateValidatorSection.validateSection(log, section, modele);
    Assert.assertTrue(log.isEmpty());
    opti.setMethodeInterpol(EnumMethodeInterpol.BAIGNOIRE);
    DelegateValidatorSection.validateSection(log, section, modele);
    Assert.assertTrue(log.isEmpty());
    opti.setMethodeInterpol(EnumMethodeInterpol.INTERPOL_ZIMP_AUX_SECTIONS);//demande des dpti aux sections
    DelegateValidatorSection.validateSection(log, section, modele);
    Assert.assertFalse(log.isEmpty());
    final CtuluLogRecord get = log.getRecords().get(0);
    Assert.assertEquals("valid.section.mustContainDPTI", get.getMsg());
    Assert.assertEquals("test", get.getArgs()[0]);
    Assert.assertEquals(EnumMethodeInterpol.INTERPOL_ZIMP_AUX_SECTIONS.name(), get.getArgs()[1]);

    //on ajoute la donnée attendue:
    section.addInfosEMH(new DonPrtCIniSection(CrueConfigMetierForTest.DEFAULT));
    log = new CtuluLog();
    DelegateValidatorSection.validateSection(log, section, modele);
    Assert.assertTrue(log.isEmpty());
  }

  @Test
  public void testNoeudWithDpti() {
    final EMHBranchePdc pdc = createValidBranche();

    final EMHNoeudNiveauContinu nd = new EMHNoeudFactory().getNode("test");
    EMHRelationFactory.addNoeudAmont(pdc, nd);
    Assert.assertTrue(nd.getActuallyActive());
    final EMHModeleBase modele = new EMHModeleBase();
    final OrdPrtCIniModeleBase opti = new OrdPrtCIniModeleBase(CrueConfigMetierForTest.DEFAULT);
    opti.setMethodeInterpol(EnumMethodeInterpol.INTERPOL_ZIMP_AUX_SECTIONS);//cette méthode ne demande pas de DPTI aux noeuds
    modele.addInfosEMH(opti);
    CtuluLog log = new CtuluLog();
    DelegateValidatorNoeud.validateNoeud(nd, modele, log);
    Assert.assertTrue(log.isEmpty());

    opti.setMethodeInterpol(EnumMethodeInterpol.LINEAIRE);//cette méthode demande pas des DPTI aux noeuds
    DelegateValidatorNoeud.validateNoeud(nd, modele, log);
    Assert.assertTrue(log.isNotEmpty());
    final CtuluLogRecord get = log.getRecords().get(0);
    Assert.assertEquals("valid.noeud.mustContainDPTI", get.getMsg());
    Assert.assertEquals("test", get.getArgs()[0]);
    Assert.assertEquals(EnumMethodeInterpol.LINEAIRE.name(), get.getArgs()[1]);

    //on ajoute l'info manquante
    nd.addInfosEMH(new DonPrtCIniNoeudNiveauContinu(CrueConfigMetierForTest.DEFAULT));
    log = new CtuluLog();
    DelegateValidatorNoeud.validateNoeud(nd, modele, log);
    Assert.assertTrue(log.isEmpty());
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testUnCalcPseudoPermParEMHParCalc() {

    final CtuluLog analyzer = new CtuluLog();
    // -- lecture drso --//
    final CrueData data = (CrueData) Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DRSO,
        TestCoeurConfig.INSTANCE_1_1_1).read(
        "/ValidatorForValuesAndContentsTest/M3-0_c10.drso.xml", analyzer, new CrueDataImpl(CrueConfigMetierForTest.DEFAULT)).getMetier();

    // -- lecture dlhy --//
    final List<Loi> listeLois = (List<Loi>) Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DLHY,
        TestCoeurConfig.INSTANCE_1_1_1).read(
        FICHIER_ERRORS_DLHY_XML_V1_1_1, analyzer, data).getMetier();
    Assert.assertNotNull(listeLois);
    // -- lecture de dclm --//
    Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DCLM, TestCoeurConfig.INSTANCE_1_1_1).read(
        FICHIER_ERRORS_DCLM_XML_V1_1_1, analyzer, data).getMetier();
    if (analyzer.containsErrorOrSevereError()) {
      analyzer.printResume();
    }
    new ReadHelperForTest(TestCoeurConfig.INSTANCE).initRelations(data);
    Assert.assertFalse(analyzer.containsErrorOrSevereError());
    final EMHScenario scenarioData = data.getScenarioData();
    final CtuluLog logs = ValidationHelper.validateDonCLimMScenario(scenarioData.getDonCLimMScenario(), CrueConfigMetierForTest.DEFAULT);
    Assert.assertEquals(3, logs.getNbOccurence(CtuluLogLevel.SEVERE));
    final Collection<CtuluLogRecord> records = logs.getRecords();
    final Collection<String> calculs = Arrays.asList("Cc_CP1", "Cc_CP2", "Cc_CT1");
    final Iterator<String> iterator = calculs.iterator();
    final Set<String> emhsWithError = new HashSet<>();
    emhsWithError.add("Br_B8");
    emhsWithError.add("Nd_N1");
    final Set<String> emhsWithErrorFound = new HashSet<>(emhsWithError);
    for (final CtuluLogRecord logRecord : records) {
      Assert.assertEquals("validation.dclm.emhDefinedSeveralTimesWithSameType", logRecord.getMsg());
      final String calculName = logRecord.getArgs()[0].toString();
      final String emh = logRecord.getArgs()[1].toString();
      Assert.assertTrue(emhsWithError.contains(emh));
      emhsWithErrorFound.remove(emh);
      Assert.assertEquals(iterator.next(), calculName);
      Assert.assertTrue(BusinessMessages.getString("validation.dclm.emhDefinedSeveralTimesWithSameType", calculName).contains(calculName));
    }
    Assert.assertTrue(emhsWithErrorFound.isEmpty());
  }

  @Test
  public void testMaxDclmPerCalc_OK() {
    CrueData data = ValidatorForCrue9ExportTest.readData("/16_DM2013.dclm.xml", "/16_DM2013.drso.xml", "/16_DM2013.dlhy.xml", TestCoeurConfig.INSTANCE);
    final CtuluLog logs = ValidationHelper.validateDonCLimMScenario(data.getScenarioData().getDonCLimMScenario(), CrueConfigMetierForTest.DEFAULT);
    assertFalse(logs.containsErrors());
  }

  @Test
  public void testMaxDclmPerCalc_KO_with_duplicate_DCLM() {
    CrueData data = ValidatorForCrue9ExportTest.readData("/16_DM2013-duplicate.dclm.xml", "/16_DM2013.drso.xml", "/16_DM2013.dlhy.xml", TestCoeurConfig.INSTANCE);
    final CtuluLog logs = ValidationHelper.validateDonCLimMScenario(data.getScenarioData().getDonCLimMScenario(), CrueConfigMetierForTest.DEFAULT);
    assertTrue(logs.containsSevereError());
    assertEquals(2, logs.getNbOccurence(CtuluLogLevel.SEVERE));
    for (CtuluLogRecord record : logs.getRecords()) {
      if (record.getLevel().equals(CtuluLogLevel.SEVERE)) {
        assertEquals("validation.dclm.severalTimeSameDclm", record.getMsg());
        assertEquals("Cc_T01_Test", record.getArgs()[0]);
        Object type = record.getArgs()[1];
        assertTrue("CalcTransNoeudBg1".equals(type) || "CalcTransNoeudBg1Av".equals(type));
      }
    }
  }

  @Test
  public void testEmptyValuesAreDetected() {
    DonCLimMScenario donCLimMScenario=new DonCLimMScenario();

    CalcTrans calc = new CalcTrans();
    calc.setNom("T_01");
    donCLimMScenario.addCalc(calc);

    CalcTransNoeudQappExt qappExt=new CalcTransNoeudQappExt();
    qappExt.setCalculParent(calc);
    qappExt.setSection("test");
    qappExt.setNomFic("test");
    qappExt.setEmh(new EMHNoeudNiveauContinu("test"));
    calc.addDclm(qappExt);

    CalcTransBrancheOrificeManoeuvreRegul orificeManoeuvreRegul=new CalcTransBrancheOrificeManoeuvreRegul(CrueConfigMetierForTest.DEFAULT);
    orificeManoeuvreRegul.setParam("test");
    orificeManoeuvreRegul.setEmh(new EMHBrancheOrifice("test"));

    calc.addDclm(orificeManoeuvreRegul);

     CtuluLog logs = ValidationHelper.validateDonCLimMScenario(donCLimMScenario, CrueConfigMetierForTest.DEFAULT);
    assertFalse(logs.containsErrorOrSevereError());

    qappExt.setNomFic("");
    qappExt.setSection("");
    orificeManoeuvreRegul.setParam("");

    logs = ValidationHelper.validateDonCLimMScenario(donCLimMScenario, CrueConfigMetierForTest.DEFAULT);
    assertEquals(3,logs.getNbOccurence(CtuluLogLevel.ERROR));
    List<CtuluLogRecord> errorLevel = logs.getRecords().stream().filter(r -> r.getLevel() == CtuluLogLevel.ERROR).collect(Collectors.toList());
    assertEquals("validation.dclm.nomFic.isEmpty",errorLevel.get(0).getMsg());
    assertEquals("validation.dclm.section.isEmpty",errorLevel.get(1).getMsg());
    assertEquals("validation.dclm.param.isEmpty",errorLevel.get(2).getMsg());

  }


  @Test
  public void testLectureM6_0_C9() {
    final CrueIOResu<CrueData> data = readM6_0_C9();
    final CrueConfigMetier crueConfigMetier = data.getMetier().getCrueConfigMetier();

    // pour tester les valeurs:
    final List<CtuluLog> validateValues = ValidatorForValuesAndContents.validateValues(data.getMetier().getScenarioData(),
        crueConfigMetier);
    for (final CtuluLog ctuluAnalyze : validateValues) {
      if (ctuluAnalyze.containsErrors()) {
        ctuluAnalyze.printResume();
      }
      assertFalse(ctuluAnalyze.containsErrors());
    }
  }

  private CrueIOResu<CrueData> readM6_0_C9() {
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueIOResu<CrueData> data = new ReadHelperForTest(TestCoeurConfig.INSTANCE)
        .readModele(analyzer, "/ValidatorForValuesAndContentsTest/M6-0_c9.dc", "/ValidatorForValuesAndContentsTest/M6-0_c9.dh");
    AbstractTestParent.testAnalyser(analyzer);
    return data;
  }
}
