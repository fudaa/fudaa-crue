/*
 GPL 2
 */
package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractCrueBinaryFileFormat;
import org.fudaa.dodico.crue.io.Crue9FileFormatFactory;
import org.fudaa.dodico.crue.io.neuf.FCBSequentialReader;
import org.fudaa.dodico.crue.io.neuf.ResultatCalculCrue9;
import org.fudaa.dodico.crue.io.test.ReadHelperForTest;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * @author Frederic Deniger
 */
public class ValidatorAndLoaderResultatCrue9Test extends AbstractTestParent {
    public ValidatorAndLoaderResultatCrue9Test() {
    }

    /**
     * Lit le fichier et teste que le CtuluAnalyse ne contient pas d'erreurs.
     *
     * @param <T> le type de retour
     * @param fmt le format binaire
     * @param path le chemin du fichier a lire
     * @param crueData
     * @return la donnée lue
     */
    public <T> CrueIOResu<T> readBinaire(final AbstractCrueBinaryFileFormat<T> fmt,
                                         final String path, final CrueData crueData) {
        final File f = AbstractTestParent.getFile(path);
        filesToDelete.add(f);
        Assert.assertNotNull(f);
        final CrueIOResu<T> read = fmt.read(f, crueData);
        AbstractTestParent.testAnalyser(read.getAnalyse());
        return read;
    }

    @Test
    public void testLectureFCBModele30() {
        final CtuluLog log = new CtuluLog();
        final CrueData data = new ReadHelperForTest(TestCoeurConfig.INSTANCE)
            .readModele(log, "/ValidatorAndLoaderResultatCrue9Test/M3-0_c9c9.dc", "/ValidatorAndLoaderResultatCrue9Test/M3-0_c9c9.dh").getMetier();
        final CrueIOResu<FCBSequentialReader> read = readBinaire(Crue9FileFormatFactory.getFCBFileFormat(),
            "/ValidatorAndLoaderResultatCrue9Test/M3-0_C9C9.FCB", data);
        Assert.assertNotNull(read);
//    final FCBSequentialReader infos = read.getMetier();
        // test des profils
//    assertEquals(26, infos.getNbProfils());
        data.setSto(readBinaire(Crue9FileFormatFactory.getSTOFileFormat(), "/ValidatorAndLoaderResultatCrue9Test/M3-0_C9C9.STO", data).getMetier());
        ValidatorAndLoaderResultatCrue9.validFcbProfils(read.getMetier(), data, log);
        AbstractTestParent.testAnalyser(log);
        Assert.assertTrue(getResultatPosition(data, "ST_B5_AM") > 0);
        Assert.assertTrue(getResultatPosition(data, "ST_B5_AV") > 0);
        Assert.assertTrue(getResultatPosition(data, "ST_B8_AM") > 0);
        Assert.assertTrue(getResultatPosition(data, "ST_B8_AV") > 0);
    }

    private int getResultatPosition(final CrueData data, final String id) {
        return ((ResultatCalculCrue9) data.findSectionByReference(id).getResultatCalcul().getDelegate()).getPosition();
    }
}
