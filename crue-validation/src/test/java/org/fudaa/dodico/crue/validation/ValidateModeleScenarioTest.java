/*
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormatETU;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.fudaa.dodico.crue.validation.util.DelegateValidatorEMHInfoUnique;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * @author deniger
 */
public class ValidateModeleScenarioTest extends AbstractTestParent {
  /**
   * Teste l'écriture du fichier de validation d'un scénario mais ne teste pas les résultats
   */
  @Test
  public void testValide_v1_2() {
    final File exportZipInTempDir = exportZipInTempDir("/org/fudaa/dodico/crue/validation/Etu3-6_v1.2.zip");
    final File file = new File(exportZipInTempDir, "Etu3-6.etu.xml");
    Assert.assertTrue(file.exists());
    final EMHProjet projet = Crue10FileFormatETU.readData(file, TestCoeurConfig.INSTANCE_1_2);
    final ManagerEMHScenario scenarioCourant = projet.getScenarioCourant();
    final ValidateModeleScenarioWithSchema validator = new ValidateModeleScenarioWithSchema(projet.getEtudeScenarioURL(scenarioCourant.getNom()),
        scenarioCourant, TestCoeurConfig.INSTANCE_1_2);
    final CtuluLogGroup validate = validator.validate();
    Assert.assertFalse(validate.containsFatalError());
    Assert.assertFalse(validate.containsError());
  }
  @Test
  public void testValide_v1_3() {
    final File exportZipInTempDir = exportZipInTempDir("/org/fudaa/dodico/crue/validation/Etu3-6_v1.3.zip");
    final File file = new File(exportZipInTempDir, "Etu3-6.etu.xml");
    Assert.assertTrue(file.exists());
    final EMHProjet emhProjet = Crue10FileFormatETU.readData(file, TestCoeurConfig.INSTANCE);
    final ManagerEMHScenario scenarioCourant = emhProjet.getScenarioCourant();
    final ValidateModeleScenarioWithSchema validator = new ValidateModeleScenarioWithSchema(emhProjet.getEtudeScenarioURL(scenarioCourant.getNom()),
        scenarioCourant, TestCoeurConfig.INSTANCE);
    final CtuluLogGroup validate = validator.validate();
    Assert.assertFalse(validate.containsFatalError());
    Assert.assertFalse(validate.containsError());
  }
  @Test
  public void testValide_v1_3_BV_2009() {
    final File exportZipInTempDir = exportZipInTempDir("/org/fudaa/dodico/crue/validation/Etu_BV2009_g1.3.zip");
    final File file = new File(exportZipInTempDir, "Etu_BV2009.etu.xml");
    Assert.assertTrue(file.exists());
    final EMHProjet emhProjet = Crue10FileFormatETU.readData(file, TestCoeurConfig.INSTANCE);
    final ManagerEMHScenario scenarioCourant = emhProjet.getScenarioCourant();
    final ValidateModeleScenarioWithSchema validator = new ValidateModeleScenarioWithSchema(emhProjet.getEtudeScenarioURL(scenarioCourant.getNom()),
        scenarioCourant, TestCoeurConfig.INSTANCE);
    final CtuluLogGroup validate = validator.validate();
    Assert.assertFalse(validate.containsFatalError());
    Assert.assertFalse(validate.containsError());
  }

}
