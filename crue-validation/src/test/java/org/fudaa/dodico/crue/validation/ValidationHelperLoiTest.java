/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.validation;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.emh.EvolutionFF;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class ValidationHelperLoiTest {

  public ValidationHelperLoiTest() {
  }

  @Test
  public void testLoiIsValid() {
    CrueConfigMetier ccm = TestCoeurConfig.INSTANCE.getCrueConfigMetier();
    Loi createValidLoi = createValidLoi();
    CtuluLog log = new CtuluLog();
    ValidationHelperLoi.valideLoi(createValidLoi, ccm, log, null);
    assertTrue(log.isEmpty());
  }

  @Test
  public void testLoiIsNotValidStrictlyIncreasing() {
    CrueConfigMetier ccm = TestCoeurConfig.INSTANCE.getCrueConfigMetier();
    Loi loi = createValidLoi();
    List<PtEvolutionFF> ptEvolutionFF = loi.getEvolutionFF().getPtEvolutionFF();
    ptEvolutionFF.add(ptEvolutionFF.get(ptEvolutionFF.size() - 1));
    CtuluLog log = new CtuluLog();
    ValidationHelperLoi.valideLoi(loi, ccm, log, null);
    assertEquals(1, log.getRecords().size());
    CtuluLogRecord record = log.getRecords().get(0);
    assertEquals("valid.loi.notStrictlyIncreasing", record.getMsg());
    assertEquals("test", record.getArgs()[0]);
    assertEquals("3", record.getArgs()[1]);

    //avec prefix
    log = new CtuluLog();
    ValidationHelperLoi.valideLoi(loi, ccm, log, "prefix");
    assertEquals(1, log.getRecords().size());
    record = log.getRecords().get(0);
    assertEquals("valid.loi.notStrictlyIncreasing", record.getMsg());
    assertEquals("prefix", record.getArgs()[0]);
    assertEquals("3", record.getArgs()[1]);

  }

  @Test
  public void testLoiIsNotValidIncreasing() {
    CrueConfigMetier ccm = TestCoeurConfig.INSTANCE.getCrueConfigMetier();
    Loi loi = createValidLoi();
    loi.setType(EnumTypeLoi.LoiPtProfil);
    List<PtEvolutionFF> ptEvolutionFF = loi.getEvolutionFF().getPtEvolutionFF();
    final PtEvolutionFF lastPoint = new PtEvolutionFF(ptEvolutionFF.get(ptEvolutionFF.size() - 1));
    ptEvolutionFF.add(lastPoint);
    //
    CtuluLog log = new CtuluLog();
    //plus tester car plus de loi non strictement croissante:
//    ValidationHelperLoi.valideLoi(loi, ccm, log, null);
//    assertTrue("no error for increasing with 2 equals value", log.isEmpty());
    lastPoint.setAbscisse(lastPoint.getAbscisse() - 0.5);
    ValidationHelperLoi.valideLoi(loi, ccm, log, null);
    assertEquals(1, log.getRecords().size());
    CtuluLogRecord record = log.getRecords().get(0);
    assertEquals("valid.loi.notStrictlyIncreasing", record.getMsg());
    assertEquals("test", record.getArgs()[0]);
    assertEquals("3", record.getArgs()[1]);

  }

  private Loi createValidLoi() {
    Loi loi = new LoiFF();
    loi.setType(EnumTypeLoi.LoiZFK);
    loi.setNom("test");
    loi.setEvolutionFF(new EvolutionFF(new ArrayList<>()));
    List<PtEvolutionFF> ptEvolutionFF = loi.getEvolutionFF().getPtEvolutionFF();
    ptEvolutionFF.add(new PtEvolutionFF(0, 8));
    ptEvolutionFF.add(new PtEvolutionFF(1, 8));
    ptEvolutionFF.add(new PtEvolutionFF(2, 8));
    return loi;
  }
}
