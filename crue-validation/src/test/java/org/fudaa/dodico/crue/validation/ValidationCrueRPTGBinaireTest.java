package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.ItemResDao;
import org.fudaa.dodico.crue.io.res.ResCatEMHContent;
import org.fudaa.dodico.crue.io.res.ResTypeEMHBuilder;
import org.fudaa.dodico.crue.io.res.ResTypeEMHContent;
import org.fudaa.dodico.crue.io.res.ResultatEntry;
import org.fudaa.dodico.crue.io.rptg.CrueDaoRPTG;
import org.fudaa.dodico.crue.io.rptg.ResPrtGeoDao;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.junit.After;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

import static org.junit.Assert.*;

/**
 * @author deniger
 */
public class ValidationCrueRPTGBinaireTest {
  private final String rptgXml = "/rcal/M201-0_c9c10.rptg.xml";
  private final String rptgBinName = "M201-0_c9c10.rptg_0001.bin";
  private final String rptgBinPath = "/rcal/" + rptgBinName;
  private final List<File> filesToDelete = new ArrayList<>();

  @Test
  public void testBinaryFile() throws IOException {
    final File createTempDir = CtuluLibFile.createTempDir();
    filesToDelete.add(createTempDir);
    final File rptg = new File(createTempDir, rptgBinName);
    CtuluLibFile.getFileFromJar(rptgBinPath, rptg);

    assertTrue(rptg.exists());
    final CrueDaoRPTG res = readXml();
    final ResPrtGeoDao firstPrtGeo = res.getResPrtGeos().get(0);
    final ResTypeEMHBuilder typeBuilder = new ResTypeEMHBuilder(res);
    final CrueIOResu<List<ResCatEMHContent>> extract = typeBuilder.extract();
    final ResultatTimeKey key = new ResultatTimeKey(ResPrtGeoDao.class.getSimpleName());
    final ResultatEntry entry = new ResultatEntry(key);
    entry.setBinFile(new File(rptg.getParentFile(), firstPrtGeo.getHref()));
    entry.setOffsetInBytes(res.getParametrage().getBytes(firstPrtGeo.getOffsetMot()));
    final Map<ResultatTimeKey, String> delimiteurs = new HashMap<>();
    delimiteurs.put(key, res.getParametrage().getDelimiteurChaine("ResPrtGeo"));
    final Map<ResultatTimeKey, ResultatEntry> pasDeTemps = new HashMap<>();
    pasDeTemps.put(key, entry);
    final ValidatorResultatCrue10BinaryFile validatorBinaryFile = new ValidatorResultatCrue10BinaryFile();
    final CtuluLog validBinaryFile = validatorBinaryFile.validBinaryFile(Collections.singletonList(entry), extract.getMetier(),
        delimiteurs);
    assertTrue(validBinaryFile.isEmpty());
  }

  /**
   * recupere le bon ResTypeEMHContent et affecte les variables en plus.
   */
  @SuppressWarnings("Duplicates")
  private static Pair<ResTypeEMHContent, ItemResDao> find(final List<ResCatEMHContent> metier, final String nom) {
    for (final ResCatEMHContent resCatEMHContent : metier) {
      for (final ResTypeEMHContent resTypeEMHContent : resCatEMHContent.getResTypeEMHContent()) {
        final List<ItemResDao> itemResDaos = resTypeEMHContent.getItemResDao();
        for (final ItemResDao itemResDao : itemResDaos) {
          if (nom.equals(itemResDao.getNomRef())) {
            resTypeEMHContent.getResVariablesContent().addVariableResLoiNbrPtDao(itemResDao.getVariableResLoiNbrPt());
            return new Pair<>(resTypeEMHContent, itemResDao);
          }
        }
      }
    }
    return null;
  }

  @After
  public void cleanFiles() {
    for (final File file : filesToDelete) {
      if (file.isDirectory()) {
        CtuluLibFile.deleteDir(file);
      } else {
        file.delete();
      }
    }
    filesToDelete.clear();
  }

  private CrueDaoRPTG readXml() {
    final URL resource = getClass().getResource(rptgXml);
    final Crue10FileFormat version = Crue10FileFormatFactory.getVersion(CrueFileType.RPTG, TestCoeurConfig.INSTANCE);
    final CtuluLog log = new CtuluLog();
    final CrueIOResu read = version.read(resource, log, null);
    assertFalse(log.containsErrorOrSevereError());
    return (CrueDaoRPTG) read.getMetier();
  }
}
