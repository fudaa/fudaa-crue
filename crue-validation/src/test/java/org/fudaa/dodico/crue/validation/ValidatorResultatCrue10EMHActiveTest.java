/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.validation;

import java.util.*;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.io.rdao.CommonResDao;
import org.fudaa.dodico.crue.io.res.ResCatEMHContent;
import org.fudaa.dodico.crue.io.res.ResTypeEMHContent;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheBarrageFilEau;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class ValidatorResultatCrue10EMHActiveTest {

  private final String br1Nom = "br1";
  private final String brInResButNotInModele = "br1NotInModele";
  private final String br2Nom = "br2";

  public ValidatorResultatCrue10EMHActiveTest() {
  }

  /**
   * Test of validActiveEMHPresentInRes method, of class ValidatorResultatCrue10EMHActive.
   */
  @Test
  public void testValidOnlyEMHActiveAreActive() {
    EMHModeleBase modele = createModele();
    ValidatorResultatCrue10EMHActive validator = new ValidatorResultatCrue10EMHActive();
    CtuluLog log = validator.validActiveEMHPresentInRes(Collections.singletonList(createContent()), modele, CtuluLogLevel.ERROR);
    assertFalse(log.isEmpty());
    Collection<CtuluLogRecord> records = log.getRecords();
    assertEquals(1, records.size());
    Iterator<CtuluLogRecord> iterator = records.iterator();
    CtuluLogRecord next = iterator.next();
    assertEquals("res.validate.activeEMHNotPresentInRes", next.getMsg());
    assertEquals(1, next.getArgs().length);
    assertEquals(br2Nom, next.getArgs()[0]);
  }

  ResCatEMHContent createContent() {
    ResCatEMHContent res = new ResCatEMHContent();
    ResTypeEMHContent type = new ResTypeEMHContent("Branche");
    type.addItemResDao(new CommonResDao.ItemResDao(br1Nom), -1);//-1 car nont teste
    type.addItemResDao(new CommonResDao.ItemResDao(brInResButNotInModele), -1);//-1 car nont teste
    res.setResTypeEMHContent(Collections.singletonList(type));
    assertEquals(2, res.getAllRef().size());
    return res;
  }

  protected EMH createEMH(String nom) {
    EMHBrancheBarrageFilEau res = new EMHBrancheBarrageFilEau(nom);
    res.setUserActive(true);
    return res;
  }

  private EMHModeleBase createModele() {
    EMHModeleBase modele = new EMHModeleBase();
    EMHSousModele sousModele = new EMHSousModele();
    EMHRelationFactory.addRelationContientEMH(modele, sousModele);
    EMHRelationFactory.addRelationContientEMH(sousModele, createEMH(br1Nom));
    EMHRelationFactory.addRelationContientEMH(sousModele, createEMH(br2Nom));
    List<EMH> allSimpleEMH = modele.getAllSimpleEMH();
    assertEquals(2, allSimpleEMH.size());
    for (EMH emh : allSimpleEMH) {
      assertTrue(emh.getActuallyActive());

    }
    return modele;
  }
}
