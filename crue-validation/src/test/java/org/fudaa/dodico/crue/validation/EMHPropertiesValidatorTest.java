/*
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.test.ReadHelperForTest;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * @author deniger
 */
public class EMHPropertiesValidatorTest extends AbstractTestParent {
    @Test
    public void testValidationModele3() {
        final CtuluLog analyze = new CtuluLog();
        final CrueIOResu<CrueData> res = new ReadHelperForTest(TestCoeurConfig.INSTANCE).readModele(analyze, "/M3-0_c9.dc",
            "/M3-0_c9.dh");
        testAnalyser(analyze);
        final List<EMH> allEMH = res.getMetier().getAllSimpleEMH();
        allEMH.add(res.getMetier().getScenarioData());
        allEMH.add(res.getMetier().getModele());
        allEMH.add(res.getMetier().getSousModele());
        final CrueData data = res.getMetier();
        final List<CtuluLog> validate = ValidatorForValuesAndContents.validateValues(data.getScenarioData(),
            data.getCrueConfigMetier());
        Assert.assertEquals(3, validate.size());
    }

    /**
     * Test la validation du modele 4.
     */
    @Test
    public void testValidationModele4() {
        final CtuluLog analyze = new CtuluLog();
        final EMHScenario scenario = new ReadHelperForTest(TestCoeurConfig.INSTANCE).readModeleAnsSetRelation(analyze, "/M4-0_c9.dc", "/M4-0_c9.dh");
        testAnalyser(analyze);
        final List<EMH> allEMH = scenario.getConcatSousModele().getAllSimpleEMH();
        allEMH.add(scenario);
        allEMH.add(scenario.getModeles().get(0));
        final List<CtuluLog> validate = ValidatorForValuesAndContents.validateValues(scenario,
            TestCoeurConfig.INSTANCE.getCrueConfigMetier());
        for (final CtuluLog ctuluAnalyze : validate) {
            if (ctuluAnalyze.containsErrorOrSevereError()) {
                ctuluAnalyze.printResume();
            }
            // ores not read
            Assert.assertFalse(ctuluAnalyze.containsErrorOrSevereError());
        }
    }
}
