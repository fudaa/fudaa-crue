package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.rcal.Crue10ResultatPasDeTempsDelegate;
import org.fudaa.dodico.crue.io.rcal.CrueDaoRCAL;
import org.fudaa.dodico.crue.io.res.RCalTimeStepBuilder;
import org.fudaa.dodico.crue.io.res.ResCatEMHContent;
import org.fudaa.dodico.crue.io.res.ResTypeEMHBuilder;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.junit.After;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author deniger
 */
public class ValidationCrueRCALBinaireTest {

  private final String rcalXml = "/rcal/M3-0_c10.rcal.xml";
  private final String rcalBinName = "M3-0_c10.rcal_0001.bin";
  private final String rcalBinPath = "/rcal/" + rcalBinName;
  private final List<File> filesToDelete = new ArrayList<>();

  private static class Result {

    RCalTimeStepBuilder.Result timeStepExtracted;
    List<ResCatEMHContent> categories;
    CrueDaoRCAL rcal;
  }

  @Test
  public void testValidationReadBinaire() throws IOException {
    final Result res = extracted();
    final List<ResultatTimeKey> keys = res.timeStepExtracted.getOrderedKey();
    assertEquals(2, keys.size());
    assertEquals("Cc_P1", keys.get(0).getNomCalcul());
    assertEquals("Cc_P2", keys.get(1).getNomCalcul());
    final Crue10ResultatPasDeTempsDelegate timeSteps = new Crue10ResultatPasDeTempsDelegate(res.timeStepExtracted.getOrderedKey(),
            res.timeStepExtracted.getEntries());
    final ValidatorResultatCrue10BinaryFile validator = new ValidatorResultatCrue10BinaryFile();
    final CtuluLog log = validator.validBinaryFile(timeSteps.getEntries().values(), res.categories, res.timeStepExtracted.getDelimiteurs());
    assertFalse(log.containsSevereError());
  }

  private Result extracted() throws IOException {
    final File createTempDir = CtuluLibFile.createTempDir();
    filesToDelete.add(createTempDir);
    final File file = new File(createTempDir, rcalBinName);
    CtuluLibFile.getFileFromJar(rcalBinPath, file);

    assertTrue(file.exists());
    final CrueDaoRCAL rcal = readXml();
    final ResTypeEMHBuilder typeBuilder = new ResTypeEMHBuilder(rcal);
    final CrueIOResu<List<ResCatEMHContent>> extract = typeBuilder.extract();
    final RCalTimeStepBuilder timeStepBuilder = new RCalTimeStepBuilder();
    final RCalTimeStepBuilder.Result timeStepExtracted = timeStepBuilder.extract(rcal, file.getParentFile());
    final List<ResCatEMHContent> categories = extract.getMetier();
    final Result res = new Result();
    res.categories = categories;
    res.timeStepExtracted = timeStepExtracted;
    res.rcal = rcal;
    return res;
  }

  @After
  public void cleanFiles() {
    for (final File file : filesToDelete) {
      if (file.isDirectory()) {
        CtuluLibFile.deleteDir(file);
      } else {
        file.delete();
      }
    }
    filesToDelete.clear();

  }

  private CrueDaoRCAL readXml() {
    final URL resource = getClass().getResource(rcalXml);
    final Crue10FileFormat version = Crue10FileFormatFactory.getVersion(CrueFileType.RCAL, TestCoeurConfig.INSTANCE);
    final CtuluLog log = new CtuluLog();
    final CrueIOResu read = version.read(resource, log, null);
    assertFalse(log.containsErrorOrSevereError());
    final CrueDaoRCAL rcal = (CrueDaoRCAL) read.getMetier();
    return rcal;
  }
}
