package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHNoeudFactory;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

/**
 * @author deniger
 */
public class ValidatorResultatCrue10Test {

  @Test
  public void testValidateResultatPretraitementIsOk() {
    final String Calc_1 = "Calc_1";
    final String Calc_2 = "Calc_2";
    final EMHScenario scenario = createScenario(Calc_1, Calc_2);
    final EMHModeleBase modele = createModele(Arrays.asList("Nd_1", "Nd_2"));
    EMHRelationFactory.addRelationContientEMH(scenario, modele);
    modele.setNom("Modele 1");
    final ResPrtReseau resPrtReseau = createReseau(scenario, Calc_1, Arrays.asList("Nd_1", "Nd_2"), Calc_2,
            Arrays.asList("Nd_1", "Nd_2"), null, null);
    modele.addInfosEMH(resPrtReseau);
    final ValidatorResultatCrue10Prt validator = new ValidatorResultatCrue10Prt();
    final CtuluLogGroup errorMng = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    final boolean validateResultatPretraitement = validator.validateResultatPretraitement(scenario, modele, errorMng);
    Assert.assertTrue(validateResultatPretraitement);
    Assert.assertFalse(errorMng.containsSomething());
    //pour tester le cas avec aucune valeur
    resPrtReseau.setListResPrtReseauNoeud(null);
    try {
      validator.validateResultatPretraitement(scenario, modele, errorMng);
    } catch (final Exception ex) {
      Assert.fail(ex.getMessage());
    }
  }

  @Test
  public void testValidateResultatPretraitementInactiveNodes() {
    final String Calc_1 = "Calc_1";
    final String Calc_2 = "Calc_2";
    final EMHScenario scenario = createScenario(Calc_1, Calc_2);
    final EMHModeleBase modele = createModele(Arrays.asList("Nd_1", "Nd_2"), false);
    EMHRelationFactory.addRelationContientEMH(scenario, modele);
    modele.setNom("Modele 1");
    final ResPrtReseau resPrtReseau = createReseau(scenario, Calc_1, Arrays.asList("Nd_1", "Nd_2"), Calc_2,
            Arrays.asList("Nd_1", "Nd_2"), null, null);
    modele.addInfosEMH(resPrtReseau);
    final ValidatorResultatCrue10Prt validator = new ValidatorResultatCrue10Prt();
    final CtuluLogGroup errorMng = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    final boolean validateResultatPretraitement = validator.validateResultatPretraitement(scenario, modele, errorMng);
    Assert.assertFalse(validateResultatPretraitement);
    final CtuluLog log = errorMng.getLogs().get(0);
    Assert.assertEquals(2, log.getRecords().size());
    Assert.assertTrue(log.containsErrors());
    final Iterator<CtuluLogRecord> iterator = log.getRecords().iterator();
    CtuluLogRecord next = iterator.next();
    Assert.assertEquals("rptr.UnactiveEMHNoeud.error", next.getMsg());
    next = iterator.next();
    Assert.assertEquals("rptr.UnactiveEMHNoeud.error", next.getMsg());
  }

  @Test
  public void testValidateResultatPretraitementContainsWrongNode() {
    final String Calc_1 = "Calc_1";
    final String Calc_2 = "Calc_2";
    final EMHScenario scenario = createScenario(Calc_1, Calc_2);
    final EMHModeleBase modele = createModele(Arrays.asList("Nd_1", "Nd_2"));
    EMHRelationFactory.addRelationContientEMH(scenario, modele);
    modele.setNom("Modele 1");
    final ResPrtReseau resPrtReseau = createReseau(scenario, Calc_1, Arrays.asList("Nd_1", "Nd_2"), Calc_2,
            Arrays.asList("Nd_1", "Nd_3"), null, null);
    modele.addInfosEMH(resPrtReseau);
    final ValidatorResultatCrue10Prt validator = new ValidatorResultatCrue10Prt();
    final CtuluLogGroup errorMng = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    final boolean validateResultatPretraitement = validator.validateResultatPretraitement(scenario, modele, errorMng);

    Assert.assertFalse(validateResultatPretraitement);
    Assert.assertTrue(errorMng.containsError());
    final CtuluLog log = errorMng.getLogs().get(0);
    Assert.assertEquals(BusinessMessages.getString("load.validRptr", modele.getNom()), log.getDesci18n());
    Assert.assertTrue(log.containsErrors());
    Assert.assertEquals(2, log.getRecords().size());
    final Iterator<CtuluLogRecord> iterator = log.getRecords().iterator();
    CtuluLogRecord next = iterator.next();
    Assert.assertEquals("rptr.CatEMHNoeudNotDefined.error", next.getMsg());
    Object[] parameters = next.getArgs();
    Assert.assertEquals(2, parameters.length);
    Assert.assertEquals(Calc_2, parameters[0]);
    Assert.assertEquals("Nd_2", parameters[1]);
    next = iterator.next();
    Assert.assertEquals("rptr.prtNoeudsUnknown.error", next.getMsg());
    parameters = next.getArgs();
    Assert.assertEquals(2, parameters.length);
    Assert.assertEquals(Calc_2, parameters[0]);
    Assert.assertEquals("Nd_3", parameters[1]);
  }

  @Test
  public void testValidateResultatPretraitementContainsDoublonNode() {
    final String Calc_1 = "Calc_1";
    final String Calc_2 = "Calc_2";
    final EMHScenario scenario = createScenario(Calc_1, Calc_2);
    final EMHModeleBase modele = createModele(Arrays.asList("Nd_1", "Nd_2"));
    EMHRelationFactory.addRelationContientEMH(scenario, modele);
    modele.setNom("Modele 1");
    final ResPrtReseau resPrtReseau = createReseau(scenario, Calc_1, Arrays.asList("Nd_1", "Nd_1", "Nd_2"), Calc_2,
            Arrays.asList("Nd_1", "Nd_2", "Nd_2"), null, null);
    modele.addInfosEMH(resPrtReseau);
    final ValidatorResultatCrue10Prt validator = new ValidatorResultatCrue10Prt();
    final CtuluLogGroup errorMng = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    final boolean validateResultatPretraitement = validator.validateResultatPretraitement(scenario, modele, errorMng);
    Assert.assertFalse(validateResultatPretraitement);
    final CtuluLog log = errorMng.getLogs().get(0);
    Assert.assertEquals(2, log.getRecords().size());
    final Iterator<CtuluLogRecord> iterator = log.getRecords().iterator();
    CtuluLogRecord next = iterator.next();
    Assert.assertEquals("rptr.prtNoeudsDoublons.error", next.getMsg());
    Object[] parameters = next.getArgs();
    Assert.assertEquals(2, parameters.length);
    Assert.assertEquals(Calc_1, parameters[0]);
    Assert.assertEquals("Nd_1", parameters[1]);

    next = iterator.next();
    Assert.assertEquals("rptr.prtNoeudsDoublons.error", next.getMsg());
    parameters = next.getArgs();
    Assert.assertEquals(2, parameters.length);
    Assert.assertEquals(Calc_2, parameters[0]);
    Assert.assertEquals("Nd_2", parameters[1]);

  }

  @Test
  public void testValidateResultatPretraitementCalcWithoutRes() {
    final String Calc_1 = "Calc_1";
    final String Calc_2 = "Calc_2";
    final String Calc_3 = "Calc_3";
    final EMHScenario scenario = createScenario(Calc_1, Calc_2, Calc_3);
    final EMHModeleBase modele = createModele(Arrays.asList("Nd_1", "Nd_2"));
    EMHRelationFactory.addRelationContientEMH(scenario, modele);
    modele.setNom("Modele 1");
    final ResPrtReseau resPrtReseau = createReseau(scenario, Calc_1, Arrays.asList("Nd_1", "Nd_2"), Calc_2,
            Arrays.asList("Nd_1", "Nd_2"), null, null);
    modele.addInfosEMH(resPrtReseau);
    final ValidatorResultatCrue10Prt validator = new ValidatorResultatCrue10Prt();
    final CtuluLogGroup errorMng = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    final boolean validateResultatPretraitement = validator.validateResultatPretraitement(scenario, modele, errorMng);
    Assert.assertFalse(validateResultatPretraitement);
    final CtuluLog log = errorMng.getLogs().get(0);
    Assert.assertEquals(1, log.getRecords().size());
    final Iterator<CtuluLogRecord> iterator = log.getRecords().iterator();
    final CtuluLogRecord next = iterator.next();
    Assert.assertEquals("rptr.calcWithoutResPrtReseauNoeuds.error", next.getMsg());
    final Object[] parameters = next.getArgs();
    Assert.assertEquals(1, parameters.length);
    Assert.assertEquals(Calc_3, parameters[0]);
  }

  private static ResPrtReseauNoeuds createResPrtReseauNoeuds(final Map<String, Calc> calcByNom, final List<String> noms, final String calcId) {
    final ResPrtReseauNoeuds resPrtReseauNoeuds = new ResPrtReseauNoeuds();
    final List<ResPrtReseauNoeud> nds = new ArrayList<>(noms.size());
    for (final String nom : noms) {
      nds.add(new ResPrtReseauNoeud(nom));
    }
    resPrtReseauNoeuds.setResPrtReseauNoeud(nds);
    resPrtReseauNoeuds.setCalc(calcByNom.get(calcId));
    return resPrtReseauNoeuds;
  }

  private static EMHScenario createScenario(final String... calcNom) {
    final EMHScenario scenario = new EMHScenario();
    final DonCLimMScenario conditionsLim = new DonCLimMScenario();
    final OrdCalcScenario ocal = new OrdCalcScenario();
    for (final String string : calcNom) {
      final CalcPseudoPerm calc = new CalcPseudoPerm();
      calc.setNom(string);
      conditionsLim.addCalcPseudoPerm(calc);
      final OrdCalcPseudoPerm ocalPseudoPerm = new OrdCalcPseudoPermIniCalcCI();
      ocalPseudoPerm.setCalcPseudoPerm(calc);
      ocal.addOrdCalc(ocalPseudoPerm);
    }
    scenario.addInfosEMH(conditionsLim);
    scenario.addInfosEMH(ocal);

    ocal.addOrdCalc(null);
    return scenario;
  }

  private static ResPrtReseau createReseau(final EMHScenario scenario,
                                           final String calc1, final List<String> noms, final String calc2, final List<String> noms2, final String calc3,
                                           final List<String> noms3) {
    final ResPrtReseau res = new ResPrtReseau();
    final DonCLimMScenario conditionsLim = scenario.getDonCLimMScenario();
    final Map<String, Calc> calcById = new HashMap<>();
    final List<Calc> calcs = conditionsLim.getCalc();
    for (final Calc calc : calcs) {
      calcById.put(calc.getNom(), calc);
    }
    final List<ResPrtReseauNoeuds> lstResPrtReseauNoeuds = new ArrayList<>();
    lstResPrtReseauNoeuds.add(createResPrtReseauNoeuds(calcById, noms, calc1));
    lstResPrtReseauNoeuds.add(createResPrtReseauNoeuds(calcById, noms2, calc2));
    if (calc3 != null) {
      lstResPrtReseauNoeuds.add(createResPrtReseauNoeuds(calcById, noms3, calc3));
    }
    res.setListResPrtReseauNoeud(lstResPrtReseauNoeuds);
    return res;
  }

  private static EMHModeleBase createModele(final List<String> noms) {
    return createModele(noms, true);
  }

  private static EMHModeleBase createModele(final List<String> noms, final boolean activeNodes) {
    final EMHModeleBase modele = new EMHModeleBase();
    final EMHSousModele sousModele = new EMHSousModele();
    EMHRelationFactory.addRelationContientEMH(modele, sousModele);
    final EMHNoeudFactory fac = new EMHNoeudFactory();
    final List<EMHNoeudNiveauContinu> nds = new ArrayList<>(noms.size());
    for (final String string : noms) {
      final EMHNoeudNiveauContinu node = fac.getNode(string);
      final EMHBranchePdc eMHBranchePdc = new EMHBranchePdc("Br_" + string);
      eMHBranchePdc.setUserActive(activeNodes);
      EMHRelationFactory.addRelationContientEMH(sousModele, eMHBranchePdc);
      node.addAllRelations(Collections.singletonList(new RelationEMHBrancheContientNoeud(eMHBranchePdc)));
      nds.add(node);
    }
    EMHRelationFactory.addRelationsContientEMH(sousModele, nds);
    return modele;
  }
}
