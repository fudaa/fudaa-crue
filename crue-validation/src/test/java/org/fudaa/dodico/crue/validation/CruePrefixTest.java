/*
 * Licence GPL Copyright
 */
package org.fudaa.dodico.crue.validation;

import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author deniger
 */
public class CruePrefixTest {
  private final String expected = "Nd_test";

  /**
   * Teste le changement de prefixe
   */
  @Test
  public void testChangePrefixe() {
    Assert.assertEquals(expected, CruePrefix.addPrefix("test", EnumCatEMH.NOEUD));
    Assert.assertEquals("Nd_tes_t", CruePrefix.addPrefix("tes#t", EnumCatEMH.NOEUD));
    Assert.assertEquals("Nd_tes_t", CruePrefix.addPrefix("tes/t", EnumCatEMH.NOEUD));
    Assert.assertEquals(expected, CruePrefix.addPrefix("ND_test", EnumCatEMH.NOEUD));
    Assert.assertEquals("PS_TOTO", CruePrefix.changePrefix("ST_TOTO", "ST_", "PS_"));
    Assert.assertEquals("PS_TOTO", CruePrefix.changePrefix("PS_TOTO", "ST_", "PS_"));
  }

  @Test
  public void testAddPrefixe() {
    Assert.assertEquals("PS_TOTO", CruePrefix.addPrefix("ST_TOTO", "ST_", "PS_"));
    Assert.assertEquals("PS_PS_TOTO", CruePrefix.addPrefix("PS_TOTO", "ST_", "PS_"));
  }

  /**
   * Teste le changement de prefixe si ce dernier commence par un autre prefix
   */
  @Test
  public void testChangePrefixeDouble() {
    Assert.assertEquals(expected, CruePrefix.changePrefix("st_test", "st_", CruePrefix.P_NOEUD));
    Assert.assertEquals(expected, CruePrefix.changePrefix("St_test", "st_", CruePrefix.P_NOEUD));
    Assert.assertEquals(expected, CruePrefix.changePrefix("ST_test", "st_", CruePrefix.P_NOEUD));
    Assert.assertEquals(expected, CruePrefix.changePrefix("SI_test", "si_", CruePrefix.P_NOEUD));
  }
}
