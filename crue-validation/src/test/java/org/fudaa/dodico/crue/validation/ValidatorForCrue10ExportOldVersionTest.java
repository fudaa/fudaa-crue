/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.validation;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.OrdCalcPseudoPermIniCliche;
import org.fudaa.dodico.crue.metier.emh.OrdCalcScenario;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTransIniCalcCI;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTransIniCalcCliche;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class ValidatorForCrue10ExportOldVersionTest {

  public ValidatorForCrue10ExportOldVersionTest() {
  }

  /**
   * Test of validate method, of class ValidatorForCrue10ExportOldVersion.
   */
  @Test
  public void testValidate() {
    EMHScenario scenario = new EMHScenario();
    ValidatorForCrue10ExportOldVersion instance = new ValidatorForCrue10ExportOldVersion();
    final OrdCalcScenario ordCalcScenario = new OrdCalcScenario();
    ordCalcScenario.addOrdCalc(new OrdCalcPseudoPermIniCliche());
    ordCalcScenario.addOrdCalc(new OrdCalcTransIniCalcCI(CrueConfigMetierForTest.DEFAULT));
    ordCalcScenario.addOrdCalc(new OrdCalcTransIniCalcCliche(CrueConfigMetierForTest.DEFAULT));
    scenario.addInfosEMH(ordCalcScenario);
    CtuluLog result = instance.validate(scenario);
    assertTrue(result.containsSevereError());
    List<CtuluLogRecord> records = new ArrayList<>(result.getRecords());
    assertEquals(3, records.size());
    assertEquals("export.oldCrue10.containOrdCalcPseudoPermIniCliche.error", records.get(0).getMsg());
    assertEquals("export.oldCrue10.containOrdCalcTransIniCalcCI.error", records.get(1).getMsg());
    assertEquals("export.oldCrue10.containOrdCalcTransIniCalcCliche.error", records.get(2).getMsg());
  }
}
