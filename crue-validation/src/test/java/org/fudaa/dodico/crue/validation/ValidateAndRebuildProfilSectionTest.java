/*
 GPL 2
 */
package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.test.ReadHelperForTest;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.metier.helper.EtiquetteIndexed;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

/**
 * @author Frederic Deniger
 */
public class ValidateAndRebuildProfilSectionTest extends AbstractTestParent {
    @Test
    public void testRebuildOn201Profil2() {
        final EMHSectionProfil section = getSectionProfil2();
        final DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection(section);
        final List<LitNumerote> litNumerotes = profilSection.getLitNumerote();
        Assert.assertEquals(5, litNumerotes.size());
        final int nbSto = 2;
        int i;
        for (i = 0; i < nbSto; i++) {
            Assert.assertEquals("Fk_K0", litNumerotes.get(i).getFrot().getNom());
        }
        Assert.assertEquals("Fk_PROF2MIN", litNumerotes.get(i++).getFrot().getNom());
        for (; i < litNumerotes.size(); i++) {
            Assert.assertEquals("Fk_K0", litNumerotes.get(i).getFrot().getNom());
        }
    }

    @Test
    public void testRebuildOn201Profil3() {
        final EMHSectionProfil section = getSectionProfil("St_PROF3");
        final DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection(section);
        final List<LitNumerote> litNumerotes = profilSection.getLitNumerote();
        Assert.assertEquals(5, litNumerotes.size());
        int i = 0;
        Assert.assertEquals("Fk_K0", litNumerotes.get(i++).getFrot().getNom());
        Assert.assertEquals("Fk_PROF3BMAJ", litNumerotes.get(i++).getFrot().getNom());
        Assert.assertEquals("Fk_PROF3BMIN", litNumerotes.get(i++).getFrot().getNom());
        for (; i < litNumerotes.size(); i++) {
            Assert.assertEquals("Fk_K0", litNumerotes.get(i).getFrot().getNom());
        }
    }

    @Test
    public void testRebuildOn201() {
        final EMHSectionProfil section = getSectionProfil();
        final DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection(section);
        Assert.assertNotNull(profilSection);
        final List<LitNumerote> litNumerotes = profilSection.getLitNumerote();
        for (final LitNumerote litNumerote : litNumerotes) {
            Assert.assertNotNull(litNumerote.getNomLit());
            Assert.assertNotNull(litNumerote.getNomLit().getNom());
        }
        final List<DonPrtGeoProfilEtiquette> etiquettes = profilSection.getEtiquettes();
        Assert.assertEquals(2, etiquettes.size());
        final Map<ItemEnum, DonPrtGeoProfilEtiquette> createMap = DonPrtGeoProfilEtiquette.createMap(etiquettes);
        final DonPrtGeoProfilEtiquette thalweg = createMap.get(CrueConfigMetierForTest.DEFAULT.getLitNomme().getEtiquetteThalweg());
        assertDoubleEquals(86d, thalweg.getPoint().getXt());
        assertDoubleEquals(0.5d, thalweg.getPoint().getZ());
        final DonPrtGeoProfilEtiquette axeHyd = createMap.get(CrueConfigMetierForTest.DEFAULT.getLitNomme().getEtiquetteAxeHyd());
        assertDoubleEquals(66d, axeHyd.getPoint().getXt());
        assertDoubleEquals(0.5d, axeHyd.getPoint().getZ());
        final ScenarioAutoModifiedState modifiedState = new ScenarioAutoModifiedState();
        final CrueIOResu<List<EtiquetteIndexed>> etiquettesAsIndexed = new ValidateAndRebuildProfilSection(CrueConfigMetierForTest.DEFAULT,
            modifiedState).getEtiquettesAsIndexed(profilSection);
        Assert.assertTrue(etiquettesAsIndexed.getAnalyse().isEmpty());
        final List<EtiquetteIndexed> etiquettesList = etiquettesAsIndexed.getMetier();
        Assert.assertEquals(2, etiquettesList.size());
        Assert.assertEquals(CrueConfigMetierForTest.DEFAULT.getLitNomme().getEtiquetteThalweg(), etiquettesList.get(0).getEtiquette().
            getTypeEtiquette());
        Assert.assertEquals(5, etiquettesList.get(0).getIdx());
        Assert.assertEquals(CrueConfigMetierForTest.DEFAULT.getLitNomme().getEtiquetteAxeHyd(), etiquettesList.get(1).getEtiquette().getTypeEtiquette());
        Assert.assertEquals(4, etiquettesList.get(1).getIdx());
    }

    @Test
    public void testRebuildWithErrorOnEtiquettesNotInLitMineur() {
        final EMHSectionProfil section = getSectionProfil();
        final DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection(section);
        final DonPrtGeoProfilEtiquette thalweg = new DonPrtGeoProfilEtiquette();
        thalweg.setTypeEtiquette(CrueConfigMetierForTest.DEFAULT.getLitNomme().getEtiquetteThalweg());
        thalweg.setPoint(profilSection.getPtProfil().get(0));
        profilSection.setEtiquettes(Collections.singletonList(thalweg));
        final ScenarioAutoModifiedState modifiedState = new ScenarioAutoModifiedState();
        CtuluLog log = new ValidateAndRebuildProfilSection(CrueConfigMetierForTest.DEFAULT, modifiedState).
            validateAndRebuildDonPrtGeoProfilSection(profilSection);
        Assert.assertTrue(log.containsSevereError());
        CtuluLogRecord record = log.getRecords().get(0);
        Assert.assertEquals("validate.thalweg.NotInLitMineur.Fente", record.getMsg());

        final DonPrtGeoProfilEtiquette axeHyd = new DonPrtGeoProfilEtiquette();
        axeHyd.setTypeEtiquette(CrueConfigMetierForTest.DEFAULT.getLitNomme().getEtiquetteAxeHyd());
        axeHyd.setPoint(profilSection.getPtProfil().get(0));
        profilSection.setEtiquettes(Arrays.asList(thalweg, axeHyd));
        log = new ValidateAndRebuildProfilSection(CrueConfigMetierForTest.DEFAULT, modifiedState).validateAndRebuildDonPrtGeoProfilSection(
            profilSection);
        Assert.assertTrue(log.containsSevereError());
        Assert.assertEquals(2, log.getRecords().size());
        record = log.getRecords().get(0);
        Assert.assertEquals("validate.thalweg.NotInLitMineur.Fente", record.getMsg());
        record = log.getRecords().get(1);
        Assert.assertEquals("validate.axeHyd.NotInLitMineur", record.getMsg());
    }

    @Test
    public void testRebuildWithErrorOnEtiquettesMultiDefinition() {
        final EMHSectionProfil section = getSectionProfil();
        final DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection(section);
        final List<DonPrtGeoProfilEtiquette> etiquettes = new ArrayList<>(profilSection.getEtiquettes());
        Assert.assertEquals(2, etiquettes.size());
        etiquettes.add(etiquettes.get(0));//on definit l'étiquette thalweg 2 fois.
        profilSection.setEtiquettes(etiquettes);
        final ScenarioAutoModifiedState modifiedState = new ScenarioAutoModifiedState();
        final CtuluLog log = new ValidateAndRebuildProfilSection(CrueConfigMetierForTest.DEFAULT, modifiedState).
            validateAndRebuildDonPrtGeoProfilSection(profilSection);
        Assert.assertTrue(log.containsSevereError());
        final CtuluLogRecord record = log.getRecords().get(0);
        Assert.assertEquals("validate.etiquette.definedSeveralTimes", record.getMsg());
        Assert.assertEquals(CrueConfigMetierForTest.DEFAULT.getLitNomme().getEtiquetteThalweg().geti18n(), record.getArgs()[0]);
        Assert.assertEquals(Integer.toString(2), record.getArgs()[1]);
    }

    private EMHSectionProfil getSectionProfil() {
        return getSectionProfil("St_PROF1");
    }

    private EMHSectionProfil getSectionProfil2() {
        return getSectionProfil("St_PROF2");
    }

    private static EMHSectionProfil getSectionProfil(final String sectionName) {
        return getSectionProfil(sectionName, "/ValidateAndRebuildProfilSectionTest/201.dc", "/ValidateAndRebuildProfilSectionTest/201.dh");
    }

    private static EMHSectionProfil getSectionProfil(final String sectionName, final String dc, final String dh) {
        final CtuluLog log = new CtuluLog();
        final CrueIOResu<CrueData> read = new ReadHelperForTest(TestCoeurConfig.INSTANCE).readModeleAndDefaultORES(log, dc, dh);
        Assert.assertFalse(log.containsErrorOrSevereError());
        final EMHScenario scenario = read.getMetier().getScenarioData();
        Assert.assertNotNull(scenario);
        final ScenarioAutoModifiedState modifiedState = new ScenarioAutoModifiedState();
        new ValidateAndRebuildProfilSection(CrueConfigMetierForTest.DEFAULT, modifiedState).validateScenario(scenario);
        Assert.assertTrue(modifiedState.isProfilModified());
        final Map<String, CatEMHSection> toMapOfNom = TransformerHelper.toMapOfNom(scenario.getSections());
        return (EMHSectionProfil) toMapOfNom.get(sectionName);
    }
}
