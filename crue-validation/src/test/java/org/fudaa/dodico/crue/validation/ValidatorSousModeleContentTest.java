/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.test.ReadHelperForTest;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSectionIdem;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author deniger
 */
public class ValidatorSousModeleContentTest {
    public ValidatorSousModeleContentTest() {
    }

    @Test
    public void testIsValid() {
        EMHScenario scenario = new ReadHelperForTest(TestCoeurConfig.INSTANCE).readModeleAnsSetRelation(new CtuluLog(), "/M3-0_c9.dc",
            "/M3-0_c9.dh");
        ValidatorSousModeleContent validator = new ValidatorSousModeleContent();
        List<CtuluLog> validateScenario = validator.validateScenario(scenario);
        assertTrue(validateScenario.isEmpty());
    }

    @Test
    public void testSectionOut() {
        EMHScenario scenario = new ReadHelperForTest(TestCoeurConfig.INSTANCE).readModeleAnsSetRelation(new CtuluLog(), "/M3-0_c9.dc",
            "/M3-0_c9.dh");
        EMHSectionIdem id = new EMHSectionIdem("test");
        EMHSousModele sousModele = new EMHSousModele();
        RelationEMHSectionDansBranche relationSection = new RelationEMHSectionDansBranche();
        relationSection.setEmh(id);
        EMHRelationFactory.addRelationContientEMH(sousModele, id);
        scenario.getBranches().get(0).addListeSections(Collections.singletonList(relationSection));
        ValidatorSousModeleContent validator = new ValidatorSousModeleContent();
        List<CtuluLog> validateScenario = validator.validateScenario(scenario);
        assertEquals(1, validateScenario.size());
        assertEquals(1, validateScenario.get(0).getRecords().size());
        assertEquals("validation.sousModeleContent.sectionNotContained", validateScenario.get(0).getRecords().get(0).getMsg());
    }
}
