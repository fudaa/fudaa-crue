package org.fudaa.fudaa.crue.otfa.node;

import java.awt.Color;
import java.io.File;
import javax.swing.Action;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLine;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertyFileToPath;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.otfa.action.DownLineNodeAction;
import org.fudaa.fudaa.crue.otfa.action.RemoveLineNodeAction;
import org.fudaa.fudaa.crue.otfa.action.UpLineAction;
import org.fudaa.fudaa.crue.otfa.action.UseRelativePathNodeAction;
import org.fudaa.fudaa.crue.otfa.perspective.PerspectiveServiceOtfa;
import org.fudaa.fudaa.crue.otfa.property.CoeurProperty;
import org.fudaa.fudaa.crue.otfa.property.CommentaireProperty;
import org.fudaa.fudaa.crue.otfa.property.ComparaisonsPropertyEditor;
import org.fudaa.fudaa.crue.otfa.property.LogsPropertyEditor;
import org.fudaa.fudaa.crue.otfa.property.OtfaPropertyFileToPath;
import org.fudaa.fudaa.crue.otfa.property.PathProperty;
import org.fudaa.fudaa.crue.otfa.property.RapportProperty;
import org.fudaa.fudaa.crue.otfa.property.ScenarioProperty;
import org.fudaa.fudaa.crue.otfa.service.OtfaService;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport.Reflection;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Christophe CANEL (Genesis)
 */
public class OtfaCampagneLineNode extends AbstractNodeFirable {

  public static final String CIBLE_ETU_FILE_PROPERPTY = "cibleEtuFile";
  public static final String REFERENCE_ETU_FILE_PROPERTY = "referenceEtuFile";
  private final OtfaService otfaService = Lookup.getDefault().lookup(OtfaService.class);
  private final PerspectiveServiceOtfa perspectiveServiceOtfa = Lookup.getDefault().lookup(PerspectiveServiceOtfa.class);

  public enum Indicateur {

    INDETERMINE,
    OK,
    WARNING,
    ERREUR;

    public Color getColor() {
      switch (this) {
        case INDETERMINE: {
          return Color.WHITE;
        }
        case OK: {
          return Color.GREEN;
        }
        case WARNING: {
          return Color.ORANGE;
        }
        case ERREUR: {
          return Color.RED;
        }
      }

      return Color.BLACK;
    }
  }

  @Override
  public boolean isEditMode() {
    return perspectiveServiceOtfa.isInEditMode();
  }

  @Override
  protected void fireObjectChange(String property, Object oldValue, Object newValue) {
    super.fireObjectChange(property, oldValue, newValue);
  }


  public OtfaCampagneLineNode(OtfaCampagneLine line) {
    this(new RTFAResultLine(line));
  }

  public OtfaCampagneLineNode(RTFAResultLine lineResult) {
    super(Children.LEAF, Lookups.singleton(lineResult));
    addPropertyChangeListener(otfaService);
  }

  private RTFAResultLine getData() {
    return getLookup().lookup(RTFAResultLine.class);
  }

  public OtfaCampagneLine getLine() {
    return getData().getInitialLine();
  }

  public RTFAResultLine getResult() {
    return getData();
  }

  public void setResult(RTFAResultLine result) {
    this.getData().initForm(result);
  }

  public void useRelativePath() {
    File otfaDir = otfaService.getCurrentOTFA().getOtfaCampagne().getOtfaDir();
    String etuPath = getLine().getReference().getEtuPath();
    if (StringUtils.isNotBlank(etuPath)) {
      File etuFile = new File(etuPath);
      if (etuFile.isAbsolute()) {
        String relativeFile = CtuluLibFile.getRelativeFile(etuFile, otfaDir, 10);
        if (!StringUtils.equals(relativeFile, etuPath)) {
          getLine().getReference().setEtuPath(relativeFile);
          firePropertyChange(REFERENCE_ETU_FILE_PROPERTY, etuPath, relativeFile);
        }
      }
    }
    etuPath = getLine().getCible().getEtuPath();
    if (StringUtils.isNotBlank(etuPath)) {
      File etuFile = new File(etuPath);
      if (etuFile.isAbsolute()) {
        String relativeFile = CtuluLibFile.getRelativeFile(etuFile, otfaDir, 10);
        if (!StringUtils.equals(relativeFile, etuPath)) {
          getLine().getCible().setEtuPath(relativeFile);
          firePropertyChange(CIBLE_ETU_FILE_PROPERPTY, etuPath, relativeFile);
        }
      }
    }
  }

  @Override
  public Action[] getActions(boolean context) {
    return new Action[]{
      SystemAction.get(UseRelativePathNodeAction.class), null,
      SystemAction.get(RemoveLineNodeAction.class),
      SystemAction.get(UpLineAction.class),
      SystemAction.get(DownLineNodeAction.class)};
  }

  @Override
  public Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    OtfaCampagneLine line = getLine();
    RTFAResultLine data = getData();
    try {
      Property indice = new PropertySupportReflection(this, line, int.class, "getIndice", null);
      Property commentaire = new CommentaireProperty(this, otfaService.getCurrentOTFA().getOtfaCampagne(), line, "getCommentaire", "setCommentaire");
      Property lancerComparaison = new PropertySupportReflection(this, line, boolean.class, "isLancerComparaison", "setLancerComparaison");
      final String logDisplayName = NbBundle.getMessage(OtfaCampagneLineNode.class, "logsName");
      final String logDescription = NbBundle.getMessage(OtfaCampagneLineNode.class, "logsDescription");
      OtfaRtfaLineProperty logs = new OtfaRtfaLineProperty(data, "logs", logDisplayName, logDescription);
      final String comparaisonDisplayName = NbBundle.getMessage(OtfaCampagneLineNode.class, "comparaisonsName");
      final String comparaisonDescription = NbBundle.getMessage(OtfaCampagneLineNode.class, "comparaisonsDescription");
      OtfaRtfaLineProperty comparaisons = new OtfaRtfaLineProperty(data, "comparaisons", comparaisonDisplayName, comparaisonDescription);

      indice.setName("indice");
      indice.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "indiceName"));
      indice.setShortDescription(NbBundle.getMessage(OtfaCampagneLineNode.class, "indiceDescription"));

      commentaire.setName("commentaire");
      commentaire.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "commentName"));
      commentaire.setShortDescription(NbBundle.getMessage(OtfaCampagneLineNode.class, "commentDescription"));

      lancerComparaison.setName("lancerComparaison");
      lancerComparaison.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "lancerComparaisonName"));
      lancerComparaison.setShortDescription(NbBundle.getMessage(OtfaCampagneLineNode.class, "lancerComparaisonDescription"));

      logs.setPropertyEditorClass(LogsPropertyEditor.class);
      comparaisons.setPropertyEditorClass(ComparaisonsPropertyEditor.class);
      if (!data.hasDifferences()) {
        PropertyCrueUtils.configureNoCustomEditor(comparaisons);
      }
      if (!data.hasLog()) {
        PropertyCrueUtils.configureNoCustomEditor(logs);
      }

      set.put(indice);
      set.put(lancerComparaison);
      set.put(commentaire);
      set.put(logs);
      set.put(comparaisons);

    } catch (NoSuchMethodException ex) {
      Exceptions.printStackTrace(ex);
    }

    sheet.put(set);
    sheet.put(createSetForReference(line));
    sheet.put(createSetForCible(line));

    return sheet;
  }

  @Override
  public boolean hasCustomizer() {
    return super.hasCustomizer();
  }

  private void configureFileEditor(PropertyFileToPath etude) {
    final OtfaCampagne otfaCampagne = otfaService.getCurrentOTFA().getOtfaCampagne();
    etude.setValue("baseDir", otfaCampagne.getOtfaDir());
    etude.setValue("currentDir", otfaCampagne.getOtfaDir());
    etude.setValue("directories", Boolean.FALSE);
    etude.setValue("files", Boolean.TRUE);
    etude.setValue("nullValue", StringUtils.EMPTY);

  }

  private Sheet.Set createSetForReference(final OtfaCampagneLine line) {
    Sheet.Set set = Sheet.createPropertiesSet();

    set.setName("reference");
    set.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "reference"));

    try {
      OtfaPropertyFileToPath etude = new OtfaPropertyFileToPath() {
        @Override
        protected String getFileAsString() {
          return line.getReference().getEtuPath();
        }

        @Override
        protected void setFileAsString(String path) {
          String old = getFileAsString();
          line.getReference().setEtuPath(path);
          OtfaCampagneLineNode.this.firePropertyChange(getName(), old, path);
        }
      };
      etude.setCanWrite(true);

      final OtfaCampagne otfaCampagne = otfaService.getCurrentOTFA().getOtfaCampagne();
      configureFileEditor(etude);

      PropertySupportReflection scenario = new ScenarioProperty(this, otfaCampagne, line.getReference(), "getScenarioNom", "setScenarioNom");
      CoeurProperty coeur = new CoeurProperty(this, line.getReference(), String.class, "getCoeurName", "setCoeurName", false);
      Property lancerCalcul = new PropertySupportReflection(this, line.getReference(), boolean.class, "isLaunchCompute", "setLaunchCompute");
      Property rapportOnTransitoire = new PropertySupportReflection(this, line.getReference(), boolean.class, "isRapportOnTransitoire",
              "setRapportOnTransitoire");

      Property cheminCSV = new PathProperty(this, otfaService.getCurrentOTFA().getOtfaCampagne(), line.getReference(), "getCheminCSV", "setCheminCSV");

      Property rapport = new RapportProperty(this, otfaService.getCurrentOTFA().getOtfaCampagne(), line.getReference(), "getRapport", "setRapport");

      etude.setName(REFERENCE_ETU_FILE_PROPERTY);
      etude.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "etudeName"));
      etude.setShortDescription(NbBundle.getMessage(OtfaCampagneLineNode.class, "etudeDescription"));

      scenario.setName("referenceScenarioNom");
      scenario.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "scenarioName"));
      scenario.setShortDescription(NbBundle.getMessage(OtfaCampagneLineNode.class, "scenarioDescription"));

      coeur.setName("referenceCoeurName");
      coeur.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "coeurName"));
      coeur.setShortDescription(NbBundle.getMessage(OtfaCampagneLineNode.class, "coeurDescription"));

      lancerCalcul.setName("referenceLaunchCompute");
      lancerCalcul.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "launchName"));
      lancerCalcul.setShortDescription(NbBundle.getMessage(OtfaCampagneLineNode.class, "launchDescription"));

      rapportOnTransitoire.setName("referenceRapportOnTransitoire");
      rapportOnTransitoire.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "rapportOnTransitoireName"));
      rapportOnTransitoire.setShortDescription(NbBundle.getMessage(OtfaCampagneLineNode.class, "rapportOnTransitoireDescription"));

      cheminCSV.setName("referenceCheminCSV");
      cheminCSV.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "cheminCSVName"));
      cheminCSV.setShortDescription(NbBundle.getMessage(OtfaCampagneLineNode.class, "cheminCSVDescription"));
      cheminCSV.setValue("nullValue", StringUtils.EMPTY);

      rapport.setName("referenceRapport");
      rapport.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "rapportName"));
      rapport.setShortDescription(NbBundle.getMessage(OtfaCampagneLineNode.class, "rapportDescription"));
      PropertyCrueUtils.configureNoEditAsText(rapport);
      set.put(etude);
      set.put(scenario);
      set.put(coeur);
      set.put(lancerCalcul);
      set.put(rapport);
      set.put(rapportOnTransitoire);
      set.put(cheminCSV);
    } catch (NoSuchMethodException ex) {
      Exceptions.printStackTrace(ex);
    }

    return set;
  }

  private Sheet.Set createSetForCible(final OtfaCampagneLine line) {
    Sheet.Set set = Sheet.createPropertiesSet();

    set.setName("cible");
    set.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "cible"));

    try {

      OtfaPropertyFileToPath etude = new OtfaPropertyFileToPath() {
        @Override
        protected String getFileAsString() {
          return StringUtils.defaultString(line.getCible().getEtuPath());
        }

        @Override
        protected void setFileAsString(String path) {
          String old = getFileAsString();
          line.getCible().setEtuPath(path);
          OtfaCampagneLineNode.this.firePropertyChange(getName(), old, path);
        }
      };
      etude.setCanWrite(true);
      configureFileEditor(etude);
      Property scenario = new ScenarioProperty(this, otfaService.getCurrentOTFA().getOtfaCampagne(), line.getCible(), "getScenarioNom",
              "setScenarioNom");
      Reflection coeur = new CoeurProperty(this, line.getCible(), String.class, "getCoeurName", "setCoeurName", true);
      Property lancerCalcul = new PropertySupportReflection(this, line.getCible(), boolean.class, "isLaunchCompute", "setLaunchCompute");

      Property rapportOnTransitoire = new PropertySupportReflection(this, line.getCible(), boolean.class, "isRapportOnTransitoire",
              "setRapportOnTransitoire");

      Property cheminCSV = new PathProperty(this, otfaService.getCurrentOTFA().getOtfaCampagne(), line.getCible(), "getCheminCSV", "setCheminCSV");

      Property rapport = new RapportProperty(this, otfaService.getCurrentOTFA().getOtfaCampagne(), line.getCible(), "getRapport",
              "setRapport");

      etude.setName(CIBLE_ETU_FILE_PROPERPTY);
      etude.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "etudeName"));
      etude.setShortDescription(NbBundle.getMessage(OtfaCampagneLineNode.class, "etudeDescription"));

      scenario.setName("cibleScenarioNom");
      scenario.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "scenarioName"));
      scenario.setShortDescription(NbBundle.getMessage(OtfaCampagneLineNode.class, "scenarioDescription"));

      coeur.setName("cibleCoeurName");
      coeur.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "coeurName"));
      coeur.setShortDescription(NbBundle.getMessage(OtfaCampagneLineNode.class, "coeurDescription"));

      lancerCalcul.setName("cibleLaunchCompute");
      lancerCalcul.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "launchName"));
      lancerCalcul.setShortDescription(NbBundle.getMessage(OtfaCampagneLineNode.class, "launchDescription"));

      rapportOnTransitoire.setName("cibleRapportOnTransitoire");
      rapportOnTransitoire.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "rapportOnTransitoireName"));
      rapportOnTransitoire.setShortDescription(NbBundle.getMessage(OtfaCampagneLineNode.class, "rapportOnTransitoireDescription"));

      cheminCSV.setName("cibleCheminCSV");
      cheminCSV.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "cheminCSVName"));
      cheminCSV.setShortDescription(NbBundle.getMessage(OtfaCampagneLineNode.class, "cheminCSVDescription"));
      cheminCSV.setValue("nullValue", StringUtils.EMPTY);

      rapport.setName("cibleRapport");
      rapport.setDisplayName(NbBundle.getMessage(OtfaCampagneLineNode.class, "rapportName"));
      rapport.setShortDescription(NbBundle.getMessage(OtfaCampagneLineNode.class, "rapportDescription"));
      PropertyCrueUtils.configureNoEditAsText(rapport);

      set.put(etude);
      set.put(scenario);
      set.put(coeur);
      set.put(lancerCalcul);
      set.put(rapport);
      set.put(rapportOnTransitoire);
      set.put(cheminCSV);
    } catch (NoSuchMethodException ex) {
      Exceptions.printStackTrace(ex);
    }

    return set;
  }
}
