package org.fudaa.fudaa.crue.otfa;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.NbSheetCustom;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.otfa//OtfaPropertiesTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = OtfaPropertiesTopComponent.TOPCOMPONENT_ID,
        persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "otfa-topLeft", openAtStartup = false)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.otfa.OtfaPropertiesTopComponent")
@TopComponent.OpenActionRegistration(displayName = "#CTL_OTFAPropertiesAction",
        preferredID = OtfaPropertiesTopComponent.TOPCOMPONENT_ID)
@ActionReference(path = "Menu/Window/OTFA", position = 2)
public final class OtfaPropertiesTopComponent extends NbSheetCustom {

  public static final String TOPCOMPONENT_ID = "OtfaPropertiesTopComponent";

  public OtfaPropertiesTopComponent() {
    super(true, PerspectiveEnum.TEST);
    setName(NbBundle.getMessage(OtfaPropertiesTopComponent.class, "CTL_OTFAPropertiesAction"));
    setToolTipText(NbBundle.getMessage(OtfaPropertiesTopComponent.class, "HINT_OTFAPropertiesAction"));
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getTopComponentHelpCtxId("vueProprietes", PerspectiveEnum.TEST));
  }

  void writeProperties(java.util.Properties p) {
    p.setProperty("version", "1.0");
  }

  void readProperties(java.util.Properties p) {
  }
}
