/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.node;

import java.util.Collections;
import java.util.List;
import javax.swing.Action;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;
import org.fudaa.fudaa.crue.otfa.process.OtfaCampagneContainer;
import org.fudaa.fudaa.crue.otfa.service.OtfaService;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

/**
 *Contient les informations relative à la campagne
 * @author Christophe CANEL (Genesis)
 */
public class OtfaCampagneNode extends AbstractNode implements ChangeListener {

  public static final String MAIN_INFOS_CHANGES_PROPERTY = "MAIN_INFOS_CHANGES";
  private final OtfaCampagneLineChildFactory childFactory;
  final OtfaService otfaService = Lookup.getDefault().lookup(OtfaService.class);

  public OtfaCampagneNode(OtfaCampagneContainer container) {
    super(Children.LEAF, Lookups.singleton(container));
    childFactory = new OtfaCampagneLineChildFactory();
    setChildren(Children.create(childFactory, true));
    setDisplayName(NbBundle.getMessage(OtfaCampagneNode.class, "lines"));
    addPropertyChangeListener(otfaService);
  }

  @Override
  public Action[] getActions(boolean context) {
    return new Action[]{};
  }

  /**
   * Utilisé uniquement pour rafraîchir les données de la campagne
   */
  public void fireSaveDone() {
    firePropertyChange(MAIN_INFOS_CHANGES_PROPERTY, Boolean.FALSE, Boolean.TRUE);
  }

  @Override
  public void stateChanged(ChangeEvent e) {
    final List<OtfaCampagneLine> lines = getLookup().lookup(List.class);

    Collections.sort(lines, new OtfaCampagneLineComparator());
  }

  /**
   * Supprime la ligne en paramètre de la grille
   * @param lineNode la ligne à supprimer
   */
  public void remove(OtfaCampagneLineNode lineNode) {
    final List<OtfaCampagneLine> lines = getLookup().lookup(OtfaCampagneContainer.class).getOtfaCampagne().getLines();
    lines.remove(lineNode.getLine());
    repaint();    
  }
  
  /**
   * Supprime les lignes en paramètre de la grille
   * @param lineNodes les lignes à supprimer
   */
  public void removeAll(List<OtfaCampagneLine> lineNodes) {
    final List<OtfaCampagneLine> lines = getLookup().lookup(OtfaCampagneContainer.class).getOtfaCampagne().getLines();
    lines.removeAll(lineNodes);
    repaint();
  }
  
  /**
   * Renumérotation des lignes de la grille
   */
  public void renumeroteAll() {
    final List<OtfaCampagneLine> lines = getLookup().lookup(OtfaCampagneContainer.class).getOtfaCampagne().getLines();
    //on renumérote les lignes:
    for (int i = 0; i < lines.size(); i++) {
      OtfaCampagneLine otfaCampagneLine = lines.get(i);
      int viewIndex = otfaService.getView().getOutline().convertRowIndexToView(i);
      otfaCampagneLine.setIndice(viewIndex + 1);
    }
    repaint();
  }

  /**
   * Echange dans la grille des lignes en paramètre (grille non triée)
   * @param line1 la première ligne à échanger
   * @param line2 la seconde ligne à échanger
   */
  public void exchange(OtfaCampagneLineNode line1, OtfaCampagneLineNode line2) {
    int indice1 = line1.getLine().getIndice();
    int indice2 = line2.getLine().getIndice();
    line1.getLine().setIndice(indice2);
    line2.getLine().setIndice(indice1);
    repaint();
  }

  /**
   * Ajoute la ligne à l'index indiqué
   * @param lineNode la ligne à ajouter
   * @param index l'index où il faut ajouter la ligne, dans la vue
   */
  public void add(OtfaCampagneLineNode lineNode, int index) {
    final OtfaCampagneContainer container = this.getLookup().lookup(OtfaCampagneContainer.class);
    List<OtfaCampagneLine> lines = container.getOtfaCampagne().getLines();
    // lecture de l'index dans la grille (si tri)
    int newIndex = otfaService.getView().getOutline().convertRowIndexToView(index - 1);
    if (newIndex >= lines.size() - 1) {
      add(lineNode);
    } else {
      lines.add(newIndex + 1, lineNode.getLine());
      // ne pas appeller la méthode repaint ici car des accès concurrentiels 
      // peuvent avoir lieu vu que cette méthode est appelé dans une boucle for
    }
  }

  /**
   * Ajout de la ligne en paramètre en fin de grille
   * @param lineNode la ligne à ajouter
   */
  public void add(OtfaCampagneLineNode lineNode) {
    final OtfaCampagneLine line = lineNode.getLine();
    final OtfaCampagneContainer container = this.getLookup().lookup(OtfaCampagneContainer.class);
    List<OtfaCampagneLine> lines = container.getOtfaCampagne().getLines();
    line.setIndice(lines.size() + 1);
    lines.add(line);
    repaint();
  }
  
  /**
   * Rafraîchissement de la grille
   */
  public void repaint() {
    childFactory.refresh(); //envoie un evt
  }
}
