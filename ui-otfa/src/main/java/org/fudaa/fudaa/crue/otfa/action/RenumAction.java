/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.action;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.NbBundle;

/**
 *
 * @author Yoan GRESSIER
 */
public class RenumAction extends AbstractOtfaAction {

  public RenumAction() {
    super("RenumAction.ActionName");
    putValue(Action.NAME, NbBundle.getMessage(AddFileAction.class, "RenumAction.ActionName"));
    putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(AddFileAction.class, "RenumAction.ShortDescription"));
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    otfaService.getNodes().renumeroteAll();
    NotificationDisplayer.getDefault().notify(NbBundle.getMessage(RenumAction.class, "RenumActionDone.title"), CrueIconsProvider.getIcon(
            "org/fudaa/fudaa/crue/common/icons/information.png"), NbBundle.getMessage(
                    RenumAction.class, "RenumActionDone.message"), null);
  }
}
