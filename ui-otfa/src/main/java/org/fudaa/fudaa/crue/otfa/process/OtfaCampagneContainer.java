package org.fudaa.fudaa.crue.otfa.process;

import java.util.ArrayList;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLine;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLines;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;
import org.fudaa.fudaa.crue.otfa.service.OtfaService;
import org.openide.util.NbBundle;

/**
 * Classe de données pour les données relatives à une campagne qui contient<br>
 * <ul>
 * <li>la campagne</li>
 * <li>les résultats de la campagne</li>
 * <li>les logs</li>
 * </ul>
 *
 * @author deniger
 */
public class OtfaCampagneContainer {

  /**
   *
   * @return des données de campagnes initiales.
   */
  public static OtfaCampagneContainer createBlankCampagne() {
    final OtfaCampagne campagne = new OtfaCampagne();
    final OtfaCampagne.OtfaRunOptions ref = new OtfaCampagne.OtfaRunOptions();
    ref.setEffacerRunAvant(false);
    ref.setEffacerRunApres(true);
    final OtfaCampagne.OtfaRunOptions cible = new OtfaCampagne.OtfaRunOptions();
    cible.setEffacerRunAvant(true);
    cible.setEffacerRunApres(false);
    campagne.setCommentaire(NbBundle.getMessage(OtfaService.class, "newCampaignComment"));
    campagne.setAuteurCreation(null);
    campagne.setDateCreation(null);
    campagne.setAuteurModification(null);
    campagne.setDateModification(null);
    campagne.setReferenceOptions(ref);
    campagne.setCibleOptions(cible);
    campagne.setLines(new ArrayList<>());
    return new OtfaCampagneContainer(campagne);
  }

  private final OtfaCampagne otfaCampagne;
  private RTFAResultLines result;
  private CtuluLogGroup batchValidation;

  public CtuluLogGroup getBatchValidation() {
    return batchValidation;
  }

  public void setBatchValidation(CtuluLogGroup batchValidation) {
    this.batchValidation = batchValidation;
  }

  public OtfaCampagne getOtfaCampagne() {
    return otfaCampagne;
  }

  public OtfaCampagneContainer(OtfaCampagne otfaCampagne) {
    this.otfaCampagne = otfaCampagne;
  }

  public OtfaCampagneContainer(OtfaCampagne otfaCampagne, RTFAResultLines result) {
    this.otfaCampagne = otfaCampagne;
    this.result = result;
  }

  public void setResult(RTFAResultLines result) {
    this.result = result;
  }

  public RTFAResultLine getResultFor(OtfaCampagneLine line) {
    if ((this.result == null) || (this.result.getLines() == null)) {
      return null;
    }

    for (RTFAResultLine lineResult : this.result.getLines()) {
      if (lineResult.getInitialLine().getIndice() == line.getIndice()) {
        return lineResult;
      }
    }

    return null;
  }

  public RTFAResultLines getResult() {
    return this.result;
  }
}
