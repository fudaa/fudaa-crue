package org.fudaa.fudaa.crue.otfa.property;

import java.awt.Component;
import java.beans.PropertyEditorSupport;
import javax.swing.JDialog;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.otfa.perspective.PerspectiveServiceOtfa;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Yoan GRESSIER
 */
public class CommentairePropertyEditorSupport extends PropertyEditorSupport implements ExPropertyEditor {

  // l'élément couremment éditer, sous 2 formes
  private final OtfaCampagne campagne;
  private final OtfaCampagneLine campagneligne;
  // l'environnement de mise à jour de la propriété
  private PropertyEnv env;
  // lookup sur le service OTFA
  final PerspectiveServiceOtfa perspectiveServiceOtfa = Lookup.getDefault().lookup(PerspectiveServiceOtfa.class);

  public CommentairePropertyEditorSupport(OtfaCampagne campagne, OtfaCampagneLine ligne) {
    this.campagne = campagne;
    this.campagneligne = ligne;
  }

  public OtfaCampagneLine getCampagneligne() {
    return campagneligne;
  }

  public OtfaCampagne getCampagne() {
    return campagne;
  }

  @Override
  public void attachEnv(PropertyEnv env) {
    this.env = env;
  }

  @Override
  public Component getCustomEditor() {
    JDialog jd;
    // encapsulation dans un JDialog pour gérer la persistence
    if (perspectiveServiceOtfa.isInEditMode()) {
      jd = DialogHelper.createOKCancelDialog(new CommentaireCustomEditor(this, env).getComponent(), this.getClass().getName(), NbBundle.getMessage(
              CommentairePropertyEditorSupport.class, "commentaireDisplayerTitle"), true, env, StringUtils.EMPTY);
    } else {
      jd = DialogHelper.createCloseDialog(new CommentaireCustomEditor(this, env).getComponent(), this.getClass().getName(), NbBundle.getMessage(
              CommentairePropertyEditorSupport.class, "commentaireDisplayerTitle"), true, StringUtils.EMPTY);
    }
    return jd;
  }

  @Override
  public boolean supportsCustomEditor() {
    return true;
  }

  @Override
  public String getAsText() {
    return (String) getValue();
  }

  @Override
  public void setAsText(String string) {
    setValue(string);
  }
}
