package org.fudaa.fudaa.crue.otfa.perspective;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractPerspectiveAction;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;

/**
 * Permet d'activer une perspective. Action utilisée dans la toobar pour changer de perspective. Pour la gestion des ordres des menus, voir le fichier
 * layer.xml
 */
@ActionID(category = "File", id = "org.fudaa.fudaa.crue.otfa.perspective.ActiveOtfa")
@ActionRegistration(displayName = "#CTL_ActiveOtfa")
@ActionReference(path = "Menu/Window", position = 6)
public final class ActiveOtfa extends AbstractPerspectiveAction {

  public ActiveOtfa() {
    super("CTL_ActiveOtfa", PerspectiveServiceOtfa.class.getName(), PerspectiveEnum.TEST);
    setBooleanState(false);
  }

  @Override
  protected String getFolderAction() {
    return "Actions/OTFA";
  }
}
