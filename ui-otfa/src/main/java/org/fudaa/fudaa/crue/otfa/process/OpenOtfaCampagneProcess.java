/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.process;

import java.io.File;
import java.net.MalformedURLException;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.xml.XmlVersionFinder;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.otfa.CrueOTFAReaderWriter;
import org.fudaa.dodico.crue.io.otfa.OtfaFileUtils;
import org.fudaa.dodico.crue.io.rtfa.CrueRTFAReaderWriter;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLines;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 *
 * @author Chris
 */
public class OpenOtfaCampagneProcess implements ProgressRunnable<OtfaCampagneContainer> {

  private final File otfaFile;

  public OpenOtfaCampagneProcess(File otfaFile) {
    this.otfaFile = otfaFile;
  }

  @Override
  public OtfaCampagneContainer run(ProgressHandle handle) {
    handle.switchToIndeterminate();
    
    final CrueOTFAReaderWriter otfaReader = new CrueOTFAReaderWriter("1.2", otfaFile);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueIOResu<OtfaCampagne> otfaResult = otfaReader.readXML(otfaFile, log);
    if (log.isNotEmpty()) {
      LogsDisplayer.displayError(log, NbBundle.getMessage(OpenOtfaCampagneProcess.class, "OpenOtfa.BilanDialog",
              otfaFile.getName()));
    }

    File rtfaFile = OtfaFileUtils.getRtfaFile(otfaFile);
    RTFAResultLines rtfaCampagnResult = null;

    if (rtfaFile.isFile()) {
      try {
        XmlVersionFinder finder = new XmlVersionFinder();
        String version = null;
        version = finder.getVersion(rtfaFile.toURI().toURL());
        if ("1.2".equals(version)) {
          DialogHelper.showError(NbBundle.getMessage(OpenOtfaCampagneProcess.class, "RtfaVersion.Old"));
        } else if (version != null) {
          final CrueRTFAReaderWriter rtfaReader = new CrueRTFAReaderWriter("1.3");
          final CrueIOResu<RTFAResultLines> rtfaResult = rtfaReader.readXML(rtfaFile, log);

          rtfaCampagnResult = rtfaResult.getMetier();
        }
      } catch (MalformedURLException ex) {
        Exceptions.printStackTrace(ex);
      }
    }
    
    handle.finish();
    OtfaCampagneContainer container = new OtfaCampagneContainer(otfaResult.getMetier(), rtfaCampagnResult);
    if (rtfaCampagnResult != null && rtfaCampagnResult.isBatch() && rtfaCampagnResult.getGlobalValidation() != null) {//autre test ?
      container.setBatchValidation(rtfaCampagnResult.getGlobalValidation());
    }
    if (log.containsErrorOrSevereError()) {
      LogsDisplayer.displayError(log, NbBundle.getMessage(OpenOtfaCampagneProcess.class, "OpenOtfa.BilanDialog",
              otfaFile.getName()));
    }

    return container;
  }
}
