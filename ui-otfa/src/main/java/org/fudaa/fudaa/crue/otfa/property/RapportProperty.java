package org.fudaa.fudaa.crue.otfa.property;

import java.beans.PropertyEditor;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLine;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneItem;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.otfa.perspective.PerspectiveServiceOtfa;
import org.openide.util.Lookup;

/**
 *
 * @author F deniger
 */
public class RapportProperty extends PropertySupportReflection<String> {

  private final OtfaCampagne campagne;
  final PerspectiveServiceOtfa perspectiveServiceOtfa = Lookup.getDefault().lookup(PerspectiveServiceOtfa.class);

  public RapportProperty(AbstractNodeFirable node, OtfaCampagne campagne, OtfaCampagneItem instance, String getter, String setter) throws NoSuchMethodException {
    super(node, instance, String.class, getter, setter);
    this.campagne = campagne;
  }

  @Override
  public boolean canWrite() {
    return super.canWrite() && perspectiveServiceOtfa.isInEditMode();
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    final RTFAResultLine line = node.getLookup().lookup(RTFAResultLine.class);
    return new RapportPropertyEditorSupport(campagne, line == null ? null : line.getInitialLine(), (OtfaCampagneItem) instance);
  }
}
