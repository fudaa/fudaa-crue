/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.action;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneItem;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;
import org.fudaa.fudaa.crue.otfa.node.OtfaCampagneLineNode;
import org.fudaa.fudaa.crue.otfa.node.OtfaCampagneNode;
import org.openide.util.NbBundle;

/**
 *
 * @author Christophe CANEL (Genesis)
 */
public class AddLineAction extends AbstractOtfaAction {

  public AddLineAction() {
    super("AddLineAction.ActionName");
    putValue(Action.NAME, NbBundle.getMessage(AddFileAction.class, "AddLineAction.ActionName"));
    putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(AddFileAction.class, "AddLineAction.ShortDescription"));
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    final OtfaCampagneLine line = createDefaultCampagneLine();
    final OtfaCampagneLineNode lineNode = new OtfaCampagneLineNode(line);
    final OtfaCampagneNode lines = otfaService.getNodes();
    lines.add(lineNode);
  }

  public static OtfaCampagneLine createDefaultCampagneLine() {
    final OtfaCampagneLine line = new OtfaCampagneLine();

    line.setCommentaire("");
    line.setReference(createDefaultCampagneItem());
    line.setCible(createDefaultCampagneItem());

    return line;
  }

  private static OtfaCampagneItem createDefaultCampagneItem() {
    OtfaCampagneItem item = new OtfaCampagneItem();

    item.setEtuPath("");
    item.setScenarioNom("");
    item.setCoeurName("");

    return item;
  }
}
