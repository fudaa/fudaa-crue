package org.fudaa.fudaa.crue.otfa.action;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.openide.util.NbBundle;

/**
 *
 * @author Chris
 */
public class ValidateCampagneAction extends AbstractOtfaAction {

  public ValidateCampagneAction() {
    super("ValidateCampagneAction.ActionName");
    putValue(Action.NAME, NbBundle.getMessage(AddFileAction.class, "ValidateCampagneAction.ActionName"));
    putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(AddFileAction.class, "ValidateCampagneAction.ShortDescription"));
  }

  @Override
  protected void updateEnabledState() {
    setEnabled(otfaService.getCurrentOTFA() != null);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    otfaService.validate();
    if (!otfaService.isValid()) {
      LogsDisplayer.displayError(otfaService.getCurrentValidationState(), (String) getValue(NAME));
    } else {
      DialogHelper.showNotifyOperationTermine(org.openide.util.NbBundle.getMessage(ValidateCampagneAction.class, "ValidateCampagneAction.IsValidMessage"));
    }
  }
}
