/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.action;

import javax.swing.Action;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;
import org.fudaa.fudaa.crue.otfa.node.OtfaCampagneLineNode;
import org.fudaa.fudaa.crue.otfa.node.OtfaCampagneNode;
import org.fudaa.fudaa.crue.otfa.service.OtfaService;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 * Action de création d'une ligne à l'indice de la sélection courante
 * @author Yoan GRESSIER
 */
public class AddLineAtAction extends OtfaEditNodeAction {
  
  final OtfaService otfaService = Lookup.getDefault().lookup(OtfaService.class);
  
  public AddLineAtAction() {
    super(NbBundle.getMessage(AddLineAtAction.class, "AddLineAtAction.ActionName"));
    putValue(Action.NAME, NbBundle.getMessage(AddFileAction.class, "AddLineAtAction.ActionName"));
    putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(AddFileAction.class, "AddLineAtAction.ShortDescription"));
  }

  @Override
  protected void performAction(Node[] nodes) {
    final OtfaCampagneLineNode name = (OtfaCampagneLineNode) nodes[0];
    final OtfaCampagneLine line = AddLineAction.createDefaultCampagneLine();
    line.setIndice(name.getLine().getIndice());
    final OtfaCampagneLineNode lineNode = new OtfaCampagneLineNode(line);
    final OtfaCampagneNode lines = otfaService.getNodes();
    
    // insertion à l'indice de la sélection courante
    lines.add(lineNode, name.getLine().getIndice());
    lines.repaint();
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    if (otfaService.isSorted()){
      return false;
    }
    
    return (activatedNodes.length == 1);
  }
}
