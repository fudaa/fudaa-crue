/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.filter;

import java.io.File;

/**
 *
 * @author Chris
 */
public abstract class FileFilter extends javax.swing.filechooser.FileFilter {

  private final String extension;

  public FileFilter(String extension) {
    this.extension = "." + extension;
  }

  @Override
  public boolean accept(File f) {
    if ((f == null) || f.isHidden()) {
      return false;
    }
    //pour voir les répertoires !
    if (f.isDirectory()) {
      return true;
    }

    if (this.extension == null) {
      return true;
    }

    return this.hasCorrectExtension(f);
  }

  public File getFileWithExtension(File file) {
    if (this.hasCorrectExtension(file)) {
      return file;
    }

    return new File(file.getAbsolutePath() + this.extension);
  }

  private boolean hasCorrectExtension(File file) {
    return file.getName().endsWith(extension);
  }
}
