/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.action;

import java.util.ArrayList;
import javax.swing.Action;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.otfa.node.OtfaCampagneLineNode;
import org.fudaa.fudaa.crue.otfa.node.OtfaCampagneNode;
import org.fudaa.fudaa.crue.otfa.service.OtfaService;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Christophe CANEL (Genesis)
 */
public class RemoveLineNodeAction extends AbstractEditNodeAction {

  OtfaService otfaService = Lookup.getDefault().lookup(OtfaService.class);

  public RemoveLineNodeAction() {
    super(NbBundle.getMessage(RemoveLineNodeAction.class, "RemoveLineNodeAction.ActionName"));
    putValue(Action.NAME, NbBundle.getMessage(AddFileAction.class, "RemoveLineNodeAction.ActionName"));
    putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(AddFileAction.class, "RemoveLineNodeAction.ShortDescription"));
  }

  @Override
  protected void performAction(Node[] nodes) {
    final Node currentNode = nodes[0];
    // Lecture de la node parent
    final OtfaCampagneNode parentNode = (OtfaCampagneNode) currentNode.getParentNode();
    // Construction de la liste de OtfaCampagneLine à supprimer
    ArrayList<OtfaCampagneLine> ar = new ArrayList<>();
    for (Node node : nodes) {
      OtfaCampagneLineNode lineNode = (OtfaCampagneLineNode) node;
      ar.add(lineNode.getLine());
    }
    // Suppression des OtfaCampagneLine
    parentNode.removeAll(ar);
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected boolean isEnable(Node[] nodes) {
    if ( nodes.length < 1) {
      return false;
    }

    return (nodes[0] instanceof OtfaCampagneLineNode);
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }
}
