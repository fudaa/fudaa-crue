/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.action;

import javax.swing.Action;
import org.fudaa.fudaa.crue.otfa.node.OtfaCampagneLineNode;
import org.fudaa.fudaa.crue.otfa.service.OtfaService;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Christophe CANEL (Genesis)
 */
public class DownLineNodeAction extends OtfaEditNodeAction {
  
  final OtfaService otfaService = Lookup.getDefault().lookup(OtfaService.class);

  public DownLineNodeAction() {
    super(NbBundle.getMessage(DownLineNodeAction.class, "DownLineNodeAction.ActionName"));
    putValue(Action.NAME, NbBundle.getMessage(AddFileAction.class, "DownLineNodeAction.ActionName"));
    putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(AddFileAction.class, "DownLineNodeAction.ShortDescription"));
  }

  @Override
  protected boolean isEnable(Node[] nodes) {
    if (otfaService.isSorted()){
      return false;
    }
    
    if (nodes.length != 1) {
      return false;
    }

    final Node node = nodes[0];

    if (node instanceof OtfaCampagneLineNode) {
      int indice = ((OtfaCampagneLineNode) node).getLine().getIndice();
      int maxIndice = node.getParentNode().getChildren().getNodesCount();

      return (indice < maxIndice);
    }

    return false;
  }

  @Override
  protected void performAction(Node[] nodes) {
    moveLineNode((OtfaCampagneLineNode) nodes[0], true);
  }
}
