package org.fudaa.fudaa.crue.otfa.service;

import java.awt.EventQueue;
import java.util.Map;
import java.util.Map.Entry;
import javax.swing.JComponent;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLines;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;
import org.fudaa.dodico.crue.projet.otfa.OtfaExecutor;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.otfa.MessagesOtfa;
import org.fudaa.fudaa.crue.otfa.OtfaCampagneTopComponent;
import org.fudaa.fudaa.crue.otfa.perspective.PerspectiveServiceOtfa;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.util.Cancellable;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

/**
 *
 * @author Chris
 */
public class LaunchOtfaCampagneRunner implements Cancellable, Runnable {

  private final OtfaExecutor executor;
  private final OtfaCampagne campaign;
  Thread thread;
  private final OtfaService otfaService;
  final PerspectiveServiceOtfa perspectiveServiceOtfa = Lookup.getDefault().lookup(PerspectiveServiceOtfa.class);

  public LaunchOtfaCampagneRunner(OtfaExecutor executor, OtfaCampagne campaign,
          final OtfaService otfaService) {
    this.executor = executor;
    this.campaign = campaign;
    this.otfaService = otfaService;
  }

  @Override
  public boolean cancel() {
    if (thread != null) {
      thread.interrupt();
      executor.stop();
    }
    perspectiveServiceOtfa.setDirty(false);
    ph.finish();
    return true;
  }
  ProgressHandle ph;
  Map<JComponent, Boolean> calculLaunched;

  public void go() {
    ph = ProgressHandleFactory.createHandle(NbBundle.getMessage(LaunchOtfaCampagneRunner.class,
            "computingCampaign"), this);
    ph.start();
    ph.switchToIndeterminate();
    perspectiveServiceOtfa.setDirty(true);
    OtfaCampagneTopComponent findTopComponent = (OtfaCampagneTopComponent) WindowManager.getDefault().findTopComponent(
            "OtfaCampagneTopComponent");
    calculLaunched = findTopComponent.calculLaunched();
    thread = new Thread(this);
    thread.start();
  }

  private class ProgressAdapter implements ProgressionInterface {

    @Override
    public void setProgression(int _v) {
      ph.progress(Math.min(100, _v));

    }

    @Override
    public void setDesc(String _s) {
      ph.progress(_s);
    }

    @Override
    public void reset() {
    }

  }

  @Override
  public void run() {
    try {
      ph.switchToDeterminate(100);//100%
      final RTFAResultLines result = executor.launch(this.campaign, new ProgressAdapter());
      EventQueue.invokeLater(new Runnable() {
        @Override
        public void run() {
          if (calculLaunched != null) {
            for (Entry<JComponent, Boolean> entry : calculLaunched.entrySet()) {
              entry.getKey().setEnabled(entry.getValue());
            }
          }
          try {
            if (result != null) {
              otfaService.setResultat(result);
              LogsDisplayer.displayError(result.getGlobalValidation(), MessagesOtfa.getMessage("OtfaBilanExecution.DialogTitle"));
            }
          } finally {
            ph.finish();

            perspectiveServiceOtfa.setDirty(false);

          }
        }
      });
    } catch (Exception e) {
      Exceptions.printStackTrace(e);
      EventQueue.invokeLater(new Runnable() {
        @Override
        public void run() {
          perspectiveServiceOtfa.setDirty(false);
          ph.finish();
        }
      });
    }
  }
}
