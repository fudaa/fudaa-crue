package org.fudaa.fudaa.crue.otfa.property;

import javax.swing.JComponent;
import javax.swing.JTextField;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.fudaa.crue.common.editor.CustomEditorAbstract;
import org.fudaa.fudaa.crue.otfa.perspective.PerspectiveServiceOtfa;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.Lookup;

/**
 *
 * @author gressier
 */
public class PathCustomEditor extends CustomEditorAbstract {

  // lookup sur le service OTFA
  final PerspectiveServiceOtfa perspectiveServiceOtfa = Lookup.getDefault().lookup(PerspectiveServiceOtfa.class);
  // composant d'édition de l'éditeur de propriété
  JTextField jtf;

  public PathCustomEditor(PathPropertyEditorSupport editor, PropertyEnv env) {
    super(editor, env);
  }

  @Override
  protected JComponent createComponent() {
    PathPropertyEditorSupport scenarioEditor = (PathPropertyEditorSupport) editor;

    // vérification si on traite la référence ou la cible
    boolean isReference = scenarioEditor.getCampagneligne().getReference() == scenarioEditor.getCampagneItem();
    String cheminCSV;
    if (isReference) {
      cheminCSV = scenarioEditor.getCampagneligne().getReference().getCheminCSV();
    } else {
      cheminCSV = scenarioEditor.getCampagneligne().getCible().getCheminCSV();
    }

    jtf = new JTextField(cheminCSV);
    jtf.setEditable(perspectiveServiceOtfa.isInEditMode());
    return jtf;
  }

  @Override
  protected Object getPropertyValue() {
    if (jtf == null || jtf.getText().isEmpty()) {
      return StringUtils.EMPTY;
    }
    return jtf.getText();
  }
}
