package org.fudaa.fudaa.crue.otfa.property;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyEditorSupport;
import javax.swing.JDialog;
import javax.swing.JLabel;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLine;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLinesLoader;
import org.fudaa.fudaa.crue.common.UserPreferencesSaver;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.CtuluLogsTopComponent;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.otfa.service.OtfaService;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Chris
 */
public class LogsPropertyEditor extends PropertyEditorSupport {

  public LogsPropertyEditor() {
  }

  @Override
  public boolean isPaintable() {
    return true;
  }
  //cf http://blogs.oracle.com/geertjan/entry/propertyeditorsupport_paintvalue

  @Override
  public void paintValue(Graphics gfx, Rectangle box) {

    RTFAResultLine indicator = (RTFAResultLine) getValue();
    Color background = null;
    if (indicator != null) {
      if (indicator.containsErrorOrSevereError()) {
        background = Color.RED;
      } else if (indicator.containsWarning()) {
        background = Color.ORANGE;
      }
    }
    if (background != null) {
      Color old = gfx.getColor();
      gfx.setColor(background);
      ((Graphics2D) gfx).fill(box);
      gfx.setColor(old);
    }
    if (indicator != null) {
      gfx.drawString(getAsText(), box.x + 5, box.y + 15);
    }

  }

  @Override
  public String getAsText() {
    RTFAResultLine logs = (RTFAResultLine) getValue();
    if (!logs.hasLog()) {
      return NbBundle.getMessage(LogsPropertyEditor.class, "noLogs");
    }
    int nbLogs = logs.getNbLogs();
    if (nbLogs == 0) {
      return NbBundle.getMessage(LogsPropertyEditor.class, "noLogs");
    }
    return NbBundle.getMessage(LogsPropertyEditor.class, "nbLogs", nbLogs);
  }

  private static class LoadResultats implements ProgressRunnable<CrueIOResu<CtuluLogGroup>> {

    private final RTFAResultLine line;

    public LoadResultats(RTFAResultLine line) {
      this.line = line;
    }

    @Override
    public CrueIOResu<CtuluLogGroup> run(ProgressHandle handle) {
      handle.switchToIndeterminate();
      OtfaService otfaService = Lookup.getDefault().lookup(OtfaService.class);
      RTFAResultLinesLoader loader = new RTFAResultLinesLoader(otfaService.getCurrentOTFA().getOtfaCampagne().getOtfaFile());
      return loader.loadLogs(line.getInitialLine());
    }
  }

  @Override
  public Component getCustomEditor() {
    RTFAResultLine result = (RTFAResultLine) getValue();
    final String title = NbBundle.getMessage(LogsPropertyEditor.class, "logDisplayerTitle");
    CrueIOResu<CtuluLogGroup> res = CrueProgressUtils.showProgressDialogAndRun(new LoadResultats(result), title);
    if (res != null && res.getAnalyse() != null && res.getAnalyse().containsErrorOrSevereError()) {
      LogsDisplayer.displayError(res.getAnalyse(), title);
    }
    CtuluLogGroup logs = res == null ? null : res.getMetier();
    if ((logs != null) && (getNbLog(logs) != 0)) {
      final CtuluLogsTopComponent table = new CtuluLogsTopComponent();
      //pour avoir un identifiant de persistance unique
      table.setPreferencePrefix(getClass().getName());
      table.setLogGroup(logs);
      //le dernier paramètre permet d'avoir un id uniquement pour la persistance
      final JDialog dialog = LogsDisplayer.createDialog(table, title, getClass().getName(), DialogHelper.NO_VERSION_FOR_PERSISTENCE);
      //pour persister les propriétés du dialogue et de la table à la fermeture.
      dialog.addWindowListener(new WindowAdapter() {
        @Override
        public void windowClosed(WindowEvent e) {
          table.writePreferences(table.getClass(), DialogHelper.NO_VERSION_FOR_PERSISTENCE);
          UserPreferencesSaver.saveLocationAndDimension(dialog, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
        }

      });

      return dialog;
    }
    return new JLabel(NbBundle.getMessage(LogsPropertyEditor.class, "noLogs"));
  }

  private int getNbLog(CtuluLogGroup logs) {
    int nbLogs = 0;
    for (CtuluLogLevel level : CtuluLogLevel.values()) {
      nbLogs += logs.getNbOccurence(level);
    }
    return nbLogs;
  }

  @Override
  public boolean supportsCustomEditor() {
    return true;
  }
}
