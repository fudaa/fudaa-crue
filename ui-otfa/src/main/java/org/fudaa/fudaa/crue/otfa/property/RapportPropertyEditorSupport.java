package org.fudaa.fudaa.crue.otfa.property;

import java.awt.Component;
import java.beans.PropertyEditorSupport;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneItem;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.otfa.perspective.PerspectiveServiceOtfa;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class RapportPropertyEditorSupport extends PropertyEditorSupport implements ExPropertyEditor {

  private final OtfaCampagne campagne;
  private final OtfaCampagneItem campagneItem;
  private final OtfaCampagneLine campagneligne;
  private PropertyEnv env;

  final PerspectiveServiceOtfa perspectiveServiceOtfa = Lookup.getDefault().lookup(PerspectiveServiceOtfa.class);

  public RapportPropertyEditorSupport(OtfaCampagne campagne, OtfaCampagneLine ligne, OtfaCampagneItem item) {
    this.campagne = campagne;
    this.campagneligne = ligne;
    this.campagneItem = item;
  }

  public OtfaCampagneLine getCampagneligne() {
    return campagneligne;
  }

  public OtfaCampagne getCampagne() {
    return campagne;
  }

  public OtfaCampagneItem getCampagneItem() {
    return campagneItem;
  }

  @Override
  public void attachEnv(PropertyEnv env) {
    this.env = env;
  }

  @Override
  public Component getCustomEditor() {
    if (perspectiveServiceOtfa.isInEditMode()) {
      return DialogHelper.createOKCancelDialog(new RapportCustomEditor(this, env).getComponent(), this.getClass().getName(), NbBundle.getMessage(
              RapportPropertyEditorSupport.class, "rapportsDisplayerTitle"), false, env, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    } else {
      // encapsulation dans un JDialog pour pouvoir gérer la persistence
      return DialogHelper.createCloseDialog(new RapportCustomEditor(this, env).getComponent(), this.getClass().getName(), NbBundle.getMessage(
              RapportPropertyEditorSupport.class, "rapportsDisplayerTitle"), false, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    }
  }

  @Override
  public boolean supportsCustomEditor() {
    return true;
  }

  @Override
  public String getAsText() {
    return (String) getValue();
  }

  @Override
  public void setAsText(String string) {
    setValue(string);
  }

}
