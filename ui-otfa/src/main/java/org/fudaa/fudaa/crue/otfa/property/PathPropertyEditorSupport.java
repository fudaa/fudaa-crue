package org.fudaa.fudaa.crue.otfa.property;

import java.awt.Component;
import java.beans.PropertyEditorSupport;
import javax.swing.JDialog;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneItem;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.otfa.perspective.PerspectiveServiceOtfa;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Yoan GRESSIER
 */
public class PathPropertyEditorSupport extends PropertyEditorSupport implements ExPropertyEditor {

  // l'élément couremment éditer, sous 3 formes
  private final OtfaCampagne campagne;
  private final OtfaCampagneItem campagneItem;
  private final OtfaCampagneLine campagneligne;

  // l'environnement de mise à jour de la propriété
  private PropertyEnv env;
  // lookup sur le service OTFA
  final PerspectiveServiceOtfa perspectiveServiceOtfa = Lookup.getDefault().lookup(PerspectiveServiceOtfa.class);

  public PathPropertyEditorSupport(OtfaCampagne campagne, OtfaCampagneLine ligne, OtfaCampagneItem item) {
    this.campagne = campagne;
    this.campagneligne = ligne;
    this.campagneItem = item;
  }

  public OtfaCampagneLine getCampagneligne() {
    return campagneligne;
  }

  public OtfaCampagne getCampagne() {
    return campagne;
  }

  public OtfaCampagneItem getCampagneItem() {
    return campagneItem;
  }

  @Override
  public void attachEnv(PropertyEnv env) {
    this.env = env;
  }

  @Override
  public Component getCustomEditor() {
    JDialog jd;
    // encapsulation dans un JDialog pour gérer la persistence
    if (perspectiveServiceOtfa.isInEditMode()) {
      jd = DialogHelper.createOKCancelDialog(new PathCustomEditor(this, env).getComponent(), this.getClass().getName(), NbBundle.getMessage(
              PathPropertyEditorSupport.class, "pathDisplayerTitle"), true, env, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    } else {
      jd = DialogHelper.createCloseDialog(new PathCustomEditor(this, env).getComponent(), this.getClass().getName(), NbBundle.getMessage(
              PathPropertyEditorSupport.class, "pathDisplayerTitle"), true, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    }
    return jd;
  }

  @Override
  public boolean supportsCustomEditor() {
    return true;
  }

  @Override
  public String getAsText() {
    return (String) getValue();
  }

  @Override
  public void setAsText(String string) {
    setValue(string);
  }

}
