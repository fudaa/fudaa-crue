package org.fudaa.fudaa.crue.otfa.service;

/**
 *
 * Permet simplement d'indiquer si une campagne est ouverte ou pas.
 * Utilsé dans le lookup du le service {@link  org.fudaa.fudaa.crue.otfa.service.OtfaService}
 *
 * @author deniger
 */
public enum OtfaState {

  OPENED

}
