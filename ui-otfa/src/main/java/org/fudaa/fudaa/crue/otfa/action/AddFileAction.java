/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.action;

import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.otfa.filter.OtfaFileFilter;
import org.fudaa.fudaa.crue.otfa.service.OtfaService;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;

/**
 * Action de complétion d'une campagne OTFA avec un ou plusieurs fichier(s) de campagne
 *
 * @author Yoan GRESSIER
 */
public class AddFileAction extends AbstractAction {
  final transient OtfaService otfaService = Lookup.getDefault().lookup(OtfaService.class);
  private transient final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

  public AddFileAction() {
    putValue(Action.NAME, NbBundle.getMessage(AddFileAction.class, "AddFileAction.ActionName"));
    putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(AddFileAction.class, "AddFileAction.ShortDescription"));
  }

  @Override
  public void actionPerformed(final ActionEvent e) {
    final File home = configurationManagerService.getDefaultDataHome();
    final String name = (String) getValue(NAME);
    final FileChooserBuilder builder = new FileChooserBuilder(getClass()).setTitle(name).setDefaultWorkingDirectory(home).setApproveText(name);
    builder.setFileFilter(OtfaFileFilter.create());

    // lecture d'un ou plusieurs fichiers
    final File[] filesToOpen = builder.showMultiOpenDialog();
    // pour chaque fichier, complétion de la campagne courante
    if (filesToOpen != null && filesToOpen.length > 0) {
      otfaService.complete(filesToOpen);
    }
  }

}
