package org.fudaa.fudaa.crue.otfa.property;

import java.awt.Color;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.ReportIndexReaderHelper;
import org.fudaa.dodico.crue.projet.report.ReportViewLineInfo;
import org.fudaa.dodico.crue.projet.report.ReportViewLineInfoAndType;
import org.fudaa.fudaa.crue.common.editor.CustomEditorAbstract;
import org.fudaa.fudaa.crue.loader.LoaderService;
import org.fudaa.fudaa.crue.loader.ProjectLoadContainer;
import org.jdesktop.swingx.JXTree;
import org.jdesktop.swingx.tree.DefaultXTreeCellRenderer;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.Lookup;

/**
 *
 * @author deniger
 */
public class RapportCustomEditor extends CustomEditorAbstract {

  final LoaderService loaderService = Lookup.getDefault().lookup(LoaderService.class);

  public RapportCustomEditor(RapportPropertyEditorSupport editor, PropertyEnv env) {
    super(editor, env);
  }
  JXTree tree;

  @Override
  protected JComponent createComponent() {
    RapportPropertyEditorSupport scenarioEditor = (RapportPropertyEditorSupport) editor;
    //test == voulu car test sur référence
    boolean isCible = scenarioEditor.getCampagneligne().getCible() == scenarioEditor.getCampagneItem();
    File etuFile = null;
    //on edite la cible mais le fichier etude n'est pas renseigné pour la cible.
    //cela veut dire que l'utilisateur veut utiliser le fichier etu de la référence
    if (isCible && StringUtils.isEmpty(scenarioEditor.getCampagneItem().getEtuPath())) {
      //donc on utilise la référence
      etuFile = scenarioEditor.getCampagneligne().getReference().getEtuFile(scenarioEditor.getCampagne().getOtfaDir());
    } else {
      etuFile = scenarioEditor.getCampagneItem().getEtuFile(scenarioEditor.getCampagne().getOtfaDir());
    }
    if (etuFile == null || !etuFile.isFile()) {
      JLabel lb = new JLabel();
      lb.setForeground(Color.RED);
      lb.setText(org.openide.util.NbBundle.getMessage(RapportCustomEditor.class, "OtfaProperty.EtuFileNotFound", etuFile == null
              ? "?" : etuFile.getAbsolutePath()));
      return lb;
    }
    ProjectLoadContainer loadProject = loaderService.loadProject(etuFile);
    if (loadProject.getProjet() != null) {
      final EMHProjet projet = loadProject.getProjet();
      final ReportContentType[] values = ReportContentType.values();

      DefaultMutableTreeNode root = new DefaultMutableTreeNode();
      ReportIndexReaderHelper helper = new ReportIndexReaderHelper(projet);
      String value = (String) editor.getValue();
      TreeNode selectedValue = null;
      List<ReportContentType> toAvoid = Arrays.asList(ReportContentType.PLANIMETRY, ReportContentType.TRANSVERSAL);
      for (ReportContentType reportContentType : values) {
        //on ignore les rapports non gérés.
        if (toAvoid.contains(reportContentType)) {
          continue;
        }
        final CrueIOResu<List<ReportViewLineInfo>> linesIO = helper.readType(reportContentType);
        if (linesIO != null && CtuluLibArray.isNotEmpty(linesIO.getMetier())) {
          DefaultMutableTreeNode lineParent = new DefaultMutableTreeNode(reportContentType.getFolderName());
          root.add(lineParent);
          final List<ReportViewLineInfo> lines = linesIO.getMetier();
          for (ReportViewLineInfo reportViewLineInfo : lines) {
            ReportViewLineInfoAndType dao = new ReportViewLineInfoAndType(reportContentType, reportViewLineInfo);
            final DefaultMutableTreeNode defaultMutableTreeNode = new DefaultMutableTreeNode(dao);
            if (dao.isSameId(value)) {
              selectedValue = defaultMutableTreeNode;
            }
            lineParent.add(defaultMutableTreeNode);
          }
        }
      }
      DefaultTreeModel model = new DefaultTreeModel(root);
      tree = new JXTree(model) {

        @Override
        public String convertValueToText(Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
          DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
          if (node.getUserObject() instanceof ReportViewLineInfoAndType) {
            return ((ReportViewLineInfoAndType) node.getUserObject()).getDescription();
          }
          return super.convertValueToText(value, selected, expanded, leaf, row, hasFocus);
        }
      };
      tree.setCellRenderer(new DefaultXTreeCellRenderer());
      tree.expandAll();
      tree.setEditable(false);
      tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
      if (selectedValue != null) {
        tree.setSelectionPath(new TreePath(model.getPathToRoot(selectedValue)));
      }
    } else {
      JLabel lb = new JLabel();
      lb.setForeground(Color.RED);
      lb.setText(org.openide.util.NbBundle.getMessage(RapportCustomEditor.class, "OtfaProperty.EtuFileNotFound", etuFile.getAbsolutePath()));
      return lb;
    }
    return new JScrollPane(tree);

  }

  @Override
  protected Object getPropertyValue() {
    if (tree == null || tree.getSelectionPath() == null) {
      return StringUtils.EMPTY;
    }
    final DefaultMutableTreeNode lastPathComponent = (DefaultMutableTreeNode) tree.getSelectionPath().getLastPathComponent();
    if (lastPathComponent != null && lastPathComponent.getUserObject() instanceof ReportViewLineInfoAndType) {
      return ((ReportViewLineInfoAndType) lastPathComponent.getUserObject()).getDescription();
    }
    return StringUtils.EMPTY;
  }
}
