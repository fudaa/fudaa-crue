package org.fudaa.fudaa.crue.otfa.action;

import com.memoire.bu.BuVerticalLayout;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.clean.EMHProjectCleaner;
import org.fudaa.dodico.crue.projet.clean.ScenarioCleanData;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;
import org.fudaa.dodico.crue.projet.otfa.OtfaEtuFileCoeurExtractor;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.loader.LoaderService;
import org.fudaa.fudaa.crue.loader.ProjectLoadContainer;
import org.fudaa.fudaa.crue.loader.action.CleanProjectUiSelector;
import org.fudaa.fudaa.crue.loader.action.ProjectFindScenarioToCleanProgressRunnable;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Parcourt toutes les etudes de la campagne et propose de nettoyer les études.
 */
public class CleanAllEtuAction extends AbstractOtfaAction {
  private final LoaderService loaderService = Lookup.getDefault().lookup(LoaderService.class);

  public CleanAllEtuAction() {
    super("CleanAllEtuAction.ActionName");
    putValue(Action.NAME, NbBundle.getMessage(CleanAllEtuAction.class, "CleanAllEtuAction.ActionName"));
    putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(CleanAllEtuAction.class, "CleanAllEtuAction.ShortDescription"));
  }

  @Override
  protected void updateEnabledState() {
    setEnabled(perspectiveServiceOtfa != null && perspectiveServiceOtfa.isInEditMode() && otfaService.getCurrentOTFA() != null);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    final OtfaCampagne otfaCampagne = otfaService.getCurrentOTFA().getOtfaCampagne();
    final List<String> etuFiles = new OtfaEtuFileCoeurExtractor().extract(otfaCampagne);
    List<File> etuFileToModified = new ArrayList<>();
    etuFiles.forEach(line -> cleanEtuFile(line, etuFileToModified));
    //rien a faire -> message simple pour avertir l'utilisateur.
    if (etuFileToModified.isEmpty()) {
      DialogHelper.showNotifyOperationTermine((String) getValue(Action.NAME), NbBundle.getMessage(CleanAllEtuAction.class, "CleanAllEtuAction.NoAction"));
    }
  }

  private void cleanEtuFile(String etuPath, List<File> etuFileToModified) {
    File otfaDir = otfaService.getCurrentOTFA().getOtfaCampagne().getOtfaDir();
    File etuFile = CrueFileHelper.getCanonicalBaseDir(otfaDir, etuPath);

    if (etuFile.exists()) {
      final ProjectLoadContainer loadContainer = loaderService.loadProject(etuFile);
      final EMHProjet projet = loadContainer.getProjet();
      if (projet != null) {
        //on recherche les scenarios a supprimer/simplifier
        final String actionTitle = (String) getValue(Action.NAME);
        ScenarioCleanData toDeleteToNormalize = CrueProgressUtils.showProgressDialogAndRun(new ProjectFindScenarioToCleanProgressRunnable(projet), etuFile.getAbsolutePath());
        //si pas vide, on demande confirmation à l'utilisateur
        if (toDeleteToNormalize.isNotEmpty()) {
          //on enregistre le fait que cette etude doit être modifiée
          etuFileToModified.add(etuFile);
          final ScenarioCleanData toDeleteToNormalizeChoosenByUser = uiSelectData(etuFile, actionTitle, toDeleteToNormalize);
          //si l'utilisateur a choisi qqchose
          if (toDeleteToNormalizeChoosenByUser != null && toDeleteToNormalizeChoosenByUser.isNotEmpty()) {
            //on effectue l'action:
            final CtuluLogGroup logGroup = CrueProgressUtils
                .showProgressDialogAndRun(progressHandle -> {
                  progressHandle.switchToIndeterminate();
                  return new EMHProjectCleaner(projet, etuFile).cleanProject(toDeleteToNormalizeChoosenByUser, true);
                }, etuFile.getAbsolutePath());
            if (logGroup.containsError()) {
              LogsDisplayer.displayError(logGroup, etuFile.getAbsolutePath());
            }
          }
        }
      }
    }
  }

  /**
   * @param etuFile le fichier etu ( pour label).
   * @param actionTitle le titre de l'action
   * @param toDeleteToNormalize les donnees initiales
   * @return les données choisies par l'utilisateur pour nettoyer le fichier etu en question
   */
  private ScenarioCleanData uiSelectData(File etuFile, String actionTitle, ScenarioCleanData toDeleteToNormalize) {
    final CleanProjectUiSelector selector = new CleanProjectUiSelector();
    JPanel etuPanel = new JPanel(new BuVerticalLayout(2));
    etuPanel.setBorder(new EmptyBorder(5, 0, 5, 0));
    etuPanel.add(new JLabel(NbBundle.getMessage(CleanAllEtuAction.class, "CleanAllEtuAction.EtuLabel") + ":"));
    final JLabel lbEtuFile = new JLabel(etuFile.getAbsolutePath());
    lbEtuFile.setToolTipText(etuFile.getAbsolutePath());
    etuPanel.add(lbEtuFile);
    selector.setTopTitle(etuPanel);
    return selector.select(toDeleteToNormalize, actionTitle, null);
  }
}
