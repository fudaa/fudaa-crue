/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.action;

import org.fudaa.fudaa.crue.otfa.node.OtfaCampagneNode;
import org.fudaa.fudaa.crue.otfa.perspective.PerspectiveServiceOtfa;
import org.fudaa.fudaa.crue.otfa.service.OtfaService;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;

import javax.swing.*;

/**
 *
 * @author deniger
 */
public abstract class AbstractOtfaAction extends AbstractAction implements LookupListener {

  protected final OtfaService otfaService = Lookup.getDefault().lookup(OtfaService.class);
  protected final PerspectiveServiceOtfa perspectiveServiceOtfa = Lookup.getDefault().lookup(PerspectiveServiceOtfa.class);
  private final Result<OtfaCampagneNode> lookupResult;

  public AbstractOtfaAction(final String name) {
    putValue(Action.NAME, NbBundle.getMessage(AbstractOtfaAction.class, name));
    lookupResult = otfaService.getLookup().lookupResult(OtfaCampagneNode.class);
    lookupResult.addLookupListener(this);
    perspectiveServiceOtfa.addStateListener(evt -> updateEnabledState());
    updateEnabledState();
  }

  @Override
  public void resultChanged(final LookupEvent ev) {
    updateEnabledState();
  }

  protected void updateEnabledState() {
    setEnabled(otfaService.getCurrentOTFA() != null && perspectiveServiceOtfa.isInEditMode());
  }
}
