package org.fudaa.fudaa.crue.otfa.property;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.beans.PropertyEditorSupport;
import javax.swing.JLabel;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLine;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLinesLoader;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLineResultComparaisons;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.comparison.ScenarioComparaisonController;
import org.fudaa.fudaa.crue.otfa.service.OtfaService;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Chris
 */
public class ComparaisonsPropertyEditor extends PropertyEditorSupport {

  public ComparaisonsPropertyEditor() {
  }

  @Override
  public String getAsText() {
    RTFAResultLine result = (RTFAResultLine) getValue();
    if (result == null) {
      return StringUtils.EMPTY;
    }
    return Integer.toString(result.getNbDifferences());
  }

  @Override
  public boolean isPaintable() {
    return true;
  }
  //cf http://blogs.oracle.com/geertjan/entry/propertyeditorsupport_paintvalue

  @Override
  public void paintValue(Graphics gfx, Rectangle box) {

    RTFAResultLine result = (RTFAResultLine) getValue();
    Color background = Color.WHITE;
    if (result != null && result.hasComparaison()) {
      background = result.getNbDifferences() > 0 ? Color.RED : Color.GREEN;
    }
    if (background != null) {
      Color old = gfx.getColor();
      gfx.setColor(background);
      ((Graphics2D) gfx).fill(box);
      gfx.setColor(old);
    }
    if (result != null && result.hasComparaison()) {
      gfx.drawString(getAsText(), box.x + 5, box.y + 15);
    }
  }

  private static class LoadResultats implements ProgressRunnable<CrueIOResu<OtfaCampagneLineResultComparaisons>> {

    private final RTFAResultLine line;

    public LoadResultats(RTFAResultLine line) {
      this.line = line;
    }

    @Override
    public CrueIOResu<OtfaCampagneLineResultComparaisons> run(ProgressHandle handle) {
      handle.switchToIndeterminate();
      OtfaService otfaService = Lookup.getDefault().lookup(OtfaService.class);
      RTFAResultLinesLoader loader = new RTFAResultLinesLoader(otfaService.getCurrentOTFA().getOtfaCampagne().getOtfaFile());
      return loader.loadResults(line.getInitialLine());
    }
  }

  @Override
  public Component getCustomEditor() {
    RTFAResultLine result = (RTFAResultLine) getValue();
    final String title = NbBundle.getMessage(LogsPropertyEditor.class, "comparisonsDisplayerTitle");
    CrueIOResu<OtfaCampagneLineResultComparaisons> res = CrueProgressUtils.showProgressDialogAndRun(new LoadResultats(result), title);
    if (res != null && res.getAnalyse() != null && res.getAnalyse().containsErrorOrSevereError()) {
      LogsDisplayer.displayError(res.getAnalyse(), title);
    }
    if (res != null && res.getMetier() != null && res.getMetier().getComparisonResult() != null) {
      final ScenarioComparaisonController s = new ScenarioComparaisonController("référence", "cible");
      // encapsulation dans un JDialog pour pouvoir gérer la persistence
      return DialogHelper.createCloseDialog(s.createView(res.getMetier().getComparisonResult()), s.getUi().getTable(), this.getClass().getName(), title,
              false, StringUtils.EMPTY);
    }

    // si aucune différence trouvée, un simple label est affiché
    return DialogHelper.createCloseDialog(new JLabel(NbBundle.getMessage(ComparaisonsPropertyEditor.class, "noComparisons")), this.getClass().
            getName(), title, true, StringUtils.EMPTY);
  }

  @Override
  public boolean supportsCustomEditor() {
    return true;
  }
}
