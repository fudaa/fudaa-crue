/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.action;

import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.otfa.filter.OtfaFileFilter;
import org.fudaa.fudaa.crue.otfa.service.OtfaService;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Chris
 */
public class OpenFileAction extends AbstractAction {
  
  final OtfaService otfaService = Lookup.getDefault().lookup(OtfaService.class);
  final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);
  
  public OpenFileAction() {
    putValue(Action.NAME, NbBundle.getMessage(OpenFileAction.class, "OpenFileAction.ActionName"));
    putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(OpenFileAction.class, "OpenFileAction.ShortDescription"));
  }
  
  @Override
  public void actionPerformed(ActionEvent e) {
    File home = configurationManagerService.getDefaultDataHome();
    final String name = (String) getValue(NAME);
    final FileChooserBuilder builder = new FileChooserBuilder(getClass()).setTitle(name).setDefaultWorkingDirectory(home).setApproveText(name);
    builder.setFileFilter(OtfaFileFilter.create());
    File toOpen = builder.showOpenDialog();
    if (toOpen != null) {
      if (toOpen.canWrite()) {
        otfaService.open(toOpen);
      } else {
        // fichier en lecture seule : notification à l'utilisateur
        DialogHelper.showError(NbBundle.getMessage(OpenFileAction.class, "OpenFileAction.ErreurReadOnlyFile"));
      }
    }
  }
}
