package org.fudaa.fudaa.crue.otfa.property;

import java.beans.PropertyEditor;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.otfa.perspective.PerspectiveServiceOtfa;
import org.openide.util.Lookup;

/**
 *
 * @author Yoan GRESSIER
 */
public class CommentaireProperty extends PropertySupportReflection<String> {

  // campagne de travail
  private final OtfaCampagne campagne;
  
  // lookup sur le service OTFA
  final PerspectiveServiceOtfa perspectiveServiceOtfa = Lookup.getDefault().lookup(PerspectiveServiceOtfa.class);

  public CommentaireProperty(AbstractNodeFirable node, OtfaCampagne campagne, OtfaCampagneLine instance, String getter, String setter) throws NoSuchMethodException {
    super(node, instance, String.class, getter, setter);
    this.campagne = campagne;
  }

  @Override
  public boolean canWrite() {
    return super.canWrite() && perspectiveServiceOtfa.isInEditMode();
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    return new CommentairePropertyEditorSupport(campagne, (OtfaCampagneLine) instance);
  }
}
