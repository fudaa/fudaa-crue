/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.node;

import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLine;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;
import org.fudaa.fudaa.crue.otfa.process.OtfaCampagneContainer;
import org.fudaa.fudaa.crue.otfa.service.OtfaService;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 *
 * @author Christophe CANEL
 */
public class OtfaCampagneLineChildFactory extends ChildFactory<RTFAResultLine> {

  final OtfaService otfaService = Lookup.getDefault().lookup(OtfaService.class);

  public OtfaCampagneLineChildFactory() {
  }

  /**
   * Rafraichit la structure et envoie un evt.
   */
  public void refresh() {
    super.refresh(true);
    otfaService.linesStructureChanged();
  }

  @Override
  protected boolean createKeys(List<RTFAResultLine> toPopulate) {
    OtfaCampagneContainer currentOTFA = otfaService.getCurrentOTFA();
    if (currentOTFA != null) {
      List<OtfaCampagneLine> lines = currentOTFA.getOtfaCampagne().getLines();
      Collections.sort(lines, new OtfaCampagneLineComparator());
      for (OtfaCampagneLine otfaCampagneLine : lines) {
        RTFAResultLine resultFor = currentOTFA.getResultFor(otfaCampagneLine);
        if (resultFor != null) {
          toPopulate.add(resultFor);
        } else {
          toPopulate.add(new RTFAResultLine(otfaCampagneLine));
        }
      }
      return true;
    }
    return false;
  }

  @Override
  protected Node createNodeForKey(RTFAResultLine key) {
    return new OtfaCampagneLineNode(key);
  }
}
