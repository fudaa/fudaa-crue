/*
 GPL 2
 */
package org.fudaa.fudaa.crue.otfa.node;

import java.lang.reflect.InvocationTargetException;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLine;
import org.fudaa.fudaa.crue.common.property.PropertySupportRead;

/**
 *
 * @author Frederic Deniger
 */
public class OtfaRtfaLineProperty extends PropertySupportRead<RTFAResultLine, RTFAResultLine> {

  public OtfaRtfaLineProperty(RTFAResultLine instance, String name, String displayName, String description) {
    super(instance, RTFAResultLine.class, name, displayName, description);
  }

  @Override
  public RTFAResultLine getValue() throws IllegalAccessException, InvocationTargetException {
    return getInstance();
  }
}
