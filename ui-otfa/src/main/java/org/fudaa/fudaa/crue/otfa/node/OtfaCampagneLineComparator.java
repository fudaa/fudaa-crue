package org.fudaa.fudaa.crue.otfa.node;

import java.util.Comparator;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;

/**
 *
 * @author deniger
 */
class OtfaCampagneLineComparator implements Comparator<OtfaCampagneLine> {

  @Override
  public int compare(OtfaCampagneLine o1, OtfaCampagneLine o2) {
    return ((Integer) o1.getIndice()).compareTo(o2.getIndice());
  }
  
}
