package org.fudaa.fudaa.crue.otfa.property;

import java.awt.Color;
import java.awt.Component;
import java.io.File;
import java.util.Collections;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.editor.CustomEditorAbstract;
import org.fudaa.fudaa.crue.loader.LoaderService;
import org.fudaa.fudaa.crue.loader.ProjectLoadContainer;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.Lookup;

/**
 *
 * @author deniger
 */
public class ScenarioCustomEditor extends CustomEditorAbstract {

  final LoaderService loaderService = Lookup.getDefault().lookup(LoaderService.class);

  public ScenarioCustomEditor(ScenarioPropertyEditorSupport editor, PropertyEnv env) {
    super(editor, env);
  }
  JList list;

  @Override
  protected Component createComponent() {
    ScenarioPropertyEditorSupport scenarioEditor = (ScenarioPropertyEditorSupport) editor;
    File etuFile = scenarioEditor.getCampagneItem().getEtuFile(scenarioEditor.getCampagne().getOtfaDir());
    if (etuFile == null || !etuFile.isFile()) {
      JLabel lb = new JLabel();
      lb.setForeground(Color.RED);
      lb.setText(org.openide.util.NbBundle.getMessage(ScenarioCustomEditor.class, "OtfaProperty.EtuFileNotFound", etuFile == null
              ? "?" : etuFile.getAbsolutePath()));
      return lb;
    }
    ProjectLoadContainer loadProject = loaderService.loadProject(etuFile);
    if (loadProject.getProjet() != null) {
      List<ManagerEMHScenario> listeScenarios = loadProject.getProjet().getListeScenarios();
      List<String> toNom = TransformerHelper.toNom(listeScenarios);
      Collections.sort(toNom);
      Object value = editor.getValue();
      list = new JList(toNom.toArray(new String[0]));
      list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      if (value != null) {
        list.setSelectedValue(value, true);
      }
    } else {
      JLabel lb = new JLabel();
      lb.setForeground(Color.RED);
      lb.setText(org.openide.util.NbBundle.getMessage(ScenarioCustomEditor.class, "OtfaProperty.EtuFileNotFound", etuFile.getAbsolutePath()));
      return lb;
    }

    return new JScrollPane(list);
  }

  @Override
  protected Object getPropertyValue() {
    if (list == null) {
      return StringUtils.EMPTY;
    }
    return list.getSelectedValue();
  }
}
