package org.fudaa.fudaa.crue.otfa.property;

import java.beans.PropertyEditor;
import java.beans.PropertyEditorSupport;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneItem;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.openide.util.Lookup;

/**
 *
 * @author Christophe CANEL
 */
public class CoeurProperty extends PropertySupportReflection<String> {

  private final boolean emptyAuthorized;

  public CoeurProperty(AbstractNodeFirable node, Object instance, Class clazz, String getter, String setter, boolean emptyAuthorized) throws NoSuchMethodException {
    super(node, instance, clazz, getter, setter);
    this.emptyAuthorized = emptyAuthorized;
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    return new CoeurPropertyEditor();
  }

  private class CoeurPropertyEditor extends PropertyEditorSupport {

    protected final ConfigurationManagerService service = Lookup.getDefault().lookup(ConfigurationManagerService.class);

    public CoeurPropertyEditor() {
    }

    @Override
    public String getAsText() {
      return (String) getValue();
    }

    @Override
    public void setAsText(String string) {
      setValue(string);
    }
    String[] coeurs;

    @Override
    public String[] getTags() {
      if (coeurs == null) {
        List<String> all = new ArrayList<>();
        if (emptyAuthorized) {
          all.add(StringUtils.EMPTY);
        }
        all.add(OtfaCampagneItem.CRUE9);
        all.addAll(service.getCoeursNames());
        coeurs = all.toArray(new String[0]);
      }
      return coeurs;
    }
  }
}
