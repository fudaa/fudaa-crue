package org.fudaa.fudaa.crue.otfa.perspective;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.UserSaveAnswer;
import org.fudaa.fudaa.crue.common.services.AbstractPerspectiveService;
import org.fudaa.fudaa.crue.common.services.PerspectiveService;
import org.fudaa.fudaa.crue.common.services.PerspectiveState;
import org.fudaa.fudaa.crue.otfa.OtfaCampagneTopComponent;
import org.fudaa.fudaa.crue.otfa.OtfaPropertiesTopComponent;
import org.fudaa.fudaa.crue.otfa.service.OtfaService;
import org.fudaa.fudaa.crue.otfa.service.OtfaState;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Le service perspective de OTFA.
 *
 * @author Fred Deniger
 */
@ServiceProviders(value = {
        @ServiceProvider(service = PerspectiveServiceOtfa.class)
        ,
        @ServiceProvider(service = PerspectiveService.class)})
public final class PerspectiveServiceOtfa extends AbstractPerspectiveService implements LookupListener {
    private final Set<String> components = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(
            OtfaCampagneTopComponent.TOPCOMPONENT_ID, OtfaPropertiesTopComponent.TOPCOMPONENT_ID)));
    private final Result<OtfaState> lookupResult;
    private final Result<Boolean> lookupDirtyResult;
    final OtfaService otfaService = Lookup.getDefault().lookup(OtfaService.class);

    /**
     * Ajoute des listener sur le service {@link OtfaService} pour réagir au (dé)chargement d'un campagne et aux modifications de la campagne.
     */
    public PerspectiveServiceOtfa() {
        super(PerspectiveEnum.TEST);
        //ajoutes des listeners
        //campagne ouverte ?
        lookupResult = otfaService.getLookup().lookupResult(OtfaState.class);
        lookupResult.addLookupListener(this);
        //campagne modifiée?
        lookupDirtyResult = otfaService.getLookup().lookupResult(Boolean.class);
        lookupDirtyResult.addLookupListener(ev -> setDirty(otfaService.getCurrentOTFA() != null && otfaService.isModified()));
        setState(PerspectiveState.MODE_READ_ONLY_ALWAYS);
    }

    @Override
    public String getPathForViewsAction() {
        return "Menu/Window/OTFA";
    }

    /**
     * appele si un fichier OTFA est totalement chargé ou décharge: méthode appelée par le listener sur {@link OtfaService}.
     * Met à jour les états edit/read modifié/non modifié de la perspective
     *
     * @param ev event
     */
    @Override
    public void resultChanged(LookupEvent ev) {
        if (otfaService.getCurrentOTFA() != null) {
            setState(PerspectiveState.MODE_READ);
            setDirty(otfaService.isModified());
        } else {
            setState(PerspectiveState.MODE_READ_ONLY_TEMP);
            setDirty(false);
        }
    }

    /**
     * @return true
     */
    @Override
    public boolean supportEdition() {
        return true;
    }

    /**
     * @return true si pas de modification sur la campagne ouverte. Sinon demande confirmation au service {@link OtfaService#closeCurrent() }.
     * @see OtfaService#closeCurrent()
     */
    @Override
    public boolean closing() {
        return !isDirty() || otfaService.closeCurrent();
    }

    /**
     * @param newState nouvel état de la perspective
     * @return true dans presque tous les cas. Si l'utilisateur veut repasser en mode read et si la campagne est modifiée, demande confirmation à l'utilisateur.
     */
    @Override
    protected boolean canStateBeModifiedTo(PerspectiveState newState) {
        //pas de campagne: on doit rester en mode read only temp
        if (otfaService.getCurrentOTFA() == null) {
            return PerspectiveState.MODE_READ_ONLY_TEMP.equals(newState);
        }
        //pas de probleme pour le mode edit
        if (PerspectiveState.MODE_EDIT.equals(newState)) {
            return true;
        }
        //pour passer en mode read, on doit vérifier si la campagne en cours est modifiée.
        if (PerspectiveState.MODE_READ.equals(newState)) {
            //Si oui, on demande confirmation à l'utilisateur pour sauvegarde et fermeture
            if (otfaService.isModified()) {
                UserSaveAnswer saveAnswer = DialogHelper.confirmSaveOrNot();
                if (UserSaveAnswer.CANCEL.equals(saveAnswer)) {
                    //fermeture annulée par l'utilsateur:
                    return false;
                }
                if (UserSaveAnswer.SAVE.equals(saveAnswer)) {
                    //l'utilisateur veut une sauvegarde: on renvoie le resultat de l'opération ( si échec pas de fermeture).
                    return otfaService.saveCurrent();
                } else {
                    //l'utilisateur ne veut pas sauvegarder: on recharge simplement la campagne pour être bien aligné avec les fichiers persistés
                    otfaService.reloadCurrent();
                }
            }
            //si non, pas de probleme.
            return true;
        }
        return false;
    }

    /**
     * Rafraichit les etats de JComponent (editable ou pas). Si on passe en mode edit, on supprime de la vue les résultats précédents.
     */
    @Override
    protected void refreshAfterStateChanged() {
        super.refreshAfterStateChanged();
        if (isInEditMode()) {
            otfaService.unsetResultat();
        }
    }

    /**
     * @return true.
     */
    @Override
    public boolean activate() {
        return true;
    }

    @Override
    public Set<String> getDefaultTopComponents() {
        return components;
    }
}
