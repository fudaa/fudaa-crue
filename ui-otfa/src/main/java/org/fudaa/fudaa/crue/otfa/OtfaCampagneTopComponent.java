/*
 * OtfaCampagneTopComponent.java
 *
 * Created on 10 nov. 2011, 15:10:04
 */
package org.fudaa.fudaa.crue.otfa;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne.OtfaRunOptions;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.DisableSortHelper;
import org.fudaa.fudaa.crue.common.helper.TopComponentHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.otfa.action.*;
import org.fudaa.fudaa.crue.otfa.node.OtfaCampagneNode;
import org.fudaa.fudaa.crue.otfa.perspective.PerspectiveServiceOtfa;
import org.fudaa.fudaa.crue.otfa.process.OtfaCampagneContainer;
import org.fudaa.fudaa.crue.otfa.service.OtfaService;
import org.netbeans.api.settings.ConvertAsProperties;
import org.netbeans.swing.etable.ETableColumn;
import org.netbeans.swing.etable.ETableColumnModel;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.nodes.Node;
import org.openide.util.*;
import org.openide.util.Lookup.Result;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Christophe CANEL (Genesis)
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.otfa//OtfaCampagneTopComponent//EN",
    autostore = false)
@TopComponent.Description(preferredID = OtfaCampagneTopComponent.TOPCOMPONENT_ID,
    persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "otfa-editor", openAtStartup = false)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.otfa.OtfaCampagneTopComponent")
@TopComponent.OpenActionRegistration(displayName = "#CTL_OtfaCampagneTopComponentAction",
    preferredID = OtfaCampagneTopComponent.TOPCOMPONENT_ID)
@ActionReference(path = "Menu/Window/OTFA", position = 1)
public class OtfaCampagneTopComponent extends TopComponent implements ExplorerManager.Provider, PropertyChangeListener, LookupListener {
  public static final String TOPCOMPONENT_ID = "OtfaCampagneTopComponent";
  private final ExplorerManager mgr = new ExplorerManager();
  final OtfaService otfaService = Lookup.getDefault().lookup(OtfaService.class);
  final PerspectiveServiceOtfa perspectiveServiceOtfa = Lookup.getDefault().lookup(PerspectiveServiceOtfa.class);
  final Result<OtfaCampagneNode> nodeLookupResult;
  final Result<Boolean> modifiedLookupResult;
  final Result<CtuluLogGroup> validatedLookupResult;
  private final DisableSortHelper sortDisabler;

  /**
   * Creates new form OtfaCampagneTopComponent
   */
  public OtfaCampagneTopComponent() {
    initComponents();
    associateLookup(ExplorerUtils.createLookup(mgr, getActionMap()));

    TopComponentHelper.setMainViewProperties(this);

    this.setDisplayName(NbBundle.getMessage(OtfaCampagneTopComponent.class, "CTL_OtfaCampagneTopComponentAction"));
    this.setToolTipText(NbBundle.getMessage(OtfaCampagneTopComponent.class, "CTL_OtfaCampagneTopComponentAction"));

    this.outlineView.addPropertyColumn("indice", NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaNumLigne"));
    this.outlineView.addPropertyColumn("commentaire", NbBundle.getMessage(OtfaCampagneTopComponent.class, "comment"));
    this.outlineView.addPropertyColumn("referenceEtuFile", NbBundle.getMessage(OtfaCampagneTopComponent.class, "refEtuFile"));
    this.outlineView.addPropertyColumn("referenceScenarioNom", NbBundle.getMessage(OtfaCampagneTopComponent.class, "refScenario"));
    this.outlineView.addPropertyColumn("referenceLaunchCompute", NbBundle.getMessage(OtfaCampagneTopComponent.class,
        "refLaunchCompute"));
    this.outlineView.addPropertyColumn("referenceCoeurName", NbBundle.getMessage(OtfaCampagneTopComponent.class, "refCoeur"));
    //les rapports
    this.outlineView.addPropertyColumn("referenceRapport", NbBundle.getMessage(OtfaCampagneTopComponent.class,
        "referenceRapport"));
    this.outlineView.addPropertyColumn("referenceRapportOnTransitoire", NbBundle.getMessage(OtfaCampagneTopComponent.class,
        "referenceRapportOnTransitoire"));
    this.outlineView.addPropertyColumn("referenceCheminCSV", NbBundle.getMessage(OtfaCampagneTopComponent.class,
        "referenceCheminCSV"));

    this.outlineView.addPropertyColumn("cibleEtuFile", NbBundle.getMessage(OtfaCampagneTopComponent.class, "srcEtuFile"));
    this.outlineView.addPropertyColumn("cibleScenarioNom", NbBundle.getMessage(OtfaCampagneTopComponent.class, "srcScenario"));
    this.outlineView.addPropertyColumn("cibleLaunchCompute", NbBundle.getMessage(OtfaCampagneTopComponent.class,
        "srcLaunchCompute"));
    this.outlineView.addPropertyColumn("cibleCoeurName", NbBundle.getMessage(OtfaCampagneTopComponent.class, "srcCoeur"));
    this.outlineView.addPropertyColumn("lancerComparaison", NbBundle.getMessage(OtfaCampagneTopComponent.class, "lancerComparaison"));
    //les rapports
    this.outlineView.addPropertyColumn("cibleRapport", NbBundle.getMessage(OtfaCampagneTopComponent.class,
        "cibleRapport"));
    this.outlineView.addPropertyColumn("cibleRapportOnTransitoire", NbBundle.getMessage(OtfaCampagneTopComponent.class,
        "cibleRapportOnTransitoire"));
    this.outlineView.addPropertyColumn("cibleCheminCSV", NbBundle.getMessage(OtfaCampagneTopComponent.class,
        "cibleCheminCSV"));

    this.outlineView.addPropertyColumn("logs", NbBundle.getMessage(OtfaCampagneTopComponent.class, "logs"));
    this.outlineView.addPropertyColumn("comparaisons", NbBundle.getMessage(OtfaCampagneTopComponent.class, "comparaisons"));
    this.outlineView.getOutline().setRootVisible(false);

    otfaService.setView(outlineView);
    otfaService.setExplorerManager(mgr);

    // Masquage de la première colonne N° Node native Netbeans
    // remplacée par la colonne "indice" afin de permettre le tri sur les entiers
    TableColumnModel columnModel = this.outlineView.getOutline().getColumnModel();
    ETableColumn column = (ETableColumn) columnModel.getColumn(0);
    ((ETableColumnModel) columnModel).setColumnHidden(column, true);

    sortDisabler = new DisableSortHelper(outlineView);

    this.openOtfaButton.setAction(new OpenFileAction());
    this.addOtfaButton.setAction(new AddFileAction());
    // désactivation du bouton Ajouter une campagne
    this.addOtfaButton.setEnabled(perspectiveServiceOtfa.isInEditMode());
    this.cleanEtuButton.setAction(new CleanAllEtuAction());
    this.saveOtfaButton.setAction(new SaveFileAction());
    this.newOtfaButton.setAction(new NewCampaignAction());
    this.launchOtfaButton.setAction(new LaunchCampagne());
    this.closeOtfaButton.setAction(new CloseFileAction());

    this.upButton.setAction(SystemAction.get(UpLineAction.class).createContextAwareInstance(getLookup()));
    this.duplicateButton.setAction(SystemAction.get(DuplicateLineNodeAction.class).createContextAwareInstance(getLookup()));
    this.downButton.setAction(SystemAction.get(DownLineNodeAction.class).createContextAwareInstance(getLookup()));
    this.addButton.setAction(new AddLineAction());

    this.addButtonAtLine.setAction(SystemAction.get(AddLineAtAction.class).createContextAwareInstance(getLookup()));
    this.renumButton.setAction(new RenumAction());

    this.validateOtfaButton.setAction(new ValidateCampagneAction());
    this.removeButton.setAction(SystemAction.get(RemoveLineNodeAction.class).createContextAwareInstance(getLookup()));

    nodeLookupResult = otfaService.getLookup().lookupResult(OtfaCampagneNode.class);
    nodeLookupResult.addLookupListener(this);
    modifiedLookupResult = otfaService.getLookup().lookupResult(Boolean.class);
    modifiedLookupResult.addLookupListener(this);
    validatedLookupResult = otfaService.getLookup().lookupResult(CtuluLogGroup.class);
    validatedLookupResult.addLookupListener(this);
    resultChanged(null);
    commentaire.getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void insertUpdate(DocumentEvent e) {
        otfaService.setModified();
        updateDataWithUI();
      }

      @Override
      public void removeUpdate(DocumentEvent e) {
        otfaService.setModified();
        updateDataWithUI();
      }

      @Override
      public void changedUpdate(DocumentEvent e) {
        otfaService.setModified();
        updateDataWithUI();
      }
    });
    ActionListener actionListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateDataWithUI();
        otfaService.setModified();
      }
    };

    refDeleteRunsAfterCheckBox.addActionListener(actionListener);
    refDeleteRunsBeforeCheckBox.addActionListener(actionListener);
    ciblesDeleteRunsAfterCheckBox.addActionListener(actionListener);
    ciblesDeleteRunsBeforeCheckBox.addActionListener(actionListener);

    perspectiveServiceOtfa.addStateListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        updateEditableState();
        // abonnement du bouton "Ajouter une campagne" au passage en édition pour activation
        addOtfaButton.setEnabled(perspectiveServiceOtfa.isInEditMode());
      }
    });
    updateEditableState();
  }

  @Override
  public Action[] getActions() {
    return TopComponentHelper.getMainViewActions();
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getTopComponentHelpCtxId("vueOTFA", PerspectiveEnum.TEST));
  }

  public void campagneChanged() {
    Node rootContext = mgr.getRootContext();
    if (rootContext instanceof OtfaCampagneNode) {
      rootContext.removePropertyChangeListener(this);
    }
    OtfaCampagneNode nodes = otfaService.getNodes();
    if (nodes == null) {
      try {
        mgr.setRootContext(Node.EMPTY);
        mgr.setSelectedNodes(new Node[0]);
      } catch (PropertyVetoException ex) {
        Exceptions.printStackTrace(ex);
      }
    } else {
      nodes.addPropertyChangeListener(this);
      mgr.setRootContext(nodes);
    }
    this.repaint();
    this.updateCampagneInformations();
  }

  private void updateDataWithUI() {
    if (updateFromModel) {
      return;
    }
    OtfaCampagneContainer currentOTFA = otfaService.getCurrentOTFA();
    if (currentOTFA != null) {
      OtfaCampagne campaign = currentOTFA.getOtfaCampagne();
      campaign.setCommentaire(commentaire.getText());
      campaign.getReferenceOptions().setEffacerRunApres(refDeleteRunsAfterCheckBox.isSelected());
      campaign.getReferenceOptions().setEffacerRunAvant(refDeleteRunsBeforeCheckBox.isSelected());
      campaign.getCibleOptions().setEffacerRunApres(ciblesDeleteRunsAfterCheckBox.isSelected());
      campaign.getCibleOptions().setEffacerRunAvant(ciblesDeleteRunsBeforeCheckBox.isSelected());
    }
  }

  public Map<JComponent, Boolean> calculLaunched() {
    Map<JComponent, Boolean> res = new HashMap<>();
    res.put(openOtfaButton, openOtfaButton.isEnabled());
    this.openOtfaButton.setEnabled(false);

    res.put(cleanEtuButton, cleanEtuButton.isEnabled());
    this.cleanEtuButton.setEnabled(false);

    res.put(validateOtfaButton, validateOtfaButton.isEnabled());
    this.validateOtfaButton.setEnabled(false);

    res.put(cleanEtuButton, cleanEtuButton.isEnabled());
    this.cleanEtuButton.setEnabled(false);

    res.put(newOtfaButton, newOtfaButton.isEnabled());
    this.newOtfaButton.setEnabled(false);

    res.put(launchOtfaButton, launchOtfaButton.isEnabled());
    this.launchOtfaButton.setEnabled(false);

    res.put(closeOtfaButton, closeOtfaButton.isEnabled());
    this.closeOtfaButton.setEnabled(false);
    return res;
  }

  @Override
  public void resultChanged(LookupEvent ev) {
    if (ev == null || ev.getSource() == nodeLookupResult) {
      campagneChanged();
      updateState();
      updateValidation();
    } else if (ev.getSource() == validatedLookupResult) {
      updateValidation();
    } else if (ev.getSource() == modifiedLookupResult) {
      updateState();
    }
  }

  void writeProperties(java.util.Properties p) {
    p.setProperty("version", "2.0");
    DialogHelper.writeProperties(outlineView, "outlineOtfaView", p);
  }

  void readProperties(java.util.Properties p) {
    if ("2.0".equals(p.getProperty("version"))) {
      DialogHelper.readProperties(outlineView, "outlineOtfaView", p);
    }
  }

  @Override
  protected void componentActivated() {
    super.componentActivated();
    ExplorerUtils.activateActions(mgr, true);
  }

  @Override
  protected void componentDeactivated() {
    super.componentDeactivated();
    ExplorerUtils.activateActions(mgr, false);
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    updateCampagneInformations();
  }

  protected void updateState() {
    OtfaCampagneContainer currentOTFA = otfaService.getCurrentOTFA();
    if (currentOTFA == null) {
      labelState.setText(StringUtils.EMPTY);
    } else if (currentOTFA.getResult() != null) {
      labelState.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaState.WithResult"));
    } else if (otfaService.isModified()) {
      labelState.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaState.Modified"));
    } else {
      labelState.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaState.NotModified"));
    }
  }

  private void updateEditableState() {
    final boolean modifiable = perspectiveServiceOtfa.isInEditMode();
    sortDisabler.setDisableSort(modifiable);
    commentaire.setEditable(modifiable);
    this.refDeleteRunsAfterCheckBox.setEnabled(modifiable);
    this.refDeleteRunsBeforeCheckBox.setEnabled(modifiable);
    this.ciblesDeleteRunsBeforeCheckBox.setEnabled(modifiable);
    this.ciblesDeleteRunsAfterCheckBox.setEnabled(modifiable);
  }

  private class ShowValidationErrorListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      LogsDisplayer.displayError(otfaService.getCurrentValidationState(),
          org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class,
              "OtfaState.ShowErrorButtonLabel"));
    }
  }

  ShowValidationErrorListener listener;

  protected void updateValidation() {
    CtuluLogGroup currentValidationState = otfaService.getCurrentValidationState();
    if (currentValidationState == null || !currentValidationState.containsError()) {
      btShowValidation.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaState.NoError"));
      btShowValidation.setForeground(UIManager.getColor("Button.foreground"));
      btShowValidation.setEnabled(false);
      if (listener != null) {
        btShowValidation.removeActionListener(listener);
      }
    } else {
      btShowValidation.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaState.ContainsError",
          currentValidationState.getNbOccurence(CtuluLogLevel.SEVERE)));
      btShowValidation.setEnabled(true);
      btShowValidation.setForeground(Color.RED);
      if (listener == null) {
        listener = new ShowValidationErrorListener();
      }
      btShowValidation.addActionListener(listener);
    }
  }

  private boolean updateFromModel = false;

  public void updateCampagneInformations() {

    OtfaCampagneContainer currentOTFA = otfaService.getCurrentOTFA();
    updateFromModel = true;
    if (currentOTFA != null) {
      this.jTxtOtfaFile.setText(currentOTFA.getOtfaCampagne().getOtfaFile().getAbsolutePath());
      this.jTxtOtfaFile.setToolTipText(jTxtOtfaFile.getText());

      OtfaCampagne campaign = currentOTFA.getOtfaCampagne();
      this.commentaire.setText(campaign.getCommentaire());
      this.commentaire.setEnabled(true);

      if (campaign.getAuteurCreation() != null) {
        auteurCreation.setText(campaign.getAuteurCreation());
        dateCreation.setText(campaign.getDateCreation().toString());
      }

      if (campaign.getAuteurModification() != null) {
        auteurModification.setText(campaign.getAuteurModification());
        dateModification.setText(campaign.getDateModification().toString());
      }

      final OtfaRunOptions refOptions = campaign.getReferenceOptions();

      refDeleteRunsBeforeCheckBox.setSelected(refOptions.isEffacerRunAvant());
      refDeleteRunsAfterCheckBox.setSelected(refOptions.isEffacerRunApres());
      this.refDeleteRunsAfterCheckBox.setEnabled(true);
      this.refDeleteRunsBeforeCheckBox.setEnabled(true);

      final OtfaRunOptions cibleOptions = campaign.getCibleOptions();

      ciblesDeleteRunsBeforeCheckBox.setSelected(cibleOptions.isEffacerRunAvant());
      ciblesDeleteRunsAfterCheckBox.setSelected(cibleOptions.isEffacerRunApres());
      this.ciblesDeleteRunsBeforeCheckBox.setEnabled(true);
      this.ciblesDeleteRunsAfterCheckBox.setEnabled(true);
    } else {
      jTxtOtfaFile.setText(StringUtils.EMPTY);
      this.jTxtOtfaFile.setToolTipText(jTxtOtfaFile.getText());
      labelState.setText(StringUtils.EMPTY);
      btShowValidation.setText(StringUtils.EMPTY);
      this.commentaire.setText(StringUtils.EMPTY);
      auteurCreation.setText(StringUtils.EMPTY);
      dateCreation.setText(StringUtils.EMPTY);
      auteurModification.setText(StringUtils.EMPTY);
      dateModification.setText(StringUtils.EMPTY);
      refDeleteRunsBeforeCheckBox.setSelected(false);
      refDeleteRunsAfterCheckBox.setSelected(false);
      ciblesDeleteRunsBeforeCheckBox.setSelected(false);
      ciblesDeleteRunsAfterCheckBox.setSelected(false);
      refDeleteRunsBeforeCheckBox.setEnabled(false);
      refDeleteRunsAfterCheckBox.setEnabled(false);
      ciblesDeleteRunsAfterCheckBox.setEnabled(false);
      ciblesDeleteRunsBeforeCheckBox.setEnabled(false);
      this.commentaire.setEnabled(false);
    }
    updateFromModel = false;
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return mgr;
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    campaignManagementPanel = new javax.swing.JPanel();
    jPanel2 = new javax.swing.JPanel();
    commentaire = new javax.swing.JTextField();
    jLabel5 = new javax.swing.JLabel();
    jLabel6 = new javax.swing.JLabel();
    auteurCreation = new javax.swing.JTextField();
    jLabel7 = new javax.swing.JLabel();
    dateCreation = new javax.swing.JTextField();
    jLabel8 = new javax.swing.JLabel();
    auteurModification = new javax.swing.JTextField();
    jLabel9 = new javax.swing.JLabel();
    dateModification = new javax.swing.JTextField();
    jlabelOtfaFile = new javax.swing.JLabel();
    jTxtOtfaFile = new javax.swing.JTextField();
    campaignPanel = new javax.swing.JPanel();
    propertiesOptionsPanel = new javax.swing.JPanel();
    ciblesDeleteRunsAfterCheckBox = new javax.swing.JCheckBox();
    ciblesDeleteRunsBeforeCheckBox = new javax.swing.JCheckBox();
    jLabel2 = new javax.swing.JLabel();
    refDeleteRunsBeforeCheckBox = new javax.swing.JCheckBox();
    refDeleteRunsAfterCheckBox = new javax.swing.JCheckBox();
    jLabel3 = new javax.swing.JLabel();
    buttonsPanel = new javax.swing.JPanel();
    upButton = new javax.swing.JButton();
    addButton = new javax.swing.JButton();
    removeButton = new javax.swing.JButton();
    downButton = new javax.swing.JButton();
    duplicateButton = new javax.swing.JButton();
    addButtonAtLine = new javax.swing.JButton();
    renumButton = new javax.swing.JButton();
    outlineView = new org.openide.explorer.view.OutlineView();
    jScrollPane2 = new javax.swing.JScrollPane();
    jPanelButton = new javax.swing.JPanel();
    newOtfaButton = new javax.swing.JButton();
    openOtfaButton = new javax.swing.JButton();
    closeOtfaButton = new javax.swing.JButton();
    jSeparator2 = new javax.swing.JSeparator();
    cleanEtuButton = new javax.swing.JButton();
    jSeparator1 = new javax.swing.JSeparator();
    launchOtfaButton = new javax.swing.JButton();
    labelState = new javax.swing.JLabel();
    btShowValidation = new javax.swing.JButton();
    validateOtfaButton = new javax.swing.JButton();
    addOtfaButton = new javax.swing.JButton();
    saveOtfaButton = new javax.swing.JButton();

    setLayout(new java.awt.BorderLayout());

    campaignManagementPanel.setLayout(new java.awt.BorderLayout());

    commentaire.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.commentaire.text")); // NOI18N

    jLabel5.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.jLabel5.text")); // NOI18N

    jLabel6.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.jLabel6.text")); // NOI18N

    auteurCreation.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.auteurCreation.text")); // NOI18N
    auteurCreation.setEnabled(false);

    jLabel7.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.jLabel7.text")); // NOI18N

    dateCreation.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.dateCreation.text")); // NOI18N
    dateCreation.setEnabled(false);

    jLabel8.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.jLabel8.text")); // NOI18N

    auteurModification.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.auteurModification.text")); // NOI18N
    auteurModification.setEnabled(false);

    jLabel9.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.jLabel9.text")); // NOI18N

    dateModification.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.dateModification.text")); // NOI18N
    dateModification.setEnabled(false);

    jlabelOtfaFile.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.jlabelOtfaFile.text")); // NOI18N

    jTxtOtfaFile.setEditable(false);
    jTxtOtfaFile.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.jTxtOtfaFile.text")); // NOI18N

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jlabelOtfaFile))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(commentaire, javax.swing.GroupLayout.DEFAULT_SIZE, 785, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(auteurCreation)
                            .addComponent(auteurModification, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE))
                        .addGap(72, 72, 72)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dateModification)
                            .addComponent(dateCreation, javax.swing.GroupLayout.DEFAULT_SIZE, 209, Short.MAX_VALUE)))
                    .addComponent(jTxtOtfaFile))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    jPanel2Layout.setVerticalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlabelOtfaFile)
                    .addComponent(jTxtOtfaFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(commentaire, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(17, 17, 17)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(auteurCreation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dateCreation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(auteurModification, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dateModification, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
    );

    campaignManagementPanel.add(jPanel2, java.awt.BorderLayout.NORTH);

    campaignPanel.setLayout(new java.awt.BorderLayout());

    ciblesDeleteRunsAfterCheckBox
        .setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.ciblesDeleteRunsAfterCheckBox.text")); // NOI18N

    ciblesDeleteRunsBeforeCheckBox
        .setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.ciblesDeleteRunsBeforeCheckBox.text")); // NOI18N

    jLabel2.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.jLabel2.text")); // NOI18N

    refDeleteRunsBeforeCheckBox
        .setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.refDeleteRunsBeforeCheckBox.text")); // NOI18N

    refDeleteRunsAfterCheckBox.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.refDeleteRunsAfterCheckBox.text")); // NOI18N

    jLabel3.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.jLabel3.text")); // NOI18N

    javax.swing.GroupLayout propertiesOptionsPanelLayout = new javax.swing.GroupLayout(propertiesOptionsPanel);
    propertiesOptionsPanel.setLayout(propertiesOptionsPanelLayout);
    propertiesOptionsPanelLayout.setHorizontalGroup(
        propertiesOptionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(propertiesOptionsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(propertiesOptionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(propertiesOptionsPanelLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(propertiesOptionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(refDeleteRunsAfterCheckBox, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(refDeleteRunsBeforeCheckBox))
                        .addGap(162, 162, 162)
                        .addGroup(propertiesOptionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(propertiesOptionsPanelLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(propertiesOptionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ciblesDeleteRunsAfterCheckBox)
                                    .addComponent(ciblesDeleteRunsBeforeCheckBox)))
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel2))
                .addContainerGap(276, Short.MAX_VALUE))
    );
    propertiesOptionsPanelLayout.setVerticalGroup(
        propertiesOptionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(propertiesOptionsPanelLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(propertiesOptionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(propertiesOptionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(refDeleteRunsBeforeCheckBox)
                    .addComponent(ciblesDeleteRunsBeforeCheckBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(propertiesOptionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(refDeleteRunsAfterCheckBox)
                    .addComponent(ciblesDeleteRunsAfterCheckBox))
                .addContainerGap(26, Short.MAX_VALUE))
    );

    campaignPanel.add(propertiesOptionsPanel, java.awt.BorderLayout.NORTH);

    upButton.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.upButton.text")); // NOI18N
    upButton.setActionCommand(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.upButton.actionCommand")); // NOI18N
    upButton.setEnabled(false);
    upButton.setMaximumSize(new java.awt.Dimension(2147483647, 23));

    addButton.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.addButton.text")); // NOI18N
    addButton.setEnabled(false);
    addButton.setMaximumSize(new java.awt.Dimension(2147483647, 23));

    removeButton.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.removeButton.text")); // NOI18N
    removeButton.setActionCommand(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.removeButton.actionCommand")); // NOI18N
    removeButton.setEnabled(false);
    removeButton.setMaximumSize(new java.awt.Dimension(2147483647, 23));

    downButton.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.downButton.text")); // NOI18N
    downButton.setActionCommand(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.downButton.actionCommand")); // NOI18N
    downButton.setEnabled(false);
    downButton.setMaximumSize(new java.awt.Dimension(2147483647, 23));

    duplicateButton.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.duplicateButton.text")); // NOI18N
    duplicateButton.setEnabled(false);

    addButtonAtLine.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.addButtonAtLine.text")); // NOI18N
    addButtonAtLine.setEnabled(false);
    addButtonAtLine.setMaximumSize(new java.awt.Dimension(2147483647, 23));

    renumButton.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.renumButton.actionCommand")); // NOI18N
    renumButton.setActionCommand(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.renumButton.actionCommand")); // NOI18N
    renumButton.setEnabled(false);
    renumButton.setMaximumSize(new java.awt.Dimension(2147483647, 23));

    javax.swing.GroupLayout buttonsPanelLayout = new javax.swing.GroupLayout(buttonsPanel);
    buttonsPanel.setLayout(buttonsPanelLayout);
    buttonsPanelLayout.setHorizontalGroup(
        buttonsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(buttonsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(buttonsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, buttonsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(addButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                            Short.MAX_VALUE)
                        .addComponent(downButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                            Short.MAX_VALUE)
                        .addComponent(upButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                            Short.MAX_VALUE)
                        .addComponent(removeButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)
                        .addComponent(addButtonAtLine, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                            Short.MAX_VALUE)
                        .addComponent(renumButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                            Short.MAX_VALUE))
                    .addComponent(duplicateButton, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE))
                .addContainerGap())
    );
    buttonsPanelLayout.setVerticalGroup(
        buttonsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(buttonsPanelLayout.createSequentialGroup()
                .addComponent(addButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addButtonAtLine, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(duplicateButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(removeButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(upButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(downButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(renumButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(216, Short.MAX_VALUE))
    );

    campaignPanel.add(buttonsPanel, java.awt.BorderLayout.EAST);
    campaignPanel.add(outlineView, java.awt.BorderLayout.CENTER);

    campaignManagementPanel.add(campaignPanel, java.awt.BorderLayout.CENTER);
    campaignManagementPanel.add(jScrollPane2, java.awt.BorderLayout.SOUTH);

    add(campaignManagementPanel, java.awt.BorderLayout.CENTER);

    newOtfaButton.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.newOtfaButton.text")); // NOI18N

    openOtfaButton.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.openOtfaButton.text")); // NOI18N

    closeOtfaButton.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.closeOtfaButton.text")); // NOI18N

    cleanEtuButton.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.cleanEtuButton.text")); // NOI18N

    launchOtfaButton.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.launchOtfaButton.text")); // NOI18N

    labelState.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.labelState.text")); // NOI18N

    btShowValidation.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.btShowValidation.text")); // NOI18N

    validateOtfaButton.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.validateOtfaButton.text")); // NOI18N

    addOtfaButton.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.addOtfaButton.text")); // NOI18N

    saveOtfaButton.setText(org.openide.util.NbBundle.getMessage(OtfaCampagneTopComponent.class, "OtfaCampagneTopComponent.saveOtfaButton.text")); // NOI18N

    javax.swing.GroupLayout jPanelButtonLayout = new javax.swing.GroupLayout(jPanelButton);
    jPanelButton.setLayout(jPanelButtonLayout);
    jPanelButtonLayout.setHorizontalGroup(
        jPanelButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelButtonLayout.createSequentialGroup()
                .addGroup(jPanelButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelButtonLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(newOtfaButton)
                        .addGap(5, 5, 5)
                        .addComponent(openOtfaButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addOtfaButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(btShowValidation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelButtonLayout.createSequentialGroup()
                                .addComponent(validateOtfaButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(5, 5, 5)
                                .addComponent(cleanEtuButton)))
                        .addGap(5, 5, 5)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(saveOtfaButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(closeOtfaButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(launchOtfaButton))
                    .addGroup(jPanelButtonLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(labelState)))
                .addContainerGap(231, Short.MAX_VALUE))
    );
    jPanelButtonLayout.setVerticalGroup(
        jPanelButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelButtonLayout.createSequentialGroup()
                .addGroup(jPanelButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelButtonLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(newOtfaButton))
                    .addGroup(jPanelButtonLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(openOtfaButton))
                    .addGroup(jPanelButtonLayout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelButtonLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(jPanelButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cleanEtuButton)
                            .addComponent(closeOtfaButton)
                            .addComponent(validateOtfaButton)
                            .addComponent(launchOtfaButton)
                            .addComponent(addOtfaButton)
                            .addComponent(saveOtfaButton)))
                    .addGroup(jPanelButtonLayout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(btShowValidation, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelState, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
    );

    add(jPanelButton, java.awt.BorderLayout.NORTH);
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton addButton;
  private javax.swing.JButton addButtonAtLine;
  private javax.swing.JButton addOtfaButton;
  private javax.swing.JTextField auteurCreation;
  private javax.swing.JTextField auteurModification;
  private javax.swing.JButton btShowValidation;
  private javax.swing.JPanel buttonsPanel;
  private javax.swing.JPanel campaignManagementPanel;
  private javax.swing.JPanel campaignPanel;
  private javax.swing.JCheckBox ciblesDeleteRunsAfterCheckBox;
  private javax.swing.JCheckBox ciblesDeleteRunsBeforeCheckBox;
  private javax.swing.JButton cleanEtuButton;
  private javax.swing.JButton closeOtfaButton;
  private javax.swing.JTextField commentaire;
  private javax.swing.JTextField dateCreation;
  private javax.swing.JTextField dateModification;
  private javax.swing.JButton downButton;
  private javax.swing.JButton duplicateButton;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel5;
  private javax.swing.JLabel jLabel6;
  private javax.swing.JLabel jLabel7;
  private javax.swing.JLabel jLabel8;
  private javax.swing.JLabel jLabel9;
  private javax.swing.JPanel jPanel2;
  private javax.swing.JPanel jPanelButton;
  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JSeparator jSeparator1;
  private javax.swing.JSeparator jSeparator2;
  private javax.swing.JTextField jTxtOtfaFile;
  private javax.swing.JLabel jlabelOtfaFile;
  private javax.swing.JLabel labelState;
  private javax.swing.JButton launchOtfaButton;
  private javax.swing.JButton newOtfaButton;
  private javax.swing.JButton openOtfaButton;
  private org.openide.explorer.view.OutlineView outlineView;
  private javax.swing.JPanel propertiesOptionsPanel;
  private javax.swing.JCheckBox refDeleteRunsAfterCheckBox;
  private javax.swing.JCheckBox refDeleteRunsBeforeCheckBox;
  private javax.swing.JButton removeButton;
  private javax.swing.JButton renumButton;
  private javax.swing.JButton saveOtfaButton;
  private javax.swing.JButton upButton;
  private javax.swing.JButton validateOtfaButton;
  // End of variables declaration//GEN-END:variables

  @Override
  public int getPersistenceType() {
    return PERSISTENCE_NEVER;
  }
}
