/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.action;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.openide.util.NbBundle;

/**
 *
 * @author Chris
 */
public class LaunchCampagne extends AbstractOtfaAction {

  public LaunchCampagne() {
    super("LaunchCampagne.ActionName");
    putValue(Action.NAME, NbBundle.getMessage(AddFileAction.class, "LaunchCampagne.ActionName"));
    putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(AddFileAction.class, "LaunchCampagne.ShortDescription"));
  }

  @Override
  protected void updateEnabledState() {
    setEnabled(otfaService.getCurrentOTFA() != null);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    perspectiveServiceOtfa.setDirty(true);
    try {
      otfaService.executeCampagne();
    } finally {
      perspectiveServiceOtfa.setDirty(false);
    }
  }
}
