/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.action;

import javax.swing.Action;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;
import org.fudaa.fudaa.crue.otfa.node.OtfaCampagneLineNode;
import org.fudaa.fudaa.crue.otfa.node.OtfaCampagneNode;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author Christophe CANEL (Genesis)
 */
public class DuplicateLineNodeAction extends OtfaEditNodeAction {

  public DuplicateLineNodeAction() {
    super(NbBundle.getMessage(DuplicateLineNodeAction.class, "DuplicateLineNodeAction.ActionName"));
    putValue(Action.NAME, NbBundle.getMessage(AddFileAction.class, "DuplicateLineNodeAction.ActionName"));
    putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(AddFileAction.class, "DuplicateLineNodeAction.ShortDescription"));
  }

  @Override
  protected boolean isEnable(Node[] nodes) {
    return (nodes.length >= 1);
  }

  @Override
  protected void performAction(Node[] nodes) {
    OtfaCampagneNode lines = null;
    for (Node node : nodes) {
      final OtfaCampagneLineNode name = (OtfaCampagneLineNode) node;
      OtfaCampagneLine newLine = new OtfaCampagneLine(name.getLine());
      lines = otfaService.getNodes();
      lines.add(new OtfaCampagneLineNode(newLine), name.getLine().getIndice());

    }
    if (lines != null) {
      // force le rafraîchissement de la grille
      // si le repaint est appelé dans la boucle for, il se produit des erreurs d'accès concurrent
      lines.repaint();
    }
  }
}
