package org.fudaa.fudaa.crue.otfa;

import org.openide.util.NbBundle;

/**
 * @author deniger
 */
public class MessagesOtfa {
  private MessagesOtfa() {
  }

  public static String getMessage(String code) {
    return NbBundle.getMessage(MessagesOtfa.class, code);
  }
}
