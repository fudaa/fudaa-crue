/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.filter;

import org.fudaa.dodico.crue.io.CustomFileFilterExtension;
import org.fudaa.dodico.crue.io.otfa.OtfaFileUtils;
import org.openide.util.NbBundle;

/**
 *
 * @author Chris
 */
public class OtfaFileFilter {

  public static javax.swing.filechooser.FileFilter create() {
    return new CustomFileFilterExtension(OtfaFileUtils.OTFA_EXTENSION, NbBundle.getMessage(OtfaFileFilter.class, "otfaFileFilterDescription"));
  }
}
