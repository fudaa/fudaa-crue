/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa;

import java.util.List;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLineResult;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneResult;

/**
 *
 * @author Chris
 */
public class OtfaUtils {

  public static OtfaCampagneResult mergeResults(OtfaCampagneResult ctfaResult, OtfaCampagneResult rtfaResult) {
    if ((ctfaResult == null) || (rtfaResult == null)) {
      return null;
    }

    if (ctfaResult.getResults().size() != rtfaResult.getResults().size()) {
      return null;
    }

    OtfaCampagneResult result = ctfaResult;
    final List<OtfaCampagneLineResult> results = result.getResults();
    final List<OtfaCampagneLineResult> rtfaResults = rtfaResult.getResults();

    for (int i = 0; i < results.size(); i++) {
      results.get(i).setComparisonResult(rtfaResults.get(i).getComparisonResults());
    }

    return result;
  }
}
