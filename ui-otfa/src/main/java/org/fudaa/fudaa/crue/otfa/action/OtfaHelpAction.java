package org.fudaa.fudaa.crue.otfa.action;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractHelpAction;
import org.openide.awt.*;

@ActionID(category = "View",
        id = "org.fudaa.fudaa.crue.otfa.action.OtfaHelpAction")
//iconBase ne semble pas fonctionner !
@ActionRegistration(displayName = "#CTL_OtfaHelpAction", iconBase = AbstractHelpAction.HELP_ICON)
@ActionReference(path = "Actions/OTFA", position = 100, separatorBefore = 99)
public final class OtfaHelpAction extends AbstractHelpAction {

  public OtfaHelpAction() {
    super(PerspectiveEnum.TEST);
  }
}
