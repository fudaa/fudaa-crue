/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.property;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.beans.PropertyEditorSupport;
import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.fudaa.crue.otfa.node.OtfaCampagneLineNode.Indicateur;

/**
 *
 * @author Chris
 */
public class IndicateurPropertyEditor extends PropertyEditorSupport {

  public IndicateurPropertyEditor() {
  }

  @Override
  public boolean isPaintable() {
    return true;
  }
  //cf http://blogs.oracle.com/geertjan/entry/propertyeditorsupport_paintvalue

  @Override
  public void paintValue(Graphics gfx, Rectangle box) {
    Color old = gfx.getColor();
    Indicateur indicator = (Indicateur) getValue();
    if (indicator == null) {
      indicator = Indicateur.INDETERMINE;
    }
    gfx.setColor(indicator.getColor());
    ((Graphics2D) gfx).fill(box);
    gfx.setColor(old);
  }

  @Override
  public String getAsText() {
    if (getValue() == null) {
      return "";
    }
    return ObjectUtils.toString(getValue());
  }
}
