/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.action;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.openide.util.NbBundle;

/**
 *
 * @author Chris
 */
public class NewCampaignAction extends AbstractOtfaAction {

  public NewCampaignAction() {
    super("NewCampaignAction.ActionName");
    putValue(Action.NAME, NbBundle.getMessage(NewCampaignAction.class, "NewCampaignAction.ActionName"));
    putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(NewCampaignAction.class, "NewCampaignAction.ShortDescription"));
  }

  @Override
  protected void updateEnabledState() {
  setEnabled(true);
  }
  
  

  @Override
  public void actionPerformed(ActionEvent e) {
    otfaService.createNew();
  }
}
