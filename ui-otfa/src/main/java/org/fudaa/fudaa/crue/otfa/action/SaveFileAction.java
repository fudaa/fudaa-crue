package org.fudaa.fudaa.crue.otfa.action;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.openide.util.Lookup.Result;
import org.openide.util.NbBundle;

/**
 *
 * @author Chris
 */
public class SaveFileAction extends AbstractOtfaAction {

  final Result<Boolean> modifiedResult;

  public SaveFileAction() {
    super("SaveFileAction.ActionName");
    putValue(Action.NAME, NbBundle.getMessage(SaveFileAction.class, "SaveFileAction.ActionName"));
    putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(SaveFileAction.class, "SaveFileAction.ShortDescription"));
    modifiedResult = otfaService.getLookup().lookupResult(Boolean.class);
    modifiedResult.addLookupListener(this);
  }

  @Override
  protected void updateEnabledState() {
    setEnabled(otfaService.getCurrentOTFA() != null && otfaService.isModified());
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    otfaService.saveCurrent();
  }
}
