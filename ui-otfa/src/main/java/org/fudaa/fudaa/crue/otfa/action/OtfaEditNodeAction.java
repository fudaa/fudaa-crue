/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.action;

import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.otfa.node.OtfaCampagneLineNode;
import org.fudaa.fudaa.crue.otfa.node.OtfaCampagneNode;
import org.fudaa.fudaa.crue.otfa.service.OtfaService;
import org.openide.nodes.Children;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

/**
 *
 * @author Christophe CANEL (Genesis)
 */
abstract class OtfaEditNodeAction extends AbstractEditNodeAction {

  final OtfaService otfaService = Lookup.getDefault().lookup(OtfaService.class);

  public OtfaEditNodeAction(String name) {
    super(name);
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  protected static void moveLineNode(OtfaCampagneLineNode movedLine, boolean moveDown) {
    final OtfaCampagneNode lines = (OtfaCampagneNode) movedLine.getParentNode();
    final Children children = lines.getChildren();
    final int indice1 = movedLine.getLine().getIndice() - 1;//start at 0:
    final int indice2 = indice1 + (moveDown ? 1 : -1);
    if (indice2 >= 0 && indice2 < children.getNodesCount()) {
      OtfaCampagneLineNode nodeCible = (OtfaCampagneLineNode) lines.getChildren().getNodeAt(indice2);
      lines.exchange(movedLine, nodeCible);
    }

  }
}
