package org.fudaa.fudaa.crue.otfa.property;

import javax.swing.JComponent;
import javax.swing.JTextArea;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.fudaa.crue.common.editor.CustomEditorAbstract;
import org.fudaa.fudaa.crue.otfa.perspective.PerspectiveServiceOtfa;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.Lookup;

/**
 *
 * @author Yoan GRESSIER
 */
public class CommentaireCustomEditor extends CustomEditorAbstract {
  // lookup sur le service OTFA
  final PerspectiveServiceOtfa perspectiveServiceOtfa = Lookup.getDefault().lookup(PerspectiveServiceOtfa.class);
  // composant d'édition de l'éditeur de propriété
  JTextArea jta;

  public CommentaireCustomEditor(CommentairePropertyEditorSupport editor, PropertyEnv env) {
    super(editor, env);
  }

  @Override
  protected JComponent createComponent() {
    CommentairePropertyEditorSupport scenarioEditor = (CommentairePropertyEditorSupport) editor;

    String commentaires = scenarioEditor.getCampagneligne().getCommentaire();

    jta = new JTextArea(commentaires);
    jta.setEditable(perspectiveServiceOtfa.isInEditMode());
    return jta;
  }

  @Override
  protected Object getPropertyValue() {
    if (jta == null || jta.getText().isEmpty()) {
      return StringUtils.EMPTY;
    }
    return jta.getText();
  }
}
