/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.otfa.process;

import java.io.File;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.otfa.CrueOTFAReaderWriter;
import org.fudaa.dodico.crue.io.otfa.OtfaFileUtils;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.joda.time.LocalDateTime;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;

/**
 *
 * @author Chris
 */
public class SaveOtfaCampagneProcess implements ProgressRunnable<Boolean> {

  final InstallationService installationService = Lookup.getDefault().lookup(InstallationService.class);
  private final File otfaFile;
  private final OtfaCampagne campagne;

  public SaveOtfaCampagneProcess(OtfaCampagne campagne) {
    this.otfaFile = campagne.getOtfaFile();
    this.campagne = campagne;
  }

  @Override
  public Boolean run(ProgressHandle handle) {
    final String auteur = installationService.getConnexionInformation().getCurrentUser();
    final LocalDateTime date = installationService.getConnexionInformation().getCurrentDate();

    if (this.campagne.getAuteurCreation() == null) {
      this.campagne.setAuteurCreation(auteur);
      this.campagne.setDateCreation(date);
    } else {
      this.campagne.setAuteurModification(auteur);
      this.campagne.setDateModification(date);
    }

    File ctfaFile = OtfaFileUtils.getRtfaZipFile(otfaFile);
    File rtfaFile = OtfaFileUtils.getRtfaFile(otfaFile);
    if (ctfaFile.isFile()) {
      CrueFileHelper.deleteFile(ctfaFile,getClass());
    }
    if (rtfaFile.isFile()) {
      CrueFileHelper.deleteFile(rtfaFile,getClass());
    }

    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueOTFAReaderWriter oftaWriter = new CrueOTFAReaderWriter("1.2");
    final CrueIOResu<OtfaCampagne> otfaResu = new CrueIOResu<>(this.campagne);
    return oftaWriter.writeXMLMetier(otfaResu, this.otfaFile, log, null);
  }
}
