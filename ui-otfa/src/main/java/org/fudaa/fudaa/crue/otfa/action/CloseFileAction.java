package org.fudaa.fudaa.crue.otfa.action;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.openide.util.NbBundle;

/**
 *
 * @author Chris
 */
public class CloseFileAction extends AbstractOtfaAction {

  public CloseFileAction() {
    super("CloseFileAction.ActionName");
    putValue(Action.NAME, NbBundle.getMessage(CloseFileAction.class, "CloseFileAction.ActionName"));
    putValue(Action.SHORT_DESCRIPTION, NbBundle.getMessage(CloseFileAction.class, "CloseFileAction.ShortDescription"));
  }
  
  @Override
  protected void updateEnabledState() {
    setEnabled(otfaService.getCurrentOTFA()!=null);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    otfaService.closeCurrent();
  }
}
