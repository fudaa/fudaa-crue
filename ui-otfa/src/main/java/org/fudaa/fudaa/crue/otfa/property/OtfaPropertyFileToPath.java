package org.fudaa.fudaa.crue.otfa.property;

import org.fudaa.fudaa.crue.common.property.PropertyFileToPath;
import org.fudaa.fudaa.crue.otfa.perspective.PerspectiveServiceOtfa;
import org.openide.util.Lookup;

/**
 *
 * @author deniger
 */
public abstract class OtfaPropertyFileToPath extends PropertyFileToPath {

  final PerspectiveServiceOtfa perspectiveServiceOtfa = Lookup.getDefault().lookup(PerspectiveServiceOtfa.class);

  @Override
  public boolean canWrite() {
    return super.canWrite() && perspectiveServiceOtfa.isInEditMode();
  }
}
