package org.fudaa.fudaa.crue.otfa.service;

import java.awt.EventQueue;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.util.List;
import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.io.otfa.OtfaFileUtils;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLines;
import org.fudaa.dodico.crue.io.rtfa.RTFAResultLinesSaver;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneLine;
import org.fudaa.dodico.crue.projet.otfa.OtfaContentValidator;
import org.fudaa.dodico.crue.projet.otfa.OtfaExecutor;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.PostRunService;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.otfa.filter.OtfaFileFilter;
import org.fudaa.fudaa.crue.otfa.node.OtfaCampagneLineNode;
import org.fudaa.fudaa.crue.otfa.node.OtfaCampagneNode;
import org.fudaa.fudaa.crue.otfa.process.OpenOtfaCampagneProcess;
import org.fudaa.fudaa.crue.otfa.process.OtfaCampagneContainer;
import org.fudaa.fudaa.crue.otfa.process.SaveOtfaCampagneProcess;
import org.netbeans.swing.etable.ETableColumn;
import org.netbeans.swing.etable.ETableColumnModel;
import org.openide.NotifyDescriptor;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ServiceProvider;

/**
 * Service principal pour la perspective OTFA qui permet de gérer complètement le cycle de vie d'une campagne: ouverture, modification, validation,
 * sauvegarde et fermeture.<br>
 * <table  border="1"  cellpadding="3" cellspacing="0"  summary="lookups">
 * <tr>
 * <th align="left">Lookup</th>
 * <th align="left">Commentaire</th>
 * <th align="left">Méthode utilisant le lookup</th>
 * </tr>
 * <tr>
 * <td>{@link org.fudaa.fudaa.crue.otfa.service.OtfaState}</td>
 * <td>
 * Indique si une campagne est ouverte ou pas (<code>OtfaState.OPENED</code>)<br>
 * utilisé par le service {@link org.fudaa.fudaa.crue.otfa.perspective.PerspectiveServiceOtfa} pour réagir à la fin du chargement d'une campagne et modifié l'état de la perspective
 * </td>
 * <td></td>
 * </tr>
 * <tr>
 * <td>{@link org.fudaa.fudaa.crue.otfa.node.OtfaCampagneNode}</td>
 * <td>	Le node principal. Utilisé par la vue pour construire la tableau (Outline) et les actions</td>
 * <td><code>{@link #getNodes()}</code></td>
 * </tr>
 * <tr>
 * <td>{@link org.fudaa.fudaa.crue.otfa.process.OtfaCampagneContainer}</td>
 * <td>	Le conteneur complet de la campagne</td>
 * <td><code>{@link #getCurrentOTFA()}</code></td>
 * </tr>
 * <tr>
 * <td>{@link org.fudaa.ctulu.CtuluLogGroup}</td>
 * <td>permet de connaître l'état de validation de la campagne en cours.<br>
 * Mis à jour par la méthode {@link #validate()}.</td>
 * <td><code>{@link #getCurrentValidationState()}</code></td>
 * </tr>
 * <tr>
 * <td>Boolean</td>
 * <td>Permet de connaître l'état "modifié" de la campagne</td>
 * <td><code>{@link #isModified()}</code></td>
 * </tr>
 * </table>
 *
 *
 *
 * @author deniger
 */
@ServiceProvider(service = OtfaService.class)
public class OtfaService implements Lookup.Provider, PropertyChangeListener {

  private final InstanceContent dynamicContent = new InstanceContent();
  /**
   * le lookup de la classe. Mettre à jour la doc principale (voir commentaire de la classe) si modification dans le contenu.
   */
  private final Lookup otfatLookup = new AbstractLookup(dynamicContent);
  /**
   * le service global de configuration ( les coeurs, options de l'application)
   */
  private final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);
  /**
   * le service de lancement de runs. Utilisés pour configurer le lancer d'une campagne
   */
  private final PostRunService postRunService = Lookup.getDefault().lookup(PostRunService.class);

  /**
   * la vue principale de la perspective OTFA.
   */
  private OutlineView view;
  /**
   * la sélection
   */
  ExplorerManager explorerManager;

  /**
   * @return la vue utilisée pour afficher la campagne en cours
   */
  public OutlineView getView() {
    return view;
  }

  /**
   *
   * @param view la vue utilisée pour afficher la campagne en cours. Utilisé par la methode {@link #isSorted() }.
   * @see #isSorted()
   */
  public void setView(OutlineView view) {
    this.view = view;
  }

  /**
   *
   * @return le {@link org.fudaa.ctulu.CtuluLogGroup} du lookup
   */
  public CtuluLogGroup getCurrentValidationState() {
    return otfatLookup.lookup(CtuluLogGroup.class);
  }

  /**
   * @see #getCurrentValidationState()
   * @return true si {@link #getCurrentValidationState} ne contient pas d'erreurs
   */
  public boolean isValid() {
    return getCurrentValidationState() == null || !getCurrentValidationState().containsError();
  }

  /**
   *
   * @return le lookup du service.
   */
  @Override
  public Lookup getLookup() {
    return otfatLookup;
  }

  /**
   * mise à jour de la validation de la campagne en cours
   */
  public void validate() {
    validate(true);
  }

  /**
   *
   * @param all si true, la validation testera si les scenarii et rapport existent.
   */
  private void validate(boolean all) {
    CtuluLogGroup currentValidation = getCurrentValidationState();
    OtfaContentValidator validator = new OtfaContentValidator(configurationManagerService.getCoeurManager());
    CtuluLogGroup newValidation = validator.valid(getCurrentOTFA().getOtfaCampagne(), all, false);
    if (currentValidation != null) {
      dynamicContent.remove(currentValidation);
    }
    dynamicContent.add(newValidation);

  }

  /**
   * Pour savoir si une campagne est chargé il faut s'inscrire au lookup
   *
   * @return la campagne chargée. Null si rien de charge
   */
  public OtfaCampagneContainer getCurrentOTFA() {
    return otfatLookup.lookup(OtfaCampagneContainer.class);
  }

  /**
   * Enregistre le caractère "modifié" de la campagne en cours dans le lookup.
   *
   * @param newValue true si la campagne en cours est modifié. false si enregistré
   */
  private void setModified(boolean newValue) {
    Boolean isModified = isModified();
    if (!ObjectUtils.equals(newValue, isModified)) {
      dynamicContent.remove(isModified);
      dynamicContent.add(newValue);
    }
  }

  /**
   * A utiliser pour indiquer que la campagne en cours est modifiée.
   */
  public void setModified() {
    setModified(true);
  }

  /**
   *
   * @return true si campagne en cours est modifiée
   */
  public boolean isModified() {
    return Boolean.TRUE.equals(otfatLookup.lookup(Boolean.class));
  }

  /**
   * Met à jour les lookup avec la nouvelle campagne
   *
   * @param campagne la campagne à charger
   * @param addState false pour ne pas modifier le lookup OtfaState. Utile pour les rechargements internes.
   */
  private void setCampagne(OtfaCampagneContainer campagne, boolean addState) {
    //on nettoie le lookup actuel
    unsetCampagne(addState);
    //on ajoute les nouvelles valeurs
    dynamicContent.add(campagne);
    dynamicContent.add(new OtfaCampagneNode(campagne));
    //etat non modifié
    setModified(false);
    //on valide la campagne
    validate(true);
    //addState vaut false pour les rechargements interne
    //on modifie le OtfaState pour réveiller les listeners.
    if (addState) {
      dynamicContent.add(OtfaState.OPENED);
    }
    //affichage des erreurs obtenus lors d'un appel batch eventuelle.
    if (campagne.getBatchValidation() != null && campagne.getBatchValidation().containsSomething()) {
      LogsDisplayer.displayError(campagne.getBatchValidation(),
              NbBundle.getMessage(OtfaService.class, "otfa.batchResultat"));
    }
  }

  /**
   * supprime les resultats du container et rafraichit la vue.
   */
  public void unsetResultat() {
    final OtfaCampagneContainer currentOTFA = getCurrentOTFA();
    if (currentOTFA != null && currentOTFA.getResult() != null) {
      currentOTFA.setResult(null);
      unsetCampagne(false);
      setCampagne(currentOTFA, false);
    }
  }

  /**
   * Nettoyage du lookup du service
   *
   * @param removeState est à false pour des rechargements internes. Dans ce cas, le OtfaState n'est pas supprimé
   */
  private void unsetCampagne(boolean removeState) {
    OtfaCampagneContainer current = getCurrentOTFA();
    OtfaCampagneNode node = getNodes();
    //on nettoie le lookup du serive
    if (current != null) {
      dynamicContent.remove(current);
    }
    if (node != null) {
      dynamicContent.remove(node);
    }
    CtuluLogGroup currentValidationState = getCurrentValidationState();
    if (currentValidationState != null) {
      dynamicContent.remove(currentValidationState);
    }
    Boolean isModified = isModified();
    dynamicContent.remove(isModified);
    if (removeState) {
      dynamicContent.remove(OtfaState.OPENED);
    }
  }

  /**
   * Creation d'une nouvelle campagne: ferme la campagne courante et si ok création d'une nouvelle campagne
   */
  public void createNew() {
    //chargment configuration site
    configurationManagerService.reloadAll();
    //probleme de configuration
    if (!configurationManagerService.isConfigValidShowMessage()) {
      return;
    }
    //on ne peut pas fermer la campagne en cours: on sort....
    if (!closeCurrent()) {
      return;
    }
    File home = configurationManagerService.getDefaultDataHome();
    final FileChooserBuilder builder = new FileChooserBuilder(getClass()).setTitle(NbBundle.getMessage(OtfaService.class,
            "createOTFA.FileChooser")).setDefaultWorkingDirectory(
                    home).setApproveText(NbBundle.getMessage(OtfaService.class, "createOTFA.FileChooser"));
    builder.setFileFilter(OtfaFileFilter.create());
    File toSave = builder.showSaveDialog();

    if (toSave
            != null) {
      toSave = OtfaFileUtils.addOtfaExtensionIfNeeded(toSave);
      OtfaCampagneContainer campagne = OtfaCampagneContainer.createBlankCampagne();
      campagne.getOtfaCampagne().setOtfaFile(toSave);
      setCampagne(campagne, true);
      setModified(true);
    }
  }

  /**
   *
   * @return true si une des lignes de la campagne requiert Crue9.
   */
  private boolean needCrue9() {
    OtfaCampagne otfaCampagne = getCurrentOTFA().getOtfaCampagne();
    List<OtfaCampagneLine> lines = otfaCampagne.getLines();
    for (OtfaCampagneLine otfaCampagneLine : lines) {
      if (otfaCampagneLine.getReference().isCrue9() && otfaCampagneLine.getReference().isLaunchCompute()) {
        return true;
      }
      if (otfaCampagneLine.getCible().isCrue9() && otfaCampagneLine.getCible().isLaunchCompute()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Lancement de la campagne.
   */
  public void executeCampagne() {
    //si la configuration du site pas correct: on ne lance pas
    if (!configurationManagerService.isConfigValidShowMessage()) {
      return;
    }
    //validation et sauvegarde de la campagne. si pas corret: on ne lance pas
    if (!saveCurrent()) {
      return;
    }
    //campagne utilisant crue 9: on teste la validité des exes crue 9
    if (needCrue9() && !postRunService.validCrue9()) //on doit vérifier que Crue9 est correctement défini.
    {
      return;
    }
    //on lance la compagne.
    RTFAResultLinesSaver saver = new RTFAResultLinesSaver(getCurrentOTFA().getOtfaCampagne().getOtfaFile());
    OtfaExecutor executor = new OtfaExecutor(saver, configurationManagerService.getCoeurManager(),
            configurationManagerService.getConnexionInformation(),
            postRunService.getCalculCrueRunnerManagerOtfa());

    final LaunchOtfaCampagneRunner process = new LaunchOtfaCampagneRunner(executor, getCurrentOTFA().getOtfaCampagne(), this);
    process.go();
  }

  /**
   * A la fin d'une opération de sauvegarde, envoie un evt et modifie l'état "modifié".
   */
  private void saveDone() {
    final OtfaCampagneNode nodes = getNodes();
    if (nodes != null) {
      nodes.fireSaveDone();
    }
    setModified(Boolean.FALSE);
  }

  /**
   * @param result les resultats à charger après le lancement d'une campagne. Les résulats sont chargés dans un thread à part via
   *               {@link org.fudaa.fudaa.crue.otfa.service.LaunchOtfaCampagneRunner#run}
   * @see org.fudaa.fudaa.crue.otfa.service.LaunchOtfaCampagneRunner#run
   */
  void setResultat(RTFAResultLines result) {
    final OtfaCampagneContainer currentOTFA = this.getCurrentOTFA();
    currentOTFA.setResult(result);
    currentOTFA.setBatchValidation(result.getGlobalValidation());
    unsetCampagne(true);
    setCampagne(currentOTFA, true);
  }

  /**
   *
   * @return le node principal de la campagne
   */
  public OtfaCampagneNode getNodes() {
    return otfatLookup.lookup(OtfaCampagneNode.class);
  }

  /**
   * Attention ne fait pas de sauvegarde et recharge la campagne depuis le fichier.
   */
  public void reloadCurrent() {
    OtfaCampagneContainer currentOTFA = getCurrentOTFA();
    if (currentOTFA != null) {
      File otfaFile = currentOTFA.getOtfaCampagne().getOtfaFile();
      unsetCampagne(true);
      open(otfaFile);

    }
  }

  /**
   * si la campagne est modifiée, teste la validité et lance la sauvegarde
   *
   * @return true si sauvegarde effectuée ( valide et sauvegarde ok)
   */
  public boolean saveCurrent() {
    if (getCurrentOTFA() != null && isModified()) {
      validate(true);
      if (!isValid()) {
        DialogHelper.showError(org.openide.util.NbBundle.getMessage(OtfaService.class, "OtfaSaveAbort.CampagneWithError"));
        return false;
      }
      SaveOtfaCampagneProcess process = new SaveOtfaCampagneProcess(getCurrentOTFA().getOtfaCampagne());
      CrueProgressUtils.showProgressDialogAndRun(process, NbBundle.getMessage(OtfaService.class, "savingCampaign"));
      saveDone();
      return true;
    }
    return true;
  }

  /**
   * Si la campagne en cours est modifiée demande confirmation avant fermeture. Si toutes les opérations sont correctes, renvoie true.
   *
   * @return true si fermée
   */
  public boolean closeCurrent() {
    if (getCurrentOTFA() != null) {
      boolean modified = isModified();
      Object showQuestionCancel;
      final String title = NbBundle.getMessage(OtfaService.class, "CloseOtfa.DialogTitle");
      //si modifie,l'utilisateur a la choix entre femer et sauver/fermer sans sauver et annuler.
      if (modified) {
        String message = NbBundle.getMessage(OtfaService.class, "CloseModifiedOtfa.DialogMessage");
        showQuestionCancel = DialogHelper.showQuestionCancel(title, message);
      } else {
        //pas de modification: on demande simplement confirmation pour la fermeture
        String message = NbBundle.getMessage(OtfaService.class, "CloseOtfa.DialogMessage");
        boolean close = DialogHelper.showQuestion(title, message);
        //on transforme la réponse pour être compatible avec le cas "modifie".
        showQuestionCancel = close ? NotifyDescriptor.NO_OPTION : NotifyDescriptor.CANCEL_OPTION;
      }
      //l'utilisateur annule la fermeture, on ne fait rien
      if (DialogHelper.isCancelOption(showQuestionCancel)) {
        return false;
      }
      //on sauvegarde si modifie et si l'utilisateur a repondu "yes".
      if (modified && DialogHelper.isYesOption(showQuestionCancel)) {
        if (!saveCurrent()) {
          //Si erreur lors de la sauvegarde on ne ferme pas.
          return false;
        }
      }
      //on ferme
      unsetCampagne(true);
    }
    return true;
  }

  /**
   * Ouverture d'une campagne. Si campagne en cours, opération de sauvegarde et fermeture lancée. Si ok alors création.
   *
   * @param toOpen fichier a ouvrir
   */
  public void open(File toOpen) {
    configurationManagerService.reloadAll();
    //probleme de configuration
    if (!configurationManagerService.isConfigValidShowMessage()) {
      return;
    }
    if (getCurrentOTFA() != null) {
      if (!closeCurrent()) {
        return;
      }
    }
    OpenOtfaCampagneProcess process = new OpenOtfaCampagneProcess(toOpen);
    OtfaCampagneContainer campagne = CrueProgressUtils.showProgressDialogAndRun(process,
            NbBundle.getMessage(OtfaService.class,
                    "OpenOTFA.ProcessName", toOpen.getName()));
    setCampagne(campagne, true);
  }

  /**
   * Complétion de la campagne courante avec les lignes de la campagne des fichiers en paramètre
   *
   * @param filesToOpen les fichiers à concaténer à la campagne courante
   */
  public void complete(File[] filesToOpen) {
    // référence sur le conteneur de campagne courante
    OtfaCampagneContainer currentCampaignContainer = getCurrentOTFA();
    if (currentCampaignContainer == null) {
      // pas de campagne chargée, on s'arrête
      return;
    }
    // référence sur la campagne courante 
    OtfaCampagne currentCampaign = currentCampaignContainer.getOtfaCampagne();
    if (currentCampaign == null) {
      // pas de campagne chargée, on s'arrête
      return;
    }

    // recherche du plus grand indice dans la campagne courante
    int oldIndex = 0;
    for (OtfaCampagneLine line : currentCampaign.getLines()) {
      if (oldIndex < line.getIndice()) {
        oldIndex = line.getIndice();
      }
    }

    for (File fileToOpen : filesToOpen) {
      // ouverture du fichier et construction d'un conteneur de campagne
      OpenOtfaCampagneProcess process = new OpenOtfaCampagneProcess(fileToOpen);
      OtfaCampagneContainer newCampaignContainer = CrueProgressUtils.showProgressDialogAndRun(process,
              NbBundle.getMessage(OtfaService.class,
                      "OpenOTFA.ProcessName", fileToOpen.getName()));
      if (newCampaignContainer == null) {
        continue;
      }

      // références sur la campagne à ajouter pour complétion
      OtfaCampagne newCampaign = newCampaignContainer.getOtfaCampagne();

      // renumérotation des indices des nouvelles lignes pour éviter les doublons
      // à chaque changement de campagne, on arrondi à la 10n supérieure
      if (oldIndex != 0) {
        // arrondi à la 10n supérieure
        oldIndex = (((oldIndex - 1) / 10) + 1) * 10;
      }

      // renumérotation des indices de la nouvelle campagne, en démarrant à oldIndex
      for (OtfaCampagneLine line : newCampaign.getLines()) {
        line.setIndice(oldIndex++);
      }

      // ajout des lignes de la nouvelle campagne à la courante
      currentCampaign.getLines().addAll(newCampaign.getLines());
    }

    // refixe la campagne courante pour MAJ de l'affichage
    setCampagne(currentCampaignContainer, false);
    setModified(true);
  }

  /**
   * Met à jour l'état modifié de la campagne et lance une validation. Si la modification vient d'un noeud, le sélectionne à nouveau afin de conserver
   * la sélection après édition. voir https://fudaa-project.atlassian.net/browse/CRUE-654
   *
   * @param evt event
   */
  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    setModified(true);
    validate(false);
    //pour maintenir la selection si on modifie une ligne de campagne.
    //normalement les editeurs "inline" le font mais dans le cas des combobox, cette sélection est perdue.
    if (evt.getSource() instanceof OtfaCampagneLineNode && explorerManager != null) {
      final OtfaCampagneLineNode node = (OtfaCampagneLineNode) evt.getSource();
      EventQueue.invokeLater(new Runnable() {
        @Override
        public void run() {
          try {
            explorerManager.setSelectedNodes(new Node[]{node});
          } catch (PropertyVetoException propertyVetoException) {
            //rien a faire ici. On ignore l'exception
          }
        }
      });
    }
  }

  /**
   * A appeler pour indiquer que la structure de la campagne a été modifiée. Relance une validation de la campagne.
   */
  public void linesStructureChanged() {
    setModified(true);
    validate(false);
  }

  /**
   * Permet de savoir si la grille est triée
   *
   * @return true si la grille est triée, false sinon
   */
  public boolean isSorted() {
    if (view != null) {
      ETableColumnModel columnModel = (ETableColumnModel) view.getOutline().getColumnModel();
      int nb = columnModel.getColumnCount();
      for (int i = 0; i < nb; i++) {
        ETableColumn col = (ETableColumn) columnModel.getColumn(i);
        if (col.isSorted()) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Utiliser pour mettre à jour la selection après l'edition d'une ligne
   *
   * @param mgr ExplorerManager
   */
  public void setExplorerManager(ExplorerManager mgr) {
    this.explorerManager = mgr;
  }
}
