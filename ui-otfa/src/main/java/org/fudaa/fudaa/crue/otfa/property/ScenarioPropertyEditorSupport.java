package org.fudaa.fudaa.crue.otfa.property;

import java.awt.Component;
import java.beans.PropertyEditorSupport;
import java.io.File;
import javax.swing.JDialog;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagne;
import org.fudaa.dodico.crue.projet.otfa.OtfaCampagneItem;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.otfa.perspective.PerspectiveServiceOtfa;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ScenarioPropertyEditorSupport extends PropertyEditorSupport implements ExPropertyEditor {

  protected final ConfigurationManagerService service = Lookup.getDefault().lookup(ConfigurationManagerService.class);
  final PerspectiveServiceOtfa perspectiveServiceOtfa = Lookup.getDefault().lookup(PerspectiveServiceOtfa.class);
  private final OtfaCampagne campagne;
  private final OtfaCampagneItem campagneItem;
  private PropertyEnv env;

  public ScenarioPropertyEditorSupport(OtfaCampagne campagne, OtfaCampagneItem item) {
    this.campagne = campagne;
    this.campagneItem = item;
  }

  public OtfaCampagne getCampagne() {
    return campagne;
  }

  public OtfaCampagneItem getCampagneItem() {
    return campagneItem;
  }

  @Override
  public Component getCustomEditor() {
    JDialog jd;
    // encapsulation dans un JDialog pour gérer la persistence
    if (perspectiveServiceOtfa.isInEditMode()) {
      jd = DialogHelper.createOKCancelDialog(new ScenarioCustomEditor(this, env).getComponent(), this.getClass().getName(), NbBundle.getMessage(
              ScenarioPropertyEditorSupport.class, "scenariosDisplayerTitle"), true, env, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    } else {
      jd = DialogHelper.createCloseDialog(new ScenarioCustomEditor(this, env).getComponent(), this.getClass().getName(), NbBundle.getMessage(
              ScenarioPropertyEditorSupport.class, "scenariosDisplayerTitle"), true, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    }
    return jd;
  }

  @Override
  public void attachEnv(PropertyEnv env) {
    this.env = env;
  }

  @Override
  public boolean supportsCustomEditor() {
    final File etuFile = campagneItem.getEtuFile(campagne.getOtfaDir());
    return etuFile != null && etuFile.isFile();
  }

  @Override
  public String getAsText() {
    return (String) getValue();
  }

  @Override
  public void setAsText(String string) {
    setValue(string);
  }

}
