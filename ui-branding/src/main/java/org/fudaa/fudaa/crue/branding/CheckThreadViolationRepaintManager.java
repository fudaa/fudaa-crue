/*
 GPL 2
 */

package org.fudaa.fudaa.crue.branding;

import java.lang.ref.WeakReference;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.RepaintManager;
import javax.swing.SwingUtilities;

/**
 *
 * @author Frederic Deniger
 */
public class CheckThreadViolationRepaintManager extends RepaintManager {

  // it is recommended to pass the complete check
  private boolean completeCheck = true;
  private WeakReference<JComponent> lastComponent;

  public CheckThreadViolationRepaintManager(final boolean completeCheck) {
    this.completeCheck = completeCheck;
  }

  public CheckThreadViolationRepaintManager() {
    this(true);
  }

  public boolean isCompleteCheck() {
    return completeCheck;
  }

  public void setCompleteCheck(final boolean completeCheck) {
    this.completeCheck = completeCheck;
  }

  @Override
  public synchronized void addInvalidComponent(final JComponent component) {
    checkThreadViolations(component);
    super.addInvalidComponent(component);
  }

  @Override
  public void addDirtyRegion(final JComponent component, final int x, final int y, final int w, final int h) {
    checkThreadViolations(component);
    super.addDirtyRegion(component, x, y, w, h);
  }

  private void checkThreadViolations(final JComponent c) {
    if (!SwingUtilities.isEventDispatchThread() && (completeCheck || c.isShowing())) {
      boolean repaint = false;
      boolean fromSwing = false;
      boolean imageUpdate = false;
      final StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
      for (final StackTraceElement st : stackTrace) {
        if (repaint && st.getClassName().startsWith("javax.swing.")
                && // for details see
                // https://swinghelper.dev.java.net/issues/show_bug.cgi?id=1
                !st.getClassName().startsWith("javax.swing.SwingWorker")) {
          fromSwing = true;
        }
        if (repaint && "imageUpdate".equals(st.getMethodName())) {
          imageUpdate = true;
        }
        if ("repaint".equals(st.getMethodName())) {
          repaint = true;
          fromSwing = false;
        }
      }
      if (imageUpdate) {
                //assuming it is java.awt.image.ImageObserver.imageUpdate(...)
        //image was asynchronously updated, that's ok
        return;
      }
      if (repaint && !fromSwing) {
        //no problems here, since repaint() is thread safe
        return;
      }
      //ignore the last processed component
      if (lastComponent != null && c == lastComponent.get()) {
        return;
      }
      lastComponent = new WeakReference<>(c);
      violationFound(c, stackTrace);
    }
  }

  protected void violationFound(final JComponent c, final StackTraceElement[] stackTrace) {
    System.out.println();
    System.out.println("EDT violation detected");
    System.out.println(c);
    for (final StackTraceElement st : stackTrace) {
      System.out.println("\tat " + st);
    }
  }

  public static void main(final String[] args) throws Exception {
    //set CheckThreadViolationRepaintManager
    RepaintManager.setCurrentManager(new CheckThreadViolationRepaintManager());
    //Valid code
    SwingUtilities.invokeAndWait(() -> test());
    System.out.println("Valid code passed...");
    repaintTest();
    System.out.println("Repaint test - correct code");
    //Invalide code (stack trace expected)
    test();
  }

  static void test() {
    final JFrame frame = new JFrame("Am I on EDT?");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.add(new JButton("JButton"));
    frame.pack();
    frame.setVisible(true);
    frame.dispose();
  }

  //this test must pass
  static void imageUpdateTest() {
    final JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    final JEditorPane editor = new JEditorPane();
    frame.setContentPane(editor);
    editor.setContentType("text/html");
    //it works with no valid image as well
    editor.setText("<html><img src=\"file:\\lala.png\"></html>");
    frame.setSize(300, 200);
    frame.setVisible(true);
  }

  private static JButton test;

  static void repaintTest() {
    try {
      SwingUtilities.invokeAndWait(() -> {
        test = new JButton();
        test.setSize(100, 100);
      });
    } catch (final Exception e) {
      e.printStackTrace();
    }
    // repaint(Rectangle) should be ok
    test.repaint(test.getBounds());
    test.repaint(0, 0, 100, 100);
    test.repaint();
  }
}
