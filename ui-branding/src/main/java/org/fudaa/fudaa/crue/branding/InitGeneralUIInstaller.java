package org.fudaa.fudaa.crue.branding;

import org.openide.awt.Toolbar;
import org.openide.awt.ToolbarPool;
import org.openide.modules.ModuleInfo;
import org.openide.modules.ModuleInstall;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Item;
import org.openide.windows.WindowManager;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.util.logging.Logger;

/**
 * Gere la configuration des Toolbar et l'initialisation des build numbers.
 *
 * @author deniger
 */
@SuppressWarnings("unused")
public class InitGeneralUIInstaller extends ModuleInstall implements Runnable {
  @Override
  public void validate() throws IllegalStateException {
    super.validate();
  }

  @Override
  public void run() {
    JFrame mainWindow = (JFrame) WindowManager.getDefault().getMainWindow();
    mainWindow.getJMenuBar().setVisible(false);
    Toolbar[] toolbars = ToolbarPool.getDefault().getToolbars();
    //pour eviter les chevrons dans les toolbars.
    ToolbarPool.getDefault().setConfiguration("toolbar");
    Toolbar state = getStateToolbar(toolbars);
    if (state != null) {
      state.setOpaque(false);
      state.setBorder(BorderFactory.createCompoundBorder(state.getBorder(), BorderFactory.createEmptyBorder(1, 1, 0, 0)));
    }
    Toolbar perspective = getPerspectiveToolbar(toolbars);
    if (perspective != null) {
      Component[] components = perspective.getComponents();
      perspective.removeAll();
      perspective.setOpaque(false);
      perspective.setLayout(new BorderLayout());
      JPanel buttons = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 0));
      buttons.setOpaque(false);
      Border commonBorder = null;
      for (int i = 0; i < components.length; i++) {
        final AbstractButton cmp = (AbstractButton) components[i];
        cmp.setBorderPainted(true);
        cmp.setOpaque(true);
        if (commonBorder == null) {
          commonBorder = BorderFactory.createCompoundBorder(new RoundedBorder(3), BorderFactory.createEmptyBorder(0, 2, 0, 2));
        }
        cmp.setBorder(commonBorder);
        buttons.add(components[i]);
      }
      buttons.setOpaque(false);
      perspective.add(buttons, BorderLayout.SOUTH);
      perspective.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
    }
    if ("true".equals(System.getProperty("debug-swing"))) {
      RepaintManager.setCurrentManager(new CheckThreadViolationRepaintManager());
    }
    UIManager.getDefaults().put(
        "OptionPane.questionIcon", ImageUtilities.loadImageIcon("org/fudaa/fudaa/crue/branding/question_32.png", false));
    UIManager.getDefaults().put(
        "OptionPane.errorIcon", ImageUtilities.loadImageIcon("org/fudaa/fudaa/crue/branding/error_32.png", false));
    UIManager.getDefaults().put(
        "OptionPane.informationIcon", ImageUtilities.loadImageIcon("org/fudaa/fudaa/crue/branding/info_32.png", false));
    UIManager.getDefaults().put(
        "OptionPane.warningIcon", ImageUtilities.loadImageIcon("org/fudaa/fudaa/crue/branding/warn_32.png", false));
  }

  public static Toolbar getPerspectiveToolbar(Toolbar[] in) {
    return getPerspective("Perspective", in);
  }

  public static Toolbar getStateToolbar(Toolbar[] in) {
    return getPerspective("State", in);
  }

  public static Toolbar getPerspective(String name, Toolbar[] in) {
    if (in == null || name == null) {
      return null;
    }
    for (int i = 0; i < in.length; i++) {
      if (name.equals(in[i].getName())) {
        return in[i];
      }
    }
    return null;
  }

  @Override
  public void restored() {
    System.setProperty("fulog.level", "nothing");
    WindowManager.getDefault().invokeWhenUIReady(this);
  }
}
