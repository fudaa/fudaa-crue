/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.branding;

import java.awt.*;
import javax.swing.AbstractButton;
import javax.swing.border.Border;

/**
 *
 * @author deniger
 */
public class RoundedBorder implements Border {

  public static final Color BLUE_BRIGHTER = Color.BLUE.brighter();
  private final int radius;

  RoundedBorder(int radius) {
    this.radius = radius;
  }

  @Override
  public Insets getBorderInsets(Component c) {
    return new Insets(this.radius + 1, this.radius + 1, this.radius + 2, this.radius);
  }

  @Override
  public boolean isBorderOpaque() {
    return true;
  }

  @Override
  public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
    g.setColor(c.getBackground().darker());
    Graphics2D g2d = (Graphics2D) g;
    Shape clip = g2d.getClip();
    g2d.setStroke(new BasicStroke(1.5f));
    g2d.setClip(x, y, width, height - 1);
    AbstractButton bt = (AbstractButton) c;
    Color initColor=g.getColor();
    if (bt.isSelected()) {
      g.setColor(c.getBackground().darker());
    } else {
      g.setColor(c.getBackground().brighter());
    }
    g.drawRoundRect(x + 1, y + 1, width - 3, 2 * height, radius, radius);
    g.setColor(initColor);
    g.drawRoundRect(x, y, width - 1, 2 * height, radius, radius);
    g.setClip(clip);
  }
}
