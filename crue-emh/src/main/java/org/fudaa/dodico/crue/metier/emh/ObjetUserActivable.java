package org.fudaa.dodico.crue.metier.emh;

/**
 * Interface pour les objets qui sont activables par l'utilisateur
 * @author deniger
 *
 */
public interface ObjetUserActivable {
  
  
  boolean getUserActive();

}
