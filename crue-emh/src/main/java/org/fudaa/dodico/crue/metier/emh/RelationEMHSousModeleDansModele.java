package org.fudaa.dodico.crue.metier.emh;

/**
 * @author deniger
 */
public class RelationEMHSousModeleDansModele extends RelationEMH<EMHModeleBase> {

  public RelationEMHSousModeleDansModele(EMHModeleBase modele) {
    setEmh(modele);
  }

  @Override
  public RelationEMHSousModeleDansModele copyWithEMH(EMHModeleBase copiedEMH) {
    return new RelationEMHSousModeleDansModele(copiedEMH);
  }
}
