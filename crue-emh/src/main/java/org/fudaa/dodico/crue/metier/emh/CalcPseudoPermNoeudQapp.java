package org.fudaa.dodico.crue.metier.emh;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

/**
 * CL de type 2 en Crue9
 *
 */
public class CalcPseudoPermNoeudQapp extends DonCLimMCommonItem implements CalcPseudoPermItem {

  private double qapp;

  public CalcPseudoPermNoeudQapp(final CrueConfigMetier def) {
    qapp = def.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_QAPP);
  }

  @PropertyDesc(i18n = "qapp.property")
  public double getQapp() {
    return qapp;
  }

  @Override
  public String getCcmVariableName() {
    return CrueConfigMetierConstants.PROP_QAPP;
  }

  @Override
  public String getShortName() {
    return BusinessMessages.getString("qapp.property");
  }

  public void setQapp(final double newQapp) {
    qapp = newQapp;
  }

  /**
   * Fonction générique permettant de récupérer les données d'un CalPseudoPerm la valeur
   */
  @Visibility(ihm = false)
  @Override
  public double getValue() {
    return getQapp();
  }

  @Override
  public void setValue(final double newVal) {
    setQapp(newVal);
  }

  CalcPseudoPermNoeudQapp deepClone() {
    try {
      return (CalcPseudoPermNoeudQapp) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransNoeudNiveauContinuTarage.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }
}
