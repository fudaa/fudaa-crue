package org.fudaa.dodico.crue.metier.emh;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.comparator.SimpleClassNameComparator;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public abstract class EMH implements ObjetNomme, Sortable, ObjetUserActivable {
  private Long uiId;
  private String nom;
  private java.util.List<InfosEMH> infosEMH;
  private java.util.List<InfosEMH> infosEMHImmutable;
  private java.util.List<RelationEMH> relationEMH;
  /**
   * Liste publique non modifiable.
   */
  private java.util.List<RelationEMH> relationEMHImmutable;
  private String commentaire = StringUtils.EMPTY;

  /**
   * @return une liste immutable
   */
  public java.util.List<InfosEMH> getInfosEMH() {
    if (infosEMH == null) {
      infosEMH = new java.util.ArrayList<>();
    }
    if (infosEMHImmutable == null) {
      infosEMHImmutable = Collections.unmodifiableList(infosEMH);
    }
    return infosEMHImmutable;
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public Long getUiId() {
    return uiId;
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public boolean isUiIdSet() {
    return uiId != null;
  }

  public void resetUid() {
    uiId = null;
  }

  public boolean hasResultat() {
    return EMHHelper.containsClass(infosEMH, ResultatCalcul.class);
  }

  @UsedByComparison(ignoreInComparison = true)//pour l'instant
  public String getCommentaire() {
    return commentaire;
  }

  public void setCommentaire(String commentaire) {
    this.commentaire = commentaire;
  }

  public void setUiId(Long uiId) {
    this.uiId = uiId;
  }

  /**
   * sort infoEMH only. Do not sort the relationEMH.
   */
  @SuppressWarnings("unchecked")
  @Override
  public boolean sort() {
    if (infosEMH != null) {
      boolean modified = false;
      for (InfosEMH info : infosEMH) {
        if (info instanceof Sortable) {
          final boolean sorted = ((Sortable) info).sort();
          modified |= sorted;
        }
      }
      //On n'avertit pas si on tri les infosEMH: confort pour l'UI uniqement
      Collections.sort(infosEMH, SimpleClassNameComparator.INSTANCE);
      return modified;
    }
    return false;
  }

  /**
   * Copie uniquement les parametres primitifs de l'EMH ( nom, id, commentaire,...) mais pas les infosEMH ou relations.
   *
   * @return EMH copiés.
   */
  public abstract EMH copyShallowFirstLevel();

  /**
   * @param in l'EMH a modifier
   * @return in
   */
  protected <T extends EMH> T copyPrimitiveIn(T in) {
    in.setNom(getNom());
    in.setCommentaire(getCommentaire());
    in.setUiId(getUiId());
    return in;
  }

  /**
   * @return la valeur réelle affectée à l'EMH. Prend en compte les relations de l'EMH. Par exemple, une section appartenant a une branche non active
   * n'est pas active.
   */
  public boolean getActuallyActive() {
    return getUserActive();
  }

  /**
   * Retourne l'info DonCalcSansPrt de la liste des infosEMH. Utilise pour le fichier (DSCP)
   */
  public List<DonCalcSansPrt> getDCSP() {
    return EMHHelper.collectDCSP(this);
  }

  String id;

  @Override
  public String getId() {
    return id;
  }

  /**
   * Retourne la condition initiale DonPrtClInit de la liste des infosEMH. Conditions initiales et manoeuvres aux branches (DPTI)
   */
  public List<DonPrtCIni> getDPTI() {

    return EMHHelper.collectDPTI(this);
  }

  /**
   * Retourne la DonPrtGeo de la liste des infosEMH. DPTG - Fichier des donnees de pretraitement geometrique (xml).
   */
  public List<DonPrtGeo> getDPTG() {
    return EMHHelper.collectDPTG(this);
  }

  public List<DonPrtGeoNomme> getDPTGNommes() {
    return EMHHelper.collectDPTGNomme(this);
  }

  /**
   * Retourne l'info DonCLimM de la liste des infosEMH. Utilisé pour le fichier DCLM
   */
  public List<DonCLimM> getDCLM() {
    return EMHHelper.collectDCLM(this);
  }

  /**
   * @param newInfosEMH
   * @pdGenerated default setter
   */
  public void setInfosEMH(final java.util.List<InfosEMH> newInfosEMH) {
    removeAllInfosEMH();
    addInfosEMH(newInfosEMH);
  }

  public void addInfosEMH(List<? extends InfosEMH> newInfosEMH) {
    for (InfosEMH emh : newInfosEMH) {
      addInfosEMH(emh);
    }
  }

  /**
   * @param newInfosEMH
   * @pdGenerated default add
   */
  public void addInfosEMH(final InfosEMH newInfosEMH) {
    if (newInfosEMH == null) {
      return;
    }
    if (this.infosEMH == null) {
      this.infosEMH = new java.util.ArrayList<>();
    }
    if (!this.infosEMH.contains(newInfosEMH)) {
      newInfosEMH.setEmh(this);
      this.infosEMH.add(newInfosEMH);
    }
  }

  public void removeInfosEMH(final Collection<? extends InfosEMH> oldInfosEMH) {
    for (InfosEMH info : oldInfosEMH) {
      removeInfosEMH(info);
    }
  }

  public void removeInfosEMH(final InfosEMH oldInfosEMH) {
    if (oldInfosEMH == null) {
      return;
    }
    if (this.infosEMH != null && this.infosEMH.contains(oldInfosEMH)) {
      oldInfosEMH.removeEMH(this);
      this.infosEMH.remove(oldInfosEMH);
    }
  }

  public void removeAllRelations(Collection<? extends RelationEMH> toRemove) {
    if (relationEMH != null) {
      relationEMH.removeAll(toRemove);
    }
  }

  /**
   * @param toAdd la liste de relation a ajouter.
   */
  public void addAllRelations(Collection<? extends RelationEMH> toAdd) {
    if (toAdd != null) {
      for (RelationEMH relationEMH : toAdd) {
        addRelationEMH(relationEMH);
      }
    }
  }

  /**
   * @return true si relationEMh est vide
   */
  public boolean isRelationsEMHEmpty() {
    return CollectionUtils.isEmpty(relationEMH);
  }

  /**
   * @return true si relationEMh est non vide
   */
  public boolean isRelationsEMHNotEmpty() {
    return !CollectionUtils.isEmpty(relationEMH);
  }

  /**
   * @return true si relationEMh est vide
   */
  public boolean isInfosEMHEmpty() {
    return CollectionUtils.isEmpty(infosEMH);
  }

  /**
   * @return true si relationEMh est vide
   */
  public boolean isInfosEMHNotEmpty() {
    return !CollectionUtils.isEmpty(infosEMH);
  }

  public void removeAllInfosEMH() {
    if (infosEMH != null) {
      for (InfosEMH info : infosEMH) {
        info.removeEMH(this);
      }
      infosEMH.clear();
    }
  }

  /**
   * @return une liste non modifiable.
   */
  public java.util.List<RelationEMH> getRelationEMH() {
    if (relationEMH == null) {
      relationEMH = new java.util.ArrayList<>();
    }
    if (relationEMHImmutable == null) {
      relationEMHImmutable = Collections.unmodifiableList(relationEMH);
    }
    return relationEMHImmutable;
  }

  public final ResultatCalcul getResultatCalcul() {
    return EMHHelper.selectFirstOfClass(getInfosEMH(), ResultatCalcul.class);
  }



  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public final ResPrtGeo getRPTG() {
    return EMHHelper.selectFirstOfClass(getInfosEMH(), ResPrtGeo.class);
  }

  public final ResultatPasDeTemps getResultatCalculPasDeTemps() {
    return EMHHelper.selectFirstOfClass(getInfosEMH(), ResultatPasDeTemps.class);
  }

  public final void clearAllResultatCalcul() {
    clearResultatCalcul();
    List<EMH> collectEMHInRelationEMHContient = EMHHelper.collectEMHInRelationEMHContient(this);
    for (EMH emh : collectEMHInRelationEMHContient) {
      emh.clearAllResultatCalcul();
    }
  }

  private final void clearResultatCalcul() {
    if (infosEMH != null) {
      infosEMH.remove(EMHHelper.selectFirstOfClass(getInfosEMH(), ResultatCalcul.class));
    }
  }

  public void setRelationEMH(final java.util.List<RelationEMH> newRelationEMH) {
    relationEMH = new ArrayList<>();
    relationEMH.addAll(newRelationEMH);
    relationEMHImmutable = Collections.unmodifiableList(relationEMH);
  }

  public void addRelationEMH(final RelationEMH newRelationEMH) {
    if (newRelationEMH == null) {
      return;
    }
    if (this.relationEMH == null) {
      this.relationEMH = new java.util.ArrayList<>();
    }
    if (!this.relationEMH.contains(newRelationEMH)) {
      this.relationEMH.add(newRelationEMH);
    }
  }

  public void removeRelationEMH(final RelationEMH oldRelationEMH) {
    if (oldRelationEMH == null) {
      return;
    }
    if (this.relationEMH != null && this.relationEMH.contains(oldRelationEMH)) {
      this.relationEMH.remove(oldRelationEMH);
    }
  }

  public void removeAllRelationEMH() {
    if (relationEMH != null) {
      relationEMH.clear();
    }
  }

  @Override
  public final void setNom(final String newNom) {
    nom = newNom;
    if (nom != null) {
      id = nom.toUpperCase();
    }
  }

  @Override
  public final String getNom() {
    return nom;
  }

  public EMH(final String nom) {
    super();
    setNom(nom);
  }

  public final String getType() {
    return getClass().getSimpleName();
  }

  private String emhType;

  public EMH() {
    super();
  }

  /**
   * @return le type
   */
  @Visibility(ihm = false)
  @UsedByComparison(ignoreInComparison = true)
  public String getEmhType() {
    if (emhType == null) {
      emhType = StringUtils.removeStart(getType(), "EMH");
    }
    return emhType;
  }

  public abstract EnumCatEMH getCatType();

  private String typei18n;
  private String typei18nLong;

  @Visibility(ihm = false)
  public final String getTypei18n() {
    if (typei18n == null) {
      typei18n = ToStringHelper.typeToi18n(getType());
    }
    return typei18n;
  }

  @Visibility(ihm = false)
  public final String getLongTypei18n() {
    if (typei18nLong == null) {
      typei18nLong = ToStringHelper.typeToi18nLong(getType());
    }
    return typei18nLong;
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public abstract Object getSubCatType();
}
