/***********************************************************************
 * Module:  RelationEMHCasierDansNoeud.java
 * Author:  deniger
 * Purpose: Defines the Class RelationEMHCasierDansNoeud
 ***********************************************************************/

package org.fudaa.dodico.crue.metier.emh;

public class RelationEMHNoeudContientCasier extends RelationEMH<CatEMHNoeud> {

  @Override
  public RelationEMHNoeudContientCasier copyWithEMH(CatEMHNoeud copiedEMH) {
    RelationEMHNoeudContientCasier res = new RelationEMHNoeudContientCasier();
    res.setEmh(copiedEMH);
    return res;
  }
}
