/**
 * *********************************************************************
 * Module: OrdResScenario.java Author: deniger Purpose: Defines the Class OrdResScenario
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.Arrays;
import java.util.List;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

public class OrdResScenario extends AbstractInfosEMH {

  private OrdResNoeudNiveauContinu ordResNoeudNiveauContinu;
  private OrdResCasier ordResCasier;
  private OrdResSection ordResSection;
  private OrdResBranchePdc ordResBranchePdc;
  private OrdResBrancheSeuilTransversal ordResBrancheSeuilTransversal;
  private OrdResBrancheSeuilLateral ordResBrancheSeuilLateral;
  private OrdResBrancheOrifice ordResBrancheOrifice;
  private OrdResBrancheStrickler ordResBrancheStrickler;
  private OrdResBrancheBarrageGenerique ordResBrancheBarrageGenerique;
  private OrdResBrancheBarrageFilEau ordResBrancheBarrageFilEau;
  private OrdResBrancheSaintVenant ordResBrancheSaintVenant;
  private OrdResBrancheNiveauxAssocies ordResBrancheNiveauxAssocies;
  private OrdResModeleRegul ordResModeleRegul;

  /**
   * @pdGenerated default parent getter
   */
  public OrdResNoeudNiveauContinu getOrdResNoeudNiveauContinu() {
    return ordResNoeudNiveauContinu;
  }

  @UsedByComparison
  public List<OrdRes> getListOfOrdRes() {
    return Arrays.asList(ordResNoeudNiveauContinu,
        ordResCasier,
        ordResSection,
        ordResBranchePdc,
        ordResBrancheSeuilTransversal,
        ordResBrancheSeuilLateral,
        ordResBrancheOrifice,
        ordResBrancheStrickler,
        ordResBrancheBarrageGenerique,
        ordResBrancheBarrageFilEau,
        ordResBrancheSaintVenant,
        ordResBrancheNiveauxAssocies,
        ordResModeleRegul
    );
  }

  public OrdResScenario deepClone() {
    OrdResScenario cloned = new OrdResScenario();
    cloned.ordResNoeudNiveauContinu = ordResNoeudNiveauContinu.clone();
    cloned.ordResCasier = ordResCasier.clone();
    cloned.ordResSection = ordResSection.clone();
    cloned.ordResBranchePdc = ordResBranchePdc.clone();
    cloned.ordResBrancheSeuilTransversal = ordResBrancheSeuilTransversal.clone();
    cloned.ordResBrancheSeuilLateral = ordResBrancheSeuilLateral.clone();
    cloned.ordResBrancheOrifice = ordResBrancheOrifice.clone();
    cloned.ordResBrancheStrickler = ordResBrancheStrickler.clone();
    cloned.ordResBrancheBarrageGenerique = ordResBrancheBarrageGenerique.clone();
    cloned.ordResBrancheBarrageFilEau = ordResBrancheBarrageFilEau.clone();
    cloned.ordResBrancheSaintVenant = ordResBrancheSaintVenant.clone();
    cloned.ordResBrancheNiveauxAssocies = ordResBrancheNiveauxAssocies.clone();
    cloned.ordResModeleRegul = ordResModeleRegul.clone();
    return cloned;
  }

  /**
   * @param newOrdResNoeudNiveauContinu
   */
  public void setOrdResNoeudNiveauContinu(final OrdResNoeudNiveauContinu newOrdResNoeudNiveauContinu) {
    this.ordResNoeudNiveauContinu = newOrdResNoeudNiveauContinu;
  }

  /**
   *
   */
  public OrdResCasier getOrdResCasier() {
    return ordResCasier;
  }

  /**
   * @param newOrdResCasier
   */
  public void setOrdResCasier(final OrdResCasier newOrdResCasier) {
    this.ordResCasier = newOrdResCasier;
  }

  /**
   *
   */
  public OrdResSection getOrdResSection() {
    return ordResSection;
  }

  public void setOrdResSection(final OrdResSection newOrdResSection) {
    this.ordResSection = newOrdResSection;
  }

  /**
   *
   */
  public OrdResBranchePdc getOrdResBranchePdc() {
    return ordResBranchePdc;
  }

  /**
   * @param newOrdResBranchePdc
   */
  public void setOrdResBranchePdc(final OrdResBranchePdc newOrdResBranchePdc) {
    this.ordResBranchePdc = newOrdResBranchePdc;
  }

  /**
   *
   */
  public OrdResBrancheSeuilTransversal getOrdResBrancheSeuilTransversal() {
    return ordResBrancheSeuilTransversal;
  }

  /**
   * @param newOrdResBrancheSeuilTransversal
   */
  public void setOrdResBrancheSeuilTransversal(final OrdResBrancheSeuilTransversal newOrdResBrancheSeuilTransversal) {
    this.ordResBrancheSeuilTransversal = newOrdResBrancheSeuilTransversal;
  }

  /**
   *
   */
  public OrdResBrancheSeuilLateral getOrdResBrancheSeuilLateral() {
    return ordResBrancheSeuilLateral;
  }

  /**
   * @param newOrdResBrancheSeuilLateral
   */
  public void setOrdResBrancheSeuilLateral(final OrdResBrancheSeuilLateral newOrdResBrancheSeuilLateral) {
    this.ordResBrancheSeuilLateral = newOrdResBrancheSeuilLateral;
  }

  public OrdResBrancheOrifice getOrdResBrancheOrifice() {
    return ordResBrancheOrifice;
  }

  /**
   * @param newOrdResBrancheOrifice
   */
  public void setOrdResBrancheOrifice(final OrdResBrancheOrifice newOrdResBrancheOrifice) {
    this.ordResBrancheOrifice = newOrdResBrancheOrifice;
  }

  public OrdResBrancheStrickler getOrdResBrancheStrickler() {
    return ordResBrancheStrickler;
  }

  /**
   * @param newOrdResBrancheStrickler
   */
  public void setOrdResBrancheStrickler(final OrdResBrancheStrickler newOrdResBrancheStrickler) {
    this.ordResBrancheStrickler = newOrdResBrancheStrickler;
  }

  public OrdResBrancheBarrageGenerique getOrdResBrancheBarrageGenerique() {
    return ordResBrancheBarrageGenerique;
  }

  /**
   * @param newOrdResBrancheBarrageGenerique
   */
  public void setOrdResBrancheBarrageGenerique(final OrdResBrancheBarrageGenerique newOrdResBrancheBarrageGenerique) {
    this.ordResBrancheBarrageGenerique = newOrdResBrancheBarrageGenerique;
  }

  public OrdResBrancheBarrageFilEau getOrdResBrancheBarrageFilEau() {
    return ordResBrancheBarrageFilEau;
  }

  /**
   * @param newOrdResBrancheBarrageFilEau
   */
  public void setOrdResBrancheBarrageFilEau(final OrdResBrancheBarrageFilEau newOrdResBrancheBarrageFilEau) {
    this.ordResBrancheBarrageFilEau = newOrdResBrancheBarrageFilEau;
  }

  public OrdResBrancheSaintVenant getOrdResBrancheSaintVenant() {
    return ordResBrancheSaintVenant;
  }

  /**
   * @param newOrdResBrancheSaintVenant
   */
  public void setOrdResBrancheSaintVenant(final OrdResBrancheSaintVenant newOrdResBrancheSaintVenant) {
    this.ordResBrancheSaintVenant = newOrdResBrancheSaintVenant;
  }

  @Override
  public final EnumInfosEMH getCatType() {
    return EnumInfosEMH.ORD_RES_SCENARIO;
  }

  /**
   * @return the ordResBrancheNiveauxAssocies
   */
  public OrdResBrancheNiveauxAssocies getOrdResBrancheNiveauxAssocies() {
    return ordResBrancheNiveauxAssocies;
  }

  /**
   * @param ordResBrancheNiveauxAssocies the ordResBrancheNiveauxAssocies to set
   */
  public void setOrdResBrancheNiveauxAssocies(OrdResBrancheNiveauxAssocies ordResBrancheNiveauxAssocies) {
    this.ordResBrancheNiveauxAssocies = ordResBrancheNiveauxAssocies;
  }


  public void setOrdResModeleRegul(final OrdResModeleRegul ordResModeleRegul) {
    this.ordResModeleRegul = ordResModeleRegul;
  }

  public OrdResModeleRegul getOrdResModeleRegul() {
    return this.ordResModeleRegul;
  }

  @Visibility(ihm = false)
  public boolean isModeleQZRegulActivated(){
    return ordResModeleRegul!=null && ordResModeleRegul.isQZRegul();
  }
}