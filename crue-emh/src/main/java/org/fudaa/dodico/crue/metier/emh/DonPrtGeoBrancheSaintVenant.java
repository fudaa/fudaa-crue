package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

public class DonPrtGeoBrancheSaintVenant extends DonPrtGeo implements Cloneable{
  public static final String PROP_COEFSINUO = "coefSinuo";
  private double coefSinuo;

  public DonPrtGeoBrancheSaintVenant(final CrueConfigMetier defaults) {
    coefSinuo = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_COEF_SINUO);
  }

  public DonPrtGeoBrancheSaintVenant(final DonPrtGeoBrancheSaintVenant defaults) {
    coefSinuo = defaults.coefSinuo;
  }

  @PropertyDesc(i18n = "coefSinuo.property")
  public double getCoefSinuo() {
    return coefSinuo;
  }

  @Override
  public DonPrtGeoBrancheSaintVenant clone() throws CloneNotSupportedException {
    return (DonPrtGeoBrancheSaintVenant)super.clone();
  }

  @Override
  public DonPrtGeo cloneDPTG() {
    try {
      return (DonPrtGeo) clone();
    } catch (CloneNotSupportedException e) {
    }
    return null;
  }

  /**
   * @param newCoefSinuo le nouveau coefficient
   */
  public void setCoefSinuo(final double newCoefSinuo) {
    coefSinuo = newCoefSinuo;
  }
}
