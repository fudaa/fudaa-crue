/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;

/**
 *
 * @author Frederic Deniger
 */
public class DonPrtGeoProfilEtiquette implements ToStringTransformable, Cloneable {

  private ItemEnum etiquette;
  private PtProfil point;

  public static Map<ItemEnum, DonPrtGeoProfilEtiquette> createMap(Collection<DonPrtGeoProfilEtiquette> in) {
    Map<ItemEnum, DonPrtGeoProfilEtiquette> res = new HashMap<>();
    if (in != null) {
      for (DonPrtGeoProfilEtiquette donPrtGeoProfilEtiquette : in) {
        res.put(donPrtGeoProfilEtiquette.getTypeEtiquette(), donPrtGeoProfilEtiquette);
      }
    }
    return res;
  }

  public ItemEnum getTypeEtiquette() {
    return etiquette;
  }

  public void setTypeEtiquette(ItemEnum etiquette) {
    this.etiquette = etiquette;
  }

  public PtProfil getPoint() {
    return point;
  }

  public void setPoint(PtProfil point) {
    this.point = point;
  }

  private static String limToString(PtProfil pt, CrueConfigMetier props, EnumToString format,DecimalFormatEpsilonEnum type) {
    return pt == null ? null : pt.toString(props, format, type);
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, DecimalFormatEpsilonEnum type) {
    return etiquette == null ? "null" : etiquette.geti18n() + ", point=" + limToString(point, props, format,type);
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + ", etiquette " + (etiquette == null ? "null" : etiquette.geti18n()) + ", point= " + point;
  }

  @Override
  public DonPrtGeoProfilEtiquette clone() {
    try {
      DonPrtGeoProfilEtiquette res = (DonPrtGeoProfilEtiquette) super.clone();
      if (point != null) {
        res.point = point.clone();
      }

      return res;
    } catch (CloneNotSupportedException cloneNotSupportedException) {
      //nothing to do :(
    }
    throw new IllegalAccessError("why ?");
  }
}
