package org.fudaa.dodico.crue.metier.emh;

import java.util.ArrayList;
import java.util.Collection;

/**
 * type 4 Crue9
 */
public class DonCalcSansPrtBrancheSeuilLateral extends DonCalcSansPrtBrancheSeuil {
  private java.util.List<ElemSeuilAvecPdc> elemSeuilAvecPdc;

  public java.util.List<ElemSeuilAvecPdc> getElemSeuilAvecPdc() {
    if (elemSeuilAvecPdc == null) {
      elemSeuilAvecPdc = new java.util.ArrayList<>();
    }
    return elemSeuilAvecPdc;
  }

  @Override
  public DonCalcSansPrt cloneDCSP() {
    try {
      DonCalcSansPrtBrancheSeuilLateral res = (DonCalcSansPrtBrancheSeuilLateral) super.clone();
      if (elemSeuilAvecPdc != null) {
        res.elemSeuilAvecPdc = new ArrayList<>();
        elemSeuilAvecPdc.forEach(seuil -> res.elemSeuilAvecPdc.add(seuil.clone()));
      }
      return res;
    } catch (CloneNotSupportedException e) {
    }
    return null;
  }

  public void setElemSeuilAvecPdc(final java.util.List<ElemSeuilAvecPdc> newElemSeuilAvecPdc) {
    removeAllElemSeuilAvecPdc();
    for (ElemSeuilAvecPdc seuilAvecPdc : newElemSeuilAvecPdc) {
      addElemSeuilAvecPdc(seuilAvecPdc);
    }
  }

  @Override
  public void fillWithLoi(final Collection<Loi> target) {
  }

  public void addElemSeuilAvecPdc(final ElemSeuilAvecPdc newElemSeuilAvecPdc) {
    if (newElemSeuilAvecPdc == null) {
      return;
    }
    if (this.elemSeuilAvecPdc == null) {
      this.elemSeuilAvecPdc = new java.util.ArrayList<>();
    }
    if (!this.elemSeuilAvecPdc.contains(newElemSeuilAvecPdc)) {
      this.elemSeuilAvecPdc.add(newElemSeuilAvecPdc);
    }
  }

  public void removeAllElemSeuilAvecPdc() {
    if (elemSeuilAvecPdc != null) {
      elemSeuilAvecPdc.clear();
    }
  }
}
