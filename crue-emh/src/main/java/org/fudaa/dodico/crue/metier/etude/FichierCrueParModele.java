/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.etude;

/**
 * Permet d'avoir la relation entre le fichier crue et son modele contenant
 * 
 * @author deniger
 */
public class FichierCrueParModele {
  private FichierCrue fichier;
  private ManagerEMHModeleBase parent;

  public FichierCrueParModele() {

  }

  public FichierCrueParModele(final FichierCrue fichier, final ManagerEMHModeleBase parent) {
    super();
    this.fichier = fichier;
    this.parent = parent;
  }

  /**
   * @return the fichier
   */
  public FichierCrue getFichier() {
    return fichier;
  }

  /**
   * @param fichier the fichier to set
   */
  public void setFichier(final FichierCrue fichier) {
    this.fichier = fichier;
  }

  /**
   * @return the parent
   */
  public ManagerEMHModeleBase getParent() {
    return parent;
  }

  /**
   * @param parent the parent to set
   */
  public void setParent(final ManagerEMHModeleBase parent) {
    this.parent = parent;
  }

}
