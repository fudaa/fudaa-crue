/**
 * *********************************************************************
 * Module: ParamNumCalcTrans.java Author: deniger Purpose: Defines the Class ParamNumCalcTrans
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ParamNumCalcTrans implements Cloneable {
  private double crMaxFlu;
  private double crMaxTor;
  private double thetaPreissmann;
  private Pdt pdt;

  public ParamNumCalcTrans(CrueConfigMetier defaultValues) {
    crMaxFlu = defaultValues.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_CR_MAX_FLU);
    crMaxTor = defaultValues.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_CR_MAX_TOR);
    thetaPreissmann = defaultValues.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_THETA_PREISSMANN);
    pdt = PdtCst.getDefaultPdtValue(defaultValues, "pdtTrans");
  }

  @Override
  protected Object clone() {
    try {
      return super.clone();
    } catch (CloneNotSupportedException ex) {
      Logger.getLogger(ParamNumCalcTrans.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("why ?");
  }

  public ParamNumCalcTrans deepClone() {
    ParamNumCalcTrans res = (ParamNumCalcTrans) clone();
    res.pdt = pdt == null ? null : pdt.deepClone();
    return res;
  }

  @PropertyDesc(i18n = "crMaxFlu.property")
  public final double getCrMaxFlu() {
    return crMaxFlu;
  }

  @PropertyDesc(i18n = "crMaxTor.property")
  public final double getCrMaxTor() {
    return crMaxTor;
  }

  @PropertyDesc(i18n = "pdt.property", order = 0)
  public Pdt getPdt() {
    return pdt;
  }

  @PropertyDesc(i18n = "thetaPreissmann.property")
  public final double getThetaPreissmann() {
    return thetaPreissmann;
  }

  public final void setCrMaxFlu(double newCrMaxFlu) {
    crMaxFlu = newCrMaxFlu;
  }

  public final void setCrMaxTor(double newCrMaxTor) {
    crMaxTor = newCrMaxTor;
  }

  public void setPdt(Pdt newPdt) {
    this.pdt = newPdt;
  }

  public final void setThetaPreissmann(double newThetaPreissmann) {
    thetaPreissmann = newThetaPreissmann;
  }

  @Override
  public String toString() {
    return "ParamNumCalcTrans [crMaxFlu=" + crMaxFlu + ", crMaxTor=" + crMaxTor + ", pdt=" + pdt + ", thetaPreissmann="
        + thetaPreissmann + "]";
  }
}
