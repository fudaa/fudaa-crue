package org.fudaa.dodico.crue.metier.result;

public final class ResCalSection extends ResCalcul {

  /**
   * @param stot the stot to set
   */
  public void setStot(double stot) {
    super.addResultat("stot", stot);
  }

  /**
   * @param vact the vact to set
   */
  public void setVact(double vact) {
    super.addResultat("vact", vact);
  }

  /**
   * @param vc the vc to set
   */
  public void setVc(double vc) {
    super.addResultat("vc", vc);
  }

  public void setQ(double q) {
    super.addResultat("q", q);
  }

  /**
   * @param z the z to set
   */
  public void setZ(double z) {
    super.addResultat("z", z);
  }
}