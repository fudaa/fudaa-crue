/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.cini;

import java.util.Objects;

/**
 * @author Frederic Deniger
 */
public class CiniImportKey implements Comparable<CiniImportKey> {
  private final String emh;
  private final String typeValue;

  public CiniImportKey(final String emhName, final String typeValue) {
    this.emh = emhName;
    this.typeValue = typeValue.toUpperCase();
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 59 * hash + (this.emh != null ? this.emh.hashCode() : 0);
    hash = 59 * hash + (this.typeValue != null ? this.typeValue.hashCode() : 0);
    return hash;
  }

  @Override
  public int compareTo(final CiniImportKey o) {
    if (o == null) {
      return 1;
    }
    int res = emh.compareTo(o.emh);
    if (res == 0) {
      res = typeValue.compareTo(o.typeValue);
    }
    return res;
  }

  public String getEmh() {
    return emh;
  }

  public String getTypeValue() {
    return typeValue;
  }

  @Override
  public String toString() {
    return emh + ", " + typeValue;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final CiniImportKey other = (CiniImportKey) obj;
    if (!Objects.equals(this.emh, other.emh)) {
      return false;
    }
    return Objects.equals(this.typeValue, other.typeValue);
  }
}
