/**
 * *********************************************************************
 * Module: OrdCalcTrans.java Author: deniger Purpose: Defines the Class OrdCalcTrans
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.joda.time.Duration;

public abstract class OrdCalcTrans extends OrdCalc {
  private CalcTrans calcTrans;
  private Duration dureeCalc;
  private Pdt pdtRes;
  private PrendreClichePonctuel prendreClichePonctuel;
  private PrendreClichePeriodique prendreClichePeriodique;

  @PropertyDesc(i18n = "prendreClichePeriodique.property")
  public PrendreClichePeriodique getPrendreClichePeriodique() {
    return prendreClichePeriodique;
  }

  @Override
  public List getCliches() {
    return Arrays.asList(prendreClichePonctuel, prendreClichePeriodique);
  }

  public void setPrendreClichePeriodique(PrendreClichePeriodique prendreClichePeriodique) {
    this.prendreClichePeriodique = prendreClichePeriodique;
  }

  @Override
  public OrdCalc deepCloneButNotCalc(Calc newCalc) {
    try {
      OrdCalcTrans res = (OrdCalcTrans) super.clone();
      if (prendreClichePeriodique != null) {
        res.prendreClichePeriodique = prendreClichePeriodique.clone();
      }
      if (prendreClichePonctuel != null) {
        res.prendreClichePonctuel = prendreClichePonctuel.clone();
      }
      if (pdtRes != null) {
        res.pdtRes = pdtRes.deepClone();
      }
      res.setCalcTrans((CalcTrans) newCalc);
      return res;
    } catch (CloneNotSupportedException ex) {
      Logger.getLogger(OrdCalcTrans.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why ?");
  }

  public OrdCalcTrans(CrueConfigMetier props) {
    pdtRes = PdtCst.getDefaultPdtValue(props, CrueConfigMetierConstants.PROP_PDT_RES);
    dureeCalc = props.getDefaultDurationValue(CrueConfigMetierConstants.PROP_DUREE_CALC);
  }

  @PropertyDesc(i18n = "prendreClichePonctuel.property")
  public PrendreClichePonctuel getPrendreClichePonctuel() {
    return prendreClichePonctuel;
  }

  public void setPrendreClichePonctuel(PrendreClichePonctuel prendreClichePonctuel) {
    this.prendreClichePonctuel = prendreClichePonctuel;
  }

  @PropertyDesc(i18n = "dureeCalc.property")
  public Duration getDureeCalc() {
    return dureeCalc;
  }

  public void setDureeCalc(Duration dureeCalc) {
    this.dureeCalc = dureeCalc;
  }

  @PropertyDesc(i18n = "pdtRes.property")
  public Pdt getPdtRes() {
    return pdtRes;
  }

  public void setPdtRes(Pdt pdtRes) {
    this.pdtRes = pdtRes;
  }

  /**
   * @return true si transitoire
   */
  @Override
  public boolean isTransitoire() {
    return true;
  }

  /**
   * @pdGenerated default parent getter
   */
  public CalcTrans getCalcTrans() {
    return calcTrans;
  }

  @Override
  public CalcTrans getCalc() {
    return calcTrans;
  }

  /**
   * @pdGenerated default parent setter
   *
   * @param newCalcTrans
   */
  public void setCalcTrans(CalcTrans newCalcTrans) {
    this.calcTrans = newCalcTrans;
  }
}
