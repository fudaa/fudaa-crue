package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

/**
 * @author Adrien Hadoux Utilise pour DPTI a la fois pour branche et casier.
 */
public class DonPrtCIniBranche extends DonPrtCIni<DonPrtCIniBranche> {

  private double qini;

  public DonPrtCIniBranche(CrueConfigMetier def) {
    qini = def.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_QINI);
  }

  @Override
  public void initFrom(DonPrtCIniBranche donPrtCIniBranche) {
    qini = donPrtCIniBranche.qini;
  }

  @PropertyDesc(i18n = "qini.property")
  public final double getQini() {
    return qini;
  }

  /**
   */
  public final void setQini(double newQini) {
    qini = newQini;
  }
}
