package org.fudaa.dodico.crue.metier.helper;

import org.fudaa.dodico.crue.metier.emh.*;

import java.util.*;

/**
 * @author deniger
 */
public class DonFrtHelper {
  public static List<DonFrt> getDonFrt(EMHSousModele ssModele, List<DonFrt> list) {
    list.addAll(ssModele.getFrtConteneur().getListFrt());
    return list;
  }

  public static List<DonFrt> getDonFrt(EMHSousModele ssModele) {
    return getDonFrt(ssModele, new ArrayList<>());
  }

  public static boolean isUsedBy(DonFrt frt, DonPrtGeoProfilSection dptg) {
    List<LitNumerote> litNumerotes = dptg.getLitNumerote();
    for (LitNumerote litNumerote : litNumerotes) {
      if (litNumerote.getFrot() == frt) {
        return true;
      }
    }
    return false;
  }

  public static Set<DonFrt> getUsedDonFrt(Collection<? extends CatEMHSection> sections) {
    Set<DonFrt> res = new HashSet<>();
    for (CatEMHSection section : sections) {
      if (section.getSectionType().equals(EnumSectionType.EMHSectionProfil)) {
        DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection((EMHSectionProfil) section);
        collectDonFrt(profilSection, res);
      }
    }
    return res;
  }

  public static Set<DonFrt> collectDonFrt(DonPrtGeoProfilSection profilSection) {
    return collectDonFrt(profilSection, new HashSet<>());
  }

  public static Set<DonFrt> collectDonFrt(DonPrtGeoProfilSection profilSection, Set<DonFrt> res) {
    List<LitNumerote> litNumerotes = profilSection.getLitNumerote();
    for (LitNumerote litNumerote : litNumerotes) {
      if (litNumerote.getFrot() != null) {
        res.add(litNumerote.getFrot());
      }
    }
    return res;
  }

  /**
   * @param modele
   * @param filter si non null, seuls les donFrt de cette collection seront pris compte.
   */
  public static Map<DonFrt, Set<CatEMHSection>> getUsedBy(EMHSousModele modele, Set<DonFrt> filter) {
    Map<DonFrt, Set<CatEMHSection>> res = new HashMap<>();
    List<CatEMHSection> sections = modele.getSections();
    for (CatEMHSection section : sections) {
      if (section.getSectionType().equals(EnumSectionType.EMHSectionProfil)) {
        DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection((EMHSectionProfil) section);
        List<LitNumerote> litNumerotes = profilSection.getLitNumerote();
        for (LitNumerote litNumerote : litNumerotes) {
          DonFrt frot = litNumerote.getFrot();
          if (filter == null || filter.contains(frot)) {
            Set<CatEMHSection> set = res.get(frot);
            if (set == null) {
              set = new HashSet<>();
              res.put(frot, set);
            }
            set.add(section);
          }
        }
      }
    }
    return res;
  }

  public static List<DonFrt> getDonFrt(EMHScenario scenario) {
    ArrayList<DonFrt> res = new ArrayList<>();
    List<EMHSousModele> sousModeles = scenario.getSousModeles();
    for (EMHSousModele sousModele : sousModeles) {
      getDonFrt(sousModele, res);
    }
    return res;
  }

  public static Map<String, String> getSousModeleNameByDonFrtName(EMHScenario scenario) {
    Map<String, String> res = new HashMap<>();
    List<EMHSousModele> sousModeles = scenario.getSousModeles();
    for (EMHSousModele sousModele : sousModeles) {
      final List<DonFrt> donFrt = getDonFrt(sousModele);
      donFrt.forEach(frt -> res.put(frt.getNom(), sousModele.getNom()));
    }
    return res;
  }
}
