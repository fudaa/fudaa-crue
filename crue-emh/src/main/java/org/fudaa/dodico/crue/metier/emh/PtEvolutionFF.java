package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;

public final class PtEvolutionFF implements Point2D {
  private double abscisse;
  private double ordonnee;

  public PtEvolutionFF(double abscisse, double ordonnee) {
    this.abscisse = abscisse;
    this.ordonnee = ordonnee;
  }

  public PtEvolutionFF(Point2D pt) {
    this.abscisse = pt.getAbscisse();
    this.ordonnee = pt.getOrdonnee();
  }

  public PtEvolutionFF() {
  }

  public PtEvolutionFF copy() {
    return new PtEvolutionFF(abscisse, ordonnee);
  }

  public String toString(CrueConfigMetier props, String abs, String ord) {
    return TransformerEMHHelper.formatFromPropertyName(abs, abscisse, props, DecimalFormatEpsilonEnum.COMPARISON) + "; " + TransformerEMHHelper
        .formatFromPropertyName(ord, ordonnee, props, DecimalFormatEpsilonEnum.COMPARISON);
  }

  public PtEvolutionFF(PtEvolutionFF oldPtEvolutionFF) {
    abscisse = oldPtEvolutionFF.abscisse;
    ordonnee = oldPtEvolutionFF.ordonnee;
  }

  public void setAbscisse(double newX) {
    abscisse = newX;
  }

  public void setOrdonnee(double newY) {
    ordonnee = newY;
  }

  @Override
  public double getOrdonnee() {
    return ordonnee;
  }

  @Override
  public double getAbscisse() {
    return abscisse;
  }
}
