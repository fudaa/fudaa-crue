/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.result;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.metier.OrdResKey;
import org.fudaa.dodico.crue.metier.emh.Dde;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.OrdRes;
import org.fudaa.dodico.crue.metier.emh.OrdResScenario;
import org.fudaa.dodico.crue.metier.helper.EMHCatTypeContent;

/**
 *
 * @author Frederic Deniger
 */
public class OrdResExtractor {

  final OrdResScenario ordRes;

  public OrdResExtractor(OrdResScenario ordRes) {
    this.ordRes = ordRes;
  }

  public List<OrdRes> getFilteredOrdRes(Collection<EMH> emhs) {
    EMHCatTypeContent content = EMHCatTypeContent.create(emhs);
    List<OrdRes> listOfOrdRes = ordRes.getListOfOrdRes();
    ArrayList<OrdRes> res = new ArrayList<>();
    for (OrdRes init : listOfOrdRes) {
      if (content.contain(init.getKey())) {
        res.add(init);
      }
    }
    return res;
  }

  public List<OrdResVariableSelection> getOrdResSelectedVarForEMH(Collection<EMH> selectedEMHs, List<String> variablesFC) {
    return getOrdResSelectedVar(getFilteredOrdRes(selectedEMHs), variablesFC);
  }

  public List<OrdResVariableSelection> getOrdResSelectedVarForEMHorAll(Collection<EMH> selectedEMHs, List<String> variablesFC) {
    if (CollectionUtils.isEmpty(selectedEMHs)) {
      return getOrdResSelectedVarForAll(variablesFC);
    }
    return getOrdResSelectedVar(getFilteredOrdRes(selectedEMHs), variablesFC);
  }

  List<OrdResVariableSelection> getOrdResSelectedVarForAll(List<String> variablesFC) {
    return getOrdResSelectedVar(ordRes.getListOfOrdRes(), variablesFC);
  }

  public OrdResVariableSelection getOrdResSelection(OrdResKey key, List<String> variablesFC) {
    List<OrdRes> listOfOrdRes = ordRes.getListOfOrdRes();
    for (OrdRes res : listOfOrdRes) {
      if (res != null && key.equals(res.getKey())) {
        return createSelection(res, variablesFC);
      }
    }
    if (CollectionUtils.isNotEmpty(variablesFC)) {
      return new OrdResVariableSelection(key, variablesFC, variablesFC);
    }
    return null;
  }

  /**
   *
   * @param enumCatEMH si null renvoie toutes les variables sélectionnée
   * @return
   */
  public List<OrdResVariableSelection> getOrdResSelection(EnumCatEMH enumCatEMH, List<String> variablesFC) {
    List<OrdResVariableSelection> res = new ArrayList<>();
    List<OrdRes> listOfOrdRes = ordRes.getListOfOrdRes();
    for (OrdRes current : listOfOrdRes) {
      if (current != null && (enumCatEMH == null || current.getKey().getCatEMH().equals(enumCatEMH))) {
        res.add(createSelection(current, variablesFC));
      }
    }
    return res;
  }

  /**
   *
   * @param enumCatEMH si null renvoie toutes les variables sélectionnées
   * @return liste triée de toutes les variables sélectionnées par la catégorie.
   */
  public List<String> getSelectableVariables(EnumCatEMH enumCatEMH, List<String> variablesFC) {
    List<OrdResVariableSelection> ordResSelection = getOrdResSelection(enumCatEMH, Collections.emptyList());
    Set<String> variables = new HashSet<>();
    for (OrdResVariableSelection ordResVariableSelection : ordResSelection) {
      variables.addAll(ordResVariableSelection.getSelectableVariables());
    }
    variables.addAll(variablesFC);
    VariableFCVariableNameComparator comparator = new VariableFCVariableNameComparator(variablesFC);
    final ArrayList<String> res = CollectionCrueUtil.getSortedList(variables, comparator);
    return res;
  }

  public List<String> getAllSelectableVariables(List<String> variablesFC) {
    return getSelectableVariables(null, variablesFC);
  }

  List<OrdResVariableSelection> getOrdResSelectedVar(Collection<OrdRes> ordResList, List<String> variablesFC) {
    ArrayList<OrdResVariableSelection> res = new ArrayList<>();
    for (OrdRes ordResItem : ordResList) {
      res.add(createSelection(ordResItem, variablesFC));
    }

    return res;
  }

  public OrdResVariableSelection createSelection(OrdRes ordResItem, List<String> variablesFC) {
    List<String> usableVariables = new ArrayList<>();
    List<Dde> ddes = ordResItem.getDdes();
    for (Dde dde : ddes) {
      if (dde.getValeur()) {
        usableVariables.add(dde.getNom());
      }
    }
    usableVariables.addAll(variablesFC);
    final List<String> allVariables = new ArrayList<>(ordResItem.getAllVariables());
    allVariables.addAll(variablesFC);
    VariableFCVariableNameComparator comparator = new VariableFCVariableNameComparator(variablesFC);
    Collections.sort(usableVariables, comparator);
    Collections.sort(allVariables, comparator);
    final OrdResVariableSelection ordResVariableSelection = new OrdResVariableSelection(ordResItem.getKey(), usableVariables, allVariables);
    return ordResVariableSelection;
  }
}
