/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.result;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.SafeComparator;

/**
 *
 * @author Frederic Deniger
 */
public class VariableFCVariableNameComparator extends SafeComparator<String> {

  private final Set<String> variableFC;

  public VariableFCVariableNameComparator(Collection<String> variableFC) {
    this.variableFC = variableFC == null ? null : new HashSet<>(variableFC);
  }

  public VariableFCVariableNameComparator(Set<String> variableFC) {
    this.variableFC = variableFC;
  }

  @Override
  protected int compareSafe(String o1, String o2) {
    return compareComparable(getString(o1), getString(o2));
  }

  private String getString(String init) {
    if (variableFC != null && variableFC.contains(init)) {
      return init;
    }
    return StringUtils.capitalize(init);

  }
}
