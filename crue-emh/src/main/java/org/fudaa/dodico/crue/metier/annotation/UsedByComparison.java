package org.fudaa.dodico.crue.metier.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface UsedByComparison {
  
   /** true if must be ignore by comparison */
  boolean ignoreInComparison() default false;
}
