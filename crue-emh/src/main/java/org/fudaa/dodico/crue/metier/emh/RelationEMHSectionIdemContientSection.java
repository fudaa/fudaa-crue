/***********************************************************************
 * Module:  RelationEMHCasierDansNoeud.java
 * Author:  deniger
 * Purpose: Defines the Class RelationEMHCasierDansNoeud
 ***********************************************************************/

package org.fudaa.dodico.crue.metier.emh;

public class RelationEMHSectionIdemContientSection extends RelationEMH<EMHSectionIdem> {

  @Override
  public RelationEMHSectionIdemContientSection copyWithEMH(EMHSectionIdem copiedEMH) {
    RelationEMHSectionIdemContientSection res = new RelationEMHSectionIdemContientSection();
    res.setEmh(copiedEMH);
    return res;
  }
}
