package org.fudaa.dodico.crue.metier.transformer;

import gnu.trove.TIntObjectHashMap;
import java.text.NumberFormat;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.metier.emh.Point2D;

public class ToStringTransformerPoint2d implements ToStringTransformer {

  private final NumberFormat xFmt;
  private final NumberFormat yFmt;
  final boolean absIsEnum;
  TIntObjectHashMap<ItemEnum> absValueByInteger;
  final boolean ordIsEnum;
  TIntObjectHashMap<ItemEnum> ordValueByInteger;

  public ToStringTransformerPoint2d(String xName, String yName, CrueConfigMetier props, DecimalFormatEpsilonEnum type) {
    this.xFmt = props.getFormatter(xName, type);
    this.yFmt = props.getFormatter(yName, type);
    absIsEnum = props.getProperty(xName).getNature().isEnum();
    if (absIsEnum) {
      absValueByInteger = props.getProperty(xName).getNature().getItemEnumByValue();
    }
    ordIsEnum = props.getProperty(yName).getNature().isEnum();
    if (ordIsEnum) {
      ordValueByInteger = props.getProperty(yName).getNature().getItemEnumByValue();
    }
    if (!absIsEnum && xFmt == null) {
      throw new IllegalArgumentException("xFmt is null");
    }
    if (!ordIsEnum && yFmt == null) {
      throw new IllegalArgumentException("yFmt is null");
    }

  }

  @Override
  public String transform(Object in) {
    Point2D p2d = (Point2D) in;
    final double abscisse = p2d.getAbscisse();
    final double ordonnee = p2d.getOrdonnee();
    return "(" + formatX(abscisse) + "; " + formatY(ordonnee) + ")";
  }

  public String formatY(final double ordonnee) {
    if (ordIsEnum) {
      ItemEnum itemEnum = ordValueByInteger.get((int) ordonnee);
      if (itemEnum != null) {
        return itemEnum.getName();
      }
      return Integer.toString((int) ordonnee);
    }
    return yFmt.format(ordonnee);
  }

  public String formatX(final double abscisse) {
    if (absIsEnum) {
      ItemEnum itemEnum = absValueByInteger.get((int) abscisse);
      if (itemEnum != null) {
        return itemEnum.getName();
      }
      return Integer.toString((int) abscisse);
    }
    return xFmt.format(abscisse);
  }
}
