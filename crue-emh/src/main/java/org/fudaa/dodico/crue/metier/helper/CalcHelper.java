/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.OrdCalc;

/**
 *
 * @author Frederic Deniger
 */
public class CalcHelper {

  /**
   * pour un calcul, donne la liste des DCLM par nom d'EMH ( la classe affectée par emh)
   *
   * @param calc
   * @return
   */
  public static Map<String, List<Class>> getDCLMActiveTypeByEMHName(Calc calc) {
    List<DonCLimM> listeDCLMUserActive = calc.getlisteDCLMUserActive();
    Map<String, List<Class>> res = new HashMap<>();
    for (DonCLimM dclm : listeDCLMUserActive) {
      String nom = dclm.getEmh().getNom();
      List<Class> list = res.get(nom);
      if (list == null) {
        list = new ArrayList<>();
        res.put(nom, list);
      }
      list.add(dclm.getClass());
    }
    return res;
  }

  /**
   *
   * @param calc
   * @return liste des EMH/type de dclm pour lequel l'EMH est definie plusieurs fois.
   */
  public static List<Pair<String, Class>> getDCLMActiveWithDoublons(Calc calc) {
    Map<String, List<Class>> dclmActiveTypeByEMHName = getDCLMActiveTypeByEMHName(calc);
    List<Pair<String, Class>> res = new ArrayList<>();
    for (Map.Entry<String, List<Class>> entry : dclmActiveTypeByEMHName.entrySet()) {
      String emhNom = entry.getKey();
      List<Class> list = entry.getValue();
      Map cardinalityMap = CollectionUtils.getCardinalityMap(list);
      for (Object object : cardinalityMap.entrySet()) {
        Class dclm = (Class) ((Map.Entry) object).getKey();
        Integer nb = (Integer) ((Map.Entry) object).getValue();
        if (nb.intValue() > 1) {
          res.add(new Pair<>(emhNom, dclm));
        }
      }
    }

    return res;
  }

  public static Map<Calc, OrdCalc> getOrdCalcByCalc(Collection<OrdCalc> ordCalc) {
    Map<Calc, OrdCalc> res = new HashMap<>();
    for (OrdCalc o : ordCalc) {
      res.put(o.getCalc(), o);
    }
    return res;
  }

    public static final String getSuffixe(DonCLimM dclm, CrueConfigMetier ccm) {
        PropertyFinder finder = new PropertyFinder();
        List<PropertyFinder.DescriptionIndexed> availablePropertiesSorted = finder.getAvailablePropertiesNotSorted(dclm.getClass());
        for (PropertyFinder.DescriptionIndexed descriptionIndexed : availablePropertiesSorted) {
            Class c = descriptionIndexed.description.propertyClass;
            if (Double.TYPE.equals(c)) {
                ItemVariable property = ccm.getProperty(descriptionIndexed.description.property);
                return descriptionIndexed.description.propertyDisplay + PropertyNature.getUniteSuffixe(property);
            } else if (Loi.class.isAssignableFrom(c)) {
                return StringUtils.capitalize(descriptionIndexed.description.propertyDisplay);
            }
        }
        return dclm.getTypei18n();
    }
}
