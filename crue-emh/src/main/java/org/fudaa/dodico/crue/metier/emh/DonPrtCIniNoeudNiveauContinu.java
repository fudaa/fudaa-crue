package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

public final class DonPrtCIniNoeudNiveauContinu extends DonPrtCIni<DonPrtCIniNoeudNiveauContinu> {

  private double zini;

  /**
   * @param def
   */
  public DonPrtCIniNoeudNiveauContinu(CrueConfigMetier def) {
    zini = def.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_ZINI);
  }

  @Override
  public void initFrom(DonPrtCIniNoeudNiveauContinu dpti) {
    this.zini = dpti.zini;
  }

  @PropertyDesc(i18n = "zini.property")
  public final double getZini() {
    return zini;
  }

  /**
   * @param newZini
   */
  public final void setZini(double newZini) {
    zini = newZini;
  }
}
