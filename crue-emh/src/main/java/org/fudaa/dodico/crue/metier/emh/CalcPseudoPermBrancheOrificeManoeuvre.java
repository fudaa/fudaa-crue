package org.fudaa.dodico.crue.metier.emh;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

/**
 * CL de type 41 ou 42 en Crue9
 *
 */
public class CalcPseudoPermBrancheOrificeManoeuvre extends DonCLimMCommonItem implements CalcPseudoPermItem, DonCLimMWithSensOuv {

  public static final String PROP_SENS_OUV = "sensOuv";
  private double ouv;
  //attention modifier la constante di-dessus si modification de nom
  private EnumSensOuv sensOuv;

  public CalcPseudoPermBrancheOrificeManoeuvre(final CrueConfigMetier defaults) {
    ouv = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_OUV);
    final String enumValue = defaults.getDefaultStringEnumValue("sensOuv");
    try {
      sensOuv = enumValue == null ? null : EnumSensOuv.valueOf(enumValue);
    } catch (final Exception e) {
    }
  }

  @PropertyDesc(i18n = "ouv.property")
  public double getOuv() {
    return ouv;
  }

  @Override
  public String getCcmVariableName() {
    return CrueConfigMetierConstants.PROP_OUV;
  }

  @Override
  public String getShortName() {
    return BusinessMessages.getString("ouv.property");
  }

  /**
   * @param newOuv
   */
  public void setOuv(final double newOuv) {
    ouv = newOuv;
  }

  @PropertyDesc(i18n = "sensOuv.property")
  public EnumSensOuv getSensOuv() {
    return sensOuv;
  }

  /**
   * @param newSensOuv nouveau sens
   */
  public void setSensOuv(final EnumSensOuv newSensOuv) {
    sensOuv = newSensOuv;
  }

  /**
   * Fonction générique permettant de récupérer les données d'un CalPseudoPerm la valeur
   *
   */
  @Visibility(ihm = false)
  @Override
  public double getValue() {
    return getOuv();
  }

  /**
   * @param newVal nouvelle valeur
   */
  @Override
  public void setValue(final double newVal) {
    setOuv(newVal);
  }

  CalcPseudoPermBrancheOrificeManoeuvre deepClone() {
    try {
      return (CalcPseudoPermBrancheOrificeManoeuvre) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransNoeudNiveauContinuTarage.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }
}
