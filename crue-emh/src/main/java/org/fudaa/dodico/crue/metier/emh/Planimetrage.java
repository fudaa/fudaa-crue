/**
 * *********************************************************************
 * Module: Planimetrage.java Author: battista Purpose: Defines the Class Planimetrage
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

public abstract class Planimetrage {

  public abstract Planimetrage deepClone();
}
