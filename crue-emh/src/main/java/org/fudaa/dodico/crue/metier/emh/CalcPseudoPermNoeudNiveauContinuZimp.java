package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * CL de type 1 en Crue9
 *
 */
public class CalcPseudoPermNoeudNiveauContinuZimp extends DonCLimMCommonItem implements CalcPseudoPermItem {
  /**
   */
  private double zimp;

  public CalcPseudoPermNoeudNiveauContinuZimp(final CrueConfigMetier def) {
    zimp = def.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_ZIMPOSE);
  }

  @PropertyDesc(i18n = "zimp.property")
  public double getZimp() {
    return zimp;
  }

  @Override
  public String getShortName() {
    return BusinessMessages.getString("zimp.property");
  }

  @Override
  public String getCcmVariableName() {
    return CrueConfigMetierConstants.PROP_ZIMPOSE;
  }

  /**
   * @param newZimp
   */
  public void setZimp(final double newZimp) {
    zimp = newZimp;
  }

  /**
   * Fonction générique permettant de récupérer les données d'un CalPseudoPerm la valeur
   */
  @Visibility(ihm = false)
  @Override
  public double getValue() {
    return getZimp();
  }

  /**
   * @param newVal
   */
  @Override
  public void setValue(final double newVal) {
    setZimp(newVal);
  }

  CalcPseudoPermNoeudNiveauContinuZimp deepClone() {
    try {
      return (CalcPseudoPermNoeudNiveauContinuZimp) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransNoeudNiveauContinuTarage.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }
}
