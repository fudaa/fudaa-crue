/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.comparator;

import gnu.trove.TObjectIntHashMap;
import java.util.List;
import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;

/**
 *
 * @author Frederic Deniger
 */
public class CasierPositionComparator extends SafeComparator<CatEMHCasier> {

  final TObjectIntHashMap<String> noeudPosition = new TObjectIntHashMap<>();

  public CasierPositionComparator(EMHSousModele sousModele) {
    List<CatEMHNoeud> noeuds = sousModele.getNoeuds();
    for (int i = 0; i < noeuds.size(); i++) {
      CatEMHNoeud catEMHNoeud = noeuds.get(i);
      noeudPosition.put(catEMHNoeud.getNom(), i);
    }
  }

  @Override
  protected int compareSafe(CatEMHCasier o1, CatEMHCasier o2) {
    CatEMHNoeud nd1 = o1.getNoeud();
    CatEMHNoeud nd2 = o2.getNoeud();
    if (nd1 == null && nd2 == null) {
      return compareString(o1.getNom(), o2.getNom());
    }
    if (nd1 == null) {
      return 1;
    }
    if (nd2 == null) {
      return -1;
    }
    int pos1 = noeudPosition.get(nd1.getNom());
    int pos2 = noeudPosition.get(nd2.getNom());
    if (pos1 == pos2) {
      return compareString(o1.getNom(), o2.getNom());
    }
    return pos1 - pos2;
  }
}
