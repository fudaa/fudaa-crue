/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.crue.metier.emh;

/**
 * Relation qui permet de connaitre la branche contenant une section pilote
 * 
 * @author deniger
 */
public class RelationEMHBrancheContientSectionPilote extends RelationEMH<CatEMHBranche> {

  /**
   * @param br
   */
  public RelationEMHBrancheContientSectionPilote(CatEMHBranche br) {
    super.setEmh(br);
  }

  @Override
  public RelationEMH<CatEMHBranche> copyWithEMH(CatEMHBranche copiedEMH) {
    return new RelationEMHBrancheContientSectionPilote(copiedEMH);
  }
}
