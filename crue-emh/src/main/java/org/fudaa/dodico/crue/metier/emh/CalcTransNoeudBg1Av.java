package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.BusinessMessages;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * CL de type 2 en Crue9
 */
public class CalcTransNoeudBg1Av extends DonCLimMCommonItem  {


  @Override
  public String getShortName() {
    return BusinessMessages.getString("bg1av.property");
  }

  CalcTransNoeudBg1Av deepClone() {
    try {
      return (CalcTransNoeudBg1Av) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransNoeudBg1Av.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }
}
