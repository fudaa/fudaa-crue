package org.fudaa.dodico.crue.metier.aoc;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

/**
 * Created by deniger on 28/06/2017.
 */
public enum EnumAocTypeCalageCritere implements ToStringInternationalizable {

        ERREUR_QUADRATIQUE("ErreurQuadratique",""), ECART_NIVEAUX_MAX("EcartNiveauxMax","NiveauMax"), ECART_TEMPS_ARRIVEE_MAX("EcartTempsArriveeMax","TempsArriveeMax"), ECART_VOLUMES("EcartVolumes","Volume");

    private final String identifiant;
    private final String nomVariable;

    EnumAocTypeCalageCritere(String identifiant,String nomVariable) {
        this.identifiant = identifiant;
        this.nomVariable = nomVariable;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public String getNomVariable() {
        return nomVariable;
    }

    @Override
    public String geti18n() {
        return BusinessMessages.getString("aoc.typeCalageCritere." + name().toLowerCase() + ".shortName");
    }

    @Override
    public String geti18nLongName() {
        return BusinessMessages.getString("aoc.typeCalageCritere." + name().toLowerCase() + ".longName");
    }
}
