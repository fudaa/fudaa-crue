package org.fudaa.dodico.crue.metier.emh;

import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

public class DonPrtGeoProfilCasier extends DonPrtGeoNomme implements Cloneable {

  private double distance;

  public DonPrtGeoProfilCasier(CrueConfigMetier def) {
    distance = def.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_DISTANCE);
  }
  private java.util.List<PtProfil> ptProfil;
  private java.util.List<PtProfil> ptProfilExtern;
  private LitUtile litUtile;

  /**
   * @pdGenerated default getter
   */
  public java.util.List<PtProfil> getPtProfil() {
    if (ptProfil == null) {
      ptProfil = new java.util.ArrayList<>();
    }
    if (ptProfilExtern == null) {
      ptProfilExtern = Collections.unmodifiableList(ptProfil);
    }
    return ptProfilExtern;
  }

  public void initFrom(DonPrtGeoProfilCasier other) {
    if (other == null || other == this) {
      return;
    }
    setCommentaire(other.getCommentaire());
    setNom(other.getNom());
    setDistance(other.getDistance());
    setLitUtile(other.getLitUtile().clone());
    setPtProfil(other.getPtProfil());
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  @Override
  public DonPrtGeo cloneDPTG() {
    return cloneProfilCasier();
  }

  public DonPrtGeoProfilCasier cloneProfilCasier() {
    try {
      DonPrtGeoProfilCasier cloned = (DonPrtGeoProfilCasier) clone();
      if (ptProfil != null) {
        cloned.ptProfil = new ArrayList<>();
        cloned.ptProfilExtern = Collections.unmodifiableList(cloned.ptProfil);
        for (PtProfil ptProfilThis : ptProfil) {
          cloned.ptProfil.add(ptProfilThis.clone());
        }
      }
      if (litUtile != null) {
        cloned.litUtile = litUtile.clone();
      }
      return cloned;
    } catch (CloneNotSupportedException ex) {
      //why ?
      Logger.getLogger(DonPrtGeoProfilCasier.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("why ?");
  }

  @UsedByComparison
  @Visibility(ihm = false)
  public int getPtProfilSize() {
    return ptProfil == null ? 0 : ptProfil.size();
  }

  /**
   * @pdGenerated default setter
   *
   * @param newPtProfil
   */
  public void setPtProfil(final java.util.List<PtProfil> newPtProfil) {
    removeAllPtProfil();
    for (final java.util.Iterator iter = newPtProfil.iterator(); iter.hasNext();) {
      addPtProfil((PtProfil) iter.next());
    }
  }

  /**
   * @pdGenerated default add
   *
   * @param newPtProfil
   */
  public void addPtProfil(final PtProfil newPtProfil) {
    if (newPtProfil == null) {
      return;
    }
    if (this.ptProfil == null) {
      this.ptProfil = new java.util.ArrayList<>();
    }
    this.ptProfil.add(newPtProfil.clone());
  }

  /**
   * @pdGenerated default remove
   *
   * @param oldPtProfil
   */
  public void removePtProfil(final PtProfil oldPtProfil) {
    if (oldPtProfil == null) {
      return;
    }
    if (this.ptProfil != null) {
      if (this.ptProfil.contains(oldPtProfil)) {
        this.ptProfil.remove(oldPtProfil);
      }
    }
  }

  /**
   * @pdGenerated default removeAll
   */
  public void removeAllPtProfil() {
    if (ptProfil != null) {
      ptProfil.clear();
    }
  }

  public LitUtile getLitUtile() {
    return litUtile;
  }

  /**
   *
   * @param newLitUtile
   */
  public void setLitUtile(final LitUtile newLitUtile) {
    this.litUtile = newLitUtile;
  }

  public double getDistance() {
    return distance;
  }

  /**
   * @param newLongueur
   */
  public void setDistance(final double newLongueur) {
    distance = newLongueur;
  }
}
