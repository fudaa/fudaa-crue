/**
 * *********************************************************************
 * Module: PdtVar.java Author: deniger Purpose: Defines the Class PdtVar *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;

import java.util.ArrayList;

public class PdtVar extends Pdt implements ToStringTransformable {
  private java.util.List<ElemPdt> elemPdt;

  public java.util.List<ElemPdt> getElemPdt() {
    if (elemPdt == null) {
      elemPdt = new java.util.ArrayList<>();
    }
    return elemPdt;
  }

  @Override
  public PdtVar deepClone() {
    PdtVar res = new PdtVar();
    if (elemPdt != null) {
      res.elemPdt = new ArrayList<>();
      for (ElemPdt elem : elemPdt) {
        res.elemPdt.add(elem.deepClone());
      }
    }
    return res;
  }

  public java.util.Iterator getIteratorElemPdt() {
    if (elemPdt == null) {
      elemPdt = new java.util.ArrayList<>();
    }
    return elemPdt.iterator();
  }

  public void setElemPdt(java.util.List<ElemPdt> newElemPdt) {
    removeAllElemPdt();
    for (java.util.Iterator iter = newElemPdt.iterator(); iter.hasNext(); ) {
      addElemPdt((ElemPdt) iter.next());
    }
  }

  public void addElemPdt(ElemPdt newElemPdt) {
    if (newElemPdt == null) {
      return;
    }
    if (this.elemPdt == null) {
      this.elemPdt = new java.util.ArrayList<>();
    }
    if (!this.elemPdt.contains(newElemPdt)) {
      this.elemPdt.add(newElemPdt);
    }
  }

  public void removeElemPdt(ElemPdt oldElemPdt) {
    if (oldElemPdt == null) {
      return;
    }
    if (this.elemPdt != null) {
      if (this.elemPdt.contains(oldElemPdt)) {
        this.elemPdt.remove(oldElemPdt);
      }
    }
  }

  public void removeAllElemPdt() {
    if (elemPdt != null) {
      elemPdt.clear();
    }
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    return "PdtVar [ " + getNbElemPdt() + " ElemPdt]";
  }

  @Override
  public String toString() {
    return toString(null, EnumToString.COMPLETE, DecimalFormatEpsilonEnum.COMPARISON);
  }

  private int getNbElemPdt() {
    return elemPdt == null ? 0 : elemPdt.size();
  }
}
