package org.fudaa.dodico.crue.metier.emh;

import java.util.Collection;

/**
 * type 5 Crue9
 *
 */
public class DonCalcSansPrtBrancheOrifice extends DonCalcSansPrt {

  private ElemOrifice elemOrifice;

  public ElemOrifice getElemOrifice() {
    return elemOrifice;
  }

  @Override
  public void fillWithLoi(final Collection<Loi> target) {
  }

  @Override
  public DonCalcSansPrt cloneDCSP() {
    try {
      DonCalcSansPrtBrancheOrifice res = (DonCalcSansPrtBrancheOrifice) super.clone();
      if (elemOrifice != null) {
        res.setElemOrifice(elemOrifice.clone());
      }
      return res;
    } catch (CloneNotSupportedException e) {
    }
    return null;
  }

  public void setElemOrifice(final ElemOrifice newElemOrifice) {
    this.elemOrifice = newElemOrifice;
  }
}
