/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.DonCLimMScenario;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.OrdCalc;
import org.fudaa.dodico.crue.metier.emh.OrdCalcScenario;
import org.fudaa.dodico.crue.metier.emh.ui.CalcOrdCalcUiState;
import org.fudaa.dodico.crue.metier.emh.ui.OrdCalcScenarioUiState;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

/**
 *
 * @author Frederic Deniger
 */
public class OrdCalcCloner {

  public static class Result {

    /**
     * contient les ordres de calcul definis dans le scenario
     */
    public final List<OrdCalc> ordCalcs = new ArrayList<>();
    /**
     * contient les ordCalc definis dans le fichier ui ocal persistant les editions de l'utilisateur
     */
    public final List<OrdCalc> ordCalcsInUI = new ArrayList<>();
    /**
     * les calculs dans l'ordre attendu.
     */
    public final List<Calc> calcs = new ArrayList<>();
  }

  public Result cloneAndSort(EMHScenario scenario, ManagerEMHScenario managerEMHScenario) {
    return cloneOrdCalcAndCalc(scenario.getDonCLimMScenario(), scenario.getOrdCalcScenario(), managerEMHScenario.getUiOCalData());
  }

  public Result cloneOrdCalcAndCalc(DonCLimMScenario dclm, OrdCalcScenario ocal, OrdCalcScenarioUiState uiOCalData) {
    List<Calc> calcs = dclm.getCalc();
    List<Calc> clonedCalcs = new ArrayList<>();
    for (Calc calc : calcs) {
      clonedCalcs.add(calc.deepClone());
    }
    Result res = null;
    if (uiOCalData == null) {
      res = sortCalcWithNoSavedUi(ocal, clonedCalcs);
    } else {
      res = sortCalcWithSavedUi(ocal, clonedCalcs, uiOCalData);
    }
    return res;

  }

  protected Result sortCalcWithNoSavedUi(OrdCalcScenario ocal, List<Calc> clonedCalcs) {
    Result res = new Result();
    Map<String, Calc> clonedCalcsByName = TransformerHelper.toMapOfNom(clonedCalcs);
    List<OrdCalc> ordCalcs = ocal.getOrdCalc();
    List<Calc> pseudoPermActivated = new ArrayList<>();
    List<Calc> pseudoPermNonActivated = new ArrayList<>();
    List<Calc> transActivated = new ArrayList<>();
    List<Calc> transNonActivated = new ArrayList<>();

    Set<String> namesOfActiveCalcul = new HashSet<>();
    for (OrdCalc ordCalc : ordCalcs) {
      String calcName = ordCalc.getCalc().getNom();
      namesOfActiveCalcul.add(calcName);
      final Calc calc = clonedCalcsByName.get(calcName);
      res.ordCalcs.add(ordCalc.deepCloneButNotCalc(calc));
      if (ordCalc.isPseudoPermanent()) {
        pseudoPermActivated.add(calc);
      } else {
        transActivated.add(calc);
      }
    }
    for (Calc calc : clonedCalcs) {
      if (!namesOfActiveCalcul.contains(calc.getNom())) {
        if (calc.isPermanent()) {
          pseudoPermNonActivated.add(calc);
        } else {
          transNonActivated.add(calc);
        }
      }
    }
    //les non actifs sont triés par ordre alpha:
    Collections.sort(pseudoPermNonActivated, ObjetNommeByNameComparator.INSTANCE);
    Collections.sort(transNonActivated, ObjetNommeByNameComparator.INSTANCE);

    res.calcs.addAll(pseudoPermActivated);
    res.calcs.addAll(pseudoPermNonActivated);
    res.calcs.addAll(transActivated);
    res.calcs.addAll(transNonActivated);
    return res;
  }

  protected Result sortCalcWithSavedUi(OrdCalcScenario ocal, List<Calc> clonedCalcs, OrdCalcScenarioUiState uiOCalData) {
    Result res = new Result();
    Map<String, Calc> clonedCalcsByName = TransformerHelper.toMapOfId(clonedCalcs);
    final List<String> calcNameInOrder = uiOCalData.getCalcNameInOrder();
    boolean isCoherent = isUiSavedCalcAccordingToCalcs(clonedCalcs, calcNameInOrder);
    if (!isCoherent) {
      Logger.getLogger(OrdCalcCloner.class.getName()).log(Level.WARNING, "The ocal ui file is not coherent with the ocal data");
      return sortCalcWithNoSavedUi(ocal, clonedCalcs);
    }
    List<OrdCalc> ordCalcsInScenario = ocal.getOrdCalc();
    Set<String> namesOfActiveCalcul = new HashSet<>();
    for (OrdCalc ordCalc : ordCalcsInScenario) {
      String calcId = ordCalc.getCalc().getId();
      namesOfActiveCalcul.add(calcId);
      final Calc calc = clonedCalcsByName.get(calcId);
      res.ordCalcs.add(ordCalc.deepCloneButNotCalc(calc));
    }
    final List<CalcOrdCalcUiState> uiStates = uiOCalData.getUiStates();
    for (CalcOrdCalcUiState uiState : uiStates) {
      final String calcName = uiState.getCalcId();
      if (!namesOfActiveCalcul.contains(calcName)) {
        final OrdCalc ordCalcSaved = uiState.getOrdCalc();
        final Calc calc = clonedCalcsByName.get(calcName.toUpperCase());
        assert calc != null : "uiOrdCalc: should not be null as the method isUiSavedCalcAccordingToCalcs should have tested it";
        res.ordCalcsInUI.add(ordCalcSaved.deepCloneButNotCalc(calc));
      }
    }
    for (String calcName : calcNameInOrder) {
      final Calc calc = clonedCalcsByName.get(calcName.toUpperCase());
      assert calc != null : "should not be null as the method isUiSavedCalcAccordingToCalcs should have tested it";
      res.calcs.add(calc);
    }
    return res;
  }

  /**
   *
   * @param clonedCalcs la liste des calculs presents dans l'etude
   * @param calcNameInOrder la liste des calculs presents dans le fichier ui-ocal permettant de persister les editions de l'utilisateur
   * @return true si les 2 données sont coherentes et si on peut les utiliser.
   */
  protected boolean isUiSavedCalcAccordingToCalcs(Collection<Calc> clonedCalcs, final Collection<String> calcNameInOrder) {
    //on va d'abord voir si le tout est cohérent:
    if (clonedCalcs.size() == calcNameInOrder.size()) {
      Map<String, Calc> clonedCalcsByName = TransformerHelper.toMapOfId(clonedCalcs);
      for (String string : calcNameInOrder) {
        if (!clonedCalcsByName.containsKey(string.toUpperCase())) {
          return false;
        }
      }
      return true;
    }
    return false;
  }
}
