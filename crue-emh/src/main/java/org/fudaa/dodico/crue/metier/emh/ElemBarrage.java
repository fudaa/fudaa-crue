package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ElemBarrage implements ToStringTransformable, Cloneable {
    public static final String PROP_COEF_NOY = "coefNoy";
    public static final String PROP_COEF_DEN = "coefDen";
    private double largeur;
    private double zseuil;
    private double coefNoy;
    private double coefDen;

    public ElemBarrage(CrueConfigMetier defaults) {
        coefNoy = defaults.getDefaultDoubleValue(PROP_COEF_NOY);
        coefDen = defaults.getDefaultDoubleValue(PROP_COEF_DEN);
        zseuil = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_ZSEUIL);
        largeur = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_LARGEUR);
    }

    public boolean isSameLargeurAndCote(ElemBarrage other, CrueConfigMetier ccm) {
        return
                ccm.getEpsilon(CrueConfigMetierConstants.PROP_LARGEUR).isSame(largeur, other.getLargeur())
                        && ccm.getEpsilon(CrueConfigMetierConstants.PROP_ZSEUIL).isSame(zseuil, other.getZseuil());
    }

    @Override
    public ElemBarrage clone() {
        try {
            return (ElemBarrage) super.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(ElemOrifice.class.getName()).log(Level.SEVERE, null, ex);
        }
        throw new IllegalAccessError("why");
    }

    @PropertyDesc(i18n = "largeur.property")
    public final double getLargeur() {
        return largeur;
    }

    /**
     * @param newLargeur
     */
    public final void setLargeur(double newLargeur) {
        largeur = newLargeur;
    }

    @PropertyDesc(i18n = "zseuil.property")
    public final double getZseuil() {
        return zseuil;
    }

    /**
     * @param newZseuil
     */
    public final void setZseuil(double newZseuil) {
        zseuil = newZseuil;
    }

    @PropertyDesc(i18n = "coefNoy.property")
    public final double getCoefNoy() {
        return coefNoy;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

    public final void setCoefNoy(double newCoefD) {
        coefNoy = newCoefD;
    }

    @Override
    public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
        if (EnumToString.OVERVIEW.equals(format)) {
            return "ElemBarrage";
        }
        return "ElemBarrage [" +
                "coefNoy=" + TransformerEMHHelper.formatFromPropertyName(PROP_COEF_NOY, coefNoy, props, DecimalFormatEpsilonEnum.COMPARISON)
                + "coefDen=" + TransformerEMHHelper.formatFromPropertyName(PROP_COEF_DEN, coefDen, props, DecimalFormatEpsilonEnum.COMPARISON)
                + ", largeur=" + TransformerEMHHelper.formatFromPropertyName(CrueConfigMetierConstants.PROP_LARGEUR, largeur, props, DecimalFormatEpsilonEnum.COMPARISON)
                + ", zseuil=" + TransformerEMHHelper.formatFromPropertyName(CrueConfigMetierConstants.PROP_ZSEUIL, zseuil, props, DecimalFormatEpsilonEnum.COMPARISON) + "]";
    }

    @PropertyDesc(i18n = "coefDen.property")
    public double getCoefDen() {
        return coefDen;
    }

    public void setCoefDen(double coefDen) {
        this.coefDen = coefDen;
    }
}
