package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 * Section de calcul positionnée sur un profil géométrique é lits multiples
 * 
 * @author Adrien Hadoux
 */
public class EMHSectionProfil extends CatEMHSection {

  public EMHSectionProfil(final String nom) {
    super(nom);
  }
  
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public EnumSectionType getSectionType() {
    return EnumSectionType.EMHSectionProfil;
  }

  @Override
  @Visibility(ihm = false)
  public EMHSectionProfil copyShallowFirstLevel() {
    return copyPrimitiveIn(new EMHSectionProfil(getNom()));
  }
}
