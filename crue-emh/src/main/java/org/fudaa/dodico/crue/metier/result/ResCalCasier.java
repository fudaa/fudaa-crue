package org.fudaa.dodico.crue.metier.result;

/**
 * @author deniger
 */
public class ResCalCasier extends ResCalcul {

  /**
   * @param qech the qech to set
   */
  public void setQech(double qech) {
    super.addResultat("qech", qech);
  }

  public void setSplan(double sPlan) {
    super.addResultat("splan", sPlan);
  }

  public void setVol(double vol) {
    super.addResultat("vol", vol);
  }
}
