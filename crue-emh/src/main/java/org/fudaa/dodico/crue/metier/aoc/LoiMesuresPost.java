package org.fudaa.dodico.crue.metier.aoc;

import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;
import org.fudaa.dodico.crue.metier.emh.LoiDF;
import org.fudaa.dodico.crue.metier.emh.LoiTF;

import java.util.*;

/**
 * Created by deniger on 13/07/2017.
 */
public class LoiMesuresPost {
  private List<AbstractLoi> lois = new ArrayList<>();
  private final Map<String, AbstractLoi> loiByName = new HashMap<>();
  private AocEchellesSections echellesSections = new AocEchellesSections();

  public AocEchellesSections getEchellesSections() {
    return echellesSections;
  }

  public void setEchellesSections(AocEchellesSections echellesSections) {
    this.echellesSections = echellesSections;
  }

  public List<AbstractLoi> getLois() {
    return lois;
  }

  public void setLois(List<AbstractLoi> lois) {
    if (lois == null) {
      this.lois = new ArrayList<>();
    }
    this.lois = new ArrayList<>(lois);
    updateLoiByNameMap();
  }

  private void updateLoiByNameMap() {
    loiByName.clear();
    this.lois.forEach(loi -> loiByName.put(loi.getNom(), loi));
  }

  public void addLoiAndSort(AbstractLoi currentLoi) {
    lois.add(currentLoi);
    loiByName.put(currentLoi.getNom(), currentLoi);
    Collections.sort(lois, ObjetNommeByNameComparator.INSTANCE);
  }

  public boolean removeLoi(AbstractLoi currentLoi) {
    loiByName.remove(currentLoi.getNom());
    return lois.remove(currentLoi);
  }

  /**
   *
   * @param name le nom de la loi
   * @return null si non trouvée
   */
  public LoiTF getLoiSectionZ(String name) {
    AbstractLoi loi = loiByName.get(name);
    if (EnumTypeLoi.LoiSectionsZ.equals(loi.getType())) {
      return (LoiTF) loi;
    }
    return null;
  }

  /**
   *
   * @param name le nom de la loi
   * @return null si non trouvée
   */
  public LoiDF getLoiDF(String name) {
    AbstractLoi loi = loiByName.get(name);
    if (EnumTypeLoi.LoiTQ.equals(loi.getType())||EnumTypeLoi.LoiTZ.equals(loi.getType())) {
      return (LoiDF) loi;
    }
    return null;
  }

  public void loiUpdated() {
    updateLoiByNameMap();
  }
}
