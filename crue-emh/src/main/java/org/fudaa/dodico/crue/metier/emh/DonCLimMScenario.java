package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.comparator.InfoEMHByUserPositionComparator;

import java.util.ArrayList;
import java.util.List;

public class DonCLimMScenario extends DonCLimMCommon implements Sortable {
  private java.util.List<CalcPseudoPerm> calcPseudoPerm;
  private java.util.List<CalcTrans> calcTrans;

  public java.util.List<CalcPseudoPerm> getCalcPseudoPerm() {
    if (calcPseudoPerm == null) {
      calcPseudoPerm = new java.util.ArrayList<>();
    }
    return calcPseudoPerm;
  }

  @Override
  public boolean sort() {
    boolean res = false;
    final EMHScenario scenario = (EMHScenario) getEmh();
    final InfoEMHByUserPositionComparator comparator = new InfoEMHByUserPositionComparator(scenario);
    if (calcPseudoPerm != null) {
      for (final CalcPseudoPerm pseudoPerm : calcPseudoPerm) {
        res |= pseudoPerm.sort(comparator);
      }
    }
    if (calcTrans != null) {
      for (final CalcTrans calc : calcTrans) {
        res |= calc.sort(comparator);
      }
    }
    return false;
  }

  public void removeEMHFromCalcul(final EMH emh) {
    if (calcPseudoPerm != null) {
      for (final CalcPseudoPerm pseudoPerm : calcPseudoPerm) {
        pseudoPerm.remove(emh);
      }
    }
    if (calcTrans != null) {
      for (final CalcTrans calc : calcTrans) {
        calc.remove(emh);
      }
    }
  }

  @Override
  public EnumInfosEMH getCatType() {
    return EnumInfosEMH.DON_CLIM_SCENARIO;
  }

  @UsedByComparison
  public java.util.List<Calc> getCalc() {
    final List<Calc> res = new ArrayList<>();
    res.addAll(getCalcPseudoPerm());
    res.addAll(getCalcTrans());
    return res;
  }

  public void setCalcPseudoPerm(final java.util.List<CalcPseudoPerm> newCalcPseudoPerm) {
    removeAllCalcPseudoPerm();
    for (CalcPseudoPerm pseudoPerm : newCalcPseudoPerm) {
      addCalcPseudoPerm(pseudoPerm);
    }
  }

  public void addCalcPseudoPerm(final CalcPseudoPerm newCalcPseudoPerm) {
    if (newCalcPseudoPerm == null) {
      return;
    }
    if (this.calcPseudoPerm == null) {
      this.calcPseudoPerm = new java.util.ArrayList<>();
    }
    if (!this.calcPseudoPerm.contains(newCalcPseudoPerm)) {
      this.calcPseudoPerm.add(newCalcPseudoPerm);
    }
  }

  public void removeAllCalcPseudoPerm() {
    if (calcPseudoPerm != null) {
      calcPseudoPerm.clear();
    }
  }

  public java.util.List<CalcTrans> getCalcTrans() {
    if (calcTrans == null) {
      calcTrans = new java.util.ArrayList<>();
    }
    return calcTrans;
  }


  public void setCalcTrans(final java.util.List<CalcTrans> newCalcTrans) {
    removeAllCalcTrans();
    for (CalcTrans newCalcTran : newCalcTrans) {
      addCalcTrans(newCalcTran);
    }
  }

  public void addCalcTrans(final CalcTrans newCalcTrans) {
    if (newCalcTrans == null) {
      return;
    }
    if (this.calcTrans == null) {
      this.calcTrans = new java.util.ArrayList<>();
    }
    if (!this.calcTrans.contains(newCalcTrans)) {
      this.calcTrans.add(newCalcTrans);
    }
  }


  /**
   * vide la liste des calc trans et pour chacun enlève la loi.
   */
  public void removeAllCalcTrans() {
    if (calcTrans != null) {
      for (final CalcTrans cal : calcTrans) {
        for (final DonCLimM dclm : cal.getlisteDCLM()) {
          if (dclm instanceof CalcTransItem) {
            ((CalcTransItem) dclm).setLoi(null);
          }
        }
      }
      calcTrans.clear();
    }
  }

  public void addCalc(final Calc calc) {
    if (calc.isPermanent()) {
      addCalcPseudoPerm((CalcPseudoPerm) calc);
    } else {
      addCalcTrans((CalcTrans) calc);
    }
  }
}
