/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.crue.metier.emh;

/**
 * Relation qui permet de connaitre la branche contenant un noeud. Est-ce utile ou la relation RelationEMHContient
 * suffit ?
 * 
 * @author deniger
 */
public class RelationEMHBrancheContientNoeud extends RelationEMH<CatEMHBranche> {

  /**
   * @param br
   */
  public RelationEMHBrancheContientNoeud(CatEMHBranche br) {
    super.setEmh(br);
  }

  @Override
  public RelationEMH<CatEMHBranche> copyWithEMH(CatEMHBranche copiedEMH) {
    return new RelationEMHBrancheContientNoeud(copiedEMH);
  }
}
