package org.fudaa.dodico.crue.metier.emh;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;

/**
 * règles définies dans OPTG.
 *
 * @author Adrien Hadoux
 */
public class Regle implements ObjetNomme, Cloneable {

  public static final String PROP_ACTIVE = "active";
  private String nom;
  //si modification de cette propriété, il faut modifier PROP_ACTIVE 
  private boolean active = true;
  /**
   * le type de la regle.
   */
  private final EnumRegle type;

  protected Regle(EnumRegle type) {
    super();
    this.type = type;
    setNom(computeNom(type));
  }

  public static String computeNom(EnumRegle type) {
    String[] part = StringUtils.split(type.toString(), '_');
    for (int i = 0; i < part.length; i++) {
      part[i] = StringUtils.capitalize(part[i].toLowerCase());
    }
    return StringUtils.join(part);
  }
  ValParam param;

  public boolean isActive() {
    return active;
  }

  public void setActive(final boolean active) {
    this.active = active;
  }

  @UsedByComparison
  public ValParam getValParam() {
    return param;
  }

  public void setSeuilDetect(final double seuilDetect) {
    this.param = new ValParamDouble(getValParamNom(), seuilDetect);
  }
  String valParamNom;

  public void setValParamNom(String valParamNom) {
    this.valParamNom = valParamNom;
    if (param != null) {
      param.setNom(valParamNom);
    }
  }

  public String getValParamNom() {
    if (valParamNom != null) {
      return valParamNom;
    }
    if (type.isOptr()) {
      return getNom();
    }
    return CruePrefix.P_VAL_PARAM + getNom();
  }

  public double getSeuilDetect() {
    return param instanceof ValParamDouble ? ((ValParamDouble) param).getValeur() : 0;
  }

  public void setValParam(final ValParam seuilDetect) {
    this.param = seuilDetect;
  }

  @UsedByComparison
  public EnumRegle getType() {
    return type;
  }

  @Override
  public String getNom() {
    return nom;
  }
  private String id;

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setNom(final String nom) {
    this.nom = nom;
    if (nom != null) {
      id = nom.toUpperCase();
    }
  }

  @Override
  protected Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  public Regle deepClone() {
    try {
      Regle clone = (Regle) clone();
      if (param != null) {
        clone.param = param.deepClone();
      }
      return clone;
    } catch (CloneNotSupportedException cloneNotSupportedException) {
      Logger.getLogger(Regle.class.getName()).log(Level.SEVERE, "message {0}", cloneNotSupportedException);
    }
    return null;

  }
}
