/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.factory;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPerm;
import org.fudaa.dodico.crue.metier.emh.CalcTrans;
import org.fudaa.dodico.crue.metier.emh.OrdCalc;
import org.fudaa.dodico.crue.metier.emh.OrdCalcPseudoPermIniCalcCI;
import org.fudaa.dodico.crue.metier.emh.OrdCalcPseudoPermIniCalcPrecedent;
import org.fudaa.dodico.crue.metier.emh.OrdCalcPseudoPermIniCliche;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTransIniCalcCI;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTransIniCalcCliche;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTransIniCalcPrecedent;

/**
 *
 * @author Frederic Deniger
 */
public class OrdCalcFactory {

  public static Map<String, OrdCalcCreator> createMapPseudoPermanent() {
    Map<String, OrdCalcCreator> res = new LinkedHashMap<>();
    res.put(BusinessMessages.getString("List.OrdCalcPseudoPermIniCalcCI.shortName"), new OrdCalcPseudoPermIniCalcCICreator());
    res.put(BusinessMessages.getString("List.OrdCalcPseudoPermIniCalcPrecedent.shortName"), new OrdCalcPseudoPermIniCalcPrecedentCreator());
    res.put(BusinessMessages.getString("List.OrdCalcPseudoPermIniClicheCreator.shortName"), new OrdCalcPseudoPermIniClicheCreator());
    return res;
  }

  public static Map<String, OrdCalcCreator> createMapTransitoire() {
    Map<String, OrdCalcCreator> res = new LinkedHashMap<>();
    res.put(BusinessMessages.getString("List.OrdCalcTransIniCalcCI.shortName"), new OrdCalcTransIniCalcCICreator());
    res.put(BusinessMessages.getString("List.OrdCalcTransIniCalcPrecedent.shortName"), new OrdCalcTransIniCalcPrecedentCreator());
    res.put(BusinessMessages.getString("List.OrdCalcTransIniClicheCreator.shortName"), new OrdCalcTransIniCalcClicheCreator());
    return res;
  }

  public static OrdCalc createDefaultOcal(Calc calc, CrueConfigMetier ccm) {
    if (calc.isPermanent()) {
      final OrdCalcPseudoPermIniCalcPrecedent res = new OrdCalcPseudoPermIniCalcPrecedent();
      res.setCalcPseudoPerm((CalcPseudoPerm) calc);
      return res;
    }
    OrdCalcTransIniCalcPrecedent res = new OrdCalcTransIniCalcPrecedent(ccm);
    res.setCalcTrans((CalcTrans) calc);
    return res;
  }

  public interface OrdCalcCreator {

    OrdCalc create(Calc calc, CrueConfigMetier ccm);

    Class getOrdCalcClass();
  }

  public static class OrdCalcPseudoPermIniCalcCICreator implements OrdCalcCreator {

    @Override
    public OrdCalc create(Calc calc, CrueConfigMetier ccm) {
      final OrdCalcPseudoPermIniCalcCI res = new OrdCalcPseudoPermIniCalcCI();
      res.setCalcPseudoPerm((CalcPseudoPerm) calc);
      return res;
    }

    @Override
    public Class getOrdCalcClass() {
      return OrdCalcPseudoPermIniCalcCI.class;
    }
  }

  public static class OrdCalcPseudoPermIniCalcPrecedentCreator implements OrdCalcCreator {

    @Override
    public OrdCalc create(Calc calc, CrueConfigMetier ccm) {
      final OrdCalcPseudoPermIniCalcPrecedent res = new OrdCalcPseudoPermIniCalcPrecedent();
      res.setCalcPseudoPerm((CalcPseudoPerm) calc);
      return res;
    }

    @Override
    public Class getOrdCalcClass() {
      return OrdCalcPseudoPermIniCalcPrecedent.class;
    }
  }

  public static class OrdCalcPseudoPermIniClicheCreator implements OrdCalcCreator {

    @Override
    public OrdCalc create(Calc calc, CrueConfigMetier ccm) {
      final OrdCalcPseudoPermIniCliche res = new OrdCalcPseudoPermIniCliche();
      res.setCalcPseudoPerm((CalcPseudoPerm) calc);
      return res;
    }

    @Override
    public Class getOrdCalcClass() {
      return OrdCalcPseudoPermIniCliche.class;
    }
  }

  public static class OrdCalcTransIniCalcCICreator implements OrdCalcCreator {

    @Override
    public OrdCalc create(Calc calc, CrueConfigMetier ccm) {
      final OrdCalcTransIniCalcCI res = new OrdCalcTransIniCalcCI(ccm);
      res.setCalcTrans((CalcTrans) calc);
      return res;
    }

    @Override
    public Class getOrdCalcClass() {
      return OrdCalcTransIniCalcCI.class;
    }
  }

  public static class OrdCalcTransIniCalcPrecedentCreator implements OrdCalcCreator {

    @Override
    public OrdCalc create(Calc calc, CrueConfigMetier ccm) {
      final OrdCalcTransIniCalcPrecedent res = new OrdCalcTransIniCalcPrecedent(ccm);
      res.setCalcTrans((CalcTrans) calc);
      return res;
    }

    @Override
    public Class getOrdCalcClass() {
      return OrdCalcTransIniCalcPrecedent.class;
    }
  }

  public static class OrdCalcTransIniCalcClicheCreator implements OrdCalcCreator {

    @Override
    public OrdCalc create(Calc calc, CrueConfigMetier ccm) {
      final OrdCalcTransIniCalcCliche res = new OrdCalcTransIniCalcCliche(ccm);
      res.setCalcTrans((CalcTrans) calc);
      return res;
    }

    @Override
    public Class getOrdCalcClass() {
      return OrdCalcTransIniCalcCliche.class;
    }
  }

  public static List<String> getOrdCalPseudoNames() {
    return new ArrayList<>(createMapPseudoPermanent().keySet());
  }

  public static List<String> getOrdCalTransitoireNames() {
    return new ArrayList<>(createMapTransitoire().keySet());
  }

  public static String findName(OrdCalc ocal) {
    Map<String, OrdCalcCreator> map = ocal.isTransitoire() ? createMapTransitoire() : createMapPseudoPermanent();
    for (Map.Entry<String, OrdCalcCreator> entry : map.entrySet()) {
      String string = entry.getKey();
      OrdCalcCreator ordCalcCreator = entry.getValue();
      if (ordCalcCreator.getOrdCalcClass().equals(ocal.getClass())) {
        return string;
      }
    }
    return ocal.getType();
  }

  public static Map<String, OrdCalcCreator> createMap(OrdCalc init) {
    if (init.isTransitoire()) {
      return createMapTransitoire();
    }
    return createMapPseudoPermanent();
  }
}
