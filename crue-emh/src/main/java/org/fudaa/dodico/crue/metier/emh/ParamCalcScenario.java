/**
 * *********************************************************************
 * Module: ParamCalcScenario.java Author: deniger Purpose: Defines the Class ParamCalcScenario
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;
import org.joda.time.LocalDateTime;

public class ParamCalcScenario extends AbstractInfosEMH implements ToStringTransformable {
  
  public static final String PROP_DATE_DEB="dateDebSce";

  private LocalDateTime dateDebSce;

  public ParamCalcScenario(CrueConfigMetier props) {
    dateDebSce = props.getDefaultDateValue("dateDebSce");
  }

  private ParamCalcScenario(LocalDateTime dateDebSce) {
    this.dateDebSce = dateDebSce;
  }

  public ParamCalcScenario deepClone() {
    return new ParamCalcScenario(dateDebSce);

  }

  @PropertyDesc(i18n="dateDebSce.property")
  public final LocalDateTime getDateDebSce() {
    return dateDebSce;
  }

  @Override
  public EnumInfosEMH getCatType() {
    return EnumInfosEMH.PARAM_CALC_SCENARIO;
  }

  /**
   * @param newDateDebSce
   */
  public final void setDateDebSce(final LocalDateTime newDateDebSce) {
    dateDebSce = newDateDebSce;
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    if (EnumToString.OVERVIEW.equals(format)) {
      return getClass().getSimpleName();
    }
    return getClass().getSimpleName()
            + " dateDebSce=" + TransformerEMHHelper.formatFromPropertyName("dateDebSce=", dateDebSce, props, DecimalFormatEpsilonEnum.COMPARISON);
  }
}
