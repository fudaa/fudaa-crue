/*
 * Licence GPL Copyright
 */
package org.fudaa.dodico.crue.metier;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.config.coeur.FileType;

/**
 * @author deniger
 */
public enum CrueFileType implements FileType{

  ETU(CrueLevelType.PROJET, null),
  AOC(CrueLevelType.PROJET, CrueVersionType.CRUE10),
  DCLM(CrueLevelType.SCENARIO, CrueVersionType.CRUE10),
  DCSP(CrueLevelType.SOUS_MODELE, CrueVersionType.CRUE10),
  DFRT(CrueLevelType.SOUS_MODELE, CrueVersionType.CRUE10),
  DLHY(CrueLevelType.SCENARIO, CrueVersionType.CRUE10),
  LHPT(CrueLevelType.SCENARIO, CrueVersionType.CRUE10),
  DPTG(CrueLevelType.SOUS_MODELE, CrueVersionType.CRUE10),
  DPTI(CrueLevelType.MODELE, CrueVersionType.CRUE10),
  DREG(CrueLevelType.MODELE, CrueVersionType.CRUE10),
  DRSO(CrueLevelType.SOUS_MODELE, CrueVersionType.CRUE10),
  OCAL(CrueLevelType.SCENARIO, CrueVersionType.CRUE10),
  OPTG(CrueLevelType.MODELE, CrueVersionType.CRUE10),
  OPTI(CrueLevelType.MODELE, CrueVersionType.CRUE10),
  OPTR(CrueLevelType.MODELE, CrueVersionType.CRUE10),
  ORES(CrueLevelType.SCENARIO, CrueVersionType.CRUE10),
  PCAL(CrueLevelType.SCENARIO, CrueVersionType.CRUE10),
  PNUM(CrueLevelType.MODELE, CrueVersionType.CRUE10),
  //les fichiers de résultats de crue10.
  RPTR(CrueLevelType.MODELE, CrueVersionType.CRUE10, CrueFileType.OPTR, new CrueFileType[]{CrueFileType.DCLM, CrueFileType.DRSO}),
  RPTG(CrueLevelType.MODELE, CrueVersionType.CRUE10, CrueFileType.OPTG,
  new CrueFileType[]{CrueFileType.DRSO, CrueFileType.DPTG, CrueFileType.DFRT, CrueFileType.DCSP,CrueFileType.ORES}),
  RPTI(CrueLevelType.MODELE, CrueVersionType.CRUE10, CrueFileType.OPTI,
  new CrueFileType[]{CrueFileType.DPTI, CrueFileType.DRSO, CrueFileType.RPTG}),
  RCAL(CrueLevelType.MODELE, CrueVersionType.CRUE10, CrueFileType.OCAL,
  new CrueFileType[]{CrueFileType.DRSO, CrueFileType.DCLM, CrueFileType.DCSP, CrueFileType.DLHY, CrueFileType.PCAL,
CrueFileType.ORES, CrueFileType.PNUM, CrueFileType.DREG,
CrueFileType.RPTR, CrueFileType.RPTG, CrueFileType.RPTI
}),
  //les fichiers crue9
  DC(CrueLevelType.MODELE, CrueVersionType.CRUE9),
  DH(CrueLevelType.MODELE, CrueVersionType.CRUE9),
  STO(CrueLevelType.MODELE, CrueVersionType.CRUE9, CrueFileType.DC),
  STR(CrueLevelType.MODELE, CrueVersionType.CRUE9, CrueFileType.DC),
  FCB(CrueLevelType.MODELE, CrueVersionType.CRUE9, CrueFileType.DH, new CrueFileType[]{CrueFileType.STO,
CrueFileType.STR});
  private final CrueLevelType level;
  private final CrueVersionType crueVersionType;
  private final String extension;
  private final CrueFileType typeUseForName;
  private final Set<CrueFileType> inputTypeForRes;
  private final boolean isResultFileType;
  private final boolean isOptional;

  /**
   * @param level
   */
  CrueFileType(final CrueLevelType level, final CrueVersionType crueType) {
    this(level, crueType, false);
  }
  CrueFileType(final CrueLevelType level, final CrueVersionType crueType,boolean isOptional) {
    this(level, crueType, isOptional,null);
  }
  CrueFileType(final CrueLevelType level, final CrueVersionType crueType,final CrueFileType typeUseForResName) {
    this(level, crueType, false,typeUseForResName);
  }


  /**
   * A result file type
   *
   * @param level
   * @param crueType
   * @param typeUseForResName if non null, the type is considered as a res file type. This type is the type use to build the name of the res file.
   */
  CrueFileType(final CrueLevelType level, final CrueVersionType crueType,boolean isOptional, final CrueFileType typeUseForResName) {
    this(level, crueType, isOptional,typeUseForResName, null);
  }

  CrueFileType(final CrueLevelType level, final CrueVersionType crueType, final CrueFileType typeUseForName,
               final CrueFileType[] inputTypeForRes) {
    this(level,crueType,false,typeUseForName,inputTypeForRes);

  }

  /**
   * A result file type
   *
   * @param level
   * @param crueType
   * @param typeUseForName if non null, the type is considered as a res file type. This type is the type use to build the name of the res file.
   * @param inputTypeForRes the input CrueFileType used to generate the result file.
   */
  CrueFileType(final CrueLevelType level, final CrueVersionType crueType,boolean isOptional, final CrueFileType typeUseForName,
               final CrueFileType[] inputTypeForRes) {
    this.level = level;
    this.isOptional=isOptional;
    this.crueVersionType = crueType;

    if (crueVersionType == CrueVersionType.CRUE9) {
      this.extension = this.name().toLowerCase();
    } else {
      this.extension = this.name().toLowerCase() + ".xml";
    }
    isResultFileType = typeUseForName != null;
    this.typeUseForName = typeUseForName;
    if (isResultFileType) {
      this.inputTypeForRes = new HashSet<>();
      this.inputTypeForRes.add(typeUseForName);
      if (inputTypeForRes != null) {
        this.inputTypeForRes.addAll(Arrays.asList(inputTypeForRes));
      }
    } else {
      this.inputTypeForRes = Collections.emptySet();
    }

  }

  public boolean isOptional() {
    return isOptional;
  }

  public CrueVersionType getCrueVersionType() {
    return crueVersionType;
  }

  @Override
  public String getExtension() {
    return extension;
  }

  public boolean hasExtension(final File file) {
    return file != null && hasExtension(file.getName());
  }

  public boolean hasExtension(final String name) {
    return name != null && name.toLowerCase().endsWith("." + getExtension());
  }

  public File getWithExtension(final File initFile) {
    if (initFile == null || initFile.isDirectory()) {
      return null;
    }
    if (!hasExtension(initFile)) {
      return new File(initFile.getParentFile(),
              CtuluLibFile.getSansExtension(initFile.getName()) + "." + getExtension());
    }
    return initFile;
  }

  public File getFile(File parentDir,String radical){
    return new File(parentDir,radical+"."+getExtension());
  }

  public Set<CrueFileType> getInputTypeForRes() {
    return inputTypeForRes;
  }

  public CrueLevelType getLevel() {
    return level;
  }

  public CrueFileType getTypeUseForName() {
    return typeUseForName;
  }

  public boolean isModeleLevel() {
    return CrueLevelType.MODELE.equals(level);
  }

  public boolean isProjetLevel() {
    return CrueLevelType.PROJET.equals(level);
  }

  public boolean isResultFileType() {
    return isResultFileType;
  }

  /**
   * @return true if the level of this file is scenario.
   */
  public boolean isScenarioLevel() {
    return CrueLevelType.SCENARIO.equals(level);
  }

  public boolean isSousModelLevel() {
    return CrueLevelType.SOUS_MODELE.equals(level);
  }

  public static CrueFileType getByExtension(final String name) {
    final CrueFileType[] types = CrueFileType.values();

    for (final CrueFileType type : types) {
      if (type.hasExtension(name)) {
        return type;
      }
    }

    return null;
  }
}
