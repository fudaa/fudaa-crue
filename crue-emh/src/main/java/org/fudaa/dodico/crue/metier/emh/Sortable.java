package org.fudaa.dodico.crue.metier.emh;

/**
 * @author deniger
 */
public interface Sortable {

  /**
   * 
   * @return true si modification opérée suite au tri.
   */
  boolean sort();

}
