/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.result;

import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.LoiDF;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;

/**
 * @author Frederic Deniger
 */
public class TimeSimuConverter {
  private static final TimeSimuConverter NULL_TEMP_SIMU_CONVERTER = new TimeSimuConverter(0, 0);
  private final long dateDebRunCourant;
  private final long dateDebRunAlternatif;

  private TimeSimuConverter(long dateDebRunCourant, long dateDebRunAlternatif) {
    this.dateDebRunCourant = dateDebRunCourant;
    this.dateDebRunAlternatif = dateDebRunAlternatif;
  }

  public static long getDatDebInMillis(EMHScenario scenario) {
    if (scenario.getParamCalcScenario().getDateDebSce() != null) {
      return scenario.getParamCalcScenario().getDateDebSce().toDateTime().getMillis();
    }
    return 0;
  }

  public static TimeSimuConverter create(EMHScenario scenarioCourant, EMHScenario scenarioAlternatif) {
    if (scenarioCourant.getParamCalcScenario().getDateDebSce() != null && scenarioAlternatif.getParamCalcScenario().getDateDebSce() != null) {
      long tCourant = getDatDebInMillis(scenarioCourant);
      long tAlternatif = getDatDebInMillis(scenarioAlternatif);
      if (tCourant == tAlternatif) {
        return NULL_TEMP_SIMU_CONVERTER;
      }
      return new TimeSimuConverter(tCourant, tAlternatif);
    }
    return NULL_TEMP_SIMU_CONVERTER;
  }

  public static double toSecond(double millis) {
    return millis / 1000d;
  }

  public static long toMilis(double sec) {
    return Math.round(sec * 1000d);
  }

  /**
   *
   * @param scenario le scenario utilise pour le dateDeb
   * @param loi la loi
   * @return le delta en second
   */
  public static double getDeltaToApplyForDateTZero(EMHScenario scenario, LoiDF loi) {
    double dateZeroSecond = 0;
    double dateZeroScenario = getDatDebInMillis(scenario);
    if (((long) dateZeroScenario) > 0) {
      dateZeroSecond = toSecond(loi.getDateZeroLoiDF() == null ? 0 : loi.getDateZeroLoiDF().toDateTime().getMillis());
      dateZeroSecond = dateZeroSecond - toSecond(dateZeroScenario);
    }
    return dateZeroSecond;
  }

  public long getDateDebRunAlternatif() {
    return dateDebRunAlternatif;
  }

  public long getDateDebRunCourant() {
    return dateDebRunCourant;
  }

  public long getTempsSimuRunAlternatif(long tempSimuRunCourant) {
    return tempSimuRunCourant + dateDebRunCourant - dateDebRunAlternatif;
  }

  public ResultatTimeKey getEquivalentTempsSimuRunAlternatif(ResultatTimeKey tempsRunCourant, EMHModeleBase modeleParentAlternatif) {
    if (tempsRunCourant == null || tempsRunCourant.isPermanent()) {
      return tempsRunCourant;
    }
    if (dateDebRunAlternatif == dateDebRunCourant) {
      return modeleParentAlternatif.getResultatPasDeTemps().getEquivalentTempsSimu(tempsRunCourant);
    }
    long tempsSimuRunAlternatif = getTempsSimuRunAlternatif(tempsRunCourant.getDuree());
    return modeleParentAlternatif.getResultatPasDeTemps().getEquivalentTempsSimu(tempsSimuRunAlternatif);
  }
}
