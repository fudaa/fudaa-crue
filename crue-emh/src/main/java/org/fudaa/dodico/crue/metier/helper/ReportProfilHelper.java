/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.helper;

import java.util.Map;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoSectionIdem;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSectionIdem;
import org.fudaa.dodico.crue.metier.emh.EMHSectionProfil;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.PtProfil;
import org.fudaa.dodico.crue.metier.emh.RelationEMHDansSousModele;

/**
 *
 * @author Frederic Deniger
 */
public class ReportProfilHelper {

  public static Pair<EMHSectionProfil, Double> getProfilDz(EMHSectionIdem from, double initDz) {
    CatEMHSection sectionRef = from.getSectionRef();
    if (EMHSectionProfil.class.equals(sectionRef.getClass())) {
      DonPrtGeoSectionIdem donPrtSectionIdem = DonPrtHelper.getDonPrtSectionIdem(from);
      double newDz = initDz + donPrtSectionIdem.getDz();
      return new Pair<>((EMHSectionProfil) sectionRef, newDz);
    } else if (EMHSectionIdem.class.equals(sectionRef.getClass())) {
      DonPrtGeoSectionIdem donPrtSectionIdem = DonPrtHelper.getDonPrtSectionIdem(from);
      double newDz = initDz + donPrtSectionIdem.getDz();
      return getProfilDz((EMHSectionIdem) sectionRef, newDz);
    }
    return null;
  }

  public static Double getRPTGZf(EMH emh) {
    if (emh.getRPTG() == null) {
      return null;
    }
    Map<String, Object> values = emh.getRPTG().getValues();
    if (!values.containsKey(CrueConfigMetierConstants.PROP_ZF)) {
      return null;
    }
    return (Double) values.get(CrueConfigMetierConstants.PROP_ZF);
  }

  public static EMHSectionProfil retrieveProfil(EMH emh) {
    if (emh == null) {
      return null;
    }
    EMHSectionProfil profil = null;
    if (EMHSectionProfil.class.equals(emh.getClass())) {
      profil = (EMHSectionProfil) emh;
    } else if (EMHSectionIdem.class.equals(emh.getClass())) {
      Pair<EMHSectionProfil, Double> profilDz = ReportProfilHelper.getProfilDz((EMHSectionIdem) emh, 0);
      if (profilDz != null) {
        profil = profilDz.first;
        double dz = profilDz.second;
        DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection(profil);
        DonPrtGeoProfilSection profilSectionDz = new DonPrtGeoProfilSection();
        profilSectionDz.initDataFrom(profilSection);
        for (PtProfil ptProfil : profilSectionDz.getPtProfil()) {
          ptProfil.setZ(ptProfil.getZ() + dz);
        }
        profil = new EMHSectionProfil(emh.getNom());
        final EMHSousModele parent = ((EMHSectionIdem) emh).getParent();
        profil.addRelationEMH(new RelationEMHDansSousModele(parent));
        profil.addInfosEMH(profilSectionDz);
      }
    }
    return profil;
  }
}
