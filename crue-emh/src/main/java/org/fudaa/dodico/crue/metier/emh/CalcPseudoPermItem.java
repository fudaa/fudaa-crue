package org.fudaa.dodico.crue.metier.emh;

public interface CalcPseudoPermItem extends DonCLimM {

  /**
   * Fonction générique permettant de récupérer les données d'un CalPseudoPerm la valeur
   *
   */
  double getValue();

  /**
   * @param newVal
   */
  void setValue(double newVal);

  boolean containValue();

  String getCcmVariableName();
}
