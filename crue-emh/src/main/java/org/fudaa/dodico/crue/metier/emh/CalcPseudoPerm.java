package org.fudaa.dodico.crue.metier.emh;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * CalcPseudoPerm
 */
public class CalcPseudoPerm extends Calc {

  public CalcPseudoPerm() {
    super(getDclmClassInOrder());
  }

  @Override
  public boolean isTransitoire() {
    return false;
  }

  @Override
  public CalcPseudoPerm deepClone() {
    try {
      CalcPseudoPerm trans = (CalcPseudoPerm) clone();
      trans.container = container.deepClone();
      trans.container.setCalculParent(trans);
      return trans;
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcPseudoPerm.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("why");
  }

  public List<CalcPseudoPermItem> getlisteDCLMPseudoPerm() {
    final ArrayList<CalcPseudoPermItem> res = new ArrayList<>();
    container.getAllDclm().forEach(item-> res.add((CalcPseudoPermItem)item));
    return res;
  }

  public static List<Class> getDclmClassInOrder() {
    return Arrays.asList(
        CalcPseudoPermNoeudQapp.class,
        CalcPseudoPermNoeudNiveauContinuZimp.class,
        CalcPseudoPermBrancheOrificeManoeuvre.class,
        CalcPseudoPermBrancheOrificeManoeuvreRegul.class,
        CalcPseudoPermBrancheSaintVenantQruis.class,
        CalcPseudoPermCasierProfilQruis.class,
        CalcPseudoPermNoeudBg1.class,
        CalcPseudoPermNoeudBg2.class,
        CalcPseudoPermNoeudUsi.class,
        CalcPseudoPermNoeudBg1Av.class,
        CalcPseudoPermNoeudBg2Av.class
            );
  }



}
