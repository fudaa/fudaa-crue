/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier;

import java.nio.ByteOrder;

/**
 *
 * @author Frederic Deniger
 */
public interface StoContent {

  ByteOrder getOrder();

  boolean isParamGenOrDimensionNotDefined();

  int getNpo();

  int getNbstr();

  int getNblitmax();

  int getNbhaut();

  int getLReclProf();

  int getNbRecProf();
  
}
