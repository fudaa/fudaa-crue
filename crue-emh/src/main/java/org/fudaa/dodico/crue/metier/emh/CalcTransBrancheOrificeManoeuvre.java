package org.fudaa.dodico.crue.metier.emh;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;

/**
 * CL de type 41 ou 42 en Crue9
 *
 */
public class CalcTransBrancheOrificeManoeuvre extends DonCLimMCommonItem implements CalcTransItem, DonCLimMWithSensOuv {

  //attention modifier la constante CalcPseudoPermBrancheOrificeManoeuvre.PROP_SENS_OUV di-dessus si modification de nom
  private EnumSensOuv sensOuv;
  private LoiDF manoeuvre;

  public CalcTransBrancheOrificeManoeuvre(final CrueConfigMetier def) {
    final String enumValue = def.getDefaultStringEnumValue("sensOuv");
    if (enumValue != null) {
      try {
        sensOuv = EnumSensOuv.valueOf(enumValue);
      } catch (final Exception e) {
      }
    }
  }

  @PropertyDesc(i18n = "sensOuv.property")
  public EnumSensOuv getSensOuv() {
    return sensOuv;
  }

  @Override
  public EnumTypeLoi getTypeLoi() {
    return EnumTypeLoi.LoiTOuv;
  }

  /**
   * @param newSensOuv
   */
  public void setSensOuv(final EnumSensOuv newSensOuv) {
    sensOuv = newSensOuv;
  }

  /**
   * Loi Manoeuvre : VarAbscisse = t VarOrdonnee = Ouv
   *
   */
  @PropertyDesc(i18n = "manoeuvre.property")
  public LoiDF getManoeuvre() {
    return manoeuvre;
  }

  @Override
  public String getShortName() {
    return BusinessMessages.getString("manoeuvre.property");
  }

  /**
   * Loi Manoeuvre : VarAbscisse = t VarOrdonnee = Ouv
   *
   */
  public void setManoeuvre(final LoiDF newManoeuvre) {
    if (this.manoeuvre != null) {
      this.manoeuvre.unregister(this);
    }

    manoeuvre = newManoeuvre;
    if (this.manoeuvre != null) {
      this.manoeuvre.register(this);
    }
  }

  @Visibility(ihm = false)
  @Override
  public Loi getLoi() {
    return getManoeuvre();
  }

  /**
   * @param newLoi
   */
  @Override
  public void setLoi(final Loi newLoi) {
    setManoeuvre((LoiDF) newLoi);
  }

  public CalcTransBrancheOrificeManoeuvre deepClone() {
    try {
      return (CalcTransBrancheOrificeManoeuvre) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransNoeudNiveauContinuTarage.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }
}
