package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 * @author Frederic Deniger
 */
public class EMHSectionInterpolee extends CatEMHSection {

  public EMHSectionInterpolee(final String nom) {
    super(nom);
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public EnumSectionType getSectionType() {
    return EnumSectionType.EMHSectionInterpolee;
  }

  @Override
  @Visibility(ihm = false)
  public EMHSectionInterpolee copyShallowFirstLevel() {
    return copyPrimitiveIn(new EMHSectionInterpolee(getNom()));
  }
}
