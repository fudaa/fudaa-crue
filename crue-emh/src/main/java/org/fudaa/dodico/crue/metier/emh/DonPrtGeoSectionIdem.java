package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;

/**
 * Données géométriques d'une EMHSectionIdem, appelé ProfIdem dans les fichiers
 */
public class DonPrtGeoSectionIdem extends DonPrtGeo implements Cloneable {
  private double dz;

  public DonPrtGeoSectionIdem(final CrueConfigMetier def) {
    dz = def.getDefaultDoubleValue("dz");
  }

  public double getDz() {
    return dz;
  }

  public void setDz(final double newDz) {
    dz = newDz;
  }

  @Override
  public DonPrtGeo cloneDPTG() {
    try {
      return (DonPrtGeo) clone();
    } catch (CloneNotSupportedException e) {
    }
    return null;
  }
}
