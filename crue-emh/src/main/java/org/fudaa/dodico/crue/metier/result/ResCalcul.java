package org.fudaa.dodico.crue.metier.result;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Pour crue9 uniquement.
 *
 * @author deniger
 */
public abstract class ResCalcul {

  private final Map<String, Object> res = new HashMap<>();
  private final Map<String, Object> resExt = Collections.unmodifiableMap(res);

  public void addResultat(String property, Object value) {
    res.put(property, value);
  }

  public Map<String, Object> getRes() {
    return resExt;
  }
}
