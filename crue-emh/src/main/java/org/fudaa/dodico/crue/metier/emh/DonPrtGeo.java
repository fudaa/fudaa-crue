package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;

public abstract class DonPrtGeo extends AbstractInfosEMH implements Cloneable{
  @Override
  public final EnumInfosEMH getCatType() {
    return EnumInfosEMH.DON_PRT_GEO;
  }

  @Visibility(ihm = false)
  public abstract DonPrtGeo cloneDPTG();
}
