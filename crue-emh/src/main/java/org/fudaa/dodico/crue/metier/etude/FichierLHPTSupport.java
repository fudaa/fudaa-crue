package org.fudaa.dodico.crue.metier.etude;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;

import java.io.File;
import java.util.*;

/**
 * Pour ne pas modifier la grammaire de l'étude les fichiers lhpt sont chargés à part.
 * Cette classe gère ces fichiers.
 * Created by deniger on 09/06/2017.
 */
public class FichierLHPTSupport {
    /**
     * Tous les fichiers LHPT par nom de scenario
     */
    private final Map<String, FichierCrue> fichierCrueByScenarioName = new TreeMap<>();
    /**
     * Tous les nom scenarios par nom de fichier LHPT
     */
    private final Map<String, String> scenarioNameByfichierCrueName = new TreeMap<>();
    private EMHProjet emhProjet;

    public FichierLHPTSupport(EMHProjet emhProjet) {
        createMap(emhProjet);
    }

    /**
     * @param listToModify liste dans laquelle on enleve les fichier lhpt
     */
    public static void removeLHPTFiles(List<FichierCrue> listToModify) {
        listToModify.removeIf(fichierCrue -> fichierCrue.getType().equals(CrueFileType.LHPT));
    }

    public static File getLHPTFile(EMHProjet emhProjet, ManagerEMHScenario scenario) {
        return getLHPTFile(emhProjet, scenario.getNom());
    }

    private static File getLHPTFile(EMHProjet emhProjet, String scenarioName) {
        String fileName = StringUtils.removeStart(scenarioName, CruePrefix.P_SCENARIO);
        return new File(emhProjet.getDirOfFichiersEtudes(), fileName + "." + CrueFileType.LHPT.getExtension());
    }

    /**
     * Met à jour la liste des fichiers LHTPT dans l'étude et ses scenarios.
     * @return true si des modifications ont été effectuées
     */
    public boolean updateLHPTFilesInProject() {
        //lhpt:
        final Collection<FichierCrue> lhptFiles = getFichierCrueList();
        Map<String, FichierCrue> fichierLHPTToAddByName = TransformerHelper.toMapOfNom(lhptFiles);
        final List<FichierCrue> fichiersProjets = emhProjet.getInfos().baseFichiersProjets;

        boolean modified = false;
        //on enlève de l'étude les fichiers LHPT qui ne sont pas présents
        for (Iterator<FichierCrue> iterator = fichiersProjets.iterator(); iterator.hasNext(); ) {
            FichierCrue fichierCrue = iterator.next();
            if (CrueFileType.LHPT.equals(fichierCrue.getType()) && !fichierLHPTToAddByName.containsKey(fichierCrue.getNom())) {
                iterator.remove();
                modified = true;
                //on doit l'enlever des scenarios
                for (ManagerEMHScenario managerEMHScenario : emhProjet.getListeScenarios()) {
                    managerEMHScenario.getListeFichiers().removeFile(fichierCrue);
                }
            }
        }
        //on ajoute les nouveaux fichiers LHPT non encore présents dans l'étude
        Map<String, FichierCrue> fichierLHPTAlreadyInProject = TransformerHelper.toMapOfNom(fichiersProjets);
        for (FichierCrue lhptFile : lhptFiles) {
            if (!fichierLHPTAlreadyInProject.containsKey(lhptFile.getNom())) {
                modified = true;
                fichiersProjets.add(lhptFile);
                //on l'ajoute au scenario
                String scenarioName = scenarioNameByfichierCrueName.get(lhptFile.getNom());
                emhProjet.getScenario(scenarioName).getListeFichiers().addFile(lhptFile);
            }
        }
        return modified;
    }

    public boolean isSupport(String fichierCrueName) {
        return fichierCrueName != null && fichierCrueName.endsWith(CrueFileType.LHPT.getExtension());
    }

    /**
     * @return fichiers lhpt gerés par l'étude
     */
    public Collection<FichierCrue> getFichierCrueList() {
        return fichierCrueByScenarioName.values();
    }

    /**
     * @param scenarioName id du fichier crue
     * @return instance FichierCrue correspondant à l'id
     */
    public FichierCrue get(String scenarioName) {
        return fichierCrueByScenarioName.get(scenarioName);
    }

    public FichierCrue get(ManagerEMHScenario scenario) {
        return fichierCrueByScenarioName.get(scenario.getNom());
    }


    private void createMap(EMHProjet emhProjet) {
        this.emhProjet = emhProjet;
        final Set<String> scenarioNames = TransformerHelper.toSetNom(emhProjet.getListeScenarios());
        final File dirOfFichiersEtudes = emhProjet.getDirOfFichiersEtudes();
        if (dirOfFichiersEtudes == null) {
            return;
        }
        for (String scenarioName : scenarioNames) {
            File toCheck = getLHPTFile(emhProjet, scenarioName);
            if (toCheck.exists()) {
                FichierCrue fichierCrue = new FichierCrue(toCheck.getName(), FichierCrue.CURRENT_PATH, CrueFileType.LHPT);
                fichierCrueByScenarioName.put(scenarioName, fichierCrue);
                scenarioNameByfichierCrueName.put(fichierCrue.getNom(), scenarioName);
            }
        }
    }

    FichierCrue getForScenario(ManagerEMHScenario baseScenario) {
        return fichierCrueByScenarioName.get(baseScenario.getNom());
    }
}
