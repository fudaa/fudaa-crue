package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * CalcPseudoPermNoeudBg1
 */
public class CalcPseudoPermNoeudUsi extends DonCLimMCommonItem implements CalcPseudoPermItem {


  public CalcPseudoPermNoeudUsi(final CrueConfigMetier def) {
  }

  @Override
  public double getValue() {
    throw new IllegalAccessError("Non Utilisable");
  }

  @Override
  public void setValue(double newVal) {
    throw new IllegalAccessError("Non Utilisable");
  }

  @Visibility(ihm = false)
  public boolean containValue() {
    return false;
  }

  @Override
  public String getCcmVariableName() {
    return null;
  }

  @Override
  public String getShortName() {
    return BusinessMessages.getString("usine.property");
  }


  /**
   * Fonction générique permettant de récupérer les données d'un CalPseudoPerm la valeur
   */

  CalcPseudoPermNoeudUsi deepClone() {
    try {
      return (CalcPseudoPermNoeudUsi) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransNoeudNiveauContinuTarage.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }
}
