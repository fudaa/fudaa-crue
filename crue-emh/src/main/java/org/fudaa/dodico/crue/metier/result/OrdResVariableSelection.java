/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.result;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.fudaa.dodico.crue.metier.OrdResKey;

/**
 * Permet de contenir, par section de OrdRes, les variables sélectionnables / sélectionnées.
 *
 * @author Frederic Deniger
 */
public class OrdResVariableSelection {

  final OrdResKey key;
  final Map<String, Boolean> selectionByVariableName = new TreeMap<>();
  final List<String> allVariable;

  /**
   *
   * @param key
   * @param selectableVar les variables sélectionnables.
   * @param allVariable toutes les variables
   */
  public OrdResVariableSelection(OrdResKey key, Collection<String> selectableVar, List<String> allVariable) {
    this.key = key;
    if (selectableVar != null) {
      for (String var : selectableVar) {
        selectionByVariableName.put(var, Boolean.FALSE);
      }
    }
    this.allVariable = Collections.unmodifiableList(allVariable);
  }

  /**
   *
   * @return toutes les variables triées associées a ce ordres meme celles nons sélectionnables
   */
  public List<String> getAllVariable() {
    return allVariable;
  }

  public OrdResKey getKey() {
    return key;
  }

  /**
   *
   * @return les variables selectionnables
   */
  public List<String> getSelectableVariables() {
    return new ArrayList<>((selectionByVariableName.keySet()));
  }

  /**
   *
   * @return les variables sélectionnés
   */
  public List<String> getSelectedVariables() {
    List<String> res = new ArrayList<>();
    for (Map.Entry<String, Boolean> entry : selectionByVariableName.entrySet()) {
      if (Boolean.TRUE.equals(entry.getValue())) {
        res.add(entry.getKey());
      }
    }
    return res;
  }

  public boolean isVariableSelected(String var) {
    return Boolean.TRUE.equals(selectionByVariableName.get(var));
  }

  public void setVariableSelected(String var, boolean used) {
    if (selectionByVariableName.containsKey(var)) {
      selectionByVariableName.put(var, Boolean.valueOf(used));
    }
  }

  public boolean isSelectable(String variable) {
    return selectionByVariableName.containsKey(variable);
  }
}
