/**
 * *********************************************************************
 * Module: OrdResBrancheSaintVenant.java Author: deniger Purpose: Defines the Class OrdResBrancheSaintVenant
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.OrdResKey;

public class OrdResBrancheSaintVenant extends OrdRes implements Cloneable {

  @Override
  public OrdResBrancheSaintVenant clone() {
    try {
      return (OrdResBrancheSaintVenant) super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why this ?");
  }
  private final static OrdResKey KEY = new OrdResKey(EnumBrancheType.EMHBrancheSaintVenant);

  @Override
  public OrdResKey getKey() {
    return KEY;
  }

  @Override
  public String toString() {
    return "OrdResBrancheSaintVenant [ddeQlat=" + getDdeValue("qlat") + ", ddeSplanAct=" + getDdeValue("splanAct") + ", ddeSplanSto="
            + getDdeValue("splanSto") + ", ddeSplanStot=" + getDdeValue("splanTot") + ", ddeVol=" + getDdeValue("vol") + "]";
  }
}
