package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;

public interface DonCLimMWithManoeuvreRegulData extends DonCLimMWithLoi, DonCLimM, DonCLimMWithSensOuv {


  @PropertyDesc(i18n = "param.property")
   String getParam();


  String getDesc();

  String getTypeAndEMH();

  void setParam(String editedParam);


}
