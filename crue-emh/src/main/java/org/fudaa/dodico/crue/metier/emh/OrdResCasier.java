/**
 * *********************************************************************
 * Module: OrdResCasierProfil.java Author: deniger Purpose: Defines the Class OrdResCasierProfil
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

// WARN: l'ordre des champs est important car utilise par l'ecriture/lecture de ORES

import org.fudaa.dodico.crue.metier.OrdResKey;

public class OrdResCasier extends OrdRes implements Cloneable {
  private final static OrdResKey KEY = new OrdResKey(EnumCatEMH.CASIER);

  @Override
  public OrdResKey getKey() {
    return KEY;
  }

  @Override
  public OrdResCasier clone() {
    try {
      return (OrdResCasier) super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why this ?");
  }

  @Override
  public String toString() {
    return "OrdResCasier [ddeQech=" + getDdeValue("qech") + ", ddeSplan=" + getDdeValue("splan") + ", ddeVol=" + getDdeValue("vol")
        + ", ddeZ=" + getDdeValue("z") + "]";
  }
}
