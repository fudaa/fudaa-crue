package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

import java.util.List;


/**
 * Nom =Conteneur de données
 */
public abstract class CatEMHConteneur extends CatEMHActivable {

  public CatEMHConteneur() {
    super(null);
    super.setUserActive(true);
  }

  public CatEMHConteneur(final String nom) {
    super(nom);
    super.setUserActive(true);
  }

  /**
   * @return true si ce conteneur ne contient ni info ni Relation
   */
  public boolean isEmpty() {
    return isRelationsEMHEmpty() && isInfosEMHEmpty();
  }


  public abstract List<CatEMHSection> getSections();

  public abstract List<CatEMHNoeud> getNoeuds();

  public abstract List<CatEMHBranche> getBranches();

  public abstract List<EMHBrancheSaintVenant> getBranchesSaintVenant();

  public abstract List<CatEMHBranche> getBranchesAvecSectionPilote();

  public abstract List<CatEMHCasier> getCasiers();

  /**
   * @return tous les emh simple (non conteneur) contenu par ce conteneur. L'ordre n'est pas assurer
   */
  public abstract List<EMH> getAllSimpleEMH();
  /**
   * @return tous les emh contenu par ce conteneur. L'ordre n'est pas assurer
   */
  @UsedByComparison
  public abstract List<EMH> getAllSimpleEMHinUserOrder();
}
