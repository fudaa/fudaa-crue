/*
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.crue.metier;

import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;

/**
 * Constante definissant le niveau d'une donnée de crue.
 *
 * @author deniger
 */
public enum CrueLevelType {

  PROJET("", null),//pour le projet ETU ??
  SCENARIO(CruePrefix.P_SCENARIO, PROJET),
  MODELE(CruePrefix.P_MODELE, SCENARIO),
  SOUS_MODELE(CruePrefix.P_SS_MODELE, MODELE);

  public static EnumCatEMH getCatEMH(final CrueLevelType in) {
    if (SCENARIO.equals(in)) {
      return EnumCatEMH.SCENARIO;
    }
    if (MODELE.equals(in)) {
      return EnumCatEMH.MODELE;
    }
    if (SOUS_MODELE.equals(in)) {
      return EnumCatEMH.SOUS_MODELE;
    }
    return null;
  }
  private final String prefix;
  private final CrueLevelType parent;

  CrueLevelType(final String prefix, final CrueLevelType parent) {
    this.prefix = prefix;
    this.parent = parent;
  }

  public String getPrefix() {
    return this.prefix;
  }

  public CrueLevelType getParent() {
    return this.parent;
  }

  public CrueLevelType getChild() {
    switch (this) {
      case PROJET: {
        return SCENARIO;
      }
      case SCENARIO: {
        return MODELE;
      }
      case MODELE: {
        return SOUS_MODELE;
      }
    }

    return null;
  }
}
