/**
 *
 */
package org.fudaa.dodico.crue.metier.transformer;

import java.util.HashMap;
import java.util.Map;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformerDelegate;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;

/**
 * Un transformer qui délègue la transformer a des specifiques ou a un default.
 *
 * @author deniger
 */
public class ToStringTransformerEMHDelegate implements ToStringTransformer, ToStringTransformerDelegate {

  private final ToStringTransformer defaultTransformer;
  private final ToStringTransformerFromTransformable fromTransformable;
  private String nullString = org.apache.commons.lang3.StringUtils.EMPTY;
  final Map<Class, ToStringTransformer> specificClassTransformers = new HashMap<>();
  final Map<Class, ToStringTransformer> specificInstanceOfTransformers = new HashMap<>();

  /**
   * @param crueProperties
   */
  public ToStringTransformerEMHDelegate(CrueConfigMetier crueProperties, DecimalFormatEpsilonEnum type) {
    this(null, crueProperties, type);
  }

  /**
   * @param defaultTransformer
   */
  public ToStringTransformerEMHDelegate(ToStringTransformer defaultTransformer, CrueConfigMetier crueProperties, DecimalFormatEpsilonEnum type) {
    super();
    this.defaultTransformer = defaultTransformer == null ? ToStringTransformerDefault.TO_STRING : defaultTransformer;
    fromTransformable = new ToStringTransformerFromTransformable(crueProperties, type);
  }

  /**
   * Prioritaire sur le instanceof.
   *
   * @param c
   * @param transformer à utiliser si l'objet est de la classe c
   */
  public void addTransformerClass(Class c, ToStringTransformer transformer) {
    specificClassTransformers.put(c, transformer);
  }

  /**
   * @param c
   * @param transformer à utiliser si l'objet est de l'instance de c
   */
  public void addTransformerInstanceOf(Class c, ToStringTransformer transformer) {
    specificInstanceOfTransformers.put(c, transformer);
  }

  @SuppressWarnings("unchecked")
  private ToStringTransformer findFromInstanceOf(Object in) {
    for (Map.Entry<Class, ToStringTransformer> instance : specificInstanceOfTransformers.entrySet()) {
      if (instance.getKey().isAssignableFrom(in.getClass())) {
        return instance.getValue();
      }
    }
    return null;
  }

  /**
   * @return the nullString
   */
  public String getNullString() {
    return nullString;
  }

  @Override
  public ToStringTransformer getTransformer(Object in) {
    ToStringTransformer transformerToUse = specificClassTransformers.get(in.getClass());
    if (transformerToUse != null) {
      return transformerToUse;
    }
    if (in instanceof ToStringTransformable) {
      return fromTransformable;
    }
    transformerToUse = findFromInstanceOf(in);
    if (transformerToUse == null) {
      transformerToUse = defaultTransformer;
    }
    return transformerToUse;
  }

  /**
   * @param nullString the nullString to set
   */
  public void setNullString(String nullString) {
    this.nullString = nullString;
  }

  @Override
  public String transform(Object in) {
    if (in == null) {
      return nullString;
    }
    ToStringTransformer transformerToUse = getTransformer(in);
    return transformerToUse.transform(in);
  }
}
