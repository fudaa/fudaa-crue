/**
 * *********************************************************************
 * Module: RelationEMHSectionDansBrancheSaintVenant.java Author: deniger Purpose: Defines the Class RelationEMHSectionDansBrancheSaintVenant
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RelationEMHSectionDansBrancheSaintVenant extends RelationEMHSectionDansBranche {
  public static final String PROP_COEFPOND = "coefPond";
  public static final String PROP_COEFCONV = "coefConv";
  public static final String PROP_COEFDIV = "coefDiv";
  private double coefPond;
  private double coefConv;
  private double coefDiv;

  public RelationEMHSectionDansBrancheSaintVenant(CrueConfigMetier defaults) {
    coefPond = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_COEF_POND);
    coefConv = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_COEF_CONV);
    coefDiv = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_COEF_DIV);
  }
  public RelationEMHSectionDansBrancheSaintVenant(RelationEMHSectionDansBrancheSaintVenant other) {
    coefPond = other.coefPond;
    coefConv = other.coefConv;
    coefDiv = other.coefDiv;
  }

  @PropertyDesc(i18n = "coefDiv.property")
  public double getCoefDiv() {
    return coefDiv;
  }

  public RelationEMHSectionDansBrancheSaintVenant cloneValuesOnly() {
    RelationEMHSectionDansBrancheSaintVenant cloned = null;
    try {
      cloned = clone();
    } catch (CloneNotSupportedException ex) {
      Logger.getLogger(RelationEMHSectionDansBrancheSaintVenant.class.getName()).log(Level.SEVERE, null, ex);
    }
    return cloned;
  }

  @Override
  public RelationEMHSectionDansBrancheSaintVenant clone() throws CloneNotSupportedException {
    final RelationEMHSectionDansBrancheSaintVenant cloned = (RelationEMHSectionDansBrancheSaintVenant) super.clone();
    return cloned;
  }

  /**
   * @return une map contenant les données spécifiques de la branche. Utile pour les relatiosn de saint-venant
   */
  @Override
  public Map getAdditionalData() {
    Map res = new HashMap();
    res.put(PROP_COEFCONV, getCoefConv());
    res.put(PROP_COEFDIV, getCoefDiv());
    res.put(PROP_COEFPOND, getCoefPond());
    return res;
  }

  @Override
  public void initAdditionnalData(Map in) {
    if (in == null) {
      return;
    }
    Double newCoefConv = (Double) in.get(PROP_COEFCONV);
    Double newCoefDiv = (Double) in.get(PROP_COEFDIV);
    Double newCoefPond = (Double) in.get(PROP_COEFPOND);
    if (newCoefConv != null) {
      this.coefConv = newCoefConv;
    }
    if (newCoefDiv != null) {
      this.coefDiv = newCoefDiv;
    }
    if (newCoefPond != null) {
      this.coefPond = newCoefPond;
    }
  }

  public void initCoefFrom(RelationEMHSectionDansBrancheSaintVenant saintVenant) {
    setCoefConv(saintVenant.getCoefConv());
    setCoefDiv(saintVenant.getCoefDiv());
    setCoefPond(saintVenant.getCoefPond());
  }

  public void setCoefDiv(final double newCoefDiv) {
    coefDiv = newCoefDiv;
  }

  @PropertyDesc(i18n = "coefPond.property")
  public double getCoefPond() {
    return coefPond;
  }

  /**
   * @param newCoefPond
   */
  public void setCoefPond(final double newCoefPond) {
    coefPond = newCoefPond;
  }

  @PropertyDesc(i18n = "coefConv.property")
  public double getCoefConv() {
    return coefConv;
  }

  public void setCoefConv(final double newCoefConv) {
    coefConv = newCoefConv;
  }

  @Override
  public RelationEMHSectionDansBrancheSaintVenant copyWithEMH(CatEMHSection copiedEMH) {
    RelationEMHSectionDansBrancheSaintVenant res = new RelationEMHSectionDansBrancheSaintVenant(this);
    res.setEmh(copiedEMH);
    res.setXp(getXp());
    res.setPos(getPos());
    return res;
  }
}
