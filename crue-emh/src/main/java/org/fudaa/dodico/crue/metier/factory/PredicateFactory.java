package org.fudaa.dodico.crue.metier.factory;

import org.apache.commons.collections4.Predicate;
import org.fudaa.dodico.crue.metier.emh.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author deniger Conteneur des predicate.
 */
public final class PredicateFactory {
  /**
   *
   */
  public static final Predicate PREDICATE_IS_USER_ACTIVATED = new PredicateIsUserActive();
  public static final Predicate PREDICATE_EMH_ACTUALLY_ACTIVE = new PredicateEMHActuallyActive();
  public static final Predicate PREDICATE_EMH_ACTUALLY_ACTIVE_FOR_CALCUL = new PredicateEMHActuallyActiveForCaclul();

  private PredicateFactory() {
  }

  /**
   * Un predicate pour retrouver objet d'une classe donne.
   *
   * @author deniger
   */
  public static class PredicateClass implements Predicate {
    private final Class info;

    /**
     * @param info info
     */
    public PredicateClass(final Class info) {
      super();
      this.info = info;
    }

    @Override
    public boolean evaluate(final Object object) {
      return object != null && info.equals(object.getClass());
    }
  }

  /**
   * Predicate pour les EMH activable.
   *
   * @author deniger
   */
  public static class PredicateIsUserActive implements Predicate {
    @Override
    public boolean evaluate(Object object) {
      return object != null && ((ObjetUserActivable) object).getUserActive();
    }
  }

  /**
   * Predicate pour les EMH actuallyActive
   *
   * @author deniger
   */
  public static class PredicateEMHActuallyActive implements Predicate {
    @Override
    public boolean evaluate(Object object) {
      return object != null && ((EMH) object).getActuallyActive();
    }
  }

  /**
   * Predicate pour les EMH actuallyActive et prend en compte le cas des sections et chargement des calculs.
   * Une section est vue comme active dans Fudaa-Crue si référencée par une section idem active.
   * Dans les calculs ce n'est pas le cas.
   * @see  CatEMHSection#getActuallyActive(boolean)
   *
   * @author deniger
   */
  public static class PredicateEMHActuallyActiveForCaclul implements Predicate {
    @Override
    public boolean evaluate(Object object) {
      EMH emh = (EMH) object;
      if (emh == null) {
        return false;
      }
      if (EnumCatEMH.SECTION.equals(emh.getCatType())) {
        return ((CatEMHSection) emh).getActuallyActive(true);
      }
      return emh.getActuallyActive();
    }
  }

  /**
   * Un predicate pour retrouver un type d'EMH.
   *
   * @author deniger
   */
  public static class PredicateEnumInfo implements Predicate {
    private final EnumInfosEMH info;

    /**
     * @param info l'infos recherchee
     */
    public PredicateEnumInfo(final EnumInfosEMH info) {
      super();
      this.info = info;
    }

    @Override
    public boolean evaluate(final Object object) {
      if (info == null) {
        return true;
      }
      return info.equals(((InfosEMH) object).getCatType());
    }
  }

  /**
   * Un predicate pour retrouver objet d'une classe donne.
   *
   * @author deniger
   */
  public static class PredicateInstanceOf implements Predicate {
    private final Class info;

    /**
     * @param info recherchée
     */
    public PredicateInstanceOf(final Class info) {
      super();
      this.info = info;
    }

    @Override
    public boolean evaluate(final Object object) {
      return object != null && info.isAssignableFrom(object.getClass());
    }
  }

  /**
   * A utiliser avec RelationEMHContient afin de récupérer les RelationEMHSectionDansBranche dont l'EMH est active
   *
   * @author deniger
   */
  public static class PredicateRelationEMHBrancheActiveContientSection implements Predicate {
    final boolean active;

    /**
     * @param active si la branche contenant doit active ou non.
     */
    public PredicateRelationEMHBrancheActiveContientSection(boolean active) {
      this.active = active;
    }

    @Override
    public boolean evaluate(final Object object) {
      if (object == null) {
        return false;
      }
      if (object instanceof RelationEMHBrancheContientSection) {
        return active == (((RelationEMHBrancheContientSection) object)
            .getEmh().getActuallyActive());
      }
      return false;
    }
  }

  /**
   * A utiliser avec RelationEMHContient afin de récupérer les relation d'un type donne et si voulu filtrer sue le type d'EMH en relation.
   *
   * @author deniger
   */
  public static class PredicateRelationEMHContientEMHOfClass implements Predicate {
    private final Class<? extends EMH> type;
    private final Set<Class<? extends RelationEMH>> typeOfRelation;

    /**
     * @param typeOfRelatedEMH le type recherche.Si null renvoie true pour toutes les relation RelationEMHContient
     * @param classesOfRelation les classes des relations recherchées.
     */
    public PredicateRelationEMHContientEMHOfClass(final Class<? extends EMH> typeOfRelatedEMH,
                                                  final Class<? extends RelationEMH>... classesOfRelation) {
      super();
      this.type = typeOfRelatedEMH;
      typeOfRelation = new HashSet<>(Arrays.asList(classesOfRelation));
    }

    public PredicateRelationEMHContientEMHOfClass(final Class<? extends RelationEMH>... classesOfRelation) {
      this(null, classesOfRelation);
    }

    @Override
    public boolean evaluate(final Object object) {
      if (object == null) {
        return false;
      }
      if (typeOfRelation.contains(object.getClass())) {
        if (type == null) {
          return true;
        }
        final RelationEMH rel = (RelationEMH) object;
        if (rel.getEmh() != null && type.equals(rel.getEmh().getClass())) {
          return true;
        }
      }
      return false;
    }
  }

  /**
   * A utiliser avec RelationEMHContient afin de récupérer les EMH avec un nom donnée.
   *
   * @author deniger
   */
  public static class PredicateRelationEMHContientId implements Predicate {
    private final Class c;
    private final String ref;
    private final EnumCatEMH typeEMH;

    /**
     * @param refId la reference recherchee.Si null renvoie true pour toutes les relation RelationEMHContient
     * @param classOfRelation la classe de la relation voulue
     */
    public PredicateRelationEMHContientId(final String refId, final Class classOfRelation) {
      this(refId, classOfRelation, null);
    }

    /**
     * @param ref la reference recherchee.Si null renvoie true pour toutes les relation RelationEMHContient
     * @param classOfRelation
     * @param typeEMH le type de l'EMH voulue
     */
    public PredicateRelationEMHContientId(final String ref, final Class classOfRelation, final EnumCatEMH typeEMH) {
      super();
      this.ref = ref == null ? ref : ref.toUpperCase();
      this.c = classOfRelation;
      this.typeEMH = typeEMH;
    }

    @Override
    public boolean evaluate(final Object object) {
      if (object == null) {
        return false;
      }
      if (c != null && !c.equals(object.getClass())) {
        return false;
      }
      final RelationEMH rel = (RelationEMH) object;
      final EMH emh = rel.getEmh();
      if (emh == null) {
        return false;
      }
      if (typeEMH != null && !typeEMH.equals(emh.getCatType())) {
        return false;
      }
      return ref == null || ref.equals(emh.getId());
    }
  }

  /**
   * A utiliser avec RelationEMHContient afin de récupérer les EMH avec un type donne.
   *
   * @author deniger
   */
  public static class PredicateRelationEMHContientType implements Predicate<Object> {
    private final EnumCatEMH type;

    /**
     * @param type le type recherche.Si null renvoie true pour toutes les relation RelationEMHContient
     */
    public PredicateRelationEMHContientType(final EnumCatEMH type) {
      super();
      this.type = type;
    }

    @Override
    public boolean evaluate(final Object object) {
      if (object == null) {
        return false;
      }
      if (object.getClass().equals(RelationEMHContient.class)) {
        if (type == null) {
          return true;
        }
        final RelationEMHContient rel = (RelationEMHContient) object;

        if (rel.getEmh() != null && type.equals(rel.getEmh().getCatType())) {
          return true;
        }
      }
      return false;
    }
  }

  /**
   * A utiliser avec RelationEMHContient afin de récupérer les RelationEMHSectionDansBranche dont la position est du type donné.
   *
   * @author deniger
   */
  public static class PredicateRelationEMHSectionDansBranche implements Predicate {
    final EnumPosSection position;

    /**
     * @param position le type recherche.Si null renvoie true pour toutes les relation RelationEMHContient
     */
    public PredicateRelationEMHSectionDansBranche(final EnumPosSection position) {
      this.position = position;
    }

    @Override
    public boolean evaluate(final Object object) {
      if (object == null) {
        return false;
      }
      if (object instanceof RelationEMHSectionDansBranche) {
        return position
            .equals(((RelationEMHSectionDansBranche) object).getPos());
      }
      return false;
    }
  }

  /**
   * utilise pour les relationEMH: renvoie true si l'EMH de destination est active.
   *
   * @author deniger
   */
  public static class PredicateRelationWithEMHActivated implements Predicate {
    @Override
    public boolean evaluate(Object object) {
      return object != null && ((RelationEMH) object).getEmh().getUserActive();
    }
  }

  /**
   * Un predicate pour retrouver un type d'EMH.
   *
   * @author deniger
   */
  public static class PredicateTypeEnum implements Predicate {
    private final EnumCatEMH cat;

    /**
     * @param info l'info recherchee
     */
    public PredicateTypeEnum(final EnumCatEMH info) {
      this.cat = info;
    }

    @Override
    public boolean evaluate(final Object object) {
      if (cat == null) {
        return true;
      }
      return object!=null && cat.equals(((EMH) object).getCatType());
    }
  }

  public static class PredicateTypeEnumArray implements Predicate {
    private final Set<EnumCatEMH> cats;

    /**
     * @param info les infos recherchees
     */
    public PredicateTypeEnumArray(final EnumCatEMH... info) {
      this.cats = new HashSet<>(Arrays.asList(info));
    }

    public PredicateTypeEnumArray(final Collection<EnumCatEMH> info) {
      this.cats = new HashSet<>(info);
    }

    @Override
    public boolean evaluate(final Object object) {
      if (cats == null) {
        return true;
      }
      final EnumCatEMH readCat = ((EMH) object).getCatType();
      return cats.contains(readCat);
    }
  }

  public static class PredicateSubTypeArray implements Predicate {
    private final Set<Object> cats;

    /**
     * @param info les infos recherchees
     */
    public PredicateSubTypeArray(final Object... info) {
      this.cats = new HashSet<>(Arrays.asList(info));
    }

    public PredicateSubTypeArray(final Collection<Object> info) {
      this.cats = new HashSet<>(info);
    }

    @Override
    public boolean evaluate(final Object object) {
      if (cats == null) {
        return true;
      }
      final Object readCat = ((EMH) object).getSubCatType();
      return cats.contains(readCat);
    }
  }
}
