package org.fudaa.dodico.crue.metier.comparator;

import java.util.Comparator;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

/**
 * A comparator for ObjetNomme
 *
 * @author deniger
 */
public class ObjetNommeByIdComparator implements Comparator<ObjetWithID> {

  /**
   * Comparator singleton.
   */
  public static final ObjetNommeByIdComparator INSTANCE = new ObjetNommeByIdComparator();

  private ObjetNommeByIdComparator() {
  }

  @Override
  public int compare(ObjetWithID o1, ObjetWithID o2) {
    if (o1 == o2) {
      return 0;
    }
    if (o1 == null) {
      return -1;
    }
    if (o2 == null) {
      return 1;
    }
    int compareString = compareString(o1.getId(), o2.getId());
    return compareString == 0 ? o1.hashCode() - o2.hashCode() : compareString;
  }

  public static int compareString(String o1, String o2) {
    if (o1 == o2) {
      return 0;
    }
    if (o1 == null) {
      return -1;
    }
    if (o2 == null) {
      return 1;
    }
    return o1.compareTo(o2);
  }
}
