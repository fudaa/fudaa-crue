package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;

public class DonPrtCIniBrancheOrifice extends DonPrtCIniBranche {

  private double ouv;
  private EnumSensOuv sensOuv;

  /**
   * @param def
   */
  public DonPrtCIniBrancheOrifice(final CrueConfigMetier def) {
    super(def);
    init(def);
  }

  private void init(final CrueConfigMetier def) {
    ouv = def.getDefaultDoubleValue("ouv");
    final String enumValue = def.getDefaultStringEnumValue("sensOuv");
    if (enumValue != null) {
      try {
        sensOuv = EnumSensOuv.valueOf(enumValue);
      } catch (final Exception e) {
      }
    }
    if (sensOuv == null) {
      sensOuv = EnumSensOuv.OUV_VERS_HAUT;
    }
  }

  @PropertyDesc(i18n = "ouv.property")
  public final double getOuv() {
    return ouv;
  }

  @Override
  public void initFrom(final DonPrtCIniBranche donPrtCIniBranche) {
    super.initFrom(donPrtCIniBranche);
    if (donPrtCIniBranche instanceof DonPrtCIniBrancheOrifice) {
      ouv = ((DonPrtCIniBrancheOrifice) donPrtCIniBranche).ouv;
      sensOuv = ((DonPrtCIniBrancheOrifice) donPrtCIniBranche).sensOuv;
    }
  }

  /**
   * @param newOuv
   */
  public final void setOuv(final double newOuv) {
    ouv = newOuv;
  }

  @PropertyDesc(i18n = "sensOuv.property")
  public final EnumSensOuv getSensOuv() {
    return sensOuv;
  }

  /**
   * @param newSensOuv
   */
  public final void setSensOuv(final EnumSensOuv newSensOuv) {
    sensOuv = newSensOuv;
  }
}
