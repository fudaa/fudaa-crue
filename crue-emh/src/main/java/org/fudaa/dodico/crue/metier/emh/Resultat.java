package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;

public class Resultat implements ToStringTransformable {

  private boolean sortieFichier;

  /**
   * 
   */
  public Resultat() {
    sortieFichier = true;

  }

  /**
   * @return the sortieFichier
   */
  @PropertyDesc(i18n="sortieFichier.property")
  public boolean getSortieFichier() {
    return sortieFichier;
  }

  /**
   * @param sortieFichier the sortieFichier to set
   */
  public void setSortieFichier(boolean sortieFichier) {
    this.sortieFichier = sortieFichier;
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    if (EnumToString.OVERVIEW.equals(format)) {
      return getClass().getSimpleName();
    }
    return getClass().getSimpleName() + " [sortieFichier=" + sortieFichier + "]";
  }

  public void initWith(Resultat avancement) {
    setSortieFichier(avancement.getSortieFichier());
  }
}
