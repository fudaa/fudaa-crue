package org.fudaa.dodico.crue.metier.etude;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.metier.CrueFileType;

import java.io.File;
import java.util.List;

/**
 * Gere les fichiers Crue definis dans le fichier xml ETU. un fichier est defini par: son id unique son type le chemin
 * vers ce fichier. Sert de base de recherche pour lancer a la demande les differents scenarios.
 *
 * @author Adrien Hadoux
 */
public class FichierCrue implements Cloneable, ObjetNomme, Comparable<FichierCrue> {
  public static final String CURRENT_PATH = ".\\";
  private String nom;
  /**
   * le type du fichier. Tous les types sont dispo via la classe CrueDaoFactoryCommom.
   */
  private CrueFileType type;
  private String cheminRelatif;

  public FichierCrue(final String nom, final String path, final CrueFileType type) {
    super();
    this.nom = nom;
    assert nom != null;
    this.cheminRelatif = path;
    if (isBlankChemin(path)) {
      this.cheminRelatif = null;
    }
    this.type = type;
  }

  @Override
  public int compareTo(FichierCrue other) {
    if (other == null) {
      return 1;
    }
    if (other == this || other.equals(this)) {
      return 0;
    }
    int res = nom.compareTo(other.nom);
    if (res == 0) {
      res = cheminRelatif.compareTo(other.cheminRelatif);
    }
    if (res == 0) {
      res = type.compareTo(other.type);
    }
    return res;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    FichierCrue that = (FichierCrue) o;

    if (nom != null ? !nom.equals(that.nom) : that.nom != null) {
      return false;
    }
    if (type != that.type) {
      return false;
    }
    return cheminRelatif != null ? cheminRelatif.equals(that.cheminRelatif) : that.cheminRelatif == null;
  }

  @Override
  public int hashCode() {
    int result = nom != null ? nom.hashCode() : 0;
    result = 31 * result + (type != null ? type.hashCode() : 0);
    result = 31 * result + (cheminRelatif != null ? cheminRelatif.hashCode() : 0);
    return result;
  }

  private static boolean isBlankChemin(final String path) {
    return StringUtils.isBlank(path) || ".\\".equals(path) || "\\".equals(path) || ".".equals(path);
  }

  /**
   * Retrouve le bon fichier par sa reference.
   *
   * @param nomRef
   * @param listeFiles
   */
  public static FichierCrue findById(final String nomRef, final List<FichierCrue> listeFiles) {
    if (listeFiles != null) {
      for (final FichierCrue f : listeFiles) {
        if (f.nom.equalsIgnoreCase(nomRef)) {
          return f;
        }
      }
    }
    return null;
  }

  @Override
  public String getId() {
    return getNom();
  }

  @Override
  public FichierCrue clone() {
    return new FichierCrue(getNom(), getPath(), getType());
  }

  /**
   * Retourne le fichier qui contient les infos.
   */
  public File getProjectFile(EMHProjet proj) {
    return getProjectFile(proj.getDirOfFichiersEtudes());
  }

  /**
   * @param baseDir
   * @return the file using dir as the base directory
   */
  public File getProjectFile(File baseDir) {
    if (nom == null) {
      return null;
    }
    final File dir = CrueFileHelper.getCanonicalBaseDir(baseDir, cheminRelatif);
    if (baseDir == null) {
      return null;
    }
    return new File(dir, nom);
  }

  @Override
  public String getNom() {
    return nom;
  }

  @Override
  public void setNom(final String nom) {
    this.nom = nom;
  }

  public CrueFileType getType() {
    return type;
  }

  public void setType(final CrueFileType type) {
    this.type = type;
  }

  public String getPath() {
    return cheminRelatif;
  }

  public void setPath(final String path) {
    this.cheminRelatif = path;
  }
}
