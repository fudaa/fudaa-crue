package org.fudaa.dodico.crue.metier.comparator;

import org.fudaa.dodico.crue.common.SafeComparator;

/**
 *
 * @author deniger
 */
public class EnumComparator extends SafeComparator<Enum> {

  @Override
  protected int compareSafe(Enum o1, Enum o2) {
    return compareString(o1.name(), o2.name());
  }
}
