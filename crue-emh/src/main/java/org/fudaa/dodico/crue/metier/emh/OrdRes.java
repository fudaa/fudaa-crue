package org.fudaa.dodico.crue.metier.emh;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.OrdResKey;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;

public abstract class OrdRes implements Cloneable, ToStringTransformable {

  /**
   * contient les Dde par variable
   */
  TreeMap<String, Dde> values = new TreeMap<>();

  @Override
  public Object clone() throws CloneNotSupportedException {
    OrdRes res = (OrdRes) super.clone();
    res.values = (TreeMap<String, Dde>) values.clone();
    return res;
  }

  @UsedByComparison
  public abstract OrdResKey getKey();

  public void setValues(Collection<Dde> inputs) {
    values.clear();
    for (Dde dde : inputs) {
      values.put(dde.getNom(), dde);
    }
  }

  /**
   *
   * @param reference
   * @return true si modification.
   */
  public boolean synchronizeValuesFrom(OrdRes reference) {
    if (reference == null) {
      return false;
    }
    Set<String> unknownVariables = new HashSet<>(values.keySet());
    unknownVariables.removeAll(reference.values.keySet());
    boolean modified = false;
    if (!unknownVariables.isEmpty()) {
      for (String unknown : unknownVariables) {
        values.remove(unknown);
      }
      modified = true;
    }
    for (Entry<String, Dde> entryInRef : reference.values.entrySet()) {
      if (!values.containsKey(entryInRef.getKey())) {
        modified = true;
        values.put(entryInRef.getKey(), entryInRef.getValue());
      }

    }
    return modified;
  }

  /**
   *
   * @param variableName le nom de la variable commencant par une minuscule
   * @return
   */
  public Dde getDde(String variableName) {
    return values.get(variableName);
  }

  public boolean getDdeValue(String variableName) {
    Dde dde = getDde(variableName);
    return dde == null ? false : dde.getValeur();
  }

  public void setDdeValue(String variableName, boolean newValue) {
    Dde dde = getDde(variableName);
    assert dde != null;//test useful ? all ddes must be defined
    if(dde instanceof DdeMultiple){
      values.put(variableName, new DdeMultiple(variableName, newValue));
    }else{
      values.put(variableName, new Dde(variableName, newValue));
    }
  }

  @Visibility(ihm = false)
  public List<Dde> getDdes() {
    return new ArrayList<>(values.values());
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    if (EnumToString.OVERVIEW.equals(format)) {
      return getClass().getSimpleName();
    }
    return toString();
  }

  public List<String> getAllVariables() {
    return new ArrayList<>(values.keySet());
  }

  @UsedByComparison
  public List<String> getActiveOrdResDisplayName() {
    Collection<Dde> ddes = values.values();
    ArrayList<String> res = new ArrayList<>();
    for (Dde dde : ddes) {
      if (dde.isValeur()) {
        res.add(dde.getDisplayName());
      }
    }
    return res;
  }
}
