package org.fudaa.dodico.crue.metier.comparator;

import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;

/**
 *
 * @author deniger
 */
public class DoubleComparator extends SafeComparator<Double> {

  private final PropertyEpsilon epsilon;

  public DoubleComparator(PropertyEpsilon epsilon) {
    this.epsilon = epsilon;
  }

  @Override
  protected int compareSafe(Double o1, Double o2) {
    double x1 = o1;
    double x2 = o2;
    if (epsilon.isSame(x1, x2)) {
      return 0;
    }
    return x1 > x2 ? 1 : -1;
  }
}
