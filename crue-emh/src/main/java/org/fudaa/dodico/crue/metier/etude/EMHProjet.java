package org.fudaa.dodico.crue.metier.etude;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Gere un projet EMH. Est construit a partir du fichier XML ETU.
 *
 * @author Adrien Hadoux
 */
public class EMHProjet {
    public static final Logger LOGGER = Logger.getLogger(EMHProjet.class.getName());
    /**
     * Base des scenarios disponible pour tout le projet.
     */
    private final List<ManagerEMHScenario> baseScenarios = new ArrayList<>();
    private final List<ManagerEMHModeleBase> listeModele = new ArrayList<>();
    private final List<ManagerEMHModeleBase> externListModele = Collections.unmodifiableList(listeModele);
    private final List<ManagerEMHSousModele> listeSousModele = new ArrayList<>();
    private final Map<String, ManagerEMHModeleBase> idModele = new HashMap<>();
    private final Map<String, ManagerEMHScenario> idScenario = new HashMap<>();
    private final Map<String, ManagerEMHSousModele> idSousModele = new HashMap<>();
    private final List<ManagerEMHScenario> externScenario = Collections.unmodifiableList(baseScenarios);
    private final List<ManagerEMHSousModele> externSousModele = Collections.unmodifiableList(listeSousModele);
    /**
     * Infos du projet, contient la base des fichiers de donnees.
     */
    private EMHProjectInfos baseRessourcesInfos;
    private CoeurConfigContrat propDefinition;
    /**
     * Scenario courant du projet. Attention peut etre null.
     */
    private ManagerEMHScenario scenarioCourant;

    public EMHProjet() {
    }

    /**
     * Copie du fichier en paramètre, de sourceProject vers destProject. Le fichier est également ajouté logiquement dans destProject
     *
     * @param fichier          le fichier à copier
     * @param destName         nom de destination
     * @param overwriteIfExist true si le fichier doit-être écraser s'il existe, false sinon
     * @param sourceProject    l'étude source de la copie
     * @param destProject      l'étude destination de la copie
     * @return le nouveau FichierCrue créé
     */
    public static FichierCrue copyFile(FichierCrue fichier, String destName, boolean overwriteIfExist, EMHProjet sourceProject, EMHProjet destProject) {
        File srcFile = fichier.getProjectFile(sourceProject);
        FichierCrue destFichier = new FichierCrue(destName, fichier.getPath(), fichier.getType());
        boolean ok = destProject.getInfos().addCrueFileToProject(destFichier);

        if (ok && srcFile.exists()) {
            File destFile = destFichier.getProjectFile(destProject);
            if (destFile.equals(srcFile)) {
                return destFichier;
            }
            if (destFile.exists() && !overwriteIfExist) {
                return destFichier;
            }
            if (CtuluLibFile.copyFile(srcFile, destFile)) {
                CrueFileHelper.setLastModified(destFile, srcFile, EMHProjet.class);
                return destFichier;
            } else {
                destProject.getInfos().removeFile(destFichier);
                return null;
            }
        }

        return null;
    }

    public File getParentDirOfEtuFile() {
        return getInfos().getParentDirOfEtuFile();
    }

    public void setEtuFile(File etuFile) {
        if (getInfos() == null) {
            baseRessourcesInfos = new EMHProjectInfos();
        }
        //pas de changement
        if (getInfos().getEtuFile() != null && getInfos().getEtuFile().equals(etuFile)) {
            return;
        }
        getInfos().setEtuFile(etuFile);
        scanToIncludeLHPTFiles();
    }

    public void scanToIncludeLHPTFiles() {
        if (includeLHPT()) {
            sortBaseFichierProjet();
        }
    }

    /**
     *
     */
    public void sortLists() {
        Collections.sort(listeModele, ObjetNommeByNameComparator.INSTANCE);
        Collections.sort(listeSousModele, ObjetNommeByNameComparator.INSTANCE);
        Collections.sort(baseScenarios, ObjetNommeByNameComparator.INSTANCE);
        sortBaseFichierProjet();
    }

    private void sortBaseFichierProjet() {
        if (getInfos().baseFichiersProjets != null) {
            Collections.sort(getInfos().baseFichiersProjets, ObjetNommeByNameComparator.INSTANCE);
        }
    }

    private boolean includeLHPT() {
        //lhpt: on enleve tous les fichiers
        final List<FichierCrue> allLHPTFiles = getInfos().baseFichiersProjets.stream().filter(fichierCrue -> (fichierCrue.getType().equals(CrueFileType.LHPT)))
                .collect(Collectors.toList());
        getInfos().baseFichiersProjets.removeAll(allLHPTFiles);

        for (ManagerEMHScenario baseScenario : baseScenarios) {
            final FichierCrueManager listeFichiers = baseScenario.getListeFichiers();
            if (listeFichiers != null) {
                listeFichiers.removeFile(listeFichiers.getFile(CrueFileType.LHPT));
            }
        }
        FichierLHPTSupport support = new FichierLHPTSupport(this);
        final Collection<FichierCrue> lhptFiles = support.getFichierCrueList();
        boolean modified = !lhptFiles.isEmpty();
        if (modified) {
            getInfos().baseFichiersProjets.addAll(lhptFiles);
            for (ManagerEMHScenario baseScenario : baseScenarios) {
                final FichierCrueManager listeFichiers = baseScenario.getListeFichiers();
                FichierCrue fichierCrue = support.getForScenario(baseScenario);
                if (fichierCrue != null && !listeFichiers.contains(CrueFileType.LHPT)) {
                    listeFichiers.addFile(fichierCrue);
                }
            }
        }
        return modified;
    }

    public void addBaseModele(final ManagerEMHModeleBase modele) {
        ManagerEMHModeleBase old = idModele.put(modele.getId(), modele);
        CollectionCrueUtil.addInList(listeModele, modele, old);
    }

    public void addBaseSousModele(final ManagerEMHSousModele sousModele) {
        ManagerEMHSousModele old = idSousModele.put(sousModele.getId(), sousModele);
        CollectionCrueUtil.addInList(listeSousModele, sousModele, old);
    }

    public void addScenario(final ManagerEMHScenario scenario) {
        ManagerEMHScenario old = this.idScenario.put(scenario.getId(), scenario);
        CollectionCrueUtil.addInList(this.baseScenarios, scenario, old);
    }

    /**
     * @param id   sera mis en upperCase
     * @param type
     * @return le container correspondant au nom et au type.
     */
    public ManagerEMHContainerBase getContainer(final String id, CrueLevelType type) {
        if (CrueLevelType.SOUS_MODELE.equals(type)) {
            return getSousModele(id);
        } else if (CrueLevelType.MODELE.equals(type)) {
            return getModele(id);
        } else if (CrueLevelType.SCENARIO.equals(type)) {
            return getScenario(id);
        }
        return null;
    }

    public File getDirForRun(final ManagerEMHScenario sc, final EMHRun runCourant) {
        if (runCourant == null) {
            return null;
        }
        File dir = getMainDirOfRuns(sc);
        dir = new File(dir, runCourant.getNom());
        return dir;
    }

    public File getMainDirOfRuns(final ManagerEMHScenario sc) {
        return new File(getInfos().getDirOfRuns(), sc.getNom());
    }

    /**
     * @return the dir that contains the dc and dh files.
     */
    public File getDirForRunModeleCrue9(final ManagerEMHScenario scenarioCrue9, final EMHRun run) {
        final File dirOfRun = getDirForRun(scenarioCrue9, run);
        final List<ManagerEMHModeleBase> fils = scenarioCrue9.getFils();
        if (fils.isEmpty()) {
            return null;
        } else if (fils.size() > 1) {
            return null;
        }
        final ManagerEMHModeleBase modele = fils.get(0);
        return new File(dirOfRun, modele.getNom());
    }

    public File getDirForRunModele(final ManagerEMHScenario scenarioCrue, ManagerEMHModeleBase modele, final EMHRun run) {
        final File dirOfRun = getDirForRun(scenarioCrue, run);
        return new File(dirOfRun, modele.getNom());
    }

    /**
     * @return l'association idFichier->File pour les fichier d'etude
     */
    public Map<String, URL> getEtudeScenarioURL(final String nomScenario) {
        final ManagerEMHScenario sc = getScenario(nomScenario);
        if (sc == null) {
            return Collections.emptyMap();
        }
        final Map<String, URL> res = new HashMap<>();
        try {
            for (final Map.Entry<String, FichierCrueParModele> entry : sc.getAllFileUsed().entrySet()) {
                res.put(entry.getKey(), entry.getValue().getFichier().getProjectFile(this).toURI().toURL());
            }
        } catch (final MalformedURLException e) {
            LOGGER.log(Level.SEVERE, "getEtudeFiles", e);
        }
        return res;
    }

    private Map<String, File> getFileEtudeFiles(final Map<String, FichierCrueParModele> allFileUsed) {
        final Map<String, File> res = new HashMap<>();
        for (final Map.Entry<String, FichierCrueParModele> entry : allFileUsed.entrySet()) {
            res.put(entry.getKey(), entry.getValue().getFichier().getProjectFile(this));
        }
        return res;
    }

    /**
     * utilise pour avoir l'arborescence dans un autre dossier. Permet de faire des sauvegarde
     *
     * @param allFileUsed les fichiers utilises
     * @param otherDir    le repertoire de destination
     */
    private Map<String, File> getFileEtudeFiles(final Map<String, FichierCrueParModele> allFileUsed, File otherDir) {
        final Map<String, File> res = new HashMap<>();
        for (final Map.Entry<String, FichierCrueParModele> entry : allFileUsed.entrySet()) {
            res.put(entry.getKey(), entry.getValue().getFichier().getProjectFile(otherDir));
        }
        return res;
    }

    /**
     * @param scenario le scenario
     * @param run      le run
     * @return l'association pour les fichiers resultats du run courant
     */
    public Map<String, RunFile> getRunFilesResultat(final ManagerEMHScenario scenario, EMHRun run) {
        final File dir = getDirForRun(scenario, run);
        if (dir != null) {
            return scenario.getRunFilesResultatIn(dir);
        }
        return null;
    }

    public EMHProjectInfos getInfos() {
        return baseRessourcesInfos;
    }

    public void setInfos(final EMHProjectInfos infos) {
        this.baseRessourcesInfos = infos;
    }

    public Map<String, File> getInputFiles(final ManagerEMHScenario sc) {
        if (sc == null) {
            return Collections.emptyMap();
        }
        return getFileEtudeFiles(sc.getAllFileUsed());
    }

    public Map<String, File> getInputFiles(final ManagerEMHScenario sc, File otherDir) {
        if (sc == null) {
            return Collections.emptyMap();
        }
        return getFileEtudeFiles(sc.getAllFileUsed(), otherDir);
    }

    /**
     * @param sc
     * @param run
     * @return the input files from the base dir of the project.
     */
    public Map<String, File> getInputFiles(final ManagerEMHScenario sc, EMHRun run) {
        if (sc == null) {
            return Collections.emptyMap();
        }
        if (run == null) {
            return getFileEtudeFiles(sc.getAllFileUsed());
        }
        return getRunFiles(sc.getAllFileUsed(), sc, run);
    }

    public List<ManagerEMHModeleBase> getListeModeles() {

        return externListModele;
    }

    public List<? extends ManagerEMHContainerBase> getListOfManager(CrueLevelType type) {
        if (CrueLevelType.SCENARIO.equals(type)) {
            return getListeScenarios();
        }
        if (CrueLevelType.MODELE.equals(type)) {
            return getListeModeles();
        }
        if (CrueLevelType.SOUS_MODELE.equals(type)) {
            return getListeSousModeles();
        }
        throw new IllegalAccessError(type + " not supported");
    }

    public List<ManagerEMHScenario> getListeScenarios() {
        return externScenario;
    }

    public Set<String> getAllRunNameIds() {
        Set<String> res = new HashSet<>();
        getListeScenarios().forEach(scenario -> scenario.getListeRuns().forEach(run -> res.add(run.getId())));
        return res;
    }

    public List<ManagerEMHSousModele> getListeSousModeles() {
        return externSousModele;
    }

    public ManagerEMHModeleBase getModele(final String id) {
        if (id == null) {
            return null;
        }
        return idModele.get(id.toUpperCase());
    }

    /**
     * @param dirOfRun
     * @param entry
     * @return the parent directory the entry. The scenario files
     */
    private File getParentDirInRun(final File dirOfRun, final Map.Entry<String, FichierCrueParModele> entry) {

        if (entry.getValue().getFichier().getType().isScenarioLevel()) {
            return dirOfRun;
        }
        final ManagerEMHModeleBase parent = entry.getValue().getParent();
        return parent == null ? dirOfRun : new File(dirOfRun, parent.getNom());
    }

    public File getDirOfFichiersEtudes() {
        return getInfos().getDirOfFichiersEtudes();
    }

    public CrueConfigMetier getPropDefinition() {
        return propDefinition.getCrueConfigMetier();
    }

    public void setPropDefinition(CoeurConfigContrat propDefinition) {
        this.propDefinition = propDefinition;
    }

    public CoeurConfigContrat getCoeurConfig() {
        return propDefinition;
    }

    /**
     * Ne fait aucun test pour savoir si le manager est utilise ou non
     *
     * @param manager
     */
    public boolean remove(ManagerEMHContainerBase manager) {
        final String key = manager.getId();
        if (this.idScenario.containsKey(key)) {
            this.baseScenarios.remove(manager);
            idScenario.remove(key);
            ManagerEMHScenario scenario = (ManagerEMHScenario) manager;
            if (getScenarioCourant() == scenario) {
                setScenarioCourant(null);
            }
            return true;
        }
        if (this.idModele.containsKey(key)) {
            this.listeModele.remove(manager);
            idModele.remove(key);
            return true;
        }
        if (this.idSousModele.containsKey(key)) {
            this.listeSousModele.remove(manager);
            idSousModele.remove(key);
            return true;
        }
        return false;
    }

    /**
     * Ne fait aucun test pour savoir si le fichier est encore utilise et ne supprime pas le fichier physique
     *
     * @param fichierCrue
     */
    public boolean removeFile(FichierCrue fichierCrue) {
        return getInfos().removeFile(fichierCrue);
    }

    /**
     * renomme uniquement les objets et déplace le fichier si nécessaire
     *
     * @param fichierCrue
     */
    public boolean renameFile(FichierCrue fichierCrue, String newName, boolean overwriteIfExist) {
        String oldName = fichierCrue.getNom();
        File oldFile = fichierCrue.getProjectFile(this);
        boolean ok = getInfos().renameFile(fichierCrue, newName);
        if (ok && oldFile.exists()) {
            File newFile = fichierCrue.getProjectFile(this);
            if (newFile.exists() && !overwriteIfExist) {
                return ok;
            }
            boolean copyFile = CtuluLibFile.copyFile(oldFile, newFile);
            if (copyFile) {
                CrueFileHelper.deleteFile(oldFile, getClass());
                return true;
            } else {
                getInfos().renameFile(fichierCrue, oldName);
                return false;
            }
        }
        return ok;
    }

    /**
     * renomme uniquement les objets et déplace le fichier si nécessaire
     *
     * @param fichierCrue
     */
    public boolean renameFile(FichierCrue fichierCrue, FichierCrue newName, boolean overwriteIfExist) {
        String oldName = fichierCrue.getNom();
        String oldPath = fichierCrue.getPath();
        File oldFile = fichierCrue.getProjectFile(this);
        boolean ok = getInfos().renameFile(fichierCrue, newName.getNom());
        if (ok && oldFile.exists()) {
            File newFile = newName.getProjectFile(this);
            //pour eviter de copier et supprimer le meme fichier...
            if (newFile.equals(oldFile)) {
                return ok;
            }
            if (newFile.exists() && !overwriteIfExist) {
                return ok;
            }
            fichierCrue.setPath(newName.getPath());
            boolean copyFile = CtuluLibFile.copyFile(oldFile, newFile);
            if (copyFile) {
                CrueFileHelper.setLastModified(newFile, oldFile, getClass());
                CrueFileHelper.deleteFile(oldFile, getClass());
                return true;
            } else {
                getInfos().renameFile(fichierCrue, oldName);
                fichierCrue.setPath(oldPath);
                return false;
            }
        }
        return ok;
    }

    public FichierCrue copyFile(FichierCrue fichier, String destName, boolean overwriteIfExist) {
        return copyFile(fichier, destName, overwriteIfExist, null);
    }

    public FichierCrue copyFile(FichierCrue fichier, String destName, boolean overwriteIfExist, CtuluLog log) {
        File srcFile = fichier.getProjectFile(this);
        if (destName == null) {
            log.addError("copyScenario.cantAddFileTargetNotDefined", fichier.getNom());
            return null;
        }
        FichierCrue destFichier = new FichierCrue(destName, fichier.getPath(), fichier.getType());
        if (!getInfos().acceptFile(destFichier.getNom())) {
            log.addError("copyScenario.cantAddFileInProjectNotAccepted", destFichier.getNom());
            return null;
        }
        boolean ok = getInfos().addCrueFileToProject(destFichier);
        if (!ok) {
            log.addError("copyScenario.cantAddFileInProjectAlreadyExists", destFichier.getNom());
            return null;
        }
        if (!srcFile.exists()) {
            log.addError("copyScenario.cantAddFileInProjectSrcNotFound", srcFile.getAbsolutePath(), destFichier.getNom());
            return null;
        }

        File destFile = destFichier.getProjectFile(this);
        if (destFile == null) {
            log.addError("copyScenario.cantAddFileInProjectStudyFolderNotConfigured", destFichier.getNom());
            return null;
        }
        if (destFile.equals(srcFile)) {
            return destFichier;
        }
        if (destFile.exists() && !overwriteIfExist) {
            return destFichier;
        }
        if (CtuluLibFile.copyFile(srcFile, destFile)) {
            CrueFileHelper.setLastModified(destFile, srcFile, getClass());
            return destFichier;
        } else {
            log.addError("copyScenario.cantAddFileInProjectFileCopyFailure", srcFile.getAbsolutePath(), destFichier.getNom());
            getInfos().removeFile(destFichier);
            return null;
        }
    }

    private <T extends ManagerEMHContainerBase> Map<String, T> getMapFor(EnumCatEMH type) {
        switch (type) {
            case SCENARIO:
                return (Map<String, T>) idScenario;
            case MODELE:
                return (Map<String, T>) idModele;
            case SOUS_MODELE:
                return (Map<String, T>) idSousModele;
            default:
                return null;
        }
    }

    /**
     * Pour renommer un container il faut passer par le RenameManager. Les methodes appelant cette méthode doivent donc venir du RenameManager.
     * Ne vérifie pas la cohérence. Pour les scenarios Crue9, s'ils sont utilisés par des scenarios crue10, on renomme.
     */
    public <T extends ManagerEMHContainerBase> boolean renameManager(EnumCatEMH type, ManagerEMHContainerBase manager,
                                                                     String newName) {
        //id pour chaque type:
        //ajouter support pour renommer les scenarios crue 9 utilisé
        Map<String, T> managerByNom = getMapFor(type);
        if (managerByNom == null) {
            LOGGER.log(Level.SEVERE, "not manager found for {0}", type);
            return false;
        }
        if (!managerByNom.containsKey(manager.getId())) {
            LOGGER.log(Level.INFO, "not renamed because {0} not exist", manager.getNom());
            return false;
        }
        String prefix = CruePrefix.getPrefix(type);
        if (!newName.startsWith(prefix)) {
            LOGGER.log(Level.WARNING, "not renamed because {0} has not the correct prefix", newName);
            return false;
        }
        if (managerByNom.containsKey(newName.toUpperCase())) {
            LOGGER.log(Level.INFO, "not renamed because {0} is already used ", newName.toUpperCase());
            return false;
        }
        if (EnumCatEMH.SCENARIO.equals(type) && getCoeurConfig().isCrue9Dependant()) {
            ManagerEMHScenario scenario = (ManagerEMHScenario) manager;
            if (scenario.isCrue9()) {
                Collection<ManagerEMHScenario> values = (Collection<ManagerEMHScenario>) managerByNom.values();
                for (ManagerEMHScenario otherScenario : values) {
                    if (otherScenario.isCrue10() && manager.getNom().equals(otherScenario.getLinkedscenarioCrue9())) {
                        otherScenario.setLinkedscenarioCrue9(newName);
                    }
                }
            }
        }
        T toModify = managerByNom.remove(manager.getId());
        toModify.setNom(newName);
        managerByNom.put(newName.toUpperCase(), toModify);
        return true;
    }


    /**
     *
     * @param newName le nouveau nom
     * @param run le run a modifier
     * @param connexionInformation les informations pour mettre à jour les infos de dernières modifications
     * @return
     */
    public boolean renameRun(String newName, EMHRun run, final ConnexionInformation connexionInformation) {
        run.changeNom(newName);

        run.getInfosVersion().setDateDerniereModif(connexionInformation.getCurrentDate());
        run.getInfosVersion().setAuteurDerniereModif(connexionInformation.getCurrentUser());
        return true;

    }

    /**
     * permet d'ajouter un nouveau manager.
     *
     * @param <T>
     * @param type
     * @param manager
     * @param newName
     */
    public <T extends ManagerEMHContainerBase> boolean addManager(EnumCatEMH type, T manager,
                                                                  String newName) {
        //id pour chaque type:
        //ajouter support pour renommer les scenarios crue 9 utilisé
        Map<String, T> managerByNom = getMapFor(type);
        if (managerByNom == null) {
            LOGGER.log(Level.SEVERE, "not manager found for {0}", type);
            return false;
        }
        String prefix = CruePrefix.getPrefix(type);
        if (!newName.startsWith(prefix)) {
            LOGGER.log(Level.WARNING, "not renamed because {0} has not the correct prefix", newName);
            return false;
        }
        if (managerByNom.containsKey(newName.toUpperCase())) {
            LOGGER.log(Level.INFO, "not renamed because {0} is already used ", newName.toUpperCase());
            return false;
        }
        manager.setNom(newName);
        managerByNom.put(newName.toUpperCase(), manager);
        return true;
    }

    /**
     * @param radical    a vérifier
     * @param manager    le manager
     * @param isCreation true si nouveau manager
     * @param <T>        la classe du manager
     * @return true si le nom est utilisé. Si pas une création et si le nom est le meme que actuel, renvoie false.
     */
    public <T extends ManagerEMHContainerBase> boolean isNameAlreadyUsed(String radical, T manager, boolean isCreation) {
        String newName = manager.getLevel().getPrefix() + radical;
        if (!isCreation && manager.getNom().equals(newName)) {
            return false;
        }
        Map<String, T> managerByNom = getMapFor(CrueLevelType.getCatEMH(manager.getLevel()));
        if (managerByNom == null) {
            return false;
        }
        return managerByNom.containsKey(newName.toUpperCase());
    }

    private Map<String, File> getRunFiles(final Map<String, FichierCrueParModele> allFileUsed,
                                          final ManagerEMHScenario scenario, EMHRun run) {
        final Map<String, File> res = new HashMap<>(allFileUsed.size());
        final File dirOfRun = getDirForRun(scenario, run);
        for (final Map.Entry<String, FichierCrueParModele> entry : allFileUsed.entrySet()) {
            final File modeleDir = getParentDirInRun(dirOfRun, entry);
            res.put(entry.getKey(), new File(modeleDir, entry.getValue().getFichier().getNom()));
        }
        return res;
    }

    /**
     * @param id id du scenario a trouver. Toujours transformer en majuscule
     */
    public ManagerEMHScenario getScenario(final String id) {
        if (id == null) {
            return null;
        }
        return idScenario.get(id.toUpperCase());
    }

    public ManagerEMHScenario getScenarioCourant() {
        return scenarioCourant;
    }

    public void setScenarioCourant(final ManagerEMHScenario scenarioCourant) {
        this.scenarioCourant = scenarioCourant;
    }

    /**
     * @param id il s'agit du nom de l'objet et nom de l'id.
     */
    public ManagerEMHSousModele getSousModele(final String id) {
        if (id == null) {
            return null;
        }
        return idSousModele.get(id.toUpperCase());
    }
}
