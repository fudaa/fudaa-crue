package org.fudaa.dodico.crue.metier.emh;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.joda.time.LocalDateTime;

public class CompteRenduAvancement extends AbstractInfosEMH {

  private String code;
  private LocalDateTime date;
  private final List<Indicateur> indicateurs = new ArrayList<>();
  private final List<Indicateur> indicateursExt = Collections.unmodifiableList(indicateurs);
  private File avcFile;

  @UsedByComparison(ignoreInComparison = true)
  public File getAvcFile() {
    return avcFile;
  }

  public void setAvcFile(File avcFile) {
    this.avcFile = avcFile;
  }

  public String getCode() {
    return code;
  }

  @Override
  public EnumInfosEMH getCatType() {
    return EnumInfosEMH.AVANCEMENT;
  }

  public void addIndicateur(Indicateur indicateur) {
    indicateurs.add(indicateur);
  }

  public void setCode(String code) {
    this.code = code;
  }

  @UsedByComparison(ignoreInComparison = true)
  public LocalDateTime getDate() {
    return date;
  }

  public void setDate(LocalDateTime date) {
    this.date = date;
  }

  public List<Indicateur> getIndicateurs() {
    return indicateursExt;
  }
}
