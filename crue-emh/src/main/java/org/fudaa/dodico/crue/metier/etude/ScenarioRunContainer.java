package org.fudaa.dodico.crue.metier.etude;

import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

/**
 * @author deniger
 */
public class ScenarioRunContainer {
  public final EMHRun run;
  public final ManagerEMHScenario scenario;

  public ScenarioRunContainer(EMHRun run, ManagerEMHScenario scenario) {
    this.run = run;
    this.scenario = scenario;
  }

  @Override
  public String toString() {
    return scenario.getNom() + " / " + run.getNom();
  }

  /**
   *
   * @param scenario le scenario a parcourir
   * @param target contienddra tous les ScenarioRunContainer issu du scenario
   */
  public static void create(ManagerEMHScenario scenario, List<ScenarioRunContainer> target) {
    if (scenario != null && CollectionUtils.isNotEmpty(scenario.getListeRuns())) {
      scenario.getListeRuns().forEach(emhRun -> target.add(new ScenarioRunContainer(emhRun, scenario)));
    }
  }
}
