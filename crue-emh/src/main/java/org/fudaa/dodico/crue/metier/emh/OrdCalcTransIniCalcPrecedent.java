/***********************************************************************
 * Module:  OrdCalcTransIniCalcPrecedent.java
 * Author:  deniger
 * Purpose: Defines the Class OrdCalcTransIniCalcPrecedent
 ***********************************************************************/

package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;

public class OrdCalcTransIniCalcPrecedent extends OrdCalcTrans {

  public OrdCalcTransIniCalcPrecedent(CrueConfigMetier props) {
    super(props);
  }


}
