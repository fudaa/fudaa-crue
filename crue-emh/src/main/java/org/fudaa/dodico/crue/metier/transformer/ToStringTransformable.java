package org.fudaa.dodico.crue.metier.transformer;

import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;

/**
 * Utilise par les objets qui peuvent etre transformés en String
 * 
 * @author deniger
 */
public interface ToStringTransformable {
  

  /**
   * @param props used to get formatter
   * @param format
   * @return
   */
  String toString(CrueConfigMetier props, EnumToString format, DecimalFormatEpsilonEnum type);
  
  

}
