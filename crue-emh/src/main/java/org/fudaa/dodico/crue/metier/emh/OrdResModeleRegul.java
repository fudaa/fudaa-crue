/**
 * *********************************************************************
 * Module: OrdResBrancheStrickler.java Author: deniger Purpose: Defines the Class OrdResBrancheStrickler
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.OrdResKey;

import java.util.List;
import java.util.stream.Collectors;

public class OrdResModeleRegul extends OrdRes implements Cloneable {

  private final static OrdResKey KEY = new OrdResKey(EnumCatEMH.MODELE);
  public static final String Q_Z_REGUL = "qZregul";

  @Override
  public OrdResKey getKey() {
    return KEY;
  }


  @Override
  public OrdResModeleRegul clone() {
    try {
      return (OrdResModeleRegul) super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why this ?");
  }

  @Visibility(ihm = false)
  public List<Dde> getDdesOnly() {
    return values.values().stream().filter(dde -> dde.getClass().equals(Dde.class)).collect(Collectors.toList());
  }

  @Visibility(ihm = false)
  public List<DdeMultiple> getDdeMultiple() {
    return values.values().stream().filter(dde -> dde.getClass().equals(DdeMultiple.class)).map(DdeMultiple.class::cast).collect(Collectors.toList());
  }

  @Visibility(ihm = false)
  public boolean isQZRegul() {
    return getDdeValue(Q_Z_REGUL);
  }

  @Override
  public String toString() {
    return "OrdResModeleRegul ["
        + "ddeMultipleQZregul=" + isQZRegul()
        + "ddeMode=" + getDdeValue("mode")
        + ", ddeLoiRegul=" + getDdeValue("loiRegul")
        + ", ddeZcns=" + getDdeValue("zcns")
        + "]";
  }
}
