/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.factory;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtBranchePdc;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtBrancheSeuilLateral;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtBrancheSeuilTransversal;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtCasierProfil;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.DonFrtManning;
import org.fudaa.dodico.crue.metier.emh.DonFrtStrickler;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoBatiCasier;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilCasier;
import org.fudaa.dodico.crue.metier.emh.EMHCasierProfil;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.EnumFormulePdc;
import org.fudaa.dodico.crue.metier.emh.EvolutionFF;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.emh.Pdt;
import org.fudaa.dodico.crue.metier.emh.PdtCst;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;
import org.joda.time.Duration;

/**
 * @author deniger
 */
public final class EMHFactory {

  public final static NumberFormat FMT_DISTMAX;
  public final static NumberFormat FMT_DISTMAX_FAIBLE;
  public final static NumberFormat FMT_PROFIL_CASIER;

  static {
    FMT_DISTMAX = NumberFormat.getIntegerInstance();
    FMT_DISTMAX.setGroupingUsed(false);
    FMT_DISTMAX.setMinimumIntegerDigits(5);

    FMT_DISTMAX_FAIBLE = NumberFormat.getNumberInstance(Locale.US);
    FMT_DISTMAX_FAIBLE.setGroupingUsed(false);
    FMT_DISTMAX_FAIBLE.setMinimumIntegerDigits(4);
    // FMT_DISTMAX_FAIBLE.setMaximumIntegerDigits(4);
    FMT_DISTMAX_FAIBLE.setMinimumFractionDigits(1);
    FMT_DISTMAX_FAIBLE.setMaximumFractionDigits(1);

    FMT_PROFIL_CASIER = NumberFormat.getIntegerInstance();
    FMT_PROFIL_CASIER.setMinimumIntegerDigits(3);
    FMT_PROFIL_CASIER.setGroupingUsed(false);
  }

  private EMHFactory() {
  }

  public static EMHCasierProfil createCasierProfil(final String nom, final CrueConfigMetier defaults, final boolean addDefault) {
    final EMHCasierProfil res = new EMHCasierProfil(nom);
    if (addDefault) {
      res.addInfosEMH(new DonCalcSansPrtCasierProfil(defaults));
    }
    return res;

  }

  public static DonFrt createDonFrt(final EnumTypeLoi loi, final String nomFrot) {
    if (EnumTypeLoi.LoiZFK.equals(loi)) {
      return createDonFrtZFk(nomFrot);
    } else if (EnumTypeLoi.LoiZFKSto.equals(loi)) {
      return createDonFrtZFkSto(nomFrot);
    } else if (EnumTypeLoi.LoiZFN.equals(loi)) {
      return createDonFrtZFn(nomFrot);
    } else if (EnumTypeLoi.LoiZFNSto.equals(loi)) {
      return createDonFrtZFnSto(nomFrot);
    }
    return null;
  }

  /**
   * @param nomFrot le nom du frottement
   * @return le strickler correctement initialise
   */
  public static DonFrtStrickler createDonFrtZFk(final String nomFrot) {
    final DonFrtStrickler frt = new DonFrtStrickler();
    frt.setNom(nomFrot);
    final LoiFF loiFF = new LoiFF();
    frt.setStrickler(loiFF);
    loiFF.setNom(nomFrot);
    loiFF.setType(EnumTypeLoi.LoiZFK);
    loiFF.setEvolutionFF(new EvolutionFF());
    final List<PtEvolutionFF> pts = new ArrayList<>();
    loiFF.getEvolutionFF().setPtEvolutionFF(pts);
    return frt;
  }

  /**
   * @param nomFrot le nom du frottement
   * @return le strickler correctement initialise
   */
  public static DonFrtStrickler createDonFrtZFkSto(final String nomFrot) {
    final DonFrtStrickler frt = new DonFrtStrickler();
    frt.setNom(nomFrot);
    final LoiFF loiFF = new LoiFF();
    frt.setStrickler(loiFF);
    loiFF.setNom(nomFrot);
    loiFF.setType(EnumTypeLoi.LoiZFKSto);
    loiFF.setEvolutionFF(new EvolutionFF());
    final List<PtEvolutionFF> pts = new ArrayList<>();
    loiFF.getEvolutionFF().setPtEvolutionFF(pts);
    return frt;
  }

  /**
   * @param nomFrot le nom du frottement
   * @return le strickler correctement initialise
   */
  public static DonFrtManning createDonFrtZFn(final String nomFrot) {
    final DonFrtManning frt = new DonFrtManning();
    frt.setNom(nomFrot);
    final LoiFF loiFF = new LoiFF();
    frt.setManning(loiFF);
    loiFF.setNom(nomFrot);
    loiFF.setType(EnumTypeLoi.LoiZFN);
    loiFF.setEvolutionFF(new EvolutionFF());
    final List<PtEvolutionFF> pts = new ArrayList<>();
    loiFF.getEvolutionFF().setPtEvolutionFF(pts);
    return frt;
  }

  public static DonFrtManning createDonFrtZFnSto(final String nomFrot) {
    final DonFrtManning frt = new DonFrtManning();
    frt.setNom(nomFrot);
    final LoiFF loiFF = new LoiFF();
    frt.setManning(loiFF);
    loiFF.setNom(nomFrot);
    loiFF.setType(EnumTypeLoi.LoiZFNSto);
    loiFF.setEvolutionFF(new EvolutionFF());
    final List<PtEvolutionFF> pts = new ArrayList<>();
    loiFF.getEvolutionFF().setPtEvolutionFF(pts);
    return frt;
  }

  /**
   * @param d la duree
   * @return le pdt constant
   */
  public static Pdt createPdtCst(final Duration d) {
    final PdtCst pdtCst = new PdtCst();
    pdtCst.setPdtCst(d);
    return pdtCst;
  }

  /**
   * @param idBranche
   * @return
   */
  public static DonCalcSansPrtBranchePdc createDCSPBranchePdcCrue9(final String idBranche) {
    final DonCalcSansPrtBranchePdc dataDCSP = new DonCalcSansPrtBranchePdc();
    final EvolutionFF listeEvolution = new EvolutionFF();
    final List<PtEvolutionFF> listePoints = new ArrayList<>();
    listeEvolution.setPtEvolutionFF(listePoints);
    final LoiFF newLoi = new LoiFF();
    newLoi.setNom(CruePrefix.addPrefixs(idBranche, EnumTypeLoi.LoiQPdc, EnumCatEMH.BRANCHE));
    newLoi.setEvolutionFF(listeEvolution);
    newLoi.setType(EnumTypeLoi.LoiQPdc);
    dataDCSP.setPdc(newLoi);
    return dataDCSP;
  }

  /**
   * @param casier le casier a traiter
   * @param pos la position du profil dans le casier
   * @return le DonPrtGeoProfilCasier produit
   */
  public static DonPrtGeoProfilCasier createDonPrtGeoProfilCasier(final CatEMHCasier casier, final int pos,
                                                                  final CrueConfigMetier defaults) {
    final DonPrtGeoProfilCasier dataDPTG = new DonPrtGeoProfilCasier(defaults);
    dataDPTG.setNom(CruePrefix.changePrefix(casier.getNom(), CruePrefix.P_CASIER, CruePrefix.P_PROFIL_CASIER) + "_"
            + FMT_PROFIL_CASIER.format(pos));
    return dataDPTG;
  }

  /**
   * @param casier le casier a traiter
   * @return le DonPrtGeoProfilCasier produit
   */
  public static DonPrtGeoBatiCasier createDonPrtGeoBatiCasierCrue9(final CatEMHCasier casier, final CrueConfigMetier defaults) {
    final String batiCasier = CruePrefix.changePrefix(casier.getNom(),
            CruePrefix.P_CASIER, CruePrefix.P_BATI_CASIER);
    final DonPrtGeoBatiCasier dataDPTG = createDonPrtGeoBatiCasier(batiCasier, defaults);
    return dataDPTG;
  }

  public static DonPrtGeoBatiCasier createDonPrtGeoBatiCasier(final String id, final CrueConfigMetier defaults) {
    final DonPrtGeoBatiCasier donPrtGeoBatiCasier = new DonPrtGeoBatiCasier(defaults);
    donPrtGeoBatiCasier.setNom(id);
    return donPrtGeoBatiCasier;
  }

  public static DonCalcSansPrtBrancheSeuilLateral createBrancheSeuilLateralForCrue9() {
    final DonCalcSansPrtBrancheSeuilLateral seuilLateral = new DonCalcSansPrtBrancheSeuilLateral();
    seuilLateral.setFormulePdc(EnumFormulePdc.DIVERGENT);
    return seuilLateral;
  }

  public static DonCalcSansPrtBrancheSeuilTransversal createBrancheSeuilTransversalForCrue9() {
    final DonCalcSansPrtBrancheSeuilTransversal seuilTransversal = new DonCalcSansPrtBrancheSeuilTransversal();
    seuilTransversal.setFormulePdc(EnumFormulePdc.DIVERGENT);
    return seuilTransversal;
  }
}
