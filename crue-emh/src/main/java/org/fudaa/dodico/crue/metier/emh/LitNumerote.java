package org.fudaa.dodico.crue.metier.emh;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.comparator.Point2dAbscisseComparator;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;

/**
 */
public class LitNumerote implements ToStringTransformable, Cloneable {

  private boolean isLitActif;
  private boolean isLitMineur;
  private PtProfil limDeb;
  private PtProfil limFin;
  private LitNomme nomLit;
  private DonFrt frot;

  public PtProfil getLimDeb() {
    return limDeb;
  }

  /**
   * Attention, pour les points limites, ne compare que l'abscisse.
   *
   * @param other
   * @param ptProfilComparator
   * @return
   */
  public boolean isSame(LitNumerote other, Point2dAbscisseComparator ptProfilComparator) {
    if (other == null) {
      return false;
    }
    if (isLitActif != other.isLitActif) {
      return false;
    }
    if (isLitMineur != other.isLitMineur) {
      return false;
    }
    if (!ObjectUtils.equals(nomLit, other.nomLit)) {
      return false;
    }
    if (!isSame(frot, other.frot)) {
      return false;
    }
    return (ptProfilComparator.compare(limDeb, other.limDeb) == 0) && (ptProfilComparator.compare(limFin, other.limFin) == 0);
  }

  private boolean isSame(DonFrt frt1, DonFrt frt2) {
    if (frt1 == frt2) {
      return true;
    }
    if (frt1 == null || frt2 == null) {
      return false;
    }
    return StringUtils.equals(frt1.getNom(), frt2.getNom());
  }

  /**
   * cloned uniquement limDeb et limFin
   *
   * @return
   */
  @Override
  public LitNumerote clone() {
    try {
      LitNumerote res = (LitNumerote) super.clone();
      if (limDeb != null) {
        res.limDeb = limDeb.clone();
      }
      if (limFin != null) {
        res.limFin = limFin.clone();
      }

      return res;
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why ?");
  }

  public void setLimDeb(PtProfil newPtProfil) {
    this.limDeb = newPtProfil;
  }

  public PtProfil getLimFin() {
    return limFin;
  }

  public void setLimFin(PtProfil newPtProfil) {
    this.limFin = newPtProfil;
  }

  public LitNomme getNomLit() {
    return nomLit;
  }

  public void setNomLit(LitNomme newLitNomme) {
    this.nomLit = newLitNomme;
  }

  public DonFrt getFrot() {
    return frot;
  }

  @UsedByComparison
  @Visibility(ihm = false)
  public String getFrotId() {
    return frot == null ? null : frot.getId();
  }

  public void setFrot(DonFrt newDonFrt) {
    this.frot = newDonFrt;
  }

  public boolean getIsLitMineur() {
    return isLitMineur;
  }

  public void setIsLitMineur(boolean newIsLitMineur) {
    isLitMineur = newIsLitMineur;
  }

  public boolean getIsLitActif() {
    return isLitActif;
  }

  public void setIsLitActif(boolean newIsLitActif) {
    isLitActif = newIsLitActif;
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, DecimalFormatEpsilonEnum type) {
    return getClass().getSimpleName() + ", limDeb=" + limToString(limDeb, props, format, type) + ", limFin="
            + limToString(limFin, props, format, type);
  }

  private static String limToString(PtProfil pt, CrueConfigMetier props, EnumToString format, DecimalFormatEpsilonEnum type) {
    return pt == null ? null : pt.toString(props, format, type);
  }
  
  /**
   * Test de la longueur du lit (Delta X <> 0)
   * @return true si le lit est de longueur nulle, false sinon
   */
  public boolean isLongueurNulle() {
    return this.getLimDeb().getXt() == this.getLimFin().getXt();
  }
}
