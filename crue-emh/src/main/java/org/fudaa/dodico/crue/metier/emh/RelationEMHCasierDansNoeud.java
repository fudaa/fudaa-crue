/***********************************************************************
 * Module:  RelationEMHCasierDansNoeud.java
 * Author:  deniger
 * Purpose: Defines the Class RelationEMHCasierDansNoeud
 ***********************************************************************/

package org.fudaa.dodico.crue.metier.emh;

public class RelationEMHCasierDansNoeud extends RelationEMH<CatEMHCasier> {


  @Override
  public RelationEMH<CatEMHCasier> copyWithEMH(CatEMHCasier copiedEMH) {
    RelationEMHCasierDansNoeud res = new RelationEMHCasierDansNoeud();
    res.setEmh(copiedEMH);
    return res;
  }

}
