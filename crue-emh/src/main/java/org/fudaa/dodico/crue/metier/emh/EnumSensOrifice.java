/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.contrat.DoubleValuable;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

public enum EnumSensOrifice implements ToStringInternationalizable, DoubleValuable {

  BIDIRECT(0), DIRECT(1), INDIRECT(2);
  private final int id;

  EnumSensOrifice(final int id) {
    this.id = id;
  }

  @Override
  public double toDoubleValue() {
    return id;
  }

  @Override
  public String geti18n() {
    return BusinessMessages.getString("EnumSensOrifice." + name() + ".shortName");
  }

  @Override
  public String geti18nLongName() {
    return BusinessMessages.getString("EnumSensOrifice." + name() + ".longName");
  }
}
