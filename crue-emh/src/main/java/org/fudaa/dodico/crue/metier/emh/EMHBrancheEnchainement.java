package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 * n'existe pas en Crue9
 * 
 */
public class EMHBrancheEnchainement extends CatEMHBranche {

  public EMHBrancheEnchainement(final String nom) {
    super(nom);
  }
  
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public EnumBrancheType getBrancheType(){
    return EnumBrancheType.EMHBrancheEnchainement;
  }


  @Override
  @Visibility(ihm = false)
  public EMHBrancheEnchainement copyShallowFirstLevel() {
    return  copyPrimitiveIn(new EMHBrancheEnchainement(getNom()));
  }

}
