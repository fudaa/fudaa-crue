/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;

public class EMHModeleEnchainement extends EMH {
  @Override
  public EnumCatEMH getCatType() {
    return EnumCatEMH.MODELE_ENCHAINEMENT;
  }

  @Override
  public boolean getUserActive() {
    return true;
  }
  
  @Override
  public Object getSubCatType() {
    return null;
  }

  @Override
  @Visibility(ihm = false)
  public EMHModeleEnchainement copyShallowFirstLevel() {
    return  copyPrimitiveIn(new EMHModeleEnchainement());
  }

}
