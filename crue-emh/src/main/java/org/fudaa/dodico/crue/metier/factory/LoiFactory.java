/**
 *
 */
package org.fudaa.dodico.crue.metier.factory;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.emh.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cde
 */
public class LoiFactory {
    /**
     * Alimente les données unitaires d'une LoiFF
     *
     * @param loiFF Loi à alimenter
     * @param nom   Nom de la loi
     */
    public static void alimenteDebutLoiFF(final LoiFF loiFF, final String nom, final EnumTypeLoi type) {

        alimenteDebutLoi(loiFF, nom, type);
    }

    /**
     * @param loiDF
     * @param nom
     */
    public static void alimenteDebutLoiDF(final LoiDF loiDF, final String nom, final EnumTypeLoi type) {

        alimenteDebutLoi(loiDF, nom, type);
    }

    /**
     * @param loi
     * @param nom , final EnumTypeLoi type
     */
    public static void alimenteDebutLoi(final Loi loi, final String nom, final EnumTypeLoi type) {
        loi.setNom(nom);
        loi.setType(type);
    }

    /**
     * Alimente la liste des points de la LoiFF seulement s'il existe un moins un point et s'il y a autant d'abscisses que d'ordonnées
     *
     * @param loiFF     Loi à alimenter
     * @param abscisses Liste des abscisses
     * @param ordonnees Liste des ordonnées
     * @return true si EvolutionFF a pu être alimentée, false si le tableau des abscisses et celui des ordonnées n'ont pas la même taille ou si aucun
     *         point défini
     */
    public static boolean alimenteEvolutionFF(final Loi loiFF, final float[] abscisses, final float[] ordonnees) {

        if (abscisses == null || ordonnees == null || abscisses.length != ordonnees.length || abscisses.length == 0) {
            return false;
        }

        final EvolutionFF evolutionFF = new EvolutionFF();
        loiFF.setEvolutionFF(evolutionFF);
        final List<PtEvolutionFF> ptsFF = new ArrayList<>();

        for (int i = 0, imax = abscisses.length; i < imax; i++) {
            final PtEvolutionFF ptFF = new PtEvolutionFF(abscisses[i], ordonnees[i]);
            ptsFF.add(ptFF);
        }
        evolutionFF.setPtEvolutionFF(ptsFF);

        return true;
    }

    public static boolean alimenteEvolutionFF(final Loi loiFF, final double[] abscisses, final double[] ordonnees) {

        if (abscisses == null || ordonnees == null || abscisses.length != ordonnees.length || abscisses.length == 0) {
            return false;
        }

        final EvolutionFF evolutionFF = new EvolutionFF();
        loiFF.setEvolutionFF(evolutionFF);
        final List<PtEvolutionFF> ptsFF = new ArrayList<>();

        for (int i = 0, imax = abscisses.length; i < imax; i++) {
            final PtEvolutionFF ptFF = new PtEvolutionFF(abscisses[i], ordonnees[i]);
            ptsFF.add(ptFF);
        }
        evolutionFF.setPtEvolutionFF(ptsFF);

        return true;
    }

    /**
     * Récupère une loi (indépendemment du type d'abscisses), à partir des données passées en paramètres
     *
     * @param abscisses Ensemble des abscisses
     * @param ordonnees Ensemble d'ordonnées
     * @param nomLoi    Nom à donner à la loi
     * @return LoiFF ou null si la loi n'a pas eu être créée
     */
    public static LoiFF getLoiFFRPTG(final float[] abscisses, final float[] ordonnees, final String nomLoi,
                                     final EnumTypeLoi type) {

        final LoiFF loiFF = new LoiFF();
        final boolean ok = alimenteEvolutionFF(loiFF, abscisses, ordonnees);
        if (ok) {
            alimenteDebutLoiFF(loiFF, nomLoi, type);
            return loiFF;
        }

        return null;
    }

    public static AbstractLoi createLoi(EnumTypeLoi typeLoi, CrueConfigMetier ccm) {

        AbstractLoi res = null;
        if (EnumTypeLoi.LoiSectionsZ == typeLoi) {
            res = new LoiTF();
            ((LoiTF) res).setEvolutionTF(new EvolutionTF(new ArrayList<>()));

        } else {
            boolean date = typeLoi.isDateAbscisse();
            res = date ? new LoiDF() : new LoiFF();
            ((Loi) res).setEvolutionFF(new EvolutionFF(new ArrayList<>()));
        }
        res.setType(typeLoi);
        return res;
    }
}
