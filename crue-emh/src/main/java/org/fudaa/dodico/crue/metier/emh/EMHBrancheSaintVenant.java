package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 * Branche 20: loi d'ecoulement de Saint-Venant Remplace les anciennes branches 0 et 9
 * 
 * @author Adrien Hadoux
 */
public class EMHBrancheSaintVenant extends CatEMHBranche {


  public EMHBrancheSaintVenant(final String nom) {
    super(nom);
  }
  
  
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public EnumBrancheType getBrancheType() {
    return EnumBrancheType.EMHBrancheSaintVenant;
  }

  @Override
  @Visibility(ihm = false)
  public EMHBrancheSaintVenant copyShallowFirstLevel() {
    return  copyPrimitiveIn(new EMHBrancheSaintVenant(getNom()));
  }
}
