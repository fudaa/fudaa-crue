package org.fudaa.dodico.crue.metier.emh;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.joda.time.Duration;

/**
 *
 * @author deniger
 */
public class PrendreClichePonctuel implements Cloneable {

  private org.joda.time.Duration tempsSimu;
  private String nomFic;

  public PrendreClichePonctuel() {
  }

  public PrendreClichePonctuel(String nomFic, Duration tempsSimu) {
    this.tempsSimu = tempsSimu;
    this.nomFic = nomFic;
  }

  @Override
  protected PrendreClichePonctuel clone() {
    try {
      return (PrendreClichePonctuel) super.clone();
    } catch (CloneNotSupportedException ex) {
      Logger.getLogger(PrendreClichePonctuel.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("why");
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + " " + nomFic + ": " + DateDurationConverter.durationToXsd(tempsSimu);
  }

  public String getNomFic() {
    return nomFic;
  }

  public void setNomFic(String nomFic) {
    this.nomFic = nomFic;
  }

  public Duration getTempsSimu() {
    return tempsSimu;
  }

  public void setTempsSimu(Duration tempsSimu) {
    this.tempsSimu = tempsSimu;
  }
}
