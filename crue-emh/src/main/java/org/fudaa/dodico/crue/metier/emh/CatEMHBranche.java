package org.fudaa.dodico.crue.metier.emh;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.comparator.ComparatorRelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.factory.PredicateFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Interface branche peut etre: EMHBranchePdc Branche 1: perte de charge singuliére EMHBrancheSeuilTransversal Branche 2: seuil avec prise en compte
 * de la charge dynamique EMHBrancheSeuilLateral Branche 4: seuil sans prise en compte de la charge dynamique EMHBrancheOrifice Branche 5: orifice,
 * clapet, vanne EMHBrancheStrickler Branche 6: loi d'écoulement de Manning-Strickler EMHBrancheBarrageGenerique Branche 14: ouvrage suivant une
 * consigne fonction d'un débit ailleurs, ou noyé (loi pt par pt) EMHBrancheBarrageFilEau Branche 15: barrage suivant une consigne fonction d'un débit
 * ailleurs, ou noyé (De Marchi) EMHBrancheSaintVenant Branche 20: loi d'écoulement de Saint-Venant EMHBrancheNiveauxAssocies Branche 12: vanne é
 * niveaux associés EMHBrancheEnchainement Liaison entre néuds appartenant é des modéles différents dans le but d'un enchaénement
 *
 * @author Adrien Hadoux
 */
public abstract class CatEMHBranche extends CatEMHActivable {
  public CatEMHBranche(final String nom) {
    super(nom);
  }

  @Override
  public boolean getActuallyActive() {
    boolean res = super.getActuallyActive();
      if (res) {
      EMHSousModele parent = getParent();
      res = parent != null && parent.getActuallyActive();
    }
    return res;
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public abstract EnumBrancheType getBrancheType();

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public Object getSubCatType() {
    return getBrancheType();
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public RelationEMHSectionDansBranche getSectionAval() {
    return (RelationEMHSectionDansBranche) CollectionUtils.find(getRelationEMH(),
        new PredicateFactory.PredicateRelationEMHSectionDansBranche(
            EnumPosSection.AVAL));
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public RelationEMHSectionDansBranche getSectionAmont() {
    return (RelationEMHSectionDansBranche) CollectionUtils.find(getRelationEMH(),
        new PredicateFactory.PredicateRelationEMHSectionDansBranche(
            EnumPosSection.AMONT));
  }

  @Override
  public EnumCatEMH getCatType() {
    return EnumCatEMH.BRANCHE;
  }

  /**
   * @return toutes les sections sauf la section pilote.
   */
  public List<RelationEMHSectionDansBranche> getListeSections() {
    return EMHHelper.collectRelationsSections(this);
  }

  public List<RelationEMHSectionDansBranche> getSections() {
    return getListeSections();
  }

  /**
   * normalement inutile car les sections sont triées à l'ouverture et si modification
   *
   * @param props
   */
  public List<RelationEMHSectionDansBranche> getListeSectionsSortedXP(CrueConfigMetier props) {
    return EMHHelper.collectListeRelationsSectionsSortedByXp(this, props);
  }

  public double getLength() {
    List<RelationEMHSectionDansBranche> sections = getListeSections();
    for (RelationEMHSectionDansBranche sectionDansBranche : sections) {
      if (EnumPosSection.AVAL.equals(sectionDansBranche.getPos())) {
        return sectionDansBranche.getXp();
      }
    }
    return 0;
  }

  public static double getLength(List<RelationEMHSectionDansBranche> sections) {
    for (RelationEMHSectionDansBranche sectionDansBranche : sections) {
      if (EnumPosSection.AVAL.equals(sectionDansBranche.getPos())) {
        return sectionDansBranche.getXp();
      }
    }
    return 0;
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public EMHSousModele getParent() {
    return EMHHelper.getParent(this);
  }

  public void addListeSections(final List<? extends RelationEMH> liste) {
    EMHHelper.addListeSectionsToRelations(this, liste);
  }

  public CatEMHNoeud getNoeudAmont() {
    return EMHHelper.getNoeudAmont(this);
  }

  @Visibility(ihm = false)
  @UsedByComparison
  public String getNoeudAmontId() {
    return EMHHelper.getNoeudAmont(this).getId();
  }

  @Visibility(ihm = false)
  public String getNoeudAmontNom() {
    return EMHHelper.getNoeudAmont(this).getNom();
  }

  public void setNoeudAmont(final CatEMHNoeud noeudAmont) {
    EMHRelationFactory.addNoeudAmont(this, noeudAmont);
  }

  public CatEMHNoeud getNoeudAval() {
    return EMHHelper.getNoeudAval(this);
  }

  @Visibility(ihm = false)
  @UsedByComparison
  public String getNoeudAvalId() {
    return EMHHelper.getNoeudAval(this).getId();
  }

  @Visibility(ihm = false)
  public String getNoeudAvalNom() {
    return EMHHelper.getNoeudAval(this).getNom();
  }

  public void setNoeudAval(final CatEMHNoeud noeudAval) {
    EMHRelationFactory.addNoeudAval(this, noeudAval);
  }

  /**
   * @param props pour avoir le eps de xp. si null pas de eps utilise
   */
  @SuppressWarnings("unchecked")
  public boolean sortRelationSectionDansBranche(CrueConfigMetier props) {
    List<RelationEMH> init = getRelationEMH();
    if (init == null) {
      return false;
    }
    List<RelationEMH> newrelationEMH = new ArrayList<>(init.size());
    List<RelationEMHSectionDansBranche> relationEMHSection = new ArrayList<>(init.size());
    // cette methode permet de trier les relation de type section dans branches des autres.
    EMHHelper.filterRelationsSections(this, relationEMHSection, newrelationEMH);
    List<RelationEMHSectionDansBranche> before = new ArrayList<>(relationEMHSection);
    Collections.sort(relationEMHSection, new ComparatorRelationEMHSectionDansBranche(props));
    newrelationEMH.addAll(relationEMHSection);
    setRelationEMH(newrelationEMH);
    return !before.equals(relationEMHSection);
  }
}
