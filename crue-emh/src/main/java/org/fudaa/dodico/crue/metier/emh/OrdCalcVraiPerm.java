/**
 * *********************************************************************
 * Module: OrdCalcVraiPerm.java Author: deniger Purpose: Defines the Class OrdCalcVraiPerm
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OrdCalcVraiPerm extends OrdCalcPerm implements Cloneable {

  /**
   */
  private Calc calc;

  /**
   */
  @Override
  public Calc getCalc() {
    return calc;
  }

  @Override
  public List getCliches() {
    return Collections.emptyList();
  }

  public void setCalc(Calc newCalc) {
    this.calc = newCalc;
  }

  @Override
  public OrdCalcVraiPerm deepCloneButNotCalc(Calc newCalc) {
    try {
      return (OrdCalcVraiPerm) super.clone();
    } catch (CloneNotSupportedException ex) {
      Logger.getLogger(OrdCalcVraiPerm.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("why?");
  }
}
