package org.fudaa.dodico.crue.metier.emh;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.factory.PredicateFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

import java.util.List;

public abstract class CatEMHSection extends EMH {
  public CatEMHSection(final String nom) {
    super(nom);
  }

  @Override
  public EnumCatEMH getCatType() {
    return EnumCatEMH.SECTION;
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public abstract EnumSectionType getSectionType();

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public Object getSubCatType() {
    return getSectionType();
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public boolean isAmontOrAval() {
    CatEMHBranche branche = getBranche();
    return branche != null && (branche.getSectionAmont().getEmh() == this || branche.getSectionAval().getEmh() == this);
  }

  @Override
  public final boolean getUserActive() {// NOPMD
    return true;
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public EMHSousModele getParent() {
    return EMHHelper.getParent(this);
  }

  @UsedByComparison
  public final ResPrtGeo getResPrtGeo() {
    return EMHHelper.selectFirstOfClass(getInfosEMH(), ResPrtGeo.class);
  }

  @UsedByComparison
  public final ResPrtCIniSection getResPrtCIniSection() {
    return EMHHelper.selectFirstOfClass(getInfosEMH(), ResPrtCIniSection.class);
  }

  /**
   * @return
   */
  @Visibility(ihm = false)
  @UsedByComparison(ignoreInComparison = true)
  public CatEMHBranche getBranche() {
    RelationEMHBrancheContientSection relation = EMHHelper.selectFirstOfClass(getRelationEMH(),
        RelationEMHBrancheContientSection.class);
    return relation == null ? null : relation.getEmh();
  }

  /**
   * @return true si active et si contenue par une branche active ou reference par une section idem active.
   */
  @Override
  public boolean getActuallyActive() {
    return getActuallyActive(false);
  }

  /**
   * @return dans le cas editiion ( forResultLoading=false) si est active si contenue par une branche active ou referencee par une section idem active.
   *     Pour les calcul (forResultLoading=true), le cas "referencee par une section idem active" n'est pas pris en compte.
   */
  public boolean getActuallyActive(boolean forResultLoading) {
    boolean res = super.getActuallyActive();
    if (res) {
      PredicateFactory.PredicateRelationEMHBrancheActiveContientSection predicate = new PredicateFactory.PredicateRelationEMHBrancheActiveContientSection(
          true);
      res = CollectionUtils.exists(getRelationEMH(), predicate);
      if (!res && getSectionType() == EnumSectionType.EMHSectionProfil && !forResultLoading) {
        //Une section avec profil d'une branche A référencée par une section idem dans une autre branche ne devrait pas être désactivée même si on désactive la branche A.
        //CRUE-749
        List<EMHSectionIdem> referencing = EMHHelper.getSectionReferencing(this);
        if (CollectionUtils.isNotEmpty(referencing)) {
          for (EMHSectionIdem emhSectionIdem : referencing) {
            if (emhSectionIdem.getActuallyActive()) {
              res = true;
              break;
            }
          }
        }
        //CRUE-749 fin
      }
    }
    return res;
  }
}
