/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.metier.helper;

import org.fudaa.dodico.crue.metier.emh.LitNumerote;

/**
 * LitNumerote represente par des indexs. On supposte que l'indice de fin est >= à l'indice de début.
 *
 * @author deniger
 */
public class LitNumeroteIndex implements Comparable<LitNumeroteIndex> {

  LitNumerote litNumerote;
  int idxStart;
  int idxEnd;

  public LitNumeroteIndex() {
  }

  public int getIdxEnd() {
    return idxEnd;
  }

  public void setIdxEnd(int idxEnd) {
    this.idxEnd = idxEnd;
  }

  public int getIdxStart() {
    return idxStart;
  }

  public void setIdxStart(int idxStart) {
    this.idxStart = idxStart;
  }

  public LitNumerote getLitNumerote() {
    return litNumerote;
  }

  public boolean isEmpty() {
    return idxStart == idxEnd;
  }

  public boolean isValid() {
    return idxEnd >= idxStart && idxStart >= 0;
  }

  public void setLitNumerote(LitNumerote litNumerote) {
    this.litNumerote = litNumerote;
  }

  /**
   *
   * @param o
   * @return true si o est complètement en dehors de this.
   */
  public boolean isDisjoint(LitNumeroteIndex o) {
    return o.getIdxStart() > getIdxEnd() || o.getIdxEnd() < getIdxStart();
  }

  public boolean isSuccessif(LitNumeroteIndex o) {
    return o.getIdxEnd() == getIdxStart() || o.getIdxStart() == getIdxEnd();
  }

  /**
   *
   * @param o
   * @return true si o suite ce lit.
   */
  public boolean isAfter(LitNumeroteIndex o) {
    return o.getIdxStart() == getIdxEnd();
  }

  /**
   * true si les 2 lits se chevauchent: non successifs et non disjoint
   *
   * @param o
   * @return
   */
  public boolean isOverlaping(LitNumeroteIndex o) {
    return !isSuccessif(o) && !isDisjoint(o);
  }

  @Override
  public int compareTo(LitNumeroteIndex o) {
    if (o == this) {
      return 0;
    }
    if (o == null) {
      return 1;
    }
    int delta = idxStart - o.idxStart;
    if (delta == 0) {
      delta = idxEnd - o.idxEnd;
    }
    if (delta == 0) {
      delta = hashCode() - o.hashCode();
    }
    return delta;
  }

  /**
   *
   * @param position
   * @return true si idxStart <= postion <= idxEnd
   */
  public boolean contains(int position) {
    return position >= getIdxStart() && position <= getIdxEnd();
  }
}
