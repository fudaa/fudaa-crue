/**
 * *********************************************************************
 * Module: ParamNumCalcVraiPerm.java Author: deniger Purpose: Defines the Class ParamNumCalcVraiPerm
 **********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ParamNumCalcVraiPerm implements Cloneable {

  @Override
  public String toString() {
    return "ParamNumCalcVraiPerm []";
  }

  @Override
  protected Object clone() {
    try {
      return super.clone();
    } catch (CloneNotSupportedException ex) {
      Logger.getLogger(ParamNumCalcTrans.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("why ?");
  }

  public ParamNumCalcVraiPerm deepClone() {
    ParamNumCalcVraiPerm res = (ParamNumCalcVraiPerm) clone();
    return res;
  }
}