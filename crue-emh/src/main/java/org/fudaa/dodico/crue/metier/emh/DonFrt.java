package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.ctulu.ToStringTransformable;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;

public abstract class DonFrt implements ObjetNomme, Cloneable, ToStringTransformable {

  private String nom;
  private String id;

  @Override
  public String getId() {
    return id;
  }

  public String getType() {
    return getClass().getSimpleName();
  }

  @Override
  public final String getNom() {
    return nom;
  }

  @Override
  public String getAsString() {
    return getNom();
  }

  @Override
  public String toString() {
    return getAsString();
  }


  /**
   * ne pas utiliser directement.
   */
  @Override
  protected Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  public abstract DonFrt deepClone();

  @Override
  public final void setNom(final String newNom) {
    nom = newNom;
    if (nom != null) {
      id = nom.toUpperCase();
    }
    if (getLoi() != null) {
      getLoi().setNom(nom);
    }
  }

  public abstract LoiFF getLoi();
}
