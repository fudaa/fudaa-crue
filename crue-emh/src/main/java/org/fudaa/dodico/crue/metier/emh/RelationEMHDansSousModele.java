package org.fudaa.dodico.crue.metier.emh;

/**
 *
 * @author deniger
 */
public class RelationEMHDansSousModele extends RelationEMH<EMHSousModele> {
  
  public RelationEMHDansSousModele(EMHSousModele sousModele){
    setEmh(sousModele);
  }

  @Override
  public RelationEMHDansSousModele copyWithEMH(EMHSousModele copiedEMH) {
    return new RelationEMHDansSousModele(copiedEMH);
  }
}
