package org.fudaa.dodico.crue.metier.aoc;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

/**
 * Created by deniger on 28/06/2017.
 */
public enum EnumAocTypeCalageDonnees implements ToStringInternationalizable {
    PERMANENT, TRANSITOIRE_LIMNIGRAMME, TRANSITOIRE_HYDROGRAMME;

    @Override
    public String geti18n() {
        return BusinessMessages.getString("aoc.typeCalageDonnees." + name().toLowerCase() + ".shortName");
    }

    @Override
    public String geti18nLongName() {
        return BusinessMessages.getString("aoc.typeCalageDonnees." + name().toLowerCase() + ".longName");
    }

    /**
     *
     * @return Hydrogramme ou limnigramme
     */
    public String getCourbeName() {
        return BusinessMessages.getString("aoc.typeCalageDonnees." + name().toLowerCase() + ".nomCourbe");
    }
}
