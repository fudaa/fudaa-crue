package org.fudaa.dodico.crue.metier.emh;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 */
public class ResPrtReseau extends AbstractInfosEMH {

  String commentaire;

  public String getCommentaire() {
    return commentaire;
  }

  public void setCommentaire(String commentaire) {
    this.commentaire = commentaire;
  }

  @Override
  public final EnumInfosEMH getCatType() {
    return EnumInfosEMH.RES_PRT_RESEAU;
  }
  private List<ResPrtReseauNoeuds> listResPrtReseauNoeud = null;

  public List<ResPrtReseauNoeuds> getResPrtReseauNoeuds() {
    return listResPrtReseauNoeud;
  }

  /**
   * @param listResPrtReseauNoeud the listResPrtReseauNoeud to set
   */
  public void setListResPrtReseauNoeud(List<ResPrtReseauNoeuds> listResPrtReseauNoeud) {
    if (listResPrtReseauNoeud == null) {
      this.listResPrtReseauNoeud = null;
    } else {
      this.listResPrtReseauNoeud = Collections.unmodifiableList(new ArrayList<>(listResPrtReseauNoeud));
    }
  }
}