package org.fudaa.dodico.crue.metier.emh;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

/**
 *
 * @author Frederic Deniger
 */
public class Dde implements ObjetWithID {

  private final String nom;
  private final boolean valeur;

  public Dde(final String nom, final boolean valeur) {
    this.nom = nom;
    this.valeur = valeur;
  }

  @Override
  public String getId() {
    return getNom();
  }

  public String getDisplayName() {
    return StringUtils.capitalize(getNom());
  }

  @Override
  public int hashCode() {
    final int hash = 7 + (valeur ? 8 : 101);
    return hash + (nom == null ? 0 : nom.hashCode());
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Dde other = (Dde) obj;
    if ((this.nom == null) ? (other.nom != null) : !this.nom.equals(other.nom)) {
      return false;
    }
    return this.valeur == other.valeur;
  }

  @Override
  public String getNom() {
    return nom;
  }

  public boolean isValeur() {
    return valeur;
  }

  public boolean getValeur() {
    return valeur;
  }
}
