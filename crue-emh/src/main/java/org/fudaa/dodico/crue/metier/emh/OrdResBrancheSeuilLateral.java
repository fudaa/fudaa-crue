/**
 * *********************************************************************
 * Module: OrdResBrancheSeuilLateral.java Author: deniger Purpose: Defines the Class OrdResBrancheSeuilLateral
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.OrdResKey;

public class OrdResBrancheSeuilLateral extends OrdRes implements Cloneable {

  private final static OrdResKey KEY = new OrdResKey(EnumBrancheType.EMHBrancheSeuilLateral);

  @Override
  public OrdResKey getKey() {
    return KEY;
  }

  @Override
  public OrdResBrancheSeuilLateral clone() {
    try {
      return (OrdResBrancheSeuilLateral) super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why this ?");
  }

  @Override
  public String toString() {
    return "OrdResBrancheSeuilLateral [ddeRegime=" + getDdeValue("regimeSeuil") + "]";
  }
}