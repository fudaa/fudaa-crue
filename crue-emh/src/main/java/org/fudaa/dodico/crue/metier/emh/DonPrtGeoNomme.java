/*
 * Licence GPL
 */
package org.fudaa.dodico.crue.metier.emh;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;

/**
 * @author deniger
 */
public abstract class DonPrtGeoNomme extends DonPrtGeo implements ObjetNomme {

  private String id;
  private String nom;
  
  private String commentaire = StringUtils.EMPTY;

  public String getCommentaire() {
    return commentaire;
  }
  
  public void setCommentaire(final String commentaire) {
    this.commentaire = StringUtils.defaultString(commentaire);
  }

  @Override
  public final String getId() {
    return id;
  }

  @Override
  public final String getNom() {
    return nom;
  }

  @Override
  public final void setNom(final String newNom) {
    nom = newNom;
    if (nom != null) {
      id = nom.toUpperCase();
    }
  }

}
