package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 * Branche 1: perte de charge singuliere
 *
 * @author Adrien Hadoux
 */
public class EMHBranchePdc extends CatEMHBranche {

  public EMHBranchePdc(final String nom) {
    super(nom);
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public EnumBrancheType getBrancheType() {
    return EnumBrancheType.EMHBranchePdc;
  }

  @Override
  @Visibility(ihm = false)
  public EMHBranchePdc copyShallowFirstLevel() {
    return  copyPrimitiveIn(new EMHBranchePdc(getNom()));
  }
}
