/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.emh;

import gnu.trove.TLongObjectHashMap;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Frederic Deniger
 */
public class ResultatPasDeTempsMapper {

  private final Set<ResultatTimeKey> defined;
  private final TLongObjectHashMap<ResultatTimeKey> equivalent = new TLongObjectHashMap<>();

  /**
   * L'ordre doit être conservé.
   *
   * @param resultatKey
   */
  protected ResultatPasDeTempsMapper(Collection<ResultatTimeKey> resultatKey) {
    defined = new HashSet<>(resultatKey);
    for (ResultatTimeKey resultatTimeKey : resultatKey) {
      long tempSimu = resultatTimeKey.getDuree();
      if (!equivalent.contains(tempSimu)) {
        equivalent.put(tempSimu, resultatTimeKey);
      }
    }
  }

  /**
   *
   * @param key
   * @return null si pas d'equivalence
   */
  protected ResultatTimeKey getEquivalentTempsSimu(ResultatTimeKey key) {
    if (key == null) {
      return null;
    }
    if (defined.contains(key)) {
      return key;
    }
    if (key.isPermanent()) {
      return null;
    }
    return equivalent.get(key.getDuree());
  }

  ResultatTimeKey getEquivalentTempsSimu(long tempsSimu) {
    return equivalent.get(tempsSimu);
  }
}
