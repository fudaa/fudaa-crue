/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.contrat.DoubleValuable;

public enum EnumPosSection implements DoubleValuable {

  AMONT(0), AVAL(2), INTERNE(1);
  final int ordre;

  EnumPosSection(final int ordre) {
    this.ordre = ordre;
  }

  @Override
  public double toDoubleValue() {
    return ordre;
  }

  public int getOrdre() {
    return ordre;
  }
}
