package org.fudaa.dodico.crue.metier.emh;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByIdComparator;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 */
public class DonFrtConteneur extends AbstractInfosEMH implements Sortable {
  public java.util.List<DonFrt> listFrt;

  public DonFrtConteneur() {
  }

  @Override
  public final EnumInfosEMH getCatType() {
    return EnumInfosEMH.DON_FRT_CONTENEUR;
  }

  @Override
  public boolean sort() {
    if (CollectionUtils.isNotEmpty(listFrt)) {
      final List<String> before = TransformerHelper.toNom(listFrt);
      Collections.sort(listFrt, ObjetNommeByIdComparator.INSTANCE);
      final List<String> after = TransformerHelper.toNom(listFrt);
      return !after.equals(before);
    }
    return false;
  }

  public java.util.List<DonFrt> getListFrt() {
    if (listFrt == null) {
      listFrt = new java.util.ArrayList<>();
    }
    return Collections.unmodifiableList(listFrt);
  }

  public void setListFrt(final java.util.List<DonFrt> newListFrt) {
    removeAllListFrt();
    for (final java.util.Iterator iter = newListFrt.iterator(); iter.hasNext(); ) {
      addFrt((DonFrt) iter.next());
    }
  }

  public void addFrt(final DonFrt newDonFrt) {
    if (newDonFrt == null) {
      return;
    }
    if (this.listFrt == null) {
      this.listFrt = new java.util.ArrayList<>();
    }
    if (!this.listFrt.contains(newDonFrt)) {
      this.listFrt.add(newDonFrt);
    }
  }

  public boolean isEmptyFrt(final DonFrt donFrt, final CrueConfigMetier configMetier) {
    final List<PtEvolutionFF> ptEvolutionFF = donFrt.getLoi().getEvolutionFF().getPtEvolutionFF();
    if (ptEvolutionFF.size() == 1) {
      final PtEvolutionFF pt = ptEvolutionFF.get(0);
      final ConfigLoi configLoi = configMetier.getConfigLoi(donFrt.getLoi());
      if (configLoi.getVarAbscisse().getEpsilon().isZero(pt.getAbscisse())
          && configLoi.getVarOrdonnee().getEpsilon().isZero(pt.getOrdonnee())) {
        return true;
      }
    }
    return false;
  }

  DonFrt lastEmptyFkSto;

  public DonFrt getEmptyDonFrtFkSto(final CrueConfigMetier configMetier) {
    if (listFrt == null) {
      return null;
    }
    if (isEmptyKSto(lastEmptyFkSto, configMetier)) {
      return lastEmptyFkSto;
    }
    for (final DonFrt donFrt : listFrt) {
      if (isEmptyFrt(donFrt, configMetier)) {
        lastEmptyFkSto = donFrt;
        return donFrt;
      }
    }
    return null;
  }

  private boolean isEmptyKSto(final DonFrt donFrt, final CrueConfigMetier configMetier) {
    if (donFrt == null) {
      return false;
    }
    if (!EnumTypeLoi.LoiZFKSto.equals(donFrt.getLoi().getType())) {
      return false;
    }
    return isEmptyFrt(donFrt, configMetier);
  }

  public DonFrt getEmptyDonFrtFk(final CrueConfigMetier configMetier) {
    if (listFrt == null) {
      return null;
    }
    for (final DonFrt donFrt : listFrt) {
      //seules les lois de stockages doivent être utilisées ?
      if (!EnumTypeLoi.LoiZFK.equals(donFrt.getLoi().getType())) {
        continue;
      }
      if (isEmptyFrt(donFrt, configMetier)) {
        return donFrt;
      }
    }
    return null;
  }

  /**
   * @param frt la liste a ajouter
   */
  public void addAllFrt(final Collection<DonFrt> frt) {
    if (frt != null) {
      for (final DonFrt it : frt) {
        addFrt(it);
      }
    }
  }

  private DonFrt defaultFrtMineur;
  private DonFrt defaultFrtMajeur;

  @UsedByComparison(ignoreInComparison = true)
  public DonFrt getDefaultFrtMineur() {
    return defaultFrtMineur;
  }

  public void setDefaultFrtMineur(final DonFrt defaultFrt) {
    this.defaultFrtMineur = defaultFrt;
  }

  @UsedByComparison(ignoreInComparison = true)
  public DonFrt getDefaultFrtMajeur() {
    return defaultFrtMajeur;
  }

  public void setDefaultFrtMajeur(final DonFrt defaultFrt) {
    this.defaultFrtMajeur = defaultFrt;
  }

  public void removeFrt(final DonFrt oldDonFrt) {
    if (oldDonFrt == null) {
      return;
    }
    if (oldDonFrt == defaultFrtMineur) {
      defaultFrtMineur = null;
    }
    if (oldDonFrt == defaultFrtMajeur) {
      defaultFrtMajeur = null;
    }
    if (oldDonFrt == lastEmptyFkSto) {
      lastEmptyFkSto = null;
    }
    if (this.listFrt != null) {
      if (this.listFrt.contains(oldDonFrt)) {
        this.listFrt.remove(oldDonFrt);
      }
    }
  }

  public void removeAllListFrt() {
    defaultFrtMajeur = null;
    defaultFrtMineur = null;
    lastEmptyFkSto = null;
    if (listFrt != null) {
      listFrt.clear();
    }
  }
}
