/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.transformer;

import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;

/**
 *
 * @author Frederic Deniger
 */
public class EnumNumberFormatter implements CtuluNumberFormatI {

  final TIntObjectHashMap<ItemEnum> itemEnumByValue;

  public EnumNumberFormatter(PropertyNature nature) {
    itemEnumByValue = nature.getItemEnumByValue();
  }

  public EnumNumberFormatter(ItemVariable variable) {
    this(variable.getNature());
  }

  public EnumNumberFormatter(TIntObjectHashMap<ItemEnum> itemEnumByValue) {
    this.itemEnumByValue = itemEnumByValue;
  }

  @Override
  public String format(double _d) {
    ItemEnum get = itemEnumByValue.get((int) _d);
    return get == null ? Integer.toString((int) _d) : get.getName();
  }

  @Override
  public CtuluNumberFormatI getCopy() {
    return new EnumNumberFormatter(itemEnumByValue);
  }

  @Override
  public String toLocalizedPattern() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean isDecimal() {
    return false;
  }
}
