/**
 * *********************************************************************
 * Module: OrdCalcPseudoPermIniCalcPrecedent.java Author: deniger Purpose: Defines the Class OrdCalcPseudoPermIniCalcPrecedent
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

public class OrdCalcPseudoPermIniCliche extends OrdCalcPseudoPerm {

  private IniCalcCliche iniCalcCliche = new IniCalcCliche();

  @PropertyDesc(i18n = "iniCalcCliche.property")
  public IniCalcCliche getIniCalcCliche() {
    return iniCalcCliche;
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public boolean isInicCalClicheEmpty() {
    return iniCalcCliche == null || iniCalcCliche.isEmpty();
  }

  @Override
  public OrdCalc deepCloneButNotCalc(Calc newCalc) {
    OrdCalcPseudoPermIniCliche res = (OrdCalcPseudoPermIniCliche) super.deepCloneButNotCalc(newCalc);
    if (iniCalcCliche != null) {
      res.iniCalcCliche = iniCalcCliche.clone();
    }
    return res;
  }

  public void setIniCalcCliche(IniCalcCliche iniCalcCliche) {
    this.iniCalcCliche = iniCalcCliche;
  }
}
