package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

import java.util.ArrayList;
import java.util.List;

public class DonPrtGeoProfilSection extends DonPrtGeoNomme {
  private DonPrtGeoProfilSectionFenteData fente;
  private List<LitNumerote> litNumerote;
  private List<PtProfil> ptProfil;
  private List<DonPrtGeoProfilEtiquette> etiquettes;

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public EMHSousModele getParent() {
    if (emh == null) {
      return null;
    }
    return ((CatEMHSection) emh).getParent();
  }

  @UsedByComparison
  @Visibility(ihm = false)
  public List<LitNomme> getLitNommes() {
    List<LitNomme> res = new ArrayList<>();
    LitNomme last = null;
    for (LitNumerote lit : litNumerote) {
      if (last == null || !last.equals(lit.getNomLit())) {
        res.add(lit.getNomLit());
      }
      last = lit.getNomLit();
    }
    return res;
  }

  @Override
  public DonPrtGeo cloneDPTG() {
    DonPrtGeoProfilSection res = new DonPrtGeoProfilSection();
    res.initDataFrom(this);
    return this;
  }

  /**
   * recopie les noms, fente (clone), PtProfil (clone), LitNumerote(clone). Les litNomme ne doivent pas etre clonés
   *
   * @param other
   */
  public void initDataFrom(DonPrtGeoProfilSection other) {
    if (other == null) {
      return;
    }
    setNom(other.getNom());
    setCommentaire(other.getCommentaire());
    setFente(other.getFente() == null ? null : other.getFente().clone());
    initProfilLitEtiquettes(other);
  }

  public void initProfilLitEtiquettes(DonPrtGeoProfilSection other) {
    if (ptProfil == null) {
      ptProfil = new ArrayList<>();
    } else {
      ptProfil.clear();
    }
    final List<PtProfil> otherPtProfils = other.ptProfil;
    if (otherPtProfils != null) {
      for (PtProfil otherPtProfil : otherPtProfils) {
        ptProfil.add(otherPtProfil.clone());
      }
    }
    if (litNumerote == null) {
      litNumerote = new ArrayList<>();
    } else {
      litNumerote.clear();
    }
    if (other.litNumerote != null) {
      for (LitNumerote otherLitNumerote : other.litNumerote) {
        litNumerote.add(otherLitNumerote.clone());
      }
    }
    if (etiquettes == null) {
      etiquettes = new ArrayList<>();
    } else {
      etiquettes.clear();
    }
    if (other.etiquettes != null) {
      for (DonPrtGeoProfilEtiquette etiquette : other.etiquettes) {
        etiquettes.add(etiquette.clone());
      }
    }
  }

  public java.util.List<LitNumerote> getLitNumerote() {
    if (litNumerote == null) {
      litNumerote = new java.util.ArrayList<>();
    }
    return litNumerote;
  }

  /**
   * on renvoie la liste sans protection... a voir
   */
  public List<DonPrtGeoProfilEtiquette> getEtiquettes() {
    if (etiquettes == null) {
      etiquettes = new java.util.ArrayList<>();
    }
    return etiquettes;
  }

  public void setEtiquettes(List<DonPrtGeoProfilEtiquette> etiquettes) {
    if (this.etiquettes == null) {
      this.etiquettes = new java.util.ArrayList<>();
    }
    this.etiquettes.clear();
    if (etiquettes != null) {
      this.etiquettes.addAll(etiquettes);
    }
  }

  public int getNombreLitNumerote() {
    return getLitNumerote().size();
  }

  /**
   * @param newLitNumerote
   * @pdGenerated default setter
   */
  public void setLitNumerote(java.util.List<LitNumerote> newLitNumerote) {
    if (litNumerote != null) {
      litNumerote.clear();
    }
    for (java.util.Iterator iter = newLitNumerote.iterator(); iter.hasNext(); ) {
      addLitNumerote((LitNumerote) iter.next());
    }
  }

  /**
   * @param newLitNumerote
   * @pdGenerated default add
   */
  public void addLitNumerote(LitNumerote newLitNumerote) {
    if (newLitNumerote == null) {
      return;
    }
    if (this.litNumerote == null) {
      this.litNumerote = new java.util.ArrayList<>();
    }
    if (!this.litNumerote.contains(newLitNumerote)) {
      this.litNumerote.add(newLitNumerote);
    }
  }

  /**
   * @pdGenerated default getter
   */
  public java.util.List<PtProfil> getPtProfil() {
    if (ptProfil == null) {
      ptProfil = new java.util.ArrayList<>();
    }
    return ptProfil;
  }

  /**
   * @param newPtProfil
   * @pdGenerated default setter
   */
  public void setPtProfil(java.util.List<PtProfil> newPtProfil) {
    if (ptProfil != null) {
      ptProfil.clear();
    }
    for (java.util.Iterator iter = newPtProfil.iterator(); iter.hasNext(); ) {
      addPtProfil((PtProfil) iter.next());
    }
  }

  @UsedByComparison
  @Visibility(ihm = false)
  public int getPtProfilSize() {
    return ptProfil == null ? 0 : ptProfil.size();
  }

  /**
   * @param newPtProfil
   * @pdGenerated default add
   */
  public void addPtProfil(PtProfil newPtProfil) {
    if (newPtProfil == null) {
      return;
    }
    if (this.ptProfil == null) {
      this.ptProfil = new java.util.ArrayList<>();
    }
    if (!this.ptProfil.contains(newPtProfil)) {
      this.ptProfil.add(newPtProfil);
    }
  }

  /**
   * @param oldPtProfil
   */
  public void removePtProfil(PtProfil oldPtProfil) {
    if (oldPtProfil == null) {
      return;
    }
    if (this.ptProfil != null) {
      if (this.ptProfil.contains(oldPtProfil)) {
        this.ptProfil.remove(oldPtProfil);
      }
    }
  }

  public final DonPrtGeoProfilSectionFenteData getFente() {
    return fente;
  }

  /**
   * @param newFente
   */
  public final void setFente(DonPrtGeoProfilSectionFenteData newFente) {
    fente = newFente;
  }
}
