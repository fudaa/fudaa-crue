package org.fudaa.dodico.crue.metier.comparator;

import java.util.Comparator;
import java.util.Map;
import org.fudaa.dodico.crue.metier.emh.RelationEMH;

/**
 * @author deniger
 */
public class RelationEMHComparator implements Comparator<RelationEMH> {

  /**
   * singleton
   */
  public static final RelationEMHComparator INSTANCE = new RelationEMHComparator();

  private Map<RelationEMH, Integer> relationNotSorted;

  private RelationEMHComparator() {

  }

  public RelationEMHComparator(Map<RelationEMH, Integer> relationNotSorted) {
    this.relationNotSorted = relationNotSorted;
  }

  @Override
  public int compare(RelationEMH o1, RelationEMH o2) {
    if (o1 == o2) { return 0; }
    if (o1 == null) { return -1; }
    if (o2 == null) { return 1; }

    if (this.relationNotSorted != null) {
      if (this.relationNotSorted.containsKey(o1) && this.relationNotSorted.containsKey(o2)) { return this.relationNotSorted
          .get(o1).compareTo(this.relationNotSorted.get(o2)); }
    }

    return ObjetNommeByIdComparator.INSTANCE.compare(o1.getEmh(), o2.getEmh());
  }

}
