package org.fudaa.dodico.crue.metier.factory;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.*;

/**
 * @author deniger
 */
public class ScenarioBuilderForTest {
  public static final String BR_1 = "Br_1";
  public static final String ND_1 = "Nd_1";
  public static final String ND_2 = "Nd_2";

  public static EMHScenario createDefaultScenario(CrueConfigMetier ccm) {
    final EMHScenario scenario = new EMHScenario();
    final EMHModeleBase modele = new EMHModeleBase();
    OrdPrtCIniModeleBase cini = new OrdPrtCIniModeleBase(ccm);
    cini.setMethodeInterpol(EnumMethodeInterpol.LINEAIRE);
    modele.addInfosEMH(cini);
    EMHRelationFactory.addRelationContientEMH(scenario, modele);
    final EMHSousModele sousModele = new EMHSousModele();
    DonFrtConteneur frt = new DonFrtConteneur();
    DonFrtStrickler dontFrt = EMHFactory.createDonFrtZFkSto("FkSto_0");
    PtEvolutionFF point = new PtEvolutionFF();
    point.setAbscisse(0);
    point.setOrdonnee(0);
    dontFrt.getLoi().getEvolutionFF().getPtEvolutionFF().add(point);
    frt.addFrt(dontFrt);
    sousModele.addInfosEMH(frt);
    EMHRelationFactory.addRelationContientEMH(modele, sousModele);
    final EMHBrancheOrifice br = new EMHBrancheOrifice(BR_1);
    final CatEMHNoeud nd1 = new EMHNoeudNiveauContinu(ND_1);
    final CatEMHNoeud nd2 = new EMHNoeudNiveauContinu(ND_2);
    EMHRelationFactory.addNoeudAval(br, nd1);
    EMHRelationFactory.addNoeudAmont(br, nd2);
    EMHRelationFactory.addRelationContientEMH(sousModele, br);
    EMHRelationFactory.addRelationContientEMH(sousModele, nd1);
    EMHRelationFactory.addRelationContientEMH(sousModele, nd2);
    IdRegistry.install(scenario);
    return scenario;
  }
}
