package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 */
public class CalcTransBrancheOrificeManoeuvreRegul extends DonCLimMCommonItem implements CalcTransItem, DonCLimMWithManoeuvreRegulData {


  //attention modifier la constante CalcPseudoPermBrancheOrificeManoeuvre.PROP_SENS_OUV di-dessus si modification de nom
  private EnumSensOuv sensOuv;

  private LoiFF manoeuvreRegul;

  private String param;

  public CalcTransBrancheOrificeManoeuvreRegul(final CrueConfigMetier def) {
    final String enumValue = def.getDefaultStringEnumValue("sensOuv");
    if (enumValue != null) {
      try {
        sensOuv = EnumSensOuv.valueOf(enumValue);
      } catch (final Exception e) {
      }
    }
  }

  @Visibility(ihm = false)
  public ManoeuvreRegulData getData() {
    return new ManoeuvreRegulData(manoeuvreRegul, param);
  }

  public void setData(ManoeuvreRegulData data){
    if(data!=null){
      setManoeuvreRegul(data.manoeuvreRegul);
      setParam(data.param);
    }
  }

  @Visibility(ihm = false)
  public String getDesc() {
    if (getLoi() == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    return getLoi().getNom() + " / " + getParam();
  }

  @Override
  public EnumTypeLoi getTypeLoi() {
    return EnumTypeLoi.LoiQOuv;
  }

  @PropertyDesc(i18n = "manoeuvreRegul.property")
  public LoiFF getManoeuvreRegul() {
    return manoeuvreRegul;
  }

  @PropertyDesc(i18n = "sensOuv.property")
  public EnumSensOuv getSensOuv() {
    return sensOuv;
  }


  @PropertyDesc(i18n = "param.property")
  public String getParam() {
    return param;
  }


  public void setParam(final String param) {
    this.param = param;
  }

  /**
   * @param newSensOuv
   */
  public void setSensOuv(final EnumSensOuv newSensOuv) {
    sensOuv = newSensOuv;
  }


  @Override
  public String getShortName() {
    return BusinessMessages.getString("manoeuvreRegul.property");
  }

  public CalcTransBrancheOrificeManoeuvreRegul deepClone() {
    try {
      return (CalcTransBrancheOrificeManoeuvreRegul) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransBrancheOrificeManoeuvreRegul.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }

  public void setManoeuvreRegul(final LoiFF newManoeuvre) {
    if (this.manoeuvreRegul != null) {
      this.manoeuvreRegul.unregister(this);
    }

    manoeuvreRegul = newManoeuvre;
    if (this.manoeuvreRegul != null) {
      this.manoeuvreRegul.register(this);
    }
  }

  @Visibility(ihm = false)
  @Override
  public Loi getLoi() {
    return getManoeuvreRegul();
  }

  /**
   * @param newLoi
   */
  @Override
  public void setLoi(final Loi newLoi) {
    setManoeuvreRegul((LoiFF) newLoi);
  }
}
