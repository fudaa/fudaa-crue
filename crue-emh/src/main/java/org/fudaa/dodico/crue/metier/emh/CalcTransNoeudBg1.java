package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.BusinessMessages;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 */
public class CalcTransNoeudBg1 extends DonCLimMCommonItem  {


  @Override
  public String getShortName() {
    return BusinessMessages.getString("bg1.property");
  }

  CalcTransNoeudBg1 deepClone() {
    try {
      return (CalcTransNoeudBg1) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransNoeudBg1.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }
}
