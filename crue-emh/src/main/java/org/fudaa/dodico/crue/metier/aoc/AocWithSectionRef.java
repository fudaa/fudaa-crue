package org.fudaa.dodico.crue.metier.aoc;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;

/**
 * Interface pour classe contenant une sectionRef.
 *
 * @author deniger
 */
public interface AocWithSectionRef {
    /**
     *
     * @return la sectionRef
     */
    @PropertyDesc(i18n = "sectionRef.property")
    String getSectionRef();

    /**
     *
     * @param sectionRef nouvelle section ref
     */
    void setSectionRef(String sectionRef);
}
