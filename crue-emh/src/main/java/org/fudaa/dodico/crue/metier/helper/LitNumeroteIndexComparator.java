/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.helper;

import gnu.trove.TObjectIntHashMap;
import java.util.List;
import org.fudaa.dodico.crue.common.SafeComparator;

/**
 *
 * @author Frederic Deniger
 */
public class LitNumeroteIndexComparator extends SafeComparator<LitNumeroteIndex> {

  final TObjectIntHashMap<LitNumeroteIndex> positions = new TObjectIntHashMap<>();

  public LitNumeroteIndexComparator(List<LitNumeroteIndex> list) {
    for (int i = 0; i < list.size(); i++) {
      LitNumeroteIndex litNumeroteIndex = list.get(i);
      positions.put(litNumeroteIndex, i);

    }
  }

  @Override
  protected int compareSafe(LitNumeroteIndex o1, LitNumeroteIndex o2) {
    int compareTo = o1.compareTo(o2);
    if (compareTo == 0 && o1 != o2) {
      compareTo = positions.get(o1) - positions.get(o2);
    }
    return compareTo;
  }
}
