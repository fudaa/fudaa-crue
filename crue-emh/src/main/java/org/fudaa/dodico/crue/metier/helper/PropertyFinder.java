/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.metier.helper;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;

/**
 * Parmi les proprités ( java bean) d'une classe récupère celle dont le getter possède l'annotation@PropertyDesc.
 *
 * @author Frédéric Deniger
 */
public class PropertyFinder {

  public static class Description {

    public String propertyDisplay;
    public String property;
    public Class propertyClass;
    public Boolean readOnly;
  }

  public static class DescriptionIndexed {

    public Description description;
    public int order;
  }

  public List<DescriptionIndexed> getAvailablePropertiesNotSorted(Class in) {
    PropertyDescriptor[] propertyDescriptors = PropertyUtils.getPropertyDescriptors(in);
    List<DescriptionIndexed> res = new ArrayList<>();
    for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
      final Method getter = propertyDescriptor.getReadMethod();
      if (getter == null) {
        continue;
      }
      PropertyDesc annotation = getter.getAnnotation(PropertyDesc.class);
      if (annotation != null) {
        DescriptionIndexed item = new DescriptionIndexed();
        item.order = annotation.order();
        item.description = new Description();
        item.description.property = propertyDescriptor.getName();
        item.description.readOnly = annotation.readOnly();
        item.description.propertyClass = propertyDescriptor.getPropertyType();
        String i18nBundleBaseName = annotation.i18nBundleBaseName();
        try {
          if (StringUtils.isNotBlank(i18nBundleBaseName)) {
            item.description.propertyDisplay = ResourceBundle.getBundle(i18nBundleBaseName).getString(annotation.i18n());
          } else {

            item.description.propertyDisplay = BusinessMessages.getStringOrDefault(annotation.i18n(), annotation.i18n());
          }
        } catch (Exception e) {
          item.description.propertyDisplay = annotation.i18n();
        }
        res.add(item);
      }
    }
    return res;
  }

  public DescriptionIndexed getAvailableProperty(Class in, String property) {
    PropertyDescriptor[] propertyDescriptors = PropertyUtils.getPropertyDescriptors(in);
    for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
      if (propertyDescriptor.getName().equals(property)) {
        final Method getter = propertyDescriptor.getReadMethod();
        PropertyDesc annotation = getter.getAnnotation(PropertyDesc.class);
        if (annotation != null) {
          DescriptionIndexed item = new DescriptionIndexed();
          item.order = annotation.order();
          item.description = new Description();
          item.description.property = propertyDescriptor.getName();
          item.description.propertyClass = propertyDescriptor.getPropertyType();
          item.description.propertyDisplay = BusinessMessages.getString(annotation.i18n());
          return item;
        }
      }
    }
    return null;
  }
  private static final DescriptionIndexedComparator COMPARATOR = new DescriptionIndexedComparator();

  private static class DescriptionIndexedComparator extends SafeComparator<DescriptionIndexed> {

    @Override
    protected int compareSafe(DescriptionIndexed o1, DescriptionIndexed o2) {
      int delta = o1.order - o2.order;
      if (delta == 0) {
        delta = o1.description.propertyDisplay.compareTo(o2.description.propertyDisplay);
      }
      return delta;
    }
  }

  public List<Description> getAvailablePropertiesSorted(Class in) {
    List<DescriptionIndexed> init = getAvailablePropertiesNotSorted(in);
    Collections.sort(init, COMPARATOR);
    List<Description> res = new ArrayList<>(init.size());
    for (DescriptionIndexed descriptionIndexed : init) {
      res.add(descriptionIndexed.description);
    }
    return res;
  }
}
