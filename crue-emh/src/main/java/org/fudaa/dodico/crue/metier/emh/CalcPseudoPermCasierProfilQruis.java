package org.fudaa.dodico.crue.metier.emh;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

/**
 * CL n° ? en Crue9
 */
public class CalcPseudoPermCasierProfilQruis extends DonCLimMCommonItem implements CalcPseudoPermItem {

  /**
   */
  private double qruis;

  public CalcPseudoPermCasierProfilQruis(final CrueConfigMetier defaults) {
    qruis = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_QRUIS);
  }

  @PropertyDesc(i18n = "qruis.property")
  public double getQruis() {
    return qruis;
  }

  @Override
  public String getCcmVariableName() {
    return CrueConfigMetierConstants.PROP_QRUIS;
  }

  @Override
  public String getShortName() {
    return BusinessMessages.getString("qruis.property");
  }

  /**
   * @param newQruis
   */
  public void setQruis(final double newQruis) {
    qruis = newQruis;
  }

  /**
   * Fonction générique permettant de récupérer les données d'un CalPseudoPerm la valeur
   *
   */
  @Visibility(ihm = false)
  @Override
  public double getValue() {
    return getQruis();
  }

  /**
   * @param newVal
   */
  @Override
  public void setValue(final double newVal) {
    setQruis(newVal);
  }

  CalcPseudoPermCasierProfilQruis deepClone() {
    try {
      return (CalcPseudoPermCasierProfilQruis) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransNoeudNiveauContinuTarage.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }
}
