package org.fudaa.dodico.crue.metier.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.LitNumerote;

/**
 *
 * @author deniger
 */
public class LitNommeIndexed {

  private final LitNomme litNomme;
  private final List<LitNumeroteIndex> litNumerotes = new ArrayList<>();

  public LitNommeIndexed(LitNomme litNomme) {
    this.litNomme = litNomme;
  }

  public boolean containsOnlyEmptyLitNumerotes() {
    for (LitNumeroteIndex litNumeroteIndex : litNumerotes) {
      if (!litNumeroteIndex.isEmpty()) {
        return false;
      }
    }
    return true;
  }

  public boolean containsAnEmptyLitNumerotes() {
    for (LitNumeroteIndex litNumeroteIndex : litNumerotes) {
      if (litNumeroteIndex.isEmpty()) {
        return true;
      }
    }
    return false;
  }

  public boolean contains(int position) {
    return position >= getFirstIdx() && position <= getlastIdx();
  }

  public List<LitNumeroteIndex> getLitNumerotes() {
    return Collections.unmodifiableList(litNumerotes);
  }

  public int getFirstIdx() {
    return litNumerotes.get(0).getIdxStart();
  }

  public void fill(List<LitNumerote> target) {
    for (LitNumeroteIndex litNumerote : litNumerotes) {
      target.add(litNumerote.getLitNumerote());
    }
  }

  public int getLitNumerotesSize() {
    return litNumerotes.size();
  }

  public LitNumeroteIndex getLitNumeroteIndex(int idx) {
    return litNumerotes.get(idx);
  }

  public int getlastIdx() {
    return litNumerotes.get(litNumerotes.size() - 1).getIdxEnd();
  }

  public DonFrt getLastDonFrt() {
    return litNumerotes.get(litNumerotes.size() - 1).getLitNumerote().getFrot();
  }

  public DonFrt getFirstDonFrt() {
    return litNumerotes.get(0).getLitNumerote().getFrot();
  }

  /**
   * Tri les litNumerotes.
   *
   * @return true si les lits numerotes sont successifs.
   */
  public boolean isLitsSuccessifs() {
    sortLitNumerote();
    if (litNumerotes.size() > 1) {
      int size = litNumerotes.size();
      for (int i = 0; i < size - 1; i++) {
        if (!litNumerotes.get(i).isSuccessif(litNumerotes.get(i + 1))) {
          return false;
        }

      }
    }
    return true;
  }

  public void sortLitNumerote() {
    Collections.sort(litNumerotes);
  }

  public void addLitNumerote(LitNumeroteIndex lit) {
    litNumerotes.add(lit);
  }

  public void setLitNumerote(List<LitNumeroteIndex> lits) {
    litNumerotes.clear();
    litNumerotes.addAll(lits);
  }

  public LitNomme getLitNomme() {
    return litNomme;
  }

  public void applyLitNomme() {
    for (LitNumeroteIndex litNumerote : litNumerotes) {
      litNumerote.getLitNumerote().setNomLit(getLitNomme());
    }
  }
}
