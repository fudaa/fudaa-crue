/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.contrat.DoubleValuable;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum EnumMethodeInterpol implements ToStringInternationalizable, DoubleValuable {
  LINEAIRE(1, true, false, true), SAINT_VENANT(2, true, false, true, EnumRegleInterpolation.TOL_ND_Z, EnumRegleInterpolation.TOL_ST_Q), BAIGNOIRE(3, true, false, true),
  INTERPOL_ZIMP_AUX_SECTIONS(0, true, true, false);
  private final boolean brancheActiveRequireDPTI;
  private final boolean sectionActiveRequireDPTI;
  private final boolean noeudActiveRequireDPTI;
  private final List<EnumRegleInterpolation> requiredValParam;
  private final int id;

  EnumMethodeInterpol(final int id, final boolean brancheActiveRequireDPTI, final boolean sectionActiveRequireDPTI, final boolean noeudActiveRequireDPTI) {
    this.id = id;
    this.brancheActiveRequireDPTI = brancheActiveRequireDPTI;
    this.sectionActiveRequireDPTI = sectionActiveRequireDPTI;
    this.noeudActiveRequireDPTI = noeudActiveRequireDPTI;
    requiredValParam = Collections.emptyList();
  }

  EnumMethodeInterpol(final int id, final boolean brancheActiveRequireDPTI, final boolean sectionActiveRequireDPTI, final boolean noeudActiveRequireDPTI, final EnumRegleInterpolation... valParams) {
    this.id = id;
    this.brancheActiveRequireDPTI = brancheActiveRequireDPTI;
    this.sectionActiveRequireDPTI = sectionActiveRequireDPTI;
    this.noeudActiveRequireDPTI = noeudActiveRequireDPTI;
    this.requiredValParam = Collections.unmodifiableList(Arrays.asList(valParams));
  }

  @Override
  public double toDoubleValue() {
    return id;
  }

  public List<EnumRegleInterpolation> getRequiredValParam() {
    return requiredValParam;
  }

  @Override
  public String geti18n() {
    return BusinessMessages.getString("methodeInterpolation." + name() + ".shortName");
  }

  @Override
  public String geti18nLongName() {
    return BusinessMessages.getString("methodeInterpolation." + name() + ".longName");
  }

  public boolean isBrancheActiveRequireDPTI() {
    return brancheActiveRequireDPTI;
  }

  public boolean isCasierActiveRequireDPTI() {
    return true;
  }

  public boolean isNoeudActiveRequireDPTI() {
    return noeudActiveRequireDPTI;
  }

  public boolean isSectionActiveRequireDPTI() {
    return sectionActiveRequireDPTI;
  }
}
