package org.fudaa.dodico.crue.metier.factory;

import java.util.HashMap;
import java.util.List;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMHNoeudNiveauContinu;

/**
 * @author canel Factory pour les noeud
 */
public class EMHNoeudFactory {

  private final HashMap<String, CatEMHNoeud> mapIdNode;

  public EMHNoeudFactory() {
    this.mapIdNode = new HashMap<>();
  }

  public void addAll(List<CatEMHNoeud> nds) {
    for (CatEMHNoeud nd : nds) {
      mapIdNode.put(nd.getId(), nd);

    }

  }

  public CatEMHNoeud getCatEMHNoeud(String id) {
    String key = id.toUpperCase();
    if (!this.mapIdNode.containsKey(key)) {
      this.mapIdNode.put(key, new EMHNoeudNiveauContinu(id));
    }
    return this.mapIdNode.get(key);
  }

  public EMHNoeudNiveauContinu getNode(String id) {
    return (EMHNoeudNiveauContinu) getCatEMHNoeud(id);
  }
}
