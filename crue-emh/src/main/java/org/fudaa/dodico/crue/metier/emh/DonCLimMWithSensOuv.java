package org.fudaa.dodico.crue.metier.emh;

public interface DonCLimMWithSensOuv extends DonCLimM {
  EnumSensOuv getSensOuv();
}
