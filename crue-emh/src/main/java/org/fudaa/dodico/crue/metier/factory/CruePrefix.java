package org.fudaa.dodico.crue.metier.factory;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;

import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author cde
 */
public class CruePrefix {

  private CruePrefix(){}

  private static final Map<EnumCatEMH, String> ENUM_PREFIX = new EnumMap<>(EnumCatEMH.class);
  /**
   * Préfixe Scénario (EMH)
   */
  public static final String SEP = "_";
  public static final String P_SCENARIO = "Sc_";
  /**
   * Préfixe Mo_
   */
  public static final String P_MODELE = "Mo_";
  /**
   * Préfixe Me_
   */
  private static final String P_MODELE_E = "Me_";
  /**
   * Préfixe Se_
   */
  private static final String P_SS_MODELE_E = "Se_";
  /**
   * Préfixe Sm_
   */
  public static final String P_SS_MODELE = "Sm_";
  /**
   * Préfixe Nd_
   */
  public static final String P_NOEUD = "Nd_";
  /**
   * Préfixe Ca_
   */
  public static final String P_CASIER = "Ca_";
  /**
   * Préfixe Br_
   */
  public static final String P_BRANCHE = "Br_";
  /**
   * Préfixe St_
   */
  public static final String P_SECTION = "St_";
  /**
   * Préfixe Pc_
   */
  public static final String P_PROFIL_CASIER = "Pc_";
  /**
   * Préfixe Bc_
   */
  public static final String P_BATI_CASIER = "Bc_";
  /**
   * Préfixe Ps_
   */
  public static final String P_PROFIL_SECTION = "Ps_";
  /**
   * Préfixe Lt_
   */
  public static final String P_LIT = "Lt_";
  /**
   * Préfixe Fk_
   */
  public static final String P_FROTT_STRICKLER = "Fk_";
  public static final String P_FROTT_STRICKLER_STO = "FkSto_";
  /**
   * Préfixe Fn_
   */
  public static final String P_FROTT_MANNING = "Fn_";
  /**
   * Préfixe Cc_
   */
  public static final String P_CALCUL = "Cc_";
  public static final String P_CALCUL_PSEUDOPERMANENT = "Cc_P";
  public static final String P_CALCUL_TRANSITOIRE = "Cc_T";
  /**
   * Préfixe Pm_
   */
  public static final String P_VAL_PARAM = "Pm_";
  /**
   * Préfixe R
   */
  public static final String P_RUN = "R";

  static {
    ENUM_PREFIX.put(EnumCatEMH.BRANCHE, P_BRANCHE);
    ENUM_PREFIX.put(EnumCatEMH.CASIER, P_CASIER);
    ENUM_PREFIX.put(EnumCatEMH.MODELE, P_MODELE);
    ENUM_PREFIX.put(EnumCatEMH.MODELE_ENCHAINEMENT, P_MODELE_E);
    ENUM_PREFIX.put(EnumCatEMH.NOEUD, P_NOEUD);
    ENUM_PREFIX.put(EnumCatEMH.SCENARIO, P_SCENARIO);
    ENUM_PREFIX.put(EnumCatEMH.SECTION, P_SECTION);
    ENUM_PREFIX.put(EnumCatEMH.SOUS_MODELE, P_SS_MODELE);
    ENUM_PREFIX.put(EnumCatEMH.SOUS_MODELE_ENCHAINEMENT, P_SS_MODELE_E);
  }

  public static Set<String> getAllPrefix() {
    return new HashSet<>(ENUM_PREFIX.values());
  }

  /**
   * Renvoie true si le préfixe doit être ajouté au nom. Comparaison case sensitive
   *
   * @param prefixe le prefix a ajouter
   * @param nom le nom a tester
   * @return true si le préfixe doit être ajouté au nom
   */
  public static boolean prefixMustBeAdded(final String prefixe, final String nom) {

    // On n'ajoute pas de préfixe si le nom est null ou si le préfixe est null ou vide
    if (nom == null || prefixe == null || prefixe.length() == 0) {
      return false;
    }

    return !nom.startsWith(prefixe);
  }

  public static String getPrefix(final EnumTypeLoi typeLoi) {
    return typeLoi.getId() + "_";
  }

  /**
   * addPrefix("ST_TOTO","ST","PS") -> return PS_TOTO addPrefix("PS_TOTO","ST","PS") -> return PS_TOTO
   *
   * @param nom le nom a modifier
   * @param oldPref le prefixe a enleve: non case sensitive. Si non présent, dans le nom initial il n'est pas enleve.
   * @param newPref le nouveau prefixe. Si le nom initial commence déja par ce newPrefixe, il est remplace par le prefixe case-sensitive.
   * @return le nom modifie
   */
  public static String changePrefix(final String nom, final String oldPref, final String newPref) {
    if (oldPref != null && StringUtils.startsWithIgnoreCase(nom, oldPref)) {
      return newPref
          + StringUtils.removeStartIgnoreCase(nom, oldPref);
    }
    return newPref + StringUtils.removeStartIgnoreCase(nom, newPref);
  }

  /**
   * addPrefix("ST_TOTO","ST","PS") -> return PS_TOTO addPrefix("PS_TOTO","ST","PS") -> return PS_PS_TOTO
   *
   * @param nom the initial name
   * @param oldPref the oldPref to remove from nom if present
   * @param newPref le nouveau prefixe
   */
  public static String addPrefix(final String nom, final String oldPref, final String newPref) {
    if (oldPref != null && StringUtils.startsWithIgnoreCase(nom, oldPref)) {
      return newPref
          + StringUtils.removeStartIgnoreCase(nom, oldPref);
    }
    return newPref + nom;
  }

  /**
   * @param nom le nom a modifier
   * @param oldPref l'ancien prefixe a enlever
   * @param typeLoi le type de loi donnant le nouveau prefix
   * @return le nom de la loit
   */
  public static String changePrefix(final String nom, final String oldPref, final EnumTypeLoi typeLoi) {
    return changePrefix(cleanNom(nom), oldPref, getPrefix(typeLoi));
  }

  public static String addPrefix(final String nom, final EnumCatEMH typeEmh) {
    return changePrefix(cleanNom(nom), null, getPrefix(typeEmh));
  }

  public static String changePrefix(final String nom, final EnumCatEMH typeEmh) {
    return getNomAvecPrefixFor(nom, typeEmh);
  }

  public static String addPrefix(final String newPref, final String nom) {
    return changePrefix(cleanNom(nom), null, newPref);
  }

  public static String addPrefixs(final String nomEMH, final EnumTypeLoi loi, final EnumCatEMH typeEMH) {
    if (EnumTypeLoi.LoiTQruis.equals(loi)) {
      return changePrefix(nomEMH, getPrefix(typeEMH), getPrefix(loi));
    }
    return changePrefix(getNomAvecPrefixFor(nomEMH, typeEMH), null, getPrefix(loi));
  }

  public static String getNomAvecPrefixFor(final String nom, final EnumCatEMH type) {
    if (nom == null) {
      return null;
    }
    final String prefix = getPrefix(type);
    return getNomAvecPrefixe(prefix, nom);
  }

  public static String getPrefix(final EnumCatEMH type) {
    return ENUM_PREFIX.get(type);
  }

  /**
   * La taille max autorisé pour les noms crue 9
   */
  public static final int NB_CAR_MAX_CRUE9 = 16;
  /**
   * La taille max autorisé pour les noms crue 9 des casiers profils.
   */
  public static final int NB_CAR_MAX_CRUE9_CASIER_PROFIL = 12;
  /**
   * Le nombre max de caractère autorise dans un nom
   */
  public static final int NB_CAR_MAX = 32;

  private static String cleanNom(final String nom) {
    return nom == null ? null : nom.replace('#', '_').replace('/', '_');
  }

  /**
   * @param prefixe le prefixe qui doit etre utilise
   * @param nomInitialNonClean le nom intial
   * @return le nom transforme.
   */
  public static String getNomAvecPrefixe(final String prefixe, final String nomInitialNonClean) {
    // pas de nom initial: on ne fait rien
    if (nomInitialNonClean == null) {
      return null;
    }
    final String nomInitial = cleanNom(nomInitialNonClean);

    if (prefixMustBeAdded(prefixe, nomInitial)) {
      // le nom comporte deja un prefixe connu, on le change
      final String usedPrefix = startWithKnownPrefix(nomInitial);
      if (usedPrefix != null) {
        return changePrefix(nomInitial, usedPrefix, prefixe);
      }
      return (prefixe + nomInitial);
    }
    return nomInitial;
  }

  /**
   * @param nomInitial le nom a traiter
   * @return le prefixe connu utilise par le nom: attention non case sensitive.
   */
  private static String startWithKnownPrefix(final String nomInitial) {
    for (final String pref : ENUM_PREFIX.values()) {
      if (!prefixMustBeAdded(pref, nomInitial)) {
        return pref;
      }
      final String upperCase = pref.toUpperCase();
      if (!prefixMustBeAdded(upperCase, nomInitial.toUpperCase())) {
        return upperCase;
      }
    }
    //pour les frottements et enlever le Fk_ inital.
    if (!prefixMustBeAdded(CruePrefix.P_FROTT_STRICKLER, nomInitial)) {
      return CruePrefix.P_FROTT_STRICKLER;
    }
    if (!prefixMustBeAdded(CruePrefix.P_FROTT_STRICKLER.toUpperCase(), nomInitial)) {
      return CruePrefix.P_FROTT_STRICKLER.toUpperCase();
    }
    return null;
  }
}
