package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

import java.util.Collection;

/**
 * type 20 Crue9
 */
public class DonCalcSansPrtBrancheSaintVenant extends DonCalcSansPrt implements Cloneable {
  public static final String PROP_COEFBETA = "coefBeta";
  public static final String PROP_COEFRUISQDM = "coefRuisQdm";
  public static final String PROP_COEFRUIS = "coefRuis";
  private double coefBeta;
  private double coefRuisQdm;
  private double coefRuis;

  public DonCalcSansPrtBrancheSaintVenant(final CrueConfigMetier defaults) {
    coefBeta = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_COEF_BETA);
    coefRuisQdm = defaults.getDefaultDoubleValue("coefRuisQdm");
    coefRuis = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_COEF_RUIS);
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  public DonCalcSansPrtBrancheSaintVenant(final DonCalcSansPrtBrancheSaintVenant source) {
    coefBeta = source.coefBeta;
    coefRuis = source.coefRuis;
    coefRuisQdm = source.coefRuisQdm;
  }

  @Override
  public DonCalcSansPrt cloneDCSP() {
    try {
      return (DonCalcSansPrtBrancheSaintVenant) super.clone();
    } catch (CloneNotSupportedException e) {
    }
    return null;
  }

  @Override
  public void fillWithLoi(final Collection<Loi> target) {
  }

  @PropertyDesc(i18n = "coefBeta.property")
  public double getCoefBeta() {
    return coefBeta;
  }

  public void setCoefBeta(final double newCoefBeta) {
    coefBeta = newCoefBeta;
  }

  @PropertyDesc(i18n = "coefRuisQdm.property")
  public double getCoefRuisQdm() {
    return coefRuisQdm;
  }

  public void setCoefRuisQdm(final double newCoefRuisQdm) {
    coefRuisQdm = newCoefRuisQdm;
  }

  @PropertyDesc(i18n = "coefRuis.property")
  public double getCoefRuis() {
    return coefRuis;
  }

  public void setCoefRuis(final double newCoefRuis) {
    coefRuis = newCoefRuis;
  }
}
