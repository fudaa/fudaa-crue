package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.OrdResKey;

public class OrdResBrancheNiveauxAssocies extends OrdRes implements Cloneable {

  @Override
  public OrdResBrancheNiveauxAssocies clone() {
    try {
      return (OrdResBrancheNiveauxAssocies) super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why this ?");
  }
  private final static OrdResKey KEY = new OrdResKey(EnumBrancheType.EMHBrancheNiveauxAssocies);

  @Override
  public OrdResKey getKey() {
    return KEY;
  }

  @Override
  public String toString() {
    return "OrdResBrancheBarrageGenerique [ddeDz=" + getDdeValue("dz") + "]";
  }
}
