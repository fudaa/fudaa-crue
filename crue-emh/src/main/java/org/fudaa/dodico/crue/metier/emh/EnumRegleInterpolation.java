package org.fudaa.dodico.crue.metier.emh;

public enum EnumRegleInterpolation {

  /**
   * Valeur de paramètre : tolérance sur les égalités de cotes aux nœuds lors d'une interpolation Saint-Venant des conditions initiales. Valeur par
   * défaut : 0,01 m
   */
  TOL_ND_Z("Pm_TolNdZ", Double.TYPE),
  TOL_ST_Q("Pm_TolStQ", Double.TYPE);
  private final String variableName;
  private final Class variableType;

  /**
   * @param variableName
   */
  EnumRegleInterpolation(String variableName, Class variableType) {
    this.variableName = variableName;
    this.variableType = variableType;
  }

  public Class getVariableType() {
    return variableType;
  }

  /**
   * @return the variableName
   */
  public String getVariableName() {
    return variableName;
  }
  
}
