/**
 * *********************************************************************
 * Module: OrdResBrancheStrickler.java Author: deniger Purpose: Defines the Class OrdResBrancheStrickler
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.OrdResKey;

public class OrdResBrancheStrickler extends OrdRes implements Cloneable {

  private final static OrdResKey KEY = new OrdResKey(EnumBrancheType.EMHBrancheStrickler);

  @Override
  public OrdResKey getKey() {
    return KEY;
  }


  @Override
  public OrdResBrancheStrickler clone() {
    try {
      return (OrdResBrancheStrickler) super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why this ?");
  }

  @Override
  public String toString() {
    return "OrdResBrancheStrickler [ddeSplan=" + getDdeValue("splan") + ", ddeVol=" + getDdeValue("vol") + "]";
  }
}
