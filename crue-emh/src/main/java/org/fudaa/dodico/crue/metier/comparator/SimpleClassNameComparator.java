package org.fudaa.dodico.crue.metier.comparator;

import java.util.Comparator;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

/**
 * @author deniger
 */
public final class SimpleClassNameComparator implements Comparator {

  /**
   * singleton
   */
  public final static SimpleClassNameComparator INSTANCE = new SimpleClassNameComparator();

  protected SimpleClassNameComparator() {

  }

  @Override
  public int compare(Object o1, Object o2) {
    if (o1 == o2) { return 0; }
    if (o1 == null) { return -1; }
    if (o2 == null) { return 1; }
    int res = o1.getClass().getSimpleName().compareTo(o2.getClass().getSimpleName());
    if (res == 0 && (o1 instanceof ObjetWithID && o2 instanceof ObjetWithID)) {
      res = ObjetNommeByIdComparator.INSTANCE.compare((ObjetNomme) o1, (ObjetNomme) o2);
    }
    if (res == 0) {
      res = o1.hashCode() - o2.hashCode();
    }
    return res;
  }

}
