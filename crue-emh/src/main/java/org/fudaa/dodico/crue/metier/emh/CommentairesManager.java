package org.fudaa.dodico.crue.metier.emh;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;

/**
 * @author deniger
 */
public class CommentairesManager {

  private final Map<CrueFileType, CommentaireItem> commentaires = new EnumMap<>(CrueFileType.class);
  private Map<CrueFileType, CommentaireItem> commentairesIm;

  public Map<CrueFileType, CommentaireItem> getContent() {
    if (commentairesIm == null) {
      commentairesIm = Collections.unmodifiableMap(commentaires);
    }
    return commentairesIm;
  }

  public CommentairesManager() {

  }

  public CommentairesManager(CommentairesManager other) {
    if (other != null) {
      commentaires.putAll(other.commentaires);
    }
  }

  public CommentairesManager copy() {
    return new CommentairesManager(this);
  }


  public void setCommentaire(final CrueFileType type, final String com) {
    commentaires.put(type, new CommentaireItem(type, com));
  }

  public Collection<CommentaireItem> getCommentaires() {
    return commentaires.values();
  }

  public void setCommentaire(final FichierCrue type, final String com) {
    commentaires.put(type.getType(), new CommentaireItem(type.getType(), StringUtils.defaultString(com)));
  }

  public String getCommentaire(final CrueFileType type) {
    CommentaireItem commentaireItem = commentaires.get(type);
    return commentaireItem == null ? null : commentaireItem.getCommentaire();
  }

}
