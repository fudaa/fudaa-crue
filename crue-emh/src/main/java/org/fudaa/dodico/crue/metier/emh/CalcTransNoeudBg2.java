package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.BusinessMessages;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * CL de type 2 en Crue9
 */
public class CalcTransNoeudBg2 extends DonCLimMCommonItem  {


  @Override
  public String getShortName() {
    return BusinessMessages.getString("bg2.property");
  }

  CalcTransNoeudBg2 deepClone() {
    try {
      return (CalcTransNoeudBg2) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransNoeudBg2.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }
}
