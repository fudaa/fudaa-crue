/**
 * *********************************************************************
 * Module: OrdResNoeudNiveauContinu.java Author: deniger Purpose: Defines the Class OrdResNoeudNiveauContinu
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.OrdResKey;

public class OrdResNoeudNiveauContinu extends OrdRes implements Cloneable {

  public final static OrdResKey KEY = new OrdResKey(EnumCatEMH.NOEUD);

  @Override
  public OrdResKey getKey() {
    return KEY;
  }

  @Override
  public OrdResNoeudNiveauContinu clone() {
    try {
      return (OrdResNoeudNiveauContinu) super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why this ?");
  }

  @Override
  public String toString() {
    return "OrdResNoeudNiveauContinu [ddeZ=" + getDdeValue("z") + "]";
  }
}
