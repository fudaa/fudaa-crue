package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

public class EMHSectionSansGeometrie extends CatEMHSection {



  public EMHSectionSansGeometrie(final String nom) {
    super(nom);
  }
  
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public EnumSectionType getSectionType() {
    return EnumSectionType.EMHSectionSansGeometrie;
  }

  @Override
  @Visibility(ihm = false)
  public EMHSectionSansGeometrie copyShallowFirstLevel() {
    return  copyPrimitiveIn(new EMHSectionSansGeometrie(getNom()));
  }
}
