/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.contrat.DoubleValuable;

/**
 * Les variables utilisable dans Crue
 */
public enum EnumLitPosition implements DoubleValuable {

  /**
   * 0 si le lit n'est pas en bordure du profil ou du lit actif
   */
  PAS_BORDURE(0),
  /**
   * 1 si le lit est en bordure droite du lit actif
   */
  BORDURE_DROITE(1),
  /**
   * 2 si le lit est en bordure gauche du lit actif
   */
  BORDURE_GAUCHE(2),
  /**
   * 3 si le lit est en bordure des deux côtés
   */
  BORDURE_2_COTES(3);
  final int id;

  EnumLitPosition(final int id) {
    this.id = id;
  }

  @Override
  public double toDoubleValue() {
    return id;
  }
}
