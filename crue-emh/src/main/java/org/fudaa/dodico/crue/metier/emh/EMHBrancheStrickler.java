package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 * type 6 en Crue9
 * 
 */
public class EMHBrancheStrickler extends CatEMHBranche {

  public EMHBrancheStrickler(final String nom) {
    super(nom);
  }
  
   @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public EnumBrancheType getBrancheType() {
    return EnumBrancheType.EMHBrancheStrickler;
  }


  @Override
  @Visibility(ihm = false)
  public EMHBrancheStrickler copyShallowFirstLevel() {
    return  copyPrimitiveIn(new EMHBrancheStrickler(getNom()));
  }

}
