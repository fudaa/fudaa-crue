/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

/**
 * Attention: le nom de l'enum est utilise pour retrouve son nom. Si vous changer ce nom, il faudra ajouter la traduction dans
 * businessMessages.properties
 * @author deniger
 */
public enum EnumNoeudType implements ToStringInternationalizable {

  EMHNoeudNiveauContinu;

  /**
   *
   * @return le nom de l'EMH.
   */
  @Override
  public String geti18n() {
    return BusinessMessages.getString(name() + ".shortName");
  }

  @Override
  public String geti18nLongName() {
    return BusinessMessages.getString(name() + ".longName");
  }
}
