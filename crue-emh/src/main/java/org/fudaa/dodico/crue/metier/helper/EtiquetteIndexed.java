/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.helper;

import java.util.List;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilEtiquette;

/**
 * Permet d'avoir une étiquette positionnée avec l'indice.
 *
 * @author Frederic Deniger
 */
public class EtiquetteIndexed {

  DonPrtGeoProfilEtiquette etiquette;
  int idx;

  public DonPrtGeoProfilEtiquette getEtiquette() {
    return etiquette;
  }

  public void setEtiquette(DonPrtGeoProfilEtiquette etiquette) {
    this.etiquette = etiquette;
  }

  public int getIdx() {
    return idx;
  }

  public void setIdx(int idx) {
    this.idx = idx;
  }

  public static DonPrtGeoProfilEtiquette find(List<EtiquetteIndexed> etiquettes, ItemEnum itemEnum) {
    if (etiquettes == null || itemEnum == null) {
      return null;
    }
    for (EtiquetteIndexed etiquette : etiquettes) {
      if (itemEnum.equals(etiquette.getEtiquette().getTypeEtiquette())) {
        return etiquette.getEtiquette();
      }
    }
    return null;
  }
}
