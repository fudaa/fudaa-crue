package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

public class EMHCasierMNT extends CatEMHCasier {

  public EMHCasierMNT(final String nom) {
    super(nom);
  }
  
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public EnumCasierType getCasierType(){
    return EnumCasierType.EMHCasierMNT;
  }


  @Override
  @Visibility(ihm = false)
  public EMHCasierMNT copyShallowFirstLevel() {
    return  copyPrimitiveIn(new EMHCasierMNT(getNom()));
  }
  

}
