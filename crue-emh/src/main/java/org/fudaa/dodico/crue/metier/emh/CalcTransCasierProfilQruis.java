package org.fudaa.dodico.crue.metier.emh;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;

/**
 * CL de type ? en Crue9
 *
 */
public class CalcTransCasierProfilQruis extends DonCLimMCommonItem implements CalcTransItem {

  /**
   * Loi HydrogrammeQruis : VarAbscisse = t VarOrdonnée = Qruis
   */
  private LoiDF hydrogrammeQruis;

  /**
   * Loi HydrogrammeQruis : VarAbscisse = t VarOrdonnée = Qruis
   *
   */
  @PropertyDesc(i18n = "hydrogrammeQruis.property")
  public LoiDF getHydrogrammeQruis() {
    return hydrogrammeQruis;
  }

  @Override
  public String getShortName() {
    return BusinessMessages.getString("hydrogrammeQruis.property");
  }

  @Override
  public EnumTypeLoi getTypeLoi() {
    return EnumTypeLoi.LoiTQruis;
  }

  /**
   * Loi HydrogrammeQruis : VarAbscisse = t VarOrdonnée = Qruis
   *
   * @param newHydrogrammeQruis
   */
  public void setHydrogrammeQruis(final LoiDF newHydrogrammeQruis) {
    if (this.hydrogrammeQruis != null) {
      this.hydrogrammeQruis.unregister(this);
    }

    hydrogrammeQruis = newHydrogrammeQruis;
    if (this.hydrogrammeQruis != null) {
      this.hydrogrammeQruis.register(this);
    }
  }

  @Visibility(ihm = false)
  @Override
  public Loi getLoi() {
    return getHydrogrammeQruis();
  }

  @Override
  public void setLoi(final Loi newLoi) {
    setHydrogrammeQruis((LoiDF) newLoi);
  }

  CalcTransCasierProfilQruis deepClone() {
    try {
      return (CalcTransCasierProfilQruis) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransNoeudNiveauContinuTarage.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }
}
