package org.fudaa.dodico.crue.metier.etude;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.ui.OrdCalcScenarioUiState;

/**
 * Entite organisant les calculs sur des modeles simples ou enchaines.
 *
 * @author Adrien Hadoux
 */
public class ManagerEMHScenario extends ManagerEMHContainer<ManagerEMHModeleBase> implements Cloneable {

  /**
   * la liste des runs associes au scenraio.
   */
  private List<EMHRun> listeRuns;
  private List<EMHRun> listeRunsImmuable;
  /**
   * données utilisateur concernant les ordre de calculs
   */
  private OrdCalcScenarioUiState uiOCalData;
  String linkedscenarioCrue9;
  /**
   * Run courant.
   */
  private EMHRun runCourant;

  public ManagerEMHScenario() {
    super(CrueLevelType.SCENARIO);
  }

  public ManagerEMHScenario(final String nom) {
    super(CrueLevelType.SCENARIO, nom);
  }

  /**
   * @param uiOCalData données utilisateur concernant les ordre de calculs
   */
  public void setUiOCalData(OrdCalcScenarioUiState uiOCalData) {
    this.uiOCalData = uiOCalData;
  }

  /**
   * @return données utilisateur concernant les ordre de calculs
   */
  public OrdCalcScenarioUiState getUiOCalData() {
    return uiOCalData;
  }

  @Override
  public String getEMHType() {
    return EMHScenario.class.getSimpleName();
  }

  public String getLinkedscenarioCrue9() {
    return linkedscenarioCrue9;
  }

  public void setLinkedscenarioCrue9(String linkedscenarioCrue9) {
    this.linkedscenarioCrue9 = linkedscenarioCrue9;
  }

  /**
   * Accepte ou non le fichier si son id est unique.
   *
   * @param id
   * @return
   */
  public boolean acceptRun(final String id) {
    return !existRunInScenario(id);
  }

  @Override
  public Map<String, FichierCrueParModele> addAllFileUsed(final Map<String, FichierCrueParModele> map) {
    ManagerEMHSousModele.addAllFile(this, map, null);
    for (final ManagerEMHModeleBase fils : getFils()) {
      fils.addAllFileUsed(map);
    }
    return map;
  }

  /**
   * Ajoute un Run .
   *
   * @param run
   * @return
   */
  public boolean addRunToScenario(final EMHRun run) {
    if (listeRuns == null) {
      listeRuns = new ArrayList<>();
    }
    return listeRuns.add(run);
  }

  /**
   * Retourne true si le fichier existe en base reference par son id.
   *
   * @param id
   * @return
   */
  public boolean existRunInScenario(final String id) {
    return getRunFromScenar(id) != null;

  }

  /**
   * @param dirOfRun le repertoire du run.
   * @return l'association id->File des fichiers resultats du run courant.
   */
  protected Map<String, RunFile> getRunFilesResultatIn(final File dirOfRun) {
    final Map<String, RunFile> res = new HashMap<>();
    if (getFils().isEmpty()) {
      return Collections.emptyMap();
    }
    if (isCrue9()) {
      // sto/str/fcb/
      final ManagerEMHModeleBase managerEMHModeleBase = getFils().get(0);
      final File base = new File(dirOfRun, managerEMHModeleBase.getNom());
      final FichierCrueManager fichiers = managerEMHModeleBase.getListeFichiers();
      final String idDc = fichiers.getFile(CrueFileType.DC).getNom();
      final String idDh = fichiers.getFile(CrueFileType.DH).getNom();
      final String nameDc = CtuluLibFile.getSansExtension(idDc);
      final String nameDh = CtuluLibFile.getSansExtension(idDh);
      // STO,STR ne dépendent que du DC
      String resKeySTO = getResKey(managerEMHModeleBase, CrueFileType.STO);
      res.put(resKeySTO, new RunFile(resKeySTO, managerEMHModeleBase, new File(base, nameDc + ".STO"), CrueFileType.STO));
      String resKeySTR = getResKey(managerEMHModeleBase, CrueFileType.STR);
      res.put(resKeySTR, new RunFile(resKeySTR, managerEMHModeleBase, new File(base, nameDc + ".STR"), CrueFileType.STR));
      // FCB ne dépendent que du DH
      String resKeyFCB = getResKey(managerEMHModeleBase, CrueFileType.FCB);
      res.put(resKeyFCB, new RunFile(resKeyFCB, managerEMHModeleBase, new File(base, nameDh + ".FCB"), CrueFileType.FCB));

      return res;

    } else {
      // RPTI and RPTG
      List<ManagerEMHModeleBase> modeles = getFils();
      for (ManagerEMHModeleBase managerEMHModeleBase : modeles) {
        final File base = new File(dirOfRun, managerEMHModeleBase.getNom());
        String key = getResKey(managerEMHModeleBase, CrueFileType.RPTI);
        FichierCrue opti = managerEMHModeleBase.getListeFichiers().getFile(CrueFileType.OPTI);
        if (opti != null) {
          res.put(key, new RunFile(key, managerEMHModeleBase, new File(base, replaceType(opti.getNom(), ".opti.", ".rpti.")),
                  CrueFileType.RPTI));
        }

        key = getResKey(managerEMHModeleBase, CrueFileType.RPTR);
        FichierCrue optr = managerEMHModeleBase.getListeFichiers().getFile(CrueFileType.OPTR);
        if (optr != null) {
          res.put(key, new RunFile(key, managerEMHModeleBase, new File(base, replaceType(optr.getNom(), ".optr.", ".rptr.")),
                  CrueFileType.RPTR));
        }

        key = getResKey(managerEMHModeleBase, CrueFileType.RCAL);
        FichierCrue ocal = getListeFichiers().getFile(CrueFileType.OCAL);
        if (ocal != null) {
          res.put(key, new RunFile(key, managerEMHModeleBase, new File(base, replaceType(ocal.getNom(), ".ocal.", ".rcal.")),
                  CrueFileType.RCAL));
        }
        key = getResKey(managerEMHModeleBase, CrueFileType.RPTG);
        FichierCrue optg = managerEMHModeleBase.getListeFichiers().getFile(CrueFileType.OPTG);
        if (optg != null) {
          res.put(key, new RunFile(key, managerEMHModeleBase, new File(base, replaceType(optg.getNom(), ".optg.", ".rptg.")),
                  CrueFileType.RPTG));
        }
      }

    }
    return res;
  }

  public static String replaceType(String name, String oldTypeLowerCase, String newTypeLowerCase) {
    if (name.contains(oldTypeLowerCase)) {
      return name.replace(oldTypeLowerCase, newTypeLowerCase);
    } else if (name.contains(oldTypeLowerCase.toUpperCase())) {
      return name.replace(oldTypeLowerCase.toUpperCase(), newTypeLowerCase.toUpperCase());
    }
    return name;
  }

  public static String getResKey(ManagerEMHModeleBase managerEMHModeleBase, CrueFileType fileType) {
    return managerEMHModeleBase.getNom() + "." + fileType.toString().toLowerCase();
  }

  public static String getResKey(EMHModeleBase modele, CrueFileType fileType) {
    return modele.getNom() + "." + fileType.toString().toLowerCase();
  }

  public boolean hasRun() {
    return CollectionUtils.isNotEmpty(listeRuns);
  }

  public List<EMHRun> getListeRuns() {
    if (CollectionUtils.isEmpty(listeRuns)) {
      return Collections.emptyList();
    }
    if (listeRunsImmuable == null) {
      listeRunsImmuable = Collections.unmodifiableList(listeRuns);
    }
    return listeRunsImmuable;
  }

  public EMHRun getRunCourant() {
    return runCourant;
  }

  /**
   * Retourne le Modele de la base.
   *
   * @param id
   * @return
   */
  public EMHRun getRunFromScenar(final String id) {
    return EMHRun.findById(id, listeRuns);
  }

  public void setListeRuns(final List<EMHRun> listeRuns) {
    this.listeRuns = listeRuns;
  }

  public void setRunCourant(final EMHRun runCourant) {
    this.runCourant = runCourant;
  }

  public boolean containRun(EMHRun runInScenario) {
    if (runInScenario == null) {
      return false;
    }
    return listeRuns.contains(runInScenario);
  }

  public boolean removeRun(EMHRun runInScenario) {
    if (runInScenario == null) {
      return false;
    }
    if (listeRuns.remove(runInScenario)) {
      if (getRunCourant() != null && getRunCourant().getId().equals(runInScenario.getId())) {
        setRunCourant(null);
      }
      return true;
    }
    return false;
  }

  public List<ManagerEMHSousModele> getAllSousModeles() {
    List<ManagerEMHSousModele> sousModeles = new ArrayList<>();

    for (ManagerEMHModeleBase modele : this.getFils()) {
      sousModeles.addAll(modele.getFils());
    }

    return sousModeles;
  }
}
