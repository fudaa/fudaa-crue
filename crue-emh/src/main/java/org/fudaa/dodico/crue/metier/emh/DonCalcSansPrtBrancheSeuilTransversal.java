package org.fudaa.dodico.crue.metier.emh;

import java.util.ArrayList;
import java.util.Collection;

/**
 * type 2 Crue9
 */
public class DonCalcSansPrtBrancheSeuilTransversal extends DonCalcSansPrtBrancheSeuil {
  private java.util.List<ElemSeuilAvecPdc> elemSeuilAvecPdc;

  public java.util.List<ElemSeuilAvecPdc> getElemSeuilAvecPdc() {
    if (elemSeuilAvecPdc == null) {
      elemSeuilAvecPdc = new java.util.ArrayList<>();
    }
    return elemSeuilAvecPdc;
  }

  @Override
  public void fillWithLoi(Collection<Loi> target) {
  }

  @Override
  public DonCalcSansPrt cloneDCSP() {
    try {
      DonCalcSansPrtBrancheSeuilTransversal res = (DonCalcSansPrtBrancheSeuilTransversal) super.clone();
      if (elemSeuilAvecPdc != null) {
        res.elemSeuilAvecPdc = new ArrayList<>();
        elemSeuilAvecPdc.forEach(seuil -> res.elemSeuilAvecPdc.add(seuil.clone()));
      }
      return res;
    } catch (CloneNotSupportedException e) {
    }
    return null;
  }

  /**
   * @param newElemSeuilAvecPdc
   */
  public void setElemSeuilAvecPdc(final java.util.List<ElemSeuilAvecPdc> newElemSeuilAvecPdc) {
    removeAllElemSeuilAvecPdc();
    for (final java.util.Iterator iter = newElemSeuilAvecPdc.iterator(); iter.hasNext(); ) {
      addElemSeuilAvecPdc((ElemSeuilAvecPdc) iter.next());
    }
  }

  /**
   * @param newElemSeuilAvecPdc
   */
  public void addElemSeuilAvecPdc(final ElemSeuilAvecPdc newElemSeuilAvecPdc) {
    if (newElemSeuilAvecPdc == null) {
      return;
    }
    if (this.elemSeuilAvecPdc == null) {
      this.elemSeuilAvecPdc = new java.util.ArrayList<>();
    }
    this.elemSeuilAvecPdc.add(newElemSeuilAvecPdc);
  }

  public void removeAllElemSeuilAvecPdc() {
    if (elemSeuilAvecPdc != null) {
      elemSeuilAvecPdc.clear();
    }
  }
}
