package org.fudaa.dodico.crue.metier.comparator;

import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

/**
 *
 * @author deniger
 */
public class ToStringInternationalizableComparator extends SafeComparator<ToStringInternationalizable> {

  public static final ToStringInternationalizableComparator INSTANCE = new ToStringInternationalizableComparator(false);
  public static final ToStringInternationalizableComparator INSTANCE_LONG_NAME = new ToStringInternationalizableComparator(true);
  final boolean useLong;

  public ToStringInternationalizableComparator(boolean useLong) {
    this.useLong = useLong;
  }

  @Override
  protected int compareSafe(ToStringInternationalizable o1, ToStringInternationalizable o2) {
    if (useLong) {
      return compareString(o1.geti18nLongName(), o2.geti18nLongName());
    }
    return compareString(o1.geti18n(), o2.geti18n());
  }
}
