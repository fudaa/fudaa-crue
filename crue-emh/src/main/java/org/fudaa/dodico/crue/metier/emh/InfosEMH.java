/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;

public interface InfosEMH {
  EnumInfosEMH getCatType();

  /**
   * @return la valeur réelle affectée à l'EMH. Prend en compte les relations de l'EMH. Par exemple, une section
   *         appartenant a une branche non active n'est pas active.
   */
  boolean getActuallyActive();

  /**
   * @return l'emh contenant cette info.
   */
  EMH getEmh();

  /**
   * Attention pour les DontPrt... il s'agit d'un ajout car peut contenir plusieurs EMH parentes
   * @param emh
   */
  void setEmh(EMH emh);
  
  /**
   * Appele si enleve d'une EMH
   * @param emh 
   */
  void removeEMH(EMH emh);

  /**
   * @return le type
   */
  String getType();
  
  /**
   * @return le type
   */
  @Visibility(ihm=false)
  String getTypei18n();


//  InfosEMH copy();

}
