package org.fudaa.dodico.crue.metier.emh;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 *
 * @author deniger
 */
public class PrendreClicheFinPermanent implements Cloneable {

  private String nomFic = StringUtils.EMPTY;

  public PrendreClicheFinPermanent() {
  }

  public PrendreClicheFinPermanent(String nomFic) {
    this.nomFic = nomFic;
  }

  @Override
  public PrendreClicheFinPermanent clone() {
    try {
      return (PrendreClicheFinPermanent) super.clone();
    } catch (CloneNotSupportedException ex) {
      Logger.getLogger(PrendreClicheFinPermanent.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("why");
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + " " + nomFic;
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public boolean isEmpty() {
    return StringUtils.isBlank(nomFic);
  }

  public String getNomFic() {
    return nomFic;
  }

  public void setNomFic(String nomFic) {
    this.nomFic = nomFic;
  }
}
