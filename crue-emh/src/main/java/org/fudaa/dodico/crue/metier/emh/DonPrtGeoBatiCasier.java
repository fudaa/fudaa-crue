package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;

public class DonPrtGeoBatiCasier extends DonPrtGeoNomme implements Cloneable {
  private double splanBati;
  private double zBatiTotal;

  public DonPrtGeoBatiCasier(final CrueConfigMetier def) {
    splanBati = def.getDefaultDoubleValue("splanBati");
    zBatiTotal = def.getDefaultDoubleValue("zBatiTotal");
  }

  @Override
  public DonPrtGeo cloneDPTG() {
    try {
      return (DonPrtGeo) clone();
    } catch (CloneNotSupportedException e) {
    }
    return null;
  }

  @Override
  protected Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  public double getSplanBati() {
    return splanBati;
  }

  public void setSplanBati(final double newSplanBati) {
    splanBati = newSplanBati;
  }

  public double getZBatiTotal() {
    return zBatiTotal;
  }

  public void setZBatiTotal(final double newZBatiTotal) {
    zBatiTotal = newZBatiTotal;
  }
}
