/**
 * *********************************************************************
 * Module: OrdResBrancheSeuilTransversal.java Author: deniger Purpose: Defines the Class OrdResBrancheSeuilTransversal
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.OrdResKey;

public class OrdResBrancheSeuilTransversal extends OrdRes implements Cloneable {

  public final static OrdResKey KEY = new OrdResKey(EnumBrancheType.EMHBrancheSeuilTransversal);

  @Override
  public OrdResKey getKey() {
    return KEY;
  }

  @Override
  public OrdResBrancheSeuilTransversal clone() {
    try {
      return (OrdResBrancheSeuilTransversal) super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why this ?");
  }


  @Override
  public String toString() {
    return "OrdResBrancheSeuilTransversal [ddeRegime=" + getDde("regimeSeuil") + "]";
  }
}
