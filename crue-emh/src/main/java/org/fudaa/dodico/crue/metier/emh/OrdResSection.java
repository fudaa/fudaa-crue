/**
 * *********************************************************************
 * Module: OrdResSectionProfil.java Author: deniger Purpose: Defines the Class OrdResSectionProfil
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.OrdResKey;

public class OrdResSection extends OrdRes implements Cloneable {

  public final static OrdResKey KEY = new OrdResKey(EnumCatEMH.SECTION);

  @Override
  public OrdResKey getKey() {
    return KEY;
  }

  public OrdResSection() {
  }

  @Override
  public OrdResSection clone() {
    try {
      return (OrdResSection) super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why this ?");
  }

  @Override
  public String toString() {
    return "OrdResSectionProfil [ddeDact=" + getDdeValue("dact") + ", ddeDtot=" + getDdeValue("dtot") + ", ddeFr=" + getDdeValue("fr") + ", ddeH="
            + getDdeValue("h")
            + ", ddeHs=" + getDdeValue("hs") + ",...]";
  }
}