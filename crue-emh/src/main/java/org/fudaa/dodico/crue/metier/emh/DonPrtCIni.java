package org.fudaa.dodico.crue.metier.emh;

/**
 */
public abstract class DonPrtCIni<T extends DonPrtCIni> extends AbstractInfosEMH implements Cloneable {
  /**
   */
  @Override
  public final EnumInfosEMH getCatType() {
    return EnumInfosEMH.DON_PRT_CINI;
  }

  public abstract void initFrom(T dpti);

  @Override
  protected Object clone() {
    try {
      return super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why");
  }

  public DonPrtCIni deepClone() {
    DonPrtCIni clone = (DonPrtCIni) clone();
    clone.emh = null;
    return clone;
  }
}
