package org.fudaa.dodico.crue.metier.emh;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByIdComparator;

/**
 * Genere par OPTR.
 *
 * @author deniger
 */
public class OrdPrtReseau extends AbstractInfosEMH implements Sortable {

  private final List<Regle> regles = new ArrayList<>();
  private final List<Regle> immuableRegles = Collections.unmodifiableList(regles);
  private EnumMethodeOrdonnancement methodeOrdonnancement;
  private Sorties sorties = new Sorties();
  private final Map<EnumRegle, Regle> typeRegle = new EnumMap<>(EnumRegle.class);

  public OrdPrtReseau(CrueConfigMetier defaults) {
    List<Regle> defaultValues = DefaultValues.initDefaultValuesOPTR(defaults);
    for (Regle regle : defaultValues) {
      addRegle(regle);
    }
  }

  public OrdPrtReseau(OrdPrtReseau toCopy) {
    for (Regle regle : toCopy.regles) {
      addRegle(regle.deepClone());
    }
    methodeOrdonnancement = toCopy.methodeOrdonnancement;
    sorties = new Sorties(toCopy.sorties);
    sort();
  }

  public OrdPrtReseau deepClone() {
    return new OrdPrtReseau(this);
  }

  /**
   * @pdGenerated default add
   * @param newRegle
   */
  private void addRegle(Regle newRegle) {

    if (newRegle == null) {
      return;
    }
    assert newRegle.getType().isOptr();

    if (!this.regles.contains(newRegle)) {
      // on enleve la regle de meme type si presente
      Regle sameType = typeRegle.get(newRegle.getType());
      if (sameType != null) {
        regles.remove(sameType);
      }
      this.regles.add(newRegle);
      typeRegle.put(newRegle.getType(), newRegle);
    }
  }

  @Override
  public EnumInfosEMH getCatType() {
    return EnumInfosEMH.ORD_PRT_RESEAU;

  }

  /**
   * @return the methodeOrdonnancement
   */
  @UsedByComparison
  public EnumMethodeOrdonnancement getMethodeOrdonnancement() {
    return methodeOrdonnancement;
  }

  public Regle getRegle(EnumRegle regle) {
    return typeRegle.get(regle);
  }

  @UsedByComparison
  public List<Regle> getRegles() {
    return immuableRegles;
  }

  /**
   * @return the sorties
   */
  public Sorties getSorties() {
    return sorties;
  }

  /**
   * @pdGenerated default remove
   * @param oldRegle
   */
  public void removeRegle(Regle oldRegle) {
    if (oldRegle == null) {
      return;
    }
    if (this.regles != null) {
      if (this.regles.contains(oldRegle)) {
        typeRegle.remove(oldRegle.getType());
        this.regles.remove(oldRegle);
      }
    }
  }

  /**
   * @param methodeOrdonnancement the methodeOrdonnancement to set
   */
  public void setMethodeOrdonnancement(EnumMethodeOrdonnancement methodeOrdonnancement) {
    this.methodeOrdonnancement = methodeOrdonnancement;
  }

  /**
   * @param sorties the sorties to set
   */
  public void setSorties(Sorties sorties) {
    this.sorties = sorties;
  }

  @Override
  public final boolean sort() {
    if (regles != null) {
      List old = new ArrayList(regles);
      Collections.sort(regles, ObjetNommeByIdComparator.INSTANCE);
      return !old.equals(regles);
    }
    return false;
  }
}
