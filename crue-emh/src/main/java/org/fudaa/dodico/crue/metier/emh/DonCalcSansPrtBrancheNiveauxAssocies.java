package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;

import java.util.Collection;

/**
 * type 12 Crue9
 */
public class DonCalcSansPrtBrancheNiveauxAssocies extends DonCalcSansPrtBrancheQ {
  private LoiFF zasso;

  public DonCalcSansPrtBrancheNiveauxAssocies(final CrueConfigMetier defaults) {
    super(defaults);
  }

  @Override
  public void fillWithLoi(final Collection<Loi> target) {
    target.add(zasso);
  }

  @Override
  public DonCalcSansPrt cloneDCSP() {
    try {
      DonCalcSansPrtBrancheNiveauxAssocies res = (DonCalcSansPrtBrancheNiveauxAssocies) super.clone();
      if (zasso != null) {
        res.setZasso(zasso.clonedWithoutUser());
      }
      return res;
    } catch (CloneNotSupportedException e) {
    }
    return null;
  }

  public LoiFF getZasso() {
    return zasso;
  }

  public void setZasso(final LoiFF newZasso) {
    if (this.zasso != null) {
      this.zasso.unregister(this);
    }

    zasso = newZasso;
    this.zasso.register(this);
  }
}
