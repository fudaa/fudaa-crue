package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

/**
 * Branche 14: ouvrage suivant une consigne fonction d'un debit ailleurs, ou noye (loi pt par pt) La loi noyee est
 * donnee point par point
 * 
 * @author Adrien Hadoux
 */
public class EMHBrancheBarrageGenerique extends CatEMHBranche {

  public CatEMHSection getSectionPilote() {
    return EMHHelper.getSectionPilote(this);
  }
  
  @UsedByComparison
  @Visibility(ihm=false)
  public String getSectionPiloteId(){
    return getSectionPilote().getId();
  }
  
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public EnumBrancheType getBrancheType(){
    return EnumBrancheType.EMHBrancheBarrageGenerique;
  }

  public void setSectionPilote(final CatEMHSection sectionPilote) {
    EMHRelationFactory.addSectionPilote(this, sectionPilote);
  }

  public EMHBrancheBarrageGenerique(final String nom) {
    super(nom);
  }

  @Override
  @Visibility(ihm = false)
  public EMHBrancheBarrageGenerique copyShallowFirstLevel() {
    return  copyPrimitiveIn(new EMHBrancheBarrageGenerique(getNom()));
  }

}
