package org.fudaa.dodico.crue.metier.emh;

import java.util.Collection;

/**
 *
 * @author deniger
 */
public interface ResultatPasDeTempsDelegate {

  int getNbResultats();
  
  
  Collection<ResultatTimeKey> getResultatKeys();
  
  boolean containsResultsFor(ResultatTimeKey key);
  
  

}
