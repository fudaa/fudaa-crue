/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.helper;

import org.fudaa.dodico.crue.metier.emh.EMH;

import java.util.Set;

/**
 * Une interface pour les filter dynamiques sur les proprietes a utilise pour une comparaison.
 *
 * @author deniger
 */
public interface DynamicPropertyFilter {
    /**
     * @return les proprietes a comparer pour l'objet donne.
     */
    Set<String> getPropertyToUse(EMH emhA, EMH emhB);
}
