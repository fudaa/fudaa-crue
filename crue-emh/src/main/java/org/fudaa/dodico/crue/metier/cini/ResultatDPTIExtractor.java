package org.fudaa.dodico.crue.metier.cini;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.metier.emh.*;

import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class ResultatDPTIExtractor {
  private final EMHScenario scenario;
  private final CrueConfigMetier ccm;
  private static final Logger LOGGER = Logger.getLogger(ResultatDPTIExtractor.class.getName());

  public ResultatDPTIExtractor(EMHScenario scenario, CrueConfigMetier ccm
  ) {
    this.scenario = scenario;
    this.ccm = ccm;
  }

  public CtuluIOResult<Map<CiniImportKey, Object>> extract(ResultatTimeKey timeKey) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    CtuluIOResult<Map<CiniImportKey, Object>> result = new CtuluIOResult<>();
    Map<CiniImportKey, Object> resValues = new TreeMap<>();
    result.setAnalyze(log);
    result.setSource(resValues);
    if (!timeKey.isPermanent()) {
      return result;
    }
    //noeuds: utilise sections associées au noeud pour zini. Verifier si plusieurs sections associées au noeud
    scenario.getNoeuds().forEach(noeud -> extractZAssociatedSection(noeud, timeKey, log, resValues));
    //noeuds: si zini sur le noeuds il sera utilisé et écrase les valeurs précédentes éventuelles
    scenario.getNoeuds().forEach(noeud -> extractValues(noeud, timeKey, createMapping(CrueConfigMetierConstants.PROP_Z, CrueConfigMetierConstants.PROP_ZINI), resValues));
    //sections
    scenario.getSections().forEach(section -> extractValues(section, timeKey, createMapping(CrueConfigMetierConstants.PROP_Z, CrueConfigMetierConstants.PROP_ZINI), resValues));
    //casiers
    scenario.getCasiers().stream().filter(catEMHCasier -> EnumCasierType.EMHCasierProfil.equals(catEMHCasier.getCasierType()))
        .forEach(casier -> extractValues(casier, timeKey, createMapping(CrueConfigMetierConstants.PROP_QECH, CrueConfigMetierConstants.PROP_QRUIS), resValues));

    //branches: utiliser la section amont: le Q sur la section devient le Qini de la branche
    scenario.getBranches().forEach(branche -> extractQSectionAmont(branche, timeKey, resValues));
    //branche saint-venant
    scenario.getBranchesSaintVenant().stream()
        .forEach(brancheSaintVenant -> extractValues(brancheSaintVenant, timeKey, createMapping(CrueConfigMetierConstants.PROP_QLAT, CrueConfigMetierConstants.PROP_QRUIS), resValues));

    return result;
  }

  /**
   * @param noeud le noeud
   * @param timeKey le pas de temps
   * @param log log utilisé si les valeurs z ne sont pas constantes sur les sections associées au noeud
   * @param res le conteneur de résultat.
   */
  private void extractZAssociatedSection(CatEMHNoeud noeud, ResultatTimeKey timeKey, CtuluLog log, Map<CiniImportKey, Object> res) {
    final List<CatEMHSection> sectionsAttachedToNode = new ArrayList<>();
    final List<CatEMHBranche> branchesAmont = noeud.getBranchesAmont();
    if (branchesAmont != null) {
      branchesAmont.stream().filter(brancheAmont -> brancheAmont.getSectionAval() != null && brancheAmont.getSectionAval().getEmh() != null)
          .forEach(branche -> sectionsAttachedToNode.add(branche.getSectionAval().getEmh()));
    }
    final List<CatEMHBranche> branchesAval = noeud.getBranchesAval();
    if (branchesAval != null) {
      branchesAval.stream().filter(brancheAval -> brancheAval.getSectionAmont() != null && brancheAval.getSectionAmont().getEmh() != null)
          .forEach(branche -> sectionsAttachedToNode.add(branche.getSectionAmont().getEmh()));
    }
    Double z = extractCommonZ(noeud.getNom(), sectionsAttachedToNode, timeKey, log);
    if (z != null) {
      res.put(new CiniImportKey(noeud.getId(), CrueConfigMetierConstants.PROP_ZINI), z);
    }
  }

  /**
   * Extrait le resultat z lus sur les sections associées au noeud. Ajoute des messages si cette valeur n'est pas constante
   * @param noeudName le nom du noeud
   * @param sections les sections associées
   * @param timeKey le pas de temps
   * @param log le log a utiliser si les valeurs de z ne sont pas constantes
   * @return la valeur z ( le première trouvée).
   */
  private Double extractCommonZ(String noeudName, List<CatEMHSection> sections, ResultatTimeKey timeKey, CtuluLog log) {
    Double res = null;
    boolean warnAdded = false;
    for (CatEMHSection section : sections) {
      try {
        if (section.getResultatCalcul() != null) {
          final Map<String, Object> read = section.getResultatCalcul().read(timeKey);
          if (read.containsKey(CrueConfigMetierConstants.PROP_Z)) {
            Double newValue = (Double) read.get(CrueConfigMetierConstants.PROP_Z);
            if (res == null) {
              res = newValue;
            } else if (!warnAdded && !ccm.getEpsilon(CrueConfigMetierConstants.PROP_Z).isSame(res, newValue)) {
              warnAdded = true;
              log.addSevereError(BusinessMessages.getString("dptiExtractor.zNotConstantSections", noeudName));
            }
          }
        }
      } catch (IOException e) {
        LOGGER.log(Level.WARNING, e.getMessage(), e);
      }
    }

    return res;
  }

  /**
   * @param branche la branche a traiter. Si la section amont a un résultat Q, utiliser pour Qini
   * @param timeKey le pas de temps demandé
   * @param res contiendra les résultats.
   */
  private void extractQSectionAmont(CatEMHBranche branche, ResultatTimeKey timeKey, Map<CiniImportKey, Object> res) {
    final RelationEMHSectionDansBranche relationSectionAmont = branche.getSectionAmont();
    if (relationSectionAmont != null && relationSectionAmont.getEmh() != null && relationSectionAmont.getEmh().getResultatCalcul() != null) {
      try {
        final Map<String, Object> read = relationSectionAmont.getEmh().getResultatCalcul().read(timeKey);
        if (read.containsKey(CrueConfigMetierConstants.PROP_Q)) {
          res.put(new CiniImportKey(branche.getId(), CrueConfigMetierConstants.PROP_QINI), read.get(CrueConfigMetierConstants.PROP_Q));
        }
      } catch (IOException e) {
        LOGGER.log(Level.WARNING, e.getMessage(), e);
      }
    }
  }

  private Map<String, String> createMapping(String resName, String ciniName) {
    Map<String, String> mapping = new HashMap<>();
    mapping.put(resName, ciniName);
    return mapping;
  }

  private void extractValues(EMH emh, ResultatTimeKey timeKey, Map<String, String> ciniNameByResMap, Map<CiniImportKey, Object> res) {
    if (emh.getResultatCalcul() != null) {
      try {
        final Map<String, Object> read = emh.getResultatCalcul().read(timeKey);
        ciniNameByResMap.entrySet().forEach(cIniByRes -> {
          if (read.containsKey(cIniByRes.getKey())) {
            res.put(new CiniImportKey(emh.getId(), cIniByRes.getValue()), read.get(cIniByRes.getKey()));
          }
        });
      } catch (IOException e) {
        LOGGER.log(Level.WARNING, e.getMessage(), e);
      }
    }
  }
}


