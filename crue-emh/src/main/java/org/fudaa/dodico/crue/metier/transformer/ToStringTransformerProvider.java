/**
 * 
 */
package org.fudaa.dodico.crue.metier.transformer;

import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;

/**
 * @author deniger
 */
public interface ToStringTransformerProvider {

  ToStringTransformer create(CrueConfigMetier props);

}
