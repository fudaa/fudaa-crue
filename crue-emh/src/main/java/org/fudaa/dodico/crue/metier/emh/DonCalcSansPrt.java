package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;

import java.util.Collection;

/**
 */
public abstract class DonCalcSansPrt extends AbstractInfosEMH implements Cloneable  {

  /**
   */
  @Override
  public final EnumInfosEMH getCatType() {
    return EnumInfosEMH.DON_CALC_SANS_PRT;
  }

  public abstract void fillWithLoi(Collection<Loi> target);

  @Visibility(ihm = false)
  public abstract DonCalcSansPrt cloneDCSP();
}
