/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.result;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.time.TimeFormatOption;
import org.fudaa.dodico.crue.common.time.TimeFormatOption.DureeFormatType;
import org.fudaa.dodico.crue.common.time.ToStringTransformerDate;
import org.fudaa.dodico.crue.common.time.ToStringTransformerMillis;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;
import org.joda.time.LocalDateTime;

/**
 *
 * @author Frederic Deniger
 */
public class ResultKeyFormatter {

  private TimeFormatOption option = new TimeFormatOption();
  private CrueConfigMetier ccm;
  private LocalDateTime dateDebSce;

  public void setCcmAndScenario(CrueConfigMetier ccm, EMHScenario scenario) {
    this.ccm = ccm;
    dateDebSce = scenario.getParamCalcScenario().getDateDebSce();
  }

  public CrueConfigMetier getCcm() {
    return ccm;
  }

  public void setOption(TimeFormatOption option) {
    this.option = option;
  }

  public TimeFormatOption getOption() {
    return option;
  }
  final MillisTempsSimuToStringTransformer millisTempsSimuToStringTransformer = new MillisTempsSimuToStringTransformer();
  final ResulatKeyTempsSimuToStringTransformer resulatKeyTempsSimuToStringTransformer = new ResulatKeyTempsSimuToStringTransformer();
  final ResulatKeyTempsSceToStringTransformer resulatKeyTempsSceToStringTransformer = new ResulatKeyTempsSceToStringTransformer();
  final ToStringTransformerMillis millisToStringTransformer = new ToStringTransformerMillis();

  public ToStringTransformer getResulatKeyTempsSimuToStringTransformer() {
    return resulatKeyTempsSimuToStringTransformer;
  }

  public ToStringTransformer getResulatKeyTempsSceToStringTransformer() {
    return resulatKeyTempsSceToStringTransformer;
  }

  public ToStringTransformer getMillisTempsSceToStringTransformer() {
    return millisTempsSimuToStringTransformer;
  }

  private class ResulatKeyTempsSimuToStringTransformer implements ToStringTransformer {

    @Override
    public String transform(Object in) {
      if (in == null) {
        return StringUtils.EMPTY;
      }
      ResultatTimeKey key = (ResultatTimeKey) in;
      if (key.isPermanent()) {
        return key.getNomCalcul();
      }
      return millisTempsSimuToStringTransformer.transform(key.getDuree());
    }
  }

  private class ResulatKeyTempsSceToStringTransformer implements ToStringTransformer {

    @Override
    public String transform(Object in) {
      if (in == null) {
        return StringUtils.EMPTY;
      }
      ResultatTimeKey key = (ResultatTimeKey) in;
      if (key.isPermanent()) {
        return key.getNomCalcul();
      }
      long tempSce = key.getDuree();
      return key.getNomCalcul() + BusinessMessages.ENTITY_SEPARATOR + millisTempsSimuToStringTransformer.transform(tempSce);
    }
  }

  private class MillisTempsSimuToStringTransformer implements ToStringTransformer {

    @Override
    public String transform(Object in) {
      if (in == null) {
        return StringUtils.EMPTY;
      }
      Long millis = (Long) in;
      if (dateDebSce == null) {
        return formatDuree(millis);
      } else {
        LocalDateTime plusMillis = dateDebSce.plusMillis(millis.intValue());
        return ToStringTransformerDate.format(plusMillis);
      }
    }

    private String formatDuree(Long millis) {
      if (option.getDureeFormatType().equals(DureeFormatType.AS_SECOND)) {
        return TransformerEMHHelper.formatFromPropertyName(CrueConfigMetierConstants.PROP_TEMPSSCE, millis, ccm, DecimalFormatEpsilonEnum.PRESENTATION) + " s";
      }
      return millisToStringTransformer.format(millis.longValue());
    }
  }
}
