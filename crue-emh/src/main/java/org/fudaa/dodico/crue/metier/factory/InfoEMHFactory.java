package org.fudaa.dodico.crue.metier.factory;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtCasierProfil;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

/**
 * @author deniger
 */
public class InfoEMHFactory {

  public static DonCalcSansPrtCasierProfil getDCSPCasierProfil(final CatEMHCasier casier,
      final CrueConfigMetier cruePropertyDefinitionContainer) {
    if(casier==null){
      return null;
    }
    DonCalcSansPrtCasierProfil dcsp = EMHHelper.selectFirstOfClass(casier.getInfosEMH(),
        DonCalcSansPrtCasierProfil.class);
    if (dcsp == null) {

      dcsp = new DonCalcSansPrtCasierProfil(cruePropertyDefinitionContainer);
      casier.addInfosEMH(dcsp);
    }
    return dcsp;
  }

}
