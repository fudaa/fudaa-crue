package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * CL de type 41 ou 42 en Crue9
 *
 */
public class CalcPseudoPermBrancheOrificeManoeuvreRegul extends DonCLimMCommonItem implements CalcPseudoPermItem, DonCLimMWithManoeuvreRegulData {


  private EnumSensOuv sensOuv;

  private LoiFF manoeuvreRegul;

  private String param;

  public CalcPseudoPermBrancheOrificeManoeuvreRegul(final CrueConfigMetier defaults) {
    final String enumValue = defaults.getDefaultStringEnumValue("sensOuv");
    try {
      sensOuv = enumValue == null ? null : EnumSensOuv.valueOf(enumValue);
    } catch (final Exception e) {
    }
  }

  @Visibility(ihm = false)
  public ManoeuvreRegulData getData() {
    return new ManoeuvreRegulData(manoeuvreRegul, param);
  }

  public void setData(ManoeuvreRegulData data){
    if(data!=null){
      setManoeuvreRegul(data.manoeuvreRegul);
      setParam(data.param);
    }
  }

  @Visibility(ihm = false)
  public String getDesc() {
    if (getManoeuvreRegul() == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    return getManoeuvreRegul().getNom() + " / " + getParam();
  }

  public void setParam(final String param) {
    this.param = param;
  }

  @PropertyDesc(i18n = "param.property")
  public String getParam() {
    return param;
  }

  public EnumTypeLoi getTypeLoi() {
    return EnumTypeLoi.LoiQOuv;
  }

  @PropertyDesc(i18n = "manoeuvreRegul.property")
  public LoiFF getManoeuvreRegul() {
    return manoeuvreRegul;
  }



  @Override
  public String getCcmVariableName() {
    return CrueConfigMetierConstants.PROP_OUV;
  }


  @Override
  public boolean containValue() {
    return false;
  }

  @Override
  public String getShortName() {
    return BusinessMessages.getString("manoeuvreRegul.property");
  }

  public void setManoeuvreRegul(final LoiFF newManoeuvre) {
    if (this.manoeuvreRegul != null) {
      this.manoeuvreRegul.unregister(this);
    }

    manoeuvreRegul = newManoeuvre;
    if (this.manoeuvreRegul != null) {
      this.manoeuvreRegul.register(this);
    }
  }

  @Override
  public Loi getLoi() {
    return getManoeuvreRegul();
  }

  @Override
  public void setLoi(final Loi newLoi) {
    setManoeuvreRegul((LoiFF) newLoi);
  }



  @PropertyDesc(i18n = "sensOuv.property")
  public EnumSensOuv getSensOuv() {
    return sensOuv;
  }

  /**
   * @param newSensOuv nouveau sens
   */
  public void setSensOuv(final EnumSensOuv newSensOuv) {
    sensOuv = newSensOuv;
  }

  /**
   * Fonction générique permettant de récupérer les données d'un CalPseudoPerm la valeur
   *
   */
  @Visibility(ihm = false)
  @Override
  public double getValue() {
    throw new IllegalAccessError("non utilisable");
  }

  /**
   * @param newVal nouvelle valeur
   */
  @Override
  public void setValue(final double newVal) {
    throw new IllegalAccessError("non utilisable");
  }

  CalcPseudoPermBrancheOrificeManoeuvreRegul deepClone() {
    try {
      return (CalcPseudoPermBrancheOrificeManoeuvreRegul) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransNoeudNiveauContinuTarage.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }
}
