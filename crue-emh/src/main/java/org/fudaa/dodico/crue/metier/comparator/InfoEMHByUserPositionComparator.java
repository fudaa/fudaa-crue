package org.fudaa.dodico.crue.metier.comparator;

import gnu.trove.TObjectIntHashMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.InfosEMH;

/**
 * @author deniger
 */
public class InfoEMHByUserPositionComparator extends SafeComparator<InfosEMH> {

  final TObjectIntHashMap<String> emhPosition = new TObjectIntHashMap<>();

  /**
   *
   * @param list
   * @return true si modification effectuée dans la liste.
   */
  public static boolean sortList(final List<? extends InfosEMH> list, InfoEMHByUserPositionComparator comparator) {
    if (list != null) {
      List before = new ArrayList(list);
      Collections.sort(list, comparator);
      return !before.equals(list);
    }
    return false;
  }

  public InfoEMHByUserPositionComparator(EMHScenario scenario) {
    List<EMH> allSimpleEMHinUserOrder = scenario.getAllSimpleEMHinUserOrder();
    for (int i = 0; i < allSimpleEMHinUserOrder.size(); i++) {
      EMH emh = allSimpleEMHinUserOrder.get(i);
      emhPosition.put(emh.getNom(), i);
    }

  }

  private InfoEMHByUserPositionComparator() {
  }

  @Override
  protected int compareSafe(InfosEMH o1, InfosEMH o2) {
    int res = RegleTypeComparator.compareString(o1.getType(), o2.getType());
    if (res == 0) {
      int pos1 = emhPosition.get(o1.getEmh().getNom());
      int pos2 = emhPosition.get(o2.getEmh().getNom());
      res = pos1 - pos2;
    }
    return res;
  }
}
