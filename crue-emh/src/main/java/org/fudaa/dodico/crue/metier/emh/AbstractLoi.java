package org.fudaa.dodico.crue.metier.emh;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.config.loi.LoiContrat;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 * Loi abstraite pour LoiDF et LoiFF
 */
public abstract class AbstractLoi implements ObjetNomme, Cloneable, LoiContrat {

    /**
     */
    private String nom;
    /**
     */
    private String commentaire = StringUtils.EMPTY;
    /**
     */
    private EnumTypeLoi type;
    protected HashSet<Object> users;

    public AbstractLoi() {
        this.users = new HashSet<>();
    }

    /**
     * Get if the rule is used.
     *
     * @return if the rule is used.
     */
    @UsedByComparison //attention c'est utilisé.
    public boolean getUsed() {
        return (!this.users.isEmpty());
    }

    /**
     * ne pas utiliser directement.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public abstract AbstractLoi clonedWithoutUser();

    /**
     * Clone uniquement le commentaire, nom. Le reste est initialisé à vide.
     *
     * @return loi sans les points
     */
    public abstract AbstractLoi cloneWithoutPoints();

    /**
     * Get if the rule is used by a specific object.
     *
     * @param object the specific object.
     * @return if the rule is used by a specific object.
     */
    public boolean getUsedBy(final Object object) {
        return this.users.contains(object);
    }

    @UsedByComparison(ignoreInComparison = true)
    @Visibility(ihm = false)
    public Collection<Object> getUsedByCollection() {
        return users == null ? Collections.emptySet() : Collections.unmodifiableSet(users);
    }

    /**
     * Register a user.
     *
     * @param user the user to register
     */
    public void register(final Object user) {
        this.users.add(user);
    }

    /**
     * Unregister a user.
     *
     * @param user the user to unregister
     */
    public void unregister(final Object user) {
        this.users.remove(user);
    }

    public abstract double getOrdonnee(int idx);

    @UsedByComparison(ignoreInComparison = true)
    @Visibility(ihm = false)
    public abstract int getSize();

    /**
     * @return the type
     */
    @Override
    public EnumTypeLoi getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(final EnumTypeLoi type) {
        this.type = type;
    }

    @Override
    public String getNom() {
        return nom;

    }

    @Override
    public void setNom(final String newNom) {
        nom = newNom;
        if (nom != null) {
            id = nom.toUpperCase();
        }
    }

    private String id;

    @Override
    public String getId() {
        return id;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(final String newCommentaire) {
        this.commentaire = StringUtils.defaultString(newCommentaire);
    }
}
