package org.fudaa.dodico.crue.metier.etude;

import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.joda.time.LocalDateTime;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Informations sur la version du modele, fichier,scenario, sous modele, run,.... Crue
 *
 * @author Adrien Hadoux
 */
public class EMHInfosVersion implements Cloneable {
  protected String auteurCreation;
  protected String commentaire;
  protected LocalDateTime dateCreation;
  protected String auteurDerniereModif;
  private LocalDateTime dateDerniereModif;
  private CrueVersionType crueVersion;

  public String getType() {
    return crueVersion.getTypeId();
  }

  public void updateForm(ConnexionInformation information) {
    if (information != null) {
      if (dateCreation == null) {
        dateCreation = information.getCurrentDate();
      }
      if (auteurCreation == null) {
        auteurCreation = information.getCurrentUser();
      }
      auteurDerniereModif = information.getCurrentUser();
      dateDerniereModif = information.getCurrentDate();
    }
  }


  public CrueVersionType getCrueVersion() {
    return crueVersion;
  }

  public boolean isCrue9() {
    return CrueVersionType.CRUE9.equals(crueVersion);
  }

  @Override
  public Object clone() {
    try {
      EMHInfosVersion info = (EMHInfosVersion) super.clone();
      info.dateCreation = new LocalDateTime(dateCreation);
      info.dateDerniereModif = new LocalDateTime(dateDerniereModif);
      return info;
    } catch (CloneNotSupportedException e) {
      Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
    }
    return null;
  }

  public String getAuteurCreation() {
    return auteurCreation;
  }

  public void setAuteurCreation(final String creation) {
    this.auteurCreation = creation;
  }

  public LocalDateTime getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(final LocalDateTime dateCreation) {
    this.dateCreation = dateCreation;
  }


  public String getAuteurDerniereModif() {
    return auteurDerniereModif;
  }

  public void setAuteurDerniereModif(final String auteurDerniereModif) {
    this.auteurDerniereModif = auteurDerniereModif;
  }

  /**
   * used by reflection by ui.
   */
  public LocalDateTime getDateDerniereModif() {
    return dateDerniereModif;
  }

  public void setDateDerniereModif(final LocalDateTime dateDerniereModif) {
    this.dateDerniereModif = dateDerniereModif;
  }

  public String getCommentaire() {
    return commentaire;
  }

  public void setCommentaire(final String commentaire) {
    this.commentaire = commentaire;
  }

  /**
   * @param version the version to set
   */
  public void setVersion(CrueVersionType version) {
    this.crueVersion = version;
  }
}
