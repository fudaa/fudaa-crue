package org.fudaa.dodico.crue.metier.emh;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;

/**
 * CL de type 3 en Crue9
 *
 */
public class CalcTransNoeudNiveauContinuTarage extends DonCLimMCommonItem implements CalcTransItem {

  /**
   * Loi Tarrage : VarAbscisse = Z VarOrdonnee = Qapp
   *
   */
  private LoiFF tarage;

  @PropertyDesc(i18n = "tarage.property")
  public LoiFF getTarage() {
    return tarage;
  }

  @Override
  public String getShortName() {
    return BusinessMessages.getString("tarage.property");
  }

  @Override
  public EnumTypeLoi getTypeLoi() {
    return EnumTypeLoi.LoiQZimp;
  }

  /**
   * Loi Tarrage : VarAbscisse = Z VarOrdonnee = Qapp
   *
   * @param newTarage
   */
  public void setTarage(final LoiFF newTarage) {
    if (this.tarage != null) {
      this.tarage.unregister(this);
    }

    tarage = newTarage;
    if (this.tarage != null) {
      this.tarage.register(this);
    }
  }

  /**
   */
  @Visibility(ihm = false)
  @Override
  public Loi getLoi() {
    return getTarage();
  }

  @Override
  public void setLoi(final Loi newLoi) {
    setTarage((LoiFF) newLoi);
  }

  CalcTransNoeudNiveauContinuTarage deepClone() {
    try {
      return (CalcTransNoeudNiveauContinuTarage) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransNoeudNiveauContinuTarage.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }
}
