/**
 * *********************************************************************
 * Module: Pdt.java Author: battista Purpose: Defines the Class Pdt
 **********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;

public abstract class Pdt implements ToStringTransformable {

  public abstract Pdt deepClone();
}
