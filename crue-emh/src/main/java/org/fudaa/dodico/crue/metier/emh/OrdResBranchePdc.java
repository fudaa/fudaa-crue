/**
 * *********************************************************************
 * Module: OrdResScenarioBranchePdc.java Author: BATTISTA Purpose: Defines the Class OrdResScenarioBranchePdc
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.OrdResKey;

public class OrdResBranchePdc extends OrdRes implements Cloneable {
  // WARN: l'ordre des champs est important car utilise par l'ecriture/lecture de ORES

  @Override
  public OrdResBranchePdc clone() {
    try {
      return (OrdResBranchePdc) super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why this ?");
  }
  private final static OrdResKey KEY = new OrdResKey(EnumBrancheType.EMHBranchePdc);

  @Override
  public OrdResKey getKey() {
    return KEY;
  }

  @Override
  public String toString() {
    return "OrdResBranchePdc [ddeDz=" + getDdeValue("dz") + "]";
  }
}