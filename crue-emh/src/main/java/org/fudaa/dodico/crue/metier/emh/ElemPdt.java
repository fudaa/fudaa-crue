package org.fudaa.dodico.crue.metier.emh;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;
import org.joda.time.Duration;

/**
 */
public class ElemPdt implements ToStringTransformable, Cloneable {

  private int nbrPdt;
  private Duration dureePdt;

  public ElemPdt(CrueConfigMetier def) {
    nbrPdt = def.getDefaultIntValue(CrueConfigMetierConstants.PROP_NBR_PDT);
  }

  @Override
  public ElemPdt clone() {
    try {
      return (ElemPdt) super.clone();
    } catch (CloneNotSupportedException ex) {
      Logger.getLogger(ElemPdt.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("why");
  }

  public ElemPdt deepClone() {
    return clone();
  }

  @PropertyDesc(i18n = "nbrPdt.property")
  public final int getNbrPdt() {
    return nbrPdt;
  }

  @Override
  public String toString() {
    return " ElemPdt: " + nbrPdt + " * " + dureePdt.getStandardSeconds() + " s";
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    return " ElemPdt: " + nbrPdt + " * " + TransformerEMHHelper.formatFromPropertyName("pdt", dureePdt.getStandardSeconds(), props, DecimalFormatEpsilonEnum.COMPARISON) + " s";
  }

  /**
   * @param newNbrPdt
   */
  public final void setNbrPdt(int newNbrPdt) {
    nbrPdt = newNbrPdt;
  }

  @PropertyDesc(i18n = "dureePdt.property")
  public final Duration getDureePdt() {
    return dureePdt;
  }

  /**
   * @param newDureePdt
   */
  public final void setDureePdt(Duration newDureePdt) {
    dureePdt = newDureePdt;

  }
}
