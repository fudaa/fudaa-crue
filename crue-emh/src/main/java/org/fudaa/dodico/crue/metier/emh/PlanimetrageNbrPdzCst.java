/**
 * *********************************************************************
 * Module: PlanimetrageNbrPdzCst.java Author: deniger Purpose: Defines the Class PlanimetrageNbrPdzCst
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Planimétrage défini par un nombre fixe de pas de hauteur
 */
public class PlanimetrageNbrPdzCst extends Planimetrage implements Cloneable {
  private int nbrPdz;

  public PlanimetrageNbrPdzCst(CrueConfigMetier def) {
    nbrPdz = def.getDefaultIntValue("nbrPdz");
  }

  @Override
  protected Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  @Override
  public PlanimetrageNbrPdzCst deepClone() {
    try {
      return (PlanimetrageNbrPdzCst) clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
      Logger.getLogger(PlanimetrageNbrPdzCst.class.getName()).log(Level.SEVERE, "cloneNotSupportedException");
    }
    return null;
  }

  @Override
  public String toString() {
    return "PlanimetrageNbrPdzCst [nbrPdz=" + nbrPdz + "]";
  }

  @PropertyDesc(i18n = "nbrPdz.property")
  public final int getNbrPdz() {
    return nbrPdz;
  }

  public final void setNbrPdz(int newNbrPdz) {
    nbrPdz = newNbrPdz;
  }
}
