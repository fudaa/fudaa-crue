/**
 * *********************************************************************
 * Module: ValParamBooleen.java Author: battista Purpose: Defines the Class ValParamBooleen
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

public class ValParamBooleen extends ValParam {
  private boolean valeur;

  @PropertyDesc(i18n = "valParamValeur.property")
  public final boolean getValeur() {
    return valeur;
  }

  public final void setValeur(boolean newValeur) {
    valeur = newValeur;
  }

  /**
   * @return le type de donnée portée par ce ValParam
   */
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public Class getTypeData() {
    return Boolean.TYPE;
  }

  @Override
  public Object getValeurObjet() {
    return valeur;
  }

  public ValParamBooleen(final String nom, final boolean valeur) {
    super(nom);
    this.valeur = valeur;
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    return "ValParamBooleen[ " + valeur + " ]";
  }

  @Override
  public String getValueObjetInString(CrueConfigMetier prop) {
    return Boolean.toString(valeur);
  }
}
