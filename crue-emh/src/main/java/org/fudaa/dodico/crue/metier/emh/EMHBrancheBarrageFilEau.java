package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

/**
 * Branche 15: barrage suivant une consigne fonction d'un debit ailleurs, ou noye (De Marchi) La loi noyee est calculee
 * par une formule de seuil epais
 * 
 * @author Adrien Hadoux
 */
public class EMHBrancheBarrageFilEau extends CatEMHBranche {

  public CatEMHSection getSectionPilote() {
    return EMHHelper.getSectionPilote(this);
  }
  
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public EnumBrancheType getBrancheType(){
    return EnumBrancheType.EMHBrancheBarrageFilEau;
  }
  
  @UsedByComparison
  @Visibility(ihm=false)
  public String getSectionPiloteId(){
    return getSectionPilote().getId();
  }

  public void setSectionPilote(final CatEMHSection sectionPilote) {
    EMHRelationFactory.addSectionPilote(this, sectionPilote);
  }

  public EMHBrancheBarrageFilEau(final String nom) {
    super(nom);
  }

  @Override
  @Visibility(ihm = false)
  public EMHBrancheBarrageFilEau copyShallowFirstLevel() {
    return  copyPrimitiveIn(new EMHBrancheBarrageFilEau(getNom()));
  }

}
