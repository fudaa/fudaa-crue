package org.fudaa.dodico.crue.metier.helper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.PredicateUtils;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.collections4.functors.AnyPredicate;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.metier.comparator.ComparatorRelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.PredicateFactory;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe utilitaire permettant de naviguer facilement dans les EMH, infosEMH et les RelationEMH
 *
 * @author deniger
 */
public final class EMHHelper {
  public static RelationEMHSectionDansSectionIdem getRelationSectionRef(final EMHSectionIdem emh) {
    final RelationEMHSectionDansSectionIdem relation = selectFirstOfClass(emh.getRelationEMH(),
        RelationEMHSectionDansSectionIdem.class);
    return relation;
  }

  public static List<EMHSectionProfil> getEMHSectionProfil(final CatEMHBranche branche, final CrueConfigMetier ccm) {
    final List<EMHSectionProfil> res = new ArrayList<>();
    fillEMHSectionProfil(branche, res, ccm);
    return res;
  }

  public static void fillEMHSectionProfil(final CatEMHBranche branche, final List<EMHSectionProfil> toFill, final CrueConfigMetier ccm) {
    if (branche == null) {
      return;
    }
    final List<RelationEMHSectionDansBranche> sections = branche.getListeSectionsSortedXP(ccm);
    for (final RelationEMHSectionDansBranche relation : sections) {
      final CatEMHSection emh = relation.getEmh();
      if (emh.getClass().equals(EMHSectionProfil.class)) {
        toFill.add((EMHSectionProfil) emh);
      }
    }
  }

  public static boolean containEMHSectionProfil(final CatEMHBranche branche) {
    if (branche == null) {
      return false;
    }
    final List<RelationEMHSectionDansBranche> sections = branche.getListeSections();
    for (final RelationEMHSectionDansBranche relation : sections) {
      final CatEMHSection emh = relation.getEmh();
      if (emh.getClass().equals(EMHSectionProfil.class)) {
        return true;
      }
    }
    return false;
  }

  public static List<EMHSectionProfil> getEMHSectionProfilWithoutBranches(final EMHSousModele sousModele) {
    final List<EMHSectionProfil> res = new ArrayList<>();
    fillWithEMHSectionProfilWithoutBranches(sousModele, res);
    return res;
  }

  public static List<EMHSectionProfil> getAllEMHSectionProfil(final EMHSousModele sousModele) {
    final List<EMHSectionProfil> res = new ArrayList<>();
    final List<CatEMHSection> sections = sousModele.getSections();
    for (final CatEMHSection catEMHSection : sections) {
      if (catEMHSection.getClass().equals(EMHSectionProfil.class)) {
        res.add((EMHSectionProfil) catEMHSection);
      }
    }
    return res;
  }

  public static void fillWithEMHSectionProfilWithoutBranches(final EMHSousModele sousModele, final List<EMHSectionProfil> toFill) {
    if (sousModele == null) {
      return;
    }
    final List<CatEMHSection> sections = sousModele.getSections();
    for (final CatEMHSection catEMHSection : sections) {
      if (catEMHSection.getBranche() == null && catEMHSection.getClass().equals(EMHSectionProfil.class)) {
        toFill.add((EMHSectionProfil) catEMHSection);
      }
    }
  }

  public static boolean containEMHSectionProfilWithoutBranches(final EMHSousModele sousModele) {
    if (sousModele == null) {
      return false;
    }
    final List<CatEMHSection> sections = sousModele.getSections();
    for (final CatEMHSection catEMHSection : sections) {
      if (catEMHSection.getBranche() == null && catEMHSection.getClass().equals(EMHSectionProfil.class)) {
        return true;
      }
    }
    return false;
  }

  public static List<RelationEMHSectionIdemContientSection> getRelationSectionIdemUsing(final CatEMHSection emh) {
    return selectClass(emh.getRelationEMH(),
        RelationEMHSectionIdemContientSection.class);
  }

  /**
   * @param in
   * @return true si la collection contient au moins un ordre de calcul transitoire
   */
  public static boolean containsTransitoire(final Collection<OrdCalc> in) {
    return CollectionUtils.exists(in, new OrdCalcIsTransitoire());
  }

  /**
   * @param in
   * @return true si la collection contient au moins un ordre de calcul transitoire
   */
  public static List<CalcPseudoPerm> collectCalcPseudoPerm(final Collection<OrdCalc> in) {
    if (in == null) {
      return null;
    }
    final Collection select = CollectionUtils.select(in, new OrdCalcIsPseudoPermanent());
    final List<CalcPseudoPerm> res = new ArrayList<>(select.size());
    CollectionUtils.collect(select, new OrdCalToCalc(), res);
    return res;
  }

  /**
   * @param in
   * @return true si la collection contient au moins un ordre de calcul transitoire
   */
  public static List<CalcTrans> collectCalcTrans(final Collection<OrdCalc> in) {
    if (in == null) {
      return null;
    }
    final Collection select = CollectionUtils.select(in, new OrdCalcIsCalcTrans());
    final List<CalcTrans> res = new ArrayList<>(select.size());
    CollectionUtils.collect(select, new OrdCalToCalc(), res);
    return res;
  }

  /**
   * @param in
   * @return true si la collection contient au moins un ordre de calcul permanent
   */
  public static boolean containsPermanent(final Collection<OrdCalc> in) {
    return CollectionUtils.exists(in, new OrdCalcIsPermanent());
  }

  /**
   * Cree les relations EMH correspondantes aux sections
   *
   * @param emh
   * @param liste
   */
  public static void addListeSectionsToRelations(final EMH emh, final List<? extends RelationEMH> liste) {
    emh.addAllRelations(liste);
  }

  /**
   * @param emh  l'emh de base a parcourir entierement: on parcourt toutes les RelationEMH des EMH contenu
   * @param pred predicate qui doit porter sur les RelationEMH
   * @return la liste des RelationEMH accepte par le pred.
   */
  public static List<RelationEMH> collectDeepRelationEMH(final EMH emh, final Predicate pred) {
    final Set<EMH> done = new HashSet<>();
    final List<RelationEMH> res = new ArrayList<>();
    collectDeepRelationEMH(emh, pred, done, res);
    return res;
  }

  private static void collectDeepRelationEMH(final EMH emh, final Predicate pred, final Set<EMH> done,
                                             final List<RelationEMH> res) {
    if (!done.contains(emh)) {
      done.add(emh);
      final List<RelationEMH> relationEMH = emh.getRelationEMH();
      CollectionUtils.select(relationEMH, pred, res);
      for (final RelationEMH rel : relationEMH) {
        collectDeepRelationEMH(rel.getEmh(), pred, done, res);
      }
    }
  }

  /**
   * @param <T>   la type demande
   * @param emh   l'emh a parcourir non null
   * @param infos le type d'info voulu
   * @return la liste correspondante
   */
  public static <T extends InfosEMH> List<T> collectInfoEMH(final EMH emh, final EnumInfosEMH infos) {
    final List<T> infosEMH = (List<T>) emh.getInfosEMH();
    final List<T> res = new ArrayList<>(infosEMH.size());
    CollectionUtils.select(infosEMH, new PredicateFactory.PredicateEnumInfo(infos), res);
    return res;
  }

  /**
   * Recherche le frottements par rapport a la reference fournie.
   *
   * @param listeMetier
   */
  public static DonFrt selectDFRTByReference(final String reference, final DonFrtConteneur listeMetier) {
    return selectObjectNomme(listeMetier.getListFrt(), reference);
  }

  /**
   * @param <T>  le type a recherche
   * @param list la liste contenant les types
   * @param id   le nom recherche: sera mis en majuscule
   * @return le type trouve ou null si non trouve ou si un des parametres est null.
   */
  public static <T extends ObjetWithID> T selectObjectNomme(final Collection<T> list, final String id) {
    if (list == null || id == null) {
      return null;
    }
    final String nomToFind = id.toUpperCase();
    for (final T t : list) {
      if (nomToFind.equals(t.getId())) {
        return t;
      }
    }
    return null;
  }

  /**
   * Retourne l'ensemble des DonClimM à partir de la liste des infosEMH. Utilisé pour le fichier DCLM
   *
   * @param emh
   * @return la liste des objets DonClimM contenus dans la liste des infos EMH
   */
  public static List<DonCLimM> collectDCLM(final EMH emh) {
    return collectInfoEMH(emh, EnumInfosEMH.DON_CLIM);
  }

  /**
   * Retourne l'info DonCalcSansPrt de la liste des infosEMH. Utilise pour le fichier (DSCP)
   */
  public static List<DonCalcSansPrt> collectDCSP(final EMH emh) {
    return collectInfoEMH(emh, EnumInfosEMH.DON_CALC_SANS_PRT);
  }

  /**
   * @param emh l'emh de base a parcourir entierement: on parcourt toutes les RelationEMH des EMH contenu
   * @param ref la ref de EMH a trouver dans les relations
   * @return la liste des relationEMH dont l'EMH contenu a pour nom ref.
   */
  public static <T extends RelationEMH> List<T> collectDeepRelationEMHsByRef(final EMH emh, final String ref,
                                                                             final Class classOfRelation) {
    final PredicateFactory.PredicateRelationEMHContientId pred = new PredicateFactory.PredicateRelationEMHContientId(
        ref, null);
    final Predicate and = PredicateUtils.andPredicate(pred, new PredicateFactory.PredicateInstanceOf(classOfRelation));
    return (List<T>) collectDeepRelationEMH(emh, and);
  }

  /**
   * @param emh l'emh de base a parcourir entierement: on parcourt toutes les RelationEMH des EMH contenu
   * @param ref la ref de EMH a trouver dans les relations
   * @return la liste des relationEMH dont l'EMH contenu a pour nom ref.
   */
  public static <T extends RelationEMH> List<T> collectDeepRelationEMHsByRef(final EMH emh, final String ref) {
    final PredicateFactory.PredicateRelationEMHContientId pred = new PredicateFactory.PredicateRelationEMHContientId(
        ref, null);
    return (List<T>) collectDeepRelationEMH(emh, pred);
  }

  /**
   * Retourne la DonPrtGeo de la liste des infosEMH. DPTG - Fichier des donnees de pretraitement geometrique (xml).
   */
  public static List<DonPrtGeo> collectDPTG(final EMH emh) {
    return collectInfoEMH(emh, EnumInfosEMH.DON_PRT_GEO);
  }

  public static List<DonPrtGeoNomme> collectDPTGNomme(final EMH emh) {
    final List<DonPrtGeo> dptg = collectDPTG(emh);
    if (CollectionUtils.isEmpty(dptg)) {
      return Collections.emptyList();
    }
    final List<DonPrtGeoNomme> dptgNomme = new ArrayList<>(dptg.size());
    dptg.stream().filter(object -> object instanceof DonPrtGeoNomme).forEach(o -> dptgNomme.add((DonPrtGeoNomme) o));
    return dptgNomme;
  }

  /**
   * Retourne la condition initiale DonPrtClInit de la liste des infosEMH. Conditions initiales et manoeuvres aux branches (DPTI)
   */
  public static List<DonPrtCIni> collectDPTI(final EMH emh) {
    return collectInfoEMH(emh, EnumInfosEMH.DON_PRT_CINI);
  }

  public static <T extends EMH, K extends RelationEMH> List<T> collectEMHInRelations(final Collection<K> in) {
    if (CollectionUtils.isEmpty(in)) {
      return Collections.emptyList();
    }
    final List<T> res = new ArrayList<>(in.size());
    CollectionUtils.collect(in, new TransformerEMHHelper.TransformerRelationToEMH(), res);
    return res;
  }

  /**
   * @param <T> le type d'EMH voulu
   * @param emh l'EMH contenant
   * @param ref la reference
   * @return l'emh contenu de ref ref.
   */
  @SuppressWarnings("unchecked")
  public static <T extends EMH> T selectEMHInRelationByRef(final EMH emh, final String ref) {
    return (T) selectEMHInRelationByRef(emh, ref, null);
  }

  /**
   * @param <T>
   * @param emh    la liste d'EMH
   * @param catEMH la categorie a sélectionner
   * @return une liste non null
   */
  @SuppressWarnings("unchecked")
  public static <T extends EMH> List<T> selectEMHS(final Collection<? extends EMH> emh, final EnumCatEMH catEMH) {
    final PredicateFactory.PredicateTypeEnum predicate = new PredicateFactory.PredicateTypeEnum(catEMH);
    final List<T> result = new ArrayList<>();
    CollectionCrueUtil.select(emh, predicate, result);
    return result;
  }

  public static <T extends EMH> Set<T> selectSetOfEMHS(final Collection<EMH> emh, final EnumCatEMH catEMH) {
    final PredicateFactory.PredicateTypeEnum predicate = new PredicateFactory.PredicateTypeEnum(catEMH);
    final Set<T> result = new HashSet<>();
    CollectionCrueUtil.select(emh, predicate, result);
    return result;
  }

  public static int countEMHS(final Collection<EMH> emh, final EnumCatEMH catEMH) {
    final PredicateFactory.PredicateTypeEnum predicate = new PredicateFactory.PredicateTypeEnum(catEMH);
    return CollectionUtils.countMatches(emh, predicate);
  }

  public static boolean containsEMHWithCat(final Collection<EMH> emh, final EnumCatEMH catEMH) {
    final PredicateFactory.PredicateTypeEnum predicate = new PredicateFactory.PredicateTypeEnum(catEMH);
    return CollectionUtils.exists(emh, predicate);
  }

  public static boolean containsEMHWithCats(final Collection<EMH> emh, final EnumCatEMH... catEMH) {
    final PredicateFactory.PredicateTypeEnumArray predicate = new PredicateFactory.PredicateTypeEnumArray(catEMH);
    return CollectionUtils.exists(emh, predicate);
  }

  public static boolean containsEMHWithSubCatTypes(final Collection<EMH> emh, final Object... catEMH) {
    final PredicateFactory.PredicateSubTypeArray predicate = new PredicateFactory.PredicateSubTypeArray(catEMH);
    return CollectionUtils.exists(emh, predicate);
  }

  public static boolean containsEMHWithCats(final Collection<EMH> emh, final Collection<EnumCatEMH> catEMH) {
    final PredicateFactory.PredicateTypeEnumArray predicate = new PredicateFactory.PredicateTypeEnumArray(catEMH);
    return CollectionUtils.exists(emh, predicate);
  }

  /**
   * @param <T>             le type d'EMH voulu
   * @param emh             l'emh a parcourir
   * @param ref             la ref voulu
   * @param classOfRelation la classe de la relation a respecter
   * @return l'EMH de nom ref contenu par une RelationEMH de emh qui doit etre de type c
   */
  @SuppressWarnings("unchecked")
  public static <T extends EMH> T selectEMHInRelationByRef(final EMH emh, final String ref, final Class classOfRelation) {
    return (T) selectEMHInRelationByRef(emh, ref, classOfRelation, null);
  }

  /**
   * @param <T>             le type d'EMH voulu
   * @param emh             l'emh a parcourir
   * @param ref             la ref voulu
   * @param classOfRelation la classe de la relation a respecter
   * @param type            le type de l'EMH voulue
   * @return l'EMH de nom ref contenu par une RelationEMH de emh qui doit etre de type c
   */
  public static <T extends EMH> T selectEMHInRelationByRef(final EMH emh, final String ref,
                                                           final Class classOfRelation, final EnumCatEMH type) {
    final RelationEMH rel = selectRelationEMHByRef(emh, ref, classOfRelation, type);
    return rel == null ? null : (T) rel.getEmh();
  }

  /**
   * Parcourt les RelationEMHContient de l'EMH et renvoie les EMH du type demande
   *
   * @param <T>  le type d'EMH voulu
   * @param emh  l'emh a parcourir
   * @param type le type voulu
   * @return la liste des EMH du type demande
   */
  public static <T extends EMH> List<T> collectEMHInRelationEMHContient(final EMH emh, final EnumCatEMH type) {
    final List<RelationEMHContient> relations = new ArrayList<>();
    CollectionCrueUtil.select(emh.getRelationEMH(), new PredicateFactory.PredicateRelationEMHContientType(type), relations);
    return collectEMHInRelations(relations);
  }

  /**
   * Parcourt les RelationEMHContient de l'EMH et renvoie les EMH
   *
   * @param <T> le type d'EMH voulu
   * @param emh l'emh a parcourir
   * @return la liste des EMH du type demande
   */
  public static <T extends EMH> List<T> collectEMHInRelationEMHContient(final EMH emh) {
    if (emh == null || emh.isRelationsEMHEmpty()) {
      return Collections.emptyList();
    }
    final List<RelationEMHContient> relations = new ArrayList<>();
    CollectionCrueUtil.select(emh.getRelationEMH(), new PredicateFactory.PredicateRelationEMHContientType(null), relations);
    return collectEMHInRelations(relations);
  }

  /**
   * Parcourt les RelationEMHContient de l'EMH et renvoie les EMH
   *
   * @param <T>   le type d'EMH voulu
   * @param emh   l'emh a parcourir
   * @param types the EMH types to collect
   * @return la liste des EMH du type demande
   */
  public static <T extends EMH> List<T> collectEMHInRelationEMHContient(final EMH emh, EnumCatEMH... types) {
    if (emh == null || emh.isRelationsEMHEmpty()) {
      return Collections.emptyList();
    }
    List<Predicate<Object>> predicates = new ArrayList<>();
    for (EnumCatEMH type : types) {
      predicates.add(new PredicateFactory.PredicateRelationEMHContientType(type));
    }
    final List<RelationEMHContient> relations = new ArrayList<>();
    CollectionCrueUtil.select(emh.getRelationEMH(), AnyPredicate.anyPredicate(predicates), relations);
    return collectEMHInRelations(relations);
  }

  /**
   * @param <T> le type d'EMH voulu
   * @param emh l'EMH contenant
   * @param ref la reference
   * @return l'emh contenu de ref ref.
   */
  @SuppressWarnings("unchecked")
  public static <T extends EMH> T selectEMHInRelationEMHContientByRef(final EMH emh, final String ref) {
    return (T) selectEMHInRelationByRef(emh, ref, RelationEMHContient.class);
  }

  /**
   * @param <T>  le type d'EMH voulu
   * @param emh  l'EMH contenant
   * @param ref  la reference
   * @param type le type de l'EMH voulue.
   * @return l'emh contenu de ref ref.
   */
  @SuppressWarnings("unchecked")
  public static <T extends EMH> T selectEMHInRelationEMHContientByRef(final EMH emh, final String ref,
                                                                      final EnumCatEMH type) {
    return (T) selectEMHInRelationByRef(emh, ref, RelationEMHContient.class, type);
  }

  /**
   * @param <T>        type de la liste a parcourir
   * @param <K>        la classe voulue dans la liste
   * @param collection la collection a parcourir
   * @param clazz      la classe voulu
   * @return premier occurence trouvee
   */
  public static <T, K extends T> K selectFirstOfClass(final Collection<T> collection, final Class<K> clazz) {
    return (K) CollectionUtils.find(collection, new PredicateFactory.PredicateClass(clazz));
  }

  public static <T, K extends T> K selectFirstInstanceOf(final Collection<T> collection, final Class<K> clazz) {
    return (K) CollectionUtils.find(collection, new PredicateFactory.PredicateInstanceOf(clazz));
  }

  /**
   * @param <T>
   * @param <K>
   * @param collection
   * @param clazz
   * @return true si la collection contient au moins un objet dont la classe est clazz
   */
  public static <T, K extends T> boolean containsClass(final Collection<T> collection,
                                                       final Class<K> clazz) {
    return CollectionUtils.exists(collection, new PredicateFactory.PredicateClass(clazz));
  }

  public static <T, K extends T> int countClass(final Collection<T> collection, final Class<K> clazz) {
    return CollectionUtils.countMatches(collection, new PredicateFactory.PredicateClass(clazz));
  }

  /**
   * @param <T>
   * @param <K>
   * @param collection
   * @param clazz
   * @return true si la collection contient au moins un objet dont l'instance est compatible avec clazz
   */
  public static <T, K extends T> boolean containsInstanceOf(final Collection<T> collection, final Class<K> clazz) {
    return CollectionUtils.exists(collection, new PredicateFactory.PredicateInstanceOf(clazz));
  }

  /**
   * @param <T>        type des objets de la collections d'entree
   * @param <K>        le type a recuperer filtre
   * @param collection la colleciton a parcourir
   * @param clazz      la classe voulu
   * @return liste des objecte de collection dont la classe est clazz
   */
  public static <T, K extends T> List<K> selectClass(final Collection<T> collection, final Class<K> clazz) {
    final List<K> res = new ArrayList<>(collection.size());
    CollectionCrueUtil.select(collection, new PredicateFactory.PredicateClass(clazz), res);
    return res;
  }

  /**
   * @param <T>        type des objets de la collections d'entree
   * @param <K>        le type a recuperer filtre
   * @param collection la colleciton a parcourir
   * @param classes    les classes voulues
   * @return liste des objecte de collection dont la classe est clazz
   */
  @SuppressWarnings("unchecked")
  public static <T, K extends T> List<K> selectInClasses(final Collection<T> collection, final Class... classes) {

    final List<K> res = new ArrayList<>(collection.size());
    if (classes == null) {
      return res;
    }
    final Collection<Predicate<Object>> any = new ArrayList<>(classes.length);
    for (final Class<K> classItem : classes) {
      any.add(new PredicateFactory.PredicateClass(classItem));
    }
    CollectionCrueUtil.select(collection, PredicateUtils.anyPredicate(any), res);
    return res;
  }

  /**
   * @param <T>
   * @param collection
   * @return la liste d'EMH activable presente dans la collection.
   */
  public static <T extends CatEMHActivable> List<T> selectEMHUserActivated(final Collection<T> collection) {
    final List<T> res = new ArrayList<>(collection.size());
    CollectionUtils.select(collection, PredicateFactory.PREDICATE_IS_USER_ACTIVATED, res);
    return res;
  }

  /**
   * @param <T>
   * @param collection
   * @return la liste d'EMH activable presente dans la collection.
   */
  public static <T extends DonCLimMCommonItem> List<T> selectDonCLimMCommonUserActivated(final Collection<T> collection) {
    final List<T> res = new ArrayList<>(collection.size());
    CollectionUtils.select(collection, PredicateFactory.PREDICATE_IS_USER_ACTIVATED, res);
    return res;
  }

  /**
   * @param <T>        type des objets de la collections d'entree
   * @param <K>        le type a recuperer filtre
   * @param collection la colleciton a parcourir
   * @param clazz      la classe voulu
   * @return liste des objecte de collection dont la classe est une instanceof clazz
   */
  public static <T, K extends T> List<K> selectInstanceOf(final Collection<T> collection, final Class<K> clazz) {
    final List<K> res = new ArrayList<>(collection.size());
    CollectionCrueUtil.select(collection, new PredicateFactory.PredicateInstanceOf(clazz), res);
    return res;
  }

  /**
   * @param emh    l'emh a parcourir
   * @param search le type d'infoEMH recherche
   * @return le premier trouve
   */
  public static InfosEMH selectInfoEMH(final EMH emh, final EnumInfosEMH search) {
    return (InfosEMH) CollectionUtils.find(emh.getInfosEMH(), new PredicateFactory.PredicateEnumInfo(search));
  }

  public static List<RelationEMHSectionDansBranche> collectRelationsSections(final EMH emh) {
    final List<RelationEMHSectionDansBranche> liste = new ArrayList<>();
    filterRelationsSections(emh, liste, null);
    return liste;
  }

  public static void filterRelationsSections(final EMH emh, final List<RelationEMHSectionDansBranche> outSection,
                                             final List<RelationEMH> outNotSection) {
    for (final RelationEMH relation : emh.getRelationEMH()) {
      if (relation != null) {
        final boolean isSectionDansBranche = isRelationSectionDansBranche(relation);
        if (outSection != null && isSectionDansBranche) {
          outSection.add((RelationEMHSectionDansBranche) relation);
        } else if (outNotSection != null && !isSectionDansBranche) {
          outNotSection.add(relation);
        }
      }
    }
  }

  private static boolean isRelationSectionDansBranche(final RelationEMH relationEMH) {
    final Class<? extends RelationEMH> classRelation = relationEMH.getClass();
    return classRelation.equals(RelationEMHSectionDansBranche.class)
        || classRelation.equals(RelationEMHSectionDansBrancheSaintVenant.class);
  }

  public static List<RelationEMHSectionDansBranche> collectListeRelationsSectionsSortedByXp(final EMH emh,
                                                                                            final CrueConfigMetier props) {
    final List<RelationEMHSectionDansBranche> liste = collectRelationsSections(emh);
    Collections.sort(liste, new ComparatorRelationEMHSectionDansBranche(props));
    return liste;
  }

  /**
   * Retourne le noeud amont de la branche.
   *
   * @param emh
   */
  public static CatEMHNoeud getNoeudAmont(final EMH emh) {
    if (emh.isRelationsEMHNotEmpty()) {
      for (final RelationEMH relation : emh.getRelationEMH()) {
        if (relation.getEmh() != null && relation instanceof RelationEMHNoeudDansBranche) {
          final RelationEMHNoeudDansBranche res = (RelationEMHNoeudDansBranche) relation;
          if (res.isAmont()) {
            return res.getEmh();
          }
        }
      }
    }
    return null;
  }

  public static List<EMHSousModele> getParents(final EMH emh) {
    final Collection<RelationEMHDansSousModele> relations = selectRelationOfType(emh, RelationEMHDansSousModele.class);
    final List<EMHSousModele> sousModel = new ArrayList<>();
    for (final RelationEMHDansSousModele relation : relations) {
      sousModel.add(relation.getEmh());
    }
    return sousModel;
  }

  public static List<EMHSousModele> getParents(final Collection<? extends EMH> emhs) {
    final List<EMHSousModele> sousModel = new ArrayList<>();
    if (emhs == null) {
      return sousModel;
    }
    for (EMH emh : emhs) {
      final Collection<RelationEMHDansSousModele> relations = selectRelationOfType(emh, RelationEMHDansSousModele.class);
      for (final RelationEMHDansSousModele relation : relations) {
        sousModel.add(relation.getEmh());
      }
    }
    return sousModel;
  }

  public static EMHScenario getScenario(final EMH emh) {
    if (emh.getCatType().equals(EnumCatEMH.SCENARIO)) {
      return (EMHScenario) emh;
    }
    if (emh.getCatType().equals(EnumCatEMH.MODELE)) {
      return ((EMHModeleBase) emh).getParent();
    }
    if (emh.getCatType().equals(EnumCatEMH.SOUS_MODELE)) {
      return ((EMHSousModele) emh).getParent().getParent();
    }
    final EMHSousModele sousModele = getParent(emh);
    return sousModele == null ? null : sousModele.getParent().getParent();
  }

  public static EMHSousModele getParent(final EMH emh) {
    final RelationEMHDansSousModele selectFirstOfClass = selectFirstOfClass(emh.getRelationEMH(), RelationEMHDansSousModele.class);
    return selectFirstOfClass == null ? null : selectFirstOfClass.getEmh();
  }

  /**
   * Retourne le noeud aval de la branche.
   *
   * @param emh
   */
  public static CatEMHNoeud getNoeudAval(final EMH emh) {
    if (emh.isRelationsEMHNotEmpty()) {
      for (final RelationEMH relation : emh.getRelationEMH()) {
        if (relation == null) {
          Logger.getLogger(EMHHelper.class.getName()).log(Level.INFO, "relation null found for {0}", emh.getId());
        }
        if (relation != null && relation.getEmh() != null && relation.getType().equals("RelationEMHNoeudDansBranche")) {
          final RelationEMHNoeudDansBranche res = (RelationEMHNoeudDansBranche) relation;
          if (res.isAval()) {
            return res.getEmh();
          }
        }
      }
    }
    return null;
  }

  /**
   * @return le noeud associe
   */
  public static CatEMHNoeud getNoeudCasier(final CatEMHCasier emh) {
    final RelationEMHNoeudContientCasier rel = selectFirstOfClass(emh.getRelationEMH(),
        RelationEMHNoeudContientCasier.class);
    return rel == null ? null : rel.getEmh();
  }

  /**
   * Reserve pour DRSO les sections des branches. Pos
   */
  public static EnumPosSection getPositionSection(final RelationEMH relation) {
    if (relation instanceof RelationEMHSectionDansBranche) {
      return ((RelationEMHSectionDansBranche) relation).getPos();
    }
    return null;
  }

  private static RelationEMH selectRelationEMHByRef(final EMH emh, final String ref, final Class c,
                                                    final EnumCatEMH type) {
    final PredicateFactory.PredicateRelationEMHContientId pred = new PredicateFactory.PredicateRelationEMHContientId(
        ref, c, type);
    return (RelationEMH) CollectionUtils.find(emh.getRelationEMH(), pred);
  }

  /**
   * @param <T>             le type de relation cherchée
   * @param emh             l'EMH parcourue
   * @param classOfRelation la classe de la relation cherchée
   * @return la collection voulue.
   */
  public static <T extends RelationEMH> Collection<T> selectRelationOfType(final EMH emh, final Class<T> classOfRelation) {
    if (CollectionUtils.isEmpty(emh.getRelationEMH())) {
      return Collections.emptyList();
    }
    final PredicateFactory.PredicateRelationEMHContientId pred = new PredicateFactory.PredicateRelationEMHContientId(
        null, classOfRelation, null);
    final Collection<T> res = new ArrayList<>(emh.getRelationEMH().size());
    CollectionCrueUtil.select(emh.getRelationEMH(), pred, res);
    return res;
  }

  public static CatEMHCasier getCasier(final CatEMHNoeud emh) {
    final PredicateFactory.PredicateRelationEMHContientId pred = new PredicateFactory.PredicateRelationEMHContientId(
        null, RelationEMHCasierDansNoeud.class, EnumCatEMH.CASIER);
    final RelationEMHCasierDansNoeud find = (RelationEMHCasierDansNoeud) CollectionUtils.find(emh.getRelationEMH(),
        pred);
    return find == null ? null : find.getEmh();
  }

  /**
   * @param emh l'emh a parcourir
   * @param ref la reference voulu
   * @param c   la classe de la relation: si null, non pris en compte
   * @return la liste des relation dont l'EMH est de nom ref.
   */
  public static List<RelationEMH> getRelationEMHsByRef(final EMH emh, final String ref, final Class c) {
    final PredicateFactory.PredicateRelationEMHContientId pred = new PredicateFactory.PredicateRelationEMHContientId(
        ref, c);
    final List<RelationEMH> res = new ArrayList<>(emh.getRelationEMH().size());
    CollectionUtils.select(emh.getRelationEMH(), pred, res);
    return res;
  }

  public static CatEMHSection getSectionPilote(final EMH emh) {
    final RelationEMHSectionPiloteDansBranche res = selectFirstOfClass(emh.getRelationEMH(),
        RelationEMHSectionPiloteDansBranche.class);
    return res == null ? null : res.getEmh();
  }

  public static CatEMHBranche getBranchePilotedBy(final CatEMHSection emh) {
    final RelationEMHBrancheContientSectionPilote res = selectFirstOfClass(emh.getRelationEMH(),
        RelationEMHBrancheContientSectionPilote.class);
    return res == null ? null : res.getEmh();
  }

  /**
   * @param emh la section idem
   * @return la section de ref
   */
  public static CatEMHSection getSectionRef(final EMHSectionIdem emh) {
    final RelationEMHSectionDansSectionIdem relation = getRelationSectionRef(emh);
    if (relation != null) {
      return relation.getEmh();
    }
    return null;
  }

  /**
   * @param emh la section
   * @return toutes les sections idems utilisant cette section.
   */
  public static List<EMHSectionIdem> getSectionReferencing(final CatEMHSection emh) {
    final List<EMHSectionIdem> sectionIdems = new ArrayList<>();
    final List<RelationEMHSectionIdemContientSection> relations = getRelationSectionIdemUsing(emh);
    for (final RelationEMHSectionIdemContientSection relation : relations) {
      sectionIdems.add(relation.getEmh());
    }
    return sectionIdems;
  }

  /**
   * @param emh  la section de ref
   * @param idem la section idem a chercher
   * @return la relation si trouvee, null sinon
   */
  public static RelationEMHSectionIdemContientSection getRelationEMHSectionIdemContientSection(final CatEMHSection emh, final EMHSectionIdem idem) {
    final List<RelationEMHSectionIdemContientSection> relations = getRelationSectionIdemUsing(emh);
    for (final RelationEMHSectionIdemContientSection relation : relations) {
      if (relation.getEmh() == idem) {
        return relation;
      }
    }
    return null;
  }

  /**
   * @param ssModele la liste de sous-modele
   * @return le sous-modele concatene
   */
  public static EMHSousModele concat(final List<EMHSousModele> ssModele) {
    return concatIn(null, ssModele);
  }

  /**
   * @param dest     le sous modele a modifier et ajouter la concatenation des ss modele. Si null, un nouvelle instance est creee
   * @param ssModele les sous-modele a concatener dans dest
   * @return dest si non null ou nouvelle instance
   */
  private static EMHSousModele concatIn(final EMHSousModele dest, final List<EMHSousModele> ssModele) {
    return concatInNotSorted(dest, ssModele);
  }

  /**
   * @param scenario ne doit pas etre null
   * @return liste non null
   */
  public static List<DonFrt> getAllDonFrt(final EMHScenario scenario) {
    final List<DonFrt> res = new ArrayList<>();
    final List<EMHSousModele> sousModeles = scenario.getSousModeles();
    for (final EMHSousModele eMHSousModele : sousModeles) {
      final DonFrtConteneur frtConteneur = eMHSousModele.getFrtConteneur();
      if (frtConteneur != null) {
        res.addAll(frtConteneur.getListFrt());
      }
    }
    return res;
  }

  public static EMHSousModele concatInNotSorted(final EMHSousModele dest, final List<EMHSousModele> ssModele) {
    EMHSousModele res = dest;
    if (res == null) {
      res = new EMHSousModele();
    }
    final Set<EMH> done = new HashSet<>();
    for (final EMHSousModele sousModele : ssModele) {
      // on ajoute toutes les relationEMH.
      final List<RelationEMHContient> selectClass = selectClass(sousModele.getRelationEMH(), RelationEMHContient.class);
      final List<RelationEMHContient> finalRelation = new ArrayList<>(selectClass.size());

      for (final RelationEMHContient relationEMHContient : selectClass) {
        if (!done.contains(relationEMHContient.getEmh())) {
          done.add(relationEMHContient.getEmh());
          finalRelation.add(relationEMHContient);
        }
      }
      res.addAllRelations(finalRelation);
      res.getFrtConteneur().addAllFrt(sousModele.getFrtConteneur().getListFrt());
      res.addAllInfos(selectClass(sousModele.getInfosEMH(), DonPrtGeoProfilSection.class));
      res.addAllInfos(selectClass(sousModele.getInfosEMH(), DonPrtGeoProfilCasier.class));
      res.addAllInfos(selectClass(sousModele.getInfosEMH(), DonPrtGeoBatiCasier.class));
    }
    return res;
  }

  /**
   * @param scenario le scenario
   * @return tous les frottements des sous-modele du scenario
   */
  public static DonFrtConteneur getAllFrottements(final EMHScenario scenario) {
    final DonFrtConteneur res = new DonFrtConteneur();
    for (final EMHSousModele sousModele : scenario.getSousModeles()) {
      if (sousModele.getFrtConteneur() != null) {
        res.addAllFrt(sousModele.getFrtConteneur().getListFrt());
      }
    }
    res.sort();
    return res;
  }

  public static DonPrtGeoSectionIdem getDPTGSectionIdem(final EMHSectionIdem section) {
    return selectFirstOfClass(section.getInfosEMH(), DonPrtGeoSectionIdem.class);
  }

  /**
   * @param distmax  le distmax a respecter
   * @param distance la distance actuelle
   * @return l'entier a utiliser pour diviser la distance afin de créer des profils distancés de moins de distmax.
   */
  public static int getDistDiviseurForDistMax(final double distmax, final double distance) {
    return (int) Math.ceil(distance / distmax);
  }

  /**
   * @param calcPerm le calcul
   * @param ccm      le CrueConfigMetier
   * @return null si different ou si pas de CalcPseudoPerm*Qruis
   */
  public static Double isQruisConstantInUserActive(final CalcPseudoPerm calcPerm, final CrueConfigMetier ccm) {
    final List<CalcPseudoPermBrancheSaintVenantQruis> calcQruis = calcPerm.getDclmUserActive(CalcPseudoPermBrancheSaintVenantQruis.class);
    final List<CalcPseudoPermCasierProfilQruis> calcCasierQruis = calcPerm.getDclmUserActive(CalcPseudoPermCasierProfilQruis.class);
    final PropertyEpsilon eps = ccm.getEpsilon("qruis");
    if (CollectionUtils.isNotEmpty(calcQruis) || CollectionUtils.isNotEmpty(calcCasierQruis)) {
      double qruis = 0;
      boolean qruisSet = false;
      if (calcQruis != null) {
        for (int j = 0, jmax = calcQruis.size(); j < jmax; j++) {

          final CalcPseudoPermBrancheSaintVenantQruis brancheSV = calcQruis.get(j);
          if (brancheSV.getEmh().getActuallyActive()) {
            if (!qruisSet) {
              qruisSet = true;
              qruis = brancheSV.getQruis();
            }
            if (!eps.isSame(brancheSV.getQruis(), qruis)) {
              return null;
            }
          }
        }
      }
      if (calcCasierQruis != null) {
        for (int j = 0, jmax = calcCasierQruis.size(); j < jmax; j++) {
          final CalcPseudoPermCasierProfilQruis casier = calcCasierQruis.get(j);
          if (casier.getEmh().getActuallyActive()) {
            if (!qruisSet) {
              qruisSet = true;
              qruis = casier.getQruis();
            }
            if (!CtuluLib.isEquals(casier.getQruis(), qruis, 1E-8)) {
              return null;
            }
          }
        }
      }
      return Double.valueOf(qruis);
    }
    return ccm.getDefaultDoubleValue("qRuis");
  }

  public static DonFrt find(final List<DonFrt> list, final String nom) {
    for (final DonFrt donFrt : list) {
      if (nom.equals(donFrt.getNom())) {
        return donFrt;
      }
    }
    return null;
  }

  public static final class OrdCalToCalc implements Transformer {
    @Override
    public Object transform(final Object input) {
      return ((OrdCalc) input).getCalc();
    }
  }

  public static final class OrdCalcIsTransitoire implements Predicate {
    @Override
    public boolean evaluate(final Object object) {
      return ((OrdCalc) object).isTransitoire();
    }
  }

  public static final class OrdCalcIsPermanent implements Predicate {
    @Override
    public boolean evaluate(final Object object) {
      return ((OrdCalc) object).isPseudoPermanent();
    }
  }

  public static final class OrdCalcIsPseudoPermanent implements Predicate {
    @Override
    public boolean evaluate(final Object object) {
      return ((OrdCalc) object).isPseudoPermanent() && ((OrdCalc) object).getCalc() instanceof CalcPseudoPerm;
    }
  }

  /**
   * @author deniger return true si l'ordCalc possede un calcul de type CalcTrans.
   */
  public static final class OrdCalcIsCalcTrans implements Predicate {
    @Override
    public boolean evaluate(final Object object) {
      return ((OrdCalc) object).isTransitoire() && ((OrdCalc) object).getCalc() instanceof CalcTrans;
    }
  }
}
