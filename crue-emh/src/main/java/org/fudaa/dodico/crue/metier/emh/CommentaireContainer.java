package org.fudaa.dodico.crue.metier.emh;

/**
 * @author deniger
 */
public interface CommentaireContainer {

  CommentairesManager getCommentairesManager();

}
