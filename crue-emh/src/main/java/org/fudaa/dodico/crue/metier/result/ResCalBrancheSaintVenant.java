package org.fudaa.dodico.crue.metier.result;

/**
 * @author deniger
 */
public class ResCalBrancheSaintVenant extends ResCalcul {


  public void setSplanAct(double splanAct) {
    super.addResultat("splanAct", splanAct);
  }

  public void setSplanTot(double splanTot) {
    super.addResultat("splanTot", splanTot);
  }

  public void setVol(double vol) {
    super.addResultat("vol", vol);
  }

  public void setSplanSto(double splanSto) {
    super.addResultat("splanSto", splanSto);
  }
}
