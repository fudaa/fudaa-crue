package org.fudaa.dodico.crue.metier.transformer;

import java.util.Collection;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformerCollection;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.emh.ElemOrifice;
import org.fudaa.dodico.crue.metier.emh.ElemPdt;
import org.fudaa.dodico.crue.metier.emh.LitUtile;
import org.fudaa.dodico.crue.metier.emh.ParamNumCalcPseudoPerm;
import org.fudaa.dodico.crue.metier.emh.ParamNumCalcTrans;
import org.fudaa.dodico.crue.metier.emh.PdtCst;
import org.fudaa.dodico.crue.metier.emh.PlanimetrageNbrPdzCst;
import org.fudaa.dodico.crue.metier.emh.RelationEMH;

/**
 * Constructeur par defaut pour un transformer utilisant les formatteurs de CrueProperties.
 *
 * @author deniger
 */
public class ToStringTransfomerFactory {

  public static ToStringTransformer create(CrueConfigMetier props, DecimalFormatEpsilonEnum type) {
    ToStringTransformerEMHDelegate delegate = new ToStringTransformerEMHDelegate(props,type);
    ToStringTransformerDefault propsTransformer = new ToStringTransformerDefault(props, type);
    propsTransformer.setAddClassName(true);
    ToStringTransformerDefault propsTransformerWithoutClassName = new ToStringTransformerDefault(props, type);
    propsTransformerWithoutClassName.setAddClassName(false);

    ToStringTransformerFromTransformable fromTransformable = new ToStringTransformerFromTransformable(props,type);

    // on ajoutera les propriétés pdt et nom dans le toString
    addWellKnownVariables(propsTransformer);
    addWellKnownVariables(propsTransformerWithoutClassName);

    delegate.addTransformerClass(ParamNumCalcPseudoPerm.class, propsTransformer);
    delegate.addTransformerClass(ParamNumCalcTrans.class, propsTransformer);
    delegate.addTransformerClass(ElemOrifice.class, ElemOrifice.createTransformer(props));
    delegate.addTransformerClass(PlanimetrageNbrPdzCst.class, propsTransformer);
    delegate.addTransformerClass(LitUtile.class, fromTransformable);
    delegate.addTransformerClass(PdtCst.class, fromTransformable);
    delegate.addTransformerClass(ElemPdt.class, fromTransformable);

    delegate.addTransformerInstanceOf(RelationEMH.class, propsTransformer);
    delegate.addTransformerInstanceOf(Collection.class, new ToStringTransformerCollection(delegate));

    return delegate;
  }

  private static void addWellKnownVariables(ToStringTransformerDefault propsTransformer) {
    propsTransformer.addSupplementProp("pdt");
    propsTransformer.addSupplementProp("nom");
  }
}
