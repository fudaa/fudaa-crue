/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.helper;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.fudaa.dodico.crue.metier.OrdResKey;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;

/**
 *
 * @author Frederic Deniger
 */
public class EMHCatTypeContent {

  private final Set<EnumCatEMH> catEMHs = new HashSet<>();
  private final Set<EnumBrancheType> catBranchesTypes = new HashSet<>();

  public void addEMH(EMH emh) {
    catEMHs.add(emh.getCatType());
    if (EnumCatEMH.BRANCHE.equals(emh.getCatType())) {
      catBranchesTypes.add(((CatEMHBranche) emh).getBrancheType());
    }
  }
  

  public boolean contain(OrdResKey key) {
    boolean ok = catEMHs.contains(key.getCatEMH());
    if (ok && key.getBrancheType() != null) {
      ok = catBranchesTypes.contains(key.getBrancheType());
    }
    return ok;
  }

  public static EMHCatTypeContent create(Collection<EMH> emhs) {
    EMHCatTypeContent res = new EMHCatTypeContent();
    for (EMH emh : emhs) {
      res.addEMH(emh);
    }
    return res;
  }
}
