package org.fudaa.dodico.crue.metier.emh;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;

/**
 * Le fichier RPTR n'est lu que par Fudaa-Crue, jamais par le coeur de calcul. Il contient la liste des noeuds ordonnée
 * par le prétraitement réseau selon la méthode d'ordonnancement choisie dans le OPTR. Ce nouvel ordonnancement est lu
 * par Fudaa-Crue et proposé à l'utilisateur. Si l'utilisateur décide de l'adopter, Fudaa-Crue l'applique au DRSO, dans
 * la section <Noeuds>. De cette façon, le coeur de calcul n'a pas à lire le RPTR, seulement le
 * 
 * @author deniger
 */
public class ResPrtReseauNoeud implements ObjetNomme {

  private final String nom;
  private final String id;

  /**
   * @param nom
   */
  public ResPrtReseauNoeud(String nom) {
    super();
    this.nom = nom;
    id = StringUtils.upperCase(nom);

  }
  
  @Override
  public String toString() {
    return getClass().getSimpleName()+" "+nom;
  }
  
  

  /**
   * @return the nom
   */
  @Override
  public String getNom() {
    return nom;
  }

  @Override
  public String getId() {
    return id;
  }



  @Override
  public void setNom(String newNom) {}

}
