/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;

/**
 * Contient un etat des modifications appporteés automatiquement lors du chargement du scenario.
 *
 * @author Frederic Deniger
 */
public class ScenarioAutoModifiedState {
  private boolean reorderedDone;
  private boolean renamedDone;
  private boolean profilModified;
  private boolean ordResModified;
  private boolean dptiModified;

  public boolean isDptiModified() {
    return dptiModified;
  }

  /**
   * Appelée si DPTI sont manquantes
   *
   * @param dptiModified
   */
  public void setDptiModified(boolean dptiModified) {
    this.dptiModified = dptiModified;
  }

  /**
   * des EMHs ont été réordonnées
   *
   * @see EMHScenario#sort()
   */
  public void setReorderedDone() {
    this.reorderedDone = true;
  }

  /**
   * @return true si des EMHs ont été réordonnées
   * @see EMHScenario#sort()
   */
  public boolean isReorderedDone() {
    return reorderedDone;
  }

  /**
   * @return true si des EMHS/lois ont été renommées (Crue 9)
   */
  public boolean isRenamedDone() {
    return renamedDone;
  }

  /**
   * @return true si des Profiles ont été modifies
   */
  public boolean isProfilModified() {
    return profilModified;
  }

  /**
   * @return si des OrdRes ont été modifiés
   */
  public boolean isOrdResModified() {
    return ordResModified;
  }

  /**
   * A appeler si des EMHS/lois ont été renommées (Crue 9)
   */
  public void setRenamedDone() {
    this.renamedDone = true;
  }

  /**
   * A appeler si des Profiles ont été modifies
   */
  public void setProfilModified() {
    this.profilModified = true;
  }

  /**
   * A appeler si des OrdRes ont été modifiés
   */
  public void setOrdResModified() {
    this.ordResModified = true;
  }

  /**
   * @return true si au moins une modification a été apportée
   */
  public boolean isNormalized() {
    return renamedDone || reorderedDone || profilModified || ordResModified || dptiModified;
  }
}
