package org.fudaa.dodico.crue.metier.comparator;


import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

/**
 * A comparator for ObjetNomme
 * 
 * @author deniger
 */
public class ObjetNommeByNameComparator extends SafeComparator<ObjetWithID> {

  /**
   * Comparator singleton.
   */
  public static final ObjetNommeByNameComparator INSTANCE = new ObjetNommeByNameComparator();

  private ObjetNommeByNameComparator() {

  }

  @Override
  protected int compareSafe(ObjetWithID o1, ObjetWithID o2) {
    int compareString = compareString(o1.getNom(), o2.getNom());
    return compareString == 0 ? o1.hashCode() - o2.hashCode() : compareString;
  }


}
