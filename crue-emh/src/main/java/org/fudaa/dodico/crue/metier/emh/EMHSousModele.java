/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.emh;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

import java.util.Collection;
import java.util.List;

public class EMHSousModele extends CatEMHConteneur implements CommentaireContainer {
  @Override
  public EnumCatEMH getCatType() {
    return EnumCatEMH.SOUS_MODELE;
  }

  @Override
  public Object getSubCatType() {
    return null;
  }

  CommentairesManager commentaires;

  public EMHSousModele() {
    super();
  }

  @Visibility(ihm = false)
  @UsedByComparison(ignoreInComparison = true)
  public EMHModeleBase getParent() {
    final RelationEMHSousModeleDansModele relation = EMHHelper.selectFirstOfClass(getRelationEMH(),
        RelationEMHSousModeleDansModele.class);
    return relation == null ? null : relation.getEmh();
  }

  @Override
  public boolean getActuallyActive() {
    boolean res = super.getActuallyActive();
    if (res) {
      final EMHModeleBase parent = getParent();
      res = parent != null && parent.getActuallyActive();
    }
    return res;
  }

  @Override
  public CommentairesManager getCommentairesManager() {
    if (commentaires == null) {
      commentaires = new CommentairesManager();
    }
    return commentaires;
  }

  @Override
  protected <T extends EMH> T copyPrimitiveIn(T in) {
    if (commentaires != null) {
      ((EMHSousModele) in).commentaires = commentaires.copy();
    }
    return super.copyPrimitiveIn(in);
  }

  /**
   * @return le conteneur de frottement toujour non null
   */
  public DonFrtConteneur getFrtConteneur() {
    return initDonFrtConteneur();
  }

  public DonFrtConteneur initDonFrtConteneur() {
    DonFrtConteneur frtConteneur = (DonFrtConteneur) EMHHelper.selectInfoEMH(this, EnumInfosEMH.DON_FRT_CONTENEUR);
    if (frtConteneur == null) {
      frtConteneur = new DonFrtConteneur();
      addInfosEMH(frtConteneur);
    }
    return frtConteneur;
  }

  /**
   * sort le Frts and the the contained EMH.
   */
  @Override
  public boolean sort() {
    boolean modified = super.sort();
    final List<CatEMHBranche> branches = getBranches();
    if (CollectionUtils.isNotEmpty(branches)) {
      for (final CatEMHBranche catEMHBranche : branches) {
        modified |= catEMHBranche.sortRelationSectionDansBranche(null);
      }
    }
    return modified;
  }

  public void addAllInfos(final Collection<? extends InfosEMH> infos) {
    if (infos != null) {
      for (final InfosEMH info : infos) {
        addInfosEMH(info);
      }
    }
  }

  @Override
  public List<CatEMHSection> getSections() {
    return CatEMHConteneurHelper.getSections(this);
  }

  @Override
  public List<CatEMHNoeud> getNoeuds() {
    return CatEMHConteneurHelper.getNoeuds(this);
  }

  @Override
  public List<CatEMHBranche> getBranches() {
    return CatEMHConteneurHelper.getBranches(this);
  }

  @Override
  public List<EMHBrancheSaintVenant> getBranchesSaintVenant() {
    return CatEMHConteneurHelper.getBranchesSaintVenant(this);
  }

  @Override
  public List<CatEMHBranche> getBranchesAvecSectionPilote() {
    return CatEMHConteneurHelper.getBranchesAvecSectionPilote(this);
  }

  /**
   * @return les casiers dans l'ordre reseau.
   */
  @Override
  public List<CatEMHCasier> getCasiers() {
    return CatEMHConteneurHelper.getCasiers(this);
  }

  /**
   * @return tous les emh contenu par ce modele
   */
  @Override
  public List<EMH> getAllSimpleEMH() {
    return EMHHelper.collectEMHInRelationEMHContient(this);
  }

  public List<EMH> getAllSimpleEMH(EnumCatEMH... types) {
    return EMHHelper.collectEMHInRelationEMHContient(this,types);
  }

  @Override
  public List<EMH> getAllSimpleEMHinUserOrder() {
    return EMHHelper.collectEMHInRelationEMHContient(this);
  }

  @Override
  @Visibility(ihm = false)
  public EMHSousModele copyShallowFirstLevel() {
    return  copyPrimitiveIn(new EMHSousModele());
  }
}
