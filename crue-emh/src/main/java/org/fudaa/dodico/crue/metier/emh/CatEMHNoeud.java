package org.fudaa.dodico.crue.metier.emh;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

public abstract class CatEMHNoeud extends EMH {

  @Override
  public EnumCatEMH getCatType() {
    return EnumCatEMH.NOEUD;
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public abstract EnumNoeudType getNoeudType();

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public Object getSubCatType() {
    return getNoeudType();
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public List<EMHSousModele> getParents() {
    return EMHHelper.getParents(this);
  }

  public CatEMHNoeud(final String nom) {
    super(nom);
  }

  @Override
  public final boolean getUserActive() {// NOPMD
    return true;
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public List<CatEMHBranche> getBranches() {
    final Collection<RelationEMHBrancheContientNoeud> branchesRelation = EMHHelper.selectRelationOfType(this,
            RelationEMHBrancheContientNoeud.class);
    if (CollectionUtils.isEmpty(branchesRelation)) {
      return Collections.emptyList();
    }
    List<CatEMHBranche> res = new ArrayList<>();
    for (RelationEMHBrancheContientNoeud relation : branchesRelation) {
      res.add(relation.getEmh());
    }
    return res;
  }

  /**
   * Liste non null des branches amont ( dans le noeud aval est this)
   */
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public List<CatEMHBranche> getBranchesAmont() {
    final Collection<RelationEMHBrancheContientNoeud> branchesRelation = EMHHelper.selectRelationOfType(this,
            RelationEMHBrancheContientNoeud.class);
    if (CollectionUtils.isEmpty(branchesRelation)) {
      return Collections.emptyList();
    }
    List<CatEMHBranche> res = new ArrayList<>();
    for (RelationEMHBrancheContientNoeud relation : branchesRelation) {
      if (relation.getEmh().getNoeudAvalId().equals(getId())) {
        res.add(relation.getEmh());
      }
    }
    return res;
  }

  /**
   * Liste non null des branches amont ( dans le noeud amont est this)
   */
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public List<CatEMHBranche> getBranchesAval() {
    final Collection<RelationEMHBrancheContientNoeud> branchesRelation = EMHHelper.selectRelationOfType(this,
            RelationEMHBrancheContientNoeud.class);
    if (CollectionUtils.isEmpty(branchesRelation)) {
      return Collections.emptyList();
    }
    List<CatEMHBranche> res = new ArrayList<>();
    for (RelationEMHBrancheContientNoeud relation : branchesRelation) {
      if (relation.getEmh().getNoeudAmontId().equals(getId())) {
        res.add(relation.getEmh());
      }
    }
    return res;
  }

  @Override
  public boolean getActuallyActive() {
    final boolean res = super.getActuallyActive();
    if (res) {
      final Collection<RelationEMHBrancheContientNoeud> brs = EMHHelper.selectRelationOfType(this,
              RelationEMHBrancheContientNoeud.class);
      for (final RelationEMHBrancheContientNoeud relationEMHNoeudDansBranche : brs) {
        if (relationEMHNoeudDansBranche.getEmh().getActuallyActive()) {
          return true;
        }

      }
    }
    return false;
  }

  public String getIdCasier() {
    final CatEMHCasier res = getCasier();
    return res == null ? null : res.getId();
  }

  public CatEMHCasier getCasier() {
    return EMHHelper.getCasier(this);
  }
}
