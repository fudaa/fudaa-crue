/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;

public class EMHSousModeleEnchainement extends EMH {

  @Override
  public EnumCatEMH getCatType() {
    return EnumCatEMH.SOUS_MODELE_ENCHAINEMENT;
  }

  @Override
  public Object getSubCatType() {
    return null;
  }

  @Override
  public final boolean getUserActive() {// NOPMD
    return true;
  }

  @Override
  @Visibility(ihm = false)
  public EMHSousModeleEnchainement copyShallowFirstLevel() {
    return  copyPrimitiveIn(new EMHSousModeleEnchainement());
  }
}
