/***********************************************************************
 * Module: ValParamDouble.java Author: deniger Purpose: Defines the Class ValParamDouble
 ***********************************************************************/
package org.fudaa.dodico.crue.metier.emh;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;

public class ValParamDouble extends ValParam {
  private double valeur;

  public ValParamDouble(final String nom, final Double valeur) {
    super(nom);
    this.valeur = valeur;
  }

  @PropertyDesc(i18n = "valParamValeur.property")
  public final double getValeur() {
    return valeur;
  }

  @Override
  public Object getValeurObjet() {
    return valeur;
  }

  /**
   * @return le type de donnée portée par ce ValParam
   */
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public Class getTypeData() {
    return Double.TYPE;
  }

  public final void setValeur(double newValeur) {
    valeur = newValeur;
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    return "ValParamDouble[ " + TransformerEMHHelper.formatFromPropertyName(StringUtils.uncapitalize(getNom()), valeur, props, DecimalFormatEpsilonEnum.COMPARISON) + " ]";
  }
}
