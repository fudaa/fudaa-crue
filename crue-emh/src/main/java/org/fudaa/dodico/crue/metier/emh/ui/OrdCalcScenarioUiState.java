/*
GPL 2
 */
package org.fudaa.dodico.crue.metier.emh.ui;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import org.fudaa.dodico.crue.metier.emh.OrdCalc;

/**
 * Une classe permettant de persister les éditions de l'utilisateurs dans l'interface. Cette classe sera persistée comme un fichier OCAL mais ne fera
 * pas parti des fichiers de l'étude pour ne pas perturber le moteur de Crue. Est uniquement gérer par la partie UI. Elle gère une liste de
 * CalcOrdCalcUiState ce qui permet de stocker l'ordre, les clichés,...
 *
 * @author Frederic Deniger
 */
public class OrdCalcScenarioUiState {

  private final LinkedHashMap<String, CalcOrdCalcUiState> calcOrdCalcUiStates = new LinkedHashMap<>();

  public void addCalcOrdCalcUiStates(String calcId, OrdCalc calcOrdCalcUiState) {
    calcOrdCalcUiStates.put(calcId.toUpperCase(), new CalcOrdCalcUiState(calcId, calcOrdCalcUiState));
  }

  /**
   *
   * @return la liste des noms de calculs dans l'ordre
   */
  public List<String> getCalcNameInOrder() {
    return new ArrayList<>(calcOrdCalcUiStates.keySet());
  }

  public void addAll(OrdCalcScenarioUiState other) {
    if (other != null) {
      calcOrdCalcUiStates.putAll(other.calcOrdCalcUiStates);
    }
  }

  /**
   *
   * @return la liste des ordCalc UI dans l'ordre
   */
  public List<CalcOrdCalcUiState> getUiStates() {
    return new ArrayList<>(calcOrdCalcUiStates.values());
  }

  /**
   *
   * @param calc le nom du calc
   * @return l'etat correspondant
   */
  public CalcOrdCalcUiState getUiState(String calc) {
    return calcOrdCalcUiStates.get(calc);
  }

}
