/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.List;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

/**
 */
public class EMHModeleBase extends EMHConteneurDeSousModele {

  @Override
  public EnumCatEMH getCatType() {
    return EnumCatEMH.MODELE;
  }

  @Override
  public Object getSubCatType() {
    return null;
  }

  public ResultatPasDeTemps getResultatPasDeTemps() {
    return EMHHelper.selectFirstOfClass(getInfosEMH(), ResultatPasDeTemps.class);
  }

  public EMHSousModele getConcatSousModele() {
    return EMHHelper.concat(getSousModeles());
  }

  @Visibility(ihm = false)
  @UsedByComparison(ignoreInComparison = true)
  public EMHScenario getParent() {
    final RelationEMHModeleDansScenario relation = EMHHelper.selectFirstOfClass(getRelationEMH(),
            RelationEMHModeleDansScenario.class);
    return relation == null ? null : relation.getEmh();
  }

  @UsedByComparison
  @Visibility(ihm = false)
  public CompteRendu getCompteRendu(final CrueFileType type) {
    final CompteRendusInfoEMH cr = EMHHelper.selectFirstOfClass(getInfosEMH(), CompteRendusInfoEMH.class);
    if (cr == null) {
      return null;
    }
    return cr.getCompteRendus(type);
  }

  @UsedByComparison
  @Visibility(ihm = false)
  public CompteRendu getCompteRenduPTR() {
    return getCompteRendu(CrueFileType.RPTR);
  }

  @UsedByComparison
  @Visibility(ihm = false)
  public CompteRendu getCompteRenduPTI() {
    return getCompteRendu(CrueFileType.RPTI);
  }

  @UsedByComparison
  @Visibility(ihm = false)
  public CompteRendu getCompteRenduPTG() {
    return getCompteRendu(CrueFileType.RPTG);
  }

  @UsedByComparison
  @Visibility(ihm = false)
  public CompteRendu getCompteRenduCAL() {
    return getCompteRendu(CrueFileType.RCAL);
  }

  /**
   */
  public EMHModeleBase() {
  }

  /**
   * @return liste des modele
   */
  @Override
  public List<EMHSousModele> getSousModeles() {
    return EMHHelper.collectEMHInRelationEMHContient(this, EnumCatEMH.SOUS_MODELE);
  }

  @Override
  public boolean sort() {
    boolean res = super.sort();
    for (final EMHSousModele modele : getSousModeles()) {
      res |= modele.sort();
    }
    return res;
  }

  public ParamNumModeleBase getParamNumModeleBase() {
    return (ParamNumModeleBase) EMHHelper.selectInfoEMH(this, EnumInfosEMH.PARAM_NUM_MODELE_BASE);
  }

  @UsedByComparison
  public OrdPrtGeoModeleBase getOrdPrtGeoModeleBase() {
    return (OrdPrtGeoModeleBase) EMHHelper.selectInfoEMH(this, EnumInfosEMH.ORD_PRT_GEO_MODELE_BASE);
  }

  @UsedByComparison
  public OrdPrtCIniModeleBase getOrdPrtCIniModeleBase() {
    return (OrdPrtCIniModeleBase) EMHHelper.selectInfoEMH(this, EnumInfosEMH.ORD_PRT_CINI_MODELE_BASE);
  }

  @UsedByComparison
  public ResPrtReseau getResPrtReseau() {
    return (ResPrtReseau) EMHHelper.selectInfoEMH(this, EnumInfosEMH.RES_PRT_RESEAU);
  }

  @UsedByComparison
  public OrdPrtReseau getOrdPrtReseau() {
    return (OrdPrtReseau) EMHHelper.selectInfoEMH(this, EnumInfosEMH.ORD_PRT_RESEAU);

  }

  public void setOrdPrtReseau(final OrdPrtReseau optr) {
    addInfosEMH(optr);
  }

  @Override
  @Visibility(ihm = false)
  public EMHModeleBase copyShallowFirstLevel() {
    return  copyPrimitiveIn(new EMHModeleBase());
  }
}
