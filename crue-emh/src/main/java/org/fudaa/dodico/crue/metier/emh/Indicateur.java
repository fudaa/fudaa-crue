package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

public class Indicateur {

  private double valeur = -1;
  private double max = -1;

  @UsedByComparison(ignoreInComparison = true)
  public double getValeur() {
    return valeur;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName()+": "+ getLine();
  }
  
  

  public boolean isValeurDefined() {
    return valeur >= 0;
  }

  public boolean isMaxDefined() {
    return max >= 0;
  }

  public void setValeur(double valeur) {
    this.valeur = valeur;
  }

  @UsedByComparison(ignoreInComparison = true)
  public double getMax() {
    return max;
  }

  public void setMax(double max) {
    this.max = max;
  }
  String line;

  @UsedByComparison
  public String getLine() {
    return line;
  }

  public void setLine(String line) {
    this.line = line;
  }
}