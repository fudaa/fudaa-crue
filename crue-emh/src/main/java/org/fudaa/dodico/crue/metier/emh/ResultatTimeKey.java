package org.fudaa.dodico.crue.metier.emh;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.joda.time.Duration;

/**
 *
 * @author deniger
 */
@XStreamAlias("ResultatTime")
public class ResultatTimeKey implements Comparable<ResultatTimeKey> {

  public static String timeToString(ResultatTimeKey time) {
    if (time.isPermanent()) {
      return time.getNomCalcul();
    }
    return time.getNomCalcul() + BusinessMessages.ENTITY_SEPARATOR + time.getTemps();
  }

  private final String nomCalcul;
  /**
   * Pour les calcul Permanents sera égal à 0
   */
  private final long duree;
  private final boolean useDay;
  private final String temps;
  private final boolean transitoire;
  public static final ResultatTimeKey NULL_VALUE = new ResultatTimeKey("<NULL>");

  public ResultatTimeKey(String nomCalcul) {
    this.nomCalcul = nomCalcul;
    this.duree = -1;
    temps = StringUtils.EMPTY;
    transitoire = false;
    useDay = false;
  }

  public ResultatTimeKey(String nomCalcul, long duree, boolean useDay) {
    this.nomCalcul = nomCalcul;
    this.duree = duree;
    temps = DateDurationConverter.durationToDDHHMMSS(new Duration(duree), useDay);
    transitoire = true;
    this.useDay = useDay;
  }

  public boolean isUseDay() {
    return useDay;
  }

  public boolean isTransitoire() {
    return transitoire;
  }

  public boolean isPermanent() {
    return !transitoire;
  }

  public String getTemps() {
    return temps;
  }
  String toString;

  @Override
  public String toString() {
    if (toString == null) {
      if (duree < 0) {
        toString = "NomCalcul= " + nomCalcul;
      }
      toString = "NomCalcul= " + nomCalcul + "; TempsSimu= " + temps;
    }
    return toString;
  }

  @Override
  public int compareTo(ResultatTimeKey o) {
    if (o == null) {
      return 1;
    }
    if (o == this) {
      return 0;
    }
    int compare = nomCalcul.compareTo(o.nomCalcul);
    if (compare == 0) {
      if (duree > o.duree) {
        compare = 1;
      } else if (duree < o.duree) {
        compare = -1;
      }
    }
    return compare;
  }

  @UsedByComparison(ignoreInComparison = true)
  public long getDuree() {
    return duree;
  }

  public String getNomCalcul() {
    return nomCalcul;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ResultatTimeKey other = (ResultatTimeKey) obj;
    if ((this.nomCalcul == null) ? (other.nomCalcul != null) : !this.nomCalcul.equals(other.nomCalcul)) {
      return false;
    }
    if (this.duree != other.duree) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    return (int) (duree ^ (duree >>> 32)) + nomCalcul.hashCode() * 13;
  }
}
