/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.crue.metier.etude;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;
import org.fudaa.dodico.crue.metier.CrueLevelType;

/**
 * @author deniger
 */
public abstract class ManagerEMHContainer<K extends ObjetWithID> extends ManagerEMHContainerBase {

  private final Map<String, K> idSousModele = new HashMap<>();

  private final List<K> listFils = new ArrayList<>();
  private final List<K> protectedFils = Collections.unmodifiableList(listFils);
  

  protected ManagerEMHContainer(CrueLevelType level) {
    super(level);
  }

  protected ManagerEMHContainer(CrueLevelType level, final String nom) {
    super(level, nom);
  }

  public abstract Map<String, FichierCrueParModele> addAllFileUsed(Map<String, FichierCrueParModele> map);

  public void addManagerFils(final K fils) {
    listFils.add(fils);
    idSousModele.put(fils.getId(), fils);
  }

  public void removeManagerFils(final K fils) {
    listFils.remove(fils);
    idSousModele.remove(fils.getId());
  }
  
  public void removeManagerFils(final int index) {
    K fils = listFils.remove(index);
    idSousModele.remove(fils.getId());
  }
  
  public void removeAllManagerFils() {
    listFils.clear();
    idSousModele.clear();
  }
  
  public Map<String, FichierCrueParModele> getAllFileUsed() {
    return addAllFileUsed(new HashMap<>());
  }

  /**
   * @return the protectedFils liste non modifiable
   */
  public List<K> getFils() {
    return protectedFils;
  }

  public K getManagerFils(final String id) {
    return idSousModele.get(id);
  }

  

}
