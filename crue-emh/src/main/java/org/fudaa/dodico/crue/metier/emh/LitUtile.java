/**
 * *********************************************************************
 * Module: LitUtile.java Author: deniger Purpose: Defines the Class LitUtile *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;

public class LitUtile implements ToStringTransformable, Cloneable {

  private PtProfil limDeb;
  private PtProfil limFin;

  /**
   * @pdGenerated default parent getter
   */
  public PtProfil getLimDeb() {
    return limDeb;
  }

  @Override
  public LitUtile clone() {
    try {
      LitUtile res = (LitUtile) super.clone();
      res.limDeb = limDeb == null ? null : limDeb.clone();
      res.limFin = limFin == null ? null : limFin.clone();
      return res;
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why ?");
  }

  @Override
  public String toString() {
    return "LitUtile (" + limDeb + ", " + limFin + ")";
  }

  /**
   * @param props
   * @return
   */
  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    if (EnumToString.OVERVIEW.equals(format)) {
      return BusinessMessages.getString("metier.litUtile.title");
    }
    return BusinessMessages.getString("metier.litUtile.string", PtProfil.toString(limDeb, props, type), PtProfil.toString(
            limFin, props, type));
  }

  /**
   * @pdGenerated default parent setter
   * @param newPtProfil
   */
  public void setLimDeb(final PtProfil newPtProfil) {
    this.limDeb = newPtProfil;
  }

  /**
   * @pdGenerated default parent getter
   */
  public PtProfil getLimFin() {
    return limFin;
  }

  /**
   * @pdGenerated default parent setter
   * @param newPtProfil
   */
  public void setLimFin(final PtProfil newPtProfil) {
    this.limFin = newPtProfil;
  }
}