package org.fudaa.dodico.crue.metier.etude;

import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.CrueLevelType;

import java.io.File;
import java.util.Collections;
import java.util.List;

/**
 * @author deniger
 */
public abstract class ManagerEMHContainerBase implements ObjetNomme {
  /**
   * Infos sur la version.
   */
  private EMHInfosVersion infosVersions;
  private boolean isActive = true;
  /**
   * Liste des fichiers rattaches au scenario . Ces fichiers proviennent de la classe EMHProjetInfos qui contient la base des fichiers du projet. Ces
   * fichiers sont uniquement des fichiers resultat ?
   */
  private FichierCrueManager listeFichiers;
  private String nom;
  private final CrueLevelType level;

  protected ManagerEMHContainerBase(CrueLevelType level) {
    this(level, null);
  }

  protected ManagerEMHContainerBase(CrueLevelType level, String nom) {
    super();
    this.nom = nom;
    this.level = level;
  }

  public abstract String getEMHType();

  public boolean isCrue9() {
    return CrueVersionType.CRUE9.equals(getInfosVersions().getCrueVersion());
  }

  public boolean isCrue10() {
    return CrueVersionType.CRUE10.equals(getInfosVersions().getCrueVersion());
  }

  /**
   * @param fichier le fichier a ajouter
   * @return true si ajouté et false sinon
   */
  public final boolean addFichierDonnees(final FichierCrue fichier) {
    if (fichier == null) {
      return false;
    }
    if (listeFichiers == null) {
      listeFichiers = new FichierCrueManager(Collections.singletonList(fichier));
    } else {
      listeFichiers.addFile(fichier);
    }
    return true;
  }

  @Override
  public String getId() {
    return getNom() == null ? null : getNom().toUpperCase();
  }

  public CrueLevelType getLevel() {
    return this.level;
  }

  public EMHInfosVersion getInfosVersions() {
    return infosVersions;
  }

  /**
   * @return le manager des fichiers
   */
  public final FichierCrueManager getListeFichiers() {
    return listeFichiers;
  }

  /**
   * @param fileType le type recherche
   * @return le fichier crue correspondant si existe. Null sinon.
   */
  public final FichierCrue getFile(CrueFileType fileType) {
    if (listeFichiers == null) {
      return null;
    }
    return listeFichiers.getFile(fileType);
  }

  public final File getFinalFile(CrueFileType fileType, EMHProjet projet) {
    FichierCrue fichierCrue = getFile(fileType);
    if (fichierCrue == null) {
      return null;
    }
    return fichierCrue.getProjectFile(projet);
  }

  public List<FichierCrue> getFichiers() {
    if (listeFichiers == null) {
      return Collections.emptyList();
    }
    return listeFichiers.getFichiers();
  }

  @Override
  public final String getNom() {
    return nom;
  }

  /**
   * @return the isActive
   */
  public boolean isActive() {
    return isActive;
  }

  /**
   * @param isActive the isActive to set
   */
  public void setActive(boolean isActive) {
    this.isActive = isActive;
  }

  public void setInfosVersions(final EMHInfosVersion infosVersions) {
    this.infosVersions = infosVersions;
  }

  /**
   * @param listeFiles la liste des fichiers
   */
  public final void setListeFichiers(final List<FichierCrue> listeFiles) {
    this.listeFichiers = new FichierCrueManager(listeFiles);
  }

  @Override
  public final void setNom(final String nom) {
    this.nom = nom;
  }
}
