package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

public abstract class CatEMHCasier extends CatEMHActivable {

  @Override
  public EnumCatEMH getCatType() {
    return EnumCatEMH.CASIER;
  }

  public CatEMHCasier(String nom) {
    super(nom);
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public abstract EnumCasierType getCasierType();

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public Object getSubCatType() {
    return getCasierType();
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public EMHSousModele getParent() {
    return EMHHelper.getParent(this);
  }

  /**
   * @return le noeud associe
   */
  public CatEMHNoeud getNoeud() {
    return EMHHelper.getNoeudCasier(this);
  }

  /**
   * @param noeud le noeud associe
   */
  public void setNoeud(final CatEMHNoeud noeud) {
    EMHRelationFactory.addNoeudCasier(this, noeud);
  }

  @UsedByComparison
  public final ResPrtGeo getResPrtGeo() {
    return EMHHelper.selectFirstOfClass(getInfosEMH(), ResPrtGeo.class);
  }

  @UsedByComparison
  public final ResPrtCIniCasier getResPrtCIniCasier() {
    return EMHHelper.selectFirstOfClass(getInfosEMH(), ResPrtCIniCasier.class);
  }

  @Override
  public boolean getActuallyActive() {
    boolean res = super.getActuallyActive();
    if (res) {
      CatEMHNoeud noeud = getNoeud();
      return noeud != null && noeud.getActuallyActive();
    }
    return res;
  }
}
