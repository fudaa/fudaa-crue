/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.contrat.DoubleValuable;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

/**
 */
public enum EnumFormulePdc implements ToStringInternationalizable, DoubleValuable {

  BORDA(1), DIVERGENT(0);
  private final int id;

  EnumFormulePdc(final int id) {
    this.id = id;
  }

  @Override
  public double toDoubleValue() {
    return id;
  }


  @Override
  public String geti18n() {
    return BusinessMessages.getString("EnumFormulePdc." + name() + ".shortName");
  }

  @Override
  public String geti18nLongName() {
    return BusinessMessages.getString("EnumFormulePdc." + name() + ".longName");
  }
}
