package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;

import java.util.ArrayList;
import java.util.Collection;

/**
 * type 15 Crue9
 */
public class DonCalcSansPrtBrancheBarrageFilEau extends DonCalcSansPrtBrancheQ implements Cloneable {
  private LoiFF regimeManoeuvrant;
  private java.util.ArrayList<ElemBarrage> elemBarrage;

  public DonCalcSansPrtBrancheBarrageFilEau(final CrueConfigMetier defaults) {
    super(defaults);
  }

  public java.util.List<ElemBarrage> getElemBarrage() {
    if (elemBarrage == null) {
      elemBarrage = new ArrayList<>();
    }
    return elemBarrage;
  }

  @Override
  public DonCalcSansPrt cloneDCSP() {
    try {
      DonCalcSansPrtBrancheBarrageFilEau res = (DonCalcSansPrtBrancheBarrageFilEau) super.clone();
      if (elemBarrage != null) {
        res.elemBarrage = new ArrayList<>();
        elemBarrage.forEach(elemSeuil -> res.elemBarrage.add(elemSeuil.clone()));
      }
      if (regimeManoeuvrant != null) {
        res.regimeManoeuvrant = regimeManoeuvrant.clonedWithoutUser();
        res.regimeManoeuvrant.register(res);
      }
      return res;
    } catch (CloneNotSupportedException e) {
    }
    return null;
  }

  @Override
  public void fillWithLoi(final Collection<Loi> target) {
    target.add(regimeManoeuvrant);
  }

  public void setElemBarrage(final java.util.Collection<ElemBarrage> newElemSeuil) {
    removeAllElemBarrage();
    for (final java.util.Iterator iter = newElemSeuil.iterator(); iter.hasNext(); ) {
      addElemBarrage((ElemBarrage) iter.next());
    }
  }

  public void addElemBarrage(final ElemBarrage newElemSeuil) {
    if (newElemSeuil == null) {
      return;
    }
    if (this.elemBarrage == null) {
      this.elemBarrage = new java.util.ArrayList<>();
    }
    if (!this.elemBarrage.contains(newElemSeuil)) {
      this.elemBarrage.add(newElemSeuil);
    }
  }


  public void removeAllElemBarrage() {
    if (elemBarrage != null) {
      elemBarrage.clear();
    }
  }

  public LoiFF getRegimeManoeuvrant() {
    return regimeManoeuvrant;
  }

  public void setRegimeManoeuvrant(final LoiFF regimeManoeuvrant) {
    if (this.regimeManoeuvrant != null) {
      this.regimeManoeuvrant.unregister(this);
    }

    this.regimeManoeuvrant = regimeManoeuvrant;
    this.regimeManoeuvrant.register(this);
  }
}
