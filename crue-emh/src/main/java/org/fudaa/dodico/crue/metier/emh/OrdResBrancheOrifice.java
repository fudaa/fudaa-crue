/**
 * *********************************************************************
 * Module: OrdResScenarioBrancheOrifice.java Author: BATTISTA Purpose: Defines the Class OrdResScenarioBrancheOrifice
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.OrdResKey;

public class OrdResBrancheOrifice extends OrdRes implements Cloneable {
  // WARN: l'ordre des champs est important car utilise par l'ecriture/lecture de ORES

  @Override
  public OrdResBrancheOrifice clone() {
    try {
      return (OrdResBrancheOrifice) super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why this ?");
  }
  private final static OrdResKey KEY = new OrdResKey(EnumBrancheType.EMHBrancheOrifice);

  @Override
  public OrdResKey getKey() {
    return KEY;
  }

  @Override
  public String toString() {
    return "OrdResBrancheOrifice [ddeCoefCtr=" + getDdeValue("coefCtr") + ", ddeRegimeOrifice=" + getDdeValue("regimeOrifice") + "]";
  }
}