/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.metier.factory;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.ValParam;
import org.fudaa.dodico.crue.metier.emh.ValParamDouble;
import org.fudaa.dodico.crue.metier.emh.ValParamEntier;

/**
 *
 * @author Frédéric Deniger
 */
public class ValParamFactory {

  public static ValParam create(Class type, String nom, CrueConfigMetier ccm) {
    if (Double.TYPE.equals(type)) {
      String prop = StringUtils.uncapitalize(nom);
      return new ValParamDouble(nom, ccm.getDefaultValue(prop).doubleValue());
    } else if (Integer.TYPE.equals(type)) {
      String prop = StringUtils.uncapitalize(nom);
      return new ValParamEntier(nom, ccm.getDefaultValue(prop).intValue());
    }
    throw new IllegalAccessError("type " + type + " not supported");
  }
}
