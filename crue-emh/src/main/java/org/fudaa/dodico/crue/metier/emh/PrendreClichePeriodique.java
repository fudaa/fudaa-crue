package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;

/**
 *
 * @author deniger
 */
public class PrendreClichePeriodique implements ToStringTransformable, Cloneable {

  Pdt pdtRes;
  String nomFic;

  public PrendreClichePeriodique() {
  }

  public PrendreClichePeriodique(String nomFic, Pdt pdtRes) {
    this.pdtRes = pdtRes;
    this.nomFic = nomFic;
  }

  @Override
  protected PrendreClichePeriodique clone() {
    try {
      PrendreClichePeriodique res = (PrendreClichePeriodique) super.clone();
      if (pdtRes != null) {
        res.pdtRes = pdtRes.deepClone();
      }
      return res;
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why");
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + " " + nomFic + ": " + pdtRes;
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    return getClass().getSimpleName() + " " + nomFic + ": " + (pdtRes == null ? "null" : pdtRes.toString());
  }

  public String getNomFic() {
    return nomFic;
  }

  public void setNomFic(String nomFic) {
    this.nomFic = nomFic;
  }

  public Pdt getPdtRes() {
    return pdtRes;
  }

  public void setPdtRes(Pdt pdtRes) {
    this.pdtRes = pdtRes;
  }
}
