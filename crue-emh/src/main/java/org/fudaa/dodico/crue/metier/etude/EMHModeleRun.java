package org.fudaa.dodico.crue.metier.etude;

import java.util.List;

/**
 * Contient un modele et sa liste de fichier de résultats associés Contenu dans Run.
 * 
 * @author Adrien Hadoux
 */
public class EMHModeleRun {

  /**
   * modele associé au run et aux fichiers résultats.
   */
  private final ManagerEMHModeleBase modeleRun;

  /**
   * Liste des fichiers de résultats rattachés au modele du runrun . Ces fichiers sont uniquement des fichiers résultat.
   * 
   * @param id_
   */
  private final List<FichierCrue> listeFichiersResultatsAssocies_;

  public EMHModeleRun(final ManagerEMHModeleBase modeleRun, final List<FichierCrue> listeFichiersResultatsAssocies) {
    super();
    this.modeleRun = modeleRun;
    this.listeFichiersResultatsAssocies_ = listeFichiersResultatsAssocies;
  }

  public ManagerEMHModeleBase getModeleRun() {
    return modeleRun;
  }

  public List<FichierCrue> getListeFichiers() {
    return listeFichiersResultatsAssocies_;
  }

}
