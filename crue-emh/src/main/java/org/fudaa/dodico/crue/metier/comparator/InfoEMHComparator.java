package org.fudaa.dodico.crue.metier.comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.fudaa.dodico.crue.metier.emh.AbstractInfosEMH;

/**
 * @author deniger
 */
public class InfoEMHComparator implements Comparator<AbstractInfosEMH> {

  /**
   * singleton
   */
  public static final InfoEMHComparator INSTANCE = new InfoEMHComparator();

  /**
   * 
   * @param list
   * @return  true si modification effectuée dans la liste.
   */
  public static boolean sortList(final List<? extends AbstractInfosEMH> list) {
    if (list != null) {
      List before = new ArrayList(list);
      Collections.sort(list, INSTANCE);
      return !before.equals(list);
    }
    return false;
  }

  private InfoEMHComparator() {
  }

  @Override
  public int compare(final AbstractInfosEMH o1, final AbstractInfosEMH o2) {
    if (o1 == o2) {
      return 0;
    }
    if (o1 == null) {
      return -1;
    }
    if (o2 == null) {
      return 1;
    }
    int type = RegleTypeComparator.compareString(o1.getType(), o2.getType());
    if (type == 0) {
      type = ObjetNommeByIdComparator.INSTANCE.compare(o1.getEmh(), o2.getEmh());
    }
    return type;
  }
}
