package org.fudaa.dodico.crue.metier.result;

/**
 * @author deniger
 */
public class ResCalBrancheStrickler extends ResCalcul {

  public void setSplan(double sPlan) {
    super.addResultat("splan", sPlan);
  }

  public void setVol(double vol) {
    super.addResultat("vol", vol);
  }
}
