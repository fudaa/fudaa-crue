package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;

/**
 */
public class PtProfil implements Point2D, ToStringTransformable, Cloneable {

  public PtProfil() {
  }

  public PtProfil(Point2D pt) {
    if (pt != null) {
      xt = pt.getAbscisse();
      z = pt.getOrdonnee();
    }
  }

  /**
   * @param pt
   * @param props
   * @return pt transforme en string.
   */
  public static String toString(PtProfil pt, CrueConfigMetier props, DecimalFormatEpsilonEnum type) {
    return pt == null ? "null" : pt.toString(props, EnumToString.COMPLETE, type);
  }

  @Override
  public PtProfil clone() {
    try {
      return ((PtProfil) super.clone());
    } catch (CloneNotSupportedException cloneNotSupportedException) {
      //why ?
    }
    throw new IllegalAccessError("why");
  }
  private double xt;
  private double z;

  /**
   * @param newYp
   * @param newZ
   */
  public PtProfil(double newYp, double newZ) {
    this.xt = newYp;
    this.z = newZ;
  }

  @Override
  public double getAbscisse() {
    return getXt();
  }

  @Override
  public double getOrdonnee() {
    return getZ();
  }

  public final double getXt() {
    return xt;
  }

  public final double getZ() {
    return z;
  }

  /**
   * @param newYp
   */
  public final void setXt(double newYp) {
    xt = newYp;
  }

  /**
   * @param newZ
   */
  public final void setZ(double newZ) {
    z = newZ;
  }

  @Override
  public String toString() {
    return xt + "; " + z;
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    return "(" + TransformerEMHHelper.formatFromPropertyName("xt", xt, props, DecimalFormatEpsilonEnum.COMPARISON) + "; " + TransformerEMHHelper.formatFromPropertyName("z", z, props, DecimalFormatEpsilonEnum.COMPARISON) + ")";
  }
}