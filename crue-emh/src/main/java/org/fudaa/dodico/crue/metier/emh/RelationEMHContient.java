/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.crue.metier.emh;

/**
 * @author deniger
 */
public class RelationEMHContient extends RelationEMH {

  public RelationEMHContient() {
  }

  public RelationEMHContient(EMH emh) {
    super.setEmh(emh);
  }

  @Override
  public RelationEMHContient copyWithEMH(EMH copiedEMH) {
    return new RelationEMHContient(copiedEMH);
  }
}
