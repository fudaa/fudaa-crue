package org.fudaa.dodico.crue.metier.emh;

import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;

/**
 *
 * @author deniger
 */
public class ResultatPasDeTempsParCalcul implements ToStringTransformable {

  private final List<ResultatTimeKey> resultats;
  private final String nomCalcul;

  public ResultatPasDeTempsParCalcul(List<ResultatTimeKey> resultats, String nomCalcul) {
    this.resultats = Collections.unmodifiableList(resultats);
    this.nomCalcul = nomCalcul;
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    if (EnumToString.OVERVIEW.equals(format)) {
      return nomCalcul;
    }
    return BusinessMessages.getString("resultatPasDeTempsParCalcul.shortName", nomCalcul);
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + " " + nomCalcul;
  }

  @UsedByComparison
  public String getNomCalcul() {
    return nomCalcul;
  }

  @UsedByComparison
  public List<ResultatTimeKey> getResultats() {
    return resultats;
  }
}
