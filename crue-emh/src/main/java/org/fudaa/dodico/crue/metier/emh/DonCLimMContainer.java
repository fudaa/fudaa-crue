package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.comparator.InfoEMHByUserPositionComparator;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

import java.util.*;

public class DonCLimMContainer {


  protected static class Line<T extends DonCLimMCommonItem> {
    private List<T> clims = new ArrayList<>();
    private List<T> climsUnmodifiable = Collections.unmodifiableList(clims);

    public List<T> getClimsUnmodifiable() {
      return climsUnmodifiable;
    }

    public boolean sort(final InfoEMHByUserPositionComparator comparator) {
      return InfoEMHByUserPositionComparator.sortList(clims, comparator);
    }

    public List<T> getDonCLimMUserActive() {
      return EMHHelper.<T>selectDonCLimMCommonUserActivated(clims);
    }

    public void addDclm(final T dclm) {
      if (!clims.contains(dclm)) {
        clims.add(dclm);
      }
    }

    public void removeDclm(final DonCLimM dclm) {
      clims.remove(dclm);
    }

    public Line<T> deepClone() {
      Line<T> res = new Line<>();
      clims.forEach(clim -> res.addDclm((T) clim.deepClone()));
      return res;
    }

    public void setCalculParent(Calc calc) {
      clims.forEach(clim -> clim.setCalculParent(calc));
    }
  }

  LinkedHashMap<Class, Line> lines = new LinkedHashMap<>();

  public DonCLimMContainer(List<Class> dclmClass) {
    dclmClass.forEach(line -> lines.put(line, new Line()));
  }

  private DonCLimMContainer(DonCLimMContainer donCLimMContainer) {
    donCLimMContainer.lines.forEach((aClass, line) -> lines.put(aClass, line.deepClone()));
  }


  @Override
  protected DonCLimMContainer clone() throws CloneNotSupportedException {
    final DonCLimMContainer res = (DonCLimMContainer) super.clone();
    return res;
  }


  public <T extends DonCLimMCommonItem> List<T> getDclm(Class<T> dclmClass) {
    return lines.get(dclmClass).getClimsUnmodifiable();
  }

  public <T extends DonCLimMCommonItem> List<T> getDclmUserActive(Class<T> dclmClass) {
    Line line = getLine(dclmClass);
    if (line == null) return null;
    return line.getDonCLimMUserActive();
  }
  public int getDclmUserActiveCount(Class dclmClass) {
    Line line = getLine(dclmClass);
    if (line == null) return 0;
    return line.getDonCLimMUserActive().size();
  }

  private Line getLine(Class dclmClass) {
    return lines.get(dclmClass);
  }

  public void addDclm(DonCLimMCommonItem dclm) {
    if (dclm != null) {
      getLine(dclm.getClass()).addDclm(dclm);
    }
  }

  public List<DonCLimM> getAllDclm() {
    ArrayList<DonCLimM> res = new ArrayList<>();
    lines.values().forEach(line -> res.addAll(line.clims));
    return res;
  }


  public <T extends DonCLimM> List<T> getAllDCLMUserActive() {
    ArrayList<T> res = new ArrayList<>();
    lines.values().forEach(line -> res.addAll(line.getDonCLimMUserActive()));
    return res;
  }

  protected DonCLimMContainer deepClone() {
    return new DonCLimMContainer(this);
  }

  protected void setCalculParent(Calc calc) {
    lines.values().forEach(line -> line.setCalculParent(calc));
  }

  public void removeDclm(DonCLimM dclm) {
    getLine(dclm.getClass()).removeDclm(dclm);
  }

  public boolean sort(InfoEMHByUserPositionComparator comparator) {
    boolean sorted = false;
    for (Line line : lines.values()) {
      if (line.sort(comparator)) {
        sorted = true;
      }
    }
    return sorted;
  }

  public void remove(EMH emh) {
    lines.values().forEach(line -> {
      Calc.remove(emh, line.clims);
    });
  }
}
