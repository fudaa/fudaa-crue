package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 * Branche 4: seuil sans prise en compte de la charge dynamique
 * 
 * @author Adrien Hadoux
 */
public class EMHBrancheSeuilLateral extends CatEMHBranche {

  public EMHBrancheSeuilLateral(final String nom) {
    super(nom);
  }
  
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public EnumBrancheType getBrancheType() {
    return EnumBrancheType.EMHBrancheSeuilLateral;
  }

  @Override
  @Visibility(ihm = false)
  public EMHBrancheSeuilLateral copyShallowFirstLevel() {
    return  copyPrimitiveIn(new EMHBrancheSeuilLateral(getNom()));
  }

}
