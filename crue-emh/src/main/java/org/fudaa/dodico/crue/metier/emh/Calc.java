package org.fudaa.dodico.crue.metier.emh;

import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.comparator.InfoEMHByUserPositionComparator;

/**
 * Classe abstraite pour les options de calcul
 */
public abstract class Calc implements ObjetNomme, ToStringInternationalizable, Cloneable {

  Long uid;
  private String nom;
  private String commentaire = StringUtils.EMPTY;

  protected DonCLimMContainer container;

  protected Calc(List<Class> dclmClassInOrder) {
    container = new DonCLimMContainer(dclmClassInOrder);
  }

  @UsedByComparison
  public final List<DonCLimM> getlisteDCLM() {
    return container.getAllDclm();
  }

  public final List<DonCLimM> getlisteDCLMUserActive() {
    return container.getAllDCLMUserActive();
  }

  public final void remove(final EMH emh) {
    container.remove(emh);
  }

  public final void addDclm(final DonCLimM dclm) {
    container.addDclm((DonCLimMCommonItem) dclm);
  }

  public final void removeDclm(final DonCLimM dclm) {
    container.removeDclm(dclm);
  }

  @Override
  protected Object clone() throws CloneNotSupportedException {
    final Calc res = (Calc) super.clone();
    return res;
  }

  public final boolean sort(final InfoEMHByUserPositionComparator comparator) {
    return container.sort(comparator);
  }

  public Calc cloneWithNameComment() {
    try {
      final Calc newInstance = getClass().newInstance();
      newInstance.setNom(nom);
      newInstance.commentaire = commentaire;
      return newInstance;
    } catch (final Exception instantiationException) {
      Logger.getLogger(Calc.class.getName()).log(Level.INFO, "can t clone calcul", instantiationException);
    }
    return null;
  }


  public final <T extends DonCLimMCommonItem> List<T> getDclm(Class<T> dclmClass) {
    return container.getDclm(dclmClass);
  }

  public final <T extends DonCLimMCommonItem> List<T> getDclmUserActive(Class<T> dclmClass) {
    return container.getDclmUserActive(dclmClass);
  }


  public int getDclmUserActiveCount(Class dclmClass) {
    return container.getDclmUserActiveCount(dclmClass);
  }


  /**
   * ne clone pas les emh...
   */
  public abstract Calc deepClone();

  public static void remove(final EMH emh, final List<? extends DonCLimM> listeDCLM) {
    listeDCLM.removeIf(donCLimM -> donCLimM.getEmh().getUiId().equals(emh.getUiId()));
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public Long getUiId() {
    return uid;
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public boolean isUiIdSet() {
    return uid != null;
  }

  public void setUiId(final Long uiId) {
    this.uid = uiId;
  }

  public abstract boolean isTransitoire();

  public boolean isPermanent() {
    return !isTransitoire();
  }

  /**
   * @param newCommentaire
   */
  public void setCommentaire(final String newCommentaire) {
    commentaire = StringUtils.defaultString(newCommentaire);
  }

  @UsedByComparison
  public final String getType() {
    return getClass().getSimpleName();
  }

  @Override
  public String geti18n() {
    return BusinessMessages.getStringOrDefault(getType() + ".shortName", getType());
  }

  @Override
  public String geti18nLongName() {
    return geti18n();
  }

  @PropertyDesc(i18n = "Comment.property")
  public String getCommentaire() {
    return commentaire;
  }

  private String id;

  @UsedByComparison
  @Override
  public String getId() {
    return id;
  }

  /**
   * @param newNom le nouveau nom
   */
  @Override
  public void setNom(final String newNom) {
    nom = newNom;
    if (nom != null) {
      id = nom.toUpperCase();
    }
  }

  @Override
  public String getNom() {
    return nom;
  }
}
