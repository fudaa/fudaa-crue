package org.fudaa.dodico.crue.metier.emh;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;

/**
 * CL de type 1 en Crue9
 *
 */
public class CalcTransNoeudNiveauContinuLimnigramme extends DonCLimMCommonItem implements CalcTransItem {

  /**
   * Loi Limnigramme : VarAbscisse = t VarOrdonnee = Z
   *
   */
  private LoiDF limnigramme;

  /**
   * Loi Limnigramme : VarAbscisse = t VarOrdonnee = Z
   *
   */
  @PropertyDesc(i18n = "limnigramme.property")
  public LoiDF getLimnigramme() {
    return limnigramme;
  }

  @Override
  public String getShortName() {
    return BusinessMessages.getString("limnigramme.property");
  }

  @Override
  public EnumTypeLoi getTypeLoi() {
    return EnumTypeLoi.LoiTZimp;
  }

  /**
   * Loi Limnigramme : VarAbscisse = t VarOrdonnee = Z
   */
  public void setLimnigramme(final LoiDF newLimnigramme) {
    if (this.limnigramme != null) {
      this.limnigramme.unregister(this);
    }

    limnigramme = newLimnigramme;
    if (this.limnigramme != null) {
      this.limnigramme.register(this);
    }
  }

  @Visibility(ihm = false)
  @Override
  public Loi getLoi() {
    return getLimnigramme();
  }

  @Override
  public void setLoi(final Loi newLoi) {
    setLimnigramme((LoiDF) newLoi);
  }

  CalcTransNoeudNiveauContinuLimnigramme deepClone() {
    try {
      return (CalcTransNoeudNiveauContinuLimnigramme) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransNoeudNiveauContinuTarage.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }
}
