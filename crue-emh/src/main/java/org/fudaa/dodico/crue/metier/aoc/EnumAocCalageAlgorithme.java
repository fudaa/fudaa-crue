package org.fudaa.dodico.crue.metier.aoc;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

/**
 * Created by deniger on 28/06/2017.
 */
public enum EnumAocCalageAlgorithme implements ToStringInternationalizable {

  RECUIT("Recuit", "RS"), MONTE_CARLO("MonteCarlo", "MC");
  private final String identifiant;
  private final String identifiantShort;

  EnumAocCalageAlgorithme(String identifiant, String identifiantShort) {
    this.identifiant = identifiant;
    this.identifiantShort = identifiantShort;
  }

  public String getIdentifiantShort() {
    return identifiantShort;
  }

  public String getIdentifiant() {
    return identifiant;
  }

  @Override
  public String geti18n() {
    return BusinessMessages.getString("aoc.calageAlgorithme." + name().toLowerCase() + ".shortName");
  }

  @Override
  public String geti18nLongName() {
    return BusinessMessages.getString("aoc.calageAlgorithme." + name().toLowerCase() + ".longName");
  }
}
