/**
 * *********************************************************************
 * Module: OrdPrtCIniModeleBase.java Author: deniger Purpose: Defines the Class OrdPrtCIniModeleBase
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByIdComparator;
import org.fudaa.dodico.crue.metier.factory.ValParamFactory;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;

public class OrdPrtCIniModeleBase extends AbstractInfosEMH implements Sortable, ToStringTransformable {

  public static final String PROP_METHOD_INTERPOLATION = "methodeInterpol";
  /**
   * attention, modifier la constante PROP_METHOD_INTERPOLATION si cette propriété change.
   */
  private EnumMethodeInterpol methodeInterpol;
  private Sorties sorties = new Sorties();
  private java.util.List<ValParam> valParam;
  private java.util.List<Regle> regles;
  private java.util.List<Regle> externRegle;
  private final Map<EnumRegle, Regle> typeRegle = new EnumMap<>(EnumRegle.class);

  public OrdPrtCIniModeleBase(CrueConfigMetier defaults) {
    List<Regle> defaultValues = DefaultValues.initDefaultValuesOPTI(defaults);
    for (Regle regle : defaultValues) {
      addRegle(regle);
    }
  }

  public OrdPrtCIniModeleBase(OrdPrtCIniModeleBase toCopy) {
    for (Regle regle : toCopy.regles) {
      addRegle(regle.deepClone());
    }
    valParam = new ArrayList<>();
    if (toCopy.valParam != null) {
      for (ValParam val : toCopy.valParam) {
        valParam.add(val.deepClone());
      }
    }
    methodeInterpol = toCopy.methodeInterpol;
    sorties = new Sorties(toCopy.sorties);
  }

  public OrdPrtCIniModeleBase deepClone() {
    return new OrdPrtCIniModeleBase(this);
  }

  public Regle getRegle(EnumRegle regle) {
    return typeRegle.get(regle);
  }

  public java.util.Collection<ValParam> getValParam() {
    if (valParam == null) {
      valParam = new java.util.ArrayList<>();
    }
    return valParam;
  }

  /**
   * A voir s'il faut optimiser ce point.
   *
   * @param nom
   * @return
   */
  public ValParam getValParam(String nom) {
    if (valParam == null) {
      return null;
    }
    for (ValParam val : valParam) {
      if (nom.equals(val.getNom())) {
        return val;
      }
    }
    return null;

  }

  private void addRegle(Regle newRegle) {
    if (newRegle == null) {
      return;
    }
    if (this.regles == null) {
      this.regles = new java.util.ArrayList<>();
      externRegle = Collections.unmodifiableList(regles);
    }

    if (!this.regles.contains(newRegle)) {
      // on enleve la regle de meme type si presente
      Regle sameType = typeRegle.get(newRegle.getType());
      if (sameType != null) {
        regles.remove(sameType);
      }
      this.regles.add(newRegle);
      typeRegle.put(newRegle.getType(), newRegle);
    }
  }

  public java.util.List<Regle> getRegles() {
    if (regles == null) {
      regles = new java.util.ArrayList<>();
      externRegle = Collections.unmodifiableList(regles);
    }
    return externRegle;
  }

  @Override
  public boolean sort() {
    if (valParam != null) {
      List old = new ArrayList(valParam);
      Collections.sort(valParam, ObjetNommeByIdComparator.INSTANCE);
      return !old.equals(valParam);
    }
    return false;
  }

  public void setValParam(final java.util.Collection<ValParam> newValParam) {
    removeAllValParam();
    for (final java.util.Iterator iter = newValParam.iterator(); iter.hasNext();) {
      addValParam((ValParam) iter.next());
    }
  }

  public void addValParam(final ValParam newValParam) {
    if (newValParam == null) {
      return;
    }
    if (this.valParam == null) {
      this.valParam = new java.util.ArrayList<>();
    }
    if (!this.valParam.contains(newValParam)) {
      this.valParam.add(newValParam);
    }
  }

  public void removeValParam(final ValParam oldValParam) {
    if (oldValParam == null) {
      return;
    }
    if (this.valParam != null) {
      if (this.valParam.contains(oldValParam)) {
        this.valParam.remove(oldValParam);
      }
    }
  }

  public void removeAllValParam() {
    if (valParam != null) {
      valParam.clear();
    }
  }

  @PropertyDesc(i18n = "methodInterpolation.property")
  public EnumMethodeInterpol getMethodeInterpol() {
    return methodeInterpol;
  }

  public void setMethodeInterpol(final EnumMethodeInterpol newMethodeInterpol) {
    methodeInterpol = newMethodeInterpol;
  }

  @Override
  public final EnumInfosEMH getCatType() {
    return EnumInfosEMH.ORD_PRT_CINI_MODELE_BASE;
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    if (EnumToString.OVERVIEW.equals(format)) {
      return getClass().getSimpleName();
    }
    return getClass().getSimpleName() + " " + getEmh().getNom();
  }

  /**
   * @return the sorties
   */
  public Sorties getSorties() {
    return sorties;
  }

  /**
   * @param sorties the sorties to set
   */
  public void setSorties(Sorties sorties) {
    this.sorties = sorties;
  }

  public Collection<ValParam> setValParams(List<EnumRegleInterpolation> requiredValParam, CrueConfigMetier ccm) {
    List<ValParam> res = new ArrayList<>();
    if (CollectionUtils.isEmpty(requiredValParam)) {
      removeAllValParam();
      return res;
    }
    Set<String> names = new HashSet<>();
    for (EnumRegleInterpolation regleRequired : requiredValParam) {
      names.add(regleRequired.getVariableName());
    }
    Set<String> existing = new HashSet<>();
    for (Iterator<ValParam> it = valParam.iterator(); it.hasNext();) {
      ValParam val = it.next();
      if (!names.contains(val.getNom())) {
        it.remove();
      } else {
        existing.add(val.getNom());
      }
    }
    for (EnumRegleInterpolation regleRequired : requiredValParam) {
      if (!existing.contains(regleRequired.getVariableName())) {
        ValParam newParam = ValParamFactory.create(regleRequired.getVariableType(), regleRequired.getVariableName(), ccm);
        valParam.add(newParam);
      }
    }

    return getValParam();

  }
}
