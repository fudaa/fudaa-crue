/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.etude;

import org.fudaa.dodico.crue.metier.CrueFileType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;

/**
 * Un manager de fichierCrue permettant de retrouver facilement les fichiers selon leur type. Il est supposé que tous
 * les fichier ont un type différent
 *
 * @author deniger
 */
public class FichierCrueManager {
  private final List<FichierCrue> fichiers;
  //attention de ne pas stocker des id car ils peuvent changer
  private final EnumMap<CrueFileType, FichierCrue> idFichier;
  private List<FichierCrue> publicFichiers;

  /**
   * @param fichiers la liste des fichiers gérés par ce manager
   */
  public FichierCrueManager(final List<FichierCrue> fichiers) {
    super();
    this.fichiers = new ArrayList<>(fichiers);
    idFichier = new EnumMap<>(CrueFileType.class);
    for (final FichierCrue fichierCrue : fichiers) {
      idFichier.put(fichierCrue.getType(), fichierCrue);
    }
  }

  public void addFile(final FichierCrue crue) {
    if (idFichier.containsKey(crue.getType())) {
      fichiers.remove(idFichier.get(crue.getType()));
    }
    fichiers.add(crue);
    idFichier.put(crue.getType(), crue);
  }

  public boolean contains(final CrueFileType crueFileType) {
    return idFichier != null && idFichier.containsKey(crueFileType);
  }

  /**
   * supprime le fichier du meme type
   *
   * @param fichierCrue le fichier crue a enlever
   */
  public void removeFile(final FichierCrue fichierCrue) {
    if (fichierCrue == null) {
      return;
    }
    final FichierCrue present = idFichier.get(fichierCrue.getType());
    if (present != null && present.getNom().equals(fichierCrue.getNom())) {
      idFichier.remove(present.getType());
      fichiers.remove(present);
    }
  }

  /**
   * @return the fichiers
   */
  public List<FichierCrue> getFichiers() {
    if (publicFichiers == null) {
      publicFichiers = Collections.unmodifiableList(fichiers);
    }
    return publicFichiers;
  }

  /**
   * @param type le type demande
   * @return le fichier correspondant
   */
  public FichierCrue getFile(final CrueFileType type) {
    return idFichier.get(type);
  }
}
