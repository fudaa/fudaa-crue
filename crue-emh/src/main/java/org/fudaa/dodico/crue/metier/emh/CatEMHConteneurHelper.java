package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.comparator.CasierPositionComparator;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

import java.util.*;

/**
 * @author deniger
 */
public class CatEMHConteneurHelper {
  public static List<CatEMHSection> getSections(final EMHSousModele sousModele) {
    final List<CatEMHSection> res = new ArrayList<>();
    final List<CatEMHBranche> branches = getBranches(sousModele);
    for (final CatEMHBranche branche : branches) {
      final List<RelationEMHSectionDansBranche> sections = branche.getListeSections();
      for (final RelationEMHSectionDansBranche relationEMHSectionDansBranche : sections) {
        final CatEMHSection section = relationEMHSectionDansBranche.getEmh();
        res.add(section);
      }
    }
    final List<CatEMHSection> sections = EMHHelper.collectEMHInRelationEMHContient(sousModele, EnumCatEMH.SECTION);
    final List<CatEMHSection> sectionsWithoutBranches = new ArrayList<>();
    for (final CatEMHSection section : sections) {
      if (section.getBranche() == null) {
        sectionsWithoutBranches.add(section);
      }
    }
    Collections.sort(sectionsWithoutBranches, ObjetNommeByNameComparator.INSTANCE);
    res.addAll(sectionsWithoutBranches);
    return res;
  }

  public static List<CatEMHSection> getSections(final List<EMHSousModele> sousModeles) {
    final List<CatEMHSection> res = new ArrayList<>();
    for (final EMHSousModele sousModele : sousModeles) {
      res.addAll(getSections(sousModele));
    }
    return res;
  }

  public static List<CatEMHNoeud> getNoeuds(final EMHSousModele sousModele) {
    return EMHHelper.collectEMHInRelationEMHContient(sousModele, EnumCatEMH.NOEUD);
  }

  /**
   * Attention, il peut y avoir plusieurs fois les meme noeud
   *
   * @param sousModele
   */
  public static List<CatEMHNoeud> getNoeuds(final List<EMHSousModele> sousModeles) {
    final List<CatEMHNoeud> res = new ArrayList<>();
    final Set<CatEMHNoeud> added = new HashSet<>();
    for (final EMHSousModele sousModele : sousModeles) {
      final List<CatEMHNoeud> noeuds = getNoeuds(sousModele);
      for (final CatEMHNoeud noeud : noeuds) {
        if (!added.contains(noeud)) {
          added.add(noeud);
          res.add(noeud);
        }
      }
    }
    return res;
  }

  public static List<CatEMHBranche> getBranches(final EMHSousModele sousModele) {
    return EMHHelper.collectEMHInRelationEMHContient(sousModele, EnumCatEMH.BRANCHE);
  }

  public static List<CatEMHBranche> getBranches(final List<EMHSousModele> sousModeles) {
    final List<CatEMHBranche> res = new ArrayList<>();
    for (final EMHSousModele sousModele : sousModeles) {
      res.addAll(getBranches(sousModele));
    }
    return res;
  }

  public static List<EMHBrancheSaintVenant> getBranchesSaintVenant(final EMHSousModele sousModele) {
    final List<EMH> inRelationEMHContient = EMHHelper.collectEMHInRelationEMHContient(sousModele, EnumCatEMH.BRANCHE);
    return EMHHelper.selectClass(inRelationEMHContient, EMHBrancheSaintVenant.class);
  }

  public static List<EMHBrancheSaintVenant> getBranchesSaintVenant(final List<EMHSousModele> sousModeles) {
    final List<EMHBrancheSaintVenant> res = new ArrayList<>();
    for (final EMHSousModele sousModele : sousModeles) {
      res.addAll(getBranchesSaintVenant(sousModele));
    }
    return res;
  }

  public static List<CatEMHBranche> getBranchesAvecSectionPilote(final EMHSousModele sousModele) {
    final List<EMH> inRelationEMHContient = EMHHelper.collectEMHInRelationEMHContient(sousModele, EnumCatEMH.BRANCHE);
    return EMHHelper.selectInClasses(inRelationEMHContient, EMHBrancheBarrageFilEau.class, EMHBrancheBarrageGenerique.class);
  }

  public static List<CatEMHBranche> getBranchesAvecSectionPilote(final List<EMHSousModele> sousModeles) {
    final List<CatEMHBranche> res = new ArrayList<>();
    for (final EMHSousModele sousModele : sousModeles) {
      res.addAll(getBranchesAvecSectionPilote(sousModele));
    }
    return res;
  }

  /**
   * @param sousModele sous-modele
   * @return la liste des casiers dans l'ordre réseau (casier ordonné selon les noeuds)
   */
  public static List<CatEMHCasier> getCasiers(final EMHSousModele sousModele) {
    final List<CatEMHCasier> res = EMHHelper.collectEMHInRelationEMHContient(sousModele, EnumCatEMH.CASIER);
    final CasierPositionComparator comparator = new CasierPositionComparator(sousModele);
    Collections.sort(res, comparator);
    return res;
  }

  public static List<CatEMHCasier> getCasiers(final List<EMHSousModele> sousModeles) {
    final List<CatEMHCasier> res = new ArrayList<>();
    for (final EMHSousModele sousModele : sousModeles) {
      res.addAll(getCasiers(sousModele));
    }
    return res;
  }

  /**
   * @return tous les emh contenu par ce sous-modele dans l'ordre réseau.
   */
  private static List<EMH> getAllSimpleEMHInUserOrder(final EMHSousModele sousModele) {
    final List<EMH> res = new ArrayList<>(sousModele.getNoeuds());
    res.addAll(sousModele.getBranches());
    res.addAll(sousModele.getCasiers());
    res.addAll(getSections(sousModele));
    return res;
  }

  /**
   * @return tous les emh contenu par ce modele
   */
  public static List<EMH> getAllSimpleEMH(final List<EMHSousModele> sousModeles) {
    final Set<EMH> res = new HashSet<>();
    for (final EMHSousModele sousModele : sousModeles) {
      res.addAll(getAllSimpleEMH(sousModele));
    }
    return new ArrayList<>(res);
  }

  /**
   * Attention, l'ordre est celui des fichiers... et ce n'est pas l'ordre reseau pour les casiers,sections
   *
   * @param sousModeles
   * @return tous les emh contenu par ce sous-modele dans l'ordre réseau.
   */
  public static List<EMH> getAllSimpleEMHinUserOrder(final List<EMHSousModele> sousModeles) {
    final Set<EMH> setForUniqueEMH = new HashSet<>();
    final List<EMH> list = new ArrayList<>();
    for (final EMHSousModele sousModele : sousModeles) {
      final List<EMH> emhs = getAllSimpleEMHInUserOrder(sousModele);
      for (final EMH emh : emhs) {
        if (!setForUniqueEMH.contains(emh)) {
          setForUniqueEMH.add(emh);
          list.add(emh);
        }
      }
    }
    return list;
  }
  /**
   * Attention, l'ordre est celui des fichiers... et ce n'est pas l'ordre reseau pour les casiers,sections
   *
   * @return tous les emh contenues par ce scenario
   */
  public static List<EMH> getAllEMHinUserOrder(final EMHScenario scenario) {
    LinkedHashSet<EMH> emhs = new LinkedHashSet<>();
    for(EMHModeleBase modele: scenario.getModeles()){
      emhs.add(modele);
      for(EMHSousModele ssModele:modele.getSousModeles()){
        emhs.add(ssModele);
        emhs.addAll(getAllSimpleEMHInUserOrder(ssModele));
      }
    }
    return new ArrayList<>(emhs);
  }

  public static List<EMH> getAllSimpleEMH(final EMHSousModele sousModele) {
    return EMHHelper.collectEMHInRelationEMHContient(sousModele);
  }
}
