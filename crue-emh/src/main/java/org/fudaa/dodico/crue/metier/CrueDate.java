package org.fudaa.dodico.crue.metier;

/**
 * Object crue date utilise
 * 
 * @author Adrien Hadoux
 */
public class CrueDate {

  /**
   * le numero ou le nombre de jours.
   */
  int nbJours;
  /**
   * le numero ou le nombre de heures.
   */
  int nbHeures;
  /**
   * le numero ou le nombre de Minutes.
   */
  int nbMinutes;
  /**
   * le numero ou le nombre de Secondes.
   */
  int nbSecondes;

  public CrueDate() {

  }

  public CrueDate(final int nbJours, final int nbHeures, final int nbMinutes, final int nbSecondes) {
    this.nbJours = nbJours;
    this.nbHeures = nbHeures;
    this.nbMinutes = nbMinutes;
    this.nbSecondes = nbSecondes;
  }

  public int getNbJours() {
    return nbJours;
  }

  public void setNbJours(final int nbJours) {
    this.nbJours = nbJours;
  }

  public int getNbHeures() {
    return nbHeures;
  }

  public void setNbHeures(final int nbHeures) {
    this.nbHeures = nbHeures;
  }

  public int getNbMinutes() {
    return nbMinutes;
  }

  public void setNbMinutes(final int nbMinutes) {
    this.nbMinutes = nbMinutes;
  }

  public int getNbSecondes() {
    return nbSecondes;
  }

  public void setNbSecondes(final int nbSecondes) {
    this.nbSecondes = nbSecondes;
  }

  public double toSecondes() {
    return nbSecondes + 60d * nbMinutes + 3600d * nbHeures + 3600d * 24d * nbJours;
  }

  /**
   * @return true si toutes les valeurs sont a 0.
   */
  public boolean isZero() {
    return nbSecondes == 0 && nbMinutes == 0 && nbHeures == 0 && nbJours == 0;
  }

  @Override
  public String toString() {
    return "CrueDate: " + nbJours + " j, " + nbHeures + " h, " + nbMinutes + " min, " + nbSecondes + " s";
  }

  /**
   * Retourne une durée au format XSD
   * 
   * @return String au format P0Y0M0DT0H0M0S
   */
  public String toDuration() {

    int noJour = nbJours;
    int noHeure = nbHeures;
    int noMinute = nbMinutes;
    int noSeconde = nbSecondes;

    int quotient = noSeconde / 60;
    if (quotient > 0) {
      noMinute = noMinute + quotient;
      noSeconde = noSeconde % 60;
    }

    quotient = noMinute / 60;
    if (quotient > 0) {
      noHeure = noHeure + quotient;
      noMinute = noMinute % 60;
    }

    quotient = noHeure / 24;
    if (quotient > 0) {
      noJour = noJour + quotient;
      noHeure = noHeure % 24;
    }

    return "P0Y0M" + noJour + "DT" + noHeure + "H" + noMinute + "M" + noSeconde + "S";
  }

  /**
   * Retourne une date au format XSD
   * 
   * @return String au format 0001-01-01T00:00:00
   */
  public String toDateXSD() {
    // on est obligé d'avoir un numéro de jour commençant par 01 (et pas 00) dans la date XSD, donc si le nombre de
    // jours de crueDate est à 0, le numéro de jour de la date XSD sera à 01 (...etc).
    int noJour = nbJours + 1;
    int noHeure = nbHeures;
    if (nbHeures == 24) {
      noJour++;
      noHeure = 0;
    }

    final String strNoJour = formatSur2Digits(noJour);
    final String strNoHeure = formatSur2Digits(noHeure);
    final String strNoMinute = formatSur2Digits(nbMinutes);
    final String strNoSeconde = formatSur2Digits(nbSecondes);

    return "0001-01-" + strNoJour + "T" + strNoHeure + ":" + strNoMinute + ":" + strNoSeconde;
  }

  /**
   * @param entier
   * @return
   */
  private String formatSur2Digits(final int entier) {
    String strFormat = "" + entier;
    if (entier < 10) {
      strFormat = "0" + strFormat;
    }

    return strFormat;
  }
}
