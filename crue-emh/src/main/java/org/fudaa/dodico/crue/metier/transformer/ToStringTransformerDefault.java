/**
 *
 */
package org.fudaa.dodico.crue.metier.transformer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;

/**
 * Transformer par défaut qui utilise les propriétés présente dans les propriétés crue
 *
 * @author deniger
 */
public class ToStringTransformerDefault implements ToStringTransformer {

  public static final ToStringTransformer TO_STRING = in -> ObjectUtils.toString(in);

  private boolean addClassName = true;
  private final CrueConfigMetier crueProperties;
  private List<String> propsSpecified;
  private final Set<String> supplementProperties = new HashSet<>();
  private final DecimalFormatEpsilonEnum type;

  public void addSupplementProp(final String prop) {
    supplementProperties.add(prop);
  }

  /**
   * @param crueProperties
   */
  public ToStringTransformerDefault(final CrueConfigMetier crueProperties, final DecimalFormatEpsilonEnum type) {
    super();
    this.crueProperties = crueProperties;
    this.type = type;
  }

  /**
   * @param crueProperties
   * @param props
   */
  public ToStringTransformerDefault(final CrueConfigMetier crueProperties, final List<String> props, final DecimalFormatEpsilonEnum type) {
    super();
    this.crueProperties = crueProperties;
    this.propsSpecified = props;
    this.type = type;
  }

  public boolean isAddClassName() {
    return addClassName;
  }

  public void setAddClassName(final boolean addClassName) {
    this.addClassName = addClassName;
  }

  @SuppressWarnings("unchecked")
  @Override
  public String transform(final Object in) {
    if (in == null) {
      return StringUtils.EMPTY;
    }
    try {
      final StringBuilder res = new StringBuilder(128);
      if (isAddClassName()) {
        res.append(ToStringHelper.typeToi18n(in.getClass().getSimpleName()));
      }
      final Map describe = PropertyUtils.describe(in);
      final boolean isPropSpecified = this.propsSpecified != null;
      final List props = isPropSpecified ? propsSpecified : new ArrayList(describe.keySet());
      if (!isPropSpecified) {
        Collections.sort(props);
      }
      boolean beginAdded = false;
      for (final Object object : props) {
        final String prop = (String) object;
        if (isPropSpecified || crueProperties.isPropertyDefined(prop) || supplementProperties.contains(prop)) {
          if (!beginAdded) {
            beginAdded = true;
            if (isAddClassName()) {
              res.append(" [");
            }
          } else {
            res.append(", ");
          }
          res.append(prop).append('=').append(TransformerEMHHelper.formatFromPropertyName(prop, describe.get(object), crueProperties, type));
        }

      }
      if (beginAdded && isAddClassName()) {
        res.append("]");
      }
      return res.toString();

    } catch (final Exception e) {
      Logger.getLogger(ToStringTransformerDefault.class.getName()).log(Level.INFO, "message {0}", e);
    }
    return StringUtils.EMPTY;
  }
}
