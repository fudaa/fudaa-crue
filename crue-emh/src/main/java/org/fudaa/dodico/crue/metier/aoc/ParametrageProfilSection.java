package org.fudaa.dodico.crue.metier.aoc;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;

/**
 * Created by deniger on 28/06/2017.
 */
public class ParametrageProfilSection implements AocWithSectionRef{

    private String pk;
    private String sectionRef;

    public ParametrageProfilSection() {

    }

    public ParametrageProfilSection(String pk, String sectionRef) {
        this.pk = pk;
        this.sectionRef = sectionRef;
    }
    @PropertyDesc(i18n = "pk.property")
    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    /**
     *
     * @return le nom de l'EMH
     */
    @PropertyDesc(i18n = "sectionRef.property")
    public String getSectionRef() {
        return sectionRef;
    }

    public void setSectionRef(String sectionRef) {
        this.sectionRef = sectionRef;
    }

    @Override
    public ParametrageProfilSection clone() {
        return new ParametrageProfilSection(pk, sectionRef);
    }
}
