package org.fudaa.dodico.crue.metier.etude;

import java.util.List;

import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

/**
 * Definit un run d'application.
 *
 * @author Adrien Hadoux
 */
public class EMHRun implements ObjetNomme, Cloneable {

    /**
     * identifiant du run.
     */
    private String id_;
    private String nom_;

    /**
     * Infos sur la version du run.
     */
    EMHInfosVersion infosVersion_;


    public EMHRun(final String nom) {
        nom_ = nom;
        id_ = nom_ == null ? null : nom_.toUpperCase();
    }


    @Override
    public String getId() {
        return id_;
    }

    public EMHInfosVersion getInfosVersion() {
        return infosVersion_;
    }

    public void setInfosVersion(final EMHInfosVersion infosVersion) {
        this.infosVersion_ = infosVersion;
    }

    /**
     * Retrouve le bon run par sa reference.
     *
     * @param id
     * @param listeFiles
     * @return
     */
    public static EMHRun findById(final String id, final List<EMHRun> listeFiles) {
        return EMHHelper.selectObjectNomme(listeFiles, id);
    }

    @Override
    public String getNom() {
        return nom_;
    }

    @Override
    public String toString() {
        return nom_;
    }

    @Override
    public void setNom(String newNom) {
    }

    public void changeNom(String nom) {
        if (nom != null){
            nom_ = nom;
            id_ = nom_.toUpperCase();
        }
    }


    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
