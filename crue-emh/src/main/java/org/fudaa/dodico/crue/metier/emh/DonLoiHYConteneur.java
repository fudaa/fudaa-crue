package org.fudaa.dodico.crue.metier.emh;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByIdComparator;

public class DonLoiHYConteneur extends AbstractInfosEMH implements Sortable {

  private java.util.List<Loi> lois;
  private java.util.List<Loi> unmodifiableLois;

  public java.util.List<Loi> getLois() {
    if (lois == null) {
      lois = new java.util.ArrayList<>();
    }
    if (unmodifiableLois == null) {
      unmodifiableLois = Collections.unmodifiableList(lois);
    }
    return unmodifiableLois;
  }

  public DonLoiHYConteneur cloneWithoutPoints() {
    final DonLoiHYConteneur res = new DonLoiHYConteneur();
    for (final Loi loi : lois) {
      res.addLois(loi.cloneWithoutPoints());
    }
    return res;
  }

  @Override
  public boolean sort() {
    if (lois != null) {
      final List before = new ArrayList(lois);
      Collections.sort(lois, ObjetNommeByIdComparator.INSTANCE);
      return !before.equals(lois);
    }
    return false;

  }

  public void setLois(final java.util.List<Loi> newLois) {
    removeAllLois();
    for (final java.util.Iterator iter = newLois.iterator(); iter.hasNext();) {
      addLois((Loi) iter.next());
    }
  }

  public void addLois(final Loi newLoi) {
    if (newLoi == null) {
      return;
    }
    if (this.lois == null) {
      this.lois = new java.util.ArrayList<>();
    }
    this.lois.add(newLoi);
  }

  public void removeLois(final Loi oldLoi) {
    if (oldLoi == null) {
      return;
    }
    if (this.lois != null) {
      if (this.lois.contains(oldLoi)) {
        this.lois.remove(oldLoi);
      }
    }
  }

  public void removeAllLois() {
    if (lois != null) {
      lois.clear();
    }
  }

  @Override
  public final EnumInfosEMH getCatType() {
    return EnumInfosEMH.DON_LOI_CONTENEUR_SCENARIO;
  }

  public void addAllLois(final java.util.List<Loi> newListLoi) {
    for (final java.util.Iterator iter = newListLoi.iterator(); iter.hasNext();) {
      addLois((Loi) iter.next());
    }
  }
}
