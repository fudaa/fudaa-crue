package org.fudaa.dodico.crue.metier.comparator;

import java.util.Comparator;
import org.fudaa.dodico.crue.metier.emh.Regle;

/**
 * A comparator for ObjetNomme
 * 
 * @author deniger
 */
public class RegleTypeComparator implements Comparator<Regle> {

  /**
   * Comparator singleton.
   */
  public static final RegleTypeComparator INSTANCE = new RegleTypeComparator();

  private RegleTypeComparator() {

  }

  @Override
  public int compare(Regle o1, Regle o2) {
    if (o1 == o2) { return 0; }
    if (o1 == null) { return -1; }
    if (o2 == null) { return 1; }
    int compareString = compareString(o1.getType().toString(), o2.getType().toString());
    return compareString == 0 ? o1.hashCode() - o2.hashCode() : compareString;
  }

  public static int compareString(String o1, String o2) {
    if (o1 == o2) { return 0; }
    if (o1 == null) { return -1; }
    if (o2 == null) { return 1; }
    return o1.compareTo(o2);
  }

}
