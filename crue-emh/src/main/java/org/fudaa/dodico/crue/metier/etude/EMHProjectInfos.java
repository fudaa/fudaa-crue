package org.fudaa.dodico.crue.metier.etude;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.metier.emh.CatEMHConteneur;

/**
 * Infos relatives au projet et la base de fichiers disponible pour tout le projet. .
 *
 * @author Adrien Hadoux
 */
public class EMHProjectInfos {

  public static final Logger LOGGER = Logger.getLogger(EMHProjectInfos.class.getName());
  String xmlVersion;
  /**
   * Contient l'ensemble des fichiers du projet qui seront utilises pour realiser les chargements des projets.
   */
  protected List<FichierCrue> baseFichiersProjets = new ArrayList<>();
  protected EMHInfosVersion infosVersions;
  /**
   * Definit les repertoires dans lesquels se situent les projets. Utilise pour les key, les string static declarees plus bas. la value indique le
   * chemin absolu vers le repertoire.
   */
  protected Map<String, String> directories;
  /**
   * Emplacements des fichiers d'etudes
   */
  public static final String FICHETUDES = "FICHETUDES";
  /**
   * Emplacement des runs
   */
  public static final String RUNS = "RUNS";
  public static final String CONFIG = "CONFIG";
  public static final String OCAL_UI_SAVE = "user.ocal.ui.xml";
  /**
   * Emplacemet des rapports
   */
  public static final String RAPPORTS = "RAPPORTS";

  public static List<String> getRepertoires() {
    return Arrays.asList(FICHETUDES, RUNS, RAPPORTS, CONFIG);
  }
  /**
   * le répertoire du fichier etu de ce projet. Utilise si chemins relatifs utilisés*
   */
  private File parentDirOfEtuFile;
  private File etuFile;

  public List<FichierCrue> getBaseFichiersProjets() {
    return baseFichiersProjets;
  }

  public File getParentDirOfEtuFile() {
    return parentDirOfEtuFile;
  }

  public File getEtuFile() {
    return etuFile;
  }

  public void setEtuFile(File etuFile) {
    this.parentDirOfEtuFile = etuFile.getParentFile();
    this.etuFile = etuFile;
  }

  public void setBaseFichiersProjets(final List<FichierCrue> listFileAvailable) {
    this.baseFichiersProjets = listFileAvailable == null ? Collections.emptyList() : listFileAvailable;
  }

  public boolean removeFile(FichierCrue fichierCrue) {
    return baseFichiersProjets.remove(fichierCrue);
  }

  public boolean renameFile(FichierCrue fichierCrue, String newName) {
    if (newName.equals(fichierCrue.getNom())) {
      return true;
    }
    if (existFileInBase(newName)) {
      if (LOGGER.isLoggable(Level.INFO)) {
        LOGGER.log(Level.INFO, "file {0} not rename in {1} because exists", new Object[]{fichierCrue.getNom(), newName});
      }
      return false;
    }
    fichierCrue.setNom(newName);
    return true;
  }

  /**
   * Accepte ou non le fichier si son id est unique.
   *
   * @param id
   * @return
   */
  public boolean acceptFile(final String id) {
    return !existFileInBase(id);

  }

  /**
   * Retourne true si le fichier existe en base reference par son id.
   *
   * @param id
   * @return
   */
  public boolean existFileInBase(final String id) {
    return getFileFromBase(id) != null;

  }

  /**
   * Retourne le fichier de la base.
   *
   * @param idOrName
   * @return true si le nom ou l'id existe
   */
  public FichierCrue getFileFromBase(final String idOrName) {
    return FichierCrue.findById(idOrName, baseFichiersProjets);
  }

  /**
   * Ajoute un fichier a la base de fichier Crue disponibles pour le projet.
   *
   * @param file
   * @return
   */
  public boolean addCrueFileToProject(final FichierCrue file) {
    if (baseFichiersProjets == null) {
      baseFichiersProjets = new ArrayList<>();
    }
    if (file != null && acceptFile(file.getNom())) {
      FichierCrue old = getFileFromBase(file.getNom());
      CollectionCrueUtil.addInList(baseFichiersProjets, file, old);
      return old == null;
    }
    return false;
  }

  /**
   * Ne pas utiliser pour récupérer les dossiers, utiliser getDir.
   *
   * @return
   */
  public Map<String, String> getDirectories() {
    return directories;
  }

  public File getDirOfFichiersEtudes() {
    return getDir(FICHETUDES);
  }

  public File getDirOfRuns() {
    return getDir(RUNS);
  }

  public File getDirOfRapports() {
    return getDir(RAPPORTS);
  }

  public File getDirOfConfig() {
    if (!isDirOfConfigDefined()) {
      return null;
    }
    return getDir(CONFIG);
  }

  public boolean isDirOfConfigDefined() {
    return directories.containsKey(CONFIG) && directories.get(CONFIG) != null;
  }

  /**
   *
   * @param container le container
   * @return le folder de configuration dédié a ce container
   */
  public File getDirOfConfig(CatEMHConteneur container) {
    if (!isDirOfConfigDefined()) {
      return null;
    }
    return new File(getDir(CONFIG), container.getId());
  }

  /**
   *
   * @param scenario le scenario en question
   * @return le fichier contenant les données persistées pour le fichier OCAL dans l'interface
   */
  public File getUiOCALFile(ManagerEMHScenario scenario) {
    return new File(getDirOfConfig(scenario), OCAL_UI_SAVE);
  }

  /**
   *
   * @param containerId l'id du container
   * @return le folder de configuration dédié a ce container
   */
  public File getDirOfConfig(String containerId) {
    if (!isDirOfConfigDefined()) {
      return null;
    }
    return new File(getDir(CONFIG), containerId);
  }

  public File getDirOfConfig(ManagerEMHContainerBase modele) {
    if (!isDirOfConfigDefined()) {
      return null;
    }
    return new File(getDir(CONFIG), modele.getId());
  }

  private File getDir(String id) {
    if(directories==null){
      return null;
    }
    String path = directories.get(id);
    if (CrueFileHelper.isRelative(path) && parentDirOfEtuFile == null) {
      LOGGER.log(Level.WARNING, "path is relative for {0} and dir of etu is not set", id);
    }
    if (path == null) {
      return null;
    }
    if (parentDirOfEtuFile == null) {
      return new File(path);
    }
    return CrueFileHelper.getCanonicalBaseDir(parentDirOfEtuFile, path);

  }

  public void setDirectories(final Map<String, String> directories) {
    this.directories = directories;
  }

  public EMHInfosVersion getInfosVersions() {
    return infosVersions;
  }

  public void setInfosVersions(final EMHInfosVersion infosVersions) {
    this.infosVersions = infosVersions;
  }

  public void setXmlVersion(String versionName) {
    this.xmlVersion = versionName;

  }

  /**
   * @return the xmlVersion
   */
  public String getXmlVersion() {
    return xmlVersion;
  }
}
