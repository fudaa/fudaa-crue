/**
 * *********************************************************************
 * Module: LoiDF.java Author: deniger Purpose: Defines the Class LoiDF
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.joda.time.LocalDateTime;

/**
 * Loi Date/Flottant
 *
 */
public class LoiDF extends Loi {

  private LocalDateTime dateZeroLoiDF;

  /**
   * @return
   */
  public LocalDateTime getDateZeroLoiDF() {
    return dateZeroLoiDF;
  }

  @Override
  public LoiDF clonedWithoutUser() {
    return (LoiDF) super.clonedWithoutUser();
  }

  /**
   * @param dateZeroLoiDF2
   */
  public void setDateZeroLoiDF(final LocalDateTime dateZeroLoiDF2) {
    dateZeroLoiDF = dateZeroLoiDF2;

  }
}
