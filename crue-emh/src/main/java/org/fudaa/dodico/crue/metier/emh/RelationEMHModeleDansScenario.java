package org.fudaa.dodico.crue.metier.emh;

/**
 *
 * @author deniger
 */
public class RelationEMHModeleDansScenario extends RelationEMH<EMHScenario> {

  public RelationEMHModeleDansScenario(EMHScenario scenario) {
    setEmh(scenario);
  }

  @Override
  public RelationEMHModeleDansScenario copyWithEMH(EMHScenario copiedEMH) {
    return new RelationEMHModeleDansScenario(copiedEMH);
  }

}
