package org.fudaa.dodico.crue.metier.emh;

/**
 * Pour généraliser les EMH activables.
 */
public abstract class CatEMHActivable extends EMH {
  private boolean userActive;

  @Override
  public boolean getUserActive() {
    return userActive;
  }

  public void setUserActive(final boolean newIsActive) {
    userActive = newIsActive;
  }

  public CatEMHActivable() {
    super();
  }

  public CatEMHActivable(final String nom) {
    super(nom);
  }


  @Override
  protected <T extends EMH> T copyPrimitiveIn(T in) {
    ((CatEMHActivable) in).setUserActive(getUserActive());
    return super.copyPrimitiveIn(in);
  }
}
