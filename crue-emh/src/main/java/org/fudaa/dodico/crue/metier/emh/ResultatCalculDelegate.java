package org.fudaa.dodico.crue.metier.emh;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author deniger
 */
public interface ResultatCalculDelegate {
  ResultatPasDeTempsDelegate getResultatPasDeTemps();

  List<String> getQZRegulVariablesId();
  List<String> getQRegulVariablesName();
  List<String> getZRegulVariablesName();

  Map<String, Object> read(final ResultatTimeKey key) throws IOException;

  /**
   * @return le type de l'EMH sans EMH devant Section, Casier, ...
   */
  String getEMHType();
}
