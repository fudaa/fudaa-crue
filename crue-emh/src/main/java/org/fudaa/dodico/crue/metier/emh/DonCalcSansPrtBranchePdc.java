package org.fudaa.dodico.crue.metier.emh;

import java.util.Collection;

/**
 * Pour les branches type 1 Crue9
 */
public class DonCalcSansPrtBranchePdc extends DonCalcSansPrt {
  private LoiFF pdc;

  public LoiFF getPdc() {
    return pdc;
  }

  @Override
  public void fillWithLoi(final Collection<Loi> target) {
    target.add(pdc);
  }

  @Override
  public DonCalcSansPrt cloneDCSP() {
    try {
      DonCalcSansPrtBranchePdc res = (DonCalcSansPrtBranchePdc) super.clone();
      if (pdc != null) {
        res.setPdc(pdc.clonedWithoutUser());
      }
      return res;
    } catch (CloneNotSupportedException e) {
    }
    return null;
  }

  public void setPdc(final LoiFF newPdc) {
    if (this.pdc != null) {
      this.pdc.unregister(this);
    }

    pdc = newPdc;
    this.pdc.register(this);
  }
}
