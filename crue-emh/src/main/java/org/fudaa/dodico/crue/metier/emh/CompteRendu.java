package org.fudaa.dodico.crue.metier.emh;

import gnu.trove.TObjectIntHashMap;
import java.io.File;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 *
 * @author deniger
 */
public class CompteRendu implements Comparable<CompteRendu> {

  private final String id;
  private final File logFile;
  private final CrueFileType crueFileType;
  private static final TObjectIntHashMap<CrueFileType> ORDER = new TObjectIntHashMap<>();

  static {
    ORDER.put(CrueFileType.RPTR, 0);
    ORDER.put(CrueFileType.RPTG, 1);
    ORDER.put(CrueFileType.RPTI, 2);
    ORDER.put(CrueFileType.RCAL, 3);
  }

  public CompteRendu(File logFile, CrueFileType crueFileType) {
    this.logFile = logFile;
    this.id = logFile.getName();
    this.crueFileType = crueFileType;
  }

  @Visibility(ihm = false)
  public CrueFileType getCrueFileType() {
    return crueFileType;
  }

  public File getLogFile() {
    return logFile;
  }

  public String getId() {
    return id;
  }

  @Override
  public int compareTo(CompteRendu o) {
    if (o == null) {
      return 1;
    }
    if (o == this) {
      return 1;
    }
    int value = ORDER.get(crueFileType);
    int other = ORDER.get(o.crueFileType);
    int res = value - other;
    if (res == 0) {
      res = logFile.compareTo(o.getLogFile());
    }
    return res;
  }
}
