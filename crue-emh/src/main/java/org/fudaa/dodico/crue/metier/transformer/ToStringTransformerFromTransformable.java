/**
 *
 */
package org.fudaa.dodico.crue.metier.transformer;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;

/**
 * @author deniger
 */
public class ToStringTransformerFromTransformable implements ToStringTransformer {

  private final CrueConfigMetier props;
  private final DecimalFormatEpsilonEnum type;

  /**
   * @param props
   */
  public ToStringTransformerFromTransformable(CrueConfigMetier props, DecimalFormatEpsilonEnum type) {
    super();
    this.props = props;
    this.type = type;
  }

  /**
   * in doit etre null ou etre ToStringTransformable
   */
  @Override
  public String transform(Object in) {
    if (in == null) {
      return StringUtils.EMPTY;
    }
    return ((ToStringTransformable) in).toString(props, EnumToString.COMPLETE, type);
  }
}
