/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.contrat.DoubleValuable;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

public enum EnumSensOuv implements ToStringInternationalizable, DoubleValuable {

  OUV_VERS_HAUT(0), OUV_VERS_BAS(1);
  private final int id;

  EnumSensOuv(final int id) {
    this.id = id;
  }

  @Override
  public double toDoubleValue() {
    return id;
  }

  @Override
  public String geti18n() {
    return BusinessMessages.getString("EnumSensOuv." + name() + ".shortName");
  }

  @Override
  public String geti18nLongName() {
    return BusinessMessages.getString("EnumSensOuv." + name() + ".longName");
  }
}
