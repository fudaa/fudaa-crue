package org.fudaa.dodico.crue.metier.cloner;

import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.IdRegistry;
import org.fudaa.dodico.crue.metier.emh.InfosEMH;

import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;

public class EMHScenarioCloner {

  public EMHScenario copy(EMHScenario in) {
    IdRegistry.install(in);
    Map<Long, EMH> initEMHById = in.getIdRegistry().getEmhById();
    Map<Long, EMH> clonedById = new HashMap<>();
    initEMHById.forEach((id, emh) -> clonedById.put(id, emh.copyShallowFirstLevel()));
    EMHScenario copiedScenario = (EMHScenario) clonedById.get(in.getUiId());
    IdentityHashMap<InfosEMH,InfosEMH> infoEMHCopies=new IdentityHashMap<>();
    clonedById.forEach((id, copied) -> {
      EMH init = initEMHById.get(id);
      assert init.getClass().equals(copied.getClass());
      assert init != copied;
      init.getRelationEMH().forEach(relationEMH -> {
        EMH copiedTargetEMH =clonedById.get(relationEMH.getEmh().getUiId());
        copied.addRelationEMH(relationEMH.copyWithEMH(copiedTargetEMH));
      });
      init.getInfosEMH().forEach(infosEMH -> copied.addInfosEMH(infosEMH));
    });



    return copiedScenario;
  }
}
