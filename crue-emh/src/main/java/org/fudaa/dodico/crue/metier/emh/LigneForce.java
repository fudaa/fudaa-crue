package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

public class LigneForce implements ObjetNomme {
  private String nom;
  private java.util.List<PtProfil> ptProfil;

  public java.util.List<PtProfil> getPtProfil() {
    if (ptProfil == null) {
      ptProfil = new java.util.ArrayList<>();
    }
    return ptProfil;
  }

  public java.util.Iterator getIteratorPtProfil() {
    if (ptProfil == null) {
      ptProfil = new java.util.ArrayList<>();
    }
    return ptProfil.iterator();
  }

  @UsedByComparison
  @Visibility(ihm = false)
  public int getPtProfilSize() {
    return ptProfil == null ? 0 : ptProfil.size();
  }

  public void setPtProfil(java.util.List<PtProfil> newPtProfil) {
    removeAllPtProfil();
    for (java.util.Iterator iter = newPtProfil.iterator(); iter.hasNext(); ) {
      addPtProfil((PtProfil) iter.next());
    }
  }

  public void addPtProfil(PtProfil newPtProfil) {
    if (newPtProfil == null) {
      return;
    }
    if (this.ptProfil == null) {
      this.ptProfil = new java.util.ArrayList<>();
    }
    if (!this.ptProfil.contains(newPtProfil)) {
      this.ptProfil.add(newPtProfil);
    }
  }

  public void removePtProfil(PtProfil oldPtProfil) {
    if (oldPtProfil == null) {
      return;
    }
    if (this.ptProfil != null) {
      if (this.ptProfil.contains(oldPtProfil)) {
        this.ptProfil.remove(oldPtProfil);
      }
    }
  }

  public void removeAllPtProfil() {
    if (ptProfil != null) {
      ptProfil.clear();
    }
  }

  @Override
  public final String getNom() {
    return nom;
  }

  private String id;

  @Override
  public String getId() {
    return id;
  }

  @Override
  public final void setNom(String newNom) {
    nom = newNom;
    if (nom != null) {
      id = nom.toUpperCase();
    }
  }
}
