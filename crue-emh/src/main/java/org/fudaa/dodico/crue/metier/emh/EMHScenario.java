/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

public class EMHScenario extends EMHConteneurDeSousModele {

  public EMHScenario() {
  }

  @Override
  public boolean sort() {
    boolean modified = super.sort();
    for (final EMHModeleBase modele : getModeles()) {
      modified |= modele.sort();
    }
    return modified;
  }

  @Override
  public Object getSubCatType() {
    return null;
  }

  @Override
  public EnumCatEMH getCatType() {
    return EnumCatEMH.SCENARIO;
  }

  @Override
  public List<EMHSousModele> getSousModeles() {
    final List<EMHModeleBase> modele = EMHHelper.collectEMHInRelationEMHContient(this, EnumCatEMH.MODELE);
    if (CollectionUtils.isEmpty(modele)) {
      return null;
    }
    final List<EMHSousModele> ssModele = new ArrayList<>();
    for (final EMHModeleBase m : modele) {
      ssModele.addAll(m.getSousModeles());
    }
    return ssModele;
  }

  /**
   * Utilisé par la comparaison.
   * @return toutes les EMHs du scenario
   */
  @UsedByComparison
  public List<EMH> getAllEMHinUserOrder(){
    return CatEMHConteneurHelper.getAllEMHinUserOrder(this);
  }

  public EMHSousModele getConcatSousModele() {
    return EMHHelper.concat(getSousModeles());
  }

  /**
   * @return liste des modele
   */
  @UsedByComparison
  public List<EMHModeleBase> getModeles() {
    return EMHHelper.collectEMHInRelationEMHContient(this, EnumCatEMH.MODELE);
  }

  public CompteRenduAvancement getAvancement() {
    return EMHHelper.selectFirstOfClass(getInfosEMH(), CompteRenduAvancement.class);
  }

  /**
   * @return toutes les emh y compris ce scenario, les modeles et les sous-modeles.
   */
  public List<EMH> getAllEMH() {
    final List<EMH> simpleEMH = getAllSimpleEMH();
    final List<EMH> res = new ArrayList<>(simpleEMH.size() + 20);
    res.add(this);
    final List<EMHModeleBase> modeles = getModeles();
    res.addAll(modeles);
    for (final EMHModeleBase modeleBase : modeles) {
      res.addAll(modeleBase.getSousModeles());
    }
    res.addAll(simpleEMH);
    return res;

  }

  /**
   * @return le conteneur de loi: toujours non null
   */
  public DonLoiHYConteneur getLoiConteneur() {
    DonLoiHYConteneur donLoiHYConteneur = (DonLoiHYConteneur) EMHHelper.selectInfoEMH(this,
            EnumInfosEMH.DON_LOI_CONTENEUR_SCENARIO);
    if (donLoiHYConteneur == null) {
      donLoiHYConteneur = new DonLoiHYConteneur();
      addInfosEMH(donLoiHYConteneur);
    }
    return donLoiHYConteneur;
  }

  /**
   * @return the ordCalc
   */
  public OrdCalcScenario getOrdCalcScenario() {
    return (OrdCalcScenario) EMHHelper.selectInfoEMH(this, EnumInfosEMH.ORD_CALC_SCENARIO);
  }

  /**
   * @return the ordRes
   */
  public OrdResScenario getOrdResScenario() {
    return (OrdResScenario) EMHHelper.selectInfoEMH(this, EnumInfosEMH.ORD_RES_SCENARIO);
  }

  public ParamCalcScenario getParamCalcScenario() {
    return (ParamCalcScenario) EMHHelper.selectInfoEMH(this, EnumInfosEMH.PARAM_CALC_SCENARIO);
  }

  @UsedByComparison
  public DonCLimMScenario getDonCLimMScenario() {
    return (DonCLimMScenario) EMHHelper.selectInfoEMH(this, EnumInfosEMH.DON_CLIM_SCENARIO);
  }
  /**
   * enregistrement d'un uid par EMH
   */
  private IdRegistry idRegisty;

  /**
   * Attention: ne fait rien ( n'enregistre pas les EMH).
   *
   * @param registry
   */
  protected void setIdRegistry(final IdRegistry registry) {
    this.idRegisty = registry;
  }

  @Override
  public List<EMH> getAllSimpleEMH() {
    if (idRegisty != null) {
      return idRegisty.getAllSimpleEMH();
    }
    return super.getAllSimpleEMH();
  }

  public IdRegistry getIdRegistry() {
    return this.idRegisty;
  }

  @Override
  @Visibility(ihm = false)
  public EMHScenario copyShallowFirstLevel() {
    return copyPrimitiveIn(new EMHScenario());
  }
}
