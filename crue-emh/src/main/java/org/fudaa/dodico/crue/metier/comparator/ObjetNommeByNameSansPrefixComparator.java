package org.fudaa.dodico.crue.metier.comparator;


import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

/**
 * A comparator for ObjetWithID
 * 
 * @author Yoan GRESSIER
 */
public class ObjetNommeByNameSansPrefixComparator extends SafeComparator<ObjetWithID> {

  /**
   * Comparator singleton.
   */
  public static final ObjetNommeByNameSansPrefixComparator INSTANCE = new ObjetNommeByNameSansPrefixComparator();

  private ObjetNommeByNameSansPrefixComparator() {

  }

  @Override
  protected int compareSafe(ObjetWithID o1, ObjetWithID o2) {
    // suppression du prefixe pour la comparaison ("Fk_", "Fn_", "FkSto_", "FnSto_")
    String obj1, obj2;
    obj1 = o1.getNom().substring(o1.getNom().indexOf("_")+1).toLowerCase();
    obj2 = o2.getNom().substring(o2.getNom().indexOf("_")+1).toLowerCase();
    
    // comparaison case insensitive
    int compareString = compareString(obj1, obj2);
    return compareString == 0 ? obj1.hashCode() - obj2.hashCode() : compareString;
  }

}
