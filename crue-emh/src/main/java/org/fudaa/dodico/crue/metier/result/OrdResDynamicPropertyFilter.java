package org.fudaa.dodico.crue.metier.result;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.fudaa.dodico.crue.common.PropertyUtilsCache;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.DynamicPropertyFilter;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;

/**
 * Permet de trouver les propriétés ordres communes entre 2 OrdResScenario
 *
 * @author deniger
 */
public class OrdResDynamicPropertyFilter implements DynamicPropertyFilter {

  public static final String ORES_ID_FOR_CASIER = "Casier";
  public static final String ORES_ID_FOR_SECTION = "Section";
  public static final String ORES_ID_FOR_MODELE = "ModeleBase";
  public static final String ORES_ID_FOR_MODELE_REGUL = "ModeleRegul";
  private final Map<String, Set<String>> cache = new HashMap<>();
  private final OrdResScenario ordResA;
  private final OrdResScenario ordResB;
  private static final Logger LOGGER = Logger.getLogger(OrdResDynamicPropertyFilter.class.getName());

  private static String normalizeEMHType(String in) {
    if (in == null) {
      return null;
    }
    if (in.startsWith(ORES_ID_FOR_CASIER)) {
      return ORES_ID_FOR_CASIER;
    }

    if (in.startsWith(ORES_ID_FOR_SECTION)) {
      return ORES_ID_FOR_SECTION;
    }
    if (isModeleType(in)) {
      return ORES_ID_FOR_MODELE_REGUL;
    }
    return in;
  }

  public static boolean isModeleType(String emhType) {
    return emhType.startsWith(ORES_ID_FOR_MODELE);
  }

  public static Set<String> getOrdresProps(String emhType, OrdResScenario ordResScenario) {
    String normalizeEMHType = normalizeEMHType(emhType);
    String method = "ordRes" + normalizeEMHType;
    try {
      OrdRes ordRes = (OrdRes) PropertyUtilsCache.getInstance().getProperty(ordResScenario, method);
      Collection<Dde> ddes = ordRes.getDdes();
      return TransformerHelper.toSetOfId(ddes);
    } catch (Exception e) {
      LOGGER.log(Level.SEVERE, "getOrdresProps " + normalizeEMHType, e);
      throw new IllegalStateException(e);
    }
  }

  public static Set<String> getOrdresPropsWithRegul(String emhType, OrdResScenario ordResScenario, ResultatCalcul resultatCalcul) {
    Set<String> ordresProps = getOrdresProps(emhType, ordResScenario);
    if (isModeleType(emhType) && ordResScenario.isModeleQZRegulActivated() && resultatCalcul != null) {
      ordresProps.addAll(resultatCalcul.getQZRegulVariablesId());
    }
    return ordresProps;
  }
  public static Set<String> getOrdresPropsWithRegul(ResultatCalcul resultatCalcul, OrdResScenario ordResScenario) {
    return getOrdresPropsWithRegul(resultatCalcul.getEMHType(),ordResScenario,resultatCalcul);
  }

  /**
   * @param ordResA
   * @param ordResB
   */
  public OrdResDynamicPropertyFilter(OrdResScenario ordResA, OrdResScenario ordResB) {
    super();
    this.ordResA = ordResA;
    this.ordResB = ordResB;
  }

  @Override
  public Set<String> getPropertyToUse(EMH emhA, EMH emhB) {
    if (emhA.getResultatCalcul() == null) {
      return Collections.emptySet();
    }
    String emhType = emhA.getResultatCalcul().getEMHType();
    if (emhType == null) {
      return Collections.emptySet();
    }
    //pour les modeles et si le mode regulation est activé, les variables des regulations communes seront renvoyés.
    if (isModeleType(emhType)) {
      boolean regulA = ordResA.isModeleQZRegulActivated();
      boolean regulB = ordResB.isModeleQZRegulActivated();
      if (regulA && regulB) {
        Set<String> ordresProps = getOrdresProps(emhType, ordResA);
        ordresProps.retainAll(getOrdresProps(emhType, ordResB));
        ordresProps.remove(OrdResModeleRegul.Q_Z_REGUL);

        Set<String> qzRegul = new HashSet<>(emhA.getResultatCalcul().getQZRegulVariablesId());
        qzRegul.retainAll(emhB.getResultatCalcul().getQZRegulVariablesId());
        ordresProps.addAll(qzRegul);
        return ordresProps;
      }
    }
    Set<String> res = cache.get(emhType);
    if (res == null) {
      Set<String> ordresProps = getOrdresProps(emhType, ordResA);
      ordresProps.retainAll(getOrdresProps(emhType, ordResB));
      res = Collections.unmodifiableSet(ordresProps);
      cache.put(emhType, res);
    }
    return res;
  }
}
