package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.CrueFileType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author deniger
 */
public class CompteRendusInfoEMH extends AbstractInfosEMH {
  public static final String STDERRCSV_NAME = "error_calcul_exited.err";
  private final List<CompteRendu> compteRendus = new ArrayList<>();
  final List<CompteRendu> compteRendusExt = Collections.unmodifiableList(compteRendus);

  public void addCompteRendu(final CompteRendu cr) {
    compteRendus.add(cr);
  }

  public void sort() {
    Collections.sort(compteRendus);
  }

  @Override
  public EnumInfosEMH getCatType() {
    return EnumInfosEMH.LOGS;
  }

  public List<CompteRendu> getCompteRendus() {
    return compteRendusExt;
  }

  public CompteRendu getError() {
    for (final CompteRendu compteRendu : compteRendus) {
      if (compteRendu.getId().equals(STDERRCSV_NAME)) {
        return compteRendu;
      }
    }
    return null;
  }

  CompteRendu getCompteRendus(final CrueFileType type) {
    for (final CompteRendu compteRendu : compteRendus) {
      if (compteRendu.getCrueFileType().equals(type)) {
        return compteRendu;
      }
    }
    return null;
  }
}
