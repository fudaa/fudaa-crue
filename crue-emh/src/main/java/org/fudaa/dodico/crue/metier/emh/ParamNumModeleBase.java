/**
 * *********************************************************************
 * Module: ParamNumModeleBase.java Author: deniger Purpose: Defines the Class ParamNumModeleBase
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;

public class ParamNumModeleBase extends AbstractInfosEMH implements ToStringTransformable, Cloneable {
  private double frLinInf;
  private double frLinSup;
  private double zref;
  private ParamNumCalcPseudoPerm paramNumCalcPseudoPerm;
  private ParamNumCalcTrans paramNumCalcTrans;
  private ParamNumCalcVraiPerm paramNumCalcVraiPerm;

  public ParamNumModeleBase(CrueConfigMetier defaults) {
    frLinInf = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_FR_LIN_INF);
    frLinSup = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_FR_LIN_SUP);
    zref = defaults.getDefaultDoubleValue("zref");
    paramNumCalcPseudoPerm = new ParamNumCalcPseudoPerm(defaults);
    paramNumCalcTrans = new ParamNumCalcTrans(defaults);
  }

  @Override
  public ParamNumModeleBase clone() {
    try {
      return (ParamNumModeleBase) super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why ?");
  }

  public ParamNumModeleBase deepClone() {
    ParamNumModeleBase res = clone();
    res.paramNumCalcPseudoPerm = paramNumCalcPseudoPerm == null ? null : paramNumCalcPseudoPerm.deepClone();
    res.paramNumCalcTrans = paramNumCalcTrans == null ? null : paramNumCalcTrans.deepClone();
    res.paramNumCalcVraiPerm = paramNumCalcVraiPerm == null ? null : paramNumCalcVraiPerm.deepClone();
    res.emh = null;
    return res;
  }

  public ParamNumCalcPseudoPerm getParamNumCalcPseudoPerm() {
    return paramNumCalcPseudoPerm;
  }

  public void setParamNumCalcPseudoPerm(ParamNumCalcPseudoPerm newParamNumCalcPseudoPerm) {
    this.paramNumCalcPseudoPerm = newParamNumCalcPseudoPerm;
  }

  public ParamNumCalcTrans getParamNumCalcTrans() {
    return paramNumCalcTrans;
  }

  public void setParamNumCalcTrans(ParamNumCalcTrans newParamNumCalcTrans) {
    this.paramNumCalcTrans = newParamNumCalcTrans;
  }

  public ParamNumCalcVraiPerm getParamNumCalcVraiPerm() {
    return paramNumCalcVraiPerm;
  }

  public void setParamNumCalcVraiPerm(ParamNumCalcVraiPerm newParamNumCalcVraiPerm) {
    this.paramNumCalcVraiPerm = newParamNumCalcVraiPerm;
  }

  @Override
  public final EnumInfosEMH getCatType() {
    return EnumInfosEMH.PARAM_NUM_MODELE_BASE;
  }

  @PropertyDesc(i18n = "frLinInf.property")
  public final double getFrLinInf() {
    return frLinInf;
  }

  public final void setFrLinInf(double newFrLinInf) {
    frLinInf = newFrLinInf;
  }

  @PropertyDesc(i18n = "frLinSup.property")
  public final double getFrLinSup() {
    return frLinSup;
  }

  public final void setFrLinSup(double newFrLinSup) {
    frLinSup = newFrLinSup;
  }

  @PropertyDesc(i18n = "zref.property")
  public final double getZref() {
    return zref;
  }

  public final void setZref(double newZref) {
    zref = newZref;
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    return getClass().getSimpleName();
  }
}
