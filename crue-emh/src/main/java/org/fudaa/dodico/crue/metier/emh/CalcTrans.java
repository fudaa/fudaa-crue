package org.fudaa.dodico.crue.metier.emh;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CalcTrans extends Calc {


  public CalcTrans() {
    super(getDclmClassInOrder());
  }


  @Override
  public boolean isTransitoire() {
    return true;
  }



  @Override
  public CalcTrans deepClone() {
    try {
      CalcTrans trans = (CalcTrans) clone();
      trans.container = container.deepClone();
      trans.container.setCalculParent(trans);
      return trans;
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcPseudoPerm.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("why");
  }


  /**
   * Doit suivre l'ordre du DCLM
   */
  public static List<Class> getDclmClassInOrder() {
    return Arrays.asList(
        CalcTransNoeudQapp.class,
        CalcTransNoeudQappExt.class,
        CalcTransNoeudNiveauContinuLimnigramme.class,
        CalcTransNoeudNiveauContinuTarage.class,
        CalcTransBrancheOrificeManoeuvre.class,
        CalcTransBrancheSaintVenantQruis.class,
        CalcTransCasierProfilQruis.class,
        CalcTransBrancheOrificeManoeuvreRegul.class,
        CalcTransNoeudBg1.class,
        CalcTransNoeudBg2.class,
        CalcTransNoeudUsine.class,
        CalcTransNoeudBg1Av.class,
        CalcTransNoeudBg2Av.class
    );
  }



}
