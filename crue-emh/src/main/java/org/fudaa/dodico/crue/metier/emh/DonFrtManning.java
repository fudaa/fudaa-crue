package org.fudaa.dodico.crue.metier.emh;

public class DonFrtManning extends DonFrt {
  private LoiFF loiZN;

  public LoiFF getManning() {
    return loiZN;
  }

  @Override
  public LoiFF getLoi() {
    return getManning();
  }

  @Override
  public DonFrtManning deepClone() {
    try {
      final DonFrtManning res = (DonFrtManning) clone();
      if (res.loiZN != null) {
        res.loiZN = loiZN.clonedWithoutUser();
        res.loiZN.register(res);
      }
      return res;
    } catch (final CloneNotSupportedException cloneNotSupportedException) {
    }
    return null;
  }

  public void setManning(final LoiFF newManning) {
    if (this.loiZN != null) {
      this.loiZN.unregister(this);
    }

    loiZN = newManning;
    this.loiZN.register(this);
  }
}
