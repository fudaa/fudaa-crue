/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.crue.metier.etude;

import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;

import java.util.List;
import java.util.Map;

/**
 * @author deniger
 */
public class ManagerEMHSousModele extends ManagerEMHContainerBase {
  public ManagerEMHSousModele() {
    super(CrueLevelType.SOUS_MODELE);
  }

  public ManagerEMHSousModele(final String nom) {
    super(CrueLevelType.SOUS_MODELE, nom);
  }

  public String getEMHType() {
    return EMHSousModele.class.getSimpleName();
  }

  protected void addAllFile(final Map<String, FichierCrueParModele> dest, final ManagerEMHModeleBase parent) {
    addAllFile(this, dest, parent);
  }

  protected static void addAllFile(ManagerEMHContainerBase from, final Map<String, FichierCrueParModele> fichierCrueByName,
                                   final ManagerEMHModeleBase parent) {
    if (from.getListeFichiers() != null) {
      final List<FichierCrue> fics = from.getListeFichiers().getFichiers();
      for (final FichierCrue fichierCrue : fics) {
        fichierCrueByName.put(fichierCrue.getNom(), new FichierCrueParModele(fichierCrue, parent));
      }
    }
  }
}
