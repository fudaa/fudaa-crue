/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.crue.metier.emh;

/**
 * Relation qui permet de connaitre la branche contenant une section noeud
 * 
 * @author deniger
 */
public final class RelationEMHBrancheContientSection extends RelationEMH<CatEMHBranche> {

  /**
   * @param br
   */
  public RelationEMHBrancheContientSection(CatEMHBranche br) {
    super.setEmh(br);
  }

  @Override
  public RelationEMH<CatEMHBranche> copyWithEMH(CatEMHBranche copiedEMH) {
    return new RelationEMHBrancheContientSection(copiedEMH);
  }

}
