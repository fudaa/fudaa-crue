/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.factory;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author deniger
 */
public final class EMHRelationFactory {
  /**
   * @param isSaintVenant true si branche de saintVenant.
   * @param section la section
   * @return la relation correctement initialisée
   */
  public static RelationEMHSectionDansBranche createSectionDansBrancheAndSetBrancheContientSection(final CatEMHBranche branche,
                                                                                                   final boolean isSaintVenant, final CatEMHSection section,
                                                                                                   final CrueConfigMetier defaults) {
    final RelationEMHSectionDansBranche res = createSectionDansBranche(isSaintVenant, defaults, section);
    addBrancheContientSection(branche, section);
    return res;
  }

  public static void addBrancheContientSection(final CatEMHBranche branche, final CatEMHSection section) {
    section.addRelationEMH(new RelationEMHBrancheContientSection(branche));
  }

  /**
   * @param nd le noeud pointe
   * @return la relation du noeud amond
   */
  static RelationEMHNoeudDansBranche createNoeudAmont(final CatEMHNoeud nd) {
    final RelationEMHNoeudDansBranche res = createRelationNoeud(nd);
    res.setAmont(true);
    return res;
  }

  /**
   * @param nd le noeud pointe
   * @return la relation du noeud amond
   */
  static RelationEMHNoeudDansBranche createNoeudAval(final CatEMHNoeud nd) {
    final RelationEMHNoeudDansBranche res = createRelationNoeud(nd);
    res.setAval(true);
    return res;
  }

  private static RelationEMHNoeudDansBranche createRelationNoeud(final CatEMHNoeud nd) {
    final RelationEMHNoeudDansBranche res = new RelationEMHNoeudDansBranche();
    res.setEmh(nd);
    return res;
  }

  /**
   * @param casier le casier de dest
   * @return une nouvelle relation casier dans noeud
   */
  public static RelationEMHCasierDansNoeud createCasierDansNoeud(final CatEMHCasier casier) {
    final RelationEMHCasierDansNoeud relationEMHCasierDansNoeud = new RelationEMHCasierDansNoeud();
    relationEMHCasierDansNoeud.setEmh(casier);
    return relationEMHCasierDansNoeud;
  }

  /**
   * @param noeud le noeud de dest
   * @return une nouvelle relation noeud contient casier
   */
  public static RelationEMHNoeudContientCasier createNoeudContientCasier(final CatEMHNoeud noeud) {
    final RelationEMHNoeudContientCasier relationEMHNoeudContientCasier = new RelationEMHNoeudContientCasier();
    relationEMHNoeudContientCasier.setEmh(noeud);
    return relationEMHNoeudContientCasier;
  }

  /**
   * @param section la section de ref
   * @return la relation RelationEMHSectionDansSectionIdem contenant la section
   */
  public static RelationEMH createSectionRef(final CatEMHSection section) {
    final RelationEMHSectionDansSectionIdem res = new RelationEMHSectionDansSectionIdem();
    res.setEmh(section);
    return res;
  }

  /**
   * @param emh la section de ref
   * @return la relation RelationEMHSectionDansSectionIdem contenant la section
   */
  public static RelationEMH createRelationContient(final EMH emh) {
    final RelationEMHContient res = new RelationEMHContient();
    res.setEmh(emh);
    return res;
  }

  public static void addNoeudAmont(final CatEMHBranche emh, final CatEMHNoeud noeud) {
    emh.addRelationEMH(createNoeudAmont(noeud));
    noeud.addRelationEMH(new RelationEMHBrancheContientNoeud(emh));
  }

  public static void addNoeudAval(final CatEMHBranche emh, final CatEMHNoeud noeud) {
    emh.addRelationEMH(createNoeudAval(noeud));
    noeud.addRelationEMH(new RelationEMHBrancheContientNoeud(emh));
  }

  public static List<RelationEMH> findRelationWithTarget(final EMH container, final EMH toFound) {
    final List<RelationEMH> relationEMHs = container.getRelationEMH();
    return findRelationWithTarget(toFound, relationEMHs);
  }

  /**
   * Enleve les relations vers toFound contenus par l'objet container
   */
  public static void removeRelation(final EMH container, final EMH toFound) {
    if (container == null) {
      return;
    }
    final List<RelationEMH> toRemove = findRelationWithTarget(container, toFound);
    if (!toRemove.isEmpty()) {
      container.removeAllRelations(toRemove);
    }
  }

  /**
   * Enleve les relations entre les 2 EMHS.
   */
  public static void removeRelationBidirect(final EMH container, final EMH toFound) {
    removeRelation(container, toFound);
    removeRelation(toFound, container);
  }

  public static void addNoeudCasier(final CatEMHCasier casier, final CatEMHNoeud noeud) {
    final RelationEMH noeudContientCasier = createNoeudContientCasier(noeud);
    casier.addRelationEMH(noeudContientCasier);
    final RelationEMH casierDansNoeud = createCasierDansNoeud(casier);
    noeud.addRelationEMH(casierDansNoeud);
  }

  /**
   * Doit être appele avec parcimonie...
   *
   * @param modele le modele contenant
   * @param contenu l'EMH a ajouter
   */
  public static void addRelationContientEMH(final EMHSousModele modele, final EMH contenu) {
    modele.addRelationEMH(createRelationContient(contenu));
    contenu.addRelationEMH(new RelationEMHDansSousModele(modele));
  }

  /**
   * @param scenario le scenario contenant
   * @param modele le modele contenu
   */
  public static void addRelationContientEMH(final EMHScenario scenario, final EMHModeleBase modele) {
    scenario.addRelationEMH(createRelationContient(modele));
    modele.addRelationEMH(new RelationEMHModeleDansScenario(scenario));
  }

  /**
   * @param modele
   * @param ssModele
   */
  public static void addRelationContientEMH(final EMHModeleBase modele, final EMHSousModele ssModele) {
    modele.addRelationEMH(createRelationContient(ssModele));
    ssModele.addRelationEMH(new RelationEMHSousModeleDansModele(modele));
  }

  public static void addSectionRef(final EMHSectionIdem emh, final CatEMHSection section) {
    emh.addRelationEMH(createSectionRef(section));
    final RelationEMHSectionIdemContientSection referenced = new RelationEMHSectionIdemContientSection();
    referenced.setEmh(emh);
    section.addRelationEMH(referenced);
  }

  public static void addSectionPilote(final CatEMHBranche emh, final CatEMHSection section) {
    final RelationEMHSectionPiloteDansBranche pilote = new RelationEMHSectionPiloteDansBranche();
    pilote.setEmh(section);
    emh.addRelationEMH(pilote);
    final RelationEMHBrancheContientSectionPilote branchePilote = new RelationEMHBrancheContientSectionPilote(emh);
    section.addRelationEMH(branchePilote);
  }

  public static CatEMHSection removeSectionPilote(final CatEMHBranche branche) {
    final CatEMHSection sectionPilote = EMHHelper.getSectionPilote(branche);
    if (sectionPilote != null) {
      final List<RelationEMHSectionPiloteDansBranche> brancheRelationsToRemoce = EMHHelper.selectClass(branche.getRelationEMH(),
        RelationEMHSectionPiloteDansBranche.class);
      branche.removeAllRelations(brancheRelationsToRemoce);
      final List<RelationEMHBrancheContientSectionPilote> sectionRelationsToRemove = EMHHelper.selectClass(sectionPilote.getRelationEMH(),
        RelationEMHBrancheContientSectionPilote.class);
      sectionPilote.removeAllRelations(sectionRelationsToRemove);
    }
    return sectionPilote;
  }

  public static void removeRelationBrancheDansSectionBidirect(final CatEMHSection section) {
    if (section.getBranche() != null) {
      final CatEMHBranche branche = section.getBranche();
      final RelationEMHBrancheContientSection toRemove = EMHHelper.selectFirstOfClass(section.getRelationEMH(),
        RelationEMHBrancheContientSection.class);
      section.removeRelationEMH(toRemove);
      final List<RelationEMHSectionDansBranche> sections = branche.getSections();
      final List<RelationEMH> findRelationWithTarget = findRelationWithTarget(section, sections);
      if (!findRelationWithTarget.isEmpty()) {
        assert findRelationWithTarget.size() == 1;
      }
      branche.removeAllRelations(findRelationWithTarget);
    }
  }

  public static double getXP(final CatEMHSection section) {
    if (section.getBranche() != null) {
      final CatEMHBranche branche = section.getBranche();
      final List<RelationEMH> findRelationWithTarget = findRelationWithTarget(section, branche.getSections());
      if (!findRelationWithTarget.isEmpty()) {
        assert findRelationWithTarget.size() == 1;
        return ((RelationEMHSectionDansBranche) findRelationWithTarget.get(0)).getXp();
      }
    }
    return -99999;
  }

  /**
   * @param modele le modele contenant
   * @param contenu la liste d'EMH contenus a ajouter
   */
  public static void addRelationsContientEMH(final EMHSousModele modele, final List<? extends EMH> contenu) {
    for (final EMH emh : contenu) {
      addRelationContientEMH(modele, emh);
    }
  }

  public static EMHSectionInterpolee createSectionInterpoleFromDistmax(final double xpos, final String brancheNom, final double distmax) {
    NumberFormat fmt = EMHFactory.FMT_DISTMAX;
    if (distmax < 10) {
      fmt = EMHFactory.FMT_DISTMAX_FAIBLE;
    }
    final EMHSectionInterpolee interpol = new EMHSectionInterpolee(
      CruePrefix.changePrefix(brancheNom, CruePrefix.P_BRANCHE, CruePrefix.P_SECTION) + "_" + fmt.format(xpos));
    return interpol;
  }

  public static RelationEMHSectionDansBranche createSectionDansBrancheAndSetBrancheContientSection(final CatEMHBranche branche,
                                                                                                   final CatEMHSection section, final double xp,
                                                                                                   final double coefPond, final double coefConv,
                                                                                                   final double coefDiv, final CrueConfigMetier defaults) {
    final boolean isSaintVenant = branche instanceof EMHBrancheSaintVenant;
    final RelationEMHSectionDansBranche relation = EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(branche, isSaintVenant,
      section, defaults);
    relation.setXp(xp);
    relation.setPos(EnumPosSection.INTERNE);
    if (isSaintVenant) {
      final RelationEMHSectionDansBrancheSaintVenant stV = (RelationEMHSectionDansBrancheSaintVenant) relation;
      stV.setCoefConv(coefConv);
      stV.setCoefPond(coefPond);
      stV.setCoefDiv(coefDiv);
    }
    return relation;
  }

  /**
   * Attention, n'ajoute pas la relation à la branche mais ajoute la relation sectionDansBranche
   */
  public static RelationEMHSectionDansBranche createSectionDansBrancheAndSetBrancheContientSection(final CatEMHBranche branche,
                                                                                                   final CatEMHSection section, final double xp,
                                                                                                   final CrueConfigMetier defaults, final boolean isSaintVenant) {
    final RelationEMHSectionDansBranche relation = createSectionDansBrancheAndSetBrancheContientSection(branche, isSaintVenant,
      section, defaults);
    relation.setXp(xp);
    relation.setPos(EnumPosSection.INTERNE);
    return relation;
  }

  /**
   * Attention, n'ajoute pas la relation à la branche.
   */
  public static RelationEMHSectionDansBranche createSectionDansBrancheAndSetRelation(final CatEMHBranche branche, final CatEMHSection section,
                                                                                     final double xp, final CrueConfigMetier defaults) {
    final boolean isSaintVenant = EnumBrancheType.EMHBrancheSaintVenant.equals(branche.getBrancheType());
    return createSectionDansBrancheAndSetBrancheContientSection(branche, section, xp, defaults, isSaintVenant);
  }

  /**
   * Attention, n'ajoute pas la relation à la branche.
   */
  public static RelationEMHSectionDansBranche createSectionDansBrancheAndSetBrancheContientSection(final CatEMHBranche branche,
                                                                                                   final CatEMHSection section,
                                                                                                   final double xp, final EnumPosSection position,
                                                                                                   final CrueConfigMetier defaults) {
    final boolean isSaintVenant = branche instanceof EMHBrancheSaintVenant;
    final RelationEMHSectionDansBranche relation = createSectionDansBrancheAndSetBrancheContientSection(branche, isSaintVenant,
      section, defaults);
    relation.setXp(xp);
    relation.setPos(position);
    return relation;
  }

  /**
   * Crée uniquement la relation
   */
  public static RelationEMHSectionDansBranche createSectionDansBranche(final boolean isSaintVenant, final CrueConfigMetier defaults,
                                                                       final CatEMHSection section) {
    final RelationEMHSectionDansBranche res = isSaintVenant ? new RelationEMHSectionDansBrancheSaintVenant(defaults)
      : new RelationEMHSectionDansBranche();
    res.setEmh(section);
    return res;
  }

  public static RelationEMHSectionDansBranche createSectionDansBranche(final CatEMHBranche branche, final CrueConfigMetier defaults,
                                                                       final CatEMHSection section) {
    final boolean isSaintVenant = EnumBrancheType.EMHBrancheSaintVenant.equals(branche.getBrancheType());
    return createSectionDansBranche(isSaintVenant, defaults, section);
  }

  public static List<RelationEMH> findRelationWithTarget(final EMH toFound,
                                                         final List<? extends RelationEMH> relationEMHs) {
    final List<RelationEMH> found = new ArrayList<>();
    final Long uid = toFound.getUiId();
    for (final RelationEMH relationEMH : relationEMHs) {
      if (relationEMH.getEmh().getUiId().equals(uid)) {
        found.add(relationEMH);
      }
    }
    return found;
  }
}
