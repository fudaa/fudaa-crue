package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.contrat.ObjetWithID;
import org.fudaa.dodico.crue.metier.CrueFileType;

public class CommentaireItem implements ObjetWithID {

  /**
   * @param fileType
   * @param commentaire
   */
  public CommentaireItem(CrueFileType fileType, String commentaire) {
    super();
    this.fileType = fileType;
    this.commentaire = commentaire;
  }

  private final CrueFileType fileType;
  private final String commentaire;

  public CrueFileType getFileType() {
    return fileType;
  }

  @Override
  public String getId() {
    return fileType.name();
  }
  
  @Override
  public String getNom() {
    return getId();
  }

  public String getCommentaire() {
    return commentaire;
  }

}
