/**
 * *********************************************************************
 * Module: ValParamEntier.java Author: deniger Purpose: Defines the Class ValParamEntier
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

public class ValParamEntier extends ValParam {
  private int valeur;

  @PropertyDesc(i18n = "valParamValeur.property")
  public final int getValeur() {
    return valeur;
  }

  public final void setValeur(int newValeur) {
    valeur = newValeur;
  }

  @Override
  public Object getValeurObjet() {
    return valeur;
  }

  public ValParamEntier(final String nom, final int valeur) {
    super(nom);
    this.valeur = valeur;
  }

  /**
   *
   * @return le type de donnée portée par ce ValParam
   *
   */
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public Class getTypeData() {
    return Integer.TYPE;
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    return "ValParamEntier[ " + valeur + " ]";
  }
}
