/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.factory;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.compress.utils.Sets;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.*;

/**
 * TODO a continuer
 *
 * @author Frederic Deniger
 */
public class DclmFactory {

  private static final Collection<Class<? extends DonCLimM>> NON_EXPORTABLE_DCLM = Sets.newHashSet(CalcTransNoeudQappExt.class, CalcTransBrancheOrificeManoeuvreRegul.class
      , CalcTransNoeudBg1.class, CalcTransNoeudBg2.class, CalcTransNoeudUsine.class, CalcTransNoeudBg1Av.class, CalcTransNoeudBg2Av.class
      , CalcPseudoPermNoeudBg1.class, CalcPseudoPermNoeudBg1Av.class, CalcPseudoPermNoeudBg2.class, CalcPseudoPermNoeudBg2Av.class, CalcPseudoPermNoeudUsi.class, CalcPseudoPermBrancheOrificeManoeuvreRegul.class);

  public static Collection<Class<? extends DonCLimM>> getNonExportableDclm() {
    return NON_EXPORTABLE_DCLM;
  }

  public static DclmFactory.CalcBuilder findCreator(List<DclmFactory.CalcBuilder> creators, DonCLimM clim) {
    EMH emh = clim.getEmh();
    for (CalcBuilder calcBuilder : creators) {
      if (calcBuilder.isAccepted(emh) && calcBuilder.isCreatorFor(clim)) {
        return calcBuilder;
      }
    }
    return null;
  }

  public static List<CalcBuilder> getPseudoPerm() {
    return Arrays.asList(
        new CalcPseudoPermNoeudQappCreator(),
        new CalcPseudoPermNoeudNiveauContinuZimpCreator(),
        new CalcPseudoPermBrancheOrificeManoeuvreCreator(),
        new CalcPseudoPermBrancheSaintVenantQruisCreator(),
        new CalcPseudoPermCasierProfilQruisCreator(),
        new CalcPseudoPermNoeudBg1Creator(),
        new CalcPseudoPermNoeudBg1AvCreator(),
        new CalcPseudoPermNoeudBg2Creator(),
        new CalcPseudoPermNoeudBg2AvCreator(),
        new CalcPseudoPermNoeudUsiCreator(),
        new CalcPseudoPermBrancheOrificeManoeuvreRegulCreator()

    );
  }

  public static List<CalcBuilder> getPseudoPermSens() {
    return Arrays.asList(
        new CalcPseudoPermNoeudQappCreator(),
        new CalcPseudoPermNoeudNiveauContinuZimpCreator(),
        new CalcPseudoPermBrancheOrificeManoeuvreCreatorSensVersBas(),
        new CalcPseudoPermBrancheOrificeManoeuvreCreatorSensVersHaut(),
        new CalcPseudoPermBrancheOrificeManoeuvreRegulCreatorSensVersBas(),
        new CalcPseudoPermBrancheOrificeManoeuvreRegulCreatorSensVersHaut(),
        new CalcPseudoPermBrancheSaintVenantQruisCreator(),
        new CalcPseudoPermCasierProfilQruisCreator(),
        new CalcPseudoPermNoeudBg1Creator(),
        new CalcPseudoPermNoeudBg1AvCreator(),
        new CalcPseudoPermNoeudBg2Creator(),
        new CalcPseudoPermNoeudBg2AvCreator(),
        new CalcPseudoPermNoeudUsiCreator()

    );
  }

  public static List<CalcBuilder> getTrans() {
    return Arrays.asList(
        new CalcTransNoeudQappCreator(),
        new CalcTransNoeudQappExtCreator(),
        new CalcTransNoeudNiveauContinuLimnigrammeCreator(),
        new CalcTransNoeudNiveauContinuTarageCreator(),
        new CalcTransBrancheOrificeManoeuvreCreator(),
        new CalcTransBrancheSaintVenantQruisCreator(),
        new CalcTransCasierProfilQruisCreator(),
        new CalcTransBrancheOrificeManoeuvreRegulCreator(),
        new CalcTransNoeudBg1Creator(),
        new CalcTransNoeudBg2Creator(),
        new CalcTransNoeudUsineCreator(),
        new CalcTransNoeudBg1AvCreator(),
        new CalcTransNoeudBg2AvCreator()
    );
  }

  public static List<CalcBuilder> getTransSens() {
    return Arrays.asList(
        new CalcTransNoeudQappCreator(),
        new CalcTransNoeudQappExtCreator(),
        new CalcTransNoeudNiveauContinuLimnigrammeCreator(),
        new CalcTransNoeudNiveauContinuTarageCreator(),
        new CalcTransBrancheOrificeManoeuvreCreatorSensVersBas(),
        new CalcTransBrancheOrificeManoeuvreCreatorSensVersHaut(),
        new CalcTransBrancheSaintVenantQruisCreator(),
        new CalcTransCasierProfilQruisCreator(),
        new CalcTransBrancheOrificeManoeuvreRegulCreatorSensVersBas(),
        new CalcTransBrancheOrificeManoeuvreRegulCreatorSensVersHaut(),
        new CalcTransNoeudBg1Creator(),
        new CalcTransNoeudBg2Creator(),
        new CalcTransNoeudUsineCreator(),
        new CalcTransNoeudBg1AvCreator(),
        new CalcTransNoeudBg2AvCreator()
    );
  }

  public static List<CalcBuilder> getCreators(Calc calc) {
    return calc.isPermanent() ? getPseudoPerm() : getTrans();
  }

  public static List<CalcBuilder> getCreatorsSens(Calc calc) {
    return calc.isPermanent() ? getPseudoPermSens() : getTransSens();
  }

  /**
   * cree la dclm, mais ne l'ajoute pas: ni au calcul parent ni à l'EMH.
   *
   * @param creator
   * @param ccm
   * @param emh
   * @param parent
   * @return
   */
  public static DonCLimMCommonItem create(CalcBuilder creator, CrueConfigMetier ccm, EMH emh, Calc parent) {
    DonCLimMCommonItem create = creator.create(ccm);
    create.setEmh(emh);
    create.setCalculParent(parent);
    return create;
  }

  private static class CalcTransNoeudQappCreator extends CalcBuilder<CalcTransNoeudQapp> {

    public CalcTransNoeudQappCreator() {
      super(CalcTransNoeudQapp.class);
    }

    @Override
    public CalcTransNoeudQapp create(CrueConfigMetier ccm) {
      return new CalcTransNoeudQapp();
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isNoeud(emh);
    }
  }

  private static class CalcTransNoeudBg1Creator extends CalcBuilder<CalcTransNoeudBg1> {

    public CalcTransNoeudBg1Creator() {
      super(CalcTransNoeudBg1.class);
    }

    @Override
    public CalcTransNoeudBg1 create(CrueConfigMetier ccm) {
      return new CalcTransNoeudBg1();
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isNoeud(emh);
    }
  }

  private static class CalcTransNoeudBg2Creator extends CalcBuilder<CalcTransNoeudBg2> {

    public CalcTransNoeudBg2Creator() {
      super(CalcTransNoeudBg2.class);
    }

    @Override
    public CalcTransNoeudBg2 create(CrueConfigMetier ccm) {
      return new CalcTransNoeudBg2();
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isNoeud(emh);
    }
  }

  private static class CalcTransNoeudBg1AvCreator extends CalcBuilder<CalcTransNoeudBg1Av> {

    public CalcTransNoeudBg1AvCreator() {
      super(CalcTransNoeudBg1Av.class);
    }

    @Override
    public CalcTransNoeudBg1Av create(CrueConfigMetier ccm) {
      return new CalcTransNoeudBg1Av();
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isNoeud(emh);
    }
  }

  private static class CalcTransNoeudBg2AvCreator extends CalcBuilder<CalcTransNoeudBg2Av> {

    public CalcTransNoeudBg2AvCreator() {
      super(CalcTransNoeudBg2Av.class);
    }

    @Override
    public CalcTransNoeudBg2Av create(CrueConfigMetier ccm) {
      return new CalcTransNoeudBg2Av();
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isNoeud(emh);
    }
  }

  private static class CalcTransNoeudUsineCreator extends CalcBuilder<CalcTransNoeudUsine> {

    public CalcTransNoeudUsineCreator() {
      super(CalcTransNoeudUsine.class);
    }

    @Override
    public CalcTransNoeudUsine create(CrueConfigMetier ccm) {
      return new CalcTransNoeudUsine();
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isNoeud(emh);
    }
  }

  private static class CalcTransNoeudQappExtCreator extends CalcBuilder<CalcTransNoeudQappExt> {

    public CalcTransNoeudQappExtCreator() {
      super(CalcTransNoeudQappExt.class);
    }

    @Override
    public CalcTransNoeudQappExt create(CrueConfigMetier ccm) {
      return new CalcTransNoeudQappExt();
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isNoeud(emh);
    }
  }

  private static class CalcTransNoeudNiveauContinuLimnigrammeCreator extends CalcBuilder<CalcTransNoeudNiveauContinuLimnigramme> {

    public CalcTransNoeudNiveauContinuLimnigrammeCreator() {
      super(CalcTransNoeudNiveauContinuLimnigramme.class);
    }

    @Override
    public CalcTransNoeudNiveauContinuLimnigramme create(CrueConfigMetier ccm) {
      return new CalcTransNoeudNiveauContinuLimnigramme();
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isNoeudNiveauContinu(emh);
    }
  }

  private static class CalcTransNoeudNiveauContinuTarageCreator extends CalcBuilder<CalcTransNoeudNiveauContinuTarage> {

    public CalcTransNoeudNiveauContinuTarageCreator() {
      super(CalcTransNoeudNiveauContinuTarage.class);
    }

    @Override
    public CalcTransNoeudNiveauContinuTarage create(CrueConfigMetier ccm) {
      return new CalcTransNoeudNiveauContinuTarage();
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isNoeudNiveauContinu(emh);
    }
  }

  public static class CalcTransBrancheOrificeManoeuvreCreator extends CalcBuilder<CalcTransBrancheOrificeManoeuvre> {

    public CalcTransBrancheOrificeManoeuvreCreator() {
      super(CalcTransBrancheOrificeManoeuvre.class);
    }

    @Override
    public CalcTransBrancheOrificeManoeuvre create(CrueConfigMetier ccm) {
      return new CalcTransBrancheOrificeManoeuvre(ccm);
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isBrancheOrifice(emh);
    }
  }

  public static class CalcTransBrancheOrificeManoeuvreRegulCreator extends CalcBuilder<CalcTransBrancheOrificeManoeuvreRegul> {

    public CalcTransBrancheOrificeManoeuvreRegulCreator() {
      super(CalcTransBrancheOrificeManoeuvreRegul.class);
    }

    @Override
    public CalcTransBrancheOrificeManoeuvreRegul create(CrueConfigMetier ccm) {
      return new CalcTransBrancheOrificeManoeuvreRegul(ccm);
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isBrancheOrifice(emh);
    }
  }

  private static class CalcTransBrancheSaintVenantQruisCreator extends CalcBuilder<CalcTransBrancheSaintVenantQruis> {

    public CalcTransBrancheSaintVenantQruisCreator() {
      super(CalcTransBrancheSaintVenantQruis.class);
    }

    @Override
    public CalcTransBrancheSaintVenantQruis create(CrueConfigMetier ccm) {
      return new CalcTransBrancheSaintVenantQruis();
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isBrancheSaintVenant(emh);
    }
  }

  private static class CalcTransCasierProfilQruisCreator extends CalcBuilder<CalcTransCasierProfilQruis> {

    public CalcTransCasierProfilQruisCreator() {
      super(CalcTransCasierProfilQruis.class);
    }

    @Override
    public CalcTransCasierProfilQruis create(CrueConfigMetier ccm) {
      return new CalcTransCasierProfilQruis();
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isCasierProfil(emh);
    }
  }

  public static abstract class CalcBuilder<T extends DonCLimMCommonItem> implements ObjetNomme, ToStringInternationalizable {

    String nom;
    String shortNom;
    final Class<T> dclmClass;

    public CalcBuilder(Class<T> dclmClass) {
      this.dclmClass = dclmClass;
      this.nom = BusinessMessages.getString(dclmClass.getSimpleName() + ".builderName");
      this.shortNom = BusinessMessages.getString(dclmClass.getSimpleName() + ".builderShortName");
    }

    @Override
    public String geti18n() {
      return shortNom;
    }

    public String getShortName() {
      return shortNom;
    }

    @Override
    public String geti18nLongName() {
      return getNom();
    }

    @Override
    public String getId() {
      return getNom();
    }

    @Override
    public void setNom(String newNom) {
    }

    public abstract T create(CrueConfigMetier ccm);

    public abstract boolean isAccepted(EMH emh);

    public Class<T> getDclmClass() {
      return dclmClass;
    }

    public boolean isCreatorFor(DonCLimM item) {
      return dclmClass.equals(item.getClass());
    }

    @Override
    public String getNom() {
      return nom;
    }
  }

  public static class CalcPseudoPermBrancheOrificeManoeuvreCreator extends CalcBuilder<CalcPseudoPermBrancheOrificeManoeuvre> {

    public CalcPseudoPermBrancheOrificeManoeuvreCreator() {
      super(CalcPseudoPermBrancheOrificeManoeuvre.class);
    }

    @Override
    public CalcPseudoPermBrancheOrificeManoeuvre create(CrueConfigMetier ccm) {
      return new CalcPseudoPermBrancheOrificeManoeuvre(ccm);
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isBrancheOrifice(emh);
    }
  }


  public static class CalcPseudoPermBrancheOrificeManoeuvreCreatorSensVersHaut extends CalcPseudoPermBrancheOrificeManoeuvreCreator {

    public CalcPseudoPermBrancheOrificeManoeuvreCreatorSensVersHaut() {
      nom = nom + " " + EnumSensOuv.OUV_VERS_HAUT.geti18n();
      shortNom = shortNom + " " + EnumSensOuv.OUV_VERS_HAUT.geti18n();
    }

    @Override
    public CalcPseudoPermBrancheOrificeManoeuvre create(CrueConfigMetier ccm) {
      final CalcPseudoPermBrancheOrificeManoeuvre create = super.create(ccm);
      create.setSensOuv(EnumSensOuv.OUV_VERS_HAUT);
      return create;
    }

    @Override
    public boolean isCreatorFor(DonCLimM item) {
      return super.isCreatorFor(item) && EnumSensOuv.OUV_VERS_HAUT.equals(((CalcPseudoPermBrancheOrificeManoeuvre) item).getSensOuv());
    }
  }

  public static class CalcPseudoPermBrancheOrificeManoeuvreCreatorSensVersBas extends CalcPseudoPermBrancheOrificeManoeuvreCreator {

    public CalcPseudoPermBrancheOrificeManoeuvreCreatorSensVersBas() {
      nom = nom + " " + EnumSensOuv.OUV_VERS_BAS.geti18n();
      shortNom = shortNom + " " + EnumSensOuv.OUV_VERS_BAS.geti18n();
    }

    @Override
    public CalcPseudoPermBrancheOrificeManoeuvre create(CrueConfigMetier ccm) {
      final CalcPseudoPermBrancheOrificeManoeuvre create = super.create(ccm);
      create.setSensOuv(EnumSensOuv.OUV_VERS_BAS);
      return create;
    }

    @Override
    public boolean isCreatorFor(DonCLimM item) {
      return super.isCreatorFor(item) && EnumSensOuv.OUV_VERS_BAS.equals(((CalcPseudoPermBrancheOrificeManoeuvre) item).getSensOuv());
    }
  }

  public static class CalcPseudoPermBrancheOrificeManoeuvreRegulCreator extends CalcBuilder<CalcPseudoPermBrancheOrificeManoeuvreRegul> {

    public CalcPseudoPermBrancheOrificeManoeuvreRegulCreator() {
      super(CalcPseudoPermBrancheOrificeManoeuvreRegul.class);
    }

    @Override
    public CalcPseudoPermBrancheOrificeManoeuvreRegul create(CrueConfigMetier ccm) {
      return new CalcPseudoPermBrancheOrificeManoeuvreRegul(ccm);
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isBrancheOrifice(emh);
    }
  }

  public static class CalcPseudoPermBrancheOrificeManoeuvreRegulCreatorSensVersHaut extends CalcPseudoPermBrancheOrificeManoeuvreRegulCreator {

    public CalcPseudoPermBrancheOrificeManoeuvreRegulCreatorSensVersHaut() {
      nom = nom + " " + EnumSensOuv.OUV_VERS_HAUT.geti18n();
      shortNom = shortNom + " " + EnumSensOuv.OUV_VERS_HAUT.geti18n();
    }

    @Override
    public CalcPseudoPermBrancheOrificeManoeuvreRegul create(CrueConfigMetier ccm) {
      CalcPseudoPermBrancheOrificeManoeuvreRegul create = new CalcPseudoPermBrancheOrificeManoeuvreRegul(ccm);
      create.setSensOuv(EnumSensOuv.OUV_VERS_HAUT);
      return create;
    }


    @Override
    public boolean isCreatorFor(DonCLimM item) {
      return super.isCreatorFor(item) && EnumSensOuv.OUV_VERS_HAUT.equals(((CalcPseudoPermBrancheOrificeManoeuvreRegul) item).getSensOuv());
    }
  }

  public static class CalcPseudoPermBrancheOrificeManoeuvreRegulCreatorSensVersBas extends CalcPseudoPermBrancheOrificeManoeuvreRegulCreator {

    public CalcPseudoPermBrancheOrificeManoeuvreRegulCreatorSensVersBas() {
      nom = nom + " " + EnumSensOuv.OUV_VERS_BAS.geti18n();
      shortNom = shortNom + " " + EnumSensOuv.OUV_VERS_BAS.geti18n();
    }

    @Override
    public CalcPseudoPermBrancheOrificeManoeuvreRegul create(CrueConfigMetier ccm) {
      CalcPseudoPermBrancheOrificeManoeuvreRegul create = new CalcPseudoPermBrancheOrificeManoeuvreRegul(ccm);
      create.setSensOuv(EnumSensOuv.OUV_VERS_BAS);
      return create;
    }


    @Override
    public boolean isCreatorFor(DonCLimM item) {
      return super.isCreatorFor(item) && EnumSensOuv.OUV_VERS_BAS.equals(((CalcPseudoPermBrancheOrificeManoeuvreRegul) item).getSensOuv());
    }
  }

  public static class CalcTransBrancheOrificeManoeuvreCreatorSensVersHaut extends CalcTransBrancheOrificeManoeuvreCreator {

    public CalcTransBrancheOrificeManoeuvreCreatorSensVersHaut() {
      nom = nom + " " + EnumSensOuv.OUV_VERS_HAUT.geti18n();
      shortNom = shortNom + " " + EnumSensOuv.OUV_VERS_HAUT.geti18n();
    }

    @Override
    public CalcTransBrancheOrificeManoeuvre create(CrueConfigMetier ccm) {
      final CalcTransBrancheOrificeManoeuvre create = super.create(ccm);
      create.setSensOuv(EnumSensOuv.OUV_VERS_HAUT);
      return create;
    }

    @Override
    public boolean isCreatorFor(DonCLimM item) {
      return super.isCreatorFor(item) && EnumSensOuv.OUV_VERS_HAUT.equals(((CalcTransBrancheOrificeManoeuvre) item).getSensOuv());
    }
  }

  public static class CalcTransBrancheOrificeManoeuvreRegulCreatorSensVersHaut extends CalcTransBrancheOrificeManoeuvreRegulCreator {

    public CalcTransBrancheOrificeManoeuvreRegulCreatorSensVersHaut() {
      nom = nom + " " + EnumSensOuv.OUV_VERS_HAUT.geti18n();
      shortNom = shortNom + " " + EnumSensOuv.OUV_VERS_HAUT.geti18n();
    }

    @Override
    public CalcTransBrancheOrificeManoeuvreRegul create(CrueConfigMetier ccm) {
      final CalcTransBrancheOrificeManoeuvreRegul create = super.create(ccm);
      create.setSensOuv(EnumSensOuv.OUV_VERS_HAUT);
      return create;
    }

    @Override
    public boolean isCreatorFor(DonCLimM item) {
      return super.isCreatorFor(item) && EnumSensOuv.OUV_VERS_HAUT.equals(((CalcTransBrancheOrificeManoeuvreRegul) item).getSensOuv());
    }
  }

  public static class CalcTransBrancheOrificeManoeuvreCreatorSensVersBas extends CalcTransBrancheOrificeManoeuvreCreator {

    public CalcTransBrancheOrificeManoeuvreCreatorSensVersBas() {
      nom = nom + " " + EnumSensOuv.OUV_VERS_BAS.geti18n();
      shortNom = shortNom + " " + EnumSensOuv.OUV_VERS_BAS.geti18n();
    }

    @Override
    public CalcTransBrancheOrificeManoeuvre create(CrueConfigMetier ccm) {
      final CalcTransBrancheOrificeManoeuvre create = super.create(ccm);
      create.setSensOuv(EnumSensOuv.OUV_VERS_BAS);
      return create;
    }

    @Override
    public boolean isCreatorFor(DonCLimM item) {
      return super.isCreatorFor(item) && EnumSensOuv.OUV_VERS_BAS.equals(((CalcTransBrancheOrificeManoeuvre) item).getSensOuv());
    }
  }

  public static class CalcTransBrancheOrificeManoeuvreRegulCreatorSensVersBas extends CalcTransBrancheOrificeManoeuvreRegulCreator {

    public CalcTransBrancheOrificeManoeuvreRegulCreatorSensVersBas() {
      nom = nom + " " + EnumSensOuv.OUV_VERS_BAS.geti18n();
      shortNom = shortNom + " " + EnumSensOuv.OUV_VERS_BAS.geti18n();
    }

    @Override
    public CalcTransBrancheOrificeManoeuvreRegul create(CrueConfigMetier ccm) {
      final CalcTransBrancheOrificeManoeuvreRegul create = super.create(ccm);
      create.setSensOuv(EnumSensOuv.OUV_VERS_BAS);
      return create;
    }

    @Override
    public boolean isCreatorFor(DonCLimM item) {
      return super.isCreatorFor(item) && EnumSensOuv.OUV_VERS_BAS.equals(((CalcTransBrancheOrificeManoeuvreRegul) item).getSensOuv());
    }
  }

  public static boolean isBrancheOrifice(EMH emh) {
    boolean ok = emh.getCatType().equals(EnumCatEMH.BRANCHE);
    if (ok) {
      EnumBrancheType brancheType = ((CatEMHBranche) emh).getBrancheType();
      ok = EnumBrancheType.EMHBrancheOrifice.equals(brancheType);
    }
    return ok;
  }

  public static class CalcPseudoPermBrancheSaintVenantQruisCreator extends CalcBuilder<CalcPseudoPermBrancheSaintVenantQruis> {

    public CalcPseudoPermBrancheSaintVenantQruisCreator() {
      super(CalcPseudoPermBrancheSaintVenantQruis.class);
    }

    @Override
    public CalcPseudoPermBrancheSaintVenantQruis create(CrueConfigMetier ccm) {
      return new CalcPseudoPermBrancheSaintVenantQruis(ccm);
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isBrancheSaintVenant(emh);
    }
  }

  public static boolean isBrancheSaintVenant(EMH emh) {
    boolean ok = emh.getCatType().equals(EnumCatEMH.BRANCHE);
    if (ok) {
      EnumBrancheType brancheType = ((CatEMHBranche) emh).getBrancheType();
      ok = EnumBrancheType.EMHBrancheSaintVenant.equals(brancheType);
    }
    return ok;
  }

  public static class CalcPseudoPermNoeudNiveauContinuZimpCreator extends CalcBuilder<CalcPseudoPermNoeudNiveauContinuZimp> {

    public CalcPseudoPermNoeudNiveauContinuZimpCreator() {
      super(CalcPseudoPermNoeudNiveauContinuZimp.class);
    }

    @Override
    public CalcPseudoPermNoeudNiveauContinuZimp create(CrueConfigMetier ccm) {
      return new CalcPseudoPermNoeudNiveauContinuZimp(ccm);
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isNoeudNiveauContinu(emh);
    }
  }

  private static class CalcPseudoPermNoeudBg1Creator extends CalcBuilder<CalcPseudoPermNoeudBg1> {

    public CalcPseudoPermNoeudBg1Creator() {
      super(CalcPseudoPermNoeudBg1.class);
    }

    @Override
    public CalcPseudoPermNoeudBg1 create(CrueConfigMetier ccm) {
      return new CalcPseudoPermNoeudBg1(ccm);
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isNoeud(emh);
    }
  }

  private static class CalcPseudoPermNoeudBg1AvCreator extends CalcBuilder<CalcPseudoPermNoeudBg1Av> {

    public CalcPseudoPermNoeudBg1AvCreator() {
      super(CalcPseudoPermNoeudBg1Av.class);
    }

    @Override
    public CalcPseudoPermNoeudBg1Av create(CrueConfigMetier ccm) {
      return new CalcPseudoPermNoeudBg1Av(ccm);
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isNoeud(emh);
    }
  }

  private static class CalcPseudoPermNoeudBg2Creator extends CalcBuilder<CalcPseudoPermNoeudBg2> {

    public CalcPseudoPermNoeudBg2Creator() {
      super(CalcPseudoPermNoeudBg2.class);
    }

    @Override
    public CalcPseudoPermNoeudBg2 create(CrueConfigMetier ccm) {
      return new CalcPseudoPermNoeudBg2(ccm);
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isNoeud(emh);
    }
  }

  private static class CalcPseudoPermNoeudBg2AvCreator extends CalcBuilder<CalcPseudoPermNoeudBg2Av> {

    public CalcPseudoPermNoeudBg2AvCreator() {
      super(CalcPseudoPermNoeudBg2Av.class);
    }

    @Override
    public CalcPseudoPermNoeudBg2Av create(CrueConfigMetier ccm) {
      return new CalcPseudoPermNoeudBg2Av(ccm);
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isNoeud(emh);
    }
  }

  private static class CalcPseudoPermNoeudUsiCreator extends CalcBuilder<CalcPseudoPermNoeudUsi> {

    public CalcPseudoPermNoeudUsiCreator() {
      super(CalcPseudoPermNoeudUsi.class);
    }

    @Override
    public CalcPseudoPermNoeudUsi create(CrueConfigMetier ccm) {
      return new CalcPseudoPermNoeudUsi(ccm);
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isNoeud(emh);
    }
  }

  public static boolean isNoeudNiveauContinu(EMH emh) {
    boolean ok = emh.getCatType().equals(EnumCatEMH.NOEUD);
    if (ok) {
      EnumNoeudType brancheType = ((CatEMHNoeud) emh).getNoeudType();
      ok = EnumNoeudType.EMHNoeudNiveauContinu.equals(brancheType);
    }
    return ok;
  }

  public static class CalcPseudoPermCasierProfilQruisCreator extends CalcBuilder<CalcPseudoPermCasierProfilQruis> {

    public CalcPseudoPermCasierProfilQruisCreator() {
      super(CalcPseudoPermCasierProfilQruis.class);
    }

    @Override
    public CalcPseudoPermCasierProfilQruis create(CrueConfigMetier ccm) {
      return new CalcPseudoPermCasierProfilQruis(ccm);
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isCasierProfil(emh);
    }
  }

  public static boolean isCasierProfil(EMH emh) {
    boolean ok = emh.getCatType().equals(EnumCatEMH.CASIER);
    if (ok) {
      EnumCasierType brancheType = ((CatEMHCasier) emh).getCasierType();
      ok = EnumCasierType.EMHCasierProfil.equals(brancheType);
    }
    return ok;
  }

  public static class CalcPseudoPermNoeudQappCreator extends CalcBuilder<CalcPseudoPermNoeudQapp> {

    public CalcPseudoPermNoeudQappCreator() {
      super(CalcPseudoPermNoeudQapp.class);
    }

    @Override
    public CalcPseudoPermNoeudQapp create(CrueConfigMetier ccm) {
      return new CalcPseudoPermNoeudQapp(ccm);
    }

    @Override
    public boolean isAccepted(EMH emh) {
      return isNoeud(emh);
    }
  }

  public static boolean isNoeud(EMH emh) {
    return emh.getCatType().equals(EnumCatEMH.NOEUD);
  }
}
