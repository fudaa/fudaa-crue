package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

public interface DonCLimM extends InfosEMH {
  Calc getCalculParent();

  String getNomCalculParent();

  void setCalculParent(Calc newCalculParent);

  @Override
  EMH getEmh();
  
  @UsedByComparison(ignoreInComparison=true)
  @Visibility(ihm = false)
  String getEmhId();

  @Override
  void setEmh(EMH newEMH);
  

}
