package org.fudaa.dodico.crue.metier.helper;

import org.fudaa.dodico.crue.metier.emh.*;

import java.util.*;

/**
 * @author deniger
 */
public class DonPrtHelper {
  public static Set<DonPrtGeoProfilSection> getAllProfilSection(EMHSousModele ssModele) {
    Set<DonPrtGeoProfilSection> res = new HashSet<>();
    getAllProfilSection(ssModele, res);
    return res;
  }

  public static List<DonPrtGeoProfilCasier> getProfilCasier(CatEMHCasier casier) {
    return EMHHelper.selectClass(casier.getInfosEMH(), DonPrtGeoProfilCasier.class);
  }

  public static Set<DonPrtGeoProfilSection> getAllProfilSection(EMHScenario scenario) {
    Set<DonPrtGeoProfilSection> res = new HashSet<>();
    List<EMHSousModele> sousModeles = scenario.getSousModeles();
    for (EMHSousModele ssModele : sousModeles) {
      getAllProfilSection(ssModele, res);
    }
    return res;
  }

  public static <T extends Collection<DonPrtGeoProfilSection>> T getAllProfilSection(EMHSousModele ssModele,
                                                                                     T res) {
    List<CatEMHSection> sections = EMHHelper.selectEMHS(ssModele.getAllSimpleEMH(), EnumCatEMH.SECTION);
    for (CatEMHSection section : sections) {
      DonPrtGeoProfilSection profilSection = EMHHelper.selectFirstOfClass(section.getInfosEMH(), DonPrtGeoProfilSection.class);
      if (profilSection != null) {
        res.add(profilSection);
      }
    }
    return res;
  }

  public static List<DonPrtGeoSectionIdem> getAllPrtGeoSectionIdem(EMHSousModele ssModele) {
    return getAllPrtGeoSectionIdem(ssModele, new ArrayList<>());
  }

  public static <T extends Collection<DonPrtGeoSectionIdem>> T getAllPrtGeoSectionIdem(EMHSousModele ssModele,
                                                                                       T res) {
    List<CatEMHSection> sections = EMHHelper.selectEMHS(ssModele.getAllSimpleEMH(), EnumCatEMH.SECTION);
    for (CatEMHSection section : sections) {
      DonPrtGeoSectionIdem profilSection = getDonPrtSectionIdem(section);
      if (profilSection != null) {
        res.add(profilSection);
      }
    }
    return res;
  }

  public static List<DonPrtGeoBrancheSaintVenant> getAllPrtGeoBrancheSaintVenant(EMHSousModele ssModele) {
    return getAllPrtGeoBrancheSaintVenant(ssModele, new ArrayList<>());
  }

  public static <T extends Collection<DonPrtGeoBrancheSaintVenant>> T getAllPrtGeoBrancheSaintVenant(EMHSousModele ssModele,
                                                                                                     T res) {
    List<EMHBrancheSaintVenant> branchesSaintVenant = ssModele.getBranchesSaintVenant();
    for (EMHBrancheSaintVenant branche : branchesSaintVenant) {
      DonPrtGeoBrancheSaintVenant dptg = EMHHelper.selectFirstOfClass(branche.getInfosEMH(), DonPrtGeoBrancheSaintVenant.class);
      if (dptg != null) {
        res.add(dptg);
      }
    }
    return res;
  }

  public static List<DonPrtGeoProfilCasier> getAllProfilCasier(EMHSousModele ssModele) {
    return getAllProfilCasier(ssModele, new ArrayList<>());
  }

  public static <T extends Collection<DonPrtGeoProfilCasier>> T getAllProfilCasier(EMHSousModele ssModele, T res) {
    List<CatEMHCasier> casiers = ssModele.getCasiers();
    for (CatEMHCasier casier : casiers) {
      res.addAll(getDonPrtGeoProfilCasier(casier));
    }
    return res;
  }

  public static Set<DonPrtGeoProfilCasier> getAllProfilCasier(EMHScenario scenario) {
    Set<DonPrtGeoProfilCasier> res = new HashSet<>();
    List<EMHSousModele> sousModeles = scenario.getSousModeles();
    for (EMHSousModele ssModele : sousModeles) {
      getAllProfilCasier(ssModele, res);
    }
    return res;
  }

  public static List<DonPrtGeoBatiCasier> getAllBatiCasier(EMHSousModele ssModele) {
    return getAllBatiCasier(ssModele, new ArrayList<>());
  }

  public static <T extends Collection<DonPrtGeoBatiCasier>> T getAllBatiCasier(EMHSousModele ssModele, T res) {
    List<CatEMHCasier> casiers = ssModele.getCasiers();
    for (CatEMHCasier casier : casiers) {
      res.addAll(getDonPrtGeoBatiCasier(casier));
    }
    return res;
  }

  public static Set<DonPrtGeoBatiCasier> getAllBatiCasier(EMHScenario scenario) {
    Set<DonPrtGeoBatiCasier> res = new HashSet<>();

    List<EMHSousModele> sousModeles = scenario.getSousModeles();
    for (EMHSousModele ssModele : sousModeles) {
      getAllBatiCasier(ssModele, res);
    }
    return res;
  }

  public static DonPrtGeoProfilSection getProfilSection(EMHSectionProfil section) {
    return section == null ? null : EMHHelper.selectFirstOfClass(section.getInfosEMH(), DonPrtGeoProfilSection.class);
  }

  public static DonPrtGeoProfilSection getProfilSection(EMH section) {
    if (section != null && section.getClass().equals(EMHSectionProfil.class)) {
      return getProfilSection((EMHSectionProfil) section);
    }
    return null;
  }

  public static DonPrtGeoSectionIdem getDonPrtSectionIdem(CatEMHSection section) {
    return EMHHelper.selectFirstOfClass(section.getInfosEMH(), DonPrtGeoSectionIdem.class);
  }

  static double getZf(EMHSectionProfil retrieveProfil) {
    DonPrtGeoProfilSection profilSection = getProfilSection(retrieveProfil);
    final int nb = profilSection == null ? 0 : profilSection.getPtProfilSize();
    if (nb == 0) {
      return 0;//que faire ?
    }
    List<PtProfil> ptProfil = profilSection.getPtProfil();
    double zf = ptProfil.get(0).getZ();
    for (int i = 1; i < nb; i++) {
      zf = Math.min(zf, ptProfil.get(i).getZ());
    }
    return zf;
  }

  public static Collection<DonPrtGeoBatiCasier> getDonPrtGeoBatiCasier(final CatEMHCasier casier) {
    return EMHHelper.selectClass(casier.getInfosEMH(), DonPrtGeoBatiCasier.class);
  }

  public static Collection<DonPrtGeoProfilCasier> getDonPrtGeoProfilCasier(final CatEMHCasier casier) {
    return EMHHelper.selectClass(casier.getInfosEMH(), DonPrtGeoProfilCasier.class);
  }
}
