package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

public class DonPrtCIniBrancheSaintVenant extends DonPrtCIniBranche {

  private double qruis;

  /**
   * @param def
   */
  public DonPrtCIniBrancheSaintVenant(CrueConfigMetier def) {
    super(def);
    init(def);
  }

  private void init(CrueConfigMetier def) {
    qruis = def.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_QRUIS);
  }

  @PropertyDesc(i18n = "qruis.property")
  public double getQruis() {
    return qruis;
  }

  @Override
  public void initFrom(DonPrtCIniBranche donPrtCIniBranche) {
    super.initFrom(donPrtCIniBranche);
    if (donPrtCIniBranche instanceof DonPrtCIniBrancheSaintVenant) {
      qruis = ((DonPrtCIniBrancheSaintVenant) donPrtCIniBranche).qruis;
    }
  }

  /**
   * @param newQruis
   */
  public void setQruis(double newQruis) {
    qruis = newQruis;
  }
}
