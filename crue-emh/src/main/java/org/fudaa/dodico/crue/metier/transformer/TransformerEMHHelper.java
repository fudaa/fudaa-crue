package org.fudaa.dodico.crue.metier.transformer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.contrat.DoubleValuable;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.PdtVar;
import org.fudaa.dodico.crue.metier.emh.RelationEMH;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.joda.time.Duration;

/**
 * @author deniger
 */
public class TransformerEMHHelper {

  /**
   *
   * @param in
   * @return set avec les uid des emh
   */
  public static final Set<Long> toSetOfUId(Collection<? extends EMH> in) {
    if (in == null) {
      return Collections.emptySet();
    }
    Set<Long> res = new HashSet<>(in.size());
    CollectionUtils.collect(in, TransformerEMHHelper.EMH_TO_UID, res);
    return res;
  }

  public static String transformToString(final Object o1, CrueConfigMetier crueProperties, EnumToString enumToString, DecimalFormatEpsilonEnum type) {
    String pref = StringUtils.EMPTY;
    if (o1 instanceof ObjetWithID) {
      pref = ((ObjetWithID) o1).getNom();
    } else if (o1 instanceof ToStringTransformable) {
      pref = ((ToStringTransformable) o1).toString(crueProperties, enumToString, type);
    }
    return pref;
  }

  public static String transformToStringOverview(final Object o1, CrueConfigMetier crueProperties, DecimalFormatEpsilonEnum type) {
    return TransformerEMHHelper.transformToString(o1, crueProperties, EnumToString.OVERVIEW, type);
  }

  public static String formatFromPropertyName(String propName, Object value, CrueConfigMetier ccm, DecimalFormatEpsilonEnum type) {
    if (value == null) {
      return StringUtils.EMPTY;
    }
    if (value instanceof ToStringTransformable) {
      return ((ToStringTransformable) value).toString(ccm, EnumToString.COMPLETE, type);
    }
    ItemVariable property = ccm.getProperty(propName);
    if (property == null) {
      return ObjectUtils.toString(value);
    }
    final PropertyNature nature = property.getNature();
    return doFormatFromNature(nature, value, ccm, type);
  }

  public static String format(ItemVariable property, Object value, CrueConfigMetier ccm, DecimalFormatEpsilonEnum type) {
    if (value == null) {
      return StringUtils.EMPTY;
    }
    if (value instanceof ToStringTransformable) {
      return ((ToStringTransformable) value).toString(ccm, EnumToString.COMPLETE, type);
    }
    if (property == null) {
      return ObjectUtils.toString(value);
    }
    final PropertyNature nature = property.getNature();
    return doFormatFromNature(nature, value, ccm, type);
  }

  public static String formatFromNature(final PropertyNature nature, Object value, CrueConfigMetier ccm, DecimalFormatEpsilonEnum type) {
    if (value == null) {
      return StringUtils.EMPTY;
    }
    if (value instanceof ToStringTransformable) {
      return ((ToStringTransformable) value).toString(ccm, EnumToString.COMPLETE, type);
    }
    return doFormatFromNature(nature, value, ccm, type);

  }

  public static String formatFromNatureName(final String nature, Object value, CrueConfigMetier ccm, DecimalFormatEpsilonEnum type) {
    if (value == null) {
      return StringUtils.EMPTY;
    }
    if (value instanceof ToStringTransformable) {
      return ((ToStringTransformable) value).toString(ccm, EnumToString.COMPLETE, type);
    }
    PropertyNature propNature = ccm.getNature(nature);
    if (propNature == null) {
      return ObjectUtils.toString(value);
    }
    return doFormatFromNature(propNature, value, ccm, type);

  }

  private static String doFormatFromNature(final PropertyNature nature, Object value, CrueConfigMetier ccm, DecimalFormatEpsilonEnum type) {
    if (nature.isEnum()) {
      if (value.getClass().isEnum()) {
        return value.toString();
      }
      return new EnumNumberFormatter(nature).format((Double) value);
    }
    if (nature.isDuration()) {
      if (value instanceof DoubleValuable) {
        return nature.getFormatter(type).format(((DoubleValuable) value).toDoubleValue());
      }
      if (value.getClass().equals(PdtVar.class)) {
        return ((PdtVar) value).toString(ccm, EnumToString.COMPLETE, type);
      }
      Long sec = null;
      if (value.getClass().equals(Long.class)) {
        sec = (Long) value;
      } else {
        sec = ((Duration) value).getStandardSeconds();
      }
      return nature.getFormatter(type).format(sec);
    }
    if (!nature.needFormatter() || nature.getFormatter(type) == null) {
      return value.toString();
    }
    try {
      return nature.getFormatter(type).format(value);
    } catch (Exception e) {
      return value.toString();
    }
  }

  /**
   * Transformer creant une RelationEMHContient par EMH
   *
   * @author deniger
   */
  public static final class TransformerEMHToRelationContient implements Transformer {

    @Override
    public Object transform(final Object input) {
      return EMHRelationFactory.createRelationContient((EMH) input);
    }
  }

  /**
   * @author deniger
   */
  public static final class TransformerRelationToEMH implements Transformer {

    @SuppressWarnings("unchecked")
    @Override
    public Object transform(final Object input) {
      return ((RelationEMH) input).getEmh();
    }
  }
  public static final Transformer EMH_TO_UID = new EMHToUid();

  /**
   * @author deniger
   */
  public static final class EMHToUid implements Transformer {

    @SuppressWarnings("unchecked")
    @Override
    public Object transform(final Object input) {
      return ((EMH) input).getUiId();
    }
  }

  /**
   *
   * @param in
   * @return List avec les uid des emh
   */
  public static final List<Long> toUId(Collection<? extends EMH> in) {
    if (in == null) {
      return Collections.emptyList();
    }
    List<Long> res = new ArrayList<>(in.size());
    CollectionUtils.collect(in, EMH_TO_UID, res);
    return res;
  }

  /**
   * @author deniger
   */
  public static final class TransformerRelationToEMHId implements Transformer {

    @SuppressWarnings("unchecked")
    @Override
    public Object transform(final Object input) {
      return ((RelationEMH) input).getEmh().getId();
    }
  }

  /**
   * @author deniger
   */
  public static final class TransformerRelationToEMHNom implements Transformer {

    @SuppressWarnings("unchecked")
    @Override
    public Object transform(final Object input) {
      return ((RelationEMH) input).getEmh().getNom();
    }
  }
}
