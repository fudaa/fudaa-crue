package org.fudaa.dodico.crue.metier.etude;

import java.util.Map;
import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;

/**
 * Entité correspondant à la portée d'un calcul
 *
 * @author Adrien Hadoux
 */
public class ManagerEMHModeleBase extends ManagerEMHContainer<ManagerEMHSousModele> {

  public ManagerEMHModeleBase(final String nom) {
    super(CrueLevelType.MODELE, nom);
  }

  public ManagerEMHModeleBase() {
    super(CrueLevelType.MODELE);
  }

  public String getEMHType() {
    return EMHModeleBase.class.getSimpleName();
  }

  @Override
  public Map<String, FichierCrueParModele> addAllFileUsed(final Map<String, FichierCrueParModele> mapByName) {
    ManagerEMHSousModele.addAllFile(this, mapByName, this);
    for (final ManagerEMHSousModele fils : getFils()) {
      fils.addAllFile(mapByName, this);
    }
    return mapByName;
  }
}
