/**
 * *********************************************************************
 * Module: OrdResScenarioBrancheBarrageFilEau.java Author: BATTISTA Purpose: Defines the Class OrdResScenarioBrancheBarrageFilEau
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.OrdResKey;

public class OrdResBrancheBarrageFilEau extends OrdRes implements Cloneable {

  @Override
  public OrdResBrancheBarrageFilEau clone() {
    try {
      return (OrdResBrancheBarrageFilEau) super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why this ?");
  }
  public final static OrdResKey KEY = new OrdResKey(EnumBrancheType.EMHBrancheBarrageFilEau);

  @Override
  public OrdResKey getKey() {
    return KEY;
  }

  @Override
  public String toString() {
    return "OrdResBrancheBarrageFilEau [ddeRegime=" + getDdeValue("regimeBarrage") + "]";
  }
}