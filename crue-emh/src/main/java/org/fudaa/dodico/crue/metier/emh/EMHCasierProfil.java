package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

public class EMHCasierProfil extends CatEMHCasier {

  public EMHCasierProfil(final String nom) {
    super(nom);
  }
  
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public EnumCasierType getCasierType(){
    return EnumCasierType.EMHCasierProfil;
  }

  @Override
  @Visibility(ihm = false)
  public EMHCasierProfil copyShallowFirstLevel() {
    return  copyPrimitiveIn(new EMHCasierProfil(getNom()));
  }

}
