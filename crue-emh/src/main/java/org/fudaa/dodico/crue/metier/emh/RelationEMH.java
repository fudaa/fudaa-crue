/**
 * *********************************************************************
 * Module: RelationEMH.java Author: deniger Purpose: Defines the Class RelationEMH
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 * Realtions entre une EMH donnée et d'autres EMH du réseau. Cette relation peut être porteuse de données (ex abscisse d'une Section dans une Branche)
 *
 */
public abstract class RelationEMH<T extends EMH> {
  /**
   * L'EMH liée par cette relation
   *
   */
  private T emh;

  public void setEmh(final T newEmh) {
    emh = newEmh;
  }

  public T getEmh() {
    return emh;
  }

  @Visibility(ihm = false)
  @UsedByComparison(ignoreInComparison = true)
  public String getEmhId() {
    return emh == null ? null : emh.getId();
  }

  @Visibility(ihm = false)
  @UsedByComparison(ignoreInComparison = true)
  public String getEmhNom() {
    return emh == null ? null : emh.getNom();
  }

  @Visibility(ihm = false)
  @UsedByComparison(ignoreInComparison = true)
  public abstract RelationEMH<T> copyWithEMH(T copiedEMH);

  public String getType() {
    return getClass().getSimpleName();
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + " " + (getEmh() == null ? "null" : emh.getNom());
  }
}
