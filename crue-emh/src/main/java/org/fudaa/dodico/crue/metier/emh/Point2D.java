/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.crue.metier.emh;

/**
 * Un point avec 2 coordonnées abscisse et coordonnee
 * 
 * @author deniger
 */
public interface Point2D {

  double getAbscisse();

  double getOrdonnee();

}
