package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.BusinessMessages;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * CL de type 2 en Crue9
 */
public class CalcTransNoeudUsine extends DonCLimMCommonItem  {


  @Override
  public String getShortName() {
    return BusinessMessages.getString("usine.property");
  }

  CalcTransNoeudUsine deepClone() {
    try {
      return (CalcTransNoeudUsine) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransNoeudUsine.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }
}
