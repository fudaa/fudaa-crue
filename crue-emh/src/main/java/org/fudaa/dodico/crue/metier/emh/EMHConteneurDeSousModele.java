/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.List;

import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 * @author deniger
 */
public abstract class EMHConteneurDeSousModele extends CatEMHConteneur implements CommentaireContainer {

  CommentairesManager commentaires;

  public EMHConteneurDeSousModele() {
    super();
  }

  @Override
  public CommentairesManager getCommentairesManager() {
    if (commentaires == null) {
      commentaires = new CommentairesManager();
    }
    return commentaires;
  }

  @Override
  protected <T extends EMH> T copyPrimitiveIn(T in) {
    if (commentaires != null) {
      ((EMHConteneurDeSousModele) in).commentaires = commentaires.copy();
    }
    return super.copyPrimitiveIn(in);

  }

  public abstract List<EMHSousModele> getSousModeles();

  @Override
  public List<EMH> getAllSimpleEMH() {
    return CatEMHConteneurHelper.getAllSimpleEMH(getSousModeles());
  }

  @Override
  @UsedByComparison
  public List<EMH> getAllSimpleEMHinUserOrder() {
    return CatEMHConteneurHelper.getAllSimpleEMHinUserOrder(getSousModeles());
  }

  @Override
  public List<CatEMHBranche> getBranches() {
    return CatEMHConteneurHelper.getBranches(getSousModeles());
  }

  @Override
  public List<CatEMHBranche> getBranchesAvecSectionPilote() {
    return CatEMHConteneurHelper.getBranchesAvecSectionPilote(getSousModeles());
  }

  @Override
  public List<EMHBrancheSaintVenant> getBranchesSaintVenant() {
    return CatEMHConteneurHelper.getBranchesSaintVenant(getSousModeles());
  }

  @Override
  public List<CatEMHCasier> getCasiers() {
    return CatEMHConteneurHelper.getCasiers(getSousModeles());
  }

  @Override
  public List<CatEMHNoeud> getNoeuds() {
    return CatEMHConteneurHelper.getNoeuds(getSousModeles());
  }

  @Override
  public List<CatEMHSection> getSections() {
    return CatEMHConteneurHelper.getSections(getSousModeles());
  }
}
