/**
 * *********************************************************************
 * Module: ValParam.java Author: deniger Purpose: Defines the Class ValParam *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;

import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class ValParam implements ObjetNomme, ToStringTransformable, Cloneable {
  public static final String PROP_VALEUR = "valeur";
  private String nom;

  @Override
  public final String getNom() {
    return nom;
  }

  private String id;

  @Override
  public String getId() {
    return id;
  }

  /**
   * @return le type de donnée portée par ce ValParam
   */
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public abstract Class getTypeData();

  public abstract Object getValeurObjet();

  public String getValueObjetInString(CrueConfigMetier prop) {
    return TransformerEMHHelper.formatFromPropertyName(StringUtils.uncapitalize(nom), getValeurObjet(), prop, DecimalFormatEpsilonEnum.COMPARISON);
  }

  @Override
  public final void setNom(final String newNom) {
    nom = newNom;
    if (nom != null) {
      id = nom.toUpperCase();
    }
  }

  public ValParam(String nom) {
    super();
    setNom(nom);
  }

  @Override
  protected Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  public ValParam deepClone() {
    try {
      return (ValParam) clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
      Logger.getLogger(ValParam.class.getName()).log(Level.SEVERE, "message {0}", cloneNotSupportedException);
    }
    return null;
  }
}
