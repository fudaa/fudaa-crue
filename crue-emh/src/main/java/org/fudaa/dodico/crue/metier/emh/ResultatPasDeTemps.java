package org.fudaa.dodico.crue.metier.emh;

import java.util.Collection;
import java.util.List;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.result.ModeleResultatTimeKeyContent;

/**
 *
 * @author deniger
 */
public class ResultatPasDeTemps extends AbstractInfosEMH {

  private final ResultatPasDeTempsDelegate delegate;
  private final Collection<ResultatTimeKey> resultatKey;
  private final List<ResultatPasDeTempsParCalcul> resultatPasDeTempsParCalcul;
  private ResultatPasDeTempsMapper mapper;

  public ResultatPasDeTemps(ResultatPasDeTempsDelegate delegate) {
    this.delegate = delegate;
    this.resultatKey = delegate.getResultatKeys();
    resultatPasDeTempsParCalcul = ModeleResultatTimeKeyContent.build(resultatKey);
  }

  @UsedByComparison
  public List<ResultatPasDeTempsParCalcul> getResultatPasDeTempsParCalcul() {
    return resultatPasDeTempsParCalcul;
  }

  @UsedByComparison
  public ResultatPasDeTempsDelegate getDelegate() {
    return delegate;
  }

  public int getNbResultats() {
    return resultatKey.size();
  }

  @Override
  public EnumInfosEMH getCatType() {
    return EnumInfosEMH.RESULTAT_PAS_DE_TEMPS;
  }

  /**
   * Attention, ne fait de translation de tempsSimu dans le cas de temps absolu.
   *
   * @param toUse
   * @return null si pas d'equivalence
   */
  public ResultatTimeKey getEquivalentTempsSimu(ResultatTimeKey toUse) {
    if (mapper == null) {
      mapper = new ResultatPasDeTempsMapper(resultatKey);
    }
    return mapper.getEquivalentTempsSimu(toUse);

  }

  public ResultatTimeKey getEquivalentTempsSimu(long tempsSimu) {
    if (mapper == null) {
      mapper = new ResultatPasDeTempsMapper(resultatKey);
    }
    return mapper.getEquivalentTempsSimu(tempsSimu);
  }
}
