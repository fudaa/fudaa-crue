package org.fudaa.dodico.crue.metier.emh;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 *
 * @author deniger
 */
public class IniCalcCliche implements Cloneable {

  private String nomFic = StringUtils.EMPTY;

  public String getNomFic() {
    return nomFic;
  }

  @Override
  protected IniCalcCliche clone() {
    try {
      return (IniCalcCliche) super.clone();
    } catch (CloneNotSupportedException ex) {
      Logger.getLogger(IniCalcCliche.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("why");
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public boolean isEmpty() {
    return StringUtils.isBlank(nomFic);
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + " " + nomFic;
  }

  public void setNomFic(String nomFic) {
    this.nomFic = nomFic;
  }
}
