package org.fudaa.dodico.crue.metier.emh;

import java.util.ArrayList;
import java.util.List;

/**
 * Point evolution FF.
 *
 * @author Adrien Hadoux
 */
public class EvolutionFF {
  private List<PtEvolutionFF> mpoints;

  public EvolutionFF() {
  }

  public EvolutionFF(final List<PtEvolutionFF> mpoints) {
    setPtEvolutionFF(mpoints);
  }

  public EvolutionFF inverse() {
    List<PtEvolutionFF> res = new ArrayList<>();
    if (mpoints != null) {
      for (PtEvolutionFF ptEvolutionFF : mpoints) {
        res.add(new PtEvolutionFF(ptEvolutionFF.getOrdonnee(), ptEvolutionFF.getAbscisse()));
      }
    }
    return new EvolutionFF(res);
  }

  public void addPoint(double x, double y) {
    initListPoints();
    mpoints.add(new PtEvolutionFF(x, y));
  }

  public void initListPoints() {
    if (mpoints == null) {
      mpoints = new ArrayList<>();
    }
  }

  public EvolutionFF copy() {
    EvolutionFF copy = new EvolutionFF();
    if (mpoints != null) {
      copy.mpoints = new ArrayList<>();
      for (PtEvolutionFF ptEvolutionFF : mpoints) {
        copy.mpoints.add(ptEvolutionFF.copy());
      }
    }
    return copy;
  }

  public List<PtEvolutionFF> getPtEvolutionFF() {
    return mpoints;
  }

  public final void setPtEvolutionFF(final List<PtEvolutionFF> mpoints) {
    this.mpoints = mpoints;
  }
}
