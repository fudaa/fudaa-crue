package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 * @author deniger
 */
public abstract class AbstractInfosEMH implements InfosEMH {

  EMH emh;

  @Override
  public  void setEmh(EMH emh) {
    this.emh = emh;
  }

  @Override
  public void removeEMH(EMH emh) {
    assert emh == this.emh;
    this.emh = null;
  }

  @Override
  public final String getType() {
    return getClass().getSimpleName();
  }

  @Visibility(ihm = false)
  @Override
  public final String getTypei18n() {
    return ToStringHelper.typeToi18n(getType());
  }

  @Override
  public boolean getActuallyActive() {
    return getEmh() != null && getEmh().getActuallyActive();
  }

  @Override
  public  EMH getEmh() {
    return emh;
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public final String getEmhId() {
    return emh == null ? null : emh.getId();
  }
}
