package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;

public abstract class DonCalcSansPrtBrancheSeuil extends DonCalcSansPrt {

  /**
   * enum parmi (Borda, Divergent)
   *
   */
  private EnumFormulePdc formulePdc;

  @PropertyDesc(i18n = "formulePdc.property")
  public final EnumFormulePdc getFormulePdc() {
    return formulePdc;
  }

  /**
   * @param newFormulePdc
   */
  public final void setFormulePdc(final EnumFormulePdc newFormulePdc) {
    formulePdc = newFormulePdc;
  }
}
