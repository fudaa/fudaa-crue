package org.fudaa.dodico.crue.metier.emh;

public class DonFrtStrickler extends DonFrt {

  private LoiFF loiZK;

  public LoiFF getStrickler() {
    return loiZK;
  }

  @Override
  public LoiFF getLoi() {
    return getStrickler();
  }

 

  @Override
  public DonFrtStrickler deepClone() {
    try {
      final DonFrtStrickler res = (DonFrtStrickler) clone();
      if (res.loiZK != null) {
        res.loiZK = loiZK.clonedWithoutUser();
        res.loiZK.register(res);
      }
      return res;
    } catch (final CloneNotSupportedException cloneNotSupportedException) {
    }
    return null;
  }

  public void setStrickler(final LoiFF newStrickler) {
    if (this.loiZK != null) {
      this.loiZK.unregister(this);
    }

    loiZK = newStrickler;
    this.loiZK.register(this);
  }
}
