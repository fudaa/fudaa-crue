package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;

public final class PtEvolutionTF {

    private String abscisse;
    private double ordonnee;

    /**
     * @param abscisse
     * @param ordonnee
     */
    public PtEvolutionTF(String abscisse, double ordonnee) {
        this.abscisse = abscisse;
        this.ordonnee = ordonnee;
    }


    public PtEvolutionTF() {
    }

    public PtEvolutionTF copy() {
        return new PtEvolutionTF(abscisse, ordonnee);
    }

    public String toString(CrueConfigMetier props, String abs, String ord) {
        return abscisse + "; " + TransformerEMHHelper.formatFromPropertyName(ord, ordonnee, props, DecimalFormatEpsilonEnum.COMPARISON);
    }

    public PtEvolutionTF(PtEvolutionTF oldPtEvolutionFF) {
        abscisse = oldPtEvolutionFF.abscisse;
        ordonnee = oldPtEvolutionFF.ordonnee;
    }

    public void setAbscisse(String newAbscisse) {
        abscisse = newAbscisse;
    }

    public void setOrdonnee(double newY) {
        ordonnee = newY;
    }

    public double getOrdonnee() {
        return ordonnee;
    }

    public String getAbscisse() {
        return abscisse;
    }
}
