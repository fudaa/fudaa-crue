/***********************************************************************
 * Module: OrdCalcPerm.java Author: deniger Purpose: Defines the Class OrdCalcPerm
 ***********************************************************************/

package org.fudaa.dodico.crue.metier.emh;

public abstract class OrdCalcPerm extends OrdCalc {
  /**
   * @return true si transitoire
   */
  @Override
  public boolean isTransitoire() {
    return false;
  }

}
