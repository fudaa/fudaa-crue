/***********************************************************************
 * Module:  RelationEMHSectionPiloteDansBranche.java
 * Author:  deniger
 * Purpose: Defines the Class RelationEMHSectionPiloteDansBranche
 ***********************************************************************/

package org.fudaa.dodico.crue.metier.emh;

public class RelationEMHSectionPiloteDansBranche extends RelationEMH<CatEMHSection> {
  @Override
  public RelationEMHSectionPiloteDansBranche copyWithEMH(CatEMHSection copiedEMH) {
    RelationEMHSectionPiloteDansBranche res = new RelationEMHSectionPiloteDansBranche();
    res.setEmh(copiedEMH);
    return res;
  }

}
