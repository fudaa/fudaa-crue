package org.fudaa.dodico.crue.metier.helper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.functors.OrPredicate;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.ConfigLoi;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.config.loi.ItemTypeExtrapolationLoi;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;

import java.util.*;

/**
 * Classe qui permet de définir les différents types de lois et qui permet de retrouver une loi parmi une liste de lois
 *
 * @author cde
 */
public class LoiHelper {
    /**
     * Loi de type Hydrogramme
     */
    public static final String LOI_HYDROGRAMME = "Hydrogramme";
    /**
     * Loi de type HydrogrammeRuis
     */
    public static final String LOI_HYDROGRAMMERUIS = "HydrogrammeRuis";
    /**
     * Loi de type Limnigramme
     */
    public static final String LOI_LIMNIGRAMME = "Limnigramme";
    /**
     * Loi de type Manoeuvre
     */
    public static final String LOI_MANOEUVRE = "Manoeuvre";
    /**
     * Loi de type Tarrage
     */
    public static final String LOI_TARRAGE = "Tarrage";
    /**
     * Valeur d'extrapolation égale à celle de la borne la plus proche
     */
    public static final String EXTRAPOL_CST = "ValCst";
    /**
     * Valeur d'extrapolation prolongée par interpolation linéaire des deux points connus les plus proches
     */
    public static final String EXTRAPOL_INTERPOL = "ValInterpol";

    /**
     * Retourne la loi qui correspond au nom passé en paramètre parmi une liste de lois FF et de lois DF
     *
     * @param reference
     * @param listeLoisMetier
     * @return la loi si trouvée, null sinon
     */
    public static Loi findByReference(final String reference, final List<? extends Loi> listeLoisMetier) {
        if (listeLoisMetier == null) {
            return null;
        }
        for (final Loi loi : listeLoisMetier) {

            if (loi.getNom().equals(reference)) {
                return loi;
            }
        }
        return null;
    }

    public static TreeMap<String, List<String>> getVariableCorrespondance(Collection<? extends Loi> lois, CrueConfigMetier ccm) {
        TreeMap<String, List<String>> res = new TreeMap<>();
        for (Loi loi : lois) {
            ConfigLoi config = ccm.getConfLoi().get(loi.getType());
            String varX = config.getVarAbscisse().getNom();
            String varY = config.getVarOrdonnee().getNom();
            addCorrespondance(res, varX, varY);
            addCorrespondance(res, varY, varX);
        }
        for (List<String> list : res.values()) {
            Collections.sort(list);
        }
        return res;
    }

    public static double getOrdonneeMoyenne(Loi pt) {
        return getOrdonneeMoyenne(pt.getEvolutionFF().getPtEvolutionFF());
    }

    public static double getOrdonneeMoyenne(DonFrt pt) {
        return getOrdonneeMoyenne(pt.getLoi().getEvolutionFF().getPtEvolutionFF());
    }

    public static double getOrdonneeMax(DonFrt pt) {
        return getOrdonneeMax(pt.getLoi().getEvolutionFF().getPtEvolutionFF());
    }

    public static double getOrdonneeMin(DonFrt pt) {
        return getOrdonneeMin(pt.getLoi().getEvolutionFF().getPtEvolutionFF());
    }

    public static double getOrdonneeMoyenne(List<? extends Point2D> pt) {
        double sum = 0;
        for (Point2D point2D : pt) {
            sum = sum + point2D.getOrdonnee();
        }
        return sum / ((double) pt.size());
    }

    public static double getOrdonneeMax(List<? extends Point2D> pt) {
        if (pt.isEmpty()) {
            return 0;
        }
        double max = pt.get(0).getOrdonnee();
        for (Point2D point2D : pt) {
            if (point2D.getOrdonnee() > max) {
                max = point2D.getOrdonnee();
            }
        }
        return max;
    }

    public static double getOrdonneeMin(List<? extends Point2D> pt) {
        if (pt.isEmpty()) {
            return 0;
        }
        double min = pt.get(0).getOrdonnee();
        for (Point2D point2D : pt) {
            if (point2D.getOrdonnee() < min) {
                min = point2D.getOrdonnee();
            }
        }
        return min;
    }

    public static Double getInterpolatedValue(Loi loi, double x, CrueConfigMetier ccm) {
        if (loi.getNombrePoint() == 0) {
            return null;
        }
        final int nombrePoint = loi.getNombrePoint();
        if (nombrePoint == 1) {
            return loi.getOrdonnee(0);
        }
        PropertyEpsilon epsilon = ccm.getLoiAbscisseEps(loi.getType());
        for (int i = 0; i < nombrePoint - 1; i++) {
            double x0 = loi.getAbscisse(i);
            double x1 = loi.getAbscisse(i + 1);
            if (epsilon.isSame(x0, x)) {
                return loi.getOrdonnee(i);
            }
            if (epsilon.isSame(x1, x)) {
                return loi.getOrdonnee(i + 1);
            }
            if ((x > x0 && x < x1) || (x > x1 && x < x0)) {
                double y0 = loi.getOrdonnee(i);
                double y1 = loi.getOrdonnee(i + 1);
                return y0 + (y1 - y0) * (x - x0) / (x1 - x0);
            }
        }
        double xLast = loi.getAbscisse(nombrePoint - 1);
        ConfigLoi confLoi = ccm.getConfLoi().get(loi.getType());
        if (x > xLast) {
            ItemTypeExtrapolationLoi extrapolSup = confLoi.getExtrapolSup();
            if (extrapolSup == null || extrapolSup.isNone()) {
                return null;
            }
            if (extrapolSup.isConstant()) {
                return loi.getOrdonnee(nombrePoint - 1);
            }
            double x0 = loi.getAbscisse(nombrePoint - 2);
            double y0 = loi.getOrdonnee(nombrePoint - 2);
            double x1 = xLast;
            double y1 = loi.getOrdonnee(nombrePoint - 1);
            return y0 + (y1 - y0) * (x - x0) / (x1 - x0);
        }

        double x0 = loi.getAbscisse(0);
        if (x < x0) {
            ItemTypeExtrapolationLoi extrapolInf = confLoi.getExtrapolInf();
            if (extrapolInf == null || extrapolInf.isNone()) {
                return null;
            }
            if (extrapolInf.isConstant()) {
                return loi.getOrdonnee(0);
            }
            double y0 = loi.getOrdonnee(0);
            double y1 = loi.getOrdonnee(1);
            double x1 = loi.getAbscisse(1);
            return y1 + (y0 - y1) * (x1 - x) / (x1 - x0);
        }
        return null;
    }

    public static List<Loi> filterDLHY(EMHScenario scenario, EnumTypeLoi typeLoi) {
        final DonLoiHYConteneur loiConteneur = scenario.getLoiConteneur();
        return filterDLHY(loiConteneur, typeLoi);
    }

    public static LoiFF createFromProfil(List<PtProfil> profils, String nom) {
        LoiFF res = new LoiFF();
        res.setNom(nom);
        res.setType(EnumTypeLoi.LoiPtProfil);
        List<PtEvolutionFF> points = new ArrayList<>();
        if (profils != null) {
            for (PtProfil ptProfil : profils) {
                points.add(new PtEvolutionFF(ptProfil));
            }
        }
        res.setEvolutionFF(new EvolutionFF(points));
        return res;
    }

    /**
     * @param loi la loi a tester
     * @return true si la loi est vide
     */
    public static boolean isEmpty(final Loi loi) {
        if (loi == null || loi.getEvolutionFF() == null) {
            return true;
        }
        return CollectionUtils.isEmpty(loi.getEvolutionFF().getPtEvolutionFF());
    }

    /**
     * @param loi la loi a tester
     * @return true si la loi est vide
     */
    public static boolean isEmpty(final LoiFF loi) {
        if (loi == null || loi.getEvolutionFF() == null) {
            return true;
        }
        return CollectionUtils.isEmpty(loi.getEvolutionFF().getPtEvolutionFF());
    }

    /**
     * @param loi la loi a tester
     * @return true si la loi n'est pas vide
     */
    public static boolean isNotEmpty(final Loi loi) {
        return !isEmpty(loi);
    }

    /**
     * @param loi la loi a tester
     * @return true si la loi n'est pas vide
     */
    public static boolean isNotEmpty(final LoiFF loi) {
        return !isEmpty(loi);
    }

    public static void validLoiNom(final CtuluLog analyser, AbstractLoi loi) {
        if (!loi.getNom().startsWith(loi.getType().getId() + CruePrefix.SEP)) {
            analyser.addSevereError("loi.nomNotValid", loi.getNom(), loi.getType().getId(), loi.getType().getId());
        }
    }

    public static List<Loi> getAllLois(EMHScenario scenario) {
        List<Loi> lois = new ArrayList<>(scenario.getLoiConteneur().getLois());
        List<EMH> allSimpleEMH = scenario.getAllSimpleEMH();
        for (EMH emh : allSimpleEMH) {
            for (DonCalcSansPrt dcsp : emh.getDCSP()) {
                dcsp.fillWithLoi(lois);
            }
        }
        return lois;
    }

    public static List<String> getUsedBy(Loi loi, CrueConfigMetier ccm, DecimalFormatEpsilonEnum type) {
        Collection<Object> usedByCollection = loi.getUsedByCollection();
        if (CollectionUtils.isEmpty(usedByCollection)) {
            return Collections.emptyList();
        }
        List<String> res = new ArrayList<>();
        for (Object object : usedByCollection) {
            res.add(TransformerEMHHelper.transformToString(object, ccm, EnumToString.COMPLETE, type));
        }
        Collections.sort(res);
        return res;
    }

    protected static void addCorrespondance(TreeMap<String, List<String>> res, String varX, String varY) {
        List<String> other = res.get(varX);
        if (other == null) {
            other = new ArrayList<>();
            res.put(varX, other);
        }
        other.add(varY);
    }

    /**
     * @param loiFF
     * @return une loi avec abscisses/ordonnées inversées. Attention le type sera à nul.
     */
    public static LoiFF switchAbscOrdonnee(LoiFF loiFF) {
        if (loiFF == null) {
            return null;
        }
        LoiFF res = new LoiFF();
        res.setNom(loiFF.getNom());
        res.setEvolutionFF(loiFF.getEvolutionFF().inverse());
        return res;
    }

    /**
     * @param loiConteneur le conteneur de loi
     * @param typeLoi      le type a filter
     * @return les lois filtrées : list non nulle
     */
    public static List<Loi> filterDLHY(final DonLoiHYConteneur loiConteneur, EnumTypeLoi typeLoi) {
        List<Loi> lois = loiConteneur.getLois();
        return CollectionCrueUtil.select(lois, new LoiTypePredicate(typeLoi));
    }

    /**
     * @param lois    les lois a filtrer
     * @param typeLoi le type a filter
     * @return les lois filtrées : list non nulle
     */
    public static List<AbstractLoi> filterLois(final Collection<AbstractLoi> lois, EnumTypeLoi typeLoi) {
        return CollectionCrueUtil.select(lois, new LoiTypePredicate(typeLoi));
    }

    /**
     * @param lois     les lois a filtrer
     * @param typeLoi  le type a filter
     * @param typeLoi2 le type a filter (OR)
     * @return les lois filtrées : list non nulle
     */
    public static List<AbstractLoi> filterLois(final Collection<AbstractLoi> lois, EnumTypeLoi typeLoi, EnumTypeLoi typeLoi2) {
        return CollectionCrueUtil.select(lois, OrPredicate.orPredicate(new LoiTypePredicate(typeLoi), new LoiTypePredicate(typeLoi2)));
    }
}
