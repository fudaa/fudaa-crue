/**
 * *********************************************************************
 * Module: ParamNumCalcPseudoPerm.java Author: deniger Purpose: Defines the Class ParamNumCalcPseudoPerm
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

public class ParamNumCalcPseudoPerm implements Cloneable {
  private double coefRelaxQ;
  private double coefRelaxZ;
  private double crMaxFlu;
  private double crMaxTor;
  private int nbrPdtDecoup;
  private int nbrPdtMax;
  private double tolMaxQ;
  private double tolMaxZ;
  private Pdt pdt;

  public ParamNumCalcPseudoPerm(CrueConfigMetier values) {
    coefRelaxQ = values.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_COEF_RELAX_Q);
    coefRelaxZ = values.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_COEF_RELAX_Z);
    crMaxFlu = values.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_CR_MAX_FLU);
    crMaxTor = values.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_CR_MAX_TOR);
    nbrPdtDecoup = values.getDefaultIntValue(CrueConfigMetierConstants.PROP_NBR_PDT_DECOUP);
    nbrPdtMax = values.getDefaultIntValue(CrueConfigMetierConstants.PROP_NBR_PDT_MAX);
    tolMaxQ = values.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_TOL_MAX_Q);
    tolMaxZ = values.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_TOL_MAX_Z);
    pdt = PdtCst.getDefaultPdtValue(values, "pdtPerm");
  }

  @PropertyDesc(i18n = "coefRelaxQ.property")
  public final double getCoefRelaxQ() {
    return coefRelaxQ;
  }

  @PropertyDesc(i18n = "coefRelaxZ.property")
  public final double getCoefRelaxZ() {
    return coefRelaxZ;
  }

  @PropertyDesc(i18n = "crMaxFlu.property")
  public final double getCrMaxFlu() {
    return crMaxFlu;
  }

  @PropertyDesc(i18n = "crMaxTor.property")
  public final double getCrMaxTor() {
    return crMaxTor;
  }

  @PropertyDesc(i18n = "nbrPdtDecoup.property")
  public final int getNbrPdtDecoup() {
    return nbrPdtDecoup;
  }

  @PropertyDesc(i18n = "nbrPdtMax.property")
  public final int getNbrPdtMax() {
    return nbrPdtMax;
  }

  @PropertyDesc(i18n = "pdt.property", order = 0)
  public Pdt getPdt() {
    return pdt;
  }

  @PropertyDesc(i18n = "tolMaxQ.property")
  public final double getTolMaxQ() {
    return tolMaxQ;
  }

  @PropertyDesc(i18n = "tolMaxZ.property")
  public final double getTolMaxZ() {
    return tolMaxZ;
  }

  public final void setCoefRelaxQ(double newCoefRelaxQ) {
    coefRelaxQ = newCoefRelaxQ;
  }

  public final void setCoefRelaxZ(double newCoefRelaxZ) {
    coefRelaxZ = newCoefRelaxZ;
  }

  public final void setCrMaxFlu(double newCrMaxFlu) {
    crMaxFlu = newCrMaxFlu;
  }

  public final void setCrMaxTor(double newCrMaxTor) {
    crMaxTor = newCrMaxTor;
  }

  public final void setNbrPdtDecoup(int newNbrPdtDecoup) {
    nbrPdtDecoup = newNbrPdtDecoup;
  }

  public final void setNbrPdtMax(int newNbrPdtMax) {
    nbrPdtMax = newNbrPdtMax;
  }

  public void setPdt(Pdt newPdt) {
    this.pdt = newPdt;
  }

  public final void setTolMaxQ(double newTolMaxQ) {
    tolMaxQ = newTolMaxQ;
  }

  public final void setTolMaxZ(double newTolMaxZ) {
    tolMaxZ = newTolMaxZ;
  }

  @Override
  public String toString() {
    return "ParamNumCalcPseudoPerm [coefRelaxQ=" + coefRelaxQ + ", coefRelaxZ=" + coefRelaxZ + ", crMaxFlu=" + crMaxFlu
        + ", crMaxTor=" + crMaxTor + ", nbrPdtDecoup=" + nbrPdtDecoup + ", nbrPdtMax=" + nbrPdtMax + ", pdt=" + pdt
        + ", tolMaxQ=" + tolMaxQ + ", tolMaxZ=" + tolMaxZ + "]";
  }

  @Override
  protected Object clone() {
    try {
      return super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why ?");
  }

  ParamNumCalcPseudoPerm deepClone() {
    ParamNumCalcPseudoPerm cloned = (ParamNumCalcPseudoPerm) clone();
    cloned.pdt = pdt == null ? null : pdt.deepClone();
    return cloned;
  }
}
