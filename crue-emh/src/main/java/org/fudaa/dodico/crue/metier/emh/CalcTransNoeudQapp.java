package org.fudaa.dodico.crue.metier.emh;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;

/**
 * CL de type 2 en Crue9
 */
public class CalcTransNoeudQapp extends DonCLimMCommonItem implements CalcTransItem {

  /**
   * Loi HydrogrammeQapp : VarAbscisse = t VarOrdonnee = Qapp
   */
  private LoiDF hydrogrammeQapp;

  /**
   * Loi HydrogrammeQapp : VarAbscisse = t VarOrdonnee = Qapp
   */
  @PropertyDesc(i18n = "qapp.property")
  public LoiDF getHydrogrammeQapp() {
    return hydrogrammeQapp;
  }

  @Override
  public String getShortName() {
    return BusinessMessages.getString("qapp.property");
  }

  @Override
  public EnumTypeLoi getTypeLoi() {
    return EnumTypeLoi.LoiTQapp;
  }

  /**
   * Loi HydrogrammeQapp : VarAbscisse = t VarOrdonnee = Qapp
   */
  public void setHydrogrammeQapp(final LoiDF newHydrogrammeQapp) {
    if (this.hydrogrammeQapp != null) {
      this.hydrogrammeQapp.unregister(this);
    }

    hydrogrammeQapp = newHydrogrammeQapp;
    if (this.hydrogrammeQapp != null) {
      this.hydrogrammeQapp.register(this);
    }
  }

  @Visibility(ihm = false)
  @Override
  public Loi getLoi() {
    return getHydrogrammeQapp();
  }
  @Override
  public void setLoi(final Loi newLoi) {
    setHydrogrammeQapp((LoiDF) newLoi);
  }

  CalcTransNoeudQapp deepClone() {
    try {
      return (CalcTransNoeudQapp) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransNoeudNiveauContinuTarage.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }
}
