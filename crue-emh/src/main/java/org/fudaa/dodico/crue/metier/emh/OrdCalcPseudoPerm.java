package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

import java.util.Collections;
import java.util.List;

public abstract class OrdCalcPseudoPerm extends OrdCalcPerm {

  private PrendreClicheFinPermanent prendreClicheFinPermanent = new PrendreClicheFinPermanent();
  private CalcPseudoPerm calcPseudoPerm;

  public CalcPseudoPerm getCalcPseudoPerm() {
    return calcPseudoPerm;
  }

  @Override
  public CalcPseudoPerm getCalc() {
    return calcPseudoPerm;
  }

  @Override
  public List getCliches() {
    return Collections.singletonList(prendreClicheFinPermanent);
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  public boolean isClicheEmpty() {
    return prendreClicheFinPermanent == null || prendreClicheFinPermanent.isEmpty();
  }

  @PropertyDesc(i18n = "PrendreClicheFinPermanent.property")
  public PrendreClicheFinPermanent getPrendreClicheFinPermanent() {
    return prendreClicheFinPermanent;
  }

  @Override
  public OrdCalc deepCloneButNotCalc(Calc newCalc) {
    try {
      OrdCalcPseudoPerm res = (OrdCalcPseudoPerm) super.clone();
      if (prendreClicheFinPermanent != null) {
        res.prendreClicheFinPermanent = prendreClicheFinPermanent.clone();
      }
      res.setCalcPseudoPerm((CalcPseudoPerm) newCalc);
      return res;
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why");
  }

  public void setPrendreClicheFinPermanent(PrendreClicheFinPermanent prendreClicheFinPermanent) {
    this.prendreClicheFinPermanent = prendreClicheFinPermanent;
  }

  public void setCalcPseudoPerm(CalcPseudoPerm newCalcPseudoPerm) {
    this.calcPseudoPerm = newCalcPseudoPerm;
  }
}
