package org.fudaa.dodico.crue.metier.comparator;

import java.util.Comparator;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;

/**
 * Tri de Amont a aval.
 *
 * @author deniger
 */
public class ComparatorRelationEMHSectionDansBranche implements Comparator<RelationEMHSectionDansBranche> {

  private PropertyEpsilon eps;

  /**
   * @param property
   */
  public ComparatorRelationEMHSectionDansBranche(CrueConfigMetier props) {
    super();
    if (props != null) {
      this.eps = props.getEpsilon("xp");
    }
  }

  /**
   * @param eps
   */
  public ComparatorRelationEMHSectionDansBranche(PropertyEpsilon eps) {
    super();
    this.eps = eps;
  }

  @Override
  public int compare(final RelationEMHSectionDansBranche o1, final RelationEMHSectionDansBranche o2) {

    if (o1 == o2) {
      return 0;
    }
    if (o1 == null) {
      return -1;
    }
    if (o2 == null) {
      return 1;
    }
    boolean xpEquals = isEquals(o1, o2);
    if (!xpEquals) {
      return o1.getXp() > o2.getXp() ? 1 : -1;
    }
    return o1.getPos().getOrdre() - o2.getPos().getOrdre();

  }

  protected boolean isEquals(final RelationEMHSectionDansBranche o1, final RelationEMHSectionDansBranche o2) {
    if (eps == null) {
      return o1.getXp() == o2.getXp();
    }
    return eps.isSame(o1.getXp(), o2.getXp());
  }

  /**
   * @return the eps
   */
  public PropertyEpsilon getEps() {
    return eps;
  }
}