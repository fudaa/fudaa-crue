package org.fudaa.dodico.crue.metier.comparator;

import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.metier.emh.Point2D;

/**
 *
 * @author deniger
 */
public class Point2dAbscisseComparator extends SafeComparator<Point2D> {

  private final PropertyEpsilon epsilon;

  public Point2dAbscisseComparator(PropertyEpsilon epsilon) {
    this.epsilon = epsilon;
  }

  @Override
  protected int compareSafe(Point2D o1, Point2D o2) {
    double x1 = o1.getAbscisse();
    double x2 = o2.getAbscisse();
    if (epsilon.isSame(x1, x2)) {
      return 0;
    }
    return x1 > x2 ? 1 : -1;
  }
}
