package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.SeveriteManager;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;

public class Trace implements ToStringTransformable {

  private boolean sortieEcran;
  private boolean sortieFichier;
  private String verbositeEcran;
  private String verbositeFichier;
  public static final String PROP_VERBOSITE_ECRAN = "verbositeEcran";
  public static final String PROP_VERBOSITE_FICHIER = "verbositeFichier";

  /**
   *
   */
  public Trace() {
    sortieEcran = true;
    sortieFichier = true;
    verbositeEcran = SeveriteManager.INFO;
    verbositeFichier = SeveriteManager.INFO;
  }

  /**
   * @return the sortieEcran
   */
  @PropertyDesc(i18n = "sortieEcran.property", order = 0)
  public boolean getSortieEcran() {
    return sortieEcran;
  }

  /**
   * @return the sortieFichier
   */
  @PropertyDesc(i18n = "sortieFichier.property", order = 2)
  public boolean getSortieFichier() {
    return sortieFichier;
  }

  /**
   * @return the verbositeEcran
   */
  @PropertyDesc(i18n = "verbositeEcran.property", order = 1)
  public String getVerbositeEcran() {
    return verbositeEcran;
  }

  /**
   * @return the verbositeFichier
   */
  @PropertyDesc(i18n = "verbositeFichier.property", order = 3)
  public String getVerbositeFichier() {
    return verbositeFichier;
  }

  /**
   * @param sortieEcran the sortieEcran to set
   */
  public void setSortieEcran(boolean sortieEcran) {
    this.sortieEcran = sortieEcran;
  }

  /**
   * @param sortieFichier the sortieFichier to set
   */
  public void setSortieFichier(boolean sortieFichier) {
    this.sortieFichier = sortieFichier;
  }

  /**
   * @param verbositeEcran the verbositeEcran to set
   */
  public void setVerbositeEcran(String verbositeEcran) {
    this.verbositeEcran = verbositeEcran;
  }

  /**
   * @param verbositeFichier the verbositeFichier to set
   */
  public void setVerbositeFichier(String verbositeFichier) {
    this.verbositeFichier = verbositeFichier;
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    if (EnumToString.OVERVIEW.equals(format)) {
      return getClass().getSimpleName();
    }
    return getClass().getSimpleName() + " [sortieEcran=" + sortieEcran + ", sortieFichier" + sortieFichier
            + ", verbositeEcran=" + verbositeEcran + ", verbositeFichier="
            + verbositeFichier + "]";
  }

  public void initWith(Trace trace) {
    setSortieEcran(trace.getSortieEcran());
    setSortieFichier(trace.getSortieFichier());
    setVerbositeEcran(trace.getVerbositeEcran());
    setVerbositeFichier(trace.getVerbositeFichier());

  }
}
