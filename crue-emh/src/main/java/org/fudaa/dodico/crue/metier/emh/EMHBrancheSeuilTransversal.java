package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 * type 2 en Crue9
 * 
 */
public class EMHBrancheSeuilTransversal extends CatEMHBranche {

  public EMHBrancheSeuilTransversal(final String nom) {
    super(nom);
  }
  
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public EnumBrancheType getBrancheType() {
    return EnumBrancheType.EMHBrancheSeuilTransversal;
  }


  @Override
  @Visibility(ihm = false)
  public EMHBrancheSeuilTransversal copyShallowFirstLevel() {
    return  copyPrimitiveIn(new EMHBrancheSeuilTransversal(getNom()));
  }
}
