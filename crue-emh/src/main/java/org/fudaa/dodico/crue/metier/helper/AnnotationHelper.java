package org.fudaa.dodico.crue.metier.helper;

import java.lang.reflect.Method;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.annotation.Visibility;

/**
 * @author deniger
 */
public class AnnotationHelper {


  /**
   * @param prop
   * @param objectClass
   * @return true si pas d'annotation ou si l'annotation
   * @Visibilty ihm=true.
   */
  @SuppressWarnings("unchecked")
  public static boolean isIhmVisible(final String prop, final Class objectClass) {
    final Method method = getGetterMethod(prop, objectClass);
    if (method == null) {
      return true;
    }
    final Visibility annotation = method.getAnnotation(Visibility.class);
    return annotation == null ? true : annotation.ihm();
  }

  @SuppressWarnings("unchecked")
  private static Method getGetterMethod(final String prop, final Class objectClass) {
    Method method = null;
    final String capitalize = StringUtils.capitalize(prop);
    try {
      method = objectClass.getMethod("get" + capitalize, (Class[]) null);
    } catch (final Exception e) {
    }
    try {
      if (method == null) {
        method = objectClass.getMethod("is" + capitalize, (Class[]) null);
      }
    } catch (final Exception e) {
    }
    return method;
  }
}
