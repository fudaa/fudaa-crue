package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 * type 12 en Crue9
 * 
 */
public class EMHBrancheNiveauxAssocies extends CatEMHBranche {

  public EMHBrancheNiveauxAssocies(final String nom) {
    super(nom);
  }
  
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public EnumBrancheType getBrancheType(){
    return EnumBrancheType.EMHBrancheNiveauxAssocies;
  }

  @Override
  @Visibility(ihm = false)
  public EMHBrancheNiveauxAssocies copyShallowFirstLevel() {
    return  copyPrimitiveIn(new EMHBrancheNiveauxAssocies(getNom()));
  }

}
