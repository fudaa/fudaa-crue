/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformerDefault;

public class ElemOrifice implements Cloneable {

  public static final String PROP_COEFCTRLIM = "coefCtrLim";
  public static final String PROP_HAUT = "haut";
  public static final String PROP_SENSORIFICE = "sensOrifice";
  private double coefCtrLim;
  private double largeur;
  private double zseuil;
  private double haut;
  private double coefD;
  private EnumSensOrifice sensOrifice;

  public ElemOrifice(final CrueConfigMetier defaults) {
    coefCtrLim = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_COEF_CTR_LIM);
    largeur = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_LARGEUR);
    zseuil = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_ZSEUIL);
    haut = defaults.getDefaultDoubleValue("haut");
    coefD = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_COEF_D);
    final String enumValue = defaults.getDefaultStringEnumValue("sensOrifice");
    sensOrifice = StringUtils.isEmpty(enumValue) ? EnumSensOrifice.BIDIRECT : EnumSensOrifice.valueOf(enumValue);
  }

  @PropertyDesc(i18n = "coefCtrLim.property")
  public double getCoefCtrLim() {
    return coefCtrLim;
  }

  @Override
  public ElemOrifice clone() {
    try {
      return (ElemOrifice) super.clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(ElemOrifice.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("why");
  }

  public static ToStringTransformer createTransformer(final CrueConfigMetier props) {
    final ToStringTransformerDefault res = new ToStringTransformerDefault(props, Arrays.asList("coefCtrLim", CrueConfigMetierConstants.PROP_COEF_D, "haut",
            CrueConfigMetierConstants.PROP_LARGEUR, "sensOrifice", CrueConfigMetierConstants.PROP_ZSEUIL),DecimalFormatEpsilonEnum.COMPARISON);
    res.setAddClassName(true);
    return res;
  }

  @Override
  public String toString() {
    return "ElemOrifice [coefCtrLim=" + coefCtrLim + ", coefD=" + coefD + ", haut=" + haut + ", largeur=" + largeur
            + ", sensOrifice=" + sensOrifice + ", zseuil=" + zseuil + "]";
  }

  public void setCoefCtrLim(final double newCoefCtrLim) {
    coefCtrLim = newCoefCtrLim;
  }

  @PropertyDesc(i18n = "largeur.property")
  public double getLargeur() {
    return largeur;
  }

  public void setLargeur(final double newLargeur) {
    largeur = newLargeur;
  }

  @PropertyDesc(i18n = "zseuil.property")
  public double getZseuil() {
    return zseuil;
  }

  public void setZseuil(final double newZseuil) {
    zseuil = newZseuil;
  }

  @PropertyDesc(i18n = "haut.property")
  public double getHaut() {
    return haut;
  }

  public void setHaut(final double newHaut) {
    haut = newHaut;
  }

  @PropertyDesc(i18n = "coefD.property")
  public double getCoefD() {
    return coefD;
  }

  public void setCoefD(final double newCoefD) {
    coefD = newCoefD;
  }

  @PropertyDesc(i18n = "sensOrifice.property")
  public EnumSensOrifice getSensOrifice() {
    return sensOrifice;
  }

  public void setSensOrifice(final EnumSensOrifice newSens) {
    sensOrifice = newSens;
  }
}
