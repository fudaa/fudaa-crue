package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

public abstract class DonCalcSansPrtBrancheQ extends DonCalcSansPrt {

  /**
   * les propriétés JAVA
   */
  public static final String PROP_QLIMINF = "QLimInf";//2 majuscules car les beans traitent cette propriété ainsi...
  /**
   * les propriétés JAVA
   */
  public static final String PROP_QLIMSUP = "QLimSup";//2 majuscules car les beans traitent cette propriété ainsi...
  private double qLimInf;
  private double qLimSup;

  public DonCalcSansPrtBrancheQ(final CrueConfigMetier defaults) {
    //les prop issues du CCM
    qLimInf = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_Q_LIM_INF);
    qLimSup = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_Q_LIM_SUP);
  }

  @PropertyDesc(i18n = "qLimInf.property")
  public final double getQLimInf() {
    return qLimInf;
  }

  /**
   * @param newQLimInf
   */
  public final void setQLimInf(final double newQLimInf) {
    qLimInf = newQLimInf;
  }

  @PropertyDesc(i18n = "qLimSup.property")
  public final double getQLimSup() {
    return qLimSup;
  }

  public final void setQLimSup(final double newQLimSup) {
    qLimSup = newQLimSup;
  }
}
