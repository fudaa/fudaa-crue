package org.fudaa.dodico.crue.metier.aoc;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

/**
 * Created by deniger on 28/06/2017.
 */
public enum EnumAocCalageType implements ToStringInternationalizable {

    SEUL("Seul"), TEST_REPETABILITE("TestRepetabilite"), ANALYSE_SENSIBILITE("AnalyseSensibilite"), VALIDATION_CROISEE("ValidationCroisee");

    private final String identifiant;

    EnumAocCalageType(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    @Override
    public String geti18n() {
        return BusinessMessages.getString("aoc.calageType." + name().toLowerCase() + ".shortName");
    }

    @Override
    public String geti18nLongName() {
        return BusinessMessages.getString("aoc.calageType." + name().toLowerCase() + ".longName");
    }
}
