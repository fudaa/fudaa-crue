package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

import java.util.*;

/**
 * @author deniger
 */
public class IdRegistry {
  private long idx;
  private long idxCalcul;
  final Map<Long, EMH> emhByUid = new HashMap<>();

  public void register(EMH emh) {
    if (!emh.isUiIdSet()) {
      emh.setUiId(Long.valueOf(idx++));
    }
    emhByUid.put(emh.getUiId(), emh);
  }

  public void register(Calc calc) {
    if (!calc.isUiIdSet()) {
      calc.setUiId(Long.valueOf(idxCalcul++));
    }
  }

  public EMH getEmh(Long uid) {
    return emhByUid.get(uid);
  }

  public EMH findByName(String name) {
    Collection<EMH> values = emhByUid.values();
    for (EMH emh : values) {
      if (emh.getNom().equals(name)) {
        return emh;
      }
    }
    return null;
  }

  public boolean containEMH(Long uid) {
    return emhByUid.containsKey(uid);
  }

  public void unregister(EMH emh) {
    emhByUid.remove(emh.getUiId());
  }

  public void registerAll(EMHScenario scenario) {
    List<EMH> allSimpleEMH = scenario.getAllEMH();
    for (EMH emh : allSimpleEMH) {
      register(emh);
    }
    if (scenario.getDonCLimMScenario() != null) {
      for (Calc calc : scenario.getDonCLimMScenario().getCalc()) {
        register(calc);
      }
    }
  }

  public void registerAll(EMHSousModele sousModele) {
    register(sousModele);
    for (EMH emh : sousModele.getAllSimpleEMH()) {
      register(emh);
    }
  }

  public List<EMH> getEMHs(EnumCatEMH emhCat) {
    Collection<EMH> values = emhByUid.values();
    return EMHHelper.selectEMHS(values, emhCat);
  }

  public List<EMH> getAllSimpleEMH() {
    Collection<EMH> values = emhByUid.values();
    final List<EMH> res = new ArrayList<>(values.size());
    for (EMH emh : values) {
      if (!emh.getCatType().isContainer()) {
        res.add(emh);
      }
    }
    return res;
  }

  public Map<String, EMH> getEmhByNom() {
    return TransformerHelper.toMapOfNom(emhByUid.values());
  }
  public Map<Long, EMH> getEmhById() {
    return new HashMap<>(emhByUid);
  }

  public static void install(EMHScenario scenario) {
    if (scenario.getIdRegistry() == null) {
      IdRegistry idRegistry = new IdRegistry();
      idRegistry.registerAll(scenario);
      scenario.setIdRegistry(idRegistry);
    }
  }
}
