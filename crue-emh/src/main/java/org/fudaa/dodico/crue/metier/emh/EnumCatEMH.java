/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

/**
 * Permet de typer les différents EMH
 *
 */
public enum EnumCatEMH implements ToStringInternationalizable {

  BRANCHE(false, "CatEMHBranche"), CASIER(false, "CatEMHCasier"), NOEUD(false, "CatEMHNoeud"),
  SECTION(false, "CatEMHSection"), MODELE(true, "EMHModeleBase"), SOUS_MODELE(true, "EMHSousModele"), MODELE_ENCHAINEMENT(true, "EMHModeleEnchainement"),
  SOUS_MODELE_ENCHAINEMENT(true, "EMHSousModeleEnchainement"), SCENARIO(true, "EMHScenario");
  private final boolean container;
  private final String i18nKey;

  EnumCatEMH(final boolean container, final String i18nKey) {
    this.container = container;
    this.i18nKey = i18nKey;
  }

  @Override
  public String geti18n() {
    return BusinessMessages.getString(i18nKey + ".shortName");
  }
  
  public String geti18nPlural() {
    return BusinessMessages.getString(i18nKey + ".shortNamePlural");
  }

  public String getI18nKey() {
    return i18nKey;
  }

  @Override
  public String geti18nLongName() {
    return BusinessMessages.getString(i18nKey + ".longName");
  }

  public boolean isContainer() {
    return container;
  }
}
