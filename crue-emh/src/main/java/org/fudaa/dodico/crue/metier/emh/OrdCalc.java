/**
 * *********************************************************************
 * Module: OrdCalc.java Author: battista Purpose: Defines the Class OrdCalc *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.List;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;

/**
 */
public abstract class OrdCalc implements ToStringTransformable, Cloneable {

  /**
   * @return true si transitoire
   */
  public abstract boolean isTransitoire();

  public final boolean isPseudoPermanent() {
    return !isTransitoire();
  }

  @Override
  protected Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  /**
   * DeepClone sauf les Calc.
   *
   * @return
   */
  public abstract OrdCalc deepCloneButNotCalc(Calc newCalc);

  public abstract Calc getCalc();

  public abstract List getCliches();

  public final String getType() {
    return getClass().getSimpleName();
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {

    return getClass().getSimpleName() + " " + getCalc().getId();
  }
}