/*
 * Licence GPL Copyright
 */
package org.fudaa.dodico.crue.metier;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonCLimMScenario;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.DonFrtConteneur;
import org.fudaa.dodico.crue.metier.emh.DonLoiHYConteneur;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoBatiCasier;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilCasier;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHNoeudNiveauContinu;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.OrdCalcScenario;
import org.fudaa.dodico.crue.metier.emh.OrdPrtCIniModeleBase;
import org.fudaa.dodico.crue.metier.emh.OrdPrtGeoModeleBase;
import org.fudaa.dodico.crue.metier.emh.OrdPrtReseau;
import org.fudaa.dodico.crue.metier.emh.OrdResScenario;
import org.fudaa.dodico.crue.metier.emh.ParamCalcScenario;
import org.fudaa.dodico.crue.metier.emh.ParamNumModeleBase;
import org.fudaa.dodico.crue.metier.emh.Pdt;
import org.joda.time.Duration;

/**
 * @author deniger
 */
public interface CrueData {

  /**
   * Ajoute automatiquement l'objet emh dans la bonne structure.
   *
   * @param object
   */
  void add(final EMH object);

  EMHNoeudNiveauContinu createNode(String id);

  /**
   * * Pas efficace mais utilise pour Crue 9 dans le cas d'id multipe
   *
   * @param nom le nom de la branche cherchee
   * @return la branche correspondante ou null si non trouvee
   */
  CatEMHBranche findBrancheByReference(String nom);

  Calc findCalcByNom(String nom);

  /**
   * * Pas efficace mais utilise pour Crue 9 dans le cas d'id multipe
   *
   * @param nom le nom du casier cherchee
   * @return le casier correspondant ou null si non trouve
   */
  CatEMHCasier findCasierByReference(String nom);

  /**
   * @param nom le nom de l'EMH a cherche
   * @return l'EMH portant le nom donnée ou null si non trouve.
   */
  EMH findEMHByReference(String ref);

  /**
   * Pas efficace mais utilise pour Crue 9 dans le cas d'id multipe
   *
   * @param nom le nom du noeud cherche
   * @return le noeuc correspondant ou null si non trouve
   */
  CatEMHNoeud findNoeudByReference(String nom);

  /**
   * * Pas efficace mais utilise pour Crue 9 dans le cas d'id multipe
   *
   * @param nom le nom de la section cherchee
   * @return la section correspondante ou null si non trouve
   */
  CatEMHSection findSectionByReference(String nom);

  /**
   *
   * @return EMHs simples par noms
   */
  Map<String, EMH> getSimpleEMHByNom();
  Map<String, EMH> getEMHByNom();

  /**
   *
   * @return EMHs simple par Id.
   */
  Map<String, EMH> getSimpleEMHById();

  /**
   * Retourne tous les objets EMH
   *
   * @return la liste des emh.
   */
  List<EMH> getAllSimpleEMH();

  List<CatEMHBranche> getBranches();

  /**
   * @return les branches de saint-venant
   */
  List<EMHBrancheSaintVenant> getBranchesSaintVenant();

  List<CatEMHCasier> getCasiers();

  DonCLimMScenario getConditionsLim();

  CrueConfigMetier getCrueConfigMetier();

  DonFrtConteneur getFrottements();

  DonLoiHYConteneur getLoiConteneur();

  /**
   * @return the lois
   */
  List<Loi> getLois();

  EMHModeleBase getModele();

  List<CatEMHNoeud> getNoeuds();

  /**
   * @return the ordCalc
   */
  OrdCalcScenario getOCAL();

  OrdPrtGeoModeleBase getOPTG();

  void getOPTG(OrdPrtGeoModeleBase optg);

  /**
   * @return setter sur les methodes d'interpolation OPTI
   */
  OrdPrtReseau getOPTR();

  /**
   * @return setter sur les methodes d'interpolation OPTI
   */
  OrdPrtCIniModeleBase getOPTI();

  void setOPTI(OrdPrtCIniModeleBase opti);

  /**
   * @return the ordRes
   */
  OrdResScenario getORES();

  /**
   * @param lois the lois to set
   */
  ParamCalcScenario getPCAL();

  ParamNumModeleBase getPNUM();

  ItemVariable getProperty(String id);

  EMHScenario getScenarioData();

  List<CatEMHSection> getSections();

  /**
   * @return le sous-modele
   */
  EMHSousModele getSousModele();

  /**
   * @return the sto
   */
  StoContent getSto();

  /**
   * A utiliser lors de la lecture uniquement.
   *
   * @param nom l'identifiant du profil
   * @return le DonPrtGeoProfilSection utilise
   */
  DonPrtGeoBatiCasier getDefinedBatiCasier(final String nom);

  /**
   * A utiliser lors de la lecture uniquement.
   *
   * @param nom l'identifiant du profil
   * @return le DonPrtGeoProfilSection utilise
   */
  DonPrtGeoProfilCasier getDefinedProfilCasier(final String nom);

  /**
   * A utiliser lors de la lecture uniquement.
   *
   * @param nom l'identifiant du profil
   * @return le DonPrtGeoProfilSection utilise
   */
  DonPrtGeoProfilSection getDefinedProfilSection(final String nom);

  /**
   * les profils sections définies mais non utilisés.
   */
  Set<String> getDefinedButNotUsedProfilSection();

  /**
   * les profils casiers définis mais non utilisés.
   */
  Set<String> getDefinedButNotUsedProfilCasier();

  /**
   * les bati casiers définis mais non utilisés.
   */
  Set<String> getDefinedButNotUsedBatiCasier();

  /**
   * boolean pour indiquer les fichiers crue 9 contiennent des cartes distmax. Dans ce cas, la lecture des fichiers de resultats ne doivent pas se
   * faire.
   *
   * @return true si contient distmax
   */
  boolean isCrue9ContientDistmax();

  /**
   * Ajoute des lectures successives de données crue data en une seule. Exemple, lecture de plusieurs fichiers DRSO.
   *
   * @return
   */
  boolean mergeWithAnotherCrueData(CrueData data);

  /**
   * @param batiCasier
   */
  void registerDefinedBatiCasier(DonPrtGeoBatiCasier batiCasier);

  /**
   * @param casierProfil un casier définit dans le fichiert dptg en tant que bibliothèque
   */
  void registerDefinedCasierProfil(DonPrtGeoProfilCasier casierProfil);

  /**
   * @param sectionProfil
   */
  void registerDefinedSectionProfil(DonPrtGeoProfilSection sectionProfil);

  /**
   * Utiliser lors de la lecture: permet d'enregistrer les bati utilises par EMH.
   *
   * @param emh
   * @param profilId
   */
  void registerUseBatiCasier(final EMH emh, final String profilId);

  /**
   * Utiliser lors de la lecture: permet d'enregistrer les profils de casier utilises par EMH.
   *
   * @param emh
   * @param profilId
   */
  void registerUseCasierProfil(final EMH emh, final String profilId);

  /**
   * Utiliser lors de la lecture: permet d'enregistrer les profils utilises par EMH.
   *
   * @param emh
   * @param profilId
   */
  void registerUseSectionProfil(final EMH emh, final String profilId);

  void setConditionsLim(final DonCLimMScenario donCLimMScenario);

  /**
   * boolean pour indiquer les fichiers crue 9 contiennent des cartes distmax. Dans ce cas, la lecture des fichiers de resultats ne doivent pas se
   * faire.
   *
   * @param crue9ContientDistmax true si contient distmax
   */
  void setCrue9ContientDistmax(boolean crue9ContientDistmax);

  void setFrottements(final List<DonFrt> frottements);

  /**
   * @return getter sur les methodes d'interpolation OPTI
   */
  void setMethodesInterpolation(OrdPrtCIniModeleBase methodesInterpolation);

  void setOPTR(OrdPrtReseau optr);

  /**
   * @param sto the sto to set
   */
  void setSto(final StoContent sto);

  void sort();

  OrdPrtGeoModeleBase getOrCreateOPTG();

  OrdPrtReseau getOrCreateOPTR();

  OrdCalcScenario getOrCreateOCAL();

  ParamCalcScenario getOrCreatePCAL();

  OrdPrtCIniModeleBase getOrCreateOPTI();

  OrdResScenario getOrCreateORES();

  ParamNumModeleBase getOrCreatePNUM();

  /**
   * utilisé pour retro compatibilité avec version 1.1.1. Cette donnée sera utilisée ensuite pour OCAL.
   *
   * @param createPdtCst
   */
  void setOldPCALPdtRes(Pdt createPdtCst);

  Pdt getOldPCALPdtRes();

  /**
   * utilisé pour retro compatibilité avec version 1.1.1. Cette donnée sera utilisée ensuite pour OCAL.
   *
   * @param duration
   */
  void setOldPCALDureeSce(Duration duration);

  Duration getOldPCALDureeSce();

  /**
   * Pendant le chanrgement, les données dpti peuvent être modifiées ( drso modifié hors de fudaa-crue par exemple).
   * Voir CRUE-725
   */
  void setDptiChangedDuringLoading();
}
