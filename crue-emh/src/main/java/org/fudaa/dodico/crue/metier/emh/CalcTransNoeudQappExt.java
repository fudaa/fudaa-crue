package org.fudaa.dodico.crue.metier.emh;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * hydrogramme externe
 */
public class CalcTransNoeudQappExt extends DonCLimMCommonItem {

  public static class Data {
    private final String nomFic;

    private final String section;

    private final String resCalcTrans;

    public Data(final String nomFic, final String section, final String resCalcTrans) {
      this.nomFic = nomFic;
      this.section = section;
      this.resCalcTrans = resCalcTrans;
    }

    public String getNomFic() {
      return nomFic;
    }

    public String getResCalcTrans() {
      return resCalcTrans;
    }

    public String getSection() {
      return section;
    }

    @Override
    public String toString() {
      if (resCalcTrans == null) {
        return section;
      }
      return section + " / " + resCalcTrans;
    }
  }

  private String nomFic;

  private String section;

  private String resCalcTrans;


  @Override
  public String getShortName() {
    return BusinessMessages.getString("qappExt.property");
  }


  @Visibility(ihm = false)
  public String getDesc() {
    if (resCalcTrans == null) {
      return section;
    }
    return section + " / " + resCalcTrans;
  }

  @Visibility(ihm = false)
  public Data getData() {
    return new Data(nomFic, section, resCalcTrans);
  }

  public void setData(Data data) {
    if (data != null) {
      setNomFic(data.getNomFic());
      setSection(data.getSection());
      setResCalcTrans(data.getResCalcTrans());
    }
  }


  CalcTransNoeudQappExt deepClone() {
    try {
      return (CalcTransNoeudQappExt) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransNoeudNiveauContinuTarage.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }

  @PropertyDesc(i18n = "nomFic.property")
  public String getNomFic() {
    return nomFic;
  }

  public void setNomFic(String nomFic) {
    this.nomFic = nomFic;
  }

  @PropertyDesc(i18n = "section.property")
  public String getSection() {
    return section;
  }

  public void setSection(String section) {
    this.section = section;
  }

  @PropertyDesc(i18n = "calcTrans.property")
  public String getResCalcTrans() {
    return resCalcTrans;
  }

  public void setResCalcTrans(String resCalcTrans) {
    if(StringUtils.isBlank(resCalcTrans)){
      this.resCalcTrans=null;
    }else{
      this.resCalcTrans = resCalcTrans;
    }
  }
}
