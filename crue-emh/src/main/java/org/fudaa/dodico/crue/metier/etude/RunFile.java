/**
 * 
 */
package org.fudaa.dodico.crue.metier.etude;

import java.io.File;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * @author CANEL Christophe
 *
 */
public class RunFile {
  private final ManagerEMHModeleBase container;
  private final File file;
  private final File runFileDir;
  private final CrueFileType type;
  private final String key;
  
  public RunFile(String key,ManagerEMHModeleBase container, File file, CrueFileType type) {
    this.container = container;
    this.file = file;
    this.type = type;
    this.runFileDir = file.getParentFile();
    this.key= key;
  }
  
  public String getKey() {
    return key;
  }

  public ManagerEMHModeleBase getContainer() {
    return container;
  }
  
  
  public File getFile() {
    return file;
  }
  
  
  public CrueFileType getType() {
    return type;
  }
  
  public File getRunFileDir() {
    return runFileDir;
  }
}
