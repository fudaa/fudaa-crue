package org.fudaa.dodico.crue.metier.emh;

import java.util.ArrayList;
import java.util.List;

/**
 * Point evolution FF.
 *
 * @author Adrien Hadoux
 */
public class EvolutionTF {

    private List<PtEvolutionTF> mpoints;

    public EvolutionTF() {
    }

    public EvolutionTF(final List<PtEvolutionTF> mpoints) {
        setPtEvolutionTF(mpoints);
    }


    public EvolutionTF copy() {
        EvolutionTF copy = new EvolutionTF();
        if (mpoints != null) {
            copy.mpoints = new ArrayList<>();
            for (PtEvolutionTF point : mpoints) {
                copy.mpoints.add(point.copy());
            }
        }
        return copy;
    }

    public List<PtEvolutionTF> getPtEvolutionTF() {
        return mpoints;
    }

    public final void setPtEvolutionTF(final List<PtEvolutionTF> mpoints) {
        this.mpoints = mpoints;
    }
}
