/*
GPL 2
 */
package org.fudaa.dodico.crue.metier.emh.ui;

import org.fudaa.dodico.crue.metier.emh.OrdCalc;

/**
 * A utiliser par la partie éditeur uniquement pour stocker les éditions des utilisateurs: pour chaque calcul permet de perister l'ordre de calcul
 * associé.
 *
 * @author Frederic Deniger
 */
public class CalcOrdCalcUiState {

  /**
   *
   */
  private String calcId;
  private OrdCalc ordCalc;


  /**
   *
   * @param calcId l'identifiant du calcul correspond. N'est pas affecté à l'ordCacl-
   * @param ordCalc l'ordre de calcul. Ne doit pas avoir de calc de setter comme c'est uniquement une donnée ui de persistence.
   */
  public CalcOrdCalcUiState(final String calcId, final OrdCalc ordCalc) {
    this.calcId = calcId;
    this.ordCalc = ordCalc;
  }

  public String getCalcId() {
    return calcId;
  }

  public void setCalcId(final String calcId) {
    this.calcId = calcId;
  }

  public OrdCalc getOrdCalc() {
    return ordCalc;
  }

  /**
   *
   * @param ordCalc l'ordre de calcul. Ne doit pas avoir de calc de setter comme c'est uniquement une donnée ui de persistence.
   */
  public void setOrdCalc(final OrdCalc ordCalc) {
    this.ordCalc = ordCalc;
    assert ordCalc.getCalc() == null;
  }

}
