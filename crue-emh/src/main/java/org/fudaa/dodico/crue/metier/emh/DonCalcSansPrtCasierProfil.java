package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

import java.util.Collection;

/**
 * type 20 Crue9
 */
public class DonCalcSansPrtCasierProfil extends DonCalcSansPrt {
  private double coefRuis;

  public DonCalcSansPrtCasierProfil(CrueConfigMetier defaults) {
    coefRuis = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_COEF_RUIS);
  }

  @Override
  public DonCalcSansPrt cloneDCSP() {
    try {
      return (DonCalcSansPrt) clone();
    } catch (CloneNotSupportedException e) {
    }
    return null;
  }

  public double getCoefRuis() {
    return coefRuis;
  }

  @Override
  public void fillWithLoi(Collection<Loi> target) {
  }

  /**
   * @param newCoefRuis
   */
  public void setCoefRuis(final double newCoefRuis) {
    coefRuis = newCoefRuis;
  }
}
