/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier;

import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumBrancheType;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;

/**
 *
 * @author Frederic Deniger
 */
public class OrdResKey implements ToStringInternationalizable {

  private final EnumCatEMH catEMH;
  private final EnumBrancheType brancheType;

  public OrdResKey(EnumBrancheType brancheType) {
    this.catEMH = EnumCatEMH.BRANCHE;
    this.brancheType = brancheType;
  }

  public OrdResKey(EMH emh) {
    this.catEMH = emh.getCatType();
    if (EnumCatEMH.BRANCHE.equals(catEMH)) {
      this.brancheType = ((CatEMHBranche) emh).getBrancheType();
    } else {
      this.brancheType = null;
    }
  }

  public OrdResKey(EnumCatEMH catEMH) {
    this.catEMH = catEMH;
    assert catEMH != EnumCatEMH.BRANCHE;
    this.brancheType = null;
  }

  @Override
  public String geti18n() {
    if (brancheType != null) {
      return brancheType.geti18n();
    }
    return catEMH.geti18n();
  }

  @Override
  public String geti18nLongName() {
    if (brancheType != null) {
      return brancheType.geti18nLongName();
    }
    return catEMH.geti18nLongName();
  }

  public EnumBrancheType getBrancheType() {
    return brancheType;
  }

  public EnumCatEMH getCatEMH() {
    return catEMH;
  }

  @Override
  public int hashCode() {
    int hash = 5;
    if (catEMH != null) {
      hash = hash + catEMH.hashCode();
    }
    if (brancheType != null) {
      hash = hash + brancheType.hashCode();
    }
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final OrdResKey other = (OrdResKey) obj;
    if (this.catEMH != other.catEMH) {
      return false;
    }
    if (this.brancheType != other.brancheType) {
      return false;
    }
    return true;
  }
}
