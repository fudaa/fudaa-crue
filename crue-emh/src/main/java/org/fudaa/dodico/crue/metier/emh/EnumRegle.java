/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 * Les variables utilisable dans Crue Attention les noms des enums régles sont utilisés pour construire les noms des regles. Si
 * nous renommer, il faut voir pour reconstruire les nom.
 * le nom de l'enum
 */
public enum EnumRegle {

  /**
   * Valeur de paramètre : hauteur limite de détection des marches d'escalier aux nœuds lors du prétraitement géométrique. Valeur
   * par défaut : 0,2 m
   */
  DECAL("pm_Decal", EnumInfosEMH.ORD_PRT_GEO_MODELE_BASE),
  /**
   * Valeur de paramètre : largeur limite de détection des éléments de seuil trop longs lors du prétraitement géométrique. Valeur
   * par défaut : 100 m
   */
  LARG_SEUIL("pm_LargSeuil", EnumInfosEMH.ORD_PRT_GEO_MODELE_BASE),
  /**
   * Valeur de paramètre : valeur limite de détection des pas d'espace trop grands entre sections de calcul successives lors du
   * prétraitement géométrique. Valeur par défaut : 200 m
   */
  PDX_MAX("pm_PdxMax", EnumInfosEMH.ORD_PRT_GEO_MODELE_BASE),
  /**
   * Valeur de paramètre : pente longitudinale limite de détection des pentes fortes entre sections lors du prétraitement
   * géométrique. Valeur par défaut : 0,01 m/m. ATTENTION, l'unité dans Crue9 est m/100m : il faut donc diviser les valeurs issues
   * de Crue9 par 100 pour les convertir en Crue10 !
   */
  PENTE_MAX("pm_PenteMax", EnumInfosEMH.ORD_PRT_GEO_MODELE_BASE),
  /**
   * Valeur de paramètre : pente longitudinale limite de détection des ruptures de pente entre sections lors du prétraitement
   * géométrique. Valeur par défaut : 0,00001 m/m
   */
  PENTE_RUPTURE("pm_PenteRupture", EnumInfosEMH.ORD_PRT_GEO_MODELE_BASE),
  /**
   * Valeur de paramètre : pente tatérale limite de détection des profils plats lors du prétraitement géométrique. Valeur par
   * défaut : 0,0001 m/m. ATTENTION, l'unité dans Crue9 est m/100m : il faut donc diviser les valeurs issues de Crue9 par 100 pour
   * les convertir en Crue10 !
   */
  PROF_PLAT("pm_ProfPlat", EnumInfosEMH.ORD_PRT_GEO_MODELE_BASE),
  /**
   * Reb deb
   */
  REB_DEB("pn_RebDeb", EnumInfosEMH.ORD_PRT_GEO_MODELE_BASE),
  /**
   * Valeur de paramètre : nombre minimal de pas d'espaces admis pour une variation de la distance entre sections de calcul
   * successives. Valeur par défaut : 1
   */
  VAR_PDX_MAX("pm_VarPdxMax", EnumInfosEMH.ORD_PRT_GEO_MODELE_BASE),
  ORD_PRT_RESEAU_COMPATIBILITE_CLIMM("RegleCompatibiliteCLimM", EnumInfosEMH.ORD_PRT_RESEAU),
  ORD_PRT_RESEAU_SIGNALER_OBJETS_INACTIFS("RegleSignalerObjetsInactifs", EnumInfosEMH.ORD_PRT_RESEAU),
  REGLE_QBR_UNIFORME("pm_QbrUniforme", EnumInfosEMH.ORD_PRT_CINI_MODELE_BASE),
  REGLE_QND("pm_Qnd", EnumInfosEMH.ORD_PRT_CINI_MODELE_BASE);
  private final String variableName;
  final EnumInfosEMH infoEMHType;

  /**
   * @param variableName
   */
  EnumRegle(final String variableName, final EnumInfosEMH container) {
    this.variableName = variableName;
    this.infoEMHType = container;
  }

  /**
   * @return the variableName
   */
  public String getVariableName() {
    return variableName;
  }

  @Visibility(ihm = false)
  @UsedByComparison(ignoreInComparison = true)
  public EnumInfosEMH getInfoEMHType() {
    return infoEMHType;
  }

  /**
   * @return the optg
   */
  @Visibility(ihm = false)
  @UsedByComparison(ignoreInComparison = true)
  public boolean isOptg() {
    return EnumInfosEMH.ORD_PRT_GEO_MODELE_BASE.equals(infoEMHType);
  }

  /**
   * @return the optg
   */
  @Visibility(ihm = false)
  @UsedByComparison(ignoreInComparison = true)
  public boolean isOptr() {
    return EnumInfosEMH.ORD_PRT_RESEAU.equals(infoEMHType);
  }

  /**
   * @return the optg
   */
  @Visibility(ihm = false)
  @UsedByComparison(ignoreInComparison = true)
  public boolean isOpti() {
    return EnumInfosEMH.ORD_PRT_CINI_MODELE_BASE.equals(infoEMHType);
  }
}
