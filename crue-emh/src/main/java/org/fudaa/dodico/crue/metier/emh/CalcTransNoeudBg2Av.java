package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.BusinessMessages;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * CL de type 2 en Crue9
 */
public class CalcTransNoeudBg2Av extends DonCLimMCommonItem  {


  @Override
  public String getShortName() {
    return BusinessMessages.getString("bg2av.property");
  }

  CalcTransNoeudBg2Av deepClone() {
    try {
      return (CalcTransNoeudBg2Av) clone();
    } catch (final CloneNotSupportedException ex) {
      Logger.getLogger(CalcTransNoeudBg2Av.class.getName()).log(Level.SEVERE, null, ex);
    }
    throw new IllegalAccessError("Why");
  }
}
