package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;

import java.util.*;

public class ResPrtGeo extends AbstractInfosEMH implements ObjetNomme {
  protected final Map<String, Object> values = new HashMap<>();
  protected final Map<String, Object> valuesExt = Collections.unmodifiableMap(values);
  private String nom;
  private final boolean c9;
  private final boolean stricklerWith1Lit;
  private final int typBra;

  /**
   * @param map
   * @param nom
   * @param isC9
   * @param typBra est uniquement utile pour les sections crue9
   * @param stricklerWith1Lit est uniquement utile pour les sections crue9
   */
  public ResPrtGeo(Map<String, Object> map, String nom, boolean isC9, int typBra, boolean stricklerWith1Lit) {
    values.putAll(map);
    setNom(nom);
    this.c9 = isC9;
    this.stricklerWith1Lit = stricklerWith1Lit;
    this.typBra = typBra;
  }

  public boolean isStricklerWith1Lit() {
    return stricklerWith1Lit;
  }

  @Visibility(ihm = false)
  public int getTypBra() {
    return typBra;
  }

  /**
   * Doit être uniquement utilisé pour les comparaisons.
   *
   * @return true si C9
   */
  @Visibility(ihm = false)
  public boolean isC9() {
    return c9;
  }

  @Override
  @Visibility(ihm = false)
  public final String getNom() {
    return nom;
  }

  private String id;

  @Override
  @Visibility(ihm = false)
  public String getId() {
    return id;
  }

  @Override
  public final void setNom(String newNom) {
    nom = newNom;
    if (nom != null) {
      id = nom.toUpperCase();
    }
  }

  public LoiFF getLoi(String prop) {
    return (LoiFF) values.get(prop);
  }

  public List<LoiFF> getLois() {
    List<LoiFF> res = new ArrayList<>();
    for (Object object : values.values()) {
      if (object instanceof LoiFF) {
        res.add((LoiFF) object);
      }
    }
    return res;
  }

  public double getValue(String prop) {
    Double res = (Double) values.get(prop);
    return res == null ? Double.NaN : res.doubleValue();
  }

  public Map<String, Object> getValues() {
    return valuesExt;
  }

  public void setValue(String prop, double v) {
    values.put(prop, v);
  }

  public void setValue(String prop, LoiFF loi) {
    LoiFF old = (LoiFF) values.remove(prop);
    if (old != null) {
      old.unregister(this);
    }
    if (loi != null) {
      loi.register(this);
      values.put(prop, loi);
    }
  }

  @Override
  public final EnumInfosEMH getCatType() {
    return EnumInfosEMH.RES_PRT_GEO;
  }
}
