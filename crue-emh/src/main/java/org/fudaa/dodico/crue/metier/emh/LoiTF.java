package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Loi String-Flottant
 */
public class LoiTF extends AbstractLoi {

    private EvolutionTF evolutionTF;

    /**
     * ne pas utiliser directement.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public LoiTF clonedWithoutUser() {
        try {
            LoiTF res = (LoiTF) clone();
            res.users = new HashSet<>();
            if (res.evolutionTF != null) {
                res.evolutionTF = evolutionTF.copy();
            }
            return res;
        } catch (CloneNotSupportedException cloneNotSupportedException) {
        }
        return null;
    }

    /**
     * Clone uniquement le commentaire, nom. Le reste est initialisé à vide.
     *
     * @return
     */
    @Override
    public LoiTF cloneWithoutPoints() {
        try {
            LoiTF res = (LoiTF) clone();
            res.users = new HashSet<>();
            res.evolutionTF = new EvolutionTF(new ArrayList<>());
            return res;
        } catch (CloneNotSupportedException cloneNotSupportedException) {
        }
        return null;
    }

    public String getAbscisse(int idx) {
        return getEvolutionTF().getPtEvolutionTF().get(idx).getAbscisse();
    }

    public int getNombrePoint() {
        return getEvolutionTF().getPtEvolutionTF().size();
    }

    public double getOrdonnee(int idx) {
        return getEvolutionTF().getPtEvolutionTF().get(idx).getOrdonnee();
    }

    @UsedByComparison(ignoreInComparison = true)
    @Visibility(ihm = false)
    @Override
    public int getSize() {
        return getEvolutionTF() == null ? 0 : getEvolutionTF().getPtEvolutionTF().size();
    }


    public EvolutionTF getEvolutionTF() {
        return evolutionTF;
    }

    /**
     * @param newEvolutionTF
     */
    public void setEvolutionTF(final EvolutionTF newEvolutionTF) {
        this.evolutionTF = newEvolutionTF;
    }

}
