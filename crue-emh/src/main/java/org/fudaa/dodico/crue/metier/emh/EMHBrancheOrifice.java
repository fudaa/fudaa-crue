package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 * type 5 en Crue9
 * 
 */
public class EMHBrancheOrifice extends CatEMHBranche {

  public EMHBrancheOrifice(final String nom) {
    super(nom);
  }
  
  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public EnumBrancheType getBrancheType(){
    return EnumBrancheType.EMHBrancheOrifice;
  }

  @Override
  @Visibility(ihm = false)
  public EMHBrancheOrifice copyShallowFirstLevel() {
    return  copyPrimitiveIn(new EMHBrancheOrifice(getNom()));
  }


}
