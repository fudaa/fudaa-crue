/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.result;

import gnu.trove.TLongObjectHashMap;
import gnu.trove.TLongObjectIterator;
import gnu.trove.TObjectLongHashMap;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.ResultatPasDeTemps;
import org.fudaa.dodico.crue.metier.emh.ResultatPasDeTempsParCalcul;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;

import java.util.*;

/**
 * Les données temporelles pour un modele.
 *
 * @author Frederic Deniger
 */
public class ModeleResultatTimeKeyContent {
    /**
     * Associe un unique pas de temps pour tous les pas de temps. Les pas de temps permanents ont des temps négatifs.
     */
    TLongObjectHashMap<ResultatTimeKey> keyByTime;
    TObjectLongHashMap<ResultatTimeKey> timeByKey;
    private List<ResultatTimeKey> timesKeys = Collections.emptyList();
    private List<ResultatTimeKey> permanentsTimes = Collections.emptyList();
    private List<ResultatTimeKey> transitoiresTimes = Collections.emptyList();
    private TObjectLongHashMap<String> firstTempsSimuByCalc = new TObjectLongHashMap<>();

    public static List<ResultatPasDeTempsParCalcul> build(Collection<ResultatTimeKey> keys) {
        Map<String, List<ResultatTimeKey>> byNomCalcul = createMapByCalcul(keys);
        List<ResultatPasDeTempsParCalcul> res = new ArrayList<>();
        Collection<List<ResultatTimeKey>> values = byNomCalcul.values();
        for (List<ResultatTimeKey> list : values) {
            res.add(new ResultatPasDeTempsParCalcul(list, list.get(0).getNomCalcul()));
        }
        return res;
    }

    public static Map<String, List<ResultatTimeKey>> createMapByCalcul(Collection<ResultatTimeKey> keys) {
        Map<String, List<ResultatTimeKey>> byNomCalcul = new LinkedHashMap<>();
        for (ResultatTimeKey resultatKey : keys) {
            List<ResultatTimeKey> list = byNomCalcul.get(resultatKey.getNomCalcul());
            if (list == null) {
                list = new ArrayList<>();
                byNomCalcul.put(resultatKey.getNomCalcul(), list);
            }
            list.add(resultatKey);
        }
        return byNomCalcul;
    }

    public static ModeleResultatTimeKeyContent extract(EMHModeleBase modele) {
        ResultatPasDeTemps pasDeTemps = modele.getResultatCalculPasDeTemps();
        if (pasDeTemps == null) {
            return new ModeleResultatTimeKeyContent();
        }
        Map<String, List<ResultatTimeKey>> createMapByCalcul = createMapByCalcul(pasDeTemps.getDelegate().getResultatKeys());
        final ArrayList<ResultatTimeKey> resultats = new ArrayList<>();
        ModeleResultatTimeKeyContent res = new ModeleResultatTimeKeyContent();
        res.firstTempsSimuByCalc = new TObjectLongHashMap<>();
        for (Map.Entry<String, List<ResultatTimeKey>> entry : createMapByCalcul.entrySet()) {
            if (CollectionUtils.isNotEmpty(entry.getValue())) {
                final List<ResultatTimeKey> value = entry.getValue();
                resultats.addAll(value);
                res.firstTempsSimuByCalc.put(entry.getKey(), value.get(0).getDuree());
            }
        }
        res.timesKeys = Collections.unmodifiableList(resultats);
        List<ResultatTimeKey> perms = new ArrayList<>();
        List<ResultatTimeKey> transitoire = new ArrayList<>();
        for (ResultatTimeKey resultatKey : resultats) {
            if (resultatKey.isPermanent()) {
                perms.add(resultatKey);
            } else {
                transitoire.add(resultatKey);
            }
        }
        res.permanentsTimes = Collections.unmodifiableList(perms);
        res.transitoiresTimes = Collections.unmodifiableList(transitoire);
        return res;
    }

    public List<ResultatTimeKey> getTimes() {
        return timesKeys;
    }

    public List<ResultatTimeKey> getTransitoiresTimes() {
        return transitoiresTimes;
    }

    public List<ResultatTimeKey> getPermanentsTimes() {
        return permanentsTimes;
    }

    /**
     * Ajoute en tempsSce du calc la duree de key
     *
     * @param key
     */
    public long getTempSimu(ResultatTimeKey key) {
        return key.getDuree();
    }

    public long getTempCalc(ResultatTimeKey key) {
        if (key == null || key.isPermanent()) {
            return 0;
        }
        long to = firstTempsSimuByCalc.get(key.getNomCalcul());
        return key.getDuree() - to;
    }

    /**
     * Pour les pas de temps transitoires, renove le Temps Sce. pour les permanent un temps négatifs: ce qui permet d'afficher les pas de temps
     * (permanents, transitoires) sur la meme courbe. Les temps permanents sont séparés de 1 sec.
     *
     * @param time
     */
    public ResultatTimeKey getTempsScePermanentTransitoire(long time) {
        if (keyByTime == null) {
            createKeyByTimeMap();
        }
        return keyByTime.get(time);
    }

    /**
     * crée si nécessaire la map Clé Temps ->temps en se basant sur createKeyByTimeMap.<br>
     * Les calculs permanents ont des pas de temps négatifs.
     */
    private void createTimeByKey() {
        if (timeByKey != null) {
            return;
        }
        if (keyByTime == null) {
            createKeyByTimeMap();
        }
        timeByKey = new TObjectLongHashMap<>();
        TLongObjectIterator<ResultatTimeKey> iterator = keyByTime.iterator();
        for (int i = keyByTime.size(); i-- > 0; ) {
            iterator.advance();
            final ResultatTimeKey key = iterator.value();
            final long time = iterator.key();
            timeByKey.put(key, time);
        }
    }

    public long getGlobalTimeForResultatKey(ResultatTimeKey key) {
        if (key == null) {
            return 0;
        }
        if (key.isPermanent()) {
            createTimeByKey();
            return timeByKey.get(key);
        }
        return key.getDuree();
    }

    /**
     * @return une équivalence resultatKey -> long pour tous les pas de temps. Les pas de temps permanent sont négatifs.
     */
    public List<Pair<ResultatTimeKey, Long>> getTimeByKeyList(List<ResultatTimeKey> list) {
        List<Pair<ResultatTimeKey, Long>> res = new ArrayList<>();
        createTimeByKey();
        for (ResultatTimeKey resultatKey : list) {
            if (resultatKey.isPermanent()) {
                res.add(new Pair<>(resultatKey, timeByKey.get(resultatKey)));
            } else {
                res.add(new Pair<>(resultatKey, resultatKey.getDuree()));
            }
        }
        return res;
    }

    public double getTempsCalcInSec(ResultatTimeKey key) {
        if (key == null || key.isPermanent()) {
            return 0;
        }
        long first = firstTempsSimuByCalc.get(key.getNomCalcul());
        return ((double) (key.getDuree() - first)) / 1000d;
    }

    public double getTempSceInSec(ResultatTimeKey key) {
        return ((double) getTempSimu(key)) / 1000d;
    }

    /**
     * Crée la map temp simu -> Clé temps
     */
    private void createKeyByTimeMap() {
        keyByTime = new TLongObjectHashMap<>();
        int delta = -1000;
        int nb = permanentsTimes.size();
        //le premier pas de temps, doit avoir un temps négatif le plus bas.
        for (int i = 0; i < nb; i++) {
            keyByTime.put((long)(nb - i) * delta, permanentsTimes.get(i));
        }
        for (ResultatTimeKey resultatKey : transitoiresTimes) {
            final long tempSimu = getTempSimu(resultatKey);
            if (!keyByTime.contains(tempSimu)) {
                keyByTime.put(tempSimu, resultatKey);
            }
        }
    }
}
