/**
 * *********************************************************************
 * Module: OrdCalcScenario.java Author: deniger Purpose: Defines the Class OrdCalcScenario
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

public class OrdCalcScenario extends AbstractInfosEMH {
  private Sorties sorties;
  public java.util.List<OrdCalc> ordCalc;

  public OrdCalcScenario() {
    sorties = new Sorties();
  }

  @Override
  public final EnumInfosEMH getCatType() {
    return EnumInfosEMH.ORD_CALC_SCENARIO;
  }

  public final boolean isUsed(Calc calc) {
    for (OrdCalc c : getOrdCalc()) {
      if (c.getCalc() == calc) {
        return true;
      }
    }
    return false;
  }

  public java.util.List<OrdCalc> getOrdCalc() {
    if (ordCalc == null) {
      ordCalc = new java.util.ArrayList<>();
    }
    return ordCalc;
  }

  @UsedByComparison
  @Visibility(ihm = false)
  public int getOrdCalcSize() {
    return ordCalc == null ? 0 : ordCalc.size();
  }

  public void setOrdCalc(java.util.List<OrdCalc> newOrdCalc) {
    removeAllOrdCalc();
    for (java.util.Iterator iter = newOrdCalc.iterator(); iter.hasNext(); ) {
      addOrdCalc((OrdCalc) iter.next());
    }
  }

  public void addOrdCalc(OrdCalc newOrdCalc) {
    if (newOrdCalc == null) {
      return;
    }
    if (this.ordCalc == null) {
      this.ordCalc = new java.util.ArrayList<>();
    }
    if (!this.ordCalc.contains(newOrdCalc)) {
      this.ordCalc.add(newOrdCalc);
    }
  }

  public void removeOrdCalc(OrdCalc oldOrdCalc) {
    if (oldOrdCalc == null) {
      return;
    }
    if (this.ordCalc != null) {
      if (this.ordCalc.contains(oldOrdCalc)) {
        this.ordCalc.remove(oldOrdCalc);
      }
    }
  }

  public void removeAllOrdCalc() {
    if (ordCalc != null) {
      ordCalc.clear();
    }
  }

  /**
   * @return the sorties
   */
  @UsedByComparison
  public Sorties getSorties() {
    return sorties;
  }

  /**
   * @param sorties the sorties to set
   */
  public void setSorties(Sorties sorties) {
    this.sorties = sorties;
  }
}
