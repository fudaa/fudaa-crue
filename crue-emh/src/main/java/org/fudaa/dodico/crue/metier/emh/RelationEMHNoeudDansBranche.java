/**
 * *********************************************************************
 * Module: RelationEMHNoeudDansBranche.java Author: deniger Purpose: Defines the Class RelationEMHNoeudDansBranche
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

public class RelationEMHNoeudDansBranche extends RelationEMH<CatEMHNoeud> {

  private boolean aval;

  public final boolean isAval() {
    return aval;
  }

  /**
   * @param newEmh
   */
  @Override
  public void setEmh(final CatEMHNoeud newEmh) {
    super.setEmh(newEmh);
  }

  /**
   * @param newAval
   */
  public final void setAval(final boolean newAval) {
    aval = newAval;
  }

  /**
   * @param newAmont
   */
  public final void setAmont(final boolean newAmont) {
    aval = !newAmont;
  }

  public final boolean isAmont() {
    return !aval;
  }

  @Override
  public RelationEMHNoeudDansBranche copyWithEMH(CatEMHNoeud copiedEMH) {
    RelationEMHNoeudDansBranche res = new RelationEMHNoeudDansBranche();
    res.setEmh(copiedEMH);
    res.setAval(aval);
    return res;
  }
}