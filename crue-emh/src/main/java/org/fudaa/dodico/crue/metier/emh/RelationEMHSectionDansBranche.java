/**
 * ********************************************************************
 * Module: RelationEMHSectionDansBranche.java Author: deniger Purpose: Defines the Class RelationEMHSectionDansBranche
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

import java.util.Collections;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RelationEMHSectionDansBranche extends RelationEMH<CatEMHSection> implements Cloneable, ObjetWithID {
  private double xp;
  private EnumPosSection pos;

  /**
   * @return the pos
   */
  public EnumPosSection getPos() {
    return pos;
  }

  @Override
  public String getNom() {
    return getEmhNom();
  }

  @Override
  public String getId() {
    return getEmhId();
  }

  public RelationEMHSectionDansBranche cloneValuesOnly() {
    RelationEMHSectionDansBranche cloned = null;
    try {
      cloned = clone();
    } catch (CloneNotSupportedException ex) {
      Logger.getLogger(RelationEMHSectionDansBranche.class.getName()).log(Level.SEVERE, null, ex);
    }
    return cloned;
  }

  @Override
  public RelationEMHSectionDansBranche clone() throws CloneNotSupportedException {
    return (RelationEMHSectionDansBranche) super.clone();
  }

  /**
   * @return une map contenant les données spécifiques de la branche. Utile pour les relatiosn de saint-venant
   */
  public Map getAdditionalData() {
    return Collections.emptyMap();
  }

  public void initAdditionnalData(Map in) {
  }

  /**
   * @param pos the pos to set
   */
  public void setPos(final EnumPosSection pos) {
    this.pos = pos;
  }

  public double getXp() {
    return xp;
  }

  public void setXp(final double newXp) {
    xp = newXp;
  }

  @Override
  public RelationEMHSectionDansBranche copyWithEMH(CatEMHSection copiedEMH) {
    RelationEMHSectionDansBranche res = new RelationEMHSectionDansBranche();
    res.setEmh(copiedEMH);
    res.setXp(xp);
    res.setPos(pos);
    return res;
  }
}
