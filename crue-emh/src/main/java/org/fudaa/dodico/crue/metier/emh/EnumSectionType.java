/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

/**
 * Attention: le nom de l'enum est utilise pour retrouve son nom. Si vous changer ce nom, il faudra ajouter la traduction dans
 * businessMessages.properties
 *
 * @author deniger
 */
public enum EnumSectionType implements ToStringInternationalizable {

  //doit reprendre le nom des classes: utilise pour les nommages
  EMHSectionIdem, EMHSectionInterpolee, EMHSectionProfil, EMHSectionSansGeometrie;

  /**
   *
   * @return le nom de l'EMH.
   */
  @Override
  public String geti18n() {
    return BusinessMessages.getString(name() + ".shortName");
  }

  @Override
  public String geti18nLongName() {
    return BusinessMessages.getString(name() + ".longName");
  }

  public static List<EnumSectionType> getAvailablesSectionType() {
    List<EnumSectionType> values = new ArrayList<>(Arrays.asList(EnumSectionType.values()));
    return values;
  }
}
