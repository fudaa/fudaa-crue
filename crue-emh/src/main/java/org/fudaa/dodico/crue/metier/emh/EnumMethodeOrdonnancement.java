/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.contrat.DoubleValuable;

/**
 */
public enum EnumMethodeOrdonnancement implements DoubleValuable {

  ORDRE_DRSO(0);
  private final int id;

  EnumMethodeOrdonnancement(final int id) {
    this.id = id;
  }

  @Override
  public double toDoubleValue() {
    return id;
  }
}
