/***********************************************************************
 * Module:  RelationEMHCasierDansNoeud.java
 * Author:  deniger
 * Purpose: Defines the Class RelationEMHCasierDansNoeud
 ***********************************************************************/

package org.fudaa.dodico.crue.metier.emh;

public class RelationEMHSectionDansSectionIdem extends RelationEMH<CatEMHSection> {

  @Override
  public RelationEMHSectionDansSectionIdem copyWithEMH(CatEMHSection copiedEMH) {
    RelationEMHSectionDansSectionIdem res = new RelationEMHSectionDansSectionIdem();
    res.setEmh(copiedEMH);
    return res;
  }
}
