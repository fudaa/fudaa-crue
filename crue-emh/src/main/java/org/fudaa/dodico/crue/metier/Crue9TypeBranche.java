/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier;

/**
 * Constante pour les types de branches crue9.
 * @author Frederic Deniger
 */
public class Crue9TypeBranche {
  public static final int STRICKLER = 6;
  public static final int BRA_2 = 2;
  public static final int BRA_15 = 15;
  
}
