package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;

import java.util.Collection;

/**
 * type 14 Crue9
 */
public class DonCalcSansPrtBrancheBarrageGenerique extends DonCalcSansPrtBrancheQ {
  private LoiFF regimeNoye;
  private LoiFF regimeDenoye;

  /**
   * @param defaults
   */
  public DonCalcSansPrtBrancheBarrageGenerique(final CrueConfigMetier defaults) {
    super(defaults);
  }

  @Override
  public void fillWithLoi(final Collection<Loi> target) {
    target.add(regimeNoye);
    target.add(regimeDenoye);
  }

  @Override
  public DonCalcSansPrt cloneDCSP() {
    try {
      DonCalcSansPrtBrancheBarrageGenerique res = (DonCalcSansPrtBrancheBarrageGenerique) super.clone();
      if (regimeDenoye != null) {
        res.setRegimeDenoye(regimeDenoye.clonedWithoutUser());
      }
      if (regimeNoye != null) {
        res.setRegimeNoye(regimeNoye.clonedWithoutUser());
      }
      return res;
    } catch (CloneNotSupportedException e) {
    }
    return null;
  }

  public LoiFF getRegimeNoye() {
    return regimeNoye;
  }

  public void setRegimeNoye(final LoiFF newRegimeNoye) {
    if (this.regimeNoye != null) {
      this.regimeNoye.unregister(this);
    }

    regimeNoye = newRegimeNoye;
    this.regimeNoye.register(this);
  }

  public LoiFF getRegimeDenoye() {
    return regimeDenoye;
  }

  public void setRegimeDenoye(final LoiFF newRegimeDenoye) {
    if (this.regimeDenoye != null) {
      this.regimeDenoye.unregister(this);
    }

    regimeDenoye = newRegimeDenoye;
    this.regimeDenoye.register(this);
  }
}
