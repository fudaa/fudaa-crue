/**
 * *********************************************************************
 * Module: AbstractLoi.java Author: deniger Purpose: Defines the Class AbstractLoi
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.ArrayList;
import java.util.HashSet;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 * Loi abstraite pour LoiDF et LoiFF
 */
public abstract class Loi extends AbstractLoi {

    private EvolutionFF evolutionFF;

    /**
     * ne pas utiliser directement.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public Loi clonedWithoutUser() {
        try {
            Loi res = (Loi) clone();
            res.users = new HashSet<>();
            if (res.evolutionFF != null) {
                res.evolutionFF = evolutionFF.copy();
            }
            return res;
        } catch (CloneNotSupportedException cloneNotSupportedException) {
        }
        return null;
    }

    /**
     * Clone uniquement le commentaire, nom. Le reste est initialisé à vide.
     *
     * @return
     */
    public Loi cloneWithoutPoints() {
        try {
            Loi res = (Loi) clone();
            res.users = new HashSet<>();
            res.evolutionFF = new EvolutionFF(new ArrayList<>());
            return res;
        } catch (CloneNotSupportedException cloneNotSupportedException) {
        }
        return null;
    }

    public double getAbscisse(int idx) {
        return getEvolutionFF().getPtEvolutionFF().get(idx).getAbscisse();
    }

    public int getNombrePoint() {
        return getEvolutionFF().getPtEvolutionFF().size();
    }

    public double getOrdonnee(int idx) {
        return getEvolutionFF().getPtEvolutionFF().get(idx).getOrdonnee();
    }

    @UsedByComparison(ignoreInComparison = true)
    @Visibility(ihm = false)
    @Override
    public int getSize() {
        return getEvolutionFF() == null ? 0 : getEvolutionFF().getPtEvolutionFF().size();
    }


    /**
     * @pdGenerated default parent getter
     */
    public EvolutionFF getEvolutionFF() {
        return evolutionFF;
    }

    /**
     * @param newEvolutionFF
     * @pdGenerated default parent setter
     */
    public void setEvolutionFF(final EvolutionFF newEvolutionFF) {
        this.evolutionFF = newEvolutionFF;
    }
}
