package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

/**
 */
public final class DonPrtCIniCasierProfil extends DonPrtCIni<DonPrtCIniCasierProfil> {

  private double qruis;

  /**
   * @param def
   */
  public DonPrtCIniCasierProfil(CrueConfigMetier def) {
    qruis = def.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_QRUIS);
  }

  @Override
  public void initFrom(DonPrtCIniCasierProfil dpti) {
    this.qruis = dpti.qruis;
  }

  @PropertyDesc(i18n = "qruis.property")
  public double getQruis() {
    return qruis;
  }

  /**
   * @param qruis the qruis to set
   */
  public void setQruis(double qruis) {
    this.qruis = qruis;
  }
}
