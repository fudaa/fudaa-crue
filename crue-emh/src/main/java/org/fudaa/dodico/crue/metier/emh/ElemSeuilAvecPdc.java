package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ElemSeuilAvecPdc
 */
public class ElemSeuilAvecPdc implements Cloneable, ToStringTransformable {

  private double coefPdc;
  private double largeur;
  private double zseuil;
  private double coefD;

  public ElemSeuilAvecPdc(CrueConfigMetier defaults) {
    ElemSeuilAvecPdc.this.coefD = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_COEF_D);
    ElemSeuilAvecPdc.this.zseuil = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_ZSEUIL);
    ElemSeuilAvecPdc.this.largeur = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_LARGEUR);
    coefPdc = defaults.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_COEFPDC);
  }

  @PropertyDesc(i18n = "coefPdc.property")
  public final double getCoefPdc() {
    return coefPdc;
  }

  @Override
  public ElemSeuilAvecPdc clone() {
    ElemSeuilAvecPdc result = null;
    boolean finished = false;
    try {
      result = (ElemSeuilAvecPdc) super.clone();
      finished = true;
    } catch (CloneNotSupportedException ex) {
      Logger.getLogger(ElemOrifice.class.getName()).log(Level.SEVERE, null, ex);
    }
    if (!finished) {
      throw new IllegalAccessError("why");
    }
    return (ElemSeuilAvecPdc) result;
  }

  /**
   * @param newCoefPdc
   */
  public final void setCoefPdc(double newCoefPdc) {
    coefPdc = newCoefPdc;
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    if (EnumToString.OVERVIEW.equals(format)) {
      return "ElemSeuilAvecPdc";
    }
    return "ElemSeuilAvecPdc [coefD="
            + TransformerEMHHelper.formatFromPropertyName(CrueConfigMetierConstants.PROP_COEF_D, getCoefD(), props, DecimalFormatEpsilonEnum.COMPARISON) + ", coefPdc="
            + TransformerEMHHelper.formatFromPropertyName("coefPdc", getCoefPdc(), props, DecimalFormatEpsilonEnum.COMPARISON) + ", largeur="
            + TransformerEMHHelper.formatFromPropertyName("largeur", getLargeur(), props, DecimalFormatEpsilonEnum.COMPARISON) + ", zseuil="
            + TransformerEMHHelper.formatFromPropertyName("zseuil", getZseuil(), props, DecimalFormatEpsilonEnum.COMPARISON) + "]";
  }

  public boolean isSameLargeurAndCote(ElemSeuilAvecPdc other, CrueConfigMetier ccm) {
    return
      ccm.getEpsilon(CrueConfigMetierConstants.PROP_LARGEUR).isSame(largeur, other.getLargeur())
        && ccm.getEpsilon(CrueConfigMetierConstants.PROP_ZSEUIL).isSame(zseuil, other.getZseuil());
  }

  @PropertyDesc(i18n = "largeur.property")
  public final double getLargeur() {
    return largeur;
  }

  public final void setLargeur(double newLargeur) {
    largeur = newLargeur;
  }

  @PropertyDesc(i18n = "zseuil.property")
  public final double getZseuil() {
    return zseuil;
  }

  public final void setZseuil(double newZseuil) {
    zseuil = newZseuil;
  }

  @PropertyDesc(i18n = "coefD.property")
  public final double getCoefD() {
    return coefD;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName();
  }

  public final void setCoefD(double newCoefD) {
    coefD = newCoefD;
  }
}