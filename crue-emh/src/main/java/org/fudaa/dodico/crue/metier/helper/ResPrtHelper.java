package org.fudaa.dodico.crue.metier.helper;

import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

/**
 *
 * @author GRESSIER
 */
public class ResPrtHelper {

  public static List<String> getZiniVar() {
    return Collections.singletonList(CrueConfigMetierConstants.PROP_ZINI);
  }

  public static List<String> getQiniVar() {
    return Collections.singletonList(CrueConfigMetierConstants.PROP_QINI);
  }

  public static boolean isRPTIVariable(String varName) {
    return CrueConfigMetierConstants.PROP_ZINI.equals(varName) || CrueConfigMetierConstants.PROP_QINI.equals(varName);
  }

}
