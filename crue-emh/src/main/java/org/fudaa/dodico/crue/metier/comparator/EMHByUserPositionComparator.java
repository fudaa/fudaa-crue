package org.fudaa.dodico.crue.metier.comparator;

import gnu.trove.TObjectIntHashMap;
import java.util.List;
import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;

/**
 * @author deniger
 */
public class EMHByUserPositionComparator extends SafeComparator<EMH> {

  final TObjectIntHashMap<String> emhPosition = new TObjectIntHashMap<>();

  public EMHByUserPositionComparator(EMHScenario scenario) {
    initMap(scenario.getAllSimpleEMHinUserOrder());
  }

  public EMHByUserPositionComparator(List<EMH> emhs) {
    initMap(emhs);
  }

  @Override
  protected int compareSafe(EMH o1, EMH o2) {
    int pos1 = emhPosition.get(o1.getNom());
    int pos2 = emhPosition.get(o2.getNom());
    return pos1 - pos2;
  }

  private void initMap(List<EMH> allSimpleEMHinUserOrder) {
    for (int i = 0; i < allSimpleEMHinUserOrder.size(); i++) {
      EMH emh = allSimpleEMHinUserOrder.get(i);
      emhPosition.put(emh.getNom(), i);
    }
  }
}
