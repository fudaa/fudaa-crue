/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.result;

import gnu.trove.TDoubleArrayList;
import gnu.trove.TLongArrayList;
import gnu.trove.TObjectIntHashMap;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.dodico.crue.metier.emh.ResultatPasDeTempsDelegate;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;

/**
 * indice des positions de chaque pdt de crue, longueur variable
 */
public class Crue9ResultatCalculPasDeTemps implements ResultatPasDeTempsDelegate {

  protected final long[] positions;
  protected final double[] pdts;
  protected final boolean[] isRegu;
  final double[] ruinou;
  /**
   * l'indice dans les tableaux internes
   */
  final TObjectIntHashMap<ResultatTimeKey> indexByResultatKey;
  final Collection<ResultatTimeKey> resultatKeys;

  /**
   * @param idx le pas de temps demande.
   * @return coefficient de ruissellement au pas de temps idx
   */
  public double getRuinou(final int idx) {
    return ruinou[idx];
  }

  @Override
  public boolean containsResultsFor(ResultatTimeKey key) {
    return indexByResultatKey.containsKey(key);
  }

  public Crue9ResultatCalculPasDeTemps() {
    positions = ArrayUtils.EMPTY_LONG_ARRAY;
    pdts = ArrayUtils.EMPTY_DOUBLE_ARRAY;
    isRegu = ArrayUtils.EMPTY_BOOLEAN_ARRAY;
    ruinou = ArrayUtils.EMPTY_DOUBLE_ARRAY;
    indexByResultatKey = new TObjectIntHashMap<>();
    resultatKeys = Collections.emptyList();
  }

  public Crue9ResultatCalculPasDeTemps(final TLongArrayList tableauIndicesPdtCrue, final TDoubleArrayList pdt, final List<Boolean> isRegu, TDoubleArrayList ruinou, TObjectIntHashMap<ResultatTimeKey> indexByResultatKey, List<ResultatTimeKey> orderedResultatKey) {
    positions = tableauIndicesPdtCrue.toNativeArray();
    this.ruinou = ruinou.toNativeArray();
    pdts = pdt.toNativeArray();
    this.isRegu = ArrayUtils.toPrimitive(isRegu.toArray(new Boolean[0]), false);
    this.indexByResultatKey = indexByResultatKey;
    resultatKeys = Collections.unmodifiableCollection(orderedResultatKey);
  }

  @Override
  public Collection<ResultatTimeKey> getResultatKeys() {
    return resultatKeys;
  }

  public int getIndex(ResultatTimeKey key) {
    if (indexByResultatKey.contains(key)) {
      return indexByResultatKey.get(key);
    }
    return -1;
  }

  @Override
  public int getNbResultats() {
    return pdts.length;
  }

  public double getPdt(int idxPdt) {
    return pdts[idxPdt];
  }

  public boolean isRegulation(final int idxPdt) {
    return isRegu[idxPdt];
  }

  public boolean containsRui(final int idxPdt) {
    return !isRegu[idxPdt];
  }

  public long getPosition(final int idxPdt) {
    return positions[idxPdt];
  }
}
