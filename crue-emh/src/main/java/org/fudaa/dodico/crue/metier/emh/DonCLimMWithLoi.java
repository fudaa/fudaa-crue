package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;

public interface DonCLimMWithLoi extends DonCLimM {
  Loi getLoi();

  void setLoi(Loi newLoi);

  EnumTypeLoi getTypeLoi();
}
