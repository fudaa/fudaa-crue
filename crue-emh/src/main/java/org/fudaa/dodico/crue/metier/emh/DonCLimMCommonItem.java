package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.annotation.Visibility;

public abstract class DonCLimMCommonItem extends DonCLimMCommon implements ObjetUserActivable, Cloneable {

  public static final String PROP_USER_ACTIVE = "userActive";
  //attention modifier la constante ci-dessus si modification de nom
  private boolean userActive = true;

  /**
   *
   */
  public DonCLimMCommonItem() {
    userActive = true;
  }


  protected DonCLimMCommonItem(DonCLimMCommonItem from) {
    userActive = from.userActive;
    setCalculParent(from.getCalculParent());
    setEmh(from.getEmh());
  }


  @Visibility(ihm = false)
  public boolean containValue() {
    return true;
  }


  public abstract String getShortName();

  @Override
  public DonCLimMCommonItem clone() throws CloneNotSupportedException {
    return (DonCLimMCommonItem) super.clone();
  }

  @Override
  public boolean getActuallyActive() {
    final boolean active = super.getActuallyActive() && getUserActive();
    return active;
  }

  /**
   * @param active the active to set
   */
  public void setUserActive(boolean active) {
    this.userActive = active;
  }

  @Override
  @PropertyDesc(i18n = "active.property")
  public boolean getUserActive() {
    return this.userActive;
  }

  abstract DonCLimMCommonItem deepClone();
}
