package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;

public class ResPrtCIniSection extends AbstractResPrtCIni implements ToStringTransformable {

  private double zini;
  private double qini;

  public ResPrtCIniSection(boolean c9) {
    super(c9);
  }

  public ResPrtCIniSection(boolean c9, ResPrtCIniSection ini) {
    super(c9);
    this.zini = ini.zini;
    this.qini = ini.qini;
  }
  
  @Override
  public Double getValue(String variableKey) {
    if( variableKey.equals(CrueConfigMetierConstants.PROP_ZINI)){
      return zini;
    } else if( variableKey.equals(CrueConfigMetierConstants.PROP_QINI)) {
      return qini;
    } else {
      return null;
    }
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    return "ResPrtCiniSection Zini= " + (TransformerEMHHelper.formatFromPropertyName(CrueConfigMetierConstants.PROP_ZINI, zini, props, DecimalFormatEpsilonEnum.COMPARISON) + ", Qini=" + TransformerEMHHelper.formatFromPropertyName(CrueConfigMetierConstants.PROP_QINI, qini, props, DecimalFormatEpsilonEnum.COMPARISON));
  }

  /**
   * @return the zIni
   */
  public double getZini() {
    return zini;
  }

  /**
   * @param zIni the zIni to set
   */
  public void setZini(double zIni) {
    this.zini = zIni;
  }

  /**
   * @return the qini
   */
  public double getQini() {
    return qini;
  }

  /**
   * @param qini the qini to set
   */
  public void setQini(double qini) {
    this.qini = qini;
  }


//  @Override
//  public InfosEMH copy() {
//    return new ResPrtCIniSection(this.isC9(),this);
//  }
}
