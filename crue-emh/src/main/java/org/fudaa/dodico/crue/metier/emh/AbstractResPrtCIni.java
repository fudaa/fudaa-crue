package org.fudaa.dodico.crue.metier.emh;

public abstract class AbstractResPrtCIni extends AbstractInfosEMH {
  
  private final boolean c9;
 
  public AbstractResPrtCIni(boolean c9) {
    this.c9 = c9;
  }

  public boolean isC9() {
    return c9;
  }
  
  @Override
  public final EnumInfosEMH getCatType() {
    return EnumInfosEMH.RES_PRT_CINIT;
  }
  
  /**
   * 
   * @param variableKey
   * @return le résultat de la variable variableKey pour l'EMH associé
   */
  public abstract Double getValue(String variableKey);

}