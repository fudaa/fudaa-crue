package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

/**
 *
 */
public class EMHNoeudNiveauContinu extends CatEMHNoeud {

  public EMHNoeudNiveauContinu(final String nom) {
    super(nom);
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public EnumNoeudType getNoeudType() {
    return EnumNoeudType.EMHNoeudNiveauContinu;
  }

  @Override
  @Visibility(ihm = false)
  public EMHNoeudNiveauContinu copyShallowFirstLevel() {
    return copyPrimitiveIn(new EMHNoeudNiveauContinu(getNom()));
  }
}
