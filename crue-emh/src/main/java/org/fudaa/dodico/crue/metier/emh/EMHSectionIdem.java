package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

/**
 * Section de calcul définie par référence é un profil géométrique
 *
 * @author Adrien Hadoux
 */
public class EMHSectionIdem extends CatEMHSection {
  /**
   * @param nom le nom de la section
   */
  public EMHSectionIdem(final String nom) {
    super(nom);
  }

  @UsedByComparison(ignoreInComparison = true)
  @Visibility(ihm = false)
  @Override
  public EnumSectionType getSectionType() {
    return EnumSectionType.EMHSectionIdem;
  }

  /**
   * @return la section de reference
   */
  public CatEMHSection getSectionRef() {
    return EMHHelper.getSectionRef(this);
  }

  /**
   * @param sect la section de reference
   */
  public void setSectionRef(CatEMHSection sect) {
    RelationEMHSectionDansSectionIdem relationSectionRef = EMHHelper.getRelationSectionRef(this);
    if (relationSectionRef != null) {
      super.removeRelationEMH(relationSectionRef);
      CatEMHSection oldRef = relationSectionRef.getEmh();
      RelationEMHSectionIdemContientSection relationToRemove = EMHHelper.getRelationEMHSectionIdemContientSection(oldRef, this);
      if (relationToRemove != null) {
        oldRef.removeRelationEMH(relationToRemove);
      }
    }
    EMHRelationFactory.addSectionRef(this, sect);
  }

  @Visibility(ihm = false)
  @UsedByComparison
  public String getSectionRefId() {
    CatEMHSection sectionRef = getSectionRef();
    return sectionRef == null ? null : sectionRef.getId();
  }

  @Visibility(ihm = false)
  public String getSectionRefNom() {
    CatEMHSection sectionRef = getSectionRef();
    return sectionRef == null ? null : sectionRef.getNom();
  }

  @Override
  @Visibility(ihm = false)
  public EMHSectionIdem copyShallowFirstLevel() {
    return copyPrimitiveIn(new EMHSectionIdem(getNom()));
  }
}
