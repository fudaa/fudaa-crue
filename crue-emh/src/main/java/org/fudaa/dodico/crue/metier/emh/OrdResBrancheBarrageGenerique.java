/**
 * *********************************************************************
 * Module: OrdResScenarioBrancheBarrageGenerique.java Author: BATTISTA Purpose: Defines the Class OrdResScenarioBrancheBarrageGenerique
 **********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.OrdResKey;

public class OrdResBrancheBarrageGenerique extends OrdRes implements Cloneable {

  @Override
  public OrdResBrancheBarrageGenerique clone() {
    try {
      return (OrdResBrancheBarrageGenerique) super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why this ?");
  }
  private final static OrdResKey KEY = new OrdResKey(EnumBrancheType.EMHBrancheBarrageGenerique);

  @Override
  public OrdResKey getKey() {
    return KEY;
  }

  @Override
  public String toString() {
    return "OrdResBrancheBarrageGenerique [ddeRegime=" + getDdeValue("regimeBarrage") + "]";
  }
}
