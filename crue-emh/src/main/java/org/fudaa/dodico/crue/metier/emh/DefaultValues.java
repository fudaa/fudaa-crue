package org.fudaa.dodico.crue.metier.emh;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;

/**
 * Valeures par defaut des EMHs. Ces valeurs sont statiques finales et sont distinguees par fichiers xml.
 *
 * @author Adrien Hadoux, Fred
 */
public final class DefaultValues {

  private DefaultValues() {
  }

  /**
   * @param ordPrtGeoModeleBase
   */
  public static List<Regle> initDefaultValuesOPTG(final CrueConfigMetier defaults) {
    final EnumRegle[] values = EnumRegle.values();
    List<Regle> res = new ArrayList<>();
    for (final EnumRegle enumRegle : values) {
      if (enumRegle.isOptg()) {
        res.add(createRegle(enumRegle, enumRegle.getVariableName(), defaults));
      }
    }
    return res;
  }

  public static List<Regle> initDefaultValuesOPTI(final CrueConfigMetier defaults) {
    final EnumRegle[] values = EnumRegle.values();
    List<Regle> res = new ArrayList<>();
    for (final EnumRegle enumRegle : values) {
      if (enumRegle.isOpti()) {
        res.add(createRegleOpti(enumRegle, enumRegle.getVariableName(), defaults));
      }
    }
    return res;
  }

  public static List<Regle> initDefaultValuesOPTR(final CrueConfigMetier defaults) {
    final EnumRegle[] values = EnumRegle.values();
    List<Regle> res = new ArrayList<>();
    for (final EnumRegle enumRegle : values) {
      if (enumRegle.isOptr()) {
        res.add(createRegleOptr(enumRegle, defaults));
      }
    }
    return res;
  }

  public static Regle createRegleOpti(final EnumRegle type, String variableName, final CrueConfigMetier defaults) {
    final Regle r = new Regle(type);
    r.setActive(false);
    r.setValParamNom(variableName);
    r.setSeuilDetect(defaults.getDefaultDoubleValue(variableName));
    return r;
  }

  public static Regle createRegleOptr(final EnumRegle type, final CrueConfigMetier defaults) {
    final Regle r = new Regle(type);
    r.setActive(false);
    r.setNom(type.getVariableName());
    return r;
  }

  public static Regle createRegle(final EnumRegle type, final String val, final CrueConfigMetier defaults) {

    if (EnumRegle.VAR_PDX_MAX.equals(type)) {
      return createRegleEntier(type, val, defaults);
    }
    final Regle r = new Regle(type);
    if (defaults.isPropertyDefined(val)) {
      r.setSeuilDetect(defaults.getDefaultDoubleValue(val));
    }
    r.setActive(false);
    return r;
  }

  public static Regle createRegleEntier(final EnumRegle type, final String val, final CrueConfigMetier defaults) {
    final Regle r = new Regle(type);
    r.setValParam(new ValParamEntier(r.getValParamNom(), defaults.getDefaultIntValue(val)));
    r.setActive(false);
    return r;
  }
}
