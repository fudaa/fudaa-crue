package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;

public abstract class DonCLimMCommon extends AbstractInfosEMH implements DonCLimM, ToStringTransformable {
  /**
   * Le nom du calcul parent
   */
  private Calc calculParent;

  @Override
  public final Calc getCalculParent() {
    return calculParent;
  }

  @UsedByComparison
  public String getTypeAndEMH() {
    return getType() + " " + getEmh().getId();
  }

  @Override
  public final void setCalculParent(final Calc newCalculParent) {
    calculParent = newCalculParent;
  }

  @Override
  public EnumInfosEMH getCatType() {
    return EnumInfosEMH.DON_CLIM;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + " " + getEmh().getNom();
  }

  @Override
  public final String getNomCalculParent() {
    return calculParent == null ? null : calculParent.getNom();
  }

  @Override
  public String toString(final CrueConfigMetier props, final EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    if (EnumToString.OVERVIEW.equals(format)) {
      return (getEmh() == null ? "?" : getEmh().getNom());
    }
    return getTypei18n() + " " + (getEmh() == null ? "?" : getEmh().getNom());
  }
}
