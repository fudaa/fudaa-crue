package org.fudaa.dodico.crue.metier.emh;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;

/**
 *
 * @author deniger
 */
public class ResultatCalcul implements InfosEMH {

  private final EMH emh;
  private final ResultatCalculDelegate delegate;
  private final boolean c9;

  public ResultatCalcul(EMH emh, ResultatCalculDelegate delegate, boolean isC9) {
    this.emh = emh;
    this.delegate = delegate;
    this.c9 = isC9;
  }
  @Visibility(ihm = false)
  public List<String> getQZRegulVariablesId() {
    return delegate.getQZRegulVariablesId();
  }

  /**
   * @return NomRef des QRegulVariables
   */
  @Visibility(ihm = false)
  public List<String> getQRegulVariablesName() {
    return delegate.getQRegulVariablesName();
  }
  /**
   * @return NomRef des ZRegulVariables
   */
  @Visibility(ihm = false)
  public List<String> getZRegulVariablesName() {
    return delegate.getZRegulVariablesName();
  }

  @Visibility(ihm = false)
  public boolean isC9() {
    return c9;
  }

  @Visibility(ihm = false)
  public ResultatCalculDelegate getDelegate() {
    return delegate;
  }

  public ResultatPasDeTempsDelegate getResultatPasDeTemps() {
    return delegate.getResultatPasDeTemps();
  }

  public String getEMHType() {
    return delegate.getEMHType();
  }

  public Map<String, Object> read(final ResultatTimeKey key) throws IOException {
    return delegate.read(key);
  }

  @Override
  public boolean getActuallyActive() {
    return true;
  }

  @Override
  public String getType() {
    return getClass().getSimpleName();
  }

  @Override
  public String getTypei18n() {
    return ToStringHelper.typeToi18n(getType());
  }

  @Override
  public void setEmh(EMH emh) {
  }

  @Override
  public EnumInfosEMH getCatType() {
    return EnumInfosEMH.RESULTAT;
  }

  @Override
  public void removeEMH(EMH emh) {
  }

  @Override
  public EMH getEmh() {
    return emh;
  }
}
