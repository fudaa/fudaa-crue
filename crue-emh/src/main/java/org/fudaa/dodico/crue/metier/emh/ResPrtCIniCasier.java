package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;

public class ResPrtCIniCasier extends AbstractResPrtCIni implements ToStringTransformable {

  private double zini;

  public ResPrtCIniCasier(boolean c9) {
    super(c9);
  }

  public ResPrtCIniCasier(boolean c9, ResPrtCIniCasier ini) {
    super(c9);
    this.zini = ini.zini;
  }

  @Override
  public Double getValue(String variableKey) {
    return variableKey.equals(CrueConfigMetierConstants.PROP_ZINI) ? zini : null;
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    return "ResPrtCiniCasier Zini= " + (TransformerEMHHelper.formatFromPropertyName(CrueConfigMetierConstants.PROP_ZINI, zini, props, DecimalFormatEpsilonEnum.COMPARISON));
  }

  /**
   * @return the zIni
   */
  public double getZini() {
    return zini;
  }

  /**
   * @param zIni the zIni to set
   */
  public void setZini(double zIni) {
    this.zini = zIni;
  }

//  @Override
//  public InfosEMH copy() {
//    return new ResPrtCIniCasier(this.isC9(), this);
//  }
}
