/**
 * *********************************************************************
 * Module: OrdPrtGeoModeleBase.java Author: deniger Purpose: Defines the Class OrdPrtGeoModeleBase
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByIdComparator;

import java.util.*;

public final class OrdPrtGeoModeleBase extends AbstractInfosEMH implements Sortable {
  private java.util.List<Regle> externRegle;
  private Planimetrage planimetrage;
  private Sorties sorties;
  private java.util.List<Regle> regles;
  private final Map<EnumRegle, Regle> typeRegle = new EnumMap<>(EnumRegle.class);

  public OrdPrtGeoModeleBase(CrueConfigMetier defaults) {
    setRegles(DefaultValues.initDefaultValuesOPTG(defaults));
    planimetrage = new PlanimetrageNbrPdzCst(defaults);
    sorties = new Sorties();
    sort();
  }

  private OrdPrtGeoModeleBase(OrdPrtGeoModeleBase other) {
    List<Regle> cloned = new ArrayList<>();
    for (Regle regle : other.regles) {
      cloned.add(regle.deepClone());
    }
    setRegles(cloned);
    planimetrage = other.planimetrage.deepClone();
    sorties = new Sorties(other.sorties);
    sort();
  }

  /**
   * @return objet clone ( hormis l'EMH contenante).
   */
  public OrdPrtGeoModeleBase deepClone() {
    return new OrdPrtGeoModeleBase(this);
  }

  /**
   * @param newRegle regle a ajouter
   */
  private void addRegle(Regle newRegle) {
    if (newRegle == null) {
      return;
    }
    if (this.regles == null) {
      this.regles = new java.util.ArrayList<>();
      externRegle = Collections.unmodifiableList(regles);
    }

    if (!this.regles.contains(newRegle)) {
      // on enleve la regle de meme type si presente
      Regle sameType = typeRegle.get(newRegle.getType());
      if (sameType != null) {
        regles.remove(sameType);
      }
      this.regles.add(newRegle);
      typeRegle.put(newRegle.getType(), newRegle);
    }
  }

  @Override
  public final EnumInfosEMH getCatType() {
    return EnumInfosEMH.ORD_PRT_GEO_MODELE_BASE;
  }

  public Planimetrage getPlanimetrage() {
    return planimetrage;
  }

  public java.util.List<Regle> getRegles() {
    if (regles == null) {
      regles = new java.util.ArrayList<>();
      externRegle = Collections.unmodifiableList(regles);
    }
    return externRegle;
  }

  public Regle getRegle(EnumRegle regle) {
    return typeRegle.get(regle);
  }

  public void removeAllRegle() {
    if (regles != null) {
      typeRegle.clear();
      regles.clear();
    }
  }

  public void removeRegle(Regle oldRegle) {
    if (oldRegle == null) {
      return;
    }
    if (this.regles != null) {
      if (this.regles.contains(oldRegle)) {
        typeRegle.remove(oldRegle.getType());
        this.regles.remove(oldRegle);
      }
    }
  }

  public void setPlanimetrage(Planimetrage newPlanimetrage) {
    this.planimetrage = newPlanimetrage;
  }

  private void setRegles(java.util.List<Regle> newRegle) {
    removeAllRegle();
    for (java.util.Iterator iter = newRegle.iterator(); iter.hasNext(); ) {
      addRegle((Regle) iter.next());
    }
  }

  @Override
  public boolean sort() {
    if (regles != null) {
      List old = new ArrayList(regles);
      Collections.sort(regles, ObjetNommeByIdComparator.INSTANCE);
      return !old.equals(regles);
    }
    return false;
  }

  /**
   * @return the sorties
   */
  @UsedByComparison
  public Sorties getSorties() {
    return sorties;
  }

  /**
   * @param sorties the sorties to set
   */
  public void setSorties(Sorties sorties) {
    this.sorties = sorties;
  }
}
