/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.helper;

import org.apache.commons.collections4.Predicate;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;

/**
 *
 * @author Frederic Deniger
 */
public class LoiTypePredicate implements Predicate {

  private final EnumTypeLoi typeLoi;

  public LoiTypePredicate(EnumTypeLoi typeLoi) {
    this.typeLoi = typeLoi;
  }

  @Override
  public boolean evaluate(Object object) {
    AbstractLoi loi = (AbstractLoi) object;
    return typeLoi.equals(loi.getType());
  }
}
