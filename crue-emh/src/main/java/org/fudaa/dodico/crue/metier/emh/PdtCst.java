/**
 * *********************************************************************
 * Module: PdtCst.java Author: battista Purpose: Defines the Class PdtCst *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.contrat.DoubleValuable;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.common.transformer.EnumToString;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformable;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;
import org.joda.time.Duration;

public class PdtCst extends Pdt implements ToStringTransformable, DoubleValuable {
  public static Pdt getDefaultPdtValue(CrueConfigMetier ccm, String prop) {
    Duration duration = ccm.getDefaultDurationValue(prop);
    if (duration == null) {
      return null;
    }
    return new PdtCst(duration);
  }

  private Duration pdtCst;

  public PdtCst() {
  }

  @Override
  public String toString() {
    return "Cst: " + (pdtCst == null ? "null" : DateDurationConverter.durationToIso(pdtCst));
  }

  @Override
  public double toDoubleValue() {
    return getPdtCst().getMillis() / 1000d;
  }

  @Override
  public String toString(CrueConfigMetier props, EnumToString format, org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum type) {
    return "Cst: " + (pdtCst == null ? "null" : (TransformerEMHHelper.formatFromPropertyName("pdt", pdtCst, props, DecimalFormatEpsilonEnum.COMPARISON) + " s"));
  }

  /**
   * @param pdtCst le pas de temps
   */
  public PdtCst(Duration pdtCst) {
    super();
    this.pdtCst = pdtCst;
  }

  /**
   * @return the pdtCst
   */
  public Duration getPdtCst() {
    return pdtCst;
  }

  /**
   * @param pdtCst the pdtCst to set
   */
  public void setPdtCst(final Duration pdtCst) {
    this.pdtCst = pdtCst;
  }

  @Override
  public PdtCst deepClone() {
    return new PdtCst(pdtCst);
  }
}
