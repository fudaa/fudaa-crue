package org.fudaa.dodico.crue.metier.aoc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by deniger on 28/06/2017.
 */
public class AocEchellesSections {
  private List<ParametrageProfilSection> echellesSectionList = new ArrayList<>();

  public List<ParametrageProfilSection> getEchellesSectionList() {
    return echellesSectionList;
  }

  public void setEchellesSectionList(final List<ParametrageProfilSection> echellesSectionList) {
    this.echellesSectionList = echellesSectionList;
  }

  public void addSection(final String pk, final String sectionRef) {
    echellesSectionList.add(new ParametrageProfilSection(pk, sectionRef));
  }

  /**
   * @return liste des echelles définies.
   */
  public Set<String> getDefinedEchelles() {
    final Set<String> res = new HashSet<>();
    for (final ParametrageProfilSection aocProfilSection : echellesSectionList) {
      res.add(aocProfilSection.getPk());
    }
    return res;
  }

  /**
   *
   * @param sectionRef la section
   * @return le nom de l'echelle correspondante
   */
  public String getEchelleFromSection(final String sectionRef) {
    for (final ParametrageProfilSection aocProfilSection : echellesSectionList) {
      if (aocProfilSection.getSectionRef().equals(sectionRef)) {
        return aocProfilSection.getPk();
      }
    }
    return null;
  }
}
