package org.fudaa.dodico.crue.metier.emh;

public class ManoeuvreRegulData {
  LoiFF manoeuvreRegul;
  String param;

  public ManoeuvreRegulData(LoiFF manoeuvreRegul, String param) {
    this.manoeuvreRegul = manoeuvreRegul;
    this.param = param;
  }

  public String getParam() {
    return param;
  }

  public LoiFF getManoeuvreRegul() {
    return manoeuvreRegul;
  }

  @Override
  public String toString() {
    if (manoeuvreRegul != null) {
      return manoeuvreRegul.getNom() + " / " + param;
    }
    return param;
  }
}
