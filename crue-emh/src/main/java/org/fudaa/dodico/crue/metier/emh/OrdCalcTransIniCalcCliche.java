/**
 * *********************************************************************
 * Module: OrdCalcTransIniCalcPrecedent.java Author: deniger Purpose: Defines the Class OrdCalcTransIniCalcPrecedent
 * *********************************************************************
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.Arrays;
import java.util.List;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;

public class OrdCalcTransIniCalcCliche extends OrdCalcTrans {

  private IniCalcCliche iniCalcCliche = new IniCalcCliche();

  public OrdCalcTransIniCalcCliche(CrueConfigMetier props) {
    super(props);
  }

  @PropertyDesc(i18n = "iniCalcCliche.property")
  public IniCalcCliche getIniCalcCliche() {
    return iniCalcCliche;
  }

  @Override
  public OrdCalcTransIniCalcCliche deepCloneButNotCalc(Calc newCalc) {
    OrdCalcTransIniCalcCliche res = (OrdCalcTransIniCalcCliche) super.deepCloneButNotCalc(newCalc);
    if (iniCalcCliche != null) {
      res.setIniCalcCliche(iniCalcCliche.clone());
    }
    return res;
  }

  @Override
  public List getCliches() {
    return Arrays.asList(getPrendreClichePonctuel(), getPrendreClichePeriodique(), iniCalcCliche);
  }

  public void setIniCalcCliche(IniCalcCliche iniCalcCliche) {
    this.iniCalcCliche = iniCalcCliche;
  }
}
