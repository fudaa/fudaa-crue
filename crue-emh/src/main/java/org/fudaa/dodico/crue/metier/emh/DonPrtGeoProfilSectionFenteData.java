package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

public class DonPrtGeoProfilSectionFenteData implements Cloneable {
  private double largeurFente;
  private double profondeurFente;

  public DonPrtGeoProfilSectionFenteData(final CrueConfigMetier def) {
    largeurFente = def.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_LARGEUR_FENTE);
    profondeurFente = def.getDefaultDoubleValue(CrueConfigMetierConstants.PROP_PROFONDEUR_FENTE);
  }

  @Override
  public DonPrtGeoProfilSectionFenteData clone() {
    try {
      return (DonPrtGeoProfilSectionFenteData) super.clone();
    } catch (final CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why ?");
  }

  public final double getLargeurFente() {
    return largeurFente;
  }

  @Override
  public String toString() {
    return "largeur " + largeurFente + " profondeur " + profondeurFente;
  }

  public final void setLargeurFente(final double newLargeurFente) {
    largeurFente = newLargeurFente;
  }

  public final double getProfondeurFente() {
    return profondeurFente;
  }

  public final void setProfondeurFente(final double newProfondeurFente) {
    profondeurFente = newProfondeurFente;
  }
}
