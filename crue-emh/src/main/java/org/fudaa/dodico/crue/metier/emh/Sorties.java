package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.common.annotation.Visibility;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.SeveriteManager;
import org.fudaa.dodico.crue.metier.annotation.UsedByComparison;

public class Sorties {

  private Avancement avancement;
  private Trace trace;
  private Resultat resultat;

  public Sorties() {
    avancement = new Avancement();
    trace = new Trace();
    resultat = new Resultat();
  }

  public Sorties(Sorties other) {
    this();
    initWith(other);
  }

  /**
   * Attention, le nom de la méthode est modify... pour éviter que les comparaisons traite cette methode.
   */
  public void modifyVerbositeForAll(String verbosite) {
    trace.setVerbositeEcran(verbosite);
    trace.setVerbositeFichier(verbosite);
  }

  @Visibility(ihm = false)
  public boolean containsVerbositeStrictlyMoreThan(String level, CrueConfigMetier metier) {
    SeveriteManager verbositeManager = metier.getVerbositeManager();
    if (verbositeManager.isManaged(level)) {
      return verbositeManager.isStrictlyMoreVerbose(trace.getVerbositeEcran(), level)
             || verbositeManager.isStrictlyMoreVerbose(trace.getVerbositeFichier(), level);
    }

    return false;

  }

  @Override
  public String toString() {
    return getClass().getSimpleName();
  }

  /**
   * @return the avancement
   */
  @UsedByComparison
  public Avancement getAvancement() {
    return avancement;
  }

  public final void initWith(Sorties sorties) {
    avancement.initWith(sorties.avancement);
    trace.initWith(sorties.trace);
    resultat.initWith(sorties.resultat);
  }

  /**
   * @return the trace
   */
  @UsedByComparison
  public Trace getTrace() {
    return trace;
  }

  /**
   * @return the resultat
   */
  @UsedByComparison
  public Resultat getResultat() {
    return resultat;
  }

  /**
   * @param avancement the avancement to set
   */
  public void setAvancement(Avancement avancement) {
    assert avancement != null;
    this.avancement = avancement;
  }

  /**
   * @param trace the trace to set
   */
  public void setTrace(Trace trace) {
    assert trace != null;
    this.trace = trace;
  }

  /**
   * @param resultat the resultat to set
   */
  public void setResultat(Resultat resultat) {
    assert resultat != null;
    this.resultat = resultat;
  }
}
