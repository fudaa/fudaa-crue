package org.fudaa.dodico.crue.metier.emh;

/**
 * Loi Flottant-Flottant
 */
public class LoiFF extends Loi {

  @Override
  public String toString() {
    return super.toString();
  }

  @Override
  public LoiFF clonedWithoutUser() {
    return (LoiFF) super.clonedWithoutUser();
  }
}
