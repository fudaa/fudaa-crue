/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.crue.metier.helper;

import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.emh.ParamNumCalcPseudoPerm;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransfomerFactory;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * @author deniger
 */
public class ToStringTest {

  @Test
  public void testToSting() {
    CrueConfigMetier def = CrueConfigMetierForTest.DEFAULT;
    ToStringTransformer create = ToStringTransfomerFactory.create(def,DecimalFormatEpsilonEnum.PRESENTATION);
    ParamNumCalcPseudoPerm pseudo = new ParamNumCalcPseudoPerm(def);
    assertEquals("ParamNumCalcPseudoPerm [coefRelaxQ=1, coefRelaxZ=1, crMaxFlu=1.0E30, crMaxTor=1.0E30, nbrPdtDecoup=1, nbrPdtMax=40, pdt=Cst: 3600 s, tolMaxQ=0.01, tolMaxZ=0.001]", create.transform(pseudo));

  }
}
