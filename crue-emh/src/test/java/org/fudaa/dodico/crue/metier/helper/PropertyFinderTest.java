/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.metier.helper;

import java.util.List;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.Avancement;
import org.fudaa.dodico.crue.metier.emh.Trace;
import org.fudaa.dodico.crue.metier.helper.PropertyFinder.Description;
import org.fudaa.dodico.crue.metier.helper.PropertyFinder.DescriptionIndexed;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frédéric Deniger
 */
public class PropertyFinderTest {

  public PropertyFinderTest() {
  }

  /**
   * Test of getAvailablePropertiesNotSorted method, of class PropertyFinder.
   */
  @Test
  public void testGetAvailablePropertiesNotSorted() {
    PropertyFinder instance = new PropertyFinder();
    List<PropertyFinder.DescriptionIndexed> result = instance.getAvailablePropertiesNotSorted(Avancement.class);
    assertEquals(1, result.size());
    DescriptionIndexed desc = result.get(0);
    assertEquals("sortieFichier", desc.description.property);
    assertEquals(Boolean.TYPE, desc.description.propertyClass);
    assertEquals(BusinessMessages.getString("sortieFichier.property"), desc.description.propertyDisplay);
  }

  /**
   * Test of getAvailablePropertiesSorted method, of class PropertyFinder.
   */
  @Test
  public void testGetAvailablePropertiesSorted() {
    PropertyFinder instance = new PropertyFinder();
    List<PropertyFinder.Description> result = instance.getAvailablePropertiesSorted(Trace.class);
    assertEquals(4, result.size());
    Description desc = result.get(0);
    assertEquals("sortieEcran", desc.property);
    assertEquals(Boolean.TYPE, desc.propertyClass);
    assertEquals(BusinessMessages.getString("sortieEcran.property"), desc.propertyDisplay);

    desc = result.get(1);
    assertEquals("verbositeEcran", desc.property);
    assertEquals(String.class, desc.propertyClass);
    assertEquals(BusinessMessages.getString("verbositeEcran.property"), desc.propertyDisplay);

    desc = result.get(2);
    assertEquals("sortieFichier", desc.property);
    assertEquals(Boolean.TYPE, desc.propertyClass);
    assertEquals(BusinessMessages.getString("sortieFichier.property"), desc.propertyDisplay);

    desc = result.get(3);
    assertEquals("verbositeFichier", desc.property);
    assertEquals(String.class, desc.propertyClass);
    assertEquals(BusinessMessages.getString("verbositeFichier.property"), desc.propertyDisplay);
  }
}
