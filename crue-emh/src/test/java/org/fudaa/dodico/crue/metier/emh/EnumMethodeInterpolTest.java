/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.metier.emh;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Test que les traductions sont bien présente.
 * @author Frédéric Deniger
 */
public class EnumMethodeInterpolTest {

  public EnumMethodeInterpolTest() {
  }

  /**
   * Test of geti18n method, of class EnumMethodeInterpol.
   */
  @Test
  public void testGeti18n() {
    EnumMethodeInterpol[] values = EnumMethodeInterpol.values();
    for (EnumMethodeInterpol enumMethodeInterpol : values) {
      try {
        String i18n = enumMethodeInterpol.geti18n();
        assertNotNull(i18n);
      } catch (Exception e) {
        fail("mssing resource dans BusinessMessages: " + e.getMessage());
      }

    }
  }

  /**
   * Test of geti18nLongName method, of class EnumMethodeInterpol.
   */
  @Test
  public void testGeti18nLongName() {
    EnumMethodeInterpol[] values = EnumMethodeInterpol.values();
    for (EnumMethodeInterpol enumMethodeInterpol : values) {
      try {
        String i18n = enumMethodeInterpol.geti18nLongName();
        assertNotNull(i18n);
      } catch (Exception e) {
        fail("mssing resource dans BusinessMessages: " + e.getMessage());
      }

    }
  }
}
