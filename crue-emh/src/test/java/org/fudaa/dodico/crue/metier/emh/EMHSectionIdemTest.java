package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EMHSectionIdemTest {
  @Test
  public void setSectionRef() throws Exception {

    EMHSectionIdem sectionIdem = new EMHSectionIdem("Idem");

    EMHSectionProfil profil1 = new EMHSectionProfil("Profil 1");
    EMHSectionProfil profil2 = new EMHSectionProfil("Profil 2");

    sectionIdem.setSectionRef(profil1);
    testOnlyOneSectionIdem(sectionIdem, profil1);


    sectionIdem.setSectionRef(profil2);
    testOnlyOneSectionIdem(sectionIdem, profil2);

    List<EMHSectionIdem> sectionReferencing = EMHHelper.getSectionReferencing(profil1);
    assertEquals(0,sectionReferencing.size());
    sectionReferencing = EMHHelper.getSectionReferencing(profil2);
    assertEquals(1,sectionReferencing.size());


  }

  public void testOnlyOneSectionIdem(EMHSectionIdem sectionIdem, EMHSectionProfil profil1) {
    assertTrue(sectionIdem.getSectionRef() == profil1);
    final List<EMHSectionIdem> sectionReferencing = EMHHelper.getSectionReferencing(profil1);
    assertEquals(1, sectionReferencing.size());
    assertEquals(profil1, sectionReferencing.get(0).getSectionRef());

    CatEMHSection sectionRef = EMHHelper.getSectionRef(sectionIdem);
    assertTrue(profil1 == sectionRef);
  }
}
