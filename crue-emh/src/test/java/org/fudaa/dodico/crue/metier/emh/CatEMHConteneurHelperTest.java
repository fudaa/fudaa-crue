/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.Arrays;
import java.util.List;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class CatEMHConteneurHelperTest {

  public CatEMHConteneurHelperTest() {
  }

  @Test
  public void testGetSections() {
    EMHSousModele sousModele = new EMHSousModele();
    EMHSectionProfil sectionZ = new EMHSectionProfil("Z");
    EMHSectionProfil sectionY = new EMHSectionProfil("Y");

    EMHSectionProfil sectionA = new EMHSectionProfil("A");
    EMHSectionProfil sectionB = new EMHSectionProfil("B");
    CatEMHBranche br = new EMHBrancheSaintVenant("Br");
    RelationEMHSectionDansBranche relationA = createRelationBranche(0, EnumPosSection.AMONT);
    relationA.setEmh(sectionA);
    RelationEMHSectionDansBranche relationB = createRelationBranche(10, EnumPosSection.AVAL);
    relationB.setEmh(sectionB);
    br.addListeSections(Arrays.asList(relationA, relationB));
    //les relations inverses:
    sectionA.addRelationEMH(new RelationEMHBrancheContientSection(br));
    sectionB.addRelationEMH(new RelationEMHBrancheContientSection(br));

    EMHRelationFactory.addRelationsContientEMH(sousModele, Arrays.asList(sectionZ, sectionY));
    EMHRelationFactory.addRelationsContientEMH(sousModele, Arrays.asList(sectionB, sectionA, br));
    List<CatEMHSection> sections = CatEMHConteneurHelper.getSections(sousModele);
    assertEquals(4, sections.size());
    assertEquals("A", sections.get(0).getId());
    assertEquals("B", sections.get(1).getId());
    assertEquals("Y", sections.get(2).getId());
    assertEquals("Z", sections.get(3).getId());
  }

  private RelationEMHSectionDansBranche createRelationBranche(final double xp, final EnumPosSection position) {
    final RelationEMHSectionDansBranche res = new RelationEMHSectionDansBranche();
    res.setXp(xp);
    res.setPos(position);
    return res;

  }
}
