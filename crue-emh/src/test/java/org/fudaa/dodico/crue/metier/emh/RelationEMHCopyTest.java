package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.junit.Assert;
import org.junit.Test;

public class RelationEMHCopyTest {


  public <T extends EMH> void testSimpleCopy(RelationEMH<T> in, T copyEMH) {
    RelationEMH copied = in.copyWithEMH(copyEMH);
    Assert.assertNotSame(in, copied);
    Assert.assertNotSame(in.getEmh(), copied.getEmh());
    Assert.assertSame(copyEMH, copied.getEmh());
    Assert.assertEquals(in.getClass(), copied.getClass());
    if (in instanceof RelationEMHSectionDansBranche) {
      RelationEMHSectionDansBranche inBranche = (RelationEMHSectionDansBranche) in;
      RelationEMHSectionDansBranche copiedBranche = (RelationEMHSectionDansBranche) copied;
      Assert.assertEquals(inBranche.getAdditionalData(), copiedBranche.getAdditionalData());
      Assert.assertEquals(inBranche.getPos(), copiedBranche.getPos());
      Assert.assertEquals(inBranche.getXp(), copiedBranche.getXp(), 1e-6);
    }
    if (in instanceof RelationEMHNoeudDansBranche) {
      Assert.assertEquals(((RelationEMHNoeudDansBranche) in).isAval(), ((RelationEMHNoeudDansBranche) copied).isAval());
    }
  }

  @Test
  public void testRelationEMHBrancheContientNoeud() {
    RelationEMHBrancheContientNoeud relation = new RelationEMHBrancheContientNoeud(new EMHBrancheOrifice("init"));
    testSimpleCopy(relation, new EMHBrancheOrifice("copied"));
  }

  @Test
  public void testRelationEMHBrancheContientSection() {
    RelationEMHBrancheContientSection relation = new RelationEMHBrancheContientSection(new EMHBrancheOrifice("init"));
    testSimpleCopy(relation, new EMHBrancheOrifice("copied"));
  }

  @Test
  public void testRelationEMHBrancheContientSectionPilote() {
    RelationEMHBrancheContientSectionPilote relation = new RelationEMHBrancheContientSectionPilote(new EMHBrancheOrifice("init"));
    testSimpleCopy(relation, new EMHBrancheOrifice("copied"));
  }

  @Test
  public void testRelationEMHCasierDansNoeud() {
    RelationEMHCasierDansNoeud relation = new RelationEMHCasierDansNoeud();
    relation.setEmh(new EMHCasierProfil("init"));
    testSimpleCopy(relation, new EMHCasierProfil("copied"));
  }

  @Test
  public void testRelationEMHContient() {
    RelationEMHContient relation = new RelationEMHContient();
    relation.setEmh(new EMHCasierProfil("init"));
    testSimpleCopy(relation, new EMHCasierProfil("copied"));
  }

  @Test
  public void testRelationRelationEMHDansSousModele() {
    RelationEMHDansSousModele relation = new RelationEMHDansSousModele(new EMHSousModele());
    testSimpleCopy(relation, new EMHSousModele());
  }

  @Test
  public void testRelationEMHModeleDansScenario() {
    RelationEMHModeleDansScenario relation = new RelationEMHModeleDansScenario(new EMHScenario());
    testSimpleCopy(relation, new EMHScenario());
  }

  @Test
  public void testRelationEMHNoeudContientCasier() {
    RelationEMHNoeudContientCasier relation = new RelationEMHNoeudContientCasier();
    relation.setEmh(new EMHNoeudNiveauContinu("test"));
    testSimpleCopy(relation, new EMHNoeudNiveauContinu("test2"));
  }

  @Test
  public void testRelationEMHNoeudDansBranche() {
    RelationEMHNoeudDansBranche relation = new RelationEMHNoeudDansBranche();
    relation.setEmh(new EMHNoeudNiveauContinu("test"));
    relation.setAval(true);
    testSimpleCopy(relation, new EMHNoeudNiveauContinu("test2"));
  }


  @Test
  public void testRelationEMHSectionDansBranche() {
    RelationEMHSectionDansBranche relation = new RelationEMHSectionDansBranche();
    relation.setPos(EnumPosSection.AMONT);
    relation.setXp(120);
    relation.setEmh(new EMHSectionIdem("init"));
    testSimpleCopy(relation, new EMHSectionIdem("copied"));
  }

  @Test
  public void testRelationEMHSectionDansBrancheSaintVenant() {
    RelationEMHSectionDansBrancheSaintVenant relation = new RelationEMHSectionDansBrancheSaintVenant(CrueConfigMetierForTest.DEFAULT);
    relation.setPos(EnumPosSection.AVAL);
    relation.setXp(121);
    relation.setCoefPond(10);
    relation.setCoefConv(11);
    relation.setCoefDiv(12);
    relation.setEmh(new EMHSectionIdem("init"));
    testSimpleCopy(relation, new EMHSectionIdem("copied"));
  }

  @Test
  public void testRelationEMHSectionDansSectionIdem() {
    RelationEMHSectionDansSectionIdem relation = new RelationEMHSectionDansSectionIdem();
    relation.setEmh(new EMHSectionIdem("init"));
    testSimpleCopy(relation, new EMHSectionIdem("copied"));
  }

  @Test
  public void testRelationEMHSectionIdemContientSection() {
    RelationEMHSectionIdemContientSection relation = new RelationEMHSectionIdemContientSection();
    relation.setEmh(new EMHSectionIdem("init"));
    testSimpleCopy(relation, new EMHSectionIdem("copied"));
  }

  @Test
  public void testRelationEMHSectionPiloteDansBranche() {
    RelationEMHSectionPiloteDansBranche relation = new RelationEMHSectionPiloteDansBranche();
    relation.setEmh(new EMHSectionIdem("section1"));
    testSimpleCopy(relation, new EMHSectionIdem("section2"));
  }

  @Test
  public void testRelationEMHSousModeleDansModele() {
    RelationEMHSousModeleDansModele relation = new RelationEMHSousModeleDansModele(new EMHModeleBase());
    testSimpleCopy(relation, new EMHModeleBase());
  }

}