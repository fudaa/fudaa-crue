package org.fudaa.dodico.crue.metier.emh;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import org.fudaa.dodico.crue.metier.result.OrdResDynamicPropertyFilter;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Pour Crue9, les variables demandées sont retrouvees par introspection. Pour eviter les erreurs à l'excution, tous les cas sont testés ici.
 *
 * @author deniger
 */
public class ResCalTest {

  @Test
  public void testOrdSaintVenant() {
    OrdResScenario ord = new OrdResScenario();
    ord.setOrdResBrancheSaintVenant(new OrdResBrancheSaintVenant());
    ord.getOrdResBrancheSaintVenant().setValues(Arrays.asList(
            new Dde("splanAct", true),
            new Dde("splanSto", true),
            new Dde("splanTot", true)));
    ord.setOrdResBrancheBarrageFilEau(new OrdResBrancheBarrageFilEau());
    ord.getOrdResBrancheBarrageFilEau().setValues(Collections.singletonList(new Dde("regimeBarrage", true)));
    Set<String> retrieveOrdres = OrdResDynamicPropertyFilter.getOrdresProps("BrancheSaintVenant", ord);
    assertNotNull(retrieveOrdres);
    assertEquals(3, retrieveOrdres.size());
    // {vol=false, splanSto=true, splanMaj=true, qlat=false, splanMin=true}
    assertTrue(retrieveOrdres.contains("splanSto"));
    assertTrue(retrieveOrdres.contains("splanTot"));
    assertTrue(retrieveOrdres.contains("splanAct"));
  }

  @Test
  public void testOrdBrancheBarrageFilEau() {
    OrdResScenario ord = new OrdResScenario();
    ord.setOrdResBrancheBarrageFilEau(new OrdResBrancheBarrageFilEau());
    ord.getOrdResBrancheBarrageFilEau().setValues(Collections.singletonList(new Dde("regimeBarrage", true)));

    Set<String> retrieveOrdres = OrdResDynamicPropertyFilter.getOrdresProps("BrancheBarrageFilEau", ord);
    assertEquals(1, retrieveOrdres.size());
    assertTrue(retrieveOrdres.contains("regimeBarrage"));
  }

  @Test
  public void testOrdResAll() {
    OrdResScenario ord = new OrdResScenario();
    ord.setOrdResBrancheBarrageGenerique(new OrdResBrancheBarrageGenerique());
    ord.getOrdResBrancheBarrageGenerique().setValues(Collections.singletonList(new Dde("regimeBarrage", true)));
    Set<String> retrieveOrdres = OrdResDynamicPropertyFilter.getOrdresProps("BrancheBarrageGenerique", ord);
    assertEquals(1, retrieveOrdres.size());
  }
}
