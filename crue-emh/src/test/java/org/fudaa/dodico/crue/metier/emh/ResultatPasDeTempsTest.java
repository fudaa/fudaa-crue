/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class ResultatPasDeTempsTest {

  private static class DefaultDelegate implements ResultatPasDeTempsDelegate {

    final List<ResultatTimeKey> times;

    public DefaultDelegate(List<ResultatTimeKey> times) {
      this.times = times;
    }

    @Override
    public int getNbResultats() {
      return times.size();
    }

    @Override
    public Collection<ResultatTimeKey> getResultatKeys() {
      return times;
    }

    @Override
    public boolean containsResultsFor(ResultatTimeKey key) {
      return times.contains(key);
    }
  }

  public ResultatPasDeTempsTest() {
  }

  @Test
  public void testEquivalence() {
    List<ResultatTimeKey> times = Arrays.asList(new ResultatTimeKey("CC"), new ResultatTimeKey("CC_T1", 0, false), new ResultatTimeKey("CC_T1", 10,
            false), new ResultatTimeKey("CC_T2", 10, false), new ResultatTimeKey("CC_T2", 20, false));
    ResultatPasDeTempsDelegate delegate = new DefaultDelegate(times);
    ResultatPasDeTemps res = new ResultatPasDeTemps(delegate);
    ResultatTimeKey in = new ResultatTimeKey("CQ");
    assertNull(res.getEquivalentTempsSimu(in));
    for (ResultatTimeKey resultatTimeKey : times) {
      assertEquals(resultatTimeKey, res.getEquivalentTempsSimu(resultatTimeKey));
    }
    in = new ResultatTimeKey("CT_T1Bis", 0, false);
    assertEquals(times.get(1), res.getEquivalentTempsSimu(in));
    in = new ResultatTimeKey("CT_T1Bis", 10, false);
    assertEquals(times.get(2), res.getEquivalentTempsSimu(in));
    in = new ResultatTimeKey("CT_T2Bis", 20, false);
    assertEquals(times.get(4), res.getEquivalentTempsSimu(in));

  }
}
