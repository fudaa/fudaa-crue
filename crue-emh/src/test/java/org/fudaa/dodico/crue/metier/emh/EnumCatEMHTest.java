/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.metier.emh;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author GRESSIER
 */
public class EnumCatEMHTest {
  
  public EnumCatEMHTest() {
  }

 

  /**
   * Test of geti18n method, of class EnumCatEMH.
   */
  @Test
  public void testGeti18n() {
    for(EnumCatEMH en : EnumCatEMH.values() ){
      assertNotNull(en.toString(), en.geti18n());
    }
  }

  /**
   * Test of geti18nPlural method, of class EnumCatEMH.
   */
  @Test
  public void testGeti18nPlural() {
    for(EnumCatEMH en : EnumCatEMH.values() ){
      assertNotNull(en.toString(), en.geti18nPlural());
    }
  }

  /**
   * Test of getI18nKey method, of class EnumCatEMH.
   */
  @Test
  public void testGetI18nKey() {
    for(EnumCatEMH en : EnumCatEMH.values() ){
      assertNotNull(en.toString(), en.getI18nKey());
    }
  }

  /**
   * Test of geti18nLongName method, of class EnumCatEMH.
   */
  @Test
  public void testGeti18nLongName() {
    for(EnumCatEMH en : EnumCatEMH.values() ){
      assertNotNull(en.toString(), en.geti18nLongName());
    }
  }

  /**
   * Test of isContainer method, of class EnumCatEMH.
   */
  @Test
  public void testIsContainer() {
    assertFalse(EnumCatEMH.BRANCHE.isContainer());
    assertFalse(EnumCatEMH.CASIER.isContainer());
    assertFalse(EnumCatEMH.NOEUD.isContainer());
    assertFalse(EnumCatEMH.SECTION.isContainer());
    assertTrue(EnumCatEMH.MODELE.isContainer());
    assertTrue(EnumCatEMH.MODELE_ENCHAINEMENT.isContainer());
    assertTrue(EnumCatEMH.SCENARIO.isContainer());
    assertTrue(EnumCatEMH.SOUS_MODELE.isContainer());
    assertTrue(EnumCatEMH.SOUS_MODELE_ENCHAINEMENT.isContainer());
  }
  
}
