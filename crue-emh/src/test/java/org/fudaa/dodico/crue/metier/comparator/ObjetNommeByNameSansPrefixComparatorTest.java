/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.metier.comparator;

import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.DonFrtStrickler;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * Classe de test pour ObjetNommeByNameSansPrefixComparator
 * @author Yoan GRESSIER
 */
public class ObjetNommeByNameSansPrefixComparatorTest {
  
  public ObjetNommeByNameSansPrefixComparatorTest() {
  }
 
  @Test
  public void testCompare() {
    // objet comparateur
    ObjetNommeByNameSansPrefixComparator comparator = ObjetNommeByNameSansPrefixComparator.INSTANCE;
    
    // données d'entrée
    DonFrt frt1 = new DonFrtStrickler();
    frt1.setNom("Fk_aaa");
    DonFrt frt2 = new DonFrtStrickler();
    frt2.setNom("Fk_aAa");
    DonFrt frt3 = new DonFrtStrickler();
    frt3.setNom("FK_AAA");
    DonFrt frt4 = new DonFrtStrickler();
    frt4.setNom("Fk_aba");
    DonFrt frt5 = new DonFrtStrickler();
    frt5.setNom("Fn_aaa");
    DonFrt frt6 = new DonFrtStrickler();
    frt6.setNom("FnSto_aaa");
    DonFrt frt7 = new DonFrtStrickler();
    frt7.setNom("FnSto_aba");
   
    // tests
    assertTrue(comparator.compare(frt1, frt2) == 0);
    assertTrue(comparator.compare(frt1, frt3) == 0);
    assertTrue(comparator.compare(frt2, frt3) == 0);
    assertTrue(comparator.compare(frt1, frt5) == 0);
    assertTrue(comparator.compare(frt5, frt6) == 0);
    
    assertEquals(-1, comparator.compare(frt1, frt4));
    assertEquals(-1, comparator.compare(frt6, frt7));
    assertEquals(1, comparator.compare(frt7, frt1));
  }
  
}
