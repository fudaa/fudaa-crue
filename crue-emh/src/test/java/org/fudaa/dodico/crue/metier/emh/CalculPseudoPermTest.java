/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.Arrays;
import java.util.List;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import static org.junit.Assert.*;

import org.fudaa.dodico.crue.metier.comparator.InfoEMHByUserPositionComparator;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class CalculPseudoPermTest {
  
  public CalculPseudoPermTest() {
  }
  
  @Test
  public void testClone() {
    CalcPseudoPerm calc = new CalcPseudoPerm();
    calc.addDclm(new CalcPseudoPermBrancheOrificeManoeuvre(CrueConfigMetierForTest.DEFAULT));
    calc.addDclm(new CalcPseudoPermBrancheSaintVenantQruis(CrueConfigMetierForTest.DEFAULT));
    calc.addDclm(new CalcPseudoPermCasierProfilQruis(CrueConfigMetierForTest.DEFAULT));
    calc.addDclm(new CalcPseudoPermNoeudNiveauContinuZimp(CrueConfigMetierForTest.DEFAULT));
    calc.addDclm(new CalcPseudoPermNoeudQapp(CrueConfigMetierForTest.DEFAULT));
    CalcPseudoPerm deepClone = calc.deepClone();
    assertEquals(1, calc.getDclm(CalcPseudoPermBrancheOrificeManoeuvre.class).size());
    assertEquals(deepClone.getDclm(CalcPseudoPermBrancheOrificeManoeuvre.class).size(), calc.getDclm(CalcPseudoPermBrancheOrificeManoeuvre.class).size());
    assertFalse(deepClone.getDclm(CalcPseudoPermBrancheOrificeManoeuvre.class).get(0) == calc.getDclm(CalcPseudoPermBrancheOrificeManoeuvre.class).get(0));
    
    final int nb = 5;
    final List<DonCLimM> listInit = calc.getlisteDCLM();
    //l'ensemble
    assertEquals(nb, listInit.size());
    final List<DonCLimM> listCloned = deepClone.getlisteDCLM();
    assertEquals(nb, listCloned.size());
    for (int i = 0; i < nb; i++) {
      assertFalse(listCloned.get(i) == listInit.get(i));
    }
    for (DonCLimM donCLimM : deepClone.getlisteDCLM()) {
      assertTrue(donCLimM.getCalculParent() == deepClone);

    }
  }
  @Test
  public void testgetListDclm() {
    CalcPseudoPerm calc = new CalcPseudoPerm();
    calc.addDclm(new CalcPseudoPermBrancheOrificeManoeuvre(CrueConfigMetierForTest.DEFAULT));
    calc.addDclm(new CalcPseudoPermBrancheSaintVenantQruis(CrueConfigMetierForTest.DEFAULT));
    calc.addDclm(new CalcPseudoPermCasierProfilQruis(CrueConfigMetierForTest.DEFAULT));
    calc.addDclm(new CalcPseudoPermNoeudNiveauContinuZimp(CrueConfigMetierForTest.DEFAULT));
    calc.addDclm(new CalcPseudoPermNoeudQapp(CrueConfigMetierForTest.DEFAULT));
    calc.addDclm(new CalcPseudoPermNoeudQapp(CrueConfigMetierForTest.DEFAULT));

    List<DonCLimM> dclms = calc.getlisteDCLM();

    assertEquals(6,dclms.size());

    assertEquals(CalcPseudoPermNoeudQapp.class, dclms.get(0).getClass());
    assertEquals(CalcPseudoPermNoeudQapp.class, dclms.get(1).getClass());
    assertEquals(CalcPseudoPermNoeudNiveauContinuZimp.class, dclms.get(2).getClass());
    assertEquals(CalcPseudoPermBrancheOrificeManoeuvre.class, dclms.get(3).getClass());
    assertEquals(CalcPseudoPermBrancheSaintVenantQruis.class, dclms.get(4).getClass());
    assertEquals(CalcPseudoPermCasierProfilQruis.class, dclms.get(5).getClass());

    assertEquals(calc.getlisteDCLMPseudoPerm(),dclms);
  }

  @Test
  public void testRemoveDCLM() {
    CalcPseudoPerm calc = new CalcPseudoPerm();
    DonCLimM dclm = new CalcPseudoPermNoeudQapp(CrueConfigMetierForTest.DEFAULT);
    calc.addDclm(dclm);
    calc.removeDclm(dclm);
    List<DonCLimM> donCLimMS = calc.getlisteDCLM();
    assertEquals(0, donCLimMS.size());
  }

  @Test
  public void testSort() {
    CalcPseudoPerm calc = new CalcPseudoPerm();
    EMHNoeudNiveauContinu emh1 = new EMHNoeudNiveauContinu("Nd_1");
    EMHNoeudNiveauContinu emh2 = new EMHNoeudNiveauContinu("Nd_2");
    EMHNoeudNiveauContinu emh3 = new EMHNoeudNiveauContinu("Nd_3");
    EMHNoeudNiveauContinu emh4 = new EMHNoeudNiveauContinu("Nd_4");
    EMHNoeudNiveauContinu emh5 = new EMHNoeudNiveauContinu("Nd_5");


    CalcPseudoPermBrancheOrificeManoeuvre dclmOrificeManOn4 = new CalcPseudoPermBrancheOrificeManoeuvre(CrueConfigMetierForTest.DEFAULT);
    dclmOrificeManOn4.setEmh(emh4);
    calc.addDclm(dclmOrificeManOn4);


    CalcPseudoPermBrancheOrificeManoeuvre dclmOrificeManOn2 = new CalcPseudoPermBrancheOrificeManoeuvre(CrueConfigMetierForTest.DEFAULT);
    dclmOrificeManOn2.setEmh(emh2);
    calc.addDclm(dclmOrificeManOn2);


    CalcPseudoPermNoeudQapp dclmNoeudQapp = new CalcPseudoPermNoeudQapp(CrueConfigMetierForTest.DEFAULT);
    dclmNoeudQapp.setEmh(emh3);
    calc.addDclm(dclmNoeudQapp);


    CalcPseudoPermCasierProfilQruis dclmQruisOn5 = new CalcPseudoPermCasierProfilQruis(CrueConfigMetierForTest.DEFAULT);
    dclmQruisOn5.setEmh(emh5);
    calc.addDclm(dclmQruisOn5);

    CalcPseudoPermNoeudNiveauContinuZimp dclmZimpOn1 = new CalcPseudoPermNoeudNiveauContinuZimp(CrueConfigMetierForTest.DEFAULT);
    dclmZimpOn1.setEmh(emh1);
    calc.addDclm(dclmZimpOn1);

    EMHScenario scenario = new EMHScenario() {
      @Override
      public List<EMH> getAllSimpleEMHinUserOrder() {
        return Arrays.asList(emh1, emh2, emh3, emh4, emh5);
      }
    };
    assertTrue(calc.sort(new InfoEMHByUserPositionComparator(scenario)));
    List<DonCLimM> donCLimMS = calc.getlisteDCLM();
    assertEquals(5, donCLimMS.size());

    assertEquals(CalcPseudoPermNoeudQapp.class, donCLimMS.get(0).getClass());
    assertEquals(emh3.getNom(), donCLimMS.get(0).getEmh().getNom());

    assertEquals(CalcPseudoPermNoeudNiveauContinuZimp.class, donCLimMS.get(1).getClass());
    assertEquals(emh1.getNom(), donCLimMS.get(1).getEmh().getNom());

    assertEquals(CalcPseudoPermBrancheOrificeManoeuvre.class, donCLimMS.get(2).getClass());
    assertEquals(emh2.getNom(), donCLimMS.get(2).getEmh().getNom());

    assertEquals(CalcPseudoPermBrancheOrificeManoeuvre.class, donCLimMS.get(3).getClass());
    assertEquals(emh4.getNom(), donCLimMS.get(3).getEmh().getNom());


    assertEquals(CalcPseudoPermCasierProfilQruis.class, donCLimMS.get(4).getClass());
    assertEquals(emh5.getNom(), donCLimMS.get(4).getEmh().getNom());
  }
}
