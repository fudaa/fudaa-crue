/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.metier.emh;

import java.util.Arrays;
import java.util.List;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;

import static org.junit.Assert.*;

import org.fudaa.dodico.crue.metier.comparator.InfoEMHByUserPositionComparator;
import org.junit.Test;

/**
 * @author deniger
 */
public class CalculTransTest {

  public CalculTransTest() {
  }


  @Test
  public void testAdd() {
    CalcTrans calc = new CalcTrans();
    CalcTransCasierProfilQruis newCalcTransCasierProfilQruis = new CalcTransCasierProfilQruis();
    calc.addDclm(newCalcTransCasierProfilQruis);
    List<DonCLimM> donCLimMS = calc.getlisteDCLM();
    assertEquals(1, donCLimMS.size());
    assertEquals(newCalcTransCasierProfilQruis, donCLimMS.get(0));
  }

  @Test
  public void testGetListDCLM() {
    CalcTrans calc = new CalcTrans();
    calc.addDclm(new CalcTransCasierProfilQruis());
    calc.addDclm(new CalcTransNoeudNiveauContinuTarage());
    calc.addDclm(new CalcTransNoeudQapp());
    calc.addDclm(new CalcTransNoeudNiveauContinuTarage());
    List<DonCLimM> donCLimMS = calc.getlisteDCLM();
    assertEquals(4, donCLimMS.size());
    assertEquals(CalcTransNoeudQapp.class, donCLimMS.get(0).getClass());
    assertEquals(CalcTransNoeudNiveauContinuTarage.class, donCLimMS.get(1).getClass());
    assertEquals(CalcTransNoeudNiveauContinuTarage.class, donCLimMS.get(2).getClass());
    assertEquals(CalcTransCasierProfilQruis.class, donCLimMS.get(3).getClass());
  }

  @Test
  public void testRemoveDCLM() {
    CalcTrans calc = new CalcTrans();
    DonCLimM dclm = new CalcTransCasierProfilQruis();
    calc.addDclm(dclm);
    calc.removeDclm(dclm);
    List<DonCLimM> donCLimMS = calc.getlisteDCLM();
    assertEquals(0, donCLimMS.size());
  }

  @Test
  public void testSort() {
    CalcTrans calc = new CalcTrans();
    EMHNoeudNiveauContinu emh1 = new EMHNoeudNiveauContinu("Nd_1");
    EMHNoeudNiveauContinu emh2 = new EMHNoeudNiveauContinu("Nd_2");
    EMHNoeudNiveauContinu emh3 = new EMHNoeudNiveauContinu("Nd_3");
    EMHNoeudNiveauContinu emh4 = new EMHNoeudNiveauContinu("Nd_4");
    EMHNoeudNiveauContinu emh5 = new EMHNoeudNiveauContinu("Nd_5");


    CalcTransNoeudNiveauContinuTarage dclmTarageOn4 = new CalcTransNoeudNiveauContinuTarage();
    dclmTarageOn4.setEmh(emh4);
    calc.addDclm(dclmTarageOn4);


    CalcTransNoeudNiveauContinuTarage dclmTarageOn1 = new CalcTransNoeudNiveauContinuTarage();
    dclmTarageOn1.setEmh(emh2);
    calc.addDclm(dclmTarageOn1);


    CalcTransNoeudQapp dclmNoeudQapp = new CalcTransNoeudQapp();
    dclmNoeudQapp.setEmh(emh3);
    calc.addDclm(dclmNoeudQapp);


    CalcTransCasierProfilQruis dclmQruisOn5 = new CalcTransCasierProfilQruis();
    dclmQruisOn5.setEmh(emh5);
    calc.addDclm(dclmQruisOn5);
    CalcTransCasierProfilQruis dclmQruisOn1 = new CalcTransCasierProfilQruis();
    dclmQruisOn1.setEmh(emh1);
    calc.addDclm(dclmQruisOn1);

    EMHScenario scenario = new EMHScenario() {
      @Override
      public List<EMH> getAllSimpleEMHinUserOrder() {
        return Arrays.asList(emh1, emh2, emh3, emh4, emh5);
      }
    };
    calc.sort(new InfoEMHByUserPositionComparator(scenario));
    List<DonCLimM> donCLimMS = calc.getlisteDCLM();
    assertEquals(5, donCLimMS.size());
    assertEquals(CalcTransNoeudQapp.class, donCLimMS.get(0).getClass());
    assertEquals(CalcTransNoeudNiveauContinuTarage.class, donCLimMS.get(1).getClass());
    assertEquals(emh2.getNom(), donCLimMS.get(1).getEmh().getNom());
    assertEquals(CalcTransNoeudNiveauContinuTarage.class, donCLimMS.get(2).getClass());
    assertEquals(emh4.getNom(), donCLimMS.get(2).getEmh().getNom());
    assertEquals(CalcTransCasierProfilQruis.class, donCLimMS.get(3).getClass());
    assertEquals(emh1.getNom(), donCLimMS.get(3).getEmh().getNom());
    assertEquals(CalcTransCasierProfilQruis.class, donCLimMS.get(4).getClass());
    assertEquals(emh5.getNom(), donCLimMS.get(4).getEmh().getNom());
  }

  @Test
  public void removeEMH() {
    CalcTrans calc = new CalcTrans();
    EMHNoeudNiveauContinu emh = new EMHNoeudNiveauContinu("Nd_1");
    emh.setUiId(1L);

    CalcTransNoeudNiveauContinuTarage dclmTarageOn = new CalcTransNoeudNiveauContinuTarage();
    dclmTarageOn.setEmh(emh);
    calc.addDclm(dclmTarageOn);

    CalcTransNoeudQapp dclmNoeudQapp = new CalcTransNoeudQapp();
    dclmNoeudQapp.setEmh(emh);
    calc.addDclm(dclmNoeudQapp);
    assertEquals(2, calc.getlisteDCLM().size());

    calc.remove(emh);

    assertEquals(0, calc.getlisteDCLM().size());
  }

  @Test
  public void testClone() {
    CalcTrans calc = new CalcTrans();
    calc.addDclm(new CalcTransBrancheOrificeManoeuvre(CrueConfigMetierForTest.DEFAULT));
    calc.addDclm(new CalcTransBrancheSaintVenantQruis());
    calc.addDclm(new CalcTransCasierProfilQruis());
    calc.addDclm(new CalcTransNoeudNiveauContinuLimnigramme());
    calc.addDclm(new CalcTransNoeudQapp());
    calc.addDclm(new CalcTransNoeudNiveauContinuTarage());
    CalcTrans deepClone = calc.deepClone();
    assertEquals(1, calc.getDclm(CalcTransBrancheOrificeManoeuvre.class).size());
    assertEquals(deepClone.getDclm(CalcTransBrancheOrificeManoeuvre.class).size(), calc.getDclm(CalcTransBrancheOrificeManoeuvre.class).size());
    assertFalse(deepClone.getDclm(CalcTransBrancheOrificeManoeuvre.class).get(0) == calc.getDclm(CalcTransBrancheOrificeManoeuvre.class).get(0));

    final int nb = 6;
    final List<DonCLimM> listInit = calc.getlisteDCLM();
    //l'ensemble
    assertEquals(nb, listInit.size());
    final List<DonCLimM> listCloned = deepClone.getlisteDCLM();
    assertEquals(nb, listCloned.size());
    for (int i = 0; i < nb; i++) {
      assertFalse(listCloned.get(i) == listInit.get(i));
    }
    for (DonCLimM donCLimM : deepClone.getlisteDCLM()) {
      assertTrue(donCLimM.getCalculParent() == deepClone);
    }
  }
}
