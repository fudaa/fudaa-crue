package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.junit.Assert;
import org.junit.Test;

public class DPTGTest {
  @Test
  public void cloneDPTG() {
    Assert.assertNotNull(new DonPrtGeoBatiCasier(TestCoeurConfig.INSTANCE.getCrueConfigMetier()).cloneDPTG());
    Assert.assertNotNull(new DonPrtGeoBrancheSaintVenant(TestCoeurConfig.INSTANCE.getCrueConfigMetier()).cloneDPTG());
    Assert.assertNotNull(new DonPrtGeoProfilCasier(TestCoeurConfig.INSTANCE.getCrueConfigMetier()).cloneDPTG());
    Assert.assertNotNull(new DonPrtGeoProfilSection().cloneDPTG());
    Assert.assertNotNull(new DonPrtGeoSectionIdem(TestCoeurConfig.INSTANCE.getCrueConfigMetier()).cloneDPTG());
  }
}
