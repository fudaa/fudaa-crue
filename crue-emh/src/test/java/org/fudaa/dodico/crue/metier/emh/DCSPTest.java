package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collection;

public class DCSPTest {
  @Test
  public void cloneDPTG() {
    Assert.assertNotNull(new DonCalcSansPrtBrancheBarrageFilEau(TestCoeurConfig.INSTANCE.getCrueConfigMetier()).cloneDCSP());
    Assert.assertNotNull(new DonCalcSansPrtBrancheBarrageGenerique(TestCoeurConfig.INSTANCE.getCrueConfigMetier()).cloneDCSP());
    Assert.assertNotNull(new DonCalcSansPrtBrancheNiveauxAssocies(TestCoeurConfig.INSTANCE.getCrueConfigMetier()).cloneDCSP());
    Assert.assertNotNull(new DonCalcSansPrtBrancheOrifice().cloneDCSP());
    Assert.assertNotNull(new DonCalcSansPrtBranchePdc().cloneDCSP());
    Assert.assertNotNull(new DonCalcSansPrtBrancheSaintVenant(TestCoeurConfig.INSTANCE.getCrueConfigMetier()).cloneDCSP());
    Assert.assertNotNull(new DonCalcSansPrtBrancheSeuilLateral().cloneDCSP());
    Assert.assertNotNull(new DonCalcSansPrtBrancheSeuilTransversal().cloneDCSP());
    Assert.assertNotNull(new DonCalcSansPrtCasierProfil(TestCoeurConfig.INSTANCE.getCrueConfigMetier()).cloneDCSP());
  }
}
