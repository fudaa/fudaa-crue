/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.result;

import java.util.Arrays;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheBarrageFilEau;
import org.fudaa.dodico.crue.metier.emh.EMHNoeudNiveauContinu;
import org.fudaa.dodico.crue.metier.emh.OrdResBrancheBarrageFilEau;
import org.fudaa.dodico.crue.metier.emh.OrdResBrancheSeuilTransversal;
import org.fudaa.dodico.crue.metier.emh.OrdResNoeudNiveauContinu;
import org.fudaa.dodico.crue.metier.helper.EMHCatTypeContent;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class EMHCatTypeContentTest {

  public EMHCatTypeContentTest() {
  }

  @Test
  public void testContain() {
    EMHBrancheBarrageFilEau fileEau = new EMHBrancheBarrageFilEau("test");
    EMHNoeudNiveauContinu nd = new EMHNoeudNiveauContinu("test");
    EMHCatTypeContent res = EMHCatTypeContent.create(Arrays.asList(fileEau, nd));
    assertTrue(res.contain(OrdResBrancheBarrageFilEau.KEY));
    assertTrue(res.contain(OrdResNoeudNiveauContinu.KEY));
    assertFalse(res.contain(OrdResBrancheSeuilTransversal.KEY));
  }
}
