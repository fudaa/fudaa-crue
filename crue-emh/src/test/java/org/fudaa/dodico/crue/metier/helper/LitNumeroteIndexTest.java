/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.metier.helper;

import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author deniger
 */
public class LitNumeroteIndexTest {

  public LitNumeroteIndexTest() {
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() {
  }

  /**
   * Test of isEmpty method, of class LitNumeroteIndex.
   */
  @Test
  public void testIsEmpty() {
    LitNumeroteIndex instance = new LitNumeroteIndex();
    assertTrue(instance.isEmpty());
  }

  /**
   * Test of isValid method, of class LitNumeroteIndex.
   */
  @Test
  public void testIsValid() {
    LitNumeroteIndex instance = new LitNumeroteIndex();
    assertTrue(instance.isValid());
  }

  /**
   * Test of isDisjoint method, of class LitNumeroteIndex.
   */
  @Test
  public void testIsDisjoint() {
    LitNumeroteIndex instance = new LitNumeroteIndex();
    instance.setIdxEnd(5);
    LitNumeroteIndex o = new LitNumeroteIndex();
    o.setIdxStart(6);
    o.setIdxEnd(7);
    assertTrue(instance.isDisjoint(o));
    assertTrue(o.isDisjoint(instance));
    instance.setIdxEnd(6);
    assertFalse(instance.isDisjoint(o));
    assertFalse(o.isDisjoint(instance));
  }

  /**
   * Test of isSuccessif method, of class LitNumeroteIndex.
   */
  @Test
  public void testIsSuccessif() {
    LitNumeroteIndex instance = new LitNumeroteIndex();
    instance.setIdxEnd(5);
    LitNumeroteIndex o = new LitNumeroteIndex();
    o.setIdxStart(5);
    o.setIdxEnd(7);
    assertTrue(instance.isSuccessif(o));
    assertTrue(o.isSuccessif(instance));
    instance.setIdxEnd(6);
    assertFalse(instance.isSuccessif(o));
    assertFalse(o.isSuccessif(instance));
  }

  /**
   * Test of isOverlaping method, of class LitNumeroteIndex.
   */
  @Test
  public void testIsOverlaping() {
    LitNumeroteIndex instance = new LitNumeroteIndex();
    instance.setIdxEnd(5);
    LitNumeroteIndex o = new LitNumeroteIndex();
    o.setIdxStart(4);
    o.setIdxEnd(7);
    assertTrue(instance.isOverlaping(o));
    assertTrue(o.isOverlaping(instance));
    instance.setIdxEnd(4);
    assertFalse(instance.isOverlaping(o));
    assertFalse(o.isOverlaping(instance));
  }

  /**
   * Test of compareTo method, of class LitNumeroteIndex.
   */
  @Test
  public void testCompareTo() {
    LitNumeroteIndex instance = new LitNumeroteIndex();
    instance.setIdxEnd(5);
    LitNumeroteIndex o = new LitNumeroteIndex();
    o.setIdxStart(4);
    o.setIdxEnd(7);
    assertTrue(instance.compareTo(o) < 0);
    assertTrue(o.compareTo(instance) > 0);
    assertEquals(0, o.compareTo(o));


  }
}
