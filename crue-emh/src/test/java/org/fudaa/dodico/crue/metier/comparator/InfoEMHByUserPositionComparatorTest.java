/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.comparator;

import java.util.List;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.factory.ScenarioBuilderForTest;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPermNoeudNiveauContinuZimp;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class InfoEMHByUserPositionComparatorTest {

  public InfoEMHByUserPositionComparatorTest() {
  }

  @Test
  public void testCompare() {
    EMHScenario scenario = ScenarioBuilderForTest.createDefaultScenario(CrueConfigMetierForTest.DEFAULT);
    List<CatEMHNoeud> noeuds = scenario.getNoeuds();
    CalcPseudoPermNoeudNiveauContinuZimp info1 = new CalcPseudoPermNoeudNiveauContinuZimp(CrueConfigMetierForTest.DEFAULT);
    CalcPseudoPermNoeudNiveauContinuZimp info2 = new CalcPseudoPermNoeudNiveauContinuZimp(CrueConfigMetierForTest.DEFAULT);
    noeuds.get(0).addInfosEMH(info1);
    noeuds.get(1).addInfosEMH(info2);
    InfoEMHByUserPositionComparator comparator = new InfoEMHByUserPositionComparator(scenario);
    assertEquals(0, comparator.compare(info1, info1));
    assertEquals(-1, comparator.compare(info1, info2));
    assertEquals(1, comparator.compare(info2, info1));

  }
}
