/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.emh;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.factory.ScenarioBuilderForTest;
import org.junit.Test;

import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

/**
 *
 * @author Frederic Deniger
 */
public class IdRegistryTest {

  public IdRegistryTest() {
  }

  @Test
  public void testGetAllSimpleEMH() {
    EMHScenario scenario = ScenarioBuilderForTest.createDefaultScenario(CrueConfigMetierForTest.DEFAULT);
    assertTrue(scenario.getIdRegistry() != null);
    List<EMH> fromId = scenario.getAllSimpleEMH();
    List<EMH> fromHelper = CatEMHConteneurHelper.getAllSimpleEMH(scenario.getSousModeles());
    assertEquals(fromHelper.size(), fromId.size());
    Collection disjunction = CollectionUtils.disjunction(fromId, fromHelper);
    assertEquals(0, disjunction.size());


  }
}
