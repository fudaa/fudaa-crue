/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.metier.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.metier.comparator.ComparatorRelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.EnumPosSection;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * @author deniger
 */
public class EMHHelperTest {
  @Test
  public void testselectInstanceOf() {
    final List l = new ArrayList();
    l.add(Float.valueOf(2));
    l.add("Test");
    l.add(Boolean.TRUE);
    l.add(Double.valueOf(1));
    l.add(Double.valueOf(2));
    l.add(Integer.valueOf(2));
    final List<Number> filter = EMHHelper.selectInstanceOf(l, Number.class);
    assertEquals(4, filter.size());
  }

  /**
   * le calcul de nombre de profil a ajouter
   */
  @Test
  public void testDistmax() {
    assertEquals(2, EMHHelper.getDistDiviseurForDistMax(50f, 70f));
  }

  /**
   * Test le comparateur {@link ComparatorRelationEMHSectionDansBranche}.
   */
  @Test
  public void testSectionComparator() {
    final List<RelationEMHSectionDansBranche> relationSectionExpected = new ArrayList<>();
    // l'ordre attendu
    relationSectionExpected.add(createRelationBranche(0, EnumPosSection.AMONT));
    relationSectionExpected.add(createRelationBranche(0, EnumPosSection.INTERNE));
    relationSectionExpected.add(createRelationBranche(0, EnumPosSection.AVAL));
    relationSectionExpected.add(createRelationBranche(0.1, EnumPosSection.INTERNE));
    relationSectionExpected.add(createRelationBranche(0.2, EnumPosSection.INTERNE));
    relationSectionExpected.add(createRelationBranche(0.3, EnumPosSection.AVAL));
    // la liste qui sera retrié avec le comparateur:
    final List<RelationEMHSectionDansBranche> relationSectionRetrie = new ArrayList<>(
            relationSectionExpected);
    Collections.shuffle(relationSectionRetrie);
    // cas tordu ou le shuffle ne fonctionne pas correctement et renvoie la meme liste.
    if (relationSectionExpected.equals(relationSectionRetrie)) {
      final RelationEMHSectionDansBranche zero = relationSectionRetrie.get(0);
      relationSectionRetrie.set(0, relationSectionRetrie.get(1));
      relationSectionRetrie.set(1, zero);
    }
    // avant le tri, les 2 collections ne sont pas égales
    assertFalse(relationSectionRetrie.equals(relationSectionExpected));
    Collections.sort(relationSectionRetrie, new ComparatorRelationEMHSectionDansBranche(new PropertyEpsilon(.001, .001)));
    // le tri remet le tout dans l'ordre attendu:
    assertTrue(relationSectionRetrie.equals(relationSectionExpected));

  }

  private RelationEMHSectionDansBranche createRelationBranche(final double xp, final EnumPosSection position) {
    final RelationEMHSectionDansBranche res = new RelationEMHSectionDansBranche();
    res.setXp(xp);
    res.setPos(position);
    return res;

  }
}
