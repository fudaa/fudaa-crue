package org.fudaa.dodico.crue.metier.etude;

import junit.framework.TestCase;

public class EMHRunTest extends TestCase {

    public void testChangeNom() {
        EMHRun run=new EMHRun("init");
        assertEquals("init",run.getNom());
        assertEquals("INIT",run.getId());
        run.changeNom("newName");
        assertEquals("newName",run.getNom());
        assertEquals("NEWNAME",run.getId());

    }
}