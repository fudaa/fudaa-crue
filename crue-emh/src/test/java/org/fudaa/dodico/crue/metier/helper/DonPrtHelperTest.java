/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.helper;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.EMHSectionProfil;
import org.fudaa.dodico.crue.metier.emh.PtProfil;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class DonPrtHelperTest {

  public DonPrtHelperTest() {
  }

  @Test
  public void testGetZf() {
    EMHSectionProfil section = new EMHSectionProfil("test");
    DonPrtGeoProfilSection profil = new DonPrtGeoProfilSection();
    List<PtProfil> pts = new ArrayList<>();
    pts.add(new PtProfil(4, -100));
    pts.add(new PtProfil(5, 1));
    profil.setPtProfil(pts);
    section.addInfosEMH(profil);
    double zf = DonPrtHelper.getZf(section);
    assertEquals(-100, (int) zf);
  }
}
