package org.fudaa.dodico.crue.metier.emh;

import org.junit.Assert;
import org.junit.Test;

public class EMHSimpleCopyShallowTest {


  public void testSimpleCopy(EMH in) {
    in.setNom("Test aVb");
    in.setUiId(345L);
    if (in instanceof CatEMHActivable) {
      ((CatEMHActivable) in).setUserActive(true);
    }
    in.setCommentaire("commentaire");
    EMH copied = in.copyShallowFirstLevel();
    Assert.assertNotSame(copied,in);
    Assert.assertEquals(in.getClass(), copied.getClass());
    Assert.assertEquals(in.getId(), copied.getId());
    Assert.assertEquals(in.getNom(), copied.getNom());
    Assert.assertEquals(in.getCommentaire(), copied.getCommentaire());
    Assert.assertEquals(in.getUiId(), copied.getUiId());
    if (in instanceof CatEMHActivable) {
      Assert.assertTrue(copied.getUserActive());
//      we test for not activated too:
      ((CatEMHActivable) in).setUserActive(false);
      copied = in.copyShallowFirstLevel();
      Assert.assertFalse(copied.getUserActive());
    }
  }

  @Test
  public void testCopyShallowAllEMH() {
    testSimpleCopy(new EMHScenario());
    testSimpleCopy(new EMHModeleBase());
    testSimpleCopy(new EMHModeleEnchainement());
    testSimpleCopy(new EMHSousModele());
    testSimpleCopy(new EMHBrancheBarrageFilEau("test1"));
    testSimpleCopy(new EMHBrancheBarrageGenerique("test1"));
    testSimpleCopy(new EMHBrancheEnchainement("test1"));
    testSimpleCopy(new EMHBrancheNiveauxAssocies("test1"));
    testSimpleCopy(new EMHBrancheOrifice("test1"));
    testSimpleCopy(new EMHBranchePdc("test1"));
    testSimpleCopy(new EMHBrancheSaintVenant("test1"));
    testSimpleCopy(new EMHBrancheSeuilLateral("test1"));
    testSimpleCopy(new EMHBrancheSeuilTransversal("test1"));
    testSimpleCopy(new EMHBrancheStrickler("test1"));
    testSimpleCopy(new EMHCasierMNT("test1"));
    testSimpleCopy(new EMHCasierProfil("test1"));
    testSimpleCopy(new EMHNoeudNiveauContinu("test1"));

  }
}