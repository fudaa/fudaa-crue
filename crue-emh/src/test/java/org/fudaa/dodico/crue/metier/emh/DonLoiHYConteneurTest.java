/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.emh;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class DonLoiHYConteneurTest {
  
  public DonLoiHYConteneurTest() {
  }
  
  @Test
  public void testSort() {
    DonLoiHYConteneur conteneur = new DonLoiHYConteneur();
    Loi loiB = new LoiFF();
    loiB.setNom("B");
    Loi loiC = new LoiFF();
    loiC.setNom("C");
    conteneur.addLois(loiB);
    conteneur.addLois(loiC);
    assertFalse(conteneur.sort());//pas de tri car dans l'ordre
    Loi loiA = new LoiFF();
    loiA.setNom("A");
    conteneur.addLois(loiA);
    assertTrue(conteneur.sort());//pas de tri car dans l'ordre
    assertTrue(loiA == conteneur.getLois().get(0));
  }
}
