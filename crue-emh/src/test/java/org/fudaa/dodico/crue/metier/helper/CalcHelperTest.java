/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.helper;

import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPerm;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPermBrancheOrificeManoeuvre;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPermBrancheSaintVenantQruis;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheBarrageFilEau;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class CalcHelperTest {

  public CalcHelperTest() {
  }

  @Test
  public void testGetDCLMActiveTypeByEMHName() {
    CalcPseudoPerm calc = new CalcPseudoPerm();
    String name1 = "Br_1";
    String name2 = "Br_2";
    calc.addDclm(createBrancheOrifice(name1));
    calc.addDclm(createBrancheOrifice(name2));
    calc.addDclm(createBrancheSaintVenantQruis(name1));
    Map<String, List<Class>> dclmActiveTypeByEMHName = CalcHelper.getDCLMActiveTypeByEMHName(calc);
    assertEquals(2, dclmActiveTypeByEMHName.size());
    List<Class> listOfClass = dclmActiveTypeByEMHName.get(name1);
    assertEquals(2, listOfClass.size());
    assertTrue(listOfClass.contains(CalcPseudoPermBrancheOrificeManoeuvre.class));
    assertTrue(listOfClass.contains(CalcPseudoPermBrancheSaintVenantQruis.class));
    listOfClass = dclmActiveTypeByEMHName.get(name2);
    assertEquals(1, listOfClass.size());
    assertTrue(listOfClass.contains(CalcPseudoPermBrancheOrificeManoeuvre.class));

    //test doublon
    calc.addDclm(createBrancheOrifice(name2));
    dclmActiveTypeByEMHName = CalcHelper.getDCLMActiveTypeByEMHName(calc);
    listOfClass = dclmActiveTypeByEMHName.get(name2);
    assertEquals(2, listOfClass.size());
    
  }

  @Test
  public void testGetDCLMActiveWithDoublons() {
     CalcPseudoPerm calc = new CalcPseudoPerm();
    String name1 = "Br_1";
    String name2 = "Br_2";
    calc.addDclm(createBrancheOrifice(name1));
    //doublon
    calc.addDclm(createBrancheOrifice(name1));
    calc.addDclm(createBrancheOrifice(name2));
    calc.addDclm(createBrancheSaintVenantQruis(name1));
    calc.addDclm(createBrancheSaintVenantQruis(name2));
    List<Pair<String, Class>> dclmActiveWithDoublons = CalcHelper.getDCLMActiveWithDoublons(calc);
    assertEquals(1, dclmActiveWithDoublons.size());
    assertEquals(name1, dclmActiveWithDoublons.get(0).first);
    assertEquals(CalcPseudoPermBrancheOrificeManoeuvre.class, dclmActiveWithDoublons.get(0).second);
    
  }

  protected CalcPseudoPermBrancheOrificeManoeuvre createBrancheOrifice(String name1) {
    final CalcPseudoPermBrancheOrificeManoeuvre res = new CalcPseudoPermBrancheOrificeManoeuvre(CrueConfigMetierForTest.DEFAULT);
    res.setEmh(new EMHBrancheBarrageFilEau(name1));
    return res;
  }

  protected CalcPseudoPermBrancheSaintVenantQruis createBrancheSaintVenantQruis(String name1) {
    final CalcPseudoPermBrancheSaintVenantQruis res = new CalcPseudoPermBrancheSaintVenantQruis(CrueConfigMetierForTest.DEFAULT);
    res.setEmh(new EMHBrancheBarrageFilEau(name1));
    return res;
  }
}
