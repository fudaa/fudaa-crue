/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.factory.LoiFactory;
import org.joda.time.LocalDateTime;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class CloneLoiTest {

  public CloneLoiTest() {
  }

  @Test
  public void testCloneLoiDF() {
    LoiDF df = new LoiDF();
    df.setDateZeroLoiDF(new LocalDateTime(2));
    final String commentaire = "commentaire";
    df.setCommentaire(commentaire);
    final String nom = "nom";
    df.setNom(nom);
    LoiFactory.alimenteEvolutionFF(df, new double[]{0, 1}, new double[]{10, 11});
    df.register(this);
    assertTrue(df.getUsed());
    LoiDF clonedWithoutUser = df.clonedWithoutUser();
    assertNotNull(clonedWithoutUser);
    assertEquals(df.getDateZeroLoiDF(), clonedWithoutUser.getDateZeroLoiDF());
    assertFalse(clonedWithoutUser.getUsed());
    testIsCloned(df, clonedWithoutUser);
  }

  @Test
  public void testCloneDonFrt() {
    DonFrtStrickler strickler = new DonFrtStrickler();
    strickler.setNom("stricker");
    LoiFF loiFF = new LoiFF();
    LoiFactory.alimenteDebutLoi(loiFF, "test", EnumTypeLoi.LoiZFK);
    LoiFactory.alimenteEvolutionFF(loiFF, new double[]{0, 1}, new double[]{10, 11});
    strickler.setStrickler(loiFF);
    assertTrue(loiFF.getUsedBy(strickler));
    DonFrtStrickler cloned = strickler.deepClone();
    assertNotNull(cloned);
    assertEquals(strickler.getNom(), cloned.getNom());
    testIsCloned(strickler.getLoi(), cloned.getLoi());
    //on tests si les utilisations sont à jours:
    assertFalse(strickler.getLoi().getUsedBy(cloned));
    assertFalse(cloned.getLoi().getUsedBy(strickler));
    assertTrue(cloned.getLoi().getUsedBy(cloned));
    assertTrue(strickler.getLoi().getUsedBy(strickler));


  }

  protected void testIsCloned(Loi df, Loi clonedWithoutUser) {
    assertEquals(df.getCommentaire(), clonedWithoutUser.getCommentaire());
    assertEquals(df.getNom(), clonedWithoutUser.getNom());
    assertFalse("cloned evolutionFF", clonedWithoutUser.getEvolutionFF() == df.getEvolutionFF());
    assertEquals(2, clonedWithoutUser.getEvolutionFF().getPtEvolutionFF().size());
    final PtEvolutionFF clonedPt0 = clonedWithoutUser.getEvolutionFF().getPtEvolutionFF().get(0);
    final PtEvolutionFF pt0 = df.getEvolutionFF().getPtEvolutionFF().get(0);
    assertFalse("cloned PtEvolutionFF", clonedPt0 == pt0);
    assertEquals(pt0.getAbscisse(), clonedPt0.getAbscisse(), 1e-15);
    final PtEvolutionFF clonedPt1 = clonedWithoutUser.getEvolutionFF().getPtEvolutionFF().get(1);
    final PtEvolutionFF pt1 = df.getEvolutionFF().getPtEvolutionFF().get(1);
    assertFalse("cloned PtEvolutionFF", clonedPt1 == pt1);
    assertEquals(pt1.getAbscisse(), clonedPt1.getAbscisse(), 1e-15);
  }
}
