/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.result;

import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.ParamCalcScenario;
import org.joda.time.LocalDateTime;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class TempSimuConverterTest {

  public TempSimuConverterTest() {
  }

  @Test
  public void testGetDatDebInMillis() {
    EMHScenario scenario = new EMHScenario();
    ParamCalcScenario pcal = new ParamCalcScenario(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    long to = System.currentTimeMillis();
    pcal.setDateDebSce(new LocalDateTime(to));
    scenario.addInfosEMH(pcal);
    long tRead = TimeSimuConverter.getDatDebInMillis(scenario);
    assertEquals(to, tRead);
  }

  @Test
  public void testCreateEmptyConverter() {
    EMHScenario scenarioCurrent = new EMHScenario();
    ParamCalcScenario pcalCurrent = new ParamCalcScenario(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    scenarioCurrent.addInfosEMH(pcalCurrent);
    EMHScenario scenarioAlternatif = new EMHScenario();
    ParamCalcScenario pcalAlternatif = new ParamCalcScenario(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    scenarioAlternatif.addInfosEMH(pcalAlternatif);
    TimeSimuConverter converter = TimeSimuConverter.create(scenarioCurrent, scenarioAlternatif);
    assertEquals(0, converter.getDateDebRunCourant());
    assertEquals(0, converter.getDateDebRunAlternatif());

    long to = System.currentTimeMillis();
    pcalCurrent.setDateDebSce(new LocalDateTime(to));
    converter = TimeSimuConverter.create(scenarioCurrent, scenarioAlternatif);
    assertEquals(0, converter.getDateDebRunCourant());
    assertEquals(0, converter.getDateDebRunAlternatif());

    pcalCurrent.setDateDebSce(null);
    pcalAlternatif.setDateDebSce(new LocalDateTime(to));
    converter = TimeSimuConverter.create(scenarioCurrent, scenarioAlternatif);
    assertEquals(0, converter.getDateDebRunCourant());
    assertEquals(0, converter.getDateDebRunAlternatif());

    assertEquals(to, converter.getTempsSimuRunAlternatif(to));

    pcalCurrent.setDateDebSce(new LocalDateTime(to));
    converter = TimeSimuConverter.create(scenarioCurrent, scenarioAlternatif);
    assertEquals(0, converter.getDateDebRunCourant());
    assertEquals(0, converter.getDateDebRunAlternatif());
    assertEquals(to, converter.getTempsSimuRunAlternatif(to));


  }

  @Test
  public void testCreateNonEmptyConverter() {
    EMHScenario scenarioCurrent = new EMHScenario();
    ParamCalcScenario pcalCurrent = new ParamCalcScenario(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    scenarioCurrent.addInfosEMH(pcalCurrent);
    EMHScenario scenarioAlternatif = new EMHScenario();
    ParamCalcScenario pcalAlternatif = new ParamCalcScenario(TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    scenarioAlternatif.addInfosEMH(pcalAlternatif);

    long tInitRunCourant = System.currentTimeMillis();
    pcalCurrent.setDateDebSce(new LocalDateTime(tInitRunCourant));
    final long tInitRunAlternatif = tInitRunCourant + 10;
    pcalAlternatif.setDateDebSce(new LocalDateTime(tInitRunAlternatif));
    TimeSimuConverter converter = TimeSimuConverter.create(scenarioCurrent, scenarioAlternatif);
    assertEquals(tInitRunCourant, converter.getDateDebRunCourant());
    assertEquals(tInitRunAlternatif, converter.getDateDebRunAlternatif());

    assertEquals(tInitRunCourant - tInitRunAlternatif, converter.getTempsSimuRunAlternatif(0));
    assertEquals(0, converter.getTempsSimuRunAlternatif(tInitRunAlternatif - tInitRunCourant));


  }
}
