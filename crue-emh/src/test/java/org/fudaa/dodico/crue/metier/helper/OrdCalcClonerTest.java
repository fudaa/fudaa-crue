/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPerm;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPermBrancheOrificeManoeuvre;
import org.fudaa.dodico.crue.metier.emh.DonCLimMScenario;
import org.fudaa.dodico.crue.metier.emh.OrdCalcPseudoPermIniCalcCI;
import org.fudaa.dodico.crue.metier.emh.OrdCalcPseudoPermIniCalcPrecedent;
import org.fudaa.dodico.crue.metier.emh.OrdCalcScenario;
import org.fudaa.dodico.crue.metier.emh.ui.OrdCalcScenarioUiState;
import org.fudaa.dodico.crue.metier.helper.OrdCalcCloner.Result;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class OrdCalcClonerTest {

  public OrdCalcClonerTest() {
  }

  @Test
  public void testCloneOrdCalcAndCalc() {
    DonCLimMScenario dclm = new DonCLimMScenario();
    OrdCalcScenario ocal = new OrdCalcScenario();
    CalcPseudoPerm pseudo = new CalcPseudoPerm();
    final CalcPseudoPermBrancheOrificeManoeuvre calcPseudoPermBrancheOrificeManoeuvre = new CalcPseudoPermBrancheOrificeManoeuvre(
            CrueConfigMetierForTest.DEFAULT);
    pseudo.addDclm(calcPseudoPermBrancheOrificeManoeuvre);
    OrdCalcPseudoPermIniCalcCI ci = new OrdCalcPseudoPermIniCalcCI();
    ci.setCalcPseudoPerm(pseudo);
    dclm.addCalcPseudoPerm(pseudo);
    ocal.addOrdCalc(ci);
    OrdCalcCloner cloner = new OrdCalcCloner();
    Result cloneOrdCalcAndCalc = cloner.cloneOrdCalcAndCalc(dclm, ocal, null);
    assertEquals(1, cloneOrdCalcAndCalc.calcs.size());
    assertEquals(1, cloneOrdCalcAndCalc.ordCalcs.size());
    assertTrue(cloneOrdCalcAndCalc.calcs.get(0) == cloneOrdCalcAndCalc.ordCalcs.get(0).getCalc());
    assertFalse(cloneOrdCalcAndCalc.calcs.get(0) == pseudo);
    assertFalse(cloneOrdCalcAndCalc.ordCalcs.get(0) == ci);
  }

  @Test
  public void testCloneOrdCalcAndCalcUI() {
    DonCLimMScenario dclm = new DonCLimMScenario();
    OrdCalcScenario ocal = new OrdCalcScenario();
    CalcPseudoPerm pseudo = new CalcPseudoPerm();
    pseudo.setNom("CC_P1");
    final CalcPseudoPermBrancheOrificeManoeuvre calcPseudoPermBrancheOrificeManoeuvre = new CalcPseudoPermBrancheOrificeManoeuvre(
            CrueConfigMetierForTest.DEFAULT);
    pseudo.addDclm(calcPseudoPermBrancheOrificeManoeuvre);
    OrdCalcPseudoPermIniCalcCI ordCalc = new OrdCalcPseudoPermIniCalcCI();
    ordCalc.setCalcPseudoPerm(pseudo);
    dclm.addCalcPseudoPerm(pseudo);
    ocal.addOrdCalc(ordCalc);

    CalcPseudoPerm pseudo2 = new CalcPseudoPerm();
    pseudo2.setNom("CC_P2");
    dclm.addCalcPseudoPerm(pseudo2);


    OrdCalcScenarioUiState ui = new OrdCalcScenarioUiState();
    //ici dans l'ordre on inverse les calcul
    ui.addCalcOrdCalcUiStates("CC_P2", new OrdCalcPseudoPermIniCalcPrecedent());
    ui.addCalcOrdCalcUiStates("CC_P1", new OrdCalcPseudoPermIniCalcPrecedent());
    //donc
    int idxCCP2 = 0;
    int idxCCP1 = 1;

    OrdCalcCloner cloner = new OrdCalcCloner();
    Result cloneOrdCalcAndCalc = cloner.cloneOrdCalcAndCalc(dclm, ocal, ui);
    //test des calculs:
    assertEquals(2, cloneOrdCalcAndCalc.calcs.size());
    //attention l'ordre est inversion
    assertTrue(cloneOrdCalcAndCalc.calcs.get(idxCCP2) == cloneOrdCalcAndCalc.ordCalcsInUI.get(0).getCalc());
    assertFalse(cloneOrdCalcAndCalc.calcs.get(idxCCP2) == pseudo2);

    assertTrue(cloneOrdCalcAndCalc.calcs.get(idxCCP1) == cloneOrdCalcAndCalc.ordCalcs.get(0).getCalc());
    assertFalse(cloneOrdCalcAndCalc.calcs.get(idxCCP1) == pseudo);

    //les ordres de calcul
    assertEquals(1, cloneOrdCalcAndCalc.ordCalcs.size());
    assertEquals("CC_P1", cloneOrdCalcAndCalc.ordCalcs.get(0).getCalc().getId());
    assertTrue(cloneOrdCalcAndCalc.ordCalcs.get(0) instanceof OrdCalcPseudoPermIniCalcCI);
    assertFalse(cloneOrdCalcAndCalc.ordCalcs.get(0) == ordCalc);
    //les ordres de calcul UI
    assertEquals(1, cloneOrdCalcAndCalc.ordCalcsInUI.size());
    assertEquals("CC_P2", cloneOrdCalcAndCalc.ordCalcsInUI.get(0).getCalc().getId());
    assertTrue(cloneOrdCalcAndCalc.ordCalcsInUI.get(0) instanceof OrdCalcPseudoPermIniCalcPrecedent);

  }

  @Test
  public void testIsUiSavedCalcAccordingToCalcs() {
    OrdCalcCloner cloner = new OrdCalcCloner();
    List<String> calcInOrder = Arrays.asList("CC_P1", "CC_P2");
    Collection<Calc> calcs = createCalcs("CC_P1");
    assertFalse(cloner.isUiSavedCalcAccordingToCalcs(calcs, calcInOrder));
    calcs = createCalcs("CC_P1", "CC_P2");
    assertTrue(cloner.isUiSavedCalcAccordingToCalcs(calcs, calcInOrder));
    calcs = createCalcs("CC_P1", "CC_P3");
    assertFalse(cloner.isUiSavedCalcAccordingToCalcs(calcs, calcInOrder));
  }

  private static Calc createCalc(String id) {
    CalcPseudoPerm res = new CalcPseudoPerm();
    res.setNom(id);
    return res;
  }

  private static Collection<Calc> createCalcs(String... ids) {
    List<Calc> calcs = new ArrayList<>();
    for (String id : ids) {
      calcs.add(createCalc(id));
    }
    return calcs;
  }

}
