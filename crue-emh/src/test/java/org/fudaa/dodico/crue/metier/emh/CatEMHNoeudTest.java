/*
 * Licence GPL Copyright
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * @author deniger
 */
public class CatEMHNoeudTest {

  /**
   * Test la méthode getActivated.
   */
  @Test
  public void testGetActivated() {
    final EMHScenario scenario = new EMHScenario();
    final EMHModeleBase modele = new EMHModeleBase();
    EMHRelationFactory.addRelationContientEMH(scenario, modele);
    final EMHSousModele sousModele = new EMHSousModele();
    EMHRelationFactory.addRelationContientEMH(modele, sousModele);
    final EMHBrancheOrifice br = new EMHBrancheOrifice("Br_1");
    final CatEMHNoeud nd = new EMHNoeudNiveauContinu("Nd_1");
    EMHRelationFactory.addNoeudAval(br, nd);
    EMHRelationFactory.addRelationContientEMH(sousModele, br);
    EMHRelationFactory.addRelationContientEMH(sousModele, nd);
    assertFalse(nd.getActuallyActive());
    br.setUserActive(true);
    assertTrue(nd.getActuallyActive());
    br.setUserActive(false);
    assertFalse(nd.getActuallyActive());
    br.setUserActive(true);
    modele.setUserActive(false);
    assertFalse(nd.getActuallyActive());

  }

  @Test
  public void testEnum() {
    assertEquals(EnumSensOrifice.BIDIRECT, EnumSensOrifice.valueOf("BIDIRECT"));
  }
}
