/*
 * Licence GPL Copyright
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author deniger
 */
public class CatEMHSectionTest {
    /**
     * Test la méthode getActivated.
     */
    @Test
    public void testGetActivated() {
        final EMHScenario scenario = new EMHScenario();
        final EMHModeleBase modele = new EMHModeleBase();
        EMHRelationFactory.addRelationContientEMH(scenario, modele);
        final EMHSousModele sousModele = new EMHSousModele();
        EMHRelationFactory.addRelationContientEMH(modele, sousModele);
        final EMHBrancheOrifice br1 = new EMHBrancheOrifice("Br_1");
        final EMHBrancheOrifice br2 = new EMHBrancheOrifice("Br_2");
        final EMHSectionProfil sectionProfil = new EMHSectionProfil("St_1");
        final EMHSectionIdem sectionIdem = new EMHSectionIdem("St_2");
        EMHRelationFactory.addRelationContientEMH(sousModele, sectionProfil);
        EMHRelationFactory.addRelationContientEMH(sousModele, sectionIdem);
        EMHRelationFactory.addRelationContientEMH(sousModele, br1);
        EMHRelationFactory.addRelationContientEMH(sousModele, br2);
        EMHRelationFactory.addRelationContientEMH(sousModele, sectionProfil);

        EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(br1, false, sectionProfil, null);

        br1.setUserActive(true);
        assertTrue(sectionProfil.getActuallyActive());
        br1.setUserActive(false);
        assertFalse(sectionProfil.getActuallyActive());

        //si referencée par sectionIdem idem est active si sectionIdem idem est active
        sectionIdem.setSectionRef(sectionProfil);
        EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(br2, false, sectionIdem, null);

        assertFalse(sectionProfil.getActuallyActive());
        assertFalse(sectionIdem.getActuallyActive());

        br2.setUserActive(true);
        //la section profil est active si section idem est active
        assertTrue(sectionProfil.getActuallyActive());
        assertTrue(sectionIdem.getActuallyActive());

        //plus de lisaison.
        sectionIdem.removeAllRelationEMH();
        assertFalse(sectionProfil.getActuallyActive());
    }
}
