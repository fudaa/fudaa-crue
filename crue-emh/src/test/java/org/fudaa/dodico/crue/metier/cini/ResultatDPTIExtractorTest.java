package org.fudaa.dodico.crue.metier.cini;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.factory.ScenarioBuilderForTest;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResultatDPTIExtractorTest {
  @Test
  public void testExtract() {
    EMHScenario scenario = ScenarioBuilderForTest.createDefaultScenario(CrueConfigMetierForTest.DEFAULT);
    final EMH nd1Aval = scenario.getIdRegistry().getEmhByNom().get(ScenarioBuilderForTest.ND_1);
    final EMH nd2Amont = scenario.getIdRegistry().getEmhByNom().get(ScenarioBuilderForTest.ND_2);
    EMHBrancheSaintVenant saintVenant = new EMHBrancheSaintVenant("Br_StVenant");
    saintVenant.setNoeudAval((CatEMHNoeud) nd1Aval);
    saintVenant.setNoeudAmont((CatEMHNoeud) nd2Amont);
    EMHSectionIdem section = new EMHSectionIdem("St_Idem");
    EMHSectionIdem sectionAval = new EMHSectionIdem("St_IdemAval");
    EMHRelationFactory.addRelationContientEMH(scenario.getSousModeles().get(0), saintVenant);
    EMHRelationFactory.addRelationContientEMH(scenario.getSousModeles().get(0), section);
    EMHRelationFactory.addRelationContientEMH(scenario.getSousModeles().get(0), sectionAval);

    final RelationEMHSectionDansBranche contientSectionAmont = EMHRelationFactory
        .createSectionDansBrancheAndSetBrancheContientSection(saintVenant, true, section, CrueConfigMetierForTest.DEFAULT);

    final RelationEMHSectionDansBranche contientSectionAval = EMHRelationFactory
        .createSectionDansBrancheAndSetBrancheContientSection(saintVenant, true, sectionAval, CrueConfigMetierForTest.DEFAULT);

    contientSectionAmont.setXp(0);
    contientSectionAmont.setPos(EnumPosSection.AMONT);

    contientSectionAval.setXp(100);
    contientSectionAval.setPos(EnumPosSection.AVAL);

    saintVenant.addRelationEMH(contientSectionAmont);
    saintVenant.addRelationEMH(contientSectionAval);
    scenario.getIdRegistry().register(saintVenant);
    scenario.getIdRegistry().register(section);

    nd1Aval.addInfosEMH(new ResultatCalcul(nd1Aval, createDelegate(CrueConfigMetierConstants.PROP_Z, Double.valueOf(5)), false));
    nd2Amont.addInfosEMH(new ResultatCalcul(nd2Amont, createDelegate("ERROR", Double.valueOf(5)), false));
    saintVenant.addInfosEMH(new ResultatCalcul(saintVenant, createDelegate("qlat", Double.valueOf(6)), false));
    final ResultatCalculDelegateMock delegate = createDelegate(CrueConfigMetierConstants.PROP_Q, Double.valueOf(7));
    delegate.addValue(CrueConfigMetierConstants.PROP_Z, Double.valueOf(8));
    section.addInfosEMH(new ResultatCalcul(section, delegate, false));

    ResultatDPTIExtractor extractor = new ResultatDPTIExtractor(scenario, CrueConfigMetierForTest.DEFAULT);
    CtuluIOResult<Map<CiniImportKey, Object>> importKey = extractor.extract(new ResultatTimeKey("Permanent"));
    Assert.assertEquals(5, importKey.getSource().size());

    CiniImportKey keybrancheQini = (CiniImportKey) importKey.getSource().keySet().toArray()[0];
    Double valueQini = (Double) importKey.getSource().get(keybrancheQini);

    Assert.assertEquals(saintVenant.getId(), keybrancheQini.getEmh());
    Assert.assertEquals("QINI", keybrancheQini.getTypeValue());
    Assert.assertEquals(7, valueQini.intValue());

    CiniImportKey keybrancheQRuis = (CiniImportKey) importKey.getSource().keySet().toArray()[1];
    Double valueQruis = (Double) importKey.getSource().get(keybrancheQRuis);

    Assert.assertEquals(saintVenant.getId(), keybrancheQRuis.getEmh());
    Assert.assertEquals("QRUIS", keybrancheQRuis.getTypeValue());
    Assert.assertEquals(6, valueQruis.intValue());

    CiniImportKey keyNoeudAval = (CiniImportKey) importKey.getSource().keySet().toArray()[2];
    Double value = (Double) importKey.getSource().get(keyNoeudAval);

    Assert.assertEquals(nd1Aval.getId(), keyNoeudAval.getEmh());
    Assert.assertEquals("ZINI", keyNoeudAval.getTypeValue());
    Assert.assertEquals(5, value.intValue());

    CiniImportKey keyNoeudAmont = (CiniImportKey) importKey.getSource().keySet().toArray()[3];
    Double valueAmont = (Double) importKey.getSource().get(keyNoeudAmont);

    Assert.assertEquals(nd2Amont.getId(), keyNoeudAmont.getEmh());
    Assert.assertEquals("ZINI", keyNoeudAmont.getTypeValue());
    Assert.assertEquals(8, valueAmont.intValue());

    CiniImportKey keySectionZini= (CiniImportKey) importKey.getSource().keySet().toArray()[4];
    Double valueSectionZini = (Double) importKey.getSource().get(keyNoeudAmont);

    Assert.assertEquals(section.getId(), keySectionZini.getEmh());
    Assert.assertEquals("ZINI", keySectionZini.getTypeValue());
    Assert.assertEquals(8, valueSectionZini.intValue());

    //pour le noeud amont, c'est la valeur

    importKey = extractor.extract(new ResultatTimeKey("NONPermanent", 10, false));
    Assert.assertTrue(importKey.getSource().isEmpty());
  }

  private ResultatCalculDelegateMock createDelegate(String name, Object value) {
    return new ResultatCalculDelegateMock(name, value);
  }

  private static class ResultatCalculDelegateMock implements ResultatCalculDelegate {
    Map<String, Object> values = new HashMap<>();

    public ResultatCalculDelegateMock(String name, Object value) {
      values.put(name, value);
    }

    public void addValue(String name, Object value) {
      values.put(name, value);
    }


    @Override
    public List<String> getQZRegulVariablesId() {
      return Collections.emptyList();
    }
    public List<String> getZRegulVariablesName() {
      return Collections.emptyList();
    }
    public List<String> getQRegulVariablesName() {
      return Collections.emptyList();
    }

    public void setValues(Map<String, Object> values) {
      this.values = values;
    }

    @Override
    public ResultatPasDeTempsDelegate getResultatPasDeTemps() {
      return null;
    }

    @Override
    public Map<String, Object> read(ResultatTimeKey key) throws IOException {
      return values;
    }

    @Override
    public String getEMHType() {
      return null;
    }
  }
}
