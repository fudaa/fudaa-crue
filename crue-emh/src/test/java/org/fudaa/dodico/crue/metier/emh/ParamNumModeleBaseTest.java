/*
 GPL 2
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class ParamNumModeleBaseTest {
  
  public ParamNumModeleBaseTest() {
  }

  @Test
  public void testConstructor() {
    ParamNumModeleBase pnum=new ParamNumModeleBase(CrueConfigMetierForTest.DEFAULT);
    assertNotNull(pnum.getParamNumCalcPseudoPerm());
    assertNotNull(pnum.getParamNumCalcTrans());
    assertNotNull(pnum.getParamNumCalcTrans().getPdt());
    assertEquals(900,((PdtCst)pnum.getParamNumCalcTrans().getPdt()).getPdtCst().toStandardSeconds().getSeconds());
  }
}
