package org.fudaa.dodico.crue.metier.helper;

import org.fudaa.dodico.crue.metier.emh.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * @author deniger
 */
public class VisibilityTest {
  @Test
  public void testVisibilty() {
    final String loi = "loi";
    final List<Class> classToTest = Arrays.asList(CalcTransBrancheOrificeManoeuvre.class,
        CalcTransBrancheSaintVenantQruis.class, CalcTransCasierProfilQruis.class,
        CalcTransNoeudNiveauContinuLimnigramme.class, CalcTransNoeudQapp.class,
        CalcTransNoeudNiveauContinuTarage.class);
    for (final Class class1 : classToTest) {
      Assert.assertFalse(class1.getName(), AnnotationHelper.isIhmVisible(loi, class1));
    }
  }
}
