/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.metier.emh;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frédéric Deniger
 */
public class OrdPrtGeoModeleBaseTest {
  
  public OrdPrtGeoModeleBaseTest() {
  }

  /**
   * Test of deepClone method, of class OrdPrtGeoModeleBase.
   */
  @Test
  public void testDeepClone() {
    OrdPrtGeoModeleBase instance = new OrdPrtGeoModeleBase(CrueConfigMetierForTest.DEFAULT);
    final EnumRegle enumRegle = EnumRegle.VAR_PDX_MAX;
    ((ValParamEntier) instance.getRegle(enumRegle).getValParam()).setValeur(123);
    OrdPrtGeoModeleBase deepClone = instance.deepClone();
    
    assertNotNull(deepClone);
    //on teste que ce sont bien des clones 
    assertFalse(instance.getSorties() == deepClone.getSorties());
    assertFalse(instance.getRegle(enumRegle) == deepClone.getRegle(enumRegle));
    assertEquals(((ValParamEntier) instance.getRegle(enumRegle).getValParam()).getValeur(),
            ((ValParamEntier) deepClone.getRegle(enumRegle).getValParam()).getValeur());
  }
}
