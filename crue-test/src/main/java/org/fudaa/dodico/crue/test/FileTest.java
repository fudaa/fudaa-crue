/*
 GPL 2
 */
package org.fudaa.dodico.crue.test;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import org.fudaa.ctulu.CtuluLibFile;
import org.junit.AfterClass;

/**
 *
 * @author Frederic Deniger
 */
public abstract class FileTest {

  private static final Set<File> files = new HashSet<>();

  public File createTempFile(final String prefix, final String suffix) throws IOException {
    final File f = File.createTempFile(prefix, suffix);
    files.add(f);
    return f;
  }
  
  @AfterClass
  public static void clearFiles(){
    for (final File file : files) {
      CtuluLibFile.deleteDir(file);
    }
    
  }
}
