/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common;

import junit.framework.Assert;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogLevel;

import java.util.logging.Logger;

/**
 * @author deniger
 */
public class ErrorBilanTester {
    private static final Logger LOGGER = Logger.getLogger(ErrorBilanTester.class.getName());
    private final int nbFatalErrorExpected;
    private final int nbErrorExpected;
    private final int nbWarnExpected;
    private final int nbInfoExpected;

    /**
     * @param nbFatalErrorExpected nombre d'erreur fatale attendues
     * @param nbErrorExpected      nombre d'erreur attendues
     * @param nbWarnExpected       nombre de warning attendues
     * @param nbInfoExpected       nombre d'infos attendues
     */
    public ErrorBilanTester(int nbFatalErrorExpected, int nbErrorExpected, int nbWarnExpected, int nbInfoExpected) {
        super();
        this.nbFatalErrorExpected = nbFatalErrorExpected;
        this.nbErrorExpected = nbErrorExpected;
        this.nbWarnExpected = nbWarnExpected;
        this.nbInfoExpected = nbInfoExpected;
    }

    public void testResultat(CtuluLogGroup errorManager, String nom) {
        if (errorManager.containsFatalError()) {
            String description = errorManager.getDesci18n();
            LOGGER.fine(description);
        }
        Assert.assertEquals("Test number of FatalError of " + nom, nbFatalErrorExpected, errorManager.getNbOccurence(
                CtuluLogLevel.SEVERE));
        Assert.assertEquals("Test number of Error for " + nom, nbErrorExpected, errorManager.getNbOccurence(CtuluLogLevel.ERROR));
        Assert.assertEquals("Test number of Warning for " + nom, nbWarnExpected, errorManager.getNbOccurence(CtuluLogLevel.WARNING));
        Assert.assertEquals("Test number of Info for " + nom, nbInfoExpected, errorManager.getNbOccurence(CtuluLogLevel.INFO));
    }
}
