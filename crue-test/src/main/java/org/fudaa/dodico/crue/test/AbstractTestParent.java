/*
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.crue.test;

import junit.framework.TestCase;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.junit.After;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static org.junit.Assert.*;

/**
 * Un test par defaut permettant de charger le fichier de log spécifique au débuggage. Vous pouvez utiliser votre propre
 * configuration, en créant le fichier custom.logging.properties dans le répertoire src/test/resources ( au meme endroit
 * que logging.properties).
 *
 * @author deniger
 */
public abstract class AbstractTestParent {
  private static final Logger LOGGER = Logger.getLogger(AbstractTestParent.class.getName());

  static {
    try {
      URL url = AbstractTestParent.class.getResource("/custom.logging.properties");
      if (url == null) {
        url = AbstractTestParent.class.getResource("/logging.properties");
      }
      if (url != null) {

        try (InputStream openStream = url.openStream()) {
          LogManager.getLogManager().readConfiguration(openStream);
        } catch (final Exception e) {
          e.printStackTrace();// NOPMD car tests
        }
      }
    } catch (final Exception e) {
      e.printStackTrace();// NOPMD car tests
    }
  }

  protected final List<File> tempFiles = new ArrayList<>();
  protected final List<File> filesToDelete = new ArrayList<>();

  protected AbstractTestParent() {// pour eviter l'instanciation
  }

  public static void printResume(final CtuluLogGroup manager) {
    final List<CtuluLog> logs = manager.getLogs();
    for (final CtuluLog ctuluLog : logs) {
      if (ctuluLog.isNotEmpty()) {
        ctuluLog.printResume();
      }
    }
    if (CollectionUtils.isNotEmpty(manager.getGroups())) {
      for (final CtuluLogGroup gr : manager.getGroups()) {
        printResume(gr);
      }
    }
  }

  public static void testAnalyser(final CtuluLog analyzer) {
    if (analyzer == null) {
      return;
    }
    if (analyzer.containsErrorOrSevereError()) {
      analyzer.printResume();
    }
    assertFalse(analyzer.containsErrorOrSevereError());
  }

  /**
   * Do not forget to delete targetDir at the end. Use {@link #exportZipInTempDir(String)} to let the unit test cleaning
   * the dir.
   *
   * @param targetDir   le repertoire cible
   * @param zipFilePath le path du fichier zip
   */
  public static void exportZip(final File targetDir, final String zipFilePath) {

    LOGGER.fine("dest directory is " + targetDir.getAbsolutePath());
    final InputStream inStream = AbstractTestParent.class.getResourceAsStream(zipFilePath);
    final String name = StringUtils.substringAfterLast(zipFilePath, "/");
    final File zipFile = new File(targetDir, name);

    try (FileOutputStream fileOutputStream = new FileOutputStream(zipFile)) {
      CtuluLibFile.copyStream(inStream, fileOutputStream, true, true);
      TestCase.assertTrue(zipFile.exists());
      TestCase.assertTrue(zipFile.length() > 0);
      TestCase.assertTrue(unzip(zipFile, targetDir));
    } catch (final Exception e) {
      LOGGER.log(Level.SEVERE, "fail creating project", e);
      fail(e.getMessage());
    } finally {
      CtuluLibFile.close(inStream);
    }
  }

  protected static File getEtuFileUpdated(final String etuId, final File dir) {
    final File etuFile = new File(dir, StringUtils.capitalize(etuId) + ".etu.xml");
    TestCase.assertTrue(etuFile.exists());
    final File destFile = new File(dir, StringUtils.capitalize(etuId) + ".new.etu.xml");
    CtuluLibFile.replaceAndCopyFile(etuFile, destFile, "@dir@", dir.getAbsolutePath() + "/", "UTF-8");
    TestCase.assertTrue(destFile.exists());
    return destFile;
  }

  private static boolean unzip(final File zipFile, final File targetDir) throws IOException {
    try (final ZipFile file = new ZipFile(zipFile)) {
      final Enumeration<? extends ZipEntry> entries = file.entries();
      while (entries.hasMoreElements()) {
        final ZipEntry nextElement = entries.nextElement();
        final File destFile = new File(targetDir, nextElement.getName());
        final File destinationParent = destFile.getParentFile();

        // create the parent directory structure if needed
        destinationParent.mkdirs();
        if (!nextElement.isDirectory()) {
          try (FileOutputStream out = new FileOutputStream(destFile)) {
            CtuluLibFile.copyStream(file.getInputStream(nextElement), out, true, true);
          } catch (final Exception e) {
            return false;
          } finally {
            boolean modified = destFile.setLastModified(nextElement.getTime());
            if (!modified) {
              LOGGER.log(Level.WARNING, "can change lastModified Time for " + destFile.getAbsolutePath());
            }
          }
        }
      }
      return true;
    }
  }

  public static File getFile(final String path) {
    return CtuluLibFile.getFileFromJar(path, null);
  }

  /**
   * Teste les 2 réels a une precision de 1E-10.
   *
   * @param expected la valeur vouluee
   * @param read     la valeur testee
   */
  public static void assertDoubleEquals(final double expected, final double read) {
    assertEquals(expected, read, 1E-5);
  }

  @SuppressWarnings("unused")
  @After
  public void cleanFiles() {
    for (final File file : filesToDelete) {
      try {
        if (file.isFile()) {
          Files.delete(file.toPath());
        }
      } catch (IOException e) {
        LOGGER.log(Level.SEVERE, "fail deleting file " + file.getAbsolutePath(), e);
      }
    }
    filesToDelete.clear();
    for (final File file : tempFiles) {
      if (file.isDirectory()) {
        CtuluLibFile.deleteDir(file);
      } else {
        try {
          if (file.isFile()) {
            Files.delete(file.toPath());
          }
        } catch (IOException e) {
          LOGGER.log(Level.SEVERE, "fail deleting file " + file.getAbsolutePath(), e);
        }
      }
    }
    tempFiles.clear();
  }

  protected File createTempFile() {
    File res = null;
    try {
      res = File.createTempFile("tmp", ".xml");
      tempFiles.add(res);
    } catch (final IOException e) {
      fail(e.getMessage());
    }
    return res;
  }

  /**
   * @return used to create tempDir.
   */
  protected File createTempDir() {
    File res = null;
    try {
      res = CtuluLibFile.createTempDir();
      res = res.getCanonicalFile();
      tempFiles.add(res);
    } catch (final IOException e) {
      fail(e.getMessage());
    }
    return res;
  }

  /**
   * @param zipFilePath le path du fichier zip
   * @return the temporary dir containing the zip uncompressed.
   */
  protected File exportZipInTempDir(final String zipFilePath) {
    final File dir = createTempDir();
    exportZip(dir, zipFilePath);
    return dir;
  }
}
