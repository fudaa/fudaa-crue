/*
 GPL 2
 */
package files;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Frederic Deniger
 */
public class FilesForTest {

  public static final String CONFIG_SITE = "/FudaaCrue_Site.xml";
  public static final String CONFIG_USER = "/FudaaCrue_User.xml";
  /**
   * Test du fichier inclus dans l'application
   */
  public static final String CONFIG_SITE_OFFICIEL = "/FudaaCrue_Site_officiel.xml";
  /**
   * Chemin du fichier XML de DCLM à tester
   */
  public static final String DCLM_V_1_3 = "/v1_3/M3-0_c10.dclm.xml";
  public static final String DCLM_V_1_3_DM = "/v1_3/16_DM2013.dclm.xml";
  public static final String DLHY_V_1_3_DM = "/v1_3/16_DM2013.dlhy.xml";
  public static final String DRSO_V_1_3_DM = "/v1_3/16_DM2013.drso.xml";
  public static final String DCLM_V_1_3_SY = "/v1_3/SY.dclm.xml";
  public static final String DCLM_V_1_3_SY_EMPTY = "/v1_3/SY-empty.dclm.xml";
  public static final String DLHY_V_1_3_SY = "/v1_3/SY.dlhy.xml";
  public static final String DRSO_V_1_3_SY = "/v1_3/SY.drso.xml";
  public static final String DCLM_V_1_2 = "/v1_2/M3-0_c10.dclm.xml";
  public static final String DCLM_V1_1_1 = "/v1_1_1/M3-0_c10.dclm.xml";
  public static final String DCSP_V_1_1_1 = "/v1_1_1/M3-0_c10.dcsp.xml";
  public static final String DCSP_V_1_2 = "/v1_2/M3-0_c10.dcsp.xml";
  public static final String DCSP_V_1_3 = "/v1_3/M3-0_c10.dcsp.xml";
  public static final String MODELE34_DC = "/crue9/M3-4_c9.dc";
  public static final String MODELE3_1_DC = "/crue9/M3-1_c9.dc";
  public static final String MODELE5_DC = "/crue9/M5-0_c9.dc";
  public static final String MODELE6_DC = "/crue9/M6-0_c9.dc";
  public static final String MODELE3_1_DC_ERREUR = "/crue9/Modele3-1-erreur.dc";
  public static final String MODELE3_2_DC = "/crue9/M3-2_c9.dc";
  public static final String MODELE3_DC = "/crue9/M3-0_c9.dc";
  public static final String MODELE3_DC_STRIC_INVERSE = "/crue9/M3-0_c9-stricInverse.dc";
  public static final String MODELE3_2_DC_REGLE = "/Etu3-2/M3-2_c9.dc";
  public static final String ETU_GE2_TEST_C9COR_DC = "/EtuGE2/Test_c9cor.DC";
  public static final String MODELE4_DC = "/crue9/M4-0_c9.dc";
  public static final String MODELE3_1_DC_ISORTI_1 = "/crue9/M3-1_c9-isorti_1.dc";
  public static final String MODELE3_1_DC_ISORTI_0 = "/crue9/M3-1_c9-isorti_0.dc";
  public static final String MODELE7_DC = "/crue9/M7-0_c9.dc";
  public static final String EXEMPLE_CUSTOM_DC = "/crue9/exempleDCbrancheOnly.dc";
  public static final String MANNING_DFRT_V_1_3 = "/v1_3/M3-0_c10Manning.dfrt.xml";
  public static final String MANNING_DFRT_V_1_2 = "/v1_2/M3-0_c10Manning.dfrt.xml";
  public static final String DFRT_V_1_3 = "/v1_3/M3-0_c10.dfrt.xml";
  public static final String DFRT_V_1_2 = "/v1_2/M3-0_c10.dfrt.xml";
  public static final String DFRT_V_1_1_1 = "/v1_1_1/M3-0_c10.dfrt.xml";
  public static final String ETU21_M212_C9DH = "/etu21/M21-2_c9.dh";
  public static final String ETU40_M40_E1C9_DH = "/Etu4-0/M4-0_e1c9.dh";
  public static final String ETU21_M212_C9DC = "/etu21/M21-2_c9.dc";
  public static final String ETU40_M40_C9DH = "/Etu4-0/M4-0_c9.dh";
  public static final String MODELE3_OCAL_V_1_3 = "/v1_3/M3-0_c10_noReprise.ocal.xml";
  public static final String MODELE3_OCAL_V_1_2 = "/v1_2/M3-0_c10_noReprise.ocal.xml";
  public static final String DH_M40 = "/crue9/M4-0_c9.dh";
  public static final String DH_M70 = "/crue9/M7-0_c9.dh";
  public static final String DH_M30_INCLUDE = "/crue9/M3-0_c9.dh.include";
  public static final String DH_M_32 = "/Etu3-2/M3-2_c9.dh";
  public static final String DH_M_30 = "/crue9/M3-0_c9.dh";
  public static final String DH_M_50 = "/crue9/M5-0_c9.dh";
  public static final String DH_M_60 = "/crue9/M6-0_c9.dh";
  public static final String DC_M_32 = "/Etu3-2/M3-2_c9.dc";
  public static final String DC_M_40 = "/Etu4-0/M4-0_c9.dc";
  public static final String DH_M_30_ISORTI_0 = "/crue9/M3-0_c9_isorti_0.dh";
  public static final String DLHY_V1_1_1 = "/v1_1_1/M3-0_c10.dlhy.xml";
  /**
   * Chemin du fichier XML de DLHY *
   */
  public static final String DLHY_V1_3 = "/v1_3/M3-0_c10.dlhy.xml";
  public static final String DLHY_V1_2 = "/v1_2/M3-0_c10.dlhy.xml";
  public static final String LHPT_V1_3 = "/v1_3/M3-0_c10.lhpt.xml";
  public static final String LHPT_V1_2 = "/v1_2/M3-0_c10.lhpt.xml";

  public static final String DPTG_V1_2_PROFILE_NOT_USED = "/v1_2/M3-0_c10_ProfilNotUsed.dptg.xml";
  public static final String DPTG_V1_3_PROFILE_NOT_USED = "/v1_3/M3-0_c10_ProfilNotUsed.dptg.xml";
  public static final String DPTG_V1_1_1 = "/v1_1_1/M3-0_c10.dptg.xml";
  
  public static final String DPTG_V1_2 = "/v1_2/M3-0_c10.dptg.xml";
  public static final String DPTG_V1_3 = "/v1_3/M3-0_c10.dptg.xml";
  public static final String DPTI_V1_3            = "/v1_3/M3-0_c10.dpti.xml";
  public static final String DPTI_V1_2             = "/v1_2/M3-0_c10.dpti.xml";
  public static final String DPTI_V1_1_1             = "/v1_1_1/M3-0_c10.dpti.xml";
  /**
   * path pour le fichier Modele3.drso.xml.
   */
  public static final String DRSO_V1_3 = "/v1_3/M3-0_c10.drso.xml";
  public static final String DRSO_V1_2 = "/v1_2/M3-0_c10.drso.xml";
  public static final String DRSO_V1_1_1 = "/v1_1_1/M3-0_c10.drso.xml";
  /**
   *
   */

  public static final String ETU_V1_1_1_NONACTIVE = "/v1_1_1/EtuEx_v1.1.1nonActive.etu.xml";
  public static final String ETU_V1_1_1 = "/v1_1_1/EtuEx_v1.1.1.etu.xml";
  
  
  public static final String STO_ETU30_M30 = "/Etu30/M3-0_C9C9.STO";
  
  public static final String FCB_MODELE30 = "/Etu30/M3-0_C9C9.FCB";
  public static final String LOG_TEST = "/logs.log.xml";
  
  public static final String OCAL_V1_1_1_TRANSITOIRE_REPRISE = "/v1_1_1/M3-0_c10-reprise.ocal.xml";
  public static final String OCAL_V1_1_1 = "/v1_1_1/M3-0_c10.ocal.xml";
  /**
   * Chemin du fichier XML de OCAL à tester
   */
  public static final String OCAL_V1_2 = "/v1_2/M3-0_c10.ocal.xml";
  /**
   *
   */
  public static final String OPTG_V1_3 = "/v1_3/M3-0_c10.optg.xml";
  public static final String OPTG_V1_2 = "/v1_2/M3-0_c10.optg.xml";
  public static final String OPTG_V1_1_1 = "/v1_1_1/M3-0_c10.optg.xml";
  
  
  public static final String OPTI_V1_1_1 = "/v1_1_1/M3-0_c10.opti.xml";
  public static final String OPTI_V1_3 = "/v1_3/M3-0_c10.opti.xml";
  public static final String OPTI_V1_2 = "/v1_2/M3-0_c10.opti.xml";
  /**
   *
   */
  public static final String OPTR_V1_3 = "/v1_3/M3-0_c10.optr.xml";
  public static final String OPTR_V1_2 = "/v1_2/M3-0_c10.optr.xml";
  public static final String OPTR_V1_3_FALSE = "/v1_3/M3-0_c10-false.optr.xml";
  public static final String OPTR_V1_2_FALSE = "/v1_2/M3-0_c10-false.optr.xml";

  
  public static final String ORES_V1_1_1 = "/v1_1_1/M3-0_c10.ores.xml";
  public static final String ORES_V1_2_DEFAULT = "/default-1.2.ores.xml";
  public static final String ORES_V1_1_1_DEFAULT = "/default-1.1.1.ores.xml";
  public static final String ORES_V1_2 = "/v1_2/M3-0_c10.ores.xml";
  public static final String ORES_V1_3 = "/v1_3/M3-0_c10.ores.xml";
  /**
   * Chemin du fichier XML de ORES à tester
   */
  public static final String PCAL_V1_3 = "/v1_3/M3-0_c10.pcal.xml";
  public static final String PCAL_V1_2 = "/v1_2/M3-0_c10.pcal.xml";
  /**
   * Chemin du fichier XML de PCAL à tester
   */
  public static final String PCAL_V1_1_1 = "/v1_1_1/M3-0_c10.pcal.xml";
  public static final String PCAL_V1_3_EMPTY = "/v1_3/Vierge_c10.pcal.xml";
  public static final String PCAL_V1_2_EMPTY = "/v1_2/Vierge_c10.pcal.xml";
  public static final String PCAL_V1_1_1_EMPTY = "/v1_1_1/Vierge_c10.pcal.xml";
  /**
   * Chemin du fichier XML de PNUM à tester
   */
  public static final String PNUM_V1_3 = "/v1_3/M3-0_c10.pnum.xml";
  public static final String PNUM_V1_2 = "/v1_2/M3-0_c10.pnum.xml";
  public static final String PNUM_V1_1_1 = "/v1_1_1/M3-0_c10.pnum.xml";
  
  public static final String STO_MODELE30 = "/Etu30/M3-0_C9.STO";
  public static final String STO_MODELE50 = "/etu50/M5-0_C9C9.STO";
  public static final String STO_MODELE50_LINUX = "/etu50/M5-0_c9c9.STO_lin";
  public static final String STR_MODELE50_LINUX = "/etu50/M5-0_c9c9.STR_lin";
  
  public static final String STO_MODELE30_BRANCHE6 = "/Etu30Branche6/M3-0_C9.STO";
  public static final String STR_MODELE30_BRANCHE6 = "/Etu30Branche6/M3-0_C9.STR";
  public static final String STO_MODELE30_BRANCHE6_LINUX = "/Etu30Branche6/M3-0_c9.STO_lin";
  public static final String STR_MODELE30_BRANCHE6_LINUX = "/Etu30Branche6/M3-0_c9.STR_lin";
  
  public static final String STR_MODELE50 = "/etu50/M5-0_C9C9.STR";
  public static final String AVCT_LOG_V1_2_SC_M30_C10 = "/v1_2/Sc_M3-0_c10.avct.log";
  public static final String ETUDE_XML_FUDAA_CRUE = "/FudaaCrue_Etude.xml";
  public static final String EXTERNE_XML_FUDAA_CRUE_ETUDE = "/FudaaCrue_Etude_Externe.xml";
  public static final String ETU_ETU_EX_V1_1_1 = "/v1_1_1/EtuEx_v1.1.1.etu.xml";
  public static final String ETU_V1_2 = "/v1_2/EtuEx.etu.xml";
  public static final String ETU_3_6_V1_2 = "/v1_2/Etu3-6.etu.xml";
  public static final String ETU_V1_3 = "/v1_3/EtuEx.etu.xml";
  public static final String ETU_3_6_V1_3 = "/v1_3/Etu3-6.etu.xml";

  public static final String DC_MODELE3 = "/Modele3.dc";
  public static final String DC_MODELE50 = "/etu50/M5-0_c9c9.dc";
  public static final String DC_ETU30_M30 = "/Etu30/M3-0_c9c9.dc";
  
  public static final String DH_MODELE50 = "/etu50/M5-0_c9c9.dh";
  public static final String DH_ETU30_M30 = "/Etu30/M3-0_c9c9.dh";
  
  public static final String DRSO_V1_2_ERREUR = "/v1_2/M3-0_c10-rptiErreur.drso.xml";
  
  public static final String EXTERNE_BAD_NUMBER_TXT = "/externe/badNumber.txt";
  public static final String EXTERNE_EMPTY_TXT = "/externe/empty.txt";
  public static final String EXTERNE_CORRECT_EMPTY_TXT = "/externe/correctEmptyExtern.txt";
  public static final String EXTERNE_NOT_SAME_COLUMNS_TXT = "/externe/notSameColumns.txt";
  public static final String EXTERNE_TXT = "/externe/correctExtern.txt";
  public static final String RPTI_V1_2_ERREUR = "/v1_2/M3-0_c10-rptiErreur.rpti.xml";
  public static final String RPTI_V1_2 = "/v1_2/M3-0_c10.rpti.xml";
  public static final String RPTI_V1_2_EMPTY = "/v1_2/vide.rpti.xml";
  public static final String RCAL_V1_2 = "/v1_2/M3-0_c10.rcal.xml";
  public static final String RCAL_V1_3_REGUL = "/rcal/v1_3/regul/BY.rcal.xml";
  public static final String RCAL_V1_3_NOREGUL = "/rcal/v1_3/noregul/BY.rcal.xml";
  public static final String RCAL_BIN_NAME_V1_2 = "M3-0_c10.rcal_0001.bin";
  public static final String RCAL_BIN_PATH_V1_2 = "/rcal/v1_2/" + RCAL_BIN_NAME_V1_2;
  public static final List<String> RCAL_BIN_PATH_V1_3_REGUL = Arrays.asList("/rcal/v1_3/regul/BY.rcal_0001.bin","/rcal/v1_3/regul/BY.rcal_0002.bin","/rcal/v1_3/regul/BY.rcal_0003.bin");
  public static final List<String> RCAL_BIN_PATH_V1_3_NOREGUL = Arrays.asList("/rcal/v1_3/noregul/BY.rcal_0001.bin","/rcal/v1_3/noregul/BY.rcal_0002.bin","/rcal/v1_3/noregul/BY.rcal_0003.bin");
  public static final String RCAL_XML_V1_2 = "/rcal/v1_2/M3-0_c10.rcal.xml";
  public static final String RPTG_V1_2 = "/v1_2/M3-0_c10.rptg.xml";
  public static final String RPTG_BIN_NAME = "M201-0_c9c10.rptg_0001.bin";
  public static final String RPTG_BIN_PATH_V_1_2 = "/rcal/v1_2/" + RPTG_BIN_NAME;
  public static final String RPTG_M201_1_2 = "/rcal/v1_2/M201-0_c9c10.rptg.xml";
  public static final String RPTR_V1_2_EMPTY = "/v1_2/vide.rptr.xml";
  public static final String RPTR_V1_2 = "/v1_2/M3-0_c10.rptr.xml";
  public static final String RPTR_V1_3 = "/v1_3/M3-0_c10.rptr.xml";
  public static final String DC_201 = "/crue9/201.dc";
  public static final String DH_201 = "/crue9/201.dh";
}
