/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clone;

import files.FilesForTest;
import java.util.List;
import org.apache.commons.collections4.PredicateUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.comparaison.ExecuteComparaison;
import org.fudaa.dodico.crue.comparaison.ExecuteComparaisonResult;
import org.fudaa.dodico.crue.comparaison.config.ConfComparaisonConteneur;
import org.fudaa.dodico.crue.comparaison.io.ReaderConfig;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.io.ReadHelperForTestsBuilder;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.CalcTrans;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.DonPrtCIniBranche;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheBarrageFilEau;
import org.fudaa.dodico.crue.metier.emh.EMHNoeudNiveauContinu;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.OrdCalc;
import org.fudaa.dodico.crue.metier.emh.OrdCalcScenario;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTransIniCalcCliche;
import org.fudaa.dodico.crue.metier.emh.RelationEMH;
import org.fudaa.dodico.crue.metier.emh.RelationEMHNoeudDansBranche;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class CloneTest {

  public CloneTest() {
  }


  @Test
  public void testCloneOrdCalc() {
    CrueIOResu<CrueData> data = ReadHelperForTestsBuilder.get().readModele(new CtuluLog(), FilesForTest.MODELE3_DC,
            FilesForTest.DH_M_30);
    EMHScenario scenarioData = data.getMetier().getScenarioData();
    OrdCalcScenario ordCalcScenario = scenarioData.getOrdCalcScenario();
    List<OrdCalc> ordCalcs = ordCalcScenario.getOrdCalc();
    assertFalse(ordCalcs.isEmpty());
    for (OrdCalc ordCalc : ordCalcs) {
      OrdCalc deepCloneButNotCalc = ordCalc.deepCloneButNotCalc(ordCalc.getCalc());
      assertNotNull(deepCloneButNotCalc);
    }
    List<Calc> calcs = scenarioData.getDonCLimMScenario().getCalc();
    assertFalse(calcs.isEmpty());
    for (Calc calc : calcs) {
      Calc deepCloneButNotCalc = calc.deepClone();
      assertNotNull(deepCloneButNotCalc);

    }
  }

  @Test
  public void testCloneOrdCalcTransIni() {
    OrdCalcTransIniCalcCliche ini = new OrdCalcTransIniCalcCliche(CrueConfigMetierForTest.DEFAULT);
    ini.getIniCalcCliche().setNomFic("test");
    final CalcTrans calcTrans = new CalcTrans();
    OrdCalcTransIniCalcCliche cloned = ini.deepCloneButNotCalc(calcTrans);
    assertEquals("test", cloned.getIniCalcCliche().getNomFic());
    assertFalse(cloned.getIniCalcCliche() == ini.getIniCalcCliche());
    assertTrue(cloned.getCalc() == calcTrans);
  }
}
