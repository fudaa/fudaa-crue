package org.fudaa.dodico.crue.io.dptg;

import files.FilesForTest;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.drso.CrueDRSOFileTest;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;

/**
 * Test des fichiers DPTG.
 *
 * @author Adrien Hadoux
 */
public class CrueDPTGFileTest extends AbstractIOParentTest {
    public CrueDPTGFileTest() {
        super(Crue10FileFormatFactory.getVersion(CrueFileType.DPTG, TestCoeurConfig.INSTANCE), FilesForTest.DPTG_V1_3);
    }

    @Test
    public void testLecture() {
        final CrueData dataDRSOetDFRT = readDptg(FilesForTest.DPTG_V1_3, FilesForTest.DRSO_V1_3,
                FilesForTest.DFRT_V_1_3, TestCoeurConfig.INSTANCE);
        // -- section SPROF11 qui contient le profil PROF11--//
        testData(dataDRSOetDFRT);
        testEtiquettes(dataDRSOetDFRT);
    }

    @Test
    public void testLecture_V_1_2() {
        final CrueData dataDRSOetDFRT = readDptg(FilesForTest.DPTG_V1_2, FilesForTest.DRSO_V1_2,
                FilesForTest.DFRT_V_1_2, TestCoeurConfig.INSTANCE_1_2);
        // -- section SPROF11 qui contient le profil PROF11--//
        testData(dataDRSOetDFRT);
        testEtiquettes(dataDRSOetDFRT);
    }

    /**
     * Pour s'assurer que les etiquettes ne sont pas écrites en version 1.1.1
     */
    @Test
    public void testLecture1_3_Et_Ecriture1_1() {
        final CrueData data = readDptg(FilesForTest.DPTG_V1_3, FilesForTest.DRSO_V1_3,
                FilesForTest.DFRT_V_1_3, TestCoeurConfig.INSTANCE);
        final File dptg = createTempFile();
        final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DPTG,
                TestCoeurConfig.INSTANCE_1_1_1);
        final CtuluLog log = new CtuluLog();
        fileFormat.write(data, dptg, log);
        Assert.assertTrue(fileFormat.isValide(dptg, log));
        dptg.delete();
    }

    @Test
    public void testLectureNotUsed() {
        final CrueData dataDRSOetDFRT = readDrsoEtDFrt(FilesForTest.DRSO_V1_3, FilesForTest.DFRT_V_1_3,
                TestCoeurConfig.INSTANCE);
        // -- lecture DPTG --//
        final CtuluLog analyzer = new CtuluLog();

        Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DPTG, TestCoeurConfig.INSTANCE).read(
                FilesForTest.DPTG_V1_3_PROFILE_NOT_USED, analyzer,
                dataDRSOetDFRT);
        Assert.assertTrue(analyzer.containsSevereError());
        final CtuluLogRecord record = analyzer.getRecords().get(0);
        Assert.assertEquals(CtuluLogLevel.SEVERE, record.getLevel());
        Assert.assertEquals("io.profilSectionNotLinkedToSection.error", record.getMsg());
        final Object arg0 = record.getArgs()[0];
        Assert.assertEquals("Ps_Prof11_NotUsed", arg0);
    }

    @Test
    public void testLecture_V_1_1_1() {
        final CrueData dataDRSOetDFRT = readDptg(FilesForTest.DPTG_V1_1_1,
                FilesForTest.DRSO_V1_1_1,
                FilesForTest.DFRT_V_1_1_1,
                TestCoeurConfig.INSTANCE_1_1_1);
        // -- section SPROF11 qui contient le profil PROF11--//
        testData(dataDRSOetDFRT);
    }

    private void testEtiquettes(final CrueData dataDRSOetDFRT) {
        final EMHSectionProfil sectionProfil = (EMHSectionProfil) dataDRSOetDFRT.findSectionByReference("St_PROF1");
        final DonPrtGeoProfilSection donnee = (DonPrtGeoProfilSection) sectionProfil.getDPTG().get(0);
        Assert.assertNotNull(donnee);
        final List<DonPrtGeoProfilEtiquette> etiquettes = donnee.getEtiquettes();
        Assert.assertEquals(2, etiquettes.size());
        DonPrtGeoProfilEtiquette et = etiquettes.get(0);
        Assert.assertEquals(dataDRSOetDFRT.getCrueConfigMetier().getLitNomme().getEtiquetteThalweg(), et.getTypeEtiquette());
        assertDoubleEquals(62.0, et.getPoint().getXt());
        assertDoubleEquals(2.1, et.getPoint().getZ());
        et = etiquettes.get(1);
        Assert.assertEquals(dataDRSOetDFRT.getCrueConfigMetier().getLitNomme().getEtiquetteAxeHyd(), et.getTypeEtiquette());
        assertDoubleEquals(90.0, et.getPoint().getXt());
        assertDoubleEquals(2.1, et.getPoint().getZ());
    }

    private void testData(final CrueData dataDRSOetDFRT) {
        final EMHSectionProfil sectionProfil = (EMHSectionProfil) dataDRSOetDFRT.findSectionByReference("St_PROF11");
        Assert.assertNotNull(sectionProfil);
        final DonPrtGeoProfilSection donnee = (DonPrtGeoProfilSection) sectionProfil.getDPTG().get(0);
        Assert.assertNotNull(donnee);

        // -- 10 PointFF --//
        Assert.assertEquals(10, donnee.getPtProfil().size());
        // -- 5 lit numerotes --//
        final List<LitNumerote> litNumerotes = donnee.getLitNumerote();
        Assert.assertEquals(5, litNumerotes.size());

        // -- lit numerote 1 --//
        Assert.assertEquals("Lt_Stockage", litNumerotes.get(0).getNomLit().getNom());
        PtProfil pt = litNumerotes.get(0).getLimDeb();
        assertDoubleEquals(0, pt.getXt());
        assertDoubleEquals(6.1, pt.getZ());
        pt = litNumerotes.get(0).getLimFin();
        assertDoubleEquals(32, pt.getXt());
        assertDoubleEquals(4.1, pt.getZ());
    }

    @Test
    public void testEcriture_V1_1_1() {
        testLectureEcriture(FilesForTest.DPTG_V1_1_1, FilesForTest.DRSO_V1_1_1,
                FilesForTest.DFRT_V_1_1_1, TestCoeurConfig.INSTANCE_1_1_1);
    }

    @Test
    public void testEcriture_V_1_2() {
        testLectureEcriture(FilesForTest.DPTG_V1_2, FilesForTest.DRSO_V1_2,
                FilesForTest.DFRT_V_1_2, TestCoeurConfig.INSTANCE_1_2);
    }

    @Test
    public void testEcritureEnVersion() {
        testLectureEcriture(FilesForTest.DPTG_V1_3, FilesForTest.DRSO_V1_3,
                FilesForTest.DFRT_V_1_3, TestCoeurConfig.INSTANCE);
    }

    private void testLectureEcriture(final String dptgFile, final String drso, final String frt, final CoeurConfigContrat version) {
        CrueData data = readDptg(dptgFile, drso, frt, version);
        final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DPTG,
                version);
        final File dptg = createTempFile();
        final CtuluLog log = new CtuluLog();
        fileFormat.write(data, dptg, log);
        data = readDptg(dptg, drso, frt, version);
        testAnalyser(log);
        testData(data);
        final DonPrtGeoBatiCasier definedBatiCasier = data.getDefinedBatiCasier("Bc_N6");
        Assert.assertNotNull(definedBatiCasier);
        if (Crue10VersionConfig.isV1_1_1(version)) {
            Assert.assertTrue(StringUtils.isEmpty(definedBatiCasier.getCommentaire()));
        } else {
            Assert.assertEquals("Test pour Bc_N6", definedBatiCasier.getCommentaire());
            testEtiquettes(data);
        }
    }

    private CrueData readDptg(final String dptg, final String drso, final String dfrt, final CoeurConfigContrat version) {
        final CrueData dataDRSOetDFRT = readDrsoEtDFrt(drso, dfrt, version);
        // -- lecture DPTG --//
        final CtuluLog analyzer = new CtuluLog();

        Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DPTG, version).read(dptg, analyzer, dataDRSOetDFRT);
        testAnalyser(analyzer);
        Assert.assertTrue(Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DPTG, version).isValide(dptg, analyzer));
        return dataDRSOetDFRT;
    }

    private CrueData readDptg(final File dptg, final String drso, final String dfrt, final CoeurConfigContrat version) {
        final CrueData dataDRSOetDFRT = readDrsoEtDFrt(drso, dfrt, version);
        // -- lecture DPTG --//
        final CtuluLog analyzer = new CtuluLog();

        Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DPTG, version).read(dptg, analyzer, dataDRSOetDFRT);
        testAnalyser(analyzer);
        Assert.assertTrue(Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DPTG, version).isValide(dptg, analyzer));
        return dataDRSOetDFRT;
    }

    private CrueData readDrsoEtDFrt(final String drsoFile, final String dfrt, final CoeurConfigContrat version) {
        // -- lecture DRSO --//
        final CtuluLog analyzer = new CtuluLog();
        final CrueData dataDRSOetDFRT = (CrueData) Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DRSO, version).read(drsoFile,
                analyzer,
                CrueDRSOFileTest.createDefault(version)).getMetier();
        testAnalyser(analyzer);

        // -- lecture DFRT --//
        final List<DonFrt> jeuDonnees = (List<DonFrt>) Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DFRT, version).read(
                dfrt, analyzer, createDefault(version)).getMetier();
        testAnalyser(analyzer);
        dataDRSOetDFRT.setFrottements(jeuDonnees);
        return dataDRSOetDFRT;
    }
}
