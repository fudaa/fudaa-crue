/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.io.rpti;

import org.junit.Test;

/**
 *
 * @author deniger
 */
public class CrueConverterRPTITest {

  public CrueConverterRPTITest() {
  }

  @Test
  public void testHexaToDoube() {
    org.junit.Assert.assertEquals(2.926d, CrueConverterRPTI.hexaToDouble("40076872B020C49C"),1e-15);
    org.junit.Assert.assertEquals("40076872B020C49C", CrueConverterRPTI.doubleToHexa(2.926d).toUpperCase());
  }
}
