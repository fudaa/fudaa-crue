package org.fudaa.dodico.crue.io.rcal;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.ItemResDao;
import org.fudaa.dodico.crue.io.res.*;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.junit.After;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * @author deniger
 */
public class CrueRCALBinaire_V1_2_Test {
  private final List<File> filesToDelete = new ArrayList<>();

  private static class Result {
    RCalTimeStepBuilder.Result timeStepExtracted;
    List<ResCatEMHContent> categories;
    CrueDaoRCAL rcal;
  }

  @Test
  public void testReadResultat() throws IOException {
    final Result extracted = extracted();
    final Crue10ResultatPasDeTempsDelegate timeSteps = new Crue10ResultatPasDeTempsDelegate(extracted.timeStepExtracted.getOrderedKey(),
        extracted.timeStepExtracted.getEntries());
    final ResTypeEMHContent typeNoeud = extracted.categories.get(0).getResTypeEMHContent().get(0);
    assertEquals("NoeudNiveauContinu", typeNoeud.getEmhType());
    final ItemResDao nd1 = typeNoeud.getItemResDao().get(0);
    assertEquals("Nd_N1", nd1.getNomRef());
    final ResultatCalculCrue10 res = new ResultatCalculCrue10(typeNoeud.getItemPositionInBytes(nd1.getNomRef()),
        nd1.getNbrMot() * extracted.rcal.getParametrage().getNbrOctetMot(),
        typeNoeud.getResVariablesContent(),
        timeSteps, typeNoeud.getEmhType(), nd1.getNomRef().toUpperCase());
    final ResultatTimeKey keyccp1 = new ResultatTimeKey("Cc_P1");
    final ResultatTimeKey keyccp2 = new ResultatTimeKey("Cc_P2");
    Map<String, Object> read = res.read(keyccp1);
    assertTrue(read.containsKey("z"));
    assertEquals(3.57328, ((Double) read.get("z")).doubleValue(), 1e-5);
    read = res.read(keyccp2);
    assertTrue(read.containsKey("z"));
    assertEquals(3.57475, ((Double) read.get("z")).doubleValue(), 1e-5);
    //ND7
    final ItemResDao nd7 = typeNoeud.getItemResDao().get(6);
    assertEquals("Nd_N7", nd7.getNomRef());
    final ResultatCalculCrue10 resNd7 = new ResultatCalculCrue10(typeNoeud.getItemPositionInBytes(nd7.getNomRef()),
        nd7.getNbrMot() * extracted.rcal.getParametrage().getNbrOctetMot(),
        typeNoeud.getResVariablesContent(),
        timeSteps, typeNoeud.getEmhType(), nd7.getNomRef().toUpperCase());
    read = resNd7.read(keyccp1);
    assertTrue(read.containsKey("z"));
    assertEquals(2.926, ((Double) read.get("z")).doubleValue(), 1e-4);

    final ResTypeEMHContent typeSectionProfil = extracted.categories.get(2).getResTypeEMHContent().get(2);
    assertEquals("SectionProfil", typeSectionProfil.getEmhType());
    final ItemResDao sectionProf9 = typeSectionProfil.getItemResDao().get(10);
    assertEquals("St_PROF9", sectionProf9.getNomRef());
    final ResultatCalculCrue10 resStr1 = new ResultatCalculCrue10(typeSectionProfil.getItemPositionInBytes(sectionProf9.getNomRef()),
        sectionProf9.getNbrMot() * extracted.rcal.getParametrage().getNbrOctetMot(),
        typeSectionProfil.getResVariablesContent(), timeSteps,
        typeSectionProfil.getEmhType(), sectionProf9.getNomRef().toUpperCase());
    read = resStr1.read(keyccp1);
    assertEquals(19, read.size());
  }

  private Result extracted() throws IOException {
    final File createTempDir = CtuluLibFile.createTempDir();
    filesToDelete.add(createTempDir);
    final File file = new File(createTempDir, FilesForTest.RCAL_BIN_NAME_V1_2);
    CtuluLibFile.getFileFromJar(FilesForTest.RCAL_BIN_PATH_V1_2, file);

    assertTrue(file.exists());
    final CrueDaoRCAL rcal = readXml();
    final ResTypeEMHBuilder typeBuilder = new ResTypeEMHBuilder(rcal);
    final CrueIOResu<List<ResCatEMHContent>> extract = typeBuilder.extract();
    final RCalTimeStepBuilder timeStepBuilder = new RCalTimeStepBuilder();
    final RCalTimeStepBuilder.Result timeStepExtracted = timeStepBuilder.extract(rcal, file.getParentFile());
    final List<ResCatEMHContent> categories = extract.getMetier();
    final Result res = new Result();
    res.categories = categories;
    res.timeStepExtracted = timeStepExtracted;
    res.rcal = rcal;
    return res;
  }

  @After
  public void cleanFiles() {
    for (final File file : filesToDelete) {
      if (file.isDirectory()) {
        CtuluLibFile.deleteDir(file);
      } else {
        file.delete();
      }
    }
    filesToDelete.clear();
  }

  @Test
  public void testReadXml() {
    final CrueDaoRCAL rcal = readXml();
    assertNotNull(rcal);
  }

  private CrueDaoRCAL readXml() {
    final URL resource = AbstractIOParentTest.getResource(FilesForTest.RCAL_XML_V1_2);
    final Crue10FileFormat version = Crue10FileFormatFactory.getVersion(CrueFileType.RCAL, TestCoeurConfig.INSTANCE);
    final CtuluLog log = new CtuluLog();
    final CrueIOResu read = version.read(resource, log, null);
    assertFalse(log.containsErrorOrSevereError());
    final CrueDaoRCAL rcal = (CrueDaoRCAL) read.getMetier();
    return rcal;
  }
}
