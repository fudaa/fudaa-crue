package org.fudaa.dodico.crue.io.neuf;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.SeveriteManager;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.Crue9FileFormatFactory;
import org.fudaa.dodico.crue.io.ReadHelperForTestsBuilder;
import org.fudaa.dodico.crue.io.dfrt.CrueDFRTFileTest;
import org.fudaa.dodico.crue.io.drso.CrueDRSOFileTest;
import org.fudaa.dodico.crue.io.optg.CrueOPTGFileTest;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Tests junit pour les fichiers DC IO. fortran, .
 *
 * @author Adrien Hadoux
 */
public class CrueDCTest extends AbstractTestParent {
  /**
   */
  public static void main(final String[] _args) {
    final CrueDCTest test = new CrueDCTest();
    test.testLectureEcritureCRUE9Modele5();
  }

  @Test
  public void testArrondi() {
    final double dist = 104;
    final float distF = (float) 104d;
    final float nb = 3f;

    final float distMax = (distF / 3);

    // DISTANCE 34.666664123535156 34.66666793823242 34.66666793823242
    final float first = 104f - 2f * distMax;
    Assert.assertEquals("34.666664123535156", Double.toString(first));
    Assert.assertEquals("34.66666793823242", Double.toString(distMax));
    final double x1 = first;
    final double x2 = first + distMax;
    final float delta = (float) (x2 - x1);
    final double d = Math.nextUp(delta);
    Assert.assertEquals("34.666664123535156", Double.toString(x1));
    Assert.assertEquals("34.66666793823242", Double.toString(d));
  }

  @Test
  public void testDistmaxOnEtuGE2() {
    final CtuluLog log = new CtuluLog();
    final CrueData data = readModeleCrue9(log, FilesForTest.ETU_GE2_TEST_C9COR_DC);
    testAnalyser(log);
    final CatEMHBranche emhBranche = data.findBrancheByReference("B1");
    Assert.assertNotNull(emhBranche);
    final List<RelationEMHSectionDansBranche> sections = emhBranche.getSections();
    Assert.assertEquals(20, sections.size());

    final List<String> distances = Arrays.asList("34.666664123535156", "34.66666793823242", "34.66666793823242", "36.0",
        "36.0", "36.0", "34.666664123535156", "34.66666793823242", "34.66666793823242", "49.0",
        "49.0", "50.0", "50.0",
        "49.5", "49.5", "50.0", "50.0", "49.0", "49.0");
    final List<Double> cpond = Arrays.asList(1d, 1d, 1d, 2d, 2d, 2d, 3d, 3d, 3d, 4d, 4d, 5d, 5d, 6d, 6d, 7d, 7d, 8d, 8d);
    for (int i = 1; i < sections.size(); i++) {
      final RelationEMHSectionDansBrancheSaintVenant sectionDansBranche = (RelationEMHSectionDansBrancheSaintVenant) sections.get(i);
      final RelationEMHSectionDansBrancheSaintVenant sectionDansBranchePrec = (RelationEMHSectionDansBrancheSaintVenant) sections.get(
          i - 1);
      final double x = (float) (sectionDansBranche.getXp() - sectionDansBranchePrec.getXp());
      Assert.assertEquals(distances.get(i - 1).trim(), Double.toString(x).trim());
      assertDoubleEquals(cpond.get(i - 1), sectionDansBranche.getCoefPond());
    }
  }

  /**
   * @return CrueData
   */
  public static CrueData readModeleCrue9(final CtuluLog analyzer, final String path) {
    return ReadHelperForTestsBuilder.get().readModeleCrue9(analyzer,path);
  }

  public static CrueData readModeleCrue9(final CtuluLog analyzer, final URL url) {
    return ReadHelperForTestsBuilder.get().readModeleCrue9(analyzer,url);
  }

  private static void writeModeleCrue9(final CtuluLog analyzer, final File f, final CrueData data) {
    Crue9FileFormatFactory.getDCFileFormat().write(data, f, analyzer);
  }

  public CrueDCTest() {
  }

  @Override
  protected File createTempFile() {
    File res = null;
    try {
      res = File.createTempFile("tmp", ".dc");
      tempFiles.add(res);
    } catch (final IOException e) {
      Assert.fail(e.getMessage());
    }
    return res;
  }

  private File createTemptxtFile(final String titre) {
    File res = null;
    try {
      res = File.createTempFile(titre + "tmpDC", ".txt");
      tempFiles.add(res);
    } catch (final IOException e) {
      Assert.fail(e.getMessage());
    }
    return res;
  }

  public static void assertSortieTrueAndInfo(final Sorties sorties) {
    Assert.assertTrue(sorties.getAvancement().getSortieFichier());
    Assert.assertTrue(sorties.getTrace().getSortieEcran());
    Assert.assertTrue(sorties.getTrace().getSortieFichier());
    Assert.assertEquals(SeveriteManager.INFO, sorties.getTrace().getVerbositeEcran());
    Assert.assertEquals(SeveriteManager.INFO, sorties.getTrace().getVerbositeFichier());
  }

  public static void assertSortieTrueAndDebug(final Sorties sorties) {
    Assert.assertTrue(sorties.getAvancement().getSortieFichier());
    Assert.assertTrue(sorties.getTrace().getSortieEcran());
    Assert.assertTrue(sorties.getTrace().getSortieFichier());
    Assert.assertEquals(SeveriteManager.DEFAULT_DEBUG_LEVEL_FOR_CRUE9, sorties.getTrace().getVerbositeEcran());
    Assert.assertEquals(SeveriteManager.DEFAULT_DEBUG_LEVEL_FOR_CRUE9, sorties.getTrace().getVerbositeFichier());
  }

  private void testBatiCasier(final CrueData data) {
    Assert.assertNotNull(data);
//    Crue9PostLoadValidation.verifiePrefixeNomDonneesCrue9(data, state);
    final CatEMHCasier findCasierByReference = data.findCasierByReference("Ca_N6");
    Assert.assertNotNull(findCasierByReference);
    final DonPrtGeoBatiCasier bati = EMHHelper.selectFirstOfClass(findCasierByReference.getInfosEMH(),
        DonPrtGeoBatiCasier.class);
    assertDoubleEquals(5000, bati.getSplanBati());
    assertDoubleEquals(0.35, bati.getZBatiTotal());
  }

  @Test
  public void testCrue9Modele34() {
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueData data = readModeleCrue9(analyzer, FilesForTest.MODELE34_DC);
    testBatiCasier(data);
    final File file = createTempFile();
    writeModeleCrue9(analyzer, file, data);
    AbstractTestParent.testAnalyser(analyzer);
    try {
      testBatiCasier(readModeleCrue9(analyzer, file.toURL()));
    } catch (final MalformedURLException e) {
      Assert.fail(e.getMessage());
    }
  }

  private void testData(final CrueData data) {
    Assert.assertNotNull(data);
  }

  /**
   * Test ecriture d'un fichier crue 9 depuis des données crue 10 apr morceau.
   */
  @Test
  public void testLectureCrue10EcritureCrue9() {
    final CtuluLog analyzer = new CtuluLog();
    // -- lecture DRSO --//
    final CrueData crueData = (CrueData) Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DRSO,
        TestCoeurConfig.INSTANCE).read(
        FilesForTest.DRSO_V1_3, analyzer, CrueDRSOFileTest.createDefault(TestCoeurConfig.INSTANCE)).getMetier();
    // -- lecture optg --//
    CrueOPTGFileTest.readModeleLastVersion(crueData);
    // -- lecture dfrt --//
    final List<DonFrt> jeuDonnees = CrueDFRTFileTest.readModeleLastVersion();
    crueData.setFrottements(jeuDonnees);

    // -- lecture DPTI --//
    Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DPTI, TestCoeurConfig.INSTANCE).read(
        FilesForTest.DPTI_V1_3, analyzer, crueData);
    // -- lecture DPTG --//
    Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DPTG, TestCoeurConfig.INSTANCE).read(
        FilesForTest.DPTG_V1_3, analyzer, crueData);

    // -- ecriture dans un fichier Crue 9 --//
    final File f = createTemptxtFile("tansformCrue10to9modele3");
    writeModeleCrue9(analyzer, f, crueData);
  }

  /**
   *
   */
  @Test
  public void testLectureCrue9EcritureCrue10ExempleCustom() {
    testLectureFichierCrue9etEcritureCrue10(FilesForTest.EXEMPLE_CUSTOM_DC);
  }

  /**
   *
   */
  @Test
  public void testLectureCrue9EcritureCrue10Modele3() {
    testLectureFichierCrue9etEcritureCrue10(FilesForTest.MODELE3_DC);
  }

  /**
   * un fichier DC avec la ligne STRIC aprés LIT
   */
  @Test
  public void testLectureCrue9EcritureCrue10Modele3StricInverse() {
    final CtuluLog log = new CtuluLog();
    final CrueData data = readModeleCrue9(log, FilesForTest.MODELE3_DC_STRIC_INVERSE);
    Assert.assertNotNull(data);
    final CatEMHSection prof = data.findSectionByReference("PROF10");
    Assert.assertNotNull(prof);
    //on s'assure que tous les lit ont un nom.
    final List<DonPrtGeo> dptg = prof.getDPTG();
    Assert.assertEquals(1, dptg.size());
    for (final DonPrtGeo donPrtGeo : dptg) {
      final List<LitNumerote> litNumerote = ((DonPrtGeoProfilSection) donPrtGeo).getLitNumerote();
      for (final LitNumerote litNumerote1 : litNumerote) {
        Assert.assertNotNull(litNumerote1.getNomLit());
        Assert.assertNotNull(litNumerote1.getNomLit().getNom());
      }
    }
  }

  /**
   * un fichier DC avec la ligne STRIC aprés LIT
   */
  @Test
  public void testLectureCrue9EcritureCrue10Modele3NoStricFound() {
    final CtuluLog log = new CtuluLog();
    final CrueData data = readModeleCrue9(log, "/crue9/M3-0_c9-errorStricManquant.dc");
    Assert.assertNotNull(data);
    Assert.assertTrue(log.containsSevereError());
    Assert.assertEquals(1, log.getRecords().size());
    final CtuluLogRecord record = log.getRecords().get(0);
    Assert.assertEquals("dc.stricCarteNotFound", record.getMsg());
    Assert.assertEquals(144, record.getArgs()[0]);
    Assert.assertEquals("PROF10", record.getArgs()[1]);
  }

  /**
   * un fichier DC avec la ligne STRIC aprés LIT
   */
  @Test
  public void testLectureCrue9EcritureCrue10Modele3WrongLitSize() {
    final CtuluLog log = new CtuluLog();
    final CrueData data = readModeleCrue9(log, "/crue9/M3-0_c9-errorWrongSize.dc");
    Assert.assertNotNull(data);
    Assert.assertTrue(log.containsSevereError());
    Assert.assertEquals(1, log.getRecords().size());
    final CtuluLogRecord record = log.getRecords().get(0);
    Assert.assertEquals("dc.lit.wrongSize", record.getMsg());
    Assert.assertEquals("PROF10", record.getArgs()[1]);
  }

  /**
   *
   */
  @Test
  public void testLectureCrue9EcritureCrue10Modele3_1() {
    testLectureFichierCrue9etEcritureCrue10(FilesForTest.MODELE3_1_DC);
  }

  /**
   * test un ficheir avec erreur et contenant une branche de saint-venant avec des distances inter-profils null.
   */
  @Test
  public void testLectureCrue9Modele3_1_avecErreur() {
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    readModeleCrue9(analyzer, FilesForTest.MODELE3_1_DC_ERREUR);
    Assert.assertTrue(analyzer.containsSevereError());
  }

  @Test
  public void testLectureCrue9Modele3_0_avecErreur() {
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    readModeleCrue9(analyzer, "/crue9/M3-0_e1c9.dc");
    Assert.assertTrue(analyzer.containsSevereError());
  }

  /**
   *
   */
  @Test
  public void testLectureCrue9EcritureCrue10Modele3_2() {
    testLectureFichierCrue9etEcritureCrue10(FilesForTest.MODELE3_2_DC);
  }

  /**
   *
   */
  @Test
  public void testLectureCrue9EcritureCrue10Modele4() {
    testLectureFichierCrue9etEcritureCrue10(FilesForTest.MODELE4_DC);
  }

  /**
   *
   */
  @Test
  public void testLectureCrue9EcritureCrue10Modele5() {
    testLectureFichierCrue9etEcritureCrue10(FilesForTest.MODELE5_DC);
  }

  /**
   * Test cycle de lecture/ecriture du modele 3 complet.
   */
  @Test
  public void testLectureEcritureCRUE9Modele3() {
    final CtuluLog analyzer = new CtuluLog();
    // -- lecture --//
    final CrueData data = testLectureFichierDC(FilesForTest.MODELE3_DC);
    final EMHSectionIdem findByReference = (EMHSectionIdem) data.findSectionByReference("PROF6B");
    Assert.assertEquals("PROF6A", findByReference.getSectionRef().getNom());
    final CatEMHBranche br1 = data.findBrancheByReference("B1");
    Assert.assertNotNull(br1);
    final List<RelationEMHSectionDansBranche> sections = br1.getSections();
    Assert.assertEquals(11, sections.size());
    double x = 0;
    for (final RelationEMHSectionDansBranche section : sections) {
      assertDoubleEquals(x, section.getXp());
      x = x + 50;
    }
    // -- ecriture --//
    final File f = createTemptxtFile("modele3");
    writeModeleCrue9(analyzer, f, data);
    if (analyzer.containsErrorOrSevereError()) {
      analyzer.printResume();
    }
    Assert.assertFalse(analyzer.containsErrorOrSevereError());
    try {
      final CrueData written = readModeleCrue9(analyzer, f.toURI().toURL());
      //on teste que l'écriture a bien écrit le frottement:
      final DonFrt fko = EMHHelper.find(written.getFrottements().getListFrt(), "K0");
      Assert.assertNotNull(fko);
      Assert.assertEquals(EnumTypeLoi.LoiZFKSto, fko.getLoi().getType());
    } catch (final MalformedURLException malformedURLException) {
      Assert.fail(malformedURLException.getMessage());
    }
  }

  @Test
  public void testLectureEcritureCRUE9Modele3_1() {
    final CtuluLog analyzer = new CtuluLog();
    // -- lecture --//
    final CrueData data = testLectureFichierDC(FilesForTest.MODELE3_1_DC);
    final CatEMHCasier casier = data.findCasierByReference("Ca_N6");
    Assert.assertNotNull(casier);
    testLitUtileIsNotNull(casier);
    final File f = createTemptxtFile("modele3_1");
    writeModeleCrue9(analyzer, f, data);
    try {
      final CrueData dataRead = readModeleCrue9(analyzer, f.toURI().toURL());
      final CatEMHCasier casierRead = dataRead.findCasierByReference("Ca_N6");
      Assert.assertNotNull(casierRead);
      testLitUtileIsNotNull(casierRead);
    } catch (final MalformedURLException e) {
      Logger.getLogger(CrueDCTest.class.getName()).log(Level.INFO, "testLectureEcritureCRUE9Modele3_1", e);
      Assert.fail(e.getMessage());
    }
  }

  private void testLitUtileIsNotNull(final CatEMHCasier casier) {
    final List<DonPrtGeoProfilCasier> selectInfoEMH = EMHHelper.selectClass(casier.getInfosEMH(), DonPrtGeoProfilCasier.class);
    for (final DonPrtGeoProfilCasier donPrtGeoProfilCasier : selectInfoEMH) {
      Assert.assertNotNull(donPrtGeoProfilCasier.getLitUtile());
    }
  }

  /**
   * Test cycle de lecture/ecriture du modele 4 complet.
   */
  @Test
  public void testLectureEcritureCRUE9Modele4() {
    final CtuluLog analyzer = new CtuluLog();
    // -- lecture --//
    final CrueData data = testLectureFichierDC(FilesForTest.MODELE4_DC);

    // -- ecriture --//
    final File f = createTemptxtFile("modele4");
    writeModeleCrue9(analyzer, f, data);
  }

  /**
   * Test cycle de lecture/ecriture du modele 5 complet.
   */
  @Test
  public void testLectureEcritureCRUE9Modele5() {
    final CtuluLog analyzer = new CtuluLog();
    // -- lecture --//
    final CrueData data = testLectureFichierDC(FilesForTest.MODELE5_DC);
    // -- ecriture --//
    final File f = createTemptxtFile("modele5");
    writeModeleCrue9(analyzer, f, data);
  }

  @Test
  public void testDcWithSorti0() {
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueData data = readModeleCrue9(analyzer, FilesForTest.MODELE3_1_DC_ISORTI_0);
    Assert.assertFalse(analyzer.containsSevereError());
    assertSortieTrueAndInfo(data.getOPTR().getSorties());
    assertSortieTrueAndInfo(data.getOPTG().getSorties());
    assertSortieTrueAndInfo(data.getOPTI().getSorties());
  }

  @Test
  public void testDcWithSorti0TransformeEn1() {
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueData data = readModeleCrue9(analyzer, FilesForTest.MODELE3_1_DC_ISORTI_0);
    Assert.assertFalse(analyzer.containsSevereError());
    data.getOPTG().getSorties().getTrace().setVerbositeEcran(SeveriteManager.DEFAULT_DEBUG_LEVEL_FOR_CRUE9);
    final File f = createTempFile();
    writeModeleCrue9(analyzer, f, data);
    try {
      final CrueData dataWith1 = readModeleCrue9(analyzer, f.toURI().toURL());
      Assert.assertFalse(analyzer.containsSevereError());
      assertSortieTrueAndDebug(dataWith1.getOPTR().getSorties());
      assertSortieTrueAndDebug(dataWith1.getOPTG().getSorties());
      assertSortieTrueAndDebug(dataWith1.getOPTI().getSorties());
    } catch (final MalformedURLException malformedURLException) {
      Assert.fail(malformedURLException.getMessage());
    }
  }

  @Test
  public void testDcWithSorti1TransformeEn0() {
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueData data = readModeleCrue9(analyzer, FilesForTest.MODELE3_1_DC_ISORTI_1);
    Assert.assertFalse(analyzer.containsSevereError());
    data.getOPTG().getSorties().getTrace().setVerbositeEcran(SeveriteManager.INFO);
    data.getOPTG().getSorties().getTrace().setVerbositeFichier(SeveriteManager.INFO);
    data.getOPTI().getSorties().getTrace().setVerbositeEcran(SeveriteManager.INFO);
    data.getOPTI().getSorties().getTrace().setVerbositeFichier(SeveriteManager.INFO);
    data.getOPTR().getSorties().getTrace().setVerbositeEcran(SeveriteManager.INFO);
    data.getOPTR().getSorties().getTrace().setVerbositeFichier(SeveriteManager.INFO);
    final File f = createTempFile();
    writeModeleCrue9(analyzer, f, data);
    try {
      final CrueData dataWith0 = readModeleCrue9(analyzer, f.toURI().toURL());
      Assert.assertFalse(analyzer.containsSevereError());
      assertSortieTrueAndInfo(dataWith0.getOPTR().getSorties());
      assertSortieTrueAndInfo(dataWith0.getOPTG().getSorties());
      assertSortieTrueAndInfo(dataWith0.getOPTI().getSorties());
    } catch (final MalformedURLException malformedURLException) {
      Assert.fail(malformedURLException.getMessage());
    }
  }

  @Test
  public void testDcWithSorti1() {
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueData data = readModeleCrue9(analyzer, FilesForTest.MODELE3_1_DC_ISORTI_1);
    Assert.assertFalse(analyzer.containsSevereError());
    assertSortieTrueAndDebug(data.getOPTR().getSorties());
    assertSortieTrueAndDebug(data.getOPTG().getSorties());
    assertSortieTrueAndDebug(data.getOPTI().getSorties());
  }

  /**
   * Lit en crue 9 et écrit en crue 10.
   */
  private void testLectureFichierCrue9etEcritureCrue10(final String path) {
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);

    final CrueData data = readModeleCrue9(analyzer, path);
    Assert.assertNotNull(data);

    final File fichierEcritureDRRSO = createTempFile();
    // -- ecriture DRSO --//
    final TestCoeurConfig lastVersion = TestCoeurConfig.INSTANCE;
    Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DRSO, lastVersion).write(data, fichierEcritureDRRSO, analyzer);

    // -- ecriture DCSP --//
    final File fichierEcritureDCSP = createTempFile();
    Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DCSP, lastVersion).write(data, fichierEcritureDCSP, analyzer);

    // -- ecriture des DFRT--//
    final File fichierEcritureDFRT = createTempFile();
    Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DFRT, lastVersion).write(data, fichierEcritureDFRT, analyzer);

    // -- ecriture des DPTG --//
    final File fichierEcritureDPTG = createTempFile();
    Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DPTG, lastVersion).write(data, fichierEcritureDPTG, analyzer);

    // -- ecriture des DPTI --//
    final File fichierEcritureDPTI = createTempFile();
    Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DPTI, lastVersion).write(data, fichierEcritureDPTI, analyzer);
  }

  /**
   * lit un fichier fabriqué avec quelques objets branches.
   *
   * @return cruedata.
   */
  @SuppressWarnings("Duplicates")
  private CrueData testLectureFichierDC(final String path) {
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueData data = readModeleCrue9(analyzer, path);
    testAnalyser(analyzer);
    testData(data);
    return data;
  }

  private CrueData testLectureFichierDC(final URL url) {
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueData data = readModeleCrue9(analyzer, url);
    testAnalyser(analyzer);
    testData(data);
    return data;
  }

  /**
   * Test de lecture du modele 3-1 complet.
   */
  @Test
  public void testLectureModele3_1() {
    testLectureFichierDC(FilesForTest.MODELE3_1_DC);
  }

  /**
   * Test de lecture du modele 3-2 complet.
   */
  @Test
  public void testLectureModele3_2_Regle() {
    CrueData crueData = testLectureFichierDC(FilesForTest.MODELE3_2_DC_REGLE);
    OrdPrtGeoModeleBase ordPrtGeoModeleBase = crueData.getModele().getOrdPrtGeoModeleBase();
    List<Regle> regles = ordPrtGeoModeleBase.getRegles();
    Assert.assertEquals(8, regles.size());
    for (final Regle regle : regles) {
      if (!regle.getType().equals(EnumRegle.REB_DEB)) {
        Assert.assertFalse(regle.getType().toString(), regle.isActive());
      }
    }
    final File f = createTemptxtFile("M3-2_c9");
    final CtuluLog log = new CtuluLog();
    writeModeleCrue9(log, f, crueData);
    try {
      crueData = testLectureFichierDC(f.toURI().toURL());
      ordPrtGeoModeleBase = crueData.getModele().getOrdPrtGeoModeleBase();
      regles = ordPrtGeoModeleBase.getRegles();
      Assert.assertEquals(8, regles.size());
      for (final Regle regle : regles) {
        if (!regle.getType().equals(EnumRegle.REB_DEB)) {
          Assert.assertFalse(regle.getType().toString(), regle.isActive());
        }
      }
    } catch (final MalformedURLException e) {
      Assert.fail(e.getMessage());
    }
  }

  /**
   * Test de lecture du modele 3-2 complet.
   */
  @Test
  public void testLectureModele3_2() {
    CrueData crueData = testLectureFichierDC(FilesForTest.MODELE3_2_DC);
    OrdPrtGeoModeleBase ordPrtGeoModeleBase = crueData.getModele().getOrdPrtGeoModeleBase();
    List<Regle> regles = ordPrtGeoModeleBase.getRegles();
    testRegleModele3_2(regles);
    final File f = createTemptxtFile("modele3_2");
    final CtuluLog log = new CtuluLog();
    writeModeleCrue9(log, f, crueData);
    try {
      crueData = testLectureFichierDC(f.toURI().toURL());
      ordPrtGeoModeleBase = crueData.getModele().getOrdPrtGeoModeleBase();
      regles = ordPrtGeoModeleBase.getRegles();
      testRegleModele3_2(regles);
    } catch (final MalformedURLException e) {
      Assert.fail(e.getMessage());
    }
  }

  private void testRegleModele3_2(final List<Regle> regles) {
    Assert.assertEquals(8, regles.size());

    for (final Regle regle : regles) {
      if (EnumRegle.PROF_PLAT.equals(regle.getType())) {
        Assert.assertTrue(regle.getNom() + " must be active", regle.isActive());
        Assert.assertEquals(0.01 / 100d, regle.getSeuilDetect(), 1E-3);
      } else if (EnumRegle.LARG_SEUIL.equals(regle.getType())) {
        Assert.assertTrue(regle.getNom() + " must be active", regle.isActive());
        Assert.assertEquals(100, regle.getSeuilDetect(), 1E-3);
      } else if (EnumRegle.DECAL.equals(regle.getType())) {
        Assert.assertTrue(regle.getNom() + " must be active", regle.isActive());
        Assert.assertEquals(0.2, regle.getSeuilDetect(), 1E-3);
      }
    }
  }

  /**
   * Test de lecture du modele 4 complet.
   */
  @Test
  public void testLectureModele4() {
    testLectureFichierDC(FilesForTest.MODELE3_DC);
  }

  /**
   * Test de lecture du modele 5 complet.
   */
  @Test
  public void testLectureModele5() {
    testLectureFichierDC(FilesForTest.MODELE5_DC);
  }

  /**
   * Test de lecture du modele 6 complet.
   */
  @Test
  public void testLectureModele6() {
    testLectureFichierDC(FilesForTest.MODELE6_DC);
  }

  /**
   * Test de lecture du modele 6 complet.
   */
  @Test
  public void testCarteBorda() {
    final String file = "/crue9/M6-0_c9Borda.dc";
    // on verifie les valeurs par defaut pour les DonCalcSansPrtBrancheSeuilTransversal et
    // DonCalcSansPrtBrancheSeuilLateral
    final CrueData data = testLectureFichierDC(file);
    CatEMHBranche findBrancheByReference = data.findBrancheByReference("Br_B3");
    final DonCalcSansPrtBrancheSeuilTransversal seuilTransversal = EMHHelper.selectFirstOfClass(
        findBrancheByReference.getInfosEMH(), DonCalcSansPrtBrancheSeuilTransversal.class);
    Assert.assertEquals(EnumFormulePdc.DIVERGENT, seuilTransversal.getFormulePdc());

    findBrancheByReference = data.findBrancheByReference("Br_B5");
    final DonCalcSansPrtBrancheSeuilLateral seuilLateral = EMHHelper.selectFirstOfClass(findBrancheByReference.getInfosEMH(),
        DonCalcSansPrtBrancheSeuilLateral.class);
    Assert.assertEquals(EnumFormulePdc.DIVERGENT, seuilLateral.getFormulePdc());
    final File fileTemp = createTempFile();
    // on teste l'écriture
    final CtuluLog log = new CtuluLog();
    writeModeleCrue9(log, fileTemp, data);
    testAnalyser(log);
    try {
      final String content = CtuluLibFile.litFichierTexte(fileTemp);
      Assert.assertTrue(content.contains("BORDA"));
    } catch (final IOException e) {
      Assert.fail(e.getMessage());
    }
  }

  /**
   * Test de lecture du modele 6 complet.
   */
  @Test
  public void testCarteBordaAvecErreur() {
    String file = "/crue9/M6-0_c9BordaErreurs.dc";
    // on verifie les valeurs par defaut pour les DonCalcSansPrtBrancheSeuilTransversal et
    // DonCalcSansPrtBrancheSeuilLateral
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    readModeleCrue9(analyzer, file);

    Assert.assertEquals(2, analyzer.getNbOccurence(CtuluLogLevel.SEVERE));
    final Iterator<CtuluLogRecord> iterator = analyzer.getRecords().iterator();
    Assert.assertEquals("io.convert.borda.error", iterator.next().getMsg());
    Assert.assertEquals("io.convert.borda.error", iterator.next().getMsg());

    // sans erreurs:
    file = "/crue9/M6-0_c9.dc";
    analyzer.clear();
    readModeleCrue9(analyzer, file);
    Assert.assertFalse(analyzer.containsErrorOrSevereError());
  }

  /**
   * Test de lecture du modele 6 complet.
   */
  @Test
  public void testCarteClapetAvecErreur() {
    final String file = "/crue9/M6-0_c9ClapetErreur.dc";
    // on verifie les valeurs par defaut pour les DonCalcSansPrtBrancheSeuilTransversal et
    // DonCalcSansPrtBrancheSeuilLateral
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    readModeleCrue9(analyzer, file);
    Assert.assertEquals(1, analyzer.getNbOccurence(CtuluLogLevel.SEVERE));
    final Iterator<CtuluLogRecord> iterator = analyzer.getRecords().iterator();
    Assert.assertEquals("io.convert.sensOuv.error", iterator.next().getMsg());
  }
}
