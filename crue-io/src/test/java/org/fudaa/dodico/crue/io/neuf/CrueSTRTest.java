package org.fudaa.dodico.crue.io.neuf;

import files.FilesForTest;
import gnu.trove.TObjectDoubleHashMap;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.AbstractIOBinaryTestParent;
import org.fudaa.dodico.crue.io.Crue9FileFormatFactory;
import org.fudaa.dodico.crue.io.ReadHelperForTestsBuilder;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;
import org.fudaa.dodico.crue.metier.emh.ResPrtGeo;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Map;

/**
 * @author cde
 */
public class CrueSTRTest extends AbstractIOBinaryTestParent {
  @Test
  public void testLectureSTRModele30() {
    final TestRes testRes = testLectureFichierSTR(FilesForTest.STR_MODELE30_BRANCHE6, FilesForTest.STO_MODELE30_BRANCHE6,
        "/Etu30Branche6/M3-0_c9.dc", "/Etu30Branche6/M3-0_c9.dh");
    final ResPrtReseauCrue9Adapter res = testRes.res;
    final int nbProf = 25;
    Assert.assertEquals(25, res.getNbResOnProfil());
    checkProfiles(res, nbProf);
    final int nbCasier = 2;
    Assert.assertEquals(nbCasier, res.getNbResOnCasier());
    for (int i = 0; i < nbCasier; i++) {
      Assert.assertFalse(StringUtils.isBlank(res.getCasierNom(i)));
    }
    // on remplit les données pour les sections appartenant a des branches de type 6:
    final Map<String, float[]> zDactForBranche6 = STRFactory.getLoiZDactForBranche6((STOSequentialReader) testRes.data.getSto(), testRes.data);
    testZDactForSectionPROFSTR2(zDactForBranche6.get("ST_PROFSTR2"));
    testZDactForSectionPROFSTR1(zDactForBranche6.get("ST_PROFSTR1"));

    final TObjectDoubleHashMap coefSinuoBySection = STRFactory.getCoefSinuoBySection((STOSequentialReader) testRes.data.getSto(), testRes.data);
    Assert.assertEquals(1d, coefSinuoBySection.get("ST_PROFSTR1"), 1e-6);
    Assert.assertEquals(1d, coefSinuoBySection.get("ST_PROFSTR2"), 1e-6);

    final ResPrtGeo resultatOnProfil = res.getResultatOnProfil("ST_B1_00050");
    Assert.assertNotNull(resultatOnProfil);
    final LoiFF loi = resultatOnProfil.getLoi("loiZBeta");
    final List<PtEvolutionFF> loiZBeta = loi.getEvolutionFF().getPtEvolutionFF();
    Assert.assertEquals(51, loiZBeta.size());
    assertDoubleEquals(1.05, loiZBeta.get(0).getAbscisse());
    assertDoubleEquals(1, loiZBeta.get(0).getOrdonnee());
    assertDoubleEquals(6.05, loiZBeta.get(50).getAbscisse());
    assertDoubleEquals(1.2816799879074097, loiZBeta.get(50).getOrdonnee());
//test pour CRUE-404
  }

  private void checkProfiles(final ResPrtReseauCrue9Adapter res, final int nbProf) {
    for (int i = 0; i < nbProf; i++) {
      final String profilNom = res.getProfilNom(i);
      Assert.assertFalse(StringUtils.isBlank(profilNom));
      Assert.assertNotNull(res.getResultatOnProfil(profilNom));
      if ("ST_PROF3AM".equals(profilNom)) {
        testStProf3AM(res.getResultatOnProfil(profilNom));
      }
      if ("ST_PROFSTR1".equals(profilNom)) {
        testStProfTableauxLength(res.getResultatOnProfil(profilNom), 1);
        testStProfStr1(res.getResultatOnProfil(profilNom));
      }
    }
  }

  //Tests Fortran non standard
  @Test
  public void testLectureSTRModele30_LINUX() {
    final TestRes testRes = testLectureFichierSTR(FilesForTest.STR_MODELE30_BRANCHE6_LINUX, FilesForTest.STO_MODELE30_BRANCHE6_LINUX,
        "/Etu30Branche6/M3-0_c9.dc", "/Etu30Branche6/M3-0_c9.dh");
    final ResPrtReseauCrue9Adapter res = testRes.res;
    final int nbProf = 25;
    Assert.assertEquals(25, res.getNbResOnProfil());
    checkProfiles(res, nbProf);
    final int nbCasier = 2;
    Assert.assertEquals(nbCasier, res.getNbResOnCasier());
    for (int i = 0; i < nbCasier; i++) {
      Assert.assertFalse(StringUtils.isBlank(res.getCasierNom(i)));
    }
    // on remplit les données pour les sections appartenant a des branches de type 6:
    final Map<String, float[]> zDactForBranche6 = STRFactory.getLoiZDactForBranche6((STOSequentialReader) testRes.data.getSto(), testRes.data);
    testZDactForSectionPROFSTR2_LINUX(zDactForBranche6.get("ST_PROFSTR2"));
    testZDactForSectionPROFSTR1_LINUX(zDactForBranche6.get("ST_PROFSTR1"));

    final ResPrtGeo resultatOnProfil = res.getResultatOnProfil("ST_B1_00050");
    Assert.assertNotNull(resultatOnProfil);
    final LoiFF loi = resultatOnProfil.getLoi("loiZBeta");
    final List<PtEvolutionFF> loiZBeta = loi.getEvolutionFF().getPtEvolutionFF();
    Assert.assertEquals(51, loiZBeta.size());
    Assert.assertEquals(1.05, loiZBeta.get(0).getAbscisse(), 1e-5);//bizarre on trouve 1.0499999
    assertDoubleEquals(1, loiZBeta.get(0).getOrdonnee());
    assertDoubleEquals(6.05, loiZBeta.get(50).getAbscisse());
    assertDoubleEquals(1.2816799879074097, loiZBeta.get(50).getOrdonnee());
  }

  private void testStProfStr1(final ResPrtGeo resultat) {
    final LoiFF loi = resultat.getLoi("lstLitTypeLit");
    Assert.assertNotNull(loi);
    Assert.assertEquals(1, loi.getEvolutionFF().getPtEvolutionFF().size());
    Assert.assertEquals(1, (int) loi.getEvolutionFF().getPtEvolutionFF().get(0).getOrdonnee());
    assertDoubleEquals(30, resultat.getLoi("lstLitKsup").getOrdonnee(0));
    assertDoubleEquals(0.8, resultat.getLoi("lstLitZf").getOrdonnee(0));
    assertDoubleEquals(1.0, resultat.getValue("numLitZf"));
  }

  @Test
  public void testLectureSTRModele50() {
    final TestRes testRes = testLectureFichierSTR(FilesForTest.STR_MODELE50, FilesForTest.STO_MODELE50, FilesForTest.DC_MODELE50, FilesForTest.DH_MODELE50);
    final ResPrtReseauCrue9Adapter res = testRes.res;
    final ResPrtGeo resultatOnProfil = res.getResultatOnProfil("ST_PROFAV");
    testStProfTableauxLength(resultatOnProfil, 5);
  }

  @Test
  public void testLectureSTRModele50_LINUX() {
    final TestRes testRes = testLectureFichierSTR(FilesForTest.STR_MODELE50_LINUX, FilesForTest.STO_MODELE50_LINUX, FilesForTest.DC_MODELE50, FilesForTest.DH_MODELE50);
    final ResPrtReseauCrue9Adapter res = testRes.res;
    final ResPrtGeo resultatOnProfil = res.getResultatOnProfil("ST_PROFAV");
    testStProfTableauxLength(resultatOnProfil, 5);
  }

  private static class TestRes {
    CrueData data;
    ResPrtReseauCrue9Adapter res;
  }

  /**
   * Lit un fichier STR (après avoir lu DC, DH, STO)
   *
   * @return List.
   */
  private TestRes testLectureFichierSTR(final String pathStr, final String pathSto, final String dc, final String dh) {
    final TestRes res = new TestRes();

    // Lecture DH (qui inclut la lecture de DC)
    final CtuluLog analyze = new CtuluLog();
    final CrueData data = ReadHelperForTestsBuilder.get().readModele(analyze, dc, dh).getMetier();
    res.data = data;

    // Lecture STO
    final CrueIOResu<STOSequentialReader> readSTO = readBinaire(Crue9FileFormatFactory.getSTOFileFormat(),
        pathSto, null);

    final STOSequentialReader stoSeq = readSTO.getMetier();
    data.setSto(stoSeq);

    // Lecture STR
    final CrueIOResu<STRSequentialReader> read = readBinaire(Crue9FileFormatFactory.getSTRFileFormat(), pathStr,
        data);

    Assert.assertNotNull(read);

    // Alimentation métier par profil
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    testAnalyser(analyzer);
    final STRSequentialReader strSeq = read.getMetier();
    res.res = STOReader.alimenteObjetsMetier(stoSeq);
    res.res.setReader(strSeq);
    return res;
  }

  private void testStProfTableauxLength(final ResPrtGeo resultat, final int taille) {
    Assert.assertEquals(taille, resultat.getLoi("lstLitKsup").getSize());
    Assert.assertEquals(taille, resultat.getLoi("lstLitLinf").getSize());
    Assert.assertEquals(taille, resultat.getLoi("lstLitLsup").getSize());
    Assert.assertEquals(taille, resultat.getLoi("lstLitPsup").getSize());
    Assert.assertEquals(taille, resultat.getLoi("lstLitSsup").getSize());
    LoiFF loi = resultat.getLoi("lstLitZf");
    Assert.assertEquals(taille, loi.getSize());
    loi = resultat.getLoi("lstLitTypeLit");
    Assert.assertEquals(taille, loi.getSize());
  }

  private void testStProf3AM(final ResPrtGeo resultat) {
    // LstLitLinf
    LoiFF loi = resultat.getLoi("lstLitLinf");
    assertDoubleEquals(0, loi.getOrdonnee(0));
    assertDoubleEquals(0, loi.getOrdonnee(1));
    assertDoubleEquals(20.399994, loi.getOrdonnee(2));
    assertDoubleEquals(0, loi.getOrdonnee(3));
    assertDoubleEquals(0, loi.getOrdonnee(4));
    // LstLitSsup
    loi = resultat.getLoi("lstLitSsup");
    assertDoubleEquals(0, loi.getOrdonnee(0));
    Assert.assertEquals(75.00001, loi.getOrdonnee(1), 1e-4);
    assertDoubleEquals(132.0, loi.getOrdonnee(2));
    Assert.assertEquals(75.00001, loi.getOrdonnee(3), 1e-4);
    assertDoubleEquals(0, loi.getOrdonnee(4));
    // LstLitPsup
    loi = resultat.getLoi("lstLitPsup");
    assertDoubleEquals(0, loi.getOrdonnee(0));
    assertDoubleEquals(35.016663, loi.getOrdonnee(1));
    assertDoubleEquals(34.944275, loi.getOrdonnee(2));
    assertDoubleEquals(35.016663, loi.getOrdonnee(3));
    assertDoubleEquals(0, loi.getOrdonnee(4));
    // LstLitKsup
    loi = resultat.getLoi("lstLitKsup");
    assertDoubleEquals(0, loi.getOrdonnee(0));
    assertDoubleEquals(15, loi.getOrdonnee(1));
    assertDoubleEquals(30, loi.getOrdonnee(2));
    assertDoubleEquals(15, loi.getOrdonnee(3));
    assertDoubleEquals(0, loi.getOrdonnee(4));
    // LstLitLsup
    loi = resultat.getLoi("lstLitLsup");
    assertDoubleEquals(32, loi.getOrdonnee(0));
    assertDoubleEquals(30, loi.getOrdonnee(1));
    assertDoubleEquals(28, loi.getOrdonnee(2));
    assertDoubleEquals(30, loi.getOrdonnee(3));
    assertDoubleEquals(32, loi.getOrdonnee(4));
    // LstLitZf
    loi = resultat.getLoi("lstLitZf");
    assertDoubleEquals(3.3, loi.getOrdonnee(0));
    assertDoubleEquals(2.3, loi.getOrdonnee(1));
    assertDoubleEquals(0.3, loi.getOrdonnee(2));
    assertDoubleEquals(2.3, loi.getOrdonnee(3));
    assertDoubleEquals(3.3, loi.getOrdonnee(4));

    loi = resultat.getLoi("lstLitTypeLit");
    Assert.assertEquals(2, (int) loi.getOrdonnee(0));
    Assert.assertEquals(1, (int) loi.getOrdonnee(1));
    Assert.assertEquals(0, (int) loi.getOrdonnee(2));
    Assert.assertEquals(1, (int) loi.getOrdonnee(3));
    Assert.assertEquals(2, (int) loi.getOrdonnee(4));

    loi = resultat.getLoi("loiZLsto");
    Assert.assertEquals(51, loi.getSize());//ajout d'une valeur à 0
    assertDoubleEquals(0.3, loi.getAbscisse(0));
    assertDoubleEquals(0, loi.getOrdonnee(0));
    assertDoubleEquals(5.3, loi.getAbscisse(50));
    assertDoubleEquals(64, loi.getOrdonnee(50));

    loi = resultat.getLoi("lstLitPosBordure");
    Assert.assertEquals(5, loi.getSize());
    assertDoubleEquals(3, loi.getOrdonnee(0));
    assertDoubleEquals(1, loi.getOrdonnee(1));
    assertDoubleEquals(0, loi.getOrdonnee(2));
    assertDoubleEquals(2, loi.getOrdonnee(3));
    assertDoubleEquals(3, loi.getOrdonnee(4));

    loi = resultat.getLoi("loiZLcont");
    Assert.assertEquals(51, loi.getSize());//ajout d'une valeur à 0
    assertDoubleEquals(0.3, loi.getAbscisse(0));
    assertDoubleEquals(0, loi.getOrdonnee(0));
    assertDoubleEquals(5.3, loi.getAbscisse(50));
    assertDoubleEquals(152, loi.getOrdonnee(50));

    loi = resultat.getLoi("loiZLtot");
    Assert.assertEquals(51, loi.getSize());//ajout d'une valeur à 0
    assertDoubleEquals(0.3, loi.getAbscisse(0));
    assertDoubleEquals(0, loi.getOrdonnee(0));
    assertDoubleEquals(5.3, loi.getAbscisse(50));
    assertDoubleEquals(152, loi.getOrdonnee(50));
  }

  private void testZDactForSectionPROFSTR2(final float[] values) {
    Assert.assertEquals(4.833968, values[0], 1e-4);
    Assert.assertEquals(15.333833, values[1], 1e-4);
    Assert.assertEquals(3048.2847, values[values.length - 2], 1e-4);
    Assert.assertEquals(3150.1428, values[values.length - 1], 1e-4);
  }

  private void testZDactForSectionPROFSTR2_LINUX(final float[] values) {
    Assert.assertEquals(4.833968, values[0], 1e-4);
    Assert.assertEquals(15.333833, values[1], 1e-4);
    Assert.assertEquals(3048.2847, values[values.length - 2], 1e-4);
    Assert.assertEquals(3150.1421, values[values.length - 1], 1e-4);
  }

  private void testZDactForSectionPROFSTR1(final float[] values) {
    Assert.assertEquals(2.9933994, values[0], 1e-4);
    Assert.assertEquals(9.497381, values[1], 1e-4);
    Assert.assertEquals(1906.0226, values[values.length - 2], 1e-4);
    Assert.assertEquals(1970.089, values[values.length - 1], 1e-4);
  }

  private void testZDactForSectionPROFSTR1_LINUX(final float[] values) {
    Assert.assertEquals(2.9933994, values[0], 1e-4);
    Assert.assertEquals(9.497381, values[1], 1e-4);
    Assert.assertEquals(1906.0227, values[values.length - 2], 1e-4);
    Assert.assertEquals(1970.0892, values[values.length - 1], 1e-4);
  }
}
