/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.io.rcal;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.io.rcal.CrueDaoStructureRCAL.ResCalcTransDao;
import org.fudaa.dodico.crue.io.rdao.ParametrageDao;
import org.fudaa.dodico.crue.io.res.RCalTimeStepBuilder;
import org.fudaa.dodico.crue.io.res.ResultatEntry;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class RCalTimeStepBuilderTest {

  private final String cc_T1 = "Cc_T1";
  private final String file1 = "file1";
  private final int offset1 = 800;
  private final String pdt1Value = "P0DT0H15M0S";
  private final String pdt1ValueBis = "P0DT0H30M0S";
  private final String cc_T2 = "Cc_T2";
  private final String file2 = "file2";
  private final int offset2 = 100;
  private final String pdt2Value = "P0DT0H45M0S";

  public RCalTimeStepBuilderTest() {
  }

  @Test
  public void testExtract() {
    CrueDaoRCAL in = new CrueDaoRCAL();
    in.Parametrage = new ParametrageDao();
    in.Parametrage.setNbrOctetMot(8);
    in.ResCalcTranss = new ArrayList<>();
    in.ResCalcTranss.add(createCCT1());
    in.ResCalcTranss.add(createCCT2());
    RCalTimeStepBuilder builder = new RCalTimeStepBuilder();
    File home = new File(".");
    Map<ResultatTimeKey, ResultatEntry> extract = builder.extract(in, home).getEntries();
    assertEquals(3, extract.size());
    List<ResultatTimeKey> keys = new ArrayList<>(extract.keySet());
    Collections.sort(keys);
    ResultatEntry entry = extract.get(keys.get(0));
    assertEquals(cc_T1, entry.getResultatKey().getNomCalcul());
    assertEquals(new File(home, file1), entry.getBinFile());
    assertEquals(8 * offset1, entry.getOffsetInBytes());
    assertEquals(15 * 60 * 1000L, entry.getResultatKey().getDuree());
    entry = extract.get(keys.get(1));
    assertEquals(cc_T1, entry.getResultatKey().getNomCalcul());
    assertEquals(new File(home, file1), entry.getBinFile());
    assertEquals(8 * offset1, entry.getOffsetInBytes());
    assertEquals(30 * 60 * 1000L, entry.getResultatKey().getDuree());
    entry = extract.get(keys.get(2));
    assertEquals(cc_T2, entry.getResultatKey().getNomCalcul());
    assertEquals(new File(home, file2), entry.getBinFile());
    assertEquals(8 * offset2, entry.getOffsetInBytes());
    assertEquals(45 * 60 * 1000L, entry.getResultatKey().getDuree());






  }

  private ResCalcTransDao createCCT1() {
    CrueDaoStructureRCAL.ResCalcTransDao cct1 = new CrueDaoStructureRCAL.ResCalcTransDao();
    cct1.setNomRef(cc_T1);
    cct1.ResPdts = new ArrayList<>();
    CrueDaoStructureRCAL.ResPdtDao pdt1 = new CrueDaoStructureRCAL.ResPdtDao();
    pdt1.setHref(file1);
    pdt1.setOffsetMot(offset1);
    pdt1.setTempsSimu(pdt1Value);
    cct1.ResPdts.add(pdt1);
    pdt1 = new CrueDaoStructureRCAL.ResPdtDao();
    pdt1.setHref(file1);
    pdt1.setOffsetMot(offset1);
    pdt1.setTempsSimu(pdt1ValueBis);
    cct1.ResPdts.add(pdt1);
    return cct1;
  }

  private ResCalcTransDao createCCT2() {
    CrueDaoStructureRCAL.ResCalcTransDao cct1 = new CrueDaoStructureRCAL.ResCalcTransDao();
    cct1.setNomRef(cc_T2);
    cct1.ResPdts = new ArrayList<>();
    CrueDaoStructureRCAL.ResPdtDao pdt1 = new CrueDaoStructureRCAL.ResPdtDao();
    pdt1.setHref(file2);
    pdt1.setOffsetMot(offset2);
    pdt1.setTempsSimu(pdt2Value);
    cct1.ResPdts.add(pdt1);
    return cct1;
  }
}
