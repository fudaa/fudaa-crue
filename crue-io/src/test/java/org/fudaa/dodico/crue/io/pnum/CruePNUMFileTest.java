package org.fudaa.dodico.crue.io.pnum;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.ParamNumCalcPseudoPerm;
import org.fudaa.dodico.crue.metier.emh.ParamNumCalcTrans;
import org.fudaa.dodico.crue.metier.emh.ParamNumModeleBase;
import org.fudaa.dodico.crue.metier.emh.PdtCst;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * Classe de tests JUnit pour PNUM (Paramètres NUMériques) ; Permet de contrôler le bon fonctionnement de lecture d'un fichier XML existant,
 * l'écriture de façon similaire de ce même fichier et valide par XSD le fichier XML initial et celui produit
 *
 * @author CDE
 */
public class CruePNUMFileTest extends AbstractIOParentTest {
  /**
   * Constructeur
   */
  public CruePNUMFileTest() {
    super(Crue10FileFormatFactory.getVersion(CrueFileType.PNUM, TestCoeurConfig.INSTANCE), FilesForTest.PNUM_V1_2);
  }

  @Test
  public void testValideVersion1p1p1() {
    testValid(FilesForTest.PNUM_V1_1_1, TestCoeurConfig.INSTANCE_1_1_1);
  }

  @Test
  public void testValideVersionLast() {
    testValid(FilesForTest.PNUM_V1_2, TestCoeurConfig.INSTANCE);
  }

  private void testValid(final String file, final CoeurConfigContrat version) {
    final CtuluLog log = new CtuluLog();
    final boolean valide = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.PNUM, version).isValide(file, log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    Assert.assertTrue(valide);
  }

  /**
   * Lit le fichier XML et verifie certaines valeurs des différents paramètres numériques
   */
  @Test
  public void testLecture() {
    testLecture(FilesForTest.PNUM_V1_2, TestCoeurConfig.INSTANCE);
  }

  @Test
  public void testLecture_v1p1p1() {
    testLecture(FilesForTest.PNUM_V1_1_1, TestCoeurConfig.INSTANCE_1_1_1);
  }

  /**
   * Lit le fichier XML et verifie certaines valeurs des différents paramètres numériques
   */
  public void testLecture(final String file, final CoeurConfigContrat version) {

    final CtuluLog analyser = new CtuluLog();
    // -- lecture de PNUM --//
    final ParamNumModeleBase jeuDonnees = (ParamNumModeleBase) Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.PNUM, version)
        .read(file, analyser, createDefault(version)).getMetier();
    testAnalyser(analyser);
    verifieDonnees(jeuDonnees);
  }

  @Test
  public void testEcriture() {
    testEcriture(FilesForTest.PNUM_V1_2, TestCoeurConfig.INSTANCE);
  }

  @Test
  public void testEcriture_v1p1p1() {
    testEcriture(FilesForTest.PNUM_V1_1_1, TestCoeurConfig.INSTANCE_1_1_1);
  }

  /**
   * Lecture du fichier XML puis écriture dans un fichier temporaire
   */
  public void testEcriture(final String file, final CoeurConfigContrat version) {

    final CtuluLog analyser = new CtuluLog();

    // On lit
    ParamNumModeleBase data = (ParamNumModeleBase) Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.PNUM, version)
        .read(file, analyser, createDefault(version)).getMetier();
    final File f = createTempFile();
    // On ecrit
    final CtuluLog analyzer = new CtuluLog();
    Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.PNUM, version)
        .writeMetierDirect(data, f, analyzer, CrueConfigMetierForTest.DEFAULT);
    testAnalyser(analyzer);
    data = (ParamNumModeleBase) Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.PNUM, version).read(f, analyzer, createDefault(version))
        .getMetier();
    testAnalyser(analyzer);
    final boolean res = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.PNUM, version).isValide(f, analyzer);
    testAnalyser(analyzer);
    Assert.assertTrue(res);
    verifieDonnees(data);
  }

  /**
   * @param metier a verifier
   */
  private static void verifieDonnees(final ParamNumModeleBase metier) {

    Assert.assertNotNull(metier);
    final ParamNumCalcPseudoPerm pseudo = metier.getParamNumCalcPseudoPerm();
    final ParamNumCalcTrans trans = metier.getParamNumCalcTrans();

    assertDoubleEquals(0.0, metier.getZref());
    assertDoubleEquals(1.0E30, metier.getFrLinInf());
    assertDoubleEquals(1.0, metier.getFrLinSup());

    assertDoubleEquals(0.75, trans.getThetaPreissmann());

    assertDoubleEquals(1.0E+30, pseudo.getCrMaxFlu());
    assertDoubleEquals(1.0E+30, pseudo.getCrMaxTor());
    Assert.assertNotNull(pseudo.getPdt());
    Assert.assertTrue(pseudo.getPdt() instanceof PdtCst);
    Assert.assertEquals("P0Y0M0DT1H0M0S", DateDurationConverter.durationToXsd(((PdtCst) pseudo.getPdt()).getPdtCst()));

    assertDoubleEquals(0.01, pseudo.getTolMaxQ());
    assertDoubleEquals(0.001, pseudo.getTolMaxZ());
    Assert.assertEquals(40, pseudo.getNbrPdtMax());
    Assert.assertEquals(1, pseudo.getNbrPdtDecoup());
    assertDoubleEquals(1.0, pseudo.getCoefRelaxQ());
    assertDoubleEquals(1.0, pseudo.getCoefRelaxZ());

    Assert.assertNotNull(trans.getPdt());
    Assert.assertTrue(trans.getPdt() instanceof PdtCst);
    Assert.assertEquals("P0Y0M0DT0H15M0S", DateDurationConverter.durationToXsd(((PdtCst) trans.getPdt()).getPdtCst()));
  }
}
