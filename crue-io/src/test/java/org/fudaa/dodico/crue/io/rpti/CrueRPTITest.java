package org.fudaa.dodico.crue.io.rpti;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.drso.CrueDRSOFileTest;
import org.fudaa.dodico.crue.io.rpti.CrueConverterRPTI;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collection;
import java.util.List;

/**
 * @author deniger
 */
public class CrueRPTITest extends AbstractIOParentTest {
  public CrueRPTITest() {
    super(Crue10FileFormatFactory.getVersion(CrueFileType.RPTI, TestCoeurConfig.INSTANCE), FilesForTest.RPTI_V1_2);
  }

  @Test
  public void testLectureEmpty() {
    final CrueIOResu<CrueData> ioData = read(FilesForTest.DRSO_V1_2, FilesForTest.RPTI_V1_2_EMPTY, TestCoeurConfig.INSTANCE);
    testAnalyser(ioData.getAnalyse());
    final CrueData metier = ioData.getMetier();
    final ResPrtCIniCasier resPrti = getResPrtCIni(metier.findCasierByReference("Ca_N6"));
    Assert.assertNotNull(resPrti);
  }

  @Test
  public void testLecture() {
    final CrueIOResu<CrueData> ioData = read(FilesForTest.DRSO_V1_2, FilesForTest.RPTI_V1_2, TestCoeurConfig.INSTANCE);
    testAnalyser(ioData.getAnalyse());
    Assert.assertTrue(ioData.getMetier().getScenarioData().getActuallyActive());
    Assert.assertTrue(ioData.getMetier().getModele().getActuallyActive());
    Assert.assertTrue(ioData.getMetier().getSousModele().getActuallyActive());
    final CrueData metier = ioData.getMetier();
    ResPrtCIniCasier resPrti = getResPrtCIni(metier.findCasierByReference("Ca_N6"));
    assertDoubleEquals(CrueConverterRPTI.hexaToDouble("40076872B020C49C"), resPrti.getZini());
    resPrti = getResPrtCIni(metier.findCasierByReference("Ca_N7"));
    //modifié pour tests:
    assertDoubleEquals(CrueConverterRPTI.hexaToDouble("400F6872B020C49C"), resPrti.getZini());

    ResPrtCIniSection resPrtiSection = getResPrtCIni(metier.findSectionByReference("St_B1_00050"));
    assertDoubleEquals(100.0, resPrtiSection.getQini());
    resPrtiSection = getResPrtCIni(metier.findSectionByReference("St_B5_Aval"));
    assertDoubleEquals(0, resPrtiSection.getQini());
    resPrtiSection = getResPrtCIni(metier.findSectionByReference("St_Prof11"));
    assertDoubleEquals(CrueConverterRPTI.hexaToDouble("bff0000000000000"), resPrtiSection.getQini());
    assertDoubleEquals(-1, resPrtiSection.getQini());
    assertDoubleEquals(CrueConverterRPTI.hexaToDouble("400D6872B020C49C"), resPrtiSection.getZini());
    final List<CatEMHSection> sections = metier.getSections();
    for (final EMH section : sections) {
      final CatEMHSection findNoeudByReference = metier.findSectionByReference(section.getNom());
      if (findNoeudByReference.getActuallyActive()) {
        Assert.assertNotNull(section.getNom(), findNoeudByReference);
        if (!"St_Prof11".equals(section.getNom())) {
          final ResPrtCIniSection toTest = getResPrtCIni(findNoeudByReference);
          assertDoubleEquals(CrueConverterRPTI.hexaToDouble("400D0B780346DC5D"), toTest.getZini());
        }
      }
    }
  }

  @Test
  public void testErreur() {
    final CrueIOResu<CrueData> ioData = read(FilesForTest.DRSO_V1_2_ERREUR, FilesForTest.RPTI_V1_2_ERREUR, TestCoeurConfig.INSTANCE);
    Assert.assertTrue(ioData.getAnalyse().containsErrorOrSevereError());
    final Collection<CtuluLogRecord> records = ioData.getAnalyse().getRecords();
    Assert.assertEquals(6, records.size());
    final String[] msg = new String[]{"io.rpti.casierResPrtNotFoundInModel.error", "io.rpti.casierFoundButNotActive.error",
        "io.rpti.activeCasierNotDefinedInRTI.error", "io.rpti.sectionResPrtNotFoundInModel.error", "io.rpti.sectionFoundButNotActive.error",
        "io.rpti.activeSectionNotDefinedInRTI.error"};
    final String[] args = new String[]{"Ca_N7NonTrouve", "Ca_N6NonActive", "Ca_N7", "St_B1_00050NonTrouve", "St_B1_00050NonActive", "St_B1_00050"};
    int idx = 0;
    for (final CtuluLogRecord record : records) {
      Assert.assertEquals(idx + ": " + msg[idx], msg[idx], record.getMsg());
      Assert.assertEquals("test of size for " + idx, 1, record.getArgs().length);
      Assert.assertEquals(idx + ": " + args[idx], args[idx], record.getArgs()[0]);
      idx++;
    }
  }

  private <T extends AbstractResPrtCIni> T getResPrtCIni(final EMH emh) {
    final List<InfosEMH> resPrtis = EMHHelper.collectInfoEMH(emh, EnumInfosEMH.RES_PRT_CINIT);
    Assert.assertEquals(1, resPrtis.size());
    return (T) (resPrtis.get(0));
  }

  public static void updateRelationScenarioModeleSousModele(CrueIOResu<CrueData> ioData) {
    EMHScenario scenario = ioData.getMetier().getScenarioData();
    EMHModeleBase modele = ioData.getMetier().getModele();
    EMHRelationFactory.addRelationContientEMH(scenario, modele);
    EMHRelationFactory.addRelationContientEMH(modele, ioData.getMetier().getSousModele());
  }

  private CrueIOResu<CrueData> read(final String drso, final String rpti, final CoeurConfigContrat version) {
    final CtuluLog log = new CtuluLog();

    CrueIOResu<CrueData> ioData = Crue10FileFormatFactory.<CrueData>getVersion(CrueFileType.DRSO, version).read(drso, log,
        CrueDRSOFileTest.createDefault(version));
    testAnalyser(log);
    updateRelationScenarioModeleSousModele(ioData);
    ioData = Crue10FileFormatFactory.<CrueData>getVersion(CrueFileType.RPTI, version).read(rpti, log, ioData.getMetier());
    return ioData;
  }
}
