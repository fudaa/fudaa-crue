package org.fudaa.dodico.crue.io.common;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.io.common.FileLocker.Data;
import org.joda.time.LocalDateTime;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class FileLockerTest {

  private static class ConnexionInformationImmuable implements ConnexionInformation {

    String user = "Tester";
    final LocalDateTime time = new LocalDateTime();

    @Override
    public LocalDateTime getCurrentDate() {
      return time;
    }

    @Override
    public String getCurrentUser() {
      return user;
    }
  }

  @Test
  public void testLockUnlock() throws IOException {
    File f = File.createTempFile("test", ".txt");
    assertTrue(f.exists());
    final ConnexionInformationImmuable connexionInformationImmuable = new ConnexionInformationImmuable();
    FileLocker fileLocker = new FileLocker();

    assertFalse(fileLocker.isLock(f));
    assertNull(fileLocker.isLockedBy(f));

    assertTrue(fileLocker.lock(f, connexionInformationImmuable));
    assertTrue(fileLocker.isLock(f));
    assertNotNull(fileLocker.isLockedBy(f));

    assertTrue(fileLocker.unlock(f));
    assertFalse(fileLocker.isLock(f));
    assertNull(fileLocker.isLockedBy(f));
    clearFiles(fileLocker, f);
  }

  @Test
  public void testLockData() throws IOException {
    File f = File.createTempFile("test", ".txt");
    assertTrue(f.exists());

    final ConnexionInformationImmuable connexionInformationImmuable = new ConnexionInformationImmuable();
    FileLocker fileLocker = new FileLocker();
    fileLocker.lock(f, connexionInformationImmuable);
    final Data lockedBy = fileLocker.isLockedBy(f);
    assertNotNull(lockedBy);
    isCorresponding(connexionInformationImmuable, lockedBy);
    clearFiles(fileLocker, f);
  }

  private void clearFiles(FileLocker fileLocker, File... files) {
    for (File f : files) {
      fileLocker.unlock(f);
      f.delete();
    }
  }

  private void isCorresponding(final ConnexionInformationImmuable connexionInformationImmuable, final Data lockedBy) {
    assertEquals(connexionInformationImmuable.getCurrentUser(), lockedBy.getUserName());
    assertEquals(FileLocker.getDateFormatted(connexionInformationImmuable.getCurrentDate()), lockedBy.getTime());
  }

  @Test
  public void testReLock() throws IOException {
    File f = File.createTempFile("test", ".txt");
    assertTrue(f.exists());

    final ConnexionInformationImmuable connexionInformationImmuable = new ConnexionInformationImmuable();
    FileLocker fileLocker = new FileLocker();
    fileLocker.lock(f, connexionInformationImmuable);
    Data lockedBy = fileLocker.isLockedBy(f);
    isCorresponding(connexionInformationImmuable, lockedBy);
    connexionInformationImmuable.user = "relocker";
    assertFalse(fileLocker.lock(f, false, connexionInformationImmuable));//pas de relock car déjà fait
    lockedBy = fileLocker.isLockedBy(f);
    assertNotSame(connexionInformationImmuable.getCurrentUser(), lockedBy.getUserName());

    assertFalse(fileLocker.lock(f, connexionInformationImmuable));//pas de relock car déjà fait
    lockedBy = fileLocker.isLockedBy(f);
    assertNotSame(connexionInformationImmuable.getCurrentUser(), lockedBy.getUserName());

    assertTrue(fileLocker.lock(f, true, connexionInformationImmuable));//cette fois on ecrase le lock
    lockedBy = fileLocker.isLockedBy(f);
    isCorresponding(connexionInformationImmuable, lockedBy);
    clearFiles(fileLocker, f);
  }

  @Test
  public void testMultiLock() throws IOException {
    final ConnexionInformationImmuable connexionInformationImmuable = new ConnexionInformationImmuable();
    FileLocker fileLocker = new FileLocker();
    assertNull(fileLocker.getLockedFiles(null));
    File f = File.createTempFile("test", ".txt");
    assertTrue(f.exists());
    File f2 = File.createTempFile("test", ".txt");
    assertTrue(f2.exists());
    File fNotLocked = File.createTempFile("test", ".txt");
    assertTrue(fNotLocked.exists());
    fileLocker.lock(f, connexionInformationImmuable);
    fileLocker.lock(f2, connexionInformationImmuable);
    List<File> lockedFiles = fileLocker.getLockedFiles(Arrays.asList(f, f2, fNotLocked));
    assertEquals(2, lockedFiles.size());
    assertTrue(lockedFiles.contains(f));
    assertTrue(lockedFiles.contains(f2));
    assertFalse(lockedFiles.contains(fNotLocked));

  }
}
