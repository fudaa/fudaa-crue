package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;

import java.io.File;

/**
 * @author deniger
 */
public abstract class AbstractIOBinaryTestParent extends AbstractTestParent {
  /**
   * Lit le fichier et teste que le CtuluAnalyse ne contient pas d'erreurs.
   *
   * @param <T> le type de retour
   * @param fmt le format binaire
   * @param path le chemin du fichier a lire
   * @param crueData
   * @return la donnée lue
   */
  public <T> CrueIOResu<T> readBinaire(final AbstractCrueBinaryFileFormat<T> fmt,
                                       final String path, final CrueData crueData) {
    final File f = AbstractTestParent.getFile(path);
    filesToDelete.add(f);
    Assert.assertNotNull(f);
    final CrueIOResu<T> read = fmt.read(f, crueData);
    AbstractTestParent.testAnalyser(read.getAnalyse());
    return read;
  }
}
