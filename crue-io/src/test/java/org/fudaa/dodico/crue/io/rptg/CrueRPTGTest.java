package org.fudaa.dodico.crue.io.rptg;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.rcal.CrueRCAL_V1_2_Test;
import org.fudaa.dodico.crue.io.rdao.*;
import org.fudaa.dodico.crue.io.rdao.CasiersDao.CasierDao;
import org.fudaa.dodico.crue.io.rdao.CasiersDao.CasierTypeDao;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.VariableResDao;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CrueRPTGTest extends AbstractIOParentTest {
  public CrueRPTGTest() {
    super(Crue10FileFormatFactory.getVersion(CrueFileType.RPTG, TestCoeurConfig.INSTANCE_1_2), FilesForTest.RPTG_V1_2);
  }

  @Test
  public void testLecture() {
    final CrueDaoRPTG donnees = readData(FilesForTest.RPTG_V1_2, TestCoeurConfig.INSTANCE_1_2);

    assertEquals("Commentaire RPTG", donnees.getCommentaire());

    testParametrage(donnees.getParametrage());
    testContexteSimulation(donnees.getContexteSimulation());
    testStructure(donnees.getStructureResultat());
    this.testResultatsPrtGeo(donnees.getResPrtGeos());
  }

  public static CrueDaoRPTG readData(final String fichier, final CoeurConfigContrat version) {
    final CtuluLog analyzer = new CtuluLog();
    final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.RPTG, version);
    final CrueDaoRPTG jeuDonneesLue = (CrueDaoRPTG) fileFormat.read(fichier, analyzer, createDefault(version)).getMetier();

    testAnalyser(analyzer);

    return jeuDonneesLue;
  }

  private void testCasiers(final CasiersDao casiers) {
    assertEquals(407, casiers.getNbrMot());
    CrueRCAL_V1_2_Test.testNomRefExact(casiers.getVariableRes(), "Zhaut", "Zf", "LoiZSplan", "LoiZVol");
    final VariableResDao loiZSplan = casiers.getVariableRes().get(2);
    assertEquals(CommonResDao.VariableResLoiDao.class, loiZSplan.getClass());
    final CasierTypeDao casierProfil = casiers.getCasierProfil();
    assertEquals(406, casierProfil.getNbrMot());
    final List<CasierDao> casierList = casierProfil.getCasier();
    assertEquals(2, casierList.size());
    testCasier(casierList.get(0));
    testCasier(casierList.get(1));
  }

  private void testCasier(final CasierDao casierDao) {
    assertEquals(203, casierDao.getNbrMot());
    assertEquals(2, casierDao.getVariableResLoiNbrPt().size());
    assertEquals("LoiZSplan", casierDao.getVariableResLoiNbrPt().get(0).getNomRef());
    assertEquals(50, casierDao.getVariableResLoiNbrPt().get(0).getNbrPt());
    assertEquals("LoiZVol", casierDao.getVariableResLoiNbrPt().get(1).getNomRef());
    assertEquals(50, casierDao.getVariableResLoiNbrPt().get(1).getNbrPt());
  }

  private void testNoeuds(final NoeudsDao noeuds) {
    assertEquals(1, noeuds.getNbrMot());
  }

  private void testResultatsPrtGeo(final List<ResPrtGeoDao> resPrtGeos) {
    assertEquals(1, resPrtGeos.size());
    final ResPrtGeoDao res = resPrtGeos.get(0);
    assertEquals("M3-0_c10.rptg.bin", res.getHref());
    assertEquals(0, res.getOffsetMot());
  }

  private void testParametrage(final ParametrageDao parametrage) {

    assertEquals(8, parametrage.getNbrOctetMot());
    assertEquals(5, parametrage.getDelimiteur().size());
    assertEquals("CatEMHBranche", parametrage.getDelimiteur().get(4).getNom());
    assertEquals(" Branche", parametrage.getDelimiteur().get(4).getChaine());
  }

  private void testContexteSimulation(final ContexteSimulationDao contexteSimulation) {
    assertEquals("2012-02-17T15:19:26.00000", contexteSimulation.getDateSimulation());
    assertEquals("10.0.0", contexteSimulation.getVersionCrue());
    assertEquals("EtuEx.etu.xml", contexteSimulation.getEtude());
    assertEquals("Sc_M3-0_c10", contexteSimulation.getScenario().getNomRef());
    assertEquals("R2012-03-01-15h29m00s", contexteSimulation.getRun().getNomRef());
    assertEquals("Mo_M3-0_c10", contexteSimulation.getModele().getNomRef());
  }

  private void testStructure(final StructureResultatsDao structureResultat) {
    assertEquals(52064, structureResultat.getNbrMot());
    testNoeuds(structureResultat.getNoeuds());
    testCasiers(structureResultat.getCasiers());
    testSections(structureResultat.getSections());
  }

  private void testSections(final SectionsDao sections) {
    assertEquals(51653, sections.getNbrMot());
    final List<VariableResDao> variableRes = sections.getSectionIdem().getVariableRes();
    assertEquals(33, variableRes.size());
    final CommonResDao.VariableResLoiDao loiDao = (CommonResDao.VariableResLoiDao) variableRes.get(32);
    assertEquals("LoiZSmin", loiDao.getNomRef());
    final CommonResDao.VariableResDao resDao = variableRes.get(24);
    assertEquals(VariableResDao.class, resDao.getClass());
    assertEquals("ZLimGLitSto", resDao.getNomRef());
  }
}
