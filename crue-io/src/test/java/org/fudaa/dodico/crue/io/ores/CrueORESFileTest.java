package org.fudaa.dodico.crue.io.ores;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * Classe de tests JUnit pour ORES (Données des REsultats Supplementaires) ; Permet de contrôler le bon fonctionnement de lecture d'un fichier XML
 * existant, l'écriture de façon similaire de ce même fichier et valide par XSD le fichier XML initial et celui produit
 *
 * @author cde
 */
public class CrueORESFileTest extends AbstractIOParentTest {
  /**
   * Constructeur
   */
  public CrueORESFileTest() {
    super(Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ORES,
        TestCoeurConfig.INSTANCE), FilesForTest.ORES_V1_3);
  }

  @Test
  public void testValideVersion_v1_2() {
    final CtuluLog log = new CtuluLog();
    final boolean valide = Crue10FileFormatFactory.getInstance()
        .getFileFormat(CrueFileType.ORES, TestCoeurConfig.INSTANCE_1_2)
        .isValide(FilesForTest.ORES_V1_2_DEFAULT, log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    Assert.assertTrue(valide);
  }

  @Test
  public void testValideVersion_v1_1_1() {
    final CtuluLog log = new CtuluLog();
    boolean valide = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ORES,
        TestCoeurConfig.INSTANCE_1_1_1).isValide(FilesForTest.ORES_V1_1_1, log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    Assert.assertTrue(valide);
    valide = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ORES,
        TestCoeurConfig.INSTANCE_1_1_1).isValide(FilesForTest.ORES_V1_1_1_DEFAULT, log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    Assert.assertTrue(valide);
  }

  /**
   * Lit le fichier XML et verifie certaines valeurs des différentes lois
   */
  @Test
  public void testLecture_v1_2() {
    final OrdResScenario data = readModele(FilesForTest.ORES_V1_2, TestCoeurConfig.INSTANCE_1_2);
    verifieDonnees(data,false);
  }
  @Test
  public void testLecture() {
    final OrdResScenario data = readModele(FilesForTest.ORES_V1_3, TestCoeurConfig.INSTANCE);
    verifieDonnees(data,true);
  }

  /**
   * Lit le fichier XML et verifie certaines valeurs des différentes lois
   */
  @Test
  public void testLecture_v1_1_1() {
    final OrdResScenario data = readModele(FilesForTest.ORES_V1_1_1, TestCoeurConfig.INSTANCE_1_1_1);
    verifieDonnees(data,false);
  }

  /**
   * Lecture du fichier XML puis écriture dans un fichier temporaire
   */
  @Test
  public void testEcriture_v1_1_1() {
    testEcriture(FilesForTest.ORES_V1_1_1, TestCoeurConfig.INSTANCE_1_1_1);
  }

  public File testEcriture(final String file, final CoeurConfigContrat version) {

    // On lit
    OrdResScenario data = readModele(file, version);
    final File f = createTempFile();

    // On ecrit
    final CtuluLog analyzer = new CtuluLog();
    final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ORES,
        version);
    fileFormat.writeMetierDirect(data, f, analyzer, version.getCrueConfigMetier());
    testAnalyser(analyzer);
    final boolean valide = fileFormat.isValide(f, analyzer);
    testAnalyser(analyzer);
    Assert.assertTrue(valide);
    AbstractIOParentTest.testAnalyser(analyzer);
    data = readModele(file, version);
    verifieDonnees(data, Crue10VersionConfig.isUpperThan_V1_2(version));
    return f;
  }

  /**
   * Lecture du fichier XML puis Ecriture dans un fichier temporaire puis Lecture de ce fichier temporaire et verification de certaines valeurs de
   * différents resultats supplémentaires
   */
  @Test
  public void testLectureEcritureLecture_v1_2() {
    testEcriture(FilesForTest.ORES_V1_2, TestCoeurConfig.INSTANCE_1_2);
  }

  @Test
  public void testLectureEcritureLecture_v1_1_1() {
    testEcriture(FilesForTest.ORES_V1_1_1, TestCoeurConfig.INSTANCE_1_1_1);
  }

  /**
   * Lecture du fichier XML du Model 3
   *
   * @return liste des resultats supplementaires
   */
  private OrdResScenario readModele(final String file, final CoeurConfigContrat version) {

    final CtuluLog analyzer = new CtuluLog();
    final OrdResScenario data = (OrdResScenario) Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ORES,
        version).read(file, analyzer, createDefault(version)).getMetier();
    testAnalyser(analyzer);
    return data;
  }

  /**
   * Verifie certaines donnees de differents resultats supplementaires
   *
   * @param data liste des resultats supplementaires à vérifier
   */
  private void verifieDonnees(final OrdResScenario data, boolean containRegul) {

    Assert.assertNotNull(data);

    final OrdResNoeudNiveauContinu noeudNivContinu = data.getOrdResNoeudNiveauContinu();
    Assert.assertTrue(noeudNivContinu.getDde("z").getValeur());

    final OrdResCasier casierProfil = data.getOrdResCasier();
    Assert.assertTrue(casierProfil.getDde("splan").getValeur());
    Assert.assertTrue(casierProfil.getDde("vol").getValeur());
    Assert.assertTrue(casierProfil.getDde("qech").getValeur());

    final OrdResSection sectionProfil = data.getOrdResSection();
    Assert.assertTrue(sectionProfil.getDde("q").getValeur());
    Assert.assertTrue(sectionProfil.getDde("z").getValeur());
    Assert.assertTrue(sectionProfil.getDde("h").getValeur());
    Assert.assertTrue(sectionProfil.getDde("zf").getValeur());
    Assert.assertTrue(sectionProfil.getDde("vact").getValeur());
    Assert.assertTrue(sectionProfil.getDde("sact").getValeur());
    Assert.assertTrue(sectionProfil.getDde("lact").getValeur());
    Assert.assertTrue(sectionProfil.getDde("dact").getValeur());

    final OrdResBranchePdc branchePdc = data.getOrdResBranchePdc();
    Assert.assertTrue(branchePdc.getDde("dz").getValeur());

    final OrdResBrancheSeuilTransversal brancheSeuilT = data.getOrdResBrancheSeuilTransversal();
    Assert.assertTrue(brancheSeuilT.getDde("regimeSeuil").getValeur());

    final OrdResBrancheSeuilLateral brancheSeuilL = data.getOrdResBrancheSeuilLateral();
    Assert.assertTrue(brancheSeuilL.getDde("regimeSeuil").getValeur());

    final OrdResBrancheOrifice brancheOrifice = data.getOrdResBrancheOrifice();
    Assert.assertTrue(brancheOrifice.getDde("regimeOrifice").getValeur());
    Assert.assertTrue(brancheOrifice.getDde("coefCtr").getValeur());

    final OrdResBrancheStrickler brancheStric = data.getOrdResBrancheStrickler();
    Assert.assertTrue(brancheStric.getDde("splan").getValeur());
    Assert.assertTrue(brancheStric.getDde("vol").getValeur());

    final OrdResBrancheBarrageGenerique brancheBarrageG = data.getOrdResBrancheBarrageGenerique();
    Assert.assertTrue(brancheBarrageG.getDde("regimeBarrage").getValeur());

    final OrdResBrancheBarrageFilEau brancheBarrageFil = data.getOrdResBrancheBarrageFilEau();
    Assert.assertTrue(brancheBarrageFil.getDde("regimeBarrage").getValeur());

    final OrdResBrancheSaintVenant brancheStVenant = data.getOrdResBrancheSaintVenant();
    Assert.assertTrue(brancheStVenant.getDde("qlat").getValeur());
    Assert.assertTrue(brancheStVenant.getDde("splanAct").getValeur());
    Assert.assertTrue(brancheStVenant.getDde("splanSto").getValeur());
    Assert.assertTrue(brancheStVenant.getDde("splanTot").getValeur());
    Assert.assertTrue(brancheStVenant.getDde("vol").getValeur());

    Assert.assertNotNull(data.getOrdResModeleRegul());
    if(containRegul){
      Assert.assertTrue(data.getOrdResModeleRegul().getDde("qZregul").getValeur());
      Assert.assertTrue(data.getOrdResModeleRegul().getDde("mode").getValeur());
      Assert.assertTrue(data.getOrdResModeleRegul().getDde("loiRegul").getValeur());
      Assert.assertTrue(data.getOrdResModeleRegul().getDde("zcns").getValeur());

    }
  }
}
