package org.fudaa.dodico.crue.io.lhpt;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.aoc.LoiMesuresPost;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;
import org.fudaa.dodico.crue.metier.emh.LoiDF;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.emh.LoiTF;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * Classe de tests JUnit pour DLHY (Données des Lois HYdrauliques) ; Permet de contrôler le bon fonctionnement de lecture d'un fichier XML existant,
 * l'écriture de façon similaire de ce même fichier ou d'un autre correspondant aux mêmes définitions de contenu
 *
 * @author cde
 */
public class CrueLHPTFileTest extends AbstractIOParentTest {
    /**
     * Constructeur
     */
    public CrueLHPTFileTest() {
        super(Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.LHPT, TestCoeurConfig.INSTANCE), FilesForTest.LHPT_V1_3);
    }

    private static LoiMesuresPost readModele(String file, CoeurConfigContrat coeurConfigContrat) {
        final CtuluLog analyzer = new CtuluLog();
        final LoiMesuresPost data = (LoiMesuresPost) getFileFormat(CrueFileType.LHPT, coeurConfigContrat).read(file, analyzer, createDefault(coeurConfigContrat)).getMetier();
        testAnalyser(analyzer);
        return data;
    }

    /**
     * Lecture du fichier XML puis écriture dans un fichier temporaire
     */
    @Test
    public void testEcriture() {
        testEcriture(readModele(FilesForTest.LHPT_V1_3, TestCoeurConfig.INSTANCE));
    }
    @Test
    public void testEcriture_V1_2() {
        testEcriture(readModele(FilesForTest.LHPT_V1_2, TestCoeurConfig.INSTANCE_1_2));
    }

    private void testEcriture(LoiMesuresPost data) {
        final File f = createTempFile();
        // On ecrit
        final CtuluLog analyzer = new CtuluLog();
        format.writeMetierDirect(data, f, analyzer, CrueConfigMetierForTest.DEFAULT);
        testAnalyser(analyzer);
        Assert.assertTrue(format.isValide(f, analyzer));
        testAnalyser(analyzer);
        final LoiMesuresPost readData = (LoiMesuresPost) format.read(f, analyzer, createDefault(CrueConfigMetierForTest.DEFAULT)).getMetier();
        testAnalyser(analyzer);
        verifieDonnees(readData);
    }

    /**
     * Lit le fichier XML et verifie certaines valeurs des différentes lois
     */
    @Test
    public void testLecture() {
        verifieDonnees(readModele(FilesForTest.LHPT_V1_3, TestCoeurConfig.INSTANCE));
    }
    @Test
    public void testLecture_V_1_2() {
        verifieDonnees(readModele(FilesForTest.LHPT_V1_2, TestCoeurConfig.INSTANCE_1_2));
    }

    /**
     * Verifie certaines donnees de differentes lois
     *
     * @param data liste des lois à vérifier
     */
    private void verifieDonnees(final LoiMesuresPost data) {

        Assert.assertEquals(4, data.getLois().size());
        final AbstractLoi tq1 = data.getLois().get(0);
        Assert.assertTrue(tq1 instanceof LoiDF);
        Assert.assertEquals("LoiTQ 1", tq1.getCommentaire());
        Assert.assertEquals("LoiTQ_1", tq1.getNom());
        Assert.assertEquals(EnumTypeLoi.LoiTQ, tq1.getType());
        final AbstractLoi tz1 = data.getLois().get(1);
        Assert.assertTrue(tz1 instanceof LoiDF);
        Assert.assertEquals("LoiTZ_1", tz1.getNom());
        Assert.assertEquals(EnumTypeLoi.LoiTZ, tz1.getType());
        Assert.assertEquals("LoiTZ 1", tz1.getCommentaire());

        final AbstractLoi tqz = data.getLois().get(2);
        Assert.assertTrue(tqz instanceof LoiFF);
        Assert.assertEquals("LoiQZ 1", tqz.getCommentaire());
        Assert.assertEquals("LoiQZ_1", tqz.getNom());
        Assert.assertEquals(EnumTypeLoi.LoiQZ, tqz.getType());

        final AbstractLoi tsection = data.getLois().get(3);
        Assert.assertTrue(tsection instanceof LoiTF);
        Assert.assertEquals("LoiSectionsZ 1", tsection.getCommentaire());
        Assert.assertEquals("LoiSectionsZ_1", tsection.getNom());
        Assert.assertEquals(EnumTypeLoi.LoiSectionsZ, tsection.getType());

        Assert.assertEquals(4, data.getEchellesSections().getEchellesSectionList().size());
        Assert.assertEquals("P153.500", data.getEchellesSections().getEchellesSectionList().get(0).getPk());
        Assert.assertEquals("St_P153.500", data.getEchellesSections().getEchellesSectionList().get(0).getSectionRef());
        Assert.assertEquals("P155.000", data.getEchellesSections().getEchellesSectionList().get(3).getPk());
        Assert.assertEquals("St_P155.000", data.getEchellesSections().getEchellesSectionList().get(3).getSectionRef());
    }
}
