/*
 * Licence GPL Copyright
 */
package org.fudaa.dodico.crue.io.common;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.LoiDF;
import static org.junit.Assert.*;
import org.junit.Test;
/**
 * @author deniger
 */
public class CrueDataImplTest   {

  /**
   * Test du conteneur de lois.
   */
  @Test
  public void testAddLoi() {
    final CrueData impl = new CrueDataImpl(CrueConfigMetierForTest.DEFAULT);
    final List<Loi> lois = new ArrayList<>();
    lois.add(new LoiDF());
    impl.getLoiConteneur().addAllLois(lois);
    final List<Loi> savedLoi = impl.getLois();
    assertEquals(savedLoi.get(0), lois.get(0));
  }
}
