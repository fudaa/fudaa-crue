/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.io.avct;

import files.FilesForTest;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.metier.emh.CompteRenduAvancement;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author deniger
 */
public class AvctReaderTest {

  public AvctReaderTest() {
  }

  @Test
  public void testRead() {
    final AvctReader reader = new AvctReader();
    final CrueIOResu<CompteRenduAvancement> read = reader.read(AbstractIOParentTest.getResource(FilesForTest.AVCT_LOG_V1_2_SC_M30_C10));
    assertFalse(read.getAnalyse().containsErrorOrSevereError());
    final CompteRenduAvancement metier = read.getMetier();
    testContent(metier);
  }
  @Test
  public void testReadDebugFormat() {
    final AvctReader reader = new AvctReader();
    final CrueIOResu<CompteRenduAvancement> read = reader.read(AbstractIOParentTest.getResource("/v1_2/Sc_M3-0_c10Debug.avct.log"));
    assertFalse(read.getAnalyse().containsErrorOrSevereError());
    final CompteRenduAvancement metier = read.getMetier();
    testContent(metier);

  }
  

  private void testContent(final CompteRenduAvancement metier) {
    assertEquals("CALPP", metier.getCode());
    assertEquals("2012-03-06T17:11:23.000", DateDurationConverter.dateToXsd(metier.getDate()));
    assertEquals("2012-03-06 17:11:23", DateDurationConverter.dateToXsdUI(metier.getDate()));
    assertEquals(4, metier.getIndicateurs().size());
    assertEquals(0d, metier.getIndicateurs().get(3).getValeur(), 1e-5);
    assertEquals(40d, metier.getIndicateurs().get(3).getMax(), 1e-5);
  }
}
