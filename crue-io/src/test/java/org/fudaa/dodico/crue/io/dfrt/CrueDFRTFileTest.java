package org.fudaa.dodico.crue.io.dfrt;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.DonFrtManning;
import org.fudaa.dodico.crue.metier.emh.DonFrtStrickler;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;

/**
 * Test en lecture/ecriture de DFRT
 */
public class CrueDFRTFileTest extends AbstractIOParentTest {
    public CrueDFRTFileTest() {
        super(Crue10FileFormatFactory.getVersion(CrueFileType.DFRT, TestCoeurConfig.INSTANCE), FilesForTest.DFRT_V_1_3);
    }

    @Test
    public void testValideVersion1_1() {
        testValide(FilesForTest.DFRT_V_1_1_1, TestCoeurConfig.INSTANCE_1_1_1);
    }

    @Test
    public void testValideVersion1_2() {
        testValide(FilesForTest.DFRT_V_1_2, TestCoeurConfig.INSTANCE_1_2);
    }

    private void testValide(String file, CoeurConfigContrat coeurConfigContrat) {
        final CtuluLog log = new CtuluLog();
        final boolean valide = getFileFormat(CrueFileType.DFRT, coeurConfigContrat).isValide(file, log);
        if (log.containsErrorOrSevereError()) {
            log.printResume();
        }
        Assert.assertTrue(valide);
    }

    @Test
    public void testLecture() {
        testDonFrt(readModele(TestCoeurConfig.INSTANCE, FilesForTest.DFRT_V_1_3));
    }

    @Test
    public void testLecture_V_1_2() {
        testDonFrt(readModele(TestCoeurConfig.INSTANCE_1_2, FilesForTest.DFRT_V_1_2));
    }

    private Crue10FileFormat<Object> getFileFormat_v_1_2() {
        return Crue10FileFormatFactory.getInstance()
                .getFileFormat(CrueFileType.DFRT, TestCoeurConfig.getInstance(Crue10VersionConfig.V_1_2));
    }

    /**
     * @param frts les frottements
     */
    private void testDonFrt(final List<DonFrt> frts) {
        Assert.assertNotNull(frts);
        for (final DonFrt donfrt : frts) {
            Assert.assertNotNull(donfrt);
        }
        DonFrtStrickler donFrtList = (DonFrtStrickler) frts.get(0);
        Assert.assertEquals("Fk_K0", donFrtList.getNom());
        assertDoubleEquals(0, donFrtList.getLoi().getEvolutionFF().getPtEvolutionFF().get(0).getOrdonnee());
        donFrtList = (DonFrtStrickler) frts.get(1);
        assertDoubleEquals(0.00, donFrtList.getLoi().getEvolutionFF().getPtEvolutionFF().get(0).getAbscisse());
        assertDoubleEquals(10.00, donFrtList.getLoi().getEvolutionFF().getPtEvolutionFF().get(1).getAbscisse());
        assertDoubleEquals(15.00, donFrtList.getLoi().getEvolutionFF().getPtEvolutionFF().get(0).getOrdonnee());
        assertDoubleEquals(15.00, donFrtList.getLoi().getEvolutionFF().getPtEvolutionFF().get(1).getOrdonnee());
        donFrtList = (DonFrtStrickler) frts.get(2);
        assertDoubleEquals(0.00, donFrtList.getLoi().getEvolutionFF().getPtEvolutionFF().get(0).getAbscisse());
        assertDoubleEquals(10.00, donFrtList.getLoi().getEvolutionFF().getPtEvolutionFF().get(1).getAbscisse());
        assertDoubleEquals(30.00, donFrtList.getLoi().getEvolutionFF().getPtEvolutionFF().get(0).getOrdonnee());
        assertDoubleEquals(30.00, donFrtList.getLoi().getEvolutionFF().getPtEvolutionFF().get(1).getOrdonnee());
    }


    public static List<DonFrt> readModeleLastVersion() {
        final CtuluLog analyse = new CtuluLog();
        final List<DonFrt> jeuDonnees = (List<DonFrt>) Crue10FileFormatFactory
                .getVersion(CrueFileType.DFRT, TestCoeurConfig.INSTANCE).read(FilesForTest.DFRT_V_1_3, analyse, createDefault(TestCoeurConfig.INSTANCE))
                .getMetier();
        if (analyse.containsErrorOrSevereError()) {
            analyse.printResume();
        }
        Assert.assertFalse(analyse.containsErrorOrSevereError());
        return jeuDonnees;
    }

    @Test
    public void testEcriture() {
        final List<DonFrt> in = readModele(TestCoeurConfig.INSTANCE, FilesForTest.DFRT_V_1_3);
        testEcriture(in);
    }

    @Test
    public void testEcriture_V_1_2() {
        final List<DonFrt> in = readModele(TestCoeurConfig.INSTANCE_1_2, FilesForTest.DFRT_V_1_2);
        testEcriture(in);
    }

    private void testEcriture(List<DonFrt> in) {
        Assert.assertNotNull(in);
        final File f = createTempFile();
        //on ecrit dans le dernier format uniquement
        testWrite(in, f);
        final CtuluLog analyse = new CtuluLog();
        final List<DonFrt> jeuDonnees = (List<DonFrt>) format.read(f, analyse, createDefault(TestCoeurConfig.INSTANCE)).getMetier();
        testAnalyser(analyse);
        testDonFrt(jeuDonnees);
    }

    @Test
    public void testEcritureManning() {
        final List<DonFrt> in = readModele(TestCoeurConfig.INSTANCE, FilesForTest.MANNING_DFRT_V_1_3);
        testEcritureManning(in, TestCoeurConfig.INSTANCE);
    }

    @Test
    public void testEcritureManning_V_1_2() {
        final List<DonFrt> in = readModele(TestCoeurConfig.INSTANCE_1_2, FilesForTest.MANNING_DFRT_V_1_2);
        testEcritureManning(in, TestCoeurConfig.INSTANCE_1_2);
    }

    public void testEcritureManning(List<DonFrt> in, CoeurConfigContrat coeurConfigContrat) {
        Assert.assertNotNull(in);
        final File f = createTempFile();
        testWrite(in, f);
        final CtuluLog analyse = new CtuluLog();
        final List<DonFrt> jeuDonnees = (List<DonFrt>) getFileFormat(CrueFileType.DFRT, coeurConfigContrat).read(f, analyse, createDefault(coeurConfigContrat)).getMetier();
        testAnalyser(analyse);
        testDonFrt(jeuDonnees);
        testForStoAndManning(in);
    }

    private void testWrite(final List<DonFrt> in, final File f) {
        final CtuluLog analyse = new CtuluLog();
        final boolean res = format.writeMetierDirect(in, f, analyse, CrueConfigMetierForTest.DEFAULT);
        testAnalyser(analyse);
        Assert.assertTrue(res);
        final boolean valide = format.isValide(f, analyse);
        testAnalyser(analyse);
        Assert.assertTrue(valide);
    }

    private void testForStoAndManning(final List<DonFrt> frts) {
        final int last = frts.size() - 1;
        final DonFrtManning manning = (DonFrtManning) frts.get(last);
        Assert.assertEquals("Fn_1", manning.getNom());
        Assert.assertEquals(EnumTypeLoi.LoiZFN, manning.getLoi().getType());
        assertDoubleEquals(1, manning.getLoi().getEvolutionFF().getPtEvolutionFF().get(0).getAbscisse());
        assertDoubleEquals(2, manning.getLoi().getEvolutionFF().getPtEvolutionFF().get(0).getOrdonnee());

        final DonFrtStrickler fkSto = (DonFrtStrickler) frts.get(last - 1);
        Assert.assertEquals("FkSto_K0", fkSto.getNom());
        Assert.assertEquals(EnumTypeLoi.LoiZFKSto, fkSto.getLoi().getType());
        assertDoubleEquals(0, fkSto.getLoi().getEvolutionFF().getPtEvolutionFF().get(0).getAbscisse());
        assertDoubleEquals(0, fkSto.getLoi().getEvolutionFF().getPtEvolutionFF().get(0).getOrdonnee());
    }

    private static List<DonFrt> readModele(CoeurConfigContrat format, final String file) {
        final CtuluLog analyse = new CtuluLog();
        final List<DonFrt> jeuDonnees = (List<DonFrt>) getFileFormat(CrueFileType.DFRT, format).read(file, analyse, createDefault(format))
                .getMetier();
        if (analyse.containsErrorOrSevereError()) {
            analyse.printResume();
        }
        Assert.assertFalse(analyse.containsErrorOrSevereError());
        return jeuDonnees;
    }
}
