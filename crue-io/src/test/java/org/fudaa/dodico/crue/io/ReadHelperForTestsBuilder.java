package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.test.ReadHelperForTest;

public class ReadHelperForTestsBuilder {
    private static ReadHelperForTest instance;

    public static ReadHelperForTest get() {

        if (instance == null) {
            instance = new ReadHelperForTest(TestCoeurConfig.INSTANCE);
        }
        return instance;
    }
}
