package org.fudaa.dodico.crue.io.dlhy;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;

/**
 * Classe de tests JUnit pour DLHY (Données des Lois HYdrauliques) ; Permet de contrôler le bon fonctionnement de lecture d'un fichier XML existant,
 * l'écriture de façon similaire de ce même fichier ou d'un autre correspondant aux mêmes définitions de contenu
 *
 * @author cde
 */
public class CrueDLHYFileTest extends AbstractIOParentTest {
  /**
   * Constructeur
   */
  public CrueDLHYFileTest() {
    super(Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DLHY, TestCoeurConfig.INSTANCE), FilesForTest.DLHY_V1_3);
  }

  @Test
  public void testValide_v_1_1_1() {
    final CtuluLog log = new CtuluLog();
    final boolean valide = Crue10FileFormatFactory.getInstance()
        .getFileFormat(CrueFileType.DLHY, TestCoeurConfig.INSTANCE_1_1_1).isValide(FilesForTest.DLHY_V1_1_1, log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    Assert.assertTrue(valide);
  }
  @Test
  public void testValide_v_1_2() {
    final CtuluLog log = new CtuluLog();
    final boolean valide = getFileFormat(CrueFileType.DLHY, TestCoeurConfig.INSTANCE_1_2).isValide(FilesForTest.DLHY_V1_2, log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    Assert.assertTrue(valide);
  }

  /**
   * Lecture du fichier XML du Model 3
   *
   * @return liste de lois métier
   */
  private List<Loi> readModele() {
    return readModele(TestCoeurConfig.INSTANCE,FilesForTest.DLHY_V1_3);
  }
  private static List<Loi> readModele(CoeurConfigContrat format, String file) {
    final CtuluLog analyzer = new CtuluLog();
    final List<Loi> data = (List<Loi>) getFileFormat(CrueFileType.DLHY,format).read(file, analyzer, createDefault(format)).getMetier();
    testAnalyser(analyzer);
    return data;
  }

  /**
   * Lecture du fichier XML puis écriture dans un fichier temporaire
   */
  @Test
  public void testEcriture() {
    final File f = createTempFile();
    // On lit
    final List<Loi> data = readModele();

    // On ecrit
    final CtuluLog analyzer = new CtuluLog();
    format.writeMetierDirect(data, f, analyzer, CrueConfigMetierForTest.DEFAULT);
    testAnalyser(analyzer);
    Assert.assertTrue(format.isValide(f, analyzer));
    testAnalyser(analyzer);
    final List<Loi> readData = (List<Loi>) format.read(f, analyzer, createDefault(CrueConfigMetierForTest.DEFAULT)).getMetier();
    testAnalyser(analyzer);
    verifieDonnees(readData);
  }

  /**
   * Lit le fichier XML et verifie certaines valeurs des différentes lois
   */
  @Test
  public void testLecture() {
    verifieDonnees(readModele());
  }
  @Test
  public void testLecture_V_1_2() {
    verifieDonnees(readModele(TestCoeurConfig.INSTANCE_1_2,FilesForTest.DLHY_V1_2));
  }

  /**
   * Verifie certaines donnees de differentes lois
   *
   * @param data liste des lois à vérifier
   */
  private void verifieDonnees(final List<Loi> data) {

    Assert.assertNotNull(data);
    int countLoiDF = 0;
    int countLoiFF = 0;
    for (final Loi loi : data) {
      if (loi instanceof LoiDF) {
        countLoiDF++;
        if ("LoiTQapp_HydrogrammeN1".equals(loi.getNom())) {
          testHydrogrammeN1(loi);
        } else if ("LoiTQruis_HydrogrammeRuis2".equals(loi.getNom())) {
          testHydrogrammeRuis2(loi);
        } else if ("LoiTouv_ManoeuvreB8".equals(loi.getNom())) {
          testManoeuvreB8(loi);
        }
      } else if (loi instanceof LoiFF) {
        countLoiFF++;
        if ("LoiQZ_TarrageN5".equals(loi.getNom())) {
          testTarrageN5(loi);
        }
      }
    }

    Assert.assertEquals(5, countLoiDF);
    Assert.assertEquals(2, countLoiFF);
  }

  private void testTarrageN5(final Loi loi) {
    final LoiFF loiTarrage = (LoiFF) loi;
    Assert.assertEquals(loiTarrage.getCommentaire(), "Tarrage N5");
    Assert.assertEquals(EnumTypeLoi.LoiQZimp, loiTarrage.getType());
    final EvolutionFF evolFF = loiTarrage.getEvolutionFF();
    Assert.assertNotNull(evolFF);
    int countPoint = 0;
    for (final PtEvolutionFF ptEvolFF : evolFF.getPtEvolutionFF()) {
      if (countPoint == 0) {
        assertDoubleEquals(ptEvolFF.getOrdonnee(), 1.0);
        assertDoubleEquals(ptEvolFF.getAbscisse(), 0.0);
      } else if (countPoint == 3) {
        assertDoubleEquals(ptEvolFF.getOrdonnee(), 2.5);
        assertDoubleEquals(ptEvolFF.getAbscisse(), -175.0);
      } else if (countPoint == 9) {
        assertDoubleEquals(ptEvolFF.getOrdonnee(), 5.5);
        assertDoubleEquals(ptEvolFF.getAbscisse(), -500.0);
      }

      countPoint++;
    }
  }

  private void testManoeuvreB8(final Loi loi) {
    final LoiDF loiVanne = (LoiDF) loi;
    Assert.assertEquals(loiVanne.getCommentaire(), "Manoeuvre B8");
    Assert.assertEquals(EnumTypeLoi.LoiTOuv, loiVanne.getType());
    final EvolutionFF evolDF = loiVanne.getEvolutionFF();
    Assert.assertNotNull(evolDF);
    int countPoint = 0;
    for (final PtEvolutionFF ptEvolDF : evolDF.getPtEvolutionFF()) {
      if (countPoint == 0) {
        assertDoubleEquals(ptEvolDF.getAbscisse(), 0);
        assertDoubleEquals(ptEvolDF.getOrdonnee(), 90.0);
      } else if (countPoint == 3) {
        assertDoubleEquals(ptEvolDF.getAbscisse(), 86400);
        assertDoubleEquals(ptEvolDF.getOrdonnee(), 100.0);
      }

      countPoint++;
    }
  }

  private void testHydrogrammeRuis2(final Loi loi) {
    final LoiDF loiHydrogramme = (LoiDF) loi;
    Assert.assertEquals(loiHydrogramme.getCommentaire(), "Hydrogramme Ruis 2");
    Assert.assertEquals(EnumTypeLoi.LoiTQruis, loiHydrogramme.getType());
    final EvolutionFF evolDF = loiHydrogramme.getEvolutionFF();
    Assert.assertNotNull(evolDF);
    int countPoint = 0;

    for (final PtEvolutionFF ptEvolDF : evolDF.getPtEvolutionFF()) {
      final double crueDatePtEvolDF = ptEvolDF.getAbscisse();
      if (countPoint == 0) {
        assertDoubleEquals(crueDatePtEvolDF, 0);
        assertDoubleEquals(ptEvolDF.getOrdonnee(), 0.0);
      } else if (countPoint == 1) {
        assertDoubleEquals(86400, crueDatePtEvolDF);
        assertDoubleEquals(ptEvolDF.getOrdonnee(), 0.0);
      }

      countPoint++;
    }
  }

  private void testHydrogrammeN1(final Loi loi) {
    final LoiDF loiHydrogramme = (LoiDF) loi;
    Assert.assertEquals(loiHydrogramme.getCommentaire(), "Hydrogramme N1");
    Assert.assertEquals(EnumTypeLoi.LoiTQapp, loiHydrogramme.getType());
    final EvolutionFF evolDF = loiHydrogramme.getEvolutionFF();
    Assert.assertNotNull(evolDF);
    int countPoint = 0;
    for (final PtEvolutionFF ptEvolDF : evolDF.getPtEvolutionFF()) {
      if (countPoint == 0) {
        assertDoubleEquals(0, ptEvolDF.getAbscisse());
        assertDoubleEquals(100.0, ptEvolDF.getOrdonnee());
      } else if (countPoint == 6) {
        assertDoubleEquals(32400, ptEvolDF.getAbscisse());
        assertDoubleEquals(450.0, ptEvolDF.getOrdonnee());
      } else if (countPoint == 16) {
        assertDoubleEquals(86400, ptEvolDF.getAbscisse());
        assertDoubleEquals(100.0, ptEvolDF.getOrdonnee());
      }

      countPoint++;
    }
  }
}
