package org.fudaa.dodico.crue.io.optg;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.OrdPrtGeoModeleBase;
import org.fudaa.dodico.crue.metier.emh.PlanimetrageNbrPdzCst;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * @author Adrien Hadoux
 */
public class CrueOPTGFileTest extends AbstractIOParentTest {
  public CrueOPTGFileTest() {
    super(Crue10FileFormatFactory.getVersion(CrueFileType.OPTG, TestCoeurConfig.INSTANCE), FilesForTest.OPTG_V1_2);
  }

  @Test
  public void testValideVersion1p1() {
    final CtuluLog log = new CtuluLog();
    final boolean valide = Crue10FileFormatFactory.getVersion(CrueFileType.OPTG, TestCoeurConfig.INSTANCE_1_1_1).isValide("/v1_1_1/M3-0_c10.optg.xml", log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    Assert.assertTrue(valide);
  }

  @Test
  public void testLecture() {
    final OrdPrtGeoModeleBase data = readModele(FilesForTest.OPTG_V1_2, TestCoeurConfig.INSTANCE);
    testDataModele3(data);
  }

  @Test
  public void testLectureV1P1P1() {
    final OrdPrtGeoModeleBase data = readModele(FilesForTest.OPTG_V1_1_1, TestCoeurConfig.INSTANCE_1_1_1);
    testDataModele3(data);
  }

  private void testDataModele3(final OrdPrtGeoModeleBase data) {
    Assert.assertNotNull(data);

    assertDoubleEquals(50, ((PlanimetrageNbrPdzCst) data.getPlanimetrage()).getNbrPdz());
    assertDoubleEquals(0.2, data.getRegles().get(0).getSeuilDetect());
  }

  @Test
  public void testEcriture() {
    testEcriture(FilesForTest.OPTG_V1_2, TestCoeurConfig.INSTANCE);
  }

  @Test
  public void testEcritureV1P1P1() {
    testEcriture(FilesForTest.OPTG_V1_1_1, TestCoeurConfig.INSTANCE_1_1_1);
  }

  public void testEcriture(final String file, final CoeurConfigContrat version) {
    final CtuluLog analyzer = new CtuluLog();
    final File f = createTempFile();
    final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getVersion(CrueFileType.OPTG, version);
    fileFormat.writeMetierDirect(readModele(file, version), f, analyzer, CrueConfigMetierForTest.DEFAULT);
    Assert.assertFalse(analyzer.containsErrors());
    analyzer.clear();
    final boolean valide = fileFormat.isValide(f, analyzer);
    testAnalyser(analyzer);
    Assert.assertTrue(valide);

    final OrdPrtGeoModeleBase read = (OrdPrtGeoModeleBase) fileFormat.read(f, analyzer, createDefault(version)).getMetier();

    testDataModele3(read);
  }

  public static OrdPrtGeoModeleBase readModeleLastVersion(final CrueData crueData) {
    final CtuluLog analyzer = new CtuluLog();
    final OrdPrtGeoModeleBase data = (OrdPrtGeoModeleBase) Crue10FileFormatFactory.getVersion(CrueFileType.OPTG, TestCoeurConfig.INSTANCE)
        .read(FilesForTest.OPTG_V1_2, analyzer, crueData).getMetier();
    testAnalyser(analyzer);
    return data;
  }

  public OrdPrtGeoModeleBase readModele(final String file, final CoeurConfigContrat version) {
    final CtuluLog analyzer = new CtuluLog();
    final OrdPrtGeoModeleBase data = (OrdPrtGeoModeleBase) Crue10FileFormatFactory.getVersion(CrueFileType.OPTG, version)
        .read(file, analyzer, createDefault(version)).getMetier();
    testAnalyser(analyzer);
    return data;
  }
}
