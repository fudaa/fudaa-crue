package org.fudaa.dodico.crue.io.pcal;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.ParamCalcScenario;
import org.fudaa.dodico.crue.metier.emh.PdtCst;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * Classe de tests JUnit pour PCAL (Paramètres de CALculs) ; Permet de contrôler le bon fonctionnement de lecture d'un fichier XML
 * existant, l'écriture de façon similaire de ce même fichier et valide par XSD le fichier XML initial et celui produit
 *
 * @author CDE
 */
public class CruePCALFileTest extends AbstractIOParentTest {
    /**
     * Constructeur
     */
    public CruePCALFileTest() {
        super(Crue10FileFormatFactory.getVersion(CrueFileType.PCAL, TestCoeurConfig.INSTANCE), FilesForTest.PCAL_V1_3);
    }

    @Test
    public void testValide_V_1_1_1() {
        Assert.assertTrue(isValid(FilesForTest.PCAL_V1_1_1, TestCoeurConfig.INSTANCE_1_1_1));
    }

    @Test
    public void testValide_V_1_2() {
        Assert.assertTrue(isValid(FilesForTest.PCAL_V1_2, TestCoeurConfig.INSTANCE_1_2));
    }

    /**
     * Lit le fichier XML et verifie certaines valeurs des différents paramètres de calculs
     */
    @Test
    public void testLecture() {
        verifieDonnees(read(FilesForTest.PCAL_V1_3, TestCoeurConfig.INSTANCE).getPCAL());
    }

    @Test
    public void testLecture_V_1_2() {
        verifieDonnees(read(FilesForTest.PCAL_V1_2, TestCoeurConfig.INSTANCE_1_2).getPCAL());
    }

    private final static CrueData read(String file, CoeurConfigContrat coeurConfigContrat) {
        final CtuluLog analyser = new CtuluLog();
        // -- lecture de pcal --//
        return (CrueData) getFileFormat(CrueFileType.PCAL, coeurConfigContrat).read(file, analyser, createDefault(coeurConfigContrat)).getMetier();
    }

    private final static CrueData read(File file, CoeurConfigContrat coeurConfigContrat) {
        final CtuluLog analyser = new CtuluLog();
        // -- lecture de pcal --//
        return (CrueData) getFileFormat(CrueFileType.PCAL, coeurConfigContrat).read(file, analyser, createDefault(coeurConfigContrat)).getMetier();
    }

    private final static boolean isValid(String file, CoeurConfigContrat coeurConfigContrat) {
        final CtuluLog analyser = new CtuluLog();
        // -- lecture de pcal --//
        return getFileFormat(CrueFileType.PCAL, coeurConfigContrat).isValide(file, analyser);
    }

    private final static boolean isValid(File file, CoeurConfigContrat coeurConfigContrat) {
        final CtuluLog analyser = new CtuluLog();
        // -- lecture de pcal --//
        return getFileFormat(CrueFileType.PCAL, coeurConfigContrat).isValide(file, analyser);
    }

    @Test
    public void testLectureV1_1_1() {
        final CrueData crueData = read(FilesForTest.PCAL_V1_1_1, TestCoeurConfig.INSTANCE_1_1_1);
        // -- lecture de pcal --//
        final ParamCalcScenario jeuDonnees = crueData.getPCAL();
        verifieDonneesV1_1_1(jeuDonnees, crueData);
    }

    /**
     * Lit le fichier XML et verifie certaines valeurs des différents paramètres de calculs
     */
    @Test
    public void testFichierVierge() {
        // -- lecture de pcal --//
        doTestFichierVierge(TestCoeurConfig.INSTANCE, FilesForTest.PCAL_V1_3_EMPTY);
    }

    @Test
    public void testFichierVierge_V_1_2() {
        // -- lecture de pcal --//
        doTestFichierVierge(TestCoeurConfig.INSTANCE_1_2, FilesForTest.PCAL_V1_2_EMPTY);
    }

    @Test
    public void testFichierVierge_V_1_1_1() {
        // -- lecture de pcal --//
        doTestFichierVierge(TestCoeurConfig.INSTANCE_1_1_1, FilesForTest.PCAL_V1_1_1_EMPTY);
    }

    private void doTestFichierVierge(TestCoeurConfig coeurConfigContrat, String file) {
        final CrueData initData = read(file, coeurConfigContrat);
        Assert.assertNull(initData.getPCAL().getDateDebSce());
        if (Crue10VersionConfig.isV1_1_1(coeurConfigContrat)) {
            Assert.assertNotNull(initData.getOldPCALDureeSce());
        }
        CtuluLog analyser = new CtuluLog();
        final File f = createTempFile();
        getFileFormat(CrueFileType.PCAL, coeurConfigContrat).writeMetierDirect(initData, f, analyser, CrueConfigMetierForTest.DEFAULT);
        Assert.assertFalse(analyser.containsErrorOrSevereError());
        Assert.assertTrue(isValid(f, coeurConfigContrat));
        final ParamCalcScenario readData = read(f, coeurConfigContrat).getPCAL();
        Assert.assertNull(readData.getDateDebSce());
    }

    /**
     * Lecture du fichier XML puis écriture dans un fichier temporaire
     *
     * @return le fichier temporaire XML créé à partir de la lecture du fichier existant
     */
    public File write(String readFile, CoeurConfigContrat readCoeurConfigContrat) {
        final CtuluLog analyser = new CtuluLog();
        final CrueData crueData = read(readFile, readCoeurConfigContrat);

        // On lit
        final ParamCalcScenario data = (crueData).getPCAL();
        final File f = createTempFile();

        // On ecrit
        final CtuluLog analyzer = new CtuluLog();
        format.writeMetierDirect(crueData, f, analyzer, CrueConfigMetierForTest.DEFAULT);
        Assert.assertFalse(analyser.containsErrorOrSevereError());
        return f;
    }

    /**
     * Lecture du fichier XML puis Ecriture dans un fichier temporaire puis Lecture de ce fichier temporaire et verification de
     * certaines valeurs de différents paramètres de calculs
     */
    @Test
    public void testLectureEcritureLecture() {

        final File f = write(FilesForTest.PCAL_V1_3, TestCoeurConfig.INSTANCE);
        final CtuluLog analyzer = new CtuluLog();
        testEcritureResults(f, analyzer);
    }

    @Test
    public void testLectureEcritureLecture_V_1_2() {

        final File f = write(FilesForTest.PCAL_V1_2, TestCoeurConfig.INSTANCE_1_2);
        final CtuluLog analyzer = new CtuluLog();
        testEcritureResults(f, analyzer);
    }

    private void testEcritureResults(File f, CtuluLog analyzer) {
        // On lit le fichier temporaire précédemment créé et on le vérifie
        final ParamCalcScenario data = ((CrueData) format.read(f, analyzer, createDefault(TestCoeurConfig.INSTANCE)).getMetier()).getPCAL();
        testAnalyser(analyzer);
        final boolean valide = format.isValide(f, analyzer);
        testAnalyser(analyzer);
        Assert.assertTrue(valide);
        verifieDonnees(data);
    }

    /**
     * @param metier
     */
    private static void verifieDonnees(final ParamCalcScenario metier) {

        Assert.assertNotNull(metier);
        Assert.assertNull(metier.getDateDebSce());
    }

    private static void verifieDonneesV1_1_1(final ParamCalcScenario metier, final CrueData crueData) {

        Assert.assertNotNull(metier);
        Assert.assertNull(metier.getDateDebSce());
        Assert.assertNotNull(crueData.getOldPCALPdtRes());
        Assert.assertEquals("P0Y0M1DT0H0M1S", DateDurationConverter.durationToXsd(crueData.getOldPCALDureeSce()));
        Assert.assertEquals("P0Y0M0DT1H0M2S", DateDurationConverter.durationToXsd(((PdtCst) crueData.getOldPCALPdtRes()).getPdtCst()));
    }
}
