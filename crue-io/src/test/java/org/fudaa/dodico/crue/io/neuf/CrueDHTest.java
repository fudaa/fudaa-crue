package org.fudaa.dodico.crue.io.neuf;

import files.FilesForTest;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.config.ccm.SeveriteManager;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.Crue9FileFormatFactory;
import org.fudaa.dodico.crue.io.ReadHelperForTestsBuilder;
import org.fudaa.dodico.crue.io.drso.CrueDRSOFileTest;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.joda.time.Duration;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.net.MalformedURLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 * Tests junit pour les fichiers DH IO. fortran, .
 *
 * @author Adrien Hadoux
 */
public class CrueDHTest extends AbstractTestParent {


  /**
   * @param analyzer
   * @param f
   * @param data
   */
  public static void writeModeleCrue9(final CtuluLog analyzer, final File f, final CrueData data) {

    Crue9FileFormatFactory.getDHFileFormat().write(data, f, analyzer);
  }

  /**
   * Test de lecture
   */
  @Test
  public void testLecture() {

    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueIOResu<CrueData> data = readModeleCrue9(analyzer, FilesForTest.DH_M_30);
    testAnalyser(analyzer);
    assertEquals(
        "CrueX - Structuration des donnees||Modele de test utilisant les elements de modelisation hydraulique les plus courants||PBa Jan09 sur la base de Modele2",
        StringUtils.replace(data.getCrueCommentaire(), "\n", "|"));

    testData(data.getMetier(), false);
    //le dh active OCAL
    CrueDCTest.assertSortieTrueAndDebug(data.getMetier().getOCAL().getSorties());
    //mais pas les autres:
    CrueDCTest.assertSortieTrueAndDebug(data.getMetier().getOPTG().getSorties());
    CrueDCTest.assertSortieTrueAndDebug(data.getMetier().getOPTI().getSorties());
    CrueDCTest.assertSortieTrueAndInfo(data.getMetier().getOPTR().getSorties());
  }

  @Test
  public void testLectureIsortiVaut0() {

    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueIOResu<CrueData> data = readModeleCrue9(analyzer, FilesForTest.DH_M_30_ISORTI_0);
    testAnalyser(analyzer);
    //le dh active OCAL
    CrueDCTest.assertSortieTrueAndInfo(data.getMetier().getOCAL().getSorties());
  }

  @Test
  public void testTransformIsorti0En1() {
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueIOResu<CrueData> data = readModeleCrue9(analyzer, FilesForTest.DH_M_30_ISORTI_0);
    testAnalyser(analyzer);
    //le dh active OCAL
    CrueDCTest.assertSortieTrueAndInfo(data.getMetier().getOCAL().getSorties());
    data.getMetier().getOCAL().getSorties().getTrace().setVerbositeFichier(SeveriteManager.DEFAULT_DEBUG_LEVEL_FOR_CRUE9);
    final File f = createTempFile();
    Crue9FileFormatFactory.getDHFileFormat().writeMetier(data, f, analyzer);
    testAnalyser(analyzer);
    try {
      final CrueIOResu<CrueData> dataWith1 = ReadHelperForTestsBuilder.get().readModele(analyzer, getClass().getResource(FilesForTest.MODELE3_DC),
          f.toURI().toURL());
      testAnalyser(dataWith1.getAnalyse());
      CrueDCTest.assertSortieTrueAndDebug(dataWith1.getMetier().getOCAL().getSorties());
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }
  }

  @Test
  public void testTransformIsorti1En0() {
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueIOResu<CrueData> data = readModeleCrue9(analyzer, FilesForTest.DH_M_30);
    testAnalyser(analyzer);
    //le dh active OCAL
    CrueDCTest.assertSortieTrueAndDebug(data.getMetier().getOCAL().getSorties());
    data.getMetier().getOCAL().getSorties().getTrace().setVerbositeEcran(SeveriteManager.INFO);
    data.getMetier().getOCAL().getSorties().getTrace().setVerbositeFichier(SeveriteManager.INFO);
    final File f = createTempFile();
    Crue9FileFormatFactory.getDHFileFormat().writeMetier(data, f, analyzer);
    testAnalyser(analyzer);
    try {
      final CrueIOResu<CrueData> dataWith1 = ReadHelperForTestsBuilder.get().readModele(analyzer, getClass().getResource(FilesForTest.MODELE3_DC),
          f.toURI().toURL());
      testAnalyser(dataWith1.getAnalyse());
      CrueDCTest.assertSortieTrueAndInfo(dataWith1.getMetier().getOCAL().getSorties());
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }
  }

  /**
   * Test du modele 4 fourni par la CNR.
   */
  @Test
  public void testLectureSc_M4_0_v1c9() {
    final CtuluLog analyzer = new CtuluLog();
    final CrueIOResu<CrueData> readModele = ReadHelperForTestsBuilder.get().readModele(analyzer, FilesForTest.DC_M_40, FilesForTest.ETU40_M40_C9DH);
    testAnalyser(analyzer);
    Assert.assertNotNull(readModele.getMetier());
    testAnalyser(readModele.getAnalyse());
  }

  @Test
  public void testLectureEcritureEtu21() {
    final CtuluLog analyzer = new CtuluLog();
    final CrueIOResu<CrueData> initModele = ReadHelperForTestsBuilder.get().readModele(analyzer, FilesForTest.ETU21_M212_C9DC, FilesForTest.ETU21_M212_C9DH);
    assert (analyzer.containsSevereError());
  }

  @Test
  public void testLectureModele7() {
    final CrueIOResu<CrueData> data = readModele7();
    testModele7(data);
  }

  @Test
  public void testEcritureModele7() {
    CrueIOResu<CrueData> data = readModele7();
    final File fdc = createTempFile();
    final File fdh = createTempFile();
    final CtuluLog analyse = new CtuluLog();
    Crue9FileFormatFactory.getDCFileFormat().writeMetier(data, fdc, analyse);
    Crue9FileFormatFactory.getDHFileFormat().writeMetier(data, fdh, analyse);
    testAnalyser(analyse);
    try {
      data = ReadHelperForTestsBuilder.get().readModele(analyse, fdc.toURI().toURL(), fdh.toURI().toURL());
    } catch (final MalformedURLException e) {
      Logger.getLogger(CrueDHTest.class.getName()).log(Level.WARNING, "testEcritureModele7", e);
    }
    testAnalyser(analyse);
    testModele7(data);
  }

  private void testModele7(final CrueIOResu<CrueData> data) {
    final EMHBrancheSaintVenant findBrancheByReference = (EMHBrancheSaintVenant) data.getMetier().findBrancheByReference("b27");
    final DonCalcSansPrtBrancheSaintVenant dcsp = EMHHelper.selectFirstOfClass(findBrancheByReference.getInfosEMH(),
        DonCalcSansPrtBrancheSaintVenant.class);
    assertDoubleEquals(1.25, dcsp.getCoefRuis());
    final CalcTransBrancheSaintVenantQruis trans = EMHHelper.selectFirstOfClass(findBrancheByReference.getInfosEMH(),
        CalcTransBrancheSaintVenantQruis.class);
    assertEquals(109, trans.getLoi().getEvolutionFF().getPtEvolutionFF().size());
    final List<OrdCalc> ordCalc = data.getMetier().getScenarioData().getOrdCalcScenario().getOrdCalc();
    final Duration dureeCalc = ((PdtCst) ((OrdCalcTransIniCalcPrecedent) ordCalc.get(2)).getPdtRes()).getPdtCst();
    assertEquals(888 * 36L, dureeCalc.getMillis());
  }

  private CrueIOResu<CrueData> readModele7() {
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueIOResu<CrueData> data = ReadHelperForTestsBuilder.get().readModeleAnsSetRelationData(analyzer, FilesForTest.MODELE7_DC, FilesForTest.DH_M70);
    if (analyzer.containsSevereError()) {
      analyzer.printResume();
    }
    Assert.assertFalse(analyzer.containsSevereError());
    return data;
  }

  @Test
  public void testInclude() {

    final CtuluLog analyzer = new CtuluLog();
    final CrueIOResu<CrueData> dataGoto = readModeleCrue9(analyzer, FilesForTest.DH_M30_INCLUDE);
    testAnalyser(analyzer);
    assertEquals("ligne 1\nligne 2\nligne 3\nligne 4\nligne 5", dataGoto.getCrueCommentaire());
  }

  /**
   * Test cycle de lecture/ecriture du modele 3
   */
  @Test
  public void testLectureEcritureCRUE9Modele3() {

    final CtuluLog analyzer = new CtuluLog();
    // -- lecture --//
    final CrueIOResu<CrueData> data = readModeleCrue9(analyzer, FilesForTest.DH_M_30);

    // -- ecriture --//
    final File f = createTemptxtFile("modele3");
    writeModeleCrue9(analyzer, f, data.getMetier());

    testAnalyser(analyzer);
  }

  /**
   * Test lecture/ecriture du modele 4.
   */
  @Test
  public void testLectureEcritureCRUE9Modele4() {
    final CrueData data = testLectureUniquement(FilesForTest.MODELE4_DC, FilesForTest.DH_M40);
    // -- ecriture --//
    final File f = createTemptxtFile("modele4");
    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    writeModeleCrue9(analyzer, f, data);
    testAnalyser(analyzer);
  }

  /**
   * Test lecture des fichiers crue 10 du modele 3 et ecriture du fichier DH correspondant
   */
  @Test
  public void testLectureFichiersCrue10etEcritureCrue9() {

    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    // -- lecture DRSO --//
    final CrueData data = (CrueData) Crue10FileFormatFactory.getVersion(CrueFileType.DRSO, TestCoeurConfig.INSTANCE).read(
        FilesForTest.DRSO_V1_3, analyzer, CrueDRSOFileTest.createDefault(TestCoeurConfig.INSTANCE)).getMetier();
    testAnalyser(analyzer);

    // -- lecture DCSP --//
    Crue10FileFormatFactory.getVersion(CrueFileType.DCSP, TestCoeurConfig.INSTANCE).read(FilesForTest.DCSP_V_1_3,
        analyzer, data);
    testAnalyser(analyzer);

    // -- lecture PCAL --//
    Crue10FileFormatFactory.getVersion(CrueFileType.PCAL, TestCoeurConfig.INSTANCE).read(FilesForTest.PCAL_V1_3,
        analyzer, data);
    testAnalyser(analyzer);

    // -- lecture DPTI --//
    Crue10FileFormatFactory.getVersion(CrueFileType.DPTI, TestCoeurConfig.INSTANCE).read(FilesForTest.DPTI_V1_3,
        analyzer, data);
    testAnalyser(analyzer);

    // -- lecture PNUM --//
    Crue10FileFormatFactory.getVersion(CrueFileType.PNUM, TestCoeurConfig.INSTANCE).read(FilesForTest.PNUM_V1_3,
        analyzer, data);
    testAnalyser(analyzer);

    // -- lecture DLHY --//
    Crue10FileFormatFactory.getVersion(CrueFileType.DLHY, TestCoeurConfig.INSTANCE).read(FilesForTest.DLHY_V1_3,
        analyzer, data);
    testAnalyser(analyzer);

    // -- lecture DCLM --//
    Crue10FileFormatFactory.getVersion(CrueFileType.DCLM, TestCoeurConfig.INSTANCE).read(FilesForTest.DCLM_V_1_3,
        analyzer, data);
    testAnalyser(analyzer);

    // -- lecture OCAL --//
    Crue10FileFormatFactory.getVersion(CrueFileType.OCAL, TestCoeurConfig.INSTANCE).read(FilesForTest.MODELE3_OCAL_V_1_3,
        analyzer, data);
    testAnalyser(analyzer);

    Crue10FileFormatFactory.getVersion(CrueFileType.OPTI, TestCoeurConfig.INSTANCE).read(FilesForTest.OPTI_V1_3,
        analyzer, data);
    testAnalyser(analyzer);
    Crue10FileFormatFactory.getVersion(CrueFileType.OPTG, TestCoeurConfig.INSTANCE).read(FilesForTest.OPTG_V1_3,
        analyzer, data);
    testAnalyser(analyzer);

    // -- ecriture DH (crue9) --//
    final File f = createTemptxtFile("modele3");
    ReadHelperForTestsBuilder.get().initRelations(data);
    writeModeleCrue9(analyzer, f, data);
    testAnalyser(analyzer);
  }

  /**
   */
  @Test
  public void testLectureFichierCrue9etEcritureCrue10() {

    final CtuluLog analyzer = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);

    final CrueIOResu<CrueData> res = readModeleCrue9(analyzer, FilesForTest.DH_M_30);
    final CrueData data = res.getMetier();

    // -- ecriture DCSP --//
    final File fichierEcritureDCSP = createTemptxtFile("DCSP");
    Crue10FileFormatFactory.getVersion(CrueFileType.DCSP, TestCoeurConfig.INSTANCE).write(data, fichierEcritureDCSP,
        analyzer);

    // -- ecriture OCAL --//
    if (data.getOCAL() != null) {
      final File fichierEcritureOCAL = createTemptxtFile("OCAL");
      Crue10FileFormatFactory.getVersion(CrueFileType.OCAL, TestCoeurConfig.INSTANCE).write(data, fichierEcritureOCAL,
          analyzer);
    }

    // -- ecriture PCAL --//
    final File fichierEcriturePCAL = createTemptxtFile("PCAL");
    Crue10FileFormatFactory.getVersion(CrueFileType.PCAL, TestCoeurConfig.INSTANCE).write(data, fichierEcriturePCAL,
        analyzer);

    // -- ecriture DPTI --//
    final File fichierEcritureDPTI = createTemptxtFile("DPTI");
    Crue10FileFormatFactory.getVersion(CrueFileType.DPTI, TestCoeurConfig.INSTANCE).write(data, fichierEcritureDPTI,
        analyzer);

    // -- ecriture PNUM --//
    final File fichierEcriturePNUM = createTemptxtFile("PNUM");
    Crue10FileFormatFactory.getVersion(CrueFileType.PNUM, TestCoeurConfig.INSTANCE).write(data, fichierEcriturePNUM,
        analyzer);

    // -- ecriture DCLM --//
    final File fichierEcritureDCLM = createTemptxtFile("DCLM");
    Crue10FileFormatFactory.getVersion(CrueFileType.DCLM, TestCoeurConfig.INSTANCE).write(data, fichierEcritureDCLM,
        analyzer);

    // -- ecriture DLHY --//
    final File fichierEcritureDLHY = createTemptxtFile("DLHY");
    Crue10FileFormatFactory.getVersion(CrueFileType.DLHY, TestCoeurConfig.INSTANCE).write(data, fichierEcritureDLHY,
        analyzer);
  }

  private File createTemptxtFile(final String titre) {
    return super.createTempFile();
  }

  /**
   * Test de M3-2_c9
   */
  @Test
  public void testLectureM3_2_c9() {
    final CtuluLog log = new CtuluLog();
    final CrueIOResu<CrueData> readModele = ReadHelperForTestsBuilder.get().readModeleAnsSetRelationData(log, FilesForTest.DC_M_32, FilesForTest.DH_M_32);
    final File createTempFile = createTempFile();
    writeModeleCrue9(log, createTempFile, readModele.getMetier());
    testAnalyser(log);
  }

  /**
   * Test de ETU40_M40_E1C9_DH
   */
  @Test
  public void testLectureM40_e1c9() {
    final CtuluLog log = new CtuluLog();
    final CrueIOResu<CrueData> readModele = ReadHelperForTestsBuilder.get().readModele(log, FilesForTest.DC_M_40, FilesForTest.ETU40_M40_E1C9_DH);
    Assert.assertTrue(readModele.getAnalyse().containsErrorOrSevereError());
  }

  private void testData(final CrueData data, final boolean isApresEcriture) {

    final ParamNumModeleBase pnum = data.getPNUM();
    assertDoubleEquals(12, pnum.getFrLinInf());
    assertDoubleEquals(23, pnum.getFrLinSup());

    Assert.assertNotNull(data);

    // Vérification pcal

    final ParamCalcScenario pcal = data.getPCAL();
    Assert.assertNotNull(pcal);
    Assert.assertNull(pcal.getDateDebSce());
    final List<OrdCalc> ordCalc = data.getOCAL().getOrdCalc();
    final OrdCalcTrans ordCalcTrans = EMHHelper.selectInstanceOf(ordCalc, OrdCalcTrans.class).get(0);
    Assert.assertNotNull(ordCalcTrans.getPdtRes());
    assertEquals("P0Y0M0DT1H0M0S", DateDurationConverter.durationToXsd(((PdtCst) ordCalcTrans.getPdtRes()).getPdtCst()));

    assertEquals("P0Y0M1DT0H0M0S", DateDurationConverter.durationToXsd(ordCalcTrans.getDureeCalc()));

    // Vérification pnum

    final Pdt pdt = pnum.getParamNumCalcPseudoPerm().getPdt();
    Assert.assertTrue(pdt instanceof PdtCst);
    assertEquals("P0Y0M0DT1H0M0S", DateDurationConverter.durationToXsd(((PdtCst) pdt).getPdtCst()));
    assertDoubleEquals(0.01, pnum.getParamNumCalcPseudoPerm().getTolMaxQ());
    assertDoubleEquals(0.001, pnum.getParamNumCalcPseudoPerm().getTolMaxZ());
    assertDoubleEquals(50, pnum.getParamNumCalcPseudoPerm().getNbrPdtMax());
    assertDoubleEquals(0, pnum.getParamNumCalcPseudoPerm().getNbrPdtDecoup());
    assertEquals("P0Y0M0DT0H15M0S",
        DateDurationConverter.durationToXsd(((PdtCst) pnum.getParamNumCalcTrans().getPdt()).getPdtCst()));

    // Vérification dlhy

    final List<Loi> dlhy = data.getLois();
    Assert.assertNotNull(dlhy);
    int countLoiDF = 0;
    int countLoiFF = 0;
    for (final Loi loi : dlhy) {
      if (loi instanceof LoiDF) {
        countLoiDF++;
        if (loi.getNom().equals("HydrogrammeN1")) {
          testHydrogrammeN1(loi);
          // } else if (((LoiDF) loi).getNom().equals("HydrogrammeRuis2")) {
          // testHydrogrammeRuis2(loi);
        } else if (loi.getNom().equals("ManoeuvreB8")) {
          testManoeuvreB8(loi);
        }
      } else if (loi instanceof LoiFF) {
        countLoiFF++;
        if (loi.getNom().equals("TarrageN5")) {
          testTarrageN5(loi);
        }
      }
    }

    assertEquals(2, countLoiDF);
    assertEquals(1, countLoiFF);

    // Vérification dclm

    final DonCLimMScenario dclm = data.getConditionsLim();
    Assert.assertNotNull(dclm);

    for (final CalcPseudoPerm calculP : dclm.getCalcPseudoPerm()) {

      if (calculP.getNom().equals("CP1")) {

        final CalcPseudoPermNoeudQapp noeudNivContQapp = calculP.getDclm(CalcPseudoPermNoeudQapp.class).iterator().next();
        assertDoubleEquals(noeudNivContQapp.getQapp(), 100.0);
        CatEMHNoeud noeud = data.findNoeudByReference(noeudNivContQapp.getEmh().getNom());
        Assert.assertNotNull(noeud);
        List<DonCLimM> donclims = noeud.getDCLM();
        Assert.assertNotNull(donclims);
        boolean trouve = false;
        for (final DonCLimM donclim : donclims) {
          if (donclim instanceof CalcPseudoPermNoeudQapp) {
            trouve = true;
            assertDoubleEquals(((CalcPseudoPermNoeudQapp) donclim).getQapp(), 100.0);
            break;
          }
        }
        Assert.assertTrue(trouve);

        final CalcPseudoPermNoeudNiveauContinuZimp noeudNivContZ = calculP.getDclm(CalcPseudoPermNoeudNiveauContinuZimp.class).iterator().next();
        assertDoubleEquals(noeudNivContZ.getZimp(), 1.5);

        noeud = data.findNoeudByReference(noeudNivContZ.getEmh().getNom());
        Assert.assertNotNull(noeud);
        donclims = noeud.getDCLM();
        Assert.assertNotNull(donclims);
        trouve = false;
        for (final DonCLimM donclim : donclims) {
          if (donclim instanceof CalcPseudoPermNoeudNiveauContinuZimp) {
            trouve = true;
            assertDoubleEquals(((CalcPseudoPermNoeudNiveauContinuZimp) donclim).getZimp(), 1.5);
            break;
          }
        }
        Assert.assertTrue(trouve);

        for (final CalcPseudoPermBrancheOrificeManoeuvre brancheCast : calculP.getDclm(CalcPseudoPermBrancheOrificeManoeuvre.class)) {
          assertEquals(brancheCast.getSensOuv(), EnumSensOuv.OUV_VERS_HAUT);
          assertDoubleEquals(brancheCast.getOuv(), 100.0);

          final CatEMHBranche brancheEMH = data.findBrancheByReference(brancheCast.getEmh().getNom());
          Assert.assertNotNull(brancheEMH);
          donclims = brancheEMH.getDCLM();
          Assert.assertNotNull(donclims);
          trouve = false;
          for (final DonCLimM donclim : donclims) {
            if (donclim instanceof CalcPseudoPermBrancheOrificeManoeuvre) {
              trouve = true;
              assertEquals(((CalcPseudoPermBrancheOrificeManoeuvre) donclim).getSensOuv(), EnumSensOuv.OUV_VERS_HAUT);
              assertDoubleEquals(((CalcPseudoPermBrancheOrificeManoeuvre) donclim).getOuv(), 100.0);
              break;
            }
          }
          Assert.assertTrue(trouve);
        }
      }// Fin calcul CP1
    }// Fin for listeCalculsPermanents

    for (final CalcTrans calculT : dclm.getCalcTrans()) {

      if (calculT.getNom().equals("CT1")) {

        final CalcTransNoeudQapp noeudCastQapp = calculT.getDclm(CalcTransNoeudQapp.class).iterator().next();
        LoiDF lois = noeudCastQapp.getHydrogrammeQapp();
        Assert.assertNotNull(lois);
        assertEquals(lois.getNom(), "LoiTQapp_N1");

        CatEMHNoeud noeud = data.findNoeudByReference(noeudCastQapp.getEmh().getNom());
        Assert.assertNotNull(noeud);
        List<DonCLimM> donclims = noeud.getDCLM();
        Assert.assertNotNull(donclims);
        boolean trouve = false;
        for (final DonCLimM donclim : donclims) {
          if (donclim instanceof CalcTransNoeudQapp) {
            trouve = true;
            lois = ((CalcTransNoeudQapp) donclim).getHydrogrammeQapp();
            Assert.assertNotNull(lois);
            assertEquals(lois.getNom(), "LoiTQapp_N1");
            assertEquals(lois.getEvolutionFF().getPtEvolutionFF().size(), 17);
            break;
          }
        }
        Assert.assertTrue(trouve);

        final CalcTransNoeudNiveauContinuTarage noeudCast = calculT.getDclm(CalcTransNoeudNiveauContinuTarage.class).iterator().next();
        LoiFF loiTarrage = noeudCast.getTarage();
        Assert.assertNotNull(loiTarrage);
        assertEquals(loiTarrage.getNom(), "LoiQZ_N5");

        noeud = data.findNoeudByReference(noeudCast.getEmh().getNom());
        Assert.assertNotNull(noeud);
        donclims = noeud.getDCLM();
        Assert.assertNotNull(donclims);
        trouve = false;
        for (final DonCLimM donclim : donclims) {
          if (donclim instanceof CalcTransNoeudNiveauContinuTarage) {
            trouve = true;
            loiTarrage = ((CalcTransNoeudNiveauContinuTarage) donclim).getTarage();
            Assert.assertNotNull(loiTarrage);
            assertEquals(loiTarrage.getNom(), "LoiQZ_N5");
            assertEquals(loiTarrage.getEvolutionFF().getPtEvolutionFF().size(), 10);
            break;
          }
        }
        Assert.assertTrue(trouve);

        for (final CalcTransBrancheOrificeManoeuvre brancheCast : calculT.getDclm(CalcTransBrancheOrificeManoeuvre.class)) {

          assertEquals(brancheCast.getSensOuv(), EnumSensOuv.OUV_VERS_HAUT);
          LoiDF loiManoeuvre = brancheCast.getManoeuvre();
          Assert.assertNotNull(loiManoeuvre);
          assertEquals(loiManoeuvre.getNom(), "LoiTOuv_B8");

          final CatEMHBranche br = data.findBrancheByReference(brancheCast.getEmh().getNom());
          Assert.assertNotNull(br);
          donclims = br.getDCLM();
          Assert.assertNotNull(donclims);
          trouve = false;
          for (final DonCLimM donclim : donclims) {
            if (donclim instanceof CalcTransBrancheOrificeManoeuvre) {
              trouve = true;
              assertEquals(((CalcTransBrancheOrificeManoeuvre) donclim).getSensOuv(), EnumSensOuv.OUV_VERS_HAUT);
              loiManoeuvre = ((CalcTransBrancheOrificeManoeuvre) donclim).getManoeuvre();
              Assert.assertNotNull(loiManoeuvre);
              assertEquals(loiManoeuvre.getNom(), "LoiTOuv_B8");
              assertEquals(loiManoeuvre.getEvolutionFF().getPtEvolutionFF().size(), 4);
              break;
            }
          }
          Assert.assertTrue(trouve);
        }
      }
    }

    // Vérification DPTI

    final DonPrtCIniBranche brancheB1 = (DonPrtCIniBranche) EMHHelper.collectDPTI(data.findBrancheByReference("B1")).get(0);
    assertEquals(100.0, brancheB1.getQini(), 0.001);
    // -- test B4 <Qini>98.7</Qini> --//
    final DonPrtCIniBranche brancheSVB4 = (DonPrtCIniBranche) EMHHelper.collectDPTI(data.findBrancheByReference("B4")).get(0);
    assertEquals(100.0, brancheSVB4.getQini(), 0.001);
    final DonPrtCIniBranche brancheSVB5 = (DonPrtCIniBranche) EMHHelper.collectDPTI(data.findBrancheByReference("B5")).get(0);
    assertEquals(0.0, brancheSVB5.getQini(), 0.001);
    final DonPrtCIniBranche brancheSVB8 = (DonPrtCIniBranche) EMHHelper.collectDPTI(data.findBrancheByReference("B8")).get(0);
    assertEquals(0.0, brancheSVB8.getQini(), 0.001);

    // -- test de N1 <Zini>3.676</Zini> --//
    assertEquals(3.676,
        ((DonPrtCIniNoeudNiveauContinu) EMHHelper.collectDPTI(data.findNoeudByReference("N1")).get(0)).getZini(), 0.001);
    assertEquals(3.222,
        ((DonPrtCIniNoeudNiveauContinu) EMHHelper.collectDPTI(data.findNoeudByReference("N2")).get(0)).getZini(), 0.001);
    assertEquals(2.779,
        ((DonPrtCIniNoeudNiveauContinu) EMHHelper.collectDPTI(data.findNoeudByReference("N3")).get(0)).getZini(), 0.001);
    assertEquals(2.624,
        ((DonPrtCIniNoeudNiveauContinu) EMHHelper.collectDPTI(data.findNoeudByReference("N4")).get(0)).getZini(), 0.001);
    assertEquals(2.000,
        ((DonPrtCIniNoeudNiveauContinu) EMHHelper.collectDPTI(data.findNoeudByReference("N5")).get(0)).getZini(), 0.001);
    assertEquals(2.926,
        ((DonPrtCIniNoeudNiveauContinu) EMHHelper.collectDPTI(data.findNoeudByReference("N6")).get(0)).getZini(), 0.001);
    assertEquals(2.926,
        ((DonPrtCIniNoeudNiveauContinu) EMHHelper.collectDPTI(data.findNoeudByReference("N7")).get(0)).getZini(), 0.001);

    // Verification de DCSP

    final EMHBrancheSaintVenant brancheSV = (EMHBrancheSaintVenant) data.findBrancheByReference("B1");
    controleRuisBrancheSV(brancheSV, isApresEcriture);
    final EMHBrancheSaintVenant brancheSV2 = (EMHBrancheSaintVenant) data.findBrancheByReference("B2");
    controleRuisBrancheSV(brancheSV2, isApresEcriture);
    final EMHBrancheSaintVenant brancheSV4 = (EMHBrancheSaintVenant) data.findBrancheByReference("B4");
    controleRuisBrancheSV(brancheSV4, isApresEcriture);

    final EMHBrancheSeuilTransversal brancheTransversal = (EMHBrancheSeuilTransversal) data.findBrancheByReference("B3");
    Assert.assertNotNull(brancheTransversal);

    final List<DonCalcSansPrt> listDcsps = brancheTransversal.getDCSP();
    if (listDcsps.size() > 0) {
      final DonCalcSansPrtBrancheSeuilTransversal donnee2 = (DonCalcSansPrtBrancheSeuilTransversal) listDcsps.get(0);
      Assert.assertNotNull(donnee2);
      assertEquals(EnumFormulePdc.BORDA, donnee2.getFormulePdc());
      assertDoubleEquals(20.0, donnee2.getElemSeuilAvecPdc().get(0).getLargeur());
      assertDoubleEquals(0.60, donnee2.getElemSeuilAvecPdc().get(0).getZseuil());
      assertDoubleEquals(0.90, donnee2.getElemSeuilAvecPdc().get(0).getCoefD());
      assertDoubleEquals(1.00, donnee2.getElemSeuilAvecPdc().get(0).getCoefPdc());
      assertDoubleEquals(2.00, donnee2.getElemSeuilAvecPdc().get(1).getCoefPdc());
    }
  }

  /**
   * @param brancheSV
   */
  private void controleRuisBrancheSV(final EMHBrancheSaintVenant brancheSV, final boolean isApresEcriture) {

    Assert.assertNotNull(brancheSV);
    final List<DonCalcSansPrt> listDcsps = brancheSV.getDCSP();
    if (listDcsps.size() > 0) {
      final DonCalcSansPrtBrancheSaintVenant donnee = (DonCalcSansPrtBrancheSaintVenant) listDcsps.get(0);
      Assert.assertNotNull(donnee);
      assertDoubleEquals(1.0, donnee.getCoefBeta());
      assertDoubleEquals(0.0, donnee.getCoefRuisQdm());
      if (!isApresEcriture) {
        assertDoubleEquals(0.0, donnee.getCoefRuis());
      } else {
        assertDoubleEquals(1.0, donnee.getCoefRuis());
      }
    }
  }

  /**
   * @param loi
   */
  private void testTarrageN5(final Loi loi) {
    final LoiFF loiTarrage = (LoiFF) loi;
    assertEquals(loiTarrage.getCommentaire(), "TarrageN5");
    assertEquals(EnumTypeLoi.LoiQDz, loiTarrage.getType());
    final EvolutionFF evolFF = loiTarrage.getEvolutionFF();
    Assert.assertNotNull(evolFF);
    int countPoint = 0;
    for (final PtEvolutionFF ptEvolFF : evolFF.getPtEvolutionFF()) {
      if (countPoint == 0) {
        assertDoubleEquals(ptEvolFF.getAbscisse(), 1.0);
        assertDoubleEquals(ptEvolFF.getOrdonnee(), 0.0);
      } else if (countPoint == 3) {
        assertDoubleEquals(ptEvolFF.getAbscisse(), 2.5);
        assertDoubleEquals(ptEvolFF.getOrdonnee(), -175.0);
      } else if (countPoint == 9) {
        assertDoubleEquals(ptEvolFF.getAbscisse(), 5.5);
        assertDoubleEquals(ptEvolFF.getOrdonnee(), -500.0);
      }

      countPoint++;
    }
  }

  /**
   * @param loi
   */
  private void testManoeuvreB8(final Loi loi) {
    final LoiDF loiVanne = (LoiDF) loi;
    assertEquals(loiVanne.getCommentaire(), "ManoeuvreB8");
    assertEquals(EnumTypeLoi.LoiTOuv, loiVanne.getType());
    final EvolutionFF evolDF = loiVanne.getEvolutionFF();
    Assert.assertNotNull(evolDF);
    int countPoint = 0;
    for (final PtEvolutionFF ptEvolDF : evolDF.getPtEvolutionFF()) {
      if (countPoint == 0) {
        assertDoubleEquals(ptEvolDF.getAbscisse(), 0);
        assertDoubleEquals(ptEvolDF.getOrdonnee(), 90.0);
      } else if (countPoint == 3) {
        assertDoubleEquals(ptEvolDF.getAbscisse(), 86400);
        assertDoubleEquals(ptEvolDF.getOrdonnee(), 100.0);
      }

      countPoint++;
    }
  }

  /**
   * @param loi
   */
  private void testHydrogrammeN1(final Loi loi) {
    final LoiDF loiHydrogramme = (LoiDF) loi;
    assertEquals(loiHydrogramme.getCommentaire(), "HydrogrammeN1");
    assertEquals(EnumTypeLoi.LoiTQapp, loiHydrogramme.getType());
    final EvolutionFF evolDF = loiHydrogramme.getEvolutionFF();
    Assert.assertNotNull(evolDF);
    int countPoint = 0;
    for (final PtEvolutionFF ptEvolDF : evolDF.getPtEvolutionFF()) {
      if (countPoint == 0) {
        assertDoubleEquals(0, ptEvolDF.getAbscisse());
        assertDoubleEquals(100.0, ptEvolDF.getOrdonnee());
      } else if (countPoint == 6) {
        assertDoubleEquals(32400, ptEvolDF.getAbscisse());
        assertEquals(450.0, ptEvolDF.getOrdonnee());
      } else if (countPoint == 16) {
        assertDoubleEquals(86400, ptEvolDF.getAbscisse());
        assertDoubleEquals(100.0, ptEvolDF.getOrdonnee());
      }

      countPoint++;
    }
  }

  /**
   * @param analyzer
   * @param path
   */
  private CrueIOResu<CrueData> readModeleCrue9(final CtuluLog analyzer, final String path) {
    final CrueIOResu<CrueData> read = ReadHelperForTestsBuilder.get().readModeleAnsSetRelationData(analyzer, FilesForTest.MODELE3_DC, path);
    testAnalyser(analyzer);
    return read;
  }

  @Test
  public void testLectureUniquement() {
    testLectureUniquement(FilesForTest.MODELE4_DC, FilesForTest.DH_M40);
    testLectureUniquement(FilesForTest.MODELE5_DC, FilesForTest.DH_M_50);
    testLectureUniquement(FilesForTest.MODELE6_DC, FilesForTest.DH_M_60);
  }

  /**
   * @param dc
   * @param dh
   */
  public CrueData testLectureUniquement(final String dc, final String dh) {
    final CtuluLog analyze = new CtuluLog();
    final CrueIOResu<CrueData> read = ReadHelperForTestsBuilder.get().readModeleAnsSetRelationData(analyze, dc, dh);
    testAnalyser(analyze);
    return read.getMetier();
  }
}
