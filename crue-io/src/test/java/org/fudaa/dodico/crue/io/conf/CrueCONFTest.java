package org.fudaa.dodico.crue.io.conf;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;
import org.fudaa.dodico.crue.config.coeur.CoeurManager;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.projet.conf.*;
import org.fudaa.dodico.crue.projet.conf.Option;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.*;

/**
 * @author CANEL Christophe
 */
public class CrueCONFTest extends AbstractIOParentTest {
  public CrueCONFTest() {
    super(FilesForTest.CONFIG_SITE);
  }

  @Test
  public void testSiteLecture() {
    final File coeurFile = extractSiteFile();
    final Configuration config = read(FilesForTest.CONFIG_SITE);
    assertCorrect(config, coeurFile.getParentFile());
  }

  @Test
  public void testSiteUser() {
    final Configuration config = read(FilesForTest.CONFIG_USER);
    assertUserCorrect(config.getUser());
    assertNull(config.getSite());
  }

  @Test
  public void testSiteOfficelLecture() {
    final Configuration config = read(FilesForTest.CONFIG_SITE_OFFICIEL);
    assertNotNull(config);
  }

  private static void assertCorrect(final Configuration config, final File baseDir) {
    assertSiteCorrect(config.getSite(), baseDir);
    assertUserCorrect(config.getUser());
  }

  private static void assertSiteCorrect(final SiteConfiguration config, final File baseDir) {
    assertCorrect(config.getCoeurs(), baseDir);

    final List<SiteOption> options = config.getOptions();

    // 9 options attendues
    assertEquals(9, options.size());

    // Option availableLanguage
    SiteOption option = options.get(0);
    assertEquals("availableLanguage", option.getId());
    assertEquals("Langues disponibles", option.getCommentaire());
    assertEquals("fr_FR", option.getValeur());
    assertTrue(option.isUserVisible());

    // Option maxComparisonItemsDisplayed
    option = options.get(1);
    assertEquals("maxComparisonItemsDisplayed", option.getId());
    assertEquals("Nombre maximal de différences affichées", option.getCommentaire());
    assertEquals("1000", option.getValeur());
    assertTrue(option.isUserVisible());

    // Option maxLogLinesRead
    option = options.get(2);
    assertEquals("maxLogLinesRead", option.getId());
    assertEquals("Nombre maximal de lignes lues dans un CR", option.getCommentaire());
    assertEquals("5000", option.getValeur());
    assertTrue(option.isUserVisible());

    // Option maxComparisonResultsByEMH
    option = options.get(3);
    assertEquals("maxComparisonResultsByEMH", option.getId());
    assertEquals("Nombre maximal de différences sur les résultats de calcul par EMH", option.getCommentaire());
    assertEquals("500", option.getValeur());
    assertTrue(option.isUserVisible());

    // Option crue10.rptr.exeOption
    option = options.get(4);
    assertEquals("crue10.rptr.exeOption", option.getId());
    assertEquals("Crue 10: option pour demande de pre-traitement réseau.", option.getCommentaire());
    assertEquals("-r", option.getValeur());
    assertTrue(option.isUserVisible());

    // Option crue10.rptg.exeOption
    option = options.get(5);
    assertEquals("crue10.rptg.exeOption", option.getId());
    assertEquals("Crue 10: Demande de pre-traitement de la géometrie.", option.getCommentaire());
    assertEquals("-g", option.getValeur());
    assertTrue(option.isUserVisible());

    // Option crue10.rpti.exeOption
    option = options.get(6);
    assertEquals("crue10.rpti.exeOption", option.getId());
    assertEquals("Crue 10: Demande de pre-traitement des conditions initiales.", option.getCommentaire());
    assertEquals("-i", option.getValeur());
    assertTrue(option.isUserVisible());

    // Option crue10.rcal.exeOption
    option = options.get(7);
    assertEquals("crue10.rcal.exeOption", option.getId());
    assertEquals("Crue 10: Demande de calculs.", option.getCommentaire());
    assertEquals("-c", option.getValeur());
    assertTrue(option.isUserVisible());

    // Option nbExportCrue9SignificantFigures
    option = options.get(8);
    assertEquals("nbExportCrue9SignificantFigures", option.getId());
    assertEquals("Nombre de chiffres significatifs pour les exports Crue9", option.getCommentaire());
    assertEquals("8", option.getValeur());
    assertTrue(option.isUserVisible());

    // Bloc Aide
    final SiteAide aide = config.getAide();
    assertEquals("Aide Flare", aide.getCommentaire());
    assertFalse(aide.getSyDocActivation());
    assertEquals("aide/%1$s/SyDoC_HTML/Default.htm#cshid=", aide.getCheminBase());
  }

  private static void assertCorrect(final List<CoeurConfig> coeurs, final File baseDir) {
    final CoeurManager coeurManager = new CoeurManager(baseDir, coeurs);
    assertEquals("1.2",coeurManager.getHigherGrammaire());
    // 2 Coeurs attendus : c10m10, old_c10m10
    assertEquals(2, coeurs.size());

    // Premier coeur : c10m10
    CoeurConfig coeur = coeurs.get(0);
    assertEquals("c10m10", coeur.getName());
    assertEquals("Coeur c10m10 le plus récent", coeur.getComment());
    assertFalse(coeur.isCrue9Dependant());
    assertEquals("1.2", coeur.getXsdVersion());
    assertEquals(new File(baseDir, "coeurs/c10m10").getAbsolutePath(), coeur.getCoeurFolder().getAbsolutePath());
    assertTrue(coeur.isUsedByDefault());

    // Deuxième coeur : old_c10m10
    coeur = coeurs.get(1);
    assertEquals("old_c10m10", coeur.getName());
    assertEquals("Coeur c10m10 précédent", coeur.getComment());
    assertEquals("1.2", coeur.getXsdVersion());
    assertEquals(new File(baseDir, "coeurs/old_c10m10").getAbsolutePath(), coeur.getCoeurFolder().getAbsolutePath());
    assertFalse(coeur.isUsedByDefault());
  }

  private File extractSiteFile() {
    final URL url = CrueCONFTest.class.getResource(FilesForTest.CONFIG_SITE);
    final File confFile = new File(createTempDir(), "FudaaCrue_Site.xml");
    try {
      CtuluLibFile.copyStream(url.openStream(), new FileOutputStream(confFile), true, true);
    } catch (final Exception e) {
      Logger.getLogger(CrueCONFTest.class.getName()).log(Level.SEVERE, "erreur while extracting FudaaCrue_Site.xml", e);
      fail(e.getMessage());
    }

    return confFile;
  }

  /**
   * @param config the config
   */
  private static void assertUserCorrect(final UserConfiguration config) {
    final Collection<UserOption> options = config.getOptions();
    final Iterator<UserOption> iterator = options.iterator();

    Option option = iterator.next();
    assertEquals("userLanguage", option.getId());
    assertEquals("Langue choisie pour l'utilisateur", option.getCommentaire());
    assertEquals("fr_FR", option.getValeur());

    option = iterator.next();
    assertEquals("uiUpdateProgressFrequence", option.getId());
    assertEquals("Fréquence de rafraîchissement en seconde de l'interface pendant le déroulement d'un run", option.getCommentaire());
    assertEquals("0.2", option.getValeur());

    option = iterator.next();
    assertEquals("externalEditor", option.getId());
    assertEquals("Chemin ou commande vers l'éditeur externe", option.getCommentaire());
    assertEquals("C:\\Program Files (x86)\\Notepad++\\notepad++.exe", option.getValeur());

    option = iterator.next();
    assertEquals("crue9Exe", option.getId());
    assertEquals("Chemin ou commande vers l'exécutable Crue 9", option.getCommentaire());
    assertEquals("Q:\\Qualif_Exec\\Crue9\\crue9.exe", option.getValeur());
  }

  @Test
  public void testUserEcriture() {
    File newConfFile = null;

    try {
      newConfFile = File.createTempFile("Test", "CrueCONF");
    } catch (final IOException e) {
      Logger.getLogger(CrueCONFTest.class.getName()).log(Level.SEVERE, "can't create temp file", e);
      fail(e.getMessage());
    }

    Configuration config = read(FilesForTest.CONFIG_USER);

    final CrueCONFReaderWriter writer = new CrueCONFReaderWriter("1.2");
    final CrueIOResu<Configuration> resu = new CrueIOResu<>(config);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);

    assertTrue(writer.writeXMLMetier(resu, newConfFile, log));

    testAnalyser(log);

    config = read(newConfFile);
    assertUserCorrect(config.getUser());
    assertNull(config.getSite());
    if (newConfFile != null) {
      newConfFile.delete();
    }
  }

  private static Configuration read(final String resource) {
    final CrueCONFReaderWriter reader = new CrueCONFReaderWriter("1.2");
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueIOResu<Configuration> result = reader.readXML(resource, log);

    testAnalyser(log);

    return result.getMetier();
  }

  private static Configuration read(final File resource) {
    final CrueCONFReaderWriter reader = new CrueCONFReaderWriter("1.2");
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueIOResu<Configuration> result = reader.readXML(resource, log);

    testAnalyser(log);

    return result.getMetier();
  }

  @Override
  public void testValide() {
  }
}
