/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.io.res;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.io.rdao.CommonResDao;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.VariableResDao;
import org.fudaa.dodico.crue.io.res.ResVariablesAnalyser.Result;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class VariablesResAnalyserTest {

  private final String variableResDao1 = "variableResDao1";
  final String variableResLoiNbrPtDao1 = "variableResLoiNbrPtDao1";
  final String variableResLoiNbrPtDao2 = "variableResLoiNbrPtDao2";
  private final String variableResDao2 = "variableResDao2";
  private final String variableResLoiDao1 = "variableResLoiDao1";
  private final String variableResLoiDao2 = "variableResLoiDao2";

  public VariablesResAnalyserTest() {
  }

  @Test
  public void testExtractResult() {
    ResVariablesContent extractResult = buildResult();
    testAllResult(extractResult);
  }

  @Test
  public void testExtractResultDoublon() {
    List<VariableResDao> listWithDoublon = createListOfVariables();
    listWithDoublon.add(new CommonResDao.VariableResDao(variableResDao1));
    listWithDoublon.add(new CommonResDao.VariableResDao(variableResDao2));
    ResVariablesAnalyser analyser = new ResVariablesAnalyser();
    Result extractResult = analyser.extractResult(listWithDoublon);
    assertEquals(2, extractResult.varEnDoublon.size());
    List<String> doublons = new ArrayList<>(extractResult.varEnDoublon);
    Collections.sort(doublons);
    assertEquals(variableResDao1, doublons.get(0));
    assertEquals(variableResDao2, doublons.get(1));
  }

  @Test
  public void testEmpty() {
    ResVariablesContent extractResult = new ResVariablesContent();
    assertTrue(extractResult.isEmpty());
    assertFalse(extractResult.containsVariable());
    assertFalse(extractResult.containsVariableResLoiNbrPtDao());
    assertFalse(extractResult.isVariableDefined(variableResDao1));
  }

  @Test
  public void testClone() {
    ResVariablesContent extractResult = buildResult();
    final ResVariablesContent clone = extractResult.clone();
    testAllResult(clone);
    assertFalse(extractResult.loiNbrPtById == clone.loiNbrPtById);
    assertFalse(extractResult.definedVariable == clone.definedVariable);
    assertFalse(extractResult.variables == clone.variables);
  }

  @Test
  public void testMergeWithParentModifyLoiNbrPt() {
    ResVariablesContent extractResult = buildResult();
    List<CommonResDao.VariableResDao> in = new ArrayList<>();
    in.add(new CommonResDao.VariableResLoiNbrPtDao(variableResLoiNbrPtDao1, 11));
    in.add(new CommonResDao.VariableResLoiNbrPtDao(variableResLoiNbrPtDao2, 22));
    ResVariablesAnalyser analyser = new ResVariablesAnalyser();
    ResVariablesContent mergeWithParent = analyser.mergeWithParent(in, extractResult).content;
    testOnlyVariables(mergeWithParent);
    assertEquals(11, mergeWithParent.getLoiNbrPt(variableResLoiNbrPtDao1));
    assertEquals(22, mergeWithParent.getLoiNbrPt(variableResLoiNbrPtDao2));
  }

  @Test
  public void testMergeWithParent() {
    ResVariablesContent extractResult = buildResult();
    List<CommonResDao.VariableResDao> in = new ArrayList<>();
    in.add(new CommonResDao.VariableResLoiNbrPtDao(variableResLoiNbrPtDao2, 22));
    in.add(new CommonResDao.VariableResDao(variableResDao1));
    ResVariablesAnalyser analyser = new ResVariablesAnalyser();
    final Result mergeWithParentResult = analyser.mergeWithParent(in, extractResult);
    //un doublon détecté:
    assertEquals(1, mergeWithParentResult.varEnDoublon.size());
    assertEquals(variableResDao1, mergeWithParentResult.varEnDoublon.iterator().next());
    ResVariablesContent mergeWithParent = mergeWithParentResult.content;
    assertEquals(5, mergeWithParent.getNbVariables());
    assertEquals(variableResDao1, mergeWithParent.getVariable(0).getId());
    assertEquals(StringUtils.capitalize(variableResDao1), mergeWithParent.getVariable(0).getNomRef());
    assertEquals(variableResLoiDao1, mergeWithParent.getVariable(1).getId());
    assertEquals(variableResDao2, mergeWithParent.getVariable(2).getId());
    assertEquals(variableResLoiDao2, mergeWithParent.getVariable(3).getId());
    assertEquals(variableResDao1, mergeWithParent.getVariable(4).getId());
    assertEquals(1, mergeWithParent.getLoiNbrPt(variableResLoiNbrPtDao1));
    assertEquals(22, mergeWithParent.getLoiNbrPt(variableResLoiNbrPtDao2));
  }

  @Test
  public void testMergeWithParentEmpty() {
    ResVariablesContent extractResult = buildResult();
    List<CommonResDao.VariableResDao> in = new ArrayList<>();
    ResVariablesAnalyser analyser = new ResVariablesAnalyser();
    ResVariablesContent mergeWithParent = analyser.mergeWithParent(in, extractResult).content;
    testAllResult(mergeWithParent);
    assertFalse(mergeWithParent == extractResult);

    ResVariablesContent empty = new ResVariablesContent();
    mergeWithParent = analyser.mergeWithParent(createListOfVariables(), empty).content;
    assertFalse(mergeWithParent == empty);
    testAllResult(mergeWithParent);
  }

  @Test
  public void testAddAll() {
    ResVariablesContent extractResult = buildResult();
    List<CommonResDao.VariableResDao> in = new ArrayList<>();
    in.add(new CommonResDao.VariableResLoiNbrPtDao(variableResLoiNbrPtDao1, 10));
    in.add(new CommonResDao.VariableResLoiNbrPtDao(variableResLoiNbrPtDao2, 20));
    ResVariablesAnalyser analyser = new ResVariablesAnalyser();
    ResVariablesContent newExtract = analyser.extractResult(in).content;
    extractResult.addAll(newExtract);
    testOnlyVariables(extractResult);
    assertEquals(10, extractResult.getLoiNbrPt(variableResLoiNbrPtDao1));
    assertEquals(20, extractResult.getLoiNbrPt(variableResLoiNbrPtDao2));

    //avec une nouvelle valeur:
    String newLoiNbrRef = "New";
    in.add(new CommonResDao.VariableResLoiNbrPtDao(newLoiNbrRef, 100));


    extractResult = buildResult();
    newExtract = analyser.extractResult(in).content;
    extractResult.addAll(newExtract);
    assertEquals(10, extractResult.getLoiNbrPt(variableResLoiNbrPtDao1));
    assertEquals(20, extractResult.getLoiNbrPt(variableResLoiNbrPtDao2));
    assertEquals(100, extractResult.getLoiNbrPt(StringUtils.uncapitalize(newLoiNbrRef)));



  }

  private ResVariablesContent buildResult() {
    List<VariableResDao> in = createListOfVariables();
    ResVariablesAnalyser analyser = new ResVariablesAnalyser();
    ResVariablesContent extractResult = analyser.extractResult(in).content;
    return extractResult;
  }

  private List<VariableResDao> createListOfVariables() {
    List<CommonResDao.VariableResDao> in = new ArrayList<>();
    in.add(new CommonResDao.VariableResDao(StringUtils.capitalize(variableResDao1)));
    in.add(new CommonResDao.VariableResLoiDao(StringUtils.capitalize(variableResLoiDao1)));
    in.add(new CommonResDao.VariableResLoiNbrPtDao(StringUtils.capitalize(variableResLoiNbrPtDao1), 1));
    in.add(new CommonResDao.VariableResDao(StringUtils.capitalize(variableResDao2)));
    in.add(new CommonResDao.VariableResLoiNbrPtDao(StringUtils.capitalize(variableResLoiNbrPtDao2), 2));
    in.add(new CommonResDao.VariableResLoiDao(StringUtils.capitalize(variableResLoiDao2)));
    return in;
  }

  private void testAllResult(ResVariablesContent extractResult) {
    testNbrPtLoi(extractResult);
    testOnlyVariables(extractResult);
  }

  private void testOnlyVariables(ResVariablesContent extractResult) {
    assertTrue(extractResult.containsVariable());
    assertTrue(extractResult.isVariableDefined(variableResDao1));
    assertTrue(extractResult.isVariableDefined(variableResDao2));
    assertTrue(extractResult.isVariableDefined(variableResLoiDao1));
    assertTrue(extractResult.isVariableDefined(variableResLoiDao2));
    assertEquals(variableResDao1, extractResult.getVariable(0).getId());
    assertEquals(StringUtils.capitalize(variableResDao1), extractResult.getVariable(0).getNomRef());
    assertEquals(variableResLoiDao1, extractResult.getVariable(1).getId());
    assertEquals(variableResDao2, extractResult.getVariable(2).getId());
    assertEquals(variableResLoiDao2, extractResult.getVariable(3).getId());
  }

  @Test
  public void testExtractResultNoVariable() {
    List<CommonResDao.VariableResDao> in = new ArrayList<>();

    in.add(new CommonResDao.VariableResLoiNbrPtDao(variableResLoiNbrPtDao1, 1));
    in.add(new CommonResDao.VariableResLoiNbrPtDao(variableResLoiNbrPtDao2, 2));
    ResVariablesAnalyser analyser = new ResVariablesAnalyser();
    ResVariablesContent extractResult = analyser.extractResult(in).content;
    assertFalse(extractResult.containsVariable());
    testNbrPtLoi(extractResult);

  }

  private void testNbrPtLoi(ResVariablesContent extractResult) {
    assertTrue(extractResult.isLoiNbrPtDaoDefined(variableResLoiNbrPtDao1));
    assertEquals(1, extractResult.getLoiNbrPt(variableResLoiNbrPtDao1));
    assertTrue(extractResult.isLoiNbrPtDaoDefined(variableResLoiNbrPtDao2));
    assertEquals(2, extractResult.getLoiNbrPt(variableResLoiNbrPtDao2));
  }
}
