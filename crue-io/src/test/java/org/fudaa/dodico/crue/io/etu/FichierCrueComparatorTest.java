package org.fudaa.dodico.crue.io.etu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class FichierCrueComparatorTest {

  @Test
  public void testComparator() {
    FichierCrueComparator comparator = new FichierCrueComparator();
    List<FichierCrue> list = new ArrayList<>();
    list.add(new FichierCrue("drso", "", CrueFileType.DRSO));
    list.add(new FichierCrue("dptg", "", CrueFileType.DPTG));
    list.add(new FichierCrue("dcsp", "", CrueFileType.DCSP));
    list.add(new FichierCrue("dptg", "", CrueFileType.DFRT));
    Collections.sort(list, comparator);
    assertEquals(CrueFileType.DRSO, list.get(0).getType());
    assertEquals(CrueFileType.DCSP, list.get(1).getType());
    assertEquals(CrueFileType.DPTG, list.get(2).getType());
    assertEquals(CrueFileType.DFRT, list.get(3).getType());
  }
}
