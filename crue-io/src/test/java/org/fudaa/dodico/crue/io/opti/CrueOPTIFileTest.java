package org.fudaa.dodico.crue.io.opti;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.SeveriteManager;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

/**
 * tests unitaires pour les opti.
 *
 * @author Adrien Hadoux
 */
public class CrueOPTIFileTest extends AbstractIOParentTest {
  public CrueOPTIFileTest() {
    super(Crue10FileFormatFactory.getVersion(CrueFileType.OPTI, TestCoeurConfig.INSTANCE), FilesForTest.OPTI_V1_3);
  }

  @Test
  public void testValideVersion_v1p1p1() {
    final CtuluLog log = new CtuluLog();
    final boolean valide = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.OPTI,
        TestCoeurConfig.getInstance(
            Crue10VersionConfig.V_1_1_1)).isValide(FilesForTest.OPTI_V1_1_1, log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    assertTrue(valide);
  }

  @Test
  public void testValideVersion_v1p2() {
    final CtuluLog log = new CtuluLog();
    final boolean valide = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.OPTI,
        TestCoeurConfig.getInstance(
            Crue10VersionConfig.V_1_2)).isValide(FilesForTest.OPTI_V1_2, log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    assertTrue(valide);
  }

  @Test
  public void testLecture() {
    final OrdPrtCIniModeleBase data = read(FilesForTest.OPTI_V1_3, TestCoeurConfig.INSTANCE);
    testSortiesTrueAndDebug(data);
    testRegle(data);
    testMethodInterpolation(data);
  }


  private void testMethodInterpolation(final OrdPrtCIniModeleBase data) {
    Assert.assertEquals(EnumMethodeInterpol.SAINT_VENANT,data.getMethodeInterpol());
    Assert.assertEquals(0.05,((ValParamDouble)data.getValParam(EnumRegleInterpolation.TOL_ST_Q.getVariableName())).getValeur(),0.01);
    Assert.assertEquals(0.02,((ValParamDouble)data.getValParam(EnumRegleInterpolation.TOL_ND_Z.getVariableName())).getValeur(),0.01);
  }
  private void testMethodInterpolationOld(final OrdPrtCIniModeleBase data) {
    Assert.assertEquals(EnumMethodeInterpol.SAINT_VENANT,data.getMethodeInterpol());
    Assert.assertEquals(0.01,((ValParamDouble)data.getValParam(EnumRegleInterpolation.TOL_ST_Q.getVariableName())).getValeur(),0.01);
    Assert.assertEquals(0.09,((ValParamDouble)data.getValParam(EnumRegleInterpolation.TOL_ND_Z.getVariableName())).getValeur(),0.01);


  }

  private void testRegle(final OrdPrtCIniModeleBase data) {
    final List<Regle> regles = data.getRegles();
    Assert.assertEquals(2, regles.size());
    Regle regle = data.getRegle(EnumRegle.REGLE_QBR_UNIFORME);
    ValParamDouble param = (ValParamDouble) regle.getValParam();
    assertDoubleEquals(0.002, param.getValeur());
    Assert.assertEquals("RegleQbrUniforme", regle.getNom());
    Assert.assertEquals("pm_QbrUniforme", param.getNom());

    regle = data.getRegle(EnumRegle.REGLE_QND);
    param = (ValParamDouble) regle.getValParam();
    assertDoubleEquals(0.003, param.getValeur());
    Assert.assertEquals("RegleQnd", regle.getNom());
    Assert.assertEquals("pm_Qnd", param.getNom());
  }

  @Test
  public void testLecture_v1p1p1() {
    final OrdPrtCIniModeleBase data = read(FilesForTest.OPTI_V1_1_1,
        TestCoeurConfig.getInstance(Crue10VersionConfig.V_1_1_1));
    testMethodInterpolationOld(data);
  }

  @Test
  public void testLecture_v1p2() {
    final OrdPrtCIniModeleBase data = read(FilesForTest.OPTI_V1_2,
        TestCoeurConfig.getInstance(Crue10VersionConfig.V_1_2));
    testMethodInterpolationOld(data);
  }

  public OrdPrtCIniModeleBase read(final String file, final CoeurConfigContrat version) {
    final CtuluLog analyzer = new CtuluLog();
    final OrdPrtCIniModeleBase data = (OrdPrtCIniModeleBase) Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.OPTI,
        version).read(
        file, analyzer, createDefault(version)).getMetier();
    testAnalyser(analyzer);
    return data;
  }


  private void testSortiesTrueAndDebug(final OrdPrtCIniModeleBase data) {
    assertTrue(data.getSorties().getResultat().getSortieFichier());
    assertTrue(data.getSorties().getAvancement().getSortieFichier());
    assertTrue(data.getSorties().getTrace().getSortieFichier());
    assertTrue(data.getSorties().getTrace().getSortieEcran());
    assertEquals(data.getSorties().getTrace().getVerbositeEcran(), SeveriteManager.DEBUG3);
    assertEquals(data.getSorties().getTrace().getVerbositeFichier(), SeveriteManager.DEBUG3);
  }

  private void testSortiesTrueAndInfo(final OrdPrtCIniModeleBase data) {
    assertTrue(data.getSorties().getResultat().getSortieFichier());
    assertTrue(data.getSorties().getAvancement().getSortieFichier());
    assertTrue(data.getSorties().getTrace().getSortieFichier());
    assertTrue(data.getSorties().getTrace().getSortieEcran());
    assertEquals(data.getSorties().getTrace().getVerbositeEcran(), SeveriteManager.INFO);
    assertEquals(data.getSorties().getTrace().getVerbositeFichier(), SeveriteManager.INFO);
  }

  @Test
  public void testEcriture() {
    testEcriture(FilesForTest.OPTI_V1_3, TestCoeurConfig.INSTANCE);
  }

  public void testEcriture(final String file, final CoeurConfigContrat version) {
    final CtuluLog analyzer = new CtuluLog();
    final File f = createTempFile();
    final boolean res = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.OPTI, version).writeMetierDirect(read(
        file, version), f, analyzer, version.getCrueConfigMetier());
    testAnalyser(analyzer);
    Assert.assertFalse(analyzer.containsErrors());
    final OrdPrtCIniModeleBase read = (OrdPrtCIniModeleBase) Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.OPTI,
        version).read(f,
        analyzer,
        createDefault(version)).getMetier();
    if (version.getXsdVersion().equals(Crue10VersionConfig.V_1_1_1)) {
      testSortiesTrueAndInfo(read);
    } else {
      testSortiesTrueAndDebug(read);
      testRegle(read);
      if (Crue10VersionConfig.isUpperThan_V1_2(version.getXsdVersion())) {
        testMethodInterpolation(read);
      }
      else{
        testMethodInterpolationOld(read);
      }
    }
  }

  @Test
  public void testEcriture_v1p1p1() {
    testEcriture(FilesForTest.OPTI_V1_1_1, TestCoeurConfig.INSTANCE_1_1_1);
  }

  @Test
  public void testEcriture_v1p2() {
    testEcriture(FilesForTest.OPTI_V1_2, TestCoeurConfig.INSTANCE_1_2);
  }
}
