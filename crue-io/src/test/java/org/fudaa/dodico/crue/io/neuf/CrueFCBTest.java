package org.fudaa.dodico.crue.io.neuf;

import files.FilesForTest;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.AbstractIOBinaryTestParent;
import org.fudaa.dodico.crue.io.Crue9FileFormatFactory;
import org.fudaa.dodico.crue.io.ReadHelperForTestsBuilder;
import org.fudaa.dodico.crue.io.neuf.FCBSequentialReader.EnteteContainer;
import org.fudaa.dodico.crue.io.neuf.FCBValueObject.EnteteBranche;
import org.fudaa.dodico.crue.io.neuf.FCBValueObject.EnteteCasier;
import org.fudaa.dodico.crue.io.neuf.FCBValueObject.ResBranche;
import org.fudaa.dodico.crue.io.neuf.FCBValueObject.ResCasier;
import org.fudaa.dodico.crue.metier.CrueData;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test FCB
 *
 * @author Adrien Hadoux
 */
public class CrueFCBTest extends AbstractIOBinaryTestParent {
  @Test
  public void testLectureFCBModele30() {
    final CtuluLog log = new CtuluLog();
    final CrueData data = ReadHelperForTestsBuilder.get().readModele(log, FilesForTest.DC_ETU30_M30, FilesForTest.DH_ETU30_M30).getMetier();
    final CrueIOResu<FCBSequentialReader> read = readBinaire(Crue9FileFormatFactory.getFCBFileFormat(), FilesForTest.FCB_MODELE30, data);
    Assert.assertNotNull(read);
    final FCBSequentialReader infos = read.getMetier();
    // test des profils
    Assert.assertEquals(26, infos.getNbProfils());
    data.setSto(readBinaire(Crue9FileFormatFactory.getSTOFileFormat(), FilesForTest.STO_ETU30_M30, data).getMetier());
    // tests des sections sans géometrie qui peuvent causer problèmes:
    final EnteteContainer<EnteteBranche, ResBranche> containerBranches = infos.getContainerBranches();
    for (int i = 0; i < containerBranches.getNbData(); i++) {
      final String name = containerBranches.getDataName(i);
      Assert.assertFalse(StringUtils.isBlank(name));
    }
    final EnteteContainer<EnteteCasier, ResCasier> containerNoeud = infos.getContainerCasiers();
    for (int i = 0; i < containerNoeud.getNbData(); i++) {
      final String name = containerNoeud.getDataName(i);
      Assert.assertFalse(StringUtils.isBlank(name));
    }
    final int nbPdt = read.getMetier().getPdt().getNbResultats();
    Assert.assertEquals(25, nbPdt);
  }
}
