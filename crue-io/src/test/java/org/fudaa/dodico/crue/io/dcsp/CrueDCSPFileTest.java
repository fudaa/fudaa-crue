package org.fudaa.dodico.crue.io.dcsp;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.drso.CrueDRSOFileTest;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;

/**
 * Test de la lecture/écriture des fichiers dcsp.
 */
public class CrueDCSPFileTest extends AbstractIOParentTest {
    private final Crue10FileFormat drsoFormat;

    /**
     *
     */
    public CrueDCSPFileTest() {
        super(Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DCSP, TestCoeurConfig.INSTANCE), FilesForTest.DCSP_V_1_3);
        drsoFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DRSO, TestCoeurConfig.INSTANCE);
    }

    @Test
    public void testValideVersion1_1_1() {
        final CtuluLog log = new CtuluLog();
        final boolean valide = Crue10FileFormatFactory.getInstance()
                .getFileFormat(CrueFileType.DCSP, TestCoeurConfig.getInstance(Crue10VersionConfig.V_1_1_1))
                .isValide(FilesForTest.DCSP_V_1_1_1, log);
        if (log.containsErrorOrSevereError()) {
            log.printResume();
        }
        Assert.assertTrue(valide);
    }

    @Test
    public void testValideVersion1_2() {
        final CtuluLog log = new CtuluLog();
        final boolean valide = Crue10FileFormatFactory.getInstance()
                .getFileFormat(CrueFileType.DCSP, TestCoeurConfig.getInstance(Crue10VersionConfig.V_1_2))
                .isValide(FilesForTest.DCSP_V_1_2, log);
        if (log.containsErrorOrSevereError()) {
            log.printResume();
        }
        Assert.assertTrue(valide);
    }

    /**
     * Test lecture Modele 3.
     */
    @Test
    public void testLectureModele() {
        testModele(readModeleX(FilesForTest.DRSO_V1_3, FilesForTest.DCSP_V_1_3, TestCoeurConfig.INSTANCE),true);
    }

    @Test
    public void testLectureModele_V_1_2() {
        testModele(readModeleX(FilesForTest.DRSO_V1_2, FilesForTest.DCSP_V_1_2, TestCoeurConfig.INSTANCE_1_2),false);
    }

    private void testModele(final CrueData read, boolean isV3) {
        final CatEMHCasier casier = read.findCasierByReference("Ca_N6");
        final List<DonCalcSansPrtCasierProfil> prfs = EMHHelper.selectClass(casier.getInfosEMH(),
                DonCalcSansPrtCasierProfil.class);
        Assert.assertEquals(1, prfs.size());
        //Attention, on change la valeur du fichier par défaut pour tester la lecture
        //remplacer 0 par 1.11
        assertDoubleEquals(1.11, prfs.get(0).getCoefRuis());
        final EMHBrancheSeuilTransversal brancheTransversal = (EMHBrancheSeuilTransversal) read
                .findBrancheByReference("Br_B3");
        Assert.assertNotNull(brancheTransversal);

        final DonCalcSansPrtBrancheSeuilTransversal donnee = (DonCalcSansPrtBrancheSeuilTransversal) brancheTransversal
                .getDCSP().get(0);
        Assert.assertNotNull(donnee);
        Assert.assertEquals(EnumFormulePdc.BORDA, donnee.getFormulePdc());

        assertDoubleEquals(20.0, donnee.getElemSeuilAvecPdc().get(0).getLargeur());
        assertDoubleEquals(0.60, donnee.getElemSeuilAvecPdc().get(0).getZseuil());
        assertDoubleEquals(0.90, donnee.getElemSeuilAvecPdc().get(0).getCoefD());
        assertDoubleEquals(1.1, donnee.getElemSeuilAvecPdc().get(0).getCoefPdc());

        final CatEMHBranche brB1 = read.findBrancheByReference("Br_B1");
        final List<DonCalcSansPrtBrancheSaintVenant> dcspBrancheSaintVenant = EMHHelper.selectClass(brB1.getInfosEMH(),
                DonCalcSansPrtBrancheSaintVenant.class);
        Assert.assertEquals(1, dcspBrancheSaintVenant.size());
        assertDoubleEquals(1.0, dcspBrancheSaintVenant.get(0).getCoefBeta());
        final CatEMHBranche brB1FileEau = read.findBrancheByReference("Br_B1FilEau");
        final List<DonCalcSansPrtBrancheBarrageFilEau> dcspBrancheFileEau = EMHHelper.selectClass(brB1FileEau.getInfosEMH(),
                DonCalcSansPrtBrancheBarrageFilEau.class);
        Assert.assertEquals(1, dcspBrancheFileEau.size());
        DonCalcSansPrtBrancheBarrageFilEau barrageFilEau = dcspBrancheFileEau.get(0);
        Assert.assertEquals(2, barrageFilEau.getElemBarrage().size());
        assertDoubleEquals(1.0, barrageFilEau.getElemBarrage().get(0).getLargeur());
        assertDoubleEquals(3.0, barrageFilEau.getElemBarrage().get(0).getCoefNoy());
        assertDoubleEquals(2.0, barrageFilEau.getElemBarrage().get(1).getLargeur());
        assertDoubleEquals(4.0, barrageFilEau.getElemBarrage().get(1).getCoefNoy());

        assertDoubleEquals(1.0,barrageFilEau.getRegimeManoeuvrant().getAbscisse(0));
        assertDoubleEquals(2.0,barrageFilEau.getRegimeManoeuvrant().getAbscisse(1));
        assertDoubleEquals(3.0,barrageFilEau.getRegimeManoeuvrant().getOrdonnee(0));
        assertDoubleEquals(4.0,barrageFilEau.getRegimeManoeuvrant().getOrdonnee(1));
        if (isV3) {
            assertDoubleEquals(4.0, barrageFilEau.getElemBarrage().get(0).getCoefDen());
            assertDoubleEquals(5.0, barrageFilEau.getElemBarrage().get(1).getCoefDen());
        }
    }

    private static CrueData readModeleX(final String fileDrso, final String fileDCSP, CoeurConfigContrat coeurConfigContrat) {
        final CtuluLog analyzer = new CtuluLog();
        // -- lecture --//
        final CrueData jeuDonneesLue = (CrueData) getFileFormat(CrueFileType.DRSO, coeurConfigContrat).read(fileDrso, analyzer, CrueDRSOFileTest.createDefault(coeurConfigContrat))
                .getMetier();
        testAnalyser(analyzer);
        final CrueData read = (CrueData) getFileFormat(CrueFileType.DCSP, coeurConfigContrat).read(fileDCSP, analyzer, jeuDonneesLue).getMetier();
        testAnalyser(analyzer);
        return read;
    }

    private static CrueData readModeleX(final String fileDrso, final File fileDCSP, CoeurConfigContrat coeurConfigContrat) {
        final CtuluLog analyzer = new CtuluLog();
        // -- lecture --//
        final CrueData jeuDonneesLue = (CrueData) getFileFormat(CrueFileType.DRSO, coeurConfigContrat).read(fileDrso, analyzer, CrueDRSOFileTest.createDefault(coeurConfigContrat))
                .getMetier();
        testAnalyser(analyzer);
        final CrueData read = (CrueData) getFileFormat(CrueFileType.DCSP, coeurConfigContrat).read(fileDCSP, analyzer, jeuDonneesLue).getMetier();
        testAnalyser(analyzer);
        return read;
    }


    /**
     * Test ecriture Modele 3.
     */
    @Test
    public void testEcritureModele() {
        final CrueData data = readModeleX(FilesForTest.DRSO_V1_3, FilesForTest.DCSP_V_1_3, TestCoeurConfig.INSTANCE);
        final CtuluLog analyzer = new CtuluLog();
        final File f = createTempFile();
        format.write(data, f, analyzer);
        testAnalyser(analyzer);
        Assert.assertTrue(format.isValide(f, analyzer));
        testModele(readModeleX(FilesForTest.DRSO_V1_3, f, TestCoeurConfig.INSTANCE),true);
    }

    @Test
    public void testEcritureModeleV2() {
        final CrueData data = readModeleX(FilesForTest.DRSO_V1_2, FilesForTest.DCSP_V_1_2, TestCoeurConfig.INSTANCE_1_2);
        testModele(data,false);
        final CtuluLog analyzer = new CtuluLog();
        final File f = createTempFile();
        Crue10FileFormat<Object> fileFormat_V1_2 = getFileFormat(CrueFileType.DCSP, TestCoeurConfig.INSTANCE_1_2);
        fileFormat_V1_2.write(data, f, analyzer);
        testAnalyser(analyzer);
        Assert.assertTrue(fileFormat_V1_2.isValide(f, analyzer));
        testModele(readModeleX(FilesForTest.DRSO_V1_2, f, TestCoeurConfig.INSTANCE_1_2),false);
    }
}
