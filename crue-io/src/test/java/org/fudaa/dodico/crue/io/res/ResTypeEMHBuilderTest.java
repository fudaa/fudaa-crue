/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.io.res;

import files.FilesForTest;
import java.util.List;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.rcal.CrueDaoRCAL;
import org.fudaa.dodico.crue.io.rcal.CrueRCAL_V1_2_Test;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.ItemResDao;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class ResTypeEMHBuilderTest {

  @Test
  public void testExtract() {
    CrueDaoRCAL readData = CrueRCAL_V1_2_Test.readData(FilesForTest.RCAL_XML_V1_2, TestCoeurConfig.INSTANCE);
    assertNotNull(readData);
    ResTypeEMHBuilder builder = new ResTypeEMHBuilder(readData);
    CrueIOResu<List<ResCatEMHContent>> extract = builder.extract();
    if (extract.getAnalyse().containsErrorOrSevereError()) {
      extract.getAnalyse().printResume();
    }
    assertFalse(extract.getAnalyse().containsErrorOrSevereError());
    List<ResCatEMHContent> cats = extract.getMetier();
    assertEquals(4, cats.size());
    for (ResCatEMHContent cat : cats) {
      for (ResTypeEMHContent type : cat.getResTypeEMHContent()) {
        assertNotNull("not nul for cat " + cat.getDelimiteur(), type);
      }

    }
    testNoeud(cats.get(0));
    testCasier(cats.get(1));
    testSection(cats.get(2));
    testBranche(cats.get(3));
  }
  final int tailleMot = 8;
  final int nbrMotEntete = 1;
  final int nbrMotNoeud = 8;
  final int nbrMotCasier = 9;
  final int nbrMotSection = 521;
  int nbrMotBranche = 22;

  public int getNbOctets(int nbrMot) {
    return nbrMot * tailleMot;
  }

  public int getNbOctets(int... nbrMots) {
    int res = 0;
    for (int i = 0; i < nbrMots.length; i++) {
      res += getNbOctets(nbrMots[i]);
    }
    return res;

  }

  private void testNoeud(ResCatEMHContent noeudCat) {
    assertEquals("   Noeud", noeudCat.getDelimiteur());
    assertEquals(getNbOctets(nbrMotEntete), noeudCat.getPositionInBytes());
    List<ResTypeEMHContent> noeudTypeList = noeudCat.getResTypeEMHContent();
    assertEquals(1, noeudTypeList.size());
    //test du type
    ResTypeEMHContent type = noeudTypeList.get(0);
    ResVariablesContent resVariablesContent = type.getResVariablesContent();
    assertEquals(1, resVariablesContent.getNbVariables());
    assertTrue(resVariablesContent.isVariableDefined("z"));
    //skipper l'entete du fichier et l'entete de la cat:
    int nbOctets = getNbOctets(nbrMotEntete, nbrMotEntete);
    assertEquals(nbOctets, type.getPositionInBytes());

    //test des noeuds
    List<ItemResDao> itemResDaoList = type.getItemResDao();
    assertEquals(7, itemResDaoList.size());
    CrueRCAL_V1_2_Test.testNomRef(itemResDaoList, "Nd_N1", "Nd_N2");
    for (ItemResDao itemResDao : itemResDaoList) {
      assertEquals(nbOctets, type.getItemPositionInBytes(itemResDao.getNomRef()));
      nbOctets += getNbOctets(itemResDao.getNbrMot());
    }
  }

  private void testCasier(ResCatEMHContent casierCat) {
    assertEquals("  Casier", casierCat.getDelimiteur());
    final int positionCat = getNbOctets(nbrMotEntete, nbrMotNoeud);
    assertEquals(positionCat, casierCat.getPositionInBytes());
    List<ResTypeEMHContent> typeList = casierCat.getResTypeEMHContent();
    assertEquals(1, typeList.size());
    int positionType = positionCat + getNbOctets(nbrMotEntete);
    ResTypeEMHContent type = typeList.get(0);
    assertEquals(4, type.getResVariablesContent().getNbVariables());
    assertEquals(positionType, type.getPositionInBytes());
    assertEquals(2, type.getItemResDao().size());
    assertEquals(positionType, type.getItemPositionInBytes("Ca_N6"));
    assertEquals(positionType + getNbOctets(type.getItemResDao().get(0).getNbrMot()), type.getItemPositionInBytes("Ca_N7"));

  }

  private void testBranche(ResCatEMHContent branche) {
    assertEquals(" Branche", branche.getDelimiteur());
    final int branchePosition = getNbOctets(nbrMotEntete, nbrMotNoeud, nbrMotCasier, nbrMotSection);
    assertEquals(branchePosition, branche.getPositionInBytes());
    List<ResTypeEMHContent> typeList = branche.getResTypeEMHContent();
    assertEquals(9, typeList.size());
    ResTypeEMHContent brancheStricklerType = typeList.get(8);
    //+ getNbOctets(nbrMotEntete) pour l'entete de la categorie.
    //19 pour la taille des types précédents strickler:
    long position = branchePosition + getNbOctets(nbrMotEntete) + getNbOctets(19);
    assertEquals(position, brancheStricklerType.getPositionInBytes());

  }

  private void testSection(ResCatEMHContent section) {
    assertEquals(" Section", section.getDelimiteur());
    final int catPosition = getNbOctets(nbrMotEntete, nbrMotNoeud, nbrMotCasier);
    assertEquals(catPosition, section.getPositionInBytes());
    List<ResTypeEMHContent> typeList = section.getResTypeEMHContent();
    assertEquals(4, typeList.size());
    for (ResTypeEMHContent type : typeList) {
      assertEquals(19, type.getResVariablesContent().getNbVariables());
    }
    //on test sectionInterpole
    ResTypeEMHContent sectionInterpolee = typeList.get(1);
    int sectionInterpoleePosition = catPosition + getNbOctets(nbrMotEntete) + getNbOctets(40);//40 est le nombre de mot de SectionIdem
    assertEquals(sectionInterpoleePosition, sectionInterpolee.getPositionInBytes());
    List<ItemResDao> sectionList = sectionInterpolee.getItemResDao();
    assertEquals(6, sectionList.size());
    //St_B1_00150
    //20 correspond au NbrMot de St_B1_00050
    assertEquals(sectionInterpoleePosition + getNbOctets(20), sectionInterpolee.getItemPositionInBytes("St_B1_00150"));
  }
}
