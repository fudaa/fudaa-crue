package org.fudaa.dodico.crue.io.dclm;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.drso.CrueDRSOFileTest;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Classe de tests JUnit pour DCLM (Données des conditions aux limites et manoeuvres) ; Permet de contrôler le bon fonctionnement de lecture d'un
 * fichier XML existant, l'écriture de façon similaire de ce même fichier ou d'un autre correspondant aux mêmes définitions de contenu
 *
 * @author cde
 */
public class CrueDCLMFileTest extends AbstractIOParentTest {
  /**
   * Constructeur
   */
  public CrueDCLMFileTest() {
    super(Crue10FileFormatFactory.getVersion(CrueFileType.DCLM, TestCoeurConfig.INSTANCE), FilesForTest.DCLM_V_1_3);
  }

  @Test
  public void testValideVersion_DM_1_3() {
    final CtuluLog log = new CtuluLog();
    final boolean valide = Crue10FileFormatFactory.getInstance()
        .getFileFormat(CrueFileType.DCLM, TestCoeurConfig.getInstance(Crue10VersionConfig.getLastVersion()))
        .isValide(FilesForTest.DCLM_V_1_3_DM, log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    Assert.assertTrue(valide);
  }

  @Test
  public void testValideVersion_SY_1_3() {
    final CtuluLog log = new CtuluLog();
    final boolean valide = Crue10FileFormatFactory.getInstance()
        .getFileFormat(CrueFileType.DCLM, TestCoeurConfig.getInstance(Crue10VersionConfig.getLastVersion()))
        .isValide(FilesForTest.DCLM_V_1_3_SY, log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    Assert.assertTrue(valide);
  }

  @Test
  public void testValideVersion1_1() {
    final CtuluLog log = new CtuluLog();
    final boolean valide = Crue10FileFormatFactory.getInstance()
        .getFileFormat(CrueFileType.DCLM, TestCoeurConfig.getInstance(Crue10VersionConfig.V_1_1_1))
        .isValide(FilesForTest.DCLM_V1_1_1, log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    Assert.assertTrue(valide);
  }

  @Test
  public void testValideVersion1_2() {
    final CtuluLog log = new CtuluLog();
    final boolean valide = Crue10FileFormatFactory.getInstance()
        .getFileFormat(CrueFileType.DCLM, TestCoeurConfig.getInstance(Crue10VersionConfig.V_1_2))
        .isValide(FilesForTest.DCLM_V_1_2, log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    Assert.assertTrue(valide);
  }

  /**
   * Lit le fichier XML et verifie certaines valeurs des différentes conditions aux limites
   */
  @Test
  public void testLecture() {
    readDataAndTest(FilesForTest.DCLM_V_1_3, FilesForTest.DRSO_V1_3, FilesForTest.DLHY_V1_3,
        TestCoeurConfig.INSTANCE);
  }

  @Test
  public void testLecture_Project_DM() {
    CrueData crueData = readData(FilesForTest.DCLM_V_1_3_DM, FilesForTest.DRSO_V_1_3_DM, FilesForTest.DLHY_V_1_3_DM,
        TestCoeurConfig.INSTANCE);
    verifieDonneesProject_DM(crueData);
  }

  @Test
  public void testEcriture_Project_DM() {
    CrueData crueData = readData(FilesForTest.DCLM_V_1_3_DM, FilesForTest.DRSO_V_1_3_DM, FilesForTest.DLHY_V_1_3_DM,
        TestCoeurConfig.INSTANCE);
    final File f = writeFile(FilesForTest.DCLM_V_1_3_DM, FilesForTest.DRSO_V_1_3_DM, FilesForTest.DLHY_V_1_3_DM, TestCoeurConfig.INSTANCE);
    // On lit le fichier temporaire précédemment créé et on le vérifie
    // -- lecture drso --//
    final CrueData data = readData(f, FilesForTest.DRSO_V_1_3_DM, FilesForTest.DLHY_V_1_3_DM, TestCoeurConfig.INSTANCE);
    verifieDonneesProject_DM(data);
  }

  @Test
  public void testEcriture_Project_DM_IN_V1_2() {
    // On lit le fichier temporaire précédemment créé et on le vérifie
    // -- lecture drso --//
    final CrueData data = readData(FilesForTest.DCLM_V_1_3_DM, FilesForTest.DRSO_V_1_3_DM, FilesForTest.DLHY_V_1_3_DM, TestCoeurConfig.INSTANCE);
    final File f = createTempFile();
    // On ecrit
    final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DCLM,
        TestCoeurConfig.INSTANCE_1_2);
    CtuluLog log = new CtuluLog();
    log.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);

    fileFormat.writeMetierDirect(data.getScenarioData().getDonCLimMScenario(), f, log, CrueConfigMetierForTest.DEFAULT_1_2);
    testAnalyser(log);
    final boolean valide = fileFormat.isValide(f, log);
    List<CtuluLogRecord> records = log.getRecords();
    assertEquals(6, records.size());
    log.updateLocalizedMessage(log.getDefaultResourceBundle());
    String localizedMessage = records.get(0).getLocalizedMessage();
    assertEquals("Les CLimM de type CalcTransBrancheOrificeManoeuvreRegul ne sont pas supportées en version 1.2 et ne seront pas sauvegardées", localizedMessage);
    Assert.assertTrue(valide);
  }

  @Test
  public void testLecture_Project_SY() {
    CrueData crueData = readData(FilesForTest.DCLM_V_1_3_SY, FilesForTest.DRSO_V_1_3_SY, FilesForTest.DLHY_V_1_3_SY,
        TestCoeurConfig.INSTANCE);
    verifieDonneesProject_SY(crueData, false);
  }

  @Test
  public void testEcriture_Project_SY() {
    final File f = writeFile(FilesForTest.DCLM_V_1_3_SY, FilesForTest.DRSO_V_1_3_SY, FilesForTest.DLHY_V_1_3_SY, TestCoeurConfig.INSTANCE);
    final CrueData crueData = readData(f, FilesForTest.DRSO_V_1_3_SY, FilesForTest.DLHY_V_1_3_SY, TestCoeurConfig.INSTANCE);
    verifieDonneesProject_SY(crueData, false);
  }

  @Test
  public void testLecture_Project_SY_Empty_ResCalcTrans() {
    CrueData crueData = readData(FilesForTest.DCLM_V_1_3_SY_EMPTY, FilesForTest.DRSO_V_1_3_SY, FilesForTest.DLHY_V_1_3_SY,
        TestCoeurConfig.INSTANCE);
    verifieDonneesProject_SY(crueData, true);
  }

  @Test
  public void testEcriture_Project_SY_With_Empty_ResCalcTrans() {
    final File f = writeFile(FilesForTest.DCLM_V_1_3_SY_EMPTY, FilesForTest.DRSO_V_1_3_SY, FilesForTest.DLHY_V_1_3_SY, TestCoeurConfig.INSTANCE);
    CtuluLog log = new CtuluLog();
    boolean valide = Crue10FileFormatFactory.getInstance()
        .getFileFormat(CrueFileType.DCLM, TestCoeurConfig.getInstance(Crue10VersionConfig.getLastVersion()))
        .isValide(f, log);
    if (log.containsErrorOrSevereError()) {
      log.printResume();
    }
    Assert.assertTrue(valide);
    final CrueData crueData = readData(f, FilesForTest.DRSO_V_1_3_SY, FilesForTest.DLHY_V_1_3_SY, TestCoeurConfig.INSTANCE);
    verifieDonneesProject_SY(crueData, true);
  }

  @Test
  public void testLecture_v_1_2() {
    readDataAndTest(FilesForTest.DCLM_V_1_2, FilesForTest.DRSO_V1_2, FilesForTest.DLHY_V1_2,
        TestCoeurConfig.INSTANCE_1_2);
  }

  @Test
  public void testLecture_v1_1_1() {
    readDataAndTest(FilesForTest.DCLM_V1_1_1, FilesForTest.DRSO_V1_1_1,
        FilesForTest.DLHY_V1_1_1, TestCoeurConfig.getInstance(Crue10VersionConfig.V_1_1_1));
  }

  /**
   * Lit le fichier XML et verifie certaines valeurs des différentes conditions aux limites
   */
  public CrueData readDataAndTest(final String dclmFile, final String drsoFile, final String dlhyFile, final CoeurConfigContrat version) {
    final CrueData data = readData(dclmFile, drsoFile, dlhyFile, version);
    verifieDonnees(data,version);
    return data;
  }

  private static CrueData readData(String dclmFile, String drsoFile, String dlhyFile, CoeurConfigContrat version) {
    final CtuluLog analyzer = new CtuluLog();
    // -- lecture drso --//
    final CrueData data = (CrueData) Crue10FileFormatFactory.getVersion(CrueFileType.DRSO, version)
        .read(drsoFile, analyzer, CrueDRSOFileTest.createDefault(version)).getMetier();
    testAnalyser(analyzer);

    // -- lecture dlhy --//
    final List<Loi> listeLois = (List<Loi>) Crue10FileFormatFactory.getVersion(CrueFileType.DLHY, version)
        .read(dlhyFile, analyzer, data).getMetier();
    testAnalyser(analyzer);
    Assert.assertNotNull(listeLois);

    // -- lecture de dclm --//
    Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DCLM, version).read(dclmFile, analyzer, data)
        .getMetier();
    testAnalyser(analyzer);
    return data;
  }

  public CrueData readData(final File dclmFile, final String drsoFile, final String dlhyFile, final CoeurConfigContrat version) {
    final CtuluLog analyzer = new CtuluLog();
    // -- lecture drso --//
    final CrueData data = (CrueData) Crue10FileFormatFactory.getVersion(CrueFileType.DRSO, version)
        .read(drsoFile, analyzer, CrueDRSOFileTest.createDefault(version)).getMetier();
    testAnalyser(analyzer);

    // -- lecture dlhy --//
    final List<Loi> listeLois = (List<Loi>) Crue10FileFormatFactory.getVersion(CrueFileType.DLHY, version)
        .read(dlhyFile, analyzer, data).getMetier();
    testAnalyser(analyzer);
    Assert.assertNotNull(listeLois);

    // -- lecture de dclm --//
    Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DCLM, version).read(dclmFile, analyzer, data)
        .getMetier();
    testAnalyser(analyzer);
    return data;
  }

  /**
   * Lit le fichier XML et verifie certaines valeurs des différentes conditions aux limites
   */
  public CrueData readDataAndTest(final File dclmFile, final String drsoFile, final String dlhyFile, final CoeurConfigContrat version) {
    final CrueData data = readData(version, drsoFile, dlhyFile, dclmFile);
    verifieDonnees(data,version);
    return data;
  }

  /**
   * Lecture du fichier XML puis écriture dans un fichier temporaire
   *
   * @return le fichier temporaire XML créé à partir de la lecture du fichier existant
   */
  public File writeFile(final String dclmFile, final String drsoFile, final String dlhyFile, final CoeurConfigContrat version) {

    final CtuluLog analyzer = new CtuluLog();
    final CrueData data = readDataAndTest(dclmFile, drsoFile, dlhyFile, version);
    final File f = createTempFile();
    // On ecrit
    final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DCLM,
        version);

    fileFormat.writeMetierDirect(data.getScenarioData().getDonCLimMScenario(), f, analyzer, CrueConfigMetierForTest.DEFAULT);
    testAnalyser(analyzer);
    final boolean valide = fileFormat.isValide(f, analyzer);
    testAnalyser(analyzer);
    Assert.assertTrue(valide);
    return f;
  }

  /**
   * Lecture du fichier XML puis Ecriture dans un fichier temporaire puis Lecture de ce fichier temporaire et verification de certaines valeurs de
   * différents calculs
   */
  @Test
  public void testLectureEcritureLecture() {
    testLectureEcritureLecture(FilesForTest.DCLM_V_1_3, FilesForTest.DRSO_V1_3,
        FilesForTest.DLHY_V1_3, TestCoeurConfig.INSTANCE);
  }

  @Test
  public void testLectureEcritureLecture_V_1_2() {
    testLectureEcritureLecture(FilesForTest.DCLM_V_1_2, FilesForTest.DRSO_V1_2,
        FilesForTest.DLHY_V1_2, TestCoeurConfig.INSTANCE_1_2);
  }

  //CRUE-450: Une CLimM créée inactive est enregistrée active
  @Test
  public void testLectureEcritureLectureNonUserActive() {
    final CtuluLog analyzer = new CtuluLog();
    final CrueData data = readDataAndTest(FilesForTest.DCLM_V_1_3, FilesForTest.DRSO_V1_3,
        FilesForTest.DLHY_V1_3, TestCoeurConfig.INSTANCE);
    final DonCLimMScenario dclm = data.getScenarioData().getDonCLimMScenario();
    for (final CalcPseudoPerm calcPseudoPerm : dclm.getCalcPseudoPerm()) {
      final List<DonCLimM> listeDCLM = calcPseudoPerm.getlisteDCLM();
      for (final DonCLimM donCLimM : listeDCLM) {
        ((DonCLimMCommonItem) donCLimM).setUserActive(false);
      }
    }
    for (final CalcTrans calcTrans : dclm.getCalcTrans()) {
      final List<DonCLimM> listeDCLM = calcTrans.getlisteDCLM();
      for (final DonCLimM donCLimM : listeDCLM) {
        ((DonCLimMCommonItem) donCLimM).setUserActive(false);
      }
    }
    final File f = createTempFile();
    // On ecrit
    final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DCLM,
        TestCoeurConfig.INSTANCE);
    fileFormat.writeMetierDirect(data.getScenarioData().getDonCLimMScenario(), f, analyzer,
        CrueConfigMetierForTest.DEFAULT);
    testAnalyser(analyzer);
    final boolean valide = fileFormat.isValide(f, analyzer);
    testAnalyser(analyzer);
    Assert.assertTrue(valide);
    final CrueData reReadData = readData(f, FilesForTest.DRSO_V1_3,
        FilesForTest.DLHY_V1_3, TestCoeurConfig.INSTANCE);
    final DonCLimMScenario unactiveDclm = reReadData.getScenarioData().getDonCLimMScenario();
    for (final CalcPseudoPerm calcPseudoPerm : unactiveDclm.getCalcPseudoPerm()) {
      final List<DonCLimM> listeDCLM = calcPseudoPerm.getlisteDCLM();
      for (final DonCLimM donCLimM : listeDCLM) {
        Assert.assertFalse(((DonCLimMCommonItem) donCLimM).getUserActive());
      }
    }
    for (final CalcTrans calcTrans : unactiveDclm.getCalcTrans()) {
      final List<DonCLimM> listeDCLM = calcTrans.getlisteDCLM();
      for (final DonCLimM donCLimM : listeDCLM) {
        Assert.assertFalse(((DonCLimMCommonItem) donCLimM).getUserActive());
      }
    }
  }

  @Test
  public void testLectureEcritureLecture_v1p1p1() {
    testLectureEcritureLecture(FilesForTest.DCLM_V1_1_1, FilesForTest.DRSO_V1_1_1,
        FilesForTest.DLHY_V1_1_1, TestCoeurConfig.getInstance(Crue10VersionConfig.V_1_1_1));
  }

  /**
   * Lecture du fichier XML puis Ecriture dans un fichier temporaire puis Lecture de ce fichier temporaire et verification de certaines valeurs de
   * différents calculs
   */
  public void testLectureEcritureLecture(final String dclmFile, final String drsoFile, final String dlhyFile, final CoeurConfigContrat version) {

    final File f = writeFile(dclmFile, drsoFile, dlhyFile, version);
    // On lit le fichier temporaire précédemment créé et on le vérifie
    // -- lecture drso --//
    final CrueData data = readDataAndTest(f, drsoFile, dlhyFile, version);
    verifieDonnees(data,version);
  }


  private void verifieDonneesProject_DM(final CrueData crueData) {
    final DonCLimMScenario data = crueData.getScenarioData().getDonCLimMScenario();

    Assert.assertNotNull(data);
    List<CalcTrans> calcTransList = data.getCalcTrans();
    CalcTrans calcTrans1 = calcTransList.get(0);
    assertEquals("Cc_T01", calcTrans1.getNom());
    List<DonCLimM> dclms = calcTrans1.getlisteDCLM();
    assertEquals(8, dclms.size());

    CalcTransNoeudQapp donCLimM = (CalcTransNoeudQapp) dclms.get(0);
    assertEquals("Nd_NVR153.100", donCLimM.getEmh().getNom());
    assertEquals("LoiTQapp_Nd_NVR153.100", donCLimM.getHydrogrammeQapp().getNom());
    Assert.assertTrue(donCLimM.getUserActive());

    donCLimM = (CalcTransNoeudQapp) dclms.get(1);
    assertEquals("Nd_PCF1", donCLimM.getEmh().getNom());
    assertEquals("LoiTQapp_Nd_PCF1", donCLimM.getHydrogrammeQapp().getNom());
    Assert.assertTrue(donCLimM.getUserActive());

    donCLimM = (CalcTransNoeudQapp) dclms.get(2);
    assertEquals("Nd_NRE166.500", donCLimM.getEmh().getNom());
    assertEquals("LoiTQapp_Nd_NRE166.500", donCLimM.getHydrogrammeQapp().getNom());
    Assert.assertTrue(donCLimM.getUserActive());

    CalcTransBrancheOrificeManoeuvreRegul donCLimMRegul = (CalcTransBrancheOrificeManoeuvreRegul) dclms.get(3);
    assertEquals("Br_BNAV12", donCLimMRegul.getEmh().getNom());
    assertEquals("Qpil", donCLimMRegul.getParam());
    assertEquals("LoiQOuv_APN", donCLimMRegul.getManoeuvreRegul().getNom());
    assertEquals(EnumSensOuv.OUV_VERS_HAUT, donCLimMRegul.getSensOuv());
    Assert.assertTrue(donCLimMRegul.getUserActive());

    donCLimMRegul = (CalcTransBrancheOrificeManoeuvreRegul) dclms.get(4);
    assertEquals("Br_BNAV22", donCLimMRegul.getEmh().getNom());
    assertEquals("Qpil2", donCLimMRegul.getParam());
    assertEquals("LoiQOuv_NPN", donCLimMRegul.getManoeuvreRegul().getNom());
    assertEquals(EnumSensOuv.OUV_VERS_HAUT, donCLimMRegul.getSensOuv());
    Assert.assertTrue(donCLimMRegul.getUserActive());

    donCLimMRegul = (CalcTransBrancheOrificeManoeuvreRegul) dclms.get(5);
    assertEquals("Br_BUSIA", donCLimMRegul.getEmh().getNom());
    assertEquals("Qpil", donCLimMRegul.getParam());
    assertEquals("LoiQOuv_BU", donCLimMRegul.getManoeuvreRegul().getNom());
    assertEquals(EnumSensOuv.OUV_VERS_BAS, donCLimMRegul.getSensOuv());
    Assert.assertTrue(donCLimMRegul.getUserActive());

    CalcTransNoeudBg1 bg1 = (CalcTransNoeudBg1) dclms.get(6);
    assertEquals("Nd_NRE171.500", bg1.getEmh().getNom());
    Assert.assertTrue(bg1.getUserActive());

    CalcTransNoeudUsine usine = (CalcTransNoeudUsine) dclms.get(7);
    assertEquals("Nd_NUS187.40", usine.getEmh().getNom());
    Assert.assertTrue(usine.getUserActive());


    CalcTrans calcTrans1Test = calcTransList.get(1);
    assertEquals("Cc_T01_Test", calcTrans1Test.getNom());
    dclms = calcTrans1Test.getlisteDCLM();
    assertEquals(8, dclms.size());

    donCLimM = (CalcTransNoeudQapp) dclms.get(0);
    assertEquals("Nd_NVR153.100", donCLimM.getEmh().getNom());
    assertEquals("LoiTQapp_Nd_NVR153.100_Test", donCLimM.getHydrogrammeQapp().getNom());
    Assert.assertTrue(donCLimM.getUserActive());

    donCLimMRegul = (CalcTransBrancheOrificeManoeuvreRegul) dclms.get(1);
    assertEquals("Br_BNAV12", donCLimMRegul.getEmh().getNom());
    assertEquals("Qpil", donCLimMRegul.getParam());
    assertEquals("LoiQOuv_APN", donCLimMRegul.getManoeuvreRegul().getNom());
    assertEquals(EnumSensOuv.OUV_VERS_HAUT, donCLimMRegul.getSensOuv());
    Assert.assertTrue(donCLimMRegul.getUserActive());

    donCLimMRegul = (CalcTransBrancheOrificeManoeuvreRegul) dclms.get(2);
    assertEquals("Br_BNAV22", donCLimMRegul.getEmh().getNom());
    assertEquals("Qpil", donCLimMRegul.getParam());
    assertEquals("LoiQOuv_NPN", donCLimMRegul.getManoeuvreRegul().getNom());
    assertEquals(EnumSensOuv.OUV_VERS_HAUT, donCLimMRegul.getSensOuv());
    Assert.assertTrue(donCLimMRegul.getUserActive());

    donCLimMRegul = (CalcTransBrancheOrificeManoeuvreRegul) dclms.get(3);
    assertEquals("Br_BUSIA", donCLimMRegul.getEmh().getNom());
    assertEquals("Qpil", donCLimMRegul.getParam());
    assertEquals("LoiQOuv_BU", donCLimMRegul.getManoeuvreRegul().getNom());
    assertEquals(EnumSensOuv.OUV_VERS_HAUT, donCLimMRegul.getSensOuv());
    Assert.assertTrue(donCLimMRegul.getUserActive());

    bg1 = (CalcTransNoeudBg1) dclms.get(4);
    assertEquals("Nd_NRE166.500", bg1.getEmh().getNom());
    Assert.assertTrue(bg1.getUserActive());

    CalcTransNoeudBg2 bg2 = (CalcTransNoeudBg2) dclms.get(5);
    assertEquals("Nd_NRE171.500", bg2.getEmh().getNom());
    Assert.assertTrue(bg2.getUserActive());

    CalcTransNoeudBg1Av bg1av = (CalcTransNoeudBg1Av) dclms.get(6);
    assertEquals("Nd_PCF1", bg1av.getEmh().getNom());
    Assert.assertFalse(bg1av.getUserActive());

    CalcTransNoeudBg2Av bg2av = (CalcTransNoeudBg2Av) dclms.get(7);
    assertEquals("Nd_NUS187.40", bg2av.getEmh().getNom());
    Assert.assertTrue(bg2av.getUserActive());


  }

  private void verifieDonneesProject_SY(CrueData crueData, boolean emptyResCalcTrans) {
    final DonCLimMScenario data = crueData.getScenarioData().getDonCLimMScenario();

    Assert.assertNotNull(data);
    List<CalcTrans> calcTransList = data.getCalcTrans();
    CalcTrans calcTrans1 = calcTransList.get(0);
    assertEquals("Cc_T01", calcTrans1.getNom());
    List<DonCLimM> dclms = calcTrans1.getlisteDCLM();
    assertEquals(4, dclms.size());

    CalcTransNoeudQappExt donCLimM = (CalcTransNoeudQappExt) dclms.get(2);
    assertEquals("Nd_RET162.000", donCLimM.getEmh().getNom());
    if (!emptyResCalcTrans)
      assertEquals("Cc_T01", donCLimM.getResCalcTrans());
    assertEquals("St_P162.200", donCLimM.getSection());
    assertEquals("..\\01_GE\\Runs\\Sc_GE\\R0000-00-00-00h00m00s\\Mo_GE\\GE.rcal.xml", donCLimM.getNomFic());
    Assert.assertTrue(donCLimM.getUserActive());


  }

  /**
   * Verifie certaines donnees de differents calculs
   *
   * @param crueData liste des calculs à vérifier
   */
  private void verifieDonnees(final CrueData crueData, final CoeurConfigContrat version) {
    final DonCLimMScenario data = crueData.getScenarioData().getDonCLimMScenario();

    Assert.assertNotNull(data);

    for (final CalcPseudoPerm calculP : data.getCalcPseudoPerm()) {

      if (calculP.getNom().equals("Cc_P1")) {
        assertEquals(calculP.getCommentaire(), "Calcul pseudo-permanent 1");
        testPseudoPermNoeudQappInCP1(crueData, calculP);
        testPseudoPermNoeudNiveauContinuZimpInCP1(crueData, calculP);
        testPseudoPermBrancheOrificeManoeuvre(calculP);
        boolean newDclmSupported = Crue10VersionConfig.isUpperThan_V1_2(version);
        if (newDclmSupported) {
          testPseudoPermBrancheOrificeManoeuvreRegul(crueData, calculP);
          testCalcPseudoPermNoeudBg1(crueData, calculP);
          testCalcPseudoPermNoeudBg2(crueData, calculP);
          testCalcPseudoPermNoeudUsi(crueData, calculP);
          testPseudoPermNoeudBg1Av(crueData, calculP);
          testPseudoPermNoeudBg2Av(crueData, calculP);
        }
      }
      testCalcPseudoPermBrancheSaintVenantQruis(crueData, calculP);
      testCalcPseudoPermCasierProfilQruis(crueData, calculP);
    }// Fin for listeCalculsPermanents

    for (final CalcTrans calculT : data.getCalcTrans()) {

      if (calculT.getNom().equals("Cc_T1")) {
        assertEquals("Calcul transitoire 1", calculT.getCommentaire());
        testCalcTransNoeudQapp(crueData, calculT);
        testCalcTransNoeudNiveauContinuTarage(crueData, calculT);
      }
      testCalcTransBrancheOrificeManoeuvre(crueData, calculT);
      testCalcTransBrancheSaintVenantQruis(crueData, calculT);
      testCalcTransCasierProfilQruis(crueData, calculT);
    }
  }

  private void testPseudoPermNoeudBg2Av(CrueData crueData, CalcPseudoPerm calculP) {
    List<CalcPseudoPermNoeudBg2Av> list = calculP.getDclm(CalcPseudoPermNoeudBg2Av.class);
    assertEquals(1, list.size());
    CalcPseudoPermNoeudBg2Av calcPseudoPermNoeudBg2Av = list.get(0);
    assertFalse(calcPseudoPermNoeudBg2Av.getUserActive());
    assertEquals("Nd_N6", calcPseudoPermNoeudBg2Av.getEmh().getNom());
  }

  private void testPseudoPermNoeudBg1Av(CrueData crueData, CalcPseudoPerm calculP) {
    List<CalcPseudoPermNoeudBg1Av> list = calculP.getDclm(CalcPseudoPermNoeudBg1Av.class);
    assertEquals(1, list.size());
    CalcPseudoPermNoeudBg1Av calc = list.get(0);
    assertFalse(calc.getUserActive());
    assertEquals("Nd_N3", calc.getEmh().getNom());
  }

  private void testCalcPseudoPermNoeudUsi(CrueData crueData, CalcPseudoPerm calculP) {
    List<CalcPseudoPermNoeudUsi> list = calculP.getDclm(CalcPseudoPermNoeudUsi.class);
    assertEquals(1, list.size());
    CalcPseudoPermNoeudUsi calc = list.get(0);
    assertTrue(calc.getUserActive());
    assertEquals("Nd_N7", calc.getEmh().getNom());
  }

  private void testCalcPseudoPermNoeudBg2(CrueData crueData, CalcPseudoPerm calculP) {
    List<CalcPseudoPermNoeudBg2> list = calculP.getDclm(CalcPseudoPermNoeudBg2.class);
    assertEquals(1, list.size());
    CalcPseudoPermNoeudBg2 calc = list.get(0);
    assertTrue(calc.getUserActive());
    assertEquals("Nd_N2", calc.getEmh().getNom());
  }

  private void testCalcPseudoPermNoeudBg1(CrueData crueData, CalcPseudoPerm calculP) {
    List<CalcPseudoPermNoeudBg1> list = calculP.getDclm(CalcPseudoPermNoeudBg1.class);
    assertEquals(1, list.size());
    CalcPseudoPermNoeudBg1 calc = list.get(0);
    assertFalse(calc.getUserActive());
    assertEquals("Nd_N1", calc.getEmh().getNom());
  }

  private void testPseudoPermBrancheOrificeManoeuvreRegul(CrueData crueData, CalcPseudoPerm calculP) {
    List<CalcPseudoPermBrancheOrificeManoeuvreRegul> list = calculP.getDclm(CalcPseudoPermBrancheOrificeManoeuvreRegul.class);
    assertEquals(1, list.size());
    CalcPseudoPermBrancheOrificeManoeuvreRegul calc = list.get(0);
    assertTrue(calc.getUserActive());
    assertEquals(EnumSensOuv.OUV_VERS_HAUT,calc.getSensOuv());
    assertEquals("Br_B8", calc.getEmh().getNom());
    assertEquals("MyParam", calc.getData().getParam());
    assertEquals("LoiQOuv_APN", calc.getData().getManoeuvreRegul().getNom());
  }

  private static void testCalcTransCasierProfilQruis(CrueData crueData, CalcTrans calculT) {
    for (final CalcTransCasierProfilQruis casierCast : calculT.getDclm(CalcTransCasierProfilQruis.class)) {

      LoiDF loi = casierCast.getHydrogrammeQruis();
      Assert.assertNotNull(loi);
      assertEquals(loi.getNom(), "LoiTQruis_HydrogrammeRuis1");

      final CatEMHCasier noeud = crueData.findCasierByReference(casierCast.getEmh().getNom());
      Assert.assertNotNull(noeud);
      final List<DonCLimM> donclims = noeud.getDCLM();
      Assert.assertNotNull(donclims);
      boolean trouve = false;
      for (final DonCLimM donclim : donclims) {
        if (donclim instanceof CalcTransCasierProfilQruis) {
          trouve = true;
          final CalcTransCasierProfilQruis calcTransCasierProfilQruis = (CalcTransCasierProfilQruis) donclim;
          Assert.assertTrue(calcTransCasierProfilQruis.getUserActive());
          loi = calcTransCasierProfilQruis.getHydrogrammeQruis();
          Assert.assertNotNull(loi);
          assertEquals(loi.getNom(), "LoiTQruis_HydrogrammeRuis1");
          assertEquals(loi.getEvolutionFF().getPtEvolutionFF().size(), 2);
          break;
        }
      }
      Assert.assertTrue(trouve);
    }
  }

  private static void testCalcTransBrancheSaintVenantQruis(CrueData crueData, CalcTrans calculT) {
    int count = 0;
    for (final CalcTransBrancheSaintVenantQruis brancheCast : calculT.getDclm(CalcTransBrancheSaintVenantQruis.class)) {
      if (count == 1) {
        LoiDF loi = brancheCast.getHydrogrammeQruis();
        Assert.assertNotNull(loi);
        assertEquals(loi.getNom(), "LoiTQruis_HydrogrammeRuis2");

        final CatEMHBranche noeud = crueData.findBrancheByReference(brancheCast.getEmh().getNom());
        Assert.assertNotNull(noeud);
        final List<DonCLimM> donclims = noeud.getDCLM();
        Assert.assertNotNull(donclims);
        boolean trouve = false;
        for (final DonCLimM donclim : donclims) {
          if (donclim instanceof CalcTransBrancheSaintVenantQruis) {
            trouve = true;
            final CalcTransBrancheSaintVenantQruis calcTransBrancheSaintVenantQruis = (CalcTransBrancheSaintVenantQruis) donclim;
            Assert.assertTrue(calcTransBrancheSaintVenantQruis.getUserActive());
            loi = calcTransBrancheSaintVenantQruis.getHydrogrammeQruis();
            Assert.assertNotNull(loi);
            assertEquals(loi.getNom(), "LoiTQruis_HydrogrammeRuis2");
            assertEquals(loi.getEvolutionFF().getPtEvolutionFF().size(), 2);
            break;
          }
        }
        Assert.assertTrue(trouve);
      }
      count++;
    }
  }

  private static void testCalcTransBrancheOrificeManoeuvre(CrueData crueData, CalcTrans calculT) {
    for (final CalcTransBrancheOrificeManoeuvre brancheCast : calculT.getDclm(CalcTransBrancheOrificeManoeuvre.class)) {
      assertEquals(brancheCast.getSensOuv(), EnumSensOuv.OUV_VERS_HAUT);
      LoiDF loi = brancheCast.getManoeuvre();
      Assert.assertNotNull(loi);
      assertEquals("LoiTOuv_Br_B8", loi.getNom());

      final CatEMHBranche noeud = crueData.findBrancheByReference(brancheCast.getEmh().getNom());
      Assert.assertNotNull(noeud);
      final List<DonCLimM> donclims = noeud.getDCLM();
      Assert.assertNotNull(donclims);
      boolean trouve = false;
      for (final DonCLimM donclim : donclims) {
        if (donclim instanceof CalcTransBrancheOrificeManoeuvre) {
          trouve = true;
          final CalcTransBrancheOrificeManoeuvre donClimFound = (CalcTransBrancheOrificeManoeuvre) donclim;
          Assert.assertTrue(donClimFound.getUserActive());
          assertEquals(donClimFound.getSensOuv(), EnumSensOuv.OUV_VERS_HAUT);
          loi = donClimFound.getManoeuvre();
          Assert.assertNotNull(loi);
          assertEquals("LoiTOuv_Br_B8", loi.getNom());
          assertEquals(loi.getEvolutionFF().getPtEvolutionFF().size(), 4);
          break;
        }
      }
      Assert.assertTrue(trouve);
    }
  }

  private static void testCalcTransNoeudNiveauContinuTarage(CrueData crueData, CalcTrans calculT) {
    final CalcTransNoeudNiveauContinuTarage noeudTarrage = calculT.getDclm(CalcTransNoeudNiveauContinuTarage.class)
        .iterator().next();
    LoiFF loiff = noeudTarrage.getTarage();
    Assert.assertNotNull(loiff);
    assertEquals("LoiQZimp_Nd_N5",loiff.getNom());

    CatEMHNoeud noeud = crueData.findNoeudByReference(noeudTarrage.getEmh().getNom());
    Assert.assertNotNull(noeud);
    List<DonCLimM> donclims = noeud.getDCLM();
    Assert.assertNotNull(donclims);
    boolean trouve = false;
    for (final DonCLimM donclim : donclims) {
      if (donclim instanceof CalcTransNoeudNiveauContinuTarage) {
        trouve = true;
        final CalcTransNoeudNiveauContinuTarage calcTransNoeudNiveauContinuTarage = (CalcTransNoeudNiveauContinuTarage) donclim;
        Assert.assertTrue(calcTransNoeudNiveauContinuTarage.getUserActive());
        loiff = calcTransNoeudNiveauContinuTarage.getTarage();
        Assert.assertNotNull(loiff);
        assertEquals("LoiQZimp_Nd_N5",loiff.getNom());
        assertEquals(loiff.getEvolutionFF().getPtEvolutionFF().size(), 10);
        break;
      }
    }
    Assert.assertTrue(trouve);
  }

  private static void testCalcTransNoeudQapp(CrueData crueData, CalcTrans calculT) {
    final CalcTransNoeudQapp noeudQapp = calculT.getDclm(CalcTransNoeudQapp.class).iterator().next();
    LoiDF loi = noeudQapp.getHydrogrammeQapp();
    Assert.assertNotNull(loi);
    assertEquals( "LoiTQapp_Nd_N1",loi.getNom());

    CatEMHNoeud noeud = crueData.findNoeudByReference(noeudQapp.getEmh().getNom());
    Assert.assertNotNull(noeud);
    List<DonCLimM> donclims = noeud.getDCLM();
    Assert.assertNotNull(donclims);
    boolean trouve = false;
    for (final DonCLimM donclim : donclims) {
      if (donclim instanceof CalcTransNoeudQapp) {
        trouve = true;
        final CalcTransNoeudQapp calcTransNoeudQapp = (CalcTransNoeudQapp) donclim;
        Assert.assertTrue(calcTransNoeudQapp.getUserActive());
        loi = calcTransNoeudQapp.getHydrogrammeQapp();
        Assert.assertNotNull(loi);
        assertEquals("LoiTQapp_Nd_N1",loi.getNom());
        assertEquals(loi.getEvolutionFF().getPtEvolutionFF().size(), 17);
        break;
      }
    }
    Assert.assertTrue(trouve);
  }

  private static void testCalcPseudoPermCasierProfilQruis(CrueData crueData, CalcPseudoPerm calculP) {
    for (final CalcPseudoPermCasierProfilQruis casier : calculP.getDclm(CalcPseudoPermCasierProfilQruis.class)) {
      assertDoubleEquals(casier.getQruis(), 0.0);

      final CatEMHCasier casierEMH = crueData.findCasierByReference(casier.getEmh().getNom());
      Assert.assertNotNull(casierEMH);
      final List<DonCLimM> donclims = casierEMH.getDCLM();
      Assert.assertNotNull(donclims);
      boolean trouve = false;
      for (final DonCLimM donclim : donclims) {
        if (donclim instanceof CalcPseudoPermCasierProfilQruis) {
          trouve = true;
          final CalcPseudoPermCasierProfilQruis calcPseudoPermCasierProfilQruis = (CalcPseudoPermCasierProfilQruis) donclim;
          Assert.assertTrue(calcPseudoPermCasierProfilQruis.getUserActive());
          assertDoubleEquals(calcPseudoPermCasierProfilQruis.getQruis(), 0.0);
          break;
        }
      }
      Assert.assertTrue(trouve);
    }
  }

  private static void testCalcPseudoPermBrancheSaintVenantQruis(CrueData crueData, CalcPseudoPerm calculP) {
    int count = 0;
    for (final CalcPseudoPermBrancheSaintVenantQruis branche : calculP.getDclm(CalcPseudoPermBrancheSaintVenantQruis.class)) {
      // Test que le 1er BrancheSaintVenantQruis
      if (count == 0) {
        assertDoubleEquals(branche.getQruis(), 0.0);
        final CatEMHBranche brancheEMH = crueData.findBrancheByReference(branche.getEmh().getNom());
        Assert.assertNotNull(brancheEMH);
        final List<DonCLimM> donclims = brancheEMH.getDCLM();
        Assert.assertNotNull(donclims);
        boolean trouve = false;
        for (final DonCLimM donclim : donclims) {
          if (donclim instanceof CalcPseudoPermBrancheSaintVenantQruis) {
            trouve = true;
            final CalcPseudoPermBrancheSaintVenantQruis calcPseudoPermBrancheSaintVenantQruis = (CalcPseudoPermBrancheSaintVenantQruis) donclim;
            Assert.assertTrue(calcPseudoPermBrancheSaintVenantQruis.getUserActive());
            assertDoubleEquals(calcPseudoPermBrancheSaintVenantQruis.getQruis(), 0.0);
            break;
          }
        }
        Assert.assertTrue(trouve);
      }
      count++;
    }
  }

  private static void testPseudoPermBrancheOrificeManoeuvre(CalcPseudoPerm calculP) {
    for (final CalcPseudoPermBrancheOrificeManoeuvre branche : calculP.getDclm(CalcPseudoPermBrancheOrificeManoeuvre.class)) {


      assertEquals(branche.getSensOuv(), EnumSensOuv.OUV_VERS_HAUT);
      assertDoubleEquals(branche.getOuv(), 100.0);

      final CatEMHBranche brancheEMH = (CatEMHBranche) branche.getEmh();
      Assert.assertNotNull(brancheEMH);
      final List<DonCLimM> donclims = brancheEMH.getDCLM();
      Assert.assertNotNull(donclims);
      boolean trouve = false;
      for (final DonCLimM donclim : donclims) {
        if (donclim instanceof CalcPseudoPermBrancheOrificeManoeuvre) {
          trouve = true;
          final CalcPseudoPermBrancheOrificeManoeuvre calcPseudoPermBrancheOrificeManoeuvre = (CalcPseudoPermBrancheOrificeManoeuvre) donclim;
          Assert.assertTrue(calcPseudoPermBrancheOrificeManoeuvre.getUserActive());
          assertEquals(calcPseudoPermBrancheOrificeManoeuvre.getSensOuv(), EnumSensOuv.OUV_VERS_HAUT);
          assertDoubleEquals(((CalcPseudoPermBrancheOrificeManoeuvre) donclim).getOuv(), 100.0);
          break;
        }
      }
      Assert.assertTrue(trouve);
    }
  }

  private static void testPseudoPermNoeudNiveauContinuZimpInCP1(CrueData crueData, CalcPseudoPerm calculP) {
    final CalcPseudoPermNoeudNiveauContinuZimp noeudNivContZ = calculP.getDclm(CalcPseudoPermNoeudNiveauContinuZimp.class)
        .get(0);
    assertDoubleEquals(noeudNivContZ.getZimp(), 2.0);

    CatEMHNoeud noeud = crueData.findNoeudByReference(noeudNivContZ.getEmh().getNom());
    Assert.assertNotNull(noeud);
    List<DonCLimM> donclims = noeud.getDCLM();
    Assert.assertNotNull(donclims);
    boolean trouve = false;
    for (final DonCLimM donclim : donclims) {
      if (donclim instanceof CalcPseudoPermNoeudNiveauContinuZimp) {

        trouve = true;
        final CalcPseudoPermNoeudNiveauContinuZimp calcPseudoPermNoeudNiveauContinuZimp = (CalcPseudoPermNoeudNiveauContinuZimp) donclim;
        assertDoubleEquals(calcPseudoPermNoeudNiveauContinuZimp.getZimp(), 2);
        Assert.assertTrue(calcPseudoPermNoeudNiveauContinuZimp.getUserActive());
        break;
      }
    }
    Assert.assertTrue(trouve);
  }

  private static void testPseudoPermNoeudQappInCP1(CrueData crueData, CalcPseudoPerm calculP) {
    final CalcPseudoPermNoeudQapp noeudNivContQapp = calculP.getDclm(CalcPseudoPermNoeudQapp.class).get(0);
    assertDoubleEquals(noeudNivContQapp.getQapp(), 100.0);
    Assert.assertTrue(noeudNivContQapp.getUserActive());
    CatEMHNoeud noeud = crueData.findNoeudByReference(noeudNivContQapp.getEmh().getNom());
    Assert.assertNotNull(noeud);
    List<DonCLimM> donclims = noeud.getDCLM();
    Assert.assertNotNull(donclims);
    boolean trouve = false;
    for (final DonCLimM donclim : donclims) {
      if (donclim instanceof CalcPseudoPermNoeudQapp) {
        trouve = true;
        assertDoubleEquals(((CalcPseudoPermNoeudQapp) donclim).getQapp(), 100.0);
        break;
      }
    }
    Assert.assertTrue(trouve);
  }

  public CrueData readData(final CoeurConfigContrat version, final String drsoFile, final String dlhyFile, final File dclmFile) {
    final CtuluLog analyzer = new CtuluLog();
    // -- lecture drso --//
    final CrueData data = (CrueData) Crue10FileFormatFactory.getVersion(CrueFileType.DRSO, version)
        .read(drsoFile, analyzer, CrueDRSOFileTest.createDefault(version)).getMetier();
    testAnalyser(analyzer);
    // -- lecture dlhy --//
    final List<Loi> listeLois = (List<Loi>) Crue10FileFormatFactory.getVersion(CrueFileType.DLHY, version)
        .read(dlhyFile, analyzer, data).getMetier();
    testAnalyser(analyzer);
    Assert.assertNotNull(listeLois);
    // -- lecture de dclm --//
    final DonCLimMScenario jeuDonnees = (DonCLimMScenario) Crue10FileFormatFactory.getInstance()
        .getFileFormat(CrueFileType.DCLM, version).read(dclmFile, analyzer, data).getMetier();
    testAnalyser(analyzer);
    return data;
  }
}
