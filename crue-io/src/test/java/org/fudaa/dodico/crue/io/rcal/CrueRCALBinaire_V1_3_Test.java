package org.fudaa.dodico.crue.io.rcal;

import files.FilesForTest;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.ItemResDao;
import org.fudaa.dodico.crue.io.res.*;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * @author deniger
 */
public class CrueRCALBinaire_V1_3_Test {
  private final List<File> filesToDelete = new ArrayList<>();

  private static class Result {
    RCalTimeStepBuilder.Result timeStepExtracted;
    List<ResCatEMHContent> categories;
    CrueDaoRCAL rcal;
  }

  @Test
  public void testReadResultatWithRegul() throws IOException {
    final Result extracted = extracted(FilesForTest.RCAL_V1_3_REGUL, FilesForTest.RCAL_BIN_PATH_V1_3_REGUL);
    final Crue10ResultatPasDeTempsDelegate timeSteps = new Crue10ResultatPasDeTempsDelegate(extracted.timeStepExtracted.getOrderedKey(),
        extracted.timeStepExtracted.getEntries());
    final ResTypeEMHContent typeSectionIdem = extracted.categories.get(2).getResTypeEMHContent().get(0);
    assertEquals("SectionIdem", typeSectionIdem.getEmhType());
    final ItemResDao section = typeSectionIdem.getItemResDao().get(0);
    assertEquals("St_P145.000B", section.getNomRef());
    final ResultatCalculCrue10 resSection = new ResultatCalculCrue10(typeSectionIdem.getItemPositionInBytes(section.getNomRef()),
        section.getNbrMot() * extracted.rcal.getParametrage().getNbrOctetMot(),
        typeSectionIdem.getResVariablesContent(), timeSteps,
        typeSectionIdem.getEmhType(), section.getNomRef().toUpperCase());

    Map<String, Object> read = resSection.read(new ResultatTimeKey("Cc_P01"));
    assertEquals(242.3980802919837, ((Double) read.get("z")).doubleValue(), 1e-6);

    read = resSection.read(new ResultatTimeKey("Cc_T01", 345600000, false));
    assertEquals(246.4848137048179, ((Double) read.get("z")).doubleValue(), 1e-6);


    final ResTypeEMHContent typeModele = extracted.categories.get(4).getResTypeEMHContent().get(0);
    assertEquals("ModeleRegul", typeModele.getEmhType());
    final ItemResDao modelRegul = typeModele.getItemResDao().get(0);
    assertEquals("Mo_BY", modelRegul.getNomRef());
    final ResultatCalculCrue10 resStr1 = new ResultatCalculCrue10(typeModele.getItemPositionInBytes(modelRegul.getNomRef()),
        modelRegul.getNbrMot() * extracted.rcal.getParametrage().getNbrOctetMot(),
        typeModele.getResVariablesContent(), timeSteps,
        typeModele.getEmhType(), modelRegul.getNomRef().toUpperCase());

    Assert.assertTrue(resStr1.isQRegul("qSavieres"));
    Assert.assertTrue(resStr1.isQRegul("qpilBY"));
    Assert.assertTrue(resStr1.isZRegul("zLac"));
    Assert.assertTrue(resStr1.isZRegul("zChanaz"));

    Assert.assertFalse(resStr1.isZRegul("qpilBY"));
    Assert.assertFalse(resStr1.isQRegul("zChanaz"));
    Assert.assertFalse(resStr1.isZRegul("qpilNonExisting"));
    Assert.assertFalse(resStr1.isQRegul("zChanazNonExisting"));

    List<String> qzRegulVariables = resStr1.getQZRegulVariablesId();
    List<String> expect= Arrays.asList("qobj","qpilBY","qentBY","qeffBY","qSavieres","zLac","zChanaz","zamQbg2","zavQbg2");
    Assert.assertEquals(expect,qzRegulVariables);

    read = resStr1.read(new ResultatTimeKey("Cc_P01"));
    assertEquals(12, read.size());
    read = resStr1.read(new ResultatTimeKey("Cc_T01", 345600000, false));
    assertEquals(12, read.size());
    assertEquals(-1, ((Double) read.get("mode")).intValue());
    assertEquals(5, ((Double) read.get("loiRegul")).intValue());
    assertEquals(235.5, ((Double) read.get("zcns")).doubleValue(), 1e-4);
    assertEquals(1563.7613, ((Double) read.get("qobj")).doubleValue(), 1e-4);
    assertEquals(1641.7282, ((Double) read.get("qpilBY")).doubleValue(), 1e-4);
    assertEquals(1899.3602, ((Double) read.get("qentBY")).doubleValue(), 1e-4);
    assertEquals(1532.9982, ((Double) read.get("qeffBY")).doubleValue(), 1e-4);
    assertEquals(246.9302, ((Double) read.get("qSavieres")).doubleValue(), 1e-4);
    assertEquals(234.3279, ((Double) read.get("zLac")).doubleValue(), 1e-4);
    assertEquals(233.5443, ((Double) read.get("zChanaz")).doubleValue(), 1e-4);
    assertEquals(233.5431, ((Double) read.get("zamQbg2")).doubleValue(), 1e-4);
    assertEquals(233.4145, ((Double) read.get("zavQbg2")).doubleValue(), 1e-4);
  }

  @Test
  public void testReadResultatNoRegul() throws IOException {
    final Result extracted = extracted(FilesForTest.RCAL_V1_3_NOREGUL, FilesForTest.RCAL_BIN_PATH_V1_3_NOREGUL);
    final Crue10ResultatPasDeTempsDelegate timeSteps = new Crue10ResultatPasDeTempsDelegate(extracted.timeStepExtracted.getOrderedKey(),
        extracted.timeStepExtracted.getEntries());
    final ResTypeEMHContent typeSectionIdem = extracted.categories.get(2).getResTypeEMHContent().get(0);
    assertEquals("SectionIdem", typeSectionIdem.getEmhType());
    final ItemResDao section = typeSectionIdem.getItemResDao().get(0);
    assertEquals("St_P145.000B", section.getNomRef());
    final ResultatCalculCrue10 resSection = new ResultatCalculCrue10(typeSectionIdem.getItemPositionInBytes(section.getNomRef()),
        section.getNbrMot() * extracted.rcal.getParametrage().getNbrOctetMot(),
        typeSectionIdem.getResVariablesContent(), timeSteps,
        typeSectionIdem.getEmhType(), section.getNomRef().toUpperCase());

    Map<String, Object> read = resSection.read(new ResultatTimeKey("Cc_P01"));
    assertEquals(242.3980802919837, (Double) read.get("z"), 1e-6);

    read = resSection.read(new ResultatTimeKey("Cc_T01", 0, false));
    assertEquals(242.3981, (Double) read.get("z"), 1e-4);

    read = resSection.read(new ResultatTimeKey("Cc_T01", 327600000, false));
    assertEquals(1542.7311, ((Double) read.get("q")).doubleValue(), 1e-4);
    assertEquals(806.4655, ((Double) read.get("stot")).doubleValue(), 1e-4);
    assertEquals(1.913, ((Double) read.get("vact")).doubleValue(), 1e-4);
    assertEquals(6.599, ((Double) read.get("vc")).doubleValue(), 1e-4);
    assertEquals(246.951, ((Double) read.get("z")).doubleValue(), 1e-4);

    read = resSection.read(new ResultatTimeKey("Cc_T01", 342000000, false));
    assertEquals(1311.9281, ((Double) read.get("q")).doubleValue(), 1e-4);
    assertEquals(740.2342, ((Double) read.get("stot")).doubleValue(), 1e-4);
    assertEquals(1.7723, ((Double) read.get("vact")).doubleValue(), 1e-4);
    assertEquals(6.3497, ((Double) read.get("vc")).doubleValue(), 1e-4);
    assertEquals(246.5846, ((Double) read.get("z")).doubleValue(), 1e-4);


    read = resSection.read(new ResultatTimeKey("Cc_T01", 345600000, false));
    //TODO to be confirmed with CNR.
    assertEquals(1253.1943, ((Double) read.get("q")).doubleValue(), 1e-4);
    assertEquals(722.2576, ((Double) read.get("stot")).doubleValue(), 1e-4);
    assertEquals(1.7351, ((Double) read.get("vact")).doubleValue(), 1e-4);
    assertEquals(6.2786, ((Double) read.get("vc")).doubleValue(), 1e-4);
    assertEquals(246.4848, ((Double) read.get("z")).doubleValue(), 1e-4);


    //pas de résultats pour le mode sans régul.
    final ResTypeEMHContent typeModele = extracted.categories.get(4).getResTypeEMHContent().get(0);
    assertEquals("ModeleRegul", typeModele.getEmhType());
    final ItemResDao modelRegul = typeModele.getItemResDao().get(0);
    assertEquals("Mo_BY", modelRegul.getNomRef());
    final ResultatCalculCrue10 resStr1 = new ResultatCalculCrue10(typeModele.getItemPositionInBytes(modelRegul.getNomRef()),
        modelRegul.getNbrMot() * extracted.rcal.getParametrage().getNbrOctetMot(),
        typeModele.getResVariablesContent(), timeSteps,
        typeModele.getEmhType(), modelRegul.getNomRef().toUpperCase());
    read = resStr1.read(new ResultatTimeKey("Cc_P01"));
    assertEquals(0, read.size());
  }

  private Result extracted(String rcalFile, List<String> files) throws IOException {
    final File createTempDir = CtuluLibFile.createTempDir();
    filesToDelete.add(createTempDir);

    for (String fileName : files) {
      final File file = new File(createTempDir, StringUtils.substringAfterLast(fileName, "/"));
      CtuluLibFile.getFileFromJar(fileName, file);
      assertTrue(file.exists());
    }
    final CrueDaoRCAL rcal = readXml(rcalFile);
    final ResTypeEMHBuilder typeBuilder = new ResTypeEMHBuilder(rcal);
    final CrueIOResu<List<ResCatEMHContent>> extract = typeBuilder.extract();
    final RCalTimeStepBuilder timeStepBuilder = new RCalTimeStepBuilder();
    final RCalTimeStepBuilder.Result timeStepExtracted = timeStepBuilder.extract(rcal, createTempDir);
    final List<ResCatEMHContent> categories = extract.getMetier();
    final Result res = new Result();
    res.categories = categories;
    res.timeStepExtracted = timeStepExtracted;
    res.rcal = rcal;
    return res;
  }

  @After
  public void cleanFiles() {
    for (final File file : filesToDelete) {
      if (file.isDirectory()) {
        CtuluLibFile.deleteDir(file);
      } else {
        file.delete();
      }
    }
    filesToDelete.clear();
  }

  @Test
  public void testReadXml() {
    CrueDaoRCAL rcal = readXml(FilesForTest.RCAL_V1_3_REGUL);
    assertNotNull(rcal);
    rcal = readXml(FilesForTest.RCAL_V1_3_NOREGUL);
    assertNotNull(rcal);
  }

  private CrueDaoRCAL readXml(String rcalFile) {
    final URL resource = AbstractIOParentTest.getResource(rcalFile);
    final Crue10FileFormat version = Crue10FileFormatFactory.getVersion(CrueFileType.RCAL, TestCoeurConfig.INSTANCE);
    final CtuluLog log = new CtuluLog();
    final CrueIOResu read = version.read(resource, log, null);
    assertFalse(log.containsErrorOrSevereError());
    final CrueDaoRCAL rcal = (CrueDaoRCAL) read.getMetier();
    return rcal;
  }
}
