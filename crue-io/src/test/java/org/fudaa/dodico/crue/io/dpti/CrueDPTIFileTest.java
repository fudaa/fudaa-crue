package org.fudaa.dodico.crue.io.dpti;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.drso.CrueDRSOFileTest;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.DonPrtCIniBranche;
import org.fudaa.dodico.crue.metier.emh.DonPrtCIniBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.emh.DonPrtCIniNoeudNiveauContinu;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * Test pour DPTI
 *
 * @author Adrien Hadoux
 */
public class CrueDPTIFileTest extends AbstractIOParentTest {
    public CrueDPTIFileTest() {
        super(Crue10FileFormatFactory.getVersion(CrueFileType.DPTI, TestCoeurConfig.INSTANCE), FilesForTest.DPTI_V1_3);
    }


    @Test
    public void testValide_V_1_1_1() {
        final CtuluLog log = new CtuluLog();
        final boolean valide = Crue10FileFormatFactory.getInstance()
                .getFileFormat(CrueFileType.DPTI, TestCoeurConfig.getInstance(Crue10VersionConfig.V_1_1_1))
                .isValide(FilesForTest.DPTI_V1_1_1, log);
        if (log.containsErrorOrSevereError()) {
            log.printResume();
        }
        Assert.assertTrue(valide);
    }

    @Test
    public void testValide_V_1_2() {
        final CtuluLog log = new CtuluLog();
        final boolean valide = Crue10FileFormatFactory.getInstance()
                .getFileFormat(CrueFileType.DPTI, TestCoeurConfig.getInstance(Crue10VersionConfig.V_1_2))
                .isValide(FilesForTest.DPTI_V1_2, log);
        if (log.containsErrorOrSevereError()) {
            log.printResume();
        }
        Assert.assertTrue(valide);
    }

    @Test
    public void testLectureModele_V_1_2() {
        final CrueData jeuDonneesLue = readData(FilesForTest.DRSO_V1_2, FilesForTest.DPTI_V1_2, TestCoeurConfig.INSTANCE_1_2);
        testDonnees(jeuDonneesLue);
    }

    @Test
    public void testLectureModele() {
        final CrueData jeuDonneesLue = readData(FilesForTest.DRSO_V1_3, FilesForTest.DPTI_V1_3, TestCoeurConfig.INSTANCE);
        testDonnees(jeuDonneesLue);
    }

    private static CrueData readData(String drsoFile, String dptiFile, CoeurConfigContrat version) {
        final CtuluLog analyzer = new CtuluLog();
        final CrueData jeuDonneesLue = (CrueData) Crue10FileFormatFactory
                .getVersion(CrueFileType.DRSO, version)
                .read(drsoFile, analyzer, CrueDRSOFileTest.createDefault(version)).getMetier();
        Crue10FileFormatFactory
                .getVersion(CrueFileType.DPTI, version).read(dptiFile, analyzer, jeuDonneesLue);
        return jeuDonneesLue;
    }

    @Test
    public void testConverterEMH() {

        final CtuluLog analyzer = new CtuluLog();
        // -- lecture --//
        final Crue10FileFormat<Object> drsoFormat = getFileFormat(CrueFileType.DRSO,
                TestCoeurConfig.INSTANCE);
        CrueData jeuDonneesLue = (CrueData) drsoFormat.read(FilesForTest.DRSO_V1_3, analyzer,
                CrueDRSOFileTest.createDefault(TestCoeurConfig.INSTANCE)).getMetier();
        format.read(FilesForTest.DPTI_V1_3, analyzer, jeuDonneesLue);
        testAnalyser(analyzer);

        // -- ecriture --//
        final File fichierEcriture = createTempFile();
        format.write(jeuDonneesLue, fichierEcriture, analyzer);

        // -- relecture --//
        jeuDonneesLue = (CrueData) drsoFormat.read(FilesForTest.DRSO_V1_3, analyzer,
                CrueDRSOFileTest.createDefault(TestCoeurConfig.INSTANCE)).getMetier();
        format.read(fichierEcriture, analyzer, jeuDonneesLue);

        testDonnees(jeuDonneesLue);
    }

    private void testDonnees(final CrueData jeuDonneesLue) {
        // -- test de N1 <Zini>3.676</Zini> --//
        Assert.assertEquals(3.676,
                ((DonPrtCIniNoeudNiveauContinu) EMHHelper.collectDPTI(jeuDonneesLue.findNoeudByReference("Nd_N1")).get(0))
                        .getZini(), 0.001);

        // -- test B4 <Qini>98.7</Qini> <Qruis>18.5</Qruis> --//
        Assert.assertEquals(100,
                ((DonPrtCIniBranche) EMHHelper.collectDPTI(jeuDonneesLue.findBrancheByReference("Br_B4")).get(0)).getQini(),
                0.001);
        Assert.assertEquals(0, ((DonPrtCIniBrancheSaintVenant) EMHHelper
                .collectDPTI(jeuDonneesLue.findBrancheByReference("Br_B4")).get(0)).getQruis(), 0.001);
    }

    /**
     *
     */
    @Test
    public void testEcritureModele3() {

        CtuluLog analyzer = new CtuluLog();
        final CrueData jeuDonneesLue = readData(FilesForTest.DRSO_V1_3, FilesForTest.DPTI_V1_3, TestCoeurConfig.INSTANCE);
        testAnalyser(analyzer);

        analyzer = new CtuluLog();
        final File fichierEcriture = createTempFile();
        format.write(jeuDonneesLue, fichierEcriture, analyzer);
        testAnalyser(analyzer);
        final boolean valide = format.isValide(fichierEcriture, analyzer);
        testAnalyser(analyzer);
        Assert.assertTrue(valide);
        format.read(fichierEcriture, analyzer, jeuDonneesLue);
    }
}
