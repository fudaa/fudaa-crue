package org.fudaa.dodico.crue.io.etu;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Test unitaires du fichier ETU. Uniquement en lecture.
 *
 * @author Adrien Hadoux
 */
public class CrueETUFileTest extends AbstractIOParentTest {

    public CrueETUFileTest() {
        super(Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ETU, TestCoeurConfig.INSTANCE), FilesForTest.ETU_V1_3);
    }

    private void doWrite(final CoeurConfigContrat version, final EMHProjet jeuDonneesLue, boolean is3_6) {
        final File fichierEcriture = createTempFile();
        final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ETU, version);
        final CtuluLog log = new CtuluLog();
        fileFormat.writeMetierDirect(jeuDonneesLue, fichierEcriture, log, version.getCrueConfigMetier());
        Assert.assertTrue(fileFormat.isValide(fichierEcriture, log));
        testAnalyser(log);
        final EMHProjet jeuDonneesLueApresEcriture = readData(fichierEcriture, version);
        if (is3_6) {
            testEtu_3_6(jeuDonneesLueApresEcriture);
        } else {
            testEtu(jeuDonneesLueApresEcriture);
        }
    }

    @Test
    public void testLecture_V1_3_Ex() {
        try {
            final EMHProjet jeuDonneesLue = readData(FilesForTest.ETU_V1_3, TestCoeurConfig.INSTANCE);
            testEtu(jeuDonneesLue);
            Assert.assertEquals(EMHProjectInfos.CONFIG, jeuDonneesLue.getInfos().getDirectories().get("CONFIG"));
        } catch (final Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Test lecture", e);
        }
    }

    @Test
    public void testLecture_V1_3_3_6() {
        try {
            final EMHProjet jeuDonneesLue = readData(FilesForTest.ETU_3_6_V1_3, TestCoeurConfig.INSTANCE);
            testEtu_3_6(jeuDonneesLue);
            Assert.assertEquals(EMHProjectInfos.CONFIG, jeuDonneesLue.getInfos().getDirectories().get("CONFIG"));
        } catch (final Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Test lecture", e);
        }
    }

    @Test
    public void testEcriture_V1_3_Ex() {
        try {
            final CoeurConfigContrat lastVersion = TestCoeurConfig.INSTANCE;
            final EMHProjet jeuDonneesLue = readData(FilesForTest.ETU_V1_3, lastVersion);
            final File fichierEcriture = createTempFile();
            Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ETU, lastVersion);
            final CtuluLog log = new CtuluLog();
            fileFormat.writeMetierDirect(jeuDonneesLue, fichierEcriture, log, CrueConfigMetierForTest.DEFAULT);
            Assert.assertTrue(fileFormat.isValide(fichierEcriture, log));
            testAnalyser(log);
            final EMHProjet jeuDonneesLueApresEcriture = readData(fichierEcriture, lastVersion);
            testEtu(jeuDonneesLueApresEcriture);
            Assert.assertEquals(EMHProjectInfos.CONFIG, jeuDonneesLueApresEcriture.getInfos().getDirectories().get("CONFIG"));
            //on ecrit dans le format 1.2 pour vérifier la non ecriture des fichier dreg.
            fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ETU, TestCoeurConfig.INSTANCE_1_2);
            fileFormat.writeMetierDirect(jeuDonneesLue, fichierEcriture, log, CrueConfigMetierForTest.DEFAULT_1_2);
            Assert.assertTrue(fileFormat.isValide(fichierEcriture, log));
            //on ecrit dans le format 1.1.1 pour vérifier la non ecriture des scenarios liés.
            fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ETU, TestCoeurConfig.INSTANCE_1_1_1);
            fileFormat.writeMetierDirect(jeuDonneesLue, fichierEcriture, log, CrueConfigMetierForTest.DEFAULT_1_1_1);
            Assert.assertTrue(fileFormat.isValide(fichierEcriture, log));

        } catch (final Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Test écriture", e);
        }
    }

    @Test
    public void testEcriture_V1_3_3_6() {
        try {
            final CoeurConfigContrat version = TestCoeurConfig.INSTANCE;
            final EMHProjet jeuDonneesLue = readData(FilesForTest.ETU_3_6_V1_3, version);
            doWrite(version, jeuDonneesLue, true);
        } catch (final Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Test écriture", e);
        }
    }

    @Test
    public void testEcriture_V1_1_1_Ex() {
        try {
            final CoeurConfigContrat version = TestCoeurConfig.INSTANCE_1_1_1;
            final EMHProjet jeuDonneesLue = readData(FilesForTest.ETU_V1_1_1, version);
            doWrite(version, jeuDonneesLue, false);
        } catch (final Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Test écriture", e);
        }
    }

    @Test
    public void testEcriture_V1_2_Ex() {
        try {
            final CoeurConfigContrat version = TestCoeurConfig.INSTANCE_1_2;
            final EMHProjet jeuDonneesLue = readData(FilesForTest.ETU_V1_2, version);
            doWrite(version, jeuDonneesLue, false);
        } catch (final Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Test écriture", e);
        }
    }

    @Test
    public void testValide_V1_1_1_Ex() {
        final CtuluLog analyzer = new CtuluLog();
        final boolean valid = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ETU, TestCoeurConfig.INSTANCE_1_1_1).isValide(
                FilesForTest.ETU_V1_1_1, analyzer);
        testAnalyser(analyzer);
        Assert.assertTrue(valid);
    }

    @Test
    public void testValide_V1_2_Ex() {
        final CtuluLog analyzer = new CtuluLog();
        final boolean valid = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ETU, TestCoeurConfig.INSTANCE_1_2).isValide(
                FilesForTest.ETU_V1_2, analyzer);
        testAnalyser(analyzer);
        Assert.assertTrue(valid);
    }

    @Test
    public void testValide_V1_3_Ex() {
        final CtuluLog analyzer = new CtuluLog();
        final boolean valid = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ETU, TestCoeurConfig.INSTANCE).isValide(
                FilesForTest.ETU_V1_3, analyzer);
        testAnalyser(analyzer);
        Assert.assertTrue(valid);
    }

    @Test
    public void testLecture_V1_1_1_Ex() {
        final EMHProjet jeuDonneesLue = readData(FilesForTest.ETU_V1_1_1, TestCoeurConfig.INSTANCE_1_1_1);
        testEtu(jeuDonneesLue);
    }

    @Test
    public void testLecture_V1_2_Ex() {
        final EMHProjet jeuDonneesLue = readData(FilesForTest.ETU_V1_2, TestCoeurConfig.INSTANCE_1_2);
        testEtu(jeuDonneesLue);
    }

    @Test
    public void testLecture_V1_2_3_6() {
        final EMHProjet jeuDonneesLue = readData(FilesForTest.ETU_3_6_V1_2, TestCoeurConfig.INSTANCE_1_2);
        testEtu_3_6(jeuDonneesLue);
    }

    @Test
    public void testLecture_V1_1_1_NonActive() {
        final EMHProjet jeuDonneesLue = readData(FilesForTest.ETU_V1_1_1_NONACTIVE, TestCoeurConfig.INSTANCE_1_1_1);
        Assert.assertFalse(jeuDonneesLue.getScenario("Sc_M3-0_c10").isActive());
        Assert.assertFalse(jeuDonneesLue.getModele("Mo_M3-0_c10").isActive());
        Assert.assertFalse(jeuDonneesLue.getSousModele("Sm_M3-0_c10").isActive());
    }

    private static EMHProjet readData(final String fichier, final CoeurConfigContrat version) {
        final CtuluLog analyzer = new CtuluLog();
        final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ETU, version);
        final EMHProjet jeuDonneesLue = (EMHProjet) fileFormat.read(fichier, analyzer, createDefault(version)).getMetier();
        testAnalyser(analyzer);
        jeuDonneesLue.setPropDefinition(version);
        return jeuDonneesLue;
    }

    public static EMHProjet readData(final File fichier, final CoeurConfigContrat version) {
        final CtuluLog analyzer = new CtuluLog();
        final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ETU, version);
        final CrueIOResu<Object> readIO = fileFormat.read(fichier, analyzer, createDefault(version));
        testAnalyser(readIO.getAnalyse());
        final EMHProjet jeuDonneesLue = (EMHProjet) readIO.getMetier();
        jeuDonneesLue.setPropDefinition(version);
        testAnalyser(analyzer);
        return jeuDonneesLue;
    }

    private void testEtu(final EMHProjet jeuDonneesLue) {
        Assert.assertNotNull(jeuDonneesLue);

        boolean containsDREG = Crue10VersionConfig.isUpperThan_V1_2(jeuDonneesLue.getCoeurConfig().getXsdVersion());
        int nbFiles = containsDREG ? 19 : 18;

        Assert.assertEquals(nbFiles, jeuDonneesLue.getInfos().getBaseFichiersProjets().size());
        for (final ManagerEMHScenario managerEMHScenario : jeuDonneesLue.getListeScenarios()) {
            if (managerEMHScenario.isCrue9()) {
                Assert.assertTrue(managerEMHScenario.isActive());
            }
        }
        for (final ManagerEMHModeleBase manager : jeuDonneesLue.getListeModeles()) {
            if (manager.isCrue9()) {
                Assert.assertTrue(manager.isActive());
            }
        }
        for (final ManagerEMHSousModele manager : jeuDonneesLue.getListeSousModeles()) {
            if (manager.isCrue9()) {
                Assert.assertTrue(manager.isActive());
            }
        }

        Assert.assertEquals(CrueFileType.DCLM, jeuDonneesLue.getInfos().getBaseFichiersProjets().get(0).getType());
        Assert.assertEquals("M3-0_c10.dclm.xml", jeuDonneesLue.getInfos().getBaseFichiersProjets().get(0).getNom());

        Assert.assertNotNull(jeuDonneesLue.getInfos().getFileFromBase("M3-0_c10.drso.xml"));
        Assert.assertNotNull(jeuDonneesLue.getInfos().getFileFromBase("M3-0_c10.dfrt.xml"));
        if (containsDREG) {
            Assert.assertNotNull(jeuDonneesLue.getInfos().getFileFromBase("M3-0_c10.dreg.xml"));
        }

        // MOModele3 existe.
        Assert.assertNotNull(jeuDonneesLue.getModele("Mo_M3-0_c10"));
        Assert.assertTrue(jeuDonneesLue.getModele("Mo_M3-0_c10").isActive());
        final ManagerEMHScenario scenarioM30c10 = jeuDonneesLue.getScenario("Sc_M3-0_c10");

        Assert.assertNotNull(scenarioM30c10);
        Assert.assertTrue(scenarioM30c10.isActive());
        if (!Crue10VersionConfig.isV1_1_1(jeuDonneesLue.getCoeurConfig())) {
            Assert.assertEquals("Sc_M3-0_c9", scenarioM30c10.getLinkedscenarioCrue9());
        }
        Assert.assertTrue(jeuDonneesLue.getScenario("Sc_M3-0_c9").isActive());

        // scenario SCMod3 contient modele MOModele3
        Assert.assertNotNull(scenarioM30c10.getManagerFils("MO_M3-0_C10"));

        // scMod 3 est scenarïo courant
        Assert.assertEquals(jeuDonneesLue.getScenarioCourant(), scenarioM30c10);
        final ManagerEMHModeleBase modele = jeuDonneesLue.getModele("Mo_M3-0_c10");
        Assert.assertNotNull(modele.getManagerFils("SM_M3-0_C10"));

        Assert.assertTrue(scenarioM30c10.isActive());
        final EMHRun run = scenarioM30c10.getRunFromScenar("RUN20090313090000");
        Assert.assertNotNull(run);
        Assert.assertEquals("Exemple de RUN Crue10", run.getInfosVersion().getCommentaire());
        Assert.assertEquals("Balayn_P", run.getInfosVersion().getAuteurCreation());
        Assert.assertEquals("Battista", run.getInfosVersion().getAuteurDerniereModif());
        Assert.assertEquals("2009-01-22T14:22:00.000", run.getInfosVersion().getDateCreation().toString());
        Assert.assertEquals("2009-01-22T14:22:20.000", run.getInfosVersion().getDateDerniereModif().toString());

    }

    private void testEtu_3_6(final EMHProjet jeuDonneesLue) {
        Assert.assertNotNull(jeuDonneesLue);

        boolean containsDREG = Crue10VersionConfig.isUpperThan_V1_2(jeuDonneesLue.getCoeurConfig().getXsdVersion());
        int nbFiles = containsDREG ? 15 : 14;


        Assert.assertEquals(nbFiles, jeuDonneesLue.getInfos().getBaseFichiersProjets().size());
        Assert.assertEquals(1, jeuDonneesLue.getListeScenarios().size());
        Assert.assertEquals(1, jeuDonneesLue.getListeModeles().size());
        Assert.assertEquals(1, jeuDonneesLue.getListeSousModeles().size());

        Assert.assertEquals(CrueFileType.DCLM, jeuDonneesLue.getInfos().getBaseFichiersProjets().get(0).getType());
        Assert.assertEquals("M3-6_c10.dclm.xml", jeuDonneesLue.getInfos().getBaseFichiersProjets().get(0).getNom());

        Assert.assertNotNull(jeuDonneesLue.getInfos().getFileFromBase("M3-6_c10.drso.xml"));
        Assert.assertNotNull(jeuDonneesLue.getInfos().getFileFromBase("M3-6_c10.dfrt.xml"));
        if (containsDREG) {
            Assert.assertNotNull(jeuDonneesLue.getInfos().getFileFromBase("M3-6_c10.dreg.xml"));
        }

        // MOModele3 existe.
        Assert.assertTrue(jeuDonneesLue.getModele("Mo_M3-6_c10").isActive());
        final ManagerEMHScenario scenario = jeuDonneesLue.getScenario("Sc_M3-6_c10");

        // scenario SCMod3 existe
        Assert.assertNotNull(scenario);
        Assert.assertTrue(scenario.isActive());

        // scenario SCMod3 contient modele MOModele3
        Assert.assertNotNull(scenario.getManagerFils("MO_M3-6_C10"));

        // scMod 3 est scenarïo courant
        Assert.assertEquals(jeuDonneesLue.getScenarioCourant(), scenario);

        final ManagerEMHModeleBase modele = jeuDonneesLue.getModele("Mo_M3-6_c10");
        if (containsDREG) {
            Assert.assertEquals("M3-6_c10.dreg.xml", modele.getFile(CrueFileType.DREG).getNom());
        }

        Assert.assertNotNull(modele.getManagerFils("SM_M3-6_C10"));

    }
}
