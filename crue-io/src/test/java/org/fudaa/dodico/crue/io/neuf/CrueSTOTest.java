package org.fudaa.dodico.crue.io.neuf;

import files.FilesForTest;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.AbstractIOBinaryTestParent;
import org.fudaa.dodico.crue.io.Crue9FileFormatFactory;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author CDE
 */
public class CrueSTOTest extends AbstractIOBinaryTestParent {
  @Test
  public void testLectureSTOModele30() {
    // Lecture STO
    final CrueIOResu<STOSequentialReader> read = readBinaire(Crue9FileFormatFactory
        .getSTOFileFormat(), FilesForTest.STO_MODELE30, null);
    Assert.assertNotNull(read);
    if (read.getAnalyse().containsErrors()) {
      read.getAnalyse().printResume();
    }
    Assert.assertFalse(read.getAnalyse().containsErrors());

    final STOSequentialReader stoSeq = read.getMetier();

    final ResPrtReseauCrue9Adapter alimenteObjetsMetier = STOReader.alimenteObjetsMetier(stoSeq);
    Assert.assertEquals(2, alimenteObjetsMetier.getNbResOnCasier());
    Assert.assertEquals("Ca_N6", alimenteObjetsMetier.getCasierNom(1));
    Assert.assertEquals("Ca_N7", alimenteObjetsMetier.getCasierNom(0));
  }
}
