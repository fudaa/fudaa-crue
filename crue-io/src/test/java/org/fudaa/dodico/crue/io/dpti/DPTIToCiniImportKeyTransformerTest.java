package org.fudaa.dodico.crue.io.dpti;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.CrueFileFormatBuilderDPTI;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.cini.CiniImportKey;
import org.fudaa.dodico.crue.metier.emh.EnumSensOuv;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class DPTIToCiniImportKeyTransformerTest {
  public DPTIToCiniImportKeyTransformerTest() {
  }

  @Test
  public void testExtract() {
    final CrueDataXmlReaderWriterImpl<CrueDaoDPTI, CrueData> readerWriter = new CrueFileFormatBuilderDPTI().createReaderWriter(TestCoeurConfig.INSTANCE);
    CtuluLog log = new CtuluLog();
    final CrueDaoDPTI crueDaoDPTI = readerWriter.readDao(FilesForTest.DPTI_V1_3, log, null);
    final Map<CiniImportKey, Object> cini = new DPTIToCiniImportKeyTransformer().extract(crueDaoDPTI);
    Assert.assertEquals(21, cini.size());
    Assert.assertEquals(EnumSensOuv.OUV_VERS_HAUT, cini.get(new CiniImportKey("BR_B8", "SENSOUV")));
    Assert.assertEquals("100.0", cini.get(new CiniImportKey("BR_B4", "QINI")).toString());
  }
}
