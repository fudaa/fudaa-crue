/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.io.conf;

import files.FilesForTest;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class CrueEtudeConfigurationHelperTest {

  public CrueEtudeConfigurationHelperTest() {
  }

  /**
   * Un test plus complet est effectué dans la partie ui
   */
  @Test
  public void testRead() {
    CrueIOResu<CrueEtudeDaoConfiguration> read = CrueEtudeConfigurationHelper.read(FilesForTest.ETUDE_XML_FUDAA_CRUE, null);
    testResult(read.getMetier());
  }

  public void testResult(CrueEtudeDaoConfiguration planimetrieConfiguration) {
    List<Option> options = planimetrieConfiguration.getPlanimetrieConfiguration().getOptions();
    assertEquals(1, options.size());
    Option option = options.get(0);
    assertEquals("Test", option.getNom());
    assertEquals("Value", option.getValeur());
    options = planimetrieConfiguration.getGlobalConfiguration().getOptions();
    assertEquals(1, options.size());
    option = options.get(0);
    assertEquals("TestGlobal", option.getNom());
    assertEquals("ValueGlobal", option.getValeur());
  }

  @Test
  public void testWrite() throws IOException {
    CrueIOResu<CrueEtudeDaoConfiguration> read = CrueEtudeConfigurationHelper.read(FilesForTest.ETUDE_XML_FUDAA_CRUE, null);
    File target = File.createTempFile("test", ".conf");
    CrueEtudeConfigurationHelper.write(target, read.getMetier(), null);
    CrueIOResu<CrueEtudeDaoConfiguration> written = CrueEtudeConfigurationHelper.read(target, null);
    testResult(written.getMetier());
    target.delete();
  }
}
