package org.fudaa.dodico.crue.io.log;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

/**
 * @author CANEL Christophe
 *
 */
public class CrueLOGTest extends AbstractIOParentTest {

  public CrueLOGTest() {
    super(FilesForTest.LOG_TEST);
  }

  @Test
  public void testLecture() {
    final File logFile = this.getTestFile();
    final CtuluLog log = read(logFile);

    assertCorrect(log);
  }

  private static void assertCorrect(final CtuluLog log) {
    Assert.assertEquals("Description des logs", log.getDesc());

    final CtuluLogRecord[] logs = log.getRecords().toArray(new CtuluLogRecord[0]);

    Assert.assertEquals(4, logs.length);
    Assert.assertEquals(CtuluLogLevel.INFO, logs[0].getLevel());
    Assert.assertEquals("Ceci est une info", logs[0].getMsg());
    Assert.assertEquals(CtuluLogLevel.WARNING, logs[1].getLevel());
    Assert.assertEquals("Ceci est un warning", logs[1].getMsg());
    Assert.assertEquals(CtuluLogLevel.SEVERE, logs[2].getLevel());
    Assert.assertEquals("Ceci est une erreur fatale", logs[2].getMsg());
    Assert.assertEquals(CtuluLogLevel.ERROR, logs[3].getLevel());
    Assert.assertEquals("Ceci est une erreur", logs[3].getMsg());
  }

  @Test
  public void testEcriture() {
    final File logFile = this.getTestFile();
    File newLogFile = null;

    try {
      newLogFile = File.createTempFile("Test", "CrueLOG");
    } catch (final IOException e) {
      Assert.fail(e.getMessage());
    }

    CtuluLog log = read(logFile);

    final CrueLOGReaderWriter writer = new CrueLOGReaderWriter("1.2");
    final CrueIOResu<CtuluLog> resu = new CrueIOResu<>(log);
    log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);

    Assert.assertTrue(writer.writeXMLMetier(resu, newLogFile, log));

    testAnalyser(log);

    log = read(newLogFile);

    assertCorrect(log);
  }

  private File getTestFile() {
    final URL url = CrueLOGTest.class.getResource(FilesForTest.LOG_TEST);
    final File otfaFile = new File(createTempDir(), "logs.log.xml");

    try {
      CtuluLibFile.copyStream(url.openStream(), new FileOutputStream(otfaFile), true, true);
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }

    return otfaFile;
  }

  private static CtuluLog read(final File logFile) {
    final CrueLOGReaderWriter reader = new CrueLOGReaderWriter("1.2");
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CrueIOResu<CtuluLog> result = reader.readXML(logFile, log);

    testAnalyser(log);

    return result.getMetier();
  }

  @Test
  @Override
  public void testValide() {
  }
}
