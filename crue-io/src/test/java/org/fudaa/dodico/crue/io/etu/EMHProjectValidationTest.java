package org.fudaa.dodico.crue.io.etu;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

public class EMHProjectValidationTest {
  @Test
  public void testValidProject() {
    final EMHProjet projet = new EMHProjet();
    final ManagerEMHSousModele sousModele = new ManagerEMHSousModele("Modele 1");
    final FichierCrue drso = createFichierCrue(CrueFileType.DRSO, "_1", sousModele);
    final FichierCrue dcsp = createFichierCrue(CrueFileType.DCSP, "_1", sousModele);
    final FichierCrue dfrt = createFichierCrue(CrueFileType.DFRT, "_1", sousModele);
    final FichierCrue dptg = createFichierCrue(CrueFileType.DPTG, "_1", sousModele);

    final ManagerEMHSousModele sousModele2 = new ManagerEMHSousModele("Modele 2");
    createFichierCrue(CrueFileType.DRSO, "_2", sousModele2);
    createFichierCrue(CrueFileType.DCSP, "_2", sousModele2);
    createFichierCrue(CrueFileType.DFRT, "_2", sousModele2);
    createFichierCrue(CrueFileType.DPTG, "_2", sousModele2);

    projet.addBaseSousModele(sousModele);
    projet.addBaseSousModele(sousModele2);

    final EMHProjectValidation validation = new EMHProjectValidation();
    final CtuluLog logEmpty = validation.validProject(projet);
    Assert.assertTrue(logEmpty.isEmpty());

    final ManagerEMHSousModele sousModele3 = new ManagerEMHSousModele("Modele 3");
    sousModele3.addFichierDonnees(drso);
    sousModele3.addFichierDonnees(dcsp);
    sousModele3.addFichierDonnees(dptg);
    sousModele3.addFichierDonnees(dfrt);
    projet.addBaseSousModele(sousModele3);
    final CtuluLog logWithError = validation.validProject(projet);
    Assert.assertEquals(3, logWithError.getRecords().size());
    logWithError.updateLocalizedMessage(logEmpty.getDefaultResourceBundle());
    int idx = 0;
    final List<String> strings = logWithError.getRecords().stream().map(CtuluLogRecord::getLocalizedMessage).sorted().collect(Collectors.toList());
    Assert.assertEquals("Le fichier DCSP_1 est utilisé par plusieurs sous-modèles: Modele 1; Modele 3", strings.get(idx++));
    Assert.assertEquals("Le fichier DPTG_1 est utilisé par plusieurs sous-modèles: Modele 1; Modele 3", strings.get(idx++));
    Assert.assertEquals("Le fichier DRSO_1 est utilisé par plusieurs sous-modèles: Modele 1; Modele 3", strings.get(idx++));
  }

  private static FichierCrue createFichierCrue(final CrueFileType type, final String suffixe, final ManagerEMHSousModele sousModele) {
    final FichierCrue fichierCrue = new FichierCrue(type.name() + suffixe, "path" + type.name() + suffixe, type);
    sousModele.addFichierDonnees(fichierCrue);
    return fichierCrue;
  }
}
