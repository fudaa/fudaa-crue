/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.io.aide;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import junit.framework.Assert;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.io.CrueXmlReaderWriterImpl;
import org.junit.Test;

/**
 *
 * @author landrodie
 */
public class TestAide {

  public TestAide() {
  }

  @Test
  public void testLecture() {
    CrueXmlReaderWriterImpl<CrueDaoAide, CrueDaoAideContents> rw = new CrueXmlReaderWriterImpl<>("Lien", "1.0",
            new CrueConverterAide(), new CrueDaoStructureAide());
    URL in = getClass().getResource("/Aide/Aide.xml");
    CtuluLog log = new CtuluLog();
    final CrueDaoAide readDao = rw.readDao(in, log);
    Assert.assertNotNull(readDao);
    Assert.assertEquals(2, readDao.getListeAide().size());
    Assert.assertEquals("EDIT_BRANCHE", readDao.getListeAide().get(0).getIdFudaaCrue());

  }

  @Test
  public void WriteFile() throws IOException {

    CrueXmlReaderWriterImpl<CrueDaoAide, CrueDaoAideContents> rw = new CrueXmlReaderWriterImpl<>("Lien", "1.0",
            new CrueConverterAide(), new CrueDaoStructureAide());
    File outDir= CtuluLibFile.createTempDir();
    File out = new File(outDir,"tstAide.xml").getCanonicalFile();
    CrueAide aide1 = new CrueAide("EDIT_LOI", "EditionLoi#Signet1");
    CrueAide aide2 = new CrueAide("OPEN_LOI", "LireLoi");
    CrueDaoAideContents aides = new CrueDaoAideContents("fichier de mappage", Arrays.asList(aide1, aide2));

    CrueIOResu<CrueDaoAideContents> in = new CrueIOResu<>(aides);
    rw.writeXMLMetier(in, out, new CtuluLog());
    CtuluLibFile.deleteDir(outDir);
  }
}
