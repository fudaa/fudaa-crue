package org.fudaa.dodico.crue.io.optr;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.ccm.SeveriteManager;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;

/**
 * @author Adrien Hadoux
 */
public class CrueOPTRFileTest extends AbstractIOParentTest {
    /**
     *
     */
    public CrueOPTRFileTest() {
        super(Crue10FileFormatFactory.getVersion(CrueFileType.OPTR, TestCoeurConfig.INSTANCE), FilesForTest.OPTR_V1_3);
    }

    @Test
    public void testLecture() {
        final OrdPrtReseau data = readModele(FilesForTest.OPTR_V1_3, TestCoeurConfig.INSTANCE);
        testData(data);
    }

    @Test
    public void testLecture_V_1_2() {
        final OrdPrtReseau data = readModele(FilesForTest.OPTR_V1_2, TestCoeurConfig.INSTANCE_1_2);
        testData(data);
    }

    @Test
    public void testLectureFalse() {
        final OrdPrtReseau data = readModele(FilesForTest.OPTR_V1_3_FALSE, TestCoeurConfig.INSTANCE);
        testDataFalse(data);
    }

    @Test
    public void testLectureFalse_V_1_2() {
        final OrdPrtReseau data = readModele(FilesForTest.OPTR_V1_2_FALSE, TestCoeurConfig.INSTANCE_1_2);
        testDataFalse(data);
    }

    private void testData(final OrdPrtReseau data) {
        Assert.assertNotNull(data);
        Assert.assertEquals(EnumMethodeOrdonnancement.ORDRE_DRSO, data.getMethodeOrdonnancement());
        final Sorties sorties = data.getSorties();
        Assert.assertTrue(sorties.getAvancement().getSortieFichier());
        Assert.assertTrue(sorties.getResultat().getSortieFichier());
        Assert.assertTrue(sorties.getTrace().getSortieFichier());
        Assert.assertTrue(sorties.getTrace().getSortieEcran());
        Assert.assertEquals(SeveriteManager.DEBUG3, sorties.getTrace().getVerbositeEcran());
        Assert.assertEquals(SeveriteManager.INFO, sorties.getTrace().getVerbositeFichier());
        final List<Regle> regles = data.getRegles();
        Assert.assertEquals(2, regles.size());
        Assert.assertTrue(regles.get(0).isActive());
        Assert.assertEquals(EnumRegle.ORD_PRT_RESEAU_COMPATIBILITE_CLIMM, regles.get(0).getType());
        Assert.assertTrue(regles.get(1).isActive());
        Assert.assertEquals(EnumRegle.ORD_PRT_RESEAU_SIGNALER_OBJETS_INACTIFS, regles.get(1).getType());
    }

    private void testDataFalse(final OrdPrtReseau data) {
        Assert.assertNotNull(data);
        Assert.assertEquals(EnumMethodeOrdonnancement.ORDRE_DRSO, data.getMethodeOrdonnancement());
        final Sorties sorties = data.getSorties();
        Assert.assertFalse(sorties.getAvancement().getSortieFichier());
        Assert.assertFalse(sorties.getResultat().getSortieFichier());
        Assert.assertFalse(sorties.getTrace().getSortieFichier());
        Assert.assertFalse(sorties.getTrace().getSortieEcran());
        Assert.assertEquals(SeveriteManager.INFO, sorties.getTrace().getVerbositeEcran());
        Assert.assertEquals(SeveriteManager.INFO, sorties.getTrace().getVerbositeFichier());
        final List<Regle> regles = data.getRegles();
        Assert.assertEquals(2, regles.size());
        Assert.assertFalse(regles.get(0).isActive());
        Assert.assertEquals(EnumRegle.ORD_PRT_RESEAU_COMPATIBILITE_CLIMM, regles.get(0).getType());
        Assert.assertFalse(regles.get(1).isActive());
        Assert.assertEquals(EnumRegle.ORD_PRT_RESEAU_SIGNALER_OBJETS_INACTIFS, regles.get(1).getType());
    }

    @Test
    public void testEcriture() {
        testData(doEcriture(FilesForTest.OPTR_V1_3, TestCoeurConfig.INSTANCE));
    }

    @Test
    public void testEcriture_V_1_2() {
        testData(doEcriture(FilesForTest.OPTR_V1_2, TestCoeurConfig.INSTANCE_1_2));
    }

    @Test
    public void testEcritureFalse() {
        testDataFalse(doEcriture(FilesForTest.OPTR_V1_3_FALSE, TestCoeurConfig.INSTANCE));
    }

    @Test
    public void testEcritureFalse_V_1_2() {
        testDataFalse(doEcriture(FilesForTest.OPTR_V1_2_FALSE, TestCoeurConfig.INSTANCE_1_2));
    }

    private OrdPrtReseau doEcriture(final String file, final CoeurConfigContrat readVersion) {
//on lit dans le format readVersion et ecrit dans le dernier format
        final CtuluLog analyzer = new CtuluLog();
        final File f = createTempFile();
        final Crue10FileFormat<Object> fileFormatToWrite = Crue10FileFormatFactory.getVersion(CrueFileType.OPTR, TestCoeurConfig.INSTANCE);
        fileFormatToWrite.writeMetierDirect(readModele(file, readVersion), f, analyzer, CrueConfigMetierForTest.DEFAULT);
        Assert.assertFalse(analyzer.containsErrors());
        analyzer.clear();
        final boolean valide = fileFormatToWrite.isValide(f, analyzer);
        testAnalyser(analyzer);
        Assert.assertTrue(valide);

        return (OrdPrtReseau) fileFormatToWrite.read(f, analyzer, createDefault(TestCoeurConfig.INSTANCE)).getMetier();
    }

    public OrdPrtReseau readModele(final String file, final CoeurConfigContrat version) {
        final CtuluLog analyzer = new CtuluLog();
        final OrdPrtReseau data = (OrdPrtReseau) Crue10FileFormatFactory.getVersion(CrueFileType.OPTR, version).read(file, analyzer, createDefault(version))
                .getMetier();
        testAnalyser(analyzer);
        return data;
    }
}
