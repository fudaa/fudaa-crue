/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.extern;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

/**
 *
 * @author Frederic Deniger
 */
public class ExternContentReaderTest {

  public ExternContentReaderTest() {
  }

  @Test
  public void testRead() {
    final File file = AbstractTestParent.getFile(FilesForTest.EXTERNE_TXT);
    assertTrue(file.exists());
    final ExternContentReader reader = new ExternContentReader();
    final CrueIOResu<ExternContent> read = reader.read(file);
    assertNotNull(read);
    assertTrue(read.getAnalyse().isEmpty());
    final ExternContent content = read.getMetier();
    assertEquals("Title", content.getName());
    assertEquals("Comment", content.getComment());
    assertEquals(3, content.getNbValues());
    final List<String> names = content.getNames();
    assertEquals(2, names.size());
    assertEquals("V1", names.get(0));
    assertEquals("v2", names.get(1));
    assertEquals("value 1", content.getLabel(0));
    assertNull(content.getLabel(1));
    assertEquals("value 2", content.getLabel(2));
    ExternContentColumn column = content.getColumn("V1");
    assertEquals(1, column.getValue(0), 1e-15);
    assertEquals(2, column.getValue(1), 1e-15);
    assertEquals(3, column.getValue(2), 1e-15);
    column = content.getColumn("v2");
    assertEquals(1.2, column.getValue(0), 1e-15);
    assertEquals(2.2, column.getValue(1), 1e-15);
    assertEquals(3.2, column.getValue(2), 1e-15);
    file.delete();

  }

  @Test
  public void testReadCorrectEmpty() {
    final File file = AbstractTestParent.getFile(FilesForTest.EXTERNE_CORRECT_EMPTY_TXT);
    assertTrue(file.exists());
    final ExternContentReader reader = new ExternContentReader();
    final CrueIOResu<ExternContent> read = reader.read(file);
    assertNotNull(read);
    assertTrue(read.getAnalyse().isEmpty());
    final ExternContent content = read.getMetier();
    assertEquals("Title", content.getName());
    assertEquals("Comment", content.getComment());
    assertEquals(0, content.getNbValues());
    final List<String> names = content.getNames();
    assertEquals(2, names.size());
    assertEquals("V1", names.get(0));
    assertEquals("v2", names.get(1));
    ExternContentColumn column = content.getColumn("V1");
    assertEquals(0, column.getSize());
    column = content.getColumn("v2");
    assertEquals(0, column.getSize());
    file.delete();

  }

  @Test
  public void testReadEmpty() {
    final File file = AbstractTestParent.getFile(FilesForTest.EXTERNE_EMPTY_TXT);
    assertTrue(file.exists());
    final ExternContentReader reader = new ExternContentReader();
    final CrueIOResu<ExternContent> read = reader.read(file);
    assertNotNull(read);
    assertTrue(read.getAnalyse().containsErrorOrSevereError());
    assertEquals(1, read.getAnalyse().getRecords().size());
    final CtuluLogRecord record = read.getAnalyse().getRecords().get(0);
    assertEquals("extern.formatFileError", record.getMsg());
    file.delete();
  }

  @Test
  public void testBadNumber() {
    final File file = AbstractTestParent.getFile(FilesForTest.EXTERNE_BAD_NUMBER_TXT);
    assertTrue(file.exists());
    final ExternContentReader reader = new ExternContentReader();
    final CrueIOResu<ExternContent> read = reader.read(file);
    assertNotNull(read);
    assertTrue(read.getAnalyse().containsErrorOrSevereError());
    assertEquals(1, read.getAnalyse().getRecords().size());
    final CtuluLogRecord record = read.getAnalyse().getRecords().get(0);
    assertEquals("extern.valueLineBadContent", record.getMsg());
    file.delete();
  }

  @Test
  public void testNotSameColumnsBadNumber() {
    final File file = AbstractTestParent.getFile(FilesForTest.EXTERNE_NOT_SAME_COLUMNS_TXT);
    assertTrue(file.exists());
    final ExternContentReader reader = new ExternContentReader();
    final CrueIOResu<ExternContent> read = reader.read(file);
    assertNotNull(read);
    assertTrue(read.getAnalyse().containsErrorOrSevereError());
    assertEquals(1, read.getAnalyse().getRecords().size());
    final CtuluLogRecord record = read.getAnalyse().getRecords().get(0);
    assertEquals("extern.valueLineBadNumberOfColumns", record.getMsg());
    file.delete();
  }
}
