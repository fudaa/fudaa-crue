/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.dodico.crue.io.conf;

import files.FilesForTest;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class CrueEtudeExternConfigurationHelperTest {

  public CrueEtudeExternConfigurationHelperTest() {
  }

  /**
   * Un test plus complet est effectué dans la partie ui
   */
  @Test
  public void testRead() {
    CrueIOResu<CrueEtudeExternDaoConfiguration> read = CrueEtudeConfigurationHelper.readExtern(FilesForTest.EXTERNE_XML_FUDAA_CRUE_ETUDE);
    testResult(read.getMetier());
  }

  public void testResult(CrueEtudeExternDaoConfiguration extern) {
    List<CrueEtudeExternRessourceInfos> ressources = extern.getRessources();
    assertEquals(2, ressources.size());
    CrueEtudeExternRessourceInfos ressource = ressources.get(0);
    assertEquals("Nom 1", ressource.getNom());
    assertEquals("Type 1", ressource.getType());
    assertEquals(2, ressource.getOptions().size());
    assertEquals("Test", ressource.getOptions().get(0).getNom());
    assertEquals("Value", ressource.getOptions().get(0).getValeur());
    assertEquals("Test 1", ressource.getOptions().get(1).getNom());
    assertEquals("Value 1", ressource.getOptions().get(1).getValeur());

    //2 eme ressource
    ressource = ressources.get(1);
    assertEquals("Nom 2", ressource.getNom());
    assertEquals("Type 2", ressource.getType());
    assertEquals(3, ressource.getOptions().size());
    assertEquals("Test 2", ressource.getOptions().get(0).getNom());
    assertEquals("Value 2", ressource.getOptions().get(0).getValeur());
    assertEquals("Test 3", ressource.getOptions().get(1).getNom());
    assertEquals("Value 3", ressource.getOptions().get(1).getValeur());
    assertEquals("Test 4", ressource.getOptions().get(2).getNom());
    assertEquals("Value 4", ressource.getOptions().get(2).getValeur());
  }

  @Test
  public void testWrite() throws IOException {
    CrueIOResu<CrueEtudeExternDaoConfiguration> read = CrueEtudeConfigurationHelper.readExtern(FilesForTest.EXTERNE_XML_FUDAA_CRUE_ETUDE);
    File target = File.createTempFile("test", ".conf");
    CrueEtudeConfigurationHelper.writeExtern(target, read.getMetier());
    CrueIOResu<CrueEtudeExternDaoConfiguration> written = CrueEtudeConfigurationHelper.readExtern(target);
    testResult(written.getMetier());
    target.delete();
  }
}
