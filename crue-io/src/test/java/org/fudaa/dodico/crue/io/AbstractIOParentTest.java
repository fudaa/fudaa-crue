/*
 * Licence GPL Copyright
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Before;
import org.junit.Test;

import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author deniger
 */
public abstract class AbstractIOParentTest extends AbstractTestParent {
  protected final List<String> files;
  protected final Crue10FileFormat format;

  /**
   * @param format le format
   * @param file les fichiers a tester
   */
  public AbstractIOParentTest(final Crue10FileFormat format, final String... file) {
    super();
    this.files = Arrays.asList(file);
    this.format = format;
  }

  protected static <T> Crue10FileFormat<T> getFileFormat(final CrueFileType type, final CoeurConfigContrat coeurConfig){
    return Crue10FileFormatFactory.getInstance().getFileFormat(type,coeurConfig);
  }

  public AbstractIOParentTest(final String... file) {
    this(null, file);
  }

  public static URL getResource(final String absolutePath) {
    final URL resource = AbstractIOParentTest.class.getResource(absolutePath);
    assertNotNull(resource);
    return resource;
  }

  @Before
  public void checkFileArePresent() {
    for (final String file : files) {
      assertNotNull("file must exist " + file, getClass().getResource(file));
    }
  }

  public static CrueData createDefault(CoeurConfigContrat coeurConfigContrat) {
    return new CrueDataImpl(coeurConfigContrat.getCrueConfigMetier());
  }
  public static CrueData createDefault(CrueConfigMetier ccm) {
    return new CrueDataImpl(ccm);
  }

  @Test
  public void testValide() {
    assertNotNull(format);
    for (final String file : files) {
      final CtuluLog res = new CtuluLog();
      final boolean valide = format.isValide(file, res);
      if (res.containsErrorOrSevereError()) {
        res.printResume();
      }
      assertTrue(file, valide);
    }
  }
}
