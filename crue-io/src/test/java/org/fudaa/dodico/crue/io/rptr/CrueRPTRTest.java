package org.fudaa.dodico.crue.io.rptr;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.drso.CrueDRSOFileTest;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.ResPrtReseau;
import org.fudaa.dodico.crue.metier.emh.ResPrtReseauNoeud;
import org.fudaa.dodico.crue.metier.emh.ResPrtReseauNoeuds;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * @author deniger
 */
public class CrueRPTRTest extends AbstractIOParentTest {
  public CrueRPTRTest() {
    super(Crue10FileFormatFactory.getVersion(CrueFileType.RPTR, TestCoeurConfig.INSTANCE), FilesForTest.RPTR_V1_3);
  }

  @Test
  public void testLecture() {
    final CtuluLog analyser = new CtuluLog();
    final CrueData read = read(FilesForTest.DRSO_V1_3, FilesForTest.DCLM_V_1_3, FilesForTest.DLHY_V1_3, TestCoeurConfig.INSTANCE);
    // -- lecture de pcal --//
    final ResPrtReseau jeuDonnees = (ResPrtReseau) format.read(FilesForTest.RPTR_V1_3, analyser, read).getMetier();
    testAnalyser(analyser);
    testDonnees(jeuDonnees);
  }
  @Test
  public void testLecture_V_1_2() {
    final CtuluLog analyser = new CtuluLog();
    final CrueData read = read(FilesForTest.DRSO_V1_2, FilesForTest.DCLM_V_1_2, FilesForTest.DLHY_V1_2, TestCoeurConfig.INSTANCE_1_2);
    // -- lecture de pcal --//
    final ResPrtReseau jeuDonnees = (ResPrtReseau) getFileFormat(CrueFileType.RPTR, TestCoeurConfig.INSTANCE_1_2).read(FilesForTest.RPTR_V1_2, analyser, read).getMetier();
    testAnalyser(analyser);
    testDonnees(jeuDonnees);
  }

  @Test
  public void testLectureVide() {
    final CtuluLog analyser = new CtuluLog();
    read(FilesForTest.DRSO_V1_3, FilesForTest.DCLM_V_1_3, FilesForTest.DLHY_V1_3, TestCoeurConfig.INSTANCE);
    // -- lecture de pcal --//
    testAnalyser(analyser);
  }
  @Test
  public void testLectureVide_V_1_2() {
    final CtuluLog analyser = new CtuluLog();
    read(FilesForTest.DRSO_V1_2, FilesForTest.DCLM_V_1_2, FilesForTest.DLHY_V1_2, TestCoeurConfig.INSTANCE_1_2);
    // -- lecture de pcal --//
    testAnalyser(analyser);
  }

  private CrueData read(final String drso, final String dclm,final String dlhy, final CoeurConfigContrat version) {
    final CtuluLog log = new CtuluLog();

    final CrueIOResu<CrueData> ioData = Crue10FileFormatFactory.<CrueData>getVersion(CrueFileType.DRSO, version).read(drso, log,
        CrueDRSOFileTest.createDefault(version));
    final CrueData res = ioData.getMetier();
    testAnalyser(log);
    Crue10FileFormatFactory.getVersion(CrueFileType.DLHY, version).read(dlhy, log, res);
    testAnalyser(log);
    Crue10FileFormatFactory.<CrueData>getVersion(CrueFileType.DCLM, version).read(dclm, log, res);
    testAnalyser(log);
    return res;
  }

  private void testDonnees(final ResPrtReseau jeuDonnees) {
    Assert.assertNotNull(jeuDonnees);
    final List<ResPrtReseauNoeuds> listResPrtReseauNoeud = jeuDonnees.getResPrtReseauNoeuds();
    // <ResPrtReseauNoeud NomRef="Nd_N1"/>
    // <ResPrtReseauNoeud NomRef="Nd_N2"/>
    // <ResPrtReseauNoeud NomRef="Nd_N6"/>
    // <ResPrtReseauNoeud NomRef="Nd_N3"/>
    // <ResPrtReseauNoeud NomRef="Nd_N4"/>
    // <ResPrtReseauNoeud NomRef="Nd_N7"/>
    // <ResPrtReseauNoeud NomRef="Nd_N5"/>
    Assert.assertEquals(3, listResPrtReseauNoeud.size());
    final List<String> calcIds = Arrays.asList("Cc_P1", "Cc_P2", "Cc_T1");
    int idx = 0;
    for (final ResPrtReseauNoeuds resPrtReseauNoeuds : listResPrtReseauNoeud) {
      Assert.assertEquals(calcIds.get(idx), resPrtReseauNoeuds.getCalc().getNom());
      idx++;
    }
    final List<String> expected = Arrays.asList("Nd_N1", "Nd_N2", "Nd_N6", "Nd_N3", "Nd_N4", "Nd_N7", "Nd_N5");
    idx = 0;
    final List<ResPrtReseauNoeud> resPrtReseauNoeuds = listResPrtReseauNoeud.get(0).getResPrtReseauNoeud();
    for (final String string : expected) {
      Assert.assertEquals(string, resPrtReseauNoeuds.get(idx).getNom());
      Assert.assertEquals(string.toUpperCase(), resPrtReseauNoeuds.get(idx).getId());
      idx++;
    }
  }
}
