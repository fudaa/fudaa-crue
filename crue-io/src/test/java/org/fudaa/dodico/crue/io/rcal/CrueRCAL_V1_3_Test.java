package org.fudaa.dodico.crue.io.rcal;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.rcal.CrueDaoStructureRCAL.ResPdtDao;
import org.fudaa.dodico.crue.io.rcal.CrueDaoStructureRCAL.ResultatsCalculPermanentDao;
import org.fudaa.dodico.crue.io.rdao.*;
import org.fudaa.dodico.crue.io.rdao.CasiersDao.CasierDao;
import org.fudaa.dodico.crue.io.rdao.CasiersDao.CasierTypeDao;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.VariableResDao;
import org.fudaa.dodico.crue.io.rdao.NoeudsDao.NoeudDao;
import org.fudaa.dodico.crue.io.rdao.NoeudsDao.NoeudTypeDao;
import org.fudaa.dodico.crue.io.rdao.ParametrageDao.DelimiteurDao;
import org.fudaa.dodico.crue.io.rdao.SectionsDao.SectionDao;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class CrueRCAL_V1_3_Test extends AbstractIOParentTest {
  public CrueRCAL_V1_3_Test() {
    super(Crue10FileFormatFactory.getVersion(CrueFileType.RCAL, TestCoeurConfig.INSTANCE), FilesForTest.RCAL_V1_3_REGUL);
  }

  @Test
  public void testLectureRegul() {

    final CrueDaoRCAL donnees = readData(FilesForTest.RCAL_V1_3_REGUL, TestCoeurConfig.INSTANCE);

    Assert.assertEquals("Commentaire RCAL", donnees.getCommentaire());
    this.testContexte(donnees.getContexteSimulation());
    testParametrage(donnees.getParametrage());
    this.testStructureResultat(donnees.getStructureResultat(),true);
    testResultatsCalculPermanents(donnees.getResCalcPerms());
    testResultatsCalculTransitoire(donnees.getResCalcTranss(),32263);
  }
  @Test
  public void testLectureNoRegul() {

    final CrueDaoRCAL donnees = readData(FilesForTest.RCAL_V1_3_NOREGUL, TestCoeurConfig.INSTANCE);

    Assert.assertEquals("Commentaire RCAL", donnees.getCommentaire());
    this.testContexteNoRegul(donnees.getContexteSimulation());
    testParametrage(donnees.getParametrage());
    this.testStructureResultat(donnees.getStructureResultat(),false);
    testResultatsCalculPermanents(donnees.getResCalcPerms());
    testResultatsCalculTransitoire(donnees.getResCalcTranss(),32131);
  }

  private void testContexte(final ContexteSimulationDao contexte) {
    Assert.assertEquals("2023-07-27T15:56:02.483", contexte.getDateSimulation());
    Assert.assertEquals("10.4.1", contexte.getVersionCrue());
    Assert.assertEquals("Etu_BY2010.etu.xml", contexte.getEtude());
    Assert.assertEquals("Sc_BY", contexte.getScenario().getNomRef());
    Assert.assertEquals(".", contexte.getRun().getNomRef());
    Assert.assertEquals("Mo_BY", contexte.getModele().getNomRef());
  }
  private void testContexteNoRegul(final ContexteSimulationDao contexte) {
    Assert.assertEquals("2023-07-27T15:57:55.027", contexte.getDateSimulation());
    Assert.assertEquals("10.4.1", contexte.getVersionCrue());
    Assert.assertEquals("Etu_BY2010.etu.xml", contexte.getEtude());
    Assert.assertEquals("Sc_BY", contexte.getScenario().getNomRef());
    Assert.assertEquals(".", contexte.getRun().getNomRef());
    Assert.assertEquals("Mo_BY", contexte.getModele().getNomRef());
  }

  private void testResultatsCalculPermanents(final List<ResultatsCalculPermanentDao> resultatsCalculPermanents) {
    Assert.assertEquals(1, resultatsCalculPermanents.size());

    final ResultatsCalculPermanentDao ccP1 = resultatsCalculPermanents.get(0);
    Assert.assertEquals("Cc_P01", ccP1.getNomRef());
    Assert.assertEquals("BY.rcal_0001.bin", ccP1.getHref());
    Assert.assertEquals(0, ccP1.getOffsetMot());
  }
//

  private void testResultatsCalculTransitoire(final List<CrueDaoStructureRCAL.ResCalcTransDao> resultatsCalculTransitoire,int lastOffset) {
    Assert.assertEquals(1, resultatsCalculTransitoire.size());

    final CrueDaoStructureRCAL.ResCalcTransDao ccT1 = resultatsCalculTransitoire.get(0);
    Assert.assertEquals("Cc_T01", ccT1.getNomRef());
    final List<ResPdtDao> ccT1resPdts = ccT1.getResPdts();
    Assert.assertEquals(97, ccT1resPdts.size());

    final ResPdtDao ccT1Res1 = ccT1resPdts.get(96);
    Assert.assertEquals("P4DT0H0M0S", ccT1Res1.getTempsSimu());
    Assert.assertEquals("BY.rcal_0003.bin", ccT1Res1.getHref());
    Assert.assertEquals(lastOffset, ccT1Res1.getOffsetMot());
  }

  public static CrueDaoRCAL readData(final String fichier, final CoeurConfigContrat version) {
    final CtuluLog analyzer = new CtuluLog();
    final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.RCAL, version);
    final CrueDaoRCAL jeuDonneesLue = (CrueDaoRCAL) fileFormat.read(fichier, analyzer, createDefault(version)).getMetier();

    testAnalyser(analyzer);

    return jeuDonneesLue;
  }

  private void testParametrage(final ParametrageDao parametrage) {
    Assert.assertEquals(8, parametrage.getNbrOctetMot());
    final List<DelimiteurDao> delimiteurs = parametrage.getDelimiteur();
    Assert.assertEquals(8, delimiteurs.size());
    DelimiteurDao delimiteur = delimiteurs.get(0);
    Assert.assertEquals("ResCalcPseudoPerm", delimiteur.getNom());
    Assert.assertEquals("  RcalPp", delimiteur.getChaine());
    delimiteur = delimiteurs.get(6);
    Assert.assertEquals("CatEMHBranche", delimiteur.getNom());
    Assert.assertEquals(" Branche", delimiteur.getChaine());
    delimiteur = delimiteurs.get(7);
    Assert.assertEquals("CatEMHModele", delimiteur.getNom());
    Assert.assertEquals("  Modele", delimiteur.getChaine());
  }

  private void testSectionInterpolee(final SectionsDao sections) {
    Assert.assertEquals(55, sections.getSectionInterpolee().getNbrMot());
    Assert.assertNull(sections.getSectionInterpolee().getVariableRes());
    final List<SectionDao> sectionInterpoleeList = sections.getSectionInterpolee().getSection();
    Assert.assertEquals(11, sectionInterpoleeList.size());
    testNbrMot(sectionInterpoleeList, 5);
    testNomRef(sectionInterpoleeList, "St_BSA7_00072", "St_BSA7_00144");
  }

  private void testSectionProfil(final SectionsDao sections) {
    Assert.assertEquals(1320, sections.getSectionProfil().getNbrMot());
    Assert.assertNull(sections.getSectionProfil().getVariableRes());
    final List<SectionDao> sectionProfilList = sections.getSectionProfil().getSection();
    Assert.assertEquals(264, sectionProfilList.size());
    testNbrMot(sectionProfilList, 5);
    Assert.assertEquals("St_P131.150", sectionProfilList.get(263).getNomRef());

  }

  private void testSectionSansGeometrie(final SectionsDao sections) {
    Assert.assertEquals(670, sections.getSectionSansGeometrie().getNbrMot());
    Assert.assertNull(sections.getSectionSansGeometrie().getVariableRes());
    final List<SectionDao> sectionProfilList = sections.getSectionSansGeometrie().getSection();
    Assert.assertEquals(134, sectionProfilList.size());
    testNbrMot(sectionProfilList, 5);
    Assert.assertEquals("St_SSav_Av", sectionProfilList.get(sectionProfilList.size() - 1).getNomRef());
  }

  private void testSectionItem(final SectionsDao sections) {
    //Section Item:
    Assert.assertEquals(405, sections.getSectionIdem().getNbrMot());
    Assert.assertNull(sections.getSectionIdem().getVariableRes());
    final List<SectionDao> sectionIdemList = sections.getSectionIdem().getSection();
    Assert.assertEquals(81, sectionIdemList.size());
    testNbrMot(sectionIdemList, 5);
    testNomRef(sectionIdemList, "St_P145.000B", "St_P144.000B");
  }

  private void testStructureResultat(final StructureResultatsDao structureResultats,boolean regul) {
    Assert.assertEquals(2920, structureResultats.getNbrMot());
    //Attention fichier d'origine MODIFIE pour tester la lecture des variables au niveau StructureResultats:
    testNoeuds(structureResultats.getNoeuds());
    testCasiers(structureResultats.getCasiers());
    testSections(structureResultats.getSections());
    testBranches(structureResultats.getBranches());
    if(regul) {
      testModeles(structureResultats.getModeles());
    }
    else{
      testModelesNoRegul(structureResultats.getModeles());
    }
  }

  private void testVariableResClass(final List objectList) {
    for (final Object object : objectList) {
      Assert.assertEquals(VariableResDao.class, object.getClass());
    }
  }

  private void testNoeuds(final NoeudsDao noeuds) {
    Assert.assertEquals(1, noeuds.getNbrMot());
    final List<VariableResDao> variableRes = noeuds.getVariableRes();
    Assert.assertNull(variableRes);
    final NoeudTypeDao noeudNiveauContinuType = noeuds.getNoeudNiveauContinu();
    Assert.assertEquals(0, noeudNiveauContinuType.getNbrMot());
    final List<NoeudDao> noeudList = noeudNiveauContinuType.getNoeud();
    Assert.assertEquals(119, noeudList.size());
  }

  /**
   * parcourt les noms et tests l'egalite. Ne teste pas que les tailles sont les mêmes
   */
  public static void testNomRef(final List<? extends CommonResDao.WithNomRef> list, final String... noms) {
    final int nb = noms.length;
    for (int i = 0; i < nb; i++) {
      Assert.assertEquals(noms[i], list.get(i).getNomRef());
    }
  }

  public static void testClass(final List<? extends CommonResDao.WithNomRef> list, final Class... clazz) {
    final int nb = clazz.length;
    for (int i = 0; i < nb; i++) {
      Assert.assertEquals(clazz[i], list.get(i).getClass());
    }
  }

  public static void testNomRefExact(final List<? extends CommonResDao.WithNomRef> list, final String... noms) {
    Assert.assertEquals(list.size(), noms.length);
    testNomRef(list, noms);
  }

  private static void testNbrMot(final List<? extends CommonResDao.NbrMotDao> list, final int nbrMot) {
    for (final CommonResDao.NbrMotDao nbrMotDao : list) {
      Assert.assertEquals(nbrMot, nbrMotDao.getNbrMot());
    }
  }

  private void testSections(final SectionsDao sections) {
    Assert.assertEquals(2451, sections.getNbrMot());
    final List<VariableResDao> variableResList = sections.getVariableRes();
    Assert.assertEquals(5, variableResList.size());
    testVariableResClass(variableResList);
    for (final VariableResDao variableResDao : variableResList) {
      Assert.assertEquals(VariableResDao.class, variableResDao.getClass());
    }
    Assert.assertEquals("Q", variableResList.get(0).getNomRef());
    Assert.assertEquals("Z", variableResList.get(4).getNomRef());
    testSectionItem(sections);
    testSectionInterpolee(sections);
    testSectionProfil(sections);
    testSectionSansGeometrie(sections);
  }

  private void testCasiers(final CasiersDao casiers) {
    Assert.assertEquals(136, casiers.getNbrMot());
    final List<VariableResDao> variableResList = casiers.getVariableRes();
    Assert.assertEquals(3, variableResList.size());
    testNomRef(variableResList, "Qech", "Splan", "Vol");

    final CasierTypeDao casierProfil = casiers.getCasierProfil();
    Assert.assertEquals(135, casierProfil.getNbrMot());
    final List<VariableResDao> variableResInTypeList = casierProfil.getVariableRes();
    Assert.assertNull(variableResInTypeList);
    final List<CasierDao> casierList = casierProfil.getCasier();
    testNbrMot(casierList, 3);
    Assert.assertEquals(45, casierList.size());
    testNomRef(casierList, "Ca_cverdet", "Ca_CVR141.8");
  }

  private void testBranches(final BranchesDao branches) {
    Assert.assertEquals(331, branches.getNbrMot());
    Assert.assertNull(branches.getVariableRes());

    testNomRefExact(branches.getBrancheSaintVenant().getVariableRes(), "SplanAct", "SplanSto", "SplanTot", "Vol");
    testBranche(branches.getBrancheSaintVenant().getBranche(), 69, 4, "Br_BVRBY2");
  }

  private void testModeles(final ModelesDao modeles) {
    Assert.assertEquals(13, modeles.getNbrMot());
    Assert.assertNull(modeles.getVariableRes());
    ModelesDao.ModeleRegulDao modeleRegulDao = modeles.getModeleRegul();
    Assert.assertEquals(12, modeleRegulDao.getNbrMot());
    Assert.assertEquals(1, modeleRegulDao.getItemRes().size());
    Assert.assertEquals("Mo_BY", modeleRegulDao.getItemRes().get(0).getNomRef());
    List<VariableResDao> variableRes = modeleRegulDao.getVariableRes();
    Assert.assertEquals(12, variableRes.size());
    testNomRefExact(variableRes, "Mode", "LoiRegul", "Zcns", "Qobj", "QpilBY", "QentBY", "QeffBY", "QSavieres", "ZLac", "ZChanaz", "ZamQbg2", "ZavQbg2");
    testClass(variableRes, VariableResDao.class, VariableResDao.class, VariableResDao.class, CommonResDao.VariableResQregulDao.class, CommonResDao.VariableResQregulDao.class, CommonResDao.VariableResQregulDao.class, CommonResDao.VariableResQregulDao.class, CommonResDao.VariableResQregulDao.class, CommonResDao.VariableResZregulDao.class, CommonResDao.VariableResZregulDao.class, CommonResDao.VariableResZregulDao.class, CommonResDao.VariableResZregulDao.class);
  }
  private void testModelesNoRegul(final ModelesDao modeles) {
    Assert.assertEquals(1, modeles.getNbrMot());
    Assert.assertNull(modeles.getVariableRes());
    ModelesDao.ModeleRegulDao modeleRegulDao = modeles.getModeleRegul();
    Assert.assertEquals(0, modeleRegulDao.getNbrMot());
    Assert.assertEquals(1, modeleRegulDao.getItemRes().size());
    Assert.assertEquals("Mo_BY", modeleRegulDao.getItemRes().get(0).getNomRef());
  }

  private void testBranche(final List<BranchesDao.BrancheDao> branches, final int size, final int nbrMot, final String lastOne) {
    Assert.assertEquals(size, branches.size());
    testNbrMot(branches, nbrMot);
    Assert.assertEquals(lastOne, branches.get(branches.size() - 1).getNomRef());
  }

}
