package org.fudaa.dodico.crue.io.rptg;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.rcal.Crue10ResultatPasDeTempsDelegate;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.ItemResDao;
import org.fudaa.dodico.crue.io.res.*;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.junit.After;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

import static org.junit.Assert.*;

/**
 * @author deniger
 */
public class CrueRPTGBinaireTest {

  private final List<File> filesToDelete = new ArrayList<>();

  @Test
  public void testBinaryFile() throws IOException {
    final File createTempDir = CtuluLibFile.createTempDir();
    filesToDelete.add(createTempDir);
    final File rptg = new File(createTempDir, FilesForTest.RPTG_BIN_NAME);
    CtuluLibFile.getFileFromJar(FilesForTest.RPTG_BIN_PATH_V_1_2, rptg);

    assertTrue(rptg.exists());
    final CrueDaoRPTG res = readXml();
    final ResPrtGeoDao firstPrtGeo = res.getResPrtGeos().get(0);
    final ResTypeEMHBuilder typeBuilder = new ResTypeEMHBuilder(res);
    final CrueIOResu<List<ResCatEMHContent>> extract = typeBuilder.extract();
    final ResultatTimeKey key = new ResultatTimeKey(ResPrtGeoDao.class.getSimpleName());
    final ResultatEntry entry = new ResultatEntry(key);
    entry.setBinFile(new File(rptg.getParentFile(), firstPrtGeo.getHref()));
    entry.setOffsetInBytes(res.getParametrage().getBytes(firstPrtGeo.getOffsetMot()));
    final Map<ResultatTimeKey, String> delimiteurs = new HashMap<>();
    delimiteurs.put(key, res.getParametrage().getDelimiteurChaine("ResPrtGeo"));
    final Map<ResultatTimeKey, ResultatEntry> pasDeTemps = new HashMap<>();
    pasDeTemps.put(key, entry);
    final Crue10ResultatPasDeTempsDelegate rptgPasDeTemps = new Crue10ResultatPasDeTempsDelegate(Collections.singletonList(key), pasDeTemps);
    final List<ResCatEMHContent> metier = extract.getMetier();
    //on lit le premier profil:
    final Pair<ResTypeEMHContent, ItemResDao> prof1 = find(metier, "St_PROF1");
    assertNotNull(prof1);
    final ItemResDao emhDef = prof1.second;
    final ResVariablesContent resVariablesContent = prof1.first.getResVariablesContent();
    final int length = emhDef.getNbrMot() * 8;
    final ResultatCalculCrue10 resultatCalcul = new ResultatCalculCrue10(prof1.first.getItemPositionInBytes("St_PROF1"),
            length, resVariablesContent, rptgPasDeTemps,
            "SectionProfil", "ST_PROF1");
    final Map<String, Object> read = resultatCalcul.read(key);
    assertNotNull(read);
    final LoiFF loi = (LoiFF) read.get("loiZSact");
    assertNotNull(loi);
    assertEquals(50, loi.getEvolutionFF().getPtEvolutionFF().size());
    final PtEvolutionFF pt = loi.getEvolutionFF().getPtEvolutionFF().get(49);
    assertEquals(5.20d, pt.getAbscisse(), 1e-4);
    assertEquals(282.495d, pt.getOrdonnee(), 1e-4);


  }

  /**
   * recupere le bon ResTypeEMHContent et affecte les variables en plus.
   *
   * @param metier
   * @param nom
   * @return
   */
  private static Pair<ResTypeEMHContent, ItemResDao> find(final List<ResCatEMHContent> metier, final String nom) {
    for (final ResCatEMHContent resCatEMHContent : metier) {
      for (final ResTypeEMHContent resTypeEMHContent : resCatEMHContent.getResTypeEMHContent()) {
        final List<ItemResDao> itemResDaos = resTypeEMHContent.getItemResDao();
        for (final ItemResDao itemResDao : itemResDaos) {
          if (nom.equals(itemResDao.getNomRef())) {
            resTypeEMHContent.getResVariablesContent().addVariableResLoiNbrPtDao(itemResDao.getVariableResLoiNbrPt());
            return new Pair<>(resTypeEMHContent, itemResDao);
          }
        }

      }
    }
    return null;
  }

  @After
  public void cleanFiles() {
    for (final File file : filesToDelete) {
      if (file.isDirectory()) {
        CtuluLibFile.deleteDir(file);
      } else {
        file.delete();
      }
    }
    filesToDelete.clear();

  }

  private CrueDaoRPTG readXml() {
    final URL resource = AbstractIOParentTest.getResource(FilesForTest.RPTG_M201_1_2);
    final Crue10FileFormat version = Crue10FileFormatFactory.getVersion(CrueFileType.RPTG, TestCoeurConfig.INSTANCE);
    final CtuluLog log = new CtuluLog();
    final CrueIOResu read = version.read(resource, log, null);
    assertFalse(log.containsErrorOrSevereError());
    final CrueDaoRPTG rcal = (CrueDaoRPTG) read.getMetier();
    return rcal;
  }
}
