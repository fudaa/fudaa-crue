package org.fudaa.dodico.crue.io.ocal;

import files.FilesForTest;
import java.io.File;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.ccm.SeveriteManager;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.*;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.DonCLimMScenario;
import org.fudaa.dodico.crue.metier.emh.OrdCalc;
import org.fudaa.dodico.crue.metier.emh.OrdCalcPseudoPerm;
import org.fudaa.dodico.crue.metier.emh.OrdCalcPseudoPermIniCalcCI;
import org.fudaa.dodico.crue.metier.emh.OrdCalcPseudoPermIniCalcPrecedent;
import org.fudaa.dodico.crue.metier.emh.OrdCalcPseudoPermIniCliche;
import org.fudaa.dodico.crue.metier.emh.OrdCalcScenario;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTrans;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTransIniCalcCI;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTransIniCalcCliche;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTransIniCalcPrecedent;
import org.fudaa.dodico.crue.metier.emh.PdtCst;
import org.fudaa.dodico.crue.metier.emh.Sorties;
import org.fudaa.dodico.crue.metier.emh.ui.CalcOrdCalcUiState;
import org.fudaa.dodico.crue.metier.emh.ui.OrdCalcScenarioUiState;
import org.junit.Assert;
import org.junit.Test;

/**
 * Classe de tests JUnit pour OCAL (Ordres de CALculs) ; Permet de contrôler le bon fonctionnement de lecture d'un fichier XML existant, l'écriture de
 * façon similaire de ce même fichier et valide par XSD le fichier XML initial et celui produit
 *
 * @author CDE
 */
public class CrueOCALFileTest extends AbstractIOParentTest {

  /**
   * @param metier le scenario a tester
   */
  private static void verifieDonneesV1_1_1(final OrdCalcScenario metier) {

    Assert.assertNotNull(metier);
    final List<OrdCalc> calcsPerm = metier.getOrdCalc();
    Assert.assertNotNull(calcsPerm);
    Assert.assertEquals(3, calcsPerm.size());

    OrdCalcPseudoPerm pseudoPerm = (OrdCalcPseudoPerm) calcsPerm.get(0);
    Assert.assertTrue(pseudoPerm instanceof OrdCalcPseudoPermIniCalcCI);
    Assert.assertEquals("Cc_P1", pseudoPerm.getCalcPseudoPerm().getNom());
    // assertEquals("0001-01-01T01:00:00.000", DateDurationConverter.dateToXsd(DateDurationConverter
    // .getDateFromZeroDate(((OrdCalcTransIniCalcCI) pseudoPerm).getTempsReprise())));

    pseudoPerm = (OrdCalcPseudoPerm) calcsPerm.get(1);
    Assert.assertTrue(pseudoPerm instanceof OrdCalcPseudoPermIniCalcPrecedent);
    Assert.assertEquals("Cc_P2", pseudoPerm.getCalcPseudoPerm().getNom());

    final OrdCalcTrans calcTrans = (OrdCalcTrans) calcsPerm.get(2);
    Assert.assertNotNull(calcTrans);
    Assert.assertTrue(calcTrans instanceof OrdCalcTransIniCalcPrecedent);
    Assert.assertEquals("Cc_T1", calcTrans.getCalc().getNom());
    //ces données sont issues de PCAL:
    Assert.assertEquals("P0Y0M1DT0H0M1S", DateDurationConverter.durationToXsd(calcTrans.getDureeCalc()));
    Assert.assertEquals("P0Y0M0DT1H0M2S", DateDurationConverter.durationToXsd(((PdtCst) calcTrans.getPdtRes()).getPdtCst()));
  }

  private static void verifieDonnees(final OrdCalcScenario metier) {

    Assert.assertNotNull(metier);
    final List<OrdCalc> ordCalcs = metier.getOrdCalc();
    Assert.assertNotNull(ordCalcs);
    Assert.assertEquals(6, ordCalcs.size());

    final OrdCalcPseudoPerm cc_P1 = (OrdCalcPseudoPerm) ordCalcs.get(0);
    Assert.assertTrue(cc_P1 instanceof OrdCalcPseudoPermIniCalcCI);
    Assert.assertEquals("Cc_P1", cc_P1.getCalcPseudoPerm().getNom());
    //ces données ne sont pas dans les fichiers originaux. Ajoutés pour vérifier les conversions sur tous les types de ocal.
    Assert.assertEquals("NomCliche_Cc_P1", ((OrdCalcPseudoPerm) metier.getOrdCalc().get(0)).getPrendreClicheFinPermanent().getNomFic());

    final OrdCalcPseudoPerm cc_P2 = (OrdCalcPseudoPerm) ordCalcs.get(1);
    Assert.assertTrue(cc_P2 instanceof OrdCalcPseudoPermIniCalcPrecedent);
    Assert.assertEquals("Cc_P2", cc_P2.getCalcPseudoPerm().getNom());
    //ces données ne sont pas dans les fichiers originaux. Ajoutés pour vérifier les conversions sur tous les types de ocal.
    Assert.assertEquals("NomCliche_Cc_P2", ((OrdCalcPseudoPerm) metier.getOrdCalc().get(1)).getPrendreClicheFinPermanent().getNomFic());

    final OrdCalcPseudoPermIniCliche cc_P3 = (OrdCalcPseudoPermIniCliche) ordCalcs.get(2);
    Assert.assertEquals("Cc_P3", cc_P3.getCalc().getNom());
    Assert.assertEquals("NomCliche1", cc_P3.getIniCalcCliche().getNomFic());
    Assert.assertEquals("NomCliche2", cc_P3.getPrendreClicheFinPermanent().getNomFic());

    final OrdCalcTrans cc_T1 = (OrdCalcTrans) ordCalcs.get(3);
    Assert.assertNotNull(cc_T1);
    Assert.assertTrue(cc_T1 instanceof OrdCalcTransIniCalcPrecedent);
    Assert.assertEquals("Cc_T1", cc_T1.getCalc().getNom());

    //modifie dans les fichiers pour avoir des temps différents:
    Assert.assertEquals("P0Y0M1DT0H0M1S", DateDurationConverter.durationToXsd(cc_T1.getDureeCalc()));
    Assert.assertEquals("P0Y0M0DT1H0M2S", DateDurationConverter.durationToXsd(((PdtCst) cc_T1.getPdtRes()).getPdtCst()));
    Assert.assertEquals("NomCliche3", cc_T1.getPrendreClichePonctuel().getNomFic());
    Assert.assertEquals("NomCliche4", cc_T1.getPrendreClichePeriodique().getNomFic());
    //modifie dans les fichiers pour avoir des temps différents:
    Assert.assertEquals("P0Y0M0DT1H0M3S", DateDurationConverter.durationToXsd(cc_T1.getPrendreClichePonctuel().getTempsSimu()));
    Assert.assertEquals("P0Y0M0DT0H1M4S", DateDurationConverter.durationToXsd(((PdtCst) cc_T1.getPrendreClichePeriodique().getPdtRes()).getPdtCst()));

    final OrdCalcTrans ccT2 = (OrdCalcTrans) metier.getOrdCalc().get(4);
    Assert.assertTrue(ccT2 instanceof OrdCalcTransIniCalcCliche);
    final OrdCalcTransIniCalcCliche ccT2Cliche = (OrdCalcTransIniCalcCliche) ccT2;
    //modifie dans le fichier origine pour avoir nom unique
    Assert.assertEquals("NomClicheCc_T2", ccT2Cliche.getIniCalcCliche().getNomFic());

    Assert.assertEquals("P0Y0M1DT0H0M5S", DateDurationConverter.durationToXsd(ccT2.getDureeCalc()));
    Assert.assertEquals("P0Y0M0DT1H0M6S", DateDurationConverter.durationToXsd(((PdtCst) ccT2.getPdtRes()).getPdtCst()));
    Assert.assertEquals("NomClichePonctuel_Cc_T2", ccT2.getPrendreClichePonctuel().getNomFic());
    Assert.assertEquals("NomClichePeriodique_Cc_T2", ccT2.getPrendreClichePeriodique().getNomFic());
    //modifie dans les fichiers pour avoir des temps différents:
    Assert.assertEquals("P0Y0M0DT1H0M7S", DateDurationConverter.durationToXsd(ccT2.getPrendreClichePonctuel().getTempsSimu()));
    Assert.assertEquals("P0Y0M0DT0H1M8S", DateDurationConverter.durationToXsd(((PdtCst) ccT2.getPrendreClichePeriodique().getPdtRes()).getPdtCst()));

    final OrdCalcTrans ccT3 = (OrdCalcTrans) metier.getOrdCalc().get(5);
    Assert.assertTrue(ccT3 instanceof OrdCalcTransIniCalcCI);
    //modifie dans le fichier origine pour avoir nom unique
    Assert.assertEquals("P0Y0M1DT0H0M9S", DateDurationConverter.durationToXsd(ccT3.getDureeCalc()));
    Assert.assertEquals("P0Y0M0DT1H0M10S", DateDurationConverter.durationToXsd(((PdtCst) ccT3.getPdtRes()).getPdtCst()));
    Assert.assertEquals("NomClichePonctuel_Cc_T3", ccT3.getPrendreClichePonctuel().getNomFic());
    Assert.assertEquals("NomClichePeriodique_Cc_T3", ccT3.getPrendreClichePeriodique().getNomFic());
    //modifie dans les fichiers pour avoir des temps différents:
    Assert.assertEquals("P0Y0M0DT1H0M11S", DateDurationConverter.durationToXsd(ccT3.getPrendreClichePonctuel().getTempsSimu()));
    Assert.assertEquals("P0Y0M0DT0H1M12S", DateDurationConverter.durationToXsd(((PdtCst) ccT3.getPrendreClichePeriodique().getPdtRes()).getPdtCst()));
  }

  private static void verifieDonnees(final OrdCalcScenarioUiState metier) {

    Assert.assertNotNull(metier);
    final List<String> allCalcs = metier.getCalcNameInOrder();
    Assert.assertNotNull(allCalcs);
    Assert.assertEquals(6, allCalcs.size());
    final String[] expected = new String[]{"CC_P1", "CC_P2", "CC_P3", "CC_T1", "CC_T2", "CC_T3"};
    for (int i = 0; i < expected.length; i++) {
      Assert.assertEquals(expected[i], allCalcs.get(i));
    }

//
    final CalcOrdCalcUiState uiCCP1 = metier.getUiState("CC_P1");
    Assert.assertEquals("Cc_P1", uiCCP1.getCalcId());
    Assert.assertTrue(uiCCP1.getOrdCalc() instanceof OrdCalcPseudoPerm);
    Assert.assertEquals("NomCliche_Cc_P1", ((OrdCalcPseudoPerm) uiCCP1.getOrdCalc()).getPrendreClicheFinPermanent().getNomFic());

    final CalcOrdCalcUiState uiCCP2 = metier.getUiState("CC_P2");
    Assert.assertEquals("Cc_P2", uiCCP2.getCalcId());
    Assert.assertTrue(uiCCP1.getOrdCalc() instanceof OrdCalcPseudoPerm);
    Assert.assertEquals("NomCliche_Cc_P2", ((OrdCalcPseudoPerm) uiCCP2.getOrdCalc()).getPrendreClicheFinPermanent().getNomFic());

    final CalcOrdCalcUiState uiCCP3 = metier.getUiState("CC_P3");
    Assert.assertEquals("Cc_P3", uiCCP3.getCalcId());
    final OrdCalcPseudoPermIniCliche ccp3 = (OrdCalcPseudoPermIniCliche) uiCCP3.getOrdCalc();
    Assert.assertEquals("NomCliche1", ccp3.getIniCalcCliche().getNomFic());
    Assert.assertEquals("NomCliche2", ccp3.getPrendreClicheFinPermanent().getNomFic());
//
    final CalcOrdCalcUiState uiccT1 = metier.getUiState("CC_T1");
    Assert.assertEquals("Cc_T1", uiccT1.getCalcId());
    final OrdCalcTrans cc_T1 = (OrdCalcTrans) uiccT1.getOrdCalc();
    Assert.assertNotNull(cc_T1);
    Assert.assertTrue(cc_T1 instanceof OrdCalcTransIniCalcPrecedent);
    //modifie dans les fichiers pour avoir des temps différents:
    Assert.assertEquals("P0Y0M1DT0H0M1S", DateDurationConverter.durationToXsd(cc_T1.getDureeCalc()));
    Assert.assertEquals("P0Y0M0DT1H0M2S", DateDurationConverter.durationToXsd(((PdtCst) cc_T1.getPdtRes()).getPdtCst()));
    Assert.assertEquals("NomCliche3", cc_T1.getPrendreClichePonctuel().getNomFic());
    Assert.assertEquals("NomCliche4", cc_T1.getPrendreClichePeriodique().getNomFic());
    //modifie dans les fichiers pour avoir des temps différents:
    Assert.assertEquals("P0Y0M0DT1H0M3S", DateDurationConverter.durationToXsd(cc_T1.getPrendreClichePonctuel().getTempsSimu()));
    Assert.assertEquals("P0Y0M0DT0H1M4S", DateDurationConverter.durationToXsd(((PdtCst) cc_T1.getPrendreClichePeriodique().getPdtRes()).getPdtCst()));
//
    final CalcOrdCalcUiState uiccT2 = metier.getUiState("CC_T2");
    Assert.assertEquals("Cc_T2", uiccT2.getCalcId());
    final OrdCalcTrans ccT2 = (OrdCalcTrans) uiccT2.getOrdCalc();
    Assert.assertTrue(ccT2 instanceof OrdCalcTransIniCalcCliche);
    final OrdCalcTransIniCalcCliche ccT2Cliche = (OrdCalcTransIniCalcCliche) ccT2;
    //modifie dans le fichier origine pour avoir nom unique
    Assert.assertEquals("NomClicheCc_T2", ccT2Cliche.getIniCalcCliche().getNomFic());

    Assert.assertEquals("P0Y0M1DT0H0M5S", DateDurationConverter.durationToXsd(ccT2.getDureeCalc()));
    Assert.assertEquals("P0Y0M0DT1H0M6S", DateDurationConverter.durationToXsd(((PdtCst) ccT2.getPdtRes()).getPdtCst()));
    Assert.assertEquals("NomClichePonctuel_Cc_T2", ccT2.getPrendreClichePonctuel().getNomFic());
    Assert.assertEquals("NomClichePeriodique_Cc_T2", ccT2.getPrendreClichePeriodique().getNomFic());
    //modifie dans les fichiers pour avoir des temps différents:
    Assert.assertEquals("P0Y0M0DT1H0M7S", DateDurationConverter.durationToXsd(ccT2.getPrendreClichePonctuel().getTempsSimu()));
    Assert.assertEquals("P0Y0M0DT0H1M8S", DateDurationConverter.durationToXsd(((PdtCst) ccT2.getPrendreClichePeriodique().getPdtRes()).getPdtCst()));
//
    final CalcOrdCalcUiState uiccT3 = metier.getUiState("CC_T3");
    Assert.assertEquals("Cc_T3", uiccT3.getCalcId());
    final OrdCalcTrans ccT3 = (OrdCalcTrans) uiccT3.getOrdCalc();
    Assert.assertTrue(ccT3 instanceof OrdCalcTransIniCalcCI);
    //modifie dans le fichier origine pour avoir nom unique
    Assert.assertEquals("P0Y0M1DT0H0M9S", DateDurationConverter.durationToXsd(ccT3.getDureeCalc()));
    Assert.assertEquals("P0Y0M0DT1H0M10S", DateDurationConverter.durationToXsd(((PdtCst) ccT3.getPdtRes()).getPdtCst()));
    Assert.assertEquals("NomClichePonctuel_Cc_T3", ccT3.getPrendreClichePonctuel().getNomFic());
    Assert.assertEquals("NomClichePeriodique_Cc_T3", ccT3.getPrendreClichePeriodique().getNomFic());
    //modifie dans les fichiers pour avoir des temps différents:
    Assert.assertEquals("P0Y0M0DT1H0M11S", DateDurationConverter.durationToXsd(ccT3.getPrendreClichePonctuel().getTempsSimu()));
    Assert.assertEquals("P0Y0M0DT0H1M12S", DateDurationConverter.durationToXsd(((PdtCst) ccT3.getPrendreClichePeriodique().getPdtRes()).getPdtCst()));
  }

  /**
   * Constructeur
   */
  public CrueOCALFileTest() {
    super(Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.OCAL, TestCoeurConfig.INSTANCE), FilesForTest.OCAL_V1_2);
  }

  private OrdCalcScenario readData() {
    return readData(FilesForTest.OCAL_V1_2);
  }

  private OrdCalcScenarioUiState readDataUI() {
    return readDataUI(FilesForTest.OCAL_V1_2);
  }

  private OrdCalcScenario readData(final String file) {
    return readData(file, TestCoeurConfig.INSTANCE);
  }

  private OrdCalcScenarioUiState readDataUI(final String file) {
    return readDataUI(file, TestCoeurConfig.INSTANCE);
  }

  private OrdCalcScenario readData(final String file, final CoeurConfigContrat version) {
    final CrueData data = readPreData(version);
    Assert.assertNotNull(data);
    final CtuluLog analyser = new CtuluLog();
    // -- lecture de ocal --//
    final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getVersion(CrueFileType.OCAL, version);
    final boolean valid = fileFormat.isValide(file, analyser);
    testAnalyser(analyser);
    Assert.assertTrue(valid);
    analyser.clear();
    final OrdCalcScenario jeuDonnees = (OrdCalcScenario) fileFormat.read(file, analyser, data).getMetier();
    testAnalyser(analyser);
    return jeuDonnees;
  }

  private OrdCalcScenarioUiState readDataUI(final String file, final CoeurConfigContrat version) {
    final CrueData data = readPreData(version);
    Assert.assertNotNull(data);
    final CtuluLog analyser = new CtuluLog();
    // -- lecture de ocal --//
    final Crue10FileFormat<OrdCalcScenario> fileFormat = Crue10FileFormatFactory.getVersion(CrueFileType.OCAL, version);
    final Crue10FileFormatOCAL ocalfileFormat = (Crue10FileFormatOCAL) fileFormat;
    final boolean valid = ocalfileFormat.isValide(file, analyser);
    testAnalyser(analyser);
    Assert.assertTrue(valid);
    analyser.clear();
    final OrdCalcScenarioUiState jeuDonnees = ocalfileFormat.readUiState(file, analyser, version.getCrueConfigMetier()).getMetier();
    testAnalyser(analyser);
    return jeuDonnees;
  }

  private CrueIOResu readDataNoTest(final String file, final CoeurConfigContrat version) {
    final CrueData data = readPreData(version);
    Assert.assertNotNull(data);
    final CtuluLog analyser = new CtuluLog();
    // -- lecture de ocal --//
    final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getVersion(CrueFileType.OCAL, version);
    final boolean valid = fileFormat.isValide(file, analyser);
    testAnalyser(analyser);
    Assert.assertTrue(valid);
    analyser.clear();
    return fileFormat.read(file, analyser, data);
  }

  private CrueData readPreData(final CoeurConfigContrat version) {
    final CtuluLog analyser = new CtuluLog();
    // -- lecture drso --//
    final CrueData data = (CrueData) Crue10FileFormatFactory.getVersion(CrueFileType.DRSO, TestCoeurConfig.INSTANCE).read(FilesForTest.DRSO_V1_3,
            analyser, createDefault(version)).getMetier();
    testAnalyser(analyser);

    // -- lecture dlhy --//
    Crue10FileFormatFactory.getVersion(CrueFileType.DLHY, TestCoeurConfig.INSTANCE).read(FilesForTest.DLHY_V1_3, analyser, data).getMetier();
    testAnalyser(analyser);
    // -- lecture de dclm --//
    final DonCLimMScenario donCLimMScenario = (DonCLimMScenario) Crue10FileFormatFactory.getVersion(CrueFileType.DCLM, TestCoeurConfig.INSTANCE).read(
            FilesForTest.DCLM_V_1_3, analyser, data).getMetier();
    data.setConditionsLim(donCLimMScenario);
    //lecture du fichier PCAL pour initialiser dureeSce, pdtRes pour version 1.1.1
    if (Crue10VersionConfig.isV1_1_1(version)) {
      Crue10FileFormatFactory.getVersion(CrueFileType.PCAL, TestCoeurConfig.INSTANCE_1_1_1).read(FilesForTest.PCAL_V1_1_1, analyser, data);
    }
    testAnalyser(analyser);
    return data;
  }

  /**
   * Lit le fichier XML et verifie certaines valeurs des différents ordres de calculs
   */
  @Test
  public void testLecture() {
    final OrdCalcScenario jeuDonnees = readData();
    verifieDonnees(jeuDonnees);
    verifieSortie(jeuDonnees);
  }

  /**
   * on vérifie la lecture des données ui uniquement
   */
  @Test
  public void testLectureUI() {
    final OrdCalcScenarioUiState jeuDonnees = readDataUI();
    verifieDonnees(jeuDonnees);
  }

  /**
   * la reprise definie en version 1.1.1 n'est plus supportée en version 1.2(+)
   */
  @Test
  public void testLecture_V1P1P1_RepriseFailed() {
    final CrueIOResu jeuDonnees = readDataNoTest(FilesForTest.OCAL_V1_1_1_TRANSITOIRE_REPRISE, TestCoeurConfig.INSTANCE_1_1_1);
    Assert.assertTrue(jeuDonnees.getAnalyse().containsSevereError());
    final CtuluLogRecord fatalError = jeuDonnees.getAnalyse().getRecords().iterator().next();
    Assert.assertEquals("io.ocal.v111.calRepriseNotSupported.error", fatalError.getMsg());
  }

  @Test
  public void testLecture_V1P1P1() {
    final OrdCalcScenario readData = readData(FilesForTest.OCAL_V1_1_1, TestCoeurConfig.INSTANCE_1_1_1);
    verifieDonneesV1_1_1(readData);
  }

  @Test
  public void testLectureEcriture_V1P1P1() {
    final OrdCalcScenario init = readData(FilesForTest.OCAL_V1_1_1, TestCoeurConfig.INSTANCE_1_1_1);
    final OrdCalcScenario writeAndRead = writeAndRead(init, TestCoeurConfig.INSTANCE_1_1_1);
    verifieDonneesV1_1_1(writeAndRead);
  }

  /**
   * Lecture du fichier XML puis Ecriture dans un fichier temporaire (et validation XSD de ce fichier) puis Lecture de ce fichier temporaire et
   * verification de certaines valeurs de différents ordres de calculs
   */
  @Test
  public void testLectureEcritureLecture() {

    // On lit le fichier ocal initial
    final OrdCalcScenario data = readData();
    Assert.assertNotNull(data);
    final OrdCalcScenario jeuDonnees = writeAndRead(data, TestCoeurConfig.INSTANCE);
    verifieDonnees(jeuDonnees);

  }

  @Test
  public void testLectureEcritureLectureUI() {

    // On lit le fichier ocal initial
    final OrdCalcScenarioUiState data = readDataUI();
    Assert.assertNotNull(data);
    final OrdCalcScenarioUiState jeuDonnees = writeAndReadUI(data, TestCoeurConfig.INSTANCE);
    verifieDonnees(jeuDonnees);

  }

  private void verifieSortie(final OrdCalcScenario jeuDonnees) {
    final Sorties sorties = jeuDonnees.getSorties();
    Assert.assertTrue(sorties.getAvancement().getSortieFichier());
    Assert.assertTrue(sorties.getResultat().getSortieFichier());
    Assert.assertTrue(sorties.getTrace().getSortieFichier());
    Assert.assertTrue(sorties.getTrace().getSortieEcran());
    Assert.assertEquals(SeveriteManager.DEBUG3, sorties.getTrace().getVerbositeEcran());
    Assert.assertEquals(SeveriteManager.DEBUG3, sorties.getTrace().getVerbositeFichier());
  }

  private OrdCalcScenario writeAndRead(final OrdCalcScenario data, final CoeurConfigContrat version) {
    final File f = createTempFile();

    // On ecrit
    final CtuluLog analyzer = new CtuluLog();
    final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.OCAL, version);
    fileFormat.writeMetierDirect(data, f, analyzer, CrueConfigMetierForTest.DEFAULT);
    testAnalyser(analyzer);
    final boolean valide = fileFormat.isValide(f, analyzer);
    testAnalyser(analyzer);
    Assert.assertTrue(valide);

    // On lit le fichier temporaire qui vient d'être créé
    final CtuluLog analyser = new CtuluLog();
    final OrdCalcScenario jeuDonnees = (OrdCalcScenario) fileFormat.read(f, analyser, readPreData(version)).getMetier();
    testAnalyser(analyser);
    return jeuDonnees;
  }

  private OrdCalcScenarioUiState writeAndReadUI(final OrdCalcScenarioUiState data, final CoeurConfigContrat version) {
    final File f = createTempFile();

    // On ecrit
    final CtuluLog analyzer = new CtuluLog();
    final Crue10FileFormat<OrdCalcScenario> fileFormat = Crue10FileFormatFactory.getVersion(CrueFileType.OCAL, version);
    final Crue10FileFormatOCAL ocalfileFormat = (Crue10FileFormatOCAL) fileFormat;
    ocalfileFormat.writeUiState(data, f, analyzer, CrueConfigMetierForTest.DEFAULT);
    testAnalyser(analyzer);
    final boolean valide = fileFormat.isValide(f, analyzer);
    testAnalyser(analyzer);
    Assert.assertTrue(valide);

    // On lit le fichier temporaire qui vient d'être créé
    final CtuluLog analyser = new CtuluLog();
    final OrdCalcScenarioUiState jeuDonnees = ocalfileFormat.readUiState(f, analyser, version.getCrueConfigMetier()).getMetier();
    testAnalyser(analyser);
    return jeuDonnees;
  }
}
