package org.fudaa.dodico.crue.io.dao;

import files.FilesForTest;
import org.fudaa.ctulu.xml.XmlVersionFinder;
import org.junit.Assert;
import org.junit.Test;

public class XmlVersionFinderTest {
  @Test
  public void testVersion() {
    final XmlVersionFinder finder = new XmlVersionFinder();
    Assert.assertEquals("1.1.1", finder.getVersion(getClass().getResource(FilesForTest.ETU_ETU_EX_V1_1_1)));
    Assert.assertEquals("1.2", finder.getVersion(getClass().getResource(FilesForTest.ETU_V1_2)));
    Assert.assertEquals("1.3", finder.getVersion(getClass().getResource(FilesForTest.ETU_V1_3)));
    Assert.assertNull(finder.getVersion(getClass().getResource(FilesForTest.DC_MODELE3)));
  }
}
