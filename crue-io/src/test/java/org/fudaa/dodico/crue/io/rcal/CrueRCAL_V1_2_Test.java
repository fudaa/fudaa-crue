package org.fudaa.dodico.crue.io.rcal;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.rcal.CrueDaoStructureRCAL.ResPdtDao;
import org.fudaa.dodico.crue.io.rcal.CrueDaoStructureRCAL.ResultatsCalculPermanentDao;
import org.fudaa.dodico.crue.io.rdao.*;
import org.fudaa.dodico.crue.io.rdao.BranchesDao.BrancheTypeDao;
import org.fudaa.dodico.crue.io.rdao.CasiersDao.CasierDao;
import org.fudaa.dodico.crue.io.rdao.CasiersDao.CasierTypeDao;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.VariableResDao;
import org.fudaa.dodico.crue.io.rdao.NoeudsDao.NoeudDao;
import org.fudaa.dodico.crue.io.rdao.NoeudsDao.NoeudTypeDao;
import org.fudaa.dodico.crue.io.rdao.ParametrageDao.DelimiteurDao;
import org.fudaa.dodico.crue.io.rdao.SectionsDao.SectionDao;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class CrueRCAL_V1_2_Test extends AbstractIOParentTest {
  public CrueRCAL_V1_2_Test() {
    super(Crue10FileFormatFactory.getVersion(CrueFileType.RCAL, TestCoeurConfig.INSTANCE_1_2), FilesForTest.RCAL_V1_2);
  }

  @Test
  public void testLecture() {

    final CrueDaoRCAL donnees = readData(FilesForTest.RCAL_V1_2, TestCoeurConfig.INSTANCE_1_2);

    Assert.assertEquals("Commentaire RCAL", donnees.getCommentaire());
    this.testContexte(donnees.getContexteSimulation());
    testParametrage(donnees.getParametrage());
    this.testStructureResultat(donnees.getStructureResultat());
    testResultatsCalculPermanents(donnees.getResCalcPerms());
    testResultatsCalculTransitoire(donnees.getResCalcTranss());
  }

  private void testContexte(final ContexteSimulationDao contexte) {
    Assert.assertEquals("2012-02-29T14:30:07", contexte.getDateSimulation());
    Assert.assertEquals("10.0.0", contexte.getVersionCrue());
    Assert.assertEquals("EtuEx.etu.xml", contexte.getEtude());
    Assert.assertEquals("Sc_M3-0_c10", contexte.getScenario().getNomRef());
    Assert.assertEquals("R2012-03-01-15h29m00s", contexte.getRun().getNomRef());
    Assert.assertEquals("Mo_M3-0_c10", contexte.getModele().getNomRef());
  }

  private void testResultatsCalculPermanents(final List<CrueDaoStructureRCAL.ResultatsCalculPermanentDao> resultatsCalculPermanents) {
    Assert.assertEquals(2, resultatsCalculPermanents.size());

    final ResultatsCalculPermanentDao ccP1 = resultatsCalculPermanents.get(0);
    Assert.assertEquals("Cc_P1", ccP1.getNomRef());
    Assert.assertEquals("M3-0_c10.rcal_0001.bin", ccP1.getHref());
    Assert.assertEquals(0, ccP1.getOffsetMot());
    final ResultatsCalculPermanentDao ccP2 = resultatsCalculPermanents.get(1);
    Assert.assertEquals("Cc_P2", ccP2.getNomRef());
    Assert.assertEquals("M3-0_c10.rcal_0001.bin", ccP2.getHref());
    Assert.assertEquals(561, ccP2.getOffsetMot());
  }
//

  private void testResultatsCalculTransitoire(final List<CrueDaoStructureRCAL.ResCalcTransDao> resultatsCalculTransitoire) {
    Assert.assertEquals(2, resultatsCalculTransitoire.size());

    final CrueDaoStructureRCAL.ResCalcTransDao ccT1 = resultatsCalculTransitoire.get(0);
    Assert.assertEquals("Cc_T1", ccT1.getNomRef());
    final List<ResPdtDao> ccT1resPdts = ccT1.getResPdts();
    Assert.assertEquals(2, ccT1resPdts.size());

    final ResPdtDao ccT1Res1 = ccT1resPdts.get(0);
    Assert.assertEquals("P0Y0M0DT0H0M0S", ccT1Res1.getTempsSimu());
    Assert.assertEquals("M3-0_c10.rcal_0002.bin", ccT1Res1.getHref());
    Assert.assertEquals(0, ccT1Res1.getOffsetMot());

    final ResPdtDao ccT1Res2 = ccT1resPdts.get(1);
    Assert.assertEquals("P0Y0M0DT1H0M0S", ccT1Res2.getTempsSimu());
    Assert.assertEquals("M3-0_c10.rcal_0002.bin", ccT1Res2.getHref());
    Assert.assertEquals(561, ccT1Res2.getOffsetMot());

    final CrueDaoStructureRCAL.ResCalcTransDao ccT2 = resultatsCalculTransitoire.get(1);
    Assert.assertEquals("Cc_T2", ccT2.getNomRef());
    final List<ResPdtDao> ccT2resPdts = ccT2.getResPdts();
    Assert.assertEquals(2, ccT2resPdts.size());

    final ResPdtDao ccT2Res1 = ccT2resPdts.get(0);
    Assert.assertEquals("P0Y0M0DT2H0M0S", ccT2Res1.getTempsSimu());
    Assert.assertEquals("M3-0_c10.rcal_0003.bin", ccT2Res1.getHref());
    Assert.assertEquals(0, ccT2Res1.getOffsetMot());

    final ResPdtDao ccT2Res2 = ccT2resPdts.get(1);
    Assert.assertEquals("P0Y0M0DT3H0M0S", ccT2Res2.getTempsSimu());
    Assert.assertEquals("M3-0_c10.rcal_0003.bin", ccT2Res2.getHref());
    Assert.assertEquals(561, ccT2Res2.getOffsetMot());
  }

  public static CrueDaoRCAL readData(final String fichier, final CoeurConfigContrat version) {
    final CtuluLog analyzer = new CtuluLog();
    final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.RCAL, version);
    final CrueDaoRCAL jeuDonneesLue = (CrueDaoRCAL) fileFormat.read(fichier, analyzer, createDefault(version)).getMetier();

    testAnalyser(analyzer);

    return jeuDonneesLue;
  }

  private void testParametrage(final ParametrageDao parametrage) {
    Assert.assertEquals(8, parametrage.getNbrOctetMot());
    final List<DelimiteurDao> delimiteurs = parametrage.getDelimiteur();
    Assert.assertEquals(7, delimiteurs.size());
    DelimiteurDao delimiteur = delimiteurs.get(0);
    Assert.assertEquals("ResCalcPseudoPerm", delimiteur.getNom());
    Assert.assertEquals("  RcalPp", delimiteur.getChaine());
    delimiteur = delimiteurs.get(6);
    Assert.assertEquals("CatEMHBranche", delimiteur.getNom());
    Assert.assertEquals(" Branche", delimiteur.getChaine());
  }

  private void testSectionInterpolee(final SectionsDao sections) {
    Assert.assertEquals(120, sections.getSectionInterpolee().getNbrMot());
    Assert.assertNull(sections.getSectionInterpolee().getVariableRes());
    final List<SectionDao> sectionInterpoleeList = sections.getSectionInterpolee().getSection();
    Assert.assertEquals(6, sectionInterpoleeList.size());
    testNbrMot(sectionInterpoleeList, 20);
    testNomRef(sectionInterpoleeList, "St_B1_00050", "St_B1_00150", "St_B1_00250", "St_B1_00350", "St_B1_00450", "St_PROF5");
  }

  private void testSectionProfil(final SectionsDao sections) {
    Assert.assertEquals(280, sections.getSectionProfil().getNbrMot());
    Assert.assertNull(sections.getSectionProfil().getVariableRes());
    final List<SectionDao> sectionProfilList = sections.getSectionProfil().getSection();
    Assert.assertEquals(14, sectionProfilList.size());
    testNbrMot(sectionProfilList, 20);
    testNomRef(sectionProfilList, "St_PROF1", "St_PROF10", "St_PROF2", "St_PROF3A", "St_PROF3AV", "St_PROF3B");
    Assert.assertEquals("St_Prof11", sectionProfilList.get(13).getNomRef());
  }

  private void testSectionSansGeometrie(final SectionsDao sections) {
    Assert.assertEquals(80, sections.getSectionSansGeometrie().getNbrMot());
    Assert.assertNull(sections.getSectionSansGeometrie().getVariableRes());
    final List<SectionDao> sectionProfilList = sections.getSectionSansGeometrie().getSection();
    Assert.assertEquals(4, sectionProfilList.size());
    testNbrMot(sectionProfilList, 20);
    testNomRef(sectionProfilList, "St_B5_Amont", "St_B5_Aval", "St_B8_Amont", "St_B8_Aval");
  }

  private void testSectionItem(final SectionsDao sections) {
    //Section Item:
    Assert.assertEquals(40, sections.getSectionIdem().getNbrMot());
    Assert.assertNull(sections.getSectionIdem().getVariableRes());
    final List<SectionDao> sectionIdemList = sections.getSectionIdem().getSection();
    Assert.assertEquals(2, sectionIdemList.size());
    testNbrMot(sectionIdemList, 20);
    testNomRef(sectionIdemList, "St_PROF3AM", "St_PROF6B");
  }

  private void testStructureResultat(final StructureResultatsDao structureResultats) {
    Assert.assertEquals(561, structureResultats.getNbrMot());
    //Attention fichier d'origine MODIFIE pour tester la lecture des variables au niveau StructureResultats:
    Assert.assertEquals(2, structureResultats.getVariableRes().size());
    testNomRef(structureResultats.getVariableRes(), "Zhaut", "Zf");
    testNoeuds(structureResultats.getNoeuds());
    testCasiers(structureResultats.getCasiers());
    testSections(structureResultats.getSections());
    testBranches(structureResultats.getBranches());

    Assert.assertNull(structureResultats.getModeles());
  }

  private void testVariableResClass(final List objectList) {
    for (final Object object : objectList) {
      Assert.assertEquals(VariableResDao.class, object.getClass());
    }
  }

  private void testNoeuds(final NoeudsDao noeuds) {
    Assert.assertEquals(8, noeuds.getNbrMot());
    final List<VariableResDao> variableRes = noeuds.getVariableRes();
    Assert.assertNull(variableRes);
    final NoeudTypeDao noeudNiveauContinuType = noeuds.getNoeudNiveauContinu();
    Assert.assertEquals(7, noeudNiveauContinuType.getNbrMot());
    final List<VariableResDao> variableResInType = noeudNiveauContinuType.getVariableRes();
    Assert.assertEquals(1, variableResInType.size());
    testVariableResClass(variableResInType);
    Assert.assertEquals("Z", variableResInType.get(0).getNomRef());
    final List<NoeudDao> noeudList = noeudNiveauContinuType.getNoeud();
    Assert.assertEquals(7, noeudList.size());
    for (int i = 0; i < noeudList.size(); i++) {
      Assert.assertEquals(1, noeudList.get(i).getNbrMot());
      Assert.assertEquals("Nd_N" + (i + 1), noeudList.get(i).getNomRef());
    }
  }

  /**
   * Parcourt les noms et tests l'égalité. Ne teste pas que les tailles sont les mêmes
   */
  public static void testNomRef(final List<? extends CommonResDao.WithNomRef> list, final String... noms) {
    final int nb = noms.length;
    for (int i = 0; i < nb; i++) {
      Assert.assertEquals(noms[i], list.get(i).getNomRef());
    }
  }

  public static void testNomRefExact(final List<? extends CommonResDao.WithNomRef> list, final String... noms) {
    Assert.assertEquals(list.size(), noms.length);
    testNomRef(list, noms);
  }

  private static void testNbrMot(final List<? extends CommonResDao.NbrMotDao> list, final int nbrMot) {
    for (final CommonResDao.NbrMotDao nbrMotDao : list) {
      Assert.assertEquals(nbrMot, nbrMotDao.getNbrMot());
    }
  }

  private void testSections(final SectionsDao sections) {
    Assert.assertEquals(521, sections.getNbrMot());
    final List<VariableResDao> variableResList = sections.getVariableRes();
    Assert.assertEquals(20, variableResList.size());
    testVariableResClass(variableResList);
    for (final VariableResDao variableResDao : variableResList) {
      Assert.assertEquals(VariableResDao.class, variableResDao.getClass());
    }
    Assert.assertEquals("Dact", variableResList.get(0).getNomRef());
    Assert.assertEquals("Zn", variableResList.get(19).getNomRef());
    testSectionItem(sections);
    testSectionInterpolee(sections);
    testSectionProfil(sections);
    testSectionSansGeometrie(sections);
  }

  private void testCasiers(final CasiersDao casiers) {
    Assert.assertEquals(9, casiers.getNbrMot());
    final List<VariableResDao> variableResList = casiers.getVariableRes();
    Assert.assertEquals(4, variableResList.size());
    testVariableResClass(variableResList);
    for (int i = 0; i < variableResList.size(); i++) {
      Assert.assertEquals(" var " + i + ": " + variableResList.get(i).getNomRef(), VariableResDao.class,
          variableResList.get(i).getClass());
    }
    testNomRef(variableResList, "Qech", "Splan", "Vol", "Z");

    final CasierTypeDao casierProfil = casiers.getCasierProfil();
    Assert.assertEquals(8, casierProfil.getNbrMot());
    final List<VariableResDao> variableResInTypeList = casierProfil.getVariableRes();
    Assert.assertNull(variableResInTypeList);
    final List<CasierDao> casierList = casierProfil.getCasier();
    testNbrMot(casierList, 4);
    testNomRef(casierList, "Ca_N6", "Ca_N7");
  }

  private void testBranches(final BranchesDao branches) {
    Assert.assertEquals(22, branches.getNbrMot());
    Assert.assertNull(branches.getVariableRes());
    testBrancheOneVariable(branches.getBrancheBarrageFilEau(), "RegimeBarrage");
    testBrancheOneVariable(branches.getBrancheBarrageGenerique(), "RegimeBarrage");
    testBrancheOneVariable(branches.getBrancheNiveauxAssocies(), "Dz");
    //orifice
    testNomRefExact(branches.getBrancheOrifice().getVariableRes(), "CoefCtr", "RegimeOrifice");
    testBranche(branches.getBrancheOrifice().getBranche(), 1, 0, "Br_B8");

    testBrancheOneVariable(branches.getBranchePdc(), "Dz");

    testNomRefExact(branches.getBrancheSaintVenant().getVariableRes(), "Qlat", "SplanAct", "SplanSto", "SplanTot", "Vol");
    testBranche(branches.getBrancheSaintVenant().getBranche(), 3, 5, "Br_B1", "Br_B2", "Br_B4");
  }

  private void testBranche(final List<BranchesDao.BrancheDao> branches, final int size, final int nbrMot, final String... nom) {
    Assert.assertEquals(size, branches.size());
    testNbrMot(branches, nbrMot);
    testNomRefExact(branches, nom);
  }

  private void testBrancheOneVariable(final BrancheTypeDao branche, final String var) {
    Assert.assertEquals(0, branche.getNbrMot());
    Assert.assertNull(branche.getBranche());
    Assert.assertEquals(1, branche.getVariableRes().size());
    Assert.assertEquals(var, branche.getVariableRes().get(0).getNomRef());
  }
}
