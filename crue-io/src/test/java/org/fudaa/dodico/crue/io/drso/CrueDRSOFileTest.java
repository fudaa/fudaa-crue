package org.fudaa.dodico.crue.io.drso;

import files.FilesForTest;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.AbstractIOParentTest;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;

/**
 * Tests junit pour les fichiers DRSO IO. Xml, parsing et xstream.
 *
 * @author Adrien Hadoux
 */
public class CrueDRSOFileTest extends AbstractIOParentTest {
    public CrueDRSOFileTest() {
        super(Crue10FileFormatFactory.getVersion(CrueFileType.DRSO, TestCoeurConfig.INSTANCE), FilesForTest.DRSO_V1_3);
    }

    @Test
    public void testEcriture() {
        final CtuluLog analyzer = new CtuluLog();
        final CrueData writeModele = writeModele(FilesForTest.DRSO_V1_3, analyzer);
        testAnalyser(analyzer);
        checkModele(writeModele);
        checkComment(writeModele);
    }

    private CrueData writeModele(final String file, final CtuluLog analyzer) {
        return writeModele(file, analyzer, TestCoeurConfig.INSTANCE);
    }

    private CrueData writeModele(final String file, final CtuluLog analyzer, final CoeurConfigContrat version) {

        // --lecture --//
        final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.DRSO,
                version);
        final CrueData data = (CrueData) fileFormat.read(file, analyzer, CrueDRSOFileTest.createDefault(version)).getMetier();
        testAnalyser(analyzer);
        final CtuluLog inAnalyzer = new CtuluLog();
        // -- ecriture --//
        final File fichierEcriture = createTempFile();
        fileFormat.write(data, fichierEcriture, inAnalyzer);
        testAnalyser(inAnalyzer);
        fileFormat.isValide(fichierEcriture, inAnalyzer);
        testAnalyser(inAnalyzer);
        return (CrueData) fileFormat.read(fichierEcriture, analyzer, CrueDRSOFileTest.createDefault(version)).getMetier();
    }

    /**
     * Test Modele3.
     */
    @Test
    public void testLecture() {
        final CtuluLog analyzer = new CtuluLog();
        final CrueData data = (CrueData) format.read(FilesForTest.DRSO_V1_3, analyzer, CrueDRSOFileTest.createDefault(TestCoeurConfig.INSTANCE))
                .getMetier();
        testAnalyser(analyzer);
        checkModele(data);
        checkComment(data);
    }

    @Test
    public void testLecture_V_1_2() {
        final CtuluLog analyzer = new CtuluLog();
        final CrueData data = (CrueData) getFileFormat(CrueFileType.DRSO, TestCoeurConfig.INSTANCE_1_2).read(FilesForTest.DRSO_V1_2, analyzer, CrueDRSOFileTest.createDefault(TestCoeurConfig.INSTANCE))
                .getMetier();
        testAnalyser(analyzer);
        checkModele(data);
        checkComment(data);
    }

    /**
     * Test Modele3.
     */
    @Test
    public void testLecture_V1_1_1() {
        final CtuluLog analyzer = new CtuluLog();
        final Crue10FileFormat<Object> fileFormat = getFileFormat(CrueFileType.DRSO, TestCoeurConfig.INSTANCE_1_1_1);
        final boolean valide = fileFormat.isValide(FilesForTest.DRSO_V1_1_1, analyzer);
        testAnalyser(analyzer);
        Assert.assertTrue(valide);
        final CrueData data = (CrueData) fileFormat.read(FilesForTest.DRSO_V1_1_1, analyzer,
                CrueDRSOFileTest.createDefault(TestCoeurConfig.INSTANCE_1_1_1)).getMetier();
        testAnalyser(analyzer);
        Assert.assertNotNull(data);
        checkModele(data);
    }

    /**
     * Test Modele3.
     */
    @Test
    public void testEcriture_V1_1_1() {
        final CtuluLog analyzer = new CtuluLog();
        final CrueData data = writeModele(FilesForTest.DRSO_V1_1_1, analyzer, TestCoeurConfig.INSTANCE_1_1_1);
        testAnalyser(analyzer);
        Assert.assertNotNull(data);
        checkModele(data);
    }
    @Test
    public void testEcriture_V_1_2() {
        final CtuluLog analyzer = new CtuluLog();
        final CrueData data = writeModele(FilesForTest.DRSO_V1_2, analyzer, TestCoeurConfig.INSTANCE_1_2);
        testAnalyser(analyzer);
        Assert.assertNotNull(data);
        checkModele(data);
    }

    private void checkModele(final CrueData data) {
        Assert.assertNotNull(data);
        final DonPrtGeoProfilSection emh = data.getDefinedProfilSection("Ps_Prof11");
        Assert.assertNotNull(emh);
        // -- recherche de la section interpolée SB1_00150.0--//
        Assert.assertNotNull(data.findSectionByReference("St_B1_00150"));

        // -- verifie que SPROF3AM eszt une section idem --//
        Assert.assertTrue(data.findSectionByReference("St_PROF3AM") instanceof EMHSectionIdem);

        final EMH prof4 = EMHHelper.selectEMHInRelationByRef(data.findBrancheByReference("Br_B2"), "St_PROF4");
        Assert.assertNotNull(prof4);
        final List<DonPrtGeo> collectDPTG = EMHHelper.collectDPTG(prof4);
        Assert.assertEquals(1, collectDPTG.size());
        final DonPrtGeoProfilSection section = (DonPrtGeoProfilSection) collectDPTG.get(0);
        Assert.assertEquals("PS_PROF4", section.getId());
        final EMHSectionIdem findByReference = (EMHSectionIdem) data.findSectionByReference("St_PROF6B");
        Assert.assertEquals("St_PROF6A", findByReference.getSectionRef().getNom());
        final CatEMHCasier findCasierByReference = data.findCasierByReference("Ca_N6");
        final List<DonPrtGeo> dptg = findCasierByReference.getDPTG();
        Assert.assertEquals(3, dptg.size());
        Assert.assertEquals("Pc_N6_001", ((DonPrtGeoProfilCasier) dptg.get(0)).getNom());
        Assert.assertEquals("Pc_N6_002", ((DonPrtGeoProfilCasier) dptg.get(1)).getNom());
        //fichier initial modifie pour ajouter bati:
        Assert.assertEquals("Bc_N6", ((DonPrtGeoBatiCasier) dptg.get(2)).getNom());
        Assert.assertNotNull(emh);
    }

    private void checkComment(final CrueData data) {
        Assert.assertEquals("Commentaire pour test Nd_N1", data.findNoeudByReference("Nd_N1").getCommentaire());
        Assert.assertEquals("Commentaire pour test St_B1_00050", data.findSectionByReference("St_B1_00050").getCommentaire());
        Assert.assertEquals("Commentaire pour test Ca_N6", data.findCasierByReference("Ca_N6").getCommentaire());
        Assert.assertEquals("Commentaire pour test Br_B1", data.findBrancheByReference("Br_B1").getCommentaire());
    }
}
