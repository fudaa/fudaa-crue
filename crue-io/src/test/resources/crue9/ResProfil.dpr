* __________________________________________________________________________
*
*                            FICHIER MODELE POUR
*                            LE TRACE DES PROFILS
*
*                        Derniere maj: CS - 17/05/2005
* __________________________________________________________________________
*
* Conventions d'ecriture utilisees pour decrire la syntaxe :
*
*    [ ]          Entoure des parametres optionnels.
*    { }          Entoure des parametres obsol�tes.
*    |            Separe les choix d'une liste.
*    ...          Specifie que l'element peut-etre repete.
*    < >          Entoure les parametres obligatoires a fournir.
*    MAJUSCULE    Indique les mots cles reserves.
*    \            Indique une continuation de ligne
*
* Sauf precision, toutes les tailles sont en centimetres.
*
* Les temps demandes sont toujours au format
* <jours> <heures> <minutes> <secondes>
* __________________________________________________________________________
*
*                1. Demandes de traces de Profils
*
*     1.1.  Un trace par demande
*
*     CASIER    <Nom Casier>                 [ Legende ]
*     PROFIL    <Nom Profil Fluvial>         [ Legende ]
*     STRI      <Nom d'un Profil Strickler>  [ Legende ]
*
*     Ces cartes sont mutuellement exclusives sur la meme feuille
*     de trace, qui peut contenir plusieurs demandes du meme type.
*
*     DONN                                   [ Legende ]
*       X/Z <x1> <z1> <x2> <z2> ...
*       X/Z ...
*     FIN
*
*     1.2.  Plusieurs traces par demande
*
*     TOUS PROFIL [ BRANCHE <Nom Branche> ... ]
*     TOUS STRI   [ BRANCHE <Nom Branche> ... ]
*     TOUS CASIER
*
*     Ces 3 cartes peuvent etre combinees.
*
*     1.3.  Decalage de l'abscisse
*
*     SUITE DELTAY <decalage>
*
*     Les cartes 1.1 et 1.2 sont mutuellement exclusives.
*           ____________________________________________
*
*               2. Demande de traces de niveau d'eau
*
*      CALCUL  <nom fichier calcul>
*      TEMPS   <jours> <heures> <minutes> <secondes>  [ Legende ]
*      ...
*      ENVINF [ Legende ]
*      ENVSUP [ Legende ]
*
*      Une seule carte CALCUL par demande de trace (1.1,1.2)
*           ____________________________________________
*
*                3. Personnalisation du Trace (1.,2.)
*
*     Couleur de la courbe
*     { SUITE NUMPLU <1|2|3|4|5|6|7|8|9> }
*
*     Materialisation des points par un symbole
*     { SUITE NUMLIG 0 NUMSYM <n> }
*
*     Type de ligne (si negatif, surcharge d'un symbole en chaque point)
*     { SUITE NUMLIG <-7|-6|-5|-4|-3|-2|-1|1|2|3|4|5|6|7>  }
*
*     Epaisseur du trait
*     { SUITE EPAIS <e> } (def:0.02)
*
*     Annulation du trace de la legende
*     { SUITE NOLEG }
*           ____________________________________________
*
*                 4. Amenagement auxiliaire
*
*     AMENAG  <nom fichier amenagement>
*     Interdite si carte TOUS.
*           ____________________________________________
*
*                 5. Changement de feuille de trace
*
*     FINTRA
*
*     Interdite si carte TOUS.
*     Ne pas mettre en fin de fichier
*     Tous les parametres modifies reprennent leur valeur par defaut.
*     Le fichier amenagement redevient l'amenagement principal, defini au menu.
*           ____________________________________________
*
*                 6. Parametres du trace
*
*     Modification de la taille du papier
*     { TAILLE <A5|A4|A3|A2|A1|A0>  } (def: A3)
*     { TAILLE <x> <y> }              (x,y : taille papier)
*     { TAILLE GRAPHE <tx> <ty>  }    (tx,ty : taille du systeme d'axes)
*
*     Ajout d'un titre
*     [ TITRE <titre> ]
*     ... (5 lignes maxi)
*
*     Trace le titre contenu dans le fichier amenagement principal
*     [ TITRDC   ]
*
*     Extremums de l'axe des abscisses
*     [ AXEY  <ymin>  <ymax> ] (def: calcules)
*
*     Extremums de l'axe des ordonnees
*     [ AXEZ  <zmin>  <zmax> ] (def: calcules)
*
*     Echelle en abscisse (en cm/unite de longueur)
*     { ECHY  <ech>} (def: calcules)
*
*     Echelle en ordonnee (en cm/unite de longueur)
*     { ECHZ  <ech>} (def: calcules)
*
*     Ecart entre 2 graduations
*     [ GRADZ <ecaz>] (def: calcules)
*
*     Nombre de decimales de la legende d'un axe
*     { NBDECZ <nombre de decimales> } (def: 2)
*
*     Trace des cotes pour tous les profils et
*     des traits de rappel pour le profil principal.
*     [ COTE ]
*
*     Trace des limites de lits, noms et stricklers
*     A combiner avec COTE pour avoir les lits de toutes les courbes
*     [ TRALITS ]
*
*     { ESPAC1 <marge entre titres et graphe> }           (def: 1)
*     { ESPAC2 <marge entre graphe et rappel des cotes> } (def: 0)
*
*     Marges entre le cadre et le graphique
*     { DXGAU <marge gauche> }  (def: 2)
*     { DXDRO <marge droite> }  (def: 1)
*     { DYINF <marge bas>    }  (def: 1)
*     { DYSUP <marge haute>  }  (def: 1)
*
*     Marges entre le bord et le cadre
*     { BCGAU <marge gauche> }  (def: 0)
*     { BCDRO <marge droite> }  (def: 0)
*     { BCINF <marge basse>  }  (def: 0)
*     { BCSUP <marge haute>  }  (def: 0)
*
*     Hauteur des caracteres
*     { HCTIT <hauteur titres >  } (def: 0.3)
*     { HCAR  <hauteur legendes> } (def: 0.2)
*     { HSYMB <hauteur symboles> } (def: 0.2)
*     { HCPROF <hauteur cotes et distances> } (def: HCAR)
*
*     Suppression du cadre
*     { NOCADR }
*
*     Trace d'un quadrillage (0: aucun,1 a 7: type de ligne)
*     { GRILLE <type> } (def: 0)
*
*     Ne pas tracer la fente si elle existe
*     [ NOFENTE ]
*__________________________________________________________________________
*
*        DEBUT DPR
*__________________________________________________________________________
PROFIL PROF6B
PROFIL PROF5
PROFIL PROF4
