/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.conf;

/**
 *
 * Contenu d'une sauvegarde d'une données
 *
 * @author Frederic Deniger
 */
public class CrueEtudeExternRessource {

  private CrueEtudeExternRessourceInfos infos;
  private CrueEtudeExternAdditionalFileSaver fileSaver;

  public CrueEtudeExternRessource() {
  }

  public CrueEtudeExternRessource(CrueEtudeExternRessourceInfos infos, CrueEtudeExternAdditionalFileSaver fileSaver) {
    this.infos = infos;
    this.fileSaver = fileSaver;
  }

  public CrueEtudeExternRessource(CrueEtudeExternRessourceInfos infos) {
    this.infos = infos;
  }

  public CrueEtudeExternRessourceInfos getInfos() {
    return infos;
  }

  public void setInfos(CrueEtudeExternRessourceInfos infos) {
    this.infos = infos;
  }

  /**
   *
   * @return le charge de sauvegarde de fichiers complémentaire.
   */
  public CrueEtudeExternAdditionalFileSaver getFileSaver() {
    return fileSaver;
  }

  public void setFileSaver(CrueEtudeExternAdditionalFileSaver fileSaver) {
    this.fileSaver = fileSaver;
  }
}
