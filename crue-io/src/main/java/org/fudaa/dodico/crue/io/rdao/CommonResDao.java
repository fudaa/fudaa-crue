/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;

public class CommonResDao {

  public static class LoiPredicate implements Predicate<VariableResDao> {

    @Override
    public boolean evaluate(final VariableResDao in) {
      return in != null && in.isLoiDefinition();
    }
  }

  private static class ToNomRef implements Transformer<WithNomRef,String> {

    @Override
    public String transform(final WithNomRef input) {
      if (input == null) {
        return null;
      }
      return input.getNomRef();
    }
  }

  private static class ToId implements Transformer {

    @Override
    public Object transform(final Object input) {
      if (input == null) {
        return null;
      }
      return ((VariableResDao) input).getId();
    }
  }

  public static Collection<String> getNomRef(final Collection<? extends WithNomRef> in) {
    if (CollectionUtils.isEmpty(in)) {
      return Collections.emptyList();
    }
    final List<String> res = new ArrayList<>();
    CollectionUtils.collect(in, new ToNomRef(), res);
    return res;
  }

  public static Collection<String> getId(final Collection<? extends VariableResDao> in) {
    if (CollectionUtils.isEmpty(in)) {
      return Collections.emptyList();
    }
    final List<String> res = new ArrayList<>();
    CollectionUtils.collect(in, new ToId(), res);
    return res;
  }

  public static List<VariableResLoiDao> selectResLoiDao(final List<VariableResDao> in) {
    final List<VariableResLoiDao> res = new ArrayList<>();
    if (in == null) {
      return res;
    }
    CollectionCrueUtil.select(in, new LoiPredicate(), res);
    return res;
  }

  public static Collection<String> selectResLoiDaoId(final List<VariableResDao> in) {
    final List<VariableResLoiDao> selectResLoiDao = selectResLoiDao(in);
    return getId(selectResLoiDao);
  }

  public CommonResDao() {
  }

  public static class NbrMotDao {

    int NbrMot;

    public int getNbrMot() {
      return NbrMot;
    }
  }

  public static class TypeEMHDao extends NbrMotDao {

    private String typeEMH;

    public String getTypeEMH() {
      return typeEMH;
    }

    public void setTypeEMH(final String typeEMH) {
      this.typeEMH = typeEMH;
    }
  }

  public interface WithNomRef {

    String getNomRef();
  }

  public static class NbrMotNomRefDao extends NbrMotDao implements WithNomRef {

    String NomRef;

    @Override
    public String getNomRef() {
      return NomRef;
    }
  }

  public static class NomRefDao implements WithNomRef {

    protected String NomRef;

    @Override
    public String getNomRef() {
      return NomRef;
    }

    public void setNomRef(final String NomRef) {
      this.NomRef = NomRef;
    }
  }

  public static class VariableResDao implements WithNomRef {

    private String NomRef;
    /**
     * Id correspond au NomRef sans la majuscule au début
     */
    protected String id;

    public VariableResDao() {
    }

    public VariableResDao(final String NomRef) {
      setNomRef(NomRef);
    }

    final void setNomRef(final String NomRef) {
      this.NomRef = NomRef;
      updateId();
    }

    protected void updateId() {
      id = StringUtils.uncapitalize(NomRef);
    }

    
    /**
     * 
     * @return  le nom java (NomRef avec la premiere lettre en minuscule).
     */
    public String getId() {
      return id;
    }

    @Override
    public String getNomRef() {
      return NomRef;
    }

    public boolean isLoiDefinition() {
      return false;
    }

    public boolean isLoiLoiNbrPtDao() {
      return false;
    }
    public boolean isQRegul() {
      return false;
    }
    public boolean isZRegul() {
      return false;
    }
  }

  public static class VariableResQregulDao extends VariableResDao {

    @Override
    public boolean isQRegul() {
      return true;
    }
  }
  public static class VariableResZregulDao extends VariableResDao {
    @Override
    public boolean isZRegul() {
      return true;
    }
  }

  public static class VariableResLoiDao extends VariableResDao {

    public VariableResLoiDao(final String NomRef) {
      super(NomRef);
    }

    public VariableResLoiDao() {
    }

    @Override
    public boolean isLoiDefinition() {
      return true;
    }
  }

  public static class VariableResLoiNbrPtDao extends VariableResDao {

    int NbrPt;

    public VariableResLoiNbrPtDao() {
    }

    public VariableResLoiNbrPtDao(final String nomRef, final int nbrPt) {
      super(nomRef);
      this.NbrPt = nbrPt;
    }

    public int getNbrPt() {
      return NbrPt;
    }

    @Override
    public boolean isLoiLoiNbrPtDao() {
      return true;
    }
  }

  public static class ItemResDao extends CommonResDao.NbrMotNomRefDao {

    List<CommonResDao.VariableResLoiNbrPtDao> VariableResLoiNbrPt;

    public ItemResDao() {
    }

    public ItemResDao(final String nomRef) {
      this.NomRef = nomRef;

    }

    public List<VariableResLoiNbrPtDao> getVariableResLoiNbrPt() {
      return VariableResLoiNbrPt;
    }
  }
}
