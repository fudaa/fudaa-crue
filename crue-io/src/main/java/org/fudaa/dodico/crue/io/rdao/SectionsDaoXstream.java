/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import com.thoughtworks.xstream.XStream;

/**
 *
 * @author deniger
 */
public class SectionsDaoXstream {
 public static void configureXstream(XStream xstream) {
    xstream.alias("Sections", SectionsDao.class);

    xstream.alias("SectionIdem", SectionsDao.SectionTypeDao.class);
    xstream.alias("SectionsInterpolee", SectionsDao.SectionTypeDao.class);
    xstream.alias("SectionsProfil", SectionsDao.SectionTypeDao.class);
    xstream.alias("SectionsSansGeometrie", SectionsDao.SectionTypeDao.class);

    xstream.alias("Section", SectionsDao.SectionDao.class);

    xstream.addImplicitCollection(SectionsDao.SectionTypeDao.class, "Section", SectionsDao.SectionDao.class);

    xstream.addImplicitCollection(SectionsDao.class, "VariableRes", CommonResDao.VariableResDao.class);
    xstream.addImplicitCollection(SectionsDao.SectionTypeDao.class, "VariableRes", CommonResDao.VariableResDao.class);


  }
}
