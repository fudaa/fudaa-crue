/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rcal;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.IdentityMap;
import org.fudaa.dodico.crue.io.res.ResultatEntry;
import org.fudaa.dodico.crue.metier.emh.ResultatPasDeTempsDelegate;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;

/**
 *
 * @author deniger
 */
public class Crue10ResultatPasDeTempsDelegate implements ResultatPasDeTempsDelegate {

  private final Map<ResultatTimeKey, ResultatEntry> entries;
  private final List<ResultatTimeKey> orderedKeys;
  //pour optimiser la lecture multiple. La méthode File.exists() peut être très longue.
  private final IdentityMap existsFile = new IdentityMap();

  public Crue10ResultatPasDeTempsDelegate(final List<ResultatTimeKey> orderedKeys, final Map<ResultatTimeKey, ResultatEntry> entries) {
    this.entries = Collections.unmodifiableMap(entries);
    this.orderedKeys = Collections.unmodifiableList(orderedKeys);
  }

  /**
   * Utilise un calque pour des raisons de performances.
   *
   * @param file
   * @return true si le fichier existe
   */
  public boolean exists(final File file) {
    if (file == null) {
      return false;
    }
    Boolean exist = (Boolean) existsFile.get(file);
    if (exist == null) {
      exist = file.isFile();
      existsFile.put(file, exist);
    }
    return exist;
  }

  @Override
  public int getNbResultats() {
    return entries.size();
  }

  public Map<ResultatTimeKey, ResultatEntry> getEntries() {
    return entries;
  }

  @Override
  public boolean containsResultsFor(final ResultatTimeKey key) {
    return entries.containsKey(key);
  }

  @Override
  public Collection<ResultatTimeKey> getResultatKeys() {
    return orderedKeys;
  }
}
