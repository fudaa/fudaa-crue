/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.dcsp.CrueConverterDCSP;
import org.fudaa.dodico.crue.io.dcsp.CrueDaoStructureDCSP;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;

public class CrueFileFormatBuilderDCSP implements CrueFileFormatBuilder<CrueData> {

  @Override
  public Crue10FileFormat<CrueData> getFileFormat(final CoeurConfigContrat version) {
    return new Crue10FileFormat<>(new CrueDataXmlReaderWriterImpl<>(CrueFileType.DCSP,
        version, new CrueConverterDCSP(version.getXsdVersion()), new CrueDaoStructureDCSP(version.getXsdVersion())));

  }

}
