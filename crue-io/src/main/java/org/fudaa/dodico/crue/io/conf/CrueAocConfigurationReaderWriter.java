/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.conf;

import org.fudaa.dodico.crue.common.io.CrueDaoStructure;
import org.fudaa.dodico.crue.common.io.CrueXmlReaderWriterImpl;

/**
 * @author CANEL Christophe
 *
 */
public class CrueAocConfigurationReaderWriter extends CrueXmlReaderWriterImpl<CrueAocDaoConfiguration, CrueAocDaoConfiguration> {

  public CrueAocConfigurationReaderWriter(final String version, final CrueDaoStructure courbeConfigurer) {
    super("etude_configuration", version, new CrueAocConverter(), new CrueAocDaoStructure(courbeConfigurer));
  }
}
