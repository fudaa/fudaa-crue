/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import gnu.trove.TDoubleArrayList;
import gnu.trove.TLongArrayList;
import gnu.trove.TObjectIntHashMap;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.CalcTrans;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.OrdCalc;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.metier.result.Crue9ResultatCalculPasDeTemps;

/**
 * Reader pour FCB.
 *
 * @author Adrien Hadoux
 */
public class FCBReader extends AbstractCrueBinaryReader<FCBSequentialReader> {

  final int sizeTitre = 256;

  /**
   * Retourne la liste des indices des pas de temps *
   *
   * @return quoi ?
   */
  @Override
  protected FCBSequentialReader internalReadResu() throws IOException {
    final FCBSequentialReader infos = new FCBSequentialReader(file);
    // le premier entier doit etre sizeTitre*10.
    // cette premiere partie est un procede pour savoir si on est en little ou big endian.
    // on sait que le premier entier doit etre 800; soit la longueur des 10 commentaires comportant
    // 256 caractères.
    final int firstInt = 10 * sizeTitre;

    // on lit le premier enregistrement qui fait 800 p
    /*
     * final int readAll =
     */ helper.readAll(firstInt + 8);// 8 pour les 2 entiers avant/apres l'enregistrement
    ByteBuffer bf = helper.getBuffer();
    int tempInt = bf.getInt();
    // le premier entier ne vaut pas 80: on inverse l'ordre de lecture
    if (tempInt != firstInt) {
      helper.inverseOrder();
    }
    tempInt = bf.getInt(0);
    // toujours pas ... ce n'est pas un fichier correct
    if (tempInt != firstInt) {
      analyze_.addError("crue.io.fcb.notValid");
      return null;
    }
    infos.setOrder(helper.getOrder());

    // -- on lit les 10 premieres lignes de titre DC et DH dans le Buffer--//
    final int nbCommentDc = 5;
    final int nbCommentDh = 5;
    final List<String> commnentDc = new ArrayList<>(nbCommentDc);
    final List<String> commnentDh = new ArrayList<>(nbCommentDh);
    for (int i = 0; i < nbCommentDh; i++) {
      commnentDh.add(helper.getStingFromBuffer(sizeTitre));
    }
    for (int i = 0; i < nbCommentDc; i++) {
      commnentDc.add(helper.getStingFromBuffer(sizeTitre));
    }

    infos.commentDc = commnentDc;
    infos.commentDh = commnentDh;

    // on recupere la ligne write(ifi2) iprm,sngl(past),icodav,ibmax,nomdc,nomdh
    helper.readData();
    bf = helper.getBuffer();

    // iprm i4 : nombre de profils du modèle
    // on lit la
    final int nbProfils = bf.getInt();

    // past r8 : pas de temps d'impression des résultats, en secondes.
    // transformer en single ->ok
    /*
     * final double r8 = *
     */
    bf.getFloat();

    // icodav i4 : présence (1) ou non (0) d'une régulation
    // icodav==0 ou 2 pas de regulation
    // icodav==1 ou 3 regulation
    // icodav=2 ou 4 zfond_ssfente present
    final int icodav = bf.getInt();
    infos.sousFente = (icodav >= 2);
    infos.regulation = (icodav == 1 || icodav == 3);
    final int nbBranches = bf.getInt();
    final int nbCharTitreDCDh = 256;
    /*
     * final String nomDc = *
     */
    helper.getStingFromBuffer(nbCharTitreDCDh);
    /*
     * final String nomDh = *
     */
    helper.getStingFromBuffer(nbCharTitreDCDh);

    // -- on passe a la ligne contenant ibmax et iparam --//
    // on arrive a la ligne des profils --//
    // write(ifi2) (tit(ipro),sngl(dist(ipro)),zfond(nuprfl(ipro)), ipro,ipro=1,iprm)
    // on recupere l'indice du début des profils.
    readProfils(infos, nbProfils);

    // -- on passe la boucle de nbbranches lignes--//
    readBranches(infos, nbBranches);

    // -- ensuite on lit les points si ou ils sont affiches--//
    readNodes(infos);

    // es t ecrite, cf spec pour entete
    if (infos.isRegulation()) {
      helper.readSequentialData();
    }
    // -- on arrive au CORPS, on repere les indice de chaque PDT --//
    readPdt(infos);
    // --on passee les lignes de Crue--//
    return infos;

  }

  /**
   * Lit les infos des noeuds si .
   *
   * @param infos
   * @throws IOException
   */
  // (ifi2) nbpoin,(nompoi(ip),nupro(ip),ip=1,nbpoin)
  private void readNodes(final FCBSequentialReader infos) throws IOException {
    // nbpoin i4 : nombre de noeuds

    helper.readData();
    final ByteBuffer bf = helper.getBuffer();

    final int nbNd = bf.getInt();

    final List<FCBValueObject.EnteteCasier> nds = new ArrayList<>(nbNd);
    for (int i = 0; i < nbNd; i++) {
      final FCBValueObject.EnteteCasier nd = new FCBValueObject.EnteteCasier();
      nd.setNom(CruePrefix.addPrefix(helper.getStingFromBuffer(16).trim(), EnumCatEMH.NOEUD));
      nd.setNumProfil(bf.getInt() - 1);
      nds.add(nd);
    }
    infos.setNoeuds(nds);

  }

  /**
   * lit les pdt.
   *
   * @param infos
   * @throws IOException
   */
  public void readPdt(final FCBSequentialReader infos) throws IOException {
    // les tabbleau enregistrant les pdt ( regu et normal), leur position et ruinou
    final TLongArrayList posNormal = new TLongArrayList();
    final TDoubleArrayList pdtNormal = new TDoubleArrayList();
    final TDoubleArrayList ruinouNormal = new TDoubleArrayList();
    final List<Boolean> regul = new ArrayList<>();

    while (helper.getAvailable() > 0) {
      helper.readData();
      // ici on a la ligne
      // write(9) test,sngl(t) si regul
      // (ifi2) test,sngl(t),sngl(ruinou) pour un pas de temps normal

      // ces 2 tableaux enregistrent les données: si regu on les modifie
      final String id = helper.getStingFromBuffer(4);
      final boolean isRegu = "REGU".equals(id);
      // on enregistre le pas de temps
      pdtNormal.add(helper.getBuffer().getFloat());
      // en mode normal, on enregistre ruinou
      regul.add(isRegu);
      if (isRegu) {
        ruinouNormal.add(0);
      } else {
        ruinouNormal.add(helper.getBuffer().getFloat());
      }
      // on enregistre la position intéressante: celle juste avant les donnéées
      posNormal.add(helper.getCurrentPosition());
      // on saute la ligne profil ou regul
      helper.skipRecord();

      // si pas regu
      if (!isRegu) {
        // on saute la ligne branche
        helper.skipRecord();

        // on saute la ligne point
        helper.skipRecord();
      }
    }
    final CrueData dataLinked1 = super.getDataLinked();

    final List<OrdCalc> ordCalc = dataLinked1.getOCAL().getOrdCalc();
    final TObjectIntHashMap<ResultatTimeKey> indexByResultatKey = new TObjectIntHashMap<>();
    final boolean isTransitoire = EMHHelper.containsTransitoire(ordCalc);
    final List<ResultatTimeKey> orderedResultatKey = new ArrayList<>();
    if (isTransitoire) {
      final CalcTrans collectCalcTrans = EMHHelper.collectCalcTrans(ordCalc).get(0);//un seul calcul transitoire
      final String nomCalcul = collectCalcTrans.getNom();
      final int nbPdt = pdtNormal.size();
      long max = 0;
      for (int i = 0; i < nbPdt; i++) {
        final long duree = DateDurationConverter.toMillis(pdtNormal.get(i));
        max = Math.max(max, duree);
      }
      final boolean useDay = DateDurationConverter.isGreaterThanDay(max);
      for (int i = 0; i < nbPdt; i++) {
        final long duree = DateDurationConverter.toMillis(pdtNormal.get(i));
        final ResultatTimeKey resultatKey = new ResultatTimeKey(nomCalcul, duree, useDay);
        indexByResultatKey.put(resultatKey, i);
        orderedResultatKey.add(resultatKey);
      }
    } //on a que des cas permanents
    else {
      if (ordCalc.size() != pdtNormal.size()) {
        analyze_.addSevereError("io.fcb.nbResultatPseudoPermanentNonConcordant.erreur", Integer.toString(pdtNormal.size()),
                Integer.toString(ordCalc.size()));
      } else {
        final int nbPdt = pdtNormal.size();
        for (int i = 0; i < nbPdt; i++) {
          final ResultatTimeKey resultatKey = new ResultatTimeKey(ordCalc.get(i).getCalc().getNom());
          indexByResultatKey.put(resultatKey, i);
          orderedResultatKey.add(resultatKey);
        }
      }
    }
    infos.setPdt(new Crue9ResultatCalculPasDeTemps(posNormal, pdtNormal, regul, ruinouNormal,
            indexByResultatKey, orderedResultatKey));

  }

  /**
   * Lit les infos des branches
   *
   * @param infos
   * @throws IOException
   */
  private void readBranches(final FCBSequentialReader infos, final int nbBranches) throws IOException {
    // do 30 ib=1,ibmax
    // write(ifi2) ntyp(ib),imax(ib),(n0pr(ip,ib),ip=1,imax(ib)),
    // * nombr(ib),nupam(ib),nupav(ib)
    // 30 continue
    final List<FCBValueObject.EnteteBranche> brs = new ArrayList<>(nbBranches);
    for (int i = 0; i < nbBranches; i++) {
      helper.readData();
      final ByteBuffer buffer = helper.getBuffer();
      final FCBValueObject.EnteteBranche br = new FCBValueObject.EnteteBranche();
      br.setTypeBranche(buffer.getInt());
      final int nbProfil = buffer.getInt();
      final int[] numeroAbsProfil = new int[nbProfil];
      // on fait des -1 pour respecter la numerotation java et comencer a 0.
      for (int iProf = 0; iProf < nbProfil; iProf++) {
        numeroAbsProfil[iProf] = buffer.getShort() - 1;
      }
      br.setTableauIndiceProfils(numeroAbsProfil);
      br.setNom(CruePrefix.addPrefix(helper.getStingFromBuffer(16).trim(), EnumCatEMH.BRANCHE));
      br.setProfilAmont(buffer.getInt() - 1);
      br.setProfilAval(buffer.getInt() - 1);
      brs.add(br);
    }
    infos.setBranches(brs);
  }

  /**
   * lecture profils write(ifi2) (tit(ipro),sngl(dist(ipro)),zfond(nuprfl(ipro)), ipro,ipro=1,iprm)
   *
   * @throws IOException
   */
  private void readProfils(final FCBSequentialReader infos, final int nbProfils) throws IOException {
    // on recupere l'indice du début des profils.
    final List<FCBValueObject.EnteteProfil> profils = new ArrayList<>(nbProfils);
    final boolean containsSsfente = infos.isSousFente();
    // (tit(ipro),sngl(dist(ipro)),zfond(nuprfl(ipro))
    helper.readData();
    final ByteBuffer buffer = helper.getBuffer();
    for (int i = 0; i < nbProfils; i++) {
      final FCBValueObject.EnteteProfil prof = new FCBValueObject.EnteteProfil();
      String nom = helper.getStingFromBuffer(16).trim();
      if (org.apache.commons.lang3.StringUtils.isNotBlank(nom)) {
        nom = CruePrefix.addPrefix(nom, EnumCatEMH.SECTION);
      }
      prof.setNom(nom);

      prof.setDistance(buffer.getFloat());
      prof.setZf(buffer.getFloat());
      if (containsSsfente) {
        prof.setZfSsFente(buffer.getFloat());
      }
      final int ipro = buffer.getInt();
      // bizarre, l'indice est ecrit en derniere position
      if (ipro != (i + 1)) {
        analyze_.addError("io.crue.fcb.profilIndice.error");
        throw new IOException("io.crue.fcb.profilIndice.error");
      }
      profils.add(prof);

    }
    infos.setProfils(profils);
  }
}
