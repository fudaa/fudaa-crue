/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.extern;

import gnu.trove.TIntObjectHashMap;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Frederic Deniger
 */
public class ExternContent {

  private final String filePath;
  private String name;
  private String comment;
  final List<ExternContentColumn> columns = new ArrayList<>();
  final TIntObjectHashMap<String> labels = new TIntObjectHashMap<>();
  private int nbValues;

  public ExternContent(final String filePath) {
    this.filePath = filePath;
  }

  public String getFilePath() {
    return filePath;
  }

  public int getNbValues() {
    return nbValues;
  }

  protected void setNbValues(final int nbValues) {
    this.nbValues = nbValues;
  }

  public String getName() {
    return name;
  }

  protected void addLabel(final int i, final String label) {
    labels.put(i, label);
  }

  public String getLabel(final int i) {
    return labels.get(i);
  }

  protected void addColumn(final ExternContentColumn col) {
    columns.add(col);
  }

  void setName(final String name) {
    this.name = name;
  }

  public String getComment() {
    return comment;
  }

  public ExternContentColumn getColumn(final String name) {
    return contentsByName.get(name);
  }

  public List<String> getNames() {
    return new ArrayList<>(contentsByName.keySet());
  }

  void setComment(final String comment) {
    this.comment = comment;
  }
  final Map<String, ExternContentColumn> contentsByName = new LinkedHashMap<>();

  boolean finalyseData() {
    nbValues = 0;
    if (!columns.isEmpty()) {
      nbValues = columns.get(0).getSize();
      for (int i = 1; i < columns.size(); i++) {
        if (nbValues != columns.get(i).getSize()) {
          nbValues = 0;
          return false;
        }
      }
    }
    contentsByName.clear();
    for (final ExternContentColumn externContentColumn : columns) {
      contentsByName.put(externContentColumn.getTitle(), externContentColumn);

    }
    return true;
  }

  public TIntObjectHashMap<String> getLabelsMap() {
    return labels.clone();
  }
}
