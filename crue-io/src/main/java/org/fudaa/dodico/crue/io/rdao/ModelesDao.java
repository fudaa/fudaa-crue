/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import org.fudaa.dodico.crue.io.rdao.CommonResDao.ItemResDao;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.VariableResDao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author deniger
 */
public class ModelesDao extends CommonResDao.NbrMotDao implements ResContainerEMHCat {

  public static class ModeleDao extends ItemResDao {
  }

  public static class ModeleRegulDao extends CommonResDao.TypeEMHDao implements ResContainerEMHType {

    List<VariableResDao> VariableRes;
    List<CommonResDao.VariableResQregulDao> VariableResQregul;
    List<CommonResDao.VariableResZregulDao> VariableResZregul;
    List<ModeleDao> Modele;

    @Override
    public List<VariableResDao> getVariableRes() {
      List<VariableResDao> res = new ArrayList<>();
      if (VariableRes != null) {
        res.addAll(VariableRes);
      }
      if (VariableResQregul != null) {
        res.addAll(VariableResQregul);
      }
      if (VariableResZregul != null) {
        res.addAll(VariableResZregul);
      }

      return res;
    }

    @Override
    public List<? extends ItemResDao> getItemRes() {
      return Modele ;
    }
  }

  ModeleRegulDao ModeleRegul;

  List<ResContainerEMHType> resContainerEMHType;


  public ModeleRegulDao getModeleRegul() {
    return ModeleRegul;
  }

  @Override
  public List<VariableResDao> getVariableRes() {
    return null;
  }

  @Override
  public List<ResContainerEMHType> getResContainerEMHType() {
    if (resContainerEMHType == null) {
      //attention, l'ordre est très important: doit suivre celui du fichier
      resContainerEMHType = Collections.singletonList(ModeleRegul);
    }
    return resContainerEMHType;
  }

  @Override
  public String getDelimiteurNom() {
    return "CatEMHModele";
  }



}
