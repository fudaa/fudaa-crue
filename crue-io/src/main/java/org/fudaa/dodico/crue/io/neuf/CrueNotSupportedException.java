/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

/**
 * Exception spécifique au format crue non supporté. Est utilisé pour notifier d'un non support de certains type de
 * Carte Crue 9 (DC,DH,...). Est également utilisé pour les IO crue 10.
 * 
 * @author Adrien Hadoux
 */
public class CrueNotSupportedException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = -863600387976615102L;
  /**
   * 
   */

  public final static String MSG_CARTE_TYPE_UNKNOWN = "io.global.typeUnknown.error"; //$NON-NLS-1$

  /**
   * Contructeur qui prend en param le message d'erreur.
   * 
   * @param message
   */
  public CrueNotSupportedException(final String message) {
    super(message);
  }

  /**
   * Constructeur par defaut qui utilise le message carte non supportée pour Crue 9.
   * 
   * @param element element
   * @param nbLigne le numero de ligne
   */
  public CrueNotSupportedException(final String element, final int nbLigne) {
    super(CrueNotSupportedException.MSG_CARTE_TYPE_UNKNOWN + element + " ligne:" + nbLigne);
  }

}
