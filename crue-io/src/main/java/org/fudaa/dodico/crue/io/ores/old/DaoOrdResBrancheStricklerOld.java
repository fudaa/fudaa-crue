/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores.old;

public class DaoOrdResBrancheStricklerOld {
  // WARN: l'ordre des champs est important car utilise par l'ecriture/lecture de ORES

  private boolean ddeSplan;
  private boolean ddeVol;

  public final boolean getDdeSplan() {
    return ddeSplan;
  }

  public final boolean getDdeVol() {
    return ddeVol;
  }

  /**
   * @param newDdeSplan
   */
  public final void setDdeSplan(boolean newDdeSplan) {
    ddeSplan = newDdeSplan;
  }

  /**
   * @param newDdeVol
   */
  public final void setDdeVol(boolean newDdeVol) {
    ddeVol = newDdeVol;
  }

  @Override
  public String toString() {
    return "OrdResBrancheStrickler [ddeSplan=" + ddeSplan + ", ddeVol=" + ddeVol + "]";
  }
}
