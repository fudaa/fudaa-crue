/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import com.thoughtworks.xstream.converters.SingleValueConverter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluNumberFormatFortran;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.comparaison.CrueComparatorHelper;
import org.fudaa.dodico.crue.comparaison.tester.EqualsTester;
import org.fudaa.dodico.crue.comparaison.tester.FactoryEqualsTester;
import org.fudaa.dodico.crue.comparaison.tester.TesterHelper;
import org.fudaa.dodico.crue.config.ccm.Crue9DecimalFormatter;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.config.ccm.SeveriteManager;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.io.common.EnumsConverter;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.comparator.RegleTypeComparator;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrt;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtBrancheBarrageFilEau;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtBrancheBarrageGenerique;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtBrancheNiveauxAssocies;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtBrancheOrifice;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtBranchePdc;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtBrancheSeuilLateral;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtBrancheSeuilTransversal;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtCasierProfil;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.DonFrtStrickler;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeo;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoBatiCasier;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilCasier;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoSectionIdem;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheBarrageFilEau;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheBarrageGenerique;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheNiveauxAssocies;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheOrifice;
import org.fudaa.dodico.crue.metier.emh.EMHBranchePdc;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheSeuilLateral;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheSeuilTransversal;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheStrickler;
import org.fudaa.dodico.crue.metier.emh.EMHSectionIdem;
import org.fudaa.dodico.crue.metier.emh.EMHSectionInterpolee;
import org.fudaa.dodico.crue.metier.emh.EMHSectionProfil;
import org.fudaa.dodico.crue.metier.emh.ElemOrifice;
import org.fudaa.dodico.crue.metier.emh.ElemBarrage;
import org.fudaa.dodico.crue.metier.emh.ElemSeuilAvecPdc;
import org.fudaa.dodico.crue.metier.emh.EnumFormulePdc;
import org.fudaa.dodico.crue.metier.emh.EnumRegle;
import org.fudaa.dodico.crue.metier.emh.EnumSensOrifice;
import org.fudaa.dodico.crue.metier.emh.LitNumerote;
import org.fudaa.dodico.crue.metier.emh.LitUtile;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;
import org.fudaa.dodico.crue.metier.emh.PtProfil;
import org.fudaa.dodico.crue.metier.emh.Regle;
import org.fudaa.dodico.crue.metier.emh.RelationEMH;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.emh.ValParamEntier;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.dodico.fortran.FortranWriter;

/**
 * Writer de la structure DC.
 *
 * @author Adrien Hadoux
 */
public class DCFileWriter extends AbstractCrue9Writer implements CtuluActivity {

  /**
   * taille colonne contenant les XZ
   */
  private static final int XZ_COLUMNS_LENGTH = 10;
  private static final int[] FMT_XZ = new int[]{XZ_COLUMNS_LENGTH, XZ_COLUMNS_LENGTH, XZ_COLUMNS_LENGTH, XZ_COLUMNS_LENGTH, XZ_COLUMNS_LENGTH, XZ_COLUMNS_LENGTH, XZ_COLUMNS_LENGTH};
  /**
   *
   */
  private static final int[] FMT_FRT = new int[]{10, 30, 10, 10, 10, 10};
  private SingleValueConverter enumFormulePdcMapCrue9;
  /**
   * Correspondance EnumSensOrifice<->String
   */
  private SingleValueConverter enumSensOrificeMapCrue9;

  /**
   * Formatteur utilisé pour écrire des colonnes float de taille fixe de x caractères
   */
  final CtuluNumberFormatFortran formatterFF = new CtuluNumberFormatFortran(XZ_COLUMNS_LENGTH - 1);
  Crue9DecimalFormatter formatterXtAndZ;

  @Override
  protected void internalWrite(final CrueIOResu o) {
    enumSensOrificeMapCrue9 = EnumsConverter.createCrue9EnumConverter(EnumSensOrifice.class, analyze_);
    enumFormulePdcMapCrue9 = EnumsConverter.createCrue9EnumConverter(EnumFormulePdc.class, analyze_);
    initWriter();

    final CrueData data = ((CrueIOResu<CrueData>) o).getMetier();
    formatterXtAndZ = new Crue9DecimalFormatter(data.getCrueConfigMetier().getCrue9DecimalFormat());
    try {
      // cf DParametresSipor.java
      // -- on commence par l'écriture des titres sur 5 lignes --//
      writeHeader((CrueIOResu<CrueData>) o, true);

      // -- on ecrit les regles --//
      writeRules(data);

      // -- on ecrit les noeuds --//
      writeNoeuds(data);

      // -- on ecrit les branches --//
      writeBranches(data);

      // -- on ecrit les frottements --//
      writeFrottements(data);

      // -- on ecrit les profils --//
      writeProfils(data);

      // -- on ecrit les casiers --//
      writeCasier(data);
    } catch (final CrueNotSupportedException e) {
      analyze_.manageException(e);
    } catch (final IOException e) {
      analyze_.manageException(e);
    } finally {
      try {
        // -- obliger de fermer manuellement le flux.
        fortranWriter.close();
      } catch (final IOException e) {
        analyze_.manageException(e);
      }
    }

  }

  /**
   * Ecrit les Noeuds du fichier.
   *
   * @param data
   * @throws IOException
   */
  public void writeNoeuds(final CrueData data) throws IOException {
    writeCom("");
    writeCom(" Definition des Noeuds");

    if (data.getNoeuds() != null) {

      int indiceFortran = 0;
      // -- on prend de la marge en comptant la carte noeud double --//
      fortranWriter.stringField(indiceFortran++, CrueIODico.NOEUD);
      final int init = CrueIODico.NOEUD.length();
      int nbCaractWritten = init;
      for (final CatEMHNoeud n : data.getNoeuds()) {

        if (n.getNom().length() > tailleMaxLine) {
          final String previous = n.getNom();
          analyze_.addErrorFromFile("io.crue9.nametoolong.error", fortranWriter.getLineNumber(), previous);
        }
        final int carToAdd = n.getNom().length() + 1;
        if (nbCaractWritten + carToAdd > tailleMaxLine) {
          nbCaractWritten = init;
          indiceFortran = 0;
          fortranWriter.writeFields();
          fortranWriter.stringField(indiceFortran++, CrueIODico.NOEUD);
        }

        fortranWriter.stringField(indiceFortran++, n.getNom());
        nbCaractWritten += carToAdd;

      }
      fortranWriter.writeFields();

    } else {
      writeCom("VIDE");
    }

    writeCom("");
  }

  /**
   * Ecrit les regles.
   *
   * @author Adrien Hadoux
   * @param metier
   * @throws IOException
   */
  public void writeRules(final CrueData metier) throws IOException {
    writeCom("");
    writeCom(" Configuration generale");
    // -- regles PNUM --//
    if (metier.getPNUM() != null) {
      fortranWriter.stringField(0, CrueIODico.ZREF);
      fortranWriter.stringField(1, CrueIODico.getCrue9Value(metier.getPNUM().getZref(), metier.getProperty("zref")));
      fortranWriter.writeFields();

      fortranWriter.stringField(0, CrueIODico.COEFF);
      fortranWriter.stringField(1, "1.0");//dal
      String theta = null;
      if (metier.getPNUM().getParamNumCalcTrans() != null) {
        theta = CrueIODico.getCrue9Value(metier.getPNUM().getParamNumCalcTrans().getThetaPreissmann(), metier.getProperty("thetaPreissmann"));
      } else {
        final ItemVariable property = metier.getProperty("thetaPreissmann");
        final Number defaultValue = property.getDefaultValue();
        theta = property.getFormatter(DecimalFormatEpsilonEnum.COMPARISON).format(defaultValue);
      }

      fortranWriter.stringField(2, theta);//theta
      //isorti:
      //si optr, optg, ou opti a une verbosite plus verbeuse que info-> 1
      boolean use1 = false;
      if (metier.getOPTR() != null) {
        use1 = metier.getOPTR().getSorties().containsVerbositeStrictlyMoreThan(SeveriteManager.INFO, metier.getCrueConfigMetier());
      }
      if (!use1 && metier.getOPTG() != null) {
        use1 = metier.getOPTG().getSorties().containsVerbositeStrictlyMoreThan(SeveriteManager.INFO, metier.getCrueConfigMetier());
      }
      if (!use1 && metier.getOPTI() != null) {
        use1 = metier.getOPTI().getSorties().containsVerbositeStrictlyMoreThan(SeveriteManager.INFO, metier.getCrueConfigMetier());
      }
      fortranWriter.stringField(3, use1 ? "1" : "0");//isorti
      fortranWriter.writeFields();
    }
    if (metier.getOPTG() != null && metier.getOPTG().getRegles() != null) {
      final List<Regle> rgs = new ArrayList<>(metier.getOPTG().getRegles());
      Collections.sort(rgs, RegleTypeComparator.INSTANCE);
      for (final Regle regle : rgs) {
        writeCom(regle.getType().toString());

        if (EnumRegle.PROF_PLAT.equals(regle.getType())) {
          final double val = regle.getSeuilDetect();
          writeCarteValeurActive(metier.getProperty("regleProfPlat"), CrueIODico.R_PRPLAT, val * 100, regle.isActive());
        } else if (EnumRegle.PENTE_RUPTURE.equals(regle.getType())) {
          final double val = regle.getSeuilDetect();
          writeCarteValeurActive(metier.getProperty("reglePenteRupture"), CrueIODico.R_RUPENT, val, regle.isActive());
        } else if (EnumRegle.DECAL.equals(regle.getType())) {
          final double val = regle.getSeuilDetect();
          writeCarteValeurActive(metier.getProperty("regleDecal"), CrueIODico.R_DECAL, val, regle.isActive());
        } else if (EnumRegle.LARG_SEUIL.equals(regle.getType())) {
          final double val = regle.getSeuilDetect();
          writeCarteValeurActive(metier.getProperty("regleLargSeuil"), CrueIODico.R_SLARGE, val, regle.isActive());
        } else if (EnumRegle.PDX_MAX.equals(regle.getType())) {
          final double val = regle.getSeuilDetect();
          writeCarteValeurActive(metier.getProperty("reglePdxMax"), CrueIODico.R_DXMAX, val, regle.isActive());
        } else if (EnumRegle.VAR_PDX_MAX.equals(regle.getType())) {
          final ValParamEntier val = (ValParamEntier) regle.getValParam();
          fortranWriter.stringField(0, CrueIODico.R_VDXMAX);
          if (regle.isActive()) {
            final int var = val.getValeur();
            fortranWriter.intField(1, var);
          } else {
            fortranWriter.stringField(1, CrueIODico.REGLE_INACTIVE);
          }
          fortranWriter.writeFields();
        } else if (EnumRegle.PENTE_MAX.equals(regle.getType())) {
          final double val = regle.getSeuilDetect();
          writeCarteValeurActive(metier.getProperty("reglePenteMax"), CrueIODico.R_PENMAX, val * 100, regle.isActive());
        } else if (EnumRegle.REB_DEB.equals(regle.getType())) {

          fortranWriter.stringField(0, CrueIODico.R_REBDEB);
          if (!regle.isActive()) {
            fortranWriter.stringField(1, CrueIODico.REGLE_INACTIVE);
          }
          fortranWriter.writeFields();

        }
      }

    }

    writeCom("");
  }

  /**
   * @return
   */
  private int[] createFmtFrottement() {
    // format du fichier : 10 caract de carte + 40 caractères de nom + 2* (15 caracteres de points)
    return FMT_FRT;

  }

  private int[] createFmtXZ() {
    // format du fichier : 10 caract de carte + 6* (10 caracteres de points)
    // ex X/Z -1185.00 92.30 -1180.00 89.30 -1000.00 89.00
    return FMT_XZ;

  }

  /**
   * Ecrit les frottements.
   *
   * @author Adrien Hadoux
   * @param metier
   * @throws IOException
   */
  public void writeFrottements(final CrueData metier) throws IOException, CrueNotSupportedException {

    writeCom("");
    writeCom(" Definition des Frottements");

    if (metier.getFrottements() != null) {

      final PropertyEpsilon eps = metier.getCrueConfigMetier().getConfLoi().get(EnumTypeLoi.LoiZFK).getVarOrdonnee().getEpsilon();

      // STRIREF(o) "nom_strickler" k (nom du strickler et valeur associée)
      for (final DonFrt frt : metier.getFrottements().getListFrt()) {

        String pref = null;
        // pour l'instant,on ne supporte que les
        if (frt instanceof DonFrtStrickler) {
          pref = CrueIODico.STRIREFZ;
          // } else if (frt instanceof StrRefY) {
          // pref = CrueIODico.STRIREFH;
          // } else if (frt instanceof StriRefC) {
          // pref = CrueIODico.STRIREF;
        } else {
          throw new CrueNotSupportedException("Only DonFrtStrickler is supported");
        }
        final DonFrtStrickler stricker = (DonFrtStrickler) frt;
        // si une seule valeur on a un StriRef normal
        final List<PtEvolutionFF> ptEvolutionFF = stricker.getStrickler().getEvolutionFF().getPtEvolutionFF();
        final boolean strirefc = ptEvolutionFF.size() == 1 && eps.isZero(ptEvolutionFF.get(0).getAbscisse());
        if (strirefc) {
          pref = CrueIODico.STRIREF;
        }
        fortranWriter.stringField(0, pref);

        // -- noom en position 1 --//
        if (frt.getNom() == null) {
          fortranWriter.stringField(1, "no_name");
        } else {
          fortranWriter.stringField(1, frt.getNom());
        }

        if (strirefc) {
          // c'est bien y car x=0
          fortranWriter.doubleField(2, ptEvolutionFF.get(0).getOrdonnee());
        } else {
          // STRIREFH(o) "nom_strickler" h1 k1 h2 k2... (nom du strickler, et couples h/k,
          int indiceFortran = 2;
          final List<PtEvolutionFF> frtWithPt = ptEvolutionFF;
          final int nbPt = frtWithPt.size();
          if (nbPt > 0) {
            for (int i = 0; i < nbPt; i++) {
              final double x = frtWithPt.get(i).getAbscisse();
              final double y = frtWithPt.get(i).getOrdonnee();
              if (indiceFortran >= 4) {
                fortranWriter.writeFields(createFmtFrottement());
                fortranWriter.stringField(0, pref);
                // -- noom en position 1 --//
                if (frt.getNom() == null) {
                  fortranWriter.stringField(1, "no_name");
                } else {
                  fortranWriter.stringField(1, frt.getNom());
                }
                indiceFortran = 2;

              }
              fortranWriter.doubleField(indiceFortran++, x);
              fortranWriter.doubleField(indiceFortran++, y);
            }
          }

        }

        fortranWriter.writeFields(createFmtFrottement());
      }

    } else {
      writeCom("VIDE");
    }

    writeCom("");
  }

  /**
   * Ecrit les casiers.
   *
   * @author Adrien Hadoux
   * @param metier
   * @throws IOException
   */
  public void writeCasier(final CrueData metier) throws IOException {
    writeCom("");
    writeCom("Definition des Casiers");
    final EqualsTester knownEqualsTester = new FactoryEqualsTester(metier.getCrueConfigMetier(), -1, -1).getKnownEqualsTester(PtProfil.class);
    if (metier.getCasiers() == null) {
      writeCom("VIDE");
    } else {

      for (final CatEMHCasier casier : metier.getCasiers()) {
        if (casier.getUserActive()) {
          // -- ecriture du nom du Noeud associé au casier --//
          // ex CASIER N6
          fortranWriter.stringField(0, CrueIODico.CASIER_TYPE);
          if (casier.getNoeud() != null) {
            fortranWriter.stringField(1, casier.getNoeud().getNom());
          }
          fortranWriter.writeFields();

          // -- donnees DPTI --//
          // -- liste des données DPTG --//
          final List<DonCalcSansPrt> dcsp = casier.getDCSP();
          if (CollectionUtils.isNotEmpty(dcsp)) {
            for (final DonCalcSansPrt geo : dcsp) {
              if (geo instanceof DonCalcSansPrtCasierProfil) {
                fortranWriter.stringField(0, CrueIODico.RUIS);
                // fortranWriter_.doubleField(1, dpti.getQruis());
                // écrit dans le DH
                fortranWriter.doubleField(1, 1);
                fortranWriter.writeFields();
                break;
              }
            }
          }
          final List<DonPrtGeo> dptgList = casier.getDPTG();
          if (dptgList != null) {
            for (final DonPrtGeo geo : dptgList) {
              if (geo instanceof DonPrtGeoProfilCasier) {
                final DonPrtGeoProfilCasier dptg = (DonPrtGeoProfilCasier) geo;
                // ex PROFCAS 100.00
                fortranWriter.stringField(0, CrueIODico.PROFCAS);
                fortranWriter.doubleField(1, dptg.getDistance());
                fortranWriter.writeFields();

                // -- lit Utile remplissage: pour LIMITX --//
                if (dptg.getLitUtile() != null) {
                  final LitUtile lit = dptg.getLitUtile();
                  if (lit.getLimDeb() != null && lit.getLimFin() != null) {
                    fortranWriter.stringField(0, CrueIODico.LIMITEJ);
                    int equalsPtProfil = CrueComparatorHelper.getEqualsPtProfil(lit.getLimDeb(), dptg.getPtProfil(),
                            knownEqualsTester);
                    if (equalsPtProfil < 0) {
                      analyze_.addSevereError("dc.ptProfilLitUtilDeb.NotFound", casier.getNom());
                    }
                    fortranWriter.intField(1, 1 + equalsPtProfil);
                    equalsPtProfil = CrueComparatorHelper.getEqualsPtProfil(lit.getLimFin(), dptg.getPtProfil(),
                            knownEqualsTester);
                    if (equalsPtProfil < 0) {
                      analyze_.addSevereError("dc.ptProfilLitUtilFin.NotFound", casier.getNom());
                    }
                    fortranWriter.intField(2, 1 + equalsPtProfil);
                    fortranWriter.writeFields();
                  }

                }
                // -- liste des couples X/Z --//
                if (dptg.getPtProfil() != null) {
                  writeListePtProfils(CrueIODico.XZ, dptg.getPtProfil());

                }

              }

            }
            final DonPrtGeoBatiCasier batiCasier = EMHHelper.selectFirstOfClass(dptgList, DonPrtGeoBatiCasier.class);
            if (batiCasier != null) {
              fortranWriter.stringField(0, CrueIODico.SBATI);
              // batiCasier.setSplanBati(in_.doubleField(1));
              // batiCasier.setZBatiTotal(in_.doubleField(2));
              fortranWriter.doubleField(1, batiCasier.getSplanBati());
              fortranWriter.doubleField(2, batiCasier.getZBatiTotal());
              fortranWriter.writeFields();

            }

          }
        }
      }
    }

  }

  /**
   * Ecrit les profils.
   *
   * @author Adrien Hadoux
   * @param data
   * @throws IOException
   */
  public void writeProfils(final CrueData metier) throws IOException {
    writeCom("");
    writeCom(" Definition des Profils");
    final EqualsTester knownEqualsTester = new FactoryEqualsTester(metier.getCrueConfigMetier(), -1, -1).getKnownEqualsTester(PtProfil.class);
    if (metier.getSections() != null) {
      for (final CatEMHSection section : metier.getSections()) {

        if (section instanceof EMHSectionProfil || section instanceof EMHSectionInterpolee) {
          final CatEMHSection profil = section;

          fortranWriter.stringField(0, section instanceof EMHSectionInterpolee ? CrueIODico.PROFINT : CrueIODico.PROFIL);
          fortranWriter.stringField(1, profil.getNom());

          fortranWriter.writeFields();
          if (profil.getDPTG() != null) {

            for (final DonPrtGeo geo : profil.getDPTG()) {
              if (geo instanceof DonPrtGeoProfilSection) {
                final DonPrtGeoProfilSection dptg = (DonPrtGeoProfilSection) geo;
                if (dptg.getFente() != null) {
                  fortranWriter.stringField(0, CrueIODico.FENTE);
                  fortranWriter.doubleField(1, dptg.getFente().getLargeurFente());
                  fortranWriter.doubleField(2, dptg.getFente().getProfondeurFente());
                  fortranWriter.writeFields();

                }

                final List<DonFrt> listeFrottements = new ArrayList<>();
                // -- liste des lits, autant de frottements que de lits --//

                // -- listes des indices X/Z qui constitueront les limites --//
                final List<Integer> indiceLimiteJ = new ArrayList<>();
                final List<Integer> listeLitsActifs = new ArrayList<>();
                final HashMap<String, ArrayList<Integer>> mapNomsLits = new HashMap<>();
                // -- etape 1 on recupere les donnees qu'on positionne dans des listes --//
                int indiceLitFortran = 1;
                if (CollectionUtils.isNotEmpty(dptg.getLitNumerote())) {
                  int lastIdx = -1;
                  int idxLit = 0;
                  for (final LitNumerote lit : dptg.getLitNumerote()) {
                    idxLit++;
                    // -- les FROTTEMENTS STRIC --//
                    if (lit.getFrot() != null) {
                      listeFrottements.add(lit.getFrot());
                    }
                    // -- les ACTIFS --//
                    if (lit.getIsLitActif()) {
                      listeLitsActifs.add(indiceLitFortran);
                    }
                    // -- positions deb et fin limJ --//
                    final int positionLimDeb = CrueComparatorHelper.getEqualsPtProfil(lit.getLimDeb(), dptg.getPtProfil(),
                            knownEqualsTester);
                    final int positionLimFin = CrueComparatorHelper.getEqualsPtProfil(lit.getLimFin(), dptg.getPtProfil(),
                            knownEqualsTester);
                    if (positionLimDeb < 0) {
                      analyze_.addSevereError("dc.limDeb.NotFound", section.getNom(), idxLit);
                    }
                    if (positionLimFin < 0) {
                      analyze_.addSevereError("dc.limFin.NotFound", section.getNom(), idxLit);
                    }
                    if (positionLimDeb != -1 && positionLimFin != -1) {
                      final int debIdx = positionLimDeb + 1;
                      if (lastIdx < 0) {
                        indiceLimiteJ.add(debIdx);
                      } else if (debIdx != lastIdx) {
                        analyze_.addError("Les lits numerotes ne sont pas dans l'ordre pour " + profil.getNom());
                      }
                      lastIdx = positionLimFin + 1;
                      indiceLimiteJ.add(lastIdx);

                    }

                    // -- les noms des lits nommees --//
                    if (lit.getNomLit() != null && lit.getNomLit().getNom() != null) {
                      final String nom = lit.getNomLit().getNom();
                      // -- cas particulier pour les lits isMineur=true: ces lit doivent etre renomes en MINEUR--//
                      // vu avec Pierre et Jean-Marc: on ne renomme pas:
                      // if (lit.getIsLitMineur()) {
                      // nom = "Lt_MINEUR";
                      // }
                      ArrayList<Integer> listeEntierAppartientLit = mapNomsLits.get(nom);
                      if (listeEntierAppartientLit == null) {
                        listeEntierAppartientLit = new ArrayList<>();

                        mapNomsLits.put(nom, listeEntierAppartientLit);
                      }
                      // -- on ajoute le lit nommé comme appartenant a ce nom. --//
                      listeEntierAppartientLit.add(indiceLitFortran);
                    }

                    indiceLitFortran++;
                  }
                }

                // -- on ecrit les données --//
                // -- LIMITEJ--//
                // LIMITEJ 11 11 13 18 64 81 81 81
                fortranWriter.stringField(0, CrueIODico.LIMITEJ);
                int cpt = 1;
                for (final int val : indiceLimiteJ) {
                  if (cpt >= 9) {
                    fortranWriter.writeFields();
                    fortranWriter.stringField(0, CrueIODico.LIMITEJ);
                    cpt = 1;
                  }
                  fortranWriter.intField(cpt++, val);
                }
                fortranWriter.writeFields();

                // -- les SDTRICKLERS --//
                // STRIC K0 P137.100MAJ P137.100BMAJ P137.100MIN P137.100MIN P137.100MAJ K0
                fortranWriter.stringField(0, CrueIODico.STRIC);
                cpt = 1;
                final int initTaille = CrueIODico.STRIC.length();
                int tailleCourante = initTaille;
                for (final DonFrt frt : listeFrottements) {

                  final int carToAdd = frt.getNom().length() + 1;
                  if (tailleCourante + carToAdd >= tailleMaxLine) {
                    tailleCourante = initTaille;
                    fortranWriter.writeFields();
                    fortranWriter.stringField(0, CrueIODico.STRIC);
                    cpt = 1;
                  }
                  fortranWriter.stringField(cpt++, frt.getNom());
                  tailleCourante += carToAdd;

                }
                fortranWriter.writeFields();

                // -- liste des couples X/Z --//
                // X/Z -1185.00 92.30 -1180.00 89.30 -1000.00 89.00
                if (dptg.getPtProfil() != null) {
                  writeListePtProfils(CrueIODico.XZ, dptg.getPtProfil());

                }

                // -- ACTIF --//
                // ACTIF 2 3 4 5 6
                fortranWriter.stringField(0, CrueIODico.ACTIF);
                cpt = 1;
                for (final int valActif : listeLitsActifs) {
                  if (cpt > 20) {
                    fortranWriter.writeFields();
                    cpt = 1;
                    fortranWriter.stringField(0, CrueIODico.ACTIF);
                  }

                  fortranWriter.intField(cpt++, valActif);
                }
                fortranWriter.writeFields();

                // -- on affiche les lits et les litNumerotes concernes par le nom grace a la map--//
                // LIT MAJD 2 3
                // LIT MINEUR 4 5
                // LIT MAJG 6
                // LIT STOCKD 1
                // LIT STOCKG 7
                final List<String> keySet = new ArrayList<>(mapNomsLits.keySet());
                Collections.sort(keySet);
                for (final String key : keySet) {
                  cpt = 0;
                  fortranWriter.stringField(cpt++, CrueIODico.LIT);
                  fortranWriter.stringField(cpt++, key);
                  final ArrayList<Integer> res = mapNomsLits.get(key);
                  if (res != null) {
                    for (final int i : res) {
                      fortranWriter.intField(cpt++, i);
                    }

                    fortranWriter.writeFields();
                  }

                }

              }// fin if dptg profil
            }// fin du for dptg

          }

          writeCom("");
        } else if (section instanceof EMHSectionIdem) {

          // -- section avec reference vers une autre section et une donnee DPTG avec un deltaZ changeant --//
          // ex PROFIDEM(o) "nom_profil" "nom_prof_appelé" [deltaz]
          final EMHSectionIdem idem = (EMHSectionIdem) section;
          if (idem.getSectionRef() != null) {
            final String ref = idem.getSectionRef().getNom();
            final String nom = idem.getNom();

            if (idem.getDPTG() != null) {
              for (final DonPrtGeo geo : idem.getDPTG()) {
                if (geo instanceof DonPrtGeoSectionIdem) {
                  final double delta = ((DonPrtGeoSectionIdem) geo).getDz();
                  fortranWriter.stringField(0, CrueIODico.PROFIDEM);
                  fortranWriter.stringField(1, nom);
                  fortranWriter.stringField(2, ref);
                  fortranWriter.doubleField(3, delta);
                  fortranWriter.writeFields();

                }
              }
            }
          }
        }

      }

    } else {

      writeCom("VIDE");
    }

  }

  /**
   * Ecrit les cartes BRANCHE_PROF - BRANCHE_DISTANCE - BRANCHE_CPOND - BRANCHE_CDIV - BRANCHE_CCONV
   *
   * @param list
   * @throws IOException
   */
  private void writeCartesProfilsDISTANCE(final List<RelationEMHSectionDansBranche> list,
          final String carteProfilToUse, final boolean useDistance, final boolean useCPOND, final boolean useCDIV,
          final boolean useCCONV) throws IOException {
    final int size = list.size();
    final List<Double> relationDistance = new ArrayList<>(size);
    final List<Double> relationCoefPond = new ArrayList<>(size);
    final List<Double> relationCoefdiv = new ArrayList<>(size);
    final List<Double> relationCoefConv = new ArrayList<>(size);
    final List<String> relationNomProfils = new ArrayList<>(size);
    double previousPositionSections = 0;
    for (int i = 0; i < size; i++) {
      final RelationEMHSectionDansBranche relation = list.get(i);
      final double xp = relation.getXp();
      // -- on calcul le delta distance car : dans Crue 10: ce sont les positions, dans Crue 9 ce sont les distance
      // inter sections.
      final double deltaDistance = Math.abs(xp - previousPositionSections);
      previousPositionSections = xp;
      relationNomProfils.add(relation.getEmh().getNom());
      // on ajoute pas le premier delta: crue 9 attent des distance inter profil ( le premier xp vaut 0)
      if (i > 0) {
        relationDistance.add(deltaDistance);
//      }
//      // on ajoute pas les dernier coef.
        double coefPond = 0.5;
        double coefConv = 0;
        double coefDiv = 0;
        if (relation.getClass().equals(RelationEMHSectionDansBrancheSaintVenant.class)) {
          final RelationEMHSectionDansBrancheSaintVenant stVenant = (RelationEMHSectionDansBrancheSaintVenant) relation;
          coefPond = stVenant.getCoefPond();
          coefDiv = stVenant.getCoefDiv();
          coefConv = stVenant.getCoefConv();
        }
        relationCoefPond.add(coefPond);
        relationCoefdiv.add(coefDiv);
        relationCoefConv.add(coefConv);
      }

    }

    // -- on ecrit les listes dans l'ordre PROF-DISTANCE-CPOND-CDIV-CCONV en passant à la ligne si necessaire.--//
    // int indiceFortran;
    // int lenghtTotal;
    writeString(carteProfilToUse, relationNomProfils);

    if (useDistance) {
      write(CrueIODico.BRANCHE_DISTANCE, relationDistance);
    }
    if (useCPOND) {
      write(CrueIODico.BRANCHE_CPOND, relationCoefPond);
    }

    if (useCDIV) {
      write(CrueIODico.BRANCHE_CDIV, relationCoefdiv);
    }

    if (useCCONV) {
      write(CrueIODico.BRANCHE_CCONV, relationCoefConv);
    }
  }

  private void write(final String carte, final List<Double> relationDistance) throws IOException {
    int indiceFortran = 0;
    fortranWriter.stringField(indiceFortran++, carte);
    final int initLength = carte.length();
    int lenghtTotal = initLength;
    for (final Double distance : relationDistance) {
      final String doubleStr = distance.toString();
      final int carToAdd = doubleStr.length() + 1;
      if (lenghtTotal + carToAdd >= tailleMaxLine) {
        fortranWriter.writeFields();
        indiceFortran = 0;
        fortranWriter.stringField(indiceFortran++, carte);
        lenghtTotal = initLength;
      }
      fortranWriter.stringField(indiceFortran++, doubleStr);
      lenghtTotal += carToAdd;
    }
    fortranWriter.writeFields();
  }

  private void writeString(final String carte, final List<String> relationNomProfils) throws IOException {
    int indiceFortran = 0;
    fortranWriter.stringField(indiceFortran++, carte);
    final int initLength = carte.length();
    int lenghtTotal = initLength;
    for (final String nomProfil : relationNomProfils) {
      final int tailleToAdd = 1 + nomProfil.length();
      // si la taille total + espace+ le nom profil depasse on revient à la ligne
      if (lenghtTotal + tailleToAdd >= tailleMaxLine) {
        fortranWriter.writeFields();
        indiceFortran = 0;
        fortranWriter.stringField(indiceFortran++, carte);
        lenghtTotal = initLength;
      }
      fortranWriter.stringField(indiceFortran++, nomProfil);
      lenghtTotal += tailleToAdd;
    }
    fortranWriter.writeFields();
  }

  /**
   * Ecrit les listes de points DZQ ou ZQ pour les lois en passant a la ligne qand besoin est.
   *
   * @param carteToUse
   * @param liste
   * @throws IOException
   */
  public void writeListeEvolutionFF(final String carteToUse, final List<PtEvolutionFF> liste) throws IOException {
    // --on ne depasse 3 couples --//
    final int nbMaxCouples = 3;
    int cptCouples = 0;
    int indiceFortran = 0;
    fortranWriter.stringField(indiceFortran++, carteToUse);

    for (final PtEvolutionFF pt : liste) {
      if (cptCouples >= nbMaxCouples) {
        indiceFortran = 0;
        cptCouples = 0;
        fortranWriter.writeFields(createFmtXZ());
        fortranWriter.stringField(indiceFortran++, carteToUse);
      }
      // on inverse tout le temps dans dc
      fortranWriter.stringField(indiceFortran++, FortranWriter.addSpacesBefore(XZ_COLUMNS_LENGTH, formatterFF.format(pt.getOrdonnee())));
      fortranWriter.stringField(indiceFortran++, FortranWriter.addSpacesBefore(XZ_COLUMNS_LENGTH, formatterFF.format(pt.getAbscisse())));
      cptCouples++;

    }
    fortranWriter.writeFields(createFmtXZ());
  }

  /**
   * Ecrit les couples X/Z en passant autant de lignes que necessqire.
   *
   * @param carteToUse
   * @param liste
   * @throws IOException
   */
  public void writeListePtProfils(final String carteToUse, final List<PtProfil> liste) throws IOException {
    // --on ne depasse 3 couples --//
    final int nbMaxCouples = 3;
    int cptCouples = 0;
    int indiceFortran = 0;
    fortranWriter.stringField(indiceFortran++, carteToUse);

    final int[] createFmtXZ = createFmtXZ();
    for (final PtProfil pt : liste) {

      fortranWriter.stringField(indiceFortran++, fortranWriter.addSpacesBefore(XZ_COLUMNS_LENGTH, formatterXtAndZ.format(pt.getXt())));
      fortranWriter.stringField(indiceFortran++, fortranWriter.addSpacesBefore(XZ_COLUMNS_LENGTH, formatterXtAndZ.format(pt.getZ())));
      cptCouples++;
      if (cptCouples >= nbMaxCouples) {
        indiceFortran = 0;
        cptCouples = 0;
        fortranWriter.writeFields(createFmtXZ);
        fortranWriter.stringField(indiceFortran++, carteToUse);
      }
    }
    fortranWriter.writeFields(createFmtXZ);
  }

  /**
   * retourne l'indice correspondant au type de branche.
   *
   * @param branche
   * @return
   */
  private int getCorrespondantNumberForBranche(final CatEMHBranche branche) {

    if (branche instanceof EMHBrancheSaintVenant) {
      return CrueIODico.BRANCHE_SAINT_VENANT;
    }
    if (branche instanceof EMHBrancheSeuilTransversal) {
      return CrueIODico.BRANCHE_SEUIL_TRANSVERSAL;
    }
    if (branche instanceof EMHBrancheSeuilLateral) {
      return CrueIODico.BRANCHE_SEUIL_LONGITUDINALE;
    }
    if (branche instanceof EMHBranchePdc) {
      return CrueIODico.BRANCHE_PDC;
    }
    if (branche instanceof EMHBrancheStrickler) {
      return CrueIODico.BRANCHE_STRICKLER;
    }
    if (branche instanceof EMHBrancheOrifice) {
      return CrueIODico.BRANCHE_ORIFICE;
    }
    if (branche instanceof EMHBrancheBarrageGenerique) {
      return CrueIODico.BRANCHE_BARRAGE_GENERIQUE;
    }
    if (branche instanceof EMHBrancheNiveauxAssocies) {
      return CrueIODico.BRANCHE_NIVEAU_ASSOCIE;
    }
    if (branche instanceof EMHBrancheBarrageFilEau) {
      return CrueIODico.BRANCHE_BARRAGE_FIL_EAU;
    }

    return -1;

  }

  /**
   * cree la branche specifique.
   *
   * @param typeBranche
   * @param metier
   * @param branche
   */
  private void writeSpecificBranche(final int typeBranche, final CrueData metier, final CatEMHBranche branche)
          throws IOException {
    switch (typeBranche) {

      case CrueIODico.BRANCHE_SAINT_VENANT:
        writeBrancheSaintVenant(metier, (EMHBrancheSaintVenant) branche);
        break;
      case CrueIODico.BRANCHE_SEUIL_TRANSVERSAL:
        writeBrancheSeuilTransversal(metier, (EMHBrancheSeuilTransversal) branche);
        break;
      case CrueIODico.BRANCHE_SEUIL_LONGITUDINALE:
        writeBrancheSeuilLongitudinale(metier, (EMHBrancheSeuilLateral) branche);
        break;
      case CrueIODico.BRANCHE_PDC:
        writeBranchePdc(metier, (EMHBranchePdc) branche);
        break;
      case CrueIODico.BRANCHE_STRICKLER:
        writeBrancheStrickler(metier, (EMHBrancheStrickler) branche);
        break;
      case CrueIODico.BRANCHE_ORIFICE:
        writeBrancheOrifice(metier, (EMHBrancheOrifice) branche);
        break;
      case CrueIODico.BRANCHE_BARRAGE_GENERIQUE:
        writeBrancheGenerique(metier, (EMHBrancheBarrageGenerique) branche);
        break;
      case CrueIODico.BRANCHE_NIVEAU_ASSOCIE:
        writeBrancheNiveauAssocie(metier, (EMHBrancheNiveauxAssocies) branche);
        break;
      case CrueIODico.BRANCHE_BARRAGE_FIL_EAU:
        writeBrancheBarrageFilEau(metier, (EMHBrancheBarrageFilEau) branche);
        break;

    }
  }

  /**
   * Ecrit le contenu specifique de la branche stvenant.
   *
   * @param data
   * @param branche
   */
  public void writeBrancheSaintVenant(final CrueData data, final EMHBrancheSaintVenant branche) throws IOException {
    // -- on s'occupe de créer les lignes distance et PROF pour chaque section profil existante: --//
    final List<RelationEMHSectionDansBranche> listeSections = branche.getListeSectionsSortedXP(data.getCrueConfigMetier());
    writeCartesProfilsDISTANCE(listeSections, CrueIODico.BRANCHE_PROF, true, true, true, true);

    // WARNING DISTMAX pas a prendre en compte dans V0.
    // -- ecriture de RUIS --//
    if (branche.getDCSP() != null) {
      for (final DonCalcSansPrt dcsp : branche.getDCSP()) {
        if (dcsp instanceof DonCalcSansPrtBrancheSaintVenant) {
          fortranWriter.stringField(0, CrueIODico.BRANCHE_RUIS);
          // on met toujours 1 dans le dc, c'est le dh qui se chargera de spécifier les coef.
          fortranWriter.doubleField(1, 1);
          fortranWriter.writeFields();
        }
      }
    }

    // -- ecriture des coeffs sinuo --//
    if (branche.getDPTG() != null) {

      for (final DonPrtGeo geo : branche.getDPTG()) {
        if (geo instanceof DonPrtGeoBrancheSaintVenant) {
          final DonPrtGeoBrancheSaintVenant geoStVenant = (DonPrtGeoBrancheSaintVenant) geo;

          // ex SINUO 600.0 500.0 (si LongMineur = 600 m et LongMajeur = 500 m) :
          final double coeff = geoStVenant.getCoefSinuo();// longMineur / longMajeur;
          final double max = listeSections.get(listeSections.size() - 1).getXp();

          writeCom("Coeff SINUO DPTG CRUE 10: " + coeff
                  + ". Ce coef vaut 'longeur Lit Mineur/Longueur Lit Majeur' avec Longueur Mineur= longueur branche");
          final double longMineur = max;
          final double longMajeur = max / coeff;
          fortranWriter.stringField(0, CrueIODico.BRANCHE_SINUO);
          fortranWriter.doubleField(1, longMineur);
          fortranWriter.doubleField(2, longMajeur);
          fortranWriter.writeFields();

        }
      }

    }

  }

  /**
   * Ecrit le contenu specifique de la branche writeBrancheSeuilTransversal.
   *
   * @param data
   * @param branche
   */
  public void writeBrancheSeuilTransversal(final CrueData data, final EMHBrancheSeuilTransversal branche)
          throws IOException {

    if (branche.getDCSP() != null) {
      for (final DonCalcSansPrt dcsp : branche.getDCSP()) {
        if (dcsp instanceof DonCalcSansPrtBrancheSeuilTransversal) {
          final DonCalcSansPrtBrancheSeuilTransversal dcspSeuil = (DonCalcSansPrtBrancheSeuilTransversal) dcsp;

          final String valueBorda = enumFormulePdcMapCrue9.toString(dcspSeuil.getFormulePdc());
          fortranWriter.stringField(0, CrueIODico.BRANCHE_BORDA);
          fortranWriter.stringField(1, valueBorda);
          fortranWriter.writeFields();

          // -- ecriture des seuils si il y a --//
          // ex: SEUIL 50.000 86.600 1.000
          if (dcspSeuil.getElemSeuilAvecPdc() != null) {

            for (final ElemSeuilAvecPdc seuil : dcspSeuil.getElemSeuilAvecPdc()) {
              fortranWriter.stringField(0, CrueIODico.BRANCHE_SEUIL);
              fortranWriter.doubleField(1, seuil.getLargeur());
              fortranWriter.doubleField(2, seuil.getZseuil());
              fortranWriter.doubleField(3, seuil.getCoefD());
              fortranWriter.doubleField(4, seuil.getCoefPdc());
              fortranWriter.writeFields();

            }
          }
        }

      }

    }

    // -- ecriture des profils --//
    if (branche.isRelationsEMHNotEmpty()) {

      boolean exist = false;
      int indiceFortran = 0;
      int tailleTotale = 10;
      fortranWriter.stringField(indiceFortran++, CrueIODico.BRANCHE_PROF);
      for (final RelationEMH relation : branche.getRelationEMH()) {

        if (relation.getEmh() != null && relation.getEmh() instanceof CatEMHSection) {
          exist = true;
          if (tailleTotale + 10 >= tailleMaxLine) {
            fortranWriter.writeFields();
            tailleTotale = 10;
            indiceFortran = 0;
            fortranWriter.stringField(indiceFortran++, CrueIODico.BRANCHE_PROF);
          }
          fortranWriter.stringField(indiceFortran++, relation.getEmh().getNom());
          tailleTotale += 10;

        }

      }
      if (exist) {
        fortranWriter.writeFields();
      }
    }

  }

  /**
   * Ecrit le contenu specifique de la branche SeuilLongitudinale.
   *
   * @param data
   * @param branche
   */
  public void writeBrancheSeuilLongitudinale(final CrueData data, final EMHBrancheSeuilLateral branche)
          throws IOException {
    // -- ecriture des coeffs sinuo --//
    if (branche.getDCSP() != null) {
      for (final DonCalcSansPrt dcsp : branche.getDCSP()) {
        if (dcsp instanceof DonCalcSansPrtBrancheSeuilLateral) {
          final DonCalcSansPrtBrancheSeuilLateral dcspSeuil = (DonCalcSansPrtBrancheSeuilLateral) dcsp;

          if (dcspSeuil.getFormulePdc() != null) {
            final String valueBorda = enumFormulePdcMapCrue9.toString(dcspSeuil.getFormulePdc());
            fortranWriter.stringField(0, CrueIODico.BRANCHE_BORDA);
            fortranWriter.stringField(1, valueBorda);
            fortranWriter.writeFields();
          }

          // -- ecriture des seuils si il y a --//
          // ex: SEUIL 50.000 86.600 1.000
          if (dcspSeuil.getElemSeuilAvecPdc() != null) {
            for (final ElemSeuilAvecPdc seuil : dcspSeuil.getElemSeuilAvecPdc()) {
              fortranWriter.stringField(0, CrueIODico.BRANCHE_SEUIL);
              fortranWriter.doubleField(1, seuil.getLargeur());
              fortranWriter.doubleField(2, seuil.getZseuil());
              fortranWriter.doubleField(3, seuil.getCoefD());
              fortranWriter.doubleField(4, seuil.getCoefPdc());
              fortranWriter.writeFields();

            }
          }
        }

      }

    }
  }

  /**
   * Ecrit le contenu specifique de la branche Pdc 1.
   *
   * @param data
   * @param branche
   */
  public void writeBranchePdc(final CrueData data, final EMHBranchePdc branche) throws IOException {

    if (branche.getDCSP() != null) {

      for (final DonCalcSansPrt don : branche.getDCSP()) {

        if (don instanceof DonCalcSansPrtBranchePdc) {
          final DonCalcSansPrtBranchePdc dcsp = (DonCalcSansPrtBranchePdc) don;
          if (LoiHelper.isNotEmpty(dcsp.getPdc())) {
            writeListeEvolutionFF(CrueIODico.BRANCHE_DZQ, dcsp.getPdc().getEvolutionFF().getPtEvolutionFF());
          }
        }

      }

    }

  }

  /**
   * Ecrit le contenu specifique de la branche Strickler.
   *
   * @param data
   * @param branche
   */
  public void writeBrancheStrickler(final CrueData data, final EMHBrancheStrickler branche) throws IOException {
    // -- on s'occupe de créer les lignes distance et PROF uniquement pour chaque section existante: --//
    writeCartesProfilsDISTANCE(branche.getListeSectionsSortedXP(data.getCrueConfigMetier()),
            CrueIODico.BRANCHE_PROF, true, false, false, false);
  }

  /**
   * Ecrit le contenu specifique de la branche Orifice 5.
   *
   * @param data
   * @param branche
   */
  public void writeBrancheOrifice(final CrueData data, final EMHBrancheOrifice branche) throws IOException {
    if (branche.getDCSP() != null) {

      for (final DonCalcSansPrt don : branche.getDCSP()) {

        if (don instanceof DonCalcSansPrtBrancheOrifice) {
          final DonCalcSansPrtBrancheOrifice dcsp = (DonCalcSansPrtBrancheOrifice) don;
          final ElemOrifice orifice = dcsp.getElemOrifice();
          if (orifice != null) {

            // CLAPET
            fortranWriter.stringField(0, CrueIODico.BRANCHE_CLAPET);

            // ex CLAPET 3.00 1.50 20.0 0.90 0
            fortranWriter.doubleField(1, orifice.getZseuil());
            fortranWriter.doubleField(2, orifice.getHaut());
            fortranWriter.doubleField(3, orifice.getLargeur());
            fortranWriter.doubleField(4, orifice.getCoefD());
            final String sens = enumSensOrificeMapCrue9.toString(orifice.getSensOrifice());
            fortranWriter.stringField(5, sens);
            fortranWriter.writeFields();
            // BRANCHE_CCTRMAX
            fortranWriter.stringField(0, CrueIODico.BRANCHE_CCTRMAX);
            fortranWriter.doubleField(1, orifice.getCoefCtrLim());
            fortranWriter.writeFields();

          }
          // }

        }
      }
    }

  }

  /**
   * Ecrit le contenu specifique de la branche Generique 14.
   *
   * @param data
   * @param branche
   */
  public void writeBrancheGenerique(final CrueData data, final EMHBrancheBarrageGenerique branche) throws IOException {

    if (branche.getDCSP() != null) {

      for (final DonCalcSansPrt don : branche.getDCSP()) {

        if (don instanceof DonCalcSansPrtBrancheBarrageGenerique) {
          final DonCalcSansPrtBrancheBarrageGenerique dcsp = (DonCalcSansPrtBrancheBarrageGenerique) don;

          // -- limites --//
          fortranWriter.stringField(0, CrueIODico.BRANCHE_QMIN_QMAX);
          fortranWriter.doubleField(1, dcsp.getQLimInf());
          fortranWriter.doubleField(2, dcsp.getQLimSup());
          fortranWriter.writeFields();
          // -- on ecrit les profils de reference et on ecrit pas les distance et autres infos, si on en a --//
          if (branche.getSectionPilote() != null) {
            fortranWriter.stringField(0, CrueIODico.BRANCHE_NOMREF);
            fortranWriter.stringField(1, branche.getSectionPilote().getNom());
            fortranWriter.writeFields();
          }
          // --denoye--//
          // ex: Z/Q(o) Z1 Q1 Z2 Q2… pour Denoye
          if (LoiHelper.isNotEmpty(dcsp.getRegimeDenoye())) {
            writeListeEvolutionFF(CrueIODico.BRANCHE_ZQ, dcsp.getRegimeDenoye().getEvolutionFF().getPtEvolutionFF());

          }

          // --noye --//
          // ex: DZ/Q(o) dZ1 Q1 dZ2 Q2… pour Noye
          if (LoiHelper.isNotEmpty(dcsp.getRegimeNoye())) {
            writeListeEvolutionFF(CrueIODico.BRANCHE_DZQ, dcsp.getRegimeNoye().getEvolutionFF().getPtEvolutionFF());
          }

        }
      }
    }

  }

  /**
   * Ecrit le contenu specifique de la branche NiveauAssocie.
   *
   * @param data
   * @param branche
   */
  public void writeBrancheNiveauAssocie(final CrueData data, final EMHBrancheNiveauxAssocies branche)
          throws IOException {
    if (branche.getDCSP() != null) {

      for (final DonCalcSansPrt don : branche.getDCSP()) {

        if (don instanceof DonCalcSansPrtBrancheNiveauxAssocies) {
          final DonCalcSansPrtBrancheNiveauxAssocies dcsp = (DonCalcSansPrtBrancheNiveauxAssocies) don;

          // -- limites --//
          fortranWriter.stringField(0, CrueIODico.BRANCHE_QMIN_QMAX);
          fortranWriter.doubleField(1, dcsp.getQLimInf());
          fortranWriter.doubleField(2, dcsp.getQLimSup());
          fortranWriter.writeFields();

          // --denoye--//
          // ex: ZAM/ZAV(o) zam1 zav1 zam2 zav2
          if (dcsp.getZasso() != null && dcsp.getZasso().getEvolutionFF() != null
                  && dcsp.getZasso().getEvolutionFF().getPtEvolutionFF() != null) {
            writeListeEvolutionFF(CrueIODico.BRANCHE_ZAMZAV, dcsp.getZasso().getEvolutionFF().getPtEvolutionFF());

          }

        }
      }
    }
  }

  /**
   * Ecrit le contenu specifique de la branche BarrageFilEau 15.
   *
   * @param data
   * @param branche
   * @throws IOException
   */
  public void writeBrancheBarrageFilEau(final CrueData data, final EMHBrancheBarrageFilEau branche) throws IOException {
    if (branche.getDCSP() != null) {

      for (final DonCalcSansPrt don : branche.getDCSP()) {

        if (don instanceof DonCalcSansPrtBrancheBarrageFilEau) {
          final DonCalcSansPrtBrancheBarrageFilEau dcsp = (DonCalcSansPrtBrancheBarrageFilEau) don;

          // -- limites --//
          fortranWriter.stringField(0, CrueIODico.BRANCHE_QMIN_QMAX);
          fortranWriter.doubleField(1, dcsp.getQLimInf());
          fortranWriter.doubleField(2, dcsp.getQLimSup());
          fortranWriter.writeFields();

          // -- on ecrit les profils de reference et on ecrit pas les distance et autres infos, si on en a --//
          if (branche.getSectionPilote() != null) {
            fortranWriter.stringField(0, CrueIODico.BRANCHE_NOMREF);
            fortranWriter.stringField(1, branche.getSectionPilote().getNom());
            fortranWriter.writeFields();
          }
          // en crue 9, on a un seul elemSeuil a ecrire dans la carte param.
          final Collection<ElemBarrage> lstElemSeuil = dcsp.getElemBarrage();
          if (CollectionUtils.isNotEmpty(lstElemSeuil)) {
            if (!TesterHelper.isSame(lstElemSeuil, data.getCrueConfigMetier(), -1, -1)) {
              analyze_.addError("io.dc.listSeuil.notConstant", branche.getNom());

            }
            final ElemBarrage elemSeuil = lstElemSeuil.iterator().next();
            fortranWriter.stringField(0, CrueIODico.BRANCHE_PARAM);
            fortranWriter.doubleField(1, elemSeuil.getCoefNoy());
            fortranWriter.intField(2, lstElemSeuil.size());
            fortranWriter.doubleField(3, elemSeuil.getLargeur());
            fortranWriter.doubleField(4, elemSeuil.getZseuil());
            fortranWriter.doubleField(5, 1d);
            fortranWriter.writeFields();
          }
          // --denoye--//
          // ex: Z/Q(o) Z1 Q1 Z2 Q2… pour Denoye
          if (dcsp.getRegimeManoeuvrant() != null && dcsp.getRegimeManoeuvrant().getEvolutionFF() != null
                  && dcsp.getRegimeManoeuvrant().getEvolutionFF().getPtEvolutionFF() != null) {
            writeListeEvolutionFF(CrueIODico.BRANCHE_ZQ, dcsp.getRegimeManoeuvrant().getEvolutionFF().getPtEvolutionFF());

          }

          // -- ecriture des profils --//
          writeCartesProfilsDISTANCE(branche.getListeSectionsSortedXP(data.getCrueConfigMetier()),
                  CrueIODico.BRANCHE_PROF, false, false, false, false);

        }
      }
    }
  }

  /**
   * Ecrit les Branches du fichier.
   *
   * @param data
   * @throws IOException
   */
  public void writeBranches(final CrueData data) throws IOException {
    writeCom("");
    writeCom(" Definition des Branches");

    if (data.getBranches() != null) {

      for (final CatEMHBranche branche : data.getBranches()) {

        // -- on ecrit au format crue 9 que si la branche est active --//
        if (branche.getUserActive()) {

          // -- le type de la branche --//
          int typeBranche = -1;
          // -- recupère le type de la branche --//
          typeBranche = getCorrespondantNumberForBranche(branche);

          if (typeBranche != -1) {

            int indiceFortran = 0;
            writeCom("");
            // -- on ecrit les infos generique de la branche --//
            fortranWriter.stringField(indiceFortran++, CrueIODico.BRANCHE_TYPE);
            fortranWriter.stringField(indiceFortran++, branche.getNom());

            if (branche.getNoeudAmont() != null) {
              fortranWriter.stringField(indiceFortran++, branche.getNoeudAmont().getNom());
            } else {
              analyze_.addErrorFromFile("io.crue9.noeudAmontvide.error", fortranWriter.getLineNumber());
              fortranWriter.stringField(indiceFortran++, "no_node");
            }
            if (branche.getNoeudAval() != null) {
              fortranWriter.stringField(indiceFortran++, branche.getNoeudAval().getNom());
            } else {
              analyze_.addErrorFromFile("io.crue9.noeudAvalvide.error", fortranWriter.getLineNumber());
              fortranWriter.stringField(indiceFortran++, "no_node");
            }

            fortranWriter.intField(indiceFortran++, typeBranche);

            fortranWriter.writeFields();

            // -- ecriture des donnees specifiques branche --//
            writeSpecificBranche(typeBranche, data, branche);

          } else {
            analyze_.addErrorFromFile("io.crue9.brancheNotSupportedInCrue9.error", fortranWriter.getLineNumber());
          }
        }
      }

    } else {
      writeCom("VIDE");
    }

    writeCom("");
  }

  @Override
  public void stop() {
  }
}
