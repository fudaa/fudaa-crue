/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.ocal.CrueConverterOCAL;
import org.fudaa.dodico.crue.io.ocal.CrueDaoStructureOCAL;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.OrdCalcScenario;

public class CrueFileFormatBuilderOCAL implements CrueFileFormatBuilder<OrdCalcScenario> {

  @Override
  public Crue10FileFormatOCAL getFileFormat(final CoeurConfigContrat coeurConfig) {

    final boolean oldVersion = Crue10VersionConfig.V_1_1_1.equals(coeurConfig.getXsdVersion());
    return new Crue10FileFormatOCAL(new CrueDataXmlReaderWriterImpl<>(
            CrueFileType.OCAL, coeurConfig, new CrueConverterOCAL(oldVersion), new CrueDaoStructureOCAL(oldVersion)));

  }

}
