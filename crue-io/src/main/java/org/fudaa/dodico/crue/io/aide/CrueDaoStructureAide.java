/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fudaa.dodico.crue.io.aide;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueDaoStructure;

/**
 *
 * @author landrodie
 */
public class CrueDaoStructureAide implements CrueDaoStructure  {

    @Override
    public void configureXStream(XStream xstream, CtuluLog analyze) {
        xstream.alias("Liens", CrueDaoAide.class);
        xstream.alias("Lien", CrueAide.class);
        xstream.useAttributeFor(CrueAide.class, "IdFudaaCrue");
        xstream.addImplicitCollection(CrueDaoAide.class, "listeAide");
    }
    
}
