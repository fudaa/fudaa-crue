/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.common;

import org.fudaa.ctulu.CtuluLog;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * @author deniger
 */
public final class ErrorHandlerDefault implements ErrorHandler {
  private boolean hasError;
  /**
   * 
   */
  private final CtuluLog res;

  /**
   * @param res
   */
  ErrorHandlerDefault(final CtuluLog res) {
    this.res = res;
  }

  @Override
  public void error(final SAXParseException exception) throws SAXException {
    hasError = true;
    res.addSevereError("io.xml.validator.error", exception.getLineNumber(), exception.getMessage());
  }

  @Override
  public void fatalError(final SAXParseException exception) throws SAXException {
    hasError = true;
    res.addSevereError("io.xml.validator.error", exception.getLineNumber(), exception.getMessage());

  }

  /**
   * @return the hasError
   */
  protected boolean isHasError() {
    return hasError;
  }

  @Override
  public void warning(final SAXParseException exception) throws SAXException {
    res.addWarnFromFile(exception.getMessage(), exception.getLineNumber());

  }
}
