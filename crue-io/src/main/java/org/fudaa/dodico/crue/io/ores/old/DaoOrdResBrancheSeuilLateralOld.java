/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores.old;

public class DaoOrdResBrancheSeuilLateralOld {
  // WARN: l'ordre des champs est important car utilise par l'ecriture/lecture de ORES

  private boolean ddeRegimeSeuil;

  public final boolean getDdeRegimeSeuil() {
    return ddeRegimeSeuil;
  }

  /**
   * @param newDdeRegime
   */
  public final void setDdeRegimeSeuil(boolean newDdeRegime) {
    ddeRegimeSeuil = newDdeRegime;
  }

  @Override
  public String toString() {
    return "OrdResBrancheSeuilLateral [ddeRegime=" + ddeRegimeSeuil + "]";
  }
}
