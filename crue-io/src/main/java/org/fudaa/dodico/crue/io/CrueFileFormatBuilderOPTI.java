/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.opti.CrueConverterOPTI;
import org.fudaa.dodico.crue.io.opti.CrueDaoStructureOPTI;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.OrdPrtCIniModeleBase;

public class CrueFileFormatBuilderOPTI implements CrueFileFormatBuilder<OrdPrtCIniModeleBase> {

  @Override
  public Crue10FileFormat<OrdPrtCIniModeleBase> getFileFormat(final CoeurConfigContrat version) {
    boolean ignoreTolStQ=Crue10VersionConfig.isLowerOrEqualsTo_V1_2(version.getXsdVersion());
    return new Crue10FileFormat<>(
        new CrueDataXmlReaderWriterImpl<>(CrueFileType.OPTI, version,new CrueConverterOPTI(ignoreTolStQ),
            new CrueDaoStructureOPTI(Crue10VersionConfig.V_1_1_1.equals(version.getXsdVersion()),ignoreTolStQ)));

  }

}
