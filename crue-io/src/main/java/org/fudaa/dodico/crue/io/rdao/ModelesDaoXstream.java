/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import com.thoughtworks.xstream.XStream;

/**
 * @author deniger
 */
public class ModelesDaoXstream {
  public static void configureXstream(final XStream xstream) {
    xstream.alias("Modeles", ModelesDao.class);
    xstream.alias("ModeleRegul", ModelesDao.ModeleRegulDao.class);
    xstream.alias("Modele", ModelesDao.ModeleDao.class);

    xstream.addImplicitCollection(ModelesDao.ModeleRegulDao.class, "Modele", ModelesDao.ModeleDao.class);
    xstream.addImplicitCollection(ModelesDao.ModeleRegulDao.class, "VariableRes", CommonResDao.VariableResDao.class);
    xstream.addImplicitCollection(ModelesDao.ModeleRegulDao.class, "VariableResQregul", CommonResDao.VariableResQregulDao.class);
    xstream.addImplicitCollection(ModelesDao.ModeleRegulDao.class, "VariableResZregul", CommonResDao.VariableResZregulDao.class);

  }
}
