/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.res;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.io.rcal.CrueDaoRCAL;
import org.fudaa.dodico.crue.io.rcal.CrueDaoStructureRCAL.ResCalcTransDao;
import org.fudaa.dodico.crue.io.rcal.CrueDaoStructureRCAL.ResPdtDao;
import org.fudaa.dodico.crue.io.rcal.CrueDaoStructureRCAL.ResultatsCalculPermanentDao;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;

/**
 * @author deniger
 */
public class RCalTimeStepBuilder {

  public static class Result {

    final Map<ResultatTimeKey, ResultatEntry> entries = new HashMap<>();
    final List<ResultatTimeKey> orderedKey = new ArrayList<>();
    final Map<ResultatTimeKey, String> delimiteurs = new HashMap<>();
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);

    public CtuluLog getLog() {
      return log;
    }

    public List<ResultatTimeKey> getOrderedKey() {
      return orderedKey;
    }

    public Map<ResultatTimeKey, String> getDelimiteurs() {
      return delimiteurs;
    }

    public Map<ResultatTimeKey, ResultatEntry> getEntries() {
      return entries;
    }
  }

  public Result extract(CrueDaoRCAL in, File rcalDir) {
    Result res = new Result();
    res.log.setDesc("rcal.extractTimeSteps");
    List<ResultatsCalculPermanentDao> resCalcPermsList = in.getResCalcPerms();
    String delimiteurChainePseudoPerm = in.getParametrage().getDelimiteurChaine("ResCalcPseudoPerm");
    if (CollectionUtils.isNotEmpty(resCalcPermsList)) {
      for (ResultatsCalculPermanentDao perm : resCalcPermsList) {
        ResultatTimeKey key = new ResultatTimeKey(perm.getNomRef());

        ResultatEntry entry = new ResultatEntry(key);
        entry.setBinFile(new File(rcalDir, perm.getHref()));
        entry.setOffsetInBytes(in.getParametrage().getBytes(perm.getOffsetMot()));
        if (res.entries.containsKey(key)) {
          res.log.addInfo("rcal.entryDefinedSeveralTime", key.toString());
        }
        res.entries.put(key, entry);
        res.orderedKey.add(key);
        res.delimiteurs.put(key, delimiteurChainePseudoPerm);
      }
    }
    String delimiteurChaineTrans = in.getParametrage().getDelimiteurChaine("ResPdt");
    List<ResCalcTransDao> resCalcTranss = in.getResCalcTranss();
    if (CollectionUtils.isNotEmpty(resCalcTranss)) {
      long max = 0;
      for (ResCalcTransDao resCalcTransDao : resCalcTranss) {
        List<ResPdtDao> resPdts = resCalcTransDao.getResPdts();
        if (CollectionUtils.isNotEmpty(resPdts)) {
          for (ResPdtDao resPdtDao : resPdts) {
            final long millis = DateDurationConverter.getDuration(resPdtDao.getTempsSimu()).getMillis();
            max = Math.max(max, millis);
          }
        }
      }
      boolean useDay = DateDurationConverter.isGreaterThanDay(max);
      for (ResCalcTransDao resCalcTransDao : resCalcTranss) {
        List<ResPdtDao> resPdts = resCalcTransDao.getResPdts();
        if (CollectionUtils.isNotEmpty(resPdts)) {
          for (ResPdtDao resPdtDao : resPdts) {
            ResultatTimeKey key = new ResultatTimeKey(resCalcTransDao.getNomRef(), DateDurationConverter.getDuration(
                    resPdtDao.getTempsSimu()).getMillis(), useDay);
            ResultatEntry entry = new ResultatEntry(key);
            entry.setBinFile(new File(rcalDir, resPdtDao.getHref()));
            entry.setOffsetInBytes(in.getParametrage().getBytes(resPdtDao.getOffsetMot()));
            if (res.entries.containsKey(key)) {
              res.log.addInfo("rcal.entryDefinedSeveralTime", key.toString());
            }
            res.entries.put(key, entry);
            res.orderedKey.add(key);
            res.delimiteurs.put(key, delimiteurChaineTrans);
          }

        }

      }
    }
    return res;
  }
}
