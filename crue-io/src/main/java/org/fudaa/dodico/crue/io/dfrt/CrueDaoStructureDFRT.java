/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dfrt;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.AbstractDaoFloatLoi;
import org.fudaa.dodico.crue.io.common.AbstractDaoLoi;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.common.AbstractDaoFloatLoi.DaoLoiFF;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * Toutes les structures dao utilisées et les inits du parser xml pour le format DFRT. Toutes ces structures sont
 * necessaires pour bien formatter le contenu xml. Ici il n'y a pas besoin de definir de sttructures supplementaires.
 * 
 * @author Adrien Hadoux
 */
@SuppressWarnings("PMD.VariableNamingConventions")
public class CrueDaoStructureDFRT implements CrueDataDaoStructure {
  
  private final String version;

  /**
   * @param version
   */
  public CrueDaoStructureDFRT(final String version) {
    super();
    this.version = version;
  }

  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    // -- creation des alias pour que ce soit + parlant dans le xml file --//
    xstream.alias(CrueFileType.DFRT.toString(), CrueDaoDFRT.class);
    xstream.alias("LoiFF", DaoLoiFF.class);
    xstream.omitField(DaoLoiFF.class, "DateZeroLoiDF");
    xstream.useAttributeFor(AbstractDaoLoi.class, "Nom");
    AbstractDaoFloatLoi.configureXstream(xstream, props,version);

  }

}
