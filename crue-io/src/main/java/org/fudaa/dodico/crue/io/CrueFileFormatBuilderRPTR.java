/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.rptr.CrueConverterRPTR;
import org.fudaa.dodico.crue.io.rptr.CrueDaoStructureRPTR;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.ResPrtReseau;

public class CrueFileFormatBuilderRPTR implements CrueFileFormatBuilder<ResPrtReseau> {

  @Override
  public Crue10FileFormat<ResPrtReseau> getFileFormat(final CoeurConfigContrat version) {

    return new Crue10FileFormat<>(new CrueDataXmlReaderWriterImpl<>(CrueFileType.RPTR,
            version, new CrueConverterRPTR(), new CrueDaoStructureRPTR()));

  }
}
