/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.conf;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueDaoStructure;

/**
 * @author f deniger
 */
public class CrueAocDaoStructure implements CrueDaoStructure {
    private final CrueDaoStructure courbeConfigurer;

    public CrueAocDaoStructure(final CrueDaoStructure courbeConfigurer) {
        this.courbeConfigurer = courbeConfigurer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configureXStream(final XStream xstream, final CtuluLog analyze) {
        xstream.alias("ConfigAoc", CrueAocDaoConfiguration.class);
        xstream.omitField(CrueAocDaoConfiguration.class, "Commentaire");
        xstream.alias("GlobalConfiguration", BlocConfiguration.class);
        xstream.alias("LoiConfigurations", CourbeConfigurations.class);
        xstream.addImplicitCollection(BlocConfiguration.class, "Options");
        this.configureAocOptionXStream(xstream);
        if (courbeConfigurer != null) {
            courbeConfigurer.configureXStream(xstream, analyze);
        }
    }

    public void configureAocOptionXStream(final XStream xstream) {
        xstream.alias("AocOption", Option.class);
        xstream.useAttributeFor(Option.class, "Nom");
    }
}
