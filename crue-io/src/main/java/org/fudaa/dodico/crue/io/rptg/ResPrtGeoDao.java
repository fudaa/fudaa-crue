/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rptg;

/**
 *
 * @author deniger
 */
public class ResPrtGeoDao {
  int OffsetMot;
  String Href;

  public String getHref() {
    return Href;
  }

  public int getOffsetMot() {
    return OffsetMot;
  }

}
