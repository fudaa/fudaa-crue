/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.pnum;

import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.pnum.CrueDaoStructurePNUM.DaoParamNumCalcPseudoPerm;
import org.fudaa.dodico.crue.io.pnum.CrueDaoStructurePNUM.DaoParamNumCalcTrans;
import org.fudaa.dodico.crue.io.pnum.CrueDaoStructurePNUM.DaoParamNumCommuns;

/**
 * Représentation persistante du fichier xml PNUM (Paramètres numériques).
 * 
 * @author CDE
 */
public class CrueDaoPNUM extends AbstractCrueDao {

  protected DaoParamNumCommuns ParamNumCommuns;
  protected DaoParamNumCalcPseudoPerm ParamNumCalcPseudoPerm;
  protected DaoParamNumCalcTrans ParamNumCalcTrans;

}
