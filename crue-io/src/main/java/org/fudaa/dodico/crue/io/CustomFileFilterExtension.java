/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import com.memoire.bu.BuFileFilter;
import java.io.File;

/**
 * @author deniger
 */
@SuppressWarnings("serial")
public final class CustomFileFilterExtension extends BuFileFilter {

  final String[] extensions;

  public CustomFileFilterExtension(final String extension) {
    this(new String[]{extension}, extension);
  }

  public CustomFileFilterExtension(final String extension, final String description) {
    this(new String[]{extension}, description);
  }

  @Override
  public String getDescription() {
    return description_;
  }

  /**
   * @param filters
   * @param description
   */
  public CustomFileFilterExtension(final String[] filters, final String description) {
    super(filters, description);
    extensions = filters;
  }

  private boolean isSelected(final String name) {
    for (int i = 0; i < extensions.length; i++) {
      final String ext = extensions[i].charAt(0) == '.' ? extensions[i] : ".".concat(extensions[i]);
      if (name.endsWith(ext)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean accept(final File _f) {
    if (_f != null) {
      if (_f.isHidden()) {
        return false;
      }
      if (_f.isDirectory()) {
        return true;
      }
      if (isSelected(_f.getName())) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean accept(final File _d, final String _fn) {
    final File f = new File(_fn);
    if (f.isDirectory()) {
      return false;
    }
    return isSelected(f.getName());
  }
}
