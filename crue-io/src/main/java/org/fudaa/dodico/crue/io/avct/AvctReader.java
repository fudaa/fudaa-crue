/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.avct;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.metier.emh.CompteRenduAvancement;
import org.fudaa.dodico.crue.metier.emh.Indicateur;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author deniger
 */
public class AvctReader {

  private static final Logger LOGGER = Logger.getLogger(AvctReader.class.getName());

  public static String getAvctName(final ManagerEMHScenario scenario) {
    return scenario.getNom() + ".avct.log";
  }

  public CrueIOResu<CompteRenduAvancement> read(final File f) {
    CrueIOResu<CompteRenduAvancement> read = null;
    try (FileReader reader= new FileReader(f) ) {
      read = read(reader);
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, null, e);
    }
    if (read!=null && read.getMetier() != null) {
      read.getMetier().setAvcFile(f);
    }
    return read;
  }

  public CrueIOResu<CompteRenduAvancement> read(final URL url) {
    CrueIOResu<CompteRenduAvancement> read = null;
    try(InputStream input = url.openStream()) {
      read = read(new InputStreamReader(input));
    } catch (final IOException ex) {
      LOGGER.log(Level.SEVERE, null, ex);
    }
    return read;
  }

  public CrueIOResu<CompteRenduAvancement> read(final Reader f) {
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final CompteRenduAvancement avct = new CompteRenduAvancement();
    final ArrayList<String> lastLines = new ArrayList<>();
    try {
      final LineNumberReader lineReader = new LineNumberReader(f);
      String line = lineReader.readLine();
      while (line != null) {
        if (line.startsWith("-----")) {
          lastLines.clear();
        } else {
          lastLines.add(line);
        }
        line = lineReader.readLine();
      }
    } catch (final IOException ioe) {
      LOGGER.log(Level.WARNING, ioe.getMessage(), ioe);
      log.addWarn("io.avct.errorWhileReader", ioe.getMessage());
    }
    //pour faciliter les tests ci-dessous
    if (lastLines.size() < 4) {
      for (int i = lastLines.size(); i < 4; i++) {
        lastLines.add(null);
      }
    }
    final String line0 = lastLines.get(0);
    if (line0 != null) {
      avct.setCode(StringUtils.trim(line0));

      final String line1 = lastLines.get(1);
      if (line1 != null && !"FIN".equals(avct.getCode())) {
        try {
          avct.setDate(DateDurationConverter.getDate(line1));
        } catch (final Exception e) {
          log.addError(e.getMessage());
        }
        final String line2 = lastLines.get(2);
        if (line2 != null) {
          int nbIndicateur = 0;
          try {
            nbIndicateur = Integer.parseInt(line2);
          } catch (final NumberFormatException fmt) {
            log.addError("io.avct.fmtInvalid", line2);
          }
          if (nbIndicateur > 0 && lastLines.size() > 3) {
            readIndicateurs(avct, nbIndicateur, lastLines.subList(3, lastLines.size()));
          }
        }

      }
    }


    return new CrueIOResu<>(avct, log);

  }

  private void readIndicateurs(final CompteRenduAvancement avct, final int nbIndicateur, final List<String> lines) {
    for (int i = 0; i < Math.min(lines.size(), nbIndicateur); i++) {
      final String line = lines.get(i);
      if (line == null) {
        return;
      }
      final String[] values = StringUtils.split(line, ';');
      final Indicateur indic = new Indicateur();
      indic.setLine(line);
      if (values.length > 0) {
        final String value = values[0];
        try {
          indic.setValeur(Double.parseDouble(value));
        } catch (final NumberFormatException numberFormatException) {
          Logger.getLogger(AvctReader.class.getName()).log(Level.INFO, "message {0}", numberFormatException);
        }
      }
      if (values.length > 1) {
        final String value = values[1];
        try {
          indic.setMax(Double.parseDouble(value));
          avct.addIndicateur(indic);
        } catch (final NumberFormatException numberFormatException) {
          Logger.getLogger(AvctReader.class.getName()).log(Level.INFO, "message {0}", numberFormatException);
        }
      }

    }

  }
}
