/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rpti;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * Structures utilisées dans la classe CrueDaoPNUM
 *
 * @author Deniger
 */
public class CrueDaoStructureRPTI implements CrueDataDaoStructure {

  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {

    xstream.alias(CrueFileType.RPTI.toString(), CrueDaoRPTI.class);
    xstream.alias("ResPrtCIniCasier", ResPrtCIniCasierDao.class);
    xstream.alias("ResPrtCIniSection", ResPrtCIniSectionDao.class);
    xstream.useAttributeFor(ResPrtCIniCasierDao.class, "NomRef");
    xstream.useAttributeFor(ResPrtCIniSectionDao.class, "NomRef");
  }

  protected class ResPrtCIniCasierDao {

    String Zini;
    String NomRef;
  }

  protected class ResPrtCIniSectionDao {

    String Zini;
    String Qini;
    String NomRef;
  }
}
