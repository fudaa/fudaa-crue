package org.fudaa.dodico.crue.io.common;

import com.thoughtworks.xstream.XStream;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;

/**
 * @author CANEL Christophe
 *
 */
public class CrueDaoStructureLog implements CrueDataDaoStructure {

  /**
   * {@inheritDoc}
   */
  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    xstream.alias("LoggerGroup", LoggerGroup.class);
    xstream.alias("Logger", Logger.class);
    xstream.alias("Log", Log.class);
  }

  public static class Header {

    public String Date;
    public LoggerGroup LoggerGroup;
  }

  public static class LoggerGroup {

    public String Description;
    public String Arguments;
    public String Description_i18n;
    public final List<Logger> Loggers = new ArrayList<>();
    public final List<LoggerGroup> LoggerGroups = new ArrayList<>();
  }

  public static class Logger {

    public String Description;
    public String Arguments;
    public String Description_i18n;
    public final List<Log> Logs = new ArrayList<>();
  }

  public static class Log {

    public String Level;
    public String Message;
    public String Arguments;
    public String Message_i18n;
  }
}
