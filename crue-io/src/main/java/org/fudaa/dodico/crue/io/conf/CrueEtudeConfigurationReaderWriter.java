/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.conf;

import org.fudaa.dodico.crue.common.io.CrueDaoStructure;
import org.fudaa.dodico.crue.common.io.CrueXmlReaderWriterImpl;

/**
 * @author CANEL Christophe
 *
 */
public class CrueEtudeConfigurationReaderWriter extends CrueXmlReaderWriterImpl<CrueEtudeDaoConfiguration, CrueEtudeDaoConfiguration> {

  public CrueEtudeConfigurationReaderWriter(final String version, final CrueDaoStructure courbeConfigurer) {
    super("etude_configuration", version, new CrueEtudeConverter(), new CrueEtudeDaoStructure(courbeConfigurer));
  }
}
