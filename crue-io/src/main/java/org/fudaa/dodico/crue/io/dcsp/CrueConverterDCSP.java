/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dcsp;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.common.AbstractDaoFloatLoi;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.common.CrueHelper;
import org.fudaa.dodico.crue.io.dcsp.CrueDaoStructureDCSP.*;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.InfoEMHFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Factory qui se charge de remplir les structures DAO du ficheir DCSP avec les donnees metier et inversement.
 *
 * @author Adrien Hadoux
 */
public class CrueConverterDCSP implements CrueDataConverter<CrueDaoDCSP, CrueData> {
    String xsdVersion;

    public CrueConverterDCSP(String xsdVersion) {
        this.xsdVersion = xsdVersion;
    }

    /**
     * Incrémente le nombre d'erreur trouvées
     */
    @Override
    public CrueData convertDaoToMetier(final CrueDaoDCSP dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
        if (dataLinked == null) {
            ctuluLog.addInfo("io.dcsp.convert.drsoNull.error");
            return null;
        }

        // -- remplissage des branches --//
        daoToMetierBranche(dao.DonCalcSansPrtBranches, dataLinked, ctuluLog, xsdVersion);
        daoToMetierCasier(dao.DonCalcSansPrtCasiers, dataLinked, ctuluLog);

        // -- remplissage des sections --//
        return dataLinked;
    }

    private void daoToMetierCasier(final List<DaoCasier> donCalcSansPrtCasiers, final CrueData dataLinked,
                                   final CtuluLog analyser) {
        if (CollectionUtils.isEmpty(donCalcSansPrtCasiers)) {
            return;
        }
        for (final DaoCasier daoCasier : donCalcSansPrtCasiers) {
            final CatEMHCasier casier = dataLinked.findCasierByReference(daoCasier.NomRef);
            if (casier == null) {
                analyser.addInfo("io.global.cantFindCasier.error", daoCasier.NomRef);
            } else {
                final CrueConfigMetier cruePropertyDefinitionContainer = dataLinked.getCrueConfigMetier();
                final DonCalcSansPrtCasierProfil dcsp = InfoEMHFactory.getDCSPCasierProfil(casier, cruePropertyDefinitionContainer);
                dcsp.setCoefRuis(daoCasier.CoefRuis);
                casier.addInfosEMH(dcsp);
            }
        }
    }

    @Override
    public CrueData getConverterData(final CrueData in) {
        return in;
    }

    @Override
    public CrueDaoDCSP convertMetierToDao(final CrueData data, final CtuluLog ctuluLog) {
        final CrueDaoDCSP res = new CrueDaoDCSP();

        convertMetierToDaoBranche(data, ctuluLog, res);
        convertMetierToDaoCasier(data, ctuluLog, res);
        return res;
    }

    private void convertMetierToDaoBranche(final CrueData data, final CtuluLog analyser, final CrueDaoDCSP res) {
        res.DonCalcSansPrtBranches = new ArrayList<>();
        final List<CatEMHBranche> metier = data.getBranches();
        if (CollectionUtils.isNotEmpty(metier)) {
            for (final CatEMHBranche branche : metier) {
                final List<DonCalcSansPrt> listeDoncalc = branche.getDCSP();
                if (listeDoncalc != null) {
                    for (final DonCalcSansPrt dataCalc : listeDoncalc) {
                        if (dataCalc != null) {
                            CrueConverterDCSP.metierToDaoBranches(res.DonCalcSansPrtBranches, branche, dataCalc, analyser);
                        } else {
                            CrueHelper.unknowdataFromFile("Branches", branche.getNom(), analyser);
                        }
                    }
                }
            }
        }
    }

    private void convertMetierToDaoCasier(final CrueData data, final CtuluLog analyser, final CrueDaoDCSP res) {
        res.DonCalcSansPrtCasiers = new ArrayList<>();
        final List<CatEMHCasier> casiers = data.getCasiers();
        if (CollectionUtils.isNotEmpty(casiers)) {
            for (final CatEMHCasier casier : casiers) {
                final List<DonCalcSansPrtCasierProfil> listeDoncalc = EMHHelper.selectClass(casier.getDCSP(),
                        DonCalcSansPrtCasierProfil.class);
                if (CollectionUtils.isNotEmpty(listeDoncalc)) {
                    if (listeDoncalc.size() > 1) {
                        analyser.addSevereError("dcsp.tooMuchDonForCasier", casier.getNom());
                    } else {
                        final DonCalcSansPrtCasierProfil pr = listeDoncalc.get(0);
                        final DaoCasier daoCasier = new DaoCasier();
                        daoCasier.NomRef = casier.getNom();
                        daoCasier.CoefRuis = pr.getCoefRuis();
                        res.DonCalcSansPrtCasiers.add(daoCasier);
                    }
                }
            }
        }
    }

    /**
     * Methode qui remplit une arrayList d'objets persistants qui constituent le fichier fichier DSCP: les Branches.
     */
    private static void metierToDaoBranches(final List<DaoBrancheAbstract> listePersistante, final CatEMHBranche branche,
                                            final DonCalcSansPrt dataCalc, final CtuluLog analyser) {
        if (branche == null) {
            return;
        }
        DaoBrancheAbstract branchePersist = null;
        final EnumBrancheType brancheType = branche.getBrancheType();

        if (EnumBrancheType.EMHBrancheBarrageFilEau.equals(brancheType)) {
            branchePersist = new DaoBrancheBarrageFilEau();

            if (dataCalc instanceof DonCalcSansPrtBrancheBarrageFilEau) {
                final DaoBrancheBarrageFilEau br = (DaoBrancheBarrageFilEau) branchePersist;

                // -- recuperation de la Loi Regime denoye --//
                if (((DonCalcSansPrtBrancheBarrageFilEau) dataCalc).getRegimeManoeuvrant() != null) {

                    // - remplissage de la loi RegimeDenoye--//
                    final LoiFF loi = ((DonCalcSansPrtBrancheBarrageFilEau) dataCalc).getRegimeManoeuvrant();
                    br.RegimeManoeuvrant = new DaoRegimeManoeuvrant();
                    AbstractDaoFloatLoi.metierToDaoLoi(br.RegimeManoeuvrant, loi);
                }
                br.QLimInf = ((DonCalcSansPrtBrancheBarrageFilEau) dataCalc).getQLimInf();
                br.QLimSup = ((DonCalcSansPrtBrancheBarrageFilEau) dataCalc).getQLimSup();
                // -- on remplit la liste des elements seuils --//
                br.ElemBarrages = metierToDaoSeuilBarrage(((DonCalcSansPrtBrancheBarrageFilEau) dataCalc).getElemBarrage());
            } else {
                analyser.addInfo("io.dcsp.convert.brancheBarrageFilEau.error", branche.getNom());
            }
        } else if (EnumBrancheType.EMHBrancheBarrageGenerique.equals(brancheType)) {
            branchePersist = new DaoBrancheBarrageGenerique();
            // -- recuperation de la donnee de calcul de la branche --//
            if (dataCalc instanceof DonCalcSansPrtBrancheBarrageGenerique) {
                final DaoBrancheBarrageGenerique br = (DaoBrancheBarrageGenerique) branchePersist;

                // -- recuperation de la loi RegimeDenoye --//
                if (((DonCalcSansPrtBrancheBarrageGenerique) dataCalc).getRegimeDenoye() != null) {
                    final LoiFF loi = ((DonCalcSansPrtBrancheBarrageGenerique) dataCalc).getRegimeDenoye();
                    br.RegimeDenoye = new DaoRegimeDenoye();
                    AbstractDaoFloatLoi.metierToDaoLoi(br.RegimeDenoye, loi);
                }

                // -- recuperation de la loi RegimeNoye --//
                if (((DonCalcSansPrtBrancheBarrageGenerique) dataCalc).getRegimeNoye() != null) {

                    // - remplissage de la loi RegimeDenoye--//
                    final LoiFF loi = ((DonCalcSansPrtBrancheBarrageGenerique) dataCalc).getRegimeNoye();
                    br.RegimeNoye = new DaoRegimeNoye();
                    AbstractDaoFloatLoi.metierToDaoLoi(br.RegimeNoye, loi);
                }

                br.QLimInf = ((DonCalcSansPrtBrancheBarrageGenerique) dataCalc).getQLimInf();
                br.QLimSup = ((DonCalcSansPrtBrancheBarrageGenerique) dataCalc).getQLimSup();
                // -- on remplit la liste des elements seuils --//

            } else {
                analyser.addInfo("io.dcsp.convert.brancheBarrageGenerique.error", branche.getNom());
            }
        } else if (EnumBrancheType.EMHBrancheOrifice.equals(brancheType)) {

            branchePersist = new DaoBrancheOrifice();

            // -- recuperation de la donnee de calcul de la branche --//

            if (dataCalc instanceof DonCalcSansPrtBrancheOrifice) {
                final DaoBrancheOrifice br = (DaoBrancheOrifice) branchePersist;

                // -- on remplit la liste des elements seuils --//
                br.ElemOrifice = metierToDaoSeuilOrifice(((DonCalcSansPrtBrancheOrifice) dataCalc).getElemOrifice());
            } else {
                analyser.addInfo("io.dcsp.convert.brancheOrifice.error", branche.getNom());
            }
        } else if (EnumBrancheType.EMHBranchePdc.equals(brancheType)) {
            branchePersist = new DaoBranchePdc();

            // -- recuperation de la donnee de calcul de la branche --//

            if (dataCalc instanceof DonCalcSansPrtBranchePdc) {
                final DaoBranchePdc br = (DaoBranchePdc) branchePersist;

                // -- recuperation de la formule --//
                if (((DonCalcSansPrtBranchePdc) dataCalc).getPdc() != null) {

                    // - remplissage de la loi RegimeDenoye--//
                    final LoiFF loi = ((DonCalcSansPrtBranchePdc) dataCalc).getPdc();
                    br.Pdc = new DaoPdc();
                    AbstractDaoFloatLoi.metierToDaoLoi(br.Pdc, loi);
                }
            } else {
                analyser.addInfo("io.dcsp.convert.branchePdc.error", branche.getNom());
            }
        } else if (EnumBrancheType.EMHBrancheNiveauxAssocies.equals(brancheType)) {
            branchePersist = new DaoBrancheNiveauxAssocies();

            // -- recuperation de la donnee de calcul de la branche --//

            if (dataCalc instanceof DonCalcSansPrtBrancheNiveauxAssocies) {
                final DaoBrancheNiveauxAssocies br = (DaoBrancheNiveauxAssocies) branchePersist;

                // -- recuperation de la formule --//
                if (((DonCalcSansPrtBrancheNiveauxAssocies) dataCalc).getZasso() != null) {

                    // - remplissage de la loi RegimeDenoye--//
                    final LoiFF loi = ((DonCalcSansPrtBrancheNiveauxAssocies) dataCalc).getZasso();
                    br.Zasso = new DaoZasso();
                    AbstractDaoFloatLoi.metierToDaoLoi(br.Zasso, loi);
                }
                br.QLimInf = ((DonCalcSansPrtBrancheNiveauxAssocies) dataCalc).getQLimInf();
                br.QLimSup = ((DonCalcSansPrtBrancheNiveauxAssocies) dataCalc).getQLimSup();
            } else {
                analyser.addInfo("io.dcsp.convert.brancheNiveauAssocie.error", branche.getNom());
            }
        } else if (EnumBrancheType.EMHBrancheSaintVenant.equals(brancheType)) {
            branchePersist = new DaoBrancheSaintVenant();
            // -- recuperation de la donnee de calcul de la branche --//

            if (dataCalc instanceof DonCalcSansPrtBrancheSaintVenant) {
                final DaoBrancheClassiqueAbstract br = (DaoBrancheClassiqueAbstract) branchePersist;

                br.CoefBeta = ((DonCalcSansPrtBrancheSaintVenant) dataCalc).getCoefBeta();
                br.CoefRuis = ((DonCalcSansPrtBrancheSaintVenant) dataCalc).getCoefRuis();
                br.CoefRuisQdm = ((DonCalcSansPrtBrancheSaintVenant) dataCalc).getCoefRuisQdm();

                // //-- remplissage des sections de branches --//
            } else {
                analyser.addInfo("io.dcsp.convert.brancheSeuilSaintVenant.error", branche.getNom());
            }
        } else if (EnumBrancheType.EMHBrancheSeuilLateral.equals(brancheType)) {
            branchePersist = new DaoBrancheSeuilLateral();

            if (dataCalc instanceof DonCalcSansPrtBrancheSeuilLateral) {
                final DaoBrancheSeuilLateral br = (DaoBrancheSeuilLateral) branchePersist;

                // -- recuperation de la formule --//
                br.FormulePdc = ((DonCalcSansPrtBrancheSeuilLateral) dataCalc).getFormulePdc();
                // -- on remplit la liste des elements seuils --//
                br.ElemSeuils = metierToDaoSeuilPdc(((DonCalcSansPrtBrancheSeuilLateral) dataCalc).getElemSeuilAvecPdc());
            } else {
                analyser.addInfo("io.dcsp.convert.brancheLongitudinale.error", branche.getNom());
            }
        } else if (EnumBrancheType.EMHBrancheSeuilTransversal.equals(brancheType)) {
            branchePersist = new DaoBrancheSeuilTransversal();

            // -- recuperation de la donnee de calcul de la branche --//

            if (dataCalc instanceof DonCalcSansPrtBrancheSeuilTransversal) {
                final DaoBrancheSeuilTransversal br = (DaoBrancheSeuilTransversal) branchePersist;
                // -- recuperation de la formule --//
                br.FormulePdc = ((DonCalcSansPrtBrancheSeuilTransversal) dataCalc).getFormulePdc();
                // -- on remplit la liste des elements seuils --//
                br.ElemSeuils = metierToDaoSeuilPdc(((DonCalcSansPrtBrancheSeuilTransversal) dataCalc).getElemSeuilAvecPdc());
            } else {
                analyser.addInfo("io.dcsp.convert.brancheSeuilTransversal.error", branche.getNom());
            }
        }
        if (branchePersist == null) {
            CrueHelper.unknowEMH("Branche", branche.getNom(), analyser);
        } else {
            branchePersist.NomRef = branche.getNom();
            listePersistante.add(branchePersist);
        }
    }

    /**
     * Genere une liste d'elements seuils persistante a partir de la liste metier lue.
     *
     * @return List<ElemAvecSeuilPdc>
     */
    private static List<DaoElemAvecSeuilPdc> metierToDaoSeuilPdc(final Collection<ElemSeuilAvecPdc> listeEltSeuilsEMH) {
        final List<DaoElemAvecSeuilPdc> listeSeuilsPersist = new ArrayList<>();
        for (final ElemSeuilAvecPdc seuilMetier : listeEltSeuilsEMH) {
            final DaoElemAvecSeuilPdc seuilPersist = new DaoElemAvecSeuilPdc();
            seuilPersist.CoefD = seuilMetier.getCoefD();
            seuilPersist.CoefPdc = seuilMetier.getCoefPdc();
            seuilPersist.Largeur = seuilMetier.getLargeur();
            seuilPersist.Zseuil = seuilMetier.getZseuil();

            listeSeuilsPersist.add(seuilPersist);
        }

        return listeSeuilsPersist;
    }

    /**
     * Genere une liste d'elements seuils persistante a partir de la liste metier lue.
     *
     * @return List<ElemSeuil>
     */
    private static List<DaoElemBarrage> metierToDaoSeuilBarrage(
            final Collection<ElemBarrage> listeEltSeuilsEMH) {
        final List<DaoElemBarrage> listeSeuilsPersist = new ArrayList<>();
        for (final ElemBarrage seuilMetier : listeEltSeuilsEMH) {
            final DaoElemBarrage seuilPersist = new DaoElemBarrage();
            seuilPersist.CoefNoy = seuilMetier.getCoefNoy();
            seuilPersist.CoefDen = seuilMetier.getCoefDen();

            seuilPersist.Largeur = seuilMetier.getLargeur();
            seuilPersist.Zseuil = seuilMetier.getZseuil();

            listeSeuilsPersist.add(seuilPersist);
        }

        return listeSeuilsPersist;
    }

    private static DaoElemSeuilOrifice metierToDaoSeuilOrifice(
            final org.fudaa.dodico.crue.metier.emh.ElemOrifice seuilMetier) {
        final DaoElemSeuilOrifice seuilPersist = new DaoElemSeuilOrifice();
        seuilPersist.CoefD = seuilMetier.getCoefD();
        seuilPersist.Largeur = seuilMetier.getLargeur();
        seuilPersist.Zseuil = seuilMetier.getZseuil();
        seuilPersist.Haut = seuilMetier.getHaut();
        seuilPersist.CoefCtrLim = seuilMetier.getCoefCtrLim();
        seuilPersist.SensOrifice = seuilMetier.getSensOrifice();
        return seuilPersist;
    }

    /**
     * Methode qui met a jour les objets metier EMH branches a partir des donnees persistantes de DPTI et les prechargements de DRSO
     */
    private static void daoToMetierBranche(final List<DaoBrancheAbstract> listePersistants, final CrueData data,
                                           final CtuluLog analyser, String xsdVersion) {
        if (CollectionUtils.isNotEmpty(listePersistants)) {
            for (final Object persist : listePersistants) {
                if (persist instanceof DaoBrancheAbstract) {
                    final DaoBrancheAbstract branchePersist = (DaoBrancheAbstract) persist;
                    final String reference = branchePersist.NomRef;
                    final CatEMHBranche branche = data.findBrancheByReference(reference);
                    if (branche == null) {
                        analyser.addInfo("io.global.cantFindBranche.error", reference);
                    } else {
                        if (branchePersist instanceof DaoBrancheClassiqueAbstract) {
                            final DaoBrancheClassiqueAbstract br = (DaoBrancheClassiqueAbstract) branchePersist;
                            final DonCalcSansPrtBrancheSaintVenant dataCalcul = new DonCalcSansPrtBrancheSaintVenant(data
                                    .getCrueConfigMetier());
                            dataCalcul.setCoefBeta(br.CoefBeta);
                            dataCalcul.setCoefRuis(br.CoefRuis);
                            dataCalcul.setCoefRuisQdm(br.CoefRuisQdm);
                            branche.addInfosEMH(dataCalcul);
                        } // -- branche seuil --//
                        else if (branchePersist instanceof DaoBrancheSeuilAbstract) {
                            final DaoBrancheSeuilAbstract br = (DaoBrancheSeuilAbstract) branchePersist;

                            if ((branche instanceof EMHBrancheSeuilLateral)) {
                                final DonCalcSansPrtBrancheSeuilLateral dataCalcul = new DonCalcSansPrtBrancheSeuilLateral();

                                // -- recuperation de la formule --//
                                // pour les version 1.0.0 et 1.1.0 pour lesquelles la formule peut etre vide
                                if (br.FormulePdc == null) {
                                    br.FormulePdc = EnumFormulePdc.DIVERGENT;
                                }
                                dataCalcul.setFormulePdc(br.FormulePdc);
                                // -- on remplit la liste des elements seuils --//
                                dataCalcul.setElemSeuilAvecPdc(daoToMetierElemSeuilAvecPdc(br.ElemSeuils, data));
                                branche.addInfosEMH(dataCalcul);
                            } else if ((branche instanceof EMHBrancheSeuilTransversal)) {
                                final DonCalcSansPrtBrancheSeuilTransversal dataCalcul = new DonCalcSansPrtBrancheSeuilTransversal();

                                // -- recuperation de la formule --//
                                // pour les version 1.0.0 et 1.1.0 pour lesquelles la formule peut etre vide
                                if (br.FormulePdc == null) {
                                    br.FormulePdc = EnumFormulePdc.DIVERGENT;
                                }
                                dataCalcul.setFormulePdc(br.FormulePdc);
                                // -- on remplit la liste des elements seuils --//
                                dataCalcul.setElemSeuilAvecPdc(daoToMetierElemSeuilAvecPdc(br.ElemSeuils, data));
                                branche.addInfosEMH(dataCalcul);
                            } else {
                                analyser.addInfo("io.dcsp.convert.brancheSeuil.error", reference);
                            }
                        } // -- branche seuils orifices --//
                        else if (branchePersist instanceof DaoBrancheOrifice) {
                            final DaoBrancheOrifice br = (DaoBrancheOrifice) branchePersist;
                            final DonCalcSansPrtBrancheOrifice dataCalcul = new DonCalcSansPrtBrancheOrifice();
                            // -- on remplit la liste des elements seuils --//
                            dataCalcul.setElemOrifice(daoMetierBrancheOrifice(br.ElemOrifice, data));
                            branche.addInfosEMH(dataCalcul);
                        } // -- branche seuils PDC --//
                        else if (branchePersist instanceof DaoBranchePDCAbstract) {
                            final DaoBranchePDCAbstract br = (DaoBranchePDCAbstract) branchePersist;
                            final DonCalcSansPrtBranchePdc dataCalcul = new DonCalcSansPrtBranchePdc();
                            // -- recuperation de la Loi PDC --//
                            if (br.Pdc != null) {
                                final LoiFF newLoi = new LoiFF();
                                AbstractDaoFloatLoi.daoToMetierLoi(newLoi, br.Pdc);
                                dataCalcul.setPdc(newLoi);
                            }

                            branche.addInfosEMH(dataCalcul);
                        } // -- Branche niveau associé --//
                        else if (branchePersist instanceof DaoBrancheNiveauxAssocies) {
                            final DaoBrancheNiveauxAssocies br = (DaoBrancheNiveauxAssocies) branchePersist;
                            final DonCalcSansPrtBrancheNiveauxAssocies dataCalcul = new DonCalcSansPrtBrancheNiveauxAssocies(data
                                    .getCrueConfigMetier());
                            // -- recuperation de la Loi PDC --//
                            if (br.Zasso != null) {
                                final LoiFF newLoi = new LoiFF();
                                AbstractDaoFloatLoi.daoToMetierLoi(newLoi, br.Zasso);
                                dataCalcul.setZasso(newLoi);
                            }
                            dataCalcul.setQLimInf(br.QLimInf);
                            dataCalcul.setQLimSup(br.QLimSup);

                            branche.addInfosEMH(dataCalcul);
                        } else if (branchePersist instanceof DaoBrancheBarrageFilEauAbstract) {
                            final DaoBrancheBarrageFilEauAbstract br = (DaoBrancheBarrageFilEauAbstract) branchePersist;
                            final DonCalcSansPrtBrancheBarrageFilEau dataCalcul = new DonCalcSansPrtBrancheBarrageFilEau(data
                                    .getCrueConfigMetier());

                            // -- recuperation de la Loi RegimeDenoye --//
                            if (br.RegimeManoeuvrant != null) {
                                final LoiFF newLoi = new LoiFF();
                                AbstractDaoFloatLoi.daoToMetierLoi(newLoi, br.RegimeManoeuvrant);
                                dataCalcul.setRegimeManoeuvrant(newLoi);
                            }
                            dataCalcul.setQLimInf(br.QLimInf);
                            dataCalcul.setQLimSup(br.QLimSup);

                            // -- on remplit la liste des elements seuils --//
                            dataCalcul.setElemBarrage(daoToMetierElemSeuil(br.ElemBarrages, data, xsdVersion));
                            branche.addInfosEMH(dataCalcul);
                        } else if (branchePersist instanceof DaoBrancheBarrageGeneriqueAbstract) {
                            final DaoBrancheBarrageGeneriqueAbstract br = (DaoBrancheBarrageGeneriqueAbstract) branchePersist;
                            final DonCalcSansPrtBrancheBarrageGenerique dataCalcul = new DonCalcSansPrtBrancheBarrageGenerique(data
                                    .getCrueConfigMetier());

                            // -- recuperation de la Loi RegimeDenoye --//
                            if (br.RegimeDenoye != null) {
                                final LoiFF newLoi = new LoiFF();
                                AbstractDaoFloatLoi.daoToMetierLoi(newLoi, br.RegimeDenoye);
                                dataCalcul.setRegimeDenoye(newLoi);
                            }
                            // -- recuperation de la Loi RegimeNoye --//
                            if (br.RegimeNoye != null) {
                                final LoiFF newLoi = new LoiFF();
                                AbstractDaoFloatLoi.daoToMetierLoi(newLoi, br.RegimeNoye);
                                dataCalcul.setRegimeNoye(newLoi);
                            }

                            dataCalcul.setQLimInf(br.QLimInf);
                            dataCalcul.setQLimSup(br.QLimSup);
                            branche.addInfosEMH(dataCalcul);
                        } else {
                            analyser.addInfo("io.dcsp.convert.branche.inconnue.error", reference);
                        }
                    }
                }
            }
        }
    }

    /**
     * Genere une liste d'elements seuils persistante a partir de la liste metier lue.
     *
     * @return List<ElemSeuilAvecPdc>
     */
    private static List<ElemSeuilAvecPdc> daoToMetierElemSeuilAvecPdc(final List<DaoElemAvecSeuilPdc> listeSeuilsPersist,
                                                                      final CrueData res) {
        final List<ElemSeuilAvecPdc> listeEltSeuilsEMH = new ArrayList<>();
        if (listeSeuilsPersist != null) {
            for (final DaoElemAvecSeuilPdc seuilPersist : listeSeuilsPersist) {
                final ElemSeuilAvecPdc seuilMetier = new ElemSeuilAvecPdc(res.getCrueConfigMetier());
                seuilMetier.setCoefD(seuilPersist.CoefD);
                seuilMetier.setCoefPdc(seuilPersist.CoefPdc);
                seuilMetier.setLargeur(seuilPersist.Largeur);
                seuilMetier.setZseuil(seuilPersist.Zseuil);

                listeEltSeuilsEMH.add(seuilMetier);
            }
        }

        return listeEltSeuilsEMH;
    }

    /**
     * Genere une liste d'elements seuils persistante a partir de la liste metier lue.
     *
     * @return List<org.fudaa.dodico.crue.metier.emh.ElemSeuil>
     */
    private static List<ElemBarrage> daoToMetierElemSeuil(
            final List<DaoElemBarrage> listeSeuilsPersist, final CrueData res, String xsdVersion) {
        final List<ElemBarrage> listeEltSeuilsEMH = new ArrayList<>();
        for (final DaoElemBarrage seuilPersist : listeSeuilsPersist) {
            final ElemBarrage seuilMetier = new ElemBarrage(res
                    .getCrueConfigMetier());
            seuilMetier.setCoefNoy(seuilPersist.CoefNoy);
            //pour les versions 1.1.1 et 1.2 le coefDen n'est pas supporté donc on ne modifie pas:
            if (Crue10VersionConfig.isUpperThan_V1_2(xsdVersion)) {
                seuilMetier.setCoefDen(seuilPersist.CoefDen);
            }
            seuilMetier.setLargeur(seuilPersist.Largeur);
            seuilMetier.setZseuil(seuilPersist.Zseuil);

            listeEltSeuilsEMH.add(seuilMetier);
        }

        return listeEltSeuilsEMH;
    }

    private static org.fudaa.dodico.crue.metier.emh.ElemOrifice daoMetierBrancheOrifice(
            final DaoElemSeuilOrifice seuilPersist, final CrueData res) {
        final org.fudaa.dodico.crue.metier.emh.ElemOrifice seuilMetier = new org.fudaa.dodico.crue.metier.emh.ElemOrifice(
                res.getCrueConfigMetier());
        seuilMetier.setCoefD(seuilPersist.CoefD);
        seuilMetier.setLargeur(seuilPersist.Largeur);
        seuilMetier.setZseuil(seuilPersist.Zseuil);
        seuilMetier.setHaut(seuilPersist.Haut);

        seuilMetier.setCoefCtrLim(seuilPersist.CoefCtrLim);
        seuilMetier.setSensOrifice(seuilPersist.SensOrifice);
        return seuilMetier;
    }
}
