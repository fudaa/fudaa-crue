/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.common;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.metier.CrueData;

/**
 *
 * @author Frederic Deniger
 */
public class CrueConverterIdentity<D extends AbstractCrueDao> implements CrueDataConverter<D, D> {

  @Override
  public D convertDaoToMetier(final D dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
    return dao;
  }

  @Override
  public D getConverterData(final CrueData in) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public D convertMetierToDao(final D metier, final CtuluLog ctuluLog) {
    return metier;
  }
}
