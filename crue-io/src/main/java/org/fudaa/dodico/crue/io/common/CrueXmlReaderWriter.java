/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.common;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;

import java.io.File;
import java.io.OutputStream;
import java.net.URL;

/**
 * @param <M> l'objet metier supporte
 * @author deniger
 */
public interface CrueXmlReaderWriter<M> {
    /**
     * @return le type du fichier supporte
     */
    CrueFileType getFileType();

    /**
     * @return l'identifiant: dfrt par exemple.
     */
    String getXsdId();

    /**
     * @return le path du fichier xsd correspondant
     */
    String getXsdValidator();

    String getVersion();

    /**
     * Lit les données dans le fichier f avec les données liées.
     *
     * @return l'objet metier lu.
     */
    CrueIOResu<M> readXML(final File f, final CtuluLog ctuluLog, final CrueData dataLinked);

    /**
     * Lit les données dans le fichier f avec les données liées.
     */
    CrueIOResu<M> readXML(final String pathToResource, final CtuluLog analyzer, final CrueData dataLinked);

    /**
     * Lit les données dans le fichier f avec les données liées.
     */
    CrueIOResu<M> read(final URL f, final CtuluLog analyzer, final CrueData dataLinked);

    boolean isValide(final File xml, final CtuluLog res);

    boolean isValide(final String xml, final CtuluLog res);

    boolean isValide(final URL xml, final CtuluLog res);

    /**
     * MEthode qui permet d'ecrire les datas dans le fichier f specifie.
     *
     * @param metier données metier
     * @param f      le fichier
     */
    boolean writeXML(final CrueIOResu<CrueData> metier, final File f, final CtuluLog ctuluLog);

    /**
     * @param metier   l'objet metier
     * @param out      le flux de sortie qui ne sera pas ferme
     * @param ctuluLog les logs
     * @return true si reussite
     */
    boolean writeXMLMetier(final CrueIOResu<M> metier, final OutputStream out, final CtuluLog ctuluLog,
                           CrueConfigMetier props);

    /**
     * MEthode qui permet d'ecrire les datas dans le fichier f specifie.
     *
     * @param metier données metier
     * @param f      le fichier
     */
    boolean writeXMLMetier(final CrueIOResu<M> metier, final File f, final CtuluLog analyzer,
                           CrueConfigMetier props);

    /**
     * @param metier   l'objet metier
     * @param out      le flux de sortie qui ne sera pas ferme
     * @param ctuluLog
     * @return true si reussite
     */
    boolean writeXML(final CrueIOResu<CrueData> metier, final OutputStream out, final CtuluLog ctuluLog);
}
