/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores;

import com.thoughtworks.xstream.XStream;
import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBrancheBarrageFilEauOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBrancheBarrageGeneriqueOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBrancheNiveauxAssociesOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBrancheOrificeOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBranchePdcOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBrancheSaintVenantOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBrancheSeuilLateralOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBrancheSeuilTransversalOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBrancheStricklerOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBranchesOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResCasierOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResCasiersOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResNoeudNiveauContinuOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResNoeudsOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResSectionIdem;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResSectionInterpolee;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResSectionProfilOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResSectionSansGeometrieOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResSectionsOld;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * Structures utilisées dans la classe CrueDaoORES
 *
 * @author cde
 */
public class CrueDaoStructureORES_V1P1P1 implements CrueDataDaoStructure {

  public static final String PREFIX_PROPERTY_DDE = "dde";

  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {

    xstream.alias(CrueFileType.ORES.toString(), CrueDaoORES_V1P1P1.class);
    xstream.alias("OrdResBranches", DaoOrdResBranchesOld.class);
    xstream.alias("OrdResNoeuds", DaoOrdResNoeudsOld.class);
    xstream.alias("OrdResSections", DaoOrdResSectionsOld.class);
    xstream.alias("OrdResCasiers", DaoOrdResCasiersOld.class);
    final List<Class> ordres = new ArrayList<>();
    ordres.add(DaoOrdResNoeudNiveauContinuOld.class);
    ordres.add(DaoOrdResCasierOld.class);
    ordres.add(DaoOrdResSectionProfilOld.class);
    ordres.add(DaoOrdResSectionIdem.class);
    ordres.add(DaoOrdResSectionInterpolee.class);
    ordres.add(DaoOrdResSectionSansGeometrieOld.class);
    ordres.add(DaoOrdResBranchePdcOld.class);
    ordres.add(DaoOrdResBrancheSeuilTransversalOld.class);
    ordres.add(DaoOrdResBrancheSeuilLateralOld.class);
    ordres.add(DaoOrdResBrancheOrificeOld.class);
    ordres.add(DaoOrdResBrancheNiveauxAssociesOld.class);
    ordres.add(DaoOrdResBrancheStricklerOld.class);
    ordres.add(DaoOrdResBrancheBarrageGeneriqueOld.class);
    ordres.add(DaoOrdResBrancheBarrageFilEauOld.class);
    ordres.add(DaoOrdResBrancheSaintVenantOld.class);
    final Map<String, String> classNameXmlName = new HashMap<>();
    classNameXmlName.put("DaoOrdResCasier", "DaoOrdResCasierProfil");
    for (final Class ordClass : ordres) {
      String className = ordClass.getSimpleName();
      if (classNameXmlName != null && classNameXmlName.containsKey(className)) {
        className = classNameXmlName.get(className);
      }
      String xmlClassName = StringUtils.removeStart(className, "Dao");
      xmlClassName = StringUtils.removeEnd(xmlClassName, "Old");
      xstream.alias(xmlClassName, ordClass);
      final PropertyDescriptor[] propertyDescriptors = PropertyUtils.getPropertyDescriptors(ordClass);
      for (final PropertyDescriptor descriptor : propertyDescriptors) {
        final String name = descriptor.getName();
        if (name.startsWith(PREFIX_PROPERTY_DDE)) {
          xstream.aliasField(StringUtils.capitalize(name), ordClass, name);
        } else {
          xstream.omitField(ordClass, name);
        }
      }
    }
    xstream.omitField(DaoOrdResCasierOld.class, "ddeZ");
    xstream.aliasField("DdeSplan", DaoOrdResBrancheSaintVenantOld.class, "ddeSplanAct");
    xstream.aliasField("DdeRegime", DaoOrdResBrancheSeuilLateralOld.class, "ddeRegimeSeuil");
    xstream.aliasField("DdeRegime", DaoOrdResBrancheSeuilTransversalOld.class, "ddeRegimeSeuil");
    xstream.aliasField("DdeRegime", DaoOrdResBrancheOrificeOld.class, "ddeRegimeOrifice");
    xstream.aliasField("DdeRegime", DaoOrdResBrancheBarrageFilEauOld.class, "ddeRegimeBarrage");
    xstream.aliasField("DdeRegime", DaoOrdResBrancheBarrageGeneriqueOld.class, "ddeRegimeBarrage");
    xstream.omitField(DaoOrdResBrancheSaintVenantOld.class, "ddeSplanSto");
    xstream.omitField(DaoOrdResBrancheSaintVenantOld.class, "ddeSplanTot");
  }
}
