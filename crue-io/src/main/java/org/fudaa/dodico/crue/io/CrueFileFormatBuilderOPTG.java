/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.optg.CrueConverterOPTG;
import org.fudaa.dodico.crue.io.optg.CrueDaoStructureOPTG;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.OrdPrtGeoModeleBase;

public class CrueFileFormatBuilderOPTG implements CrueFileFormatBuilder<OrdPrtGeoModeleBase> {

  @Override
  public Crue10FileFormat<OrdPrtGeoModeleBase> getFileFormat(final CoeurConfigContrat version) {
    return new Crue10FileFormat<>(new CrueDataXmlReaderWriterImpl<>(
        CrueFileType.OPTG, version, new CrueConverterOPTG(), new CrueDaoStructureOPTG(
            Crue10VersionConfig.V_1_1_1.equals(version.getXsdVersion()))));

  }

}
