/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.metier.CrueData;

import java.io.File;
import java.io.OutputStream;
import java.net.URL;

/**
 * @param <M> donnee lue ou ecrite
 * @author Fred Deniger
 */
public interface CustomFileFormatVersionInterface<M> {
  /**
   * @return le format parent
   */
  CustomFileFormat<M> getFileFormat();

  /**
   * @return le nom de cette version
   */
  String getVersionName();

  /**
   * Lit les données dans le fichier f avec les données liées.
   *
   * @param url URL du fichier à lire
   * @param analyzer Conteneur des messages d'erreurs
   * @param dataLinked Données liées
   * @return M objet métier
   */
  M read(final URL url, final CtuluLog analyzer, final CrueData dataLinked);

  /**
   * Lit les données dans le fichier f avec les données liées.
   *
   * @param f Fichier à lire
   * @param analyzer Conteneur des messages d'erreurs
   * @param dataLinked Données liées
   * @return M objet métier
   */
  M read(final File f, final CtuluLog analyzer, final CrueData dataLinked);

  /**
   * Lit les données dans le fichier f avec les données liées.
   *
   * @param pathToResource Chemin du fichier
   * @param analyzer Conteneur des messages d'erreurs
   * @param dataLinked Données liées
   * @return M objet métier
   */
  M read(final String pathToResource, final CtuluLog analyzer, final CrueData dataLinked);

  /**
   * Méthode qui permet d'écrire les datas dans le fichier f spécifié.
   *
   * @param metier l'objet metier
   * @param f Fichier à écrire
   * @param analyzer Conteneur des messages d'erreurs
   * @return true si l'écriture du fichier s'est bien passée
   */
  boolean write(final CrueIOResu<CrueData> metier, final File f, final CtuluLog analyzer);

  /**
   * @param data l'objet metier
   * @param out le flux de sortie qui ne sera pas ferme
   * @param analyser Conteneur des messages d'erreurs
   * @return true si reussite
   */
  boolean write(final CrueIOResu<CrueData> data, final OutputStream out, final CtuluLog analyser);
}
