/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.lhpt;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.loi.LoiTypeContainer;
import org.fudaa.dodico.crue.io.common.AbstractDaoFloatLoi;
import org.fudaa.dodico.crue.io.common.AbstractDaoLoi;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EvolutionTF;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionTF;

import java.util.List;

/**
 * Structures utilisees dans la classe CrueDaoPersistLHPT
 */
@SuppressWarnings("PMD.VariableNamingConventions")
public class CrueDaoStructureLHPT implements CrueDataDaoStructure {

    private final String version;

    /**
     * @param version la version
     */
    public CrueDaoStructureLHPT(final String version) {
        super();
        this.version = version;
    }

    public static EchelleSection create(final String pk, final String sectionRef) {
        final EchelleSection res = new EchelleSection();
        res.SectionRef = sectionRef;
        res.PK = pk;
        return res;
    }

    @Override
    public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final LoiTypeContainer props) {
        xstream.alias(CrueFileType.LHPT.toString(), CrueDaoLHPT.class);
        xstream.alias("LoiFF", AbstractDaoFloatLoi.DaoLoiFF.class);
        xstream.alias("LoiDF", AbstractDaoFloatLoi.DaoLoiDF.class);
        xstream.alias("LoiTF", DaoLoiTF.class);
        xstream.omitField(AbstractDaoFloatLoi.DaoLoiFF.class, "DateZeroLoiDF");
        xstream.useAttributeFor(AbstractDaoLoi.class, "Nom");
        xstream.alias("EvolutionTF", EvolutionTF.class);
        xstream.alias("PointTF", PtEvolutionTF.class);
        xstream.useAttributeFor(AbstractDaoLoi.class, "Type");
        xstream.useAttributeFor(AbstractDaoLoi.class, "Nom");
        final SingleConverterPointTF converter = new SingleConverterPointTF();
        converter.setAnalyse(ctuluLog);
        xstream.registerConverter(converter);
        xstream.addImplicitCollection(EvolutionTF.class, "mpoints");
        AbstractDaoFloatLoi.configureXstream(xstream, props, version, ctuluLog);

        xstream.alias("EchellesSections", EchellesSections.class);
        xstream.addImplicitCollection(EchellesSections.class, "sections");

        xstream.alias("EchelleSection", EchelleSection.class);
        xstream.useAttributeFor(EchelleSection.class, "PK");
        xstream.useAttributeFor(EchelleSection.class, "SectionRef");
    }

    public static class EchelleSection {
        public String PK;
        public String SectionRef;
    }

    public static class EchellesSections {
        public List<EchelleSection> sections;
    }

}
