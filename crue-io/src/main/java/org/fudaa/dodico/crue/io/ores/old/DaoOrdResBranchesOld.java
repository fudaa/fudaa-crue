/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores.old;

/**
 *
 * @author Frederic Deniger
 */
public class DaoOrdResBranchesOld {

  public DaoOrdResBrancheBarrageFilEauOld OrdResBrancheBarrageFilEau;
  public DaoOrdResBrancheBarrageGeneriqueOld OrdResBrancheBarrageGenerique;
  public DaoOrdResBrancheNiveauxAssociesOld OrdResBrancheNiveauxAssocies;
  public DaoOrdResBrancheOrificeOld OrdResBrancheOrifice;
  public DaoOrdResBranchePdcOld OrdResBranchePdc;
  public DaoOrdResBrancheSaintVenantOld OrdResBrancheSaintVenant;
  public DaoOrdResBrancheSeuilLateralOld OrdResBrancheSeuilLateral;
  public DaoOrdResBrancheSeuilTransversalOld OrdResBrancheSeuilTransversal;
  public DaoOrdResBrancheStricklerOld OrdResBrancheStrickler;
}
