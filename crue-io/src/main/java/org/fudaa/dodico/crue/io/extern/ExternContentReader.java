/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.extern;

import com.Ostermiller.util.BadDelimiterException;
import com.Ostermiller.util.CSVParser;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluDoubleParser;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;

import java.io.*;

/**
 * Un lecteur de fichiers externes.
 *
 * @author Frederic Deniger
 */
public class ExternContentReader {

  public CrueIOResu<ExternContent> read(final File f) {
    final CrueIOResu<ExternContent> res = new CrueIOResu<>();
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    res.setAnalyse(log);
    if (f == null) {
      log.addSevereError("extern.noFileDefined");
      return res;
    }
    log.setResource(f.getAbsolutePath());
    log.setDesc(f.getAbsolutePath());
    if (!f.exists()) {
      log.addSevereError("extern.fileNotFound", f.getAbsoluteFile());
      return res;
    }

    try(FileReader fileReader = new FileReader(f)) {
      final LineNumberReader reader = new LineNumberReader(fileReader);
      res.setMetier(read(reader, log, f));
    } catch (final Exception ex) {
      log.manageException(ex);
    }
    if (res.getMetier() != null) {
      final boolean ok = res.getMetier().finalyseData();
      if (!ok) {
        log.addError("extern.columnsWithDifferentSize");
        return null;
      }
    }

    return res;
  }
  private static final  String COMMENT = "#";

  private ExternContent read(final LineNumberReader reader, final CtuluLog log, final File f) throws Exception {
    final ExternContent content = new ExternContent(f.getAbsolutePath());
    //nom
    String readLine = readComment(reader, log);
    if (readLine == null) {
      return null;
    }
    content.setName(readLine);
    //commentaire
    readLine = readComment(reader, log);
    if (readLine == null) {
      return null;
    }
    //values
    content.setComment(readLine);
    readLine = readComment(reader, log);
    if (readLine == null) {
      return null;
    }
    String[] line = parseLine(readLine);
    if (line.length == 0) {
      return content;
    }
    final ExternContentColumn[] contents = new ExternContentColumn[line.length];
    final int nbVariables = contents.length;
    for (int i = 0; i < nbVariables; i++) {
      contents[i] = new ExternContentColumn();
      contents[i].setTitle(line[i]);
      content.addColumn(contents[i]);
    }
    readLine = reader.readLine();
    int variableIdx = 0;
    final CtuluDoubleParser doubleParser = new CtuluDoubleParser();
    doubleParser.setEmptyIsNull(true);
    while (readLine != null) {
      line = parseLine(readLine);
      if (line.length != nbVariables && line.length != nbVariables + 1) {
        log.addError("extern.valueLineBadNumberOfColumns", reader.getLineNumber(), line.length, nbVariables, nbVariables + 1);
        return null;
      }
      for (int i = 0; i < nbVariables; i++) {
        final ExternContentColumn externContentColumn = contents[i];
        try {
          externContentColumn.addValue(doubleParser.parse(line[i]));
        } catch (final NumberFormatException numberFormatException) {
          log.addError("extern.valueLineBadContent", reader.getLineNumber(), line[i]);
          return null;
        }
      }
      if (line.length == nbVariables + 1) {
        final String label = line[nbVariables];
        if (StringUtils.isNotBlank(label)) {
          content.addLabel(variableIdx, label);
        }
      }
      readLine = reader.readLine();
      variableIdx++;
    }

    return content;
  }

  private String readComment(final LineNumberReader reader, final CtuluLog log) throws IOException {
    String readLine = reader.readLine();
    if (readLine == null) {
      log.addSevereError("extern.formatFileError");
      return null;
    }
    readLine = readLine.trim();
    if (!readLine.startsWith(COMMENT)) {
      log.addSevereError("extern.formatFileError");
      return null;
    }
    return readLine.substring(1);
  }

  private String[] parseLine(final String readLine) throws BadDelimiterException, IOException {
    final CSVParser csvParser = new CSVParser(new StringReader(readLine), ';');
    csvParser.changeQuote('\'');
    return csvParser.getLine();
  }
}
