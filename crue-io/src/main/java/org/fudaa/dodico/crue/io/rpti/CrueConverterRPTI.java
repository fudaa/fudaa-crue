/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rpti;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.rpti.CrueDaoStructureRPTI.ResPrtCIniCasierDao;
import org.fudaa.dodico.crue.io.rpti.CrueDaoStructureRPTI.ResPrtCIniSectionDao;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.AbstractResPrtCIni;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.ResPrtCIniCasier;
import org.fudaa.dodico.crue.metier.emh.ResPrtCIniSection;
import org.fudaa.dodico.crue.metier.factory.PredicateFactory;

/**
 * Classe qui se charge de remplir les structures DAO du fichier PNUM avec les donnees metier et inversement.
 *
 * @author CDE
 */
public class CrueConverterRPTI implements CrueDataConverter<CrueDaoRPTI, CrueData> {

  public static double hexaToDouble(final String hexa) {
    final Long valueOf=new BigInteger(hexa, 16).longValue();
    return Double.longBitsToDouble(valueOf);
  }

  public static String doubleToHexa(final double in) {
    return Long.toHexString(Double.doubleToLongBits(in));
  }

  private static class ResPrtMapper<K extends EMH, V extends AbstractResPrtCIni> {

    final List<String> errorEMHFoundButNotActive = new ArrayList<>();
    private final String messageErrorIfActiveEMHNotDefinedInRPTI;
    private final String messageErrorPrtNotFoundInModel;
    final List<String> errorPrtNotFoundInModele = new ArrayList<>();
    final List<K> foundEMH = new ArrayList<>();
    final List<V> foundResPrt = new ArrayList<>();
    final Set<String> idsOfResPrtCini = new HashSet<>();
    private final String messageErrorEMHFoundButNotActive;

    /**
     * @param errorIfActiveEMHNotDefinedInRPTI
     */
    public ResPrtMapper(final String errorIfActiveEMHNotDefinedInRPTI, final String messageErrorPrtNotFoundInModel,
                        final String messageErrorEMHFoundButNotActive) {
      super();
      this.messageErrorIfActiveEMHNotDefinedInRPTI = errorIfActiveEMHNotDefinedInRPTI;
      this.messageErrorPrtNotFoundInModel = messageErrorPrtNotFoundInModel;
      this.messageErrorEMHFoundButNotActive = messageErrorEMHFoundButNotActive;
    }

    public void addId(final ResPrtCIniCasierDao v) {
      idsOfResPrtCini.add(StringUtils.upperCase(v.NomRef.toUpperCase()));
    }

    public void addId(final ResPrtCIniSectionDao v) {
      idsOfResPrtCini.add(StringUtils.upperCase(v.NomRef.toUpperCase()));
    }

    public void found(final K k, final V v) {
      foundEMH.add(k);
      foundResPrt.add(v);
    }

    public void foundInModeleButNonActive(final ResPrtCIniCasierDao v) {
      errorEMHFoundButNotActive.add(v.NomRef);
    }

    public void foundInModeleButNonActive(final ResPrtCIniSectionDao v) {
      errorEMHFoundButNotActive.add(v.NomRef);
    }

    public void notFoundInModele(final ResPrtCIniCasierDao v) {
      errorPrtNotFoundInModele.add(v.NomRef);
    }

    public void notFoundInModele(final ResPrtCIniSectionDao v) {
      errorPrtNotFoundInModele.add(v.NomRef);
    }

    protected boolean valid(final List<? extends EMH> activeEMH, final CtuluLog log) {
      if (!errorPrtNotFoundInModele.isEmpty()) {
        log.addError(messageErrorPrtNotFoundInModel, StringUtils.join(errorPrtNotFoundInModele, ";"));
      }
      if (!errorEMHFoundButNotActive.isEmpty()) {
        log.addError(messageErrorEMHFoundButNotActive, StringUtils.join(errorEMHFoundButNotActive, ";"));
      }
      final List<String> activeEMHNotDefinedInPrt = new ArrayList<>();
      for (final EMH emh : activeEMH) {
        if (!idsOfResPrtCini.contains(emh.getId())) {
          activeEMHNotDefinedInPrt.add(emh.getNom());
        }
      }
      final boolean activeEMHAllDefined = activeEMHNotDefinedInPrt.isEmpty();
      if (!activeEMHAllDefined) {
        log.addError(messageErrorIfActiveEMHNotDefinedInRPTI, StringUtils.join(activeEMHNotDefinedInPrt, ";"));
      }
      return activeEMHAllDefined && errorPrtNotFoundInModele.isEmpty() && errorEMHFoundButNotActive.isEmpty();
    }

    public void apply() {
      final int size = foundEMH.size();
      for (int i = 0; i < size; i++) {
        foundEMH.get(i).addInfosEMH(foundResPrt.get(i));
      }

    }
  }

  /**
   * Conversion des objets DAO en objets métier
   */
  @Override
  public CrueData convertDaoToMetier(final CrueDaoRPTI dao, final CrueData dataLinked, final CtuluLog ctuluLog) {


    final ResPrtMapper<CatEMHCasier, ResPrtCIniCasier> casiersMapper = convertDaoToMetierCasier(dao, dataLinked);
    final ResPrtMapper<CatEMHSection, ResPrtCIniSection> sectionsMapper = convertDaoToMetierSection(dao, dataLinked);
    final List<CatEMHCasier> allActiveCasier = CollectionCrueUtil.select(dataLinked.getCasiers(),
                                                                   PredicateFactory.PREDICATE_EMH_ACTUALLY_ACTIVE);
    //pour les sections il faut prendre le cas etat actif si reference par section idem active.
    final List<CatEMHSection> allActiveSections = CollectionCrueUtil.select(dataLinked.getSections(),
                                                                      PredicateFactory.PREDICATE_EMH_ACTUALLY_ACTIVE_FOR_CALCUL);
    final boolean casiersValid = casiersMapper.valid(allActiveCasier, ctuluLog);
    final boolean sectionsValid = sectionsMapper.valid(allActiveSections, ctuluLog);
    if (casiersValid && sectionsValid) {
      casiersMapper.apply();
      sectionsMapper.apply();
    }

    return dataLinked;
  }

  private ResPrtMapper<CatEMHCasier, ResPrtCIniCasier> convertDaoToMetierCasier(final CrueDaoRPTI dao,
                                                                                final CrueData dataLinked) {
    final ResPrtMapper<CatEMHCasier, ResPrtCIniCasier> casiersMapper = new ResPrtMapper<>(
            "io.rpti.activeCasierNotDefinedInRTI.error", "io.rpti.casierResPrtNotFoundInModel.error",
            "io.rpti.casierFoundButNotActive.error");
    final List<ResPrtCIniCasierDao> resPrtCiniCasiers = dao.ResPrtCIniCasiers;
    if (resPrtCiniCasiers != null) {
      for (final ResPrtCIniCasierDao resPrtCiniCasierDao : resPrtCiniCasiers) {
        resPrtCiniCasierDao.NomRef = StringUtils.trim(resPrtCiniCasierDao.NomRef);
        casiersMapper.addId(resPrtCiniCasierDao);
        final CatEMHCasier foundCasier = dataLinked.findCasierByReference(resPrtCiniCasierDao.NomRef);
        if (foundCasier == null) {
          casiersMapper.notFoundInModele(resPrtCiniCasierDao);
        } else {
          if (foundCasier.getActuallyActive()) {
            final ResPrtCIniCasier ciniCasier = new ResPrtCIniCasier(false);
            ciniCasier.setZini(hexaToDouble(resPrtCiniCasierDao.Zini));
            casiersMapper.found(foundCasier, ciniCasier);
          } else {
            casiersMapper.foundInModeleButNonActive(resPrtCiniCasierDao);
          }
        }

      }
    }
    return casiersMapper;
  }

  private ResPrtMapper<CatEMHSection, ResPrtCIniSection> convertDaoToMetierSection(final CrueDaoRPTI dao,
                                                                                   final CrueData dataLinked) {
    final ResPrtMapper<CatEMHSection, ResPrtCIniSection> sectionsMapper = new ResPrtMapper<>(
            "io.rpti.activeSectionNotDefinedInRTI.error", "io.rpti.sectionResPrtNotFoundInModel.error",
            "io.rpti.sectionFoundButNotActive.error");
    final List<ResPrtCIniSectionDao> resPrtCiniSections = dao.ResPrtCIniSections;
    if (resPrtCiniSections != null) {
      for (final ResPrtCIniSectionDao resPrtCiniSectionDao : resPrtCiniSections) {
        resPrtCiniSectionDao.NomRef = StringUtils.trim(resPrtCiniSectionDao.NomRef);
        sectionsMapper.addId(resPrtCiniSectionDao);
        final CatEMHSection foundSection = dataLinked.findSectionByReference(resPrtCiniSectionDao.NomRef);
        if (foundSection == null) {
          sectionsMapper.notFoundInModele(resPrtCiniSectionDao);
        } else {
          if (foundSection.getActuallyActive(true)) {
            final ResPrtCIniSection ciniSection = new ResPrtCIniSection(false);
            ciniSection.setQini(hexaToDouble(resPrtCiniSectionDao.Qini));
            ciniSection.setZini(hexaToDouble(resPrtCiniSectionDao.Zini));
            sectionsMapper.found(foundSection, ciniSection);
          } else {
            sectionsMapper.foundInModeleButNonActive(resPrtCiniSectionDao);
          }
        }

      }
    }
    return sectionsMapper;
  }

  /**
   * Conversion des objets métier en objets DAO
   */
  @Override
  public CrueDaoRPTI convertMetierToDao(final CrueData metier, final CtuluLog ctuluLog) {

    throw new IllegalAccessError("Not implemented");
  }

  @Override
  public CrueData getConverterData(final CrueData in) {
    return in;
  }
}
