/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fudaa.dodico.crue.io.aide;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueConverter;

/**
 *
 * @author landrodie
 */
public class CrueConverterAide implements CrueConverter<CrueDaoAide, CrueDaoAideContents> {

    @Override
    public CrueDaoAideContents convertDaoToMetier(CrueDaoAide dao, CtuluLog ctuluLog) {
        return new CrueDaoAideContents(dao.getCommentaire(),dao.getListeAide());
    }

    @Override
    public CrueDaoAide convertMetierToDao(CrueDaoAideContents metier, CtuluLog ctuluLog) {
        return new CrueDaoAide(metier.getLstAide());
    }  
 
}
