/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.common;

import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;

/**
 * Classe abstraite qui permet de factoriser les attributs communs de toutes les lois
 */
public abstract class AbstractDaoLoi {

  /**
   *   Représente la balise portant le même nom dans le fichier XML
   */
  public String Nom;
  /**
   * Représente la balise portant le même nom dans le fichier XML
   */
  protected String Commentaire;
  protected EnumTypeLoi Type;

  public EnumTypeLoi getType() {
    return Type;
  }

  public void setType(final EnumTypeLoi type) {
    Type = type;
  }

  public String getCommentaire() {
    return Commentaire;
  }

  public void setCommentaire(final String commentaire) {
    Commentaire = commentaire;
  }
}
