/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.common;

import com.Ostermiller.util.CSVParser;
import com.Ostermiller.util.CSVPrinter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ResourceBundle;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluDefaultLogFormatter;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureLog.Logger;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureLog.LoggerGroup;
import org.fudaa.dodico.crue.io.log.CrueConverterLOG;

/**
 *
 * @author Frederic Deniger
 */
public class CrueConverterCommonLog {

  public static CtuluLog convertDaoToMetier(final Logger dao) {
    final CtuluLog metier = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    metier.setDesc(dao.Description);
    metier.setDescriptionArgs(fromString(dao.Arguments));
    for (final CrueDaoStructureLog.Log log : dao.Logs) {
      final CtuluLogRecord addRecord = metier.addRecord(CrueConverterLOG.convertDaoValueToEnum(log.Level), log.Message);
      addRecord.setArgs(fromString(log.Arguments));
      BusinessMessages.updateLocalizedMessage(addRecord);
    }
    return metier;
  }

  public static CtuluLogGroup convertDaoToMetier(final LoggerGroup dao) {
    final CtuluLogGroup metier = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    metier.setDescription(dao.Description);
    metier.setDescriptionArgs(fromString(dao.Arguments));
    for (final CrueDaoStructureLog.Logger logger : dao.Loggers) {
      metier.addLog(convertDaoToMetier(logger));
    }
    for (final CrueDaoStructureLog.LoggerGroup loggerGroup : dao.LoggerGroups) {
      metier.addGroup(convertDaoToMetier(loggerGroup));
    }
    return metier;
  }

  public static String[] fromString(final String args) {
    if (StringUtils.isBlank(args)) {
      return null;
    }
    final CSVParser parser = new CSVParser(new StringReader(args));
    parser.changeQuote('"');
    parser.changeDelimiter(';');
    try {
      return parser.getLine();
    } catch (final IOException iOException) {
    }
    return null;
  }

  public static Logger convertMetierToDao(final CtuluLog metier) {
    final CrueDaoStructureLog.Logger dao = new CrueDaoStructureLog.Logger();
    final CtuluDefaultLogFormatter formatter = new CtuluDefaultLogFormatter(false);
    final ResourceBundle resourceBundle = metier.getDefaultResourceBundle();
    dao.Description = metier.getDesc();
    dao.Arguments = toString(metier.getDescriptionArgs());
    dao.Description_i18n = BusinessMessages.getString(dao.Description, (Object[]) metier.getDescriptionArgs());
    for (final CtuluLogRecord record : metier.getRecords()) {
      final CrueDaoStructureLog.Log log = new CrueDaoStructureLog.Log();
      log.Level = record.getLevel().name();
      log.Message = record.getMsg();
      log.Arguments = toString(record.getArgs());
      log.Message_i18n = record.getLocalizedMessage();
      if (log.Message_i18n == null) {
        log.Message_i18n = formatter.format(record, resourceBundle);
      }
      dao.Logs.add(log);
    }
    return dao;
  }

  public static LoggerGroup convertMetierToDao(final CtuluLogGroup metier) {
    final CrueDaoStructureLog.LoggerGroup dao = new CrueDaoStructureLog.LoggerGroup();
    dao.Description = metier.getDescription();
    dao.Arguments = toString(metier.getDescriptionArgs());
    dao.Description_i18n = metier.getDesci18n();
    for (final CtuluLog log : metier.getLogs()) {
      dao.Loggers.add(convertMetierToDao(log));
    }
    for (final CtuluLogGroup logGroup : metier.getGroups()) {
      dao.LoggerGroups.add(convertMetierToDao(logGroup));
    }
    return dao;
  }

  public static String toString(final Object[] args) {
    if (ArrayUtils.isEmpty(args)) {
      return StringUtils.EMPTY;
    }
    final StringWriter stringWriter = new StringWriter();
    final CSVPrinter parser = new CSVPrinter(stringWriter);
    parser.changeQuote('"');
    parser.changeDelimiter(';');
    final String[] values = new String[args.length];
    for (int i = 0; i < values.length; i++) {
      values[i] = ObjectUtils.toString(args[i]);
    }
    parser.print(values);
    return stringWriter.toString();
  }
}
