/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.pnum;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.pnum.CrueDaoStructurePNUM.DaoElemPdt;
import org.fudaa.dodico.crue.io.pnum.CrueDaoStructurePNUM.DaoParamNumCalcPseudoPerm;
import org.fudaa.dodico.crue.io.pnum.CrueDaoStructurePNUM.DaoParamNumCalcTrans;
import org.fudaa.dodico.crue.io.pnum.CrueDaoStructurePNUM.DaoParamNumCommuns;
import org.fudaa.dodico.crue.io.pnum.CrueDaoStructurePNUM.DaoPdt;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.ElemPdt;
import org.fudaa.dodico.crue.metier.emh.ParamNumCalcPseudoPerm;
import org.fudaa.dodico.crue.metier.emh.ParamNumCalcTrans;
import org.fudaa.dodico.crue.metier.emh.ParamNumModeleBase;
import org.fudaa.dodico.crue.metier.emh.Pdt;
import org.fudaa.dodico.crue.metier.emh.PdtCst;
import org.fudaa.dodico.crue.metier.emh.PdtVar;

/**
 * Classe qui se charge de remplir les structures DAO du fichier PNUM avec les donnees metier et inversement.
 * 
 * @author CDE
 */
public class CrueConverterPNUM implements CrueDataConverter<CrueDaoPNUM, ParamNumModeleBase> {

  @Override
  public ParamNumModeleBase getConverterData(final CrueData in) {
    return in.getPNUM();
  }

  /**
   * Conversion des objets DAO en objets métier
   */
  @Override
  public ParamNumModeleBase convertDaoToMetier(final CrueDaoPNUM dao, final CrueData dataLinked,
      final CtuluLog ctuluLog) {

    final ParamNumModeleBase metier = dataLinked.getOrCreatePNUM();
    metier.setFrLinInf(dao.ParamNumCommuns.FrLinInf);
    metier.setFrLinSup(dao.ParamNumCommuns.FrLinSup);
    metier.setZref(dao.ParamNumCommuns.Zref);
    metier.setParamNumCalcPseudoPerm(daoToMetierPseudo(dao, dataLinked));
    metier.setParamNumCalcTrans(daoToMetierTrans(dao, dataLinked));

    return metier;
  }

  /**
   * Conversion des objets métier en objets DAO
   */
  @Override
  public CrueDaoPNUM convertMetierToDao(final ParamNumModeleBase metier, final CtuluLog ctuluLog) {

    final CrueDaoPNUM dao = new CrueDaoPNUM();
    dao.ParamNumCommuns = metierToDaoCommuns(metier);
    dao.ParamNumCalcPseudoPerm = metierToDaoPseudo(metier);
    dao.ParamNumCalcTrans = metierToDaoTrans(metier);
    return dao;
  }

  private Pdt daoToMetierPdt(final DaoPdt daoPdt, final CrueConfigMetier defs) {
    if (daoPdt == null) { return null; }
    if (StringUtils.isBlank(daoPdt.PdtCst)&& daoPdt.PdtVar == null) {
      return null;
    }
    if (daoPdt.PdtCst == null) {
      final PdtVar var = new PdtVar();
      for (final DaoElemPdt elemDao : daoPdt.PdtVar) {
        final ElemPdt elemMetier = new ElemPdt(defs);
        elemMetier.setNbrPdt(elemDao.NbrPdt);
        elemMetier.setDureePdt(DateDurationConverter.getDuration(elemDao.DureePdt));
        var.addElemPdt(elemMetier);
      }
      return var;
    } else {
      final PdtCst var = new PdtCst();
      var.setPdtCst(DateDurationConverter.getDuration(daoPdt.PdtCst));
      return var;
    }
  }

  private ParamNumCalcPseudoPerm daoToMetierPseudo(final CrueDaoPNUM dao, final CrueData data) {
    if (dao.ParamNumCalcPseudoPerm == null) { return null; }
    final ParamNumCalcPseudoPerm pseudo = new ParamNumCalcPseudoPerm(data.getCrueConfigMetier());

    pseudo.setCoefRelaxQ(dao.ParamNumCalcPseudoPerm.CoefRelaxQ);
    pseudo.setCoefRelaxZ(dao.ParamNumCalcPseudoPerm.CoefRelaxZ);
    pseudo.setCrMaxFlu(dao.ParamNumCalcPseudoPerm.CrMaxFlu);
    pseudo.setCrMaxTor(dao.ParamNumCalcPseudoPerm.CrMaxTor);
    pseudo.setNbrPdtDecoup(dao.ParamNumCalcPseudoPerm.NbrPdtDecoup);
    pseudo.setNbrPdtMax(dao.ParamNumCalcPseudoPerm.NbrPdtMax);
    pseudo.setTolMaxQ(dao.ParamNumCalcPseudoPerm.TolMaxQ);
    pseudo.setTolMaxZ(dao.ParamNumCalcPseudoPerm.TolMaxZ);
    pseudo.setPdt(daoToMetierPdt(dao.ParamNumCalcPseudoPerm.Pdt, data.getCrueConfigMetier()));
    return pseudo;
  }

  private ParamNumCalcTrans daoToMetierTrans(final CrueDaoPNUM dao, final CrueData data) {
    if (dao.ParamNumCalcTrans == null) { return null; }
    final ParamNumCalcTrans paramNumCalcTrans = new ParamNumCalcTrans(data.getCrueConfigMetier());
    paramNumCalcTrans.setCrMaxFlu(dao.ParamNumCalcTrans.CrMaxFlu);
    paramNumCalcTrans.setCrMaxTor(dao.ParamNumCalcTrans.CrMaxTor);
    paramNumCalcTrans.setThetaPreissmann(dao.ParamNumCalcTrans.ThetaPreissmann);
    paramNumCalcTrans.setPdt(daoToMetierPdt(dao.ParamNumCalcTrans.Pdt, data.getCrueConfigMetier()));
    return paramNumCalcTrans;
  }

  private DaoParamNumCommuns metierToDaoCommuns(final ParamNumModeleBase metier) {
    final DaoParamNumCommuns paramNumCommuns = new DaoParamNumCommuns();
    paramNumCommuns.FrLinInf = metier.getFrLinInf();
    paramNumCommuns.FrLinSup = metier.getFrLinSup();
    paramNumCommuns.Zref = metier.getZref();
    return paramNumCommuns;
  }

  private DaoPdt metierToDaoPdt(final Pdt pdt) {
    if (pdt == null) { return null; }
    final DaoPdt daoPdt = new DaoPdt();
    if (pdt instanceof PdtCst) {
      daoPdt.PdtCst = DateDurationConverter.durationToXsd(((PdtCst) pdt).getPdtCst());
    } else {
      final PdtVar var = (PdtVar) pdt;
      final List<ElemPdt> elementsPdt = var.getElemPdt();
      daoPdt.PdtVar = new ArrayList<>(elementsPdt.size());
      for (final ElemPdt elemPdt : elementsPdt) {
        final DaoElemPdt daoElem = new DaoElemPdt();
        daoElem.DureePdt = DateDurationConverter.durationToXsd(elemPdt.getDureePdt());
        daoElem.NbrPdt = elemPdt.getNbrPdt();
        daoPdt.PdtVar.add(daoElem);
      }
    }
    return daoPdt;
  }

  private DaoParamNumCalcPseudoPerm metierToDaoPseudo(final ParamNumModeleBase metier) {
    final ParamNumCalcPseudoPerm inMetier = metier.getParamNumCalcPseudoPerm();
    if (inMetier == null) { return null; }
    final DaoParamNumCalcPseudoPerm daoPseudo = new DaoParamNumCalcPseudoPerm();
    daoPseudo.CoefRelaxQ = inMetier.getCoefRelaxQ();
    daoPseudo.CoefRelaxZ = inMetier.getCoefRelaxZ();
    daoPseudo.CrMaxFlu = inMetier.getCrMaxFlu();
    daoPseudo.CrMaxTor = inMetier.getCrMaxTor();
    daoPseudo.NbrPdtDecoup = inMetier.getNbrPdtDecoup();
    daoPseudo.NbrPdtMax = inMetier.getNbrPdtMax();
    daoPseudo.TolMaxQ = inMetier.getTolMaxQ();
    daoPseudo.TolMaxZ = inMetier.getTolMaxZ();
    daoPseudo.Pdt = metierToDaoPdt(inMetier.getPdt());
    return daoPseudo;
  }

  private DaoParamNumCalcTrans metierToDaoTrans(final ParamNumModeleBase metier) {

    final ParamNumCalcTrans inMetier = metier.getParamNumCalcTrans();
    if (inMetier == null) { return null; }
    final DaoParamNumCalcTrans daoNumTrans = new DaoParamNumCalcTrans();
    daoNumTrans.CrMaxFlu = inMetier.getCrMaxFlu();
    daoNumTrans.CrMaxTor = inMetier.getCrMaxTor();
    daoNumTrans.ThetaPreissmann = inMetier.getThetaPreissmann();
    daoNumTrans.Pdt = metierToDaoPdt(inMetier.getPdt());
    if(daoNumTrans.Pdt ==null){
      daoNumTrans.Pdt=new DaoPdt();
    }
    return daoNumTrans;
  }

}
