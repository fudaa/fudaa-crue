package org.fudaa.dodico.crue.io;

import java.io.File;
import java.io.OutputStream;
import java.net.URL;
import java.util.Map;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.common.CrueHelper;
import org.fudaa.dodico.crue.io.neuf.AbstractCrue9Reader;
import org.fudaa.dodico.crue.io.neuf.AbstractCrue9Writer;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * @author deniger
 */
public abstract class AbstractCrue9FileFormat extends CustomFileFormatUnique<CrueIOResu<CrueData>> {

  private final CrueFileType fileType;

  /**
   * @param id identifiant du format
   * @param fileFormatByFileType on ajouter l'id et this a cette map.
   */
  public AbstractCrue9FileFormat(final CrueFileType id, final Map<CrueFileType, AbstractCrue9FileFormat> fileFormatByFileType) {
    super(1);
    this.fileType = id;
    nom = id.toString();
    this.id = nom;
    extensions = new String[]{nom.toLowerCase()};
    description = nom + ".file";
    fileFormatByFileType.put(id, this);

  }

  /**
   * @return le reader qui va bien
   */
  protected abstract AbstractCrue9Reader createReader();

  /**
   * @return
   */
  protected abstract AbstractCrue9Writer createWriter();


  @Override
  public CrueIOResu<CrueData> read(final File f, final CtuluLog analyzer, final CrueData dataLinked) {
    final AbstractCrue9Reader reader = createReader();
    reader.setDataLinked(dataLinked);
    final CtuluIOResult<CrueIOResu<CrueData>> operation = reader.read(f, null);
    CrueHelper.copyAnalyzer(analyzer, operation.getAnalyze());
    return operation.getSource();
  }

  @Override
  public CrueIOResu<CrueData> read(final String pathToResource, final CtuluLog analyzer, final CrueData dataLinked) {
    final CrueIOResu<CrueData> read = read(getClass().getResource(pathToResource), analyzer, dataLinked);
    analyzer.setDesc("read.file");
    analyzer.setDescriptionArgs(pathToResource);
    return read;
  }

  @Override
  public CrueIOResu<CrueData> read(final URL url, final CtuluLog analyzer, final CrueData dataLinked) {
    final AbstractCrue9Reader crue9reader = createReader();
    crue9reader.setDataLinked(dataLinked);
    crue9reader.setFile(url);
    final CtuluIOResult<CrueIOResu<CrueData>> read = crue9reader.read();
    CrueIOResu<CrueData> source = read.getSource();
    if (source == null) {
      source = new CrueIOResu<>();
    }
    analyzer.merge(read.getAnalyze());
    analyzer.setDesc("read.file");
    analyzer.setDescriptionArgs("read.file");
    source.setAnalyse(analyzer);
    return source;
  }

  @Override
  public boolean write(final CrueIOResu<CrueData> metier, final File f, final CtuluLog analyzer) {
    return writeMetier(metier, f, analyzer);
  }

  public boolean write(final CrueData metier, final File f, final CtuluLog analyzer) {
    return writeMetier(new CrueIOResu<>(metier), f, analyzer);
  }

  @Override
  public boolean write(final CrueIOResu<CrueData> metier, final OutputStream out, final CtuluLog analyser) {
    return writeMetier(metier, out, analyser);
  }


  public boolean writeMetier(final CrueIOResu<CrueData> metier, final File f, final CtuluLog analyzer) {
    analyzer.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    analyzer.setDesc("write.file");
    analyzer.setDescriptionArgs(f.getName());
    final AbstractCrue9Writer writer = createWriter();
    writer.setFile(f);
    final CtuluIOResult<CrueIOResu<CrueData>> operation = writer.write(metier);

    final boolean ok = CrueHelper.copyAnalyzer(analyzer, operation.getAnalyze());
    // Si ok = true, cela signifie que analyzer et operation sotn non null et que la copie a bien été faite

    return (ok && (operation.containsClosingError() || operation.containsFatalError()));
  }

  private boolean writeMetier(final CrueIOResu<CrueData> metier, final OutputStream out, final CtuluLog analyser) {
    throw new IllegalAccessError("Must be done");
  }
}
