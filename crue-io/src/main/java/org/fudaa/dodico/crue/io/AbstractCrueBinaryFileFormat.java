/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import java.io.File;
import java.io.OutputStream;
import java.net.URL;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.neuf.AbstractCrueBinaryReader;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * Classe specifique pour les reader de fichier binaires. Il ne supporte pas les URL car les lecteurs utilisent des FileChannel. Il suffit de
 * redéfinir la méthode {@link #createReader()}.
 *
 * @author deniger
 * @param <M> Objet qui permet de stocker les informations lues dans le fichier binaire
 */
public abstract class AbstractCrueBinaryFileFormat<M> extends CustomFileFormatUnique<CrueIOResu<M>> {

  private final CrueFileType type;

  /**
   * @param id identifiant du format
   */
  public AbstractCrueBinaryFileFormat(final CrueFileType id) {
    super(1);
    this.type = id;
    nom = id.toString();
    this.id = nom;
    extensions = new String[]{nom.toLowerCase()};
    description = nom + ".file";
  }

  /**
   * @return le reader qui va bien
   */
  protected abstract AbstractCrueBinaryReader<M> createReader();

  /**
   * @return the type
   */
  @Override
  public CrueFileType getType() {
    return type;
  }

  public CrueIOResu<M> read(final File f, final CrueData dataLinked) {
    final AbstractCrueBinaryReader<M> reader = createReader();
    reader.setResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    reader.setDataLinked(dataLinked);
    final CtuluIOResult<CrueIOResu<M>> operation = reader.read(f, null);
    operation.getAnalyze().setDesc("load." + nom.toLowerCase());
    return operation.getSource();
  }

  @Override
  public CrueIOResu<M> read(final String pathToResource, final CtuluLog analyzer, final CrueData dataLinked) {
    throw new IllegalArgumentException("Not supported");
  }

  @Override
  public CrueIOResu<M> read(final URL url, final CtuluLog analyzer, final CrueData dataLinked) {
    throw new IllegalArgumentException("Not supported");
  }

  @Override
  public boolean write(CrueIOResu<CrueData> metier, File f, CtuluLog analyzer) {
    throw new IllegalArgumentException("Not supported");
  }

  @Override
  public boolean write(CrueIOResu<CrueData> metier, OutputStream out, CtuluLog analyser) {
    throw new IllegalArgumentException("Not supported");
  }

  public boolean writeMetier(CrueIOResu<M> metier, File f, CtuluLog analyzer) {
    throw new IllegalArgumentException("Not supported");
  }

  public boolean writeMetier(CrueIOResu<M> metier, OutputStream out, CtuluLog analyser) {
    throw new IllegalArgumentException("Not supported");
  }

  @Override
  public CrueIOResu<M> read(File f, CtuluLog analyzer, CrueData dataLinked) {
    throw new IllegalArgumentException("Not supported");
  }
}
