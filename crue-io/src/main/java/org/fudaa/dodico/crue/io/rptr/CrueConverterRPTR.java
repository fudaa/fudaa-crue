/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rptr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.rptr.CrueDaoStructureRPTR.ResPrtNoeudDao;
import org.fudaa.dodico.crue.io.rptr.CrueDaoStructureRPTR.ResPrtReseauNoeudsDao;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.DonCLimMScenario;
import org.fudaa.dodico.crue.metier.emh.ResPrtReseau;
import org.fudaa.dodico.crue.metier.emh.ResPrtReseauNoeud;
import org.fudaa.dodico.crue.metier.emh.ResPrtReseauNoeuds;

/**
 * Classe qui se charge de remplir les structures DAO du fichier PNUM avec les donnees metier et inversement.
 * 
 * @author CDE
 */
public class CrueConverterRPTR implements CrueDataConverter<CrueDaoRPTR, ResPrtReseau> {

  @Override
  public ResPrtReseau getConverterData(final CrueData in) {
    return in.getModele().getResPrtReseau();
  }

  /**
   * Conversion des objets DAO en objets métier
   */
  @Override
  public ResPrtReseau convertDaoToMetier(final CrueDaoRPTR dao, final CrueData dataLinked, final CtuluLog ctuluLog) {

    final ResPrtReseau reseau = new ResPrtReseau();
    reseau.setCommentaire(dao.getCommentaire());
    final List<ResPrtReseauNoeudsDao> resPrtReseauNoeuds = dao.ResPrtReseauNoeudsDaos;
    if (CollectionUtils.isNotEmpty(resPrtReseauNoeuds)) {
      final List<ResPrtReseauNoeuds> listNoeuds = new ArrayList<>();
      final DonCLimMScenario conditionsLim = dataLinked.getConditionsLim();
      final Map<String, Calc> calcById = new HashMap<>();
      final List<Calc> calcs = conditionsLim.getCalc();
      for (final Calc calc : calcs) {
        calcById.put(calc.getNom(), calc);
      }
      for (final ResPrtReseauNoeudsDao resNoeuds : resPrtReseauNoeuds) {
        final Calc c = calcById.get(resNoeuds.nom);
        if (c == null) {
          //on ne charge pas le resultat en question
          ctuluLog.addWarn("io.rptr.calcNotFound", resNoeuds.nom);

        } else {
          final List<ResPrtNoeudDao> nds = resNoeuds.ResPrtReseauNoeudDaos;
          if (nds != null) {
            final List<ResPrtReseauNoeud> resNds = new ArrayList<>(nds.size());
            for (final ResPrtNoeudDao resPrtReseauNoeud : nds) {
              resNds.add(new ResPrtReseauNoeud((resPrtReseauNoeud.nom)));
            }
            final ResPrtReseauNoeuds noeuds = new ResPrtReseauNoeuds();
            noeuds.setCalc(c);
            noeuds.setResPrtReseauNoeud(resNds);
            listNoeuds.add(noeuds);
          }
        }
      }
      reseau.setListResPrtReseauNoeud(listNoeuds);
    }
    final ResPrtReseau resPrtReseau = dataLinked.getModele().getResPrtReseau();
    if (resPrtReseau != null) {
      dataLinked.getModele().removeInfosEMH(resPrtReseau);
    }
    dataLinked.getModele().addInfosEMH(reseau);
    return reseau;
  }

  /**
   * Conversion des objets métier en objets DAO
   */
  @Override
  public CrueDaoRPTR convertMetierToDao(final ResPrtReseau metier, final CtuluLog ctuluLog) {
    throw new IllegalAccessError("Not implemented");
  }
}
