/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dclm;

import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;

/**
 * Classe persistante qui reprend la meme structure que le fichier XML DCLM - Fichier des donnees portant sur les
 * conditions aux limites et les manoeuvres d'ouvrages d'un modele crue10. A persister tel quel.
 * 
 * @author CDE
 */
public class CrueDaoDCLM extends AbstractCrueDao {

  /** Contient la liste des calculs permanents et transitoires **/
  public List<CrueDaoDCLMContents.CalculAbstractPersist> listeCalculs;
}
