/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import gnu.trove.TFloatArrayList;
import gnu.trove.TObjectDoubleHashMap;
import gnu.trove.TObjectIntHashMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.io.neuf.STOSequentialReader.DonneesBranche;
import org.fudaa.dodico.crue.io.neuf.STRSequentialReader.DonneesSectionOuProfil;
import org.fudaa.dodico.crue.io.neuf.STRSequentialReader.DonneesSectionPourBranche;
import org.fudaa.dodico.crue.io.neuf.STRSequentialReader.DonneesStricklerPourLitSection;
import org.fudaa.dodico.crue.io.neuf.STRSequentialReader.SectionPourAutreBranche;
import org.fudaa.dodico.crue.io.neuf.STRSequentialReader.SectionPourBranche0Ou9;
import org.fudaa.dodico.crue.metier.Crue9TypeBranche;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.comparator.ComparatorRelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.EnumLitPosition;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.LoiFactory;
import org.fudaa.dodico.mesure.EvolutionReguliere;

/**
 * Permet de transformer les données binaires en données exploitable par Fudaa-Crue.
 *
 * @author cde
 */
public class STRFactory {

  public static final String LIT_ACTIF = "ACTIF";
  public static final String LIT_MINEUR = "MINEUR";
  public static final String LIT_MAJEUR = "MAJEUR";

  /**
   * @param sto
   * @param data
   * @return une map donnant pour chaque id ( crue10 ) de section, les ordonnées de la loi ZDact a utiliser dans le cas des branches 6 pour Crue 9.
   */
  public static Map<String, float[]> getLoiZDactForBranche6(STOSequentialReader sto, CrueData data) {
    Map<String, float[]> res = new HashMap<>();
    List<DonneesBranche> donneesBranches = sto.getDonneesBranches();
    for (DonneesBranche dataBr : donneesBranches) {
      if (dataBr.getNTyp() == Crue9TypeBranche.STRICKLER) {
        String nom = CruePrefix.addPrefix(dataBr.getNomBr().trim(), EnumCatEMH.BRANCHE).toUpperCase();
        CatEMHBranche branche = data.findBrancheByReference(nom);
        if (branche != null) {
          List<RelationEMHSectionDansBranche> sections = branche.getSections();
          Collections.sort(sections, new ComparatorRelationEMHSectionDansBranche(data.getCrueConfigMetier()));
          if (CollectionUtils.isNotEmpty(sections)) {
            CatEMHSection emhAmont = sections.get(0).getEmh();
            // STO.sing(STO.nsing(ib)+22+2*STO.NbSing(ib)) à STO.sing(STO.nsing(ib)+21+3*STO.NbSing(ib))
            int initAmont = 21 + 2 * dataBr.getNbSing();
            int lastAmont = 20 + 3 * dataBr.getNbSing();
            TFloatArrayList floatsAmont = new TFloatArrayList();
            for (int i = initAmont; i <= lastAmont; i++) {
              floatsAmont.add(dataBr.getSing()[i]);
            }
            res.put(emhAmont.getId(), floatsAmont.toNativeArray());
            if (sections.size() > 1) {
              CatEMHSection emhAval = sections.get(sections.size() - 1).getEmh();
              // STO.sing(STO.nsing(ib)+22+3*STO.NbSing(ib)) à STO.sing(STO.nsing(ib)+21+4*STO.NbSing(ib))
              int initAval = 21 + 3 * dataBr.getNbSing();
              int lastAval = 20 + 4 * dataBr.getNbSing();
              TFloatArrayList floatsAval = new TFloatArrayList();
              for (int i = initAval; i <= lastAval; i++) {
                floatsAval.add(dataBr.getSing()[i]);
              }
              res.put(emhAval.getId(), floatsAval.toNativeArray());
            }
          }
        }
      }
    }
    return res;

  }

  public static TObjectDoubleHashMap getCoefSinuoBySection(STOSequentialReader sto, CrueData data) {
    TObjectDoubleHashMap res = new TObjectDoubleHashMap();
    List<DonneesBranche> donneesBranches = sto.getDonneesBranches();
    // final int nbHaut = sto.getParametresGeneraux().getNbhaut();
    for (DonneesBranche dataBr : donneesBranches) {
      String nom = CruePrefix.addPrefix(dataBr.getNomBr().trim(), EnumCatEMH.BRANCHE).toUpperCase();
      CatEMHBranche branche = data.findBrancheByReference(nom);

      if (branche != null) {
        List<RelationEMHSectionDansBranche> sections = branche.getSections();
        for (RelationEMHSectionDansBranche relationEMHSectionDansBranche : sections) {
          String emhId = relationEMHSectionDansBranche.getEmhId();
          res.put(emhId, dataBr.getSinuo());
        }
      }
    }
    return res;
  }

  /**
   * Sauvegarde du profil dans les objets métier
   *
   * @param donneesSection Profil à sauvegarder
   * @return data transformed to ResPrtGeoSection
   */
  public static Crue9ResPrtGeoSection convertDonnees(final DonneesSectionOuProfil donneesSection, CrueConfigMetier ccm) {

    if (donneesSection == null) {
      return null;
    }
    int typeBra = donneesSection.getTypBra();
    boolean isTypeBrancheSpecCrue9 = typeBra == Crue9TypeBranche.BRA_2 || typeBra == Crue9TypeBranche.STRICKLER || typeBra == Crue9TypeBranche.BRA_15;
    final Crue9ResPrtGeoSection rptgSection = isTypeBrancheSpecCrue9 ? new Crue9ResPrtGeoSectionTypeBranche9(typeBra, donneesSection.isStricklerWith1Lit())
            : new Crue9ResPrtGeoSection(typeBra, donneesSection.isStricklerWith1Lit());
    rptgSection.setNom(donneesSection.getNom());

    final DonneesSectionPourBranche sectionBranche = donneesSection.getDonneesPourBranche();
    rptgSection.setZf(donneesSection.getZf());
    rptgSection.setZhaut(donneesSection.getZHaut());
    final float[] params = donneesSection.getParams();
    if (params != null && params.length == 5) {
      //CRUE-376 Pour les sections interpolées, la VariableRes ResPrtGeoSection.NumLitZf ne doit pas être créée.
      String type = donneesSection.getType();
      if (!"PROFINT".equals(type)) {
        rptgSection.setNumLitZf((int) params[0]);
      }
      rptgSection.setFenteTxS(params[1]);
      rptgSection.setFenteTxD(params[2]);
    }

    if (isTypeBrancheSpecCrue9) {
      fillSectionPourBranche2_6_15(donneesSection, rptgSection, ccm);

    } else if (sectionBranche instanceof SectionPourAutreBranche) {
      fillSectionPourAutreBranche(donneesSection, rptgSection, ccm);
    }
    final ResultXY finalFloat = getFinalFloat(null, donneesSection.getZFondsLits());
    final LoiFF lstLitZf = createLoi(finalFloat, rptgSection.getNom(), EnumTypeLoi.LstLitZf);
    rptgSection.setLstLitZf(lstLitZf);

    // traduire les LstLitTypeLit
    String[] typesLits = donneesSection.getTypesLits();
    if (typesLits != null) {
      float[] typeLitsValues = new float[typesLits.length];
      String[] nomsLits = donneesSection.getNomsLits();
      TObjectIntHashMap<String> typeLitValues = getTypeLitValues(ccm);
      int idx = 0;
      for (String crue9TypeLit : typesLits) {
        String key = StringUtils.removeStart(crue9TypeLit, "cstr").toUpperCase();
        if (LIT_ACTIF.equals(key)) {
          String nomLit = nomsLits[idx].toUpperCase();
          boolean isMineur = "MINEUR".equals(nomLit) || "LT_MINEUR".equals(nomLit);
          key = isMineur ? LIT_MINEUR : LIT_MAJEUR;
        }
        int value = typeLitValues.get(key);
        typeLitsValues[idx++] = value;
      }
      final ResultXY finalFloatTypeLit = getFinalFloat(null, typeLitsValues);
      final LoiFF lstLitTypeLit = createLoi(finalFloatTypeLit, rptgSection.getNom(), EnumTypeLoi.LstLitTypeLit);
      rptgSection.setLstLitTypeLit(lstLitTypeLit);
    }
    return rptgSection;
  }

  private static TObjectIntHashMap<String> getTypeLitValues(CrueConfigMetier ccm) {
    TObjectIntHashMap<String> typeLitValues = new TObjectIntHashMap<>();
    List<ItemEnum> enumValues = ccm.getPropertyEnum("Ten_TypeLit").getEnumValues();
    for (ItemEnum itemEnum : enumValues) {
      typeLitValues.put(itemEnum.getName().toUpperCase(), itemEnum.getId());
    }
    return typeLitValues;
  }

  private static int getFirstIndiceForLit() {
    return 1;
  }

  /**
   * @param tab1
   * @param tab2 not null and its length must equals or higher than tab1
   * @return tab tab[i]=tab1[i]+tab2[i] and with the size of tab1 and
   */
  private static float[] sum(float[] tab1, float[] tab2) {
    int length = tab1.length;
    float[] res = new float[length];
    for (int i = 0; i < length; i++) {
      res[i] = tab1[i] + tab2[i];
    }
    return res;
  }

  private static float[] sum(float[] tab1, float[] tab2, float[] tab3) {
    int length = tab1.length;
    float[] res = new float[length];
    for (int i = 0; i < length; i++) {
      res[i] = tab1[i] + tab2[i] + tab3[i];
    }
    return res;
  }

  private static class ResultXY {

    final float[] x;
    final float[] y;

    public ResultXY(float[] x, float[] y) {
      this.x = x;
      this.y = y;
    }
  }

  static ResultXY getFinalFloat(String[] lits, float[] init) {
    TFloatArrayList yRes = new TFloatArrayList(init.length);
    TFloatArrayList xRes = new TFloatArrayList(init.length);
    int idx = getFirstIndiceForLit();
    for (int i = 0; i < init.length; i++) {
      if (lits == null || lits[i] != null) {
        yRes.add(init[i]);
        xRes.add(idx++);
      }
    }
    return new ResultXY(xRes.toNativeArray(), yRes.toNativeArray());
  }

  static double[] getFinalFloat(float[] init) {
    double[] res = new double[init.length];
    for (int i = 0; i < init.length; i++) {
      res[i] = init[i];
    }
    return res;
  }

  static String[] getFinalLit(String[] lits) {
    List<String> res = new ArrayList<>(lits.length);
    for (int i = 0; i < lits.length; i++) {
      if (lits[i] != null) {
        res.add(lits[i]);
      }
    }
    return res.toArray(new String[0]);
  }

  static EnumLitPosition[] getFinalPosition(EnumLitPosition[] pos) {
    List<EnumLitPosition> res = new ArrayList<>(pos.length);
    for (int i = 0; i < pos.length; i++) {
      if (pos[i] != null) {
        res.add(pos[i]);
      }
    }
    return res.toArray(new EnumLitPosition[0]);
  }

  private static void fillSectionPourBranche2_6_15(DonneesSectionOuProfil in, Crue9ResPrtGeoSection out, CrueConfigMetier ccm) {
    final String nom = out.getNom();
    Crue9ResPrtGeoSectionTypeBranche9 specOut = (Crue9ResPrtGeoSectionTypeBranche9) out;
    final SectionPourBranche0Ou9 inSection = (SectionPourBranche0Ou9) in.getDonneesPourBranche();
    // we create all data.
    // used lits will have a name not null;
    String[] nomLits = new String[5];
    nomLits[2] = "Lt_Mineur";
    EnumLitPosition[] posLits = new EnumLitPosition[5];
    posLits[2] = inSection.getISec2() == 1 ? EnumLitPosition.PAS_BORDURE : EnumLitPosition.BORDURE_2_COTES;
    // voir la spec de résultat: pour le type de branche 6, il y a toujours qu'un seul lit.
    //les branche Strickler peuvent avoir 1 ou 5 lits.
    if (!in.isStricklerWith1Lit()) {
      nomLits[0] = "Lt_StoD";
      posLits[0] = EnumLitPosition.BORDURE_2_COTES;
      nomLits[4] = "Lt_StoG";
      posLits[4] = EnumLitPosition.BORDURE_2_COTES;
      nomLits[1] = "Lt_MajD";
      posLits[1] = EnumLitPosition.BORDURE_DROITE;
      nomLits[3] = "Lt_MajG";
      posLits[3] = EnumLitPosition.BORDURE_GAUCHE;
    }
    float[] positionLits = getCrue10Values(posLits, ccm);

    specOut.setLstLitPosBordure(createLoiLitIndex(positionLits, nom, EnumTypeLoi.LstLitPosBordure));

    float[] litInf = new float[nomLits.length];
    litInf[0] = inSection.getListeUlar4()[0] / 2;
    litInf[1] = inSection.getListeUlar2()[0] / 2;
    litInf[2] = inSection.getListeUlar1()[0];
    litInf[3] = litInf[1];
    litInf[4] = litInf[0];
    out.setLstLitLinf(createLoi(getFinalFloat(nomLits, litInf), nom, EnumTypeLoi.LstLitLinf));

    float[] litSup = new float[nomLits.length];
    litSup[0] = inSection.getListeUlar4()[inSection.getListeUlar4().length - 1] / 2;
    litSup[1] = inSection.getListeUlar2()[inSection.getListeUlar2().length - 1] / 2;
    litSup[2] = inSection.getListeUlar1()[inSection.getListeUlar1().length - 1];
    litSup[3] = litSup[1];
    litSup[4] = litSup[0];
    out.setLstLitLsup(createLoi(getFinalFloat(nomLits, litSup), nom, EnumTypeLoi.LstLitLsup));

    float[] litPSup = new float[nomLits.length];
    litPSup[0] = 0;
    litPSup[1] = inSection.getListePer2()[inSection.getListePer2().length - 1] / 2;
    litPSup[2] = inSection.getListePer1()[inSection.getListePer1().length - 1];
    litPSup[3] = litPSup[1];
    litPSup[4] = litPSup[0];
    out.setLstLitPsup(createLoi(getFinalFloat(nomLits, litPSup), nom, EnumTypeLoi.LstLitPsup));

    float[] litSSup = new float[nomLits.length];
    litSSup[0] = 0;
    litSSup[1] = inSection.getListeSec2()[inSection.getListeSec2().length - 1] / 2;
    litSSup[2] = inSection.getListeSec1()[inSection.getListeSec1().length - 1];
    litSSup[3] = litSSup[1];
    litSSup[4] = litSSup[0];
    out.setLstLitSsup(createLoi(getFinalFloat(nomLits, litSSup), nom, EnumTypeLoi.LstLitSsup));

    List<DonneesStricklerPourLitSection> stricklers = in.getStricklers();
    int max = Math.min(stricklers.size(), nomLits.length);
    float[] litKs = new float[nomLits.length];
    for (int i = 0; i < max; i++) {
      DonneesStricklerPourLitSection stric = stricklers.get(i);
      //demande CRUE-352
      String type = stric.getType();
      if ("STRIREF".equals(type)) {
        litKs[i] = stric.getValeurConstante();
      } else {
        EvolutionReguliere evol = new EvolutionReguliere(getFinalFloat(stric.getZs()), getFinalFloat(stric.getKs()),
                false);
        litKs[i] = (float) evol.getInterpolateYValueFor(in.getZHaut());
      }
    }
    // cas particulier pour setter correction le frottement
    if (in.isStricklerWith1Lit()) {
      litKs[2] = litKs[0];
      litKs[0] = 0;
    }
    out.setLstLitKsup(createLoi(getFinalFloat(nomLits, litKs), nom, EnumTypeLoi.LstLitKsup));


    // we build the discretisation in z:
    final float[] abscissesZ = getDiscretisationEnZ(in);
    // LoiZLact LoiFF STR.ular1(NbHaut) + STR.ular2(NbHaut)
    specOut.setLoiZLact(createLoi(abscissesZ, sum(inSection.getListeUlar1(), inSection.getListeUlar2()), nom,
            EnumTypeLoi.LoiZLact));
    //newZLcont STR.ular1(NbHaut) + STR.ular2(NbHaut) + STR.ular4(NbHaut)	
    specOut.setLoiZLcont(createLoi(abscissesZ, sum(inSection.getListeUlar1(), inSection.getListeUlar2(), inSection.getListeUlar4()), nom,
            EnumTypeLoi.LoiZLcont));

    // LoiZLsto LoiFF STR.ular4(NbHaut)
    specOut.setLoiZLsto(createLoi(abscissesZ, inSection.getListeUlar4(), nom, EnumTypeLoi.LoiZLsto));

    //LoiZLtot est la somme du ular1, ular2 et ular4
    specOut.setLoiZLtot(createLoi(abscissesZ, sum(inSection.getListeUlar1(), inSection.getListeUlar2(), inSection.getListeUlar4()), nom,
            EnumTypeLoi.LoiZLtot));



    // LoiZSact LoiFF STR.sec1(NbHaut) + STR.sec2(NbHaut)
    specOut.setLoiZSact(createLoi(abscissesZ, sum(inSection.getListeSec1(), inSection.getListeSec2()), nom,
            EnumTypeLoi.LoiZSact));
    // ZLimGLitSto double STR.dzg11
    specOut.setzLimGLitSto(inSection.getDZg11());
    // ZLimDLitSto double STR.dzd11
    specOut.setzLimDLitSto(inSection.getDZd11());
    // ZLimGLitMaj double STR.dzg12
    specOut.setzLimGLitMaj(inSection.getDZg12());
    // ZLimDLitMaj double STR.dzd12
    specOut.setzLimDLitMaj(inSection.getDZd12());
    // ZLimGLitMin double STR.dzg13
    specOut.setzLimGLitMin(inSection.getDZg13());
    // ZLimDLitMin double STR.dzd13
    specOut.setzLimDLitMin(inSection.getDZd13());
    // CoefKextrapol double STR.stric
    specOut.setCoefKextrapol(inSection.getStric());

    // LoiZSmin LoiFF STR.sec1(NbHaut)
    specOut.setLoiZSmin(createLoi(abscissesZ, inSection.getListeSec1(), nom, EnumTypeLoi.LoiZSmin));
    // LoiZPmin LoiFF STR.per1(NbHaut)
    specOut.setLoiZPmin(createLoi(abscissesZ, inSection.getListePer1(), nom, EnumTypeLoi.LoiZPmin));
    // LoiZLmin LoiFF STR.ular1(NbHaut)
    specOut.setLoiZLmin(createLoi(abscissesZ, inSection.getListeUlar1(), nom, EnumTypeLoi.LoiZLmin));
    // LoiZSmaj LoiFF STR.sec2(NbHaut)
    specOut.setLoiZSmaj(createLoi(abscissesZ, inSection.getListeSec2(), nom, EnumTypeLoi.LoiZSmaj));
    // LoiZPmaj LoiFF STR.per2(NbHaut)
    specOut.setLoiZPmaj(createLoi(abscissesZ, inSection.getListePer2(), nom, EnumTypeLoi.LoiZPmaj));
    // LoiZLmaj LoiFF STR.ular2(NbHaut)
    specOut.setLoiZLmaj(createLoi(abscissesZ, inSection.getListeUlar2(), nom, EnumTypeLoi.LoiZLmaj));
    // LoiZCoefW1 LoiFF STR.coefw1(NbHaut)
    specOut.setLoiZCoefW1(createLoi(abscissesZ, inSection.getListeCoefW1(), nom, EnumTypeLoi.LoiZCoefW1));
    // LoiZCoefW2 LoiFF STR.coefw2(NbHaut)
    specOut.setLoiZCoefW2(createLoi(abscissesZ, inSection.getListeCoefW2(), nom, EnumTypeLoi.LoiZCoefW2));
  }

  private static void fillSectionPourAutreBranche(final DonneesSectionOuProfil inDonneesSection,
          final Crue9ResPrtGeoSection outRptgSection, CrueConfigMetier ccm) {
    final String nom = outRptgSection.getNom();
    final SectionPourAutreBranche sectionAutreBranche = (SectionPourAutreBranche) inDonneesSection.getDonneesPourBranche();

    final float[] abscissesZ = getDiscretisationEnZ(inDonneesSection);

    // Alimentation loi LoiZLact
    LoiFF loiFF = createLoi(abscissesZ, sectionAutreBranche.getListeLargLitActif(), nom, EnumTypeLoi.LoiZLact);
    if (loiFF != null) {
      outRptgSection.setLoiZLact(loiFF);
    }
    loiFF = createLoi(abscissesZ, sectionAutreBranche.getListeLargCont(), nom, EnumTypeLoi.LoiZLcont);
    if (loiFF != null) {
      outRptgSection.setLoiZLcont(loiFF);
    }

    loiFF = createLoi(abscissesZ, sectionAutreBranche.getListeDebLitActif(), nom, EnumTypeLoi.LoiZDact);
    if (loiFF != null) {
      outRptgSection.setLoiZDact(loiFF);
    }

    loiFF = createLoi(abscissesZ, sectionAutreBranche.getListeSLitActif(), nom, EnumTypeLoi.LoiZSact);
    if (loiFF != null) {
      outRptgSection.setLoiZSact(loiFF);
    }

    // Alimentation loi BetaDeZ
    loiFF = createLoi(abscissesZ, sectionAutreBranche.getListeBeta(), nom, EnumTypeLoi.LoiZBeta);
    if (loiFF != null) {
      outRptgSection.setLoiZBeta(loiFF);
    }

    // Alimentation loi LoiZLsto
    loiFF = createLoi(abscissesZ, sectionAutreBranche.getListeLargSto(), nom, EnumTypeLoi.LoiZLsto);
    if (loiFF != null) {
      outRptgSection.setLoiZLsto(loiFF);
    }

    //ZLtot= STR.LargLitActif(NbHaut)+STR.LargStoc(NbHaut)"
    float[] zLtotValues = sum(sectionAutreBranche.getListeLargLitActif(), sectionAutreBranche.getListeLargSto());
    loiFF = createLoi(abscissesZ, zLtotValues, nom, EnumTypeLoi.LoiZLtot);
    if (loiFF != null) {
      outRptgSection.setLoiZLtot(loiFF);
    }

    final float[] lstLibPost = getCrue10Values(sectionAutreBranche.getListeLitExtreme(), ccm);

    // Alimentation loi PsupDeLit
    outRptgSection.setLstLitPsup(createLoiLitIndex(sectionAutreBranche.getListePerHaut(), nom, EnumTypeLoi.LstLitPsup));
    // Alimentation loi LsupDeLit
    outRptgSection.setLstLitLsup(createLoiLitIndex(sectionAutreBranche.getListeLargHaut(), nom, EnumTypeLoi.LstLitLsup));
    // Alimentation loi KsupDeLit
    outRptgSection.setLstLitKsup(createLoiLitIndex(sectionAutreBranche.getListeStriHaut(), nom, EnumTypeLoi.LstLitKsup));
    // Alimentation loi LinfDeLit
    outRptgSection.setLstLitLinf(createLoiLitIndex(sectionAutreBranche.getListeLargFond(), nom, EnumTypeLoi.LstLitLinf));
    // Alimentation loi SsupDeLit
    outRptgSection.setLstLitSsup(createLoiLitIndex(sectionAutreBranche.getListeSurfHaut(), nom, EnumTypeLoi.LstLitSsup));
    // Alimentation LitPos
    outRptgSection.setLstLitPosBordure(createLoiLitIndex(lstLibPost, nom, EnumTypeLoi.LstLitPosBordure));

  }

  public static float[] getCrue10Values(int[] positions, CrueConfigMetier ccm) {
    final float[] crue10Position = new float[positions.length];
    final EnumLitPosition[] valuesEnum = EnumLitPosition.values();
    List<ItemEnum> propertyEnum = ccm.getPropertyEnum("Ten_PosBordure").getEnumValues();
    Map<String, ItemEnum> enumByName = new HashMap<>();
    for (ItemEnum itemEnum : propertyEnum) {
      enumByName.put(itemEnum.getName(), itemEnum);
    }
    for (int i = 0, imax = positions.length; i < imax; i++) {
      int j = positions[i];
      crue10Position[i] = -1f;//inconnu
      if (j >= 0 && j < valuesEnum.length) {
        ItemEnum itemEnum = enumByName.get(valuesEnum[j].name());
        if (itemEnum != null) {
          crue10Position[i] = itemEnum.getId();
        }
      }
    }
    return crue10Position;
  }

  public static float[] getCrue10Values(EnumLitPosition[] positions, CrueConfigMetier ccm) {
    final float[] crue10Position = new float[positions.length];
    List<ItemEnum> propertyEnum = ccm.getPropertyEnum("Ten_PosBordure").getEnumValues();
    Map<String, ItemEnum> enumByName = new HashMap<>();
    for (ItemEnum itemEnum : propertyEnum) {
      enumByName.put(itemEnum.getName(), itemEnum);
    }
    for (int i = 0, imax = positions.length; i < imax; i++) {
      EnumLitPosition crue9Position = positions[i];
      crue10Position[i] = -1f;//inconnu
      if (crue9Position != null) {
        ItemEnum itemEnum = enumByName.get(crue9Position.name());
        if (itemEnum != null) {
          crue10Position[i] = itemEnum.getId();
        }
      }
    }
    return crue10Position;
  }

  /**
   * Attention: ajout une valeur zf en premier. Donc si nbHaut vaut 50 renvoie un tableau avec 51 valeurs.
   *
   * @param inDonneesSection
   * @return
   */
  protected static float[] getDiscretisationEnZ(final DonneesSectionOuProfil inDonneesSection) {
    // Calcul des abscisses z
    final int nbHaut = inDonneesSection.getNbHaut();
    final float dzpro = inDonneesSection.getDZpro();
    final float[] abscissesZ = new float[nbHaut + 1];
    final float zf = inDonneesSection.getZf();
    for (int i = 0; i < nbHaut + 1; i++) {
      abscissesZ[i] = zf + i * dzpro;
    }
    return abscissesZ;
  }

  /**
   * Récupère une loi dont les abscisses sont des côtes (Z) de section, à partir des données passées en paramètres. Le nom est recompose pour le type
   * de loi et pour les emh de type Section.
   *
   * @param abscissesZ Ensemble des côtes
   * @param ordonnees Ensemble d'ordonnées
   * @param nomInitial Nom à donner à la loi
   * @param type Nom à donner à la variable Ordonnée
   * @return LoiFF ou null si la loi n'a pas eu être créée
   */
  protected static LoiFF createLoi(final float[] abscissesZ, final float[] initOrdonnees, final String nomInitial,
          final EnumTypeLoi type) {
    String id = type.getId();
    float[] ordonnees = initOrdonnees;
    //pour ces lois, on rajoute un point voir CRUE-216
    if (id.startsWith("LoiZ")) {
      assert ordonnees.length == abscissesZ.length - 1;
      ordonnees = new float[initOrdonnees.length + 1];
      System.arraycopy(initOrdonnees, 0, ordonnees, 1, initOrdonnees.length);
      if (EnumTypeLoi.LoiZBeta.equals(type)) {
        ordonnees[0] = 1;
      }
    }


    return LoiFactory.getLoiFFRPTG(abscissesZ, ordonnees, CruePrefix.addPrefixs(nomInitial, type, EnumCatEMH.SECTION),
            type);
  }

  protected static LoiFF createLoiLitIndex(final float[] ordonnees, final String nomInitial,
          final EnumTypeLoi type) {
    if (ordonnees == null) {
      return null;
    }
    float[] abs = new float[ordonnees.length];
    int idx = getFirstIndiceForLit();
    for (int i = 0; i < abs.length; i++) {
      abs[i] = idx++;
    }
    return createLoi(abs, ordonnees, nomInitial, type);
  }

  protected static LoiFF createLoi(final ResultXY xy, final String nomInitial,
          final EnumTypeLoi type) {

    return LoiFactory.getLoiFFRPTG(xy.x, xy.y, CruePrefix.addPrefixs(nomInitial, type, EnumCatEMH.SECTION),
            type);
  }
}
