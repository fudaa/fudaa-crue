/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.common;

import com.thoughtworks.xstream.converters.SingleValueConverter;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.metier.emh.EnumFormulePdc;
import org.fudaa.dodico.crue.metier.emh.EnumMethodeOrdonnancement;
import org.fudaa.dodico.crue.metier.emh.EnumPosSection;
import org.fudaa.dodico.crue.metier.emh.EnumSensOrifice;
import org.fudaa.dodico.crue.metier.emh.EnumSensOuv;

/**
 * Gestion des converter pour les enums
 *
 * @author deniger
 */
public final class EnumsConverter {

    /**
     * Code d'erreur a utiliser lorsqu'un enum ne peut etre reconstruit
     */
    public static final String CODE_CONVERT_ENUM_ERROR = "io.convert.enum.error";

    private EnumsConverter() {

    }

    protected static class DefaultEnumConverter extends AbstractSingleConverter {
        /**
         *
         */

        private final DualHashBidiMap enumString;
        private final Class classAccepted;

        /**
         * @param enumString    la map Enum<->String
         * @param classAccepted la classe de l'enum
         * @param analyze       l'analyze recevant les erreur.
         */
        public DefaultEnumConverter(final DualHashBidiMap enumString, final Class classAccepted, final CtuluLog analyze) {
            super();
            this.enumString = enumString;
            this.classAccepted = classAccepted;
            setAnalyse(analyze);
        }

        @Override
        public boolean canConvert(final Class arg0) {
            return classAccepted.equals(arg0);
        }

        @Override
        public Object fromString(final String arg0) {
            final Object res = enumString.getKey(arg0);
            if (res == null) {
                addFatalError(CODE_CONVERT_ENUM_ERROR, arg0);
            }
            return res;
        }

        @Override
        public String toString(final Object arg0) {
            final String res = (String) enumString.get(arg0);
            if (res == null) {
                addFatalError(CODE_CONVERT_ENUM_ERROR, arg0);
            }
            return res;

        }

    }

    private static final Map<Class, DualHashBidiMap> CRUE10_CONVERTER = new HashMap<>();
    private static final Map<Class, DualHashBidiMap> CRUE9_CONVERTER = new HashMap<>();

    static {
        CRUE10_CONVERTER.put(EnumFormulePdc.class, createEnumFormulePdcMap());
        CRUE9_CONVERTER.put(EnumFormulePdc.class, createEnumFormulePdcMapCrue9());
        CRUE10_CONVERTER.put(EnumSensOrifice.class, createEnumSensOrificeMap());
        CRUE9_CONVERTER.put(EnumSensOrifice.class, createEnumSensOrificeMapCrue9());
        CRUE10_CONVERTER.put(EnumSensOuv.class, createEnumSensOuvMap());
        CRUE9_CONVERTER.put(EnumSensOuv.class, createEnumSensOuvMapCrue9());
        CRUE10_CONVERTER.put(EnumPosSection.class, createEnumPositionSectionMap());
        CRUE10_CONVERTER.put(EnumMethodeOrdonnancement.class, createEnumMethodeOrdonnancementMap());
    }

    /**
     * a voir pour utiliser une sauvegarde eventuelle.
     *
     * @return
     */
    private static DualHashBidiMap createEnumFormulePdcMap() {
        final DualHashBidiMap res = new DualHashBidiMap();
        res.put(EnumFormulePdc.BORDA, "Borda");
        res.put(EnumFormulePdc.DIVERGENT, "Divergent");
        return res;
    }


    private static DualHashBidiMap createEnumPositionSectionMap() {
        final DualHashBidiMap res = new DualHashBidiMap();
        res.put(EnumPosSection.AMONT, "Amont");
        res.put(EnumPosSection.AVAL, "Aval");
        res.put(EnumPosSection.INTERNE, "Interne");
        return res;
    }

    private static DualHashBidiMap createEnumMethodeOrdonnancementMap() {
        final DualHashBidiMap res = new DualHashBidiMap();
        res.put(EnumMethodeOrdonnancement.ORDRE_DRSO, "OrdreDRSO");
        return res;
    }

    /**
     * @return une dual map enum<->String pour Crue 9.
     */
    private static DualHashBidiMap createEnumFormulePdcMapCrue9() {
        final DualHashBidiMap res = new DualHashBidiMap();
        res.put(EnumFormulePdc.BORDA, "1");
        res.put(EnumFormulePdc.DIVERGENT, "0");
        return res;
    }

    /**
     * @return une dual map enum<->String pour Crue 9.
     */
    private static DualHashBidiMap createEnumSensOrificeMapCrue9() {
        final DualHashBidiMap res = new DualHashBidiMap();
        res.put(EnumSensOrifice.BIDIRECT, "0");
        res.put(EnumSensOrifice.DIRECT, "1");
        res.put(EnumSensOrifice.INDIRECT, "-1");
        return res;
    }

    /**
     * @return une dual map enum<->String pour Crue 9.
     */
    private static DualHashBidiMap createEnumSensOrificeMap() {
        final DualHashBidiMap res = new DualHashBidiMap();
        res.put(EnumSensOrifice.BIDIRECT, "Bidirect");
        res.put(EnumSensOrifice.DIRECT, "Direct");
        res.put(EnumSensOrifice.INDIRECT, "Indirect");
        return res;
    }

    /**
     * EnumSensOuv
     *
     * @return une dual map enum<->String pour Crue 9.
     */
    private static DualHashBidiMap createEnumSensOuvMapCrue9() {
        final DualHashBidiMap res = new DualHashBidiMap();
        res.put(EnumSensOuv.OUV_VERS_BAS, "42");
        res.put(EnumSensOuv.OUV_VERS_HAUT, "41");
        return res;
    }

    /**
     * @return une dual map enum<->String pour Crue 10.
     */
    public static DualHashBidiMap createEnumSensOuvMap() {
        final DualHashBidiMap res = new DualHashBidiMap();
        res.put(EnumSensOuv.OUV_VERS_HAUT, "OuvVersHaut");
        res.put(EnumSensOuv.OUV_VERS_BAS, "OuvVersBas");
        return res;
    }

    /**
     * @param enumClass la classe de l'enum
     * @param analyze   l'analyze
     * @return le converter pour EnumFormulePdc
     */
    public static SingleValueConverter createEnumConverter(final Class enumClass, final CtuluLog analyze) {
        return createEnumConverter(CRUE10_CONVERTER, enumClass, analyze);

    }

    /**
     * @param enumClass la classe de l'enum
     * @param analyze   l'analyze
     * @return le converter pour l'enum enumClass pour Crue9
     */
    public static SingleValueConverter createCrue9EnumConverter(final Class enumClass, final CtuluLog analyze) {
        return createEnumConverter(CRUE9_CONVERTER, enumClass, analyze);

    }

    private static SingleValueConverter createEnumConverter(final Map<Class, DualHashBidiMap> map, final Class enumClass,
                                                            final CtuluLog analyze) {
        return new DefaultEnumConverter(map.get(enumClass), enumClass, analyze);

    }

    public static SingleValueConverter createEnumConverter(final DualHashBidiMap map, final Class enumClass,
                                                           final CtuluLog analyze) {
        return new DefaultEnumConverter(map, enumClass, analyze);

    }

}
