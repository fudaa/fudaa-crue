/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.opti;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.common.CrueHelper;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EnumMethodeInterpol;

/**
 * Structures de CrueDaoOPTI.
 *
 * @author Adrien Hadoux
 */
public class CrueDaoStructureOPTI implements CrueDataDaoStructure {

  private final boolean ignoreSortieAndRegles;
  private final boolean ignoreTolStQ;

  public CrueDaoStructureOPTI(final boolean ignoreSortieAndRegles, final boolean ignoreTolStQ) {
    super();
    this.ignoreSortieAndRegles = ignoreSortieAndRegles;
    this.ignoreTolStQ = ignoreTolStQ;
  }

  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    xstream.alias(CrueFileType.OPTI.toString(), CrueDaoOPTI.class);
    xstream.alias("InterpolLineaire", InterpolLineaire.class);
    xstream.alias("InterpolZimpAuxSections", InterpolZimpAuxSections.class);
    xstream.alias("InterpolBaignoire", InterpolBaignoire.class);
    xstream.alias("InterpolSaintVenant", InterpolSaintVenant.class);
    xstream.alias("RegleQbrUniforme", RegleQbrUniforme.class);
    xstream.alias("RegleQnd", RegleQnd.class);
    if (ignoreTolStQ) {
      xstream.omitField(InterpolSaintVenant.class, "Pm_TolStQ");
    }
    if (ignoreSortieAndRegles) {
      xstream.omitField(CrueDaoOPTI.class, "Sorties");
      xstream.omitField(CrueDaoOPTI.class, "Regles");
    } else {
      CrueHelper.configureSorties(xstream, ctuluLog);
    }
  }

  /**
   * methode interpolation
   *
   * @author Adrien Hadoux
   */
  public interface MethodesInterpolations {

    /**
     * retoourne le type de la methode
     *
     * @return
     */
    EnumMethodeInterpol getType();

  }

  /**
   * methode InterpolLineaire
   *
   * @author Adrien Hadoux
   */
  public static class InterpolLineaire implements MethodesInterpolations {

    @Override
    public EnumMethodeInterpol getType() {
      return EnumMethodeInterpol.LINEAIRE;
    }

  }

  /**
   * @author deniger
   */
  public static class InterpolZimpAuxSections implements MethodesInterpolations {

    @Override
    public EnumMethodeInterpol getType() {
      return EnumMethodeInterpol.INTERPOL_ZIMP_AUX_SECTIONS;
    }

  }

  /**
   * methode InterpolLineaire
   *
   * @author Adrien Hadoux
   */
  public static class InterpolBaignoire implements MethodesInterpolations {

    @Override
    public EnumMethodeInterpol getType() {
      return EnumMethodeInterpol.BAIGNOIRE;
    }

  }

  /**
   * methode InterpolSaintVenant
   *
   * @author Adrien Hadoux
   */
  public static class InterpolSaintVenant implements MethodesInterpolations {

    double Pm_TolNdZ;
    double Pm_TolStQ;

    public InterpolSaintVenant() {
      super();
    }

    @Override
    public EnumMethodeInterpol getType() {
      return EnumMethodeInterpol.SAINT_VENANT;
    }

  }

  public static abstract class RegleOPTIDAO {

    boolean IsActive;

    public abstract double getValue();

    public abstract void setValue(double v);
  }

  public static class RegleQbrUniforme extends RegleOPTIDAO {

    double Pm_QbrUniforme;

    @Override
    public double getValue() {
      return Pm_QbrUniforme;
    }

    @Override
    public void setValue(final double v) {
      Pm_QbrUniforme = v;
    }
  }

  public static class RegleQnd extends RegleOPTIDAO {

    double Pm_Qnd;

    @Override
    public double getValue() {
      return Pm_Qnd;
    }

    @Override
    public void setValue(final double v) {
      Pm_Qnd = v;
    }
  }
}
