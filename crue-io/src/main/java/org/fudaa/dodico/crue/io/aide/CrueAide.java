/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fudaa.dodico.crue.io.aide;

/**
 *
 * @author landrodie
 */
public class CrueAide {
    
    /** Id du lien interne FudaaCrue **/
    public String IdFudaaCrue;
    /** Id de l'aide en ligne  **/
    public String Help;

    /**
     * Constructeur CrueAide
     * @param idFudaaCrue
     * @param Help 
     */
    public CrueAide(String idFudaaCrue, String Help) {
        this.IdFudaaCrue = idFudaaCrue;
        this.Help = Help;
    }

    /**
     * @return the IdFudaaCrue
     */
    public String getIdFudaaCrue() {
        return IdFudaaCrue;
    }

    /**
     * @param idFudaaCrue the IdFudaaCrue to set
     */
    public void setIdFudaaCrue(String idFudaaCrue) {
        this.IdFudaaCrue = idFudaaCrue;
    }

    /**
     * @return the help
     */
    public String getHelp() {
        return Help;
    }

    /**
     * @param help the help to set
     */
    public void setHelp(String help) {
        this.Help = help;
    }
}
