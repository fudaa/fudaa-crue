/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dpti;

import com.thoughtworks.xstream.XStream;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.common.EnumsConverter;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.cini.CiniImportKey;

import java.util.Map;

/**
 * Toutes les structures dao utilisees et les inits du parser xml pour le format DPTI. Toutes ces structures sont
 * necessaires pour bien formatter le contenu xml. Ici il n'y a pas besoin de definir de sttructures supplementaires.
 *
 * @author Adrien Hadoux
 */
@SuppressWarnings("PMD.VariableNamingConventions")
public class CrueDaoStructureDPTI implements CrueDataDaoStructure {
  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    CrueDaoStructureDPTI.initXmlParserForDPTI(xstream);
  }

  public static void initXmlParserForDPTI(final XStream xstream) {
    // -- creation des alias pour que ce soit + parlant dans le xml file --//
    xstream.alias(CrueFileType.DPTI.toString(), CrueDaoDPTI.class);
    // -- liste des initialisations necessaires pour le formattage des donnees--//
    initXmlParserNode(xstream);
    initXmlParserBranche(xstream);
    initXmlParserCasier(xstream);
    initXmlParserSection(xstream);
  }

  /**
   * init parser pour les nodes et pouvoir mettre l'attribut Nom en debut de balise.
   *
   * @param xstream
   */
  public static void initXmlParserNode(final XStream xstream) {
    // -- alias gestion pour les noeuds --//
    xstream.alias("DonPrtCIniNoeudNiveauContinu", NoeudNiveauContinu.class);
    // -- mettre attribut Nom comme attribut de la balise Noeud et son converter approprie --//
    // -- creation des alias pour que ce soit + parlant dans le xml file --//
    xstream.useAttributeFor(NoeudNiveauContinu.class, "NomRef");
  }

  /**
   * Init le parser avec les infos des branches.
   *
   * @param xstream
   */
  public static void initXmlParserBranche(final XStream xstream) {
    // -- gestion des branches --//
    xstream.alias("DonPrtCIniBrancheSaintVenant", BrancheSaintVenant.class);
    xstream.alias("DonPrtCIniBrancheOrifice", BrancheOrifice.class);

    xstream.alias("DonPrtCIniBranche", BrancheBarrageGenerique.class);
    xstream.alias("DonPrtCIniBranche", BrancheSeuilLateral.class);
    xstream.alias("DonPrtCIniBranche", BrancheStrickler.class);
    xstream.alias("DonPrtCIniBranche", BrancheSeuilTransversal.class);
    xstream.alias("DonPrtCIniBranche", BrancheBarrageFilEau.class);
    xstream.alias("DonPrtCIniBranche", BrancheNiveauxAssocies.class);
    xstream.alias("DonPrtCIniBranche", BranchePdc.class);

    // -- mettre attribut Nom comme attribut de la balise branche,noeud et section et son converter approprie --//
    // -- mettre le nom de la branche dans la balise --//
    xstream.useAttributeFor(BrancheAbstract.class, "NomRef");
  }

  /**
   * Init le parser avec les infos des casiers.
   *
   * @param xstream
   */
  public static void initXmlParserCasier(final XStream xstream) {
    xstream.alias("DonPrtCIniCasierProfil", CasierProfil.class);
    xstream.alias("CasierMNT", CasierMNT.class);
    xstream.useAttributeFor(CasierAbstract.class, "NomRef");
  }

  /**
   * Init le parser avec les infos des sections.
   *
   * @param xstream
   */
  public static void initXmlParserSection(final XStream xstream) {
    xstream.alias("DonPrtCIniSection", SectionAbstract.class);
    xstream.alias("DonPrtCIniSection", SectionRefInterpolee.class);
    xstream.alias("DonPrtCIniSection", SectionRefProfil.class);
    xstream.alias("DonPrtCIniSection", SectionRefSansGeometrie.class);
    xstream.alias("DonPrtCIniSection", SectionRefPilote.class);
    xstream.alias("DonPrtCIniSection", SectionRefIdem.class);
    xstream.useAttributeFor(SectionAbstract.class, "NomRef");
  }

  protected static class NoeudNiveauContinu {
    protected String NomRef;
    protected Double Zini;

    public void fill(final Map<CiniImportKey, Object> res) {
      res.put(new CiniImportKey(NomRef.toUpperCase(), "ZINI"), Zini);
    }
  }

  // *************************GESTION DES BRANCHES *************************

  /**
   * Element branche possible
   *
   * @author Adrien Hadoux
   */
  protected static class BrancheAbstract {
    protected String NomRef;
    protected Double Qini;

    public void fill(final Map<CiniImportKey, Object> res) {
      res.put(new CiniImportKey(NomRef.toUpperCase(), "QINI"), Qini);
    }
  }

  protected static class BrancheSaintVenant extends BrancheAbstract {
    protected Double Qruis;

    @Override
    public void fill(final Map<CiniImportKey, Object> res) {
      super.fill(res);
      res.put(new CiniImportKey(NomRef.toUpperCase(), "QRUIS"), Qruis);
    }
  }

  protected static class BrancheSeuilLateral extends BrancheAbstract {
  }

  protected static class BrancheStrickler extends BrancheAbstract {
  }

  protected static class BrancheOrifice extends BrancheAbstract {
    private static final DualHashBidiMap enumSensOuvMap = EnumsConverter.createEnumSensOuvMap();
    protected Double Ouv;
    protected String SensOuv;

    @Override
    public void fill(final Map<CiniImportKey, Object> res) {
      super.fill(res);
      res.put(new CiniImportKey(NomRef.toUpperCase(), "OUV"), Ouv);
      final Object key = enumSensOuvMap.getKey(SensOuv);
      if (key != null) {
        res.put(new CiniImportKey(NomRef.toUpperCase(), "SENSOUV"), key);
      }
    }
  }

  protected static class BrancheSeuilTransversal extends BrancheAbstract {
  }

  protected static class BrancheBarrageFilEau extends BrancheAbstract {
  }

  protected static class BranchePdc extends BrancheAbstract {
  }

  protected static class BrancheBarrageGenerique extends BrancheAbstract {
  }

  protected static class BrancheBarrageRhone extends BrancheAbstract {
  }

  protected static class BrancheEnchainement extends BrancheAbstract {
  }

  protected static class BrancheNiveauxAssocies extends BrancheAbstract {
  }

  // *************************GESTION DES CASIERS *************************
  protected static class CasierAbstract {
    protected String NomRef;
    protected Double Qruis;

    public void fill(final Map<CiniImportKey, Object> res) {
      res.put(new CiniImportKey(NomRef.toUpperCase(), "QRUIS"), Qruis);
    }
  }

  protected static class CasierProfil extends CasierAbstract {
  }

  protected static class CasierMNT extends CasierAbstract {
  }

  // *************************GESTION DES SECTIONS *************************
  protected static class SectionAbstract {
    protected String NomRef;
    protected Double Zini;

    public void fill(final Map<CiniImportKey, Object> res) {
      res.put(new CiniImportKey(NomRef.toUpperCase(), "ZINI"), Zini);
    }
  }

  protected static class SectionRefIdem extends SectionAbstract {
  }

  protected static class SectionRefInterpolee extends SectionAbstract {
  }

  protected static class SectionRefProfil extends SectionAbstract {
  }

  protected static class SectionRefSansGeometrie extends SectionAbstract {
  }

  protected static class SectionRefPilote extends SectionAbstract {
  }
}
