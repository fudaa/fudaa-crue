/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import java.util.List;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.dfrt.CrueConverterDFRT;
import org.fudaa.dodico.crue.io.dfrt.CrueDaoStructureDFRT;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.DonFrt;

public class CrueFileFormatBuilderDFRT implements CrueFileFormatBuilder<List<DonFrt>> {

  @Override
  public Crue10FileFormat<List<DonFrt>> getFileFormat(final CoeurConfigContrat version) {
    return new Crue10FileFormat<>(new CrueDataXmlReaderWriterImpl<>(CrueFileType.DFRT,
        version, new CrueConverterDFRT(), new CrueDaoStructureDFRT(version.getXsdVersion())));
  }

}
