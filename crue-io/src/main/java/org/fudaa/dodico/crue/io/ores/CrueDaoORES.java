/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores;

import org.fudaa.dodico.crue.common.io.AbstractCrueDao;

/**
 * Classe persistante qui reprend la meme structure que le fichier XML ORES - Fichier des donnees portant sur les demandes de resultats
 * supplementaires d'un modele crue10. A persister tel quel.
 *
 * @author cde
 */
public class CrueDaoORES extends AbstractCrueDao {

  public CrueDaoStructureORES.DaoOrdResNoeuds OrdResNoeuds;
  public CrueDaoStructureORES.DaoOrdResCasiers OrdResCasiers;
  public CrueDaoStructureORES.DaoOrdResSections OrdResSections;
  public CrueDaoStructureORES.DaoOrdResBranches OrdResBranches;

  public CrueDaoStructureORES.DaOrdResModeles OrdResModeles;
}
