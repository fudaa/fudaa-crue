/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.lhpt;

import com.thoughtworks.xstream.XStream;
import org.fudaa.dodico.crue.config.loi.LoiTypeContainer;
import org.fudaa.dodico.crue.io.common.AbstractDaoLoi;
import org.fudaa.dodico.crue.io.common.EnumTypeLoiConverter;
import org.fudaa.dodico.crue.metier.emh.*;

/**
 * Classe abstraite qui permet de factoriser les attributs communs de toutes les lois
 */
public class DaoLoiTF extends AbstractDaoLoi {

    protected EvolutionTF EvolutionTF;


    private static final  SingleConverterPointTF POINT_TF = new SingleConverterPointTF();

    public static void configureXstream(final XStream xstream, final LoiTypeContainer data, final String version) {
        xstream.alias("EvolutionTF", EvolutionTF.class);
        xstream.alias("PointTF", PtEvolutionTF.class);
        xstream.useAttributeFor(org.fudaa.dodico.crue.io.lhpt.DaoLoiTF.class, "Type");
        xstream.useAttributeFor(org.fudaa.dodico.crue.io.lhpt.DaoLoiTF.class, "Nom");
        xstream.registerConverter(POINT_TF);
        xstream.registerConverter(new EnumTypeLoiConverter(data));

        xstream.addImplicitCollection(EvolutionTF.class, "mpoints");
    }


}
