/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import gnu.trove.TDoubleArrayList;
import gnu.trove.TObjectIntHashMap;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.fileformat.FortranInterface;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.config.ccm.SeveriteManager;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.comparator.InfoEMHByUserPositionComparator;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPerm;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPermBrancheOrificeManoeuvre;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPermBrancheSaintVenantQruis;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPermCasierProfilQruis;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPermNoeudNiveauContinuZimp;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPermNoeudQapp;
import org.fudaa.dodico.crue.metier.emh.CalcTrans;
import org.fudaa.dodico.crue.metier.emh.CalcTransBrancheOrificeManoeuvre;
import org.fudaa.dodico.crue.metier.emh.CalcTransBrancheSaintVenantQruis;
import org.fudaa.dodico.crue.metier.emh.CalcTransItem;
import org.fudaa.dodico.crue.metier.emh.CalcTransNoeudNiveauContinuLimnigramme;
import org.fudaa.dodico.crue.metier.emh.CalcTransNoeudNiveauContinuTarage;
import org.fudaa.dodico.crue.metier.emh.CalcTransNoeudQapp;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.emh.DonCalcSansPrtCasierProfil;
import org.fudaa.dodico.crue.metier.emh.DonPrtCIni;
import org.fudaa.dodico.crue.metier.emh.DonPrtCIniBranche;
import org.fudaa.dodico.crue.metier.emh.DonPrtCIniNoeudNiveauContinu;
import org.fudaa.dodico.crue.metier.emh.DonPrtCIniSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.emh.ElemPdt;
import org.fudaa.dodico.crue.metier.emh.EnumMethodeInterpol;
import org.fudaa.dodico.crue.metier.emh.EnumRegleInterpolation;
import org.fudaa.dodico.crue.metier.emh.EnumSensOuv;
import org.fudaa.dodico.crue.metier.emh.EvolutionFF;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.LoiDF;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.emh.OrdCalc;
import org.fudaa.dodico.crue.metier.emh.OrdCalcScenario;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTrans;
import org.fudaa.dodico.crue.metier.emh.OrdPrtCIniModeleBase;
import org.fudaa.dodico.crue.metier.emh.ParamCalcScenario;
import org.fudaa.dodico.crue.metier.emh.ParamNumCalcPseudoPerm;
import org.fudaa.dodico.crue.metier.emh.ParamNumCalcTrans;
import org.fudaa.dodico.crue.metier.emh.ParamNumModeleBase;
import org.fudaa.dodico.crue.metier.emh.Pdt;
import org.fudaa.dodico.crue.metier.emh.PdtCst;
import org.fudaa.dodico.crue.metier.emh.PdtVar;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.emh.ValParam;
import org.fudaa.dodico.crue.metier.emh.ValParamDouble;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.joda.time.Duration;
import org.joda.time.LocalDateTime;

/**
 * Writer de la structure DH.
 *
 * @author Adrien Hadoux
 */
public class DHWriter extends AbstractCrue9Writer implements CtuluActivity {

  private double tolndz;

  @Override
  protected void internalWrite(final CrueIOResu o) {

    initWriter();

    final CrueData data = ((CrueIOResu<CrueData>) o).getMetier();
    tolndz = getTolNdZDefaultValue(data);
    comparator = new InfoEMHByUserPositionComparator(data.getScenarioData());

    try {

      final Map<String, Double> coeffsRuisParCalcPerm = new HashMap<>();
      writeDonneesGenerales((CrueIOResu<CrueData>) o, coeffsRuisParCalcPerm);
      if (aDonneesPerm) {
        writeDonneesPermanentes(data, coeffsRuisParCalcPerm);
      }
      if (aDonneesTrans) {
        writeDonneesTransitoires(data);
      }

    } catch (final IOException e) {
      analyze_.manageException(e);
    } finally {
      try {
        // -- Fermeture du flux.
        fortranWriter.close();
      } catch (final IOException e) {
        analyze_.manageException(e);
      }
    }
  }

  @Override
  protected FortranInterface getFortranInterface() {
    return fortranWriter;
  }
  private boolean aDonneesPerm = false;
  private boolean aDonneesTrans = false;

  private boolean isModeLateral(final CrueData data) {
    final PropertyEpsilon epsForCoefRus = data.getCrueConfigMetier().getEpsilon("coefRuis");
    final List<EMH> allSimpleEMH = data.getAllSimpleEMH();
    for (final EMH emh : allSimpleEMH) {
      final List<DonCalcSansPrtBrancheSaintVenant> dcsp = EMHHelper.selectClass(emh.getInfosEMH(),
              DonCalcSansPrtBrancheSaintVenant.class);
      if (dcsp != null) {
        for (final DonCalcSansPrtBrancheSaintVenant dcspItem : dcsp) {
          if (!epsForCoefRus.isZero(dcspItem.getCoefRuis())) {
            return true;
          }
        }
      }
      final List<DonCalcSansPrtCasierProfil> dcspCasierProfil = EMHHelper.selectClass(emh.getInfosEMH(),
              DonCalcSansPrtCasierProfil.class);
      if (dcsp != null) {
        for (final DonCalcSansPrtCasierProfil dcspItem : dcspCasierProfil) {
          if (!epsForCoefRus.isZero(dcspItem.getCoefRuis())) {
            return true;
          }
        }
      }

    }
    return false;

  }

  /**
   * @param data
   * @throws IOException
   */
  private void writeDonneesGenerales(final CrueIOResu<CrueData> ioData, final Map coeffsRuisParCalcPerm)
          throws IOException {

    writeCom("");
    writeCom(" Donnees generales ");

    // Ecriture ligne A : titres sur 5 lignes
    super.writeHeader(ioData, false);
    final CrueData data = ioData.getMetier();
    final int ile = getILE(data);
    writeRegle(data, ile);

    writeCom("");
    writeCom(" ical cru cofqrq isorti ile trepr ");

    // Ecriture ligne B
    int ical = -1;
    double cru = 0;
    int cofqrq = 0;

    String isorti = "0";


    // ical

    // Si au moins une branche a un DPTI, cela signifie qu'on a une ligne C dans les données générales
    final List<CatEMHBranche> branches = EMHHelper.selectEMHUserActivated(data.getBranches());
    final boolean aDonneesGen = containsDPTI(branches);

    // On a des données permanentes si au moins un calcul permanent existe dans OCAL
    // On a des données transitoires si au moins un calcul transitoire existe dans OCAL
    final OrdCalcScenario ocal = data.getOCAL();
    boolean modru = false;
    if (ocal != null) {
      final List<OrdCalc> ordCalc = ocal.getOrdCalc();
      aDonneesPerm = EMHHelper.containsPermanent(ordCalc);
      aDonneesTrans = EMHHelper.containsTransitoire(ordCalc);
      final boolean modeLateral = isModeLateral(data);
      // cru
      if (modeLateral
              && !computeQruisIdentique(coeffsRuisParCalcPerm, ordCalc, data.getCrueConfigMetier())) {
        return;
      }

      // S'il existe au moins un débit de ruissellement pour un calcul permanent on met cru à 1/idem pour cofqrq
      if (modeLateral && MapUtils.isNotEmpty(coeffsRuisParCalcPerm)) {
        modru = true;
        cru = computeQruisQDM(data);
        // si le coef coef QruisQDM n'est pas constant on ne peut pas ecrire au format crue 9
        if (analyze_.containsSevereError()) {
          return;
        }

        cofqrq = 1;
      }
      // Valeur par défaut pour l'écriture (on ne tient pas compte de la valeur à la lecture de DH)

    }

    if (aDonneesGen && aDonneesPerm && aDonneesTrans) {
      ical = 2;
    } else if (aDonneesGen && aDonneesPerm) {
      ical = 1;
    } else if (aDonneesTrans) {
      ical = 0;
    } else {
      ical = 1;
      analyze_.addErrorFromFile("io.dh.ical.error", fortranWriter.getLineNumber());
    }
    if (ical == 0) {
      analyze_.addSevereError("repriseDh.error");
      return;
    }

    final boolean use1 = data.getOCAL().getSorties().containsVerbositeStrictlyMoreThan(SeveriteManager.INFO, data.getCrueConfigMetier());
    // isorti
    isorti = (use1 ? "1" : "0");

    // ile

    // S'il existe au moins un noeud avec un DPTI de type DonPrtCIniNoeudNiveauContinu c'est qu'il y a au moins un ligne
    // F1 et donc que ile vaut 1

    final List<CatEMHNoeud> noeuds = data.getNoeuds();

    if (ile == -1) {
      analyze_.addErrorFromFile("io.dh.ile.error", fortranWriter.getLineNumber());
    }

    // trepr

    fortranWriter.intField(0, ical);
    fortranWriter.doubleField(1, cru);
    fortranWriter.intField(2, cofqrq);
    fortranWriter.stringField(3, isorti);
    fortranWriter.intField(4, ile);
    fortranWriter.stringField(5, DateDurationConverter.CRUE_ZERO);
    fortranWriter.writeFields();
    if (modru) {
      fortranWriter.stringField(0, CrueIODico.DH_MODRU);
      fortranWriter.writeFields();
    }

    writeCom("");
    writeCom(" Debits initiaux dans les branches ");

    // Ecriture des lignes C
    final List<CatEMHBranche> branchesDPTI = new ArrayList<>();
    int countNbChampWritten = 0;
    int nbCaractWritten = 0;
    for (final CatEMHBranche br : branches) {
      final List<DonPrtCIniBranche> dptis = EMHHelper.selectInstanceOf(br.getInfosEMH(), DonPrtCIniBranche.class);
      if (dptis != null) {
        for (final DonPrtCIniBranche dpti : dptis) {

          branchesDPTI.add(br);
          final String nom = br.getNom();
          final String valeur = Double.toString(dpti.getQini());
          // Ajout de la longueur des espaces ajoutés à la chaîne à écrire
          final int nbCaractToWrite = 1 + nom.length() + 1 + valeur.length();
          // On revient à la ligne si ce qui est à écrire dépasse la taille max de la ligne
          if (countNbChampWritten >= 8 || (nbCaractWritten + nbCaractToWrite > tailleMaxLine)) {
            fortranWriter.writeFields();
            nbCaractWritten = 0;
            countNbChampWritten = 0;
          }
          fortranWriter.stringField(countNbChampWritten++, nom);
          fortranWriter.stringField(countNbChampWritten++, valeur);
          nbCaractWritten += nbCaractToWrite;
        }
      }
    }
    fortranWriter.writeFields();

    // Ecriture de la ligne D
    writeFin();
    if (modru) {
      final List<EMHBrancheSaintVenant> branchesSaintVenant = EMHHelper.selectClass(branches,
              EMHBrancheSaintVenant.class);
      if (branchesSaintVenant != null) {
        countNbChampWritten = 0;
        nbCaractWritten = 0;
        for (final EMHBrancheSaintVenant brSV : branchesSaintVenant) {
          final List<DonCalcSansPrtBrancheSaintVenant> stVt = EMHHelper.selectClass(brSV.getInfosEMH(),
                  DonCalcSansPrtBrancheSaintVenant.class);
          if (stVt.size() == 1) {
            final String nom = brSV.getNom();
            final String valeur = Double.toString(stVt.get(0).getCoefRuis());
            final int nbCaractToWrite = 1 + nom.length() + 1 + valeur.length();
            if (countNbChampWritten >= 8 || (nbCaractWritten + nbCaractToWrite > tailleMaxLine)) {
              fortranWriter.writeFields();
              nbCaractWritten = 0;
              countNbChampWritten = 0;
            }
            fortranWriter.stringField(countNbChampWritten++, nom);
            fortranWriter.stringField(countNbChampWritten++, valeur);
            nbCaractWritten += nbCaractToWrite;
          }

        }
        fortranWriter.writeFields();
      }
      writeFin();
    }

    if (ile == 0) {

      // Ecriture lignes E
      writeCom("");
      writeCom(" Niveaux initiaux aux branches / Sections ");

      // Ecriture ligne E1
      writeCom(" ligne ");

      // Ecriture lignes E2
      for (int i = 0, branchesDptiSize = branchesDPTI.size(); i < branchesDptiSize; i++) {
        fortranWriter.intField(0, 1);
        fortranWriter.writeFields();
        final CatEMHBranche brancheDPTI = branchesDPTI.get(i);
        final List<RelationEMHSectionDansBranche> sectionsBrancheDPTI = brancheDPTI.getListeSections();
        final List<DonPrtCIni> dptis = brancheDPTI.getDPTI();

        if (sectionsBrancheDPTI == null || dptis == null) {
          analyze_.addInfoFromFile("io.dh.sections.dpti.error", fortranWriter.getLineNumber(), brancheDPTI.getNom());
        } else {

          nbCaractWritten = 0;

          final int countSectionsBranche = sectionsBrancheDPTI.size();
          int idxOnLine = 0;
          for (int j = 0; j < countSectionsBranche; j++) {

            final DonPrtCIniSection dpti = EMHHelper.selectFirstOfClass(sectionsBrancheDPTI.get(j).getEmh().getInfosEMH(), DonPrtCIniSection.class);
            double zIniPourProfilJ = 0;
            if (dpti != null) {
              zIniPourProfilJ = dpti.getZini();
            }

            // Ajout de la longueur de l'espace à la chaine à écrire
            final int nbCaractToWrite = 1 + Double.toString(zIniPourProfilJ).length();
            // On envoie une erreur si ce qui est à écrire dépasse la taille max de la ligne car toutes les valeurs de
            // sections d'une branche doivent tenir sur une seule ligne. On s'occupe alors des sections de la branche
            // suivante
            if (idxOnLine >= 8 || nbCaractWritten + nbCaractToWrite > tailleMaxLine) {
              fortranWriter.writeFields();
              nbCaractWritten = 0;
              idxOnLine = 0;
            }

            fortranWriter.doubleField(idxOnLine++, zIniPourProfilJ);
            nbCaractWritten += nbCaractToWrite;
          }
          fortranWriter.writeFields();
        }
      }
      // sinon niveau initiaux aux noeuds.
    } else {

      // Ecriture lignes F
      writeCom("");
      writeCom(" Niveaux initiaux aux noeuds ");

      // Ecriture des lignes F1
      int nbBlocWritten = 0;
      nbCaractWritten = 0;
      for (final CatEMHNoeud noeud : noeuds) {
        final List<DonPrtCIni> dptis = noeud.getDPTI();

        if (dptis != null) {

          for (int j = 0, dptisSize = dptis.size(); j < dptisSize; j++) {
            if (!(dptis.get(j) instanceof DonPrtCIniNoeudNiveauContinu)) {
              continue;
            }
            final String nom = noeud.getNom();
            final String valeur = Double.toString(((DonPrtCIniNoeudNiveauContinu) dptis.get(j)).getZini());
            // On ajoute la longueur de chaque espace à la chaine à écrire
            final int nbCaractToWrite = 1 + nom.length() + 1 + valeur.length();
            // On revient à la ligne si ce qui est à écrire dépasse la taille max de la ligne
            if (nbBlocWritten >= 8 || (nbCaractWritten + nbCaractToWrite > tailleMaxLine)) {
              fortranWriter.writeFields();
              nbCaractWritten = 0;
              nbBlocWritten = 0;
            }
            fortranWriter.stringField(nbBlocWritten++, nom);
            fortranWriter.stringField(nbBlocWritten++, valeur);
            nbCaractWritten += nbCaractToWrite;
          }
        }
      }
      fortranWriter.writeFields();

      // Ecriture de la ligne F2
      writeFin();
    }
    if (modru) {
      int nbBlocWritten = 0;
      nbCaractWritten = 0;
      for (final CatEMHNoeud noeud : noeuds) {
        final CatEMHCasier casier = noeud.getCasier();
        if (casier == null || !casier.getUserActive()) {
          continue;
        }
        final List<DonCalcSansPrtCasierProfil> dcsps = EMHHelper.selectClass(casier.getInfosEMH(),
                DonCalcSansPrtCasierProfil.class);
        if (dcsps != null && dcsps.size() == 1) {
          final DonCalcSansPrtCasierProfil dcsp = dcsps.get(0);
          final String nom = noeud.getNom();
          final String valeur = Double.toString(dcsp.getCoefRuis());
          // On ajoute la longueur de chaque espace à la chaine à écrire
          final int nbCaractToWrite = 1 + nom.length() + 1 + valeur.length();
          // On revient à la ligne si ce qui est à écrire dépasse la taille max de la ligne
          if (nbBlocWritten >= 8 || (nbCaractWritten + nbCaractToWrite > tailleMaxLine)) {
            fortranWriter.writeFields();
            nbCaractWritten = 0;
            nbBlocWritten = 0;
          }
          fortranWriter.stringField(nbBlocWritten++, nom);
          fortranWriter.stringField(nbBlocWritten++, valeur);
          nbCaractWritten += nbCaractToWrite;
        }
      }
      fortranWriter.writeFields();
      writeFin();
    }
  }

  private boolean containsDPTI(final List<CatEMHBranche> branches) {
    if (branches == null) {
      return false;
    }
    final int nb = branches.size();
    for (int i = 0; i < nb; i++) {
      final List<DonPrtCIni> dptis = branches.get(i).getDPTI();
      if (dptis != null) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param data
   * @param ile
   * @throws IOException
   */
  private void writeRegle(final CrueData data, final int ile) throws IOException {
    // FrLinInf et FrLinSup
    final ParamNumModeleBase pnum = data.getPNUM();
    final double frLinInf = pnum.getFrLinInf();
    final double frLinSup = pnum.getFrLinSup();
    // les valeurs par defaut: infini: on n'ecrit pas si les 2 valeurs sont par defaut
    writeCarteValeur(data.getProperty("frLinInf"), CrueIODico.DH_SEUIL_FROUDE, frLinInf, frLinSup);
    double crMaxFlu = data.getCrueConfigMetier().getDefaultDoubleValue(CrueConfigMetierConstants.PROP_CR_MAX_FLU);
    double crMaxTor = data.getCrueConfigMetier().getDefaultDoubleValue(CrueConfigMetierConstants.PROP_CR_MAX_TOR);
    final ParamNumCalcPseudoPerm pseudoPerm = pnum.getParamNumCalcPseudoPerm();
    if (pseudoPerm != null) {
      final double coefQ = pseudoPerm.getCoefRelaxQ();
      // on devrait comparer avec
      writeCarteValeur(data.getProperty("coefRelaxQ"), CrueIODico.DH_COEFF_RELAX_DQ, coefQ);
      // }
      final double coefZ = pseudoPerm.getCoefRelaxZ();
      writeCarteValeur(data.getProperty("coefRelaxZ"), CrueIODico.DH_COEFF_RELAX_DZ, coefZ);
      // }
      crMaxFlu = pseudoPerm.getCrMaxFlu();
      crMaxTor = pseudoPerm.getCrMaxTor();
    }

    final ParamNumCalcTrans calcTrans = pnum.getParamNumCalcTrans();
    if (calcTrans != null) {
      final double crMaxFluTrans = calcTrans.getCrMaxFlu();
      final double crMaxTorTrans = calcTrans.getCrMaxTor();
      // penible: que faire si valeurs différentes ?
      if (pseudoPerm != null) {
        if (!CtuluLib.isEquals(crMaxFlu, crMaxFluTrans)) {
          analyze_.addWarn("dh.coeff.diff.trans.perm", "CrMaxFlu");
        }
        if (!CtuluLib.isEquals(crMaxTor, crMaxTorTrans)) {
          analyze_.addWarn("dh.coeff.diff.trans.perm", "CrMaxTor");
        }
      }
      crMaxFlu = crMaxFluTrans;
      crMaxTor = crMaxTorTrans;
    }
    writeCarteValeur(data.getProperty("crMaxFlu"), CrueIODico.DH_NB_COURANT_MAX, crMaxFlu, crMaxTor);
    if (ile == 2) {
      writeCarteValeur(data.getProperty("pm_TolNdZ"), CrueIODico.TOL_Z_INIT, tolndz);
    }

  }

  private int getILE(final CrueData data) {
    final OrdPrtCIniModeleBase methodesInterpolation = data.getOPTI();
    int calcILE = 0;
    if (methodesInterpolation.getMethodeInterpol() == EnumMethodeInterpol.LINEAIRE) {
      calcILE = 1;
    }
    if (methodesInterpolation.getMethodeInterpol() == EnumMethodeInterpol.INTERPOL_ZIMP_AUX_SECTIONS) {
      calcILE = 0;
    } else if (methodesInterpolation.getMethodeInterpol() == EnumMethodeInterpol.BAIGNOIRE) {
      calcILE = 3;
    } else if (methodesInterpolation.getMethodeInterpol() == EnumMethodeInterpol.SAINT_VENANT) {
      calcILE = 2;
      tolndz = getTolNdz(methodesInterpolation, data);
    }
    return calcILE;
  }

  private double getTolNdz(final OrdPrtCIniModeleBase methodesInterpolation, final CrueData data) {
    final Collection<ValParam> listValParam = methodesInterpolation.getValParam();
    if (listValParam != null) {
      final String key = EnumRegleInterpolation.TOL_ND_Z.getVariableName().toUpperCase();
      for (final ValParam valParam : listValParam) {
        if (key.equals(valParam.getId())) {
          return ((ValParamDouble) valParam).getValeur();
        }
      }
    }
    return getTolNdZDefaultValue(data);
  }

  private double getTolNdZDefaultValue(final CrueData data) {
    return data.getCrueConfigMetier().getDefaultDoubleValue(
            StringUtils.uncapitalize(EnumRegleInterpolation.TOL_ND_Z.getVariableName()));
  }

  private double computeQruisQDM(final CrueData data) {
    final List<EMHBrancheSaintVenant> branchesSaintVenant = data.getBranchesSaintVenant();
    for (final EMHBrancheSaintVenant brancheSaintVenant : branchesSaintVenant) {
      if (brancheSaintVenant.getUserActive()) {
        final List<DonCalcSansPrtBrancheSaintVenant> dcsps = EMHHelper.selectClass(brancheSaintVenant.getInfosEMH(),
                DonCalcSansPrtBrancheSaintVenant.class);
        if (CollectionUtils.isNotEmpty(dcsps)) {
          return dcsps.get(0).getCoefRuisQdm();
        }
      }
    }
    return data.getCrueConfigMetier().getDefaultDoubleValue("coefRuisQdm");
  }

  private boolean computeQruisIdentique(final Map<String, Double> coeffsRuisParCalcPerm, final List<OrdCalc> ordCalcs,
          final CrueConfigMetier props) {
    final List<CalcPseudoPerm> calcPseudoPerm = EMHHelper.collectCalcPseudoPerm(ordCalcs);
    // Pour déterminer s'il existe un coefficient de ruissellement commun pour un calcul permanent, il faut que le
    // coefficient de chaque branche Saint Venant de ce calcul soit le même.
    for (int i = 0, imax = calcPseudoPerm.size(); i < imax; i++) {

      final CalcPseudoPerm calcPerm = calcPseudoPerm.get(i);
      // Il faut au moins une branche saint venant pour considérer qu'il y a un débit de ruissellement (identique)
      final List<CalcPseudoPermBrancheSaintVenantQruis> calcQruis = calcPerm.getDclmUserActive(CalcPseudoPermBrancheSaintVenantQruis.class);
      final List<CalcPseudoPermCasierProfilQruis> calcCasierQruis = calcPerm.getDclmUserActive(CalcPseudoPermCasierProfilQruis.class);
      if (CollectionUtils.isNotEmpty(calcQruis) || CollectionUtils.isNotEmpty(calcCasierQruis)) {
        Double d = null;
        if (CollectionUtils.isNotEmpty(calcQruis)) {
          d = calcQruis.get(0).getQruis();
        } else if (CollectionUtils.isNotEmpty(calcCasierQruis)) {
          d = calcCasierQruis.get(0).getQruis();
        }
        coeffsRuisParCalcPerm.put(calcPerm.getNom(), d == null ? Double.valueOf(props.getDefaultDoubleValue("qRuis"))
                : d);
      }
    }
    return true;
  }

  /**
   * @param data
   * @throws IOException
   */
  private void writeDonneesPermanentes(final CrueData data, final Map coeffsRuisParCalcPerm) throws IOException {

    writeCom("");
    writeCom(" Donnees pseudo-permanentes");

    // Ecriture Ligne A
    writeCom("");
    writeCom(" dtperm tolz tolq icalmx iprint ndecou ");

    Duration duree = null;
    double tolz = -1.0;
    double tolq = -1.0;
    int icalmx = -1;
    // Valeur par défaut, pas stocké à la lecture de DH
    final int iprint = 0;
    int ndecou = -1;

    final ParamNumModeleBase pnum = data.getPNUM();
    PdtVar pdtVar = null;
    if (pnum != null) {
      final ParamNumCalcPseudoPerm pseudo = pnum.getParamNumCalcPseudoPerm();
      if (pseudo != null) {
        final Pdt pdtPerm = pseudo.getPdt();
        if (pdtPerm != null) {
          if (pdtPerm instanceof PdtCst) {
            final PdtCst pdtCst = (PdtCst) pdtPerm;
            duree = pdtCst.getPdtCst();
          } else if (pdtPerm instanceof PdtVar) {
            pdtVar = (PdtVar) pdtPerm;
          }
        }
        tolz = pseudo.getTolMaxZ();
        tolq = pseudo.getTolMaxQ();
        icalmx = pseudo.getNbrPdtMax();
        ndecou = pseudo.getNbrPdtDecoup();
      }
    }
    if (duree == null) {
      duree = data.getCrueConfigMetier().getDefaultDurationValue("pdtPerm");
    }
    final String durationToCrueFormat = DateDurationConverter.durationToCrueFormat(duree);
    fortranWriter.stringField(0, durationToCrueFormat);
    fortranWriter.doubleField(1, tolz);
    fortranWriter.doubleField(2, tolq);
    fortranWriter.intField(3, icalmx);
    fortranWriter.intField(4, iprint);
    fortranWriter.intField(5, ndecou);
    fortranWriter.writeFields();

    // Ecriture Lignes B
    writeCom("");
    writeCom(" Definition des types des conditions aux limites ");

    final OrdCalcScenario ocal = data.getOCAL();
    if (ocal != null) {
      final List<CalcPseudoPerm> calcsPerms = EMHHelper.collectCalcPseudoPerm(ocal.getOrdCalc());
      if (CollectionUtils.isNotEmpty(calcsPerms)) {

        // Hashtable typesCL = new Hashtable();
        final TObjectIntHashMap typesCL = new TObjectIntHashMap();
        typesCL.put(CalcPseudoPermNoeudNiveauContinuZimp.class, 1);
        typesCL.put(CalcPseudoPermNoeudQapp.class, 2);
        typesCL.put(CalcPseudoPermBrancheOrificeManoeuvre.class, 41);

        // Stocke les valeurs des Conditions aux limites pour chaque calcul pour écrire ensuite les lignes E
        final Map<String, TDoubleArrayList> lignesE = new HashMap<>();
        final List<String> nomsCalcsPerms = new ArrayList<>();
        for (int i = 0, imax = calcsPerms.size(); i < imax; i++) {
          afficheLignesBetRecupereDonneesLignesE(calcsPerms.get(i), i, typesCL, lignesE, nomsCalcsPerms);
        }
        // Ecriture Ligne C
        writeFin();

        // Ecriture Ligne D
        if (pdtVar != null && CollectionUtils.isNotEmpty(pdtVar.getElemPdt())) {
          writeCom("");
          writeCom(" Modulation du pas de temps au cours de la procedure de stabilisation ");

          ecritLignesDPermanent(pdtVar);
        }

        // Ecriture Lignes E
        writeCom("");
        writeCom(" Valeurs des conditions aux limites (regimes pseudo-permanents) ");

        for (int i = 0, imax = nomsCalcsPerms.size(); i < imax; i++) {

          final String nomCalcPerm = nomsCalcsPerms.get(i);
          if (lignesE.containsKey(nomCalcPerm)) {
            final TDoubleArrayList valeursCP = lignesE.get(nomCalcPerm);
            final int size = valeursCP.size();
            for (int j = 0, jmax = size; j < jmax; j++) {
              fortranWriter.doubleField(j, valeursCP.get(j));
            }

            fortranWriter.writeFields();
          }
        }

        // Ecriture Ligne F
        writeFin();
      }

    }
  }
  private InfoEMHByUserPositionComparator comparator;

  /**
   * Attention, ici on suppose que le test sur la cohérence des calcul pseudo permanent a été effectuée comme c'est effectué par
   * ValidatorForCrue9Export cad tous les calculs definissent les même emhs.
   *
   * @param calcPerm
   * @param numCalcPerm
   * @param typesCL
   * @param lignesE
   * @param nomsCalcsPerms
   * @throws IOException
   */
  private void afficheLignesBetRecupereDonneesLignesE(final CalcPseudoPerm calcPerm, final int numCalcPerm,
          final TObjectIntHashMap typesCL, final Map<String, TDoubleArrayList> lignesE, final List<String> nomsCalcsPerms)
          throws IOException {

    final String nomCalcPerm = calcPerm.getNom();
    nomsCalcsPerms.add(nomCalcPerm);
    //Doit suivre l'ordre:
    //    CalcPseudoPermNoeudNiveauContinuQapp
    //    CalcPseudoPermNoeudNiveauContinuZimpose
    //    CalcPseudoPermBrancheOrificeManoeuvre
    //    CalcPseudoPermBrancheSaintVenantQruis
    //    CalcPseudoPermCasierProfilQruis
    final TDoubleArrayList valeursCP = new TDoubleArrayList();

    final List<CalcPseudoPermNoeudQapp> listeQapp = new ArrayList<>(
        calcPerm.getDclmUserActive(CalcPseudoPermNoeudQapp.class));
    Collections.sort(listeQapp, comparator);
    for (final CalcPseudoPermNoeudQapp nnc : listeQapp) {
      if (nomCalcPerm.equals(nnc.getNomCalculParent())) {

        // Tous les noeuds et branches à afficher dans les lignes B sont communs à tous les calculs permanents donc on
        // affiche que les objets EMH du 1er calcul
        if (numCalcPerm == 0) {
          fortranWriter.stringField(0, nnc.getEmh().getNom());
          final Integer typeCL = typesCL.get(nnc.getClass());
          fortranWriter.intField(1, typeCL.intValue());
          fortranWriter.writeFields();
        }
        valeursCP.add(nnc.getQapp());
      }
    }
    final List<CalcPseudoPermNoeudNiveauContinuZimp> listeZ = new ArrayList<>(
        calcPerm.getDclmUserActive(CalcPseudoPermNoeudNiveauContinuZimp.class));
    Collections.sort(listeZ, comparator);
    for (final CalcPseudoPermNoeudNiveauContinuZimp nnc : listeZ) {
      if (nomCalcPerm.equals(nnc.getNomCalculParent())) {

        // Tous les noeuds et branches à afficher dans les lignes B sont communs à tous les calculs permanents donc on
        // affiche que les objets EMH du 1er calcul
        if (numCalcPerm == 0) {
          fortranWriter.stringField(0, nnc.getEmh().getNom());
          final Integer typeCL = typesCL.get(nnc.getClass());
          fortranWriter.intField(1, typeCL.intValue());
          fortranWriter.writeFields();
        }
        valeursCP.add(nnc.getZimp());
      }
    }

    final List<CalcPseudoPermBrancheOrificeManoeuvre> listeBranchesOr = new ArrayList<>(
        calcPerm.getDclmUserActive(CalcPseudoPermBrancheOrificeManoeuvre.class));
    Collections.sort(listeBranchesOr, comparator);
    for (int i = 0, listeBranchesOrSize = listeBranchesOr.size(); i < listeBranchesOrSize; i++) {

      final CalcPseudoPermBrancheOrificeManoeuvre brancheOr = listeBranchesOr.get(i);
      if (brancheOr.getNomCalculParent() != null && brancheOr.getNomCalculParent().equals(nomCalcPerm)) {

        final CalcPseudoPermBrancheOrificeManoeuvre brancheOrMan = brancheOr;

        if (numCalcPerm == 0) {
          fortranWriter.stringField(0, brancheOr.getEmh().getNom());
          int typeCL = typesCL.get(brancheOr.getClass());
          // TypeCL = 41 pour brancheOrifice OuvVersHaut et TypeCL = 42 (41+1) pour brancheOrifice OuvVersBas
          if (EnumSensOuv.OUV_VERS_BAS.equals(brancheOrMan.getSensOuv())) {
            typeCL = typeCL + 1;
          }
          fortranWriter.intField(1, typeCL);
          fortranWriter.writeFields();
        }

        valeursCP.add(brancheOrMan.getOuv());
      }
    }
    final List<CalcPseudoPermBrancheSaintVenantQruis> listeBranchesSV = calcPerm.getDclmUserActive(CalcPseudoPermBrancheSaintVenantQruis.class);
    // Ajout la valeur de ruissellement s'il y en a
    if (CollectionUtils.isNotEmpty(listeBranchesSV)) {
      final double valeur = listeBranchesSV.get(0).getQruis();
      valeursCP.add(valeur);
    }
    lignesE.put(nomCalcPerm, valeursCP);
  }

  /**
   * @param data
   * @throws IOException
   */
  private void writeDonneesTransitoires(final CrueData data) throws IOException {

    writeCom("");
    writeCom(" Donnees transitoires ");

    // Ligne A
    writeCom("");
    writeCom(" dt tmax tdeb hdeb nts ");

    String dt = null;
    String tMax = null;
    // tdeb plus supporté donc valeur par défaut toujours à 0 0 0 0
    final String tDeb = DateDurationConverter.CRUE_ZERO;
    // hdeb plus supporté donc valeur par défaut toujours à 0 0 0 0
    final String hDeb = DateDurationConverter.CRUE_ZERO;
    int nts = 0;

    final ParamNumModeleBase pnum = data.getPNUM();
    PdtVar pdtVar = null;
    Duration duree = null;
    if (pnum != null) {
      final ParamNumCalcTrans trans = pnum.getParamNumCalcTrans();
      if (trans != null) {
        final Pdt pdtTrans = trans.getPdt();
        if (pdtTrans != null) {
          if (pdtTrans instanceof PdtCst) {
            final PdtCst pdtCst = (PdtCst) pdtTrans;
            duree = pdtCst.getPdtCst();
          } else if (pdtTrans instanceof PdtVar) {
            pdtVar = (PdtVar) pdtTrans;
            if (CollectionUtils.isEmpty(pdtVar.getElemPdt())) {
              analyze_.addSevereError("io.dh.varPdt.empty");
              return;
            }
            duree = pdtVar.getElemPdt().get(0).getDureePdt();
          }
        }
      }
    }
    dt = DateDurationConverter.durationToCrueFormat(duree);

    final ParamCalcScenario pcal = data.getPCAL();
    LocalDateTime dateDebSce = pcal.getDateDebSce();
    if (dateDebSce == null) {
      dateDebSce = DateDurationConverter.ZERO_DATE;
    }
    // le tmax s'est la date de deb + duree
    final List<OrdCalcTrans> ordCalcs = EMHHelper.selectInstanceOf(data.getOCAL().getOrdCalc(), OrdCalcTrans.class);
    final List<CalcTrans> calcsTrans = EMHHelper.collectCalcTrans(data.getOCAL().getOrdCalc());
    //si on est ici on a 1 ord calc trans:
    final CalcTrans calcTrans = calcsTrans.get(0);
    final OrdCalcTrans ordCalc = ordCalcs.get(0);

    tMax = DateDurationConverter.durationToCrueFormat(ordCalc.getDureeCalc());

    final long dureeDt = duree == null ? 0 : duree.getMillis();
    if (dureeDt == 0) {
      analyze_.addError("io.dh.nts.indefini.error");
    } else if (ordCalc.getPdtRes() != null) {
      nts = (int) (((PdtCst) ordCalc.getPdtRes()).getPdtCst().getMillis() / dureeDt);
    }

    fortranWriter.stringField(0, dt);
    fortranWriter.stringField(1, tMax);
    fortranWriter.stringField(2, tDeb);
    fortranWriter.stringField(3, hDeb);
    fortranWriter.intField(4, nts);
    fortranWriter.writeFields();

    // Lignes B
    writeCom("");
    writeCom(" Definition des types des conditions aux limites ");



    final TObjectIntHashMap typesCL = new TObjectIntHashMap();
    typesCL.put(CalcTransNoeudNiveauContinuLimnigramme.class, 1);
    typesCL.put(CalcTransNoeudQapp.class, 2);
    typesCL.put(CalcTransNoeudNiveauContinuTarage.class, 3);
    typesCL.put(CalcTransBrancheOrificeManoeuvre.class, 41);

    // Stocke les lois des Conditions aux limites pour écrire ensuite les lignes E
    final List<Loi> lignesE = new ArrayList<>();
    afficheLignesBTransEtRecupereDonneesLignesETrans(calcTrans, typesCL, lignesE);

    // Ecriture Ligne C
    writeFin();

    // Ecriture Ligne D
    if (pdtVar != null && CollectionUtils.isNotEmpty(pdtVar.getElemPdt())) {
      writeCom("");
      writeCom(" Modulation du pas de temps au cours de la procedure de stabilisation ");

      ecritLignesDTransitoire(pdtVar, dateDebSce);
    }

    // Ecriture Lignes E
    for (int i = 0, imax = lignesE.size(); i < imax; i++) {
      ecritLoi(data, lignesE.get(i));
    }

    // Ecriture Lignes F (s'il y a une loi de ruissellement)
    final Collection<CalcTransBrancheSaintVenantQruis> listeBranchesSV = calcTrans.getDclmUserActive(CalcTransBrancheSaintVenantQruis.class);

    if (CollectionUtils.isEmpty(listeBranchesSV)) {
      return;
    }

    final LoiDF loiCommun = listeBranchesSV.iterator().next().getHydrogrammeQruis();
    if (loiCommun != null) {
      writeCom("");
      writeCom(" ruis ");
      fortranWriter.stringField(0, "RUIS");
      fortranWriter.writeFields();

      // Ligne F2
      ecritLoiDF(loiCommun);

      // Ligne F3
      writeFin();
    }

  }

  /**
   * @param pdtVar
   * @throws IOException
   */
  private void ecritLignesDTransitoire(final PdtVar pdtVar, final LocalDateTime dateDebut) throws IOException {

    // Ligne D1
    fortranWriter.stringField(0, "VARDT");
    fortranWriter.writeFields();

    // Lignes D2
    Duration lastDate = DateDurationConverter.dateToDurationFromZero(dateDebut);
    for (int i = 0, imax = pdtVar.getElemPdt().size(); i < imax; i++) {
      final ElemPdt elemPdt = pdtVar.getElemPdt().get(i);
      final int nbPas = elemPdt.getNbrPdt();
      final Duration duree = elemPdt.getDureePdt();
      fortranWriter.stringField(0, DateDurationConverter.durationToCrueFormat(lastDate));
      fortranWriter.stringField(1, DateDurationConverter.durationToCrueFormat(duree));
      fortranWriter.writeFields();
      lastDate = lastDate.plus(new Duration(nbPas * duree.getMillis()));
    }

    // Ligne D3
    writeFin();

  }

  /**
   * @param pdtVar
   * @throws IOException
   */
  private void ecritLignesDPermanent(final PdtVar pdtVar) throws IOException {

    // Ligne D1
    fortranWriter.stringField(0, "VARDT");
    fortranWriter.writeFields();

    // Lignes D2
    for (int i = 0, imax = pdtVar.getElemPdt().size(); i < imax; i++) {
      final ElemPdt elemPdt = pdtVar.getElemPdt().get(i);
      final int nbPas = elemPdt.getNbrPdt();
      final Duration duree = elemPdt.getDureePdt();
      fortranWriter.intField(0, nbPas);
      fortranWriter.stringField(1, DateDurationConverter.durationToCrueFormat(duree));
      fortranWriter.writeFields();
    }

    // Ligne D3
    writeFin();

  }

  /**
   * @param calcTrans
   * @param typesCL
   * @param lignesE
   * @throws IOException
   */
  private void afficheLignesBTransEtRecupereDonneesLignesETrans(final CalcTrans calcTrans,
          final TObjectIntHashMap typesCL, final List<Loi> lignesE) throws IOException {
    calcTrans.sort(comparator);
    for (final CalcTransNoeudQapp lim : calcTrans.getDclmUserActive(CalcTransNoeudQapp.class)) {
      manageTranItem(lim, typesCL, lignesE);

    }
    for (final CalcTransNoeudNiveauContinuLimnigramme lim : calcTrans.getDclmUserActive(CalcTransNoeudNiveauContinuLimnigramme.class)) {
      manageTranItem(lim, typesCL, lignesE);
    }
    for (final CalcTransNoeudNiveauContinuTarage lim : calcTrans.getDclmUserActive(CalcTransNoeudNiveauContinuTarage.class)) {
      manageTranItem(lim, typesCL, lignesE);
    }

    for (final CalcTransBrancheOrificeManoeuvre lim : calcTrans.getDclmUserActive(CalcTransBrancheOrificeManoeuvre.class)) {
      manageTranItem(lim, typesCL, lignesE);
    }

  }

  private void manageTranItem(final CalcTransItem lim, final TObjectIntHashMap typesCL, final List<Loi> lignesE)
          throws IOException {
    fortranWriter.stringField(0, lim.getEmh().getNom());
    int typeCL = typesCL.get(lim.getClass());
    if (lim instanceof CalcTransBrancheOrificeManoeuvre) {
      final CalcTransBrancheOrificeManoeuvre or = (CalcTransBrancheOrificeManoeuvre) lim;
      if (EnumSensOuv.OUV_VERS_BAS.equals(or.getSensOuv())) {
        typeCL++;
      }
    }
    fortranWriter.intField(1, typeCL);
    fortranWriter.writeFields();
    lignesE.add(lim.getLoi());
  }

  /**
   * @param loi
   * @throws IOException
   */
  private void ecritLoi(final CrueData data, final Loi loi) throws IOException {

    if (loi instanceof LoiDF) {

      // Valeur par défaut mise à 2
      final int ilecam = 2;

      ecritLoiDFIlecam2((LoiDF) loi, ilecam);

    } else if (loi instanceof LoiFF) {

      final EvolutionFF evolFF = loi.getEvolutionFF();

      if (evolFF != null && CollectionUtils.isNotEmpty(evolFF.getPtEvolutionFF())) {
        final List<PtEvolutionFF> ptEvolutionFF = evolFF.getPtEvolutionFF();
        // Ligne E3-1
        writeCom("");
        writeCom(" Zbas Dz ");

        PtEvolutionFF ptEvol = ptEvolutionFF.get(0);
        final double zbas = ptEvol.getOrdonnee();
        double dz = 0.0;

        if (ptEvolutionFF.size() == 1) {
          analyze_.addError("io.dh.dz.indefini.error", loi.getNom());
        } else {
          ptEvol = ptEvolutionFF.get(1);
          dz = ptEvol.getOrdonnee() - zbas;
        }

        fortranWriter.doubleField(0, zbas);
        fortranWriter.doubleField(1, dz);
        fortranWriter.writeFields();

        writeCom("");
        writeCom(" Valeurs des conditions aux limites pour la loi Q(z) ");
        int nbCaractToWrite = 0;
        int nbCaractWritten = 0;
        int countEltsParLigne = 0;

        // Lignes E3-2
        final PropertyEpsilon eps = data.getCrueConfigMetier().getEpsilon("dz");
        for (int j = 0, jmax = ptEvolutionFF.size(); j < jmax; j++) {
          if (j > 0) {
            final double newDz = ptEvolutionFF.get(j).getOrdonnee() - ptEvolutionFF.get(j - 1).getOrdonnee();
            if (!eps.isSame(dz, newDz)) {
              analyze_.addError("io.dh.dzNotConstant", loi.getNom());
            }
          }
          ptEvol = ptEvolutionFF.get(j);
          // ATTENTION il faut inverser ici
          final double ptY = ptEvol.getAbscisse();

          // On ajoute 1 à la longueur de la chaîne à écrire pour l'espace ajouté entre chaque valeur
          nbCaractToWrite = 1 + Double.valueOf(ptY).toString().length();
          // On revient à la ligne si ce qui est à écrire dépasse la taille max de la ligne
          if (countEltsParLigne >= 8 || (nbCaractWritten + nbCaractToWrite > tailleMaxLine)) {
            fortranWriter.writeFields();
            countEltsParLigne = 0;
            nbCaractWritten = 0;
          }
          fortranWriter.doubleField(countEltsParLigne++, ptY);
          nbCaractWritten += nbCaractToWrite;
        }
        fortranWriter.writeFields();

        // Ligne E3-3
        writeFin();
      }
    }
  }

  /**
   * @param loi
   * @throws IOException
   */
  private void ecritLoiDF(final LoiDF loi) throws IOException {

    final EvolutionFF evolFF = loi.getEvolutionFF();
    if (evolFF != null && CollectionUtils.isNotEmpty(evolFF.getPtEvolutionFF())) {
      PtEvolutionFF ptEvol = null;
      writeCom("");
      writeCom(" tj th tm ts Q ");
      for (int j = 0, jmax = evolFF.getPtEvolutionFF().size(); j < jmax; j++) {
        ptEvol = evolFF.getPtEvolutionFF().get(j);
        fortranWriter.stringField(0, toCrueFormat(ptEvol.getAbscisse()));
        fortranWriter.doubleField(1, ptEvol.getOrdonnee());
        fortranWriter.writeFields();
      }
    }
  }

  private String toCrueFormat(final double nbSec) {
    final long nbMillis = (long) (nbSec * 1000L);
    final String crueFormat = DateDurationConverter.durationToCrueFormat(new Duration(nbMillis));
    return crueFormat;
  }

  /**
   * @param loi
   * @throws IOException
   */
  private void ecritLoiDFIlecam2(final LoiDF loi, final int ilecam) throws IOException {

    // Ligne E2-1
    writeCom("");
    writeCom(" ilecam ");
    fortranWriter.intField(0, ilecam);
    fortranWriter.writeFields();

    // Ligne E2-2
    ecritLoiDF(loi);

    // Ligne E2-3
    writeFin();
  }

  /**
   * @throws IOException
   */
  private void writeFin() throws IOException {

    fortranWriter.stringField(0, "FIN");
    fortranWriter.writeFields();
  }

  @Override
  public void stop() {
  }
}
