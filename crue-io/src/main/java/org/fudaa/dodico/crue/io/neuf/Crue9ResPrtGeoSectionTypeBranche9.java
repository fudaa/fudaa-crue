/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import org.fudaa.dodico.crue.metier.emh.LoiFF;

/**
 * spécialisation de la classe ResPrtGeoSection ; cette nouvelle classe n'est instanciée que pour les sections des branches de
 * types 2, 6, 15 (de même format que les sections des branches de « type branche 9 », abandonnées mais historiquement
 * importantes). Dans de futures versions des logiciels Fudaa-Crue et Crue10, cela sera certainement abandonné, mais c’est
 * nécessaire au contrôle de non régression de l’application.
 *
 * @author deniger
 */
public class Crue9ResPrtGeoSectionTypeBranche9 extends Crue9ResPrtGeoSection {

  public Crue9ResPrtGeoSectionTypeBranche9(final int typBra, final boolean stricklerWith1Lit) {
    super(typBra,stricklerWith1Lit);
  }

  /**
   * @param zLimGLitSto the zLimGLitSto to set
   */
  public void setzLimGLitSto(final double zLimGLitSto) {
    super.setValue("zLimGLitSto", zLimGLitSto);
  }

  /**
   * @param zLimDLitSto the zLimDLitSto to set
   */
  public void setzLimDLitSto(final double zLimDLitSto) {
    super.setValue("zLimDLitSto", zLimDLitSto);

  }

  /**
   * @param zLimGLitMaj the zLimGLitMaj to set
   */
  public void setzLimGLitMaj(final double zLimGLitMaj) {
    super.setValue("zLimGLitMaj", zLimGLitMaj);
  }

  /**
   * @param zLimDLitMaj the zLimDLitMaj to set
   */
  public void setzLimDLitMaj(final double zLimDLitMaj) {
    super.setValue("zLimDLitMaj", zLimDLitMaj);
  }

  /**
   * @param zLimGLitMin the zLimGLitMin to set
   */
  public void setzLimGLitMin(final double zLimGLitMin) {
    super.setValue("zLimGLitMin", zLimGLitMin);
  }

  /**
   * @param zLimDLitMin the zLimDLitMin to set
   */
  public void setzLimDLitMin(final double zLimDLitMin) {
    super.setValue("zLimDLitMin", zLimDLitMin);
  }

  /**
   * @param zoefKextrapol the zoefKextrapol to set
   */
  public void setCoefKextrapol(final double zoefKextrapol) {
    super.setValue("coefKextrapol", zoefKextrapol);
  }

  /**
   * @param zSmin the zSmin to set
   */
  public void setLoiZSmin(final LoiFF zSmin) {
    super.setValue("loiZSmin", zSmin);
  }

  /**
   * @param zPmin the zPmin to set
   */
  public void setLoiZPmin(final LoiFF zPmin) {
    super.setValue("loiZPmin", zPmin);
  }

  /**
   * @param zLmin the zLmin to set
   */
  public void setLoiZLmin(final LoiFF zLmin) {
    super.setValue("loiZLmin", zLmin);
  }

  /**
   * @param zSmaj the zSmaj to set
   */
  public void setLoiZSmaj(final LoiFF zSmaj) {
    super.setValue("loiZSmaj", zSmaj);
  }

  /**
   * @param zPmaj the zPmaj to set
   */
  public void setLoiZPmaj(final LoiFF zPmaj) {
    super.setValue("loiZPmaj", zPmaj);
  }

  /**
   * @param zLmaj the zLmaj to set
   */
  public void setLoiZLmaj(final LoiFF zLmaj) {
    super.setValue("loiZLmaj", zLmaj);
  }

  /**
   * @param zCoefW1 the zCoefW1 to set
   */
  public void setLoiZCoefW1(final LoiFF zCoefW1) {
    super.setValue("loiZCoefW1", zCoefW1);
  }

  /**
   * @param zCoefW2 the zCoefW2 to set
   */
  public void setLoiZCoefW2(final LoiFF zCoefW2) {
    super.setValue("loiZCoefW2", zCoefW2);
  }
}
