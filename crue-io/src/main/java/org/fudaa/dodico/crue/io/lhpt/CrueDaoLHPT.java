/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.lhpt;

import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.common.AbstractDaoLoi;

import java.util.List;

/**
 * Representation persistante du fichier xml LHPT.
 * 
 */
@SuppressWarnings("PMD.VariableNamingConventions")
public class CrueDaoLHPT extends AbstractCrueDao {

  protected CrueDaoStructureLHPT.EchellesSections EchellesSections;
  protected List<AbstractDaoLoi> LoisLHPT;

}
