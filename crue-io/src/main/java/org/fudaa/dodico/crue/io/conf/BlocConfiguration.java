package org.fudaa.dodico.crue.io.conf;

import java.util.ArrayList;
import java.util.List;

public class BlocConfiguration {

  private List<Option> Options = new ArrayList<>();

  public List<Option> getOptions() {
    return Options;
  }

  public void setOptions(final List<Option> options) {
    this.Options = options;
  }
}
