/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.common;

import com.thoughtworks.xstream.XStream;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.config.loi.LoiTypeContainer;
import org.fudaa.dodico.crue.metier.emh.EvolutionFF;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;

/**
 * Classe abstraite qui permet de factoriser les attributs communs de toutes les lois
 */
public abstract class AbstractDaoFloatLoi extends AbstractDaoLoi  {
  //WARNING: keep the order like this
  /**
   * La date de début pour une loi Date. Doit être ignoree par les autres lois.
   */
  private String DateZeroLoiDF;
  protected EvolutionFF EvolutionFF;

  public static void configureXstream(final XStream xstream, final LoiTypeContainer data, final String version) {
    configureXstream(xstream, data, version, null);
  }




  public static void configureXstream(final XStream xstream, final LoiTypeContainer data, final String version, final CtuluLog log) {
    xstream.alias("EvolutionFF", EvolutionFF.class);
    xstream.alias("PointFF", PtEvolutionFF.class);
    xstream.useAttributeFor(AbstractDaoFloatLoi.class, "Type");
    xstream.useAttributeFor(AbstractDaoFloatLoi.class, "Nom");
    final SingleConverterPointFF singleConverterPointFF = new SingleConverterPointFF();
    singleConverterPointFF.setAnalyse(log);
    xstream.registerConverter(singleConverterPointFF);
    if (Crue10VersionConfig.V_1_1_1.equals(version)) {
      xstream.registerConverter(new EnumTypeLoiConverterOld(data));
    } else {
      xstream.registerConverter(new EnumTypeLoiConverter(data));
    }

    xstream.addImplicitCollection(EvolutionFF.class, "mpoints");
  }

  /**
   * La partie daoToMetier commune aux 2 lois
   */
  public static void daoToMetierLoi(final Loi outLoi, final AbstractDaoFloatLoi inLoi) {
    outLoi.setNom(inLoi.Nom);
    outLoi.setCommentaire(inLoi.Commentaire);
    outLoi.setType(inLoi.Type);
    outLoi.setEvolutionFF(inLoi.EvolutionFF);
  }

  /**
   * La partie metierToDao commune aux 2 lois
   */
  public static void metierToDaoLoi(final AbstractDaoFloatLoi outLoi, final Loi inLoi) {
    outLoi.Nom = inLoi.getNom();
    outLoi.Commentaire = StringUtils.defaultString(inLoi.getCommentaire());
    outLoi.Type = inLoi.getType();
    outLoi.EvolutionFF = inLoi.getEvolutionFF();
  }

  public String getDateZeroLoiDF() {
    return DateZeroLoiDF;
  }

  public void setDateZeroLoiDF(final String dateZeroLoiDF) {
    DateZeroLoiDF = dateZeroLoiDF;
  }

  /**
   * Classe permettant de typer les lois FF
   */
  public static class DaoLoiFF extends AbstractDaoFloatLoi {
  }

  /**
   * Classe abstraite qui permet de factoriser les attributs communs de toutes les lois DF
   */
  public static class DaoLoiDF extends AbstractDaoFloatLoi{
  }
}
