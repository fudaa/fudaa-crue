/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.common;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.StoContent;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.EMHNoeudFactory;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.joda.time.Duration;

import java.util.*;

/**
 * CrueData est un conteneur de donnees Crue, il contient des donnees EMH de chaque categorie pour optimiser leur acces. Il contient les donnees
 * d'entrees DFRT de frottements.
 *
 * @author Adrien Hadoux
 */
public final class CrueDataImpl implements CrueData {
  /**
   * @param sc le conteneur
   * @return CrueData correspondant au scenario n'ayant un seul modele
   */
  public static CrueData buildConcatFor(final EMHScenario sc, final CrueConfigMetier configMetier) {
    final List<EMHModeleBase> fils = EMHHelper.collectEMHInRelationEMHContient(sc, EnumCatEMH.MODELE);
    if (fils.size() > 1) {
      return null;
    }
    final EMHModeleBase base = fils.get(0);
    return new CrueDataImpl(sc, base, base.getConcatSousModele(), configMetier);
  }

  public static CrueData buildConcatFor(final EMHScenario sc, final EMHModeleBase conteneur, final CrueConfigMetier configMetier) {
    return new CrueDataImpl(sc, conteneur, conteneur.getConcatSousModele(), configMetier);
  }

  @Override
  public OrdPrtGeoModeleBase getOrCreateOPTG() {
    OrdPrtGeoModeleBase optg = getOPTG();
    if (optg == null) {
      optg = new OrdPrtGeoModeleBase(getCrueConfigMetier());
      setOPTG(optg);
    }
    return optg;
  }

  /**
   * compatibilité version 1.1.1
   */
  Duration oldPCALDureeSce;
  Pdt oldPCALPdtRes;
  private ScenarioAutoModifiedState modifiedState;

  @Override
  public void setOldPCALDureeSce(final Duration duration) {
    this.oldPCALDureeSce = duration;
  }

  public void setDptiChangedDuringLoading() {
    if (modifiedState != null) {
      modifiedState.setDptiModified(true);
    }
  }

  public CrueDataImpl withModifiedState(ScenarioAutoModifiedState modifiedState) {
    this.modifiedState = modifiedState;
    return this;
  }

  @Override
  public Duration getOldPCALDureeSce() {
    return oldPCALDureeSce;
  }

  @Override
  public void setOldPCALPdtRes(final Pdt createPdtCst) {
    this.oldPCALPdtRes = createPdtCst;
  }

  @Override
  public Pdt getOldPCALPdtRes() {
    return oldPCALPdtRes;
  }

  @Override
  public OrdPrtCIniModeleBase getOrCreateOPTI() {
    OrdPrtCIniModeleBase opti = getOPTI();
    if (opti == null) {
      opti = new OrdPrtCIniModeleBase(getCrueConfigMetier());
      setOPTI(opti);
    }
    return opti;
  }

  @Override
  public ParamCalcScenario getOrCreatePCAL() {
    ParamCalcScenario pcal = getPCAL();
    if (pcal == null) {
      pcal = new ParamCalcScenario(getCrueConfigMetier());
      setPCAL(pcal);
    }
    return pcal;
  }

  @Override
  public OrdCalcScenario getOrCreateOCAL() {
    OrdCalcScenario ocal = getOCAL();
    if (ocal == null) {
      ocal = new OrdCalcScenario();
      setOCAL(ocal);
    }
    return ocal;
  }

  @Override
  public ParamNumModeleBase getOrCreatePNUM() {
    ParamNumModeleBase pnum = getPNUM();
    if (pnum == null) {
      pnum = new ParamNumModeleBase(getCrueConfigMetier());
      setPNUM(pnum);
    }
    return pnum;
  }

  @Override
  public OrdResScenario getOrCreateORES() {
    OrdResScenario ores = getORES();
    if (ores == null) {
      ores = new OrdResScenario();
      setORES(ores);
    }
    return ores;
  }

  @Override
  public OrdPrtReseau getOrCreateOPTR() {
    OrdPrtReseau optr = getOPTR();
    if (optr == null) {
      optr = new OrdPrtReseau(getCrueConfigMetier());
      setOPTR(optr);
    }
    return optr;
  }

  /**
   * Attention pour l'instant les contenations de Modele ne sont pas gérées.
   *
   * @param conteneur le conteneur
   * @return le crueData qui va bien
   */
  public static CrueDataImpl buildFor(final CatEMHConteneur conteneur, final CrueConfigMetier propertyDefContainer) {
    if (conteneur.getCatType().equals(EnumCatEMH.SCENARIO)) {
      return new CrueDataImpl((EMHScenario) conteneur, null,
          ((EMHScenario) conteneur).getConcatSousModele(), propertyDefContainer);
    }
    if (conteneur.getCatType().equals(EnumCatEMH.MODELE)) {
      return new CrueDataImpl(null, (EMHModeleBase) conteneur,
          ((EMHModeleBase) conteneur).getConcatSousModele(), propertyDefContainer);
    }
    if (conteneur.getCatType().equals(EnumCatEMH.SOUS_MODELE)) {
      return new CrueDataImpl(null, null,
          (EMHSousModele) conteneur, propertyDefContainer);
    }
    return null;
  }

  /**
   * boolean pour indiquer les fichiers crue 9 contiennent des cartes distmax. Dans ce cas, la lecture des fichiers de resultats ne doivent pas se
   * faire.
   */
  boolean crue9ContientDistmax;
  final CrueConfigMetier crueProperties;
  final Map<String, DonPrtGeoBatiCasier> idBatiCasierDefined = new HashMap<>(50);
  private final Map<String, DonPrtGeoProfilCasier> idProfilCasierDefined = new HashMap<>(50);
  private final Map<String, DonPrtGeoProfilSection> idProfilSectionDefined = new HashMap<>(50);
  private final Set<String> profilSectionUsed = new HashSet<>();
  private final Set<String> profilCasierUsed = new HashSet<>();
  private final Set<String> batiCasierUsed = new HashSet<>();
  private final EMHModeleBase modele;
  private final EMHNoeudFactory nodeFactory;
  final EMHScenario scenarioData;
  final EMHSousModele ssModele;
  /**
   * Données STO
   */
  private StoContent sto;

  public CrueDataImpl(final CrueConfigMetier propertyDefContainer) {
    this(new EMHNoeudFactory(), propertyDefContainer);
  }

  public CrueDataImpl(final EMHNoeudFactory nodeFactory, final CrueConfigMetier cruePropperties) {
    scenarioData = new EMHScenario();
    scenarioData.getLoiConteneur();
    this.crueProperties = cruePropperties;
    ssModele = new EMHSousModele();

    modele = new EMHModeleBase();
    this.nodeFactory = nodeFactory;

    init();
  }

  public CrueDataImpl(final EMHScenario scenarioData, final EMHModeleBase modele, final EMHSousModele ssModele,
                      final CrueConfigMetier propertyDefContainer) {
    super();
    this.crueProperties = propertyDefContainer;
    this.scenarioData = scenarioData;
    if (scenarioData != null) {
      scenarioData.getLoiConteneur();
    }
    this.ssModele = ssModele;
    this.modele = modele;
    nodeFactory = new EMHNoeudFactory();
    if (ssModele != null) {
      final List<CatEMHNoeud> noeuds = ssModele.getNoeuds();
      if (CollectionUtils.isNotEmpty(noeuds)) {
        nodeFactory.addAll(noeuds);
      }
    }
    if (modele != null) {
      final List<EMHSousModele> sousModeles = modele.getSousModeles();
      for (final EMHSousModele sousModele : sousModeles) {
        if (sousModele != ssModele) {
          final List<CatEMHNoeud> noeuds = sousModele.getNoeuds();
          if (CollectionUtils.isNotEmpty(noeuds)) {
            nodeFactory.addAll(noeuds);
          }
        }
      }
    }
    if (scenarioData != null) {
      init();
    }
  }

  /**
   * Ajoute automatiquement l'objet emh dans la bonne structure.
   *
   * @param object
   */
  @Override
  public void add(final EMH object) {
    if (object == null) {
      return;
    }
    if (emhById != null) {
      emhById.put(object.getId(), object);
    }
    EMHRelationFactory.addRelationContientEMH(ssModele, object);
  }

  Set<String> nodeAdded = new HashSet<>();

  @Override
  public EMHNoeudNiveauContinu createNode(final String id) {
    final EMHNoeudNiveauContinu node = this.nodeFactory.getNode(id);
    return node;
  }

  @Override
  public CatEMHBranche findBrancheByReference(final String ref) {
    return EMHHelper.selectEMHInRelationEMHContientByRef(ssModele, ref, EnumCatEMH.BRANCHE);
  }

  @Override
  public Map<String, EMH> getSimpleEMHByNom() {
    final Map<String, EMH> res = new HashMap<>();
    final List<EMH> allSimpleEMH = getAllSimpleEMH();
    for (final EMH emh : allSimpleEMH) {
      res.put(emh.getNom(), emh);
    }
    return res;
  }
  public Map<String, EMH> getEMHByNom() {
    final Map<String, EMH> res = getSimpleEMHByNom();
    res.put(modele.getNom(),modele);
    res.put(ssModele.getNom(),ssModele);
    return res;
  }

  Map<String, EMH> emhById = null;

  @Override
  public Map<String, EMH> getSimpleEMHById() {
    if (emhById == null) {
      emhById = createSimpleEMHById();
    }
    return Collections.unmodifiableMap(emhById);
  }

  public Map<String, EMH> createSimpleEMHById() {
    final Map<String, EMH> res = new HashMap<>();
    final List<EMH> allSimpleEMH = getAllSimpleEMH();
    for (final EMH emh : allSimpleEMH) {
      res.put(emh.getId(), emh);
    }
    return res;
  }

  @Override
  public Calc findCalcByNom(final String nom) {
    final DonCLimMScenario in = getConditionsLim();
    if (in == null) {
      return null;
    }
    Calc res = EMHHelper.selectObjectNomme(in.getCalcPseudoPerm(), nom);
    if (res == null) {
      res = EMHHelper.selectObjectNomme(in.getCalcTrans(), nom);
    }
    return res;
  }

  @Override
  public CatEMHCasier findCasierByReference(final String ref) {
    return EMHHelper.selectEMHInRelationEMHContientByRef(ssModele, ref, EnumCatEMH.CASIER);
  }

  @Override
  public EMH findEMHByReference(final String ref) {
    if (ref == null) {
      return null;
    }
    return getSimpleEMHById().get(ref.toUpperCase());
  }

  @Override
  public CatEMHNoeud findNoeudByReference(final String ref) {
    return EMHHelper.selectEMHInRelationEMHContientByRef(ssModele, ref, EnumCatEMH.NOEUD);
  }

  @Override
  public CatEMHSection findSectionByReference(final String ref) {
    return EMHHelper.selectEMHInRelationEMHContientByRef(ssModele, ref, EnumCatEMH.SECTION);
  }

  /**
   * Retourne tous les objets EMH
   */
  @Override
  public List<EMH> getAllSimpleEMH() {
    return EMHHelper.collectEMHInRelationEMHContient(ssModele);
  }

  @Override
  public List<CatEMHBranche> getBranches() {
    return EMHHelper.collectEMHInRelationEMHContient(ssModele, EnumCatEMH.BRANCHE);
  }

  @Override
  public List<EMHBrancheSaintVenant> getBranchesSaintVenant() {
    return EMHHelper.selectClass(EMHHelper.collectEMHInRelationEMHContient(ssModele, EnumCatEMH.BRANCHE),
        EMHBrancheSaintVenant.class);
  }

  @Override
  public List<CatEMHCasier> getCasiers() {
    return EMHHelper.collectEMHInRelationEMHContient(ssModele, EnumCatEMH.CASIER);
  }

  /**
   * @return the conditionsLim
   */
  @Override
  public DonCLimMScenario getConditionsLim() {
    return (DonCLimMScenario) EMHHelper.selectInfoEMH(scenarioData, EnumInfosEMH.DON_CLIM_SCENARIO);
  }

  @Override
  public CrueConfigMetier getCrueConfigMetier() {
    return crueProperties;
  }

  @Override
  public DonFrtConteneur getFrottements() {
    return (DonFrtConteneur) EMHHelper.selectInfoEMH(ssModele, EnumInfosEMH.DON_FRT_CONTENEUR);
  }

  @Override
  public DonLoiHYConteneur getLoiConteneur() {
    return scenarioData.getLoiConteneur();
  }

  /**
   * @return the lois
   */
  @Override
  public List<Loi> getLois() {
    return scenarioData.getLoiConteneur().getLois();
  }

  @Override
  public EMHModeleBase getModele() {
    return modele;
  }

  @Override
  public List<CatEMHNoeud> getNoeuds() {
    return EMHHelper.collectEMHInRelationEMHContient(ssModele, EnumCatEMH.NOEUD);
  }

  /**
   * @return the ordCalc
   */
  @Override
  public OrdCalcScenario getOCAL() {
    return scenarioData.getOrdCalcScenario();
  }

  @Override
  public OrdPrtCIniModeleBase getOPTI() {
    return (OrdPrtCIniModeleBase) EMHHelper.selectInfoEMH(modele, EnumInfosEMH.ORD_PRT_CINI_MODELE_BASE);
  }

  @Override
  public void setOPTI(final OrdPrtCIniModeleBase opti) {
    modele.addInfosEMH(opti);
  }

  @Override
  public OrdPrtReseau getOPTR() {
    return modele.getOrdPrtReseau();
  }

  @Override
  public void setOPTR(final OrdPrtReseau optr) {
    modele.setOrdPrtReseau(optr);
  }

  /**
   * @return the ordRes
   */
  @Override
  public OrdResScenario getORES() {
    return scenarioData.getOrdResScenario();
  }

  @Override
  public ParamCalcScenario getPCAL() {
    return scenarioData.getParamCalcScenario();
  }

  @Override
  public ParamNumModeleBase getPNUM() {
    return (ParamNumModeleBase) EMHHelper.selectInfoEMH(modele, EnumInfosEMH.PARAM_NUM_MODELE_BASE);
  }

  @Override
  public OrdPrtGeoModeleBase getOPTG() {
    return (OrdPrtGeoModeleBase) EMHHelper.selectInfoEMH(modele, EnumInfosEMH.ORD_PRT_GEO_MODELE_BASE);
  }

  @Override
  public void getOPTG(final OrdPrtGeoModeleBase optg) {
    modele.addInfosEMH(optg);
  }

  @Override
  public ItemVariable getProperty(final String id) {
    return getCrueConfigMetier().getProperty(id);
  }

  /**
   * @return the scenarioData
   */
  @Override
  public EMHScenario getScenarioData() {
    return scenarioData;
  }

  @Override
  public List<CatEMHSection> getSections() {
    return EMHHelper.collectEMHInRelationEMHContient(ssModele, EnumCatEMH.SECTION);
  }

  @Override
  public EMHSousModele getSousModele() {
    return ssModele;
  }

  /**
   * @return the sto
   */
  @Override
  public StoContent getSto() {
    return sto;
  }

  @Override
  public DonPrtGeoBatiCasier getDefinedBatiCasier(final String id) {
    return idBatiCasierDefined.get(id);
  }

  @Override
  public DonPrtGeoProfilCasier getDefinedProfilCasier(final String id) {
    return idProfilCasierDefined.get(id);
  }

  @Override
  public DonPrtGeoProfilSection getDefinedProfilSection(final String id) {
    return idProfilSectionDefined.get(id);
  }

  @Override
  public Set<String> getDefinedButNotUsedProfilSection() {
    final Set<String> definedBuNotUsed = new HashSet<>(idProfilSectionDefined.keySet());
    definedBuNotUsed.removeAll(profilSectionUsed);
    return definedBuNotUsed;
  }

  @Override
  public Set<String> getDefinedButNotUsedProfilCasier() {
    final Set<String> definedBuNotUsed = new HashSet<>(idProfilCasierDefined.keySet());
    definedBuNotUsed.removeAll(profilCasierUsed);
    return definedBuNotUsed;
  }

  @Override
  public Set<String> getDefinedButNotUsedBatiCasier() {
    final Set<String> definedBuNotUsed = new HashSet<>(idBatiCasierDefined.keySet());
    definedBuNotUsed.removeAll(batiCasierUsed);
    return definedBuNotUsed;
  }

  private void init() {
    ssModele.initDonFrtConteneur();
    DonCLimMScenario dclmScenario = getConditionsLim();
    if (dclmScenario == null) {
      dclmScenario = new DonCLimMScenario();
      setConditionsLim(dclmScenario);
    }
  }

  /**
   * boolean pour indiquer les fichiers crue 9 contiennent des cartes distmax. Dans ce cas, la lecture des fichiers de resultats ne doivent pas se
   * faire.
   *
   * @return true si contient distmax
   */
  @Override
  public boolean isCrue9ContientDistmax() {
    return crue9ContientDistmax;
  }

  @Override
  public boolean mergeWithAnotherCrueData(final CrueData data) {

    return true;
  }

  /**
   * @param batiCasier
   */
  @Override
  public void registerDefinedBatiCasier(final DonPrtGeoBatiCasier batiCasier) {
    idBatiCasierDefined.put(batiCasier.getNom(), batiCasier);
  }

  /**
   * @param casierProfil un casier définit dans le fichiert dptg en tant que bibliothèque
   */
  @Override
  public void registerDefinedCasierProfil(final DonPrtGeoProfilCasier casierProfil) {
    idProfilCasierDefined.put(casierProfil.getNom(), casierProfil);
  }

  /**
   * @param sectionProfil
   */
  @Override
  public void registerDefinedSectionProfil(final DonPrtGeoProfilSection sectionProfil) {
    assert sectionProfil.getNom() != null;
    idProfilSectionDefined.put(sectionProfil.getNom(), sectionProfil);
  }

  @Override
  public void registerUseBatiCasier(final EMH emh, final String profilNom) {
    DonPrtGeoBatiCasier casier = getDefinedBatiCasier(profilNom);
    if (casier == null) {
      casier = new DonPrtGeoBatiCasier(getCrueConfigMetier());
      casier.setNom(profilNom);
      idBatiCasierDefined.put(profilNom, casier);
    }
    batiCasierUsed.add(profilNom);
    emh.addInfosEMH(casier);
  }

  @Override
  public void registerUseCasierProfil(final EMH emh, final String profilId) {
    DonPrtGeoProfilCasier casier = getDefinedProfilCasier(profilId);
    if (casier == null) {
      casier = new DonPrtGeoProfilCasier(getCrueConfigMetier());
      casier.setNom(profilId);
      idProfilCasierDefined.put(profilId, casier);
    }
    profilCasierUsed.add(profilId);
    if (emh != null) {
      emh.addInfosEMH(casier);
    }
  }

  @Override
  public void registerUseSectionProfil(final EMH emh, final String profilId) {
    DonPrtGeoProfilSection section = getDefinedProfilSection(profilId);
    if (section == null) {
      section = new DonPrtGeoProfilSection();
      section.setNom(profilId);
      idProfilSectionDefined.put(profilId, section);
    }
    profilSectionUsed.add(profilId);
    if (emh != null) {
      emh.addInfosEMH(section);
    }
  }

  /**
   * @param conditionsLim the conditionsLim to set
   */
  @Override
  public void setConditionsLim(final DonCLimMScenario conditionsLim) {
    scenarioData.addInfosEMH(conditionsLim);
  }

  /**
   * boolean pour indiquer les fichiers crue 9 contiennent des cartes distmax. Dans ce cas, la lecture des fichiers de resultats ne doivent pas se
   * faire.
   *
   * @param crue9ContientDistmax true si contient distmax
   */
  @Override
  public void setCrue9ContientDistmax(final boolean crue9ContientDistmax) {
    this.crue9ContientDistmax = crue9ContientDistmax;
  }

  @Override
  public void setFrottements(final List<DonFrt> frottements) {
    final DonFrtConteneur frottementConteneur = getFrottements();
    if (frottementConteneur == null) {
      final DonFrtConteneur ct = new DonFrtConteneur();
      ct.setListFrt(frottements);
      ssModele.addInfosEMH(ct);
    } else if (CollectionUtils.isNotEmpty(frottements)) {
      frottementConteneur.addAllFrt(frottements);
    }
  }

  @Override
  public void setMethodesInterpolation(final OrdPrtCIniModeleBase methodesInterpolation) {
    modele.addInfosEMH(methodesInterpolation);
  }

  /**
   * @param ordCalc the ordCalc to set
   */
  public void setOCAL(final OrdCalcScenario ordCalc) {
    scenarioData.addInfosEMH(ordCalc);
  }

  /**
   * @param ordRes the ordRes to set
   */
  public void setORES(final OrdResScenario ordRes) {
    scenarioData.addInfosEMH(ordRes);
  }

  public void setPCAL(final ParamCalcScenario paramCalc) {
    scenarioData.addInfosEMH(paramCalc);
  }

  public void setPNUM(final ParamNumModeleBase data) {
    modele.addInfosEMH(data);
  }

  public void setOPTG(final OrdPrtGeoModeleBase pretraitementsGeom) {
    modele.addInfosEMH(pretraitementsGeom);
  }

  /**
   * @param sto the sto to set
   */
  @Override
  public void setSto(final StoContent sto) {
    this.sto = sto;
  }

  @Override
  public void sort() {
    scenarioData.sort();
    modele.sort();
    ssModele.sort();
  }
}
