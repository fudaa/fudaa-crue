/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.etu;

import com.thoughtworks.xstream.converters.SingleValueConverter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;

public class Ref {

  private static class Pattern {

    public final String pattern;
    public final boolean startWith;

    public Pattern(String pattern, boolean startWith) {
      this.pattern = pattern;
      this.startWith = startWith;
    }
  }
  private static final Map<Pattern, Class<? extends Ref>> RefClasses = new HashMap<>();
  private static final Map<Pattern, Class<? extends Ref>> FileClasses = new HashMap<>();
  public String NomRef;

  static {
    Ref.FileClasses.put(new Pattern(".dc", false), DCRef.class);
    Ref.FileClasses.put(new Pattern(".dh", false), DHRef.class);

    Ref.FileClasses.put(new Pattern(".dclm.xml", false), DCLMRef.class);
    Ref.FileClasses.put(new Pattern(".dcsp.xml", false), DCSPRef.class);
    Ref.FileClasses.put(new Pattern(".dfrt.xml", false), DFRTRef.class);
    Ref.FileClasses.put(new Pattern(".dlhy.xml", false), DLHYRef.class);
    Ref.FileClasses.put(new Pattern(".dptg.xml", false), DPTGRef.class);
    Ref.FileClasses.put(new Pattern(".dpti.xml", false), DPTIRef.class);
    Ref.FileClasses.put(new Pattern(".dreg.xml", false), DREGRef.class);
    Ref.FileClasses.put(new Pattern(".drso.xml", false), DRSORef.class);
    Ref.FileClasses.put(new Pattern(".pnum.xml", false), PNUMRef.class);
    Ref.FileClasses.put(new Pattern(".pcal.xml", false), PCALRef.class);
    Ref.FileClasses.put(new Pattern(".ocal.xml", false), OCALRef.class);
    Ref.FileClasses.put(new Pattern(".optg.xml", false), OPTGRef.class);
    Ref.FileClasses.put(new Pattern(".opti.xml", false), OPTIRef.class);
    Ref.FileClasses.put(new Pattern(".optr.xml", false), OPTRRef.class);
    Ref.FileClasses.put(new Pattern(".ores.xml", false), ORESRef.class);
    Ref.FileClasses.put(new Pattern(".etu.xml", false), EtudeRef.class);

    Ref.RefClasses.put(new Pattern(CruePrefix.P_SCENARIO, true), ScenarioRef.class);
    Ref.RefClasses.put(new Pattern(CruePrefix.P_MODELE, true), ModeleRef.class);
    Ref.RefClasses.put(new Pattern(CruePrefix.P_SS_MODELE, true), SousModeleRef.class);
    Ref.RefClasses.put(new Pattern("R", true), RunRef.class);
  }

  public Ref() {
  }

  public static RunRef createRunRef(String NomRef) {
    RunRef res = new RunRef();
    res.NomRef = NomRef;
    return res;
  }

  public static Ref createReference(String NomRef) {
    Ref ref = createReference(NomRef, FileClasses);
    if (ref == null) {
      ref = createReference(NomRef, RefClasses);
    }
    if (ref == null) {
      ref = new Ref();
    }
    ref.NomRef = NomRef;
    return ref;

  }

  public static Ref createManagerReference(String NomRef) {
    Ref ref = createReference(NomRef, RefClasses);
    if (ref == null) {
      ref = new Ref();
    }
    ref.NomRef = NomRef;
    return ref;

  }

  public static Ref createFileReference(String NomRef) {
    Ref ref = createReference(NomRef, FileClasses);
    if (ref == null) {
      ref = new Ref();
    }
    ref.NomRef = NomRef;
    return ref;

  }

  private static Ref createReference(String NomRef, Map<Pattern, Class<? extends Ref>> mapRef) {
    Ref ref = null;

    for (Entry<Pattern, Class<? extends Ref>> entry : mapRef.entrySet()) {
      Pattern pattern = entry.getKey();

      try {
        if (pattern.startWith) {
          if (NomRef.startsWith(pattern.pattern)) {
            ref = entry.getValue().newInstance();

            break;
          }
        } else {
          if (NomRef.endsWith(pattern.pattern)) {
            ref = entry.getValue().newInstance();

            break;
          }
        }
      } catch (Exception e) {
      }
    }


    return ref;
  }

  public static class DCRef extends Ref {
  }

  public static class DHRef extends Ref {
  }

  public static class DCLMRef extends Ref {
  }

  public static class DCSPRef extends Ref {
  }

  public static class DFRTRef extends Ref {
  }

  public static class DLHYRef extends Ref {
  }

  public static class DPTGRef extends Ref {
  }

  public static class DPTIRef extends Ref {
  }
  public static class DREGRef extends Ref {
  }

  public static class DRSORef extends Ref {
  }

  public static class PNUMRef extends Ref {
  }

  public static class PCALRef extends Ref {
  }

  public static class OCALRef extends Ref {
  }

  public static class OPTGRef extends Ref {
  }

  public static class OPTIRef extends Ref {
  }

  public static class OPTRRef extends Ref {
  }

  public static class ORESRef extends Ref {
  }

  public static class EtudeRef extends Ref {
  }

  public static class ScenarioRef extends Ref {
  }

  public static class ModeleRef extends Ref {
  }

  public static class SousModeleRef extends Ref {
  }

  public static class RunRef extends Ref {
  }


  public static class RefConverter implements SingleValueConverter {

    @Override
    public String toString(final Object obj) {
      return ((Ref) obj).NomRef;
    }

    @Override
    public Object fromString(final String name) {
      final Ref pf = new Ref();
      pf.NomRef = name;
      return pf;
    }

    @Override
    public boolean canConvert(final Class type) {
      return type.isInstance(Ref.class);
    }
  }
}
