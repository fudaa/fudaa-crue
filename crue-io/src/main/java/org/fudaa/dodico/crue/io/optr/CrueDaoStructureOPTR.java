/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.optr;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.common.CrueHelper;
import org.fudaa.dodico.crue.io.common.EnumsConverter;
import org.fudaa.dodico.crue.metier.emh.EnumMethodeOrdonnancement;

/**
 * Structures de CrueDaoOPTI.
 * 
 * @author Adrien Hadoux
 */
public class CrueDaoStructureOPTR implements CrueDataDaoStructure {

  /**
   * @param ignoreSortie
   */
  public CrueDaoStructureOPTR() {
    super();
  }

  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    xstream.alias("OPTR", CrueDaoOPTR.class);
    xstream.alias("RegleCompatibiliteCLimM", RegleCompatibiliteCLimM.class);
    xstream.alias("OrdreDRSO", OrdreDRSO.class);
    xstream.alias("MethodeOrdonnancement", MethodeOrdonnancement.class);
    xstream.alias("RegleSignalerObjetsInactifs", RegleSignalerObjetsInactifs.class);
    CrueHelper.configureSorties(xstream, ctuluLog);
    xstream.registerConverter(EnumsConverter.createEnumConverter(EnumMethodeOrdonnancement.class, ctuluLog));
  }

  public static class RegleOPTR {
    boolean IsActive;
  }

  public static class RegleCompatibiliteCLimM extends RegleOPTR {}

  public static class RegleSignalerObjetsInactifs extends RegleOPTR {}

  public static abstract class MethodeOrdonnancementItem {
    
    public abstract EnumMethodeOrdonnancement getMethode();
  }
  
  public static class MethodeOrdonnancement{
    OrdreDRSO OrdreDRSO;
  }
  

  public static class OrdreDRSO extends MethodeOrdonnancementItem {

    @Override
    public EnumMethodeOrdonnancement getMethode() {
      return EnumMethodeOrdonnancement.ORDRE_DRSO;
    }
    
    

  }

}
