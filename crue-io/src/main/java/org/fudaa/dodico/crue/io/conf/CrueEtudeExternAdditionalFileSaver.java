/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.conf;

import java.io.File;

/**
 *
 * @author Frederic Deniger
 */
public interface CrueEtudeExternAdditionalFileSaver {

  /**
   *
   * @param configDir le répertoire des configs.
   * @param dessinDir le répertoire des dessins Fudaa-Crue
   */
  void save(File configDir, File dessinDir);
}
