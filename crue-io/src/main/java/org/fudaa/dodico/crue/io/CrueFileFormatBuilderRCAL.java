/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.rcal.CrueConverterRCAL;
import org.fudaa.dodico.crue.io.rcal.CrueDaoRCAL;
import org.fudaa.dodico.crue.io.rcal.CrueDaoStructureRCAL;
import org.fudaa.dodico.crue.metier.CrueFileType;

public class CrueFileFormatBuilderRCAL implements CrueFileFormatBuilder<CrueDaoRCAL> {
  @Override
  public Crue10FileFormat<CrueDaoRCAL> getFileFormat(final CoeurConfigContrat version) {
    return new Crue10FileFormat<>(new CrueDataXmlReaderWriterImpl<>(CrueFileType.RCAL,
        version, new CrueConverterRCAL(), new CrueDaoStructureRCAL()));
  }
}
