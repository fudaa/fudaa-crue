/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.optg;

import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.optg.CrueDaoStructureOPTG.Planimetrage;
import org.fudaa.dodico.crue.io.optg.CrueDaoStructureOPTG.RegleDAO;
import org.fudaa.dodico.crue.metier.emh.Sorties;

/**
 * Classe persistante qui reprend la meme structure que le fichier xml OPTG - Fichier des ordres pour le pretraitement
 * geometrique (xml). Cela permettra de persister plus facilement la donnee via xstream
 * 
 * @author Adrien Hadoux
 */
public class CrueDaoOPTG extends AbstractCrueDao {
  public Sorties Sorties;
  public Planimetrage Planimetrage;

  public List<RegleDAO> Regles;

}
