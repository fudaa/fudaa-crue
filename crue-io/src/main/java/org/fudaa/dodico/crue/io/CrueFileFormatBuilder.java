/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;

/**
 * @author deniger
 *
 * @param <T>
 */
public interface  CrueFileFormatBuilder<T> {
  
  
  Crue10FileFormat<T> getFileFormat(CoeurConfigContrat version); 

}
