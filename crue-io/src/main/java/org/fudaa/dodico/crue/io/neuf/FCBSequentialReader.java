/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import gnu.trove.TObjectIntHashMap;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.io.CacheFileInputStream;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.ResultatCalcul;
import org.fudaa.dodico.crue.metier.result.Crue9ResultatCalculPasDeTemps;

/**
 * Structure de données fcb construite à la volée avec les infos fcb qui vont bien. garde en memoire les indices des pas de temps, les noeuds
 * utilises, les branches, l'indice de regulation, le nb de pas de temps crue et regulation.
 *
 * @author Adrien Hadoux
 */
public class FCBSequentialReader {

  private final class DataContainerEmpty extends EnteteContainer {

    public DataContainerEmpty() {
      super(Collections.emptyList(), null);
    }
  }

  /**
   * @author deniger
   * @creation 27 mai 2009
   * @version
   * @param <T> l'entete
   * @param <R> le resultat
   */
  public class EnteteContainer<T extends FCBValueObject.AbstractEntete, R extends FCBValueObject.AbstractRes> {

    private final List<T> listData;
    private final Crue9ResBuilder<R> resBuilder;
    private final TObjectIntHashMap namePos;
    private final int nb;

    protected EnteteContainer(final List<T> struc, final Crue9ResBuilder<R> resBuilder) {
      super();
      this.listData = Collections.unmodifiableList(struc);
      this.resBuilder = resBuilder;
      namePos = new TObjectIntHashMap();
      int idx = 0;
      for (final T t : struc) {
        final String nom = t.getNom();
        if (StringUtils.isNotBlank(nom)) {
          namePos.put(nom.toUpperCase(), idx);
        }
        idx++;
      }
      nb = listData.size();
    }

    /**
     * @param emh l'emh en question
     * @return le resultat contenu dans une infoEMH
     */
    public ResultatCalcul createInfoEMH(final EMH emh) {
      return new ResultatCalcul(emh, new ResultatCalculCrue9<>(emh, getPosition(emh.getId()), this), true);
    }

    public ResultatCalcul createInfoEMH(final EMH emh, final int position) {
      return new ResultatCalcul(emh, new ResultatCalculCrue9<>(emh, position, this), true);
    }

    /**
     * @param nom le nom
     * @return la position de lecture. -1 si non present
     */
    public T getData(final String nom) {
      final int i = getPosition(nom);
      return i < 0 ? null : getListData().get(i);
    }

    /**
     * @param nom le nom
     * @return la position de lecture. -1 si non present
     */
    public T getData(final int i) {
      return (i < 0 || i >= getListData().size()) ? null : getListData().get(i);
    }

    /**
     * @param idx l'indice
     * @return le nom de la donnes
     */
    public String getDataName(final int idx) {
      return listData.get(idx).getNom();
    }

    /**
     * @return liste non modifiable des donnees.
     */
    public List<T> getListData() {
      return listData;
    }

    /**
     * @return le nombre de données
     */
    public int getNbData() {
      return nb;
    }

    public Crue9ResultatCalculPasDeTemps getPasDeTemps() {
      return FCBSequentialReader.this.getPdt();
    }

    /**
     * @param nom le nom
     * @return la position de lecture. -1 si non present
     */
    public int getPosition(final String nom) {
      if (namePos.contains(nom)) {
        return namePos.get(nom);
      }
      return -1;
    }

    protected Crue9ResBuilder<R> getResBuilder() {
      return resBuilder;
    }

    /**
     * @param idxPt
     * @param nomEMH
     * @param inout
     * @return le resultat lu
     * @throws IOException
     */
    public R read(final int idxPt, final String nomEMH, final R inout) throws IOException {
      return getResultat(idxPt, nomEMH, inout, this);
    }

    /**
     * @param idxPdt
     * @param nomEMH
     * @param inout
     * @return le resultat lu
     * @throws IOException
     */
    public R read(final int idxPdt, final int nomEMH, final R inout) throws IOException {
      return getResultat(idxPdt, nomEMH, inout, this);
    }
  }

  private class ResBuilderBranche implements Crue9ResBuilder<FCBValueObject.ResBranche> {

    @Override
    public FCBValueObject.ResBranche createRes() {
      return new FCBValueObject.ResBranche();
    }

    @Override
    public int getOffset(final int idxEnr) {
      return getTailleBlocProfil() + 4 + idxEnr * TAILLE_ENR_BRANCHE;
    }

    @Override
    public int getTailleEnr() {
      return TAILLE_ENR_BRANCHE;
    }
  }

  private class ResBuilderNoeud implements Crue9ResBuilder<FCBValueObject.ResCasier> {

    @Override
    public FCBValueObject.ResCasier createRes() {
      return new FCBValueObject.ResCasier();
    }

    @Override
    public int getOffset(final int idxEnr) {
      return getTailleBlocProfil() + getTailleBlocBranche() + 4 + idxEnr * TAILLE_ENR_NOEUD;
    }

    @Override
    public int getTailleEnr() {
      return TAILLE_ENR_NOEUD;
    }
  }

  private static class ResBuilderProfil implements Crue9ResBuilder<FCBValueObject.ResProfil> {

    @Override
    public FCBValueObject.ResProfil createRes() {
      return new FCBValueObject.ResProfil();
    }

    @Override
    public int getOffset(final int idxEnr) {
      return 4 + idxEnr * TAILLE_ENR_PROFIL;
    }

    @Override
    public int getTailleEnr() {
      return TAILLE_ENR_PROFIL;
    }
  }
  private static final int TAILLE_ENR_PROFIL = 20;
  private static final int TAILLE_ENR_BRANCHE = 16;
  private static final int TAILLE_ENR_NOEUD = 12;
  private final static Crue9ResultatCalculPasDeTemps EMPTY_PDT = new Crue9ResultatCalculPasDeTemps();
  private final EnteteContainer EMPTY = new DataContainerEmpty();
  /**
   * le channel.
   */
  // protected FileChannel channel;
  protected List<String> commentDc;
  protected List<String> commentDh;
  /**
   * le fichier associé.
   */
  protected final File file;
  /**
   * true si le fichier contient une régulation
   */
  protected boolean regulation;
  protected boolean sousFente;
  private Crue9ResultatCalculPasDeTemps pdt = EMPTY_PDT;
  private EnteteContainer<FCBValueObject.EnteteProfil, FCBValueObject.ResProfil> containerProfils = EMPTY;
  private EnteteContainer<FCBValueObject.EnteteBranche, FCBValueObject.ResBranche> containerBranches = EMPTY;
  private EnteteContainer<FCBValueObject.EnteteCasier, FCBValueObject.ResCasier> containerCasiers = EMPTY;
  private ByteOrder order;
  private final static Logger LOGGER = Logger.getLogger(FCBSequentialReader.class.getName());

  /**
   * Constructeur de la structure d'accès rapide fcb.
   *
   * @param file
   */
  public FCBSequentialReader(final File file) {
    this.file = file;
  }

  private ByteBuffer createByteBuffer(final int taille) {
    final ByteBuffer allocate = ByteBuffer.allocate(taille);
    if (order != null) {
      allocate.order(order);
    }
    return allocate;
  }

  /**
   * @return the commentDc
   */
  public List<String> getCommentDc() {
    return commentDc;
  }

  /**
   * @return the commentDh
   */
  public List<String> getCommentDh() {
    return commentDh;
  }

  /**
   * @return le container des branches
   */
  public EnteteContainer<FCBValueObject.EnteteBranche, FCBValueObject.ResBranche> getContainerBranches() {
    return containerBranches;
  }

  /**
   * @return le container des noeuds.
   */
  public EnteteContainer<FCBValueObject.EnteteCasier, FCBValueObject.ResCasier> getContainerCasiers() {
    return containerCasiers;
  }

  /**
   * @return le container des profils
   */
  public EnteteContainer<FCBValueObject.EnteteProfil, FCBValueObject.ResProfil> getContainerProfil() {
    return containerProfils;
  }

  /**
   * @return the nbBranches
   */
  public int getNbBranches() {
    return containerBranches.getNbData();
  }

  /**
   * @return le nombre de noeuds
   */
  public Object getNbNoeuds() {
    return containerCasiers.getNbData();
  }

  /**
   * @return the nbProfils
   */
  public int getNbProfils() {
    return containerProfils.getNbData();
  }

  protected ByteOrder getOrder() {
    return order;
  }

  /**
   * @return les pas de temps normaux
   */
  public Crue9ResultatCalculPasDeTemps getPdt() {
    return pdt;
  }

  private <T extends FCBValueObject.AbstractEntete, R extends FCBValueObject.AbstractRes> R getResultat(
          final int idxPdt, final String nomEMH, final R inout, final EnteteContainer<T, R> dataContainer)
          throws IOException {
    final int idxBranche = dataContainer.getPosition(nomEMH);
    if (idxBranche < 0) {
      throw new IOException("io.fcb.emh.notFound.error");
    }
    return getResultat(idxPdt, idxBranche, inout, dataContainer);

  }

  private <T extends FCBValueObject.AbstractEntete, R extends FCBValueObject.AbstractRes> R getResultat(
          final int idxPdt, final int idxBranche, final R inout, final EnteteContainer<T, R> dataContainer)
          throws IOException {
    final long positionTimeStep = pdt.getPosition(idxPdt);
    if (positionTimeStep < 0) {
      throw new IOException("io.fcb.timeStep.notFound.error");
    }

    final Crue9ResBuilder<R> resBuilder = dataContainer.getResBuilder();
    if (resBuilder == null) {
      throw new IOException("io.fcb.emh.noBuilder.error");
    }
    // on se positionne au debut du profil
    // +4 pour l'entier indiquant la longueur d'enregistrement du bloc.
    // les donnees branches se trouvent apres les profils d'ou le + getTailleBlocProfil()
    final long deb = positionTimeStep + resBuilder.getOffset(idxBranche);
    final ByteBuffer bf = readByte(resBuilder, deb, dataContainer);
    R res = inout;
    // si le parametre inout est null on le cree.
    if (res == null) {
      res = resBuilder.createRes();
    }
    res.read(bf);
    return res;
  }

  /**
   * @param idxPdt l'indice du pas de temps normal
   * @param nomBranche le nom de la branche
   * @param inout le resultat a modifier. si null, une nouvelle instance est créée
   * @return le resultat. inout si non null
   * @throws IOException si erreur de lecture
   */
  public FCBValueObject.ResBranche getResultatBranche(final int idxPdt, final String nomBranche,
          final FCBValueObject.ResBranche inout) throws IOException {
    return getResultat(idxPdt, nomBranche, inout, containerBranches);
  }

  /**
   * @param idxPdt l'indice du pas de temps normal
   * @param nomNoeud le nom de la branche
   * @param inout le resultat a modifier. si null, une nouvelle instance est créée
   * @return le resultat. inout si non null
   * @throws IOException si erreur de lecture
   */
  public FCBValueObject.ResCasier getResultatNoeud(final int idxPdt, final String nomNoeud,
          final FCBValueObject.ResCasier inout) throws IOException {
    return getResultat(idxPdt, nomNoeud, inout, containerCasiers);

  }

  /**
   * @param idxPdt l'indice du pas de temps normal
   * @param nomProfil le nom du profil
   * @param inout le resultat a modifier. si null, une nouvelle instance est créée
   * @return le resultat. inout si non null
   * @throws IOException si erreur de lecture
   */
  public FCBValueObject.ResProfil getResultatProfil(final int idxPdt, final String nomProfil,
          final FCBValueObject.ResProfil inout) throws IOException {
    return getResultat(idxPdt, nomProfil, inout, containerProfils);

  }

  /**
   * @param idxPdt l'indice du pas de temps normal
   * @param nomBranche le nom de la branche
   * @param inout le resultat a modifier. si null, une nouvelle instance est créée
   * @return le resultat. inout si non null
   * @throws IOException si erreur de lecture
   */
  public FCBValueObject.ResBranche getResultatBranche(final int idxPdt, final int nomBranche,
          final FCBValueObject.ResBranche inout) throws IOException {
    return getResultat(idxPdt, nomBranche, inout, containerBranches);
  }

  /**
   * @param idxPdt l'indice du pas de temps normal
   * @param nomNoeud le nom de la branche
   * @param inout le resultat a modifier. si null, une nouvelle instance est créée
   * @return le resultat. inout si non null
   * @throws IOException si erreur de lecture
   */
  public FCBValueObject.ResCasier getResultatNoeud(final int idxPdt, final int nomNoeud,
          final FCBValueObject.ResCasier inout) throws IOException {
    return getResultat(idxPdt, nomNoeud, inout, containerCasiers);

  }

  /**
   * @param idxPdt l'indice du pas de temps normal
   * @param nomProfil le nom du profil
   * @param inout le resultat a modifier. si null, une nouvelle instance est créée
   * @return le resultat. inout si non null
   * @throws IOException si erreur de lecture
   */
  public FCBValueObject.ResProfil getResultatProfil(final int idxPdt, final int nomProfil,
          final FCBValueObject.ResProfil inout) throws IOException {
    return getResultat(idxPdt, nomProfil, inout, containerProfils);

  }

  private int getTailleBlocBranche() {
    return 8 + getNbBranches() * TAILLE_ENR_BRANCHE;
  }

  private int getTailleBlocProfil() {
    // 8 pour les 2 entiers englobantsl'enregistrement
    return 8 + getNbProfils() * TAILLE_ENR_PROFIL;
  }

  /**
   * @return the isRegulation
   */
  public boolean isRegulation() {
    return regulation;
  }

  /**
   * @return the sousFente
   */
  public boolean isSousFente() {
    return sousFente;
  }

  private <R extends FCBValueObject.AbstractRes> ByteBuffer readByte(final Crue9ResBuilder<R> resBuilder, final long deb, final EnteteContainer dataContainer) {
    final ByteBuffer bf = createByteBuffer(resBuilder.getTailleEnr());
    FileInputStream fileInputStream = null;
    try {
      fileInputStream = CacheFileInputStream.getINSTANCE().open(file);
      final FileChannel channel = fileInputStream.getChannel();
      channel.read(bf, deb);
    } catch (final Exception e) {
      LOGGER.log(Level.SEVERE, "readByte", e);
    } finally {
      CacheFileInputStream.getINSTANCE().close(file, fileInputStream);
    }
    bf.rewind();
    return bf;
  }

  /**
   * @param branches the branches to set
   */
  protected void setBranches(final List<FCBValueObject.EnteteBranche> branches) {
    this.containerBranches = new EnteteContainer<>(branches,
            new ResBuilderBranche());
  }

  protected void setNoeuds(final List<FCBValueObject.EnteteCasier> noeuds) {
    this.containerCasiers = new EnteteContainer<>(noeuds,
            new ResBuilderNoeud());
  }

  protected void setOrder(final ByteOrder order) {
    this.order = order;
  }

  protected void setPdt(final Crue9ResultatCalculPasDeTemps pdt) {
    this.pdt = pdt.getNbResultats() == 0 ? EMPTY_PDT : pdt;
  }

  /**
   * @param profils the profils to set
   */
  protected void setProfils(final List<FCBValueObject.EnteteProfil> profils) {
    this.containerProfils = new EnteteContainer<>(profils,
            new ResBuilderProfil());
  }
}
