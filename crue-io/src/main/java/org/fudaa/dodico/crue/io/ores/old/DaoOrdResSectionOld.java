/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores.old;

public class DaoOrdResSectionOld {
  // WARN: l'ordre des champs est important car utilise par l'ecriture/lecture de ORES
  private boolean ddeDact;
  private boolean ddeDtot;
  private boolean ddeFr;
  private boolean ddeH;
  private boolean ddeHs;
  private boolean ddeLact;
  private boolean ddeLtot;
  private boolean ddeQ;
  private boolean ddeSact;
  private boolean ddeStot;
  private boolean ddeVact;
  private boolean ddeVc;
  private boolean ddeVtot;
  private boolean ddeY;
  private boolean ddeYc;
  private boolean ddeYn;
  private boolean ddeZ;
  private boolean ddeZc;
  private boolean ddeZf;
  private boolean ddeZn;

  public DaoOrdResSectionOld() {
  }

  public DaoOrdResSectionOld(final DaoOrdResSectionOld other) {
    this.ddeDact = other.ddeDact;
    this.ddeDtot = other.ddeDtot;
    this.ddeFr = other.ddeFr;
    this.ddeH = other.ddeH;
    this.ddeHs = other.ddeHs;
    this.ddeLact = other.ddeLact;
    this.ddeLtot = other.ddeLtot;
    this.ddeQ = other.ddeQ;
    this.ddeSact = other.ddeSact;
    this.ddeStot = other.ddeStot;
    this.ddeVact = other.ddeVact;
    this.ddeVc = other.ddeVc;
    this.ddeVtot = other.ddeVtot;
    this.ddeY = other.ddeY;
    this.ddeYc = other.ddeYc;
    this.ddeYn = other.ddeYn;
    this.ddeZ = other.ddeZ;
    this.ddeZc = other.ddeZc;
    this.ddeZf = other.ddeZf;
    this.ddeZn = other.ddeZn;
  }

  public final boolean getDdeDact() {
    return ddeDact;
  }

  public final boolean getDdeDtot() {
    return ddeDtot;
  }

  public final boolean getDdeFr() {
    return ddeFr;
  }

  public final boolean getDdeH() {
    return ddeH;
  }

  public boolean getDdeHs() {
    return ddeHs;
  }

  public final boolean getDdeLact() {
    return ddeLact;
  }

  public final boolean getDdeLtot() {
    return ddeLtot;
  }

  public final boolean getDdeQ() {
    return ddeQ;
  }

  public final boolean getDdeSact() {
    return ddeSact;
  }

  public final boolean getDdeStot() {
    return ddeStot;
  }

  public final boolean getDdeVact() {
    return ddeVact;
  }

  public final boolean getDdeVc() {
    return ddeVc;
  }

  public final boolean getDdeVtot() {
    return ddeVtot;
  }

  public boolean getDdeY() {
    return ddeY;
  }

  public final boolean getDdeYc() {
    return ddeYc;
  }

  public final boolean getDdeYn() {
    return ddeYn;
  }

  public final boolean getDdeZ() {
    return ddeZ;
  }

  public final boolean getDdeZc() {
    return ddeZc;
  }

  public final boolean getDdeZf() {
    return ddeZf;
  }

  public final boolean getDdeZn() {
    return ddeZn;
  }

  public final void setDdeDact(final boolean newDdeDact) {
    ddeDact = newDdeDact;
  }

  public final void setDdeDtot(final boolean newDdeDtot) {
    ddeDtot = newDdeDtot;
  }

  public final void setDdeFr(final boolean newDdeF) {
    ddeFr = newDdeF;
  }

  public final void setDdeH(final boolean newDdeH) {
    ddeH = newDdeH;
  }

  /**
   * @param ddeHs the ddeHs to set
   */
  public void setDdeHs(final boolean ddeHs) {
    this.ddeHs = ddeHs;
  }

  public final void setDdeLact(final boolean newDdeLact) {
    ddeLact = newDdeLact;
  }

  public final void setDdeLtot(final boolean newDdeLtot) {
    ddeLtot = newDdeLtot;
  }

  public final void setDdeQ(final boolean newDdeQ) {
    ddeQ = newDdeQ;
  }

  public final void setDdeSact(final boolean newDdeSact) {
    ddeSact = newDdeSact;
  }

  public final void setDdeStot(final boolean newDdeStot) {
    ddeStot = newDdeStot;
  }

  public final void setDdeVact(final boolean newDdeVact) {
    ddeVact = newDdeVact;
  }

  public final void setDdeVc(final boolean newDdeVc) {
    ddeVc = newDdeVc;
  }

  public final void setDdeVtot(final boolean newDdeVtot) {
    ddeVtot = newDdeVtot;
  }

  /**
   * @param ddeY the ddeY to set
   */
  public void setDdeY(final boolean ddeY) {
    this.ddeY = ddeY;
  }

  public final void setDdeYc(final boolean newDdeYc) {
    ddeYc = newDdeYc;
  }

  public final void setDdeYn(final boolean newDdeYn) {
    ddeYn = newDdeYn;
  }

  public final void setDdeZ(final boolean newDdeZ) {
    ddeZ = newDdeZ;
  }

  public final void setDdeZc(final boolean newDdeZc) {
    ddeZc = newDdeZc;
  }

  public final void setDdeZf(final boolean newDdeZf) {
    ddeZf = newDdeZf;
  }

  public final void setDdeZn(final boolean newDdeZn) {
    ddeZn = newDdeZn;
  }

  @Override
  public String toString() {
    return "OrdResSectionProfil [ddeDact=" + ddeDact + ", ddeDtot=" + ddeDtot + ", ddeFr=" + ddeFr + ", ddeH=" + ddeH
        + ", ddeHs=" + ddeHs + ",...]";
  }

  public boolean isSame(final DaoOrdResSectionOld other) {
    if (this == other) {
      return true;
    }
    if (other == null) {
      return false;
    }
    if (ddeDact != other.ddeDact) {
      return false;
    }
    if (ddeDtot != other.ddeDtot) {
      return false;
    }
    if (ddeFr != other.ddeFr) {
      return false;
    }
    if (ddeH != other.ddeH) {
      return false;
    }
    if (ddeHs != other.ddeHs) {
      return false;
    }
    if (ddeLact != other.ddeLact) {
      return false;
    }
    if (ddeLtot != other.ddeLtot) {
      return false;
    }
    if (ddeQ != other.ddeQ) {
      return false;
    }
    if (ddeSact != other.ddeSact) {
      return false;
    }
    if (ddeStot != other.ddeStot) {
      return false;
    }
    if (ddeVact != other.ddeVact) {
      return false;
    }
    if (ddeVc != other.ddeVc) {
      return false;
    }
    if (ddeVtot != other.ddeVtot) {
      return false;
    }
    if (ddeY != other.ddeY) {
      return false;
    }
    if (ddeYc != other.ddeYc) {
      return false;
    }
    if (ddeYn != other.ddeYn) {
      return false;
    }
    if (ddeZ != other.ddeZ) {
      return false;
    }
    if (ddeZc != other.ddeZc) {
      return false;
    }
    if (ddeZf != other.ddeZf) {
      return false;
    }
    return ddeZn == other.ddeZn;
  }
}
