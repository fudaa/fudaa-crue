package org.fudaa.dodico.crue.io.log;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueDaoStructure;

/**
 * @author CANEL Christophe
 *
 */
public class CrueDaoStructureLOG implements CrueDaoStructure {

  /**
   * {@inheritDoc}
   */
  @Override
  public void configureXStream(final XStream xstream, final CtuluLog analyze)
  {
    xstream.alias("Logger", CrueDaoLOG.class);
    xstream.alias("Log", Log.class);
  }

  protected static class Log
  {
    public String Level;
    public String Message;
  }
}
