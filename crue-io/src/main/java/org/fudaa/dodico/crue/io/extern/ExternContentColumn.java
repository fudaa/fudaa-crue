/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.extern;

import gnu.trove.TDoubleArrayList;

/**
 *
 * @author Frederic Deniger
 */
public class ExternContentColumn {

  String title;
  final TDoubleArrayList values = new TDoubleArrayList();

  public String getTitle() {
    return title;
  }

  void setTitle(final String title) {
    this.title = title;
  }

  public double[] toNativeArray() {
    return values.toNativeArray();
  }

  protected void addValue(final double x) {
    values.add(x);
  }

  public int getSize() {
    return values.size();
  }

  public double getValue(final int i) {
    return values.get(i);
  }
}
