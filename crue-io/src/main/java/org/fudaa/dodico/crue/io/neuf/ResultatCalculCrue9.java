/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.ResultatCalculDelegate;
import org.fudaa.dodico.crue.metier.emh.ResultatPasDeTempsDelegate;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.metier.result.Crue9ResultatCalculPasDeTemps;
import org.fudaa.dodico.crue.metier.result.ResCalcul;

/**
 *
 * @author deniger
 */
public class ResultatCalculCrue9<T extends FCBValueObject.AbstractEntete, R extends FCBValueObject.AbstractRes>
        implements ResultatCalculDelegate {

  final EMH emh;
  final int position;
  final FCBSequentialReader.EnteteContainer<T, R> entete;

  /**
   * @param emh
   * @param entete
   */
  public ResultatCalculCrue9(EMH emh, int position,
          FCBSequentialReader.EnteteContainer<T, R> entete) {
    super();
    this.emh = emh;
    this.position = position;
    this.entete = entete;
  }

  @Override
  public String getEMHType() {
    return emh.getEmhType();
  }

  public R getResContainer() {
    return entete.getResBuilder().createRes();
  }

  public T getEntete() {
    return entete.getData(position);
  }

  public EMH getEmh() {
    return emh;
  }

  @Override
  public List<String> getQZRegulVariablesId() {
    return Collections.emptyList();
  }

  @Override
  public List<String> getQRegulVariablesName() {
    return Collections.emptyList();
  }
  @Override
  public List<String> getZRegulVariablesName() {
    return Collections.emptyList();
  }

  @Override
  public ResultatPasDeTempsDelegate getResultatPasDeTemps() {
    return getCrue9PasDeTemps();
  }

  /**
   * @return les pas de temps du résultats.
   */
  public Crue9ResultatCalculPasDeTemps getCrue9PasDeTemps() {
    return entete.getPasDeTemps();
  }

  /**
   * @param idxPdt
   * @param inout
   * @return le resultat lu
   * @throws IOException
   */
  public ResCalcul read(final int idxPdt, final R inout) throws IOException {
    R read = entete.read(idxPdt, position, inout);
    return read == null ? null : read.createRes(emh);
  }

  /**
   * @param idxPdt
   * @return
   * @throws IOException
   */
  public ResCalcul readCalcul(final int idxPdt) throws IOException {
    R read = entete.read(idxPdt, position, null);
    return read == null ? null : read.createRes(emh);
  }

  @Override
  public Map<String, Object> read(ResultatTimeKey key) throws IOException {
    int index = getCrue9PasDeTemps().getIndex(key);
    if (index < 0) {
      return null;
    }
    return read(index);
  }

  public Map<String, Object> read(int idxPdt) throws IOException {
    ResCalcul res = readCalcul(idxPdt);
    return res == null
            ? Collections.emptyMap()
            : res.getRes();
  }

  /**
   * @return the position
   */
  public int getPosition() {
    return position;
  }
}
