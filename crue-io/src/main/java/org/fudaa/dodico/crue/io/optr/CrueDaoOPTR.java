/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.optr;

import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.optr.CrueDaoStructureOPTR.MethodeOrdonnancement;
import org.fudaa.dodico.crue.io.optr.CrueDaoStructureOPTR.RegleOPTR;
import org.fudaa.dodico.crue.metier.emh.Sorties;

/**
 * Représentation persistante du fichier xml OPTI (Fichier des ordres pour le prétraitement des conditions initiales
 * (xml)).
 * 
 * @author Adrien Hadoux
 */
public class CrueDaoOPTR extends AbstractCrueDao {

  Sorties Sorties;
  MethodeOrdonnancement MethodeOrdonnancement;
  List<RegleOPTR> Regles;

  public CrueDaoOPTR() {}

}
