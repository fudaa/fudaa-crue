/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ocal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.common.CrueHelper;
import org.fudaa.dodico.crue.io.ocal.CrueDaoStructureOCAL.IniCalculPrecedentPersist;
import org.fudaa.dodico.crue.io.ocal.CrueDaoStructureOCAL.OrdCalculAbstractPersist;
import org.fudaa.dodico.crue.io.ocal.CrueDaoStructureOCAL.OrdCalculPseudoPermanentPersist;
import org.fudaa.dodico.crue.io.ocal.CrueDaoStructureOCAL.OrdCalculPseudoPermanentPersistOld;
import org.fudaa.dodico.crue.io.ocal.CrueDaoStructureOCAL.OrdCalculTransitoirePersist;
import org.fudaa.dodico.crue.io.ocal.CrueDaoStructureOCAL.OrdCalculTransitoirePersistOld;
import org.fudaa.dodico.crue.io.ocal.CrueDaoStructureOCAL.OrdCalculVraiPermanentPersist;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPerm;
import org.fudaa.dodico.crue.metier.emh.CalcTrans;
import org.fudaa.dodico.crue.metier.emh.OrdCalc;
import org.fudaa.dodico.crue.metier.emh.OrdCalcPseudoPerm;
import org.fudaa.dodico.crue.metier.emh.OrdCalcPseudoPermIniCalcCI;
import org.fudaa.dodico.crue.metier.emh.OrdCalcPseudoPermIniCalcPrecedent;
import org.fudaa.dodico.crue.metier.emh.OrdCalcPseudoPermIniCliche;
import org.fudaa.dodico.crue.metier.emh.OrdCalcScenario;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTrans;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTransIniCalcCI;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTransIniCalcCliche;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTransIniCalcPrecedent;
import org.fudaa.dodico.crue.metier.emh.OrdCalcVraiPerm;
import org.fudaa.dodico.crue.metier.emh.PdtCst;
import org.fudaa.dodico.crue.metier.emh.PrendreClicheFinPermanent;
import org.fudaa.dodico.crue.metier.emh.PrendreClichePeriodique;
import org.fudaa.dodico.crue.metier.emh.PrendreClichePonctuel;
import org.fudaa.dodico.crue.metier.emh.Sorties;
import org.fudaa.dodico.crue.metier.emh.ui.CalcOrdCalcUiState;
import org.fudaa.dodico.crue.metier.emh.ui.OrdCalcScenarioUiState;
import org.fudaa.dodico.crue.metier.factory.EMHFactory;

/**
 * Classe qui se charge de remplir les structures DAO du fichier OCAL avec les donnees metier et inversement.
 *
 * @author CDE
 */
public class CrueConverterOCAL implements CrueDataConverter<CrueDaoOCAL, OrdCalcScenario> {

  private final boolean oldVersion;

  /**
   *
   * @param oldVersion true si ancienne version (1.1)
   */
  public CrueConverterOCAL(final boolean oldVersion) {
    this.oldVersion = oldVersion;

  }

  public OrdCalcScenarioUiState convertDaoToMetierUiState(final CrueDaoOCAL dao, final CrueConfigMetier ccm,
                                                          final CtuluLog analyser) {
    final OrdCalcScenarioUiState ordCalcScenarioUiState = new OrdCalcScenarioUiState();
    if (CollectionUtils.isNotEmpty(dao.listeOrdCalculs)) {
      for (final OrdCalculAbstractPersist ordCalculPersist : dao.listeOrdCalculs) {
        OrdCalc ordCalc = null;
        if (ordCalculPersist instanceof OrdCalculPseudoPermanentPersist) {
          ordCalc = daoToMetierPseudoPermanent((OrdCalculPseudoPermanentPersist) ordCalculPersist, null, analyser);

        } else if (ordCalculPersist instanceof OrdCalculVraiPermanentPersist) {
          ordCalc = daoToMetierVraiPermanent((OrdCalculVraiPermanentPersist) ordCalculPersist, null, analyser);

        } else if (ordCalculPersist instanceof OrdCalculTransitoirePersist) {
          ordCalc = daoToMetierTransitoire((OrdCalculTransitoirePersist) ordCalculPersist, ccm, null, analyser);

        }
        if (ordCalc == null) {
          analyser.addSevereError("io.ocal.type.calc.indefini.error", ordCalculPersist.NomRef);
        } else {
          ordCalcScenarioUiState.addCalcOrdCalcUiStates(ordCalculPersist.NomRef, ordCalc);
        }

      }
    }
    return ordCalcScenarioUiState;

  }

  /**
   * Conversion des objets DAO en objets métier
   */
  @Override
  public OrdCalcScenario convertDaoToMetier(final CrueDaoOCAL dao, final CrueData dataLinked,
          final CtuluLog ctuluLog) {

    if (dataLinked == null) {
      ctuluLog.addInfo("io.ocal.main.error");
      return null;
    }

    final OrdCalcScenario ordCalc = dataLinked.getOrCreateOCAL();
    if (dao.Sorties != null) {
      ordCalc.getSorties().initWith(dao.Sorties);
    }

    if (CollectionUtils.isNotEmpty(dao.listeOrdCalculs)) {
      for (final OrdCalculAbstractPersist ordCalculPersist : dao.listeOrdCalculs) {

        if (ordCalculPersist instanceof OrdCalculPseudoPermanentPersist) {
          daoToMetierPseudoPermanent((OrdCalculPseudoPermanentPersist) ordCalculPersist, dataLinked, ctuluLog);
        } else if (ordCalculPersist instanceof OrdCalculPseudoPermanentPersistOld) {
          daoToMetierPseudoPermanentOld((OrdCalculPseudoPermanentPersistOld) ordCalculPersist, dataLinked, ctuluLog);

        } else if (ordCalculPersist instanceof OrdCalculVraiPermanentPersist) {
          daoToMetierVraiPermanent((OrdCalculVraiPermanentPersist) ordCalculPersist, dataLinked, ctuluLog);

        } else if (ordCalculPersist instanceof OrdCalculTransitoirePersist) {
          daoToMetierTransitoire((OrdCalculTransitoirePersist) ordCalculPersist, dataLinked.getCrueConfigMetier(), dataLinked, ctuluLog);
        } else if (ordCalculPersist instanceof OrdCalculTransitoirePersistOld) {
          daoToMetierTransitoireOld((OrdCalculTransitoirePersistOld) ordCalculPersist, dataLinked, ctuluLog);
        }

      }

    }

    return dataLinked.getOCAL();
  }

  /**
   * Conversion des objets métier en objets DAO
   */
  @Override
  public CrueDaoOCAL convertMetierToDao(final OrdCalcScenario metier, final CtuluLog ctuluLog) {

    final CrueDaoOCAL dao = new CrueDaoOCAL();
    if (!oldVersion) {
      dao.Sorties = new Sorties(metier.getSorties());
    }
    final Collection<OrdCalc> ordCalcList = metier.getOrdCalc();
    if (ordCalcList == null) {
      return dao;
    }
    dao.listeOrdCalculs = new ArrayList<>(ordCalcList.size());
    for (final OrdCalc ordCalc : ordCalcList) {
      OrdCalculAbstractPersist res = null;

      final Class<? extends OrdCalc> clazz = ordCalc.getClass();
      //pseudo-perm:
      if (clazz.equals(OrdCalcPseudoPermIniCalcCI.class)) {
        res = metierToDaoPseudoPermIniCalcCI((OrdCalcPseudoPermIniCalcCI) ordCalc);
      } else if (clazz.equals(OrdCalcPseudoPermIniCalcPrecedent.class)) {
        res = metierToDaoPseudoPermIniCalcPrecedent((OrdCalcPseudoPermIniCalcPrecedent) ordCalc);
      } else if (clazz.equals(OrdCalcPseudoPermIniCliche.class)) {
        res = metierToDaoPseudoPermIniCliche((OrdCalcPseudoPermIniCliche) ordCalc, ctuluLog);
      } else if (clazz.equals(OrdCalcVraiPerm.class)) {
        final OrdCalculVraiPermanentPersist vraiPersist = new OrdCalculVraiPermanentPersist();
        res = vraiPersist;
        //transitoire
      } else if (clazz.equals(OrdCalcTransIniCalcPrecedent.class)) {
        res = metierToDaoTransIniCalcPrecedent((OrdCalcTransIniCalcPrecedent) ordCalc);
      } else if (clazz.equals(OrdCalcTransIniCalcCliche.class)) {
        res = metierToDaoTransIniCalcCliche((OrdCalcTransIniCalcCliche) ordCalc, ctuluLog);
      } else if (clazz.equals(OrdCalcTransIniCalcCI.class)) {
        res = metierToDaoTransIniCalcReprise((OrdCalcTransIniCalcCI) ordCalc, ctuluLog);
      }

      if (res == null) {
        ctuluLog.addSevereError("io.ocal.type.calc.indefini.error", clazz.toString());
      } else {
        res.NomRef = ordCalc.getCalc().getNom();

      }
      dao.listeOrdCalculs.add(res);
    }

    return dao;
  }

  public CrueDaoOCAL convertMetierToDaoUiState(final OrdCalcScenarioUiState metier,
          final CtuluLog analyser) {
    final CrueDaoOCAL dao = new CrueDaoOCAL();
    final List<CalcOrdCalcUiState> ordCalcList = metier.getUiStates();
    if (ordCalcList == null) {
      return dao;
    }
    dao.listeOrdCalculs = new ArrayList<>(ordCalcList.size());
    for (final CalcOrdCalcUiState uiOrdCalc : ordCalcList) {
      OrdCalculAbstractPersist res = null;
      final OrdCalc ordCalc = uiOrdCalc.getOrdCalc();

      final Class<? extends OrdCalc> clazz = ordCalc.getClass();
      //pseudo-perm:
      if (clazz.equals(OrdCalcPseudoPermIniCalcCI.class)) {
        res = metierToDaoPseudoPermIniCalcCI((OrdCalcPseudoPermIniCalcCI) ordCalc);
      } else if (clazz.equals(OrdCalcPseudoPermIniCalcPrecedent.class)) {
        res = metierToDaoPseudoPermIniCalcPrecedent((OrdCalcPseudoPermIniCalcPrecedent) ordCalc);
      } else if (clazz.equals(OrdCalcPseudoPermIniCliche.class)) {
        res = metierToDaoPseudoPermIniCliche((OrdCalcPseudoPermIniCliche) ordCalc, analyser);
      } else if (clazz.equals(OrdCalcVraiPerm.class)) {
        final OrdCalculVraiPermanentPersist vraiPersist = new OrdCalculVraiPermanentPersist();
        res = vraiPersist;
        //transitoire
      } else if (clazz.equals(OrdCalcTransIniCalcPrecedent.class)) {
        res = metierToDaoTransIniCalcPrecedent((OrdCalcTransIniCalcPrecedent) ordCalc);
      } else if (clazz.equals(OrdCalcTransIniCalcCliche.class)) {
        res = metierToDaoTransIniCalcCliche((OrdCalcTransIniCalcCliche) ordCalc, analyser);
      } else if (clazz.equals(OrdCalcTransIniCalcCI.class)) {
        res = metierToDaoTransIniCalcReprise((OrdCalcTransIniCalcCI) ordCalc, analyser);
      }

      if (res == null) {
        analyser.addSevereError("io.ocal.type.calc.indefini.error", clazz.toString());
      } else {
        res.NomRef = uiOrdCalc.getCalcId();

      }
      dao.Sorties = new Sorties();
      dao.listeOrdCalculs.add(res);
    }

    return dao;
  }

  private OrdCalculAbstractPersist metierToDaoTransIniCalcPrecedent(final OrdCalcTransIniCalcPrecedent trans) {
    if (oldVersion) {
      final OrdCalculTransitoirePersistOld calcTransPersist = new OrdCalculTransitoirePersistOld();
      final IniCalculPrecedentPersist iniCalculPrecedentPersist = new IniCalculPrecedentPersist();
      calcTransPersist.IniCalcPrecedent = iniCalculPrecedentPersist;
      return calcTransPersist;
    }
    final OrdCalculTransitoirePersist calcTransPersist = new OrdCalculTransitoirePersist();
    final IniCalculPrecedentPersist iniCalculPrecedentPersist = new IniCalculPrecedentPersist();
    calcTransPersist.IniCalcPrecedent = iniCalculPrecedentPersist;
    metierToDaoTransCommonAttributes(calcTransPersist, trans);
    return calcTransPersist;
  }

  private void metierToDaoTransCommonAttributes(final OrdCalculTransitoirePersist calcTransPersist, final OrdCalcTrans trans) {
    calcTransPersist.DureeCalc = DateDurationConverter.durationToXsd(trans.getDureeCalc());
    calcTransPersist.PdtRes = new CrueDaoStructureOCAL.PdtResPersist();
    if (trans.getPdtRes() != null && trans.getPdtRes() instanceof PdtCst) {
      calcTransPersist.PdtRes.PdtCst = DateDurationConverter.durationToXsd(((PdtCst) trans.getPdtRes()).getPdtCst());
    }
    if (trans.getPrendreClichePeriodique() != null && trans.getPrendreClichePeriodique().getPdtRes() != null) {
      calcTransPersist.PrendreClichePeriodique = new CrueDaoStructureOCAL.PrendreClichePeriodiquePersist();
      calcTransPersist.PrendreClichePeriodique.NomFic = trans.getPrendreClichePeriodique().getNomFic();
      calcTransPersist.PrendreClichePeriodique.PdtRes = new CrueDaoStructureOCAL.PdtResPersist();
      calcTransPersist.PrendreClichePeriodique.PdtRes.PdtCst = DateDurationConverter.durationToXsd(
              ((PdtCst) trans.getPrendreClichePeriodique().getPdtRes()).getPdtCst());
    }
    if (trans.getPrendreClichePonctuel() != null && trans.getPrendreClichePonctuel().getTempsSimu() != null) {
      calcTransPersist.PrendreClichePonctuel = new CrueDaoStructureOCAL.PrendreClichePonctuelPersist();
      calcTransPersist.PrendreClichePonctuel.NomFic = trans.getPrendreClichePonctuel().getNomFic();
      calcTransPersist.PrendreClichePonctuel.TempsSimu = DateDurationConverter.durationToXsd(
              trans.getPrendreClichePonctuel().getTempsSimu());
    }
  }

  private OrdCalculAbstractPersist metierToDaoTransIniCalcCliche(final OrdCalcTransIniCalcCliche trans, final CtuluLog log) {
    if (oldVersion) {
      log.addSevereError("io.occal.clicheNoSupportedIn1.1.1", trans.getCalc().getNom());
      return null;
    }
    final OrdCalculTransitoirePersist calcTransPersist = new OrdCalculTransitoirePersist();
    final CrueDaoStructureOCAL.IniCalcClichePersist iniCalculPrecedentPersist = new CrueDaoStructureOCAL.IniCalcClichePersist();
    calcTransPersist.IniCalcCliche = iniCalculPrecedentPersist;
    calcTransPersist.IniCalcCliche.NomFic = trans.getIniCalcCliche().getNomFic();
    metierToDaoTransCommonAttributes(calcTransPersist, trans);
    return calcTransPersist;
  }

  private OrdCalculAbstractPersist metierToDaoPseudoPermIniCalcCI(final OrdCalcPseudoPermIniCalcCI calcPseudoPerm) {
    if (oldVersion) {
      return metierToDaoPseudoPermIniCalcCIOld(calcPseudoPerm);
    }
    final OrdCalculPseudoPermanentPersist calcPermPersist = createPseudoPermPersist(calcPseudoPerm);
    calcPermPersist.IniCalcCI = new CrueDaoStructureOCAL.IniCalcCIPersist();
    return calcPermPersist;
  }

  private OrdCalculAbstractPersist metierToDaoPseudoPermIniCalcCIOld(final OrdCalcPseudoPermIniCalcCI calcPseudoPerm) {
    final OrdCalculPseudoPermanentPersistOld calcPermPersist = createPseudoPermPersistOld(calcPseudoPerm);
    calcPermPersist.IniCalcCI = new CrueDaoStructureOCAL.IniCalcCIPersist();
    return calcPermPersist;
  }

  private OrdCalculAbstractPersist metierToDaoPseudoPermIniCalcPrecedent(
          final OrdCalcPseudoPermIniCalcPrecedent calcPseudoPerm) {
    if (oldVersion) {
      return metierToDaoPseudoPermIniCalcPrecedentOld(calcPseudoPerm);
    }
    final OrdCalculPseudoPermanentPersist calcPermPersist = createPseudoPermPersist(calcPseudoPerm);
    calcPermPersist.IniCalcPrecedent = new IniCalculPrecedentPersist();
    return calcPermPersist;
  }

  private OrdCalculAbstractPersist metierToDaoPseudoPermIniCliche(final OrdCalcPseudoPermIniCliche calcPseudoPerm,
          final CtuluLog analyser) {
    if (oldVersion) {
      analyser.addSevereError("io.occal.clicheNoSupportedIn1.1.1", calcPseudoPerm.getCalc().getNom());
    }
    final OrdCalculPseudoPermanentPersist calcPermPersist = createPseudoPermPersist(calcPseudoPerm);
    if (!calcPseudoPerm.isInicCalClicheEmpty()) {
      calcPermPersist.IniCalcCliche = new CrueDaoStructureOCAL.IniCalcClichePersist();
      calcPermPersist.IniCalcCliche.NomFic = calcPseudoPerm.getIniCalcCliche().getNomFic();
    }
    return calcPermPersist;
  }

  private OrdCalculAbstractPersist metierToDaoPseudoPermIniCalcPrecedentOld(
          final OrdCalcPseudoPermIniCalcPrecedent calcPseudoPerm) {
    final OrdCalculPseudoPermanentPersistOld calcPermPersist = createPseudoPermPersistOld(calcPseudoPerm);
    calcPermPersist.IniCalcPrecedent = new IniCalculPrecedentPersist();
    return calcPermPersist;
  }

  private OrdCalculAbstractPersist metierToDaoTransIniCalcReprise(final OrdCalcTransIniCalcCI reprise, final CtuluLog log) {
    if (oldVersion) {
      log.addSevereError("io.occal.transCINoSupportedIn1.1.1", reprise.getCalc().getNom());
      return null;
    }
    final OrdCalculTransitoirePersist calcTransPersist = new OrdCalculTransitoirePersist();
    final CrueDaoStructureOCAL.IniCalcCIPersist iniReprisePersist = new CrueDaoStructureOCAL.IniCalcCIPersist();
    calcTransPersist.IniCalcCI = iniReprisePersist;
    metierToDaoTransCommonAttributes(calcTransPersist, reprise);
    return calcTransPersist;
  }

  private OrdCalculPseudoPermanentPersist createPseudoPermPersist(final OrdCalcPseudoPerm calcPseudoPerm) {
    final OrdCalculPseudoPermanentPersist calcPermPersist = new OrdCalculPseudoPermanentPersist();
    if (!calcPseudoPerm.isClicheEmpty()) {
      calcPermPersist.PrendreClicheFinPermanent = new CrueDaoStructureOCAL.PrendreClicheFinPermanentPersist();
      calcPermPersist.PrendreClicheFinPermanent.NomFic = calcPseudoPerm.getPrendreClicheFinPermanent().getNomFic();
    }
    return calcPermPersist;
  }

  private OrdCalculPseudoPermanentPersistOld createPseudoPermPersistOld(final OrdCalcPseudoPerm calcPseudoPerm) {
    final OrdCalculPseudoPermanentPersistOld calcPermPersist = new OrdCalculPseudoPermanentPersistOld();
    calcPermPersist.NomRef = calcPseudoPerm.getCalcPseudoPerm().getNom();

    return calcPermPersist;
  }

  @Override
  public OrdCalcScenario getConverterData(final CrueData in) {
    return in.getOCAL();
  }

  /**
   * @param ordCalculPersist
   * @param dataLinked
   * @param analyser
   */
  private OrdCalcPseudoPerm daoToMetierPseudoPermanent(final OrdCalculPseudoPermanentPersist ordCalculPersist,
          final CrueData dataLinked, final CtuluLog analyser) {

    Calc calc = null;
    if (dataLinked != null) {
      calc = dataLinked.findCalcByNom(ordCalculPersist.NomRef);
      if (!(calc instanceof CalcPseudoPerm)) {
        CrueHelper.unknowReference("OrdCalculPseudoPermanent", ordCalculPersist.NomRef, analyser);
        return null;
      }
    }
    final OrdCalcPseudoPerm metier;
    // Pas de références pour IniCondIni, IniCalculPrecedent et IniReprise
    if (ordCalculPersist.IniCalcCI != null) {
      metier = new OrdCalcPseudoPermIniCalcCI();
    } else if (ordCalculPersist.IniCalcPrecedent != null) {
      metier = new OrdCalcPseudoPermIniCalcPrecedent();
    } else {
      metier = new OrdCalcPseudoPermIniCliche();
      if (ordCalculPersist.IniCalcCliche != null) {
        ((OrdCalcPseudoPermIniCliche) metier).getIniCalcCliche().setNomFic(ordCalculPersist.IniCalcCliche.NomFic);
      }

    }
    if (ordCalculPersist.PrendreClicheFinPermanent != null) {
      metier.setPrendreClicheFinPermanent(new PrendreClicheFinPermanent(ordCalculPersist.PrendreClicheFinPermanent.NomFic));
    }
    metier.setCalcPseudoPerm(
            (CalcPseudoPerm) calc);
    if (dataLinked != null) {
      dataLinked.getOCAL().addOrdCalc(metier);
    }
    return metier;
  }

  /**
   * @param listeCalculsPermanents
   * @param ordCalculPersist
   * @param dataLinked
   * @param analyser
   */
  private void daoToMetierPseudoPermanentOld(final OrdCalculPseudoPermanentPersistOld ordCalculPersist,
          final CrueData dataLinked, final CtuluLog analyser) {

    final Calc calc = dataLinked.findCalcByNom(ordCalculPersist.NomRef);

    // Pas de références pour IniCondIni, IniCalculPrecedent et IniReprise
    if (ordCalculPersist.IniCalcCI != null) {
      final OrdCalcPseudoPerm metier = new OrdCalcPseudoPermIniCalcCI();
      if (!(calc instanceof CalcPseudoPerm)) {
        CrueHelper.unknowReference("OrdCalculPseudoPermanent", ordCalculPersist.NomRef, analyser);
        return;
      }
      metier.setCalcPseudoPerm((CalcPseudoPerm) calc);
      dataLinked.getOCAL().addOrdCalc(metier);
    } else if (ordCalculPersist.IniCalcPrecedent != null) {
      final OrdCalcPseudoPerm metier = new OrdCalcPseudoPermIniCalcPrecedent();
      if (!(calc instanceof CalcPseudoPerm)) {
        CrueHelper.unknowReference("OrdCalculPseudoPermanent", ordCalculPersist.NomRef, analyser);
        return;
      }
      metier.setCalcPseudoPerm((CalcPseudoPerm) calc);
      dataLinked.getOCAL().addOrdCalc(metier);
    } else if (ordCalculPersist.IniCalcReprise != null) {
      analyser.addSevereError("io.ocal.v111.calRepriseNotSupported.error", ordCalculPersist.NomRef);
    } else {
      analyser.addSevereError("io.ocal.no.calcul.error");
    }

  }

  /**
   * @param calculTransitoire
   * @param ordCalculPersist
   * @param dataLinked
   * @param analyser
   */
  private OrdCalcTrans daoToMetierTransitoire(final OrdCalculTransitoirePersist ordCalculPersist, final CrueConfigMetier ccm,
          final CrueData dataLinked,
          final CtuluLog analyser) {

    Calc trouve = null;
    if (dataLinked != null) {
      trouve = dataLinked.findCalcByNom(ordCalculPersist.NomRef);
      if (!(trouve instanceof CalcTrans)) {
        CrueHelper.unknowReference("OrdCalculTransitoire", ordCalculPersist.NomRef, analyser);
        return null;
      }
    }
    OrdCalcTrans added = null;
    if (ordCalculPersist.IniCalcCI != null) {
      final OrdCalcTransIniCalcCI initReprise = new OrdCalcTransIniCalcCI(ccm);
      added = initReprise;
    } else if (ordCalculPersist.IniCalcPrecedent != null) {
      final OrdCalcTransIniCalcPrecedent iniPrec = new OrdCalcTransIniCalcPrecedent(ccm);
      added = iniPrec;
    } else if (ordCalculPersist.IniCalcCliche != null) {
      final OrdCalcTransIniCalcCliche initCliche = new OrdCalcTransIniCalcCliche(ccm);
      initCliche.getIniCalcCliche().setNomFic(ordCalculPersist.IniCalcCliche.NomFic);
      added = initCliche;
    } else {
      analyser.addSevereError("io.ocal.trans.inconnu");
    }
    if (added != null) {
      added.setCalcTrans((CalcTrans) trouve);
      added.setDureeCalc(DateDurationConverter.getDuration(ordCalculPersist.DureeCalc));
      if (ordCalculPersist.PdtRes != null && StringUtils.isNotBlank(ordCalculPersist.PdtRes.PdtCst)) {
        added.setPdtRes(EMHFactory.createPdtCst(DateDurationConverter.getDuration(ordCalculPersist.PdtRes.PdtCst)));
      }
      if (ordCalculPersist.PrendreClichePeriodique != null && ordCalculPersist.PrendreClichePeriodique.PdtRes != null) {
        added.setPrendreClichePeriodique(new PrendreClichePeriodique(ordCalculPersist.PrendreClichePeriodique.NomFic,
                EMHFactory.createPdtCst(DateDurationConverter.getDuration(
                        ordCalculPersist.PrendreClichePeriodique.PdtRes.PdtCst))));
      }
      if (ordCalculPersist.PrendreClichePonctuel != null && ordCalculPersist.PrendreClichePonctuel.TempsSimu != null) {
        added.setPrendreClichePonctuel(new PrendreClichePonctuel(ordCalculPersist.PrendreClichePonctuel.NomFic,
                DateDurationConverter.getDuration(
                        ordCalculPersist.PrendreClichePonctuel.TempsSimu)));
      }
      if (dataLinked != null) {
        dataLinked.getOCAL().addOrdCalc(added);
      }
    }
    return added;
  }

  private void daoToMetierTransitoireOld(final OrdCalculTransitoirePersistOld ordCalculPersist,
          final CrueData dataLinked, final CtuluLog analyser) {

    final Calc trouve = dataLinked.findCalcByNom(ordCalculPersist.NomRef);
    if (!(trouve instanceof CalcTrans)) {
      CrueHelper.unknowReference("OrdCalculTransitoire", ordCalculPersist.NomRef, analyser);
      return;
    }
    if (ordCalculPersist.IniCalcPrecedent == null) {
      analyser.addSevereError("io.ocal.trans.inconnu");
    } else if (ordCalculPersist.IniCalcPrecedent != null) {
      final OrdCalcTransIniCalcPrecedent iniPrec = new OrdCalcTransIniCalcPrecedent(dataLinked.getCrueConfigMetier());
      iniPrec.setCalcTrans((CalcTrans) trouve);
      iniPrec.setDureeCalc(dataLinked.getOldPCALDureeSce());
      iniPrec.setPdtRes(dataLinked.getOldPCALPdtRes());
      dataLinked.getOCAL().addOrdCalc(iniPrec);
    } else {
      analyser.addSevereError("io.ocal.trans.inconnu");
    }
  }

  /**
   * @param listeCalculsPermanents
   * @param ordCalculPersist
   * @param dataLinked
   * @param analyser
   */
  private OrdCalcVraiPerm daoToMetierVraiPermanent(final OrdCalculVraiPermanentPersist ordCalculPersist,
          final CrueData dataLinked, final CtuluLog analyser) {

    final OrdCalcVraiPerm metier = new OrdCalcVraiPerm();
    if (dataLinked != null) {
      final Calc calc = dataLinked.findCalcByNom(ordCalculPersist.NomRef);
      if (calc == null) {
        CrueHelper.unknowReference("OrdCalcVraiPerm", ordCalculPersist.NomRef, analyser);
        return null;
      }
      metier.setCalc(calc);
      dataLinked.getOCAL().addOrdCalc(metier);
    }

    return metier;

  }
}
