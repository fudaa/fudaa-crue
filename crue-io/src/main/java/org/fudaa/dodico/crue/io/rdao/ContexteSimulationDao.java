/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

/**
 *
 * @author deniger
 */
public class ContexteSimulationDao {

 


  public static class ScenarioDao extends CommonResDao.NomRefDao {
  }

  public static class ModeleDao extends CommonResDao.NomRefDao {
  }

  public static class SousModeleDao extends CommonResDao.NomRefDao {
  }

  public static class RunDao extends CommonResDao.NomRefDao {
  }

  public ContexteSimulationDao() {
  }
  String DateSimulation;
  String VersionCrue;
  String Etude;
  ContexteSimulationDao.ScenarioDao Scenario;
  ContexteSimulationDao.RunDao Run;
  ContexteSimulationDao.ModeleDao Modele;

  public String getDateSimulation() {
    return DateSimulation;
  }

  public String getEtude() {
    return Etude;
  }

  public ModeleDao getModele() {
    return Modele;
  }

  public RunDao getRun() {
    return Run;
  }

  public ScenarioDao getScenario() {
    return Scenario;
  }

  public String getVersionCrue() {
    return VersionCrue;
  }
  
  
}
