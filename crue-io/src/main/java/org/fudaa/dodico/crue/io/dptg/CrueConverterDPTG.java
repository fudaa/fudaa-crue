/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dptg;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.dptg.CrueDaoStructureDPTG.Fente;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByIdComparator;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoBatiCasier;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilCasier;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilEtiquette;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSectionFenteData;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoSectionIdem;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EvolutionFF;
import org.fudaa.dodico.crue.metier.emh.LitNumerote;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;
import org.fudaa.dodico.crue.metier.emh.PtProfil;
import org.fudaa.dodico.crue.metier.factory.EMHFactory;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

/**
 * Factory qui se charge de remplir les structures DAO du fichier DPTG avec les données métier et inversement.
 *
 * @author Adrien Hadoux
 */
public class CrueConverterDPTG implements CrueDataConverter<CrueDaoDPTG, CrueData> {

  @Override
  public CrueData convertDaoToMetier(final CrueDaoDPTG dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
    if (dataLinked != null) {
      if (dao == null) {
        ctuluLog.addInfo("io.dptg.lecture.error");
      } else {
        // -- cas des SectionsProfils --//
        convertDaoToMetierSectionsProfils(dataLinked, dao.DonPrtGeoProfilSections, dataLinked.getAllSimpleEMH(),
                dataLinked.getFrottements().getListFrt(), ctuluLog);

        // -- cas des Volumes --//
        convertDaoToMetierCasierProfils(dataLinked, this.getCasiers(dao.DonPrtGeoProfilCasiers, dao.DonPrtGeoCasiers),
                dataLinked.getAllSimpleEMH(), ctuluLog);

        // -- cas des Sections --//
        convertDaoToMetierSections(dao.DonPrtGeoSections, dataLinked, ctuluLog);

        // -- cas des branches --//
        convertDaoToMetierBranches(dao.DonPrtGeoBranches, dataLinked, ctuluLog);

        //on vérifie que tous les profils sont attachés à une section.
        Set<String> definedButNotUsed = dataLinked.getDefinedButNotUsedProfilSection();
        if (CollectionUtils.isNotEmpty(definedButNotUsed)) {
          final ArrayList<String> sortedList = CollectionCrueUtil.getSortedList(
                  definedButNotUsed);
          ctuluLog.addSevereError("io.profilSectionNotLinkedToSection.error", StringUtils.join(sortedList, "; "));
        }
        //on vérifie que tous les profils casiers sont attachés à une section.
        definedButNotUsed = dataLinked.getDefinedButNotUsedProfilCasier();
        if (CollectionUtils.isNotEmpty(definedButNotUsed)) {
          final ArrayList<String> sortedList = CollectionCrueUtil.getSortedList(
                  definedButNotUsed);
          ctuluLog.addSevereError("io.profilCasierNotLinkedToSection.error", StringUtils.join(sortedList, "; "));
        }
        definedButNotUsed = dataLinked.getDefinedButNotUsedBatiCasier();
        if (CollectionUtils.isNotEmpty(definedButNotUsed)) {
          final ArrayList<String> sortedList = CollectionCrueUtil.getSortedList(
                  definedButNotUsed);
          ctuluLog.addSevereError("io.batiCasierNotLinkedToSection.error", StringUtils.join(sortedList, "; "));
        }

      }
      return dataLinked;

    }
    return null;
  }

  private List<CrueDaoStructureDPTG.AbstractCasier> getCasiers(
          List<CrueDaoStructureDPTG.ProfilCasier> profilCasiers, List<CrueDaoStructureDPTG.BatiCasier> batiCasiers) {
    List<CrueDaoStructureDPTG.AbstractCasier> listCasiers = new ArrayList<>();
    if (profilCasiers != null) {
      listCasiers.addAll(profilCasiers);
    }
    if (batiCasiers != null) {
      listCasiers.addAll(batiCasiers);
    }

    return listCasiers;
  }

  @Override
  public CrueData getConverterData(final CrueData in) {
    return in;
  }

  @Override
  public CrueDaoDPTG convertMetierToDao(final CrueData metier, final CtuluLog ctuluLog) {
    final CrueDaoDPTG res = new CrueDaoDPTG();
    // -- liste qui assure d'ajouter une seule référence de profil: car un meme profil peut etre referencé plusieurs
    // fois
    final Set<String> doneCasierId = new HashSet<>();
    final Set<String> doneCasierBatiId = new HashSet<>();
    final Set<String> doneSectionId = new HashSet<>();

    res.DonPrtGeoProfilCasiers = new ArrayList<>();
    res.DonPrtGeoProfilSections = new ArrayList<>();
    res.DonPrtGeoCasiers = new ArrayList<>();
    res.DonPrtGeoBranches = new ArrayList<>();
    res.DonPrtGeoSections = new ArrayList<>();

    Set<DonPrtGeoProfilSection> profilsSection = DonPrtHelper.getAllProfilSection(metier.getSousModele());
    for (DonPrtGeoProfilSection profil : profilsSection) {
      convertMetierToDaoSectionsProfils(profil, res, ctuluLog, doneSectionId);
    }
    Collection<DonPrtGeoProfilCasier> profilsCasier = DonPrtHelper.getAllProfilCasier(metier.getSousModele());
    for (DonPrtGeoProfilCasier profilCasier : profilsCasier) {
      convertMetierToDaoCasiersProfils(profilCasier, res.DonPrtGeoProfilCasiers, ctuluLog,
              doneCasierId);
    }
    Collection<DonPrtGeoBatiCasier> batisCasier = DonPrtHelper.getAllBatiCasier(metier.getSousModele());
    for (DonPrtGeoBatiCasier bati : batisCasier) {
      convertMetierToDaoCasierBati(res, doneCasierBatiId, bati);
    }
    Collection<DonPrtGeoSectionIdem> allPrtGeoSectionIdem = DonPrtHelper.getAllPrtGeoSectionIdem(metier.getSousModele());
    for (DonPrtGeoSectionIdem sectionIdem : allPrtGeoSectionIdem) {
      convertMetierToDaoSectionsIdem(res.DonPrtGeoSections, sectionIdem, sectionIdem.getEmh(), ctuluLog);
    }
    Collection<DonPrtGeoBrancheSaintVenant> dptgSaintVenant = DonPrtHelper.getAllPrtGeoBrancheSaintVenant(metier.getSousModele());
    for (DonPrtGeoBrancheSaintVenant dptg : dptgSaintVenant) {
      convertMetierToDaoBranches(res.DonPrtGeoBranches, dptg.getEmh(), dptg, ctuluLog);
    }

    Collections.sort(res.DonPrtGeoBranches, ObjetNommeByIdComparator.INSTANCE);
    Collections.sort(res.DonPrtGeoCasiers, ObjetNommeByIdComparator.INSTANCE);
    Collections.sort(res.DonPrtGeoProfilSections, ObjetNommeByIdComparator.INSTANCE);
    Collections.sort(res.DonPrtGeoSections, ObjetNommeByIdComparator.INSTANCE);
    return res;
  }

  private void convertMetierToDaoCasierBati(final CrueDaoDPTG res, final Set<String> doneCasierBatiId,
          final DonPrtGeoBatiCasier bati) {
    if (!doneCasierBatiId.contains(bati.getId())) {
      final CrueDaoStructureDPTG.BatiCasier batiCasier = new CrueDaoStructureDPTG.BatiCasier();
      batiCasier.Nom = bati.getNom();
      batiCasier.Commentaire = bati.getCommentaire();
      batiCasier.SplanBati = bati.getSplanBati();
      batiCasier.ZBatiTotal = bati.getZBatiTotal();
      res.DonPrtGeoCasiers.add(batiCasier);
      doneCasierBatiId.add(bati.getId());
    }
  }

  /**
   * @param in l'objet metier a transformer en dao.
   * @param out l'endroit ou sont stockés les DAO
   * @param analyser le receveur de log
   * @param uniqueRef la liste permettant d'eviter de restocker 2 fois le meme objets
   */
  private void convertMetierToDaoSectionsProfils(final DonPrtGeoProfilSection in, final CrueDaoDPTG out,
          final CtuluLog analyser, final Set<String> uniqueRef) {
    if (!uniqueRef.contains(in.getId())) {
      convertMetierToDaoSectionsProfils(in, out.DonPrtGeoProfilSections, analyser);
      // -- on enregistre la reference --//
      uniqueRef.add(in.getId());

    }
  }

  /**
   * Remplit la premiere partie du fichier DPTG avec le type d'objet: sections profils.
   *
   * @param in
   * @param out
   * @param analyser
   */
  private static void convertMetierToDaoSectionsProfils(final DonPrtGeoProfilSection in,
          final List<CrueDaoStructureDPTG.ProfilSection> out,
          final CtuluLog analyser) {

    final CrueDaoStructureDPTG.ProfilSection profilPersistant = new CrueDaoStructureDPTG.ProfilSection();

    profilPersistant.Nom = in.getNom();
    profilPersistant.Commentaire = in.getCommentaire();
    if (in.getFente() != null) {
      profilPersistant.Fente = new Fente();
      profilPersistant.Fente.LargeurFente = in.getFente().getLargeurFente();
      profilPersistant.Fente.ProfondeurFente = in.getFente().getProfondeurFente();
    }

    // -- Ecriture des lits numerotes --//
    if (in.getLitNumerote() != null && in.getLitNumerote().size() > 0) {
      profilPersistant.LitNumerotes = new ArrayList<>();
      for (final LitNumerote litMetier : in.getLitNumerote()) {
        final CrueDaoStructureDPTG.Lit litPersist = new CrueDaoStructureDPTG.Lit();
        litPersist.IsLitActif = litMetier.getIsLitActif();
        litPersist.IsLitMineur = litMetier.getIsLitMineur();
        litPersist.LimDeb = litMetier.getLimDeb();
        litPersist.LimFin = litMetier.getLimFin();

        if (litMetier.getNomLit() != null) {
          litPersist.LitNomme = litMetier.getNomLit().getNom();
        }

        if (litMetier.getFrot() != null) {
          litPersist.Frot = new CrueDaoStructureDPTG.Frottement();
          // -- on ajoute le nom avec un suffixe --//
          litPersist.Frot.NomRef = litMetier.getFrot().getNom();
        } else {
          analyser.addInfo("io.dptg.frotForLitNotFound.error", in.getNom());
        }

        // -- ajout du lit persistant --//
        profilPersistant.LitNumerotes.add(litPersist);
      }

    } else {
      analyser.addInfo("io.dptg.convert.noLit.error", in.getNom());
    }

    // -- ecriture des series pt --//
    if (CollectionUtils.isNotEmpty(in.getPtProfil())) {
      profilPersistant.EvolutionFF = new EvolutionFF();
      final List<PtEvolutionFF> pts = new ArrayList<>(in.getPtProfil().size());
      profilPersistant.EvolutionFF.setPtEvolutionFF(pts);
      for (final PtProfil serie : in.getPtProfil()) {
        pts.add(new PtEvolutionFF(serie.getXt(), serie.getZ()));
      }
    } else {
      analyser.addInfo("io.dptg.convert.noProfil.error", in.getNom());
    }
    if (CollectionUtils.isNotEmpty(in.getEtiquettes())) {
      profilPersistant.Etiquettes = new ArrayList<>();
      for (DonPrtGeoProfilEtiquette donPrtGeoProfilEtiquette : in.getEtiquettes()) {
        CrueDaoStructureDPTG.Etiquette daoEtiquette = new CrueDaoStructureDPTG.Etiquette();
        daoEtiquette.Nom = donPrtGeoProfilEtiquette.getTypeEtiquette().getName();
        daoEtiquette.PointFF = new PtProfil(donPrtGeoProfilEtiquette.getPoint());
        profilPersistant.Etiquettes.add(daoEtiquette);
      }
    }
    // -- ajout du profil persistant --//
    out.add(profilPersistant);
  }

  private static void convertMetierToDaoCasiersProfils(final DonPrtGeoProfilCasier in,
          final List<CrueDaoStructureDPTG.ProfilCasier> out, final CtuluLog analyser,
          Set<String> idDone) {
    if (!idDone.contains(in.getId())) {
      idDone.add(in.getId());
      convertMetierToDaoCasiersProfils(in, out, analyser);
    }
  }

  /**
   * Remplit la deuxieme partie du fichier DPTG avec le type d'objet: casiers profils
   *
   * @param in
   * @param out
   * @param analyser
   */
  private static void convertMetierToDaoCasiersProfils(final DonPrtGeoProfilCasier in,
          final List<CrueDaoStructureDPTG.ProfilCasier> out, final CtuluLog analyser) {

    final CrueDaoStructureDPTG.ProfilCasier profilPersistant = new CrueDaoStructureDPTG.ProfilCasier();
    // -- donneee specifique pour le profil casier --//
    profilPersistant.Longueur = in.getDistance();

    profilPersistant.Nom = in.getNom();
    profilPersistant.Commentaire = in.getCommentaire();

    // -- Ecriture du lit utile dans le profil casier --//
    if (in.getLitUtile() != null) {
      profilPersistant.LitUtile = new CrueDaoStructureDPTG.LitUtile();

      final org.fudaa.dodico.crue.metier.emh.LitUtile litMetier = in.getLitUtile();

      profilPersistant.LitUtile.LimDeb = litMetier.getLimDeb();
      profilPersistant.LitUtile.LimFin = litMetier.getLimFin();

    } else {
      analyser.addInfo("io.dptg.convert.noLit.error", in.getNom());
    }

    // -- ecriture des series pt --//
    if (CollectionUtils.isNotEmpty(in.getPtProfil())) {
      profilPersistant.EvolutionFF = new EvolutionFF();
      final List<PtEvolutionFF> pts = new ArrayList<>(in.getPtProfil().size());
      profilPersistant.EvolutionFF.setPtEvolutionFF(pts);
      for (final PtProfil serie : in.getPtProfil()) {
        pts.add(new PtEvolutionFF(serie.getXt(), serie.getZ()));
      }
    } else {
      analyser.addInfo("io.dptg.convert.noProfil.error", in.getNom());
    }

    // -- ajout du profil persistant --//
    out.add(profilPersistant);
  }

  private static void convertMetierToDaoSectionsIdem(
          final List<CrueDaoStructureDPTG.SectionIdem> listePersistante, final DonPrtGeoSectionIdem prtgeo,
          final EMH emh, final CtuluLog analyser) {

    final CrueDaoStructureDPTG.SectionIdem sectionPersist = new CrueDaoStructureDPTG.SectionIdem();
    sectionPersist.NomRef = emh.getNom();
    sectionPersist.Dz = prtgeo.getDz();
    listePersistante.add(sectionPersist);
  }

  private static void convertMetierToDaoBranches(
          final List<CrueDaoStructureDPTG.BrancheSaintVenant> listePersistante, final EMH emh,
          final DonPrtGeoBrancheSaintVenant prtgeo, final CtuluLog analyser) {
    final CrueDaoStructureDPTG.BrancheSaintVenant branchePersist = new CrueDaoStructureDPTG.BrancheSaintVenant();
    branchePersist.NomRef = emh.getNom();
    branchePersist.CoefSinuo = prtgeo.getCoefSinuo();

    listePersistante.add(branchePersist);

  }

  private static void convertDaoToMetierSectionsProfils(final CrueData crueData,
          final List<CrueDaoStructureDPTG.ProfilSection> listePersistante,
          final List<EMH> data,
          final List<DonFrt> frottements, final CtuluLog analyser) {
    Map<String, ItemEnum> etiquetteByNomMap = createEtiquetteByNomMap(crueData.getCrueConfigMetier());
    if (CollectionUtils.isNotEmpty(listePersistante)) {
      for (final CrueDaoStructureDPTG.ProfilSection profilPersist : listePersistante) {

        // -- on recupere la branche qu'il faut --//
        final String profil = profilPersist.Nom;
        // -- on tente de trouver tous les objets referencé par ce profil --//
        DonPrtGeoProfilSection profilGeo = crueData.getDefinedProfilSection(profil);

        if (profilGeo == null) {
          profilGeo = new DonPrtGeoProfilSection();
          profilGeo.setNom(profil);
          crueData.registerDefinedSectionProfil(profilGeo);
        }

        // -- on remplit son contenu --//
        profilGeo.setNom(profilPersist.Nom);
        profilGeo.setCommentaire(profilPersist.Commentaire);
        if (profilPersist.Fente != null) {
          final DonPrtGeoProfilSectionFenteData dataFente = new DonPrtGeoProfilSectionFenteData(crueData.getCrueConfigMetier());
          if (profilPersist.Fente.LargeurFente == null) {
            analyser.addWarn("dptg.largeurFenteNotDefined");
          } else {
            dataFente.setLargeurFente(profilPersist.Fente.LargeurFente);
          }
          if (profilPersist.Fente.ProfondeurFente == null) {
            analyser.addWarn("dptg.profondeurFenteNotDefined");
          } else {
            dataFente.setProfondeurFente(profilPersist.Fente.ProfondeurFente);
          }
          profilGeo.setFente(dataFente);
        }

        // -- on remplit les litsNommes --//
        if (profilPersist.LitNumerotes != null) {
          final List<LitNumerote> listeLits = new ArrayList<>();

          for (final CrueDaoStructureDPTG.Lit litPersist : profilPersist.LitNumerotes) {

            final LitNumerote litNum = new LitNumerote();
            litNum.setLimDeb(litPersist.LimDeb);
            litNum.setLimFin(litPersist.LimFin);
            litNum.setIsLitActif(litPersist.IsLitActif);
            litNum.setIsLitMineur(litPersist.IsLitMineur);
            if (litPersist.LitNomme != null) {
              litNum.setNomLit(new LitNomme(litPersist.LitNomme, 0));
            }
            // -- recherche de référence au bon DonFrt --//
            if (litPersist.Frot != null) {
              final String idFrottement = litPersist.Frot.NomRef;
              final DonFrt frottement = EMHHelper.selectObjectNomme(frottements, idFrottement);

              if (frottement != null) {
                litNum.setFrot(frottement);
              } else {
                analyser.addSevereError("io.dptg.profil.ref.error", profil, idFrottement);
              }

            }

            // -- ajout du litnum dans metier --//
            listeLits.add(litNum);
          }

          // -- ajout de la liste de litNum dans donprtgeo --//
          profilGeo.setLitNumerote(listeLits);

        } else {
          analyser.addInfo("io.dptg.convert.noLit.error", profilPersist.Nom);
        }

        // -- on remplit les séries --//
        if (profilPersist.EvolutionFF != null) {
          final List<PtProfil> listeProfils = new ArrayList<>();
          if (profilPersist.EvolutionFF.getPtEvolutionFF() != null) {
            for (final PtEvolutionFF pt : profilPersist.EvolutionFF.getPtEvolutionFF()) {
              listeProfils.add(new PtProfil(pt.getAbscisse(), pt.getOrdonnee()));
            }
          }
          // -- ajout de la liste de profils dans le donprtgeo --//
          profilGeo.setPtProfil(listeProfils);

        } else {
          analyser.addInfo("io.dptg.convert.noProfil.error", profilPersist.Nom);
        }
        if (profilPersist.Etiquettes != null) {
          List<DonPrtGeoProfilEtiquette> etiquettes = new ArrayList<>();
          for (CrueDaoStructureDPTG.Etiquette daoEtiquette : profilPersist.Etiquettes) {
            ItemEnum type = etiquetteByNomMap.get(daoEtiquette.Nom);
            if (type == null) {
              analyser.addSevereError("io.dptg.convert.etiquette.unknown", profilPersist.Nom, daoEtiquette.Nom);
            } else {
              DonPrtGeoProfilEtiquette detiquette = new DonPrtGeoProfilEtiquette();
              detiquette.setTypeEtiquette(type);
              detiquette.setPoint(new PtProfil(daoEtiquette.PointFF));
              etiquettes.add(detiquette);
            }
          }
          profilGeo.setEtiquettes(etiquettes);
        }
      }
    }

  }

  private static Map<String, ItemEnum> createEtiquetteByNomMap(CrueConfigMetier ccm) {
    Map<String, ItemEnum> etiquetteByNom = new HashMap<>();
    List<ItemEnum> etiquettesEnums = ccm.getLitNomme().getEtiquettesEnums();
    for (ItemEnum et : etiquettesEnums) {
      etiquetteByNom.put(et.getName(), et);
    }
    return etiquetteByNom;
  }

  private static void convertDaoToMetierCasierProfils(final CrueData crueData,
          final List<CrueDaoStructureDPTG.AbstractCasier> listePersistante,
          final List<EMH> data,
          final CtuluLog analyser) {

    if (CollectionUtils.isNotEmpty(listePersistante)) {
      for (final CrueDaoStructureDPTG.AbstractCasier casier : listePersistante) {
        if (casier instanceof CrueDaoStructureDPTG.ProfilCasier) {
          final CrueDaoStructureDPTG.ProfilCasier profilPersist = (CrueDaoStructureDPTG.ProfilCasier) casier;
          // -- on recupere la branche qu'il faut --//
          final String nomProfil = profilPersist.Nom;
          // -- on tente de trouver tous les objets referencé par ce profil --//
          DonPrtGeoProfilCasier profilCasier = crueData.getDefinedProfilCasier(nomProfil);
          if (profilCasier == null) {
            profilCasier = new DonPrtGeoProfilCasier(crueData.getCrueConfigMetier());
            profilCasier.setNom(nomProfil);
            profilCasier.setCommentaire(profilPersist.Commentaire);
            crueData.registerDefinedCasierProfil(profilCasier);
          }

          // -- on récupére le casier ou le profil et on le stocke dans le emh --//
          // -- donnee particulieres a ProfilCasier --//
          profilCasier.setDistance(profilPersist.Longueur);

          // -- on remplit son contenu --//
          profilCasier.setNom(profilPersist.Nom);

          // -- on remplit les litsNommes --//
          if (profilPersist.LitUtile != null) {
            final CrueDaoStructureDPTG.LitUtile litPersist = profilPersist.LitUtile;

            final org.fudaa.dodico.crue.metier.emh.LitUtile litUtileMetier = new org.fudaa.dodico.crue.metier.emh.LitUtile();
            litUtileMetier.setLimDeb(litPersist.LimDeb);
            litUtileMetier.setLimFin(litPersist.LimFin);

            // -- ajout de la liste de litNum dans donprtgeo --//
            profilCasier.setLitUtile(litUtileMetier);

          } else {
            analyser.addInfo("io.dptg.profil.no.lit.num.error", profilPersist.Nom);
          }

          // -- on remplit les séries --//
          if (profilPersist.EvolutionFF != null && profilPersist.EvolutionFF.getPtEvolutionFF() != null) {
            final List<PtProfil> listeProfils = new ArrayList<>();

            for (final PtEvolutionFF buff : profilPersist.EvolutionFF.getPtEvolutionFF()) {
              listeProfils.add(new PtProfil(buff.getAbscisse(), buff.getOrdonnee()));
            }

            // -- ajout de la liste de profils dans le donprtgeo --//
            profilCasier.setPtProfil(listeProfils);

          } else {
            analyser.addInfo("io.dptg.convert.noProfil.error", nomProfil);
          }

        } else if (casier instanceof CrueDaoStructureDPTG.BatiCasier) {
          final CrueDaoStructureDPTG.BatiCasier bati = (CrueDaoStructureDPTG.BatiCasier) casier;
          // -- on recupere la branche qu'il faut --//
          final String nomProfil = bati.Nom;
          // -- on tente de trouver tous les objets referencé par ce profil --//
          DonPrtGeoBatiCasier profilGeo = crueData.getDefinedBatiCasier(nomProfil);
          if (profilGeo == null) {
            profilGeo = EMHFactory.createDonPrtGeoBatiCasier(nomProfil, crueData.getCrueConfigMetier());
            crueData.registerDefinedBatiCasier(profilGeo);
          }
          profilGeo.setCommentaire(bati.Commentaire);
          profilGeo.setSplanBati(bati.SplanBati);
          profilGeo.setZBatiTotal(bati.ZBatiTotal);

        }

      }
    }
  }

  private static void convertDaoToMetierSections(final List<CrueDaoStructureDPTG.SectionIdem> listePersistante,
          final CrueData dataLinked, final CtuluLog analyser) {

    if (CollectionUtils.isNotEmpty(listePersistante)) {
      for (final CrueDaoStructureDPTG.SectionIdem sectionPersist : listePersistante) {

        // -- on recupere la branche qu'il faut --//
        final String reference = sectionPersist.NomRef;
        final CatEMHSection section = dataLinked.findSectionByReference(reference);
        if (section == null) {
          analyser.addInfo("io.dptg.section.ref.error", reference);
        } else {
          final DonPrtGeoSectionIdem prtgeo = new DonPrtGeoSectionIdem(dataLinked.getCrueConfigMetier());
          prtgeo.setDz(sectionPersist.Dz);
          section.addInfosEMH(prtgeo);
        }

      }
    }
  }

  private static void convertDaoToMetierBranches(
          final List<CrueDaoStructureDPTG.BrancheSaintVenant> listePersistante, final CrueData data,
          final CtuluLog analyser) {
    if (CollectionUtils.isNotEmpty(listePersistante)) {
      for (final CrueDaoStructureDPTG.BrancheSaintVenant branchePersist : listePersistante) {

        // -- on recupere la branche qu'il faut --//
        final String reference = branchePersist.NomRef;
        final CatEMHBranche branche = data.findBrancheByReference(reference);
        if (branche == null) {
          analyser.addInfo("io.dptg.branche.ref.error", reference);
        } else {
          final DonPrtGeoBrancheSaintVenant prtgeo = new DonPrtGeoBrancheSaintVenant(data.getCrueConfigMetier());
          prtgeo.setCoefSinuo(branchePersist.CoefSinuo);

          branche.addInfosEMH(prtgeo);
        }

      }
    }
  }
}
