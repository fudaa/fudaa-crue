/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.etu;

import org.fudaa.dodico.crue.config.coeur.CrueVersionType;

public interface CrueEtuInfosContainer {
  
  String getAuteurCreation();
  String getDateCreation();
  String getAuteurDerniereModif();
  String getDateDerniereModif();
  String getCommentaire();
  String getType();
  void setAuteurCreation(String auteur);
  void setDateCreation(String date);
  void setAuteurDerniereModif(String auteur);
  void setDateDerniereModif(String date);
  void setCommentaire(String commentaire);
  void setCrueVersion(CrueVersionType version);

}
