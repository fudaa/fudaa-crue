/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import java.util.Map;
import org.fudaa.dodico.crue.io.neuf.DCFileReader;
import org.fudaa.dodico.crue.io.neuf.DCFileWriter;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * Gère la lecture du format crue 9 Utilise fortranReader.
 * 
 * @author Adrien Hadoux
 */
public class Crue9DCFileFormat extends AbstractCrue9FileFormat {

  /**
   * déclare seulement l'extension
   */
  public Crue9DCFileFormat(final Map<CrueFileType, AbstractCrue9FileFormat> map) {
    super(CrueFileType.DC, map);
  }

  @Override
  protected DCFileReader createReader() {
    return new DCFileReader();
  }

  @Override
  protected DCFileWriter createWriter() {
    return new DCFileWriter();
  }
}
