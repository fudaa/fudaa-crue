/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.conf;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Frederic Deniger
 */
public class CrueEtudeExternRessourceInfos {
  String Nom;
  String LayerId;
  String Id;
  String Type;
  List<Option> Options = new ArrayList<>();

  public String getNom() {
    return Nom;
  }

  public String getLayerId() {
    return LayerId;
  }

  public void setLayerId(String LayerId) {
    this.LayerId = LayerId;
  }
  
  

  public String getId() {
    return Id;
  }

  public void setId(String Id) {
    this.Id = Id;
  }

  public void setNom(String Nom) {
    this.Nom = Nom;
  }

  public void setType(String Type) {
    this.Type = Type;
  }

  public String getType() {
    return Type;
  }

  public List<Option> getOptions() {
    return Options;
  }

  public void setOptions(List<Option> options) {
    this.Options = options;
  }
  
}
