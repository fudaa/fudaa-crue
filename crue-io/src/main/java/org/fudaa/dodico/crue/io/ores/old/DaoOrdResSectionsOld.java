/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores.old;

/**
 *
 * @author Frederic Deniger
 */
public class DaoOrdResSectionsOld {

  public DaoOrdResSectionIdem OrdResSectionIdem;
  public DaoOrdResSectionInterpolee OrdResSectionInterpolee;
  public DaoOrdResSectionProfilOld OrdResSectionProfil;
  public DaoOrdResSectionSansGeometrieOld OrdResSectionSansGeometrie;
}
