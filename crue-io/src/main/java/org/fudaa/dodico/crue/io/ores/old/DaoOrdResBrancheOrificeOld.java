/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores.old;

public class DaoOrdResBrancheOrificeOld {
  // WARN: l'ordre des champs est important car utilise par l'ecriture/lecture de ORES

  private boolean ddeCoefCtr;
  private boolean ddeRegimeOrifice;

  public boolean getDdeCoefCtr() {
    return ddeCoefCtr;
  }

  public boolean getDdeRegimeOrifice() {
    return ddeRegimeOrifice;
  }

  /**
   * @param ddeCoefCtr the ddeCoefCtr to set
   */
  public void setDdeCoefCtr(final boolean ddeCoefCtr) {
    this.ddeCoefCtr = ddeCoefCtr;
  }

  /**
   * @param ddeRegimeOrifice the ddeRegimeOrifice to set
   */
  public void setDdeRegimeOrifice(final boolean ddeRegimeOrifice) {
    this.ddeRegimeOrifice = ddeRegimeOrifice;
  }

  @Override
  public String toString() {
    return "OrdResBrancheOrifice [ddeCoefCtr=" + ddeCoefCtr + ", ddeRegimeOrifice=" + ddeRegimeOrifice + "]";
  }
}
