/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import com.thoughtworks.xstream.XStream;

/**
 *
 * @author deniger
 */
public class NoeudsDaoXstream {

  public static void configureXstream(XStream xstream) {
    xstream.alias("Noeuds", NoeudsDao.class);
    xstream.alias("NoeudNiveauContinu", NoeudsDao.NoeudTypeDao.class);
    xstream.alias("Noeud", NoeudsDao.NoeudDao.class);
    xstream.addImplicitCollection(NoeudsDao.NoeudTypeDao.class, "Noeud", NoeudsDao.NoeudDao.class);

    xstream.addImplicitCollection(NoeudsDao.NoeudTypeDao.class, "VariableRes", CommonResDao.VariableResDao.class);
    xstream.addImplicitCollection(NoeudsDao.class, "VariableRes", CommonResDao.VariableResDao.class);

  }
}
