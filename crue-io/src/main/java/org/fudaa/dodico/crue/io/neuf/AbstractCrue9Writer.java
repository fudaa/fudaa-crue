/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;
import org.fudaa.dodico.fortran.FortranWriter;

/**
 * writer qui permet d'écrire les structures de crue 09 dans les fichiers appropriés. Utilise fortran writer
 *
 * @author Adrien Hadoux
 */
public abstract class AbstractCrue9Writer extends FileCharSimpleWriterAbstract<CrueIOResu<CrueData>> {

  /**
   * Toutes les lignes qui commencent par * sont des dlignes caracteres
   */
  protected final static String CARACT_COMMENT = "*";
  /**
   * Nb de lignes qui composent le titre en début de fichier. Valable pour dc et dh.
   */
  protected final static int NB_LINES_TITLE = 5;
  /**
   * *******************************************************************************************************************
   * tailel totale en caracteres d'une ligne.
   */
  protected final int tailleMaxLine = 80;
  /**
   * UNe reference sur le fortran writer.
   */
  FortranWriter fortranWriter;
  private boolean warnDoneForTitle;

  /**
   * Initialise le writer avec les params par defaut.
   */
  protected void initWriter() {
    // -- on enleve les quotes sur les strings --//
    fortranWriter.setStringQuoted(false);

  }

  /**
   * Ecrit un commentaire avec le message passé en parametre. Commence par le caractere de commentaire.
   *
   * @param message
   * @throws IOException
   */
  public void writeCom(final String message) throws IOException {
    fortranWriter.stringField(0, CARACT_COMMENT);
    fortranWriter.stringField(1, message);
    fortranWriter.writeFields();
  }

  /**
   * Ecrit l'Header du fichier. Commun a DC et DH.
   *
   * @param data
   * @throws IOException
   */
  public void writeHeader(final CrueIOResu<CrueData> data, final boolean writeTitre) throws IOException {
    writeCom("");
    final String commentaire = StringUtils.defaultString(data.getCrueCommentaire());
    final String[] titles = StringUtils.splitPreserveAllTokens(commentaire, '\n');
    warnDoneForTitle = false;
    for (int i = 0; i < Math.min(NB_LINES_TITLE, titles.length); i++) {
      final String trim = titles[i].trim();
      if (writeTitre) {

        if (trim.length() > 0) {
          fortranWriter.stringField(0, buildTitleLine(CrueIODico.TITRE + " " + trim));
        } else {
          fortranWriter.stringField(0, CrueIODico.TITRE);
        }
      } else {
        fortranWriter.stringField(0, buildTitleLine(trim));
      }
      fortranWriter.writeFields();
    }
    if (titles.length > NB_LINES_TITLE) {
      analyze_.addWarn("commentaire.tooManyLines");
    }
    /*
     * onécrit nblignes-1 de titre.
     */
    for (int i = titles.length; i < NB_LINES_TITLE; i++) {
      fortranWriter.stringField(0, writeTitre ? CrueIODico.TITRE : StringUtils.EMPTY);
      fortranWriter.writeFields();
    }

    writeCom("");
  }

  private String buildTitleLine(String titre) {
    if (titre.length() > CrueIODico.TITRE_MAX_CHAR) {
      if (!warnDoneForTitle) {
        analyze_.addWarn("commentaireLine.tooLongLine");
        warnDoneForTitle = true;
      }
      titre = StringUtils.substring(titre, 0, CrueIODico.TITRE_MAX_CHAR);
    }
    return titre;
  }

  @Override
  public void setFile(final File _f) {
    _f.getParentFile().mkdirs();
    analyze_ = new CtuluLog();
    analyze_.setResource(_f.getAbsolutePath());
    analyze_.setDesc(_f.getName());
    analyze_.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    try {
      out_ = new OutputStreamWriter(new FileOutputStream(_f), AbstractCrue9Reader.getCrue9Charset());
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
    fortranWriter = new FortranWriter(out_);
  }

  protected void writeCarteValeur(final ItemVariable variable, final String carte, final double val1,
          final double val2) throws IOException {
    fortranWriter.stringField(0, carte);
    fortranWriter.stringField(1, CrueIODico.getCrue9Value(val1, variable));
    fortranWriter.stringField(2, CrueIODico.getCrue9Value(val2, variable));
    fortranWriter.writeFields();
  }

  protected void writeCarteValeurAcrive(final ItemVariable variable, final String carte, final double val1,
                                        final double val2, final boolean active) throws IOException {
    fortranWriter.stringField(0, carte);
    if (active) {
      fortranWriter.stringField(1, CrueIODico.getCrue9Value(val1, variable));
      fortranWriter.stringField(2, CrueIODico.getCrue9Value(val2, variable));
    } else {
      fortranWriter.stringField(1, CrueIODico.REGLE_INACTIVE);
    }
    fortranWriter.writeFields();
  }

  protected void writeCarteValeur(final ItemVariable variable, final String carte, final double val)
          throws IOException {
    fortranWriter.stringField(0, carte);
    fortranWriter.stringField(1, CrueIODico.getCrue9Value(val, variable));
    fortranWriter.writeFields();
  }

  protected void writeCarteValeurActive(final ItemVariable variable, final String carte, final double val,
                                        final boolean active) throws IOException {
    fortranWriter.stringField(0, carte);
    fortranWriter.stringField(1, active ? CrueIODico.getCrue9Value(val, variable) : CrueIODico.REGLE_INACTIVE);
    fortranWriter.writeFields();
  }
}
