/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rcal;

import com.thoughtworks.xstream.XStream;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.rdao.CommonResDao;
import org.fudaa.dodico.crue.io.rdao.CommonResDaoXstream;
import org.fudaa.dodico.crue.io.rdao.ContexteSimulationDaoXstream;
import org.fudaa.dodico.crue.io.rdao.ParametrageDaoXstream;
import org.fudaa.dodico.crue.io.rdao.StructureResultatsDaoXstream;
import org.fudaa.dodico.crue.metier.CrueFileType;

public class CrueDaoStructureRCAL implements CrueDataDaoStructure {

  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    xstream.alias(CrueFileType.RCAL.toString(), CrueDaoRCAL.class);

    CommonResDaoXstream.configureXstream(xstream);
    ParametrageDaoXstream.configureXstream(xstream);
    ContexteSimulationDaoXstream.configureXstream(xstream);
    StructureResultatsDaoXstream.configureXstream(xstream);
    ResultatsCalculPermanentDao.configureXstream(xstream);
    ResCalcTransDao.configureXstream(xstream);

  }

  public static class ResultatsCalculPermanentDao extends CommonResDao.NomRefDao {

    public static void configureXstream(final XStream xstream) {
      xstream.alias("ResCalcPseudoPerm", ResultatsCalculPermanentDao.class);
      xstream.alias("ResCalcPseudoPerm", ResultatsCalculPermanentDao.class);
      xstream.useAttributeFor(ResultatsCalculPermanentDao.class, "Href");
      xstream.useAttributeFor(ResultatsCalculPermanentDao.class, "OffsetMot");
    }
    String Href;
    int OffsetMot;

    public String getHref() {
      return Href;
    }

    public int getOffsetMot() {
      return OffsetMot;
    }
  }

  public static class ResCalcTransDao extends CommonResDao.NomRefDao {

    public static void configureXstream(final XStream xstream) {
      xstream.alias("ResCalcTrans", ResCalcTransDao.class);
      xstream.addImplicitCollection(ResCalcTransDao.class, "ResPdts");
      xstream.alias("ResPdt", ResPdtDao.class);
      xstream.useAttributeFor(ResPdtDao.class, "Href");
      xstream.useAttributeFor(ResPdtDao.class, "OffsetMot");
      xstream.useAttributeFor(ResPdtDao.class, "TempsSimu");
    }
    List<ResPdtDao> ResPdts;

    public List<ResPdtDao> getResPdts() {
      return ResPdts;
    }
  }

  public static class ResPdtDao {

    String Href;
    int OffsetMot;
    String TempsSimu;

    public String getTempsSimu() {
      return TempsSimu;
    }

    public String getHref() {
      return Href;
    }

    public int getOffsetMot() {
      return OffsetMot;
    }

    public void setHref(final String Href) {
      this.Href = Href;
    }

    public void setOffsetMot(final int OffsetMot) {
      this.OffsetMot = OffsetMot;
    }

    public void setTempsSimu(final String TempsSimu) {
      this.TempsSimu = TempsSimu;
    }
    
    
  }
}
