/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.common;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.crue.metier.emh.Point2D;

/**
 * @author deniger
 */
public abstract class AbstractSingleConverterPoint extends AbstractSingleConverter {

  private final Class clazz;

  /**
   * @param clazz la classe acceptee par ce converter
   */
  public AbstractSingleConverterPoint(final Class clazz) {
    super();
    this.clazz = clazz;
  }

  protected abstract Object createFor(double x, double y);

  @Override
  public boolean canConvert(final Class type) {
    return clazz.equals(type);
  }

  @Override
  public Object fromString(final String str) {
    if (CtuluLibString.isEmpty(str)) { return null; }
    final String[] xy = StringUtils.split(str);
    if (xy == null || xy.length != 2) {
      if (analyse != null) {
        analyse.addSevereError("io.convert.PtEvolutionFF.error", str);
      }
      return null;
    }
    final Object res;
    try {
      res = createFor(Double.parseDouble(xy[0]), Double.parseDouble(xy[1]));
    } catch (final NumberFormatException e) {
      if (analyse != null) {
        analyse.addSevereError("io.convert.PtEvolutionFF.error", str);
      }
      return null;
    }
    return res;
  }

  @Override
  public String toString(final Object obj) {
    if (obj == null) { return CtuluLibString.EMPTY_STRING; }
    final Point2D in = (Point2D) obj;
    return in.getAbscisse() + " " + in.getOrdonnee();
  }

}
