/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores.old;

/**
 * @author Frederic Deniger
 */
public class DaoOrdResSectionSansGeometrieOld extends DaoOrdResSectionOld {

  public DaoOrdResSectionSansGeometrieOld() {
  }

  public DaoOrdResSectionSansGeometrieOld(final DaoOrdResSectionOld other) {
    super(other);
  }
  
}
