/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores.old;

public class DaoOrdResBrancheSaintVenantOld {
  // WARN: l'ordre des champs est important car utilise par l'ecriture/lecture de ORES

  private boolean ddeQlat;
  private boolean ddeSplanAct;
  private boolean ddeSplanSto;
  private boolean ddeSplanTot;
  private boolean ddeVol;

  public final boolean getDdeQlat() {
    return ddeQlat;
  }

  public final boolean getDdeSplanAct() {
    return ddeSplanAct;
  }

  public final boolean getDdeSplanSto() {
    return ddeSplanSto;
  }

  public final boolean getDdeSplanTot() {
    return ddeSplanTot;
  }

  public final boolean getDdeVol() {
    return ddeVol;
  }

  public final void setDdeQlat(final boolean newDdeQlat) {
    ddeQlat = newDdeQlat;
  }

  /**
   * @param newDdeVol
   */
  public final void setDdeVol(final boolean newDdeVol) {
    ddeVol = newDdeVol;
  }

  /**
   * @param ddeSplanSto the ddeSplanSto to set
   */
  public void setDdeSplanSto(final boolean ddeSplanSto) {
    this.ddeSplanSto = ddeSplanSto;
  }

  /**
   * @param ddeSplanStot the ddeSplanStot to set
   */
  public void setDdeSplanTot(final boolean ddeSplanStot) {
    this.ddeSplanTot = ddeSplanStot;
  }

  /**
   * @param ddeSplanAct the ddeSplanAct to set
   */
  public void setDdeSplanAct(final boolean ddeSplanAct) {
    this.ddeSplanAct = ddeSplanAct;
  }

  @Override
  public String toString() {
    return "OrdResBrancheSaintVenant [ddeQlat=" + ddeQlat + ", ddeSplanAct=" + ddeSplanAct + ", ddeSplanSto="
            + ddeSplanSto + ", ddeSplanStot=" + ddeSplanTot + ", ddeVol=" + ddeVol + "]";
  }
}
