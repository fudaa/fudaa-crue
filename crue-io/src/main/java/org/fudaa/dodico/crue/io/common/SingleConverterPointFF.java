/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.common;

import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;

/**
 * @author deniger
 */
public class SingleConverterPointFF extends AbstractSingleConverterPoint {

  /**
   * Constructeur par défaut.
   */
  public SingleConverterPointFF() {
    super(PtEvolutionFF.class);
  }

  @Override
  protected Object createFor(double x, double y) {
    return new PtEvolutionFF(x, y);
  }

}
