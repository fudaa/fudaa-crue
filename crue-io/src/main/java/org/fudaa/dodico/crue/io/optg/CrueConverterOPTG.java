/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.optg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.optg.CrueDaoStructureOPTG.DaoPlanimetrageNbrPdzCst;
import org.fudaa.dodico.crue.io.optg.CrueDaoStructureOPTG.Planimetrage;
import org.fudaa.dodico.crue.io.optg.CrueDaoStructureOPTG.RegleDAO;
import org.fudaa.dodico.crue.io.optg.CrueDaoStructureOPTG.RegleSeuilDetect;
import org.fudaa.dodico.crue.io.optg.CrueDaoStructureOPTG.RegleVarPdxMax;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.comparator.RegleTypeComparator;
import org.fudaa.dodico.crue.metier.emh.EnumRegle;
import org.fudaa.dodico.crue.metier.emh.OrdPrtGeoModeleBase;
import org.fudaa.dodico.crue.metier.emh.PlanimetrageNbrPdzCst;
import org.fudaa.dodico.crue.metier.emh.Regle;
import org.fudaa.dodico.crue.metier.emh.Sorties;
import org.fudaa.dodico.crue.metier.emh.ValParamDouble;
import org.fudaa.dodico.crue.metier.emh.ValParamEntier;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;

public class CrueConverterOPTG implements CrueDataConverter<CrueDaoOPTG, OrdPrtGeoModeleBase> {

  private BidiMap getCorrespondance() {
    final BidiMap daoToRegle = new DualHashBidiMap();
    daoToRegle.put(CrueDaoStructureOPTG.RegleDecal.class, EnumRegle.DECAL);
    daoToRegle.put(CrueDaoStructureOPTG.RegleLargSeuil.class, EnumRegle.LARG_SEUIL);
    daoToRegle.put(CrueDaoStructureOPTG.ReglePdxMax.class, EnumRegle.PDX_MAX);
    daoToRegle.put(CrueDaoStructureOPTG.ReglePenteMax.class, EnumRegle.PENTE_MAX);
    daoToRegle.put(CrueDaoStructureOPTG.ReglePenteRupture.class, EnumRegle.PENTE_RUPTURE);
    daoToRegle.put(CrueDaoStructureOPTG.RegleProfPlat.class, EnumRegle.PROF_PLAT);
    daoToRegle.put(CrueDaoStructureOPTG.RegleRebDeb.class, EnumRegle.REB_DEB);
    daoToRegle.put(CrueDaoStructureOPTG.RegleVarPdxMax.class, EnumRegle.VAR_PDX_MAX);
    return daoToRegle;
  }

  @Override
  public OrdPrtGeoModeleBase getConverterData(final CrueData in) {
    return in.getOPTG();
  }

  @Override
  public OrdPrtGeoModeleBase convertDaoToMetier(final CrueDaoOPTG dao, final CrueData dataLinked,
                                                final CtuluLog ctuluLog) {
    final OrdPrtGeoModeleBase ordres = dataLinked.getOrCreateOPTG();

    if (dao.Planimetrage != null && dao.Planimetrage.PlanimetrageNbrPdzCst != null) {
      final PlanimetrageNbrPdzCst planiMetier = new PlanimetrageNbrPdzCst(
              dataLinked.getCrueConfigMetier());
      planiMetier.setNbrPdz((int) Double.parseDouble(dao.Planimetrage.PlanimetrageNbrPdzCst.NbrPdz));
      ordres.setPlanimetrage(planiMetier);
    }
    if (dao.Sorties != null) {
      ordres.getSorties().initWith(dao.Sorties);
    }
    if (dao.Regles != null) {
      final Map classToEnum = getCorrespondance();
      for (final RegleDAO regleDAO : dao.Regles) {
        final Regle regle = ordres.getRegle((EnumRegle) classToEnum.get(regleDAO.getClass()));
        regle.setActive(regleDAO.IsActive);
        if (regleDAO instanceof RegleSeuilDetect) {
          regle.setValParam(new ValParamDouble(CruePrefix.P_VAL_PARAM + regle.getNom(), ((RegleSeuilDetect) regleDAO).getValue()));
        } else if (regleDAO instanceof RegleVarPdxMax) {
          final RegleVarPdxMax pdx = (RegleVarPdxMax) regleDAO;
          regle.setValParam(new ValParamEntier(CruePrefix.P_VAL_PARAM + regle.getNom(), pdx.getValue()));
        }
      }
    }
    return ordres;
  }
  private final static Logger LOGGER = Logger.getLogger(CrueConverterOPTG.class.getName());

  @Override
  public CrueDaoOPTG convertMetierToDao(final OrdPrtGeoModeleBase metier, final CtuluLog ctuluLog) {
    final CrueDaoOPTG dao = new CrueDaoOPTG();

    if (metier.getPlanimetrage() instanceof PlanimetrageNbrPdzCst) {
      dao.Planimetrage = new Planimetrage();
      dao.Planimetrage.PlanimetrageNbrPdzCst = new DaoPlanimetrageNbrPdzCst();
      dao.Planimetrage.PlanimetrageNbrPdzCst.NbrPdz = Integer.toString(
              ((PlanimetrageNbrPdzCst) metier.getPlanimetrage()).getNbrPdz());
    }
    dao.Sorties = new Sorties(metier.getSorties());
    if (CollectionUtils.isNotEmpty(metier.getRegles())) {

      dao.Regles = new ArrayList<>();
      final BidiMap classToEnum = getCorrespondance();
      final List<Regle> rgs = new ArrayList<>(metier.getRegles());
      Collections.sort(rgs, RegleTypeComparator.INSTANCE);
      for (final Regle regleMetier : rgs) {
        final Class daoClass = (Class) classToEnum.getKey(regleMetier.getType());
        RegleDAO reglePersist = null;
        try {
          reglePersist = (RegleDAO) daoClass.newInstance();
        } catch (final Exception e) {
          LOGGER.log(Level.SEVERE, "convertMetierToDao", e);
          return dao;
        }
        reglePersist.IsActive = regleMetier.isActive();
        if (reglePersist instanceof RegleSeuilDetect) {
          ((RegleSeuilDetect) reglePersist).setValue(((ValParamDouble) regleMetier.getValParam()).getValeur());
        } else if (reglePersist instanceof RegleVarPdxMax) {
          final RegleVarPdxMax pdx = (RegleVarPdxMax) reglePersist;
          pdx.setValue(((ValParamEntier) regleMetier.getValParam()).getValeur());
        }

        dao.Regles.add(reglePersist);

      }

    }

    return dao;
  }
}
