/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.common;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.metier.CrueData;

/**
 * @author deniger Interface pour les converter entre les objet dao et les objet metier
 * @param <D> Represente la structure DAO
 * @param <M> Represente le modele Metier
 */
public interface CrueDataConverter<D extends AbstractCrueDao, M> {

  /**
   * Remplit les infos de la classe persistantes avec les données métier appropriées. Cette méthode est appelée dans le constructeur.
   *
   * @param dao la DAO a convertir
   * @param ctuluLog les logs
   * @param dataLinked les données a modifier
   * @return l'objet metier
   */
  M convertDaoToMetier(D dao, CrueData dataLinked, CtuluLog ctuluLog);

  /**
   * @param in les données
   * @return les données gérées par ce converter
   */
  M getConverterData(CrueData in);

  /**
   * Crée une structure métier à partir des informations persistantes. Utiliser pour le remplissage de la structure métier apres lecture du fichier
   * xml. Utilise en plus des données liées.
   *
   * @param metier le metier
   * @param ctuluLog le log
   * @return l'objet DAO
   */
  D convertMetierToDao(M metier, CtuluLog ctuluLog);
}
