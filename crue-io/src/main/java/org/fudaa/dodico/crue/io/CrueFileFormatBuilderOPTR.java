/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.optr.CrueConverterOPTR;
import org.fudaa.dodico.crue.io.optr.CrueDaoStructureOPTR;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.OrdPrtReseau;

public class CrueFileFormatBuilderOPTR implements CrueFileFormatBuilder<OrdPrtReseau> {

  @Override
  public Crue10FileFormat<OrdPrtReseau> getFileFormat(final CoeurConfigContrat version) {
    return new Crue10FileFormat<>(new CrueDataXmlReaderWriterImpl<>(CrueFileType.OPTR,
        version, new CrueConverterOPTR(), new CrueDaoStructureOPTR()));

  }

}
