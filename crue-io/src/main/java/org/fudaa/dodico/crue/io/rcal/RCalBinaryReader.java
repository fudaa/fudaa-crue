/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rcal;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import org.fudaa.dodico.crue.common.io.CacheFileInputStream;

/**
 * @author deniger
 */
public class RCalBinaryReader {

  private static final int REAL_SIZE = 8;
  File file;

  public RCalBinaryReader() {
  }

  public RCalBinaryReader(File file) {
    this.file = file;
  }

  public File getFile() {
    return file;
  }

  private ByteBuffer createByteBuffer(final int taille) {
    final ByteBuffer allocate = ByteBuffer.allocate(taille);
    allocate.order(ByteOrder.LITTLE_ENDIAN);
    return allocate;
  }

  public String getString(ByteBuffer bf, final int nbChar) {
    final byte[] charByte = new byte[nbChar];
    bf.get(charByte);
    return new String(charByte);
  }

  public String readString(final long position) throws IOException {
    return getString(readByte(position, REAL_SIZE), REAL_SIZE);
  }

  public double readDouble(final long position) throws IOException {
    return readByte(position, REAL_SIZE).getDouble();
  }

  public ByteBuffer readByte(final long position, final int size) throws IOException {
    FileInputStream fileInputStream = null;
    ByteBuffer bf = null;
    
    try {
      bf = createByteBuffer(size);
      fileInputStream = CacheFileInputStream.getINSTANCE().open(file);
      final FileChannel channel = fileInputStream.getChannel();
      channel.read(bf, position);
      bf.rewind();
    } finally {
      CacheFileInputStream.getINSTANCE().close(file, fileInputStream);
    }
    return bf;
  }

  public void setFile(File file) {
    this.file = file;
    assert file != null;
  }

  public double[] readDoubles(long offsetInFile, int length) throws IOException {
    ByteBuffer buffer = readByte(offsetInFile, length);
    int nbDouble = length / REAL_SIZE;
    double[] res = new double[nbDouble];
    for (int i = 0; i < res.length; i++) {
      res[i] = buffer.getDouble();
    }
    return res;
  }
}
