/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rptr;

import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.rptr.CrueDaoStructureRPTR.ResPrtReseauNoeudsDao;

/**
 * Représentation persistante du fichier xml PNUM (Paramètres numériques).
 *
 * @author CDE
 */
public class CrueDaoRPTR extends AbstractCrueDao {

  List<ResPrtReseauNoeudsDao> ResPrtReseauNoeudsDaos;
}
