package org.fudaa.dodico.crue.io.etu;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Permet de valider un projet: test si les fichier DRSO, DCSP ou DPTG ne sont utilisés que par un seul projet
 *
 * @author deniger
 */
public class EMHProjectValidation {
  public CtuluLog validProject(EMHProjet emhProjet) {
    return validProject(emhProjet, null);
  }

  /**
   * @param emhProjet projet à valider
   * @return les erreurs ( fichier DRSO, DCSP ou DPTG  utilisés par différents sous-modeles...)
   */
  public CtuluLog validProject(EMHProjet emhProjet, CtuluLog parentLog) {

    Map<FichierCrue, List<String>> sousModeleUsingFichierCrue = getSousModeleNameUsingDedicatedFile(emhProjet);

    final CtuluLog log = parentLog == null ? new CtuluLog(BusinessMessages.RESOURCE_BUNDLE) : parentLog;
    sousModeleUsingFichierCrue.forEach((fichierCrue, sousModeleNames) -> {
      if (sousModeleNames.size() > 1) {
        log.addError("project.file.usedBySeveralSousModele", fichierCrue.getNom(), ToStringHelper.join(sousModeleNames));
      }
    });
    return log;
  }

  /**
   * @param emhProjet le projet
   * @return pour les nom des sous-modeles utilisant un fichier DRSO, DCSP, DPTG ( fichiers a usage unique)
   */
  public static Map<FichierCrue, List<String>> getSousModeleNameUsingDedicatedFile(EMHProjet emhProjet) {
    Map<FichierCrue, List<String>> sousModeleUsingFichierCrue = new HashMap<>();
    emhProjet.getListeSousModeles()
        .forEach(managerEMHSousModele -> managerEMHSousModele.getFichiers().stream().filter(fichierCrue -> isFileToBeUsedByOneSousModele(fichierCrue))
            .forEach(fichierCrue -> collectFiles(managerEMHSousModele.getNom(), fichierCrue, sousModeleUsingFichierCrue)));
    return sousModeleUsingFichierCrue;
  }

  private static boolean isFileToBeUsedByOneSousModele(FichierCrue fichierCrue) {
    final CrueFileType type = fichierCrue.getType();
    return CrueFileType.DRSO.equals(type)
        || CrueFileType.DCSP.equals(type)
        || CrueFileType.DPTG.equals(type);
  }

  private static void collectFiles(String sousModeleName, FichierCrue fichierCrue, Map<FichierCrue, List<String>> sousModeleUsingFichierCrue) {
    List<String> modeleNameList = sousModeleUsingFichierCrue.get(fichierCrue);
    if (modeleNameList == null) {
      modeleNameList = new ArrayList<>();
      sousModeleUsingFichierCrue.put(fichierCrue, modeleNameList);
    }
    modeleNameList.add(sousModeleName);
  }
}
