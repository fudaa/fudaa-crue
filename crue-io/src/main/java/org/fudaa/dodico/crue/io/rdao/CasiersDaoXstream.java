/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import com.thoughtworks.xstream.XStream;

/**
 *
 * @author deniger
 */
public class CasiersDaoXstream {

  public static void configureXstream(final XStream xstream) {
    xstream.alias("Casiers", CasiersDao.class);
    xstream.alias("CasierProfil", CasiersDao.CasierTypeDao.class);
    xstream.alias("Casier", CasiersDao.CasierDao.class);

    xstream.addImplicitCollection(CasiersDao.class, "VariableRes", CommonResDao.VariableResDao.class);
    xstream.addImplicitCollection(CasiersDao.CasierTypeDao.class, "VariableRes", CommonResDao.VariableResDao.class);

    xstream.addImplicitCollection(CasiersDao.CasierTypeDao.class, "Casier", CasiersDao.CasierDao.class);

  }
}
