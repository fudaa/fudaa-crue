/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.res;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.ItemResDao;
import org.fudaa.dodico.crue.io.rdao.ResContainerDao;
import org.fudaa.dodico.crue.io.rdao.ResContainerEMHCat;
import org.fudaa.dodico.crue.io.rdao.ResContainerEMHType;
import org.fudaa.dodico.crue.io.rdao.StructureResultatsDao;

/**
 *
 * @author deniger
 */
public class ResTypeEMHBuilder {

  private final ResVariablesAnalyser analyser = new ResVariablesAnalyser();
  private final ResContainerDao container;
  private final int tailleMot;
  private CtuluLog log;
  final Set<String> doublons = new HashSet<>();
  ResVariablesContent structureVariablesContent;

  public ResTypeEMHBuilder(ResContainerDao container) {
    this.container = container;

    tailleMot = container.getParametrage().getNbrOctetMot();
  }

  private ResVariablesContent compute(ResVariablesAnalyser.Result result) {
    doublons.addAll(result.varEnDoublon);
    return result.content;
  }

  public CrueIOResu<List<ResCatEMHContent>> extract() {
    log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("io.res.analyseXml");
    doublons.clear();
    CrueIOResu<List<ResCatEMHContent>> res = new CrueIOResu<>();
    List<ResCatEMHContent> catContainer = new ArrayList<>();
    res.setAnalyse(log);
    res.setMetier(catContainer);
    final StructureResultatsDao struct = container.getStructureResultat();
    structureVariablesContent = compute(analyser.extractResult(struct.getVariableRes()));

    //en premier il y a "  RcalPp" ou autre d'ou l'initialisation:
    int bytesOffset = tailleMot;
    for (ResContainerEMHCat cat : struct.getResContainerEMHCat()) {
      ResCatEMHContent byCat = extractCat(cat, bytesOffset);
      if (log.containsSevereError()) {
        return res;
      }
      bytesOffset += getBytes(cat.getNbrMot());
      if (byCat != null) {//erreur
        catContainer.add(byCat);
      }
    }
    if (!doublons.isEmpty()) {
      List<String> doublonsList = new ArrayList<>(doublons);
      Collections.sort(doublonsList);
      for (String string : doublonsList) {
        log.addWarn("io.res.doublonFound", string);

      }
    }
    return res;
  }

  private int getBytes(int nbrMot) {
    return tailleMot * nbrMot;
  }

  public String getDelimiteurValue(String nom) {
    return container.getParametrage().getDelimiteurChaine(nom);
  }

  private ResCatEMHContent extractCat(ResContainerEMHCat cat, int bytesOffset) {
    ResCatEMHContent res = new ResCatEMHContent();
    res.setPositionBytes(bytesOffset);
    String delimiteur = getDelimiteurValue(cat.getDelimiteurNom());
    if (StringUtils.isEmpty(delimiteur)) {
      log.addSevereError("io.res.delimiteurNotFound", cat.getDelimiteurNom());
      return null;
    }
    res.setDelimiteur(delimiteur);
    List<ResTypeEMHContent> list = new ArrayList<>();
    res.setResTypeEMHContent(list);
    //pour lire après le délimiteur:
    int typeBytesOffset = bytesOffset + tailleMot;
    ResVariablesContent categoryVariableContent = compute(analyser.mergeWithParent(cat.getVariableRes(),
                                                                                   structureVariablesContent));
    for (ResContainerEMHType type : cat.getResContainerEMHType()) {
      ResTypeEMHContent typeEMHContent = extractType(typeBytesOffset, type, categoryVariableContent);
      if (log.containsSevereError()) {
        return null;
      }
      list.add(typeEMHContent);
      //on incrémente
      typeBytesOffset += getBytes(type.getNbrMot());


    }

    return res;
  }

  private ResTypeEMHContent extractType(int typeBytesOffset, ResContainerEMHType typeData,
                                        ResVariablesContent parentVariableContent) {
    ResTypeEMHContent typeEMHContent = new ResTypeEMHContent(typeData.getTypeEMH());
    typeEMHContent.setPositionInBytes(typeBytesOffset);
    ResVariablesContent typeVariableContent = compute(analyser.mergeWithParent(typeData.getVariableRes(),
                                                                               parentVariableContent));
    typeEMHContent.setResVariablesContent(typeVariableContent);
    int bytesOffset = typeBytesOffset;
    if (typeData.getItemRes() != null) {
      for (ItemResDao itemRes : typeData.getItemRes()) {
        typeEMHContent.addItemResDao(itemRes, bytesOffset);
        bytesOffset += getBytes(itemRes.getNbrMot());
      }
    }
    return typeEMHContent;

  }
}
