package org.fudaa.dodico.crue.io.lhpt;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.io.common.AbstractDaoLoi;
import org.fudaa.dodico.crue.io.dlhy.ConvertLoi;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;
import org.fudaa.dodico.crue.metier.emh.LoiTF;

/**
 * @author deniger
 */
public final class ConvertLoiTF implements ConvertLoi {

    public DaoLoiTF metiertoDao(final AbstractLoi in) {
        final DaoLoiTF outLoi = new DaoLoiTF();
        outLoi.Nom = in.getNom();
        outLoi.setCommentaire(StringUtils.defaultString(in.getCommentaire()));
        outLoi.setType(in.getType());
        outLoi.EvolutionTF = ((LoiTF) in).getEvolutionTF();
        return outLoi;
    }

    public LoiTF daoToMetier(final AbstractDaoLoi in) {
        final LoiTF loiTF = new LoiTF();
        loiTF.setNom(((DaoLoiTF) in).Nom);
        loiTF.setCommentaire(in.getCommentaire());
        loiTF.setType(in.getType());
        loiTF.setEvolutionTF(((DaoLoiTF) in).EvolutionTF);
        return loiTF;
    }
}
