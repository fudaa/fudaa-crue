/*
GPL 2
 */
package org.fudaa.dodico.crue.io;

import java.io.File;
import java.net.URL;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.common.CrueXmlReaderWriter;
import org.fudaa.dodico.crue.io.ocal.CrueConverterOCAL;
import org.fudaa.dodico.crue.io.ocal.CrueDaoOCAL;
import org.fudaa.dodico.crue.metier.emh.OrdCalcScenario;
import org.fudaa.dodico.crue.metier.emh.ui.OrdCalcScenarioUiState;

/**
 * Un FileFormat spécificique pour ocal permettant de lire les fichier de persistence ui.
 *
 * @author Frederic Deniger
 */
public class Crue10FileFormatOCAL extends Crue10FileFormat<OrdCalcScenario> {

  /**
   *
   * @param readerWriter le readerWriterh
   */
  public Crue10FileFormatOCAL(CrueXmlReaderWriter<OrdCalcScenario> readerWriter) {
    super(readerWriter);
  }

  /**
   *
   * @param url l'url du fichier a lire
   * @param analyzer contiendra les résultat de la lecture
   * @param ccm le CrueConfigMetier
   * @return resultat de lecture
   */
  public final CrueIOResu<OrdCalcScenarioUiState> readUiState(final URL url, final CtuluLog analyzer, CrueConfigMetier ccm) {
    CrueDataXmlReaderWriterImpl<CrueDaoOCAL, OrdCalcScenario> crueDataXmlReaderWriterImpl = (CrueDataXmlReaderWriterImpl<CrueDaoOCAL, OrdCalcScenario>) readerWriter;
    final CrueDaoOCAL readDao = crueDataXmlReaderWriterImpl.readDao(url, analyzer, null);
    return processReadMethod(crueDataXmlReaderWriterImpl, readDao, ccm, analyzer, url.getFile());

  }

  /**
   *
   * @param file le fichier a lire
   * @param analyzer contiendra les résultat de la lecture
   * @param ccm le CrueConfigMetier
   * @return resultat de lecture
   */
  public final CrueIOResu<OrdCalcScenarioUiState> readUiState(final File file, final CtuluLog analyzer, CrueConfigMetier ccm) {
    CrueDataXmlReaderWriterImpl<CrueDaoOCAL, OrdCalcScenario> crueDataXmlReaderWriterImpl = (CrueDataXmlReaderWriterImpl<CrueDaoOCAL, OrdCalcScenario>) readerWriter;
    final CrueDaoOCAL readDao = crueDataXmlReaderWriterImpl.readDao(file, analyzer, null);
    String resourceName = file.getName();
    return processReadMethod(crueDataXmlReaderWriterImpl, readDao, ccm, analyzer, resourceName);
  }

  /**
   *
   * @param file le fichier a lire
   * @param analyzer contiendra les logs de la lecture
   * @param ccm le CrueConfigMetier
   * @return resultat de lecture
   */
  public final CrueIOResu<OrdCalcScenarioUiState> readUiState(final String file, final CtuluLog analyzer, CrueConfigMetier ccm) {
    CrueDataXmlReaderWriterImpl<CrueDaoOCAL, OrdCalcScenario> crueDataXmlReaderWriterImpl = (CrueDataXmlReaderWriterImpl<CrueDaoOCAL, OrdCalcScenario>) readerWriter;
    final CrueDaoOCAL readDao = crueDataXmlReaderWriterImpl.readDao(file, analyzer, null);
    return processReadMethod(crueDataXmlReaderWriterImpl, readDao, ccm, analyzer, file);

  }

  /**
   *
   * @param metier la donnée a ecrire
   * @param file le fichier de destination
   * @param analyzer contiendra les logs de l'écriture
   * @param ccm le CrueConfigMetier
   * @return true si ecriture ok
   */
  public final boolean writeUiState(OrdCalcScenarioUiState metier, final File file, final CtuluLog analyzer, CrueConfigMetier ccm) {
    CrueDataXmlReaderWriterImpl<CrueDaoOCAL, OrdCalcScenario> crueDataXmlReaderWriterImpl = (CrueDataXmlReaderWriterImpl<CrueDaoOCAL, OrdCalcScenario>) readerWriter;
    CrueConverterOCAL converter = (CrueConverterOCAL) crueDataXmlReaderWriterImpl.getConverter();
    final CrueDaoOCAL dao = converter.convertMetierToDaoUiState(metier, analyzer);
    dao.setXsdName(crueDataXmlReaderWriterImpl.getXsdFile());
    return crueDataXmlReaderWriterImpl.writeDAO(file, dao, analyzer, ccm);
  }

  /**
   * Methode privée pour lire les données
   *
   * @param crueDataXmlReaderWriterImpl
   * @param readDao
   * @param ccm
   * @param analyzer
   * @param resourceName
   * @return
   */
  private CrueIOResu<OrdCalcScenarioUiState> processReadMethod(
          CrueDataXmlReaderWriterImpl<CrueDaoOCAL, OrdCalcScenario> crueDataXmlReaderWriterImpl, final CrueDaoOCAL readDao, CrueConfigMetier ccm,
          final CtuluLog analyzer, String resourceName) {
    CrueConverterOCAL converter = (CrueConverterOCAL) crueDataXmlReaderWriterImpl.getConverter();
    final OrdCalcScenarioUiState convertDaoToMetier = converter.convertDaoToMetierUiState(readDao, ccm, analyzer);
    analyzer.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    analyzer.setDesc("read.file");
    analyzer.setDescriptionArgs(resourceName);
    return new CrueIOResu<>(convertDaoToMetier, analyzer);
  }

}
