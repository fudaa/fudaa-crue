/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.common;

import org.fudaa.dodico.crue.metier.emh.PtProfil;

/**
 * @author deniger
 */
public class SingleConverterPtProfil extends AbstractSingleConverterPoint {

  /**
   * Constructeur par defaut
   */
  public SingleConverterPtProfil() {
    super(PtProfil.class);
  }

  @Override
  public boolean canConvert(final Class type) {
    return PtProfil.class.equals(type);
  }

  @Override
  protected Object createFor(final double x, final double y) {
    final PtProfil res = new PtProfil();
    res.setXt(x);
    res.setZ(y);
    return res;
  }

}
