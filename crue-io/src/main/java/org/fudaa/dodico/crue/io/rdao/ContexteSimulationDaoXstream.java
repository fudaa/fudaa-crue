/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import com.thoughtworks.xstream.XStream;

/**
 *
 * @author deniger
 */
public class ContexteSimulationDaoXstream {
 public static void configureXstream(final XStream xstream) {
    xstream.alias("Scenario", ContexteSimulationDao.ScenarioDao.class);
    xstream.alias("Modele", ContexteSimulationDao.ModeleDao.class);
    xstream.alias("Run", ContexteSimulationDao.RunDao.class);

  }
}
