/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import org.fudaa.dodico.crue.io.rdao.CommonResDao.ItemResDao;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.VariableResDao;

import java.util.Collections;
import java.util.List;

/**
 *
 * @author deniger
 */
public class CasiersDao extends CommonResDao.NbrMotDao implements ResContainerEMHCat {

  public static class CasierDao extends ItemResDao {
  }

  public static class CasierTypeDao extends CommonResDao.TypeEMHDao implements ResContainerEMHType {

    List<CommonResDao.VariableResDao> VariableRes;
    List<CasierDao> Casier;

    public List<CasierDao> getCasier() {
      return Casier;
    }

    @Override
    public List<? extends ItemResDao> getItemRes() {
      return getCasier();
    }

    @Override
    public List<VariableResDao> getVariableRes() {
      return VariableRes;
    }
  }
  List<CommonResDao.VariableResDao> VariableRes;
  CasierTypeDao CasierProfil;

  public CasierTypeDao getCasierProfil() {
    return CasierProfil;
  }
  List<ResContainerEMHType> resContainerEMHType;

  @Override
  public List<ResContainerEMHType> getResContainerEMHType() {
    if (resContainerEMHType == null) {
      //attention, l'ordre est très important: doit suivre celui du fichier
      resContainerEMHType = Collections.unmodifiableList(Collections.<ResContainerEMHType>singletonList(CasierProfil));
    }
    return resContainerEMHType;

  }

  @Override
  public List<VariableResDao> getVariableRes() {
    return VariableRes;
  }

  @Override
  public String getDelimiteurNom() {
    return "CatEMHCasier";
  }
}
