/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores.old;

public class DaoOrdResBrancheBarrageGeneriqueOld {

  private boolean ddeRegimeBarrage;

  public boolean getDdeRegimeBarrage() {
    return ddeRegimeBarrage;
  }

  /**
   * @param ddeRegimeBarrage the ddeRegimeBarrage to set
   */
  public void setDdeRegimeBarrage(final boolean ddeRegimeBarrage) {
    this.ddeRegimeBarrage = ddeRegimeBarrage;
  }

  @Override
  public String toString() {
    return "OrdResBrancheBarrageGenerique [ddeRegime=" + ddeRegimeBarrage + "]";
  }
}
