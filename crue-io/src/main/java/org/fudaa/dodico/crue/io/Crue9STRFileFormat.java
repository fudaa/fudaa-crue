/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.io.neuf.AbstractCrueBinaryReader;
import org.fudaa.dodico.crue.io.neuf.STRReader;
import org.fudaa.dodico.crue.io.neuf.STRSequentialReader;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * @author cde
 */
public class Crue9STRFileFormat extends AbstractCrueBinaryFileFormat<STRSequentialReader> {

  /**
   * Constructeur qui précise l'extension autorisée pour ce type de fichier
   */
  public Crue9STRFileFormat() {
    super(CrueFileType.STR);
  }

  @Override
  protected AbstractCrueBinaryReader<STRSequentialReader> createReader() {
    return new STRReader();
  }

}
