/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dlhy;

import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.common.AbstractDaoLoi;

/**
 * Representation persistante du fichier xml DLHY.
 * 
 * @author Adrien Hadoux, Carole Delhaye
 */
@SuppressWarnings("PMD.VariableNamingConventions")
public class CrueDaoDLHY extends AbstractCrueDao {

  protected List<AbstractDaoLoi> Lois;

}
