/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.common.CrueHelper;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBrancheBarrageFilEauOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBrancheBarrageGeneriqueOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBrancheNiveauxAssociesOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBrancheOrificeOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBranchePdcOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBrancheSaintVenantOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBrancheSeuilLateralOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBrancheSeuilTransversalOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBrancheStricklerOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBranchesOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResCasierOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResCasiersOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResNoeudNiveauContinuOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResNoeudsOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResSectionIdem;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResSectionInterpolee;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResSectionOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResSectionProfilOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResSectionSansGeometrieOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResSectionsOld;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.*;

/**
 * Classe qui se charge de remplir les structures DAO du fichier ORES avec les donnees metier et inversement.
 *
 * @author cde
 */
public class CrueConverterORES_V1P1P1 implements CrueDataConverter<CrueDaoORES_V1P1P1, OrdResScenario> {

  public CrueConverterORES_V1P1P1() {
    super();
  }

  @Override
  public OrdResScenario getConverterData(final CrueData in) {
    return in.getORES();
  }

  /**
   * Conversion de la couche persistance vers la couche métier
   */
  @Override
  public OrdResScenario convertDaoToMetier(final CrueDaoORES_V1P1P1 dao, final CrueData dataLinked, final CtuluLog ctuluLog) {

    final OrdResScenario metier = dataLinked == null ? new OrdResScenario() : dataLinked.getOrCreateORES();

    // Noeuds
    if (dao.OrdResNoeuds.OrdResNoeudNiveauContinu != null) {
      final OrdResNoeudNiveauContinu res = new OrdResNoeudNiveauContinu();
      res.setValues(getDdes(dao.OrdResNoeuds.OrdResNoeudNiveauContinu));
      metier.setOrdResNoeudNiveauContinu(res);
    }
    if (dao.OrdResCasiers.OrdResCasierProfil != null) {
      final OrdResCasier casier = new OrdResCasier();
      casier.setValues(getDdes(dao.OrdResCasiers.OrdResCasierProfil));
      metier.setOrdResCasier(casier);
    }

    // Sections
    if (dao.OrdResSections.OrdResSectionProfil != null) {
      final OrdResSection section = createOresSection(dao.OrdResSections.OrdResSectionProfil);
      metier.setOrdResSection(section);
      final boolean same = dao.OrdResSections.OrdResSectionProfil.isSame(dao.OrdResSections.OrdResSectionIdem)
              && dao.OrdResSections.OrdResSectionProfil.isSame(dao.OrdResSections.OrdResSectionInterpolee)
              && dao.OrdResSections.OrdResSectionProfil.isSame(dao.OrdResSections.OrdResSectionSansGeometrie);
      //pour ajouter un warning si les dde sont différentes.
      if (!same) {
        ctuluLog.addWarn("io.ores.ordResSectionNotSame.warn");
      }
    }
    final DaoOrdResBranchesOld branches = dao.OrdResBranches;

    // Branches
    final DaoOrdResBranchePdcOld daoPdc = branches.OrdResBranchePdc;
    if (daoPdc != null) {
      final OrdResBranchePdc ores = new OrdResBranchePdc();
      ores.setValues(getDdes(daoPdc));
      metier.setOrdResBranchePdc(ores);
    }
    final DaoOrdResBrancheSeuilTransversalOld daoOrdResBrancheSeuilTransversal = branches.OrdResBrancheSeuilTransversal;
    if (daoOrdResBrancheSeuilTransversal != null) {
      final OrdResBrancheSeuilTransversal ores = new OrdResBrancheSeuilTransversal();
      ores.setValues(getDdes(daoOrdResBrancheSeuilTransversal));
      metier.setOrdResBrancheSeuilTransversal(ores);
    }

    final DaoOrdResBrancheSeuilLateralOld daoOrdResBrancheSeuilLateral = branches.OrdResBrancheSeuilLateral;
    if (daoOrdResBrancheSeuilLateral != null) {
      final OrdResBrancheSeuilLateral ores = new OrdResBrancheSeuilLateral();
      ores.setValues(getDdes(daoOrdResBrancheSeuilLateral));
      metier.setOrdResBrancheSeuilLateral(ores);
    }
    final DaoOrdResBrancheOrificeOld daoOrdResBrancheOrifice = branches.OrdResBrancheOrifice;
    if (daoOrdResBrancheOrifice != null) {
      final OrdResBrancheOrifice ores = new OrdResBrancheOrifice();
      ores.setValues(getDdes(daoOrdResBrancheOrifice));
      metier.setOrdResBrancheOrifice(ores);
    }

    final DaoOrdResBrancheNiveauxAssociesOld daoOrdResBrancheNiveauxAssocies = branches.OrdResBrancheNiveauxAssocies;
    if (daoOrdResBrancheNiveauxAssocies != null) {
      final OrdResBrancheNiveauxAssocies ores = new OrdResBrancheNiveauxAssocies();
      ores.setValues(getDdes(daoOrdResBrancheNiveauxAssocies));
      metier.setOrdResBrancheNiveauxAssocies(ores);
    }
    final DaoOrdResBrancheStricklerOld daoOrdResBrancheStrickler = branches.OrdResBrancheStrickler;
    if (daoOrdResBrancheStrickler != null) {
      final OrdResBrancheStrickler ores = new OrdResBrancheStrickler();
      ores.setValues(getDdes(daoOrdResBrancheStrickler));
      metier.setOrdResBrancheStrickler(ores);
    }
    final DaoOrdResBrancheBarrageGeneriqueOld daoOrdResBrancheBarrageGenerique = branches.OrdResBrancheBarrageGenerique;
    if (daoOrdResBrancheBarrageGenerique != null) {
      final OrdResBrancheBarrageGenerique ores = new OrdResBrancheBarrageGenerique();
      ores.setValues(getDdes(daoOrdResBrancheBarrageGenerique));
      metier.setOrdResBrancheBarrageGenerique(ores);
    }

    final DaoOrdResBrancheBarrageFilEauOld daoOrdResBrancheBarrageFilEau = branches.OrdResBrancheBarrageFilEau;
    if (daoOrdResBrancheBarrageFilEau != null) {
      final OrdResBrancheBarrageFilEau ores = new OrdResBrancheBarrageFilEau();
      ores.setValues(getDdes(daoOrdResBrancheBarrageFilEau));
      metier.setOrdResBrancheBarrageFilEau(ores);
    }
    final DaoOrdResBrancheSaintVenantOld daoOrdResBrancheSaintVenant = branches.OrdResBrancheSaintVenant;
    if (daoOrdResBrancheSaintVenant != null) {
      final OrdResBrancheSaintVenant ores = new OrdResBrancheSaintVenant();
      final List<Dde> ddes = getDdes(daoOrdResBrancheSaintVenant);
      ddes.add(new Dde("splanSto", daoOrdResBrancheSaintVenant.getDdeSplanAct()));
      ddes.add(new Dde("splanTot", daoOrdResBrancheSaintVenant.getDdeSplanAct()));
      ores.setValues(ddes);
      metier.setOrdResBrancheSaintVenant(ores);
    }
    loadImplicitValues(metier);
    return metier;
  }

  /**
   * To old version for which no OrdResBrancheNiveauxAssocies is set.
   *
   * @param metier les ordRes
   */
  private void loadImplicitValues(final OrdResScenario metier) {
    if (metier.getOrdResBrancheNiveauxAssocies() == null) {
      metier.setOrdResBrancheNiveauxAssocies(new OrdResBrancheNiveauxAssocies());
    }
    if (metier.getOrdResModeleRegul() == null) {
      metier.setOrdResModeleRegul(new OrdResModeleRegul());
    }
  }

  private List<Dde> getDdes(final Object dao) {
    final PropertyDescriptor[] propertyDescriptors = PropertyUtils.getPropertyDescriptors(dao);
    final List<Dde> ddes = new ArrayList<>();
    for (final PropertyDescriptor propertyDescriptor : propertyDescriptors) {
      if (propertyDescriptor.getName().startsWith(CrueDaoStructureORES_V1P1P1.PREFIX_PROPERTY_DDE)) {
        try {
          final Boolean value = (Boolean) PropertyUtils.getProperty(dao, propertyDescriptor.getName());
          final String propertyName = StringUtils.uncapitalize(StringUtils.removeStart(propertyDescriptor.getName(), CrueDaoStructureORES_V1P1P1.PREFIX_PROPERTY_DDE));
          ddes.add(new Dde(propertyName, value != null && value.booleanValue()));




        } catch (final Exception ex) {
          Logger.getLogger(CrueConverterORES_V1P1P1.class
                  .getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return ddes;
  }

  public void convertMetierToDao(final OrdRes in, final Object daoToFill, final CtuluLog log) {
    final PropertyDescriptor[] propertyDescriptors = PropertyUtils.getPropertyDescriptors(daoToFill);
    final Set<String> ddeProperties = new HashSet<>();
    for (final PropertyDescriptor propertyDescriptor : propertyDescriptors) {
      final String name = propertyDescriptor.getName();
      if (name.startsWith(CrueDaoStructureORES_V1P1P1.PREFIX_PROPERTY_DDE)) {
        ddeProperties.add(name);
      }
    }
    final Collection<Dde> ddes = in.getDdes();
    for (final Dde dde : ddes) {
      final String nom = dde.getNom();
      final String ddeNom = CrueDaoStructureORES_V1P1P1.PREFIX_PROPERTY_DDE + StringUtils.capitalize(nom);
      if (!ddeProperties.contains(ddeNom)) {
        log.addSevereError("io.ores.ddeUnknown", in.getClass().getSimpleName(), nom);
      } else {
        try {
          PropertyUtils.setProperty(daoToFill, ddeNom, dde.getValeur());
        } catch (final Exception exception) {
          Logger.getLogger(CrueConverterORES_V1P1P1.class
                  .getName()).log(Level.INFO, "message {0}", exception);
        }
      }
    }

  }

  /**
   * Conversion de la couche métier vers la couche persistance
   */
  @Override
  public CrueDaoORES_V1P1P1 convertMetierToDao(final OrdResScenario metier, final CtuluLog ctuluLog) {

    final CrueDaoORES_V1P1P1 res = new CrueDaoORES_V1P1P1();

    res.OrdResNoeuds = new DaoOrdResNoeudsOld();
    res.OrdResCasiers = new DaoOrdResCasiersOld();
    res.OrdResSections = new DaoOrdResSectionsOld();
    res.OrdResBranches = new DaoOrdResBranchesOld();

    final OrdResNoeudNiveauContinu noeudNivContinu = metier.getOrdResNoeudNiveauContinu();
    if (noeudNivContinu != null) {
      res.OrdResNoeuds.OrdResNoeudNiveauContinu = new DaoOrdResNoeudNiveauContinuOld();
      convertMetierToDao(noeudNivContinu, res.OrdResNoeuds.OrdResNoeudNiveauContinu, ctuluLog);
    }

    final OrdResCasier casierProfil = metier.getOrdResCasier();
    assert (casierProfil != null);
    res.OrdResCasiers.OrdResCasierProfil = new DaoOrdResCasierOld();
    convertMetierToDao(casierProfil, res.OrdResCasiers.OrdResCasierProfil, ctuluLog);

    final OrdResSection section = metier.getOrdResSection();
    assert (section != null);
    res.OrdResSections.OrdResSectionProfil = new DaoOrdResSectionProfilOld();
    convertMetierToDao(section, res.OrdResSections.OrdResSectionProfil, ctuluLog);
    res.OrdResSections.OrdResSectionIdem = new DaoOrdResSectionIdem(res.OrdResSections.OrdResSectionProfil);
    res.OrdResSections.OrdResSectionInterpolee = new DaoOrdResSectionInterpolee(res.OrdResSections.OrdResSectionProfil);
    res.OrdResSections.OrdResSectionSansGeometrie = new DaoOrdResSectionSansGeometrieOld(res.OrdResSections.OrdResSectionProfil);

    final OrdResBranchePdc branchePdc = metier.getOrdResBranchePdc();
    assert (branchePdc != null);
    res.OrdResBranches.OrdResBranchePdc = new DaoOrdResBranchePdcOld();
    convertMetierToDao(branchePdc, res.OrdResBranches.OrdResBranchePdc, ctuluLog);

    final OrdResBrancheSeuilTransversal brancheSeuilT = metier.getOrdResBrancheSeuilTransversal();
    assert (brancheSeuilT != null);
    res.OrdResBranches.OrdResBrancheSeuilTransversal = new DaoOrdResBrancheSeuilTransversalOld();
    convertMetierToDao(brancheSeuilT, res.OrdResBranches.OrdResBrancheSeuilTransversal, ctuluLog);

    final OrdResBrancheSeuilLateral brancheSeuilL = metier.getOrdResBrancheSeuilLateral();
    assert (brancheSeuilL != null);
    res.OrdResBranches.OrdResBrancheSeuilLateral = new DaoOrdResBrancheSeuilLateralOld();
    convertMetierToDao(brancheSeuilL, res.OrdResBranches.OrdResBrancheSeuilLateral, ctuluLog);

    final OrdResBrancheOrifice brancheOrifice = metier.getOrdResBrancheOrifice();
    assert (brancheOrifice != null);
    res.OrdResBranches.OrdResBrancheOrifice = new DaoOrdResBrancheOrificeOld();
    convertMetierToDao(brancheOrifice, res.OrdResBranches.OrdResBrancheOrifice, ctuluLog);

    final OrdResBrancheNiveauxAssocies ordResBrancheNiveauxAssocies = metier.getOrdResBrancheNiveauxAssocies();
    if (ordResBrancheNiveauxAssocies != null) {
      res.OrdResBranches.OrdResBrancheNiveauxAssocies = new DaoOrdResBrancheNiveauxAssociesOld();
      convertMetierToDao(ordResBrancheNiveauxAssocies, res.OrdResBranches.OrdResBrancheNiveauxAssocies, ctuluLog);
    }

    final OrdResBrancheStrickler brancheStric = metier.getOrdResBrancheStrickler();
    assert (brancheStric != null);
    res.OrdResBranches.OrdResBrancheStrickler = new DaoOrdResBrancheStricklerOld();
    convertMetierToDao(brancheStric, res.OrdResBranches.OrdResBrancheStrickler, ctuluLog);

    final OrdResBrancheBarrageGenerique brancheBarrageG = metier.getOrdResBrancheBarrageGenerique();
    assert (brancheBarrageG != null);
    res.OrdResBranches.OrdResBrancheBarrageGenerique = new DaoOrdResBrancheBarrageGeneriqueOld();
    convertMetierToDao(brancheBarrageG, res.OrdResBranches.OrdResBrancheBarrageGenerique, ctuluLog);


    final OrdResBrancheBarrageFilEau brancheBarrageFil = metier.getOrdResBrancheBarrageFilEau();
    assert (brancheBarrageFil != null);
    res.OrdResBranches.OrdResBrancheBarrageFilEau = new DaoOrdResBrancheBarrageFilEauOld();
    convertMetierToDao(brancheBarrageFil, res.OrdResBranches.OrdResBrancheBarrageFilEau, ctuluLog);

    final OrdResBrancheSaintVenant brancheStVenant = metier.getOrdResBrancheSaintVenant();
    assert (brancheStVenant != null);
    res.OrdResBranches.OrdResBrancheSaintVenant = new DaoOrdResBrancheSaintVenantOld();
    convertMetierToDao(brancheStVenant, res.OrdResBranches.OrdResBrancheSaintVenant, ctuluLog);

    if (res.OrdResCasiers.OrdResCasierProfil == null) {
      CrueHelper.emhEmpty("Casiers", ctuluLog);
    }
    if (res.OrdResNoeuds.OrdResNoeudNiveauContinu == null) {
      CrueHelper.emhEmpty("Noeuds", ctuluLog);
    }
    return res;
  }

  private OrdResSection createOresSection(final DaoOrdResSectionOld DaoOrdResSectionProfil) {
    final OrdResSection section = new OrdResSection();
    section.setValues(getDdes(DaoOrdResSectionProfil));
    return section;
  }
}
