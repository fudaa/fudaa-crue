/*
 GPL 2
 */
package org.fudaa.dodico.crue.io.conf;

import java.util.List;

/**
 * Contient les ressources nécessaires pour sauvegarder les objets.
 * @author Frederic Deniger
 */
public class CrueEtudeExternSaveContent {

  /**
   * une donnée complémentaire pour certaines données externes qui peuvent se sauvegarder dans un dossier.
   */
  private CrueEtudeExternDaoConfiguration config;
  private List<CrueEtudeExternAdditionalFileSaver> additionalFileSaver;

  public CrueEtudeExternDaoConfiguration getConfig() {
    return config;
  }

  public void setConfig(CrueEtudeExternDaoConfiguration config) {
    this.config = config;
  }

  public List<CrueEtudeExternAdditionalFileSaver> getAdditionalFileSaver() {
    return additionalFileSaver;
  }

  public void setAdditionalFileSaver(List<CrueEtudeExternAdditionalFileSaver> additionalFileSaver) {
    this.additionalFileSaver = additionalFileSaver;
  }
}
