/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.common;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.joda.time.LocalDateTime;

/**
 *
 * @author Frederic Deniger
 */
public class LocalDateTimeConverter implements Converter {

  @Override
  public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
    writer.setValue(DateDurationConverter.dateToXsd((LocalDateTime) source));
  }

  @Override
  public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
    return DateDurationConverter.getDate(reader.getValue());
  }

  @Override
  public boolean canConvert(Class type) {
    return LocalDateTime.class.equals(type);
  }
}
