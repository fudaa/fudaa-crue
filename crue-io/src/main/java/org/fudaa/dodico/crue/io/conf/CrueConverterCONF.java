/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.conf;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueConverter;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;
import org.fudaa.dodico.crue.projet.conf.Configuration;
import org.fudaa.dodico.crue.projet.conf.Option;
import org.fudaa.dodico.crue.projet.conf.SiteAide;
import org.fudaa.dodico.crue.projet.conf.SiteConfiguration;
import org.fudaa.dodico.crue.projet.conf.SiteOption;
import org.fudaa.dodico.crue.projet.conf.UserConfiguration;
import org.fudaa.dodico.crue.projet.conf.UserOption;

/**
 * Attention, à l'écriture ne s'occupe que de la partie User.
 *
 * @author CANEL Christophe
 */
public class CrueConverterCONF implements CrueConverter<CrueDaoCONF, Configuration> {

  /**
   * {@inheritDoc}
   */
  @Override
  public Configuration convertDaoToMetier(final CrueDaoCONF dao, final CtuluLog ctuluLog) {
    final Configuration metier = new Configuration();

    if (dao.Site != null) {
      metier.setSite(this.convertDaoToMetier(dao.Site, ctuluLog));
    }
    metier.setUser(this.convertDaoToMetier(dao.User));

    return metier;
  }

  private SiteConfiguration convertDaoToMetier(final CrueDaoStructureCONF.Site dao, final CtuluLog log) {
    final SiteConfiguration metier = new SiteConfiguration();
    final List<CoeurConfig> coeurs = new ArrayList<>(dao.Coeurs.size());
    final List<SiteOption> options = new ArrayList<>(dao.SiteOptions.size());
    SiteAide aide = null;
     if (dao.SiteAide != null)
     {
        aide = new SiteAide(dao.SiteAide.SyDocActivation, dao.SiteAide.CheminBase, dao.SiteAide.Type, dao.SiteAide.Commentaire);
        metier.setAide(aide);
     }
     
    for (final CrueDaoStructureCONF.Coeur coeur : dao.Coeurs) {
      coeurs.add(this.convertDaoToMetier(coeur));
    }
    final Set<String> ids = new HashSet<>();
    for (final CoeurConfig coeur : coeurs) {
      if (ids.contains(coeur.getName())) {
        log.addSevereError("coeur.validator.idNotUnique", coeur.getName());
      } else {
        ids.add(coeur.getName());
      }
    }

    metier.setCoeurs(coeurs);

    for (final CrueDaoStructureCONF.SiteOption option : dao.SiteOptions) {
      options.add(this.convertDaoToMetier(option));
    }

    metier.setOptions(options);
        
    return metier;
  }

  private CoeurConfig convertDaoToMetier(final CrueDaoStructureCONF.Coeur dao) {
    final CoeurConfig metier = new CoeurConfig();

    metier.setName(dao.id);
    metier.setComment(dao.Commentaire);
    metier.setXsdVersion(dao.VersionGrammaire);
    metier.setCrue9Dependant(dao.Crue9Dependant);
    metier.setCoeurFolderPath(dao.DossierCoeur);
    metier.setUsedByDefault(dao.CoeurParDefaut);

    return metier;
  }

  private SiteOption convertDaoToMetier(final CrueDaoStructureCONF.SiteOption dao) {
    final SiteOption metier = new SiteOption(dao.Nom, dao.Valeur, dao.Commentaire, dao.UserVisible);
    return metier;
  }
 
  private UserConfiguration convertDaoToMetier(final CrueDaoStructureCONF.User dao) {
    final UserConfiguration metier = new UserConfiguration();
    final List<UserOption> options = new ArrayList<>(dao.UserOptions.size());

    for (final CrueDaoStructureCONF.UserOption option : dao.UserOptions) {
      options.add(this.convertDaoToMetier(option));
    }

    metier.setOptions(options);

    return metier;
  }

  private UserOption convertDaoToMetier(final CrueDaoStructureCONF.UserOption dao) {
    final UserOption metier = new UserOption();

    metier.setNom(dao.Nom);
    metier.setCommentaire(dao.Commentaire);
    metier.setValeur(dao.Valeur);

    return metier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CrueDaoCONF convertMetierToDao(final Configuration metier, final CtuluLog ctuluLog) {
    final CrueDaoCONF dao = new CrueDaoCONF();

    dao.User = this.convertMetierToDao(metier.getUser());

    return dao;
  }

  private CrueDaoStructureCONF.User convertMetierToDao(final UserConfiguration metier) {
    final CrueDaoStructureCONF.User dao = new CrueDaoStructureCONF.User();
    final List<CrueDaoStructureCONF.UserOption> options = new ArrayList<>(metier.getOptions().size());

    for (final Option option : metier.getOptions()) {
      options.add(this.convertMetierToDao(option));
    }

    dao.UserOptions = options;

    return dao;
  }

  private CrueDaoStructureCONF.UserOption convertMetierToDao(final Option metier) {
    final CrueDaoStructureCONF.UserOption dao = new CrueDaoStructureCONF.UserOption();

    dao.Nom = metier.getId();
    dao.Commentaire = metier.getCommentaire();
    dao.Valeur = metier.getValeur();

    return dao;
  }
}
