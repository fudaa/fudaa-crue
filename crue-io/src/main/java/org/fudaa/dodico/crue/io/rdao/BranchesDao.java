/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.ItemResDao;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.VariableResDao;

/**
 *
 * @author deniger
 */
public class BranchesDao extends CommonResDao.NbrMotDao implements ResContainerEMHCat {

  public static class BrancheDao extends ItemResDao {
  }

  public static class BrancheTypeDao extends CommonResDao.TypeEMHDao implements ResContainerEMHType {

    List<CommonResDao.VariableResDao> VariableRes;
    List<BrancheDao> Branche;

    public List<BrancheDao> getBranche() {
      return Branche;
    }

    @Override
    public List<VariableResDao> getVariableRes() {
      return VariableRes;
    }

    @Override
    public List<? extends ItemResDao> getItemRes() {
      return getBranche();
    }
  }
  //attention, l'ordre est très important: doit suivre celui du fichier
  List<CommonResDao.VariableResDao> VariableRes;
  BrancheTypeDao BrancheBarrageFilEau;
  BrancheTypeDao BrancheBarrageGenerique;
  BrancheTypeDao BrancheNiveauxAssocies;
  BrancheTypeDao BrancheOrifice;
  BrancheTypeDao BranchePdc;
  BrancheTypeDao BrancheSaintVenant;
  BrancheTypeDao BrancheSeuilLateral;
  BrancheTypeDao BrancheSeuilTransversal;
  BrancheTypeDao BrancheStrickler;
  List<ResContainerEMHType> resContainerEMHType;

  @Override
  public List<ResContainerEMHType> getResContainerEMHType() {
    if (resContainerEMHType == null) {
      //attention, l'ordre est très important: doit suivre celui du fichier
      resContainerEMHType = Collections.unmodifiableList(Arrays.<ResContainerEMHType>asList(BrancheBarrageFilEau,
                                                                                            BrancheBarrageGenerique,
                                                                                            BrancheNiveauxAssocies, BrancheOrifice,
                                                                                            BranchePdc, BrancheSaintVenant,
                                                                                            BrancheSeuilLateral,
                                                                                            BrancheSeuilTransversal,
                                                                                            BrancheStrickler));
    }
    return resContainerEMHType;

  }

  @Override
  public List<VariableResDao> getVariableRes() {
    return VariableRes;
  }

  public BrancheTypeDao getBrancheBarrageFilEau() {
    return BrancheBarrageFilEau;
  }

  public BrancheTypeDao getBrancheBarrageGenerique() {
    return BrancheBarrageGenerique;
  }

  public BrancheTypeDao getBrancheNiveauxAssocies() {
    return BrancheNiveauxAssocies;
  }

  public BrancheTypeDao getBrancheOrifice() {
    return BrancheOrifice;
  }

  public BrancheTypeDao getBranchePdc() {
    return BranchePdc;
  }

  public BrancheTypeDao getBrancheSaintVenant() {
    return BrancheSaintVenant;
  }

  public BrancheTypeDao getBrancheSeuilLateral() {
    return BrancheSeuilLateral;
  }

  public BrancheTypeDao getBrancheSeuilTransversal() {
    return BrancheSeuilTransversal;
  }

  public BrancheTypeDao getBrancheStrickler() {
    return BrancheStrickler;
  }

  @Override
  public String getDelimiteurNom() {
    return "CatEMHBranche";
  }
}
