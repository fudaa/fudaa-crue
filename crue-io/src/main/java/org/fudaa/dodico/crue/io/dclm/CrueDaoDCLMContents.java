/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dclm;

import org.fudaa.dodico.crue.metier.emh.EnumSensOuv;

import java.util.List;

public class CrueDaoDCLMContents {
  /**
   * Element Branche Orifice utilisant la loi Manoeuvre
   */
  protected static class CalcTransBrancheOrificeManoeuvrePersist extends ActivableDcml {
    /**
     * Représente la valeur de la balise portant le même nom dans le fichier XML
     */
    protected EnumSensOuv SensOuv;
    /**
     * Représente la liste des Manoeuvre sans la balise englobante
     */
    protected CrueDaoDCLMContents.ManoeuvrePersist Manoeuvre;
  }
  protected static class CalcTransBrancheOrificeManoeuvreRegulPersist extends ActivableDcml {
    /**
     * Représente la valeur de la balise portant le même nom dans le fichier XML
     */
    protected EnumSensOuv SensOuv;
    /**
     * Représente la liste des Manoeuvres sans la balise englobante
     */
    protected CrueDaoDCLMContents.ManoeuvreRegulPersist ManoeuvreRegul;
  }

  // *** Definition des classes de structures contenant un attribut ***

  /**
   * Classe abstraite qui contient les attributs communs de calcul
   */
  public static abstract class CalculAbstractPersist {
    /**
     * Nom du calcul
     */
    public String Nom;
    /**
     * Commentaire du calcul
     */
    public String Commentaire;
  }

  /**
   * Calcul Permanent
   */
  public static class CalculPermanentPersist extends CalculAbstractPersist {
    /**
     * Représente la liste des elements qui composent le Calcul Permanent sans balise englobante
     */
    public List<RefDCLMAbstractPersist> listeElementsCalculPermanent;
  }

  /**
   * Calcul transitoire
   */
  public static class CalculTransitoirePersist extends CalculAbstractPersist {
    /**
     * Représente la liste des elements qui composent le Calcul Transitoire sans balise englobante
     */
    public List<RefDCLMAbstractPersist> listeElementsCalculTransitoire;
  }

  /**
   * Classe contenant l'attribut NomRef
   */
  public static abstract class RefDCLMAbstractPersist {
    /**
     * Représente la valeur de la balise portant le même nom dans le fichier XML
     */
    public String NomRef;

    public void setValue(final double value) {

    }
  }

  /**
   * Element Noeud de niveau continu Qapp
   */
  public static class CalcPseudoPermNoeudNiveauContinuQappPersist extends ActivableDcml {
    /**
     * Représente la valeur de la balise portant le même nom dans le fichier XML
     */
    public double Qapp;

    @Override
    public void setValue(final double value) {
      Qapp = value;
    }
  }

  /**
   * Element Noeud de niveau continu Zimpose
   */
  protected static class CalcPseudoPermNoeudNiveauContinuZimposePersist extends ActivableDcml {
    /**
     * Représente la valeur de la balise portant le même nom dans le fichier XML
     */
    protected double Zimp;

    @Override
    public void setValue(final double value) {
      Zimp = value;
    }
  }

  /**
   * Element Noeud de Branche Orifice Ouverture (vers le haut ou vers le bas)
   */
  protected static class CalcPseudoPermBrancheOrificeManoeuvrePersist extends ActivableDcml {
    /**
     * Représente la valeur de la balise portant le même nom dans le fichier XML
     */
    protected EnumSensOuv SensOuv;
    /**
     * Représente la valeur de la balise portant le même nom dans le fichier XML
     */
    protected double Ouv;

    @Override
    public void setValue(final double value) {
      Ouv = value;
    }
  }
  protected static class CalcPseudoPermBrancheOrificeManoeuvreRegulPersist extends ActivableDcml {
    /**
     * Représente la valeur de la balise portant le même nom dans le fichier XML
     */
    protected EnumSensOuv SensOuv;
    protected CrueDaoDCLMContents.ManoeuvreRegulPersist ManoeuvreRegul;

  }


  /**
   * Element Noeud de BrancheSaintVenantQruis
   */
  public static class CalcPseudoPermBrancheBrancheSaintVenantQruisPersist extends ActivableDcml {
    /**
     * Représente la valeur de la balise portant le même nom dans le fichier XML
     */
    public double Qruis;

    @Override
    public void setValue(final double value) {
      Qruis = value;
    }
  }

  /**
   * Element Noeud de CasierProfilQruis
   */
  public static class CalcPseudoPermBrancheCasierProfilQruisPersist extends ActivableDcml {
    /**
     * Représente la valeur de la balise portant le même nom dans le fichier XML
     */
    public double Qruis;

    @Override
    public void setValue(final double value) {
      Qruis = value;
    }
  }

  /**
   * pour DCLM de type CalcPseudoPermNoeudBg1
   */
  protected static class CalcPseudoPermNoeudBg1Persist extends ActivableDcml {
  }
  protected static class CalcPseudoPermNoeudBg1AvPersist extends ActivableDcml {
  }
  protected static class CalcPseudoPermNoeudBg2Persist extends ActivableDcml {
  }
  protected static class CalcPseudoPermNoeudBg2AvPersist extends ActivableDcml {
  }
  protected static class CalcPseudoPermNoeudUsiPersist extends ActivableDcml {
  }

  /**
   * Permet de représenter une loi DF de type Hydrogramme en persistance
   **/
  protected static class HydrogrammeQappPersist extends RefDCLMAbstractPersist {
  }
  protected static class HydrogrammeQappExtPersist extends RefDCLMAbstractPersist {
    public String NomFic;
    public String ResCalcTrans;
  }

  /**
   * Permet de représenter une loi DF de type HydrogrammeRuis en persistance
   **/
  protected static class HydrogrammeQruisPersist extends RefDCLMAbstractPersist {
  }

  /**
   * Permet de représenter une loi DF de type Limnigramme en persistance
   **/
  protected static class LimnigrammePersist extends RefDCLMAbstractPersist {
  }

  /**
   * Permet de représenter une loi DF de type Manoeuvre en persistance
   **/
  protected static class ManoeuvrePersist extends RefDCLMAbstractPersist {
  }
  protected static class ManoeuvreRegulPersist extends RefDCLMAbstractPersist {
    protected String Param;
  }

  /**
   * Permet de représenter une loi FF de type Tarrage en persistance
   **/
  protected static class TarragePersist extends RefDCLMAbstractPersist {
  }

  protected static class ActivableDcml extends RefDCLMAbstractPersist {
    protected boolean IsActive = true;
  }

  /**
   * Element Noeud de Noeud Niveau Continu utilisant la loi Hydrogramme
   */
  protected static class CalcTransNoeudNiveauContinuHydrogrammePersist extends ActivableDcml {
    /**
     * Représente la liste des Hydrogramme sans la balise englobante
     */
    protected HydrogrammeQappPersist HydrogrammeQapp;
  }

  protected static class CalcTransNoeudQappExtPersist extends ActivableDcml {
    protected HydrogrammeQappExtPersist HydrogrammeQappExt;
  }
  protected static class CalcTransNoeudBg1Persist extends ActivableDcml {
  }
  protected static class CalcTransNoeudBg2Persist extends ActivableDcml {
  }
  protected static class CalcTransNoeudUsinePersist extends ActivableDcml {
  }
  protected static class CalcTransNoeudBg1AvPersist extends ActivableDcml {
  }
  protected static class CalcTransNoeudBg2AvPersist extends ActivableDcml {
  }

  /**
   * Element Noeud de Noeud Niveau Continu utilisant la loi Limnigramme
   */
  protected static class CalcTransNoeudNiveauContinuLimnigrammePersist extends ActivableDcml {
    /**
     * Représente la liste des Limnigramme sans la balise englobante
     */
    protected LimnigrammePersist Limnigramme;
  }

  /**
   * Element Noeud de Noeud Niveau Continu utilisant la loi Tarrage
   */
  protected static class CalcTransNoeudNiveauContinuTaragePersist extends ActivableDcml {
    /**
     * Représente la liste des Tarrage sans la balise englobante
     */
    protected TarragePersist Tarage;
  }

  /**
   * Element Noeud de Branche SaintVenant utilisant la loi HydrogrammeRuis
   */
  protected static class CalcTransBrancheSaintVenantHydrogrammeRuisPersist extends ActivableDcml {
    /**
     * Représente la liste des HydrogrammeRuis sans la balise englobante
     */
    protected HydrogrammeQruisPersist HydrogrammeQruis;
  }

  /**
   * Element Noeud de Casier Profil utilisant la loi HydrogrammeRuis
   */
  protected static class CalcTransCasierProfilHydrogrammeRuisPersist extends ActivableDcml {
    /**
     * Représente la liste des HydrogrammeRuis sans la balise englobante
     */
    protected HydrogrammeQruisPersist HydrogrammeQruis;
  }
}
