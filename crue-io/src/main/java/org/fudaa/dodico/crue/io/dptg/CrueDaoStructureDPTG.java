/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dptg;

import com.thoughtworks.xstream.XStream;

import java.util.List;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.common.AbstractDaoFloatLoi;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.common.SingleConverterPtProfil;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EvolutionFF;
import org.fudaa.dodico.crue.metier.emh.PtProfil;

/**
 * Toutes les structures dao utilisées et les inits du parser xml pour le format DPTG. Toutes ces structures sont nécessaires pour bien formatter le
 * contenu xml. Ici il n'y a pas besoin de définir de sttructures supplémentaires.
 *
 * @author Adrien Hadoux
 */
public class CrueDaoStructureDPTG implements CrueDataDaoStructure {

    private final String version;

    /**
     * @param version la version
     */
    public CrueDaoStructureDPTG(final String version) {
        super();
        this.version = version;
    }

    @Override
    public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
        // -- creation des alias pour que ce soit + parlant dans le xml file --//
        xstream.alias(CrueFileType.DPTG.toString(), CrueDaoDPTG.class);
        // -- liste des initialisations necessaires pour le formattage des donnees--//
        initXmlParserSectionProfils(xstream);
        initXmlParserSections(xstream);
        initXmlParserCasierProfils(xstream);
        initXmlParserBranches(xstream);
        AbstractDaoFloatLoi.configureXstream(xstream, props, version);
        xstream.registerConverter(new SingleConverterPtProfil());
    }

    private void initXmlParserSectionProfils(final XStream xstream) {
        // -- alias gestion pour les frottements --//
        xstream.alias("ProfilSection", ProfilSection.class);

        xstream.alias("LitNumerote", Lit.class);

        xstream.useAttributeFor(ProfilSection.class, "Nom");
        // -- reference au frottement --//
        xstream.useAttributeFor(Frottement.class, "NomRef");
        if (Crue10VersionConfig.isV1_1_1(version)) {
            xstream.omitField(ProfilSection.class, "Etiquettes");
        }
        xstream.alias("Etiquette", Etiquette.class);
        xstream.useAttributeFor(Etiquette.class, "Nom");

    }

    private void initXmlParserCasierProfils(final XStream xstream) {
        xstream.alias("ProfilCasier", ProfilCasier.class);
        xstream.alias("BatiCasier", BatiCasier.class);
        xstream.useAttributeFor(AbstractCasier.class, "Nom");
        xstream.alias("LitUtile", LitUtile.class);
        if (Crue10VersionConfig.isV1_1_1(version)) {
            xstream.omitField(BatiCasier.class, "Commentaire");
        }
    }

    private static void initXmlParserSections(final XStream xstream) {
        xstream.alias("DonPrtGeoSectionIdem", SectionIdem.class);
        xstream.useAttributeFor(SectionIdem.class, "NomRef");
    }

    private static void initXmlParserBranches(final XStream xstream) {
        xstream.alias("DonPrtGeoBrancheSaintVenant", BrancheSaintVenant.class);
        xstream.useAttributeFor(BrancheSaintVenant.class, "NomRef");
    }

    protected static class Fente {

        protected Double LargeurFente;
        protected Double ProfondeurFente;
    }

    protected static class ProfilSection implements ObjetNomme {

        @Override
        public String getId() {
            return Nom;
        }

        @Override
        public String getNom() {
            return Nom;
        }

        @Override
        public void setNom(final String newNom) {
            Nom = newNom;
        }

        protected String Nom;
        /**
         * optionnel.
         */
        protected String Commentaire;
        protected Fente Fente;
        protected EvolutionFF EvolutionFF;
        protected List<Lit> LitNumerotes;
        protected List<Etiquette> Etiquettes;
    }

    protected static class Lit {

        protected PtProfil LimDeb;
        protected PtProfil LimFin;
        protected String LitNomme;
        protected boolean IsLitActif;
        protected boolean IsLitMineur;
        protected Frottement Frot;
    }

    protected static class Etiquette {

        protected String Nom;
        protected PtProfil PointFF;
    }

    protected static class Frottement {

        protected String NomRef;
    }

    protected static class LitUtile {

        protected PtProfil LimDeb;
        protected PtProfil LimFin;
    }

    protected static class AbstractCasier implements ObjetNomme {

        @Override
        public String getId() {
            return Nom;
        }

        @Override
        public String getNom() {
            return Nom;
        }

        @Override
        public void setNom(final String newNom) {
            Nom = newNom;

        }

        protected String Nom;
    }

    protected static class ProfilCasier extends AbstractCasier {

        protected String Commentaire;
        protected double Longueur;
        protected EvolutionFF EvolutionFF;
        protected LitUtile LitUtile;
    }

    protected static class BatiCasier extends AbstractCasier {

        protected String Commentaire;
        protected double SplanBati;
        protected double ZBatiTotal;
    }

    // -------------- SECTIONS -------------------//
    protected static class SectionIdem implements ObjetNomme {

        protected String NomRef;
        protected double Dz;

        @Override
        public String getId() {
            return NomRef;
        }

        @Override
        public String getNom() {
            return NomRef;
        }

        @Override
        public void setNom(final String newNom) {
            NomRef = newNom;
        }
    }

    // -------------- BRANCHES -------------------//
    protected static class BrancheSaintVenant implements ObjetNomme {

        protected String NomRef;
        protected double CoefSinuo;

        @Override
        public String getId() {
            return NomRef;
        }

        @Override
        public String getNom() {
            return NomRef;
        }

        @Override
        public void setNom(final String newNom) {
            NomRef = newNom;
        }
    }
}
