/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores.old;

// WARN: l'ordre des champs est important car utilise par l'ecriture/lecture de ORES
public class DaoOrdResCasierOld {

  private boolean ddeQech;
  private boolean ddeSplan;
  private boolean ddeVol;
  private boolean ddeZ;

  public final boolean getDdeSplan() {
    return ddeSplan;
  }

  /**
   * @return the ddeQech
   */
  public boolean getDdeQech() {
    return ddeQech;
  }

  public final boolean getDdeVol() {
    return ddeVol;
  }

  /**
   * @param newDdeSplan
   */
  public final void setDdeSplan(boolean newDdeSplan) {
    ddeSplan = newDdeSplan;
  }

  /**
   * @param ddeQech the ddeQech to set
   */
  public void setDdeQech(boolean ddeQech) {
    this.ddeQech = ddeQech;
  }

  /**
   * @param newDdeVol
   */
  public final void setDdeVol(boolean newDdeVol) {
    ddeVol = newDdeVol;
  }

  @Override
  public String toString() {
    return "OrdResCasier [ddeQech=" + ddeQech + ", ddeSplan=" + ddeSplan + ", ddeVol=" + ddeVol + ", ddeZ=" + ddeZ + "]";
  }

  /**
   * @return the ddeZ
   */
  public boolean getDdeZ() {
    return ddeZ;
  }

  /**
   * @param ddeZ the ddeZ to set
   */
  public void setDdeZ(boolean ddeZ) {
    this.ddeZ = ddeZ;
  }
}
