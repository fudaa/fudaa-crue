/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.optg;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.common.CrueHelper;

/**
 * @author Adrien Hadoux
 */
public class CrueDaoStructureOPTG implements CrueDataDaoStructure {

  private final boolean ignoreSortie;

  /**
   * @param ignoreSortie
   */
  public CrueDaoStructureOPTG(final boolean ignoreSortie) {
    super();
    this.ignoreSortie = ignoreSortie;
  }

  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    xstream.alias("OPTG", CrueDaoOPTG.class);
    xstream.alias("RegleRebDeb", RegleRebDeb.class);
    xstream.alias("RegleProfPlat", RegleProfPlat.class);
    xstream.alias("ReglePenteRupture", ReglePenteRupture.class);
    xstream.alias("RegleDecal", RegleDecal.class);
    xstream.alias("RegleLargSeuil", RegleLargSeuil.class);
    xstream.alias("ReglePdxMax", ReglePdxMax.class);
    xstream.alias("RegleVarPdxMax", RegleVarPdxMax.class);
    xstream.alias("RegleTolNdZ", RegleTolNdZ.class);
    xstream.alias("ReglePenteMax", ReglePenteMax.class);
    xstream.alias("Planimetrage", Planimetrage.class);
    xstream.alias("PlanimetrageNbrPdzCst", DaoPlanimetrageNbrPdzCst.class);
    if (ignoreSortie) {
      xstream.omitField(CrueDaoOPTG.class, "Sorties");
    } else {
      CrueHelper.configureSorties(xstream, ctuluLog);
    }

  }

  public static class DaoPlanimetrageNbrPdzCst {
    String NbrPdz;
  }

  public static class Planimetrage {
    DaoPlanimetrageNbrPdzCst PlanimetrageNbrPdzCst;
  }

  public static abstract class RegleDAO {
    boolean IsActive;

  }

  public static abstract class RegleSeuilDetect extends RegleDAO {
    public abstract double getValue();

    public abstract void setValue(double v);
  }

  /**
   * pas de seuil associé.
   * 
   * @author Adrien Hadoux
   */
  public static class RegleRebDeb extends RegleDAO {}

  public static class RegleProfPlat extends RegleSeuilDetect {
    double Pm_ProfPlat;

    @Override
    public double getValue() {
      return Pm_ProfPlat;
    }

    @Override
    public void setValue(final double v) {

      Pm_ProfPlat = v;
    }

  }

  public static class ReglePenteRupture extends RegleSeuilDetect {
    double Pm_PenteRupture;

    @Override
    public double getValue() {
      return Pm_PenteRupture;
    }

    @Override
    public void setValue(final double v) {

      Pm_PenteRupture = v;
    }

  }

  public static class RegleDecal extends RegleSeuilDetect {
    double Pm_Decal;

    @Override
    public double getValue() {
      return Pm_Decal;
    }

    @Override
    public void setValue(final double v) {

      Pm_Decal = v;
    }

  }

  public static class RegleLargSeuil extends RegleSeuilDetect {
    double Pm_LargSeuil;

    @Override
    public double getValue() {
      return Pm_LargSeuil;
    }

    @Override
    public void setValue(final double v) {

      Pm_LargSeuil = v;
    }

  }

  public static class ReglePdxMax extends RegleSeuilDetect {
    double Pm_PdxMax;

    @Override
    public double getValue() {
      return Pm_PdxMax;
    }

    @Override
    public void setValue(final double v) {
      Pm_PdxMax = v;
    }

  }

  public static class RegleVarPdxMax extends RegleDAO {
    int Pm_VarPdxMax;

    public int getValue() {
      return Pm_VarPdxMax;
    }

    public void setValue(final int v) {
      Pm_VarPdxMax = v;
    }

  }

  public static class RegleTolNdZ extends RegleSeuilDetect {
    double Pm_TolNdZ;

    @Override
    public double getValue() {
      return Pm_TolNdZ;
    }

    @Override
    public void setValue(final double v) {
      Pm_TolNdZ = v;
    }

  }

  public static class ReglePenteMax extends RegleSeuilDetect {
    double Pm_PenteMax;

    @Override
    public double getValue() {
      return Pm_PenteMax;
    }

    @Override
    public void setValue(final double v) {
      Pm_PenteMax = v;
    }

  }

}
