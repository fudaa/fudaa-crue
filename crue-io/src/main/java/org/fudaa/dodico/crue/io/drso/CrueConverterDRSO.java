/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.drso;

import com.thoughtworks.xstream.converters.SingleValueConverter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.common.CrueHelper;
import org.fudaa.dodico.crue.io.common.EnumsConverter;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.BrancheAbstract;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.BrancheBarrageFilEau;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.BrancheBarrageGenerique;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.BrancheDefault;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.BrancheEnchainement;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.BrancheNiveauxAssocies;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.BrancheOrifice;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.BranchePdc;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.BrancheSaintVenant;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.BrancheSeuilLateral;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.BrancheSeuilTransversal;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.BrancheStrickler;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.CasierAbstract;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.CasierMNT;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.CasierProfil;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.NdAm;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.NdAv;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.NoeudNiveauContinu;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.ReferenceBati;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.ReferenceCasierProfil;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.ReferenceNoeud;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.ReferenceSectionProfil;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.SectionAbstract;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.SectionBranche;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.SectionBrancheSaintVenant;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.SectionRefIdem;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.SectionRefInterpolee;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.SectionRefProfil;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.SectionRefSansGeometrie;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.SectionReferenceeParIdem;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeo;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoBatiCasier;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilCasier;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheBarrageFilEau;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheBarrageGenerique;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheEnchainement;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheNiveauxAssocies;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheOrifice;
import org.fudaa.dodico.crue.metier.emh.EMHBranchePdc;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheSeuilLateral;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheSeuilTransversal;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheStrickler;
import org.fudaa.dodico.crue.metier.emh.EMHCasierMNT;
import org.fudaa.dodico.crue.metier.emh.EMHCasierProfil;
import org.fudaa.dodico.crue.metier.emh.EMHSectionIdem;
import org.fudaa.dodico.crue.metier.emh.EMHSectionInterpolee;
import org.fudaa.dodico.crue.metier.emh.EMHSectionProfil;
import org.fudaa.dodico.crue.metier.emh.EMHSectionSansGeometrie;
import org.fudaa.dodico.crue.metier.emh.EnumPosSection;
import org.fudaa.dodico.crue.metier.emh.RelationEMH;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.factory.EMHFactory;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

/**
 * Factory qui se charge de remplir les structures DAO du fichier DRSO avec les données métier et inversement.
 *
 * @author Adrien Hadoux
 */
public class CrueConverterDRSO implements CrueDataConverter<CrueDaoDRSO, CrueData> {

  private SingleValueConverter sectionPositionConverter;

  @Override
  public CrueData getConverterData(final CrueData in) {
    return in;
  }

  @Override
  public CrueData convertDaoToMetier(final CrueDaoDRSO dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
    sectionPositionConverter = EnumsConverter.createEnumConverter(EnumPosSection.class, ctuluLog);
    final CrueData res = dataLinked;

    // -- etape 1: on remplit les infso des noeuds --//
    convertDaoToMetierNoeud(res, dao.Noeuds, ctuluLog);
    // -- etape 2: les casiers --//
    convertDaoToMetierCasier(res, dao.Casiers, ctuluLog);
    // -- etape 3: les sections a partir des sections--//
    convertDaoToMetierSections(res, dao.Sections, ctuluLog);
    // -- etape 4: les branches et les sections de bransche crées lors du remplissage des sections --//
    convertDaoToMetierBranche(res, dao.Branches, ctuluLog);

    return res;
  }

  @Override
  public CrueDaoDRSO convertMetierToDao(final CrueData metier, final CtuluLog ctuluLog) {
    sectionPositionConverter = EnumsConverter.createEnumConverter(EnumPosSection.class, ctuluLog);
    final CrueDaoDRSO res = new CrueDaoDRSO();
    // -- etape 1: on remplit les infso des noeuds --//
    res.Noeuds = CrueConverterDRSO.convertMetierToDaoNoeud(metier.getNoeuds(), ctuluLog);

    // -- etape 2: on remplit les infso des casiers --//
    res.Casiers = CrueConverterDRSO.convertMetierToDaoCasier(metier.getCasiers(), ctuluLog);

    // -- etape 3: on remplit les infso des sections --//
    res.Sections = CrueConverterDRSO.convertMetierToDaoSections(metier.getSections(), ctuluLog);

    // -- etape 4: on remplit les infso des branches --//
    res.Branches = convertMetierToDaoBranche(metier.getBranches(), ctuluLog);
    return res;
  }

  /**
   * Methode qui remplit une arrayList d'objets persistants qui constituent la premiere partie du fichier DRSO: les Noeuds.
   *
   * @param data
   * @return
   */
  private static List<NoeudNiveauContinu> convertMetierToDaoNoeud(final List<CatEMHNoeud> data,
          final CtuluLog analyser) {
    final List<NoeudNiveauContinu> listePersistante = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(data)) {
      for (final EMH emh : data) {
        if (emh instanceof CatEMHNoeud) {
          final CatEMHNoeud noeud = (CatEMHNoeud) emh;
          final NoeudNiveauContinu noeudPersist = new NoeudNiveauContinu();
          noeudPersist.Nom = noeud.getNom();
          noeudPersist.Commentaire = noeud.getCommentaire();
          listePersistante.add(noeudPersist);
        }
      }
    }
    return listePersistante;
  }

  /**
   * Methode qui remplit une arrayList d'objets métier EMH noeuds a partir des données persistantes qui constituent la premiere partie du fichier
   * DRSO: les Noeuds.
   *
   * @param data
   */
  private static void convertDaoToMetierNoeud(final CrueData res, final List listePersistants,
          final CtuluLog analyser) {

    if (!CollectionUtils.isEmpty(listePersistants)) {
      for (final Object persist : listePersistants) {
        if (persist instanceof NoeudNiveauContinu) {
          final NoeudNiveauContinu noeudPersist = (NoeudNiveauContinu) persist;
          final CatEMHNoeud noeud = res.createNode(noeudPersist.Nom);// new EMHNoeudNiveauContinu(noeudPersist.Nom);
          noeud.setCommentaire(noeudPersist.Commentaire);
          res.add(noeud);
        }
      }
    }
  }

  /**
   * Methode qui remplit une arrayList d'objets persistants qui constituent la deuxieme partie du fichier DRSO: les Branches.
   *
   * @param data
   * @return
   */
  private List<BrancheAbstract> convertMetierToDaoBranche(final List<CatEMHBranche> data, final CtuluLog analyser) {

    if (CollectionUtils.isEmpty(data)) {
      return new ArrayList<>();// ne pas renvoyer une empty list pour
      // xstream
    }

    final List<BrancheAbstract> listePersistante = new ArrayList<>();
    for (final CatEMHBranche emh : data) {
      boolean isSaintVenant = false;
      final CatEMHBranche branche = emh;
      BrancheAbstract branchePersist = null;
      if (branche instanceof EMHBrancheBarrageFilEau) {
        final EMHBrancheBarrageFilEau new_name = (EMHBrancheBarrageFilEau) branche;
        branchePersist = new BrancheBarrageFilEau();

        if (new_name.getSectionPilote() != null) {
          ((BrancheBarrageFilEau) branchePersist).SectionPilote = new ReferenceCasierProfil();
          ((BrancheBarrageFilEau) branchePersist).SectionPilote.NomRef = new_name.getSectionPilote().getNom();
        }

      } else if (branche instanceof EMHBrancheBarrageGenerique) {
        final EMHBrancheBarrageGenerique new_name = (EMHBrancheBarrageGenerique) branche;
        branchePersist = new BrancheBarrageGenerique();
        if (new_name.getSectionPilote() != null) {
          ((BrancheBarrageGenerique) branchePersist).SectionPilote = new ReferenceCasierProfil();
          ((BrancheBarrageGenerique) branchePersist).SectionPilote.NomRef = new_name.getSectionPilote().getNom();
        }
      } else if (branche instanceof EMHBrancheEnchainement) {
        branchePersist = new BrancheEnchainement();
      } else if (branche instanceof EMHBrancheNiveauxAssocies) {
        branchePersist = new BrancheNiveauxAssocies();
      } else if (branche instanceof EMHBrancheOrifice) {
        branchePersist = new BrancheOrifice();
      } else if (branche instanceof EMHBranchePdc) {
        branchePersist = new BranchePdc();
      } else if (branche instanceof EMHBrancheSaintVenant) {
        isSaintVenant = true;
        branchePersist = new BrancheSaintVenant();
      } else if (branche instanceof EMHBrancheSeuilLateral) {
        branchePersist = new BrancheSeuilLateral();
      } else if (branche instanceof EMHBrancheSeuilTransversal) {
        branchePersist = new BrancheSeuilTransversal();
      } else if (branche instanceof EMHBrancheStrickler) {
        branchePersist = new BrancheStrickler();
      }

      if (branchePersist != null) {
        // -- commun --//
        final BrancheAbstract br = branchePersist;
        br.IsActive = branche.getUserActive();
        br.Commentaire = branche.getCommentaire();
        br.Nom = branche.getNom();
        if (branche.getNoeudAmont() != null) {
          br.NdAm = new NdAm(branche.getNoeudAmont().getNom());
        }
        if (branche.getNoeudAval() != null) {
          br.NdAv = new NdAv(branche.getNoeudAval().getNom());
        }
        // -- on remplit les sections persistantes avec celle de la branche --//
        br
                .setSections(remplirPersistanceAvecSectionsBrancheDRSO(isSaintVenant, branche.getListeSections(),
                analyser));
        listePersistante.add(br);
      }
    }
    return listePersistante;

  }

  private void convertDaoToMetierBranche(final CrueData res, final List listePersistante, final CtuluLog analyser) {

    if (listePersistante == null || listePersistante.isEmpty()) {
      return;
    }
    for (final Object objet : listePersistante) {
      if (objet instanceof BrancheAbstract) {
        boolean isSaintVenant = false;
        CatEMHBranche branche = null;
        final BrancheAbstract branchePersist = (BrancheAbstract) objet;
        final String nom = branchePersist.Nom;
        if (branchePersist instanceof BrancheBarrageFilEau) {
          branche = new EMHBrancheBarrageFilEau(nom);

          final BrancheBarrageFilEau br = (BrancheBarrageFilEau) branchePersist;
          if (br.SectionPilote != null && br.SectionPilote.NomRef != null) {

            final CatEMHSection sectionPilote = res.findSectionByReference(br.SectionPilote.NomRef);
            if (sectionPilote != null) {
              ((EMHBrancheBarrageFilEau) branche).setSectionPilote(sectionPilote);
            } else {
              CrueHelper.unknowReference("Branche-Section", br.SectionPilote.NomRef, analyser);
            }
          }

        } else if (branchePersist instanceof BrancheBarrageGenerique) {
          branche = new EMHBrancheBarrageGenerique(nom);

          final BrancheBarrageGenerique br = (BrancheBarrageGenerique) branchePersist;
          if (br.SectionPilote != null && br.SectionPilote.NomRef != null) {

            final CatEMHSection sectionPilote = res.findSectionByReference(br.SectionPilote.NomRef);
            if (sectionPilote != null) {
              ((EMHBrancheBarrageGenerique) branche).setSectionPilote(sectionPilote);
            } else {
              CrueHelper.unknowReference("Branche-Section", br.SectionPilote.NomRef, analyser);
            }
          }


        } else if (branchePersist instanceof BrancheEnchainement) {
          branche = new EMHBrancheEnchainement(nom);

        } else if (branchePersist instanceof BrancheNiveauxAssocies) {
          branche = new EMHBrancheNiveauxAssocies(nom);

        } else if (branchePersist instanceof BrancheOrifice) {
          branche = new EMHBrancheOrifice(nom);

        } else if (branchePersist instanceof BranchePdc) {
          branche = new EMHBranchePdc(nom);

        } else if (branchePersist instanceof BrancheSaintVenant) {
          branche = new EMHBrancheSaintVenant(nom);
          isSaintVenant = true;


        } else if (branchePersist instanceof BrancheSeuilLateral) {
          branche = new EMHBrancheSeuilLateral(nom);

        } else if (branchePersist instanceof BrancheSeuilTransversal) {
          branche = new EMHBrancheSeuilTransversal(nom);

        } else if (branchePersist instanceof BrancheStrickler) {
          branche = new EMHBrancheStrickler(nom);

        }

        if (branche != null) {
          // -- commun --//

          branche.setUserActive(branchePersist.IsActive);
          branche.setCommentaire(branchePersist.Commentaire);
          if (branchePersist.NdAm != null) {
            final CatEMHNoeud noeudAmont = res.findNoeudByReference(branchePersist.NdAm.NomRef);
            if (noeudAmont != null) {
              branche.setNoeudAmont(noeudAmont);
            } else {
              analyser.addSevereError("io.drso.branches.noeud.ref.amont.error", branchePersist.NdAm.NomRef,
                      branchePersist.Nom);
            }
          } else {
            analyser.addSevereError("io.drso.branches.no.noeud.amont.error", branche.getNom());
          }
          if (branchePersist.NdAv != null) {
            final CatEMHNoeud noeudAval = res.findNoeudByReference(branchePersist.NdAv.NomRef);
            if (noeudAval != null) {
              branche.setNoeudAval(noeudAval);
            } else {
              analyser.addSevereError("io.drso.branches.noeud.ref.aval.error", branchePersist.NdAv.NomRef,
                      branchePersist.Nom);
            }
          } else {
            analyser.addSevereError("io.drso.branches.no.noeud.aval.error", branche.getNom());
          }

          // -- on remplit les sections persistantes avec celle de la branche --//
          if (isSaintVenant) {
            final BrancheSaintVenant brancheSaintVenant = (BrancheSaintVenant) branchePersist;
            if (brancheSaintVenant.SectionsBrancheSaintVenant != null) {
              branche.addListeSections(remplirSectionsBrancheAvecPersistanceDRSO(branche, res, isSaintVenant,
                      brancheSaintVenant.SectionsBrancheSaintVenant, analyser));
            }
          } else if (((BrancheDefault) branchePersist).Sections != null) {
            branche.addListeSections(remplirSectionsBrancheAvecPersistanceDRSO(branche, res, isSaintVenant,
                    ((BrancheDefault) branchePersist).Sections, analyser));
          }

          res.add(branche);
        }
      }
    }
  }

  /**
   * Methode qui remplit une arrayList d'objets persistants qui constituent la troisiéme partie du fichier DRSO: les Casier.
   *
   * @param data
   */
  private static List<CasierAbstract> convertMetierToDaoCasier(final List<CatEMHCasier> data,
          final CtuluLog analyser) {
    final List<CasierAbstract> listePersistante = new ArrayList<>();
    for (final EMH emh : data) {
      if (emh instanceof CatEMHCasier) {
        final CatEMHCasier casier = (CatEMHCasier) emh;
        CasierAbstract casierPersist = null;
        if (casier instanceof EMHCasierMNT) {
          casierPersist = new CasierMNT();
        } else if (casier instanceof EMHCasierProfil) {
          casierPersist = new CasierProfil();
          final EMHCasierProfil casierProfil = (EMHCasierProfil) casier;
          final List<DonPrtGeo> dptg = casierProfil.getDPTG();
          final CasierProfil profil = (CasierProfil) casierPersist;
          for (final DonPrtGeo donPrtGeo : dptg) {
            if (donPrtGeo instanceof DonPrtGeoProfilCasier) {
              if (profil.ProfilCasiers == null) {
                profil.ProfilCasiers = new ArrayList<>();
              }
              final ReferenceCasierProfil profilPersist = new ReferenceCasierProfil();
              profilPersist.NomRef = ((DonPrtGeoProfilCasier) donPrtGeo).getNom();
              profil.ProfilCasiers.add(profilPersist);
            } else if (donPrtGeo instanceof DonPrtGeoBatiCasier) {
              profil.BatiCasier = new ReferenceBati();
              profil.BatiCasier.NomRef = ((DonPrtGeoBatiCasier) donPrtGeo).getNom();
            }
          }
        }
        // -- commun --//
        if (casierPersist != null) {
          casierPersist.IsActive = casier.getUserActive();
          casierPersist.Commentaire = casier.getCommentaire();
          if (casier.getNoeud() != null) {
            casierPersist.Noeud = new ReferenceNoeud(casier.getNoeud().getNom());
          }
          casierPersist.Nom = casier.getNom();
          listePersistante.add(casierPersist);
        }

      }
    }

    return listePersistante;
  }

  /**
   * Methode qui remplit une arrayList d'objets métier EMH casier a partir des données persistantes qui constituent a troisiéme partie du fichier
   * DRSO: les Casier.
   *
   * @param data
   * @param listePersistants
   * @param analyser
   * @return
   */
  private static void convertDaoToMetierCasier(final CrueData data, final List listePersistants,
          final CtuluLog analyser) {
    if (CollectionUtils.isNotEmpty(listePersistants)) {
      for (final Object persist : listePersistants) {
        if (persist instanceof CasierAbstract) {
          final CasierAbstract casierPersist = (CasierAbstract) persist;
          final String nom = casierPersist.Nom;
          // -- on déclare une version abstraite EMH métier du type --//
          CatEMHCasier casier = null;

          // -- on recherche sa spécialisation --//
          if (casierPersist instanceof CasierProfil) {
            casier = EMHFactory.createCasierProfil(nom, data.getCrueConfigMetier(), false);
            final CasierProfil casierProfilPersist = (CasierProfil) casierPersist;
            if (CollectionUtils.isNotEmpty(casierProfilPersist.ProfilCasiers)) {

              // -- on crée la liste des références des profils casier qui seront décrit dans le fichier DPTG --//
              for (final ReferenceCasierProfil refProfil : casierProfilPersist.ProfilCasiers) {
                data.registerUseCasierProfil(casier, refProfil.NomRef);
              }
              if (casierProfilPersist.BatiCasier != null) {
                data.registerUseBatiCasier(casier, casierProfilPersist.BatiCasier.NomRef);
              }
              // -- ajout des references dans l'objet EMH --//

            } else {
              analyser.addInfo("io.drso.casier.profil.ref.error", casierPersist.Nom);
            }
          } else if (casierPersist instanceof CasierMNT) {
            casier = new EMHCasierMNT(nom);
          }

          // -- commun aux objets --//
          if (casier != null) {
            casier.setUserActive(casierPersist.IsActive);
            casier.setCommentaire(casierPersist.Commentaire);
            if (casierPersist.Noeud != null) {
              final String reference = casierPersist.Noeud.NomRef;

              final CatEMHNoeud noeudRef = data.findNoeudByReference(reference);
              if (noeudRef != null) {
                casier.setNoeud(noeudRef);
              } else {
                analyser.addSevereError("io.drso.casier.noeud.ref.error", reference, casier.getNom());
              }
            } else {
              analyser.addSevereError("io.drso.casier.no.noeud.error", casier.getNom());
            }
            data.add(casier);
          }
        }
      }
    }
  }

  /**
   * Methode qui remplit une arrayList d'objets persistants qui constituent la quatriéme partie du fichier DRSO: les Sections-references.
   *
   * @param data
   * @return
   */
  private static List<SectionAbstract> convertMetierToDaoSections(final List<CatEMHSection> data,
          final CtuluLog analyser) {
    final List<SectionAbstract> listePersistante = new ArrayList<>();
    for (final EMH emh : data) {
      if (emh instanceof CatEMHSection) {
        final CatEMHSection section = (CatEMHSection) emh;
        SectionAbstract sectionPersist = null;
        if (section instanceof EMHSectionIdem) {
          sectionPersist = new SectionRefIdem();
          ((SectionRefIdem) sectionPersist).Section = new SectionReferenceeParIdem();
          ((SectionRefIdem) sectionPersist).Section.NomRef = ((EMHSectionIdem) section).getSectionRef().getNom();

        } else if (section instanceof EMHSectionInterpolee) {
          sectionPersist = new SectionRefInterpolee();
        } else if (section instanceof EMHSectionProfil) {
          sectionPersist = new SectionRefProfil();

          final EMHSectionProfil sectionProfil = (EMHSectionProfil) section;
          final List<DonPrtGeo> dptg = sectionProfil.getDPTG();
          for (final DonPrtGeo donPrtGeo : dptg) {
            if (donPrtGeo instanceof DonPrtGeoProfilSection) {
              if (((SectionRefProfil) sectionPersist).ProfilSections == null) {
                ((SectionRefProfil) sectionPersist).ProfilSections = new ArrayList<>();
              }
              final ReferenceSectionProfil profilPersist = new ReferenceSectionProfil();
              profilPersist.NomRef = ((DonPrtGeoProfilSection) donPrtGeo).getNom();
              ((SectionRefProfil) sectionPersist).ProfilSections.add(profilPersist);
            }
          }

        } else if (section instanceof EMHSectionSansGeometrie) {
          sectionPersist = new SectionRefSansGeometrie();
        }

        // -- commun --//
        if (sectionPersist != null) {
          sectionPersist.Nom = section.getNom();
          sectionPersist.Commentaire = section.getCommentaire();
          listePersistante.add(sectionPersist);
        }
      }
    }
    return listePersistante;
  }

  /**
   * Retourne la liste des sections associée a la branche. Ces sections sont récupérées a partir des données persistantes incluses dans une branches
   * par reference NomRef. Ces NomRef permettent de retrouver les Sections chargées précédemment qui constituent a troisieme partie du fichier DRSO:
   * les Sections.
   *
   * @param data
   * @return
   */
  private List<RelationEMH> remplirSectionsBrancheAvecPersistanceDRSO(final CatEMHBranche branche, final CrueData res,
          final boolean isSaintVenant, final List listePersistanteSections, final CtuluLog analyser) {
    final List<RelationEMH> listeRelationEMH = new ArrayList<>();
    if (CollectionUtils.isEmpty(listePersistanteSections)) {
    } else {
      for (final Object persist : listePersistanteSections) {
        if (persist instanceof SectionBranche) {
          final SectionBranche sectionPersist = (SectionBranche) persist;

          // -- on déclare une version abstraite EMH métier du type --//
          CatEMHSection section = null;
          final String reference = sectionPersist.NomRef;
          section = res.findSectionByReference(reference);

          // -- commun aux objets --//
          if (section != null) {
            final RelationEMHSectionDansBranche relation = EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(branche,
                    isSaintVenant, section, res.getCrueConfigMetier());
            relation.setXp(sectionPersist.Xp);
            relation.setPos((EnumPosSection) sectionPositionConverter.fromString(sectionPersist.Pos));
            if (isSaintVenant) {
              final RelationEMHSectionDansBrancheSaintVenant stVenant = (RelationEMHSectionDansBrancheSaintVenant) relation;
              stVenant.setCoefConv(((SectionBrancheSaintVenant) sectionPersist).CoefConv);
              stVenant.setCoefDiv(((SectionBrancheSaintVenant) sectionPersist).CoefDiv);
              stVenant.setCoefPond(((SectionBrancheSaintVenant) sectionPersist).CoefPond);
            }

            listeRelationEMH.add(relation);
          } else {

            CrueHelper.unknowReference("Sections", reference, analyser);
          }
        }
      }
    }
    return listeRelationEMH;
  }

  /**
   * Methode qui remplit une arrayList d'objets métier EMH sections
   *
   * @param data
   * @return
   */
  private static void convertDaoToMetierSections(final CrueData data, final List listePersistantsSection,
          final CtuluLog analyser) {

    if (CollectionUtils.isNotEmpty(listePersistantsSection)) {
      final List<CatEMHSection> listeEMH = new ArrayList<>(listePersistantsSection.size());
      final Map<EMHSectionIdem, String> sectionIdem = new HashMap<>(Math.max(10,
              listePersistantsSection.size() / 2 + 1));
      for (final Object persist : listePersistantsSection) {
        if (persist instanceof SectionAbstract) {
          final SectionAbstract sectionPersist = (SectionAbstract) persist;

          // -- on déclare une version abstraite EMH métier du type --//
          CatEMHSection section = null;
          final String nom = sectionPersist.Nom;
          // -- on recherche sa spécialisation --//
          if (sectionPersist instanceof SectionRefIdem) {
            section = new EMHSectionIdem(nom);

            final SectionRefIdem sectionref = (SectionRefIdem) sectionPersist;
            if (sectionref.Section != null) {
              sectionIdem.put((EMHSectionIdem) section, sectionref.Section.NomRef);
            } else {
              CrueHelper.unknowReference("Sections Idem", sectionPersist.Nom, analyser);
            }

          } else if (sectionPersist instanceof SectionRefInterpolee) {
            section = new EMHSectionInterpolee(nom);
          } else if (sectionPersist instanceof SectionRefProfil) {
            section = new EMHSectionProfil(nom);
            final SectionRefProfil sectionProfilPersist = (SectionRefProfil) sectionPersist;

            // -- recupere l'ensemble des profils --//
            if (sectionProfilPersist.ProfilSections != null && !sectionProfilPersist.ProfilSections.isEmpty()) {

              // -- on crée la liste des références des profils casier qui seront décrit dans le fichier DPTG --//
              final EMHSectionProfil sectionProfil = ((EMHSectionProfil) section);
              for (final ReferenceSectionProfil refProfil : sectionProfilPersist.ProfilSections) {
                data.registerUseSectionProfil(sectionProfil, refProfil.NomRef);
              }

            } else {
              CrueHelper.unknowReference("Sections Profils", sectionPersist.Nom, analyser);

            }

          } else if (sectionPersist instanceof SectionRefSansGeometrie) {
            section = new EMHSectionSansGeometrie(nom);
          }

          // -- commun aux objets --//
          if (section != null) {
            section.setCommentaire(sectionPersist.Commentaire);
            data.add(section);
            listeEMH.add(section);
          } else {
            CrueHelper.unknowEMH("Sections", nom, analyser);
          }
        }
      }
      if (!sectionIdem.isEmpty()) {
        final Map<String, CatEMHSection> refEMH = new HashMap<>(listeEMH.size());
        for (final CatEMHSection catEMHSection : listeEMH) {
          refEMH.put(catEMHSection.getNom(), catEMHSection);
        }
        for (final Entry<EMHSectionIdem, String> entry : sectionIdem.entrySet()) {
          final CatEMHSection refSection = refEMH.get(entry.getValue());
          if (refSection == null) {
            CrueHelper.unknowReference("EMHSectionIdem " + entry.getKey().getNom(), entry.getValue(), analyser);

          } else {
            entry.getKey().setSectionRef(refSection);
          }

        }
      }
    }
  }

  /**
   * Remplit les donénes persistantes avec la liste des sections d'une branche. Remplit toutes les données associées aux sections
   *
   * @param list
   * @return
   */
  private List<SectionBranche> remplirPersistanceAvecSectionsBrancheDRSO(final boolean isSaintVenant,
          final List<RelationEMHSectionDansBranche> list, final CtuluLog analyser) {
    final List<SectionBranche> listePersistante = new ArrayList<>();
    for (final RelationEMHSectionDansBranche relation : list) {
      final CatEMHSection section = relation.getEmh();
      SectionBranche sectionPersist = null;
      if (isSaintVenant) {
        sectionPersist = new SectionBrancheSaintVenant();
      } else if (section instanceof EMHSectionIdem) {
        sectionPersist = new SectionBranche();
      } else if (section instanceof EMHSectionInterpolee) {
        sectionPersist = new SectionBranche();
      } else if (section instanceof EMHSectionProfil) {
        sectionPersist = new SectionBranche();

      } else if (section instanceof EMHSectionSansGeometrie) {
        sectionPersist = new SectionBranche();
      }

      // -- commun --//
      if (sectionPersist != null) {
        sectionPersist.NomRef = section.getNom();

        sectionPersist.Pos = sectionPositionConverter.toString(EMHHelper.getPositionSection(relation));

        sectionPersist.Xp = relation.getXp();
        if (isSaintVenant) {
          final RelationEMHSectionDansBrancheSaintVenant stV = (RelationEMHSectionDansBrancheSaintVenant) relation;
          ((SectionBrancheSaintVenant) sectionPersist).CoefPond = stV.getCoefPond();
          ((SectionBrancheSaintVenant) sectionPersist).CoefConv = stV.getCoefConv();
          ((SectionBrancheSaintVenant) sectionPersist).CoefDiv = stV.getCoefDiv();
        }

        listePersistante.add(sectionPersist);
      }
    }
    return listePersistante;
  }
}
