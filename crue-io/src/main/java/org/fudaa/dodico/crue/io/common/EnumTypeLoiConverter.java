package org.fudaa.dodico.crue.io.common;

import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.config.loi.LoiTypeContainer;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by deniger on 20/06/2017.
 */
public final class EnumTypeLoiConverter extends AbstractSingleConverter {

    final LoiTypeContainer crueProps;

    private final static Logger LOGGER = Logger.getLogger(EnumTypeLoiConverter.class.getName());

    public EnumTypeLoiConverter(LoiTypeContainer crueProps) {
        super();
        assert crueProps != null;
        this.crueProps = crueProps;
    }

    @Override
    public boolean canConvert(final Class arg0) {
        return EnumTypeLoi.class.equals(arg0);
    }

    @Override
    public Object fromString(final String arg0) {
        final EnumTypeLoi valueOf = crueProps.getTypeLoi(arg0);
        if (valueOf == null) {
            LOGGER.log(Level.SEVERE, "EnumTypeLoiConverter can''t convert {0}", arg0);
        }
        return valueOf;
    }

    @Override
    public String toString(final Object arg0) {

        return ((EnumTypeLoi) arg0).getId();
    }
}
