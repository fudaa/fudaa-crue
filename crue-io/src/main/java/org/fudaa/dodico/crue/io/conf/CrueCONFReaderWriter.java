/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.conf;

import org.fudaa.dodico.crue.common.io.CrueXmlReaderWriterImpl;
import org.fudaa.dodico.crue.projet.conf.Configuration;

/**
 * @author CANEL Christophe
 *
 */
public class CrueCONFReaderWriter extends CrueXmlReaderWriterImpl<CrueDaoCONF, Configuration> {

  public CrueCONFReaderWriter(final String version) {
    super("conf", version, new CrueConverterCONF(), new CrueDaoStructureCONF());
  }
}
