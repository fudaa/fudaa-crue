/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.conf;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueConverter;

/**
 *
 * @author deniger
 */
public class CrueEtudeConverter implements CrueConverter<CrueEtudeDaoConfiguration, CrueEtudeDaoConfiguration> {

  @Override
  public CrueEtudeDaoConfiguration convertDaoToMetier(final CrueEtudeDaoConfiguration dao, final CtuluLog ctuluLog) {
    return dao;
  }

  @Override
  public CrueEtudeDaoConfiguration convertMetierToDao(final CrueEtudeDaoConfiguration metier, final CtuluLog ctuluLog) {
    return metier;
  }
}
