/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.conf;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueDaoStructure;
import org.fudaa.dodico.crue.common.io.CrueIOResu;

import java.io.File;

/**
 * @author deniger
 */
public class CrueAocConfigurationHelper {
    public static CrueIOResu<CrueAocDaoConfiguration> read(final File f, final CrueDaoStructure courbeConfigurer) {
        final CrueIOResu<CrueAocDaoConfiguration> res = new CrueIOResu<>();
        res.setAnalyse(new CtuluLog(BusinessMessages.RESOURCE_BUNDLE));
        final CrueAocConfigurationReaderWriter readerWriter = new CrueAocConfigurationReaderWriter("1.0", courbeConfigurer);
        final CrueIOResu<CrueAocDaoConfiguration> readXML = readerWriter.readXML(f, res.getAnalyse());
        if (readXML.getMetier() != null) {
            readXML.getMetier().updateXmlns();
        }
        return readXML;
    }

    public static CrueIOResu<CrueAocDaoConfiguration> read(final String f, final CrueDaoStructure courbeConfigurer) {
        final CrueIOResu<CrueAocDaoConfiguration> res = new CrueIOResu<>();
        res.setAnalyse(new CtuluLog(BusinessMessages.RESOURCE_BUNDLE));
        final CrueAocConfigurationReaderWriter readerWriter = new CrueAocConfigurationReaderWriter("1.0", courbeConfigurer);
        final CrueIOResu<CrueAocDaoConfiguration> readXML = readerWriter.readXML(f, res.getAnalyse());
        readXML.getMetier().updateXmlns();
        return readXML;
    }

    public static CrueIOResu<CrueAocDaoConfiguration> write(final File f, final CrueEtudeConfigurationHelper.InputData input, final CrueDaoStructure courbeConfigurer) {
        final CrueAocDaoConfiguration configEtude = new CrueAocDaoConfiguration();
        CrueEtudeConfigurationHelper.addDataInList(input.globalOptions, configEtude.getGlobalConfiguration().getOptions());
        configEtude.initLoiConfiguration(input.loiOptions);
        return write(f, configEtude, courbeConfigurer);
    }

    public static CrueIOResu<CrueAocDaoConfiguration> write(final File f, final CrueAocDaoConfiguration conf, final CrueDaoStructure courbeConfigurer) {
        final CrueIOResu<CrueAocDaoConfiguration> res = new CrueIOResu<>();
        res.setAnalyse(new CtuluLog(BusinessMessages.RESOURCE_BUNDLE));
        res.setMetier(conf);
        final CrueAocConfigurationReaderWriter readerWriter = new CrueAocConfigurationReaderWriter("1.0", courbeConfigurer);
        readerWriter.writeXMLMetier(res, f, res.getAnalyse());
        return res;
    }
}
