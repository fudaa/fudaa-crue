/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.io.neuf.STOSequentialReader.DonneesBranche;
import org.fudaa.dodico.crue.io.neuf.STOSequentialReader.DonneesCasier;
import org.fudaa.dodico.crue.io.neuf.STOSequentialReader.DonneesGenProfil;
import org.fudaa.dodico.crue.io.neuf.STOSequentialReader.DonneesNoeud;
import org.fudaa.dodico.crue.io.neuf.STOSequentialReader.DonneesSpecProf;
import org.fudaa.dodico.crue.io.neuf.STOSequentialReader.ParametresGeneraux;
import org.fudaa.dodico.crue.metier.Crue9TypeBranche;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.LoiFactory;

/**
 * Classe permettant de lire le fichier STO en totalité.
 *
 * @author CDE
 */
@SuppressWarnings("PMD.SystemPrintln")
public class STOReader extends AbstractCrueBinaryReader<STOSequentialReader> {

  private int nbRecProf;
  private int iBmax;
  private int nbPoin;
  private int iPrM;
  private int nbHaut;
  private final int nbCharTitreDcDh = 80;
  final int nbCharNomDcDh = 256;

  @Override
  protected STOSequentialReader internalReadResu() throws IOException {

    final STOSequentialReader data = new STOSequentialReader();

    // le premier entier doit etre 44.
    // cette premiere partie est un procede pour savoir si on est en little ou big endian.
    // on sait que le premier enregistrement doit etre 44; soit la longueur des 11 entiers
    final int firstInt = 44;
    // on lit le premier enregistrement qui fait 44
    helper.readAll(44 + 8);// 8 pour les 2 entiers avant/apres l'enregistrement
    final ByteBuffer bf = helper.getBuffer();
    int tempInt = bf.getInt();
    // Si le premier entier ne vaut pas 44, on inverse l'ordre de lecture
    if (tempInt != firstInt) {
      helper.inverseOrder();
    }
    tempInt = bf.getInt(0);
    // Si, après inversion de l'ordre, on n'a toujours pas le bon entier, c'est que le fichier n'est pas valide
    if (tempInt != firstInt) {
      analyze_.addSevereError("crue.io.sto.notValid");
      return null;
    }

    this.readParametresGeneraux(data);

    this.readDimensionsModele(data);

    this.readDonneesGenModele(data);

    this.readDonneesSpecProf(data);

    this.readDonneesBranches(data);

    this.readDonneesNoeuds(data);

    this.readDonneesGenProfils(data);

    return data;
  }

  /**
   * Lecture des paramètres généraux: a ce stade la lecture a deja ete faite, il reste a recuperer les valeurs du ByteBuffer.
   *
   * @param data
   * @throws IOException
   */
  @SuppressWarnings("PMD.SystemPrintln")
  private void readParametresGeneraux(final STOSequentialReader data) throws IOException {

    final STOSequentialReader.ParametresGeneraux donnesGen = new STOSequentialReader.ParametresGeneraux();

    final ByteBuffer readData = helper.getBuffer();

    donnesGen.setNpr(readData.getInt());

    donnesGen.setNbr(readData.getInt());

    donnesGen.setNtpr(readData.getInt());

    donnesGen.setNbpro(readData.getInt());

    nbHaut = readData.getInt();
    donnesGen.setNbhaut(nbHaut);

    donnesGen.setNbpmax(readData.getInt());

    donnesGen.setNbbram(readData.getInt());

    donnesGen.setNtsing(readData.getInt());

    donnesGen.setNbstr(readData.getInt());

    donnesGen.setNblitmax(readData.getInt());

    donnesGen.setNpo(readData.getInt());

    data.setParametresGeneraux(donnesGen);
  }

  /**
   * Lecture des dimensions du modèle
   *
   * @param data
   * @throws IOException
   */
  private void readDimensionsModele(final STOSequentialReader data) throws IOException {

    final STOSequentialReader.DimensionsModele dims = new STOSequentialReader.DimensionsModele();

    final ByteBuffer readData = helper.readData();

    iBmax = readData.getInt();
    dims.setIBmax(iBmax);

    nbPoin = readData.getInt();
    dims.setNbPoin(nbPoin);

    iPrM = readData.getInt();
    dims.setIPrM(iPrM);

    dims.setIPrFlM(readData.getInt());

    dims.setNSing(readData.getInt());

    dims.setLarBan(readData.getInt());

    dims.setLReclProf(readData.getInt());

    nbRecProf = readData.getInt();
    dims.setNbRecProf(nbRecProf);

    data.setDimensionsModele(dims);
  }

  /**
   * Lecture des données générales du modèle
   *
   * @param data
   * @throws IOException
   */
  private void readDonneesGenModele(final STOSequentialReader data) throws IOException {

    final STOSequentialReader.DonneesGeneralesModele donnees = new STOSequentialReader.DonneesGeneralesModele();

    final ByteBuffer buffer = helper.readData();

    donnees.setDal(buffer.getDouble());
    donnees.setG(buffer.getDouble());
    donnees.setTheta(buffer.getDouble());
    donnees.setZRef(buffer.getFloat());

    StringBuilder titreDcBuilder = new StringBuilder();
    for (int i = 0; i < 5; i++) {

      final String temp = helper.getStingFromBuffer(nbCharTitreDcDh);
      titreDcBuilder.append(temp);
    }
    String titreDc = titreDcBuilder.toString();
    donnees.setTitreDc(titreDc);

    donnees.setNomDc(helper.getStingFromBuffer(nbCharNomDcDh));

    data.setDonneesGenerales(donnees);
  }

  /**
   * Lecture des données profils servant d'index au fichier STR
   *
   * @param data
   * @throws IOException
   */
  private void readDonneesSpecProf(final STOSequentialReader data) throws IOException {

    final List<DonneesSpecProf> listeDonneesSpecProf = new ArrayList<>();

    for (int i = 0; i < nbRecProf; i++) {
      // dans ce cas il y a un write par profil
      helper.readData();

      final DonneesSpecProf donneesSpecProf = new DonneesSpecProf();

      donneesSpecProf.setNomProfil(helper.getStingFromBuffer(16));

      donneesSpecProf.setNomCasier(helper.getStingFromBuffer(16));

      listeDonneesSpecProf.add(donneesSpecProf);
    }

    data.setDonneesSpecProf(listeDonneesSpecProf);
  }

  /**
   * Lecture des données branches
   *
   * @param data
   * @throws IOException
   */
  private void readDonneesBranches(final STOSequentialReader data) throws IOException {

    final List<STOSequentialReader.DonneesBranche> donnees = new ArrayList<>();

    for (int i = 0; i < iBmax; i++) {

      // write(isto) nombr(ib),ntyp(ib),imax(ib),nupam(ib),nupav(ib),
      // + ruibra(ib),nbsing(ib),nsing(ib),n,
      // + Sinuo(ib),Alpha(ib)
      ByteBuffer readData = helper.readData();

      final DonneesBranche donneesBranche = new DonneesBranche();

      // Apparemment, erreur dans dictionnaire de données car doit être codé sur 16 caractères (au lieu de 8)
      donneesBranche.setNomBr(helper.getStingFromBuffer(16));
      donneesBranche.setNTyp(readData.getInt());

      final int imax = readData.getInt();
      donneesBranche.setIMax(imax);

      donneesBranche.setNuPam(readData.getInt());
      donneesBranche.setNuPav(readData.getInt());
      donneesBranche.setRuiBra(readData.getFloat());

      int nbSing = readData.getInt();
      // cas particulier pour les branches de type 6
      if (donneesBranche.getNTyp() == Crue9TypeBranche.STRICKLER) {
        nbSing = data.getParametresGeneraux().getNbhaut();
      }
      donneesBranche.setNbSing(nbSing);

      donneesBranche.setNSing(readData.getInt());

      final int n = readData.getInt();
      donneesBranche.setN(n);

      donneesBranche.setSinuo(readData.getFloat());
      donneesBranche.setAlpha(readData.getFloat());

      // write(isto) (n0pr(i,ib),i=1,imax(ib))
      readData = helper.readData();
      int[] indexAbsoluProfil = new int[imax];
      for (int j = 0; j < imax; j++) {
        // n0pr(NPR,NBR) i2 : n° absolu du profil dont le n° est ip dans la branche ib
        // pas sur que l'on en ait besoin
        indexAbsoluProfil[j] = readData.getShort();
      }
      donneesBranche.setN0Pr(indexAbsoluProfil);

      // if(n.gt.0) then
      // write(isto) (sing(i),i=nsing(ib)+1,nsing(ib+1))
      if (n > 0) {
        readData = helper.readData();

        final float[] sings = new float[n];
        for (int j = 0; j < n; j++) {
          sings[j] = readData.getFloat();
        }
        donneesBranche.setSing(sings);

      }

      donnees.add(donneesBranche);
    }

    data.setDonneesBranches(donnees);
  }

  /**
   * Lecture des données des nœuds
   *
   * @param data
   * @throws IOException
   */
  private void readDonneesNoeuds(final STOSequentialReader data) throws IOException {

    final List<STOSequentialReader.DonneesNoeud> donnees = new ArrayList<>();

    for (int i = 0; i < nbPoin; i++) {
      // write(isto) nompoi(ipoin),nucas(ipoin),nbbran(ipoin)
      ByteBuffer readData = helper.readData();

      final DonneesNoeud donneesNoeud = new DonneesNoeud();

      // Apparemment, erreur dans dictionnaire de données car doit être codé sur 16 caractères (au lieu de 8)
      donneesNoeud.setNomPoi(helper.getStingFromBuffer(16));

      final int nuCas = readData.getInt();
      donneesNoeud.setNuCas(nuCas);

      final int nbBran = readData.getInt();
      donneesNoeud.setNbBran(nbBran);

      // write(isto) (nubran(i,ipoin),i=1,nbbran(ipoin))
      readData = helper.readData();
      final int[] numerosBranches = new int[nbBran];
      for (int j = 0; j < nbBran; j++) {
        numerosBranches[j] = readData.getInt();
      }
      donneesNoeud.setNumerosBranches(numerosBranches);

      // Enregistrement si le noeud est un casier
      // if(nucas(ipoin).ne.0) then
      // write(isto) zfonca(ipoin),ruicas(ipoin), (sing(i),i= nucas(ip),nucas(ip)+11+2*NBHAUT)
      if (nuCas != 0) {
        readData = helper.readData();
        donneesNoeud.setZfonca(readData.getFloat());
        donneesNoeud.setRuicas(readData.getFloat());

        final STOSequentialReader.DonneesCasier casier = new STOSequentialReader.DonneesCasier();
        final int jmax = (11 + 2 * nbHaut);
        final float[] sings = new float[jmax];
        final float[] superfPlan = new float[nbHaut];
        final float[] surfMouill = new float[nbHaut];
        int cptNbHaut = 0;
        for (int j = 0; j < jmax; j++) {

          sings[j] = readData.getFloat();
          if (j == 0) {
          } else if (j == 1) {
            casier.setZFon(sings[j]);
          } else if (j == 2) {
            casier.setZHau(sings[j]);
          } else if (j == 3) {
            casier.setDZpr(sings[j]);
          } else if (j == 10) {
            casier.setDistTot(sings[j]);
          } else if (j >= 11 && j <= (10 + nbHaut) && cptNbHaut < nbHaut) {
            superfPlan[cptNbHaut] = sings[j];
            cptNbHaut++;
          } else if (j >= (10 + nbHaut + 1) && j <= (10 + 2 * nbHaut)) {
            // on reset pour ce j
            if (j == (10 + nbHaut + 1)) {
              cptNbHaut = 0;
            }
            surfMouill[cptNbHaut] = sings[j];
            cptNbHaut++;
          } else if (j == (10 + 2 * nbHaut + 1)) {
            casier.setUlmCas(sings[j]);
          }
        }
        casier.setSuperficiePlan(superfPlan);
        casier.setSurfaceMouillee(surfMouill);
        donneesNoeud.setCasier(casier);
      }

      donnees.add(donneesNoeud);
    }

    data.setDonneesNoeuds(donnees);
  }

  /**
   * Lecture des données générales des profils
   *
   * @param data
   * @throws IOException
   */
  private void readDonneesGenProfils(final STOSequentialReader data) throws IOException {

    final List<STOSequentialReader.DonneesGenProfil> donnees = new ArrayList<>();

    for (int i = 0; i < iPrM; i++) {
      // write(isto) tit(ip),nuprfl(ip), dist(ip),cdiv(ip),cconv(ip),cpond(ip)
      final ByteBuffer readData = helper.readData();

      final DonneesGenProfil donneesProfil = new DonneesGenProfil();

      // Apparemment, erreur dans dictionnaire de données car doit être codé sur 16 caractères (au lieu de 8)
      // donneesProfil.setTit(helper.getStingFromBuffer(8));
      donneesProfil.setTit(helper.getStingFromBuffer(16));
      donneesProfil.setNuPrFl(readData.getInt());
      donneesProfil.setDist(readData.getFloat());
      donneesProfil.setCDiv(readData.getFloat());
      donneesProfil.setCConv(readData.getFloat());
      donneesProfil.setCPond(readData.getFloat());

      donnees.add(donneesProfil);
    }

    data.setDonneesGenProfils(donnees);
  }

  /**
   * @param crueData
   * @param data
   */
  public static ResPrtReseauCrue9Adapter alimenteObjetsMetier(final STOSequentialReader data) {

    final ParametresGeneraux paramsGens = data.getParametresGeneraux();
    int nbHaut = 0;
    if (paramsGens != null) {
      nbHaut = paramsGens.getNbhaut();
    }

    // *** Casiers RPTG
    final Map<String, Crue9ResPrtGeoCasier> res = new HashMap<>();
    final List<DonneesNoeud> listeNoeuds = data.getDonneesNoeuds();
    if (listeNoeuds != null) {
      for (final DonneesNoeud noeud : listeNoeuds) {

        final DonneesCasier casier = noeud.getCasier();
        if (casier == null) {
          continue;
        }
        final Crue9ResPrtGeoCasier rptgCasier = new Crue9ResPrtGeoCasier();
        res.put(noeud.getNomPoi().trim(), rptgCasier);

        // Zf et Zhaut
        rptgCasier.setZf(casier.getZFon());
        rptgCasier.setZhaut(casier.getZHau());

        // Calcul des abscisses z
        final float dzpro = casier.getDZpr();
        final float zmin = casier.getZFon();
        final float[] abscissesZ = new float[nbHaut + 1];

        for (int i = 0; i < nbHaut + 1; i++) {
          abscissesZ[i] = zmin + i * dzpro;
        }

        final EnumTypeLoi loiZSplan = EnumTypeLoi.LoiZSplan;
        final float[] superficiePlan = casier.getSuperficiePlan();
        final float[] superficiePlanPlusUn = new float[superficiePlan.length + 1];
        System.arraycopy(superficiePlan, 0, superficiePlanPlusUn, 1, superficiePlan.length);//la premiere valeur est 0
        LoiFF loiFF = LoiFactory.getLoiFFRPTG(abscissesZ, superficiePlanPlusUn, CruePrefix.changePrefix(noeud
                .getNomPoi(), CruePrefix.P_NOEUD, loiZSplan), loiZSplan);
        if (loiFF != null) {
          rptgCasier.setLoiZSplan(loiFF);
        }

        final float[] ordonneesVol = new float[nbHaut + 1];
        final float[] sm = casier.getSurfaceMouillee();
        final float distTot = casier.getDistTot();
        for (int i = 0; i < nbHaut; i++) {//la premiere valeur est 0
          ordonneesVol[i + 1] = sm[i] * distTot;
        }
        final EnumTypeLoi loiZVol = EnumTypeLoi.LoiZVol;
        loiFF = LoiFactory.getLoiFFRPTG(abscissesZ, ordonneesVol, CruePrefix.changePrefix(noeud.getNomPoi(),
                CruePrefix.P_NOEUD, loiZVol), loiZVol);
        if (loiFF != null) {
          rptgCasier.setLoiZVol(loiFF);
        }

      }
    }
    return new ResPrtReseauCrue9Adapter(res);

  }
}
