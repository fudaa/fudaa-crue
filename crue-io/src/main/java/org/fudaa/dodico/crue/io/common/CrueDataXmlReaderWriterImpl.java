/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.common;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.ConversionException;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.thoughtworks.xstream.mapper.CannotResolveClassException;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.xml.UnicodeInputStream;
import org.fudaa.ctulu.xml.XmlVersionFinder;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.contrat.XmlFileConfig;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.io.CrueXmlReaderWriterImpl;
import org.fudaa.dodico.crue.common.io.CustomPrettyPrintWriter;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.loi.LoiTypeContainer;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * File format customisé pour Crue. Indique si le fichier est de type crue 9 ou 10. Contient un validator xsd pour le fichier donné. Contient une
 * méthode statique très pratique qui permet de retourner automatiquement le bon FileFormat en fonction du type renvoyé. T correspond à la structure
 * métier associée au format du fichier
 *
 * @param <D> Represente la structure DAO
 * @param <M> Represente le modele Metier
 * @author Adrien Hadoux
 */
public class CrueDataXmlReaderWriterImpl<D extends AbstractCrueDao, M> implements CrueXmlReaderWriter<M> {

  protected final CrueDataConverter<D, M> converter;
  private final CrueDataDaoStructure daoConfigurer;
  /**
   * La version du fichier
   */
  private final String version;
  /**
   * le nom du fichier xsd a utiliser
   */
  private final String xsdId;
  private final CrueFileType fileType;

  /**
   * @return the fileType
   */
  @Override
  public CrueFileType getFileType() {
    return fileType;
  }
  /**
   * La path complet du fichier xsd
   */
  private final String xsdPath;
  private final String xsdFile;

  /**
   * @param fileType
   * @param coeurConfig
   * @param converter
   * @param daoConfigurer
   */
  public CrueDataXmlReaderWriterImpl(final CrueFileType fileType, final XmlFileConfig coeurConfig,
          final CrueDataConverter<D, M> converter, final CrueDataDaoStructure daoConfigurer) {
    this.daoConfigurer = daoConfigurer;
    this.converter = converter;
    this.version = coeurConfig==null?"":coeurConfig.getXsdVersion();
    this.fileType = fileType;
    this.xsdId = fileType.toString();
    this.xsdFile = xsdId.toLowerCase() + "-" + version + ".xsd";
    xsdPath =  coeurConfig==null?"":coeurConfig.getXsdUrl(xsdFile);
  }

  public CrueDataConverter<D, M> getConverter() {
    return converter;
  }

  /**
   * Utilise par des lecteurs/ecrivain qui ne sont pas des fichiers xml de Crue10.
   *
   * @param fileType
   * @param version
   * @param converter
   * @param daoConfigurer
   */
  protected CrueDataXmlReaderWriterImpl(final String fileType, final String version, final CrueDataConverter<D, M> converter,
          final CrueDataDaoStructure daoConfigurer) {
    this.daoConfigurer = daoConfigurer;
    this.converter = converter;
    this.version = version;
    this.fileType = null;
    this.xsdId = fileType;
    this.xsdFile = xsdId.toLowerCase() + "-" + version + ".xsd";
    xsdPath = "/xsd/" + xsdFile;
  }

  public String getXsdFile() {
    return xsdFile;
  }

  /**
   * @return the xsdId
   */
  @Override
  public String getXsdId() {
    return xsdId;
  }

  protected final void configureXStream(final XStream xstream, final CtuluLog analyse, final LoiTypeContainer props) {
    daoConfigurer.configureXStream(xstream, analyse, props);
  }

  /**
   * @return the version
   */
  @Override
  public String getVersion() {
    return version;
  }

  /**
   * @return le path dans jar vers le fichier xsd correspondant
   */
  @Override
  public final String getXsdValidator() {
    return xsdPath;
  }

  protected XStream initXmlParser(final CtuluLog analyse, final LoiTypeContainer props) {
    final XmlFriendlyNameCoder replacer = createReplacer();
    final StaxDriver staxDriver = new StaxDriver(replacer);
    final XStream xstream = new XStream(staxDriver);
    CrueXmlReaderWriterImpl.initXstreamSecurity(xstream);
    xstream.setMode(XStream.NO_REFERENCES);
    // -- creation des alias pour que ce soit + parlant dans le xml file --//

    // -- alias pour les entete xsd --//

    xstream.aliasAttribute("xmlns:xsi", "xmlnsxsi");
    xstream.aliasAttribute("xsi:schemaLocation", "xsischemaLocation");

    xstream.useAttributeFor(AbstractCrueDao.class, "xmlns");
    xstream.useAttributeFor(AbstractCrueDao.class, "xmlnsxsi");
    xstream.useAttributeFor(AbstractCrueDao.class, "xsischemaLocation");

    configureXStream(xstream, analyse, props);
    return xstream;
  }

  private XmlFriendlyNameCoder createReplacer() {
    return new XmlFriendlyNameCoder("#", "_");
  }

  @Override
  public boolean isValide(final File xml, final CtuluLog res) {
    try {
      final boolean valide = isValide(xml.toURI().toURL(), res);
      res.setDesc("valid.xml");
      res.setDescriptionArgs(xml.getName());
      return valide;

    } catch (final MalformedURLException e) {
      res.manageException(e);
      LOGGER.log(Level.SEVERE, "isValide", e);
      return false;
    }
  }

  @Override
  public boolean isValide(final String xml, final CtuluLog res) {

    return isValide(getClass().getResource(xml), res);
  }

  @Override
  public boolean isValide(final URL xml, final CtuluLog res) {
    return CrueXmlReaderWriterImpl.isValide(xml, xsdPath, res);
  }

  /**
   * Lit les données dans le fichier f avec les données liées.
   *
   * @param dataLinked
   * @return
   */
  @Override
  public final CrueIOResu<M> read(final URL f, final CtuluLog analyzer, final CrueData dataLinked) {
    analyzer.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    analyzer.setDesc("read.file");
    analyzer.setDescriptionArgs(f.getFile());
    final D d = readDao(f, analyzer, dataLinked);
    if (d != null) {
      return createResu(d, converter.convertDaoToMetier(d, dataLinked, analyzer), analyzer);
    }
    return null;
  }

  protected CrueIOResu<M> createResu(final D d, final M m, final CtuluLog analyze) {
    final CrueIOResu<M> res = new CrueIOResu<>();
    res.setMetier(m);
    if (d != null) {
      res.setCrueCommentaire(d.getCommentaire());
    }
    res.setAnalyse(analyze);
    return res;
  }
  private final static Logger LOGGER = Logger.getLogger(CrueDataXmlReaderWriterImpl.class.getName());

  /**
   * @param fichier
   * @return
   */
  public D readDao(final File fichier, final CtuluLog analyser, final CrueData dataLinked) {
    InputStream in = null;
    D newData = null;
    try {
      in = new FileInputStream(fichier);
      newData = readDao(in, analyser, dataLinked);
    } catch (final FileNotFoundException e) {
      LOGGER.log(Level.FINE, "readDao", e);
      final String path = fichier == null ? "null" : fichier.getAbsolutePath();
      analyser.addSevereError("io.FileNotFoundException.error", path);
    } catch (final Exception e) {
      LOGGER.log(Level.FINE, "readDao", e);
      final String path = fichier == null ? "null" : fichier.getAbsolutePath();
      analyser.addSevereError("io.xml.error", path);
    } finally {
      CtuluLibFile.close(in);
    }
    return newData;

  }

  /**
   * @param in
   * @return le dao
   */
  @SuppressWarnings("unchecked")
  protected D readDao(final InputStream in, final CtuluLog analyser, final CrueData dataLinked) {
    analyser.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    D newData = null;
    InputStreamReader contentRead = null;
    try {
      final XStream parser = initXmlParser(analyser, dataLinked == null ? null : dataLinked.getCrueConfigMetier());

      final BufferedInputStream buffered = new BufferedInputStream(in, 8192 * 8);
      final UnicodeInputStream unicodeStream = new UnicodeInputStream(buffered, XmlVersionFinder.ENCODING);
      unicodeStream.init();
      contentRead = new InputStreamReader(unicodeStream, XmlVersionFinder.ENCODING);

      newData = (D) parser.fromXML(contentRead);// we not that is a D object.
    } catch (final ConversionException conversionException) {
      LOGGER.log(Level.SEVERE, "io.unknown.balise", conversionException);
      analyser.addSevereError("io.unknown.balise",
              StringUtils.substringAfterLast(conversionException.getShortMessage(), "."));

    } catch (final CannotResolveClassException cannotResolveException) {
      LOGGER.log(Level.FINE, "io.unknown.balise", cannotResolveException);
      analyser.addSevereError("io.unknown.balise", StringUtils.substringAfterLast(cannotResolveException.getMessage(), "."));

    } catch (final Exception e) {
      LOGGER.log(Level.SEVERE, "io.xml.error", e);
      analyser.addSevereError("io.xml.error", e.getMessage());
    } finally {
      CtuluLibFile.close(contentRead);
    }
    return newData;

  }

  /**
   * @param pathToResource l'adresse du fichier a charger commencant par /
   * @param analyser
   * @param dataLinked
   * @return
   */
  public D readDao(final String pathToResource, final CtuluLog analyser, final CrueData dataLinked) {
    return readDao(getClass().getResource(pathToResource), analyser, dataLinked);
  }

  /**
   * @param fichier
   * @return
   */
  public D readDao(final URL url, final CtuluLog analyser, final CrueData dataLinked) {
    if (url == null) {
      analyser.addSevereError("file.url.null.error");
      return null;
    }

    InputStream in = null;
    D newData = null;
    try {
      in = url.openStream();
      newData = readDao(in, analyser, dataLinked);
    } catch (final IOException e) {
      LOGGER.log(Level.FINE, e.getMessage(), e);
      analyser.addSevereError("io.xml.error", e.getMessage());
    } finally {
      CtuluLibFile.close(in);
    }
    return newData;

  }

  /**
   * Lit les données dans le fichier f avec les données liées.
   *
   * @param validation si vrai valide les données.
   * @return
   */
  public final CrueIOResu<M> readXML(final File f, final CtuluLog analyzer) {
    return readXML(f, analyzer, null);
  }

  /**
   * Lit les données dans le fichier f avec les données liées.
   *
   * @param dataLinked
   * @param validation si vrai valide les données.
   * @return
   */
  @Override
  public final CrueIOResu<M> readXML(final File f, final CtuluLog ctuluLog, final CrueData dataLinked) {
    ctuluLog.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    ctuluLog.setDesc("read.file");
    ctuluLog.setDescriptionArgs(f.getName());
    final D d = readDao(f, ctuluLog, dataLinked);
    if (d != null) {
      return createResu(d, converter.convertDaoToMetier(d, dataLinked, ctuluLog), ctuluLog);
    }
    return createResu(null, null, ctuluLog);

  }

  public final CrueIOResu<M> readXML(final InputStream f, final CtuluLog analyzer, final CrueData dataLinked) {
    analyzer.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    analyzer.setDesc("read.file");
//    analyzer.setDescriptionArgs(f.getName());
    final D d = readDao(f, analyzer, dataLinked);
    if (d != null) {
      return createResu(d, converter.convertDaoToMetier(d, dataLinked, analyzer), analyzer);
    }
    return createResu(null, null, analyzer);

  }

  public final CrueIOResu<M> readXML(final String pathToResource, final CtuluLog analyzer) {
    return readXML(pathToResource, analyzer, null);
  }

  /**
   * Lit les données dans le fichier f avec les données liées.
   *
   * @param dataLinked
   * @return
   */
  @Override
  public final CrueIOResu<M> readXML(final String pathToResource, final CtuluLog analyzer, final CrueData dataLinked) {
    analyzer.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    analyzer.setDesc("read.file");
    analyzer.setDescriptionArgs(pathToResource);
    final D d = readDao(pathToResource, analyzer, dataLinked);
    if (d != null) {
      return createResu(d, converter.convertDaoToMetier(d, dataLinked, analyzer), analyzer);
    }
    final CrueIOResu<M> res = new CrueIOResu<>();
    res.setAnalyse(analyzer);
    return res;

  }

  public boolean writeDAO(final File file, final D dao, final CtuluLog analyser, final CrueConfigMetier props) {
    FileOutputStream out = null;
    boolean ok = true;
    try {
      out = new FileOutputStream(file);
      out.write(UnicodeInputStream.getBOM(XmlVersionFinder.ENCODING));
      out.flush();
      ok = writeDAO(out, dao, analyser, props);
    } catch (final IOException e) {
      LOGGER.log(Level.SEVERE, "writeDAO " + file.getName(), e);
      ok = false;
    } finally {
      CtuluLibFile.close(out);
    }
    return ok;

  }

  /**
   * @param out le flux de sortie
   * @param dao le dao a persister
   * @param analyser le receveur d'information
   * @return
   */
  @SuppressWarnings("deprecation")
  public boolean writeDAO(final OutputStream out, final D dao, final CtuluLog analyser, final CrueConfigMetier props) {
    boolean isOk = true;
    try {
      final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, XmlVersionFinder.ENCODING), 8192 * 4);
      writer.write(XmlVersionFinder.ENTETE_XML + CtuluLibString.LINE_SEP);
      if (dao.getCommentaire() == null) {
        dao.setCommentaire(StringUtils.EMPTY);
      }
      dao.setXsdName(xsdFile);
      dao.updateXmlns();
      final XStream parser = initXmlParser(analyser, props);
      parser.marshal(dao, new CustomPrettyPrintWriter(writer, new char[]{' ', ' '},
              createReplacer()));

    } catch (final IOException e) {
      LOGGER.log(Level.SEVERE, "writeDAO", e);
      analyser.addSevereError("file.write.error");
      isOk = false;
    } finally {
      CtuluLibFile.close(out);
    }
    return isOk;
  }

  /**
   * MEthode qui permet d'ecrire les datas dans le fichier f specifie.
   *
   * @param data
   * @param f
   * @return
   */
  @Override
  public final boolean writeXML(final CrueIOResu<CrueData> metier, final File f, final CtuluLog ctuluLog) {
    ctuluLog.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    f.getParentFile().mkdirs();
    ctuluLog.setDesc("write.file");
    ctuluLog.setDescriptionArgs(f.getName());
    final D d = converter.convertMetierToDao(converter.getConverterData(metier.getMetier()), ctuluLog);

    if (d != null) {
      d.setXsdName(xsdFile);
      d.setCommentaire(metier.getCrueCommentaire());
      return writeDAO(f, d, ctuluLog, metier.getMetier().getCrueConfigMetier());
    }
    return false;
  }

  /**
   * @param metier l'objet metier
   * @param out le flux de sortie qui ne sera pas ferme
   * @param ctuluLog
   * @return true si reussite
   */
  @Override
  public boolean writeXML(final CrueIOResu<CrueData> metier, final OutputStream out, final CtuluLog ctuluLog) {
    ctuluLog.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    ctuluLog.setDesc("write.file");
    ctuluLog.setDescriptionArgs(xsdId);
    final D d = converter.convertMetierToDao(converter.getConverterData(metier.getMetier()), ctuluLog);
    if (d != null) {
      d.setCommentaire(metier.getCrueCommentaire());
      return writeDAO(out, d, ctuluLog, metier.getMetier().getCrueConfigMetier());
    }
    return false;
  }

  /**
   * Methode qui permet d'ecrire les datas dans le fichier f specifie.
   *
   * @param data les données
   * @param f
   * @return
   */
  @Override
  public final boolean writeXMLMetier(final CrueIOResu<M> metier, final File f, final CtuluLog analyzer,
          final CrueConfigMetier props) {
    f.getParentFile().mkdirs();
    analyzer.setDesc("write.file");
    analyzer.setDescriptionArgs(f.getName());
    analyzer.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    final D d = converter.convertMetierToDao(metier.getMetier(), analyzer);
    if (d != null) {
      if (StringUtils.isEmpty(d.getCommentaire())) {
        d.setCommentaire(metier.getCrueCommentaire());
      }
      d.setXsdName(xsdFile);
      return writeDAO(f, d, analyzer, props);
    }
    return false;
  }

  /**
   * @param metier l'objet metier
   * @param out le flux de sortie qui ne sera pas ferme
   * @param ctuluLog
   * @return true si reussite
   */
  @Override
  public boolean writeXMLMetier(final CrueIOResu<M> metier, final OutputStream out, final CtuluLog ctuluLog,
          final CrueConfigMetier props) {
    ctuluLog.setDesc("write.file");
    ctuluLog.setDesc(xsdId);
    ctuluLog.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    final D d = converter.convertMetierToDao(metier.getMetier(), ctuluLog);
    if (d != null) {
      d.setCommentaire(metier.getCrueCommentaire());
      return writeDAO(out, d, ctuluLog, props);
    }
    return false;
  }
}
