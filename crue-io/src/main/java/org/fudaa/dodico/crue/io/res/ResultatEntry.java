/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.res;

import java.io.File;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;

/**
 *
 * @author deniger
 */
public class ResultatEntry {

  File binFile;
  int offsetInBytes;
  final ResultatTimeKey resultatKey;

  public ResultatEntry(ResultatTimeKey resultatKey) {
    this.resultatKey = resultatKey;
  }

  public ResultatTimeKey getResultatKey() {
    return resultatKey;
  }

  public File getBinFile() {
    return binFile;
  }

  public void setBinFile(File binFile) {
    this.binFile = binFile;
  }

  public int getOffsetInBytes() {
    return offsetInBytes;
  }

  public void setOffsetInBytes(int offset) {
    this.offsetInBytes = offset;
  }
}
