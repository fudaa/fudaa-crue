/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;

import java.util.HashSet;
import java.util.Set;

/**
 * Dictionnaire des CARTES des fichiers Crue9. Y sont déclarés les variables de type d'élément, le nombre de champs....
 *
 * @author Adrien Hadoux
 */
public final class CrueIODico {
  private CrueIODico() {
  }

  // -- CARTES RESERVEES AUX TITRES --//
  protected static final String TITRE = "TITRE";
  protected static final int TITRE_MAX_CHAR = 256;
  // -- CARTES réservés aux règles --//
  protected static final String R_SLARGE = "R_SLARGE";
  protected static final String COEFF = "COEFF";
  protected static final String ZREF = "ZREF";
  protected static final String R_PRPLAT = "R_PRPLAT";
  protected static final String R_RUPENT = "R_RUPENT";
  protected static final String R_DECAL = "R_DECAL";
  protected static final String R_REBDEB = "R_REBDEB";
  protected static final String R_DXMAX = "R_DXMAX";
  protected static final String R_VDXMAX = "R_VDXMAX";
  protected static final String R_PENMAX = "R_PENMAX";
  protected static final String TOL_Z_INIT = "TOL_Z_INIT";
  // -- CARTES réservées aux NOEUDS --//
  protected static final String NOEUD = "NOEUD";
  // -- CARTES réservé aux branches --//
  /**
   * l'identifiant de la ligne de branche, premier mot en début de fichier *
   */
  protected static final String BRANCHE_TYPE = "BRANCHE";
  // -- pour les branches saint venant --///
  protected static final String BRANCHE_DISTANCE = "DISTANCE";
  protected static final String BRANCHE_PROF = "PROF";
  protected static final String BRANCHE_DISTMAX = "DISTMAX";
  protected static final String BRANCHE_CCONV = "CCONV";
  protected static final String BRANCHE_CPOND = "CPOND";
  protected static final String BRANCHE_CDIV = "CDIV";
  protected static final String BRANCHE_ALPHA = "ALPHA";
  protected static final String BRANCHE_RUIS = "RUIS";
  protected static final String BRANCHE_SINUO = "SINUO";
  // -- branche seuils --//
  protected static final String BRANCHE_BORDA = "BORDA";
  protected static final String BRANCHE_ZQ = "Z/Q";
  protected static final String BRANCHE_SEUIL = "SEUIL";
  // -- branche orifice --//
  protected static final String BRANCHE_CCTRMAX = "CCTRMAX";
  protected static final String BRANCHE_CLAPET = "CLAPET";
  // -- branche niveau associe --//
  protected static final String BRANCHE_QMIN_QMAX = "QMIN/QMAX";
  protected static final String BRANCHE_ZAMZAV = "ZAM/ZAV";
  // -- branche pdc et generaiques --//
  protected static final String BRANCHE_DZQ = "DZ/Q";
  // -- branche barrage fil eau poru les ref aux profils --//
  protected static final String BRANCHE_NOMREF = "NOMREF";
  // -- branche barrage fil eau --//
  protected static final String BRANCHE_PARAM = "PARAM";
  // -- donnees associées a DFRT --//
  protected static final String STRIREF = "STRIREF";
  protected static final String STRIREFZ = "STRIREFZ";
  protected static final String STRIREFH = "STRIREFH";
  // -- données associées aux profils --//
  // -- section de reference --//
  protected static final String PROFREF = "PROFREF";
  protected static final String XZ = "X/Z";
  // -- section rectangulaire et trapezoidale--//
  protected static final String PROFRECT = "PROFRECT";
  protected static final String DVERS = "DVERS";
  protected static final String DIGUE = "DIGUE";
  protected static final String FENTE = "FENTE";
  protected static final String LIT = "LIT";
  // -- section trapezoidale --//
  protected static final String PROFTRAP = "PROFTRAP";
  // -- section PROFIL --//
  protected static final String PROFIL = "PROFIL";
  protected static final String STRIC = "STRIC";
  protected static final String LIMITEJ = "LIMITEJ";
  protected static final String LIMITEX = "LIMITEX";
  protected static final String ACTIF = "ACTIF";
  // -- Profil interpolé --//
  protected static final String PROFINT = "PROFINT";
  // -- Profil identique idem a delta Z près. --//
  protected static final String PROFIDEM = "PROFIDEM";
  // -- casiers --//
  protected static final String CASIER_TYPE = "CASIER";
  protected static final String RUIS = "RUIS";
  protected static final String SBATI = "SBATI";
  protected static final String PROFCAS = "PROFCAS";
  // pour dh
  protected static final String DH_SEUIL_FROUDE = "SEUIL_FROUDE";
  protected static final String DH_NB_COURANT_MAX = "NB_COURANT_MAX";
  protected static final String DH_COEFF_RELAX_DZ = "COEFF_RELAX_DZ";
  protected static final String DH_COEFF_RELAX_DQ = "COEFF_RELAX_DQ";
  protected static final String DH_MODRU = "MODRU";
  protected static final Set<String> CARTE_DH = new HashSet<>();

  private static boolean isCrue9PlusInfini(final double val) {
    return val >= 99999 || CtuluLib.isEquals(99999, val);
  }

  private static boolean isCrue9MoinsInfini(final double val) {
    return val <= -99999 || CtuluLib.isEquals(-99999, val);
  }

  private static final String CRUE9_INFINI = "1E+30";
  private static final String CRUE9_MOINS_INFINI = "-1E+30";

  public static double getCrue10Value(final double v, final ItemVariable variable) {
    if (isCrue9PlusInfini(v)) {
      return variable.getNature().getTypeNumerique().getInfini();
    }
    if (isCrue9MoinsInfini(v)) {
      return -variable.getNature().getTypeNumerique().getInfini();
    }
    return v;
  }

  public static String getCrue9Value(final double v, final ItemVariable variable) {
    if (variable.isInfiniPositif(v)) {
      return CRUE9_INFINI;
    }
    if (variable.isInfiniNegatif(v)) {
      return CRUE9_MOINS_INFINI;
    }
    return Double.toString(v);
  }

  /**
   * Les cartes associées à la branche.
   */
  private static final Set<String> cartesBranche = new HashSet<>();

  static {
    CARTE_DH.add(DH_SEUIL_FROUDE);
    CARTE_DH.add(DH_NB_COURANT_MAX);
    CARTE_DH.add(DH_COEFF_RELAX_DZ);
    CARTE_DH.add(DH_COEFF_RELAX_DQ);
    CARTE_DH.add(TOL_Z_INIT);

    cartesBranche.add(BRANCHE_TYPE);
    cartesBranche.add("ZIMPO");
    cartesBranche.add("DZ/Q");
    cartesBranche.add(BRANCHE_DISTANCE);
    cartesBranche.add(BRANCHE_PROF);
    cartesBranche.add(BRANCHE_DISTMAX);
    cartesBranche.add(BRANCHE_CCONV);
    cartesBranche.add(BRANCHE_CPOND);
    cartesBranche.add(BRANCHE_CDIV);
    cartesBranche.add(BRANCHE_ALPHA);
    cartesBranche.add(BRANCHE_RUIS);
    cartesBranche.add(BRANCHE_SINUO);
    cartesBranche.add(BRANCHE_BORDA);
    cartesBranche.add(BRANCHE_ZQ);
    cartesBranche.add(BRANCHE_SEUIL);
    cartesBranche.add(BRANCHE_CCTRMAX);
    cartesBranche.add(BRANCHE_CLAPET);
    cartesBranche.add(BRANCHE_QMIN_QMAX);
    cartesBranche.add(BRANCHE_ZAMZAV);
    cartesBranche.add(BRANCHE_DZQ);
    cartesBranche.add(BRANCHE_NOMREF);
    cartesBranche.add(BRANCHE_PARAM);
  }

  private static final HashSet<String> cartesNoeuds = new HashSet<>();

  static {
    cartesNoeuds.add(NOEUD);
  }

  private static final HashSet<String> cartesRegles = new HashSet<>();

  static {
    cartesRegles.add(R_SLARGE);
    cartesRegles.add(COEFF);
    cartesRegles.add(ZREF);
    cartesRegles.add(R_PRPLAT);
    cartesRegles.add(R_RUPENT);
    cartesRegles.add(R_DECAL);
    cartesRegles.add(R_REBDEB);
    cartesRegles.add(R_DXMAX);
    cartesRegles.add(R_VDXMAX);
    cartesRegles.add(R_PENMAX);
  }

  /**
   * Les cartes associées à la branche.
   */
  private static final HashSet<String> cartesFrottements = new HashSet<>();

  static {
    cartesFrottements.add(STRIREF);
    cartesFrottements.add(STRIREFZ);
    cartesFrottements.add(STRIREFH);
  }

  /**
   * Les cartes associées aux profils.
   */
  private static final HashSet<String> cartesProfils = new HashSet<>();

  static {

    cartesProfils.add(PROFREF);
    cartesProfils.add(XZ);
    cartesProfils.add(PROFRECT);
    cartesProfils.add(DVERS);
    cartesProfils.add(DIGUE);
    cartesProfils.add(FENTE);
    cartesProfils.add(LIT);
    cartesProfils.add(PROFTRAP);
    cartesProfils.add(PROFIL);
    cartesProfils.add(STRIC);
    cartesProfils.add(LIMITEJ);
    cartesProfils.add(LIMITEX);
    cartesProfils.add(ACTIF);
    cartesProfils.add(PROFINT);
    cartesProfils.add(PROFIDEM);
  }

  private static final HashSet<String> cartesCasiers = new HashSet<>();

  static {

    cartesCasiers.add(CASIER_TYPE);
    cartesCasiers.add(RUIS);
    cartesCasiers.add(SBATI);
    cartesCasiers.add(PROFCAS);
    cartesCasiers.add(XZ);
    cartesCasiers.add(LIMITEJ);
    cartesCasiers.add(LIMITEX);
  }

  /**
   * Methode qui indique si l'élément de début de ligne correspond à une carte réservée aux branche.
   *
   * @param element a tester
   */
  protected static boolean appartientReglesCarte(final String element) {
    return cartesRegles.contains(element.toUpperCase());
  }

  /**
   * Methode qui indique si l'élément de début de ligne correspond à une carte réservée aux profils.
   *
   * @param element a tester
   */
  protected static boolean appartientProfilsCarte(final String element) {
    if (element == null) {
      return false;
    }
    final String elt = element.toUpperCase();
    boolean ok = false;
    for (final String carte : cartesProfils) {
      ok = ok || elt.contains(carte);
    }
    return ok;
  }

  /**
   * Methode qui indique si l'élément de début de ligne correspond à une carte réservée aux casiers.
   *
   * @param element a tester
   */
  protected static boolean appartientCasiersCarte(final String element) {
    if (element == null) {
      return false;
    }
    final String elt = element.toUpperCase();
    boolean ok = false;
    for (final String carte : cartesCasiers) {
      ok = ok || elt.contains(carte);
    }
    return ok;
  }

  /**
   * Verifie si la ligne correspond a des définition de type noeud.
   */
  protected static boolean appartientNoeudsCarte(final String element) {
    if (element == null) {
      return false;
    }
    final String elt = element.toUpperCase();
    boolean ok = false;
    for (final String carte : cartesNoeuds) {
      ok = ok || elt.contains(carte);
    }
    return ok;
  }

  /**
   * Methode qui indique si l'élément de début de ligne correspond à une carte réservée aux frottements.
   */
  protected static boolean appartientFrottementsCarte(final String element) {
    if (element == null) {
      return false;
    }
    final String elt = element.toUpperCase();
    boolean ok = false;
    for (final String carte : cartesFrottements) {
      ok = ok || elt.contains(carte);
    }
    return ok;
  }

  /**
   * @return true si on amorce la lecture d'une nouvelle casier.Condition d'arret.
   */
  protected static boolean isAnewCasierDefinition(final String ligne) {
    if (ligne == null) {
      return false;
    }
    return ligne.toUpperCase().contains(CrueIODico.CASIER_TYPE);
  }

  /**
   * @return true si on amorce al lecture d'une nouvelle branche.Condition d'arret.
   */
  protected static boolean isAnewBrancheDefinition(final String ligne) {
    if (ligne == null) {
      return false;
    }
    return ligne.toUpperCase().contains(CrueIODico.BRANCHE_TYPE);
  }

  protected static boolean isAnewProfilDefinition(final String ligne) {

    if (ligne == null) {
      return false;
    }
    return ligne.toUpperCase().contains(CrueIODico.PROFIL) || ligne.toUpperCase().contains(CrueIODico.PROFIDEM)
        || ligne.toUpperCase().contains(CrueIODico.PROFINT) || ligne.toUpperCase().contains(CrueIODico.PROFRECT)
        || ligne.toUpperCase().contains(CrueIODico.PROFREF);
  }

  /**
   * Methode qui indique si l'élément de début de ligne correspond à une carte réservée aux branche.
   */
  protected static boolean appartientBrancheCarte(final String element) {
    if (element == null) {
      return false;
    }
    final String elt = element.toUpperCase();
    for (final String carte : cartesBranche) {

      if (elt.contains(carte)) {
        return true;
      }
    }
    return false;
  }

  protected static final int BRANCHE_PDC = 1;
  protected static final int BRANCHE_SAINT_VENANT = 20;
  protected static final int BRANCHE_SEUIL_TRANSVERSAL = 2;
  protected static final int BRANCHE_SEUIL_LONGITUDINALE = 4;
  protected static final int BRANCHE_STRICKLER = 6;
  protected static final int BRANCHE_ORIFICE = 5;
  protected static final int BRANCHE_NIVEAU_ASSOCIE = 12;
  protected static final int BRANCHE_BARRAGE_GENERIQUE = 14;
  protected static final int BRANCHE_BARRAGE_FIL_EAU = 15;
  protected static final int BRANCHE_0_ABANDONNE = 0;
  protected static final int BRANCHE_9_ABANDONNE = 9;
  protected static final int BRANCHE_3_ABANDONNE = 3;
  protected static final int BRANCHE_7_ABANDONNE = 7;
  protected static final int BRANCHE_8_ABANDONNE = 8;
  protected static final int BRANCHE_10_ABANDONNE = 10;
  public static final String REGLE_INACTIVE = "NON";
}
