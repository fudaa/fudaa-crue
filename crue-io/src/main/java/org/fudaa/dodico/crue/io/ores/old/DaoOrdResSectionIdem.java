/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores.old;

/**
 * @author Frederic Deniger
 */
public class DaoOrdResSectionIdem extends DaoOrdResSectionOld {

  /**
   *
   */
  public DaoOrdResSectionIdem() {
  }

  /**
   * @param other to init from
   */
  public DaoOrdResSectionIdem(final DaoOrdResSectionOld other) {
    super(other);
  }
  
}
