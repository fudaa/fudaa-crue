/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dptg;

import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.dptg.CrueDaoStructureDPTG.BatiCasier;
import org.fudaa.dodico.crue.io.dptg.CrueDaoStructureDPTG.BrancheSaintVenant;
import org.fudaa.dodico.crue.io.dptg.CrueDaoStructureDPTG.ProfilCasier;
import org.fudaa.dodico.crue.io.dptg.CrueDaoStructureDPTG.ProfilSection;
import org.fudaa.dodico.crue.io.dptg.CrueDaoStructureDPTG.SectionIdem;

/**
 * Classe persistante qui reprend la meme structure que le fichier xml DPTG - Fichier des donnees de pretraitement
 * geometrique (xml) A persister telle qu'elle. WARNING: DEPEND DES FICHIERS DRSO ET DFRT (references aux donnees des 2
 * fichiers)
 * 
 * @author Adrien Hadoux
 */
@SuppressWarnings("PMD.VariableNamingConventions")
public class CrueDaoDPTG extends AbstractCrueDao {
  List<ProfilCasier> DonPrtGeoProfilCasiers;
  List<ProfilSection> DonPrtGeoProfilSections;
  List<BatiCasier> DonPrtGeoCasiers;
  List<SectionIdem> DonPrtGeoSections;
  List<BrancheSaintVenant> DonPrtGeoBranches;

}
