/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dcsp;

import com.thoughtworks.xstream.XStream;

import java.util.List;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.config.loi.LoiTypeContainer;
import org.fudaa.dodico.crue.io.common.AbstractDaoFloatLoi;
import org.fudaa.dodico.crue.io.common.AbstractDaoLoi;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.common.EnumsConverter;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EnumFormulePdc;
import org.fudaa.dodico.crue.metier.emh.EnumSensOrifice;

/**
 * @author ?
 */
public class CrueDaoStructureDCSP implements CrueDataDaoStructure {

    private final String version;

    /**
     * @param version
     */
    public CrueDaoStructureDCSP(final String version) {
        super();
        this.version = version;
    }

    @Override
    public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
        // -- creation des alias pour que ce soit + parlant dans le xml file --//
        xstream.alias(CrueFileType.DCSP.toString(), CrueDaoDCSP.class);
        // -- liste des initialisations nécessaires pour le formattage des donnees--//
        initXmlParserBranche(xstream, props, version);
        initXmlParserSeuils(xstream, version);

        xstream.alias("DonCalcSansPrtCasierProfil", DaoCasier.class);
        xstream.useAttributeFor(DaoCasier.class, "NomRef");
        // les enums:
        xstream.registerConverter(EnumsConverter.createEnumConverter(EnumFormulePdc.class, ctuluLog));
        if (Crue10VersionConfig.V_1_1_1.equals(version)) {
            xstream.aliasField("Sens", DaoElemSeuilOrifice.class, "SensOrifice");
        }
        xstream.registerConverter(EnumsConverter.createEnumConverter(EnumSensOrifice.class, ctuluLog));
    }

    /**
     * Init le parser avec les infos des branches.
     *
     * @param xstream
     */
    protected static void initXmlParserBranche(final XStream xstream, final LoiTypeContainer data, final String version) {
        // -- gestion des branches --//
        xstream.alias("DonCalcSansPrtBrancheSaintVenant", DaoBrancheSaintVenant.class);
        xstream.alias("DonCalcSansPrtBrancheSeuilLateral", DaoBrancheSeuilLateral.class);
        xstream.alias("DonCalcSansPrtBrancheStrickler", DaoBrancheStrickler.class);
        xstream.alias("DonCalcSansPrtBrancheOrifice", DaoBrancheOrifice.class);
        xstream.alias("DonCalcSansPrtBrancheSeuilTransversal", DaoBrancheSeuilTransversal.class);
        xstream.alias("DonCalcSansPrtBrancheBarrageFilEau", DaoBrancheBarrageFilEau.class);
        xstream.alias("DonCalcSansPrtBranchePdc", DaoBranchePdc.class);
        xstream.alias("DonCalcSansPrtBrancheBarrageGenerique", DaoBrancheBarrageGenerique.class);
        xstream.alias("DonCalcSansPrtBrancheNiveauxAssocies", DaoBrancheNiveauxAssocies.class);

        // -- mettre le nom de la branche dans la balise --//
        xstream.useAttributeFor(DaoBrancheAbstract.class, "NomRef");

        // -- gestion des lois --//
        xstream.alias("Pdc", DaoPdc.class);
        xstream.alias("Zasso", DaoZasso.class);
        xstream.alias("RegimeDenoye", DaoRegimeDenoye.class);
        xstream.alias("RegimeNoye", DaoRegimeNoye.class);
        AbstractDaoFloatLoi.configureXstream(xstream, data, version);
        xstream.omitField(AbstractDaoLoi.class, "DateZeroLoiDF");

    }

    protected static void initXmlParserSeuils(final XStream xstream, final String version) {
        xstream.alias("ElemSeuilAvecPdc", DaoElemAvecSeuilPdc.class);
        xstream.alias("ElemOrifice", DaoElemSeuilOrifice.class);
        if (Crue10VersionConfig.isLowerOrEqualsTo_V1_2(version)) {
            xstream.alias("RegimeDenoye", DaoRegimeManoeuvrant.class);
            xstream.aliasField("RegimeDenoye", DaoBrancheBarrageFilEauAbstract.class,"RegimeManoeuvrant");
            xstream.alias("ElemSeuil", DaoElemBarrage.class);
            xstream.aliasField("CoefD", DaoElemBarrage.class, "CoefNoy");
            xstream.omitField(DaoElemBarrage.class, "CoefDen");
        } else {
            xstream.alias("ElemBarrage", DaoElemBarrage.class);
            xstream.alias("RegimeManoeuvrant", DaoRegimeManoeuvrant.class);
        }
        xstream.addImplicitCollection(DaoBrancheBarrageFilEauAbstract.class, "ElemBarrages");
        xstream.addImplicitCollection(DaoBrancheSeuilAbstract.class, "ElemSeuils");


    }

    /**
     * Element branche possible
     *
     * @author Adrien Hadoux
     */
    protected static class DaoBrancheAbstract {
        protected String NomRef;
    }

    protected static class DaoBranchePDCAbstract extends DaoBrancheAbstract {
        protected DaoPdc Pdc;
    }

    protected static class DaoCasier {
        protected String NomRef;
        protected double CoefRuis;

    }

    protected static class DaoBrancheClassiqueAbstract extends DaoBrancheAbstract {

        protected double CoefBeta;
        protected double CoefRuisQdm;
        protected double CoefRuis;

    }

    protected static class DaoBrancheSeuilAbstract extends DaoBrancheAbstract {

        protected EnumFormulePdc FormulePdc;
        protected List<DaoElemAvecSeuilPdc> ElemSeuils;

    }

    protected static class DaoBrancheBarrageFilEauAbstract extends DaoBrancheAbstract {

        protected double QLimInf;
        protected double QLimSup;
        protected List<DaoElemBarrage> ElemBarrages;
        protected DaoRegimeManoeuvrant RegimeManoeuvrant;

    }

    protected static class DaoRegimeManoeuvrant extends AbstractDaoFloatLoi {
    }
    protected static class DaoRegimeDenoye extends AbstractDaoFloatLoi {
    }

    protected static class DaoRegimeNoye extends AbstractDaoFloatLoi {
    }

    protected static class DaoPdc extends AbstractDaoFloatLoi {
    }

    protected static class DaoZasso extends AbstractDaoFloatLoi {
    }

    protected static class DaoBrancheBarrageGeneriqueAbstract extends DaoBrancheAbstract {

        protected double QLimInf;
        protected double QLimSup;
        protected DaoRegimeNoye RegimeNoye;
        protected DaoRegimeDenoye RegimeDenoye;

    }


    protected static class DaoElemBarrage {
        protected double Largeur;
        protected double Zseuil;
        protected double CoefNoy;
        protected double CoefDen;

    }

    protected static class DaoElemAvecSeuilPdc {
        protected double Largeur;
        protected double Zseuil;
        protected double CoefD;
        protected double CoefPdc;
    }

    protected static class DaoElemSeuilOrifice {
        protected double CoefCtrLim;
        protected double Largeur;
        protected double Zseuil;
        protected double Haut;
        protected double CoefD;
        protected EnumSensOrifice SensOrifice;
    }

    protected static class DaoBrancheSaintVenant extends DaoBrancheClassiqueAbstract {
    }

    protected static class DaoBrancheSeuilLateral extends DaoBrancheSeuilAbstract {
    }

    protected static class DaoBrancheStrickler extends DaoBrancheClassiqueAbstract {
    }

    protected static class DaoBrancheOrifice extends DaoBrancheAbstract {
        protected DaoElemSeuilOrifice ElemOrifice;
    }

    protected static class DaoBrancheSeuilTransversal extends DaoBrancheSeuilAbstract {
    }

    protected static class DaoBrancheBarrageFilEau extends DaoBrancheBarrageFilEauAbstract {
    }

    protected static class DaoBranchePdc extends DaoBranchePDCAbstract {
    }

    protected static class DaoBrancheBarrageGenerique extends DaoBrancheBarrageGeneriqueAbstract {
    }

    protected static class BrancheEnchainement extends DaoBrancheClassiqueAbstract {
    }

    protected static class DaoBrancheNiveauxAssocies extends DaoBrancheAbstract {
        protected double QLimInf;
        protected double QLimSup;
        protected DaoZasso Zasso;
    }

}
