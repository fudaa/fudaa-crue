/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.pnum.CrueConverterPNUM;
import org.fudaa.dodico.crue.io.pnum.CrueDaoStructurePNUM;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.ParamNumModeleBase;

public class CrueFileFormatBuilderPNUM implements CrueFileFormatBuilder<ParamNumModeleBase> {

  @Override
  public Crue10FileFormat<ParamNumModeleBase> getFileFormat(final CoeurConfigContrat version) {
    
    return new Crue10FileFormat<>(new CrueDataXmlReaderWriterImpl<>(
        CrueFileType.PNUM, version, new CrueConverterPNUM(), new CrueDaoStructurePNUM()));

  }

}
