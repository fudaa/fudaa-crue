/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.pcal;

import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.pcal.CrueDaoStructurePCAL.PdtRes;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.OrdCalcTrans;
import org.fudaa.dodico.crue.metier.emh.ParamCalcScenario;
import org.fudaa.dodico.crue.metier.emh.PdtCst;
import org.fudaa.dodico.crue.metier.factory.EMHFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.joda.time.Duration;

/**
 * Classe qui se charge de remplir les structures DAO du fichier PCAL avec les donnees metier et inversement.
 *
 * @author CDE
 */
public class CrueConverterPCAL implements CrueDataConverter<CrueDaoPCAL, CrueData> {

  private final boolean v1_1_1;

  public CrueConverterPCAL(final boolean v1_1_1) {
    this.v1_1_1 = v1_1_1;
  }

  @Override
  public CrueData getConverterData(final CrueData in) {
    return in;
  }

  /**
   * Conversion des objets DAO en objets métier
   */
  @Override
  public CrueData convertDaoToMetier(final CrueDaoPCAL dao, final CrueData dataLinked,
          final CtuluLog ctuluLog) {
    final ParamCalcScenario metier = dataLinked.getOrCreatePCAL();


    if (dao.PdtRes != null && StringUtils.isNotBlank(dao.PdtRes.PdtCst)) {
      dataLinked.setOldPCALPdtRes(EMHFactory.createPdtCst(DateDurationConverter.getDuration(dao.PdtRes.PdtCst)));
    }

    if (StringUtils.isNotBlank(dao.DateDebSce)) {
      metier.setDateDebSce(DateDurationConverter.getDate(dao.DateDebSce));
    }

    if (StringUtils.isNotBlank(dao.DureeSce)) {
      dataLinked.setOldPCALDureeSce(DateDurationConverter.getDuration(dao.DureeSce));
    }
    return dataLinked;
  }

  /**
   * Conversion des objets métier en objets DAO
   */
  @Override
  public CrueDaoPCAL convertMetierToDao(final CrueData metier, final CtuluLog ctuluLog) {

    final CrueDaoPCAL dao = new CrueDaoPCAL();

    dao.PdtRes = new PdtRes();
    final ParamCalcScenario pcal = metier.getPCAL();

    dao.DateDebSce = StringUtils.EMPTY;
    if (pcal.getDateDebSce() != null) {
      dao.DateDebSce = DateDurationConverter.dateToXsd(pcal.getDateDebSce());
    }
    dao.DureeSce = StringUtils.EMPTY;
    if (v1_1_1 && metier.getOCAL() != null && metier.getOCAL().getOrdCalc() != null) {
      final List<OrdCalcTrans> collectCalcTrans = EMHHelper.selectInstanceOf(metier.getOCAL().getOrdCalc(), OrdCalcTrans.class);
      if (CollectionUtils.isNotEmpty(collectCalcTrans)) {
        final OrdCalcTrans ordCalcTrans = collectCalcTrans.get(0);
        if (ordCalcTrans.getPdtRes() != null) {
          final Duration objDuree = ((PdtCst) ordCalcTrans.getPdtRes()).getPdtCst();
          if (objDuree != null) {
            dao.PdtRes.PdtCst = DateDurationConverter.durationToXsd(objDuree);
          }
        }
        if (ordCalcTrans.getDureeCalc() != null) {
          dao.DureeSce = DateDurationConverter.durationToXsd(ordCalcTrans.getDureeCalc());
        }
      }


    }

    dao.Verbosite = "UN";

    return dao;
  }
}
