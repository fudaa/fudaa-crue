/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fudaa.dodico.crue.io.aide;

import java.util.List;

/**
 *
 * @author landrodie
 */
public class CrueDaoAideContents {
    
    private String commentaire;
    private List<CrueAide> lstAide;

    public CrueDaoAideContents(String commentaire, List<CrueAide> lstAide) {
        this.commentaire = commentaire;
        this.lstAide = lstAide;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public List<CrueAide> getLstAide() {
        return lstAide;
    }

    public void setLstAide(List<CrueAide> lstAide) {
        this.lstAide = lstAide;
    }
       
}
