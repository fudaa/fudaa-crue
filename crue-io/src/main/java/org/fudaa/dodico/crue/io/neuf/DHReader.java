/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import com.thoughtworks.xstream.converters.SingleValueConverter;
import gnu.trove.TIntObjectHashMap;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.fileformat.FortranLib;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.SeveriteManager;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.io.common.EnumsConverter;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.EMHFactory;
import org.fudaa.dodico.crue.metier.factory.InfoEMHFactory;
import org.fudaa.dodico.crue.metier.factory.LoiFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.dodico.fortran.FortranReader;
import org.joda.time.Duration;

import java.io.*;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Reader pour le format Dh crue 9. C'est la methode {@link #readAllDonnees(CrueIOResu)} qui fait le travail. Ensuite elle dispatche les taches entre {@link #readDonneesGenerales(CompositeReader, CrueData, int)},
 * {@link #readDonneesPermanentes(CompositeReader, CrueData, double)} et {@link #readDonneesTransitoires(CompositeReader, CrueData, double)}. Ce sont
 * ces 3 methodes qui doivent etre completees.
 *
 * @author Adrien Hadoux, F Deniger
 */
public class DHReader extends AbstractCrue9Reader {
  /**
   * La variable tolndz est sette lors de la lecture des regles. Mais elle est finalement utilise lorsque ile est les OrdPrtCIniModeleBase sont
   * definie ( variable ile)
   */
  private double tolndz;
  /**
   * cette set contient les id des emhs pour lesquelles les DPTI (DonPrtCIniBrancheOrifice par exemple) on déja été initialise. Dans Crue 9, ces dpti
   * sont initialisée avec la premier valeur recontree dans les etat permanent/transitoire
   */
  private final Set<String> emhsWithDPTIInitialized = new HashSet<>();
  /**
   * Cas particulier pour les sens d'ouverture pour les branche de type 5: il faut tester que le sens est constant car crue 9 ne supporte pas des
   * changements de sens.
   */
  private final Set<String> emhsWithSensOuvInitialized = new HashSet<>();
  final DecimalFormat calulFormat = new DecimalFormat();

  public DHReader() {
    calulFormat.setParseIntegerOnly(true);
    calulFormat.setMinimumIntegerDigits(2);
  }

  @Override
  public void stop() {
    // ne fait rien pour l'instant
  }

  /**
   * Le composite reader permet de lire les fichiers imbriqués. Le lecteur courant peut etre recupere via la methode {@link #getCurrent()}. Pour
   * obtenir une valeur à une colonne i, il suffit d'appeler les méthodes {@link #doubleField(int)} {@link #intField(int)} ou
   * {@link #stringField(int)}. Ce n'est pas la peine de s'occuper de la fermeture des flux: le tout sera fait par {@link DHReader#internalRead()}.
   *
   * @author deniger
   */
  protected class CompositeReader {
    private FortranReader current;
    private CompositeReader parent;

    /**
     * @param current le lecteur principal
     */
    public CompositeReader(final FortranReader current) {
      super();
      this.current = current;
    }

    /**
     * @param _i indice de la colonne
     * @return la valeur du reel en colonne i
     */
    public double doubleField(final int _i) {
      return current.doubleField(_i);
    }

    /**
     * @return le reader courant
     */
    public FortranReader getCurrent() {
      return current;
    }

    public int getLineNumber() {
      return current.getLineNumber();
    }

    public int getNumberOfFields() {
      return current.getNumberOfFields();
    }

    public int intField(final int _i) {
      return current.intField(_i);
    }

    /**
     * Methode principale qui permet de lire les fichiers inclus.
     *
     * @return la ligne lue
     */
    public String readLine() throws IOException {
      String res = null;
      try {
        res = readAndAvoidLabel(current);
        if ("FICH".equals(current.stringField(0))) { //$NON-NLS-1$
          final String file = current.stringField(1);
          // créé pour testé si le lien est en absolu ou non.
          final File testAbs = new File(file);
          // le lien peut etre un chemin absolu. Dans ce cas, on utilise l'URL de testAbs
          final URL newReaderUrl = testAbs.isAbsolute() ? testAbs.toURL() : new URL(folder + file);
          try {
            final InputStream openStream = newReaderUrl.openStream();
            final FortranReader newCurrent = new FortranReader(new InputStreamReader(openStream, getCrue9Charset()));
            initReader(newCurrent);
            readers.add(newCurrent);
            final CompositeReader newParent = new CompositeReader(current);
            newParent.parent = parent;
            parent = newParent;
            current = newCurrent;
            return readLine();
          } catch (final Exception e) {
            final String msg = "io.dh.compositeFile.error";
            analyze_.addSevereError(msg, e);
            logError(msg, this, e);
          }
        }
      } catch (final EOFException e) {
        if (parent == null) {
          throw e;
        } else {
          current.close();
          current = parent.current;
          parent = parent.parent;
          return readLine();
        }
      }

      return res;
    }

    public String stringField(final int _i) {
      return current.stringField(_i);
    }
  }

  /**
   * la chaine indiquant la fin d'un bloc.
   */
  private static final String FIN = "FIN";
  private final static Logger LOGGER = Logger.getLogger(DHReader.class.getName());
  /**
   * la chaine indiquant les blocs "variable en temps".
   */
  private static final String VARDT = "VARDT";
  /**
   * Le nombre de conditions limites définies. Si <0, la variable n'a pas été initialisée.
   */
  private int nbConlim = -1;
  /**
   * Les readers des fichiers inclus.
   */
  protected final Set<FortranReader> readers = new HashSet<>();
  private SingleValueConverter enumSensOuvMapCrue9;

  protected void initReader(final FortranReader in) {
    in.setCommentInOneField("*"); //$NON-NLS-1$
    in.setJumpCommentLine(true);
  }

  @Override
  protected CrueIOResu<CrueData> internalRead() {
    if (super.in_ == null) {
      analyze_.addSevereError("io.dh.isEmpty.error");//$NON-NLS-1$
      logError("io.dh.isEmpty.error", -1, null);//$NON-NLS-1$
      return null;
    }
    enumSensOuvMapCrue9 = EnumsConverter.createCrue9EnumConverter(EnumSensOuv.class, analyze_);
    final CrueIOResu<CrueData> res = new CrueIOResu<>();
    // on doit sortie: on a besoin des infos de dc:
    if (dataLinked == null) {
      analyze_.addSevereError("io.dh.crueDataNull.error");//$NON-NLS-1$
      logError("io.dh.crueDataNull.error", -1, null);//$NON-NLS-1$
      return null;
    }
    res.setMetier(dataLinked);
    try {
      readAllDonnees(res);
    } catch (final EOFException e) {
      if (isFine()) {
        LOGGER.log(Level.FINE, "{0} end of file", analyze_.getResource());
      }
    } catch (final Exception e) {
      analyze_.addSevereError("io.dh.error", in_.getLineNumber());
      logError(e.getMessage(), in_.getLineNumber(), e);
    }
    for (final FortranReader ins : readers) {
      FortranLib.close(ins);
    }

    return res;
  }

  private boolean isFine() {
    return LOGGER.isLoggable(Level.FINE);
  }

  private boolean isFiner() {
    return LOGGER.isLoggable(Level.FINER);
  }

  private void logError(final String msg, final CompositeReader reader) {
    logError(msg, reader, null);
  }

  private void logError(final String msg, final CompositeReader reader, final Exception e) {
    logError(msg, reader.getCurrent().getLineNumber(), e);
  }

  private void logError(final String msg, final int line, final Exception e) {
    LOGGER.log(Level.SEVERE, analyze_.getResource() + "; Ligne=" + line + ": " + msg, e);
  }

  private Duration readDuration(final CompositeReader reader) {
    return readDuration(reader, 0);
  }

  private Duration readDuration(final CompositeReader reader, final int startIdx) {
    int idx = startIdx;
    final Duration duration = DateDurationConverter.getDuration(reader.intField(idx++), reader.intField(idx++),
        reader.intField(idx++), reader.doubleField(idx++));
    if (isFiner()) {
      LOGGER.log(Level.FINER, "DATE: Ligne {0} read date {1}", new Object[]{reader.getCurrent().getLineNumber(), DateDurationConverter.durationToXsd(
          duration)});
    }
    return duration;
  }

  /**
   * @param reader le reader
   * @param res l'objet stockant les resultats
   * @param ile l'entier ile necessaire
   * @return false si erreur dans la lecture
   */
  private boolean readDonneesGenerales(final CompositeReader reader, final CrueData res, final int ile)
      throws IOException {
    // Ligne C : lecture des debits initiaux sur les branches
    // Ligne D : FIN
    reader.readLine();
    // true si les coeff de ruissellement sont défini par branches/casiers
    boolean modru = false;
    if (CrueIODico.DH_MODRU.equals(reader.stringField(0))) {
      modru = true;
      reader.readLine();
      dhContainsRuis = true;
    }

    final List<String> nomsBranches = new ArrayList<>();
    while (!FIN.equalsIgnoreCase(reader.stringField(0))) {

      final int nbFields = reader.getNumberOfFields();
      boolean readBrancheName = true;
      String brancheName = null;
      for (int i = 0; i < nbFields; i++) {
        if (readBrancheName) {
          brancheName = reader.stringField(i);
          nomsBranches.add(brancheName);
          readBrancheName = false;
        } else {
          final double debitInitial = reader.doubleField(i);
          if (isFine()) {
            LOGGER.log(Level.FINE, "le debit initial de la branche {0} est {1}", new Object[]{brancheName, debitInitial}); //$NON-NLS-1$ //$NON-NLS-2$
          }

          final CatEMHBranche br = res.findBrancheByReference(brancheName);
          if (br == null) {
            analyze_.addSevereError("io.dh.ref.emhbranche.debitini.error", brancheName);
          } else {
            addDptiInBranche(debitInitial, br, res);
          }

          readBrancheName = true;
        }
      }
      reader.readLine();
    }
    // on lit les coef de ruissellement
    if (modru) {
      reader.readLine();
      while (!FIN.equalsIgnoreCase(reader.stringField(0))) {

        final int nbFields = reader.getNumberOfFields();
        boolean readBrancheName = true;
        String brancheName = null;
        for (int i = 0; i < nbFields; i++) {
          if (readBrancheName) {
            brancheName = reader.stringField(i);
            readBrancheName = false;
          } else {
            final double coefRuiss = reader.doubleField(i);
            if (isFine()) {
              LOGGER.log(Level.FINE, "le coefRuiss de la branche {0} est {1}", new Object[]{brancheName, coefRuiss}); //$NON-NLS-1$ //$NON-NLS-2$
            }

            final CatEMHBranche br = res.findBrancheByReference(brancheName);
            if (br == null) {
              analyze_.addWarn("dh.ref.emhbranche.coefRuis.error", brancheName);
            } else if (!br.getClass().equals(EMHBrancheSaintVenant.class)) {
              analyze_.addWarn("dh.ref.coefRuis.definiPourBrancheNonSaintVenant", brancheName);
            } else {
              final List<DonCalcSansPrtBrancheSaintVenant> dcsps = EMHHelper.selectClass(br.getDCSP(),
                  DonCalcSansPrtBrancheSaintVenant.class);
              if (CollectionUtils.isEmpty(dcsps)) {
                final DonCalcSansPrtBrancheSaintVenant newInfosEMH = new DonCalcSansPrtBrancheSaintVenant(
                    dataLinked.getCrueConfigMetier());
                newInfosEMH.setCoefRuis(0);// d'apres la doc, il faut mettre 0: ruissellement.pdf
                br.addInfosEMH(newInfosEMH);
              } else {
                final int dcspsSize = dcsps.size();
                if (dcspsSize > 1) {
                  LOGGER.log(Level.SEVERE, "Trop de DonCalcSansPrtBrancheSaintVenant definit par {0}", br.getNom());
                }
                for (int j = 0; j < dcspsSize; j++) {
                  final DonCalcSansPrtBrancheSaintVenant dcsp = dcsps.get(j);
                  if (dcsp != null) {
                    final DonCalcSansPrtBrancheSaintVenant brancheSVDCSP = dcsp;
                    brancheSVDCSP.setCoefRuis(coefRuiss * brancheSVDCSP.getCoefRuis());
                  }
                }
              }
            }

            readBrancheName = true;
          }
        }
        reader.readLine();
      }
    }

    // lecture Ligne E : Niveaux d'eau initiaux des branches / sections
    if (ile == 0) {
      // On doit avoir autant de lignes e2 ou e3 que de branches.
      final int nbBranche = nomsBranches.size();
      String brancheName = null;
      for (int i = 0; i < nbBranche; i++) {
        reader.readLine();
        final int iligne = reader.intField(0);
        reader.readLine();

        brancheName = nomsBranches.get(i);
        final CatEMHBranche br = res.findBrancheByReference(brancheName);
        if (br == null) {
          analyze_.addSevereError("io.dh.ref.emhbranche.Zini.error", brancheName);
          return false;
        }

        final List<RelationEMHSectionDansBranche> sectionsBranche = br.getListeSections();
        if (CollectionUtils.isEmpty(sectionsBranche)) {
          analyze_.addSevereError("io.dh.no.sections.ligneE.error", brancheName);
          return false;
        }

        // Ligne E2
        if (iligne == 1) {
          final int nbDataToRead = sectionsBranche.size();
          int idxOnLine = 0;
          int maxOnLine = reader.getNumberOfFields();
          for (int j = 0; j < nbDataToRead; j++) {
            // données sur plusieurs lignes
            if (idxOnLine >= maxOnLine) {
              reader.readLine();
              maxOnLine = reader.getNumberOfFields();
              idxOnLine = 0;
            }
            final double niveauEauPourProfilJ = reader.doubleField(idxOnLine++);
            final DonPrtCIniSection section = new DonPrtCIniSection(res.getCrueConfigMetier());
            section.setZini(niveauEauPourProfilJ);
            sectionsBranche.get(j).getEmh().addInfosEMH(section);
          }
        } else if (iligne == 2) { // Ligne E3
          final double coefRempliPourBrancheI = reader.doubleField(0);
          if (isFine()) {
            LOGGER.log(Level.FINE, "ligne:{0}-> le niveau initial de la branche {1} est {2}",
                new Object[]{reader.getCurrent().getLineNumber(), brancheName, coefRempliPourBrancheI});
          }
          final int size = sectionsBranche.size();
          CatEMHSection sectionEMH = null;
          for (int j = 0; j < size; j++) {

            sectionEMH = sectionsBranche.get(j).getEmh();
            if (sectionEMH instanceof EMHSectionProfil) {
              final DonPrtCIniSection sectionDPTI = new DonPrtCIniSection(res.getCrueConfigMetier());
              sectionDPTI.setZini(calculeNiveauEauSection((EMHSectionProfil) sectionEMH, coefRempliPourBrancheI));
              sectionEMH.addInfosEMH(sectionDPTI);
            }
          }
        }
      }
      // reader.readLine();// lecture de la ligne FIN
      if (FIN.equalsIgnoreCase(reader.stringField(0))) {
        analyze_.addSevereError("io.dh.tagFINFound.error", reader.getCurrent() //$NON-NLS-1$
            .getLineNumber());
      }
    } else {
      reader.readLine();
      // Lecture lignes F1 et F2
      while (!FIN.equalsIgnoreCase(reader.stringField(0))) {

        final int nbFields = reader.getNumberOfFields();
        boolean readNoeudName = true;
        String noeudName = null;
        for (int i = 0; i < nbFields; i++) {
          if (readNoeudName) {
            noeudName = reader.stringField(i);
            readNoeudName = false;
          } else {
            final double niveauEau = reader.doubleField(i);
            if (isFine()) {
              LOGGER.log(Level.FINE, "ligne:{0}-> le niveau initial du noeud {1} est {2}",
                  new Object[]{reader.getCurrent().getLineNumber(), noeudName, niveauEau}); //$NON-NLS-1$ //$NON-NLS-2$
            }

            final CatEMHNoeud noeud = res.findNoeudByReference(noeudName);
            if (noeud == null) {
              analyze_.addSevereError("io.dh.ref.emhnoeud.Zini.error", noeudName);
            } else {

              final List<DonPrtCIniNoeudNiveauContinu> dptis = EMHHelper.selectClass(noeud.getInfosEMH(),
                  DonPrtCIniNoeudNiveauContinu.class);
              if (CollectionUtils.isEmpty(dptis)) {
                final DonPrtCIniNoeudNiveauContinu newInfosEMH = new DonPrtCIniNoeudNiveauContinu(
                    res.getCrueConfigMetier());
                newInfosEMH.setZini(niveauEau);
                noeud.addInfosEMH(newInfosEMH);
              } else {
                for (final DonPrtCIniNoeudNiveauContinu condInit : dptis) {
                  condInit.setZini(niveauEau);
                }
              }
            }

            readNoeudName = true;
          }
        }
        reader.readLine();
      }
    }
    // on lit pour les noeuds/casier
    if (modru) {
      reader.readLine();
      while (!FIN.equalsIgnoreCase(reader.stringField(0))) {

        final int nbFields = reader.getNumberOfFields();
        boolean readNoeudName = true;
        String noeudName = null;
        for (int i = 0; i < nbFields; i++) {
          if (readNoeudName) {
            noeudName = reader.stringField(i);
            readNoeudName = false;
          } else {
            final double coefRuis = reader.doubleField(i);
            if (isFine()) {
              LOGGER.log(Level.FINE, "ligne:{0}-> le coefficient de ruissellement {1} est {2}",
                  new Object[]{reader.getCurrent().getLineNumber(), noeudName, coefRuis}); //$NON-NLS-1$ //$NON-NLS-2$
            }

            final CatEMHNoeud noeud = res.findNoeudByReference(noeudName);
            if (noeud == null) {
              analyze_.addWarn("io.dh.ref.emhnoeud.coefRuis.error", noeudName);
            } else {
              final CatEMHCasier casier = noeud.getCasier();
              if (casier == null) {
                analyze_.addWarn("io.dh.ref.emhnoeud.coefRuis.noCasier.error", noeudName);
              } else {
                final DonCalcSansPrtCasierProfil condInit = InfoEMHFactory.getDCSPCasierProfil(casier,
                    res.getCrueConfigMetier());
                condInit.setCoefRuis(condInit.getCoefRuis() * coefRuis);
              }
            }

            readNoeudName = true;
          }
        }
        reader.readLine();
      }
    }
    return true;
  }

  private void addDptiInBranche(final double debitInitial, final CatEMHBranche br, final CrueData res) {
    List<DonPrtCIni> dptis = br.getDPTI();
    if (CollectionUtils.isEmpty(dptis)) {
      if (br instanceof EMHBrancheSaintVenant) {
        final DonPrtCIniBrancheSaintVenant condInit = new DonPrtCIniBrancheSaintVenant(
            res.getCrueConfigMetier());
        condInit.setQini(debitInitial);
        br.addInfosEMH(condInit);
      } else if (br instanceof EMHBrancheOrifice) {
        final DonPrtCIniBrancheOrifice condInit = new DonPrtCIniBrancheOrifice(res.getCrueConfigMetier());
        // initialisation par défaut: on enregistre pas dans emhsWithSensOuvInitialized
        condInit.setSensOuv(EnumSensOuv.OUV_VERS_BAS);
        condInit.setQini(debitInitial);
        br.addInfosEMH(condInit);
      } else {
        final DonPrtCIniBranche condInit = new DonPrtCIniBranche(res.getCrueConfigMetier());
        condInit.setQini(debitInitial);
        br.addInfosEMH(condInit);
      }
      dptis = br.getDPTI();
    } else {
      final int size = dptis.size();
      for (int j = 0; j < size; j++) {
        final DonPrtCIni dpti = dptis.get(j);
        if (dpti instanceof DonPrtCIniBranche) {
          final DonPrtCIniBranche condInit = (DonPrtCIniBranche) dpti;
          condInit.setQini(debitInitial);
        }
      }
    }
  }

  /**
   * On doit avoir Zini = Zf + coefRempliPourBrancheI*(ZmaxProfil - Zf). Pour connaître Zf et Zmax, il faut récupérer tous les points(x,y) de la
   * section dans DPTG et détermniner le ymin et ymax entre tous ces points qui correspondront respectivement à Zf et Zmax.
   *
   * @param sectionEMH
   * @param coefRempliPourBrancheI
   */
  private double calculeNiveauEauSection(final EMHSectionProfil sectionEMH, final double coefRempliPourBrancheI) {

    double zini = 0.0;
    double zmin = 0.0;
    double zmax = 0.0;

    final List<DonPrtGeo> dptgsSection = sectionEMH.getDPTG();
    if (dptgsSection != null) {

      for (int i = 0, imax = dptgsSection.size(); i < imax; i++) {
        final DonPrtGeo dptg = dptgsSection.get(i);
        // On prend le ymin et ymax du premier profil trouvé
        if (dptg instanceof DonPrtGeoProfilSection) {
          final DonPrtGeoProfilSection profilSection = (DonPrtGeoProfilSection) dptg;
          final List<PtProfil> ptsProfil = profilSection.getPtProfil();

          for (int j = 0, jmax = ptsProfil.size(); j < jmax; j++) {
            final PtProfil ptProfil = ptsProfil.get(j);
            if (j == 0) {
              zmin = ptProfil.getZ();
              zmax = ptProfil.getZ();
            } else {
              if (ptProfil.getZ() < zmin) {
                zmin = ptProfil.getZ();
              } else if (ptProfil.getZ() > zmax) {
                zmax = ptProfil.getZ();
              }
            }
          }
          break;
        }
      }
    }
    zini = zmin + coefRempliPourBrancheI * (zmax - zmin);

    return zini;
  }

  /**
   * @param reader
   * @param res
   * @param cru
   */
  private void readDonneesPermanentes(final CompositeReader reader, final CrueData res, final double cru)
      throws IOException {
    final CrueConfigMetier cruePropertyDefinitionContainer = dataLinked.getCrueConfigMetier();
    // Ligne A : DTPJ, DTPH, DTPM, DTPS, TOLZ, TOLQ, ICALMX, IPRINT, NDECOU
    reader.readLine();
    final Duration dtperm = readDuration(reader);
    final int nbField = reader.getNumberOfFields();
    final double tolz = nbField > 4 ? reader.doubleField(4) : cruePropertyDefinitionContainer.getDefaultDoubleValue(
        CrueConfigMetierConstants.PROP_TOL_MAX_Z);
    final double tolq = nbField > 5 ? reader.doubleField(5) : cruePropertyDefinitionContainer.getDefaultDoubleValue(
        CrueConfigMetierConstants.PROP_TOL_MAX_Q);
    final int icalmx = nbField > 6 ? reader.intField(6) : cruePropertyDefinitionContainer.getDefaultIntValue(
        CrueConfigMetierConstants.PROP_NBR_PDT_MAX);
    final int ndecou = nbField > 8 ? reader.intField(8) : cruePropertyDefinitionContainer.getDefaultIntValue(
        CrueConfigMetierConstants.PROP_NBR_PDT_DECOUP);
    if (isFine()) {
      LOGGER.log(Level.FINE, "dtperm{0} | tolz {1}tolq  {2}  icalmx {3} ndecou{4}", new Object[]{dtperm, tolz, tolq, icalmx, ndecou});
    }
    final ParamNumModeleBase pnum = res.getOrCreatePNUM();
    final PdtCst pdtCst = new PdtCst();
    pdtCst.setPdtCst(dtperm);
    ParamNumCalcPseudoPerm numPerm = pnum.getParamNumCalcPseudoPerm();
    if (numPerm == null) {

      numPerm = new ParamNumCalcPseudoPerm(cruePropertyDefinitionContainer);
      pnum.setParamNumCalcPseudoPerm(numPerm);
    }
    numPerm.setPdt(pdtCst);
    numPerm.setTolMaxZ(tolz);
    numPerm.setTolMaxQ(tolq);
    numPerm.setNbrPdtMax(icalmx);
    numPerm.setNbrPdtDecoup(ndecou);

    final TIntObjectHashMap typesConditionLimites = new TIntObjectHashMap();
    typesConditionLimites.put(1, CalcPseudoPermNoeudNiveauContinuZimp.class);
    typesConditionLimites.put(2, CalcPseudoPermNoeudQapp.class);
    typesConditionLimites.put(3, "loi Q = f(z)");
    // station de pompage == Manoeuvre : non possible pour un calcul permanent (cf DCLM)
    typesConditionLimites.put(10, "station de pompage");
    typesConditionLimites.put(41, CalcPseudoPermBrancheOrificeManoeuvre.class);
    typesConditionLimites.put(42, CalcPseudoPermBrancheOrificeManoeuvre.class);

    // Ligne B : NUMPOI(I), LIMTYP(I) page 7 strucDh.doc
    // Ligne C : FIN
    reader.readLine();
    nbConlim = 0;
    // Liste qui contient le nom des noeuds ou branches dont la condition à la limite est à créer
    final List<String> nomsEmhs = new ArrayList<>();
    while (!FIN.equalsIgnoreCase(reader.stringField(0))) {
      final String nomEmh = reader.stringField(0);
      final int limtyp = reader.intField(1);

      nomsEmhs.add(nomEmh);

      if (!typesConditionLimites.contains(limtyp)) {
        // voir le fichier
        analyze_.addSevereError("io.dh.conLimUnknown.error", reader.getCurrent().getLineNumber(),
            Integer.valueOf(limtyp));
      } else {
        if (isFine()) {
          LOGGER.log(Level.FINE, "{0}: numPoint={1} limtype={2} soit {3}",
              new Object[]{reader.getCurrent().getLineNumber(), nomEmh, limtyp, typesConditionLimites.get(limtyp)});
        }

        // Ajout de la condition aux limites à l'EMH correspondante
        final Class classe = (Class) typesConditionLimites.get(limtyp);
        if (classe == CalcPseudoPermNoeudNiveauContinuZimp.class) {

          final CatEMHNoeud noeud = res.findNoeudByReference(nomEmh);
          if (noeud == null) {
            analyze_.addSevereError("io.dh.ref.emhnoeud.zimpose.error", nomEmh);
            return;
          }
          final CalcPseudoPermNoeudNiveauContinuZimp metier = new CalcPseudoPermNoeudNiveauContinuZimp(
              res.getCrueConfigMetier());
          noeud.addInfosEMH(metier);
          metier.setEmh(noeud);
        } else if (classe == CalcPseudoPermNoeudQapp.class) {

          final CatEMHNoeud noeud = res.findNoeudByReference(nomEmh);
          if (noeud == null) {
            analyze_.addSevereError("io.dh.ref.emhnoeud.qapp.error", nomEmh);
            return;
          }
          final CalcPseudoPermNoeudQapp metier = new CalcPseudoPermNoeudQapp(res.getCrueConfigMetier());
          noeud.addInfosEMH(metier);
          metier.setEmh(noeud);
        } else if (classe == CalcPseudoPermBrancheOrificeManoeuvre.class) {

          final CatEMHBranche branche = res.findBrancheByReference(nomEmh);
          if (branche == null) {
            analyze_.addSevereError("io.dh.ref.emhbranche.ouv.error", nomEmh);
            return;
          }
          final CalcPseudoPermBrancheOrificeManoeuvre calcPseudoPerm = new CalcPseudoPermBrancheOrificeManoeuvre(
              res.getCrueConfigMetier());
          EnumSensOuv newSensOuv = (EnumSensOuv) enumSensOuvMapCrue9.fromString(CtuluLibString.getString(limtyp));
          calcPseudoPerm.setSensOuv(newSensOuv);

          DonPrtCIniBrancheOrifice cini = EMHHelper.selectFirstOfClass(branche.getInfosEMH(),
              DonPrtCIniBrancheOrifice.class);
          if (cini == null) {
            cini = new DonPrtCIniBrancheOrifice(res.getCrueConfigMetier());
            branche.addInfosEMH(cini);
          }
          if (emhsWithSensOuvInitialized.contains(branche.getId())) {
            if (!newSensOuv.equals(cini.getSensOuv())) {
              analyze_.addWarn("Le sens d'ouverture de la branche " + branche.getId() + " doit être constant. Le sens "
                  + cini.getSensOuv() + " sera pris");
              newSensOuv = cini.getSensOuv();
            }
          } else {
            emhsWithSensOuvInitialized.add(branche.getId());
          }
          cini.setSensOuv(newSensOuv);
          branche.addInfosEMH(calcPseudoPerm);
          calcPseudoPerm.setEmh(branche);
        } else if (limtyp == 3 || limtyp == 10) {

          analyze_.addSevereError("io.dh.limtyp.notsupported.calcPerm.error", typesConditionLimites.get(limtyp));
        }
      }
      reader.readLine();
      nbConlim++;
    }
    if (isFine()) {
      LOGGER.log(Level.FINE, "Nombre de conditions aux limites {0}", nbConlim);
    }

    reader.readLine();
    // Ligne D : modulation du pas de temps au cours de la procédure de stabilisation (les lignes D sont facultatives)
    // Ligne D1
    if (VARDT.equals(reader.stringField(0))) {
      if (isFine()) {
        LOGGER.fine("Modulation du pas de temps");
      }
      reader.readLine();

      // Une ligne D2 par pas de temps variable
      // Ligne D3 : FIN
      final List<ElemPdt> elementsPdt = new ArrayList<>();
      while (!FIN.equalsIgnoreCase(reader.stringField(0))) {
        // Ligne D2 : N, VARDTJ, VARDTH, VARDTM, VARDTS
        final int n = reader.intField(0);
        final Duration variation = readDuration(reader, 1);

        if (isFine()) {
          LOGGER.fine("module pour " + n + " var=" + variation);
        }

        final ElemPdt elemPdt = new ElemPdt(res.getCrueConfigMetier());
        elemPdt.setNbrPdt(n);
        elemPdt.setDureePdt(variation);
        elementsPdt.add(elemPdt);

        reader.readLine();
      }

      if (!elementsPdt.isEmpty()) {
        // pnum et pdtPerm déjà instanciés plus haut
        final PdtVar pdtVar = new PdtVar();
        pdtVar.setElemPdt(elementsPdt);
        pnum.getParamNumCalcPseudoPerm().setPdt(pdtVar);
      }

      // on lit la premiere ligne pour etre au meme niveau que sans vardt
      reader.readLine();
    }

    // Valeurs des conditions aux limites (régimes permanents)
    // Une ligne E définit la valeur des conditions aux limites d'un état permanent. Il y a autant de lignes E que
    // d'états permanents
    if (isFine()) {
      LOGGER.log(Level.FINE, "lecture de qrq {0}", dhContainsRuis);
    }
    int nbEtatPermanent = 0;
    final int nbColExpected = nbConlim + (dhContainsRuis ? 1 : 0);

    DonCLimMScenario dclmScenario = res.getConditionsLim();
    if (dclmScenario == null) {
      dclmScenario = new DonCLimMScenario();
      res.setConditionsLim(dclmScenario);
    }

    final List<CalcPseudoPerm> calculsPermanents = new ArrayList<>();
    final OrdCalcScenario ocal = res.getOCAL();

    while (!FIN.equalsIgnoreCase(reader.stringField(0))) {
      // CONLIM (IM) : valeur de la IMème condition à la limite
      // QRQ : valeur de la loi de ruissellement (à donner si CRU # 0)
      if (nbColExpected != reader.getNumberOfFields()) {
        analyze_.addSevereError("io.dh.conLimRegimePermanent.error", reader.current.getLineNumber());
      }

      // Le coef de ruissellement est en derniere position
      double qruis = 0;
      if (dhContainsRuis) {
        qruis = reader.doubleField(nbConlim);
        if (isFine()) {
          LOGGER.log(Level.FINE, "{0} qrq={1}", new Object[]{reader.getCurrent().getLineNumber(), qruis});
        }
      }

      final CalcPseudoPerm calcPerm = new CalcPseudoPerm();

      calcPerm.setNom(CruePrefix.P_CALCUL + "P" + calulFormat.format(nbEtatPermanent + 1L));
      calcPerm.setCommentaire("Calcul pseudo-permanent " + (nbEtatPermanent + 1));
      // pour le premier on prend en compte la reprise éventuelle
      final boolean first = ocal.getOrdCalc().isEmpty();

      final OrdCalcPseudoPerm iniPrec = first ? new OrdCalcPseudoPermIniCalcCI()
          : new OrdCalcPseudoPermIniCalcPrecedent();
      iniPrec.setCalcPseudoPerm(calcPerm);
      ocal.addOrdCalc(iniPrec);

      for (int i = 0; i < nbConlim; i++) {
        // la valeur des conditions aux limites d'un état permanent.
        final double conlimi = reader.doubleField(i);

        final String nomEmh = nomsEmhs.get(i);
        final EMH emh = res.findEMHByReference(nomEmh);
        if (emh == null) {
          analyze_.addSevereError("io.dh.ref.emh.ligneE.error", nomEmh);
          return;
        }

        // Stockage de la condition aux limites correspondant au noeud ou à la branche définie dans les lignes B
        if (emh instanceof CatEMHNoeud) {

          final List<DonCLimM> listeDCLMs = emh.getDCLM();
          DonCLimM dclm = null;
          // il devrait y avoir une seule DCLM par noeud, ajouté plus haut
          if (CollectionUtils.isNotEmpty(listeDCLMs)) {
            dclm = listeDCLMs.get(0);

            // listeRefs.add(emh);
            if (dclm instanceof CalcPseudoPermNoeudQapp) {

              CalcPseudoPermNoeudQapp nncQapp = null;
              // Pour le 1er calcul permanent, on utilise l'objet crée plus haut et on le compléte, sinon, on crée un
              // nouvel objet que l'on affecte à l'emh
              if (nbEtatPermanent == 0) {
                nncQapp = (CalcPseudoPermNoeudQapp) dclm;
              } else {
                nncQapp = new CalcPseudoPermNoeudQapp(res.getCrueConfigMetier());
                emh.addInfosEMH(nncQapp);
                nncQapp.setEmh(emh);
              }
              nncQapp.setQapp(conlimi);
              nncQapp.setCalculParent(calcPerm);
              calcPerm.addDclm(nncQapp);
            } else if (dclm instanceof CalcPseudoPermNoeudNiveauContinuZimp) {

              CalcPseudoPermNoeudNiveauContinuZimp nncZimpose = null;
              if (nbEtatPermanent == 0) {
                nncZimpose = (CalcPseudoPermNoeudNiveauContinuZimp) dclm;
              } else {
                nncZimpose = new CalcPseudoPermNoeudNiveauContinuZimp(res.getCrueConfigMetier());
                emh.addInfosEMH(nncZimpose);
                nncZimpose.setEmh(emh);
              }
              nncZimpose.setZimp(conlimi);
              nncZimpose.setCalculParent(calcPerm);
              calcPerm.addDclm(nncZimpose);
            }
          }
        } else if (emh instanceof EMHBrancheOrifice) {

          final List<DonCLimM> listeDCLMs = emh.getDCLM();
          DonCLimM dclm = null;
          if (CollectionUtils.isNotEmpty(listeDCLMs)) {
            dclm = listeDCLMs.get(0);

            if (dclm instanceof CalcPseudoPermBrancheOrificeManoeuvre) {

              CalcPseudoPermBrancheOrificeManoeuvre brancheOrOuv = null;
              if (nbEtatPermanent == 0) {
                brancheOrOuv = (CalcPseudoPermBrancheOrificeManoeuvre) dclm;
              } else {
                brancheOrOuv = new CalcPseudoPermBrancheOrificeManoeuvre(res.getCrueConfigMetier());
                // On récupère le SensOuv qui a été enregistré pour la même branche dans le 1er calcul permanent
                brancheOrOuv.setSensOuv(((CalcPseudoPermBrancheOrificeManoeuvre) dclm).getSensOuv());
                emh.addInfosEMH(brancheOrOuv);
                brancheOrOuv.setEmh(emh);
              }
              brancheOrOuv.setOuv(conlimi);
              brancheOrOuv.setCalculParent(calcPerm);
              calcPerm.addDclm(brancheOrOuv);
            }
          }
          if (!emhsWithDPTIInitialized.contains(emh.getId())) {
            emhsWithDPTIInitialized.add(emh.getId());
            final DonPrtCIniBrancheOrifice cti = EMHHelper.selectFirstOfClass(emh.getInfosEMH(),
                DonPrtCIniBrancheOrifice.class);
            if (cti != null) {// que faire si null ?
              cti.setOuv(conlimi);
            }
          }

          // CalcPseudoPermCasierProfilQruis inexistants en crue 9
        }
      }

      // S'il y a un débit de ruissellement, il faut créer autant de brancheSaintVenantQruis que de branches de type
      // SaintVenant de DRSO
      if (dhContainsRuis) {

        final List<EMHBrancheSaintVenant> branches = res.getBranchesSaintVenant();
        if (branches != null) {
          for (int i = 0, imax = branches.size(); i < imax; i++) {// NOPMD ok
            final EMHBrancheSaintVenant emh = branches.get(i);
            final CalcPseudoPermBrancheSaintVenantQruis brancheSV = new CalcPseudoPermBrancheSaintVenantQruis(
                res.getCrueConfigMetier());
            // qruis lu en amont
            brancheSV.setQruis(qruis);
            brancheSV.setCalculParent(calcPerm);
            calcPerm.addDclm(brancheSV);
            emh.addInfosEMH(brancheSV);
            brancheSV.setEmh(emh);
          }
        }
        final List<EMHCasierProfil> casiers = EMHHelper.selectClass(res.getCasiers(), EMHCasierProfil.class);
        for (int i = 0, imax = casiers.size(); i < imax; i++) {// NOPMD ok
          final EMHCasierProfil emh = casiers.get(i);
          final CalcPseudoPermCasierProfilQruis calcCasier = new CalcPseudoPermCasierProfilQruis(
              res.getCrueConfigMetier());
          // qruis lu en amont
          calcCasier.setQruis(qruis);
          calcCasier.setCalculParent(calcPerm);
          calcPerm.addDclm(calcCasier);
          emh.addInfosEMH(calcCasier);
          calcCasier.setEmh(emh);
        }
      }

      calculsPermanents.add(calcPerm);

      if (isFine()) {
        final StringBuilder build = new StringBuilder();
        for (int i = 0; i < nbConlim; i++) {
          // la valeur des conditions aux limites d'un état permanent.
          build.append(reader.doubleField(i)).append(" ; ");
        }
        LOGGER.log(Level.FINE, "{0} conlims={1}", new Object[]{reader.getCurrent().getLineNumber(), build});
      }

      nbEtatPermanent++;
      reader.readLine();
    }

    if (CollectionUtils.isNotEmpty(calculsPermanents)) {
      dclmScenario.setCalcPseudoPerm(calculsPermanents);
    }

    if (isFine()) {
      LOGGER.log(Level.FINE, "Nombre d''\u00e9tats pseudo-permanents {0}", nbEtatPermanent);
    }
  }

  /**
   * @param reader
   * @param res
   * @param cofqrq
   */
  private void readDonneesTransitoires(final CompositeReader reader, final CrueData res, final double cofqrq)
      throws Exception {
    // Ligne A : DTJ,DTH, DTM, DTS, TMAXJ, TMAXH, TMAXM, TMAXS, TDEBJ,
    // TDEBH, TDEBM, TDEBS, HDEBJ, HDEBH, HDEBM, HDEBS, NTS, TMAX
    reader.readLine();
    final Duration pasDeTemps = readDuration(reader);
    final Duration transDureeCalc = readDuration(reader, 4);
    final int nts = reader.intField(16);

    final ParamNumModeleBase pnum = res.getOrCreatePNUM();
    final PdtCst pdtCst = new PdtCst();
    pdtCst.setPdtCst(pasDeTemps);
    ParamNumCalcTrans trans = pnum.getParamNumCalcTrans();
    if (trans == null) {
      trans = new ParamNumCalcTrans(res.getCrueConfigMetier());
      pnum.setParamNumCalcTrans(trans);
    }
    trans.setPdt(pdtCst);

    // Enregistrement de tMax, hDeb et nts transformé en période dans PCAL
    //NTS = OCAL.OrdCalcTrans.PdtRes.PdtCst / PNUM.ParamNumCalcTrans.Pdt.PdtCst
    final Pdt transPdtRes = EMHFactory.createPdtCst(new Duration((pasDeTemps.getMillis() * nts)));
    reader.readLine();

    // Définition de toutes les conditions limites supportees.
    final TIntObjectHashMap typesConditionLimites = new TIntObjectHashMap();
    // La loi pour Limnigramme et Zimpose est la même
    typesConditionLimites.put(1, CalcTransNoeudNiveauContinuLimnigramme.class);
    typesConditionLimites.put(2, CalcTransNoeudQapp.class);
    typesConditionLimites.put(3, CalcTransNoeudNiveauContinuTarage.class);
    // loi 10 = manoeuvre pour branche de type 10 : non supporté (hors périmètre)
    typesConditionLimites.put(10, "Station de pompage");
    typesConditionLimites.put(20, "débit au barrage");
    typesConditionLimites.put(21, "débit à l'usine");
    typesConditionLimites.put(22, "débit au barrage + débit à l'usine");
    // Pour + tard
    typesConditionLimites.put(41, CalcTransBrancheOrificeManoeuvre.class);
    typesConditionLimites.put(42, CalcTransBrancheOrificeManoeuvre.class);

    // Ligne B : NUMPOI(I), LIMTYP(I)
    // Ligne C : FIN
    // Définition des noeuds et des branches auxquels sont appliquées les conditions aux limites, et du type des
    // conditions aux limites
    int nbConlimTrans = 0;

    // Alimentation du calcul transitoire
    DonCLimMScenario dclm = res.getConditionsLim();
    if (dclm == null) {
      dclm = new DonCLimMScenario();
      res.setConditionsLim(dclm);
    }

    // Un seul calcul transitoire possible en version crue 9
    final CalcTrans calcTrans = new CalcTrans();
    calcTrans.setNom(CruePrefix.P_CALCUL + "T01");
    calcTrans.setCommentaire("Calcul transitoire 1");
    dclm.addCalcTrans(calcTrans);
    final OrdCalcTransIniCalcPrecedent prec = new OrdCalcTransIniCalcPrecedent(dataLinked.getCrueConfigMetier());
    prec.setDureeCalc(transDureeCalc);
    prec.setPdtRes(transPdtRes);
    prec.setCalcTrans(calcTrans);
    res.getOCAL().addOrdCalc(prec);

    final List<String> nomsEmhs = new ArrayList<>();
    final Map<String, String> nomsEmhsNomInDc = new HashMap<>();

    while (!FIN.equalsIgnoreCase(reader.stringField(0))) {
      final String nomEmh = reader.stringField(0);
      final int limtyp = reader.intField(1);
      nomsEmhs.add(nomEmh);

      if (!typesConditionLimites.contains(limtyp)) {
        analyze_.addSevereError("io.dh.unknownConlimTransitoire.error", reader.getCurrent().getLineNumber());
        logError("io.dc.unknownConlimTransitoire", reader);
      } else {
        if (isFine()) {
          LOGGER.log(Level.FINE, "condition limite transitoire en {0} var={1}->{2}", new Object[]{nomEmh, limtyp, typesConditionLimites.get(limtyp)});
        }

        // Ajout de la condition aux limites à l'EMH correspondante
        final Class classe = (Class) typesConditionLimites.get(limtyp);
        if (classe == CalcTransNoeudQapp.class) {
          final CatEMHNoeud noeud = res.findNoeudByReference(nomEmh);
          if (noeud == null) {
            analyze_.addSevereError("io.dh.ref.emhnoeud.hydro.error", nomEmh);
            return;
          }
          nomsEmhsNomInDc.put(nomEmh, noeud.getNom());
          final CalcTransNoeudQapp metier = new CalcTransNoeudQapp();
          metier.setCalculParent(calcTrans);

          noeud.addInfosEMH(metier);
          metier.setEmh(noeud);
          // listeRefs.add(noeud);
          calcTrans.addDclm(metier);
        } else if (classe == CalcTransNoeudNiveauContinuLimnigramme.class) {

          final CatEMHNoeud noeud = res.findNoeudByReference(nomEmh);
          if (noeud == null) {
            analyze_.addSevereError("io.dh.ref.emhnoeud.limni.error", nomEmh);
            return;
          }
          nomsEmhsNomInDc.put(nomEmh, noeud.getNom());
          final CalcTransNoeudNiveauContinuLimnigramme metier = new CalcTransNoeudNiveauContinuLimnigramme();
          metier.setCalculParent(calcTrans);
          noeud.addInfosEMH(metier);
          metier.setEmh(noeud);
          calcTrans.addDclm(metier);
        } else if (classe == CalcTransNoeudNiveauContinuTarage.class) {

          final CatEMHNoeud noeud = res.findNoeudByReference(nomEmh);
          if (noeud == null) {
            analyze_.addSevereError("io.dh.ref.emhnoeud.tarrage.error", nomEmh);
            return;
          }
          nomsEmhsNomInDc.put(nomEmh, noeud.getNom());
          final CalcTransNoeudNiveauContinuTarage metier = new CalcTransNoeudNiveauContinuTarage();
          metier.setCalculParent(calcTrans);
          noeud.addInfosEMH(metier);
          metier.setEmh(noeud);
          calcTrans.addDclm(metier);
        } else if (classe == CalcTransBrancheOrificeManoeuvre.class) {

          final CatEMHBranche branche = res.findBrancheByReference(nomEmh);
          if (branche == null) {
            analyze_.addSevereError("io.dh.ref.emhbranche.manoeuvre.error", nomEmh);
            return;
          }
          nomsEmhsNomInDc.put(nomEmh, branche.getNom());
          final CalcTransBrancheOrificeManoeuvre metier = new CalcTransBrancheOrificeManoeuvre(
              res.getCrueConfigMetier());
          metier.setCalculParent(calcTrans);
          if (limtyp == 41) {
            metier.setSensOuv(EnumSensOuv.OUV_VERS_HAUT);
          } else if (limtyp == 42) {
            metier.setSensOuv(EnumSensOuv.OUV_VERS_BAS);
          }
          branche.addInfosEMH(metier);
          metier.setEmh(branche);
          calcTrans.addDclm(metier);
        } else if (limtyp == 10 || limtyp == 20 || limtyp == 21 || limtyp == 22) {

          analyze_.addSevereError("io.dh.limtyp.notsupported.calcTrans.error", typesConditionLimites.get(limtyp));
        }
      }
      reader.readLine();
      nbConlimTrans++;
    }

    // Ligne D
    reader.readLine();
    // on lit la carte vardt
    if (VARDT.equals(reader.stringField(0))) {
      if (isFine()) {
        LOGGER.fine("Modulation du pas de temps pour le transitoire");
      }
      reader.readLine();

      final List<ElemPdt> elementsPdt = new ArrayList<>();

      Duration lastDuration = new Duration(0);
      Duration currentDuration;
      Duration lastStepTime = pasDeTemps;
      int nbStepTime = 0;

      while (!FIN.equalsIgnoreCase(reader.stringField(0))) {

        currentDuration = readDuration(reader, 0);

        nbStepTime = (int) Math.ceil(((double) currentDuration.minus(lastDuration).getStandardSeconds())
            / lastStepTime.getStandardSeconds());

        if (elementsPdt.isEmpty()) {
          if (nbStepTime != 0) {
            final ElemPdt firstElemPdt = new ElemPdt(res.getCrueConfigMetier());
            firstElemPdt.setNbrPdt(nbStepTime);
            firstElemPdt.setDureePdt(lastStepTime);
            elementsPdt.add(firstElemPdt);
            if (isFine()) {
              LOGGER.log(Level.FINE, "module pour {0} pas de temps de duree {1}", new Object[]{nbStepTime, lastStepTime});
            }
          }
        } else {
          elementsPdt.get(elementsPdt.size() - 1).setNbrPdt(nbStepTime);
          if (isFine()) {
            LOGGER.log(Level.FINE, "module pour {0} pas de temps de duree {1}", new Object[]{nbStepTime, lastStepTime});
          }
        }

        nbStepTime = 1;
        lastStepTime = readDuration(reader, 4);
        lastDuration = currentDuration;

        final ElemPdt elemPdt = new ElemPdt(res.getCrueConfigMetier());
        elemPdt.setNbrPdt(nbStepTime);
        elemPdt.setDureePdt(lastStepTime);
        elementsPdt.add(elemPdt);

        reader.readLine();
      }

      if (isFine()) {
        LOGGER.log(Level.FINE, "module pour {0} pas de temps de duree {1}", new Object[]{nbStepTime, lastStepTime});
      }

      if (!elementsPdt.isEmpty()) {
        // pnum et pdtTrans déjà instanciés plus haut
        final PdtVar pdtVar = new PdtVar();
        pdtVar.setElemPdt(elementsPdt);
        trans.setPdt(pdtVar);
      }

      reader.readLine();
    }

    if (isFine()) {
      LOGGER.fine("Lecture des conditions aux limites transitoires");
    }

    // Ligne E : page 11 du document
    for (int i = 0; i < nbConlimTrans; i++) {
      String nomToUse = nomsEmhsNomInDc.get(nomsEmhs.get(i));
      if (nomToUse == null) {
        nomToUse = nomsEmhs.get(i);
      }

      final int nbOfField = reader.getNumberOfFields();
      int ilecam = 3;
      // pour chaque conlim, on a soit une valeur (ilecam) soit 2 valeurs (zbas et dz)
      if (nbOfField == 1) {
        ilecam = reader.intField(0);
        // on lit la ligne dans ce cas.
        reader.readLine();
      } else if (nbOfField != 2) {
        analyze_.addSevereError("io.dh.conlimTransitoire.error", reader.getLineNumber());
        logError("io.dh.conlimTransitoire.error", reader);
        return;
      }

      if (ilecam == 1) {
        // LOI FONCTION DU TEMPS FOURNIE, AVEC DEFAULT_DEBUG_LEVEL_FOR_CRUE9 PAS DE TEMPS CONSTANT

        final Duration pasDeTempsDefLoi = readDuration(reader);
        final Duration decal = readDuration(reader, 4);
        reader.readLine();
        // les valeurs sur les pas de temps
        final EvolutionFF evolutionFF = new EvolutionFF();
        final List<PtEvolutionFF> pts = new ArrayList<>();
        evolutionFF.setPtEvolutionFF(pts);
        final double dt = pasDeTempsDefLoi.getStandardSeconds();
        double t = 0;
        // si on a un decalage, on ajoute la valeur 0 pour Y en attendant d'affecter la valeur de la loi avec le 1er
        // point (cf apres le while qui suit)
        final boolean isDecal = decal.getMillis() > 0;
        if (isDecal) {
          pts.add(new PtEvolutionFF(t, 0));
          t = decal.getStandardSeconds();
        }
        while (!FIN.equalsIgnoreCase(reader.stringField(0))) {
          final int nbField = reader.getNumberOfFields();
          // lecture de ALAM (I) : Ième valeur de la loi amont
          for (int j = 0; j < nbField; j++) {
            pts.add(new PtEvolutionFF(t, reader.doubleField(j)));
            t += dt;
          }
          reader.readLine();
        }
        // on met a jour la premiere valeur si décalage
        if (isDecal && pts.get(1) != null) {
          pts.get(0).setOrdonnee(pts.get(1).getOrdonnee());
        }
        if (isFine()) {
          LOGGER.log(Level.FINE, "ligne {0} loi lue {1}", new Object[]{reader.getLineNumber(), evolutionFF});
        }
        alimenteCalculTransAvecCL(calcTrans, nomToUse, res, evolutionFF);
      } else if (ilecam == 2) {

        final EvolutionFF evolutionFF = new EvolutionFF();
        final List<PtEvolutionFF> pts = new ArrayList<>();
        evolutionFF.setPtEvolutionFF(pts);
        // les pas de temps
        Duration lastDuration = null;
        while (!FIN.equalsIgnoreCase(reader.stringField(0))) {
          if (reader.getNumberOfFields() != 5) {
            analyze_.addSevereError("io.dh.ref.emh.ligneE2.error", reader.getLineNumber());
            return;
          }
          final Duration date = readDuration(reader);
          if (lastDuration != null && lastDuration.compareTo(date) > 0) {
            analyze_.addSevereError("io.dh.ref.emh.ligneE2.notCroissant.error", reader.getLineNumber());
            return;
          }

          lastDuration = date;
          pts.add(new PtEvolutionFF(date.getStandardSeconds(), reader.doubleField(4)));
          reader.readLine();
        }
        if (isFine()) {
          LOGGER.log(Level.FINE, "ligne {0} loi lue {1}", new Object[]{reader.getLineNumber(), evolutionFF});
        }

        alimenteCalculTransAvecCL(calcTrans, nomToUse, res, evolutionFF);

        // ilecam ne vaut jamais 3 dans le fichier mais c'est plus simple
      } else if (ilecam == 3) {
        // LIGNE E3 : LOI NE DEPENDANT PAS DU TEMPS (LOI Q(z))
        //
        // Ligne E3_1 : ZBAS, DZ
        // ZBAS : valeur du paramètre (z) correspondant au 1er point
        // DZ : incrément du paramètre (z) entre 2 valeurs de la loi
        final double zbas = reader.doubleField(0);
        final double dz = reader.doubleField(1);
        if (isFine()) {
          LOGGER.log(Level.FINE, "ligne {0} zbas {1} dz {2}", new Object[]{reader.getLineNumber(), zbas, dz});
        }

        reader.readLine();

        final EvolutionFF evolutionFF = new EvolutionFF();
        final List<PtEvolutionFF> pts = new ArrayList<>();
        evolutionFF.setPtEvolutionFF(pts);
        double z = zbas;
        while (!FIN.equalsIgnoreCase(reader.stringField(0))) {
          // Ligne E3_2 : (Q (I), I = 1, KMXAA)
          // Q (I) : Ième valeur de la loi Q (z)
          // on lit une loi QZ donc Q en abscisse
          final int nbField = reader.getNumberOfFields();

          for (int j = 0; j < nbField; j++) {
            pts.add(new PtEvolutionFF(reader.doubleField(j), z));
            z += dz;
          }
          reader.readLine();
        }
        if (isFine()) {
          LOGGER.log(Level.FINE, "ligne {0} loi ne dependant pas du temps lue {1}", new Object[]{reader.getLineNumber(), evolutionFF});
        }

        final String nomEmh = nomsEmhs.get(i);
        final EMH emh = res.findEMHByReference(nomEmh);
        if (emh == null) {
          analyze_.addSevereError("io.dh.ref.emh.ligneE.trans.error", nomEmh);
          return;
        }
        alimenteCalculTransAvecCL(calcTrans, emh.getNom(), res, evolutionFF);
      } else {
        analyze_.addSevereError("io.dh.conLimTransitoire.ilecam.error", reader.getLineNumber(), String.valueOf(ilecam));
        logError("io.dh.conLimTransitoire.ilecam.error " + ilecam, reader);
      }

      // Génére une EOFException si la lecture du fichier est terminée
      reader.readLine();
    }

    if (dhContainsRuis && "RUIS".equalsIgnoreCase(reader.stringField(0))) {
      reader.readLine();
      final EvolutionFF evolutionFF = new EvolutionFF();
      final List<PtEvolutionFF> pts = new ArrayList<>();
      evolutionFF.setPtEvolutionFF(pts);

      while (!FIN.equalsIgnoreCase(reader.stringField(0))) {
        final Duration date = readDuration(reader);
        pts.add(new PtEvolutionFF(date.getStandardSeconds(), reader.doubleField(4)));
        reader.readLine();
      }

      // Création des branches SaintVenantQruis correspondant aux branchex saint venant de DRSO
      final DonLoiHYConteneur dlhy = res.getLoiConteneur();

      final List<EMHBrancheSaintVenant> branches = res.getBranchesSaintVenant();
      final String nomLoi = CruePrefix.addPrefixs("1", EnumTypeLoi.LoiTQruis, EnumCatEMH.BRANCHE);
      final LoiDF loiDF = new LoiDF();
      LoiFactory.alimenteDebutLoiDF(loiDF, nomLoi, EnumTypeLoi.LoiTQruis);
      loiDF.setEvolutionFF(evolutionFF);
      dlhy.addLois(loiDF);
      for (final EMHBrancheSaintVenant emh : branches) {
        final CalcTransBrancheSaintVenantQruis brancheSV = new CalcTransBrancheSaintVenantQruis();
        calcTrans.addDclm(brancheSV);
        brancheSV.setCalculParent(calcTrans);
        brancheSV.setEmh(emh);
        emh.addInfosEMH(brancheSV);
        brancheSV.setHydrogrammeQruis(loiDF);
      }
      final List<EMHCasierProfil> casiers = EMHHelper.selectClass(res.getCasiers(), EMHCasierProfil.class);
      for (final EMHCasierProfil casier : casiers) {
        final CalcTransCasierProfilQruis ctProfil = new CalcTransCasierProfilQruis();
        calcTrans.addDclm(ctProfil);
        ctProfil.setCalculParent(calcTrans);
        ctProfil.setEmh(casier);
        casier.addInfosEMH(ctProfil);
        ctProfil.setHydrogrammeQruis(loiDF);
      }
    }
  }

  /**
   * @param nomCT
   * @param nomEmh
   * @param res
   * @param evolutionFF
   */
  private void alimenteCalculTransAvecCL(final CalcTrans calcTrans, final String nomEmh, final CrueData res,
                                         final EvolutionFF evolutionFF) {

    final EMH emh = res.findEMHByReference(nomEmh);
    if (emh == null) {
      analyze_.addSevereError("io.dh.ref.emh.ligneE.trans.error", nomEmh);
      return;
    }

    final DonLoiHYConteneur dlhy = res.getLoiConteneur();
    final List<DonCLimM> listeDCLMs = emh.getDCLM();
    // il devrait y avoir la DCLM du calcul transitoire, ajouté plus haut (sinon c'est une erreur d'algo)
    if (CollectionUtils.isEmpty(listeDCLMs)) {
      return;
    }

    final int size = listeDCLMs.size();
    String nomLoi = null;

    if (emh instanceof CatEMHNoeud) {
      for (int i = 0; i < size; i++) {
        final DonCLimM donneeTrans = listeDCLMs.get(i);
        if (donneeTrans instanceof CalcTransNoeudQapp || donneeTrans instanceof CalcTransNoeudNiveauContinuLimnigramme
            || donneeTrans instanceof CalcTransNoeudNiveauContinuTarage) {

          final CalcTransItem nncTrans = (CalcTransItem) donneeTrans;
          // pour s'assurer de la correspondance
          nncTrans.setEmh(emh);

          if (nncTrans.getNomCalculParent() != null && nncTrans.getNomCalculParent().equals(calcTrans.getNom())) {

            if (nncTrans instanceof CalcTransNoeudQapp) {

              nomLoi = CruePrefix.addPrefixs(nomEmh, EnumTypeLoi.LoiTQapp, EnumCatEMH.NOEUD);
              final Loi loi = LoiHelper.findByReference(nomLoi, dlhy.getLois());
              if (loi == null) {
                final LoiDF loiDF = new LoiDF();
                LoiFactory.alimenteDebutLoiDF(loiDF, nomLoi, EnumTypeLoi.LoiTQapp);
                loiDF.setEvolutionFF(evolutionFF);

                final CalcTransNoeudQapp nncHydro = (CalcTransNoeudQapp) nncTrans;
                nncHydro.setHydrogrammeQapp(loiDF);

                // Ajout de la loi dans DLHY
                dlhy.addLois(loiDF);
              }
            } else if (nncTrans instanceof CalcTransNoeudNiveauContinuLimnigramme) {

              nomLoi = CruePrefix.addPrefixs(nomEmh, EnumTypeLoi.LoiTZimp, EnumCatEMH.NOEUD);
              final Loi loi = LoiHelper.findByReference(nomLoi, dlhy.getLois());
              if (loi == null) {
                final LoiDF loiDF = new LoiDF();
                LoiFactory.alimenteDebutLoiDF(loiDF, nomLoi, EnumTypeLoi.LoiTZimp);
                loiDF.setEvolutionFF(evolutionFF);

                final CalcTransNoeudNiveauContinuLimnigramme nncLimni = (CalcTransNoeudNiveauContinuLimnigramme) nncTrans;
                nncLimni.setLimnigramme(loiDF);

                // Ajout de la loi dans DLHY
                dlhy.addLois(loiDF);
              }
            } else if (nncTrans instanceof CalcTransNoeudNiveauContinuTarage) {

              nomLoi = CruePrefix.addPrefixs(nomEmh, EnumTypeLoi.LoiQZimp, EnumCatEMH.NOEUD);
              final Loi loi = LoiHelper.findByReference(nomLoi, dlhy.getLois());
              if (loi == null) {
                final LoiFF loiFF = new LoiFF();
                LoiFactory.alimenteDebutLoi(loiFF, nomLoi, EnumTypeLoi.LoiQZimp);
                loiFF.setEvolutionFF(evolutionFF);
                final CalcTransNoeudNiveauContinuTarage nncTarrage = (CalcTransNoeudNiveauContinuTarage) nncTrans;
                nncTarrage.setTarage(loiFF);

                // Ajout de la loi dans DLHY
                dlhy.addLois(loiFF);
              }
            }
          }

          break;
        }
      }
    } else if (emh instanceof EMHBrancheOrifice) {

      for (int i = 0; i < size; i++) {
        final DonCLimM donneeTrans = listeDCLMs.get(i);
        if (donneeTrans instanceof CalcTransBrancheOrificeManoeuvre) {

          final CalcTransBrancheOrificeManoeuvre brancheOrManoeuvre = (CalcTransBrancheOrificeManoeuvre) donneeTrans;
          if (brancheOrManoeuvre.getNomCalculParent() != null
              && brancheOrManoeuvre.getNomCalculParent().equals(calcTrans.getNom())) {

            nomLoi = CruePrefix.addPrefixs(nomEmh, EnumTypeLoi.LoiTOuv, EnumCatEMH.BRANCHE);
            final Loi loi = LoiHelper.findByReference(nomLoi, dlhy.getLois());
            if (loi == null) {
              final LoiDF loiDF = new LoiDF();
              LoiFactory.alimenteDebutLoiDF(loiDF, nomLoi, EnumTypeLoi.LoiTOuv);
              loiDF.setEvolutionFF(evolutionFF);

              brancheOrManoeuvre.setManoeuvre(loiDF);

              // Ajout de la loi dans DLHY
              dlhy.addLois(loiDF);
            }
          }
          break;
        }
      }
    }

    // Pas de CasierProfilQruis en crue 9
  }

  boolean dhContainsRuis;
  private double cru;

  /**
   * Methode principale qui fait le tout.
   *
   * @param res
   */
  private void readAllDonnees(final CrueIOResu<CrueData> res) throws Exception {
    // on ignore les lignes blanches et on ignore les commentaires commencant par #
    initReader(in_);
    final CompositeReader reader = new CompositeReader(in_);
    // on lit les 5 lignes de titre
    // Lecture ligne A
    final StringBuilder comm = new StringBuilder(420);
    for (int i = 0; i < NB_LINES_TITLE; i++) {
      if (i > 0) {
        comm.append('\n');
      }
      comm.append(reader.readLine());
    }
    res.setCrueCommentaire(comm.toString());
    // fin lecture ligne A
    reader.readLine();

    final CrueData metier = readRegles(res, reader);

    // lecture ligne B
    // Ligne B : ICAL, CRU, COFQRQ, ISORTI, ILE, TREPJ, TREPH, TREPM, TREPS
    // ICAL = 0 : reprise de calcul en transitoire
    // ICAL = 1 : succession de calculs en régime permanent
    // ICAL = 2 : stabilisation d'une ligne d'eau initiale par une suite de calculs en régime permanent, puis calcul en
    // régime transitoire à partir de la dernière ligne d'eau calculée en régime permanent.
    final int ical = reader.intField(0);

    cru = reader.doubleField(1);

    // COFQRQ : coefficient multiplicatif des données de ruissellement
    // (0 = pas de débit de ruissellement)
    final double cofqrq = reader.doubleField(2);

    // = 1 sortie détaillée des calculs (à éviter)
    // = 0 sinon
    final int isorti = reader.intField(3);

    // ILE : = 0 : La ligne d'eau initiale sera définie par des lignes du type E
    // = 1 : la ligne d'eau sera définie par des lignes du type F
    final int ile = reader.intField(4);

    // TREPJ
    // TREPH : temps de reprise dans le fichier FCB (en Jours/
    // TREPM Heures/Minutes/Secondes) (utilisés dans le cas ICAL = 0)
    // TREPS
    final Duration tempReprise = readDuration(reader, 5);
    if (tempReprise.getMillis() != 0) {
      analyze_.addSevereError("repriseDh.error");
    }

    // Enregistrement de "ical" et de "trepr" dans OCAL (seulement s'il vaut 0 ; aucun enregistrement metier pour une
    // valeur différente)
    final CrueData crueData = metier;
    // Création de IniCalcReprise avec une date de reprise
    if (ical == 0) {
      analyze_.addSevereError("repriseDh.error");
      return;
    }

    initialiseRuisInBranche(cofqrq, crueData);
    initialiseRuisInCasier(cofqrq, crueData);
    if (cofqrq > 0) {
      dhContainsRuis = true;
    }
//    // Enregistrement de isorti dans PCAL
    crueData.getOrCreateOCAL().getSorties().modifyVerbositeForAll(isorti == 0
        ? SeveriteManager.INFO
        : SeveriteManager.DEFAULT_DEBUG_LEVEL_FOR_CRUE9);
    // pour cree un PCAL par defaut.
    crueData.getOrCreatePCAL();

    final OrdPrtCIniModeleBase methodesInterpolation = createOpti(ile);
    crueData.setMethodesInterpolation(methodesInterpolation);

    // Pas d'enregistrement de "ile" qui sert pour l'algo uniquement
    // Remplissage des données métier en fonction de la valeur de "ical"
    // final CrueData metier = metier;
    if (ical == 0) {
      analyze_.addSevereError("repriseDh.error");
      return;
    } else if (ical == 1) {
      if (readDonneesGenerales(reader, metier, ile)) {
        readDonneesPermanentes(reader, metier, cru);
      }
    } else if (ical == 2) {
      if (readDonneesGenerales(reader, metier, ile)) {
        readDonneesPermanentes(reader, metier, cru);
        readDonneesTransitoires(reader, metier, cofqrq);
      }
    } else {
      analyze_.addSevereError("io.dh.ical.error", String.valueOf(ical));// a completer par la suite //$NON-NLS-1$
      logError("io.dh.ical.error " + ical, reader, null);
    }
  }

  private void initialiseRuisInBranche(final double cofqrq, final CrueData crueData) {
    final List<EMHBrancheSaintVenant> branches = crueData.getBranchesSaintVenant();
    for (final EMHBrancheSaintVenant branche : branches) {
      final List<DonCalcSansPrtBrancheSaintVenant> dcsps = EMHHelper.selectClass(branche.getDCSP(),
          DonCalcSansPrtBrancheSaintVenant.class);
      if (CollectionUtils.isEmpty(dcsps)) {
        final DonCalcSansPrtBrancheSaintVenant newInfosEMH = new DonCalcSansPrtBrancheSaintVenant(
            crueData.getCrueConfigMetier());
        newInfosEMH.setCoefRuisQdm(cru);
        branche.addInfosEMH(newInfosEMH);
      } else {
        final int dcspsSize = dcsps.size();
        if (dcspsSize > 1) {
          LOGGER.severe("Trop de DonCalcSansPrtBrancheSaintVenant definit par " + branche.getNom());
        }
        for (int j = 0; j < dcspsSize; j++) {
          final DonCalcSansPrtBrancheSaintVenant dcsp = dcsps.get(j);
          dcsp.setCoefRuisQdm(cru);
          dcsp.setCoefRuis(cofqrq * dcsp.getCoefRuis());
        }
      }
    }
  }

  private void initialiseRuisInCasier(final double cofqrq, final CrueData crueData) {
    final List<CatEMHCasier> casiers = crueData.getCasiers();
    for (final CatEMHCasier casier : casiers) {
      final DonCalcSansPrtCasierProfil dcsp = InfoEMHFactory.getDCSPCasierProfil(casier,
          crueData.getCrueConfigMetier());
      dcsp.setCoefRuis(cofqrq * dcsp.getCoefRuis());
    }
  }

  private CrueData readRegles(final CrueIOResu<CrueData> res, final CompositeReader reader) throws IOException {
    String carte = reader.stringField(0);
    // c'est une carte connue
    final CrueData metier = res.getMetier();
    tolndz = dataLinked.getCrueConfigMetier().getDefaultDoubleValue(
        StringUtils.uncapitalize(EnumRegleInterpolation.TOL_ND_Z.getVariableName()));

    while (CrueIODico.CARTE_DH.contains(carte)) {
      // la carte DH_R_REBDEB est ignoree
      if (carte.equals(CrueIODico.DH_COEFF_RELAX_DQ)) {
        getParamNumCalcPseudoPerm(metier).setCoefRelaxQ(
            CrueIODico.getCrue10Value(reader.doubleField(1), metier.getProperty("coefRelaxQ")));
      } else if (carte.equals(CrueIODico.DH_COEFF_RELAX_DZ)) {
        getParamNumCalcPseudoPerm(metier).setCoefRelaxZ(
            CrueIODico.getCrue10Value(reader.doubleField(1), metier.getProperty("coefRelaxZ")));
      } else if (carte.equals(CrueIODico.DH_NB_COURANT_MAX)) {
        if (reader.getNumberOfFields() > 1) {
          final double val = CrueIODico.getCrue10Value(reader.doubleField(1), metier.getProperty("crMaxFlu"));
          // la premiere valeur fait pour les 2
          getParamNumCalcPseudoPerm(metier).setCrMaxFlu(val);
          getParamNumCalcPseudoPerm(metier).setCrMaxTor(val);
          getParamNumCalcTrans(metier).setCrMaxFlu(val);
          getParamNumCalcTrans(metier).setCrMaxTor(val);
        }
        if (reader.getNumberOfFields() > 2) {
          final double val = CrueIODico.getCrue10Value(reader.doubleField(2), metier.getProperty("crMaxTor"));
          getParamNumCalcPseudoPerm(metier).setCrMaxTor(val);
          getParamNumCalcTrans(metier).setCrMaxTor(val);
        }
      } else if (carte.equals(CrueIODico.DH_SEUIL_FROUDE)) {
        if (reader.getNumberOfFields() > 1) {
          metier.getOrCreatePNUM().setFrLinInf(
              CrueIODico.getCrue10Value(reader.doubleField(1), metier.getProperty("frLinInf")));
        }
        if (reader.getNumberOfFields() > 2) {
          metier.getOrCreatePNUM().setFrLinSup(
              CrueIODico.getCrue10Value(reader.doubleField(2), metier.getProperty("frLinSup")));
        }
      } else if (carte.equals(CrueIODico.TOL_Z_INIT)) {
        tolndz = reader.doubleField(1);
      }

      reader.readLine();
      carte = reader.stringField(0);
    }
    return metier;
  }

  private ParamNumCalcPseudoPerm getParamNumCalcPseudoPerm(final CrueData metier) {
    final ParamNumModeleBase pnum = metier.getOrCreatePNUM();
    ParamNumCalcPseudoPerm res = pnum.getParamNumCalcPseudoPerm();
    if (res == null) {
      res = new ParamNumCalcPseudoPerm(metier.getCrueConfigMetier());
      pnum.setParamNumCalcPseudoPerm(res);
    }
    return res;
  }

  private ParamNumCalcTrans getParamNumCalcTrans(final CrueData metier) {
    final ParamNumModeleBase pnum = metier.getOrCreatePNUM();
    ParamNumCalcTrans res = pnum.getParamNumCalcTrans();
    if (res == null) {
      res = new ParamNumCalcTrans(metier.getCrueConfigMetier());
      pnum.setParamNumCalcTrans(res);
    }
    return res;
  }

  private OrdPrtCIniModeleBase createOpti(final int ile) {
    final OrdPrtCIniModeleBase res = dataLinked.getOrCreateOPTI();
    // <!-- cas DH.iLE=1 -->
    // <InterpolLineaire />
    // <!--
    // XOR, si DH.iLE=0, une interpolation par cotes imposées aux sections
    // <InterpolZimpAuxSections/>
    // -->
    // <!--
    // XOR, si DH.iLE=2, une interpolation des cotes par calcul de la courbe
    // de remous en permanent <InterpolSaintVenant>
    // <Pm_TolNdZ>0.01</Pm_TolNdZ> </InterpolSaintVenant>
    // -->
    // <!--
    // XOR, si DH.iLE=3, une interpolation à cotes constantes type baignoire
    // <InterpolBaignoire/>
    // -->
    EnumMethodeInterpol enumInterpol = EnumMethodeInterpol.INTERPOL_ZIMP_AUX_SECTIONS;
    if (ile == 1) {
      enumInterpol = EnumMethodeInterpol.LINEAIRE;
    } else if (ile == 2) {
      enumInterpol = EnumMethodeInterpol.SAINT_VENANT;
      res.addValParam(new ValParamDouble(EnumRegleInterpolation.TOL_ND_Z.getVariableName(), tolndz));
      double tolstq = dataLinked.getCrueConfigMetier().getDefaultDoubleValue(EnumRegleInterpolation.TOL_ST_Q.getVariableName());
      res.addValParam(new ValParamDouble(EnumRegleInterpolation.TOL_ST_Q.getVariableName(), tolstq));
    } else if (ile == 3) {
      enumInterpol = EnumMethodeInterpol.BAIGNOIRE;
    }
    res.setMethodeInterpol(enumInterpol);

    return res;
  }
}
