/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import java.io.File;
import java.net.MalformedURLException;
import java.util.EnumMap;
import java.util.Map;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.xml.XmlVersionFinder;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * Gere la lecture du format crue 10 Utilise du parsing xml
 *
 * @author Adrien Hadoux
 */
public final class Crue10FileFormatFactory {
  private final Map<CrueFileType, CrueFileFormatBuilder> fnts = new EnumMap<>(CrueFileType.class);
  private static final Crue10FileFormatFactory INSTANCE = new Crue10FileFormatFactory();

  /**
   * @return le singleton
   */
  public static final Crue10FileFormatFactory getInstance() {
    return INSTANCE;
  }

  public static final <T> Crue10FileFormat<T> getVersion(final CrueFileType fileType, final CoeurConfigContrat version) {
    return INSTANCE.getFileFormat(fileType, version);
  }

  private Crue10FileFormatFactory() {
    fnts.put(CrueFileType.DCSP, new CrueFileFormatBuilderDCSP());
    fnts.put(CrueFileType.DFRT, new CrueFileFormatBuilderDFRT());
    fnts.put(CrueFileType.DLHY, new CrueFileFormatBuilderDLHY());
    fnts.put(CrueFileType.DPTG, new CrueFileFormatBuilderDPTG());
    fnts.put(CrueFileType.DPTI, new CrueFileFormatBuilderDPTI());
    fnts.put(CrueFileType.DRSO, new CrueFileFormatBuilderDRSO());
    fnts.put(CrueFileType.ETU, new CrueFileFormatBuilderETU());
    fnts.put(CrueFileType.OPTG, new CrueFileFormatBuilderOPTG());
    fnts.put(CrueFileType.DCLM, new CrueFileFormatBuilderDCLM());
    fnts.put(CrueFileType.ORES, new CrueFileFormatBuilderORES());
    fnts.put(CrueFileType.OCAL, new CrueFileFormatBuilderOCAL());
    fnts.put(CrueFileType.PCAL, new CrueFileFormatBuilderPCAL());
    fnts.put(CrueFileType.PNUM, new CrueFileFormatBuilderPNUM());
    fnts.put(CrueFileType.OPTI, new CrueFileFormatBuilderOPTI());
    fnts.put(CrueFileType.OPTR, new CrueFileFormatBuilderOPTR());
    fnts.put(CrueFileType.RCAL, new CrueFileFormatBuilderRCAL());
    fnts.put(CrueFileType.RPTG, new CrueFileFormatBuilderRPTG());
    fnts.put(CrueFileType.RPTI, new CrueFileFormatBuilderRPTI());
    fnts.put(CrueFileType.RPTR, new CrueFileFormatBuilderRPTR());
    fnts.put(CrueFileType.LHPT, new CrueFileFormatBuilderLHPT());
  }

  /**
   * @param type le type voulu
   * @param coeurConfig la version voulue.
   * @return le fileformat correspondant
   */
  @SuppressWarnings("unchecked")
  public <T> Crue10FileFormat<T> getFileFormat(final CrueFileType type, final CoeurConfigContrat coeurConfig) {
    CrueFileFormatBuilder crueFileFormatBuilder = fnts.get(type);
    if(crueFileFormatBuilder==null){
      return null;
    }
    return crueFileFormatBuilder.getFileFormat(coeurConfig);
  }

  public static class FileFormatResult<T> {

    public CtuluLog log;
    public Crue10FileFormat<T> fileFormatFound;
  }

  public static class VersionResult {

    private final CtuluLog log;
    private final String version;

    /**
     * @param log
     * @param version
     */
    public VersionResult(final CtuluLog log, final String version) {
      super();
      this.log = log;
      this.version = version;
    }

    /**
     * @return the log
     */
    public CtuluLog getLog() {
      return log;
    }

    /**
     * @return the version
     */
    public String getVersion() {
      return version;
    }
  }

  /**
   *
   * @param file
   * @return la version. Teste si la version est supportée par Fudaa-Crue.
   */
  public static VersionResult findVersion(final File file) {
    final XmlVersionFinder finder = new XmlVersionFinder();

    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    String version = null;
    try {
      version = finder.getVersion(file.toURI().toURL());
    } catch (final MalformedURLException e) {
      log.addErrorThrown(BusinessMessages.getString("io.find.version"), e);
    }
    log.setDesc("io.find.version");
    log.setDescriptionArgs(file.getName());
    if (version == null) {
      log.addSevereError("io.version.notFound");
    }
    if (!Crue10VersionConfig.isSupported(version)) {
      log.addSevereError("io.version.notSupported", version);
      version = null;
    }
    return new VersionResult(log, version);
  }

}
