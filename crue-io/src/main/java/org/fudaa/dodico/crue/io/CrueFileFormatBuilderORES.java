/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.ores.CrueConverterORES;
import org.fudaa.dodico.crue.io.ores.CrueConverterORES_V1P1P1;
import org.fudaa.dodico.crue.io.ores.CrueDaoStructureORES;
import org.fudaa.dodico.crue.io.ores.CrueDaoStructureORES_V1P1P1;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.OrdResScenario;

public class CrueFileFormatBuilderORES implements CrueFileFormatBuilder<OrdResScenario> {

  @Override
  public Crue10FileFormat<OrdResScenario> getFileFormat(final CoeurConfigContrat version) {
    if (Crue10VersionConfig.V_1_1_1.equals(version.getXsdVersion())) {
      return new Crue10FileFormat<>(new CrueDataXmlReaderWriterImpl<>(
          CrueFileType.ORES, version, new CrueConverterORES_V1P1P1(), new CrueDaoStructureORES_V1P1P1()));
    }
    return new Crue10FileFormat<>(new CrueDataXmlReaderWriterImpl<>(
        CrueFileType.ORES, version, new CrueConverterORES(), new CrueDaoStructureORES(version)));

  }

}
