/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.common;

import com.thoughtworks.xstream.converters.SingleValueConverter;
import org.fudaa.ctulu.CtuluLog;

/**
 * @author deniger
 */
public abstract class AbstractSingleConverter implements SingleValueConverter {
  protected CtuluLog analyse;

  /**
   *
   */
  public AbstractSingleConverter() {
    super();
  }

  /**
   * @return the analyse
   */
  public CtuluLog getAnalyse() {
    return analyse;
  }

  /**
   * @param code code erreur
   * @param argument pour i18n
   */
  protected void addFatalError(final String code, final Object argument) {
    if (analyse != null) {
      analyse.addSevereError(code, argument);
    }
  }

  /**
   * @param analyse the analyse to set
   */
  public final void setAnalyse(final CtuluLog analyse) {
    this.analyse = analyse;
  }
}
