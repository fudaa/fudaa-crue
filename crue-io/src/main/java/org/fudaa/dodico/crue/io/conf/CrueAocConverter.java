/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.conf;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueConverter;

/**
 * @author deniger
 */
public class CrueAocConverter implements CrueConverter<CrueAocDaoConfiguration, CrueAocDaoConfiguration> {
    @Override
    public CrueAocDaoConfiguration convertDaoToMetier(final CrueAocDaoConfiguration dao, final CtuluLog ctuluLog) {
        return dao;
    }

    @Override
    public CrueAocDaoConfiguration convertMetierToDao(final CrueAocDaoConfiguration metier, final CtuluLog ctuluLog) {
        return metier;
    }
}
