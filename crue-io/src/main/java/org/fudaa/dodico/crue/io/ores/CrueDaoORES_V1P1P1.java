/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores;


import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResBranchesOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResCasiersOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResNoeudsOld;
import org.fudaa.dodico.crue.io.ores.old.DaoOrdResSectionsOld;

/**
 * Classe persistante qui reprend la meme structure que le fichier XML ORES - Fichier des donnees portant sur les demandes de resultats
 * supplementaires d'un modele crue10. A persister tel quel.
 *
 * @author cde
 */
public class CrueDaoORES_V1P1P1 extends AbstractCrueDao {

  /**
   * Représente la balise portant le même nom dans le fichier XML
   */
  public DaoOrdResNoeudsOld OrdResNoeuds;
  /**
   * Représente la balise portant le même nom dans le fichier XML
   */
  public DaoOrdResCasiersOld OrdResCasiers;
  /**
   * Représente la balise portant le même nom dans le fichier XML
   */
  public DaoOrdResSectionsOld OrdResSections;
  /**
   * Représente la balise portant le même nom dans le fichier XML
   */
  public DaoOrdResBranchesOld OrdResBranches;
}
