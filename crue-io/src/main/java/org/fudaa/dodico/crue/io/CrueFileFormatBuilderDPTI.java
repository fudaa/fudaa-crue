/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.dpti.CrueConverterDPTI;
import org.fudaa.dodico.crue.io.dpti.CrueDaoDPTI;
import org.fudaa.dodico.crue.io.dpti.CrueDaoStructureDPTI;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;

public class CrueFileFormatBuilderDPTI implements CrueFileFormatBuilder<CrueData> {

  @Override
  public Crue10FileFormat<CrueData> getFileFormat(final CoeurConfigContrat coeurConfig) {
    return new Crue10FileFormat<>(createReaderWriter(coeurConfig));

  }

  public CrueDataXmlReaderWriterImpl<CrueDaoDPTI, CrueData> createReaderWriter(final CoeurConfigContrat coeurConfig) {
    return new CrueDataXmlReaderWriterImpl<>(CrueFileType.DPTI,coeurConfig,
        new CrueConverterDPTI(), new CrueDaoStructureDPTI());
  }
}
