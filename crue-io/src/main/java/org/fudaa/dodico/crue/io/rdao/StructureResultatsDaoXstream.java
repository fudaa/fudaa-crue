/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import com.thoughtworks.xstream.XStream;
import java.util.List;

/**
 *
 * @author deniger
 */
public class StructureResultatsDaoXstream {

  public static final String MODELE_REGUL = "ModeleRegul";

  public static void configureXstream(XStream xstream) {
    xstream.alias("StructureResultat", StructureResultatsDao.class);
    xstream.addImplicitCollection(StructureResultatsDao.class, "VariableRes", CommonResDao.VariableResDao.class);
    NoeudsDaoXstream.configureXstream(xstream);
    CasiersDaoXstream.configureXstream(xstream);
    SectionsDaoXstream.configureXstream(xstream);
    BranchesDaoXstream.configureXstream(xstream);
    ModelesDaoXstream.configureXstream(xstream);
  }

  public static void processVariablesAndTypes(ResContainerDao container) {
    final StructureResultatsDao structureResultat = container.getStructureResultat();
    NoeudsDao noeuds = structureResultat.getNoeuds();
    if (noeuds != null) {
      setType(noeuds.getNoeudNiveauContinu(), "NoeudNiveauContinu");
    }
    BranchesDao branches = structureResultat.getBranches();
    if (branches != null) {
      setType(branches.getBrancheBarrageFilEau(), "BrancheBarrageFilEau");
      setType(branches.getBrancheBarrageGenerique(), "BrancheBarrageGenerique");
      setType(branches.getBrancheNiveauxAssocies(), "BrancheNiveauxAssocies");
      setType(branches.getBrancheOrifice(), "BrancheOrifice");
      setType(branches.getBranchePdc(), "BranchePdc");
      setType(branches.getBrancheSaintVenant(), "BrancheSaintVenant");
      setType(branches.getBrancheSeuilLateral(), "BrancheSeuilLateral");
      setType(branches.getBrancheSeuilTransversal(), "BrancheSeuilTransversal");
      setType(branches.getBrancheStrickler(), "BrancheStrickler");
    }
    CasiersDao casiers = structureResultat.getCasiers();
    if (casiers != null) {
      setType(casiers.getCasierProfil(), "CasierProfil");
    }
    SectionsDao sections = structureResultat.getSections();
    if (sections != null) {
      setType(sections.getSectionIdem(), "SectionIdem");
      setType(sections.getSectionInterpolee(), "SectionInterpolee");
      setType(sections.getSectionProfil(), "SectionProfil");
      setType(sections.getSectionSansGeometrie(), "SectionSansGeometrie");
    }
    ModelesDao modeles= structureResultat.getModeles();
    if (modeles != null) {
      setType(modeles.getModeleRegul(), MODELE_REGUL);
    }
    processVariables(structureResultat.getVariableRes());
    for (ResContainerEMHCat cat : structureResultat.getResContainerEMHCat()) {
      processVariables(cat.getVariableRes());
      for (ResContainerEMHType type : cat.getResContainerEMHType()) {
        processVariables(type.getVariableRes());
      }

    }
  }

  public static void setType(CommonResDao.TypeEMHDao type, String nom) {
    if (type != null) {
      type.setTypeEMH(nom);
    }
  }

  public static void processVariables(List<CommonResDao.VariableResDao> variableRes) {
    if (variableRes == null) {
      return;
    }
    for (CommonResDao.VariableResDao variableResDao : variableRes) {
      variableResDao.updateId();

    }
  }
}
