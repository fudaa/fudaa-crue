/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.res;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.fudaa.dodico.crue.io.rdao.CommonResDao;

/**
 *
 * @author deniger
 */
public class ResVariablesAnalyser {

  public static class Result {

    public final ResVariablesContent content;
    public final Set<String> varEnDoublon;

    public Result(ResVariablesContent content, Set<String> varEnDoublon) {
      this.content = content;
      this.varEnDoublon = varEnDoublon;
    }
  }

  public Result extractResult(List<CommonResDao.VariableResDao> in) {
    if (in == null) {
      return new Result(new ResVariablesContent(), Collections.emptySet());
    }
    ResVariablesContent res = new ResVariablesContent();
    Set<String> doublons = new HashSet<>();
    for (CommonResDao.VariableResDao variableResDao : in) {
      if (variableResDao.isLoiLoiNbrPtDao()) {
        res.loiNbrPtById.put(variableResDao.getId(), ((CommonResDao.VariableResLoiNbrPtDao) variableResDao).getNbrPt());
      } else {
        if (res.isVariableDefined(variableResDao.getId())) {
          doublons.add(variableResDao.getNomRef());
        }
        res.addVariables(variableResDao);
      }
    }
    return new Result(res, doublons);
  }

  public Result mergeWithParent(List<CommonResDao.VariableResDao> in, ResVariablesContent parent) {
    Result current = extractResult(in);
    if (parent == null) {
      return current;
    }
    ResVariablesContent clonedOfParent = parent.clone();
    Set<String> doublons = new HashSet<>(current.varEnDoublon);
    doublons.addAll(clonedOfParent.addAll(current.content));
    return new Result(clonedOfParent, doublons);
  }
}
