package org.fudaa.dodico.crue.io.dlhy;

import org.fudaa.dodico.crue.io.common.AbstractDaoFloatLoi;
import org.fudaa.dodico.crue.io.common.AbstractDaoLoi;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.LoiFF;

/**
 * @author deniger
 */
public final class ConvertLoiFF implements ConvertLoi {

  @Override
  public AbstractDaoLoi metiertoDao(final AbstractLoi in) {
    final AbstractDaoFloatLoi.DaoLoiFF outLoi = new AbstractDaoFloatLoi.DaoLoiFF();
    AbstractDaoFloatLoi.metierToDaoLoi(outLoi, (Loi)in);
    return outLoi;
  }

  @Override
  public Loi daoToMetier(final AbstractDaoLoi in) {
    final LoiFF loiFF = new LoiFF();
    AbstractDaoFloatLoi.daoToMetierLoi(loiFF, (AbstractDaoFloatLoi)in);
    return loiFF;
  }
}
