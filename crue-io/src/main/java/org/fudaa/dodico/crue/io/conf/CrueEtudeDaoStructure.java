/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.conf;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueDaoStructure;

/**
 * @author CANEL Christophe
 */
public class CrueEtudeDaoStructure implements CrueDaoStructure {

  private final CrueDaoStructure courbeConfigurer;

  public CrueEtudeDaoStructure(final CrueDaoStructure courbeConfigurer) {
    this.courbeConfigurer = courbeConfigurer;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void configureXStream(final XStream xstream, final CtuluLog analyze) {
    xstream.alias("ConfigEtude", CrueEtudeDaoConfiguration.class);
    xstream.omitField(CrueEtudeDaoConfiguration.class, "Commentaire");
    xstream.alias("PlanimetrieConfiguration", BlocConfiguration.class);
    xstream.alias("GlobalConfiguration", BlocConfiguration.class);
    //ancien nom à ne plus utiliser
    xstream.omitField(CrueEtudeDaoConfiguration.class, "LoiConfiguration");
    xstream.alias("LoiConfigurations", CourbeConfigurations.class);
    xstream.addImplicitCollection(BlocConfiguration.class, "Options");
    this.configureEtudeOptionXStream(xstream);
    if (courbeConfigurer != null) {
      courbeConfigurer.configureXStream(xstream, analyze);
    }
  }

  public void configureEtudeOptionXStream(final XStream xstream) {
    xstream.alias("EtudeOption", Option.class);
    xstream.useAttributeFor(Option.class, "Nom");
  }
}
