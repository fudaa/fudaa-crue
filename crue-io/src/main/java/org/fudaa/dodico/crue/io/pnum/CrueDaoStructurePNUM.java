/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.pnum;

import com.thoughtworks.xstream.XStream;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * Structures utilisées dans la classe CrueDaoPNUM
 * 
 * @author CDE
 */
@SuppressWarnings("PMD.VariableNamingConventions")
public class CrueDaoStructurePNUM implements CrueDataDaoStructure {

  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {

    xstream.alias(CrueFileType.PNUM.toString(), CrueDaoPNUM.class);
    xstream.alias("Pdt", DaoPdt.class);
    xstream.alias("ElemPdt", DaoElemPdt.class);
    xstream.alias("ParamNumCommuns", DaoParamNumCommuns.class);
    xstream.alias("ParamNumCalcPseudoPerm", DaoParamNumCalcPseudoPerm.class);
    xstream.alias("ParamNumCalcTrans", DaoParamNumCalcTrans.class);
  }

  /**
   * Classe abstraite contenant les propriétés communes de PdtPermPersist et de PdtTransPersist
   * 
   * @author CDE
   */
  public static class DaoPdt {

    /** Représente la valeur de la balise portant le même nom dans le fichier XML */
    protected String PdtCst;
    /**
     * Cas des pas de temps variable
     */
    protected List<DaoElemPdt> PdtVar;

  }

  /**
   * Représente la valeur de la balise ElemPdt dans le fichier XML
   * 
   * @author CDE
   */
  public static class DaoElemPdt {

    /** Représente la valeur de la balise portant le même nom dans le fichier XML */
    protected int NbrPdt;
    /** Représente la valeur de la balise portant le même nom dans le fichier XML */
    protected String DureePdt;
  }

  /**
   * @author deniger
   */
  protected static class DaoParamNumCommuns {
    protected double FrLinInf;
    protected double FrLinSup;
    protected double Zref;
  }

  /**
   * @author deniger
   */
  protected static class DaoParamNumCalcPseudoPerm {
    protected double CoefRelaxQ;
    protected double CoefRelaxZ;
    protected double CrMaxFlu;
    protected double CrMaxTor;
    protected int NbrPdtDecoup;
    protected int NbrPdtMax;
    protected DaoPdt Pdt;
    protected double TolMaxQ;
    protected double TolMaxZ;
  }

  /**
   * @author deniger
   */
  public static class DaoParamNumCalcTrans {
    protected double CrMaxFlu;
    protected double CrMaxTor;
    protected DaoPdt Pdt;
    protected double ThetaPreissmann;
  }

}
