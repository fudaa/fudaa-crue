/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.conf;

import org.fudaa.dodico.crue.common.io.AbstractCrueDao;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Les configurations liées à AOC.
 *
 * @author deniger
 */
public class CrueAocDaoConfiguration extends AbstractCrueDao {
    BlocConfiguration GlobalConfiguration = new BlocConfiguration();
    CourbeConfigurations LoiConfigurations = new CourbeConfigurations();

    public Map<String, ?> getCourbeConfiguration() {
        Map<String, ?> res = null;
        if (getLoiConfigurationOptions() != null) {
            res = getLoiConfigurationOptions().getCourbeConfiguration();
        }
        if (res == null) {
            res = Collections.emptyMap();
        }
        return res;
    }

    public BlocConfiguration getGlobalConfiguration() {
        return GlobalConfiguration;
    }

    public void setGlobalConfiguration(final BlocConfiguration GlobalConfiguration) {
        this.GlobalConfiguration = GlobalConfiguration;
    }

    public List<Option> getGlobalConfigurationOptions() {
        return GlobalConfiguration == null ? null
                : GlobalConfiguration.getOptions();
    }

    public CourbeConfigurations getLoiConfigurationOptions() {
        return LoiConfigurations;
    }

    public void setLoiConfiguration(final CourbeConfigurations LoiConfiguration) {
        this.LoiConfigurations = LoiConfiguration;
    }

    public void initLoiConfiguration(final Map<String, Object> courbeConfiguration) {
        this.LoiConfigurations = new CourbeConfigurations();
        if (courbeConfiguration != null) {
            this.LoiConfigurations.CourbeConfiguration = new HashMap<>(courbeConfiguration);
        }
    }
}
