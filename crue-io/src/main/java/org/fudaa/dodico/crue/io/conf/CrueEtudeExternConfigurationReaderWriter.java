/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.conf;

import org.fudaa.dodico.crue.common.io.CrueXmlReaderWriterImpl;

/**
 * @author CANEL Christophe
 *
 */
public class CrueEtudeExternConfigurationReaderWriter extends CrueXmlReaderWriterImpl<CrueEtudeExternDaoConfiguration, CrueEtudeExternDaoConfiguration> {

  public CrueEtudeExternConfigurationReaderWriter(final String version) {
    super("etude_extern", version, new CrueEtudeExternConverter(), new CrueEtudeExternDaoStructure());
  }
}
