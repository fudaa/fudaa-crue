/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ocal;

import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.ocal.CrueDaoStructureOCAL.OrdCalculAbstractPersist;
import org.fudaa.dodico.crue.metier.emh.Sorties;

/**
 * Représentation persistante du fichier xml OCAL (Ordres de calculs).
 * 
 * @author CDE
 */
public class CrueDaoOCAL extends AbstractCrueDao {
  
  public Sorties Sorties;

  /** Contient la liste des ordres de calculs permanents et transitoires **/
  public List<OrdCalculAbstractPersist> listeOrdCalculs;

}
