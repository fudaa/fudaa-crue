/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dfrt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.io.common.AbstractDaoFloatLoi;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.common.AbstractDaoFloatLoi.DaoLoiFF;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.DonFrtManning;
import org.fudaa.dodico.crue.metier.emh.DonFrtStrickler;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.factory.EMHFactory;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;

/**
 * Factory qui se charge de remplir les structures DAO dufichier DFRT avec les donnees metier et inversement.
 *
 * @author Adrien Hadoux
 */
public class CrueConverterDFRT implements CrueDataConverter<CrueDaoDFRT, List<DonFrt>> {

  private Map<EnumTypeLoi, ConvertStriRef> createConverterMap() {
    final Map<EnumTypeLoi, ConvertStriRef> converterByPrefix = new HashMap<>();
    converterByPrefix.put(EnumTypeLoi.LoiZFK, new ConvertLoiZfk());
    converterByPrefix.put(EnumTypeLoi.LoiZFN, new ConvertLoiZfn());
    converterByPrefix.put(EnumTypeLoi.LoiZFKSto, new ConvertLoiZfkSto());
    return converterByPrefix;
  }

  private interface ConvertStriRef {

    DonFrt toMetier(DaoLoiFF in);

    DaoLoiFF toDao(DonFrt in);
  }

  private static class ConvertLoiZfk implements ConvertStriRef {

    @Override
    public DaoLoiFF toDao(final DonFrt inMetier) {
      final DonFrtStrickler stri = (DonFrtStrickler) inMetier;
      final DaoLoiFF res = new DaoLoiFF();
      AbstractDaoFloatLoi.metierToDaoLoi(res, stri.getLoi());
      return res;
    }

    @Override
    public DonFrt toMetier(final DaoLoiFF inDao) {
      final DonFrtStrickler outMetier = EMHFactory.createDonFrtZFk(inDao.Nom);
      AbstractDaoFloatLoi.daoToMetierLoi(outMetier.getLoi(), inDao);
      return outMetier;
    }
  }

  private static class ConvertLoiZfkSto implements ConvertStriRef {

    @Override
    public DaoLoiFF toDao(final DonFrt inMetier) {
      final DonFrtStrickler stri = (DonFrtStrickler) inMetier;
      final DaoLoiFF res = new DaoLoiFF();
      AbstractDaoFloatLoi.metierToDaoLoi(res, stri.getLoi());
      return res;
    }

    @Override
    public DonFrt toMetier(final DaoLoiFF inDao) {
      final DonFrtStrickler outMetier = EMHFactory.createDonFrtZFkSto(inDao.Nom);
      AbstractDaoFloatLoi.daoToMetierLoi(outMetier.getLoi(), inDao);
      return outMetier;
    }
  }

  private static class ConvertLoiZfn implements ConvertStriRef {

    @Override
    public DaoLoiFF toDao(final DonFrt inMetier) {
      final DonFrtManning stri = (DonFrtManning) inMetier;
      final DaoLoiFF res = new DaoLoiFF();
      AbstractDaoFloatLoi.metierToDaoLoi(res, stri.getLoi());
      return res;
    }

    @Override
    public DonFrt toMetier(final DaoLoiFF inDao) {
      final DonFrtManning outMetier = EMHFactory.createDonFrtZFn(inDao.Nom);
      AbstractDaoFloatLoi.daoToMetierLoi(outMetier.getLoi(), inDao);
      return outMetier;
    }
  }

  @Override
  public List<DonFrt> convertDaoToMetier(final CrueDaoDFRT dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
    final List<DaoLoiFF> persistListe = dao.LoiFFs;
    final List<DonFrt> out = new ArrayList<>(persistListe.size());
    final Map<EnumTypeLoi, ConvertStriRef> converterByPrefix = createConverterMap();
    if (CollectionUtils.isNotEmpty(persistListe)) {
      for (final DaoLoiFF in : persistListe) {
        final ConvertStriRef converter = converterByPrefix.get(in.getType());
        if (converter == null) {
          ctuluLog.addSevereError("io.dfrt.frtUnknown", in.Nom);
          return out;
        }
        final DonFrt metier = converter.toMetier(in);
        final Loi loi = metier.getLoi();
        LoiHelper.validLoiNom(ctuluLog, loi);
        out.add(metier);

      }
    }
    if (dataLinked != null) {
      dataLinked.setFrottements(out);
    }
    return out;
  }

  @Override
  public List<DonFrt> getConverterData(final CrueData in) {
    return in.getFrottements().getListFrt();
  }

  @Override
  public CrueDaoDFRT convertMetierToDao(final List<DonFrt> metier, final CtuluLog ctuluLog) {
    final CrueDaoDFRT res = new CrueDaoDFRT();
    res.LoiFFs = new ArrayList<>(metier.size());
    final Map<EnumTypeLoi, ConvertStriRef> converterByPrefix = createConverterMap();
    for (final DonFrt in : metier) {
      final ConvertStriRef converter = converterByPrefix.get(in.getLoi().getType());
      if (converter == null) {
        ctuluLog.addSevereError("io.dfrt.frtUnknown", in.getNom());
        return res;
      }
      res.LoiFFs.add(converter.toDao(in));
    }
    return res;
  }
}
