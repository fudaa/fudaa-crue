/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores.old;

public class DaoOrdResBrancheNiveauxAssociesOld {

  private boolean ddeDz;

  public boolean getDdeDz() {
    return ddeDz;
  }

  /**
   * @param ddeDz the ddeDz to set
   */
  public void setDdeDz(final boolean ddeDz) {
    this.ddeDz = ddeDz;
  }

  @Override
  public String toString() {
    return "OrdResBrancheBarrageGenerique [ddeDz=" + ddeDz + "]";
  }
}
