/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores.old;

/**
 * pour la version 1.1.1 uniquement pour anciennes xsd.
 *
 * @author Frederic Deniger
 */
public class DaoOrdResBrancheBarrageFilEauOld {

  private boolean ddeRegimeBarrage;

  public boolean getDdeRegimeBarrage() {
    return ddeRegimeBarrage;
  }

  /**
   * @param ddeRegimeBarrage the ddeRegimeBarrage to set
   */
  public void setDdeRegimeBarrage(final boolean ddeRegimeBarrage) {
    this.ddeRegimeBarrage = ddeRegimeBarrage;
  }

  @Override
  public String toString() {
    return "OrdResBrancheBarrageFilEau [ddeRegime=" + ddeRegimeBarrage + "]";
  }
}
