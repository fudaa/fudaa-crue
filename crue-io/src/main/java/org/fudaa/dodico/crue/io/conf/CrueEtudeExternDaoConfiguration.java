/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.conf;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;

/**
 *
 * @author deniger
 */
public class CrueEtudeExternDaoConfiguration extends AbstractCrueDao {
  List<CrueEtudeExternRessourceInfos> Ressources = new ArrayList<>();

  public List<CrueEtudeExternRessourceInfos> getRessources() {
    return Ressources;
  }

  public void setRessources(final List<CrueEtudeExternRessourceInfos> resources) {
    this.Ressources = resources;
  }
}
