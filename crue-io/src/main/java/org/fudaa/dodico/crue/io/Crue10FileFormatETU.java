/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.io.common.CrueXmlReaderWriter;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;

import java.io.File;

/**
 *
 * @author deniger
 */
public class Crue10FileFormatETU extends Crue10FileFormat<EMHProjet> {

  public static EMHProjet readData(final File fichier, final CoeurConfigContrat version) {
    final CtuluLog analyzer = new CtuluLog();
    final Crue10FileFormat<Object> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ETU, version);
    final CrueIOResu<Object> readIO = fileFormat.read(fichier, analyzer,new CrueDataImpl(version.getCrueConfigMetier()));
    final EMHProjet jeuDonneesLue = (EMHProjet) readIO.getMetier();
    jeuDonneesLue.setPropDefinition(version);
    return jeuDonneesLue;
  }


  public Crue10FileFormatETU(final CrueXmlReaderWriter<EMHProjet> readerWriter) {
    super(readerWriter);
  }

  /**
   * Utilise pour iniatiliser le répertoire parent.
   */
  @Override
  protected void decoreResult(final File file, final CrueIOResu<EMHProjet> emhProjetCrueIOResu) {
    if (emhProjetCrueIOResu.getMetier() != null) {
      emhProjetCrueIOResu.getMetier().setEtuFile(file);
    }


  }
}
