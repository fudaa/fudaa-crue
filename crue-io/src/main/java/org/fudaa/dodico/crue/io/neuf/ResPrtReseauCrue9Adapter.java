/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.ResPrtGeo;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;

/**
 * @author deniger
 */
public class ResPrtReseauCrue9Adapter {

  private final Map<String, ResPrtGeo> resSto;
  private final List<String> listCasier;
  private STRSequentialReader reader;

  /**
   * @return the reader
   */
  public STRSequentialReader getReader() {
    return reader;
  }

  /**
   * @param reader the reader to set
   */
  public void setReader(final STRSequentialReader reader) {
    this.reader = reader;
  }

  /**
   * @param readSto les resultat issus de sto. Les cles sont les noms Crue9.
   */
  public ResPrtReseauCrue9Adapter(final Map<String, Crue9ResPrtGeoCasier> readSto) {
    this.resSto = new HashMap<>(readSto.size());
    final List tmplistCasier = new ArrayList<String>();
    for (final Map.Entry<String, Crue9ResPrtGeoCasier> it : readSto.entrySet()) {
      final String nomCasier = CruePrefix.changePrefix(it.getKey(), CruePrefix.getPrefix(EnumCatEMH.NOEUD),
              CruePrefix.getPrefix(EnumCatEMH.CASIER));
      tmplistCasier.add(nomCasier);
      ResPrtGeo rptg = new ResPrtGeo(it.getValue().getValues(), nomCasier, true, -1,false);
      resSto.put(nomCasier, rptg);
    }
    listCasier = Collections.unmodifiableList(tmplistCasier);

  }

  public int getNbResOnCasier() {
    return resSto.size();
  }

  public int getNbResOnProfil() {
    return reader.getNbProfil();
  }

  public String getCasierNom(final int idx) {
    return listCasier.get(idx);
  }

  public String getProfilNom(final int idx) {
    return reader.getProfilName(idx);
  }

  /**
   * le nomProfl est le nom lu dans le fichier str *
   */
  public ResPrtGeo getResultatOnCasier(final String nomCasier) {
    return resSto.get(nomCasier);
  }

  /**
   * le nomProfl est le nom lu dans le fichier str *
   */
  public ResPrtGeo getResultatOnProfil(final String nomProfil) {
    return reader.getResultatOnProfil(nomProfil);
  }

  public double getCoefSinuo(String idCrue10) {
    return reader.getCoefSinuo(idCrue10);
  }
  
  
  
  
  
}
