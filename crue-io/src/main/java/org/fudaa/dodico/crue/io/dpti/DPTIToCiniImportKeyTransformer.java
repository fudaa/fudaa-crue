package org.fudaa.dodico.crue.io.dpti;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.CrueFileFormatBuilderDPTI;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.cini.CiniImportKey;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Permet de transfomer les données d'un fichier DPTI en CiniImportKey utilisable pour modifier simplement des conditions initiales ( depuis l'UI).
 *
 * @author deniger
 */
public class DPTIToCiniImportKeyTransformer {
  /**
   * Parcourt toutes les données d'un fichier DPTI pour extraire les données.
   *
   * @param daoDPTI les données à importer.
   * @return les valeurs
   */
  public Map<CiniImportKey, Object> extract(CrueDaoDPTI daoDPTI) {
    Map<CiniImportKey, Object> res = new HashMap<>();
    if (daoDPTI.DonPrtCIniNoeuds != null) {
      daoDPTI.DonPrtCIniNoeuds.forEach(cini -> add(res, cini));
    }
    if (daoDPTI.DonPrtCIniBranches != null) {
      daoDPTI.DonPrtCIniBranches.forEach(cini -> add(res, cini));
    }
    if (daoDPTI.DonPrtCIniCasiers != null) {
      daoDPTI.DonPrtCIniCasiers.forEach(cini -> add(res, cini));
    }
    if (daoDPTI.DonPrtCIniSections != null) {
      daoDPTI.DonPrtCIniSections.forEach(cini -> add(res, cini));
    }
    return res;
  }

  private void add(Map<CiniImportKey, Object> res, CrueDaoStructureDPTI.NoeudNiveauContinu cini) {
    cini.fill(res);
  }

  private void add(Map<CiniImportKey, Object> res, CrueDaoStructureDPTI.BrancheAbstract cini) {
    cini.fill(res);
  }

  private void add(Map<CiniImportKey, Object> res, CrueDaoStructureDPTI.CasierAbstract cini) {
    cini.fill(res);
  }

  private void add(Map<CiniImportKey, Object> res, CrueDaoStructureDPTI.SectionAbstract cini) {
    cini.fill(res);
  }

  /**
   * Methode utilitaire pour lire un fichier
   *
   * @param dptiFile le fichier DPTI a lire
   * @param coeurConfigContrat le coeur
   * @return les données transformées.
   */
  public static CrueIOResu<Map<CiniImportKey, Object>> read(File dptiFile, CoeurConfigContrat coeurConfigContrat) {
    CrueIOResu<Map<CiniImportKey, Object>> resu = new CrueIOResu<>();
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    resu.setAnalyse(log);
    final CrueDataXmlReaderWriterImpl<CrueDaoDPTI, CrueData> readerWriter = new CrueFileFormatBuilderDPTI().createReaderWriter(coeurConfigContrat);
    final CrueDaoDPTI crueDaoDPTI = readerWriter.readDao(dptiFile, log, null);
    if (crueDaoDPTI == null) {
      resu.setMetier(Collections.emptyMap());
    } else {
      resu.setMetier(new DPTIToCiniImportKeyTransformer().extract(crueDaoDPTI));
    }

    return resu;
  }
}
