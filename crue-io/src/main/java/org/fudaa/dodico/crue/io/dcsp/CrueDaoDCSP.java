/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dcsp;

import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.dcsp.CrueDaoStructureDCSP.DaoBrancheAbstract;
import org.fudaa.dodico.crue.io.dcsp.CrueDaoStructureDCSP.DaoCasier;

/**
 * Classe persistante qui reprend la meme structure que le fichier xml DSCP - Fichier des donnees de calcul (xml) A
 * persister telle qu'elle.
 * 
 * @author Adrien Hadoux
 */
public class CrueDaoDCSP extends AbstractCrueDao {

  /**
   * la liste des branches
   */
  protected List<DaoBrancheAbstract> DonCalcSansPrtBranches;
  /**
   * La liste des sections
   */
  protected List<DaoCasier> DonCalcSansPrtCasiers;
}
