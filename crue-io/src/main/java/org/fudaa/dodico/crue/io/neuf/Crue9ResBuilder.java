/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

/**
 * Interface definissant un contrat permettant de construire un resultat
 *
 * @param <T> le resultat voulu
 * @author deniger
 */
public interface Crue9ResBuilder<T extends FCBValueObject.AbstractRes> {
  /**
   * @return le resultat instancie
   */
  T createRes();

  /**
   * @param idxEnr l'indice de l'enregistrement a lire
   * @return le nombre de byte a sauter pour lire la bonne donnee
   */
  int getOffset(int idxEnr);

  /**
   * @return la longueur des donnees pour un resultat ( pour un profil,noeud ou branche).
   */
  int getTailleEnr();
}
