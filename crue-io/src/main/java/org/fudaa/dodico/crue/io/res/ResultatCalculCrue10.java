/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.res;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.io.rcal.Crue10ResultatPasDeTempsDelegate;
import org.fudaa.dodico.crue.io.rcal.RCalBinaryReader;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.VariableResDao;
import org.fudaa.dodico.crue.metier.emh.EvolutionFF;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;
import org.fudaa.dodico.crue.metier.emh.ResultatCalculDelegate;
import org.fudaa.dodico.crue.metier.emh.ResultatPasDeTempsDelegate;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;

/**
 * Conteneur de résultat commun a rcal et rptg.
 *
 * @author deniger
 */
public class ResultatCalculCrue10 implements ResultatCalculDelegate {

  private final int offset;
  private final int length;



  private final ResVariablesContent variables;
  private final Crue10ResultatPasDeTempsDelegate timeDelegate;
  private final String typeEMH;

  public ResultatCalculCrue10(int offset, int length, ResVariablesContent variables, Crue10ResultatPasDeTempsDelegate timeDelegate,
          String typeEMH, String emhId) {
    this.offset = offset;
    this.length = length;
    this.variables = variables;
    this.timeDelegate = timeDelegate;
    this.typeEMH = typeEMH;
  }

  public List<String> getQZRegulVariablesId(){
    return variables.getQZRegulVariablesId();
  }
  public List<String> getZRegulVariablesName(){
    return variables.getZRegulVariablesName();
  }
  public List<String> getQRegulVariablesName(){
    return variables.getQRegulVariablesName();
  }

  @Override
  public String getEMHType() {
    return typeEMH;
  }

  public boolean isQRegul(String nom) {
    return variables.isQRegul(nom);
  }

  public boolean isZRegul(String nom) {
    return variables.isZRegul(nom);
  }

  @Override
  public ResultatPasDeTempsDelegate getResultatPasDeTemps() {
    return timeDelegate;
  }

  @Override
  public Map<String, Object> read(ResultatTimeKey key) throws IOException {
    ResultatEntry rCalTimeStepEntry = timeDelegate.getEntries().get(key);
    if (rCalTimeStepEntry == null) {
      return Collections.emptyMap();
    }
    File binFile = rCalTimeStepEntry.getBinFile();
    if (!timeDelegate.exists(binFile)) {
      return Collections.emptyMap();
    }
    long offsetInFile =(long) offset + rCalTimeStepEntry.offsetInBytes;
    RCalBinaryReader binReader = new RCalBinaryReader(binFile);
    double[] res = binReader.readDoubles(offsetInFile, length);
    List<VariableResDao> variablesList = variables.getVariables();
    Map<String, Object> map = new HashMap<>(variablesList.size());
    int idx = 0;
    for (int i = 0; i < variablesList.size(); i++) {
      VariableResDao variableResDao = variablesList.get(i);
      String id = variableResDao.getId();
      if (variableResDao.isLoiDefinition()) {
        int nbValues = variables.getLoiNbrPt(id);
        int nbPoints = nbValues;
        List<PtEvolutionFF> points = new ArrayList<>(nbPoints);
        for (int j = 0; j < nbPoints; j++) {
          PtEvolutionFF pt = new PtEvolutionFF(res[idx++], res[idx++]);
          points.add(pt);
        }
        LoiFF loi = new LoiFF();
        final String idUsedForLoi = variableResDao.getId();
        loi.setType(EnumTypeLoi.getTypeLoi(variableResDao.getNomRef()));
        loi.setEvolutionFF(new EvolutionFF(points));
        map.put(idUsedForLoi, loi);
      } else {
        map.put(id, res[idx++]);
      }
    }
    return map;
  }
}
