/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import java.util.Map;
import org.fudaa.dodico.crue.io.neuf.AbstractCrue9Writer;
import org.fudaa.dodico.crue.io.neuf.DHReader;
import org.fudaa.dodico.crue.io.neuf.DHWriter;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * Gère la lecture du format crue 9 Utilise fortranReader.
 *
 * @author Adrien Hadoux
 */
public class Crue9DHFileFormat extends AbstractCrue9FileFormat {

  /**
   * Constructeur qui précise l'extension autorisée pour ce type de fichier
   */
  public Crue9DHFileFormat(final Map<CrueFileType, AbstractCrue9FileFormat> map) {
    super(CrueFileType.DH, map);
  }

  @Override
  protected DHReader createReader() {
    return new DHReader();
  }

  @Override
  protected AbstractCrue9Writer createWriter() {
    return new DHWriter();
  }
}
