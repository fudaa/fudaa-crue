/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dclm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.common.CrueHelper;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;

/**
 * Converter qui remplit les structures dao avec les objets metier et inversement.
 *
 * @author deniger, CDE
 */
public class CrueConverterDCLM implements CrueDataConverter<CrueDaoDCLM, DonCLimMScenario> {

  private final boolean ignoreIsActive;
  private final CoeurConfigContrat coeurConfig;

  /**
   * @param useIsActive true si la fonctionnalité active est utilisable
   */
  public CrueConverterDCLM(final boolean useIsActive, CoeurConfigContrat coeurConfig) {
    super();
    this.ignoreIsActive = useIsActive;
    this.coeurConfig = coeurConfig;
  }

  /**
   * Convertit les objets persistants en objets métier
   */
  @Override
  public DonCLimMScenario convertDaoToMetier(final CrueDaoDCLM dao, final CrueData dataLinked,
                                             final CtuluLog ctuluLog) {

    if (dataLinked == null) {
      ctuluLog.addInfo("io.dclm.main.error");
      return null;
    }
    final List<CrueDaoDCLMContents.CalculAbstractPersist> calculsPersist = dao.listeCalculs;
    DonCLimMScenario metier = dataLinked.getConditionsLim();
    if (metier == null) {
      metier = new DonCLimMScenario();
      dataLinked.setConditionsLim(metier);
    }

    final List<CalcPseudoPerm> listeCalculsPermanents = new ArrayList<>();
    final List<CalcTrans> listeCalculsTransitoires = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(calculsPersist)) {
      for (final CrueDaoDCLMContents.CalculAbstractPersist calculPersist : calculsPersist) {

        if (calculPersist instanceof CrueDaoDCLMContents.CalculPermanentPersist) {

          convertDaoToMetierCalculPermanent(listeCalculsPermanents,
              (CrueDaoDCLMContents.CalculPermanentPersist) calculPersist, dataLinked, ctuluLog);

        } else if (calculPersist instanceof CrueDaoDCLMContents.CalculTransitoirePersist) {

          convertDaoToMetierCalculTransitoire(listeCalculsTransitoires,
              (CrueDaoDCLMContents.CalculTransitoirePersist) calculPersist, dataLinked, ctuluLog);
        }

      }
      metier.setCalcPseudoPerm(listeCalculsPermanents);
      metier.setCalcTrans(listeCalculsTransitoires);
    }

    return metier;
  }

  @Override
  public DonCLimMScenario getConverterData(final CrueData in) {
    return in.getConditionsLim();
  }

  /**
   * Convertit les objets métier en objets persistants
   */
  @Override
  public CrueDaoDCLM convertMetierToDao(final DonCLimMScenario metier, final CtuluLog ctuluLog) {

    final CrueDaoDCLM res = new CrueDaoDCLM();

    res.listeCalculs = new ArrayList<>();

    List<CrueDaoDCLMContents.CalculAbstractPersist> listeCalculs = convertMetierToDaoCalculsPermanents(metier
        .getCalcPseudoPerm(), ctuluLog);
    res.listeCalculs.addAll(listeCalculs);

    listeCalculs = convertMetierToDaoCalculsTransitoires(metier.getCalcTrans(), ctuluLog);
    res.listeCalculs.addAll(listeCalculs);

    return res;
  }

  private void convertDaoToMetierCalculPermanent(final List<CalcPseudoPerm> listeMetier,
                                                 final CrueDaoDCLMContents.CalculPermanentPersist calculPermPersist, final CrueData dataLinked,
                                                 final CtuluLog analyzer) {

    final CalcPseudoPerm calculPermMetier = new CalcPseudoPerm();
    calculPermMetier.setNom(calculPermPersist.Nom);
    calculPermMetier.setCommentaire(calculPermPersist.Commentaire);

    // *** Gestion des elements d'un calcul permanent
    if (calculPermPersist.listeElementsCalculPermanent == null
        || calculPermPersist.listeElementsCalculPermanent.isEmpty()) {
      analyzer.addInfo("io.dclm.no.calcul.perm.error");
    } else {

      for (final CrueDaoDCLMContents.RefDCLMAbstractPersist eltRefPersist : calculPermPersist.listeElementsCalculPermanent) {
        if (eltRefPersist instanceof CrueDaoDCLMContents.CalcPseudoPermNoeudNiveauContinuQappPersist) {
          final CrueDaoDCLMContents.CalcPseudoPermNoeudNiveauContinuQappPersist eltPersist = (CrueDaoDCLMContents.CalcPseudoPermNoeudNiveauContinuQappPersist) eltRefPersist;
          final CatEMHNoeud noeudMetier = getEMHNoeudFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (noeudMetier != null) {
            final CalcPseudoPermNoeudQapp eltMetier = new CalcPseudoPermNoeudQapp(dataLinked
                .getCrueConfigMetier());
            eltMetier.setQapp(eltPersist.Qapp);
            initMetierPseudoPerm(eltMetier, calculPermMetier, eltPersist, noeudMetier);
          }

        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcPseudoPermNoeudNiveauContinuZimposePersist) {

          final CrueDaoDCLMContents.CalcPseudoPermNoeudNiveauContinuZimposePersist eltPersist = (CrueDaoDCLMContents.CalcPseudoPermNoeudNiveauContinuZimposePersist) eltRefPersist;
          final CatEMHNoeud noeudMetier = getEMHNoeudFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (noeudMetier != null) {
            final CalcPseudoPermNoeudNiveauContinuZimp eltMetier = new CalcPseudoPermNoeudNiveauContinuZimp(dataLinked
                .getCrueConfigMetier());
            eltMetier.setZimp(eltPersist.Zimp);
            initMetierPseudoPerm(eltMetier, calculPermMetier, eltPersist, noeudMetier);
          }

        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcPseudoPermBrancheOrificeManoeuvrePersist) {

          final CrueDaoDCLMContents.CalcPseudoPermBrancheOrificeManoeuvrePersist eltPersist = (CrueDaoDCLMContents.CalcPseudoPermBrancheOrificeManoeuvrePersist) eltRefPersist;
          final CatEMHBranche brancheMetier = getEMHBrancheFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (brancheMetier != null) {
            final CalcPseudoPermBrancheOrificeManoeuvre eltMetier = new CalcPseudoPermBrancheOrificeManoeuvre(
                dataLinked.getCrueConfigMetier());
            eltMetier.setOuv(eltPersist.Ouv);
            eltMetier.setSensOuv(eltPersist.SensOuv);
            initMetierPseudoPerm(eltMetier, calculPermMetier, eltPersist, brancheMetier);
          }
        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcPseudoPermBrancheOrificeManoeuvreRegulPersist) {

          final CrueDaoDCLMContents.CalcPseudoPermBrancheOrificeManoeuvreRegulPersist eltPersist = (CrueDaoDCLMContents.CalcPseudoPermBrancheOrificeManoeuvreRegulPersist) eltRefPersist;
          final CatEMHBranche brancheMetier = getEMHBrancheFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (brancheMetier != null) {
            final CalcPseudoPermBrancheOrificeManoeuvreRegul eltMetier = new CalcPseudoPermBrancheOrificeManoeuvreRegul(
                dataLinked.getCrueConfigMetier());
            if (eltPersist.ManoeuvreRegul == null) {
              CrueHelper.emhEmpty("Calcul Permanent - BrancheOrificeManoeuvreRegul", analyzer);
            } else {
              LoiFF loi = (LoiFF) getLoiFromReferenceDLHY(eltPersist.ManoeuvreRegul.NomRef, dataLinked, analyzer);
              eltMetier.setParam(eltPersist.ManoeuvreRegul.Param);
              if (loi != null) {
                eltMetier.setManoeuvreRegul(loi);
              }
            }
            eltMetier.setSensOuv(eltPersist.SensOuv);
            initMetierPseudoPerm(eltMetier, calculPermMetier, eltPersist, brancheMetier);
          }

        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcPseudoPermBrancheBrancheSaintVenantQruisPersist) {

          final CrueDaoDCLMContents.CalcPseudoPermBrancheBrancheSaintVenantQruisPersist eltPersist = (CrueDaoDCLMContents.CalcPseudoPermBrancheBrancheSaintVenantQruisPersist) eltRefPersist;
          final CatEMHBranche brancheMetier = getEMHBrancheFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (brancheMetier != null) {
            final CalcPseudoPermBrancheSaintVenantQruis eltMetier = new CalcPseudoPermBrancheSaintVenantQruis(
                dataLinked.getCrueConfigMetier());
            eltMetier.setQruis(eltPersist.Qruis);
            initMetierPseudoPerm(eltMetier, calculPermMetier, eltPersist, brancheMetier);
          }

        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcPseudoPermBrancheCasierProfilQruisPersist) {

          final CrueDaoDCLMContents.CalcPseudoPermBrancheCasierProfilQruisPersist eltPersist = (CrueDaoDCLMContents.CalcPseudoPermBrancheCasierProfilQruisPersist) eltRefPersist;
          final CatEMHCasier casierMetier = getEMHCasierFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (casierMetier != null) {
            final CalcPseudoPermCasierProfilQruis eltMetier = new CalcPseudoPermCasierProfilQruis(dataLinked
                .getCrueConfigMetier());
            eltMetier.setQruis(eltPersist.Qruis);
            initMetierPseudoPerm(eltMetier, calculPermMetier, eltPersist, casierMetier);
          }

        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcPseudoPermNoeudBg1Persist) {
          final CrueDaoDCLMContents.CalcPseudoPermNoeudBg1Persist eltPersist = (CrueDaoDCLMContents.CalcPseudoPermNoeudBg1Persist) eltRefPersist;
          final CatEMHNoeud noeudMetier = getEMHNoeudFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (noeudMetier != null) {
            final CalcPseudoPermNoeudBg1 eltMetier = new CalcPseudoPermNoeudBg1(dataLinked
                .getCrueConfigMetier());
            initMetierPseudoPerm(eltMetier, calculPermMetier, eltPersist, noeudMetier);
          }

        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcPseudoPermNoeudBg1AvPersist) {
          final CrueDaoDCLMContents.CalcPseudoPermNoeudBg1AvPersist eltPersist = (CrueDaoDCLMContents.CalcPseudoPermNoeudBg1AvPersist) eltRefPersist;
          final CatEMHNoeud noeudMetier = getEMHNoeudFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (noeudMetier != null) {
            final CalcPseudoPermNoeudBg1Av eltMetier = new CalcPseudoPermNoeudBg1Av(dataLinked
                .getCrueConfigMetier());
            initMetierPseudoPerm(eltMetier, calculPermMetier, eltPersist, noeudMetier);
          }

        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcPseudoPermNoeudBg2Persist) {
          final CrueDaoDCLMContents.CalcPseudoPermNoeudBg2Persist eltPersist = (CrueDaoDCLMContents.CalcPseudoPermNoeudBg2Persist) eltRefPersist;
          final CatEMHNoeud noeudMetier = getEMHNoeudFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (noeudMetier != null) {
            final CalcPseudoPermNoeudBg2 eltMetier = new CalcPseudoPermNoeudBg2(dataLinked
                .getCrueConfigMetier());
            initMetierPseudoPerm(eltMetier, calculPermMetier, eltPersist, noeudMetier);
          }
        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcPseudoPermNoeudBg2AvPersist) {
          final CrueDaoDCLMContents.CalcPseudoPermNoeudBg2AvPersist eltPersist = (CrueDaoDCLMContents.CalcPseudoPermNoeudBg2AvPersist) eltRefPersist;
          final CatEMHNoeud noeudMetier = getEMHNoeudFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (noeudMetier != null) {
            final CalcPseudoPermNoeudBg2Av eltMetier = new CalcPseudoPermNoeudBg2Av(dataLinked
                .getCrueConfigMetier());
            initMetierPseudoPerm(eltMetier, calculPermMetier, eltPersist, noeudMetier);
          }
        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcPseudoPermNoeudUsiPersist) {
          final CrueDaoDCLMContents.CalcPseudoPermNoeudUsiPersist eltPersist = (CrueDaoDCLMContents.CalcPseudoPermNoeudUsiPersist) eltRefPersist;
          final CatEMHNoeud noeudMetier = getEMHNoeudFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (noeudMetier != null) {
            final CalcPseudoPermNoeudUsi eltMetier = new CalcPseudoPermNoeudUsi(dataLinked
                .getCrueConfigMetier());
            initMetierPseudoPerm(eltMetier, calculPermMetier, eltPersist, noeudMetier);
          }
        }
      }
    }

    listeMetier.add(calculPermMetier);
  }

  private void initMetierPseudoPerm(DonCLimMCommonItem eltMetier, CalcPseudoPerm calculPermMetier, CrueDaoDCLMContents.ActivableDcml eltPersist, EMH emh) {
    eltMetier.setCalculParent(calculPermMetier);
    this.daoToMetierupdateIsActive(eltPersist, eltMetier);
    calculPermMetier.addDclm(eltMetier);
    emh.addInfosEMH(eltMetier);
    eltMetier.setEmh(emh);
  }

  private void daoToMetierupdateIsActive(final CrueDaoDCLMContents.ActivableDcml eltPersist,
                                         final DonCLimMCommonItem eltMetier) {
    if (!ignoreIsActive) {
      eltMetier.setUserActive(eltPersist.IsActive);
    }
  }

  private void convertDaoToMetierCalculTransitoire(final List<CalcTrans> listeMetier,
                                                   final CrueDaoDCLMContents.CalculTransitoirePersist calculTransPersist, final CrueData dataLinked,
                                                   final CtuluLog analyzer) {

    final CalcTrans calculTransMetier = new CalcTrans();
    calculTransMetier.setNom(calculTransPersist.Nom);
    calculTransMetier.setCommentaire(calculTransPersist.Commentaire);

    // *** Gestion des elements d'un calcul transitoire
    if (calculTransPersist.listeElementsCalculTransitoire == null
        || calculTransPersist.listeElementsCalculTransitoire.isEmpty()) {
      analyzer.addInfo("io.dclm.no.calcul.trans.error");
    } else {

      for (final CrueDaoDCLMContents.RefDCLMAbstractPersist eltRefPersist : calculTransPersist.listeElementsCalculTransitoire) {

        if (eltRefPersist instanceof CrueDaoDCLMContents.CalcTransNoeudNiveauContinuHydrogrammePersist) {

          final CrueDaoDCLMContents.CalcTransNoeudNiveauContinuHydrogrammePersist eltPersist = (CrueDaoDCLMContents.CalcTransNoeudNiveauContinuHydrogrammePersist) eltRefPersist;
          final CatEMHNoeud emh = getEMHNoeudFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (emh != null) {
            final CalcTransNoeudQapp eltMetier = new CalcTransNoeudQapp();
            initMetier(calculTransMetier, eltPersist, emh, eltMetier);

            LoiDF loi = null;
            if (eltPersist.HydrogrammeQapp == null) {
              CrueHelper.emhEmpty("Calcul Transitoire - NoeudNiveauContinu - Hydrogrammes", analyzer);
            } else {
              loi = (LoiDF) getLoiFromReferenceDLHY(eltPersist.HydrogrammeQapp.NomRef, dataLinked, analyzer);
            }
            eltMetier.setHydrogrammeQapp(loi);
          }

        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcTransNoeudNiveauContinuLimnigrammePersist) {

          final CrueDaoDCLMContents.CalcTransNoeudNiveauContinuLimnigrammePersist eltPersist = (CrueDaoDCLMContents.CalcTransNoeudNiveauContinuLimnigrammePersist) eltRefPersist;
          final CatEMHNoeud emh = getEMHNoeudFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (emh != null) {
            final CalcTransNoeudNiveauContinuLimnigramme eltMetier = new CalcTransNoeudNiveauContinuLimnigramme();
            initMetier(calculTransMetier, eltPersist, emh, eltMetier);

            LoiDF loi = null;

            if (eltPersist.Limnigramme == null) {
              CrueHelper.emhEmpty("Calcul Transitoire - NoeudNiveauContinu - Limnigrammes", analyzer);
            } else {
              loi = (LoiDF) getLoiFromReferenceDLHY(eltPersist.Limnigramme.NomRef, dataLinked, analyzer);
            }
            eltMetier.setLimnigramme(loi);
          }

        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcTransNoeudNiveauContinuTaragePersist) {

          final CrueDaoDCLMContents.CalcTransNoeudNiveauContinuTaragePersist eltPersist = (CrueDaoDCLMContents.CalcTransNoeudNiveauContinuTaragePersist) eltRefPersist;
          final CatEMHNoeud emh = getEMHNoeudFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (emh != null) {
            final CalcTransNoeudNiveauContinuTarage eltMetier = new CalcTransNoeudNiveauContinuTarage();
            initMetier(calculTransMetier, eltPersist, emh, eltMetier);

            LoiFF loi = null;
            if (eltPersist.Tarage == null) {
              CrueHelper.emhEmpty("Calcul Transitoire - NoeudNiveauContinu - Tarrages", analyzer);
            } else {
              loi = (LoiFF) getLoiFromReferenceDLHY(eltPersist.Tarage.NomRef, dataLinked, analyzer);
            }
            eltMetier.setTarage(loi);
          }

        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcTransBrancheOrificeManoeuvrePersist) {

          final CrueDaoDCLMContents.CalcTransBrancheOrificeManoeuvrePersist eltPersist = (CrueDaoDCLMContents.CalcTransBrancheOrificeManoeuvrePersist) eltRefPersist;
          final CatEMHBranche emh = getEMHBrancheFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (emh != null) {
            final CalcTransBrancheOrificeManoeuvre eltMetier = new CalcTransBrancheOrificeManoeuvre(dataLinked
                .getCrueConfigMetier());
            initMetier(calculTransMetier, eltPersist, emh, eltMetier);

            LoiDF loi = null;

            if (eltPersist.Manoeuvre == null) {
              CrueHelper.emhEmpty("Calcul Transitoire - BrancheOrificeManoeuvre - Manoeuvres", analyzer);
            } else {
              loi = (LoiDF) getLoiFromReferenceDLHY(eltPersist.Manoeuvre.NomRef, dataLinked, analyzer);
            }
            if (loi != null) {
              eltMetier.setManoeuvre(loi);
            }
            eltMetier.setSensOuv(eltPersist.SensOuv);
          }

        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcTransBrancheSaintVenantHydrogrammeRuisPersist) {

          final CrueDaoDCLMContents.CalcTransBrancheSaintVenantHydrogrammeRuisPersist eltPersist = (CrueDaoDCLMContents.CalcTransBrancheSaintVenantHydrogrammeRuisPersist) eltRefPersist;
          final CatEMHBranche emh = getEMHBrancheFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (emh != null) {
            final CalcTransBrancheSaintVenantQruis eltMetier = new CalcTransBrancheSaintVenantQruis();
            initMetier(calculTransMetier, eltPersist, emh, eltMetier);

            LoiDF loi = null;
            if (eltPersist.HydrogrammeQruis != null) {
              loi = (LoiDF) getLoiFromReferenceDLHY(eltPersist.HydrogrammeQruis.NomRef, dataLinked, analyzer);
            }
            eltMetier.setHydrogrammeQruis(loi);
          }

        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcTransCasierProfilHydrogrammeRuisPersist) {

          final CrueDaoDCLMContents.CalcTransCasierProfilHydrogrammeRuisPersist eltPersist = (CrueDaoDCLMContents.CalcTransCasierProfilHydrogrammeRuisPersist) eltRefPersist;
          final CatEMHCasier emh = getEMHCasierFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (emh != null) {
            final CalcTransCasierProfilQruis eltMetier = new CalcTransCasierProfilQruis();
            initMetier(calculTransMetier, eltPersist, emh, eltMetier);

            LoiDF loi = null;
            if (eltPersist.HydrogrammeQruis != null) {
              loi = (LoiDF) getLoiFromReferenceDLHY(eltPersist.HydrogrammeQruis.NomRef, dataLinked, analyzer);
            }
            eltMetier.setHydrogrammeQruis(loi);

          }

        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcTransNoeudQappExtPersist) {

          final CrueDaoDCLMContents.CalcTransNoeudQappExtPersist eltPersist = (CrueDaoDCLMContents.CalcTransNoeudQappExtPersist) eltRefPersist;
          final CatEMHNoeud emh = getEMHNoeudFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (emh != null) {
            final CalcTransNoeudQappExt eltMetier = new CalcTransNoeudQappExt();
            initMetier(calculTransMetier, eltPersist, emh, eltMetier);
            eltMetier.setResCalcTrans(eltPersist.HydrogrammeQappExt.ResCalcTrans);
            eltMetier.setSection(eltPersist.HydrogrammeQappExt.NomRef);
            eltMetier.setNomFic(eltPersist.HydrogrammeQappExt.NomFic);
          }

        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcTransBrancheOrificeManoeuvreRegulPersist) {

          final CrueDaoDCLMContents.CalcTransBrancheOrificeManoeuvreRegulPersist eltPersist = (CrueDaoDCLMContents.CalcTransBrancheOrificeManoeuvreRegulPersist) eltRefPersist;
          final CatEMHBranche emh = getEMHBrancheFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (emh != null) {
            final CalcTransBrancheOrificeManoeuvreRegul eltMetier = new CalcTransBrancheOrificeManoeuvreRegul(dataLinked.getCrueConfigMetier());
            initMetier(calculTransMetier, eltPersist, emh, eltMetier);
            LoiFF loi = null;
            if (eltPersist.ManoeuvreRegul == null) {
              CrueHelper.emhEmpty("Calcul Transitoire - BrancheOrificeManoeuvreRegul", analyzer);
            } else {
              loi = (LoiFF) getLoiFromReferenceDLHY(eltPersist.ManoeuvreRegul.NomRef, dataLinked, analyzer);
              eltMetier.setParam(eltPersist.ManoeuvreRegul.Param);
            }
            if (loi != null) {
              eltMetier.setManoeuvreRegul(loi);
            }
            eltMetier.setSensOuv(eltPersist.SensOuv);
          }

        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcTransNoeudBg1Persist) {
          final CrueDaoDCLMContents.CalcTransNoeudBg1Persist eltPersist = (CrueDaoDCLMContents.CalcTransNoeudBg1Persist) eltRefPersist;
          final CatEMHNoeud emh = getEMHNoeudFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (emh != null) {
            final CalcTransNoeudBg1 eltMetier = new CalcTransNoeudBg1();
            initMetier(calculTransMetier, eltPersist, emh, eltMetier);
          }
        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcTransNoeudBg2Persist) {
          final CrueDaoDCLMContents.CalcTransNoeudBg2Persist eltPersist = (CrueDaoDCLMContents.CalcTransNoeudBg2Persist) eltRefPersist;
          final CatEMHNoeud emh = getEMHNoeudFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (emh != null) {
            final CalcTransNoeudBg2 eltMetier = new CalcTransNoeudBg2();
            initMetier(calculTransMetier, eltPersist, emh, eltMetier);
          }
        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcTransNoeudBg1AvPersist) {
          final CrueDaoDCLMContents.CalcTransNoeudBg1AvPersist eltPersist = (CrueDaoDCLMContents.CalcTransNoeudBg1AvPersist) eltRefPersist;
          final CatEMHNoeud emh = getEMHNoeudFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (emh != null) {
            final CalcTransNoeudBg1Av eltMetier = new CalcTransNoeudBg1Av();
            initMetier(calculTransMetier, eltPersist, emh, eltMetier);
          }
        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcTransNoeudBg2AvPersist) {
          final CrueDaoDCLMContents.CalcTransNoeudBg2AvPersist eltPersist = (CrueDaoDCLMContents.CalcTransNoeudBg2AvPersist) eltRefPersist;
          final CatEMHNoeud emh = getEMHNoeudFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (emh != null) {
            final CalcTransNoeudBg2Av eltMetier = new CalcTransNoeudBg2Av();
            initMetier(calculTransMetier, eltPersist, emh, eltMetier);
          }
        } else if (eltRefPersist instanceof CrueDaoDCLMContents.CalcTransNoeudUsinePersist) {
          final CrueDaoDCLMContents.CalcTransNoeudUsinePersist eltPersist = (CrueDaoDCLMContents.CalcTransNoeudUsinePersist) eltRefPersist;
          final CatEMHNoeud emh = getEMHNoeudFromReferenceDRSO(eltPersist.NomRef, dataLinked, analyzer);
          if (emh != null) {
            final CalcTransNoeudUsine eltMetier = new CalcTransNoeudUsine();
            initMetier(calculTransMetier, eltPersist, emh, eltMetier);
          }
        }
      }
    }

    listeMetier.add(calculTransMetier);
  }

  private void initMetier(CalcTrans calculTransMetier, final CrueDaoDCLMContents.ActivableDcml eltPersist, EMH emh, DonCLimMCommonItem eltMetier) {
    eltMetier.setCalculParent(calculTransMetier);
    emh.addInfosEMH(eltMetier);
    eltMetier.setEmh(emh);
    calculTransMetier.addDclm(eltMetier);
    this.daoToMetierupdateIsActive(eltPersist, eltMetier);
  }

  /**
   * Recherche la loi correspondant au nom passé en paramètre parmi l'ensemble des lois métiers
   */
  private static Loi getLoiFromReferenceDLHY(final String nomRef, final CrueData dataLinked, final CtuluLog analyzer) {

    final Loi loi = LoiHelper.findByReference(nomRef, dataLinked.getLois());
    if (loi == null) {
      analyzer.addInfo("io.dclm.ref.loi.error", nomRef);
    }

    return loi;
  }

  /**
   * Recherche le noeud EMH correspondant au nom passé en paramètre parmi l'ensemble des noeuds métiers
   */
  private static CatEMHNoeud getEMHNoeudFromReferenceDRSO(final String nomRef, final CrueData dataLinked,
                                                          final CtuluLog analyzer) {

    final CatEMHNoeud noeudMetier = dataLinked.findNoeudByReference(nomRef);
    if (noeudMetier == null) {
      analyzer.addInfo("io.dclm.ref.emhnoeud.error", nomRef);
    }

    return noeudMetier;
  }

  /**
   * Recherche la branche EMH correspondant au nom passé en paramètre parmi l'ensemble des branches métiers
   */
  private static CatEMHBranche getEMHBrancheFromReferenceDRSO(final String nomRef, final CrueData dataLinked,
                                                              final CtuluLog analyzer) {

    final CatEMHBranche brancheMetier = dataLinked.findBrancheByReference(nomRef);
    if (brancheMetier == null) {
      analyzer.addInfo("io.dclm.ref.emhbranche.error", nomRef);
    }

    return brancheMetier;
  }

  /**
   * Recherche le casier EMH correspondant au nom passé en paramètre parmi l'ensemble des casiers métiers
   */
  private static CatEMHCasier getEMHCasierFromReferenceDRSO(final String nomRef, final CrueData dataLinked,
                                                            final CtuluLog analyzer) {

    final CatEMHCasier casierMetier = dataLinked.findCasierByReference(nomRef);
    if (casierMetier == null) {
      analyzer.addInfo("io.dclm.ref.emhcasier.error", nomRef);
    }

    return casierMetier;
  }

  /**
   * Chaque calcul permanent contient une liste de références EMH qui sont utilisées pour ce calcul; Chaque référence EMH (de DRSO) contient une liste
   * d'objets métier de DCLM qui sont de types métier différents mais référencent le même objet EMH de DRSO. On écrit alors dans la persistance,
   * l'objet métier qui correspond à un type possible pour ce calcul et surtout au nom du calcul en cours
   */
  private List<CrueDaoDCLMContents.CalculAbstractPersist> convertMetierToDaoCalculsPermanents(
      final List<CalcPseudoPerm> data, final CtuluLog analyser) {

    final List<CrueDaoDCLMContents.CalculAbstractPersist> listePersistante = new ArrayList<>();
    boolean newDclmSupported = Crue10VersionConfig.isUpperThan_V1_2(coeurConfig);
    if (CollectionUtils.isNotEmpty(data)) {
      for (final CalcPseudoPerm calculMetier : data) {
        final CrueDaoDCLMContents.CalculPermanentPersist calculPersist = new CrueDaoDCLMContents.CalculPermanentPersist();
        calculPersist.Nom = calculMetier.getNom();
        calculPersist.Commentaire = calculMetier.getCommentaire();
        calculPersist.listeElementsCalculPermanent = new ArrayList<>();
        convertMetierToDaoPseudoPermNoeudQapp(calculMetier, calculPersist);
        convertMetierToDaoPermNoeudNiveauContinuZimp(calculMetier, calculPersist);
        convertMetierToDaoPseudoPermBrancheOrificeManoeuvre(calculMetier, calculPersist);
        convertMetierToDaoPseudoPermBrancheOrificeManoeuvreRegul(calculMetier, calculPersist, analyser, newDclmSupported);
        convertMetierToDaoPseudoPermBrancheSaintVenantQruis(calculMetier, calculPersist);
        convertMetierToDaoPseudoPermCasierProfilQruis(calculMetier, calculPersist);
        convertMetierToDaoPseudoPermNoeudBg1(calculMetier, calculPersist, analyser, newDclmSupported);
        convertMetierToDaoPseudoPermNoeudBg2(calculMetier, calculPersist, analyser, newDclmSupported);
        convertMetierToDaoPseudoPermNoeudUsi(calculMetier, calculPersist, analyser, newDclmSupported);
        convertMetierToDaoPseudoPermNoeudBg1Av(calculMetier, calculPersist, analyser, newDclmSupported);
        convertMetierToDaoPseudoPermNoeudBg2Av(calculMetier, calculPersist, analyser, newDclmSupported);

        listePersistante.add(calculPersist);

      }// Fin for listeCalculsPermanents

    }

    return listePersistante;
  }

  private void convertMetierToDaoPseudoPermNoeudUsi(CalcPseudoPerm calculMetier, CrueDaoDCLMContents.CalculPermanentPersist calculPersist, CtuluLog analyser, boolean newDclmSupported) {
    List<CalcPseudoPermNoeudUsi> list = calculMetier.getDclm(CalcPseudoPermNoeudUsi.class);
    if (!newDclmSupported && !list.isEmpty()) {
      addWarningNewDclmNotSupported("CalcPseudoPermNoeudUsi", analyser);
    }
    if (!newDclmSupported) return;
    for (final CalcPseudoPermNoeudUsi in : list) {
      final CrueDaoDCLMContents.CalcPseudoPermNoeudUsiPersist dao = new CrueDaoDCLMContents.CalcPseudoPermNoeudUsiPersist();
      dao.NomRef = in.getEmh().getNom();
      dao.IsActive = in.getUserActive();
      calculPersist.listeElementsCalculPermanent.add(dao);
    }
  }

  private void convertMetierToDaoPseudoPermNoeudBg2Av(CalcPseudoPerm calculMetier, CrueDaoDCLMContents.CalculPermanentPersist calculPersist, CtuluLog analyser, boolean newDclmSupported) {
    List<CalcPseudoPermNoeudBg2Av> list = calculMetier.getDclm(CalcPseudoPermNoeudBg2Av.class);
    if (!newDclmSupported && !list.isEmpty()) {
      addWarningNewDclmNotSupported("CalcPseudoPermNoeudBg2Av", analyser);
    }
    if (!newDclmSupported) return;
    for (final CalcPseudoPermNoeudBg2Av in : list) {
      final CrueDaoDCLMContents.CalcPseudoPermNoeudBg2AvPersist dao = new CrueDaoDCLMContents.CalcPseudoPermNoeudBg2AvPersist();
      dao.NomRef = in.getEmh().getNom();
      dao.IsActive = in.getUserActive();
      calculPersist.listeElementsCalculPermanent.add(dao);
    }
  }

  private void convertMetierToDaoPseudoPermNoeudBg2(CalcPseudoPerm calculMetier, CrueDaoDCLMContents.CalculPermanentPersist calculPersist, CtuluLog analyser, boolean newDclmSupported) {
    List<CalcPseudoPermNoeudBg2> list = calculMetier.getDclm(CalcPseudoPermNoeudBg2.class);
    if (!newDclmSupported && !list.isEmpty()) {
      addWarningNewDclmNotSupported("CalcPseudoPermNoeudBg2", analyser);
    }
    if (!newDclmSupported) return;
    for (final CalcPseudoPermNoeudBg2 in : list) {
      final CrueDaoDCLMContents.CalcPseudoPermNoeudBg2Persist dao = new CrueDaoDCLMContents.CalcPseudoPermNoeudBg2Persist();
      dao.NomRef = in.getEmh().getNom();
      dao.IsActive = in.getUserActive();
      calculPersist.listeElementsCalculPermanent.add(dao);
    }
  }

  private void convertMetierToDaoPseudoPermNoeudBg1Av(CalcPseudoPerm calculMetier, CrueDaoDCLMContents.CalculPermanentPersist calculPersist, CtuluLog analyser, boolean newDclmSupported) {
    List<CalcPseudoPermNoeudBg1Av> list = calculMetier.getDclm(CalcPseudoPermNoeudBg1Av.class);
    if (!newDclmSupported && !list.isEmpty()) {
      addWarningNewDclmNotSupported("CalcPseudoPermNoeudBg1Av", analyser);
    }
    if (!newDclmSupported) return;
    for (final CalcPseudoPermNoeudBg1Av in : list) {
      final CrueDaoDCLMContents.CalcPseudoPermNoeudBg1AvPersist dao = new CrueDaoDCLMContents.CalcPseudoPermNoeudBg1AvPersist();
      dao.NomRef = in.getEmh().getNom();
      dao.IsActive = in.getUserActive();
      calculPersist.listeElementsCalculPermanent.add(dao);
    }
  }

  private void convertMetierToDaoPseudoPermNoeudBg1(CalcPseudoPerm calculMetier, CrueDaoDCLMContents.CalculPermanentPersist calculPersist, CtuluLog analyser, boolean newDclmSupported) {
    List<CalcPseudoPermNoeudBg1> list = calculMetier.getDclm(CalcPseudoPermNoeudBg1.class);
    if (!newDclmSupported && !list.isEmpty()) {
      addWarningNewDclmNotSupported("CalcPseudoPermNoeudBg1", analyser);
    }
    if (!newDclmSupported) return;
    for (final CalcPseudoPermNoeudBg1 in : list) {
      final CrueDaoDCLMContents.CalcPseudoPermNoeudBg1Persist dao = new CrueDaoDCLMContents.CalcPseudoPermNoeudBg1Persist();
      dao.NomRef = in.getEmh().getNom();
      dao.IsActive = in.getUserActive();
      calculPersist.listeElementsCalculPermanent.add(dao);
    }
  }

  private static void convertMetierToDaoPseudoPermCasierProfilQruis(CalcPseudoPerm calculMetier, CrueDaoDCLMContents.CalculPermanentPersist calculPersist) {
    for (final CalcPseudoPermCasierProfilQruis in : calculMetier.getDclm(CalcPseudoPermCasierProfilQruis.class)) {
      final CrueDaoDCLMContents.CalcPseudoPermBrancheCasierProfilQruisPersist dao = new CrueDaoDCLMContents.CalcPseudoPermBrancheCasierProfilQruisPersist();
      dao.NomRef = in.getEmh().getNom();
      dao.Qruis = in.getValue();
      dao.IsActive = in.getUserActive();
      calculPersist.listeElementsCalculPermanent.add(dao);
    }
  }

  private static void convertMetierToDaoPseudoPermBrancheSaintVenantQruis(CalcPseudoPerm calculMetier, CrueDaoDCLMContents.CalculPermanentPersist calculPersist) {
    for (final CalcPseudoPermBrancheSaintVenantQruis in : calculMetier.getDclm(CalcPseudoPermBrancheSaintVenantQruis.class)) {
      final CrueDaoDCLMContents.CalcPseudoPermBrancheBrancheSaintVenantQruisPersist dao = new CrueDaoDCLMContents.CalcPseudoPermBrancheBrancheSaintVenantQruisPersist();
      dao.NomRef = in.getEmh().getNom();
      dao.Qruis = in.getValue();
      dao.IsActive = in.getUserActive();
      calculPersist.listeElementsCalculPermanent.add(dao);
    }
  }

  private void convertMetierToDaoPseudoPermBrancheOrificeManoeuvreRegul(CalcPseudoPerm calculMetier, CrueDaoDCLMContents.CalculPermanentPersist calculPersist, CtuluLog analyser, boolean newDclmSupported) {
    List<CalcPseudoPermBrancheOrificeManoeuvreRegul> list = calculMetier.getDclm(CalcPseudoPermBrancheOrificeManoeuvreRegul.class);
    if (!newDclmSupported && !list.isEmpty()) {
      addWarningNewDclmNotSupported("CalcPseudoPermBrancheOrificeManoeuvreRegul", analyser);
    }
    if (!newDclmSupported) return;
    for (final CalcPseudoPermBrancheOrificeManoeuvreRegul in : list) {
      final CrueDaoDCLMContents.CalcPseudoPermBrancheOrificeManoeuvreRegulPersist dao = new CrueDaoDCLMContents.CalcPseudoPermBrancheOrificeManoeuvreRegulPersist();
      dao.NomRef = in.getEmh().getNom();
      dao.ManoeuvreRegul = new CrueDaoDCLMContents.ManoeuvreRegulPersist();
      dao.ManoeuvreRegul.NomRef = in.getManoeuvreRegul().getNom();
      dao.ManoeuvreRegul.Param = in.getParam();
      dao.SensOuv = in.getSensOuv();
      dao.IsActive = in.getUserActive();
      calculPersist.listeElementsCalculPermanent.add(dao);
    }
  }

  private static void convertMetierToDaoPseudoPermBrancheOrificeManoeuvre(CalcPseudoPerm calculMetier, CrueDaoDCLMContents.CalculPermanentPersist calculPersist) {
    for (final CalcPseudoPermBrancheOrificeManoeuvre in : calculMetier.getDclm(CalcPseudoPermBrancheOrificeManoeuvre.class)) {
      final CrueDaoDCLMContents.CalcPseudoPermBrancheOrificeManoeuvrePersist dao = new CrueDaoDCLMContents.CalcPseudoPermBrancheOrificeManoeuvrePersist();
      dao.NomRef = in.getEmh().getNom();
      dao.Ouv = in.getValue();
      dao.SensOuv = in.getSensOuv();
      dao.IsActive = in.getUserActive();
      calculPersist.listeElementsCalculPermanent.add(dao);
    }
  }

  private static void convertMetierToDaoPermNoeudNiveauContinuZimp(CalcPseudoPerm calculMetier, CrueDaoDCLMContents.CalculPermanentPersist calculPersist) {
    for (final CalcPseudoPermNoeudNiveauContinuZimp in : calculMetier.getDclm(CalcPseudoPermNoeudNiveauContinuZimp.class)) {
      final CrueDaoDCLMContents.CalcPseudoPermNoeudNiveauContinuZimposePersist dao = new CrueDaoDCLMContents.CalcPseudoPermNoeudNiveauContinuZimposePersist();
      dao.NomRef = in.getEmh().getNom();
      dao.Zimp = in.getValue();
      dao.IsActive = in.getUserActive();
      calculPersist.listeElementsCalculPermanent.add(dao);
    }
  }

  private static void convertMetierToDaoPseudoPermNoeudQapp(CalcPseudoPerm calculMetier, CrueDaoDCLMContents.CalculPermanentPersist calculPersist) {
    for (final CalcPseudoPermNoeudQapp in : calculMetier.getDclm(CalcPseudoPermNoeudQapp.class)) {
      final CrueDaoDCLMContents.CalcPseudoPermNoeudNiveauContinuQappPersist dao = new CrueDaoDCLMContents.CalcPseudoPermNoeudNiveauContinuQappPersist();
      dao.NomRef = in.getEmh().getNom();
      dao.Qapp = in.getValue();
      dao.IsActive = in.getUserActive();
      calculPersist.listeElementsCalculPermanent.add(dao);
    }
  }

  Set<String> warningDone = new HashSet<>();

  /**
   * Chaque calcul transitoire contient une liste de références EMH qui sont utilisées pour ce calcul; Chaque référence EMH (de DRSO) contient une
   * liste d'objets métier de DCLM qui sont de types métier différents mais référencent le même objet EMH de DRSO. On écrit alors dans la persistance,
   * l'objet métier qui correspond à un type possible pour ce calcul et surtout au nom du calcul en cours
   */
  private List<CrueDaoDCLMContents.CalculAbstractPersist> convertMetierToDaoCalculsTransitoires(
      final List<CalcTrans> data, final CtuluLog analyser) {
    boolean newDclmSupported = Crue10VersionConfig.isUpperThan_V1_2(coeurConfig);

    final List<CrueDaoDCLMContents.CalculAbstractPersist> listePersistante = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(data)) {
      for (final CalcTrans calculMetier : data) {

        final CrueDaoDCLMContents.CalculTransitoirePersist calculPersist = new CrueDaoDCLMContents.CalculTransitoirePersist();
        calculPersist.Nom = calculMetier.getNom();
        calculPersist.Commentaire = calculMetier.getCommentaire();

        calculPersist.listeElementsCalculTransitoire = new ArrayList<>();
        convertMetierToDaoTransNoeudQapp(calculMetier, calculPersist);
        convertMetierToDaoTransNoeudQappExt(calculMetier, calculPersist, analyser, newDclmSupported);
        convertMetierToDaoTransNoeudNiveauContinuLimnigramme(calculMetier, calculPersist);
        convertMetierToDaoTransNoeudNiveauContinuTarage(calculMetier, calculPersist);
        convertMetierToDaoTransBrancheOrificeManoeuvre(calculMetier, calculPersist);
        convertMetierToDaoTransBrancheSaintVenantQruis(calculMetier, calculPersist);
        convertMetierToDaoTransCasierProfilQruis(calculMetier, calculPersist);
        convertMetierToDaoTransBrancheOrificeManoeuvreRegul(calculMetier, calculPersist, analyser, newDclmSupported);
        convertMetierToDaoTransNoeudBg1(calculMetier, calculPersist, analyser, newDclmSupported);
        convertMetierToDaoTransNoeudBg2(calculMetier, calculPersist, analyser, newDclmSupported);
        convertMetierToDaoTransNoeudUsine(calculMetier, calculPersist, analyser, newDclmSupported);
        convertMetierToDaoTransNoeudBg1Av(calculMetier, calculPersist, analyser, newDclmSupported);
        convertMetierToDaoTransNoeudBg2Av(calculMetier, calculPersist, analyser, newDclmSupported);
        listePersistante.add(calculPersist);
      }
    }

    return listePersistante;
  }

  private static void convertMetierToDaoTransNoeudQapp(CalcTrans calculMetier, CrueDaoDCLMContents.CalculTransitoirePersist calculPersist) {
    for (final CalcTransNoeudQapp in : calculMetier.getDclm(CalcTransNoeudQapp.class)) {
      final CrueDaoDCLMContents.CalcTransNoeudNiveauContinuHydrogrammePersist dao = new CrueDaoDCLMContents.CalcTransNoeudNiveauContinuHydrogrammePersist();
      dao.HydrogrammeQapp = new CrueDaoDCLMContents.HydrogrammeQappPersist();
      dao.HydrogrammeQapp.NomRef = in.getLoi().getNom();
      dao.NomRef = in.getEmh().getNom();
      dao.IsActive = in.getUserActive();
      calculPersist.listeElementsCalculTransitoire.add(dao);
    }
  }

  private void convertMetierToDaoTransNoeudQappExt(CalcTrans calculMetier, CrueDaoDCLMContents.CalculTransitoirePersist calculPersist, CtuluLog analyser, boolean newDclmSupported) {
    List<CalcTransNoeudQappExt> qappExts = calculMetier.getDclm(CalcTransNoeudQappExt.class);
    if (!newDclmSupported && !qappExts.isEmpty()) {
      addWarningNewDclmNotSupported("CalcTransNoeudQappExt", analyser);
    }
    if (newDclmSupported) {
      for (CalcTransNoeudQappExt in : qappExts) {
        final CrueDaoDCLMContents.CalcTransNoeudQappExtPersist dao = new CrueDaoDCLMContents.CalcTransNoeudQappExtPersist();
        dao.HydrogrammeQappExt = new CrueDaoDCLMContents.HydrogrammeQappExtPersist();
        dao.HydrogrammeQappExt.NomFic = in.getNomFic();
        dao.HydrogrammeQappExt.NomRef = in.getSection();
        dao.HydrogrammeQappExt.ResCalcTrans = in.getResCalcTrans();
        dao.NomRef = in.getEmh().getNom();
        dao.IsActive = in.getUserActive();
        calculPersist.listeElementsCalculTransitoire.add(dao);
      }
    }
  }

  private static void convertMetierToDaoTransNoeudNiveauContinuLimnigramme(CalcTrans calculMetier, CrueDaoDCLMContents.CalculTransitoirePersist calculPersist) {
    for (final CalcTransNoeudNiveauContinuLimnigramme in : calculMetier.getDclm(CalcTransNoeudNiveauContinuLimnigramme.class)) {
      final CrueDaoDCLMContents.CalcTransNoeudNiveauContinuLimnigrammePersist dao = new CrueDaoDCLMContents.CalcTransNoeudNiveauContinuLimnigrammePersist();
      dao.Limnigramme = new CrueDaoDCLMContents.LimnigrammePersist();
      dao.Limnigramme.NomRef = in.getLoi().getNom();
      dao.NomRef = in.getEmh().getNom();
      dao.IsActive = in.getUserActive();
      calculPersist.listeElementsCalculTransitoire.add(dao);
    }
  }

  private static void convertMetierToDaoTransNoeudNiveauContinuTarage(CalcTrans calculMetier, CrueDaoDCLMContents.CalculTransitoirePersist calculPersist) {
    for (final CalcTransNoeudNiveauContinuTarage in : calculMetier.getDclm(CalcTransNoeudNiveauContinuTarage.class)) {
      final CrueDaoDCLMContents.CalcTransNoeudNiveauContinuTaragePersist dao = new CrueDaoDCLMContents.CalcTransNoeudNiveauContinuTaragePersist();
      dao.Tarage = new CrueDaoDCLMContents.TarragePersist();
      dao.Tarage.NomRef = in.getLoi().getNom();
      dao.NomRef = in.getEmh().getNom();
      dao.IsActive = in.getUserActive();
      calculPersist.listeElementsCalculTransitoire.add(dao);
    }
  }

  private static void convertMetierToDaoTransBrancheOrificeManoeuvre(CalcTrans calculMetier, CrueDaoDCLMContents.CalculTransitoirePersist calculPersist) {
    for (final CalcTransBrancheOrificeManoeuvre in : calculMetier.getDclm(CalcTransBrancheOrificeManoeuvre.class)) {
      final CrueDaoDCLMContents.CalcTransBrancheOrificeManoeuvrePersist dao = new CrueDaoDCLMContents.CalcTransBrancheOrificeManoeuvrePersist();
      dao.Manoeuvre = new CrueDaoDCLMContents.ManoeuvrePersist();
      dao.Manoeuvre.NomRef = in.getLoi().getNom();
      dao.NomRef = in.getEmh().getNom();
      dao.SensOuv = in.getSensOuv();
      dao.IsActive = in.getUserActive();
      calculPersist.listeElementsCalculTransitoire.add(dao);
    }
  }

  private static void convertMetierToDaoTransBrancheSaintVenantQruis(CalcTrans calculMetier, CrueDaoDCLMContents.CalculTransitoirePersist calculPersist) {
    for (final CalcTransBrancheSaintVenantQruis in : calculMetier.getDclm(CalcTransBrancheSaintVenantQruis.class)) {
      final CrueDaoDCLMContents.CalcTransBrancheSaintVenantHydrogrammeRuisPersist dao = new CrueDaoDCLMContents.CalcTransBrancheSaintVenantHydrogrammeRuisPersist();
      dao.HydrogrammeQruis = new CrueDaoDCLMContents.HydrogrammeQruisPersist();
      if (in.getLoi() != null) {
        dao.HydrogrammeQruis.NomRef = in.getLoi().getNom();
      }
      dao.NomRef = in.getEmh().getNom();
      dao.IsActive = in.getUserActive();
      calculPersist.listeElementsCalculTransitoire.add(dao);
    }
  }

  private static void convertMetierToDaoTransCasierProfilQruis(CalcTrans calculMetier, CrueDaoDCLMContents.CalculTransitoirePersist calculPersist) {
    for (final CalcTransCasierProfilQruis in : calculMetier.getDclm(CalcTransCasierProfilQruis.class)) {
      final CrueDaoDCLMContents.CalcTransCasierProfilHydrogrammeRuisPersist dao = new CrueDaoDCLMContents.CalcTransCasierProfilHydrogrammeRuisPersist();
      dao.HydrogrammeQruis = new CrueDaoDCLMContents.HydrogrammeQruisPersist();
      dao.HydrogrammeQruis.NomRef = in.getLoi().getNom();
      dao.NomRef = in.getEmh().getNom();
      dao.IsActive = in.getUserActive();
      calculPersist.listeElementsCalculTransitoire.add(dao);
    }
  }

  private void addWarningNewDclmNotSupported(String title, CtuluLog analyser) {
    if (!warningDone.contains(title)) {
      analyser.addWarn("io.dclm.newDclmNotSupported", title, coeurConfig.getXsdVersion());
      warningDone.add(title);
    }
  }

  private void convertMetierToDaoTransBrancheOrificeManoeuvreRegul(CalcTrans calculMetier, CrueDaoDCLMContents.CalculTransitoirePersist calculPersist, CtuluLog analyser, boolean newDclmSupported) {
    List<CalcTransBrancheOrificeManoeuvreRegul> list = calculMetier.getDclm(CalcTransBrancheOrificeManoeuvreRegul.class);
    if (!newDclmSupported && !list.isEmpty()) {
      addWarningNewDclmNotSupported("CalcTransBrancheOrificeManoeuvreRegul", analyser);
    }
    if (newDclmSupported) {
      for (final CalcTransBrancheOrificeManoeuvreRegul in : list) {
        final CrueDaoDCLMContents.CalcTransBrancheOrificeManoeuvreRegulPersist dao = new CrueDaoDCLMContents.CalcTransBrancheOrificeManoeuvreRegulPersist();
        dao.ManoeuvreRegul = new CrueDaoDCLMContents.ManoeuvreRegulPersist();
        dao.ManoeuvreRegul.NomRef = in.getLoi().getNom();
        dao.ManoeuvreRegul.Param = in.getParam();
        dao.NomRef = in.getEmh().getNom();
        dao.IsActive = in.getUserActive();
        dao.SensOuv = in.getSensOuv();
        calculPersist.listeElementsCalculTransitoire.add(dao);

      }
    }
  }

  private void convertMetierToDaoTransNoeudBg2(CalcTrans calculMetier, CrueDaoDCLMContents.CalculTransitoirePersist calculPersist, CtuluLog analyser, boolean newDclmSupported) {
    List<CalcTransNoeudBg2> list = calculMetier.getDclm(CalcTransNoeudBg2.class);
    if (!newDclmSupported && !list.isEmpty()) {
      addWarningNewDclmNotSupported("CalcTransNoeudBg2", analyser);
    }
    if (newDclmSupported) {
      for (final CalcTransNoeudBg2 in : list) {
        final CrueDaoDCLMContents.CalcTransNoeudBg2Persist dao = new CrueDaoDCLMContents.CalcTransNoeudBg2Persist();
        dao.NomRef = in.getEmh().getNom();
        dao.IsActive = in.getUserActive();
        calculPersist.listeElementsCalculTransitoire.add(dao);
      }
    }
  }

  private void convertMetierToDaoTransNoeudBg2Av(CalcTrans calculMetier, CrueDaoDCLMContents.CalculTransitoirePersist calculPersist, CtuluLog analyser, boolean newDclmSupported) {
    List<CalcTransNoeudBg2Av> list = calculMetier.getDclm(CalcTransNoeudBg2Av.class);
    if (!newDclmSupported && !list.isEmpty()) {
      addWarningNewDclmNotSupported("CalcTransNoeudBg2Av", analyser);
    }
    if (newDclmSupported) {
      for (final CalcTransNoeudBg2Av in : list) {
        final CrueDaoDCLMContents.CalcTransNoeudBg2AvPersist dao = new CrueDaoDCLMContents.CalcTransNoeudBg2AvPersist();
        dao.NomRef = in.getEmh().getNom();
        dao.IsActive = in.getUserActive();
        calculPersist.listeElementsCalculTransitoire.add(dao);
      }
    }
  }

  private void convertMetierToDaoTransNoeudBg1Av(CalcTrans calculMetier, CrueDaoDCLMContents.CalculTransitoirePersist calculPersist, CtuluLog analyser, boolean newDclmSupported) {
    List<CalcTransNoeudBg1Av> list = calculMetier.getDclm(CalcTransNoeudBg1Av.class);
    if (!newDclmSupported && !list.isEmpty()) {
      addWarningNewDclmNotSupported("CalcTransNoeudBg1Av", analyser);
    }
    if (newDclmSupported) {
      for (final CalcTransNoeudBg1Av in : list) {
        final CrueDaoDCLMContents.CalcTransNoeudBg1AvPersist dao = new CrueDaoDCLMContents.CalcTransNoeudBg1AvPersist();
        dao.NomRef = in.getEmh().getNom();
        dao.IsActive = in.getUserActive();
        calculPersist.listeElementsCalculTransitoire.add(dao);
      }
    }
  }

  private void convertMetierToDaoTransNoeudUsine(CalcTrans calculMetier, CrueDaoDCLMContents.CalculTransitoirePersist calculPersist, CtuluLog analyser, boolean newDclmSupported) {
    List<CalcTransNoeudUsine> list = calculMetier.getDclm(CalcTransNoeudUsine.class);
    if (!newDclmSupported && !list.isEmpty()) {
      addWarningNewDclmNotSupported("CalcTransNoeudUsine", analyser);
    }
    if (newDclmSupported) {
      for (final CalcTransNoeudUsine in : list) {
        final CrueDaoDCLMContents.CalcTransNoeudUsinePersist dao = new CrueDaoDCLMContents.CalcTransNoeudUsinePersist();
        dao.NomRef = in.getEmh().getNom();
        dao.IsActive = in.getUserActive();
        calculPersist.listeElementsCalculTransitoire.add(dao);
      }
    }
  }

  private void convertMetierToDaoTransNoeudBg1(CalcTrans calculMetier, CrueDaoDCLMContents.CalculTransitoirePersist calculPersist, CtuluLog analyser, boolean newDclmSupported) {
    List<CalcTransNoeudBg1> calcTransNoeudBg1List = calculMetier.getDclm(CalcTransNoeudBg1.class);
    if (!newDclmSupported && !calcTransNoeudBg1List.isEmpty()) {
      addWarningNewDclmNotSupported("CalcTransNoeudBg1", analyser);
    }
    if (newDclmSupported) {
      for (final CalcTransNoeudBg1 in : calcTransNoeudBg1List) {
        final CrueDaoDCLMContents.CalcTransNoeudBg1Persist dao = new CrueDaoDCLMContents.CalcTransNoeudBg1Persist();
        dao.NomRef = in.getEmh().getNom();
        dao.IsActive = in.getUserActive();
        calculPersist.listeElementsCalculTransitoire.add(dao);
      }
    }
  }
}
