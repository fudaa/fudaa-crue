/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.etu;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;

/**
 *
 * @author deniger
 */
public class FichierCrueComparator extends SafeComparator<FichierCrue> {

  public static Map<CrueFileType, Integer> createPosition() {
    final Map<CrueFileType, Integer> res = new EnumMap<>(CrueFileType.class);
    final List<CrueFileType> typesInOrder = new ArrayList<>();
    typesInOrder.add(CrueFileType.OCAL);
    typesInOrder.add(CrueFileType.ORES);
    typesInOrder.add(CrueFileType.PCAL);
    typesInOrder.add(CrueFileType.DCLM);
    typesInOrder.add(CrueFileType.DLHY);
    typesInOrder.add(CrueFileType.LHPT);
    typesInOrder.add(CrueFileType.OPTR);
    typesInOrder.add(CrueFileType.OPTG);
    typesInOrder.add(CrueFileType.OPTI);
    typesInOrder.add(CrueFileType.PNUM);
    typesInOrder.add(CrueFileType.DPTI);
    typesInOrder.add(CrueFileType.DREG);
    typesInOrder.add(CrueFileType.DC);
    typesInOrder.add(CrueFileType.DH);
    typesInOrder.add(CrueFileType.DRSO);
    typesInOrder.add(CrueFileType.DCSP);
    typesInOrder.add(CrueFileType.DPTG);
    typesInOrder.add(CrueFileType.DFRT);
    final int size = typesInOrder.size();
    for (int i = 0; i < size; i++) {
      res.put(typesInOrder.get(i), Integer.valueOf(i));
    }
    return res;
  }
  private final Map<CrueFileType, Integer> fileTypePosition = createPosition();

  @Override
  protected int compareSafe(final FichierCrue o1, final FichierCrue o2) {
    final Integer i1 = fileTypePosition.get(o1.getType());
    if (i1 == null) {
      throw new IllegalStateException("type not found " + o1.getType());
    }
    final Integer i2 = fileTypePosition.get(o2.getType());
    if (i2 == null) {
      throw new IllegalStateException("type not found " + o2.getType());
    }
    return i1.intValue() - i2.intValue();



  }
}
