/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.res;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Donne pour chaque Type d'EMH les informations nécessaires.
 *
 * @author deniger
 */
public class ResCatEMHContent {

  /**
   * la position en bytes de la categorie: nbrMot * 8. Le premier enregistrement doit être le délimiteur de la catégorie.
   */
  private long positionBytes;
  /**
   * les types contenus:
   */
  private List<ResTypeEMHContent> resTypeEMHContent;
  private String delimiteur;

  public String getDelimiteur() {
    return delimiteur;
  }

  public List<String> getAllRef() {
    List<String> allRef = new ArrayList<>();
    getAllRef(allRef);
    return allRef;
  }

  public void getAllRef(Collection<String> collector) {
    if (resTypeEMHContent != null) {
      for (ResTypeEMHContent type : resTypeEMHContent) {
        type.getAllRef(collector);
      }
    }
  }

  public long getPositionInBytes() {
    return positionBytes;
  }

  public List<ResTypeEMHContent> getResTypeEMHContent() {
    return resTypeEMHContent;
  }

  void setDelimiteur(String delimiteur) {
    this.delimiteur = delimiteur;
  }

  void setPositionBytes(long absolutePositionBytes) {
    this.positionBytes = absolutePositionBytes;
  }

 public void setResTypeEMHContent(List<ResTypeEMHContent> resTypeEMHContent) {
    this.resTypeEMHContent = resTypeEMHContent;
  }
}
