/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dlhy;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.AbstractDaoFloatLoi;
import org.fudaa.dodico.crue.io.common.AbstractDaoLoi;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * Structures utilisees dans la classe CrueDaoPersistDLHY
 *
 * @author Adrien Hadoux, Carole Delhaye
 */
@SuppressWarnings("PMD.VariableNamingConventions")
public class CrueDaoStructureDLHY implements CrueDataDaoStructure {

    private final String version;

    /**
     * @param version
     */
    public CrueDaoStructureDLHY(final String version) {
        super();
        this.version = version;
    }

    @Override
    public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {

        xstream.alias(CrueFileType.DLHY.toString(), CrueDaoDLHY.class);
        xstream.alias("LoiFF", AbstractDaoFloatLoi.DaoLoiFF.class);
        xstream.alias("LoiDF", AbstractDaoFloatLoi.DaoLoiDF.class);
        xstream.omitField(AbstractDaoFloatLoi.DaoLoiFF.class, "DateZeroLoiDF");
        xstream.useAttributeFor(AbstractDaoLoi.class, "Nom");
        AbstractDaoFloatLoi.configureXstream(xstream, props, version);
    }

}
