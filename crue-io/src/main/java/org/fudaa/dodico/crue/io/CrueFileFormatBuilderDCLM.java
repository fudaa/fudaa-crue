/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.dclm.CrueConverterDCLM;
import org.fudaa.dodico.crue.io.dclm.CrueDaoStructureDCLM;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.DonCLimMScenario;

/**
 * @author deniger
 */
public class CrueFileFormatBuilderDCLM implements CrueFileFormatBuilder<DonCLimMScenario> {

  @Override
  public Crue10FileFormat<DonCLimMScenario> getFileFormat(CoeurConfigContrat coeurConfig) {
    final CrueDaoStructureDCLM crueDaoStructureDCLM = new CrueDaoStructureDCLM();
    final boolean ignoreIsActive = Crue10VersionConfig.V_1_1_1.equals(coeurConfig.getXsdVersion());
    if (ignoreIsActive) {
      crueDaoStructureDCLM.initForV1P1P1();
    }

    return new Crue10FileFormat<>(new CrueDataXmlReaderWriterImpl<>(
        CrueFileType.DCLM, coeurConfig, new CrueConverterDCLM(ignoreIsActive, coeurConfig), crueDaoStructureDCLM));

  }

}
