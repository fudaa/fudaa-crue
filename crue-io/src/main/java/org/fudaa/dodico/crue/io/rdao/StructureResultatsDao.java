/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.fudaa.dodico.crue.io.rdao.CommonResDao.VariableResDao;

/**
 * @author deniger
 */
public class StructureResultatsDao extends CommonResDao.NbrMotDao {

  List<CommonResDao.VariableResDao> VariableRes;
  //attention, l'ordre est très important: doit suivre celui du fichier
  NoeudsDao Noeuds;
  CasiersDao Casiers;
  SectionsDao Sections;
  BranchesDao Branches;
  ModelesDao Modeles;
  List<ResContainerEMHCat> resContainerEMHCat;

  public List<ResContainerEMHCat> getResContainerEMHCat() {
    if (resContainerEMHCat == null) {
      //attention, l'ordre est très important: doit suivre celui du fichier
      if (Modeles == null) {
        resContainerEMHCat = Collections.unmodifiableList(Arrays.<ResContainerEMHCat>asList(Noeuds, Casiers, Sections, Branches));
      } else {
        resContainerEMHCat = Collections.unmodifiableList(Arrays.<ResContainerEMHCat>asList(Noeuds, Casiers, Sections, Branches, Modeles));
      }
    }
    return resContainerEMHCat;

  }

  public List<VariableResDao> getVariableRes() {
    return VariableRes;
  }

  public BranchesDao getBranches() {
    return Branches;
  }

  public CasiersDao getCasiers() {
    return Casiers;
  }

  public NoeudsDao getNoeuds() {
    return Noeuds;
  }

  public SectionsDao getSections() {
    return Sections;
  }

  public ModelesDao getModeles() {
    return Modeles;
  }
}
