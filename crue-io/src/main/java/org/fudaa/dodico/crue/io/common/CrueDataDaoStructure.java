/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.common;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.loi.LoiTypeContainer;

/**
 * Interface pour les structures des DAO
 *
 * @author deniger
 */
public interface CrueDataDaoStructure {

  /**
   * @param xstream le flux a configurer
   * @param ctuluLog l'anayze recevant les erreurs
   * @param props les propriétés utilisées pour récuperer les types de loi.
   */
  void configureXStream(XStream xstream, CtuluLog ctuluLog, LoiTypeContainer props);
}
