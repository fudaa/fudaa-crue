/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.common;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.metier.emh.Avancement;
import org.fudaa.dodico.crue.metier.emh.Resultat;
import org.fudaa.dodico.crue.metier.emh.Sorties;
import org.fudaa.dodico.crue.metier.emh.Trace;

/**
 * Plusieurs methodes et classes communes utilisees pour les factory.
 *
 * @author Adrien Hadoux
 */
public class CrueHelper {


  /**
   * Message qui indique la balise n'est pas reconnue par les structures de données EMH.
   */
  public static void unknowEMH(final String balise, final String nomEMH, final CtuluLog out) {
    out.addInfo("io.unknownEMH.error", balise, nomEMH);
  }

  public static void unknowdataFromFile(final String balise, final String nom, final CtuluLog out) {
    out.addInfo("io.unknownDataFromFile.error", balise, nom); //$NON-NLS-1$
  }

  /**
   * Message qui indique la balise n'est pas reconnue par les structures de données EMH.
   */
  public static void unknowReference(final String balise, final String nomRef, final CtuluLog out) {
    out.addWarn("io.unknownReference.error", balise, nomRef, new Throwable()); //$NON-NLS-1$ //NOPMD

  }

  /**
   * Envoie un message formatte pour une erreur de doublons d'id
   *
   * @param out l'analyse recevant le message
   */
  public static void errorIdNonUnique(final String balise, final String nomRef, final CtuluLog out) {
    out.addInfo("io.idNotUnique.error", balise, nomRef); //$NON-NLS-1$

  }

  public static void messageBaliseError(final String balise, final String message, final CtuluLog out) {
    out.addInfo(message, balise);

  }


  public static void emhEmpty(final String balise, final CtuluLog out) {
    out.addInfo("io.emptyEMH.error", balise, new Throwable()); //$NON-NLS-1$ //NOPMD

  }

  public static boolean copyAnalyzer(final CtuluLog one, final CtuluLog tocopy) {
    if (one == null || tocopy == null) {
      return false;
    }
    one.merge(tocopy);
    return true;
  }

  public static void configureSorties(final XStream xstream, final CtuluLog log) {
    xstream.alias(Sorties.class.getSimpleName(), Sorties.class);
    xstream.alias(Avancement.class.getSimpleName(), Avancement.class);
    xstream.alias(Trace.class.getSimpleName(), Trace.class);
    xstream.alias(Resultat.class.getSimpleName(), Resultat.class);
    xstream.aliasField("Avancement", Sorties.class, "avancement");
    xstream.aliasField("SortieFichier", Avancement.class, "sortieFichier");
    xstream.aliasField("Trace", Sorties.class, "trace");
    xstream.aliasField("SortieEcran", Trace.class, "sortieEcran");
    xstream.aliasField("SortieFichier", Trace.class, "sortieFichier");
    xstream.aliasField("VerbositeEcran", Trace.class, "verbositeEcran");
    xstream.aliasField("VerbositeFichier", Trace.class, "verbositeFichier");

    xstream.aliasField("Resultat", Sorties.class, "resultat");
    xstream.aliasField("SortieFichier", Resultat.class, "sortieFichier");
  }
}
