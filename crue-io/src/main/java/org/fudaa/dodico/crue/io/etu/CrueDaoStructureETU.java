/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.etu;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.SingleValueConverter;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.etu.Ref.DCLMRef;
import org.fudaa.dodico.crue.io.etu.Ref.DCRef;
import org.fudaa.dodico.crue.io.etu.Ref.DCSPRef;
import org.fudaa.dodico.crue.io.etu.Ref.DFRTRef;
import org.fudaa.dodico.crue.io.etu.Ref.DHRef;
import org.fudaa.dodico.crue.io.etu.Ref.DLHYRef;
import org.fudaa.dodico.crue.io.etu.Ref.DPTGRef;
import org.fudaa.dodico.crue.io.etu.Ref.DPTIRef;
import org.fudaa.dodico.crue.io.etu.Ref.DREGRef;
import org.fudaa.dodico.crue.io.etu.Ref.DRSORef;
import org.fudaa.dodico.crue.io.etu.Ref.ModeleRef;
import org.fudaa.dodico.crue.io.etu.Ref.OCALRef;
import org.fudaa.dodico.crue.io.etu.Ref.OPTGRef;
import org.fudaa.dodico.crue.io.etu.Ref.OPTIRef;
import org.fudaa.dodico.crue.io.etu.Ref.OPTRRef;
import org.fudaa.dodico.crue.io.etu.Ref.ORESRef;
import org.fudaa.dodico.crue.io.etu.Ref.PCALRef;
import org.fudaa.dodico.crue.io.etu.Ref.PNUMRef;
import org.fudaa.dodico.crue.io.etu.Ref.RefConverter;
import org.fudaa.dodico.crue.io.etu.Ref.RunRef;
import org.fudaa.dodico.crue.io.etu.Ref.SousModeleRef;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * @author Adrien Hadoux
 */
public class CrueDaoStructureETU implements CrueDataDaoStructure {

  private final String version;

  public CrueDaoStructureETU(final String version) {
    this.version = version;
  }
  
  

  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    initXmlParserForETU(xstream);
  }

  private void initXmlParserForETU(final XStream xstream) {
    // -- creation des alias pour que ce soit + parlant dans le xml file --//
    xstream.alias(CrueFileType.ETU.toString(), CrueDaoETU.class);
    xstream.omitField(CrueDaoETU.class, "VersionCrue");
    initXmlParserForScenario(xstream);
    initXmlParserForRepertoires(xstream);
    initXmlParserForFichiers(xstream);
    initXmlParserForSousModele(xstream);
    initXmlParserForModele(xstream);
    initXmlParserForReference(xstream);
    initXmlParserForRun(xstream);
  }

  private void initXmlParserForScenario(final XStream xstream) {
    xstream.alias("ScenarioCourant", ScenarioCourant.class);
    xstream.useAttributeFor(ScenarioCourant.class, "NomRef");

    xstream.alias("Scenario", Scenario.class);
    xstream.aliasAttribute(Scenario.class, "Modeles", "Scenario-Modeles");
    xstream.aliasAttribute(Scenario.class, "FichEtudes", "Scenario-FichEtudes");
    xstream.aliasAttribute(Scenario.class, "ScenarioScenarioV9", "Scenario-ScenarioV9");
    if (Crue10VersionConfig.isV1_1_1(version)) {
      xstream.omitField(Scenario.class, "ScenarioScenarioV9");
    }
    xstream.useAttributeFor(Scenario.class, "Nom");

    xstream.alias("ScenarioV9", ScenarioV9.class);
    xstream.aliasField("Scenario-ModeleV9", ScenarioV9.class, "ScenarioModeleV9");
    xstream.omitField(ScenarioV9.class, "ScenarioScenarioV9");
    xstream.aliasField("RunV9s", ScenarioV9.class, "Runs");
    xstream.useAttributeFor("Nom", ScenarioV9.class);

  }

  private void initXmlParserForRepertoires(final XStream xstream) {
    xstream.alias("Repertoire", Repertoire.class);
    xstream.useAttributeFor(Repertoire.class, "Nom");
    xstream.registerConverter(new RepertoireConverter());
  }

  public void initXmlParserForFichiers(final XStream xstream) {

    xstream.alias("FichEtude", TypeFichierDispo.class);

    xstream.useAttributeFor(TypeFichierDispo.class, "Nom");
    xstream.useAttributeFor(TypeFichierDispo.class, "Chemin");
    xstream.useAttributeFor(TypeFichierDispo.class, "Type");
  }

  public void initXmlParserForSousModele(final XStream xstream) {
    // --Ce fonctionnement ne peut etre ok que dans le cas de la lecture seule. --//
    xstream.alias("SousModele", SousModele.class);
    xstream.useAttributeFor(SousModele.class, "Nom");
    xstream.aliasAttribute(SousModele.class, "SousModeleFichEtudes", "SousModele-FichEtudes");

  }

  public void initXmlParserForModele(final XStream xstream) {
    // --Ce fonctionnement ne peut etre ok que dans le cas de la lecture seule. --//
    xstream.alias("Modele", Modele.class);
    xstream.alias("ModeleV9", ModeleV9.class);

    xstream.aliasField("ModeleV9-FichEtudes", ModeleV9.class, "FichEtudes");
    xstream.aliasAttribute(Modele.class, "FichEtudes", "Modele-FichEtudes");
    xstream.aliasAttribute(Modele.class, "SousModeles", "Modele-SousModeles");

    xstream.useAttributeFor(Modele.class, "Nom");
    xstream.useAttributeFor("Nom", ModeleV9.class);

  }

  public void initXmlParserForReference(final XStream xstream) {
    xstream.useAttributeFor(Ref.class, "NomRef");
    xstream.registerConverter(new RefConverter());

    xstream.alias(CrueFileType.DC.toString(), DCRef.class);
    xstream.alias(CrueFileType.DH.toString(), DHRef.class);
    xstream.alias(CrueFileType.DCLM.toString(), DCLMRef.class);
    xstream.alias(CrueFileType.DCSP.toString(), DCSPRef.class);
    xstream.alias(CrueFileType.DFRT.toString(), DFRTRef.class);
    xstream.alias(CrueFileType.DLHY.toString(), DLHYRef.class);
    xstream.alias(CrueFileType.DPTG.toString(), DPTGRef.class);
    xstream.alias(CrueFileType.DPTI.toString(), DPTIRef.class);
    xstream.alias(CrueFileType.DREG.toString(), DREGRef.class);
    xstream.alias(CrueFileType.DRSO.toString(), DRSORef.class);
    xstream.alias(CrueFileType.PNUM.toString(), PNUMRef.class);
    xstream.alias(CrueFileType.PCAL.toString(), PCALRef.class);
    xstream.alias(CrueFileType.OCAL.toString(), OCALRef.class);
    xstream.alias(CrueFileType.OPTG.toString(), OPTGRef.class);
    xstream.alias(CrueFileType.OPTI.toString(), OPTIRef.class);
    xstream.alias(CrueFileType.OPTR.toString(), OPTRRef.class);
    xstream.alias(CrueFileType.ORES.toString(), ORESRef.class);

    // -- dans la balise <Modele> --//
    xstream.alias("Modele-SousModele", SousModeleRef.class);
    // -- dans la balise <Scenario> --//
    xstream.alias("Scenario-Modele", ModeleRef.class);
    xstream.alias("Scenario-ModeleV9", ScenarioModeleV9Ref.class);
    // -- dans la balise <Scenario> --//
    xstream.alias("RunCourant", RunRef.class);

  }

  public void initXmlParserForRun(final XStream xstream) {
    xstream.alias("Run", Run.class);
    xstream.omitField(Run.class, "Run-FichRuns");
    xstream.useAttributeFor(Run.class, "Nom");
    xstream.alias("RunV9", RunV9.class);
    xstream.useAttributeFor("Nom", RunV9.class);

  }

  // -------------------------------------- SCENARIO --------------------------------------//
  public static class ScenarioCourant {

    String NomRef;
  }

  public static class ScenarioModeleV9Ref extends Ref {
  }

  public static class ScenarioV9 extends Scenario {
  }

  public static class Scenario implements CrueEtuInfosContainer {

    public String Nom;
    public String Type;
    public Boolean IsActive = Boolean.TRUE;
    public String VersionCrue;
    public String Commentaire;
    public String AuteurCreation;
    public String DateCreation;
    public String AuteurDerniereModif;
    public String DateDerniereModif;
    
    public List<Ref> FichEtudes;
    public List<Ref> Modeles;
    /**
     * optionnel: dans le cas de crue 9, il n'y a qu'un seul modele , pas de model enchaine.
     */
    ScenarioModeleV9Ref ScenarioModeleV9;
    public Ref ScenarioScenarioV9;
    public List<Run> Runs;
    public RunRef RunCourant;

    @Override
    public String getAuteurCreation() {
      return AuteurCreation;
    }

    @Override
    public String getAuteurDerniereModif() {
      return AuteurDerniereModif;
    }

    @Override
    public String getCommentaire() {
      return Commentaire;
    }

    @Override
    public String getDateCreation() {
      return DateCreation;
    }

    @Override
    public String getDateDerniereModif() {
      return DateDerniereModif;
    }

    @Override
    public String getType() {
      return Type;
    }

    public void setType(final String type) {
      Type = type;
    }

    @Override
    public void setCrueVersion(final CrueVersionType version) {
      Type = version.getTypeId();
    }

    @Override
    public void setCommentaire(final String commentaire) {
      Commentaire = commentaire;
    }

    @Override
    public void setAuteurCreation(final String auteurCreation) {
      AuteurCreation = auteurCreation;
    }

    @Override
    public void setDateCreation(final String dateCreation) {
      DateCreation = dateCreation;
    }

    @Override
    public void setAuteurDerniereModif(final String auteurDerniereModif) {
      AuteurDerniereModif = auteurDerniereModif;
    }

    @Override
    public void setDateDerniereModif(final String dateDerniereModif) {
      DateDerniereModif = dateDerniereModif;
    }
  }

  // -------------------------------------- Repertoires --------------------------------------//
  public static class Repertoire implements Comparable<Repertoire> {

    public String Nom;
    public String path;

    @Override
    public int compareTo(final Repertoire o) {
      if (o == null) {
        return 1;
      }
      return StringUtils.defaultString(Nom).compareTo(o.Nom);
    }
  }

  public static class RepertoireConverter implements SingleValueConverter {

    @Override
    public String toString(final Object obj) {
      return ((Repertoire) obj).Nom;
    }

    @Override
    public Object fromString(final String name) {
      final Repertoire pf = new Repertoire();
      pf.Nom = name;
      return pf;
    }

    @Override
    public boolean canConvert(final Class type) {
      return type.isInstance(Repertoire.class);
    }
  }

  // -------------------------------------- TYPE FICHIER --------------------------------------//
  /**
   * classe abstraite qui gere tous les differents fichiers disponibles.
   *
   * @author Adrien Hadoux
   */
  public static class TypeFichierDispo {

    public String Nom;
    public String Chemin;
    /**
     * Type du fichier: DRSO,DCSP.... est lu au chargement du fichier mais pas à l'écriture.
     */
    public String Type;

    public String getType() {
      return Type;
    }

    public void setType(final String type) {
      this.Type = type;
    }
  }

  // -------------------------------------- SousModele --------------------------------------//
  public static class SousModele implements CrueEtuInfosContainer {

    public String Nom;
    public String Type;
    public Boolean IsActive = Boolean.TRUE;
    public String Commentaire;
    public String AuteurCreation;
    public String DateCreation;
    public String AuteurDerniereModif;
    public String DateDerniereModif;
    public List<Ref> SousModeleFichEtudes;

    @Override
    public String getAuteurDerniereModif() {
      return AuteurDerniereModif;
    }

    @Override
    public String getDateCreation() {
      return DateCreation;
    }

    @Override
    public String getDateDerniereModif() {
      return DateDerniereModif;
    }

    @Override
    public String getAuteurCreation() {
      return AuteurCreation;
    }

    @Override
    public String getCommentaire() {
      return Commentaire;
    }

    @Override
    public String getType() {
      return Type;
    }

    public void setType(final String type) {
      Type = type;
    }

    @Override
    public void setCrueVersion(final CrueVersionType version) {
      Type = version.getTypeId();
    }

    @Override
    public void setCommentaire(final String commentaire) {
      Commentaire = commentaire;
    }

    @Override
    public void setAuteurCreation(final String auteurCreation) {
      AuteurCreation = auteurCreation;
    }

    @Override
    public void setDateCreation(final String dateCreation) {
      DateCreation = dateCreation;
    }

    @Override
    public void setAuteurDerniereModif(final String auteurDerniereModif) {
      AuteurDerniereModif = auteurDerniereModif;
    }

    @Override
    public void setDateDerniereModif(final String dateDerniereModif) {
      DateDerniereModif = dateDerniereModif;
    }
  }

  // -------------------------------------- SousModele --------------------------------------//  
  public static class ModeleV9 extends Modele {
  }

  public static class Modele implements CrueEtuInfosContainer {

    public String Nom;
    public String Type;
    public Boolean IsActive = Boolean.TRUE;
    public String Commentaire;
    public String AuteurCreation;
    public String DateCreation;
    public String AuteurDerniereModif;
    public String DateDerniereModif;
    public List<Ref> FichEtudes;
    public List<Ref> SousModeles;

    @Override
    public String getAuteurDerniereModif() {
      return AuteurDerniereModif;
    }

    @Override
    public String getDateCreation() {
      return DateCreation;
    }

    @Override
    public String getDateDerniereModif() {
      return DateDerniereModif;
    }

    @Override
    public String getAuteurCreation() {
      return AuteurCreation;
    }

    @Override
    public String getCommentaire() {
      return Commentaire;
    }

    @Override
    public String getType() {
      return Type;
    }

    public void setType(final String type) {
      Type = type;
    }

    @Override
    public void setCrueVersion(final CrueVersionType version) {
      Type = version.getTypeId();
    }

    @Override
    public void setCommentaire(final String commentaire) {
      Commentaire = commentaire;
    }

    @Override
    public void setAuteurCreation(final String auteurCreation) {
      AuteurCreation = auteurCreation;
    }

    @Override
    public void setDateCreation(final String dateCreation) {
      DateCreation = dateCreation;
    }

    @Override
    public void setAuteurDerniereModif(final String auteurDerniereModif) {
      AuteurDerniereModif = auteurDerniereModif;
    }

    @Override
    public void setDateDerniereModif(final String dateDerniereModif) {
      DateDerniereModif = dateDerniereModif;
    }
  }

  // -------------------------------------- RUNS --------------------------------------//
  public static class RunV9 extends Run {
  }

  public static class Run implements CrueEtuInfosContainer {

    public String Nom;
    public String Commentaire = StringUtils.EMPTY;
    public String AuteurCreation;
    public String DateCreation;
    public String AuteurDerniereModif;
    public String DateDerniereModif;

    @Override
    public String getAuteurDerniereModif() {
      return AuteurDerniereModif;
    }

    @Override
    public String getDateCreation() {
      return DateCreation;
    }

    @Override
    public String getDateDerniereModif() {
      return DateDerniereModif;
    }

    @Override
    public String getAuteurCreation() {
      return AuteurCreation;
    }

    @Override
    public String getCommentaire() {
      return Commentaire;
    }

    @Override
    public String getType() {
      return null;
    }

    @Override
    public void setCrueVersion(final CrueVersionType version) {
    }

    @Override
    public void setCommentaire(final String commentaire) {
      Commentaire = commentaire;
    }

    @Override
    public void setAuteurCreation(final String auteurCreation) {
      AuteurCreation = auteurCreation;
    }

    @Override
    public void setDateCreation(final String dateCreation) {
      DateCreation = dateCreation;
    }

    @Override
    public void setAuteurDerniereModif(final String auteurDerniereModif) {
      AuteurDerniereModif = auteurDerniereModif;
    }

    @Override
    public void setDateDerniereModif(final String dateDerniereModif) {
      DateDerniereModif = dateDerniereModif;
    }
  }
}
