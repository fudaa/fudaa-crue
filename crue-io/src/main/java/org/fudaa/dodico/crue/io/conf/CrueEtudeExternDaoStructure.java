package org.fudaa.dodico.crue.io.conf;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueDaoStructure;

/**
 * @author CANEL Christophe
 */
public class CrueEtudeExternDaoStructure implements CrueDaoStructure {

  /**
   * {@inheritDoc}
   */
  @Override
  public void configureXStream(final XStream xstream, final CtuluLog analyze) {
    xstream.alias("ConfigEtudeExtern", CrueEtudeExternDaoConfiguration.class);
    xstream.omitField(CrueEtudeExternDaoConfiguration.class, "Commentaire");
    xstream.alias("Ressource", CrueEtudeExternRessourceInfos.class);
    xstream.addImplicitCollection(CrueEtudeExternRessourceInfos.class, "Options");
    xstream.addImplicitCollection(CrueEtudeExternDaoConfiguration.class, "Ressources");
    this.configureEtudeOptionXStream(xstream);
  }

  public void configureEtudeOptionXStream(final XStream xstream) {
    xstream.alias("Option", Option.class);
    xstream.useAttributeFor(Option.class, "Nom");
  }
}
