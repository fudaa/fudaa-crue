/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.log;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.log.CrueDaoStructureLOG.Log;

/**
 * @author CANEL Christophe
 *
 */
public class CrueDaoLOG extends AbstractCrueDao {
  public String Description;
  public final List<Log> Logs = new ArrayList<>();
}
