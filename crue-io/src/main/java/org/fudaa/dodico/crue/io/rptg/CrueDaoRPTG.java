/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rptg;

import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.rdao.ContexteSimulationDao;
import org.fudaa.dodico.crue.io.rdao.ParametrageDao;
import org.fudaa.dodico.crue.io.rdao.ResContainerDao;
import org.fudaa.dodico.crue.io.rdao.StructureResultatsDao;

/**
 * 
 * @author Frédéric Deniger
 */
public class CrueDaoRPTG extends AbstractCrueDao implements ResContainerDao{

  ParametrageDao Parametrage;
  ContexteSimulationDao ContexteSimulation;
  StructureResultatsDao StructureResultat;
  List<ResPrtGeoDao> ResPrtGeos;

  @Override
  public ContexteSimulationDao getContexteSimulation() {
    return ContexteSimulation;
  }

  @Override
  public ParametrageDao getParametrage() {
    return Parametrage;
  }

  public List<ResPrtGeoDao> getResPrtGeos() {
    return ResPrtGeos;
  }

  @Override
  public StructureResultatsDao getStructureResultat() {
    return StructureResultat;
  }
}
