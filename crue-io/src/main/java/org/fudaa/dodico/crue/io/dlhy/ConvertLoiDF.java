package org.fudaa.dodico.crue.io.dlhy;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.io.common.AbstractDaoFloatLoi;
import org.fudaa.dodico.crue.io.common.AbstractDaoLoi;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.LoiDF;

/**
 * @author deniger
 */
public final class ConvertLoiDF implements ConvertLoi {

    @Override
    public AbstractDaoLoi metiertoDao(final AbstractLoi in) {
        final LoiDF inLoi = (LoiDF) in;
        final AbstractDaoFloatLoi.DaoLoiDF out = new AbstractDaoFloatLoi.DaoLoiDF();
        AbstractDaoFloatLoi.metierToDaoLoi(out, inLoi);
        out.setDateZeroLoiDF(inLoi.getDateZeroLoiDF() == null ? StringUtils.EMPTY : DateDurationConverter.dateToXsd(inLoi.getDateZeroLoiDF()));
        return out;
    }

    @Override
    public Loi daoToMetier(final AbstractDaoLoi in) {
        final AbstractDaoFloatLoi.DaoLoiDF loiDFAbstractPersist = (AbstractDaoFloatLoi.DaoLoiDF) in;
        final LoiDF outLoi = new LoiDF();
        AbstractDaoFloatLoi.daoToMetierLoi(outLoi, loiDFAbstractPersist);
        if (StringUtils.isNotBlank(loiDFAbstractPersist.getDateZeroLoiDF())) {
            outLoi.setDateZeroLoiDF(DateDurationConverter.getDate(loiDFAbstractPersist.getDateZeroLoiDF()));
        }
        return outLoi;
    }
}
