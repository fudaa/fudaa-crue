package org.fudaa.dodico.crue.io.dlhy;

import org.fudaa.dodico.crue.io.common.AbstractDaoLoi;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;

/**
 * Created by deniger on 09/06/2017.
 */
public interface ConvertLoi {

  AbstractDaoLoi metiertoDao(AbstractLoi in);

  AbstractLoi daoToMetier(AbstractDaoLoi in);
}
