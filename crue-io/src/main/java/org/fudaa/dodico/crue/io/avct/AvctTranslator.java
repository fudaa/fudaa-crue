/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.avct;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.CompteRenduAvancement;

/**
 *
 * @author deniger
 */
public class AvctTranslator {

  public static List<String> getNoms(final CompteRenduAvancement cr) {
    final List<String> res = new ArrayList<>(cr.getIndicateurs().size());
    final String code = cr.getCode();
    //en pré-traitement géométrique : 3 indicateurs
    //    indice de la passe de pré-traitement, nombre de passes nécessaires ;
    //    nombre de profils casiers traités (EMHCasierProfil) et nombre total de profils casier,
    //    nombre de sections traitées (EMHSectionProfil, Idem et Interpolee) et nombre total de sections à traiter ;
    if ("PTG".equals(code)) {
      res.add(BusinessMessages.getString("avct.ptg.1"));
      res.add(BusinessMessages.getString("avct.ptg.2"));
      res.add(BusinessMessages.getString("avct.ptg.3"));
    } //en pré-traitement des conditions initiales :
    //nombre de branches traitées et nombre total de branches à traiter,
    //nombre de sections traitées et nombre total de sections à traiter ;
    else if ("PTI".equals(code)) {
      res.add(BusinessMessages.getString("avct.pti.1"));
      res.add(BusinessMessages.getString("avct.pti.2"));
    } //en pré-traitement du réseau :
    //    avec ordre du DRSO : pas de traitements, donc pas d'indicateurs ;
    //    avec ré-ordonnancement des nœuds : ADU=des indices de convergence de l'algo de réordonnancement,
    else if ("PTR".equals(code)) {
      res.add(BusinessMessages.getString("avct.ptr.1"));
      
    } else if ("CALPP".equals(code)) {
    //en calcul pseudo-permanent : 4 indices
    //  l'indice du calcul par rapport au nombre total de calculs à lancer ;
    //  la mesure d'écart en cote de la surface libre servant à établir la convergence (valeur en cours, ie la plus forte variation de cote de la surface libre, et le seuil de convergence),
    //  la mesure d'écart en débit servant à établir la convergence (valeur en cours, ie la plus forte variation de débit, et seuil de convergence),
    //  le nombre d'itérations (itération en cours et nombre maximal d'itérations) ;
      res.add(BusinessMessages.getString("avct.calpp.1"));
      res.add(BusinessMessages.getString("avct.calpp.2"));
      res.add(BusinessMessages.getString("avct.calpp.3"));
      res.add(BusinessMessages.getString("avct.calpp.4"));
      //en calcul permanent : ADU (a priori : un indice = le niveau de convergence, avec un seuil comme valeur de référence),
      //    l'indice du calcul par rapport au nombre total de calculs à lancer ;
    } else if ("CALVP".equals(code)) {
      res.add(BusinessMessages.getString("avct.calvp.1"));
      //en calcul transitoire : 2 indices
      //    l'indice du calcul par rapport au nombre total de calculs à lancer ;
      //    la valeur du temps de simulation (temps en cours et durée du scénario).
    } else if ("CALTR".equals(code)) {
      res.add(BusinessMessages.getString("avct.caltr.1"));
      res.add(BusinessMessages.getString("avct.caltr.2"));
    }
    for (int i = res.size(); i < cr.getIndicateurs().size(); i++) {
      res.add(BusinessMessages.getString("avct.default", Integer.toString(i + 1)));
    }
    return res;
  }
}
