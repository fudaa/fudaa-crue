/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores.old;

/**
 *
 * @author Frederic Deniger
 */
public class DaoOrdResSectionInterpolee extends DaoOrdResSectionOld {

  public DaoOrdResSectionInterpolee() {
  }

  public DaoOrdResSectionInterpolee(final DaoOrdResSectionOld other) {
    super(other);
  }
  
}
