/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import java.util.EnumMap;
import java.util.Map;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * Gere la lecture du format crue 9.
 *
 * @author Adrien Hadoux
 */
public final class Crue9FileFormatFactory {

  /**
   * la map contenant les correspondance
   */
  private static final Map<CrueFileType, AbstractCrue9FileFormat> MAP = new EnumMap<>(CrueFileType.class);
  private static final Crue9DCFileFormat DC_FILEFORMAT = new Crue9DCFileFormat(MAP);
  private static final Crue9DHFileFormat DH_FILEFORMAT = new Crue9DHFileFormat(MAP);
  private static final Crue9FCBFileFormat FCB_FILEFORMAT = new Crue9FCBFileFormat();
  private static final Crue9STOFileFormat STO_FILEFORMAT = new Crue9STOFileFormat();
  private static final Crue9STRFileFormat STR_FILEFORMAT = new Crue9STRFileFormat();

  /**
   * @return the DC
   */
  public static Crue9DCFileFormat getDCFileFormat() {
    return DC_FILEFORMAT;
  }

  /**
   * @param type le type demande
   * @return le format correspondant
   */
  public static AbstractCrue9FileFormat getFileFormat(final CrueFileType type) {
    return MAP.get(type);
  }

  /**
   * @return the DH
   */
  public static Crue9DHFileFormat getDHFileFormat() {
    return DH_FILEFORMAT;
  }

  /**
   * @return the FCB
   */
  public static Crue9FCBFileFormat getFCBFileFormat() {
    return FCB_FILEFORMAT;
  }

  /**
   * @return the STO
   */
  public static Crue9STOFileFormat getSTOFileFormat() {
    return STO_FILEFORMAT;
  }

  /**
   * @return the STR
   */
  public static Crue9STRFileFormat getSTRFileFormat() {
    return STR_FILEFORMAT;
  }
}
