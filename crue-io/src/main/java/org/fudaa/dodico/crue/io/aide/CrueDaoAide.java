/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fudaa.dodico.crue.io.aide;

import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;

/**
 *
 * @author landrodie
 */
public class CrueDaoAide  extends AbstractCrueDao  {
    public List<CrueAide> listeAide;

    public CrueDaoAide(List<CrueAide> listeAide) {
        this.listeAide = listeAide;
    }

    public List<CrueAide> getListeAide() {
        return listeAide;
    }

    public void setListeAide(List<CrueAide> listeAide) {
        this.listeAide = listeAide;
    }
}
