/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.dptg.CrueConverterDPTG;
import org.fudaa.dodico.crue.io.dptg.CrueDaoStructureDPTG;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * Builder for FileFormat that build the corrects converter for the given version.
 * 
 * @author deniger
 */
public class CrueFileFormatBuilderDPTG implements CrueFileFormatBuilder<CrueData> {

  @Override
  public Crue10FileFormat<CrueData> getFileFormat(final CoeurConfigContrat coeurConfig) {

    return new Crue10FileFormat<>(new CrueDataXmlReaderWriterImpl<>(CrueFileType.DPTG,
        coeurConfig, new CrueConverterDPTG(), new CrueDaoStructureDPTG(coeurConfig.getXsdVersion())));

  }

}
