/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.drso;

import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.BrancheAbstract;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.CasierAbstract;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.NoeudNiveauContinu;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO.SectionAbstract;

/**
 * Classe persistante qui reprend la meme structure que le fichier xml DRSO - Fichier des donnees du reseau (xml) A
 * persister telle qu'elle.
 * 
 * @author Adrien Hadoux
 */
public class CrueDaoDRSO extends AbstractCrueDao {

  /**
   * la liste des noeuds
   */
  List<NoeudNiveauContinu> Noeuds;

  /**
   * la liste des casiers
   */
  List<CasierAbstract> Casiers;

  /**
   * la liste des sections
   */
  List<SectionAbstract> Sections;
  /**
   * la liste des branches
   */
  List<BrancheAbstract> Branches;


  public List<SectionAbstract> getSections() {
    return Sections;
  }
}
