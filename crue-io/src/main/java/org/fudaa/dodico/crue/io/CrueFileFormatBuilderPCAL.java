/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.pcal.CrueConverterPCAL;
import org.fudaa.dodico.crue.io.pcal.CrueDaoStructurePCAL;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;

public class CrueFileFormatBuilderPCAL implements CrueFileFormatBuilder<CrueData> {

  @Override
  public Crue10FileFormat<CrueData> getFileFormat(final CoeurConfigContrat version) {

    return new Crue10FileFormat<>(new CrueDataXmlReaderWriterImpl<>(
            CrueFileType.PCAL, version, new CrueConverterPCAL(Crue10VersionConfig.isV1_1_1(version)), new CrueDaoStructurePCAL(version.getXsdVersion())));

  }
}
