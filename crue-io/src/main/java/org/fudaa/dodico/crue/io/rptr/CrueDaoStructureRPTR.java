/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rptr;

import com.thoughtworks.xstream.XStream;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * Structures utilisées dans la classe CrueDaoPNUM
 *
 * @author deniger
 */
public class CrueDaoStructureRPTR implements CrueDataDaoStructure {

  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {

    xstream.alias(CrueFileType.RPTR.toString(), CrueDaoRPTR.class);
    xstream.alias("ResPrtReseauNoeud", ResPrtNoeudDao.class);
    xstream.alias("ResPrtReseauNoeuds", ResPrtReseauNoeudsDao.class);
    xstream.addImplicitCollection(CrueDaoRPTR.class, "ResPrtReseauNoeudsDaos");
    xstream.addImplicitCollection(ResPrtReseauNoeudsDao.class, "ResPrtReseauNoeudDaos");
    xstream.aliasAttribute(ResPrtNoeudDao.class, "nom", "NomRef");
    xstream.aliasAttribute(ResPrtReseauNoeudsDao.class, "nom", "NomRef");
  }

  static class ResPrtReseauNoeudsDao {

    String nom;
    List<ResPrtNoeudDao> ResPrtReseauNoeudDaos;
  }

  static class ResPrtNoeudDao {

    String nom;
  }
}
