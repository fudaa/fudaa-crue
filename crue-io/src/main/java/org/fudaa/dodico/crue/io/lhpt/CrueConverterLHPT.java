/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.lhpt;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.AbstractDaoFloatLoi;
import org.fudaa.dodico.crue.io.common.AbstractDaoLoi;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.dlhy.ConvertLoi;
import org.fudaa.dodico.crue.io.dlhy.ConvertLoiDF;
import org.fudaa.dodico.crue.io.dlhy.ConvertLoiFF;
import org.fudaa.dodico.crue.io.dlhy.ConvertLoiHelper;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.aoc.AocEchellesSections;
import org.fudaa.dodico.crue.metier.aoc.ParametrageProfilSection;
import org.fudaa.dodico.crue.metier.aoc.LoiMesuresPost;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;
import org.fudaa.dodico.crue.metier.emh.LoiDF;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.emh.LoiTF;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Converter qui remplit les structures dao avec les objets metier et inversement.
 */
public class CrueConverterLHPT implements CrueDataConverter<CrueDaoLHPT, LoiMesuresPost> {
    /**
     * Convertit les objets persistants en objets métier
     */
    @Override
    public LoiMesuresPost convertDaoToMetier(final CrueDaoLHPT dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
        final LoiMesuresPost res = new LoiMesuresPost();
        final List<AbstractDaoLoi> inLois = dao.LoisLHPT;
        final Map<Class, ConvertLoi> corr = new HashMap<>();
        corr.put(AbstractDaoFloatLoi.DaoLoiDF.class, new ConvertLoiDF());
        corr.put(AbstractDaoFloatLoi.DaoLoiFF.class, new ConvertLoiFF());
        corr.put(DaoLoiTF.class, new ConvertLoiTF());
        res.setLois(ConvertLoiHelper.convertDaoToMetier(ctuluLog, inLois, corr, true));
        convertDaoToMetierEchelleSections(dao, res);
        return res;
    }

    @Override
    public LoiMesuresPost getConverterData(final CrueData in) {
        return null;
    }

    /**
     * Convertit les objets métier en objets persistants
     */
    @Override
    public CrueDaoLHPT convertMetierToDao(final LoiMesuresPost metier, final CtuluLog ctuluLog) {

        final CrueDaoLHPT res = new CrueDaoLHPT();

        res.LoisLHPT = new ArrayList<>(metier.getLois().size());
        final Map<Class, ConvertLoi> corr = new HashMap<>();
        corr.put(LoiDF.class, new ConvertLoiDF());
        corr.put(LoiFF.class, new ConvertLoiFF());
        corr.put(LoiTF.class, new ConvertLoiTF());

        for (final AbstractLoi loi : metier.getLois()) {
            final ConvertLoi cs = corr.get(loi.getClass());
            res.LoisLHPT.add(cs.metiertoDao(loi));
        }

        convertMetierToDaoEchellesSection(metier, res);
        return res;
    }

    public void convertDaoToMetierEchelleSections(final CrueDaoLHPT dao, final LoiMesuresPost metier) {
        if (dao.EchellesSections != null && dao.EchellesSections.sections != null) {
            final List<CrueDaoStructureLHPT.EchelleSection> sections = dao.EchellesSections.sections;
            if (sections != null) {
                for (final CrueDaoStructureLHPT.EchelleSection section : sections) {
                    metier.getEchellesSections().addSection(section.PK, section.SectionRef);
                }
            }
        }
    }

    public void convertMetierToDaoEchellesSection(final LoiMesuresPost metier, final CrueDaoLHPT dao) {
        //Echelles section
        final AocEchellesSections echellesSections = metier.getEchellesSections();
        dao.EchellesSections = new CrueDaoStructureLHPT.EchellesSections();
        dao.EchellesSections.sections = new ArrayList<>();
        for (final ParametrageProfilSection aocEchellesSection : echellesSections.getEchellesSectionList()) {
            dao.EchellesSections.sections
                    .add(CrueDaoStructureLHPT.create(aocEchellesSection.getPk(),
                            aocEchellesSection.getSectionRef()));
        }
    }
}
