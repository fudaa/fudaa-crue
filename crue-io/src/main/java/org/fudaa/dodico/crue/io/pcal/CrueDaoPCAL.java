/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.pcal;

import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.pcal.CrueDaoStructurePCAL.PdtCouplagePersist;
import org.fudaa.dodico.crue.io.pcal.CrueDaoStructurePCAL.PdtRes;

/**
 * Représentation persistante du fichier xml PCAL (Paramètres de calculs).
 *
 * @author CDE
 */
@SuppressWarnings("PMD.VariableNamingConventions")
public class CrueDaoPCAL extends AbstractCrueDao {
  /**
   * Représente la valeur de la balise portant le même nom dans le fichier XML
   */
  public String DateDebSce;
  /**
   * Représente la valeur de la balise portant le même nom dans le fichier XML
   * Conserve pour compatibilité version 1.1.1
   */
  public String DureeSce;
  /**
   * Représente la valeur de la balise portant le même nom dans le fichier XML
   * Conserve pour compatibilité version 1.1.1
   */
  public PdtRes PdtRes;
  /**
   * Représente la valeur de la balise portant le même nom dans le fichier XML
   * Conserve pour compatibilité version 1.1.1
   */
  public String Verbosite;
  /**
   * Représente la valeur de la balise portant le même nom dans le fichier XML
   * Conserve pour compatibilité version 1.1.1
   */
  public PdtCouplagePersist PdtCouplage;
}
