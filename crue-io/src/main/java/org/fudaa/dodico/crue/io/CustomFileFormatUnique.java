/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.ctulu.CtuluLibString;

/**
 * @param <M> donnee lue ou ecrite
 * @author Fred Deniger
 */
public abstract class CustomFileFormatUnique<M> extends CustomFileFormat<M> implements
    CustomFileFormatVersionInterface<M> {
  /**
   */
  public CustomFileFormatUnique(final int _nbFile) {
    super(_nbFile);
  }

  @Override
  public final String getLastVersion() {
    return getVersionName();
  }

  @Override
  public String getVersionName() {
    return CtuluLibString.UN;
  }

  @Override
  public final CustomFileFormat<M> getFileFormat() {
    return this;
  }
}
