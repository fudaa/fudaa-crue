/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import java.util.List;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.dlhy.CrueConverterDLHY;
import org.fudaa.dodico.crue.io.dlhy.CrueDaoStructureDLHY;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.Loi;

public class CrueFileFormatBuilderDLHY implements CrueFileFormatBuilder<List<Loi>> {

  @Override
  public Crue10FileFormat<List<Loi>> getFileFormat(final CoeurConfigContrat coeurConfig) {
    return new Crue10FileFormat<>(new CrueDataXmlReaderWriterImpl<>(CrueFileType.DLHY,
        coeurConfig, new CrueConverterDLHY(), new CrueDaoStructureDLHY(coeurConfig.getXsdVersion())));

  }

}
