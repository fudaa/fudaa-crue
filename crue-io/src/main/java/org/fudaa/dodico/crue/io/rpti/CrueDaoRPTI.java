/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rpti;

import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.rpti.CrueDaoStructureRPTI.ResPrtCIniCasierDao;
import org.fudaa.dodico.crue.io.rpti.CrueDaoStructureRPTI.ResPrtCIniSectionDao;

/**
 * Représentation persistante du fichier xml PNUM (Paramètres numériques).
 * 
 * @author CDE
 */
public class CrueDaoRPTI extends AbstractCrueDao {
  
  List<ResPrtCIniCasierDao> ResPrtCIniCasiers;
  List<ResPrtCIniSectionDao> ResPrtCIniSections;


}
