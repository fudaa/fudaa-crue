/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.io.neuf.AbstractCrueBinaryReader;
import org.fudaa.dodico.crue.io.neuf.STOReader;
import org.fudaa.dodico.crue.io.neuf.STOSequentialReader;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * @author CDE
 */
public class Crue9STOFileFormat extends AbstractCrueBinaryFileFormat<STOSequentialReader> {

  /**
   * Constructeur qui précise l'extension autorisée pour ce type de fichier
   */
  public Crue9STOFileFormat() {
    super(CrueFileType.STO);
  }

  @Override
  protected AbstractCrueBinaryReader<STOSequentialReader> createReader() {
    return new STOReader();
  }

}
