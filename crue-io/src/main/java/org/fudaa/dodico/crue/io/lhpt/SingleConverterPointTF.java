/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.lhpt;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.crue.io.common.AbstractSingleConverter;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionTF;

/**
 * @author deniger
 */
public class SingleConverterPointTF extends AbstractSingleConverter {
  @Override
  public boolean canConvert(Class aClass) {
    return PtEvolutionTF.class.equals(aClass);
  }

  protected Object createFor(String x, double y) {
    return new PtEvolutionTF(x, y);
  }

  /**
   * @param str
   */
  @Override
  public Object fromString(final String str) {
    if (CtuluLibString.isEmpty(str)) {
      return null;
    }
    if (str.indexOf(' ') < 0) {
      if (analyse != null) {
        analyse.addSevereError("io.convert.PtEvolutionTF.error", str);
      }
      return null;
    }
    String x = StringUtils.substringBeforeLast(str, " ");
    String y = StringUtils.substringAfterLast(str, " ");
    if (StringUtils.isBlank(x) || StringUtils.isBlank(y)) {
      if (analyse != null) {
        analyse.addSevereError("io.convert.PtEvolutionTF.error", str);
      }
      return null;
    }
    Object res = null;
    try {
      res = createFor(x, Double.parseDouble(y));
    } catch (final NumberFormatException e) {
      if (analyse != null) {
        analyse.addSevereError("io.convert.PtEvolutionTF.error", str);
      }
      return null;
    }
    return res;
  }

  @Override
  public String toString(final Object obj) {
    if (obj == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    final PtEvolutionTF in = (PtEvolutionTF) obj;
    return in.getAbscisse() + " " + in.getOrdonnee();
  }
}
