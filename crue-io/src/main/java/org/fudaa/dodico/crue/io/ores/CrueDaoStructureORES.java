/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.ores.CrueDaoStructureORES.DdeConverter;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.Dde;
import org.fudaa.dodico.crue.metier.emh.DdeMultiple;

/**
 * Structures utilisées dans la classe CrueDaoORES
 *
 * @author cde
 */
public class CrueDaoStructureORES implements CrueDataDaoStructure {

  private final CoeurConfigContrat version;

  public CrueDaoStructureORES(CoeurConfigContrat version) {

    this.version = version;
  }

  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    xstream.alias(CrueFileType.ORES.toString(), CrueDaoORES.class);
    final List<Class> ordres = new ArrayList<>();
    xstream.alias("OrdResBranches", DaoOrdResBranches.class);
    xstream.alias("OrdResNoeuds", DaoOrdResNoeuds.class);
    xstream.alias("OrdResSections", DaoOrdResSections.class);
    xstream.alias("OrdResCasiers", DaoOrdResCasiers.class);
    ordres.add(DaoOrdResNoeudNiveauContinu.class);
    ordres.add(DaoOrdResCasier.class);
    ordres.add(DaoOrdResSection.class);
    ordres.add(DaoOrdResBranchePdc.class);
    ordres.add(DaoOrdResBrancheSeuilTransversal.class);
    ordres.add(DaoOrdResBrancheSeuilLateral.class);
    ordres.add(DaoOrdResBrancheOrifice.class);
    ordres.add(DaoOrdResBrancheNiveauxAssocies.class);
    ordres.add(DaoOrdResBrancheStrickler.class);
    ordres.add(DaoOrdResBrancheBarrageGenerique.class);
    ordres.add(DaoOrdResBrancheBarrageFilEau.class);
    ordres.add(DaoOrdResBrancheSaintVenant.class);
    ordres.add(DaoOrdResModeleRegul.class);
    for (final Class ordClass : ordres) {
      final String className = ordClass.getSimpleName();
      final String xmlClassName = StringUtils.removeStart(className, "Dao");
      xstream.alias(xmlClassName, ordClass);
      xstream.addImplicitCollection(ordClass, "Ddes");
    }
//    xstream.addImplicitCollection(DaoOrdResModeleRegul.class, "DdeMultiples");
    xstream.alias("Dde", Dde.class);
    xstream.alias("DdeMultiple", DdeMultiple.class);
    xstream.registerConverter(new DdeConverter());
    xstream.registerConverter(new DdeMultipeConverter());
    if (Crue10VersionConfig.isLowerOrEqualsTo_V1_2(version)) {
      xstream.omitField(CrueDaoORES.class, "OrdResModeles");
    }
  }

  protected static class DaoOres {

    public List<Dde> Ddes;
  }

  protected static class DaoOrdResNoeudNiveauContinu extends DaoOres {
  }

  protected static class DaoOrdResCasier extends DaoOres {
  }

  protected static class DaoOrdResSection extends DaoOres {
  }

  protected static class DaoOrdResBranchePdc extends DaoOres {
  }

  protected static class DaoOrdResBrancheSeuilTransversal extends DaoOres {
  }

  protected static class DaoOrdResBrancheSeuilLateral extends DaoOres {
  }

  protected static class DaoOrdResBrancheOrifice extends DaoOres {
  }

  protected static class DaoOrdResBrancheNiveauxAssocies extends DaoOres {
  }

  protected static class DaoOrdResBrancheStrickler extends DaoOres {
  }

  protected static class DaoOrdResBrancheBarrageGenerique extends DaoOres {
  }

  protected static class DaoOrdResBrancheBarrageFilEau extends DaoOres {
  }

  protected static class DaoOrdResBrancheSaintVenant extends DaoOres {
  }

  protected static class DaoOrdResModeleRegul extends  DaoOres {
  }

  public static class DaoOrdResBranches {

    public DaoOrdResBrancheBarrageFilEau OrdResBrancheBarrageFilEau;
    public DaoOrdResBrancheBarrageGenerique OrdResBrancheBarrageGenerique;
    public DaoOrdResBrancheNiveauxAssocies OrdResBrancheNiveauxAssocies;
    public DaoOrdResBrancheOrifice OrdResBrancheOrifice;
    public DaoOrdResBranchePdc OrdResBranchePdc;
    public DaoOrdResBrancheSaintVenant OrdResBrancheSaintVenant;
    public DaoOrdResBrancheSeuilLateral OrdResBrancheSeuilLateral;
    public DaoOrdResBrancheSeuilTransversal OrdResBrancheSeuilTransversal;
    public DaoOrdResBrancheStrickler OrdResBrancheStrickler;
  }

  public static class DaOrdResModeles {
    public DaoOrdResModeleRegul OrdResModeleRegul;
  }

  public static class DaoOrdResCasiers {

    public DaoOrdResCasier OrdResCasier;
  }

  public static class DaoOrdResSections {

    public DaoOrdResSection OrdResSection;
  }

  public static class DaoOrdResNoeuds {

    public DaoOrdResNoeudNiveauContinu OrdResNoeudNiveauContinu;
  }

  protected static class DdeConverter implements Converter {

    @Override
    public boolean canConvert(final Class type) {
      return type.equals(Dde.class);
    }

    @Override
    public void marshal(final Object source, final HierarchicalStreamWriter writer, final MarshallingContext context) {
      final Dde valeur = (Dde) source;
      writer.addAttribute("NomRef", StringUtils.capitalize(valeur.getNom()));
      writer.setValue(Boolean.toString(valeur.getValeur()));
    }

    @Override
    public Object unmarshal(final HierarchicalStreamReader reader, final UnmarshallingContext context) {
      final String nom = StringUtils.uncapitalize(reader.getAttribute(0));
      return new Dde(nom, Boolean.parseBoolean(reader.getValue()));
    }
  }

  protected static class DdeMultipeConverter extends DdeConverter {
    @Override
    public boolean canConvert(final Class type) {
      return DdeMultiple.class.equals(type);
    }

    @Override
    public Object unmarshal(final HierarchicalStreamReader reader, final UnmarshallingContext context) {
      final String nom = StringUtils.uncapitalize(reader.getAttribute(0));
      return new DdeMultiple(nom, Boolean.parseBoolean(reader.getValue()));
    }
  }
}
