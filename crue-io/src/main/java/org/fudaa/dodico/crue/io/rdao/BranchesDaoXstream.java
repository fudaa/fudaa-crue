/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import com.thoughtworks.xstream.XStream;

/**
 *
 * @author deniger
 */
public class BranchesDaoXstream {
  public static void configureXstream(final XStream xstream) {
    xstream.alias("Branches", BranchesDao.class);

    xstream.alias("BrancheBarrageFilEau", BranchesDao.BrancheTypeDao.class);
    xstream.alias("BrancheBarrageGenerique", BranchesDao.BrancheTypeDao.class);
    xstream.alias("BrancheNiveauxAssocies", BranchesDao.BrancheTypeDao.class);
    xstream.alias("BrancheOrifice", BranchesDao.BrancheTypeDao.class);
    xstream.alias("BranchePdc", BranchesDao.BrancheTypeDao.class);
    xstream.alias("BrancheSaintVenant", BranchesDao.BrancheTypeDao.class);
    xstream.alias("BrancheSeuilLateral", BranchesDao.BrancheTypeDao.class);
    xstream.alias("BrancheSeuilTransversal", BranchesDao.BrancheTypeDao.class);
    xstream.alias("BrancheStrickler", BranchesDao.BrancheTypeDao.class);

    xstream.alias("Branche", BranchesDao.BrancheDao.class);

    xstream.addImplicitCollection(BranchesDao.BrancheTypeDao.class, "Branche", BranchesDao.BrancheDao.class);
    xstream.addImplicitCollection(BranchesDao.class, "VariableRes", CommonResDao.VariableResDao.class);
    xstream.addImplicitCollection(BranchesDao.BrancheTypeDao.class, "VariableRes", CommonResDao.VariableResDao.class);

  }
}
