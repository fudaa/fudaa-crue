/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dpti;

import com.thoughtworks.xstream.converters.SingleValueConverter;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.common.EnumsConverter;
import org.fudaa.dodico.crue.io.dpti.CrueDaoStructureDPTI.*;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Factory qui se charge de remplir les structures DAO du fichier DPTI avec les donnees metier et inversement.
 *
 * @author Adrien Hadoux
 */
public class CrueConverterDPTI implements CrueDataConverter<CrueDaoDPTI, CrueData> {
  SingleValueConverter sensOuvConverter;

  @Override
  public CrueData getConverterData(final CrueData in) {
    return in;
  }

  /*
   * @see org.fudaa.dodico.crue.io.dao.CrueDataConverter#convertDaoToMetier(org.fudaa.dodico.crue.io.dao.AbstractCrueDao,
   * org.fudaa.dodico.crue.metier.CrueData, org.fudaa.ctulu.CtuluAnalyze)
   */
  @Override
  public CrueData convertDaoToMetier(final CrueDaoDPTI dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
    sensOuvConverter = EnumsConverter.createEnumConverter(EnumSensOuv.class, ctuluLog);

    if (dataLinked == null) {
      ctuluLog.addInfo("io.dpti.lecture.error");
      return null;
    }
    final CrueData dataCrue = dataLinked;

    // -- remplissage des noeuds --//
    daoToMetierNoeud(dao.DonPrtCIniNoeuds, dataCrue, ctuluLog);

    // -- remplissage des branches --//
    daoToMetierBranche(dao.DonPrtCIniBranches, dataCrue, ctuluLog);

    // -- remplissage des casiers --//
    daoToMetierCasier(dao.DonPrtCIniCasiers, dataCrue, ctuluLog);

    // -- remplissage des sections --//
    daoToMetierSection(dao.DonPrtCIniSections, dataCrue, ctuluLog);

    return dataCrue;
  }

  /*
   * @see org.fudaa.dodico.crue.io.dao.CrueDataConverter#convertMetierToDao(java.lang.Object, org.fudaa.ctulu.CtuluAnalyze)
   */
  @Override
  public CrueDaoDPTI convertMetierToDao(final CrueData metier, final CtuluLog ctuluLog) {
    sensOuvConverter = EnumsConverter.createEnumConverter(EnumSensOuv.class, ctuluLog);
    final CrueDaoDPTI res = new CrueDaoDPTI();
    // -- etape 1: on remplit les infso des noeuds --//
    res.DonPrtCIniNoeuds = metierToDaoNoeud(metier.getNoeuds(), ctuluLog);

    // -- etape 2: on remplit les infso des branches --//
    res.DonPrtCIniBranches = metierToDaoBranche(metier.getBranches(), ctuluLog);

    // -- etape 3: on remplit les infso des casiers --//
    res.DonPrtCIniCasiers = metierToDaoCasier(metier.getCasiers(), ctuluLog);

    // -- etape 4: on remplit les infso des sections --//
    res.DonPrtCIniSections = metierToDaoSection(metier.getSections());
    return res;
  }

  /**
   * Remplit les donnees persistante avec les conditions initiales des noeuds
   *
   * @param data
   * @param analyser
   * @return liste de noeuds niveau continu persistants
   */
  public static List metierToDaoNoeud(final List<CatEMHNoeud> data, final CtuluLog analyser) {

    final List listePersistante = new ArrayList();
    if (CollectionUtils.isNotEmpty(data)) {
      for (final CatEMHNoeud noeud : data) {
        final List<DonPrtCIni> listeCond = noeud.getDPTI();
        if (CollectionUtils.isNotEmpty(listeCond)) {

          for (final DonPrtCIni conditionInit : listeCond) {
            if (conditionInit != null && (conditionInit instanceof DonPrtCIniNoeudNiveauContinu)) {
              final NoeudNiveauContinu noeudPersist = new NoeudNiveauContinu();
              noeudPersist.NomRef = noeud.getNom();
              noeudPersist.Zini = ((DonPrtCIniNoeudNiveauContinu) conditionInit).getZini();
              listePersistante.add(noeudPersist);
            }
          }
        } else {
          analyser.addInfo("io.dpti.ecriture.noeud.error", noeud.getNom());
        }
      }
    }
    return listePersistante;
  }

  /**
   * Remplit les donnees persistante avec les conditions initiales des noeuds
   *
   * @return List<SectionAbstract>
   */
  private static List<SectionAbstract> metierToDaoSection(final List<CatEMHSection> data) {

    final List<SectionAbstract> listePersistante = new ArrayList<>();
    for (final CatEMHSection section : data) {
      final List<DonPrtCIni> listeCond = section.getDPTI();
      if (CollectionUtils.isNotEmpty(listeCond)) {

        for (final DonPrtCIni conditionInit : listeCond) {
          if ((conditionInit instanceof DonPrtCIniSection)) {
            SectionAbstract sectionPersist = null;

            if (section instanceof EMHSectionIdem) {
              sectionPersist = new SectionRefIdem();
            } else if (section instanceof EMHSectionInterpolee) {
              sectionPersist = new SectionRefInterpolee();
            } else if (section instanceof EMHSectionProfil) {
              sectionPersist = new SectionRefProfil();
            } else if (section instanceof EMHSectionSansGeometrie) {
              sectionPersist = new SectionRefSansGeometrie();
            }

            if (sectionPersist != null) {
              sectionPersist.NomRef = section.getNom();
              sectionPersist.Zini = ((DonPrtCIniSection) conditionInit).getZini();
              listePersistante.add(sectionPersist);
            }
          }
        }
      }
    }
    return listePersistante;
  }

  /**
   * Methode qui remplit une arrayList d'objets persistants qui constituent la deuxieme partie du fichier DRSO: les
   * Branches.
   *
   * @return List<BrancheAbstract>
   */
  private List<BrancheAbstract> metierToDaoBranche(final List<CatEMHBranche> data, final CtuluLog analyser) {
    final List<BrancheAbstract> listePersistante = new ArrayList<>();
    for (final CatEMHBranche branche : data) {
      BrancheAbstract branchePersist = null;
      if (branche instanceof EMHBrancheBarrageFilEau) {
        branchePersist = new BrancheBarrageFilEau();
      } else if (branche instanceof EMHBrancheBarrageGenerique) {
        branchePersist = new BrancheBarrageGenerique();
      } else if (branche instanceof EMHBrancheEnchainement) {
        branchePersist = new BrancheEnchainement();
      } else if (branche instanceof EMHBrancheNiveauxAssocies) {
        branchePersist = new BrancheNiveauxAssocies();
      } else if (branche instanceof EMHBrancheOrifice) {
        branchePersist = new BrancheOrifice();
      } else if (branche instanceof EMHBranchePdc) {
        branchePersist = new BranchePdc();
      } else if (branche instanceof EMHBrancheSaintVenant) {
        branchePersist = new BrancheSaintVenant();
      } else if (branche instanceof EMHBrancheSeuilLateral) {
        branchePersist = new BrancheSeuilLateral();
      } else if (branche instanceof EMHBrancheSeuilTransversal) {
        branchePersist = new BrancheSeuilTransversal();
      } else if (branche instanceof EMHBrancheStrickler) {
        branchePersist = new BrancheStrickler();
      }

      if (branchePersist != null) {
        // -- commun --//

        final List<DonPrtCIni> listeCond = branche.getDPTI();
        if (listeCond != null && !listeCond.isEmpty()) {

          for (final DonPrtCIni conditionInit : listeCond) {

            if (conditionInit != null && (conditionInit instanceof DonPrtCIniBrancheOrifice)) {
              final DonPrtCIniBrancheOrifice cinit = (DonPrtCIniBrancheOrifice) conditionInit;

              branchePersist.NomRef = branche.getNom();
              if (branchePersist instanceof BrancheOrifice) {
                ((BrancheOrifice) branchePersist).Ouv = cinit.getOuv();
                ((BrancheOrifice) branchePersist).SensOuv = sensOuvConverter.toString(cinit.getSensOuv());
              }

              branchePersist.Qini = ((DonPrtCIniBranche) conditionInit).getQini();

              listePersistante.add(branchePersist);
            } else if (conditionInit != null && (conditionInit instanceof DonPrtCIniBrancheSaintVenant)) {

              branchePersist.NomRef = branche.getNom();

              branchePersist.Qini = ((DonPrtCIniBranche) conditionInit).getQini();
              ((BrancheSaintVenant) branchePersist).Qruis = ((DonPrtCIniBrancheSaintVenant) conditionInit).getQruis();

              listePersistante.add(branchePersist);
            } else if (conditionInit != null && (conditionInit instanceof DonPrtCIniBranche)) {

              branchePersist.NomRef = branche.getNom();
              branchePersist.Qini = ((DonPrtCIniBranche) conditionInit).getQini();
              listePersistante.add(branchePersist);
            }
          }
        } else {
          analyser.addInfo("io.dpti.ecriture.branche.error", branche.getNom());
        }
      }
    }
    return listePersistante;
  }

  /**
   * Methode qui remplit une arrayList d'objets persistants qui constituent la troisieme partie du fichier DRSO: les
   * Casier.
   *
   * @return List<CasierAbstract>
   */
  private static List<CasierAbstract> metierToDaoCasier(final List<CatEMHCasier> data, final CtuluLog analyser) {
    final List<CasierAbstract> listePersistante = new ArrayList<>();
    for (final CatEMHCasier casier : data) {
      CasierAbstract casierPersist = null;
      if (casier instanceof EMHCasierMNT) {
        casierPersist = new CasierMNT();
      } else if (casier instanceof EMHCasierProfil) {
        casierPersist = new CasierProfil();
      }
      // -- commun --//
      if (casierPersist != null) {

        final List<DonPrtCIni> listeCond = casier.getDPTI();
        if (listeCond != null && !listeCond.isEmpty()) {
          for (final DonPrtCIni conditionInit : listeCond) {
            if (conditionInit != null && (conditionInit instanceof DonPrtCIniCasierProfil)) {
              casierPersist.NomRef = casier.getNom();
              casierPersist.Qruis = ((DonPrtCIniCasierProfil) conditionInit).getQruis();
              listePersistante.add(casierPersist);
            }
          }
        } else {
          analyser.addInfo("io.dpti.ecriture.casier.error", casier.getNom());
        }
      }
    }
    return listePersistante;
  }

  /**
   * Methode qui met a jour les objets metier EMH noeuds a partir des donnees persistantes de DPTI et les prechargements
   * de DRSO
   */
  private static void daoToMetierNoeud(final List<NoeudNiveauContinu> listePersistants, final CrueData data,
                                       final CtuluLog analyser) {
    if (CollectionUtils.isNotEmpty(listePersistants)) {
      for (final NoeudNiveauContinu noeudPersist : listePersistants) {
        final String reference = noeudPersist.NomRef;
        final CatEMHNoeud noeud = data.findNoeudByReference(reference);
        if (noeud == null) {
          analyser.addInfo("io.dpti.noeud.ref.error", reference);
          data.setDptiChangedDuringLoading();
        } else {
          final DonPrtCIniNoeudNiveauContinu condInit = new DonPrtCIniNoeudNiveauContinu(data
              .getCrueConfigMetier());
          condInit.setZini(noeudPersist.Zini);
          noeud.addInfosEMH(condInit);
        }
      }
    }
  }

  /**
   * Methode qui met a jour les objets metier EMH sections a partir des donnees persistantes de DPTI et les
   * prechargements de DRSO
   */
  private static void daoToMetierSection(final List<SectionAbstract> listePersistants, final CrueData data,
                                         final CtuluLog analyser) {
    if (CollectionUtils.isNotEmpty(listePersistants)) {
      for (final SectionAbstract sectionPersist : listePersistants) {
        final String reference = sectionPersist.NomRef;
        final CatEMHSection section = data.findSectionByReference(reference);
        if (section == null) {
          analyser.addInfo("io.dpti.section.ref.error", reference);
          data.setDptiChangedDuringLoading();
        } else {
          final DonPrtCIniSection condInit = new DonPrtCIniSection(data.getCrueConfigMetier());
          condInit.setZini(sectionPersist.Zini);
          section.addInfosEMH(condInit);
        }
      }
    }
  }

  /**
   * Methode qui met a jour les objets metier EMH branches a partir des données persistantes de DPTI et les
   * préchargements de DRSO
   */
  private void daoToMetierBranche(final List<BrancheAbstract> listePersistants, final CrueData data,
                                  final CtuluLog analyser) {

    for (final BrancheAbstract branchePersist : listePersistants) {
      final String reference = branchePersist.NomRef;
      final CatEMHBranche branche = data.findBrancheByReference(reference);
      if (branche == null) {
        data.setDptiChangedDuringLoading();
        analyser.addInfo("io.dpti.branche.ref.error", reference);
      } else {
        DonPrtCIniBranche condInit = null;
        // -- cas particuliers Orifice--//
        if (branchePersist instanceof BrancheOrifice) {
          condInit = new DonPrtCIniBrancheOrifice(data.getCrueConfigMetier());
          final BrancheOrifice orif = (BrancheOrifice) branchePersist;
          if (orif.Ouv != null) {
            ((DonPrtCIniBrancheOrifice) condInit).setOuv(orif.Ouv);
          }
          if (orif.SensOuv != null) {
            ((DonPrtCIniBrancheOrifice) condInit).setSensOuv((EnumSensOuv) sensOuvConverter.fromString(orif.SensOuv));
          }
          // -- cas particuliers SaintVenant--//
        } else if (branchePersist instanceof BrancheSaintVenant) {
          condInit = new DonPrtCIniBrancheSaintVenant(data.getCrueConfigMetier());
          final BrancheSaintVenant stVenant = (BrancheSaintVenant) branchePersist;
          if (stVenant.Qruis != null) {
            ((DonPrtCIniBrancheSaintVenant) condInit).setQruis(stVenant.Qruis);
          }
        }
        if (condInit == null) {
          condInit = new DonPrtCIniBranche(data.getCrueConfigMetier());
        }
        if (branchePersist.Qini != null) {
          condInit.setQini(branchePersist.Qini);
        }
        branche.addInfosEMH(condInit);
      }
    }
  }

  /**
   * Methode qui met a jour les objets metier EMH noeuds a partir des donnees persistantes de DPTI et les prechargements
   * de DRSO
   */
  private static void daoToMetierCasier(final List<CasierAbstract> listePersistants, final CrueData data,
                                        final CtuluLog analyser) {

    for (final CasierAbstract casierPersist : listePersistants) {
      final String reference = casierPersist.NomRef;
      final CatEMHCasier casier = data.findCasierByReference(reference);
      if (casier == null) {
        data.setDptiChangedDuringLoading();
        analyser.addInfo("io.dpti.casier.ref.error", reference);
      } else {
        if (casierPersist.Qruis != null) {
          final DonPrtCIniCasierProfil condInit = new DonPrtCIniCasierProfil(data.getCrueConfigMetier());
          condInit.setQruis(casierPersist.Qruis);
          casier.addInfosEMH(condInit);
        }
      }
    }
  }
}
