package org.fudaa.dodico.crue.io.common;

import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.config.loi.LoiTypeContainer;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by deniger on 20/06/2017.
 */
final class EnumTypeLoiConverterOld extends AbstractSingleConverter {

    final LoiTypeContainer crueProps;
    final DualHashBidiMap newOld = new DualHashBidiMap();

    private final static Logger LOGGER = Logger.getLogger(EnumTypeLoiConverterOld.class.getName());

    public EnumTypeLoiConverterOld(LoiTypeContainer crueProps) {
        assert crueProps != null;
        this.crueProps = crueProps;
        newOld.put("LoiTZimp", "LoiTZ");
        newOld.put("LoiQZimp", "LoiQZ");
    }

    @Override
    public boolean canConvert(final Class arg0) {
        return EnumTypeLoi.class.equals(arg0);
    }

    @Override
    public Object fromString(final String arg0) {
        String readValue = arg0;
        if (newOld.containsValue(arg0)) {
            readValue = (String) newOld.getKey(readValue);
        }
        final EnumTypeLoi valueOf = crueProps.getTypeLoi(readValue);
        if (valueOf == null) {
            LOGGER.log(Level.SEVERE, "EnumTypeLoiConverter can''t convert {0}", arg0);
        }
        return valueOf;
    }

    @Override
    public String toString(final Object arg0) {

        String enumToString = ((EnumTypeLoi) arg0).getId();
        if (newOld.containsKey(enumToString)) {
            enumToString = (String) newOld.get(enumToString);
        }
        return enumToString;
    }
}
