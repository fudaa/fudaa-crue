/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.etu;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.common.CrueHelper;
import org.fudaa.dodico.crue.io.etu.CrueDaoStructureETU.*;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.etude.*;
import org.joda.time.LocalDateTime;

import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Factory qui se charge de remplir les structures DAO dufichier ETU avec les données métier et inversement.
 *
 * @author Adrien Hadoux
 */
public class CrueConverterETU implements CrueDataConverter<CrueDaoETU, EMHProjet> {
    public List<FichierCrue> sortFiles(final FichierCrueManager fichiers, final FichierCrueComparator comparator) {
        final List<FichierCrue> sortedFiles = new ArrayList<>();
        final List<FichierCrue> initFiles = fichiers.getFichiers();
        for (final FichierCrue fichierCrue : initFiles) {
            if (!fileMustBeSkip(fichierCrue)) {
                sortedFiles.add(fichierCrue);
            }
        }
        Collections.sort(sortedFiles, comparator);
        return sortedFiles;
    }

    private final String version;

    /**
     * @param version
     */
    public CrueConverterETU(final String version) {
        super();
        this.version = version;
    }

    @Override
    public EMHProjet convertDaoToMetier(final CrueDaoETU dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
        final EMHProjet project = new EMHProjet();

        final EMHProjectInfos infos = new EMHProjectInfos();
        infos.setXmlVersion(version);

        // -- remplissage des infos du projet --//
        infos.setInfosVersions(CrueConverterETU.daoToMetierInfos(dao, ctuluLog));

        // -- Directories --//
        CrueConverterETU.daoToMetierBaseDirectories(dao.Repertoires, infos, ctuluLog);

        // -- remplissage de la base de fichiers utilisés dans tout le projet --//
        CrueConverterETU.daoToMetierBaseFichiers(dao.FichEtudes, infos, ctuluLog);

        // -- remplissage des sous modeles. A besoin de la liste des fichiers crues.--//
        CrueConverterETU.daoToMetierBaseSousModeles(project, dao.SousModeles, infos, ctuluLog);
        // -- Remplissage des modeles --//
        CrueConverterETU.daoToMetierBaseModeles(project, dao.Modeles, infos, ctuluLog);

        // -- remplissage des scénarios --//
        CrueConverterETU.daoToMetierBaseScenarios(project, dao.Scenarios, infos, ctuluLog);

        // -- mise en place du scenario courant --//
        if (dao.ScenarioCourant != null) {
            final ManagerEMHScenario scenario = project.getScenario(dao.ScenarioCourant.NomRef);
            project.setScenarioCourant(scenario);
            if (scenario == null) {
                CrueHelper.unknowReference("Scenario", dao.ScenarioCourant.NomRef, ctuluLog);
            }
        }

        // -- ajout des infos au projet actuel --//
        project.setInfos(infos);
        //tri des conteneurs:
        project.sortLists();
        new EMHProjectValidation().validProject(project, ctuluLog);
        return project;
    }

    public boolean fileMustBeSkip(final FichierCrue fichier) {
        return (Crue10VersionConfig.V_1_1_1.equals(version) && CrueFileType.OPTR.equals(fichier.getType()))
                ||
                (Crue10VersionConfig.isLowerOrEqualsTo_V1_2(version) && CrueFileType.DREG.equals(fichier.getType()))
                ;
    }

    @Override
    public EMHProjet getConverterData(final CrueData in) {
        return null;
    }

    @Override
    public CrueDaoETU convertMetierToDao(final EMHProjet metier, final CtuluLog ctuluLog) {

        final CrueDaoETU dao = new CrueDaoETU();
        final EMHProjectInfos infos = metier.getInfos();

        CrueConverterETU.metierToDaoInfos(infos.getInfosVersions(), dao);

        final Map<String, String> directories = infos.getDirectories();

        if (directories != null) {
            CrueConverterETU.metierToDaoBaseDirectories(directories, dao);
        }

        final List<FichierCrue> fichiers = infos.getBaseFichiersProjets();

        if (fichiers != null) {
            final List<FichierCrue> fichiersSorted = new ArrayList<>(infos.getBaseFichiersProjets());
            FichierLHPTSupport.removeLHPTFiles(fichiersSorted);
            Collections.sort(fichiersSorted, ObjetNommeByNameComparator.INSTANCE);
            metierToDaoBaseFichiers(fichiersSorted, dao);
        }
        final FichierCrueComparator comparator = new FichierCrueComparator();

        metierToDaoBaseSousModeles(metier, dao, comparator);
        metierToDaoBaseModeles(metier, dao, comparator);
        metierToDaoBaseScenarios(metier, dao, comparator);

        final ManagerEMHScenario scenarioCourant = metier.getScenarioCourant();

        if (scenarioCourant != null) {
            dao.ScenarioCourant = new ScenarioCourant();

            dao.ScenarioCourant.NomRef = scenarioCourant.getNom();
        }

        return dao;
    }

    /**
     * Remplissage de la base des scenarios. Affectation des modeles aux scenarios concernés. Affectation des runs aux scenarios concernés. Affectation
     * des fichiers de données aux sous scenarios concernés.
     *
     * @param project
     * @param listePersist
     * @param infos
     * @param analyser
     */
    private static void daoToMetierBaseScenarios(final EMHProjet project, final List<Scenario> listePersist,
                                                 final EMHProjectInfos infos, final CtuluLog analyser) {

        if (CollectionUtils.isNotEmpty(listePersist)) {
            for (final Scenario scenarPersist : listePersist) {

                if (scenarPersist.Nom != null) {
                    final ManagerEMHScenario newScenar = new ManagerEMHScenario(scenarPersist.Nom);
                    newScenar.setActive(isActive(scenarPersist.IsActive));
                    EMHInfosVersion infosVersions = daoToMetierInfos(scenarPersist, analyser);
                    newScenar.setInfosVersions(infosVersions);

                    // -- ajout des fichiers de données --//
                    if (CollectionUtils.isNotEmpty(scenarPersist.FichEtudes)) {
                        for (final Ref reference : scenarPersist.FichEtudes) {
                            // -- on essaie de retrouver la reference du fichier --//
                            if (reference.NomRef != null && infos.existFileInBase(reference.NomRef)) {
                                final FichierCrue fichierData = infos.getFileFromBase(reference.NomRef);
                                newScenar.addFichierDonnees(fichierData);
                            } else {
                                CrueHelper.unknowReference("Fichier", reference.NomRef, analyser);
                            }
                        }
                    }

                    // -- ajout des modeles concernes --//
                    // cas de v9:

                    if (newScenar.isCrue9()) {
                        final ScenarioV9 v9 = (ScenarioV9) scenarPersist;
                        final ManagerEMHModeleBase modeleData = project.getModele(v9.ScenarioModeleV9.NomRef);
                        if (modeleData == null) {
                            CrueHelper.unknowReference("Modele v9", v9.ScenarioModeleV9.NomRef, analyser);
                        } else if (!isType9(modeleData)) {
                            analyser.addSevereError("Le scénario Crue 9 " + scenarPersist.Nom + " doit utiliser un modèle Crue 9");
                        } else {
                            // -- ajout du modele dans le scenar --//
                            newScenar.addManagerFils(modeleData);
                        }
                    } else if (CtuluLibArray.isNotEmpty(scenarPersist.Modeles)) {
                        if (scenarPersist.ScenarioScenarioV9 != null) {
                            newScenar.setLinkedscenarioCrue9(scenarPersist.ScenarioScenarioV9.NomRef);
                        }
                        for (final Ref reference : scenarPersist.Modeles) {
                            final ManagerEMHModeleBase modeleData = project.getModele(reference.NomRef);
                            // -- on essaie de retrouver la reference du fichier --//
                            if (modeleData == null) {
                                CrueHelper.unknowReference("Modele", reference.NomRef, analyser);
                            } else if (!isType10(modeleData)) {
                                analyser.addSevereError(
                                        "Le modèle " + modeleData.getNom()
                                                + " n'est pas un modèle Crue10. Il est utilisé par le scénario Crue 10 " + scenarPersist.Nom);
                            } else {
                                newScenar.addManagerFils(modeleData);
                            }
                        }
                    }
                    // -- ajout des runs --//
                    if (scenarPersist.Runs != null) {
                        for (final Run run : scenarPersist.Runs) {
                            // -- on essaie d'ajouter le run --//
                            if (run.Nom != null) {

                                // -- Creation du run --//
                                final EMHRun runMetier = new EMHRun(run.Nom);

                                // -- infos relatives au run --//
                                infosVersions = daoToMetierInfos(run, analyser);
                                runMetier.setInfosVersion(infosVersions);
                                // -- ajout du modele dans le scenar --//
                                if (newScenar.acceptRun(runMetier.getId())) {
                                    newScenar.addRunToScenario(runMetier);
                                } else {
                                    CrueHelper.errorIdNonUnique("Run", runMetier.getId(), analyser);
                                }
                            } else {
                                CrueHelper.emhEmpty("Run", analyser);
                            }
                        }

                        // -- recherche du run courant si spécifié --//
                        if (scenarPersist.RunCourant != null && newScenar.existRunInScenario(scenarPersist.RunCourant.NomRef)) {
                            final EMHRun runReference = newScenar.getRunFromScenar(scenarPersist.RunCourant.NomRef.trim());

                            // -- gestion du run courant --//
                            newScenar.setRunCourant(runReference);
                        }
                    }
                    project.addScenario(newScenar);
                } else {
                    CrueHelper.errorIdNonUnique("Scenario", scenarPersist.Nom, analyser);
                }
            }
        }
    }

    private static boolean isType9(final ManagerEMHContainerBase modeleData) {
        return CrueVersionType.CRUE9.equals(modeleData.getInfosVersions().getCrueVersion());
    }

    private static boolean isType10(final ManagerEMHContainerBase modeleData) {
        return CrueVersionType.CRUE10.equals(modeleData.getInfosVersions().getCrueVersion());
    }

    public void metierToDaoBaseScenarios(final EMHProjet metier, final CrueDaoETU dao, final FichierCrueComparator comparator) {
        dao.Scenarios = new ArrayList<>();
        final List<ManagerEMHScenario> listeScenarios = new ArrayList<>(metier.getListeScenarios());
        Collections.sort(listeScenarios, ObjetNommeByNameComparator.INSTANCE);
        for (final ManagerEMHScenario managerScenario : listeScenarios) {
            final boolean crue9 = managerScenario.isCrue9();
            final Scenario scenario = crue9 ? new ScenarioV9() : new Scenario();

            scenario.Nom = managerScenario.getNom();
            scenario.IsActive = managerScenario.isCrue9() ? null : managerScenario.isActive();
            if (managerScenario.getLinkedscenarioCrue9() != null && metier.getCoeurConfig().isCrue9Dependant()) {
                scenario.ScenarioScenarioV9 = new Ref();
                scenario.ScenarioScenarioV9.NomRef = managerScenario.getLinkedscenarioCrue9();
            }
            CrueConverterETU.metierToDaoInfos(managerScenario.getInfosVersions(), scenario);

            scenario.FichEtudes = new ArrayList<>();

            final FichierCrueManager fichiers = managerScenario.getListeFichiers();

            if (fichiers != null) {
                final List<FichierCrue> sortedFiles = sortFiles(fichiers, comparator);
                FichierLHPTSupport.removeLHPTFiles(sortedFiles);
                for (final FichierCrue fichier : sortedFiles) {
                    scenario.FichEtudes.add(Ref.createFileReference(fichier.getNom()));
                }
            }

            scenario.Modeles = new ArrayList<>();

            final List<ManagerEMHModeleBase> fils = managerScenario.getFils();

            for (final ManagerEMHModeleBase managerModele : fils) {
                if (crue9) {
                    ((ScenarioV9) scenario).ScenarioModeleV9 = new ScenarioModeleV9Ref();
                    ((ScenarioV9) scenario).ScenarioModeleV9.NomRef = managerModele.getNom();
                } else {
                    scenario.Modeles.add(Ref.createManagerReference(managerModele.getNom()));
                }
            }

            final List<EMHRun> runs = managerScenario.getListeRuns();

            if (CollectionUtils.isNotEmpty(runs)) {
                scenario.Runs = new ArrayList<>();
                for (final EMHRun eMHrun : runs) {
                    final Run run = crue9 ? new RunV9() : new Run();

                    run.Nom = eMHrun.getNom();
                    CrueConverterETU.metierToDaoInfos(eMHrun.getInfosVersion(), run);
                    //pour crue 9 pas d'auteur,...
                    if (crue9) {
                        run.AuteurCreation = null;
                        run.AuteurDerniereModif = null;
                        run.DateCreation = null;
                        run.DateDerniereModif = null;
                    }
                    scenario.Runs.add(run);
                }
            }

            final EMHRun runCourant = managerScenario.getRunCourant();

            if (runCourant != null) {
                scenario.RunCourant = Ref.createRunRef(runCourant.getNom());
            }
            if (crue9) {
                scenario.FichEtudes = null;
                scenario.Modeles = null;
            }

            dao.Scenarios.add(scenario);
        }
    }

    /**
     * Remplissage de la base des sous modeles. Affectation des fichiers de données aux sous modeles concernés.
     *
     * @param dest
     * @param listePersist
     * @param infos
     * @param analyser
     */
    public static void daoToMetierBaseSousModeles(final EMHProjet dest, final List<SousModele> listePersist,
                                                  final EMHProjectInfos infos, final CtuluLog analyser) {

        if (CollectionUtils.isNotEmpty(listePersist)) {
            for (final SousModele smPersist : listePersist) {
                // -- remplisage des infos --//
                final EMHInfosVersion infosVersions = daoToMetierInfos(smPersist, analyser);
                if (smPersist.Nom != null) {
                    final ManagerEMHSousModele smMetier = new ManagerEMHSousModele(smPersist.Nom);
                    smMetier.setActive(isActive(smPersist.IsActive));
                    smMetier.setInfosVersions(infosVersions);
                    if (smPersist.SousModeleFichEtudes != null) {
                        for (final Ref reference : smPersist.SousModeleFichEtudes) {
                            // -- on essaie de retrouver la reference du fichier --//
                            if (reference.NomRef != null && infos.existFileInBase(reference.NomRef)) {
                                final FichierCrue fichierData = infos.getFileFromBase(reference.NomRef);
                                smMetier.addFichierDonnees(fichierData);
                            } else {
                                CrueHelper.unknowReference("Fichier", reference.NomRef, analyser);
                            }
                        }
                    }
                    dest.addBaseSousModele(smMetier);
                } else {
                    CrueHelper.errorIdNonUnique("SousModele", smPersist.Nom, analyser);
                }
            }
        }
    }

    private static boolean isActive(final Boolean value) {
        return value == null || value.booleanValue();
    }

    public void metierToDaoBaseSousModeles(final EMHProjet metier, final CrueDaoETU dao, final FichierCrueComparator comparator) {
        dao.SousModeles = new ArrayList<>();
        final List<ManagerEMHSousModele> listeSousModeles = new ArrayList<>(metier.getListeSousModeles());
        Collections.sort(listeSousModeles, ObjetNommeByNameComparator.INSTANCE);
        for (final ManagerEMHSousModele managerSousModele : listeSousModeles) {
            final SousModele sousModele = new SousModele();

            sousModele.Nom = managerSousModele.getNom();
            sousModele.Type = managerSousModele.getInfosVersions().getType();
            sousModele.IsActive = managerSousModele.isCrue9() ? null : managerSousModele.isActive();
            CrueConverterETU.metierToDaoInfos(managerSousModele.getInfosVersions(), sousModele);

            sousModele.SousModeleFichEtudes = new ArrayList<>();

            final FichierCrueManager fichiers = managerSousModele.getListeFichiers();

            if (fichiers != null) {
                final List<FichierCrue> sortedFiles = sortFiles(fichiers, comparator);
                for (final FichierCrue fichier : sortedFiles) {
                    sousModele.SousModeleFichEtudes.add(Ref.createFileReference(fichier.getNom()));
                }
            }

            dao.SousModeles.add(sousModele);
        }
    }

    /**
     * Remplissage de la base des modeles. Affectation des fichiers de données aux sous modeles concernés. Affectation des sous modeles aux modeles
     * concernés.
     *
     * @param project
     * @param listePersist
     * @param infos
     * @param analyser
     */
    private static void daoToMetierBaseModeles(final EMHProjet project, final List<Modele> listePersist,
                                               final EMHProjectInfos infos, final CtuluLog analyser) {

        if (listePersist != null) {
            for (final Modele smPersist : listePersist) {
                // -- remplisage des infos --//
                final EMHInfosVersion infosVersions = daoToMetierInfos(smPersist, analyser);
                if (smPersist.Nom != null) {
                    final ManagerEMHModeleBase newModeleMetier = new ManagerEMHModeleBase(smPersist.Nom);
                    newModeleMetier.setActive(isActive(smPersist.IsActive));
                    newModeleMetier.setInfosVersions(infosVersions);
                    // -- ajout des fichiers de données --//
                    if (smPersist.FichEtudes != null) {
                        for (final Ref reference : smPersist.FichEtudes) {
                            // -- on essaie de retrouver la reference du fichier --//
                            if (reference.NomRef != null && infos.existFileInBase(reference.NomRef)) {
                                final FichierCrue fichierData = infos.getFileFromBase(reference.NomRef);
                                newModeleMetier.addFichierDonnees(fichierData);
                            } else {
                                CrueHelper.unknowReference("Fichier", reference.NomRef, analyser);
                            }
                        }
                    }
                    // -- ajout de sous modeles --//
                    if (CollectionUtils.isNotEmpty(smPersist.SousModeles)) {
                        for (final Ref reference : smPersist.SousModeles) {
                            // -- on essaie de retrouver la reference du fichier --//
                            final ManagerEMHSousModele sousModeleData = project.getSousModele(reference.NomRef);
                            if (sousModeleData != null) {
                                newModeleMetier.addManagerFils(sousModeleData);
                            } else {
                                CrueHelper.unknowReference("SousModele", reference.NomRef, analyser);
                            }
                        }
                    }
                    // -- ajout du sous modele a la liste --//
                    project.addBaseModele(newModeleMetier);
                } else {
                    CrueHelper.errorIdNonUnique("Modele", smPersist.Nom, analyser);
                }
            }
        }
    }

    public void metierToDaoBaseModeles(final EMHProjet metier, final CrueDaoETU dao, final FichierCrueComparator comparator) {
        dao.Modeles = new ArrayList<>();
        final List<ManagerEMHModeleBase> listeModeles = new ArrayList<>(metier.getListeModeles());
        Collections.sort(listeModeles, ObjetNommeByNameComparator.INSTANCE);
        for (final ManagerEMHModeleBase managerModele : listeModeles) {
            final boolean crue9 = managerModele.isCrue9();
            final Modele modele = crue9 ? new ModeleV9() : new Modele();

            modele.Nom = managerModele.getNom();
            modele.Type = managerModele.getInfosVersions().getType();
            modele.IsActive = managerModele.isCrue9() ? null : managerModele.isActive();
            CrueConverterETU.metierToDaoInfos(managerModele.getInfosVersions(), modele);

            modele.FichEtudes = new ArrayList<>();

            final FichierCrueManager fichiers = managerModele.getListeFichiers();

            if (fichiers != null) {
                final List<FichierCrue> sortedFiles = sortFiles(fichiers, comparator);
                for (final FichierCrue fichier : sortedFiles) {
                    modele.FichEtudes.add(Ref.createFileReference(fichier.getNom()));
                }
            }

            if (!crue9) {
                modele.SousModeles = new ArrayList<>();

                for (final ManagerEMHSousModele managerSousModele : managerModele.getFils()) {
                    modele.SousModeles.add(Ref.createManagerReference(managerSousModele.getNom()));
                }
            }

            dao.Modeles.add(modele);
        }
    }

    /**
     * Remplit les infos avce les commentaires,dates, user,... Donnees optionnelles.
     *
     * @param persistance
     */
    public static EMHInfosVersion daoToMetierInfos(final CrueEtuInfosContainer persistance, final CtuluLog log) {
        final EMHInfosVersion infos = new EMHInfosVersion();

        if (persistance.getAuteurDerniereModif() != null) {
            infos.setAuteurDerniereModif(persistance.getAuteurDerniereModif().trim());
        }
        if (persistance.getCommentaire() != null) {
            infos.setCommentaire(persistance.getCommentaire().trim());
        }
        if (persistance.getAuteurCreation() != null) {
            infos.setAuteurCreation(persistance.getAuteurCreation().trim());
        }
        if (persistance.getDateCreation() != null) {
            infos.setDateCreation(DateDurationConverter.getDate(persistance.getDateCreation()));
        }
        if (persistance.getAuteurDerniereModif() != null) {
            infos.setAuteurDerniereModif(persistance.getAuteurDerniereModif().trim());
        }
        if (persistance.getDateDerniereModif() != null) {
            infos.setDateDerniereModif(DateDurationConverter.getDate(persistance.getDateDerniereModif()));
        }
        final CrueVersionType crueVersionType = CrueVersionType.getVersionFromType(persistance.getType());
        infos.setVersion(crueVersionType);

        return infos;
    }

    /**
     * Remplit les infos avce les commentaires,dates, user,... Donnees optionnelles.
     *
     * @param infosVersion Les informations à utiliser.
     * @param container    La Dao à remplir.
     */
    public static void metierToDaoInfos(final EMHInfosVersion infosVersion, final CrueEtuInfosContainer container) {
        container.setAuteurDerniereModif(StringUtils.trim(infosVersion.getAuteurDerniereModif()));
        container.setCommentaire(StringUtils.defaultString(StringUtils.trim(infosVersion.getCommentaire())));
        container.setAuteurCreation(StringUtils.trim(infosVersion.getAuteurCreation()));
        container.setCrueVersion(infosVersion.getCrueVersion());
        final LocalDateTime dateCreation = infosVersion.getDateCreation();

        if (dateCreation != null) {
            container.setDateCreation(DateDurationConverter.dateToXsd(dateCreation));
        }
        container.setAuteurDerniereModif(StringUtils.trim(infosVersion.getAuteurDerniereModif()));

        final LocalDateTime dateDerniereModif = infosVersion.getDateDerniereModif();

        if (dateDerniereModif != null) {
            container.setDateDerniereModif(DateDurationConverter.dateToXsd(dateDerniereModif));
        }
    }

    /**
     * Remplit la base de fichiers du projet.
     *
     * @param fichEtudesPersist
     * @param infoProjet
     * @param analyser
     */
    public static void daoToMetierBaseFichiers(final List<TypeFichierDispo> fichEtudesPersist,
                                               final EMHProjectInfos infoProjet, final CtuluLog analyser) {
        if (CollectionUtils.isNotEmpty(fichEtudesPersist)) {
            for (final TypeFichierDispo fichierPersist : fichEtudesPersist) {

                if (fichierPersist.Nom != null && fichierPersist.Chemin != null && fichierPersist.getType() != null) {
                    CrueFileType fichierType = null;
                    try {
                        fichierType = CrueFileType.valueOf(fichierPersist.getType().trim());
                    } catch (final Exception e) {
                        Logger.getLogger(CrueConverterETU.class.getName()).log(Level.INFO, "message {0}", e);
                    }

                    final FichierCrue file = new FichierCrue(fichierPersist.Nom.trim(), fichierPersist.Chemin.trim(), fichierType);

                    if (!infoProjet.addCrueFileToProject(file)) {
                        CrueHelper.errorIdNonUnique("FichEtudes", file.getNom(), analyser);
                    }
                } else {
                    analyser.addInfo("io.etu.param.manquant.error", "FichEtudes");
                }
            }
        }
    }

    public void metierToDaoBaseFichiers(final List<FichierCrue> fichiers, final CrueDaoETU dao) {
        dao.FichEtudes = new ArrayList<>();

        for (final FichierCrue fichier : fichiers) {
            //ignore en v1.1.1
            if (fileMustBeSkip(fichier)) {
                continue;
            }
            final TypeFichierDispo typeFichier = new TypeFichierDispo();

            typeFichier.Nom = fichier.getNom();
            typeFichier.Chemin = fichier.getPath();
            typeFichier.Type = fichier.getType().name();

            if (typeFichier.Chemin == null) {
                typeFichier.Chemin = ".\\";
            }

            dao.FichEtudes.add(typeFichier);
        }
    }

    /**
     * Remplit les directories: les répertoires principaux qui contiennent les fichier de données, ETUDE,RUN et RAPPORT. Gere les no clef et retourne
     * les erreurs en cas de probléme.
     *
     * @param directoriesPersist
     * @param infoProjet
     * @param analyser
     */
    public static void daoToMetierBaseDirectories(final List<Repertoire> directoriesPersist,
                                                  final EMHProjectInfos infoProjet, final CtuluLog analyser) {
        if (CollectionUtils.isNotEmpty(directoriesPersist)) {
            final Map<String, String> directories = new HashMap<>();

            for (final Repertoire fichierPersist : directoriesPersist) {

                if (fichierPersist.Nom != null && fichierPersist.path != null) {
                    final String key = fichierPersist.Nom;
                    if (key.toUpperCase().equals(EMHProjectInfos.FICHETUDES)) {
                        // -- fichier etudes --//
                        directories.put(EMHProjectInfos.FICHETUDES, fichierPersist.path.trim());
                    } else if (key.toUpperCase().equals(EMHProjectInfos.RAPPORTS)) {
                        // -- fichier etudes --//
                        directories.put(EMHProjectInfos.RAPPORTS, fichierPersist.path.trim());
                    } else if (key.toUpperCase().equals(EMHProjectInfos.RUNS)) {
                        // -- fichier etudes --//
                        directories.put(EMHProjectInfos.RUNS, fichierPersist.path.trim());
                    } else if (key.toUpperCase().equals(EMHProjectInfos.CONFIG)) {
                        // -- fichier etudes --//
                        directories.put(EMHProjectInfos.CONFIG, fichierPersist.path.trim());
                    } else {
                        // -- erreur clef non reconnue --//

                        analyser.addInfo("io.etu.convert.mot.cle.invalide.error", "Repertoires", key);
                    }
                } else {
                    CrueHelper.messageBaliseError("Repertoires",
                            "<{balise}>:Erreur Fichier ETU: un des 2 paramètres (nom,path) est manquant", analyser);
                }
            }
            final List<String> repertoires = EMHProjectInfos.getRepertoires();
            for (final String rep : repertoires) {
                if (!directories.containsKey(rep)) {
                    analyser.addSevereError("io.etu.convert.repertoireNotDefined", rep);
                }
            }

            // -- ajout du directories dans les infos EMH --//
            infoProjet.setDirectories(directories);
        }
    }

    public static void metierToDaoBaseDirectories(final Map<String, String> directories, final CrueDaoETU dao) {
        dao.Repertoires = new ArrayList<>();

        for (final Entry<String, String> entry : directories.entrySet()) {
            final Repertoire repertoire = new Repertoire();
            repertoire.Nom = entry.getKey();
            repertoire.path = entry.getValue();
            dao.Repertoires.add(repertoire);
        }
        Collections.sort(dao.Repertoires);
    }
}
