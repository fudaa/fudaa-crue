/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dpti;

import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.dpti.CrueDaoStructureDPTI.BrancheAbstract;
import org.fudaa.dodico.crue.io.dpti.CrueDaoStructureDPTI.CasierAbstract;
import org.fudaa.dodico.crue.io.dpti.CrueDaoStructureDPTI.NoeudNiveauContinu;
import org.fudaa.dodico.crue.io.dpti.CrueDaoStructureDPTI.SectionAbstract;

/**
 * Classe persistante qui reprend la meme structure que le fichier xml A persister telle qu'elle. Le fichier DPTI decrit
 * les conditions initiales (dont les manoeuvres d'ouvrages) d'un modele CrueX. Son perimetre est le modele
 * 
 * @author Adrien Hadoux
 */
@SuppressWarnings("PMD.VariableNamingConventions")
public class CrueDaoDPTI extends AbstractCrueDao {

  /**
   * la liste des noeuds
   */
  List<NoeudNiveauContinu> DonPrtCIniNoeuds;

  /**
   * la liste des casiers
   */
  List<CasierAbstract> DonPrtCIniCasiers;

  /**
   * la liste des sections
   */
  List<SectionAbstract> DonPrtCIniSections;

  /**
   * la liste des branches
   */
  List<BrancheAbstract> DonPrtCIniBranches;

}
