/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author deniger
 */
public class ParametrageDao {

  public static class DelimiteurDao {

    String Nom;
    String Chaine;

    public DelimiteurDao() {
    }

    public String getChaine() {
      return Chaine;
    }

    public String getNom() {
      return Nom;
    }
  }
  int NbrOctetMot;
  List<DelimiteurDao> Delimiteur;

  public List<DelimiteurDao> getDelimiteur() {
    return Delimiteur;
  }

  public String getDelimiteurChaine(String nom) {
    if(Delimiteur==null){
    return null;
  }
    for (DelimiteurDao delimiteur : Delimiteur) {
      if (StringUtils.equals(nom, delimiteur.getNom())) {
        return delimiteur.getChaine();
      }
    }
    return null;
  }

  public int getNbrOctetMot() {
    return NbrOctetMot;
  }
  
  public int getBytes(int nbMot){
    return NbrOctetMot*nbMot;
  }

  public void setNbrOctetMot(int NbrOctetMot) {
    this.NbrOctetMot = NbrOctetMot;
  }
}
