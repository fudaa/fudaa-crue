/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.lhpt.CrueConverterLHPT;
import org.fudaa.dodico.crue.io.lhpt.CrueDaoStructureLHPT;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.aoc.LoiMesuresPost;

public class CrueFileFormatBuilderLHPT implements CrueFileFormatBuilder<LoiMesuresPost> {

    @Override
    public Crue10FileFormat<LoiMesuresPost> getFileFormat(final CoeurConfigContrat coeurConfig) {
        return new Crue10FileFormat<>(new CrueDataXmlReaderWriterImpl<>(CrueFileType.LHPT,
                coeurConfig, new CrueConverterLHPT(), new CrueDaoStructureLHPT(coeurConfig.getXsdVersion())));

    }

}
