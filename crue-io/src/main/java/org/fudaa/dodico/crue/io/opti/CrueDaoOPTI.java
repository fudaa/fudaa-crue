/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.opti;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.opti.CrueDaoStructureOPTI.MethodesInterpolations;
import org.fudaa.dodico.crue.io.opti.CrueDaoStructureOPTI.RegleOPTIDAO;
import org.fudaa.dodico.crue.metier.emh.Sorties;

/**
 * Représentation persistante du fichier xml OPTI (Fichier des ordres pour le prétraitement des conditions initiales (xml)).
 *
 * @author Adrien Hadoux
 */
public class CrueDaoOPTI extends AbstractCrueDao {

  private Sorties Sorties;
  /**
   * Choix d'une interpolation.
   *
   * @NB Adrien: Ici la structure est une liste ce n'est pas nécessaire pour l'état actuel du fichier. car il y a juste une
   * interpolation. Mais il vaut mieux anticiper une éventuelle modif du fichier.
   */
  private List<MethodesInterpolations> MethodeInterpol;
  public List<RegleOPTIDAO> Regles;

  public CrueDaoOPTI() {
    MethodeInterpol = new ArrayList<>();
  }

  public List<MethodesInterpolations> getListeInterpol() {
    return MethodeInterpol;
  }

  public void setListeInterpol(final List<MethodesInterpolations> listeInterpol) {
    this.MethodeInterpol = listeInterpol;
  }

  /**
   * @return the sorties
   */
  public Sorties getSorties() {
    return Sorties;
  }

  /**
   * @param sorties the sorties to set
   */
  public void setSorties(final Sorties sorties) {
    this.Sorties = sorties;
  }
}
