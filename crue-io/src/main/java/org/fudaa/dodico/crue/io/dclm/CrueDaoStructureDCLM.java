/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dclm;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.common.EnumsConverter;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EnumSensOuv;

/**
 * Structures utilisees dans la classe CrueDaoPersistDCLM
 *
 * @author CDE
 */
public class CrueDaoStructureDCLM implements CrueDataDaoStructure {

  private String noeudNiveauContinuQapp = "CalcPseudoPermNoeudQapp";
  private String noeudNiveauContinuZimp = "CalcPseudoPermNoeudNiveauContinuZimp";
  private String transNoeudNiveauContinuQapp = "CalcTransNoeudQapp";
  private boolean ignoreIsActive;


  public void initForV1P1P1() {
    noeudNiveauContinuQapp = "CalcPseudoPermNoeudNiveauContinuQapp";
    noeudNiveauContinuZimp = "CalcPseudoPermNoeudNiveauContinuZimpose";
    transNoeudNiveauContinuQapp = "CalcTransNoeudNiveauContinuQapp";
    ignoreIsActive = true;
  }

  /**
   * Initialise le parser XML en lui donnant les alias a utiliser pour avoir des noms de balises courts et en précisant
   * quels sont les objets avec un attribut dans la balise correspondante
   *
   * @param xstream Parser XML
   */
  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {

    xstream.alias(CrueFileType.DCLM.toString(), CrueDaoDCLM.class);

    xstream.addImplicitCollection(CrueDaoDCLM.class, "listeCalculs");

    xstream.alias("CalcPseudoPerm", CrueDaoDCLMContents.CalculPermanentPersist.class);
    xstream.addImplicitCollection(CrueDaoDCLMContents.CalculPermanentPersist.class, "listeElementsCalculPermanent");
    xstream.alias(noeudNiveauContinuQapp, CrueDaoDCLMContents.CalcPseudoPermNoeudNiveauContinuQappPersist.class);
    xstream.alias(noeudNiveauContinuZimp, CrueDaoDCLMContents.CalcPseudoPermNoeudNiveauContinuZimposePersist.class);
    xstream.alias("CalcPseudoPermBrancheOrificeManoeuvre",
        CrueDaoDCLMContents.CalcPseudoPermBrancheOrificeManoeuvrePersist.class);
    xstream.alias("CalcPseudoPermBrancheOrificeManoeuvreRegul",
        CrueDaoDCLMContents.CalcPseudoPermBrancheOrificeManoeuvreRegulPersist.class);

    xstream.alias("CalcPseudoPermBrancheSaintVenantQruis",
        CrueDaoDCLMContents.CalcPseudoPermBrancheBrancheSaintVenantQruisPersist.class);
    xstream.alias("CalcPseudoPermCasierProfilQruis",
        CrueDaoDCLMContents.CalcPseudoPermBrancheCasierProfilQruisPersist.class);
    xstream.alias("CalcPseudoPermNoeudBg1",
        CrueDaoDCLMContents.CalcPseudoPermNoeudBg1Persist.class);
    xstream.alias("CalcPseudoPermNoeudBg1Av",
        CrueDaoDCLMContents.CalcPseudoPermNoeudBg1AvPersist.class);
    xstream.alias("CalcPseudoPermNoeudBg2",
        CrueDaoDCLMContents.CalcPseudoPermNoeudBg2Persist.class);
    xstream.alias("CalcPseudoPermNoeudBg2Av",
        CrueDaoDCLMContents.CalcPseudoPermNoeudBg2AvPersist.class);
    xstream.alias("CalcPseudoPermNoeudUsi",
        CrueDaoDCLMContents.CalcPseudoPermNoeudUsiPersist.class);

    xstream.alias("CalcTrans", CrueDaoDCLMContents.CalculTransitoirePersist.class);

    xstream.addImplicitCollection(CrueDaoDCLMContents.CalculTransitoirePersist.class, "listeElementsCalculTransitoire");

    xstream.alias("HydrogrammeQapp", CrueDaoDCLMContents.HydrogrammeQappPersist.class);
    xstream.alias("HydrogrammeQruis", CrueDaoDCLMContents.HydrogrammeQruisPersist.class);
    xstream.alias("Limnigramme", CrueDaoDCLMContents.LimnigrammePersist.class);
    xstream.alias("Manoeuvre", CrueDaoDCLMContents.ManoeuvrePersist.class);
    xstream.alias("ManoeuvreRegul", CrueDaoDCLMContents.ManoeuvreRegulPersist.class);
    xstream.useAttributeFor(CrueDaoDCLMContents.ManoeuvreRegulPersist.class,"Param");

    xstream.alias("Tarage", CrueDaoDCLMContents.TarragePersist.class);

    xstream.alias(transNoeudNiveauContinuQapp, CrueDaoDCLMContents.CalcTransNoeudNiveauContinuHydrogrammePersist.class);
    xstream.alias("CalcTransNoeudQappExt", CrueDaoDCLMContents.CalcTransNoeudQappExtPersist.class);
    xstream.alias("CalcTransNoeudBg1", CrueDaoDCLMContents.CalcTransNoeudBg1Persist.class);
    xstream.alias("CalcTransNoeudBg2", CrueDaoDCLMContents.CalcTransNoeudBg2Persist.class);
    xstream.alias("CalcTransNoeudBg1Av", CrueDaoDCLMContents.CalcTransNoeudBg1AvPersist.class);
    xstream.alias("CalcTransNoeudBg2Av", CrueDaoDCLMContents.CalcTransNoeudBg2AvPersist.class);
    xstream.alias("CalcTransNoeudUsi", CrueDaoDCLMContents.CalcTransNoeudUsinePersist.class);

    xstream.alias("CalcTransNoeudNiveauContinuLimnigramme",
        CrueDaoDCLMContents.CalcTransNoeudNiveauContinuLimnigrammePersist.class);
    xstream.alias("CalcTransNoeudNiveauContinuTarage", CrueDaoDCLMContents.CalcTransNoeudNiveauContinuTaragePersist.class);
    xstream.alias("CalcTransBrancheOrificeManoeuvre", CrueDaoDCLMContents.CalcTransBrancheOrificeManoeuvrePersist.class);
    xstream.alias("CalcTransBrancheOrificeManoeuvreRegul", CrueDaoDCLMContents.CalcTransBrancheOrificeManoeuvreRegulPersist.class);
    xstream.alias("CalcTransBrancheSaintVenantQruis",
        CrueDaoDCLMContents.CalcTransBrancheSaintVenantHydrogrammeRuisPersist.class);
    xstream.alias("CalcTransCasierProfilQruis", CrueDaoDCLMContents.CalcTransCasierProfilHydrogrammeRuisPersist.class);

    xstream.useAttributeFor(CrueDaoDCLMContents.CalculAbstractPersist.class, "Nom");
    if (ignoreIsActive) {
      xstream.omitField(CrueDaoDCLMContents.ActivableDcml.class, "IsActive");
      xstream.aliasField("Zimpose", CrueDaoDCLMContents.CalcPseudoPermNoeudNiveauContinuZimposePersist.class, "Zimp");


    }
    xstream.useAttributeFor(CrueDaoDCLMContents.RefDCLMAbstractPersist.class, "NomRef");
    xstream.useAttributeFor(CrueDaoDCLMContents.HydrogrammeQappExtPersist.class, "NomFic");
    xstream.useAttributeFor(CrueDaoDCLMContents.HydrogrammeQappExtPersist.class, "ResCalcTrans");
    xstream.registerConverter(EnumsConverter.createEnumConverter(EnumSensOuv.class, ctuluLog));


  }

}
