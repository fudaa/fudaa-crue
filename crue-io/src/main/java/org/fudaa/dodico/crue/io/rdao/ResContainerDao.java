/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

/**
 *
 * @author deniger
 */
public interface ResContainerDao {
  
  ContexteSimulationDao getContexteSimulation();
  StructureResultatsDao getStructureResultat();
  ParametrageDao getParametrage();

}
