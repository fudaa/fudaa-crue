/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.ItemResDao;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.VariableResDao;

/**
 *
 * @author deniger
 */
public class SectionsDao extends CommonResDao.NbrMotDao implements ResContainerEMHCat {

  public static class SectionDao extends ItemResDao {
  }

  public static class SectionTypeDao extends CommonResDao.TypeEMHDao implements ResContainerEMHType {

    List<CommonResDao.VariableResDao> VariableRes;
    List<SectionDao> Section;

    public List<SectionDao> getSection() {
      return Section;
    }

    @Override
    public List<? extends CommonResDao.ItemResDao> getItemRes() {
      return getSection();
    }

    @Override
    public List<VariableResDao> getVariableRes() {
      return VariableRes;
    }
  }
  List<CommonResDao.VariableResDao> VariableRes;
  SectionTypeDao SectionIdem;
  SectionTypeDao SectionInterpolee;
  SectionTypeDao SectionProfil;
  SectionTypeDao SectionSansGeometrie;

  public SectionTypeDao getSectionIdem() {
    return SectionIdem;
  }

  public SectionTypeDao getSectionInterpolee() {
    return SectionInterpolee;
  }

  public SectionTypeDao getSectionProfil() {
    return SectionProfil;
  }

  public SectionTypeDao getSectionSansGeometrie() {
    return SectionSansGeometrie;
  }

  @Override
  public List<VariableResDao> getVariableRes() {
    return VariableRes;
  }
  List<ResContainerEMHType> resContainerEMHType;

  @Override
  public List<ResContainerEMHType> getResContainerEMHType() {
    if (resContainerEMHType == null) {
      //attention, l'ordre est très important: doit suivre celui du fichier
      resContainerEMHType = Collections.unmodifiableList(Arrays.<ResContainerEMHType>asList(SectionIdem, SectionInterpolee,
                                                                                            SectionProfil, SectionSansGeometrie));
    }
    return resContainerEMHType;
  }

  @Override
  public String getDelimiteurNom() {
    return "CatEMHSection";
  }
}
