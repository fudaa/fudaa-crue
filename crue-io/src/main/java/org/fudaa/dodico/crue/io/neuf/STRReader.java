/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import gnu.trove.TObjectIntHashMap;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.StoContent;

/**
 * Classe permettant de lire le fichier STR en le parcourant une première fois pour repérer la position de chaque profil et leur nom associé
 *
 * @author cde
 */
public class STRReader extends AbstractCrueBinaryReader<STRSequentialReader> {

  @Override
  protected STRSequentialReader internalReadResu() throws IOException {
    final CrueData crueData = this.getDataLinked();
    final STRSequentialReader data = new STRSequentialReader(crueData.getCrueConfigMetier());
    if (crueData == null || crueData.getSto() == null) {
      analyze_.addSevereError("io.str.lg.enreg.indefinie.error");
      return null;
    }

    final StoContent sto = crueData.getSto();

    if (sto.isParamGenOrDimensionNotDefined()) {
      analyze_.addSevereError("io.str.paramgen.dimension.null.error");
      return null;
    }

    // Lors de la lecture de sto, on enregistre l'ordre
    data.byteOrder = sto.getOrder();
    data.npo = sto.getNpo();
    data.nbStr = sto.getNbstr();
    data.nbLitMax = sto.getNblitmax();
    data.nbHaut = sto.getNbhaut();

    data.file = file;
    // 49 + 2*Npo + (17 + 2*NbStr)*NbLitMax + 9*NbHaut
    int longueurEnregistrement = 49 + 2 * data.npo + (17 + 2 * data.nbStr) * data.nbLitMax + 9 * data.nbHaut;
    longueurEnregistrement = longueurEnregistrement * 4;
    // 37 + (7+TAILLE_STRI)*NBLITMAX + 2*NPO + 2*(2*NBLITMAX) + 12 + 9*NBHAUT + 4 + 12*NBHAUT)
    int longueurEnregistrement2 = 37 + (7+ (8+2*(data.nbStr)) )*data.nbLitMax + 2*data.npo + 2*(2*data.nbLitMax) + 12 + 9*data.nbHaut + 4 + 12*data.nbHaut;
    longueurEnregistrement2 = longueurEnregistrement2 * 4;

    final int longueurEnregistrementSTO = sto.getLReclProf();

    if (longueurEnregistrement == (longueurEnregistrementSTO)) {
		//Format fortran non standard (intel) non windows
		data.setFormatFortranStandard(false);
		data.longueurEnregistrement = longueurEnregistrementSTO;
    } else if (longueurEnregistrement == (longueurEnregistrementSTO*4)) {
		//Format fortran non standard (intel) windows
		data.setFormatFortranStandard(false);
		data.longueurEnregistrement = longueurEnregistrementSTO * 4;
	} else if (longueurEnregistrement2 == longueurEnregistrementSTO) {
		//Format fortran standard (gcc)
		data.setFormatFortranStandard(true);
		data.longueurEnregistrement = longueurEnregistrementSTO;
	} else {
      analyze_.addWarn("io.str.lg.enreg.diff.error");
    }

    final int nbProfil = (int) (helper.getChannel().size() / data.longueurEnregistrement);
    final int nbProfilStoRes = sto.getNbRecProf();
    if (nbProfilStoRes != nbProfil) {
      analyze_.addError("io.str.nbProfil.wrong.error", Integer.valueOf(nbProfil));
      return null;
    }

    data.nomProfils = new ArrayList<>(nbProfil);
    data.nomProfilPosition = new TObjectIntHashMap(nbProfil);
    final int lengthNom = 16;
    for (int i = 0; i < nbProfil; i++) {
      final ByteBuffer bf = ByteBuffer.allocate(lengthNom);
      helper.getChannel().read(bf, (long)i * data.longueurEnregistrement);
      bf.rewind();

      String nom = STRSequentialReader.getStringFromBuffer(bf, lengthNom);
      data.nomProfils.add(nom);
      data.nomProfilPosition.put(nom, i);
    }

    return data;
  }
}
