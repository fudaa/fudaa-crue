/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import org.fudaa.dodico.crue.metier.emh.LoiFF;

/**
 * Résultats de prétraitement géométrique pour les sections de type EMHSectionProfil uniquement.
 *
 */
public class Crue9ResPrtGeoSection extends Crue9ResPrtGeo {
  
  
  private final int typBra;
  private final boolean stricklerWith1Lit;

  public Crue9ResPrtGeoSection(int typBra,boolean stricklerWith1Lit) {
    this.typBra = typBra;
    this.stricklerWith1Lit=stricklerWith1Lit;
  }

  public int getTypBra() {
    return typBra;
  }

  public boolean isStricklerWith1Lit() {
    return stricklerWith1Lit;
  }
  
  
  /**
   * @param newNumLitZf
   */
  public final void setNumLitZf(int newNumLitZf) {
    super.setValue("numLitZf", newNumLitZf);
  }

  /**
   * @param newZf
   */
  public final void setZf(double newZf) {
    super.setValue("zf", newZf);
  }
  /**
   * @param newZf
   */
  public final void setCoefSinuo(double coefSinuo) {
    super.setValue("coefSinuo", coefSinuo);
  }

  /**
   * @param newZHaut
   */
  public final void setZhaut(double newZHaut) {
    super.setValue("zhaut", newZHaut);
  }

  /**
   * @param newLitKsup
   */
  public final void setLstLitKsup(LoiFF newLitKsup) {
    super.setValue("lstLitKsup", newLitKsup);
  }

  /**
   * @param newLitLsup
   */
  public final void setLstLitLsup(LoiFF newLitLsup) {
    super.setValue("lstLitLsup", newLitLsup);
  }

  void setLstLitTypeLit(LoiFF lstLitTypeLit) {
    super.setValue("lstLitTypeLit", lstLitTypeLit);
  }

  /**
   * @param newLitPsup
   */
  public final void setLstLitPsup(LoiFF newLitPsup) {
    super.setValue("lstLitPsup", newLitPsup);
  }

  /**
   * @param newLitSsup
   */
  public final void setLstLitSsup(LoiFF newLitSsup) {
    super.setValue("lstLitSsup", newLitSsup);
  }

  public final void setLstLitPosBordure(LoiFF newLitSsup) {
    super.setValue("lstLitPosBordure", newLitSsup);
  }

  /**
   * @param newLitZf
   */
  public final void setLstLitZf(LoiFF newLitZf) {
    super.setValue("lstLitZf", newLitZf);
  }

  /**
   * @param newLitLinf
   */
  public final void setLstLitLinf(LoiFF newLitLinf) {
    super.setValue("lstLitLinf", newLitLinf);
  }

  /**
   * @param newZBeta
   */
  public final void setLoiZBeta(LoiFF newZBeta) {
    super.setValue("loiZBeta", newZBeta);
  }

  /**
   * @param newZDact
   */
  public final void setLoiZDact(LoiFF newZDact) {
    super.setValue("loiZDact", newZDact);
  }

  /**
   * @param newZLact
   */
  public final void setLoiZLact(LoiFF newZLact) {
    super.setValue("loiZLact", newZLact);
  }
  /**
   * 
   * @param newZLcont 
   */
  public final void setLoiZLcont(LoiFF newZLcont) {
    super.setValue("loiZLcont", newZLcont);
  }

  /**
   * @param newZSact
   */
  public final void setLoiZSact(LoiFF newZSact) {
    super.setValue("loiZSact", newZSact);
  }

  /**
   * @param newFenteTxD
   */
  public final void setFenteTxD(double newFenteTxD) {
    super.setValue("fenteTxD", newFenteTxD);
  }

  /**
   * @param newFenteTxS
   */
  public final void setFenteTxS(double newFenteTxS) {
    super.setValue("fenteTxS", newFenteTxS);
  }

  /**
   * @param newZLsto
   */
  public final void setLoiZLsto(LoiFF newZLsto) {
    super.setValue("loiZLsto", newZLsto);
  }

  public final void setLoiZLtot(LoiFF newZLtot) {
    super.setValue("loiZLtot", newZLtot);
  }
}
