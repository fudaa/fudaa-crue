/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import java.util.List;

/**
 *
 * @author deniger
 */
public interface ResContainerEMHType {

  /**
   * @return les variables définies pour ce type d'EMH
   */
  List<CommonResDao.VariableResDao> getVariableRes();

  List<? extends CommonResDao.ItemResDao> getItemRes();

  int getNbrMot();

  String getTypeEMH();
}
