/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.fudaa.dodico.crue.metier.emh.LoiFF;

public class Crue9ResPrtGeo {

  protected final Map<String, Object> values = new HashMap<>();
  protected final Map<String, Object> valuesExt = Collections.unmodifiableMap(values);
  private String nom;

  public Crue9ResPrtGeo() {
  }

  public final String getNom() {
    return nom;
  }
  private String id;

  public String getId() {
    return id;
  }

  public final void setNom(final String newNom) {
    nom = newNom;
    if (nom != null) {
      id = nom.toUpperCase();
    }
  }

  public LoiFF getLoi(final String prop) {
    return (LoiFF) values.get(prop);
  }

  public double getValue(final String prop) {
    final Double res = (Double) values.get(prop);
    return res == null ? Double.NaN : res.doubleValue();
  }

  public Map<String, Object> getValues() {
    return valuesExt;
  }

  public void setValue(final String prop, final double v) {
    values.put(prop, v);
  }

  public void setValue(final String prop, final LoiFF loi) {
    final LoiFF old = (LoiFF) values.remove(prop);
    if (old != null) {
      old.unregister(this);
    }
    if (loi != null) {
      loi.register(this);
      values.put(prop, loi);
    }
  }
}
