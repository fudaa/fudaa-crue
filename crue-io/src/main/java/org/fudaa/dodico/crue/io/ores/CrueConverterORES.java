/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores;

import java.util.ArrayList;
import java.util.List;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.common.CrueHelper;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.*;

/**
 * Classe qui se charge de remplir les structures DAO du fichier ORES avec les donnees metier et inversement.
 *
 * @author cde
 */
public class CrueConverterORES implements CrueDataConverter<CrueDaoORES, OrdResScenario> {

  public CrueConverterORES() {
    super();
  }

  @Override
  public OrdResScenario getConverterData(final CrueData in) {
    return in.getORES();
  }

  /**
   * Conversion de la couche persistance vers la couche métier
   */
  @Override
  public OrdResScenario convertDaoToMetier(final CrueDaoORES dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
    final OrdResScenario metier = dataLinked == null ? new OrdResScenario() : dataLinked.getOrCreateORES();
    if (dao.OrdResNoeuds.OrdResNoeudNiveauContinu != null) {
      final OrdResNoeudNiveauContinu res = new OrdResNoeudNiveauContinu();
      res.setValues(dao.OrdResNoeuds.OrdResNoeudNiveauContinu.Ddes);
      metier.setOrdResNoeudNiveauContinu(res);
    }
    if (dao.OrdResCasiers.OrdResCasier != null) {
      final OrdResCasier res = new OrdResCasier();
      res.setValues(dao.OrdResCasiers.OrdResCasier.Ddes);
      metier.setOrdResCasier(res);
    }
    if (dao.OrdResSections.OrdResSection != null) {
      final OrdResSection res = new OrdResSection();
      res.setValues(dao.OrdResSections.OrdResSection.Ddes);
      metier.setOrdResSection(res);
    }

    if (dao.OrdResBranches.OrdResBrancheBarrageFilEau != null) {
      final OrdResBrancheBarrageFilEau res = new OrdResBrancheBarrageFilEau();
      res.setValues(dao.OrdResBranches.OrdResBrancheBarrageFilEau.Ddes);
      metier.setOrdResBrancheBarrageFilEau(res);
    }
    if (dao.OrdResBranches.OrdResBrancheBarrageGenerique != null) {
      final OrdResBrancheBarrageGenerique res = new OrdResBrancheBarrageGenerique();
      res.setValues(dao.OrdResBranches.OrdResBrancheBarrageGenerique.Ddes);
      metier.setOrdResBrancheBarrageGenerique(res);
    }
    if (dao.OrdResBranches.OrdResBrancheNiveauxAssocies != null) {
      final OrdResBrancheNiveauxAssocies res = new OrdResBrancheNiveauxAssocies();
      res.setValues(dao.OrdResBranches.OrdResBrancheNiveauxAssocies.Ddes);
      metier.setOrdResBrancheNiveauxAssocies(res);
    }
    if (dao.OrdResBranches.OrdResBrancheOrifice != null) {
      final OrdResBrancheOrifice res = new OrdResBrancheOrifice();
      res.setValues(dao.OrdResBranches.OrdResBrancheOrifice.Ddes);
      metier.setOrdResBrancheOrifice(res);
    }
    if (dao.OrdResBranches.OrdResBranchePdc != null) {
      final OrdResBranchePdc res = new OrdResBranchePdc();
      res.setValues(dao.OrdResBranches.OrdResBranchePdc.Ddes);
      metier.setOrdResBranchePdc(res);
    }
    if (dao.OrdResBranches.OrdResBrancheSaintVenant != null) {
      final OrdResBrancheSaintVenant res = new OrdResBrancheSaintVenant();
      res.setValues(dao.OrdResBranches.OrdResBrancheSaintVenant.Ddes);
      metier.setOrdResBrancheSaintVenant(res);
    }
    if (dao.OrdResBranches.OrdResBrancheSeuilLateral != null) {
      final OrdResBrancheSeuilLateral res = new OrdResBrancheSeuilLateral();
      res.setValues(dao.OrdResBranches.OrdResBrancheSeuilLateral.Ddes);
      metier.setOrdResBrancheSeuilLateral(res);
    }
    if (dao.OrdResBranches.OrdResBrancheSeuilTransversal != null) {
      final OrdResBrancheSeuilTransversal res = new OrdResBrancheSeuilTransversal();
      res.setValues(dao.OrdResBranches.OrdResBrancheSeuilTransversal.Ddes);
      metier.setOrdResBrancheSeuilTransversal(res);
    }
    if (dao.OrdResBranches.OrdResBrancheStrickler != null) {
      final OrdResBrancheStrickler res = new OrdResBrancheStrickler();
      res.setValues(dao.OrdResBranches.OrdResBrancheStrickler.Ddes);
      metier.setOrdResBrancheStrickler(res);
    }
    if (dao.OrdResModeles != null && dao.OrdResModeles.OrdResModeleRegul != null && dao.OrdResModeles.OrdResModeleRegul.Ddes != null) {
      final OrdResModeleRegul res = new OrdResModeleRegul();
      List<Dde> dde = new ArrayList<>();
      dde.addAll(dao.OrdResModeles.OrdResModeleRegul.Ddes);
      res.setValues(dde);
      metier.setOrdResModeleRegul(res);
    }

    loadImplicitValues(metier);

    return metier;
  }

  /**
   * To old version for which no OrdResBrancheNiveauxAssocies is set.
   *
   * @param metier
   */
  private void loadImplicitValues(final OrdResScenario metier) {
    if (metier.getOrdResBrancheNiveauxAssocies() == null) {
      metier.setOrdResBrancheNiveauxAssocies(new OrdResBrancheNiveauxAssocies());
    }
    if (metier.getOrdResModeleRegul() == null) {
      metier.setOrdResModeleRegul(new OrdResModeleRegul());
    }

  }

  /**
   * Conversion de la couche métier vers la couche persistance
   */
  @Override
  public CrueDaoORES convertMetierToDao(final OrdResScenario metier, final CtuluLog ctuluLog) {

    final CrueDaoORES res = new CrueDaoORES();

    res.OrdResNoeuds = new CrueDaoStructureORES.DaoOrdResNoeuds();
    res.OrdResCasiers = new CrueDaoStructureORES.DaoOrdResCasiers();
    res.OrdResSections = new CrueDaoStructureORES.DaoOrdResSections();
    res.OrdResBranches = new CrueDaoStructureORES.DaoOrdResBranches();
    res.OrdResModeles = new CrueDaoStructureORES.DaOrdResModeles();
//
    final OrdResNoeudNiveauContinu noeudNivContinu = metier.getOrdResNoeudNiveauContinu();
    if (noeudNivContinu != null) {
      res.OrdResNoeuds.OrdResNoeudNiveauContinu = new CrueDaoStructureORES.DaoOrdResNoeudNiveauContinu();
      res.OrdResNoeuds.OrdResNoeudNiveauContinu.Ddes = new ArrayList<>(noeudNivContinu.getDdes());
    }
//
    final OrdResCasier casierProfil = metier.getOrdResCasier();
    res.OrdResCasiers.OrdResCasier = new CrueDaoStructureORES.DaoOrdResCasier();
    res.OrdResCasiers.OrdResCasier.Ddes = new ArrayList<>(casierProfil.getDdes());

    final OrdResSection section = metier.getOrdResSection();
    res.OrdResSections.OrdResSection = new CrueDaoStructureORES.DaoOrdResSection();
    res.OrdResSections.OrdResSection.Ddes = new ArrayList<>(section.getDdes());

    final OrdResBranchePdc branchePdc = metier.getOrdResBranchePdc();
    res.OrdResBranches.OrdResBranchePdc = new CrueDaoStructureORES.DaoOrdResBranchePdc();
    res.OrdResBranches.OrdResBranchePdc.Ddes = new ArrayList<>(branchePdc.getDdes());
    final OrdResBrancheSeuilTransversal brancheSeuilT = metier.getOrdResBrancheSeuilTransversal();
    res.OrdResBranches.OrdResBrancheSeuilTransversal = new CrueDaoStructureORES.DaoOrdResBrancheSeuilTransversal();
    res.OrdResBranches.OrdResBrancheSeuilTransversal.Ddes = new ArrayList<>(brancheSeuilT.getDdes());

    final OrdResBrancheSeuilLateral brancheSeuilL = metier.getOrdResBrancheSeuilLateral();
    res.OrdResBranches.OrdResBrancheSeuilLateral = new CrueDaoStructureORES.DaoOrdResBrancheSeuilLateral();
    res.OrdResBranches.OrdResBrancheSeuilLateral.Ddes = new ArrayList<>(brancheSeuilL.getDdes());

    final OrdResBrancheOrifice brancheOrifice = metier.getOrdResBrancheOrifice();
    res.OrdResBranches.OrdResBrancheOrifice = new CrueDaoStructureORES.DaoOrdResBrancheOrifice();
    res.OrdResBranches.OrdResBrancheOrifice.Ddes = new ArrayList<>(brancheOrifice.getDdes());

    final OrdResBrancheNiveauxAssocies ordResBrancheNiveauxAssocies = metier.getOrdResBrancheNiveauxAssocies();
    if (ordResBrancheNiveauxAssocies != null) {
      res.OrdResBranches.OrdResBrancheNiveauxAssocies = new CrueDaoStructureORES.DaoOrdResBrancheNiveauxAssocies();
      res.OrdResBranches.OrdResBrancheNiveauxAssocies.Ddes = new ArrayList<>(ordResBrancheNiveauxAssocies.getDdes());
    }

    final OrdResBrancheStrickler brancheStric = metier.getOrdResBrancheStrickler();
    res.OrdResBranches.OrdResBrancheStrickler = new CrueDaoStructureORES.DaoOrdResBrancheStrickler();
    res.OrdResBranches.OrdResBrancheStrickler.Ddes = new ArrayList<>(brancheStric.getDdes());

    final OrdResBrancheBarrageGenerique brancheBarrageG = metier.getOrdResBrancheBarrageGenerique();
    res.OrdResBranches.OrdResBrancheBarrageGenerique = new CrueDaoStructureORES.DaoOrdResBrancheBarrageGenerique();
    res.OrdResBranches.OrdResBrancheBarrageGenerique.Ddes = new ArrayList<>(brancheBarrageG.getDdes());

    final OrdResBrancheBarrageFilEau brancheBarrageFil = metier.getOrdResBrancheBarrageFilEau();
    res.OrdResBranches.OrdResBrancheBarrageFilEau = new CrueDaoStructureORES.DaoOrdResBrancheBarrageFilEau();
    res.OrdResBranches.OrdResBrancheBarrageFilEau.Ddes = new ArrayList<>(brancheBarrageFil.getDdes());

    final OrdResBrancheSaintVenant brancheStVenant = metier.getOrdResBrancheSaintVenant();
    res.OrdResBranches.OrdResBrancheSaintVenant = new CrueDaoStructureORES.DaoOrdResBrancheSaintVenant();
    res.OrdResBranches.OrdResBrancheSaintVenant.Ddes = new ArrayList<>(brancheStVenant.getDdes());

    OrdResModeleRegul resModeleRegul = metier.getOrdResModeleRegul();
    res.OrdResModeles.OrdResModeleRegul = new CrueDaoStructureORES.DaoOrdResModeleRegul();
    res.OrdResModeles.OrdResModeleRegul.Ddes = new ArrayList<>();
    res.OrdResModeles.OrdResModeleRegul.Ddes.addAll(resModeleRegul.getDdeMultiple());
    res.OrdResModeles.OrdResModeleRegul.Ddes.addAll(resModeleRegul.getDdesOnly());

    if (res.OrdResCasiers.OrdResCasier == null) {
      CrueHelper.emhEmpty("Casiers", ctuluLog);
    }
    if (res.OrdResNoeuds.OrdResNoeudNiveauContinu == null) {
      CrueHelper.emhEmpty("Noeuds", ctuluLog);
    }
    if (res.OrdResSections.OrdResSection == null) {
      CrueHelper.emhEmpty("Sections", ctuluLog);
    }
    return res;
  }
}
