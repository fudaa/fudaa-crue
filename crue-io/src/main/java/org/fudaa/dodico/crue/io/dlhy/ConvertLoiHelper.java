package org.fudaa.dodico.crue.io.dlhy;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.AbstractDaoLoi;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;
import org.fudaa.dodico.crue.metier.emh.LoiDF;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by deniger on 09/06/2017.
 */
public class ConvertLoiHelper {
    public static List<AbstractLoi> convertDaoToMetier(CtuluLog analyser, List<AbstractDaoLoi> inLois, Map<Class, ConvertLoi> corr) {
        return convertDaoToMetier(analyser, inLois, corr, false);
    }

    public static List<AbstractLoi> convertDaoToMetier(CtuluLog analyser, List<AbstractDaoLoi> inLois, Map<Class, ConvertLoi> corr, boolean checkDF) {
        List<AbstractLoi> outLoi = Collections.emptyList();
        if (CollectionUtils.isNotEmpty(inLois)) {
            outLoi = new ArrayList<>(inLois.size());
            for (final AbstractDaoLoi in : inLois) {
                final ConvertLoi cs = corr.get(in.getClass());
                AbstractLoi daoToMetier = cs.daoToMetier(in);
                LoiHelper.validLoiNom(analyser, daoToMetier);
                if(checkDF){
                    if(daoToMetier.getType().isDateAbscisse() && !(daoToMetier instanceof LoiDF)){
                        analyser.addSevereError("dateLoi.notInCorrectFormat",daoToMetier.getNom());
                    }
                }
                outLoi.add(daoToMetier);
            }
        }
        return outLoi;
    }
}
