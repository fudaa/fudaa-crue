/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import java.nio.ByteBuffer;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.emh.EMHBrancheStrickler;
import org.fudaa.dodico.crue.metier.result.ResCalBrancheSaintVenant;
import org.fudaa.dodico.crue.metier.result.ResCalBrancheStrickler;
import org.fudaa.dodico.crue.metier.result.ResCalCasier;
import org.fudaa.dodico.crue.metier.result.ResCalSection;
import org.fudaa.dodico.crue.metier.result.ResCalcul;

/**
 * @author deniger
 * @creation 27 mai 2009
 * @version
 */
public class FCBValueObject {

  public static abstract class AbstractRes {
    protected abstract void read(ByteBuffer buf);

    protected abstract ResCalcul createRes(EMH emhDest);
  }

  /**
   * Une structure de resultat nommee.
   * 
   * @author deniger
   * @creation 27 mai 2009
   * @version
   */
  public static abstract class AbstractEntete {// NOPMD
    protected String nom;

    protected void setNom(final String nom) {
      this.nom = nom;
    }

    /**
     * @return le nom du resultat
     */
    public String getNom() {
      return nom;
    }

  }

  /**
   * Entete d'un profil.
   * 
   * @author Adrien Hadoux
   */
  public static class EnteteProfil extends AbstractEntete {

    private double distance;
    private double zf;
    private double zfSsFente;

    /**
     * @param distance the distance to set
     */
    protected void setDistance(final double distance) {
      this.distance = distance;
    }

    /**
     * @param zf the zfonds to set
     */
    protected void setZf(final double zf) {
      this.zf = zf;
    }

    /**
     * @param zfSsFente the ZfSsFente to set
     */
    protected void setZfSsFente(final double zfSsFente) {
      this.zfSsFente = zfSsFente;
    }

    /**
     * @return the distance
     */
    public double getDistance() {
      return distance;
    }

    /**
     * @return the zfonds
     */
    public double getZf() {
      return zf;
    }

    /**
     * @return the ZfSsFente
     */
    public double getZfSsFente() {
      return zfSsFente;
    }

    @Override
    public String toString() {
      return nom + " dist=" + distance + ", Zf=" + zf + ", ZfSsFente=" + zfSsFente;
    }

  }

  /**
   * Resultat par branche
   * 
   * @author deniger
   * @creation 27 mai 2009
   * @version
   */
  public static class ResBranche extends AbstractRes {

    /**
     * pour le PDT donné, voloume de la branche r4
     */
    private float volbra;
    /**
     * surface du lit mineur de la branche ib r4
     */
    private float su1bra;
    /**
     * surface du lit majeur de la branche ib
     */
    private float su2bra;
    /**
     * surface du champ d'inondation de la branche ib
     */
    private float su4bra;

    @Override
    protected void read(final ByteBuffer bf) {
      volbra = bf.getFloat();
      su1bra = bf.getFloat();
      su2bra = bf.getFloat();
      su4bra = bf.getFloat();

    }

    @Override
    public ResCalcul createRes(final EMH emhDest) {
      if(emhDest.getClass().equals(EMHBrancheStrickler.class)){
        final ResCalBrancheStrickler res = new ResCalBrancheStrickler();
        res.setSplan(su1bra);
        res.setVol(volbra * 1E6);
        return res;
      }
      if(emhDest.getClass().equals(EMHBrancheSaintVenant.class)){
      final ResCalBrancheSaintVenant res = new ResCalBrancheSaintVenant();
      res.setSplanAct(su1bra);
      res.setSplanTot(su2bra);
      res.setSplanSto(su4bra);
      res.setVol(volbra * 1E6);
      return res;
      }
      return null;

    }

    /**
     * @return surface du lit mineur de la branche ib r4
     */
    public float getSu1bra() {
      return su1bra;
    }

    /**
     * @return surface du lit majeur de la branche ib
     */
    public float getSu2bra() {
      return su2bra;
    }

    /**
     * @return surface du champ d'inondation de la branche ib
     */
    public float getSu4bra() {
      return su4bra;
    }

    /**
     * @return pour le PDT donné, voloume de la branche r4
     */
    public float getVolbra() {
      return volbra;
    }

  }

  /**
   * Donne les resultat pour le casier attache.
   * 
   * @author deniger
   * @creation 27 mai 2009
   * @version
   */
  public static class ResCasier extends AbstractRes {

    /**
     * débit résultant du casier ip
     */
    private float qcas;

    /**
     * surface du casier ip
     */
    private float scas;

    /**
     * volume du casier ip
     */
    private float vcas;

    @Override
    protected void read(final ByteBuffer buf) {
      qcas = buf.getFloat();
      scas = buf.getFloat();
      vcas = buf.getFloat();
    }

    @Override
    protected ResCalcul createRes(final EMH emhDest) {
      final ResCalCasier res = new ResCalCasier();
      res.setSplan(scas);
      res.setQech(qcas);
      res.setVol(vcas);
      return res;
    }

    /**
     * @return débit résultant du casier ip
     */
    public float getQcas() {
      return qcas;
    }

    /**
     * @return surface du casier ip
     */
    public float getScas() {
      return scas;
    }

    /**
     * @return volume du casier ip
     */
    public float getVcas() {
      return vcas;
    }

  }

  /**
   * Resultat d'un profil
   * 
   * @author deniger
   * @creation 27 mai 2009
   * @version
   */
  public static class ResProfil extends AbstractRes {
    /**
     * pour le PDT donné, niveau à chaque profil
     */
    private float ccz;
    /**
     * pour le PDT donné, débit à chaque profil
     */
    private float ccq;

    /**
     * pour le PDT donné, surface totale à chaque profil
     */
    private float stot;

    /**
     * pour le PDT donné, vitesse à chaque profil
     */
    private float vit;
    /**
     * pour le PDT donné, vitesse critique à chaque profil
     */
    private float vcrit;

    @Override
    protected void read(final ByteBuffer bf) {
      ccz = bf.getFloat();
      ccq = bf.getFloat();
      stot = bf.getFloat();
      vit = bf.getFloat();
      vcrit = bf.getFloat();
    }

    @Override
    protected ResCalcul createRes(final EMH emhDest) {
      final ResCalSection res=new ResCalSection();
      res.setStot(stot);
      res.setVact(vit);
      res.setVc(vcrit);
      res.setZ(ccz);
      res.setQ(ccq);
      return res;
    }

    /**
     * @return pour le PDT donné, débit à chaque profil
     */
    public float getCcq() {
      return ccq;
    }

    /**
     * @return pour le PDT donné, niveau à chaque profil
     */
    public float getCcz() {
      return ccz;
    }

    /**
     * @return pour le PDT donné, surface totale à chaque profil
     */
    public float getStot() {
      return stot;
    }

    /**
     * @return pour le PDT donné, vitesse critique à chaque profil
     */
    public double getVcrit() {
      return vcrit;
    }

    /**
     * @return pour le PDT donné, vitesse à chaque profil
     */
    public float getVit() {
      return vit;
    }
  }

  /**
   * Structure qui correspond a un enregistrement d'une branche.
   * 
   * @author Adrien Hadoux
   */
  public static class EnteteBranche extends AbstractEntete {
    private int profilAmont;
    private int profilAval;
    private int[] tableauIndiceProfils;

    private int typeBranche;

    /**
     * @param profilAmont the profilAmont to set
     */
    protected void setProfilAmont(final int profilAmont) {
      this.profilAmont = profilAmont;
    }

    /**
     * @param profilAval the profilAval to set
     */
    protected void setProfilAval(final int profilAval) {
      this.profilAval = profilAval;
    }

    /**
     * @param tableauIndiceProfils the tableauIndiceProfils to set
     */
    protected void setTableauIndiceProfils(final int[] tableauIndiceProfils) {// NOPMD usage local ok
      this.tableauIndiceProfils = tableauIndiceProfils;
    }

    /**
     * @param typeBranche the typeBranche to set
     */
    protected void setTypeBranche(final int typeBranche) {
      this.typeBranche = typeBranche;
    }

    /**
     * @param idxLocal l'indice du profil dans la branche
     * @return l'indice global ( commence a 0!)
     */
    public int getIndiceProfil(final int idxLocal) {
      return tableauIndiceProfils[idxLocal];
    }

    /**
     * @return le nombre de profils dans cette branche
     */
    public int getNbProfils() {
      return tableauIndiceProfils == null ? 0 : tableauIndiceProfils.length;
    }

    /**
     * @return the profilAmont
     */
    public int getProfilAmont() {
      return profilAmont;
    }

    /**
     * @return the profilAval
     */
    public int getProfilAval() {
      return profilAval;
    }

    /**
     * @return the typeBranche
     */
    public int getTypeBranche() {
      return typeBranche;
    }
  }

  /**
   * Structure qui correspond a un enregistrement d'une noeud.
   * 
   * @author Adrien Hadoux
   */
  public static class EnteteCasier extends AbstractEntete {

    private int numProfil;

    /**
     * @param numPro the numPro to set
     */
    protected void setNumProfil(final int numPro) {
      this.numProfil = numPro;
    }

    /**
     * @return the numPro
     */
    public int getNumProfil() {
      return numProfil;
    }

  }

}
