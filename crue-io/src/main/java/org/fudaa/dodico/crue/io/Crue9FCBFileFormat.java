/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.io.neuf.AbstractCrueBinaryReader;
import org.fudaa.dodico.crue.io.neuf.FCBReader;
import org.fudaa.dodico.crue.io.neuf.FCBSequentialReader;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * Format FCB
 * 
 * @author Adrien Hadoux
 */
public class Crue9FCBFileFormat extends AbstractCrueBinaryFileFormat<FCBSequentialReader> {

  /**
   * Constructeur qui précise l'extension autorisée pour ce type de fichier
   */
  public Crue9FCBFileFormat() {
    super(CrueFileType.FCB);
  }

  @Override
  protected AbstractCrueBinaryReader<FCBSequentialReader> createReader() {
    return new FCBReader();
  }

}
