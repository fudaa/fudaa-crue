/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rcal;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.rdao.StructureResultatsDaoXstream;
import org.fudaa.dodico.crue.metier.CrueData;

public class CrueConverterRCAL implements CrueDataConverter<CrueDaoRCAL, CrueDaoRCAL> {

  @Override
  public CrueDaoRCAL convertDaoToMetier(final CrueDaoRCAL dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
    StructureResultatsDaoXstream.processVariablesAndTypes(dao);
    return dao;

  }

  @Override
  public CrueDaoRCAL getConverterData(final CrueData in) {
    return null;
  }

  @Override
  public CrueDaoRCAL convertMetierToDao(final CrueDaoRCAL metier, final CtuluLog ctuluLog) {
    return null;
  }
}
