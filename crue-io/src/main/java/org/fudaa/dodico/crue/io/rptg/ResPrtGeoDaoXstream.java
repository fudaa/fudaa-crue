/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rptg;

import com.thoughtworks.xstream.XStream;

/**
 *
 * @author deniger
 */
public class ResPrtGeoDaoXstream {
 public static void configureXstream(XStream xstream) {
      xstream.alias("ResPrtGeo", ResPrtGeoDao.class);
      xstream.useAttributeFor(ResPrtGeoDao.class, "Href");
      xstream.useAttributeFor(ResPrtGeoDao.class, "OffsetMot");
    }
}
