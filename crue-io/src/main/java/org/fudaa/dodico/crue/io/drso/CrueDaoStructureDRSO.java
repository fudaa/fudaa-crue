/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.drso;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.metier.CrueFileType;

import java.util.List;

/**
 * Toutes les structures dao utilisees et les inits du parser xml pour le format DRSO. Toutes ces structures sont necessaires pour bien formatter le
 * contenu xml.
 *
 * @author Adrien Hadoux
 */
public class CrueDaoStructureDRSO implements CrueDataDaoStructure {

    final boolean ignoreComment;

    public CrueDaoStructureDRSO(final boolean ignoreComment) {
        this.ignoreComment = ignoreComment;
    }

    @Override
    public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
        // -- creation des alias pour que ce soit + parlant dans le xml file --//
        xstream.alias(CrueFileType.DRSO.toString(), CrueDaoDRSO.class);

        // -- liste des initialisations necessaires pour le formattage des donnees--//
        initXmlParserNode(xstream);
        initXmlParserBranche(xstream);
        initXmlParserCasier(xstream);
        initXmlParserSection(xstream);
    }

    /**
     * init parser pour les nodes et pouvoir mettre l'attribut Nom en debut de balise.
     *
     * @param xstream
     */
    private void initXmlParserNode(final XStream xstream) {
        // -- alias gestion pour les noeuds --//
        xstream.alias("NoeudNiveauContinu", NoeudNiveauContinu.class);
        // -- mettre attribut Nom comme attribut de la balise Noeud et son converter approprie --//
        // -- creation des alias pour que ce soit + parlant dans le xml file --//
        xstream.useAttributeFor(NoeudNiveauContinu.class, "Nom");
        if (ignoreComment) {
            xstream.omitField(NoeudNiveauContinu.class, "Commentaire");
        }
    }

    /**
     * Init le parser avec les infos des branches.
     */
    private void initXmlParserBranche(final XStream xstream) {
        // -- gestion des branches --//
        xstream.alias("BrancheSaintVenant", BrancheSaintVenant.class);
        xstream.alias("BrancheSeuilLateral", BrancheSeuilLateral.class);
        xstream.alias("BrancheStrickler", BrancheStrickler.class);
        xstream.alias("BrancheOrifice", BrancheOrifice.class);
        xstream.alias("BrancheSeuilTransversal", BrancheSeuilTransversal.class);
        xstream.alias("BrancheBarrageFilEau", BrancheBarrageFilEau.class);
        xstream.alias("BranchePdc", BranchePdc.class);
        xstream.alias("BrancheBarrageGenerique", BrancheBarrageGenerique.class);
        xstream.alias("BrancheNiveauxAssocies", BrancheNiveauxAssocies.class);

        // -- mettre attribut Nom comme attribut de la balise branche,noeud et section et son converter approprie --//

        // -- mettre le nom de la branche dans la balise --//
        xstream.useAttributeFor(BrancheAbstract.class, "Nom");
        xstream.aliasField("BrancheSaintVenant-Sections", BrancheSaintVenant.class, "SectionsBrancheSaintVenant");
        xstream.aliasField("Branche-Sections", BrancheDefault.class, "Sections");

        // -- gestion des noeuds des branches --//
        xstream.alias("NdAm", NdAm.class);
        xstream.alias("NdAv", NdAv.class);

        xstream.useAttributeFor(NdAm.class, "NomRef");
        xstream.useAttributeFor(NdAv.class, "NomRef");
        if (ignoreComment) {
            xstream.omitField(BrancheAbstract.class, "Commentaire");
        }

    }

    /**
     * Init le parser avec les infos des casiers.
     */
    private void initXmlParserCasier(final XStream xstream) {
        xstream.alias("CasierProfil", CasierProfil.class);
        xstream.addImplicitCollection(CasierProfil.class, "ProfilCasiers");
        xstream.alias("ProfilCasier", ReferenceCasierProfil.class);

        xstream.alias("CasierMNT", CasierMNT.class);
        xstream.alias("Bati", ReferenceBati.class);
        xstream.alias("Noeud", ReferenceNoeud.class);

        xstream.useAttributeFor(CasierAbstract.class, "Nom");
        xstream.useAttributeFor(AbstractReferenceProfil.class, "NomRef");

        xstream.useAttributeFor(ReferenceBati.class, "NomRef");

        // -- implicite liste pour les references profils--//

        // -- alias et converter du noeud reference --//
        xstream.useAttributeFor(ReferenceNoeud.class, "NomRef");
        if (ignoreComment) {
            xstream.omitField(CasierAbstract.class, "Commentaire");
        }

    }

    /**
     * Init le parser avec les infos des sections.
     */
    private void initXmlParserSection(final XStream xstream) {
        // -- gestion des sections --//
        xstream.alias("SectionIdem", SectionRefIdem.class);
        xstream.alias("SectionSansGeometrie", SectionRefSansGeometrie.class);

        xstream.alias("SectionInterpolee", SectionRefInterpolee.class);
        xstream.alias("SectionPilote", SectionRefPilote.class);
        xstream.alias("Section", SectionBranche.class);

        xstream.alias("SectionProfil", SectionRefProfil.class);
        xstream.addImplicitCollection(SectionRefProfil.class, "ProfilSections");
        xstream.alias("ProfilSection", ReferenceSectionProfil.class);


        xstream.alias("BrancheSaintVenant-Section", SectionBrancheSaintVenant.class);
        xstream.alias("Branche-Section", SectionBranche.class);

        // -- cas particlier pour les sections --//
        xstream.alias("Section", SectionReferenceeParIdem.class);
        xstream.useAttributeFor(SectionReferenceeParIdem.class, "NomRef");

        // -- implicite liste pour les references profils--//

        xstream.useAttributeFor(SectionAbstract.class, "Nom");
        xstream.useAttributeFor(SectionBranche.class, "NomRef");

        if (ignoreComment) {
            xstream.omitField(SectionAbstract.class, "Commentaire");
        }

    }

    /**
     * Element possible qui soit persiste dans la liste de noeuds
     */
    protected static class NoeudNiveauContinu {

        protected String Nom;
        protected String Commentaire;
    }

    // *************************GESTION DES BRANCHES *************************

    /**
     * Element branche possible
     *
     * @author Adrien Hadoux
     */
    protected abstract static class BrancheAbstract {

        protected String Nom;
        protected String Commentaire;
        protected boolean IsActive;
        protected NdAm NdAm;
        protected NdAv NdAv;
        protected ReferenceCasierProfil SectionPilote;

        public abstract void setSections(List<? extends SectionBranche> sections);
    }

    protected static class BrancheSaintVenant extends BrancheAbstract {

        List<SectionBrancheSaintVenant> SectionsBrancheSaintVenant;

        @Override
        public void setSections(final List<? extends SectionBranche> sections) {
            SectionsBrancheSaintVenant = (List<SectionBrancheSaintVenant>) sections;

        }
    }

    protected static class BrancheDefault extends BrancheAbstract {

        List<SectionBranche> Sections;

        @Override
        public void setSections(final List<? extends SectionBranche> sections) {
            Sections = (List<SectionBranche>) sections;

        }
    }

    protected static class BrancheSeuilLateral extends BrancheDefault {
    }

    protected static class BrancheStrickler extends BrancheDefault {
    }

    protected static class BrancheOrifice extends BrancheDefault {
    }

    protected static class BrancheSeuilTransversal extends BrancheDefault {
    }

    protected static class BrancheBarrageFilEau extends BrancheDefault {
    }

    protected static class BranchePdc extends BrancheDefault {
    }

    protected static class BrancheBarrageGenerique extends BrancheDefault {
    }

    protected static class BrancheBarrageRhone extends BrancheDefault {
    }

    protected static class BrancheEnchainement extends BrancheDefault {
    }

    protected static class BrancheNiveauxAssocies extends BrancheDefault {
    }

    protected static class ReferenceNoeud {

        protected String NomRef;

        protected ReferenceNoeud(final String nomRef) {
            NomRef = nomRef;
        }

        protected ReferenceNoeud() {
        }
    }

    protected static class NdAm {

        protected String NomRef;

        protected NdAm(final String nomRef) {
            NomRef = nomRef;
        }

        protected NdAm() {
        }
    }

    protected static class NdAv {

        protected String NomRef;

        protected NdAv(final String nomRef) {
            NomRef = nomRef;
        }

        protected NdAv() {
        }
    }

    // -- gestion des sections --//

    /**
     * Elementsection qui se trouve dans branche, dispose d'une reference positionnee dans la balise de debut.
     *
     * @author Adrien Hadoux Pos: position.
     */
    protected static class SectionBranche {

        protected String NomRef;
        protected String Pos;
        protected double Xp;
    }

    protected static class SectionBrancheSaintVenant extends SectionBranche {

        protected Double CoefPond;
        protected Double CoefConv;
        protected Double CoefDiv;
    }

    // *************************GESTION DES CASIERS *************************
    protected static class CasierAbstract {

        protected String Nom;
        protected String Commentaire;
        protected boolean IsActive;
        protected ReferenceNoeud Noeud;
    }

    protected abstract static class AbstractReferenceProfil {

        protected String NomRef;
    }

    protected static class ReferenceSectionProfil extends AbstractReferenceProfil {
    }

    /**
     * la classe profil de casier inclus dans le CasierProfil.
     *
     * @author Adrien Hadoux
     */
    protected static class ReferenceCasierProfil extends AbstractReferenceProfil {
    }

    protected static class ReferenceBati {

        protected String NomRef;
    }

    protected static class CasierProfil extends CasierAbstract {

        protected List<ReferenceCasierProfil> ProfilCasiers;
        protected ReferenceBati BatiCasier;
    }

    protected static class CasierMNT extends CasierAbstract {
    }

    // *************************GESTION DES SECTIONS REFERENCES*************************
    protected static class ReferenceSection {

        protected String Nom;
    }

    public static class SectionAbstract {

        protected String Nom;
        protected String Commentaire;

        public String getNom() {
            return Nom;
        }
    }

    protected static class SectionReferenceeParIdem {

        protected String NomRef;
    }

    protected static class SectionRefIdem extends SectionAbstract {

        SectionReferenceeParIdem Section;
    }

    protected static class SectionRefInterpolee extends SectionAbstract {
    }

    protected static class SectionRefProfil extends SectionAbstract {

        protected List<ReferenceSectionProfil> ProfilSections;
    }

    protected static class SectionRefSansGeometrie extends SectionAbstract {
    }

    protected static class SectionRefPilote extends SectionAbstract {
    }
}
