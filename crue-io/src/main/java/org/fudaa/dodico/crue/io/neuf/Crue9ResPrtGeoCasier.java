/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import org.fudaa.dodico.crue.metier.emh.LoiFF;

public class Crue9ResPrtGeoCasier extends Crue9ResPrtGeo {

  public void setLoiZVol(final LoiFF newZVol) {
    super.setValue("loiZVol", newZVol);
  }

  public void setLoiZSplan(final LoiFF newZSplan) {
    super.setValue("loiZSplan", newZSplan);
  }
  public void setZf(final double newZf) {
    super.setValue("zf", newZf);
  }

  public void setZhaut(final double newZHaut) {
    super.setValue("zhaut", newZHaut);
  }
}
