/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores.old;

public class DaoOrdResBrancheSeuilTransversalOld  {
  // WARN: l'ordre des champs est important car utilise par l'ecriture/lecture de ORES

  private boolean ddeRegimeSeuil;

  public final boolean getDdeRegimeSeuil() {
    return ddeRegimeSeuil;
  }

 

  /**
   * @param newDdeRegime
   */
  public final void setDdeRegimeSeuil(final boolean newDdeRegime) {
    ddeRegimeSeuil = newDdeRegime;
  }

  @Override
  public String toString() {
    return "OrdResBrancheSeuilTransversal [ddeRegime=" + ddeRegimeSeuil + "]";
  }
}
