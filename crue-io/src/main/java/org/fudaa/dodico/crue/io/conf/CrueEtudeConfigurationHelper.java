/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.conf;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueDaoStructure;
import org.fudaa.dodico.crue.common.io.CrueIOResu;

/**
 *
 * @author deniger
 */
public class CrueEtudeConfigurationHelper {

  public static CrueIOResu<CrueEtudeDaoConfiguration> read(final File f, final CrueDaoStructure courbeConfigurer) {
    final CrueIOResu<CrueEtudeDaoConfiguration> res = new CrueIOResu<>();
    res.setAnalyse(new CtuluLog(BusinessMessages.RESOURCE_BUNDLE));
    final CrueEtudeConfigurationReaderWriter readerWriter = new CrueEtudeConfigurationReaderWriter("1.0", courbeConfigurer);
    final CrueIOResu<CrueEtudeDaoConfiguration> readXML = readerWriter.readXML(f, res.getAnalyse());
    if (readXML.getMetier() != null) {
      readXML.getMetier().updateXmlns();
    }
    return readXML;
  }

  public static Map<String, String> getPlanimetryProperties(final CrueEtudeDaoConfiguration in) {
    final List<Option> options = (in==null||in.getPlanimetrieConfiguration()==null)?null:in.getPlanimetrieConfiguration().getOptions();
    return getProperties(options);
  }

  public static CrueIOResu<CrueEtudeDaoConfiguration> read(final String f, final CrueDaoStructure courbeConfigurer) {
    final CrueIOResu<CrueEtudeDaoConfiguration> res = new CrueIOResu<>();
    res.setAnalyse(new CtuluLog(BusinessMessages.RESOURCE_BUNDLE));
    final CrueEtudeConfigurationReaderWriter readerWriter = new CrueEtudeConfigurationReaderWriter("1.0", courbeConfigurer);
    final CrueIOResu<CrueEtudeDaoConfiguration> readXML = readerWriter.readXML(f, res.getAnalyse());
    readXML.getMetier().updateXmlns();
    return readXML;
  }

  public static CrueIOResu<CrueEtudeExternDaoConfiguration> readExtern(final String f) {
    final CrueIOResu<CrueEtudeExternDaoConfiguration> res = new CrueIOResu<>();
    res.setAnalyse(new CtuluLog(BusinessMessages.RESOURCE_BUNDLE));
    final CrueEtudeExternConfigurationReaderWriter readerWriter = new CrueEtudeExternConfigurationReaderWriter("1.0");
    final CrueIOResu<CrueEtudeExternDaoConfiguration> readXML = readerWriter.readXML(f, res.getAnalyse());
    readXML.getMetier().updateXmlns();
    return readXML;
  }

  public static CrueIOResu<CrueEtudeExternDaoConfiguration> readExtern(final File f) {
    final CrueIOResu<CrueEtudeExternDaoConfiguration> res = new CrueIOResu<>();
    res.setAnalyse(new CtuluLog(BusinessMessages.RESOURCE_BUNDLE));
    final CrueEtudeExternConfigurationReaderWriter readerWriter = new CrueEtudeExternConfigurationReaderWriter("1.0");
    final CrueIOResu<CrueEtudeExternDaoConfiguration> readXML = readerWriter.readXML(f, res.getAnalyse());
    readXML.getMetier().updateXmlns();
    return readXML;
  }

  public static Map<String, String> getProperties(final List<Option> options) {
    final Map<String, String> res = new LinkedHashMap<>();
    if (options == null) {
      return res;
    }
    for (final Option option : options) {
      res.put(option.getNom(), option.getValeur());
    }
    return res;
  }

  public static class InputData {

    public Map<String, String> planimetryOptions;
    public Map loiOptions;
    public Map<String, String> globalOptions;
  }

  public static CrueIOResu<CrueEtudeDaoConfiguration> write(final File f, final InputData input, final CrueDaoStructure courbeConfigurer) {
    final CrueEtudeDaoConfiguration configEtude = new CrueEtudeDaoConfiguration();
    addDataInList(input.planimetryOptions, configEtude.getPlanimetrieConfiguration().getOptions());
    addDataInList(input.globalOptions, configEtude.getGlobalConfiguration().getOptions());
    configEtude.initLoiConfiguration(input.loiOptions);
    return write(f, configEtude, courbeConfigurer);
  }

  public static void addDataInList(final Map<String, String> in, final List<Option> out) {
    if (in != null) {
      for (final Map.Entry<String, String> entry : in.entrySet()) {
        out.add(new Option(entry.getKey(), entry.getValue()));
      }
    }
  }

  public static CrueIOResu<CrueEtudeDaoConfiguration> write(final File f, final CrueEtudeDaoConfiguration conf, final CrueDaoStructure courbeConfigurer) {
    final CrueIOResu<CrueEtudeDaoConfiguration> res = new CrueIOResu<>();
    res.setAnalyse(new CtuluLog(BusinessMessages.RESOURCE_BUNDLE));
    res.setMetier(conf);
    final CrueEtudeConfigurationReaderWriter readerWriter = new CrueEtudeConfigurationReaderWriter("1.0", courbeConfigurer);
    readerWriter.writeXMLMetier(res, f, res.getAnalyse());
    return res;
  }

  public static CrueIOResu<CrueEtudeExternDaoConfiguration> writeExtern(final File f, final CrueEtudeExternDaoConfiguration conf) {
    final CrueIOResu<CrueEtudeExternDaoConfiguration> res = new CrueIOResu<>();
    res.setAnalyse(new CtuluLog(BusinessMessages.RESOURCE_BUNDLE));
    res.setMetier(conf);
    final CrueEtudeExternConfigurationReaderWriter readerWriter = new CrueEtudeExternConfigurationReaderWriter("1.0");
    readerWriter.writeXMLMetier(res, f, res.getAnalyse());
    return res;
  }
}
