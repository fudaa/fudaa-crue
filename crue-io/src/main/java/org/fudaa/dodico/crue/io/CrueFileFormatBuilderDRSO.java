/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.drso.CrueConverterDRSO;
import org.fudaa.dodico.crue.io.drso.CrueDaoStructureDRSO;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;

public class CrueFileFormatBuilderDRSO implements CrueFileFormatBuilder<CrueData> {

  @Override
  public Crue10FileFormat<CrueData> getFileFormat(final CoeurConfigContrat coeurConfig) {
    return new Crue10FileFormat<>(new CrueDataXmlReaderWriterImpl<>(CrueFileType.DRSO,
            coeurConfig, new CrueConverterDRSO(), new CrueDaoStructureDRSO(Crue10VersionConfig.isV1_1_1(coeurConfig))));

  }
}
