/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.rptg.CrueConverterRPTG;
import org.fudaa.dodico.crue.io.rptg.CrueDaoRPTG;
import org.fudaa.dodico.crue.io.rptg.CrueDaoStructureRPTG;
import org.fudaa.dodico.crue.metier.CrueFileType;

public class CrueFileFormatBuilderRPTG implements CrueFileFormatBuilder<CrueDaoRPTG> {
  @Override
  public Crue10FileFormat<CrueDaoRPTG> getFileFormat(final CoeurConfigContrat version) {
    return new Crue10FileFormat<>(new CrueDataXmlReaderWriterImpl<>(CrueFileType.RPTG,
        version, new CrueConverterRPTG(), new CrueDaoStructureRPTG()));
  }

}
