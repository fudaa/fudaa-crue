/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.conf;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;

/**
 * Les configurations liées à une étude.
 *
 * @author deniger
 */
public class CrueEtudeDaoConfiguration extends AbstractCrueDao {

  public Map<String, ?> getCourbeConfiguration() {
    Map<String, ?> res = null;
    if (getLoiConfigurationOptions() != null) {
      res = getLoiConfigurationOptions().getCourbeConfiguration();
    }
    if (res == null) {
      res = Collections.emptyMap();
    }
    return res;
  }

  BlocConfiguration GlobalConfiguration = new BlocConfiguration();
  BlocConfiguration PlanimetrieConfiguration = new BlocConfiguration();
  CourbeConfigurations LoiConfigurations = new CourbeConfigurations();

  public BlocConfiguration getPlanimetrieConfiguration() {
    return PlanimetrieConfiguration;
  }

  public BlocConfiguration getGlobalConfiguration() {
    return GlobalConfiguration;
  }

  public List<Option> getGlobalConfigurationOptions() {
    return GlobalConfiguration == null ? null
            : GlobalConfiguration.getOptions();
  }

  public CourbeConfigurations getLoiConfigurationOptions() {
    return LoiConfigurations;
  }

  public void setGlobalConfiguration(final BlocConfiguration GlobalConfiguration) {
    this.GlobalConfiguration = GlobalConfiguration;
  }

  public void setLoiConfiguration(final CourbeConfigurations LoiConfiguration) {
    this.LoiConfigurations = LoiConfiguration;
  }

  public void initLoiConfiguration(final Map<String, Object> courbeConfiguration) {
    this.LoiConfigurations = new CourbeConfigurations();
    if (courbeConfiguration != null) {
      this.LoiConfigurations.CourbeConfiguration = new HashMap<>(courbeConfiguration);
    }
  }

  public void setPlanimetrieConfiguration(final BlocConfiguration planimetryConfiguration) {
    this.PlanimetrieConfiguration = planimetryConfiguration;
  }
}
