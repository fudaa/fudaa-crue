/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import org.fudaa.dodico.crue.io.rdao.CommonResDao.VariableResDao;

import java.util.Collections;
import java.util.List;

/**
 *
 * @author deniger
 */
public class NoeudsDao extends CommonResDao.NbrMotDao implements ResContainerEMHCat {

 

  public static class NoeudDao extends CommonResDao.ItemResDao {
  }

  public static class NoeudTypeDao extends CommonResDao.TypeEMHDao implements ResContainerEMHType{

    List<CommonResDao.VariableResDao> VariableRes;
    List<NoeudDao> Noeud;

    public List<NoeudDao> getNoeud() {
      return Noeud;
    }

    @Override
    public List<VariableResDao> getVariableRes() {
      return VariableRes;
    }
    
    @Override
    public List<? extends CommonResDao.ItemResDao> getItemRes() {
      return getNoeud();
    }

  }
  List<CommonResDao.VariableResDao> VariableRes;
  NoeudTypeDao NoeudNiveauContinu;

  public NoeudTypeDao getNoeudNiveauContinu() {
    return NoeudNiveauContinu;
  }

  @Override
  public List<VariableResDao> getVariableRes() {
    return VariableRes;
  }
  
  List<ResContainerEMHType> resContainerEMHType;

  @Override
  public List<ResContainerEMHType> getResContainerEMHType() {
    if (resContainerEMHType == null) {
      //attention, l'ordre est très important: doit suivre celui du fichier
      resContainerEMHType = Collections.unmodifiableList(Collections.<ResContainerEMHType>singletonList(NoeudNiveauContinu));
    }
    return resContainerEMHType;
  }
  
  @Override
  public String getDelimiteurNom() {
  return "CatEMHNoeud";
  }

}
