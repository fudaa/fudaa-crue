/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import java.util.List;

/**
 *
 * @author deniger
 */
public interface ResContainerEMHCat {

  List<CommonResDao.VariableResDao> getVariableRes();

  List<ResContainerEMHType> getResContainerEMHType();

  int getNbrMot();

  String getDelimiteurNom();
}
