/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.optr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.optr.CrueDaoStructureOPTR.MethodeOrdonnancement;
import org.fudaa.dodico.crue.io.optr.CrueDaoStructureOPTR.OrdreDRSO;
import org.fudaa.dodico.crue.io.optr.CrueDaoStructureOPTR.RegleOPTR;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.comparator.RegleTypeComparator;
import org.fudaa.dodico.crue.metier.emh.EnumMethodeOrdonnancement;
import org.fudaa.dodico.crue.metier.emh.EnumRegle;
import org.fudaa.dodico.crue.metier.emh.OrdPrtReseau;
import org.fudaa.dodico.crue.metier.emh.Regle;
import org.fudaa.dodico.crue.metier.emh.Sorties;

/**
 * Creation des objets methodes interpolations
 * 
 * @author deniger
 */
public class CrueConverterOPTR implements CrueDataConverter<CrueDaoOPTR, OrdPrtReseau> {

  private final static Logger LOGGER = Logger.getLogger(CrueConverterOPTR.class.getName());

  private BidiMap getCorrespondance() {
    final BidiMap daoToRegle = new DualHashBidiMap();
    daoToRegle.put(CrueDaoStructureOPTR.RegleSignalerObjetsInactifs.class,
        EnumRegle.ORD_PRT_RESEAU_SIGNALER_OBJETS_INACTIFS);
    daoToRegle.put(CrueDaoStructureOPTR.RegleCompatibiliteCLimM.class, EnumRegle.ORD_PRT_RESEAU_COMPATIBILITE_CLIMM);
    return daoToRegle;
  }

  @Override
  public OrdPrtReseau getConverterData(final CrueData in) {
    return in.getOPTR();
  }

  @Override
  public OrdPrtReseau convertDaoToMetier(final CrueDaoOPTR dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
    final OrdPrtReseau metier =dataLinked.getOrCreateOPTR();
    metier.setSorties(new Sorties(dao.Sorties));
    if (dao.MethodeOrdonnancement.OrdreDRSO != null) {
      metier.setMethodeOrdonnancement(EnumMethodeOrdonnancement.ORDRE_DRSO);
    }
    if (dao.Regles != null) {
      final Map classToEnum = getCorrespondance();
      for (final RegleOPTR regleDao : dao.Regles) {
        final Regle regle = metier.getRegle((EnumRegle) classToEnum.get(regleDao.getClass()));
        regle.setActive(regleDao.IsActive);
      }
    }
    if (dataLinked != null) {
      dataLinked.getModele().addInfosEMH(metier);
    }
    return metier;
  }

  @Override
  public CrueDaoOPTR convertMetierToDao(final OrdPrtReseau metier, final CtuluLog ctuluLog) {
    final CrueDaoOPTR dao = new CrueDaoOPTR();
    dao.Sorties = new Sorties(metier.getSorties());
    final EnumMethodeOrdonnancement methodeOrdonnancement = metier.getMethodeOrdonnancement();
    dao.MethodeOrdonnancement = new MethodeOrdonnancement();
    if (methodeOrdonnancement == EnumMethodeOrdonnancement.ORDRE_DRSO) {
      dao.MethodeOrdonnancement.OrdreDRSO = new OrdreDRSO();
    }
    dao.Regles = new ArrayList<>();
    final List<Regle> regles = new ArrayList<>(metier.getRegles());
    Collections.sort(regles, RegleTypeComparator.INSTANCE);
    final BidiMap classToEnum = getCorrespondance();
    for (final Regle regleMetier : regles) {
      final Class daoClass = (Class) classToEnum.getKey(regleMetier.getType());
      RegleOPTR reglePersist = null;
      try {
        reglePersist = (RegleOPTR) daoClass.newInstance();
      } catch (final Exception e) {
        LOGGER.log(Level.SEVERE,"convertMetierToDao", e);
        return dao;
      }
      reglePersist.IsActive = regleMetier.isActive();
      dao.Regles.add(reglePersist);
    }
    return dao;
  }
}
