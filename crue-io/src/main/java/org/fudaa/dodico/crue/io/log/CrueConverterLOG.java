/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.log;

import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.CtuluDefaultLogFormatter;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.io.CrueConverter;
import org.fudaa.dodico.crue.io.log.CrueDaoStructureLOG.Log;

/**
 * @author CANEL Christophe
 *
 */
public class CrueConverterLOG implements CrueConverter<CrueDaoLOG, CtuluLog> {

  /**
   * {@inheritDoc}
   */
  @Override
  public CtuluLog convertDaoToMetier(final CrueDaoLOG dao, final CtuluLog ctuluLog) {
    final CtuluLog metier = new CtuluLog();

    metier.setDesc(dao.Description);

    for (final Log log : dao.Logs) {
      final CtuluLogLevel crueEnum = convertDaoValueToEnum(log.Level);
      metier.addRecord(crueEnum, log.Message);
    }

    return metier;
  }

  public static CtuluLogLevel convertDaoValueToEnum(final String logLevel) {
    String initlevel = logLevel;
    if ("FATAL".equals(initlevel)) {
      initlevel = CtuluLogLevel.SEVERE.toString();
    }
    CtuluLogLevel crueEnum = CtuluLogLevel.SEVERE;
    try {
      crueEnum = CtuluLogLevel.valueOf(initlevel);
    } catch (final Exception e) {
      Logger.getLogger(CrueConverterLOG.class.getName()).log(Level.INFO, "level {0} unknown", initlevel);
    }
    return crueEnum;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CrueDaoLOG convertMetierToDao(final CtuluLog metier, final CtuluLog ctuluLog) {
    final CrueDaoLOG dao = new CrueDaoLOG();
    final CtuluDefaultLogFormatter formatter = new CtuluDefaultLogFormatter(false);
    final ResourceBundle resourceBundle = metier.getDefaultResourceBundle();

    dao.Description = metier.getDesc();

    for (final CtuluLogRecord record : metier.getRecords()) {
      final Log log = new Log();

      log.Level = record.getLevel().name();
      log.Message = formatter.format(record, resourceBundle);

      dao.Logs.add(log);
    }

    return dao;
  }
}
