/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rptg;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.rdao.StructureResultatsDaoXstream;
import org.fudaa.dodico.crue.metier.CrueData;

public class CrueConverterRPTG implements CrueDataConverter<CrueDaoRPTG, CrueDaoRPTG> {

  @Override
  public CrueDaoRPTG convertDaoToMetier(final CrueDaoRPTG dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
    StructureResultatsDaoXstream.processVariablesAndTypes(dao);
    return dao;
  }

  @Override
  public CrueDaoRPTG getConverterData(final CrueData in) {
    return null;
  }

  @Override
  public CrueDaoRPTG convertMetierToDao(final CrueDaoRPTG metier, final CtuluLog ctuluLog) {
    return null;
  }
}
