/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.res;

import gnu.trove.TObjectIntHashMap;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.ItemResDao;

import java.util.*;

/**
 * Donne pour chaque Type d'EMH les informations nécessaires.
 *
 * @author deniger
 */
public class ResTypeEMHContent {
  /**
   * la position en bytes: nbrMot * 8.
   */
  private long positionInBytes;
  /**
   * le contenu des variables pour cet type. Mergé avec les parents.
   */
  private ResVariablesContent resVariablesContent;
  private final List<ItemResDao> itemResDaoIntern = new ArrayList<>();
  private final List<ItemResDao> itemResDao = Collections.unmodifiableList(itemResDaoIntern);
  private final TObjectIntHashMap<String> emhPositionBytes = new TObjectIntHashMap<>();
  private final String emhType;

  public ResTypeEMHContent(String emhType) {
    this.emhType = emhType;
  }

  public String getEmhType() {
    return emhType;
  }

  public List<ItemResDao> getItemResDao() {
    return itemResDao;
  }

  public void addItemResDao(ItemResDao itemResDao, int absolutePositionInBytes) {
    this.itemResDaoIntern.add(itemResDao);
    emhPositionBytes.put(itemResDao.getNomRef(), absolutePositionInBytes);
  }

  public int getItemPositionInBytes(String ref) {
    if (emhPositionBytes.contains(ref)) {
      return emhPositionBytes.get(ref);
    }
    return -1;
  }

  public ResVariablesContent getResVariablesContent() {
    return resVariablesContent;
  }


  void getAllRef(Collection<String> collector) {
    for (ItemResDao item : itemResDaoIntern) {
      collector.add(item.getNomRef());
    }
  }

  long getPositionInBytes() {
    return positionInBytes;
  }

  void setPositionInBytes(long positionInBytes) {
    this.positionInBytes = positionInBytes;
  }

  void setResVariablesContent(ResVariablesContent resVariablesContent) {
    this.resVariablesContent = resVariablesContent;
  }
}
