/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rcal;

import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.rcal.CrueDaoStructureRCAL.ResCalcTransDao;
import org.fudaa.dodico.crue.io.rcal.CrueDaoStructureRCAL.ResultatsCalculPermanentDao;
import org.fudaa.dodico.crue.io.rdao.ContexteSimulationDao;
import org.fudaa.dodico.crue.io.rdao.ParametrageDao;
import org.fudaa.dodico.crue.io.rdao.ResContainerDao;
import org.fudaa.dodico.crue.io.rdao.StructureResultatsDao;

/**
 *
 * @author Frédéric Deniger
 */
public class CrueDaoRCAL extends AbstractCrueDao implements ResContainerDao {

  ParametrageDao Parametrage;
  ContexteSimulationDao ContexteSimulation;
  StructureResultatsDao StructureResultat;
  List<ResultatsCalculPermanentDao> ResCalcPerms;
  List<ResCalcTransDao> ResCalcTranss;

  @Override
  public ParametrageDao getParametrage() {
    return Parametrage;
  }

  @Override
  public ContexteSimulationDao getContexteSimulation() {
    return ContexteSimulation;
  }

  public List<ResultatsCalculPermanentDao> getResCalcPerms() {
    return ResCalcPerms;
  }

  public List<ResCalcTransDao> getResCalcTranss() {
    return ResCalcTranss;
  }

  @Override
  public StructureResultatsDao getStructureResultat() {
    return StructureResultat;
  }
}
