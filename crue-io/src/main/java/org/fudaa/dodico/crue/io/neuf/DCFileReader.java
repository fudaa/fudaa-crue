/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import com.thoughtworks.xstream.converters.SingleValueConverter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.comparaison.CrueComparatorHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.config.ccm.SeveriteManager;
import org.fudaa.dodico.crue.config.lit.LitNomme;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.io.common.CrueHelper;
import org.fudaa.dodico.crue.io.common.EnumsConverter;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.EMHFactory;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;
import org.fudaa.dodico.crue.metier.factory.InfoEMHFactory;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;

import java.io.EOFException;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Reader pour le format DC crue 9.
 *
 * @author Adrien Hadoux
 */
public class DCFileReader extends AbstractCrue9Reader {
    /**
     * Contient les noms des sections réellement trouvées dans la partie profiles
     */
    final Set<String> createdNomSection = new HashSet<>(100);
    /**
     * Contient les noms de sections utilisées par les branches.
     */
    final Set<String> usedNomSection = new HashSet<>(100);
    private final Map<EMHSectionIdem, String> sectionIdemWithNoRef = new HashMap<>();
    /**
     * Sera non nulle si la carte noeud est trouvée. Permet de generer un warning si tous les noeuds utilisées par les branches ne sont pas définies
     * dans la carte noeuds.
     */
    Set<String> definedNodes;
    boolean warnDoneForDvers;
    private SingleValueConverter enumFormulePdcMapCrue9;
    /**
     * Correspondance EnumSensOrifice<->String
     */
    private SingleValueConverter enumSensOrificeMapCrue9;
    private boolean warnDoneForDigue;

    /**
     * @param in la chaine a tester
     * @return si identifie un lit mineur
     */
    protected static boolean isLitMineur(final String in) {
        if (CtuluLibString.isEmpty(in)) {
            return false;
        }
        final String inUpper = in.toUpperCase();
        return inUpper.startsWith("LT_MINEUR") || inUpper.startsWith("MINEUR");
    }

    /**
     * @param branche  pour laquelle on initalise 2 sections sans geometrie: une amont et une aval.
     * @param longueur la longueur de la branche
     * @param metier   structure dans laquelle seront ajoutés les sections.
     */
    public static void addSectionSansGeometrie(final CatEMHBranche branche, final double longueur, final CrueData metier) {
        final EMHSectionSansGeometrie amont = new EMHSectionSansGeometrie(CruePrefix.changePrefix(branche.getNom(),
                CruePrefix.P_BRANCHE,
                CruePrefix.P_SECTION)
                + "_Am");
        final EMHSectionSansGeometrie aval = new EMHSectionSansGeometrie(CruePrefix.changePrefix(branche.getNom(),
                CruePrefix.P_BRANCHE,
                CruePrefix.P_SECTION)
                + "_Av");
        metier.add(amont);
        metier.add(aval);
        branche.addListeSections(Arrays.asList(EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(branche, amont, 0,
                EnumPosSection.AMONT, metier.getCrueConfigMetier()),
                EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(
                        branche, aval, longueur, EnumPosSection.AVAL, metier.getCrueConfigMetier())));
    }

    private String addErrorForOldBranche(final int typeBranche, final String nomBranche) {
        switch (typeBranche) {
            case CrueIODico.BRANCHE_0_ABANDONNE:
                return "dc.branche0_9";
            case CrueIODico.BRANCHE_9_ABANDONNE:
                return "dc.branche0_9";
            case CrueIODico.BRANCHE_3_ABANDONNE:
                return "dc.branche3_7";
            case CrueIODico.BRANCHE_7_ABANDONNE:
                return "dc.branche3_7";
            case CrueIODico.BRANCHE_8_ABANDONNE:
                return "dc.branche8";
            case CrueIODico.BRANCHE_10_ABANDONNE:
                return "dc.branche10";
            default:
                return "io.global.cantCreateBranche.error";
        }
    }

    private void analyzeDistanceInterProfil(final String idBranche, final List<Double> listeDistance) {
        if (CollectionUtils.isEmpty(listeDistance)) {
            analyze_.addSevereError("dc.distance.intersection.null", idBranche);
            return;
        }
        final PropertyEpsilon eps = dataLinked.getCrueConfigMetier().getEpsilon("xp");
        for (final Double dis : listeDistance) {
            final double val = dis.doubleValue();
            if (val < 0 || eps.isZero(val)) {
                analyze_.addSevereError("dc.distance.intersection.null", idBranche);
                return;
            }
        }
    }

    private void checkCasier(final CrueData crueData) {
        final List<CatEMHCasier> casiers = crueData.getCasiers();
        for (final CatEMHCasier catEMHCasier : casiers) {
            final List<DonPrtGeoProfilCasier> profils = EMHHelper.selectInstanceOf(catEMHCasier.getInfosEMH(),
                    DonPrtGeoProfilCasier.class);
            for (final DonPrtGeoProfilCasier profil : profils) {
                final int size = profil.getPtProfil().size();
                if (size < 2) {
                    analyze_.addSevereError("io.dh.casier.tooSmall", catEMHCasier.getId(), profil.getId(), size);
                }
                if (profil.getLitUtile() == null && size > 1) {
                    final LitUtile lit = new LitUtile();
                    lit.setLimDeb(profil.getPtProfil().get(0));
                    lit.setLimFin(profil.getPtProfil().get(size - 1));
                    profil.setLitUtile(lit);
                }
            }
        }
    }

    /**
     * Complete la branche avec la liste des infos lues sur chaque branche spécifiques.
     *
     * @param metier              metier
     * @param branche             la branche
     * @param listeDistance       les distances
     * @param listeCconv          les cconv
     * @param listeCpond          les cpond
     * @param listeCdiv           les cdiv
     * @param listeSectionBranche les sections
     * @author Adrien Hadoux
     */
    private void completeBrancheWithInfosRead(final CrueData metier, final CatEMHBranche branche,
                                              final List<Double> listeDistance, final List<Double> listeCconv,
                                              final List<Double> listeCpond,
                                              final List<Double> listeCdiv, final List<CatEMHSection> listeSectionBranche) {

        final List<CatEMHSection> listeTotaleSection = listeSectionBranche;

        final List<Double> listeTotaleDistance = listeDistance;
        final List<Double> listeTotaleCconv = listeCconv;
        final List<Double> listeTotalecdiv = listeCdiv;
        final List<Double> listeTotalecpond = listeCpond;

        // -- on remplit les distances avec les bonnes sections profils crees --//
        // int indiceSection = 0;
        double sumDistanceTotaleXP = 0;
        final List<RelationEMH> listeRelationEmh = new ArrayList<>();
        final int size = listeTotaleSection.size();
        for (int i = 0; i < size; i++) {
            final CatEMHSection section = listeSectionBranche.get(i);
            // -- donnees liees aux section profils de branches, cf DRSO --//
            double coefPond = metier.getCrueConfigMetier().getDefaultDoubleValue(CrueConfigMetierConstants.PROP_COEF_POND);
            double coefConv = metier.getCrueConfigMetier().getDefaultDoubleValue(CrueConfigMetierConstants.PROP_COEF_CONV);
            double coefDiv = metier.getCrueConfigMetier().getDefaultDoubleValue(CrueConfigMetierConstants.PROP_COEF_DIV);

            // -- cconv --//
            // pour la première section ( section amont) on laisse les valeurs nulles.
            // pour les autres on utilise les valeurs lues
            if (i > 0) {
                if (listeTotaleDistance.size() == 1) {
                    sumDistanceTotaleXP += listeTotaleDistance.get(0);
                } else if (listeTotaleDistance.size() > i - 1) {
                    // -- la liste des distantce est inter-profil avec le premier profil a distance=0.
                    // donc on commence a positionner la deuxieme section a la position indiceSection-1.
                    sumDistanceTotaleXP += listeTotaleDistance.get(i - 1);
                }
                if (listeTotaleCconv.size() == 1) {
                    coefConv = listeTotaleCconv.get(0);
                } // a revoir car on devrait pas
                else if (listeTotaleCconv.size() > i - 1) {
                    coefConv = listeTotaleCconv.get(i - 1);
                }
                // -- cdiv --//
                if (listeTotalecdiv.size() == 1) {
                    coefDiv = listeTotalecdiv.get(0);
                } else if (listeTotalecdiv.size() > i - 1) {
                    coefDiv = listeTotalecdiv.get(i - 1);
                }
                if (listeTotalecpond.size() == 1) {
                    coefPond = listeTotalecpond.get(0);
                } else if (listeTotalecpond.size() > i - 1) {
                    coefPond = listeTotalecpond.get(i - 1);
                }
            }

            // -- ajout de la section dans les données métier si pas déja existant --//
            if (!metier.getSections().contains(section)) {
                metier.add(section);
            }

            listeRelationEmh.add(EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(branche, section, sumDistanceTotaleXP, coefPond,
                    coefConv, coefDiv, dataLinked.getCrueConfigMetier()));
        }
        // on cree 2 sections par défaut si pas de géometrie
        if (listeRelationEmh.isEmpty()) {
            double xp = 0;
            if (CollectionUtils.isNotEmpty(listeDistance)) {
                xp = listeDistance.get(0);
            }
            addSectionSansGeometrie(branche, xp, metier);
        } else {
            // -- on ajoute la liste des relations EMH à la branche --//
            branche.addListeSections(listeRelationEmh);
            if (!listeRelationEmh.isEmpty()) {
                ((RelationEMHSectionDansBranche) listeRelationEmh.get(0)).setPos(EnumPosSection.AMONT);
            }
            if (listeRelationEmh.size() > 1) {
                ((RelationEMHSectionDansBranche) listeRelationEmh.get(listeRelationEmh.size() - 1)).setPos(EnumPosSection.AVAL);
            }
        }
    }

    /**
     * Remplit les infos DPTG du profil avec les données lues pour les profils (lits, frottements, pt)
     *
     * @param sectionProfil    la section
     * @param listeFrottements les frottements
     * @param listeLits        les lits
     * @param listeProfils     les profils
     */
    private void completeProfilWithInfosRead(final CatEMHSection sectionProfil, final List<DonFrt> listeFrottements,
                                             final List<LitNumerote> listeLits, final List<PtProfil> listeProfils,
                                             final List<Integer> listeLimiteJ,
                                             final List<Double> listeLimiteX, final List<Integer> listeLitsActifs,
                                             final DonPrtGeoProfilSectionFenteData fente) {
        // -- on associe les objets crées entre eux pour creer un DPTG--//
        final DonPrtGeoProfilSection dataDPTG = new DonPrtGeoProfilSection();
        dataDPTG.setNom(CruePrefix.addPrefix(sectionProfil.getNom(), CruePrefix.P_SECTION, CruePrefix.P_PROFIL_SECTION));
        dataDPTG.setFente(fente);
        sectionProfil.addInfosEMH(dataDPTG);
        dataDPTG.setPtProfil(listeProfils);
        int cpt = 0;
        LitNumerote litPrecedent = null;
        for (final LitNumerote lit : listeLits) {

            DonFrt frottement = null;
            if (cpt < listeFrottements.size()) {
                frottement = listeFrottements.get(cpt);
            }
            // -- ajout du frottement au lit --//
            if (frottement != null) {
                lit.setFrot(frottement);
            }

            // -- ajout des limites au lit --//
            if (cpt < listeLimiteJ.size()) {
                final int indicePt = listeLimiteJ.get(cpt);
                final PtProfil limite = listeProfils.get(indicePt);
                lit.setLimDeb(limite);

                if (litPrecedent != null) {
                    litPrecedent.setLimFin(limite);
                }
            } else {
                if (cpt < listeLimiteX.size()) {

                    final double abscisseArraprocher = listeLimiteX.get(cpt);
                    final int limDebIdx = CrueComparatorHelper.getNearestPtProfilEnY(new PtProfil(abscisseArraprocher, 0), listeProfils,
                            litPrecedent == null);
                    final PtProfil limDeb = limDebIdx >= 0 ? listeProfils.get(limDebIdx) : null;
                    if (limDeb != null) {
                        lit.setLimDeb(limDeb);
                        if (litPrecedent != null) {
                            litPrecedent.setLimFin(limDeb);
                        }
                    }
                }
            }

            // -- on vérifie si il est actif --//
            if (listeLitsActifs.isEmpty()) {
                // -- cas par défaut: ligne non renseognée =>est actif --//
                lit.setIsLitActif(true);
            } else {
                for (final int indiceLitActif : listeLitsActifs) {
                    if (indiceLitActif == cpt) {
                        lit.setIsLitActif(true);
                        break;
                    }
                }
            }

            // -- ajout du lit dans dptg --//
            dataDPTG.addLitNumerote(lit);
            litPrecedent = lit;
            cpt++;
        }
        // -- on ajoute la limite fin du dernier elt, puisque les dernier elt n sont les premiers des n+1--//
        if (listeLits.size() > 0) {

            final LitNumerote lastLit = listeLits.get(listeLits.size() - 1);
            // si pas de limite de définies, le lit est definit avec le premier et dernier point:
            // question de fred: normalement listLits.size doit valoir 1 ici avec LimDeb null pour ce lit
            // le cas limitX ( limites définies avec des X position et non pas desindices) est embetant
            if (listeLimiteJ.isEmpty() && listeLimiteX.isEmpty() && listeLits.size() == 1) {
                lastLit.setLimFin(listeProfils.get(listeProfils.size() - 1));
                lastLit.setLimDeb(listeProfils.get(0));
            } else if (!listeLimiteX.isEmpty()) {
                final double abscisseArraprocher = listeLimiteX.get(listeLimiteX.size() - 1);
                final int limDebIdx = CrueComparatorHelper.getNearestPtProfilEnY(new PtProfil(abscisseArraprocher, 0), listeProfils,
                        litPrecedent == null);
                final PtProfil limFin = limDebIdx >= 0 ? listeProfils.get(limDebIdx) : null;
                lastLit.setLimFin(limFin);
            }
            if (!listeLimiteJ.isEmpty()) {
                final int indicePt = listeLimiteJ.get(listeLimiteJ.size() - 1);
                final PtProfil limiteFin = listeProfils.get(indicePt);
                lastLit.setLimFin(limiteFin);
            }
        }
    }

    /**
     * Cas particulier de Branche st venant: pour la valeur DISTMAX: DISTMAX(f, u) distmax (Distance max entre 2 profils. Crée les profils interpolés
     * nécessaires) Cette valeur distmax indique la distance max entre chaque profil, donc si cette distance n'est pas respectee, il faut ajouter des
     * profil interpoles avec pour nom: S+NomBranche+_+distanceParcourue.
     *
     * @param branche
     */
    private void completeSaintVenantWithDistMax(final EMHBrancheSaintVenant branche, final float distmax,
                                                final CrueData data) {
//    final boolean isSaintVenant = branche instanceof EMHBrancheSaintVenant;
        final List<RelationEMHSectionDansBranche> sectionEMH = branche.getListeSections();
        if (CollectionUtils.isEmpty(sectionEMH)) {
            return;
        }
        branche.removeAllRelations(sectionEMH);
        // branche.getRelationEMH().add(relation2);
        branche.addRelationEMH(sectionEMH.get(0));
        for (int i = 0; i < sectionEMH.size() - 1; i++) {
            final RelationEMHSectionDansBranche relation1 = sectionEMH.get(i);
            // -- si la relation concerne les sections --//
            final RelationEMHSectionDansBranche relation2 = sectionEMH.get(i + 1);
            // WARN: pour respecter la norme Crue), on doit passer par des float:
            final double xpos1 = relation1.getXp();
            final double xpos2 = relation2.getXp();

            final float distance = (float) Math.abs(xpos1 - xpos2);
            if (distmax < distance) {

                // -- il faut ajouter des profils interpolées --//

                // -- etape 1: on compte combien on en ajoute --//
                final int nbProfilsToAdd = EMHHelper.getDistDiviseurForDistMax(distmax, distance);

                // -- on creer une section interpolee pour chaque besoin et on l'ajoute dans cruedata puis dans les relations
                // branche --//.
                final float distMaxCorrige = distance / nbProfilsToAdd;
                // on commence a 1: c'est normal on cree un prol
                // la doc indique
                // !* On calcule la distance moyenne distmoy, entre les profils
                // !* inseres. Le 1er profil insere recevra la distance distprem=reste,
                // !* pour eviter les problemes de troncation due a la precision.
                // !* Les autres profils recoivent la distance distmoy.
                // !* distance : distance entre les 2 profils d’origine
                // !* NbPrIns : nombre de profils interpolés à insérer
                // real*4 distance, distmoy, distprem
                // integer*4 NbPrIns
                // distmoy=distance/(NbPrIns+1)
                // distprem=distance-NbPrIns*distmoy
                final float distPrem = distance - (nbProfilsToAdd - 1) * distMaxCorrige;
                for (int k = 1; k < nbProfilsToAdd; k++) {
                    // on refait les calcul en double
                    final double xpos = xpos1 + ((((k - 1) * ((double) distMaxCorrige)) + ((double) distPrem)));
                    double doubleForNom = xpos;
                    if (doubleForNom >= 1) {
                        doubleForNom = (float) Math.floor(doubleForNom);
                    }
                    final EMHSectionInterpolee interpol = EMHRelationFactory.createSectionInterpoleFromDistmax(doubleForNom,
                            branche.getNom(), distMaxCorrige);

                    final RelationEMHSectionDansBranche relation = EMHRelationFactory.createSectionDansBrancheAndSetBrancheContientSection(branche, interpol,
                            xpos,
                            data.getCrueConfigMetier(),
                            true);
                    final RelationEMHSectionDansBrancheSaintVenant relationSaintVenant = (RelationEMHSectionDansBrancheSaintVenant) relation;
                    relationSaintVenant.initCoefFrom((RelationEMHSectionDansBrancheSaintVenant) relation2);
                    // -- ajout dans metier --//
                    data.add(interpol);
                    branche.addRelationEMH(relation);
                }
            }
            branche.addRelationEMH(relation2);
        }
    }

    /**
     * branche barrage fil d eau 15.
     *
     * @param metier CrueDate
     * @param idBranche l'id de la branche
     */
    private EMHBrancheBarrageFilEau createBrancheBarrageFilEau(final CrueData metier, final String idBranche)
            throws IOException {
        final EMHBrancheBarrageFilEau branche = new EMHBrancheBarrageFilEau(idBranche);

        // -- lecture du contenu formatté branche saint venant --//
        lireSuite();
        String lineRead = in_.getLine();

        // -- la liste des distances Xp sont affichées avant les sections --//
        final List<Double> listeDistance = new ArrayList<>();
        // -- la liste des coef Cconv sont affichées avant les sections --//
        final List<Double> listeCconv = new ArrayList<>();
        // -- la liste des coef Cpond sont affichées avant les sections --//
        final List<Double> listeCpond = new ArrayList<>();
        // -- la liste des coef Cdiv sont affichées avant les sections --//
        final List<Double> listeCdiv = new ArrayList<>();

        // -- la donnée dcsp si existe --//
        final DonCalcSansPrtBrancheBarrageFilEau dataDCSP = new DonCalcSansPrtBrancheBarrageFilEau(
                metier.getCrueConfigMetier());
        branche.addInfosEMH(dataDCSP);

        dataDCSP.setElemBarrage(new ArrayList<>());
        String nomSectionPilote = null;
        // -- la liste des sections branches --//
        final List<CatEMHSection> listeSectionBranche = new ArrayList<>();
        // -- tant qu'on est pas arrivé à une autre branche ou une autre definition, on lit les infos de la branche --//
        while (!isEof() && !CrueIODico.isAnewBrancheDefinition(lineRead) && CrueIODico.appartientBrancheCarte(lineRead)) {
            // -- recuperation de l'element lu en 1ere position --//
            final String typeLigne = in_.stringField(0);

            // -- remplissage des qmin qmax par rapport aux sections --//
            // ex: QMIN/QMAX -11000.000 11000.000
            if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_QMIN_QMAX)) {

                final double qmin = in_.doubleField(1);
                final double qmax = in_.doubleField(2);
                dataDCSP.setQLimInf(qmin);
                dataDCSP.setQLimSup(qmax);
            } else if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_ZQ)) {
                LoiFF LoiQZam = dataDCSP.getRegimeManoeuvrant();
                if (LoiQZam == null) {
                    LoiQZam = createLoiQpilZam(idBranche);
                    dataDCSP.setRegimeManoeuvrant(LoiQZam);
                }
                final List<PtEvolutionFF> ptEvolutionFF = LoiQZam.getEvolutionFF().getPtEvolutionFF();
                // ex: Z/Q(o) Z1 Q1 Z2 Q2… pour Denoye
                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    final PtEvolutionFF pointFF = new PtEvolutionFF();

                    // loiQZam donc q en abscisse et z en ordonnee
                    // z en premier et q en second !
                    pointFF.setOrdonnee(in_.doubleField(i++));
                    pointFF.setAbscisse(in_.doubleField(i));

                    ptEvolutionFF.add(pointFF);
                }
            } else if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_PROF)) {
                // -- peut y avoir 1 ou plusieurs coefficients --//
                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    final String nomSection = in_.stringField(i);
                    final CatEMHSection profil = createInitSectionForBranche(metier, nomSection);
                    listeSectionBranche.add(profil);
                }
            } else if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_NOMREF)) {
                // section pilote
                nomSectionPilote = in_.stringField(1);
            } else if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_PARAM)) {
                final int nbSeuil = (int) in_.doubleField(2);
                final double zseuil = in_.doubleField(4);
                final double coefD = in_.doubleField(1);
                for (int i = 0; i < nbSeuil; i++) {
                    final ElemBarrage seuil = new ElemBarrage(metier.getCrueConfigMetier());
                    final double largeur = in_.doubleField(3);
                    seuil.setZseuil(zseuil);
                    seuil.setCoefNoy(coefD);
                    seuil.setLargeur(largeur);
                    dataDCSP.addElemBarrage(seuil);
                }
            } else {
                errorCarteNotRecognized(typeLigne);
            }

            // -- on lit la suite en passant les commentaires sans mot clef --//
            lireSuite();
            lineRead = in_.getLine();
        }
        // -- on remplit avec les infos spécifiques de la branche --//
        completeBrancheWithInfosRead(metier, branche, listeDistance, listeCconv, listeCpond, listeCdiv, listeSectionBranche);

        if (nomSectionPilote != null) {
            CatEMHSection pilote = metier.findSectionByReference(nomSectionPilote);
            if (pilote == null) {
                pilote = new EMHSectionProfil(nomSectionPilote);
                metier.add(pilote);
            }
            branche.setSectionPilote(pilote);
        }

        return branche;
    }

    /**
     * branche generique 14.
     *
     * @param metier CrueData
     * @param idBranche id de la branche
     */
    private EMHBrancheBarrageGenerique createBrancheGenerique(final CrueData metier, final String idBranche)
            throws IOException {
        final EMHBrancheBarrageGenerique branche = new EMHBrancheBarrageGenerique(idBranche);

        // -- lecture du contenu formatté branche saint venant --//
        lireSuite();
        String lineRead = in_.getLine();

        // -- la liste des distances Xp sont affichées avant les sections --//
        final List<Double> listeDistance = new ArrayList<>();
        // -- la liste des coef Cconv sont affichées avant les sections --//
        final List<Double> listeCconv = new ArrayList<>();
        // -- la liste des coef Cpond sont affichées avant les sections --//
        final List<Double> listeCpond = new ArrayList<>();
        // -- la liste des coef Cdiv sont affichées avant les sections --//
        final List<Double> listeCdiv = new ArrayList<>();

        // -- la donnée dcsp si existe --//
        final DonCalcSansPrtBrancheBarrageGenerique dataDCSP = new DonCalcSansPrtBrancheBarrageGenerique(
                metier.getCrueConfigMetier());
        branche.addInfosEMH(dataDCSP);

        String nomSectionPilote = null;
        // -- la liste des sections branches --//
        final List<CatEMHSection> listeSectionBranche = new ArrayList<>();
        // -- tant qu'on est pas arrivé à une autre branche ou une autre definition, on lit les infos de la branche --//
        while (!isEof() && !CrueIODico.isAnewBrancheDefinition(lineRead) && CrueIODico.appartientBrancheCarte(lineRead)) {
            // -- recuperation de l'element lu en 1ere position --//
            final String typeLigne = in_.stringField(0);

            // -- remplissage des qmin qmax par rapport aux sections --//
            // ex: QMIN/QMAX -11000.000 11000.000
            if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_QMIN_QMAX)) {

                final double qmin = in_.doubleField(1);
                final double qmax = in_.doubleField(2);
                dataDCSP.setQLimInf(qmin);
                dataDCSP.setQLimSup(qmax);
            } else if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_ZQ)) {
                final List<PtEvolutionFF> ptEvolutionFF = getLoiQZam(idBranche, dataDCSP);
                // ex: Z/Q(o) Z1 Q1 Z2 Q2… pour Denoye
                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    final PtEvolutionFF pointFF = new PtEvolutionFF();
                    // inversion normale
                    pointFF.setOrdonnee(in_.doubleField(i++));
                    pointFF.setAbscisse(in_.doubleField(i));

                    ptEvolutionFF.add(pointFF);
                }
            } else if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_DZQ)) {
                final List<PtEvolutionFF> ptEvolutionFF = getLoiQDz(idBranche, dataDCSP);
                // ex: DZ/Q(o) dZ1 Q1 dZ2 Q2… pour Noye
                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    final PtEvolutionFF pointFF = new PtEvolutionFF();
                    // dz en premier alors que dz en ordonnee (QDz)
                    pointFF.setOrdonnee(in_.doubleField(i++));
                    pointFF.setAbscisse(in_.doubleField(i));

                    ptEvolutionFF.add(pointFF);
                }
            } else if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_PROF)) {
                // -- peut y avoir 1 ou plusieurs coefficients --//
                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    final String nomSection = in_.stringField(i);
                    final CatEMHSection profil = createInitSectionForBranche(metier, nomSection);
                    listeSectionBranche.add(profil);
                }
            } else if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_NOMREF)) {
                nomSectionPilote = in_.stringField(1);
            } else {
                errorCarteNotRecognized(typeLigne);
            }

            // -- on lit la suite en passant les commentaires sans mot clef --//
            lireSuite();
            lineRead = in_.getLine();
        }
        // -- on remplit avec les infos spécifiques de la branche --//
        completeBrancheWithInfosRead(metier, branche, listeDistance, listeCconv, listeCpond, listeCdiv, listeSectionBranche);

        if (nomSectionPilote != null) {
            CatEMHSection pilote = metier.findSectionByReference(nomSectionPilote);
            if (pilote == null) {
                pilote = new EMHSectionProfil(nomSectionPilote);
                metier.add(pilote);
            }
            branche.setSectionPilote(pilote);
        }

        return branche;
    }

    /**
     * branche niveaux associes 12.
     *
     * @param metier metier
     * @param idBranche id de la branche
     * @return branche.
     */
    private EMHBrancheNiveauxAssocies createBrancheNiveauAssocie(final CrueData metier, final String idBranche)
            throws IOException {
        final EMHBrancheNiveauxAssocies branche = new EMHBrancheNiveauxAssocies(idBranche);

        // -- lecture du contenu formatté branche saint venant --//
        lireSuite();
        String lineRead = in_.getLine();

        // -- la liste des distances Xp sont affichées avant les sections --//
        final List<Double> listeDistance = new ArrayList<>();
        // -- la liste des coef Cconv sont affichées avant les sections --//
        final List<Double> listeCconv = new ArrayList<>();
        // -- la liste des coef Cpond sont affichées avant les sections --//
        final List<Double> listeCpond = new ArrayList<>();
        // -- la liste des coef Cdiv sont affichées avant les sections --//
        final List<Double> listeCdiv = new ArrayList<>();

        // -- la donnée dcsp si existe --//
        final DonCalcSansPrtBrancheNiveauxAssocies dataDCSP = new DonCalcSansPrtBrancheNiveauxAssocies(
                metier.getCrueConfigMetier());
        branche.addInfosEMH(dataDCSP);

        // -- la liste des sections branches --//
        final List<CatEMHSection> listeSectionBranche = new ArrayList<>();
        // -- tant qu'on est pas arrivé à une autre branche ou une autre definition, on lit les infos de la branche --//
        while (!isEof() && !CrueIODico.isAnewBrancheDefinition(lineRead) && CrueIODico.appartientBrancheCarte(lineRead)) {
            // -- recuperation de l'element lu en 1ere position --//
            final String typeLigne = in_.stringField(0);

            // -- remplissage des qmin qmax par rapport aux sections --//
            // ex: QMIN/QMAX -11000.000 11000.000
            if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_QMIN_QMAX)) {
                final double qmin = in_.doubleField(1);
                final double qmax = in_.doubleField(2);
                dataDCSP.setQLimInf(qmin);
                dataDCSP.setQLimSup(qmax);
            } else if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_ZAMZAV)) {
                final List<PtEvolutionFF> ptEvolutionFF = getLoiZavZam(idBranche, dataDCSP);
                // ex: ZAM/ZAV(o) zam1 zav1 zam2 zav2
                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    final PtEvolutionFF pointFF = new PtEvolutionFF();
                    // inversion normale
                    pointFF.setOrdonnee(in_.doubleField(i++));
                    pointFF.setAbscisse(in_.doubleField(i));
                    ptEvolutionFF.add(pointFF);
                }
            } else {
                errorCarteNotRecognized(typeLigne);
            }

            // -- on lit la suite en passant les commentaires sans mot clef --//
            lireSuite();
            lineRead = in_.getLine();
        }
        // -- on remplit avec les infos spécifiques de la branche --//
        completeBrancheWithInfosRead(metier, branche, listeDistance, listeCconv, listeCpond, listeCdiv, listeSectionBranche);
        return branche;
    }

    /**
     * branche orifice 5.
     *
     * @param metier CrueData
     * @param idBranche nom de la branche
     */
    private EMHBrancheOrifice createBrancheOrifice(final CrueData metier, final String idBranche) throws IOException {
        final EMHBrancheOrifice branche = new EMHBrancheOrifice(idBranche);

        // -- lecture du contenu formatté branche saint venant --//
        lireSuite();
        String lineRead = in_.getLine();

        // -- la liste des distances Xp sont affichées avant les sections --//
        final List<Double> listeDistance = new ArrayList<>();
        // -- la liste des coef Cconv sont affichées avant les sections --//
        final List<Double> listeCconv = new ArrayList<>();
        // -- la liste des coef Cpond sont affichées avant les sections --//
        final List<Double> listeCpond = new ArrayList<>();
        // -- la liste des coef Cdiv sont affichées avant les sections --//
        final List<Double> listeCdiv = new ArrayList<>();

        ElemOrifice elementOrifice = null;

        // final ArrayList<ElemOrifice> listeElem = new ArrayList<ElemOrifice>();

        // -- la liste des sections branches --//
        final List<CatEMHSection> listeSectionBranche = new ArrayList<>();
        // -- tant qu'on est pas arrivé à une autre branche ou une autre definition, on lit les infos de la branche --//
        while (!isEof() && !CrueIODico.isAnewBrancheDefinition(lineRead) && CrueIODico.appartientBrancheCarte(lineRead)) {
            // -- recuperation de l'element lu en 1ere position --//
            final String typeLigne = in_.stringField(0);

            if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_CCTRMAX)) {
                if (elementOrifice == null) {
                    elementOrifice = new ElemOrifice(metier.getCrueConfigMetier());
                }
                // ex CCTRMAX 0.65
                final double coeff = in_.doubleField(1);
                elementOrifice.setCoefCtrLim(coeff);
            } else if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_CLAPET)) {
                if (elementOrifice == null) {
                    elementOrifice = new ElemOrifice(metier.getCrueConfigMetier());
                }
                String sens = null;
                final double doubleField = in_.doubleField(5);
                if (CtuluLib.isEquals(doubleField, -1) || CtuluLib.isEquals(doubleField, 0)
                        || CtuluLib.isEquals(doubleField, 1)) {
                    sens = CtuluLibString.getString((int) doubleField);
                }
                elementOrifice.setZseuil(in_.doubleField(1));
                elementOrifice.setHaut(in_.doubleField(2));
                elementOrifice.setLargeur(in_.doubleField(3));
                if (in_.getNumberOfFields() > 4) {
                    elementOrifice.setCoefD(in_.doubleField(4));
                }
                final EnumSensOrifice enumSens = (EnumSensOrifice) this.enumSensOrificeMapCrue9.fromString(sens);
                if (enumSens == null) {
                    analyze_.addSevereError("io.convert.sensOuv.error", in_.getLineNumber());
                }

                elementOrifice.setSensOrifice(enumSens);
            } else {
                errorCarteNotRecognized(typeLigne);
            }

            // -- on lit la suite en passant les commentaires sans mot clef --//
            lireSuite();
            lineRead = in_.getLine();
        }

        // -- la donnée dcsp si existe --//
        // -- remplissage dcsp avec element orifice --//
        if (elementOrifice != null) {
            final DonCalcSansPrtBrancheOrifice dataDCSP =  new DonCalcSansPrtBrancheOrifice();
            dataDCSP.setElemOrifice(elementOrifice);
            branche.addInfosEMH(dataDCSP);
        }
        // -- on remplit avec les infos spécifiques de la branche --//
        completeBrancheWithInfosRead(metier, branche, listeDistance, listeCconv, listeCpond, listeCdiv, listeSectionBranche);
        return branche;
    }

    /**
     * branche pdc 1.
     *
     * @param metier
     * @param idBranche
     */
    private EMHBranchePdc createBranchePdc(final CrueData metier, final String idBranche) throws IOException {
        final EMHBranchePdc branche = new EMHBranchePdc(idBranche);

        // -- lecture du contenu formatté branche saint venant --//
        lireSuite();
        String lineRead = in_.getLine();

        // -- la liste des distances Xp sont affichées avant les sections --//
        final List<Double> listeDistance = new ArrayList<>();
        // -- la liste des coef Cconv sont affichées avant les sections --//
        final List<Double> listeCconv = new ArrayList<>();
        // -- la liste des coef Cpond sont affichées avant les sections --//
        final List<Double> listeCpond = new ArrayList<>();
        // -- la liste des coef Cdiv sont affichées avant les sections --//
        final List<Double> listeCdiv = new ArrayList<>();

        // -- la donnée dcsp si existe --//
        DonCalcSansPrtBranchePdc dataDCSP = null;

        // -- la liste des sections branches --//
        final List<CatEMHSection> listeSectionBranche = new ArrayList<>();
        // -- tant qu'on est pas arrivé à une autre branche ou une autre definition, on lit les infos de la branche --//
        while (!isEof() && !CrueIODico.isAnewBrancheDefinition(lineRead) && CrueIODico.appartientBrancheCarte(lineRead)) {
            // -- recuperation de l'element lu en 1ere position --//
            final String typeLigne = in_.stringField(0);

            if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_DZQ)) {
                // ex DZ/Q 0 0 0.03 300
                if (dataDCSP == null) {
                    dataDCSP = EMHFactory.createDCSPBranchePdcCrue9(idBranche);
                }

                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    final PtEvolutionFF pointFF = new PtEvolutionFF();
                    // inversion normale
                    pointFF.setOrdonnee(in_.doubleField(i++));
                    pointFF.setAbscisse(in_.doubleField(i));
                    dataDCSP.getPdc().getEvolutionFF().getPtEvolutionFF().add(pointFF);
                }
            } else {
                errorCarteNotRecognized(typeLigne);
            }

            // -- on lit la suite en passant les commentaires sans mot clef --//
            lireSuite();
            lineRead = in_.getLine();
        }
        if (dataDCSP != null) {
            branche.addInfosEMH(dataDCSP);
        }
        // -- on remplit avec les infos spécifiques de la branche --//
        completeBrancheWithInfosRead(metier, branche, listeDistance, listeCconv, listeCpond, listeCdiv, listeSectionBranche);
        return branche;
    }

    /**
     * Lit la branche SaintVenant en adaptant le contenu selon les lignes lues. Arrive à la ligne début de branche. Lit toutes les lignes
     * correspondantes à la branche saint venant.
     *
     * @param metier CrueData
     * @param idBranche nom de la branche
     * @return branche.
     */
    private EMHBrancheSaintVenant createBrancheSaintVenant(final CrueData metier, final String idBranche)
            throws IOException {
        final EMHBrancheSaintVenant branche = new EMHBrancheSaintVenant(idBranche);

        // -- lecture du contenu formatté branche saint venant --//
        lireSuite();
        String lineRead = in_.getLine();

        // -- la liste des distances Xp sont affichées avant les sections --//
        final List<Double> listeDistance = new ArrayList<>();
        // -- la liste des coef Cconv sont affichées avant les sections --//
        final List<Double> listeCconv = new ArrayList<>();
        // -- la liste des coef Cpond sont affichées avant les sections --//
        final List<Double> listeCpond = new ArrayList<>();
        // -- la liste des coef Cdiv sont affichées avant les sections --//
        final List<Double> listeCdiv = new ArrayList<>();

        // -- la donnée dcsp si existe --//
        final DonCalcSansPrtBrancheSaintVenant dataDCSP = new DonCalcSansPrtBrancheSaintVenant(
                metier.getCrueConfigMetier());
        branche.addInfosEMH(dataDCSP);

        // -- la donnée DPTG de st venant --//
        final DonPrtGeoBrancheSaintVenant dataDPTG = new DonPrtGeoBrancheSaintVenant(
                metier.getCrueConfigMetier());
        branche.addInfosEMH(dataDPTG);
        // String nomSectionPilote = null;
        double distMAX = 0;
        // -- la liste des sections branches --//
        final List<CatEMHSection> listeSectionBranche = new ArrayList<>();
        // -- tant qu'on est pas arrivé à une autre branche ou une autre definition, on lit les infos de la branche --//
        while (!isEof() && !CrueIODico.isAnewBrancheDefinition(lineRead) && CrueIODico.appartientBrancheCarte(lineRead)) {
            // -- recuperation de l'element lu en 1ere position --//
            final String typeLigne = in_.stringField(0);

            // -- remplissage des distances par rapport aux sections --//
            // ex: DISTANCE 100.00 100.00 100.00
            if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_DISTANCE)) {
                // -- peut y avoir 1 ou plusieurs coefficients --//
                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    listeDistance.add(in_.doubleField(i));
                }
            } else // -- gestion des sections profils --//
                // ex: PROF PROF6B PROF5 PROF4 PROF3A
                if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_PROF)) {
                    // -- peut y avoir 1 ou plusieurs coefficients --//
                    for (int i = 1; i < in_.getNumberOfFields(); i++) {
                        final String nomSection = in_.stringField(i);
                        final CatEMHSection profil = createInitSectionForBranche(metier, nomSection);
                        listeSectionBranche.add(profil);
                    }
                } else if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_DISTMAX)) {
                    metier.setCrue9ContientDistmax(true);
                    distMAX = in_.doubleField(1);
                    if (distMAX <= 0) {
                        analyze_.addSevereError("dc.distmax.null", in_.getLineNumber(), idBranche);
                    }
                } else // -- coefficients cconv --//
                    if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_CCONV)) {
                        // -- peut y avoir 1 ou plusieurs coefficients --//
                        for (int i = 1; i < in_.getNumberOfFields(); i++) {
                            listeCconv.add(in_.doubleField(i));
                        }
                    } else // -- coefficients cconv --//
                        if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_CPOND)) {
                            // -- peut y avoir 1 ou plusieurs coefficients --//
                            for (int i = 1; i < in_.getNumberOfFields(); i++) {
                                listeCpond.add(in_.doubleField(i));
                            }
                        } else // -- coefficients cconv --//
                            if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_CDIV)) {
                                // -- peut y avoir 1 ou plusieurs coefficients --//
                                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                                    listeCdiv.add(in_.doubleField(i));
                                }
                            } else // -- coeffBeta de DCSP--//
                                if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_ALPHA)) {
                                    final double coeffbeta = in_.doubleField(1);
                                    (dataDCSP).setCoefBeta(coeffbeta);
                                } else // -- coeffRuis de DCSP--//
                                    if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_RUIS)) {
                                        final double coeffRuis = in_.doubleField(1);
                                        (dataDCSP).setCoefRuis(coeffRuis);
                                    } else // -- SINUO de DCSP--//
                                        if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_SINUO)) {
                                            // -- concerne données DPTG --//

                                            // ex SINUO 600.0 500.0 (si LongMineur = 600 m et LongMajeur = 500 m) :

                                            final double longMineur = in_.doubleField(1);
                                            final double longMajeur = in_.doubleField(2);

                                            final double coeff = longMineur / longMajeur;
                                            dataDPTG.setCoefSinuo(coeff);
                                        } else {
                                            errorCarteNotRecognized(typeLigne);
                                        }

            // -- on lit la suite en passant les commentaires sans mot clef --//
            lireSuite();
            lineRead = in_.getLine();
        }// on boucle sur le while.

        // test des distance.
        analyzeDistanceInterProfil(idBranche, listeDistance);
        // -- on remplit avec les infos spécifiques de la branche --//
        completeBrancheWithInfosRead(metier, branche, listeDistance, listeCconv, listeCpond, listeCdiv, listeSectionBranche);

        if (distMAX > 0) {
            // on doit utiliser des float:
            completeSaintVenantWithDistMax(branche, (float) distMAX, metier);
        }

        return branche;
    }

    /**
     * branche latérale 4.
     *
     * @param metier
     * @param idBranche
     */
    private EMHBrancheSeuilLateral createBrancheSeuilLateral(final CrueData metier, final String idBranche)
            throws IOException {
        final EMHBrancheSeuilLateral branche = new EMHBrancheSeuilLateral(idBranche);

        // -- lecture du contenu formatté branche saint venant --//
        lireSuite();
        String lineRead = in_.getLine();

        // -- la liste des distances Xp sont affichées avant les sections --//
        final List<Double> listeDistance = new ArrayList<>();
        // -- la liste des coef Cconv sont affichées avant les sections --//
        final List<Double> listeCconv = new ArrayList<>();
        // -- la liste des coef Cpond sont affichées avant les sections --//
        final List<Double> listeCpond = new ArrayList<>();
        // -- la liste des coef Cdiv sont affichées avant les sections --//
        final List<Double> listeCdiv = new ArrayList<>();

        // -- la donnée dcsp si existe --//
        DonCalcSansPrtBrancheSeuilLateral dataDCSP = null;

        // -- la liste des sections branches --//
        final List<CatEMHSection> listeSectionBranche = new ArrayList<>();
        // -- tant qu'on est pas arrivé à une autre branche ou une autre definition, on lit les infos de la branche --//
        while (!isEof() && !CrueIODico.isAnewBrancheDefinition(lineRead) && CrueIODico.appartientBrancheCarte(lineRead)) {
            // -- recuperation de l'element lu en 1ere position --//
            final String typeLigne = in_.stringField(0);

            // -- gestion des données DCSP: lignes commencant par Borda ou seuil ... --//
            if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_BORDA)) {
                if (dataDCSP == null) {
                    // -- on init la donnée dcsp --//
                    dataDCSP = EMHFactory.createBrancheSeuilLateralForCrue9();
                }
                dataDCSP.setFormulePdc(readFormulePdc());
            } else // -- gestion des données DCSP: lignes commencant par Borda ou seuil ... --//
                if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_SEUIL)) {
                    if (dataDCSP == null) {
                        // -- on init la donnée dcsp --//
                        dataDCSP = EMHFactory.createBrancheSeuilLateralForCrue9();
                    }
                    // ex: SEUIL 50.000 86.600 1.000

                    // -- recuperation des donneees du seuil --//
                    final ElemSeuilAvecPdc seuil = new ElemSeuilAvecPdc(metier.getCrueConfigMetier());
                    seuil.setLargeur(in_.doubleField(1));
                    seuil.setZseuil(in_.doubleField(2));
                    if (in_.getNumberOfFields() > 3) {
                        seuil.setCoefD(in_.doubleField(3));
                    }
                    // lecture de cpert
                    if (in_.getNumberOfFields() > 4) {
                        seuil.setCoefPdc(in_.doubleField(4));
                    }
                    // -- ajout du seuil --//
                    dataDCSP.addElemSeuilAvecPdc(seuil);
                    // }

                } else {
                    errorCarteNotRecognized(typeLigne);
                }

            // -- on lit la suite en passant les commentaires sans mot clef --//
            lireSuite();
            lineRead = in_.getLine();
        }
        if (dataDCSP != null) {
            branche.addInfosEMH(dataDCSP);
        }
        // -- on remplit avec les infos spécifiques de la branche --//
        completeBrancheWithInfosRead(metier, branche, listeDistance, listeCconv, listeCpond, listeCdiv, listeSectionBranche);
        return branche;
    }

    private EnumFormulePdc readFormulePdc() {
        final double doubleField = in_.doubleField(1);
        EnumFormulePdc loi = null;
        if (CtuluLib.isEquals(0, doubleField) || CtuluLib.isEquals(1, doubleField)) {
            final String valBool = CtuluLibString.getString((int) doubleField);
            loi = (EnumFormulePdc) enumFormulePdcMapCrue9.fromString(valBool);
        }
        if (loi == null) {
            analyze_.addSevereError("io.convert.borda.error", in_.getLineNumber());
        }
        return loi;
    }

    /**
     * Creer une branche seuil transversal. 2
     *
     * @param metier
     * @param idBranche
     */
    public EMHBrancheSeuilTransversal createBrancheSeuilTransversal(final CrueData metier, final String idBranche)
            throws IOException {
        final EMHBrancheSeuilTransversal branche = new EMHBrancheSeuilTransversal(idBranche);

        // -- lecture du contenu formatté branche saint venant --//
        lireSuite();
        String lineRead = in_.getLine();

        // -- la liste des distances Xp sont affichées avant les sections --//
        final List<Double> listeDistance = new ArrayList<>();
        // -- la liste des coef Cconv sont affichées avant les sections --//
        final List<Double> listeCconv = new ArrayList<>();
        // -- la liste des coef Cpond sont affichées avant les sections --//
        final List<Double> listeCpond = new ArrayList<>();
        // -- la liste des coef Cdiv sont affichées avant les sections --//
        final List<Double> listeCdiv = new ArrayList<>();

        // -- la donnée dcsp si existe --//
        DonCalcSansPrtBrancheSeuilTransversal dataDCSP = null;

        // -- la liste des sections branches --//
        final List<CatEMHSection> listeSectionBranche = new ArrayList<>();
        // -- tant qu'on est pas arrivé à une autre branche ou une autre definition, on lit les infos de la branche --//
        while (!isEof() && !CrueIODico.isAnewBrancheDefinition(lineRead) && CrueIODico.appartientBrancheCarte(lineRead)) {
            // -- recuperation de l'element lu en 1ere position --//
            final String typeLigne = in_.stringField(0);

            // -- gestion des données DCSP: lignes commencant par Borda ou seuil ... --//
            if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_BORDA)) {
                if (dataDCSP == null) {
                    // -- on init la donnée dcsp --//
                    dataDCSP = EMHFactory.createBrancheSeuilTransversalForCrue9();
                }
                // -- selon le booleen 1 ou 0 le type de loi --//
                dataDCSP.setFormulePdc(readFormulePdc());
            } else // -- gestion des données DCSP: lignes commencant par Borda ou seuil ... --//
                if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_SEUIL)) {
                    if (dataDCSP == null) {
                        // -- on init la donnée dcsp --//
                        dataDCSP = EMHFactory.createBrancheSeuilTransversalForCrue9();
                    }

                    // ex: SEUIL 50.000 86.600 1.000

                    // -- recuperation des donneees du seuil --//
                    final ElemSeuilAvecPdc seuil = new ElemSeuilAvecPdc(metier.getCrueConfigMetier());
                    seuil.setLargeur(in_.doubleField(1));
                    seuil.setZseuil(in_.doubleField(2));
                    if (in_.getNumberOfFields() > 3) {
                        seuil.setCoefD(in_.doubleField(3));
                    }
                    if (in_.getNumberOfFields() > 4) {
                        seuil.setCoefPdc(in_.doubleField(4));
                    }
                    // -- ajout du seuil --//
                    dataDCSP.addElemSeuilAvecPdc(seuil);
                    // }

                } else // -- gestion des sections profils toujours 2--//
                    // ex: PROF PROFAMont PROF AVL
                    if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_PROF)
                            || typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_NOMREF)) {
                        // -- peut y avoir 1 ou plusieurs coefficients --//
                        for (int i = 1; i < in_.getNumberOfFields(); i++) {
                            final String nomSection = in_.stringField(i);
                            final CatEMHSection profil = createInitSectionForBranche(metier, nomSection);
                            listeSectionBranche.add(profil);
                        }
                    } else {
                        errorCarteNotRecognized(typeLigne);
                    }

            // -- on lit la suite en passant les commentaires sans mot clef --//
            lireSuite();
            lineRead = in_.getLine();
        }
        if (dataDCSP != null) {
            branche.addInfosEMH(dataDCSP);
        }
        // -- on remplit avec les infos spécifiques de la branche --//
        completeBrancheWithInfosRead(metier, branche, listeDistance, listeCconv, listeCpond, listeCdiv, listeSectionBranche);
        return branche;
    }

    /**
     * Creer la branche specifique en fonction de son numero type.
     *
     * @param typebranche
     * @param idBranche
     * @return CatEMHBranche
     */
    public CatEMHBranche createBrancheSpecifique(final CrueData metier, final int typebranche, final String idBranche)
            throws IOException {

        switch (typebranche) {

            case CrueIODico.BRANCHE_SAINT_VENANT:
                return createBrancheSaintVenant(metier, idBranche);
            case CrueIODico.BRANCHE_SEUIL_TRANSVERSAL:
                return createBrancheSeuilTransversal(metier, idBranche);
            case CrueIODico.BRANCHE_SEUIL_LONGITUDINALE:
                return createBrancheSeuilLateral(metier, idBranche);
            case CrueIODico.BRANCHE_PDC:
                return createBranchePdc(metier, idBranche);
            case CrueIODico.BRANCHE_STRICKLER:
                return createBrancheStrickler(metier, idBranche);
            case CrueIODico.BRANCHE_ORIFICE:
                return createBrancheOrifice(metier, idBranche);
            case CrueIODico.BRANCHE_BARRAGE_GENERIQUE:
                return createBrancheGenerique(metier, idBranche);
            case CrueIODico.BRANCHE_NIVEAU_ASSOCIE:
                return createBrancheNiveauAssocie(metier, idBranche);
            case CrueIODico.BRANCHE_BARRAGE_FIL_EAU:
                return createBrancheBarrageFilEau(metier, idBranche);

            default:
                return null;
        }
    }

    /**
     * branche strickler 6.
     *
     * @param metier
     * @param idBranche
     * @return la branche strickler
     */
    public EMHBrancheStrickler createBrancheStrickler(final CrueData metier, final String idBranche) throws IOException {
        final EMHBrancheStrickler branche = new EMHBrancheStrickler(idBranche);

        // -- lecture du contenu formatté branche saint venant --//
        lireSuite();
        String lineRead = in_.getLine();

        // -- la liste des distances Xp sont affichées avant les sections --//
        final List<Double> listeDistance = new ArrayList<>();
        // -- la liste des coef Cconv sont affichées avant les sections --//
        final List<Double> listeCconv = new ArrayList<>();
        // -- la liste des coef Cpond sont affichées avant les sections --//
        final List<Double> listeCpond = new ArrayList<>();
        // -- la liste des coef Cdiv sont affichées avant les sections --//
        final List<Double> listeCdiv = new ArrayList<>();

        // -- la donnée dcsp si existe --//
        // final DonCalcSansPrt dataDCSP = null;

        // -- la liste des sections branches --//
        final List<CatEMHSection> listeSectionBranche = new ArrayList<>();
        // -- tant qu'on est pas arrivé à une autre branche ou une autre definition, on lit les infos de la branche --//
        while (!isEof() && !CrueIODico.isAnewBrancheDefinition(lineRead) && CrueIODico.appartientBrancheCarte(lineRead)) {
            // -- recuperation de l'element lu en 1ere position --//
            final String typeLigne = in_.stringField(0);

            // -- remplissage des distances par rapport aux sections --//
            // ex: DISTANCE 100.00 100.00 100.00
            if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_DISTANCE)) {
                // -- peut y avoir 1 ou plusieurs coefficients --//
                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    listeDistance.add(in_.doubleField(i));
                }
            } else // -- gestion des sections profils --//
                // ex: PROF PROF6B PROF5 PROF4 PROF3A
                if (typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_PROF)
                        || typeLigne.toUpperCase().equals(CrueIODico.BRANCHE_NOMREF)) {
                    // -- peut y avoir 1 ou plusieurs coefficients --//
                    for (int i = 1; i < in_.getNumberOfFields(); i++) {
                        final String nomSection = in_.stringField(i);
                        final CatEMHSection profil = createInitSectionForBranche(metier, nomSection);
                        listeSectionBranche.add(profil);
                    }
                } else {
                    errorCarteNotRecognized(typeLigne);
                }

            // -- on lit la suite en passant les commentaires sans mot clef --//
            lireSuite();
            lineRead = in_.getLine();
        }
        analyzeDistanceInterProfil(idBranche, listeDistance);
        // -- on remplit avec les infos spécifiques de la branche --//
        completeBrancheWithInfosRead(metier, branche, listeDistance, listeCconv, listeCpond, listeCdiv, listeSectionBranche);
        return branche;
    }

    /**
     * Creer un casier avec les données lues entre la carte CASIER et la prochaine rencontrée (ou autre type de carte non CASIER).
     *
     * @param metier
     * @param idNoeud
     * @author Adrien Hadoux
     */
    public CatEMHCasier createCasier(final CrueData metier, final String idNoeud) throws IOException,
            CrueNotSupportedException {

        // -- on lit la suite --//
        lireSuite();

        final EMHCasierProfil newCasier = EMHFactory.createCasierProfil(
                CruePrefix.changePrefix(idNoeud, CruePrefix.P_NOEUD, CruePrefix.P_CASIER),
                metier.getCrueConfigMetier(), true);
        newCasier.setUserActive(true);
        CatEMHNoeud noeudRef = metier.findNoeudByReference(idNoeud);

        if (noeudRef == null) {
            noeudRef = metier.createNode(idNoeud);
            if (noeudRef == null) {
                analyze_.addSevereError("crue.noeud.notExist.forCasier", idNoeud, newCasier.getNom());
            } else {
                metier.add(noeudRef);
            }
        }
        if (noeudRef != null) {
            newCasier.setNoeud(noeudRef);
            //pour s'assurer que les nom sont bien égaux en prenant en compte la casse
            if (!noeudRef.getNom().equals(idNoeud)) {
                noeudRef.setNom(idNoeud);
            }
        }
        // on ajoute la valeur par défaut pour dpti:
        newCasier.addInfosEMH(new DonPrtCIniCasierProfil(metier.getCrueConfigMetier()));
        metier.add(newCasier);

        // -- listes des X/Z --//
        List<PtProfil> listeProfils = new ArrayList<>();

        // -- listes des indices X/Z qui constitueront les limites --//
        List<Integer> indiceLimiteJ = new ArrayList<>();

        // -- liste des abscisses des X/Z qui constituent les limites, presque comme limiteJ --//
        final List<Double> abscissesLimiteX = new ArrayList<>();
        // valeurs par defaut
        final DonCalcSansPrtCasierProfil dataDPTGQRuis = InfoEMHFactory.getDCSPCasierProfil(newCasier,
                metier.getCrueConfigMetier());
        newCasier.addInfosEMH(dataDPTGQRuis);
        DonPrtGeoProfilCasier dataDPTG = null;
        // double oldLong = 0;
        int idx = 0;
        while (!isEof() && !CrueIODico.isAnewCasierDefinition(in_.getLine())
                && CrueIODico.appartientCasiersCarte(in_.getLine())) {
            // -- recuperation de l'element lu en 1ere position --//
            final String element = in_.stringField(0);

            if (element.toUpperCase().equals(CrueIODico.RUIS)) {
                // -- donnee DPTI --//
                // ex: RUIS 0.000000
                final double qruis = in_.doubleField(1);
                dataDPTGQRuis.setCoefRuis(qruis);
            } else if (element.toUpperCase().equals(CrueIODico.SBATI)) {
                final DonPrtGeoBatiCasier batiCasier = EMHFactory.createDonPrtGeoBatiCasierCrue9(newCasier,
                        metier.getCrueConfigMetier());
                batiCasier.setSplanBati(in_.doubleField(1));
                batiCasier.setZBatiTotal(in_.doubleField(2));
                newCasier.addInfosEMH(batiCasier);
            } else if (element.toUpperCase().equals(CrueIODico.PROFCAS)) {

                // -- on finalise l'ancienne DPTG si on tombe sur une nouvelle --//
                if (dataDPTG != null) {
                    newCasier.addInfosEMH(dataDPTG);
                    // -- on enregistre les couples X/Z --//
                    dataDPTG.setPtProfil(listeProfils);

                    // - on recupere les limites de deb et fin --//
                    if (indiceLimiteJ.size() > 1 && indiceLimiteJ.get(0) < listeProfils.size()
                            && indiceLimiteJ.get(1) < listeProfils.size()) {
                        final PtProfil limDeb = listeProfils.get(indiceLimiteJ.get(0));
                        final PtProfil limFin = listeProfils.get(indiceLimiteJ.get(1));
                        final LitUtile lit = new LitUtile();
                        lit.setLimDeb(limDeb);
                        lit.setLimFin(limFin);
                        dataDPTG.setLitUtile(lit);
                    }

                    // -- on reinitialise --//
                    listeProfils = new ArrayList<>();
                    indiceLimiteJ = new ArrayList<>();
                }
                idx++;
                dataDPTG = EMHFactory.createDonPrtGeoProfilCasier(newCasier, idx, metier.getCrueConfigMetier());
                dataDPTG.setDistance(in_.doubleField(1));
            } else if (element.toUpperCase().equals(CrueIODico.XZ)) {
                // -- ajout des listes de profils --//
                // ex: X/Z 0.00 6.10 2.00 5.10 32.00 4.10 62.00 3.10
                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    final double x = in_.doubleField(i++);
                    final double z = in_.doubleField(i);
                    final PtProfil pt = new PtProfil(x, z);
                    listeProfils.add(pt);
                }
            } else if (element.toUpperCase().equals(CrueIODico.LIMITEJ)) {

                // ex: LIMITEJ 1 8
                // les indices des points limites, attention en fortran on commence a 1 et pas 0.

                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    final int val = in_.intField(i) - 1;
                    indiceLimiteJ.add(val);
                }
            } else if (element.toUpperCase().equals(CrueIODico.LIMITEX)) {

                // ex: LIMITEX(f) x1 x2 x3...
                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    final double val = in_.doubleField(i) - 1;
                    abscissesLimiteX.add(val);
                }
            } else {
                analyze_.addSevereError(CrueNotSupportedException.MSG_CARTE_TYPE_UNKNOWN, in_.getLineNumber());
            }

            // -- on lit la suite --//
            lireSuite();
        }

        // -- on remplit les infos avec les listes lues. --//
        if (dataDPTG != null) {
            newCasier.addInfosEMH(dataDPTG);

            // -- on enregistre les couples X/Z --//
            dataDPTG.setPtProfil(listeProfils);

            // - on recupere les limites de deb et fin --//
            if (indiceLimiteJ.size() > 1 && indiceLimiteJ.get(0) < listeProfils.size()
                    && indiceLimiteJ.get(1) < listeProfils.size()) {
                final PtProfil limDeb = listeProfils.get(indiceLimiteJ.get(0));
                final PtProfil limFin = listeProfils.get(indiceLimiteJ.get(1));
                final LitUtile lit = new LitUtile();
                lit.setLimDeb(limDeb);
                lit.setLimFin(limFin);
                dataDPTG.setLitUtile(lit);
            }
        }

        return newCasier;
    }

    /**
     * A utiliser lors de la lecture des branches: cree des sections temporaires et stockes les nomSxtio
     *
     * @param metier     le metier
     * @param nomSection le nom de la section
     * @return la section qui va bien
     */
    private CatEMHSection createInitSectionForBranche(final CrueData metier, final String nomSection) {
        CatEMHSection profil = metier.findSectionByReference(nomSection);
        if (profil == null) {
            this.usedNomSection.add(nomSection.toUpperCase());
            profil = new EMHSectionProfil(nomSection);
        }
        return profil;
    }

    private LoiFF createLoiQDz(final String idBranche) {
        final LoiFF newLoi = new LoiFF();
        newLoi.setNom(CruePrefix.addPrefixs(idBranche, EnumTypeLoi.LoiQDz, EnumCatEMH.BRANCHE));
        newLoi.setType(EnumTypeLoi.LoiQDz);
        newLoi.setEvolutionFF(new EvolutionFF(new ArrayList<>()));
        return newLoi;
    }

    private LoiFF createLoiQpilZam(final String idBranche) {
        final LoiFF newLoi = new LoiFF();
        newLoi.setNom(CruePrefix.addPrefixs(idBranche, EnumTypeLoi.LoiQpilZam, EnumCatEMH.BRANCHE));
        newLoi.setType(EnumTypeLoi.LoiQpilZam);
        newLoi.setEvolutionFF(new EvolutionFF(new ArrayList<>()));
        return newLoi;
    }

    private LoiFF createLoiZavZam(final String idBranche) {
        final LoiFF newLoi = new LoiFF();
        newLoi.setNom(CruePrefix.addPrefixs(idBranche, EnumTypeLoi.LoiZavZam, EnumCatEMH.BRANCHE));
        newLoi.setEvolutionFF(new EvolutionFF(new ArrayList<>()));
        newLoi.setType(EnumTypeLoi.LoiZavZam);
        return newLoi;
    }

    /**
     * Creer une section profile classique.
     *
     * @param metier
     * @param id
     * @return une section profil cree.
     * @author Adrien Hadoux
     */
    public CatEMHSection createSectionProfilClassique(final CrueData metier, final String id) throws IOException,
            CrueNotSupportedException {

        // -- on lit la suite --//
        lireSuite();

        // -- data qui seront referencées --//

        final String nomProfil = id;
        CatEMHSection sectionProfil = metier.findSectionByReference(nomProfil);
        if (sectionProfil == null) {

            sectionProfil = new EMHSectionProfil(nomProfil);
            metier.add(sectionProfil);
        }

        // -- liste des frottements qui contiendra le contenu des STRIC --//
        final List<DonFrt> listeFrottements = new ArrayList<>();
        // -- liste des lits, autant de frottements que de lits --//
        final List<LitNumerote> listeLits = new ArrayList<>();
        // -- listes des X/Z --//
        final List<PtProfil> listeProfils = new ArrayList<>();

        // -- listes des indices X/Z qui constitueront les limites --//
        final List<Integer> indiceLimiteJ = new ArrayList<>();

        // -- liste des abscisses des X/Z qui constituent les limites, presque comme limiteJ --//
        final List<Double> abscissesLimiteX = new ArrayList<>();

        // -- listes des indices des lits actifs. si vide tous actifs par defaut--//
        final List<Integer> listeLitsActifs = new ArrayList<>();

        DonPrtGeoProfilSectionFenteData fente = null;

        boolean strictFound = false;
        int nbFrottement = 0;
        int nbLit = 0;

        while (!isEof() && !CrueIODico.isAnewProfilDefinition(in_.getLine())
                && CrueIODico.appartientProfilsCarte(in_.getLine())) {
            // -- recuperation de l'element lu en 1ere position --//
            final String element = in_.stringField(0);

            if (element.toUpperCase().equals(CrueIODico.LIT)) {
                // -- format lit: Nom du lit Nommé, et indices des lits qui contiennent ce nom --//
                // LIT MAJD 2
                // LIT MINEUR 3
                // LIT MAJG 4
                // LIT STOCKAGE 1 5
                final String nomLitNomme = in_.stringField(1);
                // -- recuperation des indices des noeuds correspondants --//
                for (int i = 2; i < in_.getNumberOfFields(); i++) {
                    // -- lecture de l'indice du lit: attention il faut enlever 1 car en fortran --//
                    final int indiceLit = in_.intField(i) - 1;
                    nbLit = Math.max(nbLit, indiceLit + 1);//le nombre de lit est égal au max des indices +1 trouvé
                    if (indiceLit >= listeLits.size()) {
                        // -- il faut créer autant de LitNumerote pour arriver à l'indice, cas ou frottements Strickler non présents
                        // --//
                        while (indiceLit >= listeLits.size()) {
                            listeLits.add(new LitNumerote());
                        }
                    }
                    final LitNumerote lit = listeLits.get(indiceLit);
                    final LitNomme nomme = new LitNomme(nomLitNomme, 0);
                    lit.setNomLit(nomme);

                    // -- cas particulier: pour lit mineurs, il faut renommer en MINEUR --//
                    lit.setIsLitMineur(isLitMineur(nomLitNomme));
                }
            } else if (element.toUpperCase().equals(CrueIODico.XZ)) {
                // -- ajout des listes de profils --//
                // ex: X/Z 0.00 6.10 2.00 5.10 32.00 4.10 62.00 3.10
                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    final double x = in_.doubleField(i++);
                    final double z = in_.doubleField(i);
                    final PtProfil pt = new PtProfil(x, z);
                    listeProfils.add(pt);
                }
            } else if (element.toUpperCase().equals(CrueIODico.DVERS)) {
                if (!warnDoneForDvers) {
                    warnDoneForDvers = true;
                    analyze_.addError("dc.dvers.ignored", in_.getLineNumber(), id);
                }
            } else if (element.toUpperCase().equals(CrueIODico.DIGUE)) {
                if (!warnDoneForDigue) {
                    warnDoneForDigue = true;
                    analyze_.addError("dc.digue.ignored", in_.getLineNumber(), id);
                }
            } else if (element.toUpperCase().equals(CrueIODico.FENTE)) {
                fente = new DonPrtGeoProfilSectionFenteData(metier.getCrueConfigMetier());
                fente.setLargeurFente(in_.doubleField(1));
                fente.setProfondeurFente(in_.doubleField(2));
            } else if (element.toUpperCase().equals(CrueIODico.STRIC)) {
                strictFound = true;
                // -- liste des frottements associés au DPTG --//
                // ex: STRIC K0 PROF3BMAJ PROF3BMIN PROF3BMAJ K0
                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    final String reference = in_.stringField(i);
                    // -- on recherche la reference du frottement sinon on ajoute --//
                    DonFrt frottements = null;
                    nbFrottement++;
                    if (metier.getFrottements() != null) {
                        frottements = EMHHelper.selectDFRTByReference(reference, metier.getFrottements());
                    }
                    // -- ajout du frottements associé au profil --//
                    if (frottements != null) {
                        listeFrottements.add(frottements);
                        // -- autant de lits que de frottements --//
                        while (listeLits.size() < listeFrottements.size()) {
                            listeLits.add(new LitNumerote());
                        }
                    }
                }
            } else if (element.toUpperCase().equals(CrueIODico.LIMITEJ)) {

                // ex: LIMITEJ 1 8
                // les indices des points limites, attention en fortran on commence a 1 et pas 0.

                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    final int val = in_.intField(i) - 1;
                    indiceLimiteJ.add(val);
                }
            } else if (element.toUpperCase().equals(CrueIODico.LIMITEX)) {

                // ex: LIMITEX(f) x1 x2 x3...
                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    final double val = in_.doubleField(i) - 1;
                    abscissesLimiteX.add(val);
                }
            } else if (element.toUpperCase().equals(CrueIODico.ACTIF)) {
                // ex: ACTIF 2 3 4
                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    final int val = in_.intField(i) - 1;
                    listeLitsActifs.add(val);
                }
            } else {
                analyze_.addSevereError(CrueNotSupportedException.MSG_CARTE_TYPE_UNKNOWN, in_.getLineNumber());
            }

            // -- on lit la suite --//
            lireSuite();
        }
        if (!strictFound) {
            analyze_.addSevereError("dc.stricCarteNotFound", in_.getLineNumber(), nomProfil);
        } else {
            final int limitjSize = indiceLimiteJ.size();
            if (limitjSize > 0 && limitjSize != listeLits.size() + 1) {
                analyze_.addSevereError("dc.limitJ.wrongSize", in_.getLineNumber(), nomProfil, Integer.toString(listeLits.size() + 1));
            }
            if (nbLit > 0 && nbLit > nbFrottement) {
                analyze_.addSevereError("dc.lit.wrongSize", in_.getLineNumber(), nomProfil, nbLit, nbFrottement);
            }

            for (final Integer idx : listeLitsActifs) {
                if (idx.intValue() >= nbFrottement) {
                    analyze_.addSevereError("dc.active.wrongActive", in_.getLineNumber(), nomProfil, idx.intValue() + 1, nbLit);
                }
            }
        }

        // -- on remplit les infos DPTG de la section profil avec les objets lues précédemment --//
        completeProfilWithInfosRead(sectionProfil, listeFrottements, listeLits, listeProfils, indiceLimiteJ,
                abscissesLimiteX, listeLitsActifs, fente);

        return sectionProfil;
    }

    /**
     * Creer un profil de reference.
     *
     * @param metier
     * @param id
     * @author Adrien Hadoux
     */
    public CatEMHSection createSectionProfilReference(final CrueData metier, final String id) throws IOException {
        // -- on lit la suite --//
        lireSuite();
        // -- data qui seront referencées --//

        CatEMHSection sectionProfil = null;
        final String nomProfil = id;
        sectionProfil = metier.findSectionByReference(nomProfil);
        if (sectionProfil == null) {
            sectionProfil = new EMHSectionProfil(nomProfil);
            metier.add(sectionProfil);
        }

        // -- liste des frottements qui contiendra le contenu des STRIC --//
        final List<DonFrt> listeFrottements = new ArrayList<>();
        // -- liste des lits, autant de frottements que de lits --//
        final List<LitNumerote> listeLits = new ArrayList<>();
        // -- listes des X/Z --//
        final List<PtProfil> listeProfils = new ArrayList<>();
        // -- listes des indices X/Z qui constitueront les limites --//
        final List<Integer> indiceLimiteJ = new ArrayList<>();

        // -- listes des indices des lits actifs. si vide tous actifs par defaut--//
        final List<Integer> listeLitsActifs = new ArrayList<>();

        // -- liste des abscisses des X/Z qui constituent les limites, presque comme limiteJ --//
        final List<Double> abscissesLimiteX = new ArrayList<>();

        while (!isEof() && !CrueIODico.isAnewProfilDefinition(in_.getLine())
                && CrueIODico.appartientProfilsCarte(in_.getLine())) {
            // -- recuperation de l'element lu en 1ere position --//
            final String element = in_.stringField(0);

            if (element.toUpperCase().equals(CrueIODico.XZ)) {
                // -- ajout des listes de profils --//
                // ex: X/Z 0.00 6.10 2.00 5.10 32.00 4.10 62.00 3.10
                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    final double x = in_.doubleField(i++);
                    final double z = in_.doubleField(i);
                    final PtProfil pt = new PtProfil(x, z);
                    listeProfils.add(pt);
                }
            } else {
                analyze_.addSevereError(CrueNotSupportedException.MSG_CARTE_TYPE_UNKNOWN, in_.getLineNumber());
            }

            // -- on lit la suite --//
            lireSuite();
        }

        // -- on remplit les infos DPTG de la section profil avec les objets lues précédemment --//
        completeProfilWithInfosRead(sectionProfil, listeFrottements, listeLits, listeProfils, indiceLimiteJ,
                abscissesLimiteX, listeLitsActifs, null);

        return sectionProfil;
    }

    private List<PtEvolutionFF> getLoiQDz(final String idBranche, final DonCalcSansPrtBrancheBarrageGenerique dataDCSP) {
        LoiFF loiQDZ = dataDCSP.getRegimeNoye();
        if (loiQDZ == null) {
            loiQDZ = createLoiQDz(idBranche);
            dataDCSP.setRegimeNoye(loiQDZ);
        }
        return loiQDZ.getEvolutionFF().getPtEvolutionFF();
    }

    private List<PtEvolutionFF> getLoiQZam(final String idBranche, final DonCalcSansPrtBrancheBarrageGenerique dataDCSP) {
        LoiFF loiQZam = dataDCSP.getRegimeDenoye();
        if (loiQZam == null) {
            loiQZam = createLoiQpilZam(idBranche);
            dataDCSP.setRegimeDenoye(loiQZam);
        }
        return dataDCSP.getRegimeDenoye().getEvolutionFF().getPtEvolutionFF();
    }

    private List<PtEvolutionFF> getLoiZavZam(final String idBranche, final DonCalcSansPrtBrancheNiveauxAssocies dataDCSP) {
        if (dataDCSP.getZasso() == null) {
            final LoiFF newLoi = createLoiZavZam(idBranche);
            dataDCSP.setZasso(newLoi);
        }
        final List<PtEvolutionFF> ptEvolutionFF = dataDCSP.getZasso().getEvolutionFF().getPtEvolutionFF();
        return ptEvolutionFF;
    }

    @Override
    protected CrueIOResu<CrueData> internalRead() {
        enumSensOrificeMapCrue9 = EnumsConverter.createCrue9EnumConverter(EnumSensOrifice.class, null);
        enumFormulePdcMapCrue9 = EnumsConverter.createCrue9EnumConverter(EnumFormulePdc.class, null);

        return readFile();
    }

    /**
     * Lit la partie générale d'une branche. Ligne du type BRANCHE B2 N2 N3 20 passe ensuite le relais a la lecture spécifique de branche en fonction du
     * type (dernier elt).
     *
     * @param metier
     */
    public void readBrancheGeneral(final CrueData metier) throws IOException {

        final int nbFieldsBranche = in_.getNumberOfFields();
        if (nbFieldsBranche >= 5) {
            final String nomBranche = in_.stringField(1);

            // -- creation des noeuds amont et avals --//
            final String nomNoeudAmont = in_.stringField(2);
            CatEMHNoeud noeudAmont = metier.findNoeudByReference(nomNoeudAmont);
            if (noeudAmont == null) {
                noeudAmont = metier.createNode(nomNoeudAmont);
                metier.add(noeudAmont);
            }
            final String nomNoeudAval = in_.stringField(3);
            CatEMHNoeud noeudAval = metier.findNoeudByReference(nomNoeudAval);
            if (noeudAval == null) {
                noeudAval = metier.createNode(nomNoeudAval);
                metier.add(noeudAval);
            }

            // -- creation de la branche en fonction de son type --//
            CatEMHBranche branche = null;
            final int typeBranche = in_.intField(4);
            try {
                branche = createBrancheSpecifique(metier, typeBranche, nomBranche);
                // -- ajout des relations des noeuds amont et avals --//
                if (branche != null) {
                    branche.setNoeudAmont(noeudAmont);
                    branche.setNoeudAval(noeudAval);
                    branche.setUserActive(true);

                    metier.add(branche);
                } else {
                    analyze_.addError(addErrorForOldBranche(typeBranche, nomBranche), in_.getLineNumber(), typeBranche,
                            nomBranche);
                    in_.readFields();
                }
            } catch (final EOFException eofe) {
                if (branche != null) {
                    branche.setNoeudAmont(noeudAmont);
                    branche.setNoeudAval(noeudAval);
                    branche.setUserActive(true);

                    metier.add(branche);
                }
            } catch (final Exception e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, "readBrancheGeneral", e);
                analyze_.manageExceptionFromFile(e, "io.dc.error", in_.getLineNumber());
                // -- on continue pour les autres branches --//
            }
        } else {
            analyze_.addSevereError("io.dc.brancheMalFormatee.error", in_.getLineNumber());
        }
    }

    /**
     * Lit les branches du fortran reader.
     *
     * @param metier
     */
    public void readBranches(final CrueData metier) throws IOException {

        // -- tant qu'on ne revient pas a une nouvelle définition--//
        while (!isEof() && CrueIODico.appartientBrancheCarte(in_.getLine())) {

            // -- analyse du premier element de la ligne pour gerer la bonne lecture --//
            final String element = in_.stringField(0);

            if (element.toUpperCase().equals(CrueIODico.BRANCHE_TYPE)) {
                readBrancheGeneral(metier);
            } else {
                in_.readFields();
            }
        }
    }

    /**
     * Lit les casiers
     *
     * @param metier
     */
    public void readCasiers(final CrueData metier) throws IOException, CrueNotSupportedException {

        while (!isEof() && CrueIODico.appartientCasiersCarte(in_.getLine())) {
            // -- recuperation de l'element lu en 1ere position --//
            final String element = in_.stringField(0);

            if (element.toUpperCase().equals(CrueIODico.CASIER_TYPE)) {
                // ex CASIER N6
                final String nom = in_.stringField(1);
        /*
         * final CatEMHCasier casier =
         */
                createCasier(metier, nom);
            } else {
                analyze_.addSevereError(CrueNotSupportedException.MSG_CARTE_TYPE_UNKNOWN, in_.getLineNumber());
                lireSuite();
            }
        }
    }

    /**
     * @return Object l'objet Cruedata remplit avec DC.
     * @author Adrien Hadoux
     */
    public CrueIOResu<CrueData> readFile() {
        if (super.in_ == null) {
            CrueHelper.emhEmpty("io.dc.fileEmpty.error", analyze_);
            return null;
        }

        // -- on initialise le reader frotran avec les lignes commentaires par exemple --//
        initReader();
        final CrueData metier = dataLinked;
        final CrueIOResu<CrueData> resu = new CrueIOResu<>(metier);
        try {

            // -- lecture du header --//
            readHeader(resu);

            while (!eof) {
                final String element = in_.stringField(0);

                if (element.toUpperCase().equals(CrueIODico.NOEUD)) {
                    // -- lecture des Noeuds --//
                    readNoeuds(metier);
                } else if (in_.getNumberOfFields() > 0 && CrueIODico.appartientReglesCarte(in_.stringField(0))) {
                    // -- lecture des regles --//
                    readRegles(metier);
                } else if (CrueIODico.isAnewBrancheDefinition(in_.getLine())) {
                    // -- lecture des branches --//
                    readBranches(metier);
                } else // -- lecture des frottements DFRT --//
                    if (CrueIODico.appartientFrottementsCarte(in_.getLine())) {
                        readFrottements(metier);
                    } else if (CrueIODico.isAnewProfilDefinition(in_.getLine())) {
                        readProfils(metier);
                    } else if (CrueIODico.isAnewCasierDefinition(in_.getLine())) {
                        // -- lecture des casiers DRSO,DPTI,DPTG,DCSP --//

                        readCasiers(metier);
                    } else {
                        if (StringUtils.isNotBlank(element)) {
                            analyze_.addSevereError("io.dc.carteNotRecognized", in_.getLineNumber(), element);
                        }
                        lireSuite();
                    }
            }
        } catch (final EOFException Exc) {
            // -- fin de fichier, on passe toujours par la. --//
        } catch (final IOException e) {
            analyze_.manageException(e);
        } catch (final CrueNotSupportedException e) {
            analyze_.manageException(e);
            return null;
        } catch (final NumberFormatException e) {
            analyze_.addSevereError("io.dc.decimalFormatError", in_.getLineNumber());
            resu.getMetier();
        }

        validateReadValues(metier);
        resu.getMetier().getOrCreateOPTG();
        return resu;
    }

    private void validateReadValues(final CrueData metier) {
        // finalement on remet les sectionIdem en place en recherchant les références.
        if (sectionIdemWithNoRef != null) {
            for (final Entry<EMHSectionIdem, String> it : sectionIdemWithNoRef.entrySet()) {
                final CatEMHSection ref = metier.findSectionByReference(it.getValue());
                it.getKey().setSectionRef(ref);
                if (ref == null) {
                    analyze_.addSevereError("io.dc.refFroSectionIdemNotFound", it.getKey().getNom(), it.getValue());
                }
            }
        }

        // erreur si un profil utilisés n'est pas défini
        final Collection usedButNotCreationProfils = ListUtils.removeAll(usedNomSection, createdNomSection);
        if (!usedButNotCreationProfils.isEmpty()) {
            analyze_.addSevereError(usedButNotCreationProfils.size() == 1 ? "io.dc.sectionUsedButNotExist.one"
                    : "io.dc.sectionUsedButNotExist.multi", StringUtils.join(usedButNotCreationProfils, "; "));
        }
        final Collection createdButNotUsed = ListUtils.removeAll(createdNomSection, usedNomSection);
        if (!createdButNotUsed.isEmpty()) {
            analyze_.addWarn(createdButNotUsed.size() == 1 ? "io.dc.sectionCreatedButNotUsed.one"
                    : "io.dc.sectionCreatedButNotUsed.multi", StringUtils.join(createdButNotUsed, "; "));
        }
        checkCasier(metier);
        if (this.definedNodes != null) {
            final Set<String> notDefinedNode = new HashSet<>();
            final List<CatEMHBranche> branches = metier.getBranches();
            for (final CatEMHBranche catEMHBranche : branches) {
                if (catEMHBranche.getUserActive()) {
                    String id = catEMHBranche.getNoeudAmont().getId();
                    if (!definedNodes.contains(id)) {
                        notDefinedNode.add(id);
                    }
                    id = catEMHBranche.getNoeudAval().getId();
                    if (!definedNodes.contains(id)) {
                        notDefinedNode.add(id);
                    }
                }
            }
            if (!notDefinedNode.isEmpty()) {
                final ArrayList<String> listeTriee = new ArrayList<>(notDefinedNode);
                Collections.sort(listeTriee);
                analyze_.addWarn("dc.carteNodes.nodesNotDefined", StringUtils.join(listeTriee, "; "));
            }
        }
    }

    /**
     * Lit les frottements
     *
     * @param metier
     * @author Adrien Hadoux
     */
    public void readFrottements(final CrueData metier) throws CrueNotSupportedException, IOException {
        metier.setFrottements(new ArrayList<>());
        // permet de gérer les strickler sur plusieurs lignes.
        final Map<String, DonFrtStrickler> done = new HashMap<>();
        final PropertyEpsilon eps = metier.getCrueConfigMetier().getConfLoi().get(EnumTypeLoi.LoiZFK).getVarOrdonnee().getEpsilon();
        while (!isEof() && CrueIODico.appartientFrottementsCarte(in_.getLine())) {

            // -- si on est un noeud --//
            final String element = in_.stringField(0);

            if (element.toUpperCase().equals(CrueIODico.STRIREF)) {
                // strickler constant
                // STRIREF(o) "nom_strickler" k (nom du strickler et valeur associée)
                final String nomFrot = in_.stringField(1);
                final double value = in_.doubleField(2);
                DonFrtStrickler frt = null;
                //voir CRUE-214: strickler nulle-> FkSto
                if (eps.isZero(value)) {
                    frt = EMHFactory.createDonFrtZFkSto(nomFrot);
                } else {
                    frt = EMHFactory.createDonFrtZFk(nomFrot);
                }

                // on met a zero le x
                frt.getLoi().getEvolutionFF().getPtEvolutionFF().add(new PtEvolutionFF(0, value));
                metier.getFrottements().addFrt(frt);
            } else if (element.toUpperCase().equals(CrueIODico.STRIREFH)) {
                throw new CrueNotSupportedException(CrueIODico.STRIREFH, in_.getLineNumber());
            } else if (element.toUpperCase().equals(CrueIODico.STRIREFZ)) {
                // STRIREFH(o) "nom_strickler" h1 k1 h2 k2... (nom du strickler, et couples h/k, h étant la hauteur par rapport
                // au point le plus bas du profil)
                final String nomFrot = in_.stringField(1);
                DonFrtStrickler frt = done.get(nomFrot.toUpperCase());
                if (frt == null) {
                    frt = EMHFactory.createDonFrtZFk(nomFrot);
                    done.put(nomFrot.toUpperCase(), frt);
                }
                final List<PtEvolutionFF> list = frt.getStrickler().getEvolutionFF().getPtEvolutionFF();
                for (int i = 2; i < in_.getNumberOfFields(); i++) {
                    final double valX = in_.doubleField(i++);
                    final double valY = in_.doubleField(i);
                    list.add(new PtEvolutionFF(valX, valY));
                }
                //demande CRUE-246
                if (metier.getFrottements().isEmptyFrt(frt, metier.getCrueConfigMetier())) {
                    frt.getLoi().setType(EnumTypeLoi.LoiZFKSto);
                }
                metier.getFrottements().addFrt(frt);
            } else {
                analyze_.addSevereError(CrueNotSupportedException.MSG_CARTE_TYPE_UNKNOWN, in_.getLineNumber());
            }

            // -- on lit la suite --//
            lireSuite();
        }
    }

    /**
     * Lit l'header du fichier fortran. 5 lignes de titre Commun a DC et DH.
     */
    @Override
    public void readHeader(final CrueIOResu<CrueData> res) throws IOException {

        final StringBuilder comm = new StringBuilder(420);
        boolean readSuite = true;
        for (int i = 0; i < AbstractCrue9Reader.NB_LINES_TITLE; i++) {
            final String readAndAvoidLabel = readAndAvoidLabel(in_).trim();
            if (readAndAvoidLabel.startsWith(CrueIODico.TITRE)) {
                if (i > 0) {
                    comm.append('\n');
                }
                comm.append(StringUtils.removeStart(in_.getLine(), CrueIODico.TITRE).trim());
            } else {
                readSuite = false;
                break;
            }
        }
        res.setCrueCommentaire(comm.toString());
        if (readSuite) {
            // -- on lit la suite --//
            lireSuite();
        }
    }

    /**
     * Lit la partie des noeuds du fichier DC.
     *
     * @param metier données métier.
     * @throws IOException exception jetee.
     */
    public void readNoeuds(final CrueData metier) throws IOException {

        while (!isEof() && CrueIODico.appartientNoeudsCarte(in_.getLine())) {

            // -- si on est un noeud --//
            final String element = in_.stringField(0);

            if (element.toUpperCase().equals(CrueIODico.NOEUD)) {
                // ex: NOEUD(f) "n1" "n22 … (liste des noeuds dans l'ordre amont aval)
                for (int i = 1; i < in_.getNumberOfFields(); i++) {
                    final String nomNoeud = in_.stringField(i);
                    CatEMHNoeud noeud = metier.findNoeudByReference(nomNoeud);
                    if (noeud == null) {
                        noeud = metier.createNode(nomNoeud);
                        metier.add(noeud);
                    }
                    if (definedNodes == null) {
                        definedNodes = new HashSet<>();
                    }
                    definedNodes.add(noeud.getId());
                }
            } else {
                analyze_.addSevereError(CrueNotSupportedException.MSG_CARTE_TYPE_UNKNOWN, in_.getLineNumber());
            }
            // -- on lit la suite --//
            lireSuite();
        }
    }

    /**
     * Lit les profils
     *
     * @param metier
     * @author Adrien Hadoux
     */
    public void readProfils(final CrueData metier) throws IOException, CrueNotSupportedException {

        while (!isEof() && CrueIODico.appartientProfilsCarte(in_.getLine())) {
            CatEMHSection sectionCree = null;
            // -- recuperation de l'element lu en 1ere position --//
            final String element = in_.stringField(0);
            final String nomProfil = in_.stringField(1);
            final String upperCase = element.toUpperCase();
            createdNomSection.add(nomProfil.toUpperCase());
            if (upperCase.equals(CrueIODico.PROFIL)) {

                sectionCree = createSectionProfilClassique(metier, nomProfil);
            } else if (upperCase.equals(CrueIODico.PROFREF)) {
                // ex: PROFREF(o) "nom_prof_ref"
                sectionCree = createSectionProfilReference(metier, nomProfil);
            } else if (upperCase.equals(CrueIODico.PROFIDEM)) {
                // -- section avec reference vers une autre section et une donnee DPTG avec un deltaZ changeant --//
                // ex PROFIDEM(o) "nom_profil" "nom_prof_appelé" [deltaz]
                final String nomReference = in_.stringField(2);
                final double deltaZ = in_.doubleField(3);

                // -- il faut vérifier que cette section n'a pas deja été crée sous forme de section profil dans les donneés des
                // branches --//
                final CatEMHSection sectionToreplace = metier.findSectionByReference(nomProfil);
                final EMHSectionIdem newSection = new EMHSectionIdem(nomProfil);
                if (sectionToreplace != null) {
                    replaceSection(metier, sectionToreplace, newSection);
                } else {
                    metier.add(newSection);
                }
                // une section qui sert de support est utilisee.
                this.usedNomSection.add(nomReference.toUpperCase());
                // on met les ref dans cette map temporaire.
                sectionIdemWithNoRef.put(newSection, nomReference);

                final DonPrtGeoSectionIdem dataDPTG = new DonPrtGeoSectionIdem(metier.getCrueConfigMetier());
                dataDPTG.setDz(deltaZ);
                newSection.addInfosEMH(dataDPTG);

                sectionCree = newSection;
                lireSuite();
            } else if (upperCase.equals(CrueIODico.PROFINT)) {

                final CatEMHSection sectionToreplace = metier.findSectionByReference(nomProfil);

                // -- il faut transformer cette section en interpole: on le fait maintenant car dans la methode cela ne change
                // rien
                // car c'est au niveau de la branche que l'on cree les sections.

                sectionCree = new EMHSectionInterpolee(nomProfil);
                if (sectionToreplace == null) {
                    metier.add(sectionCree);
                } else {
                    replaceSection(metier, sectionToreplace, sectionCree);
                }
                lireSuite();
            } else if (upperCase.equals(CrueIODico.PROFRECT)) {
                throw new CrueNotSupportedException(element, in_.getLineNumber());
            } else if (upperCase.equals(CrueIODico.PROFTRAP)) {
                throw new CrueNotSupportedException(element, in_.getLineNumber());
            } else {
                in_.readFields();
            }

            if (sectionCree == null) {
                analyze_.addSevereError("io.global.cantCreateProfil.error", in_.getLineNumber());
            }
        } // fin du while.
    }

    private boolean isCurrentLigneActive() {
        return (in_.getNumberOfFields() <= 1 || !CrueIODico.REGLE_INACTIVE.equals(in_.stringField(1)));
    }

    /**
     * Lit les regles du debut de fichier
     *
     * @param metier
     */
    public void readRegles(final CrueData metier) throws IOException {

        final OrdPrtGeoModeleBase reglesOPTG = metier.getOrCreateOPTG();
        while (!isEof() && CrueIODico.appartientReglesCarte(in_.stringField(0))) {
            // -- analyse du premier element de la ligne pour gerer la bonne lecture --//
            final String element = in_.stringField(0);
            final String upperCase = element.toUpperCase();
            metier.getOrCreatePNUM();
            if (upperCase.equals(CrueIODico.ZREF)) {
                // Ce paramètre numérique, de valeur par défaut 0.0 m, correspond à une altitude que l’utilisateur peut donner
                // s’il veut modifier la référence altimétrique dans les calculs.Dans Crue10, il est positionné dans PNUM.
                metier.getPNUM().setZref(CrueIODico.getCrue10Value(in_.doubleField(1), getPropertyDefinition("zref")));
            } else if (upperCase.equals(CrueIODico.COEFF)) {
                final String isorti = in_.stringField(3);
                metier.getOrCreateOPTR();
                metier.getOrCreateOPTG();
                metier.getOrCreateOPTI();
                if (!"0".equals(isorti)) {
                    metier.getOrCreateOPTR().getSorties().modifyVerbositeForAll(SeveriteManager.DEFAULT_DEBUG_LEVEL_FOR_CRUE9);
                    metier.getOrCreateOPTG().getSorties().modifyVerbositeForAll(SeveriteManager.DEFAULT_DEBUG_LEVEL_FOR_CRUE9);
                    metier.getOrCreateOPTI().getSorties().modifyVerbositeForAll(SeveriteManager.DEFAULT_DEBUG_LEVEL_FOR_CRUE9);
                }
                if (metier.getPNUM().getParamNumCalcTrans() == null) {
                    metier.getPNUM().setParamNumCalcTrans(new ParamNumCalcTrans(metier.getCrueConfigMetier()));
                }
                metier.getPNUM().getParamNumCalcTrans().setThetaPreissmann(CrueIODico.getCrue10Value(in_.doubleField(2),
                        getPropertyDefinition(
                                "thetaPreissmann")));
            } else if (upperCase.equals(CrueIODico.R_SLARGE)) {
                final Regle regle = reglesOPTG.getRegle(EnumRegle.LARG_SEUIL);
                regle.setActive(isCurrentLigneActive());
                if (regle.isActive()) {
                    regle.setSeuilDetect(CrueIODico.getCrue10Value(in_.doubleField(1), getPropertyDefinition("regleLargSeuil")));
                }
            } else if (upperCase.equals(CrueIODico.R_PRPLAT)) {

                final Regle regle = reglesOPTG.getRegle(EnumRegle.PROF_PLAT);
                regle.setActive(isCurrentLigneActive());
                if (regle.isActive()) {
                    final ItemVariable variableProfPlat = getPropertyDefinition("regleProfPlat");
                    final double crue10Val = CrueIODico.getCrue10Value(in_.doubleField(1), variableProfPlat);
                    if (variableProfPlat.isInfiniPositif(crue10Val)) {
                        regle.setSeuilDetect(crue10Val);
                    } else {
                        regle.setSeuilDetect(crue10Val / 100);
                    }
                }
            } else if (upperCase.equals(CrueIODico.R_RUPENT)) {
                final Regle regle = reglesOPTG.getRegle(EnumRegle.PENTE_RUPTURE);
                regle.setActive(isCurrentLigneActive());
                if (regle.isActive()) {
                    regle.setSeuilDetect(CrueIODico.getCrue10Value(in_.doubleField(1), getPropertyDefinition("reglePenteRupture")));
                }
            } else if (upperCase.equals(CrueIODico.R_DECAL)) {
                final Regle regle = reglesOPTG.getRegle(EnumRegle.DECAL);
                regle.setActive(isCurrentLigneActive());
                if (regle.isActive()) {
                    regle.setSeuilDetect(CrueIODico.getCrue10Value(in_.doubleField(1), getPropertyDefinition("regleDecal")));
                }
            } else if (upperCase.equals(CrueIODico.R_REBDEB)) {
                final Regle regle = reglesOPTG.getRegle(EnumRegle.REB_DEB);
                regle.setActive(isCurrentLigneActive());
            } else if (upperCase.equals(CrueIODico.R_DXMAX)) {
                final Regle regle = reglesOPTG.getRegle(EnumRegle.PDX_MAX);
                regle.setActive(isCurrentLigneActive());
                if (regle.isActive()) {
                    regle.setSeuilDetect(CrueIODico.getCrue10Value(in_.doubleField(1), getPropertyDefinition("reglePdxMax")));
                }
            } else if (upperCase.equals(CrueIODico.R_VDXMAX)) {
                final Regle regle = reglesOPTG.getRegle(EnumRegle.VAR_PDX_MAX);
                regle.setActive(isCurrentLigneActive());
                if (regle.isActive()) {
                    regle.setValParam(new ValParamEntier(regle.getValParamNom(), (int) in_.doubleField(1)));
                }
            } else if (upperCase.equals(CrueIODico.R_PENMAX)) {
                final Regle regle = reglesOPTG.getRegle(EnumRegle.PENTE_MAX);
                regle.setActive(isCurrentLigneActive());
                if (regle.isActive()) {
                    final ItemVariable variablePenteMax = getPropertyDefinition("reglePenteMax");
                    final double crue10Val = CrueIODico.getCrue10Value(in_.doubleField(1), variablePenteMax);
                    if (variablePenteMax.isInfiniPositif(crue10Val)) {
                        regle.setSeuilDetect(crue10Val);
                    } else {
                        regle.setSeuilDetect(crue10Val / 100);
                    }
                }
            } else {
                analyze_.addSevereError(CrueNotSupportedException.MSG_CARTE_TYPE_UNKNOWN, in_.getLineNumber());
            }

            // -- on lit la suite --//
            lireSuite();
        }
    }

    private ItemVariable getPropertyDefinition(final String prop) {
        return getDataLinked().getCrueConfigMetier().getProperty(prop);
    }

    private void replaceSection(final CrueData metier, final CatEMHSection sectionToreplace,
                                final CatEMHSection newSection) {
        if (sectionToreplace != null) {
            final List<RelationEMH> relationContenant = EMHHelper.collectDeepRelationEMHsByRef(metier.getSousModele(),
                    sectionToreplace.getNom());
            if (relationContenant != null) {
                for (final RelationEMH relationEMH : relationContenant) {
                    if (relationEMH.getEmh().getCatType().equals(EnumCatEMH.SECTION)) {
                        relationEMH.setEmh(newSection);
                    }
                }
            }
            newSection.addRelationEMH(new RelationEMHDansSousModele(metier.getSousModele()));
            // on met a jour les relations.
            final Collection<RelationEMHBrancheContientSection> findRelationOfType = EMHHelper.selectRelationOfType(
                    sectionToreplace, RelationEMHBrancheContientSection.class);
            for (final RelationEMHBrancheContientSection relationToChange : findRelationOfType) {
                newSection.addRelationEMH(relationToChange);
            }
        }
    }

    @Override
    public void stop() {
    }
}
