/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.conf;

import com.thoughtworks.xstream.XStream;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueDaoStructure;

/**
 * @author CANEL Christophe
 */
public class CrueDaoStructureCONF implements CrueDaoStructure {

  /**
   * {@inheritDoc}
   */
  @Override
  public void configureXStream(final XStream xstream, final CtuluLog analyze) {
    xstream.alias("Configuration", CrueDaoCONF.class);

    this.configureSiteAideXStream(xstream);
    this.configureCoeurXStream(xstream);
    this.configureSiteOptionXStream(xstream);
    this.configureUserOptionXStream(xstream);
  }

  public void configureCoeurXStream(final XStream xstream) {
    xstream.alias("Coeur", Coeur.class);
    xstream.useAttributeFor(Coeur.class, "id");
  }

  public void configureSiteOptionXStream(final XStream xstream) {
    xstream.alias("SiteOption", SiteOption.class);
    xstream.useAttributeFor(SiteOption.class, "UserVisible");
  }

  public void configureSiteAideXStream(final XStream xstream) {
    xstream.alias("SiteAide", SiteAide.class);       
    xstream.useAttributeFor(SiteAide.class, "Type");
  }
  
  public void configureUserOptionXStream(final XStream xstream) {
    xstream.alias("UserOption", UserOption.class);
    xstream.useAttributeFor(UserOption.class, "Nom");
  }

  protected static class UserOption {

    public String Nom;
    public String Commentaire;
    public String Valeur;
  }

  protected static class SiteOption extends UserOption {

    public boolean UserVisible;
  }

  protected static class SiteAide {

    public String Commentaire;
    public boolean SyDocActivation;
    public String CheminBase;
    public String Type;
  }
  
  protected static class Site {

    public final List<Coeur> Coeurs = new ArrayList<>();
    public final List<SiteOption> SiteOptions = new ArrayList<>();
    public final SiteAide SiteAide = new SiteAide();
  }

  protected static class Coeur {

    public String id;
    public String Commentaire;
    public String VersionGrammaire;
    public boolean Crue9Dependant = true;
    public boolean CoeurParDefaut;
    public String DossierCoeur;
  }

  protected static class User {

    public List<UserOption> UserOptions = new ArrayList<>();
  }
}
