/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.TypeEMHDao;

/**
 *
 * @author deniger
 */
public class CommonResDaoXstream {

  public static void configureXstream(final XStream xstream) {
    xstream.useAttributeFor(CommonResDao.NomRefDao.class, "NomRef");
    xstream.useAttributeFor(CommonResDao.VariableResDao.class, "NomRef");
    xstream.useAttributeFor(CommonResDao.NbrMotNomRefDao.class, "NomRef");
    xstream.useAttributeFor(CommonResDao.NbrMotDao.class, "NbrMot");
    xstream.omitField(TypeEMHDao.class, "typeEMH");
    xstream.registerConverter(new VariableResLoiNbrPtConverter());
    xstream.alias("VariableRes", CommonResDao.VariableResDao.class);
    xstream.alias("VariableResLoi", CommonResDao.VariableResLoiDao.class);
    xstream.alias("VariableResLoiNbrPt", CommonResDao.VariableResLoiNbrPtDao.class);
    xstream.addImplicitCollection(CommonResDao.ItemResDao.class, "VariableResLoiNbrPt", CommonResDao.VariableResLoiNbrPtDao.class);
  }

  protected static class VariableResLoiNbrPtConverter implements Converter {

    @Override
    public boolean canConvert(final Class type) {
      return type.equals(CommonResDao.VariableResLoiNbrPtDao.class);
    }

    @Override
    public void marshal(final Object source, final HierarchicalStreamWriter writer, final MarshallingContext context) {
      final CommonResDao.VariableResLoiNbrPtDao valeur = (CommonResDao.VariableResLoiNbrPtDao) source;
      writer.setValue(Integer.toString(valeur.getNbrPt()));
      writer.addAttribute("NomRef", valeur.getNomRef());
    }

    @Override
    public Object unmarshal(final HierarchicalStreamReader reader, final UnmarshallingContext context) {
      final CommonResDao.VariableResLoiNbrPtDao valeur = new CommonResDao.VariableResLoiNbrPtDao();
      //attention a l'ordre !
      valeur.setNomRef(reader.getAttribute(0));
      valeur.NbrPt = Integer.parseInt(reader.getValue());
      return valeur;
    }
  }
}
