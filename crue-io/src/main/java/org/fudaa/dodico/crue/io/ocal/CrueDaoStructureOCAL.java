/*
 * License GPL v2
 */

package org.fudaa.dodico.crue.io.ocal;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.common.CrueHelper;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * Structures utilisees dans la classe CrueDaoOCAL
 *
 * @author CDE
 */
public class CrueDaoStructureOCAL implements CrueDataDaoStructure {

  private final boolean oldVersion;

  /**
   * @param oldVersion ancien version
   */
  public CrueDaoStructureOCAL(final boolean oldVersion) {
    super();
    this.oldVersion = oldVersion;
  }

  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {

    xstream.alias(CrueFileType.OCAL.toString(), CrueDaoOCAL.class);

    xstream.addImplicitCollection(CrueDaoOCAL.class, "listeOrdCalculs");

    xstream.alias("OrdCalcVraiPerm", OrdCalculVraiPermanentPersist.class);
    xstream.alias("IniCalcCI", IniCalcCIPersist.class);

    xstream.alias("IniCalcPrecedent", IniCalculPrecedentPersist.class);
    xstream.useAttributeFor(OrdCalculAbstractPersist.class, "NomRef");
    if (oldVersion) {
      xstream.omitField(CrueDaoOCAL.class, "Sorties");
      xstream.alias("OrdCalcPseudoPerm", OrdCalculPseudoPermanentPersistOld.class);
      xstream.alias("IniCalcReprise", IniReprisePersistOld.class);
      xstream.alias("OrdCalcTrans", OrdCalculTransitoirePersistOld.class);
    } else {
      CrueHelper.configureSorties(xstream, ctuluLog);
      xstream.alias("OrdCalcPseudoPerm", OrdCalculPseudoPermanentPersist.class);
      xstream.alias("OrdCalcTrans", OrdCalculTransitoirePersist.class);
      xstream.alias("PrendreClichePonctuel", PrendreClichePonctuelPersist.class);
      xstream.alias("PrendreClichePeriodique", PrendreClichePeriodiquePersist.class);
      xstream.alias("PdtRes", PdtResPersist.class);
      xstream.alias("IniCalcCliche", IniCalcClichePersist.class);
      xstream.alias("PrendreClicheFinPermanent", PrendreClicheFinPermanentPersist.class);
      xstream.useAttributeFor(IniCalcClichePersist.class, "NomFic");
      xstream.useAttributeFor(PrendreClicheFinPermanentPersist.class, "NomFic");
      xstream.useAttributeFor(PrendreClichePonctuelPersist.class, "NomFic");
      xstream.useAttributeFor(PrendreClichePeriodiquePersist.class, "NomFic");
    }

  }

  /**
   * Classe abstraite contenant l'attribut NomRef
   */
  abstract static class OrdCalculAbstractPersist {

    /**
     * Contient le nom de référence
     */
    String NomRef;
  }

  /**
   * Element Noeud de OrdCalculPseudoPermanent
   */
  static class OrdCalculPseudoPermanentPersistOld extends OrdCalculAbstractPersist {

    /**
     * Représente la valeur de la balise portant le même nom dans le fichier XML
     */
    IniCalcCIPersist IniCalcCI;
    /**
     * Représente la valeur de la balise portant le même nom dans le fichier XML
     */
    IniReprisePersistOld IniCalcReprise;
    /**
     * Représente la valeur de la balise portant le même nom dans le fichier XML
     */
    IniCalculPrecedentPersist IniCalcPrecedent;
  }

  /**
   * Element Noeud de OrdCalculPseudoPermanent
   */
  static class OrdCalculPseudoPermanentPersist extends OrdCalculAbstractPersist {

    /**
     * Représente la valeur de la balise portant le même nom dans le fichier XML
     */
    IniCalcCIPersist IniCalcCI;
    IniCalculPrecedentPersist IniCalcPrecedent;
    /**
     * Représente la valeur de la balise portant le même nom dans le fichier XML
     */
    IniCalcClichePersist IniCalcCliche;
    PrendreClicheFinPermanentPersist PrendreClicheFinPermanent;
  }

  /**
   * Element Noeud de OrdCalculVraiPermanent
   */
  static class OrdCalculVraiPermanentPersist extends OrdCalculAbstractPersist {
  }

  /**
   * Element Noeud de OrdCalculTransitoire
   */
  static class OrdCalculTransitoirePersistOld extends OrdCalculAbstractPersist {

    IniCalculPrecedentPersist IniCalcPrecedent;
  }

  public static class PdtResPersist {

    public String PdtCst;
  }

  /**
   * Element Noeud de OrdCalculTransitoire
   */
  static class OrdCalculTransitoirePersist extends OrdCalculAbstractPersist {

    String DureeCalc;
    public PdtResPersist PdtRes;
    /**
     * Représente la valeur de la balise portant le même nom dans le fichier XML
     */
    IniCalcCIPersist IniCalcCI;
    IniCalculPrecedentPersist IniCalcPrecedent;
    IniCalcClichePersist IniCalcCliche;
    PrendreClichePonctuelPersist PrendreClichePonctuel;
    PrendreClichePeriodiquePersist PrendreClichePeriodique;
  }

  static class PrendreClichePeriodiquePersist {

    String NomFic;
    PdtResPersist PdtRes;
  }

  static class PrendreClichePonctuelPersist {

    String NomFic;
    String TempsSimu;
  }

  /**
   * Element Noeud de IniCondIni
   */
  
  static class IniCalcCIPersist extends OrdCalculAbstractPersist {
  }

  /**
   * Element Noeud de IniReprise
   */
  static class IniReprisePersistOld extends OrdCalculAbstractPersist {

    /**
     * Représente la valeur de la balise portant le même nom dans le fichier XML
     */
    String TempsReprise;
  }

  

  static class IniCalcClichePersist {

    String NomFic;
  }

  static class PrendreClicheFinPermanentPersist {

    String NomFic;
  }

  /**
   * Element Noeud de IniCalculPrecedent
   */
  static class IniCalculPrecedentPersist extends OrdCalculAbstractPersist {
  }
}
