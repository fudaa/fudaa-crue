/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores.old;

public class DaoOrdResNoeudNiveauContinuOld {
  // WARN: l'ordre des champs est important car utilise par l'ecriture/lecture de ORES

  private boolean ddeZ;

  public final boolean getDdeZ() {
    return ddeZ;
  }

  /**
   * @param newDdeZ
   */
  public final void setDdeZ(boolean newDdeZ) {
    ddeZ = newDdeZ;
  }

  @Override
  public String toString() {
    return "OrdResNoeudNiveauContinu [ddeZ=" + ddeZ + "]";
  }
}
