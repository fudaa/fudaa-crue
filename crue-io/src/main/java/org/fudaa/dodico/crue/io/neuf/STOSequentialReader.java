/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import java.nio.ByteOrder;
import java.util.List;
import org.fudaa.dodico.crue.metier.StoContent;

/**
 * Ce reader permettant d'accéder aux données. Pour l'instant c'est un conteneur de données mais si on se rend compte que le nombre de données est
 * important on lira les données que si nécessaire.
 *
 * @author deniger
 */
public class STOSequentialReader implements StoContent {

  @Override
  public boolean isParamGenOrDimensionNotDefined() {
    return (getParametresGeneraux() == null || getDimensionsModele() == null);
  }

  @Override
  public int getNpo() {
    return getParametresGeneraux().getNpo();
  }

  @Override
  public int getNbstr() {
    return getParametresGeneraux().getNbstr();
  }

  @Override
  public int getNblitmax() {
    return getParametresGeneraux().getNblitmax();
  }

  @Override
  public int getNbhaut() {
    return getParametresGeneraux().getNbhaut();
  }

  @Override
  public int getLReclProf() {
    return getDimensionsModele().getLReclProf();
  }

  @Override
  public int getNbRecProf() {
    return getDimensionsModele().getNbRecProf();
  }

  /**
   * Classe qui permet de stocker les données d'une branche
   *
   * @author CDE
   */
  public static class DonneesBranche {

    private float alpha;
    private int iMax;
    private int n;
    private int n0Pr[];
    private int nbSing;
    private String nomBr;
    private int nSing;
    private int nTyp;
    private int nuPam;
    private int nuPav;
    private float ruiBra;
    private float[] sing;
    private float sinuo;

    /**
     * @return the alpha
     */
    public float getAlpha() {
      return alpha;
    }

    /**
     * @return the iMax
     */
    public int getIMax() {
      return iMax;
    }

    /**
     * @return the n
     */
    public int getN() {
      return n;
    }

    /**
     * @return the n0Pr
     */
    public int[] getN0Pr() {
      return n0Pr;
    }

    /**
     * @return the nbSing
     */
    public int getNbSing() {
      return nbSing;
    }

    /**
     * @return the nomBr
     */
    public String getNomBr() {
      return nomBr;
    }

    /**
     * @return the nSing
     */
    public int getNSing() {
      return nSing;
    }

    /**
     * @return the nTyp
     */
    public int getNTyp() {
      return nTyp;
    }

    /**
     * @return the nuPam
     */
    public int getNuPam() {
      return nuPam;
    }

    /**
     * @return the nuPav
     */
    public int getNuPav() {
      return nuPav;
    }

    /**
     * @return the ruiBra
     */
    public float getRuiBra() {
      return ruiBra;
    }

    /**
     * @return the sing
     */
    public float[] getSing() {
      return sing;
    }

    /**
     * @return the sinuo
     */
    public float getSinuo() {
      return sinuo;
    }

    /**
     * @param alpha the alpha to set
     */
    protected void setAlpha(final float alpha) {
      this.alpha = alpha;
    }

    /**
     * @param max the iMax to set
     */
    protected void setIMax(final int max) {
      iMax = max;
    }

    /**
     * @param n the n to set
     */
    protected void setN(final int n) {
      this.n = n;
    }

    /**
     * @param pr the n0Pr to set
     */
    protected void setN0Pr(final int[] pr) {
      n0Pr = pr;
    }

    /**
     * @param nbSing the nbSing to set
     */
    protected void setNbSing(final int nbSing) {
      this.nbSing = nbSing;
    }

    /**
     * @param nomBr the nomBr to set
     */
    protected void setNomBr(final String nomBr) {
      this.nomBr = nomBr;
    }

    /**
     * @param sing the nSing to set
     */
    protected void setNSing(final int sing) {
      nSing = sing;
    }

    /**
     * @param typ the nTyp to set
     */
    protected void setNTyp(final int typ) {
      nTyp = typ;
    }

    /**
     * @param nuPam the nuPam to set
     */
    protected void setNuPam(final int nuPam) {
      this.nuPam = nuPam;
    }

    /**
     * @param nuPav the nuPav to set
     */
    protected void setNuPav(final int nuPav) {
      this.nuPav = nuPav;
    }

    /**
     * @param ruiBra the ruiBra to set
     */
    protected void setRuiBra(final float ruiBra) {
      this.ruiBra = ruiBra;
    }

    /**
     * @param sing the sing to set
     */
    protected void setSing(final float[] sing) {
      this.sing = sing;
    }

    /**
     * @param sinuo the sinuo to set
     */
    protected void setSinuo(final float sinuo) {
      this.sinuo = sinuo;
    }
  }

  /**
   * Classe qui permet de stocker les données générales du modèle
   *
   * @author CDE
   */
  public static class DonneesGeneralesModele {

    private double dal;
    private double g;
    private String nomDc;
    private double theta;
    private String titreDc;
    private float zRef;

    /**
     * @return the dal
     */
    protected double getDal() {
      return dal;
    }

    /**
     * @return the g
     */
    protected double getG() {
      return g;
    }

    /**
     * @return the nomDc
     */
    protected String getNomDc() {
      return nomDc;
    }

    /**
     * @return the theta
     */
    protected double getTheta() {
      return theta;
    }

    /**
     * @return the titreDc
     */
    protected String getTitreDc() {
      return titreDc;
    }

    /**
     * @return the zRef
     */
    protected float getZRef() {
      return zRef;
    }

    /**
     * @param dal the dal to set
     */
    protected void setDal(final double dal) {
      this.dal = dal;
    }

    /**
     * @param g the g to set
     */
    protected void setG(final double g) {
      this.g = g;
    }

    /**
     * @param nomDc the nomDc to set
     */
    protected void setNomDc(final String nomDc) {
      this.nomDc = nomDc;
    }

    /**
     * @param theta the theta to set
     */
    protected void setTheta(final double theta) {
      this.theta = theta;
    }

    /**
     * @param titreDc the titreDc to set
     */
    protected void setTitreDc(final String titreDc) {
      this.titreDc = titreDc;
    }

    /**
     * @param ref the zRef to set
     */
    protected void setZRef(final float ref) {
      zRef = ref;
    }
  }

  /**
   * Conteneur des parametres generaux lues dans le fichiers sto Voir le fichier liste des variables.doc pour signification des variables.
   *
   * @author deniger
   */
  public static class ParametresGeneraux {

    /**
     * nombre maxi de branches connectées à un même noeud
     */
    private int nbbram;
    /**
     * nombre de pas de hauteur
     */
    private int nbhaut;
    /**
     * ?
     */
    private int nblitmax;
    /**
     * nombre maxi de noeuds
     */
    private int nbpmax;
    /**
     * nombre maxi de profils dans le modèle
     */
    private int nbpro;
    /**
     * nombre maxi de branches
     */
    private int nbr;
    /**
     * ?
     */
    private int nbstr;
    /**
     * ?
     */
    private int npo;
    /**
     * nombre maxi de profils par branche
     */
    private int npr;
    /**
     * nombre maxi de profils (= nbr*npr)
     */
    private int ntpr;
    /**
     * dimension du tableau sing() des singularités
     */
    private int ntsing;

    /**
     * @return the nbbram
     */
    public int getNbbram() {
      return nbbram;
    }

    /**
     * @return the nbhaut
     */
    public int getNbhaut() {
      return nbhaut;
    }

    /**
     * @return the nblitmax
     */
    public int getNblitmax() {
      return nblitmax;
    }

    /**
     * @return the nbpmax
     */
    public int getNbpmax() {
      return nbpmax;
    }

    /**
     * @return the nbpro
     */
    public int getNbpro() {
      return nbpro;
    }

    /**
     * @return the nbr
     */
    public int getNbr() {
      return nbr;
    }

    /**
     * @return the nbstr
     */
    public int getNbstr() {
      return nbstr;
    }

    /**
     * @return the npo
     */
    public int getNpo() {
      return npo;
    }

    /**
     * @return the npr
     */
    public int getNpr() {
      return npr;
    }

    /**
     * @return the ntpr
     */
    public int getNtpr() {
      return ntpr;
    }

    /**
     * @return the ntsing
     */
    public int getNtsing() {
      return ntsing;
    }

    /**
     * @param nbbram the nbbram to set
     */
    public void setNbbram(final int nbbram) {
      this.nbbram = nbbram;
    }

    /**
     * @param nbhaut the nbhaut to set
     */
    public void setNbhaut(final int nbhaut) {
      this.nbhaut = nbhaut;
    }

    /**
     * @param nblitmax the nblitmax to set
     */
    public void setNblitmax(final int nblitmax) {
      this.nblitmax = nblitmax;
    }

    /**
     * @param nbpmax the nbpmax to set
     */
    public void setNbpmax(final int nbpmax) {
      this.nbpmax = nbpmax;
    }

    /**
     * @param nbpro the nbpro to set
     */
    public void setNbpro(final int nbpro) {
      this.nbpro = nbpro;
    }

    /**
     * @param nbr the nbr to set
     */
    public void setNbr(final int nbr) {
      this.nbr = nbr;
    }

    /**
     * @param nbstr the nbstr to set
     */
    public void setNbstr(final int nbstr) {
      this.nbstr = nbstr;
    }

    /**
     * @param npo the npo to set
     */
    public void setNpo(final int npo) {
      this.npo = npo;
    }

    /**
     * @param npr the npr to set
     */
    public void setNpr(final int npr) {
      this.npr = npr;
    }

    /**
     * @param ntpr the ntpr to set
     */
    public void setNtpr(final int ntpr) {
      this.ntpr = ntpr;
    }

    /**
     * @param ntsing the ntsing to set
     */
    public void setNtsing(final int ntsing) {
      this.ntsing = ntsing;
    }
  }

  /**
   * Classe décrivant les dimencions du modèle
   *
   * @author CDE
   */
  public static class DimensionsModele {

    /**
     * Nombre de branches du modèle
     */
    private int iBmax;
    /**
     * Nombre de noeuds du modèle
     */
    private int nbPoin;
    /**
     * Nombre de profils du modèle
     */
    private int iPrM;
    /**
     * Nombre de profils fluviaux du modèle
     */
    private int iPrFlM;
    /**
     * Adresse maximale du tableau des singularités
     */
    private int nSing;
    /**
     * Largeur de bande de la matrice de connexion des nœuds
     */
    private int larBan;
    /**
     * Longueur d'enregistrement d'une structure Prof du fichier STR
     */
    private int lReclProf;
    /**
     * Nombre de structures Prof du fichier STR
     */
    private int nbRecProf;

    /**
     * @return the iBmax
     */
    public int getIBmax() {
      return iBmax;
    }

    /**
     * @param bmax the iBmax to set
     */
    public void setIBmax(int bmax) {
      iBmax = bmax;
    }

    /**
     * @return the nbPoin
     */
    public int getNbPoin() {
      return nbPoin;
    }

    /**
     * @param nbPoin the nbPoin to set
     */
    public void setNbPoin(int nbPoin) {
      this.nbPoin = nbPoin;
    }

    /**
     * @return the iPrM
     */
    public int getIPrM() {
      return iPrM;
    }

    /**
     * @param prM the iPrM to set
     */
    public void setIPrM(int prM) {
      iPrM = prM;
    }

    /**
     * @return the iPrFlM
     */
    public int getIPrFlM() {
      return iPrFlM;
    }

    /**
     * @param prFlM the iPrFlM to set
     */
    public void setIPrFlM(int prFlM) {
      iPrFlM = prFlM;
    }

    /**
     * @return the nSing
     */
    public int getNSing() {
      return nSing;
    }

    /**
     * @param sing the nSing to set
     */
    public void setNSing(int sing) {
      nSing = sing;
    }

    /**
     * @return the larBan
     */
    public int getLarBan() {
      return larBan;
    }

    /**
     * @param larBan the larBan to set
     */
    public void setLarBan(int larBan) {
      this.larBan = larBan;
    }

    /**
     * @return the lReclProf
     */
    public int getLReclProf() {
      return lReclProf;
    }

    /**
     * @param reclProf the lReclProf to set
     */
    public void setLReclProf(int reclProf) {
      lReclProf = reclProf;
    }

    /**
     * @return the nbRecProf
     */
    public int getNbRecProf() {
      return nbRecProf;
    }

    /**
     * @param nbRecProf the nbRecProf to set
     */
    public void setNbRecProf(int nbRecProf) {
      this.nbRecProf = nbRecProf;
    }
  }

  /**
   * Classe décrivant les données spécifiques d'un profil
   *
   * @author CDE
   */
  public static class DonneesSpecProf {

    /**
     * Nom du profil
     */
    private String nomProfil;
    /**
     * Nom du casier
     */
    private String nomCasier;

    /**
     * @return the nomProfil
     */
    public String getNomProfil() {
      return nomProfil;
    }

    /**
     * @param nomProfil the nomProfil to set
     */
    public void setNomProfil(final String nomProfil) {
      this.nomProfil = nomProfil;
    }

    /**
     * @return the nomCasier
     */
    public String getNomCasier() {
      return nomCasier;
    }

    /**
     * @param nomCasier the nomCasier to set
     */
    public void setNomCasier(final String nomCasier) {
      this.nomCasier = nomCasier;
    }
  }

  /**
   * Classe décrivant les données d'un noeud
   *
   * @author CDE
   */
  public static class DonneesNoeud {

    /**
     * Nom du nœud
     */
    private String nomPoi;
    /**
     * Si le nœud porte un casier, emplacement de ses caractéristiques dans le tableau Sing(), 0 sinon
     */
    private int nuCas;
    /**
     * Nombre de branches connectées au nœud
     */
    private int nbBran;
    /**
     * Numéro de chacune des branches connectées au nœud
     */
    private int[] numerosBranches;
    /**
     * Cote du fond du casier
     */
    private float zfonca;
    /**
     * Coefficient multiplicatif de ruissellement pour le casier
     */
    private float ruicas;
    /**
     * Tableau des caractéristiques du casier
     */
    private DonneesCasier casier;

    /**
     * @return the nomPoi
     */
    public String getNomPoi() {
      return nomPoi;
    }

    /**
     * @param nomPoi the nomPoi to set
     */
    public void setNomPoi(final String nomPoi) {
      this.nomPoi = nomPoi;
    }

    /**
     * @return the nuCas
     */
    public int getNuCas() {
      return nuCas;
    }

    /**
     * @param nuCas the nuCas to set
     */
    public void setNuCas(final int nuCas) {
      this.nuCas = nuCas;
    }

    /**
     * @return the nbBran
     */
    public int getNbBran() {
      return nbBran;
    }

    /**
     * @param nbBran the nbBran to set
     */
    public void setNbBran(final int nbBran) {
      this.nbBran = nbBran;
    }

    /**
     * @return the numerosBranches
     */
    public int[] getNumerosBranches() {
      return numerosBranches;
    }

    /**
     * @param numerosBranches the numerosBranches to set
     */
    public void setNumerosBranches(final int[] numerosBranches) {
      this.numerosBranches = numerosBranches;
    }

    /**
     * @return the zfonca
     */
    public float getZfonca() {
      return zfonca;
    }

    /**
     * @param zfonca the zfonca to set
     */
    public void setZfonca(final float zfonca) {
      this.zfonca = zfonca;
    }

    /**
     * @return the ruicas
     */
    public float getRuicas() {
      return ruicas;
    }

    /**
     * @param ruicas the ruicas to set
     */
    public void setRuicas(final float ruicas) {
      this.ruicas = ruicas;
    }

    /**
     * @return the casier
     */
    public DonneesCasier getCasier() {
      return casier;
    }

    /**
     * @param casier the casier to set
     */
    public void setCasier(final DonneesCasier casier) {
      this.casier = casier;
    }
  }

  /**
   * @author cde
   */
  public static class DonneesCasier {

    private float zFon;
    private float zHau;
    private float dZpr;
    private float distTot;
    private float[] superficiePlan;
    private float[] surfaceMouillee;
    private float ulmCas;

    /**
     * @return the zFon
     */
    public float getZFon() {
      return zFon;
    }

    /**
     * @param fon the zFon to set
     */
    public void setZFon(final float fon) {
      zFon = fon;
    }

    /**
     * @return the zHau
     */
    public float getZHau() {
      return zHau;
    }

    /**
     * @param hau the zHau to set
     */
    public void setZHau(final float hau) {
      zHau = hau;
    }

    /**
     * @return the dZpr
     */
    public float getDZpr() {
      return dZpr;
    }

    /**
     * @param zpr the dZpr to set
     */
    public void setDZpr(final float zpr) {
      dZpr = zpr;
    }

    /**
     * @return the distTot
     */
    public float getDistTot() {
      return distTot;
    }

    /**
     * @param distTot the distTot to set
     */
    public void setDistTot(final float distTot) {
      this.distTot = distTot;
    }

    /**
     * @return the superficiePlan
     */
    public float[] getSuperficiePlan() {
      return superficiePlan;
    }

    /**
     * @param superficiePlan the superficiePlan to set
     */
    public void setSuperficiePlan(final float[] superficiePlan) {
      this.superficiePlan = superficiePlan;
    }

    /**
     * @return the surfaceMouillee
     */
    public float[] getSurfaceMouillee() {
      return surfaceMouillee;
    }

    /**
     * @param surfaceMouillee the surfaceMouillee to set
     */
    public void setSurfaceMouillee(final float[] surfaceMouillee) {
      this.surfaceMouillee = surfaceMouillee;
    }

    /**
     * @return the ulmCas
     */
    public float getUlmCas() {
      return ulmCas;
    }

    /**
     * @param ulmCas the ulmCas to set
     */
    public void setUlmCas(final float ulmCas) {
      this.ulmCas = ulmCas;
    }
  }

  /**
   * Classe décrivant les données générales d'un profil
   *
   * @author CDE
   */
  public static class DonneesGenProfil {

    /**
     * Nom du profil
     */
    private String tit;
    /**
     * Numéro du profil fluvial
     */
    private int nuPrFl;
    /**
     * Distance depuis le profil précédent
     */
    private float dist;
    /**
     * Coefficient de divergence depuis le profil précédent
     */
    private float cDiv;
    /**
     * Coefficient de convergence depuis le profil précédent
     */
    private float cConv;
    /**
     * Coefficient de pondération amont/aval depuis le profil précédent
     */
    private float cPond;

    /**
     * @return the tit
     */
    public String getTit() {
      return tit;
    }

    /**
     * @param tit the tit to set
     */
    public void setTit(final String tit) {
      this.tit = tit;
    }

    /**
     * @return the nuPrFl
     */
    public int getNuPrFl() {
      return nuPrFl;
    }

    /**
     * @param nuPrFl the nuPrFl to set
     */
    public void setNuPrFl(final int nuPrFl) {
      this.nuPrFl = nuPrFl;
    }

    /**
     * @return the dist
     */
    public float getDist() {
      return dist;
    }

    /**
     * @param dist the dist to set
     */
    public void setDist(final float dist) {
      this.dist = dist;
    }

    /**
     * @return the cDiv
     */
    public float getCDiv() {
      return cDiv;
    }

    /**
     * @param div the cDiv to set
     */
    public void setCDiv(final float div) {
      cDiv = div;
    }

    /**
     * @return the cConv
     */
    public float getCConv() {
      return cConv;
    }

    /**
     * @param conv the cConv to set
     */
    public void setCConv(final float conv) {
      cConv = conv;
    }

    /**
     * @return the cPond
     */
    public float getCPond() {
      return cPond;
    }

    /**
     * @param pond the cPond to set
     */
    public void setCPond(final float pond) {
      cPond = pond;
    }
  }
  /**
   * Attributs de la classe STOSequentialReader
   */
  private ParametresGeneraux parametresGeneraux;
  private DimensionsModele dimensionsModele;
  private DonneesGeneralesModele donneesGenerales;
  private List<DonneesSpecProf> donneesSpecProf;
  private List<DonneesBranche> donneesBranches;
  private List<DonneesNoeud> donneesNoeuds;
  private List<DonneesGenProfil> donneesGenProfils;
  private ByteOrder order;

  /**
   * @return the order
   */
  @Override
  public ByteOrder getOrder() {
    return order;
  }

  /**
   * @param order
   */
  public void setOrder(ByteOrder order) {
    this.order = order;
  }

  /**
   * @return the donneesGenerales
   */
  protected DonneesGeneralesModele getDonneesGenerales() {
    return donneesGenerales;
  }

  /**
   * @return the parametresGeneraux
   */
  public ParametresGeneraux getParametresGeneraux() {
    return parametresGeneraux;
  }

  /**
   * @param donneesGenerales the donneesGenerales to set
   */
  protected void setDonneesGenerales(final DonneesGeneralesModele donneesGenerales) {
    this.donneesGenerales = donneesGenerales;
  }

  /**
   * @param parametresGeneraux the parametresGeneraux to set
   */
  public void setParametresGeneraux(final ParametresGeneraux parametresGeneraux) {
    this.parametresGeneraux = parametresGeneraux;
  }

  /**
   * @return the donneesSpecProf
   */
  public List<DonneesSpecProf> getDonneesSpecProf() {
    return donneesSpecProf;
  }

  /**
   * @param donneesSpecProf the donneesSpecProf to set
   */
  public void setDonneesSpecProf(final List<DonneesSpecProf> donneesSpecProf) {
    this.donneesSpecProf = donneesSpecProf;
  }

  /**
   * @return the donneesBranches
   */
  public List<DonneesBranche> getDonneesBranches() {
    return donneesBranches;
  }

  /**
   * @param donneesBranches the donneesBranches to set
   */
  public void setDonneesBranches(final List<DonneesBranche> donneesBranches) {
    this.donneesBranches = donneesBranches;
  }

  /**
   * @return the donneesNoeuds
   */
  public List<DonneesNoeud> getDonneesNoeuds() {
    return donneesNoeuds;
  }

  /**
   * @param donneesNoeuds the donneesNoeuds to set
   */
  public void setDonneesNoeuds(final List<DonneesNoeud> donneesNoeuds) {
    this.donneesNoeuds = donneesNoeuds;
  }

  /**
   * @return the donneesGenProfils
   */
  public List<DonneesGenProfil> getDonneesGenProfils() {
    return donneesGenProfils;
  }

  /**
   * @param donneesGenProfils the donneesGenProfils to set
   */
  public void setDonneesGenProfils(final List<DonneesGenProfil> donneesGenProfils) {
    this.donneesGenProfils = donneesGenProfils;
  }

  /**
   * @return the dimensionsModele
   */
  public DimensionsModele getDimensionsModele() {
    return dimensionsModele;
  }

  /**
   * @param dimensionsModele the dimensionsModele to set
   */
  public void setDimensionsModele(final DimensionsModele dimensionsModele) {
    this.dimensionsModele = dimensionsModele;
  }
}
