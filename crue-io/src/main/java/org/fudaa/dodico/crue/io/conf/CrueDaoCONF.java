/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.conf;

import org.fudaa.dodico.crue.common.io.AbstractCrueDao;

/**
 * @author CANEL Christophe
 *
 */
public class CrueDaoCONF extends AbstractCrueDao {

  public CrueDaoStructureCONF.Site Site;
  public CrueDaoStructureCONF.User User;
}
