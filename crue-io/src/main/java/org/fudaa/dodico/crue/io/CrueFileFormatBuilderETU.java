/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.etu.CrueConverterETU;
import org.fudaa.dodico.crue.io.etu.CrueDaoStructureETU;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;

public class CrueFileFormatBuilderETU implements CrueFileFormatBuilder<EMHProjet> {

  @Override
  public Crue10FileFormat<EMHProjet> getFileFormat(final CoeurConfigContrat coeurConfig) {
    String version = coeurConfig == null ? null : coeurConfig.getXsdVersion();
    return new Crue10FileFormatETU(new CrueDataXmlReaderWriterImpl<>(CrueFileType.ETU,
                                                                                      coeurConfig,
                                                                                      new CrueConverterETU(
                                                                                              version), new CrueDaoStructureETU(version)));

  }
}
