package org.fudaa.dodico.crue.io.test;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.Crue9FileFormatFactory;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.factory.EMHRelationFactory;

import java.net.URL;

public class ReadHelperForTest {
    private String modeleName="Mo_Toto";
    private String scenarioName = "Sc_Test";
    private final CoeurConfig coeurConfig;

    public ReadHelperForTest(CoeurConfig coeurConfig) {
        this.coeurConfig = coeurConfig;
    }

    public void setModeleName(String modeleName) {
        this.modeleName = modeleName;
    }

    public String getModeleName() {
        return modeleName;
    }

    public void setScenarioName(String scenarioName) {
        this.scenarioName = scenarioName;
    }

    public String getScenarioName() {
        return scenarioName;
    }

    public EMHScenario readModeleAnsSetRelation(final CtuluLog analyzer, final String dcPath, final String dhPath) {
        return readModeleAnsSetRelationData(analyzer, dcPath, dhPath).getMetier().getScenarioData();
    }

    public CrueIOResu<CrueData> readModeleAnsSetRelationData(final CtuluLog analyzer, final String dcPath, final String dhPath) {
        final CrueIOResu<CrueData> data = readModele(analyzer, dcPath, dhPath);
        final CrueData res = data.getMetier();
        Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ORES, coeurConfig).read(coeurConfig.getDefaultFile(CrueFileType.ORES),
            analyzer, res);
        Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.OPTR, coeurConfig).read(coeurConfig.getDefaultFile(
            CrueFileType.OPTR), analyzer, res);
        initRelations(res);
        return data;
    }

    public CrueIOResu<CrueData> readModele(final CtuluLog analyzer, final String dcPath, final String dhPath) {
        final CrueData data = readModeleCrue9(analyzer, dcPath);
        return Crue9FileFormatFactory.getDHFileFormat().read(dhPath, analyzer, data);
    }

    public CrueIOResu<CrueData> readModeleAndDefaultORES(final CtuluLog analyzer, final String dcPath,
                                                         final String dhPath) {
        final CrueData data = readModeleCrue9(analyzer, dcPath);
        final CrueIOResu<CrueData> read = Crue9FileFormatFactory.getDHFileFormat().read(dhPath, analyzer, data);
        final URL defaultFile = coeurConfig.getDefaultFile(CrueFileType.ORES);
        Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ORES, coeurConfig).read(defaultFile,
            analyzer, read.getMetier());
        Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.OPTR, coeurConfig).read(coeurConfig.getDefaultFile(
            CrueFileType.OPTR), analyzer, read.getMetier());
        data.getModele().setNom(modeleName);
        data.getSousModele().setNom(CruePrefix.changePrefix(modeleName, CruePrefix.P_MODELE, CruePrefix.P_SS_MODELE));
        EMHRelationFactory.addRelationContientEMH(data.getScenarioData(), data.getModele());
        EMHRelationFactory.addRelationContientEMH(data.getModele(), data.getSousModele());
        return read;
    }

    public CrueIOResu<CrueData> readModele(final CtuluLog analyzer, final URL dcPath, final URL dhPath) {
        final CrueData data = readModeleCrue9(analyzer, dcPath);
        return Crue9FileFormatFactory.getDHFileFormat().read(dhPath, analyzer, data);
    }



    public  void initRelations(final CrueData res) {

        res.getScenarioData().setNom(scenarioName);
        res.getModele().setNom(modeleName);
        res.getSousModele().setNom(CruePrefix.changePrefix(res.getModele().getNom(), CruePrefix.P_MODELE, CruePrefix.P_SS_MODELE));
        EMHScenario emhScenario = res.getScenarioData();
        EMHRelationFactory.addRelationContientEMH(emhScenario, res.getModele());
        EMHRelationFactory.addRelationContientEMH(res.getModele(), res.getSousModele());
    }

    public CrueData readModeleCrue9(final CtuluLog analyzer, final String path) {
        return readModeleCrue9(analyzer, ReadHelperForTest.class.getResource(path));
    }

    public CrueData readModeleCrue9(final CtuluLog analyzer, final URL url) {
        final CrueIOResu<CrueData> data = Crue9FileFormatFactory.getDCFileFormat().read(url, analyzer,
            new CrueDataImpl(coeurConfig.getCrueConfigMetier()));
        return data.getMetier();
    }
}
