/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.res;

import gnu.trove.TObjectIntHashMap;
import gnu.trove.TObjectIntIterator;

import java.util.*;
import java.util.stream.Collectors;

import org.fudaa.dodico.crue.io.rdao.CommonResDao;
import org.fudaa.dodico.crue.io.rdao.CommonResDao.VariableResDao;

/**
 * @author deniger
 */
public class ResVariablesContent {

  /**
   * Attention: une variable avec la même ref peut être définie plusieurs fois. Contient uniquement des VariablesResDao et
   * VariableResLoiDao
   */
  final List<CommonResDao.VariableResDao> variables = new ArrayList<>();
  final List<CommonResDao.VariableResDao> variablesExt = Collections.unmodifiableList(variables);
  Map<String, VariableResDao> definedVariable = new HashMap<>();
  /**
   * pour chaque VariableResDaoNbrPt, donne la taille définie.
   */
  TObjectIntHashMap<String> loiNbrPtById = new TObjectIntHashMap<>();

  public boolean isVariableDefined(String ref) {
    return definedVariable.containsKey(ref);
  }

  public List<VariableResDao> getVariables() {
    return variablesExt;
  }

  void addVariables(CommonResDao.VariableResDao var) {
    variables.add(var);
    definedVariable.put(var.getId(), var);
  }

  public CommonResDao.VariableResDao getVariable(int idx) {
    return variables.get(idx);
  }

  @Override
  public ResVariablesContent clone() {
    ResVariablesContent res = new ResVariablesContent();
    res.variables.addAll(variables);
    res.definedVariable = new HashMap<>(definedVariable);
    res.loiNbrPtById = loiNbrPtById.clone();
    return res;
  }

  public boolean isQRegul(String nom) {
    VariableResDao variableResDao = definedVariable.get(nom);
    return variableResDao != null && variableResDao.isQRegul();
  }

  public boolean isZRegul(String nom) {
    VariableResDao variableResDao = definedVariable.get(nom);
    return variableResDao != null && variableResDao.isZRegul();
  }

  /**
   * ajoute toutes les définitions des tailles de loi. Ecrase les données existante is overwrite vaut true.
   *
   * @param res
   */
  public Set<String> addAll(ResVariablesContent res) {
    if (res == null) {
      return Collections.emptySet();
    }
    Set<String> variablesEnDoublon = addVariables(res.variables);
    for (TObjectIntIterator<String> it = res.loiNbrPtById.iterator(); it.hasNext(); ) {
      it.advance();
      loiNbrPtById.put(it.key(), it.value());
    }
    return variablesEnDoublon;
  }

  public void addVariableResLoiNbrPtDao(Collection<CommonResDao.VariableResLoiNbrPtDao> loiNbrPt) {
    for (CommonResDao.VariableResLoiNbrPtDao variableResLoiNbrPtDao : loiNbrPt) {
      loiNbrPtById.put(variableResLoiNbrPtDao.getId(), variableResLoiNbrPtDao.getNbrPt());

    }
  }

  /**
   * @return liste des variables déjà définies.
   */
  private Set<String> addVariables(final List<VariableResDao> variablesList) {
    Set<String> alreadyDefinedVariables = new HashSet<>();
    for (CommonResDao.VariableResDao variableResDao : variablesList) {
      if (definedVariable.containsKey(variableResDao.getId())) {
        alreadyDefinedVariables.add(variableResDao.getId());
      } else {
        definedVariable.put(variableResDao.getId(), variableResDao);
      }
      variables.add(variableResDao);
    }
    return alreadyDefinedVariables;
  }

  public boolean isLoiNbrPtDaoDefined(String ref) {
    return loiNbrPtById.containsKey(ref);
  }

  public Set<String> getLoiNbrPtDefinition() {
    return new HashSet<>(Arrays.asList(loiNbrPtById.keys(new String[loiNbrPtById.size()])));
  }

  public int getLoiNbrPt(String ref) {
    if (!isLoiNbrPtDaoDefined(ref)) {
      return -1;
    }
    return loiNbrPtById.get(ref);
  }

  public boolean containsVariable() {
    return !variables.isEmpty();
  }

  public boolean containsVariableResLoiNbrPtDao() {
    return !loiNbrPtById.isEmpty();
  }

  public boolean isEmpty() {
    return loiNbrPtById.isEmpty() && variables.isEmpty();
  }

  public int getNbVariables() {
    return variables.size();
  }

  public List<String> getQZRegulVariablesId() {
    return variables.stream().filter(variableResDao -> variableResDao.isZRegul() || variableResDao.isQRegul()).map(VariableResDao::getId).collect(Collectors.toList());
  }
  public List<String> getZRegulVariablesName() {
    return variables.stream().filter(VariableResDao::isZRegul).map(VariableResDao::getNomRef).collect(Collectors.toList());
  }
  public List<String> getZRegulVariablesId() {
    return variables.stream().filter(VariableResDao::isZRegul).map(VariableResDao::getId).collect(Collectors.toList());
  }
  public List<String> getQRegulVariablesName() {
    return variables.stream().filter(VariableResDao::isQRegul).map(VariableResDao::getNomRef).collect(Collectors.toList());
  }
  public List<String> getQRegulVariablesId() {
    return variables.stream().filter(VariableResDao::isQRegul).map(VariableResDao::getId).collect(Collectors.toList());
  }
}
