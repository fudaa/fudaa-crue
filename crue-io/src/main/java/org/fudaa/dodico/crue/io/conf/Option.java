/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.conf;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 *
 * @author deniger
 */
@XStreamAlias("Option")
public class Option {

  @XStreamAlias("Nom")
  @XStreamAsAttribute
  public String Nom;
  @XStreamAlias("Valeur")
  @XStreamAsAttribute
  public String Valeur;

  public Option() {
  }

  public Option(String Nom, String Valeur) {
    this.Nom = Nom;
    this.Valeur = Valeur;
  }

  public String getNom() {
    return Nom;
  }

  public void setNom(String Nom) {
    this.Nom = Nom;
  }

  public String getValeur() {
    return Valeur;
  }

  public void setValeur(String Valeur) {
    this.Valeur = Valeur;
  }
}
