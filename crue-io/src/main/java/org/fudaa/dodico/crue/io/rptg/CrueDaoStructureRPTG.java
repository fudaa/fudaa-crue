/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rptg;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.rdao.CommonResDaoXstream;
import org.fudaa.dodico.crue.io.rdao.ContexteSimulationDaoXstream;
import org.fudaa.dodico.crue.io.rdao.ParametrageDaoXstream;
import org.fudaa.dodico.crue.io.rdao.StructureResultatsDaoXstream;
import org.fudaa.dodico.crue.metier.CrueFileType;

public class CrueDaoStructureRPTG implements CrueDataDaoStructure {
  
  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    xstream.alias(CrueFileType.RPTG.toString(), CrueDaoRPTG.class);
    
    CommonResDaoXstream.configureXstream(xstream);
    ParametrageDaoXstream.configureXstream(xstream);
    ContexteSimulationDaoXstream.configureXstream(xstream);
    StructureResultatsDaoXstream.configureXstream(xstream);
    ResPrtGeoDaoXstream.configureXstream(xstream);
  }
}
