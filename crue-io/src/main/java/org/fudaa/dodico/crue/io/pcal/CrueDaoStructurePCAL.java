/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.pcal;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.metier.CrueFileType;

/**
 * Structures utilisées dans la classe CrueDaoPCAL
 * 
 * @author CDE
 */
public class CrueDaoStructurePCAL implements CrueDataDaoStructure {
  
  private final String version;

  /**
   * @param version la version
   */
  public CrueDaoStructurePCAL(final String version) {
    super();
    this.version = version;
  }

  /*
   * @see org.fudaa.dodico.crue.dao.CrueDataDaoStructure#configureXStream(com.thoughtworks.xstream.XStream)
   */
  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {

    xstream.alias(CrueFileType.PCAL.toString(), CrueDaoPCAL.class);

    xstream.alias("PdtCouplage", PdtCouplagePersist.class);
    xstream.alias("PdtRes", PdtRes.class);
    if(!Crue10VersionConfig.V_1_1_1.equals(version)){
      xstream.omitField(CrueDaoPCAL.class, "Verbosite");
      xstream.omitField(CrueDaoPCAL.class, "PdtCouplage");
      xstream.omitField(CrueDaoPCAL.class, "DureeSce");
      xstream.omitField(CrueDaoPCAL.class, "PdtRes");
    }
  }

  /**
   * Représente la valeur de la balise PdtCouplage dans le fichier XML
   * 
   * @author CDE
   */
  public static class PdtCouplagePersist {
    /** Représente la valeur de la balise portant le même nom dans le fichier XML */
    public String PdtCst;
  }

  /**
   * Représente la valeur de la balise PdtRes dans le fichier XML
   * 
   * @author CDE
   */
  public static class PdtRes extends PdtCouplagePersist {}
}
