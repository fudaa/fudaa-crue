package org.fudaa.dodico.crue.io.log;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueXmlReaderWriterImpl;

/**
 * @author CANEL Christophe
 *
 */
public class CrueLOGReaderWriter extends CrueXmlReaderWriterImpl<CrueDaoLOG, CtuluLog> {

  public CrueLOGReaderWriter(final String version) {
    super("log", version, new CrueConverterLOG(), new CrueDaoStructureLOG());
  }
}
