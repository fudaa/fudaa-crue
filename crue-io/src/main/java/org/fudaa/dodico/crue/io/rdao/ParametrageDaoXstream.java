/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.rdao;

import com.thoughtworks.xstream.XStream;

/**
 *
 * @author deniger
 */
public class ParametrageDaoXstream {
  public static void configureXstream(XStream xstream) {
    xstream.alias("Parametrage", ParametrageDao.class);
    xstream.alias("Delimiteur", ParametrageDao.DelimiteurDao.class);
    xstream.useAttributeFor(ParametrageDao.DelimiteurDao.class, "Nom");
    xstream.useAttributeFor(ParametrageDao.DelimiteurDao.class, "Chaine");
    
    xstream.addImplicitCollection(ParametrageDao.class, "Delimiteur", ParametrageDao.DelimiteurDao.class);

  }
}
