/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.ores.old;

public class DaoOrdResBranchePdcOld {
  // WARN: l'ordre des champs est important car utilise par l'ecriture/lecture de ORES

  private boolean ddeDz;

  public boolean getDdeDz() {
    return ddeDz;
  }

  /**
   * @param ddeDz the ddeDz to set
   */
  public void setDdeDz(final boolean ddeDz) {
    this.ddeDz = ddeDz;
  }

  @Override
  public String toString() {
    return "OrdResBranchePdc [ddeDz=" + ddeDz + "]";
  }
}
