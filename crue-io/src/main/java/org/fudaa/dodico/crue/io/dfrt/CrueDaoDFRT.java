/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dfrt;

import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.io.common.AbstractDaoFloatLoi.DaoLoiFF;

/**
 * Classe persistante qui reprend la meme structure que le fichier xml DFRT - Fichier des donnees de frottement (xml) A
 * persister telle qu'elle.
 * 
 * @author Adrien Hadoux
 */
public class CrueDaoDFRT extends AbstractCrueDao {

  /**
   * Il faut ajouter un parametre dans xstream pour lui indiquer de degager les balises 'collections' car il n'y en a
   * pas dans ce fichier.
   */
  protected List<DaoLoiFF> LoiFFs;

}
