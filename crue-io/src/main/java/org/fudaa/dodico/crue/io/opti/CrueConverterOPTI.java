/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.opti;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.opti.CrueDaoStructureOPTI.InterpolLineaire;
import org.fudaa.dodico.crue.io.opti.CrueDaoStructureOPTI.InterpolSaintVenant;
import org.fudaa.dodico.crue.io.opti.CrueDaoStructureOPTI.MethodesInterpolations;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.EnumMethodeInterpol;
import org.fudaa.dodico.crue.metier.emh.EnumRegle;
import org.fudaa.dodico.crue.metier.emh.EnumRegleInterpolation;
import org.fudaa.dodico.crue.metier.emh.OrdPrtCIniModeleBase;
import org.fudaa.dodico.crue.metier.emh.Regle;
import org.fudaa.dodico.crue.metier.emh.Sorties;
import org.fudaa.dodico.crue.metier.emh.ValParam;
import org.fudaa.dodico.crue.metier.emh.ValParamDouble;

/**
 * Creation des objets methodes interpolations
 *
 * @author Adrien Hadoux
 */
public class CrueConverterOPTI implements CrueDataConverter<CrueDaoOPTI, OrdPrtCIniModeleBase> {

  private final static Logger LOGGER = Logger.getLogger(CrueConverterOPTI.class.getName());
  private final boolean ignoreTolStQ;


  public CrueConverterOPTI(boolean ignoreTolStQ) {
    this.ignoreTolStQ = ignoreTolStQ;
  }

  private BidiMap getCorrespondance() {
    final BidiMap daoToRegle = new DualHashBidiMap();
    daoToRegle.put(CrueDaoStructureOPTI.RegleQbrUniforme.class, EnumRegle.REGLE_QBR_UNIFORME);
    daoToRegle.put(CrueDaoStructureOPTI.RegleQnd.class, EnumRegle.REGLE_QND);
    return daoToRegle;
  }

  @Override
  public OrdPrtCIniModeleBase getConverterData(final CrueData in) {
    return in.getOPTI();
  }

  @Override
  public OrdPrtCIniModeleBase convertDaoToMetier(final CrueDaoOPTI dao, final CrueData dataLinked,
                                                 final CtuluLog ctuluLog) {
    /*
     * NB Adrien:pour le moment on ne gère qu'un seul élément, liste en backup.
     */
    final OrdPrtCIniModeleBase metier = dataLinked.getOrCreateOPTI();
    if (dao.getListeInterpol() != null) {
      for (final MethodesInterpolations interpol : dao.getListeInterpol()) {

        // -- ajout de l'enum --//
        final EnumMethodeInterpol type = interpol.getType();
        metier.setMethodeInterpol(type);
        if (EnumMethodeInterpol.SAINT_VENANT.equals(type)) {
          InterpolSaintVenant interpolSaintVenant = (InterpolSaintVenant) interpol;
          metier.addValParam(new ValParamDouble(EnumRegleInterpolation.TOL_ND_Z.getVariableName(), interpolSaintVenant.Pm_TolNdZ));
          double tolstq = interpolSaintVenant.Pm_TolStQ;
          if (ignoreTolStQ) {
            //on utilise la valeur par défaut pour les versions 1.1, 1.2
            tolstq = dataLinked.getCrueConfigMetier().getDefaultDoubleValue(EnumRegleInterpolation.TOL_ST_Q.getVariableName());
          }
          metier.addValParam(new ValParamDouble(EnumRegleInterpolation.TOL_ST_Q.getVariableName(), tolstq));
        }
        break;
      }

    }
    if (dao.getSorties() != null) {
      metier.getSorties().initWith(dao.getSorties());
    }
    if (dao.Regles != null) {
      final Map classToEnum = getCorrespondance();
      for (final CrueDaoStructureOPTI.RegleOPTIDAO regleDAO : dao.Regles) {
        final Regle regle = metier.getRegle((EnumRegle) classToEnum.get(regleDAO.getClass()));
        regle.setActive(regleDAO.IsActive);
        regle.setSeuilDetect(regleDAO.getValue());
      }
    }
    if (dataLinked != null) {
      dataLinked.setMethodesInterpolation(metier);
    }
    return metier;
  }

  @Override
  public CrueDaoOPTI convertMetierToDao(final OrdPrtCIniModeleBase metier, final CtuluLog ctuluLog) {
    final CrueDaoOPTI dao = new CrueDaoOPTI();

    if (metier == null) {
      return dao;
    }

    MethodesInterpolations methode = null;

    final EnumMethodeInterpol interpol = metier.getMethodeInterpol();
    if (EnumMethodeInterpol.LINEAIRE.equals(interpol)) {// NOPMD
      methode = new InterpolLineaire();
    } else if (EnumMethodeInterpol.BAIGNOIRE.equals(interpol)) {
      methode = new CrueDaoStructureOPTI.InterpolBaignoire();
    } else if (EnumMethodeInterpol.INTERPOL_ZIMP_AUX_SECTIONS.equals(interpol)) {
      methode = new CrueDaoStructureOPTI.InterpolZimpAuxSections();
    } else {
      InterpolSaintVenant interpolSaintVenant = new InterpolSaintVenant();
      methode = interpolSaintVenant;
      // -- on recupere le double --//
      interpolSaintVenant.Pm_TolNdZ = ((ValParamDouble) metier.getValParam(EnumRegleInterpolation.TOL_ND_Z.getVariableName())).getValeur();
      interpolSaintVenant.Pm_TolStQ = ((ValParamDouble) metier.getValParam(EnumRegleInterpolation.TOL_ST_Q.getVariableName())).getValeur();

    }
    dao.getListeInterpol().add(methode);
    dao.setSorties(new Sorties(metier.getSorties()));
    dao.Regles = new ArrayList<>();
    final BidiMap classToEnum = getCorrespondance();
    //on les mets dans l'ordre:
    final List<Regle> rgs = new ArrayList<>();
    rgs.add(metier.getRegle(EnumRegle.REGLE_QBR_UNIFORME));
    rgs.add(metier.getRegle(EnumRegle.REGLE_QND));

    for (final Regle regleMetier : rgs) {
      final Class daoClass = (Class) classToEnum.getKey(regleMetier.getType());
      CrueDaoStructureOPTI.RegleOPTIDAO reglePersist = null;
      try {
        reglePersist = (CrueDaoStructureOPTI.RegleOPTIDAO) daoClass.newInstance();
      } catch (final Exception e) {
        LOGGER.log(Level.SEVERE, "convertMetierToDao", e);
        return dao;
      }
      reglePersist.IsActive = regleMetier.isActive();
      reglePersist.setValue(((ValParamDouble) regleMetier.getValParam()).getValeur());
      dao.Regles.add(reglePersist);
    }
    return dao;
  }
}
