/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.neuf;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.fortran.FileCharSimpleReaderAbstract;
import org.fudaa.dodico.fortran.FortranReader;

/**
 * Gere la lecteure du format crue 09. Utilise fortran reader
 *
 * @author Adrien Hadoux
 */
public abstract class AbstractCrue9Reader extends FileCharSimpleReaderAbstract<CrueIOResu<CrueData>> implements
        CtuluActivity {

  /**
   * Toutes les lignes qui commencent par * sont des dlignes caracteres
   */
  private final static String CARACT_COMMENT = "*";
  /**
   * Nb de lignes qui composent le titre en début de fichier. Valable pour dc et dh.
   */
  protected final static int NB_LINES_TITLE = 5;
  CrueData dataLinked;
  /**
   * Fin de fichier. Est notifiée par la methode lireSuite().
   */
  protected boolean eof = false;
  protected String folder;

  /**
   * Retourne une erreur pour le premier element non reconnu.
   */
  protected void errorCarteNotRecognized(final String typeLigne) {

    analyze_.addErrorFromFile("io.UnknownLine.error", in_.getLineNumber(), typeLigne);
  }

  public CrueData getDataLinked() {
    return dataLinked;
  }

  /**
   * Initialise le reader avec la lecture des commentaires.
   */
  protected void initReader() {
    in_.setCommentInOneField(CARACT_COMMENT);
    in_.setJumpCommentLine(true);
  }

  /**
   * retourne true si la ligne est null ou commence par *.
   *
   * @param lineRead
   * @return true si la ligne lue est une ligne de commentaire.
   */
  public boolean isACommentLine(final String lineRead) {

    return lineRead == null ? true : lineRead.startsWith(CARACT_COMMENT);

  }

  protected boolean isEof() {
    return eof;
  }

  /**
   * Il faut gérer les fin d'exception en douceur pour pouvoir terminer les boucles des algorithmes et les remplissages de structure. Pour
   * fortranReader eof => getLine=null ce cas est trés bien géré dans toutes mes conditions d'arret. Methode qui fait avancer le fortranreader jusqu'à
   * la prochaine ligne non commentaire. ACHTUNG: apres appel à cette méthode il ne faut pas faire appel à un in_.readFields();!!!! En effet le reader
   * sera positionné a la derniere ligne potable non commentaire à lire.
   *
   * @throws IOException
   */
  public void lireSuite() throws IOException {
    try {
      readAndAvoidLabel(in_);
    } catch (final EOFException e) {
      eof = true;
    }

    // return;// commentaires;

  }

  /**
   * Prend en compte le GOTO et les labels des fichiers DH.
   *
   * @param in le reader a utilser
   * @return la chaine lu
   * @throws IOException
   */
  protected String readAndAvoidLabel(final FortranReader in) throws IOException {
    in.readFields();
    String res = in.getLine().trim();
    if ("GOTO".equalsIgnoreCase(in.stringField(0))) {
      // le label a recherche
      final String label = in.stringField(1).toLowerCase().trim();
      // on lit jusqu'a retrouver la ligne commencant par LABEL
      do {
        in.readFields();
        res = in.getLine().trim();
      } while (!label.equals(in.stringField(0).toLowerCase().trim()));
      return readAndAvoidLabel(in);
    }
    return res;

  }

  /**
   * Lit l'header du fichier fortran. 5 lignes de titre Commun a DC et DH.
   *
   * @throws IOException
   */
  public void readHeader(CrueIOResu<CrueData> res) throws IOException {

    final StringBuilder comm = new StringBuilder();

    for (int i = 0; i < AbstractCrue9Reader.NB_LINES_TITLE; i++) {
      if (i > 0) {
        comm.append('\n');
      }
      comm.append(readAndAvoidLabel(in_));
    }
    res.setCrueCommentaire(comm.toString());

    // -- on lit la suite --//
    lireSuite();

  }

  public void setDataLinked(final CrueData dataLinked) {
    this.dataLinked = dataLinked;
  }

  @Override
  public void setFile(final File _f) {
    try {
      folder = _f.getParentFile().toURL().toExternalForm();
    } catch (final MalformedURLException e) {
      Logger.getLogger(getClass().getName()).log(Level.SEVERE, "setFile " + _f, e);
    }
    setResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    analyze_.setDesc("read.file");
    analyze_.setDescriptionArgs(_f.getAbsolutePath());

    Reader r = null;
    try {
      r = new InputStreamReader(new FileInputStream(_f), getCrue9Charset());
      processFile(_f);
    } catch (final FileNotFoundException _e) {
      analyze_.addSevereError(DodicoLib.getS("Fichier inconnu"));
    }
    if (r != null) {
      setFile(r);
    }
  }

  /**
   * @deprecated user setFile(URL)
   */
  @Deprecated
  @Override
  public void setFile(final Reader _r) {
    analyze_.setDesc("read.file");
    analyze_.setDescriptionArgs("Crue 9");
    super.setFile(_r);
    setResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
  }

  /**
   * @param res le resourceBundle attacée aux messages.
   */
  public void setResourceBundle(final ResourceBundle res) {
    if (analyze_ == null) {
      analyze_ = new CtuluLog(res);
    } else {
      analyze_.setDefaultResourceBundle(res);
    }
  }

  /**
   * @param _r l'url du fichier
   */
  public void setFile(final URL _r) {
    try {
      final String urlAll = _r.toExternalForm();
      folder = urlAll.substring(0, urlAll.lastIndexOf('/') + 1);
      setResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
      analyze_.setResource(_r.toString());
      analyze_.setDesc("read.file");
      analyze_.setDescriptionArgs(_r.toString());
      super.setFile(new InputStreamReader(_r.openStream(), getCrue9Charset()));
    } catch (final IOException e) {
      Logger.getLogger(getClass().getName()).log(Level.SEVERE, "setFile " + _r, e);
    }
  }

  public static Charset getCrue9Charset() {
    return Charset.forName("Windows-1252");
  }
}
