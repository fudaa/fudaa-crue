/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;

import java.io.File;

/**
 * @param <M> la classe métier generee
 * @author deniger
 */
public abstract class CustomFileFormat<M> implements Comparable {
  // Le nombre de fichiers demande par ce format.
  final private int nbFile;
  protected String[] extensions;
  protected String nom;
  protected String id;
  protected String description;
  // le type du fichier projet,maillage,autre,...
  protected Object type;

  /**
   * @param _nbFile le nombre de fichier requis par ce format
   */
  public CustomFileFormat(final int _nbFile) {
    if (_nbFile < 1) {
      throw new IllegalArgumentException("Nombre de fichier strictement positif");
    }
    nbFile = _nbFile;
  }

  /**
   * @return les identifiants des versions
   */
  public String[] getVersions() {
    return new String[]{getLastVersion()};
  }

  /**
   * @return l'identifiant de la version la plus recente
   */
  public abstract String getLastVersion();

  /**
   * Returns the description.
   *
   * @return String
   */
  public String getDescription() {
    return description;
  }

  /**
   * @return l'identifiant de ce format
   */
  public String getID() {
    return id;
  }

  /**
   * Returns the extensions.
   *
   * @return String[]
   */
  public String[] getExtensions() {
    return extensions;
  }

  /**
   * @return Object
   */
  public Object getType() {
    return type;
  }

  /**
   * Returns the nom.
   *
   * @return String
   */
  public String getName() {
    return nom;
  }

  /**
   * Renvoie a partir du nombre de fichier et des extensions du format les fichiers correspondants (si le nombre de
   * fichier est 1, fichier donne est renvoye).
   *
   * @param fInit le fichier initiale
   * @return un tableau de taille getNbFile() avec les fichiers existants et correspondant aux extensions.
   */
  public final File[] getFile(final File fInit) {
    final File[] r = new File[nbFile];
    if (fInit == null) {
      return r;
    }
    if (nbFile == 1) {
      return new File[]{fInit};
    }
    final String base = CtuluLibFile.getSansExtension(fInit.getAbsolutePath());
    for (int i = 0; i < nbFile; i++) {
      r[i] = new File(base + CtuluLibString.DOT + extensions[i]);
    }
    return r;
  }

  /**
   * Compare les noms des formats.
   */
  @Override
  public int compareTo(final Object o) {
    if (!(o instanceof CustomFileFormat)) {
      throw new IllegalArgumentException("o doit etre de type FileFormat");
    }
    return getName().compareTo(((CustomFileFormat) o).getName());
  }

  /**
   *
   */
  @Override
  public String toString() {
    return getName();
  }
}
