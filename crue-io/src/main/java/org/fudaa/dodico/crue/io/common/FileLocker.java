/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;

/**
 *
 * @author deniger
 */
public class FileLocker {

  public static final String LOCKEDBY = "LockedBy";
  public static final String LOCKEDSINCE = "LockedSince";

  public static class Data {

    String userName;
    String time;

    public Data() {
    }

    public Data(final String userName, final String time) {
      this.userName = userName;
      this.time = time;
    }
    
    

    public String getTime() {
      return time;
    }

    public String getUserName() {
      return userName;
    }
  }

  public FileLocker() {
  }

  public File getLockFile(final File initFile) {
    if (initFile == null || initFile.isDirectory()) {
      return null;
    }
    return new File(initFile.getParentFile(), initFile.getName() + ".lock");
  }

  public List<File> getLockedFiles(final Collection<File> in) {
    if (in == null) {
      return null;
    }
    final List<File> res = new ArrayList<>(in.size());
    for (final File file : in) {
      if (isLock(file)) {
        res.add(file);
      }
    }
    return res;
  }

  public boolean isLock(final File file) {
    final File locked = getLockFile(file);
    return locked != null && locked.isFile();
  }

  public Data isLockedBy(final File file) {
    if (!isLock(file)) {
      return null;
    }
    final File lockFile = getLockFile(file);
    if (lockFile != null) {
      final Properties p = new Properties();
      InputStream in = null;
      try {
        in = new FileInputStream(lockFile);
        p.loadFromXML(in);
      } catch (final IOException e) {
        Logger.getLogger(FileLocker.class.getName()).log(Level.WARNING, "isLockedBy", e);
        return null;
      } finally {
        CtuluLibFile.close(in);
      }
      final Data res = new Data();
      res.userName = p.getProperty(LOCKEDBY);
      res.time = p.getProperty(LOCKEDSINCE);
      return res;
    }
    return null;
  }

  public boolean lock(final File f, final boolean force, final ConnexionInformation connexionInformation) {
    if (isLock(f)) {
      if (force) {
        final boolean unlock = unlock(f);
        if (!unlock) {
          return false;
        }
      } else {
        return false;
      }
    }
    final File lockFile = getLockFile(f);
    boolean createNewFile = false;
    try {
      createNewFile = lockFile.createNewFile();
    } catch (final IOException iOException) {
      Logger.getLogger(FileLocker.class.getName()).log(Level.WARNING, "cant create lockFile", iOException);
    }
    if (!createNewFile) {
      Logger.getLogger(FileLocker.class.getName()).log(Level.WARNING, "cant create lockFile for ", f);
      return false;
    }
    final Properties p = new Properties();
    p.put(LOCKEDBY, connexionInformation.getCurrentUser());
    final String date = getDateFormatted(connexionInformation.getCurrentDate());
    p.put(LOCKEDSINCE, date);
    OutputStream out = null;
    try {
      out = new FileOutputStream(lockFile);
      p.storeToXML(out, BusinessMessages.getString("lockedFileComment"), "UTF-8");
    } catch (final IOException e) {
      Logger.getLogger(FileLocker.class.getName()).log(Level.WARNING, "isLockedBy", e);
      return false;
    } finally {
      CtuluLibFile.close(out);
    }
    return true;

  }

  public static String getDateFormatted(final LocalDateTime time) {
    return DateTimeFormat.shortDateTime().print(time);
  }

  public boolean unlock(final File file) {
    final File locked = getLockFile(file);
    if (locked != null) {
      return locked.delete();
    }
    return false;
  }

  /**
   * Si le fichier est déjà locke, ne fait rien.
   * @param file
   * @return 
   */
  public boolean lock(final File file, final ConnexionInformation connexionInformation) {

    return lock(file, false,connexionInformation);
  }
}
