/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.io.dlhy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.AbstractDaoLoi;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.io.common.AbstractDaoFloatLoi.DaoLoiDF;
import org.fudaa.dodico.crue.io.common.AbstractDaoFloatLoi.DaoLoiFF;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.LoiDF;
import org.fudaa.dodico.crue.metier.emh.LoiFF;

/**
 * Converter qui remplit les structures dao avec les objets metier et inversement.
 *
 * @author Adrien Hadoux, Carole Delhaye
 */
public class CrueConverterDLHY implements CrueDataConverter<CrueDaoDLHY, List<Loi>> {

    /**
     * Convertit les objets persistants en objets métier
     */
    @Override
    public List<Loi> convertDaoToMetier(final CrueDaoDLHY dao, final CrueData dataLinked, final CtuluLog ctuluLog) {

        final List<AbstractDaoLoi> inLois = dao.Lois;
        final Map<Class, ConvertLoi> corr = new HashMap<>();
        corr.put(DaoLoiDF.class, new ConvertLoiDF());
        corr.put(DaoLoiFF.class, new ConvertLoiFF());

        final List<AbstractLoi> outLoiInit = ConvertLoiHelper.convertDaoToMetier(ctuluLog, inLois, corr);
        final List<Loi> outLoi = new ArrayList<>();
        for (final AbstractLoi abstractLoi : outLoiInit) {
            outLoi.add((Loi) abstractLoi);
        }
        if (dataLinked != null) {
            dataLinked.getLoiConteneur().setLois(outLoi);
        }
        return outLoi;
    }

    @Override
    public List<Loi> getConverterData(final CrueData in) {
        return in.getLois();
    }

    /**
     * Convertit les objets métier en objets persistants
     */
    @Override
    public CrueDaoDLHY convertMetierToDao(final List<Loi> metier, final CtuluLog ctuluLog) {

        final CrueDaoDLHY res = new CrueDaoDLHY();

        res.Lois = new ArrayList<>(metier.size());
        final Map<Class, ConvertLoi> corr = new HashMap<>();
        corr.put(LoiDF.class, new ConvertLoiDF());
        corr.put(LoiFF.class, new ConvertLoiFF());

        for (final Loi loi : metier) {
            final ConvertLoi cs = corr.get(loi.getClass());
            res.Lois.add(cs.metiertoDao(loi));
        }

        return res;
    }
}
