/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loader.action;

import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.ProjectNormalizableCallable;
import org.fudaa.dodico.crue.projet.clean.ScenarioCleanData;
import org.fudaa.dodico.crue.projet.select.DoublonScenarioFinder;
import org.fudaa.dodico.crue.projet.select.ToNormalizeScenarioFinder;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

import java.util.List;

/**
 * Permet de trouver les scenarios à nettoyer ( à normaliser ou si doublons trouvés).
 *
 * @author Frederic Deniger
 * @see ToNormalizeScenarioFinder
 */
public class ProjectFindScenarioToCleanProgressRunnable implements ProgressRunnable<ScenarioCleanData> {
  private final EMHProjet projet;

  public ProjectFindScenarioToCleanProgressRunnable(EMHProjet projet) {
    this.projet = projet;
  }

  @Override
  public ScenarioCleanData run(ProgressHandle handle) {
    handle.switchToIndeterminate();
    ScenarioCleanData scenarioCleanData = new ScenarioCleanData();
    final List<ProjectNormalizableCallable.Result> toNormalize = new ToNormalizeScenarioFinder(projet).findToNormalizeOrWithErrors();
    if (toNormalize != null) {
      scenarioCleanData.scenarioToNormalizeOrWithErrors.addAll(toNormalize);
    }
    scenarioCleanData.scenarioNamesToDelete.addAll(TransformerHelper.toNom(new DoublonScenarioFinder(projet).getDoublons()));
    return scenarioCleanData;
  }
}
