package org.fudaa.fudaa.crue.loader;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.metier.ScenarioAutoModifiedState;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.ScenarioLoaderOperation;
import org.fudaa.fudaa.crue.common.CommonMessage;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

import java.awt.*;

import static org.openide.util.NbBundle.getMessage;

/**
 * Classe abstraite pour les services gerant le cycle de vie d'un scenario.
 * <table  border="1"  cellpadding="3" cellspacing="0"  summary="lookups">
 * <tr>
 * <th align="left">Lookup</th>
 * <th align="left">Commentaire</th>
 * <th align="left">Méthode utilisant le lookup</th>
 * </tr>
 * <tr>
 * <td>{@link EMHScenario}</td>
 * <td>
 * Le scenario chargé
 * </td>
 * <td><code>{@link #getScenarioLoaded()}</code></td>
 * </tr>
 * <tr>
 * <td>{@link ManagerEMHScenario}</td>
 * <td>	Le ManagerEMHScenario chargé: contient en plus des infos sur le run chargé mais non utile ici
 * <td><code>{@link #getManagerScenarioLoaded()}</code></td>
 * </tr>
 * <tr>
 * <td>{@link EMHRun}</td>
 * <td>	Le run chargé</td>
 * <td><code>{@link #getRunLoaded()}</code></td>
 * </tr>
 * * <tr>
 * <td>{@link org.fudaa.ctulu.CtuluLogGroup}</td>
 * <td>Bilan de la dernière opération effectuée dans la perspective</td>
 * <td><code>{@link #getLastLogsGroup()}</code></td>
 * </tr>
 * </table>
 *
 * @author deniger
 */
public class ScenarioLoaderServiceAbstract implements Lookup.Provider {
  //la perspective utilisant cette classe.
  private final PerspectiveEnum perspectiveToActive;
  //les lookups:
  protected final InstanceContent dynamicContent = new InstanceContent();
  protected final Lookup lookup = new AbstractLookup(dynamicContent);
  //pour suivre la selection de la perspective.
  private final SelectedPerspectiveService service = Lookup.getDefault().lookup(SelectedPerspectiveService.class);

  /**
   * @param perspectiveToActive la perspective a activer après le chargement d'un scenario.
   */
  public ScenarioLoaderServiceAbstract(PerspectiveEnum perspectiveToActive) {
    this.perspectiveToActive = perspectiveToActive;
  }

  /**
   * @return le scenario chargé
   */
  public EMHScenario getScenarioLoaded() {
    return lookup.lookup(EMHScenario.class);
  }

  /**
   * @return true si scenario chargé
   */
  public boolean isScenarioLoaded() {
    return getScenarioLoaded() != null;
  }

  /**
   * @return le managerEMHScenario chargé
   */
  public ManagerEMHScenario getManagerScenarioLoaded() {
    return lookup.lookup(ManagerEMHScenario.class);
  }

  /**
   * @return le run chargé.
   */
  public EMHRun getRunLoaded() {
    return lookup.lookup(EMHRun.class);
  }

  /**
   * @return true si run chargé
   */
  protected boolean isRunLoaded() {
    return getRunLoaded() != null;
  }

  @Override
  public Lookup getLookup() {
    return lookup;
  }

  /**
   * @return bilan de la dernière opération.
   */
  public CtuluLogGroup getLastLogsGroup() {
    return lookup.lookup(CtuluLogGroup.class);
  }

  /**
   * @param ctuluLogGroup le nouveau bilan a persister.
   */
  protected void updateLastLogsGroup(CtuluLogGroup ctuluLogGroup) {
    CtuluLogGroup lastLogsGroup = getLastLogsGroup();
    //on enleve le précédent
    if (lastLogsGroup != null) {
      dynamicContent.remove(lastLogsGroup);
    }
    //et ajoute le nouveau.
    if (ctuluLogGroup != null) {
      dynamicContent.add(ctuluLogGroup);
    }
  }

  /**
   * Decharge l'ensemble: Scenario, ManagerEMHScenario, Run et le dernier bilan.
   *
   * @return toujours true
   */
  public boolean unloadScenario() {
    final EMHRun runLoaded = getRunLoaded();
    if (runLoaded != null) {
      dynamicContent.remove(getRunLoaded());
    }
    CtuluLogGroup lastLogsGroup = getLastLogsGroup();
    if (lastLogsGroup != null) {
      dynamicContent.remove(lastLogsGroup);
    }
    final ManagerEMHScenario managerScenarioLoaded = getManagerScenarioLoaded();
    if (managerScenarioLoaded != null) {
      dynamicContent.remove(managerScenarioLoaded);
    }
    final EMHScenario scenarioLoaded = getScenarioLoaded();
    if (scenarioLoaded != null) {
      dynamicContent.remove(scenarioLoaded);
    }
    return true;
  }

  /**
   * Lors du chargement d'un scenario, des modifications automatiques peuvent être apportées. Si un service veut afficher ces modifications, il doit redéfinir
   * cette methode.
   *
   * @param modificationDoneWhileLoading les modifications effectuées.
   */
  protected void manageAutomaticModification(ScenarioAutoModifiedState modificationDoneWhileLoading) {
  }

  /**
   * Chargement du scenario/run avec mis à jour du bilan et activation de la perspective (voir constructeur)
   *
   * @param projet le projet
   * @param managerScenario le scenario a charger
   * @param run le run a chargé. Si null, ignoré.
   * @return true si chargé
   * @see ProjectLoadProgressRunnable
   * @see org.fudaa.dodico.crue.projet.ScenarioLoader
   */
  protected EMHScenario loadScenario(final EMHProjet projet, final ManagerEMHScenario managerScenario, final EMHRun run) {
    //Chargement
    if (projet == null) {
      DialogHelper.showError(getMessage(ScenarioLoaderServiceAbstract.class, "ScenarioLoaderServiceAbstract.ProjectNotExisting"));
      return null;
    }
    if (managerScenario == null) {
      DialogHelper
          .showError(getMessage(ScenarioLoaderServiceAbstract.class, "ScenarioLoaderServiceAbstract.ScenarioNotExisting", projet.getInfos().getEtuFile().getAbsolutePath()));
      return null;
    }
    final ProjectLoadProgressRunnable projectLoadProgressRunnable = new ProjectLoadProgressRunnable(projet, managerScenario, run);
    final ScenarioLoaderOperation load = doLoad(projectLoadProgressRunnable, LoaderService.getNom(managerScenario, run));
    //l'opération est terminée. On se place dans le thread swing pour mettre à jour les composants swing...
    EventQueue.invokeLater(() -> {
      final EMHScenario result = load == null ? null : load.getResult();
      if (result != null) {
        //on active la perspective
        service.activePerspective(perspectiveToActive);
        //on ferme eventuellement l'ancien scenario
        unloadScenario();
        //on charge les logs et le manager
        dynamicContent.add(load.getLogs());
        dynamicContent.add(managerScenario);
        //on charge le run
        if (run != null) {
          dynamicContent.add(run);
        }
        //et finalement le scenario
        //attention l'ordre est important car beaucoup de service ecoute l'objet EMHScenario.
        //en l'ajoutant à la fin, on est sur que ces services auront aussi accès aux autres données ( bilan, run, manager).
        dynamicContent.add(load.getResult());
      }
      if (load != null && (load.getLogs().containsError() || load.getLogs().containsFatalError())) {
        LogsDisplayer.displayError(load.getLogs(), CommonMessage.getMessage("LoadScenarioBilan.DialogTitle", managerScenario.getNom()));
      }
      //et eventuellement les modifications opérées pendant le chargement.
      if (result != null) {
        manageAutomaticModification(load.getAutoModifiedState());
      }
    });
    //return le scenario
    return load == null ? null : load.getResult();
  }

  private ScenarioLoaderOperation doLoad(final ProjectLoadProgressRunnable projectLoadProgressRunnable, final String nom) {
    ScenarioLoaderOperation load;
    if (EventQueue.isDispatchThread()) {
      load = CrueProgressUtils.showProgressDialogAndRun(projectLoadProgressRunnable, nom);
    } else {
      load = projectLoadProgressRunnable.load();
    }
    return load;
  }
}
