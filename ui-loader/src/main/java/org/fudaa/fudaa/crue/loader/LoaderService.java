package org.fudaa.fudaa.crue.loader;

import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory.VersionResult;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.ScenarioLoader;
import org.fudaa.dodico.crue.projet.ScenarioLoaderOperation;
import org.fudaa.fudaa.crue.common.CommonMessage;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.io.File;

import static org.fudaa.fudaa.crue.loader.Bundle.*;

/**
 * Service permettant de charger des projets et des scenarios en prenant en compte la configuration site courante.
 * Pas de lookup dans ce service.
 *
 * @author deniger
 */
@ServiceProvider(service = LoaderService.class)
public class LoaderService {
  /**
   * le service gerant la configuration site.
   */
  private final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

  /**
   * @return Un FileFilter par defaut pour charger un fichier etu.
   */
  public static FileFilter createEtuFileFilter() {
    return Crue10FileFormat.createFileFilter(CrueFileType.ETU, EtuFile());
  }

  /**
   * @param scenario le scenario. Si null renvoie chaine vide
   * @param run le run. Si null renvoie uniquement le nom du scenario.
   * @return chaine caractérisant le couple. Si run est nom renvoie uniquement le nom du scenario.
   */
  public static String getNom(final ManagerEMHScenario scenario, EMHRun run) {
    if (scenario == null) {
      return "";
    }
    if (run != null) {
      return scenario.getNom() + "/" + run.getNom();
    }
    return scenario.getNom();
  }

  /**
   * @return {@link ConfigurationManagerService#getDefaultDataHome() }
   */
  public File getDefaultDataHome() {
    return configurationManagerService.getDefaultDataHome();
  }

  /**
   * @return {@link org.fudaa.dodico.crue.config.coeur.CoeurManager#getSiteDir() } soit le dossier contenant les fichiers de configuration site.
   */
  public File getSiteDir() {
    return configurationManagerService.getCoeurManager().getSiteDir();
  }

  /**
   * @param projet le projet à charger
   * @param scenario le scenario à charger
   * @param run le run à charger ( peut etre null)
   * @return le scenario chargé
   * @see #load(org.fudaa.dodico.crue.metier.etude.EMHProjet, org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario,
   *     org.fudaa.dodico.crue.metier.etude.EMHRun, boolean)
   */
  public EMHScenario load(final EMHProjet projet, final ManagerEMHScenario scenario, EMHRun run) {
    return load(projet, scenario, run, false);
  }

  /**
   * @param projet le projet à charger
   * @param scenario le scenario à charger
   * @param run le run à charger ( peut etre null)
   * @param displayOnlyIfError si true, le bilan de l'opération sera affiché que si des erreurs sont présentes.
   * @return le scenario /run chargé
   * @see ScenarioLoader
   */
  public EMHScenario load(final EMHProjet projet, final ManagerEMHScenario scenario, EMHRun run, boolean displayOnlyIfError) {
    ScenarioLoaderOperation load;
    //si on est dans le thread swing, on bloque l'interface le temps du chargement -> pour eviter de figer la fenetre
    if (EventQueue.isDispatchThread()) {
      load = CrueProgressUtils.showProgressDialogAndRun(new ProjectLoadProgressRunnable(projet, scenario, run), scenario.getNom());
    } else {
      //sinon on charge le scenario ( thread non swing).
      ScenarioLoader loader = new ScenarioLoader(scenario, projet, projet.getCoeurConfig());
      load = loader.load(run);
    }
    //recuperation du bilan
    final boolean isLoadedWithoutFatalError = load.getResult() != null && !load.getLogs().containsFatalError();
    //on affiche le bilan en prenant en compte la valeur de displayOnlyIfError
    if (displayOnlyIfError) {
      if (load.getLogs().containsError() || load.getLogs().containsFatalError()) {
        LogsDisplayer.displayError(load.getLogs(), CommonMessage.getMessage("LoadScenarioBilan.DialogTitle", getNom(scenario, run)));
      }
    } else if (load.getLogs().containsSomething()) {
      LogsDisplayer.displayError(load.getLogs(), CommonMessage.getMessage("LoadScenarioBilan.DialogTitle", getNom(scenario, run)));
    }
    //on renvoie le resultats si pas d'erreur  fatale
    return isLoadedWithoutFatalError ? load.getResult() : null;
  }

  /**
   * Charge le fichier etu en respectant la configuration des coeurs (coeur par défaut, si plusieurs coeurs choix à faire...).
   *
   * @param etuFileToOpen: le fichier etu à ouvrir.si null renvoie null.
   * @return le resultat de l'ouverture avec le projet et le fichier etu.
   * @see OpenFileProcess
   */
  public ProjectLoadContainer loadProject(File etuFileToOpen) {
    if (etuFileToOpen != null) {
      CoeurConfigContrat findCoeurConfig = findCoeurConfig(etuFileToOpen);
      if (findCoeurConfig == null) {
        return null;
      }
      OpenFileProcess runner = new OpenFileProcess(etuFileToOpen, findCoeurConfig);
      final EMHProjet projet = runner.run(null);
      final ProjectLoadContainer projectLoadContainer = new ProjectLoadContainer(etuFileToOpen, projet);
      projectLoadContainer.setLogs(runner.getLogs());
      return projectLoadContainer;
    }
    return null;
  }

  /**
   * Ouvre un filechooser permettant de choisir un fichier etu. le dossier par défaut est donné par {@link #getDefaultDataHome() }.
   *
   * @return le fichier etu choisi par l'utilisateur
   */
  public File chooseEtuFile() {
    File home = getDefaultDataHome();
    //on construit le filechooser:
    final FileChooserBuilder builder = new FileChooserBuilder(LoaderService.class).setTitle(ChooseEtu()).
        setDefaultWorkingDirectory(home).setApproveText(OpenEtu());
    builder.setFilesOnly(true);
    final FileFilter fileFilter = createEtuFileFilter();
    builder.setFileFilter(fileFilter);
    //pour afficher différement les fichier etu:
    final EtuFileIconProvider badgeProvider = new EtuFileIconProvider(fileFilter);
    builder.setBadgeProvider(badgeProvider);
    //choix du fichier
    return builder.showOpenDialog();
  }

  /**
   * @param etuFile le fichier etu.xml
   * @return la configuration coeur correspondant au fichier etu.
   * @see ConfigurationManagerService#chooseCoeur(java.lang.String)
   */
  private CoeurConfigContrat findCoeurConfig(File etuFile) {
    VersionResult findVersion = Crue10FileFormatFactory.findVersion(etuFile);
    if (findVersion.getLog().isNotEmpty()) {
      LogsDisplayer.displayError(findVersion.getLog(), null);
    }
    return configurationManagerService.chooseCoeur(findVersion.getVersion());
  }

  /**
   * @return permet à l'utilisateur de choisir le fichier etu et l'ouvre par la suite.
   * @see #loadProject(java.io.File)
   */
  public ProjectLoadContainer loadProject() {
    File toOpen = chooseEtuFile();
    return loadProject(toOpen);
  }
}
