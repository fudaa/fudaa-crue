/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loader.action;

import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.ProjectNormalizableCallable;
import org.fudaa.dodico.crue.projet.select.ToNormalizeScenarioFinder;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

import java.util.List;

/**
 * Permet de trouver les scenarios à normaliser.
 *
 * @author Frederic Deniger
 * @see ToNormalizeScenarioFinder
 */
public class ProjectFindScenarioToNormalizeProgressRunnable implements ProgressRunnable<List<ProjectNormalizableCallable.Result>> {
  private final EMHProjet projet;

  public ProjectFindScenarioToNormalizeProgressRunnable(EMHProjet projet) {
    this.projet = projet;
  }

  @Override
  public List<ProjectNormalizableCallable.Result> run(ProgressHandle handle) {
    return new ProjectFindScenarioToCleanProgressRunnable(projet).run(handle).scenarioToNormalizeOrWithErrors;
  }
}
