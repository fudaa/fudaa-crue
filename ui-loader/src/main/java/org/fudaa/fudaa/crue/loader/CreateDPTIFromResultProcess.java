/*
 * To change this template, choose Tools | Templates and open the template in the editor.
 */
package org.fudaa.fudaa.crue.loader;

import com.jidesoft.swing.JideScrollPane;
import com.jidesoft.swing.SearchableUtils;
import com.memoire.bu.BuBorderLayout;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.cini.CiniImportKey;
import org.fudaa.dodico.crue.metier.cini.ResultatDPTIExtractor;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.jdesktop.swingx.JXTable;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Map;
import java.util.Vector;

import static org.openide.util.NbBundle.getMessage;

/**
 * Permet d'extraire les donnees correspondantes à des conditions initiales.
 *
 * @author Fred Deniger
 */
public class CreateDPTIFromResultProcess implements ProgressRunnable<CtuluIOResult<Map<CiniImportKey, Object>>> {
  private final EMHScenario scenario;
  private final CrueConfigMetier ccm;
  private final ResultatTimeKey calcul;

  public CreateDPTIFromResultProcess(CrueConfigMetier ccm, EMHScenario scenario, ResultatTimeKey calcul) {
    this.scenario = scenario;
    this.ccm = ccm;
    this.calcul = calcul;
  }

  @Override
  public CtuluIOResult<Map<CiniImportKey, Object>> run(ProgressHandle handle) {
    if (handle != null) {
      handle.switchToIndeterminate();
    }
    return new ResultatDPTIExtractor(scenario, ccm).extract(calcul);
  }

  public static void displayPreview(EMHScenario scenario, ResultatTimeKey calcul, CrueConfigMetier crueConfigMetier) {
    final Map<CiniImportKey, Object> ciniKeys = extract(crueConfigMetier,scenario, calcul);
    if (ciniKeys.isEmpty()) {
      final String title = getMessage(CreateDPTIFromResultProcess.class, "CreateDPTIFromResultProcess.Progress");
      DialogHelper.showNotifyOperationTermine(title, getMessage(CreateDPTIFromResultProcess.class, "CreateDPTIFromResultProcess.NoDPTIFound"));
    } else {
      DefaultTableModel model = buildTableModel(ciniKeys);
      JXTable table = new JXTable(model);
      final NumberFormat formatter = crueConfigMetier.getProperty(CrueConfigMetierConstants.PROP_ZINI).getFormatter(DecimalFormatEpsilonEnum.PRESENTATION);
      table.getColumn(2).setCellRenderer(new CtuluCellTextRenderer() {
        @Override
        protected void setValue(Object _value) {
          if (_value instanceof Double) {
            super.setValue(formatter.format(_value));
            setToolTipText(_value.toString());
          } else {
            setToolTipText(_value.toString());
            super.setValue(_value);
          }
        }
      });
      SearchableUtils.installSearchable(table);
      CtuluDialogPanel panel = new CtuluDialogPanel(false);
      panel.setLayout(new BuBorderLayout());
      panel.add(new JideScrollPane(table), BorderLayout.CENTER);
      DialogHelper.showDialogAndTable(panel, CtuluDialog.OK_OPTION, table, CreateDPTIFromResultProcess.class.getName(),
          getMessage(CreateDPTIFromResultProcess.class, "CreateDPTIFromResultProcess.DialogTitle"), DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    }
  }

  public static Map<CiniImportKey, Object> extract(CrueConfigMetier ccm, EMHScenario scenario, ResultatTimeKey calcul) {
    final CtuluIOResult<Map<CiniImportKey, Object>> ctuluIOResult = CrueProgressUtils.showProgressDialogAndRun(new CreateDPTIFromResultProcess(ccm, scenario, calcul),
        getMessage(CreateDPTIFromResultProcess.class, "CreateDPTIFromResultProcess.Progress"));
    if (ctuluIOResult.getAnalyze().containsErrorOrSevereError()) {
      LogsDisplayer.displayError(ctuluIOResult.getAnalyze(), getMessage(CreateDPTIFromResultProcess.class, "CreateDPTIFromResultProcess.Progress"));
    }
    return ctuluIOResult.getSource();
  }

  /**
   * @param ciniKeys les cles
   * @return un table modele comportant les données
   */
  private static DefaultTableModel buildTableModel(Map<CiniImportKey, Object> ciniKeys) {
    final Vector<Vector<Object>> data = new Vector<>(ciniKeys.size());
    Vector<String> columnNames = new Vector(Arrays.asList(
        BusinessMessages.getString("EMH.shortName"), BusinessMessages.getString("Type"), getMessage(CreateDPTIFromResultProcess.class,
            "CreateDPTIFromResultProcess.Value")));
    ciniKeys.forEach((key, value) -> {
      Vector line = new Vector(3);
      line.add(key.getEmh());
      line.add(key.getTypeValue());
      line.add(value);
      data.add(line);
    });
    return new DefaultTableModel(data, columnNames) {
      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
  }
}
