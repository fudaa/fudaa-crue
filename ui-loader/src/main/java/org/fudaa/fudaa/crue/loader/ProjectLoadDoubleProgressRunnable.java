/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loader;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.ProjectLoadCallable;
import org.fudaa.dodico.crue.projet.ScenarioLoaderOperation;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Exceptions;

/**
 * Permet de charger 2 scenario/run
 *
 * @author Frederic Deniger
 */
public class ProjectLoadDoubleProgressRunnable implements ProgressRunnable<Pair<ScenarioLoaderOperation, ScenarioLoaderOperation>> {

  final EMHProjet projet;
  final ManagerEMHScenario scenarioRef;
  final EMHRun runRef;
  final ManagerEMHScenario scenarioCible;
  final EMHRun runCible;

  public ProjectLoadDoubleProgressRunnable(EMHProjet projet, ManagerEMHScenario scenarioRef, EMHRun runRef, ManagerEMHScenario scenarioCible, EMHRun runCible) {
    this.projet = projet;
    this.scenarioRef = scenarioRef;
    this.runRef = runRef;
    this.scenarioCible = scenarioCible;
    this.runCible = runCible;
  }

  @Override
  public Pair<ScenarioLoaderOperation, ScenarioLoaderOperation> run(ProgressHandle handle) {
    handle.switchToIndeterminate();
    return load();
  }

  public Pair<ScenarioLoaderOperation, ScenarioLoaderOperation> load() {
    ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(2);
    ProjectLoadCallable ref = new ProjectLoadCallable(projet, scenarioRef, runRef);
    ProjectLoadCallable cible = new ProjectLoadCallable(projet, scenarioCible, runCible);
    try {
      List<Future<ScenarioLoaderOperation>> invokeAll = newFixedThreadPool.invokeAll(Arrays.asList(ref, cible));
      ScenarioLoaderOperation resRef = invokeAll.get(0).get();
      ScenarioLoaderOperation resCible = invokeAll.get(1).get();
      return new Pair<>(resRef, resCible);
    } catch (Exception ex) {
      Exceptions.printStackTrace(ex);
    }
    return null;
  }
}
