package org.fudaa.fudaa.crue.loader;

import java.io.File;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;

/**
 *
 * Contient le résultat d'une ouverture d'un fichier etu
 *
 * @author deniger
 */
public class ProjectLoadContainer {

  private final File etuFile;
  private final EMHProjet projet;
  CtuluLogGroup logs;

  /**
   *
   * @param etuFile le fichier etu
   * @param projet  le projet correspondant
   */
  public ProjectLoadContainer(File etuFile, EMHProjet projet) {
    this.etuFile = etuFile;
    this.projet = projet;
  }

  public CtuluLogGroup getLogs() {
    return logs;
  }

  public void setLogs(CtuluLogGroup logs) {
    this.logs = logs;
  }

  /**
   *
   * @return le fichier etu
   */
  public File getEtuFile() {
    return etuFile;
  }

  /**
   *
   * @return le project correspondant.
   */
  public EMHProjet getProjet() {
    return projet;
  }

}
