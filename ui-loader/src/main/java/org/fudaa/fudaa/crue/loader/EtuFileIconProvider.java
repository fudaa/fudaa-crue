package org.fudaa.fudaa.crue.loader;

import java.io.File;
import javax.swing.Icon;
import javax.swing.filechooser.FileFilter;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.openide.filesystems.FileChooserBuilder.BadgeProvider;

/**
 *
 * @author deniger
 */
public class EtuFileIconProvider implements BadgeProvider {
  private final FileFilter composed;
  private final Icon icon = CrueIconsProvider.getEtuIcon();

  public EtuFileIconProvider(FileFilter composed) {
    this.composed = composed;
  }

  @Override
  public Icon getBadge(File file) {
    if (isAccepted(file)) {
      return icon;
    }
    return null;
  }

  public boolean isAccepted(File file) {
    return file.isFile() && composed.accept(file);
  }

  @Override
  public int getXOffset() {
    return 0;
  }

  @Override
  public int getYOffset() {
    return 0;
  }
  
}
