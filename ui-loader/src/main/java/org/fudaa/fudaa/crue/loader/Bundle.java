package org.fudaa.fudaa.crue.loader;

public class Bundle {
  /**
   * @return <i>Choisir fichier Etude (*.etu.xml)</i>
   * @see org.fudaa.fudaa.crue.common.services.EMHProjetService
   */
  static String ChooseEtu() {
    return org.openide.util.NbBundle.getMessage(Bundle.class, "ChooseEtu");
  }

  /**
   * @return <i>Fichier Etu</i>
   * @see org.fudaa.fudaa.crue.common.services.EMHProjetService
   */
  static String EtuFile() {
    return org.openide.util.NbBundle.getMessage(Bundle.class, "EtuFile");
  }

  /**
   * @return <i>Ouvrir l'étude</i>
   * @see org.fudaa.fudaa.crue.common.services.EMHProjetService
   */
  static String OpenEtu() {
    return org.openide.util.NbBundle.getMessage(Bundle.class, "OpenEtu");
  }

  /**
   * @return <i>Ouverture de l''étude </i>{@code arg0}
   * @see org.fudaa.fudaa.crue.common.services.EMHProjetService
   */
  static String OpenEtuFileAction(Object arg0) {
    return org.openide.util.NbBundle.getMessage(Bundle.class, "OpenEtuFileAction", arg0);
  }

  private void Bundle() {
  }
}
