/*
 GPL 2
 */
package org.fudaa.fudaa.crue.loader;

import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.ScenarioLoader;
import org.fudaa.dodico.crue.projet.ScenarioLoaderOperation;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

/**
 * permet de charger un scenario/run en etant compatible avec Netbeans RCP et {@link ProgressRunnable}
 *
 * @author Frederic Deniger
 */
public class ProjectLoadProgressRunnable implements ProgressRunnable<ScenarioLoaderOperation> {
    final EMHProjet projet;
    final ManagerEMHScenario scenario;
    final EMHRun run;

    public ProjectLoadProgressRunnable(EMHProjet projet, ManagerEMHScenario scenario, EMHRun run) {
        this.projet = projet;
        this.scenario = scenario;
        this.run = run;
    }

    /**
     * met à jour la barre de progression (indeterminate) et charge le scenario/run.
     *
     * @param handle la progression
     * @return le resultat du chargement: jamais null
     */
    @Override
    public ScenarioLoaderOperation run(ProgressHandle handle) {
        handle.switchToIndeterminate();
        return load();
    }

    /**
     * Lance directement le chargement via {@link ScenarioLoader}
     *
     * @return le resultat du chargement du scenario.
     * @see ScenarioLoader
     */
    public ScenarioLoaderOperation load() {
        ScenarioLoader loader = new ScenarioLoader(scenario, projet, projet.getCoeurConfig());
        return loader.load(run);
    }
}
