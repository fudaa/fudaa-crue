/*
 * To change this template, choose Tools | Templates and open the template in the editor.
 */
package org.fudaa.fudaa.crue.loader;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.projet.conf.OptionsEnum;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import java.awt.*;
import java.io.File;
import java.util.List;

/**
 * @author Fred Deniger
 */
public class OpenFileProcess implements ProgressRunnable<EMHProjet> {
  private final File etuFile;
  private final CoeurConfigContrat coeurConfig;
  private CtuluLogGroup logs;
  final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

  public OpenFileProcess(File etuFile, CoeurConfigContrat coeurConfig) {
    this.etuFile = etuFile;
    this.coeurConfig = coeurConfig;
  }

  public CtuluLogGroup getLogs() {
    return logs;
  }

  @Override
  public EMHProjet run(ProgressHandle handle) {
    if (handle != null) {
      handle.switchToIndeterminate();
    }

    if (coeurConfig == null) {
      return null;
    }
    final CrueOperationResult<EMHProjet> readProjet = EMHProjetController.readProjet(etuFile, coeurConfig, true);
    logs = readProjet.getLogs();
    String currentLanguage = configurationManagerService.getOptionsManager().getOption(OptionsEnum.USER_LANGUAGE).getValeur();
    List<String> avaibleLanguagesForMessage = coeurConfig.getAvailableLanguagesForMessage();
    if (!avaibleLanguagesForMessage.contains(currentLanguage)) {
      String availableLanguages = StringUtils.join(avaibleLanguagesForMessage, "; ");
      if (StringUtils.isBlank(availableLanguages)) {
        availableLanguages = NbBundle.getMessage(OpenFileProcess.class, "noLanguageFountInMessage.message");
      }
      logs.createNewLog("validationLocale.log").addWarn("coeur.currentLocaleNotSupported", currentLanguage, coeurConfig.getXsdVersion(), availableLanguages);
    }

    if (logs.containsSomething()) {
      EventQueue.invokeLater(() -> LogsDisplayer.displayError(readProjet.getLogs(), NbBundle.getMessage(OpenFileProcess.class, "OpenEtu.ErrorDialogTitle",
          etuFile.getName())));
    }
    return readProjet.getResult();
  }
}
