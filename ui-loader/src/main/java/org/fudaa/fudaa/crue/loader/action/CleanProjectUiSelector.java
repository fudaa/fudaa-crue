package org.fudaa.fudaa.crue.loader.action;

import com.memoire.bu.BuVerticalLayout;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.projet.ProjectNormalizableCallable;
import org.fudaa.dodico.crue.projet.clean.EMHProjectCleaner;
import org.fudaa.dodico.crue.projet.clean.ScenarioCleanData;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.view.MultiEntriesChooser;
import org.openide.NotifyDescriptor;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * UI pour choisir les scenarios à normaliser/supprimer
 *
 * @author deniger
 */
public class CleanProjectUiSelector {
  private MultiEntriesChooser<String> toDeleteChooser;
  private MultiEntriesChooser<String> toNormalizeChooser;
  private JComponent topComponent;

  public void setTopTitle(JComponent topComponent) {
    this.topComponent = topComponent;
  }

  private static boolean isEmpty(MultiEntriesChooser<String> chooser) {
    return chooser == null || CollectionUtils.isEmpty(chooser.getSelectedEntries());
  }

  private void createDeleteZone(ScenarioCleanData data) {
    if (CollectionUtils.isNotEmpty(data.scenarioNamesToDelete)) {
      final ArrayList<String> scenarioNames = new ArrayList<>(data.scenarioNamesToDelete);
      Collections.sort(scenarioNames);
      this.toDeleteChooser = new MultiEntriesChooser<>(scenarioNames);
      this.toDeleteChooser.setPreSelectedEntries(scenarioNames);
    }
  }

  private void createNormalizeZone(ScenarioCleanData data) {
    if (CollectionUtils.isNotEmpty(data.scenarioToNormalizeOrWithErrors)) {
      final List<String> scenarioNames = EMHProjectCleaner.getScenarioToNormalize(data.scenarioToNormalizeOrWithErrors);
      Collections.sort(scenarioNames);
      this.toNormalizeChooser = new MultiEntriesChooser<>(scenarioNames);
      this.toNormalizeChooser.setPreSelectedEntries(scenarioNames);
    }
  }

  /**
   * @param data donnees initiales
   * @param dialogTitle le titre du dialog
   * @param okAction le label pour l'action OK
   * @return null si l'utilisateur annule l'action
   */
  public ScenarioCleanData select(ScenarioCleanData data, String dialogTitle, String okAction) {
    //Si des scenarios ont des erreurs, c'est affichés ici:
    final CtuluLogGroup scenarioWithError = EMHProjectCleaner.getLogs(data.scenarioToNormalizeOrWithErrors);
    if (scenarioWithError.containsSomething()) {
      final String title = NbBundle.getMessage(CleanProjectUiSelector.class, "NormalizeScenariosAction.ScenarioWithErrors");
      LogsDisplayer.displayError(scenarioWithError, title);
    }

    //les liste de sélections
    createDeleteZone(data);
    createNormalizeZone(data);

    //construire le panel ici...
    final JPanel jPanel = buildPanel();
    //si des scenarios sont en erreurs, ils ne seront pas proposés donc les choix peuvent être vides:
    if (isEmpty(toDeleteChooser) && isEmpty(toNormalizeChooser)) {
      return null;
    }
    Object[] options = new Object[]{NotifyDescriptor.OK_OPTION, NotifyDescriptor.CANCEL_OPTION};
    if (okAction != null) {
      options[0] = okAction;
    }

    Object userSelection = DialogHelper.showQuestion(dialogTitle, jPanel, options);

    //le user a accepte
    if (options[0].equals(userSelection)) {
      return createDataFromUserSelection(data);
    }
    //annulation par l'utilisateur
    return null;
  }

  /**
   *
   * @param data les données initiales
   * @return les données prenant en compte les choix de l'utilisateur
   */
  private ScenarioCleanData createDataFromUserSelection(ScenarioCleanData data) {
    ScenarioCleanData newData = new ScenarioCleanData();
    if (toDeleteChooser != null) {
      newData.scenarioNamesToDelete.addAll(toDeleteChooser.getSelectedEntries());
    }
    if (toNormalizeChooser != null) {
      final Map<String, ProjectNormalizableCallable.Result> byScenarioName = data.getResultByScenarioName();
      for (String selectedEntry : toNormalizeChooser.getSelectedEntries()) {
        newData.scenarioToNormalizeOrWithErrors.add(byScenarioName.get(selectedEntry));
      }
    }
    return newData;
  }

  private JPanel buildPanel() {
    JPanel pn = new JPanel(new BuVerticalLayout(5));
    if (topComponent != null) {
      pn.add(topComponent);
    }
    if (toDeleteChooser != null) {
      pn.add(new JLabel(NbBundle.getMessage(CleanProjectUiSelector.class, "DeleteDuplicatedScenario.Choose")));
      pn.add(toDeleteChooser.getPanel());
    }
    if (toNormalizeChooser != null) {
      if (toDeleteChooser != null) {
        pn.add(new JSeparator());
      }
      pn.add(new JLabel(NbBundle.getMessage(CleanProjectUiSelector.class, "NormalizeScenariosAction.Choose")));
      pn.add(toNormalizeChooser.getPanel());
    }
    return pn;
  }
}
