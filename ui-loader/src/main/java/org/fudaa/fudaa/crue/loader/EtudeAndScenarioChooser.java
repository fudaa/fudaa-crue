package org.fudaa.fudaa.crue.loader;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuGridLayout;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.openide.DialogDescriptor;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.util.List;
import java.util.function.Predicate;

public class EtudeAndScenarioChooser {
  private Predicate<String> scenarioNameFilter;

  /**
   * Dans certains cas, les scenarios proposés doivent etre filtrés.
   *
   * @param scenarioNameFilter le predicate: si non null, doit renvoyer true pour les noms de scenarios acceptables.
   */
  public void setScenarioNameFilter(final Predicate<String> scenarioNameFilter) {
    this.scenarioNameFilter = scenarioNameFilter;
  }

  public static void main(final String[] arg) {
    new EtudeAndScenarioChooser().choose();
  }

  public Pair<String, String> choose() {
    return choose(-1, null);
  }

  public Pair<String, String> choose(final int tailleMaxForScenarioNom, final String errorMessageForTailleMax) {
    final EtudeScenarioDialog etudeScenarioDialog = new EtudeScenarioDialog(tailleMaxForScenarioNom, errorMessageForTailleMax, scenarioNameFilter);
    etudeScenarioDialog.setPreferredSize(new Dimension(450, 250));
    final DialogDescriptor dialogDescriptor = new DialogDescriptor(etudeScenarioDialog,
        NbBundle.getMessage(EtudeAndScenarioChooser.class, "ChooseEtuAndScenario"));
    etudeScenarioDialog.setDialogDescriptor(dialogDescriptor);
    final boolean accepted = DialogHelper.showQuestion(dialogDescriptor, getClass(), null, PerspectiveEnum.STUDY,
        DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    if (accepted) {
      return etudeScenarioDialog.getValues();
    }
    return null;
  }

  private static class EtudeScenarioDialog extends CtuluDialogPanel {
    private final CtuluFileChooserPanel chooseEtu;
    final LoaderService loaderService = Lookup.getDefault().lookup(LoaderService.class);
    transient DialogDescriptor dialogDescriptor;
    final JComboBox cbScenario = new JComboBox();
    JLabel lbScenarioError = new JLabel();
    final DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();
    private final int tailleMaxsForScenarioNom;
    private final String errorMessageForTailleMax;
    /**
     * si non null, sera utilisé pour filtrer les scenarios.
     */
    private final transient Predicate<String> scenarioFilter;

    /**
     * @param tailleMaxForScenarioNom la taille max attendu pour le nom du scenario
     * @param errorMessageForTailleMax le message a utiliser
     * @param scenarioFilter si non null, sera utilisé pour filtrer les scenarios.
     */
    protected EtudeScenarioDialog(final int tailleMaxForScenarioNom, final String errorMessageForTailleMax, final Predicate<String> scenarioFilter) {
      tailleMaxsForScenarioNom = tailleMaxForScenarioNom;
      this.errorMessageForTailleMax = errorMessageForTailleMax;
      this.scenarioFilter = scenarioFilter;
      setLayout(new BuGridLayout(2, 10, 10));
      setBorder(BuBorders.EMPTY5555);

      chooseEtu = addFileChooserPanel(this, NbBundle.getMessage(EtudeAndScenarioChooser.class, "ChooseEtuLabel"), false, false);
      chooseEtu.setAllFileFilter(false);
      chooseEtu.setFilter(new FileFilter[]{LoaderService.createEtuFileFilter()});
      chooseEtu.getTf().setEditable(false);
      chooseEtu.getTf().getDocument().addDocumentListener(new DocumentListener() {
        @Override
        public void insertUpdate(final DocumentEvent e) {
          loadEtude();
        }

        @Override
        public void removeUpdate(final DocumentEvent e) {
          loadEtude();
        }

        @Override
        public void changedUpdate(final DocumentEvent e) {
          loadEtude();
        }
      });

      final JLabel chooseScenarioLabel = new JLabel(NbBundle.getMessage(EtudeAndScenarioChooser.class, "ChooseScenarioLabel"));

      add(chooseScenarioLabel);
      add(cbScenario);
      if (errorMessageForTailleMax != null) {
        chooseScenarioLabel.setToolTipText(errorMessageForTailleMax);
        cbScenario.setToolTipText(errorMessageForTailleMax);
        lbScenarioError = new JLabel();
        lbScenarioError.setForeground(Color.RED);
        add(new JLabel());
        add(lbScenarioError);
      }
      cbScenario.addItemListener(e -> validateData());
      cbScenario.setModel(comboBoxModel);
    }

    public Pair<String, String> getValues() {
      return new Pair<>(chooseEtu.getFile().getAbsolutePath(), (String) cbScenario.getSelectedItem());
    }

    public void setDialogDescriptor(final DialogDescriptor dialogDescriptor) {
      this.dialogDescriptor = dialogDescriptor;
    }

    private void validateData() {
      lbScenarioError.setText("");
      boolean valid = StringUtils.isNotBlank(chooseEtu.getTf().getText()) && StringUtils.isNotBlank((String) cbScenario.getSelectedItem());
      if (valid && tailleMaxsForScenarioNom > 0) {
        final String scenarioName = (String) cbScenario.getSelectedItem();
        if (scenarioName.length() > tailleMaxsForScenarioNom) {
          final String errorText = errorMessageForTailleMax != null ? errorMessageForTailleMax
              : NbBundle.getMessage(Bundle.class, "MaxSizeForScenarioName", tailleMaxsForScenarioNom);
          lbScenarioError.setText(errorText);
          valid = false;
        }
      }
      dialogDescriptor.setValid(valid);
    }

    protected void loadEtude() {
      final ProjectLoadContainer loadProject = loaderService.loadProject(chooseEtu.getFile());
      comboBoxModel.removeAllElements();
      comboBoxModel.addElement("");
      if (loadProject != null && loadProject.getProjet() != null) {
        final List<ManagerEMHScenario> listeScenarios = loadProject.getProjet().getListeScenarios();
        for (final ManagerEMHScenario emhScenario : listeScenarios) {
          if (emhScenario.isCrue10() && emhScenario.isActive()) {
            boolean accepted = true;
            if (scenarioFilter != null) {
              accepted = scenarioFilter.test(emhScenario.getNom());
            }
            if (accepted) {
              comboBoxModel.addElement(emhScenario.getNom());
            }
          }
        }
      }
      cbScenario.setSelectedIndex(0);
      validateData();
    }
  }
}
