/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.example;

import org.fudaa.ebli.animation.EbliAnimationOutputGIF;
import org.fudaa.fudaa.crue.report.longitudinal.ReportProfilLongitudinalTopComponent;
import org.fudaa.fudaa.crue.report.time.ReportTimeVideoRecorderEditor;
import org.fudaa.fudaa.crue.report.time.ReportTimeVideoRecorderItem;
import org.fudaa.fudaa.crue.report.transversal.ReportProfilTransversalTopComponent;

import java.util.Arrays;

/**
 *
 * @author Frederic Deniger
 */
public class ReportTimeVideoRecorderEditorExample {

  public ReportTimeVideoRecorderEditorExample() {
  }

  public static void main(String[] args) {
    ReportTimeVideoRecorderEditor editor = new ReportTimeVideoRecorderEditor();
    ReportTimeVideoRecorderItem item = new ReportTimeVideoRecorderItem();
    item.setActivated(false);
    item.setNbFrames(100);
    final ReportProfilLongitudinalTopComponent reportProfilLongitudinalTopComponent = new ReportProfilLongitudinalTopComponent();
    reportProfilLongitudinalTopComponent.setName("longitudinal");
    item.setTopComponent(reportProfilLongitudinalTopComponent);
    ReportTimeVideoRecorderItem item1 = new ReportTimeVideoRecorderItem();
    item1.setActivated(true);
    final ReportProfilTransversalTopComponent reportProfilTransversalTopComponent = new ReportProfilTransversalTopComponent();
    reportProfilLongitudinalTopComponent.setName("transversal");
    item1.setTopComponent(reportProfilTransversalTopComponent);
    item1.setOutput(new EbliAnimationOutputGIF());
    item1.setNbFrames(10);
    editor.edit(Arrays.asList(item, item1));
  }
}
