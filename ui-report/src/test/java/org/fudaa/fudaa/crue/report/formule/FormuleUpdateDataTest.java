/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.formule;

import org.fudaa.dodico.crue.projet.report.formule.FormuleDataEdited;
import org.fudaa.dodico.crue.projet.report.formule.FormuleParametersExpr;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleUpdateDataTest {

  public FormuleUpdateDataTest() {
  }

  @Test
  public void testAll() {
    FormuleDataEdited data = new FormuleDataEdited();
    FormuleParametersExpr expr = new FormuleParametersExpr();
    expr.setId("test 1");
    data.addParameter(null, expr);

    FormuleParametersExpr renamed = new FormuleParametersExpr();
    renamed.setId("test 2");
    data.addParameter("renamed", renamed);
    assertEquals(expr.getId(), data.getNewParameters().get(0).getId());
    assertTrue(data.isProbablyModified("renamed"));
    assertEquals(renamed.getId(), data.getNewId("renamed"));

  }
}
