/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.dodico.crue.projet.report.persist.ReportTemporalConfig;
import org.fudaa.dodico.crue.projet.report.transformer.KeysToStringConverter;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.fudaa.dodico.crue.test.FileTest;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

/**
 *
 * @author Frederic Deniger
 */
public class ReportConfigSaverTemporalTest extends FileTest {

  final ReportRunKey current = new ReportRunKey();
  private final String fileName = "/org/fudaa/fudaa/crue/report/view/persist/temporel.xml";

  public ReportConfigSaverTemporalTest() {
  }

  @Test
  public void testReadTemporel() {
    final File file = AbstractTestParent.getFile(fileName);
    final ReportConfigContrat metier = read(file);
    assertNotNull(metier);
    assertEquals(ReportTemporalConfig.class, metier.getClass());
    file.delete();
    final ReportTemporalConfig config = (ReportTemporalConfig) metier;
    testContent(config);
  }

  @Test
  public void testWriteTemporel() throws IOException {
    final File file = AbstractTestParent.getFile(fileName);
    ReportConfigContrat metier = read(file);
    file.delete();
    final File out = createTempFile("temporel", ".xml");
    createSaver().save(out, metier, "test");
    metier = read(out);
    out.delete();
    testContent((ReportTemporalConfig) metier);
  }

  private ReportConfigContrat read(final File file) {
    assertTrue(file.exists());
    final ReportConfigSaver saver = createSaver();
    final CrueIOResu<ReportConfigContrat> read = createReader().read(file);
    return read.getMetier();
  }

  @Test
  public void testCreateReadWriteEmptyTemporal() throws IOException {
    final ReportTemporalConfig config = new ReportTemporalConfig();
    assertNotNull(config.getExternFiles());
    final File target = createTempFile("temporal", ".xml");
    final ReportConfigSaver saver = createSaver();
    saver.save(target, config, "test");
    final CrueIOResu<ReportConfigContrat> read = createReader().read(target);
    final ReportConfigContrat metier = read.getMetier();
    assertNotNull(metier);
    assertEquals(ReportTemporalConfig.class, metier.getClass());
    target.delete();
    final ReportTemporalConfig readConfig = (ReportTemporalConfig) metier;
    assertNotNull(readConfig.getExternFiles());
    assertNotNull(readConfig.getLoiLegendConfig());
    assertNotNull(readConfig.getLoiLegendConfig().getLabels());
    assertNotNull(readConfig.getEmhs());
    assertNotNull(readConfig.getTemporalVariables());
    assertNotNull(readConfig.getTimeConfig());
  }

  private ReportConfigSaver createSaver() {
    final KeysToStringConverter keysToString = new KeysToStringConverter(current);
    return new ReportConfigSaver(new TraceToStringConverter(), keysToString);
  }
  private ReportConfigReader createReader() {
    final KeysToStringConverter keysToString = new KeysToStringConverter(current);
    return new ReportConfigReader(new TraceToStringConverter(), keysToString);
  }

  private void testContent(final ReportTemporalConfig reportTemporalConfig) {
    assertNotNull(reportTemporalConfig.getAxeVConfig("splanAct"));
    assertNotNull(reportTemporalConfig.getAxeVConfig("splanTot"));
    assertEquals(1, reportTemporalConfig.getEmhs().size());
    assertEquals("Br_B1", reportTemporalConfig.getEmhs().get(0));
    final List<ReportRunVariableKey> temporalVariables = reportTemporalConfig.getTemporalVariables();
    assertEquals(6, temporalVariables.size());
    ReportRunVariableKey key = temporalVariables.get(5);
    assertEquals("splanTot", key.getVariable().getVariableName());
    final ReportRunKey reportRunKey = key.getReportRunKey();
    assertEquals(new ReportRunKey("Sc_M201-1_c9c9c10", "R2012-09-13-13h17m01s"), reportRunKey);

    key = temporalVariables.get(0);
    assertTrue(current == key.getReportRunKey());
  }
}
