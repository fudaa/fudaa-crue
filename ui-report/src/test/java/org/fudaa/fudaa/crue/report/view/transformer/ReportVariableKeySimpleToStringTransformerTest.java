/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.transformer;

import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.transformer.KeysToStringConverter;
import org.fudaa.dodico.crue.projet.report.transformer.ReportVariableKeySimpleToStringTransformer;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class ReportVariableKeySimpleToStringTransformerTest {
  
  public ReportVariableKeySimpleToStringTransformerTest() {
  }
  
  @Test
  public void testReadWrite() {
    ReportVariableKeySimpleToStringTransformer transformer = new ReportVariableKeySimpleToStringTransformer();
    ReportVariableKey key = new ReportVariableKey(ReportVariableTypeEnum.READ, "Test");
    String toString = transformer.toString(key);
    assertEquals(key.getVariableName()+KeysToStringConverter.MINOR_SEPARATOR + key.getVariableType().getPersistId(), toString);
    ReportVariableKey readKey = transformer.fromString(toString);
    assertEquals(key, readKey);
  }
}
