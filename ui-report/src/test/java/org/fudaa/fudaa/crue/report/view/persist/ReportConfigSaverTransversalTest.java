/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.dodico.crue.projet.report.persist.ReportTransversalConfig;
import org.fudaa.dodico.crue.projet.report.transformer.KeysToStringConverter;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.fudaa.dodico.crue.test.FileTest;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Frederic Deniger
 */
public class ReportConfigSaverTransversalTest extends FileTest {
  final ReportRunKey current = new ReportRunKey();

  public ReportConfigSaverTransversalTest() {
  }

  @Test
  public void testReadTransversal() {
    final File file = AbstractTestParent.getFile("/org/fudaa/fudaa/crue/report/view/persist/transversal.xml");
    final ReportConfigContrat metier = read(file);
    assertNotNull(metier);
    assertEquals(ReportTransversalConfig.class, metier.getClass());
    file.delete();
    final ReportTransversalConfig config = (ReportTransversalConfig) metier;
    testContent(config);
  }

  @Test
  public void testWriteTransversal() throws IOException {
    final File file = AbstractTestParent.getFile("/org/fudaa/fudaa/crue/report/view/persist/transversal.xml");
    ReportConfigContrat metier = read(file);
    file.delete();
    final File out = createTempFile("transversal", ".xml");
    createSaver().save(out, metier, "test");
    metier = read(out);
    out.delete();
    testContent((ReportTransversalConfig) metier);
  }

  private ReportConfigContrat read(final File file) {
    assertTrue(file.exists());
    final CrueIOResu<ReportConfigContrat> read = createReader().read(file);
    return read.getMetier();
  }

  private void testContent(final ReportTransversalConfig config) {
    assertEquals("SectionName", config.getSectionName());
    final List<String> externFiles = config.getExternFiles();
    assertEquals(2, externFiles.size());
    assertEquals("C:\\data\\tmp\\example.csv", externFiles.get(0));
    final List<ReportRunVariableKey> profilVariables = config.getProfilVariables();
    assertEquals(1, profilVariables.size());
    assertTrue(current == profilVariables.get(0).getReportRunKey());
    assertEquals("variable", profilVariables.get(0).getVariable().getVariableName());
    final List<ReportRunKey> profilXtZ = config.getProfilXtZ();
    assertEquals(1, profilXtZ.size());
    assertEquals("manager", profilXtZ.get(0).getManagerNom());
    assertEquals("run", profilXtZ.get(0).getRunAlternatifId());
  }

  @Test
  public void testCreateReadWriteEmptyTransversal() throws IOException {
    final ReportTransversalConfig config = new ReportTransversalConfig();
    assertNotNull(config.getExternFiles());
    final File target = createTempFile("transversal", ".xml");
    final ReportConfigSaver saver = createSaver();
    saver.save(target, config, "test");
    final CrueIOResu<ReportConfigContrat> read = createReader().read(target);
    final ReportConfigContrat metier = read.getMetier();
    assertNotNull(metier);
    assertEquals(ReportTransversalConfig.class, metier.getClass());
    target.delete();
    final ReportTransversalConfig readConfig = (ReportTransversalConfig) metier;
    assertNotNull(readConfig.getExternFiles());
    assertNotNull(readConfig.getLoiLegendConfig());
    assertNotNull(readConfig.getLoiLegendConfig().getLabels());
    assertNotNull(readConfig.getProfilVariables());
    assertNotNull(readConfig.getProfilXtZ());
  }

  private ReportConfigSaver createSaver() {
    final KeysToStringConverter keysToString = new KeysToStringConverter(current);
    return new ReportConfigSaver(new TraceToStringConverter(), keysToString);
  }

  private ReportConfigReader createReader() {
    final KeysToStringConverter keysToString = new KeysToStringConverter(current);
    return new ReportConfigReader(new TraceToStringConverter(), keysToString);
  }
}
