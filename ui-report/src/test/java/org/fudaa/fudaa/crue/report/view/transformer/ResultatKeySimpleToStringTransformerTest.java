/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.transformer;

import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.transformer.KeysToStringConverter;
import org.fudaa.dodico.crue.projet.report.transformer.ResultatTimeKeySimpleToStringTransformer;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class ResultatKeySimpleToStringTransformerTest {

  public ResultatKeySimpleToStringTransformerTest() {
  }

  @Test
  public void testForPermanent() {
    ResultatTimeKeySimpleToStringTransformer transformer = new ResultatTimeKeySimpleToStringTransformer();
    ResultatTimeKey key = new ResultatTimeKey("perm");
    final String toString = transformer.toString(key);
    assertEquals(key.getNomCalcul(), toString);
    ResultatTimeKey readKey = transformer.fromString(toString);
    assertEquals(readKey, key);
    assertTrue(readKey.isPermanent());
  }

  @Test
  public void testForTransitoireOldFormat() {
    String old = "calcul;1234";
    ResultatTimeKeySimpleToStringTransformer transformer = new ResultatTimeKeySimpleToStringTransformer();
    final ResultatTimeKey fromString = transformer.fromString(old);
    assertFalse(fromString.isUseDay());
    assertEquals("calcul", fromString.getNomCalcul());
    assertEquals(1234L, fromString.getDuree());

  }

  @Test
  public void testForTransitoire() {
    ResultatTimeKeySimpleToStringTransformer transformer = new ResultatTimeKeySimpleToStringTransformer();
    final long duree = 12345L;
    ResultatTimeKey key = new ResultatTimeKey("transitoire", duree, true);
    final String toString = transformer.toString(key);
    assertEquals(
            key.getNomCalcul() + KeysToStringConverter.MINOR_SEPARATOR + duree + KeysToStringConverter.MINOR_SEPARATOR + ResultatTimeKeySimpleToStringTransformer.DAY_YES,
            toString);
    ResultatTimeKey readKey = transformer.fromString(toString);
    assertEquals(readKey, key);
    assertTrue(key.isUseDay());
    assertEquals(duree, readKey.getDuree());
    assertTrue(readKey.isTransitoire());
  }
}
