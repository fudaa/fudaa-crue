/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.projet.report.formule.FormuleParametersExpr;
import org.fudaa.dodico.crue.test.FileTest;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class ReportFormuleReaderSaverTest extends FileTest {

  public ReportFormuleReaderSaverTest() {
  }

  @Test
  public void testReadWrite() throws IOException {
    List<FormuleParametersExpr> keys = new ArrayList<>();
    final FormuleParametersExpr formuleParametersExprOne = new FormuleParametersExpr();
    formuleParametersExprOne.setDisplayName("One");
    formuleParametersExprOne.setFormule("1+MoySurT(\"H\")");//attention on teste aussi la correction automatique
    formuleParametersExprOne.setNature("OneType");
    final FormuleParametersExpr formuleParametersExprTwo = new FormuleParametersExpr();
    formuleParametersExprTwo.setDisplayName("Two");
    formuleParametersExprTwo.setFormule("2+2");
    formuleParametersExprTwo.setNature("TwoType");
    keys.add(formuleParametersExprOne);
    keys.add(formuleParametersExprTwo);
    ReportFormuleReaderSaver saver = new ReportFormuleReaderSaver();

    File f = createTempFile("formules", ".xml");
    CtuluLog save = saver.save(f, keys);
    assertTrue(save.isEmpty());
    CrueIOResu<List<FormuleParametersExpr>> read = saver.read(f);
    f.delete();
    assertTrue(read.getAnalyse().isEmpty());
    List<FormuleParametersExpr> metier = read.getMetier();
    assertEquals(2, metier.size());
    //du à la correction automatique
    formuleParametersExprOne.setFormule("1+MoySurTousC_T(\"H\")");
    assertTrue(formuleParametersExprOne.equals(metier.get(0)));
    assertTrue(formuleParametersExprTwo.equals(metier.get(1)));
  }
}
