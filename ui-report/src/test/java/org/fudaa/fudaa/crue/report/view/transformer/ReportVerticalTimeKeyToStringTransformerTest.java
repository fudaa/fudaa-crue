/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.transformer;

import org.fudaa.dodico.crue.projet.report.data.ReportVerticalTimeKey;
import org.fudaa.dodico.crue.projet.report.transformer.ReportVerticalTimeKeyToStringTransformer;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class ReportVerticalTimeKeyToStringTransformerTest {
  
  public ReportVerticalTimeKeyToStringTransformerTest() {
  }
  
  @Test
  public void testReadWrite() {
    ReportVerticalTimeKeyToStringTransformer transformer = new ReportVerticalTimeKeyToStringTransformer();
    assertEquals(ReportVerticalTimeKeyToStringTransformer.ID, transformer.toString(ReportVerticalTimeKey.INSTANCE));
    assertTrue(ReportVerticalTimeKey.INSTANCE == transformer.fromString(ReportVerticalTimeKeyToStringTransformer.ID));
  }
}
