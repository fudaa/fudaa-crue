/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.fudaa.dodico.crue.test.FileTest;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Frederic Deniger
 */
public class RunAlternatifSaverTest extends FileTest {
  private final String pathname = "/org/fudaa/fudaa/crue/report/view/persist/runs.xml";

  public RunAlternatifSaverTest() {
  }

  @Test
  public void testRead() {
    final File file = AbstractTestParent.getFile(pathname);
    final RunAlternatifSaver saver = new RunAlternatifSaver();
    final CrueIOResu<List<ReportRunKey>> read = saver.read(file);
    assertTrue(read.getAnalyse().isEmpty());
    testKeys(read.getMetier());
  }

  @Test
  public void testWrite() throws IOException {
    final File file = AbstractTestParent.getFile(pathname);
    final RunAlternatifSaver saver = new RunAlternatifSaver();
    CrueIOResu<List<ReportRunKey>> read = saver.read(file);
    final File out = createTempFile();
    final CtuluLog save = saver.save(out, read.getMetier());
    assertTrue(save.isEmpty());
    read = saver.read(out);
    assertTrue(read.getAnalyse().isEmpty());
    testKeys(read.getMetier());
  }

  @Test
  public void testReadWrite() throws IOException {
    final List<ReportRunKey> keys = new ArrayList<>();
    final ReportRunKey reportRunKeyOne = new ReportRunKey("manager_1", "run##1");
    keys.add(reportRunKeyOne);
    final ReportRunKey reportRunKeyTwo = new ReportRunKey("manager_2", "run##2.1234");
    keys.add(reportRunKeyTwo);
    final RunAlternatifSaver saver = new RunAlternatifSaver();

    final File f = createTempFile();
    final CtuluLog save = saver.save(f, keys);
    assertTrue(save.isEmpty());
    final CrueIOResu<List<ReportRunKey>> read = saver.read(f);
    f.delete();
    assertTrue(read.getAnalyse().isEmpty());
    final List<ReportRunKey> metier = read.getMetier();
    assertEquals(2, metier.size());
    assertEquals(reportRunKeyOne, metier.get(0));
    assertEquals(reportRunKeyTwo, metier.get(1));
  }

  private void testKeys(final List<ReportRunKey> metier) {
    assertEquals(2, metier.size());
    assertEquals(new ReportRunKey("manager1", "run1"), metier.get(0));
    assertEquals(new ReportRunKey("manager2", "run2"), metier.get(1));
  }

  private File createTempFile() throws IOException {
    return createTempFile("runs", ".xml");
  }
}
