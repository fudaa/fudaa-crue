/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.IdRegistry;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.metier.etude.EMHProjectInfos;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.fudaa.fudaa.crue.common.helper.LoadEtuHelper;
import org.fudaa.fudaa.crue.post.services.PostServiceImpl;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.netbeans.core.startup.CoreBridge;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Frederic Deniger
 */
public class ReportTestHelper {
    public static void display(final JComponent jc, final File toDelete) {
        final JFrame fr = new JFrame();
        fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fr.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                if (toDelete != null) {
                    CtuluLibFile.deleteDir(toDelete);
                }
            }
        });

        fr.setContentPane(jc);
        fr.setSize(500, 600);
        fr.setVisible(true);
    }

    public static Pair<EMHScenarioContainer, File> readScenarioAndRun() {
        File target = null;
        try {
            target = CtuluLibFile.createTempDir();
        } catch (final IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        AbstractTestParent.exportZip(target, "/Etu3-0.zip");
        final File file = new File(new File(target, "Etu3-0"), "Etu3-0.etu.xml");
        final EMHProjet projet = LoadEtuHelper.loadEtu(file, TestCoeurConfig.INSTANCE_1_1_1);
        final EMHScenarioContainer runv8 = LoadEtuHelper.loadRun("Sc_M3-0_v8c9", projet, 0);
        return new Pair<>(runv8, target);
    }

    public static Pair<EMHScenarioContainer, File> readScenario() {
        CoreBridge.getDefault().registerPropertyEditors();
        Pair<EMHScenarioContainer, File> readScenarioAndRun = readScenarioAndRun();
        PostServiceImpl postService = Lookup.getDefault().lookup(PostServiceImpl.class);
        EMHProjet projet = new EMHProjet();
        final EMHProjectInfos infos = new EMHProjectInfos();
        Map<String, String> dir = new HashMap<>();
        dir.put(EMHProjectInfos.CONFIG, "nowherer");
        infos.setDirectories(dir);
        projet.setInfos(infos);

        projet.setPropDefinition(TestCoeurConfig.INSTANCE);

        //appele pour avoir le chargement:
        ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
        ReportGlobalServiceContrat contrat = Lookup.getDefault().lookup(ReportGlobalServiceContrat.class);

        final EMHScenarioContainer first = readScenarioAndRun.first;
        EMHScenario emhScenario = first.getEmhScenario();
        IdRegistry.install(emhScenario);
        postService.setForTest(first.getManagerEMHScenario(), first.getEmhScenario(), first.getManagerEMHScenario().getListeRuns().get(0), projet);

        return readScenarioAndRun;
    }

    public static void displayTime(JComponent jc, final File toDelete) {
        displayTime(jc, toDelete, null);
    }

    public static void displayTime(JComponent jc, final File toDelete, JComponent componentToAddInToolbar) {
        final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
        List<ResultatTimeKey> times = reportService.getTimeContent().getTimes();
        final JComboBox cb = new JComboBox(times.toArray());
        cb.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                reportService.setSelectedTime((ResultatTimeKey) cb.getSelectedItem());
            }
        });
        JPanel panel = new JPanel(new BorderLayout(5, 5));
        JPanel top = new JPanel(new FlowLayout(FlowLayout.LEFT));
        top.add(new JLabel("Temps: "));
        top.add(cb);
        cb.setSelectedIndex(0);
        if (componentToAddInToolbar != null) {
            top.add(componentToAddInToolbar);
        }
        panel.add(top, BorderLayout.NORTH);
        panel.add(jc);
        display(panel, toDelete);
    }
}
