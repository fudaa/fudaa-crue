/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report;

import java.awt.EventQueue;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class ReportVisualTopComponentTest {

  public ReportVisualTopComponentTest() {
  }

  @Test
  public void testClose() {
    try {
      System.setProperty("java.awt.headless", "true");//sur le moteur d'intégration continue c'est le cas.
      Runnable runnable = new Runnable() {
        @Override
        public void run() {
          ReportVisualTopComponent top = new ReportVisualTopComponent();
          top.componentOpened();
          top.runUnloaded();
        }
      };
      EventQueue.invokeAndWait(runnable);
    } catch (Exception ex) {
      Assert.fail(ex.getMessage());
    }
  }
}
