/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.formule;

import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.dodico.crue.projet.report.FormuleContentManager;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;
import org.fudaa.dodico.crue.projet.report.formule.FormuleContentManagerDefaultBuilder;
import org.fudaa.dodico.crue.projet.report.formule.FormuleParametersExpr;
import org.fudaa.fudaa.crue.report.AbstractTestWithScenarioTest;
import org.junit.Test;
import org.openide.util.Lookup;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Besoin d'un scenario charge
 *
 * @author Frederic Deniger
 */
public class FormuleContentManagerDefaultBuilderTest extends AbstractTestWithScenarioTest {

  public FormuleContentManagerDefaultBuilderTest() {
  }

  @Test
  public void testCreate() {
    FormuleParametersExpr expr1 = new FormuleParametersExpr();
    expr1.setDisplayName("expr1");
    expr1.setFormule("Q+Z");

    FormuleParametersExpr expr2 = new FormuleParametersExpr();
    expr2.setDisplayName("expr2");
    expr2.setFormule("1+MaxSurTousC_T(\"Vol\")+Q+MoySurTousC_T(\"Splan\")");
    ReportGlobalServiceContrat reportGlobalServiceContrat = Lookup.getDefault().lookup(ReportGlobalServiceContrat.class);
    FormuleContentManagerDefaultBuilder builder = new FormuleContentManagerDefaultBuilder(Arrays.asList(expr1, expr2), reportGlobalServiceContrat);
    CtuluLogResult<FormuleContentManager> create = builder.create();
    assertTrue(create.getLog().isEmpty());
    FormuleContentManager resultat = create.getResultat();
    assertContained("expr1 used", resultat.getDirectUsedVariable("expr1"), "Q", "Z");
    assertContained("expr2 used", resultat.getDirectUsedVariable("expr2"), "Q", "Vol", "Splan");
  }

  public static void assertContained(String msg, Collection<String> var, String... includes) {
    assertEquals(msg, includes.length, var.size());
    for (String string : includes) {
      assertTrue(msg, var.contains(string));
    }
  }
}
