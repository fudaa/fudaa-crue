/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.example;

import java.io.File;
import java.util.ArrayList;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.fudaa.crue.report.ReportTestHelper;
import org.fudaa.fudaa.crue.report.export.ReportExportHelper;

/**
 *
 * @author Frederic Deniger
 */
public class ReportExportHelperExample {

  public static void main(String[] args) {
    Pair<EMHScenarioContainer, File> load = ReportTestHelper.readScenario();
    ReportExportHelper topComponent = new ReportExportHelper();
    CtuluLibFile.deleteDir(load.second);
    topComponent.displayDialogExport(new ArrayList<>(load.first.getEmhScenario().getBranches()));
  }
}
