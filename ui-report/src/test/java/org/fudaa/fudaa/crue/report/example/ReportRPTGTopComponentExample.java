/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.example;

import java.io.File;
import javax.swing.SwingConstants;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.projet.report.ReportRPTGConfig;
import org.fudaa.dodico.crue.projet.report.data.ReportExpressionHelper;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.fudaa.crue.report.ReportTestHelper;
import org.fudaa.fudaa.crue.report.actions.ReportOpenProfilTransversalViewNodeAction;
import org.fudaa.fudaa.crue.report.rptg.ReportRPTGTopComponent;

/**
 *
 * @author Frederic Deniger
 */
public class ReportRPTGTopComponentExample {

  public static void main(String[] args) {
    Pair<EMHScenarioContainer, File> load = ReportTestHelper.readScenario();
    ReportRPTGTopComponent topComponent = new ReportRPTGTopComponent();
    ReportRPTGConfig content = new ReportRPTGConfig();
    content.setEmh("St_Prof11");
    content.setVarHorizontal("q");
    content.getVariables().add("z");
    content.getLoiLegendConfig().setTitleConfig(createLabelNomEMH());
    topComponent.setReportConfig(content, true);
    topComponent.setEditable(true);
    topComponent.runLoaded();
    ReportTestHelper.displayTime(topComponent, load.second);
  }

  public static LabelConfig createLabelTime() {
    return ReportOpenProfilTransversalViewNodeAction.createLabelTime();
  }

  public static LabelConfig createLabelNomEMH() {
    ReportLabelContent label = new ReportLabelContent();
    label.setContent(new ReportVariableKey(ReportVariableTypeEnum.EXPR, ReportExpressionHelper.EXPR_NOM));
    label.setUnit(false);
    final LabelConfig res = createLabelConfig(label);
    res.setHorizontalAlignment(SwingConstants.CENTER);
    return res;
  }

  public static LabelConfig createLabelConfig(ReportLabelContent label) {
    return ReportOpenProfilTransversalViewNodeAction.createLabelConfig(label);
  }
}
