/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.formule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLogGroupResult;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.projet.report.formule.FormuleContent;
import org.fudaa.dodico.crue.projet.report.formule.FormuleContentExpr;
import org.fudaa.dodico.crue.projet.report.formule.FormuleContentServiceUpdater;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataEdited;
import org.fudaa.dodico.crue.projet.report.formule.FormuleParametersExpr;
import org.fudaa.dodico.crue.projet.report.formule.FormuleServiceContent;
import org.fudaa.fudaa.crue.report.AbstractTestWithScenarioTest;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleContentServiceUpdaterTest extends AbstractTestWithScenarioTest {

  public FormuleContentServiceUpdaterTest() {
  }

  @Test
  public void testComputeChanges() {

    final String suffix = "_new";
    List<FormuleContent> old = new ArrayList<>();
    FormuleParametersExpr parameterExprDeleted = new FormuleParametersExpr();
    parameterExprDeleted.setId("removed");
    parameterExprDeleted.setFormule("4+1");

    FormuleParametersExpr parameterExprModified = new FormuleParametersExpr();
    parameterExprModified.setId("modified");
    parameterExprModified.setFormule("3+10");

    FormuleParametersExpr parameterExprRenamed = new FormuleParametersExpr();
    parameterExprRenamed.setId("renamed");
    final String oldNamed = parameterExprRenamed.getId();
    parameterExprRenamed.setFormule("2+100");

    FormuleParametersExpr parameterExprRenamedAndModified = new FormuleParametersExpr();
    parameterExprRenamedAndModified.setId("renamedAndModified");
    parameterExprRenamedAndModified.setFormule("1+1000");

    old.add(createContent(parameterExprDeleted));
    old.add(createContent(parameterExprModified));
    old.add(createContent(parameterExprRenamed));
    old.add(createContent(parameterExprRenamedAndModified));

    FormuleDataEdited edited = new FormuleDataEdited();

    FormuleParametersExpr newParameterExprModified = new FormuleParametersExpr();
    newParameterExprModified.setId(parameterExprModified.getId());
    newParameterExprModified.setFormule(parameterExprModified.getFormule() + "+1");
    edited.addParameter(parameterExprModified.getId(), newParameterExprModified);

    FormuleParametersExpr newParameterExprRenamed = new FormuleParametersExpr();
    newParameterExprRenamed.setId(parameterExprRenamed.getId() + suffix);
    newParameterExprRenamed.setFormule(parameterExprRenamed.getFormule());
    edited.addParameter(parameterExprRenamed.getId(), newParameterExprRenamed);

    FormuleParametersExpr newParameterExprRenamedAndModified = new FormuleParametersExpr();
    newParameterExprRenamedAndModified.setId(parameterExprRenamedAndModified.getId() + suffix);
    newParameterExprRenamedAndModified.setFormule(parameterExprRenamedAndModified.getFormule() + "+1");
    edited.addParameter(parameterExprRenamedAndModified.getId(), newParameterExprRenamedAndModified);

    FormuleParametersExpr newParameter = new FormuleParametersExpr();
    newParameter.setId("new");
    newParameter.setFormule("111");
    edited.addParameter(null, newParameter);

    FormuleContentServiceUpdater updater = new FormuleContentServiceUpdater(new FormuleServiceContent(old), globalService);
    CtuluLogGroupResult<FormuleServiceContent, FormuleDataChanges> computeChanges = updater.computeChanges(edited);
    FormuleServiceContent resultat = computeChanges.getResultat();

    assertEquals(edited.getAllParameters().size(), resultat.getVariables().size());
    Map<String, FormuleContent> newByName = TransformerHelper.toMapOfId(resultat.getVariables());

    assertTrue(newParameter == newByName.get(newParameter.getId()).getParameters());
    assertTrue(newParameterExprRenamedAndModified == newByName.get(newParameterExprRenamedAndModified.getId()).getParameters());
    assertTrue(newParameterExprModified == newByName.get(newParameterExprModified.getId()).getParameters());
    //different ici
    assertTrue(parameterExprRenamed == newByName.get(newParameterExprRenamed.getId()).getParameters());
    FormuleDataChanges complement = computeChanges.getComplement();
    assertTrue(complement.isRemoved(parameterExprDeleted.getId()));
    assertTrue(complement.isModified(parameterExprModified.getId()));
    assertTrue(complement.isModified(parameterExprRenamedAndModified.getId()));
    assertFalse(complement.isModified(parameterExprRenamed.getId()));
    assertFalse(complement.isModified(newParameter.getId()));

    //attention différent ici:
    assertTrue(complement.isRenamed(oldNamed));
    assertTrue(complement.isRenamed(parameterExprRenamedAndModified.getId()));
    assertFalse(complement.isRenamed(parameterExprModified.getId()));
    assertFalse(complement.isRenamed(newParameter.getId()));

    assertFalse(complement.isRemoved(oldNamed));
    assertFalse(complement.isRemoved(parameterExprRenamedAndModified.getId()));
  }

  public FormuleContentExpr createContent(FormuleParametersExpr parameterExprDeleted) {
    FormuleContentExpr exprDeleted = new FormuleContentExpr(globalService);
    exprDeleted.setParameter(parameterExprDeleted, Collections.emptyList());
    return exprDeleted;
  }
}
