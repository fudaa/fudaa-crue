/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.example;

import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.fudaa.crue.report.ReportTestHelper;
import org.fudaa.fudaa.crue.report.ReportTimeTopComponent;

import java.io.File;

/**
 * @author Frederic Deniger
 */
public class ReportTimeTopComponentExample {

    public static void main(String[] args) {
        Pair<EMHScenarioContainer, File> load = ReportTestHelper.readScenario();
        ReportTimeTopComponent topComponent = new ReportTimeTopComponent();
        topComponent.runLoaded();
        topComponent.setEditable(true);
        ReportTestHelper.display(topComponent, load.second);
    }
}
