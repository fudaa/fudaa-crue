/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.example;

import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.config.lit.ReportLongitudinalLimitHelper;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBrancheConfig;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalConfig;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.PerspectiveState;
import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.fudaa.fudaa.crue.report.ReportTestHelper;
import org.fudaa.fudaa.crue.report.actions.ReportOpenProfilTransversalViewNodeAction;
import org.fudaa.fudaa.crue.report.longitudinal.ReportProfilLongitudinalTopComponent;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.util.Lookup;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public class ReportProfilLongitudinalTopComponentExample {
  public static void main(final String[] args) {
    final Pair<EMHScenarioContainer, File> load = ReportTestHelper.readScenario();
    final SelectedPerspectiveService selectedPerspectiveService = Lookup.getDefault().lookup(SelectedPerspectiveService.class);
    selectedPerspectiveService.activePerspective(PerspectiveEnum.REPORT);
    final PerspectiveServiceReport perspectiveServiceReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);
    perspectiveServiceReport.setState(PerspectiveState.MODE_EDIT);
    final ReportProfilLongitudinalTopComponent topComponent = new ReportProfilLongitudinalTopComponent();
    final ReportLongitudinalConfig content = new ReportLongitudinalConfig();
    final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
    ReportLongitudinalBrancheConfig reportLongitudinalBrancheConfig = new ReportLongitudinalBrancheConfig();
    reportLongitudinalBrancheConfig.setLength(150);
    reportLongitudinalBrancheConfig.setName("Br_B1");
    content.getBranchesConfig().add(reportLongitudinalBrancheConfig);
    reportLongitudinalBrancheConfig = new ReportLongitudinalBrancheConfig();
    reportLongitudinalBrancheConfig.setLength(350);
    reportLongitudinalBrancheConfig.setDecalXp(0);
    reportLongitudinalBrancheConfig.setName("Br_B2");
    content.getBranchesConfig().add(reportLongitudinalBrancheConfig);
    content.getTimes().add(reportService.getTimeContent().getTimes().get(6));
    content.getProfilVariables().add(new ReportRunVariableKey(reportService.getRunCourant().getRunKey(),
        new ReportVariableKey(ReportVariableTypeEnum.LIMIT_PROFIL, ReportLongitudinalLimitHelper.getLimiteId(0))));
    content.getProfilVariables().add(new ReportRunVariableKey(reportService.getRunCourant().getRunKey(),
        new ReportVariableKey(ReportVariableTypeEnum.LIMIT_PROFIL, ReportLongitudinalLimitHelper.getLimiteId(1))));
    content.getProfilVariables().add(new ReportRunVariableKey(reportService.getRunCourant().getRunKey(),
        new ReportVariableKey(ReportVariableTypeEnum.LIMIT_PROFIL, ReportLongitudinalLimitHelper.getLimiteId(2))));
    content.getProfilVariables().add(new ReportRunVariableKey(reportService.getRunCourant().getRunKey(),
        new ReportVariableKey(ReportVariableTypeEnum.LIMIT_PROFIL, ReportLongitudinalLimitHelper.getLimiteId(3))));
    content.getProfilVariables().add(new ReportRunVariableKey(reportService.getRunCourant().getRunKey(),
        new ReportVariableKey(ReportVariableTypeEnum.LIMIT_PROFIL, ReportLongitudinalLimitHelper.getLimiteId(4))));
    content.getProfilVariables().add(new ReportRunVariableKey(reportService.getRunCourant().getRunKey(),
        new ReportVariableKey(ReportVariableTypeEnum.LIMIT_PROFIL, ReportLongitudinalLimitHelper.getLimiteId(5))));
    content.getProfilVariables().add(new ReportRunVariableKey(reportService.getRunCourant().getRunKey(), new ReportVariableKey(ReportVariableTypeEnum.READ, "q")));
    content.getProfilVariables().add(new ReportRunVariableKey(reportService.getRunCourant().getRunKey(), new ReportVariableKey(ReportVariableTypeEnum.READ, "y")));
    final File file = AbstractTestParent.getFile("/externe/zXt.txt");
    if (file.exists()) {
      content.setExternalFiles(Collections.singletonList(file.getAbsolutePath()));
      file.deleteOnExit();
    }

    final List<LabelConfig> labels = new ArrayList<>();
    labels.add(createLabelTime());
    content.getLoiLegendConfig().setLabels(labels);
    topComponent.setReportConfig(content, true);
    topComponent.setEditable(true);
    topComponent.runLoaded();
    ReportTestHelper.displayTime(topComponent, load.second);
  }

  public static LabelConfig createLabelNomEMH() {
    return ReportOpenProfilTransversalViewNodeAction.createLabelNomEMH();
  }

  public static LabelConfig createLabelTime() {
    return ReportOpenProfilTransversalViewNodeAction.createLabelTime();
  }

  public static LabelConfig createLabelConfigQ() {
    return ReportOpenProfilTransversalViewNodeAction.createLabelConfigQ();
  }

  public static LabelConfig createLabelConfig(final ReportLabelContent label) {
    return ReportOpenProfilTransversalViewNodeAction.createLabelConfig(label);
  }
}
