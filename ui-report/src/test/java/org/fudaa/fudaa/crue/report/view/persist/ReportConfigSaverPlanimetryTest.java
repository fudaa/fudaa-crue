/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import org.fudaa.ctulu.converter.ColorToStringTransformer;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.conf.Option;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.dodico.crue.projet.report.transformer.KeysToStringConverter;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.fudaa.dodico.crue.test.FileTest;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.fudaa.fudaa.crue.report.persist.ReportPlanimetryConfig;
import org.fudaa.fudaa.crue.report.persist.ReportPlanimetryConfigPersist;
import org.junit.Test;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * @author Frederic Deniger
 */
public class ReportConfigSaverPlanimetryTest extends FileTest {
  final ReportRunKey current = new ReportRunKey();

  public ReportConfigSaverPlanimetryTest() {
  }

  @Test
  public void testRead() {
    final File file = AbstractTestParent.getFile("/org/fudaa/fudaa/crue/report/view/persist/planimetry.xml");
    final ReportConfigContrat metier = read(file);
    assertNotNull(metier);
    assertEquals(ReportPlanimetryConfigPersist.class, metier.getClass());
    file.delete();
    final ReportPlanimetryConfigPersist config = (ReportPlanimetryConfigPersist) metier;
    testContent(config);
  }

  @Test
  public void testWrite() throws IOException {
    final File file = AbstractTestParent.getFile("/org/fudaa/fudaa/crue/report/view/persist/planimetry.xml");
    ReportPlanimetryConfigPersist metier = (ReportPlanimetryConfigPersist) read(file);
    final ReportPlanimetryConfig config = metier.createConfig();
    file.delete();
    final File out = createTempFile("planimetry", ".xml");
    createSaver().save(out, config, "test");
    metier = (ReportPlanimetryConfigPersist) read(out);
    out.delete();
    testContent(metier);
  }

  private ReportConfigContrat read(final File file) {
    assertTrue(file.exists());
    final CrueIOResu<ReportConfigContrat> read = createReader().read(file);
    return read.getMetier();
  }

  private void testContent(final ReportPlanimetryConfigPersist config) {
    final List<Option> visuConfiguration = config.getVisuConfiguration();
    final Map<String, Option> options = new HashMap<>();
    for (final Option option : visuConfiguration) {
      options.put(option.getNom(), option);
    }
    final Option colorOption = options.get("branches.colors.EMHBrancheSaintVenant");
    final String valeur = colorOption.getValeur();
    final Color color = new ColorToStringTransformer().fromString(valeur);
    assertEquals(new Color(204, 0, 51), color);
  }

  @Test
  public void testCreateReadWriteEmptyPlanimetry() throws IOException {
    final ReportPlanimetryConfig config = new ReportPlanimetryConfig();
    final File target = createTempFile("planimetry", ".xml");
    final ReportConfigSaver saver = createSaver();
    saver.save(target, config, "test");
    final CrueIOResu<ReportConfigContrat> read = createReader().read(target);
    final ReportConfigContrat metier = read.getMetier();
    assertNotNull(metier);
    assertEquals(ReportPlanimetryConfigPersist.class, metier.getClass());
    target.delete();
    final ReportPlanimetryConfigPersist readConfig = (ReportPlanimetryConfigPersist) metier;
    assertNotNull(readConfig.getReportPlanimetryExtraContainer());
    assertNotNull(readConfig.getVisuConfiguration());
  }

  private ReportConfigSaver createSaver() {
    final KeysToStringConverter keysToString = new KeysToStringConverter(current);
    return new ReportConfigSaver(new TraceToStringConverter(), keysToString);
  }

  private ReportConfigReader createReader() {
    final KeysToStringConverter keysToString = new KeysToStringConverter(current);
    return new ReportConfigReader(new TraceToStringConverter(), keysToString);
  }
}
