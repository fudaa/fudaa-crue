/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.example;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collections;
import javax.swing.JCheckBox;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryBrancheExtraData;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryTraceExtraData;
import org.fudaa.fudaa.crue.report.*;
import org.fudaa.fudaa.crue.report.service.ReportVisuPanelService;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ReportVisualTopComponentExample {

  public ReportVisualTopComponentExample() {
  }

  private static ReportPlanimetryBrancheExtraData createBrancheData() {
    final ReportPlanimetryBrancheExtraData res = new ReportPlanimetryBrancheExtraData();
    res.setUseLabels(true);
    res.setSameIfSelected(false);
    final ReportLabelContent ct = new ReportLabelContent();
    ct.setContent(new ReportVariableKey(ReportVariableTypeEnum.READ, "vol"));
    final ReportLabelContent ctSelected = new ReportLabelContent();
    ctSelected.setContent(new ReportVariableKey(ReportVariableTypeEnum.READ, "vol"));
    ctSelected.setPrefix("Vol: ");
    ctSelected.setUnit(true);
    res.setLabels(Collections.singletonList(ct));
    res.setLabelsIfSelected(Collections.singletonList(ctSelected));
    return res;
  }

  private static ReportPlanimetryTraceExtraData createTraceData() {
    final ReportPlanimetryTraceExtraData res = new ReportPlanimetryTraceExtraData();
    res.setUseLabels(true);
    res.setSameIfSelected(false);
    final ReportLabelContent ct = new ReportLabelContent();
    ct.setContent(new ReportVariableKey(ReportVariableTypeEnum.READ, "z"));
    final ReportLabelContent ctSelected = new ReportLabelContent();
    ctSelected.setContent(new ReportVariableKey(ReportVariableTypeEnum.READ, "z"));
    ctSelected.setPrefix("Z: ");
    ctSelected.setUnit(true);
    res.setLabels(Collections.singletonList(ct));
    res.setLabelsIfSelected(Collections.singletonList(ctSelected));
    return res;
  }

  public static void main(final String[] args) {
    //pour activer le service
    System.setProperty("dev.etcDir", "C:\\data\\Fudaa-Crue\\etc");
    final ReportVisuPanelService reportVisuPanelService = Lookup.getDefault().lookup(ReportVisuPanelService.class);
    final Pair<EMHScenarioContainer, File> load = ReportTestHelper.readScenario();
    final ReportVisualTopComponent topComponent = new ReportVisualTopComponent();
    topComponent.getReportPlanimetryConfigurer().getBrancheConfigurationExtra().setData(createBrancheData());
    topComponent.getReportPlanimetryConfigurer().getTraceConfigurationExtra().setData(createTraceData());
    topComponent.setEditable(true);
    final JCheckBox cb = new JCheckBox("editable");
    cb.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        topComponent.setEditable(cb.isSelected());
      }
    });
    final Runnable runnable = new Runnable() {
      @Override
      public void run() {
        topComponent.open();
        topComponent.setEditable(true);
        reportVisuPanelService.resultChanged(null);
        ReportTestHelper.displayTime(topComponent, load.second, cb);
      }
    };
    EventQueue.invokeLater(runnable);
  }
}
