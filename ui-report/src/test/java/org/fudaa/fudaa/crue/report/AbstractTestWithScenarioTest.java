/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report;

import java.io.File;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public abstract class AbstractTestWithScenarioTest {

  static File toDelete;
  static final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
  protected static final ReportGlobalServiceContrat globalService = Lookup.getDefault().lookup(ReportGlobalServiceContrat.class);

  @BeforeClass
  public static void beforeTest() {
    if (!reportService.isRunLoaded()) {
      toDelete = ReportTestHelper.readScenario().second;
    }
  }

  @AfterClass
  public static void afterTest() {
    CtuluLibFile.deleteDir(toDelete);
  }
}
