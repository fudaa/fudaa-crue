/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.example;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.dodico.crue.projet.report.persist.ReportTransversalConfig;
import org.fudaa.fudaa.crue.report.ReportTestHelper;
import org.fudaa.fudaa.crue.report.actions.ReportOpenProfilTransversalViewNodeAction;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.fudaa.fudaa.crue.report.transversal.ReportProfilTransversalTopComponent;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ReportProfilTransversalTopComponentExample {

  public static void main(String[] args) {
    Pair<EMHScenarioContainer, File> load = ReportTestHelper.readScenario();
    ReportProfilTransversalTopComponent topComponent = new ReportProfilTransversalTopComponent();
    ReportTransversalConfig content = new ReportTransversalConfig();
    ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
    content.addProfilVariable(new ReportRunVariableKey(reportService.getRunCourant().getRunKey(), ReportVariableTypeEnum.READ, "z"));
    content.setSectionName("St_Prof11");
    List<LabelConfig> labels = new ArrayList<>();
    labels.add(createLabelTime());
    labels.add(createLabelConfigQ());
    content.getLoiLegendConfig().setLabels(labels);
    content.getLoiLegendConfig().setTitleConfig(createLabelNomEMH());
    topComponent.setReportConfig(content, true);
    topComponent.setEditable(true);
    topComponent.runLoaded();
    ReportTestHelper.displayTime(topComponent, load.second);
  }

  public static LabelConfig createLabelNomEMH() {
    return ReportOpenProfilTransversalViewNodeAction.createLabelNomEMH();
  }

  public static LabelConfig createLabelTime() {
    return ReportOpenProfilTransversalViewNodeAction.createLabelTime();
  }

  public static LabelConfig createLabelConfigQ() {
    return ReportOpenProfilTransversalViewNodeAction.createLabelConfigQ();
  }

  public static LabelConfig createLabelConfig(ReportLabelContent label) {
    return ReportOpenProfilTransversalViewNodeAction.createLabelConfig(label);
  }
}
