/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.example;

import java.io.File;
import java.util.Collections;

import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.projet.report.ReportMultiVarConfig;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.fudaa.crue.report.ReportTestHelper;
import org.fudaa.fudaa.crue.report.actions.ReportOpenProfilTransversalViewNodeAction;
import org.fudaa.fudaa.crue.report.multivar.ReportMultiVarTopComponent;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ReportMultiVarTopComponentExample {

  public static void main(final String[] args) {
    final Pair<EMHScenarioContainer, File> load = ReportTestHelper.readScenario();
    final ReportMultiVarTopComponent topComponent = new ReportMultiVarTopComponent();
    final ReportMultiVarConfig content = new ReportMultiVarConfig();
    final ReportResultProviderService reportService = Lookup.getDefault().lookup(ReportResultProviderService.class);
    content.setEMHAndAddRunCourant(Collections.singletonList("St_Prof11"), reportService);
    content.setHorizontalVar("q");
    content.getVariables().add("z");
    content.getLoiLegendConfig().setTitleConfig(createLabelTime());
    topComponent.setReportConfig(content, true);
    topComponent.setEditable(true);
    topComponent.runLoaded();
    ReportTestHelper.displayTime(topComponent, load.second);
  }

  public static LabelConfig createLabelTime() {
    return ReportOpenProfilTransversalViewNodeAction.createLabelTime();
  }

  public static LabelConfig createLabelConfig(final ReportLabelContent label) {
    return ReportOpenProfilTransversalViewNodeAction.createLabelConfig(label);
  }
}
