/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.ReportViewLineInfo;
import org.fudaa.dodico.crue.test.FileTest;
import org.fudaa.fudaa.crue.report.view.ReportViewLine;
import org.joda.time.LocalDateTime;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class IndexSaverUITest extends FileTest {

  private final String auteurCreation = "AuteurCreation";
  private final String auteurDerniereModif = "AuteurDerniereModif";

  public IndexSaverUITest() {
  }


  @Test
  public void testWrite() throws IOException {
    ReportIndexReaderSaverUI saver = new ReportIndexReaderSaverUI();
    File target = createTempFile("index", ".xml");
    final long creationDate = System.currentTimeMillis() - 10000;
    final long modificationDate = System.currentTimeMillis();
    final String fileNameOne = "fileName 1";
    final String fileNameTwo = "fileName 2";
    ReportViewLine lineOne = new ReportViewLine(ReportContentType.LONGITUDINAL, createInfo(creationDate, modificationDate, fileNameOne), null, null, null);
    ReportViewLine lineTwo = new ReportViewLine(ReportContentType.LONGITUDINAL, createInfo(creationDate, modificationDate, fileNameTwo), null, null, null);
    lineOne.getLineInformation().setNom("Vue 1");
    lineTwo.getLineInformation().setNom("Vue 2");
    CtuluLog save = saver.save(target, Arrays.asList(lineOne, lineTwo), "longitudinal");

    assertTrue(save.isEmpty());
    CrueIOResu<List<ReportViewLineInfo>> read = saver.read(target);
    assertTrue(read.getAnalyse().isEmpty());
    List<ReportViewLineInfo> metier = read.getMetier();
    testResult(metier, creationDate, modificationDate, fileNameOne, fileNameTwo);
  }

  private ReportViewLineInfo createInfo(final long creationDate, final long modificationDate, final String fileName) {
    ReportViewLineInfo info = new ReportViewLineInfo();
    info.setAuteurCreation(auteurCreation);
    info.setAuteurDerniereModif(auteurDerniereModif);
    info.setDateCreation(new LocalDateTime(creationDate));
    info.setDateDerniereModif(new LocalDateTime(modificationDate));
    info.setFilename(fileName);
    info.setCommentaire(fileName);
    return info;
  }

  private void testGeneralInfos(final ReportViewLineInfo line, final long creationDate, final long modificationDate) {
    assertEquals(auteurCreation, line.getAuteurCreation());
    assertEquals(auteurDerniereModif, line.getAuteurDerniereModif());
    assertEquals(creationDate, line.getDateCreation().toDateTime().getMillis());
    assertEquals(modificationDate, line.getDateDerniereModif().toDateTime().getMillis());
  }

  private void testResult(List<ReportViewLineInfo> metier, final long creationDate, final long modificationDate, final String fileNameOne, final String fileNameTwo) {
    assertEquals(2, metier.size());
    ReportViewLineInfo line = metier.get(0);
    assertEquals("Vue 1", line.getNom());
    assertEquals(fileNameOne, line.getFilename());
    assertEquals(fileNameOne, line.getCommentaire());
    testGeneralInfos(line, creationDate, modificationDate);
    line = metier.get(1);
    assertEquals("Vue 2", line.getNom());
    assertEquals(fileNameTwo, line.getFilename());
    assertEquals(fileNameTwo, line.getCommentaire());
    testGeneralInfos(line, creationDate, modificationDate);
  }
}
