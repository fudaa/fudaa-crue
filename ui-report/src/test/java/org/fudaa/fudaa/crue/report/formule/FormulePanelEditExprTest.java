/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.formule;

import javax.swing.DefaultComboBoxModel;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.projet.report.formule.FormuleParametersExpr;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class FormulePanelEditExprTest {

  public FormulePanelEditExprTest() {
  }

  @Test
  public void testCreateEditFormule() {
    CtuluExpr exp = new CtuluExpr();
    exp.addVar("x", "x");
    exp.addVar("y", "y");
    exp.addVar("z", "z");
    exp.getParser().parseExpression("x+y+1");
    FormulePanelEditExpr.DesignExpr design = new FormulePanelEditExpr.DesignExpr(exp);
    design.tfName.setText("formule");
    CrueConfigMetier ccm = CrueConfigMetierForTest.DEFAULT;
    PropertyNature nature1 = ccm.getNature("nat_Vol");
    PropertyNature nature2 = ccm.getNature("nat_Beta");
    design.cbNature.setModel(new DefaultComboBoxModel(new PropertyNature[]{nature1, nature2}));
    design.cbNature.setSelectedItem(nature1);
    FormuleParametersExpr createExpr = FormulePanelEditExpr.createExpr(design);
    assertEquals(design.tfName.getText(), createExpr.getNom());
    assertEquals(nature1.getNom(), createExpr.getNature());
  }
}
