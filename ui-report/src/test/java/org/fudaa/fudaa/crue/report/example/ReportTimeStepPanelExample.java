/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.example;

import org.fudaa.fudaa.crue.report.ReportTestHelper;
import org.fudaa.fudaa.crue.report.time.ReportTimeStepTopPanel;

import javax.swing.*;
import java.util.Arrays;

/**
 *
 * @author Frederic Deniger
 */
public class ReportTimeStepPanelExample {

  public static void main(String[] args) {
    ReportTimeStepTopPanel panel = new ReportTimeStepTopPanel();
    JPanel pn = panel.getPanel();
    panel.setValues(Arrays.asList("Un", "deux", "trois", "quatre", "cinq"));
    ReportTestHelper.display(pn, null);
  }
}
