/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.transformer;

import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.transformer.KeysToStringConverter;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class ReportRunVariableTimeKeyMultiToStringTransformerTest {

  public ReportRunVariableTimeKeyMultiToStringTransformerTest() {
  }

  @Test
  public void testReadWrite() {
    final ReportRunKey current = new ReportRunKey();
    final ResultatTimeKey currentKey = new ResultatTimeKey("calcul", 1234, false);
    final KeysToStringConverter converter = new KeysToStringConverter(current);
    final AbstractPropertyToStringTransformer<ReportRunVariableTimeKey> transformer = converter.getReportRunVariableTimeKeyConverter();
    ReportRunVariableTimeKey key = new ReportRunVariableTimeKey(current, ReportVariableTypeEnum.READ, "variable", currentKey);
    String toString = transformer.toString(key);
    assertEquals("CURRENT|variable;read|calcul;1234;NO_DAY", toString);
    ReportRunVariableTimeKey fromKey = transformer.fromString(toString);
    assertNotNull(fromKey);
    assertTrue(current == fromKey.getReportRunKey().getReportRunKey());
    assertEquals(key, fromKey);
    //sans le NO_DAY
    fromKey = transformer.fromString("CURRENT|variable;read|calcul;1234");
    assertNotNull(fromKey);
    assertTrue(current == fromKey.getReportRunKey().getReportRunKey());
    assertEquals(key, fromKey);
    final ReportRunKey reportRunKeyAlternatif = new ReportRunKey("manager", "run");
    key = new ReportRunVariableTimeKey(reportRunKeyAlternatif, ReportVariableTypeEnum.READ, "variable", currentKey);
    toString = transformer.toString(key);
    assertEquals("manager;run|variable;read|calcul;1234;NO_DAY", toString);
    fromKey = transformer.fromString(toString);
    assertEquals(key, fromKey);



  }
}
