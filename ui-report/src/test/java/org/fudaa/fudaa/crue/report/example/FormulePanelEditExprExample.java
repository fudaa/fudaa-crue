/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.example;

import java.io.File;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.fudaa.crue.report.ReportTestHelper;
import org.fudaa.fudaa.crue.report.formule.FormulePanelEditExpr;
import org.openide.nodes.Children;

/**
 *
 * @author Frederic Deniger
 */
public class FormulePanelEditExprExample {

  public static void main(String[] args) {
    Pair<EMHScenarioContainer, File> load = ReportTestHelper.readScenario();
    FormulePanelEditExpr edit = new FormulePanelEditExpr(new org.openide.nodes.AbstractNode(Children.LEAF));
    CtuluLibFile.deleteDir(load.second);
    edit.show();
  }
}
