/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.formule;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.CtuluUIDefault;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;
import org.fudaa.dodico.crue.projet.report.data.ReportRunContent;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.formule.function.AggregationCacheKey;
import org.fudaa.fudaa.crue.report.contrat.ReportReadResultServiceContrat;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class MockResultProvider implements ReportResultProviderServiceContrat, ReportReadResultServiceContrat, ReportGlobalServiceContrat {

  final Map<String, Double> values = new HashMap<>();
  final ReportResultProviderServiceContrat reportResultProviderServiceContrat = Lookup.getDefault().lookup(ReportResultProviderServiceContrat.class);
  final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);

  public void setValue(String name, double val) {
    values.put(name, val);
  }

  @Override
  public CtuluUI getUI() {
    return new CtuluUIDefault();
  }

  @Override
  public ReportResultProviderServiceContrat getResultService() {
    return this;
  }

  @Override
  public Double getCachedValue(AggregationCacheKey cacheKey) {
    return null;
  }

  @Override
  public EMH getEMH(String nom, ReportRunKey key) {
    return reportService.getRunCourant().getEMH(nom);
  }

  @Override
  public void putCachedValue(AggregationCacheKey cacheKey, Double resValue) {
  }

    @Override
  public List<ResultatTimeKey> getAllPermanentTimeKeys() {
    return reportService.getTimeContent().getPermanentsTimes();
  }

  @Override
  public List<ResultatTimeKey> getAllTransientTimeKeys() {
    return reportService.getTimeContent().getTransitoiresTimes();
  }

  @Override
  public <T> T showProgressDialogAndRun(ProgressRunnable<T> operation, String displayName) {
    return operation.run(null);
  }

  @Override
  public ReportRunContent getRunCourant() {
    return reportResultProviderServiceContrat.getRunCourant();
  }

  @Override
  public PropertyNature getPropertyNature(String key) {
    return reportResultProviderServiceContrat.getPropertyNature(key);
  }

  @Override
  public ReportVariableKey createVariableKey(String varName) {
    return new ReportVariableKey(ReportVariableTypeEnum.READ, StringUtils.uncapitalize(varName));
  }

  @Override
  public Double getValue(ReportRunVariableKey key, String emhNom, Collection<ResultatTimeKey> selectedTimesInRapport) {
    return getReadValue(key, emhNom);
  }

  @Override
  public Double getValue(ResultatTimeKey selectedTime, ReportRunVariableKey key, String emhNom, Collection<ResultatTimeKey> selectedTimesInRapport) {
    return getReadValue(selectedTime, key, emhNom);
  }

  @Override
  public Double getReadValue(ReportRunVariableKey key, String emhNom) {
    return values.get(key.getVariable().getVariableName());
  }

  @Override
  public CrueConfigMetier getCcm() {
    return CrueConfigMetierForTest.DEFAULT;
  }

  @Override
  public Double getReadValue(ResultatTimeKey selectedTime, ReportRunVariableKey key, String emhNom) {
    return values.get(key.getVariable().getVariableName());
  }
}
