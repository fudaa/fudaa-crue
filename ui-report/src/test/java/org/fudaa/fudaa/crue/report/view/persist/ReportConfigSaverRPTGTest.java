/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.projet.report.ReportRPTGConfig;
import org.fudaa.dodico.crue.projet.report.data.ExternFileKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.dodico.crue.projet.report.transformer.KeysToStringConverter;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.fudaa.dodico.crue.test.FileTest;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.junit.Test;

import java.awt.*;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

/**
 *
 * @author Frederic Deniger
 */
public class ReportConfigSaverRPTGTest extends FileTest {

  private final String fileName = "/org/fudaa/fudaa/crue/report/view/persist/rptg.xml";
  final ReportRunKey current = new ReportRunKey();

  public ReportConfigSaverRPTGTest() {
  }

  @Test
  public void testReadRPTG() {
    final File file = AbstractTestParent.getFile(fileName);
    final ReportConfigContrat metier = read(file);
    assertNotNull(metier);
    assertEquals(ReportRPTGConfig.class, metier.getClass());
    file.delete();
    final ReportRPTGConfig config = (ReportRPTGConfig) metier;
    testContent(config);
  }

  @Test
  public void testWriteRPTG() throws IOException {
    final File file = AbstractTestParent.getFile(fileName);
    ReportConfigContrat metier = read(file);
    file.delete();
    final File out = createTempFile("rptg", ".xml");
    createSaver().save(out, metier, "test");
    metier = read(out);
    out.delete();
    testContent((ReportRPTGConfig) metier);
  }

  private ReportConfigContrat read(final File file) {
    assertTrue(file.exists());
    final ReportConfigSaver saver = createSaver();
    final CrueIOResu<ReportConfigContrat> read = createReader().read(file);
    return read.getMetier();
  }

  @Test
  public void testCreateReadWriteEmptyRPTG() throws IOException {
    final ReportRPTGConfig config = new ReportRPTGConfig();
    assertNotNull(config.getExternFiles());
    final File target = createTempFile("rptg", ".xml");
    final ReportConfigSaver saver = createSaver();
    saver.save(target, config, "test");
    final CrueIOResu<ReportConfigContrat> read = createReader().read(target);
    final ReportConfigContrat metier = read.getMetier();
    assertNotNull(metier);
    assertEquals(ReportRPTGConfig.class, metier.getClass());
    target.delete();
    final ReportRPTGConfig readConfig = (ReportRPTGConfig) metier;
    assertNotNull(readConfig.getExternFiles());
    assertNotNull(readConfig.getLoiLegendConfig());
    assertNotNull(readConfig.getLoiLegendConfig().getLabels());
  }

  private ReportConfigSaver createSaver() {
    final KeysToStringConverter keysToString = new KeysToStringConverter(current);
    return new ReportConfigSaver(new TraceToStringConverter(), keysToString);
  }
  private ReportConfigReader createReader() {
    final KeysToStringConverter keysToString = new KeysToStringConverter(current);
    return new ReportConfigReader(new TraceToStringConverter(), keysToString);
  }

  private void testContent(final ReportRPTGConfig reportRPTGConfig) {
    assertNotNull(reportRPTGConfig.getAxeVConfig("lsto"));
    assertNotNull(reportRPTGConfig.getAxeVConfig("beta"));

    assertEquals("St_PROF1", reportRPTGConfig.getEmh());
    assertEquals("St_PROF3", reportRPTGConfig.getPrevious());

    assertEquals(1, reportRPTGConfig.getExternFiles().size());
    assertEquals("C:\\extern.txt", reportRPTGConfig.getExternFiles().get(0));
    final ExternFileKey externFileKey = new ExternFileKey("C:\\extern.txt", "Z", "Beta");
    final EGCourbePersist externFileCourbeConfig = reportRPTGConfig.getExternFileCourbeConfig(externFileKey);
    assertNotNull(externFileCourbeConfig);
    final TraceLigneModel lineModel = externFileCourbeConfig.getLineModel_();
    assertEquals(1, lineModel.getTypeTrait());
    assertEquals(1, (int) lineModel.getEpaisseur());
    assertEquals(Color.BLACK, lineModel.getCouleur());
  }
}
