/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.projet.report.ReportMultiVarConfig;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.dodico.crue.projet.report.transformer.KeysToStringConverter;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.fudaa.dodico.crue.test.FileTest;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.fudaa.ebli.courbe.EGAxeVerticalPersist;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

/**
 *
 * @author Frederic Deniger
 */
public class ReportConfigSaverMultiVarTest extends FileTest {

  final ReportRunKey current = new ReportRunKey();
  private final String fileName = "/org/fudaa/fudaa/crue/report/view/persist/multiVar.xml";

  public ReportConfigSaverMultiVarTest() {
  }

  @Test
  public void testReadMultivar() {
    final File file = AbstractTestParent.getFile(fileName);
    final ReportConfigContrat metier = read(file);
    assertNotNull(metier);
    assertEquals(ReportMultiVarConfig.class, metier.getClass());
    file.delete();
    final ReportMultiVarConfig config = (ReportMultiVarConfig) metier;
    testContent(config);
  }

  @Test
  public void testWriteMultivar() throws IOException {
    final File file = AbstractTestParent.getFile(fileName);
    ReportConfigContrat metier = read(file);
    file.delete();
    final File out = createTempFile("multivar", ".xml");
    createSaver().save(out, metier, "test");
    metier = read(out);
    out.delete();
    testContent((ReportMultiVarConfig) metier);
  }

  private ReportConfigContrat read(final File file) {
    assertTrue(file.exists());
    final CrueIOResu<ReportConfigContrat> read = createReader().read(file);
    return read.getMetier();
  }

  @Test
  public void testCreateReadWriteEmptyMultiVar() throws IOException {
    final ReportMultiVarConfig config = new ReportMultiVarConfig();
    assertNotNull(config.getExternFiles());
    final File target = createTempFile("multivar", ".xml");
    final ReportConfigSaver saver = createSaver();
    saver.save(target, config, "test");
    final CrueIOResu<ReportConfigContrat> read = createReader().read(target);
    final ReportConfigContrat metier = read.getMetier();
    assertNotNull(metier);
    assertEquals(ReportMultiVarConfig.class, metier.getClass());
    target.delete();
    final ReportMultiVarConfig readConfig = (ReportMultiVarConfig) metier;
    assertNotNull(readConfig.getExternFiles());
    assertNotNull(readConfig.getLoiLegendConfig());
    assertNotNull(readConfig.getLoiLegendConfig().getLabels());
    assertNotNull(readConfig.getEmhs());
    assertNotNull(readConfig.getReportRunEmhs());
  }

  private ReportConfigSaver createSaver() {
    final KeysToStringConverter keysToString = new KeysToStringConverter(current);
    return new ReportConfigSaver(new TraceToStringConverter(), keysToString);
  }

  private ReportConfigReader createReader() {
    final KeysToStringConverter keysToString = new KeysToStringConverter(current);
    return new ReportConfigReader(new TraceToStringConverter(), keysToString);
  }

  private void testContent(final ReportMultiVarConfig reportMultiVarConfig) {
    EGAxeVerticalPersist axeVConfig = reportMultiVarConfig.getAxeVConfig("vc");
    assertNotNull(axeVConfig);
    assertTrue(axeVConfig.isDroite());
    axeVConfig = reportMultiVarConfig.getAxeVConfig("z");
    assertNotNull(axeVConfig);
    assertFalse(axeVConfig.isDroite());
    assertEquals("q", reportMultiVarConfig.getHorizontalVar());
    assertEquals(3, reportMultiVarConfig.getEmhs().size());
    assertEquals("St_PROF1", reportMultiVarConfig.getEmhs().get(0));
    assertEquals("St_PROF2", reportMultiVarConfig.getEmhs().get(1));
    assertEquals("St_PROF3", reportMultiVarConfig.getEmhs().get(2));

    assertEquals(2, reportMultiVarConfig.getVariables().size());
    assertEquals("z", reportMultiVarConfig.getVariables().get(0));
    assertEquals("vc", reportMultiVarConfig.getVariables().get(1));

    assertEquals(5, reportMultiVarConfig.getReportRunEmhs().size());

  }
}
