/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.helper;

import java.io.File;
import java.io.IOException;
import org.fudaa.ctulu.CtuluLibFile;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class UniqueFileCreatorTest {

  public UniqueFileCreatorTest() {
  }

  @Test
  public void testCreate() throws IOException {
    File createTempDir = CtuluLibFile.createTempDir();
    File one = new File(createTempDir, "001.xml");
    assertTrue(one.createNewFile());
    UniqueFileCreator creator = new UniqueFileCreator(createTempDir);
    assertEquals("002.xml", creator.createNewFilename());
    assertEquals("003.xml", creator.createNewFilename());
    CtuluLibFile.deleteDir(createTempDir);
  }
}
