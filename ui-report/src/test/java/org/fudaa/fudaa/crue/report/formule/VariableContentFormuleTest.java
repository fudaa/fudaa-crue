/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.formule;

import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.formule.FormuleCalculatorExpr;
import org.fudaa.dodico.crue.projet.report.formule.FormuleContentExpr;
import org.fudaa.dodico.crue.projet.report.formule.FormuleParametersExpr;
import org.fudaa.dodico.crue.projet.report.formule.function.EnumFormuleStat;
import org.fudaa.fudaa.crue.report.AbstractTestWithScenarioTest;
import org.fudaa.fudaa.crue.report.service.RangeSelectedTimeKey;
import org.fudaa.fudaa.crue.report.service.ReportService;
import static org.junit.Assert.*;
import org.junit.Test;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class VariableContentFormuleTest extends AbstractTestWithScenarioTest {

  public VariableContentFormuleTest() {
  }

  @Test
  public void testGoodFormula() {
    final MockResultProvider mock = new MockResultProvider();
    mock.setValue("vol", 100);
    mock.setValue("z", 1000);

    final FormuleParametersExpr params = new FormuleParametersExpr();
    params.setFormule("Vol+Z+10");
    final FormuleContentExpr content = new FormuleContentExpr(mock);
    final CtuluLog log = content.setParameter(params, Collections.emptyList());
    assertFalse(log.containsErrors());
    final FormuleCalculatorExpr calculator = content.getCalculator();

//    calculator.setService(mock);
    final Double value = calculator.getValue(null, new ReportRunVariableKey(new ReportRunKey(), ReportVariableTypeEnum.FORMULE, ""), null, null);
    assertEquals(1110, value.intValue());
  }

  @Test
  public void testBadFormula() {
    final FormuleParametersExpr params = new FormuleParametersExpr();
    params.setFormule("Vol+y+10");
    final FormuleContentExpr content = new FormuleContentExpr(globalService);
    final CtuluLog log = content.setParameter(params, Collections.emptyList());
    assertTrue(log.containsErrorOrSevereError());
    final CtuluLogRecord get = log.getRecords().get(0);
    assertEquals("Unrecognized symbol \"y\"", get.getMsg().trim());
  }

  @Test
  public void testAllAggregationFormule() {
    final List<String> expressions = EnumFormuleStat.getExpressions();
    final MockResultRunProvider runProvider = new MockResultRunProvider();
    for (final String expression : expressions) {
      final FormuleParametersExpr params = new FormuleParametersExpr();
      params.setFormule(expression + "(\"Z\")");
      final FormuleContentExpr content = new FormuleContentExpr(runProvider);
      final CtuluLog log = content.setParameter(params, Collections.emptyList());
      assertFalse(log.containsErrorOrSevereError());
    }

  }

  @Test
  public void testNotContainsTimeDependantVariable() {
    final MockResultRunProvider runProvider = new MockResultRunProvider();
    checkIsTimeDependant(runProvider, "1+1", false);
    checkIsTimeDependant(runProvider, EnumFormuleStat.MAX_ALL_PERMANENT.getExpression() + "(\"Z\")", false);
    checkIsTimeDependant(runProvider, EnumFormuleStat.MAX_ALL_PERMANENT.getExpression() + "(\"Z\")*2", false);
    checkIsTimeDependant(runProvider, EnumFormuleStat.MAX_ALL_PERMANENT.getExpression() + "(\"Z\")*2+e", false);
    checkIsTimeDependant(runProvider, EnumFormuleStat.MAX_ALL_PERMANENT.getExpression() + "(\"Z\")*2+e+" + EnumFormuleStat.MOY_SELECTION_PERMANENT.
            getExpression() + "(\"Z\")", false);

  }

  @Test
  public void testContainsTimeDependantVariable() {
    final MockResultRunProvider runProvider = new MockResultRunProvider();
    checkIsTimeDependant(runProvider, "Z", true);
    checkIsTimeDependant(runProvider, "cos(Z)", true);
    checkIsTimeDependant(runProvider, EnumFormuleStat.MAX_ALL_PERMANENT.getExpression() + "(\"Z\")*2+Vol", true);

  }

  protected void checkIsTimeDependant(final MockResultRunProvider runProvider, final String formule, final boolean isTimeDependant) {
    final FormuleParametersExpr params = new FormuleParametersExpr();
    params.setFormule(formule);
    final FormuleContentExpr content = new FormuleContentExpr(runProvider);
    final CtuluLog log = content.setParameter(params, Collections.emptyList());
    assertFalse(log.containsErrorOrSevereError());
    final FormuleCalculatorExpr calculator = content.getCalculator();
    assertEquals("formule is " + (isTimeDependant ? "time dependant" : "not time dependant") + " " + formule, isTimeDependant, calculator.
            containsTimeDependantVariable());
  }

  @Test
  public void testWithAggregation() {
    //le calcul se fait sur la sélection des pas de temps.
    final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
    reportService.setRangeSelectedTimeKey(new RangeSelectedTimeKey(Collections.singletonList(new ResultatTimeKey("Cc_P1"))));

    final MockResultProvider mockCurrent = new MockResultProvider();
    mockCurrent.setValue("z", 1000);
    final MockResultProvider mockOther = new MockResultProvider();
    mockOther.setValue("z", 10);
    final ReportRunKey current = new ReportRunKey();
    final ReportRunKey other = new ReportRunKey("Test", "Test");
    final MockResultRunProvider runProvider = new MockResultRunProvider();
    runProvider.setValue(current, mockCurrent);
    runProvider.setValue(other, mockOther);

    final FormuleParametersExpr params = new FormuleParametersExpr();
    params.setFormule(EnumFormuleStat.MAX_ALL_TRANSIENT.getExpression() + "(\"Z\")");
    final FormuleContentExpr content = new FormuleContentExpr(runProvider);
    final CtuluLog log = content.setParameter(params, Collections.emptyList());
    assertFalse(log.containsErrorOrSevereError());

    final FormuleCalculatorExpr calculator = content.getCalculator();
    Double value = calculator.getValue(null, new ReportRunVariableKey(current, ReportVariableTypeEnum.FORMULE, "Br_B1"), "Br_B1", null);
    assertEquals(1000, value.intValue());
    value = calculator.getValue(null, new ReportRunVariableKey(other, ReportVariableTypeEnum.FORMULE, "Br_B1"), "Br_B1", null);
    assertEquals(10, value.intValue());

  }
}
