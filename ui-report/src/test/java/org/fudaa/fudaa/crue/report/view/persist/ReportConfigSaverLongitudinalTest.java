/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBannerConfig;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBrancheConfig;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalConfig;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.dodico.crue.projet.report.transformer.KeysToStringConverter;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.fudaa.dodico.crue.test.FileTest;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.junit.Test;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Frederic Deniger
 */
public class ReportConfigSaverLongitudinalTest extends FileTest {
  final ReportRunKey current = new ReportRunKey();

  public ReportConfigSaverLongitudinalTest() {
  }

  @Test
  public void testCreateReadWriteEmptyLongitudinal() throws IOException {
    final ReportLongitudinalConfig config = new ReportLongitudinalConfig();
    assertNotNull(config.getExternFiles());
    final File target = createTempFile("longitudinal", ".xml");
    createSaver().save(target, config, "test");
    final CrueIOResu<ReportConfigContrat> read = createReader().read(target);
    final ReportConfigContrat metier = read.getMetier();
    assertNotNull(metier);
    assertEquals(ReportLongitudinalConfig.class, metier.getClass());
    target.delete();
    final ReportLongitudinalConfig readConfig = (ReportLongitudinalConfig) metier;
    assertNotNull(readConfig.getExternFiles());
    assertNotNull(readConfig.getLoiLegendConfig());
    assertNotNull(readConfig.getLoiLegendConfig().getLabels());
    assertNotNull(readConfig.getBannerConfig());
    assertNotNull(readConfig.getProfilVariables());
    assertNotNull(readConfig.getTimes());
  }

  @Test
  public void testReadLongitudinal() {
    final File file = AbstractTestParent.getFile("/org/fudaa/fudaa/crue/report/view/persist/longitudinal.xml");
    assertTrue(file.exists());
    final ReportConfigContrat metier = read(file);
    assertEquals(ReportLongitudinalConfig.class, metier.getClass());
    file.delete();
    final ReportLongitudinalConfig config = (ReportLongitudinalConfig) metier;
    testContent(config);
  }

  @Test
  public void testWriteLongitudinal() throws IOException {
    final File file = AbstractTestParent.getFile("/org/fudaa/fudaa/crue/report/view/persist/longitudinal.xml");
    ReportConfigContrat metier = read(file);
    file.delete();
    final File out = File.createTempFile("longitudinal", ".xml");
    createSaver().save(out, metier, "test");
    metier = read(out);
    out.delete();
    testContent((ReportLongitudinalConfig) metier);
  }

  private ReportConfigSaver createSaver() {
    final KeysToStringConverter keysToString = new KeysToStringConverter(current);
    return new ReportConfigSaver(new TraceToStringConverter(), keysToString);
  }

  private ReportConfigReader createReader() {
    final KeysToStringConverter keysToString = new KeysToStringConverter(current);
    return new ReportConfigReader(new TraceToStringConverter(), keysToString);
  }

  private void testContent(final ReportLongitudinalConfig config) {
    final List<ReportRunVariableKey> profilVariables = config.getProfilVariables();
    assertEquals(1, profilVariables.size());
    assertTrue(current == profilVariables.get(0).getReportRunKey());
    assertEquals("z", profilVariables.get(0).getVariable().getVariableName());
    assertEquals(ReportVariableTypeEnum.READ, profilVariables.get(0).getVariable().getVariableType());
    final List<ReportLongitudinalBrancheConfig> branchesConfig = config.getBranchesConfig();
    assertEquals(1, branchesConfig.size());
    assertEquals(ReportLongitudinalBrancheConfig.SensParcours.AVAL_AMONT, branchesConfig.get(0).getSensParcours());
    final List<ResultatTimeKey> times = config.getTimes();
    assertEquals(2, times.size());
    assertTrue(times.get(0).isPermanent());
    assertTrue(times.get(1).isTransitoire());
    assertEquals("Calcul transitoire", times.get(1).getNomCalcul());
    final ReportLongitudinalBannerConfig bannerConfig = config.getBannerConfig();
    assertTrue(bannerConfig.isBrancheVisible());
    assertFalse(bannerConfig.isSectionVisible());
    assertEquals(new Color(192, 192, 192), bannerConfig.getSectionBox().getColorBoite());
  }

  private ReportConfigContrat read(final File file) {
    final CrueIOResu<ReportConfigContrat> read = createReader().read(file);
    final ReportConfigContrat metier = read.getMetier();
    assertNotNull(metier);
    return metier;
  }
}
