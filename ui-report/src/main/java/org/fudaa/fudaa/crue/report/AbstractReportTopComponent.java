package org.fudaa.fudaa.crue.report;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunContent;
import org.fudaa.fudaa.crue.common.AbstractTopComponent;
import org.fudaa.fudaa.crue.report.actions.ReportGlobalFindAction;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.fudaa.fudaa.crue.report.view.ReportViewsService;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * @author deniger
 */
public abstract class AbstractReportTopComponent extends AbstractTopComponent implements LookupListener {
    public static final String END_NAME_IF_MODIFIED = " *";
    protected final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
    protected boolean updating;//true si les donnees UI sont en train d'être modifiee
    protected final List<Action> editActions = new ArrayList<>();
    protected final List<JComponent> editComponent = new ArrayList<>();
    protected boolean editable;
    private Lookup.Result<ReportRunContent> resultat;
    private final PerspectiveServiceReport perspectiveReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);
    protected final ReportViewsService reportViewsService = Lookup.getDefault().lookup(ReportViewsService.class);
    protected boolean modified;
    final FocusListener editFocusListener = new FocusListener() {
        @Override
        public void focusGained(final FocusEvent e) {
            updateEditableState();
        }

        @Override
        public void focusLost(final FocusEvent e) {
        }
    };
    final PropertyChangeListener stateListener = evt -> perspectiveStateChanged();

    protected AbstractReportTopComponent() {
        setFocusable(true);
        ReportGlobalFindAction.installAction(this);
    }

    protected CrueConfigMetier getCcm() {
        return reportService.getCcm();
    }

    protected ResultatTimeKey getSelectedTime() {
        return reportService.getSelectedTime();
    }

    public void setAdditonnalLayerModified() {
        if (updating) {
            return;
        }
        perspectiveReport.setAdditionalLayersModified(true);
        this.modified = true;
        if (perspectiveReport != null) {//pour les tests uniquement
            perspectiveReport.setDirty(true);
        }
        if (buttonSave != null) {
            buttonSave.setEnabled(modified);
        }
        if (buttonCancel != null) {
            buttonCancel.setEnabled(modified);
        }
        updateTopComponentName();
    }

    protected JPanel createCancelSaveButtons() {
        return createCancelSaveButtons(null);
    }

    @Override
    public boolean valideModification() {
        return true;
    }

    @Override
    public void cancelModification() {
    }

    protected void updateTopComponentName() {
        final String name = getName();
        if (name == null) {
            return;
        }
        if (modified && !name.endsWith(END_NAME_IF_MODIFIED)) {
            setName(name + END_NAME_IF_MODIFIED);
        } else if (!modified && name.endsWith(END_NAME_IF_MODIFIED)) {
            setName(StringUtils.removeEnd(name, END_NAME_IF_MODIFIED));
        }
    }

    protected abstract void runLoaded();

    public abstract void runUnloaded();

    @Override
    public boolean isModified() {
        return modified;
    }

    public void setModified(final boolean modified) {
        if (updating) {
            return;
        }
        this.modified = modified;
        if (modified) {
            if (perspectiveReport != null) {//pour les tests uniquement
                perspectiveReport.setDirty(true);
            }
        }
        updateTopComponentName();
        if (buttonSave != null) {
            buttonSave.setEnabled(modified);
        }
        if (buttonCancel != null) {
            buttonCancel.setEnabled(modified);
        }
        if (modified) {
            reportViewsService.setTopComponentModified(this);
        }
    }

    protected EMHScenario getCurrentScenario() {
        final ReportRunContent runCourant = reportService.getRunCourant();
        return runCourant == null ? null : runCourant.getScenario();
    }

    @Override
    public final void resultChanged(final LookupEvent ev) {
        if (reportService.isRunLoaded()) {
            runLoaded();
        } else {
            cleanTopComponent();
        }
    }

    private void cleanTopComponent() {
        runUnloaded();
    }

    protected void updateEditableState() {
        setEditable(
                reportService != null && WindowManager.getDefault().getRegistry().getActivated() == this && perspectiveReport.isInEditMode());
    }

    @Override
    public final void componentClosedDefinitly() {
        if (resultat != null) {
            resultat.removeLookupListener(this);
            resultat = null;
        }
        cleanTopComponent();
        removeFocusListener(editFocusListener);
        componentClosedDefinitlyHandler();
    }

    protected void componentClosedDefinitlyHandler() {
    }

    @Override
    public void componentClosedTemporarily() {
    }

    protected void setEditable(final boolean b) {
        this.editable = b;
        for (final Action action : editActions) {
            action.setEnabled(b);
        }
        for (final JComponent jComponent : editComponent) {
            jComponent.setEnabled(b);
        }
    }

    @Override
    public boolean canClose() {
        return super.canClose();
    }

    @Override
    public final void componentOpened() {
        addFocusListener(editFocusListener);
        super.componentOpened();
        if (resultat == null) {
            resultat = reportService.getLookup().lookupResult(ReportRunContent.class);
            resultat.addLookupListener(this);
        }
        resultChanged(null);
        componentOpenedHandler();
    }

    protected void componentOpenedHandler() {
    }

    protected void perspectiveStateChanged() {
        updateEditableState();
    }

    @Override
    protected void componentDeactivated() {
        if (reportService == null) {
            return;
        }
        perspectiveReport.removeStateListener(stateListener);
        setEditable(false);

        super.componentDeactivated();
    }

    @Override
    protected void componentActivated() {
        if (reportService == null) {
            return;
        }
        perspectiveReport.addStateListener(stateListener);
        updateEditableState();
        super.componentActivated();
    }
}
