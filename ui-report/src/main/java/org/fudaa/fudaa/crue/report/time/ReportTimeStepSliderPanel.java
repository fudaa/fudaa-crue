/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.time;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInsets;
import com.memoire.bu.BuLib;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformerDefault;
import org.fudaa.fudaa.crue.report.service.ReportHelper;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportTimeStepSliderPanel extends Observable {

  JSlider slider;
  List values = Collections.emptyList();
  JPanel panel;
  JLabel lbMin;
  JLabel lbMax;
  JButton btNext;
  JButton btLast;
  JButton btFirst;
  JButton btPrevious;
  JToggleButton btPlay;
  JButton btReplay;
  ToStringTransformer toStringTransformer = ToStringTransformerDefault.TO_STRING;
  ReportTimeVideoRecorder videoRecorder = new ReportTimeVideoRecorder();

  public ReportTimeVideoRecorder getVideoRecorder() {
    return videoRecorder;
  }

  public void setVideoRecorder(ReportTimeVideoRecorder videoRecorder) {
    this.videoRecorder = videoRecorder;
  }

  public List getValues() {
    return Collections.unmodifiableList(values);
  }

  JPanel getPanel() {
    if (panel == null) {
      buildPanel();
    }
    return panel;
  }

  public void setToStringTransformer(ToStringTransformer toStringTransformer) {
    this.toStringTransformer = toStringTransformer;
    updateLabels();
  }

  private void buildPanel() {
    panel = new JPanel(new BorderLayout(2, 0));
    slider = new JSlider(0, values.size());
    slider.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        sliderChanged();
      }
    });
    slider.setPaintTicks(true);
    slider.setPaintLabels(false);
    JPanel pnMinMax = createMinMaxLabels();
    JPanel pnSlider = new JPanel(new BuGridLayout(1, 2, 0, true, false));
    pnSlider.setBorder(BorderFactory.createEmptyBorder(3, 0, 0, 0));
    pnSlider.add(slider);
    pnSlider.add(pnMinMax);
    panel.add(pnSlider);
    panel.add(createFirst(), BorderLayout.WEST);
    panel.add(createLast(), BorderLayout.EAST);
    updateComponents();
  }

  protected static JButton create(String iconName) {
    JButton bt = new JButton();
    bt.setMargin(BuInsets.INSETS1111);
    setIcon(bt, iconName);
    return bt;
  }

  private JToggleButton createToggle(String iconName) {
    JToggleButton bt = new JToggleButton();
    bt.setMargin(BuInsets.INSETS1111);
    setIcon(bt, iconName);
    return bt;
  }

  protected void previous() {
    setValue(slider.getValue() - 1);
  }

  public void setValue(int idx) {
    if (idx >= 0 && idx < values.size()) {
      slider.setValue(idx);
    }

  }
  Timer playTimer;

  private void createTimer() {
    playTimer = new Timer(playDelay, new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        playNext();
      }
    });
  }

  protected void playNext() {
    boolean ok = next();
    if (!ok) {
      playTimer.stop();
      btPlay.setSelected(false);
      if (videoRecorder != null) {
        videoRecorder.end();
      }
    } else if (videoRecorder != null) {
      videoRecorder.nextDone();
    }
  }
  protected int playDelay = 1000;

  public double getPlayDelayInSec() {
    return playDelay / 1000d;
  }

  public void setPlayDelayInSec(double newPlayDelay) {
    playDelay = (int) (newPlayDelay * 1000d);
  }

  protected void replay() {
    first();
    if (!btPlay.isSelected()) {
      btPlay.doClick();
    }
  }

  protected void play() {
    stopPlay();
    if (btPlay.isSelected()) {
      if (videoRecorder.isVideoRecorded()) {
        if (!videoRecorder.checkFiles()) {
          return;
        }
      }
      createTimer();
      setIcon(btPlay, "pause.png");
      playTimer.setRepeats(true);
      playTimer.setDelay(playDelay);
      if (videoRecorder != null) {
        videoRecorder.start(getNbValues() - getSelectedValue());
      }
      playTimer.start();
    }
  }

  protected void first() {
    setValue(0);
  }

  protected boolean next() {
    if (isNextAvailable()) {
      setValue(slider.getValue() + 1);
      return true;
    }
    return false;
  }

  protected void last() {
    setValue(values.size() - 1);
  }

  public void setValues(List values, boolean reselectLastSelection) {
    stopPlay();
    updating = true;
    Object current = getSelectedItem();
    this.values = new ArrayList(values);
    if (panel != null) {
      updateComponents();
    }
    if (!values.isEmpty()) {
      int idx = 0;
      if (current != null) {
        idx = values.indexOf(current);
      }
      slider.setValue(idx);
    }
    updating = false;
    sliderChanged();
  }
  boolean updating;

  protected void sliderChanged() {
    if (updating) {
      return;
    }
    setChanged();
    notifyObservers(getSelectedItem());
    updateButtonState();
  }

  public int getSelectedValue() {
    return slider.getValue();
  }

  public Object getSelectedItem() {
    if (values.isEmpty()) {
      return null;
    }
    return values.get(slider.getValue());
  }

  protected void updateButtonState() {
    int value = slider.getValue();
    btFirst.setEnabled(editable && value > 0);
    btPrevious.setEnabled(editable && value > 0);
    boolean enable = isNextAvailable();
    btNext.setEnabled(editable && enable);
    btLast.setEnabled(editable && enable);
    btPlay.setEnabled(editable && enable);
    if (editable && !enable) {
      setIcon(btPlay, "play.png");
    }
    btReplay.setEnabled(editable);
  }

  protected boolean isNextAvailable() {
    int value = slider.getValue();
    return value < values.size() - 1;

  }

  protected Object getMinObject() {
    return values.isEmpty() ? null : values.get(0);
  }

  protected Object getMaxObject() {
    return values.isEmpty() ? null : values.get(values.size() - 1);
  }

  public int getNbValues() {
    return values.size();
  }

  protected String toString(Object o) {
    if (o == null) {
      return StringUtils.EMPTY;
    }
    return toStringTransformer.transform(o);
  }

  private void updateComponents() {
    slider.setModel(new DefaultBoundedRangeModel(0, 0, 0, Math.max(0, values.size() - 1)));
    updateLabels();
    updateButtonState();

  }

  private JPanel createFirst() {
    JPanel pnFirst = new JPanel(new BuGridLayout(1, 1, 2, false, false, false, false));
    btPrevious = create("previous.png");
    btPrevious.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        previous();
      }
    });
    btFirst = create("first.png");
    btFirst.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        first();
      }
    });
    pnFirst.add(btPrevious);
    pnFirst.add(btFirst);
    return pnFirst;
  }

  private JPanel createLast() {
    JPanel pnLast = new JPanel(new BuGridLayout(3, 2, 2, false, false));
    btNext = create("next.png");
    btNext.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        next();
      }
    });
    btPlay = createToggle("play.png");
    btPlay.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        play();
      }
    });
    btReplay = create("replay.png");
    btReplay.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        replay();
      }
    });
    btPlay.setToolTipText(NbBundle.getMessage(ReportTimeStepSliderPanel.class, "play.tooltip"));
    btReplay.setToolTipText(NbBundle.getMessage(ReportTimeStepSliderPanel.class, "replay.tooltip"));
    btLast = create("last.png");
    btLast.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        last();
      }
    });
    pnLast.add(btNext);
    pnLast.add(btPlay);
    pnLast.add(btReplay);
    pnLast.add(btLast);
    return pnLast;
  }

  private JPanel createMinMaxLabels() {
    lbMin = new JLabel();
    lbMin.setFont(BuLib.deriveFont(lbMin.getFont(), -1));
    lbMax = new JLabel();
    lbMax.setFont(BuLib.deriveFont(lbMax.getFont(), -1));
    JPanel pnMinMax = new JPanel(new BorderLayout());
    pnMinMax.setBorder(BorderFactory.createEmptyBorder(0, 1, 0, 3));
    pnMinMax.add(lbMin, BorderLayout.NORTH);
    pnMinMax.add(lbMax, BorderLayout.SOUTH);
    lbMin.setHorizontalTextPosition(SwingConstants.LEFT);
    lbMax.setHorizontalTextPosition(SwingConstants.RIGHT);
    lbMax.setHorizontalAlignment(SwingConstants.RIGHT);
    return pnMinMax;
  }

  protected static void setIcon(AbstractButton bt, String iconName) {
    if (iconName != null) {
      bt.setIcon(ReportHelper.getIcon(iconName));
    }
  }

  private void updateLabels() {
    lbMax.setText(toString(getMaxObject()));
    lbMax.setToolTipText(lbMax.getToolTipText());
    lbMin.setText(toString(getMinObject()));
    lbMin.setToolTipText(lbMin.getToolTipText());
  }

  private void stopPlay() {
    if (playTimer != null) {
      playTimer.stop();
      if (videoRecorder != null) {
        videoRecorder.end();
      }
      playTimer = null;
    }
    setIcon(btPlay, "play.png");
  }
  boolean editable = false;

  void setEditable(boolean b) {
    editable = b;
    slider.setEnabled(b);
    updateButtonState();
  }
}
