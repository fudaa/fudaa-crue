/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.longitudinal;

import com.jidesoft.swing.JideTitledBorder;
import com.memoire.bu.BuGridLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Arrays;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBannerConfig;
import org.fudaa.ebli.controle.BSelecteurColorChooserBt;
import org.fudaa.ebli.controle.BSelecteurFont;
import org.fudaa.ebli.controle.BSelecteurLineModel;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.report.config.DefaultSelecteurTarget;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLongitudinalBannerConfigUI {

  public ReportLongitudinalBannerConfigUI() {
  }

  public boolean configure(ReportLongitudinalBannerConfig config) {
    JPanel pnSection = new JPanel(new BuGridLayout(2, 5, 5, true, true, false, false));
    JideTitledBorder border = new JideTitledBorder(NbBundle.getMessage(ReportLongitudinalBannerConfigUI.class, "SectionTitle.DisplayName"));
    pnSection.setBorder(border);
    pnSection.add(new JLabel(NbBundle.getMessage(ReportLongitudinalBannerConfigUI.class, "SectionVisible.DisplayName")));
    JCheckBox cbSectionVisible = new JCheckBox();
    cbSectionVisible.setSelected(config.isSectionVisible());
    pnSection.add(cbSectionVisible);
    pnSection.add(new JLabel(NbBundle.getMessage(ReportLongitudinalBannerConfigUI.class, "OnlySectionAmontAvalVisible.DisplayName")));
    JCheckBox cbOnlySectionAmontAvalVisible = new JCheckBox();
    cbOnlySectionAmontAvalVisible.setSelected(config.isSectionAmontAvalOnly());
    pnSection.add(cbOnlySectionAmontAvalVisible);

    //font
    pnSection.add(new JLabel(NbBundle.getMessage(ReportLongitudinalBannerConfigUI.class, "FontSection.DisplayName")));
    BSelecteurFont sectionFont = new BSelecteurFont("SECTION_FONT");
    DefaultSelecteurTarget<Font> sectionFontTarget = new DefaultSelecteurTarget<>(config.getSectionFont());
    sectionFont.setSelecteurTarget(sectionFontTarget);
    pnSection.add(sectionFont.getButton());

    //couleur
    pnSection.add(new JLabel(NbBundle.getMessage(ReportLongitudinalBannerConfigUI.class, "FontColorSection.DisplayName")));
    DefaultSelecteurTarget<Color> sectionFontColorTarget = new DefaultSelecteurTarget<>(config.getSectionFontColor());
    BSelecteurColorChooserBt sectionFontColor = new BSelecteurColorChooserBt("COLOR_SECTION");
    sectionFontColor.setSelecteurTarget(sectionFontColorTarget);
    pnSection.add(sectionFontColor.getPanel());

    //ligne
    pnSection.add(new JLabel(NbBundle.getMessage(ReportLongitudinalBannerConfigUI.class, "LineSection.DisplayName")));
    BSelecteurLineModel sectionLine = new BSelecteurLineModel(config.getSectionBoxLine());
    DefaultSelecteurTarget<TraceLigneModel> sectionLineTarget = new DefaultSelecteurTarget<>(config.getSectionBoxLine());
    sectionLine.setSelecteurTarget(sectionLineTarget);
    pnSection.add(sectionLine.buildPanel());

    //branches
    JPanel pnBranche = new JPanel(new BuGridLayout(2, 5, 5, true, true, false, false));
    pnBranche.setBorder(new JideTitledBorder(NbBundle.getMessage(ReportLongitudinalBannerConfigUI.class, "BrancheTitle.DisplayName")));
    pnBranche.add(new JLabel(NbBundle.getMessage(ReportLongitudinalBannerConfigUI.class, "BrancheVisible.DisplayName")));
    JCheckBox cbBrancheVisible = new JCheckBox();
    cbBrancheVisible.setSelected(config.isBrancheVisible());
    pnBranche.add(cbBrancheVisible);
    pnBranche.add(new JLabel(NbBundle.getMessage(ReportLongitudinalBannerConfigUI.class, "FontBranche.DisplayName")));
    BSelecteurFont brancheFont = new BSelecteurFont("BRANCHE_FONT");
    DefaultSelecteurTarget<Font> brancheFontTarget = new DefaultSelecteurTarget<>(config.getBrancheFont());
    brancheFont.setSelecteurTarget(brancheFontTarget);
    pnBranche.add(brancheFont.getButton());

    //couleur
    pnBranche.add(new JLabel(NbBundle.getMessage(ReportLongitudinalBannerConfigUI.class, "FontColorBranche.DisplayName")));
    DefaultSelecteurTarget<Color> brancheFontColorTarget = new DefaultSelecteurTarget<>(config.getBrancheFontColor());
    BSelecteurColorChooserBt brancheFontColor = new BSelecteurColorChooserBt("COLOR_BRANCHE");
    brancheFontColor.setSelecteurTarget(brancheFontColorTarget);
    pnBranche.add(brancheFontColor.getPanel());

    //ligne
    pnBranche.add(new JLabel(NbBundle.getMessage(ReportLongitudinalBannerConfigUI.class, "LineBranche.DisplayName")));
    BSelecteurLineModel brancheLine = new BSelecteurLineModel(config.getBrancheBoxLine());
    DefaultSelecteurTarget<TraceLigneModel> brancheLineTarget = new DefaultSelecteurTarget<>(config.getBrancheBoxLine());
    brancheLine.setSelecteurTarget(brancheLineTarget);
    pnBranche.add(brancheLine.buildPanel());

    JPanel pn = new JPanel(new BuGridLayout(1, 0, 10));
    pn.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    pn.add(pnSection);
    pn.add(pnBranche);
    List<JLabel> selectClassSection = EMHHelper.selectClass(Arrays.asList(pnSection.getComponents()), JLabel.class);
    List<JLabel> selectLabels = EMHHelper.selectClass(Arrays.asList(pnBranche.getComponents()), JLabel.class);
    selectLabels.addAll(selectClassSection);
    int maxW = 0;
    for (JLabel jLabel : selectLabels) {
      maxW = Math.max(maxW, jLabel.getPreferredSize().width);
    }
    for (JLabel jLabel : selectLabels) {
      Dimension preferredSize = jLabel.getPreferredSize();
      preferredSize.width = maxW;
      jLabel.setPreferredSize(preferredSize);
    }
    SysdocUrlBuilder.installHelpShortcut(pn, SysdocUrlBuilder.getDialogHelpCtxId("configurationCartouche", PerspectiveEnum.REPORT));
    boolean ok = DialogHelper.showQuestionAndSaveDialogConf(NbBundle.getMessage(ReportLongitudinalBannerConfigUI.class,
            "BannerConfiguration.DialogTitle"), pn, getClass(), DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    if (ok) {
      config.setSectionVisible(cbSectionVisible.isSelected());
      config.setSectionAmontAvalOnly(cbOnlySectionAmontAvalVisible.isSelected());
      config.setSectionFont(sectionFontTarget.getObject());
      config.setSectionFontColor(sectionFontColorTarget.getObject());
      config.setSectionBoxLine(sectionLine.getNewData());
      config.setBrancheVisible(cbBrancheVisible.isSelected());
      config.setBrancheFont(brancheFontTarget.getObject());
      config.setBrancheFontColor(brancheFontColorTarget.getObject());
      config.setBrancheBoxLine(brancheLine.getNewData());
    }
    return ok;
  }
}
