/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.config;

import java.util.List;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.fudaa.crue.loi.CourbeViewConfigurationTarget;

/**
 *
 * @author Frederic Deniger
 */
public interface CourbeViewReportConfigurationTarget extends CourbeViewConfigurationTarget {

  List<ReportVariableKey> getTitleAvailableVariables();
}
