/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.projet.report.IndexDao;
import org.fudaa.dodico.crue.projet.report.ReportIndexReader;
import org.fudaa.dodico.crue.projet.report.ReportViewLineInfo;
import org.fudaa.dodico.crue.projet.report.ReportXstreamReaderWriter;
import org.fudaa.fudaa.crue.report.view.ReportViewLine;

/**
 *
 * @author Frederic Deniger
 */
public class ReportIndexReaderSaverUI {

  public static void setLines(IndexDao target, List<ReportViewLine> lines) {
    if (lines == null) {
      target.setInfoLines(new ArrayList<>());
    }
    for (ReportViewLine reportViewLine : lines) {
      target.getLines().add(reportViewLine.getLineInformation());
    }
  }

  public CtuluLog save(File targetFile, List<ReportViewLine> allLines, String type) {
    IndexDao dao = new IndexDao();
    dao.setType(type);
    setLines(dao, allLines);
    ReportIndexReader indexReader = new ReportIndexReader();
    ReportXstreamReaderWriter<IndexDao> readerWriter = new ReportXstreamReaderWriter<>("report-index", ReportXstreamReaderWriter.VERSION,
            indexReader);
    CrueIOResu<IndexDao> res = new CrueIOResu<>(dao);
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc(org.openide.util.NbBundle.getMessage(ReportIndexReaderSaverUI.class, "SaveIndex.LogDisplayName"));
    readerWriter.writeXMLMetier(res, targetFile, log, null);
    return log;
  }

  public CrueIOResu<List<ReportViewLineInfo>> read(File targetFile) {
    return new ReportIndexReader().read(targetFile);
  }

}
