/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.ConnexionInformationFixed;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.common.FileIntegrityManager;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.report.ReportIndexReader;
import org.fudaa.dodico.crue.projet.report.transformer.KeysToStringConverter;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.report.helper.UniqueFileCreator;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.fudaa.fudaa.crue.report.view.ReportViewLine;
import org.fudaa.fudaa.crue.report.view.ReportViewsByType;
import org.fudaa.fudaa.crue.report.view.ReportViewsService;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportConfigSaverProcessor implements ProgressRunnable<CtuluLogGroup> {

  private final ReportViewsService reportViewsService = Lookup.getDefault().lookup(ReportViewsService.class);
  private final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
  private final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

  @Override
  public CtuluLogGroup run(ProgressHandle handle) {
    if (handle != null) {
      handle.switchToIndeterminate();
    }
    List<ReportViewsByType> viewTypes = reportViewsService.getConfigViews();
    ConnexionInformationFixed ci = new ConnexionInformationFixed(configurationManagerService.getConnexionInformation());
    EMHProjet projectLoaded = reportViewsService.getProjectLoaded();
    File dirOfRapports = projectLoaded.getInfos().getDirOfRapports();
    KeysToStringConverter keysToString = new KeysToStringConverter(reportService.getCurrentRunKey());
    ReportConfigSaver configSaver = new ReportConfigSaver(new TraceToStringConverter(), keysToString);
    List<SaveCallableContrat> allCallable = new ArrayList<>();
    for (ReportViewsByType node : viewTypes) {
      List<SaveCallableContrat> createCallables = createCallables(node, ci, dirOfRapports, configSaver);
      allCallable.addAll(createCallables);
    }
    List<File> filesToSave = new ArrayList<>();

    for (SaveCallableContrat saveCallableContrat : allCallable) {
      filesToSave.add(saveCallableContrat.getTargetFile());
    }
    FileIntegrityManager backupManager = new FileIntegrityManager(filesToSave);
    //on fait un backup des fichiers
    CtuluLogGroup logs = new CtuluLogGroup(null);
    logs.setDescription(NbBundle.getMessage(ReportConfigSaverProcessor.class, "SaveFiles.LogDisplayName"));
    backupManager.start();
    ExecutorService newCachedThreadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    try {
      List<Future<CtuluLog>> invokeAll = newCachedThreadPool.invokeAll(allCallable);
      for (Future<CtuluLog> future : invokeAll) {
        CtuluLog log = future.get();
        if (log != null) {
          logs.addLog(log);
        }
      }
    } catch (Exception ex) {
      Exceptions.printStackTrace(ex);
    } finally {
      newCachedThreadPool.shutdownNow();
    }
    backupManager.finish(!logs.containsFatalError());
    //on supprime les fichiers inutile
    if (!logs.containsFatalError()) {
      for (ReportViewsByType viewType : viewTypes) {
        viewType.saveDone();
        List<ReportViewLine> allLines = getAllLines(viewType.getNode());
        File targetDir = getFolder(viewType, dirOfRapports);
        Set<File> expectedFiles = new HashSet<>();
        expectedFiles.add(new File(targetDir, ReportIndexReader.INDEX_FILENAME));
        for (ReportViewLine reportViewLine : allLines) {
          expectedFiles.add(new File(targetDir, reportViewLine.getLineInformation().getFilename()));
        }
        if (targetDir.exists()) {
          File[] listFiles = targetDir.listFiles();
          if (listFiles != null) {
            for (File file : listFiles) {
              if (!expectedFiles.contains(file)) {
                CrueFileHelper.deleteFile(file,getClass());
              }
            }
          }
        }
      }
    }
    if (handle != null) {
      handle.finish();
    }
    return logs;
  }

  private List<SaveCallableContrat> createCallables(ReportViewsByType viewType, ConnexionInformationFixed ci, File dirOfRapports, ReportConfigSaver configSaver) {
    List<SaveCallableContrat> res = new ArrayList<>();
    List<ReportViewLine> modifiedLines = getModifiedLines(viewType.getNode());
    //on met à jour les informations de modifications:
    for (ReportViewLine reportViewLine : modifiedLines) {
      reportViewLine.getLineInformation().updateEdited(ci);
    }
    //rien a faire:
    if (!viewType.isModified() && modifiedLines.isEmpty()) {
      return res;
    }
    File targetDir = getFolder(viewType, dirOfRapports);
    boolean mkdirs=targetDir.mkdirs();
    if(!mkdirs){
      Logger.getLogger(getClass().getName()).warning("can't create dir "+targetDir.getAbsolutePath());
    }
    for (ReportViewLine reportViewLine : modifiedLines) {
      reportViewLine.getLineInformation().updateEdited(ci);
    }
    UniqueFileCreator fileNameCreator = new UniqueFileCreator(targetDir);
    for (ReportViewLine reportViewLine : modifiedLines) {
      String filename = reportViewLine.getLineInformation().getFilename();
      if (filename == null) {
        filename = fileNameCreator.createNewFilename();
        reportViewLine.getLineInformation().setFilename(filename);
      }
    }
    List<ReportViewLine> allLines = getAllLines(viewType.getNode());
    File targetIndexFile = new File(targetDir, ReportIndexReader.INDEX_FILENAME);
    String type = viewType.getType().name();
    res.add(new IndexSaverCallable(allLines, targetIndexFile, type));

    for (ReportViewLine reportViewLine : modifiedLines) {
      if (reportViewLine.getReportConfig() != null) {
        //faire des clean des configs:
        File targetConfigFile = new File(targetDir, reportViewLine.getLineInformation().getFilename());
        res.add(new ReportConfigSaverCallable(configSaver,  reportViewLine.getReportConfig(), targetConfigFile, reportViewLine.getLineInformation().getNom()));
      }
    }
    return res;
  }

  private List<ReportViewLine> getModifiedLines(Node mainNode) {
    Node[] nodes = mainNode.getChildren().getNodes();
    List<ReportViewLine> modifiedLines = new ArrayList<>();
    for (Node node : nodes) {
      ReportViewLine line = node.getLookup().lookup(ReportViewLine.class);
      if (line.isModified()) {
        modifiedLines.add(line);
      }
    }
    return modifiedLines;
  }

  private List<ReportViewLine> getAllLines(Node mainNode) {
    Node[] nodes = mainNode.getChildren().getNodes();
    List<ReportViewLine> allLines = new ArrayList<>();
    for (Node node : nodes) {
      ReportViewLine line = node.getLookup().lookup(ReportViewLine.class);
      allLines.add(line);
    }
    return allLines;
  }

  private File getFolder(ReportViewsByType viewType, File dirOfRapports) {
    return viewType.getFolder(dirOfRapports);
  }
}
