/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.service;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.common.time.TimeFormatOption;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

/**
 *
 * @author Frederic Deniger
 */
public class ReportTimeListenerManager {

  final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
  final List<ReportTimeListener> listeners = new ArrayList<>();

  public void addListener(ReportTimeListener listener) {
    boolean createListener = listeners.isEmpty();
    listeners.add(listener);
    if (createListener) {
      createListenerToService();
    }
  }
  Result<ResultatTimeKey> resultatKeyResult;
  Result<RangeSelectedTimeKey> rangeKeyResult;
  Result<TimeFormatOption> timeFormatResult;
  final LookupListener resultatKeyListener = new LookupListener() {
    @Override
    public void resultChanged(LookupEvent ev) {
      selectedTimeStepChanged();
    }
  };
  final LookupListener rangeKeyListener = new LookupListener() {
    @Override
    public void resultChanged(LookupEvent ev) {
      rangeTimeStepChanged();
    }
  };
  final LookupListener timeFormatListener = new LookupListener() {
    @Override
    public void resultChanged(LookupEvent ev) {
      timeFormatChanged();
    }
  };

  protected void selectedTimeStepChanged() {
    if (reportService.isRunLoaded() && reportService.getSelectedTime() != null) {
      for (ReportTimeListener reportTimeListener : listeners) {
        reportTimeListener.selectedTimeChanged();
      }
    }
  }

  protected void timeFormatChanged() {
    if (reportService.isRunLoaded() && reportService.getTimeFormat() != null) {
      for (ReportTimeListener reportTimeListener : listeners) {
        reportTimeListener.timeFormatChanged();
      }
    }
  }

  protected void rangeTimeStepChanged() {
    if (reportService.isRunLoaded() && reportService.getTimeFormat() != null) {
      for (ReportTimeListener reportTimeListener : listeners) {
        reportTimeListener.rangeChanged();
      }
    }
  }

  private void createListenerToService() {
    resultatKeyResult = reportService.getLookup().lookupResult(ResultatTimeKey.class);
    resultatKeyResult.addLookupListener(resultatKeyListener);
    rangeKeyResult = reportService.getLookup().lookupResult(RangeSelectedTimeKey.class);
    rangeKeyResult.addLookupListener(rangeKeyListener);
    timeFormatResult = reportService.getLookup().lookupResult(TimeFormatOption.class);
    timeFormatResult.addLookupListener(timeFormatListener);
  }

  private void clean() {
    if (resultatKeyResult != null) {
      resultatKeyResult.removeLookupListener(resultatKeyListener);
    }
    if (rangeKeyResult != null) {
      rangeKeyResult.removeLookupListener(rangeKeyListener);
    }
    if (timeFormatResult != null) {
      timeFormatResult.removeLookupListener(timeFormatListener);
    }
    resultatKeyResult = null;
    timeFormatResult = null;
  }

  public void removeListener(ReportTimeListener listener) {
    listeners.remove(listener);
    if (listeners.isEmpty()) {
      clean();
    }
  }

  public void removeAllListeners() {
    listeners.clear();
    clean();
  }
}
