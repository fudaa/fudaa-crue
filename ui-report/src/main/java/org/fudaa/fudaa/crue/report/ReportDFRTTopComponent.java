package org.fudaa.fudaa.crue.report;

import com.memoire.bu.BuGridLayout;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.*;
import javax.swing.*;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.ExportTableCommentSupplier;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.projet.report.data.ReportRunContent;
import org.fudaa.fudaa.crue.common.AbstractTopComponent;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.loi.common.CourbeMoyenneDeltaController;
import org.fudaa.fudaa.crue.loi.loiff.LoiUiController;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.fudaa.fudaa.crue.study.services.TableExportCommentSupplier;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Editeur de lois.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.report//ReportDFRTTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = ReportDFRTTopComponent.TOPCOMPONENT_ID, persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ReportDFRTTopComponent.MODE, openAtStartup = false)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.report.ReportDFRTTopComponent")
public final class ReportDFRTTopComponent extends AbstractTopComponent implements ItemListener, ExportTableCommentSupplier, LookupListener {

  public static final String MODE = "loi-dfrtEditorReport"; //NOI18N
  public static final String TOPCOMPONENT_ID = "ReportDFRTTopComponent"; //NOI18N
  private final transient LoiUiController loiUiController;
  private final JTextField txtCommentaire;
  private final JTextField txtNom;
  private final String initName;
  protected final transient ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
  private Lookup.Result<ReportRunContent> resultat;
  private transient DonFrt defaultSelected;

  public void setDefaultSectionDonFrt(final DonFrt frt) {
    if (isOpened()) {
      cbDonFrt.setSelectedItem(frt);
    } else {
      defaultSelected = frt;
    }
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueVisualisationFrottements", PerspectiveEnum.REPORT);
  }

  public ReportDFRTTopComponent() {
    initComponents();
    initName = NbBundle.getMessage(ReportDFRTTopComponent.class, "CTL_" + TOPCOMPONENT_ID);
    setName(initName);
    setToolTipText(NbBundle.getMessage(ReportDFRTTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    loiUiController = new LoiUiController();
    loiUiController.addExportImagesToToolbar();
    add(loiUiController.getToolbar(), BorderLayout.NORTH);
    loiUiController.getPanel().setPreferredSize(new Dimension(550, 350));
    final JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, loiUiController.getPanel(), loiUiController.getTableGraphePanel());
    splitPane.setDividerLocation(550);
    splitPane.setContinuousLayout(true);
    splitPane.setOneTouchExpandable(true);
    add(splitPane);

    final JPanel nomComment = new JPanel(new BuGridLayout(2, 10, 10));
    nomComment.add(new JLabel(NbBundle.getMessage(ReportDFRTTopComponent.class, "Nom.DisplayName")));
    txtNom = new JTextField(30);
    nomComment.add(txtNom);
    nomComment.add(new JLabel(NbBundle.getMessage(ReportDFRTTopComponent.class, "Commentaire.DisplayName")));
    txtCommentaire = new JTextField(30);
    txtCommentaire.setEditable(false);
    nomComment.add(txtCommentaire);
    final JPanel pnInfo = new JPanel(new BorderLayout(5, 5));
    pnInfo.add(loiUiController.getInfoController().getPanel());
    pnInfo.add(nomComment, BorderLayout.NORTH);
    cbDonFrt = new JComboBox();
    cbDonFrt.addItemListener(this);
    cbDonFrt.setRenderer(new ObjetNommeCellRenderer());
    final JPanel pnActions = new JPanel(new BuGridLayout(1, 5, 5));
    pnActions.add(cbDonFrt);
    pnActions.add(new JSeparator());
    final JPanel pnSouth = new JPanel(new BorderLayout(10, 10));
    pnSouth.add(pnActions, BorderLayout.WEST);
    pnSouth.setBorder(BorderFactory.createEtchedBorder());
    pnSouth.add(pnInfo);
    pnSouth.add(new CourbeMoyenneDeltaController(loiUiController.getGraphe()).getPnInfo(), BorderLayout.EAST);
    add(pnSouth, BorderLayout.SOUTH);
    loiUiController.setEditable(false);
    txtCommentaire.setEditable(false);
    txtNom.setEditable(false);
  }

  @Override
  public final void componentClosedDefinitly() {
    if (resultat != null) {
      resultat.removeLookupListener(this);
      resultat = null;
    }
  }

  @Override
  public void componentClosedTemporarily() {
    //nothing to do
  }

  @Override
  public final void componentOpened() {
    super.componentOpened();
    if (resultat == null) {
      resultat = reportService.getLookup().lookupResult(ReportRunContent.class);
      resultat.addLookupListener(this);
    }
    resultChanged(null);
    if (defaultSelected != null) {
      cbDonFrt.setSelectedItem(defaultSelected);
      defaultSelected = null;
    }
  }

  @Override
  public List<String> getComments() {
    return new TableExportCommentSupplier(false).getComments(getName());
  }

  @Override
  public final void resultChanged(final LookupEvent ev) {
    if (reportService.isRunLoaded()) {
      runLoaded();
    } else {
      runUnloaded();
    }
  }

  @Override
  public void itemStateChanged(final ItemEvent e) {
    if (e.getStateChange() == ItemEvent.SELECTED) {
      updateWithDonFrt();
    }
  }

  protected void runLoaded() {
    final EMHScenario scenario = reportService.getRunCourant().getScenario();
    final List<DonFrt> lois = EMHHelper.getAllDonFrt(scenario);
    lois.sort(ObjetNommeByNameComparator.INSTANCE);
    cbDonFrt.setModel(new DefaultComboBoxModel(lois.toArray(new DonFrt[0])));
    cbDonFrt.setSelectedItem(null);

    this.loiUiController.getGraphe().setExportTableCommentSupplier(this);

  }

  private void updateWithDonFrt() {
    final DonFrt donFrt = getSelectedDonFrt();
    txtCommentaire.setText(StringUtils.EMPTY);
    txtNom.setText(StringUtils.EMPTY);
    if (donFrt != null) {
      txtCommentaire.setText(donFrt.getLoi().getCommentaire());
      final String desc = StringUtils.defaultString(donFrt.getNom());
      txtNom.setText(desc);
      setName(initName + BusinessMessages.ENTITY_SEPARATOR + desc);
      loiUiController.setLoi(donFrt.getLoi(), reportService.getCcm(), false);
    } else {
      setName(initName);
    }

    loiUiController.setEditable(false);
  }

  protected void runUnloaded() {
    loiUiController.setLoi(null, null, false);
    cbDonFrt.setModel(new DefaultComboBoxModel());
    close();
  }
  private final JComboBox cbDonFrt;

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  void writeProperties(final java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
  }

  void readProperties(final java.util.Properties p) {
  }


  private DonFrt getSelectedDonFrt() {
    return (DonFrt) cbDonFrt.getSelectedItem();
  }
}
