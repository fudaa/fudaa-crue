/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.config;

import gnu.trove.TIntHashSet;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.palette.BPalettePlageAbstract;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTColor;

/**
 *
 * @author Frederic Deniger
 */
public class CourbeColorFinder {

  public static final Color NONE = new Color(255, 255, 255, 0);
  public static long colorIndexeslastTimeModified = -1;
  private static List<Color> siteColors;
  final InstallationService installationService = Lookup.getDefault().lookup(InstallationService.class);

  private List<Color> getSiteColors() {
    File siteDir = installationService.getSiteDir();
    if (siteDir == null) {
      return Collections.emptyList();
    }
    File colors = new File(siteDir, "default-colors.xlsx");
    if (colors.isFile()) {
      if (siteColors != null && colors.lastModified() <= colorIndexeslastTimeModified) {
        return siteColors;
      }
      colorIndexeslastTimeModified = colors.lastModified();
      siteColors = loadColors(colors);
      return siteColors;
    }
    return Collections.emptyList();
  }

  public static TIntHashSet createUsedColor(Collection<EGCourbePersist> courbeConfigs) {
    TIntHashSet res = new TIntHashSet();
    if (courbeConfigs != null) {
      for (EGCourbePersist config : courbeConfigs) {
        Color couleur = config.getLineModel_().getCouleur();
        if (couleur != null) {
          res.add(couleur.getRGB());
        }

      }
    }
    return res;
  }

  public void findNewColor(EGGraphe graphe) {
    findNewColor(graphe, null);
  }

  public void findNewColor(EGGraphe graphe, TIntHashSet initUsedColor) {

    TIntHashSet usedColors = initUsedColor;
    if (usedColors == null) {
      usedColors = new TIntHashSet();
    }
    List<EGCourbe> toModify = new ArrayList<>();
    if (graphe != null) {
      EGCourbe[] courbes = graphe.getModel().getCourbes();
      for (EGCourbe courbe : courbes) {
        Color aspectContour = courbe.getLigneModel().getCouleur();
        if (NONE == aspectContour) {
          toModify.add(courbe);
        } else if (aspectContour != null) {
          usedColors.add(aspectContour.getRGB());
        }
      }
    }
    for (EGCourbe courbe : toModify) {
      Color c = findInDefault(usedColors);
      if (c == null) {
        c = Color.BLACK;
      }
      usedColors.add(c.getRGB());
      courbe.setAspectContour(c);
    }
  }

  private Color findInDefault(TIntHashSet used) {
    for (Color siteColor : getSiteColors()) {
      if (siteColor != null && !used.contains(siteColor.getRGB())) {
        return siteColor;
      }
    }
    double dx = 0.1;
    double v = 0;
    //200 essais:
    for (int i = 0; i < 200; i++) {
      if (v >= 1d) {
        v = Math.random();
      }
      Color color = BPalettePlageAbstract.getCouleur(Color.red, Color.magenta, v);
      if (!used.contains(color.getRGB())) {
        return color;
      }
      v = v + dx;
    }
    return null;
  }

  private List<Color> loadColors(File file) {
    List<Color> colors = new ArrayList<>();
    FileInputStream input = null;
    Map<Integer, HSSFColor> indexedColors = HSSFColor.getIndexHash();
    try {
      input = new FileInputStream(file);

      XSSFWorkbook workbook = new XSSFWorkbook(input);
      XSSFSheet sheet = workbook.getSheet("Curves");
      if (sheet == null) {
        return colors;
      }
      for (Iterator<Row> it = sheet.rowIterator(); it.hasNext();) {
        Row row = it.next();
        if (row.getLastCellNum() > 0) {
          Cell cell = row.getCell(0);
          if (cell != null) {
            final XSSFCellStyle cellStyle = (XSSFCellStyle) cell.getCellStyle();
            XSSFColor xssfColor = cellStyle.getFillForegroundXSSFColor();
            Color res = null;
            if (xssfColor != null && xssfColor.getCTColor() != null) {
              CTColor ctColor = xssfColor.getCTColor();
              if (ctColor.isSetIndexed()) {
                HSSFColor hssfColor = indexedColors.get(Integer.valueOf(xssfColor.getIndexed()));
                if (hssfColor != null) {
                  res = createColor(hssfColor.getTriplet());
                }

              } else if (ctColor.isSetTheme()) {
                xssfColor = workbook.getTheme().getThemeColor(xssfColor.getTheme());
                if (xssfColor != null) {
                  res = createColor(xssfColor.getARGBHex());
                }
              } else {
                res = createColor(xssfColor.getARGBHex());
              }
              if (res != null) {
                colors.add(res);
              }
            }
          }
        }
      }

    } catch (Exception ex) {
      Exceptions.printStackTrace(ex);
    } finally {
      CtuluLibFile.close(input);
    }
    return colors;
  }

  private static Color createColor(short[] rgb) {
    if (rgb == null) {
      return null;
    }
    return new Color(rgb[0], rgb[1], rgb[2]);
  }

  private static Color createColor(String argex) {
    String substring = StringUtils.substring(argex, 2);
    try {
      return Color.decode("#" + substring);
    } catch (NumberFormatException numberFormatException) {
    }
    return null;
  }
}
