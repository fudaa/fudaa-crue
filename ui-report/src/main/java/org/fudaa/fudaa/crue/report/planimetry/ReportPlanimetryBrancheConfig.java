/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.planimetry;

import java.awt.Color;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryBrancheExtraData;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconFleche;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.planimetry.configuration.BrancheConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.BrancheConfigurationExtra;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryBrancheModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLigneBriseeLayerModel;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;

/**
 *
 * @author Frederic Deniger
 */
public class ReportPlanimetryBrancheConfig extends AbstractConfigurationExtra<ReportPlanimetryBrancheExtraData> implements BrancheConfigurationExtra {

  public ReportPlanimetryBrancheConfig(ReportResultProviderService reportResultProviderService) {
    super(reportResultProviderService);
    data = new ReportPlanimetryBrancheExtraData();
  }

  @Override
  public Color getColor(CatEMHBranche branche, PlanimetryBrancheModel model, BrancheConfiguration brancheConfiguration) {
    return getColor(branche);
  }

  public void initFlecheRatio() {
    if (getData().getArrowVariable() != null) {
      ReportRangeFinder rangeFinder = new ReportRangeFinder();
      CtuluRange range = rangeFinder.getRangeForSelectedTime(EnumCatEMH.BRANCHE, getData().getArrowVariable());
      double max = range.getMax();
      getData().setArrowRatio(ReportPlanimetryBrancheConfig.getDefaultRatio(max));
    }
  }

  @Override
  public void decoreTraceLigne(TraceLigneModel ligne, PlanimetryLigneBriseeLayerModel model, BrancheConfiguration brancheConfiguration) {
  }

  public static double getDefaultRatio(double maxValue) {
    //20 represente les pixel max.
    return 20 / maxValue;
  }

  @Override
  public TraceIcon decoreMiddleIcon(CatEMHBranche branche, TraceIcon traceIcon, PlanimetryBrancheModel model, BrancheConfiguration brancheConfiguration, GrSegment direction) {
    if (data.getArrowVariable() != null && branche != null) {
      Double val = getValue(data.getArrowVariable(), branche);
      if (val != null) {
        double size = val * data.getArrowRatio();//pour l'instant...
        if (val < 0) {
          size = Math.min(size, -brancheConfiguration.getLigneModel().getEpaisseur() - 3);
        } else {
          size = Math.max(size, brancheConfiguration.getLigneModel().getEpaisseur() + 3);
        }
        double longueurXY = direction.longueurXY();
        double dx = direction.getVx();
        double dy = direction.getVy();
        direction.o_.x_ = 0;
        direction.o_.y_ = 0;
        direction.e_.x_ = -dx * size / longueurXY;
        direction.e_.y_ = -dy * size / longueurXY;
        GrSegment pt1 = direction.applique(GrMorphisme.rotationZ(Math.PI / 4));
        GrSegment pt2 = direction.applique(GrMorphisme.rotationZ(-Math.PI / 4));
        TraceIconFleche iconFleche = new TraceIconFleche();
        iconFleche.setDx1((int) pt1.e_.x_);
        iconFleche.setDy1(-(int) pt1.e_.y_);//les y vont vers le bas
        iconFleche.setDx2((int) pt2.e_.x_);
        iconFleche.setDy2(-(int) pt2.e_.y_);//les y vont vers le bas
        final TraceIconModel iconModel = traceIcon.getModel().cloneData();
        iconModel.setEpaisseurLigne(brancheConfiguration.getLigneModel().getEpaisseur());
        iconModel.setTaille(Math.abs((int) size));
        iconFleche.setModel(iconModel);
        return iconFleche;
      }
    }
    return traceIcon;
  }

  @Override
  public int getMiddleIconWidth(int initTaille, PlanimetryBrancheModel model, BrancheConfiguration brancheConfiguration) {
    if (data.getArrowVariable() != null) {
      return 0;
    }
    return initTaille;
  }

  @Override
  public String getDisplayName(CatEMHBranche branche, PlanimetryBrancheModel planimetryBrancheModel, BrancheConfiguration brancheConfiguration, boolean isSelected) {
    return getDisplayName(branche, isSelected);
  }
}
