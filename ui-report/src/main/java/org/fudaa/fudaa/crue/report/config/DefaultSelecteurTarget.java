/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.config;

import java.beans.PropertyChangeListener;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;

/**
 *
 * @author Frederic Deniger
 */
public class DefaultSelecteurTarget<T> implements BSelecteurTargetInterface {

  T ft;

  public DefaultSelecteurTarget(T ft) {
    this.ft = ft;
  }

  public T getObject() {
    return ft;
  }

  @Override
  public void addPropertyChangeListener(String _key, PropertyChangeListener _l) {
  }

  @Override
  public void removePropertyChangeListener(String _key, PropertyChangeListener _l) {
  }

  @Override
  public Object getProperty(String _key) {
    return ft;
  }

  @Override
  public Object getMoy(String _key) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Object getMin(String _key) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean setProperty(String _key, Object _newProp) {
    ft = (T) _newProp;
    return true;
  }
}
