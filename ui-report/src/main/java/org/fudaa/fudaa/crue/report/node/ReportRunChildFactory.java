/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.node;

import java.util.List;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;

/**
 *
 * @author Frederic Deniger
 */
public class ReportRunChildFactory extends ChildFactory<EMHRun> {

  private final ManagerEMHScenario scenario;

  public ReportRunChildFactory(ManagerEMHScenario scenario) {
    this.scenario = scenario;
  }

  @Override
  protected boolean createKeys(List<EMHRun> toPopulate) {
    if (scenario.getListeRuns() != null) {
      toPopulate.addAll(scenario.getListeRuns());
    }
    return scenario.hasRun();
  }

  @Override
  protected Node createNodeForKey(EMHRun key) {
    return new ReportRunNode(key);
  }
}
