/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.helper;

import com.jidesoft.swing.CheckBoxList;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.CheckBoxListPopupListener;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.ToStringInternationalizableCellRenderer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.util.List;
import java.util.*;

import static org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder.getDialogHelpCtxId;

/**
 *
 * @author Frederic Deniger
 */
public class ChooseEMHByTypeHelper {

  final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);

  public void createListModel(final EnumCatEMH cat, final List<EMH> allSimpleEMH, final DefaultListModel toUse) {
    toUse.clear();
    for (final EMH emh : allSimpleEMH) {
      if (cat.equals(emh.getCatType())) {
        toUse.addElement(emh.getNom());
      }
    }
  }

  public List<String> chooseEMH(final List<String> emhs) {
    return chooseEMH(emhs, false);
  }

  public String chooseUniqueEMH(final String emh) {
    final List<String> chooseEMH = chooseEMH(Collections.singletonList(emh), true);
    if (CollectionUtils.isNotEmpty(chooseEMH) && chooseEMH.size() == 1) {
      return chooseEMH.get(0);
    }
    return null;
  }

  private List<String> chooseEMH(final List<String> emhs, final boolean unique) {
    final CheckBoxList listOfEmhToAdd = new CheckBoxList(new DefaultListModel());
    if (unique) {
      listOfEmhToAdd.getCheckBoxListSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    final JPanel pn = new JPanel(new BorderLayout(5, 5));
    pn.add(listOfEmhToAdd);
    final EnumCatEMH[] values = new EnumCatEMH[]{EnumCatEMH.CASIER, EnumCatEMH.BRANCHE, EnumCatEMH.NOEUD, EnumCatEMH.SECTION};
    final JComboBox cb = new JComboBox(values);
    cb.setRenderer(new ToStringInternationalizableCellRenderer());
    cb.setSelectedItem(null);


    final List<EMH> allSimpleEMH = reportService.getModele().getAllSimpleEMHinUserOrder();
    final Set<String> toSelect = new HashSet<>(emhs);
    cb.addItemListener(e -> {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        final DefaultListModel toUse = (DefaultListModel) listOfEmhToAdd.getModel();
        createListModel((EnumCatEMH) cb.getSelectedItem(), allSimpleEMH, toUse);
        for (int i = 0; i < toUse.getSize(); i++) {
          if (toSelect.contains(toUse.getElementAt(i))) {
            listOfEmhToAdd.addCheckBoxListSelectedIndex(i);
          }
        }
      }
    });
    final EnumCatEMH usedCat = getUsedCat(emhs);
    if (CollectionUtils.isNotEmpty(emhs)
            && usedCat == null) {
      DialogHelper.showWarn(org.openide.util.NbBundle.getMessage(ChooseEMHByTypeHelper.class, "UsedEMHNotFound.warning"));
    }
    if (usedCat != null) {
      cb.setSelectedItem(usedCat);
    }
    pn.add(cb, BorderLayout.NORTH);
    if (!unique) {
      new CheckBoxListPopupListener(listOfEmhToAdd);
    }
    pn.add(new JScrollPane(listOfEmhToAdd));
    SysdocUrlBuilder.installHelpShortcut(pn,getDialogHelpCtxId("ChoixEMHs", PerspectiveEnum.REPORT));
    pn.add(SysdocUrlBuilder.createButtonHelpNotionDialog("EMH"), BorderLayout.SOUTH);
    final boolean accepted = DialogHelper.showQuestionOkCancel(NbBundle.getMessage(ChooseEMHByTypeHelper.class, "AddEmh.ButtonName"), pn);
    if (accepted) {
      final Object[] checkBoxListSelectedValues = listOfEmhToAdd.getCheckBoxListSelectedValues();
      final ArrayList<String> selected = new ArrayList<>();
      for (final Object object : checkBoxListSelectedValues) {
        selected.add((String) object);
      }
      return selected;
    }
    return null;
  }

  public static EnumCatEMH getUsedCat(final ReportService reportService, final List<String> emhs) {
    for (final String emh : emhs) {
      final EMH found = reportService.getRunCourant().getEMH(emh);
      if (found != null) {
        return found.getCatType();
      }
    }
    return null;
  }

  public EnumCatEMH getUsedCat(final List<String> emhs) {
    return getUsedCat(reportService, emhs);

  }
}
