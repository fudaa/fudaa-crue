package org.fudaa.fudaa.crue.report.actions;

import java.awt.event.ActionEvent;
import javax.swing.*;
import org.fudaa.fudaa.crue.aoc.service.AocService;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.UserSaveAnswer;
import org.fudaa.fudaa.crue.report.ReportParametrageMesurePostTraitementTopComponent;
import org.fudaa.fudaa.crue.report.ReportParametrageEchelleSectionTopComponent;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.fudaa.fudaa.crue.report.service.ReportSaver;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

@ActionID(category = "View",
        id = "org.fudaa.fudaa.crue.report.actions.PostCloseAction")
@ActionRegistration(displayName = "#CTL_ReportCloseAction")
@ActionReferences({
        @ActionReference(path = "Actions/Report", position = 3)
})
public final class ReportCloseAction extends AbstractReportAction {
    protected final AocService aocService = Lookup.getDefault().lookup(AocService.class);

    public ReportCloseAction() {
        super(false);
        putValue(Action.NAME, NbBundle.getMessage(ReportCloseAction.class, "CTL_ReportCloseAction"));
    }

    @Override
    public Action createContextAwareInstance(Lookup actionContext) {
        return new ReportCloseAction();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        PerspectiveServiceReport perspectiveServiceReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);
        if (perspectiveServiceReport.isDirty()) {
            UserSaveAnswer confirmSaveOrNot = DialogHelper.confirmSaveOrNot();
            if (UserSaveAnswer.CANCEL.equals(confirmSaveOrNot)) {
                return;
            }
            if (UserSaveAnswer.SAVE.equals(confirmSaveOrNot)) {
                new ReportSaver().save();
            } else {
                new ReportSaver().reload();
            }
        }

        perspectiveServiceReport.setDirty(false);
        postService.unloadCurrentRun();
        TopComponent toClose = WindowManager.getDefault().findTopComponent(ReportParametrageMesurePostTraitementTopComponent.TOPCOMPONENT_ID);
        if (toClose != null) {
            toClose.close();
        }
        toClose = WindowManager.getDefault().findTopComponent(ReportParametrageEchelleSectionTopComponent.TOPCOMPONENT_ID);
        if (toClose != null) {
            toClose.close();
        }
        if (aocService.getLhptService().isLoaded() && !aocService.isAocCampagneLoaded()) {
            aocService.getLhptService().close();
        }

        selectedPerspectiveService.activePerspective(PerspectiveEnum.STUDY);
    }
}
