/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.ordres;

import java.util.HashSet;
import java.util.Set;
import org.fudaa.dodico.crue.metier.result.OrdResVariableSelection;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;

/**
 * Contient l'ensemble des variables sélectionnables/sélectionnées par run
 *
 * @author Frederic Deniger
 */
public class ReportOrdResSelectionByRun {

  final ReportRunKey runKey;
  final OrdResVariableSelection selection;
  final Set<String> nonEditableVariables = new HashSet<>();

  public ReportOrdResSelectionByRun(ReportRunKey runKey, OrdResVariableSelection selection) {
    this.runKey = runKey;
    this.selection = selection;
  }

  public ReportRunKey getRunKey() {
    return runKey;
  }

  public OrdResVariableSelection getSelection() {
    return selection;
  }

  public void addNonEditableVar(String var) {
    nonEditableVariables.add(var);
  }

  boolean isEditable(String variable) {
    return !nonEditableVariables.contains(variable);
  }

  boolean isSelectable(String variable) {
    return selection.isSelectable(variable);
  }
}
