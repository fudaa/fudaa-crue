/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.longitudinal;

import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLongitudinalBrancheConfigRestoreLengthNodeAction extends AbstractEditNodeAction {


  public ReportLongitudinalBrancheConfigRestoreLengthNodeAction() {
    super(NbBundle.getMessage(ReportLongitudinalBrancheConfigRestoreLengthNodeAction.class, "RestoreBrancheLength.ActionName"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return activatedNodes.length >= 1;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    for (Node node : activatedNodes) {
      ((ReportLongitudinalBrancheConfigNode) node).updateLength();
    }
  }
}
