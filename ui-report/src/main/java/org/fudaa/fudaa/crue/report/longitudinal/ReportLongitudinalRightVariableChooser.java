/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.longitudinal;

import com.memoire.bu.BuGridLayout;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.helper.ResPrtHelper;
import org.fudaa.dodico.crue.metier.result.OrdResExtractor;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableHelper;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalConfig;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.report.data.ReportVariableCellRenderer;
import org.fudaa.fudaa.crue.report.helper.ByRunSelector;
import org.fudaa.fudaa.crue.report.helper.ByRunSelector.Line;
import org.fudaa.fudaa.crue.report.service.ReportFormuleService;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLongitudinalRightVariableChooser {

  final ReportResultProviderService reportResultProviderService = Lookup.getDefault().lookup(ReportResultProviderService.class);
  final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
  final ReportFormuleService reportFormuleService = Lookup.getDefault().lookup(ReportFormuleService.class);
  final ReportLongitudinalConfig config;

  public ReportLongitudinalRightVariableChooser(final ReportLongitudinalConfig config) {
    this.config = config;
  }

  public boolean displayChooseUI() {
    final List<String> oresVariables = getChoosableVariables();

    final List<String> selectedVariables = new ArrayList<>();

    // ajout sélection vide
    selectedVariables.add(" ");

    // ajout des variables RPTI
    final List<String> varQIni = ResPrtHelper.getQiniVar();
    selectedVariables.addAll(varQIni);

    // ajout des variables ORES
    selectedVariables.addAll(oresVariables);

    final ByRunSelector selector = new ByRunSelector();
    final List<Line> createLines = selector.createLines(1);
    final int nbCheckBox = createLines.get(0).getNbComponents();
    final ReportVariableCellRenderer cellRenderer = new ReportVariableCellRenderer();
    final JPanel pn = new JPanel(new BuGridLayout(2 + nbCheckBox, 5, 5));
    pn.add(new JLabel(NbBundle.getMessage(ReportLongitudinalRightVariableChooser.class, "ChooseRightVariable.Label")));
    final JComboBox cb1 = new JComboBox(selectedVariables.toArray(new String[0]));
    cb1.setRenderer(cellRenderer);
    pn.add(cb1);
    createLines.get(0).addToPanel(pn);
    final String var = config.getVariableDrawnOnRightAxis(reportResultProviderService);
    cb1.setSelectedItem(var);
    if (StringUtils.isNotBlank(var)) {
      final List<ReportRunVariableKey> runVariableKeys = config.getReportRunVariableKey(var);
      final Set<ReportRunKey> runKeys = new HashSet<>();
      for (final ReportRunVariableKey runVariableKey : runVariableKeys) {
        runKeys.add(runVariableKey.getRunKey());
      }
      createLines.get(0).updateCheckBoxes(runKeys);
    } else {
      final Set<ReportRunKey> runKeys = new HashSet<>();
      runKeys.add(reportService.getCurrentRunKey());
      createLines.get(0).updateCheckBoxes(runKeys);
    }

    final boolean ok = DialogHelper.showQuestionAndSaveDialogConf(NbBundle.getMessage(ReportLongitudinalRightVariableChooser.class,
            "ChooseRightVariable.DialogTitle"), pn, getClass(), "vueProfilLongitudinal_ConfigurationAutresVariables", PerspectiveEnum.REPORT, false,
            DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    if (ok) {
      config.removeAllVariableDrawnOnRightAxis(reportResultProviderService);
      String newVar = (String) cb1.getSelectedItem();
      if (StringUtils.isBlank(newVar)) {
        newVar = null;
      }
      ReportVariableKey varKey = null;
      if (varQIni.contains(newVar)) {
        varKey = new ReportVariableKey(ReportVariableTypeEnum.RESULTAT_RPTI, newVar);
      } else if (newVar != null) {
        varKey = reportResultProviderService.createVariableKey(newVar);
      }
      if (newVar != null) {
        final List<ReportRunKey> selectedKeys = createLines.get(0).getSelectedKeys();
        for (final ReportRunKey reportRunKey : selectedKeys) {
          config.getProfilVariables().add(new ReportRunVariableKey(reportRunKey, varKey));
        }
      }
    }

    return ok;

  }

  protected List<String> getChoosableVariables() {
    final EMHScenario scenario = reportService.getRunCourant().getScenario();
    final OrdResExtractor extractor = new OrdResExtractor(scenario.getOrdResScenario());
    final List<String> oresVariables = extractor.getSelectableVariables(EnumCatEMH.SECTION, reportFormuleService.getVariablesKeys());
    //on enleve les variables qui sont affichées sur l'axe des z:
    for (final Iterator<String> it = oresVariables.iterator(); it.hasNext();) {
      final String string = it.next();
      if (ReportRunVariableHelper.isNatZVar(string, reportResultProviderService)) {
        it.remove();
      }
    }
    return oresVariables;
  }
}
