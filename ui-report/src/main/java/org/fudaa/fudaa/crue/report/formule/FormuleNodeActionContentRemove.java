/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.formule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDependenciesContent;
import org.fudaa.dodico.crue.projet.report.formule.FormuleParametersExpr;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleNodeActionContentRemove extends AbstractEditNodeAction {

  final ReportGlobalServiceContrat reportGlobalServiceContrat = Lookup.getDefault().lookup(ReportGlobalServiceContrat.class);

  public FormuleNodeActionContentRemove() {
    super(NbBundle.getMessage(FormuleNodeActionContentRemove.class, "formule.delete"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return true;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    FormuleDependenciesContent dependencies = FormuleContentChildFactory.getDependenciesContent(activatedNodes[0], reportGlobalServiceContrat);
    FormuleDependenciesContent.UsedBy createUsedBy = dependencies.createUsedBy();
    List<String> notDeleted = new ArrayList<>();
    try {
      for (Node node : activatedNodes) {
        FormuleParametersExpr lookup = node.getLookup().lookup(FormuleParametersExpr.class);
        if (createUsedBy.isUsed(lookup.getId())) {
          notDeleted.add(lookup.getId());
        } else {
          node.destroy();
        }
      }
    } catch (IOException ex) {
      Exceptions.printStackTrace(ex);
    }
    if (!notDeleted.isEmpty()) {
      String msg = null;
      if (notDeleted.size() == 1) {
        final String var = notDeleted.get(0);
        final String usedBy = "<li>" + StringUtils.join(createUsedBy.usedBy(var), "</li><li>") + "</li>";
        msg = NbBundle.getMessage(FormuleNodeActionContentRemove.class, "formuleNotDeleted.UsedBy", var, usedBy);
      } else {
        StringBuilder builder = new StringBuilder();
        for (String string : notDeleted) {
          builder.append("<li>");
          builder.append(NbBundle.getMessage(FormuleNodeActionContentRemove.class, "formuleUsedBy", string, StringUtils.join(createUsedBy.usedBy(
                  string), ", ")));
          builder.append("</li>");
        }
        msg = NbBundle.getMessage(FormuleNodeActionContentRemove.class, "listFormulesNotDeleted.UsedBy", builder.toString());
      }
      DialogHelper.showWarn(getName(), msg);

    }
  }
}
