/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report;

import java.util.List;
import javax.swing.Action;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.fudaa.crue.loi.ViewCourbeManager;
import org.fudaa.fudaa.crue.report.config.CourbeViewReportConfigurationTarget;

/**
 *
 * @author Frederic Deniger
 */
public class ReportConfigurationManagerTarget<T extends AbstractReportTimeViewTopComponent> implements CourbeViewReportConfigurationTarget {
  
  final T topComponent;
  
  public ReportConfigurationManagerTarget(T topComponent) {
    this.topComponent = topComponent;
  }
  
  @Override
  public ViewCourbeManager getManager() {
    return topComponent.getLoiLegendManager();
  }
  
  @Override
  public void applyTitleChanged() {
    topComponent.applyTitleConfigChanged();
  }
  
  @Override
  public void setChanged() {
    topComponent.setModified(true);
  }
  
  @Override
  public void applyLabelsChanged() {
    topComponent.applyLabelsConfigChanged();
  }
  
  @Override
  public List<ReportVariableKey> getTitleAvailableVariables() {
    return topComponent.getTitleAvailableVariables();
  }
  
  @Override
  public List<? extends Action> getAdditionalActions() {
    return topComponent.getAdditionalActionsToAddConfigureButton();
  }
}
