package org.fudaa.fudaa.crue.report.actions;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractHelpAction;
import org.fudaa.fudaa.crue.report.perspective.ActiveReport;
import org.openide.awt.*;

@ActionID(category = "View",
        id = "org.fudaa.fudaa.crue.report.actions.ReportHelpAction")
//iconBase ne semble pas fonctionner !
@ActionRegistration(displayName = "#CTL_ReportHelpAction", iconBase = AbstractHelpAction.HELP_ICON)
@ActionReference(path = ActiveReport.ACTIONS_REPORT, position = 100, separatorBefore = 99)
public final class ReportHelpAction extends AbstractHelpAction {

  public ReportHelpAction() {
    super(PerspectiveEnum.REPORT);
  }
}
