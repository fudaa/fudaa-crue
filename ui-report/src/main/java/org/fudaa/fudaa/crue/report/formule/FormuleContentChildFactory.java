/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.formule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.CtuluLogResult;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.projet.report.FormuleContentManager;
import org.fudaa.dodico.crue.projet.report.ReportFormuleServiceContrat;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;
import org.fudaa.dodico.crue.projet.report.formule.FormuleContent;
import org.fudaa.dodico.crue.projet.report.formule.FormuleContentExpr;
import org.fudaa.dodico.crue.projet.report.formule.FormuleContentManagerDefault;
import org.fudaa.dodico.crue.projet.report.formule.FormuleContentManagerDefaultBuilder;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDependenciesContent;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDependenciesProcessor;
import org.fudaa.dodico.crue.projet.report.formule.FormuleParametersExpr;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleContentChildFactory {

  public static List<FormuleNodeParameters> getAllNodesFromRoot(final Node rootNode) {
    final List<FormuleNodeParameters> res = new ArrayList<>();
    if (rootNode == null) {
      return res;
    }
    final Node[] children = rootNode.getChildren().getNodes();
    for (final Node parameterNode : children) {
      res.add((FormuleNodeParameters) parameterNode);
    }
    return res;
  }

  public static List<FormuleParametersExpr> getAllParametersFromRoot(final Node rootNode) {
    final List<FormuleNodeParameters> allNodesFromRoot = getAllNodesFromRoot(rootNode);
    final List<FormuleParametersExpr> res = new ArrayList<>();
    for (final FormuleNodeParameters node : allNodesFromRoot) {
      final FormuleParametersExpr lookup = node.getLookup().lookup(FormuleParametersExpr.class);
      if (lookup != null) {
        res.add(lookup);
      }
    }
    return res;
  }

  public static Node getRootNode(final Node one) {
    Node rootNode = one;
    while (rootNode != null && rootNode.getParentNode() != null) {
      rootNode = rootNode.getParentNode();
    }
    return rootNode;
  }
  final ReportFormuleServiceContrat reportFormuleService = Lookup.getDefault().lookup(ReportFormuleServiceContrat.class);

  public FormuleContentChildFactory() {
  }

  public Node createMainNode() {
    return new FormuleNodeGroupExpr(NodeHelper.createArray(createNodes(FormuleContentExpr.class), false));
  }

  public static List<FormuleNodeParameters> getAllNodes(final Node one) {
    final Node rootNode = getRootNode(one);
    return getAllNodesFromRoot(rootNode);
  }

  public static List<FormuleParametersExpr> getAllParameters(final Node one) {
    final Node rootNode = getRootNode(one);
    return getAllParametersFromRoot(rootNode);
  }

  public static FormuleDependenciesContent getDependenciesContent(final Node one, final ReportGlobalServiceContrat service) {
    final List<FormuleParametersExpr> allParameters = FormuleContentChildFactory.getAllParameters(one);
    final FormuleContentManagerDefaultBuilder builder = new FormuleContentManagerDefaultBuilder(allParameters, service);
    final CtuluLogResult<FormuleContentManager> create = builder.create();
    FormuleContentManager resultat = create.getResultat();
    if (resultat == null) {
      resultat = new FormuleContentManagerDefault();
    }
    final FormuleDependenciesProcessor processor = new FormuleDependenciesProcessor(resultat);
    return processor.compute(TransformerHelper.toId(allParameters));

  }

  protected List<FormuleNodeParameters> createNodes(final Class variableType) {
    final List<FormuleNodeParameters> nodes = new ArrayList<>();
    final List<FormuleContent> variables = reportFormuleService.getContents();
    List<FormuleContent> selected = null;
    if (variableType != null) {
      selected = EMHHelper.selectInClasses(variables, variableType);
    } else {
      selected = new ArrayList<>(variables);
    }
    Collections.sort(selected);
    for (final FormuleContent formuleContent : selected) {
      if (FormuleContentExpr.class.equals(variableType)) {
        nodes.add(new FormuleNodeParametersExpr((FormuleParametersExpr) formuleContent.getParameters().clone(), false));
      }

    }
    return nodes;
  }
}
