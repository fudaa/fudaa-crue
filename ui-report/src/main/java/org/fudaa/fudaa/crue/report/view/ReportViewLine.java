/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view;

import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.ReportViewLineInfo;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.fudaa.crue.report.ReportTopComponentConfigurable;
import org.fudaa.fudaa.crue.report.ReportVisualTopComponent;
import org.openide.windows.WindowManager;

/**
 * Représente une ligne de la vue des rapports
 *
 * @author Frederic Deniger
 */
public class ReportViewLine {
    final Class<ReportTopComponentConfigurable> topComponentClass;
    final String topComponentMode;
    final String topComponentId;
    final ReportViewLineInfo lineInformation;
    final ReportContentType contentType;
    ReportConfigContrat reportConfig;
    boolean modified;
    ReportTopComponentConfigurable topComponent;

    public ReportViewLine(ReportContentType contentType, ReportViewLineInfo info, Class<ReportTopComponentConfigurable> topComponentClass,
                          String topComponentMode, String topComponentId) {
        this.topComponentClass = topComponentClass;
        this.topComponentId = topComponentId;
        this.topComponentMode = topComponentMode;
        this.lineInformation = info;
        this.contentType = contentType;
        assert contentType != null;
    }

    public ReportViewLineInfo getLineInformation() {
        return lineInformation;
    }

    public ReportContentType getContentType() {
        return contentType;
    }

    public String getTopComponentId() {
        return topComponentId;
    }

    public ReportViewLine copy(ConnexionInformation ci) {
        ReportViewLine res = new ReportViewLine(contentType, lineInformation.copy(ci), topComponentClass, topComponentMode, topComponentId);
        if (reportConfig == null) {
            ReportViewsSave save = new ReportViewsSave();
            res.reportConfig = save.load(this, contentType);
        } else {
            res.reportConfig = reportConfig.duplicate();
        }
        res.setModified(true);
        return res;
    }

    public ReportConfigContrat getReportConfig() {
        return reportConfig;
    }

    public void setReportConfig(ReportConfigContrat reportConfig) {
        this.reportConfig = reportConfig;
    }

    public String getTopComponentMode() {
        return topComponentMode;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    /**
     * @return le topComponent associé si crée
     */
    public ReportTopComponentConfigurable getTopComponent() {
        if (contentType.isPlanimetry()) {
            return (ReportTopComponentConfigurable) WindowManager.getDefault().findTopComponent(ReportVisualTopComponent.TOPCOMPONENT_ID);
        }
        return topComponent;
    }

    /**
     * @param topComponent le topComponent associé si crée
     */
    public void setTopComponent(ReportTopComponentConfigurable topComponent) {
        this.topComponent = topComponent;
    }

    Class<ReportTopComponentConfigurable> getTopComponentClass() {
        return topComponentClass;
    }
}
