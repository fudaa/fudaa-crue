/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.time;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.animation.EbliAnimationOutputAbstract;
import org.fudaa.ebli.animation.EbliAnimationOutputAvi;
import org.fudaa.ebli.animation.EbliAnimationOutputGIF;
import org.fudaa.ebli.animation.EbliAnimationOutputImage;
import org.fudaa.ebli.animation.EbliAnimationOutputInterface;
import org.fudaa.fudaa.crue.common.helper.ComboBoxHelper;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;

/**
 * @author Frederic Deniger
 */
public class ReportTimeVideoRecorderItemEditorPanel {

  private EbliAnimationOutputInterface output;
  private int nbFrames;

  public EbliAnimationOutputInterface getOutput() {
    if (configurePanel != null) {
      configurePanel.apply();
      configurePanel.validate();
    }
    return output;
  }

  protected boolean validate() {
    if (output != null && output != NONE) {
      String valid = output.isValid(false);
      if (valid != null) {
        DialogHelper.showError(valid);
      }
      return valid != null;
    }
    return true;
  }

  protected String validateString() {
    if (output != null && output != NONE) {
      configurePanel.apply();
      return output.isValid(false);
    }
    return null;
  }

  public void setOutput(EbliAnimationOutputInterface output, int nbFrames) {
    this.output = output;
    if (cb != null) {
      updateCbModel();
    }
    this.nbFrames = nbFrames;
  }

  private JPanel mainPanel;
  private JComboBox cb;
  private JPanel specPanel;

  Component getPanel() {
    if (mainPanel == null) {
      createMainPanel();
    }
    return mainPanel;
  }

  private static final EbliAnimationOutputInterface NONE = new EbliAnimationOutputAbstract((String) null) {


    @Override
    public boolean init(CtuluImageProducer _p, Component _parent, int _nbImg) {
      throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public EbliAnimationOutputInterface copy() {
      return NONE;
    }

    @Override
    public String getName() {
      throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void appendFrame(BufferedImage _im, boolean _last) {
      throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void finish() {
      throw new UnsupportedOperationException("Not supported yet.");
    }
  };

  private void updateCbModel() {
    ArrayList<EbliAnimationOutputInterface> outputs = new ArrayList<>();
    outputs.add(NONE);
    outputs.add(getOutput(new EbliAnimationOutputAvi()));
    outputs.add(getOutput(new EbliAnimationOutputGIF()));
    outputs.add(getOutput(new EbliAnimationOutputImage()));
    ComboBoxHelper.setDefaultModel(cb, outputs);
    cb.setSelectedItem(output == null ? NONE : output);

  }

  private EbliAnimationOutputInterface getOutput(EbliAnimationOutputInterface proposed) {
    if (output != null && output.getClass().equals(proposed.getClass())) {
      return output;
    }
    return proposed;

  }

  private void createMainPanel() {
    mainPanel = new JPanel(new BorderLayout(0, 5));
    cb = new JComboBox();
    cb.setRenderer(new CtuluCellTextRenderer() {
      @Override
      protected void setValue(Object _value) {
        if (_value != NONE) {
          super.setValue(((EbliAnimationOutputInterface) _value).getName());
        } else {
          super.setValue(org.openide.util.NbBundle.getMessage(ReportTimeVideoRecorderItemEditorPanel.class, "NonOutput.Item"));
        }
      }
    });
    updateCbModel();
    cb.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
          output = (EbliAnimationOutputInterface) cb.getSelectedItem();
          if (output == NONE) {
            output = null;
          }
          updateSpecPanel();
        }
      }
    });
    cb.setSelectedItem(output == null ? NONE : output);
    mainPanel.add(cb, BorderLayout.NORTH);
    specPanel = new JPanel(new BorderLayout());
    mainPanel.add(specPanel);
    updateSpecPanel();
  }

  CtuluDialogPanel configurePanel;

  private void updateSpecPanel() {
    specPanel.removeAll();
    if (output != null) {
      configurePanel = output.getConfigurePanel(nbFrames);
      specPanel.add(configurePanel);
    } else {
      configurePanel = null;
    }
    mainPanel.doLayout();
    mainPanel.repaint(0);
    JDialog ancestor = (JDialog) SwingUtilities.getAncestorOfClass(JDialog.class, mainPanel);
    if (ancestor != null) {
      ancestor.pack();
    }
  }
}
