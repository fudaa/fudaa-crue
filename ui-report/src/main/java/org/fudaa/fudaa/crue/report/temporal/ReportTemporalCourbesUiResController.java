/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.temporal;

import javax.swing.table.AbstractTableModel;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGTableAction;
import org.fudaa.ebli.courbe.EGTableGraphePanel;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.loi.res.CourbesUiResController;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportTemporalCourbesUiResController extends CourbesUiResController {

  public ReportTemporalCourbesUiResController() {
    EGTableAction tableAction = (EGTableAction) getActions("TABLE").get(0);
    tableAction.setAddOptions(false);
    tableAction.setUi(CtuluUIForNetbeans.DEFAULT);
    tableAction.setAddCheckbox(true);
    tableAction.setShowColumnToExport(false);
    tableAction.setDisplayAll(true);
    tableAction.setShowLabel(true);
    
  }

  @Override
  protected AbstractTableModel createTableModel() {
    return new ReportTemporalTableModel(getTableGraphePanel());
  }

  @Override
  protected void configureTablePanel() {
    super.configureTablePanel();
    tableGraphePanel.getTable().getColumnModel().getColumn(0).setCellRenderer(new CtuluCellTextRenderer());
  }

  protected static class ReportTemporalTableModel extends EGTableGraphePanel.SpecTableModel {

    public static final int COLUMN_CALCUL = 0;

    public ReportTemporalTableModel(EGTableGraphePanel graphePanel) {
      super(graphePanel);
      xColIndex = 1;
      yColIndex = 2;
    }

    @Override
    public int getColumnCount() {
      return 3;
    }

    @Override
    public Object getValueAt(int _rowIndex, int _columnIndex) {
      if (_columnIndex == COLUMN_CALCUL) {
        final EGCourbe courbe = getCourbe();
        String res = null;
        if (courbe != null) {
          res = courbe.getModel().getPointLabel(_rowIndex);
        }
        return StringUtils.defaultString(res);
      }
      return super.getValueAt(_rowIndex, _columnIndex);
    }

    @Override
    public String getColumnName(int _column) {
      if (_column == COLUMN_CALCUL) {
        return NbBundle.getMessage(ReportTemporalCourbesUiResController.class, "Calcul.ColName");
      }
      return super.getColumnName(_column);
    }

    @Override
    public Class getColumnClass(int _columnIndex) {
      if (_columnIndex <= COLUMN_CALCUL) {
        return String.class;
      }
      return Double.class;
    }

    @Override
    public void setValueAt(Object _value, int _rowIndex, int _columnIndex) {
    }
  }
}
