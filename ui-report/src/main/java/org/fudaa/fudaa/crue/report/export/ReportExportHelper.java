/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.export;

import com.memoire.bu.BuBorders;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserWriter;
import org.fudaa.ctulu.table.CtuluTableCsvWriter;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.metier.OrdResKey;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportFormuleServiceContrat;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.CrueSwingWorker;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.report.ordres.SelectOrdResHelper;
import org.fudaa.fudaa.crue.report.ordres.SelectOrdResNode;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.fudaa.fudaa.crue.report.time.ReportTimeChooser;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;
import java.util.*;

/**
 *
 * @author Frederic Deniger
 */
public class ReportExportHelper {

  final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
  final ReportFormuleServiceContrat reportFormuleService = Lookup.getDefault().lookup(ReportFormuleServiceContrat.class);

  public ReportExportHelper() {
  }

  public void displayDialogExport(final Collection<EMH> emhs) {
    if (CollectionUtils.isEmpty(emhs)) {

      DialogHelper.showError(org.openide.util.NbBundle.getMessage(ReportExportHelper.class, "Export.NoEmhSelected"));
    } else {
      final EMHScenario scenario = reportService.getRunCourant().getScenario();
      final SelectOrdResNode node = SelectOrdResHelper.createNode(scenario, emhs, reportFormuleService.getVariablesKeys());
      final PropertySheet sheet = new PropertySheet();
      sheet.setNodes(new Node[]{node});
      final JPanel pn = new JPanel(new BorderLayout(10, 5));
      final JPanel pnSheet = new JPanel(new BorderLayout(2, 2));
      pnSheet.add(new JLabel(NbBundle.getMessage(ReportExportHelper.class, "Export.ChooseVariable")), BorderLayout.NORTH);
      pnSheet.add(sheet);
      pn.add(pnSheet, BorderLayout.WEST);
      final List<ResultatTimeKey> times = reportService.getTimeContent().getTimes();
      final ReportTimeChooser timeChooser = new ReportTimeChooser(times, reportService.getReportTimeFormatter().getResulatKeyTempsSimuToStringTransformer());
      pn.add(timeChooser.getPanel());
      final CtuluFileChooser fileChooser = new CtuluFileChooserWriter(CtuluUIForNetbeans.DEFAULT, Collections.singletonList(new CtuluTableCsvWriter()));
      final CtuluFileChooserPanel fileChooserPanel = new CtuluFileChooserPanel() {
        @Override
        protected CtuluFileChooser createFileChooser() {
          return fileChooser;
        }
      };
      final JPanel pnSouth = new JPanel(new FlowLayout(FlowLayout.LEFT));
      pnSouth.add(new JLabel(NbBundle.getMessage(ReportExportHelper.class, "Export.ChooseFile.LabelName")));
      pnSouth.add(fileChooserPanel);
      pn.add(pnSouth, BorderLayout.SOUTH);
      pn.setBorder(BuBorders.EMPTY3333);
      final DialogDescriptor dialogDescriptor = new DialogDescriptor(pn, NbBundle.getMessage(ReportExportHelper.class, "Export.DialogTitle"), true,
              new Object[]{NotifyDescriptor.OK_OPTION, NotifyDescriptor.CANCEL_OPTION},
              NotifyDescriptor.OK_OPTION,
              DialogDescriptor.DEFAULT_ALIGN,
              HelpCtx.DEFAULT_HELP,
              null);
      dialogDescriptor.setClosingOptions(new Object[]{NotifyDescriptor.CANCEL_OPTION});
      dialogDescriptor.createNotificationLineSupport();
      final OkActionListener listener = new OkActionListener(dialogDescriptor);
      listener.setEmhs(emhs);
      listener.setFileChooserPanel(fileChooserPanel);
      listener.setNode(node);
      listener.setTimeChooser(timeChooser);
      dialogDescriptor.setButtonListener(listener);
      SysdocUrlBuilder.installDialogHelpCtx(dialogDescriptor, "exporterResultats", PerspectiveEnum.REPORT, false);
      final Dialog dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
      listener.setDialog((JDialog) dialog);
      dialog.pack();
      dialog.setLocationRelativeTo(WindowManager.getDefault().getMainWindow());
      dialog.setVisible(true);
    }
  }

  private class OkActionListener implements ActionListener {

    JDialog dialog;
    final DialogDescriptor dialogDescriptor;
    CtuluFileChooserPanel fileChooserPanel;
    Collection<EMH> emhs;
    SelectOrdResNode node;
    ReportTimeChooser timeChooser;

    public void setTimeChooser(final ReportTimeChooser timeChooser) {
      this.timeChooser = timeChooser;
    }

    public void setNode(final SelectOrdResNode node) {
      this.node = node;
    }

    public void setEmhs(final Collection<EMH> emhs) {
      this.emhs = emhs;
    }

    public OkActionListener(final DialogDescriptor dialogDescriptor) {
      this.dialogDescriptor = dialogDescriptor;
    }

    public void setFileChooserPanel(final CtuluFileChooserPanel fileChooserPanel) {
      this.fileChooserPanel = fileChooserPanel;
    }

    public void setDialog(final JDialog dialog) {
      this.dialog = dialog;
    }

    protected boolean isEmpty(final Map<OrdResKey, List<String>> vars) {
      if (vars == null) {
        return true;
      }
      for (final List<String> list : vars.values()) {
        if (!list.isEmpty()) {
          return false;
        }
      }
      return true;
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
      final Object value = dialogDescriptor.getValue();
      if (!NotifyDescriptor.OK_OPTION.equals(value)) {
        return;
      }
      final File fileWithExt = fileChooserPanel.getFileWithExt();
      if (fileWithExt == null) {
        DialogHelper
                .showError(org.openide.util.NbBundle.getMessage(ReportExportHelper.class, "Export.NoFile.Error"));
        return;
      }
      final String error = CtuluLibFile.canWrite(fileWithExt);
      if (error != null) {
        DialogHelper.showError(error);
        return;
      }
      final Map<OrdResKey, List<String>> ordResVariableSelectionList = node.getSelectedVariables();
      if (isEmpty(ordResVariableSelectionList)) {
        DialogHelper
                .showError(org.openide.util.NbBundle.getMessage(ReportExportHelper.class, "Export.NoVariable.Error"));
        return;
      }
      final List<ResultatTimeKey> selected = timeChooser.getSelected();
      if (selected.isEmpty()) {
        DialogHelper
                .showError(org.openide.util.NbBundle.getMessage(ReportExportHelper.class, "Export.NoTime.Error"));
        return;
      }
      final ArrayList<EMH> sortedEMHS = CollectionCrueUtil.getSortedList(emhs, ObjetNommeByNameComparator.INSTANCE);
      final List<ReportExportContentByEMH> contentByEMH = new ArrayList<>();
      for (final EMH emh : sortedEMHS) {
        final List<String> variables = ordResVariableSelectionList.get(new OrdResKey(emh));
        if (CollectionUtils.isNotEmpty(variables)) {
          contentByEMH.add(new ReportExportContentByEMH(emh, variables));
        }
      }
      dialog.dispose();

      final CrueSwingWorker<Boolean> worker = new CrueSwingWorker<Boolean>(NbBundle.getMessage(ReportExportHelper.class,
              "Export.Task.Name")) {
        @Override
        protected Boolean doProcess() throws Exception {
          final ReportExportWriter writer = new ReportExportWriter();
          return writer.write(fileWithExt, selected, contentByEMH, reportService.getReportTimeFormatter());
        }

        @Override
        protected void done(final Boolean res) {
          if (Boolean.TRUE.equals(res)) {
            DialogHelper.showNotifyOperationTermine(NbBundle.getMessage(ReportExportHelper.class, "Export.OperationFinish", fileChooserPanel.
                    getFileWithExt().getAbsolutePath()));
          } else {
            DialogHelper.showError(NbBundle.getMessage(ReportExportHelper.class, "Export.DialogTitle"), org.openide.util.NbBundle.getMessage(
                    ReportExportHelper.class, "Exportg.OperationFailed",
                    fileChooserPanel.getFileWithExt().getAbsolutePath()));
          }
        }
      };
      worker.execute();

    }
  }
}
