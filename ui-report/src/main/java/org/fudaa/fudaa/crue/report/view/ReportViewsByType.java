/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view;

import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.ReportViewLineInfo;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.fudaa.crue.common.node.NodeChildrenHelper;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.report.ReportTopComponentConfigurable;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Children.Array;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public class ReportViewsByType {
    private final ReportContentType type;
    private final Class topComponentClass;
    private final String topComponentMode;
    private final String topComponentId;
    final AbstractNode node;
    private final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);
    private final Children.Array children = new Children.Array();
    //utilie si une vue a été supprimée
    private boolean modified;

    public ReportViewsByType(ReportContentType type, final Class topComponentClass, final String topComponentMode, final String topComponentId) {
        this.type = type;
        this.topComponentClass = topComponentClass;
        this.topComponentMode = topComponentMode;
        this.topComponentId = topComponentId;
        node = new AbstractNode(children, Lookups.fixed(type));
        node.setDisplayName(type.geti18n());
    }

    public Class getTopComponentClass() {
        return topComponentClass;
    }

    public String getTopComponentMode() {
        return topComponentMode;
    }

    public String getTopComponentId() {
        return topComponentId;
    }

    public void saveDone() {
        Node[] nodes = getChildren().getNodes();
        for (Node childNode : nodes) {
            ((ReportViewNode) childNode).saveOldName();
        }
    }

    public File getFolder(File dirOfRapports) {
        String folderName = getType().getFolderName();
        return new File(dirOfRapports, folderName);
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
        if (modified) {
            node.setDisplayName(type.geti18n() + ReportViewNode.MODIFIED_SUFFIX);
        } else {
            node.setDisplayName(type.geti18n());
        }
    }

    public AbstractNode getNode() {
        return node;
    }

    private List<ReportViewLine> getLines() {
        List<ReportViewLine> lines = new ArrayList<>();
        Node[] nodes = getChildren().getNodes();
        for (Node childNode : nodes) {
            ReportViewLine line = childNode.getLookup().lookup(ReportViewLine.class);
            if (line != null) {
                lines.add(line);
            }
        }
        return lines;
    }

    protected void clearNode() {
        Node[] nodes = getChildren().getNodes();
        for (Node childNode : nodes) {
            ReportViewLine line = childNode.getLookup().lookup(ReportViewLine.class);
            if (line != null && line.getTopComponent() != null) {
                if (line.getTopComponent().isClosable()) {
                    line.getTopComponent().getTopComponent().close();
                }
            }
        }
        NodeChildrenHelper.clearChildren(children);
    }

    public Array getChildren() {
        return children;
    }

    public ReportContentType getType() {
        return type;
    }

    private List<String> getUsedName() {
        List<String> res = new ArrayList<>();
        Node[] nodes = children.getNodes();
        for (Node node : nodes) {
            res.add(node.getName());
        }
        return res;
    }

    ReportViewNode created(ReportTopComponentConfigurable newTopComponent) {
        ReportViewLineInfo info = new ReportViewLineInfo();
        info.updateEdited(configurationManagerService.getConnexionInformation());

        ReportViewLine view = new ReportViewLine(type, info, topComponentClass, topComponentMode, topComponentId);
        view.setModified(true);
        view.setReportConfig(newTopComponent.getReportConfig());
        view.setTopComponent(newTopComponent);
        List<String> usedName = getUsedName();
        String name = newTopComponent.getReportConfig().getType().geti18n();
        UniqueNomFinder nomFinder = new UniqueNomFinder();
        String findUniqueName = nomFinder.findUniqueName(usedName, name, 2);
        info.setNom(findUniqueName);
        newTopComponent.setReportInfo(info);
        final ReportViewNode nodeAdded = added(view);
        newTopComponent.getTopComponent().setModified(true);
        view.setModified(true);
        return nodeAdded;
    }

    ReportViewNode added(ReportViewLine newLine) {
        final ReportViewNode newNode = new ReportViewNode(newLine);
        children.add(new Node[]{newNode});
        sortNodes();
        setModified(true);
        return newNode;
    }

    protected void sortNodes() {
        Node[] nodes = children.getNodes();
        children.remove(nodes);
        Arrays.sort(nodes);
        children.add(nodes);
    }

    /**
     * on suppose que toutes les configurations sont chargées.
     *
     * @param changes les changements a appliquer
     */
    void variablesUpdated(FormuleDataChanges changes) {
        List<ReportViewLine> lines = getLines();
        for (ReportViewLine reportViewLine : lines) {
            if (reportViewLine != null && reportViewLine.getReportConfig() != null) {
                boolean variablesModified = reportViewLine.getReportConfig().variablesUpdated(changes);
                if (variablesModified) {
                    reportViewLine.setModified(variablesModified);
                    ReportTopComponentConfigurable topComponent = reportViewLine.getTopComponent();
                    if (topComponent != null) {
                        topComponent.setReportConfig(topComponent.getReportConfig(), false);
                    }
                }
            }
        }
    }

    void loadAll(ReportViewsService reportViewsService) {
        List<ReportViewLine> lines = getLines();
        for (ReportViewLine reportViewLine : lines) {
            if (reportViewLine.getReportConfig() == null) {
                ReportConfigContrat load = reportViewsService.load(reportViewLine, type);
                reportViewLine.setReportConfig(load);
            }
        }
    }
}
