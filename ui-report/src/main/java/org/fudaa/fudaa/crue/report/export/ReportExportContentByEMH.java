/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.export;

import java.util.List;
import org.fudaa.dodico.crue.metier.emh.EMH;

/**
 *
 * @author Frederic Deniger
 */
public class ReportExportContentByEMH {

  final EMH emh;
  final List<String> var;

  public ReportExportContentByEMH(EMH emh, List<String> var) {
    this.emh = emh;
    this.var = var;
  }
  
  
}
