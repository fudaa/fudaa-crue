/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.export;

import com.Ostermiller.util.CSVPrinter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.fudaa.fudaa.crue.report.service.ReportTimeFormatter;
import org.fudaa.fudaa.crue.study.services.CrueService;
import org.fudaa.fudaa.crue.study.services.TableExportCommentSupplier;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportExportWriter {

  final ReportResultProviderService reportResultProviderService = Lookup.getDefault().lookup(ReportResultProviderService.class);
  CrueService crueService = Lookup.getDefault().lookup(CrueService.class);

  public ReportExportWriter() {
  }

  public boolean write(File f, List<ResultatTimeKey> times, List<ReportExportContentByEMH> content, ReportTimeFormatter timeFormatter) {
    CSVPrinter writer = null;
    ReportRunKey runKey = reportResultProviderService.getReportService().getRunCourant().getRunKey();
    boolean ok = false;
    try {
      writer = new CSVPrinter(new FileOutputStream(f));
      writer.changeQuote('"');
      writer.changeDelimiter(';');
      TableExportCommentSupplier commentSupplier = new TableExportCommentSupplier(true);
      List<String> comments = commentSupplier.getComments(NbBundle.getMessage(ReportExportWriter.class, "DefaultExportType"));
      for (String comment : comments) {
        writer.printlnComment(comment);
      }
      writer.write(NbBundle.getMessage(ReportExportWriter.class, "Export.CsvTitle.NomCalcul"));
      writer.write(NbBundle.getMessage(ReportExportWriter.class, "Export.CsvTitle.TempsSce"));
      writer.write(NbBundle.getMessage(ReportExportWriter.class, "Export.CsvTitle.DureeCalc"));
      for (ReportExportContentByEMH reportExportContentByEMH : content) {
        final List<String> variables = reportExportContentByEMH.var;
        for (String string : variables) {
          final PropertyNature property = reportResultProviderService.getPropertyNature(string);
          writer.write(reportExportContentByEMH.emh.getNom() + "-" + reportResultProviderService.getVariableName(string)
                  + PropertyNature.getUniteSuffixe(property));
        }
      }
      writer.writeln();
      for (ResultatTimeKey resultatKey : times) {
        writer.write(resultatKey.getNomCalcul());
        writer.write(timeFormatter.getResulatKeyTempsSceToStringTransformer().transform(resultatKey));
        writer.write(timeFormatter.getResulatKeyTempsSimuToStringTransformer().transform(resultatKey));
        for (ReportExportContentByEMH reportExportContentByEMH : content) {
          final List<String> variables = reportExportContentByEMH.var;
          for (String string : variables) {
            PropertyNature variable = reportResultProviderService.getPropertyNature(string);
            ReportVariableKey createVariableKey = reportResultProviderService.createVariableKey(string);
            Double value = reportResultProviderService.getValue(resultatKey, new ReportRunVariableKey(runKey, createVariableKey),
                    reportExportContentByEMH.emh.getNom(), ReportResultProviderServiceContrat.NO_SELECTED_TIMES_REPORT);
            writer.write(TransformerEMHHelper.formatFromNature(variable, value, reportResultProviderService.getReportService().getCcm(),
                    DecimalFormatEpsilonEnum.COMPARISON));
          }
        }
        writer.writeln();
        ok = true;
      }
    } catch (FileNotFoundException ex) {

    } catch (IOException ex) {
      Exceptions.printStackTrace(ex);
    } finally {
      if (writer != null) {
        try {
          writer.close();
        } catch (IOException iOException) {
          Exceptions.printStackTrace(iOException);
        }
      }
    }
    return ok;

  }
}
