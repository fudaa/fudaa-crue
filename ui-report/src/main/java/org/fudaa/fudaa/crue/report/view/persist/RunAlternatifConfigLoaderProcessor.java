/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import java.io.File;
import java.util.List;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.fudaa.fudaa.crue.report.view.ReportViewsService;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class RunAlternatifConfigLoaderProcessor implements ProgressRunnable<CrueIOResu<List<ReportRunKey>>> {

  ReportViewsService reportViewsService = Lookup.getDefault().lookup(ReportViewsService.class);
  ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
  ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);
  private final File file;

  public RunAlternatifConfigLoaderProcessor(File file) {
    this.file = file;
  }

  @Override
  public CrueIOResu<List<ReportRunKey>> run(ProgressHandle handle) {
    if (handle != null) {
      handle.switchToIndeterminate();
    }
    RunAlternatifSaver configSaver = new RunAlternatifSaver();
    return configSaver.read(file);
  }
}
