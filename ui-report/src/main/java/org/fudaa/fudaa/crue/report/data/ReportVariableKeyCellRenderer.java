/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.data;

import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportVariableKeyCellRenderer extends CtuluCellTextRenderer {

  final String nothing = NbBundle.getMessage(ReportVariableKeyCellRenderer.class, "Variable.NoSelection");

  @Override
  protected void setValue(Object _value) {
    if (_value == null) {
      super.setValue(nothing);
    } else if (ReportVariableKey.class.equals(_value.getClass())) {
      ReportVariableKey key = (ReportVariableKey) _value;
      super.setValue(ReportVariableKey.toString(key));
    } else {
      super.setValue(_value);
    }
  }
}
