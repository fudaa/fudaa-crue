/*
GPL 2
 */
package org.fudaa.fudaa.crue.report.longitudinal;

import com.memoire.fu.FuEmptyArrays;
import gnu.trove.TIntObjectHashMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fudaa.ctulu.table.CtuluTableCellDoubleValue;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableTimeKey;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGExportData;
import org.fudaa.ebli.courbe.EGExportDataBuilder;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGTimeLabel;
import org.fudaa.fudaa.crue.loi.common.LoiConstanteCourbeModel;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLongitudinalExportDataBuilder implements EGExportDataBuilder {

  @Override
  public EGExportData createExportData(EGCourbe[] courbesInitiales, EGGraphe _g, boolean _isSameH, boolean showLabel) {
    return new LongitudinalExportData(EGExportData.prepareCourbesForExport(courbesInitiales), _g, showLabel);
  }

  private static class LongitudinalExportData extends EGExportData {

    protected static final int COLUMN_X = 2;
    protected static final int COLUMN_SECTION = 1;
    protected static final int COLUMN_BRANCHE = 0;

//    private double[] time_;
//    private List<String> branches;
//    private List<String> sections;
final Data data = new Data();

    static class ColumnData {

      Class type;
      String columnName;
      final List values = new ArrayList();

      public Object getValue(int i) {
        if (i < 0 || i >= values.size()) {
          return null;
        }
        return values.get(i);
      }
    }

    static class Data {

      final List<ColumnData> columns = new ArrayList<>();

      ColumnData addColumn() {
        ColumnData res = new ColumnData();
        columns.add(res);
        return res;
      }

      int getMaxLine() {
        int res = 0;
        for (ColumnData column : columns) {
          res = Math.max(column.values.size(), res);
        }
        return res;
      }
    }

    LongitudinalExportData(final EGCourbe[] _cs, final EGGraphe _g, boolean showLabel) {
      super(_cs, _g, showLabel);
      //nombre de courbe + branche+ section + x
      if (cs_.length > 0) {
        final EGAxeHorizontal h = g_.getTransformer().getXAxe();
        //on arrange par run
        Map<ReportRunKey, List<EGCourbe>> courbesByRun = getCourbesByRun(_cs);
        for (Map.Entry<ReportRunKey, List<EGCourbe>> entry : courbesByRun.entrySet()) {

          List<EGCourbe> listOfCourbes = entry.getValue();
          List<EGTimeLabel> values = buildAbscisses(listOfCourbes);
          final ColumnData brancheColumn = data.addColumn();
          final ColumnData sectionColumn = data.addColumn();
          final ColumnData xColumn = data.addColumn();
          brancheColumn.type = String.class;
          ReportRunKey key = entry.getKey();
          brancheColumn.columnName = NbBundle.getMessage(ReportLongitudinalExportDataBuilder.class, "BrancheColumn.Name") + BusinessMessages.ENTITY_SEPARATOR + key.
                  getDisplayName();
          sectionColumn.type = String.class;
          sectionColumn.columnName = NbBundle.getMessage(ReportLongitudinalExportDataBuilder.class, "SectionColumn.Name") + BusinessMessages.ENTITY_SEPARATOR + key.
                  getDisplayName();
          xColumn.type = CtuluTableCellDoubleValue.class;
          xColumn.columnName = h.getTitre() + BusinessMessages.ENTITY_SEPARATOR + key.getDisplayName();
          for (EGTimeLabel line : values) {
            brancheColumn.values.add(line.getLabel());
            sectionColumn.values.add(line.getSubLabel());
            xColumn.values.add(getXValueFor(line.getX()));
          }
          for (EGCourbe c : listOfCourbes) {
            ColumnData valueColumn = data.addColumn();
            valueColumn.type = CtuluTableCellDoubleValue.class;
            valueColumn.columnName = c.getTitle();
            for (EGTimeLabel line : values) {
              valueColumn.values.add(getYValueFor(c, c.interpol(line.getX())));
            }
          }
        }
        maxCol_ = data.columns.size();
        maxLine_ = data.getMaxLine();
      }
    }

    protected List<EGTimeLabel> buildAbscisses(List<EGCourbe> value) {
      Set<EGTimeLabel> set = new HashSet<>();
      for (EGCourbe c : value) {
        final LoiConstanteCourbeModel model = (LoiConstanteCourbeModel) c.getModel();
        final TIntObjectHashMap<String> subLabelsBranches = model.getSubLabels(CruePrefix.P_BRANCHE);
        final TIntObjectHashMap<String> subLabelsSection = model.getSubLabels(CruePrefix.P_SECTION);
        for (int j = model.getNbValues() - 1; j >= 0; j--) {
          final double x = model.getX(j);
          EGTimeLabel timeValue = new EGTimeLabel(x);
          set.add(timeValue);
          timeValue.setLabel(subLabelsBranches.get(j));
          timeValue.setSubLabel(subLabelsSection.get(j));
        }
      }
      List<EGTimeLabel> values = new ArrayList<>(set);
      Collections.sort(values);
      return values;
    }

    protected Map<ReportRunKey, List<EGCourbe>> getCourbesByRun(final EGCourbe[] _cs) {
      Map<ReportRunKey, List<EGCourbe>> courbesByRun = new LinkedHashMap<>();
      for (int i = 0; i < _cs.length; i++) {
        EGCourbe c = _cs[i];
        final LoiConstanteCourbeModel model = (LoiConstanteCourbeModel) c.getModel();
        ReportRunVariableTimeKey key = (ReportRunVariableTimeKey) model.getKey();
        List<EGCourbe> courbes = courbesByRun.get(key.getReportRunKey());
        if (courbes == null) {
          courbes = new ArrayList<>();
          courbesByRun.put(key.getReportRunKey(), courbes);
        }
        courbes.add(c);
      }
      return courbesByRun;
    }

    @Override
    public Class getColumnClass(int _i) {
      return data.columns.get(_i).type;
    }

    @Override
    public String getColumnName(int _i) {
      return data.columns.get(_i).columnName;
    }

    @Override
    public int[] getSelectedRows() {
      return FuEmptyArrays.INT0;
    }

    @Override
    public Object getValue(int _row, int _col) {
      return data.columns.get(_col).getValue(_row);
    }

  }

}
