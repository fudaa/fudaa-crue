/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.service;

/**
 *
 * @author Frederic Deniger
 */
public interface ReportTimeListener {

  /**
   * si le pas de temps courant est modifié
   */
  void selectedTimeChanged();

  void rangeChanged();

  void timeFormatChanged();
}
