/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.data;

import com.jidesoft.swing.CheckBoxList;
import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableEmhKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.fudaa.crue.common.helper.CheckBoxListPopupListener;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.report.longitudinal.ReportLongitudinalTimeChooser;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportVariableChooser {

  private final List<String> choosableVariables;
  private final Set<String> currentSelected;

  public ReportVariableChooser(List<String> choosableVariables, Set<String> currentSelected) {
    this.choosableVariables = choosableVariables;
    this.currentSelected = currentSelected;
  }

  public static List<ReportRunVariableEmhKey> getKeysForCurrentRun(ReportResultProviderService reportService, List<String> variables,
          List<String> emhs) {
    List<ReportRunVariableEmhKey> res = new ArrayList<>();
    if (variables != null) {
      for (String variable : variables) {
        final ReportVariableKey createVariableKey = reportService.createVariableKey(variable);
        for (String emh : emhs) {
          ReportRunVariableEmhKey key = new ReportRunVariableEmhKey(reportService.getReportService().getCurrentRunKey(), createVariableKey, emh);
          res.add(key);
        }
      }
    }
    return res;

  }

  public static List<ReportRunVariableEmhKey> getKeysForRuns(ReportResultProviderService reportService, List<String> variables, List<String> emhs,
          List<ReportRunKey> runkeys) {
    List<ReportRunVariableEmhKey> res = new ArrayList<>();
    if (variables != null) {
      for (ReportRunKey runkey : runkeys) {
        for (String emh : emhs) {
          for (String variable : variables) {
            final ReportVariableKey createVariableKey = reportService.createVariableKey(variable);
            ReportRunVariableEmhKey key = new ReportRunVariableEmhKey(runkey, createVariableKey, emh);
            res.add(key);
          }

        }
      }
    }
    return res;

  }

  public List<ReportRunVariableKey> getKeysForRuns(ReportResultProviderService reportService, List<String> variables,
          List<ReportRunKey> runkeys) {
    List<ReportRunVariableKey> res = new ArrayList<>();
    if (variables != null) {
      for (String variable : variables) {
        final ReportVariableKey createVariableKey = reportService.createVariableKey(variable);
        for (ReportRunKey runkey : runkeys) {
          res.add(new ReportRunVariableKey(runkey, createVariableKey));
        }

      }
    }
    return res;

  }

  public List<String> choose() {
    final CheckBoxList list = new CheckBoxList(choosableVariables.toArray());
    for (int i = 0; i < choosableVariables.size(); i++) {
      if (currentSelected.contains(choosableVariables.get(i))) {
        list.addCheckBoxListSelectedIndex(i);
      }

    }
    list.setCellRenderer(new ReportVariableCellRenderer());
    new CheckBoxListPopupListener(list);
    JPanel pn = new JPanel(new BorderLayout(0, 5));
    final JCheckBox selectAll = new JCheckBox(NbBundle.getMessage(ReportLongitudinalTimeChooser.class, "ChooseTime.SelectAllNone"));
    selectAll.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(ItemEvent e) {
        boolean select = selectAll.isSelected();
        if (select) {
          list.selectAll();
        } else {
          list.selectNone();
        }
      }
    });
    pn.add(new JScrollPane(list));
    pn.add(selectAll, BorderLayout.NORTH);
    String title = org.openide.util.NbBundle.getMessage(ReportVariableChooser.class, "ReportVariableChooser.DialogTitle");
    boolean ok = DialogHelper.showQuestionAndSaveDialogConf(title, pn, getClass(), DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    if (ok) {
      final Object[] checkBoxListSelectedValues = list.getCheckBoxListSelectedValues();
      List<String> res = new ArrayList<>();
      for (Object checkBoxListSelectedValue : checkBoxListSelectedValues) {
        res.add((String) checkBoxListSelectedValue);
      }
      return res;
    }
    return Collections.emptyList();

  }

}
