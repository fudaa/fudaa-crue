/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.formule;

import com.jidesoft.swing.CheckBoxList;
import java.awt.BorderLayout;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.projet.report.formule.FormuleParametersExpr;
import org.fudaa.fudaa.commun.impl.FudaaGuiLib;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.report.view.persist.FormuleLoaderProcessor;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleNodeActionImport extends AbstractEditNodeAction {

  public FormuleNodeActionImport() {
    super(NbBundle.getMessage(FormuleNodeActionImport.class, "formule.import"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return activatedNodes.length >= 1;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    final CtuluFileChooser fileChooser = new CtuluFileChooser(true);
    File chooseFile = FudaaGuiLib.chooseFile(WindowManager.getDefault().getMainWindow(), false, fileChooser);
    if (chooseFile != null) {
      FormuleLoaderProcessor loader = new FormuleLoaderProcessor(chooseFile);
      CrueIOResu<List<FormuleParametersExpr>> bilan = CrueProgressUtils.showProgressDialogAndRun(loader, getName());
      if (bilan.getAnalyse().containsErrorOrSevereError()) {
        LogsDisplayer.displayError(bilan.getAnalyse(), getName());
        return;
      }
      final List<FormuleParametersExpr> toImports = bilan.getMetier() == null ? Collections.emptyList() : new ArrayList<>(bilan.getMetier());
      if (toImports.isEmpty()) {
        DialogHelper.showInfo(getName(), NbBundle.getMessage(FormuleNodeActionImport.class, "formuleImport.NoVariable"));
        return;
      }
      List<FormuleParametersExpr> allNodes = FormuleContentChildFactory.getAllParameters(activatedNodes[0]);
      Map<String, FormuleParametersExpr> existingName = TransformerHelper.toMapOfNom(allNodes);
      List<String> doublons = new ArrayList<>();
      for (Iterator<FormuleParametersExpr> it = toImports.iterator(); it.hasNext();) {
        FormuleParametersExpr toImport = it.next();
        if (existingName.containsKey(toImport.getNom())) {
          doublons.add(toImport.getNom() + " = " + toImport.getFormule());
          it.remove();
        }
      }
      if (!doublons.isEmpty()) {
        String values = "<ul><li>" + StringUtils.join(doublons, "</li><li>" + "</ul>");
        DialogHelper.showWarn(getName(), NbBundle.getMessage(FormuleNodeActionImport.class, "formuleImport.DoublonFound", values));
        if (toImports.isEmpty()) {
          DialogHelper.showInfo(getName(), NbBundle.getMessage(FormuleNodeActionImport.class, "formuleImport.NoVariable"));
          return;
        }
      }
      final Object[] toImportAsArray = toImports.toArray();
      CheckBoxList toImportList = new CheckBoxList(toImportAsArray);
      toImportList.setCellRenderer(new CtuluCellTextRenderer() {
        @Override
        protected void setValue(Object _value) {
          if (_value == null) {
            super.setValue(_value);
          } else {
            FormuleParametersExpr expr = (FormuleParametersExpr) _value;
            super.setValue(expr.getNom() + " = " + expr.getFormule());
          }
        }
      });
      toImportList.setSelectedObjects(toImportAsArray);
      JPanel pn = new JPanel(new BorderLayout());
      pn.add(new JLabel(NbBundle.getMessage(FormuleNodeActionImport.class, "formuleImport.ChooseVariable")), BorderLayout.NORTH);
      pn.add(new JScrollPane(toImportList));
      boolean ok = DialogHelper.showQuestion(getName(), pn);
      if (ok) {
        Object[] values = toImportList.getCheckBoxListSelectedValues();
        if (values != null && values.length > 0) {
          Node rootNode = FormuleContentChildFactory.getRootNode(activatedNodes[0]);
          Node[] toAdd = new Node[values.length];
          for (int i = 0; i < toAdd.length; i++) {
            FormuleNodeParametersExpr newNode = new FormuleNodeParametersExpr((FormuleParametersExpr) values[i], true);
            toAdd[i] = newNode;
          }
          rootNode.getChildren().add(toAdd);
        }
      }

    }


  }
}
