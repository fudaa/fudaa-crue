/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.planimetry;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.fudaa.ebli.controle.BMolette;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;

/**
 *
 * @author Frederic Deniger
 */
public class ReportBrancheSizeConfigurer {

  JComponent createConfigurer(final PlanimetryVisuPanel panel) {
    final BMolette molette = new BMolette();
    molette.setToolTipText(org.openide.util.NbBundle.getMessage(ReportBrancheSizeConfigurer.class, "TailleBrancheVector.Tooltip"));
    molette.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
          final ReportPlanimetryBrancheConfig brancheConfigurationExtra = (ReportPlanimetryBrancheConfig) panel.getPlanimetryController().getCqBranche().modeleDonnees().getBrancheConfigurationExtra();
          brancheConfigurationExtra.initFlecheRatio();
        }
      }
    });
    //pour mettre à jour le toolitp:
    molette.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseEntered(MouseEvent e) {
        final ReportPlanimetryBrancheConfig brancheConfigurationExtra = (ReportPlanimetryBrancheConfig) panel.getPlanimetryController().getCqBranche().modeleDonnees().getBrancheConfigurationExtra();
        if (brancheConfigurationExtra.getData().getArrowVariable() == null) {
          molette.setToolTipText(org.openide.util.NbBundle.getMessage(ReportBrancheSizeConfigurer.class, "TailleBrancheVector.Tooltip"));
        } else {
          molette.setToolTipText(org.openide.util.NbBundle.getMessage(ReportBrancheSizeConfigurer.class, "TailleBrancheVector.SelectedTooltip", brancheConfigurationExtra.getData().getArrowVariable().getVariableName()));
        }

      }
    });

    molette.setOrientation(BMolette.HORIZONTAL);
    molette.setPreferredSize(new Dimension(60, 20));
    molette.setMaximumSize(new Dimension(60, 20));
    molette.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        final ReportPlanimetryBrancheConfig brancheConfigurationExtra = (ReportPlanimetryBrancheConfig) panel.getPlanimetryController().getCqBranche().modeleDonnees().getBrancheConfigurationExtra();
        double multi = 1.02;
        if (molette.getDirection() == BMolette.MOINS) {
          multi = 1 / multi;
        }
        brancheConfigurationExtra.getData().setArrowRatio(brancheConfigurationExtra.getData().getArrowRatio() * multi);
        panel.getVueCalque().repaint(0);

      }
    });
    return molette;
  }
}
