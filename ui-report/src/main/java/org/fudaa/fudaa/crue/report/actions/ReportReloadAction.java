package org.fudaa.fudaa.crue.report.actions;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.fudaa.fudaa.crue.report.service.ReportSaver;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
id = "org.fudaa.fudaa.crue.report.actions.ReportReloadAction")
@ActionRegistration(displayName = "#CTL_ReportReloadAction")
@ActionReferences({
  @ActionReference(path = "Actions/Report", position = 2)
})
public final class ReportReloadAction extends AbstractReportAction {

  final PerspectiveServiceReport perspectiveServiceReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);

  public ReportReloadAction() {
    super(false);
    putValue(Action.NAME, NbBundle.getMessage(ReportReloadAction.class, "CTL_ReportReloadAction"));
    perspectiveServiceReport.addDirtyListener(evt -> updateEnableState());
    updateEnableState();
  }

  @Override
  protected boolean getEnableState() {
    return super.getEnableState() && perspectiveServiceReport != null && perspectiveServiceReport.isDirty();
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ReportReloadAction();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    boolean ok = DialogHelper.showQuestion((String) getValue(Action.NAME), org.openide.util.NbBundle.getMessage(ReportReloadAction.class, "ReloadAllWarning.Message"));
    if (ok) {
      new ReportSaver().reload();
    }
  }
}
