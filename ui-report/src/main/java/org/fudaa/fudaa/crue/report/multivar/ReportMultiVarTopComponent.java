package org.fudaa.fudaa.crue.report.multivar;

import com.memoire.bu.BuGridLayout;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Set;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.result.OrdResExtractor;
import org.fudaa.dodico.crue.projet.report.ReportMultiVarConfig;
import org.fudaa.dodico.crue.projet.report.data.ReportKeyContract;
import org.fudaa.dodico.crue.projet.report.data.ReportRunEmhKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableEmhKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableHelper;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.courbe.EGAxeVerticalPersist;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import static org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder.getDialogHelpCtxId;
import org.fudaa.fudaa.crue.loi.res.CourbesUiResController;
import org.fudaa.fudaa.crue.report.AbstractReportTimeViewTopComponent;
import org.fudaa.fudaa.crue.report.data.ReportVariableCellRenderer;
import org.fudaa.fudaa.crue.report.export.ReportMulitVarExportAction;
import org.fudaa.fudaa.crue.report.helper.ByRunSelector;
import org.fudaa.fudaa.crue.report.helper.ByRunSelector.Line;
import org.fudaa.fudaa.crue.report.helper.ChooseEMHByTypeHelper;
import org.fudaa.fudaa.crue.report.service.ReportFormuleService;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.report//ReportMultiVarTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = ReportMultiVarTopComponent.TOPCOMPONENT_ID,
        persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ReportMultiVarTopComponent.MODE, openAtStartup = false, position = 1)
public final class ReportMultiVarTopComponent extends AbstractReportTimeViewTopComponent<ReportMultiVarConfig, ReportMultiVarGrapheBuilder> {

  public static final String MODE = "report-multivar";
  public static final String TOPCOMPONENT_ID = "ReportMultiVarTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  private final ReportFormuleService reportFormuleService = Lookup.getDefault().lookup(ReportFormuleService.class);
  private final ReportResultProviderService reportResultProviderService = Lookup.getDefault().lookup(ReportResultProviderService.class);

  public ReportMultiVarTopComponent() {
    setName(NbBundle.getMessage(ReportMultiVarTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ReportMultiVarTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueMultiVariables", PerspectiveEnum.REPORT);
  }

  @Override
  protected ReportMultiVarGrapheBuilder createBuilder() {
    return new ReportMultiVarGrapheBuilder(profilUiController, loiLegendManager);
  }

  @Override
  protected void buildComponents() {
    initComponents();
    final JPanel pnTable = new JPanel(new BorderLayout(5, 5));
    pnTable.add(profilUiController.getTableGraphePanel());
    profilUiController.installComboxSelector();
    final JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, loiLegendManager.getPanel(), pnTable);
    splitPane.setDividerLocation(550);
    splitPane.setContinuousLayout(true);
    splitPane.setOneTouchExpandable(true);
    add(splitPane);
  }

  @Override
  public void alternatifRunChanged() {
    final Set<ReportRunKey> reportRunContents = reportRunAlternatifService.getAvailableRunKeys();
    boolean modified = false;
    for (final Iterator<ReportRunEmhKey> it = content.getReportRunEmhs().iterator(); it.hasNext();) {
      final ReportRunEmhKey key = it.next();
      if (key.getReportRunKey().isAlternatifRun() && !reportRunContents.contains(key.getReportRunKey())) {
        modified = true;
        it.remove();
      }
    }
    final Set<ReportRunVariableEmhKey> keySet = content.getCourbeconfigs().keySet();
    for (final Iterator<ReportRunVariableEmhKey> it = keySet.iterator(); it.hasNext();) {
      final ReportRunVariableEmhKey reportRunVariableEmhKey = it.next();
      final ReportRunKey reportRunKey = reportRunVariableEmhKey.getReportRunKey();
      if (reportRunKey.isAlternatifRun() && !reportRunContents.contains(reportRunKey)) {
        it.remove();
        modified = true;
      }

    }
    if (modified) {
      applyChangeInContent(false);
      setModified(true);
    }
  }

  @Override
  protected CourbesUiResController createCourbesUiController() {
    final CourbesUiResController res = new CourbesUiResController();
    res.addToolbarAction(new ReportMulitVarExportAction(this));
    return res;
  }

  @Override
  protected void saveCourbeConfig(final EGCourbePersist persitUiConfig, final ReportKeyContract key) {
    content.getCourbeconfigs().put((ReportRunVariableEmhKey) key, persitUiConfig);
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables

  @Override
  protected void componentOpenedHandler() {
  }

  @Override
  protected void componentClosedDefinitlyHandler() {
  }

  void writeProperties(final java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
    p.setProperty("version", "1.0");
  }

  void readProperties(final java.util.Properties p) {
  }

  @Override
  public List<EbliActionInterface> getMainActions() {
    final EbliActionSimple addEMHAction = new EbliActionSimple(NbBundle.getMessage(ReportMultiVarTopComponent.class, "AddEmh.ButtonName"), null, "ADD_EMH") {
      @Override
      public void actionPerformed(final ActionEvent actionEvent) {
        addEMH();
      }
    };
    final EbliActionSimple chooseVariables = new EbliActionSimple(NbBundle.getMessage(ReportMultiVarTopComponent.class, "ChooseVariable.ButtonName"), null,
            "CHOOSE_VARIABLES") {
              @Override
              public void actionPerformed(final ActionEvent actionEvent) {
                chooseVariable();
              }
            };
    final EbliActionSimple chooseRuns = new EbliActionSimple(NbBundle.getMessage(ReportMultiVarTopComponent.class, "ChooseRun.ButtonName"), null,
            "CHOOSE_RUNS") {
              @Override
              public void actionPerformed(final ActionEvent actionEvent) {
                chooseRuns();
              }
            };
    return Arrays.asList(addEMHAction, chooseVariables, chooseRuns, createConfigExternAction());

  }

  @Override
  public List<ReportVariableKey> getTitleAvailableVariables() {
    return Collections.emptyList();
  }

  private void addEMH() {
    final List<String> emhs = content.getEmhs();
    final List<String> selectedEMH = new ChooseEMHByTypeHelper().chooseEMH(emhs);
    if (selectedEMH != null) {
      content.setEMHAndAddRunCourant(selectedEMH, reportResultProviderService);

      propagateChange();
    }
  }

  @Override
  public ReportMultiVarConfig getReportConfig() {
    return content;
  }

  private void chooseRuns() {
    final List<String> emhs = content.getEmhs();
    final int nb = emhs.size();
    if (nb == 0) {
      DialogHelper.showError(NbBundle.getMessage(ReportMultiVarTopComponent.class, "ChooseRun.NoEMHSelected"));
      return;
    }
    final ByRunSelector selector = new ByRunSelector();
    final List<Line> createLines = selector.createLines(nb);
    final JPanel pn = new JPanel(new BuGridLayout(1 + createLines.get(0).getNbComponents(), 5, 5));
    final Map<String, List<ReportRunKey>> byEmh = ReportRunVariableHelper.getByEmh(content.getReportRunEmhs());
    for (int i = 0; i < nb; i++) {
      final String emhName = emhs.get(i);
      pn.add(new JLabel(emhName));
      createLines.get(i).addToPanel(pn);
      final List<ReportRunKey> selected = byEmh.get(emhName);
      if (selected != null) {
        createLines.get(i).updateCheckBoxes(new HashSet<>(selected));
      }
    }    
    SysdocUrlBuilder.installHelpShortcut(pn,getDialogHelpCtxId("vueMultiVariable_ConfigurationRuns", PerspectiveEnum.REPORT));
    pn.add(SysdocUrlBuilder.createButtonHelpNotionDialog("Runs"), BorderLayout.SOUTH);    
    final boolean accepted = DialogHelper.showQuestionOkCancel(NbBundle.getMessage(ReportMultiVarTopComponent.class, "ChooseRun.ButtonName"), pn);
    if (accepted) {
      content.getReportRunEmhs().clear();
      for (int i = 0; i < nb; i++) {
        final String emhName = emhs.get(i);
        final List<ReportRunKey> selectedKeys = createLines.get(i).getSelectedKeys();
        for (final ReportRunKey reportRunKey : selectedKeys) {
          content.getReportRunEmhs().add(new ReportRunEmhKey(reportRunKey, emhName));
        }
      }
      propagateChange();
    }
  }

  public void chooseVariable() {
    final List<String> horizontalVariables = getChoosableVariables();
    //on ajoute la valeur null.
    final List<String> selectedVariables = new ArrayList<>(horizontalVariables);
    selectedVariables.add(0, null);
    final String[] selectedVariablesArray = selectedVariables.toArray(new String[0]);
    final ReportVariableCellRenderer cellRenderer = new ReportVariableCellRenderer();
    final JPanel pn = new JPanel(new BuGridLayout(2, 5, 5));
    pn.add(new JLabel(NbBundle.getMessage(ReportMultiVarTopComponent.class, "ChooseVariable.LabelHorizontal")));
    final JComboBox cbHorizontal = new JComboBox(horizontalVariables.toArray(new String[0]));
    cbHorizontal.setRenderer(cellRenderer);
    pn.add(cbHorizontal);
    cbHorizontal.setSelectedItem(content.getHorizontalVar());
    pn.add(new JLabel(NbBundle.getMessage(ReportMultiVarTopComponent.class, "ChooseVariable.LabelFirst")));
    final JComboBox cb1 = new JComboBox(selectedVariablesArray);
    cb1.setRenderer(cellRenderer);
    pn.add(cb1);
    cb1.setSelectedItem(null);
    pn.add(new JLabel(NbBundle.getMessage(ReportMultiVarTopComponent.class, "ChooseVariable.LabelSecond")));
    final JComboBox cb2 = new JComboBox(selectedVariablesArray);
    cb2.setRenderer(cellRenderer);
    cb2.setSelectedItem(null);
    pn.add(cb2);
    final List<String> variables = content.getVariables();
    if (variables.size() > 0) {
      final String var = variables.get(0);
      cb1.setSelectedItem(var);

    }
    if (variables.size() > 1) {
      final String var = variables.get(1);
      cb2.setSelectedItem(var);
    }
    
    final boolean accepted = DialogHelper.showQuestionOkCancel(NbBundle.getMessage(ReportMultiVarTopComponent.class, "ChooseVariable.ButtonName"), pn);
    if (accepted) {
      content.setHorizontalVar((String) cbHorizontal.getSelectedItem());
      content.getVariables().clear();
      String v1 = (String) cb1.getSelectedItem();
      final String v2 = (String) cb2.getSelectedItem();
      if (v1 == null && v2 != null) {
        v1 = v2;
      }
      if (v1 != null) {
        content.getVariables().add(v1);
      }
      if (v2 != null) {
        content.getVariables().add(v2);
      }
      EGAxeVerticalPersist axeV1 = null;
      if (v1 != null) {
        axeV1 = createAxeVerticalConfig(v1);
        axeV1.setDroite(false);
      }
      if (v2 != null) {
        final EGAxeVerticalPersist axeV2 = createAxeVerticalConfig(v2);
        if (axeV2 != axeV1) {
          axeV2.setDroite(true);
        }

      }
      propagateChange();

    }
  }

  public List<String> getChoosableVariables() throws MissingResourceException {
    final List<String> emhs = content.getEmhs();
    final EnumCatEMH usedCat = ChooseEMHByTypeHelper.getUsedCat(reportService, emhs);
    if (CollectionUtils.isNotEmpty(emhs)
            && usedCat == null) {
      DialogHelper.showWarn(org.openide.util.NbBundle.getMessage(ChooseEMHByTypeHelper.class, "UsedEMHNotFound.warning"));
    }
    final OrdResExtractor extractor = new OrdResExtractor(getScenario().getOrdResScenario());
    return extractor.getSelectableVariables(usedCat, reportFormuleService.getVariablesKeys());
  }
}
