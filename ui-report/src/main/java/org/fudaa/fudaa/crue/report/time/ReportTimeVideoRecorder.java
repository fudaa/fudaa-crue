/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.time;

import com.memoire.bu.BuResource;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JButton;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.report.service.ReportHelper;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 * La classe de base pour les enregistrements videos.
 *
 * @author Frederic Deniger
 */
public final class ReportTimeVideoRecorder implements PropertyChangeListener {
  
  final Map<String, ReportTimeVideoRecorderItem> itemsByTcId = new HashMap<>();
  
  public ReportTimeVideoRecorder() {
    register();
  }
  private JButton btVideo;
  
  public JButton getBtVideo() {
    if (btVideo == null) {
      btVideo = ReportTimeStepSliderPanel.create(null);
      btVideo.setIcon(BuResource.BU.getIcon("crystal_video"));
      btVideo.setToolTipText(NbBundle.getMessage(ReportTimeVideoRecorder.class, "video.ButtonTooltip"));
    }
    return btVideo;
  }
  
  public void updateBtVideo() {
    if (btVideo != null) {
      boolean recorded = isVideoRecorded();
      btVideo.setIcon(recorded ? ReportHelper.getIcon("crystal_video_activated.png") : BuResource.BU.getIcon("crystal_video"));
    }
  }
  
  public void edit(int nbFrames) {
    List<ReportTimeVideoRecorderItem> items = new ArrayList<>();
    final Collection<ReportTimeVideoRecorderItem> values = itemsByTcId.values();
    for (ReportTimeVideoRecorderItem item : values) {
      ReportTimeVideoRecorderItem cloned = new ReportTimeVideoRecorderItem();
      cloned.setTopComponent(item.getTopComponent());
      cloned.setNbFrames(nbFrames);
      cloned.setOutput(item.getOutput().copy());
      cloned.setActivated(item.isActivated());
      items.add(cloned);
    }
    Set<TopComponent> opened = WindowManager.getDefault().getRegistry().getOpened();
    for (TopComponent topComponent : opened) {
      if (topComponent instanceof CtuluImageProducer) {
        String id = WindowManager.getDefault().findTopComponentID(topComponent);
        if (!itemsByTcId.containsKey(id)) {
          ReportTimeVideoRecorderItem item = new ReportTimeVideoRecorderItem();
          item.setNbFrames(nbFrames);
          item.setTopComponent(topComponent);
          items.add(item);
        }
      }
    }
    ReportTimeVideoRecorderEditor editor = new ReportTimeVideoRecorderEditor();
    if (editor.edit(items)) {
      itemsByTcId.clear();
      for (ReportTimeVideoRecorderItem item : items) {
        if (item.getOutput() != null) {
          String id = WindowManager.getDefault().findTopComponentID(item.getTopComponent());
          itemsByTcId.put(id, item);
        }
      }
    }
    
  }
  
  private void topComponentClosed(TopComponent topComponent) {
    String id = WindowManager.getDefault().findTopComponentID(topComponent);
    if (itemsByTcId.containsKey(id)) {
      itemsByTcId.remove(id);
      updateBtVideo();
    }
  }
  
  public void register() {
    WindowManager.getDefault().getRegistry().addPropertyChangeListener(this);
  }
  
  public void unregister() {
    WindowManager.getDefault().getRegistry().removePropertyChangeListener(this);
  }
  
  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (TopComponent.Registry.PROP_TC_CLOSED.equals(evt.getPropertyName())) {
      topComponentClosed((TopComponent) evt.getNewValue());
    }
  }
  private volatile boolean started = false;
  
  public void start(int nbImages) {
    started = true;
    Collection<ReportTimeVideoRecorderItem> values = itemsByTcId.values();
    for (ReportTimeVideoRecorderItem item : values) {
      if (item.isActivated()) {
        CtuluImageProducer imageProducer = (CtuluImageProducer) item.getTopComponent();
        Dimension dim = imageProducer.getDefaultImageDimension();
        item.getOutput().setWarnIfFileExists(false);
        item.getOutput().setHeight(dim.height);
        item.getOutput().setWidth(dim.width);
        item.getOutput().init(imageProducer, item.getTopComponent(), nbImages);
      }
    }
    nextDone();
  }
  
  public void nextDone() {
    Collection<ReportTimeVideoRecorderItem> values = itemsByTcId.values();
    for (ReportTimeVideoRecorderItem item : values) {
      if (item.isActivated()) {
        CtuluImageProducer imageProducer = (CtuluImageProducer) item.getTopComponent();
        item.getOutput().appendFrame(imageProducer.produceImage(null), false);
      }
    }
  }
  
  public void end() {
    if (!started) {
      return;
    }
    Collection<ReportTimeVideoRecorderItem> values = itemsByTcId.values();
    for (ReportTimeVideoRecorderItem item : values) {
      if (item.isActivated()) {
        item.getOutput().finish();
      }
    }
    started = false;
  }
  
  boolean isVideoRecorded() {
    if (!itemsByTcId.isEmpty()) {
      for (ReportTimeVideoRecorderItem item : itemsByTcId.values()) {
        if (item != null && item.getOutput() != null && item.getOutput().isActivated()) {
          return true;
        }
      }
    }
    return false;
  }
  
  boolean checkFiles() {
    Collection<ReportTimeVideoRecorderItem> values = itemsByTcId.values();
    List<String> overtwrittenFiles = new ArrayList<>();
    for (ReportTimeVideoRecorderItem item : values) {
      if (item.isActivated()) {
        File destFile = item.getOutput().getDestFile();
        if (destFile.exists()) {
          overtwrittenFiles.add(destFile.getAbsolutePath());
        }
      }
    }
    if (!overtwrittenFiles.isEmpty()) {
      String message = org.openide.util.NbBundle.getMessage(ReportTimeVideoRecorder.class, "VideoFilesWillBeOverwritten.Message", StringUtils.join(overtwrittenFiles, "\n"));
      return DialogHelper.showQuestion(message);
    }
    return true;
  }
}
