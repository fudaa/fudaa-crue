/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.ReportIndexReader;
import org.fudaa.dodico.crue.projet.report.ReportViewLineInfo;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.report.view.persist.ReportConfigLoaderProcessor;
import org.fudaa.fudaa.crue.report.view.persist.ReportConfigSaverProcessor;
import org.fudaa.fudaa.crue.report.view.persist.ReportIndexReaderSaverUI;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportViewsSave {

  final ReportViewsService reportViewsService = Lookup.getDefault().lookup(ReportViewsService.class);

  public void reloadIndex() {
    reportViewsService.reloadAll();
    loadIndex();
  }

  public void loadIndex() {
    final EMHProjet projectLoaded = reportViewsService.getProjectLoaded();
    final CtuluLogGroup logs = new CtuluLogGroup(null);
    logs.setDescription(org.openide.util.NbBundle.getMessage(ReportViewsService.class, "VueConfigLoad.DisplayLog"));
    final File dirOfRapports = projectLoaded.getInfos().getDirOfRapports();
    final List<ReportViewsByType> configViews = reportViewsService.getConfigViews();
    boolean modified = false;
    for (final ReportViewsByType viewType : configViews) {
      final File folder = viewType.getFolder(dirOfRapports);
      final File index = new File(folder, ReportIndexReader.INDEX_FILENAME);
      if (index.exists()) {
        final ReportIndexReaderSaverUI saver = new ReportIndexReaderSaverUI();
        final CrueIOResu<List<ReportViewLineInfo>> ioResu = saver.read(index);
        final List<ReportViewLineInfo> read = ioResu.getMetier();
        if (read != null) {
          final List<ReportViewNode> nodes = new ArrayList<>();
          for (final ReportViewLineInfo reportViewLineInfo : read) {
            if (reportViewLineInfo.getFilename() == null) {
              Logger.getLogger(ReportViewsSave.class.getName()).log(Level.SEVERE, "no file for {0}", reportViewLineInfo.getNom());
              continue;
            }
            final File lineFile = new File(folder, reportViewLineInfo.getFilename());
            if (!lineFile.exists()) {
              ioResu.getAnalyse().addError(NbBundle.getMessage(ReportViewsSave.class, "OpenViewConfigFile.FileNotExist", reportViewLineInfo.getNom(),
                      lineFile.getAbsolutePath()));
              viewType.setModified(true);
              modified = true;
            } else {
              final ReportViewLine line = new ReportViewLine(viewType.getType(), reportViewLineInfo, viewType.getTopComponentClass(), viewType.
                      getTopComponentMode(), viewType.getTopComponentId());
              final ReportViewNode node = new ReportViewNode(line);
              line.setModified(false);
              nodes.add(node);
            }
          }
          final Node[] currentsNode = viewType.getChildren().getNodes();
          if (currentsNode.length > 0) {
            viewType.getChildren().remove(currentsNode);
          }
          viewType.getChildren().add(nodes.toArray(new AbstractNode[0]));
        }
        final CtuluLog analyse = ioResu.getAnalyse();
        if (analyse != null && analyse.isNotEmpty()) {
          logs.addLog(analyse);
        }

      }

    }
    if (modified) {
      reportViewsService.modificationDone();
    }
    if (logs.containsSomething()) {
      LogsDisplayer.displayError(logs, logs.getDescription());
    }
  }

  public ReportConfigContrat load(final ReportViewLine line, final ReportContentType type) {
    final File dirOfRapports = reportViewsService.getProjectLoaded().getInfos().getDirOfRapports();
    final File configFile = new File(new File(dirOfRapports, type.getFolderName()), line
            .getLineInformation().getFilename());
    if (!configFile.exists()) {
      DialogHelper.showError(NbBundle.getMessage(ReportViewsService.class, "OpenViewConfigFile.FileNotExist", line.getLineInformation().getNom(), configFile.
              getAbsolutePath()));
      return null;
    }

    final ReportConfigLoaderProcessor loader = new ReportConfigLoaderProcessor(configFile);
    final String actionTitle = NbBundle.getMessage(ReportViewsSave.class, "Load.ActionName", line.getLineInformation().getNom());
    final CrueIOResu<ReportConfigContrat> result = CrueProgressUtils.showProgressDialogAndRun(loader, actionTitle);
    if (result == null) {
      return null;
    }
    if (result.getAnalyse().isNotEmpty()) {
      LogsDisplayer.displayError(result.getAnalyse(), actionTitle);
    }
    return result.getMetier();

  }

  public void save() {
    final ReportConfigSaverProcessor saveProcessor = new ReportConfigSaverProcessor();
    final String actionName = org.openide.util.NbBundle.getMessage(ReportViewsSave.class, "SaveOperation.DisplayName");
    final CtuluLogGroup logs = CrueProgressUtils.showProgressDialogAndRun(saveProcessor, actionName);
    if (logs.containsSomething()) {
      LogsDisplayer.displayError(logs, actionName);
    }
    if (logs.containsFatalError()) {
      return;
    }
    //on nettoie les caractères modifiés:
    for (final ReportViewsByType viewType : reportViewsService.getConfigViews()) {
      final AbstractNode viewNode = viewType.getNode();
      viewType.setModified(false);
      final Node[] nodes = viewNode.getChildren().getNodes();
      for (final Node node : nodes) {
        final ReportViewLine reportViewLine = node.getLookup().lookup(ReportViewLine.class);
        if (reportViewLine.getTopComponent() != null) {
          reportViewLine.getTopComponent().getTopComponent().setModified(false);
        }
        ((ReportViewNode) node).updateDisplayName();
        reportViewLine.setModified(false);

      }

    }

  }
}
