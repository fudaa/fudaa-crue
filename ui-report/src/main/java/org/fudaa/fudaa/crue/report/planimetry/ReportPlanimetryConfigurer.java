/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.planimetry;

import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryExtraContainer;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ReportPlanimetryConfigurer {

  private final ReportPlanimetryBrancheConfig brancheConfigurationExtra;
  private final ReportPlanimetryNodeConfig nodeConfigurationExtra;
  private final ReportPlanimetryCasierConfig casierConfigurationExtra;
  private final ReportPlanimetrySectionConfig sectionConfigurationExtra;
  private final ReportPlanimetryTraceConfig traceConfigurationExtra;
  private final ReportPlanimetryCondLimitConfig conLimConfigurationExtra;
  final ReportResultProviderService reportResultProviderService = Lookup.getDefault().lookup(ReportResultProviderService.class);

  public ReportPlanimetryConfigurer() {
    brancheConfigurationExtra = new ReportPlanimetryBrancheConfig(reportResultProviderService);
    nodeConfigurationExtra = new ReportPlanimetryNodeConfig(reportResultProviderService);
    casierConfigurationExtra = new ReportPlanimetryCasierConfig(reportResultProviderService);
    sectionConfigurationExtra = new ReportPlanimetrySectionConfig(reportResultProviderService);
    traceConfigurationExtra = new ReportPlanimetryTraceConfig(reportResultProviderService);
    conLimConfigurationExtra = new ReportPlanimetryCondLimitConfig();
  }

  public ReportPlanimetryExtraContainer getDatas() {
    ReportPlanimetryExtraContainer res = new ReportPlanimetryExtraContainer(false);
    res.setBrancheConfigurationExtraData(brancheConfigurationExtra.getData());
    res.setCasierConfigurationExtraData(casierConfigurationExtra.getData());
    res.setNodeConfigurationExtraData(nodeConfigurationExtra.getData());
    res.setSectionConfigurationExtraData(sectionConfigurationExtra.getData());
    res.setTraceConfigurationExtraData(traceConfigurationExtra.getData());
    return res;
  }

  public void initFrom(ReportPlanimetryExtraContainer in) {
    brancheConfigurationExtra.setData(in.getBrancheConfigurationExtraData());
    nodeConfigurationExtra.setData(in.getNodeConfigurationExtraData());
    casierConfigurationExtra.setData(in.getCasierConfigurationExtraData());
    sectionConfigurationExtra.setData(in.getSectionConfigurationExtraData());
    traceConfigurationExtra.setData(in.getTraceConfigurationExtraData());
  }

  public ReportPlanimetryTraceConfig getTraceConfigurationExtra() {
    return traceConfigurationExtra;
  }

  public ReportPlanimetryBrancheConfig getBrancheConfigurationExtra() {
    return brancheConfigurationExtra;
  }

  public ReportPlanimetryNodeConfig getNodeConfigurationExtra() {
    return nodeConfigurationExtra;
  }

  public ReportPlanimetryCasierConfig getCasierConfigurationExtra() {
    return casierConfigurationExtra;
  }

  public ReportPlanimetrySectionConfig getSectionConfigurationExtra() {
    return sectionConfigurationExtra;
  }

  public ReportPlanimetryCondLimitConfig getConLimConfigurationExtra() {
    return conLimConfigurationExtra;
  }
}
