package org.fudaa.fudaa.crue.report;

import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.dodico.crue.metier.emh.Calc;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.ReportViewLineInfo;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.TopComponentHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.planimetry.PlanimetryCalqueState;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanelBuilder;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.report.actions.SaveTemplateAction;
import org.fudaa.fudaa.crue.report.contrat.ReportViewServiceContrat;
import org.fudaa.fudaa.crue.report.node.ReportNodeEMHFactory;
import org.fudaa.fudaa.crue.report.persist.ReportPlanimetryConfig;
import org.fudaa.fudaa.crue.report.planimetry.ReportPlanimetryCondLimitConfig;
import org.fudaa.fudaa.crue.report.planimetry.ReportPlanimetryConfigUI;
import org.fudaa.fudaa.crue.report.planimetry.ReportPlanimetryConfigurer;
import org.fudaa.fudaa.crue.report.service.ReportSelectedEMHService;
import org.fudaa.fudaa.crue.report.service.ReportTemplateFileProvider;
import org.fudaa.fudaa.crue.report.service.ReportVisuPanelService;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.*;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.util.List;
import java.util.*;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.report//ReportVisualTopComponent//EN",
    autostore = false)
@TopComponent.Description(preferredID = ReportVisualTopComponent.TOPCOMPONENT_ID,
    iconBase = "org/fudaa/fudaa/crue/report/rond-orange_16.png",
    persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ReportVisualTopComponent.MODE, openAtStartup = false, position = 4)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.report.ReportVisualTopComponent")
@ActionReference(path = "Menu/Window/Report", position = 1)
@TopComponent.OpenActionRegistration(displayName = ReportVisualTopComponent.TOPCOMPONENT_ACTION_DISPLAYNAME,
    preferredID = ReportVisualTopComponent.TOPCOMPONENT_ID)
public final class ReportVisualTopComponent extends AbstractReportTimeTopComponent implements LookupListener, ExplorerManager.Provider,
    CtuluImageProducer, ReportTopComponentConfigurable<ReportPlanimetryConfig>, PropertyChangeListener {
  public static final String TOPCOMPONENT_ID = "ReportVisualTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  public static final String MODE = "report-editor";
  private final ExplorerManager em = new ExplorerManager();
  private final transient ReportPlanimetryConfigurer reportPlanimetryConfigurer = new ReportPlanimetryConfigurer();
  private final Set<String> ignoreProperties = new TreeSet<>(Arrays.asList("versEcran", "versReel"));
  private final ReportViewServiceContrat reportViewServiceContrat = Lookup.getDefault().lookup(ReportViewServiceContrat.class);
  private final ReportSelectedEMHService reportSelectedEMHService = Lookup.getDefault().lookup(ReportSelectedEMHService.class);
  private transient ReportPlanimetryConfig reportPlanimetryConfig = new ReportPlanimetryConfig();
  private Map<String, Long> calcByName;
  private transient VisuConfiguration initConfiguration;
  private final transient VisuChangerListener modificationChangeListener = new VisuChangerListener();
  private transient SelectedEMHsLookupListener selectedEMHsLookupListener;
  private transient VisuPanelLookupListener visuLookupListener;
  private final transient ReportVisuPanelService reportVisuService = Lookup.getDefault().lookup(ReportVisuPanelService.class);
  private transient Lookup.Result<PlanimetryVisuPanel> resultatVisuPanel;
  private transient Lookup.Result<Long> resultatEMHsSelected;
  private boolean runOpenedAndZoomSet = false;
  private transient ReportViewLineInfo info;
  private PlanimetryVisuPanel panel;
  private transient LayerSelectionListener selectionListener;

  public ReportVisualTopComponent() {
    initComponents();
    setDefaultName();
    setToolTipText(NbBundle.getMessage(ReportVisualTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    TopComponentHelper.setMainViewProperties(this);
    final ActionMap map = this.getActionMap();
    associateLookup(ExplorerUtils.createLookup(em, map));
  }

  @Override
  public Action[] getActions() {
    return TopComponentHelper.getMainViewActions();
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vuePlanimetrie", PerspectiveEnum.REPORT);
  }

  @Override
  public ReportPlanimetryConfig getReportConfig() {
    return reportPlanimetryConfig;
  }

  @Override
  public boolean isClosable() {
    return false;
  }

  @Override
  public void rangeChanged() {
    setReportConfig(getReportConfig(), false);
  }

  @Override
  public void setReportConfig(final ReportPlanimetryConfig newConfig, final boolean fromTemplate) {
    super.updating = true;
    this.reportPlanimetryConfig = newConfig;
    try {
      this.reportPlanimetryConfigurer.initFrom(newConfig.getReportPlanimetryExtraContainer());
      if (panel != null) {
        panel.getPlanimetryController().apply(reportPlanimetryConfig.getVisuConfiguration());
        if (reportPlanimetryConfig.getLegendProperties() != null) {
          panel.getCqLegend().initFrom(reportPlanimetryConfig.getLegendProperties());
        }
        panel.getPlanimetryController().applyExternLayerVisibilty(reportPlanimetryConfig.getVisibilityByExternLayerName());
        new ReportPlanimetryConfigUI(panel.getPlanimetryController()).updatePalettes();
        panel.getVueCalque().repaint(0);
      }
      final ReportLayersTreeComponent layersTree = (ReportLayersTreeComponent) WindowManager.getDefault().findTopComponent(
          ReportLayersTreeComponent.TOPCOMPONENT_ID);
      if (layersTree != null) {
        layersTree.setModified(false);
      }
    } finally {
      clearModificationState();
      super.updating = false;
    }
  }

  @Override
  public void setReportInfo(final ReportViewLineInfo info) {
    this.info = info;
  }

  @Override
  public AbstractReportTopComponent getTopComponent() {
    return this;
  }

  @Override
  public void updateFrameName() {
    setName(info == null ? "" : info.getNom());
    updateTopComponentName();
  }

  public ReportPlanimetryConfigurer getReportPlanimetryConfigurer() {
    return reportPlanimetryConfigurer;
  }

  private void updateConfigurer() {
    if (panel != null) {
      final PlanimetryController planimetryController = panel.getPlanimetryController();

      planimetryController.getCqBranche().modeleDonnees().setBrancheConfigurationExtra(reportPlanimetryConfigurer.getBrancheConfigurationExtra());
      planimetryController.getCqCasier().modeleDonnees().setCasierConfigurationExtra(reportPlanimetryConfigurer.getCasierConfigurationExtra());
      planimetryController.getCqNoeud().modeleDonnees().setNodeConfigurationExtra(reportPlanimetryConfigurer.getNodeConfigurationExtra());
      planimetryController.getCqSection().modeleDonnees().setSectionConfigurationExtra(reportPlanimetryConfigurer.getSectionConfigurationExtra());
      planimetryController.getCqTrace().modeleDonnees().setTraceConfigurationExtra(reportPlanimetryConfigurer.getTraceConfigurationExtra());
      planimetryController.getcondLimiteController().getLayer().modeleDonnees().
          setCondLimitConfigurationExtra(new ReportPlanimetryCondLimitConfig());
      new ReportPlanimetryConfigUI(planimetryController).updatePalettes();
      panel.getVueCalque().repaint(0);
    }
  }

  @Override
  public BufferedImage produceImage(final Map params) {
    return panel.produceImage(params);
  }

  @Override
  public BufferedImage produceImage(final int w, final int h, final Map params) {
    return panel.produceImage(w, h, params);
  }

  @Override
  public Dimension getDefaultImageDimension() {
    return panel.getDefaultImageDimension();
  }

  @Override
  protected void componentActivated() {
    super.componentActivated();
    ExplorerUtils.activateActions(em, true);
    if (panel != null) {
      panel.requestFocusInWindow();
    }
  }

  @Override
  protected void componentDeactivated() {
    super.componentDeactivated();
    ExplorerUtils.activateActions(em, false);
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return em;
  }

  @Override
  public void selectedTimeChanged() {
    if (reportService.getRunCourant() != null && panel != null) {
      final ResultatTimeKey selectedTime = getSelectedTime();
      buildCalcMapIfNeeded();
      Long calcSelected = null;
      if (selectedTime != null) {
        calcSelected = calcByName.get(selectedTime.getNomCalcul());
      }
      panel.getPlanimetryController().setEditedCalcul(calcSelected);
    }
  }

  @Override
  public void timeFormatChanged() {
    //nothing to do
  }

  @Override
  public void setEditable(final boolean b) {
    this.editable = b;
    final PlanimetryVisuPanel planimetryVisuPanel = reportVisuService.getPlanimetryVisuPanel();
    if (planimetryVisuPanel != null) {
      planimetryVisuPanel.getPlanimetryController().setEditable(b, false);
    }
  }

  @Override
  public void setModified(final boolean modified) {
    super.setModified(modified);
    if (!modified && panel != null) {
      panel.getPlanimetryController().saveDone();
    }
  }

  @Override
  protected void runLoadedHandler() {
    if (reportVisuService.getPlanimetryVisuPanel() != null) {
      scenarioVisuLoaded();
    }
  }

  private void scenarioVisuLoaded() {
    final double[][] repere = (panel == null) ? null : panel.getVueCalque().getRepere();
    this.removeAll();

    panel = reportVisuService.getPlanimetryVisuPanel();
    final List<BCalque> mainLayers = panel.getPlanimetryController().getMainLayers();
    for (final BCalque bCalque : mainLayers) {
      bCalque.addPropertyChangeListener(BSelecteurCheckBox.PROP_USER_VISIBLE, this);
    }
    initConfiguration = panel.getPlanimetryController().getVisuConfiguration().copy();
    restoreDefaultConfigurationData();
    updateConfigurer();
    selectionListener = new LayerSelectionListener();
    panel.getScene().addSelectionListener(selectionListener);
    panel.getPlanimetryController().getState().addModifiedListener(modificationChangeListener);
    panel.getCqLegend().addPropertyChangeListener(modificationChangeListener);
    panel.getPlanimetryController().getGroupExternController().addObserver(modificationChangeListener);
    final JPanel main = new JPanel();
    main.setLayout(new BorderLayout());
    panel.setPreferredSize(new Dimension(600, 700));
    main.add(createToolbar(panel), BorderLayout.NORTH);
    main.add(panel.createGisEditionPalette(false), BorderLayout.WEST);
    main.add(panel);
    add(main);
    final JPanel bottom = new JPanel(new BorderLayout(0, 5));
    main.add(bottom, BorderLayout.SOUTH);
    this.repaint();
    super.revalidate();
    if (!GraphicsEnvironment.isHeadless()) {
      EventQueue.invokeLater(
          () -> {
            if (repere != null && isOpened() && runOpenedAndZoomSet) {
              panel.getVueCalque().setRepere(repere);
            } else {
              panel.restaurer();
            }
            if (isOpened()) {
              runOpenedAndZoomSet = true;
            }
            selectedTimeChanged();

            panel.getPlanimetryController()
                .setEditable(editable);
          }
      );
    }
  }

  @Override
  public void propertyChange(final PropertyChangeEvent evt) {
    if (panel != null && !updating) {
      reportPlanimetryConfig.setVisibility(panel.getPlanimetryController().getLayerVisibility());
      reportViewServiceContrat.changesDoneIn(ReportVisualTopComponent.this);
    }
  }

  private void buildCalcMapIfNeeded() {
    if (calcByName == null) {
      calcByName = new HashMap<>();
      final EMHScenario scenario = reportService.getRunCourant().getScenario();
      final List<Calc> calcs = scenario.getDonCLimMScenario().getCalc();
      for (final Calc calc : calcs) {
        calcByName.put(calc.getNom(), calc.getUiId());
      }
    }
  }

  @Override
  public void clearReportModifications() {
    updating = true;
    restoreDefaultConfigurationData();
    if (panel != null) {
      panel.getPlanimetryController().apply(reportPlanimetryConfig.getVisuConfiguration());
      panel.getPlanimetryController().applyExternLayerVisibilty(reportPlanimetryConfig.getVisibilityByExternLayerName());
      panel.getVueCalque().repaint(0);
    }
    clearModificationState();
    updating = false;
  }

  private void setDefaultName() throws MissingResourceException {
    setName(NbBundle.getMessage(ReportVisualTopComponent.class, TOPCOMPONENT_ACTION));
  }

  private void restoreDefaultConfigurationData() {
    final ReportTemplateFileProvider template = new ReportTemplateFileProvider();
    reportPlanimetryConfig = template.loadTemplate(ReportContentType.PLANIMETRY);
    if (reportPlanimetryConfig == null) {
      reportPlanimetryConfig = new ReportPlanimetryConfig();
      reportPlanimetryConfig.setVisuConfiguration(initConfiguration);
    }
    reportPlanimetryConfigurer.initFrom(reportPlanimetryConfig.getReportPlanimetryExtraContainer());
    reportPlanimetryConfig.setVisibilityForExternLayer(panel.getPlanimetryController().getVisibilityForExternLayer());
    new ReportPlanimetryConfigUI(panel.getPlanimetryController()).updatePalettes();
    setDefaultName();
    setModified(false);
  }

  private void clearModificationState() {
    setModified(false);
  }

  private void selectionChangedInPanel() {
    if (updating) {
      return;
    }
    final List<EMH> selected = panel.getSelectedEMHs();
    final List<Node> nodes = new ArrayList<>();
    for (final EMH emh : selected) {
      nodes.add(new ReportNodeEMHFactory().createNode(emh, false));
    }
    if (nodes.isEmpty()) {
      em.setRootContext(Node.EMPTY);
    } else {
      final Node[] nodesArray = nodes.toArray(new Node[0]);
      final Children.Array array = new Children.Array();
      array.add(nodesArray);
      final AbstractNode node = new AbstractNode(array);
      em.setRootContext(node);
      try {
        em.setSelectedNodes(nodesArray);
      } catch (final PropertyVetoException propertyVetoException) {
        Exceptions.printStackTrace(propertyVetoException);
      }
    }
    //met à jour la sélection des EMHs.
    reportSelectedEMHService.setSelectedEMHs(selected);
  }

  private JComponent createToolbar(final PlanimetryVisuPanel panel) {
    final SaveTemplateAction action = new SaveTemplateAction(this);
    return PlanimetryVisuPanelBuilder.buildTopComponent(panel, action.buildButton(EbliComponentFactory.INSTANCE));
  }

  @Override
  protected void runUnloadedHandler() {
    updating = true;
    if (panel != null) {
      panel.getPlanimetryController().getState().removeModifiedListener(modificationChangeListener);
      panel.getCqLegend().removePropertyChangeListener(modificationChangeListener);
      panel.getScene().removeSelectionListener(selectionListener);
      final List<BCalque> mainLayers = panel.getPlanimetryController().getMainLayers();
      for (final BCalque bCalque : mainLayers) {
        bCalque.removePropertyChangeListener(BSelecteurCheckBox.PROP_USER_VISIBLE, this);
      }
    }

    panel = null;
    calcByName = null;
    runOpenedAndZoomSet = false;
    this.removeAll();

    this.add(new JLabel(NbBundle.getMessage(ReportVisualTopComponent.class, "TopComponent.NoScenarioLoadedInformations")));
    super.revalidate();

    this.repaint();
    updating = false;
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables

  @Override
  protected void componentOpenedHandler() {
    if (resultatVisuPanel == null) {
      visuLookupListener = new VisuPanelLookupListener();
      resultatVisuPanel
          = reportVisuService.getLookup().lookupResult(PlanimetryVisuPanel.class
      );
      resultatVisuPanel.addLookupListener(visuLookupListener);
    }
    if (resultatEMHsSelected == null) {
      selectedEMHsLookupListener = new SelectedEMHsLookupListener();
      resultatEMHsSelected
          = reportSelectedEMHService.getLookup().lookupResult(Long.class
      );
      resultatEMHsSelected.addLookupListener(selectedEMHsLookupListener);
    }
    if (info != null) {
      updateFrameName();
    }
  }

  @Override
  protected void componentClosedDefinitlyHandler() {
    if (resultatVisuPanel != null) {
      resultatVisuPanel.removeLookupListener(visuLookupListener);
      resultatVisuPanel = null;
    }
    if (resultatEMHsSelected != null) {
      resultatEMHsSelected.removeLookupListener(selectedEMHsLookupListener);
      resultatEMHsSelected = null;
    }
  }


  void writeProperties(final java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
    p.setProperty("version", "1.0");

  }

  void readProperties(final java.util.Properties p) {
  }

  private class LayerSelectionListener implements ZSelectionListener {
    @Override
    public void selectionChanged(final ZSelectionEvent event) {
      selectionChangedInPanel();
    }
  }

  private class VisuPanelLookupListener implements LookupListener {
    @Override
    public void resultChanged(final LookupEvent ev) {
      if (reportVisuService.getPlanimetryVisuPanel() != null) {
        scenarioVisuLoaded();
      }
    }
  }

  private class SelectedEMHsLookupListener implements LookupListener {
    @Override
    public void resultChanged(final LookupEvent ev) {

      if (panel == null || WindowManager.getDefault().getRegistry().getActivated() == ReportVisualTopComponent.this) {
        return;
      }
      if (reportSelectedEMHService.isUpdating()) {
        return;
      }
      final Collection<? extends Long> allItems = resultatEMHsSelected.allInstances();
      final Collection<Long> uids = new ArrayList<>(allItems.size());
      uids.addAll(allItems);
      updating = true;
      panel.setSelectedEMHFromUids(uids);
      updating = false;
    }
  }

  private class VisuChangerListener implements PropertyChangeListener, Observer {
    VisuChangerListener() {
    }

    @Override
    public void update(final Observable o, final Object arg) {
      if (!updating && "visible".equals(arg)) {
        reportPlanimetryConfig.setVisibilityForExternLayer(panel.getPlanimetryController().getVisibilityForExternLayer());
        reportViewServiceContrat.changesDoneIn(ReportVisualTopComponent.this);
      }
    }

    @Override
    public void propertyChange(final PropertyChangeEvent evt) {

      if (panel == null) {
        return;
      }
      if (evt.getSource() == panel.getCqLegend() && (!editable || ignoreProperties.contains(evt.getPropertyName()))) {
        return;
      }
      if (updating) {
        return;
      }
      final PlanimetryCalqueState state = panel.getPlanimetryController().getState();
      if (state.isExternDataModified()) {
        setAdditonnalLayerModified();
      }
      if (state.isScenarioModified() || state.isStudyConfigModified()) {
        reportPlanimetryConfig.setReportPlanimetryExtraContainer(reportPlanimetryConfigurer.getDatas());
        reportPlanimetryConfig.setVisuConfiguration(panel.getPlanimetryController().getVisuConfiguration());
        reportPlanimetryConfig.setLegendPropertes(panel.getCqLegend().saveUIProperties());
        reportViewServiceContrat.changesDoneIn(ReportVisualTopComponent.this);
      }
    }
  }
}
