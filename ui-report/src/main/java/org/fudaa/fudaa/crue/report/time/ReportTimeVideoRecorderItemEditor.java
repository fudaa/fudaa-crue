/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.time;

import java.awt.Component;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyEditorSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ebli.animation.EbliAnimationOutputInterface;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertySupportReadWrite;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

/**
 *
 * @author Frederic Deniger
 */
public class ReportTimeVideoRecorderItemEditor extends PropertyEditorSupport implements ExPropertyEditor, VetoableChangeListener {
  
  PropertyEnv env;
  
  @Override
  public void attachEnv(PropertyEnv env) {
    this.env = env;
    env.addVetoableChangeListener(this);
    env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
    FeatureDescriptor featureDescriptor = env.getFeatureDescriptor();
    featureDescriptor.setValue("suppressCustomEditor", Boolean.FALSE);
    PropertyCrueUtils.configureNoEditAsText(featureDescriptor);
  }
  ReportTimeVideoRecorderItemEditorPanel outputPanel;
  
  @Override
  public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
    if (PropertyEnv.PROP_STATE.equals(evt.getPropertyName())
            && evt.getNewValue() == PropertyEnv.STATE_VALID) {
      String error = outputPanel.validateString();
      if (error == null) {
        Object old = getValue();
        setValue(outputPanel.getOutput());
        if (old == null && getValue() != null) {
          PropertySupportReadWrite featureDescriptor = (PropertySupportReadWrite) env.getFeatureDescriptor();
          ReportTimeVideoRecorderItem item = (ReportTimeVideoRecorderItem) featureDescriptor.getInstance();
          item.setActivated(true);
        }
      } else {
        throw new PropertyVetoException(error, evt);
      }
    }
  }
  
  @Override
  public boolean supportsCustomEditor() {
    return true;
  }
  
  @Override
  public Component getCustomEditor() {
    if (outputPanel == null) {
      outputPanel = new ReportTimeVideoRecorderItemEditorPanel();
    }
    PropertySupportReadWrite featureDescriptor = (PropertySupportReadWrite) env.getFeatureDescriptor();
    ReportTimeVideoRecorderItem item = (ReportTimeVideoRecorderItem) featureDescriptor.getInstance();
    outputPanel.setOutput((EbliAnimationOutputInterface) getValue(), item.getNbFrames());
    return outputPanel.getPanel();
  }
  
  @Override
  public String getAsText() {
    EbliAnimationOutputInterface value = (EbliAnimationOutputInterface) getValue();
    if (value == null) {
      return StringUtils.EMPTY;
    } else {
      return value.getShortName() + "-> " + (value.getDestFile() == null ? StringUtils.EMPTY : value.getDestFile().getName());
    }
  }
  
  @Override
  public void setAsText(String text) throws IllegalArgumentException {
  }
}
