package org.fudaa.fudaa.crue.report.property;

import java.lang.reflect.InvocationTargetException;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.openide.nodes.PropertySupport;
import org.openide.util.NbBundle;

/**
 *
 * @author gressier
 */
public class RunProperty extends PropertySupport.ReadOnly<String> {
  
  public static final String ID = "Run";
  private final EMHRun run;

  public RunProperty(EMHRun run) {
    super(ID, String.class, getDefaultDisplayName(), getDescription());
    this.run = run;
    // non éditable
    PropertyCrueUtils.configureNoCustomEditor(this);
  }
  
  public static String getDescription() {
    return NbBundle.getMessage(RunProperty.class, "RunProperty.Description");
  }

  public static String getDefaultDisplayName() {
    return NbBundle.getMessage(RunProperty.class, "RunProperty.DefaultDisplayName");
  }
  
  @Override
  public String toString() {
    return run.getNom();
  }
  
  @Override
  public String getValue() throws IllegalAccessException, InvocationTargetException {
    return toString();
  }
  
}
