package org.fudaa.fudaa.crue.report.actions;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.fudaa.crue.loi.section.ProfilSectionOpenFrtTarget;
import org.fudaa.fudaa.crue.report.ReportDFRTTopComponent;
import org.fudaa.fudaa.crue.report.perspective.ActiveReport;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle;

/**
 * permet d'ouvrir la liste des frottements
 */
@ActionID(category = "View", id = "org.fudaa.fudaa.crue.report.actions.ModellingOpenListFrottementsAction")
@ActionRegistration(displayName = "#ReportOpenListFrottementsAction.Name")
@ActionReferences({
  @ActionReference(path = ActiveReport.ACTIONS_REPORT_VIEWS, position = 1)
})
public final class ReportOpenListFrottementsAction extends AbstractOpenViewAction implements ProfilSectionOpenFrtTarget {

  public ReportOpenListFrottementsAction() {
    super(ReportDFRTTopComponent.MODE, ReportDFRTTopComponent.TOPCOMPONENT_ID);
    putValue(Action.NAME, NbBundle.getMessage(ReportOpenListFrottementsAction.class, "ReportOpenListFrottementsAction.Name"));
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    super.open();
  }

  @Override
  public void openFrt(DonFrt frt) {
    ReportDFRTTopComponent open = (ReportDFRTTopComponent) super.open();
    open.setDefaultSectionDonFrt(frt);
  }

  @Override
  public boolean validateBeforeOpenFrtEditor() {
    return true;
  }
}
