/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.actions;

import java.awt.event.ActionEvent;
import java.util.Collections;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.fudaa.crue.report.service.ReportVisuPanelService;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ReportGlobalFindAction extends EbliActionSimple {

  public ReportGlobalFindAction() {
    super("Find", null, "Find");
    setKey(KeyStroke.getKeyStroke("ctrl F"));
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final ReportVisuPanelService reportVisuPanelService = Lookup.getDefault().lookup(ReportVisuPanelService.class);
    if (reportVisuPanelService.getPlanimetryVisuPanel() != null) {
      reportVisuPanelService.getPlanimetryVisuPanel().find();
    }
  }

  public static void installAction(final JComponent c) {
    EbliLib.updateMapKeyStroke(c, Collections.singletonList(new ReportGlobalFindAction()));

  }
}
