package org.fudaa.fudaa.crue.report.perspective;

import java.util.Collection;
import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractPerspectiveAction;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;
/**
 * Permet d'activer une perspective. Action utilisée dans la toobar pour changer de perspective. Pour la gestion des ordres des menus, voir le fichier
 * layer.xml
 */
@ActionID(category = "File", id = "org.fudaa.fudaa.crue.report.perspective.ActiveReport")
@ActionRegistration(displayName = "#CTL_ActiveReport")
@ActionReference(path = "Menu/Window", position = 5)
public final class ActiveReport extends AbstractPerspectiveAction {

  public static final String ACTIONS_REPORT_VIEWS = "Actions/Reports-Views";
  public static final String ACTIONS_REPORT = "Actions/Report";

  public ActiveReport() {
    super("CTL_ActiveReport", PerspectiveServiceReport.class.getName(), PerspectiveEnum.REPORT);
    setBooleanState(false);
  }

  @Override
  protected String getFolderAction() {
    return ACTIONS_REPORT;
  }

  @Override
  protected void addSpecificMenuAtEnd(JPopupMenu menu) {
    JMenu menuFrame = new JMenu(NbBundle.getMessage(ActiveReport.class, "MenuFrame.Name"));
    menu.addSeparator();
    menu.add(menuFrame);
    Collection all = Lookups.forPath(ACTIONS_REPORT_VIEWS).lookupAll(Object.class);
    for (Object object : all) {
      if (object instanceof AbstractAction) {
        menuFrame.add((AbstractAction) object);
      } else if (object instanceof JSeparator) {
        menuFrame.addSeparator();
      }
    }
  }
}
