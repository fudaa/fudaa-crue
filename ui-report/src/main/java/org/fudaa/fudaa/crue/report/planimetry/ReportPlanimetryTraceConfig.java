/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.planimetry;

import java.awt.Color;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryTraceExtraData;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.planimetry.configuration.TraceConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.TraceConfigurationExtra;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryLigneBriseeLayerModel;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryTraceLayerModel;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;

/**
 *
 * @author Frederic Deniger
 */
public class ReportPlanimetryTraceConfig extends AbstractConfigurationExtra<ReportPlanimetryTraceExtraData> implements TraceConfigurationExtra {

  public ReportPlanimetryTraceConfig(ReportResultProviderService reportResultProviderService) {
    super(reportResultProviderService);
    data = new ReportPlanimetryTraceExtraData();
  }

  @Override
  public String getDisplayName(DonPrtGeoProfilSection profilSection, PlanimetryTraceLayerModel modeleDonnees, TraceConfiguration aThis, boolean selected) {
//    return
    return getDisplayName(profilSection.getNom(), profilSection.getEmh(), selected);
  }

  @Override
  public void decoreTraceLigne(CatEMHSection section, TraceLigneModel ligne, PlanimetryLigneBriseeLayerModel model, TraceConfiguration aThis) {
    Color color = getColor(section);
    if (color != null) {
      ligne.setColor(color);
    }
  }
}
