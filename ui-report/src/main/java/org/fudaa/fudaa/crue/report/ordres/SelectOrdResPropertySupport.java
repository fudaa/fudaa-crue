/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.ordres;

import java.lang.reflect.InvocationTargetException;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.result.OrdResVariableSelection;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertySupportReadWrite;

/**
 *
 * @author Frederic Deniger
 */
public class SelectOrdResPropertySupport extends PropertySupportReadWrite<OrdResVariableSelection, Boolean> {

  private final String variableName;

  public SelectOrdResPropertySupport(AbstractNodeFirable node, OrdResVariableSelection ores, String variableName) {
    super(node, ores, Boolean.TYPE, variableName, StringUtils.capitalize(variableName));
    this.variableName = variableName;
  }

  @Override
  protected void setValueInInstance(Boolean newVal) {
    getInstance().setVariableSelected(variableName, newVal);
  }

  @Override
  public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
    return Boolean.valueOf(getInstance().isVariableSelected(variableName));
  }
}
