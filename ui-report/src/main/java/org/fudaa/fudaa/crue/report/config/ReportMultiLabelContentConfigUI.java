/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.config;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultOutlineViewEditor;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportMultiLabelContentConfigUI {

  final List<ReportVariableKey> availableVar;

  public ReportMultiLabelContentConfigUI(List<ReportVariableKey> initAvailableVar) {
    availableVar = new ArrayList<>();
    availableVar.add(null);
    availableVar.addAll(initAvailableVar);
  }

  public List<ReportLabelContentConfig> configure(List<ReportLabelContentConfig> configs) {
    List<ReportLabelContentConfigNode> nodes = new ArrayList<>();
    for (ReportLabelContentConfig reportLabelContentConfig : configs) {
      nodes.add(new ReportLabelContentConfigNode(reportLabelContentConfig.clone(), availableVar));
    }
    LabelsEditor editor = new LabelsEditor();
    editor.getExplorerManager().setRootContext(NodeHelper.createNode(nodes, "", false));
    boolean ok = DialogHelper.showQuestionOkCancel(NbBundle.getMessage(ReportMultiLabelContentConfigUI.class, "ConfigureLabels.Action"), editor);
    if (ok) {
      List<ReportLabelContentConfig> res = new ArrayList<>();
      Node[] resNodes = editor.getExplorerManager().getRootContext().getChildren().getNodes();
      for (Node node : resNodes) {
        res.add(node.getLookup().lookup(ReportLabelContentConfig.class));
      }
      return res;
    }
    return null;
  }

  protected class LabelsEditor extends DefaultOutlineViewEditor {

    public LabelsEditor() {     
        SysdocUrlBuilder.installHelpShortcut(this, SysdocUrlBuilder.getDialogHelpCtxId("configurerLabels", PerspectiveEnum.REPORT));
    }

    @Override
    protected void addElement() {
      ReportLabelContentConfig newConfig = new ReportLabelContentConfig();
      newConfig.setConfig(new LabelConfig());
      newConfig.setContent(new ReportLabelContent());
      Children children = getExplorerManager().getRootContext().getChildren();
      final ReportLabelContentConfigNode newNode = new ReportLabelContentConfigNode(newConfig, availableVar);
      children.add(new Node[]{newNode});

    }

    @Override
    protected void createOutlineView() {
      view = new OutlineView(StringUtils.EMPTY);
      view.setPropertyColumns(
              ReportLabelContent.PROP_PREFIX, ReportLabelContent.geti18nPrefix(),
              ReportLabelContent.PROP_CONTENT, ReportLabelContent.geti18nContent(),
              ReportLabelContent.PROP_UNIT, ReportLabelContent.geti18nUnit(),
              LabelConfig.PROP_FGCOLOR, LabelConfig.geti18nFgColor(),
              LabelConfig.PROP_FONT, LabelConfig.geti18nFont());
      view.getOutline().setColumnHidingAllowed(false);
      view.getOutline().setRootVisible(false);
    }
  }
}
