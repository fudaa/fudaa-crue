/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import com.thoughtworks.xstream.XStream;
import org.fudaa.dodico.crue.projet.report.persist.ReportCourbeConfigReader;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryBrancheExtraData;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryCasierExtraData;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryExtraContainer;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryNodeExtraData;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetrySectionExtraData;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryTraceExtraData;
import org.fudaa.dodico.crue.projet.report.transformer.KeysToStringConverter;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.fudaa.fudaa.crue.report.persist.ReportConfigPlanimetryDao;
import org.fudaa.fudaa.crue.report.persist.ReportPlanimetryConfigPersist;

/**
 *
 * @author Frederic Deniger
 */
public class ReportConfigReader extends ReportCourbeConfigReader {

  public ReportConfigReader(TraceToStringConverter traceToStringConverter, KeysToStringConverter keysToStringConverter) {
    super(traceToStringConverter, keysToStringConverter);
  }

  @Override
  protected void configureXStreamPlanimetry(XStream xstream) {

    xstream.processAnnotations(ReportConfigPlanimetryDao.class);

    xstream.processAnnotations(ReportPlanimetryConfigPersist.class);
    xstream.processAnnotations(ReportPlanimetryExtraContainer.class);
    xstream.processAnnotations(ReportPlanimetryBrancheExtraData.class);
    xstream.processAnnotations(ReportPlanimetryNodeExtraData.class);
    xstream.processAnnotations(ReportPlanimetryCasierExtraData.class);
    xstream.processAnnotations(ReportPlanimetrySectionExtraData.class);
    xstream.processAnnotations(ReportPlanimetryTraceExtraData.class);

  }
}
