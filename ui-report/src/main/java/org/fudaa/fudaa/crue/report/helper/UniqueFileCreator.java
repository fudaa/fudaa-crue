/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.helper;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.projet.select.UniqueNomFinder;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Frederic Deniger
 */
public class UniqueFileCreator {
  private final String extension;
  private final Set<String> filenames;

  public UniqueFileCreator(final File dir) {
    this(".xml", dir);
  }

  /**
   * @param extension .xml par exemple
   * @param dir le repertoire parent
   */
  private UniqueFileCreator(final String extension, final File dir) {
    this.extension = extension;
    filenames = new HashSet<>();
    final File[] listFiles = dir.listFiles();
    if (listFiles != null) {
      for (final File file : listFiles) {
        final String name = file.getName();
        if (name.endsWith(extension)) {
          filenames.add(StringUtils.removeEnd(name, extension));
        }
      }
    }
  }

  /**
   * Ne créé pas le nouveau fichier
   *
   * @return nom du nouveau fichier
   */
  public String createNewFilename() {
    final UniqueNomFinder finder = new UniqueNomFinder();
    final String findNewName = finder.findUniqueName(filenames, "", 3);
    filenames.add(findNewName);
    return findNewName + extension;
  }
}
