/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.config;

import com.memoire.bu.BuGridLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.ComboBoxHelper;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.report.data.ReportVariableKeyCellRenderer;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLabelContentUI {

  final List<ReportVariableKey> availableVar;

  public ReportLabelContentUI(List<ReportVariableKey> initAvailableVar) {
    availableVar = new ArrayList<>();
    availableVar.add(null);
    availableVar.addAll(initAvailableVar);
  }

  public boolean configure(ReportLabelContent config) {
    JPanel pn = new JPanel(new BuGridLayout(2, 5, 5));
    pn.add(new JLabel(NbBundle.getMessage(ReportLabelContentUI.class, "LabelContent.Text")));
    JTextField tf = new JTextField(50);
    tf.setText(config.getPrefix());
    pn.add(tf);
    pn.add(new JLabel(NbBundle.getMessage(ReportLabelContentUI.class, "VariableContent.Text")));
    JComboBox cb = new JComboBox();
    ComboBoxHelper.setDefaultModel(cb, availableVar);
    cb.setRenderer(new ReportVariableKeyCellRenderer());
    //le cell renderer:
    cb.setSelectedItem(config.getContent());
    pn.add(cb);
    pn.add(new JLabel(NbBundle.getMessage(ReportLabelContentUI.class, "UnitContent.Text")));
    JCheckBox cbUnit = new JCheckBox();
    cbUnit.setSelected(config.isUnit());
    pn.add(cbUnit);    
    SysdocUrlBuilder.installHelpShortcut(pn, SysdocUrlBuilder.getDialogHelpCtxId("configurationTitre", PerspectiveEnum.REPORT));
    boolean ok = DialogHelper.showQuestionOkCancel(NbBundle.getMessage(ReportLabelContentUI.class, "Configuration.DialogTitle"), pn);
    if (ok) {
      config.setContent((ReportVariableKey) cb.getSelectedItem());
      config.setPrefix(tf.getText());
      config.setUnit(cbUnit.isSelected());
    }
    return ok;
  }
}
