/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.actions;

import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.report.node.ReportRunNode;
import org.fudaa.fudaa.crue.report.service.ReportRunAlternatifService;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportUnsetAsAlternatifNodeAction extends AbstractEditNodeAction {

  final ReportRunAlternatifService reportRunAlternatifService = Lookup.getDefault().lookup(ReportRunAlternatifService.class);

  public ReportUnsetAsAlternatifNodeAction() {
    super(NbBundle.getMessage(ReportUnsetAsAlternatifNodeAction.class, "ReportUnsetAsAlternatifNodeAction.DisplayName"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return activatedNodes != null && activatedNodes.length > 0;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    for (Node node : activatedNodes) {
      reportRunAlternatifService.removeRun(((ReportRunNode) node).getRun());
    }
  }
}
