/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.helper;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;

/**
 * Permet de toujours ajouter un prefix avec le nom de calcul.
 *
 * @author Frederic Deniger
 */
public class ResultatKeyNomCalcPrefixToStringTransformer implements ToStringTransformer {

  final ToStringTransformer iniTransformer;

  public ResultatKeyNomCalcPrefixToStringTransformer(ToStringTransformer iniTransformer) {
    this.iniTransformer = iniTransformer;
  }

  @Override
  public String transform(Object in) {
    if (in == null) {
      return StringUtils.EMPTY;
    }
    ResultatTimeKey key = (ResultatTimeKey) in;
    if (key.isPermanent()) {
      return key.getNomCalcul();
    }
    return key.getNomCalcul() + BusinessMessages.ENTITY_SEPARATOR + iniTransformer.transform(in);
  }
}
