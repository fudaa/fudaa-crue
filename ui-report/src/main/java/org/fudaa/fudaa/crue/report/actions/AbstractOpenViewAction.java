/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.actions;

import org.fudaa.fudaa.crue.common.helper.TopComponentOpener;
import org.openide.windows.TopComponent;

import javax.swing.*;

/**
 * @author Frederic Deniger
 */
public abstract class AbstractOpenViewAction extends AbstractAction {
    protected final TopComponentOpener topComponentOpener;

    public AbstractOpenViewAction(String mode, String topComponentId) {
        topComponentOpener = new TopComponentOpener(mode, topComponentId);
    }

    protected TopComponent open() {
       return topComponentOpener.open();
    }
}
