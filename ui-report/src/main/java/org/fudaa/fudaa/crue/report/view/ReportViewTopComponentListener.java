/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view;

import org.fudaa.fudaa.crue.common.services.TopComponentOpenCloseHelper;
import org.fudaa.fudaa.crue.report.ReportTopComponentConfigurable;
import org.openide.nodes.Node;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * @author Frederic Deniger
 */
public class ReportViewTopComponentListener implements PropertyChangeListener {
    private boolean listenerInstalled;
    private Target target;

    public Target getTarget() {
        return target;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

    public boolean isListenerInstalled() {
        return listenerInstalled;
    }

    public void installListener() {
        listenerInstalled = true;
        WindowManager.getDefault().getRegistry().addPropertyChangeListener(this);
    }

    public void removeListener() {
        WindowManager.getDefault().getRegistry().addPropertyChangeListener(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (target == null) {
            return;
        }
        final boolean activatedProperty = TopComponent.Registry.PROP_ACTIVATED.equals(evt.getPropertyName());
        if (evt.getNewValue() instanceof ReportTopComponentConfigurable) {
            final ReportTopComponentConfigurable src = (ReportTopComponentConfigurable) evt.getNewValue();
            final Node viewNode = ReportViewsService.getViewNode(src.getTopComponent());
            if (activatedProperty) {
                target.reportTopComponentActivated(src, viewNode);
            } else if (TopComponent.Registry.PROP_TC_OPENED.equals(evt.getPropertyName())) {
                target.reportTopComponentOpened(src, viewNode);
            } else if (TopComponent.Registry.PROP_TC_CLOSED.equals(evt.getPropertyName())) {
                boolean temporarely = TopComponentOpenCloseHelper.isTemporarelyClosed(src.getTopComponent());
                if (temporarely) {
                    target.reportTopComponentClosedTemporarely(src, viewNode);
                } else {
                    target.reportTopComponentClosedDefinetly(src, viewNode);
                }
            }
        } else if (activatedProperty) {
            target.reportTopComponentActivated(null, null);
        }
    }

    public static class Target {
        /**
         * Attention, cette méthode peut être appelé avec tc à null pour signifier que le TopComponent activé n'est pas un Report.
         *
         * @param topComponentConfigurable peut etre null.
         * @param viewConfigNode           le node de configuration
         */
        public void reportTopComponentActivated(ReportTopComponentConfigurable topComponentConfigurable, Node viewConfigNode) {
        }

        /**
         * @param topComponentConfigurable le {@link TopComponent} ouvert
         * @param viewConfigNode           si null, c'est que c'est une nouvelle vue
         */
        public void reportTopComponentOpened(ReportTopComponentConfigurable topComponentConfigurable, Node viewConfigNode) {
        }

        /**
         * Lors des changement de perspectives, des vues sont fermés temporairement, d'ou cette méthode
         *
         * @param topComponentConfigurable le {@link TopComponent} fermé
         * @param viewConfigNode           le node correspondant
         */
        public void reportTopComponentClosedTemporarely(ReportTopComponentConfigurable topComponentConfigurable, Node viewConfigNode) {
        }

        /**
         * Lors des changement de perspectives, des vues sont fermés temporairement, d'ou cette méthode
         *
         * @param topComponentConfigurable le {@link TopComponent} fermé definitivement
         */
        public void reportTopComponentClosedDefinetly(ReportTopComponentConfigurable topComponentConfigurable, Node viewConfigNode) {
        }
    }
}
