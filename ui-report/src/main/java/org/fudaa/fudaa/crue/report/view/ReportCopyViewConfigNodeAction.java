/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view;

import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportCopyViewConfigNodeAction extends AbstractEditNodeAction {

  final ReportViewsService reportViewsService = Lookup.getDefault().lookup(ReportViewsService.class);
  final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

  public ReportCopyViewConfigNodeAction() {
    super(org.openide.util.NbBundle.getMessage(ReportCopyViewConfigNodeAction.class, "CopyViewConfig.ActionName"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    if (activatedNodes != null && activatedNodes.length == 1) {
      return activatedNodes[0].getLookup().lookup(ReportViewLine.class) != null;
    }
    return false;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    ReportViewLine line = activatedNodes[0].getLookup().lookup(ReportViewLine.class);
    ReportViewLine copy = line.copy(configurationManagerService.getConnexionInformation());
    copy.getLineInformation().setNom(line.getLineInformation().getNom() + " " + NbBundle.getMessage(ReportCopyViewConfigNodeAction.class, "ViewConfig.CopySuffix"));
    reportViewsService.add(copy);
    copy.setModified(true);

  }
}
