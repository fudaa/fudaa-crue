/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.config;

import gnu.trove.TIntArrayList;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.projet.report.data.ExternFileKey;
import org.fudaa.dodico.crue.projet.report.data.ReportKeyContract;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVerticalTimeKey;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.dodico.crue.projet.report.persist.AbstractReportCourbeConfig;
import org.fudaa.ebli.courbe.*;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.loi.ViewCourbeManager;
import org.fudaa.fudaa.crue.loi.common.CourbeModelWithKey;
import org.fudaa.fudaa.crue.loi.common.CourbeModelWithKeyHelper;
import org.fudaa.fudaa.crue.loi.common.CourbesUiController;
import org.fudaa.fudaa.crue.report.helper.CourbeComparator;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.fudaa.fudaa.crue.report.temporal.ReportTemporalTimeCourbe;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public abstract class AbstractReportGrapheBuilder<U extends CourbesUiController, Z extends AbstractReportCourbeConfig> {
  protected final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
  protected final ReportResultProviderService reportResultProviderService = Lookup.getDefault().lookup(ReportResultProviderService.class);
  protected final U uiController;
  protected final ViewCourbeManager loiLabelsManager;
  private final ExternFileBuilder externFileBuilder = new ExternFileBuilder();

  public static void applyPersistConfig(final EGCourbePersist persist, final EGCourbeSimple courbe) {
    if (persist == null) {
      courbe.setAspectContour(CourbeColorFinder.NONE);
    } else {
      courbe.applyPersitUiConfig(persist);
    }
  }

  public ReportResultProviderService getReportResultProviderService() {
    return reportResultProviderService;
  }

  public void synchronizeExternFiles(final Z config) {
    final List<EGCourbeSimple> otherCourbes = new ArrayList<>();
    final EGCourbeSimple[] courbes = uiController.getEGGrapheSimpleModel().getCourbes();
    for (final EGCourbeSimple c : courbes) {
      final CourbeModelWithKey model = (CourbeModelWithKey) c.getModel();
      final ReportKeyContract key = (ReportKeyContract) model.getKey();
      if (!key.isExtern()) {
        otherCourbes.add(c);
      }
    }
    loadExternFilesAndAllToModel(config, otherCourbes);

    uiController.getGraphe().restore();
    uiController.getGraphe().fullRepaint();
  }

  private static boolean isSamePosition(final List<EGAxeVertical> axes) {
    if (axes.size() > 1) {
      final boolean first = axes.get(0).isDroite();
      for (int i = 1; i < axes.size(); i++) {
        if (first != axes.get(i).isDroite()) {
          return false;
        }
      }
    }

    return true;
  }

  public AbstractReportGrapheBuilder(final U uiController, final ViewCourbeManager loiLabelsManager) {
    this.uiController = uiController;
    this.loiLabelsManager = loiLabelsManager;
  }

  public EGAxeVertical getOrCreateAxeVertical(final ItemVariable variable) {
    return getOrCreateAxeVertical(variable.getNature());
  }

  public EGAxeVertical getOrCreateAxeVertical(final PropertyNature variable) {
    EGAxeVertical findAxe = uiController.findAxe(variable);
    if (findAxe == null) {
      findAxe = uiController.createAxeVertical(variable, true);
    }
    return findAxe;
  }

  public void runUnloaded() {
    final EGCourbeSimple[] courbes = uiController.getEGGrapheSimpleModel().getCourbes();
    final TIntArrayList toRemove = new TIntArrayList();
    for (int i = 0; i < courbes.length; i++) {
      final EGCourbeSimple courbe = courbes[i];
      final CourbeModelWithKey model = (CourbeModelWithKey) courbe.getModel();
      final ReportKeyContract contract = (ReportKeyContract) model.getKey();
      if (!contract.isExtern() && (contract.getReportRunKey() == null || contract.getReportRunKey().isCourant())) {
        toRemove.add(i);
      }
    }
    if (!toRemove.isEmpty()) {
      uiController.getEGGrapheSimpleModel().removeCurves(toRemove.toNativeArray(), null);
    }
    loiLabelsManager.removeTitleLabels();
  }

  protected Map<String, EGAxeVertical> getOrCreateAxesVerticalConfigured(final Z content, final Collection<PropertyNature> variables) {
    final Map<String, EGAxeVertical> res = new HashMap<>();
    for (final PropertyNature itemVariable : variables) {
      res.put(itemVariable.getNom(), getOrCreateAxeVerticalConfigured(itemVariable, content, res));
    }
    return res;
  }

  protected EGAxeVertical getOrCreateAxeVerticalConfigured(final PropertyNature variable, final Z content, final Map<String, EGAxeVertical> saved) {
    final String key = variable.getNom();
    EGAxeVertical findAxe = saved == null ? null : saved.get(key);
    if (findAxe == null) {
      findAxe = getOrCreateAxeVertical(variable);
    }
    final EGAxeVerticalPersist persist = content.getAxeVConfig((String) findAxe.getKey());
    if (persist != null) {
      persist.apply(findAxe);
    }
    if (saved != null) {
      saved.put(key, findAxe);
    }
    return findAxe;
  }

  private void applyUiConfig() {
    loiLabelsManager.applyConfig();
  }

  /**
   * maj des courbes si temps change
   *
   * @param content le contenu
   */
  public void updateResultCurvesAfterTimeChanged(final Z content) {
    final List<EGCourbeSimple> internCourbesAfterTimeChanged = getInternCourbesAfterTimeChanged(content);
    final List<EGCourbeSimple> externeCourbes = new ArrayList<>();
    final EGCourbeSimple[] courbes = uiController.getEGGrapheSimpleModel().getCourbes();
    for (final EGCourbeSimple c : courbes) {
      final CourbeModelWithKey model = (CourbeModelWithKey) c.getModel();
      final ReportKeyContract key = (ReportKeyContract) model.getKey();
      if (key.isExtern()) {
        externeCourbes.add(c);
      }
    }
    addAllToModel(externeCourbes, internCourbesAfterTimeChanged);
  }

  private List<EGCourbeSimple> getInternCourbesAfterTimeChanged(final Z content) {
    if (EventQueue.isDispatchThread()) {
      final ProgressRunnable<List<EGCourbeSimple>> runnable = handle -> getInternCourbesAfterTimeChanged(content, handle);
      return CrueProgressUtils.showProgressDialogAndRun(runnable, NbBundle.getMessage(AbstractReportGrapheBuilder.class,
          "CourbesBuilder.ProgressTitle"));
    } else {
      return getInternCourbesAfterTimeChanged(content, null);
    }
  }

  public abstract List<EGCourbeSimple> getInternCourbesAfterTimeChanged(Z content, ProgressHandle progress);

  /**
   * maj des courbes si le contenu a été modifie. Ne doit renvoyer que les courbes internes.
   *
   * @param content le contenu
   * @param progress Attention peut être null
   */
  public abstract List<EGCourbeSimple> getInternCourbes(Z content, ProgressHandle progress);

  private List<EGCourbeSimple> getInternCourbes(final Z content) {
    if (EventQueue.isDispatchThread()) {
      final ProgressRunnable<List<EGCourbeSimple>> runnable = new ProgressRunnable<List<EGCourbeSimple>>() {
        @Override
        public List<EGCourbeSimple> run(final ProgressHandle handle) {
          return getInternCourbes(content, handle);
        }
      };
      return CrueProgressUtils.showProgressDialogAndRun(runnable, NbBundle.getMessage(AbstractReportGrapheBuilder.class,
          "CourbesBuilder.ProgressTitle"));
    } else {
      return getInternCourbes(content, null);
    }
  }

  public final void applyChange(final Z content, final boolean fromTemplate) {
    EGAxeHorizontalPersist horizontalPersist = content.getHorizontalPersist();
    if (horizontalPersist != null) {
      horizontalPersist = new EGAxeHorizontalPersist(horizontalPersist.generateAxe());
    }
    final List<EGCourbeSimple> internCourbes = getInternCourbes(content);
    loadExternFilesAndAllToModel(content, internCourbes);
    cleanUnusedConfigs(content);
    applyUiConfig();
    applyLabelsConfigChanged(content);
    applyTitleConfigChanged(content);
    applyAxesConfigAndZoom(content, horizontalPersist, fromTemplate);
    uiController.getGraphe().fullRepaint();
    if (uiController.getGraphe().isAutoRestore()) {
      uiController.getGraphe().restore();
    }
  }

  private void cleanUnusedConfigs(final Z content) {
    final Set<String> usedAxes = new HashSet<>();
    final Set<ReportKeyContract> usedCourbes = new HashSet<>();
    final EGCourbeSimple[] courbes = uiController.getEGGrapheSimpleModel().getCourbes();
    for (final EGCourbeSimple courbe : courbes) {
      if (courbe.getAxeY() != null) {
        usedAxes.add((String) courbe.getAxeY().getKey());
      }
      final CourbeModelWithKey model = (CourbeModelWithKey) courbe.getModel();
      usedCourbes.add((ReportKeyContract) model.getKey());
    }
    content.cleanUnusedConfig(usedCourbes, usedAxes);
  }

  private void applyAxesConfigAndZoom(final Z content, final EGAxeHorizontalPersist horizontalPersist, final boolean fromTemplate) {
    final List<EGAxeVertical> axesY = uiController.getAxesY();
    if (axesY.size() > 1 && isSamePosition(axesY)) {
      final EGAxeVertical first = axesY.get(0);
      first.setDroite(false);
      for (int i = 1; i < axesY.size(); i++) {
        if (axesY.get(i) != first) {
          axesY.get(i).setDroite(true);
        }
      }
    }
    new CourbeColorFinder().findNewColor(uiController.getGraphe());
    for (final EGAxeVertical axe : axesY) {
      final EGAxeVerticalPersist axeVConfig = content.getAxeVConfig((String) axe.getKey());
      if (axeVConfig != null) {
        axeVConfig.apply(axe);
      }
    }
    boolean performZoom = false;
    if (horizontalPersist != null && StringUtils.equals(horizontalPersist.getTitre(), uiController.getAxeX().getTitre())) {
      horizontalPersist.apply(uiController.getAxeX());
    } else {
      performZoom = true;
    }
    if (performZoom || fromTemplate || content.isNoZoomSet()) {
      uiController.getGraphe().restore();
    }
  }

  public void applyLabelsConfigChanged(final Z content) {
    final List<LabelConfig> labels = content.getLoiLegendConfig().getLabels();
    final ReportRunKey runKey = reportService.getRunCourant() == null ? null : reportService.getRunCourant().getRunKey();
    for (final LabelConfig config : labels) {
      final ReportLabelContent labelContent = (ReportLabelContent) config.getKey();
      final String text = runKey == null ? StringUtils.EMPTY : reportResultProviderService.getLabelString(labelContent, runKey, content.getMainEMHName(),
          DecimalFormatEpsilonEnum.PRESENTATION, content.getSelectedTimeStepInReport());
      final String tltip = runKey == null ? StringUtils.EMPTY : reportResultProviderService.getLabelString(labelContent, runKey, content.getMainEMHName(),
          DecimalFormatEpsilonEnum.COMPARISON, content.getSelectedTimeStepInReport());
      config.setText(text);
      config.setTooltipText(tltip);
    }
    this.loiLabelsManager.getLoiLabelsManager().applyConfig();
  }

  public void updateLabelsAndTitleContent(final Z content) {
    final ReportRunKey runKey = reportService.getRunCourant() == null ? null : reportService.getRunCourant().getRunKey();
    for (final Pair<JLabel, LabelConfig> pair : loiLabelsManager.getLoiLabelsManager().getLabels()) {
      final JLabel label = pair.first;
      final LabelConfig labelConfig = pair.second;
      final ReportLabelContent labelContent = (ReportLabelContent) labelConfig.getKey();
      final String labelString = runKey == null ? StringUtils.EMPTY : reportResultProviderService.getLabelString(labelContent, runKey, content.
          getMainEMHName(), DecimalFormatEpsilonEnum.PRESENTATION, content.getSelectedTimeStepInReport());
      final String tltip = runKey == null ? StringUtils.EMPTY : reportResultProviderService.getLabelString(labelContent, runKey, content.getMainEMHName(),
          DecimalFormatEpsilonEnum.COMPARISON, content.getSelectedTimeStepInReport());
      labelConfig.setText(labelString);
      labelConfig.setTooltipText(tltip);
      labelConfig.apply(label);
    }

    applyTitleConfigChanged(content);
  }

  public void applyTitleConfigChanged(final Z content) {
    final LabelConfig titleConfig = loiLabelsManager.getConfig().getTitleConfig();
    final ReportLabelContent labelContent = (ReportLabelContent) titleConfig.getKey();
    titleConfig.setKey(labelContent);
    final String text = reportService.getRunCourant() == null ? StringUtils.EMPTY : reportResultProviderService.getLabelString(labelContent, reportService.
        getRunCourant().getRunKey(), content.getMainEMHName(), DecimalFormatEpsilonEnum.PRESENTATION, content.getSelectedTimeStepInReport());
    final String tltip = reportService.getRunCourant() == null ? StringUtils.EMPTY : reportResultProviderService.getLabelString(labelContent, reportService.
        getRunCourant().getRunKey(), content.getMainEMHName(), DecimalFormatEpsilonEnum.PRESENTATION, content.getSelectedTimeStepInReport());
    titleConfig.setText(text);
    titleConfig.setTooltipText(tltip);
    titleConfig.apply(loiLabelsManager.getLabelTitle());
  }

  private Object getSelectedKey() {
    final EGCourbe selectedComponent = uiController.getEGGrapheSimpleModel().getSelectedComponent();
    Object selectedKey = null;
    if (selectedComponent != null && selectedComponent.getModel() != null) {
      selectedKey = ((CourbeModelWithKey) selectedComponent.getModel()).getKey();
    }
    if (ReportVerticalTimeKey.isTimeKey(selectedKey)) {
      selectedKey = null;
    }
    return selectedKey;
  }

  private void loadExternFilesAndAllToModel(final Z content, final List<EGCourbeSimple> internCourbes) {
    //on doit les trier.
    //on doit ajouter les courbes externes.
    externFileBuilder.loadFiles(content);
    final EGAxeHorizontal axeX = uiController.getEGGrapheSimpleModel().getAxeX();
    final Set<EGAxeVertical> usedAxes = new HashSet<>();
    if (internCourbes != null) {
      for (final EGCourbeSimple c : internCourbes) {
        if (c.getAxeY() != null) {
          usedAxes.add(c.getAxeY());
        }
      }
    }
    final List<EGCourbeSimple> externe = externFileBuilder.synchronize(axeX, usedAxes);
    for (final EGCourbeSimple eGCourbeSimple : externe) {
      final CourbeModelWithKey model = (CourbeModelWithKey) eGCourbeSimple.getModel();
      final ReportKeyContract key = (ReportKeyContract) model.getKey();
      if (key instanceof ExternFileKey) {
        final EGCourbePersist externFileCourbeConfig = content.getExternFileCourbeConfig((ExternFileKey) key);
        applyPersistConfig(externFileCourbeConfig, eGCourbeSimple);
      }
    }
    addAllToModel(externe, internCourbes);
  }

  private void addAllToModel(final List<EGCourbeSimple> externe, final List<EGCourbeSimple> internCourbes) {
    final Object selectedKey = getSelectedKey();
    final CourbeComparator comparator = new CourbeComparator();
    Collections.sort(externe, comparator);
    if (internCourbes != null) {
      internCourbes.sort(comparator);
    }
    final List<EGCourbeSimple> courbes = new ArrayList<>();
    if (internCourbes != null) {
      courbes.addAll(internCourbes);
    }
    courbes.addAll(externe);
    uiController.getEGGrapheSimpleModel().removeAllCurves(null);
    uiController.getEGGrapheSimpleModel().addCourbes(courbes.toArray(new EGCourbeSimple[0]), null);

    if (selectedKey != null) {
      final EGCourbeSimple selected = CourbeModelWithKeyHelper.find(courbes, selectedKey);
      if (selected != null) {
        uiController.getEGGrapheSimpleModel().setSelectedComponent(selected);
      }
    }
    if (uiController.getEGGrapheSimpleModel().getSelectedComponent() == null && !courbes.isEmpty()) {
      EGCourbeSimple first = courbes.get(0);
      if (first.getClass().equals(ReportTemporalTimeCourbe.class)) {
        first = null;
        if (courbes.size() > 1) {
          first = courbes.get(1);
        }
      }
      uiController.getEGGrapheSimpleModel().setSelectedComponent(first);
    }
  }
}
