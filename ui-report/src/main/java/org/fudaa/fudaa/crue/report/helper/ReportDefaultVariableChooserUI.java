/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.helper;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultOutlineViewEditor;
import org.fudaa.fudaa.crue.report.ordres.ReportOrdResSelectionByRunLineNode;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.openide.explorer.view.OutlineView;

/**
 *
 * @author Frederic Deniger
 */
public class ReportDefaultVariableChooserUI extends DefaultOutlineViewEditor {

  final ReportResultProviderService reportResultProviderService;

  public ReportDefaultVariableChooserUI(final ReportResultProviderService reportResultProviderService) {
    this.reportResultProviderService = reportResultProviderService;
    final List<String> columns = new ArrayList<>();
    columns.add(reportResultProviderService.getReportService().getRunCourant().getRunKey().getDisplayName());
    columns.add(reportResultProviderService.getReportService().getRunCourant().getRunKey().getDisplayName());
    final List<ReportRunKey> keys = reportResultProviderService.getReportRunAlternatifService().getSortedReportRunContentKeys();
    for (final ReportRunKey reportRunKey : keys) {
      columns.add(reportRunKey.getDisplayName());//2 fois pour le nom de la propriété et pour son affichage.
      columns.add(reportRunKey.getDisplayName());
    }
    view.setPropertyColumns(columns.toArray(new String[0]));
    SysdocUrlBuilder.installHelpShortcut(this, SysdocUrlBuilder.getDialogHelpCtxId("vueProfilEnTravers_choixVariables", PerspectiveEnum.REPORT));
  }

  @Override
  protected void initActions() {
  }

  public void setNodes(final List<ReportOrdResSelectionByRunLineNode> nodes) {
    getExplorerManager().setRootContext(NodeHelper.createNode(nodes, StringUtils.EMPTY));
  }

  @Override
  protected void createOutlineView() {
    view = new OutlineView(StringUtils.EMPTY);


    view.getOutline().setColumnHidingAllowed(false);
    view.getOutline().setRootVisible(false);
    view.setDragSource(false);
    view.setDropTarget(false);
  }

  @Override
  protected void addElement() {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
