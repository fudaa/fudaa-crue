/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import java.io.File;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.dodico.crue.projet.report.transformer.KeysToStringConverter;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.report.persist.ReportPlanimetryConfigPersist;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.fudaa.fudaa.crue.report.view.ReportViewsService;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;

/**
 * Permet de charger une configuration.
 *
 * @author Frederic Deniger
 */
public class ReportConfigLoaderProcessor implements ProgressRunnable<CrueIOResu<ReportConfigContrat>> {

  ReportViewsService reportViewsService = Lookup.getDefault().lookup(ReportViewsService.class);
  final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
  ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);
  private final File file;

  public ReportConfigLoaderProcessor(File file) {
    this.file = file;
  }

  @Override
  public CrueIOResu<ReportConfigContrat> run(ProgressHandle handle) {
    if (handle != null) {
      handle.start();
      handle.switchToIndeterminate();
    }
    KeysToStringConverter keysToString = new KeysToStringConverter(reportService.getCurrentRunKey());
    ReportConfigReader configSaver = new ReportConfigReader(new TraceToStringConverter(), keysToString);
    CrueIOResu<ReportConfigContrat> read = configSaver.read(file);
    if (read.getMetier() != null && read.getMetier().getType().isPlanimetry()) {
      ReportPlanimetryConfigPersist persist = (ReportPlanimetryConfigPersist) read.getMetier();
      persist.reinitContent();
      read.setMetier(persist.createConfig());
    }
    return read;
  }
}
