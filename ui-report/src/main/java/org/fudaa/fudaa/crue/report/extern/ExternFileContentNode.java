/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.extern;

import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;

import java.awt.datatransfer.Transferable;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public class ExternFileContentNode extends AbstractNodeFirable {
    final PerspectiveServiceReport perspectiveServiceReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);

    public ExternFileContentNode(ExternFileContent content) {
        super(Children.LEAF, Lookups.singleton(content));
        setValue(DefaultNodePasteType.PROP_USE_INDEX_AS_DISPLAY_NAME, Boolean.TRUE);
    }

    @Override
    public boolean isEditMode() {
        return perspectiveServiceReport.isInEditMode();
    }

    @Override
    public PasteType getDropType(Transferable t, int action, int index) {
        if (isEditMode()) {
            Node dragged = DefaultNodePasteType.getMovedNodeInTransferable(t);
            return dragged==null?null:new DefaultNodePasteType(dragged, this);
        }
        return null;
    }



    @Override
    public boolean canDestroy() {
        return isEditMode();
    }

    @Override
    public boolean canCopy() {
        return true;
    }

    @Override
    public boolean canCut() {
        return isEditMode();
    }

    @Override
    protected Sheet createSheet() {
        ExternFileContent brancheConfig = getLookup().lookup(ExternFileContent.class);
        PropertyNodeBuilder builder = new PropertyNodeBuilder();
        List<PropertySupportReflection> createFromPropertyDesc = builder.createFromPropertyDesc(DecimalFormatEpsilonEnum.PRESENTATION, brancheConfig, this);
        Sheet res = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        res.put(set);
        for (PropertySupportReflection propertySupportReflection : createFromPropertyDesc) {
            set.put(propertySupportReflection);
            PropertyCrueUtils.configureCustomEditor(propertySupportReflection);
            PropertyCrueUtils.configureNoEditAsText(propertySupportReflection);
        }
        return res;
    }
}
