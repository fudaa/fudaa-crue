/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import java.io.File;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;
import org.fudaa.dodico.crue.projet.report.ReportXstreamReaderWriter;
import org.fudaa.dodico.crue.projet.report.formule.FormuleParametersExpr;
import org.fudaa.dodico.crue.projet.report.formule.FormuleReader;
import org.fudaa.dodico.crue.projet.report.formule.FormuleServiceContent;
import org.fudaa.dodico.crue.projet.report.persist.FormulesDao;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ReportFormuleReaderSaver {

  final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);
  final ReportGlobalServiceContrat reportGlobalServiceContrat = Lookup.getDefault().lookup(ReportGlobalServiceContrat.class);

  public CtuluLog save(File targetFile, List<FormuleParametersExpr> formules) {
    FormulesDao dao = new FormulesDao();
    ConnexionInformation connexionInformation = configurationManagerService.getConnexionInformation();
    dao.setAuteurDerniereModif(connexionInformation.getCurrentUser());
    dao.setDateDerniereModif(connexionInformation.getCurrentDate());
    dao.setFormules(formules);
    FormuleReader reader = new FormuleReader();
    ReportXstreamReaderWriter<FormulesDao> readerWriter = new ReportXstreamReaderWriter<>("Variable-FCs", ReportXstreamReaderWriter.VERSION,
            reader);
    CrueIOResu<FormulesDao> res = new CrueIOResu<>(dao);
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc(org.openide.util.NbBundle.getMessage(ReportFormuleReaderSaver.class, "SaveFormules.LogDisplayName"));
    readerWriter.writeXMLMetier(res, targetFile, log, null);
    return log;
  }

  public CrueIOResu<List<FormuleParametersExpr>> read(File targetFile) {
    FormuleReader reader = new FormuleReader();
    return reader.read(targetFile);
  }

  public CrueIOResu<FormuleServiceContent> readContent(File targetFile) {
    FormuleReader reader = new FormuleReader();
    return reader.readContent(targetFile, reportGlobalServiceContrat);
  }

}
