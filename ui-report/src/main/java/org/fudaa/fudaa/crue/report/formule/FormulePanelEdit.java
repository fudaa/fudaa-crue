/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.formule;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuGridLayout;
import java.awt.BorderLayout;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.ItemConstant;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.projet.report.formule.FormuleParameters;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import static org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder.getDialogHelpCtxId;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public abstract class FormulePanelEdit<N extends FormuleNodeParameters> {

  protected final N node;
  protected final Node parentNode;
  protected final Set<String> otherFormuleNames = new HashSet<>();
  protected final Set<String> toAvoid = new HashSet<>();
  protected final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);

  protected abstract static class Design {

    public final JPanel pnMain = new JPanel();
    public final JPanel pnName = new JPanel();
    public final JTextField tfName = new JTextField(15);
    public boolean nameValid = true;

    public boolean isAllValid() {
      return nameValid && isParameterValid();
    }

    protected abstract boolean isParameterValid();

    public Design() {
      pnMain.setBorder(BuBorders.EMPTY5555);
      pnMain.setLayout(new BorderLayout(5, 15));
      pnName.setLayout(new BuGridLayout(2, 5, 5));
      pnName.add(new JLabel(NbBundle.getMessage(FormulePanelEdit.class, "formule.Name")));
      pnName.add(tfName);
      pnMain.add(pnName, BorderLayout.NORTH);
      String id = getDialogHelpCtxId("vueVariable_EditerFormule", PerspectiveEnum.REPORT);
      SysdocUrlBuilder.installHelpShortcut(pnMain,id);
    }
  }

  public FormulePanelEdit(N node) {
    this.node = node;
    this.parentNode = null;
    List<FormuleNodeParameters> allNodes = FormuleContentChildFactory.getAllNodes(node);
    for (FormuleNodeParameters formuleNodeParameters : allNodes) {
      if (formuleNodeParameters != node) {
        FormuleParameters lookup = formuleNodeParameters.getLookup().lookup(FormuleParameters.class);
        otherFormuleNames.add(lookup.getNom());
      }
    }
    CrueConfigMetier ccm = reportService.getCcm();
    for (ItemVariable itemVariable : ccm.getPropDefinition().values()) {
      toAvoid.add(itemVariable.getDisplayNom());
    }
    for (EnumTypeLoi typeLoi : ccm.getConfLoi().keySet()) {
      toAvoid.add(typeLoi.getNom());
    }
    for (ItemConstant itemConstant : ccm.getPropConstante().values()) {
      toAvoid.add(itemConstant.getDisplayNom());
    }
  }

  public FormulePanelEdit(Node parentNode) {
    if (parentNode instanceof FormuleNodeParameters) {
      this.parentNode = null;
      this.node = (N) parentNode;
    } else {
      this.parentNode = parentNode;
      this.node = null;
    }
    List<FormuleNodeParameters> allNodes = FormuleContentChildFactory.getAllNodes(parentNode);
    for (FormuleNodeParameters formuleNodeParameters : allNodes) {
      if (formuleNodeParameters != null) {
        FormuleParameters lookup = formuleNodeParameters.getLookup().lookup(FormuleParameters.class);
        otherFormuleNames.add(lookup.getNom());
      }
    }
  }

  protected boolean isNewFormule() {
    return node == null;
  }

  protected void initNamePart(Design design, NotifyDescriptor descriptor) {
    if (node != null) {
      FormuleParameters lookup = node.getLookup().lookup(FormuleParameters.class);
      design.tfName.setText(lookup.getNom());
    }
    final NameValidDocumentListener nameValidDocumentListener = new NameValidDocumentListener(design, descriptor, otherFormuleNames, toAvoid);
    nameValidDocumentListener.update();
    design.tfName.getDocument().addDocumentListener(nameValidDocumentListener);

  }

  public abstract void show();

  private static class NameValidDocumentListener implements DocumentListener {

    private final Design design;
    private final NotifyDescriptor descriptor;
    private final Set<String> alreadyUsed;
    private final Set<String> toAvoid;

    public NameValidDocumentListener(Design design, NotifyDescriptor descriptor, Set<String> alreadyUsed, Set<String> toAvoid) {
      this.design = design;
      this.toAvoid = toAvoid;
      this.alreadyUsed = alreadyUsed;
      this.descriptor = descriptor;
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
      update();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
      update();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
      update();
    }

    protected void update() {
      String name = StringUtils.trim(design.tfName.getText());
      if (name.length() == 0) {
        design.nameValid = false;
        descriptor.getNotificationLineSupport().setErrorMessage(NbBundle.getMessage(FormulePanelEditExpr.class, "variable.empty"));
      } else if (alreadyUsed.contains(name)) {
        design.nameValid = false;
        descriptor.getNotificationLineSupport().setErrorMessage(NbBundle.getMessage(FormulePanelEditExpr.class, "variable.nameUsed"));
      } else if (toAvoid.contains(name)) {
        design.nameValid = false;
        descriptor.getNotificationLineSupport().setErrorMessage(NbBundle.getMessage(FormulePanelEditExpr.class, "variable.reserved"));
      } else {
        design.nameValid = true;
        descriptor.getNotificationLineSupport().setErrorMessage(null);
        descriptor.getNotificationLineSupport().clearMessages();
      }
      descriptor.setValid(design.isAllValid());
    }
  }
}
