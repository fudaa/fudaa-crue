/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import java.io.File;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.fudaa.crue.report.view.ReportViewLine;

/**
 *
 * @author Frederic Deniger
 */
public class IndexSaverCallable implements SaveCallableContrat {

  private final List<ReportViewLine> allLines;
  private final File targetIndexFile;
  private final String type;

  public IndexSaverCallable(List<ReportViewLine> allLines, File targetIndexFile, String type) {
    this.allLines = allLines;
    this.targetIndexFile = targetIndexFile;
    this.type = type;
  }

  @Override
  public File getTargetFile() {
    return targetIndexFile;
  }

  @Override
  public CtuluLog call() throws Exception {
    ReportIndexReaderSaverUI indexSaver = new ReportIndexReaderSaverUI();
    return indexSaver.save(targetIndexFile, allLines, type);
  }
}
