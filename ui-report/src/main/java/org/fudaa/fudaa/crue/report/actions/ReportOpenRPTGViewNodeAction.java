/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.actions;

import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.ReportRPTGConfig;
import org.fudaa.dodico.crue.projet.report.data.ReportExpressionHelper;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.report.rptg.ReportRPTGTopComponent;
import org.fudaa.fudaa.crue.report.service.ReportTemplateFileProvider;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

import javax.swing.*;
import java.util.Collection;

/**
 *
 * @author Frederic Deniger
 */
public class ReportOpenRPTGViewNodeAction extends AbstractReportOpenTopComponentNodeAction {


  public ReportOpenRPTGViewNodeAction() {
    this(NbBundle.getMessage(ReportOpenRPTGViewNodeAction.class, "OpenRPTG.ActionName"));
  }

  private ReportOpenRPTGViewNodeAction(final String name) {
    super(name, ReportRPTGTopComponent.MODE);
  }

  @Override
  protected TopComponent createNewTopComponentFor(final Node selectedNode) {
    final EMH emh = selectedNode.getLookup().lookup(EMH.class);
    if (emh != null) {
      final ReportRPTGTopComponent tc = create();
      tc.setReportConfig(createDefaultContent(emh), true);
      return tc;
    }

    return null;
  }

  @Override
  protected void postOpenNewTopComponent(final TopComponent openNew) {
    ((ReportRPTGTopComponent) openNew).chooseVariable();
  }

  public static void open(final Collection<? extends EMH> emhs) {
    if (emhs == null) {
      return;
    }
    final ReportOpenRPTGViewNodeAction action = SystemAction.get(ReportOpenRPTGViewNodeAction.class);
    action.performAction(NodeHelper.createNodesWithLookup(emhs));

  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    if (activatedNodes != null && activatedNodes.length == 1) {
      final EMH emh = activatedNodes[0].getLookup().lookup(EMH.class);
      return emh != null;
    }
    return false;
  }

    private ReportRPTGConfig createDefaultContent(final EMH emh) {
    final ReportTemplateFileProvider template = new ReportTemplateFileProvider();
    ReportRPTGConfig content = template.loadTemplate(ReportContentType.RPTG);
    if (content == null) {
      content = new ReportRPTGConfig();
      content.getLoiLegendConfig().setTitleConfig(createLabelNomEMH());
    }
    content.setEmh(emh.getNom());
    return content;
  }

  private static LabelConfig createLabelConfig(final ReportLabelContent label) {
    final LabelConfig res = new LabelConfig();
    res.setKey(label);
    return res;
  }

  private static LabelConfig createLabelNomEMH() {
    final ReportLabelContent label = new ReportLabelContent();
    label.setContent(new ReportVariableKey(ReportVariableTypeEnum.EXPR, ReportExpressionHelper.EXPR_NOM));
    label.setUnit(false);
    final LabelConfig res = createLabelConfig(label);
    res.setHorizontalAlignment(SwingConstants.CENTER);
    return res;
  }

  public ReportRPTGTopComponent create() {
    ReportRPTGTopComponent res = getMainTopComponentAvailable();
    if (res == null) {
      res = new ReportRPTGTopComponent();
    }
    return res;
  }
}
