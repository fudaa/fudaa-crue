/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.node;

import javax.swing.Action;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.fudaa.crue.report.actions.ReportOpenMultiVarViewNodeAction;
import org.fudaa.fudaa.crue.report.actions.ReportOpenProfilLongitudinalViewNodeAction;
import org.fudaa.fudaa.crue.report.actions.ReportOpenRPTGViewNodeAction;
import org.fudaa.fudaa.crue.report.actions.ReportOpenTemporalViewNodeAction;
import org.fudaa.fudaa.crue.report.export.ReportExportNodeAction;
import org.openide.nodes.Children;
import org.openide.util.actions.SystemAction;

/**
 *
 * @author Frederic Deniger
 */
public class ReportNodeCatEMHBranche extends ReportNodeEMH {

  public ReportNodeCatEMHBranche(Children children, CatEMHBranche emh, String displayName) {
    super(children, emh, displayName);
  }

  public ReportNodeCatEMHBranche(Children children, CatEMHBranche emh) {
    super(children, emh);
  }

  @Override
  public Action[] getActions(boolean context) {
    return new Action[]{
              SystemAction.get(ReportExportNodeAction.class),
              null,
              SystemAction.get(ReportOpenProfilLongitudinalViewNodeAction.class),
              SystemAction.get(ReportOpenTemporalViewNodeAction.class),
              SystemAction.get(ReportOpenMultiVarViewNodeAction.class),
              null,
              SystemAction.get(ReportOpenRPTGViewNodeAction.class)
            };
  }
}
