/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.time;

import com.jidesoft.swing.RangeSlider;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLib;
import org.apache.commons.lang3.IntegerRange;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.ComboBoxHelper;
import org.fudaa.fudaa.crue.common.helper.ToStringTransfomerCellRenderer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.openide.util.HelpCtx;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

/**
 *
 * @author Frederic Deniger
 */
public class ReportTimeReduceRangePanel implements HelpCtx.Provider {

  RangeSlider rangeSlider;
  private final List values;
  private final ToStringTransformer toStringTransformer;
  IntegerRange range;
  JComboBox cbMin;
  JComboBox cbMax;

  public ReportTimeReduceRangePanel(List values, ToStringTransformer toStringTransformer, IntegerRange initRange) {
    this.values = values;
    this.toStringTransformer = toStringTransformer;
    this.range = initRange;
    if (range == null) {
      if (values.isEmpty()) {
        range = IntegerRange.of(0, 0);
      } else {
        range = IntegerRange.of(0, values.size() - 1);
      }
    }
  }
  JPanel panel;

  public JPanel getPanel() {
    if (panel == null) {
      createPanel();
    }
    return panel;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getDialogHelpCtxId("vueGestionnaireTemporel_ReductionIntervalleTemps", PerspectiveEnum.REPORT));
  }

  private JPanel createMinMaxLabels() {
    JLabel lbMin = new JLabel();
    lbMin.setFont(BuLib.deriveFont(lbMin.getFont(), -1));

    JLabel lbMax = new JLabel();
    lbMax.setFont(BuLib.deriveFont(lbMax.getFont(), -1));
    JPanel pnMinMax = new JPanel(new BorderLayout());
    pnMinMax.setBorder(BorderFactory.createEmptyBorder(0, 1, 0, 3));
    pnMinMax.add(lbMin, BorderLayout.WEST);
    pnMinMax.add(lbMax, BorderLayout.EAST);
    if (!values.isEmpty()) {
      lbMin.setText(toStringTransformer.transform(values.get(0)));
      lbMin.setToolTipText(lbMin.getText());
      lbMax.setText(toStringTransformer.transform(values.get(values.size() - 1)));
      lbMax.setToolTipText(lbMax.getText());
    }
    return pnMinMax;
  }

  void createPanel() {
    panel = new JPanel(new BuGridLayout(2, 2, 2, true, false, false, false));
    if (values.isEmpty()) {
      return;
    }
    panel.add(createComboxesPanel());
    rangeSlider = new RangeSlider(0, values.size() - 1, range.getMinimum(), range.getMaximum());
    JPanel pnSlider = new JPanel(new BuGridLayout(1, 2, 2, true, false));
    pnSlider.add(rangeSlider);
    rangeSlider.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        int min = rangeSlider.getLowValue();
        final int max = rangeSlider.getHighValue();
        cbMin.setSelectedIndex(min);
        cbMax.setSelectedIndex(max);
      }
    });
    JPanel pnMinMax = createMinMaxLabels();
    pnSlider.add(pnMinMax, BorderLayout.SOUTH);
    SysdocUrlBuilder.installHelpShortcut(panel, getHelpCtx().getHelpID());
    panel.add(pnSlider);
	
  }

  private JPanel createComboxesPanel() {
    final ToStringTransfomerCellRenderer cellRenderer = new ToStringTransfomerCellRenderer(toStringTransformer);
    cbMin = new JComboBox();
    ComboBoxHelper.setDefaultModel(cbMin, values);
    cbMin.setSelectedIndex(range.getMinimum());
    cbMin.setRenderer(cellRenderer);
    cbMax = new JComboBox();
    ComboBoxHelper.setDefaultModel(cbMax, values);
    cbMax.setSelectedIndex(range.getMaximum());
    cbMax.setRenderer(cellRenderer);
    JPanel cbPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    cbPanel.add(new JLabel(org.openide.util.NbBundle.getMessage(ReportTimeReduceRangePanel.class, "MinRange.Label")));
    cbPanel.add(cbMin);
    cbPanel.add(new JLabel(org.openide.util.NbBundle.getMessage(ReportTimeReduceRangePanel.class, "MaxRange.Label")));
    cbPanel.add(cbMax);
    cbMax.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        rangeSlider.setHighValue(cbMax.getSelectedIndex());
      }
    });
    cbMin.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        rangeSlider.setLowValue(cbMin.getSelectedIndex());
      }
    });
    return cbPanel;

  }

  IntegerRange getRange() {
    int min = rangeSlider.getLowValue();
    int max = rangeSlider.getHighValue();
    return IntegerRange.of(min, max);
  }
}
