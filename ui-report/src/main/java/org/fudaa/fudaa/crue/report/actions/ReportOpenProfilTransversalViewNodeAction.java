/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.actions;

import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingConstants;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.data.ReportExpressionHelper;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.dodico.crue.projet.report.persist.ReportTransversalConfig;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.fudaa.fudaa.crue.report.service.ReportTemplateFileProvider;
import org.fudaa.fudaa.crue.report.transversal.ReportProfilTransversalTopComponent;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;

/**
 *
 * @author Frederic Deniger
 */
public class ReportOpenProfilTransversalViewNodeAction extends AbstractReportOpenTopComponentNodeAction {

  final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);

  public ReportOpenProfilTransversalViewNodeAction() {
    this(NbBundle.getMessage(ReportOpenProfilTransversalViewNodeAction.class, "OpenProfilTransversal.ActionName"));
  }

  private ReportOpenProfilTransversalViewNodeAction(final String name) {
    super(name, ReportProfilTransversalTopComponent.MODE);
  }

  @Override
  protected TopComponent createNewTopComponentFor(final Node selectedNode) {
    final EMH emh = selectedNode.getLookup().lookup(EMH.class);
    if (emh != null && isEnableFor(emh)) {
      final ReportProfilTransversalTopComponent tc = create();
      tc.setReportConfig(createDefaultContent(emh), true);
      return tc;
    }

    return null;
  }

  public static void open(final EMH emh, final boolean reopen) {
    if (emh == null) {
      return;
    }
    final ReportOpenProfilTransversalViewNodeAction action = SystemAction.get(ReportOpenProfilTransversalViewNodeAction.class);
    final AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(emh));
    action.performAction(new Node[]{node});

  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    if (activatedNodes != null && activatedNodes.length == 1) {
      final EMH emh = activatedNodes[0].getLookup().lookup(EMH.class);
      return isEnableFor(emh);
    }
    return false;
  }

    private ReportTransversalConfig createDefaultContent(final EMH emh) {
    final ReportTemplateFileProvider template = new ReportTemplateFileProvider();
    ReportTransversalConfig content = template.loadTemplate(ReportContentType.TRANSVERSAL);
    if (content == null) {
      content = new ReportTransversalConfig();
      content.addProfilVariable(new ReportRunVariableKey(reportService.getRunCourant().getRunKey(), ReportVariableTypeEnum.READ,
              CrueConfigMetierConstants.PROP_Z));
      final List<LabelConfig> labels = new ArrayList<>();
      labels.add(createLabelTime());
      labels.add(createLabelConfigQ());
      labels.add(createLabelConfigZ());
      content.getLoiLegendConfig().setLabels(labels);
      content.getLoiLegendConfig().setTitleConfig(createLabelNomEMH());
    }
    content.setSectionName(emh.getNom());

    return content;
  }

  public static LabelConfig createLabelTime() {
    final ReportLabelContent label = new ReportLabelContent();
    label.setContent(new ReportVariableKey(ReportVariableTypeEnum.TIME, ReportExpressionHelper.TIME_TEMPS_SCE));
    label.setPrefix(NbBundle.getMessage(ReportOpenProfilTransversalViewNodeAction.class, "DefaultPrefixTempsSce"));
    label.setUnit(false);
    return createLabelConfig(label);
  }

  public static LabelConfig createLabelConfigQ() {
    final ReportLabelContent label = new ReportLabelContent();
    label.setContent(new ReportVariableKey(ReportVariableTypeEnum.READ, CrueConfigMetierConstants.PROP_Q));
    label.setPrefix(NbBundle.getMessage(ReportOpenProfilTransversalViewNodeAction.class, "ProfilTransversal.DefaultPrefixDebit"));
    label.setUnit(true);
    return createLabelConfig(label);
  }

  public static LabelConfig createLabelConfigZ() {
    final ReportLabelContent label = new ReportLabelContent();
    label.setContent(new ReportVariableKey(ReportVariableTypeEnum.READ, CrueConfigMetierConstants.PROP_Z));
    label.setPrefix(NbBundle.getMessage(ReportOpenProfilTransversalViewNodeAction.class, "ProfilTransversal.DefaultPrefixNiveau"));
    label.setUnit(true);
    return createLabelConfig(label);
  }

  public static LabelConfig createLabelConfig(final ReportLabelContent label) {
    final LabelConfig res = new LabelConfig();
    res.setKey(label);
    return res;
  }

  public static LabelConfig createLabelNomEMH() {
    final ReportLabelContent label = new ReportLabelContent();
    label.setContent(new ReportVariableKey(ReportVariableTypeEnum.EXPR, ReportExpressionHelper.EXPR_NOM));
//    label.setPrefix("Nom=");
    label.setUnit(false);
    final LabelConfig res = createLabelConfig(label);
    res.setHorizontalAlignment(SwingConstants.CENTER);
    return res;
  }

  public static boolean isEnableFor(final EMH emh) {
    return emh.getCatType().equals(EnumCatEMH.SECTION);
  }

  public ReportProfilTransversalTopComponent create() {
    ReportProfilTransversalTopComponent res = super.getMainTopComponentAvailable();
    if (res == null) {
      res = new ReportProfilTransversalTopComponent();
    }
    return res;
  }
}
