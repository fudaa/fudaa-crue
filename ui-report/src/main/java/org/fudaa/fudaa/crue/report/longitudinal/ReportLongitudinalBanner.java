/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.longitudinal;

import gnu.trove.TIntObjectHashMap;
import gnu.trove.TIntObjectIterator;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.List;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBannerConfig;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBrancheCartouche;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalConfig;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalPositionBrancheContent;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalPositionSection;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalPositionSectionByRun;
import org.fudaa.ebli.courbe.EGHorizontalBanner;
import org.fudaa.ebli.courbe.EGRepere;
import org.fudaa.ebli.trace.TraceBox;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLongitudinalBanner implements EGHorizontalBanner {

  final ReportLongitudinalConfig config;
  final ReportLongitudinalPositionBrancheContent content;
  final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);

  public ReportLongitudinalBanner(ReportLongitudinalConfig config, ReportLongitudinalPositionBrancheContent content) {
    this.config = config;
    this.content = content;
  }

  @Override
  public void dessine(Graphics2D g, EGRepere _f) {
    if (config == null || content == null || !reportService.isRunLoaded()) {
      return;
    }
    Font old = g.getFont();

    final ReportLongitudinalBannerConfig bannerConfig = config.getBannerConfig();
    if (bannerConfig.isBrancheVisible()) {
      TraceBox brancheBox = bannerConfig.getBrancheBox();
      FontMetrics fontMetrics = g.getFontMetrics(bannerConfig.getBrancheFont());
      g.setFont(bannerConfig.getBrancheFont());
      int hBranche = fontMetrics.getHeight() + brancheBox.getTotalHeightMargin();
      int yMin = _f.getH() - hBranche;
      int yMinNeg = _f.getH() - hBranche * 2;
      int currentY = yMin;
      List<ReportLongitudinalBrancheCartouche> cartouches = content.getCartouches();
      for (ReportLongitudinalBrancheCartouche cartouche : cartouches) {
        if (!cartouche.containsSection()) {
          continue;
        }
        CtuluRange range = _f.getXAxe().getRange().intersectWith(cartouche.getDisplayRange());
        if (!range.isNill()) {
          if (cartouche.isNegativeDecal()) {
            if (currentY == yMin) {
              currentY = yMinNeg;
            } else {
              currentY = yMin;
            }
          }
          String brancheName = cartouche.getBrancheName();
          int xMin = _f.getXEcran(cartouche.getXpMinDisplay());
          int xmax = _f.getXEcran(cartouche.getXpMaxDisplay());
          //pour dessiner le nom au milieu du rectangle visible
          int xmiddle = _f.getXEcran((range.getMax() + range.getMin()) / 2);

          brancheBox.getTraceLigne().dessineRectangle(g, xMin, currentY, xmax - xMin - 1, hBranche);
          Rectangle view = new Rectangle(xMin, currentY, xmax - xMin - 1, hBranche);//pour s'assurer de dessiner dans le cadre.
          brancheBox.paintBox(g, xmiddle, currentY, brancheName, view);
        }
      }
    }
    g.setFont(old);
    if (bannerConfig.isSectionVisible()) {

      TIntObjectHashMap<StringBuilder> labels = buildLabelsPositionForSection(_f, bannerConfig.isSectionAmontAvalOnly());
      TraceBox box = bannerConfig.getSectionBox();
      int yMin = _f.getH() - getHeightNeeded(g);
      TIntObjectIterator<StringBuilder> iterator = labels.iterator();
      g.setFont(bannerConfig.getSectionFont());
      for (int i = labels.size(); i-- > 0;) {
        iterator.advance();
        String display = iterator.value().toString();

        int pos = iterator.key();
        box.paintBox(g, pos, yMin, display);
      }
    }
    g.setFont(old);
  }

  @Override
  public int getHeightNeeded(Graphics2D g) {
    if (config == null || content == null || !reportService.isRunLoaded()) {
      return 0;
    }
    int h = 0;
    final ReportLongitudinalBannerConfig bannerConfig = config.getBannerConfig();
    if (bannerConfig.isBrancheVisible() && bannerConfig.isSectionVisible()) {
      h = bannerConfig.getVerticalSpace();
    }
    if (bannerConfig.isBrancheVisible()) {

      TraceBox brancheBox = bannerConfig.getBrancheBox();

      FontMetrics fontMetrics = g.getFontMetrics(bannerConfig.getBrancheFont());
      int deltaForBranche = brancheBox.getTotalHeightMargin() + fontMetrics.getHeight();
      boolean negativeXp = config.containsNegativeDecal();
      if (negativeXp) {
        deltaForBranche = deltaForBranche * 2;//sur 2 lignes.
      }
      h = h + deltaForBranche;
    }
    if (bannerConfig.isSectionVisible()) {
      boolean onlyAmontAval = bannerConfig.isSectionAmontAvalOnly();
      TraceBox box = bannerConfig.getSectionBox();
      h = h + box.getTotalHeightMargin();
      FontMetrics fontMetrics = g.getFontMetrics(bannerConfig.getSectionFont());
      List<ReportLongitudinalBrancheCartouche> cartouches = content.getCartouches();
      final ReportRunKey runKey = reportService.getRunCourant().getRunKey();
      int maxH = 0;
      for (ReportLongitudinalBrancheCartouche cartouche : cartouches) {
        ReportLongitudinalPositionSectionByRun sectionPositions = cartouche.getSectionPositions(runKey);
        if (sectionPositions == null) {
          continue;
        }
        List<ReportLongitudinalPositionSection> positions = sectionPositions.getSectionDisplayPositions();
        for (ReportLongitudinalPositionSection position : positions) {
          if (onlyAmontAval && !position.getName().isAmontOrAval()) {
            continue;
          }
          String nom = position.getName().getNom();
          maxH = Math.max(maxH, fontMetrics.stringWidth(nom));
        }
      }
      h = h + maxH;
    }
    return h;
  }

  private TIntObjectHashMap<StringBuilder> buildLabelsPositionForSection(EGRepere _f, boolean onlyAmontAval) {
    TIntObjectHashMap<StringBuilder> labels = new TIntObjectHashMap<>();
    final ReportRunKey runKey = reportService.getRunCourant().getRunKey();
    List<ReportLongitudinalBrancheCartouche> cartouches = content.getCartouches();
    for (ReportLongitudinalBrancheCartouche cartouche : cartouches) {
      ReportLongitudinalPositionSectionByRun sectionPositions = cartouche.getSectionPositions(runKey);
      if (sectionPositions == null) {
        continue;
      }
      List<ReportLongitudinalPositionSection> positions = sectionPositions.getSectionDisplayPositions();
      for (ReportLongitudinalPositionSection position : positions) {
        final CatEMHSection section = position.getName();
        if (onlyAmontAval && !section.isAmontOrAval()) {
          continue;
        }
        String nom = section.getNom();
        if (_f.getXAxe().containsPoint(position.getXpDisplay())) {
          int xEcran = _f.getXEcran(position.getXpDisplay());
          StringBuilder builder = labels.get(xEcran);
          if (builder == null) {
            builder = new StringBuilder();
            builder.append(nom);
            labels.put(xEcran, builder);
          } else {
            builder.append('\n').append(nom);
          }
        }
      }
    }
    return labels;
  }
}
