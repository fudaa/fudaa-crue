/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.config;

import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLabelContentConfig implements Cloneable {

  ReportLabelContent content;
  LabelConfig config = new LabelConfig();

  public ReportLabelContent getContent() {
    return content;
  }

  public ReportLabelContentConfig() {
  }

  public ReportLabelContentConfig(ReportLabelContent content) {
    this.content = content;
  }

  public ReportLabelContentConfig(ReportLabelContent content, LabelConfig config) {
    this.content = content;
    this.config = config;
  }

  @Override
  public ReportLabelContentConfig clone() {
    try {
      ReportLabelContentConfig res = (ReportLabelContentConfig) super.clone();
      res.config = config.duplicate();
      res.content = content.copy();
      return res;
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why");
  }

  public void setContent(ReportLabelContent content) {
    this.content = content;
  }

  public LabelConfig getConfig() {
    return config;
  }

  public void setConfig(LabelConfig config) {
    this.config = config;
  }
}
