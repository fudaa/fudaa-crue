/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.actions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.comparator.ToStringInternationalizableComparator;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.ReportMultiVarConfig;
import org.fudaa.fudaa.crue.common.helper.ToStringInternationalizableCellRenderer;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.view.ChooseInListHelper;
import org.fudaa.fudaa.crue.report.multivar.ReportMultiVarTopComponent;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.fudaa.fudaa.crue.report.service.ReportTemplateFileProvider;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;

/**
 *
 * @author Frederic Deniger
 */
public class ReportOpenMultiVarViewNodeAction extends AbstractReportOpenTopComponentNodeAction {

  final ReportResultProviderService reportResultService = Lookup.getDefault().lookup(ReportResultProviderService.class);

  public ReportOpenMultiVarViewNodeAction() {
    this(NbBundle.getMessage(ReportOpenMultiVarViewNodeAction.class, "OpenMulitVar.ActionName"));
  }

  private ReportOpenMultiVarViewNodeAction(final String name) {
    super(name, ReportMultiVarTopComponent.MODE);
  }

  private ReportMultiVarTopComponent create() {
    ReportMultiVarTopComponent res = super.getMainTopComponentAvailable();
    if (res == null) {
      res = new ReportMultiVarTopComponent();
    }
    return res;
  }

  @Override
  protected TopComponent createNewTopComponentFor(final Node[] activatedNodes) {
    final Set<EMH> emhs = NodeHelper.getEMHs(activatedNodes);
    if (!emhs.isEmpty()) {
      final List<EMH> choosenEMH = chooseEMHOfSameType(emhs);
      if (CollectionUtils.isNotEmpty(choosenEMH)) {
        final ReportMultiVarTopComponent tc = create();
        tc.setReportConfig(createDefaultContent(choosenEMH), true);
        return tc;
      }
    }
    return null;
  }

  @Override
  protected TopComponent createNewTopComponentFor(final Node selectedNode) {
    final EMH emh = selectedNode.getLookup().lookup(EMH.class);
    if (emh != null) {
      final ReportMultiVarTopComponent tc = create();
      tc.setReportConfig(createDefaultContent(Collections.singletonList(emh)), true);
      return tc;
    }

    return null;
  }

  @Override
  protected void postOpenNewTopComponent(final TopComponent openNew) {
    ((ReportMultiVarTopComponent) openNew).chooseVariable();
  }

  public static void open(final EMH emh, final boolean reopen) {
    if (emh == null) {
      return;
    }
    final ReportOpenMultiVarViewNodeAction action = SystemAction.get(ReportOpenMultiVarViewNodeAction.class);
    final AbstractNode node = new AbstractNode(Children.LEAF, Lookups.fixed(emh));
    action.performAction(new Node[]{node});
  }

  public static void open(final Collection<EMH> emhs) {
    if (emhs == null) {
      return;
    }
    final ReportOpenMultiVarViewNodeAction action = SystemAction.get(ReportOpenMultiVarViewNodeAction.class);
    action.performAction(NodeHelper.createNodesWithLookup(emhs));
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    return NodeHelper.containEMH(activatedNodes);
  }

    private ReportMultiVarConfig createDefaultContent(final List<EMH> emhs) {
    final ReportTemplateFileProvider template = new ReportTemplateFileProvider();
    ReportMultiVarConfig content = template.loadTemplate(ReportContentType.MULTI_VAR);
    if (content == null) {
      content = new ReportMultiVarConfig();
    }
    content.setEMHAndAddRunCourant(TransformerHelper.toNom(emhs), reportResultService);
    return content;
  }

  public static List<EMH> chooseEMHOfSameType(final Set<EMH> emhs) {
    final Map<EnumCatEMH, List<EMH>> emhByCat = new EnumMap<>(EnumCatEMH.class);
    for (final EMH emh : emhs) {
      List<EMH> list = emhByCat.get(emh.getCatType());
      if (list == null) {
        list = new ArrayList<>();
        emhByCat.put(emh.getCatType(), list);
      }
      list.add(emh);
    }
    List<EMH> choosenEMH = null;
    if (emhByCat.size() > 1) {
      final List<EnumCatEMH> cats = new ArrayList<>(emhByCat.keySet());
      Collections.sort(cats,
              ToStringInternationalizableComparator.INSTANCE);
      final ChooseInListHelper<EnumCatEMH> chooser = new ChooseInListHelper<>();
      chooser.setDialogTitle(NbBundle.getMessage(ReportOpenMultiVarViewNodeAction.class, "SeveralCategory.Choose"));
      chooser.setDialogDescription(NbBundle.getMessage(ReportOpenMultiVarViewNodeAction.class, "SeveralCategory.ChooseDescription"));
      chooser.setLabel(NbBundle.getMessage(ReportOpenMultiVarViewNodeAction.class, "SeveralCategory.ChooseLabel"));
      chooser.setRenderer(new ToStringInternationalizableCellRenderer());
      final EnumCatEMH choose = chooser.choose(cats, null);
      if (choose != null) {
        choosenEMH = emhByCat.get(choose);
      }
    } else {
      choosenEMH = emhByCat.values().iterator().next();
    }
    Collections.sort(choosenEMH, ObjetNommeByNameComparator.INSTANCE);
    return choosenEMH;
  }
}
