package org.fudaa.fudaa.crue.report.temporal;

import com.memoire.bu.BuGridLayout;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.result.OrdResExtractor;
import org.fudaa.dodico.crue.projet.report.data.*;
import org.fudaa.dodico.crue.projet.report.persist.ReportTemporalConfig;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.courbe.EGAxeVerticalPersist;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.fudaa.crue.aoc.service.LhptService;
import org.fudaa.fudaa.crue.aoc.service.LhptValuesDisplayer;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.MultiEntriesChooser;
import org.fudaa.fudaa.crue.loi.res.CourbesUiResController;
import org.fudaa.fudaa.crue.report.AbstractReportTimeViewTopComponent;
import org.fudaa.fudaa.crue.report.data.ReportVariableCellRenderer;
import org.fudaa.fudaa.crue.report.export.ReportTemporalExportAction;
import org.fudaa.fudaa.crue.report.helper.ByRunSelector;
import org.fudaa.fudaa.crue.report.helper.ByRunSelector.Line;
import org.fudaa.fudaa.crue.report.helper.ChooseEMHByTypeHelper;
import org.fudaa.fudaa.crue.report.service.ReportFormuleService;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.report//ReportTemporalTopComponent//EN",
    autostore = false)
@TopComponent.Description(preferredID = ReportTemporalTopComponent.TOPCOMPONENT_ID,
    persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ReportTemporalTopComponent.MODE, openAtStartup = false, position = 1)
public final class ReportTemporalTopComponent extends AbstractReportTimeViewTopComponent<ReportTemporalConfig, ReportTemporalGrapheBuilder>
    implements CtuluImageProducer, LhptValuesDisplayer {
  public static final String MODE = "report-temporal";
  public static final String TOPCOMPONENT_ID = "ReportTemporalTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  final ReportFormuleService reportFormuleService = Lookup.getDefault().lookup(ReportFormuleService.class);
  final LhptService lhptService = Lookup.getDefault().lookup(LhptService.class);

  /**
   * Contient toutes les données qui vont bien
   */
  public ReportTemporalTopComponent() {
    setName(NbBundle.getMessage(ReportTemporalTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ReportTemporalTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueTemporel", PerspectiveEnum.REPORT);
  }

  @Override
  protected ReportTemporalGrapheBuilder createBuilder() {
    return new ReportTemporalGrapheBuilder(profilUiController, loiLegendManager);
  }

  @Override
  public BufferedImage produceImage(final Map _params) {
    return profilUiController.produceImage(_params);
  }

  @Override
  public BufferedImage produceImage(final int _w, final int _h, final Map _params) {
    return profilUiController.produceImage(_w, _h, _params);
  }

  @Override
  public Dimension getDefaultImageDimension() {
    return profilUiController.getDefaultImageDimension();
  }

  @Override
  protected void buildComponents() {
    initComponents();
    final JPanel pnTable = new JPanel(new BorderLayout(5, 5));
    profilUiController.installComboxSelector();
    pnTable.add(profilUiController.getTableGraphePanel());
    final JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, loiLegendManager.getPanel(), pnTable);
    splitPane.setDividerLocation(550);
    splitPane.setContinuousLayout(true);
    splitPane.setOneTouchExpandable(true);
    add(splitPane);
  }

  @Override
  protected CourbesUiResController createCourbesUiController() {
    final CourbesUiResController res = new ReportTemporalCourbesUiResController();
    res.addToolbarAction(new ReportTemporalExportAction(this));
    return res;
  }

  /**
   * we
   *
   * @param scenario le scenario mise à jour
   */
  @Override
  public void lhptUpdated(final ManagerEMHScenario scenario) {
    if (scenario.getNom() != null && CollectionUtils.isNotEmpty(content.getLhptLois()) && scenario.getNom().equals(reportService.getRunCourant().getManagerScenario().getNom())) {
      final List<Pair<ResultatTimeKey, Long>> timeByKey = reportService.getTimeByKeyList();
      if (!timeByKey.isEmpty() && timeByKey.get(0).first.isTransitoire()) {
        propagateChange();
      }
    }
  }

  @Override
  public List<EbliActionInterface> getMainActions() {
    final EbliActionSimple addEMHAction = new EbliActionSimple(NbBundle.getMessage(ReportTemporalTopComponent.class, "AddEmh.ButtonName"), null, "ADD_EMH") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        addEMH();
      }
    };
    final EbliActionSimple chooseVariables = new EbliActionSimple(NbBundle.getMessage(ReportTemporalTopComponent.class, "ChooseVariable.ButtonName"), null,
        "CHOOSE_VARIABLES") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        chooseVariable();
      }
    };
    final EbliActionSimple chooseLhptLois = new EbliActionSimple(NbBundle.getMessage(ReportTemporalTopComponent.class, "ChooseLhpt.ButtonName"), null,
        "CHOOSE_LHPT") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        chooseLhpt();
      }
    };
    return Arrays.asList(addEMHAction, chooseVariables, chooseLhptLois, createConfigExternAction());
  }

  private void chooseLhpt() {
    final List<String> loisSectionsT = lhptService.getLoiSectionTFor(reportService.getRunCourant().getManagerScenario());
    if (loisSectionsT.isEmpty()) {
      DialogHelper.showWarn(NbBundle.getMessage(ReportTemporalTopComponent.class, "ChooseLhpt.NoLoiAvailables"));
      return;
    }
    final List<String> selectedlhtpLois = content.getLhptLois();
    final MultiEntriesChooser<String> chooser = new MultiEntriesChooser<>(loisSectionsT);
    chooser.setPreSelectedEntries(selectedlhtpLois);
    final boolean ok = DialogHelper.showQuestionAndSaveDialogConf(
        NbBundle.getMessage(ReportTemporalTopComponent.class, "ChooseLhpt.DialogTitle"), chooser.getPanel(), getClass().getSimpleName() + ".ChooseLhpt");
    if (ok) {
      content.setLhptLois(chooser.getSelectedEntries());
      propagateChange();
    }
  }

  protected void addEMH() {
    final List<String> emhs = content.getEmhs();
    final EnumCatEMH oldUsedCat = ChooseEMHByTypeHelper.getUsedCat(reportService, content.getEmhs());
    final List<String> selectedEMH = new ChooseEMHByTypeHelper().chooseEMH(emhs);
    if (selectedEMH != null) {
      content.setEmhs(selectedEMH);
      final EnumCatEMH usedCat = ChooseEMHByTypeHelper.getUsedCat(reportService, content.getEmhs());
      if (usedCat != oldUsedCat) {
        content.getVariables().clear();
        content.getTemporalVariables().clear();
      }
      propagateChange();
    }
  }

  @Override
  public void setEditable(final boolean b) {
    super.setEditable(b);
  }

  @Override
  public void alternatifRunChanged() {
    final Set<ReportRunKey> reportRunContents = reportRunAlternatifService.getAvailableRunKeys();
    boolean modified = false;
    for (final Iterator<ReportRunVariableKey> it = content.getTemporalVariables().iterator(); it.hasNext(); ) {
      final ReportRunVariableKey reportRunVariableKey = it.next();
      if (reportRunVariableKey.getRunKey().isAlternatifRun() && !reportRunContents.contains(reportRunVariableKey.getRunKey())) {
        modified = true;
        it.remove();
      }
    }
    final Set<ReportRunVariableEmhKey> keySet = content.getCourbeconfigs().keySet();
    for (final Iterator<ReportRunVariableEmhKey> it = keySet.iterator(); it.hasNext(); ) {
      final ReportRunVariableEmhKey reportRunVariableEmhKey = it.next();
      final ReportRunKey reportRunKey = reportRunVariableEmhKey.getReportRunKey();
      if (reportRunKey.isAlternatifRun() && !reportRunContents.contains(reportRunKey)) {
        modified = true;
        it.remove();
      }
    }
    if (modified) {
      propagateChange();
    }
  }

  public void chooseVariable() {
    final String[] selectedVariablesArray = getChoosableVariables();

    final ReportVariableCellRenderer cellRenderer = new ReportVariableCellRenderer();
    final ByRunSelector selector = new ByRunSelector();
    final List<Line> createLines = selector.createLines(2);
    final int nbCheckBox = createLines.get(0).getNbComponents();
    final JPanel pn = new JPanel(new BuGridLayout(2 + nbCheckBox, 5, 5));
    pn.add(new JLabel(NbBundle.getMessage(ReportTemporalTopComponent.class, "ChooseVariable.LabelFirst")));
    final JComboBox cb1 = new JComboBox(selectedVariablesArray);
    cb1.setRenderer(cellRenderer);
    pn.add(cb1);
    createLines.get(0).addToPanel(pn);
    cb1.setSelectedItem(null);
    pn.add(new JLabel(NbBundle.getMessage(ReportTemporalTopComponent.class, "ChooseVariable.LabelSecond")));
    final JComboBox cb2 = new JComboBox(selectedVariablesArray);
    cb2.setRenderer(cellRenderer);
    cb2.setSelectedItem(null);
    pn.add(cb2);
    createLines.get(1).addToPanel(pn);
    final LinkedHashMap<String, Set<ReportRunKey>> reportRunKeyByVariable = content.getVariablesMap();
    final List<String> variables = new ArrayList<>(reportRunKeyByVariable.keySet());
    boolean alreadyDefined = false;
    if (variables.size() > 0) {
      final String var = variables.get(0);
      cb1.setSelectedItem(var);
      createLines.get(0).updateCheckBoxes(reportRunKeyByVariable.get(var));
      alreadyDefined = true;
    }
    if (variables.size() > 1) {
      final String var = variables.get(1);
      cb2.setSelectedItem(var);
      createLines.get(1).updateCheckBoxes(reportRunKeyByVariable.get(var));
      alreadyDefined = true;
    }
    if (!alreadyDefined) {
      if (createLines.size() > 0) {
        createLines.get(0).setSelected(0);
        if (createLines.size() > 1) {
          createLines.get(1).setSelected(0);
        }
      }
    }

    final boolean accepted = DialogHelper.showQuestionOkCancel(NbBundle.getMessage(ReportTemporalTopComponent.class, "ChooseVariable.ButtonName"), pn);
    if (accepted) {
      content.getVariables().clear();
      content.getTemporalVariables().clear();
      String v1 = (String) cb1.getSelectedItem();
      final String v2 = (String) cb2.getSelectedItem();

      if (v1 == null && v2 != null) {
        v1 = v2;
      }
      if (v1 != null) {
        content.getVariables().add(v1);
      }
      if (v2 != null) {
        content.getVariables().add(v2);
      }
      EGAxeVerticalPersist axeV1 = null;
      if (v1 != null) {
        axeV1 = createAxeVerticalConfig(v1);
        axeV1.setDroite(false);
        final List<ReportRunKey> selectedKeys = createLines.get(0).getSelectedKeys();
        for (final ReportRunKey reportRunKey : selectedKeys) {
          content.getTemporalVariables().add(new ReportRunVariableKey(reportRunKey, builder.getReportResultProviderService().createVariableKey(v1)));
        }
      }
      if (v2 != null) {
        final EGAxeVerticalPersist axeV2 = createAxeVerticalConfig(v2);
        if (axeV2 != axeV1) {
          axeV2.setDroite(true);
        }
        final List<ReportRunKey> selectedKeys = createLines.get(1).getSelectedKeys();
        for (final ReportRunKey reportRunKey : selectedKeys) {
          content.getTemporalVariables().add(new ReportRunVariableKey(reportRunKey, builder.getReportResultProviderService().createVariableKey(v2)));
        }
      }
      propagateChange();
    }
  }

  public String[] getChoosableVariables() throws MissingResourceException {
    final EnumCatEMH usedCat = ChooseEMHByTypeHelper.getUsedCat(reportService, content.getEmhs());
    if (CollectionUtils.isNotEmpty(content.getEmhs())
        && usedCat == null) {
      DialogHelper.showWarn(org.openide.util.NbBundle.getMessage(ChooseEMHByTypeHelper.class, "UsedEMHNotFound.warning"));
    }
    final OrdResExtractor extractor = new OrdResExtractor(getScenario().getOrdResScenario());
    final List<String> selectedVariables = extractor.getSelectableVariables(usedCat, reportFormuleService.getVariablesKeys());
    selectedVariables.add(0, null);
    return selectedVariables.toArray(new String[0]);
  }

  @Override
  protected void saveCourbeConfig(final EGCourbePersist persitUiConfig, final ReportKeyContract key) {
    if (ReportVerticalTimeKey.isTimeKey(key)) {
      content.setTimeConfig(persitUiConfig);
    } else {
      content.getCourbeconfigs().put((ReportRunVariableEmhKey) key, persitUiConfig);
    }
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables

  @Override
  protected void componentOpenedHandler() {
  }

  @Override
  protected void componentClosedDefinitlyHandler() {
  }

  void writeProperties(final java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
    p.setProperty("version", "1.0");
  }

  void readProperties(final java.util.Properties p) {
  }

  @Override
  public List<ReportVariableKey> getTitleAvailableVariables() {
    final List<ReportVariableKey> res = new ArrayList<>();

    res.add(new ReportVariableKey(ReportVariableTypeEnum.TIME, ReportExpressionHelper.TIME_NOM_CALC));
    res.add(new ReportVariableKey(ReportVariableTypeEnum.TIME, ReportExpressionHelper.TIME_TEMPS_SIMU));
    res.add(new ReportVariableKey(ReportVariableTypeEnum.TIME, ReportExpressionHelper.TIME_TEMPS_SCE));
    return res;
  }

  @Override
  public ReportTemporalConfig getReportConfig() {
    return content;
  }
}
