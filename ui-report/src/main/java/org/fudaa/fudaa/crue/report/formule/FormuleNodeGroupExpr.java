/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.formule;

import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleNodeGroupExpr extends AbstractNode {

  public FormuleNodeGroupExpr(final Children children) {
    super(children);
    setName("groupe.expression");
    setDisplayName(NbBundle.getMessage(FormuleContentChildFactory.class, "groupe.expression"));
  }

  @Override
  public Action[] getActions(final boolean context) {
    return new Action[]{
      SystemAction.get(FormuleNodeActionAddExpr.class),
      null,
      SystemAction.get(FormuleNodeActionImport.class),
      SystemAction.get(FormuleNodeActionExport.class)
    };
  }
}
