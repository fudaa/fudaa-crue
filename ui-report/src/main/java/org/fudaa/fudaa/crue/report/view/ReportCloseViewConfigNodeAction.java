/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view;

import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.fudaa.crue.common.action.AbstractNodeAction;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ReportCloseViewConfigNodeAction extends AbstractNodeAction {

  ReportViewsService reportViewsService = Lookup.getDefault().lookup(ReportViewsService.class);

  public ReportCloseViewConfigNodeAction() {
    super(org.openide.util.NbBundle.getMessage(ReportCloseViewConfigNodeAction.class, "CloseViewConfig.ActionName"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    if (ArrayUtils.isNotEmpty(activatedNodes)) {
      for (Node node : activatedNodes) {
        ReportViewLine lookup = node.getLookup().lookup(ReportViewLine.class);
        if (lookup != null && lookup.getTopComponent() != null) {
          return true;
        }

      }
    }
    return false;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    for (Node node : activatedNodes) {
      ReportViewLine line = node.getLookup().lookup(ReportViewLine.class);
      if (line != null && line.getTopComponent() != null) {
        if (!line.getTopComponent().isClosable()) {
          ReportViewsService.uninstallNode(line.getTopComponent(), node);
          line.getTopComponent().clearReportModifications();
        } else {
          line.getTopComponent().getTopComponent().close();
        }
      }
    }

  }
}
