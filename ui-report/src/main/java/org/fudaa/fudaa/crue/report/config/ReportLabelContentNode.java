/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.config;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLabelContentNode extends AbstractNodeFirable {

  public ReportLabelContentNode(ReportLabelContent config, List<ReportVariableKey> availableVar) {
    super(Children.LEAF, Lookups.fixed(config, availableVar));
    assert availableVar != null;
    setNoImage();
    setDisplayName(config.getDisplayName());
  }
  boolean editable = true;

  @Override
  public boolean isEditMode() {
    return editable;
  }

  public void setEditable(boolean editable) {
    this.editable = editable;
  }

  @Override
  public boolean canDestroy() {
    return true;
  }

  @Override
  protected Sheet createSheet() {
    ReportLabelContent config = getLookup().lookup(ReportLabelContent.class);
    List<ReportVariableKey> vars = getLookup().lookup(List.class);
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    PropertyNodeBuilder nodeBuilder = new PropertyNodeBuilder();
    List<PropertySupportReflection> createFromPropertyDesc = nodeBuilder.createFromPropertyDesc(DecimalFormatEpsilonEnum.PRESENTATION, config, this);
    for (PropertySupportReflection propertySupportReflection : createFromPropertyDesc) {
      if (ReportLabelContent.PROP_CONTENT.equals(propertySupportReflection.getName())) {
        String[] varsAsString = new String[vars.size()];
        Map<String, Object> translate = new HashMap<>();
        for (int i = 0; i < varsAsString.length; i++) {
          final ReportVariableKey varAt = vars.get(i);
          varsAsString[i] = varAt == null ? StringUtils.EMPTY : varAt.geti18n();
          translate.put(varsAsString[i], vars.get(i));
        }
        propertySupportReflection.setTags(varsAsString);
        propertySupportReflection.setTranslationForObjects(translate);
      }

      set.put(propertySupportReflection);
    }
    addPropertyChangeListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        ReportLabelContent config = getLookup().lookup(ReportLabelContent.class);
        setDisplayName(config.getDisplayName());
      }
    });
    sheet.put(set);
    return sheet;
  }
}
