/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.persist;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.io.conf.CrueEtudeConfigurationHelper;
import org.fudaa.dodico.crue.io.conf.Option;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.planimetry.LayerVisibility;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryExtraContainer;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.fudaa.crue.common.config.ConfigSaverHelper;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;
import org.fudaa.fudaa.crue.planimetry.save.ConfigLoaderHydraulic;
import org.fudaa.fudaa.crue.planimetry.save.ConfigLoaderHydraulic.Result;
import org.fudaa.fudaa.crue.planimetry.save.ConfigSaverHdydaulicAndVisibility;
import org.openide.nodes.Node;

import java.util.*;

/**
 * @author Frederic Deniger
 */
@XStreamAlias("ReportPlanimetryConfiguration")
public class ReportPlanimetryConfigPersist implements ReportConfigContrat {
  @XStreamAlias("Common-VisuConfiguration")
  private List<Option> visuConfiguration;
  @XStreamAlias("Report-Configuration")
  private ReportPlanimetryExtraContainer reportPlanimetryExtraContainer;
  @XStreamAlias("Legend-Properties")
  EbliUIProperties legendPropertes;
  @XStreamAlias("ExternLayer-Visibility")
  private Map<String, Boolean> visibilityByExternrLayerName;

  public ReportPlanimetryConfigPersist() {
  }

  public ReportPlanimetryConfigPersist(ReportPlanimetryConfig config) {
    if (config != null) {
      setVisuConfiguration(config.getVisuConfiguration(), config.getVisibility());
      legendPropertes = config.getLegendProperties();
      reportPlanimetryExtraContainer = config.getReportPlanimetryExtraContainer();
      this.visibilityByExternrLayerName = config.getVisibilityByExternLayerName();
    }
  }

  @Override
  public boolean variablesUpdated(FormuleDataChanges changes) {
    return reportPlanimetryExtraContainer.variablesUpdated(changes);
  }

  @Override
  public Collection<ResultatTimeKey> getSelectedTimeStepInReport() {
    return ReportResultProviderServiceContrat.NO_SELECTED_TIMES_REPORT;
  }

  @Override
  public ReportConfigContrat createTemplateConfig() {
    return duplicate();
  }

  public ReportPlanimetryConfig createConfig() {
    ReportPlanimetryConfig res = new ReportPlanimetryConfig();
    res.setReportPlanimetryExtraContainer(reportPlanimetryExtraContainer);
    ConfigLoaderHydraulic loader = new ConfigLoaderHydraulic();
    Map<String, String> properties = CrueEtudeConfigurationHelper.getProperties(visuConfiguration);
    Result load = loader.load(properties, CtuluLogLevel.WARNING);
    res.setVisibility(load.layerVisibility);
    res.setVisuConfiguration(load.visuConfiguration);
    res.setLegendPropertes(legendPropertes);
    res.setVisibilityForExternLayer(visibilityByExternrLayerName);
    return res;
  }

  @Override
  public ReportConfigContrat duplicate() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public ReportContentType getType() {
    return ReportContentType.PLANIMETRY;
  }

  @Override
  public void reinitContent() {
    if (reportPlanimetryExtraContainer == null) {
      reportPlanimetryExtraContainer = new ReportPlanimetryExtraContainer();
    } else {
      reportPlanimetryExtraContainer.reinitContent();
    }
  }

  public List<Option> getVisuConfiguration() {
    return visuConfiguration;
  }

  private void setVisuConfiguration(VisuConfiguration visuConfiguration, LayerVisibility visibility) {
    ConfigSaverHdydaulicAndVisibility saver = new ConfigSaverHdydaulicAndVisibility();
    LinkedHashMap<String, Node.Property> planimetryProperties = saver.getProperties(visuConfiguration, visibility);
    LinkedHashMap<String, String> planimetryValues = ConfigSaverHelper.transform(planimetryProperties);
    this.visuConfiguration = new ArrayList<>();
    CrueEtudeConfigurationHelper.addDataInList(planimetryValues, this.visuConfiguration);
  }

  public ReportPlanimetryExtraContainer getReportPlanimetryExtraContainer() {
    return reportPlanimetryExtraContainer;
  }

  public void setReportPlanimetryExtraContainer(ReportPlanimetryExtraContainer reportPlanimetryExtraContainer) {
    this.reportPlanimetryExtraContainer = reportPlanimetryExtraContainer;
  }
}
