/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.helper;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;

/**
 * Permet de transformer les temps permanent en leur noms.
 *
 * @author Frederic Deniger
 */
public class ResultatKeyPermanentUsedToStringTransformer implements ToStringTransformer {

  ToStringTransformer iniTransformer;

  public ResultatKeyPermanentUsedToStringTransformer(ToStringTransformer iniTransformer) {
    this.iniTransformer = iniTransformer;
  }

  public void setIniTransformer(ToStringTransformer iniTransformer) {
    this.iniTransformer = iniTransformer;
  }

  @Override
  public String transform(Object in) {
    if (in == null) {
      return StringUtils.EMPTY;
    }
    ResultatTimeKey key = (ResultatTimeKey) in;
    if (key.isPermanent()) {
      return key.getNomCalcul();
    }
    return iniTransformer.transform(in);
  }
}
