/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.ordres;

import java.util.List;
import org.fudaa.dodico.crue.metier.OrdResKey;

/**
 *
 * @author Frederic Deniger
 */
public class ReportOrdResSelectionByRunLine {
  
  OrdResKey key;
  String variable;
  List<ReportOrdResSelectionByRun> selectionByRun;

  public OrdResKey getKey() {
    return key;
  }

  public void setKey(OrdResKey key) {
    this.key = key;
  }

  public String getVariable() {
    return variable;
  }

  public void setVariable(String variable) {
    this.variable = variable;
  }

  public List<ReportOrdResSelectionByRun> getSelectionByRun() {
    return selectionByRun;
  }

  public void setSelectionByRun(List<ReportOrdResSelectionByRun> selectionByRun) {
    this.selectionByRun = selectionByRun;
  }
  
  
  
  
}
