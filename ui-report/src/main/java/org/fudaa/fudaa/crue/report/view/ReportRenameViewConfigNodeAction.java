/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view;

import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.helper.ChooseNameView;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ReportRenameViewConfigNodeAction extends AbstractEditNodeAction {
  
  ReportViewsService reportViewsService = Lookup.getDefault().lookup(ReportViewsService.class);
  
  public ReportRenameViewConfigNodeAction() {
    super(org.openide.util.NbBundle.getMessage(ReportRenameViewConfigNodeAction.class, "RenameViewConfig.ActionName"));
  }
  
  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return activatedNodes.length == 1;
  }
  
  @Override
  protected void performAction(Node[] activatedNodes) {
    Node n = activatedNodes[0];
    ChooseNameView chooser = new ChooseNameView();
    chooser.setPattern(ValidationPatternHelper.getRapportNamePattern());
    String chooseName = chooser.chooseName("", n.getName());
    if (chooseName != null) {
      n.setName(chooseName);
    }
  }
}
