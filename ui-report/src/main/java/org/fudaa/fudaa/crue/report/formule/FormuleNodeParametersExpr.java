/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.formule;

import org.fudaa.dodico.crue.projet.report.formule.FormuleParametersExpr;
import org.openide.util.actions.SystemAction;

import javax.swing.*;

/**
 * @author Frederic Deniger
 */
public class FormuleNodeParametersExpr extends FormuleNodeParameters {
    /**
     * @param formuleContent le contenu de la formule
     * @param isNew          true si vient d'etre crée
     */
    public FormuleNodeParametersExpr(final FormuleParametersExpr formuleContent, final boolean isNew) {
        super(formuleContent, isNew);
    }

    @Override
    public SystemAction getDefaultAction() {
        return SystemAction.get(FormuleNodeActionEditExpr.class);
    }

    @Override
    public Action[] getActions(final boolean context) {
        return new Action[]{
                SystemAction.get(FormuleNodeActionEditExpr.class),
                null,
                SystemAction.get(FormuleNodeActionAddExpr.class),
                SystemAction.get(FormuleNodeActionContentRemove.class),
                null,
                SystemAction.get(FormuleNodeActionImport.class),
                SystemAction.get(FormuleNodeActionExport.class)
        };
    }
}
