package org.fudaa.fudaa.crue.report.actions;

import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;

import org.fudaa.fudaa.crue.aoc.service.LhptService;
import org.fudaa.fudaa.crue.report.ReportParametrageEchelleSectionTopComponent;
import org.fudaa.fudaa.crue.report.perspective.ActiveReport;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 * Permet d'ouvrir la fenêtre profils<->sections
 */
@ActionID(category = "View", id = "org.fudaa.fudaa.crue.report.actions.ReportOpenParametrageEchelleSectionAction")
@ActionRegistration(displayName = "#ReportOpenParametrageEchelleSectionAction.Name")
@ActionReferences({
        @ActionReference(path = ActiveReport.ACTIONS_REPORT, position = 90, separatorBefore = 89),
        @ActionReference(path = ActiveReport.ACTIONS_REPORT_VIEWS, position = 90, separatorBefore = 89)
})
public final class ReportOpenParametrageEchelleSectionAction extends AbstractOpenViewAction implements ContextAwareAction {

    final LhptService service = Lookup.getDefault().lookup(LhptService.class);

    public ReportOpenParametrageEchelleSectionAction() {
        super(ReportParametrageEchelleSectionTopComponent.MODE, ReportParametrageEchelleSectionTopComponent.TOPCOMPONENT_ID);
        putValue(Action.NAME, NbBundle.getMessage(ReportOpenParametrageEchelleSectionAction.class, "ReportOpenParametrageEchelleSectionAction.Name"));
        service.addProjectListener(evt -> updateEnableState());
        updateEnableState();
    }

    @Override
    public Action createContextAwareInstance(Lookup actionContext) {
        return new ReportOpenParametrageMesurePostTraitementAction();
    }

    protected void updateEnableState() {
        if (!EventQueue.isDispatchThread()) {
            EventQueue.invokeLater(
                () -> updateEnableState());
            return;
        }
        setEnabled(getEnableState());
    }

    private boolean getEnableState() {
        return service.isProjectLoaded();
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        super.open();
    }


}
