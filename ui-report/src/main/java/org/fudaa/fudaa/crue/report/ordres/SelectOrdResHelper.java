/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.ordres;

import java.util.Collection;
import java.util.List;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.result.OrdResExtractor;
import org.fudaa.dodico.crue.metier.result.OrdResVariableSelection;

/**
 *
 * @author Frederic Deniger
 */
public class SelectOrdResHelper {

  public static SelectOrdResNode createNode(final EMHScenario scenario, final Collection<EMH> selectedEMHs, final List<String> variableFC) {
    final OrdResExtractor extractor = new OrdResExtractor(scenario.getOrdResScenario());
    final List<OrdResVariableSelection> ordResSelectedVarForEMH = extractor.getOrdResSelectedVarForEMH(selectedEMHs, variableFC);
    return new SelectOrdResNode(ordResSelectedVarForEMH);
  }
}
