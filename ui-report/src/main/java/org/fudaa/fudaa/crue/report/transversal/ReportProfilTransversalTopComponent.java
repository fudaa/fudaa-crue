package org.fudaa.fudaa.crue.report.transversal;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.OrdResScenario;
import org.fudaa.dodico.crue.metier.emh.OrdResSection;
import org.fudaa.dodico.crue.metier.result.OrdResExtractor;
import org.fudaa.dodico.crue.metier.result.OrdResVariableSelection;
import org.fudaa.dodico.crue.projet.report.data.ReportExpressionHelper;
import org.fudaa.dodico.crue.projet.report.data.ReportKeyContract;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.persist.ReportTransversalConfig;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.ChooseInListHelper;
import org.fudaa.fudaa.crue.loi.common.CourbesUiController;
import org.fudaa.fudaa.crue.loi.res.ProfilSectionLoiUiResController;
import org.fudaa.fudaa.crue.loi.section.ProfilSectionLoiUiController;
import org.fudaa.fudaa.crue.report.AbstractReportTimeViewTopComponent;
import org.fudaa.fudaa.crue.report.actions.ReportOpenListFrottementsAction;
import org.fudaa.fudaa.crue.report.service.ReportFormuleService;
import org.fudaa.fudaa.crue.report.service.ReportHelper;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.report//ReportProfilTransversalTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = ReportProfilTransversalTopComponent.TOPCOMPONENT_ID,
        persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ReportProfilTransversalTopComponent.MODE, openAtStartup = false, position = 1)
public final class ReportProfilTransversalTopComponent extends AbstractReportTimeViewTopComponent<ReportTransversalConfig, ReportTransversalGrapheBuilder> {

  public static final String MODE = "report-transversal";
  public static final String TOPCOMPONENT_ID = "ReportProfilTransversalTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  final ReportFormuleService reportFormuleService = Lookup.getDefault().lookup(ReportFormuleService.class);

  /**
   * Contient toutes les données qui vont bien
   */
  public ReportProfilTransversalTopComponent() {
    setName(NbBundle.getMessage(ReportProfilTransversalTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ReportProfilTransversalTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueProfilTransversal", PerspectiveEnum.REPORT);
  }

  @Override
  protected ReportTransversalGrapheBuilder createBuilder() {
    return new ReportTransversalGrapheBuilder((ProfilSectionLoiUiController) profilUiController, loiLegendManager);
  }

  @Override
  protected void buildComponents() {
    initComponents();
    JPanel pnTable = new JPanel(new BorderLayout(5, 5));
    pnTable.add(profilUiController.getTableGraphePanel());
    JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, loiLegendManager.getPanel(), pnTable);
    splitPane.setDividerLocation(550);
    splitPane.setContinuousLayout(true);
    splitPane.setOneTouchExpandable(true);
    add(splitPane);
  }

  @Override
  protected CourbesUiController createCourbesUiController() {
    ProfilSectionLoiUiResController res = new ProfilSectionLoiUiResController(null);
    res.setProfilSectionOpenFrtTarget(new ReportOpenListFrottementsAction());
    res.setUseSectionName(true);
    res.setAddFrt(false);
    return res;
  }

  @Override
  protected void saveCourbeConfig(EGCourbePersist persitUiConfig, ReportKeyContract key) {
    content.put((ReportRunVariableKey) key, persitUiConfig);
  }

  @Override
  public void alternatifRunChanged() {
    Set<ReportRunKey> reportRunContents = reportRunAlternatifService.getAvailableRunKeys();
    boolean modified = false;
    for (Iterator<ReportRunVariableKey> it = content.getProfilVariables().iterator(); it.hasNext();) {
      ReportRunVariableKey reportRunVariableKey = it.next();
      if (reportRunVariableKey.getRunKey().isAlternatifRun() && !reportRunContents.contains(reportRunVariableKey.getRunKey())) {
        modified = true;
        it.remove();
      }
    }
    for (Iterator<ReportRunKey> it = content.getProfilXtZ().iterator(); it.hasNext();) {
      ReportRunKey next = it.next();
      if (next.isAlternatifRun() && !reportRunContents.contains(next)) {
        modified = true;
        it.remove();
      }
    }
    for (Iterator<ReportRunVariableKey> it = content.getCourbeconfigs().keySet().iterator(); it.hasNext();) {
      ReportRunVariableKey reportRunVariableKey = it.next();
      ReportRunKey reportRunKey = reportRunVariableKey.getReportRunKey();
      if (reportRunKey.isAlternatifRun() && !reportRunContents.contains(reportRunKey)) {
        it.remove();
        modified = true;
      }
    }
    if (modified) {
      setModified(true);
      applyChangeInContent(false);
    }
  }

  protected void chooseProfilVariables() {
    ReportProfilTransvervalCourbeChooser chooser = new ReportProfilTransvervalCourbeChooser(content);
    if (chooser.displayChooseUI()) {
      propagateChange();
    }
  }

  protected void chooseSection() {
    final List<CatEMHSection> sections = reportService.getModele().getSections();
    List<String> sectionNames = TransformerHelper.toNom(sections);
    ChooseInListHelper<String> chooser = new ChooseInListHelper<>();
    chooser.setLabel(NbBundle.getMessage(ReportProfilTransversalTopComponent.class, "ChooseSectionName.Label"));
    chooser.setDialogTitle(NbBundle.getMessage(ReportProfilTransversalTopComponent.class, "ChooseSection.DialogTitle"));
    String newSection = chooser.choose(sectionNames, content.getSectionName());
    if (newSection != null) {
      content.setSectionName(newSection);
      propagateChange();
    }
  }

  protected void chooseAval() {
    chooseFromDelta(1);
  }

  protected void chooseAmont() {
    chooseFromDelta(-1);
  }

  private CatEMHSection findSection(CatEMHSection current, int inc) {
    if (current == null) {
      return null;
    }
    final List<CatEMHSection> sections = reportService.getModele().getSections();
    int idx = CtuluLibArray.findObjectEgalEgal(sections, current) + inc;
    if (idx < 0) {
      DialogHelper.showInfo(NbBundle.getMessage(ReportProfilTransversalTopComponent.class, "SectionNavigation.InfoDialog"),
              NbBundle.getMessage(ReportProfilTransversalTopComponent.class, "NoAmontSectionWithResultat.DialogContent"));
      return null;
    }
    if (idx >= sections.size()) {
      DialogHelper.showInfo(NbBundle.getMessage(ReportProfilTransversalTopComponent.class, "SectionNavigation.InfoDialog"),
              NbBundle.getMessage(ReportProfilTransversalTopComponent.class, "NoAvalSectionWithResultat.DialogContent"));
      return null;
    }
    return sections.get(idx);
  }

  

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables

  @Override
  protected void componentOpenedHandler() {
  }

  @Override
  protected void componentClosedDefinitlyHandler() {
  }

  void writeProperties(java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
    p.setProperty("version", "1.0");
  }

  void readProperties(java.util.Properties p) {
  }

  @Override
  public List<EbliActionInterface> getMainActions() {
    EbliActionSimple chooseSection = new EbliActionSimple(NbBundle.getMessage(ReportProfilTransversalTopComponent.class, "ChooseSection.ButtonName"),
            null, "SECTIONS") {
              @Override
              public void actionPerformed(ActionEvent _e) {
                chooseSection();
              }
            };
    EbliActionSimple amont = new EbliActionSimple(NbBundle.getMessage(ReportProfilTransversalTopComponent.class, "ChooseSectionAmont.ButtonName"),
            ReportHelper.getIcon("navigation_previous.png"), "AMONT") {
              @Override
              public void actionPerformed(ActionEvent _e) {
                chooseAmont();
              }
            };
    EbliActionSimple aval = new EbliActionSimple(NbBundle.getMessage(ReportProfilTransversalTopComponent.class, "ChooseSectionAval.ButtonName"),
            ReportHelper.getIcon("navigation_next.png"), "NEXT") {
              @Override
              public void actionPerformed(ActionEvent _e) {
                chooseAval();
              }
            };
    EbliActionSimple chooseVariables = new EbliActionSimple(NbBundle.getMessage(ReportProfilTransversalTopComponent.class, "AddVariable.ButtonName"),
            null, "VARIABLES") {
              @Override
              public void actionPerformed(ActionEvent _e) {
                chooseProfilVariables();
              }
            };
    return Arrays.asList(chooseSection, amont, aval, chooseVariables, createConfigExternAction());

  }

  @Override
  public List<ReportVariableKey> getTitleAvailableVariables() {
    List<ReportVariableKey> res = new ArrayList<>();
    res.add(new ReportVariableKey(ReportVariableTypeEnum.EXPR, ReportExpressionHelper.EXPR_NOM));
    res.add(new ReportVariableKey(ReportVariableTypeEnum.EXPR, ReportExpressionHelper.EXPR_COMMENTAIRE));
    res.add(new ReportVariableKey(ReportVariableTypeEnum.EXPR, ReportExpressionHelper.EXPR_PROFIL_COMMENTAIRE));
    res.add(new ReportVariableKey(ReportVariableTypeEnum.TIME, ReportExpressionHelper.TIME_NOM_CALC));
    res.add(new ReportVariableKey(ReportVariableTypeEnum.TIME, ReportExpressionHelper.TIME_TEMPS_SIMU));
    res.add(new ReportVariableKey(ReportVariableTypeEnum.TIME, ReportExpressionHelper.TIME_TEMPS_SCE));
    OrdResScenario ordResScenario = getScenario().getOrdResScenario();
    OrdResExtractor extract = new OrdResExtractor(ordResScenario);
    OrdResVariableSelection sectionVariable = extract.getOrdResSelection(OrdResSection.KEY, reportFormuleService.getVariablesKeys());
    List<String> selectableVariables = sectionVariable.getSelectableVariables();
    for (String var : selectableVariables) {
      res.add(builder.getReportResultProviderService().createVariableKey(var));
    }
    return res;
  }

  public void chooseFromDelta(final int inc) {
    CatEMHSection section = (CatEMHSection) reportService.getRunCourant().getEMH(content.getSectionName());
    CatEMHSection findSection = findSection(section, inc);
    if (findSection != null) {
      content.setSectionName(findSection.getNom());
      propagateChange();
    }
  }

  @Override
  public ReportTransversalConfig getReportConfig() {
    return content;
  }
}
