/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.service;

import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;
import org.fudaa.dodico.crue.projet.report.formule.function.AggregationCacheKey;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;

import java.util.List;

/**
 * Permet d'avoir une vue d'ensemble des services de Post-traitement
 * Ne contient pas de lookup.
 *
 * @author Frederic Deniger
 */
@ServiceProviders(value = {
        @ServiceProvider(service = ReportGlobalService.class),
        @ServiceProvider(service = ReportGlobalServiceContrat.class)})
public class ReportGlobalService implements ReportGlobalServiceContrat {
    private final ReportResultProviderService reportResultProviderService = Lookup.getDefault().lookup(ReportResultProviderService.class);
    private final ReportAggregationCacheService reportAggregationCacheService = Lookup.getDefault().lookup(ReportAggregationCacheService.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public ReportResultProviderServiceContrat getResultService() {
        return reportResultProviderService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Double getCachedValue(AggregationCacheKey cacheKey) {
        return reportAggregationCacheService.getCachedValue(cacheKey);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putCachedValue(AggregationCacheKey cacheKey, Double resValue) {
        reportAggregationCacheService.putCachedValue(cacheKey, resValue);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ResultatTimeKey> getAllPermanentTimeKeys() {
        return reportResultProviderService.getReportService().getTimeContent().getPermanentsTimes();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ResultatTimeKey> getAllTransientTimeKeys() {
        return reportResultProviderService.getReportService().getTimeContent().getTransitoiresTimes();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T showProgressDialogAndRun(ProgressRunnable<T> operation, String displayName) {
        return CrueProgressUtils.showProgressDialogAndRun(operation, displayName);
    }
}
