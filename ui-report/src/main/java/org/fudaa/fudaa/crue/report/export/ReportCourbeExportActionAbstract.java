/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.export;

import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.gui.CtuluExportTableChooseFile;
import org.fudaa.ctulu.gui.CtuluFileChooserCsvExcel;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableEmhKey;
import org.fudaa.dodico.crue.projet.report.export.ReportExportAbstract;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.report.AbstractReportTimeViewTopComponent;
import org.fudaa.fudaa.crue.report.data.ReportVariableChooser;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public abstract class ReportCourbeExportActionAbstract<T extends AbstractReportTimeViewTopComponent> extends EbliActionSimple {

  protected final T topComponent;

  public ReportCourbeExportActionAbstract(final T topComponent) {
    super(org.openide.util.NbBundle.getMessage(ReportCourbeExportActionAbstract.class, "ExportAll.Action"), BuResource.BU.getToolIcon("exporter"),
            "EXPORT");
    setDefaultToolTip(org.openide.util.NbBundle.getMessage(ReportCourbeExportActionAbstract.class, "ExportAll.Tootltip"));
    this.topComponent = topComponent;
  }

  protected abstract ReportExportAbstract createReportExporter(ReportResultProviderService reportResultProviderService);

  protected abstract List<String> getChoosableVariables();

  protected abstract List<String> getEMHs();

  protected abstract Set<String> getCurrentSelectedVariables();

  /**
   * Le run courant est automatiquement ajouté.
   *
   * @param keys set à remplir avec les runs utilisées dans la vue
   */
  protected abstract void fillWithSelectedRunKeys(Set<ReportRunKey> keys);

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final List<String> toExport = chooseVariables();
    if (!toExport.isEmpty()) {
      //on récupère tous les runs configurés pour la vue en question.
      final ReportResultProviderService reportResultProviderService = Lookup.getDefault().lookup(ReportResultProviderService.class);
      final Set<ReportRunKey> runKey = new HashSet<>();
      fillWithSelectedRunKeys(runKey);
      runKey.add(reportResultProviderService.getRunCourant().getRunKey());
      final List<ReportRunKey> keys = new ArrayList<>();
      keys.addAll(runKey);
      Collections.sort(keys, ObjetNommeByNameComparator.INSTANCE);
      final List<ResultatTimeKey> selectedTimeKeys = reportResultProviderService.getReportService().getRangeSelectedTimeKeys();
      final List<ReportRunVariableEmhKey> variablesKeys = ReportVariableChooser.getKeysForRuns(reportResultProviderService, toExport,
              ReportCourbeExportActionAbstract.this.getEMHs(), keys);

      final File f = chooseTargetFile();
      if (f != null) {
        String error = CtuluLibFile.canWrite(f);
        if (error == null && f.exists() && !f.delete()) {
          error = org.openide.util.NbBundle.getMessage(ReportCourbeExportActionAbstract.class, "cantWriteToFileUsedByAnotherProcess.error");
        }
        if (error != null) {
          DialogHelper.showError(org.openide.util.NbBundle.getMessage(ReportCourbeExportActionAbstract.class, "cantWriteToFile.error", f.getName(),
                  error));
          return;

        }
        final ProgressRunnable runnable = new ProgressRunnable<Boolean>() {

          @Override
          public Boolean run(final ProgressHandle handle) {
            final ReportExportAbstract exporter = createReportExporter(reportResultProviderService);
            exporter.export(variablesKeys, selectedTimeKeys, f);
            return Boolean.TRUE;
          }
        };
        execute(runnable, f);

      }

    }
  }

  protected List<String> chooseVariables() {
    final List<String> choosableVariables = getChoosableVariables();
    final Set<String> selectedVariables = getCurrentSelectedVariables();
    final ReportVariableChooser variableChooser = new ReportVariableChooser(choosableVariables, selectedVariables);
    return variableChooser.choose();
  }

  protected File chooseTargetFile() {
    final CtuluFileChooserCsvExcel choose = new CtuluFileChooserCsvExcel(CtuluUIForNetbeans.DEFAULT);
    choose.removeChoosableFileFilter(choose.getFtCsv());
    choose.removeChoosableFileFilter(choose.getFtXsl());
    return new CtuluExportTableChooseFile(choose).chooseExportFile();
  }

  protected void execute(final ProgressRunnable<Boolean> runnable, final File f) {
    final Boolean res = CrueProgressUtils.showProgressDialogAndRun(runnable, getTitle());
    if (Boolean.TRUE.equals(res)) {
      DialogHelper.showNotifyOperationTermine(getTitle(), org.openide.util.NbBundle.
              getMessage(ReportCourbeExportActionAbstract.class, "ExportEnded", f.
                      getAbsolutePath()));
    }
  }

}
