/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.ordres;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frederic Deniger
 */
public class ReportOrdResSelectionByRunLineNode extends AbstractNodeFirable {

  public ReportOrdResSelectionByRunLineNode(ReportOrdResSelectionByRunLine line) {
    super(Children.LEAF, Lookups.singleton(line));
    setName(line.getVariable());
    setDisplayName(StringUtils.capitalize(line.getVariable()));
    setNoImage();
  }

  @Override
  public boolean isEditMode() {
    return true;
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    sheet.put(set);
    ReportOrdResSelectionByRunLine line = getLine();
    final List<ReportOrdResSelectionByRun> selectionByRun = line.selectionByRun;
    for (ReportOrdResSelectionByRun byRun : selectionByRun) {
      if (byRun.isSelectable(line.variable)) {
        SelectOrdResPropertySupport propertySupport = new SelectOrdResPropertySupport(this, byRun.selection, line.variable);
        propertySupport.setCanWrite(byRun.isEditable(line.variable));
        propertySupport.setDisplayName(byRun.runKey.getDisplayName());
        propertySupport.setName(byRun.runKey.getDisplayName());
        set.put(propertySupport);
      }

    }

    return sheet;
  }

  public ReportOrdResSelectionByRunLine getLine() {
    return getLookup().lookup(ReportOrdResSelectionByRunLine.class);
  }
}
