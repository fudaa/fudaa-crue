/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.actions;

import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import java.util.List;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.report.export.ReportExportHelper;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportPlanimetryExportAction extends EbliActionSimple {

  private final PlanimetryController planimetryController;

  public ReportPlanimetryExportAction(PlanimetryController planimetryController) {
    super(NbBundle.getMessage(ReportPlanimetryExportAction.class, "ExportAction.Name"), BuResource.BU.getIcon("crystal_exporter"), "EXPORT");
    this.planimetryController = planimetryController;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    new ReportExportHelper().displayDialogExport(planimetryController.getVisuPanel().getSelectedEMHs());
  }

  @Override
  public void updateStateBeforeShow() {
    List<EMH> selectedEMHs = planimetryController.getVisuPanel().getSelectedEMHs();
    setEnabled(!selectedEMHs.isEmpty());
  }
}
