/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.time;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ebli.animation.EbliAnimationOutputInterface;
import org.openide.windows.TopComponent;

/**
 *
 * @author Frederic Deniger
 */
public class ReportTimeVideoRecorderItem implements Comparable<ReportTimeVideoRecorderItem> {

  private TopComponent topComponent;
  private EbliAnimationOutputInterface output;
  private boolean activated;
  private int nbFrames;

  /**
   * Modifiées à chaque réouverture.
   *
   * @return le nombre d'images enregistrées.
   */
  public int getNbFrames() {
    return nbFrames;
  }

  @Override
  public int compareTo(ReportTimeVideoRecorderItem o) {
    if (o == null) {
      return 1;
    }
    return StringUtils.defaultString(getTopComponent().getName()).compareTo(o.getTopComponent().getName());
  }

  public void setNbFrames(int nbFrames) {
    this.nbFrames = nbFrames;
  }

  public boolean isActivated() {
    return activated;
  }

  public void setActivated(boolean activated) {
    this.activated = activated;
    if (output != null) {
      output.setActivated(activated);
    }
  }

  public TopComponent getTopComponent() {
    return topComponent;
  }

  public void setTopComponent(TopComponent topComponent) {
    this.topComponent = topComponent;
  }

  public EbliAnimationOutputInterface getOutput() {
    return output;
  }

  public void setOutput(EbliAnimationOutputInterface producer) {
    this.output = producer;
    if (output != null) {
      output.setActivated(activated);
    }
  }
}
