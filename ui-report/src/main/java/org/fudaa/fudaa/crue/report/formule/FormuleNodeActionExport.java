/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.formule;

import java.io.File;
import java.util.List;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluFileChooserTestWritable;
import org.fudaa.dodico.crue.projet.report.formule.FormuleParametersExpr;
import org.fudaa.fudaa.commun.impl.FudaaGuiLib;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.report.view.persist.FormuleSaverProcessor;
import org.openide.nodes.Node;
import org.openide.windows.WindowManager;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleNodeActionExport extends AbstractEditNodeAction {

  public FormuleNodeActionExport() {
    super(org.openide.util.NbBundle.getMessage(FormuleNodeActionExport.class, "formule.export"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return activatedNodes.length == 1 && !FormuleContentChildFactory.getAllNodes(activatedNodes[0]).isEmpty();
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    final CtuluFileChooser fileChooser = new CtuluFileChooser(true);
    fileChooser.setTester(new CtuluFileChooserTestWritable(CtuluUIForNetbeans.DEFAULT));
    File chooseFile = FudaaGuiLib.chooseFile(WindowManager.getDefault().getMainWindow(), true, fileChooser);
    if (chooseFile != null) {
      List<FormuleParametersExpr> allNodes = FormuleContentChildFactory.getAllParameters(activatedNodes[0]);
      FormuleSaverProcessor processor = new FormuleSaverProcessor(chooseFile, allNodes);
      CrueProgressUtils.showProgressDialogAndRun(processor, getName());
    }


  }
}
