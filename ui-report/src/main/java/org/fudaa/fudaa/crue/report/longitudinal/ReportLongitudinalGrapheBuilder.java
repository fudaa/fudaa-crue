/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.longitudinal;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSectionProfil;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.metier.helper.ReportProfilHelper;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBrancheConfig;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalConfig;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalPositionBrancheContent;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalPositionBuilder;
import org.fudaa.ebli.courbe.*;
import org.fudaa.fudaa.crue.loi.ViewCourbeManager;
import org.fudaa.fudaa.crue.loi.common.CourbeModelWithKey;
import org.fudaa.fudaa.crue.loi.common.CourbesUiController;
import org.fudaa.fudaa.crue.loi.common.LoiConstanteCourbeModel;
import org.fudaa.fudaa.crue.report.config.AbstractReportGrapheBuilder;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.netbeans.api.progress.ProgressHandle;

import java.util.*;

/**
 * @author Frederic Deniger
 */
public class ReportLongitudinalGrapheBuilder extends AbstractReportGrapheBuilder<CourbesUiController, ReportLongitudinalConfig> {
  ReportLongitudinalPositionBrancheContent result;

  public ReportLongitudinalGrapheBuilder(CourbesUiController uiController, ViewCourbeManager loiLabelsManager) {
    super(uiController, loiLabelsManager);
  }

  protected EGAxeVertical getOrCreateAxeVerticalZ(ReportLongitudinalConfig content, Map<String, EGAxeVertical> saved) {
    return getOrCreateAxeVerticalConfigured(getVariableZ().getNature(), content, saved);
  }

  @Override
  public void runUnloaded() {
    super.runUnloaded();
    result = null;
  }

  @Override
  public ReportResultProviderService getReportResultProviderService() {
    return reportResultProviderService;
  }

  @Override
  public List<EGCourbeSimple> getInternCourbes(ReportLongitudinalConfig content, ProgressHandle progress) {
    Map<String, EGAxeVertical> saved = new HashMap<>();
    ReportLongitudinalGrapheBuilderCourbe courbeBuilder = new ReportLongitudinalGrapheBuilderCourbe(reportService.getCcm(), this, content, saved);
    EGAxeVertical axeZ = courbeBuilder.getAxeZ();
    uiController.configureAxeH(reportService.getCcm().getProperty(CrueConfigMetierConstants.PROP_XP), true);
    double xpInit = 0;
    result = new ReportLongitudinalPositionBrancheContent();
    ReportLongitudinalPositionBuilder positionBuilder = new ReportLongitudinalPositionBuilder(reportResultProviderService, reportService.getCcm());
    Set<ReportRunKey> keys = getRunKeysToUse(content);
    for (ReportLongitudinalBrancheConfig brancheConfig : content.getBranchesConfig()) {
      if (progress != null) {
        progress.setDisplayName(brancheConfig.getName());
      }
      xpInit = xpInit + brancheConfig.getDecalXp();
      positionBuilder.buildPositions(xpInit, brancheConfig, result, keys);
      xpInit = xpInit + brancheConfig.getLength();
    }
    uiController.getGraphe().setHorizontalBanner(new ReportLongitudinalBanner(content, result));
    final List<EGCourbeSimple> res = createNonTimeStepCourbes(content, axeZ, courbeBuilder, progress);
    res.addAll(createCourbesOnCurrentTimeStep(content, progress, saved));
    return res;
  }

  @Override
  public void applyLabelsConfigChanged(ReportLongitudinalConfig content) {
    List<LabelConfig> labels = content.getLoiLegendConfig().getLabels();
    ReportRunKey runKey = reportService.getRunCourant() == null ? null : reportService.getRunCourant().getRunKey();
    for (LabelConfig config : labels) {
      ReportLabelContent labelContent = (ReportLabelContent) config.getKey();
      String text = runKey == null ? StringUtils.EMPTY : reportResultProviderService.getLabelString(labelContent, runKey, content.getMainEMHName(),
          DecimalFormatEpsilonEnum.PRESENTATION, content.getSelectedTimeStepInReport());
      String tltip = runKey == null ? StringUtils.EMPTY : reportResultProviderService.getLabelString(labelContent, runKey, content.getMainEMHName(),
          DecimalFormatEpsilonEnum.COMPARISON, content.getSelectedTimeStepInReport());
      config.setText(text);
      config.setTooltipText(tltip);
    }
    this.loiLabelsManager.getLoiLabelsManager().applyConfig();
  }

  protected List<EGCourbeSimple> retrieveCourbesNonTimeStep() {
    EGGrapheSimpleModel model = uiController.getEGGrapheSimpleModel();
    List<EGCourbeSimple> res = new ArrayList<>();
    EGCourbeSimple[] courbes = model.getCourbes();
    for (int i = 0; i < courbes.length; i++) {
      EGCourbeSimple courbe = courbes[i];
      Object key = ((CourbeModelWithKey) courbe.getModel()).getKey();
      if (key != null && ReportRunVariableTimeKey.class.equals(key.getClass())) {
        ReportRunVariableTimeKey reportKey = (ReportRunVariableTimeKey) key;
        if (!reportKey.isDefinedOnSelectedTimeStep() || reportKey.isNotTimeDependant() || isExpressionNotTimeDependant(
            reportKey.getRunVariableKey())) {
          res.add(courbe);
        }
      }
    }
    return res;
  }

  @Override
  public List<EGCourbeSimple> getInternCourbesAfterTimeChanged(ReportLongitudinalConfig content, ProgressHandle progress) {
    Map<String, EGAxeVertical> saved = new HashMap<>();
    List<EGCourbeSimple> res = retrieveCourbesNonTimeStep();
    res.addAll(createCourbesOnCurrentTimeStep(content, progress, saved));
    return res;
  }

  private List<EGCourbeSimple> createCourbesOnCurrentTimeStep(ReportLongitudinalConfig content, ProgressHandle progress,
                                                              Map<String, EGAxeVertical> saved) {
    if (Boolean.FALSE.equals(content.getCurrentTimeDisplayed()) || result == null) {
      return Collections.emptyList();
    }
    ReportLongitudinalGrapheBuilderCourbe courbeBuilder = new ReportLongitudinalGrapheBuilderCourbe(reportService.getCcm(), this, content, saved);
    List<ReportRunVariableKey> var = content.getProfilVariables();
    List<EGCourbeSimple> courbes = new ArrayList<>();
    ResultatTimeKey selectedTime = reportService.getSelectedTime();
    if (selectedTime != null) {
      for (ReportRunVariableKey reportRunVariableKey : var) {
        if (reportRunVariableKey.isReadOrVariable() && !isExpressionNotTimeDependant(reportRunVariableKey)) {
          if (progress != null) {
            progress.setDisplayName(reportRunVariableKey.getVariableName() + ": " + reportRunVariableKey.getReportRunKey().getDisplayName());
          }
          EGCourbeSimple courbe = courbeBuilder.createCourbe(reportRunVariableKey, selectedTime, true);
          courbes.add(courbe);
        }
      }
    }
    return courbes;
  }

  public EMHSectionProfil retrieveProfil(EMH emh) {
    return ReportProfilHelper.retrieveProfil(emh);
  }

  public ItemVariable getVariableZ() {
    return reportService.getCcm().getProperty(CrueConfigMetierConstants.PROP_Z);
  }

  protected void updateReadCourbeName(EGCourbe courbe) {
    ReportRunVariableTimeKey key = (ReportRunVariableTimeKey) ((CourbeModelWithKey) courbe.getModel()).getKey();
    if (!key.isLimitProfilVariable()) {
      courbe.getModel().setTitle(key.getDisplayName(getReportResultProviderService().getReportService().getReportTimeFormatter().
          getResulatKeyTempsSceToStringTransformer()));
    }
  }

  public Set<ReportRunKey> getRunKeysToUse(ReportLongitudinalConfig content) {
    final List<ReportRunVariableKey> variables = content.getProfilVariables();
    Set<ReportRunKey> keys = new HashSet<>();
    if (reportService.getRunCourant() != null) {
      keys.add(reportService.getRunCourant().getRunKey());
    }
    for (ReportRunVariableKey reportRunVariableKey : variables) {
      keys.add(reportRunVariableKey.getRunKey());
    }
    return keys;
  }

  EGAxeVertical getRightAxis(ReportLongitudinalConfig content, Map<String, EGAxeVertical> savedAxed) {
    String variableDrawnOnRightAxis = content.getVariableDrawnOnRightAxis(reportResultProviderService);
    EGAxeVertical rightAxis = null;
    if (StringUtils.isNotBlank(variableDrawnOnRightAxis)) {
      rightAxis = getOrCreateAxeVerticalConfigured(reportResultProviderService.getPropertyNature(variableDrawnOnRightAxis), content, savedAxed);
    }
    if (rightAxis != null) {
      rightAxis.setDroite(true);
    }
    return rightAxis;
  }

  void updateCourbeNames(ReportLongitudinalConfig content) {
    EGCourbeSimple[] courbes = uiController.getEGGrapheSimpleModel().getCourbes();
    for (EGCourbeSimple courbe : courbes) {
      updateReadCourbeName(courbe);
    }
  }

  public EGGraphe getGraphe() {
    return uiController.getGraphe();
  }

  /**
   * @param content
   * @param axeZ
   * @param courbeBuilder
   * @param progress
   * @return les courbes non dépendants du temps sélectionné.
   */
  private List<EGCourbeSimple> createNonTimeStepCourbes(ReportLongitudinalConfig content, EGAxeVertical axeZ,
                                                        ReportLongitudinalGrapheBuilderCourbe courbeBuilder, ProgressHandle progress) {
    List<EGCourbeSimple> courbes = new ArrayList<>();
    ReportLongitudinalGrapheBuilderLimit limitBuilder = new ReportLongitudinalGrapheBuilderLimit(reportService.getCcm());
    final List<ReportRunVariableKey> variables = content.getProfilVariables();
    for (ReportRunVariableKey reportRunVariableKey : variables) {

      final ReportVariableTypeEnum variableType = reportRunVariableKey.getVariable().getVariableType();
      if (ReportVariableTypeEnum.LIMIT_PROFIL.equals(variableType)) {
        if (progress != null) {
          progress.setDisplayName(reportRunVariableKey.getDisplayName());
        }
        LoiConstanteCourbeModel model = limitBuilder.build(reportRunVariableKey, result.getCartouches());
        if (model != null && model.getKey() != null && content.getCourbeconfigs() != null) {
          EGCourbeSimple courbe = new EGCourbeSimple(axeZ, model);

          AbstractReportGrapheBuilder.applyPersistConfig(content.getCourbeconfigs().get(model.getKey()), courbe);
          courbes.add(courbe);
        }
      } else if (ReportVariableTypeEnum.RESULTAT_RPTI.equals(variableType) || ReportVariableTypeEnum.LHPT.equals(variableType)) {
        if (progress != null) {
          progress.setDisplayName(reportRunVariableKey.getDisplayName());
        }
        EGCourbeSimple courbe = courbeBuilder.createCourbe(reportRunVariableKey, null, false);
        courbes.add(courbe);
      }
    }
    //on traite les variables qui dépendent du temps. Doivent être créées pour chaque pas de temps selectionné
    final List<ResultatTimeKey> times = content.getTimes();
    for (ResultatTimeKey timeKey : times) {
      for (ReportRunVariableKey reportRunVariableKey : variables) {
        if (reportRunVariableKey.isReadOrVariable() && !isExpressionNotTimeDependant(reportRunVariableKey)) {
          if (progress != null) {
            progress.setDisplayName(reportRunVariableKey.getDisplayName() + " " + timeKey.toString());
          }
          EGCourbeSimple createCourbe = courbeBuilder.createCourbe(reportRunVariableKey, timeKey, false);
          courbes.add(createCourbe);
        }
      }
    }
    //on traite les variables qui ne dépendent pas du temps: ne sont créés qu'une seule fois
    for (ReportRunVariableKey reportRunVariableKey : variables) {
      if (reportRunVariableKey.isReadOrVariable() && isExpressionNotTimeDependant(reportRunVariableKey) && isExpressionValid(reportRunVariableKey, times)) {
        if (progress != null) {
          progress.setDisplayName(reportRunVariableKey.getDisplayName());
        }
        EGCourbeSimple createCourbe = courbeBuilder.createCourbe(reportRunVariableKey, reportService.getSelectedTime(), true);
        courbes.add(createCourbe);
      }
    }
    return courbes;
  }

  /**
   * @param reportRunVariableKey
   * @return true si la variable n'est pas dépendante du temps. C'est le cas des MaxSurTous..., MaxSurSelection,...
   */
  protected boolean isExpressionNotTimeDependant(ReportRunVariableKey reportRunVariableKey) {
    boolean isExpressionNotTimeDependant = false;
    if (reportRunVariableKey.getVariable().getVariableType().equals(ReportVariableTypeEnum.FORMULE)) {
      boolean containsTimeDependantVariable = reportResultProviderService.containsTimeDependantVariable(reportRunVariableKey.getVariable().
          getVariableName());
      isExpressionNotTimeDependant = !containsTimeDependantVariable;
    }
    return isExpressionNotTimeDependant;
  }

  /**
   * @param reportRunVariableKey
   * @param times
   * @return true si l'expression est valide
   */
  protected boolean isExpressionValid(ReportRunVariableKey reportRunVariableKey, List<ResultatTimeKey> times) {
    return reportResultProviderService.isExpressionValid(reportRunVariableKey.getVariable().
        getVariableName(), times);
  }
}
