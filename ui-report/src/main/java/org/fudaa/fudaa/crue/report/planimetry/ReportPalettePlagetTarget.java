/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.planimetry;

import java.beans.PropertyChangeListener;
import java.text.NumberFormat;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.palette.BPalettePlageInterface;
import org.fudaa.ebli.palette.BPalettePlageTarget;
import org.fudaa.ebli.trace.BPlageInterface;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ReportPalettePlagetTarget implements BPalettePlageTarget {

  final ReportResultProviderService reportResultProviderService = Lookup.getDefault().lookup(ReportResultProviderService.class);
  private final ReportRunVariableKey key;
  private final EnumCatEMH emhType;
  private boolean editable = true;

  public ReportPalettePlagetTarget(ReportRunVariableKey key, EnumCatEMH emhType) {
    this.key = key;
    this.emhType = emhType;
  }

  public boolean isEditable() {
    return editable;
  }

  public void setEditable(boolean editable) {
    this.editable = editable;
  }

  @Override
  public void addPropertyChangeListener(String key, PropertyChangeListener l) {
  }

  @Override
  public void removePropertyChangeListener(String key, PropertyChangeListener l) {
  }

  @Override
  public String getTitle() {
    return (key == null || key.getVariable() == null) ? StringUtils.EMPTY : key.getVariable().geti18n();
  }

  @Override
  public boolean getRange(CtuluRange b) {
    List<ResultatTimeKey> currentTimes = reportResultProviderService.getReportService().getRangeSelectedTimeKeys();
    if (currentTimes == null) {
      return false;
    }
    boolean modified = false;
    for (ResultatTimeKey resultatKey : currentTimes) {
      modified |= getRange(b, resultatKey);
    }
    return modified;
  }

  @Override
  public boolean getTimeRange(CtuluRange b) {
    return getRange(b, reportResultProviderService.getReportService().getSelectedTime());
  }
  private BPalettePlage plage;

  public BPalettePlage getPlage() {
    return plage;
  }

  @Override
  public BPalettePlageInterface createPaletteCouleur() {
    final BPalettePlage bPalettePlage = new BPalettePlage();
    updateFormatter(bPalettePlage);
    bPalettePlage.setPlages(new BPlageInterface[0]);
    return bPalettePlage;
  }

  private boolean getRange(CtuluRange range, ResultatTimeKey time) {
    List<EMH> allSimpleEMH = reportResultProviderService.getReportService().getModele().getAllSimpleEMH();
    boolean modified = false;
    for (EMH emh : allSimpleEMH) {
      if (emhType.equals(emh.getCatType())) {
        Double readValue = reportResultProviderService.getValue(time, key, emh.getNom(), ReportResultProviderServiceContrat.NO_SELECTED_TIMES_REPORT);
        if (readValue != null) {
          modified = true;
          range.expandTo(readValue);
        }
      }
    }
    return modified;
  }

  @Override
  public BPalettePlageInterface getPaletteCouleur() {
    return plage;
  }

  @Override
  public void setPaletteCouleurPlages(BPalettePlageInterface newPlage) {
    if (plage == null) {
      plage = new BPalettePlage();
    }
    this.plage.initFrom(newPlage);
    updateFormatter(this.plage);
  }

  @Override
  public boolean isPaletteModifiable() {
    return editable;
  }

  @Override
  public boolean isDiscrete() {
    return false;
  }

  @Override
  public String getDataDescription() {
    return getTitle();
  }

  @Override
  public boolean isDonneesBoiteAvailable() {
    return true;
  }

  @Override
  public boolean isDonneesBoiteTimeAvailable() {
    return true;
  }

  private void updateFormatter(final BPalettePlage bPalettePlage) {
    if (key != null && key.getVariable() != null) {
      CrueConfigMetier ccm = reportResultProviderService.getReportService().getCcm();
      NumberFormat formatter = ccm.getFormatter(key.getVariable().getVariableName(), DecimalFormatEpsilonEnum.PRESENTATION);
      if (formatter != null) {
        bPalettePlage.setFormatter(new CtuluNumberFormatDefault(formatter));
      }
      bPalettePlage.setSousTitre(key.getVariable().getVariableDisplayName());
      bPalettePlage.setSubTitleLabel(key.getVariable().getVariableDisplayName());
      bPalettePlage.setSubTitleVisible(true);
    }
  }
}
