/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.service;

import org.fudaa.fudaa.crue.common.services.AbstractSelectedEMHService;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 * conserve les UID des EMH sélectionnées. Pour la liste des lookup voir {@link org.fudaa.fudaa.crue.common.services.AbstractSelectedEMHService}.
 *
 * @see org.fudaa.fudaa.crue.common.services.AbstractSelectedEMHService
 *
 * @author Frederic Deniger
 */
@ServiceProvider(service = ReportSelectedEMHService.class)
public class ReportSelectedEMHService extends AbstractSelectedEMHService implements Lookup.Provider {

  /**
   *
   * @return le lookup du service
   */
  @Override
  public Lookup getLookup() {
    return super.getLookup();
  }
}
