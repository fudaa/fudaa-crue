/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.planimetry;

import java.awt.Color;
import org.fudaa.dodico.crue.metier.emh.CatEMHCasier;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryCasierExtraData;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.planimetry.configuration.CasierConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.CasierConfigurationExtra;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryCasierLayerModel;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;

/**
 *
 * @author Frederic Deniger
 */
public class ReportPlanimetryCasierConfig extends AbstractConfigurationExtra<ReportPlanimetryCasierExtraData> implements CasierConfigurationExtra {

  public ReportPlanimetryCasierConfig(ReportResultProviderService reportResultProviderService) {
    super(reportResultProviderService);
     data = new ReportPlanimetryCasierExtraData();
  }

  @Override
  public void decoreTraceLigne(TraceLigneModel ligne, CatEMHCasier casier, PlanimetryCasierLayerModel casierModel, CasierConfiguration aThis) {
  }

  @Override
  public void decoreTraceIcon(TraceIconModel modelIcon, CatEMHCasier casier, PlanimetryCasierLayerModel casierModel, CasierConfiguration aThis) {
  }

  @Override
  public String getDisplayName(CatEMHCasier casier, PlanimetryCasierLayerModel casierModel, boolean selected, CasierConfiguration casierConfiguration) {
    return getDisplayName(casier, selected);
  }

  @Override
  public Color getFond(CatEMHCasier casier, PlanimetryCasierLayerModel casierModel) {
    return getColor(casier);
  }
}
