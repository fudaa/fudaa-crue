/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.longitudinal;

import org.apache.commons.collections4.Transformer;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBrancheConfig;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertyStringReadOnly;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;

import java.awt.datatransfer.Transferable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Frederic Deniger
 */
public class ReportLongitudinalBrancheConfigNode extends AbstractNodeFirable implements PropertyChangeListener {
    final PerspectiveServiceReport perspectiveServiceReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);
    final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);

    public ReportLongitudinalBrancheConfigNode(final ReportLongitudinalBrancheConfig config) {
        super(Children.LEAF, Lookups.singleton(config));
        setNoImage();
        setValue(DefaultNodePasteType.PROP_USE_INDEX_AS_DISPLAY_NAME, Boolean.TRUE);
        addPropertyChangeListener(this);
    }

    @Override
    public SystemAction[] getActions() {
        return new SystemAction[]{SystemAction.get(ReportLongitudinalBrancheConfigRestoreLengthNodeAction.class)};
    }

    @Override
    public boolean isEditMode() {
        return perspectiveServiceReport.isInEditMode();
    }

    @Override
    public void propertyChange(final PropertyChangeEvent evt) {
        if (ReportLongitudinalBrancheConfig.PROP_NAME.equals(evt.getPropertyName())) {
            updateLength();
        }
    }

    @Override
    public Transferable clipboardCut() throws IOException {
        return super.clipboardCut();
    }

    @Override
    public Transferable clipboardCopy() throws IOException {
        return super.clipboardCopy();
    }

    protected void updateLength() {
        final ReportLongitudinalBrancheConfig config = getLookup().lookup(ReportLongitudinalBrancheConfig.class);
        final CatEMHBranche branche = (CatEMHBranche) reportService.getRunCourant().getEMH(config.getName());
        if (branche != null) {
            final double length = branche.getLength();
            config.setLength(length);
            fireObjectChange(ReportLongitudinalBrancheConfig.PROP_LENGTH, null, length);
        }
    }

    @Override
    protected void fireObjectChange(final String property, final Object oldValue, final Object newValue) {
        super.fireObjectChange(property, oldValue, newValue);
    }

    @Override
    public Transferable drag() throws IOException {
        return super.drag();
    }

    @Override
    public PasteType getDropType(final Transferable t, final int action, final int index) {
        if (isEditMode()) {
            final Node dragged = DefaultNodePasteType.getMovedNodeInTransferable(t);
            if (dragged == null) {
                return null;
            }
            return new DefaultNodePasteType(dragged, this);
        }
        return null;
    }

    @Override
    public boolean canDestroy() {
        return isEditMode();
    }

    @Override
    public boolean canCopy() {
        return true;
    }

    @Override
    public boolean canCut() {
        return isEditMode();
    }

    @Override
    protected Sheet createSheet() {
        final ReportLongitudinalBrancheConfig brancheConfig = getLookup().lookup(ReportLongitudinalBrancheConfig.class);
        final PropertyNodeBuilder builder = new PropertyNodeBuilder();
        final List<PropertySupportReflection> createFromPropertyDesc = builder.
                createFromPropertyDesc(DecimalFormatEpsilonEnum.PRESENTATION, brancheConfig, this);
        final Sheet res = Sheet.createDefault();
        final Sheet.Set set = Sheet.createPropertiesSet();
        res.put(set);
        final List<String> toNom = TransformerHelper.toNom(reportService.getModele().getBranches());
        if (toNom.isEmpty()) {
            toNom.add(StringUtils.EMPTY);
        } else {
            toNom.add(0, StringUtils.EMPTY);
        }
        for (final PropertySupportReflection propertySupportReflection : createFromPropertyDesc) {
            set.put(propertySupportReflection);
            PropertyNodeBuilder.configurePropertySupport(null, propertySupportReflection, this);
            if (propertySupportReflection.getName().equals(ReportLongitudinalBrancheConfig.PROP_NAME)) {
                propertySupportReflection.setTagsFilter(new TagsFilter());
                propertySupportReflection.setTags(toNom);
            }
        }
        final CatEMHBranche branche = (CatEMHBranche) reportService.getRunCourant().getEMH(brancheConfig.getName());
        if (branche != null) {
            final String value = TransformerEMHHelper.formatFromPropertyName(CrueConfigMetierConstants.PROP_XP, branche.getLength(), reportService.getCcm(),
                    DecimalFormatEpsilonEnum.PRESENTATION);
            final String valueDetail = TransformerEMHHelper.formatFromPropertyName(CrueConfigMetierConstants.PROP_XP, branche.getLength(), reportService.getCcm(),
                    DecimalFormatEpsilonEnum.PRESENTATION);
            final String displayName = NbBundle.getMessage(ReportLongitudinalBrancheConfigNode.class, "lengthHyd.property");
            final PropertyStringReadOnly lenghtHyd = new PropertyStringReadOnly(value, ReportLongitudinalBrancheConfig.PROP_LENGTH_HYD, displayName, displayName);
            lenghtHyd.setShortDescription(valueDetail);
            set.put(lenghtHyd);
        }

        return res;
    }

    private class TagsFilter implements Transformer {
        @Override
        public Object transform(final Object input) {
            final Set<String> toAvoid = new HashSet<>();
            final Node[] nodes = getParentNode().getChildren().getNodes();
            for (final Node node : nodes) {
                if (node != ReportLongitudinalBrancheConfigNode.this) {
                    final ReportLongitudinalBrancheConfig conf = node.getLookup().lookup(ReportLongitudinalBrancheConfig.class);
                    if (conf != null && StringUtils.isNotEmpty(conf.getName())) {
                        toAvoid.add(conf.getName());
                    }
                }
            }
            final String[] inputNames = (String[]) input;
            final List<String> out = new ArrayList<>();
            for (final String inputName : inputNames) {
                if (!toAvoid.contains(inputName)) {
                    out.add(inputName);
                }
            }
            return out.toArray(new String[0]);
        }
    }
}
