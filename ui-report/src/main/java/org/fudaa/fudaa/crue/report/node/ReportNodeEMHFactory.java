/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.node;

import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.fudaa.crue.common.ContainerActivityVisibility;
import org.fudaa.fudaa.crue.emh.node.NodeEMHFactory;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;

/**
 *
 * @author Frederic Deniger
 */
public class ReportNodeEMHFactory extends NodeEMHFactory {


  public ReportNodeEMHFactory() {
    super(null);
  }

  @Override
  protected AbstractNode createNodeEMH(Children children, EMH emh, String displayName) {
    if (emh.getCatType().equals(EnumCatEMH.SECTION)) {
      return new ReportNodeCatEMHSection(children, (CatEMHSection) emh, displayName);
    } else if (emh.getCatType().equals(EnumCatEMH.BRANCHE)) {
      return new ReportNodeCatEMHBranche(children, (CatEMHBranche) emh, displayName);
    }
    return new ReportNodeEMH(children, emh, displayName);
  }

  @Override
  protected AbstractNode createNodeEMH(Children children, EMH emh) {
    if (emh.getCatType().equals(EnumCatEMH.SECTION)) {
      return new ReportNodeCatEMHSection(children, emh);
    } else if (emh.getCatType().equals(EnumCatEMH.BRANCHE)) {
      return new ReportNodeCatEMHBranche(children, (CatEMHBranche) emh);
    }
    return new ReportNodeEMH(children, emh);
  }
}
