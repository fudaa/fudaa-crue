/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import java.io.File;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.projet.report.formule.FormuleParametersExpr;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleSaverProcessor implements ProgressRunnable<CtuluLog> {

  private final File file;
  private final List<FormuleParametersExpr> keys;

  public FormuleSaverProcessor(File file, List<FormuleParametersExpr> expr) {
    this.file = file;
    this.keys = expr;
  }

  @Override
  public CtuluLog run(ProgressHandle handle) {
    if (handle != null) {
      handle.switchToIndeterminate();
    }
    ReportFormuleReaderSaver configSaver = new ReportFormuleReaderSaver();
    return configSaver.save(file, keys);
  }
}
