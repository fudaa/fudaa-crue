/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.actions;

import java.util.*;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.data.ReportExpressionHelper;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalConfig;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.report.longitudinal.ReportProfilLongitudinalTopComponent;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.fudaa.fudaa.crue.report.service.ReportTemplateFileProvider;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

/**
 *
 * @author Frederic Deniger
 */
public class ReportOpenProfilLongitudinalViewNodeAction extends AbstractReportOpenTopComponentNodeAction {

  final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);

  public ReportOpenProfilLongitudinalViewNodeAction() {
    this(NbBundle.getMessage(ReportOpenProfilLongitudinalViewNodeAction.class, "OpenProfilLongitudinal.ActionName"));
  }

  private ReportOpenProfilLongitudinalViewNodeAction(final String name) {
    super(name, ReportProfilLongitudinalTopComponent.MODE);
  }

  @Override
  protected TopComponent createNewTopComponentFor(final Node selectedNode) {
    final EMH emh = selectedNode.getLookup().lookup(EMH.class);
    if (emh != null && emh.getCatType().equals(EnumCatEMH.BRANCHE)) {
      final ReportProfilLongitudinalTopComponent tc = create();
      tc.setReportConfig(createDefaultContent(Collections.singletonList((CatEMHBranche) emh)), true);
      return tc;
    }

    return null;
  }

  @Override
  protected void postOpenNewTopComponent(final TopComponent openNew) {
    ((ReportProfilLongitudinalTopComponent) openNew).chooseBranches();
  }

  @Override
  protected TopComponent createNewTopComponentFor(final Node[] selectedNode) {
    final List<CatEMHBranche> branches = new ArrayList<>();
    for (final Node node : selectedNode) {
      final EMH emh = node.getLookup().lookup(EMH.class);
      if (emh.getCatType().equals(EnumCatEMH.BRANCHE)) {
        branches.add((CatEMHBranche) emh);
      }
    }
    if (!branches.isEmpty()) {
      final ReportProfilLongitudinalTopComponent tc = create();
      tc.setReportConfig(createDefaultContent(branches), true);
      return tc;
    }

    return null;
  }

  public static void open(final Collection branches) {
    if (branches == null) {
      return;
    }
    final ReportOpenProfilLongitudinalViewNodeAction action = SystemAction.get(ReportOpenProfilLongitudinalViewNodeAction.class);
    final Node[] nodes = NodeHelper.createNodesWithLookup(branches);
    action.performAction(nodes);

  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    if (activatedNodes != null && activatedNodes.length >= 1) {
      for (final Node node : activatedNodes) {
        final EMH emh = node.getLookup().lookup(EMH.class);
        if(emh==null){
          return  false;
        }
        if (emh.getCatType().equals(EnumCatEMH.BRANCHE)) {
          return true;
        }
      }

    }
    return false;
  }

    private ReportLongitudinalConfig createDefaultContent(final List<CatEMHBranche> branches) {
    final ReportTemplateFileProvider template = new ReportTemplateFileProvider();
    ReportLongitudinalConfig content = template.loadTemplate(ReportContentType.LONGITUDINAL);
    if (content == null) {
      content = new ReportLongitudinalConfig();
      content.addProfilVariable(new ReportRunVariableKey(reportService.getRunCourant().getRunKey(), ReportVariableTypeEnum.READ,
              CrueConfigMetierConstants.PROP_Z));
      final List<LabelConfig> labels = new ArrayList<>();
      labels.add(createLabelTime());
      content.getLoiLegendConfig().setLabels(labels);
    }
    content.initBranchesWith(branches);

    return content;
  }

  private static LabelConfig createLabelTime() {
    final ReportLabelContent label = new ReportLabelContent();
    label.setContent(new ReportVariableKey(ReportVariableTypeEnum.TIME, ReportExpressionHelper.TIME_TEMPS_SCE));
    label.setPrefix(NbBundle.getMessage(ReportOpenProfilLongitudinalViewNodeAction.class, "DefaultPrefixTempsSce"));
    label.setUnit(false);
    return createLabelConfig(label);
  }

  public static LabelConfig createLabelConfig(final ReportLabelContent label) {
    final LabelConfig res = new LabelConfig();
    res.setKey(label);
    return res;
  }

  public ReportProfilLongitudinalTopComponent create() {
    ReportProfilLongitudinalTopComponent res = super.getMainTopComponentAvailable();
    if (res == null) {
      res = new ReportProfilLongitudinalTopComponent();
    }
    return res;
  }
}
