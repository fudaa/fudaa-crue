/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.longitudinal;

import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableHelper;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableTimeKey;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBrancheCartouche;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalConfig;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalPositionSection;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalPositionSectionByRun;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.fudaa.crue.loi.common.LoiConstanteCourbeModel;
import org.fudaa.fudaa.crue.report.config.AbstractReportGrapheBuilder;
import org.openide.util.NbBundle;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Frederic Deniger
 */
public class ReportLongitudinalGrapheBuilderCourbe {
  final CrueConfigMetier ccm;
  final ReportLongitudinalGrapheBuilder builder;
  final EGAxeVertical axeZ;
  final EGAxeVertical rightAxis;
  final ReportLongitudinalConfig content;

  public ReportLongitudinalGrapheBuilderCourbe(CrueConfigMetier ccm, ReportLongitudinalGrapheBuilder result, ReportLongitudinalConfig content,
                                               Map<String, EGAxeVertical> saved) {
    this.ccm = ccm;
    this.builder = result;
    this.content = content;
    axeZ = builder.getOrCreateAxeVerticalZ(content, saved);
    rightAxis = builder.getRightAxis(content, saved);
  }

  public static void connectVerticalSegment(LoiConstanteCourbeModel model, CrueConfigMetier ccm) {
    PropertyEpsilon epsilon = ccm.getEpsilon(CrueConfigMetierConstants.PROP_XP);
    CtuluListSelection drawnSegment = model.getDrawnSegment();
    int nbSegment = model.getNbValues() - 1;
    for (int i = 0; i < nbSegment; i++) {
      if (!drawnSegment.isSelected(i)) {
        double xp = model.getX(i);
        double xpNext = model.getX(i + 1);
        if (epsilon.isSame(xp, xpNext)) {
          drawnSegment.add(i);
        }
      }
    }
  }

  public EGAxeVertical getAxeZ() {
    return axeZ;
  }

  public EGAxeVertical getRightAxis() {
    return rightAxis;
  }

  EGCourbeSimple createCourbe(final ReportRunVariableKey reportRunVariableKey, final ResultatTimeKey selectedTime, final boolean currentTime) {

    TDoubleArrayList xps = new TDoubleArrayList();
    TDoubleArrayList values = new TDoubleArrayList();
    TIntObjectHashMap labels = new TIntObjectHashMap();
    Map<String, TIntObjectHashMap<String>> sublabels = new HashMap<>();
    TIntObjectHashMap<String> labelBranches = new TIntObjectHashMap<>();
    TIntObjectHashMap<String> labelSections = new TIntObjectHashMap<>();
    sublabels.put(CruePrefix.P_BRANCHE, labelBranches);
    sublabels.put(CruePrefix.P_SECTION, labelSections);
    CtuluListSelection drawnSegment = new CtuluListSelection();
    for (ReportLongitudinalBrancheCartouche cartouche : builder.result.getCartouches()) {
      ReportLongitudinalPositionSectionByRun positions = cartouche.getSectionPositions(reportRunVariableKey.getRunKey());
      if (positions == null) {
        continue;
      }
      List<ReportLongitudinalPositionSection> sectionPositions = positions.getSectionDisplayPositions();
      boolean firstAddInBranche = true;

      for (ReportLongitudinalPositionSection sectionPosition : sectionPositions) {
        CatEMHSection section = sectionPosition.getName();
        Double value = builder.getReportResultProviderService().getValue(selectedTime, reportRunVariableKey, section.getNom(), content.
            getSelectedTimeStepInReport());
        if (value != null) {
          if (!firstAddInBranche) {//ce n'est pas le premier point de la ligne
            drawnSegment.add(xps.size() - 1);//segment dessine
          }
          labels.put(xps.size(), section.getBranche().getNom() + " / " + section.getNom());//le faire avant l'ajout !
          labelBranches.put(xps.size(), section.getBranche().getNom());
          labelSections.put(xps.size(), section.getNom());
          xps.add(sectionPosition.getXpDisplay());
          values.add(value);
          firstAddInBranche = false;
        }
      }
    }

    ReportRunVariableTimeKey key = new ReportRunVariableTimeKey(reportRunVariableKey, currentTime ? null : selectedTime);
    ItemVariable varX = ccm.getProperty(CrueConfigMetierConstants.PROP_XP);
    ItemVariable varY = builder.getReportResultProviderService().getCcmVariable(reportRunVariableKey);
    LoiConstanteCourbeModel model = LoiConstanteCourbeModel.create(key, xps, values, varX, varY);
    model.setLabelColumnName(NbBundle.getMessage(ReportLongitudinalCourbesUiResController.class, "BrancheSectionColumn.Name"));
    model.setDrawnSegment(drawnSegment);
    model.setLabels(labels);
    model.setSubLabels(sublabels);
    boolean isOnZ = ReportRunVariableHelper.isNatZVar(reportRunVariableKey.getVariable(), builder.getReportResultProviderService());
    final EGAxeVertical axisToUse = isOnZ ? axeZ : rightAxis;
    connectVerticalSegment(model, ccm);
    EGCourbeSimple courbe = new EGCourbeSimple((axisToUse), model);
    builder.updateReadCourbeName(courbe);

    EGCourbePersist egCourbePersist = content.getCourbeconfigs().get(model.getKey());
    AbstractReportGrapheBuilder.applyPersistConfig(egCourbePersist, courbe);
    //par defaut, pour les courbes LHPT, des nuages de points sont utilisés:
    if (key.isLHPTVariable() && egCourbePersist == null) {
      egCourbePersist = courbe.getPersitUiConfig();
      egCourbePersist.setNuagePoints(true);
      AbstractReportGrapheBuilder.applyPersistConfig(egCourbePersist, courbe);
    }
    return courbe;
  }
}
