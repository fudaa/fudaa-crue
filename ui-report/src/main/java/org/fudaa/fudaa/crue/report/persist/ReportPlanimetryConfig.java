/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.persist;

import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.planimetry.LayerVisibility;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryExtraContainer;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.fudaa.crue.planimetry.configuration.VisuConfiguration;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Frederic Deniger
 */
public class ReportPlanimetryConfig implements ReportConfigContrat {

  private VisuConfiguration visuConfiguration;
  private ReportPlanimetryExtraContainer reportPlanimetryExtraContainer = new ReportPlanimetryExtraContainer();
  private LayerVisibility visibility = new LayerVisibility();
  private Map<String, Boolean> visibilityByExternLayerName;
  EbliUIProperties legendPropertes;

  public VisuConfiguration getVisuConfiguration() {
    return visuConfiguration;
  }

  @Override
  public boolean variablesUpdated(FormuleDataChanges changes) {
    return reportPlanimetryExtraContainer.variablesUpdated(changes);
  }

  @Override
  public Collection<ResultatTimeKey> getSelectedTimeStepInReport() {
    return ReportResultProviderServiceContrat.NO_SELECTED_TIMES_REPORT;
  }

  public EbliUIProperties getLegendProperties() {
    return legendPropertes;
  }

  public void setLegendPropertes(EbliUIProperties legendPropertes) {
    this.legendPropertes = legendPropertes;
  }

  public LayerVisibility getVisibility() {
    return visibility;
  }

  public void setVisibility(LayerVisibility visibility) {
    this.visibility = visibility;
  }

  public void setVisuConfiguration(VisuConfiguration visuConfiguration) {
    this.visuConfiguration = visuConfiguration;
  }

  @Override
  public void reinitContent() {
  }

  public ReportPlanimetryExtraContainer getReportPlanimetryExtraContainer() {
    return reportPlanimetryExtraContainer;
  }

  public void setReportPlanimetryExtraContainer(ReportPlanimetryExtraContainer reportPlanimetryExtraContainer) {
    this.reportPlanimetryExtraContainer = reportPlanimetryExtraContainer;
  }

  @Override
  public ReportConfigContrat createTemplateConfig() {
    ReportPlanimetryConfig duplicate = (ReportPlanimetryConfig) duplicate();
    if (duplicate.visibilityByExternLayerName != null) {
      duplicate.visibilityByExternLayerName.clear();
    }
    return duplicate;
  }

  @Override
  public ReportConfigContrat duplicate() {
    ReportPlanimetryConfig config = new ReportPlanimetryConfig();

    if (reportPlanimetryExtraContainer != null) {
      config.reportPlanimetryExtraContainer = reportPlanimetryExtraContainer.copy();
    }
    if (visuConfiguration != null) {
      config.visuConfiguration = visuConfiguration.copy();
    }
    if (visibilityByExternLayerName != null) {
      config.visibilityByExternLayerName = new HashMap<>();
      config.visibilityByExternLayerName.putAll(visibilityByExternLayerName);
    }
    return config;
  }

  @Override
  public ReportContentType getType() {
    return ReportContentType.PLANIMETRY;
  }

  public boolean isExternLayerVisible(BCalque cq) {
    if (visibilityByExternLayerName == null) {
      return true;
    }
    //par defaut un calque est visible
    boolean invisible = Boolean.FALSE.equals(visibilityByExternLayerName.get(cq.getName()));
    return !invisible;
  }

  public void setVisibilityForExternLayer(Map<String, Boolean> visibilityByExternrLayerName) {
    this.visibilityByExternLayerName = visibilityByExternrLayerName;
  }

  public Map<String, Boolean> getVisibilityByExternLayerName() {
    return visibilityByExternLayerName;
  }
}
