/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.report.ordres;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.metier.OrdResKey;
import org.fudaa.dodico.crue.metier.result.OrdResVariableSelection;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frédéric Deniger
 */
public class SelectOrdResNode extends AbstractNodeFirable {

  public SelectOrdResNode(List<OrdResVariableSelection> selections) {
    super(Children.LEAF, Lookups.singleton(selections));
  }

  @Override
  public HelpCtx getHelpCtx() {
      //A corriger FLA
    List ordResVariableSelectionList = getOrdResVariableSelectionList();
    if(ordResVariableSelectionList != null && !ordResVariableSelectionList.isEmpty())
    {
        OrdResVariableSelection selection = (OrdResVariableSelection) ordResVariableSelectionList.get(0);    
        return new HelpCtx(SysdocUrlBuilder.getHelpInfosCtxId(selection));
    }    
    return null;
  }
  
  @Override
  public boolean isEditMode() {
    return true;
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
    List ores = getOrdResVariableSelectionList();
    for (Object object : ores) {
      sheet.put(createSet((OrdResVariableSelection) object));
    }
    return sheet;
  }

  private Sheet.Set createSet(OrdResVariableSelection in) {
    Sheet.Set set = new Sheet.Set();
    set.setName(in.getKey().geti18n());
    set.setDisplayName(in.getKey().geti18n());
    List<String> variables = in.getSelectableVariables();
    for (String var : variables) {
      set.put(new SelectOrdResPropertySupport(this, in, var));

    }
    return set;
  }

  public List getOrdResVariableSelectionList() {
    return getLookup().lookup(List.class);
  }

  public Map<OrdResKey, List<String>> getSelectedVariables() {
    List ordResVariableSelectionList = getOrdResVariableSelectionList();
    Map<OrdResKey, List<String>> variableByOrdRes = new HashMap<>();
    for (Object object : ordResVariableSelectionList) {
      OrdResVariableSelection selection = (OrdResVariableSelection) object;
      variableByOrdRes.put(selection.getKey(), selection.getSelectedVariables());
    }
    return variableByOrdRes;
  }
}
