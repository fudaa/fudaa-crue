/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.config;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.io.extern.ExternContent;
import org.fudaa.dodico.crue.io.extern.ExternContentColumn;
import org.fudaa.dodico.crue.projet.report.data.ExternFileKey;
import org.fudaa.dodico.crue.projet.report.persist.AbstractReportCourbeConfig;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.loi.common.LoiConstanteCourbeModel;

import java.io.File;
import java.util.*;

/**
 * @author Frederic Deniger
 */
public class ExternFileBuilder {
    private final Map<String, ExternContent> contentsByFilename = new LinkedHashMap<>();

    Map<String, ExternContent> getContentsByFilename() {
        return contentsByFilename;
    }

    /**
     * appelé si les fichiers externes sont modifiés.
     *
     * @param config la configuration
     */
    public void loadFiles(AbstractReportCourbeConfig config) {
        ExternFileBuilderProcessor processor = new ExternFileBuilderProcessor(this, config);
        if (!processor.isSomethingToLoad()) {
            return;
        }
        final String name = org.openide.util.NbBundle.getMessage(ExternFileBuilder.class, "ExternFile.Loading");

        CtuluLogGroup logs = CrueProgressUtils.showProgressDialogAndRun(processor, name);
        if (logs != null && logs.containsSomething()) {
            LogsDisplayer.displayError(logs, name);
        }
    }

    public List<EGCourbeSimple> synchronize(EGAxeHorizontal axeX, Collection<EGAxeVertical> axesV) {
        String xVar = axeX.getTitre();
        Map<String, EGAxeVertical> axeByName = new LinkedHashMap<>();
        for (EGAxeVertical axe : axesV) {
            axeByName.put(axe.getTitre(), axe);
        }
        List<EGCourbeSimple> newCourbes = new ArrayList<>();
        Collection<ExternContent> values = contentsByFilename.values();
        for (ExternContent externContent : values) {
            ExternContentColumn columnX = externContent.getColumn(xVar);
            if (columnX != null) {
                for (Map.Entry<String, EGAxeVertical> entry : axeByName.entrySet()) {
                    String yVar = entry.getKey();
                    EGAxeVertical eGAxeVertical = entry.getValue();
                    ExternContentColumn columnY = externContent.getColumn(yVar);
                    if (columnY != null) {
                        ExternFileKey extKey = new ExternFileKey(externContent.getFilePath(), xVar, yVar);
                        LoiConstanteCourbeModel model = LoiConstanteCourbeModel.create(extKey, columnX, columnY, externContent, null, null);
                        model.setTitle(new File(externContent.getFilePath()).getName() + " " + xVar + "/" + yVar);
                        EGCourbeSimple courbe = new EGCourbeSimple(eGAxeVertical, model);
                        newCourbes.add(courbe);
                    }
                }
            }
        }
        return newCourbes;
    }
}
