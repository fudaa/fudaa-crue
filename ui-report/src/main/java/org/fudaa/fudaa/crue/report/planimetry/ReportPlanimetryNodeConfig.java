/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.planimetry;

import java.awt.Color;
import org.fudaa.dodico.crue.metier.emh.CatEMHNoeud;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryNodeExtraData;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.crue.planimetry.configuration.NodeConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.NodeConfigurationExtra;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryPointLayerModel;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;

/**
 *
 * @author Frederic Deniger
 */
public class ReportPlanimetryNodeConfig extends AbstractConfigurationExtra<ReportPlanimetryNodeExtraData> implements NodeConfigurationExtra {

  public ReportPlanimetryNodeConfig(ReportResultProviderService reportResultProviderService) {
    super(reportResultProviderService);
    data = new ReportPlanimetryNodeExtraData();
  }

  @Override
  public void decoreTraceIcon(TraceIconModel ligne, CatEMHNoeud noeud, PlanimetryPointLayerModel model) {
    Color color = getColor(noeud);
    if (color != null) {
      ligne.setCouleur(color);
    }
  }

  @Override
  public String getDisplayedLabel(CatEMHNoeud noeud, NodeConfiguration aThis, boolean isSelected) {
    return getDisplayName(noeud, isSelected);
  }
}
