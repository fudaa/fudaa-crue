/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.service;

import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.dodico.crue.projet.report.transformer.KeysToStringConverter;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.fudaa.fudaa.crue.report.view.ReportViewsService;
import org.fudaa.fudaa.crue.report.view.persist.ReportConfigLoaderProcessor;
import org.fudaa.fudaa.crue.report.view.persist.ReportConfigSaver;
import org.openide.util.Lookup;

import java.io.File;
import java.util.logging.Logger;

/**
 * @author Frederic Deniger
 */
public class ReportTemplateFileProvider {
  private final InstallationService installationService = Lookup.getDefault().lookup(InstallationService.class);
  private final ReportViewsService reportViewsService = Lookup.getDefault().lookup(ReportViewsService.class);
  private final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
  private final String folderName = "report-templates";

  private File getTemplateToLoad(final ReportContentType type) {
    final File etudeTemplateTarget = getEtudeTemplateTarget(type);
    if (etudeTemplateTarget.exists()) {
      return etudeTemplateTarget;
    }
    return getTargetFile(installationService.getSiteDir(), type);
  }

  public <T extends ReportConfigContrat> T loadTemplate(final ReportContentType type) {
    final File templateToLoad = getTemplateToLoad(type);
    T content = null;
    if (templateToLoad.isFile()) {
      final ReportConfigLoaderProcessor loader = new ReportConfigLoaderProcessor(templateToLoad);
      content = (T) loader.run(null).getMetier();
    }
    if (content == null) {
      return null;
    }
    return (T) content.createTemplateConfig();
  }

  public void saveTemplate(final ReportConfigContrat contrat) {
    final File file = getEtudeTemplateTarget(contrat.getType());
    if (file.exists()) {
      CrueFileHelper.deleteFile(file, getClass());
    }
    //on reconstruit les dossiers parents
    if (!file.getParentFile().isDirectory()) {
      boolean mkdirs = file.getParentFile().mkdirs();
      if (!mkdirs) {
        Logger.getLogger(getClass().getName()).warning("Can't create directory " + file.getParentFile().getAbsolutePath());
      }
    }
    final KeysToStringConverter keysToString = new KeysToStringConverter(reportService.getCurrentRunKey());
    final ReportConfigSaver configSaver = new ReportConfigSaver(new TraceToStringConverter(), keysToString);
    configSaver.save(file, contrat.createTemplateConfig(), "template");
  }

  private File getEtudeTemplateTarget(final ReportContentType type) {
    final EMHProjet projectLoaded = reportViewsService.getProjectLoaded();
    final File dirOfRapports = projectLoaded.getInfos().getDirOfRapports();
    return getTargetFile(dirOfRapports, type);
  }

  private File getTargetFile(final File dirOfRapports, final ReportContentType type) {
    final File templates = new File(dirOfRapports, folderName);
    return new File(templates, type.getFolderName() + ".xml");
  }
}
