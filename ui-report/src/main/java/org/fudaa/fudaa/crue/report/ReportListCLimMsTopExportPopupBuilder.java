/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.MissingResourceException;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.fudaa.ctulu.gui.CtuluPopupListener;
import org.fudaa.ctulu.gui.CtuluTableSimpleExporter;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.views.LoiDisplayer;
import org.fudaa.fudaa.crue.views.LoiDisplayerInstaller;
import org.fudaa.fudaa.crue.views.export.DonClimMExportTableModel;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportListCLimMsTopExportPopupBuilder extends LoiDisplayerInstaller implements CtuluPopupListener.PopupReceiver {

  final ReportListCLimMsTopComponent tc;

  public ReportListCLimMsTopExportPopupBuilder(ReportListCLimMsTopComponent tc, LoiDisplayer loiDisplayer) {
    super(loiDisplayer);
    this.tc = tc;
  }

  @Override
  protected void fillWithCustomItems(JPopupMenu menu) {
    if (menu.getComponentCount() > 0) {
      menu.addSeparator();
    }
    menu.add(createExportMenuItem());
  }

  protected void exportTable() {
    DonClimMExportTableModel exportModel = new DonClimMExportTableModel(tc.tableModel, tc.getCcm());
    CtuluTableSimpleExporter.doExport(';', exportModel, CtuluUIForNetbeans.DEFAULT);
  }

  protected JMenuItem createExportMenuItem() throws MissingResourceException {
    final JMenuItem menuItemExport = new JMenuItem(NbBundle.getMessage(ReportListCLimMsTopExportPopupBuilder.class, "button.export.name"));
    menuItemExport.setIcon(EbliResource.EBLI.getToolIcon("crystal_exporter"));
    menuItemExport.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        exportTable();
      }
    });
    return menuItemExport;
  }
}
