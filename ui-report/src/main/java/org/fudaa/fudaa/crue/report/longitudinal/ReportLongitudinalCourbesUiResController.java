/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.longitudinal;

import javax.swing.table.AbstractTableModel;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGTableAction;
import org.fudaa.ebli.courbe.EGTableGraphePanel;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.loi.common.LoiConstanteCourbeModel;
import org.fudaa.fudaa.crue.loi.res.CourbesUiResController;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public final class ReportLongitudinalCourbesUiResController extends CourbesUiResController {

  public ReportLongitudinalCourbesUiResController() {
    EGTableAction tableAction = (EGTableAction) getActions("TABLE").get(0);
    tableAction.setUi(CtuluUIForNetbeans.DEFAULT);
    tableAction.setAddOptions(false);
    tableAction.setAddCheckbox(false);
    tableAction.setShowColumnToExport(false);
    tableAction.setDisplayAll(true);
    tableAction.setShowLabel(true);
    setUseVariableForAxeH(true);
    getGraphe().setExportDataBuilder(new ReportLongitudinalExportDataBuilder());
  }

  @Override
  protected AbstractTableModel createTableModel() {
    return new ReportLongitudinalTableModel(getTableGraphePanel());
  }

  @Override
  protected void configureTablePanel() {
    super.configureTablePanel();
    tableGraphePanel.getTable().getColumnModel().getColumn(0).setCellRenderer(new CtuluCellTextRenderer());
  }

  protected static class ReportLongitudinalTableModel extends EGTableGraphePanel.SpecTableModel {

    public static final int COLUMN_BRANCHE = 0;
    public static final int COLUMN_SECTION = 1;

    public ReportLongitudinalTableModel(EGTableGraphePanel graphePanel) {
      super(graphePanel);
      xColIndex = 2;
      yColIndex = 3;
    }

    @Override
    public int getColumnCount() {
      return 4;
    }

    @Override
    public Object getValueAt(int _rowIndex, int _columnIndex) {
      if (_columnIndex == COLUMN_SECTION || _columnIndex == COLUMN_BRANCHE) {
        final EGCourbe courbe = getCourbe();
        String res = null;
        if (courbe != null) {
          LoiConstanteCourbeModel model = (LoiConstanteCourbeModel) courbe.getModel();
          String key = CruePrefix.P_BRANCHE;
          if (_columnIndex == COLUMN_SECTION) {
            key = CruePrefix.P_SECTION;
          }
          res = model.getSubLabels(key).get(_rowIndex);
        }
        return StringUtils.defaultString(res);
      }
      return super.getValueAt(_rowIndex, _columnIndex);
    }

    @Override
    public String getColumnName(int _column) {
      if (_column == COLUMN_SECTION) {
        return NbBundle.getMessage(ReportLongitudinalCourbesUiResController.class, "SectionColumn.Name");
      }
      if (_column == COLUMN_BRANCHE) {
        return NbBundle.getMessage(ReportLongitudinalCourbesUiResController.class, "BrancheColumn.Name");
      }
      return super.getColumnName(_column);
    }

    @Override
    public Class getColumnClass(int _columnIndex) {
      if (_columnIndex <= COLUMN_SECTION) {
        return String.class;
      }
      return Double.class;
    }

    @Override
    public void setValueAt(Object _value, int _rowIndex, int _columnIndex) {
    }
  }
}
