/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report;

import org.fudaa.dodico.crue.projet.report.ReportViewLineInfo;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;

/**
 * @author Frederic Deniger
 */
public interface ReportTopComponentConfigurable<T extends ReportConfigContrat> {
    String VIEW_NODE = "VIEW_NODE";

    T getReportConfig();

    /**
     * @param newConfig    la nouvelle coniguration
     * @param fromTemplate si true, le graphe doit être restaurer
     */
    void setReportConfig(T newConfig, boolean fromTemplate);

    /**
     * @param info les infos issue de vue View
     */
    void setReportInfo(ReportViewLineInfo info);

    AbstractReportTopComponent getTopComponent();

    /**
     * Methode à appeler si le nom de la fenêtre change
     */
    void updateFrameName();

    /**
     * @return true si peut etre fermé
     */
    boolean isClosable();

    /**
     * si le topcomponent n'est pas fermé, on demande sa réinitialisation si la vue est supprimée ou fermée.
     */
    void clearReportModifications();
}
