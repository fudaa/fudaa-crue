/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.service;

import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.common.time.TimeFormatOption;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.result.ModeleResultatTimeKeyContent;
import org.fudaa.dodico.crue.projet.report.data.ReportRunContent;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.fudaa.crue.aoc.service.LhptService;
import org.fudaa.fudaa.crue.common.services.PostRunService;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ServiceProvider;

/**
 * Service principal de la perspective Report
 * * <table  border="1"  cellpadding="3" cellspacing="0"  summary="lookups">
 * <tr>
 * <th align="left">Lookup</th>
 * <th align="left">Commentaire</th>
 * <th align="left">Méthode utilisant le lookup</th>
 * </tr>
 * <tr>
 * <td>{@link ModeleResultatTimeKeyContent}</td>
 * <td>	les pas de temps du modele</td>
 * <td><code>{@link #getTimeContent()} </code></td>
 * </tr>
 * <tr>
 * <td>{@link TimeFormatOption}</td>
 * <td>Les options de formatage.</td>
 * <td><code>{@link #getTimeFormat()} </code></td>
 * </tr>
 * <tr>
 * <td>{@link EMHModeleBase}</td>
 * <td>le modèle chargé par defaut</td>
 * <td><code>{@link #getModele()}</code></td>
 * </tr>
 * <tr>
 * <td>{@link ReportRunContent}</td>
 * <td>le run chargé</td>
 * <td><code>{@link #getRunCourant()} </code></td>
 * </tr>
 * </table>
 *
 * @author Frederic Deniger
 */
@ServiceProvider(service = ReportService.class)
public final class ReportService implements Lookup.Provider {
    private final PostRunService postRunService = Lookup.getDefault().lookup(PostRunService.class);
    private final Result<EMHScenario> resultat;
    private final InstanceContent dynamicContent = new InstanceContent();
    private final Lookup lookup = new AbstractLookup(dynamicContent);
    private final ReportRunKey currentRunKey = new ReportRunKey();
    private ReportTimeFormatter reportTimeFormatter;

    public ReportService() {
        resultat = postRunService.getLookup().lookupResult(EMHScenario.class);
        resultat.addLookupListener(ev -> currentRunChanged());
        currentRunChanged();

        //pour ecouter les modifications ou annulation de modifications dans le fichier LHPT.
        LhptService aocLhptService = Lookup.getDefault().lookup(LhptService.class);
        aocLhptService.addModifiedStateListener(evt -> new ReportSaver().updatePerspectiveState());
    }

    /**
     * @return la clé du run chargé
     */
    public ReportRunKey getCurrentRunKey() {
        return currentRunKey;
    }

    /**
     * @return le formateur des pas de temps
     */
    public ReportTimeFormatter getReportTimeFormatter() {
        return reportTimeFormatter;
    }

    /**
     * Le run courant est modifié: on recharge l'ensemble
     */
    private void currentRunChanged() {
        unloadCurrentRun();
        if (postRunService.isRunLoaded()) {
            ReportRunContent run = new ReportRunContent(postRunService.getRunLoaded(), postRunService.getManagerScenarioLoaded(), getCurrentRunKey());
            run.setScenario(postRunService.getScenarioLoaded());
            getCurrentRunKey().updateNom(run.getManagerScenario(), postRunService.getRunLoaded());
            final EMHModeleBase defaultModele = run.getScenario().getModeles().get(0);
            final ModeleResultatTimeKeyContent timeContent = ModeleResultatTimeKeyContent.extract(defaultModele);
            dynamicContent.add(timeContent);
            reportTimeFormatter = new ReportTimeFormatter();
            dynamicContent.add(reportTimeFormatter.getOption());
            reportTimeFormatter.setService(this, run.getScenario());
            dynamicContent.add(defaultModele);
            dynamicContent.add(run);
        }
    }

    /**
     * @return true si un run est chargé
     */
    public boolean isRunLoaded() {
        return getRunCourant() != null;
    }

    /**
     * @return le projet ouvert
     */
    public EMHProjet getSelectedProjet() {
        return postRunService.getSelectedProjet();
    }

    /**
     * @return le CCM de l'étude en courrs
     */
    public CrueConfigMetier getCcm() {
        return postRunService.getCcm();
    }

    /**
     * Met à jour tous les lookup du service
     */
    public void unloadCurrentRun() {
        ReportRunContent run = getRunCourant();
        if (run != null) {
            postRunService.unloadRun();
            dynamicContent.remove(run);
            ModeleResultatTimeKeyContent timeContent = getTimeContent();
            if (timeContent != null) {
                dynamicContent.remove(timeContent);
            }
            ResultatTimeKey selectedTime = getSelectedTime();
            if (selectedTime != null) {
                dynamicContent.remove(selectedTime);
            }
            RangeSelectedTimeKey rangeSelectedTimeKey = getRangeSelectedTimeKey();
            if (rangeSelectedTimeKey != null) {
                dynamicContent.remove(rangeSelectedTimeKey);
            }
            TimeFormatOption timeFormat = getTimeFormat();
            if (timeFormat != null) {
                dynamicContent.remove(timeFormat);
            }
            EMHModeleBase modele = getModele();
            if (modele != null) {
                dynamicContent.remove(modele);
            }
        }
    }

    /**
     * @return le run courant chargés issu du lookup {@link #getLookup()}
     */
    public ReportRunContent getRunCourant() {
        return lookup.lookup(ReportRunContent.class);
    }

    /**
     * @return l'ensemble des pas de temps issu du lookup {@link #getLookup()}
     */
    public ModeleResultatTimeKeyContent getTimeContent() {
        return lookup.lookup(ModeleResultatTimeKeyContent.class);
    }

    /**
     * @return le modele des resultats chargés issu du lookup {@link #getLookup()}
     */
    public EMHModeleBase getModele() {
        return lookup.lookup(EMHModeleBase.class);
    }

    /**
     * @return le TimeFormat issu du lookup {@link #getLookup()}
     */
    public TimeFormatOption getTimeFormat() {
        return lookup.lookup(TimeFormatOption.class);
    }

    /**
     * Met à jour le {@link #getLookup()} du service
     *
     * @param timeFormat option de formattage ( durée, temps date)
     */
    public void setTimeFormat(TimeFormatOption timeFormat) {
        TimeFormatOption selectedTime = getTimeFormat();
        reportTimeFormatter.setOption(timeFormat);
        if (selectedTime != null) {
            dynamicContent.remove(selectedTime);
        }
        dynamicContent.add(timeFormat);
    }

    /**
     * @return le pas de temps sélectionné
     */
    public ResultatTimeKey getSelectedTime() {
        return lookup.lookup(ResultatTimeKey.class);
    }

    /**
     * @param resultatTimeKey le pas de temps sélectionné
     */
    public void setSelectedTime(ResultatTimeKey resultatTimeKey) {
        ResultatTimeKey selectedTime = getSelectedTime();
        if (selectedTime != null) {
            dynamicContent.remove(selectedTime);
        }
        if (resultatTimeKey != null) {
            dynamicContent.add(resultatTimeKey);
        }
    }

    /**
     * @return le range sélectionné dans la perspective
     */
    public RangeSelectedTimeKey getRangeSelectedTimeKey() {
        return lookup.lookup(RangeSelectedTimeKey.class);
    }

    /**
     * @param selectedTimeKey les pas de temps sélectionnés dans la perspective
     */
    public void setRangeSelectedTimeKey(RangeSelectedTimeKey selectedTimeKey) {
        RangeSelectedTimeKey selectedTime = getRangeSelectedTimeKey();
        if (selectedTime != null) {
            dynamicContent.remove(selectedTime);
        }
        if (selectedTimeKey != null) {
            dynamicContent.add(selectedTimeKey);
        }
    }

    /**
     * @return les pas de temps sélectionnés dans la perspective
     */
    public List<ResultatTimeKey> getRangeSelectedTimeKeys() {
        RangeSelectedTimeKey rangeSelectedTimeKey = getRangeSelectedTimeKey();
        if (rangeSelectedTimeKey == null) {
            return Collections.emptyList();
        }
        return rangeSelectedTimeKey.getKeys();
    }

    @Override
    public Lookup getLookup() {
        return lookup;
    }

    /**
     * @return des temps négatifs pour les calcul permanents
     */
    public List<Pair<ResultatTimeKey, Long>> getTimeByKeyList() {
        return getTimeContent().getTimeByKeyList(getRangeSelectedTimeKeys());
    }
}
