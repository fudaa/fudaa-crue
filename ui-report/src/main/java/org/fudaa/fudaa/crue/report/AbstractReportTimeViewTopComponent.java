/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report;

import com.memoire.bu.BuGridLayout;
import org.fudaa.ctulu.gui.ExportTableCommentSupplier;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.projet.report.ReportViewLineInfo;
import org.fudaa.dodico.crue.projet.report.data.ExternFileKey;
import org.fudaa.dodico.crue.projet.report.data.ReportKeyContract;
import org.fudaa.dodico.crue.projet.report.data.ReportRunContent;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.persist.AbstractReportCourbeConfig;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.courbe.*;
import org.fudaa.fudaa.crue.common.action.ExportImageAction;
import org.fudaa.fudaa.crue.common.action.ExportImageToClipboardAction;
import org.fudaa.fudaa.crue.loi.ViewCourbeManager;
import org.fudaa.fudaa.crue.loi.common.CourbeModelWithKey;
import org.fudaa.fudaa.crue.loi.common.CourbesUiController;
import org.fudaa.fudaa.crue.report.actions.SaveTemplateAction;
import org.fudaa.fudaa.crue.report.config.AbstractReportGrapheBuilder;
import org.fudaa.fudaa.crue.report.config.CourbeViewReportConfigurationButtonUI;
import org.fudaa.fudaa.crue.report.extern.ExternFileChooser;
import org.fudaa.fudaa.crue.report.service.ReportRunAlternatifService;
import org.fudaa.fudaa.crue.study.services.TableExportCommentSupplier;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;

/**
 * @author Frederic Deniger
 */
public abstract class AbstractReportTimeViewTopComponent<C extends AbstractReportCourbeConfig, B extends AbstractReportGrapheBuilder<? extends CourbesUiController, C>>
    extends AbstractReportTimeTopComponent implements ReportTopComponentConfigurable<C>, CtuluImageProducer, ExportTableCommentSupplier {
  protected transient C content;
  protected transient final ViewCourbeManager loiLegendManager;
  protected transient final CourbesUiController profilUiController;
  final transient CourbeViewReportConfigurationButtonUI configManager;
  protected transient final B builder;
  protected transient final ReportRunAlternatifService reportRunAlternatifService = Lookup.getDefault().lookup(ReportRunAlternatifService.class);
  private ReportViewLineInfo lineInfo;

  protected AbstractReportTimeViewTopComponent() {
    putClientProperty(PROP_SLIDING_DISABLED, Boolean.FALSE);
    putClientProperty(PROP_MAXIMIZATION_DISABLED, Boolean.FALSE);
    putClientProperty(PROP_DRAGGING_DISABLED, Boolean.FALSE);
    putClientProperty(PROP_UNDOCKING_DISABLED, Boolean.FALSE);

    profilUiController = createCourbesUiController();
    profilUiController.removeEditActions();
    final EGFillePanel courbePanel = profilUiController.getPanel();
    profilUiController.getGraphe().setExportTableCommentSupplier(this);
    courbePanel.setPreferredSize(new Dimension(550, 350));
    loiLegendManager = new ViewCourbeManager(courbePanel);
    loiLegendManager.getConfig().setDisplayLabels(true);
    configManager = new CourbeViewReportConfigurationButtonUI(new ReportConfigurationManagerTarget(this));
    configManager.installDoubleClickListener(loiLegendManager);

    buildComponents();
    initActions();
    installGraphListener();
    builder = createBuilder();
  }

  @Override
  public List<String> getComments() {
    return new TableExportCommentSupplier(true).getComments(getReportConfig().getType().geti18n() + " (" + getName() + ")");
  }

  @Override
  public void rangeChanged() {
    if (reportService.getRangeSelectedTimeKey() != null) {//pour ne pas modifier en cas de mise à jour
      setReportConfig(getReportConfig(), false);
    }
  }

  @Override
  public boolean isClosable() {
    return true;
  }

  @Override
  public void clearReportModifications() {
  }

  @Override
  public BufferedImage produceImage(final Map params) {
    return profilUiController.produceImage(params);
  }

  @Override
  public BufferedImage produceImage(final int w, final int h, final Map params) {
    return profilUiController.produceImage(w, h, params);
  }

  @Override
  public Dimension getDefaultImageDimension() {
    return profilUiController.getDefaultImageDimension();
  }

  @Override
  public void setReportInfo(final ReportViewLineInfo info) {
    this.lineInfo = info;
  }

  private void updateAspectFor(final EGObject _c) {
    if (updating) {
      return;
    }
    if (_c instanceof EGCourbe) {
      final CourbeModelWithKey model = (CourbeModelWithKey) ((EGCourbe) _c).getModel();
      final ReportKeyContract key = (ReportKeyContract) model.getKey();
      final EGCourbePersist persitUiConfig = ((EGCourbe) _c).getPersitUiConfig();
      if (key.isExtern()) {
        content.saveExternKeyConfig((ExternFileKey) key, persitUiConfig);
      } else {
        saveCourbeConfig(persitUiConfig, key);
      }
    }
    setModified(true);
  }

  protected abstract void saveCourbeConfig(EGCourbePersist persitUiConfig, ReportKeyContract key);

  public EGAxeVerticalPersist createAxeVerticalConfig(final String variableName) {
    final PropertyNature property = builder.getReportResultProviderService().getPropertyNature(variableName);
    EGAxeVerticalPersist axeV1 = content.getAxeVConfig(property);
    if (axeV1 == null) {
      final EGAxeVertical axeV = builder.getOrCreateAxeVertical(property);
      axeV1 = new EGAxeVerticalPersist(axeV);
      content.saveAxeVConfig(property, axeV1);
    }
    return axeV1;
  }

  protected abstract B createBuilder();

  protected abstract void buildComponents();

  /**
   * @return les actions complémentaires à ajouter dans le bouton de configuration.
   */
  protected List<? extends Action> getAdditionalActionsToAddConfigureButton() {
    return getCourbesUiController().getEditActions();
  }

  public final void applyTitleConfigChanged() {
    builder.applyTitleConfigChanged(content);
    setModified(true);
  }

  @Override
  public void setEditable(final boolean b) {
    super.setEditable(b);
  }

  @Override
  public void timeFormatChanged() {
    builder.updateLabelsAndTitleContent(content);
  }

  @Override
  public final void selectedTimeChanged() {
    builder.updateResultCurvesAfterTimeChanged(content);
    builder.updateLabelsAndTitleContent(content);
  }

  /**
   * @param fromTemplate si true un restore sera effectué car les templates peuvent provenir d'autre graphe avec des zoom bien différents.
   */
  protected final void applyChangeInContent(final boolean fromTemplate) {
    builder.applyChange(content, fromTemplate);
  }

  public void applyLabelsConfigChanged() {
    builder.applyLabelsConfigChanged(getReportConfig());
    setModified(true);
  }

  public void installGraphListener() {
    profilUiController.getEGGrapheSimpleModel().addModelListener(new EGGrapheModelListener() {
      @Override
      public void structureChanged() {
      }

      @Override
      public void courbeContentChanged(final EGObject _c, final boolean _mustRestore) {
      }

      @Override
      public void courbeAspectChanged(final EGObject _c, final boolean _visibil) {
        updateAspectFor(_c);
      }

      @Override
      public void axeContentChanged(final EGAxe _c) {
        if (EventQueue.isDispatchThread()) {
          updateContentForAxe(_c);
        }
      }

      @Override
      public void axeAspectChanged(final EGAxe _c) {
        updateAspectForAxe(_c);
      }
    });
  }

  public abstract List<EbliActionInterface> getMainActions();

  public void addButton(final JPanel toolbar, final AbstractButton bt) {
    toolbar.add(bt);
  }

  public void initActions() throws MissingResourceException {
    final List<EbliActionInterface> mainActions = getMainActions();
    final JPanel courbeToolBar = new JPanel(new FlowLayout(FlowLayout.LEFT));
    addButton(courbeToolBar, configManager.getButton());
    for (final EbliActionInterface action : mainActions) {
      addButton(courbeToolBar, action.buildButton(EbliComponentFactory.INSTANCE));
    }
    courbeToolBar.add(new ExportImageAction(loiLegendManager).buildToolButton(EbliComponentFactory.INSTANCE));
    courbeToolBar.add(new ExportImageToClipboardAction(loiLegendManager).buildToolButton(EbliComponentFactory.INSTANCE));
    final SaveTemplateAction saveTemplate = new SaveTemplateAction(this);
    courbeToolBar.add(saveTemplate.buildButton(EbliComponentFactory.INSTANCE));
    final JPanel allToolBar = new JPanel(new BuGridLayout(1, 0, 0, true, false));
    allToolBar.add(courbeToolBar);
    allToolBar.add(profilUiController.getToolbar());
    add(allToolBar, BorderLayout.NORTH);

    super.editActions.addAll(mainActions);
    super.editActions.add(saveTemplate);
    super.editComponent.add(configManager.getButton());
    for (final EbliActionInterface action : profilUiController.getEditActions()) {
      super.editActions.add(action);
    }
  }

  private void updateContentForAxe(final EGAxe _c) {
    if (updating || !isOpened()) {
      return;
    }
    if (_c != null) {
      updateAspectForAxe(_c);
      return;
    }
    content.saveAxeH(new EGAxeHorizontalPersist(profilUiController.getAxeX()));
    final List<EGAxeVertical> axesY = profilUiController.getAxesY();
    for (final EGAxeVertical axe : axesY) {
      content.saveAxeVConfig((String) axe.getKey(), new EGAxeVerticalPersist(axe));
    }
    if (editable) {
      setModified(true);
    }
  }

  private void updateAspectForAxe(final EGAxe egAxe) {
    if (updating || egAxe == null) {
      return;
    }
    if (egAxe.isVertical()) {
      final String key = (String) egAxe.getKey();
      if (key != null) {
        content.saveAxeVConfig(key, new EGAxeVerticalPersist((EGAxeVertical) egAxe));
      }
    } else {
      content.saveAxeH(new EGAxeHorizontalPersist((EGAxeHorizontal) egAxe));
    }
    if (editable) {
      setModified(true);
    }
  }

  public ViewCourbeManager getLoiLegendManager() {
    return loiLegendManager;
  }

  protected CourbesUiController getCourbesUiController() {
    return profilUiController;
  }

  public abstract List<ReportVariableKey> getTitleAvailableVariables();

  @Override
  public C getReportConfig() {
    return content;
  }

  @Override
  public AbstractReportTopComponent getTopComponent() {
    return this;
  }

  protected abstract CourbesUiController createCourbesUiController();

  public EMHScenario getScenario() {
    return reportService.getRunCourant().getScenario();
  }

  private Result<ReportRunContent> alternatifResult;
  private final LookupListener alternatifListener = ev -> alternatifRunChanged();

  public abstract void alternatifRunChanged();

  @Override
  protected void runLoadedHandler() {
    if (content != null) {
      super.updating = true;
      applyChangeInContent(false);
      super.updating = false;
    }
    alternatifResult = reportRunAlternatifService.getLookup().lookupResult(ReportRunContent.class);
    alternatifResult.addLookupListener(alternatifListener);
  }

  @Override
  protected final void runUnloadedHandler() {
    super.runUnloadedHandler();
    if (builder != null) {
      builder.runUnloaded();
    }
    if (alternatifResult != null) {
      alternatifResult.removeLookupListener(alternatifListener);
      alternatifResult = null;
    }
  }

  private void showConfigExternUI() {
    final ExternFileChooser chooser = new ExternFileChooser(content);
    if (chooser.displayChooseUI()) {
      builder.synchronizeExternFiles(content);
      setModified(true);
    }
  }

  protected EbliActionInterface createConfigExternAction() {
    return new EbliActionSimple(NbBundle.getMessage(AbstractReportTimeViewTopComponent.class, "ExternFile.ActioName"), null, "EXTERN_FILES") {
      @Override
      public void actionPerformed(final ActionEvent ae) {
        showConfigExternUI();
      }
    };
  }

  @Override
  public void updateFrameName() {
    setName(lineInfo.getNom());
    updateTopComponentName();
  }

  protected void propagateChange() {
    applyChangeInContent(false);
    setModified(true);
  }

  @Override
  public void setReportConfig(final C newConfig, final boolean fromTemplate) {
    this.content = newConfig;
    this.loiLegendManager.setConfig(this.content.getLoiLegendConfig());
    applyChangeInContent(fromTemplate);
  }
}
