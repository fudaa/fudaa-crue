/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.extern;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.fudaa.crue.common.view.CustomNodePopupFactory;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.common.view.DefaultOutlineViewEditor;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ExternFileChooserUI extends DefaultOutlineViewEditor {

  @Override
  protected void addElement() {
    final ExternFileContent elem = new ExternFileContent();
    final Children children = getExplorerManager().getRootContext().getChildren();
    final ExternFileContentNode elemPdtNode = new ExternFileContentNode(elem);
    children.add(new Node[]{elemPdtNode});
    DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
  }

  @Override
  public OutlineView getView() {
    return view;
  }

  @Override
  protected void createOutlineView() {
    super.view = new OutlineView("");
    view.getOutline().setColumnHidingAllowed(false);
    view.getOutline().setRootVisible(false);
    final List<String> columns = new ArrayList<>();
    columns.add(ExternFileContent.PROP_NAME);
    columns.add(NbBundle.getMessage(ExternFileContent.class, "fileExtern.property"));
    view.setPropertyColumns(columns.toArray(new String[0]));
    view.setNodePopupFactory(new CustomNodePopupFactory());
  }
}
