/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view;

import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ReportReloadViewConfigNodeAction extends AbstractEditNodeAction {

  final ReportViewsService reportViewsService = Lookup.getDefault().lookup(ReportViewsService.class);
  ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

  public ReportReloadViewConfigNodeAction() {
    super(org.openide.util.NbBundle.getMessage(ReportReloadViewConfigNodeAction.class, "ReloadViewConfig.ActionName"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    for (Node node : activatedNodes) {
      ReportViewLine line = node.getLookup().lookup(ReportViewLine.class);
      if (line != null && line.getLineInformation().getFilename() != null && line.isModified()) {
        return true;
      }
    }
    return false;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    for (Node node : activatedNodes) {
      if (node instanceof ReportViewNode) {
        reportViewsService.reloadNodeFromFile((ReportViewNode) node);
      }
    }

  }
}
