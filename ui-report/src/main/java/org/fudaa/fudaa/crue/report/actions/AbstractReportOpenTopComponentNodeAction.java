package org.fudaa.fudaa.crue.report.actions;

import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.openide.nodes.Node;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 * @author deniger
 */
public abstract class AbstractReportOpenTopComponentNodeAction extends AbstractEditNodeAction {
  private final String mode;

  /**
   * @param name nom de l'action
   * @param mode attention ce mode doit être déclaré dans le projet ui-branding
   */
  public AbstractReportOpenTopComponentNodeAction(final String name, final String mode) {
    super(name);
    this.mode = mode;
  }

  private static void activeInMode(final TopComponent openNew, final String mode) {
    final Mode findMode = WindowManager.getDefault().findMode(mode);
    if (findMode != null) {
      findMode.dockInto(openNew);
    }
    openNew.open();
    openNew.requestActive();
  }


  protected String getMode(final TopComponent top) {
    return mode;
  }

  protected TopComponent createNewTopComponentFor(final Node[] activatedNodes) {
    return createNewTopComponentFor(activatedNodes[0]);
  }

  protected <T extends TopComponent> T getMainTopComponentAvailable() {
    return null;
  }

  private void openNewTopComponent(final Node[] activatedNodes) {
    final TopComponent openNew = createNewTopComponentFor(activatedNodes);
    if (openNew != null) {
      activeInMode(openNew, getMode(openNew));
    }
    postOpenNewTopComponent(openNew);
  }

  protected abstract TopComponent createNewTopComponentFor(Node selectedNode);

  @Override
  protected void performAction(final Node[] activatedNodes) {
    openNewTopComponent(activatedNodes);
  }

  /**
   * méthode appelée après l'ouverture du TopComponent
   *
   * @param openNew le {@link TopComponent} ouvert
   */
  protected void postOpenNewTopComponent(final TopComponent openNew) {
  }

  protected <T extends TopComponent> T getMainTopComponentAvailable(final String id) {
    final T initTc = (T) WindowManager.getDefault().findTopComponent(id);
    if (initTc.isOpened()) {
      return null;
    }
    return initTc;
  }
}
