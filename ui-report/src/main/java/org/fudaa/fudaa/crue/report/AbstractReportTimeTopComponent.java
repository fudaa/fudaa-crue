/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report;

import org.fudaa.fudaa.crue.report.service.ReportTimeListener;
import org.fudaa.fudaa.crue.report.service.ReportTimeListenerManager;
import org.openide.util.LookupListener;

/**
 *
 * @author Frederic Deniger
 */
public abstract class AbstractReportTimeTopComponent
        extends AbstractReportTopComponent implements LookupListener, ReportTimeListener {

  ReportTimeListenerManager manager;

  /**
   * Ne doit être appele que pour les tests !
   */
  @Override
  public final void runLoaded() {
    manager = new ReportTimeListenerManager();
    manager.addListener(this);
    runLoadedHandler();
  }

  protected void runLoadedHandler() {
  }

  protected void runUnloadedHandler() {
  }

  @Override
  public final void runUnloaded() {

    if (manager != null) {
      manager.removeAllListeners();
    }
    runUnloadedHandler();
  }
}
