/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.config;

import com.memoire.bu.BuGridLayout;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.ebli.controle.BSelecteurColorChooserBt;
import org.fudaa.ebli.controle.BSelecteurFont;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.ComboBoxHelper;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.report.data.ReportVariableKeyCellRenderer;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLabelContentConfigUI {

  final List<ReportVariableKey> availableVar;

  public ReportLabelContentConfigUI(List<ReportVariableKey> initAvailableVar) {
    availableVar = new ArrayList<>();
    availableVar.add(null);
    availableVar.addAll(initAvailableVar);
  }

  public boolean configure(ReportLabelContentConfig config) {
    ReportLabelContent in = config.getContent();
    if(in==null){
      in=new ReportLabelContent();
    }
    JPanel pn = new JPanel(new BuGridLayout(2, 5, 5));
    pn.add(new JLabel(NbBundle.getMessage(ReportLabelContentConfigUI.class, "LabelContent.Text")));
    JTextField tf = new JTextField(50);
    tf.setText(in.getPrefix());
    pn.add(tf);
    pn.add(new JLabel(NbBundle.getMessage(ReportLabelContentConfigUI.class, "VariableContent.Text")));
    JComboBox cb = new JComboBox();
    ComboBoxHelper.setDefaultModel(cb, availableVar);
    cb.setRenderer(new ReportVariableKeyCellRenderer());
    //le cell renderer:
    cb.setSelectedItem(in.getContent());
    pn.add(cb);
    pn.add(new JLabel(NbBundle.getMessage(ReportLabelContentConfigUI.class, "UnitContent.Text")));
    JCheckBox cbUnit = new JCheckBox();
    cbUnit.setSelected(in.isUnit());
    pn.add(cbUnit);
    pn.add(new JLabel(NbBundle.getMessage(ReportLabelContentConfigUI.class, "FontContent.Text")));
    BSelecteurFont fontSelecteur = new BSelecteurFont("FONT");
    DefaultSelecteurTarget<Font> fontTarget = new DefaultSelecteurTarget<>(config.getConfig().getFont());
    fontSelecteur.setSelecteurTarget(fontTarget);
    pn.add(fontSelecteur.getButton());

    pn.add(new JLabel(NbBundle.getMessage(ReportLabelContentConfigUI.class, "ColorContent.Text")));
    DefaultSelecteurTarget<Color> colorTarget = new DefaultSelecteurTarget<>(config.getConfig().getFgColor());
    BSelecteurColorChooserBt selecteurCouleur = new BSelecteurColorChooserBt("COLOR");
    selecteurCouleur.setSelecteurTarget(colorTarget);
    pn.add(selecteurCouleur.getPanel());    
    SysdocUrlBuilder.installHelpShortcut(pn, SysdocUrlBuilder.getDialogHelpCtxId("configurationTitre", PerspectiveEnum.REPORT));
    boolean ok = DialogHelper.showQuestionOkCancel(NbBundle.getMessage(ReportLabelContentConfigUI.class, "Configuration.DialogTitle"), pn);
    if (ok) {
      config.getConfig().setFgColor(colorTarget.getObject());
      config.getConfig().setFont(fontTarget.getObject());
      if(config.getContent()==null){
        config.setContent(new ReportLabelContent());
      }
      config.getContent().setContent((ReportVariableKey) cb.getSelectedItem());
      config.getContent().setPrefix(tf.getText());
      config.getContent().setUnit(cbUnit.isSelected());
    }
    return ok;
  }
}
