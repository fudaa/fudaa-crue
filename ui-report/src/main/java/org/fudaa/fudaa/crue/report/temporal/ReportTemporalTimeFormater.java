/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.temporal;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ReportTemporalTimeFormater implements CtuluNumberFormatI {

  final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);

  @Override
  public String format(double _d) {
    if (!reportService.isRunLoaded()) {
      return StringUtils.EMPTY;
    }
    long timeInMillis = (long) _d;
    if (timeInMillis > 0) {
      double sec = _d / 1000d;
      timeInMillis = ((long) sec) * 1000l;
      ToStringTransformer millisTempsSceToStringTransformer = reportService.getReportTimeFormatter().getMillisTempsSceToStringTransformer();
      return millisTempsSceToStringTransformer.transform(timeInMillis);
    }
    ResultatTimeKey tempsScePermanentTransitoire = reportService.getTimeContent().getTempsScePermanentTransitoire(timeInMillis);
    return reportService.getReportTimeFormatter().getResulatKeyTempsSimuToStringTransformer().transform(tempsScePermanentTransitoire);
  }

  @Override
  public boolean isDecimal() {
    return false;
  }

  @Override
  public String toLocalizedPattern() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public CtuluNumberFormatI getCopy() {
    return this;
  }
}
