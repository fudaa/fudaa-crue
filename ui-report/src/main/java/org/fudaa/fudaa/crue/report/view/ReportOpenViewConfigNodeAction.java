/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view;

import javax.swing.JMenuItem;
import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.report.AbstractReportTimeViewTopComponent;
import org.fudaa.fudaa.crue.report.AbstractReportTopComponent;
import org.fudaa.fudaa.crue.report.ReportTopComponentConfigurable;
import org.fudaa.fudaa.crue.report.actions.AbstractReportOpenTopComponentNodeAction;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 *
 * @author Frederic Deniger
 */
public class ReportOpenViewConfigNodeAction extends AbstractReportOpenTopComponentNodeAction {

  ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
  final ReportViewsService reportViewsService = Lookup.getDefault().lookup(ReportViewsService.class);

  private ReportOpenViewConfigNodeAction() {
    super(NbBundle.getMessage(ReportOpenViewConfigNodeAction.class, "Open.ActionName"), null);
  }

  @Override
  protected String getMode(final TopComponent top) {
    return (String) top.getClientProperty("OPENED_MODE");
  }

  @Override
  protected boolean preEnable(final Node[] activatedNodes) {
    final boolean ok = ArrayUtils.isNotEmpty(activatedNodes);
    if (ok) {
      for (final Node node : activatedNodes) {
        final ReportViewLine line = node.getLookup().lookup(ReportViewLine.class);
        if (line != null) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  protected TopComponent createNewTopComponentFor(final Node selectedNode) {
    final ReportViewLine line = selectedNode.getLookup().lookup(ReportViewLine.class);
    if (line == null) {
      return null;
    }
    assert selectedNode.getClass().equals(ReportViewNode.class);
    if (line.getReportConfig() == null) {
      final ReportContentType type = selectedNode.getParentNode().getLookup().lookup(ReportContentType.class);
      final ReportConfigContrat load = reportViewsService.load(line, type);
      if (load == null) {
        return null;
      }
      line.setReportConfig(load);
    }
    if (line.getTopComponent() != null) {
      final AbstractReportTopComponent topComponent = line.getTopComponent().getTopComponent();
      topComponent.setName(line.getLineInformation().getNom());
      line.getTopComponent().setReportInfo(line.getLineInformation());
      if (line.getContentType().isPlanimetry()) {
        ReportViewsService.installNode(line.getTopComponent(), selectedNode);
        line.getTopComponent().setReportConfig(line.getReportConfig(), false);
      }
      topComponent.requestVisible();

    } else {
      final Class<ReportTopComponentConfigurable> cl = line.getTopComponentClass();
      try {
        final TopComponent mainTopComponentAvailable = super.getMainTopComponentAvailable(line.getTopComponentId());
        ReportTopComponentConfigurable tc = (ReportTopComponentConfigurable) mainTopComponentAvailable;
        if (tc == null) {
          tc = cl.newInstance();
        }
        tc.setReportConfig(line.getReportConfig(), false);
        ReportViewsService.installNode(tc, selectedNode);
        final AbstractReportTopComponent topComponent = tc.getTopComponent();
        topComponent.putClientProperty("OPENED_MODE", line.getTopComponentMode());
        topComponent.setName(line.getLineInformation().getNom());
        tc.setReportInfo(line.getLineInformation());
        topComponent.setModified(line.modified);
        if (topComponent instanceof AbstractReportTimeViewTopComponent) {
          final boolean wasModified = topComponent.isModified();
          ((AbstractReportTimeViewTopComponent) topComponent).alternatifRunChanged();
          if (!wasModified && topComponent.isModified()) {
            DialogHelper.showInfo(line.getLineInformation().getNom(), NbBundle.getMessage(ReportOpenViewConfigNodeAction.class, "OpenVue.RunAlternatifMissing"));
          }
        }
        return topComponent;
      } catch (final Exception exception) {
        Exceptions.printStackTrace(exception);
      }

    }

    return null;
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    if (activatedNodes != null && activatedNodes.length == 1) {
      return activatedNodes[0].getLookup().lookup(ReportViewLine.class) != null;
    }
    return false;
  }

  @Override
  public JMenuItem getPopupPresenter() {
    return NodeHelper.useBoldFont(super.getPopupPresenter());
  }
}
