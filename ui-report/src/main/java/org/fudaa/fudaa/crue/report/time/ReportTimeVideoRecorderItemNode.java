/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.time;

import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import org.fudaa.ebli.animation.EbliAnimationOutputInterface;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertySupportReadWrite;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.lookup.Lookups;

/**
 * Node pour éditer la config video.
 *
 * @author Frederic Deniger
 */
public class ReportTimeVideoRecorderItemNode extends AbstractNodeFirable {

  public static final String PROP_VIDEO_CONFIG = "videoConfig";
  public static final String PROP_VIDEO_CONFIG_ACTIVE = "videoConfigActive";

  public ReportTimeVideoRecorderItemNode(ReportTimeVideoRecorderItem item) {
    super(Children.LEAF, Lookups.singleton(item));
    setName(item.getTopComponent().getName());
  }

  @Override
  public boolean isEditMode() {
    return true;
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
    ReportTimeVideoRecorderItem lookup = getLookup().lookup(ReportTimeVideoRecorderItem.class);
    Set set = Sheet.createPropertiesSet();
    sheet.put(set);
    PropertySupportReadWrite<ReportTimeVideoRecorderItem, EbliAnimationOutputInterface> propertyItem =
            new PropertySupportReadWrite<ReportTimeVideoRecorderItem, EbliAnimationOutputInterface>(this, lookup, EbliAnimationOutputInterface.class, PROP_VIDEO_CONFIG, getVideoConfigi18n()) {
              @Override
              protected void setValueInInstance(EbliAnimationOutputInterface newVal) {
                getInstance().setOutput(newVal);
              }

              @Override
              public EbliAnimationOutputInterface getValue() throws IllegalAccessException, InvocationTargetException {
                return getInstance().getOutput();
              }

              @Override
              public PropertyEditor getPropertyEditor() {
                return new ReportTimeVideoRecorderItemEditor();
              }
            };

    PropertySupportReadWrite<ReportTimeVideoRecorderItem, Boolean> activeItem = new PropertySupportReadWrite<ReportTimeVideoRecorderItem, Boolean>(this, lookup, Boolean.TYPE, PROP_VIDEO_CONFIG_ACTIVE, getPropVideoConfigActivedi18n()) {
      @Override
      protected void setValueInInstance(Boolean newVal) {
        getInstance().setActivated(newVal);
      }

      @Override
      public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
        return getInstance().isActivated();
      }
    };
    set.put(propertyItem);
    set.put(activeItem);
    return sheet;
  }

  public static String getPropVideoConfigActivedi18n() {
    return org.openide.util.NbBundle.getMessage(ReportTimeVideoRecorderItemNode.class, "VideoConfigActive.Property");
  }

  public static String getVideoConfigi18n() {
    return org.openide.util.NbBundle.getMessage(ReportTimeVideoRecorderItemNode.class, "VideoConfig.Property");
  }
}
