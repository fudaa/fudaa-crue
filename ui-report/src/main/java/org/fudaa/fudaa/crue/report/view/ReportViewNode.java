/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view;

import java.io.IOException;
import javax.swing.Action;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.openide.nodes.Children;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frederic Deniger
 */
public final class ReportViewNode extends AbstractNodeFirable implements Comparable<ReportViewNode>, ObjetNomme {
  
  public static final String MODIFIED_SUFFIX = " *";
  final PerspectiveServiceReport perspectiveServiceReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);
  
  public ReportViewNode(ReportViewLine view) {
    super(Children.LEAF, Lookups.singleton(view));
    setName(view.getLineInformation().getNom());
    saveOldName();
    view.setModified(false);
    updateDisplayName();
  }
  
  protected final void saveOldName() {
    setValue("OLD_NAME", getName());
  }
  
  public String getOldName() {
    return (String) getValue("OLD_NAME");
  }
  
  @Override
  public SystemAction getDefaultAction() {
    return SystemAction.get(ReportOpenViewConfigNodeAction.class);
  }
  
  @Override
  public Action[] getActions(boolean context) {
    return new Action[]{
              SystemAction.get(ReportOpenViewConfigNodeAction.class),
              SystemAction.get(ReportCopyViewConfigNodeAction.class),
              SystemAction.get(ReportCloseViewConfigNodeAction.class),
              null,
              SystemAction.get(ReportReloadViewConfigNodeAction.class),
              null,
              SystemAction.get(ReportRenameViewConfigNodeAction.class),
              SystemAction.get(ReportDeleteViewConfigNodeAction.class)
            };
  }
  
  @Override
  public String getNom() {
    return getDisplayName();
  }
  
  @Override
  public String getId() {
    return getDisplayName();
  }
  
  @Override
  public void setNom(String newNom) {
    setName(newNom);
  }
  
  @Override
  public int compareTo(ReportViewNode o) {
    if (o == this) {
      return 0;
    }
    if (o == null) {
      return 1;
    }
    return getDisplayName().compareTo(o.getDisplayName());
  }
  
  @Override
  public boolean isEditMode() {
    return perspectiveServiceReport.isInEditMode();
  }
  
  @Override
  public void setDisplayName(String s) {
    super.setDisplayName(s);
    
  }
  
  @Override
  public void setName(String s) {
    super.setName(s);
    ReportViewLine view = getLine();
    view.setModified(true);
    view.getLineInformation().setNom(s);
    if (view.getTopComponent() != null) {
      view.getTopComponent().updateFrameName();
    }
    updateDisplayName();
  }
  
  @Override
  public boolean canDestroy() {
    return isEditMode();
  }
  
  @Override
  public String getHtmlDisplayName() {
    String name = getDisplayName();
    ReportViewLine lookup = getLine();
    if (lookup.isModified()) {
      name = name + MODIFIED_SUFFIX;
    }
    boolean opened = lookup.getTopComponent() != null && ReportViewsService.getViewNode(lookup.getTopComponent().getTopComponent()) == this;
    if (opened) {
      name = "<b>" + name + "</b>";
    }
    return name;
  }
  
  @Override
  public void destroy() throws IOException {
    super.destroy();
    ReportViewLine view = getLine();
    if (view.getTopComponent() != null) {
      if (view.getTopComponent().isClosable()) {
        view.getTopComponent().getTopComponent().close();
      } else {
        ReportViewsService.uninstallNode(view.getTopComponent(), this);
        view.getTopComponent().clearReportModifications();
      }
    }
  }
  
  protected void updateDisplayName() {
    fireDisplayNameChange(null, getDisplayName());
  }
  
  @Override
  public boolean canRename() {
    return false;
  }
  
  public ReportViewLine getLine() {
    return getLookup().lookup(ReportViewLine.class);
  }
}
