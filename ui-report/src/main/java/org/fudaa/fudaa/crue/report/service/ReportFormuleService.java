/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.service;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroupResult;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.FormuleContentManager;
import org.fudaa.dodico.crue.projet.report.ReportFormuleServiceContrat;
import org.fudaa.dodico.crue.projet.report.data.ReportRunContent;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.formule.*;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.CrueSwingWorker;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.report.contrat.ReportViewServiceContrat;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.fudaa.fudaa.crue.report.view.persist.FormuleSaverProcessor;
import org.fudaa.fudaa.crue.report.view.persist.ReportFormuleReaderSaver;
import org.openide.util.Lookup;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Permet de gérer les expressions utilisees pour le run chargé.
 * * <table  border="1"  cellpadding="3" cellspacing="0"  summary="lookups">
 * <tr>
 * <th align="left">Lookup</th>
 * <th align="left">Commentaire</th>
 * <th align="left">Méthode utilisant le lookup</th>
 * </tr>
 * <tr>
 * <td>{@link FormuleServiceContent}</td>
 * <td>	Le conteneur des variables définies pour le service</td>
 * <td><code>{@link #getServiceContent()}}</code></td>
 * </tr>
 * </table>
 *
 * @author Frederic Deniger
 */
@ServiceProviders(value = {
        @ServiceProvider(service = ReportFormuleService.class),
        @ServiceProvider(service = ReportFormuleServiceContrat.class)})
public class ReportFormuleService implements Lookup.Provider, FormuleContentManager, ReportFormuleServiceContrat {
    private final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
    private final Lookup.Result<ReportRunContent> resultat;
    private final ReportViewServiceContrat reportViewServiceContrat = Lookup.getDefault().lookup(ReportViewServiceContrat.class);
    private final PerspectiveServiceReport perspectiveServiceReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);
    /**
     * true si modifié
     */
    volatile boolean modified;
    private final InstanceContent dynamicContent = new InstanceContent();
    protected final Lookup lookup = new AbstractLookup(dynamicContent);
    private final LookupListener resulRunLookupListener;
    private FormuleServiceContent serviceContent;
    private volatile boolean updating = false;
    private volatile boolean loading = false;

    public ReportFormuleService() {
        resultat = reportService.getLookup().lookupResult(ReportRunContent.class);
        resulRunLookupListener = ev -> runLoadedChanged();
        resultat.addLookupListener(resulRunLookupListener);
        runLoadedChanged();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FormuleContent> getContents() {
        return getServiceContent().getVariables();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getVariablesKeys() {
        return serviceContent == null ? Collections.emptyList() : serviceContent.getVariablesKeys();
    }

    /**
     * @return le manager des expressions
     */
    public FormuleServiceContent getServiceContent() {
        return serviceContent;
    }

    /**
     * le run courant a changé. On recharge les exrpressions
     */
    private void runLoadedChanged() {
        if (serviceContent != null) {
            dynamicContent.remove(serviceContent);
            serviceContent = new FormuleServiceContent();
            dynamicContent.add(serviceContent);
        }
        if (reportService.isRunLoaded()) {
            loading = true;
            new CrueSwingWorker<FormuleServiceContent>(NbBundle.getMessage(ReportFormuleService.class, "ReadVariableFC.Bilan")) {
                @Override
                protected FormuleServiceContent doProcess() throws Exception {
                    return load();
                }

                @Override
                protected void done(FormuleServiceContent result) {
                    loading = false;
                    serviceContent = result;
                    if (serviceContent == null) {
                        serviceContent = new FormuleServiceContent();
                    }
                    dynamicContent.add(serviceContent);
                    detectCycle();
                }
            }.execute();
        }
    }

    private File getTargetFile() {
        final File dirOfRapports = reportService.getSelectedProjet().getInfos().getDirOfRapports();
        if (dirOfRapports == null) {
            return null;
        }
        return new File(dirOfRapports, "VariablesFC.xml");
    }

    /**
     * Parcourt l'ensemble des variables pour trouver des cycles éventuels.
     */
    private void detectCycle() {
        FormuleDependenciesProcessor processor = new FormuleDependenciesProcessor(this);
        FormuleDependenciesContent compute = processor.compute(getServiceContent().getVariablesKeys());
        List<String> cycliqueVar = compute.getCycliqueVar();
        if (!cycliqueVar.isEmpty()) {
            List<FormuleContent> contents = getContents();
            if (contents != null) {
                for (FormuleContent formuleContent : contents) {
                    if (cycliqueVar.contains(formuleContent.getId())) {
                        formuleContent.setCyclique(true);
                    }
                }
            }

            DialogHelper.showError(NbBundle.getMessage(ReportFormuleService.class, "cycleDetected.Error"), "<li>" + StringUtils.join(cycliqueVar,
                    "</li><li>") + "</li>");
        }
    }

    /**
     * @return le conteneur des expressions charngés pour le run en cours.
     */
    private FormuleServiceContent load() {
        if (!reportService.isRunLoaded()) {
            return new FormuleServiceContent();
        }
        ReportFormuleReaderSaver saver = new ReportFormuleReaderSaver();
        final File targetFile = getTargetFile();
        if (targetFile == null || !targetFile.exists()) {
            return new FormuleServiceContent();
        }
        CrueIOResu<FormuleServiceContent> read = saver.readContent(targetFile);
        if (read.getAnalyse().containsErrorOrSevereError()) {
            LogsDisplayer.displayError(read.getAnalyse(), NbBundle.getMessage(ReportFormuleService.class, "ReadVariableFC.Bilan"));
        }

        return read.getMetier();
    }

    /**
     * @return true si en cours de mise à jour
     */
    public boolean IsUpdating() {
        return updating;
    }

    /**
     * @return true si en cours de chargement
     */
    public boolean isLoading() {
        return loading;
    }

    @Override
    public Lookup getLookup() {
        return lookup;
    }

    /**
     * @param selectedTime           le temps sélectionné
     * @param key                    la clé de la variables demandé
     * @param emhNom                 le nom de l'EMH porteuse des résultats
     * @param selectedTimesInRapport l'ensemble des pas de temps sélectionnés dans la perspective
     * @return null si non chargé ou valeur non trouvée
     */
    Double getValue(ResultatTimeKey selectedTime, ReportRunVariableKey key, String emhNom, Collection<ResultatTimeKey> selectedTimesInRapport) {
        loadIfNeeded();
        if (serviceContent == null) {
            return null;
        }
        FormuleContent formuleContent = serviceContent.getContent(key);
        if (formuleContent == null || formuleContent.getCalculator() == null) {
            return null;
        }
        return formuleContent.getCalculator().getValue(selectedTime, key, emhNom, selectedTimesInRapport);
    }

    /**
     * @param key la cle de la variable
     * @return la nature utilisé par l'expression correspondante ( clé du CCM)
     */
    public String getPropertyNature(ReportVariableKey key) {
        loadIfNeeded();
        FormuleContent content = serviceContent.getContent(key);
        if (content == null) {
            return null;
        }
        return content.getPropertyNature();
    }

    /**
     * @param key la clé de la variable
     * @param ccm pour récupérer la {@link PropertyNature}
     * @return la nature utilisé par l'expression correspondante
     */
    PropertyNature getPropertyNature(ReportVariableKey key, CrueConfigMetier ccm) {
        loadIfNeeded();
        String variableName = getPropertyNature(key);
        if (variableName != null) {
            return ccm.getNature(variableName);
        }
        return null;
    }

    /**
     * @param varName le nom de la variable définie
     * @param ccm     pour récupérer la {@link PropertyNature}
     * @return la nature utilisé par l'expression correspondante
     */
    PropertyNature getPropertyNature(String varName, CrueConfigMetier ccm) {
        loadIfNeeded();
        if (serviceContent == null) {
            return null;
        }
        FormuleContent content = serviceContent.getContent(varName);
        if (content == null || content.getPropertyNature() == null) {
            return null;
        }
        return ccm.getNature(content.getPropertyNature());
    }

    /**
     * Met à jour tous les services suite à une modification des formules.
     *
     * @param data les formules éditées
     */
    public void modified(FormuleDataEdited data) {
        //perspective modifiée
        modified = true;
        perspectiveServiceReport.setDirty(true);
        final ReportGlobalService reportGlobalService = Lookup.getDefault().lookup(ReportGlobalService.class);
        //calcul des différences
        FormuleContentServiceUpdater updater = new FormuleContentServiceUpdater(serviceContent, reportGlobalService);
        CtuluLogGroupResult<FormuleServiceContent, FormuleDataChanges> computeChanges = updater.computeChanges(data);
        updating = true;
        try {
            if (serviceContent != null) {
                dynamicContent.remove(serviceContent);
                serviceContent = null;
            }
            serviceContent = computeChanges.getResultat();
            updating = false;
            if (serviceContent != null) {
                dynamicContent.add(serviceContent);
            }
        } finally {
            updating = false;
        }
        //envoi des evts
        reportViewServiceContrat.variablesUpdated(computeChanges.getComplement());
        //affichages des erreurs si trouvées
        if (computeChanges.getLog().containsFatalError()) {
            LogsDisplayer.displayError(computeChanges.getLog(), NbBundle.getMessage(ReportFormuleService.class, "UpdateFormules.Bilan"));
        }
        //détections des cycles
        detectCycle();
    }

    /**
     * Sauvegarde sur fichier des formules
     *
     * @see FormuleSaverProcessor
     */
    void save() {
        if (modified) {
            File target = getTargetFile();
            FormuleSaverProcessor processor = new FormuleSaverProcessor(target, getServiceContent().getParameters());
            final String action = NbBundle.getMessage(ReportRunAlternatifService.class, "Formules.SaveAction");
            CtuluLog log = CrueProgressUtils.showProgressDialogAndRun(processor, action);
            if (log.isNotEmpty()) {
                LogsDisplayer.displayError(log, action);
            }
            modified = false;
        }
    }

    /**
     * rechargement de l'ensemble des données.
     */
    void reload() {
        runLoadedChanged();
    }

    /**
     * @param varName le nom de la variable
     * @return true si le nom <code>varName</code> correspond à une expression
     */
    public boolean isFormule(String varName) {
        loadIfNeeded();
        return this.serviceContent != null && serviceContent.getContent(varName) != null;
    }

    /**
     * Chargement des données si nécessaire.
     */
    private void loadIfNeeded() {
        if (serviceContent == null && reportService.isRunLoaded()) {
            this.serviceContent = load();
            detectCycle();
            this.dynamicContent.add(serviceContent);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<String> getDirectUsedVariable(String var) {
        FormuleContent content = serviceContent.getContent(var);
        if (content == null || content.getCalculator() == null) {
            return Collections.emptySet();
        }
        return content.getCalculator().getDirectUsedVariables();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsTimeDependantVariable(String var) {
        FormuleContent content = serviceContent.getContent(var);
        return content != null && content.getCalculator() != null && content.getCalculator().containsTimeDependantVariable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isExpressionValid(String var, Collection<ResultatTimeKey> selectedTimes) {
        FormuleContent content = serviceContent.getContent(var);
        return content == null || content.getCalculator() == null || content.getCalculator().isExpressionValid(selectedTimes);
    }
}
