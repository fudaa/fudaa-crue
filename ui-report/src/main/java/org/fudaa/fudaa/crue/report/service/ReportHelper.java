/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.service;

import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.openide.util.ImageUtilities;

/**
 *
 * @author Frederic Deniger
 */
public class ReportHelper {

  public static <T extends EMH> List<T> getEMHWithResultat(List<T> in) {
    if (in == null) {
      return null;
    }
    List<T> res = new ArrayList<>();
    for (T emh : in) {
      if (emh.hasResultat()) {
        res.add(emh);
      }
    }
    return res;

  }

  public static ImageIcon getIcon(String iconName) {
    return ImageUtilities.loadImageIcon("org/fudaa/fudaa/crue/report/icons/" + iconName, false);
  }
}
