/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import java.util.List;
import org.fudaa.dodico.crue.common.io.AbstractCrueDao;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.joda.time.LocalDateTime;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("Runs-Alternatifs")
public class RunAlternatifDao extends AbstractCrueDao {

  @XStreamAlias("AuteurDerniereModif")
  private String auteurDerniereModif;
  @XStreamAlias("DateDerniereModif")
  private LocalDateTime dateDerniereModif;
  @XStreamImplicit(itemFieldName = "Run-Alternatif")
  private List<ReportRunKey> runAlternatifs;

  public String getAuteurDerniereModif() {
    return auteurDerniereModif;
  }

  public void setAuteurDerniereModif(String auteurDerniereModif) {
    this.auteurDerniereModif = auteurDerniereModif;
  }

  public LocalDateTime getDateDerniereModif() {
    return dateDerniereModif;
  }

  public void setDateDerniereModif(LocalDateTime dateDerniereModif) {
    this.dateDerniereModif = dateDerniereModif;
  }

  public List<ReportRunKey> getRunAlternatifs() {
    return runAlternatifs;
  }

  public void setRunAlternatifs(List<ReportRunKey> runAlternatifs) {
    this.runAlternatifs = runAlternatifs;
  }
}
