/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.data;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.fudaa.crue.report.service.ReportFormuleService;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ReportVariableCellRenderer extends CtuluCellTextRenderer {

  final ReportFormuleService reportFormuleService = Lookup.getDefault().lookup(ReportFormuleService.class);

  public ReportVariableCellRenderer() {
    super(null);
  }

  @Override
  protected void setValue(Object _value) {
    if (_value == null) {
      super.setValue(" ");
    } else {
      final String toString = ObjectUtils.toString(_value);
      if (reportFormuleService.isFormule(toString)) {
        super.setValue(toString);
      } else {
        super.setValue(StringUtils.capitalize(toString));
      }
    }
  }
}
