/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.extern;

import java.io.File;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;

/**
 *
 * @author Frederic Deniger
 */
public class ExternFileContent {
  
  public static final String PROP_NAME="file";

  File file;

  public ExternFileContent() {
  }

  public ExternFileContent(File file) {
    this.file = file;
  }
  
  

  @PropertyDesc(i18n = "fileExtern.property", i18nBundleBaseName = "org/fudaa/fudaa/crue/report/extern/Bundles.properties")
  public File getFile() {
    return file;
  }

  public void setFile(File file) {
    this.file = file;
  }
}
