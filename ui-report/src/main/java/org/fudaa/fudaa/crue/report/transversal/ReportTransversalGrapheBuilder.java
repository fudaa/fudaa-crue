/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.transversal;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHSectionProfil;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.dodico.crue.metier.helper.ReportProfilHelper;
import org.fudaa.dodico.crue.projet.report.data.*;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.dodico.crue.projet.report.persist.ReportTransversalConfig;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.ebli.courbe.EGGrapheSimpleModel;
import org.fudaa.fudaa.crue.loi.ViewCourbeManager;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiCourbeModel;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiUiController;
import org.fudaa.fudaa.crue.loi.common.CourbeModelWithKey;
import org.fudaa.fudaa.crue.loi.common.LoiConstanteCourbeModel;
import org.fudaa.fudaa.crue.loi.section.ProfiLSectionCourbeModel;
import org.fudaa.fudaa.crue.loi.section.ProfilSectionCourbeBuilder;
import org.fudaa.fudaa.crue.loi.section.ProfilSectionLoiUiController;
import org.fudaa.fudaa.crue.report.config.AbstractReportGrapheBuilder;
import org.netbeans.api.progress.ProgressHandle;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public class ReportTransversalGrapheBuilder extends AbstractReportGrapheBuilder<ProfilSectionLoiUiController, ReportTransversalConfig> {

    ReportTransversalGrapheBuilder(ProfilSectionLoiUiController uiController, ViewCourbeManager loiLabelsManager) {
        super(uiController, loiLabelsManager);
    }

    private EGAxeVertical getOrCreateAxeVertical() {
        return getOrCreateAxeVertical(getVariableZ());
    }

    @Override
    public List<EGCourbeSimple> getInternCourbes(ReportTransversalConfig content, ProgressHandle progress) {
        final List<EGCourbeSimple> zXtCourbes = createZXtCourbes(content, progress);
        zXtCourbes.addAll(createVariableCourbe(content, progress));
        return zXtCourbes;
    }

    @Override
    public List<EGCourbeSimple> getInternCourbesAfterTimeChanged(ReportTransversalConfig content, ProgressHandle progress) {
        final List<EGCourbeSimple> zXtCourbes = retrieveZXtCourbes();//on ne reconstruit pas les courbes.
        zXtCourbes.addAll(createVariableCourbe(content, progress));
        return zXtCourbes;
    }

    @Override
    public void applyLabelsConfigChanged(ReportTransversalConfig content) {
        List<LabelConfig> labels = content.getLoiLegendConfig().getLabels();
        ReportRunKey runKey = reportService.getRunCourant() == null ? null : reportService.getRunCourant().getRunKey();
        for (LabelConfig config : labels) {
            ReportLabelContent labelContent = (ReportLabelContent) config.getKey();
            String text = runKey == null ? StringUtils.EMPTY : reportResultProviderService.getLabelString(labelContent, runKey, content.getSectionName(),
                    DecimalFormatEpsilonEnum.PRESENTATION, content.getSelectedTimeStepInReport());
            String tltip = runKey == null ? StringUtils.EMPTY : reportResultProviderService.getLabelString(labelContent, runKey, content.getSectionName(),
                    DecimalFormatEpsilonEnum.COMPARISON, content.getSelectedTimeStepInReport());
            config.setText(text);
            config.setTooltipText(tltip);
        }
        this.loiLabelsManager.getLoiLabelsManager().applyConfig();
    }

    private List<EGCourbeSimple> retrieveZXtCourbes() {
        EGGrapheSimpleModel model = uiController.getEGGrapheSimpleModel();
        List<EGCourbeSimple> res = new ArrayList<>();
        for (EGCourbeSimple courbe : model.getCourbes()) {
            ReportRunVariableKey key = (ReportRunVariableKey) ((CourbeModelWithKey) courbe.getModel()).getKey();
            if (AbstractLoiUiController.PROP_Z_XT.equals(key.getVariable().getVariableName())) {
                res.add(courbe);
            }
        }
        return res;
    }

    private List<EGCourbeSimple> createVariableCourbe(ReportTransversalConfig content, ProgressHandle progress) {
        EGAxeVertical axeVertical = getOrCreateAxeVertical();
        List<EGCourbeSimple> res = new ArrayList<>();
        double min = uiController.getGraphe().getXMin();
        double max = uiController.getGraphe().getXMax();
        if (CtuluLib.isEquals(min, max, 1e-1)) {
            max = min + 1;
        }
        final List<ReportRunVariableKey> profils = content.getProfilVariables();
        for (ReportRunVariableKey reportRunVariableKey : profils) {
            if (!reportResultProviderService.isExpressionValid(reportRunVariableKey.getVariableName(), content.getSelectedTimeStepInReport())) {
                continue;
            }
            if (progress != null) {
                progress.setDisplayName(reportRunVariableKey.getDisplayName());
            }
            Double value = reportResultProviderService.getValue(reportRunVariableKey, content.getSectionName(), content.getSelectedTimeStepInReport());
            if (value != null) {
                ItemVariable varX = reportResultProviderService.getCcmVariable(getXtZVariableName());
                ItemVariable varY = reportResultProviderService.getCcmVariable(reportRunVariableKey.getVariable());
                LoiConstanteCourbeModel model = LoiConstanteCourbeModel.create(reportRunVariableKey, min, max, value, varX, varY);
                model.setTitle(reportRunVariableKey.getVariable().getVariableDisplayName() + BusinessMessages.ENTITY_SEPARATOR + reportRunVariableKey.
                    getRunKey().getDisplayName());
                EGCourbeSimple courbe = new EGCourbeSimple(axeVertical, model);
                AbstractReportGrapheBuilder.applyPersistConfig(content.getCourbeconfigs().get(reportRunVariableKey), courbe);
                if (!content.getCourbeconfigs().containsKey(reportRunVariableKey)) {
                    content.getCourbeconfigs().put(reportRunVariableKey, courbe.getPersitUiConfig());
                }
                res.add(courbe);
            }
        }
        return res;
    }

    private ReportRunVariableKey createXtZKey(final ReportRunKey runKey) {
        return new ReportRunVariableKey(runKey, new ReportVariableKey(ReportVariableTypeEnum.READ, getXtZVariableName()));
    }

    private ItemVariable getVariableZ() {
        return reportService.getCcm().getProperty(CrueConfigMetierConstants.PROP_Z);
    }

    private String getXtZVariableName() {
        return AbstractLoiUiController.PROP_Z_XT;
    }

    private List<EGCourbeSimple> createZXtCourbes(ReportTransversalConfig content, ProgressHandle progress) {
        uiController.configureAxeH(reportService.getCcm().getProperty(CrueConfigMetierConstants.PROP_XT).getNature(), true);
        final ReportRunContent runCourant = reportService.getRunCourant();
        EMH emhInRunCourant = runCourant == null ? null : runCourant.getEMH(content.getSectionName());
        EMHSectionProfil sectionProfil = ReportProfilHelper.retrieveProfil(emhInRunCourant);
        //on construit toujours le profil du run courant si possible:
        final String displayNom = getXtZVariableName();
        List<EGCourbeSimple> courbes = new ArrayList<>();
        if (sectionProfil != null) {
            final String name = displayNom + BusinessMessages.ENTITY_SEPARATOR + runCourant.getRunKey().getDisplayName();
            if (progress != null) {
                progress.setDisplayName(name);
            }
            uiController.setProfilSection(DonPrtHelper.getProfilSection(sectionProfil), reportService.getCcm(), sectionProfil.getParent(), name);
            ReportRunVariableKey key = createXtZKey(runCourant.getRunKey());
            AbstractLoiCourbeModel withKey = (AbstractLoiCourbeModel) uiController.getCourbe().getModel();
            withKey.setKey(key);
            uiController.getCourbe().setSilent(true);
            AbstractReportGrapheBuilder.applyPersistConfig(content.getCourbeconfigs().get(key), uiController.getCourbe());
            uiController.getCourbe().setSilent(false);
            courbes.add(uiController.getCourbe());
        }
        final ItemVariable variableZ = getVariableZ();
        EGAxeVertical axeVertical;
        if (uiController.getCourbe() != null) {
            axeVertical = uiController.getCourbe().getAxeY();
        } else {
            axeVertical = uiController.createAxeVertical(variableZ, true);
        }
        for (ReportRunKey reportRunKey : content.getProfilXtZ()) {
            ReportRunContent reportRunContent = reportResultProviderService.getRunContent(reportRunKey);
            EMH emh = reportRunContent.getEMH(content.getSectionName());
            sectionProfil = ReportProfilHelper.retrieveProfil(emh);
            if (sectionProfil != null) {
                if (progress != null) {
                    progress.setDisplayName(reportRunKey.getDisplayName());
                }
                DonPrtGeoProfilSection profil = DonPrtHelper.getProfilSection(sectionProfil);
                LoiFF createFromProfil = LoiHelper.createFromProfil(profil.getPtProfil(), emh.getNom());
                ReportRunVariableKey key = createXtZKey(reportRunContent.getRunKey());
                ProfiLSectionCourbeModel model = ProfiLSectionCourbeModel.create(createFromProfil, reportService.getCcm().getConfLoi().get(
                        EnumTypeLoi.LoiPtProfil));
                model.setKey(key);
                model.setTitle(displayNom + BusinessMessages.ENTITY_SEPARATOR + reportRunContent.getRunKey().getDisplayName());
                EGCourbeSimple courbe = ProfilSectionCourbeBuilder.build(axeVertical, model);
                final EGCourbePersist courbeConfig = content.getCourbeconfigs().get(key);
                AbstractReportGrapheBuilder.applyPersistConfig(courbeConfig, courbe);
                courbes.add(courbe);
            }
        }
        return courbes;
    }
}
