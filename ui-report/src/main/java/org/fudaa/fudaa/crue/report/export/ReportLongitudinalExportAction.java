/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.export;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.export.ReportExportProfilLongitudinal;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.report.data.ReportVariableChooser;
import org.fudaa.fudaa.crue.report.longitudinal.ReportProfilLongitudinalTopComponent;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLongitudinalExportAction extends ReportCourbeExportActionAbstract<ReportProfilLongitudinalTopComponent> {

  public ReportLongitudinalExportAction(ReportProfilLongitudinalTopComponent topComponent) {
    super(topComponent);
  }

  @Override
  protected ReportExportProfilLongitudinal createReportExporter(ReportResultProviderService reportResultProviderService) {
    return new ReportExportProfilLongitudinal(topComponent.getReportConfig(), reportResultProviderService);
  }

  @Override
  protected List<String> getChoosableVariables() {
    final List<String> choosableVariables = topComponent.getChoosableVariablesForExport();
    choosableVariables.remove(null);
    return choosableVariables;
  }

  @Override
  protected List<String> getEMHs() {
    return Collections.emptyList();
  }

  @Override
  protected Set<String> getCurrentSelectedVariables() {
    Set<String> res = new HashSet<>();
    final ReportResultProviderService reportResultProviderService = Lookup.getDefault().lookup(ReportResultProviderService.class);
    String var = topComponent.getReportConfig().getVariableDrawnOnRightAxis(reportResultProviderService);
    if (var != null) {
      res.add(var);
    }
    final List<String> variablesAsZDrawn = topComponent.getReportConfig().
            getVariablesAsZDrawn(reportResultProviderService.getRunCourant().getRunKey(), reportResultProviderService);
    if (variablesAsZDrawn != null) {
      res.addAll(variablesAsZDrawn);
    }
    return res;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    final List<String> choosableVariables = getChoosableVariables();
    final Set<String> selectedVariables = getCurrentSelectedVariables();
    ReportVariableChooser variableChooser = new ReportVariableChooser(choosableVariables, selectedVariables);
    final List<String> toExport = variableChooser.choose();
    if (!toExport.isEmpty()) {
      //on récupère tous les runs configurés pour la vue en question.
      final ReportResultProviderService reportResultProviderService = Lookup.getDefault().lookup(ReportResultProviderService.class);
      Set<ReportRunKey> runKey = new HashSet<>();
      fillWithSelectedRunKeys(runKey);
      runKey.add(reportResultProviderService.getRunCourant().getRunKey());
      List<ReportRunKey> keys = new ArrayList<>();
      keys.addAll(runKey);
      Collections.sort(keys, ObjetNommeByNameComparator.INSTANCE);
      final Set<ResultatTimeKey> selectedTimeKeys = new TreeSet<>();
      if (reportResultProviderService.getReportService().getSelectedTime() != null) {
        selectedTimeKeys.add(reportResultProviderService.getReportService().getSelectedTime());
      }
      selectedTimeKeys.addAll(topComponent.getReportConfig().getTimes());

      final List<ReportRunVariableKey> variablesKeys = variableChooser.getKeysForRuns(reportResultProviderService, toExport, keys);

      final File f = chooseTargetFile();

      if (f != null) {
        String error = CtuluLibFile.canWrite(f);
        if (error == null && f.exists() && !f.delete()) {
          error = org.openide.util.NbBundle.getMessage(ReportCourbeExportActionAbstract.class, "cantWriteToFileUsedByAnotherProcess.error");
        }
        if (error != null) {
          DialogHelper.showError(org.openide.util.NbBundle.getMessage(ReportCourbeExportActionAbstract.class, "cantWriteToFile.error", f.getName(),
                  error));
          return;

        }
        ProgressRunnable runnable = new ProgressRunnable<Boolean>() {

          @Override
          public Boolean run(ProgressHandle handle) {
            ReportExportProfilLongitudinal exporter = createReportExporter(reportResultProviderService);
            exporter.export(variablesKeys, new ArrayList<>(selectedTimeKeys), f);
            return Boolean.TRUE;
          }
        };
        execute(runnable, f);

      }

    }
  }
//

  @Override
  protected void fillWithSelectedRunKeys(Set<ReportRunKey> keys) {
    final List<ReportRunVariableKey> profilVariables = topComponent.getReportConfig().getProfilVariables();
    for (ReportRunVariableKey profilVariable : profilVariables) {
      keys.add(profilVariable.getReportRunKey());
    }
  }
}
