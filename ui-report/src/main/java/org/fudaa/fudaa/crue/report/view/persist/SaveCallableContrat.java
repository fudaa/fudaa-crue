/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import java.io.File;
import java.util.concurrent.Callable;
import org.fudaa.ctulu.CtuluLog;

/**
 *
 * @author Frederic Deniger
 */
public interface SaveCallableContrat extends  Callable<CtuluLog>{
  
  File getTargetFile();
  
}
