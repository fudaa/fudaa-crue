/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.helper;

import com.memoire.fu.FuEmptyArrays;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVerticalTimeKey;
import org.fudaa.ebli.courbe.EGModelExportable;
import org.fudaa.fudaa.crue.loi.common.LoiConstanteCourbeModel;
import org.fudaa.fudaa.crue.report.service.ReportService;

/**
 *
 * @author Frederic Deniger
 */
public class ReportVerticalTimeValueCourbeModel extends LoiConstanteCourbeModel implements EGModelExportable {

  ResultatTimeKey resultatKey;
  ToStringTransformer transformer;

  public ReportVerticalTimeValueCourbeModel() {
    super(FuEmptyArrays.DOUBLE0, FuEmptyArrays.DOUBLE0, null, null);
    setKey(ReportVerticalTimeKey.INSTANCE);
  }

  public ReportVerticalTimeValueCourbeModel(ReportService reportService, long x) {
    super(new double[]{x, x}, new double[]{0, 1}, null, null);
    this.resultatKey = reportService.getSelectedTime();
    transformer = reportService.getReportTimeFormatter().getResulatKeyTempsSceToStringTransformer();
    setKey(ReportVerticalTimeKey.INSTANCE);
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  @Override
  public boolean isModifiable() {
    return false;
  }

  @Override
  public String getPointLabel(int _i) {
    if (super.getNbValues() > 1 && _i == 0) {
      return transformer.transform(resultatKey);
    }
    return null;
  }

  @Override
  public boolean isViewableinTable() {
    return false;
  }
}
