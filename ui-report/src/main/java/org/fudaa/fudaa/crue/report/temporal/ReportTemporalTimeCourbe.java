/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.temporal;

import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.courbe.EGRepere;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Frederic Deniger
 */
public class ReportTemporalTimeCourbe extends EGCourbeSimple {

  ReportTemporalTimeCourbe(EGAxeVertical egAxeVertical, EGModel egModel) {
    super(egAxeVertical, egModel);
  }

  @Override
  protected void paintLabelBox(Graphics2D graphics2D, int xBox, int yBox, String pLabel, EGRepere egRepere, Rectangle view) {
    tboxLabels_.setVPosition(SwingConstants.TOP);
    view.y -= egRepere.getMargesHaut();
    tboxLabels_.paintBox(graphics2D, xBox, egRepere.getMinEcranY() - egRepere.getMargesHaut(), pLabel, view);
  }

  @Override
  public double getYEcranForAxeYNull(double yi, final EGRepere egRepere) {
    double yie;
    double ratio = Math.min(1, Math.max(0, yi));
    if (ratio >= 1) {
      yie = egRepere.getMaxEcranY();
    } else if (ratio <= 0) {
      yie = (double)egRepere.getMinEcranY() - egRepere.getMargesHaut();
    } else {
      yie = egRepere.getMinEcranY() + (egRepere.getMaxEcranY() - egRepere.getMinEcranY()) * ratio;
    }
    return yie;
  }
}
