package org.fudaa.fudaa.crue.report;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuGridLayout;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.dodico.crue.common.time.TimeFormatOption;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.metier.result.ModeleResultatTimeKeyContent;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.report.service.RangeSelectedTimeKey;
import org.fudaa.fudaa.crue.report.service.ReportTimeFormatter;
import org.fudaa.fudaa.crue.report.service.ReportTimeListener;
import org.fudaa.fudaa.crue.report.service.ReportTimeListenerManager;
import org.fudaa.fudaa.crue.report.time.ReportTimeStepTopPanel;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.report//ReportTimeTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = ReportTimeTopComponent.TOPCOMPONENT_ID,
        iconBase = "org/fudaa/fudaa/crue/report/rond-orange_16.png",
        persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "report-middleLeft", openAtStartup = false, position = 1)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.report.ReportTimeTopComponent")
@ActionReference(path = "Menu/Window/Report", position = 7)
@TopComponent.OpenActionRegistration(displayName = ReportTimeTopComponent.TOPCOMPONENT_ACTION_DISPLAYNAME,
        preferredID = ReportTimeTopComponent.TOPCOMPONENT_ID)
public final class ReportTimeTopComponent extends AbstractReportTopComponent implements LookupListener, ReportTimeListener {

  public static final String TOPCOMPONENT_ID = "ReportTimeTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  final JCheckBox cbChooseTransitoire;
  final ReportTimeStepTopPanel timePanel;

  public ReportTimeTopComponent() {
    initComponents();
    setName(NbBundle.getMessage(ReportTimeTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ReportTimeTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    cbChooseTransitoire = new JCheckBox(org.openide.util.NbBundle.getMessage(ReportTimeTopComponent.class, "ButtonTransitoire.Label"));
    add(cbChooseTransitoire, BorderLayout.NORTH);
    timePanel = new ReportTimeStepTopPanel();
    timePanel.addObserver(new Observer() {
      @Override
      public void update(Observable o, Object arg) {
        if (ReportTimeStepTopPanel.RANGE_MODIFIED.equals(arg)) {
          rangeUpdatedFromPanel();
        }
      }
    });
    add(timePanel.getPanel(), BorderLayout.CENTER);
    timePanel.getSliderPanel().addObserver(new Observer() {
      @Override
      public void update(Observable o, Object arg) {
        timeChangedFromSlider();
      }
    });
    timePanel.getBtParameter().addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        editParameters();
      }
    });
    timePanel.getSliderPanel().getVideoRecorder().getBtVideo().addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        editVideo();
      }
    });
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueGestionnaireTemporel", PerspectiveEnum.REPORT);
  }

  protected void editVideo() {
    timePanel.getSliderPanel().getVideoRecorder().edit(timePanel.getNbTimeStepWillBePlayed());
    timePanel.getSliderPanel().getVideoRecorder().updateBtVideo();
  }

  public List<ResultatTimeKey> getCurrentTimes() {
    return new ArrayList<>(timePanel.getSliderPanel().getValues());
  }

  protected void editParameters() {
    boolean useSec = reportService.getTimeFormat().useSeconds();
    JPanel panel = new JPanel(new BuGridLayout(2, 5, 5));
    panel.setBorder(BuBorders.EMPTY2222);
    panel.add(new JLabel(NbBundle.getMessage(ReportTimeTopComponent.class, "CheckBoxFormatInSecond.Label")));
    JCheckBox cb = new JCheckBox();
    cb.setSelected(useSec);
    panel.add(cb);
    panel.add(new JLabel(NbBundle.getMessage(ReportTimeTopComponent.class, "AnimationDelay.Label")));
    JSpinner spinner = new JSpinner(new SpinnerNumberModel(timePanel.getSliderPanel().getPlayDelayInSec(), 0.1, 100, 0.1));
    panel.add(spinner);    
    SysdocUrlBuilder.installHelpShortcut(panel, SysdocUrlBuilder.getDialogHelpCtxId("vueGestionnaireTemporel_EditerParametres", PerspectiveEnum.REPORT));
    boolean ok = DialogHelper.showQuestionOkCancel(NbBundle.getMessage(ReportTimeTopComponent.class, "ChangeParameter.DialogTitle"), panel);
    if (ok) {
      boolean newValue = cb.isSelected();
      if (newValue != useSec) {
        TimeFormatOption clone = reportService.getTimeFormat().clone();
        clone.setUseSeconds(newValue);
        reportService.setTimeFormat(clone);
      }
      double delay = ((Double) spinner.getValue()).intValue();
      timePanel.getSliderPanel().setPlayDelayInSec(delay);
    }

  }

  @Override
  public void timeFormatChanged() {
    updateTransformers();
  }

  @Override
  public void rangeChanged() {
  }

  public void timeChangedFromSlider() {
    ResultatTimeKey newKey = (ResultatTimeKey) timePanel.getSliderPanel().getSelectedItem();
    ResultatTimeKey currentKey = reportService.getSelectedTime();
    if (!ObjectUtils.equals(newKey, currentKey)) {
      reportService.setSelectedTime(newKey);
    }
  }

  protected void rangeUpdatedFromPanel() {
    reportService.setRangeSelectedTimeKey(new RangeSelectedTimeKey(getCurrentTimes()));
  }

  @Override
  public void selectedTimeChanged() {
  }

  @Override
  protected void componentActivated() {
    super.componentActivated();
  }

  @Override
  protected void componentDeactivated() {
    super.componentDeactivated();
  }

  @Override
  public void setEditable(boolean b) {
  }
  final ChangeTransitoireActionListener changeTransitoireActionListener = new ChangeTransitoireActionListener();

  private class ChangeTransitoireActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      timeLoaded();
    }
  }

  @Override
  public void runLoaded() {
    manager = new ReportTimeListenerManager();
    manager.addListener(this);
    timeLoaded();
    timeResult = reportService.getLookup().lookupResult(ModeleResultatTimeKeyContent.class);
    timeResult.addLookupListener(timeListener);
    updateEditableState(true);
    cbChooseTransitoire.addActionListener(changeTransitoireActionListener);

  }

  @Override
  public void runUnloaded() {
    if (manager != null) {
      manager.removeAllListeners();
    }
    unloadTime();
    if (timeResult != null) {
      timeResult.removeLookupListener(timeListener);
      timeResult = null;
    }
    cbChooseTransitoire.removeActionListener(changeTransitoireActionListener);
    updateEditableState(false);
  }
  ReportTimeListenerManager manager;

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  Result<ModeleResultatTimeKeyContent> timeResult;
  final LookupListener timeListener = new LookupListener() {
    @Override
    public void resultChanged(LookupEvent ev) {
      if (reportService.getTimeContent() != null) {
        timeLoaded();
      }
    }
  };

  @Override
  protected void componentOpenedHandler() {
  }

  @Override
  protected void componentClosedDefinitlyHandler() {
  }

  void writeProperties(java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
    p.setProperty("version", "1.0");
  }

  void readProperties(java.util.Properties p) {
  }

  private void updateTransformers() {
    ReportTimeFormatter timeFormatter = reportService.getReportTimeFormatter();
    ToStringTransformer tempsSimu = timeFormatter.getResulatKeyTempsSimuToStringTransformer();
    ToStringTransformer tempsSce = timeFormatter.getResulatKeyTempsSceToStringTransformer();
    timePanel.setToStringTransformer(tempsSce, tempsSimu);
    timePanel.getSliderPanel().setToStringTransformer(tempsSce);
  }
  ModeleResultatTimeKeyContent timeContent;

  private void timeLoaded() {
    timeContent = reportService.getTimeContent();
    if (timeContent == null || !reportService.isRunLoaded()) {
      return;
    }

    final boolean containsPermanent = !timeContent.getPermanentsTimes().isEmpty();
    final boolean containsTransitoire = !timeContent.getTransitoiresTimes().isEmpty();

    boolean useTransitoire = cbChooseTransitoire.isSelected();
    if (useTransitoire && !containsTransitoire) {
      cbChooseTransitoire.setSelected(false);
    } else if (!useTransitoire && !containsPermanent && containsTransitoire) {
      cbChooseTransitoire.setSelected(true);
      useTransitoire = true;

    }
    List toUse = Collections.emptyList();
    if (useTransitoire) {
      toUse = timeContent.getTransitoiresTimes();
    } else if (containsPermanent) {
      toUse = timeContent.getPermanentsTimes();
    }
    updateTransformers();
    timePanel.setValues(toUse);
    reportService.setRangeSelectedTimeKey(new RangeSelectedTimeKey(getCurrentTimes()));
    if (!toUse.isEmpty()) {
      timePanel.getSliderPanel().setValue(0);
    }
  }

  private void unloadTime() {
    if (reportService.isRunLoaded()) {
      return;
    }
    timePanel.setValues(Collections.emptyList());
    cbChooseTransitoire.setSelected(false);
  }

  public void updateEditableState(boolean b) {
    cbChooseTransitoire.setEnabled(b && (timeContent != null) && !timeContent.getPermanentsTimes().isEmpty() && !timeContent.getTransitoiresTimes().isEmpty());
    timePanel.setEditable(b);
  }
}
