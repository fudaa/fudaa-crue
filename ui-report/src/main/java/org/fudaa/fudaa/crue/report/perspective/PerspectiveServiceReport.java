package org.fudaa.fudaa.crue.report.perspective;

import org.fudaa.dodico.crue.projet.report.data.ReportRunContent;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.UserSaveAnswer;
import org.fudaa.fudaa.crue.common.services.AbstractPerspectiveService;
import org.fudaa.fudaa.crue.common.services.PerspectiveService;
import org.fudaa.fudaa.crue.common.services.PerspectiveState;
import org.fudaa.fudaa.crue.report.*;
import org.fudaa.fudaa.crue.report.service.ReportSaver;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.fudaa.fudaa.crue.study.services.CrueService;
import org.openide.LifecycleManager;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;

import java.awt.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Le service perspective de Compte-Rendu.
 *
 * @author Fred Deniger
 */
@ServiceProviders(value = {
        @ServiceProvider(service = PerspectiveServiceReport.class)
        ,
        @ServiceProvider(service = PerspectiveService.class)})
public class PerspectiveServiceReport extends AbstractPerspectiveService {
    private final Set<String> components = Collections.unmodifiableSet(new LinkedHashSet<>(Arrays.asList(
            ReportSelectRunTopComponent.TOPCOMPONENT_ID,
            ReportTimeTopComponent.TOPCOMPONENT_ID,
            ReportVariableTopComponent.TOPCOMPONENT_ID,
            ReportVisualTopComponent.TOPCOMPONENT_ID,
            ReportPropertiesTopComponent.TOPCOMPONENT_ID,
            ReportLayersTreeComponent.TOPCOMPONENT_ID,
            ReportNetworkTopComponent.TOPCOMPONENT_ID,
            ReportViewManagerTopComponent.TOPCOMPONENT_ID)));
    private final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
    private final Result<ReportRunContent> runContentResult;
    /**
     * contient l'état ouvert ou non
     */
    private final CrueService crueService = Lookup.getDefault().lookup(CrueService.class);
    private boolean additionalLayersModified;

    /**
     * Ajoute des listener sur le service {@link CrueService} pour suivre le (de)chargement de l'étude.
     * Ajoute des listener sur le service {@link ReportService} pour suivre le (de)chargement de runs.
     *
     * @see ReportRunContent
     */
    public PerspectiveServiceReport() {
        super(PerspectiveEnum.REPORT);
        crueService.addEMHProjetStateLookupListener(ev -> projectLoadedChange());
        runContentResult = reportService.getLookup().lookupResult(ReportRunContent.class);
        runContentResult.addLookupListener(ev -> reportLoadedChanged());
    }

    @Override
    public String getPathForViewsAction() {
        return ActiveReport.ACTIONS_REPORT_VIEWS;
    }

    /**
     * @return true si un "additional Layer" est modifié.
     * @see ReportSaver
     */
    public boolean isAdditionalLayersModified() {
        return additionalLayersModified;
    }

    /**
     * Pour stocker l'état de modification des calques additionnels ( importer SIG).
     * Cette propriété <code>additionalLayersModified</code> est stockée pour faire "passe-plat" entre les TopComponent et {@link ReportSaver}.
     *
     * @param modified true si des calques additionnels sont modifiés ( importer SIG par exemple).
     */
    public void setAdditionalLayersModified(boolean modified) {
        this.additionalLayersModified = modified;
    }

    /**
     * Appelé par le listener sur les chargements de {@link ReportRunContent} et appeele {@link #updateState()}
     */
    private void reportLoadedChanged() {
        //la methode setState gère le mode RO.
        updateState();
    }

    /**
     * Appelé par le listener sur les chargements de projet
     * Si pas de projet chargé, pas l'etat a read-only. Appelle {@link #updateState()} sinon.
     */
    private void projectLoadedChange() {
        if (crueService.getProjectLoaded() == null) {
            resetStateToReadOnlyTemp();
        } else {
            updateState();
        }
    }

    /**
     * Met à jour l'état read/edit de la perspective en prenant en compte l'état read-only du projet ( pas d'edition possible) et si un run est chargé
     * permet de le passer en mode "visu".
     */
    private void updateState() {
        if (crueService.isProjetReadOnly()) {
            setState(PerspectiveState.MODE_READ_ONLY_ALWAYS);
        } else {
            //si pas de run courant, mode read-only ( pas d'edition possible).
//      setState(reportService.getRunCourant() != null ? PerspectiveState.MODE_READ : PerspectiveState.MODE_READ_ONLY_TEMP);
            //les fenetres AOC sont éditables dès qu'une étude est chargée.
            setState(PerspectiveState.MODE_READ);
        }
    }

    /**
     * @return true
     */
    @Override
    public boolean supportEdition() {
        return true;
    }

    /**
     * @return si le rapport est modifié, demande confirmation avec fermeture.
     */
    @Override
    public boolean closing() {
        //modification en cours
        if (isDirty()) {
            UserSaveAnswer confirmSaveOrNot = DialogHelper.confirmSaveOrNot();
            if (UserSaveAnswer.CANCEL.equals(confirmSaveOrNot)) {
                //l utilisateur annule la fermeture
                return false;
            }
            //l'utilisateur veut sauvegader: pour cette perspective la sauvegarde doit se faire dans le thread swing.
            //on lance la sauvegarde et on demande de fermer si réussite
            if (UserSaveAnswer.SAVE.equals(confirmSaveOrNot)) {
                EventQueue.invokeLater(() -> {
                    Runnable runnable = () -> LifecycleManager.getDefault().exit();
                    new ReportSaver().save(runnable);
                });
                //on renvoie false: c'est le thread crée ci-dessous qui fera la fermeture.
                return false;
            }
        }
        //pas de modification ou l'utilisateur ne veut pas sauvegarder: on peut fermer
        return true;
    }

    /**
     * @param state le nouvel etat
     * @return true. Pas de vérification a effecture.
     */
    @Override
    protected boolean canStateBeModifiedTo(PerspectiveState state) {
        return true;
    }

    /**
     * Appele {@link #updateState()} pour mettre à jour les JComponent.
     *
     * @return true
     */
    @Override
    public boolean activate() {
        updateState();
        return true;
    }

    @Override
    public Set<String> getDefaultTopComponents() {
        return components;
    }
}
