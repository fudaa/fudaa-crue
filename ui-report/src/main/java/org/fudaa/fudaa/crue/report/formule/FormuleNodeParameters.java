/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.formule;

import java.lang.reflect.InvocationTargetException;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.projet.report.formule.FormuleParameters;
import org.fudaa.fudaa.crue.common.property.PropertySupportRead;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frederic Deniger
 */
public abstract class FormuleNodeParameters extends AbstractNode {
  
  public static final String DESCRIPTION_PROPERTY = "description";
  final PerspectiveServiceReport perspectiveServiceReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);
  
  public FormuleNodeParameters(FormuleParameters formuleContent, boolean isNew) {
    super(Children.LEAF, Lookups.fixed(formuleContent, isNew ? StringUtils.EMPTY : formuleContent.getId()));
    setName(formuleContent.getId());
    setDisplayName(formuleContent.getNom());
    
  }
  
  public FormuleParameters getFormuleParameter() {
    return getLookup().lookup(FormuleParameters.class);
  }
  
  public String getInitId() {
    return getLookup().lookup(String.class);
  }
  
  void fireChanged() {
    super.firePropertyChange(DESCRIPTION_PROPERTY, null, getFormuleParameter().getDescription());
  }
  
  public boolean isNewFormule() {
    return StringUtils.EMPTY.equals(getInitId());
  }
  
  @Override
  public boolean canDestroy() {
    return perspectiveServiceReport.isInEditMode();
  }
  
  @Override
  protected Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    FormuleParameters lookup = getLookup().lookup(FormuleParameters.class);
    set.put(new FormuleDescriptionProperty(lookup));
    sheet.put(set);
    return sheet;
  }
  
  private static class FormuleDescriptionProperty extends PropertySupportRead<FormuleParameters, String> {
    
    public FormuleDescriptionProperty(FormuleParameters instance) {
      super(instance, String.class, DESCRIPTION_PROPERTY, NbBundle.getMessage(FormuleDescriptionProperty.class, "formule.description"), NbBundle.getMessage(FormuleNodeParameters.class, "formule.description"));
    }
    
    @Override
    public String getValue() throws IllegalAccessException, InvocationTargetException {
      return getInstance().getDescription();
    }
  }
}
