/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.longitudinal;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBrancheConfig;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalConfig;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.netbeans.swing.etable.ETableColumnModel;
import org.openide.DialogDescriptor;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLongitudinalBrancheChooser {

  final ReportLongitudinalConfig config;
  final PerspectiveServiceReport perspectiveServiceReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);

  public ReportLongitudinalBrancheChooser(ReportLongitudinalConfig config) {
    this.config = config;
  }

  public boolean displayChooseUI() {
    ReportLongitudinalBrancheChooserUI ui = new ReportLongitudinalBrancheChooserUI();
    ui.setEditable(perspectiveServiceReport.isInEditMode());
    initNodes(ui);
    DialogDescriptor descriptor = new DialogDescriptor(ui, NbBundle.getMessage(ReportLongitudinalBrancheChooser.class, "ChooseBranches.DialogTitle"));
    descriptor.createNotificationLineSupport();
    ui.setDescriptor(descriptor);
    DialogHelper.readInPreferences(ui.getView(), "outlineView", ReportLongitudinalBrancheChooserUI.class);
    //pour corriger problème de tri
    ui.getView().setTreeSortable(true);
    ETableColumnModel columnModel = (ETableColumnModel) ui.getView().getOutline().getColumnModel();
    columnModel.clearSortedColumns();
    ui.getView().setTreeSortable(false);
    //fin correctif
    SysdocUrlBuilder.installDialogHelpCtx(descriptor, "vueProfilLongitudinal_ParametrageProfil", PerspectiveEnum.REPORT,false);
    boolean ok = DialogHelper.showQuestion(descriptor, getClass(), DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    DialogHelper.writeInPreferences(ui.getView(), "outlineView", ReportLongitudinalBrancheChooserUI.class);
    if (ok) {
      config.getBranchesConfig().clear();
      Node[] nodes = ui.getExplorerManager().getRootContext().getChildren().getNodes();
      for (Node node : nodes) {
        config.getBranchesConfig().add(node.getLookup().lookup(ReportLongitudinalBrancheConfig.class));
      }
    }
    return ok;
  }

  private void initNodes(ReportLongitudinalBrancheChooserUI view) {
    final List<ReportLongitudinalBrancheConfig> branches = config.getBranchesConfig();
    final List<ReportLongitudinalBrancheConfigNode> nodes = new ArrayList<>();
    for (ReportLongitudinalBrancheConfig branche : branches) {
      ReportLongitudinalBrancheConfigNode node = new ReportLongitudinalBrancheConfigNode(branche.duplicate());
      node.setDisplayName(node.getName());
      nodes.add(node);
    }
    view.getExplorerManager().setRootContext(NodeHelper.createNode(nodes, "config", false));
    DefaultNodePasteType.updateIndexedDisplayName(view.getExplorerManager().getRootContext());
  }
}
