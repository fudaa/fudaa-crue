/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.fudaa.crue.report.service.ReportRunAlternatifService;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ByRunSelector {

  public class Line {

    final List<ReportRunKey> keys = new ArrayList<>();
    final List<JCheckBox> checkBoxes = new ArrayList<>();

    public int getNbComponents() {
      return checkBoxes.size();
    }

    public List<ReportRunKey> getSelectedKeys() {
      List<ReportRunKey> selectedkeys = new ArrayList<>();
      for (int i = 0; i < checkBoxes.size(); i++) {
        JCheckBox jCheckBox = checkBoxes.get(i);
        if (jCheckBox.isSelected()) {
          selectedkeys.add(keys.get(i));
        }
      }
      return selectedkeys;
    }

    public void setSelected(int i) {
      if (i < checkBoxes.size()) {
        checkBoxes.get(i).setSelected(true);
      }
    }

    private void create(List<ReportRunKey> runs) {
      for (ReportRunKey reportRunKey : runs) {
        JCheckBox cb = new JCheckBox(reportRunKey.getDisplayName());
        keys.add(reportRunKey);
        checkBoxes.add(cb);
      }
    }

    public void addToPanel(JPanel target) {
      for (JCheckBox jCheckBox : checkBoxes) {
        target.add(jCheckBox);
      }
    }

    public void updateCheckBoxes(Set<ReportRunKey> runKeys) {
      for (int i = 0; i < keys.size(); i++) {
        ReportRunKey key = keys.get(i);
        if (runKeys.contains(key)) {
          checkBoxes.get(i).setSelected(true);
        }
      }
    }
  }
  final ReportRunAlternatifService reportRunAlternatifService = Lookup.getDefault().lookup(ReportRunAlternatifService.class);
  final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);

  public List<Line> createLines(int nb) {
    List<ReportRunKey> allRunKey = new ArrayList<>();
    allRunKey.add(reportService.getRunCourant().getRunKey());
    allRunKey.addAll(reportRunAlternatifService.getSortedReportRunContentKeys());
    List<Line> res = new ArrayList<>(nb);
    for (int i = 0; i < nb; i++) {
      Line line = new Line();
      line.create(allRunKey);
      res.add(line);
    }
    return res;
  }
}
