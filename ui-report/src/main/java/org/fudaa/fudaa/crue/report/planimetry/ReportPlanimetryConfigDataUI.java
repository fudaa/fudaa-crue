/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.planimetry;

import com.memoire.bu.BuBorders;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.planimetry.AbstractReportPlanimetryConfigData;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryBrancheExtraData;
import org.fudaa.ebli.palette.PaletteSelecteurCouleurPlage;
import org.fudaa.fudaa.crue.common.helper.ComboBoxHelper;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.ToStringInternationalizableCellRenderer;
import org.fudaa.fudaa.crue.report.config.ReportLabelContentNode;
import org.fudaa.fudaa.crue.report.config.ReportMultiLabelContentUI;
import org.fudaa.fudaa.crue.report.config.ReportMultiLabelContentUI.LabelsEditor;
import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public class ReportPlanimetryConfigDataUI {

  private final List<ReportVariableKey> availableVar;
  private final EnumCatEMH catEMH;
  private final ReportRunKey reportRunKey;

  public ReportPlanimetryConfigDataUI(final List<ReportVariableKey> availableVar, final EnumCatEMH catEMH, final ReportRunKey reportRunKey) {
    this.availableVar = availableVar;
    this.catEMH = catEMH;
    this.reportRunKey = reportRunKey;
  }

  public boolean configure(final AbstractConfigurationExtra extraConfig) {
    final AbstractReportPlanimetryConfigData data = extraConfig.getData();
    final AbstractReportPlanimetryConfigData editedData = cloneData(data);
    final ExtraEditor editor = createEditor(editedData);
    final boolean accepted = DialogHelper.showQuestion("edit", editor.getPanel());
    if (accepted) {
      editor.apply();
    }
    extraConfig.setData(editedData);
    return accepted;

  }

  public ExtraEditor createEditor(final AbstractReportPlanimetryConfigData editedData) {
    return new ExtraEditor(editedData);
  }

  public static AbstractReportPlanimetryConfigData cloneData(final AbstractReportPlanimetryConfigData data) {
    return data.copy();
  }

  public class ExtraEditor {

    private final AbstractReportPlanimetryConfigData data;
    private JXTaskPaneContainer panel;
    LabelsEditor labelEditor;
    LabelsEditor selectedLabelEditor;
    JCheckBox cbUseLabel;
    JCheckBox cbUsePalette;
    JCheckBox cbUseDifferentSelectedLabel;
    JComboBox cbSelectPaletteVar;
    JComboBox cbSelectBrancheArrow;
    PaletteSelecteurCouleurPlage paletteSelecteur;
    ReportPalettePlagetTarget reportPalettePlagetTarget;
    private final boolean branche;

    public ExtraEditor(final AbstractReportPlanimetryConfigData data) {
      this.data = data;
      branche = (data instanceof ReportPlanimetryBrancheExtraData);
    }

    public JPanel getPanel() {
      if (panel == null) {
        createPanel();
      }
      return panel;
    }

    public AbstractReportPlanimetryConfigData apply() {
      final ReportMultiLabelContentUI labelUI = new ReportMultiLabelContentUI(availableVar);
      data.setLabels(labelUI.getValuesFromEditor(labelEditor));
      data.setLabelsIfSelected(labelUI.getValuesFromEditor(selectedLabelEditor));
      data.setUseLabels(cbUseLabel.isSelected());
      data.setSameIfSelected(!cbUseDifferentSelectedLabel.isSelected());
      data.setPaletteVariable((ReportVariableKey) cbSelectPaletteVar.getSelectedItem());
      paletteSelecteur.actionApply();//pour enregistrer les modifications
      data.setBPalettePlageProperties(reportPalettePlagetTarget.getPlage());
      data.setUsePalette(cbUsePalette.isSelected());
      if (branche) {
        final ReportPlanimetryBrancheExtraData brancheData = (ReportPlanimetryBrancheExtraData) data;
        final ReportVariableKey variable = (ReportVariableKey) cbSelectBrancheArrow.getSelectedItem();
        brancheData.setArrowVariable(variable == ReportVariableKey.EMPTY ? null : variable);
      }
      return data;
    }

    private void updateLabelsState() {
      final boolean editable = cbUseLabel.isSelected();
      final boolean selectedEditable = editable && cbUseDifferentSelectedLabel.isSelected();
      labelEditor.setEditable(editable);
      selectedLabelEditor.setEditable(selectedEditable);
      cbUseDifferentSelectedLabel.setEnabled(editable);
      updateEditableState(labelEditor.getExplorerManager(), editable);
      updateEditableState(selectedLabelEditor.getExplorerManager(), selectedEditable);
    }

    private void createPanel() {
      panel = new JXTaskPaneContainer();
      panel.setBorder(BuBorders.EMPTY3333);
      createLabelsPanels();
      createPalettePanel();
      if (branche) {
        createBranchePanel();
      }

    }

    private void updateEditableState(final ExplorerManager explorerManager, final boolean editable) {
      final Node[] nodes = explorerManager.getRootContext().getChildren().getNodes();
      for (final Node node : nodes) {
        ((ReportLabelContentNode) node).setEditable(editable);

      }
    }

    private void createLabelsPanels() {
      cbUseLabel = new JCheckBox(NbBundle.getMessage(ReportPlanimetryConfigDataUI.class, "UseLabelInView.Name"));
      cbUseLabel.setOpaque(false);
      cbUseDifferentSelectedLabel = new JCheckBox(NbBundle.getMessage(ReportPlanimetryConfigDataUI.class, "SelectedLabelsDifferent.Name"));
      cbUseDifferentSelectedLabel.setOpaque(false);
      cbUseLabel.setSelected(data.isUseLabels());
      cbUseDifferentSelectedLabel.setSelected(!data.isSameIfSelected());


      final JXTaskPane taskPane = new JXTaskPane(org.openide.util.NbBundle.getMessage(ReportPlanimetryConfigDataUI.class, "PanelConfigureLabel.Name"));
      final JXTaskPane taskPaneSelected = new JXTaskPane(org.openide.util.NbBundle.getMessage(ReportPlanimetryConfigDataUI.class, "PanelConfigureSelectedLabel.Name"));
      panel.add(taskPane);
      panel.add(taskPaneSelected);

      final ReportMultiLabelContentUI labelUI = new ReportMultiLabelContentUI(availableVar);
      labelEditor = labelUI.createEditor(data.getLabels());
      labelEditor.setOpaque(false);
      labelEditor.setPreferredSize(new Dimension(400, 100));
      selectedLabelEditor = labelUI.createEditor(data.getLabelsIfSelected());
      selectedLabelEditor.setOpaque(false);
      selectedLabelEditor.setPreferredSize(new Dimension(400, 100));

      taskPane.add(cbUseLabel);
      taskPane.setOpaque(false);
      taskPane.add(labelEditor);

      taskPaneSelected.add(cbUseDifferentSelectedLabel);
      taskPaneSelected.add(selectedLabelEditor);
      final UpdateActionListener actionListenerImpl = new UpdateActionListener();
      cbUseLabel.addActionListener(actionListenerImpl);
      cbUseDifferentSelectedLabel.addActionListener(actionListenerImpl);
      updateLabelsState();
    }

    private void createBranchePanel() {
      final JXTaskPane taskBranche = new JXTaskPane(org.openide.util.NbBundle.getMessage(ReportPlanimetryConfigDataUI.class, "PanelConfigureBranche.Name"));
      cbSelectBrancheArrow = new JComboBox();
      final List<ReportVariableKey> vars = new ArrayList<>();
      vars.add(ReportVariableKey.EMPTY);
      vars.addAll(availableVar);
      ComboBoxHelper.setDefaultModel(cbSelectBrancheArrow, vars);
      final ReportVariableKey arrowVariable = ((ReportPlanimetryBrancheExtraData) data).getArrowVariable();
      cbSelectBrancheArrow.setSelectedItem(arrowVariable == null ? ReportVariableKey.EMPTY : arrowVariable);
      cbSelectBrancheArrow.setRenderer(new ToStringInternationalizableCellRenderer());
      final JPanel pnSelectVar = new JPanel(new BorderLayout());
      pnSelectVar.setOpaque(false);
      pnSelectVar.add(new JLabel(org.openide.util.NbBundle.getMessage(ReportPlanimetryConfigDataUI.class, "VariableLabel.Name")), BorderLayout.WEST);
      pnSelectVar.add(cbSelectBrancheArrow);
      taskBranche.add(pnSelectVar);
      panel.add(taskBranche);

    }

    private void createPalettePanel() {
      final JXTaskPane taskColor = new JXTaskPane(org.openide.util.NbBundle.getMessage(ReportPlanimetryConfigDataUI.class, "PanelConfigurePalette.Name"));
      cbUsePalette = new JCheckBox(NbBundle.getMessage(ReportPlanimetryConfigDataUI.class, "UsePalette.Name"));
      cbUsePalette.setOpaque(false);
      taskColor.add(cbUsePalette);
      cbUsePalette.setSelected(data.isUsePalette());
      cbUsePalette.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          updatePaletteState();
        }
      });

      cbSelectPaletteVar = new JComboBox();
      ComboBoxHelper.setDefaultModel(cbSelectPaletteVar, availableVar);
      cbSelectPaletteVar.setSelectedItem(data.getPaletteVariable());
      cbSelectPaletteVar.setRenderer(new ToStringInternationalizableCellRenderer());

      final JPanel pnSelectVar = new JPanel(new BorderLayout());
      pnSelectVar.setOpaque(false);
      pnSelectVar.add(new JLabel(org.openide.util.NbBundle.getMessage(ReportPlanimetryConfigDataUI.class, "VariableLabel.Name")), BorderLayout.WEST);
      pnSelectVar.add(cbSelectPaletteVar);
      taskColor.add(pnSelectVar);

      paletteSelecteur = new PaletteSelecteurCouleurPlage();
      taskColor.add(paletteSelecteur);

      final ReportRunVariableKey runVariableKey = data.getPaletteVariable() == null ? null : new ReportRunVariableKey(reportRunKey, data.getPaletteVariable());
      reportPalettePlagetTarget = new ReportPalettePlagetTarget(runVariableKey, catEMH);
      reportPalettePlagetTarget.setEditable(cbSelectPaletteVar.getSelectedItem() != null);
      reportPalettePlagetTarget.setPaletteCouleurPlages(data.getPalette());
      paletteSelecteur.setTargetPalette(reportPalettePlagetTarget);
      paletteSelecteur.setEnabled(true);
      paletteSelecteur.setUseFormatter(true);
      paletteSelecteur.setTitlePanelVisible(false);
      paletteSelecteur.setTitlePanelVisible(false);
      paletteSelecteur.getBtApply().setVisible(false);
      paletteSelecteur.getBtFormat().setVisible(false);
      paletteSelecteur.setOpaque(false);
      panel.add(taskColor);

      cbSelectPaletteVar.addItemListener(e -> {
        if (e.getStateChange() == ItemEvent.SELECTED) {
          final Object selectedItem = cbSelectPaletteVar.getSelectedItem();
          if (selectedItem == null) {
            paletteSelecteur.setTargetPalette(null);
          } else {
            final ReportRunVariableKey newRunVariableKey = new ReportRunVariableKey(reportRunKey, (ReportVariableKey) selectedItem);
            reportPalettePlagetTarget = new ReportPalettePlagetTarget(newRunVariableKey, catEMH);
            reportPalettePlagetTarget.setEditable(true);
            reportPalettePlagetTarget.setPaletteCouleurPlages(reportPalettePlagetTarget.createPaletteCouleur());
            paletteSelecteur.setTargetPalette(reportPalettePlagetTarget);
          }
        }
      });
      updatePaletteState();
    }

    private void updatePaletteState() {
      final boolean editable = cbUsePalette.isSelected();
      cbSelectPaletteVar.setEnabled(editable);
      reportPalettePlagetTarget.setEditable(editable);
      final Object selectedItem = cbSelectPaletteVar.getSelectedItem();
      if (selectedItem == null) {
        paletteSelecteur.setTargetPalette(null);
      } else {
        paletteSelecteur.setTargetPalette(reportPalettePlagetTarget);
      }
    }

    private class UpdateActionListener implements ActionListener {

      public UpdateActionListener() {
      }

      @Override
      public void actionPerformed(final ActionEvent e) {
        updateLabelsState();
      }
    }
  }
}
