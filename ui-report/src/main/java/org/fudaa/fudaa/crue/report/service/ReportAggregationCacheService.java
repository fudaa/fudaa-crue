/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.service;

import org.apache.commons.collections4.map.LRUMap;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.projet.report.formule.function.AggregationCacheKey;
import org.fudaa.dodico.crue.projet.report.formule.function.ReportAggregationCacheServiceContrat;
import org.fudaa.fudaa.crue.common.services.PostRunService;
import org.openide.util.Lookup;
import org.openide.util.LookupListener;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;

/**
 * Un cache qui permet de stocker les valeurs min/max et de les supprimer sur modification du run ou des pas de temps.
 * Ne possède pas de lookup
 *
 * @author Frederic Deniger
 */
@ServiceProviders(value = {
        @ServiceProvider(service = ReportAggregationCacheService.class),
        @ServiceProvider(service = ReportAggregationCacheServiceContrat.class)})
public class ReportAggregationCacheService implements ReportAggregationCacheServiceContrat {
    private final PostRunService postRunService = Lookup.getDefault().lookup(PostRunService.class);
    private final Lookup.Result<EMHScenario> resultat;
    private final Lookup.Result<RangeSelectedTimeKey> resultatRange;
    final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
    private final LRUMap map = new LRUMap(100000);

    public ReportAggregationCacheService() {
        resultat = postRunService.getLookup().lookupResult(EMHScenario.class);
        resultatRange = reportService.getLookup().lookupResult(RangeSelectedTimeKey.class);
        final LookupListener lookupListener = ev -> map.clear();
        resultat.addLookupListener(lookupListener);
        resultatRange.addLookupListener(lookupListener);
    }

    /**
     * @param key la clé
     * @return la valeur mise en cache
     */
    @Override
    public Double getCachedValue(AggregationCacheKey key) {
        if (key.getStatKey().getTimeSelection().isCachable()) {
            return (Double) map.get(key);
        }
        return null;
    }

    /**
     * @param key la clé du cache
     * @param val la valeur à cacher
     */
    @Override
    public void putCachedValue(AggregationCacheKey key, Double val) {
        if (key.getStatKey().getTimeSelection().isCachable()) {
            map.put(key, val);
        }
    }
}
