/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import com.thoughtworks.xstream.XStream;
import java.io.File;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.projet.report.ReportMultiVarConfig;
import org.fudaa.dodico.crue.projet.report.ReportRPTGConfig;
import org.fudaa.dodico.crue.projet.report.ReportXstreamReaderWriter;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalConfig;
import org.fudaa.dodico.crue.projet.report.persist.AbstractReportConfigDao;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigLongitudinalDao;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigMultiVarDao;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigRPTGDao;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigTemporalDao;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigTransversalDao;
import org.fudaa.dodico.crue.projet.report.persist.ReportTemporalConfig;
import org.fudaa.dodico.crue.projet.report.persist.ReportTransversalConfig;
import org.fudaa.dodico.crue.projet.report.transformer.KeysToStringConverter;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.fudaa.fudaa.crue.report.persist.ReportConfigPlanimetryDao;
import org.fudaa.fudaa.crue.report.persist.ReportPlanimetryConfig;

/**
 *
 * @author Frederic Deniger
 */
public class ReportConfigSaver implements CrueDataDaoStructure {

  private final ReportConfigReader reader;

  public ReportConfigSaver(TraceToStringConverter traceToStringConverter, KeysToStringConverter keysToStringConverter) {
    reader = new ReportConfigReader(traceToStringConverter, keysToStringConverter);
  }

  public CtuluLog save(File targetFile, ReportConfigContrat config, String configName) {

    AbstractReportConfigDao dao = create(config);
    ReportXstreamReaderWriter<AbstractReportConfigDao> readerWriter = new ReportXstreamReaderWriter<>("report-vue",
            ReportXstreamReaderWriter.VERSION, this);
    CrueIOResu<AbstractReportConfigDao> res = new CrueIOResu<>(dao);
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc(configName);
    readerWriter.writeXMLMetier(res, targetFile, log, null);
    return log;
  }

  private AbstractReportConfigDao create(ReportConfigContrat config) {
    switch (config.getType()) {
      case TRANSVERSAL:
        return new ReportConfigTransversalDao((ReportTransversalConfig) config);
      case LONGITUDINAL:
        return new ReportConfigLongitudinalDao((ReportLongitudinalConfig) config);
      case MULTI_VAR:
        return new ReportConfigMultiVarDao((ReportMultiVarConfig) config);
      case RPTG:
        return new ReportConfigRPTGDao((ReportRPTGConfig) config);
      case TEMPORAL:
        return new ReportConfigTemporalDao((ReportTemporalConfig) config);
      case PLANIMETRY:
        return new ReportConfigPlanimetryDao((ReportPlanimetryConfig) config);
    }
    throw new IllegalAccessError("not supported");

  }

  @Override
  public void configureXStream(XStream xstream, CtuluLog ctuluLog, org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    reader.configureXStream(xstream, ctuluLog, props);
  }
}
