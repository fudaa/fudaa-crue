/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.time;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultOutlineViewEditor;
import org.openide.DialogDescriptor;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;

/**
 *
 * @author Frederic Deniger
 */
public class ReportTimeVideoRecorderEditor {

  public boolean edit(List<ReportTimeVideoRecorderItem> items) {
    List<Node> nodes = new ArrayList<>();
    for (ReportTimeVideoRecorderItem item : items) {
      nodes.add(new ReportTimeVideoRecorderItemNode(item));
    }
    Editor editor = new Editor();
    editor.getExplorerManager().setRootContext(NodeHelper.createNode(nodes, "videos"));
    DialogDescriptor descriptor = new DialogDescriptor(editor, org.openide.util.NbBundle.getMessage(ReportTimeVideoRecorderEditor.class,
            "ConfigurationVideos.DialogTitle"));
    SysdocUrlBuilder.installDialogHelpCtx(descriptor, "vueGestionnaireTemporel_ConfigurationExportVideo", PerspectiveEnum.REPORT, false);
    return DialogHelper.showQuestion(descriptor, getClass(), DialogHelper.NO_VERSION_FOR_PERSISTENCE);
  }

  private static class Editor extends DefaultOutlineViewEditor {

    @Override
    protected void addElement() {
    }

    @Override
    protected void initActions() {
    }

    @Override
    protected void createOutlineView() {
      view = new OutlineView(StringUtils.EMPTY);
      view.setPropertyColumns(ReportTimeVideoRecorderItemNode.PROP_VIDEO_CONFIG,
              ReportTimeVideoRecorderItemNode.getVideoConfigi18n(),
              ReportTimeVideoRecorderItemNode.PROP_VIDEO_CONFIG_ACTIVE,
              ReportTimeVideoRecorderItemNode.getPropVideoConfigActivedi18n());
      view.getOutline().setColumnHidingAllowed(false);
      view.getOutline().setRootVisible(false);
      view.setDragSource(false);
      view.setDropTarget(false);
    }
  }
}
