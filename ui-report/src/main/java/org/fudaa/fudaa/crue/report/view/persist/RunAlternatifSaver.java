/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import java.io.File;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.common.LocalDateTimeConverter;
import org.fudaa.dodico.crue.projet.report.ReportXstreamReaderWriter;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.transformer.ReportRunKeySimpleToStringTransformer;
import org.fudaa.ebli.converter.ToStringTransfomerXstreamConverter;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class RunAlternatifSaver implements CrueDataDaoStructure {

  final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);


  public CtuluLog save(final File targetFile, final List<ReportRunKey> runKeys) {
    final RunAlternatifDao dao = new RunAlternatifDao();
    final ConnexionInformation connexionInformation = configurationManagerService.getConnexionInformation();
    dao.setAuteurDerniereModif(connexionInformation.getCurrentUser());
    dao.setDateDerniereModif(connexionInformation.getCurrentDate());
    dao.setRunAlternatifs(runKeys);
    final ReportXstreamReaderWriter<RunAlternatifDao> readerWriter = new ReportXstreamReaderWriter<>("report-runs", ReportXstreamReaderWriter.VERSION, this);
    final CrueIOResu<RunAlternatifDao> res = new CrueIOResu<>(dao);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc(org.openide.util.NbBundle.getMessage(RunAlternatifSaver.class, "SaveRuns.LogDisplayName"));
    readerWriter.writeXMLMetier(res, targetFile, log, null);
    return log;
  }

  public CrueIOResu<List<ReportRunKey>> read(final File targetFile) {
    final ReportXstreamReaderWriter<RunAlternatifDao> readerWriter = new ReportXstreamReaderWriter<>("report-runs", ReportXstreamReaderWriter.VERSION, this);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    final RunAlternatifDao readDao = readerWriter.readDao(targetFile, log, null);
    final CrueIOResu<List<ReportRunKey>> resuFinal = new CrueIOResu<>();
    resuFinal.setAnalyse(log);
    resuFinal.setMetier(readDao.getRunAlternatifs());
    return resuFinal;
  }

  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    xstream.processAnnotations(RunAlternatifDao.class);
    xstream.registerConverter(new LocalDateTimeConverter());
    final ReportRunKeySimpleToStringTransformer transformer = new ReportRunKeySimpleToStringTransformer(null);
    xstream.registerConverter(new ToStringTransfomerXstreamConverter(transformer, transformer.getSupportedClass()));
  }
}
