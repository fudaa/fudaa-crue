/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.export;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.fudaa.crue.common.action.AbstractNodeAction;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportExportNodeAction extends AbstractNodeAction {

  public ReportExportNodeAction() {
    super(NbBundle.getMessage(ReportExportNodeAction.class, "ReportExportNodeAction.DisplayName"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return ArrayUtils.isNotEmpty(activatedNodes);
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    List<EMH> emhs = new ArrayList<>();
    for (Node node : activatedNodes) {
      EMH emh = node.getLookup().lookup(EMH.class);
      if (emh != null) {
        emhs.add(emh);
      }
    }
    new ReportExportHelper().displayDialogExport(emhs);

  }
}
