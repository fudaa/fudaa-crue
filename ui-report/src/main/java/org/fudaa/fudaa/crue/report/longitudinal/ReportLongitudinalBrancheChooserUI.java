/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.longitudinal;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluDefaultLogFormatter;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBrancheConfig;
import org.fudaa.fudaa.crue.common.view.CustomNodePopupFactory;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.common.view.DefaultOutlineViewEditor;
import org.openide.DialogDescriptor;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLongitudinalBrancheChooserUI extends DefaultOutlineViewEditor {

  public ReportLongitudinalBrancheChooserUI() {
  }
  DialogDescriptor descriptor;

  public void setDescriptor(final DialogDescriptor descriptor) {
    this.descriptor = descriptor;
  }

  @Override
  protected void addElement() {
    final ReportLongitudinalBrancheConfig elem = new ReportLongitudinalBrancheConfig();
    final Children children = getExplorerManager().getRootContext().getChildren();
    final ReportLongitudinalBrancheConfigNode elemPdtNode = new ReportLongitudinalBrancheConfigNode(elem);
    children.add(new Node[]{elemPdtNode});
    DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
  }

  @Override
  protected void changeInNode(final String propertyName) {
    validData();
  }

  protected void validData() {
    final CtuluLog error = new CtuluLog();
    final Children children = getExplorerManager().getRootContext().getChildren();
    final Node[] nodes = children.getNodes();
    final Set<String> usedBranche = new HashSet<>();
    int idx = 1;
    for (final Node node : nodes) {
      final ReportLongitudinalBrancheConfig config = node.getLookup().lookup(ReportLongitudinalBrancheConfig.class);
      final String name = config.getName();
      if (StringUtils.isBlank(name)) {
        error.addError(NbBundle.getMessage(ReportLongitudinalBrancheChooserUI.class, "BrancheConfig.BrancheNameIsEmpty.error", idx));
      } else {
        if (usedBranche.contains(name)) {
          error.addError(NbBundle.getMessage(ReportLongitudinalBrancheChooserUI.class, "BrancheConfig.BrancheNameAlreadyUsed.error"), idx, name);
        }
        usedBranche.add(name);
      }
      final double length = config.getLength();
      if (length < 0) {
        error.addError(NbBundle.getMessage(ReportLongitudinalBrancheChooserUI.class, "BrancheConfig.BrancheLengthNegativeOrNull.error"), idx, name);

      }
      idx++;
    }

    if (descriptor != null) {
      descriptor.getNotificationLineSupport().setErrorMessage(StringUtils.EMPTY);
      if (error.isNotEmpty()) {
        String asHtml = CtuluDefaultLogFormatter.asHtml(error);
        if (asHtml.indexOf("<br>") > 0) {
          asHtml = StringUtils.substringBefore(asHtml, "<br>");
        }
        descriptor.getNotificationLineSupport().setErrorMessage(asHtml);
      }
      descriptor.setValid(error.isEmpty());
    }
  }

  @Override
  public OutlineView getView() {
    return view;
  }

  @Override
  protected void createOutlineView() {
    super.view = new OutlineView("");
    view.getOutline().setColumnHidingAllowed(false);
    view.getOutline().setRootVisible(false);
    final List<String> columns = new ArrayList<>();
    columns.add(ReportLongitudinalBrancheConfig.PROP_NAME);
    columns.add(NbBundle.getMessage(ReportLongitudinalBrancheChooserUI.class, "name.property"));
    columns.add(ReportLongitudinalBrancheConfig.PROP_SENS);
    columns.add(NbBundle.getMessage(ReportLongitudinalBrancheChooserUI.class, "sens.property"));
    columns.add(ReportLongitudinalBrancheConfig.PROP_LENGTH);
    columns.add(NbBundle.getMessage(ReportLongitudinalBrancheChooserUI.class, "length.property"));
    columns.add(ReportLongitudinalBrancheConfig.PROP_DECAL);
    columns.add(NbBundle.getMessage(ReportLongitudinalBrancheChooserUI.class, "decalXp.property"));
    columns.add(ReportLongitudinalBrancheConfig.PROP_LENGTH_HYD);
    columns.add(NbBundle.getMessage(ReportLongitudinalBrancheChooserUI.class, "lengthHyd.property"));
    view.setPropertyColumns(columns.toArray(new String[0]));
    view.setNodePopupFactory(new CustomNodePopupFactory());
  }
}
