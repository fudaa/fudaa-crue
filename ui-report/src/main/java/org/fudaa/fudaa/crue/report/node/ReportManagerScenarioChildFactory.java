package org.fudaa.fudaa.crue.report.node;

import java.util.List;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;

/**
 *
 * @author Frederic Deniger
 */
public class ReportManagerScenarioChildFactory extends ChildFactory<ManagerEMHScenario> {

  private final EMHProjet projet;

  public ReportManagerScenarioChildFactory(EMHProjet projet) {
    this.projet = projet;
  }

  @Override
  protected boolean createKeys(List<ManagerEMHScenario> toPopulate) {
    List<ManagerEMHScenario> listeScenarios = projet.getListeScenarios();
    boolean res = false;
    for (ManagerEMHScenario managerEMHScenario : listeScenarios) {
      if (managerEMHScenario.hasRun()) {
        toPopulate.add(managerEMHScenario);
        res = true;
      }
    }
    return res;
  }

  @Override
  protected Node createNodeForKey(ManagerEMHScenario key) {
    return new ReportManagerScenarioNode(key);
  }
}
