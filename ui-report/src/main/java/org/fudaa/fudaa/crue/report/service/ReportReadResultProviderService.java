/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.service;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.metier.result.TimeSimuConverter;
import org.fudaa.dodico.crue.projet.report.data.ReportRunContent;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.fudaa.crue.report.contrat.ReportReadResultServiceContrat;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;

import java.io.IOException;
import java.util.Map;

/**
 * Service permettant d'accéder aux résulats du run courent
 * Ne contient pas de lookup.
 *
 * @author Frederic Deniger
 */
@ServiceProviders(value = {
        @ServiceProvider(service = ReportReadResultProviderService.class),
        @ServiceProvider(service = ReportReadResultServiceContrat.class)})
public class ReportReadResultProviderService implements ReportReadResultServiceContrat {
    private final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
    private final ReportRunAlternatifService reportRunAlternatifService = Lookup.getDefault().lookup(ReportRunAlternatifService.class);

    /**
     * @return le service donnant les runs alternatifs chargés
     */
    public ReportRunAlternatifService getReportRunAlternatifService() {
        return reportRunAlternatifService;
    }

    /**
     * @return le service princiapl
     */
    public ReportService getReportService() {
        return reportService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CrueConfigMetier getCcm() {
        return reportService.getCcm();
    }

    /**
     * @param key la clé qui peut être le run principal ou un run alternatif
     * @return contenu complet du run
     */
    private ReportRunContent getRunContent(ReportRunKey key) {
        if (key.isCourant()) {
            return reportService.getRunCourant();
        }
        return reportRunAlternatifService.getRunContentLoaded(key);
    }

    /**
     * @param nom nom de l'EMH
     * @param key le run parent (donc scenario)
     * @return l'EMH demandé
     */
    public EMH getEMH(String nom, ReportRunKey key) {
        ReportRunContent runContent = getRunContent(key);
        if (runContent == null) {
            return null;
        }
        return runContent.getEMH(nom);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Double getReadValue(ReportRunVariableKey key, String emhNom) {
        ResultatTimeKey selectedTime = reportService.getSelectedTime();
        return getReadValue(selectedTime, key, emhNom);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Double getReadValue(ResultatTimeKey selectedTime, ReportRunVariableKey key, String emhNom) {
        if (selectedTime == null) {
            return null;
        }
        EMH emh = getEMH(emhNom, key.getRunKey());
        if (emh == null || emh.getResultatCalcul() == null) {
            return null;
        }
        try {
            ResultatTimeKey toUse = getInitialTimeKey(selectedTime, key.getReportRunKey(), emh);
            if (toUse == null) {
                return null;
            }
            final Map<String, Object> read = emh.getResultatCalcul().read(toUse);
            if (read == null) {
                return null;
            }
            return (Double) read.get(key.getVariable().getVariableName());
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        return null;
    }

    /**
     * @param timeKey le pas de temps demandé
     * @param key     la clé du run
     * @param emh     le nom de l'emh en question
     * @return le pas de temps équivalent pour les runs alternatifs
     * @see #getInitialTimeKey(ResultatTimeKey, ReportRunKey, EMH)
     */
    public ResultatTimeKey getInitialTimeKey(ResultatTimeKey timeKey, ReportRunKey key, String emh) {
        return getInitialTimeKey(timeKey, key, getEMH(emh, key));
    }

    /**
     * @param timeKey le pas de temps demandé
     * @param key     la clé du run
     * @param emh     l'emh en question
     * @return le pas de temps équivalent pour les runs alternatifs
     * @see TimeSimuConverter
     * @see TimeSimuConverter#getEquivalentTempsSimuRunAlternatif(ResultatTimeKey, EMHModeleBase)
     */
    private ResultatTimeKey getInitialTimeKey(ResultatTimeKey timeKey, ReportRunKey key, EMH emh) {
        ResultatTimeKey toUse = timeKey;
        //pour les runs non courants, on doit chercher l'équivalent
        if (!key.isCourant()) {
            ReportRunContent runContent = getRunContent(key);
            TimeSimuConverter tempSimuConverter = TimeSimuConverter.create(getReportService().getRunCourant().getScenario(), runContent.getScenario());
            final EMHSousModele emhSousModele = EMHHelper.getParent(emh);
            if (emhSousModele == null) {
                return toUse;
            }
            EMHModeleBase parent = emhSousModele.getParent();
            toUse = tempSimuConverter.getEquivalentTempsSimuRunAlternatif(toUse, parent);
        }
        return toUse;
    }
}
