/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.rptg;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.emh.ResPrtGeo;
import org.fudaa.dodico.crue.projet.otfa.OtfaReportExecutorDelegateRPTG;
import org.fudaa.dodico.crue.projet.report.ReportRPTGConfig;
import org.fudaa.dodico.crue.projet.report.data.ReportRPTGCourbeKey;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.crue.loi.ViewCourbeManager;
import org.fudaa.fudaa.crue.loi.common.CourbesUiController;
import org.fudaa.fudaa.crue.loi.loiff.SimpleLoiCourbeModel;
import org.fudaa.fudaa.crue.report.config.AbstractReportGrapheBuilder;
import org.netbeans.api.progress.ProgressHandle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportRPTGGrapheBuilder extends AbstractReportGrapheBuilder<CourbesUiController, ReportRPTGConfig> {

  public ReportRPTGGrapheBuilder(final CourbesUiController uiController, final ViewCourbeManager loiLabelsManager) {
    super(uiController, loiLabelsManager);
  }

  @Override
  public void applyLabelsConfigChanged(final ReportRPTGConfig content) {
    super.applyLabelsConfigChanged(content);
  }

  @Override
  public void updateResultCurvesAfterTimeChanged(final ReportRPTGConfig content) {
  }

  @Override
  public List<EGCourbeSimple> getInternCourbesAfterTimeChanged(final ReportRPTGConfig content, final ProgressHandle progress) {
    return Collections.emptyList();
  }

  @Override
  public EGAxeVertical getOrCreateAxeVertical(final ItemVariable variable) {
    return super.getOrCreateAxeVertical(variable);
  }

  List<EGCourbeSimple> createModels(final ReportRPTGConfig config, final EMH emh, final CrueConfigMetier ccm,
                                    final boolean main, final Map<String, EGAxeVertical> axesY, final ProgressHandle progress) {
    final List<EGCourbeSimple> courbe = new ArrayList<>();
    if (emh == null) {
      return courbe;
    }
    final ResPrtGeo rptg = emh.getRPTG();
    if (rptg == null) {
      return Collections.emptyList();
    }
    final List<LoiFF> lois = rptg.getLois();
    final String var1 = config.getVar1();
    if (var1 != null) {
      if (progress != null) {
        progress.setDisplayName(var1 + ": " + emh.getNom());
      }
      final EGCourbeSimple createCourbe = createCourbe(lois, config, emh.getNom(), var1, ccm, main, axesY);
      if (createCourbe != null) {
        courbe.add(createCourbe);
      }
      if (config.getVar2() != null) {
        if (progress != null) {
          progress.setDisplayName(config.getVar2() + ": " + emh.getNom());
        }
        final EGCourbeSimple createCourbe2 = createCourbe(lois, config, emh.getNom(), config.getVar2(), ccm, main, axesY);
        if (createCourbe2 != null) {
          courbe.add(createCourbe2);
        }
      }
    }

    return courbe;
  }

  @Override
  public List<EGCourbeSimple> getInternCourbes(final ReportRPTGConfig content, final ProgressHandle progress) {
    final String varHorizontal = content.getVarHorizontal();
    final PropertyNature property = reportResultProviderService.getPropertyNature(varHorizontal);
    uiController.configureAxeH(property, true);

    final List<EGCourbeSimple> courbes = new ArrayList<>();
    if (reportService.getRunCourant() == null) {
      return courbes;
    }
    final EMH emh = reportService.getRunCourant().getEMH(content.getEmh());
    final List<PropertyNature> variables = new ArrayList<>();
    if (content.getVar1() != null) {
      variables.add(reportResultProviderService.getPropertyNature(content.getVar1()));
    }
    if (content.getVar2() != null) {
      variables.add(reportResultProviderService.getPropertyNature(content.getVar2()));
    }
    final Map<String, EGAxeVertical> axesYByNatureNom = getOrCreateAxesVerticalConfigured(content, variables);
    courbes.addAll(createModels(content, emh, reportService.getCcm(), true, axesYByNatureNom, progress));
    if (content.getPrevious() != null) {
      final EMH emhPrevious = reportService.getRunCourant().getEMH(content.getPrevious());
      courbes.addAll(createModels(content, emhPrevious, reportService.getCcm(), false, axesYByNatureNom, progress));
    }
    super.uiController.getEGGrapheSimpleModel().removeAllCurves(null);
    super.uiController.getEGGrapheSimpleModel().addCourbes(courbes.toArray(new EGCourbeSimple[0]), null);
    return courbes;
  }

  EGCourbePersist getOrCreateConfig(final ReportRPTGCourbeKey key, final ReportRPTGConfig config) {
    EGCourbePersist ui = config.getCourbeconfigs().get(key);
    if (ui == null) {
      ui = new EGCourbePersist();
      if (!key.isMain()) {
        ui.setLineModel(new TraceLigneModel(TraceLigne.POINTILLE, 1f, Color.LIGHT_GRAY));
      }
    }
    return ui;
  }

  protected EGCourbeSimple createCourbe(final List<LoiFF> lois, final ReportRPTGConfig config, final String emhName, final String var1, final CrueConfigMetier ccm,
                                        final boolean main, final Map<String, EGAxeVertical> axesY) {
    final LoiFF loi1 = OtfaReportExecutorDelegateRPTG.findLoi(lois, config.getVarHorizontal(), var1, ccm);
    if (loi1 != null) {
      final PropertyNature property1 = reportResultProviderService.getPropertyNature(var1);
      final ItemVariable varX = reportResultProviderService.getCcmVariable(config.getVarHorizontal());
      final ItemVariable varY = reportResultProviderService.getCcmVariable(var1);
      final SimpleLoiCourbeModel model = new SimpleLoiCourbeModel(loi1, varX, varY);
      final ReportRPTGCourbeKey key = new ReportRPTGCourbeKey(var1, main);
      model.setKey(key);
      model.setTitle(reportResultProviderService.getVariableName(var1) + BusinessMessages.ENTITY_SEPARATOR + emhName);
      final EGAxeVertical axeY = axesY.get(property1.getNom());
      final EGCourbeSimple courbe = new EGCourbeSimple(axeY, model);
//      boolean defined = config.courbeconfigs.containsKey(key);
//      EGCourbePersist configUI = getOrCreateConfig(key, config);
//      if (!defined) {
//        if (configUI.getLineModel_() == null) {
//          configUI.setLineModel(new TraceLigneModel());
//        }
//      }
      AbstractReportGrapheBuilder.applyPersistConfig(config.getCourbeconfigs().get(key), courbe);
      return courbe;
    }
    return null;
  }
}
