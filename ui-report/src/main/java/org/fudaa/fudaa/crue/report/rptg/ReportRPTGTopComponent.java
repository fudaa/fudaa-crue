package org.fudaa.fudaa.crue.report.rptg;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.projet.report.ReportRPTGConfig;
import org.fudaa.dodico.crue.projet.report.data.ReportExpressionHelper;
import org.fudaa.dodico.crue.projet.report.data.ReportKeyContract;
import org.fudaa.dodico.crue.projet.report.data.ReportRPTGCourbeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.loi.res.CourbesUiResController;
import org.fudaa.fudaa.crue.report.AbstractReportTimeViewTopComponent;
import org.fudaa.fudaa.crue.report.helper.ChooseEMHByTypeHelper;
import org.fudaa.fudaa.crue.report.service.ReportHelper;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.report//ReportRPTGTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = ReportRPTGTopComponent.TOPCOMPONENT_ID,
        persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ReportRPTGTopComponent.MODE, openAtStartup = false, position = 1)
public final class ReportRPTGTopComponent extends AbstractReportTimeViewTopComponent<ReportRPTGConfig, ReportRPTGGrapheBuilder> {

  public static final String MODE = "report-rptg";
  public static final String TOPCOMPONENT_ID = "ReportRPTGTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;

  /**
   * Contient toutes les données qui vont bien
   */
  public ReportRPTGTopComponent() {
    setName(NbBundle.getMessage(ReportRPTGTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ReportRPTGTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
  }

  @Override
  protected ReportRPTGGrapheBuilder createBuilder() {
    return new ReportRPTGGrapheBuilder(profilUiController, loiLegendManager);
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueRPTG", PerspectiveEnum.REPORT);
  }

  @Override
  protected void buildComponents() {
    initComponents();
    final JPanel pnTable = new JPanel(new BorderLayout(5, 5));
    profilUiController.installComboxSelector();
    pnTable.add(profilUiController.getTableGraphePanel());
    final JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, loiLegendManager.getPanel(), pnTable);
    splitPane.setDividerLocation(550);
    splitPane.setContinuousLayout(true);
    splitPane.setOneTouchExpandable(true);
    add(splitPane);
  }

  @Override
  protected CourbesUiResController createCourbesUiController() {
    return new CourbesUiResController();
  }

  @Override
  public List<EbliActionInterface> getMainActions() {
    final EbliActionSimple chooseSection = new EbliActionSimple(NbBundle.getMessage(ReportRPTGTopComponent.class, "ChooseEMH.ButtonName"), null, "SECTIONS") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        chooseEMH();
      }
    };
    final EbliActionSimple previous = new EbliActionSimple(NbBundle.getMessage(ReportRPTGTopComponent.class, "ChoosePrevious.ButtonName"), ReportHelper.
            getIcon("navigation_previous.png"), "PREVIOUS") {
              @Override
              public void actionPerformed(final ActionEvent _e) {
                choosePrevious();
              }
            };
    final EbliActionSimple next = new EbliActionSimple(NbBundle.getMessage(ReportRPTGTopComponent.class, "ChooseNext.ButtonName"), ReportHelper.getIcon(
            "navigation_next.png"), "NEXT") {
              @Override
              public void actionPerformed(final ActionEvent _e) {
                chooseNext();
              }
            };
    final EbliActionSimple chooseVariables = new EbliActionSimple(NbBundle.getMessage(ReportRPTGTopComponent.class, "ChooseVariable.ButtonName"), null,
            "VARIABLES") {
              @Override
              public void actionPerformed(final ActionEvent _e) {
                chooseVariable();
              }
            };
    return Arrays.asList(chooseSection, previous, next, chooseVariables, createConfigExternAction());

  }

  @Override
  public void setEditable(final boolean b) {
    super.setEditable(b);
  }

  @Override
  public void alternatifRunChanged() {
  }

  protected void chooseEMH() {
    final String selectedEMH = new ChooseEMHByTypeHelper().chooseUniqueEMH(content.getEmh());
    if (selectedEMH != null) {
      setSelectedEMH(selectedEMH);
    }
  }

  public void choose(final int inc) {

    final EMH emh = reportService.getRunCourant().getEMH(content.getEmh());
    if (emh == null) {
      return;
    }
    final List<EMH> allSimpleEMH = reportService.getModele().getAllSimpleEMH();
    final List<EMH> selectEMHS = EMHHelper.selectEMHS(allSimpleEMH, emh.getCatType());
    if (selectEMHS.size() > 1) {
      final List<String> toNom = TransformerHelper.toNom(selectEMHS);
      int idx = toNom.indexOf(emh.getNom()) + inc;
      final int size = toNom.size();
      if (idx < 0) {
        idx = size - 1;
      }
      if (idx >= size) {
        idx = 0;
      }
      setSelectedEMH(toNom.get(idx));
    }
  }

  protected void chooseNext() {
    choose(1);
  }

  protected void choosePrevious() {
    choose(-1);
  }

  public void chooseVariable() {
    final boolean changed = new ReportRPTGChooseVariableUI(this).chooseVariable(content);
    if (changed) {
      propagateChange();
      profilUiController.getGraphe().restore();
    }
  }

  @Override
  protected void saveCourbeConfig(final EGCourbePersist persitUiConfig, final ReportKeyContract key) {
    content.getCourbeconfigs().put((ReportRPTGCourbeKey) key, persitUiConfig);
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables

  @Override
  protected void componentOpenedHandler() {
  }

  @Override
  protected void componentClosedDefinitlyHandler() {
  }

  void writeProperties(final java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
    p.setProperty("version", "1.0");
  }

  void readProperties(final java.util.Properties p) {
  }

  @Override
  public List<ReportVariableKey> getTitleAvailableVariables() {
    final List<ReportVariableKey> res = new ArrayList<>();
    res.add(new ReportVariableKey(ReportVariableTypeEnum.EXPR, ReportExpressionHelper.EXPR_NOM));
    res.add(new ReportVariableKey(ReportVariableTypeEnum.EXPR, ReportExpressionHelper.EXPR_COMMENTAIRE));
    res.add(new ReportVariableKey(ReportVariableTypeEnum.EXPR, ReportExpressionHelper.EXPR_PROFIL_COMMENTAIRE));
    res.add(new ReportVariableKey(ReportVariableTypeEnum.EXPR, ReportExpressionHelper.EXPR_PROFIL_NOM));
    return res;
  }

  @Override
  public ReportRPTGConfig getReportConfig() {
    return content;
  }

  protected void setSelectedEMH(final String newEMH) {
    if (newEMH != null) {
      content.setPrevious(content.getEmh());
      content.setEmh(newEMH);
      propagateChange();
    }
  }
}
