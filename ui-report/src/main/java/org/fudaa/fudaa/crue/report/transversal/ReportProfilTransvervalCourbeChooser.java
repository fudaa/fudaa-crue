/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.transversal;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.metier.emh.OrdResScenario;
import org.fudaa.dodico.crue.metier.emh.OrdResSection;
import org.fudaa.dodico.crue.metier.result.OrdResExtractor;
import org.fudaa.dodico.crue.metier.result.OrdResVariableSelection;
import org.fudaa.dodico.crue.projet.report.data.ReportRunContent;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.persist.ReportTransversalConfig;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.loi.common.AbstractLoiUiController;
import org.fudaa.fudaa.crue.report.helper.ReportDefaultVariableChooserUI;
import org.fudaa.fudaa.crue.report.ordres.ReportOrdResSelectionByRun;
import org.fudaa.fudaa.crue.report.ordres.ReportOrdResSelectionByRunLine;
import org.fudaa.fudaa.crue.report.ordres.ReportOrdResSelectionByRunLineNode;
import org.fudaa.fudaa.crue.report.service.ReportFormuleService;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.fudaa.fudaa.crue.report.service.ReportRunAlternatifService;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportProfilTransvervalCourbeChooser {

  final ReportTransversalConfig content;
  final ReportResultProviderService reportResultProviderService = Lookup.getDefault().lookup(ReportResultProviderService.class);
  final ReportFormuleService reportFormuleService = Lookup.getDefault().lookup(ReportFormuleService.class);

  public ReportProfilTransvervalCourbeChooser(ReportTransversalConfig content) {
    this.content = content;
  }

  public boolean displayChooseUI() {
    ReportDefaultVariableChooserUI ui = new ReportDefaultVariableChooserUI(reportResultProviderService);
    final List<ReportOrdResSelectionByRunLineNode> nodes = getNodes();
    ui.setNodes(nodes);
    boolean ok = DialogHelper.showQuestionOkCancel(NbBundle.getMessage(ReportProfilTransvervalCourbeChooser.class, "ChooseVariable.DialogTitle"), ui);
    if (ok) {
      content.getProfilXtZ().clear();
      content.getProfilVariables().clear();
      for (ReportOrdResSelectionByRunLineNode node : nodes) { //pour respecter l'ordre
        ReportOrdResSelectionByRunLine line = node.getLine();
        String variable = line.getVariable();
        ReportVariableKey variableKey = reportResultProviderService.createVariableKey(variable);
        List<ReportOrdResSelectionByRun> selectionByRuns = line.getSelectionByRun();
        final boolean isXtZ = AbstractLoiUiController.PROP_Z_XT.equals(variable);
        for (ReportOrdResSelectionByRun byRun : selectionByRuns) {
          if (byRun.getSelection().isVariableSelected(variable)) {
            if (isXtZ && !byRun.getRunKey().isCourant()) {
              content.getProfilXtZ().add(byRun.getRunKey());
            } else {
              content.getProfilVariables().add(new ReportRunVariableKey(byRun.getRunKey(), variableKey));
            }
          }
        }
      }
    }
    return ok;
  }

  protected OrdResVariableSelection getSectionWithXtZ(ReportRunKey key) {
    return addXtZ(getSection(key));

  }

  protected OrdResVariableSelection getSection(ReportRunKey key) {
    ReportRunContent runContent = reportResultProviderService.getRunContent(key);
    OrdResScenario ordResScenario = runContent.getScenario().getOrdResScenario();
    OrdResExtractor extractor = new OrdResExtractor(ordResScenario);
    return extractor.getOrdResSelection(OrdResSection.KEY, reportFormuleService.getVariablesKeys());
  }

  protected OrdResVariableSelection addXtZ(OrdResVariableSelection in) {
    List<String> selectableVariables = new ArrayList<>();
    List<String> allVariables = new ArrayList<>();
    selectableVariables.add(AbstractLoiUiController.PROP_Z_XT);
    allVariables.add(AbstractLoiUiController.PROP_Z_XT);
    selectableVariables.addAll(in.getSelectableVariables());
    allVariables.addAll(in.getAllVariable());
    return new OrdResVariableSelection(in.getKey(), selectableVariables, allVariables);
  }

  public List<ReportOrdResSelectionByRunLineNode> getNodes() {
    ReportRunContent runCourant = reportResultProviderService.getReportService().getRunCourant();
    List<String> yOrZVariables = retrieveZYVariables(runCourant);
    List<ReportOrdResSelectionByRun> selectionByRunList = createSelectionByRun(runCourant);
    //on met à jour la sélection.
    for (ReportOrdResSelectionByRun selection : selectionByRunList) {
      if (content.isProfilDrawn(selection.getRunKey())) {
        selection.getSelection().setVariableSelected(AbstractLoiUiController.PROP_Z_XT, true);
      }
      List<String> variableToSelect = content.getVariablesDrawn(selection.getRunKey());
      for (String string : variableToSelect) {
        selection.getSelection().setVariableSelected(string, true);
      }

    }
    //les lignes
    List<ReportOrdResSelectionByRunLineNode> nodes = new ArrayList<>();
    for (String var : yOrZVariables) {
      ReportOrdResSelectionByRunLine line = new ReportOrdResSelectionByRunLine();
      line.setKey(OrdResSection.KEY);
      line.setVariable(var);
      line.setSelectionByRun(selectionByRunList);
      ReportOrdResSelectionByRunLineNode node = new ReportOrdResSelectionByRunLineNode(line);
      nodes.add(node);
    }
    return nodes;

  }

  public List<String> retrieveZYVariables(ReportRunContent runCourant) {
    OrdResVariableSelection section = getSection(runCourant.getRunKey());
    List<String> selectableVariables = section.getSelectableVariables();
    //seul les z et y sont important.
    List<String> zVariables = new ArrayList<>();
    zVariables.add(AbstractLoiUiController.PROP_Z_XT);//pour les profils
    for (String variableName : selectableVariables) {
      final PropertyNature itemVariable = reportResultProviderService.getPropertyNature(variableName);
      final String natureName = itemVariable == null ? null : itemVariable.getNom();
      if (CrueConfigMetierConstants.NAT_Z.equals(natureName)) {
        zVariables.add(variableName);
      }
    }
    return zVariables;
  }

  public List<ReportOrdResSelectionByRun> createSelectionByRun(ReportRunContent runCourant) {
    List<ReportOrdResSelectionByRun> selectionByRunList = new ArrayList<>();
    //pour le run courant
    ReportOrdResSelectionByRun currentRun = new ReportOrdResSelectionByRun(runCourant.getRunKey(), getSectionWithXtZ(runCourant.getRunKey()));
    currentRun.addNonEditableVar(AbstractLoiUiController.PROP_Z_XT);
    selectionByRunList.add(currentRun);
    ReportRunAlternatifService reportRunAlternatifService = reportResultProviderService.getReportRunAlternatifService();
    //pour les runs alternatifs:
    List<ReportRunKey> sortedReportRunContentKeys = reportRunAlternatifService.getSortedReportRunContentKeys();
    for (ReportRunKey reportRunKey : sortedReportRunContentKeys) {
      final ReportOrdResSelectionByRun selection = new ReportOrdResSelectionByRun(reportRunKey, getSectionWithXtZ(reportRunKey));
      selectionByRunList.add(selection);
    }
    return selectionByRunList;
  }
}
