/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.export;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.fudaa.dodico.crue.projet.report.data.ReportRunEmhKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.export.ReportExportAbstract;
import org.fudaa.dodico.crue.projet.report.export.ReportExportMultiVar;
import org.fudaa.fudaa.crue.report.multivar.ReportMultiVarTopComponent;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;

/**
 *
 * @author Frederic Deniger
 */
public class ReportMulitVarExportAction extends ReportCourbeExportActionAbstract<ReportMultiVarTopComponent> {

  public ReportMulitVarExportAction(ReportMultiVarTopComponent topComponent) {
    super(topComponent);
  }

  @Override
  protected ReportExportAbstract createReportExporter(ReportResultProviderService reportResultProviderService) {
    return new ReportExportMultiVar(topComponent.getReportConfig(), reportResultProviderService);
  }

  @Override
  protected List<String> chooseVariables() {
    final List<String> chooseVariables = super.chooseVariables();
    if (!chooseVariables.isEmpty()) {
      chooseVariables.add(0, topComponent.getReportConfig().getHorizontalVar());
    }
    return chooseVariables;
  }

  @Override
  protected List<String> getChoosableVariables() {
    final List<String> choosableVariables = new ArrayList<>(topComponent.getChoosableVariables());
    choosableVariables.remove(null);
    choosableVariables.remove(topComponent.getReportConfig().getHorizontalVar());
    return choosableVariables;
  }

  @Override
  protected List<String> getEMHs() {
    return topComponent.
            getReportConfig().getEmhs();
  }

  @Override
  protected Set<String> getCurrentSelectedVariables() {
    return new HashSet<>(topComponent.getReportConfig().getVariables());
  }

  @Override
  protected void fillWithSelectedRunKeys(Set<ReportRunKey> keys) {
    final Set<ReportRunEmhKey> variablesMap = topComponent.getReportConfig().getReportRunEmhs();
    for (ReportRunEmhKey reportRunEmhKey : variablesMap) {
      keys.add(reportRunEmhKey.getReportRunKey());
    }
  }

}
