package org.fudaa.fudaa.crue.report.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import javax.swing.Icon;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.fudaa.fudaa.crue.report.service.ReportRunAlternatifService;
import org.fudaa.fudaa.crue.report.service.ReportSaver;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.awt.Toolbar;
import org.openide.awt.ToolbarPool;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
id = "org.fudaa.fudaa.crue.report.actions.ReportSaveAction")
@ActionRegistration(displayName = "#CTL_ReportSaveAction")
@ActionReferences({
  @ActionReference(path = "Toolbars/SaveReport", position = 1),
  @ActionReference(path = "Actions/Report", position = 1)
})
public final class ReportSaveAction extends AbstractReportAction {

  final PerspectiveServiceReport perspectiveServiceReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);
  ReportRunAlternatifService reportRunAlternatifService = Lookup.getDefault().lookup(ReportRunAlternatifService.class);

  public ReportSaveAction() {
    super(false);
    putValue(Action.NAME, NbBundle.getMessage(ReportSaveAction.class, "CTL_ReportSaveAction"));
    final Icon icon = ImageUtilities.image2Icon(ImageUtilities.loadImage("org/fudaa/fudaa/crue/common/icons/save.png"));
    putValue(LARGE_ICON_KEY, icon);
    putValue(SMALL_ICON, icon);
    perspectiveServiceReport.addDirtyListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        updateEnableState();
      }
    });
  }

  @Override
  protected boolean getEnableState() {
    return super.getEnableState() && perspectiveServiceReport != null && perspectiveServiceReport.isDirty();
  }

  @Override
  protected void updateEnableState() {
    super.updateEnableState();
    Toolbar toolbar = ToolbarPool.getDefault().findToolbar("SaveReport");
    if (toolbar != null) {
      boolean visible = isMyPerspectiveActivated();
      toolbar.setVisible(visible);
    }
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ReportSaveAction();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    new ReportSaver().save();
  }
}
