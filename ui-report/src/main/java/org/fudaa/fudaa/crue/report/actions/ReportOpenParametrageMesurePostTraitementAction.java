package org.fudaa.fudaa.crue.report.actions;

import org.fudaa.fudaa.crue.aoc.service.LhptService;
import org.fudaa.fudaa.crue.report.ReportParametrageMesurePostTraitementTopComponent;
import org.fudaa.fudaa.crue.report.perspective.ActiveReport;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Permet d'ouvrir la fenêtre Mesures post-traitements
 */
@ActionID(category = "View", id = "org.fudaa.fudaa.crue.report.actions.ReportOpenParametrageMesurePostTraitementAction")
@ActionRegistration(displayName = "#ReportOpenParametrageMesurePostTraitementAction.Name")
@ActionReferences({
    @ActionReference(path = ActiveReport.ACTIONS_REPORT, position = 91),
    @ActionReference(path = ActiveReport.ACTIONS_REPORT_VIEWS, position = 91)
})
public final class ReportOpenParametrageMesurePostTraitementAction extends AbstractOpenViewAction implements ContextAwareAction {
  final LhptService service = Lookup.getDefault().lookup(LhptService.class);

  public ReportOpenParametrageMesurePostTraitementAction() {
    super(ReportParametrageMesurePostTraitementTopComponent.MODE, ReportParametrageMesurePostTraitementTopComponent.TOPCOMPONENT_ID);
    putValue(Action.NAME, NbBundle.getMessage(ReportOpenParametrageMesurePostTraitementAction.class, "ReportOpenParametrageMesurePostTraitementAction.Name"));
    service.addProjectListener(evt -> updateEnableState());
    updateEnableState();
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new ReportOpenParametrageMesurePostTraitementAction();
  }

  protected void updateEnableState() {
    if (!EventQueue.isDispatchThread()) {
      EventQueue.invokeLater(
          new Runnable() {
            @Override
            public void run() {
              updateEnableState();
            }
          });
      return;
    }
    setEnabled(getEnableState());
  }

  private boolean getEnableState() {
    return service.isProjectLoaded();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    super.open();
  }
}
