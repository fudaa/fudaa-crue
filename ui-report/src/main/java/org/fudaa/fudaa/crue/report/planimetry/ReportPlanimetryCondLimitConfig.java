/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.planimetry;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.fudaa.crue.planimetry.configuration.CondLimitConfigurationExtra;
import org.fudaa.fudaa.crue.planimetry.configuration.CondLimitConfigurationLabelBuilder;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.util.Lookup;

/**
 * Dans le cas transitoire, calcul la valeur interpolée
 *
 * @author Frederic Deniger
 */
public class ReportPlanimetryCondLimitConfig implements CondLimitConfigurationExtra {

  private final CondLimitConfigurationLabelBuilder labelBuilder = new CondLimitConfigurationLabelBuilder();
  final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);

  @Override
  public String getLabelFor(DonCLimM donCLimM, CrueConfigMetier ccm, boolean longText) {
    if (donCLimM.getCalculParent().isPermanent() ||! (donCLimM instanceof CalcTransItem)) {
      return labelBuilder.getLabelFor(donCLimM, ccm, longText);
    }
    ResultatTimeKey selectedTime = reportService.getSelectedTime();
    long tempCalc = reportService.getTimeContent().getTempCalc(selectedTime);
    Loi loi = ((CalcTransItem) donCLimM).getLoi();
    Double interpolatedValue = LoiHelper.getInterpolatedValue(loi, tempCalc / 1000D, ccm);
    if (interpolatedValue == null) {
      return "?";
    }
    ItemVariable loiOrdonnee = ccm.getLoiOrdonnee(loi.getType());
    if (loiOrdonnee == null) {
      return interpolatedValue.toString();
    }
    return labelBuilder.getFormattedValue(ccm, loiOrdonnee.getNom(), interpolatedValue, longText);
  }
}
