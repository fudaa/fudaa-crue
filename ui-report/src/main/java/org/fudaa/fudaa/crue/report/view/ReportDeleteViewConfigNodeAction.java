/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view;

import java.io.IOException;
import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ReportDeleteViewConfigNodeAction extends AbstractEditNodeAction {

  private final ReportViewsService reportViewsService = Lookup.getDefault().lookup(ReportViewsService.class);

  public ReportDeleteViewConfigNodeAction() {
    super(org.openide.util.NbBundle.getMessage(ReportDeleteViewConfigNodeAction.class, "DeleteViewConfig.ActionName"));
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    return ArrayUtils.isNotEmpty(activatedNodes);
  }

  @Override
  protected void performAction(final Node[] activatedNodes) {
    try {
      for (final Node node : activatedNodes) {
        final Node parentNode = node.getParentNode();
        node.destroy();
        final ReportContentType type = parentNode.getLookup().lookup(ReportContentType.class);
        reportViewsService.getFor(type).setModified(true);
      }
    } catch (final IOException iOException) {
      //rien a faire...
    }

  }
}
