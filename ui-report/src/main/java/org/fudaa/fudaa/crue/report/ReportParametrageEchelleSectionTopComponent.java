package org.fudaa.fudaa.crue.report;

import org.fudaa.fudaa.crue.aoc.ParametrageEchelleSectionTopComponent;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

/**
 * La vue de paramétrage Profil<->Section
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.report//ReportParametrageEchelleSectionTopComponent//EN",
    autostore = false)
@TopComponent.Description(preferredID = ReportParametrageEchelleSectionTopComponent.TOPCOMPONENT_ID, persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ReportParametrageEchelleSectionTopComponent.MODE, openAtStartup = false)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.report.ReportParametrageEchelleSectionTopComponent")
public final class ReportParametrageEchelleSectionTopComponent extends ParametrageEchelleSectionTopComponent {
  public static final String MODE = "report-parametrageEchelleSectionReport"; //NOI18N
  public static final String TOPCOMPONENT_ID = "ReportParametrageEchelleSectionTopComponent"; //NOI18N

  public ReportParametrageEchelleSectionTopComponent() {
    super(Lookup.getDefault().lookup(PerspectiveServiceReport.class));
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  void writeProperties(java.util.Properties p) {
    // no settings
  }

  void readProperties(java.util.Properties p) {
    // no settings
  }
}
