/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.planimetry;

import org.fudaa.ctulu.CtuluRange;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.fudaa.crue.report.ReportTimeTopComponent;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.util.Lookup;
import org.openide.windows.WindowManager;

/**
 *
 * @author Frederic Deniger
 */
public class ReportRangeFinder {

  final ReportTimeTopComponent tc;
  final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);

  public ReportRangeFinder() {
    tc = (ReportTimeTopComponent) WindowManager.getDefault().findTopComponent(ReportTimeTopComponent.TOPCOMPONENT_ID);
  }

  public CtuluRange getRangeForSelectedTime(final EnumCatEMH catEMH, final ReportVariableKey variable) {
    final ReportPalettePlagetTarget target = createTarget(variable, catEMH);
    final CtuluRange range = new CtuluRange();
    if (reportService.getSelectedTime() != null) {
      target.getTimeRange(range);
    }
    return range;
  }

  public CtuluRange getRangeGlobal(final EnumCatEMH catEMH, final ReportVariableKey variable) {
    final ReportPalettePlagetTarget target = createTarget(variable, catEMH);
    final CtuluRange range = new CtuluRange();
    target.getRange(range);
    return range;
  }

  private ReportPalettePlagetTarget createTarget(final ReportVariableKey variable, final EnumCatEMH catEMH) {
    return new ReportPalettePlagetTarget(new ReportRunVariableKey(reportService.getCurrentRunKey(), variable), catEMH);
  }
}
