package org.fudaa.fudaa.crue.report;

import org.fudaa.fudaa.crue.aoc.ParametrageMesurePostTraitementTopComponent;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

/**
 * La vue pour éditer les lois Mesures post-traitement
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.report//ReportParametrageMesurePostTraitementTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = ReportParametrageMesurePostTraitementTopComponent.TOPCOMPONENT_ID, persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ReportParametrageMesurePostTraitementTopComponent.MODE, openAtStartup = false)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.report.ReportParametrageMesurePostTraitementTopComponent")
public final class ReportParametrageMesurePostTraitementTopComponent extends ParametrageMesurePostTraitementTopComponent {

    public static final String MODE = "report-parametrageMesurePostTraitementReport"; //NOI18N
    public static final String TOPCOMPONENT_ID = "ReportParametrageMesurePostTraitementTopComponent"; //NOI18N

    public ReportParametrageMesurePostTraitementTopComponent() {
        super(Lookup.getDefault().lookup(PerspectiveServiceReport.class));
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
    }

    void readProperties(java.util.Properties p) {
    }

    @Override
    public String getDefaultTopComponentId() {
        return TOPCOMPONENT_ID;
    }


}
