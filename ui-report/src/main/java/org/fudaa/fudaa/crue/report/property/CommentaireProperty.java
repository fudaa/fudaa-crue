package org.fudaa.fudaa.crue.report.property;

import java.lang.reflect.InvocationTargetException;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.openide.nodes.PropertySupport;
import org.openide.util.NbBundle;

/**
 *
 * @author gressier
 */
public class CommentaireProperty extends PropertySupport.ReadOnly<String> {
  
  public static final String ID = "Commentaire";
  private final EMHRun run;

  public CommentaireProperty(EMHRun run) {
    super(ID, String.class, getDefaultDisplayName(), getDescription());
    this.run = run;
    // non éditable
    PropertyCrueUtils.configureNoCustomEditor(this);
  }
  
  public static String getDescription() {
    return NbBundle.getMessage(CommentaireProperty.class, "CommentaireProperty.Description");
  }

  public static String getDefaultDisplayName() {
    return NbBundle.getMessage(CommentaireProperty.class, "CommentaireProperty.DefaultDisplayName");
  }
  
  @Override
  public String toString() {
    if (run != null && run.getInfosVersion() != null ) {
    return run.getInfosVersion().getCommentaire();
    } else {
      return StringUtils.EMPTY;
    }
  }
  
  @Override
  public String getValue() throws IllegalAccessException, InvocationTargetException {
    return toString();
  }
  
}
