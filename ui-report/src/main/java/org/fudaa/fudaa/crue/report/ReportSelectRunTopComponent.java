package org.fudaa.fudaa.crue.report;

import java.util.Set;
import javax.swing.ActionMap;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.report.data.ReportRunContent;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.report.node.ReportManagerScenarioChildFactory;
import org.fudaa.fudaa.crue.report.node.ReportRunNode;
import org.fudaa.fudaa.crue.report.property.CommentaireProperty;
import org.fudaa.fudaa.crue.report.service.ReportRunAlternatifService;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.report//ReportSelectRunTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = ReportSelectRunTopComponent.TOPCOMPONENT_ID,
        iconBase = "org/fudaa/fudaa/crue/report/rond-orange_16.png",
        persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "report-topLeft", openAtStartup = false, position = 3)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.report.ReportSelectRunTopComponent")
@ActionReference(path = "Menu/Window/Report", position = 3)
@TopComponent.OpenActionRegistration(displayName = ReportSelectRunTopComponent.TOPCOMPONENT_ACTION_DISPLAYNAME,
        preferredID = ReportSelectRunTopComponent.TOPCOMPONENT_ID)
public final class ReportSelectRunTopComponent extends AbstractReportTopComponent implements LookupListener, ExplorerManager.Provider {

  public static final String TOPCOMPONENT_ID = "ReportSelectRunTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  private final ExplorerManager em = new ExplorerManager();
  private final org.openide.explorer.view.OutlineView outlineView;
  final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  final ReportRunAlternatifService reportRunAlternatifService = Lookup.getDefault().lookup(ReportRunAlternatifService.class);
  Result<EMHProjet> resultProjet;
  Result<ReportRunContent> resultRunAlternatif;
  private LookupListener runAlternatifLookupListener;
  private LookupListener resultProjetLookupListener;

  public ReportSelectRunTopComponent() {
    initComponents();

    outlineView = new OutlineView(NbBundle.getMessage(ReportSelectRunTopComponent.class, "ReportSelectRunTopComponent.FirstColumn.Name"));
    //outlineView.addPropertyColumn(CommentaireProperty.ID, NbBundle.getMessage(ReportSelectRunTopComponent.class, "ReportSelectRunTopComponent.Commentaire"), NbBundle.getMessage(ReportSelectRunTopComponent.class, "ReportSelectRunTopComponent.Commentaire"));
    PropertyCrueUtils.addProperty(outlineView, CommentaireProperty.ID, ReportSelectRunTopComponent.class, "ReportSelectRunTopComponent.Commentaire");
    outlineView.getOutline().setFullyNonEditable(true);
    outlineView.getOutline().setFillsViewportHeight(true);
    outlineView.getOutline().setColumnHidingAllowed(false);
    outlineView.getOutline().setRootVisible(false);
    add(outlineView);

    setName(NbBundle.getMessage(ReportSelectRunTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ReportSelectRunTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    ActionMap map = this.getActionMap();
    associateLookup(ExplorerUtils.createLookup(em, map));
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueSelectionFichiersResultats", PerspectiveEnum.REPORT);
  }

  @Override
  protected void componentOpenedHandler() {
    if (resultProjet == null) {
      resultProjet = projetService.getLookup().lookupResult(EMHProjet.class);
      resultProjetLookupListener = new LookupListener() {
        @Override
        public void resultChanged(LookupEvent ev) {
          projectChanged();
        }
      };
      resultProjet.addLookupListener(resultProjetLookupListener);
    }
    if (resultRunAlternatif == null) {
      resultRunAlternatif = reportRunAlternatifService.getLookup().lookupResult(ReportRunContent.class);
      runAlternatifLookupListener = new LookupListener() {
        @Override
        public void resultChanged(LookupEvent ev) {
          runAlternatifChanged();
        }
      };
      resultRunAlternatif.addLookupListener(runAlternatifLookupListener);
    }
  }

  protected void runAlternatifChanged() {
    updateRunNodeLoadedState();
  }

  @Override
  protected void componentClosedDefinitlyHandler() {
    if (resultProjet != null) {
      resultProjet.removeLookupListener(resultProjetLookupListener);
      resultProjet = null;
    }
    if (resultRunAlternatif != null) {
      resultRunAlternatif.removeLookupListener(runAlternatifLookupListener);
      resultRunAlternatif = null;
    }
  }

  protected void projectChanged() {
    initRootNode();
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return em;
  }

  @Override
  protected void componentActivated() {
    super.componentActivated();
    ExplorerUtils.activateActions(em, true);
  }

  @Override
  protected void componentDeactivated() {
    super.componentDeactivated();
    ExplorerUtils.activateActions(em, false);
  }

  @Override
  protected void setEditable(boolean b) {
  }

  @Override
  protected void runLoaded() {
    if (!isOpened()) {
      return;
    }
    if (em.getRootContext() == null || em.getRootContext().equals(Node.EMPTY)) {
      initRootNode();
    } else {
      updateRunNodeLoadedState();
    }
    //on charge les runs:
    reportRunAlternatifService.loadRuns();
    updateRunNodeLoadedState();
  }

  @Override
  public void runUnloaded() {
    updateRunNodeLoadedState();
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  void writeProperties(java.util.Properties p) {
    p.setProperty("version", "1.0");
    DialogHelper.writeProperties(outlineView, "outlineView", p);
  }

  void readProperties(java.util.Properties p) {
    if ("1.0".equals(p.getProperty("version"))) {
      DialogHelper.readProperties(outlineView, "outlineView", p);
    }
  }

  private void initRootNode() {
    EMHProjet selectedProject = projetService.getSelectedProject();
    if (selectedProject == null || !reportService.isRunLoaded()) {
      em.setRootContext(Node.EMPTY);
    } else {
      AbstractNode etu = new AbstractNode(
              Children.create(new ReportManagerScenarioChildFactory(projetService.getSelectedProject()), false), Lookups.singleton(projetService.getSelectedProject()));
      em.setRootContext(etu);
      updateRunNodeLoadedState();
      NodeHelper.expandNode(etu, outlineView);
    }
  }

  private void updateRunNodeLoadedState() {
    if (em.getRootContext() == null) {
      return;
    }
    Set<String> alternatifRuns = TransformerHelper.toSetOfId(reportRunAlternatifService.getRunAlternatif());
    ReportRunContent runCourant = reportService.getRunCourant();
    Node[] managerScenarioNodes = em.getRootContext().getChildren().getNodes();
    for (Node node : managerScenarioNodes) {
      Node[] runNodes = node.getChildren().getNodes();
      for (Node runNode : runNodes) {
        ReportRunNode reportRunNode = (ReportRunNode) runNode;
        if (runCourant != null) {
          reportRunNode.setRunLoaded(reportRunNode.isSameRun(runCourant.getRun()));
        } else {
          reportRunNode.setRunLoaded(false);
        }
        reportRunNode.setRunAlternatif(alternatifRuns.contains(reportRunNode.getRun().getId()));
      }
    }
  }
}
