/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.planimetry;

import java.awt.Color;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetrySectionExtraData;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.crue.planimetry.configuration.SectionConfiguration;
import org.fudaa.fudaa.crue.planimetry.configuration.SectionConfigurationExtra;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetrySectionLayerModel;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;

/**
 *
 * @author Frederic Deniger
 */
public class ReportPlanimetrySectionConfig extends AbstractConfigurationExtra<ReportPlanimetrySectionExtraData> implements SectionConfigurationExtra {

  public ReportPlanimetrySectionConfig(ReportResultProviderService reportResultProviderService) {
    super(reportResultProviderService);
    data=new ReportPlanimetrySectionExtraData();
  }

  @Override
  public void decorateTraceIcon(CatEMHSection emh, TraceIconModel ligne, PlanimetrySectionLayerModel sectionModel, SectionConfiguration aThis) {
    Color color = getColor(emh);
    if (color != null) {
      ligne.setCouleur(color);
    }
  }

  @Override
  public String getDisplayName(CatEMHSection emh, PlanimetrySectionLayerModel modeleDonnees, SectionConfiguration aThis, boolean selected) {
    return getDisplayName(emh, selected);
  }

  @Override
  public boolean canLabelsBeAggregate() {
    return data != null && data.isUseLabels() && CollectionUtils.isNotEmpty(data.getLabels());
  }
}
