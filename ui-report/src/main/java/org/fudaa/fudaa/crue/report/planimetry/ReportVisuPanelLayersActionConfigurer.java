/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.planimetry;

import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.report.actions.ReportPlanimetryExportAction;
import org.fudaa.fudaa.crue.report.actions.ReportPlanimetryOpenMultiVarViewAction;
import org.fudaa.fudaa.crue.report.actions.ReportPlanimetryOpenProfilLongitudinalViewAction;
import org.fudaa.fudaa.crue.report.actions.ReportPlanimetryOpenProfilTransversalViewAction;
import org.fudaa.fudaa.crue.report.actions.ReportPlanimetryOpenRPTGViewAction;
import org.fudaa.fudaa.crue.report.actions.ReportPlanimetryOpenTemporalViewAction;

/**
 * Configuration des actions des claques de la vue planimetriques.
 *
 * @author Frederic Deniger
 */
public class ReportVisuPanelLayersActionConfigurer {

  private final ReportPlanimetryExportAction exportAction;
  private final ReportPlanimetryOpenMultiVarViewAction openMultiVarViewAction;
  private final ReportPlanimetryOpenProfilTransversalViewAction openProfilTransversalViewAction;
  private final ReportPlanimetryOpenProfilLongitudinalViewAction openProfilLongitudinalViewAction;
  private final ReportPlanimetryOpenRPTGViewAction openRPTGViewAction;
  private final ReportPlanimetryOpenTemporalViewAction openTemporalViewAction;
  private final PlanimetryController planimetryController;

  public ReportVisuPanelLayersActionConfigurer(PlanimetryController planimetryController) {
    this.planimetryController = planimetryController;
    exportAction = new ReportPlanimetryExportAction(planimetryController);
    openMultiVarViewAction = new ReportPlanimetryOpenMultiVarViewAction(planimetryController);
    openProfilTransversalViewAction = new ReportPlanimetryOpenProfilTransversalViewAction(planimetryController);
    openProfilLongitudinalViewAction = new ReportPlanimetryOpenProfilLongitudinalViewAction(planimetryController);
    openRPTGViewAction = new ReportPlanimetryOpenRPTGViewAction(planimetryController);
    openTemporalViewAction = new ReportPlanimetryOpenTemporalViewAction(planimetryController);
  }

  public EbliActionInterface[] getBranchesActions() {
    return new EbliActionInterface[]{
              exportAction,
              null,
              openProfilLongitudinalViewAction,
              openTemporalViewAction,
              openMultiVarViewAction,
              null,
              openRPTGViewAction,
              null,
              planimetryController.getVisuPanel().createFindAction(planimetryController.getCqBranche())
            };
  }

  public EbliActionInterface[] getNoeudActions() {
    return new EbliActionInterface[]{
              exportAction,
              null,
              openTemporalViewAction,
              openMultiVarViewAction,
              null,
              openRPTGViewAction,
              null,
              planimetryController.getVisuPanel().createFindAction(planimetryController.getCqNoeud())
            };
  }

  public EbliActionInterface[] getCasiersActions() {
    return new EbliActionInterface[]{
              exportAction,
              null,
              openTemporalViewAction,
              openMultiVarViewAction,
              null,
              openRPTGViewAction,
              null,
              planimetryController.getVisuPanel().createFindAction(planimetryController.getCqCasier())
            };
  }

  public EbliActionInterface[] getSectionsActions() {
    return new EbliActionInterface[]{
              exportAction,
              null,
              openTemporalViewAction,
              openMultiVarViewAction,
              openProfilTransversalViewAction,
              null,
              openRPTGViewAction,
              null,
              planimetryController.getVisuPanel().createFindAction(planimetryController.getCqSection())};
  }

  public EbliActionInterface[] getTracesActions() {
    return new EbliActionInterface[]{
              exportAction,
              null,
              openTemporalViewAction,
              openMultiVarViewAction,
              openProfilTransversalViewAction,
              null,
              openRPTGViewAction,
              null,
              planimetryController.getVisuPanel().createFindAction(planimetryController.getCqTrace())};
  }
}
