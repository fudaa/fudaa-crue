/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.formule;

import javax.swing.JMenuItem;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.openide.nodes.Node;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleNodeActionEditExpr extends AbstractEditNodeAction {

  public FormuleNodeActionEditExpr() {
    super(org.openide.util.NbBundle.getMessage(FormuleNodeActionEditExpr.class, "formule.edit"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return activatedNodes.length == 1;
  }
  
  @Override
  public JMenuItem getPopupPresenter() {
    return NodeHelper.useBoldFont(super.getPopupPresenter());
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    FormulePanelEditExpr editor = new FormulePanelEditExpr((FormuleNodeParametersExpr) activatedNodes[0]);
    editor.show();
  }
}
