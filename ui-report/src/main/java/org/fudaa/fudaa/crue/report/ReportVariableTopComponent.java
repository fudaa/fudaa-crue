package org.fudaa.fudaa.crue.report;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.List;
import javax.swing.ActionMap;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataEdited;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDependenciesContent;
import org.fudaa.dodico.crue.projet.report.formule.FormuleServiceContent;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.UserSaveAnswer;
import org.fudaa.fudaa.crue.common.node.NodeTreeListener;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.report.formule.FormuleContentChildFactory;
import org.fudaa.fudaa.crue.report.formule.FormuleNodeParameters;
import org.fudaa.fudaa.crue.report.formule.FormuleNodeParametersExpr;
import org.fudaa.fudaa.crue.report.service.ReportFormuleService;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Affichage des variables utilisées dans la perspective Rapports.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.report//ReportVariableTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = ReportVariableTopComponent.TOPCOMPONENT_ID,
        iconBase = "org/fudaa/fudaa/crue/report/rond-orange_16.png",
        persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "report-topLeft", openAtStartup = false, position = 1)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.report.ReportVariableTopComponent")
@ActionReference(path = "Menu/Window/Report", position = 2)
@TopComponent.OpenActionRegistration(displayName = ReportVariableTopComponent.TOPCOMPONENT_ACTION_DISPLAYNAME,
        preferredID = ReportVariableTopComponent.TOPCOMPONENT_ID)
public final class ReportVariableTopComponent extends AbstractReportTopComponent implements LookupListener, ExplorerManager.Provider {

  public static final String TOPCOMPONENT_ID = "ReportVariableTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  private final ExplorerManager em = new ExplorerManager();
  private final org.openide.explorer.view.OutlineView outlineView;
  final ReportFormuleService reportFormuleService = Lookup.getDefault().lookup(ReportFormuleService.class);
  final ReportGlobalServiceContrat reportGlobalServiceContrat = Lookup.getDefault().lookup(ReportGlobalServiceContrat.class);

  private LookupListener resultProjetLookupListener;
  private Lookup.Result<FormuleServiceContent> resultProjet;
  private final NodeTreeListener treeListener = new NodeTreeListener() {
    @Override
    public void changeDoneInNode(String propertyName) {
      setModified(true);
    }
  };

  public ReportVariableTopComponent() {
    initComponents();
    setName(NbBundle.getMessage(ReportVariableTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ReportVariableTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    outlineView = new OutlineView(NbBundle.getMessage(ReportSelectRunTopComponent.class,
            "ReportVariableTopComponent.FirstColumn.Name"));
    outlineView.addPropertyColumn(FormuleNodeParametersExpr.DESCRIPTION_PROPERTY, NbBundle.getMessage(ReportVariableTopComponent.class,
            "formule.description"));
    outlineView.getOutline().setFullyNonEditable(true);
    outlineView.getOutline().setFillsViewportHeight(true);
    outlineView.getOutline().setColumnHidingAllowed(false);
    outlineView.getOutline().setRootVisible(false);
    add(outlineView);
    add(createCancelSaveButtons(), BorderLayout.SOUTH);

    setName(NbBundle.getMessage(ReportSelectRunTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ReportSelectRunTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    ActionMap map = this.getActionMap();
    associateLookup(ExplorerUtils.createLookup(em, map));

  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueVariables", PerspectiveEnum.REPORT);
  }

  @Override
  public boolean valideModification() {
    FormuleDependenciesContent dependenciesContent = FormuleContentChildFactory.getDependenciesContent(getExplorerManager().getRootContext(),
            reportGlobalServiceContrat);
    List<String> cycliqueVar = dependenciesContent.getCycliqueVar();
    if (CollectionUtils.isNotEmpty(cycliqueVar)) {
      String arg = "<li>" + StringUtils.join(cycliqueVar, "</li><li>") + "</li>";
      DialogHelper.showError(NbBundle.getMessage(ReportVariableTopComponent.class, "formuleCyclesDetected.error", arg));
      return false;
    }
    List<FormuleNodeParameters> allNodes = FormuleContentChildFactory.getAllNodes(getExplorerManager().getRootContext());

    FormuleDataEdited data = new FormuleDataEdited();
    for (FormuleNodeParameters formuleNodeParameters : allNodes) {
      data.addParameter(formuleNodeParameters.getInitId(), formuleNodeParameters.getFormuleParameter());
    }
    reportFormuleService.modified(data);
    return true;
  }

  @Override
  public void cancelModification() {
    variableContentChanged();
  }



  @Override
  public ExplorerManager getExplorerManager() {
    return em;
  }

  @Override
  public void setModified(boolean modified) {
    if (updating) {
      return;
    }
    this.modified = modified;
    updateTopComponentName();
    if (buttonSave != null) {
      buttonSave.setEnabled(modified);
    }
    if (buttonCancel != null) {
      buttonCancel.setEnabled(modified);
    }
  }

  @Override
  public void setEditable(boolean b) {
  }

  @Override
  public void runLoaded() {
  }

  @Override
  public void runUnloaded() {
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables

  @Override
  protected void componentOpenedHandler() {
    if (resultProjet == null) {
      resultProjet = reportFormuleService.getLookup().lookupResult(FormuleServiceContent.class);
      resultProjetLookupListener = new LookupListener() {
        @Override
        public void resultChanged(LookupEvent ev) {
          variableContentChanged();
        }
      };
      resultProjet.addLookupListener(resultProjetLookupListener);
    }
    //pour recharger les variables
    variableContentChanged();
  }

  void variableContentChanged() {
    FormuleServiceContent currentVariables = reportFormuleService.getServiceContent();
    if (currentVariables == null && reportFormuleService.IsUpdating()) {
      return;
    }
    if (em.getRootContext() != Node.EMPTY) {
      treeListener.removeListener(em.getRootContext());
    }
    if (currentVariables == null) {
      em.setRootContext(Node.EMPTY);
    } else if (isOpened()) {
      FormuleContentChildFactory nodeFactory = new FormuleContentChildFactory();
      em.setRootContext(nodeFactory.createMainNode());
      treeListener.installListener(em.getRootContext());
    }
    setModified(false);
  }

  @Override
  protected void componentActivated() {
    super.componentActivated();
    ExplorerUtils.activateActions(em, true);
  }

  @Override
  protected void componentDeactivated() {
    UserSaveAnswer confirmSaveOrNot = askForSave();

    if (UserSaveAnswer.CANCEL.equals(confirmSaveOrNot)) {
      return;
    }
    super.componentDeactivated();
    ExplorerUtils.activateActions(em, false);
  }

  @Override
  protected void componentClosedDefinitlyHandler() {
    if (resultProjet != null) {
      resultProjet.removeLookupListener(resultProjetLookupListener);
      resultProjet = null;
    }
  }

  void writeProperties(java.util.Properties p) {
    DialogHelper.writeProperties(outlineView, "outlineView", p);
  }

  void readProperties(java.util.Properties p) {
    DialogHelper.readProperties(outlineView, "outlineView", p);
  }

  public void requestActiveLater() {
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        requestActive();
      }
    });
  }
}
