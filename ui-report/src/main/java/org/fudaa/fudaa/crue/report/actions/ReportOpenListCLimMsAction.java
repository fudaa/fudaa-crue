package org.fudaa.fudaa.crue.report.actions;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.fudaa.fudaa.crue.report.ReportListCLimMsTopComponent;
import org.fudaa.fudaa.crue.report.perspective.ActiveReport;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle;

/**
 * permet d'ouvrir la liste des frottements
 */
@ActionID(category = "View", id = "org.fudaa.fudaa.crue.report.actions.ReportOpenListCLimMsAction")
@ActionRegistration(displayName = "#ReportOpenListCLimMsAction.Name")
@ActionReferences({
  @ActionReference(path = ActiveReport.ACTIONS_REPORT_VIEWS, position = 2)
})
public final class ReportOpenListCLimMsAction extends AbstractOpenViewAction {

  public ReportOpenListCLimMsAction() {
    super(ReportListCLimMsTopComponent.MODE, ReportListCLimMsTopComponent.TOPCOMPONENT_ID);
    putValue(Action.NAME, NbBundle.getMessage(ReportOpenListCLimMsAction.class, "ReportOpenListCLimMsAction.Name"));
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    super.open();
  }
}
