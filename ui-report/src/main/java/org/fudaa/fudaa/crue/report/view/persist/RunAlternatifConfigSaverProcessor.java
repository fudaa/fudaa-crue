/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import java.io.File;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

/**
 *
 * @author Frederic Deniger
 */
public class RunAlternatifConfigSaverProcessor implements ProgressRunnable<CtuluLog> {

  private final File file;
  private final List<ReportRunKey> keys;

  public RunAlternatifConfigSaverProcessor(File file, List<ReportRunKey> keys) {
    this.file = file;
    this.keys = keys;
  }

  @Override
  public CtuluLog run(ProgressHandle handle) {
    if (handle != null) {
      handle.switchToIndeterminate();
    }
    RunAlternatifSaver configSaver = new RunAlternatifSaver();
    return configSaver.save(file, keys);
  }
}
