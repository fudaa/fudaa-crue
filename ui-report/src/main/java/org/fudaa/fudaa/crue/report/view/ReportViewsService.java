/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view;

import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;
import org.fudaa.fudaa.crue.common.helper.CrueSwingWorker;
import org.fudaa.fudaa.crue.common.node.NodeChildrenHelper;
import org.fudaa.fudaa.crue.common.node.NodeTreeListener;
import org.fudaa.fudaa.crue.report.AbstractReportTopComponent;
import org.fudaa.fudaa.crue.report.ReportTopComponentConfigurable;
import org.fudaa.fudaa.crue.report.ReportVisualTopComponent;
import org.fudaa.fudaa.crue.report.contrat.ReportViewServiceContrat;
import org.fudaa.fudaa.crue.report.longitudinal.ReportProfilLongitudinalTopComponent;
import org.fudaa.fudaa.crue.report.multivar.ReportMultiVarTopComponent;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.fudaa.fudaa.crue.report.rptg.ReportRPTGTopComponent;
import org.fudaa.fudaa.crue.report.temporal.ReportTemporalTopComponent;
import org.fudaa.fudaa.crue.report.transversal.ReportProfilTransversalTopComponent;
import org.fudaa.fudaa.crue.study.services.CrueService;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import org.openide.windows.TopComponent;

import java.util.*;

/**
 * Service de gestion des vues dans la perspective rapport. Pas de lookup.
 *
 * @author Frederic Deniger
 */
@ServiceProviders({
        @ServiceProvider(service = ReportViewsService.class),
        @ServiceProvider(service = ReportViewServiceContrat.class)})
public final class ReportViewsService extends ReportViewTopComponentListener.Target implements ReportViewServiceContrat {
    private static final String LOADED_ATTRIBUTE = "loaded";
    private final Map<ReportContentType, ReportViewsByType> configs = new EnumMap<>(ReportContentType.class);
    private final CrueService crueService = Lookup.getDefault().lookup(CrueService.class);
    private final ReportViewTopComponentListener listener = new ReportViewTopComponentListener();
    private final PerspectiveServiceReport perspectiveServiceReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);
    private final AbstractNode mainNode;
    private final NodeTreeListener nodeTreeListener;
    private boolean loadingProject;

    /**
     * Initialise les configurations pour les vues.
     */
    public ReportViewsService() {
        addConfig(
                new ReportViewsByType(ReportContentType.TRANSVERSAL, ReportProfilTransversalTopComponent.class, ReportProfilTransversalTopComponent.MODE,
                        ReportProfilTransversalTopComponent.TOPCOMPONENT_ID));
        addConfig(new ReportViewsByType(ReportContentType.TEMPORAL, ReportTemporalTopComponent.class, ReportTemporalTopComponent.MODE,
                ReportTemporalTopComponent.TOPCOMPONENT_ID));
        addConfig(new ReportViewsByType(ReportContentType.LONGITUDINAL, ReportProfilLongitudinalTopComponent.class,
                ReportProfilLongitudinalTopComponent.MODE, ReportProfilLongitudinalTopComponent.TOPCOMPONENT_ID));
        addConfig(new ReportViewsByType(ReportContentType.MULTI_VAR, ReportMultiVarTopComponent.class, ReportMultiVarTopComponent.MODE,
                ReportMultiVarTopComponent.TOPCOMPONENT_ID));
        addConfig(new ReportViewsByType(ReportContentType.RPTG, ReportRPTGTopComponent.class, ReportRPTGTopComponent.MODE,
                ReportRPTGTopComponent.TOPCOMPONENT_ID));
        addConfig(new ReportViewsByType(ReportContentType.PLANIMETRY, ReportVisualTopComponent.class, ReportVisualTopComponent.MODE,
                ReportVisualTopComponent.TOPCOMPONENT_ID));
        mainNode = new AbstractNode(new Children.Array());

        listener.setTarget(this);
        crueService.addEMHProjetStateLookupListener(ev -> {
            loadingProject = true;
            projetStateChanged();
            loadingProject = false;
        });
        if (crueService.getProjectLoaded() != null) {
            projetStateChanged();
        }
        nodeTreeListener = new NodeTreeListener() {
            @Override
            public void changeDoneInNode(final String propertyName) {
                if (!loadingProject && !"displayName".equals(propertyName)) {
                    modificationDone();
                }
            }
        };
        nodeTreeListener.installListener(mainNode);
    }

    /**
     * @param topComponent le topComponent
     * @return le node de la vue permettant de configuration le contenu de tc
     */
    public static Node getViewNode(final TopComponent topComponent) {
        return (Node) topComponent.getClientProperty(ReportTopComponentConfigurable.VIEW_NODE);
    }

    /**
     * @param newTopComponent le {@link TopComponent} créé à lier au noeud <code>createdNode</code>
     * @param createdNode     le {@link Node} a lier
     */
    protected static void installNode(final ReportTopComponentConfigurable newTopComponent, final Node createdNode) {
        newTopComponent.getTopComponent().putClientProperty(ReportTopComponentConfigurable.VIEW_NODE, createdNode);
    }

    /**
     * Annule l'opération {@link #installNode(ReportTopComponentConfigurable, Node)}
     *
     * @param newTopComponent le {@link TopComponent} à delier
     * @param createdNode     le {@link Node} à delier
     */
    protected static void uninstallNode(final ReportTopComponentConfigurable newTopComponent, final Node createdNode) {
        newTopComponent.getTopComponent().putClientProperty(ReportTopComponentConfigurable.VIEW_NODE, null);
    }

    /**
     * modification effectué sur une vue &gt; envoie d'evenements
     *
     * @see PerspectiveServiceReport#setDirty(boolean)
     */
    protected void modificationDone() {
        perspectiveServiceReport.setDirty(true);
    }

    /**
     * @param contentType l'enum de la vue
     * @return le ReportView contenant toutes les vues liés au {@link ReportContentType}
     */
    public ReportViewsByType getFor(final ReportContentType contentType) {
        return configs.get(contentType);
    }

    /**
     * @return le projet courant
     */
    public EMHProjet getProjectLoaded() {
        return crueService.getProjectLoaded();
    }

    @Override
    public void changesDoneIn(final ReportVisualTopComponent reportVisualTopComponent) {
        ReportViewNode viewNode = (ReportViewNode) ReportViewsService.getViewNode(reportVisualTopComponent.getTopComponent());
        if (viewNode == null) {
            viewNode = created(reportVisualTopComponent);
        }
        this.setTopComponentModified(reportVisualTopComponent);
    }

    /**
     * @return la lite des vues ( organisées par type de vues)
     */
    public List<ReportViewsByType> getConfigViews() {
        return new ArrayList<>(configs.values());
    }

    private void loadAllConfig() {
        final List<ReportViewsByType> configViews = getConfigViews();
        for (final ReportViewsByType reportViewsByType : configViews) {
            reportViewsByType.loadAll(this);
        }
    }

    @Override
    public void variablesUpdated(final FormuleDataChanges changes) {
        if (changes.isSomethingModified()) {
            new CrueSwingWorker<Boolean>(NbBundle.getMessage(ReportViewsService.class, "LoadConfigs.ProgressTitle")) {
                @Override
                protected Boolean doProcess() throws Exception {
                    loadAllConfig();
                    return Boolean.TRUE;
                }

                @Override
                protected void done(final Boolean result) {
                    final List<ReportViewsByType> configViews = getConfigViews();
                    for (final ReportViewsByType reportViewsByType : configViews) {
                        reportViewsByType.variablesUpdated(changes);
                    }
                }
            }.execute();
        }
    }

    private void addConfig(final ReportViewsByType type) {
        configs.put(type.getType(), type);
    }

    /**
     * @param topComponentConfigurable le topComponent correspondant a une vue
     * @param viewConfigNode           si null, c'est que c'est une nouvelle vue
     */
    @Override
    public void reportTopComponentOpened(final ReportTopComponentConfigurable topComponentConfigurable, final Node viewConfigNode) {
        if (topComponentConfigurable.getReportConfig().getType().isPlanimetry()) {
            return;
        }
        if (viewConfigNode == null) {//creation
            created(topComponentConfigurable).updateDisplayName();
        } else {
            final ReportViewLine lookup = viewConfigNode.getLookup().lookup(ReportViewLine.class);
            lookup.setTopComponent(topComponentConfigurable);
            ((ReportViewNode) viewConfigNode).updateDisplayName();
        }
    }

    @Override
    public void reportTopComponentClosedDefinetly(final ReportTopComponentConfigurable tc, final Node viewConfigNode) {
        if (viewConfigNode != null) {
            final ReportViewLine lookup = viewConfigNode.getLookup().lookup(ReportViewLine.class);
            lookup.setTopComponent(null);
            ((ReportViewNode) viewConfigNode).updateDisplayName();
        }
    }

    /**
     * @param line le contenu de la vue
     * @param type le type de vue
     * @return le contenu chargé depuis un fichier
     */
    public ReportConfigContrat load(final ReportViewLine line, final ReportContentType type) {
        return new ReportViewsSave().load(line, type);
    }

    /**
     * Chargement de toutes les vues
     *
     * @see ReportViewsSave
     */
    public void loadConfigViews() {
        loadingProject = true;
        try {
            if (crueService.getProjectLoaded() == null || Boolean.TRUE.equals(mainNode.getValue(LOADED_ATTRIBUTE))) {
                return;
            }
            final ReportViewsSave save = new ReportViewsSave();
            save.loadIndex();
            mainNode.setValue(LOADED_ATTRIBUTE, Boolean.TRUE);
        } finally {
            loadingProject = false;
        }
    }

    /**
     * recharge l'ensemble
     */
    public void reloadAll() {
        clearAll();
        loadAll();
    }

    /**
     * si le projet est fermé-> tout est fermé.
     * Sinon chargement.
     */
    private void projetStateChanged() {
        if (crueService.getProjectLoaded() == null) {
            clearAll();
        } else {
            loadAll();
        }
    }

    /**
     * @param newLine nouvelle ligne ajoutée
     */
    protected void add(final ReportViewLine newLine) {
        final ReportContentType type = newLine.getContentType();
        final ReportViewsByType get = configs.get(type);
        get.added(newLine);
    }

    /**
     * @param newTopComponent le {@link TopComponent} créé
     * @return le noeud correspondant au {@link TopComponent} créé.
     */
    private ReportViewNode created(final ReportTopComponentConfigurable newTopComponent) {
        final ReportContentType type = newTopComponent.getReportConfig().getType();
        final ReportViewNode createdNode = configs.get(type).created(newTopComponent);
        installNode(newTopComponent, createdNode);
        return createdNode;
    }

    /**
     * @param node pour lequel les données sont rechargés des fichiers
     * @see #load(ReportViewLine, ReportContentType)
     */
    public void reloadNodeFromFile(final ReportViewNode node) {
        final ReportViewLine line = node.getLookup().lookup(ReportViewLine.class);
        if (line.isModified() && line.getLineInformation().getFilename() != null) {
            final ReportContentType type = node.getParentNode().getLookup().lookup(ReportContentType.class);
            final ReportConfigContrat loaded = load(line, type);
            final String oldName = node.getOldName();
            if (oldName != null) {
                node.setName(oldName);
            }
            if (loaded != null) {
                line.setReportConfig(loaded);
                final ReportTopComponentConfigurable topComponent = line.getTopComponent();
                if (topComponent != null) {
                    topComponent.setReportConfig(loaded, false);
                    topComponent.updateFrameName();
                }
            }
            line.setModified(false);
            node.updateDisplayName();
        }
    }

    /**
     * @return le noeud principal de la vue.
     */
    public AbstractNode getMainNode() {
        return mainNode;
    }

    public void setTopComponentModified(final AbstractReportTopComponent aThis) {
        final Node viewNode = getViewNode(aThis);
        if (viewNode != null) {
            ((ReportViewNode) viewNode).getLine().setModified(true);
            ((ReportViewNode) viewNode).updateDisplayName();
            modificationDone();
        }
    }

    private void clearAll() {
        NodeChildrenHelper.clearChildren(mainNode.getChildren());
        final Collection<ReportViewsByType> values = configs.values();
        for (final ReportViewsByType reportViewsByType : values) {
            reportViewsByType.setModified(false);
            reportViewsByType.clearNode();
        }
        listener.removeListener();
        mainNode.setValue(LOADED_ATTRIBUTE, Boolean.FALSE);
    }

    private void loadAll() {
        final List<Node> childrenNode = new ArrayList<>();
        childrenNode.add(configs.get(ReportContentType.TRANSVERSAL).getNode());
        childrenNode.add(configs.get(ReportContentType.LONGITUDINAL).getNode());
        childrenNode.add(configs.get(ReportContentType.TEMPORAL).getNode());
        childrenNode.add(configs.get(ReportContentType.MULTI_VAR).getNode());
        childrenNode.add(configs.get(ReportContentType.RPTG).getNode());
        childrenNode.add(configs.get(ReportContentType.PLANIMETRY).getNode());
        mainNode.getChildren().add(childrenNode.toArray(new Node[0]));
        listener.installListener();
    }
}
