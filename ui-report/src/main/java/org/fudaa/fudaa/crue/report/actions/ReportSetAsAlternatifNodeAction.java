/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.actions;

import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.report.node.ReportRunNode;
import org.fudaa.fudaa.crue.report.service.ReportRunAlternatifService;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportSetAsAlternatifNodeAction extends AbstractEditNodeAction {

  final ReportRunAlternatifService reportRunAlternatifService = Lookup.getDefault().lookup(ReportRunAlternatifService.class);

  public ReportSetAsAlternatifNodeAction() {
    super(NbBundle.getMessage(ReportSetAsAlternatifNodeAction.class, "ReportSetAsAlternatifNodeAction.DisplayName"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return activatedNodes != null && activatedNodes.length > 0;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    for (Node node : activatedNodes) {
      ManagerEMHScenario scenario = node.getParentNode().getLookup().lookup(ManagerEMHScenario.class);
      reportRunAlternatifService.addRun(scenario, ((ReportRunNode) node).getRun());
    }
  }
}
