/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.longitudinal;

import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.ItemEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.lit.LitNommeLimite;
import org.fudaa.dodico.crue.config.lit.ReportLongitudinalLimitHelper;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilEtiquette;
import org.fudaa.dodico.crue.metier.emh.DonPrtGeoProfilSection;
import org.fudaa.dodico.crue.metier.emh.EMHSectionProfil;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.metier.helper.EtiquetteIndexed;
import org.fudaa.dodico.crue.metier.helper.LitNommeIndexed;
import org.fudaa.dodico.crue.metier.helper.ReportProfilHelper;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableTimeKey;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalBrancheCartouche;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalHelper;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalPositionSection;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalPositionSectionByRun;
import org.fudaa.dodico.crue.validation.ValidateAndRebuildProfilSection;
import org.fudaa.fudaa.crue.loi.common.LoiConstanteCourbeModel;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Charger de construire les courbes basées sur des limites/étiquettes
 *
 * @author Frederic Deniger
 */
public class ReportLongitudinalGrapheBuilderLimit {
    private final CrueConfigMetier ccm;
    private final ReportLongitudinalLimitHelper limitHelper;
    private final ReportResultProviderService reportResultProviderService = Lookup.getDefault().lookup(ReportResultProviderService.class);

    public ReportLongitudinalGrapheBuilderLimit(CrueConfigMetier ccm) {
        this.ccm = ccm;
        limitHelper = new ReportLongitudinalLimitHelper(ccm);
    }

    /**
     * @param reportRunVariableKey lacle
     * @param cartouches           les cartouches
     * @return courbe model avec comme cle reportRunVariableKey +null
     */
    public LoiConstanteCourbeModel build(ReportRunVariableKey reportRunVariableKey, List<ReportLongitudinalBrancheCartouche> cartouches) {
        TDoubleArrayList xps = new TDoubleArrayList();
        TDoubleArrayList values = new TDoubleArrayList();
        TIntObjectHashMap labels = new TIntObjectHashMap();
        Map<String, TIntObjectHashMap<String>> sublabels = new HashMap<>();
        TIntObjectHashMap<String> labelBranches = new TIntObjectHashMap<>();
        TIntObjectHashMap<String> labelSections = new TIntObjectHashMap<>();
        sublabels.put(CruePrefix.P_BRANCHE, labelBranches);
        sublabels.put(CruePrefix.P_SECTION, labelSections);
        CtuluListSelection drawnSegment = new CtuluListSelection();
        ItemEnum etiquette = limitHelper.getEtiquette(reportRunVariableKey.getVariable().getVariableName());
        LitNommeLimite limite = limitHelper.getLimite(reportRunVariableKey.getVariable().getVariableName());
        if (etiquette != null || limite != null) {
            for (ReportLongitudinalBrancheCartouche cartouche : cartouches) {
                ReportLongitudinalPositionSectionByRun positions = cartouche.getSectionPositions(reportRunVariableKey.getRunKey());
                List<ReportLongitudinalPositionSection> sectionPositions = positions.getSectionDisplayPositions();
                boolean firstAddInBranche = true;
                for (ReportLongitudinalPositionSection sectionPosition : sectionPositions) {
                    CatEMHSection section = sectionPosition.getName();
                    EMHSectionProfil retrieveProfil = ReportProfilHelper.retrieveProfil(section);
                    if (retrieveProfil != null) {
                        DonPrtGeoProfilSection profil = DonPrtHelper.getProfilSection(retrieveProfil);
                        boolean ok = computeZ(etiquette, limite, profil, values);
                        if (ok) {
                            labels.put(xps.size(), section.getBranche().getNom() + " / " + section.getNom());
                            labelBranches.put(xps.size(), section.getBranche().getNom());
                            labelSections.put(xps.size(), section.getNom());
                            if (!firstAddInBranche) {//ce n'est pas le premier point de la ligne
                                drawnSegment.add(xps.size() - 1);//segment dessine
                            }
                            xps.add(sectionPosition.getXpDisplay());
                            firstAddInBranche = false;
                        }
                    }
                }
            }
        }
        ReportRunVariableTimeKey key = new ReportRunVariableTimeKey(reportRunVariableKey, null);
        ItemVariable varX = ccm.getProperty(CrueConfigMetierConstants.PROP_XP);
        ItemVariable varY = reportResultProviderService.getCcmVariable(reportRunVariableKey);
        LoiConstanteCourbeModel model = LoiConstanteCourbeModel.create(key, xps, values, varX, varY);
        model.setDrawnSegment(drawnSegment);
        model.setLabels(labels);
        model.setSubLabels(sublabels);
        model.setLabelColumnName(NbBundle.getMessage(ReportLongitudinalCourbesUiResController.class, "BrancheSectionColumn.Name"));
        ReportLongitudinalGrapheBuilderCourbe.connectVerticalSegment(model, ccm);
        if (etiquette != null) {
            model.setTitle(etiquette.geti18n() + BusinessMessages.ENTITY_SEPARATOR + reportRunVariableKey.getRunKey().getDisplayName());
        } else if (limite != null) {
            model.setTitle(limite.geti18n() + BusinessMessages.ENTITY_SEPARATOR + reportRunVariableKey.getRunKey().getDisplayName());
        }
        return model;
    }

    private boolean computeZ(ItemEnum etiquette, LitNommeLimite limite, DonPrtGeoProfilSection profil, TDoubleArrayList values) {
        boolean ok = false;
        if (etiquette != null) {
            CrueIOResu<List<EtiquetteIndexed>> etiquettesAsIndexed = new ValidateAndRebuildProfilSection(ccm, null).getEtiquettesAsIndexed(profil);
            DonPrtGeoProfilEtiquette foundPt = EtiquetteIndexed.find(etiquettesAsIndexed.getMetier(), etiquette);
            if (foundPt != null) {
                ok = true;
                values.add(foundPt.getPoint().getZ());
            }
        } else if (limite != null) {
            CrueIOResu<List<LitNommeIndexed>> asIndexedData = new ValidateAndRebuildProfilSection(ccm, null).getAsIndexedData(profil);
            if (asIndexedData != null && asIndexedData.getMetier() != null) {
                int ptIdx = ReportLongitudinalHelper.finPtIdx(asIndexedData, limite);
                if (ptIdx >= 0) {
                    ok = true;
                    values.add(profil.getPtProfil().get(ptIdx).getZ());
                }
            }
        }
        return ok;
    }
}
