/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import java.io.File;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.projet.report.persist.ReportConfigContrat;

/**
 *
 * @author Frederic Deniger
 */
public class ReportConfigSaverCallable implements SaveCallableContrat {

  private final ReportConfigSaver configSaver;
  private final ReportConfigContrat reportConfig;
  private final File targetFile;
  private final String name;

  public ReportConfigSaverCallable(ReportConfigSaver configSaver, ReportConfigContrat reportConfig, File targetFile, String name) {
    this.configSaver = configSaver;
    this.reportConfig = reportConfig;
    this.targetFile = targetFile;
    this.name = name;
  }

  @Override
  public File getTargetFile() {
    return targetFile;
  }

  @Override
  public CtuluLog call() throws Exception {
    return configSaver.save(targetFile, reportConfig, name);
  }
}
