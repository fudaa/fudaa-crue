/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.temporal;

import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntObjectHashMap;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.editor.CtuluValueEditorDuration;
import org.fudaa.ctulu.iterator.FixedIntegerIterator;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.data.*;
import org.fudaa.dodico.crue.projet.report.persist.ReportTemporalConfig;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.fudaa.crue.aoc.service.LhptService;
import org.fudaa.fudaa.crue.loi.ViewCourbeManager;
import org.fudaa.fudaa.crue.loi.common.CourbeModelWithKey;
import org.fudaa.fudaa.crue.loi.common.CourbesUiController;
import org.fudaa.fudaa.crue.loi.common.LoiConstanteCourbeModel;
import org.fudaa.fudaa.crue.report.config.AbstractReportGrapheBuilder;
import org.fudaa.fudaa.crue.report.helper.ReportVerticalTimeValueCourbeModel;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import java.util.*;

/**
 * @author Frederic Deniger
 */
public class ReportTemporalGrapheBuilder extends AbstractReportGrapheBuilder<CourbesUiController, ReportTemporalConfig> {
  private final LhptService lhptService = Lookup.getDefault().lookup(LhptService.class);

  public ReportTemporalGrapheBuilder(CourbesUiController uiController, ViewCourbeManager loiLabelsManager) {
    super(uiController, loiLabelsManager);
    EGAxeHorizontal axeX = uiController.getGraphe().getModel().getAxeX();
    axeX.setTitre(org.openide.util.NbBundle.getMessage(ReportTemporalGrapheBuilder.class, "AxeTime.Title"));
    axeX.setUniteVisible(false);
    // Les valeurs de l'axe X sont des durées en millisecondes.
    CtuluValueEditorDuration ed = new CtuluValueEditorDuration(true);
    axeX.setValueEditor(ed);
    // Définit que les données en X seront affichées suivant ce format.
    axeX.setSpecificFormat(ed.getDefaultFormatter());
  }

  @Override
  public void applyLabelsConfigChanged(ReportTemporalConfig content) {
    super.applyLabelsConfigChanged(content);
  }

  @Override
  public void updateResultCurvesAfterTimeChanged(ReportTemporalConfig content) {
    EGCourbeSimple timeCourbe = getTimeCourbe();
    if (timeCourbe != null) {
      EGModel createTimeModel = createTimeModel();
      if (createTimeModel != null) {
        timeCourbe.setModel(createTimeModel);
      }
    }
    uiController.getGraphe().fullRepaint();
  }

  @Override
  public List<EGCourbeSimple> getInternCourbesAfterTimeChanged(ReportTemporalConfig content, ProgressHandle progress) {
    return Collections.emptyList();
  }

  protected EGCourbeSimple getTimeCourbe() {
    EGCourbeSimple[] courbes = uiController.getEGGrapheSimpleModel().getCourbes();
    for (EGCourbeSimple courbe : courbes) {
      CourbeModelWithKey withKey = (CourbeModelWithKey) courbe.getModel();
      if (ReportVerticalTimeKey.isTimeKey(withKey.getKey())) {
        return courbe;
      }
    }
    return null;
  }

  @Override
  public EGAxeVertical getOrCreateAxeVertical(ItemVariable variable) {
    return super.getOrCreateAxeVertical(variable);
  }

  @Override
  public List<EGCourbeSimple> getInternCourbes(ReportTemporalConfig content, ProgressHandle progress) {
    Map<String, List<ReportRunKey>> keyByVar = ReportRunVariableHelper.getByVarName(content.getTemporalVariables());
    List<EGCourbeSimple> courbes = new ArrayList<>();
    if (reportService.getTimeContent() == null) {
      return courbes;
    }
    Map<String, EGAxeVertical> saved = new HashMap<>();
    List<Pair<ResultatTimeKey, Long>> timeByKey = reportService.getTimeByKeyList();

    for (String variableName : content.getVariables()) {
      if (!reportResultProviderService.isExpressionValid(variableName, content.getSelectedTimeStepInReport())) {
        continue;
      }
      PropertyNature property = reportResultProviderService.getPropertyNature(variableName);
      ReportVariableKey variableKey = reportResultProviderService.createVariableKey(variableName);
      EGAxeVertical axeVertical = getOrCreateAxeVerticalConfigured(property, content, saved);

      List<ReportRunKey> keys = keyByVar.get(variableName);
      if (keys != null) {
        for (String emhName : content.getEmhs()) {
          for (ReportRunKey runKey : keys) {
            ReportRunVariableEmhKey key = new ReportRunVariableEmhKey(runKey, variableKey, emhName);

            if (progress != null) {
              progress.setDisplayName(key.getDisplayName());
            }
            LoiConstanteCourbeModel model = createCourbeModel(timeByKey, key, content);
            EGCourbeSimple courbe = new EGCourbeSimple(axeVertical, model);
            AbstractReportGrapheBuilder.applyPersistConfig(content.getCourbeconfigs().get(key), courbe);
            courbes.add(courbe);
          }
        }
      }
    }
    //premier filtre pour ne prendre en compte que le cas transitoire.
    if (!timeByKey.isEmpty() && timeByKey.get(0).first.isTransitoire()) {
      if (content.getLhptLois() != null) {
        for (String lhptName : content.getLhptLois()) {
          if (lhptName == null) {
            continue;
          }
          if (progress != null) {
            progress.setDisplayName(lhptName);
          }
          ReportVariableKey variableKey = new ReportVariableKey(ReportVariableTypeEnum.LHPT, lhptName);
          ReportRunVariableEmhKey key = new ReportRunVariableEmhKey(reportService.getCurrentRunKey(), variableKey, "none");

          LoiConstanteCourbeModel loiConstanteCourbeModel = lhptService
              .createLoiCourbeModel(reportService.getRunCourant().getScenario(), key, reportService.getRunCourant().getManagerScenario(), lhptName, timeByKey);
          if (loiConstanteCourbeModel != null) {
            EGAxeVertical axeVertical = getOrCreateAxeVerticalConfigured(loiConstanteCourbeModel.getVariableAxeY().getNature(), content, saved);
            loiConstanteCourbeModel.setKey(key);

            EGCourbeSimple courbe = new EGCourbeSimple(axeVertical, loiConstanteCourbeModel);
            courbe.setTitle(lhptName);
            AbstractReportGrapheBuilder.applyPersistConfig(content.getCourbeconfigs().get(key), courbe);
            courbes.add(courbe);
          }
        }
      }
    }
    final EGCourbeSimple timeCourbe = new ReportTemporalTimeCourbe(null, createTimeModel());
    AbstractReportGrapheBuilder.applyPersistConfig(content.getTimeConfig(), timeCourbe);
    courbes.add(timeCourbe);
    updateAxeX(timeByKey);
    return courbes;
  }

  private EGModel createTimeModel() {
    ResultatTimeKey selectedTime = reportService.getSelectedTime();
    LoiConstanteCourbeModel create;
    if (selectedTime == null) {
      create = new ReportVerticalTimeValueCourbeModel();
    } else {
      long val = reportService.getTimeContent().getGlobalTimeForResultatKey(selectedTime);
      create = new ReportVerticalTimeValueCourbeModel(reportService, val);
    }
    create.setTitle(NbBundle.getMessage(ReportTemporalGrapheBuilder.class, "courbe.SelectedTimeStep.Name"));
    return create;
  }

  public LoiConstanteCourbeModel createCourbeModel(List<Pair<ResultatTimeKey, Long>> timeByKey, ReportRunVariableEmhKey key,
                                                   ReportTemporalConfig content) {
    TDoubleArrayList values = new TDoubleArrayList();
    TDoubleArrayList times = new TDoubleArrayList();
    TIntObjectHashMap<String> labels = new TIntObjectHashMap<>();

    for (Pair<ResultatTimeKey, Long> pair : timeByKey) {
      Double value = reportResultProviderService.
          getValue(pair.first, key.getRunVariableKey(), key.getEmhName(), content.getSelectedTimeStepInReport());
      if (value != null) {
        ResultatTimeKey equivalentTime = reportResultProviderService.getInitialTimeKey(pair.first, key.getReportRunKey(), key.getEmhName());
        labels.put(times.size(), equivalentTime == null ? StringUtils.EMPTY : equivalentTime.getNomCalcul());
        times.add(pair.second);
        values.add(value);
      }
    }
    ItemVariable varX = null;
    ItemVariable varY = reportResultProviderService.getCcmVariable(key.getRunVariableKey());
    LoiConstanteCourbeModel model = LoiConstanteCourbeModel.create(key, times, values, varX, varY);
    model.setLabels(labels);
    model.setLabelColumnName(NbBundle.getMessage(ReportTemporalGrapheBuilder.class, "Calcul.ColName"));
    model.setTitle(key.getDisplayName());
    return model;
  }

  public void updateAxeX(List<Pair<ResultatTimeKey, Long>> timeByKey) {
    boolean isPermanent = !timeByKey.isEmpty() && timeByKey.get(0).first.isPermanent();
    if (isPermanent) {
      int[] times = new int[timeByKey.size()];
      int idx = 0;
      for (Pair<ResultatTimeKey, Long> pair : timeByKey) {
        times[idx++] = pair.second.intValue();
      }
      super.uiController.getAxeX().setAxisIterator(new FixedIntegerIterator(times));
    } else {
      super.uiController.getAxeX().setAxisIterator(null);
    }
  }
}
