/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.config;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.view.DefaultOutlineViewEditor;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportMultiLabelContentUI {

  private final List<ReportVariableKey> availableVar;

  public ReportMultiLabelContentUI(List<ReportVariableKey> initAvailableVar) {
    availableVar = new ArrayList<>();
    availableVar.add(null);
    availableVar.addAll(initAvailableVar);
  }

  public List<ReportLabelContent> configure(List<ReportLabelContent> s) {
    LabelsEditor editor = createEditor(s);
    boolean ok = DialogHelper.showQuestionOkCancel(NbBundle.getMessage(ReportMultiLabelContentUI.class, "ConfigureLabels.Action"), editor);
    if (ok) {
      return getValuesFromEditor(editor);
    }
    return null;
  }

  public LabelsEditor createEditor(List<ReportLabelContent> s) {
    List<ReportLabelContentNode> nodes = new ArrayList<>();
    for (ReportLabelContent reportLabelContent : s) {
      nodes.add(new ReportLabelContentNode(reportLabelContent.copy(), availableVar));
    }
    LabelsEditor editor = new LabelsEditor();
    editor.getExplorerManager().setRootContext(NodeHelper.createNode(nodes, "", false));
    editor.setEditable(true);
    return editor;
  }

  public List<ReportLabelContent> getValuesFromEditor(LabelsEditor editor) {
    List<ReportLabelContent> res = new ArrayList<>();
    Node[] resNodes = editor.getExplorerManager().getRootContext().getChildren().getNodes();
    for (Node node : resNodes) {
      res.add(node.getLookup().lookup(ReportLabelContent.class));
    }
    return res;
  }

  public class LabelsEditor extends DefaultOutlineViewEditor {

    @Override
    protected void addElement() {
      ReportLabelContent newLabel = new ReportLabelContent();
      Children children = getExplorerManager().getRootContext().getChildren();
      final ReportLabelContentNode newNode = new ReportLabelContentNode(newLabel, availableVar);
      children.add(new Node[]{newNode});
    }

    @Override
    protected void createOutlineView() {
      view = new OutlineView(StringUtils.EMPTY);
      view.setPropertyColumns(
              ReportLabelContent.PROP_PREFIX, ReportLabelContent.geti18nPrefix(),
              ReportLabelContent.PROP_CONTENT, ReportLabelContent.geti18nContent(),
              ReportLabelContent.PROP_UNIT, ReportLabelContent.geti18nUnit());
      view.getOutline().setColumnHidingAllowed(false);
      view.getOutline().setRootVisible(false);
    }
  }
}
