/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.planimetry;

import com.jidesoft.swing.JideTabbedPane;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.result.OrdResExtractor;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryBrancheExtraData;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryCasierExtraData;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryNodeExtraData;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetrySectionExtraData;
import org.fudaa.dodico.crue.projet.report.planimetry.ReportPlanimetryTraceExtraData;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesAbstract;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.report.planimetry.ReportPlanimetryConfigDataUI.ExtraEditor;
import org.fudaa.fudaa.crue.report.service.ReportFormuleService;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.DialogDescriptor;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class ReportPlanimetryConfigUI {

  private final PlanimetryController planimetryController;
  final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
  final ReportFormuleService reportFormuleService = Lookup.getDefault().lookup(ReportFormuleService.class);
  final ReportResultProviderServiceContrat reportResultProviderServiceContrat = Lookup.getDefault().lookup(ReportResultProviderServiceContrat.class);

  public ReportPlanimetryConfigUI(PlanimetryController planimetryController) {
    this.planimetryController = planimetryController;
  }

  public boolean configure() {
    final ReportPlanimetryBrancheConfig brancheConfigurationExtra = (ReportPlanimetryBrancheConfig) planimetryController.getCqBranche().
            modeleDonnees().getBrancheConfigurationExtra();
    final ReportPlanimetryCasierConfig casierConfigurationExtra = (ReportPlanimetryCasierConfig) planimetryController.getCqCasier().modeleDonnees().
            getCasierConfigurationExtra();
    final ReportPlanimetryNodeConfig nodeConfigurationExtra = (ReportPlanimetryNodeConfig) planimetryController.getCqNoeud().modeleDonnees().
            getNodeConfigurationExtra();
    final ReportPlanimetrySectionConfig sectionConfigurationExtra = (ReportPlanimetrySectionConfig) planimetryController.getCqSection().
            modeleDonnees().getSectionConfigurationExtra();
    final ReportPlanimetryTraceConfig traceConfigurationExtra = (ReportPlanimetryTraceConfig) planimetryController.getCqTrace().modeleDonnees().
            getTraceConfigurationExtra();

    ExtraEditor brancheEditor = create(EnumCatEMH.BRANCHE).createEditor(ReportPlanimetryConfigDataUI.cloneData(brancheConfigurationExtra.getData()));
    ExtraEditor casierEditor = create(EnumCatEMH.CASIER).createEditor(ReportPlanimetryConfigDataUI.cloneData(casierConfigurationExtra.getData()));
    ExtraEditor nodeEditor = create(EnumCatEMH.NOEUD).createEditor(ReportPlanimetryConfigDataUI.cloneData(nodeConfigurationExtra.getData()));
    //pour les sections, les labels sont issus des sections et les couleurs sont appliquées au 2.
    ExtraEditor sectionEditor = create(EnumCatEMH.SECTION).createEditor(ReportPlanimetryConfigDataUI.cloneData(sectionConfigurationExtra.getData()));

    JideTabbedPane tabbedPane = new JideTabbedPane();
    tabbedPane.add(brancheEditor.getPanel(), EnumCatEMH.BRANCHE.geti18n());
    tabbedPane.add(casierEditor.getPanel(), EnumCatEMH.CASIER.geti18n());
    tabbedPane.add(nodeEditor.getPanel(), EnumCatEMH.NOEUD.geti18n());
    tabbedPane.add(sectionEditor.getPanel(), EnumCatEMH.SECTION.geti18n());
    BCalque calqueActif = planimetryController.getVisuPanel().getScene().getCalqueActif();
    if (calqueActif == planimetryController.getCqBranche()) {
      tabbedPane.setSelectedIndex(0);
    } else if (calqueActif == planimetryController.getCqCasier()) {
      tabbedPane.setSelectedIndex(1);
    } else if (calqueActif == planimetryController.getCqNoeud()) {
      tabbedPane.setSelectedIndex(2);
    } else if (calqueActif == planimetryController.getCqTrace() || calqueActif == planimetryController.getCqSection()) {
      tabbedPane.setSelectedIndex(3);
    }

    DialogDescriptor dialog = new DialogDescriptor(tabbedPane, org.openide.util.NbBundle.getMessage(ReportPlanimetryConfigUI.class,
            "DialogEditRapport.Name"));
    SysdocUrlBuilder.installDialogHelpCtx(dialog, "vuePlanimetrie_ConfigurationVuePlanimetrique", PerspectiveEnum.REPORT, false);
    boolean ok = DialogHelper.showQuestion(dialog, getClass(), DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    if (ok) {
      ReportVariableKey before = brancheConfigurationExtra.getData().getArrowVariable();
      brancheConfigurationExtra.setData((ReportPlanimetryBrancheExtraData) brancheEditor.apply());
      ReportVariableKey after = brancheConfigurationExtra.getData().getArrowVariable();
      if (before != after) {
        brancheConfigurationExtra.initFlecheRatio();
      }
      casierConfigurationExtra.setData((ReportPlanimetryCasierExtraData) casierEditor.apply());
      nodeConfigurationExtra.setData((ReportPlanimetryNodeExtraData) nodeEditor.apply());
      final ReportPlanimetrySectionExtraData sectionData = (ReportPlanimetrySectionExtraData) sectionEditor.apply();
      ReportPlanimetryTraceExtraData data = traceConfigurationExtra.getData();
      data.setBPalettePlageProperties(sectionData.getPalette());
      data.setUsePalette(sectionData.isUsePalette());
      data.setPaletteVariable(sectionData.getPaletteVariable());
      sectionConfigurationExtra.setData(sectionData);
      updatePalettes();
      planimetryController.getState().setStudyConfigModified();
    }

    return ok;
  }

  public void updatePalettes() {
    final ReportPlanimetryBrancheConfig brancheConfigurationExtra = (ReportPlanimetryBrancheConfig) planimetryController.getCqBranche().
            modeleDonnees().getBrancheConfigurationExtra();
    final ReportPlanimetryCasierConfig casierConfigurationExtra = (ReportPlanimetryCasierConfig) planimetryController.getCqCasier().modeleDonnees().
            getCasierConfigurationExtra();
    final ReportPlanimetryNodeConfig nodeConfigurationExtra = (ReportPlanimetryNodeConfig) planimetryController.getCqNoeud().modeleDonnees().
            getNodeConfigurationExtra();
    final ReportPlanimetrySectionConfig sectionConfigurationExtra = (ReportPlanimetrySectionConfig) planimetryController.getCqSection().
            modeleDonnees().getSectionConfigurationExtra();
    updatePalette(planimetryController.getCqBranche(), brancheConfigurationExtra);
    updatePalette(planimetryController.getCqCasier(), casierConfigurationExtra);
    updatePalette(planimetryController.getCqNoeud(), nodeConfigurationExtra);
    updatePalette(planimetryController.getCqSection(), sectionConfigurationExtra);
  }

  protected static void updatePalette(ZCalqueAffichageDonneesAbstract target, AbstractConfigurationExtra config) {
    if (config != null && config.getData() != null && config.getData().isUsePalette()) {
      target.setPaletteCouleurPlages(config.getData().getPalette());
    } else {
      target.setPaletteCouleurPlages(null);
    }
  }

  private ReportPlanimetryConfigDataUI create(EnumCatEMH catEMH) {
    EMHScenario scenario = reportService.getRunCourant().getScenario();
    OrdResExtractor extractor = new OrdResExtractor(scenario.getOrdResScenario());
    List<String> selectableVariablesName = extractor.getSelectableVariables(catEMH, reportFormuleService.getVariablesKeys());
    List<ReportVariableKey> selectableVariables = new ArrayList<>();
    if (selectableVariablesName != null) {
      for (String name : selectableVariablesName) {
        selectableVariables.add(reportResultProviderServiceContrat.createVariableKey(name));
      }
    }
    return new ReportPlanimetryConfigDataUI(selectableVariables, catEMH, reportService.getCurrentRunKey());
  }
}
