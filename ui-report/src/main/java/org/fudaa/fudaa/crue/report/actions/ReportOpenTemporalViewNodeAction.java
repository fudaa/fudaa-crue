/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.actions;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.projet.report.ReportContentType;
import org.fudaa.dodico.crue.projet.report.data.ReportExpressionHelper;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableTypeEnum;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.dodico.crue.projet.report.persist.ReportTemporalConfig;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.fudaa.fudaa.crue.report.service.ReportTemplateFileProvider;
import org.fudaa.fudaa.crue.report.temporal.ReportTemporalTopComponent;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

import java.util.*;

/**
 * @author Frederic Deniger
 */
public class ReportOpenTemporalViewNodeAction extends AbstractReportOpenTopComponentNodeAction {
  ReportService reportService = Lookup.getDefault().lookup(ReportService.class);

  public ReportOpenTemporalViewNodeAction() {
    this(NbBundle.getMessage(ReportOpenTemporalViewNodeAction.class, "OpenTemporal.ActionName"));
  }

  private ReportOpenTemporalViewNodeAction(final String name) {
    super(name, ReportTemporalTopComponent.MODE);
  }

  @Override
  protected TopComponent createNewTopComponentFor(final Node[] activatedNodes) {
    final Set<EMH> emhs = NodeHelper.getEMHs(activatedNodes);
    if (!emhs.isEmpty()) {
      final List<EMH> choosenEMH = ReportOpenMultiVarViewNodeAction.chooseEMHOfSameType(emhs);
      if (CollectionUtils.isNotEmpty(choosenEMH)) {
        final ReportTemporalTopComponent tc = create();
        tc.setReportConfig(createDefaultContent(choosenEMH), true);
        return tc;
      }
    }
    return null;
  }

  @Override
  protected TopComponent createNewTopComponentFor(final Node selectedNode) {
    final EMH emh = selectedNode.getLookup().lookup(EMH.class);
    if (emh != null) {
      final ReportTemporalTopComponent tc = create();
      tc.setReportConfig(createDefaultContent(Collections.singletonList(emh)), true);
      return tc;
    }

    return null;
  }

  @Override
  protected void postOpenNewTopComponent(final TopComponent openNew) {
    ((ReportTemporalTopComponent) openNew).chooseVariable();
  }

  public static void open(final Collection<EMH> emhs) {
    if (emhs == null) {
      return;
    }
    final ReportOpenTemporalViewNodeAction action = SystemAction.get(ReportOpenTemporalViewNodeAction.class);
    action.performAction(NodeHelper.createNodesWithLookup(emhs));
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    return NodeHelper.containEMH(activatedNodes);
  }

  private ReportTemporalConfig createDefaultContent(final List<EMH> emhs) {
    final ReportTemplateFileProvider template = new ReportTemplateFileProvider();
    ReportTemporalConfig content = template.loadTemplate(ReportContentType.TEMPORAL);
    if (content == null) {
      content = new ReportTemporalConfig();
      final List<LabelConfig> labels = new ArrayList<>();
      labels.add(createLabelTime());
      content.getLoiLegendConfig().setLabels(labels);
    }
    for (final EMH emh : emhs) {
      content.addEMH(emh.getNom());
    }

    return content;
  }

  private static LabelConfig createLabelTime() {
    final ReportLabelContent label = new ReportLabelContent();
    label.setContent(new ReportVariableKey(ReportVariableTypeEnum.TIME, ReportExpressionHelper.TIME_TEMPS_SCE));
    label
        .setPrefix(NbBundle.getMessage(ReportOpenTemporalViewNodeAction.class, "DefaultPrefixTempsSce"));
    label.setUnit(
        false);
    return createLabelConfig(label);
  }

  private static LabelConfig createLabelConfig(final ReportLabelContent label) {
    final LabelConfig res = new LabelConfig();
    res.setKey(label);
    return res;
  }

  public ReportTemporalTopComponent create() {
    ReportTemporalTopComponent res = super.getMainTopComponentAvailable();
    if (res == null) {
      res = new ReportTemporalTopComponent();
    }
    return res;
  }
}
