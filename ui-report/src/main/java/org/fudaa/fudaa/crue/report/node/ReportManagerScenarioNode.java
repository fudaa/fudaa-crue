/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.node;

import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frederic Deniger
 */
public class ReportManagerScenarioNode extends AbstractNode {

  public ReportManagerScenarioNode(ManagerEMHScenario managerScenario) {
    super(Children.create(new ReportRunChildFactory(managerScenario), false), Lookups.singleton(managerScenario));
    setName(managerScenario.getNom());
    setIconBaseWithExtension(CrueIconsProvider.getIconBase(EnumCatEMH.SCENARIO));
  }

  @Override
  public boolean canDestroy() {
    return false;
  }
}
