/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.service;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.fudaa.crue.aoc.service.LhptService;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.save.ConfigSaverExternProcessor;
import org.fudaa.fudaa.crue.planimetry.services.AdditionalLayersSaveServices;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.fudaa.fudaa.crue.report.view.ReportViewsSave;
import org.openide.util.Lookup;

/**
 * @author Frederic Deniger
 */
public class ReportSaver {
    final PerspectiveServiceReport perspectiveServiceReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);
    final ReportRunAlternatifService reportRunAlternatifService = Lookup.getDefault().lookup(ReportRunAlternatifService.class);
    final ReportVisuPanelService reportVisuPanelService = Lookup.getDefault().lookup(ReportVisuPanelService.class);
    final AdditionalLayersSaveServices additionalLayersSaveServices = Lookup.getDefault().lookup(AdditionalLayersSaveServices.class);
    final ReportFormuleService reportFormuleService = Lookup.getDefault().lookup(ReportFormuleService.class);
    final LhptService aocLHPTService = Lookup.getDefault().lookup(LhptService.class);

    public void save() {
        save(null);
    }

    public void save(Runnable runnableAfter) {
        ReportViewsSave saver = new ReportViewsSave();
        saver.save();
        reportRunAlternatifService.save();
        aocLHPTService.saveOnFiles();

        if (perspectiveServiceReport.isAdditionalLayersModified()) {
            //on sauvegarde les données externes:
            final EMHProjet project = reportRunAlternatifService.getProject();
            if (!project.getInfos().isDirOfConfigDefined()) {
                perspectiveServiceReport.setAdditionalLayersModified(false);
            } else {
                PlanimetryController planimetryController = reportVisuPanelService.getPlanimetryVisuPanel().getPlanimetryController();
                CtuluLogGroup gr = new ConfigSaverExternProcessor(project).saveAdditionalLayers(planimetryController);
                perspectiveServiceReport.setAdditionalLayersModified(false);
                additionalLayersSaveServices.setAdditionalLayerSavedFromReport(planimetryController);
                if (gr.containsSomething()) {
                    LogsDisplayer.displayError(gr, null);
                }
            }
        }
        reportFormuleService.save();
        perspectiveServiceReport.setDirty(false);
        if (runnableAfter != null) {
            runnableAfter.run();
        }
    }

    public void updatePerspectiveState() {
        boolean modified = reportRunAlternatifService.isModified() || aocLHPTService.isLoiMesureMesureModified() || perspectiveServiceReport
                .isAdditionalLayersModified() || reportFormuleService.modified;
        perspectiveServiceReport.setDirty(modified);
    }

    public void reload() {
        new ReportViewsSave().reloadIndex();
        aocLHPTService.reload();
        reportRunAlternatifService.reload();
        if (reportVisuPanelService.getPlanimetryVisuPanel() != null) {
            reportVisuPanelService.getPlanimetryVisuPanel().getPlanimetryController().cancel();
        }
        perspectiveServiceReport.setAdditionalLayersModified(false);
        reportFormuleService.reload();
        perspectiveServiceReport.setDirty(false);
    }
}
