package org.fudaa.fudaa.crue.report.actions;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.fudaa.crue.report.ReportDLHYTopComponent;
import org.fudaa.fudaa.crue.report.perspective.ActiveReport;
import org.fudaa.fudaa.crue.views.LoiDisplayer;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle;

/**
 * permet d'ouvrir la liste des frottements
 */
@ActionID(category = "View", id = "org.fudaa.fudaa.crue.report.actions.ReportOpenDLHYAction")
@ActionRegistration(displayName = "#ReportOpenDLHYAction.Name")
@ActionReferences({
  @ActionReference(path = ActiveReport.ACTIONS_REPORT_VIEWS, position = 3)
})
public final class ReportOpenDLHYAction extends AbstractOpenViewAction implements LoiDisplayer {

  public ReportOpenDLHYAction() {
    super(ReportDLHYTopComponent.MODE, ReportDLHYTopComponent.TOPCOMPONENT_ID);
    putValue(Action.NAME, NbBundle.getMessage(ReportOpenDLHYAction.class, "ReportOpenDLHYAction.Name"));
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    super.open();
  }

  @Override
  public void display(Loi loi) {
    ReportDLHYTopComponent open = (ReportDLHYTopComponent) open();
    open.selectLoi(loi);
    open.requestActive();


  }
}
