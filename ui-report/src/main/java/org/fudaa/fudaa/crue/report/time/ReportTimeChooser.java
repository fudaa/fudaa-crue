/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.time;

import com.jidesoft.swing.CheckBoxList;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import org.fudaa.ctulu.gui.CtuluPopupListener;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.fudaa.crue.common.helper.ToStringTransfomerCellRenderer;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportTimeChooser {

  final List<ResultatTimeKey> keys;
  final ToStringTransformer toStringTransformer;
  JPanel panel;
  CheckBoxList checkBoxList;

  public ReportTimeChooser(List<ResultatTimeKey> keys, ToStringTransformer toStringTransformer) {
    this.keys = keys;
    this.toStringTransformer = toStringTransformer;
  }

  protected void selectAll() {
    checkBoxList.selectAll();
  }

  protected void selectNone() {
    checkBoxList.selectNone();
  }

  public List<ResultatTimeKey> getSelected() {
    List<ResultatTimeKey> res = new ArrayList<>();
    int[] selectedIndices = checkBoxList.getCheckBoxListSelectedIndices();
    for (int i = 0; i < selectedIndices.length; i++) {
      res.add((ResultatTimeKey) checkBoxList.getModel().getElementAt(selectedIndices[i]));

    }
    return res;
  }

  protected void select(String nomCalc, boolean select) {
    int size = checkBoxList.getModel().getSize();
    for (int i = 0; i < size; i++) {
      ResultatTimeKey key = (ResultatTimeKey) checkBoxList.getModel().getElementAt(i);
      if (nomCalc.equals(key.getNomCalcul())) {
        if (select) {
          checkBoxList.addCheckBoxListSelectedIndex(i);
        } else {
          checkBoxList.removeCheckBoxListSelectedIndex(i);
        }
      }
    }
  }

  protected void selectPermanent(boolean select) {
    int size = checkBoxList.getModel().getSize();
    for (int i = 0; i < size; i++) {
      ResultatTimeKey key = (ResultatTimeKey) checkBoxList.getModel().getElementAt(i);
      if (key.isPermanent()) {
        if (select) {
          checkBoxList.addCheckBoxListSelectedIndex(i);
        } else {
          checkBoxList.removeCheckBoxListSelectedIndex(i);
        }
      }
    }
  }

  protected void selectTransitoire(boolean select) {
    int size = checkBoxList.getModel().getSize();
    for (int i = 0; i < size; i++) {
      ResultatTimeKey key = (ResultatTimeKey) checkBoxList.getModel().getElementAt(i);
      if (key.isTransitoire()) {
        if (select) {
          checkBoxList.addCheckBoxListSelectedIndex(i);
        } else {
          checkBoxList.removeCheckBoxListSelectedIndex(i);
        }
      }
    }
  }

  public JPanel getPanel() {
    if (panel == null) {
      createPanel();
    }
    return panel;
  }

  private void createPanel() {
    panel = new JPanel(new BorderLayout(2, 2));
    checkBoxList = new CheckBoxList(keys.toArray());
    checkBoxList.setCellRenderer(new ToStringTransfomerCellRenderer(toStringTransformer));
    panel.add(new JLabel(org.openide.util.NbBundle.getMessage(ReportTimeChooser.class, "TimeChooser.TitleLabel")), BorderLayout.NORTH);
    panel.add(new JScrollPane(checkBoxList));

    CtuluPopupListener.PopupReceiver receiver = new CtuluPopupListener.PopupReceiver() {
      @Override
      public void popup(MouseEvent _evt) {
        showPopup(_evt);
      }
    };
    new CtuluPopupListener(receiver, checkBoxList);

  }

  private void showPopup(MouseEvent _evt) {
    createMenu().show(checkBoxList, _evt.getX(), _evt.getY());
  }

  private JPopupMenu createMenu() {
    JPopupMenu menu = new JPopupMenu();
    TreeSet<String> calcul = new TreeSet<>();
    boolean containTransitoire = false;
    boolean containPermanent = false;
    for (ResultatTimeKey key : keys) {
      calcul.add(key.getNomCalcul());
      if (key.isTransitoire()) {
        containTransitoire = true;
      } else {
        containPermanent = true;
      }
    }
    menu.add(NbBundle.getMessage(ReportTimeChooser.class, "SelectTime.SelectAll")).addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        selectAll();
      }
    });
    menu.add(NbBundle.getMessage(ReportTimeChooser.class, "SelectTime.SelectNone")).addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        selectNone();
      }
    });
    if (containPermanent) {
      menu.addSeparator();
      menu.add(NbBundle.getMessage(ReportTimeChooser.class, "SelectTime.SelectPermanent")).addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          selectPermanent(true);
        }
      });
      menu.add(NbBundle.getMessage(ReportTimeChooser.class, "SelectTime.UnselectPermanent")).addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          selectPermanent(false);
        }
      });
    }
    if (containTransitoire) {
      menu.addSeparator();
      menu.add(NbBundle.getMessage(ReportTimeChooser.class, "SelectTime.SelectTransitoire")).addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          selectTransitoire(true);
        }
      });
      menu.add(NbBundle.getMessage(ReportTimeChooser.class, "SelectTime.UnselectTransitoire")).addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          selectTransitoire(false);
        }
      });
    }
    if (calcul.size() > 1) {
      for (final String calculName : calcul) {
        JMenu menuCalcul = new JMenu(calculName);
        menu.add(menuCalcul);
        menuCalcul.add(NbBundle.getMessage(ReportTimeChooser.class, "SelectTime.SelectAllCalcul")).addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            select(calculName, true);
          }
        });
        menuCalcul.add(NbBundle.getMessage(ReportTimeChooser.class, "SelectTime.UnselectAllCalcul")).addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            select(calculName, false);
          }
        });

      }
    }
    return menu;
  }
}
