package org.fudaa.fudaa.crue.report.longitudinal;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.emh.OrdResScenario;
import org.fudaa.dodico.crue.metier.emh.OrdResSection;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.helper.ResPrtHelper;
import org.fudaa.dodico.crue.metier.result.OrdResExtractor;
import org.fudaa.dodico.crue.metier.result.OrdResVariableSelection;
import org.fudaa.dodico.crue.projet.report.data.*;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalConfig;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.fudaa.crue.aoc.service.LhptValuesDisplayer;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.loi.res.CourbesUiResController;
import org.fudaa.fudaa.crue.report.AbstractReportTimeViewTopComponent;
import org.fudaa.fudaa.crue.report.export.ReportLongitudinalExportAction;
import org.fudaa.fudaa.crue.report.service.ReportFormuleService;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.*;
import java.util.List;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.report//ReportProfilLongitudinalTopComponent//EN",
    autostore = false)
@TopComponent.Description(preferredID = ReportProfilLongitudinalTopComponent.TOPCOMPONENT_ID,
    persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ReportProfilLongitudinalTopComponent.MODE, openAtStartup = false, position = 1)
public final class ReportProfilLongitudinalTopComponent extends AbstractReportTimeViewTopComponent<ReportLongitudinalConfig, ReportLongitudinalGrapheBuilder> implements LhptValuesDisplayer {
  public static final String MODE = "report-longitudinal";
  public static final String TOPCOMPONENT_ID = "ReportProfilLongitunalTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  final ReportFormuleService reportFormuleService = Lookup.getDefault().lookup(ReportFormuleService.class);

  /**
   * Contient toutes les données qui vont bien
   */
  public ReportProfilLongitudinalTopComponent() {
    setName(NbBundle.getMessage(ReportProfilLongitudinalTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ReportProfilLongitudinalTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
  }

  @Override
  public void lhptUpdated(ManagerEMHScenario scenario) {
    if (containsLhptFor(scenario)) {
      applyChangeInContent(false);
    }
  }

  /**
   *
   * @param scenario le scenario modifie
   * @return true si contient des données lhpt correspondantes
   */
  protected boolean containsLhptFor(ManagerEMHScenario scenario) {
    for (ReportRunVariableKey key : content.getProfilVariables()) {
      if (key.isLhpt()) {
        final String scenarioNom = scenario.getNom();
        if (key.getRunKey().isCourant()) {
          if (scenarioNom.equals(reportService.getRunCourant().getManagerScenario().getNom())) {
            return true;
          }
        } else {
          if (scenarioNom.equals(key.getRunKey().getManagerNom())) {
            return true;
          }
        }
      }
    }
    return false;
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueProfilLongitudinal", PerspectiveEnum.REPORT);
  }

  @Override
  protected ReportLongitudinalGrapheBuilder createBuilder() {
    return new ReportLongitudinalGrapheBuilder(profilUiController, loiLegendManager);
  }

  @Override
  public void timeFormatChanged() {
    builder.updateLabelsAndTitleContent(content);
    builder.updateCourbeNames(content);
  }

  public void chooseBranches() {
    ReportLongitudinalBrancheChooser chooser = new ReportLongitudinalBrancheChooser(content);
    if (chooser.displayChooseUI()) {
      propagateChange();
      profilUiController.getGraphe().restore();
    }
  }

  @Override
  public void alternatifRunChanged() {
    Set<ReportRunKey> reportRunContents = reportRunAlternatifService.getAvailableRunKeys();
    boolean modified = false;
    for (Iterator<ReportRunVariableKey> it = content.getProfilVariables().iterator(); it.hasNext(); ) {
      ReportRunVariableKey reportRunVariableKey = it.next();
      final ReportRunKey reportRunKey = reportRunVariableKey.getReportRunKey();
      if (reportRunKey.isAlternatifRun() && !reportRunContents.contains(reportRunKey)) {
        it.remove();
        modified = true;
      }
    }
    for (Iterator<ReportRunVariableTimeKey> it = content.getCourbeconfigs().keySet().iterator(); it.hasNext(); ) {
      ReportRunVariableTimeKey reportRunVariableTimeKey = it.next();
      if (reportRunVariableTimeKey == null) {
        it.remove();
        modified = true;
      } else {
        ReportRunKey reportRunKey = reportRunVariableTimeKey.getReportRunKey();
        if (reportRunKey.isAlternatifRun() && !reportRunContents.contains(reportRunKey)) {
          it.remove();
          modified = true;
        }
      }
    }
    if (modified) {
      applyChangeInContent(false);
      setModified(true);
    }
  }

  /**
   * @return les actions complémentaires à ajouter dans le bouton de configuration.
   */
  @Override
  protected List<? extends Action> getAdditionalActionsToAddConfigureButton() {
    List<Action> res = new ArrayList<>();
    EbliActionSimple configureCartouche;
    configureCartouche = new EbliActionSimple(NbBundle.getMessage(ReportProfilLongitudinalTopComponent.class, "BannerConfiguration.DialogTitle"), null,
        "CONFIGURE_BANNER") {
      @Override
      public void actionPerformed(ActionEvent _e) {
        configureBanner();
      }
    };
    res.add(configureCartouche);
    res.addAll(super.getCourbesUiController().getEditActions());
    return res;
  }

  protected void configureBanner() {
    ReportLongitudinalBannerConfigUI bannerConfigUI = new ReportLongitudinalBannerConfigUI();
    if (bannerConfigUI.configure(content.getBannerConfig())) {
      profilUiController.getGraphe().fullRepaint();
      setModified(true);
    }
  }

  protected void chooseTimes() {
    ReportLongitudinalTimeChooser chooser = new ReportLongitudinalTimeChooser(content);
    if (chooser.displayChooseUI()) {
      propagateChange();
      profilUiController.getGraphe().restore();
    }
  }

  public List<String> getChoosableVariablesForExport() {
    final List<String> choosableVariables = new ReportLongitudinalRightVariableChooser(content).getChoosableVariables();
    //ajout des variables z
    ReportResultProviderService reportResultProviderService = Lookup.getDefault().lookup(ReportResultProviderService.class);
    EMHScenario scenario = reportService.getRunCourant().getScenario();
    OrdResExtractor extractor = new OrdResExtractor(scenario.getOrdResScenario());
    List<String> selectableVariables = extractor.getSelectableVariables(EnumCatEMH.SECTION, reportFormuleService.getVariablesKeys());
    //on ne conserve que les variables qui sont affichées sur l'axe des z:
    for (Iterator<String> it = selectableVariables.iterator(); it.hasNext(); ) {
      String string = it.next();
      if (ReportRunVariableHelper.isNatZVar(string, reportResultProviderService)) {
        choosableVariables.add(string);
        it.remove();
      }
    }
    // ajout Zini issu du RPTI
    choosableVariables.addAll(ResPrtHelper.getZiniVar());
    choosableVariables.addAll(ResPrtHelper.getQiniVar());
    return choosableVariables;
  }

  protected void chooseRightVariable() {
    ReportLongitudinalRightVariableChooser chooser = new ReportLongitudinalRightVariableChooser(content);
    if (chooser.displayChooseUI()) {
      propagateChange();
      profilUiController.getGraphe().restore();
    }
  }

  protected void chooseAsZVariable() {
    ReportLongitudinalAsZVariableChooser chooser = new ReportLongitudinalAsZVariableChooser(content);
    if (chooser.displayChooseUI()) {
      propagateChange();
      profilUiController.getGraphe().restore();
    }
  }

  @Override
  protected void buildComponents() {
    initComponents();
    JPanel pnTable = new JPanel(new BorderLayout(5, 5));
    pnTable.add(profilUiController.getTableGraphePanel());
    profilUiController.installComboxSelector();
    JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, loiLegendManager.getPanel(), pnTable);
    splitPane.setDividerLocation(550);
    splitPane.setOneTouchExpandable(true);
    splitPane.setContinuousLayout(true);
    add(splitPane);
  }

  @Override
  protected CourbesUiResController createCourbesUiController() {
    CourbesUiResController res = new ReportLongitudinalCourbesUiResController();
    res.addToolbarAction(new ReportLongitudinalExportAction(this));
    return res;
  }

  @Override
  protected void saveCourbeConfig(EGCourbePersist persitUiConfig, ReportKeyContract key) {
    content.getCourbeconfigs().put((ReportRunVariableTimeKey) key, persitUiConfig);
  }

  protected void chooseSection() {
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables

  @Override
  protected void componentOpenedHandler() {
  }

  void writeProperties(java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
    p.setProperty("version", "1.0");
  }

  void readProperties(java.util.Properties p) {
  }

  @Override
  public List<EbliActionInterface> getMainActions() {
    EbliActionInterface chooseBranches = new EbliActionSimple(NbBundle.getMessage(ReportProfilLongitudinalTopComponent.class,
        "ChooseBranches.ActionName"), null, "BRANCHES") {
      @Override
      public void actionPerformed(ActionEvent _e) {
        chooseBranches();
      }
    };
    EbliActionInterface chooseTimes = new EbliActionSimple(NbBundle.getMessage(ReportProfilLongitudinalTopComponent.class, "ChooseTimes.ActionName"),
        null, "TIMES") {
      @Override
      public void actionPerformed(ActionEvent _e) {
        chooseTimes();
      }
    };
    EbliActionInterface chooseAsZVariable = new EbliActionSimple(NbBundle.getMessage(ReportProfilLongitudinalTopComponent.class,
        "ChooseAsZVariable.ActionName"), null, "RIGHT_VAR") {
      @Override
      public void actionPerformed(ActionEvent _e) {
        chooseAsZVariable();
      }
    };
    EbliActionInterface chooseRightVar = new EbliActionSimple(NbBundle.getMessage(ReportProfilLongitudinalTopComponent.class,
        "ChooseRightVariable.ActionName"), null, "AS_Z") {
      @Override
      public void actionPerformed(ActionEvent _e) {
        chooseRightVariable();
      }
    };
    return Arrays.asList(chooseBranches, chooseTimes, chooseAsZVariable, chooseRightVar, createConfigExternAction());
  }

  @Override
  public List<ReportVariableKey> getTitleAvailableVariables() {
    List<ReportVariableKey> res = new ArrayList<>();
    res.add(new ReportVariableKey(ReportVariableTypeEnum.EXPR, ReportExpressionHelper.EXPR_NOM));
    res.add(new ReportVariableKey(ReportVariableTypeEnum.EXPR, ReportExpressionHelper.EXPR_COMMENTAIRE));
    res.add(new ReportVariableKey(ReportVariableTypeEnum.EXPR, ReportExpressionHelper.EXPR_PROFIL_COMMENTAIRE));
    res.add(new ReportVariableKey(ReportVariableTypeEnum.TIME, ReportExpressionHelper.TIME_NOM_CALC));
    res.add(new ReportVariableKey(ReportVariableTypeEnum.TIME, ReportExpressionHelper.TIME_TEMPS_SIMU));
    res.add(new ReportVariableKey(ReportVariableTypeEnum.TIME, ReportExpressionHelper.TIME_TEMPS_SCE));
    OrdResScenario ordResScenario = getScenario().getOrdResScenario();
    OrdResExtractor extract = new OrdResExtractor(ordResScenario);
    OrdResVariableSelection sectionVariable = extract.getOrdResSelection(OrdResSection.KEY, reportFormuleService.getVariablesKeys());
    List<String> selectableVariables = sectionVariable.getSelectableVariables();
    for (String var : selectableVariables) {
      res.add(builder.getReportResultProviderService().createVariableKey(var));
    }
    return res;
  }

  @Override
  public ReportLongitudinalConfig getReportConfig() {
    return content;
  }
}
