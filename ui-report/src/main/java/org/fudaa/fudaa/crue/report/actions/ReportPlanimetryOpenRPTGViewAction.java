/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.actions;

import java.awt.event.ActionEvent;
import java.util.List;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 * L'action permettant d'ouvir une vue multi-var depuis la vue planimetrique
 *
 * @author Frederic Deniger
 */
public class ReportPlanimetryOpenRPTGViewAction extends EbliActionSimple {

  private final PlanimetryController planimetryController;
  final PerspectiveServiceReport perspectiveServiceReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);

  public ReportPlanimetryOpenRPTGViewAction(final PlanimetryController planimetryController) {
    super(NbBundle.getMessage(ReportPlanimetryOpenRPTGViewAction.class, "OpenRPTG.ActionName"), null, "OPEN_RPTG");
    this.planimetryController = planimetryController;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    final List<EMH> selectedEMHs = planimetryController.getVisuPanel().getSelectedEMHs();
    ReportOpenRPTGViewNodeAction.open(selectedEMHs);
  }

  @Override
  public void updateStateBeforeShow() {
    List<EMH> selectedEMHs = planimetryController.getVisuPanel().getSelectedEMHs();
    setEnabled(perspectiveServiceReport.isInEditMode() && !selectedEMHs.isEmpty()
            && EMHHelper.containsEMHWithCats(selectedEMHs, EnumCatEMH.BRANCHE, EnumCatEMH.SECTION, EnumCatEMH.NOEUD));
  }
}
