/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.config;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.extern.ExternContent;
import org.fudaa.dodico.crue.io.extern.ExternContentReader;
import org.fudaa.dodico.crue.projet.report.persist.AbstractReportCourbeConfig;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Exceptions;

/**
 *
 * @author Frederic Deniger
 */
public class ExternFileBuilderProcessor implements ProgressRunnable<CtuluLogGroup> {

  private final ExternFileBuilder builder;
  private final AbstractReportCourbeConfig config;

  public ExternFileBuilderProcessor(ExternFileBuilder builder, AbstractReportCourbeConfig config) {
    this.builder = builder;
    this.config = config;
  }

  public boolean isSomethingToLoad() {
    for (String externFile : config.getExternFiles()) {
      ExternContent content = builder.getContentsByFilename().get(externFile);
      if (content == null) {
        return true;
      }
    }
    return false;
  }

  @Override
  public CtuluLogGroup run(ProgressHandle handle) {
    if (handle != null) {
      handle.switchToIndeterminate();
    }
    CtuluLogGroup groupe = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    final List<String> externFiles = config.getExternFiles();
    List<ReadCallable> filesToLoad = new ArrayList<>();
    Map<String, ExternContent> newContentsByFilename = new HashMap<>();
    for (String externFile : externFiles) {
      if (externFile == null) {
        continue;
      }
      ExternContent content = builder.getContentsByFilename().get(externFile);
      if (content != null) {
        newContentsByFilename.put(externFile, content);
      } else {
        filesToLoad.add(new ReadCallable(new File(externFile)));
      }
    }
    if (!filesToLoad.isEmpty()) {
      ExecutorService newCachedThreadPool = Executors.newFixedThreadPool(2);
      try {
        List<Future<CrueIOResu<ExternContent>>> invokeAll = newCachedThreadPool.invokeAll(filesToLoad);
        for (Future<CrueIOResu<ExternContent>> future : invokeAll) {
          CrueIOResu<ExternContent> resultat = future.get();
          if (resultat != null) {
            if (resultat.getMetier() != null) {
              newContentsByFilename.put(resultat.getAnalyse().getResource(), resultat.getMetier());
            } else {
              groupe.addLog(resultat.getAnalyse());
            }
          }
        }
      } catch (Exception ex) {
        Exceptions.printStackTrace(ex);
      } finally {
        newCachedThreadPool.shutdownNow();
      }
    }
    builder.getContentsByFilename().clear();
    builder.getContentsByFilename().putAll(newContentsByFilename);
    return groupe;
  }

  protected static class ReadCallable implements Callable<CrueIOResu<ExternContent>> {

    final File file;

    public ReadCallable(File file) {
      this.file = file;
    }

    @Override
    public CrueIOResu<ExternContent> call() throws Exception {
      return new ExternContentReader().read(file);
    }
  }
}
