/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.time;

import com.memoire.bu.BuResource;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.IntegerRange;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.metier.transformer.ToStringTransformerDefault;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.openide.DialogDescriptor;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportTimeStepTopPanel extends Observable implements Observer {

  public static final String RANGE_MODIFIED = "RANGE";
  private final ReportTimeStepSliderPanel slider = new ReportTimeStepSliderPanel();
  private JPanel panel;
  private List values = Collections.emptyList();
  private JLabel mainLabel;
  private JLabel subLabel;
  private JButton btFullExtent;
  private JButton btReduceTime;
  private JButton btParameter;
  private ToStringTransformer mainLabeltoStringTransformer = ToStringTransformerDefault.TO_STRING;
  private ToStringTransformer subLabeltoStringTransformer = ToStringTransformerDefault.TO_STRING;

  public ReportTimeStepSliderPanel getSliderPanel() {
    return slider;
  }

  public JButton getBtParameter() {
    return btParameter;
  }

  public JPanel getPanel() {
    if (panel == null) {
      createPanel();
    }
    return panel;
  }
  IntegerRange reducedRange;

  public int getNbTimeStepWillBePlayed() {
    int current = slider.getSelectedValue();
    return slider.getNbValues() - current;
  }

  @Override
  public void update(Observable o, Object arg) {
    updateLabel();
  }

  void createPanel() {
    panel = new JPanel(new BorderLayout());

    mainLabel = new JLabel();
    mainLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    mainLabel.setHorizontalAlignment(SwingConstants.CENTER);
    subLabel = new JLabel();
    subLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    subLabel.setHorizontalAlignment(SwingConstants.CENTER);

    btFullExtent = ReportTimeStepSliderPanel.create("fullExtent.png");
    btParameter = ReportTimeStepSliderPanel.create(null);
    btParameter.setToolTipText(NbBundle.getMessage(ReportTimeStepTopPanel.class, "timeParameter.ButtonName"));
    btParameter.setIcon(BuResource.BU.getIcon("crystal_parametre"));

    btFullExtent.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        fullExtent();
      }
    });
    btFullExtent.setToolTipText(NbBundle.getMessage(ReportTimeStepTopPanel.class, "FullTimeExtent.Tooltip"));
    btReduceTime = ReportTimeStepSliderPanel.create("shrink.png");
    btReduceTime.setToolTipText(NbBundle.getMessage(ReportTimeStepTopPanel.class, "ReduceTimeExtent.Tooltip"));
    btReduceTime.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        shrink();
      }
    });
    JPanel pnLast = new JPanel(new FlowLayout(FlowLayout.LEFT));
    pnLast.add(btReduceTime);
    pnLast.add(btFullExtent);
    JPanel pnFirst = new JPanel(new FlowLayout(FlowLayout.LEFT));
    pnFirst.add(btParameter);
    pnFirst.add(slider.getVideoRecorder().getBtVideo());
    JPanel top = new JPanel(new BorderLayout());
    top.add(mainLabel, BorderLayout.NORTH);
    top.add(subLabel);
    top.add(pnFirst, BorderLayout.WEST);
    top.add(pnLast, BorderLayout.EAST);
    panel.add(top, BorderLayout.NORTH);
    panel.add(slider.getPanel());
    slider.addObserver(this);
    updateComponents();
  }

  protected void fullExtent() {
    slider.setValues(values, true);
    if (reducedRange != null) {
      reducedRange = null;
      setChanged();
      notifyObservers(RANGE_MODIFIED);
    }
    updateButtons();
  }

  protected void shrink() {
    ReportTimeReduceRangePanel reducePanelBuilder = new ReportTimeReduceRangePanel(values, subLabeltoStringTransformer, reducedRange);
    final JPanel reducePanel = reducePanelBuilder.getPanel();
    DialogDescriptor dial = new DialogDescriptor(reducePanel, NbBundle.getMessage(ReportTimeStepTopPanel.class, "ReduceTime.DialogTitle"));
    boolean ok = DialogHelper.showQuestion(dial, ReportTimeReduceRangePanel.class, false);
    if (ok) {
      IntegerRange newRange = reducePanelBuilder.getRange();
      if (!ObjectUtils.equals(newRange, reducedRange)) {
        reducedRange = newRange;
        List reduceValue = new ArrayList();
        for (int i = reducedRange.getMinimum(); i <= reducedRange.getMaximum(); i++) {
          reduceValue.add(values.get(i));
        }
        slider.setValues(reduceValue, true);
        updateButtons();
        setChanged();
        notifyObservers(RANGE_MODIFIED);
      }
    }
  }

  protected String toStringMainLabel(Object o) {
    if (o == null) {
      return StringUtils.EMPTY;
    }
    return mainLabeltoStringTransformer.transform(o);
  }

  protected String toStringSubLabel(Object o) {
    if (o == null) {
      return StringUtils.EMPTY;
    }
    return subLabeltoStringTransformer.transform(o);
  }

  protected void updateComponents() {
    updateLabel();
  }

  public void setToStringTransformer(ToStringTransformer subLabel, ToStringTransformer mainLabel) {
    subLabeltoStringTransformer = subLabel;
    mainLabeltoStringTransformer = mainLabel;
    updateLabel();
  }

  protected void updateLabel() {
    mainLabel.setText(toStringMainLabel(slider.getSelectedItem()));
    mainLabel.setToolTipText(mainLabel.getText());
    subLabel.setText(toStringSubLabel(slider.getSelectedItem()));
    subLabel.setToolTipText(subLabel.getText());
  }

  protected void updateButtons() {
    btFullExtent.setEnabled(reducedRange != null);
    btReduceTime.setEnabled(!values.isEmpty());
  }

  public void setValues(List values) {
    reducedRange = null;
    this.values = values;
    slider.setValues(values, false);
    updateButtons();
  }

  public void setEditable(boolean b) {
    slider.setEditable(b);
    btFullExtent.setEnabled(b);
    btReduceTime.setEnabled(b);
    btParameter.setEnabled(b);
  }
}
