/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.contrat;

import org.fudaa.dodico.crue.projet.report.formule.FormuleDataChanges;
import org.fudaa.fudaa.crue.report.ReportVisualTopComponent;

/**
 * @author Frederic Deniger
 */
public interface ReportViewServiceContrat {
    /**
     * @param reportVisualTopComponent le TopComponent dans lequel les modifications ont ete effectuées également
     */
    void changesDoneIn(ReportVisualTopComponent reportVisualTopComponent);

    /**
     * appele pour propager les modifications.
     *
     * @param changes les modifications effectuées.
     */
    void variablesUpdated(FormuleDataChanges changes);
}
