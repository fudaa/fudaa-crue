/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.persist;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.fudaa.dodico.crue.projet.report.persist.AbstractReportConfigDao;

/**
 *
 * @author Frederic Deniger
 */
@XStreamAlias("PlanimetrieConfig")
public class ReportConfigPlanimetryDao extends AbstractReportConfigDao<ReportPlanimetryConfigPersist> {

  @XStreamAlias("Planimetrie")
  private ReportPlanimetryConfigPersist config;

  public ReportConfigPlanimetryDao() {
  }

  public ReportConfigPlanimetryDao(ReportPlanimetryConfig config) {
    this.config = new ReportPlanimetryConfigPersist(config);
  }

  @Override
  public ReportPlanimetryConfigPersist getConfig() {
    return config;
  }

  @Override
  public void setConfig(ReportPlanimetryConfigPersist config) {
    this.config = config;
  }
}
