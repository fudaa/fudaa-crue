/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.helper;

import org.fudaa.dodico.crue.common.SafeComparator;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.fudaa.crue.report.temporal.ReportTemporalTimeCourbe;

/**
 *
 * @author Frederic Deniger
 */
public class CourbeComparator extends SafeComparator<EGCourbe> {

  @Override
  protected int compareSafe(EGCourbe o1, EGCourbe o2) {
    if (o1.getClass().equals(ReportTemporalTimeCourbe.class)) {
      return -1;
    } else if (o2.getClass().equals(ReportTemporalTimeCourbe.class)) {
      return 1;
    }
    return compareString(o1.getTitle(), o2.getTitle());
  }
}
