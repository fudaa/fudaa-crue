/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.contrat;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;

/**
 *
 * @author Frederic Deniger
 */
public interface ReportReadResultServiceContrat {

  Double getReadValue(ReportRunVariableKey key, String emhNom);

  Double getReadValue(ResultatTimeKey selectedTime, ReportRunVariableKey key, String emhNom);

  CrueConfigMetier getCcm();
}
