/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.service;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.report.data.ReportRunContent;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.fudaa.fudaa.crue.report.view.persist.RunAlternatifConfigLoaderProcessor;
import org.fudaa.fudaa.crue.report.view.persist.RunAlternatifConfigSaverProcessor;
import org.fudaa.fudaa.crue.study.services.CrueService;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ServiceProvider;

import java.io.File;
import java.util.*;

/**
 * Permet de gérer les runs alternatifs.
 * * <table  border="1"  cellpadding="3" cellspacing="0"  summary="lookups">
 * <tr>
 * <th align="left">Lookup</th>
 * <th align="left">Commentaire</th>
 * <th align="left">Méthode utilisant le lookup</th>
 * </tr>
 * <tr>
 * <td>Liste de {@link ReportRunContent}</td>
 * <td>	Les conteneurs de run atlernatifs</td>
 * <td>
 * <code>{@link #getReportRunContents()} }</code>
 * <code>{@link #getRunContent(ReportRunKey)}</code>
 * <code>{@link #getRunContentLoaded(ReportRunKey)} </code>
 * </td>
 * </tr>
 * </table>
 * F
 *
 * @author Frederic Deniger
 */
@ServiceProvider(service = ReportRunAlternatifService.class)
public class ReportRunAlternatifService implements Lookup.Provider {
    private final static String RUN_FILE_NAME = "runs.xml";
    private final CrueService crueService = Lookup.getDefault().lookup(CrueService.class);
    private final PerspectiveServiceReport perspectiveServiceReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);
    private boolean runAlternatifLoaded;
    private boolean modified;
    private final InstanceContent dynamicContent = new InstanceContent();
    private final Lookup lookup = new AbstractLookup(dynamicContent);

    public ReportRunAlternatifService() {
        crueService.addEMHProjetStateLookupListener(ev -> projectChanged());
    }

    public boolean isModified() {
        return modified;
    }

    private void setModified() {
        this.modified = true;
        perspectiveServiceReport.setDirty(true);
    }

    public void save() {
        if (modified) {
            File target = getTargetFile();
            RunAlternatifConfigSaverProcessor processor = new RunAlternatifConfigSaverProcessor(target, getSortedReportRunContentKeys());
            final String action = NbBundle.getMessage(ReportRunAlternatifService.class, "RunAlternatif.SaveAction");
            CtuluLog log = CrueProgressUtils.showProgressDialogAndRun(processor, action);
            if (log.isNotEmpty()) {
                LogsDisplayer.displayError(log, action);
            }
            modified = false;
        }
    }

    public void loadRuns() {
        final EMHProjet projectLoaded = getProject();
        if (projectLoaded == null || runAlternatifLoaded) {
            return;
        }
        runAlternatifLoaded = true;
        final File targetFile = getTargetFile();
        if (!targetFile.exists()) {
            return;
        }
        RunAlternatifConfigLoaderProcessor processor = new RunAlternatifConfigLoaderProcessor(targetFile);
        final String action = NbBundle.getMessage(ReportRunAlternatifService.class, "RunAlternatif.LoadAction");
        CrueIOResu<List<ReportRunKey>> ioResu = CrueProgressUtils.showProgressDialogAndRun(processor, action);
        CtuluLog log = ioResu.getAnalyse();
        if (log.isNotEmpty()) {
            LogsDisplayer.displayError(log, action);
        }
        boolean modifiedWhileLoading = false;
        if (ioResu.getMetier() != null) {
            for (ReportRunKey reportRunKey : ioResu.getMetier()) {
                String managerId = reportRunKey.getManagerNom();
                String runId = reportRunKey.getRunAlternatifId();
                Pair<ManagerEMHScenario, EMHRun> managers = getManagers(managerId, runId);
                ManagerEMHScenario manager = managers.first;
                EMHRun run = managers.second;
                if (manager == null) {
                    modifiedWhileLoading = true;
                    log.addWarn(NbBundle.getMessage(ReportRunAlternatifService.class, "RunAlternatif.ScenarioDeleted", managerId));
                } else {
                    if (run == null) {
                        modifiedWhileLoading = true;
                        log.addWarn(NbBundle.getMessage(ReportRunAlternatifService.class, "RunAlternatif.RunDeleted", managerId, runId));
                    } else {
                        ReportRunContent report = new ReportRunContent(run, manager);
                        dynamicContent.add(report);
                    }
                }
            }
        }
        if (log.isNotEmpty()) {
            LogsDisplayer.displayError(log, action);
        }
        if (modifiedWhileLoading) {
            setModified();
        }
    }

    private Pair<ManagerEMHScenario, EMHRun> getManagers(ReportRunContent content) {
        return getManagers(content.getManagerScenario().getNom(), content.getRun().getId());
    }

    private Pair<ManagerEMHScenario, EMHRun> getManagers(String managerId, String runId) {
        return RunAlternatifLoaderProcessor.getManagers(getProject(), managerId, runId);
    }

    private void projectChanged() {
        if (!runAlternatifLoaded) {
            return;
        }
        if (crueService.isProjetCurrentlyReloading() && getProject() == null) {
            return;
        }

        //etude fermée:
        if (getProject() == null) {
            clearAll();
        } else {
            Collection<? extends ReportRunContent> lookupAll = new ArrayList<>(lookup.lookupAll(ReportRunContent.class));
            for (ReportRunContent reportRunContent : lookupAll) {
                Pair<ManagerEMHScenario, EMHRun> managers = getManagers(reportRunContent);
                if (managers.first == null || managers.second == null) {
                    dynamicContent.remove(reportRunContent);
                    setModified();
                }
            }
        }
    }

    /**
     * Ajoute le runId à la liste des runs alternatifs et le charge pas
     *
     * @param parent le scenario parent
     * @param run    le run
     */
    public void addRun(ManagerEMHScenario parent, EMHRun run) {
        ReportRunContent exist = getRunContent(run);
        setModified();
        if (exist == null) {
            ReportRunContent report = new ReportRunContent(run, parent);
            dynamicContent.add(report);
            RunAlternatifLoaderProcessor processor = new RunAlternatifLoaderProcessor(report, getProject());
            CrueProgressUtils.showProgressDialogAndRun(processor,
                    NbBundle.getMessage(ReportRunAlternatifService.class, "RunAlternatif.LoadRun", parent.getNom(), run.getNom()));
            if (report.getScenario() == null) {
                removeRun(run);
            }
        }
    }

    private ReportRunContent getRunContent(EMHRun run) {
        if (run == null) {
            return null;
        }
        Collection<? extends ReportRunContent> lookupAll = getReportRunContents();
        for (ReportRunContent reportRunContent : lookupAll) {
            if (run.getId().equals(reportRunContent.getRun().getId())) {
                return reportRunContent;
            }
        }
        return null;
    }

    public List<EMHRun> getRunAlternatif() {
        Collection<? extends ReportRunContent> lookupAll = lookup.lookupAll(ReportRunContent.class);
        List<EMHRun> res = new ArrayList<>();
        for (ReportRunContent reportRunContent : lookupAll) {
            res.add(reportRunContent.getRun());
        }
        return res;
    }

    public void removeRun(EMHRun run) {
        ReportRunContent runContent = getRunContent(run);
        if (runContent != null) {
            dynamicContent.remove(runContent);
            setModified();
        }
    }

    @Override
    public Lookup getLookup() {
        return lookup;
    }

    private ReportRunContent getRunContent(ReportRunKey key) {
        if (key == null) {
            return null;
        }
        Collection<? extends ReportRunContent> lookupAll = lookup.lookupAll(ReportRunContent.class);
        for (ReportRunContent reportRunContent : lookupAll) {
            if (key.equals(reportRunContent.getRunKey())) {
                return reportRunContent;
            }
        }

        return null;
    }

    public List<ReportRunKey> getSortedReportRunContentKeys() {
        Collection<? extends ReportRunContent> reportRunContents = getReportRunContents();
        List<ReportRunKey> res = new ArrayList<>();
        for (ReportRunContent reportRunContent : reportRunContents) {
            res.add(reportRunContent.getRunKey());
        }
        res.sort(ObjetNommeByNameComparator.INSTANCE);
        return res;
    }

    /**
     * @return la liste de tous les runs alternatifs pris en charge
     */
    private Collection<? extends ReportRunContent> getReportRunContents() {
        return lookup.lookupAll(ReportRunContent.class);
    }

    /**
     * @return la liste des cles {@link ReportRunKey} des runs alternatifs
     */
    public Set<ReportRunKey> getAvailableRunKeys() {
        Set<ReportRunKey> availableKeys = new HashSet<>();
        Collection<? extends ReportRunContent> reportRunContents = getReportRunContents();
        for (ReportRunContent reportRunContent : reportRunContents) {
            availableKeys.add(reportRunContent.getRunKey());
        }
        return availableKeys;
    }

    private File getTargetFile() {
        return new File(getProject().getInfos().getDirOfRapports(), RUN_FILE_NAME);
    }

    /**
     * @param key la clé du run
     * @return le contenu chargé. Si pas chargé, le chargement est effectué.
     */
    ReportRunContent getRunContentLoaded(ReportRunKey key) {
        ReportRunContent runContent = getRunContent(key);
        if (runContent == null) {
            return null;
        }
        if (!runContent.isLoaded()) {
            RunAlternatifLoaderProcessor processor = new RunAlternatifLoaderProcessor(runContent, getProject());
            CrueProgressUtils.showProgressDialogAndRun(processor,
                    NbBundle.getMessage(ReportRunAlternatifService.class, "RunAlternatif.LoadRun", key.getManagerNom(), key.getRunAlternatifId()));
        }
        return runContent;
    }

    /**
     * recharge tous les runs.
     */
    void reload() {
        clearAll();
        loadRuns();
    }

    private void clearAll() {
        Collection<? extends ReportRunContent> lookupAll = new ArrayList<>(lookup.lookupAll(ReportRunContent.class));
        for (ReportRunContent reportRunContent : lookupAll) {
            dynamicContent.remove(reportRunContent);
        }
        runAlternatifLoaded = false;
        modified = false;
    }

    /**
     * @return le projet chargé
     */
    public EMHProjet getProject() {
        return crueService.getProjectLoaded();
    }
}
