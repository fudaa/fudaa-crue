/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.formule;

import com.memoire.bu.BuBorders;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.editor.CtuluExprGUI;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.dodico.crue.common.CollectionCrueUtil;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.config.ccm.PropertyTypeNumerique;
import org.fudaa.dodico.crue.projet.report.ReportGlobalServiceContrat;
import org.fudaa.dodico.crue.projet.report.formule.FormuleCalculatorExpr;
import org.fudaa.dodico.crue.projet.report.formule.FormuleContentExprBuilder;
import org.fudaa.dodico.crue.projet.report.formule.FormuleDependenciesContent;
import org.fudaa.dodico.crue.projet.report.formule.FormuleParametersExpr;
import org.nfunk.jep.Variable;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.util.*;

/**
 * @author Frederic Deniger
 */
public class FormulePanelEditExpr extends FormulePanelEdit<FormuleNodeParametersExpr> {
  private final ReportGlobalServiceContrat reportGlobalServiceContrat = Lookup.getDefault().lookup(ReportGlobalServiceContrat.class);

  private List<PropertyNature> getEntierReelNatures() {
    List<PropertyNature> vars = new ArrayList<>();
    Collection<PropertyNature> values = reportService.getCcm().getNumeriqueNatures();
    for (PropertyNature itemVariable : values) {
      final PropertyTypeNumerique typeNumerique = itemVariable.getTypeNumerique();
      if (typeNumerique != null) {
        vars.add(itemVariable);
      }
    }
    Collections.sort(vars);
    return vars;
  }

  protected static class DesignExpr extends Design {
    final CtuluExprGUI exprUI;
    boolean exprValid = true;
    final JComboBox cbNature = new JComboBox();

    @Override
    protected boolean isParameterValid() {
      return exprValid;
    }

    DesignExpr(CtuluExpr expr) {
      exprUI = new CtuluExprGUI(expr);
      final JLabel jLabel = new JLabel(NbBundle.getMessage(FormulePanelEditExpr.class, "expr.nature"));
      pnName.add(jLabel);
      jLabel.setToolTipText(NbBundle.getMessage(FormulePanelEditExpr.class, "expr.natureTooltip"));
      pnName.add(cbNature);
      exprUI.setBorder(BuBorders.EMPTY5555);
      exprUI.setButtonValidVisible(false);
      pnMain.add(exprUI);
    }
  }

  FormulePanelEditExpr(FormuleNodeParametersExpr node) {
    super(node);
  }

  public FormulePanelEditExpr(Node parentNode) {
    super(parentNode);
  }

  static FormuleParametersExpr createExpr(DesignExpr design) {
    return updateExpr(design, new FormuleParametersExpr());
  }

  private static FormuleParametersExpr updateExpr(DesignExpr design, FormuleParametersExpr res) {
    res.setNature(((PropertyNature) design.cbNature.getSelectedItem()).getNom());
    res.setDisplayName(design.tfName.getText().trim());
    final CtuluExpr expr = design.exprUI.getExpr();
    res.setFormule(expr.getLastExpr().trim());
    return res;
  }

  @Override
  public void show() {
    Set<String> usedByThisVariable = null;
    FormuleDependenciesContent dependenciesContent;
    if (node != null) {
      FormuleParametersExpr lookup = node.getLookup().lookup(FormuleParametersExpr.class);
      dependenciesContent = FormuleContentChildFactory.getDependenciesContent(node, reportGlobalServiceContrat);
      usedByThisVariable = dependenciesContent.createUsedBy().usedBy(lookup.getId());
    }
    if (usedByThisVariable == null) {
      usedByThisVariable = Collections.emptySet();
    }
    FormuleContentExprBuilder exprBuilder = new FormuleContentExprBuilder(reportGlobalServiceContrat);
    Set<String> variableFCName = new HashSet<>(otherFormuleNames);
    variableFCName.removeAll(usedByThisVariable);

    CtuluExpr expr = exprBuilder.createExpr(variableFCName);

    if (node != null) {
      FormuleParametersExpr lookup = node.getLookup().lookup(FormuleParametersExpr.class);
      expr.getParser().parseExpression(lookup.getFormule());
    }

    final DesignExpr design = new DesignExpr(expr);
    if (CollectionUtils.isNotEmpty(usedByThisVariable)) {
      design.tfName.setEditable(false);
      design.tfName.setToolTipText(NbBundle.getMessage(FormulePanelEditExpr.class, "formuleNameNotEditable", StringUtils.join(CollectionCrueUtil.
          getSortedList(usedByThisVariable), ", ")));
    }
    final String title = NbBundle.getMessage(FormulePanelEditExpr.class, "formule.editExpression");
    final NotifyDescriptor descriptor = new NotifyDescriptor(design.pnMain, title, NotifyDescriptor.OK_CANCEL_OPTION,
        NotifyDescriptor.QUESTION_MESSAGE, null, null);
    descriptor.createNotificationLineSupport();
    super.initNamePart(design, descriptor);
    final FormuleValidDocumentListener formuleValidDocumentListener = new FormuleValidDocumentListener(design, descriptor);
    formuleValidDocumentListener.setUsedByThisVariable(usedByThisVariable);
    formuleValidDocumentListener.update();
    design.cbNature.setModel(new DefaultComboBoxModel(getEntierReelNatures().toArray()));
    design.cbNature.setRenderer(new CtuluCellTextRenderer() {
      @Override
      protected void setValue(Object _value) {
        super.setValue(((PropertyNature) _value).getDisplayName());
      }
    });
    if (node != null) {
      FormuleParametersExpr lookup = node.getLookup().lookup(FormuleParametersExpr.class);
      PropertyNature nature = null;
      if (lookup.getNature() != null) {
        nature = reportService.getCcm().getNature(lookup.getNature());
      }
      if (nature != null) {
        design.cbNature.setSelectedItem(nature);
      }
    }
    descriptor.setValid(design.isAllValid());
    design.exprUI.getTxtArea().getDocument().addDocumentListener(formuleValidDocumentListener);
    Object notify = DialogDisplayer.getDefault().notify(descriptor);
    if (NotifyDescriptor.OK_OPTION.equals(notify)) {
      if (isNewFormule()) {
        Children.Array children = (Children.Array) parentNode.getChildren();
        FormuleParametersExpr newExpr = createExpr(design);
        FormuleNodeParametersExpr newNode = new FormuleNodeParametersExpr(newExpr, true);
        children.add(new Node[]{newNode});
      } else if (node != null) {
        FormuleParametersExpr lookup = node.getLookup().lookup(FormuleParametersExpr.class);
        updateExpr(design, lookup);
        node.setDisplayName(lookup.getNom());
        node.fireChanged();
      }
    }
  }

  private static class FormuleValidDocumentListener implements DocumentListener {
    private final DesignExpr design;
    private final NotifyDescriptor descriptor;
    Set<String> usedByThisVariable = Collections.emptySet();

    FormuleValidDocumentListener(DesignExpr design, NotifyDescriptor descriptor) {
      this.design = design;
      this.descriptor = descriptor;
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
      update();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
      update();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
      update();
    }

    void update() {
      design.exprUI.valideExpr();
      design.exprValid = design.exprUI.isExprValide();
      if (design.exprValid) {
        Variable[] findUsedVar = design.exprUI.getExpr().getParser().findUsedVar();
        List<String> var = new ArrayList<>();
        for (Variable variable : findUsedVar) {
          if (usedByThisVariable.contains(variable.getName())) {
            var.add(variable.getName());
          }
        }
        String exprText = design.exprUI.getExprText();
        Set<String> findFunctionReferencedVariable = FormuleCalculatorExpr.findFunctionReferencedVariable(exprText);
        for (String v : findFunctionReferencedVariable) {
          if (usedByThisVariable.contains(v)) {
            var.add(v);
          }
        }
        if (!var.isEmpty()) {
          design.exprUI.setError(NbBundle.getMessage(FormulePanelEditExpr.class, "formuleCycleDetected.Error", StringUtils.join(var, ", ")));
          design.exprValid = false;
        }
      }
      descriptor.setValid(design.isAllValid());
    }

    private void setUsedByThisVariable(Set<String> usedBy) {
      this.usedByThisVariable = usedBy;
      if (this.usedByThisVariable == null) {
        this.usedByThisVariable = Collections.emptySet();
      }
    }
  }
}
