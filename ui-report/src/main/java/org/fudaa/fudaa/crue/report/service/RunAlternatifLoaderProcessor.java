/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.service;

import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.report.data.ReportRunContent;
import org.fudaa.fudaa.crue.loader.LoaderService;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class RunAlternatifLoaderProcessor implements ProgressRunnable<ReportRunContent> {

  private final ReportRunContent content;
  final EMHProjet project;

  public RunAlternatifLoaderProcessor(ReportRunContent content, EMHProjet project) {
    this.content = content;
    this.project = project;
  }

  public static Pair<ManagerEMHScenario, EMHRun> getManagers(EMHProjet project, String managerId, String runId) {
    ManagerEMHScenario manager = project.getScenario(managerId);
    EMHRun run = null;
    if (manager != null) {
      run = manager.getRunFromScenar(runId);
    }
    return new Pair<>(manager, run);
  }

  @Override
  public ReportRunContent run(ProgressHandle handle) {
    LoaderService loaderService = Lookup.getDefault().lookup(LoaderService.class);
    EMHScenario load = loaderService.load(project, content.getManagerScenario(), content.getRun(), true);
    content.setLoaded(true);
    if (load == null) {
      return null;
    }
    assert load.getIdRegistry() != null;
    content.setScenario(load);
    return content;
  }
}
