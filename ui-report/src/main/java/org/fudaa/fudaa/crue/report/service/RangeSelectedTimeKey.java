/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.service;

import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;

/**
 *
 * @author Frederic Deniger
 */
public class RangeSelectedTimeKey {

  public final List<ResultatTimeKey> keys;

  public RangeSelectedTimeKey(List<ResultatTimeKey> keys) {
    this.keys = keys == null ? Collections.emptyList() : Collections.unmodifiableList(keys);
  }

  public List<ResultatTimeKey> getKeys() {
    return keys;
  }
  
  
}
