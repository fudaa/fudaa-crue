package org.fudaa.fudaa.crue.report.service;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.report.data.ReportRunContent;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.planimetry.layout.NetworkBuilder;
import org.fudaa.fudaa.crue.planimetry.layout.NetworkGisPositionnerResult;
import org.fudaa.fudaa.crue.planimetry.save.VisuLoadingResult;
import org.fudaa.fudaa.crue.planimetry.services.AdditionalLayerSaveStateListener;
import org.fudaa.fudaa.crue.planimetry.services.AdditionalLayersSaveServices;
import org.fudaa.fudaa.crue.report.planimetry.ReportVisuPanelConfigurer;
import org.fudaa.fudaa.crue.report.planimetry.ReportVisuPanelLayersActionConfigurer;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.util.*;
import org.openide.util.Lookup.Result;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ServiceProvider;

import javax.swing.*;

/**
 * Service gérant la vue planimetrie du scenario dans la perspective rapport
 * <table  border="1"  cellpadding="3" cellspacing="0"  summary="lookups">
 * <tr>
 * <th align="left">Lookup</th>
 * <th align="left">Commentaire</th>
 * <th align="left">Méthode utilisant le lookup</th>
 * </tr>
 * <tr>
 * <td>{@link PlanimetryVisuPanel}</td>
 * <td>Le conteneur pour l'affichage de la planimetrie.</td>
 * <td><code>{@link #getPlanimetryVisuPanel()} </code></td>
 * </tr>
 * </table>
 *
 * @author deniger
 */
@ServiceProvider(service = ReportVisuPanelService.class)
public final class ReportVisuPanelService implements LookupListener, Lookup.Provider, AdditionalLayerSaveStateListener {
    private final Result<ReportRunContent> resultat;
    private final InstanceContent dynamicContent = new InstanceContent();
    private final Lookup lookup = new AbstractLookup(dynamicContent);
    private final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
    private final AdditionalLayersSaveServices additionalLayersSaveServices = Lookup.getDefault().lookup(AdditionalLayersSaveServices.class);

    public ReportVisuPanelService() {
        resultat = reportService.getLookup().lookupResult(
                ReportRunContent.class);
        resultat.addLookupListener(this);
        if (reportService.isRunLoaded()) {
            resultChanged(null);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void additionalLayerSavedBy(String source, PlanimetryController controller) {
        if (getPlanimetryVisuPanel() != null && !AdditionalLayerSaveStateListener.FROM_REPORT.equals(source)) {
            getPlanimetryVisuPanel().getPlanimetryController().reloadAdditionLayersFrom(controller);
        }
    }

    /**
     * @return le service princiap
     */
    public ReportService getReportService() {
        return reportService;
    }

    /**
     * Ecoute les de/chargement de run pour charger ou pas la données planimétriques
     *
     * @param ev non utilise
     */
    @Override
    public void resultChanged(LookupEvent ev) {
        if (reportService.isRunLoaded()) {
            loadRun();
        } else {
            unloadRun();
        }
    }

    @Override
    public Lookup getLookup() {
        return lookup;
    }

    /**
     * Charge la vue planimétrie du scenario dans un thread à part.
     *
     * @param scenarioLoaded le scenario chargé
     */
    private void load(final EMHScenario scenarioLoaded) {
        SwingWorker loader = new VisuPanelLoader(scenarioLoaded);
        loader.execute();
    }

    /**
     * @return la vue planimetrique issu du lookup {@link #getLookup()}
     */
    public PlanimetryVisuPanel getPlanimetryVisuPanel() {
        return lookup.lookup(PlanimetryVisuPanel.class);
    }

    /**
     * le run est déchargé: on supprime la vue planimetrque et on enleve les listeners
     */
    private void unloadRun() {
        PlanimetryVisuPanel old = getPlanimetryVisuPanel();
        if (old != null) {
            dynamicContent.remove(old);
            additionalLayersSaveServices.removeListener(this);
        }
    }

    /**
     * run chargé: on charge en arriere plan la vue planimetrique
     */
    private void loadRun() {
        unloadRun();
        load(reportService.getRunCourant().getScenario());
    }

    /**
     * Le laoder.
     */
    private class VisuPanelLoader extends SwingWorker<VisuLoadingResult, Object> {
        final ProgressHandle ph = ProgressHandleFactory.createHandle(NbBundle.getMessage(ReportVisuPanelService.class,
                "gisDataLoad.Task.Name"));
        private final EMHScenario scenarioLoaded;

        public VisuPanelLoader(EMHScenario scenarioLoaded) {
            this.scenarioLoaded = scenarioLoaded;
        }

        @Override
        protected VisuLoadingResult doInBackground() throws Exception {
            ph.start();
            ph.switchToIndeterminate();
            final EMHProjet selectedProjet = reportService.getSelectedProjet();
            return VisuLoadingResult.loadVisuConfig(selectedProjet, scenarioLoaded);
        }

        @Override
        protected void done() {
            try {
                VisuLoadingResult result = get();
                final NetworkGisPositionnerResult networkGisPositionnerResult = result.networkGisPositionnerResult;
                NetworkBuilder builder = new NetworkBuilder();
                PlanimetryVisuPanel panel = builder.loadCalqueFrom(CtuluUIForNetbeans.DEFAULT, scenarioLoaded, networkGisPositionnerResult,
                        networkGisPositionnerResult.getVisuConfiguration(),
                        networkGisPositionnerResult.getCcm(), false);
                panel.getPlanimetryController().apply(networkGisPositionnerResult.getLayerVisibility());
                final ReportVisuPanelConfigurer reportVisuPanelConfigurer = new ReportVisuPanelConfigurer();
                panel.setConfigurer(reportVisuPanelConfigurer);
                ReportVisuPanelLayersActionConfigurer actionsConfigurer = new ReportVisuPanelLayersActionConfigurer(panel.getPlanimetryController());

                panel.getPlanimetryController().getCqBranche().setActions(actionsConfigurer.getBranchesActions());
                panel.getPlanimetryController().getCqNoeud().setActions(actionsConfigurer.getNoeudActions());
                panel.getPlanimetryController().getCqCasier().setActions(actionsConfigurer.getCasiersActions());
                panel.getPlanimetryController().getCqSection().setActions(actionsConfigurer.getSectionsActions());
                panel.getPlanimetryController().getCqTrace().setActions(actionsConfigurer.getTracesActions());
                panel.addCalqueLegend();
                final BCalqueLegende cqLegend = panel.getCqLegend();
                panel.getPlanimetryController().getCqBranche().setLegende(cqLegend);
                panel.getPlanimetryController().getCqNoeud().setLegende(cqLegend);
                panel.getPlanimetryController().getCqCasier().setLegende(cqLegend);
                panel.getPlanimetryController().getCqSection().setLegende(cqLegend);
                if (CollectionUtils.isNotEmpty(result.additionnalLayer)) {
                    panel.getPlanimetryController().initWithAdditionnalLayers(result.additionnalLayer);
                }
                dynamicContent.add(panel);
                additionalLayersSaveServices.addListener(ReportVisuPanelService.this);
                if (result.log != null && result.log.containsErrorOrSevereError()) {
                    LogsDisplayer.displayError(result.log, NbBundle.getMessage(ReportVisuPanelService.class,
                            "gisDataLoad.Task.Name"));
                }
            } catch (Exception exception) {
                Exceptions.printStackTrace(exception);
            } finally {
                ph.finish();
            }
        }
    }
}
