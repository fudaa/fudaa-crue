/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.view.persist;

import java.io.File;
import java.util.List;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.projet.report.formule.FormuleParametersExpr;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleLoaderProcessor implements ProgressRunnable<CrueIOResu<List<FormuleParametersExpr>>> {

  private final File file;

  public FormuleLoaderProcessor(File file) {
    this.file = file;
  }

  @Override
  public CrueIOResu<List<FormuleParametersExpr>> run(ProgressHandle handle) {
    if (handle != null) {
      handle.switchToIndeterminate();
    }
    ReportFormuleReaderSaver configSaver = new ReportFormuleReaderSaver();
    return configSaver.read(file);
  }
}
