/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.rptg;

import com.memoire.bu.BuGridLayout;
import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.LoiFF;
import org.fudaa.dodico.crue.metier.emh.ResPrtGeo;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.dodico.crue.projet.report.ReportRPTGConfig;
import org.fudaa.ebli.courbe.EGAxeVerticalPersist;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.report.data.ReportVariableCellRenderer;
import org.fudaa.fudaa.crue.report.service.ReportFormuleService;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportRPTGChooseVariableUI implements ItemListener {

  final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
  ReportFormuleService reportFormuleService = Lookup.getDefault().lookup(ReportFormuleService.class);
  JComboBox cbVar1;
  JComboBox cbVar2;
  JComboBox cbHorizontal;
  JLabel lbError;
  TreeMap<String, List<String>> variableCorrespondance;
  final ReportRPTGTopComponent tc;

  public ReportRPTGChooseVariableUI(final ReportRPTGTopComponent tc) {
    this.tc = tc;
  }

  public boolean chooseVariable(final ReportRPTGConfig config) {
    final EMH emh = reportService.getRunCourant().getEMH(config.getEmh());
    if (emh == null) {
      return false;
    }
    final ResPrtGeo rptg = emh.getRPTG();
    if (rptg != null) {
      final List<LoiFF> lois = rptg.getLois();
      variableCorrespondance = LoiHelper.getVariableCorrespondance(lois, reportService.getCcm());
      final Set<String> keySet = variableCorrespondance.keySet();
      final String[] variables = keySet.toArray(new String[0]);
      final JPanel pn = new JPanel(new BuGridLayout(2, 5, 5));
      final ReportVariableCellRenderer cellRenderer = new ReportVariableCellRenderer();
      cbHorizontal = new JComboBox(variables);

      cbHorizontal.setRenderer(cellRenderer);
      cbVar1 = new JComboBox();
      cbVar1.setRenderer(cellRenderer);
      cbVar2 = new JComboBox();
      cbVar2.setRenderer(cellRenderer);
      pn.add(new JLabel(NbBundle.getMessage(ReportRPTGChooseVariableUI.class, "ChooseVariable.LabelHorizontal")));
      pn.add(cbHorizontal);
      pn.add(new JLabel());
      lbError = new JLabel(" ");
      lbError.setForeground(Color.RED);
      pn.add(lbError);
      pn.add(new JLabel(NbBundle.getMessage(ReportRPTGChooseVariableUI.class, "ChooseVariable.LabelFirst")));
      pn.add(cbVar1);
      pn.add(new JLabel(NbBundle.getMessage(ReportRPTGChooseVariableUI.class, "ChooseVariable.LabelSecond")));
      pn.add(cbVar2);
      cbHorizontal.addItemListener(this);
      cbHorizontal.setSelectedItem(config.getVarHorizontal());
      updateLabel();
      pn.add(SysdocUrlBuilder.createButtonHelpDialog(PerspectiveEnum.REPORT, "vueRPTG_ChoixVariable"));
      cbVar1.setSelectedItem(config.getVar1());
      cbVar2.setSelectedItem(config.getVar2());
      final boolean accepted = DialogHelper.showQuestionOkCancel(NbBundle.getMessage(ReportRPTGChooseVariableUI.class, "ChooseVariable.ButtonName"), pn);
      if (accepted && cbHorizontal.getSelectedItem() != null) {
        config.setVarHorizontal((String) cbHorizontal.getSelectedItem());
        config.getVariables().clear();
        String v1 = (String) cbVar1.getSelectedItem();
        final String v2 = (String) cbVar2.getSelectedItem();
        if (v1 == null && v2 != null) {
          v1 = v2;
        }
        if (v1 != null) {
          config.getVariables().add(v1);
        }
        if (v2 != null) {
          config.getVariables().add(v2);
        }
        if (v1 != null) {
          final EGAxeVerticalPersist axeV1 = tc.createAxeVerticalConfig(v1);
          axeV1.setDroite(false);
        }
        if (v2 != null) {
          final EGAxeVerticalPersist axeV2 = tc.createAxeVerticalConfig(v2);
          axeV2.setDroite(true);
        }
      }
      return true;
    } else {
      DialogHelper.showError(NbBundle.getMessage(ReportRPTGChooseVariableUI.class, "ChooseVariable.NoRPTG"));
      return false;
    }
  }

  protected void updateLabel() {
    final DefaultComboBoxModel model1 = (DefaultComboBoxModel) cbVar1.getModel();
    final DefaultComboBoxModel model2 = (DefaultComboBoxModel) cbVar2.getModel();
    if (cbHorizontal.getSelectedItem() == null) {
      lbError.setText(NbBundle.getMessage(ReportRPTGChooseVariableUI.class, "ChooseVariable.NoHorizontalVariable"));
      model1.removeAllElements();
      model2.removeAllElements();
    } else {
      final String h = (String) cbHorizontal.getSelectedItem();
      final List<String> availableVar = this.variableCorrespondance.get(h);
      final String oldV1 = (String) model1.getSelectedItem();
      final String oldV2 = (String) model2.getSelectedItem();
      model1.removeAllElements();
      model2.removeAllElements();
      model1.addElement(null);
      model2.addElement(null);
      for (final String var : availableVar) {
        model1.addElement(var);
        model2.addElement(var);
      }
      cbVar1.setSelectedItem(oldV1);
      cbVar2.setSelectedItem(oldV2);
    }
  }

  @Override
  public void itemStateChanged(final ItemEvent e) {
    updateLabel();
  }
}
