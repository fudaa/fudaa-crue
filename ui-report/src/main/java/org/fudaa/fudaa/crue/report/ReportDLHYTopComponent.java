package org.fudaa.fudaa.crue.report;

import com.memoire.bu.BuGridLayout;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.ExportTableCommentSupplier;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.comparator.ObjetNommeByNameComparator;
import org.fudaa.dodico.crue.metier.emh.DonLoiHYConteneur;
import org.fudaa.dodico.crue.metier.emh.Loi;
import org.fudaa.dodico.crue.metier.emh.LoiDF;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.editor.LocalDateTimePanel;
import org.fudaa.fudaa.crue.common.helper.ObjetNommeCellRenderer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.loi.loiff.LoiUiController;
import org.fudaa.fudaa.crue.study.services.TableExportCommentSupplier;
import org.joda.time.LocalDateTime;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.*;
import java.util.List;

/**
 * visualisateur de lois.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.report//ReportDLHYTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = ReportDLHYTopComponent.TOPCOMPONENT_ID, persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = ReportDLHYTopComponent.MODE, openAtStartup = false)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.report.ReportDLHYTopComponent")
public final class ReportDLHYTopComponent extends AbstractReportTopComponent implements ItemListener, ExportTableCommentSupplier {

    public static final String MODE = "loi-dlhyReport"; //NOI18N
    public static final String TOPCOMPONENT_ID = "ReportDLHYTopComponent"; //NOI18N
    public static final String PROPERTY_INIT_LOI = "initLoi";
    final JComboBox cbLois;
    Loi currentLoi;
    private final LoiUiController loiUiController;
    private final JTextField txtCommentaire;
    private final JTextField txtNom;
    private final JLabel lbNomValidation;
    private final String initName;
    private final DateDebEditor dateDeb;

    public ReportDLHYTopComponent() {
        initComponents();
        initName = NbBundle.getMessage(ReportDLHYTopComponent.class, "CTL_" + TOPCOMPONENT_ID);
        setName(initName);
        setToolTipText(NbBundle.getMessage(ReportDLHYTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
        loiUiController = new LoiUiController();
        loiUiController.addExportImagesToToolbar();
        loiUiController.setUseVariableForAxeH(true);
        add(loiUiController.getToolbar(), BorderLayout.NORTH);
        loiUiController.getPanel().setPreferredSize(new Dimension(550, 350));
        final JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, loiUiController.getPanel(), loiUiController.getTableGraphePanel());

        splitPane.setDividerLocation(550);
        splitPane.setOneTouchExpandable(true);
        splitPane.setContinuousLayout(true);
        add(splitPane);

        final JPanel nomComment = new JPanel(new BuGridLayout(3, 10, 10));


        txtNom = new JTextField(30);
        lbNomValidation = new JLabel();

        //ligne 1
        nomComment.add(new JLabel(NbBundle.getMessage(ReportDLHYTopComponent.class, "Nom.DisplayName")));
        nomComment.add(txtNom);
        nomComment.add(lbNomValidation);

        txtCommentaire = new JTextField(30);
        txtCommentaire.setEditable(false);
        //ligne 2
        nomComment.add(new JLabel(NbBundle.getMessage(ReportDLHYTopComponent.class, "Commentaire.DisplayName")));
        nomComment.add(txtCommentaire);
        nomComment.add(new JLabel());

        dateDeb = new DateDebEditor();
        //ligne 3
        nomComment.add(new JLabel(NbBundle.getMessage(ReportDLHYTopComponent.class, "DateDebut.DisplayName")));
        nomComment.add(dateDeb.getPn());
        final JPanel pnInfo = new JPanel(new BorderLayout(5, 5));
        pnInfo.add(loiUiController.getInfoController().getPanel());
        pnInfo.add(nomComment, BorderLayout.NORTH);
        pnInfo.add(createCancelSaveButtons(), BorderLayout.SOUTH);
        cbLois = new JComboBox();
        cbLois.addItemListener(this);
        cbLois.setRenderer(new ObjetNommeCellRenderer());
        final JPanel pnActions = new JPanel(new BuGridLayout(1, 5, 5));
        pnActions.add(cbLois);
        final JPanel pnSouth = new JPanel(new BorderLayout(10, 10));
        pnSouth.add(pnActions, BorderLayout.WEST);
        pnSouth.setBorder(BorderFactory.createEtchedBorder());
        pnSouth.add(pnInfo);
        add(pnSouth, BorderLayout.SOUTH);
        setEditable(false);
    }

    @Override
    protected String getHelpCtxId() {
        return SysdocUrlBuilder.getTopComponentHelpCtxId("vueDLHY", PerspectiveEnum.REPORT);
    }

    @Override
    public List<String> getComments() {
        return new TableExportCommentSupplier(false).getComments(getName());
    }

    @Override
    protected void componentOpenedHandler() {
        super.componentOpenedHandler();
        loiUiController.getGraphe().setExportTableCommentSupplier(this);
        final Loi init = (Loi) getClientProperty(PROPERTY_INIT_LOI);
        if (init != null) {
            putClientProperty(PROPERTY_INIT_LOI, null);
            cbLois.setSelectedItem(init);
        }
    }

    @Override
    public void itemStateChanged(final ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            currentLoi = (Loi) cbLois.getSelectedItem();
            updateWithLoi();
        }
    }

    @Override
    protected void runLoaded() {
        final Loi oldLoi = getCurrentLoi();
        currentLoi = null;
        final DonLoiHYConteneur dlhy = reportService.getRunCourant().getScenario().getLoiConteneur();
        final List<Loi> lois = new ArrayList<>(dlhy.getLois());
        Collections.sort(lois, ObjetNommeByNameComparator.INSTANCE);
        final int idx = lois.indexOf(oldLoi);
        cbLois.setModel(new DefaultComboBoxModel(lois.toArray(new Loi[0])));
        cbLois.setSelectedItem(null);
        if (idx >= 0) {
            cbLois.setSelectedIndex(idx);
        }

    }

    protected void updateWithLoi() {
        updating = true;
        final Loi loi = getCurrentLoi();
        txtCommentaire.setText(StringUtils.EMPTY);
        txtNom.setText(StringUtils.EMPTY);
        if (loi != null) {
            txtCommentaire.setText(loi.getCommentaire());
            final String desc = StringUtils.defaultString(loi.getNom());
            txtNom.setText(desc);
            setName(initName + BusinessMessages.ENTITY_SEPARATOR + desc);
        } else {
        }
        dateDeb.setLoi(loi);
        loiUiController.setLoi(loi, getCcm(), false);
        loiUiController.getLoiModel().addObserver(new Observer() {
            @Override
            public void update(final Observable o, final Object arg) {
                setModified(true);
            }
        });
        loiUiController.setEditable(false);
        updating = false;
        setModified(false);
    }

    @Override
    public void runUnloaded() {
        loiUiController.setLoi(null, null, false);
        currentLoi = null;
        cbLois.setModel(new DefaultComboBoxModel());
    }

    Loi getCurrentLoi() {
        return currentLoi;

    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    void writeProperties(final java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
    }

    void readProperties(final java.util.Properties p) {
    }

    public void selectLoi(final Loi loiToActivate) {
        if (isModified()) {
            return;
        }
        cbLois.setSelectedItem(loiToActivate);
    }

    protected class DateDebEditor {

        final LocalDateTimePanel pnDateDeb = new LocalDateTimePanel();
        final JCheckBox cbDateDeb = new JCheckBox();
        final JPanel pn = new JPanel(new FlowLayout(FlowLayout.LEFT));

        public DateDebEditor() {
            pn.add(cbDateDeb);
            pn.add(pnDateDeb);
            cbDateDeb.setEnabled(false);
        }

        void updateState() {
            pnDateDeb.setAllEnable(cbDateDeb.isSelected() && cbDateDeb.isEnabled());
        }

        public JPanel getPn() {
            return pn;
        }

        LocalDateTime getTime() {
            return cbDateDeb.isSelected() ? pnDateDeb.getLocalDateTime() : null;
        }

        public void setLoi(final Loi loi) {
            if (loi == null || !LoiDF.class.equals(loi.getClass())) {
                cbDateDeb.setSelected(false);
                pnDateDeb.setLocalDateTime(null);
                cbDateDeb.setEnabled(false);
                pnDateDeb.setAllEnable(false);
            } else {
                final LoiDF loiDF = (LoiDF) loi;
                cbDateDeb.setSelected(loiDF.getDateZeroLoiDF() != null);
                pnDateDeb.setLocalDateTime(loiDF.getDateZeroLoiDF());
            }
        }
    }
}
