/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.planimetry;

import java.awt.Color;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.data.ReportRunVariableKey;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.dodico.crue.projet.report.planimetry.AbstractReportPlanimetryConfigData;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;

/**
 *
 * @author Frederic Deniger
 */
public class AbstractConfigurationExtra<T extends AbstractReportPlanimetryConfigData> {

  protected T data;
  protected final ReportResultProviderService reportResultProviderService;

  public AbstractConfigurationExtra(final ReportResultProviderService reportResultProviderService) {
    this.reportResultProviderService = reportResultProviderService;
  }

  public T getData() {
    return data;
  }

  public void setData(final T data) {
    this.data = data;
  }

  protected String getDisplayName(final EMH emh, final boolean isSelected) {
    return getDisplayName(emh.getNom(), emh, isSelected);
  }

  public Color getColor(final EMH emh) {
    final ReportVariableKey variableKey = data.getPaletteVariable();
    if (data == null || !data.isUsePalette() || data.getPalette() == null || variableKey == null || emh == null) {
      return null;
    }
    final Double value = getValue(variableKey, emh);
    if (value == null) {
      return null;
    }
    return data.getPalette().getColorFor(value);
  }

  protected String getDisplayName(final String nomToUse, final EMH emh, final boolean isSelected) {
    if (data == null || !data.isUseLabels()) {
      return null;
    }
    final List<ReportLabelContent> labelsToDisplay = data.getLabelsToDisplay(isSelected);
    if (CollectionUtils.isEmpty(labelsToDisplay)) {
      return null;
    }
    final StringBuilder builder = new StringBuilder();
    builder.append(nomToUse);
    for (final ReportLabelContent reportLabelContent : labelsToDisplay) {
      final DecimalFormatEpsilonEnum precision = isSelected ? DecimalFormatEpsilonEnum.COMPARISON : DecimalFormatEpsilonEnum.PRESENTATION;
      final String labelAsString = reportResultProviderService.getLabelString(reportLabelContent, reportResultProviderService.getReportService().
              getCurrentRunKey(), emh.getNom(), precision, ReportResultProviderServiceContrat.NO_SELECTED_TIMES_REPORT);
      if (StringUtils.isNotBlank(labelAsString)) {
        builder.append('\n').append(labelAsString);
      }
    }
    return builder.toString();
  }

  protected Double getValue(final ReportVariableKey variableKey, final EMH emh) {
    final ReportRunKey currentRunKey = reportResultProviderService.getReportService().getCurrentRunKey();
    return reportResultProviderService.getValue(new ReportRunVariableKey(currentRunKey, variableKey), emh.getNom(),
            ReportResultProviderServiceContrat.NO_SELECTED_TIMES_REPORT);
  }
}
