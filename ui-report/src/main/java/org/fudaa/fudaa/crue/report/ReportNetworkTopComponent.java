package org.fudaa.fudaa.crue.report;

import java.awt.BorderLayout;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.swing.ActionMap;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.emh.node.HierarchyChildFactory;
import org.fudaa.fudaa.crue.emh.node.LinkedEMHHelper;
import org.fudaa.fudaa.crue.emh.node.NodeEMHDefault;
import org.fudaa.fudaa.crue.report.node.ReportNodeEMHFactory;
import org.fudaa.fudaa.crue.report.service.ReportSelectedEMHService;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.report//ReportNetworkTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = ReportNetworkTopComponent.TOPCOMPONENT_ID,
        iconBase = "org/fudaa/fudaa/crue/report/rond-orange_16.png",
        persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "report-bottomRight", openAtStartup = false, position = 3)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.report.ReportNetworkTopComponent")
@ActionReference(path = "Menu/Window/Report", position = 5)
@TopComponent.OpenActionRegistration(displayName = ReportNetworkTopComponent.TOPCOMPONENT_ACTION_DISPLAYNAME,
        preferredID = ReportNetworkTopComponent.TOPCOMPONENT_ID)
public final class ReportNetworkTopComponent extends AbstractReportTopComponent implements LookupListener, ExplorerManager.Provider {

  final ReportSelectedEMHService reportSelectedEMHService = Lookup.getDefault().lookup(ReportSelectedEMHService.class);
  public static final String TOPCOMPONENT_ID = "ReportNetworkTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  private Lookup.Result<Long> resultatEMHsSelected;
  private Lookup.Result<NodeEMHDefault> resultatNodesSelected;
  private final ExplorerManager em = new ExplorerManager();
  private final org.openide.explorer.view.OutlineView outlineView;
  private SelectedEMHsLookupListener selectedEMHsLookupListener;
  private SelectedNodeLookupListener selectedNodeLookupListener;

  public ReportNetworkTopComponent() {
    initComponents();
    outlineView = new OutlineView(NbBundle.getMessage(ReportNetworkTopComponent.class,
            "ReportNetworkTopComponent.FirstColumn.Name"));
    outlineView.getOutline().setFullyNonEditable(true);
    outlineView.getOutline().setFillsViewportHeight(true);
    outlineView.getOutline().setColumnHidingAllowed(false);
    outlineView.addPropertyColumn(NodeEMHDefault.PROP_VALUE,
            NbBundle.getMessage(ReportNetworkTopComponent.class,
            "ReportNetworkTopComponent." + NodeEMHDefault.PROP_VALUE + ".Name"));
    add(outlineView);
//    add(createCancelSaveButtons(), BorderLayout.SOUTH);
    setName(NbBundle.getMessage(ReportNetworkTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ReportNetworkTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
    ActionMap map = this.getActionMap();
    associateLookup(ExplorerUtils.createLookup(em, map));
    outlineView.setDefaultActionAllowed(true);
    LinkedEMHHelper.installShortcutsAndCellRenderer(outlineView, em);
    add(LinkedEMHHelper.installBackNextButtons(outlineView), BorderLayout.NORTH);
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueReseau", PerspectiveEnum.REPORT);
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return em;
  }

  @Override
  protected void componentActivated() {
    super.componentActivated();
    ExplorerUtils.activateActions(em, true);
  }

  @Override
  protected void componentDeactivated() {
    super.componentDeactivated();
    ExplorerUtils.activateActions(em, false);
  }

  @Override
  protected void setEditable(boolean b) {
  }

  @Override
  protected void runLoaded() {
    final EMHScenario currentScenario = getCurrentScenario();
    em.setRootContext(HierarchyChildFactory.createScenarioNode(currentScenario, new ReportNodeEMHFactory()));
    if (resultatEMHsSelected == null) {
      selectedEMHsLookupListener = new SelectedEMHsLookupListener();
      resultatEMHsSelected = reportSelectedEMHService.getLookup().lookupResult(Long.class);
      resultatEMHsSelected.addLookupListener(selectedEMHsLookupListener);
    }
    if (resultatNodesSelected == null) {
      selectedNodeLookupListener = new SelectedNodeLookupListener();
      resultatNodesSelected = getLookup().lookupResult(NodeEMHDefault.class);
      resultatNodesSelected.addLookupListener(selectedNodeLookupListener);
    }
  }

  @Override
  public void runUnloaded() {
    em.setRootContext(Node.EMPTY);
    if (resultatEMHsSelected != null) {
      resultatEMHsSelected.removeLookupListener(selectedEMHsLookupListener);
      resultatEMHsSelected = null;
    }
    if (resultatNodesSelected != null) {
      resultatNodesSelected.removeLookupListener(selectedNodeLookupListener);
      resultatNodesSelected = null;
    }
  }

  private class SelectedEMHsLookupListener implements LookupListener {

    private boolean updating;

    @Override
    public void resultChanged(LookupEvent ev) {
      if (reportSelectedEMHService.isUpdating() || updating) {
        return;
      }
      updating = true;
      Window activeWindow = KeyboardFocusManager.getCurrentKeyboardFocusManager().
              getActiveWindow();
      //pas de modification si vue active et si pas de recherche depuis la fenetre de recherche
      if (activeWindow != WindowManager.getDefault().getMainWindow() || WindowManager.getDefault().getRegistry().getActivated() != ReportNetworkTopComponent.this) {
        Collection<? extends Long> allItems = resultatEMHsSelected.allInstances();
        LinkedEMHHelper.selectEMHs(getExplorerManager(), new HashSet<>(allItems));
      }
      updating = false;
    }
  }

  /**
   * met à jour la selection global si cette selection change
   */
  private class SelectedNodeLookupListener implements LookupListener {

    @Override
    public void resultChanged(LookupEvent ev) {
      if (WindowManager.getDefault().getRegistry().getActivated() == ReportNetworkTopComponent.this) {
        Collection<? extends NodeEMHDefault> allItems = resultatNodesSelected.allInstances();
        Set<Long> selectedUid = new HashSet<>();
        for (NodeEMHDefault node : allItems) {
          EMH emh = node.getLookup().lookup(EMH.class);
          if (emh != null) {
            selectedUid.add(emh.getUiId());
          }
        }
        reportSelectedEMHService.setSelectedUids(selectedUid);
      }
    }
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  @Override
  protected void componentOpenedHandler() {
  }

  @Override
  protected void componentClosedDefinitlyHandler() {
  }

  void writeProperties(java.util.Properties p) {
    DialogHelper.writeProperties(outlineView, "outlineView", p);
  }

  void readProperties(java.util.Properties p) {
    DialogHelper.readProperties(outlineView, "outlineView", p);
  }
}
