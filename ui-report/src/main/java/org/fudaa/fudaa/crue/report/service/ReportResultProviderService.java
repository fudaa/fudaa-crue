/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.service;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.crue.aoc.exec.AocExecVariableHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.helper.DonPrtHelper;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.metier.helper.ResPrtHelper;
import org.fudaa.dodico.crue.metier.transformer.TransformerEMHHelper;
import org.fudaa.dodico.crue.projet.report.FormuleContentManager;
import org.fudaa.dodico.crue.projet.report.ReportResultProviderServiceContrat;
import org.fudaa.dodico.crue.projet.report.data.*;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.fudaa.crue.aoc.service.LhptService;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * Implementation du service permettant de lire les résultat
 *
 * @author Frederic Deniger
 */
@ServiceProviders(value = {
    @ServiceProvider(service = ReportResultProviderService.class),
    @ServiceProvider(service = FormuleContentManager.class),
    @ServiceProvider(service = ReportResultProviderServiceContrat.class)})
public class ReportResultProviderService implements ReportResultProviderServiceContrat, FormuleContentManager {
  private final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
  private final ReportReadResultProviderService reportReadResultProviderService = Lookup.getDefault().lookup(ReportReadResultProviderService.class);
  private final ReportFormuleService reportFormuleService = Lookup.getDefault().lookup(ReportFormuleService.class);
  private final LhptService lhptService = Lookup.getDefault().lookup(LhptService.class);

  /**
   * {@inheritDoc}
   */
  @Override
  public Double getValue(ReportRunVariableKey key, String emhNom, Collection<ResultatTimeKey> selectedTimesInRapport) {
    ResultatTimeKey selectedTime = reportService.getSelectedTime();
    return getValue(selectedTime, key, emhNom, selectedTimesInRapport);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CrueConfigMetier getCcm() {
    return reportService.getCcm();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CtuluUI getUI() {
    return CtuluUIForNetbeans.DEFAULT;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ReportRunContent getRunCourant() {
    return reportService.getRunCourant();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Set<String> getDirectUsedVariable(String var) {
    if (reportFormuleService.isFormule(var)) {
      return reportFormuleService.getDirectUsedVariable(var);
    }
    return Collections.emptySet();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean containsTimeDependantVariable(String var) {
    return reportFormuleService.isFormule(var) && reportFormuleService.containsTimeDependantVariable(var);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isExpressionValid(String var, Collection<ResultatTimeKey> selectedTimes) {
    return !reportFormuleService.isFormule(var) || reportFormuleService.isExpressionValid(var, selectedTimes);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Double getValue(ResultatTimeKey selectedTime, ReportRunVariableKey key, String emhNom, Collection<ResultatTimeKey> selectedTimesInRapport) {
    ReportVariableTypeEnum variableType = key.getVariable().getVariableType();
    switch (variableType) {
      case TIME:
        return getTimeValue(key);
      case FORMULE:
        return reportFormuleService.getValue(selectedTime, key, emhNom, selectedTimesInRapport);
      case READ:
        return getReadValue(selectedTime, key, emhNom);
      case RESULTAT_RPTI:
        return getRPTIValue(key, emhNom);
      case LHPT:
        return getLHPTValue(key, emhNom);
    }
    throw new IllegalAccessError("unknown " + variableType);
  }

  /**
   * @return le service pour les runs alternatifs
   */
  public ReportRunAlternatifService getReportRunAlternatifService() {
    return reportReadResultProviderService.getReportRunAlternatifService();
  }

  /**
   * @return le service principal de la perspective rapport
   */
  public ReportService getReportService() {
    return reportService;
  }

  /**
   * @param key la clé du run
   * @return le contenu du run complet cherché dans le run courant puis les runs alternatifs
   */
  public ReportRunContent getRunContent(ReportRunKey key) {
    if (key.isCourant()) {
      return reportService.getRunCourant();
    }
    return getReportRunAlternatifService().getRunContentLoaded(key);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public EMH getEMH(String nom, ReportRunKey key) {
    return reportReadResultProviderService.getEMH(nom, key);
  }

  /**
   * @param labelConfig la configuration
   * @param runKey le run
   * @param emh le nom de l'EMH support
   * @param decimalFormat la précision demandée dans l'affichage
   * @param selectedTimesInRapport les pas de temps sélectionnés
   * @return la représentation complète en accord avec la configuration de {@link ReportLabelContent}
   */
  public String getLabelString(ReportLabelContent labelConfig, ReportRunKey runKey, String emh, DecimalFormatEpsilonEnum decimalFormat,
                               Collection<ResultatTimeKey> selectedTimesInRapport) {
    if (labelConfig == null) {
      return null;
    }
    if (labelConfig.getContent() == null) {
      return labelConfig.getPrefix();
    } else {
      ReportRunVariableKey key = new ReportRunVariableKey(runKey, labelConfig.getContent());
      String unit = StringUtils.EMPTY;
      if (labelConfig.isUnit()) {
        unit = getUnit(labelConfig.getContent());
        if (unit != null) {
          unit = " " + unit;
        }
      }
      return StringUtils.defaultString(labelConfig.getPrefix()) + getValueAsString(key, emh, decimalFormat, selectedTimesInRapport) + unit;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ReportVariableKey createVariableKey(String varName) {
    if (ResPrtHelper.isRPTIVariable(varName)) {
      return new ReportVariableKey(ReportVariableTypeEnum.RESULTAT_RPTI, varName);
    }
    if (varName.startsWith(EnumTypeLoi.LoiSectionsZ.getNom())) {
      return new ReportVariableKey(ReportVariableTypeEnum.LHPT, varName);
    }
    if (reportFormuleService.isFormule(varName)) {
      return new ReportVariableKey(ReportVariableTypeEnum.FORMULE, varName);
    }
    return new ReportVariableKey(ReportVariableTypeEnum.READ, StringUtils.uncapitalize(varName));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PropertyNature getPropertyNature(String varName) {
    if (reportFormuleService.isFormule(varName)) {
      return reportFormuleService.getPropertyNature(varName, reportService.getCcm());
    }
    ItemVariable property = reportService.getCcm().getProperty(varName);
    if (property == null) {
      return PropertyNature.DEFAULT;
    }
    return property.getNature();
  }

  /**
   * @param varName le nom de la variable à formater
   * @return si formule renvoie varName sinon utlise le {@link CrueConfigMetier} pour récupérer le nom à afficher
   */
  public String getVariableName(String varName) {
    if (reportFormuleService.isFormule(varName)) {
      return varName;
    }
    ItemVariable property = reportService.getCcm().getProperty(varName);
    return property == null ? varName : property.getDisplayNom();
  }

  /**
   * @param key la clé de la variable
   * @return la {@link PropertyNature} correspondante
   */
  public PropertyNature getPropertyNature(ReportVariableKey key) {
    if (ReportVariableTypeEnum.FORMULE.equals(key.getVariableType())) {
      return reportFormuleService.getPropertyNature(key, getReportService().getCcm());
    } else if (ReportVariableTypeEnum.READ.equals(key.getVariableType())) {
      final ItemVariable property = reportService.getCcm().getProperty(key.getVariableName());
      return property == null ? null : property.getNature();
    }
    return null;
  }

  /**
   * @param key la clé run/variable
   * @param emhNom le nom de l'EMH support
   * @param decimalFormat le type de précision attendue dans l'affichage
   * @param selectedTimesInRapport les pas de temps
   * @return la valeur formatée
   */
  private String getValueAsString(ReportRunVariableKey key, String emhNom, DecimalFormatEpsilonEnum decimalFormat,
                                  Collection<ResultatTimeKey> selectedTimesInRapport) {
    if (key == null) {
      return " ? ";
    }
    ReportVariableTypeEnum variableType = key.getVariable().getVariableType();
    switch (variableType) {
      case TIME:
        return getTimeValueAsString(key);
      case EXPR:
        return getExpressionValueAsString(key, emhNom, decimalFormat, selectedTimesInRapport);
      case FORMULE:
        return getFormuleValueAsString(key, emhNom, decimalFormat, selectedTimesInRapport);
      case READ:
        return getReadValueAsString(key, emhNom, decimalFormat);
      case RESULTAT_RPTI:
        return getRPTIValueAsString(key, emhNom, decimalFormat);
      case LHPT:
        return getLHPTValueAsString(key, emhNom, decimalFormat);
    }
    throw new IllegalAccessError("unknown " + variableType);
  }

  private Double getTimeValue(ReportRunVariableKey key) {
    final String variableName = key.getVariable().getVariableName();
    if (ReportExpressionHelper.TIME_NOM_CALC.equals(variableName)) {
      throw new IllegalAccessError("nom calc cannot be translate to double");
    } else if (ReportExpressionHelper.TIME_TEMPS_SIMU.equals(variableName)) {
      ResultatTimeKey selectedTime = reportService.getSelectedTime();
      if (selectedTime == null) {
        return null;
      }
      return reportService.getTimeContent().getTempsCalcInSec(selectedTime);
    } else if (ReportExpressionHelper.TIME_TEMPS_SCE.equals(variableName)) {
      ResultatTimeKey selectedTime = reportService.getSelectedTime();
      if (selectedTime == null) {
        return null;
      }
      return reportService.getTimeContent().getTempSceInSec(selectedTime);
    }
    throw new UnsupportedOperationException("Not yet implemented");
  }

  private Double getReadValue(ReportRunVariableKey key, String emhNom) {
    return reportReadResultProviderService.getReadValue(key, emhNom);
  }

  private String getTimeValueAsString(ReportRunVariableKey key) {
    final String variableName = key.getVariable().getVariableName();
    if (ReportExpressionHelper.TIME_NOM_CALC.equals(variableName)) {
      ResultatTimeKey selectedTime = reportService.getSelectedTime();
      return selectedTime == null ? StringUtils.EMPTY : selectedTime.getNomCalcul();
    } else if (ReportExpressionHelper.TIME_TEMPS_SIMU.equals(variableName)) {
      return reportService.getReportTimeFormatter().getTempsCalcAsString();
    } else if (ReportExpressionHelper.TIME_TEMPS_SCE.equals(variableName)) {
      return reportService.getReportTimeFormatter().getTempsSceAsString();
    }
    throw new UnsupportedOperationException("Not yet implemented");
  }

  private String getReadValueAsString(ReportRunVariableKey key, String emhNom, DecimalFormatEpsilonEnum type) {
    Double val = getReadValue(key, emhNom);
    if (val == null) {
      return StringUtils.EMPTY;
    }
    return TransformerEMHHelper.formatFromPropertyName(key.getVariable().getVariableName(), val, reportService.getCcm(), type);
  }

  private String getFormuleValueAsString(ReportRunVariableKey key, String emhNom, DecimalFormatEpsilonEnum type,
                                         Collection<ResultatTimeKey> selectedTimesInRapport) {
    Double val = reportFormuleService.getValue(reportService.getSelectedTime(), key, emhNom, selectedTimesInRapport);
    if (val == null) {
      return StringUtils.EMPTY;
    }
    return TransformerEMHHelper.formatFromPropertyName(reportFormuleService.getPropertyNature(key.getVariable()), val, reportService.getCcm(), type);
  }

  private String getUnit(ReportVariableKey content) {
    PropertyNature itemVariable = getPropertyNature(content);
    return itemVariable == null ? null : itemVariable.getUnite();
  }

  private String getExpressionValueAsString(ReportRunVariableKey key, String emhNom, DecimalFormatEpsilonEnum type,
                                            Collection<ResultatTimeKey> selectedTimesInRapport) {
    String variableName = key.getVariable().getVariableName();
    if (ReportExpressionHelper.EXPR_NOM.equals(variableName)) {
      return emhNom;
    } else if (ReportExpressionHelper.EXPR_COMMENTAIRE.equals(variableName)) {
      ReportRunContent runContent = getRunContent(key.getRunKey());
      if (runContent != null) {
        EMH emh = runContent.getEMH(emhNom);
        if (emh != null) {
          return emh.getCommentaire();
        }
      }
      return null;
    } else if (ReportExpressionHelper.EXPR_PROFIL_NOM.equals(variableName)) {
      ReportRunContent runContent = getRunContent(key.getRunKey());
      if (runContent != null) {
        EMH emh = runContent.getEMH(emhNom);
        if (emh != null) {
          DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection(emh);
          if (profilSection == null) {
            return null;
          }
          return profilSection.getNom();
        }
      }
      return null;
    } else if (ReportExpressionHelper.EXPR_PROFIL_COMMENTAIRE.equals(variableName)) {
      ReportRunContent runContent = getRunContent(key.getRunKey());
      if (runContent != null) {
        EMH emh = runContent.getEMH(emhNom);
        if (emh != null) {
          DonPrtGeoProfilSection profilSection = DonPrtHelper.getProfilSection(emh);
          if (profilSection == null) {
            return null;
          }
          return profilSection.getCommentaire();
        }
      }
      return null;
    }
    Double val = reportFormuleService.getValue(reportService.getSelectedTime(), key, emhNom, selectedTimesInRapport);
    if (val == null) {
      return StringUtils.EMPTY;
    }
    String nature = reportFormuleService.getPropertyNature(key.getVariable());
    if (nature != null) {
      return TransformerEMHHelper.formatFromNatureName(nature, val, reportService.getCcm(), type);
    }

    return val.toString();
  }

  private Double getReadValue(ResultatTimeKey selectedTime, ReportRunVariableKey key, String emhNom) {
    return reportReadResultProviderService.getReadValue(selectedTime, key, emhNom);
  }

  /**
   * @param key la clé demande
   * @param emhNom le nom de l'EMH support
   * @return la valeur RPTI de la variable "key" pour l'EMH emhNom
   */
  private Double getRPTIValue(ReportRunVariableKey key, String emhNom) {
    // lecture de l'EMH associé au run
    EMH emh = getEMH(emhNom, key.getRunKey());
    if (emh != null) {
      // lecture du resultat Cini associé à l'EMH
      AbstractResPrtCIni res = (AbstractResPrtCIni) EMHHelper.selectInfoEMH(emh, EnumInfosEMH.RES_PRT_CINIT);
      if (res != null) {
        return res.getValue(key.getVariableName());
      }
    }
    return null;
  }

  /**
   * @param key la clé demande
   * @param emhNom le nom de l'EMH support
   * @return la valeur RPTI de la variable "key" pour l'EMH emhNom
   */
  private Double getLHPTValue(ReportRunVariableKey key, String emhNom) {
    ManagerEMHScenario managerEMHScenario;
    if (key.getRunKey().isCourant()) {
      managerEMHScenario = getRunCourant().getManagerScenario();
    } else {
      managerEMHScenario = lhptService.getProjetService().getSelectedProject().getScenario(key.getRunKey().getManagerNom());
    }
    if (managerEMHScenario == null) {
      return null;
    }
    final LhptService.LoiMesurePostState mesurePostState = lhptService.loadLoiMesure(managerEMHScenario, null);
    if (mesurePostState == null) {
      return null;
    }

    final String variableName = key.getVariableName();
    if (variableName.startsWith(EnumTypeLoi.LoiSectionsZ.getNom())) {
      final LoiTF loiTF = mesurePostState.getLoiMesuresPost().getLoiSectionZ(variableName);
      if (loiTF == null) {
        return null;
      }
      String profil = mesurePostState.getLoiMesuresPost().getEchellesSections().getEchelleFromSection(emhNom);
      if (profil == null) {
        return null;
      }

      for (PtEvolutionTF point : loiTF.getEvolutionTF().getPtEvolutionTF()) {
        if (point.getAbscisse().equals(profil)) {
          return point.getOrdonnee();
        }
      }
    }
    return null;
  }

  /**
   * @param key la clé demande
   * @param emhNom le nom de L'EMH support
   * @param decimalFormat le format d'affichage
   * @return getRPTIValue formattée
   */
  private String getRPTIValueAsString(ReportRunVariableKey key, String emhNom, DecimalFormatEpsilonEnum decimalFormat) {
    Double val = getRPTIValue(key, emhNom);
    if (val == null) {
      return StringUtils.EMPTY;
    }
    return TransformerEMHHelper.formatFromPropertyName(key.getVariable().getVariableName(), val, reportService.getCcm(), decimalFormat);
  }

  /**
   * @param key la clé demande
   * @param emhNom le nom de L'EMH support
   * @param decimalFormat le format d'affichage
   * @return getRPTIValue formattée
   */
  private String getLHPTValueAsString(ReportRunVariableKey key, String emhNom, DecimalFormatEpsilonEnum decimalFormat) {
    Double val = getLHPTValue(key, emhNom);
    if (val == null) {
      return StringUtils.EMPTY;
    }
    return TransformerEMHHelper.formatFromPropertyName(AocExecVariableHelper.PROP_Z, val, reportService.getCcm(), decimalFormat);
  }

  public ItemVariable getCcmVariable(String variableName) {
    return reportService.getCcm().getProperty(variableName);
  }

  /**
   * @param reportVariableKey la clé initiales
   * @return l'itemVariable correspondante. Si c'est une formule, renvoie null.
   */
  public ItemVariable getCcmVariable(ReportVariableKey reportVariableKey) {
    if (reportVariableKey == null) {
      return null;
    }
    if (reportVariableKey.getVariableType().equals(ReportVariableTypeEnum.READ)) {
      return reportService.getCcm().getProperty(reportVariableKey.getVariableName());
    }
    return null;
  }

  public ItemVariable getCcmVariable(ReportRunVariableKey reportRunVariableKey) {
    if (reportRunVariableKey == null) {
      return null;
    }
    return getCcmVariable(reportRunVariableKey.getVariable());
  }

  /**
   * @param timeKey le pas de temps initial
   * @return pour les runs alternatifs, renvoir le pas de temps réel et non l'équivalent du run cournt
   * @see ReportReadResultProviderService#getInitialTimeKey(ResultatTimeKey, ReportRunKey, EMH)
   */
  public ResultatTimeKey getInitialTimeKey(ResultatTimeKey timeKey, ReportRunKey runKey, String emhName) {
    return reportReadResultProviderService.getInitialTimeKey(timeKey, runKey, emhName);
  }
}
