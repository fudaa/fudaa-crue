/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.longitudinal;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.lit.ReportLongitudinalLimitHelper;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.helper.ResPrtHelper;
import org.fudaa.dodico.crue.metier.result.OrdResExtractor;
import org.fudaa.dodico.crue.metier.result.OrdResVariableSelection;
import org.fudaa.dodico.crue.projet.report.data.*;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalConfig;
import org.fudaa.fudaa.crue.aoc.service.LhptService;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.report.helper.ReportDefaultVariableChooserUI;
import org.fudaa.fudaa.crue.report.ordres.ReportOrdResSelectionByRun;
import org.fudaa.fudaa.crue.report.ordres.ReportOrdResSelectionByRunLine;
import org.fudaa.fudaa.crue.report.ordres.ReportOrdResSelectionByRunLineNode;
import org.fudaa.fudaa.crue.report.service.ReportFormuleService;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.fudaa.fudaa.crue.report.service.ReportRunAlternatifService;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import java.util.*;

/**
 * @author Frederic Deniger
 */
public class ReportLongitudinalAsZVariableChooser {
  final ReportLongitudinalConfig config;
  final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);
  final ReportResultProviderService reportResultProviderService = Lookup.getDefault().lookup(ReportResultProviderService.class);
  final ReportFormuleService reportFormuleService = Lookup.getDefault().lookup(ReportFormuleService.class);
  final LhptService lhptService = Lookup.getDefault().lookup(LhptService.class);

  public ReportLongitudinalAsZVariableChooser(final ReportLongitudinalConfig config) {
    this.config = config;
  }

  public boolean displayChooseUI() {
    final EMHScenario scenario = reportService.getRunCourant().getScenario();
    final OrdResExtractor extractor = new OrdResExtractor(scenario.getOrdResScenario());
    final List<String> selectableVariables = extractor.getSelectableVariables(EnumCatEMH.SECTION, reportFormuleService.getVariablesKeys());
    final CrueConfigMetier ccm = reportService.getCcm();
    //on ne conserve que les variables qui sont affichées sur l'axe des z:
    for (final Iterator<String> it = selectableVariables.iterator(); it.hasNext(); ) {
      final String string = it.next();
      if (!ReportRunVariableHelper.isNatZVar(string, reportResultProviderService)) {
        it.remove();
      }
    }
    //on récupère les variables limite, etiquettes
    final List<String> variables = new ArrayList<>();
    final ReportLongitudinalLimitHelper helper = new ReportLongitudinalLimitHelper(ccm);
    //permet de faire le lien entre chaine affiché -> id
    final LinkedHashMap<String, String> limitesLitsId = helper.getIdByDisplayName();
    variables.addAll(limitesLitsId.keySet());
    final Map<String, String> displayNameById = new HashMap<>();
    for (final Map.Entry<String, String> entry : limitesLitsId.entrySet()) {
      displayNameById.put(entry.getValue(), entry.getKey());
    }

    // ajout Zini issu du RPTI
    final List<String> varZIni = ResPrtHelper.getZiniVar();
    variables.addAll(varZIni);

    // Ajout du Z
    variables.addAll(selectableVariables);

    //ajout des donnees issues du LHPT
    variables.addAll(lhptService.getLoiSectionZFor(reportService.getRunCourant().getManagerScenario()));

    final ReportDefaultVariableChooserUI variableChooserUi = new ReportDefaultVariableChooserUI(reportResultProviderService);
    final List<ReportOrdResSelectionByRunLineNode> nodes = getNodes(variables, displayNameById);
    variableChooserUi.setNodes(nodes);

    final boolean ok = DialogHelper.showQuestionAndSaveDialogConf(NbBundle.getMessage(ReportLongitudinalAsZVariableChooser.class,
        "ChooseAsZVariable.DialogTitle"), variableChooserUi, getClass(), "vueProfilLongitudinal_ChoixVariableAxeZ", PerspectiveEnum.REPORT, false,
        DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    if (ok) {
      final List<ReportRunVariableKey> rightVariables = config.getReportRunVariableRightAxis(reportResultProviderService);
      final List<ReportRunVariableKey> zVariables = new ArrayList<>();
      config.getProfilVariables().clear();
      for (final ReportOrdResSelectionByRunLineNode node : nodes) { //pour respecter l'ordre
        final ReportOrdResSelectionByRunLine line = node.getLine();
        final String variable = line.getVariable();
        final ReportVariableKey variableKey = reportResultProviderService.createVariableKey(variable);
        final List<ReportOrdResSelectionByRun> selectionByRuns = line.getSelectionByRun();
        for (final ReportOrdResSelectionByRun byRun : selectionByRuns) {
          if (byRun.getSelection().isVariableSelected(variable)) {
            if (limitesLitsId.containsKey(variable)) {
              final String limiteKey = limitesLitsId.get(variable);
              config.getProfilVariables().add(new ReportRunVariableKey(byRun.getRunKey(), new ReportVariableKey(ReportVariableTypeEnum.LIMIT_PROFIL,
                  limiteKey)));
            } else if (varZIni.contains(variable)) {
              config.getProfilVariables().add(new ReportRunVariableKey(byRun.getRunKey(), new ReportVariableKey(ReportVariableTypeEnum.RESULTAT_RPTI,
                  variable)));
            } else {
              zVariables.add(new ReportRunVariableKey(byRun.getRunKey(), variableKey));
            }
          }
        }
      }
      config.getProfilVariables().addAll(zVariables);
      config.getProfilVariables().addAll(rightVariables);
    }

    return ok;
  }

  private List<ReportOrdResSelectionByRunLineNode> getNodes(final List<String> variables, final Map<String, String> displayNameById) {
    final ReportRunContent runCourant = reportResultProviderService.getReportService().getRunCourant();
    final List<ReportOrdResSelectionByRun> selectionByRunList = createSelectionByRun(runCourant, variables);
    //on met à jour la sélection.
    for (final ReportOrdResSelectionByRun selection : selectionByRunList) {
      final List<String> variableToSelect = config.getVariablesAsZDrawn(selection.getRunKey(), reportResultProviderService);
      for (final String vKey : variableToSelect) {
        String key = vKey;
        if (displayNameById.containsKey(key)) {
          key = displayNameById.get(key);
        }
        selection.getSelection().setVariableSelected(key, true);
      }
    }
    //les lignes
    final List<ReportOrdResSelectionByRunLineNode> nodes = new ArrayList<>();
    for (final String var : variables) {
      final ReportOrdResSelectionByRunLine line = new ReportOrdResSelectionByRunLine();
//      line.setKey(OrdResSection.KEY);
      line.setVariable(var);
      line.setSelectionByRun(selectionByRunList);
      final ReportOrdResSelectionByRunLineNode node = new ReportOrdResSelectionByRunLineNode(line);
      nodes.add(node);
    }
    return nodes;
  }

  public List<ReportOrdResSelectionByRun> createSelectionByRun(final ReportRunContent runCourant, final List<String> variables) {
    final List<ReportOrdResSelectionByRun> selectionByRunList = new ArrayList<>();
    //pour le run courant
    final ReportOrdResSelectionByRun currentRun = new ReportOrdResSelectionByRun(runCourant.getRunKey(), new OrdResVariableSelection(null, variables,
        variables));
    selectionByRunList.add(currentRun);
    final ReportRunAlternatifService reportRunAlternatifService = reportResultProviderService.getReportRunAlternatifService();
    //pour les runs alternatifs:
    final List<ReportRunKey> sortedReportRunContentKeys = reportRunAlternatifService.getSortedReportRunContentKeys();
    for (final ReportRunKey reportRunKey : sortedReportRunContentKeys) {
      final ReportOrdResSelectionByRun selection = new ReportOrdResSelectionByRun(reportRunKey,
          new OrdResVariableSelection(null, variables, variables));
      selectionByRunList.add(selection);
    }
    return selectionByRunList;
  }
}
