/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.planimetry;

import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.planimetry.controller.PlanimetryController;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportPlanimetryConfigureAction extends EbliActionSimple {

  final PlanimetryController planimetryController;
  ReportService reportService = Lookup.getDefault().lookup(ReportService.class);

  public ReportPlanimetryConfigureAction(PlanimetryController planimetryController) {
    super(NbBundle.getMessage(ReportPlanimetryConfigureAction.class, "ConfigureAction.Name"), BuResource.BU.getIcon("crystal_couleur"), "CONFIGURE");
    this.planimetryController = planimetryController;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    if (new ReportPlanimetryConfigUI(planimetryController).configure()) {
      planimetryController.getVisuPanel().getVueCalque().repaint(0);
    }
  }

  @Override
  public void updateStateBeforeShow() {
    setEnabled(true);
  }
}
