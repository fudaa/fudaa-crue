/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.extern;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.projet.report.persist.AbstractReportCourbeConfig;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.openide.DialogDescriptor;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ExternFileChooser {

  final AbstractReportCourbeConfig config;
  final PerspectiveServiceReport perspectiveServiceReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);

  public ExternFileChooser(AbstractReportCourbeConfig config) {
    this.config = config;
  }

  public boolean displayChooseUI() {
    ExternFileChooserUI view = new ExternFileChooserUI();
    view.setEditable(perspectiveServiceReport.isInEditMode());
    List<String> externFiles = config.getExternFiles();

    final List<ExternFileContentNode> nodes = new ArrayList<>();
    for (String file : externFiles) {
      ExternFileContentNode node = new ExternFileContentNode(new ExternFileContent(new File(file)));
      nodes.add(node);
    }
    view.getExplorerManager().setRootContext(NodeHelper.createNode(nodes, "files", false));
    DefaultNodePasteType.updateIndexedDisplayName(view.getExplorerManager().getRootContext());
    DialogDescriptor descriptor = new DialogDescriptor(view, NbBundle.getMessage(ExternFileChooser.class, "fileExtern.DialogTitle"));
    DialogHelper.readInPreferences(view.getView(), "outlineView", ExternFileChooser.class);
    SysdocUrlBuilder.installDialogHelpCtx(descriptor, "choisirFichierExterne", PerspectiveEnum.REPORT,false);
    boolean ok = DialogHelper.showQuestion(descriptor, getClass(), DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    DialogHelper.writeInPreferences(view.getView(), "outlineView", ExternFileChooser.class);
    if (ok) {
      List<String> files = new ArrayList<>();
      Node[] finalNodes = view.getExplorerManager().getRootContext().getChildren().getNodes();
      for (Node node : finalNodes) {
        final File file = node.getLookup().lookup(ExternFileContent.class).getFile();
        if (file != null) {
          try {
            files.add(file.getCanonicalPath());
          } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
          }
        }
      }
      config.setExternalFiles(files);

    }
    return ok;
  }
}
