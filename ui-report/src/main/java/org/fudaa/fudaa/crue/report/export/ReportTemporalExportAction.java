/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.export;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fudaa.dodico.crue.projet.report.data.ReportRunKey;
import org.fudaa.dodico.crue.projet.report.export.ReportExportAbstract;
import org.fudaa.dodico.crue.projet.report.export.ReportExportTemporal;
import org.fudaa.fudaa.crue.report.service.ReportResultProviderService;
import org.fudaa.fudaa.crue.report.temporal.ReportTemporalTopComponent;

/**
 *
 * @author Frederic Deniger
 */
public class ReportTemporalExportAction extends ReportCourbeExportActionAbstract<ReportTemporalTopComponent> {

  public ReportTemporalExportAction(ReportTemporalTopComponent topComponent) {
    super(topComponent);
  }

  @Override
  protected ReportExportAbstract createReportExporter(ReportResultProviderService reportResultProviderService) {
    return new ReportExportTemporal(topComponent.getReportConfig(), reportResultProviderService);
  }

  @Override
  protected List<String> getChoosableVariables() {
    final List<String> choosableVariables = new ArrayList<>(Arrays.asList(topComponent.getChoosableVariables()));
    choosableVariables.remove(null);
    return choosableVariables;
  }

  @Override
  protected List<String> getEMHs() {
    return topComponent.
            getReportConfig().getEmhs();
  }

  @Override
  protected Set<String> getCurrentSelectedVariables() {
    return new HashSet<>(topComponent.getReportConfig().getVariables());
  }

  @Override
  protected void fillWithSelectedRunKeys(Set<ReportRunKey> keys) {
    final LinkedHashMap<String, Set<ReportRunKey>> variablesMap = topComponent.getReportConfig().getVariablesMap();
    for (Map.Entry<String, Set<ReportRunKey>> entrySet : variablesMap.entrySet()) {
      keys.addAll(entrySet.getValue());
    }
  }

}
