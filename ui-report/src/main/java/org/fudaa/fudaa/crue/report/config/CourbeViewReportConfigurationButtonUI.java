/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.config;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.loi.LabelConfig;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.fudaa.crue.loi.CourbeViewConfigurationButtonUI;

/**
 * Cree un bouton qui contient les menus nécessaires pour configurer une CourbeView.
 *
 * @author Frederic Deniger
 */
public class CourbeViewReportConfigurationButtonUI extends CourbeViewConfigurationButtonUI<CourbeViewReportConfigurationTarget> {

  public CourbeViewReportConfigurationButtonUI(CourbeViewReportConfigurationTarget topComponent) {
    super(topComponent);
  }

  protected List<ReportVariableKey> getTitleAvailableVariables() {
    return target.getTitleAvailableVariables();
  }

  @Override
  public void configureTitle() {
    LabelConfig titleConfig = target.getManager().getConfig().getTitleConfig();
    ReportLabelContent content = (ReportLabelContent) titleConfig.getKey();
    if (content == null) {
      content = new ReportLabelContent();
      titleConfig.setKey(content);
    }
    final ReportLabelContentConfig titleVariable = new ReportLabelContentConfig();
    titleVariable.setConfig(titleConfig);
    titleVariable.setContent(content);
    boolean configure = new ReportLabelContentConfigUI(getTitleAvailableVariables()).configure(titleVariable);
    if (configure) {
      target.applyTitleChanged();
    }
  }

  @Override
  public void configureLabels() {
    List<LabelConfig> labels = target.getManager().getConfig().getLabels();
    List<ReportLabelContentConfig> reportConfig = new ArrayList<>();
    for (LabelConfig labelConfig : labels) {
      reportConfig.add(new ReportLabelContentConfig((ReportLabelContent) labelConfig.getKey(), labelConfig));
    }
    ReportMultiLabelContentConfigUI config = new ReportMultiLabelContentConfigUI(target.getTitleAvailableVariables());
    List<ReportLabelContentConfig> finalConfigLabels = config.configure(reportConfig);
    if (finalConfigLabels != null) {
      List<LabelConfig> finalLabels = new ArrayList<>();
      for (ReportLabelContentConfig reportLabelContentConfig : finalConfigLabels) {
        LabelConfig item = reportLabelContentConfig.getConfig();
        item.setKey(reportLabelContentConfig.getContent());
        finalLabels.add(item);
      }
      target.getManager().getConfig().setLabels(finalLabels);
      target.applyLabelsChanged();
    }
  }
}
