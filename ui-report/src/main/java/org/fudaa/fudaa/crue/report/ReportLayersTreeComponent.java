package org.fudaa.fudaa.crue.report;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZCalqueImageRaster;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanelBuilder;
import org.fudaa.fudaa.crue.planimetry.controller.AbstractGroupExternController;
import org.fudaa.fudaa.crue.planimetry.layer.PlanimetryAdditionalLayerContrat;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.fudaa.fudaa.crue.report.service.ReportVisuPanelService;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Contient l'arbres de calques. Devrait être renomé.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.report//ReportLayersTreeComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = ReportLayersTreeComponent.TOPCOMPONENT_ID,
        iconBase = "org/fudaa/fudaa/crue/report/rond-orange_16.png",
        persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "report-bottomLeft", openAtStartup = false, position = 1)
@ActionReference(path = "Menu/Window/Report", position = 6)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.report.ReportLayersTreeComponent")
@TopComponent.OpenActionRegistration(displayName = ReportLayersTreeComponent.TOPCOMPONENT_ACTION_DISPLAYNAME,
        preferredID = ReportLayersTreeComponent.TOPCOMPONENT_ID)
public final class ReportLayersTreeComponent extends AbstractReportTopComponent implements LookupListener, PropertyChangeListener, Observer {

  public static final String TOPCOMPONENT_ID = "ReportLayersTreeComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;
  private final ReportVisuPanelService reportVisuService = Lookup.getDefault().lookup(
          ReportVisuPanelService.class);
  private Lookup.Result<PlanimetryVisuPanel> resultatVisuPanel;
  final PerspectiveServiceReport perspectiveServiceReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);

  public ReportLayersTreeComponent() {
    initComponents();
    setName(NbBundle.getMessage(ReportLayersTreeComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(ReportLayersTreeComponent.class, "HINT_" + TOPCOMPONENT_ID));
  }

  @Override
  protected String getHelpCtxId() {
    return SysdocUrlBuilder.getTopComponentHelpCtxId("vueGestionnaireCalques", PerspectiveEnum.REPORT);
  }

  @Override
  protected void setEditable(boolean b) {
    if (panel != null) {
      panel.getPlanimetryController().getGroupExternController().setArbreEditable(b);
    }
    if (bArbreCalque != null) {
      bArbreCalque.setEditable(b);
    }
    if (oneLayer != null) {
      oneLayer.setEnabled(b);
    }
  }

  @Override
  public void setModified(boolean modified) {
    super.setModified(modified);
  }
  private PlanimetryVisuPanel panel;
  private BArbreCalque bArbreCalque;
  private JCheckBox oneLayer;

  @Override
  protected void runLoaded() {
    if (reportVisuService.getPlanimetryVisuPanel() != null) {
      scenarioVisuLoaded();
    }
  }

  protected void scenarioVisuLoaded() {
    panel = reportVisuService.getPlanimetryVisuPanel();
    this.removeAll();
    bArbreCalque = new BArbreCalque(panel.getArbreCalqueModel());
    bArbreCalque.setRootVisible(false);
    oneLayer = new JCheckBox(NbBundle.getMessage(PlanimetryVisuPanelBuilder.class, "OneLayer.Action"));
    oneLayer.setToolTipText(NbBundle.getMessage(PlanimetryVisuPanelBuilder.class, "OneLayer.Tooltip"));
    oneLayer.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        panel.setWokOnOneLayer(oneLayer.isSelected());
      }
    });
    add(oneLayer, BorderLayout.NORTH);
    add(new JScrollPane(bArbreCalque));
    this.repaint();
    this.revalidate();
    List<BCalque> mainLayers = panel.getPlanimetryController().getMainLayers();
    for (BCalque bCalque : mainLayers) {
      bCalque.addPropertyChangeListener(BSelecteurCheckBox.PROP_USER_VISIBLE, this);
    }
    panel.getPlanimetryController().getGroupExternController().addObserver(this);

  }
  private final Set<String> propertiesToListenTo = new HashSet<>(
          Arrays.asList("modele", ZCalqueImageRaster.PROP_IMAGE, AbstractGroupExternController.PROPERTY_LAYER_ADDED,
          AbstractGroupExternController.PROPERTY_LAYER_REMOVED,
          PlanimetryAdditionalLayerContrat.PROPERTY_LAYER_CONFIGURATION));

  @Override
  public void update(Observable o, Object arg) {
    if (updating) {
      return;
    }
    if (propertiesToListenTo.contains(arg)) {
      setAdditonnalLayerModified();
    }
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
//    System.err.println("alors....");
//    reportService.setStudyConfigModified();
  }

  @Override
  public void runUnloaded() {
    if (panel != null) {
      List<BCalque> mainLayers = panel.getPlanimetryController().getMainLayers();
      for (BCalque bCalque : mainLayers) {
        bCalque.removePropertyChangeListener(BSelecteurCheckBox.PROP_USER_VISIBLE, this);
      }
      panel.getPlanimetryController().getGroupExternController().deleteObserver(this);
    }
    this.removeAll();
    bArbreCalque = null;
    this.add(new JLabel(NbBundle.getMessage(ReportLayersTreeComponent.class, "TopComponent.NoScenarioLoadedInformations")));
    super.revalidate();
    this.repaint();
  }
  boolean alreadyOpened;

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.BorderLayout());
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  @Override
  public void componentClosedTemporarily() {
  }

  void writeProperties(java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
    p.setProperty("version", "1.0");
  }

  void readProperties(java.util.Properties p) {
  }

  private class VisuPanelLookupListener implements LookupListener {

    @Override
    public void resultChanged(LookupEvent ev) {
      if (reportVisuService.getPlanimetryVisuPanel() != null) {
        scenarioVisuLoaded();
      }
    }
  }
  VisuPanelLookupListener visuLookupListener;
  private final PropertyChangeListener dirtyPropertyChangeListener = new PropertyChangeListener() {
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if (perspectiveServiceReport.isDirty()) {
        ReportLayersTreeComponent.this.setModified(false);
      }
    }
  };

  @Override
  protected void componentOpenedHandler() {
    if (resultatVisuPanel == null) {
      visuLookupListener = new VisuPanelLookupListener();
      resultatVisuPanel = reportVisuService.getLookup().lookupResult(PlanimetryVisuPanel.class);
      resultatVisuPanel.addLookupListener(visuLookupListener);
    }
    perspectiveServiceReport.addDirtyListener(dirtyPropertyChangeListener);

  }

  @Override
  protected void componentClosedDefinitlyHandler() {
    super.componentClosedDefinitlyHandler();
    if (resultatVisuPanel != null) {
      resultatVisuPanel.removeLookupListener(visuLookupListener);
      resultatVisuPanel = null;
    }
    perspectiveServiceReport.removeDirtyListener(dirtyPropertyChangeListener);
  }
}
