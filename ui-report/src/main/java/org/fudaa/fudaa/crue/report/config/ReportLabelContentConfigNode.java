/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.config;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.projet.report.data.ReportVariableKey;
import org.fudaa.dodico.crue.projet.report.loi.ReportLabelContent;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLabelContentConfigNode extends AbstractNodeFirable {

  public ReportLabelContentConfigNode(ReportLabelContentConfig config, List<ReportVariableKey> availableVar) {
    super(Children.LEAF, Lookups.fixed(config, availableVar));
    assert availableVar != null;
    setNoImage();
    setDisplayName(config.getContent().getDisplayName());
  }

  @Override
  public boolean isEditMode() {
    return true;
  }

  @Override
  public boolean canDestroy() {
    return true;
  }

  @Override
  protected Sheet createSheet() {
    ReportLabelContentConfig config = getLookup().lookup(ReportLabelContentConfig.class);
    List<ReportVariableKey> vars = getLookup().lookup(List.class);
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    PropertyNodeBuilder nodeBuilder = new PropertyNodeBuilder();
    List<PropertySupportReflection> createFromPropertyDesc = nodeBuilder.createFromPropertyDesc(DecimalFormatEpsilonEnum.PRESENTATION, config.getContent(), this);
    for (PropertySupportReflection propertySupportReflection : createFromPropertyDesc) {
      if (ReportLabelContent.PROP_CONTENT.equals(propertySupportReflection.getName())) {
        String[] varsAsString = new String[vars.size()];
        Map<String, Object> translate = new HashMap<>();
        for (int i = 0; i < varsAsString.length; i++) {
          final ReportVariableKey varAt = vars.get(i);
          varsAsString[i] = varAt == null ? StringUtils.EMPTY : varAt.geti18n();
          translate.put(varsAsString[i], vars.get(i));
        }
        propertySupportReflection.setTags(varsAsString);
        propertySupportReflection.setTranslationForObjects(translate);
      }

      set.put(propertySupportReflection);
    }
    createFromPropertyDesc = nodeBuilder.createFromPropertyDesc(DecimalFormatEpsilonEnum.PRESENTATION, config.getConfig(), this);
    for (PropertySupportReflection propertySupportReflection : createFromPropertyDesc) {
      set.put(propertySupportReflection);
    }
    sheet.put(set);
    addPropertyChangeListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        ReportLabelContentConfig config = getLookup().lookup(ReportLabelContentConfig.class);
        setDisplayName(config.getContent().getDisplayName());
      }
    });
    return sheet;
  }
}
