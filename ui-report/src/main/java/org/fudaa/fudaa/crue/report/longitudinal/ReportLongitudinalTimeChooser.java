/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.longitudinal;

import com.jidesoft.swing.CheckBoxList;
import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.longitudinal.ReportLongitudinalConfig;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.CheckBoxListPopupListener;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.ToStringTransfomerCellRenderer;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ReportLongitudinalTimeChooser {

  final ReportLongitudinalConfig config;
  final ReportService reportService = Lookup.getDefault().lookup(ReportService.class);

  public ReportLongitudinalTimeChooser(final ReportLongitudinalConfig config) {
    this.config = config;
  }

  public boolean displayChooseUI() {
    final List<ResultatTimeKey> times = reportService.getTimeContent().getTimes();
    final ToStringTransformer resulatKeyTempsSceToStringTransformer = reportService.getReportTimeFormatter().
            getResulatKeyTempsSceToStringTransformer();
    final ToStringTransfomerCellRenderer renderer = new ToStringTransfomerCellRenderer(resulatKeyTempsSceToStringTransformer);
    final CheckBoxList list = new CheckBoxList(times.toArray(new ResultatTimeKey[0]));
    list.setCellRenderer(renderer);
    new CheckBoxListPopupListener(list);
    for (int i = 0; i < times.size(); i++) {
      if (config.getTimes().contains(times.get(i))) {
        list.addCheckBoxListSelectedIndex(i);
      }
    }
    final JPanel pnCenter = new JPanel(new BorderLayout(0, 5));
    final JCheckBox selectAll = new JCheckBox(NbBundle.getMessage(ReportLongitudinalTimeChooser.class, "ChooseTime.SelectAllNone"));
    selectAll.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(final ItemEvent e) {
        final boolean select = selectAll.isSelected();
        if (select) {
          list.selectAll();
        } else {
          list.selectNone();
        }
      }
    });
    pnCenter.add(new JScrollPane(list));
    pnCenter.add(selectAll, BorderLayout.NORTH);
    pnCenter.setBorder(BorderFactory.createEmptyBorder(10, 3, 0, 3));
    final JPanel pn = new JPanel(new BorderLayout(0, 5));
    final JComboBox currentTimeDisplayed = new JComboBox(new String[]{
      org.openide.util.NbBundle.getMessage(ReportLongitudinalTimeChooser.class, "ChooseTime.DisplayCurrentTime"),
      org.openide.util.NbBundle.getMessage(ReportLongitudinalTimeChooser.class, "ChooseTime.NotDisplayCurrentTime")

    });
    currentTimeDisplayed.setSelectedIndex(0);
    if (Boolean.FALSE.equals(config.getCurrentTimeDisplayed())) {
      currentTimeDisplayed.setSelectedIndex(1);

    }
    pn.add(currentTimeDisplayed, BorderLayout.NORTH);
    pn.add(pnCenter);

    final String title = org.openide.util.NbBundle.getMessage(ReportLongitudinalTimeChooser.class, "ChooseTime.DialogTitle");
    final boolean ok = DialogHelper.showQuestionAndSaveDialogConf(title, pn, getClass(), "vueProfilLongitudinal_ConfigurationPasDeTemps",
            PerspectiveEnum.REPORT, true, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    if (ok) {
      config.getTimes().clear();
      config.setCurrentTimeDisplayed(currentTimeDisplayed.getSelectedIndex() == 0);
      final Object[] selectedValues = list.getCheckBoxListSelectedValues();
      for (final Object object : selectedValues) {
        config.getTimes().add((ResultatTimeKey) object);

      }
    }
    return ok;
  }
}
