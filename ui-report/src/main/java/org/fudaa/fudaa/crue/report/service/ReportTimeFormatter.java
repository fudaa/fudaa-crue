/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.service;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.result.ResultKeyFormatter;

/**
 *
 * @author Frederic Deniger
 */
public class ReportTimeFormatter extends ResultKeyFormatter {

  ReportService service;

  protected void setService(ReportService service, EMHScenario scenario) {
    super.setCcmAndScenario(service.getCcm(), scenario);
    this.service = service;
  }

  public String getTempsSceAsString() {
    return getResulatKeyTempsSceToStringTransformer().transform(service.getSelectedTime());
  }

  public String getTempsCalcAsString() {
    return getResulatKeyTempsSimuToStringTransformer().transform(service.getSelectedTime());
  }

}
