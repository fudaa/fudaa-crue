package org.fudaa.fudaa.crue.report.node;

import javax.swing.Action;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.fudaa.fudaa.crue.report.actions.ReportSetAsAlternatifNodeAction;
import org.fudaa.fudaa.crue.report.actions.ReportUnsetAsAlternatifNodeAction;
import org.fudaa.fudaa.crue.report.property.CommentaireProperty;
import org.fudaa.fudaa.crue.report.property.RunProperty;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Frederic Deniger
 */
public class ReportRunNode extends AbstractNode {

  boolean loaded;
  boolean alternatif;
  
  public ReportRunNode(final EMHRun run) {
    super(Children.LEAF, Lookups.singleton(run));
    setName(run.getId());
    setDisplayName(run.getNom());
    setShortDescription(run.getNom());
    setIconBaseWithExtension(CrueIconsProvider.getIconBaseRun());
  }
  
  public void setRunLoaded(final boolean loaded) {
    if (this.loaded == loaded) {
      return;
    }
    this.loaded = loaded;
    fireDisplayNameChange(null, getDisplayName());
  }

  public void setRunAlternatif(final boolean alternatif) {
    if (this.alternatif == alternatif) {
      return;
    }
    this.alternatif = alternatif;
    final EMHRun run = getRun();
    if (alternatif) {
      setDisplayName(NbBundle.getMessage(ReportRunNode.class, "RunAlternatifName", run.getNom()));
      setShortDescription(getDisplayName());
    } else {
      setDisplayName(run.getNom());
      setShortDescription(getDisplayName());
    }
    fireDisplayNameChange(null, getDisplayName());
  }

  public EMHRun getRun() {
    return getLookup().lookup(EMHRun.class);
  }

  @Override
  public Action[] getActions(final boolean context) {
    return new Action[]{
      SystemAction.get(ReportSetAsAlternatifNodeAction.class),
      SystemAction.get(ReportUnsetAsAlternatifNodeAction.class)
    };
  }

  public boolean isSameRun(final EMHRun run) {
    return getRun().getId().equals(run.getId());
  }

  @Override
  public String getHtmlDisplayName() {
    return loaded ? "<b>" + getDisplayName() + "</b>" : null;
  }

  @Override
  public boolean canDestroy() {
    return false;
  }

  @Override
  public Sheet createSheet() {
    final Sheet sheet = Sheet.createDefault();
    final Sheet.Set set = Sheet.createPropertiesSet();

    final Property run = new RunProperty(getRun());
    final Property commentaire = new CommentaireProperty(getRun());

    set.put(run);
    set.put(commentaire);

    sheet.put(set);

    return sheet;
  }
}
