/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.multivar;

import gnu.trove.TDoubleArrayList;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.projet.report.ReportMultiVarConfig;
import org.fudaa.dodico.crue.projet.report.data.*;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.fudaa.crue.loi.ViewCourbeManager;
import org.fudaa.fudaa.crue.loi.common.CourbesUiController;
import org.fudaa.fudaa.crue.loi.common.LoiConstanteCourbeModel;
import org.fudaa.fudaa.crue.report.config.AbstractReportGrapheBuilder;
import org.netbeans.api.progress.ProgressHandle;

import java.util.*;

/**
 * @author Frederic Deniger
 */
public class ReportMultiVarGrapheBuilder extends AbstractReportGrapheBuilder<CourbesUiController, ReportMultiVarConfig> {

    public ReportMultiVarGrapheBuilder(CourbesUiController uiController, ViewCourbeManager loiLabelsManager) {
        super(uiController, loiLabelsManager);
    }

    @Override
    public List<EGCourbeSimple> getInternCourbes(ReportMultiVarConfig content, ProgressHandle progress) {
        if (!reportService.isRunLoaded()) {
            return Collections.emptyList();
        }
        EGAxeHorizontal axeX = uiController.getEGGrapheSimpleModel().getAxeX();
        if (axeX == null) {
            axeX = new EGAxeHorizontal();
            uiController.getEGGrapheSimpleModel().setAxeX(axeX);
        }
        //si pas de variable ou non valide, on renvoie vide
        if (content.getHorizontalVar() == null || !reportResultProviderService.isExpressionValid(content.getHorizontalVar(), content.getSelectedTimeStepInReport())) {
            return Collections.emptyList();
        }
        PropertyNature propertyHorizontal = reportResultProviderService.getPropertyNature(content.getHorizontalVar());

        //on change de variables horizontales: on oublie les pref.
        if (propertyHorizontal != axeX.getUserObject()) {
            content.saveAxeH(null);
        }
        axeX.setTitre(propertyHorizontal.getDisplayName());
        axeX.setUniteVisible(true);
        axeX.setUnite(propertyHorizontal.getUnite());
        axeX.setTitleModifiable(false);
        axeX.setUniteModifiable(false);
        axeX.setSpecificFormat(new CtuluNumberFormatDefault(propertyHorizontal.getFormatter(DecimalFormatEpsilonEnum.PRESENTATION)));
        axeX.setSpecificDetailFormat(new CtuluNumberFormatDefault(propertyHorizontal.getFormatter(DecimalFormatEpsilonEnum.COMPARISON)));
        axeX.setUserObject(propertyHorizontal);
        Map<String, List<ReportRunKey>> keyByVar = ReportRunVariableHelper.getByEmh(content.getReportRunEmhs());
        List<ResultatTimeKey> times = reportService.getRangeSelectedTimeKeys();
        List<EGCourbeSimple> courbes = new ArrayList<>();
        Map<String, EGAxeVertical> saved = new HashMap<>();
        for (String variableName : content.getVariables()) {
            //variable non valide
            if (!reportResultProviderService.isExpressionValid(variableName, content.getSelectedTimeStepInReport())) {
                continue;
            }
            PropertyNature property = reportResultProviderService.getPropertyNature(variableName);
            ReportVariableKey variableKey = reportResultProviderService.createVariableKey(variableName);
            EGAxeVertical axeVertical = getOrCreateAxeVerticalConfigured(property, content, saved);
            for (String emhName : content.getEmhs()) {
                List<ReportRunKey> keys = keyByVar.get(emhName);
                if (keys != null) {
                    for (ReportRunKey runKey : keys) {
                        ReportRunVariableEmhKey key = new ReportRunVariableEmhKey(runKey, variableKey, emhName);
                        if (progress != null) {
                            progress.setDisplayName(key.getDisplayName());
                        }
                        LoiConstanteCourbeModel model = createCourbeModel(content, times, key);
                        if (model != null) {
                            EGCourbeSimple courbe = new EGCourbeSimple(axeVertical, model);
                            final EGCourbePersist persist = content.getCourbeconfigs().get(key);
                            applyPersistConfig(persist, courbe);
                            courbes.add(courbe);
                        }
                    }
                }
            }
        }
        return courbes;
    }

    @Override
    public void updateResultCurvesAfterTimeChanged(ReportMultiVarConfig content) {
    }

    @Override
    public List<EGCourbeSimple> getInternCourbesAfterTimeChanged(ReportMultiVarConfig content, ProgressHandle progress) {
        return Collections.emptyList();
    }

    private LoiConstanteCourbeModel createCourbeModel(ReportMultiVarConfig config, List<ResultatTimeKey> times, ReportRunVariableEmhKey key) {
        ReportRunVariableKey hKey = new ReportRunVariableKey(key.getRunVariableKey().getRunKey(), reportResultProviderService.createVariableKey(
                config.getHorizontalVar()));
        TDoubleArrayList x = new TDoubleArrayList();
        TDoubleArrayList y = new TDoubleArrayList();
        for (ResultatTimeKey resultatKey : times) {
            Double foundX = reportResultProviderService.getValue(resultatKey, hKey, key.getEmhName(), config.getSelectedTimeStepInReport());
            Double foundY = reportResultProviderService.getValue(resultatKey, key.getRunVariableKey(), key.getEmhName(), config.
                    getSelectedTimeStepInReport());
            if (foundX != null && foundY != null) {
                x.add(foundX);
                y.add(foundY);
            }
        }
        if (x.size() > 0) {
            ItemVariable varX = reportResultProviderService.getCcmVariable(hKey);
            ItemVariable varY = reportResultProviderService.getCcmVariable(key.getRunVariableKey());
            final LoiConstanteCourbeModel model = LoiConstanteCourbeModel.create(key, x, y, varX, varY);
            model.setTitle(key.getDisplayName("/" + StringUtils.capitalize(config.getHorizontalVar())));
            return model;
        }
        return null;
    }
}
