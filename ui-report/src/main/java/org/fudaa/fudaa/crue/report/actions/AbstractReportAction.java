/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.report.actions;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractPerspectiveAwareAction;
import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.fudaa.fudaa.crue.report.service.ReportService;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;

public abstract class AbstractReportAction extends AbstractPerspectiveAwareAction {

  protected final ReportService postService = Lookup.getDefault().lookup(ReportService.class);
  private final Result<EMHScenario> resultat;
  final SelectedPerspectiveService selectedPerspectiveService = Lookup.getDefault().lookup(SelectedPerspectiveService.class);

  public AbstractReportAction(boolean enableInEditMode) {
    super(PerspectiveEnum.REPORT, enableInEditMode);
    resultat = postService.getLookup().lookupResult(EMHScenario.class);
    resultat.addLookupListener(this);
    resultChanged(null);
  }

  @Override
  protected boolean getEnableState() {
    return super.getEnableState();
  }
}
