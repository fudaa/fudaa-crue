/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.node;

import javax.swing.Action;
import org.fudaa.dodico.crue.metier.emh.CatEMHSection;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.fudaa.crue.emh.node.NodeEMHDefault;
import org.fudaa.fudaa.crue.report.actions.ReportOpenMultiVarViewNodeAction;
import org.fudaa.fudaa.crue.report.actions.ReportOpenProfilTransversalViewNodeAction;
import org.fudaa.fudaa.crue.report.actions.ReportOpenRPTGViewNodeAction;
import org.fudaa.fudaa.crue.report.actions.ReportOpenTemporalViewNodeAction;
import org.fudaa.fudaa.crue.report.export.ReportExportNodeAction;
import org.openide.nodes.Children;
import org.openide.util.actions.SystemAction;

/**
 *
 * @author Frederic Deniger
 */
public class ReportNodeCatEMHSection extends NodeEMHDefault {

  public ReportNodeCatEMHSection(Children children, CatEMHSection emh, String displayName) {
    super(children, emh, displayName);
  }

  public ReportNodeCatEMHSection(Children children, EMH emh) {
    super(children, emh);
  }

  @Override
  public Action[] getActions(boolean context) {
    return new Action[]{SystemAction.get(ReportExportNodeAction.class),
              null,
              SystemAction.get(ReportOpenTemporalViewNodeAction.class),
              SystemAction.get(ReportOpenMultiVarViewNodeAction.class),
              SystemAction.get(ReportOpenProfilTransversalViewNodeAction.class),
              null,
              SystemAction.get(ReportOpenRPTGViewNodeAction.class)
            };
  }
}
