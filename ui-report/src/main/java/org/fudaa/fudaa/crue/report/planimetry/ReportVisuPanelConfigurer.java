/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.planimetry;

import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JToolBar;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.fudaa.crue.planimetry.ConfigureUIAction;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanel;
import org.fudaa.fudaa.crue.planimetry.PlanimetryVisuPanelConfigurer;
import org.fudaa.fudaa.crue.report.actions.ReportPlanimetryExportAction;

/**
 * Permet de configurer l'affichage de la vue planimétrique pour les rapports. De plus permet d'ajouter les actions spécifiques sur les calques en
 * question.
 *
 * @author Frederic Deniger
 */
public class ReportVisuPanelConfigurer extends PlanimetryVisuPanelConfigurer {

  @Override
  public void addApplicationActions(PlanimetryVisuPanel visuPanel, List<EbliActionInterface> arrayList) {
    arrayList.add(null);
    arrayList.add(new ConfigureUIAction(visuPanel));
  }

  public void addActions(PlanimetryVisuPanel visuPanel) {
  }

  @Override
  public void addSpecificButtonsInToolbar(final PlanimetryVisuPanel panel, JToolBar componentBar) {
   

    ReportPlanimetryExportAction exportAction = new ReportPlanimetryExportAction(panel.getPlanimetryController());
    AbstractButton exportButton = exportAction.buildToolButton(EbliComponentFactory.INSTANCE);

    ReportPlanimetryConfigureAction configureAction = new ReportPlanimetryConfigureAction(panel.getPlanimetryController());
    AbstractButton configureButton = configureAction.buildToolButton(EbliComponentFactory.INSTANCE);
    panel.getPlanimetryVisuController().addEditionComponent(exportButton);
    panel.getPlanimetryVisuController().addEditionComponent(configureButton);
    componentBar.add(exportButton);
    componentBar.add(configureButton);
    ReportBrancheSizeConfigurer brancheSize = new ReportBrancheSizeConfigurer();
    final JComponent spinner = brancheSize.createConfigurer(panel);
    panel.getPlanimetryVisuController().addEditionComponent(spinner);
    componentBar.add(spinner);


  }

 
}
