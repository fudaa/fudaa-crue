/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.formule;

import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.openide.nodes.Node;

/**
 *
 * @author Frederic Deniger
 */
public class FormuleNodeActionAddExpr extends AbstractEditNodeAction {

  public FormuleNodeActionAddExpr() {
    super(org.openide.util.NbBundle.getMessage(FormuleNodeActionAddExpr.class, "formule.addExpr"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return activatedNodes.length == 1;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    final Node parentNode = activatedNodes[0].getParentNode();
    FormulePanelEditExpr editor = new FormulePanelEditExpr(parentNode==null?activatedNodes[0]:parentNode);
    editor.show();
  }
}
