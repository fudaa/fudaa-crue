/*
 GPL 2
 */
package org.fudaa.fudaa.crue.report.actions;

import java.awt.event.ActionEvent;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.report.ReportTopComponentConfigurable;
import org.fudaa.fudaa.crue.report.perspective.PerspectiveServiceReport;
import org.fudaa.fudaa.crue.report.service.ReportTemplateFileProvider;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class SaveTemplateAction extends EbliActionSimple {

  private final ReportTopComponentConfigurable topComponent;
  PerspectiveServiceReport perspectiveServiceReport = Lookup.getDefault().lookup(PerspectiveServiceReport.class);

  public SaveTemplateAction(final ReportTopComponentConfigurable topComponent) {
    super(NbBundle.getMessage(SaveTemplateAction.class, "saveTemplateAction"), null, "SAVE_TEMPLATE");
    setDefaultToolTip(NbBundle.getMessage(SaveTemplateAction.class, "saveTemplateAction.tooltip"));
    this.topComponent = topComponent;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    new ReportTemplateFileProvider().saveTemplate(topComponent.getReportConfig());
  }
}
