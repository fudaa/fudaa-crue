package org.fudaa.fudaa.crue.study.node;

import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

/**
 *
 * @author deniger
 */
public class ManagerEMHScenarioChildren extends Children.Array {

  private final Node runNode;

  public ManagerEMHScenarioChildren(Node runNode) {
    this.runNode = runNode;
  }

  public EMHRunNode findNode(EMHRun run) {
    if (runNode == null) {
      return null;
    }
    return (EMHRunNode) runNode.getChildren().findChild(run.getId());
  }
}
