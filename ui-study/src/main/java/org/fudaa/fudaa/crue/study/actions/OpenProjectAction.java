/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import javax.swing.Action;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
id = "org.fudaa.fudaa.crue.study.actions.OpenProjectAction")
@ActionRegistration(displayName = "#LoadStudy.Name")
@ActionReferences({
  @ActionReference(path = "Actions/Study", position = 3)
})
public final class OpenProjectAction extends AbstractOpenProjectAction {


  public OpenProjectAction() {
    super(false);
    putValue(Action.NAME, NbBundle.getMessage(OpenProjectAction.class, "LoadStudy.Name"));
  }


  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new OpenProjectAction();
  }
}
