/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.node;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.study.actions.*;
import org.fudaa.fudaa.crue.study.perspective.PerspectiveServiceStudy;
import org.fudaa.fudaa.crue.study.property.ManagerPropertyFactory;
import org.fudaa.fudaa.crue.study.property.PropertyScenarioCrue9Linked;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.PasteType;

import javax.swing.*;
import java.awt.datatransfer.Transferable;
import java.util.List;

/**
 * @author Fred Deniger
 */
public class ManagerEMHScenarioNode extends AbstractManagerNode<ManagerEMHScenario> {
  private final NodesManager nodesManager;
  private final PerspectiveServiceStudy perspectiveService = Lookup.getDefault().lookup(PerspectiveServiceStudy.class);
  private boolean courant;
  private boolean loaded;

  public ManagerEMHScenarioNode(Children children, ManagerEMHScenario scenario, NodesManager nodesManager) {
    super(children, scenario, nodesManager.getProjet());
    setName(scenario.getNom());
    this.nodesManager = nodesManager;
    updateIconBaseScenario(scenario);
  }

  /**
   * Gestion de l'icône des scénarios en fonction de la présence ou non de Run(s)
   *
   * @param scenario
   */
  private void updateIconBaseScenario(ManagerEMHScenario scenario) {
    if (scenario.getListeRuns().isEmpty()) {
      setIconBaseWithExtension(CrueIconsProvider.getIconBase(EnumCatEMH.SCENARIO));
    } else {
      setIconBaseWithExtension(CrueIconsProvider.getCompleteName("scenarioRun.png"));
    }
  }

  public ManagerEMHScenario getManagerEMHScenario() {
    return getLookup().lookup(ManagerEMHScenario.class);
  }

  public EMHRunNode getRun(EMHRun run) {
    ManagerEMHScenarioChildren children = (ManagerEMHScenarioChildren) getChildren();
    return children.findNode(run);
  }

  public void setScenarioCourant(boolean courant) {
    this.courant = courant;
    ManagerEMHScenario scenario = getManagerEMHScenario();
    if (courant) {
      setIconBaseWithExtension(CrueIconsProvider.getIconBaseScenarioCourant());
      setShortDescription(org.openide.util.NbBundle.getMessage(ManagerEMHScenarioNode.class, "ScenarioIsCurrentName.Tooltip",
          scenario.getNom()));
    } else {
      updateIconBaseScenario(scenario);
      setShortDescription(scenario.getNom());
    }
  }

  @Override
  protected void fireObjectChange(String property, Object oldValue, Object newValue) {
    if (PropertyScenarioCrue9Linked.LINKED_SCENARIO_CRUE9.equals(property)) {
      new UsedByScenarioCrue10Builder(nodesManager).updateAll();
    }
    super.fireObjectChange(property, oldValue, newValue);
  }

  @Override
  public final boolean canDestroy() {
    ManagerEMHScenario thisScenario = getManagerEMHScenario();
    if (thisScenario.isCrue9() && getProjet().getCoeurConfig().isCrue9Dependant()) {
      List<ManagerEMHScenario> listeScenarios = getProjet().getListeScenarios();
      for (ManagerEMHScenario scenario : listeScenarios) {
        if (scenario.isCrue10() && StringUtils.equals(thisScenario.getNom(), scenario.getLinkedscenarioCrue9())) {
          return false;
        }
      }
    }
    return true;
  }

  @Override
  public String getHtmlDisplayName() {
    return loaded ? "<b>" + getDisplayName() + "</b>" : null;
  }

  public void setScenarioLoaded(boolean loaded) {
    if (this.loaded == loaded) {
      return;
    }
    this.loaded = loaded;
    if (loaded) {
      setShortDescription(org.openide.util.NbBundle.getMessage(ManagerEMHScenarioNode.class, "ScenarioIsLoadedName.Tooltip",
          getManagerEMHScenario().getNom()));
    } else {
      setScenarioCourant(courant);
    }
    fireDisplayNameChange(null, getDisplayName());
  }

  @Override
  public PasteType getDropType(Transferable t, int action, int index) {
    if (perspectiveService.isInEditMode()) {
      final Node dragged = DefaultNodePasteType.getMovedNodeInTransferable(t);
      if (dragged instanceof ManagerEMHModeleNode) {
        final ManagerEMHModeleBase draggedManager = ((ManagerEMHModeleNode) dragged).getManager();
        CrueVersionType modeleVersion = draggedManager.getInfosVersions().getCrueVersion();
        CrueVersionType scenarioVersion = this.getManager().getInfosVersions().getCrueVersion();

        if ((scenarioVersion == modeleVersion) && (getManager().getManagerFils(draggedManager.getId()) == null)) {
          return new ManagerEMHContainerNodePastType(this, dragged);
        }
      } else if (dragged instanceof FichierCrueNode) {
        CrueVersionType scenarioVersion = this.getManager().getInfosVersions().getCrueVersion();
        FichierCrue file = ((FichierCrueNode) dragged).getFichierCrue();

        if ((scenarioVersion == CrueVersionType.CRUE10) && file.getType().isScenarioLevel() && !getManager().getListeFichiers().getFichiers().contains(
            file)) {
          return new ManagerEMHContainerNodePastType(this, dragged);
        }
      }
    }

    return this.getParentNode().getDropType(t, index, index);
  }

  @Override
  public SystemAction getDefaultAction() {
    return SystemAction.get(OpenScenarioNodeAction.class);
  }

  @Override
  public Action[] getActions(boolean context) {
    return new Action[]{
        SystemAction.get(OpenScenarioNodeAction.class),
        SystemAction.get(CompareActionNode.class),
        SystemAction.get(UnloadScenarioAction.class),
        null,
        SystemAction.get(ModifyScenarioNodeAction.class),
        SystemAction.get(RenameManagerNodeAction.class),
        SystemAction.get(CopyManagerNodeAction.class),
        null,
        SystemAction.get(DeleteManagerNodeAction.class),
        null,
        SystemAction.get(OpenRunCourantNodeAction.class),
        SystemAction.get(LaunchNewRunNodeAction.class),
        SystemAction.get(LaunchNewRunNodeAllOptionsAction.class),
        SystemAction.get(LaunchNewRunNodeOptionAction.class),
        null,
        SystemAction.get(DeleteAllRunsNodeAction.class),
        null,
        SystemAction.get(ConvertScenarioCrue9NodeAction.class),
        SystemAction.get(ConvertScenarioCrue10NodeAction.class),
        null,
        SystemAction.get(ShowUsedByNodeAction.class)};
  }

  @Override
  protected Sheet createSheet() {
    return ManagerPropertyFactory.createSheet(this, getProjet());
  }

  @Override
  public boolean canRename() {
    return super.canRename() && this.getManager().getListeRuns().isEmpty();
  }
}
