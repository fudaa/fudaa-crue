package org.fudaa.fudaa.crue.study.dialog;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.openide.util.ImageUtilities;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * Validateur de radical.
 *
 * @author deniger
 */
public class RadicalValidationInstaller implements DocumentListener {
  private boolean attached = true;
  private final JLabel infoMessage;
  private Runnable runAfterValidation;
  private final String specifMaxTailleMessage;
  private final int specificMaxTaille;
  private final JTextField txtField;
  private boolean valid = false;

  public RadicalValidationInstaller(JTextField txtField, JLabel infoMessage) {
    this(txtField, infoMessage, -1, null);
  }

  /**
   * @param txtField le jtext field contenant le contenu à valider
   * @param infoMessage le label contenant les messages de valdation
   * @param maxTaille la taille max attendue pour le nom
   * @param maxTailleErrorMessage le message à afficher si dépassé.
   */
  public RadicalValidationInstaller(JTextField txtField, JLabel infoMessage, int maxTaille, String maxTailleErrorMessage) {
    this.txtField = txtField;
    this.infoMessage = infoMessage;
    this.specificMaxTaille = maxTaille;
    this.specifMaxTailleMessage = maxTailleErrorMessage;
    //valeur par défaut qui ne devrait pas arriver
    if (maxTaille > 0) {
      assert maxTailleErrorMessage != null;
    }
    if (maxTailleErrorMessage != null) {
      txtField.setToolTipText(BusinessMessages.getString(maxTailleErrorMessage));
    }
    txtField.getDocument().addDocumentListener(this);
    updateValidState();
  }

  public void setRunAfterValidation(Runnable runAfterValidation) {
    this.runAfterValidation = runAfterValidation;
  }

  public boolean isValid() {
    return valid;
  }

  public void updateValidState() {
    final String nameToValid = txtField.getText().trim();
    String radicalValideMsg = ValidationPatternHelper.isRadicalValideForEMHContainerMsg(nameToValid, specificMaxTaille, specifMaxTailleMessage);
    valid = (radicalValideMsg == null);
    int maxChar = ValidationPatternHelper.getRadicalMaxLength(specificMaxTaille);
    int currentSize = nameToValid.length();
    final String nbCharState = currentSize + "/" + maxChar;
    final String remaining = Integer.toString(maxChar - currentSize);
    if (valid) {
      infoMessage.setText(RadicalInputNotifier.getRadicalInfoMessageScenario(nbCharState, remaining));
      infoMessage.setIcon(ImageUtilities.loadImageIcon("org/netbeans/modules/dialogs/info.png", false));
    } else {
      infoMessage.setText(RadicalInputNotifier.getRadicalInfoMessageHtmlError(BusinessMessages.getString(radicalValideMsg), nbCharState, remaining));
      setErrorIcon();
    }
    if (runAfterValidation != null) {
      runAfterValidation.run();
    }
  }

  public void setErrorIcon() {
    infoMessage.setIcon(ImageUtilities.loadImageIcon("org/netbeans/modules/dialogs/error.gif", false));
  }

  public void setText(String text) {
    infoMessage.setText(text);
  }

  public void attach(boolean enabled) {
    attached = enabled;
  }

  @Override
  public void changedUpdate(DocumentEvent e) {
    if (attached) {
      updateValidState();
    }
  }

  @Override
  public void insertUpdate(DocumentEvent e) {
    if (attached) {
      updateValidState();
    }
  }

  @Override
  public void removeUpdate(DocumentEvent e) {
    if (attached) {
      updateValidState();
    }
  }
}
