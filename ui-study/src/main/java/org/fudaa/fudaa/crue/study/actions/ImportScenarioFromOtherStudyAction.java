package org.fudaa.fudaa.crue.study.actions;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.metier.etude.*;
import org.fudaa.dodico.crue.projet.copy.CopyInformations;
import org.fudaa.dodico.crue.projet.rename.RenameManager;
import org.fudaa.dodico.crue.projet.select.SelectableFinder;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.loader.LoaderService;
import org.fudaa.fudaa.crue.loader.ProjectLoadContainer;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.fudaa.fudaa.crue.study.dialog.RadicalInputNotifier;
import org.fudaa.fudaa.crue.study.services.CrueService;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Action d'importer d'un scénario depuis une autre étude
 *
 * @author Yoan GRESSIER
 */
@ActionID(category = "View", id = "org.fudaa.fudaa.crue.study.actions.ImportScenarioFromOtherStudyAction")
@ActionRegistration(displayName = "#MigrateProjectAction.Name")
@ActionReferences({
    @ActionReference(path = "Actions/Study", position = 8, separatorAfter = 9)})
public final class ImportScenarioFromOtherStudyAction extends AbstractStudyAction {
  protected final transient EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  private final transient LoaderService loaderService = Lookup.getDefault().lookup(LoaderService.class);
  private final transient CrueService crueService = Lookup.getDefault().lookup(CrueService.class);
  private final transient ConnexionInformation connexionInfos = Lookup.getDefault().lookup(InstallationService.class).getConnexionInformation();
  /**
   * Combobox permettant de lister les scénarios de l'étude sélectionnée
   */
  private javax.swing.JComboBox<String> cbbScenarios;
  /**
   * Champs texte affichant le commentaire du scénario sélectionné
   */
  private javax.swing.JTextArea commentTextArea;

  /**
   * Constructeur
   */
  public ImportScenarioFromOtherStudyAction() {
    super(true);
    putValue(Action.NAME, NbBundle.getMessage(ImportScenarioFromOtherStudyAction.class, "ImportScenarioFromOtherStudyAction.Name"));
  }

  @Override
  public Action createContextAwareInstance(final Lookup actionContext) {
    return new ImportScenarioFromOtherStudyAction();
  }

  @Override
  public void actionPerformed(final ActionEvent e) {
    // pas d'importer si un scénario ou run est chargé
    if (crueService.isScenarioOrRunLoaded()) {
      DialogHelper.showError(getActionTitle(), NbBundle.getMessage(ImportScenarioFromOtherStudyAction.class, "RunOrScenarioLoaded.Error"));
      return;
    }

    // sélection du fichier d'étude
    final File etuFileOpened = loaderService.chooseEtuFile();

    // chargement de l'étude sélectionnée
    final ProjectLoadContainer resultat = loaderService.loadProject(etuFileOpened);

    if (resultat == null) {
      // problème de chargement de l'étude (fichier invalide ou autre)
      return;
    }

    // pointeur sur l'étude chargée
    final EMHProjet sourceEtude = resultat.getProjet();

    // choix du scénario et modification éventuelle du commentaire
    final JPanel pnlChooseScenario = createChooseScenarioPanel(sourceEtude);
    final boolean isOkClicked = DialogHelper
        .showQuestionAndSaveDialogConf(NbBundle.getMessage(getClass(), "ImportScenarioFromOtherStudyAction.ChooseScenarioDialog.title"),
            pnlChooseScenario, "1.0");

    if (isOkClicked) {
      ManagerEMHScenario importScenario = null;

      if (cbbScenarios.getSelectedItem() != null) {
        // lecture du scénario choisi
        importScenario = sourceEtude.getScenario(cbbScenarios.getSelectedItem().toString());
        if (sourceEtude.getScenario(cbbScenarios.getSelectedItem().toString()) != null
            && sourceEtude.getScenario(cbbScenarios.getSelectedItem().toString()).getInfosVersions() != null) {
          // MAJ du commentaire
          importScenario.getInfosVersions().setCommentaire(this.commentTextArea.getText());
        }
      }

      if (importScenario == null) {
        // problème de sélection du scénario
        return;
      }

      // étude courante où sera importé le scénario
      final EMHProjet currentEtude = projetService.getSelectedProject();

      // Gestionnaire pour le renommage éventuel
      final RenameManager manager = new RenameManager();
      manager.setProject(currentEtude);

      // choix du nom du scénario d'importer
      final RenameManager.NewNameInformations newNameInfos = this.chooseNewNames(importScenario, manager, true);

      if ((newNameInfos == null) || !this.testNewNameInformations(manager, newNameInfos)) {
        return;
      }

      // construction des informations de noms
      CopyInformations copyInfos = new CopyInformations(importScenario, newNameInfos, true, sourceEtude, currentEtude);
      String name = importScenario.getNom();

      // vérif et information utilisateur si des fichiers seront écrasés
      final List<String> overwrittenFiles = copyInfos.getOverwrittenFiles();
      boolean overwrite = false;
      if (!overwrittenFiles.isEmpty()) {
        final OverwriteFileHelper.OverwriteResult confirmOverwriteFiles = OverwriteFileHelper.confirmOverwriteFiles(overwrittenFiles);
        if (OverwriteFileHelper.OverwriteResult.CANCEL.equals(confirmOverwriteFiles)) {
          return;
        }
        overwrite = OverwriteFileHelper.OverwriteResult.DO_OVERWRITE.equals(confirmOverwriteFiles);
      }
      // lance l'opération d'importer
      final Callable<Boolean> copyCallable = new CopyCallable(copyInfos, overwrite);
      projetService.deepModification(copyCallable, NbBundle.getMessage(RenameManagerNodeAction.class, "Log.CopyAction", name));
    }
  }

  /**
   * Création d'un panel de choix du scénario à importer dans l'étude en paramètre.
   * Ce panel est utiliser dans DialogHelper.showQuestionAndSaveDialogConf
   *
   * @param selectedEtude l'étude dans laquelle on choisit le scénario à importer
   * @return le panel construit
   */
  private JPanel createChooseScenarioPanel(final EMHProjet selectedEtude) {
    commentTextArea = new JTextArea();
    commentTextArea.setRows(5);
    commentTextArea.setLineWrap(true);

    cbbScenarios = new JComboBox<>();

    final JLabel scenarioLabel = new JLabel();
    scenarioLabel.setText(NbBundle.getMessage(getClass(), "ImportScenarioFromOtherStudyAction.ChooseScenarioDialog.scenarioLabel.text"));

    final JLabel commentLabel = new JLabel();
    commentLabel.setText(NbBundle.getMessage(getClass(), "ImportScenarioFromOtherStudyAction.ChooseScenarioDialog.commentLabel.text"));

    final JPanel pnl = new JPanel();
    pnl.setLayout(new BorderLayout());
    pnl.setBorder(new EmptyBorder(10, 10, 10, 10));

    final JPanel pnlLeft = new JPanel(new BorderLayout());
    pnlLeft.add(scenarioLabel, BorderLayout.NORTH);
    pnlLeft.add(commentLabel, BorderLayout.CENTER);
    pnl.add(pnlLeft, BorderLayout.WEST);

    final JPanel pnlCenter = new JPanel(new BorderLayout());
    pnlCenter.add(cbbScenarios, BorderLayout.NORTH);
    pnlCenter.add(commentTextArea, BorderLayout.CENTER);
    pnl.add(pnlCenter, BorderLayout.CENTER);

    for (final ManagerEMHScenario sc : selectedEtude.getListeScenarios()) {
      cbbScenarios.addItem(sc.getNom());
    }

    this.commentTextArea.setText(selectedEtude.getScenario(cbbScenarios.getSelectedItem().toString()).getInfosVersions().getCommentaire());

    cbbScenarios.addActionListener(ae -> {
      // on fixe le commentaire associé
      if (cbbScenarios.getSelectedItem() != null
          && selectedEtude.getScenario(cbbScenarios.getSelectedItem().toString()) != null
          && selectedEtude.getScenario(cbbScenarios.getSelectedItem().toString()).getInfosVersions() != null) {
        commentTextArea.setText(selectedEtude.getScenario(cbbScenarios.getSelectedItem().toString()).getInfosVersions().getCommentaire());
      } else {
        commentTextArea.setText(StringUtils.EMPTY);
      }
    });

    return pnl;
  }

  /**
   * Choisir le nom de l'EMH importée en paramètre
   *
   * @param container l'EMH importée
   * @param manager le manager de création des nouveaux noms
   * @param deep true si le nouveau nom est appliqué en cascade, false sinon
   * @return l'objet contenant les nouvelles informations de nommage
   */
  private RenameManager.NewNameInformations chooseNewNames(final ManagerEMHContainerBase container, final RenameManager manager, final boolean deep) {
    final String init = StringUtils.removeStart(container.getNom(), container.getLevel().getPrefix());
    // choix du radical à utiliser pour copier l'EMH
    final String radical = chooseRenameRadical(init, container.getNom());
    if (radical == null) {
      return null;
    }

    final SelectableFinder finder = new SelectableFinder();
    finder.setProject(manager.getProject());

    return manager.getNewNames(container, finder.getItems(Collections.singletonList(container), deep), radical, deep);
  }

  /**
   * Ouvre une fenêtre de choix de radical pour l'EMH importée, avec contrôle des règles de nommage associé au type d'EMH
   *
   * @param init valeur initial du radical
   * @param containerName nom de l'EMH importée
   * @return le radical choisi/saisi par l'utilisateur, null si saisie annuler
   */
  private String chooseRenameRadical(final String init, final String containerName) {
    final RadicalInputNotifier input = new RadicalInputNotifier(
        NbBundle.getMessage(getClass(), "CopyAction.RadicalMessage"),
        NbBundle.getMessage(getClass(), "CopyAction.Title", containerName),
        init,
        true, -1, null);
    if (DialogDisplayer.getDefault().notify(input) == NotifyDescriptor.OK_OPTION) {
      return input.getInputText().trim();
    }
    return null;
  }

  /**
   * Vérification de la validité des nom de l'EMH importée
   *
   * @param manager le manager de renommage
   * @param infos les informations de renommage
   * @return true si aucun problème, false s'il existe des doublons de noms des EMH anciennes/nouvelles (unicité à chaque niveau)
   */
  private boolean testNewNameInformations(final RenameManager manager, final RenameManager.NewNameInformations infos) {
    final CtuluLog logs = manager.canUseNewNames(infos);

    if (logs.containsErrorOrSevereError()) {
      LogsDisplayer.displayError(logs, NbBundle.getMessage(getClass(), "CopyAction.Title"));
      return false;
    }

    return true;
  }

  /**
   * Classe délégate de copie
   */
  private class CopyCallable implements Callable<Boolean> {
    /**
     * Informations de copie
     */
    private final CopyInformations infos;
    /**
     * Faut-il écraser les fichiers
     */
    private final boolean overwrite;

    /**
     * Constructeur
     *
     * @param infos Informations de copie
     * @param overwrite Faut-il écraser les fichiers
     */
    CopyCallable(final CopyInformations infos, final boolean overwrite) {
      this.infos = infos;
      this.overwrite = overwrite;
    }

    @Override
    public Boolean call() throws Exception {
      if (this.infos.rootContainer != null) {
        return this.copy(this.infos.rootContainer, overwrite) != null;
      }
      if (this.infos.rootFile != null) {
        return this.copy(this.infos.rootFile, overwrite) != null;
      }
      return true;
    }

    /**
     * Méthode de copie récursive du container en paramètre
     *
     * @param container le conteneur à copier
     * @param overwriteFiles true s'il faut écraser les fichiers existants, false sinon
     * @return le manager de l'EMH copiée
     */
    private ManagerEMHContainerBase copy(final ManagerEMHContainerBase container, final boolean overwriteFiles) {
      final ManagerEMHContainerBase destContainer;

      if (this.infos.deepCopy) {
        // copie du conteneur
        destContainer = this.copyContainer(container, true);

        // si EMH conteneur, copie récursive des EMH enfants
        if (container instanceof ManagerEMHContainer<?>) {
          final List<ManagerEMHContainerBase> children = ((ManagerEMHContainer) container).getFils();

          for (final ManagerEMHContainerBase child : children) {
            final ManagerEMHContainerBase destChild = this.copy(child, overwriteFiles);
            if (destChild == null) {
              return null;
            }
            if (destContainer != null) {
              ((ManagerEMHContainer) destContainer).addManagerFils(destChild);
            }
          }
        }

        // copie physique des fichiers de l'EMH
        final FichierCrueManager files = container.getListeFichiers();

        if (files != null) {
          for (final FichierCrue file : files.getFichiers()) {
            final FichierCrue destFile = this.copy(file, overwriteFiles);

            if (destFile == null) {
              return null;
            }
            if (destContainer != null) {
              destContainer.addFichierDonnees(destFile);
            }
          }
        }

        if (destContainer != null) {
          this.addToProject(destContainer);
        }
      } else {
        destContainer = this.copyContainer(container, false);
      }

      if (destContainer != null) {
        this.addToProject(destContainer);
        //on copie ici les fichier de configuration de niveau sous-modele et/ou scenario
        final File initConfig = infos.sourceProject.getInfos().getDirOfConfig(container);
        if (CtuluLibFile.exists(initConfig)) {
          final File destConfig = infos.destProject.getInfos().getDirOfConfig(destContainer);
          CrueFileHelper.copyDirWithSameLastDate(initConfig, destConfig);
        }
      }

      return destContainer;
    }

    /**
     * Ajout de l'EMH en paramètre à l'étude courante
     *
     * @param container le conteneur à ajouter, pouvant être de type scénario, modèle, sous-modèle
     */
    private void addToProject(final ManagerEMHContainerBase container) {
      if (container instanceof ManagerEMHScenario) {
        this.infos.destProject.addScenario((ManagerEMHScenario) container);
      } else if (container instanceof ManagerEMHModeleBase) {
        this.infos.destProject.addBaseModele((ManagerEMHModeleBase) container);
      } else if (container instanceof ManagerEMHSousModele) {
        this.infos.destProject.addBaseSousModele((ManagerEMHSousModele) container);
      }
    }

    /**
     * MAJ du conteneur en paramètre avec les infos contenu dans this.infos
     *
     * @param srcContainer le conteneur à copier
     * @param copyEmpty true pour copier les vides
     * @return le conteneur mis à jour
     */
    private ManagerEMHContainerBase copyContainer(final ManagerEMHContainerBase srcContainer, final boolean copyEmpty) {
      ManagerEMHContainerBase destContainer = null;

      if (srcContainer instanceof ManagerEMHScenario) {
        final ManagerEMHScenario destScenario = new ManagerEMHScenario(this.infos.containersToCopy.get(srcContainer));
        if (srcContainer.isCrue10() && infos.sourceProject.getCoeurConfig().isCrue9Dependant()) {
          destScenario.setLinkedscenarioCrue9(((ManagerEMHScenario) srcContainer).getLinkedscenarioCrue9());
        }

        if (!copyEmpty) {
          final List<ManagerEMHModeleBase> children = ((ManagerEMHScenario) srcContainer).getFils();

          for (final ManagerEMHModeleBase child : children) {
            destScenario.addManagerFils(child);
          }
        }

        destContainer = destScenario;
      } else if (srcContainer instanceof ManagerEMHModeleBase) {
        final ManagerEMHModeleBase destModele = new ManagerEMHModeleBase(this.infos.containersToCopy.get(srcContainer));

        if (!copyEmpty) {
          final List<ManagerEMHSousModele> children = ((ManagerEMHModeleBase) srcContainer).getFils();

          for (final ManagerEMHSousModele child : children) {
            destModele.addManagerFils(child);
          }
        }

        destContainer = destModele;
      } else if (srcContainer instanceof ManagerEMHSousModele) {
        final ManagerEMHSousModele destSousModele = new ManagerEMHSousModele(this.infos.containersToCopy.get(srcContainer));

        destContainer = destSousModele;
      }

      if (!copyEmpty) {
        final FichierCrueManager files = srcContainer.getListeFichiers();
        if (files != null && destContainer != null) {
          destContainer.setListeFichiers(files.getFichiers());
        }
      }

      final EMHInfosVersion infosVersion = new EMHInfosVersion();

      infosVersion.setVersion(srcContainer.getInfosVersions().getCrueVersion());
      infosVersion.setAuteurCreation(connexionInfos.getCurrentUser());
      infosVersion.setDateCreation(connexionInfos.getCurrentDate());
      infosVersion.setAuteurDerniereModif(infosVersion.getAuteurCreation());
      infosVersion.setDateDerniereModif(infosVersion.getDateCreation());
      infosVersion.setCommentaire(srcContainer.getInfosVersions().getCommentaire());

      if (destContainer != null) {
        destContainer.setInfosVersions(infosVersion);
      }

      return destContainer;
    }

    /**
     * Copie physique d'un fichier
     *
     * @param file le fichier à copier
     * @param overwriteFile true si le fichier existant est écrasé, false sinon
     * @return le fichier créé
     */
    private FichierCrue copy(final FichierCrue file, final boolean overwriteFile) {
      return EMHProjet.copyFile(file, this.infos.filesToCopy.get(file), overwriteFile, this.infos.sourceProject, this.infos.destProject);
    }
  }
}
