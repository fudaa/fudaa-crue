package org.fudaa.fudaa.crue.study.perspective;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.AbstractPerspectiveService;
import org.fudaa.fudaa.crue.common.services.PerspectiveService;
import org.fudaa.fudaa.crue.common.services.PerspectiveState;
import org.fudaa.fudaa.crue.study.*;
import org.fudaa.fudaa.crue.study.services.CrueService;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Le service perspective de Etude.
 * Note: dans cette perspective toutes les actions sont automatiquement persistées
 *
 * @author Fred Deniger
 */
@ServiceProviders(value = {
        @ServiceProvider(service = PerspectiveServiceStudy.class)
        ,
        @ServiceProvider(service = PerspectiveService.class)})
public class PerspectiveServiceStudy extends AbstractPerspectiveService {
    private final Set<String> components = Collections.unmodifiableSet(
            new LinkedHashSet<>(Arrays.asList(ScenariosTopComponent.TOPCOMPONENT_ID,
                    FileListTopComponent.TOPCOMPONENT_ID,
                    StudyPropertiesTopComponent.TOPCOMPONENT_ID,
                    ModeleListTopComponent.TOPCOMPONENT_ID,
                    SousModeleListTopComponent.TOPCOMPONENT_ID)));
    final CrueService crueService = Lookup.getDefault().lookup(CrueService.class);
    boolean active;

    /**
     * Ajouter les listener sur le service {@link CrueService} pour réagir aux chargement de projet et scenario.
     */
    public PerspectiveServiceStudy() {
        super(PerspectiveEnum.STUDY);
        crueService.addModellingEMHScenarioLookupListener(ev -> modellingScenarioLoadedChange());
        crueService.addEMHProjetStateLookupListener(ev -> projectLoadedChange());
    }

    @Override
    public String getPathForViewsAction() {
        return "Menu/Window/Study";
    }

    /**
     * Appele par le listener sur le (dé)chargement de projet. Utilise pour mettre à jour les etat read/edit.
     */
    protected void projectLoadedChange() {
        //projet en cours de chargement: on ne fait rien. Un autre evenement viendra à la fin.
        if (crueService.isProjetCurrentlyReloading()) {
            return;
        }
        //projet ferme: on passe en mode read only temporaire
        if (crueService.getProjectLoaded() == null) {
            resetStateToReadOnlyTemp();
        } else {
            //projet ouvert: on prend en compte le cas read only du projet:
            setState(crueService.isProjetReadOnly() ? PerspectiveState.MODE_READ_ONLY_ALWAYS : PerspectiveState.MODE_READ);
        }
    }

    /**
     * Appele par le listener sur le chargement de scenario et utilise pour changer l état de la perspective via {@link
     * #setState(org.fudaa.fudaa.crue.common.services.PerspectiveState) }.
     * Si un scenario est chargé, la perspective doit passer en mode read_only temporaire: etude non modifiable si scenario chargé
     */
    protected void modellingScenarioLoadedChange() {
        //actions menées dans d'autres perspective.
        if (!active) {
            return;
        }
        //Pour info: la methode setState gere le cas projet chargé en mode READ_ONLY.
        //un scenario est chargé: pas de modification
        if (crueService.isAScenarioLoadedInModellingPerspective()|| crueService.getProjectLoaded()==null) {
            setState(PerspectiveState.MODE_READ_ONLY_TEMP);
        } else {
            setState(PerspectiveState.MODE_READ);
        }
    }

    /**
     * @return true
     */
    @Override
    public boolean supportEdition() {
        return true;
    }

    /**
     * @return false si une campagne AOC est chargée.
     */
    @Override
    protected boolean canStateBeModifiedTo(PerspectiveState newState) {
        if (PerspectiveState.MODE_EDIT.equals(newState) ) {
            if(crueService.isAocCampagneLoaded()) {
                DialogHelper.showWarn(NbBundle.getMessage(getClass(), "Study.EditonImpossibleAocOpened"));
                return false;
            }
            if(crueService.isLhptLoaded()) {
                DialogHelper.showWarn(NbBundle.getMessage(getClass(), "Study.EditonImpossibleLhptOpened"));
                return false;
            }
        }
        return true;
    }

    /**
     * Recharge le projet si nécessaire: par exemple un run a été créé dans la perspective modélisation.
     *
     * @return true
     */
    @Override
    public boolean activate() {
        active = true;
        //on vérifie si le fichier etu doit être rechargé
        crueService.getProjetService().refreshEtuFileIfNeeded(null);
        //pour s'assurer de l'état readonly.
        modellingScenarioLoadedChange();
        return true;
    }

    /**
     * @return true si peut être desactivé. Pour cette perspective sera true car toutes les éditions sont automatiquement sauvegardées.
     */
    @Override
    public boolean deactivate() {
        boolean res = super.deactivate();
        if (res) {
            active = false;
        }
        return res;
    }

    /**
     * Ferme le projet en cours et renvoie true.
     *
     * @return true.
     */
    @Override
    public boolean closing() {
        if (crueService.getProjetService().isProjectOpened()) {
            crueService.getProjetService().close();
        }
        return true;
    }

    @Override
    public Set<String> getDefaultTopComponents() {
        return components;
    }
}
