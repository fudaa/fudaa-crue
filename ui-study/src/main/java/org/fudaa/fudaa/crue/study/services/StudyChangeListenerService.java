package org.fudaa.fudaa.crue.study.services;

import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.fudaa.fudaa.crue.study.node.ModifiableNodeContrat;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.ServiceProvider;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Un listener utilisé dans les nodes de la vue Etude pour sauvegarder automatiquement les modifications effectués sur les données.
 *
 * @author deniger
 */
@ServiceProvider(service = StudyChangeListenerService.class)
public class StudyChangeListenerService implements PropertyChangeListener {
    private final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
    private final InstallationService installationService = Lookup.getDefault().lookup(
            InstallationService.class);
    private boolean isUpdating;

    /**
     * Logue et sauvegarde automatiquement les modifications effectuées.
     *
     * @param changeEvent utiliser pour recuperer la source
     */
    @Override
    public void propertyChange(PropertyChangeEvent changeEvent) {
        //donnée deja en cours de mise à jour -> on ignore
        if (isUpdating) {
            return;
        }
        isUpdating = true;
        try {
            //le noeud source de la modifications
            ModifiableNodeContrat node = (ModifiableNodeContrat) changeEvent.getSource();
            projetService
                    .log(NbBundle.getMessage(StudyChangeListenerService.class, "LogPropertyChanged", ((Node) node).getDisplayName(), changeEvent.getPropertyName()));
            //on met à jour la date/user de dernière modification
            final ConnexionInformation connexionInformation = installationService.getConnexionInformation();
            node.updateInfos(connexionInformation);
            //sauvegarde rapide
            projetService.quickSave();
        } finally {
            //fin
            isUpdating = false;
        }
    }
}
