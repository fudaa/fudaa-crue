package org.fudaa.fudaa.crue.study.node;

import org.apache.commons.collections4.CollectionUtils;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

import java.io.File;
import java.util.List;

/**
 * Used to create scenario node
 *
 * @author Fred Deniger
 */
public class FileChildFactory extends ChildFactory<File> {
  private final File dir;

  public FileChildFactory(File dir) {
    this.dir = dir;
  }

  public static Children createChildren(File fileOrDir) {
    if (fileOrDir.isFile()) {
      return Children.LEAF;
    }
    return Children.create(new FileChildFactory(fileOrDir), true);
  }

  @Override
  protected boolean createKeys(List<File> list) {
    final File[] listFiles = dir.listFiles();
    if (listFiles != null && listFiles.length > 0) {
      CollectionUtils.addAll(list, listFiles);
    }
    return true;
  }

  @Override
  protected Node createNodeForKey(File file) {
    return new FileNode(file);
  }
}
