/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import javax.swing.JMenuItem;
import org.fudaa.fudaa.crue.common.action.OneSelectionNodeAction;
import org.fudaa.fudaa.crue.study.node.UsedByNodeBuilder;
import org.fudaa.fudaa.crue.study.property.PropertyEditorUsedBy;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

public class ShowUsedByNodeAction extends OneSelectionNodeAction {

  public ShowUsedByNodeAction() {
    super(NbBundle.getMessage(ShowUsedByNodeAction.class, "ShowUsedByNodeAction.Name"), false);
  }

  @Override
  protected boolean enable(Node activatedNodes) {
    return UsedByNodeBuilder.isUsedBy(activatedNodes);
  }
  

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected void performAction(Node activatedNodes) {
    PropertyEditorUsedBy.displayParent(activatedNodes);
  }

  @Override
  public JMenuItem getMenuPresenter() {
    return super.getMenuPresenter();
  }

  
}
