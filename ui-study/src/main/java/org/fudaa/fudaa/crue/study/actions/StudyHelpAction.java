package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractHelpAction;
import org.openide.awt.*;

@ActionID(category = "View",
        id = "org.fudaa.fudaa.crue.study.actions.StudyHelpAction")
//iconBase ne semble pas fonctionner !
@ActionRegistration(displayName = "#CTL_StudyHelpAction", iconBase = AbstractHelpAction.HELP_ICON)
@ActionReference(path = "Actions/Study", position = 100, separatorBefore = 99)
public final class StudyHelpAction extends AbstractHelpAction {

  public StudyHelpAction() {
    super(PerspectiveEnum.STUDY);
  }
}
