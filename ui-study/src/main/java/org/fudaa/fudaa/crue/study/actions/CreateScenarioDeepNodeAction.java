/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.metier.etude.FichierCrueParModele;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.UserPreferencesSaver;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.study.actions.OverwriteFileHelper.OverwriteResult;
import org.fudaa.fudaa.crue.study.dialog.CreateBlankScenarioDialog;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Chris
 */
public class CreateScenarioDeepNodeAction extends AbstractEditNodeAction {
  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public CreateScenarioDeepNodeAction() {
    super(NbBundle.getMessage(CreateScenarioDeepNodeAction.class, "CreateScenarioDeepAction.ActionName"));
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected boolean isEnableWithoutActivatedNodes() {
    return true;
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return true;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    CreateBlankScenarioDialog dialog = new CreateBlankScenarioDialog(WindowManager.getDefault().getMainWindow(), true);
    dialog.setName(dialog.getClass().getName());
    UserPreferencesSaver.loadDialogLocationAndDimension(dialog, "1.0");
    dialog.setVisible(true);
    UserPreferencesSaver.saveLocationAndDimension(dialog, "1.0");
    if (dialog.isOkClicked()) {
      ManagerEMHScenario scenario = dialog.getScenario();
      Collection<FichierCrueParModele> allFileUsed = scenario.getAllFileUsed().values();
      List<String> existingFiles = new ArrayList<>();
      for (FichierCrueParModele fichierCrueParModele : allFileUsed) {
        File projectFile = fichierCrueParModele.getFichier().getProjectFile(projetService.getSelectedProject());
        if (projectFile.exists()) {
          existingFiles.add(projectFile.getAbsolutePath());
        }
      }

      boolean overwrite = false;
      if (!existingFiles.isEmpty()) {
        OverwriteResult confirmOverwriteFiles = OverwriteFileHelper.confirmOverwriteFiles(existingFiles);
        if (OverwriteResult.CANCEL.equals(confirmOverwriteFiles)) {
          return;
        }
        overwrite = OverwriteResult.DO_OVERWRITE.equals(confirmOverwriteFiles);
      }

      projetService.deepModification(new CreateContainerDeepCallable(scenario, projetService.getSelectedProject(), overwrite),
          NbBundle.getMessage(getClass(), "CreateScenarioDeepAction.Log", scenario.getNom()));
    }
  }
}
