package org.fudaa.fudaa.crue.study.node;

import java.util.List;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;

/**
 * Used to create scenario node
 * @author Fred Deniger
 */
public class ManagerEMHModeleChildFactory extends ChildFactory<ManagerEMHModeleBase> {

  private final ManagerEMHScenario scenario;
  private final NodesManager nodeManager;

  public ManagerEMHModeleChildFactory(final ManagerEMHScenario scenario, final NodesManager nodeManager) {
    this.scenario = scenario;
    this.nodeManager = nodeManager;
  }

  @Override
  protected boolean createKeys(final List<ManagerEMHModeleBase> list) {
    list.addAll(scenario.getFils());
    return true;
  }

  @Override
  protected Node createNodeForKey(final ManagerEMHModeleBase c) {
    return new CrueFilterNode(nodeManager.getListingModeles().getNode(c));
  }
}
