package org.fudaa.fudaa.crue.study.property;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.openide.nodes.PropertySupport.ReadOnly;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class PropertyFichierCrueFile extends ReadOnly {

  private final FichierCrue fichierCrue;
  private final EMHProjet projet;

  public PropertyFichierCrueFile(final FichierCrue fichierCrue, EMHProjet projet) {
    super("AbsolutePath", String.class, NbBundle.getMessage(ManagerPropertyFactory.class, "File.AbsolutePath.Property"), org.openide.util.NbBundle.getMessage(ManagerPropertyFactory.class, "File.AbsolutePath.Property.Description"));
    this.fichierCrue = fichierCrue;
    this.projet = projet;
  }

  @Override
  public Object getValue() throws IllegalAccessException, InvocationTargetException {
    final File projectFile = fichierCrue.getProjectFile(projet);
    return projectFile==null?StringUtils.EMPTY:projectFile.getAbsolutePath();
  }
}
