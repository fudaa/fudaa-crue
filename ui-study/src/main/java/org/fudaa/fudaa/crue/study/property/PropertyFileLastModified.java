package org.fudaa.fudaa.crue.study.property;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.lang3.StringUtils;
import org.openide.nodes.PropertySupport.ReadOnly;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class PropertyFileLastModified extends ReadOnly {

  private final File file;

  public PropertyFileLastModified(final File file) {
    super("LastModified", String.class, NbBundle.getMessage(ManagerPropertyFactory.class, "File.LastModified.Property"), org.openide.util.NbBundle.getMessage(ManagerPropertyFactory.class, "File.LastModified.Property.Description"));
    this.file = file;
  }

  @Override
  public Object getValue() throws IllegalAccessException, InvocationTargetException {
    if (file == null) {
      return StringUtils.EMPTY;
    }
    return PropertyEditorLocalDateTime.SHORT_DATE_TIME.print(file.lastModified());
  }
}
