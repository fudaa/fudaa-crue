/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.node;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.fudaa.crue.study.services.StudyChangeListenerService;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 *
 * @author Fred Deniger
 */
public class ListingManagerEMHModele extends ChildrenKeysAbstract<ManagerEMHModeleBase> {

  final StudyChangeListenerService studyChangeListenerService = Lookup.getDefault().lookup(StudyChangeListenerService.class);
  private final NodesManager nodeManager;

  public ListingManagerEMHModele(NodesManager nodeManager) {
    this.nodeManager = nodeManager;
  }

  @Override
  protected Node createNode(String key) {
    final EMHProjet projet = nodeManager.getProjet();
    ManagerEMHModeleBase modele = projet.getModele(key);
    final ManagerEMHModeleNode managerEMHModeleNode = new ManagerEMHModeleNode(createChildren(modele), modele,
                                                                               projet);
    managerEMHModeleNode.addPropertyChangeListener(studyChangeListenerService);
    return managerEMHModeleNode;
  }

  @Override
  protected Collection getObjects() {
    return nodeManager.getProjet().getListeModeles();
  }

  public Children createChildren(ManagerEMHModeleBase modele) {
    List<Node> nodesToAdd = new ArrayList<>();
    if (!modele.isCrue9()) {
      AbstractNode sousModeles = new SousModelesManagerNode(Children.create(new ManagerEMHSousModeleChildFactory(modele,
                                                                                                                 nodeManager),
                                                                            false), Lookup.EMPTY);
      nodesToAdd.add(sousModeles);
    }
    AbstractNode fichier = new FichierCrueManagerNode(modele.getListeFichiers(), nodeManager);
    nodesToAdd.add(fichier);
    Children.Array res = new Children.Array();
    res.add(nodesToAdd.toArray(new Node[0]));
    return res;


  }
}
