package org.fudaa.fudaa.crue.study.node;

import java.io.IOException;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.metier.etude.EMHInfosVersion;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.study.perspective.PerspectiveServiceStudy;
import org.fudaa.fudaa.crue.study.property.ManagerPropertyFactory;
import org.joda.time.LocalDateTime;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public abstract class AbstractManagerNode<T extends ManagerEMHContainerBase> extends AbstractNodeFirable implements ModifiableNodeContrat {

  private final Class classManager;
  private final PerspectiveServiceStudy perspectiveServiceStudy = Lookup.getDefault().lookup(PerspectiveServiceStudy.class);
  private final EMHProjet projet;

  public AbstractManagerNode(Children children, T manager, EMHProjet projet) {
    super(children, Lookups.singleton(manager));
    classManager = manager.getClass();
    setValue("customDelete", Boolean.TRUE);
    this.projet = projet;
  }

  public EMHProjet getProjet() {
    return projet;
  }

  @Override
  public boolean isEditMode() {
    return perspectiveServiceStudy.isInEditMode();
  }

  public boolean isCrue9() {
    return getManager().isCrue9();
  }

  public boolean isCrue10() {
    return getManager().isCrue10();
  }

  public T getManager() {
    return (T) getLookup().lookup(classManager);
  }

  public Node getParentsTree() {
    return UsedByNodeBuilder.getParentsTree(this);
  }

  @Override
  public void destroy() throws IOException {
    //ne doit rien faire
  }

  @Override
  public boolean canDestroy() {
    return getParentsTree() == null;
  }

  @Override
  public final boolean canCopy() {
    return true;
  }

  public boolean isModifiable() {
    return canRename();
  }

  @Override
  public boolean canRename() {
    return UsedByNodeBuilder.nodeIsNotUsedByScenarioWithRun(this);
  }

  @Override
  public void updateInfos(ConnexionInformation connexionInformation) {
    final EMHInfosVersion infosVersions = getManager().getInfosVersions();
    String auteurDerniereModif = infosVersions.getAuteurDerniereModif();
    LocalDateTime dateDerniereModif = infosVersions.getDateDerniereModif();
    infosVersions.updateForm(connexionInformation);
    firePropertyChange(ManagerPropertyFactory.AUTEUR_MODIFICATION, auteurDerniereModif, infosVersions.getAuteurDerniereModif());
    firePropertyChange(ManagerPropertyFactory.DATE_MODIFICATION, dateDerniereModif, infosVersions.getDateDerniereModif());
  }
}
