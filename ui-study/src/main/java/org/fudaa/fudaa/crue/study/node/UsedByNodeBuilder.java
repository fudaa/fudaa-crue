package org.fudaa.fudaa.crue.study.node;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;
import org.openide.nodes.Children;
import org.openide.nodes.Children.Array;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;

/**
 *
 * @author deniger
 */
public class UsedByNodeBuilder {

  public static final String USED_BY = "USED_BY";
  private static final String USED_BY_SCENARIO_WITH_RUN = "USED_BY_SCENARIO_WITH_RUN";
  private final NodesManager nodesManager;

  public static boolean isUsedBy(Node node) {
    final Object value = node.getValue(UsedByNodeBuilder.USED_BY);
    return value != null && value != Node.EMPTY;
  }

  public static Node getParentsTree(Node node) {
    final Node usedBy = (Node) node.getValue(UsedByNodeBuilder.USED_BY);
    return usedBy == Node.EMPTY ? null : usedBy;
  }

  public static boolean nodeIsNotUsedByScenarioWithRun(Node node) {
    return !nodeIsUsedByScenarioWithRun(node);
  }

  public static boolean nodeIsUsedByScenarioWithRun(Node node) {
    if (node == null) {
      return false;
    }
    return Boolean.TRUE.equals(node.getValue(USED_BY_SCENARIO_WITH_RUN));
  }

  private class ParentNodes {

    private Node node;
    private final Set<String> parentNodeId = new HashSet<>();
    private FilterNode filterNode;
    private final boolean hasRun;

    public ParentNodes(boolean hasRun) {
      this.hasRun = hasRun;
    }

    private FilterNode getParentsNodes() {
      if (filterNode == null) {
        filterNode = buildParentNode();
        return filterNode;
      }
      return new FilterNode(filterNode);
    }

    private boolean hasParentWithNodeRecursive() {
      if (hasRun) {
        return true;
      }
      for (String id : parentNodeId) {
        ParentNodes parentNodes = parentById.get(id);
        if (parentNodes.hasParentWithNodeRecursive()) {
          return true;
        }
      }
      return false;
    }

    private FilterNode buildParentNode() {
      if (parentNodeId.isEmpty()) {
        if (hasRun) {
          setNodeHasParentWithRun();
        }
        return new FilterNode(node, Children.LEAF);
      } else {
        List<Node> children = new ArrayList<>();
        for (String string : parentNodeId) {
          children.add(parentById.get(string).getParentsNodes());
        }
        Array array = new Children.Array();
        array.add(children.toArray(new Node[0]));
        final FilterNode parentNodes = new FilterNode(node, array);
        node.setValue(USED_BY, parentNodes);
        if (hasParentWithNodeRecursive()) {
          setNodeHasParentWithRun();
        }
        return parentNodes;
      }
    }

    private void setNodeHasParentWithRun() {
      node.setValue(USED_BY_SCENARIO_WITH_RUN, Boolean.TRUE);
    }
  }

  public UsedByNodeBuilder(NodesManager nodesManager) {
    this.nodesManager = nodesManager;
  }

  private void registerUsedByForFiles(ManagerEMHContainerBase container, Node parent, boolean parentHasRun) {
    if (container.getListeFichiers() != null) {
      List<FichierCrue> fichiers = container.getListeFichiers().getFichiers();
      for (FichierCrue fichierCrue : fichiers) {
        registerUsedBy(nodesManager.getListingFichierCrue().getNode(fichierCrue), parent, parentHasRun);
      }
    }
  }

  protected void buildParents() {
    List<ManagerEMHScenario> listeScenarios = nodesManager.getProjet().getListeScenarios();
    //pour chaque scenario, on enregistre les fichiers et on associe les modeles au scenario parent
    for (ManagerEMHScenario managerEMHScenario : listeScenarios) {
      boolean hasRun = CollectionUtils.isNotEmpty(managerEMHScenario.getListeRuns());
      Node nodeScenario = nodesManager.getListingScenario().getNode(managerEMHScenario);
      registerUsedByForFiles(managerEMHScenario, nodeScenario, hasRun);
      List<ManagerEMHModeleBase> modeles = managerEMHScenario.getFils();
      for (ManagerEMHModeleBase managerEMHModeleBase : modeles) {
        Node nodeModele = nodesManager.getListingModeles().getNode(managerEMHModeleBase);
        registerUsedBy(nodeModele, nodeScenario, hasRun);
      }
    }
    //de meme pour chaque modele pour les sous-modeles et les fichiers.
    List<ManagerEMHModeleBase> listeModeles = nodesManager.getProjet().getListeModeles();
    for (ManagerEMHModeleBase managerEMHModeleBase : listeModeles) {
      Node nodeModele = nodesManager.getListingModeles().getNode(managerEMHModeleBase);
      registerUsedByForFiles(managerEMHModeleBase, nodeModele, false);
      List<ManagerEMHSousModele> sousModeles = managerEMHModeleBase.getFils();
      for (ManagerEMHSousModele managerEMHSousModele : sousModeles) {
        Node nodeSousModele = nodesManager.getListingSousModeles().getNode(managerEMHSousModele);
        registerUsedBy(nodeSousModele, nodeModele, false);
      }
    }
    //de meme pour chaque sous-modele pour les fichiers:
    List<ManagerEMHSousModele> listeSousModeles = nodesManager.getProjet().getListeSousModeles();
    for (ManagerEMHSousModele managerEMHSousModele : listeSousModeles) {
      Node nodeSousModele = nodesManager.getListingSousModeles().getNode(managerEMHSousModele);
      registerUsedByForFiles(managerEMHSousModele, nodeSousModele, false);
    }

    Collection<ParentNodes> values = parentById.values();
    for (ParentNodes parentNodes : values) {
      parentNodes.buildParentNode();
    }
  }
  private final Map<String, ParentNodes> parentById = new HashMap<>();

  private void registerUsedBy(Node node, Node parent, boolean parentHasRun) {
    ParentNodes parentsDataOfNode = parentById.get(node.getName());
    if (parentsDataOfNode == null) {
      parentsDataOfNode = new ParentNodes(false);
      parentsDataOfNode.node = node;
      parentById.put(node.getName(), parentsDataOfNode);
    }
    parentsDataOfNode.parentNodeId.add(parent.getName());
    ParentNodes parentsDataOfParent = parentById.get(parent.getName());
    if (parentsDataOfParent == null) {
      parentsDataOfParent = new ParentNodes(parentHasRun);
      parentsDataOfParent.node = parent;
      parentById.put(parent.getName(), parentsDataOfParent);
    }
  }
}
