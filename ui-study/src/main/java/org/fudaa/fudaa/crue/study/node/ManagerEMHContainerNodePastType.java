/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.node;

import java.awt.datatransfer.Transferable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.FichierCrueManager;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.datatransfer.PasteType;

/**
 *
 * @author Chris
 */
public class ManagerEMHContainerNodePastType extends PasteType {

  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  private static class TransferNodeCallable implements Callable<Boolean> {

    private final Node target;
    private final Node dragged;
    private final Node targetParent;

    public TransferNodeCallable(Node target, Node dragged, Node targetParent) {
      this.target = target;
      this.dragged = dragged;
      this.targetParent = targetParent;
    }

    @Override
    public Boolean call() throws Exception {
      if (this.target instanceof AbstractManagerNode<?>) {
        ManagerEMHContainerBase container = ((AbstractManagerNode<?>) this.target).getManager();

        if (this.dragged instanceof FichierCrueNode) {
          return this.transferFile(container, ((FichierCrueNode) this.dragged).getFichierCrue());
        } else if ((this.target instanceof ManagerEMHScenarioNode) && (this.dragged instanceof ManagerEMHModeleNode)) {
          return this.transferModele((ManagerEMHScenario) container, ((ManagerEMHModeleNode) this.dragged).getManager());
        } else if ((this.target instanceof ManagerEMHModeleNode) && (this.dragged instanceof ManagerEMHSousModeleNode)) {
          return this.transferSousModele((ManagerEMHModeleBase) container, ((ManagerEMHSousModeleNode) this.dragged).getManager());
        } else if ((this.target instanceof ManagerEMHSousModeleNode) && (this.dragged instanceof ManagerEMHSousModeleNode)) {
          ManagerEMHModeleNode modeleNode = (ManagerEMHModeleNode) this.targetParent.getParentNode();

          return transferSousModele(modeleNode.getManager(), (ManagerEMHSousModele) container, ((ManagerEMHSousModeleNode) this.dragged).getManager());
        }
      }

      return false;
    }

    public boolean transferModele(ManagerEMHScenario scenario, ManagerEMHModeleBase modele) {
      if (scenario.getManagerFils(modele.getId()) != null) {
        return false;
      }
      scenario.removeAllManagerFils();
      scenario.addManagerFils(modele);
      return true;
    }

    public boolean transferSousModele(ManagerEMHModeleBase modele, ManagerEMHSousModele sousModele) {
      if (modele.getManagerFils(sousModele.getId()) != null) {
        return false;
      }
      modele.addManagerFils(sousModele);
      return true;
    }

    public boolean transferSousModele(ManagerEMHModeleBase modele, ManagerEMHSousModele oldSousModele, ManagerEMHSousModele newSousModele) {
      if (oldSousModele == newSousModele) {
        return false;
      }
      modele.removeManagerFils(oldSousModele);
      modele.addManagerFils(newSousModele);
      return true;
    }

    public boolean transferFile(ManagerEMHContainerBase container, FichierCrue file) {
      final FichierCrueManager filesManager = container.getListeFichiers();
      if (filesManager.getFichiers().contains(file)) {
        return false;
      }
      final List<FichierCrue> files = new ArrayList<>();

      files.add(file);

      for (FichierCrue fichier : filesManager.getFichiers()) {
        if (fichier.getType() != file.getType()) {
          files.add(fichier);
        }
      }

      container.setListeFichiers(files);
      return true;
    }
  }
  private final TransferNodeCallable transferCallable;

  public ManagerEMHContainerNodePastType(Node target, Node dragged, Node targetParent) {
    this.transferCallable = new TransferNodeCallable(target, dragged, targetParent);
  }

  public ManagerEMHContainerNodePastType(Node target, Node dragged) {
    this(target, dragged, null);
  }

  @Override
  public Transferable paste() throws IOException {
    projetService.deepModification(this.transferCallable, NbBundle.getMessage(getClass(), "ManagerEMHContainerNodePastType.Log", this.transferCallable.dragged.getDisplayName()));
    return null;
  }
}
