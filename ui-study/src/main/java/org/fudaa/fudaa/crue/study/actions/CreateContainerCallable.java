/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import java.io.File;
import java.util.concurrent.Callable;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;

/**
 *
 * @author Chris
 */
public class CreateContainerCallable implements Callable<Boolean> {

  protected final ManagerEMHContainerBase container;
  protected final EMHProjet project;
  private final boolean createAllFiles;
  private final boolean overwriteExistingFile;

  public CreateContainerCallable(ManagerEMHContainerBase container, EMHProjet project, boolean createAll, boolean overwriteExistingFile) {
    this.container = container;
    this.project = project;
    this.createAllFiles = createAll;
    this.overwriteExistingFile = overwriteExistingFile;
  }

  @Override
  public Boolean call() throws Exception {
    return this.createContainer(container);
  }

  protected boolean createContainer(ManagerEMHContainerBase container) throws Exception {
    for (FichierCrue file : container.getListeFichiers().getFichiers()) {
      if (!project.getInfos().existFileInBase(file.getNom())) {

        boolean fileCreated = true;
        if (createAllFiles) {
          final File destFile = file.getProjectFile(project);
          if (!destFile.exists() || overwriteExistingFile) {
            final File blankFile = new File(project.getCoeurConfig().getDefaultFile(file.getType()).toURI());
            if (blankFile.isFile()) {
              fileCreated = CtuluLibFile.copyFile(blankFile, destFile);
            }
          }
        }
        if (!fileCreated) {
          return false;
        }
        if (!project.getInfos().addCrueFileToProject(file)) {
          return false;
        }
      }
    }

    if (container.getLevel() == CrueLevelType.SCENARIO) {
      project.addScenario((ManagerEMHScenario) container);
    } else if (container.getLevel() == CrueLevelType.MODELE) {
      project.addBaseModele((ManagerEMHModeleBase) container);
    } else if (container.getLevel() == CrueLevelType.SOUS_MODELE) {
      project.addBaseSousModele((ManagerEMHSousModele) container);
    } else {
      return false;
    }

    return true;
  }
}
