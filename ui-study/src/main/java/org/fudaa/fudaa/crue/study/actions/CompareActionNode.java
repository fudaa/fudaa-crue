/*
 GPL 2
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.ScenarioLoaderOperation;
import org.fudaa.fudaa.crue.common.CommonMessage;
import org.fudaa.fudaa.crue.common.action.AbstractNodeAction;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.EMHProjetService;
import org.fudaa.fudaa.crue.comparison.ScenarioComparaisonLauncher;
import org.fudaa.fudaa.crue.loader.LoaderService;
import org.fudaa.fudaa.crue.loader.ProjectLoadDoubleProgressRunnable;
import org.fudaa.fudaa.crue.study.node.EMHRunNode;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class CompareActionNode extends AbstractNodeAction {

  public CompareActionNode() {
    super(org.openide.util.NbBundle.getMessage(CompareActionNode.class, "CompareActionNode.Label"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    if (activatedNodes == null || activatedNodes.length != 2) {
      return false;
    }
    return getScenario(activatedNodes[0]) != null && getScenario(activatedNodes[1]) != null;
  }

  protected ManagerEMHScenario getScenario(Node node) {
    if (EMHRunNode.class.equals(node.getClass())) {
      return ((EMHRunNode) node).getParent();
    }
    return node.getLookup().lookup(ManagerEMHScenario.class);
  }

  protected EMHRun getRun(Node node) {
    return node.getLookup().lookup(EMHRun.class);
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    EMHProjetService projetService = Lookup.getDefault().lookup(EMHProjetService.class);
    EMHProjet projet = projetService.getSelectedProject();

    ManagerEMHScenario managerScenarioRef = getScenario(activatedNodes[0]);
    EMHRun runRef = getRun(activatedNodes[0]);

    ManagerEMHScenario managerScenarioCible = getScenario(activatedNodes[1]);
    EMHRun runCible = getRun(activatedNodes[1]);
    ProjectLoadDoubleProgressRunnable runnable = new ProjectLoadDoubleProgressRunnable(projet, managerScenarioRef, runRef, managerScenarioCible, runCible);
    Pair<ScenarioLoaderOperation, ScenarioLoaderOperation> res = CrueProgressUtils.showProgressDialogAndRun(runnable, NbBundle.getMessage(CompareActionNode.class, "CompareTwoScenario.LoadingScenario"));
    if (res != null) {
      EMHScenario refScenario = getFromRes(res.first, true, LoaderService.getNom(managerScenarioRef, runRef));
      EMHScenario cibleScenario = getFromRes(res.second, true, LoaderService.getNom(managerScenarioCible, runCible));
      if (refScenario != null && cibleScenario != null) {
        ScenarioComparaisonLauncher launcher = new ScenarioComparaisonLauncher(managerScenarioRef, refScenario, projet.getPropDefinition(), runRef, projet, true);
        launcher.launchComparison(cibleScenario, managerScenarioCible, runCible);
      }
    }
  }

  private EMHScenario getFromRes(ScenarioLoaderOperation load, boolean displayOnlyIfError, String name) {
    if (load == null) {
      return null;
    }
    final boolean isLoadedWithoutFatalError = load.getResult() != null && !load.getLogs().containsFatalError();
    if (displayOnlyIfError) {
      if (load.getLogs().containsError() || load.getLogs().containsFatalError()) {
        LogsDisplayer.displayError(load.getLogs(), CommonMessage.getMessage("LoadScenarioBilan.DialogTitle", name));
      }
    } else if (load.getLogs().containsSomething()) {
      LogsDisplayer.displayError(load.getLogs(), CommonMessage.getMessage("LoadScenarioBilan.DialogTitle", name));
    }
    return isLoadedWithoutFatalError ? load.getResult() : null;
  }
}
