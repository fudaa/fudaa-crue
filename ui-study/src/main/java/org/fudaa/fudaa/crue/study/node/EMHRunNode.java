/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.node;

import java.io.File;
import java.io.IOException;
import javax.swing.Action;

import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.study.actions.*;
import org.fudaa.fudaa.crue.study.perspective.PerspectiveServiceStudy;
import org.fudaa.fudaa.crue.study.property.ManagerPropertyFactory;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileUtil;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 * @author Fred Deniger
 */
public class EMHRunNode extends AbstractNodeFirable implements FileChangeListener, LookupListener, ModifiableNodeContrat {

    final ManagerEMHScenario parent;
    private boolean courant;
    private final File dir;
    private FileObject fileObject;
    final EMHProjetServiceImpl service = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
    private final Result<EMHProjet> resultat;
    private final PerspectiveServiceStudy perspectiveServiceStudy = Lookup.getDefault().lookup(PerspectiveServiceStudy.class);

    public EMHRunNode(EMHRun run, ManagerEMHScenario parent, File dir) {
        super(FileChildFactory.createChildren(dir), Lookups.singleton(run));
        this.parent = parent;
        this.dir = dir;

        setName(run.getId());
        setDisplayName(run.getNom());
        setShortDescription(run.getNom());
        setIconBaseWithExtension(CrueIconsProvider.getIconBaseRun());
        try {
            fileObject = FileUtil.createFolder(dir);
            fileObject.addRecursiveListener(this);
        } catch (IOException iOException) {
            Exceptions.printStackTrace(iOException);
        }
        resultat = service.getLookup().lookupResult(EMHProjet.class);
        resultat.addLookupListener(this);
    }

    @Override
    public boolean isEditMode() {
        return perspectiveServiceStudy.isInEditMode();
    }

    @Override
    public void updateInfos(ConnexionInformation connexionInformation) {
        getRun().getInfosVersion().updateForm(connexionInformation);
    }

    @Override
    public void resultChanged(LookupEvent ev) {
        if (!service.isProjectOpened()) {
            removeDirListener();
        }
    }

    void removeDirListener() {
        if (fileObject != null) {
            fileObject.removeRecursiveListener(this);
            fileObject = null;
        }
    }

    @Override
    public void fileAttributeChanged(FileAttributeEvent fe) {
        fileChanged();
    }

    @Override
    public void fileChanged(FileEvent fe) {
        fileChanged();
    }

    @Override
    public void fileDataCreated(FileEvent fe) {
        fileChanged();
    }

    @Override
    public void fileDeleted(FileEvent fe) {
        fileChanged();
    }

    @Override
    public void fileFolderCreated(FileEvent fe) {
        fileChanged();
    }

    @Override
    public void fileRenamed(FileRenameEvent fe) {
        fileChanged();
    }

    private void fileChanged() {
        setChildren(FileChildFactory.createChildren(dir));
    }

    public ManagerEMHScenario getParent() {
        return parent;
    }

    public void setRunCourant(boolean courant) {
        this.courant = courant;
        if (courant) {
            setIconBaseWithExtension(CrueIconsProvider.getIconBaseRunCourant());
            setShortDescription(org.openide.util.NbBundle.getMessage(EMHRunNode.class, "RunIsCourant.Tooltip", getRun().getNom()));
        } else {
            setIconBaseWithExtension(CrueIconsProvider.getIconBaseRun());
            setShortDescription(getRun().getNom());
        }
    }

    @Override
    public String getHtmlDisplayName() {
        return loaded ? "<b>" + getDisplayName() + "</b>" : null;
    }

    private boolean loaded;

    public void setRunLoaded(boolean loaded) {
        if (this.loaded == loaded) {
            return;
        }
        this.loaded = loaded;
        if (loaded) {
            setShortDescription(org.openide.util.NbBundle.getMessage(EMHRunNode.class, "RunIsLoaded.Tooltip", getRun().getNom()));
        } else {
            setRunCourant(courant);
        }
        fireDisplayNameChange(null, getDisplayName());
    }

    public EMHRun getRun() {
        return getLookup().lookup(EMHRun.class);
    }

    @Override
    public SystemAction getDefaultAction() {
        return SystemAction.get(OpenRunNodeAction.class);
    }

    @Override
    public Action[] getActions(boolean context) {
        Action[] result = new Action[]{
                SystemAction.get(OpenRunNodeAction.class),
                SystemAction.get(RenameRunNodeAction.class),
                SystemAction.get(CompareActionNode.class),
                null,
                SystemAction.get(DeleteEMHRunNodeAction.class),
                null,
                SystemAction.get(ReloadEMHRunNodeAction.class)
        };
        return result;
    }

    @Override
    public boolean canDestroy() {
        return false;
    }

    @Override
    public boolean canRename() {
        return perspectiveServiceStudy.isInEditMode();
    }

    @Override
    protected Sheet createSheet() {
        return ManagerPropertyFactory.createSheet(this, getRun().getInfosVersion());
    }

    @Override
    public void destroy() throws IOException {
        super.destroy();
    }
}
