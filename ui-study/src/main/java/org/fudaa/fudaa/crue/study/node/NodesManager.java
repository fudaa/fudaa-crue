/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.node;

import java.io.File;
import java.util.Set;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.fudaa.crue.study.ListTopComponentAbstract;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeListener;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 *
 * @author deniger
 */
public class NodesManager {

  public static final String FICHIERS = "FICHIERS";
  public static final String MODELES = "MODELES";
  public static final String SCENARIO = "SCENARIO";
  public static final String SOUS_MODELES = "SOUS_MODELES";
  private Node nodeFichierCrueList;
  private Node nodeSousModeleCrueList;
  private Node nodeModeleCrueList;
  private Node nodeScenarioCrueList;
  private EMHProjet emhProjet;
  private final Children.Array mainNodeChildren = new Children.Array();
  private final Node mainNode = new AbstractNode(mainNodeChildren, Lookup.EMPTY);

  public NodesManager() {
    clear(false);
  }

  public EMHProjetNode getProjectNode() {
    if (nodeScenarioCrueList == Node.EMPTY) {
      return null;
    }
    return (EMHProjetNode) nodeScenarioCrueList;
  }

  public void addNodeListener(NodeListener listener) {
    mainNode.addNodeListener(listener);
  }

  public void removeNodeListener(NodeListener listener) {
    mainNode.removeNodeListener(listener);
  }

  public final void clear(boolean reloading) {
    if (reloading) {
      Set<TopComponent> opened = WindowManager.getDefault().getRegistry().getOpened();
      for (TopComponent topComponent : opened) {
        if (topComponent instanceof ListTopComponentAbstract) {
          ((ListTopComponentAbstract) topComponent).saveExpandedNodes();
        }

      }
    }
    emhProjet = null;
    final Node[] nodesToRemove = mainNodeChildren.getNodes();
    nodeFichierCrueList = Node.EMPTY;
    nodeSousModeleCrueList = Node.EMPTY;
    nodeModeleCrueList = Node.EMPTY;
    nodeScenarioCrueList = Node.EMPTY;
    mainNodeChildren.remove(nodesToRemove);
  }

  public ListingManagerEMHScenario getListingScenario() {
    if (emhProjet == null) {
      return null;
    }
    return (ListingManagerEMHScenario) nodeScenarioCrueList.getChildren();
  }

  public ListingFichierCrue getListingFichierCrue() {
    if (emhProjet == null) {
      return null;
    }
    return (ListingFichierCrue) nodeFichierCrueList.getChildren();
  }

  public ListingManagerEMHSousModele getListingSousModeles() {
    if (emhProjet == null) {
      return null;
    }
    return (ListingManagerEMHSousModele) nodeSousModeleCrueList.getChildren();
  }

  public ListingManagerEMHModele getListingModeles() {
    if (emhProjet == null) {
      return null;
    }
    return (ListingManagerEMHModele) nodeModeleCrueList.getChildren();
  }

  public EMHProjet getProjet() {
    return emhProjet;
  }

  public Node getNodeFichierCrueList() {
    return nodeFichierCrueList;
  }

  public Node getNodeModeleCrueList() {
    return nodeModeleCrueList;
  }

  public Node getNodeScenarioCrueList() {
    return nodeScenarioCrueList;
  }

  public Node getNodeSousModeleCrueList() {
    return nodeSousModeleCrueList;
  }

  public final void setProject(EMHProjet projet, File etuFile, boolean reloading) {
    if (projet == null) {
      clear(reloading);
      return;
    }
    emhProjet = projet;

    nodeFichierCrueList = new EMHProjetNode(new ListingFichierCrue(getProjet()).build(), emhProjet);
    nodeFichierCrueList.setDisplayName(etuFile.getName());
    nodeFichierCrueList.setShortDescription(etuFile.getAbsolutePath());
    nodeFichierCrueList.setName(FICHIERS);

    nodeSousModeleCrueList = new EMHProjetNode(new ListingManagerEMHSousModele(this).build(), emhProjet);
    nodeSousModeleCrueList.setDisplayName(etuFile.getName());
    nodeSousModeleCrueList.setShortDescription(etuFile.getAbsolutePath());
    nodeSousModeleCrueList.setName(SOUS_MODELES);

    nodeModeleCrueList = new EMHProjetNode(new ListingManagerEMHModele(this).build(), emhProjet);
    nodeModeleCrueList.setDisplayName(etuFile.getName());
    nodeModeleCrueList.setShortDescription(etuFile.getAbsolutePath());
    nodeModeleCrueList.setName(MODELES);

    nodeScenarioCrueList = new EMHProjetNode(new ListingManagerEMHScenario(this, emhProjet).build(), emhProjet);
    nodeScenarioCrueList.setDisplayName(etuFile.getName());
    nodeScenarioCrueList.setShortDescription(etuFile.getAbsolutePath());
    nodeScenarioCrueList.setName(SCENARIO);


    mainNodeChildren.add(new Node[]{nodeFichierCrueList, nodeSousModeleCrueList, nodeModeleCrueList, nodeScenarioCrueList});
    //construction des utilisations:
    UsedByNodeBuilder parentBuilder = new UsedByNodeBuilder(this);
    parentBuilder.buildParents();
    UsedByScenarioCrue10Builder linkedCrue9 = new UsedByScenarioCrue10Builder(this);
    linkedCrue9.updateAll();
    if (reloading) {
      Set<TopComponent> opened = WindowManager.getDefault().getRegistry().getOpened();
      for (TopComponent topComponent : opened) {
        if (topComponent instanceof ListTopComponentAbstract) {
          ((ListTopComponentAbstract) topComponent).restoreExpandedNodes();
        }

      }
    }

  }

  public void updateEtuFiles() {
    if (getListingFichierCrue() != null) {
      getListingFichierCrue().updateProperties();
    }
  }
}
