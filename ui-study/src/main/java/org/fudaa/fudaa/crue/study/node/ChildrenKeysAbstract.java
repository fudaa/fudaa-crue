/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.node;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

/**
 *
 * @author Fred Deniger
 */
public abstract class ChildrenKeysAbstract<T extends ObjetNomme> extends Children.Keys<String> {

  private final java.util.Map<String, Node> nodesByKey = new HashMap<>(25);

  public ChildrenKeysAbstract() {
  }

  public ChildrenKeysAbstract build() {
    nodesByKey.clear();
    setKeys(getKeys());
    return this;
  }

  protected abstract Node createNode(String id);

  @Override
  protected final Node[] createNodes(String key) {
    final Node node = createNode(key);
    nodesByKey.put(key, node);
    return new Node[]{node};
  }

  public Node getNode(ObjetWithID object) {
    if (object == null) {
      return null;
    }
    return getNode(object.getNom());

  }

  public Node getNode(String key) {
    if (nodesByKey.isEmpty()) {
      super.getNodes();
    }
    return nodesByKey.get(key);
  }

  public Node getNode(T key) {
    return getNode(key.getNom());
  }

  protected abstract Collection<T> getObjects();

  protected final Collection<String> getKeys() {
    final Collection<String> toNom = TransformerHelper.toNom(getObjects());
    return toNom==null?Collections.emptyList():toNom;
  }

  @Override
  protected final void addNotify() {
    build();
  }

  @Override
  protected void removeNotify() {
    nodesByKey.clear();
    setKeys(Collections.EMPTY_LIST);
  }
}
