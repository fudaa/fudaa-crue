package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.projet.importer.SmImportValidator;
import org.fudaa.fudaa.crue.common.CrueFileTypeFilter;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.action.DrsoFileApprover;
import org.fudaa.fudaa.crue.common.helper.CrueFileChooserBuilder;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.study.dialog.RadicalInputNotifier;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.Collections;

/**
 * @author Chris
 */
public class ImportSousModeleNodeAction extends AbstractEditNodeAction {
  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  private final transient ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

  public ImportSousModeleNodeAction() {
    super(NbBundle.getMessage(ImportSousModeleNodeAction.class, "ImportSousModeleAction.ActionName"));
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return true;
  }

  @Override
  protected boolean isEnableWithoutActivatedNodes() {
    return true;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    //choix du fichier
    final File home = configurationManagerService.getDefaultDataHome();
    final CrueFileChooserBuilder builder = new CrueFileChooserBuilder(getClass()).setTitle(NbBundle.getMessage(getClass(), "ImportSousModeleAction.ChooseDrsoFileTitle"))
      .setDefaultWorkingDirectory(home)
      .setApproveText(NbBundle.getMessage(getClass(), "ImportSousModeleAction.ChooseDrsoFileAccept"));
    builder.setFileFilter(new CrueFileTypeFilter(Collections.singletonList(CrueFileType.DRSO)));
    builder.setSelectionApprover(new DrsoFileApprover(projetService.getSelectedProject(), NbBundle.getMessage(getClass(), "ImportSousModeleAction.ChooseDrsoFileTitle"), true));

    final File selectedDrsoFile = builder.showOpenDialog();
    if (selectedDrsoFile == null) {
      return;
    }

    //choix du commentaire
    final String radical = SmImportValidator.getSousModeleRadical(selectedDrsoFile, CrueFileType.DRSO);
    final String sousModeleNom = CruePrefix.addPrefix(radical, EnumCatEMH.SOUS_MODELE);
    NotifyDescriptor.InputLine commentInput = new NotifyDescriptor.InputLine(
      NbBundle.getMessage(getClass(), "ImportSousModeleAction.CommentText"),
      NbBundle.getMessage(getClass(), "ImportSousModeleAction.CommentTitle")
    ) {
      @Override
      protected Component createDesign(String text) {
        final JComponent design = (JComponent) super.createDesign(text);

        final String helpMessage = NbBundle.getMessage(ImportSousModeleNodeAction.class, "ImportSousModeleAction.HtmlHelp", sousModeleNom);
        return RadicalInputNotifier.addHelpLabel(helpMessage, design);
      }
    };
    if (DialogDisplayer.getDefault().notify(commentInput) == NotifyDescriptor.OK_OPTION) {
      final String comment = commentInput.getInputText().trim();
      projetService.deepModification(new ImportSousModeleCallable(projetService.getSelectedProject(), selectedDrsoFile, comment),
        NbBundle.getMessage(getClass(), "ImportSousModeleAction.Log", sousModeleNom));
    }
  }
}
