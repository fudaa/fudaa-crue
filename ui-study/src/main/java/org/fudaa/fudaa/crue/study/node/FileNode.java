package org.fudaa.fudaa.crue.study.node;

import java.io.File;
import javax.swing.Action;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.study.actions.OpenFileNodeAction;
import org.fudaa.fudaa.crue.study.property.ManagerPropertyFactory;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 * Used to create scenario node
 *
 * @author Fred Deniger
 */
public class FileNode extends AbstractNode {
  
  public FileNode(File dir) {
    super(FileChildFactory.createChildren(dir), Lookups.singleton(dir));
    setName(dir.getName());
    setDisplayName(dir.getName());
    setShortDescription(dir.getAbsolutePath());
    if (dir.isDirectory()) {
      setIconBaseWithExtension(CrueIconsProvider.getDirIconBase());
    } else {
      setIconBaseWithExtension(CrueIconsProvider.getFileIconBase());
    }
  }
  
  public File getFile() {
    return getLookup().lookup(File.class);
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getStudyFilesCtxId());
  }

  @Override
  public SystemAction getDefaultAction() {
    return SystemAction.get(OpenFileNodeAction.class);
  }
  
  @Override
  public Action[] getActions(boolean context) {
    Action[] result = new Action[]{SystemAction.get(OpenFileNodeAction.class)};
    return result;
  }
  
  @Override
  protected Sheet createSheet() {
    return ManagerPropertyFactory.createSheet(getFile());
  }
}
