/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.node;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.study.services.StudyChangeListenerService;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Fred Deniger
 */
public class ListingManagerEMHScenario extends ChildrenKeysAbstract<ManagerEMHScenario> {

  private final NodesManager nodeManager;
  private final EMHProjet projet;
  final StudyChangeListenerService studyChangeListenerService = Lookup.getDefault().lookup(StudyChangeListenerService.class);

  public ListingManagerEMHScenario(NodesManager nodeManager, EMHProjet projet) {
    this.nodeManager = nodeManager;
    this.projet = projet;
  }

  @Override
  protected Node createNode(String key) {
    ManagerEMHScenario scenario = projet.getScenario(key);
    final ManagerEMHScenarioNode managerEMHScenarioNode = new ManagerEMHScenarioNode(createChildren(scenario), scenario,
                                                                                     nodeManager);
    if (projet.getScenarioCourant() != null && projet.getScenarioCourant().equals(scenario)) {
      managerEMHScenarioNode.setScenarioCourant(true);
    }
    managerEMHScenarioNode.addPropertyChangeListener(studyChangeListenerService);
    return managerEMHScenarioNode;
  }

  @Override
  protected Collection<ManagerEMHScenario> getObjects() {
    return nodeManager.getProjet().getListeScenarios();
  }

  public Children createChildren(ManagerEMHScenario scenario) {
    List<Node> nodesToAdd = new ArrayList<>();
    AbstractNode runs = null;
    if (scenario.getListeRuns() != null && !scenario.getListeRuns().isEmpty()) {
      runs = new AbstractNode(Children.create(new EMHRunChildFactory(scenario, projet), false), Lookup.EMPTY);
      runs.setDisplayName(NbBundle.getMessage(ListingManagerEMHScenario.class, "RunsDisplayName"));
      runs.setIconBaseWithExtension("org/fudaa/fudaa/crue/study/icons/folder.png");
      nodesToAdd.add(runs);
    }

    AbstractNode modeles = new ModelesManagerNode(Children.create(new ManagerEMHModeleChildFactory(scenario, nodeManager), false),
                                                  Lookup.EMPTY);
    nodesToAdd.add(modeles);

    if (!scenario.isCrue9()) {
      AbstractNode fichier = new FichierCrueManagerNode(scenario.getListeFichiers(), nodeManager);
      nodesToAdd.add(fichier);
    }
    ManagerEMHScenarioChildren res = new ManagerEMHScenarioChildren(runs);
    res.add(nodesToAdd.toArray(new Node[0]));
    return res;


  }
}
