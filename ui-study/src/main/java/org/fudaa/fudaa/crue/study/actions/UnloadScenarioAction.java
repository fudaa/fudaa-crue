/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;

public final class UnloadScenarioAction extends NodeAction {

  final ModellingScenarioService scenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);
  final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  protected ManagerEMHScenario getScenario(Node[] activatedNodes) {
    return OpenScenarioNodeAction.getScenarioFromNodes(activatedNodes);
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected boolean enable(Node[] activatedNodes) {
    if (projetService.getSelectedProject() != null) {
      ManagerEMHScenario scenario = getScenario(activatedNodes);
      return (scenario != null) && scenarioService.getManagerScenarioLoaded() == scenario;
    }
    return false;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    scenarioService.unloadScenario();
  }

  @Override
  public String getName() {
    return NbBundle.getMessage(OpenFichierCrueNodeAction.class, "UnloadScenarioAction.Name");
  }
}
