/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.property;

import java.beans.PropertyEditorSupport;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author Fred Deniger
 */
public class PropertyEditorLocalDateTime extends PropertyEditorSupport {
  public final static DateTimeFormatter SHORT_DATE_TIME = DateTimeFormat.shortDateTime();

  public PropertyEditorLocalDateTime(LocalDateTime source) {
    super(source);
  }

  public PropertyEditorLocalDateTime() {
  }

  @Override
  public String getAsText() {
    LocalDateTime value = (LocalDateTime) getValue();
    if (value == null) {
      return StringUtils.EMPTY;
    }
    return SHORT_DATE_TIME.print(value);
  }
}
