/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.rename.RenameManager;
import org.fudaa.fudaa.crue.common.action.OneSelectionNodeAction;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.study.dialog.RunNameInputNotifier;
import org.fudaa.fudaa.crue.study.node.EMHRunNode;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import java.io.File;
import java.util.concurrent.Callable;

public final class RenameRunNodeAction extends OneSelectionNodeAction {
    protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

    public RenameRunNodeAction() {
        super(NbBundle.getMessage(RenameRunNodeAction.class, "RenameRunAction.Name"), true);
    }

    @Override
    protected boolean enable(Node activatedNodes) {
        return activatedNodes.canRename();
    }

    @Override
    protected void performAction(Node activatedNode) {
        final EMHProjet project = projetService.getSelectedProject();
        final RenameManager manager = new RenameManager();
        manager.setProject(project);
        EMHRunNode activatedRunNode = (EMHRunNode) activatedNode;

//    Node node = CrueFilterNode.getOriginalNode(activatedNodes);
        EMHRun run = activatedRunNode.getRun();
        ManagerEMHScenario scenario = activatedRunNode.getParent();
        String newRunName = getNewNames(run);
        if (newRunName != null) {
            this.rename(newRunName, manager, scenario, run);
        }
    }

    private String chooseNameInDialog(String init) {
        RunNameInputNotifier input = new RunNameInputNotifier(NbBundle.getMessage(RenameRunNodeAction.class,
                "RenameRunAction.NewRunName"),
                NbBundle.getMessage(RenameRunNodeAction.class, "RenameRunAction.Name"),
                init,
                projetService.getController().getProjet().getAllRunNameIds());
        if (DialogDisplayer.getDefault().notify(input) == NotifyDescriptor.OK_OPTION) {
            final String inputText = input.getInputText().trim();
            if (StringUtils.equals(inputText, init)) {
                return null;
            }
            return inputText;
        }
        return null;
    }

    private String getNewNames(EMHRun run) {
        String initRadical = run.getNom();
        return chooseNameInDialog(initRadical);
    }


    /**
     * Renomme le run avec le nouveau nom. Teste si pas de conflit sur le dossier
     * @param newName la nouveau nom
     * @param manager le manager opérant le renommage
     * @param scenario le scenario parent
     * @param run le run à renommer
     */
    protected void rename(String newName, RenameManager manager, ManagerEMHScenario scenario, EMHRun run) {
        final CtuluLog logs = manager.canUseNewRunName(newName);

        if (!logs.containsErrorOrSevereError()) {
            File dir = projetService.getSelectedProject().getMainDirOfRuns(scenario);
            dir = new File(dir, newName);
            if(dir.exists()){
                DialogHelper.showError(
                        NbBundle.getMessage(RenameRunNodeAction.class, "RenameRunAction.NewRunDirExist.Title"),
                        NbBundle.getMessage(RenameRunNodeAction.class, "RenameRunAction.NewRunDirExist.Message",dir.getAbsolutePath())
                        );
                return;
            }

            manager.setConnexionInformation(projetService.getController().getConnexionInformation());
            Callable<Boolean> renameCallable = new RenameCallable(newName, scenario,run, manager);

            projetService.deepModification(renameCallable, NbBundle.getMessage(RenameRunNodeAction.class, "Log.RenameAction", newName));
        }
    }

    private static class RenameCallable implements Callable<Boolean> {

        private final String newRunName;
        private final EMHRun run;
        private final ManagerEMHScenario scenario;
        private final RenameManager manager;

        private RenameCallable(String newRunName, ManagerEMHScenario scenario, EMHRun run, RenameManager manager) {
            this.newRunName = newRunName;
            this.scenario = scenario;
            this.run = run;
            this.manager = manager;
        }


        @Override
        public Boolean call() throws Exception {
            final boolean renamed = this.manager.renameRun(newRunName,scenario, run);
            return renamed;
        }
    }
}
