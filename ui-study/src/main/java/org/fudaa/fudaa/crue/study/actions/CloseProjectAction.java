/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.fudaa.crue.study.services.CrueService;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("unused")
@ActionID(category = "View",
    id = "org.fudaa.fudaa.crue.study.actions.CloseStudy")
@ActionRegistration(displayName = "#CloseStudy.Name")
@ActionReferences({
    @ActionReference(path = "Actions/Study", position = 15)
})
public final class CloseProjectAction extends AbstractStudyAction {
  private CrueService crueService;

  public CloseProjectAction() {
    super(false);
    putValue(Action.NAME, NbBundle.getMessage(CloseProjectAction.class, "CloseStudy.Name"));
    crueService = Lookup.getDefault().lookup(CrueService.class);
    //si campagne oac chargée -> on ne peut pas fermer d'étude
    crueService.addAocCampagneLookupListener(this);
  }

  @Override
  protected boolean getEnableState() {
    if (crueService == null) {
      crueService = Lookup.getDefault().lookup(CrueService.class);
    }

    //si campagne oac chargée -> on ne peut pas fermer d'étude
    return super.getEnableState() && crueService != null && !crueService.isAocCampagneLoaded();
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new CloseProjectAction();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    crueService.closeAll();
    WindowManager.getDefault().getMainWindow().repaint(0);
  }
}
