/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import javax.swing.JMenuItem;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.services.PostRunService;
import org.fudaa.fudaa.crue.study.node.EMHRunNode;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;

public final class OpenRunNodeAction extends NodeAction {

  final PostRunService postService = Lookup.getDefault().lookup(PostRunService.class);
  final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  protected EMHRun getRun(Node[] activatedNodes) {
    if (activatedNodes != null && activatedNodes.length == 1) {
      return activatedNodes[0].getLookup().lookup(EMHRun.class);
    }
    return null;
  }

  @Override
  protected boolean enable(Node[] activatedNodes) {
    if (projetService.getSelectedProject() != null) {
      EMHRun run = getRun(activatedNodes);
      return run != null && run != postService.getRunLoaded();
    }
    return false;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    if (activatedNodes != null && activatedNodes.length == 1) {
      EMHRunNode runNode = (EMHRunNode) activatedNodes[0];
      postService.loadRun(projetService.getSelectedProject(), runNode.getParent(), runNode.getRun());
    }

  }

  @Override
  public String getName() {
    return NbBundle.getMessage(OpenFichierCrueNodeAction.class, "OpenRunAction.Name");
  }

  @Override
  public JMenuItem getPopupPresenter() {
    return NodeHelper.useBoldFont(super.getPopupPresenter());
  }
}
