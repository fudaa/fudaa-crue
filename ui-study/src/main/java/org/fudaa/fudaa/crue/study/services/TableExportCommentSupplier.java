/*
 GPL 2
 */
package org.fudaa.fudaa.crue.study.services;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.openide.util.Lookup;

/**
 *
 * @author Frederic Deniger
 */
public class TableExportCommentSupplier {

  final CrueService crueService = Lookup.getDefault().lookup(CrueService.class);
  private final boolean post;

  public TableExportCommentSupplier(boolean post) {
    this.post = post;
  }

  public List<String> getComments(String type) {
    //pour tests uniquement
    if (crueService == null) {
      return Collections.emptyList();
    }
    if (post) {
      return Arrays.asList(
              BusinessMessages.getString("ExportFichierEtu") + ": " + crueService.getEtuFileLoaded().getAbsolutePath(),
              BusinessMessages.getString("ExportScenario") + ": " + crueService.getManagerScenarioLoadedInPostPerspective().getNom(),
              BusinessMessages.getString("ExportRun") + ": " + crueService.getRunLoadedInPostPerspective().getNom(),
              type);

    }
    return Arrays.asList(
            BusinessMessages.getString("ExportFichierEtu") + ": " + crueService.getEtuFileLoaded().getAbsolutePath(),
            BusinessMessages.getString("ExportScenario") + ": " + crueService.getManagerScenarioLoadedInModellingPerspective().getNom(),
            type);
  }
}
