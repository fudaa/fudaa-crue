/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import java.util.List;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainer;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;

/**
 *
 * @author Chris
 */
public class CreateContainerDeepCallable extends CreateContainerCallable {

  public CreateContainerDeepCallable(ManagerEMHContainerBase container, EMHProjet project, boolean overwrite) {
    super(container, project, true, overwrite);
  }

  @Override
  public Boolean call() throws Exception {
    return this.createContainer(container);
  }

  @Override
  protected boolean createContainer(ManagerEMHContainerBase container) throws Exception {
    if (container instanceof ManagerEMHContainer<?>) {
      final List<ManagerEMHContainerBase> children = ((ManagerEMHContainer<ManagerEMHContainerBase>) container).getFils();

      for (ManagerEMHContainerBase child : children) {
        if (!this.createContainer(child)) {
          return false;
        }
      }
    }

    return super.createContainer(container);
  }
}
