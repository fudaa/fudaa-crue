/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.node;

import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.fudaa.crue.study.services.StudyChangeListenerService;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Donne la liste de tous les fichiers présents dans l'étude
 *
 * @author Fred Deniger
 */
public class ListingFichierCrue extends ChildrenKeysAbstract<FichierCrue> {

    private final EMHProjet projet;
    private final StudyChangeListenerService studyChangeListenerService = Lookup.getDefault().lookup(StudyChangeListenerService.class);

    public ListingFichierCrue(final EMHProjet projet) {
        this.projet = projet;
    }

    @Override
    protected Node createNode(final String key) {
        final FichierCrueNode fichierCrueNode = new FichierCrueNode(projet.getInfos().getFileFromBase(key));
        fichierCrueNode.addPropertyChangeListener(studyChangeListenerService);
        return fichierCrueNode;
    }

    @Override
    protected Collection<FichierCrue> getObjects() {
        return new ArrayList<>(projet.getInfos().getBaseFichiersProjets());
    }

    void updateProperties() {
        final Node[] allNodes = getNodes();
        for (final Node node : allNodes) {
            final FichierCrueNode fichierCrueNode = (FichierCrueNode) node;
            fichierCrueNode.updateIcon();

        }
    }
}
