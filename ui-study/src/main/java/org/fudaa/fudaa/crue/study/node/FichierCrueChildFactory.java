package org.fudaa.fudaa.crue.study.node;

import java.util.List;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.FichierCrueManager;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;

/**
 * Used to create files node for a conteneur
 * @author Fred Deniger
 */
public class FichierCrueChildFactory extends ChildFactory<FichierCrue> {

  private final FichierCrueManager conteneur;
  private final NodesManager manager;

  public FichierCrueChildFactory(final FichierCrueManager conteneur, final NodesManager manager) {
    this.conteneur = conteneur;
    this.manager = manager;
  }

  @Override
  protected boolean createKeys(final List<FichierCrue> list) {
    list.addAll(conteneur.getFichiers());
    return true;
  }

  @Override
  protected Node createNodeForKey(final FichierCrue c) {
    return new CrueFilterNode(manager.getListingFichierCrue().getNode(c));
  }
}
