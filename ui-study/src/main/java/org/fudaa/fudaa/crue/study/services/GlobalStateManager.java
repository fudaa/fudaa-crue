/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.services;

import com.jidesoft.swing.SimpleScrollPane;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.*;
import javax.swing.border.Border;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.fudaa.fudaa.crue.common.action.ChangeEditStateAction;
import org.fudaa.fudaa.crue.study.node.EMHRunNode;
import org.fudaa.fudaa.crue.study.node.ListingManagerEMHScenario;
import org.fudaa.fudaa.crue.study.node.ManagerEMHScenarioNode;
import org.openide.awt.ActionID;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.actions.BooleanStateAction;
import org.openide.util.actions.Presenter;
import org.openide.util.lookup.Lookups;

@ActionID(category = "File",
id = "org.fudaa.fudaa.crue.study.services.GlobalStateManager")
@ActionRegistration(displayName = "#GlobalStateManager")
public final class GlobalStateManager extends AbstractAction implements Presenter.Toolbar {
  
  final CrueService crueService = Lookup.getDefault().lookup(CrueService.class);
  private JTextField txtEtu;
  private JLabel jlEtu;
  private JTextField txtModelling;
  private JLabel jlModelling;
  private JTextField txtPost;
  private JLabel jlPost;
  
  public GlobalStateManager() {
    crueService.addModellingEMHScenarioLookupListener(new ModellingLookupListener());
    crueService.addPostLookupListener(new PostLookupListener());
    crueService.addEMHProjetStateLookupListener(new EMHProjetLookupListener());
  }
  
  @Override
  public Component getToolbarPresenter() {
    txtModelling = new JTextField();
    txtModelling.setColumns(20);
    txtModelling.setEditable(false);
    txtEtu = new JTextField();
    txtEtu.setColumns(15);
    txtEtu.setEditable(false);
    txtPost = new JTextField();
    txtPost.setColumns(45);
    txtPost.setEditable(false);
    
    jlEtu = new JLabel(NbBundle.getMessage(GlobalStateManager.class, "GlobalPresenter.EtuState.Text"));
    jlEtu.setIcon(CrueIconsProvider.getEtuIcon());
    jlModelling = new JLabel(NbBundle.getMessage(GlobalStateManager.class, "GlobalPresenter.ModellingState.Text"));
    jlModelling.setIcon(CrueIconsProvider.getIcon(EnumCatEMH.SCENARIO));
    
    jlPost = new JLabel(NbBundle.getMessage(GlobalStateManager.class, "GlobalPresenter.PostState.Text"));
    jlPost.setIcon(CrueIconsProvider.getIconRun());
    
    Lookup.Template<BooleanStateAction> template = new Lookup.Template<>(BooleanStateAction.class,
            ChangeEditStateAction.getActionLookupId(),
            null);
    Lookup.Item<BooleanStateAction> lookupAll = Lookups.forPath(ChangeEditStateAction.getFolderLookup()).lookupItem(template);
    
    final FlowLayout layout = new FlowLayout(FlowLayout.LEFT, 0, 0);
    layout.setAlignOnBaseline(true);
    JPanel pnInfos = new JPanel(layout);
    pnInfos.add(jlEtu);
    pnInfos.add(txtEtu);
    
    pnInfos.add(jlModelling);
    pnInfos.add(txtModelling);
    pnInfos.add(jlPost);
    pnInfos.add(txtPost);
    final JScrollPane scrollPane = new SimpleScrollPane();
    scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
    scrollPane.setViewportBorder(BorderFactory.createEmptyBorder());
    scrollPane.setBorder(BorderFactory.createEmptyBorder());
    scrollPane.setViewportView(pnInfos);
    final JPanel panel = new JPanel(new BorderLayout());
    final Component button = lookupAll.getInstance().getToolbarPresenter();
    panel.add(button, BorderLayout.WEST);
    panel.add(scrollPane);
    final Border separatorBorder = BorderFactory.createEmptyBorder(0, 3, 0, 0);
    jlModelling.setBorder(separatorBorder);
    jlEtu.setBorder(separatorBorder);
    jlPost.setBorder(separatorBorder);
    updateStatePost();
    updateStateModelling();
    return panel;
  }
  
  @Override
  public void actionPerformed(ActionEvent e) {
    //inutile
  }
  
  public void updateStatePost() {
    if (txtPost != null) {
      boolean postVisible = crueService.isAScenarioLoadedInPostPerspective();
//      txtPost.setVisible(postVisible);
//      jlPost.setVisible(postVisible);
      if (postVisible) {
        txtPost.setText(
                crueService.getScenarioLoadedInPostPerspective().getNom() + " / " + crueService.getRunLoadedInPostPerspective().getNom());
        
      } else {
        txtPost.setText(StringUtils.EMPTY);
      }
      txtPost.setToolTipText(txtPost.getText());
    }
  }
  
  public void updateStateEtu() {
    File file = crueService.getEtuFileLoaded();
    if (file == null) {
      txtEtu.setText(StringUtils.EMPTY);
      txtEtu.setToolTipText(StringUtils.EMPTY);
    } else {
      txtEtu.setText(file.getName());
      txtEtu.setToolTipText(file.getAbsolutePath());
      if (crueService.isAScenarioLoadedInModellingPerspective()) {
        final ListingManagerEMHScenario listingScenario = crueService.getNodesManager().getListingScenario();
        ManagerEMHScenarioNode node = (ManagerEMHScenarioNode) listingScenario.getNode(
                crueService.getScenarioLoadedInModellingPerspective());
        node.setScenarioLoaded(true);
      }
      if (crueService.isAScenarioLoadedInPostPerspective()) {
        final EMHScenario scenarioLoaded = crueService.getScenarioLoadedInPostPerspective();
        ManagerEMHScenarioNode node = (ManagerEMHScenarioNode) crueService.getNodesManager().getListingScenario().getNode(
                scenarioLoaded);
        if (node != null) {
          final EMHRun runLoaded = crueService.getRunLoadedInPostPerspective();
          EMHRunNode runNode = node.getRun(runLoaded);
          if (runNode != null) {
            runNode.setRunLoaded(true);
          }
        }
      }
    }
    
  }
  
  public void updateStateModelling() {
    if (txtModelling != null) {
      boolean modellingVisible = crueService.isAScenarioLoadedInModellingPerspective();
      if (modellingVisible) {
        txtModelling.setText(crueService.getScenarioLoadedInModellingPerspective().getNom());
      } else {
        txtModelling.setText(StringUtils.EMPTY);
      }
      txtModelling.setToolTipText(txtModelling.getText());
    }
  }
  
  private class ModellingLookupListener implements LookupListener {
    
    ManagerEMHScenarioNode node;
    
    @Override
    public void resultChanged(LookupEvent ev) {
      updateStateModelling();
      if (crueService.isAScenarioLoadedInModellingPerspective()) {
        ManagerEMHScenario managerScenarioLoaded = crueService.getManagerScenarioLoadedInModellingPerspective();
        final ListingManagerEMHScenario listingScenario = crueService.getNodesManager().getListingScenario();
        node = (ManagerEMHScenarioNode) listingScenario.getNode(managerScenarioLoaded);
        //le scenario charge devient le scenario courant.
        node.setScenarioCourant(true);
        node.setScenarioLoaded(true);
        EMHProjet projet = crueService.getProjectLoaded();
        EMHProjetServiceImpl projetService = crueService.getProjetService();
        //on sauvegarde que si différent:
        if (projet.getScenarioCourant() != managerScenarioLoaded) {
          if (projet.getScenarioCourant() != null) {
            ManagerEMHScenarioNode oldScenarioCourant = (ManagerEMHScenarioNode) listingScenario.getNode(
                    projet.getScenarioCourant());
            oldScenarioCourant.setScenarioCourant(false);
          }
          projetService.log(NbBundle.getMessage(StudyChangeListenerService.class, "LogScenarioCourantChanged",
                  (managerScenarioLoaded == null ? "null" : managerScenarioLoaded.getNom())));
          projet.setScenarioCourant(managerScenarioLoaded);
          projetService.quickSave();
        }
      } else if (node != null) {
        node.setScenarioLoaded(false);
      }
      
    }
  }
  
  private class EMHProjetLookupListener implements LookupListener {
    
    @Override
    public void resultChanged(LookupEvent ev) {
      updateStateEtu();
    }
  }
  
  private class PostLookupListener implements LookupListener {
    
    ManagerEMHScenarioNode node;
    EMHRunNode runNode;
    
    @Override
    public void resultChanged(LookupEvent ev) {
      updateStatePost();
      if (crueService.isAScenarioLoadedInPostPerspective()) {
        final EMHScenario scenarioLoaded = crueService.getScenarioLoadedInPostPerspective();
        final EMHRun runLoaded = crueService.getRunLoadedInPostPerspective();
        node = (ManagerEMHScenarioNode) crueService.getNodesManager().getListingScenario().getNode(scenarioLoaded);
        runNode = node.getRun(runLoaded);
        runNode.setRunLoaded(true);
      } else {
        if (runNode != null) {
          runNode.setRunLoaded(false);
        }
      }
      
    }
  }
}
