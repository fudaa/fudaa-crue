/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.openide.util.NbBundle;

public final class ConvertScenarioCrue9NodeAction extends AbstractConvertScenarioNodeAction {

  public ConvertScenarioCrue9NodeAction() {
    super(NbBundle.getMessage(ConvertScenarioCrue9NodeAction.class, "ConvertScenarioCrue9NodeAction.Name"), CrueVersionType.CRUE9);
  }
}
