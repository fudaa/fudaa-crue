/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;

public abstract class AbstractConvertScenarioNodeAction extends AbstractEditNodeAction {

  private final CrueVersionType targetVersion;

  public AbstractConvertScenarioNodeAction(String name,CrueVersionType targetVersion) {
    super(name);
    this.targetVersion = targetVersion;
  }

  public void convertScenario(ManagerEMHScenario sourceScenario) {
    new ConvertScenarioProcessor().convertScenario(sourceScenario, targetVersion);
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return OpenScenarioNodeAction.containsScenarioInNodes(activatedNodes);
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    if (activatedNodes != null && activatedNodes.length > 0) {
      for (Node activatedNode : activatedNodes) {
        ManagerEMHScenario sourceScenario = activatedNode.getLookup().lookup(ManagerEMHScenario.class);
        if (sourceScenario != null) {
          convertScenario(sourceScenario);
        }
      }
    }
  }

 
}
