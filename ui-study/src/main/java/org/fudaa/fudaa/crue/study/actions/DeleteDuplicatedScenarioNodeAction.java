/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.clean.EMHProjectCleaner;
import org.fudaa.dodico.crue.projet.clean.ScenarioCleanData;
import org.fudaa.dodico.crue.projet.select.DoublonScenarioFinder;
import org.fudaa.dodico.crue.projet.select.SelectableFinder;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.loader.action.CleanProjectUiSelector;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import java.util.List;

public final class DeleteDuplicatedScenarioNodeAction extends AbstractEditNodeAction {
  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public DeleteDuplicatedScenarioNodeAction() {
    super(NbBundle.getMessage(DeleteDuplicatedScenarioNodeAction.class, "DeleteDuplicatedScenario.Name"));
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return true;
  }

  @Override
  protected boolean isEnableWithoutActivatedNodes() {
    return true;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  /**
   * Suppression des scenarios en doublons ( terminant par c10 et si un autre sans c10) issus des campagnes OTFA.
   *
   * @param projetService
   */
  public static void deleteDuplicatedScenarios(final EMHProjetServiceImpl projetService) {
    //etape 1: on recherche les scenarios en doublons
    DoublonScenarioFinder doublonScenarioFinder = new DoublonScenarioFinder();
    doublonScenarioFinder.setProject(projetService.getSelectedProject());
    final List<ManagerEMHScenario> doublons = doublonScenarioFinder.getDoublons();
    final String actionTitle = NbBundle.getMessage(DeleteDuplicatedScenarioNodeAction.class, "DeleteDuplicatedScenario.Name");

    if (doublons.isEmpty()) {
      DialogHelper.showNotifyOperationTermine(actionTitle,
          NbBundle.getMessage(DeleteDuplicatedScenarioNodeAction.class, "NoDuplicatedScenarioFound")
      );
      return;
    }
    CleanProjectUiSelector selector = new CleanProjectUiSelector();
    ScenarioCleanData data = new ScenarioCleanData();
    data.scenarioNamesToDelete.addAll(TransformerHelper.toNom(doublons));
    data = selector.select(data, actionTitle, NbBundle.getMessage(DeleteDuplicatedScenarioNodeAction.class, "DeleteDuplicatedScenarionConfirmation.Yes"));

    if (data != null && data.isNotEmpty()) {
      //on supprime
      final SelectableFinder.SelectedItems items = EMHProjectCleaner.getSelectedItems(projetService.getSelectedProject(), data.scenarioNamesToDelete);
      String log = NbBundle.getMessage(DeleteManagerNodeAction.class, "Log.DeleteAction", StringUtils.join(data.scenarioNamesToDelete, ToStringHelper.SEPARATOR));
      DeleteManagerNodeAction.deleteManagers(projetService, items, log, actionTitle);
    }
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    deleteDuplicatedScenarios(projetService);
  }
}
