package org.fudaa.fudaa.crue.study.actions;

import java.io.File;
import static org.apache.commons.lang3.CharUtils.*;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.projet.create.InfosCreation;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class ConvertScenarioProgressRunnable implements ProgressRunnable<CtuluLogGroup> {

  private final ManagerEMHScenario sourceScenario;
  private final EMHProjet projet;
  private final File etuFile;
  private final CrueVersionType targetVersion;
  private final InstallationService installationService = Lookup.getDefault().lookup(InstallationService.class);

  public ConvertScenarioProgressRunnable(ManagerEMHScenario sourceScenario, EMHProjet projet, File etuFile, CrueVersionType targetVersion) {
    this.sourceScenario = sourceScenario;
    this.projet = projet;
    this.etuFile = etuFile;
    this.targetVersion = targetVersion;
  }

  @Override
  public CtuluLogGroup run(ProgressHandle handle) {
    EMHProjetController projetController = new EMHProjetController(projet, installationService.getConnexionInformation());
    InfosCreation infosCreation = new InfosCreation(installationService.getConnexionInformation());
    infosCreation.setCommentaire(NbBundle.getMessage(ConvertScenarioCrue10NodeAction.class, "ConvertScenarioComment",
            sourceScenario.getNom(), targetVersion.toString()) + CR + LF + sourceScenario.getInfosVersions().getCommentaire());
    infosCreation.setCrueVersion(targetVersion);
    CrueOperationResult<EMHScenarioContainer> convertScenario = projetController.convertScenario(sourceScenario.getNom(), infosCreation, etuFile);

    return convertScenario.getLogs();
  }
}
