package org.fudaa.fudaa.crue.study.node;

import org.fudaa.dodico.crue.common.ConnexionInformation;

/**
 *
 * @author deniger
 */
public interface ModifiableNodeContrat {
  
  
  void updateInfos(ConnexionInformation connexionInformation);
  
}
