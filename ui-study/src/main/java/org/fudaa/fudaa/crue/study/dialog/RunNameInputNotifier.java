package org.fudaa.fudaa.crue.study.dialog;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.fudaa.crue.common.swing.CommonGuiLib;
import org.jdesktop.swingx.VerticalLayout;
import org.openide.NotifyDescriptor;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.util.Set;

/**
 * @author deniger
 */
public class RunNameInputNotifier extends NotifyDescriptor.InputLine implements DocumentListener {
    /**
     * Radical utilisé pour l'initialisation du champ texte de la fenêtre
     */
    private final String initName;
    private final Set<String> existingRunIds;


    /**
     * Constructeur permettant de spécifier si le changement de radical est obligatoire ou non
     *
     * @param text          le texte affiché à gauche du champ de saisi de radical
     * @param title         le titre de la fenêtre
     * @param initName      le nom initial du run
     * @param existingNames les noms de run déjà utilisés
     */
    public RunNameInputNotifier(String text, String title, String initName, Set<String> existingNames) {
        super(text, title);
        super.textField.setText(initName);
        super.textField.setToolTipText(NbBundle.getMessage(RunNameInputNotifier.class, "RunNameInputNotifierTooltip", initName));
        this.initName = initName;
        this.existingRunIds = existingNames;
        setValid(false);
        createNotificationLineSupport();
        super.textField.getDocument().addDocumentListener(this);
        //le faire après pour mettre à jour le message correctement
        setDefaultMessage();
    }

    @Override
    protected Component createDesign(String text) {
        final String helpText = NbBundle.getMessage(RunNameInputNotifier.class, "RunNameInputNotifier.HtmlHelp");
        final JComponent design = (JComponent) super.createDesign(text);
        return addHelpLabel(helpText, design);
    }

    public static Component addHelpLabel(String helpText, JComponent origin) {
        JLabel informationLabel = new JLabel();
        informationLabel.setText(helpText);
        CommonGuiLib.initilalizeHelpLabel(informationLabel);
        JPanel pnInfo = new JPanel(new BorderLayout());
        pnInfo.add(informationLabel, BorderLayout.CENTER);
        pnInfo.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));

        JPanel pn = new JPanel(new VerticalLayout());
        pn.add(origin);
        pn.add(pnInfo);
        //important pour que les messages d'erreur puissent s'afficher totalement.
        pn.setPreferredSize(new Dimension(700, 90));
        return pn;
    }

    /**
     * Fixe le message d'aide
     */
    private void setDefaultMessage() {
        getNotificationLineSupport().setInformationMessage(computeDefaultMessage(null));
    }

    /**
     * Retourne le message d'aide
     *
     * @return le message d'aide, format texte
     */
    private String computeDefaultMessage(String error) {
        int maxChar = CruePrefix.NB_CAR_MAX;
        int currentSize = textField.getText().length();
        final String nbCharState = currentSize + "/" + maxChar;
        final String remaining = Integer.toString(maxChar - currentSize);
        if (error != null) {
            return NbBundle.getMessage(RunNameInputNotifier.class, "RadicalInputNotifier.HelpError", error, nbCharState, remaining);
        }
        return NbBundle.getMessage(RunNameInputNotifier.class, "RadicalInputNotifier.Help", nbCharState, remaining);
    }


    @Override
    public void changedUpdate(DocumentEvent e) {
        updateValidState();
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        updateValidState();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        updateValidState();
    }

    /**
     * Gestion de l'activation/désactivation du bouton "OK" de la fenêtre et des messages affichés en fonction de la saisie
     */
    private void updateValidState() {
        final String newRunName = textField.getText().trim();
        final String radicalValidei18nMsg = ValidationPatternHelper.isRunNameValidei18nMessage(newRunName);
        boolean isValid = (radicalValidei18nMsg == null);
        if (!isValid) {
            getNotificationLineSupport().setErrorMessage(computeDefaultMessage(radicalValidei18nMsg));
        }
        if (isValid) {
            isValid = !StringUtils.equals(initName, newRunName);
            if (!isValid) {
                getNotificationLineSupport().setErrorMessage(computeDefaultMessage(BusinessMessages.getString("validation.name.NotChanged.error")));
            }
        }
        if (isValid) {
            isValid = !existingRunIds.contains(newRunName.toUpperCase());
            if (!isValid) {
                getNotificationLineSupport().setErrorMessage(computeDefaultMessage(BusinessMessages.getString("validation.name.alreadyUsedShort.error")));
            }
        }
        setValid(isValid);
        if (isValid) {
            getNotificationLineSupport().setErrorMessage(null);
            setDefaultMessage();
        }
    }
}
