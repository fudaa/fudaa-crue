/*
GPL 2
 */
package org.fudaa.fudaa.crue.study.services;

import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.metier.etude.*;

import java.util.List;
import java.util.Set;

/**
 * Classe utilitaire pour mettre à jour l'etat des fichiers d'un scenario ( le modifier et la date).
 *
 * @author Frederic Deniger
 * @see ConnexionInformation
 */
public class FichierCrueManagerStateUpdater {
    /**
     * @param fichierCrueIdToCheck la collection de fichiers a tester
     * @param fichiers             les ids des fichiers modifiés
     * @return true si la collection de fichiers contient un id donnée par <code>fichierCrueIdToCheck</code>
     * @see FichierCrue#getId()
     */
    private static boolean containsAtLeastOnFichierCrueId(Set<String> fichierCrueIdToCheck, FichierCrueManager fichiers) {
        if (fichiers == null) {
            return false;
        }
        for (FichierCrue fichierCrue : fichiers.getFichiers()) {
            if (fichierCrueIdToCheck.contains(fichierCrue.getId())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Mis a jour des états des conteneurs si un fichier contenu est modifié.
     *
     * @param scenario          le scenario
     * @param info              les infos de modification ( auteur et date)
     * @param idOfModifiedFiles les ids des fichiers modifiés
     */
    protected static void updateModifiedState(ManagerEMHScenario scenario, ConnexionInformation info, Set<String> idOfModifiedFiles) {
        if (containsAtLeastOnFichierCrueId(idOfModifiedFiles, scenario.getListeFichiers())) {
            scenario.getInfosVersions().updateForm(info);
        }
        List<ManagerEMHModeleBase> modeles = scenario.getFils();
        for (ManagerEMHModeleBase modele : modeles) {
            boolean modified = updateModifiedState(modele, info, idOfModifiedFiles);
            //si un fichier d'un Modele/sous modele est modifié, il faut mettre à jour le scenario egalement.
            if (modified) {
                scenario.getInfosVersions().updateForm(info);
            }
        }
    }

    /**
     * Mis a jour des états du modele et des sous-modele: si l'id d'un fichier de ces conteneurs fait partis des modifiés, les infos du conteneur sont mis à
     * jour.
     *
     * @param modele            le modele
     * @param info              les infos de modification ( auteur et date)
     * @param idOfModifiedFiles les ids des fichiers modifiés
     * @return true si un fichier modifié a été trouvé.
     */
    private static boolean updateModifiedState(ManagerEMHModeleBase modele, ConnexionInformation info, Set<String> idOfModifiedFiles) {
        boolean res = containsAtLeastOnFichierCrueId(idOfModifiedFiles, modele.getListeFichiers());
        List<ManagerEMHSousModele> sousModeles = modele.getFils();
        for (ManagerEMHSousModele sousModele : sousModeles) {
            boolean sousModeleModified = containsAtLeastOnFichierCrueId(idOfModifiedFiles, sousModele.getListeFichiers());
            if (sousModeleModified) {
                res = true;
                sousModele.getInfosVersions().updateForm(info);
            }
        }
        if (res) {
            modele.getInfosVersions().updateForm(info);
        }
        return res;
    }
}
