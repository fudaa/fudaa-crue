package org.fudaa.fudaa.crue.study;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainer;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.fudaa.crue.study.node.ChildrenKeysAbstract;
import org.openide.nodes.Node;
import org.openide.util.Lookup.Result;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 *
 * @author deniger
 */
public class FileSelectionListener extends AbstractSelectionListener {

  public FileSelectionListener(ListTopComponentAbstract component) {
    super(component, true);
  }

  @Override
  protected Set<Node> selectNodes(ChildrenKeysAbstract node) {
    Collection<ManagerEMHContainerBase> allInstances = getAllSelectedManager();
    Set<Node> found = new HashSet<>(allInstances.size());
    for (ManagerEMHContainerBase object : allInstances) {
      addUsedNode(object, node, found);
    }
    TopComponent activated = TopComponent.getRegistry().getActivated();
    if (activated == null) {
      return Collections.emptySet();
    }
    String findTopComponentID = WindowManager.getDefault().findTopComponentID(activated);
    Result<FichierCrue> get = selectedFileById.get(findTopComponentID);
    if (get != null) {
      Collection<? extends FichierCrue> allInstances1 = get.allInstances();
      for (FichierCrue fichierCrue : allInstances1) {
        Node fileNode = node.getNode(fichierCrue);
        if (fileNode != null) {
          found.add(fileNode);
        }
      }
    }

    return found;
  }

  private void addUsedNode(ManagerEMHContainerBase object, ChildrenKeysAbstract listingFichierCrue, Set<Node> found) {
    if (object.getListeFichiers() != null && object.getListeFichiers().getFichiers() != null) {
      List<FichierCrue> fichiers = object.getListeFichiers().getFichiers();
      for (FichierCrue fichierCrue : fichiers) {
        Node node = listingFichierCrue.getNode(fichierCrue);
        if (node != null) {
          found.add(node);
        }
      }
    }
    if (object instanceof ManagerEMHContainer) {
      ManagerEMHContainer container = (ManagerEMHContainer) object;
      List listFils = container.getFils();
      for (Object fils : listFils) {
        addUsedNode((ManagerEMHContainerBase) fils, listingFichierCrue, found);

      }

    }
  }
}
