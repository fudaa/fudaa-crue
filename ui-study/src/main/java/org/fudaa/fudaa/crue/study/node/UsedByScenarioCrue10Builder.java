package org.fudaa.fudaa.crue.study.node;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;

/**
 * Permet de mettre à jour les propriétés usedBy pour les scenarios Crue9.
 *
 * @author deniger
 */
public class UsedByScenarioCrue10Builder {

  private final NodesManager nodesManager;

  public UsedByScenarioCrue10Builder(NodesManager nodesManager) {
    this.nodesManager = nodesManager;
  }

  /**
   * Met à jour les scenarios Crue9.
   */
  public void updateAll() {
    ListingManagerEMHScenario listingScenario = nodesManager.getListingScenario();
    Collection<ManagerEMHScenario> scenarios = listingScenario.getObjects();
    List<ManagerEMHScenario> crue9Scenario = new ArrayList<>();
    Map<String, List<ManagerEMHScenario>> usedByCrue10 = new HashMap<>();
    for (ManagerEMHScenario scenario : scenarios) {
      if (scenario.isCrue9()) {
        crue9Scenario.add(scenario);
      } else if (scenario.getLinkedscenarioCrue9() != null && nodesManager.getProjet().getCoeurConfig().isCrue9Dependant()) {
        List<ManagerEMHScenario> usedby = usedByCrue10.get(scenario.getLinkedscenarioCrue9());
        if (usedby == null) {
          usedby = new ArrayList<>();
          usedByCrue10.put(scenario.getLinkedscenarioCrue9(), usedby);
        }
        usedby.add(scenario);
      }
    }
    for (ManagerEMHScenario crue9 : crue9Scenario) {
      List<ManagerEMHScenario> crue10Scenarios = usedByCrue10.get(crue9.getNom());
      final Node node = listingScenario.getNode(crue9);
      if (crue10Scenarios == null) {
        node.setValue(UsedByNodeBuilder.USED_BY, Node.EMPTY);
      } else {
        List<Node> nodes = new ArrayList<>();
        for (ManagerEMHScenario crue10Scenario : crue10Scenarios) {
          nodes.add(new FilterNode(listingScenario.getNode(crue10Scenario), Children.LEAF));
        }
        Children.Array array = new Children.Array();
        array.add(nodes.toArray(new Node[0]));
        node.setValue(UsedByNodeBuilder.USED_BY, new FilterNode(new FilterNode(node), array));
      }

    }


  }
}
