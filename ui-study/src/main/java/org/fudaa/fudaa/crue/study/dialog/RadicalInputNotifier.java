package org.fudaa.fudaa.crue.study.dialog;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.fudaa.crue.common.swing.CommonGuiLib;
import org.jdesktop.swingx.VerticalLayout;
import org.openide.NotifyDescriptor;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;

/**
 * @author deniger
 */
public class RadicalInputNotifier extends NotifyDescriptor.InputLine implements DocumentListener {
  /**
   * Radical utilisé pour l'initialisation du champ texte de la fenêtre
   */
  private final String initRadical;
  /**
   * Le radical par défaut peut-il être la valeur finale ?
   */
  private final boolean allowSameRadical;
  private final int specificMaxTaille;
  private final String specifMaxTailleMessage;

  /**
   * Constructeur rendant obligatoire le changement de radical
   *
   * @param text le texte affiché à gauche du champ de saisi de radical
   * @param title le titre de la fenêtre
   * @param initRadical le radical initial
   */
  public RadicalInputNotifier(String text, String title, String initRadical) {
    this(text, title, initRadical, false, -1, null);
  }

  /**
   * Constructeur permettant de spécifier si le changement de radical est obligatoire ou non
   *
   * @param text le texte affiché à gauche du champ de saisi de radical
   * @param title le titre de la fenêtre
   * @param initRadical le radical initial
   * @param allowSameRadical true si le même radical est autorisé, false sinon
   * @param maxTaille la taille max attendue pour le nom
   * @param maxTailleErrorMessage le message à afficher si dépassé.
   */
  public RadicalInputNotifier(String text, String title, String initRadical, boolean allowSameRadical, int maxTaille, String maxTailleErrorMessage) {
    super(text, title);
    super.textField.setText(initRadical);
    super.textField.setToolTipText(NbBundle.getMessage(RadicalInputNotifier.class, "RadicalInputNotifierTooltip", initRadical));
    this.initRadical = initRadical;
    this.allowSameRadical = allowSameRadical;
    setValid(allowSameRadical);
    createNotificationLineSupport();
    super.textField.getDocument().addDocumentListener(this);
    this.specificMaxTaille = maxTaille;
    this.specifMaxTailleMessage = maxTailleErrorMessage;
    //le faire après pour mettre à jour le message correctement
    setDefaultMessage();
  }

  @Override
  protected Component createDesign(String text) {
    final String helpText = NbBundle.getMessage(RadicalInputNotifier.class, "RadicalInputNotifier.HtmlHelp");
    final JComponent design = (JComponent) super.createDesign(text);
    return addHelpLabel(helpText, design);
  }

  public static Component addHelpLabel(String helpText, JComponent origin) {
    JLabel informationLabel = new JLabel();
    informationLabel.setText(helpText);
    CommonGuiLib.initilalizeHelpLabel(informationLabel);
    JPanel pnInfo = new JPanel(new BorderLayout());
    pnInfo.add(informationLabel, BorderLayout.CENTER);
    pnInfo.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));

    JPanel pn = new JPanel(new VerticalLayout());
    pn.add(origin);
    pn.add(pnInfo);
    //important pour que les messages d'erreur puissent s'afficher totalement.
    pn.setPreferredSize(new Dimension(700, 90));
    return pn;
  }

  /**
   * Fixe le message d'aide
   */
  private void setDefaultMessage() {
    getNotificationLineSupport().setInformationMessage(computeRadicalMessage(null));
  }

  /**
   * Retourne le message d'aide
   *
   * @return le message d'aide, format texte
   */
  private String computeRadicalMessage(String error) {
    int maxChar = ValidationPatternHelper.getRadicalMaxLength(specificMaxTaille);
    int currentSize = textField.getText().length();
    final String nbCharState = currentSize + "/" + maxChar;
    final String remaining = Integer.toString(maxChar - currentSize);
    if (error != null) {
      return NbBundle.getMessage(RadicalInputNotifier.class, "RadicalInputNotifier.HelpError", error, nbCharState, remaining);
    }
    return NbBundle.getMessage(RadicalInputNotifier.class, "RadicalInputNotifier.Help", nbCharState, remaining);
  }

  /**
   * Retourne le message d'aide
   *
   * @param nbCharState "nombreCaractereCourant/maxAutorise"
   * @param nbCharRemaining "Nombre caractere restant"
   * @return le message d'aide, format HTML
   */
  public static String getRadicalInfoMessageScenario(String nbCharState, String nbCharRemaining) {
    return NbBundle.getMessage(RadicalInputNotifier.class, "RadicalInputNotifier.Help", nbCharState, nbCharRemaining);
  }

  /**
   * Retourne le message d'aide dans le cas de renommage d'un scenario
   *
   * @return le message d'aide, format HTML
   */
  public static String getRadicalInfoMessageScenario() {
    return NbBundle.getMessage(RadicalInputNotifier.class, "RadicalInputNotifier.HtmlHelp");
  }

  /**
   * Retourne le message d'aide dans le cas de renommage d'une entité différente d'un scenario
   *
   * @return le message d'aide, format HTML
   */
  public static String getRadicalInfoMessageOther() {
    return NbBundle.getMessage(RadicalInputNotifier.class, "RadicalInputNotifierOther.HtmlHelp");
  }

  /**
   * Retourne le message d'aide d'erreur
   *
   * @param nbCharState "nombreCaractereCourant/maxAutorise"
   * @param nbCharRemaining "Nombre caractere restant"
   * @return le message d'aide d'erreur, format HTML
   */
  public static String getRadicalInfoMessageHtmlError(String errorMsg, String nbCharState, String nbCharRemaining) {
    return NbBundle.getMessage(RadicalInputNotifier.class, "RadicalInputNotifier.HelpError", errorMsg, nbCharState, nbCharRemaining);
  }

  @Override
  public void changedUpdate(DocumentEvent e) {
    updateValidState();
  }

  @Override
  public void insertUpdate(DocumentEvent e) {
    updateValidState();
  }

  @Override
  public void removeUpdate(DocumentEvent e) {
    updateValidState();
  }

  /**
   * Gestion de l'activation/désactivation du bouton "OK" de la fenêtre et des messages affichés en fonction de la saisie
   */
  private void updateValidState() {
    final String newRadical = textField.getText().trim();
    //si le nom initial contient déjà un suffixe AOC, on autorise le renommage avec un tel suffixe
    //donc si le nom initial est utilisable pour une campagne AOC ( ne contient pas le suffixe), le nouveau radical doit l'être aussi
    boolean radicalValidForAocCampagne = ValidationPatternHelper.getScenarioFilterForAoc().test(initRadical);
    final String radicalValideMsg = ValidationPatternHelper
        .isRadicalValideForEMHContainerMsg(newRadical, this.specificMaxTaille, specifMaxTailleMessage, radicalValidForAocCampagne);
    final boolean isValid = (radicalValideMsg == null);
    if (!isValid) {
      setValid(false);
      getNotificationLineSupport().setErrorMessage(computeRadicalMessage(BusinessMessages.getString(radicalValideMsg)));
      return;
    }
    getNotificationLineSupport().setErrorMessage(null);
    final boolean isNotSame = allowSameRadical || !StringUtils.equals(initRadical, newRadical);
    setValid(isNotSame);
    setDefaultMessage();
  }
}
