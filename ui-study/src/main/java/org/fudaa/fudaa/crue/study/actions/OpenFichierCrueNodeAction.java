/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JMenuItem;
import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.options.services.OpenFileProcess;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;

/**
 *
 * @author Fred Deniger
 */
public class OpenFichierCrueNodeAction extends NodeAction {

  private final OpenFileProcess nodeAction = new OpenFileProcess();
  final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  @Override
  protected boolean enable(Node[] activatedNodes) {
    if (ArrayUtils.isEmpty(activatedNodes)) {
      return false;
    }
    for (Node node : activatedNodes) {
      File fichier = getFichier(node);
      if (fichier != null && fichier.isFile()) {
        return true;
      }
    }
    return false;
  }

  public File getFichier(Node node) {
    FichierCrue fichierCrue = node.getLookup().lookup(FichierCrue.class);
    if (fichierCrue == null) {
      return null;
    }
    return fichierCrue.getProjectFile(projetService.getSelectedProject());
  }

  @Override
  protected void initialize() {
    super.initialize();
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    final List<File> files = new ArrayList<>();
    for (Node node : activatedNodes) {
      File fichier = getFichier(node);
      if (fichier != null && fichier.isFile()) {
        files.add(fichier);
      }
    }
    nodeAction.openFiles(files);
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  public String getName() {
    return NbBundle.getMessage(OpenFichierCrueNodeAction.class, "OpenFichierAction.Name");
  }

  @Override
  public JMenuItem getPopupPresenter() {
    return NodeHelper.useBoldFont(super.getPopupPresenter());
  }
}
