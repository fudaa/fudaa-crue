package org.fudaa.fudaa.crue.study;

import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainer;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.fudaa.crue.study.node.ChildrenKeysAbstract;
import org.openide.nodes.Node;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author deniger
 */
public class ManagerSelectionListener extends AbstractSelectionListener {
    public ManagerSelectionListener(ListTopComponentAbstract component) {
        super(component, false);
    }

    /**
     * @param nodes les nodes a selectionner
     * @return tous les nodes sélectionnés (le manager et tous ces fils).
     */
    @Override
    protected Set<Node> selectNodes(ChildrenKeysAbstract nodes) {
        Collection<ManagerEMHContainerBase> allInstances = getAllSelectedManager();
        Set<Node> found = new HashSet<>(allInstances.size());
        for (ManagerEMHContainerBase object : allInstances) {
            addUsedNode(object, nodes, found);
        }
        return found;
    }

    private void addUsedNode(ManagerEMHContainerBase object, ChildrenKeysAbstract nodes, Set<Node> found) {
        Node node = nodes.getNode(object);
        if (node != null) {
            found.add(node);
        }
        if (object instanceof ManagerEMHContainer) {
            ManagerEMHContainer container = (ManagerEMHContainer) object;
            List listFils = container.getFils();
            for (Object fils : listFils) {
                addUsedNode((ManagerEMHContainerBase) fils, nodes, found);
            }
        }
    }
}
