package org.fudaa.fudaa.crue.study.property;

import java.lang.reflect.InvocationTargetException;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.etude.EMHInfosVersion;
import org.fudaa.fudaa.crue.common.CommonMessage;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertySupportReadWrite;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class PropertyEMHInfosVersionCommentaire extends PropertySupportReadWrite<EMHInfosVersion, String> {
  
  public PropertyEMHInfosVersionCommentaire(final AbstractNodeFirable node, EMHInfosVersion instance) {
    super(node, instance, String.class, ManagerPropertyFactory.COMMENTAIRE_NAME, NbBundle.getMessage(CommonMessage.class, ManagerPropertyFactory.COMMENTAIRE_NAME), null);
    setValue("nullValue", StringUtils.EMPTY);
  }
  
  @Override
  public String getValue() throws IllegalAccessException, InvocationTargetException {
    return getInstance().getCommentaire();
  }

  @Override
  protected void setValueInInstance(String newVal) {
    getInstance().setCommentaire(newVal);
  }
  
  
}
