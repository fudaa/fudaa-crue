package org.fudaa.fudaa.crue.study.node;

import java.util.List;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;

/**
 * Used to create scenario node
 * @author Fred Deniger
 */
public class ManagerEMHSousModeleChildFactory extends ChildFactory<ManagerEMHSousModele> {

  private final ManagerEMHModeleBase modele;
  private final NodesManager nodeManager;

  public ManagerEMHSousModeleChildFactory(final ManagerEMHModeleBase modele, final NodesManager nodeManager) {
    this.modele = modele;
    this.nodeManager = nodeManager;
  }

  @Override
  protected boolean createKeys(final List<ManagerEMHSousModele> list) {
    list.addAll(modele.getFils());
    return true;
  }

  @Override
  protected Node createNodeForKey(final ManagerEMHSousModele c) {
    return new CrueFilterNode(nodeManager.getListingSousModeles().getNode(c));
  }
}
