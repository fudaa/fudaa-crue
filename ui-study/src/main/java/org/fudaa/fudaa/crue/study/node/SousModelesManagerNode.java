/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.node;

import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.datatransfer.PasteType;

import java.awt.datatransfer.Transferable;

/**
 * @author deniger
 */
public class SousModelesManagerNode extends AbstractNode {
    public SousModelesManagerNode(Children children, Lookup lookup) {
        super(children, lookup);
        setDisplayName(NbBundle.getMessage(getClass(), "SousModelesManagerNodeDisplayName"));
        setIconBaseWithExtension("org/fudaa/fudaa/crue/study/icons/folder.png");
    }

    @Override
    public PasteType getDropType(Transferable t, int action, int index) {
        final Node dragged = DefaultNodePasteType.getMovedNodeInTransferable(t);

        if (dragged instanceof ManagerEMHSousModeleNode) {
            return this.getParentNode().getDropType(t, action, index);
        }

        return null;
    }
}
