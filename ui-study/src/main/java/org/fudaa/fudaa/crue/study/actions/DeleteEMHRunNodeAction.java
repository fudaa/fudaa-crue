/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.concurrent.Callable;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.FileDeleteResult;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;
import org.fudaa.dodico.crue.edition.EditionDeleteContainer;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.projet.calcul.RunScenarioPair;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.NodeToStringTransformer;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.study.node.EMHRunNode;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public final class DeleteEMHRunNodeAction extends AbstractEditNodeAction {

  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public DeleteEMHRunNodeAction() {
    super(NbBundle.getMessage(DeleteEMHRunNodeAction.class, "Delete.Name"));
  }

  public static void showInfoIfRunDirNotDeleted(final Map<RunScenarioPair, FileDeleteResult> managers, final boolean later) {
    final CtuluLogGroup log = EditionDeleteContainer.createLogsIfRunNotDeleted(managers);
    LogsDisplayer.displayError(log, log.getDesci18n());
  }

  public boolean containsRuns(final ManagerEMHContainerBase manager) {
    return manager instanceof ManagerEMHScenario && CollectionUtils.isNotEmpty(((ManagerEMHScenario) manager).getListeRuns());
  }

  public String getDialogTitleI18n(final boolean single, final String name) throws MissingResourceException {
    return NbBundle.getMessage(DeleteEMHRunNodeAction.class,
            single ? "DeleteAction.EMHRun.DialogTitle" : "DeleteAction.EMHRunMulti.DialogTitle", name);
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    if (activatedNodes != null) {
      for (final Node node : activatedNodes) {
        if (node instanceof EMHRunNode) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected void performAction(final Node[] activatedNodes) {
    final List<EMHRunNode> toBeDeleted = new ArrayList<>();
    if (activatedNodes != null) {
      for (final Node node : activatedNodes) {
        if (node instanceof EMHRunNode) {
          toBeDeleted.add((EMHRunNode) node);
        }
      }
      delete(toBeDeleted);
    }

  }

  public boolean userCancelAction(final boolean single, final String name) {
    final String dialogContent = single ? "DeleteAction.EMHRun.DialogContent" : "DeleteAction.EMHRunMulti.DialogContent";
    final String dialogTitle = getDialogTitleI18n(single, name);

    return !DialogHelper.showQuestion(dialogTitle,
            NbBundle.getMessage(DeleteEMHRunNodeAction.class, dialogContent, name));
  }

  protected void delete(final List<EMHRunNode> nodes) {

    final boolean isSingle = nodes.size() == 1;

    final String name = isSingle ? nodes.get(0).getDisplayName() : Integer.toString(nodes.size());
    if (userCancelAction(isSingle, name)) {
      return;
    }
    final Callable<Boolean> deleteCallable = () -> {
      final EMHProjetController controller = projetService.getController();
      final Map<RunScenarioPair, FileDeleteResult> notClean = new HashMap<>();
      boolean modificationDone = false;
      for (final EMHRunNode nodeToDelete : nodes) {
        final ManagerEMHScenario scenario = nodeToDelete.getParent();
        final FileDeleteResult deleteResultat = new FileDeleteResult();
        if (controller.deleteRun(scenario, nodeToDelete.getRun(), deleteResultat)) {
          modificationDone = true;
        }
        if (deleteResultat.isNotEmpty()) {
          notClean.put(new RunScenarioPair(scenario, nodeToDelete.getRun()), deleteResultat);
        }
      }
      if (!notClean.isEmpty()) {
        showInfoIfRunDirNotDeleted(notClean, true);
      }
      return modificationDone;
    };
    final String logEntry = ToStringHelper.transformTo(nodes, new NodeToStringTransformer());

    projetService.deepModification(deleteCallable, org.openide.util.NbBundle.getMessage(DeleteEMHRunNodeAction.class,
            "Log.DeleteAction", logEntry));
  }
}
