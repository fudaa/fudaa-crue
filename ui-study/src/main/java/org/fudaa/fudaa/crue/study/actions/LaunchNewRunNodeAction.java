package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.action.OneSelectionNodeAction;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class LaunchNewRunNodeAction extends OneSelectionNodeAction {

  final EMHProjetServiceImpl projetController = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public LaunchNewRunNodeAction() {
    super(NbBundle.getMessage(OpenFichierCrueNodeAction.class, "LaunchNewRunNodeAction.Name"),true);
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }
  
  

  @Override
  protected boolean enable(Node activatedNodes) {
    ManagerEMHScenario lookup = activatedNodes.getLookup().lookup(ManagerEMHScenario.class);
    if (lookup != null) {
      return lookup.isActive();
    }
    return false;
  }

  @Override
  protected void performAction(Node activatedNodes) {
    projetController.launchRun(activatedNodes.getLookup().lookup(ManagerEMHScenario.class));



  }
}
