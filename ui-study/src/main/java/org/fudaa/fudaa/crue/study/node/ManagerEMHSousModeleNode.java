/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.node;

import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.study.actions.*;
import org.fudaa.fudaa.crue.study.perspective.PerspectiveServiceStudy;
import org.fudaa.fudaa.crue.study.property.ManagerPropertyFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.PasteType;

import javax.swing.*;
import java.awt.datatransfer.Transferable;

/**
 * @author Fred Deniger
 */
public class ManagerEMHSousModeleNode extends AbstractManagerNode<ManagerEMHSousModele> implements FilterableNode {
    private final PerspectiveServiceStudy perspectiveService = Lookup.getDefault().lookup(PerspectiveServiceStudy.class);

    public ManagerEMHSousModeleNode(Children children, ManagerEMHSousModele modele, EMHProjet projet) {
        super(children, modele, projet);
        setName(modele.getNom());
        setIconBaseWithExtension(CrueIconsProvider.getIconBase(EnumCatEMH.SOUS_MODELE));
    }

    @Override
    public Action[] getActions(boolean context) {
        Action[] result = new Action[]{
                SystemAction.get(ModifySousModeleNodeAction.class),
                SystemAction.get(RenameManagerNodeAction.class),
                SystemAction.get(CopyManagerNodeAction.class),
                null,
                SystemAction.get(DeleteManagerNodeAction.class),
                null,
                SystemAction.get(ShowUsedByDefaultNodeAction.class)
        };
        return result;
    }

    @Override
    public SystemAction getDefaultAction() {
        return SystemAction.get(ShowUsedByDefaultNodeAction.class);
    }

    @Override
    protected Sheet createSheet() {
        return ManagerPropertyFactory.createSheet(this, getLookup().lookup(ManagerEMHSousModele.class), getProjet());
    }

    @Override
    public PasteType getDropType(Transferable t, int action, int index) {
        return this.getDropType(this.getParentNode(), t, action, index);
    }

    @Override
    public PasteType getDropType(Node parent, Transferable t, int action, int index) {
        if (perspectiveService.isInEditMode()) {
            final Node dragged = DefaultNodePasteType.getMovedNodeInTransferable(t);

            if (dragged instanceof FichierCrueNode) {
                CrueVersionType sousModeleVersion = this.getManager().getInfosVersions().getCrueVersion();
                FichierCrue file = ((FichierCrueNode) dragged).getFichierCrue();

                if ((file.getType().getCrueVersionType() == sousModeleVersion) && file.getType().isSousModelLevel() && !getManager().getListeFichiers()
                        .getFichiers().contains(
                                file)) {
                    return new ManagerEMHContainerNodePastType(this, dragged, parent);
                }
            } else if (dragged instanceof ManagerEMHSousModeleNode) {
                if (parent.getParentNode() != null && ((ManagerEMHSousModeleNode) dragged).getManager() != getManager()) {
                    return new ManagerEMHContainerNodePastType(this, dragged, parent);
                }
            }
        }

        return parent.getDropType(t, index, index);
    }
}
