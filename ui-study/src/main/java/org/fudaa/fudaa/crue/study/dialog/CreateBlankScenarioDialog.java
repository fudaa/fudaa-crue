/*
 * ManageManagerEMHContainerDialog.java
 *
 * Created on 12 janv. 2012, 16:02:15
 */
package org.fudaa.fudaa.crue.study.dialog;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.etude.*;
import org.fudaa.dodico.crue.projet.rename.RenameManager;
import org.fudaa.dodico.crue.projet.rename.RenameManager.NewNameInformations;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.SysdocContrat;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.swing.CommonGuiLib;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import java.util.ArrayList;
import java.util.List;

/**
 * Dialogue de creation d'un scenario vierge.
 */
public class CreateBlankScenarioDialog extends javax.swing.JDialog {
  private final EMHProjet project = Lookup.getDefault().lookup(EMHProjetServiceImpl.class).getSelectedProject();
  private final ConnexionInformation connexionInfos = Lookup.getDefault().lookup(InstallationService.class).getConnexionInformation();
  private ManagerEMHScenario scenario;
  private boolean okClicked = false;
  public final String helpId = SysdocUrlBuilder.getDialogHelpCtxId("creerScenarioCrue10Vierge", PerspectiveEnum.STUDY);

  /**
   * Creates new form ManageManagerEMHContainerDialog
   */
  public CreateBlankScenarioDialog(final java.awt.Frame parent, final boolean modal) {
    super(parent, modal);
    initComponents();
    final RadicalValidationInstaller validation = new RadicalValidationInstaller(radicalField, jLabelValidation,
        ValidationPatternHelper.SPECIFIC_MAX_TAILLE_FOR_BLANK_SCENARIO,
        ValidationPatternHelper.MESSAGE_ERROR_BLANK_SCENARIO_NAME_TOO_LONG);
    validation.setRunAfterValidation(() -> okButton.setEnabled(validation.isValid()));
    jLabelMessage.setText(RadicalInputNotifier.getRadicalInfoMessageScenario());
    CommonGuiLib.initilalizeHelpLabel(jLabelMessage);

    this.setLocationRelativeTo(null);
    setResizable(true);
    SysdocUrlBuilder.installHelpShortcut(jBtHelp, helpId);
  }

  public boolean isOkClicked() {
    return this.okClicked;
  }

  public ManagerEMHScenario getScenario() {
    return this.scenario;
  }

  private List<FichierCrue> getFiles(final String radical, final NewNameInformations infos, final CrueLevelType level) {
    final List<FichierCrue> files = new ArrayList<>();
    for (final CrueFileType type : CrueFileType.values()) {
      if (!type.isResultFileType() && (type.getCrueVersionType() == CrueVersionType.CRUE10) && (type.getLevel() == level)) {
        if (!EMHProjetServiceImpl.isOptrAccepted(project) && CrueFileType.OPTR.equals(type)) {
          continue;
        }
        final String name = radical + "." + type.getExtension();
        final FichierCrue file = new FichierCrue(name, FichierCrue.CURRENT_PATH, type);
        files.add(file);
        infos.newNameFiles.put(name, file);
      }
    }

    return files;
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    containerPanel = new javax.swing.JPanel();
    radicalLabel = new javax.swing.JLabel();
    radicalField = new javax.swing.JTextField();
    commentLabel = new javax.swing.JLabel();
    commentField = new javax.swing.JTextField();
    jLabelPrefix = new javax.swing.JLabel();
    jLabelMessage = new javax.swing.JLabel();
    jLabelValidation = new javax.swing.JLabel();
    buttonsPanel = new javax.swing.JPanel();
    okButton = new javax.swing.JButton();
    cancelButton = new javax.swing.JButton();
    jBtHelp = new javax.swing.JButton();

    setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    setTitle(org.openide.util.NbBundle.getMessage(CreateBlankScenarioDialog.class, "CreateBlankScenarioDialog.title")); // NOI18N
    setMinimumSize(new java.awt.Dimension(650, 220));

    containerPanel.setMinimumSize(new java.awt.Dimension(570, 160));

    radicalLabel.setText(org.openide.util.NbBundle.getMessage(CreateBlankScenarioDialog.class, "CreateBlankScenarioDialog.radicalLabel.text")); // NOI18N

    radicalField.setText(org.openide.util.NbBundle.getMessage(CreateBlankScenarioDialog.class, "CreateBlankScenarioDialog.radicalField.text")); // NOI18N
    radicalField.setPreferredSize(new java.awt.Dimension(324, 20));

    commentLabel.setText(org.openide.util.NbBundle.getMessage(CreateBlankScenarioDialog.class, "CreateBlankScenarioDialog.commentLabel.text")); // NOI18N

    commentField.setText(org.openide.util.NbBundle.getMessage(CreateBlankScenarioDialog.class, "CreateBlankScenarioDialog.commentField.text")); // NOI18N
    commentField.setPreferredSize(new java.awt.Dimension(295, 20));

    jLabelPrefix.setText(org.openide.util.NbBundle.getMessage(CreateBlankScenarioDialog.class, "CreateBlankScenarioDialog.jLabelPrefix.text")); // NOI18N

    jLabelMessage.setText(org.openide.util.NbBundle.getMessage(CreateBlankScenarioDialog.class, "CreateBlankScenarioDialog.jLabelMessage.text")); // NOI18N

    jLabelValidation.setText(org.openide.util.NbBundle.getMessage(CreateBlankScenarioDialog.class, "CreateBlankScenarioDialog.jLabelValidation.text")); // NOI18N

    final javax.swing.GroupLayout containerPanelLayout = new javax.swing.GroupLayout(containerPanel);
    containerPanel.setLayout(containerPanelLayout);
    containerPanelLayout.setHorizontalGroup(
      containerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(containerPanelLayout.createSequentialGroup()
        .addContainerGap()
        .addGroup(containerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jLabelMessage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addGroup(containerPanelLayout.createSequentialGroup()
            .addGroup(containerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(commentLabel)
              .addComponent(radicalLabel))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(containerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
              .addGroup(javax.swing.GroupLayout.Alignment.LEADING, containerPanelLayout.createSequentialGroup()
                .addComponent(jLabelPrefix, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radicalField, javax.swing.GroupLayout.DEFAULT_SIZE, 423, Short.MAX_VALUE))
              .addComponent(jLabelValidation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addComponent(commentField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGap(15, 15, 15)))
        .addGap(0, 0, 0))
    );
    containerPanelLayout.setVerticalGroup(
      containerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(containerPanelLayout.createSequentialGroup()
        .addGap(14, 14, 14)
        .addComponent(jLabelMessage, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)
        .addGap(18, 18, 18)
        .addGroup(containerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(radicalLabel)
          .addComponent(radicalField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jLabelPrefix))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jLabelValidation)
        .addGap(18, 18, 18)
        .addGroup(containerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(commentLabel)
          .addComponent(commentField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addContainerGap())
    );

    getContentPane().add(containerPanel, java.awt.BorderLayout.CENTER);

    buttonsPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

    okButton.setText(org.openide.util.NbBundle.getMessage(CreateBlankScenarioDialog.class, "CreateBlankScenarioDialog.okButton.text")); // NOI18N
    okButton.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(final java.awt.event.ActionEvent evt) {
        okButtonActionPerformed(evt);
      }
    });
    buttonsPanel.add(okButton);

    cancelButton.setText(org.openide.util.NbBundle.getMessage(CreateBlankScenarioDialog.class, "CreateBlankScenarioDialog.cancelButton.text")); // NOI18N
    cancelButton.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(final java.awt.event.ActionEvent evt) {
        cancelButtonActionPerformed(evt);
      }
    });
    buttonsPanel.add(cancelButton);

    jBtHelp.setText(org.openide.util.NbBundle.getMessage(CreateBlankScenarioDialog.class, "CreateBlankScenarioDialog.jBtHelp.text")); // NOI18N
    jBtHelp.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(final java.awt.event.ActionEvent evt) {
        jBtHelpActionPerformed(evt);
      }
    });
    buttonsPanel.add(jBtHelp);

    getContentPane().add(buttonsPanel, java.awt.BorderLayout.SOUTH);

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void cancelButtonActionPerformed(final java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
    this.setVisible(false);
    dispose();
  }//GEN-LAST:event_cancelButtonActionPerformed

  private void okButtonActionPerformed(final java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
    final String radical = this.radicalField.getText();
    final String msg = ValidationPatternHelper.isRadicalValideForBlankScenario(radical);
    if (msg != null) {
      DialogHelper.showError(NbBundle.getMessage(getClass(), "CreateBlankScenarioDialog.title"),
          NbBundle.getMessage(getClass(), "CreateBlankScenarioDialog.invalidRadical", BusinessMessages.getString(msg)));
      return;
    }

    final NewNameInformations infos = new NewNameInformations();

    final ManagerEMHSousModele sousModele = new ManagerEMHSousModele(CrueLevelType.SOUS_MODELE.getPrefix() + radical);
    infos.newNameContainers.put(sousModele.getNom(), sousModele);
    sousModele.setListeFichiers(this.getFiles(radical, infos, CrueLevelType.SOUS_MODELE));

    EMHInfosVersion infosVersion = new EMHInfosVersion();
    infosVersion.setVersion(CrueVersionType.CRUE10);
    infosVersion.setAuteurCreation(connexionInfos.getCurrentUser());
    infosVersion.setDateCreation(connexionInfos.getCurrentDate());
    infosVersion.setAuteurDerniereModif(infosVersion.getAuteurCreation());
    infosVersion.setDateDerniereModif(infosVersion.getDateCreation());
    infosVersion.setCommentaire(this.commentField.getText());

    sousModele.setInfosVersions(infosVersion);

    final ManagerEMHModeleBase modele = new ManagerEMHModeleBase(CrueLevelType.MODELE.getPrefix() + radical);
    infos.newNameContainers.put(modele.getNom(), modele);
    modele.setListeFichiers(this.getFiles(radical, infos, CrueLevelType.MODELE));
    modele.addManagerFils(sousModele);

    infosVersion = new EMHInfosVersion();
    infosVersion.setVersion(CrueVersionType.CRUE10);
    infosVersion.setAuteurCreation(connexionInfos.getCurrentUser());
    infosVersion.setDateCreation(connexionInfos.getCurrentDate());
    infosVersion.setAuteurDerniereModif(infosVersion.getAuteurCreation());
    infosVersion.setDateDerniereModif(infosVersion.getDateCreation());
    infosVersion.setCommentaire(this.commentField.getText());

    modele.setInfosVersions(infosVersion);

    this.scenario = new ManagerEMHScenario(CrueLevelType.SCENARIO.getPrefix() + radical);
    infos.newNameContainers.put(scenario.getNom(), scenario);
    scenario.setListeFichiers(this.getFiles(radical, infos, CrueLevelType.SCENARIO));
    scenario.addManagerFils(modele);

    infosVersion = new EMHInfosVersion();
    infosVersion.setVersion(CrueVersionType.CRUE10);
    infosVersion.setAuteurCreation(connexionInfos.getCurrentUser());
    infosVersion.setDateCreation(connexionInfos.getCurrentDate());
    infosVersion.setAuteurDerniereModif(infosVersion.getAuteurCreation());
    infosVersion.setDateDerniereModif(infosVersion.getDateCreation());
    infosVersion.setCommentaire(this.commentField.getText());

    scenario.setInfosVersions(infosVersion);

    final RenameManager manager = new RenameManager();
    manager.setProject(this.project);

    final CtuluLog logs = manager.canUseNewNames(infos);

    if (logs.containsErrorOrSevereError()) {
      LogsDisplayer.displayError(logs, NbBundle.getMessage(getClass(), "CreateBlankScenarioDialog.title"));
      return;
    }

    this.okClicked = true;
    this.setVisible(false);
    this.dispose();
  }//GEN-LAST:event_okButtonActionPerformed

  private void jBtHelpActionPerformed(final java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtHelpActionPerformed
    final SysdocContrat sysdocContrat = Lookup.getDefault().lookup(SysdocContrat.class);
    sysdocContrat.display(helpId);
  }//GEN-LAST:event_jBtHelpActionPerformed

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JPanel buttonsPanel;
  private javax.swing.JButton cancelButton;
  private javax.swing.JTextField commentField;
  private javax.swing.JLabel commentLabel;
  private javax.swing.JPanel containerPanel;
  private javax.swing.JButton jBtHelp;
  private javax.swing.JLabel jLabelMessage;
  private javax.swing.JLabel jLabelPrefix;
  private javax.swing.JLabel jLabelValidation;
  private javax.swing.JButton okButton;
  private javax.swing.JTextField radicalField;
  private javax.swing.JLabel radicalLabel;
  // End of variables declaration//GEN-END:variables
}
