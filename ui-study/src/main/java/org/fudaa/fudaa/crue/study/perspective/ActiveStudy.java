package org.fudaa.fudaa.crue.study.perspective;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractPerspectiveAction;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
/**
 * Permet d'activer une perspective. Action utilisée dans la toobar pour changer de perspective. Pour la gestion des ordres des menus, voir le fichier
 * layer.xml
 */
@ActionID(category = "File",
id = "org.fudaa.fudaa.crue.study.perspective.ActiveStudy")
@ActionRegistration(displayName = "#CTL_ActiveStudy")
@ActionReference(path = "Menu/Window", position = 2)
public final class ActiveStudy extends AbstractPerspectiveAction {

  public ActiveStudy() {
    super("CTL_ActiveStudy", PerspectiveServiceStudy.class.getName(),PerspectiveEnum.STUDY);
  }
  
  @Override
  protected String getFolderAction() {
    return "Actions/Study";
  }
}
