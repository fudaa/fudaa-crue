/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.services;

import java.awt.Component;
import java.awt.event.ActionEvent;
import javax.swing.*;
import org.openide.awt.ActionID;
import org.openide.awt.ActionRegistration;
import org.openide.util.actions.Presenter;

/**
 * pour pallier un bug de Netbeans lorsque l'on retaille la fenetre.
 *
 * @author Frederic Deniger
 */
@ActionID(category = "File",
id = "org.fudaa.fudaa.crue.study.services.GlobalStateManagerRien")
@ActionRegistration(displayName = "#GlobalStateManagerRien")
public final class GlobalStateManagerRien extends AbstractAction implements Presenter.Toolbar {

  public GlobalStateManagerRien() {
  }

  @Override
  public Component getToolbarPresenter() {
    return new JLabel("          ");
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    //inutile
  }
}
