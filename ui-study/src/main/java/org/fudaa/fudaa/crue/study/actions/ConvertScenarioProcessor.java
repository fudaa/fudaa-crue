package org.fudaa.fudaa.crue.study.actions;

import org.apache.commons.collections4.CollectionUtils;
import static org.apache.commons.lang3.CharUtils.CR;
import static org.apache.commons.lang3.CharUtils.LF;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.create.ContainerLevelData;
import org.fudaa.dodico.crue.projet.create.ContainerLevelDataValidator;
import org.fudaa.dodico.crue.projet.create.ConversionNameConverter;
import org.fudaa.dodico.crue.projet.create.InfosCreation;
import org.fudaa.dodico.crue.projet.create.LevelDataFactory;
import org.fudaa.dodico.crue.projet.create.LevelDataFactory.NameConverter;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class ConvertScenarioProcessor {

  final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  final InstallationService installationService = Lookup.getDefault().lookup(InstallationService.class);

  public void convertScenario(ManagerEMHScenario sourceScenario, CrueVersionType targetVersion) {
    String name = NbBundle.getMessage(getClass(), "ConvertScenario.Name", sourceScenario.getNom(), targetVersion.getDisplayName());
    final NameConverter converter = new ConversionNameConverter(targetVersion);
    final ContainerLevelData data = LevelDataFactory.createCopyScenarioLevelData(sourceScenario, targetVersion, converter);
    ContainerLevelDataValidator validator = new ContainerLevelDataValidator(projetService.getSelectedProject());
    CtuluLog log = validator.valid(data, true);
    if (log.containsWarnings()) {
      String nameDest = data.getName();
      final ManagerEMHScenario deletedScenario = projetService.getSelectedProject().getScenario(nameDest);
      String msg="ConvertScenario.OverrideBottomMessageInDialog";
      if(deletedScenario!=null && CollectionUtils.isNotEmpty(deletedScenario.getListeRuns())){
        msg="ConvertScenario.OverrideAndDeleteRunsBottomMessageInDialog";
      }
      boolean isOk = LogsDisplayer.displayErrorWithQuestion(log, name,
              NbBundle.getMessage(AbstractConvertScenarioNodeAction.class, "ConvertScenario.OverrideTopMessageInDialog"),
              NbBundle.getMessage(AbstractConvertScenarioNodeAction.class, msg), DialogHelper.NO_VERSION_FOR_PERSISTENCE);
      if (!isOk) {
        return;
      }
    }
    InfosCreation infosCreation = new InfosCreation(installationService.getConnexionInformation());
    final String comment = NbBundle.getMessage(AbstractConvertScenarioNodeAction.class, "ConvertScenarioComment", sourceScenario.getNom(), targetVersion.toString()) + CR + LF + sourceScenario.getInfosVersions().getCommentaire();
    infosCreation.setCommentaire(comment);
    infosCreation.setCrueVersion(targetVersion);
    ConvertScenarioProgressRunnable runner = new ConvertScenarioProgressRunnable(sourceScenario, projetService.getSelectedProject(), projetService.getEtuFile(), targetVersion);
    projetService.createHistory();//on cree un historique avant l'action
    final CtuluLogGroup logs = CrueProgressUtils.showProgressDialogAndRun(runner, name);
    projetService.updateLastLogsGroup(logs);
    if (!logs.containsFatalError()) {
      projetService.reload(comment);
    }
    if (logs.containsSomething()) {
      LogsDisplayer.displayError(logs, name);
    }
  }
}
