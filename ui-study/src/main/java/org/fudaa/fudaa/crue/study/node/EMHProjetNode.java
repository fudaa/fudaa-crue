package org.fudaa.fudaa.crue.study.node;

import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.metier.etude.EMHInfosVersion;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.study.actions.*;
import org.fudaa.fudaa.crue.study.perspective.PerspectiveServiceStudy;
import org.fudaa.fudaa.crue.study.property.ManagerPropertyFactory;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.fudaa.fudaa.crue.study.services.StudyChangeListenerService;
import org.joda.time.LocalDateTime;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

import javax.swing.*;
import java.io.File;

/**
 * @author deniger
 */
public class EMHProjetNode extends AbstractNodeFirable implements ModifiableNodeContrat {
  private final StudyChangeListenerService studyChangeListenerService = Lookup.getDefault().lookup(StudyChangeListenerService.class);
  private final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  private final PerspectiveServiceStudy perspectiveServiceStudy = Lookup.getDefault().lookup(PerspectiveServiceStudy.class);

  public EMHProjetNode(Children children, EMHProjet project) {
    super(children, Lookups.singleton(project));
    addPropertyChangeListener(studyChangeListenerService);
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getStudyCtxId());
  }

  @Override
  public Action[] getActions(boolean context) {
    if (this.getChildren() instanceof ListingManagerEMHScenario) {
      return new Action[]{
          SystemAction.get(CreateScenarioNodeAction.class),
          SystemAction.get(CreateScenarioDeepNodeAction.class),
          SystemAction.get(CreateScenarioCrue9NodeAction.class),
          null,
          SystemAction.get(DeleteAllRunsOfEtuNodeAction.class),
          SystemAction.get(DeleteDuplicatedScenarioNodeAction.class),
          null,
          SystemAction.get(NormalizeScenariosNodeAction.class)
      };
    } else if (this.getChildren() instanceof ListingManagerEMHModele) {
      return new Action[]{SystemAction.get(CreateModeleNodeAction.class), SystemAction.get(CreateModeleCrue9NodeAction.class)};
    } else if (this.getChildren() instanceof ListingManagerEMHSousModele) {
      return new Action[]{SystemAction.get(CreateSousModeleNodeAction.class),SystemAction.get(ImportSousModeleNodeAction.class)};
    } else if (this.getChildren() instanceof ListingFichierCrue) {
      return new Action[]{SystemAction.get(AddFileAction.class), SystemAction.get(CreateFileNodeAction.class)};
    }

    return super.getActions(context);
  }

  @Override
  public boolean isEditMode() {
    return perspectiveServiceStudy.isInEditMode();
  }

  public File getEtuFile() {
    return projetService.getEtuFile();
  }

  @Override
  public void updateInfos(ConnexionInformation connexionInformation) {
    EMHProjet projet = getProject();
    EMHInfosVersion infosVersions = projet.getInfos().getInfosVersions();
    String auteurDerniereModif = infosVersions.getAuteurDerniereModif();
    LocalDateTime dateDerniereModif = infosVersions.getDateDerniereModif();
    infosVersions.updateForm(connexionInformation);
    firePropertyChange(ManagerPropertyFactory.AUTEUR_MODIFICATION, auteurDerniereModif, infosVersions.getAuteurDerniereModif());
    firePropertyChange(ManagerPropertyFactory.DATE_MODIFICATION, dateDerniereModif, infosVersions.getDateDerniereModif());
  }

  public EMHProjet getProject() {
    return getLookup().lookup(EMHProjet.class);
  }

  @Override
  protected Sheet createSheet() {
    return ManagerPropertyFactory.createProjectNodeSheet(this);
  }
}
