/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.node;

import org.fudaa.dodico.crue.metier.etude.FichierCrueManager;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;

import java.awt.datatransfer.Transferable;

/**
 * @author deniger
 */
public class FichierCrueManagerNode extends AbstractNode {
    public FichierCrueManagerNode(FichierCrueManager fichierCrueManager, NodesManager nodeManager) {
        super(Children.create(new FichierCrueChildFactory(fichierCrueManager, nodeManager), false), Lookups.singleton(fichierCrueManager));
        setDisplayName(NbBundle.getMessage(FichierCrueManagerNode.class, "FichierCrueManagerDisplayName"));
        setIconBaseWithExtension("org/fudaa/fudaa/crue/study/icons/folder.png");
    }

    @Override
    public PasteType getDropType(Transferable t, int action, int index) {
        final Node dragged = DefaultNodePasteType.getMovedNodeInTransferable(t);
        if (dragged instanceof FichierCrueNode) {
            return this.getParentNode().getDropType(t, action, index);
        }

        return null;
    }
}
