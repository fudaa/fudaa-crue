/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.dodico.crue.projet.rename.RenameManager;
import org.fudaa.dodico.crue.projet.rename.RenameManager.NewNameInformations;
import org.fudaa.dodico.crue.projet.select.SelectableFinder;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.fudaa.crue.common.action.OneSelectionNodeAction;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.study.actions.OverwriteFileHelper.OverwriteResult;
import org.fudaa.fudaa.crue.study.dialog.RadicalInputNotifier;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

public final class RenameManagerNodeAction extends OneSelectionNodeAction {
  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public RenameManagerNodeAction() {
    super(NbBundle.getMessage(RenameManagerNodeAction.class, "RenameAction.Name"), true);
  }

  @Override
  protected boolean enable(Node activatedNodes) {
    return activatedNodes.canRename();
  }

  @Override
  protected void performAction(Node activatedNodes) {
    final EMHProjet project = projetService.getSelectedProject();
    final RenameManager manager = new RenameManager();
    manager.setProject(project);

//    Node node = CrueFilterNode.getOriginalNode(activatedNodes);
    ManagerEMHContainerBase container = activatedNodes.getLookup().lookup(ManagerEMHContainerBase.class);
    NewNameInformations infos = null;
    String name = null;
    if (container != null) {
      infos = this.getNewNames(container, manager);
      name = container.getNom();
    } else if (activatedNodes.getLookup().lookup(FichierCrue.class) != null) {
      final FichierCrue fichier = activatedNodes.getLookup().lookup(FichierCrue.class);

      infos = this.getNewNames(fichier, manager);
      name = fichier.getNom();
    }

    if (infos != null) {
      this.rename(infos, manager, name);
    }
  }

  private String chooseNameInDialog(String init) {
    RadicalInputNotifier input = new RadicalInputNotifier(NbBundle.getMessage(RenameManagerNodeAction.class,
        "RenameAction.RadicalMessage"),
        NbBundle.getMessage(RenameManagerNodeAction.class, "RenameAction.Title"),
        init,
        false,
        ValidationPatternHelper.SPECIFIC_MAX_TAILLE_FOR_BLANK_SCENARIO,
        ValidationPatternHelper.MESSAGE_ERROR_RENAME_NAME_TOO_LONG

    );
    if (DialogDisplayer.getDefault().notify(input) == NotifyDescriptor.OK_OPTION) {
      final String inputText = input.getInputText().trim();
      if (StringUtils.equals(inputText, init)) {
        return null;
      }
      return inputText;
    }
    return null;
  }

  private NewNameInformations getNewNames(ManagerEMHContainerBase container, RenameManager manager) {
    int res = JOptionPane.showOptionDialog(WindowManager.getDefault().getMainWindow(),
        NbBundle.getMessage(getClass(), "RenameAction.TypeMessage"),
        NbBundle.getMessage(getClass(), "RenameAction.Title"), JOptionPane.YES_NO_CANCEL_OPTION,
        JOptionPane.QUESTION_MESSAGE, null, new String[]{NbBundle.getMessage(getClass(),
            "RenameAction.OneButton"), NbBundle.getMessage(
            getClass(), "RenameAction.DeepButton"), NbBundle.getMessage(getClass(), "RenameAction.CancelButton")},
        NbBundle.getMessage(getClass(), "RenameAction.OneButton"));

    if (res < 0 || res > 1) {
      return null;
    }

    final boolean deep = res == 1;

    String initRadical = StringUtils.removeStart(container.getNom(), container.getLevel().getPrefix());
    String radical = chooseNameInDialog(initRadical);
    if (radical == null) {
      return null;
    }
    SelectableFinder finder = new SelectableFinder();
    finder.setProject(manager.getProject());

    return manager.getNewNames(container, finder.getIndependantSelection(Collections.singletonList(container), deep),
        radical, deep);
  }

  private NewNameInformations getNewNames(FichierCrue fichier, RenameManager manager) {

    String initRadical = StringUtils.removeEnd(fichier.getNom(), "." + fichier.getType().getExtension());
    String radical = chooseNameInDialog(initRadical);
    if (radical == null) {
      return null;
    }
    return manager.getNewNames(fichier, radical);
  }

  protected void rename(NewNameInformations infos, RenameManager manager, String name) {
    if (infos.isEmpty()) {
      DialogHelper.showError(NbBundle.getMessage(getClass(), "RenameAction.Title"), NbBundle.getMessage(
          RenameManagerNodeAction.class, "RenameAction.RenameNothing"));

      return;
    }
    final CtuluLog logs = manager.canUseNewNames(infos);

    if (!logs.containsErrorOrSevereError()) {

      List<String> overwrittenFiles = manager.getOverwrittenFiles(infos);
      OverwriteResult confirmOverwriteFiles = OverwriteFileHelper.confirmOverwriteFiles(overwrittenFiles);
      if (OverwriteResult.CANCEL.equals(confirmOverwriteFiles)) {
        return;
      }
      boolean overwrite = OverwriteResult.DO_OVERWRITE.equals(confirmOverwriteFiles);
      Callable<Boolean> renameCallable = new RenameCallable(infos, manager, overwrite);
      projetService.deepModification(renameCallable, NbBundle.getMessage(RenameManagerNodeAction.class, "Log.RenameAction", name));
    } else {
      LogsDisplayer.displayError(logs, NbBundle.getMessage(getClass(), "RenameAction.Title"));
      CtuluLogGroup gr = new CtuluLogGroup(logs.getDefaultResourceBundle());
      gr.setDescription(logs.getDesc());
      projetService.updateLastLogsGroup(gr);
    }
  }

  private static class RenameCallable implements Callable<Boolean> {
    private final NewNameInformations infos;
    private final RenameManager manager;
    private final boolean overwriteExistingFile;

    public RenameCallable(NewNameInformations infos, RenameManager manager, boolean overwriteExistingFile) {
      this.infos = infos;
      this.manager = manager;
      this.overwriteExistingFile = overwriteExistingFile;
    }

    @Override
    public Boolean call() throws Exception {
      final boolean renamed = this.manager.rename(infos, overwriteExistingFile);
      return renamed;
    }
  }
}
