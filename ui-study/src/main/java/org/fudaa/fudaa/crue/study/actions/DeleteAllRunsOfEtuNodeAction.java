/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.ctulu.FileDeleteResult;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.projet.calcul.RunScenarioPair;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import java.util.Map;
import java.util.concurrent.Callable;

public final class DeleteAllRunsOfEtuNodeAction extends AbstractEditNodeAction {
  protected final transient  EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public DeleteAllRunsOfEtuNodeAction() {
    super(NbBundle.getMessage(DeleteAllRunsOfEtuNodeAction.class, "DeleteAllRunOfEtu.name"));
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    return true;
  }

  @Override
  protected boolean isEnableWithoutActivatedNodes() {
    return true;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  public static void deleteAllRunsOfEtu(final EMHProjetServiceImpl projetService) {
    final boolean ok = DialogHelper.showQuestion(NbBundle.getMessage(DeleteAllRunsOfEtuNodeAction.class, "DeleteAllRunOfEtu.name"),
        NbBundle.getMessage(DeleteAllRunsOfEtuNodeAction.class,
            "DeleteAllRunOfEtu.DialogMessage"));
    if (ok) {
      final Callable<Boolean> deleteCallable = () -> {
        final EMHProjetController controller = projetService.getController();
        final Map<RunScenarioPair, FileDeleteResult> notCleanScenarios = controller.deleteAllRunsOfEtu();
        if (!notCleanScenarios.isEmpty()) {
          DeleteEMHRunNodeAction.showInfoIfRunDirNotDeleted(notCleanScenarios, true);
        }
        return Boolean.TRUE;
      };
      projetService.deepModification(deleteCallable, org.openide.util.NbBundle.getMessage(DeleteAllRunsOfEtuNodeAction.class,
          "Log.DeleteAllRunsAction"));
    }
  }

  @Override
  protected void performAction(final Node[] activatedNodes) {
    deleteAllRunsOfEtu(projetService);
  }
}
