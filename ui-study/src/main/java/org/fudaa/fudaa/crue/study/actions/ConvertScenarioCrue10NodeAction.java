/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.openide.util.NbBundle;

public final class ConvertScenarioCrue10NodeAction extends AbstractConvertScenarioNodeAction {

  public ConvertScenarioCrue10NodeAction() {
    super(NbBundle.getMessage(ConvertScenarioCrue10NodeAction.class, "ConvertScenarioCrue10NodeAction.Name"), CrueVersionType.CRUE10);
  }
}
