/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.property;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.etude.EMHInfosVersion;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.fudaa.fudaa.crue.common.property.PropertyStringReadOnly;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.study.node.EMHProjetNode;
import org.fudaa.fudaa.crue.study.node.FichierCrueNode;
import org.fudaa.fudaa.crue.study.node.ManagerEMHScenarioNode;
import org.joda.time.LocalDateTime;
import org.openide.nodes.Node;
import org.openide.nodes.Node.Property;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 *
 * @author Fred Deniger
 */
public class ManagerPropertyFactory extends PropertyCrueUtils {

  public static final String AUTEUR_MODIFICATION = "AuteurModification.Property";
  public static final String AUTEUR_CREATION = "AuteurCreation.Property";
  public static final String DATE_MODIFICATION = "DateModification.Property";
  public static final String DATE_CREATION = "DateCreation.Property";
  public static final String ISACTIVE = "IsActive.Property";
  public static final String NOMBRE_RUNS = "NbRuns.Property";

  public static void createPropertyInfosVersions(final AbstractNodeFirable node, final EMHInfosVersion infosVersions, Set set) {
    //les infos

    try {
      //commentaire
      Property commentaireProp = new PropertyEMHInfosVersionCommentaire(node, infosVersions);
      configureNoEditAsText(commentaireProp);
      //auteur derniere modif
      Property auteurModifProp = new PropertySupport.Reflection(infosVersions, String.class, "getAuteurDerniereModif", null);
      setNameAndDisplayName(auteurModifProp, AUTEUR_MODIFICATION, ManagerPropertyFactory.class);
      auteurModifProp.setValue("nullValue", StringUtils.EMPTY);
      configureNoCustomEditor(auteurModifProp);
      //date dernier modif.
      PropertySupport.Reflection dateModifProp = new PropertySupport.Reflection(infosVersions, LocalDateTime.class,
              "getDateDerniereModif", null);
      dateModifProp.setPropertyEditorClass(PropertyEditorLocalDateTime.class);
      dateModifProp.setValue("nullValue", StringUtils.EMPTY);
      setNameAndDisplayName(dateModifProp, DATE_MODIFICATION, ManagerPropertyFactory.class);

      Property auteurCreationProp = new PropertySupport.Reflection(infosVersions, String.class, "getAuteurCreation", null);
      setNameAndDisplayName(auteurCreationProp, AUTEUR_CREATION, ManagerPropertyFactory.class);
      auteurCreationProp.setValue("nullValue", StringUtils.EMPTY);
      configureNoCustomEditor(auteurCreationProp);
      //date dernier modif.
      PropertySupport.Reflection dateCreationProp = new PropertySupport.Reflection(infosVersions, LocalDateTime.class,
              "getDateCreation", null);
      dateCreationProp.setPropertyEditorClass(PropertyEditorLocalDateTime.class);
      dateCreationProp.setValue("nullValue", StringUtils.EMPTY);
      setNameAndDisplayName(dateCreationProp, DATE_CREATION, ManagerPropertyFactory.class);


      set.put(commentaireProp);
      set.put(auteurModifProp);
      set.put(auteurCreationProp);
      set.put(dateModifProp);
      set.put(dateCreationProp);
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
  }

  public static Sheet createSheet(final ManagerEMHScenarioNode node, EMHProjet projet) {
    ManagerEMHScenario scenario = node.getLookup().lookup(ManagerEMHScenario.class);
    return createSheet(node, scenario, projet);
  }

  public static Sheet createProjectNodeSheet(final EMHProjetNode node) {
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    Property etuPath = new PropertyStringReadOnly(node.getEtuFile().getAbsolutePath(), "ETU_FILE.PROPERTY",
            NbBundle.getMessage(ManagerPropertyFactory.class, "EtuFile.Property"),
            NbBundle.getMessage(ManagerPropertyFactory.class, "EtuFile.Property.Description"));
    set.put(etuPath);
    Property grammarVersion = new PropertyStringReadOnly(node.getProject().getInfos().getXmlVersion(), "GRAMMAR.PROPERTY",
            NbBundle.getMessage(ManagerPropertyFactory.class, "Grammar.Property"),
            NbBundle.getMessage(ManagerPropertyFactory.class,
            "Grammar.Property.Description"));
    set.put(grammarVersion);
    Property etuDir = new PropertyStringReadOnly(node.getProject().getInfos().getDirOfFichiersEtudes().getAbsolutePath(),
            "ETUDES_DIR.PROPERTY",
            NbBundle.getMessage(ManagerPropertyFactory.class, "EtudesDir.Property"),
            NbBundle.getMessage(ManagerPropertyFactory.class,
            "EtudesDir.Property.Description"));
    set.put(etuDir);
    Property configDir = new PropertyStringReadOnly(node.getProject().getInfos().getDirOfConfig().getAbsolutePath(),
            "CONFIG_DIR.PROPERTY",
            NbBundle.getMessage(ManagerPropertyFactory.class, "ConfigDir.Property"),
            NbBundle.getMessage(ManagerPropertyFactory.class, "ConfigDir.Property.Description"));
    set.put(configDir);
    Property runDir = new PropertyStringReadOnly(node.getProject().getInfos().getDirOfRuns().getAbsolutePath(),
            "RUNS_DIR.PROPERTY",
            NbBundle.getMessage(ManagerPropertyFactory.class, "RunsDir.Property"),
            NbBundle.getMessage(ManagerPropertyFactory.class, "RunsDir.Property.Description"));
    set.put(runDir);
    Property rapportDir = new PropertyStringReadOnly(node.getProject().getInfos().getDirOfRapports().getAbsolutePath(),
            "RAPPORTS_DIR.PROPERTY",
            NbBundle.getMessage(ManagerPropertyFactory.class, "RapportsDir.Property"),
            NbBundle.getMessage(ManagerPropertyFactory.class,
            "RapportsDir.Property.Description"));
    set.put(rapportDir);

    createPropertyInfosVersions(node, node.getProject().getInfos().getInfosVersions(), set);
    sheet.put(set);
    return sheet;
  }

  public static Sheet createSheet(final AbstractNodeFirable node, EMHInfosVersion infos) {
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    createPropertyInfosVersions(node, infos, set);
    sheet.put(set);
    return sheet;
  }

  public static Sheet createSheet(final FichierCrueNode node, EMHProjet projet) {
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    set.put(new PropertyFichierCrueFile(node.getFichierCrue(), projet));
    set.put(new PropertyFichierCrueLastModified(node.getFichierCrue(), projet));
    PropertySupport.ReadOnly<Node> usedBy = new PropertyUsedBy(node);
    set.put(usedBy);
    sheet.put(set);
    return sheet;
  }

  public static Sheet createSheet(final File file) {
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    set.put(new PropertyFileAbsolutePath(file));
    set.put(new PropertyFileLastModified(file));
    sheet.put(set);
    return sheet;
  }

  public static Sheet createSheet(final AbstractNodeFirable node, final ManagerEMHContainerBase emhContainer,
          EMHProjet projet) {
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    try {
      final EMHInfosVersion infosVersions = emhContainer.getInfosVersions();
      Property isActiveProp = null;
      if (emhContainer.isCrue9()) {
        isActiveProp = new PropertySupport.Reflection(emhContainer, boolean.class, "isActive", null);
      } else {
        isActiveProp = new PropertySupportReflection(node, emhContainer, boolean.class, "isActive", "setActive");
      }
      setNameAndDisplayName(isActiveProp, ISACTIVE, ManagerPropertyFactory.class);
      isActiveProp.setPreferred(true);



      //type
      PropertySupport.Reflection typeProp = new PropertySupport.Reflection(infosVersions, String.class, "getType", null);
      configureNoCustomEditor(typeProp);
      setNameAndDisplayName(typeProp, TYPE_NAME);

      set.put(typeProp);
      set.put(isActiveProp);
      //scenario crue9 lié:
      if (projet.getCoeurConfig().isCrue9Dependant() && emhContainer.isCrue10() && emhContainer.getLevel().equals(CrueLevelType.SCENARIO) && !Crue10VersionConfig.
              isV1_1_1(
              projet.getCoeurConfig())) {
        PropertyScenarioCrue9Linked linkedScenario = new PropertyScenarioCrue9Linked(node, (ManagerEMHScenario) emhContainer,
                projet);
        set.put(linkedScenario);
      }
      PropertySupport.ReadOnly<Node> usedBy = new PropertyUsedBy(node);
      set.put(usedBy);
      createPropertyInfosVersions(node, infosVersions, set);

      // Pour un scénario, ajout de la propriété affichant le nombre de Run(s) contenu(s)
      if (emhContainer.getLevel().equals(CrueLevelType.SCENARIO)) {
        // création de la propriété qui sera affiché dans les propriétés du node
        PropertySupport.ReadOnly<Integer> propNbRun = new PropertySupport.ReadOnly(ManagerPropertyFactory.NOMBRE_RUNS, Integer.class, NbBundle.getMessage(ManagerPropertyFactory.class, ManagerPropertyFactory.NOMBRE_RUNS), NbBundle.getMessage(ManagerPropertyFactory.class, "NbRuns.ShortDescription")) {

          @Override
          public Integer getValue() throws IllegalAccessException, InvocationTargetException {
            return ((ManagerEMHScenario) emhContainer).getListeRuns().size();
          }
          
        };
        set.put(propNbRun);
      }

              
    } catch (Exception ex) {
      Exceptions.printStackTrace(ex);
    }
    sheet.put(set);
    return sheet;
  }

  public ManagerPropertyFactory() {
  }
}
