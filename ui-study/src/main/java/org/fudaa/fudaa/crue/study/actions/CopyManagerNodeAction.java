package org.fudaa.fudaa.crue.study.actions;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.dodico.crue.projet.copy.CopyInformations;
import org.fudaa.dodico.crue.projet.copy.ManagerCopyCallable;
import org.fudaa.dodico.crue.projet.rename.RenameManager;
import org.fudaa.dodico.crue.projet.rename.RenameManager.NewNameInformations;
import org.fudaa.dodico.crue.projet.select.SelectableFinder;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.fudaa.crue.common.action.MultipleSelectionNodeAction;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.fudaa.fudaa.crue.study.actions.OverwriteFileHelper.OverwriteResult;
import org.fudaa.fudaa.crue.study.dialog.RadicalInputNotifier;
import org.fudaa.fudaa.crue.study.node.AbstractManagerNode;
import org.fudaa.fudaa.crue.study.node.FichierCrueNode;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

public final class CopyManagerNodeAction extends MultipleSelectionNodeAction {
  protected final transient EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  private final transient ConnexionInformation connexionInfos = Lookup.getDefault().lookup(InstallationService.class).getConnexionInformation();

  public CopyManagerNodeAction() {
    super(NbBundle.getMessage(CopyManagerNodeAction.class, "CopyAction.Name"), true);
  }

  /**
   * @param init nom initiale
   * @param containerName le nom du container
   */
  public String chooseRenameRadical(String init, String containerName) {
    final String fieldName = NbBundle.getMessage(getClass(), "CopyAction.RadicalMessage");
    final String dialogTitle = getActionTitle(containerName);
    int maxTaille = ValidationPatternHelper.SPECIFIC_MAX_TAILLE_FOR_BLANK_SCENARIO;
    String maxTailleErrorMessage = ValidationPatternHelper.MESSAGE_ERROR_DEEP_COPY_NAME_TOO_LONG;
    RadicalInputNotifier input = new RadicalInputNotifier(fieldName, dialogTitle, init, false, maxTaille, maxTailleErrorMessage);
    if (DialogDisplayer.getDefault().notify(input) == NotifyDescriptor.OK_OPTION) {
      final String inputText = input.getInputText().trim();
      if (StringUtils.equals(inputText, init)) {
        return null;
      }
      return inputText;
    }
    return null;
  }

  private String getActionTitle(String containerName) {
    return NbBundle.getMessage(getClass(), "CopyAction.Title", containerName);
  }

  @Override
  protected boolean enable(Node activatedNodes) {
    return activatedNodes.canCopy();
  }

  @Override
  protected void performAction(Node activatedNodes) {
    final EMHProjet project = projetService.getSelectedProject();
    final RenameManager manager = new RenameManager();
    manager.setProject(project);

    CopyInformations copyInfos = null;
    String name = null;

    if (activatedNodes instanceof AbstractManagerNode<?>) {
      final ManagerEMHContainerBase container = ((AbstractManagerNode) activatedNodes).getManager();
      int res = JOptionPane.showOptionDialog(WindowManager.getDefault().getMainWindow(),
          NbBundle.getMessage(getClass(), "CopyAction.TypeMessage"),
          getActionTitle(container.getNom()), JOptionPane.YES_NO_CANCEL_OPTION,
          JOptionPane.QUESTION_MESSAGE, null, new String[]{NbBundle.getMessage(getClass(),
              "CopyAction.OneButton"), NbBundle.getMessage(
              getClass(), "CopyAction.DeepButton"), NbBundle.getMessage(getClass(), "CopyAction.CancelButton")},
          NbBundle.getMessage(getClass(), "CopyAction.OneButton"));

      if (res < 0 || res > 1) {
        return;
      }

      final boolean deep = res == 1;
      final NewNameInformations newNameInfos = this.chooseNewNames(container, manager, deep);

      if ((newNameInfos == null) || !this.testNewNameInformations(manager, newNameInfos)) {
        return;
      }

      copyInfos = new CopyInformations(container, newNameInfos, deep, project);
      name = container.getNom();
    } else if (activatedNodes instanceof FichierCrueNode) {
      final FichierCrue fichier = ((FichierCrueNode) activatedNodes).getFichierCrue();

      final NewNameInformations newNameInfos = this.getNewNames(fichier, manager);

      if ((newNameInfos == null) || !this.testNewNameInformations(manager, newNameInfos)) {
        return;
      }

      copyInfos = new CopyInformations(fichier, newNameInfos, project);

      name = fichier.getNom();
    }

    if (copyInfos != null) {
      List<String> overwrittenFiles = copyInfos.getOverwrittenFiles();
      boolean overwrite = false;
      if (!overwrittenFiles.isEmpty()) {
        OverwriteResult confirmOverwriteFiles = OverwriteFileHelper.confirmOverwriteFiles(overwrittenFiles);
        if (OverwriteResult.CANCEL.equals(confirmOverwriteFiles)) {
          return;
        }
        overwrite = OverwriteResult.DO_OVERWRITE.equals(confirmOverwriteFiles);
      }
      Callable<Boolean> copyCallable = new ManagerCopyCallable(copyInfos, overwrite, connexionInfos);
      projetService.deepModification(copyCallable, NbBundle.getMessage(RenameManagerNodeAction.class, "Log.CopyAction", name));
    }
  }

  private boolean testNewNameInformations(RenameManager manager, NewNameInformations infos) {
    final CtuluLog logs = manager.canUseNewNames(infos);

    if (logs.containsErrorOrSevereError()) {
      LogsDisplayer.displayError(logs, NbBundle.getMessage(getClass(), "CopyAction.Title"));
      return false;
    }

    return true;
  }

  private NewNameInformations chooseNewNames(ManagerEMHContainerBase container, RenameManager manager, boolean deep) {
    String init = StringUtils.removeStart(container.getNom(), container.getLevel().getPrefix());
    final String radical = chooseRenameRadical(init, container.getNom());
    if (radical == null) {
      return null;
    }

    SelectableFinder finder = new SelectableFinder();
    finder.setProject(manager.getProject());

    return manager.getNewNames(container, finder.getItems(Collections.singletonList(container), deep), radical, deep);
  }

  private NewNameInformations getNewNames(FichierCrue fichier, RenameManager manager) {
    String init = StringUtils.removeEnd(fichier.getNom(), "." + fichier.getType().getExtension());
    final String radical = chooseRenameRadical(init, fichier.getNom());
    if (radical == null) {
      return null;
    }
    return manager.getNewNames(fichier, radical);
  }
}
