/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.metier.etude.EMHInfosVersion;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.fudaa.crue.common.UserPreferencesSaver;
import org.fudaa.fudaa.crue.common.action.OneSelectionNodeAction;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.fudaa.fudaa.crue.study.dialog.ManageManagerEMHContainerDialog;
import org.fudaa.fudaa.crue.study.node.UsedByNodeBuilder;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

/**
 *
 * @author Chris
 */
public class ModifyModeleNodeAction extends OneSelectionNodeAction {

  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  private final ConnexionInformation connexionInfos = Lookup.getDefault().lookup(InstallationService.class).getConnexionInformation();

  public ModifyModeleNodeAction() {
    super(NbBundle.getMessage(CreateSousModeleNodeAction.class, "ModifyModeleAction.ActionName"), true);
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected boolean enable(Node activatedNode) {
    if (UsedByNodeBuilder.nodeIsUsedByScenarioWithRun(activatedNode)) {
      return false;
    }
    return activatedNode.getLookup().lookup(ManagerEMHModeleBase.class) != null;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected void performAction(Node activatedNodes) {
    final ManagerEMHModeleBase modele = activatedNodes.getLookup().lookup(ManagerEMHModeleBase.class);
    ManageManagerEMHContainerDialog dialog = new ManageManagerEMHContainerDialog(WindowManager.getDefault().getMainWindow(), true, modele, false, "modifierModele");

    dialog.setName(getClass().getName());
    UserPreferencesSaver.loadDialogLocationAndDimension(dialog, null);
    dialog.display();
    UserPreferencesSaver.saveLocationAndDimension(dialog, null);

    if (dialog.isOkClicked()) {
      final EMHInfosVersion infosVersion = modele.getInfosVersions();
      infosVersion.setAuteurCreation(connexionInfos.getCurrentUser());
      infosVersion.setDateCreation(connexionInfos.getCurrentDate());

      projetService.deepModification(new CallableTrue(), NbBundle.getMessage(getClass(), "ModifyModeleAction.Log", modele.getNom()));
    }
  }
}
