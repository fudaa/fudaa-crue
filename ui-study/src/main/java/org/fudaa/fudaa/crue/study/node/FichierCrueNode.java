package org.fudaa.fudaa.crue.study.node;

import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.study.actions.*;
import org.fudaa.fudaa.crue.study.property.ManagerPropertyFactory;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;

import javax.swing.*;
import java.awt.datatransfer.Transferable;
import java.io.File;
import java.io.IOException;

/**
 * @author Fred Deniger
 */
public class FichierCrueNode extends AbstractNode implements FilterableNode {
  final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public FichierCrueNode(FichierCrue fichier) {
    super(Children.LEAF, Lookups.singleton(fichier));
    setName(fichier.getNom());
    updateIcon();
    setValue("customDelete", Boolean.TRUE);
  }

  public FichierCrue getFichierCrue() {
    return getLookup().lookup(FichierCrue.class);
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getStudyFilesCtxId());
  }

  @Override
  public Action getPreferredAction() {
    return SystemAction.get(OpenFichierCrueNodeAction.class);
  }

  public Node getParentsTree() {
    return UsedByNodeBuilder.getParentsTree(this);
  }

  protected final void updateIcon() {
    boolean exists = fileExists();
    if (exists) {
      setIconBaseWithExtension(CrueIconsProvider.getIconBaseFile());
    } else {
      setIconBaseWithExtension(CrueIconsProvider.getIconBaseFileBroken());
    }
  }

  public boolean fileExists() {
    FichierCrue fichierCrue = getFichierCrue();
    final EMHProjet selectedProject = projetService.getSelectedProject();
    if (selectedProject == null) {
      return false;
    }
    final boolean exists = fichierCrue.getProjectFile(selectedProject).exists();
    return exists;
  }

  @Override
  protected Sheet createSheet() {
    return ManagerPropertyFactory.createSheet(this, projetService.getSelectedProject());
  }

  public File getFile() {
    FichierCrue lookup = getFichierCrue();
    return lookup.getProjectFile(projetService.getSelectedProject());
  }

  @Override
  public Action[] getActions(boolean context) {
    Action[] result = new Action[]{
        SystemAction.get(OpenFichierCrueNodeAction.class),
        null,
        SystemAction.get(ModifyFileNodeAction.class),
        SystemAction.get(RenameManagerNodeAction.class),
        SystemAction.get(CopyManagerNodeAction.class),
        null,
        SystemAction.get(DeleteFichierCrueNodeAction.class),
        null,
        SystemAction.get(ShowUsedByNodeAction.class)};
    return result;
  }

  @Override
  public boolean canDestroy() {
    return getParentsTree() == null;
  }

  @Override
  public void destroy() throws IOException {
    //ne doit rien faire
  }

  public boolean isModifiable() {
    return UsedByNodeBuilder.nodeIsNotUsedByScenarioWithRun(this);
  }

  @Override
  public boolean canRename() {
    return false;
  }

  @Override
  public boolean canCopy() {
    return true;
  }

  @Override
  public PasteType getDropType(Transferable t, int action, int index) {
    return this.getDropType(this.getParentNode(), t, action, index);
  }

  @Override
  public PasteType getDropType(Node parent, Transferable t, int action, int index) {
    final Node dragged = DefaultNodePasteType.getMovedNodeInTransferable(t);

    if (dragged instanceof FichierCrueNode) {
      return parent.getDropType(t, action, index);
    }

    return null;
  }
}
