/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study;

import org.fudaa.fudaa.crue.study.node.NodesManager;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;

import java.beans.PropertyChangeEvent;

/**
 * @author Fred Deniger
 */
public class MainNodeListener implements NodeListener {
  public interface Target {
    void buildContent();
  }

  private final Target target;
  private final NodesManager manager;

  public MainNodeListener(final Target target, final NodesManager manager) {
    this.target = target;
    this.manager = manager;
  }

  public void unregister() {
    manager.removeNodeListener(this);
  }

  public void register() {
    manager.addNodeListener(this);
  }

  @Override
  public void propertyChange(final PropertyChangeEvent evt) {
  }

  @Override
  public void childrenAdded(final NodeMemberEvent ev) {
    target.buildContent();
  }

  @Override
  public void childrenRemoved(final NodeMemberEvent ev) {
    target.buildContent();
  }

  @Override
  public void childrenReordered(final NodeReorderEvent ev) {
  }

  @Override
  public void nodeDestroyed(final NodeEvent ev) {
  }
}
