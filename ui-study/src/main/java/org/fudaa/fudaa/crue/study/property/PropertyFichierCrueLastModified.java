package org.fudaa.fudaa.crue.study.property;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.openide.nodes.PropertySupport.ReadOnly;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class PropertyFichierCrueLastModified extends ReadOnly {

  private final FichierCrue fichierCrue;
  private final EMHProjet projet;

  public PropertyFichierCrueLastModified(final FichierCrue fichierCrue, EMHProjet projet) {
    super("LastModified", String.class, NbBundle.getMessage(ManagerPropertyFactory.class, "File.LastModified.Property"), org.openide.util.NbBundle.getMessage(ManagerPropertyFactory.class, "File.LastModified.Property.Description"));
    this.fichierCrue = fichierCrue;
    this.projet = projet;
  }

  @Override
  public Object getValue() throws IllegalAccessException, InvocationTargetException {
    File f = fichierCrue.getProjectFile(projet);
    if (f == null) {
      return StringUtils.EMPTY;
    }
    return PropertyEditorLocalDateTime.SHORT_DATE_TIME.print(f.lastModified());
  }
}
