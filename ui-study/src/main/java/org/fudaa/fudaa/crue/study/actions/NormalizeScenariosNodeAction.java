/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.projet.ProjectNormalizableCallable;
import org.fudaa.dodico.crue.projet.clean.EMHProjectCleanerCallableAdapter;
import org.fudaa.dodico.crue.projet.clean.ScenarioCleanData;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.loader.action.CleanProjectUiSelector;
import org.fudaa.fudaa.crue.loader.action.ProjectFindScenarioToNormalizeProgressRunnable;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import java.util.List;

/**
 * Action permettant de normaliser les scenarios d'une etude
 */
public final class NormalizeScenariosNodeAction extends AbstractEditNodeAction {
  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public NormalizeScenariosNodeAction() {
    super(NbBundle.getMessage(NormalizeScenariosNodeAction.class, "NormalizeScenariosAction.Name"));
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return true;
  }

  @Override
  protected boolean isEnableWithoutActivatedNodes() {
    return true;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  /**
   * Ouvre tous les scenarios et ceux qui doivent être normalisés seront sauvegardés après validation par l'utilisateur.
   *
   * @param projetService
   */
  public static void normalizeAllScenarios(final EMHProjetServiceImpl projetService) {

    //recherche des scenarios a normaliser
    ProjectFindScenarioToNormalizeProgressRunnable runnable = new ProjectFindScenarioToNormalizeProgressRunnable(projetService.getSelectedProject());

    List<ProjectNormalizableCallable.Result> scenarioToNormalize = CrueProgressUtils
        .showProgressDialogAndRun(runnable, NbBundle.getMessage(NormalizeScenariosNodeAction.class, "NormalizeScenariosAction.Search"));

    final String actionTitle = NbBundle.getMessage(NormalizeScenariosNodeAction.class, "NormalizeScenariosAction.Name");
    //si aucun message simple à l'utilisateur
    if (scenarioToNormalize.isEmpty()) {
      DialogHelper.showNotifyOperationTermine(actionTitle,
          NbBundle.getMessage(NormalizeScenariosNodeAction.class, "NoScenarioToNormalizeFound")
      );
      return;
    }

    //interface pour l'utilisateur pour choisir les scenarios à normaliser
    CleanProjectUiSelector selector = new CleanProjectUiSelector();
    ScenarioCleanData data = new ScenarioCleanData();
    data.scenarioToNormalizeOrWithErrors.addAll(scenarioToNormalize);
    data = selector.select(data, actionTitle, NbBundle.getMessage(DeleteDuplicatedScenarioNodeAction.class, "NormalizeScenariosAction.Yes"));

    //action
    if (data != null && data.isNotEmpty()) {
      final EMHProjectCleanerCallableAdapter action = new EMHProjectCleanerCallableAdapter(projetService.getController(), projetService.getEtuFile(), data, false);
      //on lance le tout dans un thread à part
      final String progressTitle = NbBundle.getMessage(DeleteDuplicatedScenarioNodeAction.class, "NormalizeScenariosAction.Progress");
      CrueProgressUtils.showProgressDialogAndRun(progressHandle -> {
            progressHandle.switchToIndeterminate();
            projetService.deepModification(
                action,
                NbBundle.getMessage(NormalizeScenariosNodeAction.class, "NormalizeScenariosAction.Progress"));
            return Boolean.TRUE;
          },
          progressTitle);

      //affichage des logs si presents.
      if (action.getLogs().containsFatalError()) {
        LogsDisplayer.displayError(action.getLogs(), progressTitle);
        projetService.updateLastLogsGroup(action.getLogs());
      }
    }
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    normalizeAllScenarios(projetService);
  }
}
