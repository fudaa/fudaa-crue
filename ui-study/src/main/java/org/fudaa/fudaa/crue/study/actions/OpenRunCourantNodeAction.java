/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.services.PostRunService;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;

public final class OpenRunCourantNodeAction extends NodeAction {

  final PostRunService postService = Lookup.getDefault().lookup(PostRunService.class);
  final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  protected EMHRun getRunCourant(Node[] activatedNodes) {
    ManagerEMHScenario scenario = OpenScenarioNodeAction.getScenarioFromNodes(activatedNodes);
    return scenario == null ? null : scenario.getRunCourant();
  }

  @Override
  protected boolean enable(Node[] activatedNodes) {
    if (projetService.getSelectedProject() != null) {
      EMHRun run = getRunCourant(activatedNodes);
      return run != null && run != postService.getRunLoaded();
    }
    return false;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    postService.loadRun(projetService.getSelectedProject(), OpenScenarioNodeAction.getScenarioFromNodes(activatedNodes),
                        getRunCourant(activatedNodes));
  }

  @Override
  public String getName() {
    return NbBundle.getMessage(OpenFichierCrueNodeAction.class, "OpenRunCourantAction.Name");
  }
}
