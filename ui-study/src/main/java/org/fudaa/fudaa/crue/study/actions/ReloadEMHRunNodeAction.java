/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import java.util.concurrent.Callable;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.fudaa.crue.common.action.OneSelectionNodeAction;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.study.node.EMHRunNode;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public final class ReloadEMHRunNodeAction extends OneSelectionNodeAction {

  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public ReloadEMHRunNodeAction() {
    super(NbBundle.getMessage(ReloadEMHRunNodeAction.class, "ReloadEMHRun.Name"), true);
  }

  @Override
  protected boolean enable(Node node) {
    return node instanceof EMHRunNode;
  }

  @Override
  protected void performAction(final Node node) {
    if (!(node instanceof EMHRunNode)) {
      return;
    }
    boolean hasRunCourant = ((EMHRunNode) node).getParent().getRunCourant() != null;
    String msg = hasRunCourant ? "ReloadEMHRunNode.CurrentRun.DialogMessage" : "ReloadEMHRunNode.NoCurrentRun.DialogMessage";
    String ok = NbBundle.getMessage(ReloadEMHRunNodeAction.class, "Action.Overwrite");
    String no = NbBundle.getMessage(ReloadEMHRunNodeAction.class, "Action.Cancel");
    boolean accepted = ok.equals(DialogHelper.showQuestion(getName(), NbBundle.getMessage(ReloadEMHRunNodeAction.class, msg), new Object[]{ok, no}));
    if (!accepted) {
      return;
    }
    Callable<Boolean> deleteCallable = new Callable<Boolean>() {

      @Override
      public Boolean call() throws Exception {
        EMHRunNode runNode = (EMHRunNode) node;
        EMHProjetController controller = projetService.getController();
        CrueIOResu<Boolean> reloadRun = controller.reloadRun(runNode.getParent(), runNode.getRun());
        if (Boolean.FALSE.equals(reloadRun.getMetier())) {
          LogsDisplayer.displayError(reloadRun.getAnalyse(), NbBundle.getMessage(ReloadEMHRunNodeAction.class, "ReloadEMHRunNode.ActionDescription", node.getName()));
        }
        return reloadRun.getMetier();
      }
    };
    projetService.deepModification(deleteCallable, NbBundle.getMessage(ReloadEMHRunNodeAction.class, "ReloadEMHRunNode.ActionDescription", node.getName()));
  }
}
