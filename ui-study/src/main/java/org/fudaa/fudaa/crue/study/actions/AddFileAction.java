/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.EMHProjectInfos;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.fudaa.crue.common.CrueFileTypeFilter;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.helper.CrueFileChooserBuilder;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.study.dialog.FileDialog;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * @author Chris
 */
public class AddFileAction extends AbstractEditNodeAction {
  private  final transient ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);
  protected final transient EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public AddFileAction() {
    super(NbBundle.getMessage(AddFileAction.class, "AddFileAction.ActionName"));
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    return true;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected void performAction(final Node[] activatedNodes) {
    final File home = configurationManagerService.getDefaultDataHome();
    final CrueFileChooserBuilder builder = new CrueFileChooserBuilder(getClass()).setTitle(NbBundle.getMessage(getClass(), "AddFileAction.Title")).setDefaultWorkingDirectory(home)
        .setApproveText(NbBundle.getMessage(getClass(), "AddFileAction.AddButton"));
    final List<CrueFileType> types = new ArrayList<>();

    for (final CrueFileType type : CrueFileType.values()) {
      if (!type.isResultFileType() && !type.isProjetLevel()) {
        types.add(type);
      }
    }

    builder.setFileFilter(new CrueFileTypeFilter(types));
    builder.setSelectionApprover(new NewFileSelectionApprover());

    final File[] selectedFiles = builder.showMultiOpenDialog();

    if ((selectedFiles != null) && (selectedFiles.length > 0)) {
      final EMHProjectInfos infos = projetService.getSelectedProject().getInfos();
      final Set<File> toAdds = new HashSet<>();

      for (final File file : selectedFiles) {
        if (!infos.existFileInBase(file.getName())) {
          toAdds.add(file);
        }
      }

      if (toAdds.size() < selectedFiles.length) {
        JOptionPane.showMessageDialog(WindowManager.getDefault().getMainWindow(), NbBundle.getMessage(getClass(), "AddFileAction.NotInStudyFiles"),
            NbBundle.getMessage(getClass(), "AddFileAction.Title"), JOptionPane.INFORMATION_MESSAGE);
      }

      if (toAdds.isEmpty()) {
        JOptionPane.showMessageDialog(WindowManager.getDefault().getMainWindow(), NbBundle.getMessage(getClass(), "AddFileAction.NoFilesToAdd"),
            NbBundle.getMessage(getClass(), "AddFileAction.Title"), JOptionPane.INFORMATION_MESSAGE);
      } else {
        final Callable<Boolean> callable = () -> {

          for (final File toAdd : toAdds) {
            final String fileName = toAdd.getName();
            final File parentDir = toAdd.getParentFile();
            //on recherche le path relatif
            final String relativeFile = FileDialog.findPath(parentDir, projetService);
            final FichierCrue file = new FichierCrue(fileName, relativeFile, CrueFileType.getByExtension(fileName));
            if (!infos.addCrueFileToProject(file)) {
              return false;
            }
          }
          return true;
        };

        final StringBuilder msg = new StringBuilder(NbBundle.getMessage(getClass(), "AddFileAction.AddFiles"));
        boolean isFirst = true;

        for (final File toAdd : toAdds) {
          if (!isFirst) {
            msg.append(", ");
          }
          msg.append(toAdd.getAbsolutePath());
          isFirst = false;
        }

        projetService.deepModification(callable, msg.toString());
      }
    }
  }

  private class NewFileSelectionApprover implements CrueFileChooserBuilder.SelectionApprover {
    @Override
    public boolean approve(final File[] files) {
      if (files.length < 1) {
        return false;
      }

      final List<File> invalidFiles = new ArrayList<>();
      final List<File> invalidOptrFiles = new ArrayList<>();

      for (final File file : files) {
        final String name = file.getName();
        final CrueFileType type = CrueFileType.getByExtension(name);

        if (!file.isFile() || (type == null) || !ValidationPatternHelper.isFilenameValide(name, type)) {
          invalidFiles.add(file);
        } else if (CrueFileType.OPTR.equals(type) && !projetService.isOptrAccepted()) {
          invalidOptrFiles.add(file);
        }
      }

      if (invalidFiles.size() > 0) {
        final StringBuilder invalidMsg = new StringBuilder(NbBundle.getMessage(getClass(), "AddFileAction.InvalidFiles"));

        for (final File file : invalidFiles) {
          invalidMsg.append("\n");
          invalidMsg.append(file.getName());
        }

        DialogHelper.showError(invalidMsg.toString());
        return false;
      }
      if (invalidOptrFiles.size() > 0) {
        DialogHelper.showError(NbBundle.getMessage(AddFileAction.class, "AddFileAction,OptrNotSupported"));
        return false;
      }

      return true;
    }

    @Override
    public boolean approve(final File selection) {
      if (!selection.exists()) {
        JOptionPane.showMessageDialog(WindowManager.getDefault().getMainWindow(), NbBundle.getMessage(AddFileAction.class, "AddFileAction.InvalidFile", selection.getName()),
            NbBundle.getMessage(getClass(), "AddFileAction.Title"), JOptionPane.ERROR_MESSAGE);
        return false;
      }
      return true;
    }
  }
}
