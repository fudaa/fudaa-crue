/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.concurrent.Callable;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.FileDeleteResult;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.projet.calcul.RunScenarioPair;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.NodeToStringTransformer;
import org.fudaa.fudaa.crue.study.node.ManagerEMHScenarioNode;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public final class DeleteAllRunsNodeAction extends AbstractEditNodeAction {

  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public DeleteAllRunsNodeAction() {
    super(NbBundle.getMessage(DeleteAllRunsNodeAction.class, "DeleteAllRun.name"));
  }

  public boolean containsRuns(final ManagerEMHContainerBase manager) {
    return manager instanceof ManagerEMHScenario && CollectionUtils.isNotEmpty(((ManagerEMHScenario) manager).getListeRuns());
  }

  public String getDialogTitleI18n(final boolean single, final String name) throws MissingResourceException {
    return NbBundle.getMessage(getClass(), single ? "DeleteAction.AllRun.DialogTitle" : "DeleteAction.AllRunMulti.DialogTitle",
            name);
  }

  public boolean isNodeScenarioWithRun(final Node node) {
    return node instanceof ManagerEMHScenarioNode && ((ManagerEMHScenarioNode) node).getManagerEMHScenario().hasRun();
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    if (activatedNodes != null) {
      for (final Node node : activatedNodes) {
        if (isNodeScenarioWithRun(node)) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected void performAction(final Node[] activatedNodes) {
    final List<ManagerEMHScenarioNode> toBeDeleted = new ArrayList<>();
    if (activatedNodes != null) {
      for (final Node node : activatedNodes) {
        if (isNodeScenarioWithRun(node)) {
          toBeDeleted.add((ManagerEMHScenarioNode) node);
        }
      }
    }
    delete(toBeDeleted);
  }

  public boolean userCancelAction(final boolean single, final String name) {
    final String dialogContent = single ? "DeleteAction.AllRuns.DialogContent" : "DeleteAction.AllRunsMulti.DialogContent";
    final String dialogTitle = getDialogTitleI18n(single, name);

    return !DialogHelper.showQuestion(dialogTitle,
            NbBundle.getMessage(DeleteAllRunsNodeAction.class, dialogContent, name));
  }

  protected void delete(final List<ManagerEMHScenarioNode> nodes) {

    final boolean isSingle = nodes.size() == 1;

    final String name = isSingle ? nodes.get(0).getDisplayName() : Integer.toString(nodes.size());
    if (userCancelAction(isSingle, name)) {
      return;
    }
    final Callable<Boolean> deleteCallable = () -> {
      final EMHProjetController controller = projetService.getController();
      final Map<RunScenarioPair, FileDeleteResult> notCleanScenarios = new HashMap<>();
      for (final ManagerEMHScenarioNode nodeToDelete : nodes) {
        controller.deleteAllRuns(nodeToDelete.getManager(), notCleanScenarios);
      }
      if (!notCleanScenarios.isEmpty()) {
        DeleteEMHRunNodeAction.showInfoIfRunDirNotDeleted(notCleanScenarios, true);
      }

      return Boolean.TRUE;
    };
    final String logEntry = ToStringHelper.transformTo(nodes, new NodeToStringTransformer());

    projetService.deepModification(deleteCallable, org.openide.util.NbBundle.getMessage(DeleteAllRunsNodeAction.class,
            "Log.DeleteAllRunsAction", logEntry));
  }
}
