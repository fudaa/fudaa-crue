/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study;

import javax.swing.ActionMap;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.fudaa.crue.common.AbstractTopComponent;
import org.fudaa.fudaa.crue.common.view.ExpandedNodesManager;
import org.fudaa.fudaa.crue.study.node.NodesManager;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.fudaa.fudaa.crue.study.services.ProjectState;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

/**
 *
 * @author Fred Deniger
 */
public abstract class ListTopComponentAbstract<T extends ObjetNomme> extends AbstractTopComponent implements ExplorerManager.Provider, MainNodeListener.Target, LookupListener {

  private final ExplorerManager em = new ExplorerManager();
  protected final EMHProjetServiceImpl service = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  AbstractSelectionListener listenerForSelection;
  Result<ProjectState> lookupResult;
  ExpandedNodesManager expandedNodeMemory;

  public ListTopComponentAbstract(Class classOfSelectedObject) {
    ActionMap map = this.getActionMap();
    map.put("delete", ExplorerUtils.actionDelete(em, true)); //NOI18N
    associateLookup(ExplorerUtils.createLookup(em, map));
  }

  @Override
  public final void resultChanged(LookupEvent ev) {
    if (service.isReloading()) {
      return;
    }
    if (service.getSelectedProject() != null) {
      createSelectionListener();
    } else if (listenerForSelection != null) {
      listenerForSelection.clean();
      listenerForSelection = null;
    }
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return em;
  }
  private MainNodeListener listener;

  @Override
  public void componentOpened() {
    if (lookupResult == null) {
      lookupResult = service.getLookup().lookupResult(ProjectState.class);
      lookupResult.addLookupListener(this);
      resultChanged(null);
    }
    if (listener == null) {
      listener = new MainNodeListener(this, getNodeManager());
      listener.register();
    }
    if (this.expandedNodeMemory != null && expandedNodeMemory.isNotEmpty()) {
      expandedNodeMemory.restoreState();
    }
  }

  @Override
  public void componentClosedTemporarily() {
    saveExpandedNodes();
  }

  @Override
  public void componentClosedDefinitly() {
    if (lookupResult != null) {
      lookupResult.removeLookupListener(this);
      lookupResult = null;
      em.setRootContext(Node.EMPTY);
    }
  }

  protected void createSelectionListener() {
    if (listenerForSelection == null) {
      listenerForSelection = new ManagerSelectionListener(ListTopComponentAbstract.this);
      listenerForSelection.register();
    }
  }

  protected NodesManager getNodeManager() {
    return service.getNodesManager();
  }

  @Override
  protected void componentActivated() {
    super.componentActivated();
    ExplorerUtils.activateActions(em, true);
  }

  @Override
  protected void componentDeactivated() {
    super.componentDeactivated();
    ExplorerUtils.activateActions(em, false);
  }

  protected abstract Node getNode(NodesManager nodesManager);

  @Override
  public void buildContent() {
    em.setRootContext(getNode(getNodeManager()));
  }

  public final void saveExpandedNodes() {
    if (expandedNodeMemory != null) {
      expandedNodeMemory.saveState();
    }
  }

  public final void restoreExpandedNodes() {
    if (expandedNodeMemory != null && expandedNodeMemory.isNotEmpty()) {
      expandedNodeMemory.restoreState();
    }
  }
}
