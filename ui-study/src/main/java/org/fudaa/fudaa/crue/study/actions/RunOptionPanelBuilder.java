package org.fudaa.fudaa.crue.study.actions;

import com.memoire.bu.BuGridLayout;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.create.RunCalculOption;
import org.fudaa.dodico.crue.projet.create.RunCreatorOptions;
import org.fudaa.fudaa.crue.common.helper.ToStringTransfomerCellRenderer;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import static org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder.getDialogHelpCtxId;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class RunOptionPanelBuilder {
  
  private final ManagerEMHScenario scenario;
  private JPanel pn;
  private JComboBox runs;
  private Map<CrueFileType, JComboBox> options;
  
  public RunOptionPanelBuilder(ManagerEMHScenario scenario) {
    this.scenario = scenario;
  }
  
  private static class OptionToStringTransformer implements ToStringTransformer {
    
    @Override
    public String transform(Object in) {
      return ((RunCalculOption) in).geti18n();
    }
  }
  
  private JPanel createPanel() {
    if (pn != null) {
      return pn;
    }
    pn = new JPanel();
    pn.setLayout(new BorderLayout(3, 5));
    pn.add(new JLabel(NbBundle.getMessage(RunOptionPanelBuilder.class, "RunOption.SourceFromStudy")), BorderLayout.NORTH);
    JPanel pnOption = new JPanel(new BuGridLayout(2,5,5));
    List<EMHRun> listeRuns = scenario.getListeRuns();
    if (CollectionUtils.isNotEmpty(listeRuns)) {
      List objects = new ArrayList();
      objects.add(null);//pour aucun
      objects.addAll(listeRuns);
      runs = new JComboBox(objects.toArray(new Object[0]));
      if (scenario.getRunCourant() != null) {
        runs.setSelectedItem(scenario.getRunCourant());
      } else {
        runs.setSelectedIndex(objects.size() - 1);
      }
      pnOption.add(new JLabel(org.openide.util.NbBundle.getMessage(RunOptionPanelBuilder.class, "RunOption.RunSource")));
      pnOption.add(runs);
    } 
    if (!scenario.isCrue9()) {
      options = new EnumMap<>(CrueFileType.class);
      List<CrueFileType> runFileType = RunCreatorOptions.getRunFileType();
      Map<CrueFileType, RunCalculOption> defaultOptions = RunCreatorOptions.createDefaultMap();
      
      Object[] runOptions = RunCalculOption.values();
      OptionToStringTransformer toString = new OptionToStringTransformer();
      for (CrueFileType crueFileType : runFileType) {
        JComboBox cb = new JComboBox(runOptions);
        cb.setRenderer(new ToStringTransfomerCellRenderer(toString));
        cb.setSelectedItem(defaultOptions.get(crueFileType));
        options.put(crueFileType, cb);
        
        pnOption.add(new JLabel(RunCreatorOptions.geti18n(crueFileType)));
        pnOption.add(cb);
      }
      pn.add(pnOption, BorderLayout.CENTER);
    }
    String id = getDialogHelpCtxId("bdlLancerRunOptions", null);
    SysdocUrlBuilder.installHelpShortcut(pn,id);    
    return pn;
  }
  
  public RunCreatorOptions getOptions() {
    JPanel pn = createPanel();
    NotifyDescriptor nd = new NotifyDescriptor.Confirmation(pn, org.openide.util.NbBundle.getMessage(RunOptionPanelBuilder.class, "LaunchRunWithOption.DialogTitle", scenario.getNom()));
    final String yesOption = org.openide.util.NbBundle.getMessage(RunOptionPanelBuilder.class, "RunAction.AcceptOptions");
    nd.setOptions(new Object[]{yesOption, NotifyDescriptor.CANCEL_OPTION});
    final Object notify = DialogDisplayer.getDefault().notify(nd);
    boolean equals = notify.equals(yesOption);
    if (!equals) {
      return null;
    }
    EMHRun run = (EMHRun) (runs == null ? null : runs.getSelectedItem());
    RunCreatorOptions res = new RunCreatorOptions(run);
    if (!scenario.isCrue9()) {
      Set<Entry<CrueFileType, JComboBox>> entrySet = options.entrySet();
      for (Entry<CrueFileType, JComboBox> entry : entrySet) {
        res.setCompute(entry.getKey(), (RunCalculOption) entry.getValue().getSelectedItem());
      }
    }
    return res;
  }
}
