/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.node;

import java.awt.datatransfer.Transferable;
import org.openide.nodes.Node;
import org.openide.util.datatransfer.PasteType;

/**
 *
 * @author Chris
 */
public interface FilterableNode {
  PasteType getDropType(Node parent, Transferable t, int action, int index);
}
