package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.metier.etude.EMHInfosVersion;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.fudaa.fudaa.crue.study.dialog.ManageManagerEMHContainerDialog;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

/**
 *
 * @author Chris
 */
public class CreateScenarioNodeAction extends AbstractEditNodeAction {

  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  private final ConnexionInformation connexionInfos = Lookup.getDefault().lookup(InstallationService.class).getConnexionInformation();

  public CreateScenarioNodeAction() {
    super(NbBundle.getMessage(CreateScenarioNodeAction.class, "CreateScenarioAction.ActionName"));
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return true;
  }

  @Override
  protected boolean isEnableWithoutActivatedNodes() {
    return true;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    final ManagerEMHScenario scenario = new ManagerEMHScenario();
    final EMHInfosVersion infosVersion = new EMHInfosVersion();
    infosVersion.setVersion(CrueVersionType.CRUE10);
    infosVersion.setAuteurCreation(connexionInfos.getCurrentUser());
    infosVersion.setDateCreation(connexionInfos.getCurrentDate());
    infosVersion.setAuteurDerniereModif(infosVersion.getAuteurCreation());
    infosVersion.setDateDerniereModif(infosVersion.getDateCreation());
    infosVersion.setCommentaire("");

    scenario.setInfosVersions(infosVersion);

    ManageManagerEMHContainerDialog dialog = new ManageManagerEMHContainerDialog(WindowManager.getDefault().getMainWindow(), true, scenario, true, "creerScenarioCrue10");
    dialog.display();

    if (dialog.isOkClicked()) {
      projetService.deepModification(new CreateContainerCallable(scenario, projetService.getSelectedProject(), false, false), NbBundle.getMessage(getClass(), "CreateScenarioAction.Log", scenario.getNom()));
    }
  }
}
