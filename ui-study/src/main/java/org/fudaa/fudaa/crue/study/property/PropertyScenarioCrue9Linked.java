package org.fudaa.fudaa.crue.study.property;

import java.beans.PropertyEditor;
import java.beans.PropertyEditorSupport;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertySupportReadWrite;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class PropertyScenarioCrue9Linked extends PropertySupportReadWrite<ManagerEMHScenario, String> {

  public static final String LINKED_SCENARIO_CRUE9 = "LinkedScenarioCrue9.Property";
  private final EMHProjet projet;

  public PropertyScenarioCrue9Linked(final AbstractNodeFirable node, ManagerEMHScenario instance, EMHProjet projet) {
    super(node, instance, String.class, LINKED_SCENARIO_CRUE9,
          NbBundle.getMessage(PropertyScenarioCrue9Linked.class, LINKED_SCENARIO_CRUE9), null);
    setValue("nullValue", StringUtils.EMPTY);
    this.projet = projet;
  }

  @Override
  public String getValue() throws IllegalAccessException, InvocationTargetException {
    return getInstance().getLinkedscenarioCrue9();
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    if (!canWrite()) {
      return super.getPropertyEditor();
    }
    return new ScenarioCrue9Editor(projet);
  }

  @Override
  protected void setValueInInstance(String newVal) {
    getInstance().setLinkedscenarioCrue9(StringUtils.isEmpty(newVal) ? null : newVal);
  }

  public static class ScenarioCrue9Editor extends PropertyEditorSupport {

    final String none = NbBundle.getMessage(PropertyScenarioCrue9Linked.class, "scenarioCrue9Linked.noScenarioChoice");
    private final EMHProjet projet;

    public ScenarioCrue9Editor(EMHProjet projet) {
      this.projet = projet;
    }

    @Override
    public String[] getTags() {
      List<ManagerEMHScenario> listeScenarios = projet.getListeScenarios();
      List<String> crue9Scenarios = new ArrayList<>();
      for (ManagerEMHScenario managerEMHScenario : listeScenarios) {
        if (managerEMHScenario.isCrue9()) {
          crue9Scenarios.add(managerEMHScenario.getNom());
        }
      }
      if (crue9Scenarios.isEmpty()) {
        return new String[]{""};
      }
      Collections.sort(crue9Scenarios);
      crue9Scenarios.add(0, none);
      return crue9Scenarios.toArray(new String[0]);

    }

    @Override
    public String getAsText() {
      return super.getAsText();
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
      setValue((StringUtils.isEmpty(text) || none.equals(text)) ? StringUtils.EMPTY : text);
    }
  }
}
