/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.services;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.fudaa.crue.common.property.listener.UnregisterableListener;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileRenameEvent;

/**
 *
 * @author Fred Deniger
 */
class FileChangeListenerEtudeDir implements FileChangeListener, UnregisterableListener {

  private final EMHProjetServiceImpl parent;

  public FileChangeListenerEtudeDir(final EMHProjetServiceImpl parent) {
    this.parent = parent;
  }

  @Override
  public void register() {
    parent.getEtuFolder().addRecursiveListener(this);
  }

  @Override
  public void unregister() {
    try{
    parent.getEtuFolder().removeRecursiveListener(this);
    }
    catch(Exception e){
      Logger.getLogger(FileChangeListenerEtudeDir.class.getName()).log(Level.WARNING, "message {0}", e);
    }
  }

  @Override
  public void fileFolderCreated(FileEvent fe) {
    parent.updateEtuFiles();
  }

  @Override
  public void fileDataCreated(FileEvent fe) {
    parent.updateEtuFiles();
  }

  @Override
  public void fileChanged(FileEvent fe) {
    parent.updateEtuFiles();
  }

  @Override
  public void fileDeleted(FileEvent fe) {
    parent.updateEtuFiles();
  }

  @Override
  public void fileRenamed(FileRenameEvent fe) {
    parent.updateEtuFiles();
  }

  @Override
  public void fileAttributeChanged(FileAttributeEvent fe) {
    parent.updateEtuFiles();
  }
}
