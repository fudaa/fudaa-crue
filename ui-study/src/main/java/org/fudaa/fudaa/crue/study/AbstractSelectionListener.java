package org.fudaa.fudaa.crue.study;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.util.*;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.fudaa.crue.study.node.ChildrenKeysAbstract;
import org.fudaa.fudaa.crue.study.perspective.PerspectiveServiceStudy;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 *
 * Un listener utilisé par tous les topComponents afin de mettre à jour la sélection courante. Il ecoute tous les TopComponent et
 * leur selection afin de mettre à jour la sélection du component passé en paramètre.
 *
 * @author deniger
 */
public abstract class AbstractSelectionListener implements LookupListener, PropertyChangeListener {

  public static final String SELECTION_CHANGE_IN_TOPCOMPONENT = "SELECTIONCHANGE";
  /**
   * liste des container selectionnés par id de TopComponent.
   */
  private final Map<String, Lookup.Result<ManagerEMHContainerBase>> selectedContainerById = new HashMap<>();
  /**
   * liste des fichiers selectionnés par id de TopComponent.
   */
  final Map<String, Lookup.Result<FichierCrue>> selectedFileById = new HashMap<>();
  final PerspectiveServiceStudy perspectiveServiceStudy = Lookup.getDefault().lookup(PerspectiveServiceStudy.class);
  private final ListTopComponentAbstract component;
  private final boolean listenToFile;

  public AbstractSelectionListener(final ListTopComponentAbstract component, boolean listenToFiles) {
    this.component = component;
    this.listenToFile = listenToFiles;
  }

  public void register() {
    Set<String> topComponents = perspectiveServiceStudy.getDefaultTopComponents();
    WindowManager.getDefault().getRegistry().addPropertyChangeListener(this);
    for (String id : topComponents) {
      if (StudyPropertiesTopComponent.TOPCOMPONENT_ID.equals(id)) {
        continue;
      }
      TopComponent findTopComponent = WindowManager.getDefault().findTopComponent(id);
      if (findTopComponent == component) {
        continue;
      }
      if (findTopComponent instanceof ExplorerManager.Provider) {

        Result<ManagerEMHContainerBase> lookupResult = findTopComponent.getLookup().lookupResult(ManagerEMHContainerBase.class);
        selectedContainerById.put(id, lookupResult);
        if (listenToFile) {
          Result<FichierCrue> lookupFileResult = findTopComponent.getLookup().lookupResult(FichierCrue.class);
          selectedFileById.put(id, lookupFileResult);
        }
      }
    }
  }
  Result<ManagerEMHContainerBase> currentResult;
  Result<FichierCrue> currentResultFile;

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (SELECTION_CHANGE_IN_TOPCOMPONENT.equals(evt.getPropertyName())) {
      updateSelection();
    } else if (TopComponent.Registry.PROP_ACTIVATED.equals(evt.getPropertyName())) {
      cleanListener();
      String id = getActivatedId();
      if (id == null || !selectedContainerById.containsKey(id)) {
        return;
      }
      currentResult = selectedContainerById.get(id);
      if (listenToFile) {
        currentResultFile = selectedFileById.get(id);
      }
      if (currentResult != null) {
        currentResult.addLookupListener(this);
      }
      if (currentResultFile != null) {
        currentResultFile.addLookupListener(this);
      }
      TopComponent activated = TopComponent.getRegistry().getActivated();
      if (activated != null) {
        activated.addPropertyChangeListener(SELECTION_CHANGE_IN_TOPCOMPONENT, this);
      }


    }
  }

  private void cleanListener() {
    TopComponent activated = TopComponent.getRegistry().getActivated();
    if (activated != null) {
      activated.removePropertyChangeListener(SELECTION_CHANGE_IN_TOPCOMPONENT, this);
    }

    if (currentResult != null) {
      currentResult.removeLookupListener(this);
    }
    if (currentResultFile != null) {
      currentResultFile.removeLookupListener(this);
    }
  }

  public void clean() {
    cleanListener();
    WindowManager.getDefault().getRegistry().removePropertyChangeListener(this);
    selectedContainerById.clear();
    selectedFileById.clear();
  }

  private static String getActivatedId() {
    TopComponent activated = TopComponent.getRegistry().getActivated();
    if (activated == null) {
      return null;
    }
    return WindowManager.getDefault().findTopComponentID(activated);
  }

  public void updateSelection() {
    if (TopComponent.getRegistry().getActivated() == component) {
      return;
    }
    final Children children = component.getNode(component.getNodeManager()).getChildren();
    Set<Node> found = Collections.emptySet();
    if (children.getNodesCount() > 0) {
      //pour eviter erreur de cast avec Children.Empty
      found = selectNodes((ChildrenKeysAbstract) children);
    }
    Node[] nodes = found.toArray(new Node[0]);
    try {

      component.getExplorerManager().setSelectedNodes(nodes);
      //bug dans Netbeans....
      component.getExplorerManager().setSelectedNodes(nodes);

    } catch (PropertyVetoException ex) {
      Exceptions.printStackTrace(ex);
    }
  }

  /**
   * @return les manager selectionne du TopComponent activé.
   */
  Collection<ManagerEMHContainerBase> getAllSelectedManager() {
    String findTopComponentID = getActivatedId();
    if (findTopComponentID == null) {
      return Collections.emptyList();
    }
    Result<ManagerEMHContainerBase> get = selectedContainerById.get(findTopComponentID);
    Set<ManagerEMHContainerBase> all = new HashSet<>();
    if (get != null) {
      all.addAll(get.allInstances());
    }
    return all;
  }

  @Override
  public void resultChanged(LookupEvent ev) {
    updateSelection();
  }

  protected abstract Set<Node> selectNodes(ChildrenKeysAbstract node);
}
