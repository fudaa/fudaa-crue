/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.migrate.MigrateTargetDirValidator;
import org.fudaa.dodico.crue.projet.migrate.ScenarioMigrateProcessor;
import org.fudaa.fudaa.crue.common.helper.CrueFileChooserBuilder;
import org.fudaa.fudaa.crue.common.helper.CrueProgressUtils;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.fudaa.fudaa.crue.study.services.CrueService;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;

@SuppressWarnings("unused")
@ActionID(category = "View",
    id = "org.fudaa.fudaa.crue.study.actions.MigrateProjectAction")
@ActionRegistration(displayName = "#MigrateProjectAction.Name")
@ActionReferences({
    @ActionReference(path = "Actions/Study", position = 6, separatorAfter = 7)
})
public final class MigrateProjectAction extends AbstractStudyAction {
  final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);
  private final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);
  private final InstallationService installationService = Lookup.getDefault().lookup(InstallationService.class);
  private CrueService crueService = Lookup.getDefault().lookup(CrueService.class);

  public MigrateProjectAction() {
    super(false);
    putValue(Action.NAME, NbBundle.getMessage(MigrateProjectAction.class, "MigrateProjectAction.Name"));
    //si campagne oac chargée -> on ne peut pas migrer d'étude
    crueService.addAocCampagneLookupListener(this);
  }

  @Override
  protected boolean getEnableState() {
    if (crueService == null) {
      crueService = Lookup.getDefault().lookup(CrueService.class);
    }
    return super.getEnableState() && crueService != null && !crueService.isAocCampagneLoaded();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (crueService.isScenarioOrRunLoaded()) {
      DialogHelper.showError(getActionTitle(), NbBundle.getMessage(MigrateProjectAction.class, "RunOrScenarioLoaded.Error"));
      return;
    }
    EMHProjet selectedProject = service.getSelectedProject();
    String version = configurationManagerService.chooseXsdVersion();
    if (version == null) {
      return;
    }
    CoeurConfigContrat destinationCoeurConfig = configurationManagerService.chooseCoeur(version);
    if (destinationCoeurConfig == null) {
      return;
    }
    CrueFileChooserBuilder builder = new CrueFileChooserBuilder(getClass());
    builder.setFileHiding(true);
    builder.setSelectionApprover(new DirSelectionApprover(new MigrateTargetDirValidator(selectedProject.getParentDirOfEtuFile())));
    File file = builder.showOpenDialog();
    if (file == null) {
      return;
    }
    if (file.isDirectory()) {
      file = new File(file, service.getEtuFile().getName());
    }
    file = CrueFileType.ETU.getWithExtension(file);
    MigrateProcessor runner = new MigrateProcessor(selectedProject, destinationCoeurConfig, file, installationService.getConnexionInformation());
    final CtuluLogGroup exportTo = CrueProgressUtils.showProgressDialogAndRun(runner, getActionTitle());
    if (exportTo.containsSomething()) {
      LogsDisplayer.displayError(exportTo, NbBundle.getMessage(DirSelectionApprover.class, "MigrateProject.BilanDialog", file.getAbsoluteFile()));
      service.updateLastLogsGroup(exportTo);
    }
    if (!exportTo.containsError()) {
      crueService.closeAll();
      service.load(file, false);
      OpenProjectAction.openCurrentScenario(modellingScenarioService, service.getSelectedProject());
    }
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new MigrateProjectAction();
  }

  private static class DirSelectionApprover implements CrueFileChooserBuilder.SelectionApprover {
    private final MigrateTargetDirValidator validator;

    public DirSelectionApprover(MigrateTargetDirValidator validator) {
      this.validator = validator;
    }

    @Override
    public boolean approve(File[] selection) {
      if (selection != null && selection.length == 1) {
        return approve(selection[0]);
      }
      return false;
    }

    @Override
    public boolean approve(File selection) {
      CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
      File destFile = null;
      if (selection.isDirectory()) {
        validator.valid(selection, log, null);
      } else {
        destFile = CrueFileType.ETU.getWithExtension(selection);
        validator.valid(selection.getParentFile(), log, destFile);
      }
      if (log.containsErrorOrSevereError()) {
        LogsDisplayer.displayError(log,
            NbBundle.getMessage(DirSelectionApprover.class, "MigrateProjectChooseFolder.BilanDialog", selection.getAbsoluteFile()));
        return false;
      }
      if (destFile != null && destFile.isFile()) {
        boolean overwrite = DialogHelper
            .showQuestion(NbBundle.getMessage(MigrateProjectAction.class, "migration.overwriteFile.DialogMessage", destFile.getName()));
        if (!overwrite) {
          return false;
        }
      }
      return true;
    }
  }

  private static class MigrateProcessor implements ProgressRunnable<CtuluLogGroup> {
    private final EMHProjet initProjet;
    private final CoeurConfigContrat destVersion;
    private final File destFile;
    private final ConnexionInformation connexionInformation;

    public MigrateProcessor(EMHProjet initProjet, CoeurConfigContrat destVersion, File destFile, ConnexionInformation connexionInformation) {
      this.initProjet = initProjet;
      this.destVersion = destVersion;
      this.destFile = destFile;
      this.connexionInformation = connexionInformation;
    }

    @Override
    public CtuluLogGroup run(ProgressHandle handle) {
      handle.switchToIndeterminate();
      ScenarioMigrateProcessor processor = new ScenarioMigrateProcessor(initProjet, connexionInformation);
      return processor.exportTo(destFile.getParentFile(), destFile.getName(), destVersion);
    }
  }
}
