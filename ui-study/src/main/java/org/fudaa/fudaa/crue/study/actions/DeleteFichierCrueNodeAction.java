/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.NodeToStringTransformer;
import org.fudaa.fudaa.crue.study.node.FichierCrueNode;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;
import java.util.concurrent.Callable;

public final class DeleteFichierCrueNodeAction extends AbstractEditNodeAction {

  protected final transient EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public DeleteFichierCrueNodeAction() {
    super(NbBundle.getMessage(DeleteFichierCrueNodeAction.class, "Delete.Name"));
  }


  public String getDialogTitleI18n(boolean single, String name) throws MissingResourceException {
    return NbBundle.getMessage(DeleteFichierCrueNodeAction.class, single ? "FichierCrue.DeleteFile.DialogTitle" : "FichierCrue.DeleteFileMulti.DialogTitle", name);
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    if (activatedNodes != null) {
      for (Node node : activatedNodes) {
        if (node.canDestroy() && node instanceof FichierCrueNode) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    List<FichierCrueNode> toBeDeleted = new ArrayList<>();
    if (activatedNodes != null) {
      for (Node node : activatedNodes) {
        if (node.canDestroy() && node instanceof FichierCrueNode) {
          toBeDeleted.add((FichierCrueNode) node);
        }
      }
      delete(toBeDeleted);
    }

  }

  private boolean containsExistingFile(final List<FichierCrueNode> node) {
    for (FichierCrueNode fichierCrueNode : node) {
      if (fichierCrueNode.fileExists()) {
        return true;
      }
    }
    return false;
  }

  private boolean userCancelAction(boolean single, String name) {
    final String dialogContent = single ? "FichierCrue.DeleteFile.DialogContent" : "FichierCrue.DeleteFileMulti.DialogContent";
    final String dialogTitle = getDialogTitleI18n(single, name);

    return !DialogHelper.showQuestion(dialogTitle,
            NbBundle.getMessage(DeleteFichierCrueNodeAction.class, dialogContent,name));
  }

  protected void delete(final List<FichierCrueNode> nodes) {

    boolean isSingle = nodes.size() == 1;


    boolean deleteFile = false;
    final String name = isSingle ? nodes.get(0).getDisplayName() : Integer.toString(nodes.size());
    if (containsExistingFile(nodes)) {
      Object[] options = new Object[]{
        org.openide.util.NbBundle.getMessage(DeleteFichierCrueNodeAction.class, "FichierCrue.DeleteNodeEtFile.Option"),
        org.openide.util.NbBundle.getMessage(DeleteFichierCrueNodeAction.class, "FichierCrue.DeleteNode.Option"),
        NotifyDescriptor.CANCEL_OPTION
      };
      final String dialogMessage = isSingle ? "FichierCrue.DeleteFileOnDisc.DialogContent" : "FichierCrue.DeleteFileOnDiscMulti.DialogContent";
      Object deleteFileAtEnd = DialogHelper.showQuestion(getDialogTitleI18n(isSingle, name),
              NbBundle.getMessage(DeleteFichierCrueNodeAction.class, dialogMessage), options);
      if (DialogHelper.isCancelOption(deleteFileAtEnd)) {
        return;
      }
      deleteFile = options[0].equals(deleteFileAtEnd);

    } //pas de fichier
    else if (userCancelAction(isSingle, name)) {
      return;
    }
    final boolean deleteFileConfirmed = deleteFile;
    Callable<Boolean> deleteCallable = () -> {
      Boolean res = Boolean.FALSE;
      for (FichierCrueNode nodeToDelete : nodes) {
        boolean removed = projetService.getSelectedProject().removeFile(nodeToDelete.getFichierCrue());
        if (removed && deleteFileConfirmed && nodeToDelete.fileExists()) {
          CrueFileHelper.deleteFile(nodeToDelete.getFile(),getClass());
        }
        if (removed) {
          res = Boolean.TRUE;
        }
      }
      return res;
    };
    String logEntry = ToStringHelper.transformTo(nodes, new NodeToStringTransformer());
    projetService.deepModification(deleteCallable, org.openide.util.NbBundle.getMessage(DeleteFichierCrueNodeAction.class, "Log.DeleteAction", logEntry));
  }

}
