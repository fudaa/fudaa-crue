package org.fudaa.fudaa.crue.study.property;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import org.openide.nodes.PropertySupport.ReadOnly;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class PropertyFileAbsolutePath extends ReadOnly {

  private final File file;

  public PropertyFileAbsolutePath(final File file) {
    super("AbsolutePath", File.class, NbBundle.getMessage(ManagerPropertyFactory.class, "File.AbsolutePath.Property"), org.openide.util.NbBundle.getMessage(ManagerPropertyFactory.class, "File.AbsolutePath.Property.Description"));
    this.file = file;
  }

  @Override
  public Object getValue() throws IllegalAccessException, InvocationTargetException {
    return file;
  }
}
