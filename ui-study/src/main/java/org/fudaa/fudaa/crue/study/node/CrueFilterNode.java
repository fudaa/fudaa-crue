/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.node;

import java.awt.datatransfer.Transferable;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.datatransfer.PasteType;

/**
 *
 * @author Chris
 */
public class CrueFilterNode extends FilterNode {

//  public static Node getOriginalNode(Node init) {
//    if (init instanceof FilterNode) {
//      return ((CrueFilterNode) init).getOriginal();
//    }
//    
//    return init;
//
//  }

  public CrueFilterNode(Node original) {
    super(original);
  }

  @Override
  public PasteType getDropType(Transferable t, int action, int index) {
    final Node original = this.getOriginal();
    if (original instanceof FilterableNode) {
      return ((FilterableNode) original).getDropType(this.getParentNode(), t, action, index);
    }

    return super.getDropType(t, action, index);
  }
}
