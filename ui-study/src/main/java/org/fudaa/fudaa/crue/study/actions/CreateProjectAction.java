package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.io.CustomFileFilterExtension;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.projet.migrate.FilteredProjectCreator;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractPerspectiveAwareAction;
import org.fudaa.fudaa.crue.common.helper.CrueFileChooserBuilder;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.PerspectiveState;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.study.perspective.PerspectiveServiceStudy;
import org.fudaa.fudaa.crue.study.services.CrueService;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;

/**
 * @author deniger
 */
@SuppressWarnings("unused")
@ActionID(category = "View",
    id = "org.fudaa.fudaa.crue.study.actions.CreateProjectAction")
@ActionRegistration(displayName = "#CreateProjectAction.Name")
@ActionReferences({
    @ActionReference(path = "Actions/Study", position = 1, separatorAfter = 2)
})
public class CreateProjectAction extends AbstractPerspectiveAwareAction {
  private final EMHProjetServiceImpl service = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  final PerspectiveServiceStudy perspectiveServiceStudy = Lookup.getDefault().lookup(PerspectiveServiceStudy.class);
  private CrueService crueService = Lookup.getDefault().lookup(CrueService.class);
  private final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

  public CreateProjectAction() {
    super(PerspectiveEnum.STUDY, false);
    putValue(Action.NAME, NbBundle.getMessage(OpenProjectAction.class, "CreateProjectAction.Name"));
    //si campagne oac chargée -> on ne peut pas creer d'étude
    crueService.addAocCampagneLookupListener(this);
  }

  @Override
  protected boolean getEnableState() {
    if (crueService == null) {
      crueService = Lookup.getDefault().lookup(CrueService.class);
    }
    //si campagne oac chargée -> on ne peut pas creer d'étude
    return !crueService.isAocCampagneLoaded();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    perspectiveServiceStudy.setState(PerspectiveState.MODE_READ);
    if (!configurationManagerService.isConfigValidShowMessage()) {
      return;
    }
    String version = configurationManagerService.chooseXsdVersion();
    if (version == null) {
      return;
    }
    File home = configurationManagerService.getDefaultDataHome();
    final CrueFileChooserBuilder builder = new CrueFileChooserBuilder(getClass()).setTitle(
        NbBundle.getMessage(CreateProjectAction.class, "CreateProjectAction.FileChooserTitle")).setDefaultWorkingDirectory(home).
        setApproveText(NbBundle.getMessage(CreateProjectAction.class, "CreateProjectAction.FileChooserTitle"));
    builder.setFileFilter(new CustomFileFilterExtension(CrueFileType.ETU.getExtension()));
    builder.setSelectionApprover(new CrueFileChooserBuilder.SelectionApprover() {
      @Override
      public boolean approve(File[] selection) {
        return false;
      }

      @Override
      public boolean approve(File selection) {
        if (selection.isDirectory()) {
          return false;
        }
        File completeFile = CtuluLibFile.appendStrictExtensionIfNeeded(selection, CrueFileType.ETU.getExtension());
        if (completeFile.exists()) {
          return DialogHelper.showQuestion(
              NbBundle.getMessage(CreateProjectAction.class, "CreateProjectAction.OverwriteDialogTitle"),
              NbBundle.getMessage(CreateProjectAction.class, "CreateProjectAction.OverwriteFile", completeFile.getName()));
        }
        return true;
      }
    });
    File toSave = builder.showSaveDialog();
    if (toSave == null) {
      return;
    }
    //l'utilisateur a accepté: on peut fermer les données chargées.
    crueService.closeAll();

    File completeFile = CtuluLibFile.appendStrictExtensionIfNeeded(toSave, CrueFileType.ETU.getExtension());
    EMHProjet newProject = FilteredProjectCreator
        .createEmptyProject(configurationManagerService.chooseCoeur(version), configurationManagerService.getConnexionInformation(), completeFile);
    if (newProject != null) {
      EMHProjetController controller = new EMHProjetController(newProject, configurationManagerService.getConnexionInformation());
      CtuluLogGroup group = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
      controller.saveProjet(completeFile, group);
      if (group.containsFatalError()) {
        LogsDisplayer.displayError(group, (String) getValue(NAME));
        return;
      }
      service.load(completeFile, false);
    }
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new CreateProjectAction();
  }
}
