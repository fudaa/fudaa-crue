/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.FileDeleteResult;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.ScenarioRunCleaner;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractPerspectiveAwareAction;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.common.services.PerspectiveState;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.study.perspective.PerspectiveServiceStudy;
import org.fudaa.fudaa.crue.study.services.CrueService;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.util.Lookup;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.List;

import static org.openide.util.NbBundle.getMessage;

public abstract class AbstractOpenProjectAction extends AbstractPerspectiveAwareAction {
  private final boolean readOnly;
  private final transient EMHProjetServiceImpl service = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  private final transient CrueService crueService = Lookup.getDefault().lookup(CrueService.class);
  private final transient ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);
  private final transient PerspectiveServiceStudy perspectiveServiceStudy = Lookup.getDefault().lookup(PerspectiveServiceStudy.class);
  private final transient ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

  public AbstractOpenProjectAction(boolean readOnly) {
    super(PerspectiveEnum.STUDY, false);
    this.readOnly = readOnly;
    //si campagne oac chargée -> on ne peut pas ouvrir d'étude
    crueService.addAocCampagneLookupListener(this);
  }

  public static void openCurrentScenario(ModellingScenarioService modellingScenarioService, EMHProjet selectedProject) {
    if (selectedProject != null) {
      ManagerEMHScenario scenarioCourant = selectedProject.getScenarioCourant();
      if (scenarioCourant != null) {
        boolean accepted = DialogHelper.showQuestion(
            getMessage(AbstractOpenProjectAction.class,
                "OpenScenarioCourantAtStartupTitle"),
            getMessage(AbstractOpenProjectAction.class,
                "OpenScenarioCourantAtStartup", scenarioCourant.getNom()));
        if (accepted) {
          modellingScenarioService.loadScenario(selectedProject, scenarioCourant);
        }
      }
    }
  }

  @Override
  protected boolean getEnableState() {
    //si campagne oac chargée -> on ne peut pas ouvrir d'étude
    return super.getEnableState() && !crueService.isAocCampagneLoaded();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    perspectiveServiceStudy.setState(PerspectiveState.MODE_READ_ONLY_TEMP);
    if (configurationManagerService.isConfigValidShowMessage()) {
      service.load(null, readOnly, () -> crueService.closeAll());
      EMHProjet selectedProject = service.getSelectedProject();
      cleanUnusedRunDir(selectedProject);
      manageDirFichierEtude(selectedProject);
      openCurrentScenario(modellingScenarioService, selectedProject);
    }
    else {
      perspectiveServiceStudy.setState(PerspectiveState.MODE_READ_ONLY_TEMP);
    }
  }

  /**
   * Créer
   *
   * @param selectedProject le projet ouvert
   */
  private void manageDirFichierEtude(EMHProjet selectedProject) {
    if (selectedProject != null) {
      final File dirOfFichiersEtudes = selectedProject.getDirOfFichiersEtudes();
      if (!dirOfFichiersEtudes.exists()) {
        final boolean created = dirOfFichiersEtudes.mkdirs();
        if (!created) {
          DialogHelper.showError(
              getMessage(AbstractOpenProjectAction.class, "AbstractOpenProjectAction.CreateEtuDirFailTitle"),
              getMessage(AbstractOpenProjectAction.class, "AbstractOpenProjectAction.CreateEtuDirFailMessage", dirOfFichiersEtudes.getAbsolutePath())

          );
        }
      } else if (dirOfFichiersEtudes.isFile()) {
        DialogHelper.showError(
            getMessage(AbstractOpenProjectAction.class, "AbstractOpenProjectAction.EtuDirIsFileTitle"),
            getMessage(AbstractOpenProjectAction.class, "AbstractOpenProjectAction.EtuDirIsFileTitleMessage", dirOfFichiersEtudes.getAbsolutePath())

        );
      }
    }
  }

  private void cleanUnusedRunDir(EMHProjet selectedProject) {
    if (selectedProject != null && !readOnly) {
      ScenarioRunCleaner cleaner = new ScenarioRunCleaner(selectedProject);
      List<File> dirToDelete = cleaner.getDirToDelete();
      if (CollectionUtils.isNotEmpty(dirToDelete)) {
        String filesAsString = getFilesAsString(dirToDelete);
        String question = getMessage(AbstractOpenProjectAction.class, "unusedDirFound.deleteAction", filesAsString);
        boolean showQuestion = DialogHelper.showQuestion(question);
        if (showQuestion) {
          FileDeleteResult deleteUnusedDir = cleaner.deleteUnusedDir();
          if (deleteUnusedDir.isNotEmpty()) {
            CtuluLogGroup gr = new CtuluLogGroup(null);
            gr.setDescription(getMessage(AbstractOpenProjectAction.class, "deleteNotComplete.Title"));
            if (!deleteUnusedDir.getFilesNotDeleted().isEmpty()) {
              CtuluLog log = gr.createLog();
              log.setDesc(getMessage(AbstractOpenProjectAction.class, "deleteFileNotComplete.Title"));
              for (File file : deleteUnusedDir.getFilesNotDeleted()) {
                log.addWarn(file.getAbsolutePath());
              }
            }
            if (!deleteUnusedDir.getDirNotDeleted().isEmpty()) {
              CtuluLog log = gr.createLog();
              log.setDesc(getMessage(AbstractOpenProjectAction.class, "deleteDirNotComplete.Title"));
              for (File file : deleteUnusedDir.getDirNotDeleted()) {
                log.addWarn(file.getAbsolutePath());
              }
            }
            LogsDisplayer.displayError(gr, gr.getDescription());
          }
        }
      }
    }
  }

  private String getFilesAsString(List<File> dirToDelete) {
    int max = Math.min(10, dirToDelete.size());
    StringBuilder builder = new StringBuilder();
    builder.append("<ul>");
    for (int i = 0; i < max; i++) {
      builder.append("<li>").append(dirToDelete.get(i).getAbsolutePath()).append("</li>");
    }
    if (max < dirToDelete.size()) {
      builder.append("<li>...</li>");
    }
    builder.append("</ul>");
    return builder.toString();
  }
}
