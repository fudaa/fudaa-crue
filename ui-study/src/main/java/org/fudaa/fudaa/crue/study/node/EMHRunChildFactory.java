package org.fudaa.fudaa.crue.study.node;

import java.util.List;
import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.study.services.StudyChangeListenerService;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 * Used to create scenario node
 * @author Fred Deniger
 */
public class EMHRunChildFactory extends ChildFactory<EMHRun> {

  private final ManagerEMHScenario scenario;
  private final EMHProjet projet;
  final StudyChangeListenerService studyChangeListenerService = Lookup.getDefault().lookup(StudyChangeListenerService.class);

  public EMHRunChildFactory(final ManagerEMHScenario modele, final EMHProjet projet) {
    this.scenario = modele;
    this.projet = projet;
  }

  @Override
  protected boolean createKeys(final List<EMHRun> list) {
    if (scenario.getListeRuns() != null) {
      list.addAll(scenario.getListeRuns());
    }
    return true;
  }

  @Override
  protected Node createNodeForKey(final EMHRun c) {
    final EMHRunNode emhRunNode = new EMHRunNode(c, scenario, projet.getDirForRun(scenario, c));
    emhRunNode.addPropertyChangeListener(studyChangeListenerService);
    if (ObjectUtils.equals(scenario.getRunCourant(), c)) {
      emhRunNode.setRunCourant(true);
    }
    return emhRunNode;
  }
}
