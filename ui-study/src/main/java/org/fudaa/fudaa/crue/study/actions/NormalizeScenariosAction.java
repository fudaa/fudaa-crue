/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Action permettrant de normaliser les scenarios d'une étude
 */
@ActionID(category = "View",
    id = "org.fudaa.fudaa.crue.study.actions.NormalizeScenariosAction")
@ActionRegistration(displayName = "#NormalizeScenariosAction.Name")
@ActionReferences({
    @ActionReference(path = "Actions/Study", position = 13, separatorAfter = 14)
})
public final class NormalizeScenariosAction extends AbstractStudyAction {
  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public NormalizeScenariosAction() {
    super(true);
    putValue(Action.NAME, NbBundle.getMessage(NormalizeScenariosAction.class, "NormalizeScenariosAction.Name"));
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    NormalizeScenariosNodeAction.normalizeAllScenarios(projetService);
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new NormalizeScenariosAction();
  }
}
