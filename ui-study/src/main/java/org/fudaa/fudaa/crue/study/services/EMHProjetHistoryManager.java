package org.fudaa.fudaa.crue.study.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

/**
 *
 * @author deniger
 */
public class EMHProjetHistoryManager {

  private FileObject historyFile;
  final FileSystem fs = FileUtil.createMemoryFileSystem();

  protected void createHistory(File file) {
    try {
      if (historyFile != null) {
        historyFile.delete();
      }
      String extension = CrueFileType.ETU.getExtension();
      historyFile = FileUtil.copyFile(FileUtil.createData(file), fs.getRoot(), StringUtils.removeEnd(file.getName(), extension), extension);
    } catch (IOException iOException) {
      Logger.getLogger(EMHProjetHistoryManager.class.getName()).log(Level.WARNING, "createHistory", iOException);
    }
  }

  public boolean hasHistory() {
    return historyFile != null;
  }

  public void restoreTo(File dest) {
    if (historyFile != null && historyFile.isData()) {
      OutputStream out = null;
      try {
        out = new FileOutputStream(dest);
        CtuluLibFile.copyStream(historyFile.getInputStream(), out, true, true);
        historyFile.delete();
        historyFile = null;
      } catch (Exception ex) {
        Exceptions.printStackTrace(ex);
      } finally {
        CtuluLibFile.close(out);
      }
    }
  }
}
