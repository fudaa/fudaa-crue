package org.fudaa.fudaa.crue.study.property;

import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import org.fudaa.fudaa.crue.study.node.UsedByNodeBuilder;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport.ReadOnly;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class PropertyUsedBy extends ReadOnly {

  private final Node node;

  public PropertyUsedBy(Node node) {
    super("UsedBy", Object.class, NbBundle.getMessage(ManagerPropertyFactory.class, "UsedBy.Property"),
          org.openide.util.NbBundle.getMessage(ManagerPropertyFactory.class, "UsedBy.Property.Description"));
    this.node = node;
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    return new PropertyEditorUsedBy();
  }

  @Override
  public Object getValue() throws IllegalAccessException, InvocationTargetException {
    return UsedByNodeBuilder.getParentsTree(node);
  }
}
