/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.node;

import java.util.Collection;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;
import org.fudaa.fudaa.crue.study.services.StudyChangeListenerService;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 *
 * @author Fred Deniger
 */
public class ListingManagerEMHSousModele extends ChildrenKeysAbstract<ManagerEMHSousModele> {

  final StudyChangeListenerService studyChangeListenerService = Lookup.getDefault().lookup(StudyChangeListenerService.class);
  private final NodesManager nodeManager;

  public ListingManagerEMHSousModele(NodesManager nodeManager) {
    this.nodeManager = nodeManager;
  }

  @Override
  protected Node createNode(String key) {
    ManagerEMHSousModele sousModele = nodeManager.getProjet().getSousModele(key);
    final ManagerEMHSousModeleNode managerEMHSousModeleNode = new ManagerEMHSousModeleNode(createChildren(sousModele), sousModele,nodeManager.getProjet());
    managerEMHSousModeleNode.addPropertyChangeListener(studyChangeListenerService);
    return managerEMHSousModeleNode;
  }

  @Override
  protected Collection getObjects() {
    return nodeManager.getProjet().getListeSousModeles();
  }

  public Children createChildren(ManagerEMHSousModele sousModele) {
    if (sousModele.getListeFichiers() == null) {
      return Children.LEAF;
    }
    AbstractNode fichier = new FichierCrueManagerNode(sousModele.getListeFichiers(), nodeManager);
    Children.Array res = new Children.Array();
    res.add(new Node[]{fichier});
    return res;


  }
}
