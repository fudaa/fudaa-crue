/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import java.util.concurrent.Callable;

/**
 *
 * @author Chris
 */
public class CallableTrue implements Callable<Boolean> {

  public CallableTrue() {
  }

  @Override
  public Boolean call() throws Exception {
    return Boolean.TRUE;
  }
}
