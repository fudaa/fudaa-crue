/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import java.io.File;
import java.util.Collections;
import java.util.concurrent.Callable;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.fudaa.crue.common.UserPreferencesSaver;
import org.fudaa.fudaa.crue.common.action.OneSelectionNodeAction;
import org.fudaa.fudaa.crue.study.actions.OverwriteFileHelper.OverwriteResult;
import org.fudaa.fudaa.crue.study.dialog.FileDialog;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Chris
 */
public class ModifyFileNodeAction extends OneSelectionNodeAction {

  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public ModifyFileNodeAction() {
    super(NbBundle.getMessage(ModifyFileNodeAction.class, "ModifyFileAction.ActionName"), true);
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected boolean enable(Node activatedNodes) {
    return activatedNodes.getLookup().lookup(FichierCrue.class) != null;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected void performAction(Node activatedNodes) {
    final FileDialog dialog = new FileDialog(true);
    final FichierCrue initFichierCrue = activatedNodes.getLookup().lookup(FichierCrue.class);
    dialog.setFichierCrueToModify(initFichierCrue);

    dialog.setName(getClass().getName());
    UserPreferencesSaver.loadDialogLocationAndDimension(dialog, null);
    dialog.setVisible(true);
    UserPreferencesSaver.saveLocationAndDimension(dialog, null);


    final FichierCrue newFile = dialog.getFile();

    if (newFile != null) {
      File existFile = newFile.getProjectFile(projetService.getSelectedProject());
      boolean overwrite = false;
      if (existFile.exists()) {
        OverwriteResult confirmOverwriteFiles = OverwriteFileHelper.confirmOverwriteFiles(Collections.singletonList(existFile.getAbsolutePath()));
        if (OverwriteResult.CANCEL.equals(confirmOverwriteFiles)) {
          return;
        }
        overwrite = OverwriteResult.DO_OVERWRITE.equals(confirmOverwriteFiles);
      }

      final boolean overwriteFinal = overwrite;
      Callable<Boolean> callable = new Callable<Boolean>() {

        @Override
        public Boolean call() throws Exception {
          final EMHProjet project = projetService.getSelectedProject();
          return project.renameFile(initFichierCrue, newFile, overwriteFinal);
        }
      };

      projetService.deepModification(callable, NbBundle.getMessage(AddFileAction.class, "ModifyFileAction.Log", initFichierCrue.getNom(), newFile.getNom()));
    }
  }
}
