/*
 GPL 2
 */
package org.fudaa.fudaa.crue.study.actions;

import javax.swing.JMenuItem;
import org.fudaa.fudaa.crue.common.node.NodeHelper;

/**
 *
 * @author Frederic Deniger
 */
public class ShowUsedByDefaultNodeAction extends ShowUsedByNodeAction {

  @Override
  public JMenuItem getPopupPresenter() {
    return NodeHelper.useBoldFont(super.getPopupPresenter());
  }
}
