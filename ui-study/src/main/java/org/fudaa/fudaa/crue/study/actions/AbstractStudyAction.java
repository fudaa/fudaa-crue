/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractPerspectiveAwareAction;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;

public abstract class AbstractStudyAction extends AbstractPerspectiveAwareAction  {

    private final Result<EMHProjet> resultat;
    final EMHProjetServiceImpl service = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

    public AbstractStudyAction(final boolean enableInEditModeOnly) {
        super(PerspectiveEnum.STUDY, enableInEditModeOnly);
        setEnabled(false);
        resultat = service.getLookup().lookupResult(EMHProjet.class);
        resultat.addLookupListener(this);
        resultChanged(null);
    }

    protected boolean asynchronous() {
        return false;
    }


    @Override
    public String getActionTitle() {
        return (String) getValue(NAME);
    }

    @Override
    protected boolean getEnableState() {
        return super.getEnableState() && service.isProjectOpened();
    }
}
