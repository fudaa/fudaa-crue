/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import javax.swing.Action;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
id = "org.fudaa.fudaa.crue.study.actions.OpenProjectReadOnlyAction")
@ActionRegistration(displayName = "#LoadStudyReadOnly.Name")
@ActionReferences({
  @ActionReference(path = "Actions/Study", position = 4,separatorAfter=5)
})
public final class OpenProjectReadOnlyAction extends AbstractOpenProjectAction {


  public OpenProjectReadOnlyAction() {
    super(true);
    putValue(Action.NAME, NbBundle.getMessage(OpenProjectReadOnlyAction.class, "LoadStudyReadOnly.Name"));
  }


  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new OpenProjectReadOnlyAction();
  }
}
