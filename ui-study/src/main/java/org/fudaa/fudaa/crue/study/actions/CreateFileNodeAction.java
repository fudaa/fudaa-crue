/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import java.io.File;
import java.util.Collections;
import java.util.concurrent.Callable;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.study.actions.OverwriteFileHelper.OverwriteResult;
import org.fudaa.fudaa.crue.study.dialog.FileDialog;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Chris
 */
public class CreateFileNodeAction extends AbstractEditNodeAction {

  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public CreateFileNodeAction() {
    super(NbBundle.getMessage(CreateFileNodeAction.class, "CreateFileAction.ActionName"));
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected boolean isEnableWithoutActivatedNodes() {
    return true;
  }

  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    return true;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected void performAction(final Node[] activatedNodes) {
    final FileDialog dialog = new FileDialog(true);
    dialog.setVisible(true);

    final FichierCrue newFile = dialog.getFile();

    if (newFile != null) {
      final File targetFile = newFile.getProjectFile(projetService.getSelectedProject());
      boolean overwrite = false;
      if (targetFile.exists()) {
        final OverwriteResult confirmOverwriteFiles = OverwriteFileHelper.confirmOverwriteFiles(Collections.singletonList(targetFile.getAbsolutePath()));
        if (OverwriteResult.CANCEL.equals(confirmOverwriteFiles)) {
          return;
        }
        overwrite = OverwriteResult.DO_OVERWRITE.equals(confirmOverwriteFiles);
      }
      final boolean overwriteFinal = overwrite;
      final Callable<Boolean> callable = () -> {
        final EMHProjet project = projetService.getSelectedProject();
        //le fichier existe et pas d'ecrasement
        if (targetFile.exists() && !overwriteFinal) {
          return project.getInfos().addCrueFileToProject(newFile);
        }
        //on ecrase:
        final File blankFile = new File(project.getCoeurConfig().getDefaultFile(newFile.getType()).toURI());
        if (blankFile.isFile()) {

          if (CtuluLibFile.copyFile(blankFile, targetFile)) {
            return project.getInfos().addCrueFileToProject(newFile);
          }
        }
        return false;
      };

      projetService.deepModification(callable, NbBundle.getMessage(AddFileAction.class, "CreateFileAction.Log", newFile.getNom()));
    }
  }
}
