package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.metier.etude.EMHInfosVersion;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.fudaa.fudaa.crue.study.dialog.ManageManagerEMHContainerDialog;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

/**
 *
 * @author Chris
 */
public class CreateSousModeleNodeAction extends AbstractEditNodeAction {

  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  private final ConnexionInformation connexionInfos = Lookup.getDefault().lookup(InstallationService.class).getConnexionInformation();

  public CreateSousModeleNodeAction() {
    super(NbBundle.getMessage(CreateSousModeleNodeAction.class, "CreateSousModeleAction.ActionName"));
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return true;
  }

  @Override
  protected boolean isEnableWithoutActivatedNodes() {
    return true;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    final ManagerEMHSousModele sousModele = new ManagerEMHSousModele();
    final EMHInfosVersion infosVersion = new EMHInfosVersion();
    infosVersion.setVersion(CrueVersionType.CRUE10);
    infosVersion.setAuteurCreation(connexionInfos.getCurrentUser());
    infosVersion.setDateCreation(connexionInfos.getCurrentDate());
    infosVersion.setAuteurDerniereModif(infosVersion.getAuteurCreation());
    infosVersion.setDateDerniereModif(infosVersion.getDateCreation());
    infosVersion.setCommentaire("");

    sousModele.setInfosVersions(infosVersion);

    ManageManagerEMHContainerDialog dialog = new ManageManagerEMHContainerDialog(WindowManager.getDefault().getMainWindow(), true, sousModele, true, "creerSousModele");

    dialog.display();

    if (dialog.isOkClicked()) {
      projetService.deepModification(new CreateContainerCallable(sousModele, projetService.getSelectedProject(), false, false), NbBundle.getMessage(getClass(), "CreateSousModeleAction.Log", sousModele.getNom()));
    }
  }
}
