package org.fudaa.fudaa.crue.study.node;

import java.util.List;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
/**
 * Used to create scenario node
 * @author Fred Deniger
 */
public class ManagerEMHScenarioChildFactory extends ChildFactory<ManagerEMHScenario> {

  private final List<ManagerEMHScenario> resultList;

  public ManagerEMHScenarioChildFactory(final List<ManagerEMHScenario> resultList) {
    this.resultList = resultList;
  }

  @Override
  protected boolean createKeys(final List<ManagerEMHScenario> list) {
    list.addAll(resultList);
    return true;
  }

  @Override
  protected Node createNodeForKey(final ManagerEMHScenario c) {
    
    return null;
  }
}
