package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.create.RunCreatorOptions;
import org.fudaa.fudaa.crue.common.action.OneSelectionNodeAction;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class LaunchNewRunNodeOptionAction extends OneSelectionNodeAction {

  final EMHProjetServiceImpl projetController = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public LaunchNewRunNodeOptionAction() {
    super(NbBundle.getMessage(OpenFichierCrueNodeAction.class, "LaunchNewRunNodeOptionAction.Name"),true);
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }
  
  

  @Override
  protected boolean enable(Node activatedNodes) {
    ManagerEMHScenario scenario = activatedNodes.getLookup().lookup(ManagerEMHScenario.class);
    if (scenario != null) {
      return scenario.isActive() && !scenario.isCrue9();
    }
    return false;
  }

  @Override
  protected void performAction(Node activatedNodes) {
    final ManagerEMHScenario scenario = activatedNodes.getLookup().lookup(ManagerEMHScenario.class);
    RunOptionPanelBuilder panelBuilder=new RunOptionPanelBuilder(scenario);
    RunCreatorOptions options = panelBuilder.getOptions();
    
    if(options==null){//l'utilisateur a appuye sur cancel...
      return;
    }
    projetController.launchRun(scenario,options);



  }
}
