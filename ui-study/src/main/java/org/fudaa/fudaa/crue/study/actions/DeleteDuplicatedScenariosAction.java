/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.event.ActionEvent;

@ActionID(category = "View",
    id = "org.fudaa.fudaa.crue.study.actions.DeleteDuplicatedScenariosAction")
@ActionRegistration(displayName = "#DeleteDuplicatedScenario.Name")
@ActionReferences({
    @ActionReference(path = "Actions/Study", position = 11, separatorAfter = 12)
})
public final class DeleteDuplicatedScenariosAction extends AbstractStudyAction {
  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public DeleteDuplicatedScenariosAction() {
    super(true);
    putValue(Action.NAME, NbBundle.getMessage(DeleteDuplicatedScenariosAction.class, "DeleteDuplicatedScenario.Name"));
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    DeleteDuplicatedScenarioNodeAction.deleteDuplicatedScenarios(projetService);
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new DeleteDuplicatedScenariosAction();
  }
}
