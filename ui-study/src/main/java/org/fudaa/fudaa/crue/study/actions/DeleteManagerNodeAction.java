/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;
import org.fudaa.dodico.crue.edition.DeleteContainersCallable;
import org.fudaa.dodico.crue.edition.EditionDeleteContainer;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.select.SelectableFinder;
import org.fudaa.dodico.crue.projet.select.SelectableFinder.SelectedItems;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.fudaa.fudaa.crue.common.helper.NodeToStringTransformer;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.study.node.AbstractManagerNode;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public final class DeleteManagerNodeAction extends AbstractEditNodeAction {
  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public DeleteManagerNodeAction() {
    super(NbBundle.getMessage(DeleteManagerNodeAction.class, "Delete.Name"));
  }

  public boolean containsRuns(final ManagerEMHContainerBase manager) {
    return manager instanceof ManagerEMHScenario && CollectionUtils.isNotEmpty(((ManagerEMHScenario) manager).getListeRuns());
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    if (activatedNodes != null) {
      for (Node node : activatedNodes) {
        if (node.canDestroy() && node instanceof AbstractManagerNode) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    List<AbstractManagerNode> toBeDeleted = new ArrayList<>();
    if (activatedNodes != null) {
      for (Node node : activatedNodes) {
        if (node.canDestroy() && node instanceof AbstractManagerNode) {
          toBeDeleted.add((AbstractManagerNode) node);
        }
      }
      delete(toBeDeleted);
    }
  }

  private boolean containsScenarioWithRun(List<AbstractManagerNode> toBeDeleted) {
    for (AbstractManagerNode node : toBeDeleted) {
      final ManagerEMHContainerBase manager = node.getManager();
      if (containsRuns(manager)) {
        return true;
      }
    }
    return false;
  }

  protected void delete(final List<AbstractManagerNode> nodes) {
    int res = JOptionPane.showOptionDialog(WindowManager.getDefault().getMainWindow(),
        containsScenarioWithRun(nodes) ? NbBundle.getMessage(getClass(),
            "DeleteAction.ConfirmationMessage.DialogContentRun") : NbBundle.getMessage(
            getClass(), "DeleteAction.ConfirmationMessage.DialogContent"),
        NbBundle.getMessage(getClass(), "DeleteAction.ConfirmationMessage.DialogTitle"),
        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null,
        new String[]{NbBundle.getMessage(getClass(), "DeleteAction.OneButton"), NbBundle.getMessage(
            getClass(), "DeleteAction.DeepButton"), NbBundle.getMessage(getClass(), "DeleteAction.CancelButton")},
        NbBundle.getMessage(getClass(), "DeleteAction.OneButton"));

    if (res < 0 || res > 1) {
      return;
    }

    final boolean deep = res == 1;

    final EMHProjet project = projetService.getSelectedProject();
    final List<ManagerEMHContainerBase> containers = new ArrayList<>(nodes.size());

    for (AbstractManagerNode<?> node : nodes) {
      containers.add(node.getManager());
    }

    final SelectableFinder finder = new SelectableFinder();
    finder.setProject(project);

    final SelectedItems items = finder.getIndependantSelection(containers, deep);

    if (items.isEmpty()) {
      JOptionPane.showMessageDialog(WindowManager.getDefault().getMainWindow(), NbBundle.getMessage(DeleteManagerNodeAction.class,
          "DeleteAction.DeleteNothing"),
          NbBundle.getMessage(getClass(), "DeleteAction.ConfirmationMessage.DialogTitle"),
          JOptionPane.ERROR_MESSAGE);

      return;
    }
    String log = NbBundle.getMessage(DeleteManagerNodeAction.class, "Log.DeleteAction",
        ToStringHelper.transformTo(nodes,
            new NodeToStringTransformer()));
    deleteManagers(projetService, items, log, getName());
  }

  /**
   * @param projetService le servive faisant l'action
   * @param items les items à supprimer
   * @param logString le log
   */
  public static void deleteManagers(EMHProjetServiceImpl projetService, SelectedItems items, String logString, String title) {
    DeleteContainersCallable deleteCallable = new DeleteContainersCallable(items, projetService.getController());
    projetService.deepModification(deleteCallable, logString);
    final CtuluLogGroup bilan = EditionDeleteContainer.createBilan(deleteCallable, title);
    if (bilan.containsSomething()) {
      LogsDisplayer.displayError(bilan, title);
    }
  }
}
