/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.metier.etude.EMHInfosVersion;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.UserPreferencesSaver;
import org.fudaa.fudaa.crue.common.action.OneSelectionNodeAction;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.fudaa.fudaa.crue.study.dialog.ManageManagerEMHContainerDialog;
import org.fudaa.fudaa.crue.study.node.UsedByNodeBuilder;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

/**
 * @author Chris
 */
public class ModifyScenarioNodeAction extends OneSelectionNodeAction {
  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  private final ConnexionInformation connexionInfos = Lookup.getDefault().lookup(InstallationService.class).getConnexionInformation();

  public ModifyScenarioNodeAction() {
    super(NbBundle.getMessage(CreateSousModeleNodeAction.class, "ModifyScenarioAction.ActionName"), true);
  }

  @Override
  protected boolean enable(Node activatedNodes) {
    if (UsedByNodeBuilder.nodeIsUsedByScenarioWithRun(activatedNodes)) {
      return false;
    }
    return activatedNodes.getLookup().lookup(ManagerEMHScenario.class) != null;
  }

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected void performAction(Node activatedNode) {
    //c'est pas bon: la dialogue modifie le scenario sans savoir si les données sont correctes.
    final ManagerEMHScenario scenario = activatedNode.getLookup().lookup(ManagerEMHScenario.class);
    ManageManagerEMHContainerDialog dialog = new ManageManagerEMHContainerDialog(WindowManager.getDefault().getMainWindow(), true, scenario, false, "modifierScenario");
    dialog.setResizable(true);
    dialog.setName(getClass().getName());
    UserPreferencesSaver.loadDialogLocationAndDimension(dialog, null);
    dialog.display();
    UserPreferencesSaver.saveLocationAndDimension(dialog, null);

    if (dialog.isOkClicked()) {
      final EMHInfosVersion infosVersion = scenario.getInfosVersions();
      infosVersion.setAuteurCreation(connexionInfos.getCurrentUser());
      infosVersion.setDateCreation(connexionInfos.getCurrentDate());
      projetService.deepModification(new CallableTrue(), NbBundle.getMessage(getClass(), "ModifyScenarioAction.Log", scenario.getNom()));
    }
  }
}
