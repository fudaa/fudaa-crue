package org.fudaa.fudaa.crue.study.services;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.fudaa.crue.common.services.AocServiceContrat;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.common.services.PostRunService;
import org.fudaa.fudaa.crue.study.node.NodesManager;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupListener;
import org.openide.util.lookup.ServiceProvider;

import java.io.File;

/**
 * Le service principal capable de donner l'état de l'application: projet ouvert ?, scenario chargé ?, run chargé ?
 * N'a pas de lookup propre et utilise les autres services dediées
 *
 * @author deniger
 * @see ModellingScenarioService
 * @see PostRunService
 * @see EMHProjetServiceImpl
 */
@ServiceProvider(service = CrueService.class)
public class CrueService {
    private final ModellingScenarioService modellingScenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);
    private final PostRunService postRunService = Lookup.getDefault().lookup(PostRunService.class);
    private final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
    private final AocServiceContrat aocServiceContrat = Lookup.getDefault().lookup(AocServiceContrat.class);
    /**
     * attention ces references doivent etre gardés ici: sinon elles sont garbagées et on perd les lisnteners
     */
    private final Result<EMHScenario> resultModelling;
    private final Result<EMHScenario> resultPost;
    private final Result<ProjectState> resultStateProjet;
    private final Result<AocCampagne> resultAocCampagne;

    /**
     * ajoute des listener sur les autres sevices: ecoute les modifications sur {@link  EMHScenario} pour les services {@link ModellingScenarioService} et
     * {@link PostRunService}. Ecoute les modifications de {@link EMHProjet} et {@link ProjectState} sur {@link EMHProjetServiceImpl}
     *
     * @see ModellingScenarioService
     * @see PostRunService
     * @see EMHProjetServiceImpl
     */
    public CrueService() {
        resultModelling = modellingScenarioService.getLookup().lookupResult(EMHScenario.class);
        resultPost = postRunService == null ? null : postRunService.getLookup().lookupResult(EMHScenario.class);
        resultStateProjet = projetService.getLookup().lookupResult(ProjectState.class);
        resultAocCampagne = aocServiceContrat==null?null:aocServiceContrat.getLookup().lookupResult(AocCampagne.class);
    }

    /**
     * @return le projet chargé
     * @see EMHProjetServiceImpl#getSelectedProject()
     */
    public EMHProjet getProjectLoaded() {
        return projetService.getSelectedProject();
    }

    /**
     * @return le fichier etu chargé
     * @see EMHProjetServiceImpl#getEtuFile()
     */
    public File getEtuFileLoaded() {
        return getProjetService().getEtuFile();
    }

    /**
     * @return le service EMHProjetServiceImpl
     */
    public EMHProjetServiceImpl getProjetService() {
        return projetService;
    }

    /**
     * @return true si un scenario est chargé dans la perspective Modelisation.
     * @see ModellingScenarioService#isScenarioLoaded()
     */
    public boolean isAScenarioLoadedInModellingPerspective() {
        return modellingScenarioService.isScenarioLoaded();
    }

    /**
     * @return true si une étude AOC est chargée
     * @see AocServiceContrat#isAocCampagneLoaded()
     */
    public boolean isAocCampagneLoaded() {
        return aocServiceContrat.isAocCampagneLoaded();
    }

    /**
     * @return true si une donnée LHPT est chargée
     * @see AocServiceContrat#isLhptLoaded()
     */
    public boolean isLhptLoaded() {
        return aocServiceContrat.isLhptLoaded();
    }

    /**
     * @return le scenario chargé dans la perspective Modelisation.
     * @see ModellingScenarioService#getScenarioLoaded()
     */
    public EMHScenario getScenarioLoadedInModellingPerspective() {
        return modellingScenarioService.getScenarioLoaded();
    }

    /**
     * @return le ManagerEMHScenario chargé dans la perspective Modelisation.
     * @see ModellingScenarioService#getManagerScenarioLoaded()
     */
    public ManagerEMHScenario getManagerScenarioLoadedInModellingPerspective() {
        return modellingScenarioService.getManagerScenarioLoaded();
    }

    /**
     * @return le gestionnaire des noeuds scenario/modele/sous-modele chargés dans la vue etude.
     * @see EMHProjetServiceImpl#getNodesManager()
     */
    public NodesManager getNodesManager() {
        return projetService.getNodesManager();
    }

    /**
     * @return true si un run est chargé dans la perspective rapport
     * @see PostRunService#isRunLoaded()
     */
    public boolean isAScenarioLoadedInPostPerspective() {
        return postRunService.isRunLoaded();
    }

    /**
     * @return le run chargé dans la perspective rapport
     * @see PostRunService#getRunLoaded()
     */
    public EMHRun getRunLoadedInPostPerspective() {
        return postRunService.getRunLoaded();
    }

    /**
     * @return le scenario (parent du run) chargé dans la perspective rapport
     * @see PostRunService#getScenarioLoaded()
     */
    public EMHScenario getScenarioLoadedInPostPerspective() {
        return postRunService.getScenarioLoaded();
    }

    /**
     * @return le ManagerEMHScenario (parent du run) chargé dans la perspective rapport
     * @see PostRunService#getManagerScenarioLoaded()
     */
    public ManagerEMHScenario getManagerScenarioLoadedInPostPerspective() {
        return postRunService.getManagerScenarioLoaded();
    }

    /**
     * Ajoute un listener sur les modifications de campagne AOC
     *
     * @param listener le listener à ajouter.
     */
    public void addAocCampagneLookupListener(LookupListener listener) {
        resultAocCampagne.addLookupListener(listener);
    }

    /**
     * Ajoute un listener sur les modifications de scenarios dans la perspective modélisation
     *
     * @param listener le listener à ajouter.
     */
    public void addModellingEMHScenarioLookupListener(LookupListener listener) {
        resultModelling.addLookupListener(listener);
    }

    /**
     * Ajoute un listener sur l etat de projet. Cette modification est effectué quand un projet est complètement chargé ou déchargé.
     *
     * @param listener le listener à ajouter.
     * @see ProjectState
     */
    public void addEMHProjetStateLookupListener(LookupListener listener) {
        resultStateProjet.addLookupListener(listener);
    }

    /**
     * Enleve un listener sur l etat de projet. Cette modification est effectué quand un projet est complètement chargé ou déchargé.
     *
     * @param listener le listener à enlever.
     * @see ProjectState
     */
    public void removeEMHProjetStateLookupListener(LookupListener listener) {
        resultStateProjet.removeLookupListener(listener);
    }

    /**
     * Ajoute un listener sur le scenario chargé en post-traitement ( rapport / Compte-rendu) de projet.
     *
     * @param listener le listener à ajouter.
     * @see PostRunService#getScenarioLoaded()
     */
    public void addPostLookupListener(LookupListener listener) {
        resultPost.addLookupListener(listener);
    }

    /**
     * Demande à tous les services principaux ( post, modélisation et etude) de fermer les scenario/run/etude en cours.
     *
     * @return true
     */
    public boolean closeAll() {
        if (postRunService.isRunLoaded()) {
            postRunService.unloadRun();
        }
        if (modellingScenarioService.isScenarioLoaded()) {
            modellingScenarioService.unloadScenario();
        }
        if (projetService.isProjectOpened()) {
            projetService.close();
        }
        return true;
    }

    /**
     * @return true si un scenario (modélisation) ou un run est chargé (Compte-rendu/Rapport).
     */
    public boolean isScenarioOrRunLoaded() {
        return modellingScenarioService.isScenarioLoaded() || postRunService.isRunLoaded();
    }

    /**
     * @return si le projet est chargé en mode read-only.
     */
    public boolean isProjetReadOnly() {
        return projetService.isProjetReadOnly();
    }

    /**
     * @return true si le projet est en cours de relecture depuis le fichier. Arrive lors de la sauvegarde d'une modification de l'étude.
     */
    public boolean isProjetCurrentlyReloading() {
        return projetService.isReloading();
    }
}
