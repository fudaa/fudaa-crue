/*
 * ManageManagerEMHContainerDialog.java
 *
 * Created on 12 janv. 2012, 16:02:15
 */
package org.fudaa.fudaa.crue.study.dialog;

import com.memoire.bu.BuVerticalLayout;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.etude.*;
import org.fudaa.dodico.crue.projet.rename.RenameManager;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.UserPreferencesSaver;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocContrat;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.swing.CommonGuiLib;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.net.URISyntaxException;
import java.util.*;
import java.util.List;

import static org.openide.util.NbBundle.getMessage;

/**
 * @author Chris
 */
public class ManageManagerEMHContainerDialog extends javax.swing.JDialog {
    private final String helpId;
    private final ManagerEMHContainerBase managerEMHContainerBase;
    private final List<FileLine> lines = new ArrayList<>();
    private final String dialogTitle;
    private final EMHProjet projet = Lookup.getDefault().lookup(EMHProjetServiceImpl.class).getSelectedProject();
    private final boolean creation;
    private boolean okClicked = false;
    private transient List<ManagerEMHModeleBase> modeles;
    private transient List<ManagerEMHSousModele> sousModeles;
    private final transient Map<String, ManagerEMHModeleBase> modelesByName = new HashMap<>();
    private final transient Map<String, ManagerEMHSousModele> sousModelesByName = new HashMap<>();
    private final transient Runnable validRunnable = this::checkValidState;

    private void checkValidState() {
        okButton.setEnabled(isDataValid());
    }

    /**
     * Creates new form ManageManagerEMHContainerDialog
     *
     * @param helpDialogId uniquement l'id de la bdl
     */
    public ManageManagerEMHContainerDialog(java.awt.Frame parent, boolean modal, ManagerEMHContainerBase container, boolean creation, String helpDialogId) {
        super(parent, modal);

        helpId = SysdocUrlBuilder.getDialogHelpCtxId(helpDialogId, PerspectiveEnum.STUDY);
        this.managerEMHContainerBase = container;
        this.creation = creation;
        initComponents();
        containerPanel.setLayout(new BuVerticalLayout(5, true, false));
        CrueLevelType level = container.getLevel();
        CrueVersionType version = container.getInfosVersions().getCrueVersion();
        setName(getClass().getName());

        if (creation) {
            this.dialogTitle = getMessage(FileDialog.class, "ManageManagerEMHContainerDialog.TitleCreation", this.convertLevel(level),
                    this.convertVersion(version));
        } else {
            this.dialogTitle = getMessage(FileDialog.class, "ManageManagerEMHContainerDialog.TitleModification", this.convertLevel(
                    level), this.convertVersion(version));
        }

        this.setTitle(dialogTitle);
        this.jLabelPrefix.setText(level.getPrefix());

        this.nameField.setText(StringUtils.removeStart(container.getNom(), level.getPrefix()));
        this.activeCheckBox.setSelected(container.isActive());
        this.commentField.setText(container.getInfosVersions().getCommentaire());

        if (level == CrueLevelType.SCENARIO) {
            modeles = new ArrayList<>();

            for (ManagerEMHModeleBase modele : projet.getListeModeles()) {
                if (modele.getInfosVersions().getCrueVersion() == version) {
                    modeles.add(modele);
                    modelesByName.put(modele.getNom(), modele);
                }
            }

            ManagerEMHScenario scenario = (ManagerEMHScenario) container;//on est sur que c'est un scenario

            this.fillModelesList(scenario.getFils());

            this.containerPanel.add(this.modelesPanel);
            if (version == CrueVersionType.CRUE10) {
                this.containerPanel.add(this.filesPanel);
            }
        } else if (level == CrueLevelType.MODELE) {
            sousModeles = projet.getListeSousModeles();
            for (ManagerEMHSousModele ssModele : sousModeles) {
                sousModelesByName.put(ssModele.getNom(), ssModele);
            }

            ManagerEMHModeleBase modele = (ManagerEMHModeleBase) container;//on est sur que c'est un modele

            this.fillSousModelesList(modele.getFils());

            if (version == CrueVersionType.CRUE10) {
                this.containerPanel.add(this.sousModelesPanel);
            }
            this.containerPanel.add(this.filesPanel);
        } else if (level == CrueLevelType.SOUS_MODELE) {
            this.containerPanel.add(this.filesPanel);
        }

        createPanelForFiles(container, level, version);

        final RadicalValidationInstaller radicalValidationInstaller = new RadicalValidationInstaller(this.nameField, jLabelValidation);
        radicalValidationInstaller.setRunAfterValidation(() -> {
            if (isContainerNameAlreadyUsed()) {
                radicalValidationInstaller.setErrorIcon();
                radicalValidationInstaller.setText(getMessage(ManageManagerEMHContainerDialog.class, "ModifiyEMHContainer.NameUsed", getManagerNewCompleteName()));
            }
            validRunnable.run();
        });
        if (level == CrueLevelType.SCENARIO) {
            jLabelMessage.setText(RadicalInputNotifier.getRadicalInfoMessageScenario());
        } else {
            jLabelMessage.setText(RadicalInputNotifier.getRadicalInfoMessageOther());
        }
        CommonGuiLib.initilalizeHelpLabel(jLabelMessage);
        modelesScrollPane.setPreferredSize(new Dimension(150, 120));
        sousModelesScrollPane.setPreferredSize(new Dimension(150, 120));
        jButtonModeleEdit.setEnabled(false);
        jButtonSousModeleEdit.setEnabled(false);
        modelesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        sousModelesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        modelesList.addListSelectionListener(e -> jButtonModeleEdit.setEnabled(!modelesList.isSelectionEmpty()));

        sousModelesList.addListSelectionListener(e -> jButtonSousModeleEdit.setEnabled(!sousModelesList.isSelectionEmpty()));
        modelesList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    editSelectedModele();
                }
            }
        });
        sousModelesList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    editSelectedSousModele();
                }
            }
        });
        this.setLocationRelativeTo(null);
        SysdocUrlBuilder.installHelpShortcut(jBtHelp, helpId);
    }

    private void createPanelForFiles(ManagerEMHContainerBase container, CrueLevelType level, CrueVersionType version) {
        final FichierCrueManager filesManager = container.getListeFichiers();
        filesPanel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        int row = 0;
        for (CrueFileType type : CrueFileType.values()) {
            //tant que des projets en 1.1.1...
            if (!EMHProjetServiceImpl.isOptrAccepted(projet) && CrueFileType.OPTR.equals(type)) {
                continue;
            }
            if (!EMHProjetServiceImpl.isDregAccepted(projet) && CrueFileType.DREG.equals(type)) {
                continue;
            }
            //les fichiers lhpt ne sont pas editable pour l'instant
            if (CrueFileType.LHPT.equals(type)) {
                continue;
            }
            if (!type.isResultFileType() && (type.getCrueVersionType() == version) && (type.getLevel() == level)) {
                FichierCrue file = filesManager == null ? null : filesManager.getFile(type);
                final FileLine line = new FileLine(type, file, container);
                line.setParentRunnable(validRunnable);
                lines.add(line);
                row = line.addToPanel(filesPanel, constraints, row);
            }
        }
    }

    protected void editSelectedModele() {
        int idx = modelesList.getSelectedIndex();
        if (idx >= 0) {
            SimplifiedManagerItem currentValue = (SimplifiedManagerItem) modelesList.getSelectedValue();
            DefaultListModel listModel = (DefaultListModel) modelesList.getModel();
            JComboBox combox = createModelCombo(currentValue.nom);
            combox.setSelectedItem(currentValue.nom);
            if (JOptionPane.showConfirmDialog(this, combox, getMessage(ManageManagerEMHContainerDialog.class,
                            "ManageManagerEMHContainerDialog.editModele"),
                    JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
                String newName = (String) combox.getSelectedItem();
                //modification done:
                if (newName != null && !newName.equals(currentValue.nom)) {
                    SimplifiedManagerItem newValue = new SimplifiedManagerItem();
                    newValue.nom = newName;
                    listModel.set(idx, newValue);
                }
            }
        }
    }

    protected void editSelectedSousModele() {
        int idx = sousModelesList.getSelectedIndex();
        if (idx >= 0) {
            SimplifiedManagerItem currentValue = (SimplifiedManagerItem) sousModelesList.getSelectedValue();
            DefaultListModel listModel = (DefaultListModel) sousModelesList.getModel();
            JComboBox combox = createSousModelCombo(currentValue.nom);
            combox.setSelectedItem(currentValue.nom);
            if (JOptionPane.showConfirmDialog(this, combox, getMessage(ManageManagerEMHContainerDialog.class,
                            "ManageManagerEMHContainerDialog.editSousModele"),
                    JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
                String newName = (String) combox.getSelectedItem();
                //modification done:
                if (newName != null && !newName.equals(currentValue.nom)) {
                    SimplifiedManagerItem newValue = new SimplifiedManagerItem();
                    newValue.nom = newName;
                    listModel.set(idx, newValue);
                }
            }
        }
    }

    public void display() {
        UserPreferencesSaver.loadDialogLocationAndDimension(this, "1.0");
        pack();
        setVisible(true);
        UserPreferencesSaver.saveLocationAndDimension(this, "1.0");
    }

    private Set<String> getUsedModeleNames() {
        ListModel model = modelesList.getModel();
        return getUseNames(model);
    }

    private Set<String> getUsedSousModeleNames() {
        ListModel model = sousModelesList.getModel();
        return getUseNames(model);
    }

    /**
     * @return true si le nom est déjà utilisé
     */
    private boolean isContainerNameAlreadyUsed() {
        return projet.isNameAlreadyUsed(getManagerNewRadical(), managerEMHContainerBase, creation);
    }

    private Set<String> getUseNames(ListModel model) {
        Set<String> names = new HashSet<>();
        int nbUsed = model.getSize();
        for (int i = 0; i < nbUsed; i++) {
            SimplifiedManagerItem item = (SimplifiedManagerItem) model.getElementAt(i);
            names.add(item.nom);
        }
        return names;
    }

    private void fillModelesList(List<ManagerEMHModeleBase> modeles) {
        final DefaultListModel model = new DefaultListModel();

        for (ManagerEMHModeleBase modele : modeles) {
            final SimplifiedManagerItem item = new SimplifiedManagerItem();
            item.nom = modele.getNom();

            model.addElement(item);
        }

        if (!modeles.isEmpty()) {
            this.modelesAddButton.setEnabled(false);
            this.modelesRemoveButton.setEnabled(true);
        }

        modelesList.setModel(model);
    }

    private void fillSousModelesList(List<ManagerEMHSousModele> sousModeles) {
        final DefaultListModel model = new DefaultListModel();

        for (ManagerEMHSousModele sousModele : sousModeles) {
            final SimplifiedManagerItem item = new SimplifiedManagerItem();

            item.nom = sousModele.getNom();

            model.addElement(item);
        }

        sousModelesList.setModel(model);
    }

    private JComboBox createModelCombo(String nameToAdd) {
        JComboBox modelesCombo = new JComboBox();
        Set<String> alreadyUsed = getUsedModeleNames();
        for (ManagerEMHModeleBase modele : modeles) {
            final String modeleName = modele.getNom();
            if (StringUtils.equals(nameToAdd, modeleName) || !alreadyUsed.contains(modeleName)) {
                modelesCombo.addItem(modeleName);
            }
        }
        return modelesCombo;
    }

    private JComboBox createSousModelCombo(String nameToAdd) {
        JComboBox sousModelesCombo = new JComboBox();
        Set<String> alreadyUsed = getUsedSousModeleNames();
        for (ManagerEMHSousModele sousModele : sousModeles) {
            final String sousModeleName = sousModele.getNom();
            if (StringUtils.equals(nameToAdd, sousModeleName) || !alreadyUsed.contains(sousModeleName)) {
                sousModelesCombo.addItem(sousModeleName);
            }
        }
        return sousModelesCombo;
    }

    public boolean isOkClicked() {
        return this.okClicked;
    }

    private String convertVersion(CrueVersionType version) {
        switch (version) {
            case CRUE10: {
                return getMessage(FileDialog.class, "ManageManagerEMHContainerDialog.Crue10");
            }

            case CRUE9: {
                return getMessage(FileDialog.class, "ManageManagerEMHContainerDialog.Crue9");
            }
        }

        return "";
    }

    private String convertLevel(CrueLevelType level) {
        switch (level) {
            case SCENARIO: {
                return getMessage(FileDialog.class, "ManageManagerEMHContainerDialog.Scenario");
            }

            case MODELE: {
                return getMessage(FileDialog.class, "ManageManagerEMHContainerDialog.Modele");
            }

            case SOUS_MODELE: {
                return getMessage(FileDialog.class, "ManageManagerEMHContainerDialog.SousModele");
            }
        }

        return "";
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of
     * this method is always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        modelesPanel = new javax.swing.JPanel();
        modelesButtonsPanel = new javax.swing.JPanel();
        modelesAddButton = new javax.swing.JButton();
        modelesRemoveButton = new javax.swing.JButton();
        jButtonModeleEdit = new javax.swing.JButton();
        modelesScrollPane = new javax.swing.JScrollPane();
        modelesList = new javax.swing.JList();
        sousModelesPanel = new javax.swing.JPanel();
        sousModelesButtonsPanel = new javax.swing.JPanel();
        sousModelesAddButton = new javax.swing.JButton();
        sousModelesRemoveButton = new javax.swing.JButton();
        jButtonSousModeleEdit = new javax.swing.JButton();
        sousModelesScrollPane = new javax.swing.JScrollPane();
        sousModelesList = new javax.swing.JList();
        filesPanel = new javax.swing.JPanel();
        headerPanel = new javax.swing.JPanel();
        commentLabel = new javax.swing.JLabel();
        commentField = new javax.swing.JTextField();
        nameLabel = new javax.swing.JLabel();
        nameField = new javax.swing.JTextField();
        activeCheckBox = new javax.swing.JCheckBox();
        jLabelMessage = new javax.swing.JLabel();
        jLabelPrefix = new javax.swing.JLabel();
        jLabelActif = new javax.swing.JLabel();
        jLabelValidation = new javax.swing.JLabel();
        buttonsPanel = new javax.swing.JPanel();
        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        jBtHelp = new javax.swing.JButton();
        containerPanel = new javax.swing.JPanel();

        modelesPanel.setBorder(javax.swing.BorderFactory
                .createTitledBorder(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.modelesPanel.border.title"))); // NOI18N
        modelesPanel.setLayout(new java.awt.BorderLayout());

        modelesAddButton.setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.modelesAddButton.text")); // NOI18N
        modelesAddButton.setMaximumSize(new java.awt.Dimension(81, 23));
        modelesAddButton.setMinimumSize(new java.awt.Dimension(81, 23));
        modelesAddButton.setPreferredSize(new java.awt.Dimension(81, 23));
        modelesAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modelesAddButtonActionPerformed(evt);
            }
        });

        modelesRemoveButton.setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.modelesRemoveButton.text")); // NOI18N
        modelesRemoveButton.setEnabled(false);
        modelesRemoveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modelesRemoveButtonActionPerformed(evt);
            }
        });

        jButtonModeleEdit.setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.jButtonModeleEdit.text")); // NOI18N
        jButtonModeleEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonModeleEditActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout modelesButtonsPanelLayout = new javax.swing.GroupLayout(modelesButtonsPanel);
        modelesButtonsPanel.setLayout(modelesButtonsPanelLayout);
        modelesButtonsPanelLayout.setHorizontalGroup(
                modelesButtonsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(modelesButtonsPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(modelesButtonsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(modelesRemoveButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                                        .addComponent(modelesAddButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                                        .addComponent(jButtonModeleEdit, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE))
                                .addContainerGap())
        );
        modelesButtonsPanelLayout.setVerticalGroup(
                modelesButtonsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(modelesButtonsPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(modelesAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonModeleEdit)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(modelesRemoveButton)
                                .addContainerGap())
        );

        modelesPanel.add(modelesButtonsPanel, java.awt.BorderLayout.EAST);

        modelesScrollPane.setViewportView(modelesList);

        modelesPanel.add(modelesScrollPane, java.awt.BorderLayout.CENTER);

        sousModelesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(
                getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.sousModelesPanel.border.title"))); // NOI18N
        sousModelesPanel.setLayout(new java.awt.BorderLayout());

        sousModelesAddButton
                .setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.sousModelesAddButton.text")); // NOI18N
        sousModelesAddButton.setMaximumSize(new java.awt.Dimension(81, 23));
        sousModelesAddButton.setMinimumSize(new java.awt.Dimension(81, 23));
        sousModelesAddButton.setPreferredSize(new java.awt.Dimension(81, 23));
        sousModelesAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sousModelesAddButtonActionPerformed(evt);
            }
        });

        sousModelesRemoveButton
                .setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.sousModelesRemoveButton.text")); // NOI18N
        sousModelesRemoveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sousModelesRemoveButtonActionPerformed(evt);
            }
        });

        jButtonSousModeleEdit
                .setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.jButtonSousModeleEdit.text")); // NOI18N
        jButtonSousModeleEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSousModeleEditActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout sousModelesButtonsPanelLayout = new javax.swing.GroupLayout(sousModelesButtonsPanel);
        sousModelesButtonsPanel.setLayout(sousModelesButtonsPanelLayout);
        sousModelesButtonsPanelLayout.setHorizontalGroup(
                sousModelesButtonsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(sousModelesButtonsPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(sousModelesButtonsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(sousModelesRemoveButton, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
                                        .addComponent(sousModelesAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
                                        .addComponent(jButtonSousModeleEdit, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE))
                                .addContainerGap())
        );
        sousModelesButtonsPanelLayout.setVerticalGroup(
                sousModelesButtonsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(sousModelesButtonsPanelLayout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(sousModelesAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonSousModeleEdit)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(sousModelesRemoveButton)
                                .addContainerGap(43, Short.MAX_VALUE))
        );

        sousModelesPanel.add(sousModelesButtonsPanel, java.awt.BorderLayout.EAST);

        sousModelesScrollPane.setViewportView(sousModelesList);

        sousModelesPanel.add(sousModelesScrollPane, java.awt.BorderLayout.CENTER);

        filesPanel.setBorder(javax.swing.BorderFactory
                .createTitledBorder(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.filesPanel.border.title"))); // NOI18N
        filesPanel.setLayout(null);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(815, 560));

        commentLabel.setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.commentLabel.text")); // NOI18N

        commentField.setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.commentField.text")); // NOI18N
        commentField.setPreferredSize(new java.awt.Dimension(290, 20));

        nameLabel.setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.nameLabel.text")); // NOI18N

        nameField.setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.nameField.text")); // NOI18N
        nameField.setPreferredSize(new java.awt.Dimension(260, 20));

        activeCheckBox.setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.activeCheckBox.text")); // NOI18N

        jLabelMessage.setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.jLabelMessage.text")); // NOI18N

        jLabelPrefix.setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.jLabelPrefix.text")); // NOI18N

        jLabelActif.setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.jLabelActif.text")); // NOI18N

        jLabelValidation.setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.jLabelValidation.text")); // NOI18N

        javax.swing.GroupLayout headerPanelLayout = new javax.swing.GroupLayout(headerPanel);
        headerPanel.setLayout(headerPanelLayout);
        headerPanelLayout.setHorizontalGroup(
                headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(headerPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelMessage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, headerPanelLayout.createSequentialGroup()
                                                .addComponent(commentLabel)
                                                .addGap(18, 18, 18)
                                                .addComponent(commentField, javax.swing.GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, headerPanelLayout.createSequentialGroup()
                                                .addGroup(headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, headerPanelLayout.createSequentialGroup()
                                                                .addComponent(nameLabel)
                                                                .addGap(48, 48, 48))
                                                        .addGroup(headerPanelLayout.createSequentialGroup()
                                                                .addComponent(jLabelActif)
                                                                .addGap(54, 54, 54)))
                                                .addGap(21, 21, 21)
                                                .addGroup(headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabelValidation, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                Short.MAX_VALUE)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, headerPanelLayout.createSequentialGroup()
                                                                .addComponent(jLabelPrefix)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(nameField, javax.swing.GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE))
                                                        .addComponent(activeCheckBox))))
                                .addContainerGap())
        );
        headerPanelLayout.setVerticalGroup(
                headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(headerPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabelMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(nameLabel)
                                        .addComponent(nameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelPrefix))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelValidation)
                                .addGap(18, 18, 18)
                                .addGroup(headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelActif)
                                        .addComponent(activeCheckBox))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(commentLabel)
                                        .addComponent(commentField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(headerPanel, java.awt.BorderLayout.NORTH);

        buttonsPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        okButton.setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.okButton.text")); // NOI18N
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });
        buttonsPanel.add(okButton);

        cancelButton.setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.cancelButton.text")); // NOI18N
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        buttonsPanel.add(cancelButton);

        jBtHelp.setText(getMessage(ManageManagerEMHContainerDialog.class, "ManageManagerEMHContainerDialog.jBtHelp.text")); // NOI18N
        jBtHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtHelpActionPerformed(evt);
            }
        });
        buttonsPanel.add(jBtHelp);

        getContentPane().add(buttonsPanel, java.awt.BorderLayout.SOUTH);

        containerPanel.setLayout(new java.awt.BorderLayout());
        getContentPane().add(containerPanel, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.setVisible(false);
        dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private boolean scenarioContainsActiveModele() {
        for (int i = 0; i < this.modelesList.getModel().getSize(); i++) {
            final SimplifiedManagerItem name = (SimplifiedManagerItem) this.modelesList.getModel().getElementAt(i);
            ManagerEMHModeleBase modele = this.modelesByName.get(name.nom);
            if (modele.isActive()) {
                return true;
            }
        }
        return false;
    }

    private boolean modeleContainsActiveSousModele() {
        for (int i = 0; i < this.sousModelesList.getModel().getSize(); i++) {
            final SimplifiedManagerItem name = (SimplifiedManagerItem) this.sousModelesList.getModel().getElementAt(i);
            ManagerEMHSousModele sousModele = this.sousModelesByName.get(name.nom);

            if (sousModele.isActive()) {
                return true;
            }
        }
        return false;
    }

    private boolean isDataValid() {
        String radical = getManagerNewRadical();
        String msg = ValidationPatternHelper.isRadicalValideForEMHContainerMsg(radical);
        if (msg != null) {
            return false;
        }
        if (isContainerNameAlreadyUsed()) {
            return false;
        }
        return lines.stream().noneMatch(fileLine -> fileLine.isInvalid());
    }

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        if (!isDataValid()) {
            DialogHelper.showError(dialogTitle, getMessage(ManageManagerEMHContainerDialog.class,
                    "ManageManagerEMHContainerDialog.invalidData"));
            return;
        }
        String managerCompleteName = getManagerNewCompleteName();

        //ne devrait pas arriver car controller en direct:
        final ManagerEMHContainerBase existingContainer = projet.getContainer(managerCompleteName, this.managerEMHContainerBase.getLevel());
        final boolean isCreationAndManagerExist = creation && existingContainer != null;
        final boolean isModificationAndOtherManagerWithSameName = !creation && existingContainer != null && existingContainer != managerEMHContainerBase;
        if (isCreationAndManagerExist || isModificationAndOtherManagerWithSameName) {
            JOptionPane.showMessageDialog(this, getMessage(ManageManagerEMHContainerDialog.class,
                            "ManageManagerEMHContainerDialog.nameExists", managerCompleteName), dialogTitle,
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        final List<FichierCrue> files = new ArrayList<>();

        for (FileLine line : this.lines) {
            FichierCrue file = line.getSelectedFile();
            if (line.isNewFile()) {
                file = line.getNewFile();
                final File targetFile = file.getProjectFile(projet);
                final File blankFile;
                try {
                    blankFile = new File(projet.getCoeurConfig().getDefaultFile(file.getType()).toURI());
                    if (blankFile.isFile()) {
                        if (CtuluLibFile.copyFile(blankFile, targetFile)) {
                            projet.getInfos().addCrueFileToProject(file);
                        }
                    }
                } catch (URISyntaxException e) {
                    Exceptions.printStackTrace(e);
                }
            }
            if (!line.isNoFile()) {
                if (file != null) {
                    files.add(file);
                } else {
                    JOptionPane.showMessageDialog(this, getMessage(ManageManagerEMHContainerDialog.class,
                                    "ManageManagerEMHContainerDialog.fileMissing"), dialogTitle,
                            JOptionPane.ERROR_MESSAGE);

                    return;
                }
            }
        }

        final CrueLevelType level = this.managerEMHContainerBase.getLevel();
//on valide d'abord:
        if (level == CrueLevelType.SCENARIO) {
            boolean hasActive = scenarioContainsActiveModele();
            if (!hasActive) {
                JOptionPane.showMessageDialog(this, getMessage(ManageManagerEMHContainerDialog.class,
                                "ManageManagerEMHContainerDialog.activeModele"), dialogTitle,
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        if ((level == CrueLevelType.MODELE) && (this.managerEMHContainerBase.getInfosVersions().getCrueVersion() == CrueVersionType.CRUE10)) {
            boolean hasActive = modeleContainsActiveSousModele();
            if (!hasActive) {
                JOptionPane.showMessageDialog(this, getMessage(ManageManagerEMHContainerDialog.class,
                                "ManageManagerEMHContainerDialog.activeSousModele"), dialogTitle,
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        //ok maintenant on essaie de renommer le manager:
        RenameManager renameMager = new RenameManager();
        renameMager.setProject(projet);
        if (creation) {
            //on ajoute le manager:
            projet.addManager(CrueLevelType.getCatEMH(level), managerEMHContainerBase, managerCompleteName);
        } else if (!renameMager.renameManager(CrueLevelType.getCatEMH(level), managerEMHContainerBase, managerCompleteName)) {
            DialogHelper.showError(getMessage(ManageManagerEMHContainerDialog.class, "ModifiyEMHContainer.NameUsed", managerCompleteName));
            return;
        }
        if (level == CrueLevelType.SCENARIO) {
            ManagerEMHScenario scenario = (ManagerEMHScenario) this.managerEMHContainerBase;
            scenario.removeAllManagerFils();
            for (int i = 0; i < this.modelesList.getModel().getSize(); i++) {
                final SimplifiedManagerItem name = (SimplifiedManagerItem) this.modelesList.getModel().getElementAt(i);
                ManagerEMHModeleBase modele = this.modelesByName.get(name.nom);
                scenario.addManagerFils(modele);
            }
        } else if ((level == CrueLevelType.MODELE) && (this.managerEMHContainerBase.getInfosVersions().getCrueVersion() == CrueVersionType.CRUE10)) {
            ManagerEMHModeleBase modele = (ManagerEMHModeleBase) this.managerEMHContainerBase;
            modele.removeAllManagerFils();
            for (int i = 0; i < this.sousModelesList.getModel().getSize(); i++) {
                final SimplifiedManagerItem name = (SimplifiedManagerItem) this.sousModelesList.getModel().getElementAt(i);
                ManagerEMHSousModele sousModele = this.sousModelesByName.get(name.nom);
                modele.addManagerFils(sousModele);
            }
        }

        this.managerEMHContainerBase.setActive(this.activeCheckBox.isSelected());
        this.managerEMHContainerBase.getInfosVersions().setCommentaire(this.commentField.getText());
        this.managerEMHContainerBase.setListeFichiers(files);

        this.okClicked = true;
        this.setVisible(false);
        dispose();
    }//GEN-LAST:event_okButtonActionPerformed

    private String getManagerNewRadical() {
        return this.nameField.getText();
    }

    private String getManagerNewCompleteName() {
        return managerEMHContainerBase.getLevel().getPrefix() + this.nameField.getText();
    }

    private void modelesAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modelesAddButtonActionPerformed
        JComboBox modelesCombo = createModelCombo(null);
        if (modelesCombo.getItemCount() > 0) {
            modelesCombo.setSelectedIndex(0);
        }

        if (JOptionPane.showConfirmDialog(this, modelesCombo, getMessage(ManageManagerEMHContainerDialog.class,
                        "ManageManagerEMHContainerDialog.addModele"),
                JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
            final int selectedIndex = modelesCombo.getSelectedIndex();

            if (selectedIndex > -1) {
                final SimplifiedManagerItem item = new SimplifiedManagerItem();

                item.nom = (String) modelesCombo.getSelectedItem();

                ((DefaultListModel) this.modelesList.getModel()).addElement(item);

                this.modelesAddButton.setEnabled(false);
                this.modelesRemoveButton.setEnabled(true);
            }
        }
    }//GEN-LAST:event_modelesAddButtonActionPerformed

    private void modelesRemoveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modelesRemoveButtonActionPerformed
        final int selectedIndex = this.modelesList.getSelectedIndex();
        if (selectedIndex != -1) {
            if (JOptionPane.showConfirmDialog(this, getMessage(ManageManagerEMHContainerDialog.class,
                            "ManageManagerEMHContainerDialog.removeModele"), dialogTitle,
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                ((DefaultListModel) this.modelesList.getModel()).remove(selectedIndex);

                this.modelesAddButton.setEnabled(true);
                this.modelesRemoveButton.setEnabled(false);
            }
        } else {
            JOptionPane.showMessageDialog(this, getMessage(ManageManagerEMHContainerDialog.class,
                            "ManageManagerEMHContainerDialog.selectModele"), dialogTitle,
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_modelesRemoveButtonActionPerformed

    private void sousModelesAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sousModelesAddButtonActionPerformed
        JComboBox sousModelesCombo = createSousModelCombo(null);
        if (sousModelesCombo.getItemCount() > 0) {
            sousModelesCombo.setSelectedIndex(0);
        }

        if (JOptionPane.showConfirmDialog(this, sousModelesCombo, getMessage(ManageManagerEMHContainerDialog.class,
                        "ManageManagerEMHContainerDialog.addSousModele"),
                JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
            final int selectedIndex = sousModelesCombo.getSelectedIndex();

            if (selectedIndex > -1) {
                final SimplifiedManagerItem item = new SimplifiedManagerItem();
                item.nom = (String) sousModelesCombo.getSelectedItem();

                ((DefaultListModel) this.sousModelesList.getModel()).addElement(item);
            }
        }
    }//GEN-LAST:event_sousModelesAddButtonActionPerformed

    private void sousModelesRemoveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sousModelesRemoveButtonActionPerformed
        final int selectedIndex = this.sousModelesList.getSelectedIndex();
        if (selectedIndex != -1) {
            if (JOptionPane.showConfirmDialog(this, getMessage(ManageManagerEMHContainerDialog.class,
                            "ManageManagerEMHContainerDialog.removeSousModele"), dialogTitle,
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                ((DefaultListModel) this.sousModelesList.getModel()).remove(selectedIndex);
            }
        } else {
            JOptionPane.showMessageDialog(this, getMessage(ManageManagerEMHContainerDialog.class,
                            "ManageManagerEMHContainerDialog.selectSousModele"), dialogTitle,
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_sousModelesRemoveButtonActionPerformed

    private void jButtonModeleEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonModeleEditActionPerformed
        editSelectedModele();
    }//GEN-LAST:event_jButtonModeleEditActionPerformed

    private void jButtonSousModeleEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSousModeleEditActionPerformed
        editSelectedSousModele();
    }//GEN-LAST:event_jButtonSousModeleEditActionPerformed

    private void jBtHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtHelpActionPerformed
        SysdocContrat sysdocContrat = Lookup.getDefault().lookup(SysdocContrat.class);
        sysdocContrat.display(helpId);
    }//GEN-LAST:event_jBtHelpActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox activeCheckBox;
    private javax.swing.JPanel buttonsPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JTextField commentField;
    private javax.swing.JLabel commentLabel;
    private javax.swing.JPanel containerPanel;
    private javax.swing.JPanel filesPanel;
    private javax.swing.JPanel headerPanel;
    private javax.swing.JButton jBtHelp;
    private javax.swing.JButton jButtonModeleEdit;
    private javax.swing.JButton jButtonSousModeleEdit;
    private javax.swing.JLabel jLabelActif;
    private javax.swing.JLabel jLabelMessage;
    private javax.swing.JLabel jLabelPrefix;
    private javax.swing.JLabel jLabelValidation;
    private javax.swing.JButton modelesAddButton;
    private javax.swing.JPanel modelesButtonsPanel;
    private javax.swing.JList modelesList;
    private javax.swing.JPanel modelesPanel;
    private javax.swing.JButton modelesRemoveButton;
    private javax.swing.JScrollPane modelesScrollPane;
    private javax.swing.JTextField nameField;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JButton okButton;
    private javax.swing.JButton sousModelesAddButton;
    private javax.swing.JPanel sousModelesButtonsPanel;
    private javax.swing.JList sousModelesList;
    private javax.swing.JPanel sousModelesPanel;
    private javax.swing.JButton sousModelesRemoveButton;
    private javax.swing.JScrollPane sousModelesScrollPane;
    // End of variables declaration//GEN-END:variables

    private static final class SimplifiedManagerItem {
        // L'index la liste contenant les managers (modeles ou sousModeles).
        public String nom;

        @Override
        public String toString() {
            return this.nom;
        }
    }
}
