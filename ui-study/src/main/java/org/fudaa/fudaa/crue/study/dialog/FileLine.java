package org.fudaa.fudaa.crue.study.dialog;

import com.jidesoft.swing.SearchableUtils;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.dodico.crue.io.etu.EMHProjectValidation;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.util.Lookup;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.openide.util.NbBundle.getMessage;

/**
 * @author deniger
 */
public class FileLine {
  private static final FichierCrue CREATE_NEW = new FichierCrue(getMessage(FileLine.class, "FileLinePanel.NewFile"), null, null);
  private static final FichierCrue NO_FILE = new FichierCrue(getMessage(FileLine.class, "FileLinePanel.NoFile"), null, null);
  private final javax.swing.JComboBox<FichierCrue> comboboxFile;
  private final JLabel jLabelValidationRadical;
  private final javax.swing.JTextField jTextFieldNewName;
  private final EMHProjetServiceImpl emhProjetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  private final JLabel jLabelName;
  private final RadicalValidationInstaller radicalChecker;
  private final CrueFileType type;
  private Runnable parentRunnable;

  public void setParentRunnable(Runnable parentRunnable) {
    this.parentRunnable = parentRunnable;
  }

  public FileLine(CrueFileType type, FichierCrue currentFile, ManagerEMHContainerBase container) {
    comboboxFile = new JComboBox();
    this.type = type;
    comboboxFile.setRenderer(new CtuluCellTextRenderer() {
      @Override
      protected void setValue(Object value) {
        if (value == null) {
          super.setValue(null);
        } else {
          super.setValue(((FichierCrue) value).getNom());
        }
      }
    });
    jLabelValidationRadical = new JLabel();
    jLabelName = new JLabel();
    this.jLabelName.setText(type.name());

    jTextFieldNewName = new JTextField(10);
    SearchableUtils.installSearchable(comboboxFile);

    final Map<FichierCrue, List<String>> sousModelNameUsingFile = new EMHProjectValidation().getSousModeleNameUsingDedicatedFile(emhProjetService.getSelectedProject());
    List<FichierCrue> usableFiles = emhProjetService.getSelectedProject().getInfos().getBaseFichiersProjets();
    //on trie les fichier par type et on enleve ceux deja utilisé
    usableFiles = usableFiles.stream().filter(fichierCrue -> type.equals(fichierCrue.getType()) && !sousModelNameUsingFile.containsKey(fichierCrue)).sorted()
        .collect(Collectors.toList());

    //on ajoute celui du
    final FichierCrue fileUsedByThisContainer = container.getFile(type);
    if (fileUsedByThisContainer != null && !usableFiles.contains(fileUsedByThisContainer)) {
      usableFiles.add(fileUsedByThisContainer);
      Collections.sort(usableFiles);
    }

    if (type.isOptional()) {
      usableFiles.add(0, NO_FILE);
    }
    usableFiles.add(0, CREATE_NEW);
    usableFiles.forEach(fichierCrue -> this.comboboxFile.addItem(fichierCrue));

    jTextFieldNewName.setColumns(20);
    //on met plus 3 pour le prefixe des entités (Sc_,...) car la classe RadicalValidationInstaller ajoute la taille du prefixe pour la validation
    // Classe utilisée à la base pour tester la taille des noms de conteneurs comme Sc_Radical.
    radicalChecker = new RadicalValidationInstaller(this.jTextFieldNewName, jLabelValidationRadical,
        ValidationPatternHelper.SPECIFIC_MAX_TAILLE_FOR_FILE_RADICAL + ValidationPatternHelper.PREFIX_LENGTH,
        ValidationPatternHelper.MESSAGE_ERROR_FILE_NAME_TOO_LONG);
    radicalChecker.setRunAfterValidation(() -> checkNewFileCanBeCreated());
    radicalChecker.attach(false);
    comboboxFile.addItemListener(e -> updateNewNameState());
    if (currentFile == null && type.isOptional()) {
      this.comboboxFile.setSelectedItem(NO_FILE);
    } else if (currentFile != null) {
      this.comboboxFile.setSelectedItem(currentFile);
    } else {
      this.comboboxFile.setSelectedIndex(0);
    }
    updateNewNameState();
  }

  private boolean newFileCanBeCreated = true;

  private void checkNewFileCanBeCreated() {
    newFileCanBeCreated = true;
    if (isNewFile()) {
      if (isNewNameAlreadyUsed()) {
        newFileCanBeCreated = false;
        radicalChecker.setErrorIcon();
        radicalChecker.setText(getMessage(FileLine.class, "FileLine.NewFileNameAlreadyUsed"));
      } else if (isNewNameUsedByAFile()) {
        radicalChecker.setErrorIcon();
        newFileCanBeCreated = false;
        radicalChecker.setText(getMessage(FileLine.class, "FileLine.FileAlreadyExists"));
      }
    }
    if (parentRunnable != null) {
      parentRunnable.run();
    }
  }

  public boolean isInvalid() {
    return !isValid();
  }

  public boolean isValid() {
    if (isNewFile()) {
      return radicalChecker.isValid() && newFileCanBeCreated;
    }
    return getSelectedFile() != null;
  }

  public FichierCrue getSelectedFile() {
    return (FichierCrue) this.comboboxFile.getSelectedItem();
  }

  private void updateNewNameState() {
    jTextFieldNewName.setEnabled(comboboxFile.getSelectedItem() == CREATE_NEW);

    radicalChecker.attach(jTextFieldNewName.isEnabled());
    if (!jTextFieldNewName.isEnabled()) {
      jLabelValidationRadical.setText("");
      jLabelValidationRadical.setIcon(null);
    } else {
      radicalChecker.updateValidState();
    }
    if (parentRunnable != null) {
      parentRunnable.run();
    }
  }

  /**
   * @return true si le nom du fichier est déjà utilisé
   */
  private boolean isNewNameAlreadyUsed() {
    return emhProjetService.getSelectedProject().getInfos().getFileFromBase(getFilenameForNewFile()) != null;
  }

  /**
   * @return true si un fichier avec le meme nom existe deja
   */
  private boolean isNewNameUsedByAFile() {
    FichierCrue newFile = getNewFile();
    final File targetFile = newFile.getProjectFile(emhProjetService.getSelectedProject());
    return targetFile.exists();
  }

  protected FichierCrue getNewFile() {
    return new FichierCrue(getFilenameForNewFile(), ".", this.type);
  }

  /**
   * @return true si le user a choisi de créer un nouveau fichier
   */
  public boolean isNewFile() {
    return CREATE_NEW == comboboxFile.getSelectedItem();
  }

  public boolean isNoFile() {
    return NO_FILE == comboboxFile.getSelectedItem();
  }

  /**
   * @return le nom du nouveau fichier
   */
  private String getFilenameForNewFile() {
    return jTextFieldNewName.getText().trim() + "." + type.getExtension();
  }

  /**
   * Methode utilitaire pour ajout au panel parent
   *
   * @param parent      le parent
   * @param constraints les contraintes
   * @param row         la ligne en cours
   * @return la ligne pour les prochains ajouts.
   */
  protected int addToPanel(JPanel parent, GridBagConstraints constraints, int row) {
    constraints.gridx = 0;
    constraints.anchor = GridBagConstraints.PAGE_START;
    constraints.gridy = row;
    constraints.gridwidth = 1;
    constraints.gridheight = 2;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.weightx = 0.2;
    constraints.weighty = 1;
    constraints.insets = new Insets(2, 0, 0, 0);
    parent.add(jLabelName, constraints);

    constraints.gridx = 1;
    constraints.gridheight = 1;
    constraints.weightx = 0.4;
    parent.add(comboboxFile, constraints);

    constraints.gridx = 2;
    parent.add(jTextFieldNewName, constraints);

    constraints.gridx = 1;
    constraints.gridy = row + 1;
    constraints.anchor = GridBagConstraints.NORTH;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.weightx = 0.8;
    constraints.weighty = 1;
    constraints.gridwidth = 2;
    constraints.insets = new Insets(1, 0, 5, 0);
    parent.add(jLabelValidationRadical, constraints);

    return row + 2;
  }
}
