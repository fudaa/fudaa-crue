/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.property;

import java.awt.Component;
import java.awt.EventQueue;
import java.beans.PropertyEditorSupport;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.DisplayerView;
import org.fudaa.fudaa.crue.study.node.UsedByNodeBuilder;
import org.openide.explorer.view.BeanTreeView;
import org.openide.nodes.Node;

/**
 *
 * @author Fred Deniger
 */
public class PropertyEditorUsedBy extends PropertyEditorSupport {

  public PropertyEditorUsedBy() {
  }

  private static DisplayerView createPanel(final Node value) {
    final DisplayerView view = new DisplayerView(value);
    final BeanTreeView installTreeView = view.installTreeView(false);
    EventQueue.invokeLater(new Runnable() {

      @Override
      public void run() {
        view.expandAll(installTreeView);
      }
    });
    return view;
  }

  @Override
  public Node getValue() {
    return (Node) super.getValue();
  }

  @Override
  public String getAsText() {
    if (getValue() == null || getValue().getChildren().getNodesCount() == 0) {
      return org.openide.util.NbBundle.getMessage(PropertyEditorUsedBy.class, "UsedByProperty.NotUsed");
    } else {
      return org.openide.util.NbBundle.getMessage(PropertyEditorUsedBy.class, "UsedByProperty.Used");
    }
  }

  @Override
  public boolean supportsCustomEditor() {
    return (getValue() != null && getValue().getChildren().getNodesCount() > 0);
  }

  public static void displayParent(Node node) {
    Node parent = UsedByNodeBuilder.getParentsTree(node);
    if (parent != null) {
      DisplayerView createPanel = createPanel(parent);
      DialogHelper.show(org.openide.util.NbBundle.getMessage(PropertyEditorUsedBy.class, "UsedBy.DialogTitle", node.getName()),
                        createPanel);
    }

  }

  @Override
  public Component getCustomEditor() {
    final Node value = getValue();
    DisplayerView view = createPanel(value);
    return view;
  }
}
