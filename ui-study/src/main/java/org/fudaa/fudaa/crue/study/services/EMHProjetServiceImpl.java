package org.fudaa.fudaa.crue.study.services;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformationFixed;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;
import org.fudaa.dodico.crue.config.coeur.Crue10VersionConfig;
import org.fudaa.dodico.crue.io.common.FileLocker;
import org.fudaa.dodico.crue.io.common.FileLocker.Data;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.projet.calcul.ExecInputDefault;
import org.fudaa.dodico.crue.projet.create.RunCalculOption;
import org.fudaa.dodico.crue.projet.create.RunCreatorOptions;
import org.fudaa.fudaa.crue.common.CommonMessage;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.common.property.listener.ListenerManager;
import org.fudaa.fudaa.crue.common.services.EMHProjetService;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.common.services.PostRunService;
import org.fudaa.fudaa.crue.loader.LoaderService;
import org.fudaa.fudaa.crue.loader.ProjectLoadContainer;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.fudaa.fudaa.crue.study.node.NodesManager;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Service gerant le chargement d'une etude dans l'application. Prend en charge également la création de run également.
 * <table  border="1"  cellpadding="3" cellspacing="0"  summary="lookups">
 * <tr>
 * <th align="left">Lookup</th>
 * <th align="left">Commentaire</th>
 * <th align="left">Méthode utilisant le lookup</th>
 * </tr>
 * <tr>
 * <td>{@link EMHProjet}</td>
 * <td>
 * Le projet chargé dans l'application
 * </td>
 * <td><code>{@link #getSelectedProject()}</code></td>
 * </tr>
 * <tr>
 * <td>{@link ProjectState}</td>
 * <td>	Enum indiquant si un projet est chargé dans l'application.</td>
 * <td>Pas exposé par une methode mais c'est un lookup ecoute par les autres services</td>
 * </tr>
 * <tr>
 * <td>{@link CtuluLogGroup}</td>
 * <td>
 * Les derniers logs obtenu avec la dernière opérations
 * </td>
 * <td><code>{@link #getLastLogsGroup()} </code></td>
 * </tr>
 * </table>
 *
 * @author Fred Deniger
 */
@ServiceProviders(value = {
        @ServiceProvider(service = EMHProjetService.class)
        ,
        @ServiceProvider(service = EMHProjetServiceImpl.class)})
public class EMHProjetServiceImpl implements Lookup.Provider, EMHProjetService {
    private static final Logger LOGGER = Logger.getLogger(EMHProjetServiceImpl.class.getName());
    /**
     * pour le lookup
     */
    private final InstanceContent dynamicContent = new InstanceContent();
    /**
     * pour le lookup
     */
    private final Lookup projectLookup = new AbstractLookup(dynamicContent);
    /**
     * Le gestionnaire des noeuds représentant l'étude.
     */
    private final NodesManager nodesManager = new NodesManager();
    /**
     * Lock de l'étude.
     */
    private final FileLocker fileLocker = new FileLocker();
    /**
     * Le fileObject du dossier contenant le fichier etu. Utilisé pour écouter toutes les modifications apportés dans ce dossier.
     */
    private FileObject etuFolder;
    /**
     * Utilise pour ecouter les changements dans le dossier du fichier etu.
     */
    private final ListenerManager listeners = new ListenerManager();
    /**
     * Service utilitaire de gestion de conf et de chargement d'etude/scenario
     */
    private final LoaderService loaderService = Lookup.getDefault().lookup(LoaderService.class);
    /**
     * Paramètres d'installation de l'application
     */
    private final InstallationService installationService = Lookup.getDefault().lookup(InstallationService.class);
    /**
     * Utilisé lors de la création d'un run.
     */
    private PostRunService postRunService;
    /**
     * Le TopComponent fourni par Netbeans RCP pour afficher les logs applicatifs.
     */
    private InputOutput io;
    /**
     * Date/Time formatter pour l'écriture des logs dans InputOutput.
     */
    private DateTimeFormatter formatter;
    /**
     * Gestionnaire de l'historique du fichier ETU: permet de revenir en arrière si le fichier etu devient invalide du fait d'opérations sur le fichier.
     */
    private final EMHProjetHistoryManager history = new EMHProjetHistoryManager();
    /**
     * True si l'étude est cours de rechargement: modifications effectuées dans l'UI -> sauvegarde et rechargement du fichier etu.
     * vaut true pendant le rechargement.
     */
    private boolean reloading;
    /**
     * si etude chargée en mode read-only
     */
    private boolean readOnly;
    /**
     * le fichier etu chargé.
     */
    private File etuFile;
    /**
     * la date de dernière modification du fichier etu chargé. Permet de savoir s'il a été modifié à l'extérieur.
     */
    private long lastModifiedEtuFile;

    /**
     * Le fichier OPTR a été ajouté apès la version 1.1.1. Pour cette version, il n'est donc pas supporté.
     *
     * @param projet le projet a tester.
     * @return true pour les projets de version > 1.1.1
     * @see CoeurConfig#getXsdVersion()
     */
    public static boolean isOptrAccepted(final EMHProjet projet) {
        return projet != null && Crue10VersionConfig.isOptrSupported(projet.getCoeurConfig());
    }
    public static boolean isDregAccepted(final EMHProjet projet) {
        return projet != null && Crue10VersionConfig.isDregSupported(projet.getCoeurConfig());
    }

    /**
     * @return nouvelle instance de {@link EMHProjetController}
     */
    public EMHProjetController getController() {
        return new EMHProjetController(getSelectedProject(), installationService.getConnexionInformation());
    }

    /**
     * mise à jour de la propriété <code>lastModifiedEtuFile</code>.
     */
    private void saveEtuFileLastModified() {
        lastModifiedEtuFile = etuFile == null ? 0 : etuFile.lastModified();
    }

    /**
     * @return true si le fichier OPTR est compatible avec le projet ouvert.
     */
    public boolean isOptrAccepted() {
        final EMHProjet projet = getSelectedProject();
        return isOptrAccepted(projet);
    }

    /**
     * @return le TopComponent InputOutput
     * @see IOProvider
     */
    private InputOutput getIO() {
        if (io == null) {
            io = IOProvider.getDefault().getIO(
                    org.openide.util.NbBundle.getMessage(EMHProjetServiceImpl.class, "IOFrame.NAME"), true);
        }
        return io;
    }

    /**
     * @return true si le projet est ouvert en mode read-only
     */
    public boolean isReadOnly() {
        return readOnly;
    }

    /**
     * Demande la création d'un historique du fichier etu. Utilise en interne et par {@link org.fudaa.fudaa.crue.study.actions.ConvertScenarioProcessor}
     */
    public void createHistory() {
        history.createHistory(etuFile);
    }

    /**
     * <b>ATTENTION</b>: pour les tests uniquement !!!!!!!
     *
     * @param projet le projet chargé par un test unitiare.
     */
    public void setForTest(EMHProjet projet) {
        dynamicContent.add(projet);
    }

    /**
     * Utilise pour effectuer une modification qui demande un rechargement de l'etude: suppresion/ajout de noeuds.
     * <code>processToCall</code> est appelé et si renvoie true, l'étude est persisté puis rechargée.
     *
     * @param processToCall l'action a appeler sur l'étude chargée.
     * @param logString     le prefix pour les logs
     */
    public void deepModification(Callable<Boolean> processToCall, String logString) {
        //on crée un historique avant les modifications
        createHistory();
        try {
            //on lance la modification
            Boolean ok = processToCall.call();
            //si ok
            if (Boolean.TRUE.equals(ok)) {
                //on sauvegarde
                quickSave();
                if (EventQueue.isDispatchThread()) {
                    //et on recharche:
                    reload(logString);
                } else {
                    EventQueue.invokeAndWait(() -> reload(logString));
                }
            }
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    /**
     * Log dans {@link #getIO() } l'action <code>actionToLog</code>.
     *
     * @param actionToLog l'action a loggeur
     */
    protected void log(String actionToLog) {
        if (formatter == null) {
            formatter = DateTimeFormat.mediumDateTime();
        }
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Action on Project {0}", actionToLog);
        }
        getIO().getOut().println(formatter.print(new LocalDateTime()) + ": " + actionToLog);
    }

    /**
     * @return NodesManager
     */
    public NodesManager getNodesManager() {
        return nodesManager;
    }

    /**
     * @return l'objet Netbeans representant le folder du fichier etu.
     */
    public FileObject getEtuFolder() {
        return etuFolder;
    }

    @Override
    public Lookup getLookup() {
        return projectLookup;
    }

    /**
     * si un projet est ouvert, desactive ce projet
     *
     * @see #unactiveProject(org.fudaa.dodico.crue.metier.etude.EMHProjet)
     */
    public void close() {
        EMHProjet currentProject = getSelectedProject();
        if (currentProject != null) {
            unactiveProject(currentProject);
        }
    }

    /**
     * Demande de rechargement du gestion etu et utilisation de l'historique si échec lors du rechargement.
     * Appelle {@link #reload(java.lang.String, boolean) avec true comme deuxieme parametre
     *
     * @param action pour le log: action a "logguer"
     */
    public void reload(String action) {
        reload(action, true);
    }

    /**
     * @return true si le projet est en cours de rechargement
     * @see #reload(java.lang.String, boolean)
     */
    public boolean isReloading() {
        return reloading;
    }

    /**
     * @param action     chaine a ajouter au log de l'application via {@link #log(java.lang.String) }
     * @param useHistory si true, l'historique sera utilisée si échec lors de rechargement du fichier etu: pour charger la version n-1 du fichier etu.
     */
    public void reload(String action, boolean useHistory) {
        //on indique que l'on recharge
        reloading = true;
        //on logue l'action
        log(action);
        // on persiste l'ancien fichier
        File oldEtuFile = etuFile;
        //on le charge à nouveau
        boolean loadedOk = load(etuFile, readOnly);
        //si erreur dans le rechargement
        if (!loadedOk) {
            //on tente d'utiliser l'historique
            if (useHistory && history.hasHistory()) {
                //on avertir l'utilisateur
                DialogHelper.showError(NbBundle.getMessage(EMHProjetServiceImpl.class, "ProjetSave.ReloadingFromHistoryError",
                        oldEtuFile.getName()));
                //on restore
                history.restoreTo(oldEtuFile);
                //et on recharge
                reload(action, false);
            } else {
                //si pas d'historique, on indique l'erreur à l'utilisateur
                DialogHelper.showError(NbBundle.getMessage(EMHProjetServiceImpl.class, "ProjetSave.ReloadingError", oldEtuFile.getName()));
            }
        }
        //chargement terminé
        reloading = false;
        //on persiste la date de dernière modification du fichier.
        saveEtuFileLastModified();
    }

    /**
     * @param iniEtuFile si null, un FileChooser est ouvert pour permettre à l'utilisateur de choisir
     * @param readOnly   true si node read-only demande
     * @return true si chargement ok.
     */
    public boolean load(File iniEtuFile, boolean readOnly) {
        return load(iniEtuFile, readOnly, null);
    }

    /**
     * Demande le chargement d'un fichier. Avant, ferme les projet/scenario/run ouverts.
     *
     * @param iniEtuFile  si null, un FileChooser est ouvert pour permettre à l utilisateur de choisir
     * @param readOnly    true si node read-only demande
     * @param closeAction runnable à lancer pour fermer l'etude/scenario ouvert si un fichier etu est choisi
     * @return true si chargement ok.
     * @see #close()
     */
    public boolean load(File iniEtuFile, boolean readOnly, Runnable closeAction) {
        //si le fichier etu n'est pas précisé, on propose à l'utilisateur d'en choisir un.
        File etuFileOpened = iniEtuFile;
        if (etuFileOpened == null) {
            etuFileOpened = loaderService.chooseEtuFile();
        }
        //pas de fichier choisi: on annule l'ouverture
        if (etuFileOpened == null) {
            return false;
        }
        //on traite le cas du fichier locké pour eviter les modifications simultanées de la meme etude.
        //si ouverture en mode read-only on ignore car aucune modification possible.
        //si on recharge on ignore également car fichier locké par user courant
        //si le fichier est déjà locké:
        boolean reloadingSameFile=reloading && etuFileOpened.equals(etuFile);
        if (!readOnly && !reloadingSameFile && fileLocker.isLock(etuFileOpened) ) {
            //on affiche un message à l'utilisateur indiquant qui le lock
            Data lockedBy = fileLocker.isLockedBy(etuFileOpened);
            if (lockedBy == null) {
                //cas particulier ou le fichier est locké mais impossible de trouver par qui
                lockedBy = new Data("?", "?");
            }
            //on avertit l'utilisateur:
            String title = NbBundle.getMessage(EMHProjetServiceImpl.class, "EtuFileLocked.DialogTitle");
            String openReadOnly = NbBundle.getMessage(EMHProjetServiceImpl.class, "EtuFileLocked.OpenInReadOnly");
            String unlock = NbBundle.getMessage(EMHProjetServiceImpl.class, "EtuFileLocked.Unlock");
            String cancel = NbBundle.getMessage(EMHProjetServiceImpl.class, "EtuFileLocked.Cancel");
            final String message = NbBundle.getMessage(EMHProjetServiceImpl.class, "EtuFileLocked.DialogMessage", lockedBy.getUserName(),
                    lockedBy.getTime());
            Object option = DialogHelper.showQuestion(title, message, new Object[]{openReadOnly, unlock, cancel});
            //le user veut ouvrir en mode read-only
            if (openReadOnly.equals(option)) {
                return load(etuFileOpened, true,closeAction);
                //le user veut annuler le lock sur le fichier etu ( il doit s'avoir ce qu'il fait...)
            } else if (unlock.equals(option)) {
                fileLocker.unlock(etuFileOpened);
                return load(etuFileOpened, readOnly,closeAction);
            } else {
                //il annule l'opération.
                return false;
            }
        }
        //Ici: le fichier peut être ouvert: pas de lock ou ouverture en mode read-only demandée
        //on ferme le scenario existant
        if (closeAction != null) {
            closeAction.run();
        } else {
            close();
        }

        //on charge l'étude:
        ProjectLoadContainer resultat = loaderService.loadProject(etuFileOpened);
        updateLastLogsGroup(resultat.getLogs());
        //on persiste le fichier etu ouvert
        this.etuFile = etuFileOpened;
        //l'étude est bien chargée
        if (resultat != null && resultat.getProjet() != null) {
            //persiste le mode read-only
            this.readOnly = readOnly;
            //si pas de read-only, on lock le fichier etu
            if (!readOnly) {
                fileLocker.lock(resultat.getEtuFile(), installationService.getConnexionInformation());
            }
            //on active finalement le projet (maj des TopComponent)
            activeProject(resultat.getProjet(), resultat.getEtuFile());
            //ok...
            return true;
        }
        //etude non chargée -> un message d'erreur a du être affiché -> pas de chargement
        return false;
    }

    /**
     * Methode interne pour activation d'un projet: persistence des infos, maj des listeners, maj du titre de l'application, construction des Nodes et
     * modification du Lookup de ce service.
     *
     * @param project: le projet ouvert
     * @param etuFile  le fichier etu
     */
    private void activeProject(EMHProjet project, File etuFile) {
        this.etuFile = etuFile;
        saveEtuFileLastModified();
        dynamicContent.add(project);
        nodesManager.setProject(project, etuFile, reloading);
        registerListener(project);
        CommonMessage.modifyFrameTitle(etuFile.getAbsolutePath());
        dynamicContent.add(ProjectState.OPENED);
    }

    /**
     * Test le timestamp du fichier etu. Si plus récent que cela stocké en memoire ( run lancé dans la perspective modélisation par exemple),
     * le fichier est rechargé.
     *
     * @param action message à utiliser pour les logs
     * @see #reload(java.lang.String)
     */
    public void refreshEtuFileIfNeeded(String action) {
        if (!isProjetReadOnly()) {
            return;
        }
        //premier appel: on initiale la propriété lastModifiedEtuFile
        if (lastModifiedEtuFile == 0) {
            saveEtuFileLastModified();
            //le fichier doit être rechargé:
        } else if (etuFile.lastModified() > lastModifiedEtuFile) {
            reload(action == null ? NbBundle.getMessage(EMHProjetServiceImpl.class, "ReloadEtuFile") : action);
        }
    }

    /**
     * Nettoyage suite fermeture projet: suppression du lock sur le fichier etu, nettoyage des listeners,
     *
     * @param currentProject
     */
    private void unactiveProject(EMHProjet currentProject) {
        //on enleve le lock
        if (!readOnly) {
            fileLocker.unlock(etuFile);
        }
        //nettoie les noeuds
        nodesManager.clear(reloading);
        //le listener sur le dossier du fichier etu
        listeners.clean();

        etuFolder = null;
        etuFile = null;
        //RAS des infos locales:
        dynamicContent.remove(currentProject);
        lastModifiedEtuFile = 0;
        readOnly = false;
        CommonMessage.resetFrameTitle();
        dynamicContent.remove(ProjectState.OPENED);
    }

    @Override
    public EMHProjet getSelectedProject() {
        return projectLookup.lookup(EMHProjet.class);
    }

    @Override
    public CrueConfigMetier getCcm() {
        EMHProjet selectedProject = getSelectedProject();
        return selectedProject == null ? null : selectedProject.getPropDefinition();
    }

    /**
     * @return true si une etude est ouverte
     */
    public boolean isProjectOpened() {
        return getSelectedProject() != null;
    }

    /**
     * Maj du listener sur le dossier du fichier etu.
     *
     * @param project le projet
     */
    private void registerListener(EMHProjet project) {
        File file = project.getDirOfFichiersEtudes();
        try {
            if (file.exists()) {
                etuFolder = FileUtil.createFolder(file);
                listeners.registerAndaddListener(new FileChangeListenerEtudeDir(this));
            }
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    /**
     * @see #getNodesManager()
     * @see NodesManager#updateEtuFiles()
     */
    void updateEtuFiles() {
        getNodesManager().updateEtuFiles();
    }

    /**
     * sauvegarde de l'etude en cours ( historique, save et affichage des erreurs si présentes).
     */
    void quickSave() {
        assert !readOnly;
        createHistory();
        EMHProjetController controller = getController();
        CtuluLogGroup ctuluLogGroup = new CtuluLogGroup((BusinessMessages.RESOURCE_BUNDLE));
        controller.saveProjet(etuFile, ctuluLogGroup);
        if (ctuluLogGroup.containsSomething()) {
            LogsDisplayer.displayError(ctuluLogGroup, NbBundle.getMessage(EMHProjetServiceImpl.class, "MessageInQuickSave.BilanDialogTitle"));
        }
        log(org.openide.util.NbBundle.getMessage(EMHProjetServiceImpl.class, "LogProjectSaved"));
        updateLastLogsGroup(ctuluLogGroup);
        saveEtuFileLastModified();
    }

    /**
     * Lancement d'un run et delegation au {@link PostRunService} pour la gestion du lancement.
     *
     * @param scenario le scenario pour lequel le run est lancé.
     * @param options  les options du run
     */
    public ManagerEMHScenario launchRun(ManagerEMHScenario scenario, RunCreatorOptions options) {
        //le fichier etu va être modifié: on sauvegarde la version en cours...
        createHistory();
        if (this.postRunService == null) {
            postRunService = Lookup.getDefault().lookup(PostRunService.class);
        }
        //est-ce que le run peut être lancé ? non -> on annule
        if (!postRunService.validConfigurationForRun(scenario, getSelectedProject().getCoeurConfig())) {
            return null;
        }
        //on créé le run à partir des options fournie
        EMHProjetController controller = getController();
        CrueOperationResult<EMHRun> createRun;
        //le run de reference utilise pour ce run
        EMHRun runReference = null;
        if (options == null) {
            //ordre important pour recuperer le run courant avant la création du nouveau run courant
            runReference = scenario.getRunCourant();
            createRun = controller.createRun(scenario.getNom(), etuFile);
        } else {
            runReference = options.getSelectedRunToStartFrom();
            createRun = controller.createRun(scenario.getNom(), options, etuFile);
        }
        //si messages, on les affiche
        if (createRun.getLogs().containsSomething()) {
            LogsDisplayer.displayError(createRun.getLogs(), NbBundle.getMessage(EMHProjetServiceImpl.class, "CreateRunError.DialogTitle",
                    scenario.getNom()));
        }
        updateLastLogsGroup(createRun.getLogs());
        //la creation a echouer-> on annule
        if (createRun.getResult() == null) {
            return null;
        }

        //creation de la map des options: donne par type de fichiers, les actions (RunCalculOption) à effectuer.
        Map<CrueFileType, RunCalculOption> optionsByCompute;

        if (options == null) {
            optionsByCompute = RunCreatorOptions.createDefaultMap();
        } else {
            optionsByCompute = options.getOptionsByCompute();
        }
        //lancement du run opéré par le service de Compte-Rrednu.
        postRunService.run(new ExecInputDefault(getSelectedProject(), scenario, createRun.getResult(), runReference),
                optionsByCompute);
        //on recharge l'etude.
        reload(NbBundle.getMessage(EMHProjetServiceImpl.class, "LogLaunchRunDefaultOption", scenario.getNom()));
        return controller.getProjet().getScenario(scenario.getId());
    }

    /**
     * @return le fichier etu chargé. Null si pas chargé
     */
    public File getEtuFile() {
        return etuFile;
    }

    /**
     * Appelle {@link #launchRun(org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario, org.fudaa.dodico.crue.projet.create.RunCreatorOptions)} sans options.
     *
     * @param scenario le scenario pour lequel le run est lancé.
     * @return le scenario modifié avec le nouveau run
     */
    public ManagerEMHScenario launchRun(ManagerEMHScenario scenario) {
        return launchRun(scenario, null);
    }

    /**
     * Appelle {@link #launchRun(org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario, org.fudaa.dodico.crue.projet.create.RunCreatorOptions)}
     * avec toutes les options activées et sans run d'initialisation.
     *
     * @param scenario le scenario pour lequel le run est lancé.
     * @return le scenario modifié avec le nouveau run
     */
    public ManagerEMHScenario launchRunAllOptions(ManagerEMHScenario scenario) {
        RunCreatorOptions options = new RunCreatorOptions(null);
        options.setComputeAll(RunCalculOption.FORCE_EXCUTE);
        return launchRun(scenario, options);
    }

    /**
     * Appelé par {@link ModellingScenarioService} pour indiquer des modifications dans le scenario.
     * Permet de mettre à jour les infos des conteneurs ( qui a modifie et quand ).
     *
     * @param managerScenarioLoaded la manager loadé
     * @param modifiedFiles         les fichiers modifiées
     */
    public void scenarioSaved(ManagerEMHScenario managerScenarioLoaded, Set<String> modifiedFiles) {
        //on récupère les infos du user et le temps de modifications.
        ConnexionInformationFixed connexionInfo = new ConnexionInformationFixed(installationService.getConnexionInformation());
        //mises à jour des infos pour le scenario chargé
        FichierCrueManagerStateUpdater.updateModifiedState(managerScenarioLoaded, connexionInfo, modifiedFiles);
        List<ManagerEMHScenario> listeScenarios = getSelectedProject().getListeScenarios();
        //et tous les autres
        for (ManagerEMHScenario managerEMHScenario : listeScenarios) {
            FichierCrueManagerStateUpdater.updateModifiedState(managerEMHScenario, connexionInfo, modifiedFiles);
        }
        //sauvegarde des infos.
        quickSave();
    }

    /**
     * @param ctuluLogGroup le nouveau bilan a persister.
     */
    public void updateLastLogsGroup(CtuluLogGroup ctuluLogGroup) {
        CtuluLogGroup lastLogsGroup = getLastLogsGroup();
        //on enleve le précédent
        if (lastLogsGroup != null) {
            dynamicContent.remove(lastLogsGroup);
        }
        //et ajoute le nouveau.
        if (ctuluLogGroup != null) {
            dynamicContent.add(ctuluLogGroup);
        }
    }

    boolean isProjetReadOnly() {
        return readOnly;
    }

    @Override
    public CtuluLogGroup getLastLogsGroup() {
        return projectLookup.lookup(CtuluLogGroup.class);
    }
}
