/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.node;

import org.fudaa.dodico.crue.config.coeur.CrueVersionType;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHModeleBase;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.fudaa.fudaa.crue.study.actions.*;
import org.fudaa.fudaa.crue.study.perspective.PerspectiveServiceStudy;
import org.fudaa.fudaa.crue.study.property.ManagerPropertyFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeTransfer;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.PasteType;

import javax.swing.*;
import java.awt.datatransfer.Transferable;

/**
 * @author Fred Deniger
 */
public class ManagerEMHModeleNode extends AbstractManagerNode<ManagerEMHModeleBase> implements FilterableNode {
    private final PerspectiveServiceStudy perspectiveService = Lookup.getDefault().lookup(PerspectiveServiceStudy.class);

    public ManagerEMHModeleNode(Children children, ManagerEMHModeleBase modele, EMHProjet projet) {
        super(children, modele, projet);
        setName(modele.getNom());
        setIconBaseWithExtension(CrueIconsProvider.getIconBase(EnumCatEMH.MODELE));
    }

    @Override
    public Action[] getActions(boolean context) {
        Action[] result = new Action[]{
                SystemAction.get(ModifyModeleNodeAction.class),
                SystemAction.get(RenameManagerNodeAction.class),
                SystemAction.get(CopyManagerNodeAction.class),
                null,
                SystemAction.get(DeleteManagerNodeAction.class),
                null,
                SystemAction.get(ShowUsedByDefaultNodeAction.class)
        };
        return result;
    }

    @Override
    public SystemAction getDefaultAction() {
        return SystemAction.get(ShowUsedByDefaultNodeAction.class);
    }

    @Override
    protected Sheet createSheet() {
        return ManagerPropertyFactory.createSheet(this, getLookup().lookup(ManagerEMHModeleBase.class), getProjet());
    }

    @Override
    public PasteType getDropType(Transferable t, int action, int index) {
        return this.getDropType(this.getParentNode(), t, action, index);
    }

    @Override
    public PasteType getDropType(Node parent, Transferable t, int action, int index) {
        if (perspectiveService.isInEditMode()) {
            Node dragged = DefaultNodePasteType.getMovedNodeInTransferable(t);

            if (dragged == null) {
                dragged = NodeTransfer.node(t, NodeTransfer.CLIPBOARD_CUT);
            }

            if (dragged instanceof ManagerEMHSousModeleNode) {
                final ManagerEMHSousModele draggedManager = ((ManagerEMHSousModeleNode) dragged).getManager();
                CrueVersionType sousModeleVersion = draggedManager.getInfosVersions().getCrueVersion();
                CrueVersionType modeleVersion = this.getManager().getInfosVersions().getCrueVersion();
//si meme version de modele et si sous-modele pas deja présent.
                if (modeleVersion == sousModeleVersion && getManager().getManagerFils(draggedManager.getId()) == null) {
                    return new ManagerEMHContainerNodePastType(this, dragged);
                }
            } else if (dragged instanceof FichierCrueNode) {
                CrueVersionType modeleVersion = this.getManager().getInfosVersions().getCrueVersion();
                FichierCrue file = ((FichierCrueNode) dragged).getFichierCrue();
                if ((file.getType().getCrueVersionType() == modeleVersion) && file.getType().isModeleLevel()
                        && !getManager().getListeFichiers().getFichiers().contains(file)) {
                    return new ManagerEMHContainerNodePastType(this, dragged);
                }
            }
        }

        return parent.getDropType(t, index, index);
    }
}
