/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.importer.SmImportFromFolder;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import java.io.File;
import java.util.concurrent.Callable;

/**
 * Importe un sous-modèle à partir d'un dossier
 */
public class ImportSousModeleCallable implements Callable<Boolean> {
  private final EMHProjet project;
  private final File drsoFile;
  private final String comment;
  private final ConnexionInformation connexionInfos = Lookup.getDefault().lookup(InstallationService.class).getConnexionInformation();

  /**
   * @param project le projet
   * @param drsoFile le fichier drso choisi
   * @param comment le commentaire
   */
  public ImportSousModeleCallable(EMHProjet project, File drsoFile, String comment) {
    this.project = project;
    this.drsoFile = drsoFile;
    this.comment = comment;
  }

  @Override
  public Boolean call() {
    SmImportFromFolder importAction = new SmImportFromFolder(project,connexionInfos);
    final CtuluLog log = importAction.importSousModele(drsoFile, comment);
    if (log.isNotEmpty()) {
      LogsDisplayer.displayError(log, NbBundle.getMessage(ImportSousModeleCallable.class, "ImportSousModeleAction.ActionName"));
    }
    return !log.containsErrorOrSevereError();
  }
}
