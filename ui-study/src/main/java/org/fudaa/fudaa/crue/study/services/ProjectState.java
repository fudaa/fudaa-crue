package org.fudaa.fudaa.crue.study.services;

/**
 * Utilise pour indiquer qu'une action d'ouverture/fermeture est totalement terminée.
 * Dans la perspective etude,toute modification sur l'etude est persistée directement dans le fichier et demande de recharger l'étude. Ces rechargements doivent
 * être silencieux pour certains services et ne doivent pas générer les mêmes actions lors d'un premier chargement d'étude.
 * Cet enum sert à différencier les 2 cas et les services qui veulent etre notifiés du (dé)chargement d'un projet doivent s'enregistrer sur le lookup ProjectState fourni
 * par {@link org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl}
 *
 * @see org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl
 *
 * @author deniger
 */
public enum ProjectState {

  OPENED
}
