/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.NbSheetCustom;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.study//StudyPropertiesTopComponent//EN",
        autostore = false)
@TopComponent.Description(preferredID = StudyPropertiesTopComponent.TOPCOMPONENT_ID,
        iconBase = "org/fudaa/fudaa/crue/study/icons/sous-modele_16.png",
        persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "study-bottomLeft", position = 4, openAtStartup = false)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.study.StudyPropertiesTopComponent")
@TopComponent.OpenActionRegistration(displayName = "#CTL_StudyPropertiesAction",
        preferredID = StudyPropertiesTopComponent.TOPCOMPONENT_ID)
@ActionReference(path = "Menu/Window/Study", position = 7, separatorBefore = 6)
public final class StudyPropertiesTopComponent extends NbSheetCustom {

  public static final String TOPCOMPONENT_ID = "StudyPropertiesTopComponent";

  public StudyPropertiesTopComponent() {
    super(true, PerspectiveEnum.STUDY);
    setName(NbBundle.getMessage(SousModeleListTopComponent.class, "CTL_StudyPropertiesAction"));
    setToolTipText(NbBundle.getMessage(SousModeleListTopComponent.class, "HINT_StudyPropertiesAction"));
  }

  @Override
  public int getPersistenceType() {
    return super.getPersistenceType();
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getTopComponentHelpCtxId("vueProprietes", PerspectiveEnum.STUDY));
  }

  void writeProperties(java.util.Properties p) {
    p.setProperty("version", "1.0");
  }

  void readProperties(java.util.Properties p) {
  }
}
