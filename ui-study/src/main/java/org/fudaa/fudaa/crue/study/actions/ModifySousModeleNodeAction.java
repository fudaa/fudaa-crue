/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.metier.etude.EMHInfosVersion;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHSousModele;
import org.fudaa.fudaa.crue.common.UserPreferencesSaver;
import org.fudaa.fudaa.crue.common.action.OneSelectionNodeAction;
import org.fudaa.fudaa.crue.options.services.InstallationService;
import org.fudaa.fudaa.crue.study.dialog.ManageManagerEMHContainerDialog;
import org.fudaa.fudaa.crue.study.node.UsedByNodeBuilder;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

/**
 * @author Chris
 */
public class ModifySousModeleNodeAction extends OneSelectionNodeAction {
  protected final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
  private final ConnexionInformation connexionInfos = Lookup.getDefault().lookup(InstallationService.class).getConnexionInformation();

  public ModifySousModeleNodeAction() {
    super(NbBundle.getMessage(CreateSousModeleNodeAction.class, "ModifySousModeleAction.ActionName"), true);
  }

  @Override
  protected boolean enable(Node activatedNodes) {
    if (UsedByNodeBuilder.nodeIsUsedByScenarioWithRun(activatedNodes)) {
      return false;
    }
    return activatedNodes.getLookup().lookup(ManagerEMHSousModele.class) != null;
  }

  @Override
  protected void performAction(Node activatedNodes) {
    final ManagerEMHSousModele sousModele = activatedNodes.getLookup().lookup(ManagerEMHSousModele.class);

    ManageManagerEMHContainerDialog dialog = new ManageManagerEMHContainerDialog(WindowManager.getDefault().getMainWindow(), true, sousModele, false, "modifierSousModele");

    dialog.setName(getClass().getName());
    UserPreferencesSaver.loadDialogLocationAndDimension(dialog, null);
    dialog.display();
    UserPreferencesSaver.saveLocationAndDimension(dialog, null);

    if (dialog.isOkClicked()) {
      final EMHInfosVersion infosVersion = sousModele.getInfosVersions();
      infosVersion.setAuteurCreation(connexionInfos.getCurrentUser());
      infosVersion.setDateCreation(connexionInfos.getCurrentDate());

      projetService.deepModification(new CallableTrue(), NbBundle.getMessage(getClass(), "ModifySousModeleAction.Log", sousModele.getNom()));
    }
  }
}
