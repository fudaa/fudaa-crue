/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.study.actions;

import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.services.ModellingScenarioService;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;

import javax.swing.*;

public final class OpenScenarioNodeAction extends NodeAction {
    final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
    private final ModellingScenarioService scenarioService = Lookup.getDefault().lookup(ModellingScenarioService.class);

    protected static ManagerEMHScenario getScenarioFromNodes(Node[] activatedNodes) {
        if (activatedNodes != null && activatedNodes.length == 1) {
            return activatedNodes[0].getLookup().lookup(ManagerEMHScenario.class);
        }
        return null;
    }

    /**
     * Cherche un ManagerEMHScenario dans la liste des nodes
     *
     * @param activatedNodes les noeuds activés
     * @return true si au moins un ManagerEMHScenario trouvé, false sinon
     */
    protected static boolean containsScenarioInNodes(Node[] activatedNodes) {
        if (activatedNodes != null && activatedNodes.length > 0) {
            for (Node activatedNode : activatedNodes) {
                if (activatedNode.getLookup().lookup(ManagerEMHScenario.class) != null) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected ManagerEMHScenario getScenario(Node[] activatedNodes) {
        return getScenarioFromNodes(activatedNodes);
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }

    @Override
    protected boolean enable(Node[] activatedNodes) {
        if (projetService.getSelectedProject() != null) {
            ManagerEMHScenario scenario = getScenario(activatedNodes);
            return (scenario != null) && scenarioService.getManagerScenarioLoaded() != scenario;
        }
        return false;
    }

    @Override
    protected void performAction(Node[] activatedNodes) {
        ManagerEMHScenario scenario = getScenario(activatedNodes);
        scenarioService.loadScenario(projetService.getSelectedProject(), scenario);
    }

    @Override
    public String getName() {
        return NbBundle.getMessage(OpenFichierCrueNodeAction.class, "OpenScenarioAction.Name");
    }

    @Override
    public JMenuItem getPopupPresenter() {
        return NodeHelper.useBoldFont(super.getPopupPresenter());
    }
}
