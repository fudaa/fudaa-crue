/*
 * Licence GPL Copyright
 */
package org.fudaa.dodico.crue.aoc;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author deniger
 */
public abstract class AbstractIOParentTest extends AbstractTestParent {
  protected final List<String> files;
  protected final Crue10FileFormat format;

  /**
   * @param format le format
   * @param file les fichiers a tester
   */
  public AbstractIOParentTest(final Crue10FileFormat format, final String... file) {
    super();
    this.files = Arrays.asList(file);
    this.format = format;
  }


  @Before
  public void checkFileArePresent() {
    for (final String file : files) {
      assertNotNull("file must exist " + file, getClass().getResource(file));
    }
  }


  @Test
  public void testValide() {
    assertNotNull(format);
    for (final String file : files) {
      final CtuluLog res = new CtuluLog();
      final boolean valide = format.isValide(file, res);
      if (res.containsErrorOrSevereError()) {
        res.printResume();
      }
      assertTrue(file, valide);
    }
  }
}
