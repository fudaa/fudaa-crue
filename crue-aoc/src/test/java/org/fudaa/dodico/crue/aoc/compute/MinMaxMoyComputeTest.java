package org.fudaa.dodico.crue.aoc.compute;

import org.junit.Test;

import static org.junit.Assert.*;

public class MinMaxMoyComputeTest {
  public static final double DELTA = 1e-10;

  @Test
  public void testInit() {
    MinMaxMoyCompute content = new MinMaxMoyCompute();
    assertTrue(content.isEmpty());
    assertFalse(content.isNotEmpty());
    assertEquals(0, content.getMoy(), DELTA);
  }

  @Test
  public void testMoy() {
    MinMaxMoyCompute content = new MinMaxMoyCompute();
    content.add(-5);
    content.add(5);
    assertFalse(content.isEmpty());
    assertTrue(content.isNotEmpty());
    assertEquals(0, content.getMoy(), DELTA);
    assertEquals(-5d, content.getMin(), DELTA);
    assertEquals(5d, content.getMax(), DELTA);
    content.add(3);
    assertEquals(1, content.getMoy(), DELTA);
    assertEquals(-5d, content.getMin(), DELTA);
    assertEquals(5d, content.getMax(), DELTA);

    content.add(0);
    assertEquals(3d / 4d, content.getMoy(), DELTA);
    assertEquals(-5d, content.getMin(), DELTA);
    assertEquals(5d, content.getMax(), DELTA);
  }
}
