package org.fudaa.dodico.crue.aoc.io;

import junit.framework.Assert;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageType;
import org.fudaa.dodico.crue.metier.aoc.EnumAocTypeCalageCritere;
import org.fudaa.dodico.crue.metier.aoc.EnumAocTypeCalageDonnees;
import org.junit.Test;

/**
 * Created by deniger on 28/06/2017.
 */
public class CrueDaoStructureAOCTest {
  /**
   * On s'assure que tous les enums sont bien convertis
   */
  @Test
  public void createTypeCalageDonneesMap() {
    final DualHashBidiMap typeCalageDonneesMap = CrueDaoStructureAOC.createTypeCalageDonneesMap();
    for (Enum e : EnumAocTypeCalageDonnees.values()) {
      Assert.assertTrue(typeCalageDonneesMap.containsKey(e));
    }
  }

  @Test
  public void createCalageTypeMap() {
    final DualHashBidiMap typeCalageDonneesMap = CrueDaoStructureAOC.createCalageTypeMap();
    for (Enum e : EnumAocCalageType.values()) {
      Assert.assertTrue(typeCalageDonneesMap.containsKey(e));
    }
  }

  /**
   * On s'assure que tous les enums sont bien convertis
   */
  @Test
  public void createTypeCalageCriteresMap() {
    final DualHashBidiMap typeCalageCriteresMap = CrueDaoStructureAOC.createTypeCalageCriteresMap();
    for (Enum e : EnumAocTypeCalageCritere.values()) {
      Assert.assertTrue(typeCalageCriteresMap.containsKey(e));
    }
  }
}
