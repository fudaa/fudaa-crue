package org.fudaa.dodico.crue.aoc.compute;

import org.fudaa.dodico.crue.metier.result.TimeSimuConverter;
import org.junit.Test;

import static org.junit.Assert.*;

public class RmseOnTimeCourbeComputeTest {
  @Test
  public void testSameValues() {
    CollectCompute collectObserved = new CollectCompute();
    CollectCompute collectComputed = new CollectCompute();
    collectComputed.addXY(0, 100);
    collectObserved.addXY(0, 100);
    collectComputed.addXY(3600, 200);
    collectObserved.addXY(3600, 200);
    collectComputed.addXY(7200, -200);
    collectObserved.addXY(7200, -200);
    assertEquals(0, new RmseOnTimeCourbeCompute().compute(collectObserved, collectObserved), 1e-12);
  }

  @Test
  public void testTwoDifferentsValuesAndSameTime() {
    CollectCompute collectObserved = new CollectCompute();
    CollectCompute collectComputed = new CollectCompute();
    collectComputed.addXY(0, 100);
    collectObserved.addXY(0, 100);

    final int yComputed3600 = 200;
    final int yObserved3600 = 500;
    collectComputed.addXY(3600, yComputed3600);
    collectObserved.addXY(3600, yObserved3600);

    final int yComputed7200 = -201;
    final int yObserved7200 = -200;
    collectComputed.addXY(7200, yComputed7200);
    collectObserved.addXY(7200, yObserved7200);

    final int nbValues = collectObserved.getNbValues();

    //ces 2 valeurs sont en dehors du scope
    collectComputed.addXY(-10, 1000);
    collectObserved.addXY(100000000, 1000);
    //-1 car une valeur sera ignorée

    final double delta3600 = yComputed3600 - yObserved3600;
    final double delta7200 = yComputed7200 - yObserved7200;
    double rmse = Math.sqrt((delta7200 * delta7200 + delta3600 * delta3600) / ((double) nbValues));
    assertEquals(rmse, new RmseOnTimeCourbeCompute().compute(collectObserved, collectComputed), 1e-12);
  }

  @Test
  public void testInterpolation() {
    //on construit les 2 lois avec une plate (observed) et une en triangle
    //  computed     0---------10----------20 ( valeur de 120 sur 10)
    //
    //  observbed    0----5----10----15----20

    CollectCompute collectObserved = new CollectCompute();
    final int yMin = 0;
    collectObserved.addXY(0, yMin);
    collectObserved.addXY(5, yMin);
    collectObserved.addXY(10, yMin);
    collectObserved.addXY(15, yMin);
    collectObserved.addXY(20, yMin);
    CollectCompute collectComputed = new CollectCompute();
    collectComputed.addXY(0, yMin);
    final double hauteurTriangle = 100d;
    collectComputed.addXY(10, yMin + hauteurTriangle);
    collectComputed.addXY(20, yMin);
    final int nbValues = collectObserved.getNbValues();

    //ces 2 valeurs sont en dehors du scope
    collectComputed.addXY(-10, 1000);
    collectObserved.addXY(100000000, 1000);
    //-1 car une valeur sera ignorée
    final double delta10 = hauteurTriangle;
    final double delta5 = hauteurTriangle / 2d;
    final double delta15 = hauteurTriangle / 2d;
    double rmse = Math.sqrt((delta5 * delta5 + delta10 * delta10 + delta15 * delta15) / ((double) nbValues));
    assertEquals(rmse, new RmseOnTimeCourbeCompute().compute(collectObserved, collectComputed), 1e-12);
  }

  @Test
  public void testExtractValuesToUseToInterpolate() {
    final RmseOnTimeCourbeCompute compute = new RmseOnTimeCourbeCompute();

    CollectCompute collectObserved = new CollectCompute();
    collectObserved.addXY(0, 100);
    collectObserved.addXY(3600, 200);
    collectObserved.addXY(3602, 200);
    collectObserved.addXY(7200, -200);

    CollectCompute collectComputed = new CollectCompute();
    collectComputed.addXY(10, 100);
    collectComputed.addXY(3600, 200);
    collectComputed.addXY(7200, -200);
    collectComputed.addXY(7203, -200);

    final long[] valueToInterpolate = compute.extractValuesToUseToInterpolate(compute.extractValues(collectObserved), compute.extractValues(collectComputed));
    //les valeurs seront 10, 3600, 3602 et 7200
    assertArrayEquals(new long[]{TimeSimuConverter.toMilis(10l), TimeSimuConverter.toMilis(3600l), TimeSimuConverter.toMilis(3602l), TimeSimuConverter.toMilis(7200l)}, valueToInterpolate);
  }

  @Test
  public void testExtractValues() {
    CollectCompute collectObserved = new CollectCompute();
    collectObserved.addXY(7200, -200);
    collectObserved.addXY(0, 100);
    collectObserved.addXY(3600, 200);
    collectObserved.addXY(10, -200);
    final RmseOnTimeCourbeCompute.ValueContent values = new RmseOnTimeCourbeCompute().extractValues(collectObserved);
    assertEquals(collectObserved.getNbValues(), values.getSize());
    assertEquals(TimeSimuConverter.toMilis(7200), values.getMax());
    assertEquals(0, values.getMin());
    assertEquals(TimeSimuConverter.toMilis(10), values.getValues().get(1).getT());
  }
}
