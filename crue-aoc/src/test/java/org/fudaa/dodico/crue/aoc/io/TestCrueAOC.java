/**
 * GPL v2
 */
package org.fudaa.dodico.crue.aoc.io;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.AbstractIOParentTest;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.aoc.projet.AocValidationGroupe;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierForTest;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageAlgorithme;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageType;
import org.fudaa.dodico.crue.metier.aoc.EnumAocTypeCalageCritere;
import org.fudaa.dodico.crue.metier.aoc.EnumAocTypeCalageDonnees;
import org.joda.time.LocalDateTime;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;

/**
 * @author deniger
 */
public class TestCrueAOC extends AbstractIOParentTest {
  private static final String FICHIER_TEST_XML = "/aoc/campagne.aoc.xml";
  private static final String FICHIER_TEST_WITH_PARAMETERS_XML = "/aoc/campagneWithParametresNumeriques.aoc.xml";

  public TestCrueAOC() {
    super(new CrueFileFormatBuilderAOC().getFileFormat(TestCoeurConfig.INSTANCE), FICHIER_TEST_XML, FICHIER_TEST_WITH_PARAMETERS_XML);
  }

  private static AocCampagne read(final File otfaFile) {
    final CrueAOCReaderWriter reader = new CrueAOCReaderWriter("1.2", TestCoeurConfig.INSTANCE.getCrueConfigMetier());
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    Assert.assertTrue(reader.isValide(otfaFile, log));
    final CrueIOResu<AocCampagne> result = reader.readXML(otfaFile, log, new CrueDataImpl(CrueConfigMetierForTest.DEFAULT));

    testAnalyser(log);

    return result.getMetier();
  }

  private void assertAocCampagneParametresNumeriques(final AocCampagne campagne) {
    assertDoubleEquals(0.9, campagne.getParametreNumeriques().getCoefAmplitude_Recuit());
    assertDoubleEquals(1.1, campagne.getParametreNumeriques().getTemperatureInitiale_Recuit());
    assertDoubleEquals(1.2, campagne.getParametreNumeriques().getTemperatureFinale_Recuit());
    assertDoubleEquals(1.3, campagne.getParametreNumeriques().getSeuilRMSEZ_CriterePermTransAOC());
    assertDoubleEquals(1.4, campagne.getParametreNumeriques().getSeuilRMSEQ_CritereTransAOC());
    assertDoubleEquals(1.5, campagne.getParametreNumeriques().getSeuilTarriveeMax_CritereTransAOC());
    assertDoubleEquals(1.6, campagne.getParametreNumeriques().getSeuilZmax_CritereTransAOC());
    assertDoubleEquals(1.7, campagne.getParametreNumeriques().getSeuilQmax_CritereTransAOC());
    assertDoubleEquals(1.8, campagne.getParametreNumeriques().getSeuilVol_CritereTransAOC());
  }

  private void assertAocCampagne(final AocCampagne campagne) {
    Assert.assertEquals("Commentaire de la campagne AOC", campagne.getCommentaire());
    Assert.assertEquals("User", campagne.getAuteurCreation());
    Assert.assertEquals(new LocalDateTime(2017, 4, 26, 12, 4, 27), campagne.getDateCreation());
    Assert.assertEquals("User modif", campagne.getAuteurModification());
    Assert.assertEquals(new LocalDateTime(2017, 4, 27, 11, 57, 25), campagne.getDateModification());
    Assert.assertEquals("C:\\PathEtude", campagne.getEtudeChemin());
    Assert.assertEquals("NomScenario", campagne.getNomScenario());
    Assert.assertTrue(campagne.isLogAll());
    Assert.assertEquals(EnumAocTypeCalageCritere.ERREUR_QUADRATIQUE, campagne.getTypeCalageCritere());
    Assert.assertEquals(EnumAocTypeCalageDonnees.PERMANENT, campagne.getTypeCalageDonnees());
    Assert.assertEquals(EnumAocCalageType.TEST_REPETABILITE, campagne.getCalage().getOperation());
    Assert.assertEquals(EnumAocCalageAlgorithme.MONTE_CARLO, campagne.getCalage().getAlgorithme());
    Assert.assertEquals(3, campagne.getCalage().getNombreIterationSeul());
    Assert.assertEquals(4, campagne.getCalage().getNombreIterationTir());
    Assert.assertEquals(5, campagne.getCalage().getNombreTir());
    Assert.assertEquals(6, campagne.getCalage().getNombreIterationValidationCroisee());
    assertDoubleEquals(0.6, campagne.getCalage().getPonderationApprentissage());
    assertDoubleEquals(0.4, campagne.getCalage().getPonderationValidation());

    Assert.assertEquals(2, campagne.getDonnees().getLoisCalculsPermanents().getLois().size());
//        Cc_P01
    Assert.assertEquals("Cc_P01", campagne.getDonnees().getLoisCalculsPermanents().getLois().get(0).getCalculRef());
    Assert.assertEquals("Loi 1", campagne.getDonnees().getLoisCalculsPermanents().getLois().get(0).getLoiRef());
    Assert.assertEquals(4, campagne.getDonnees().getLoisCalculsPermanents().getLois().get(0).getPonderation());

//        Cc_P01
    Assert.assertEquals("Cc_P02", campagne.getDonnees().getLoisCalculsPermanents().getLois().get(1).getCalculRef());
    Assert.assertEquals("Loi 2", campagne.getDonnees().getLoisCalculsPermanents().getLois().get(1).getLoiRef());
    Assert.assertEquals(1, campagne.getDonnees().getLoisCalculsPermanents().getLois().get(1).getPonderation());

//        transitoire
    Assert.assertEquals(3, campagne.getDonnees().getLoisCalculsTransitoires().getLois().size());
//        Cc_T01
    Assert.assertEquals("Cc_T01", campagne.getDonnees().getLoisCalculsTransitoires().getLois().get(0).getCalculRef());
    Assert.assertEquals("LoiT 1", campagne.getDonnees().getLoisCalculsTransitoires().getLois().get(0).getLoiRef());
    Assert.assertEquals("St_Section 1", campagne.getDonnees().getLoisCalculsTransitoires().getLois().get(0).getSectionRef());
    Assert.assertEquals(1, campagne.getDonnees().getLoisCalculsTransitoires().getLois().get(0).getPonderation());
//        Cc_T02
    Assert.assertEquals("Cc_T02", campagne.getDonnees().getLoisCalculsTransitoires().getLois().get(1).getCalculRef());
    Assert.assertEquals("LoiT 2", campagne.getDonnees().getLoisCalculsTransitoires().getLois().get(1).getLoiRef());
    Assert.assertEquals("St_Section 2", campagne.getDonnees().getLoisCalculsTransitoires().getLois().get(1).getSectionRef());
    Assert.assertEquals(2, campagne.getDonnees().getLoisCalculsTransitoires().getLois().get(1).getPonderation());
//        Cc_T03
    Assert.assertEquals("Cc_T03", campagne.getDonnees().getLoisCalculsTransitoires().getLois().get(2).getCalculRef());
    Assert.assertEquals("LoiT 3", campagne.getDonnees().getLoisCalculsTransitoires().getLois().get(2).getLoiRef());
    Assert.assertEquals("St_Section 3", campagne.getDonnees().getLoisCalculsTransitoires().getLois().get(2).getSectionRef());
    Assert.assertEquals(3, campagne.getDonnees().getLoisCalculsTransitoires().getLois().get(2).getPonderation());

    //Strickler
    Assert.assertEquals(4, campagne.getDonnees().getLoisStrickler().getLois().size());
    //Loi 1
    Assert.assertEquals("Loi 1", campagne.getDonnees().getLoisStrickler().getLois().get(0).getLoiRef());
    Assert.assertEquals(0, campagne.getDonnees().getLoisStrickler().getLois().get(0).getMin());
    Assert.assertEquals(1, campagne.getDonnees().getLoisStrickler().getLois().get(0).getIni());
    Assert.assertEquals(3, campagne.getDonnees().getLoisStrickler().getLois().get(0).getMax());
    //Loi 2
    Assert.assertEquals("Loi 2", campagne.getDonnees().getLoisStrickler().getLois().get(1).getLoiRef());
    Assert.assertEquals(10, campagne.getDonnees().getLoisStrickler().getLois().get(1).getMin());
    Assert.assertEquals(15, campagne.getDonnees().getLoisStrickler().getLois().get(1).getIni());
    Assert.assertEquals(20, campagne.getDonnees().getLoisStrickler().getLois().get(1).getMax());
    //Loi 3
    Assert.assertEquals("Loi 3", campagne.getDonnees().getLoisStrickler().getLois().get(2).getLoiRef());
    Assert.assertEquals(23, campagne.getDonnees().getLoisStrickler().getLois().get(2).getMin());
    Assert.assertEquals(28, campagne.getDonnees().getLoisStrickler().getLois().get(2).getIni());
    Assert.assertEquals(33, campagne.getDonnees().getLoisStrickler().getLois().get(2).getMax());
    //Loi 4
    Assert.assertEquals("Loi 4", campagne.getDonnees().getLoisStrickler().getLois().get(3).getLoiRef());
    Assert.assertEquals(10, campagne.getDonnees().getLoisStrickler().getLois().get(3).getMin());
    Assert.assertEquals(15, campagne.getDonnees().getLoisStrickler().getLois().get(3).getIni());
    Assert.assertEquals(20, campagne.getDonnees().getLoisStrickler().getLois().get(3).getMax());

    //liste calcul
    Assert.assertEquals(2, campagne.getDonnees().getListeCalculs().getParamCalcs().size());
    //Cc_PO1
    Assert.assertEquals("Cc_P01", campagne.getDonnees().getListeCalculs().getParamCalcs().get(0).getCalculRef());
    Assert.assertEquals("Nd_N1", campagne.getDonnees().getListeCalculs().getParamCalcs().get(0).getEmhRef());
    assertDoubleEquals(0.1, campagne.getDonnees().getListeCalculs().getParamCalcs().get(0).getDeltaQ());
    //Cc_PO2
    Assert.assertEquals("Cc_P02", campagne.getDonnees().getListeCalculs().getParamCalcs().get(1).getCalculRef());
    Assert.assertEquals("Nd_N2", campagne.getDonnees().getListeCalculs().getParamCalcs().get(1).getEmhRef());
    assertDoubleEquals(0.3, campagne.getDonnees().getListeCalculs().getParamCalcs().get(1).getDeltaQ());

    //Validation croisee
    Assert.assertEquals(2, campagne.getDonnees().getValidationCroisee().getGroupes().size());
    AocValidationGroupe validationGroupe = campagne.getDonnees().getValidationCroisee().getGroupes().get(0);
    Assert.assertEquals(2, validationGroupe.getEffectifApprentissage());
    Assert.assertEquals("GR_1", validationGroupe.getId());
    Assert.assertEquals("Groupe 1", validationGroupe.getNom());
    Assert.assertEquals("Un commentaire", validationGroupe.getCommentaire());

    Assert.assertEquals(2, validationGroupe.getLois().size());
    Assert.assertEquals("Loi 3", validationGroupe.getLois().get(0).getLoiRef());
    Assert.assertEquals("Loi 1", validationGroupe.getLois().get(1).getLoiRef());

    validationGroupe = campagne.getDonnees().getValidationCroisee().getGroupes().get(1);
    Assert.assertEquals(3, validationGroupe.getEffectifApprentissage());
    Assert.assertEquals("GR_2", validationGroupe.getId());
    Assert.assertEquals("Groupe 2", validationGroupe.getNom());
    Assert.assertEquals("", validationGroupe.getCommentaire());

    Assert.assertEquals(3, validationGroupe.getLois().size());
    Assert.assertEquals("Loi 1", validationGroupe.getLois().get(0).getLoiRef());
    Assert.assertEquals("Loi 5", validationGroupe.getLois().get(1).getLoiRef());
    Assert.assertEquals("Loi 2", validationGroupe.getLois().get(2).getLoiRef());
  }

  @Test
  public void testLecture() {
    final File aocFile = this.getTestFile(FICHIER_TEST_XML);
    final AocCampagne campagne = read(aocFile);

    assertAocCampagne(campagne);
  }

  @Test
  public void testLectureWithParameters() {
    final File aocFile = this.getTestFile(FICHIER_TEST_WITH_PARAMETERS_XML);
    final AocCampagne campagne = read(aocFile);

    assertAocCampagne(campagne);
    assertAocCampagneParametresNumeriques(campagne);
  }

  @Test
  public void testLectureHydro() {
    final File aocFile = this.getTestFile("/aoc/campagne-hydro.aoc.xml");
    final AocCampagne campagne = read(aocFile);
    Assert.assertEquals(EnumAocTypeCalageDonnees.TRANSITOIRE_HYDROGRAMME, campagne.getTypeCalageDonnees());
    aocFile.delete();
  }

  @Test
  public void testEcriture() {
    final File aocFile = this.getTestFile(FICHIER_TEST_XML);
    final File newOtfaFile = new File(aocFile.getParentFile(), "campagne.new.aoc.xml");

    AocCampagne campagne = read(aocFile);

    final CrueAOCReaderWriter writer = new CrueAOCReaderWriter("1.2", null);
    final CrueIOResu<AocCampagne> resu = new CrueIOResu<>(campagne);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);

    Assert.assertTrue(writer.writeXMLMetier(resu, newOtfaFile, log, CrueConfigMetierForTest.DEFAULT));

    testAnalyser(log);

    campagne = read(newOtfaFile);
    assertAocCampagne(campagne);
  }

  @Test
  public void testEcritureWithParametresNumeriques() {
    final File aocFile = this.getTestFile(FICHIER_TEST_WITH_PARAMETERS_XML);
    final File newOtfaFile = new File(aocFile.getParentFile(), "campagne.newWithParameters.aoc.xml");
    AocCampagne campagne = read(aocFile);
    final CrueAOCReaderWriter writer = new CrueAOCReaderWriter("1.2", null);
    final CrueIOResu<AocCampagne> resu = new CrueIOResu<>(campagne);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);

    Assert.assertTrue(writer.writeXMLMetier(resu, newOtfaFile, log, CrueConfigMetierForTest.DEFAULT));

    testAnalyser(log);

    campagne = read(newOtfaFile);
    assertAocCampagne(campagne);
    assertAocCampagneParametresNumeriques(campagne);
  }

  private File getTestFile(final String name) {
    final URL url = TestCrueAOC.class.getResource(name);
    final File res = new File(createTempDir(), StringUtils.substringAfterLast(name, "/"));
    try {
      CtuluLibFile.copyStream(url.openStream(), new FileOutputStream(res), true, true);
    } catch (final Exception e) {
      e.printStackTrace();
      Assert.fail();
    }

    return res;
  }
}
