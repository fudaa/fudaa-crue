package org.fudaa.dodico.crue.aoc.projet;

import org.fudaa.dodico.crue.metier.aoc.EnumAocTypeCalageDonnees;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by deniger on 28/06/2017.
 */
public class EnumAocTypeCalageDonneesTest {
  @Test
  public void geti18n() {
    for (EnumAocTypeCalageDonnees en : EnumAocTypeCalageDonnees.values()) {
      assertNotNull(en.geti18n());
    }
  }

  @Test
  public void geti18nLongName() {
    for (EnumAocTypeCalageDonnees en : EnumAocTypeCalageDonnees.values()) {
      assertNotNull(en.geti18nLongName());
    }
  }
}
