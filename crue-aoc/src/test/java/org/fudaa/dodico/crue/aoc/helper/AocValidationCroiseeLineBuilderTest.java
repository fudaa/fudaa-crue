package org.fudaa.dodico.crue.aoc.helper;

import org.fudaa.dodico.crue.aoc.projet.AocValidationCroisee;
import org.fudaa.dodico.crue.aoc.projet.AocValidationGroupe;
import org.fudaa.dodico.crue.aoc.projet.AocValidationLoi;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class AocValidationCroiseeLineBuilderTest {
  @Test
  public void testGeneratePermutations() {
    AocValidationCroisee groupe = new AocValidationCroisee();
    AocValidationGroupe groupe1 = new AocValidationGroupe();
    groupe1.setEffectifApprentissage(1);
    groupe1.getLois().add(new AocValidationLoi("L1"));

    AocValidationGroupe groupe2 = new AocValidationGroupe();
    groupe2.setEffectifApprentissage(2);
    groupe2.getLois().add(new AocValidationLoi("L2"));
    groupe2.getLois().add(new AocValidationLoi("L3"));
    groupe2.getLois().add(new AocValidationLoi("L4"));

    AocValidationGroupe groupe3 = new AocValidationGroupe();
    groupe3.setEffectifApprentissage(1);
    groupe3.getLois().add(new AocValidationLoi("L5"));
    groupe3.getLois().add(new AocValidationLoi("L6"));
    groupe3.getLois().add(new AocValidationLoi("L7"));
    groupe3.getLois().add(new AocValidationLoi("L8"));

    groupe.getGroupes().add(groupe1);
    groupe.getGroupes().add(groupe2);
    groupe.getGroupes().add(groupe3);

    AocValidationCroiseeLineBuilder builder = new AocValidationCroiseeLineBuilder();
    final List<AocValidationCroiseeLine> aocValidationCroiseeLines = builder.generatePermutations(groupe);
    assertEquals(12, aocValidationCroiseeLines.size());

    testValues(aocValidationCroiseeLines.get(0).getLigneEauApprentissage(), "L1", "L2", "L3", "L5");
    testValues(aocValidationCroiseeLines.get(0).getLigneEauValidation(), "L4", "L6", "L7", "L8");

    testValues(aocValidationCroiseeLines.get(1).getLigneEauApprentissage(), "L1", "L2", "L3", "L6");
    testValues(aocValidationCroiseeLines.get(1).getLigneEauValidation(), "L4", "L5", "L7", "L8");
  }

  private void testValues(List<String> values, String... expected) {
    assertEquals(expected.length, values.size());
    for (int i = 0; i < expected.length; i++) {
      assertEquals(values.get(i), expected[i]);
    }
  }

  @Test
  public void testGeneratePermutationByGroupSizeOneWithEffectifOne() {
    AocValidationGroupe groupeWithOne = new AocValidationGroupe();
    final String loiRef = "loi1";
    groupeWithOne.getLois().add(new AocValidationLoi(loiRef));
    AocValidationCroiseeLineBuilder builder = new AocValidationCroiseeLineBuilder();
    final AocValidationCroiseeLineBuilder.PermutationByGroupGamme PermutationByGroup = builder.generatePermutationByGroup(groupeWithOne);
    assertEquals(1, PermutationByGroup.getSize());
    assertEquals(1, PermutationByGroup.getPermutation(0).getSize());
    assertEquals(loiRef, PermutationByGroup.getPermutation(0).getValue(0));
  }

  @Test
  public void testGeneratePermutationByGroupSizeFourWithEffectifOne() {
    AocValidationGroupe groupeWithFourLoi = new AocValidationGroupe();
    final int size = 4;
    for (int i = 0; i < size; i++) {
      groupeWithFourLoi.getLois().add(new AocValidationLoi("loiRef" + i));
    }
    AocValidationCroiseeLineBuilder builder = new AocValidationCroiseeLineBuilder();
    final AocValidationCroiseeLineBuilder.PermutationByGroupGamme PermutationByGroup = builder.generatePermutationByGroup(groupeWithFourLoi);
    assertEquals(size, PermutationByGroup.getSize());
    for (int i = 0; i < size; i++) {
      assertEquals(1, PermutationByGroup.getPermutation(i).getSize());
      assertEquals("loiRef" + i, PermutationByGroup.getPermutation(i).getValue(0));
    }
  }

  @Test
  public void testGeneratePermutationByGroupSizeFiveWithEffectifTwo() {
    AocValidationGroupe groupeWithFourLoi = new AocValidationGroupe();
    groupeWithFourLoi.setEffectifApprentissage(2);
    final int size = 5;
    for (int i = 0; i < size; i++) {
      groupeWithFourLoi.getLois().add(new AocValidationLoi("loiRef" + i));
    }
    AocValidationCroiseeLineBuilder builder = new AocValidationCroiseeLineBuilder();
    final AocValidationCroiseeLineBuilder.PermutationByGroupGamme PermutationByGroup = builder.generatePermutationByGroup(groupeWithFourLoi);
    //on va avoir
    //0,1 0,2 0,3 0,4 1,2 1,3 1,4 2,3 2,4 3,4
    assertEquals(10, PermutationByGroup.getSize());
    for (int i = 0; i < 10; i++) {
      assertEquals(2, PermutationByGroup.getPermutation(i).getSize());
    }

    assertEquals("loiRef0", PermutationByGroup.getPermutation(0).getValue(0));
    assertEquals("loiRef1", PermutationByGroup.getPermutation(0).getValue(1));

    assertEquals("loiRef3", PermutationByGroup.getPermutation(9).getValue(0));
    assertEquals("loiRef4", PermutationByGroup.getPermutation(9).getValue(1));
  }

  @Test
  public void testGeneratePermutationByGroupSizeSixWithEffectifThree() {
    AocValidationGroupe groupeWithFourLoi = new AocValidationGroupe();
    groupeWithFourLoi.setEffectifApprentissage(3);
    final int size = 6;
    for (int i = 0; i < size; i++) {
      groupeWithFourLoi.getLois().add(new AocValidationLoi("loiRef" + i));
    }
    AocValidationCroiseeLineBuilder builder = new AocValidationCroiseeLineBuilder();
    final AocValidationCroiseeLineBuilder.PermutationByGroupGamme PermutationByGroup = builder.generatePermutationByGroup(groupeWithFourLoi);
    //on va avoir
    //0,1,2|0,1,3|0,1,4|0,1,5|0,2,3|0,2,4|0,2,5|0,3,4|0,3,5|0,4,5|1,2,3|1,2,4|1,2,5|1,3,4|1,3,5|1,4,5|2,3,4|2,3,5|2,4,5|3,4,5
    assertEquals(20, PermutationByGroup.getSize());
    for (int i = 0; i < 20; i++) {
      assertEquals(3, PermutationByGroup.getPermutation(i).getSize());
    }

    assertEquals("loiRef0", PermutationByGroup.getPermutation(0).getValue(0));
    assertEquals("loiRef1", PermutationByGroup.getPermutation(0).getValue(1));
    assertEquals("loiRef2", PermutationByGroup.getPermutation(0).getValue(2));

    assertEquals("loiRef0", PermutationByGroup.getPermutation(9).getValue(0));
    assertEquals("loiRef4", PermutationByGroup.getPermutation(9).getValue(1));
    assertEquals("loiRef5", PermutationByGroup.getPermutation(9).getValue(2));

    assertEquals("loiRef1", PermutationByGroup.getPermutation(10).getValue(0));
    assertEquals("loiRef2", PermutationByGroup.getPermutation(10).getValue(1));
    assertEquals("loiRef3", PermutationByGroup.getPermutation(10).getValue(2));

    assertEquals("loiRef3", PermutationByGroup.getPermutation(19).getValue(0));
    assertEquals("loiRef4", PermutationByGroup.getPermutation(19).getValue(1));
    assertEquals("loiRef5", PermutationByGroup.getPermutation(19).getValue(2));
  }
}
