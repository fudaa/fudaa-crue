package org.fudaa.dodico.crue.aoc.exec;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.io.CrueFileFormatBuilderAOC;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageAlgorithme;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageType;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openide.util.Exceptions;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class AocExecTestHelper {
  final static String scenarioName = "Sc_DM2013_cconc_zone";


  public static File createAocProject(File dir,final EnumAocCalageType type, final EnumAocCalageAlgorithme algo) throws IOException {
    final File target = new File(dir, "aoc");
    target.mkdirs();
    assertTrue(target.isDirectory());
    AbstractTestParent.exportZip(target, "/aoc/Etu_DM2013_Conc.zip");
    final File etuFile = new File(target, "Etu_DM2013_Conc.etu.xml");
    final File aocDir = new File(target, "aoc");
    final String campagneName = "campagne-template.aoc.xml";

    aocDir.mkdirs();
    assertTrue(aocDir.isDirectory());
    final File aocFile = new File(aocDir, campagneName);
    aocDir.setWritable(true);
    final File campagne = CtuluLibFile.getFileFromJar("/aoc/" + campagneName, aocFile);
    assertTrue(campagne.exists());
    final String typeToken = "@Type@";
    final String algoToken = "@Algorithme@";
    final String cheminToken = "@Chemin@";
    final String nomScenarioToken = "@NomScenario@";
    String fileContent = FileUtils.readFileToString(aocFile);
    fileContent = StringUtils.replace(fileContent, typeToken, type.getIdentifiant());
    fileContent = StringUtils.replace(fileContent, algoToken, algo.getIdentifiant());
    fileContent = StringUtils.replace(fileContent, cheminToken, etuFile.getAbsolutePath());
    fileContent = StringUtils.replace(fileContent, nomScenarioToken, scenarioName);
    FileUtils.writeStringToFile(aocFile, fileContent);
    final Crue10FileFormat<AocCampagne> fileFormat = new CrueFileFormatBuilderAOC().getFileFormat(TestCoeurConfig.INSTANCE_1_2);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    fileFormat.read(aocFile, log, null);
    assertFalse(log.containsErrorOrSevereError());
    return aocFile;
  }
}
