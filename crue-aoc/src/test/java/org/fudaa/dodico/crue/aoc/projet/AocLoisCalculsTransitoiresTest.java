package org.fudaa.dodico.crue.aoc.projet;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AocLoisCalculsTransitoiresTest {
  @Test
  public void setLois() {
    AocLoisCalculsTransitoires aocLoisCalculsTransitoires = new AocLoisCalculsTransitoires();

    List<AocLoiCalculTransitoire> lois = new ArrayList<>();
    final AocLoiCalculTransitoire loiCalculTransitoire1 = new AocLoiCalculTransitoire("calculRef1", "loiRef1", "sectionRef1", 2);
    lois.add(loiCalculTransitoire1);

    aocLoisCalculsTransitoires.setClonedLois(lois);

    final List<AocLoiCalculTransitoire> loisFromObject = aocLoisCalculsTransitoires.getLois();
    Assert.assertEquals(1, loisFromObject.size());
    Assert.assertFalse("lois is cloned", loisFromObject == lois);
    Assert.assertFalse("lois is cloned", loisFromObject.get(0) == lois.get(0));
  }

  @Test
  public void addLoiCalculTransitoire() {
    AocLoisCalculsTransitoires aocLoisCalculsTransitoires = new AocLoisCalculsTransitoires();
    aocLoisCalculsTransitoires.addLoiCalculTransitoire("calculRefInit", "loiRefInit", "sectionRefInit", 3);
    Assert.assertEquals(1, aocLoisCalculsTransitoires.getLois().size());
    Assert.assertEquals("calculRefInit", aocLoisCalculsTransitoires.getLois().get(0).getCalculRef());
    Assert.assertEquals("loiRefInit", aocLoisCalculsTransitoires.getLois().get(0).getLoiRef());
    Assert.assertEquals("sectionRefInit", aocLoisCalculsTransitoires.getLois().get(0).getSectionRef());
  }
}
