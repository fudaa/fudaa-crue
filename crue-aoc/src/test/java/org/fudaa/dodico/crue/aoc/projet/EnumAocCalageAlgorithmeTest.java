package org.fudaa.dodico.crue.aoc.projet;

import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageAlgorithme;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by deniger on 28/06/2017.
 */
public class EnumAocCalageAlgorithmeTest {
  @Test
  public void geti18n() {
    for (EnumAocCalageAlgorithme en : EnumAocCalageAlgorithme.values()) {
      assertNotNull(en.geti18n());
    }
  }

  @Test
  public void geti18nLongName() {
    for (EnumAocCalageAlgorithme en : EnumAocCalageAlgorithme.values()) {
      assertNotNull(en.geti18nLongName());
    }
  }
}
