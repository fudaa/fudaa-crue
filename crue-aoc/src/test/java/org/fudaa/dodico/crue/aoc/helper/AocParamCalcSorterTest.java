package org.fudaa.dodico.crue.aoc.helper;

import org.fudaa.dodico.crue.aoc.projet.AocParamCalc;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class AocParamCalcSorterTest {
    @Test
    public void testGet() {
        AocParamCalc c1_1 = new AocParamCalc("Cc_1", "emh1", 1d);
        AocParamCalc c1_2 = new AocParamCalc("Cc_1", "emh2", 2d);
        AocParamCalc c2_1 = new AocParamCalc("Cc_2", "emh1", 3d);
        AocParamCalcSorter sorter = new AocParamCalcSorter(Arrays.asList(c1_1, c1_2, c2_1));
        assertTrue(c1_1 == sorter.getForCalcul("Cc_1").getAocParamCalc("emh1"));
        assertTrue(c1_2 == sorter.getForCalcul("Cc_1").getAocParamCalc("emh2"));
        assertTrue(c2_1 == sorter.getForCalcul("Cc_2").getAocParamCalc("emh1"));
        assertTrue(c2_1 == sorter.get("Cc_2", "emh1"));
        assertNull(sorter.get("Cc_3", "emh1"));

        assertEquals(2, sorter.getNbCalc());
        assertEquals(2, sorter.getForCalcul("Cc_1").getNbEmh());
        assertEquals(1, sorter.getForCalcul("Cc_2").getNbEmh());
    }

    @Test
    public void testGetAndRemove() {
        AocParamCalc c1_1 = new AocParamCalc("Cc_1", "emh1", 1d);
        AocParamCalc c1_2 = new AocParamCalc("Cc_1", "emh2", 2d);
        AocParamCalc c2_1 = new AocParamCalc("Cc_2", "emh1", 3d);
        AocParamCalcSorter sorter = new AocParamCalcSorter(Arrays.asList(c1_1, c1_2, c2_1));
        assertTrue(c1_1 == sorter.getAndRemove("Cc_1", "emh1"));
        assertNull(sorter.getAndRemove("Cc_1", "emh1"));
        assertTrue(c1_2 == sorter.getAndRemove("Cc_1", "emh2"));
        assertNull(sorter.getAndRemove("Cc_1", "emh2"));
        final List<AocParamCalc> aocParamCalcs = sorter.getAocParamCalcs();
        assertEquals(1, aocParamCalcs.size());
        assertTrue(c2_1 == aocParamCalcs.get(0));
    }
}
