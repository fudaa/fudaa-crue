package org.fudaa.dodico.crue.aoc.projet;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AocLoisCalculsPermanentsTest {
  @Test
  public void setClonedLois() {
    AocLoisCalculsPermanents aocLoisCalculsTransitoires = new AocLoisCalculsPermanents();

    List<AocLoiCalculPermanent> lois = new ArrayList<>();
    final AocLoiCalculPermanent loiCalculPermanent = new AocLoiCalculPermanent("calculRef1", "loiRef1", 2);
    lois.add(loiCalculPermanent);

    aocLoisCalculsTransitoires.setClonedLois(lois);

    final List<AocLoiCalculPermanent> loisFromObject = aocLoisCalculsTransitoires.getLois();
    Assert.assertEquals(1, loisFromObject.size());
    Assert.assertFalse("lois is cloned", loisFromObject == lois);
    Assert.assertFalse("lois is cloned", loisFromObject.get(0) == lois.get(0));
  }

  @Test
  public void addLoiCalculPermanent() throws Exception {
    AocLoisCalculsPermanents aocLoisCalculsPermanents = new AocLoisCalculsPermanents();
    aocLoisCalculsPermanents.addLoiCalculPermanent("calculRefInit", "loiRefInit", 3);
    Assert.assertEquals(1, aocLoisCalculsPermanents.getLois().size());
    Assert.assertEquals("calculRefInit", aocLoisCalculsPermanents.getLois().get(0).getCalculRef());
    Assert.assertEquals("loiRefInit", aocLoisCalculsPermanents.getLois().get(0).getLoiRef());
  }
}
