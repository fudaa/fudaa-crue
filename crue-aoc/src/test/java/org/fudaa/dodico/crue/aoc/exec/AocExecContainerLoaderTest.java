package org.fudaa.dodico.crue.aoc.exec;

import org.apache.commons.io.FileUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageAlgorithme;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageType;
import org.fudaa.dodico.crue.common.ConnexionInformationFixed;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.common.transformer.LogsHelper;
import org.fudaa.dodico.crue.config.coeur.CoeurManagerForTest;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openide.util.Exceptions;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class AocExecContainerLoaderTest  {

  static File target = null;

  @BeforeClass
  public static void prepare() {
    try {
      target = CtuluLibFile.createTempDir();
      assertTrue(target.exists());
    } catch (final IOException ex) {
      Exceptions.printStackTrace(ex);
    }
  }

  @AfterClass
  public static void cleanUp() {
    try {
      FileUtils.deleteDirectory(target);
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testLoadAndBuild() throws IOException {
    final File aocFile = AocExecTestHelper.createAocProject(target,EnumAocCalageType.SEUL, EnumAocCalageAlgorithme.MONTE_CARLO);
    AocExecContainerLoader builder = new AocExecContainerLoader(CoeurManagerForTest.createDefault());

    //test du chargement
    CrueOperationResult<AocExecContainer> result = builder.load(aocFile);
    if (result.getLogs().containsError()) {
      System.err.println(LogsHelper.toHtml(result.getLogs().createCleanGroup(), null, true));
    }
    assertNotNull(result.getResult());
    assertNotNull(result.getResult().getProjet());
    assertFalse(result.getLogs().containsError());
    LocalDateTime dateTime = new LocalDateTime(new DateTime(2017, 05, 20, 19, 45, 1, 1));
    ConnexionInformationFixed connexionInformationFixed = new ConnexionInformationFixed(dateTime, "user");
    final File parent = aocFile.getParentFile();

    assertEquals(1, parent.listFiles().length);
    int nbScenarioBeforeCopy = result.getResult().getProjet().getListeScenarios().size();
    //on teste la creation du scenario.
    testBuild(builder, result, connexionInformationFixed, parent, nbScenarioBeforeCopy);
  }

  public void testBuild(AocExecContainerLoader builder, CrueOperationResult<AocExecContainer> result, ConnexionInformationFixed connexionInformationFixed, File parent,
                        int nbScenarioBeforeCopy) {
    //test de la construction: dossier + scenario par copie profonde:
    result = builder.build(result, connexionInformationFixed);
    if (result.getLogs().containsError()) {
      System.err.println(LogsHelper.toHtml(result.getLogs().createCleanGroup(), null, true));
    }
    assertFalse(result.getLogs().containsError());
    assertFalse(result.getLogs().containsFatalError());

    assertEquals(2, parent.listFiles().length);
    File rep = new File(parent, EnumAocCalageAlgorithme.MONTE_CARLO.getIdentifiantShort() + "_20170520 19-45-01");
    assertTrue(rep.isDirectory());

    final AocExecContainer aocExecContainer = result.getResult();
    assertNotNull(aocExecContainer.getProjet());
    assertNotNull(aocExecContainer.getManagerScenarioReference());
    assertNotNull(aocExecContainer.getManagerScenarioCible());
    assertEquals("Sc_DM2013_cconc_zone_MC", aocExecContainer.getManagerScenarioCible().getNom());
    assertFalse(aocExecContainer.getManagerScenarioCible().hasRun());

    //on a un scenario de plus..
    assertEquals(nbScenarioBeforeCopy + 1, aocExecContainer.getProjet().getListeScenarios().size());
  }
}
