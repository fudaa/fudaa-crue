package org.fudaa.dodico.crue.aoc.projet;

import org.fudaa.dodico.crue.metier.aoc.EnumAocTypeCalageCritere;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by deniger on 28/06/2017.
 */
public class EnumAocTypeCalageCritereTest {
  @Test
  public void geti18n() {
    for (EnumAocTypeCalageCritere en : EnumAocTypeCalageCritere.values()) {
      assertNotNull(en.geti18n());
    }
  }

  @Test
  public void geti18nLongName() {
    for (EnumAocTypeCalageCritere en : EnumAocTypeCalageCritere.values()) {
      assertNotNull(en.geti18nLongName());
    }
  }
}
