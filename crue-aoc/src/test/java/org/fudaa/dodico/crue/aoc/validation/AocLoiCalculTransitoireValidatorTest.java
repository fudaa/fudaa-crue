package org.fudaa.dodico.crue.aoc.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculTransitoire;
import org.fudaa.dodico.crue.validation.ValidatingItem;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AocLoiCalculTransitoireValidatorTest {
    @Test
    public void testValidate() {
        String[] availableSections = new String[]{"St_1", "St_2"};
        String[] availableLois = new String[]{"LoiTZ_1", "LoiTQ_1"};
        String[] availableCalculs = new String[]{"Cc_T1", "Cc_T2", "Cc_T3"};
        List<ValidatingItem<AocLoiCalculTransitoire>> nodes = new ArrayList<>();
        nodes.add(new ValidatingItem(new AocLoiCalculTransitoire("Cc_T1", "LoiZ_1", "St_10", 0), null));
        nodes.add(new ValidatingItem(new AocLoiCalculTransitoire("Cc_T3", "LoiTQ_1", "", 1), null));
        nodes.add(new ValidatingItem(new AocLoiCalculTransitoire("Cc_T2", "LoiTZ_1", "St_1", 1), null));
        nodes.add(new ValidatingItem(new AocLoiCalculTransitoire("Cc_T2", "LoiTQ_1", "St_1", 1), null));
        AocLoiCalculTransitoireValidator validator = new AocLoiCalculTransitoireValidator();
        final CtuluLog ctuluLog = validator.validate(nodes, availableCalculs, availableLois, availableSections);
        assertTrue(ctuluLog.isNotEmpty());
        assertEquals(7, ctuluLog.getRecords().size());
        assertEquals("Entrée 1: la loi LoiZ_1 n'est pas définie dans le fichier LHPT", ctuluLog.getRecords().get(0).getLocalizedMessage());
        assertEquals("Entrée 1: la loi LoiZ_1 n'est pas du bon type soit LoiTQ ou LoiTZ", ctuluLog.getRecords().get(1).getLocalizedMessage());
        assertEquals("Entrée 1: la section St_10 n'est pas définie (ou inactive) dans le scénario", ctuluLog.getRecords().get(2).getLocalizedMessage());
        assertEquals("Entrée 1: la valeur de la pondération doit être supérieure ou égale à 1", ctuluLog.getRecords().get(3).getLocalizedMessage());
        assertEquals("Entrée 2: le nom de la section est vide", ctuluLog.getRecords().get(4).getLocalizedMessage());
        assertEquals("Entrée 4: la loi LoiTQ_1 est déjà associée", ctuluLog.getRecords().get(5).getLocalizedMessage());
        assertEquals("Entrée 4: la section St_1 est utilisée plusieurs fois pour le calcul Cc_T2", ctuluLog.getRecords().get(6).getLocalizedMessage());
    }
}
