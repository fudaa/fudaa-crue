package org.fudaa.dodico.crue.aoc.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.metier.aoc.ParametrageProfilSection;
import org.fudaa.dodico.crue.validation.ValidatingItem;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AocProfilSectionNodesValidatorTest {
  @Test
  public void validate() {

    String[] availableSections = new String[]{"St_1", "St_2"};

    ParametrageProfilSection section1 = new ParametrageProfilSection();
    ValidatingItem<ParametrageProfilSection> node1 = new ValidatingItem(section1, null);
    List<ValidatingItem<ParametrageProfilSection>> nodes = new ArrayList<>();
    nodes.add(node1);
    AocProfilSectionValidator validator = new AocProfilSectionValidator();

    //test tout ok
    section1.setSectionRef("St_1");
    section1.setPk("PK_1");
    CtuluLog ctuluLog = validator.validate(nodes, availableSections);
    assertTrue(ctuluLog.isEmpty());

    //un echelle section vide
    ParametrageProfilSection section2 = new ParametrageProfilSection();
    ValidatingItem<ParametrageProfilSection> node2 = new ValidatingItem(section2, null);
    nodes.add(node2);

    //pk etsection ref vide
    ctuluLog = validator.validate(nodes, availableSections);
    assertFalse(ctuluLog.isEmpty());
    assertEquals(2, ctuluLog.getRecords().size());
    assertEquals("Entrée 2: le nom de l'échelle est vide", ctuluLog.getRecords().get(0).getLocalizedMessage());
    assertEquals("Entrée 2: le nom de la section est vide", ctuluLog.getRecords().get(1).getLocalizedMessage());

    //nom de section non correct
    section2.setPk("PK_2");
    section2.setSectionRef("PK_2");
    ctuluLog = validator.validate(nodes, availableSections);
    assertFalse(ctuluLog.isEmpty());
    assertEquals(1, ctuluLog.getRecords().size());
    assertEquals("Entrée 2: le nom de la section PK_2 est invalide et ne commence pas par St_", ctuluLog.getRecords().get(0).getLocalizedMessage());

    //nom de section et profil dupliqué
    section2.setPk(section1.getPk());
    section2.setSectionRef(section1.getSectionRef());
    ctuluLog = validator.validate(nodes, availableSections);
    assertEquals(2, ctuluLog.getRecords().size());
    assertEquals("Entrée 2: l'échelle PK_1 est déjà utilisée", ctuluLog.getRecords().get(0).getLocalizedMessage());
    assertEquals("Entrée 2: la section St_1 est déjà utilisée", ctuluLog.getRecords().get(1).getLocalizedMessage());

    //Section inconnue
    section2.setPk("PK_2");
    section2.setSectionRef("St_33");
    ctuluLog = validator.validate(nodes, availableSections);
    assertEquals(1, ctuluLog.getRecords().size());
    assertEquals("Entrée 2: la section St_33 n'est pas définie ou n'est pas active dans le scénario", ctuluLog.getRecords().get(0).getLocalizedMessage());

    //TOUT bon
    section2.setPk("PK_2");
    section2.setSectionRef("St_2");
    ctuluLog = validator.validate(nodes, availableSections);
    assertTrue(ctuluLog.isEmpty());
  }
}
