package org.fudaa.dodico.crue.aoc.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.projet.AocValidationGroupe;
import org.fudaa.dodico.crue.validation.ValidatingItem;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 */
public class AocValidationCroiseeValidatorTest {
    @Test
    public void testIsValide() {
        AocValidationCroiseeValidator validator = new AocValidationCroiseeValidator();
        List<ValidatingItem<AocValidationGroupe>> nodes = new ArrayList<>();

        String[] availableLoi = new String[]{"Loi_1", "Loi_11", "Loi_2"};
        AocValidationGroupe gr1 = new AocValidationGroupe();
        gr1.setNom("GR1");
        gr1.setLoisAsString("Loi_1;Loi_11");
        gr1.setEffectifApprentissage(2);
        AocValidationGroupe gr2 = new AocValidationGroupe();
        gr2.setNom("GR2");
        gr2.setLoisAsString("Loi_2");
        gr2.setEffectifApprentissage(1);

        nodes.add(new ValidatingItem(gr1, null));
        nodes.add(new ValidatingItem(gr2, null));

        final CtuluLog ctuluLog = validator.validate(nodes, availableLoi);
        assertTrue(ctuluLog.isEmpty());
    }

    @Test
    public void testIsNotValid() {
        AocValidationCroiseeValidator validator = new AocValidationCroiseeValidator();
        List<ValidatingItem<AocValidationGroupe>> nodes = new ArrayList<>();

        String[] availableLoi = new String[]{"Loi_1", "Loi_11", "Loi_2"};
        AocValidationGroupe grEmpty = new AocValidationGroupe();
        grEmpty.setNom("");
        grEmpty.setEffectifApprentissage(1);
        nodes.add(new ValidatingItem(grEmpty, null));

        CtuluLog ctuluLog = validator.validate(nodes, availableLoi);
        assertEquals(2, ctuluLog.getRecords().size());
        assertEquals("Entrée 1: le nom du groupe est vide", ctuluLog.getRecords().get(0).getLocalizedMessage());
        assertEquals("Entrée 1: le groupe  ne contient pas de lois", ctuluLog.getRecords().get(1).getLocalizedMessage());

        AocValidationGroupe gr1EffectifZero = new AocValidationGroupe();
        gr1EffectifZero.setNom("GR1");
        gr1EffectifZero.setLoisAsString("Loi_1");
        gr1EffectifZero.setEffectifApprentissage(2);
        nodes.add(new ValidatingItem(gr1EffectifZero, null));
        ctuluLog = validator.validate(nodes, availableLoi);
        assertEquals(3, ctuluLog.getRecords().size());
        assertEquals(
                "Entrée 2: la valeur effectif d'apprentissage du groupe GR1 est invalide. Elle doit être comprise entre 1 et le nombre de loi(s) sélectionnée(s) pour le groupe GR1",
                ctuluLog.getRecords().get(2).getLocalizedMessage());

        AocValidationGroupe gr2 = new AocValidationGroupe();
        gr2.setNom("GR2");
        gr2.setLoisAsString("Loi_11;;Loi_2;Loi_2;LoiNotPresent;Loi_1");
        gr2.setEffectifApprentissage(1);
        nodes.add(new ValidatingItem(gr2, null));
        ctuluLog = validator.validate(nodes, availableLoi);
        assertEquals(6, ctuluLog.getRecords().size());
        assertEquals(
                "Entrée 3: le groupe GR2 utilise la loi LoiNotPresent qui n'est pas connue ou pas définie dans la liste des correspondances Lois ⇔ Calculs Permanents",
                ctuluLog.getRecords().get(3).getLocalizedMessage());
        assertEquals(
                "Entrée 3: le groupe GR2 utilise la loi Loi_1 qui appartient déjà au groupe GR1",
                ctuluLog.getRecords().get(4).getLocalizedMessage());
        assertEquals(
                "Entrée 3: le groupe GR2 utilise la loi Loi_2 qui appartient déjà au groupe GR2",
                ctuluLog.getRecords().get(5).getLocalizedMessage());

    }
}
