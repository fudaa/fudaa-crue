package org.fudaa.dodico.crue.aoc.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.projet.AocLoiStrickler;
import org.fudaa.dodico.crue.validation.ValidatingItem;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AocLoiStrickerValidatorTest {
    @Test
    public void testValidate() {
        String[] availableLois = new String[]{"Fk_1", "Fk_2", "Wr_ong"};
        List<ValidatingItem<AocLoiStrickler>> nodes = new ArrayList<>();
        nodes.add(new ValidatingItem(new AocLoiStrickler("", 0, 0, 0), null));
        nodes.add(new ValidatingItem(new AocLoiStrickler("Wr_ong", 10, 0, 5), null));
        nodes.add(new ValidatingItem(new AocLoiStrickler("Fk_1", 0, 0, 0), null));
        nodes.add(new ValidatingItem(new AocLoiStrickler("Fk_2", 0, 1, 2), null));
        nodes.add(new ValidatingItem(new AocLoiStrickler("Fk_1", 0, 1, 2), null));
        AocLoiStricklerValidator validator = new AocLoiStricklerValidator();
        final CtuluLog ctuluLog = validator.validate(nodes, availableLois);
        assertTrue(ctuluLog.isNotEmpty());
        assertEquals(5, ctuluLog.getRecords().size());
        assertEquals("Entrée 1: le nom du frottement est vide", ctuluLog.getRecords().get(0).getLocalizedMessage());
        assertEquals("Entrée 2: le type du frottement Wr_ong n'est pas supporté", ctuluLog.getRecords().get(1).getLocalizedMessage());
        assertEquals("Entrée 2: la valeur Max doit être supérieure à la valeur Min", ctuluLog.getRecords().get(2).getLocalizedMessage());
        assertEquals("Entrée 2: la valeur Ini doit être comprise dans [Min;Max]", ctuluLog.getRecords().get(3).getLocalizedMessage());
        assertEquals("Entrée 5: le frottement Fk_1 est déjà utilisé", ctuluLog.getRecords().get(4).getLocalizedMessage());
    }
}
