package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.dodico.crue.aoc.projet.AocLoisStrickler;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class AOCStricklerBuilderTest {

  @Test
  public void testGetMaxAmplitude() {
    AocLoisStrickler loisStrickler = new AocLoisStrickler();
    //delta 2
    loisStrickler.addStrickler("loi1", 1, 2, 3);
    //delta 1
    loisStrickler.addStrickler("loi2", 5, 5, 6);
    //delta 2
    loisStrickler.addStrickler("loi3", 7, 8, 9);
    assertEquals(2d,AOCStricklerBuilder.getMaxAmplitude(loisStrickler,1),0.00001);
    assertEquals(1d,AOCStricklerBuilder.getMaxAmplitude(loisStrickler,0.5),0.00001);
    //delta 10
    loisStrickler.addStrickler("loi3", 0, 8, 10);
    assertEquals(5d,AOCStricklerBuilder.getMaxAmplitude(loisStrickler,0.5),0.00001);
  }

  @Test
  public void testBuildInitValue() {
    AocLoisStrickler loisStrickler = new AocLoisStrickler();
    loisStrickler.addStrickler("loi1", 1, 2, 3);
    loisStrickler.addStrickler("loi2", 4, 5, 6);
    loisStrickler.addStrickler("loi3", 7, 8, 9);
    final Map<String, Double> initValueByLoi = AOCStricklerBuilder.buildInitValue(loisStrickler);
    assertEquals(3, initValueByLoi.size());
    assertEquals(2, (int) initValueByLoi.get("loi1").doubleValue());
    assertEquals(5, (int) initValueByLoi.get("loi2").doubleValue());
    assertEquals(8, (int) initValueByLoi.get("loi3").doubleValue());
  }
}
