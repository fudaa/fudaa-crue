package org.fudaa.dodico.crue.aoc.compute;

import org.junit.Test;

import static org.junit.Assert.*;

public class AreaComputeTest {
  @Test
  public void testSquare() {
    AreaCompute compute = new AreaCompute();
    final double hauteur = 11;
    final double  largeur = 4;
    compute.add(0, hauteur);
    compute.add(largeur, hauteur);
    assertEquals(hauteur*largeur, compute.getArea(), 1e-10);
    assertEquals(2, compute.getNbValues());
  }


  @Test
  public void testTriangle() {
    AreaCompute compute = new AreaCompute();
    final double base = 20d;
    final double hauteur = 10;
    compute.add(0, 0);
    compute.add(8, hauteur);
    compute.add(base, 0);
    assertEquals(base*hauteur/2, compute.getArea(), 1e-10);
    assertEquals(3, compute.getNbValues());
  }
}
