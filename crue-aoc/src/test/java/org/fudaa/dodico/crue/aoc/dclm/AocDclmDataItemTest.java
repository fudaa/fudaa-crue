package org.fudaa.dodico.crue.aoc.dclm;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import static org.junit.Assert.*;

public class AocDclmDataItemTest {
    @Test
    public void getByEMHSorted() {
        List<AocDclmDataItem> list = new ArrayList<>();
        final String calulRef1 = "calulRef1";
        final String calulRef2 = "calulRef2";
        final String emh1 = "emh1";
        final String emh2 = "emh2";
        list.add(new AocDclmDataItem(calulRef2, emh2, 22, 2, false));
        list.add(new AocDclmDataItem(calulRef1, emh2, 12, 10, false));
        list.add(new AocDclmDataItem(calulRef2, emh1, 21, 2, false));
        list.add(new AocDclmDataItem(calulRef1, emh1, 11, 10, false));
        final List<AocDclmDataItemByEMH> byEMHSorted = AocDclmDataItem.getByEMHSorted(list);
        assertEquals(2, byEMHSorted.size());
        assertEquals(emh1, byEMHSorted.get(0).getNom());
        assertEquals(emh2, byEMHSorted.get(1).getNom());

        assertEquals(11, (int) byEMHSorted.get(0).getValue(calulRef1).doubleValue());
        assertEquals(21, (int) byEMHSorted.get(0).getValue(calulRef2).doubleValue());

        assertEquals(12, (int) byEMHSorted.get(1).getValue(calulRef1).doubleValue());
        assertEquals(22, (int) byEMHSorted.get(1).getValue(calulRef2).doubleValue());
    }

    @Test
    public void getByKeys() {
        List<AocDclmDataItem> list = new ArrayList<>();
        final String calulRef1 = "calulRef1";
        final String calulRef2 = "calulRef2";
        final String emh1 = "emh1";
        final String emh2 = "emh2";
        list.add(new AocDclmDataItem(calulRef2, emh2, 22, 2, false));
        list.add(new AocDclmDataItem(calulRef1, emh2, 12, 10, false));
        list.add(new AocDclmDataItem(calulRef2, emh1, 21, 2, false));
        list.add(new AocDclmDataItem(calulRef1, emh1, 11, 10, false));
        final TreeMap<AocDclmKey, Double> byKeys = AocDclmDataItem.getByKeys(list);
        assertEquals(4, byKeys.size());

        List<AocDclmDataItem> keysInOrder = new ArrayList(byKeys.keySet());
        assertEquals(new AocDclmKey(calulRef1, emh1), keysInOrder.get(0));
        assertEquals(new AocDclmKey(calulRef1, emh2), keysInOrder.get(1));
        assertEquals(new AocDclmKey(calulRef2, emh1), keysInOrder.get(2));
        assertEquals(new AocDclmKey(calulRef2, emh2), keysInOrder.get(3));

        assertEquals(11, (int) byKeys.get(new AocDclmKey(calulRef1, emh1)).doubleValue());
        assertEquals(21, (int) byKeys.get(new AocDclmKey(calulRef2, emh1)).doubleValue());

        assertEquals(12, (int) byKeys.get(new AocDclmKey(calulRef1, emh2)).doubleValue());
        assertEquals(22, (int) byKeys.get(new AocDclmKey(calulRef2, emh2)).doubleValue());
    }

    @Test
    public void generateRandomValuesCentered() {
        List<AocDclmDataItem> current = new ArrayList<>();
        AocDclmDataItem item = new AocDclmDataItem("calulRef1", "emh1", 10, 10, false);
        AocDclmDataItem item2 = new AocDclmDataItem("calulRef2", "emh1", 5, 2, false);
        current.add(item);
        current.add(item2);
        final double amplitude = 0.5;
        final List<AocDclmDataItem> randomValuesCentered = AocDclmDataItem.generateRandomValuesCentered(current, amplitude);
        assertFalse(randomValuesCentered == current);
        assertEquals(2, randomValuesCentered.size());
        assertEquals(item.getInitDeltaQ(), randomValuesCentered.get(0).getInitDeltaQ(), 0.00001);
        assertEquals(item.getCalculRef(), randomValuesCentered.get(0).getCalculRef());
        assertEquals(item.getEmhRef(), randomValuesCentered.get(0).getEmhRef());
        assertEquals(item2.getInitDeltaQ(), randomValuesCentered.get(1).getInitDeltaQ(), 0.00001);
        assertEquals(item2.getCalculRef(), randomValuesCentered.get(1).getCalculRef());
        assertEquals(item2.getEmhRef(), randomValuesCentered.get(1).getEmhRef());

        assertTrue(randomValuesCentered.get(0).getValue() >= item.getValue() * (1 - item.getInitDeltaQ() * amplitude));
        assertTrue(randomValuesCentered.get(0).getValue() < item.getValue() * (1 + item.getInitDeltaQ() * amplitude));

        assertTrue(randomValuesCentered.get(1).getValue() >= item2.getValue() * (1 - item2.getInitDeltaQ() * amplitude));
        assertTrue(randomValuesCentered.get(1).getValue() < item2.getValue() * (1 + item2.getInitDeltaQ() * amplitude));
    }
}
