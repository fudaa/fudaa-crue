package org.fudaa.dodico.crue.aoc.exec;

import org.apache.commons.io.FileUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.ConnexionInformationDefault;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.coeur.CoeurManagerForTest;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.CrueFileFormatBuilderDFRT;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageAlgorithme;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageType;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionFF;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openide.util.Exceptions;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class AOCStricklerModifierTest {
  static File target = null;

  @BeforeClass
  public static void prepare() {
    try {
      target = CtuluLibFile.createTempDir();
      assertTrue(target.exists());
    } catch (final IOException ex) {
      Exceptions.printStackTrace(ex);
    }
  }

  @AfterClass
  public static void cleanUp() {
    try {
      FileUtils.deleteDirectory(target);
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testModifier() throws IOException {

    //on teste la modification sur le fichier init
    final File dfrtDir = CtuluLibFile.createTempDir();
    File dfrt = new File(dfrtDir, "example.dfrt.xml");
    CtuluLibFile.getFileFromJar("/M3-0_c10.dfrt.xml", dfrt);
    final Crue10FileFormat<List<DonFrt>> fileFormat = new CrueFileFormatBuilderDFRT().getFileFormat(TestCoeurConfig.INSTANCE);
    final CrueConfigMetier ccm = TestCoeurConfig.INSTANCE.getCrueConfigMetier();
    CrueIOResu<List<DonFrt>> read = fileFormat.read(dfrt, new CtuluLog(), new CrueDataImpl(ccm));
    String frt10MajName = "Fk_PROF10MAJ";
    String frt10MinName = "Fk_PROF10MIN";
    String frt11majName = "Fk_Prof11MAJ";
    Map<String, DonFrt> stringDonFrtMap = TransformerHelper.toMapOfNom(read.getMetier());
    DonFrt frt10Maj = stringDonFrtMap.get(frt10MajName);
    assertEquals(0, (int) frt10Maj.getLoi().getAbscisse(0));
    assertEquals(15, (int) frt10Maj.getLoi().getOrdonnee(0));
    assertEquals(10, (int) frt10Maj.getLoi().getAbscisse(1));
    assertEquals(15, (int) frt10Maj.getLoi().getOrdonnee(1));
    DonFrt frt10Min = stringDonFrtMap.get(frt10MinName);
    assertEquals(0, (int) frt10Min.getLoi().getAbscisse(0));
    assertEquals(30, (int) frt10Min.getLoi().getOrdonnee(0));
    assertEquals(10, (int) frt10Min.getLoi().getAbscisse(1));
    assertEquals(30, (int) frt10Min.getLoi().getOrdonnee(1));
    DonFrt frt11Maj = stringDonFrtMap.get(frt11majName);
    assertEquals(0, (int) frt11Maj.getLoi().getAbscisse(0));
    assertEquals(15, (int) frt11Maj.getLoi().getOrdonnee(0));
    assertEquals(10, (int) frt11Maj.getLoi().getAbscisse(1));
    assertEquals(15, (int) frt11Maj.getLoi().getOrdonnee(1));

    AOCStricklerModifier modifier = new AOCStricklerModifier();
    Map<String, Double> newValues = new HashMap<>();
    newValues.put(frt10MajName, 2d);
    newValues.put(frt10MinName, -1d);//les valeurs seront l

    //on applique les modifications
    modifier.applyFrottements(ccm, newValues, fileFormat, dfrt);

    //on relit le fichier
    read = fileFormat.read(dfrt, new CtuluLog(), new CrueDataImpl(ccm));

    stringDonFrtMap = TransformerHelper.toMapOfNom(read.getMetier());
    frt10Maj = stringDonFrtMap.get(frt10MajName);
    assertEquals(0, (int) frt10Maj.getLoi().getAbscisse(0));
    assertEquals(2, (int) frt10Maj.getLoi().getOrdonnee(0));
    assertEquals(10, (int) frt10Maj.getLoi().getAbscisse(1));
    assertEquals(2, (int) frt10Maj.getLoi().getOrdonnee(1));
    frt10Min = stringDonFrtMap.get(frt10MinName);
    assertEquals(0, (int) frt10Min.getLoi().getAbscisse(0));
    assertEquals(1, (int) frt10Min.getLoi().getOrdonnee(0));
    assertEquals(10, (int) frt10Min.getLoi().getAbscisse(1));
    assertEquals(1, (int) frt10Min.getLoi().getOrdonnee(1));
    frt11Maj = stringDonFrtMap.get(frt11majName);
    assertEquals(0, (int) frt11Maj.getLoi().getAbscisse(0));
    assertEquals(15, (int) frt11Maj.getLoi().getOrdonnee(0));
    assertEquals(10, (int) frt11Maj.getLoi().getAbscisse(1));
    assertEquals(15, (int) frt11Maj.getLoi().getOrdonnee(1));
  }

  @Test
  public void testModifierIntegration() throws IOException {

    //on teste la modification sur le fichier init
    final File aocFile = AocExecTestHelper.createAocProject(target, EnumAocCalageType.SEUL, EnumAocCalageAlgorithme.MONTE_CARLO);
    AocExecContainerLoader builder = new AocExecContainerLoader(CoeurManagerForTest.createDefault());
    //test du chargement
    CrueOperationResult<AocExecContainer> result = builder.load(aocFile);
    final AocExecContainer aocExecContainer = result.getResult();
    result = builder.build(result, new ConnexionInformationDefault());
    final Map<String, Double> newDfrtValues = AOCStricklerBuilder.buildInitValue(aocExecContainer.getCampagne().getDonnees().getLoisStrickler());

    File dfrt = aocExecContainer.getProjet().getInfos().getFileFromBase("DM2013_cconc_zone_MC.dfrt.xml").getProjectFile(aocExecContainer.getProjet());
    long lastModificationBeforeModification = dfrt.lastModified();
    assertTrue(dfrt.exists());

    AOCStricklerModifier modifier = new AOCStricklerModifier();
    //on applique les modifications sur la reference
    modifier.applyFrottement(aocExecContainer.getProjet(), aocExecContainer.getManagerScenarioCible(), newDfrtValues);
    assertTrue(dfrt.lastModified() > lastModificationBeforeModification);

    final CrueConfigMetier ccm = TestCoeurConfig.INSTANCE.getCrueConfigMetier();
    final Crue10FileFormat<List<DonFrt>> fileFormat = new CrueFileFormatBuilderDFRT().getFileFormat(TestCoeurConfig.INSTANCE);
    CrueIOResu<List<DonFrt>> read = fileFormat.read(dfrt, new CtuluLog(), new CrueDataImpl(ccm));
    Map<String, DonFrt> stringDonFrtMap = TransformerHelper.toMapOfNom(read.getMetier());
    for (PtEvolutionFF ptEvolutionFF : stringDonFrtMap.get("Fk_P153.800MAJ").getLoi().getEvolutionFF().getPtEvolutionFF()) {
      assertEquals(10, (int) ptEvolutionFF.getOrdonnee());
    }
    for (PtEvolutionFF ptEvolutionFF : stringDonFrtMap.get("Fk_P153.800MIN").getLoi().getEvolutionFF().getPtEvolutionFF()) {
      assertEquals(28, (int) ptEvolutionFF.getOrdonnee());
    }
    for (PtEvolutionFF ptEvolutionFF : stringDonFrtMap.get("Fk_P153.900MAJ").getLoi().getEvolutionFF().getPtEvolutionFF()) {
      assertEquals(10, (int) ptEvolutionFF.getOrdonnee());
    }
  }
}
