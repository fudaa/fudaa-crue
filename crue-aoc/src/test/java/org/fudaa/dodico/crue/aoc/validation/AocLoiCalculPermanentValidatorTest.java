package org.fudaa.dodico.crue.aoc.validation;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculPermanent;
import org.fudaa.dodico.crue.validation.ValidatingItem;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AocLoiCalculPermanentValidatorTest {
    @Test
    public void testValidate() {
        String[] availableLois = new String[]{"LoiSectionsZ_1", "LoiTQ_1"};
        String[] availableCalculs = new String[]{"Cc_P1"};
        List<ValidatingItem<AocLoiCalculPermanent>> nodes = new ArrayList<>();
        nodes.add(new ValidatingItem(new AocLoiCalculPermanent("Cc_T1", "LoiTQ_1", 0), null));
        nodes.add(new ValidatingItem(new AocLoiCalculPermanent(null, "Loi_Inexistante", 1), null));
        nodes.add(new ValidatingItem(new AocLoiCalculPermanent("Cc_P1", "LoiSectionsZ_1", 1), null));
        nodes.add(new ValidatingItem(new AocLoiCalculPermanent("Cc_P1", "LoiSectionsZ_1", 1), null));
        AocLoiCalculPermanentValidator validator = new AocLoiCalculPermanentValidator();
        final CtuluLog ctuluLog = validator.validate(nodes, availableCalculs, availableLois);
        assertTrue(ctuluLog.isNotEmpty());
        assertEquals(7, ctuluLog.getRecords().size());
        assertEquals("Entrée 1: le calcul permanent Cc_T1 n'est pas défini ou pas actif dans le scénario", ctuluLog.getRecords().get(0).getLocalizedMessage());
        assertEquals("Entrée 1: la loi LoiTQ_1 n'est pas du bon type soit SectionZ", ctuluLog.getRecords().get(1).getLocalizedMessage());
        assertEquals("Entrée 1: la valeur de la pondération doit être supérieure ou égale à 1", ctuluLog.getRecords().get(2).getLocalizedMessage());
        assertEquals("Entrée 2: le nom du calcul permanent est vide", ctuluLog.getRecords().get(3).getLocalizedMessage());
        assertEquals("Entrée 2: la loi Loi_Inexistante n'est pas définie dans le fichier LHPT", ctuluLog.getRecords().get(4).getLocalizedMessage());
        assertEquals("Entrée 2: la loi Loi_Inexistante n'est pas du bon type soit SectionZ", ctuluLog.getRecords().get(5).getLocalizedMessage());
        assertEquals("Entrée 4: la loi LoiSectionsZ_1 est déjà associée", ctuluLog.getRecords().get(6).getLocalizedMessage());
    }
}
