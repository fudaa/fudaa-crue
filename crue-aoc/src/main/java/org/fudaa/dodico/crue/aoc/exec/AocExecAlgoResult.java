package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.dodico.crue.aoc.dclm.AocDclmDataItem;

import java.util.List;
import java.util.Map;

/**
 * Les resultats de l'algo.
 * @author deniger
 */
public class AocExecAlgoResult {
  private final Map<String, Double> dfrtValues;
  private final List<AocDclmDataItem> dclmValues;

  private int iteration;

  public AocExecAlgoResult(Map<String, Double> dfrtValues, List<AocDclmDataItem> dclmdValues) {
    this.dfrtValues = dfrtValues;
    this.dclmValues = dclmdValues;
  }

  public int getIteration() {
    return iteration;
  }

  public void setIteration(int iteration) {
    this.iteration = iteration;
  }

  public Map<String, Double> getDfrtValues() {
    return dfrtValues;
  }

  public List<AocDclmDataItem> getDclmValues() {
    return dclmValues;
  }
}
