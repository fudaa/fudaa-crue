package org.fudaa.dodico.crue.aoc.exec.writer;

import org.fudaa.dodico.crue.aoc.exec.AocExecContainer;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.emh.Loi;

import java.text.NumberFormat;
import java.util.Map;

/**
 * @author deniger
 */
public class AocExecFormatterHelper {
  /**
   *
   * @param formatter le formatteur
   * @param dfrtName le nom du frottement
   * @param troncature la valeur a la troncature
   * @param dfrtOptimum la valeur à l'optimum
   * @param dfrtValuesInit la valeur intiale definie dans la compagne
   * @return string : Nom loi;Valeur initial;Valeur Optimum;Valeur Troncature
   */
  public static String format(final NumberFormat formatter, final String dfrtName, final Double troncature, final Map<String, Double> dfrtOptimum, final Map<String, Double> dfrtValuesInit) {
    final Double init = dfrtValuesInit.get(dfrtName);
    final Double optimum = dfrtOptimum.get(dfrtName);
    return dfrtName
        + AocExecWriterHelper.DELIMITER + formatValue(formatter, init)
        + AocExecWriterHelper.DELIMITER + formatValue(formatter, optimum)
        + AocExecWriterHelper.DELIMITER + formatValue(formatter, troncature);
  }

  public static String formatValue(final NumberFormat formatter, final Double value) {
    if (value == null) {
      return "";
    }
    return formatDoubleValue(formatter, value.doubleValue());
  }

  public static NumberFormat getAbscisseFormater(final AocExecContainer result, final Loi loi) {
    return getVariableAbscisse(result, loi).getFormatter(DecimalFormatEpsilonEnum.COMPARISON);
  }

  public static ItemVariable getVariableAbscisse(final AocExecContainer result, final Loi loi) {
    return result.getProjet().getPropDefinition().getLoiAbscisse(loi.getType());
  }

  public static NumberFormat getOrdonneeFormater(final AocExecContainer result, final Loi loi) {
    return getVariableOrdonnee(result, loi).getFormatter(DecimalFormatEpsilonEnum.COMPARISON);
  }

  public static ItemVariable getVariableOrdonnee(final AocExecContainer result, final Loi loi) {
    return result.getProjet().getPropDefinition().getLoiOrdonnee(loi.getType());
  }

  public static String formatDoubleValue(final NumberFormat formatter, final double value) {
    if (formatter == null) {
      return Double.toString(value);
    }
    return formatter.format(value);
  }

  /**
   * @param result pour recuperer le ccm
   * @return le format pour les ordonnées des lois ZK
   */
  public static NumberFormat getDfrtFormater(final AocExecContainer result) {
    NumberFormat formatter = null;
    final ItemVariable loiOrdonnee = result.getProjet().getPropDefinition().getLoiOrdonnee(EnumTypeLoi.LoiZFK);
    if (loiOrdonnee != null) {
      formatter = loiOrdonnee.getFormatter(DecimalFormatEpsilonEnum.COMPARISON);
    }
    return formatter;
  }

  /**
   * @param result pour recuperer le ccm
   * @return le format pour les ordonnées des lois Z
   */
  public static NumberFormat getZFormater(final AocExecContainer result) {
    NumberFormat formatter = null;
    final ItemVariable itemVariable = result.getProjet().getPropDefinition().getProperty(CrueConfigMetierConstants.PROP_Z);
    if (itemVariable != null) {
      formatter = itemVariable.getFormatter(DecimalFormatEpsilonEnum.COMPARISON);
    }
    return formatter;
  }


  /**
   * @param result pour recuperer le ccm
   * @return le format pour les debit
   */
  public static NumberFormat getQFormater(final AocExecContainer result) {
    NumberFormat formatter = null;
    final ItemVariable loiOrdonnee = result.getProjet().getPropDefinition().getProperty(CrueConfigMetierConstants.PROP_Q);
    if (loiOrdonnee != null) {
      formatter = loiOrdonnee.getFormatter(DecimalFormatEpsilonEnum.COMPARISON);
    }
    return formatter;
  }

  public static String getCritereName(final AocExecContainer aocExecContainer) {
    if (aocExecContainer.getCampagne().isPermanent()) {
      return "RMSE";
    }
    return aocExecContainer.getCampagne().getTypeCalageCritere().getIdentifiant();
  }
}
