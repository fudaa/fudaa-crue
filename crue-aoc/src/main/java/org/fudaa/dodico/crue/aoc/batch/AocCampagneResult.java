package org.fudaa.dodico.crue.aoc.batch;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.joda.time.LocalDateTime;

/**
 * @author CANEL Christophe
 */
public class AocCampagneResult {
  private String commentaire;
  private LocalDateTime date;
  private CtuluLogGroup globalValidation;

  /**
   * @return the commentaire
   */
  public String getCommentaire() {
    return commentaire;
  }

  /**
   * @param commentaire the commentaire to set
   */
  public void setCommentaire(final String commentaire) {
    this.commentaire = commentaire;
  }


  /**
   * @return the date
   */
  public LocalDateTime getDate() {
    return date;
  }

  /**
   * @param date the date to set
   */
  public void setDate(final LocalDateTime date) {
    this.date = date;
  }

  /**
   * @return the globalValidation
   */
  public CtuluLogGroup getGlobalValidation() {
    if (globalValidation == null) {
      globalValidation = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    }
    return globalValidation;
  }

  /**
   * @param globalValidation the globalValidation to set
   */
  public void setGlobalValidation(final CtuluLogGroup globalValidation) {
    this.globalValidation = globalValidation;
  }
}
