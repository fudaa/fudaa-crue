package org.fudaa.dodico.crue.aoc.io;

import com.thoughtworks.xstream.XStream;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;
import org.fudaa.dodico.crue.io.common.EnumsConverter;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageAlgorithme;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageType;
import org.fudaa.dodico.crue.metier.aoc.EnumAocTypeCalageCritere;
import org.fudaa.dodico.crue.metier.aoc.EnumAocTypeCalageDonnees;

import java.util.ArrayList;
import java.util.List;

/**
 * @author deniger
 */
public class CrueDaoStructureAOC implements CrueDataDaoStructure {
  /**
   * @return map de correspondance pour les enums
   */
  public static DualHashBidiMap createTypeCalageDonneesMap() {
    final DualHashBidiMap res = new DualHashBidiMap();
    res.put(EnumAocTypeCalageDonnees.PERMANENT, "Permanent");
    res.put(EnumAocTypeCalageDonnees.TRANSITOIRE_HYDROGRAMME, "TransitoireHydrogramme");
    res.put(EnumAocTypeCalageDonnees.TRANSITOIRE_LIMNIGRAMME, "TransitoireLimnigramme");
    return res;
  }

  /**
   * @return map de correspondance pour les enums
   */
  public static DualHashBidiMap createTypeCalageCriteresMap() {
    final DualHashBidiMap res = new DualHashBidiMap();
    res.put(EnumAocTypeCalageCritere.ERREUR_QUADRATIQUE, EnumAocTypeCalageCritere.ERREUR_QUADRATIQUE.getIdentifiant());
    res.put(EnumAocTypeCalageCritere.ECART_NIVEAUX_MAX, EnumAocTypeCalageCritere.ECART_NIVEAUX_MAX.getIdentifiant());
    res.put(EnumAocTypeCalageCritere.ECART_TEMPS_ARRIVEE_MAX, EnumAocTypeCalageCritere.ECART_TEMPS_ARRIVEE_MAX.getIdentifiant());
    res.put(EnumAocTypeCalageCritere.ECART_VOLUMES, EnumAocTypeCalageCritere.ECART_VOLUMES.getIdentifiant());
    return res;
  }

  /**
   * @return map de correspondance pour les enums
   */
  public static DualHashBidiMap createCalageTypeMap() {
    final DualHashBidiMap res = new DualHashBidiMap();
    res.put(EnumAocCalageType.SEUL, EnumAocCalageType.SEUL.getIdentifiant());
    res.put(EnumAocCalageType.TEST_REPETABILITE, EnumAocCalageType.TEST_REPETABILITE.getIdentifiant());
    res.put(EnumAocCalageType.ANALYSE_SENSIBILITE, EnumAocCalageType.ANALYSE_SENSIBILITE.getIdentifiant());
    res.put(EnumAocCalageType.VALIDATION_CROISEE, EnumAocCalageType.VALIDATION_CROISEE.getIdentifiant());
    return res;
  }

  /**
   * @return map de correspondance pour les enums
   */
  public static DualHashBidiMap createCalageAlgorithmeMap() {
    final DualHashBidiMap res = new DualHashBidiMap();
    res.put(EnumAocCalageAlgorithme.RECUIT, EnumAocCalageAlgorithme.RECUIT.getIdentifiant());
    res.put(EnumAocCalageAlgorithme.MONTE_CARLO, EnumAocCalageAlgorithme.MONTE_CARLO.getIdentifiant());
    return res;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog,
                               final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    //on configure les alias
    xstream.alias("AOC", CrueDaoAOC.class);
    xstream.alias("EtudeAssociee", EtudeAssociee.class);
    xstream.alias("TypeCalage", TypeCalage.class);
    xstream.alias("Calage", Calage.class);
    xstream.alias("Seul", CalageSeul.class);
    xstream.alias("CalageTestRepetabilite", CalageTestRepetabilite.class);
    xstream.alias("CalageValidationCroisee", CalageValidationCroisee.class);
    xstream.alias("DonneesCampagne", DonneesCampagne.class);
    xstream.alias("ParametresNumeriques", ParametresNumeriques.class);

    xstream.alias("LoisCalculsPermanents", LoisCalculsPermanents.class);
    xstream.addImplicitCollection(LoisCalculsPermanents.class, "calculs");

    xstream.alias("LoiCalculPermanent", LoiCalculPermanent.class);
    xstream.useAttributeFor(LoiCalculPermanent.class, "CalculRef");
    xstream.useAttributeFor(LoiCalculPermanent.class, "LoiRef");
    xstream.useAttributeFor(LoiCalculPermanent.class, "Ponderation");

    xstream.alias("LoisCalculsTransitoires", LoisCalculsTransitoires.class);
    xstream.addImplicitCollection(LoisCalculsTransitoires.class, "calculs");

    xstream.alias("LoiCalculTransitoire", LoiCalculTransitoire.class);
    xstream.useAttributeFor(LoiCalculTransitoire.class, "CalculRef");
    xstream.useAttributeFor(LoiCalculTransitoire.class, "LoiRef");
    xstream.useAttributeFor(LoiCalculTransitoire.class, "SectionRef");
    xstream.useAttributeFor(LoiCalculTransitoire.class, "Ponderation");

    xstream.alias("LoisStrickler", LoisStrickler.class);
    xstream.addImplicitCollection(LoisStrickler.class, "stricklers");

    xstream.alias("LoiStrickler", LoiStrickler.class);
    xstream.useAttributeFor(LoiStrickler.class, "LoiRef");
    xstream.useAttributeFor(LoiStrickler.class, "Min");
    xstream.useAttributeFor(LoiStrickler.class, "Ini");
    xstream.useAttributeFor(LoiStrickler.class, "Max");

    xstream.alias("ListeCalculs", ListeCalculs.class);
    xstream.addImplicitCollection(ListeCalculs.class, "calculs");

    xstream.alias("ParamCalc", ParamCalc.class);
    xstream.useAttributeFor(ParamCalc.class, "CalculRef");
    xstream.useAttributeFor(ParamCalc.class, "EMHRef");
    xstream.useAttributeFor(ParamCalc.class, "DeltaQ");

    xstream.alias("ValidationCroisee", ValidationCroisee.class);
    xstream.addImplicitCollection(ValidationCroisee.class, "groupes");

    xstream.alias("Groupe", ValidationGroupe.class);
    xstream.useAttributeFor(ValidationGroupe.class, "Id");
    xstream.useAttributeFor(ValidationGroupe.class, "Nom");

    xstream.alias("Loi", ValidationLoi.class);
    xstream.useAttributeFor(ValidationLoi.class, "LoiRef");

    xstream.registerConverter(EnumsConverter
        .createEnumConverter(createTypeCalageCriteresMap(), EnumAocTypeCalageCritere.class, ctuluLog));
    xstream.registerConverter(EnumsConverter
        .createEnumConverter(createTypeCalageDonneesMap(), EnumAocTypeCalageDonnees.class, ctuluLog));
    xstream.registerConverter(
        EnumsConverter.createEnumConverter(createCalageTypeMap(), EnumAocCalageType.class, ctuluLog));
    xstream.registerConverter(EnumsConverter
        .createEnumConverter(createCalageAlgorithmeMap(), EnumAocCalageAlgorithme.class, ctuluLog));
  }

  public static class EtudeAssociee {
    public String Chemin;
    public String NomScenario;
  }

  public static class TypeCalage {
    public EnumAocTypeCalageDonnees Donnees;
    public EnumAocTypeCalageCritere Critere;
  }

  public static class Calage {
    public EnumAocCalageType Type;
    public EnumAocCalageAlgorithme Algorithme;
    public CalageSeul Seul;
    public CalageTestRepetabilite TestRepetabilite;
    public CalageValidationCroisee ValidationCroisee;
  }

  public static class CalageSeul {
    public int NombreIteration;
  }

  public static class CalageTestRepetabilite {
    public int NombreIterationTir;
    public int NombreTir;
  }

  public static class CalageValidationCroisee {
    public int NombreIteration;
    public double PonderationApprentissage;
    public double PonderationValidation;
  }

  public static class DonneesCampagne {
    public LoisCalculsPermanents LoisCalculsPermanents;
    public LoisCalculsTransitoires LoisCalculsTransitoires;
    public LoisStrickler LoisStrickler;
    public ListeCalculs ListeCalculs;
    public ValidationCroisee ValidationCroisee;
  }

  public static class ParametresNumeriques {
    public double Amplitude;
    public double TemperatureInitiale;
    public double TemperatureFinale;
    public double SeuilRMSEZ;
    public double SeuilRMSEQ;
    public double SeuilT;
    public double SeuilZmax;
    public double SeuilQmax;
    public double SeuilVol;
  }

  public static class LoisCalculsPermanents {
    public List<LoiCalculPermanent> calculs = new ArrayList<>();
  }

  public static class LoisCalculsTransitoires {
    public List<LoiCalculTransitoire> calculs = new ArrayList<>();
  }

  public static class LoisStrickler {
    public List<LoiStrickler> stricklers = new ArrayList<>();
  }

  public static class ListeCalculs {
    public List<ParamCalc> calculs = new ArrayList<>();
  }

  public static class ValidationCroisee {
    public List<ValidationGroupe> groupes = new ArrayList<>();
  }

  public static class ValidationGroupe {
    public String Id;
    public String Nom;
    public List<ValidationLoi> Lois = new ArrayList<>();
    public int EffectifApprentissage;
    public String Commentaire;

    public ValidationGroupe() {
    }

    public ValidationGroupe(final String id, final String nom, final int effectifApprentissage, final String commentaire) {
      Id = id;
      Nom = nom;
      EffectifApprentissage = effectifApprentissage;
      Commentaire = commentaire;
    }
  }

  public static class LoiCalculPermanent {
    public String CalculRef;
    public String LoiRef;
    public int Ponderation;

    public LoiCalculPermanent() {

    }

    public LoiCalculPermanent(final String calculRef, final String loiRef, final int ponderation) {
      CalculRef = calculRef;
      LoiRef = loiRef;
      Ponderation = ponderation;
    }
  }

  public static class LoiCalculTransitoire {
    public String CalculRef;
    public String LoiRef;
    public String SectionRef;
    public int Ponderation;

    public LoiCalculTransitoire() {

    }

    public LoiCalculTransitoire(final String calculRef, final String loiRef, final String sectionRef, final int ponderation) {
      CalculRef = calculRef;
      LoiRef = loiRef;
      SectionRef = sectionRef;
      Ponderation = ponderation;
    }
  }

  public static class LoiStrickler {
    public String LoiRef;
    public int Min;
    public int Ini;
    public int Max;

    public LoiStrickler() {

    }

    public LoiStrickler(final String loiRef, final int min, final int ini, final int max) {
      LoiRef = loiRef;
      Min = min;
      Ini = ini;
      Max = max;
    }
  }

  public static class ParamCalc {
    public String CalculRef;
    public String EMHRef;
    public double DeltaQ;

    public ParamCalc() {

    }

    public ParamCalc(final String calculRef, final String EMHRef, final double deltaQ) {
      CalculRef = calculRef;
      this.EMHRef = EMHRef;
      DeltaQ = deltaQ;
    }
  }

  public static class ValidationLoi {
    public String LoiRef;

    public ValidationLoi() {

    }

    public ValidationLoi(final String loiRef) {
      LoiRef = loiRef;
    }

    public String getLoiRef() {
      return LoiRef;
    }

    public void setLoiRef(final String loiRef) {
      LoiRef = loiRef;
    }
  }
}
