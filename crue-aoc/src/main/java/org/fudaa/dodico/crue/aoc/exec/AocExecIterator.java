package org.fudaa.dodico.crue.aoc.exec;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmContainer;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmDataItem;
import org.fudaa.dodico.crue.aoc.exec.writer.AocExecFormatterHelper;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageAlgorithme;
import org.fudaa.dodico.crue.metier.etude.EMHRun;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Classe executant les iterations
 *
 * @author deniger
 */
public class AocExecIterator implements AocExecIteratorContrat {
  public static final int DEFAULT_MAX_ERROR_ALLOWED = 5;
  public static final String PROPERTY_AOC_MAX_ERROR_ALLOWED_FOR_CRUE10 = "aoc.maxErrorAllowedForCrue10";
  private static int maxErrorAllowed = -1;
  private final AocExecProcessor parent;
  AocExecRunLauncher currentLauncher;

  public AocExecIterator(AocExecProcessor parent) {

    this.parent = parent;
    if (maxErrorAllowed < 0) {
      maxErrorAllowed = DEFAULT_MAX_ERROR_ALLOWED;
      String byEnv = System.getProperty(PROPERTY_AOC_MAX_ERROR_ALLOWED_FOR_CRUE10);
      if (StringUtils.isNumeric(byEnv)) {
        try {
          maxErrorAllowed = Integer.parseInt(byEnv);
        } catch (Exception exception) {
          Logger.getLogger(getClass().getName()).warning("the property aoc.maxErrorAllowedForCrue10 is not an integer and is ignored");
        }
      }
      if (maxErrorAllowed < 0) {
        maxErrorAllowed = DEFAULT_MAX_ERROR_ALLOWED;
      }
    }
  }

  /**
   * @return l'algo a utiliser. Attention, ils peuvent avoir des états.
   */
  protected AocExecAlgoContrat createContrat(AocExecContainer data) {
    //pour validation croisee on force l'algo
    if (AocExecHelper.isValidationCroisee(data.getCampagne()) || data.getCampagne().getCalage().getAlgorithme().equals(EnumAocCalageAlgorithme.RECUIT)) {
      return new AocExecAlgoRecuit(data.getCampagne());
    }
    return new AocExecAlgoMonteCarlo();
  }

  /**
   * @param in
   */
  @Override
  public CrueOperationResult<AocExecContainer> compute(CrueOperationResult<AocExecContainer> in, ProgressionUpdater progressionUpdater, String prefixForProgressionUpdater,
                                                       AocExecAllLogger logger) {

    final AocExecContainer aocExecContainer = in.getResult();
    //pour extraire les resultats
    AocExecResultExtractor extractor = new AocExecResultExtractor(aocExecContainer);
    final AocExecAlgoContrat algoContrat = createContrat(aocExecContainer);

    //les resultats:
    List<AocExecResultIteration> iterations = new ArrayList<>();
    in.getResult().getResultContainer().setResultIterations(iterations);
    //pour la gestion des dclm
    final AocDclmContainer dclmBuilder = AocDclmContainer
        .create(aocExecContainer.getCampagne(), aocExecContainer.getScenarioReference(), aocExecContainer.getProjet().getPropDefinition());
    final boolean debitCouldBeModified = AocExecHelper.debitCouldBeModified(dclmBuilder, in.getResult().getCampagne());

    final List<AocDclmDataItem> initDclm = debitCouldBeModified ? dclmBuilder.generateNotModifiedValues() : null;

    final int idxForInitValue = 0;
    final int nbIteration = AocExecHelper.getNbIterations(aocExecContainer.getCampagne());
    final int idxToUseOptimalStrickerAsInteger = nbIteration + 1;
    final int nbIterationMax = nbIteration + 2;

    logger.addLogFormated("Nombre d'itérations dans le fichier de configuration: %o", nbIteration);
    logger.addLogFormated("Nombre d'itérations total avec initialisation et stricklers tronqués: %o", nbIterationMax);
    logger.addLogFormated("Nombre d'echecs autorisés Crue 10: %o. Peut être modifié avec la variable d'environnement %s ( java -D...)", maxErrorAllowed,
        PROPERTY_AOC_MAX_ERROR_ALLOWED_FOR_CRUE10);
    CtuluLogGroup logsIteration = null;
    CtuluLog logForCrue10Error = null;
    int currentErrorCount = 0;
    List<EMHRun> runsToDelete = new ArrayList<>();
    for (int i = 0; i < nbIterationMax; i++) {
      logger.flush();
      logger.addLogFormated("");

      progressionUpdater.majProgessionStateOnly(prefixForProgressionUpdater + "iteration " + (i + 1) + " / " + nbIterationMax);
      progressionUpdater.majAvancement();
      //on reinitialise les logs si on passe à l'iteration suivante:
      if (logsIteration == null || currentErrorCount == 0) {
        logsIteration = in.getLogs().createGroup("aoc.iteration.log");
        logsIteration.setDescriptionArgs(Integer.toString(i + 1));
      }

      if (currentErrorCount > 0) {
        //plantage à l'initialisation -> on ne peut pas recommencer
        if (i == idxForInitValue) {
          logForCrue10Error = createLogForCrue10Failure(logsIteration, logForCrue10Error);
          logForCrue10Error.addError("aoc.iteration.crue10Failure.initIteration");
          logger.addLogFormated("Iterations stoppées car Crue10 échoue avec les valeurs initiales");
          aocExecContainer.getResultContainer().setiterationsStoppedDueToCrue10Failure(EnumAocExecCrue10Failure.FAILURE_AT_INIT_ITERATION);
          break;
        } else if (i == idxToUseOptimalStrickerAsInteger) {
          logForCrue10Error = createLogForCrue10Failure(logsIteration, logForCrue10Error);
          logForCrue10Error.addError("aoc.iteration.crue10Failure.optimumStricklerAsInteger");
          logger.addLogFormated("Iterations stoppées car Crue10 échoue avec les valeurs optimales tronquées pour les stricklers");
          aocExecContainer.getResultContainer().setiterationsStoppedDueToCrue10Failure(EnumAocExecCrue10Failure.FAILURE_AT_OPTIMUM_STRICKLER_AS_INTEGER);
          break;
        } else if (currentErrorCount > maxErrorAllowed) {
          logForCrue10Error = createLogForCrue10Failure(logsIteration, logForCrue10Error);
          logForCrue10Error.addError("aoc.iteration.crue10Failure.maxErrorReached", maxErrorAllowed);
          logger.addLogFormated("Iterations stoppées car Crue 10 a échoué %o fois ( max autorisé=%o)", currentErrorCount - 1, maxErrorAllowed);
          aocExecContainer.getResultContainer().setiterationsStoppedDueToCrue10Failure(EnumAocExecCrue10Failure.TOO_MANY_FAILURE);
          break;
        }
      }
      if (currentErrorCount == 0) {
        logger.addLogFormated("iteration %o", i + 1);
      } else {
        logger.addLogFormated("iteration %o relancée car erreur crue 10 ( erreur n° %o)", i + 1, currentErrorCount);
      }
      //Step 1: avec les valeurs initiales
      Map<String, Double> dfrtValues;
      List<AocDclmDataItem> dclmValues;
      //premiere iteration: seuls les frottements sont modifiés
      if (i == idxForInitValue) {
        dfrtValues = AOCStricklerBuilder.buildInitValue(aocExecContainer.getCampagne().getDonnees().getLoisStrickler());
        //les valeurs dclm sont chargées depuis le scenario
        dclmValues = initDclm;
        logger.addLogDfrtDclm(";Utilisation des valeurs initiales", dfrtValues, dclmValues);
        //derniere itearation: on tronque les frt optimums et les dclms associés
      } else if (i == idxToUseOptimalStrickerAsInteger) {
        final int minCritereIdx = AocExecResultIteration.computeMinCritere(iterations);
        final AocExecResultIteration resultWithOptimum = iterations.get(minCritereIdx);
        logger.addLogFormated(";Récupération Valeurs des Stricklers tronqués et DCLM sur itération %o et %s=%f", minCritereIdx + 1,
            AocExecFormatterHelper.getCritereName(aocExecContainer),
            resultWithOptimum.getCritere());
        final Map<String, Double> optimalDfrtValues = resultWithOptimum.getDfrtValues();
        final Map<String, Double> optimalDfrtValuesAsInteger = new HashMap<>(optimalDfrtValues.size());
        optimalDfrtValues.entrySet().forEach(e -> optimalDfrtValuesAsInteger.put(e.getKey(), (double) Math.round(e.getValue())));
        dfrtValues = optimalDfrtValuesAsInteger;
        dclmValues = resultWithOptimum.getDclmValues();
        logger.addLogDfrtDclm(";Valeurs des Stricklers tronqués", dfrtValues, dclmValues);
        //iteration "normale" on calcul les prochaines valeurs pour dfrt et dclm
      } else {
        logger.addLogFormated(";Calcul valeurs dfrt et dclm pour prochain calcul");
        final AocExecAlgoResult aocExecAlgoResult = algoContrat.generateForNextIteration(aocExecContainer, dclmBuilder, logger, currentErrorCount == 0);
        dfrtValues = aocExecAlgoResult.getDfrtValues();
        dclmValues = aocExecAlgoResult.getDclmValues();
        logger.addLogDfrtDclm(";Valeurs générées pour prochain calcul ", dfrtValues, dclmValues);
      }
      //frottements toujours modifiés
      AOCStricklerModifier dfrtModifier = new AOCStricklerModifier();
      CtuluLogGroup dftrModifiedLogs = dfrtModifier.applyFrottement(aocExecContainer.getProjet(), aocExecContainer.getManagerScenarioCible(), dfrtValues);
      logsIteration.addGroup(dftrModifiedLogs.createCleanGroup());
      if (dftrModifiedLogs.containsError()) {
        deleteAllRunsInCible(in);
        return in;
      }
      //on modifie les dclms si deltaQ > 0 ( pas utile sinon) et pas à la premiere iteration
      //pour i=0 on modifie aussi les dclm ce qui permet de les initialiser avec les valeurs du scenario de reference.
      //cela est important dans le cas de tir multiple: le tir précédent peut avoir modifié les dclm du scenario cible.
      if (debitCouldBeModified && dclmValues != null) {
        final CtuluLog logModificationDclm = AOCDclmModifier.applyDclms(aocExecContainer.getProjet(), aocExecContainer.getManagerScenarioCible(), dclmValues);
        if (logModificationDclm.isNotEmpty()) {
          logsIteration.addLog(logModificationDclm);
          if (logModificationDclm.containsErrorOrSevereError()) {
            deleteAllRunsInCible(in);
            return in;
          }
        }
      }
      if (parent.isStop()) {
        deleteAllRunsInCible(in);
        return in;
      }
      //step 2: lancer un run complet
      currentLauncher = parent.createLauncher(aocExecContainer);

      //demarrage crueX avec chronométrage:
      in.getResult().getResultContainer().getTimeInfo().startCrueX();
      CrueOperationResult<EMHRun> runCreated = currentLauncher.launchRun(logsIteration);

      in.getResult().getResultContainer().getTimeInfo().endCrueX();
      if (currentLauncher.isComputeFinishedWithError()) {
        //on annule l'itération
        i--;
        currentErrorCount++;
        logger.addLogDfrtDclm("Echec n°" + currentErrorCount + " Crue 10 sur iteration " + i, dfrtValues, dclmValues);
        in.getResult().getResultContainer().addFatalErrorCrue10(dfrtValues, dclmValues, i);
        continue;
      } else {
        //on ajoute un log pour l'utilisateur
        if (currentErrorCount > 0) {
          logForCrue10Error = createLogForCrue10Failure(logsIteration, logForCrue10Error);
          logForCrue10Error.addWarn("aoc.iteration.crue10Failure.report", currentErrorCount);
        }
        //et on reinitialise les variables temporaires
        logForCrue10Error = null;
        currentErrorCount = 0;
      }
      if (parent.isStop() || runCreated.isErrorOrEmpty()) {
        deleteAllRunsInCible(in);
        return in;
      }

      //lecture des résultats
      CrueOperationResult<AocExecResultReader> runLoadOperation = currentLauncher.loadRun(runCreated.getResult());
      if (runLoadOperation.getLogs().containsError()) {
        logsIteration.addGroup(runLoadOperation.getLogs().createCleanGroup());
      }
      if (runLoadOperation.isErrorOrEmpty()) {
        deleteAllRunsInCible(in);
        return in;
      }

      //apres le calcul, on extrait les données de l'iteration
      final CtuluLog logExtractionResultat = logsIteration.createNewLog("aoc.exec.extractionResultat");
      logExtractionResultat.setDescriptionArgs(Integer.toString(i + 1));
      extractor.setOperationLog(logExtractionResultat);
      AocExecResultIteration iteration = extractor.create(runLoadOperation.getResult(), dfrtValues, dclmValues, logger, i);
      if (logExtractionResultat.containsErrorOrSevereError()) {
        deleteAllRunsInCible(in);
        return in;
      }
      if (i == idxToUseOptimalStrickerAsInteger) {
        iteration.setIterationType(AocEnumIterationType.STRICKLER_TRONQUE);
        iteration.setRunTroncature(runCreated.getResult());
        logger.addLogFormated("");
        logger.addLogFormated("Le run %s (iteration troncature) sera conserve", runCreated.getResult().getNom());
      } else if (i == idxForInitValue) {
        iteration.setIterationType(AocEnumIterationType.INITIALISATION);
      } else {
        algoContrat.fillIterationResultWithParamater(iteration);
      }

      if (i != idxToUseOptimalStrickerAsInteger) {
        runsToDelete.add(runCreated.getResult());
      }
      iterations.add(iteration);
    }

    logger.addLogFormated("Suppression runs;; " + ToStringHelper.transformToNom(runsToDelete));
    this.currentLauncher.deleteRunsAndSave(in.getResult().getManagerScenarioCible(), runsToDelete, in.getLogs());
    logger.addLogFormated("");
    logger.addLogFormated("Fin iterations");

    if (in.getResult().getResultContainer().isStoppedDueToCrue10Error()) {
      return in;
    }

    //on recherche le critere optimal
    int idxMin = AocExecResultIteration.computeMinCritere(iterations);
    in.getResult().getResultContainer().setResultIterations(iterations);
    in.getResult().getResultContainer().setIterationWithMinCritere(iterations.get(idxMin));
    final AocExecResultIteration withOptimumStricklerAsInt = in.getResult().getResultContainer().getIterationWithOptimumStricklerAsInteger();

    //on ecrit les valeurs dans les fichiers dfrt/dclm
    if (withOptimumStricklerAsInt != null) {
      progressionUpdater.majProgessionStateOnly(prefixForProgressionUpdater + BusinessMessages.getString("aoc.exec.progression.modificationScenarioCible"));
      String critereName = AocExecFormatterHelper.getCritereName(aocExecContainer);
      logger.addLogFormated("Iteration avec %s min (valeur= %f) et stricklers tronqués sur iteration %o", critereName, withOptimumStricklerAsInt.getCritere(),
          withOptimumStricklerAsInt.getIndex() + 1);
      //le scenario cible doit contenir les valeurs optimales.
      AOCStricklerModifier dfrtModifier = new AOCStricklerModifier();
      CtuluLogGroup dftrModifiedLogs = dfrtModifier
          .applyFrottement(aocExecContainer.getProjet(), aocExecContainer.getManagerScenarioCible(), withOptimumStricklerAsInt.getDfrtValues());
      in.getLogs().addGroup(dftrModifiedLogs);
      if (debitCouldBeModified) {
        final CtuluLog logModificationDclm = AOCDclmModifier
            .applyDclms(aocExecContainer.getProjet(), aocExecContainer.getManagerScenarioCible(), withOptimumStricklerAsInt.getDclmValues());
        in.getLogs().addLog(logModificationDclm);
      }
    }
    return in;
  }

  private CtuluLog createLogForCrue10Failure(CtuluLogGroup logsIteration, CtuluLog logForCrue10Error) {
    if (logForCrue10Error == null) {
      return logsIteration.createNewLog("aoc.iteration.crue10Failure.log");
    }
    return logForCrue10Error;
  }

  public void deleteAllRunsInCible(CrueOperationResult<AocExecContainer> in) {
    currentLauncher.deleteAllRunsAndSave(in.getResult().getManagerScenarioCible(), in.getLogs());
  }

  @Override
  public void stop() {
    if (currentLauncher != null) {
      currentLauncher.stop();
    }
  }
}
