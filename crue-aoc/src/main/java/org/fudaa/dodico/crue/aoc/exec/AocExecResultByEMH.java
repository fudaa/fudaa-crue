package org.fudaa.dodico.crue.aoc.exec;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.aoc.exec.writer.AocExecFormatterHelper;

import java.text.NumberFormat;
import java.util.List;

/**
 * @author deniger
 */
public class AocExecResultByEMH {
  private String nomVariable;
  private final AocDataContainer observedValues = new AocDataContainer();
  private final AocDataContainer computedValues = new AocDataContainer();
  private final AocDataContainer deltaValues = new AocDataContainer();
  private final NumberFormat numberFormat;
  private String nameVariableObserved;
  private String nameVariableComputed;
  private String nameVariableDelta;

  public AocExecResultByEMH(String nomVariable, NumberFormat numberFormat) {
    this.nomVariable = nomVariable;
    this.numberFormat = numberFormat;
  }

  public void setNameVariableObserved(String nameVariableObserved) {
    this.nameVariableObserved = nameVariableObserved;
  }

  public void setNameVariableComputed(String nameVariableComputed) {
    this.nameVariableComputed = nameVariableComputed;
  }

  public void setNameVariableDelta(String nameVariableDelta) {
    this.nameVariableDelta = nameVariableDelta;
  }

  public String getNomVariableObserved() {
    return StringUtils.defaultString(nameVariableObserved, AocExecVariableHelper.getNomVariableObserved(getNomVariable()));
  }

  public String getNomVariableComputed() {
    return StringUtils.defaultString(nameVariableComputed, AocExecVariableHelper.getNomVariableComputed(getNomVariable()));
  }

  public String getNomVariableDelta() {
    return StringUtils.defaultString(nameVariableDelta, AocExecVariableHelper.getNomVariableDelta(getNomVariable()));
  }

  public String getNomVariable() {
    return nomVariable;
  }

  public void setNomVariable(String nomVariable) {
    this.nomVariable = nomVariable;
  }

  public void addComputedValueForEMH(String emhName, double computedValue) {
    computedValues.addValue(emhName, computedValue);
  }

  public void addObservedValueForEMH(String emhName, double computedValue) {
    observedValues.addValue(emhName, computedValue);
  }

  public void addDeltaValueForEMH(String emhName, double computedValue) {
    deltaValues.addValue(emhName, computedValue);
  }

  public String getObservedValuesAsString(List<String> emhs, String nameColumnOne, String nameColumnTwo) {
    return observedValues.lineToString(emhs, nameColumnOne, nameColumnTwo, numberFormat);
  }

  public Double getComputedValue(String emh) {
    return computedValues.get(emh);
  }

  public String getObservedValueAsString(String emh) {
    return AocExecFormatterHelper.formatValue(numberFormat, observedValues.get(emh));
  }

  public String getComputedValueAsString(String emh) {
    return AocExecFormatterHelper.formatValue(numberFormat, computedValues.get(emh));
  }

  public String getDeltaValueAsString(String emh) {
    return AocExecFormatterHelper.formatValue(numberFormat, deltaValues.get(emh));
  }

  public boolean containsObservedValue() {
    return !observedValues.isEmpty();
  }

  public String getComputedValuesAsString(List<String> emhs, String nameColumnOne, String nameColumnTwo) {
    return computedValues.lineToString(emhs, nameColumnOne, nameColumnTwo, numberFormat);
  }

  public String getDeltaValuesAsString(List<String> emhs, String nameColumnOne, String nameColumnTwo) {
    return deltaValues.lineToString(emhs, nameColumnOne, nameColumnTwo, numberFormat);
  }
}
