package org.fudaa.dodico.crue.aoc.projet;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;

/**
 * Classe abstraite faisant le lien entre un calcul, une loi et une ponderation
 */
public class AbstractLoiCalcul {
    public static final int DEFAULT_PONDERATION_VALUE = 1;
    private String calculRef;
    private String loiRef;
    private int ponderation = DEFAULT_PONDERATION_VALUE;

    public AbstractLoiCalcul() {

    }

    public AbstractLoiCalcul(String calculRef, String loiRef, int ponderation) {
        this.calculRef = calculRef;
        this.loiRef = loiRef;
        this.ponderation = ponderation;
    }

    public String getCalculRef() {
        return calculRef;
    }

    public void setCalculRef(String calculRef) {
        this.calculRef = calculRef;
    }

    public String getLoiRef() {
        return loiRef;
    }

    public void setLoiRef(String loiRef) {
        this.loiRef = loiRef;
    }

    @PropertyDesc(i18n = "ponderation.property")
    public int getPonderation() {
        return ponderation;
    }

    public void setPonderation(int ponderation) {
        this.ponderation = ponderation;
    }
}
