package org.fudaa.dodico.crue.aoc.helper;

import org.fudaa.dodico.crue.aoc.projet.AocValidationCroisee;
import org.fudaa.dodico.crue.aoc.projet.AocValidationGroupe;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author deniger
 */
public class AocValidationCroiseeLineBuilder {
  /**
   * @param groupe les parametres de validation croisee
   * @return les permutations apprentissage/validation
   */
  public List<AocValidationCroiseeLine> generatePermutations(final AocValidationCroisee groupe) {
    //on genere les permutation par gamme
    final List<PermutationByGroupGamme> permutationByGroups = groupe.getGroupes().stream().map(this::generatePermutationByGroup).collect(Collectors.toList());
    final List<Permutation> permutations = addAllPermutations(new ArrayList<>(), 0, permutationByGroups, null);

    //pour la determintation de la validation ( complement)
    final List<String> allLignes = getAllLigneEau(groupe);

    //les resultats
    final List<AocValidationCroiseeLine> res = new ArrayList<>();
    permutations.forEach(permutation -> {
      final AocValidationCroiseeLine line = new AocValidationCroiseeLine();
      //l'apprentissage
      line.getLigneEauApprentissage().addAll(permutation.ligneEau);
      //la validation
      line.updateValidationWithComplement(allLignes);
      res.add(line);
    });
    return res;
  }

  public static List<String> getAllLigneEau(final AocValidationCroisee groupe) {
    final List<String> allLignes = new ArrayList<>();
    groupe.getGroupes().forEach(groupeItem ->
        groupeItem.getLois().forEach(e -> allLignes.add(e.getLoiRef()))
    );
    return allLignes;
  }

  /**
   * @param groupe le goupe
   * @return l'ensembre des permutation pour le groupe
   */
  public PermutationByGroupGamme generatePermutationByGroup(final AocValidationGroupe groupe) {
    final PermutationByGroupGamme byGroup = new PermutationByGroupGamme();
    combine(byGroup, 0, groupe, null);
    return byGroup;
  }

  /**
   * @param permutationsResults la liste résultat de toutes les permutations
   * @param idxStart l'indice de départ
   * @param permutationsByGamme les permutations par gammme
   * @param parent la permutation parent calculee a partir de la gamme superieur
   * @return la valeur permutationsResults
   */
  private List<Permutation> addAllPermutations(final List<Permutation> permutationsResults, final int idxStart, final List<PermutationByGroupGamme> permutationsByGamme, final Permutation parent) {
    if (idxStart >= permutationsByGamme.size()) {
      permutationsResults.add(parent);
      return permutationsResults;
    }
    final PermutationByGroupGamme group = permutationsByGamme.get(idxStart);
    final int size = group.getSize();
    for (int i = 0; i < size; i++) {
      final Permutation toUse = parent == null ? new Permutation() : new Permutation(parent);
      toUse.ligneEau.addAll(group.permutations.get(i).ligneEau);
      addAllPermutations(permutationsResults, idxStart + 1, permutationsByGamme, new Permutation(toUse));
    }
    return permutationsResults;
  }

  /**
   * @param result la liste qui ve recevoir les permutations
   * @param start l'indice de de départ
   * @param current la permutation en cours
   */
  private void combine(final PermutationByGroupGamme result, final int start, final AocValidationGroupe group, final Permutation current) {

    if (current != null && current.getSize() == group.getEffectifApprentissage()) {
      result.addPermutation(current);
      return;
    }

    for (int i = start; i < group.getLois().size(); i++) {
      final Permutation toUse = current == null ? new Permutation() : new Permutation(current);
      toUse.ligneEau.add(group.getLois().get(i).getLoiRef());
      combine(result, i + 1, group, new Permutation(toUse));
    }
  }

  public static class PermutationByGroupGamme {
    final List<Permutation> permutations = new ArrayList<>();

    public int getSize() {
      return permutations.size();
    }

    public Permutation getPermutation(final int idx) {
      return permutations.get(idx);
    }

    public void addPermutation(final Permutation permutation) {
      permutations.add(permutation);
    }
  }

  public static class Permutation {
    final List<String> ligneEau = new ArrayList<>();

    public Permutation() {

    }

    public Permutation(final Permutation other) {
      ligneEau.addAll(other.ligneEau);
    }

    public int getSize() {
      return ligneEau.size();
    }

    public String getValue(final int idx) {
      return ligneEau.get(idx);
    }
  }
}
