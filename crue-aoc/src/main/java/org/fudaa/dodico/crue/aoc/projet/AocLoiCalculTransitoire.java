package org.fudaa.dodico.crue.aoc.projet;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.metier.aoc.AocWithSectionRef;

/**
 * Created by deniger on 28/06/2017.
 */
public class AocLoiCalculTransitoire extends AbstractLoiCalcul implements AocWithSectionRef {
    private String sectionRef;

    public AocLoiCalculTransitoire() {

    }

    public AocLoiCalculTransitoire(String calculRef, String loiRef, String sectionRef, int ponderation) {
        super(calculRef, loiRef, ponderation);
        this.sectionRef = sectionRef;
    }

    @PropertyDesc(i18n = "sectionRef.property")
    public String getSectionRef() {
        return sectionRef;
    }

    public void setSectionRef(String sectionRef) {
        this.sectionRef = sectionRef;
    }

    public AocLoiCalculTransitoire clone() {
        return new AocLoiCalculTransitoire(getCalculRef(), getLoiRef(), getSectionRef(), getPonderation());
    }
}
