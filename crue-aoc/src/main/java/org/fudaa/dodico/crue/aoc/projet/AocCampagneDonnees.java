package org.fudaa.dodico.crue.aoc.projet;

/**
 * Created by deniger on 28/06/2017.
 */
public class AocCampagneDonnees {
  private final AocLoisCalculsPermanents loisCalculsPermanents = new AocLoisCalculsPermanents();
  private final AocLoisCalculsTransitoires loisCalculsTransitoires = new AocLoisCalculsTransitoires();
  private final AocLoisStrickler loisStrickler = new AocLoisStrickler();
  private final AocListeCalculs listeCalculs = new AocListeCalculs();
  private final AocValidationCroisee validationCroisee = new AocValidationCroisee();

  public AocLoisCalculsPermanents getLoisCalculsPermanents() {
    return loisCalculsPermanents;
  }

  public AocLoisCalculsTransitoires getLoisCalculsTransitoires() {
    return loisCalculsTransitoires;
  }

  public AocLoisStrickler getLoisStrickler() {
    return loisStrickler;
  }

  public AocListeCalculs getListeCalculs() {
    return listeCalculs;
  }

  public AocValidationCroisee getValidationCroisee() {
    return validationCroisee;
  }
}
