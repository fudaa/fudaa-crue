package org.fudaa.dodico.crue.aoc.batch;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.aoc.io.CrueAOCReaderWriter;
import org.fudaa.dodico.crue.aoc.io.CrueRAOCReaderWriter;
import org.fudaa.dodico.crue.batch.BatchLogSaver;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.joda.time.LocalDateTime;

import java.io.File;

/**
 * Pour gerer et persister les données liées à la campagne
 *
 * @author deniger
 */
public class AocBatchLineSaver implements BatchLogSaver {
  private File aocFile;
  private CtuluLog mainLog;
  private AocCampagneResult aocCampagneResult;
  private boolean started;

  public AocBatchLineSaver() {
  }

  public static void writeGlobalLogFile(AocCampagneResult aocCampagneResult, File aocFile) {
    final CrueRAOCReaderWriter writer = new CrueRAOCReaderWriter(CrueRAOCReaderWriter.LAST_VERSION);
    final CrueIOResu<AocCampagneResult> resu = new CrueIOResu<>(aocCampagneResult);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    File fileForROAC = CrueFileHelper.changeExtension(aocFile, CrueAOCReaderWriter.FILE_TYPE + ".xml", CrueRAOCReaderWriter.FILE_TYPE + ".xml");
    final CtuluLogGroup globalValidation = resu.getMetier().getGlobalValidation();
    resu.getMetier().setGlobalValidation(globalValidation);
    writer.writeXMLMetier(resu, fileForROAC, log, null);
  }

  public void keepOnlyError() {
    if (aocCampagneResult != null && aocCampagneResult.getGlobalValidation() != null) {
      aocCampagneResult.setGlobalValidation(aocCampagneResult.getGlobalValidation());
    }
  }

  public void setAocFile(File aocFile) {
    this.aocFile = aocFile;
  }

  public CtuluLogGroup getGlobalValidation() {
    return aocCampagneResult.getGlobalValidation();
  }

  @Override
  public void close() {
    writeGlobalLogFile(aocCampagneResult, aocFile);
  }

  @Override
  public CtuluLog start(boolean isBatch) {
    if (started) {
      return mainLog;
    }
    started = true;
    mainLog = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    if (isBatch) {
      mainLog.setDesc("aocBatch.launch");
    }
    aocCampagneResult = new AocCampagneResult();
    aocCampagneResult.setDate(new LocalDateTime());
    aocCampagneResult.getGlobalValidation().addLog(mainLog);
    return mainLog;
  }
}
