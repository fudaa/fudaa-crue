package org.fudaa.dodico.crue.aoc.batch;

import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.commun.ProgressionTestAdapter;
import org.fudaa.dodico.crue.aoc.exec.AocExecProcessor;
import org.fudaa.dodico.crue.batch.BatchData;
import org.fudaa.dodico.crue.batch.BatchHelper;
import org.fudaa.dodico.crue.batch.BatchLauncher;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformationDefault;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.common.transformer.LogsHelper;
import org.fudaa.dodico.crue.projet.calcul.CalculCrueRunnerManagerImpl;
import org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager;
import org.fudaa.dodico.crue.projet.conf.UserConfiguration;

import java.io.File;

/**
 * @author deniger
 */
public class AocBatch {
  public static void main(String[] args) {
    try {
      CtuluLog runAoc = runAoc(args);
      runAoc.updateLocalizedMessage(BusinessMessages.RESOURCE_BUNDLE);
      if (runAoc.containsErrorOrSevereError()) {
        System.err.println(runAoc.getResume());
      } else {
        System.out.println(runAoc.getResume());
      }
    } catch (Exception e) {
      e.printStackTrace();//mode console
    }
    System.exit(0);
  }

  public static CtuluLog runAoc(String[] args) {
    AocBatchLineSaver saver = new AocBatchLineSaver();
    CtuluLog initLog = saver.start(true);
    initLog.setDesc("aocBatch.launch");
    if (ArrayUtils.isEmpty(args)) {
      initLog.addSevereError("aocBatch.noArgument");
      saver.close();
      return initLog;
    }
    File aocFile = new File(args[0]).getAbsoluteFile();
    final String otfaFileAbsolutePath = aocFile.getAbsolutePath();
    if (!aocFile.exists()) {
      initLog.addSevereError("aocBatch.fileNotExists", otfaFileAbsolutePath);
      saver.close();
      return initLog;
    }
    saver.setAocFile(aocFile);
    final String messageFileLoaded = "aocBatch.fileLoaded";

    BatchData data = BatchLauncher.prepare(saver, messageFileLoaded, otfaFileAbsolutePath);

    if (data.getLog().containsErrorOrSevereError()) {
      saver.close();
      return data.getLog();
    }
    GlobalOptionsManager optionsManager = new GlobalOptionsManager();
    CtuluLogGroup init = optionsManager.init(data.getConfiguration(), new UserConfiguration());
    if (init.containsFatalError()) {
      initLog.addSevereError("otfaBatch.siteConfigNotLoadable");
      saver.getGlobalValidation().addGroup(init.createCleanGroup());
      saver.close();
      return data.getLog();
    }

    final ConnexionInformationDefault connexionInformation = new ConnexionInformationDefault();
    CalculCrueRunnerManagerImpl runner = new CalculCrueRunnerManagerImpl();
    runner.setExecOptions(BatchHelper.getExecOptions(optionsManager));
    AocExecProcessor exec = new AocExecProcessor(data.getCoeurManager(), connexionInformation, runner);
    initLog.addInfo("aocBatch.aocLaunched", otfaFileAbsolutePath);
    final CrueOperationResult launch = exec.launch(aocFile, new ProgressionTestAdapter());
    if (launch.getLogs().containsError()) {
      initLog.addError("aocBatch.aocTerminatedWithError");
    } else {
      initLog.addInfo("aocBatch.aocTerminated");
    }
    final CtuluLogGroup cleanGroup = launch.getLogs().createCleanGroup();
    saver.getGlobalValidation().addGroup(cleanGroup);
    saver.close();
    if (cleanGroup.containsError()) {
      System.err.println(LogsHelper.toText(cleanGroup, true));
    }
    return initLog;
  }
}
