package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.dodico.crue.aoc.dclm.AocDclmContainer;

/**
 * @author deniger
 */
public interface AocExecAlgoContrat {
  /**
   *
   * @param aocExecContainer le container
   * @param dclmBuilder utilitaire pour les données dclm
   * @param doIterate si true, l'algo peut passer à l'iteration suivante.Sinon reste sur les meme paramètres et cela est le cas si plantage de crue10.
   * @return les données a utiliser pour la prochaine iteration. Si null pour le debit, les valeurs initiales doivent etre utilisées.
   */
   AocExecAlgoResult generateForNextIteration(AocExecContainer aocExecContainer, AocDclmContainer dclmBuilder, AocExecAllLogger logger, boolean doIterate);

  /**
   * Permet d'ajouter les informations nécessaire dans le résultat de l'iteration
   * @param resultIteration
   */
  void fillIterationResultWithParamater(AocExecResultIteration resultIteration);

}
