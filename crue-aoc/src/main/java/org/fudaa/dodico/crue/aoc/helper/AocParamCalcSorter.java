package org.fudaa.dodico.crue.aoc.helper;

import org.fudaa.dodico.crue.aoc.projet.AocListeCalculs;
import org.fudaa.dodico.crue.aoc.projet.AocParamCalc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author deniger
 */
public class AocParamCalcSorter {
    private final Map<String, ByCalc> paramByCalc = new HashMap<>();

    public AocParamCalcSorter(AocListeCalculs listeCalculs) {
        this(listeCalculs.getParamCalcs());
    }

    public AocParamCalcSorter(List<AocParamCalc> paramCalcList) {
        for (AocParamCalc aocParamCalc : paramCalcList) {
            paramByCalc.putIfAbsent(aocParamCalc.getCalculRef(), new ByCalc());
            paramByCalc.get(aocParamCalc.getCalculRef()).add(aocParamCalc);
        }
    }

    public int getNbCalc() {
        return paramByCalc.size();
    }

    public ByCalc getForCalcul(String calcul) {
        return paramByCalc.get(calcul);
    }

    public AocParamCalc get(String calcul, String emh) {
        final ByCalc calculValues = getForCalcul(calcul);
        return calculValues == null ? null : calculValues.getAocParamCalc(emh);
    }

    public AocParamCalc getAndRemove(String calcul, String emh) {
        final ByCalc calculValues = getForCalcul(calcul);
        return calculValues == null ? null : calculValues.getAocParamCalcAndRemove(emh);
    }

    public List<AocParamCalc> getAocParamCalcs() {
        List<AocParamCalc> res = new ArrayList<>();
        for (ByCalc byCalc : paramByCalc.values()) {
            res.addAll(byCalc.paramByEmh.values());
        }
        return res;
    }

    public static class ByCalc {
        private final Map<String, AocParamCalc> paramByEmh = new HashMap<>();

        private void add(AocParamCalc in) {
            paramByEmh.put(in.getEmhRef(), in);
        }

        public int getNbEmh() {
            return paramByEmh.size();
        }

        public AocParamCalc getAocParamCalc(String emh) {
            return paramByEmh.get(emh);
        }

        public AocParamCalc getAocParamCalcAndRemove(String emh) {
            return paramByEmh.remove(emh);
        }
    }
}
