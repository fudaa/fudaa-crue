package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.projet.ScenarioLoader;
import org.fudaa.dodico.crue.projet.ScenarioLoaderOperation;
import org.fudaa.dodico.crue.projet.calcul.*;
import org.fudaa.dodico.crue.projet.create.RunCalculOption;
import org.fudaa.dodico.crue.projet.create.RunCreatorOptions;

import java.util.HashMap;
import java.util.List;

/**
 * Permet de lancer un run sur le scenario cible
 *
 * @author deniger
 */
public class AocExecRunLauncher {
  private final AocExecContainer aocExecContainer;
  private final EMHProjetController controller;
  private final CalculCrueRunnerManager calculCrueRunnerManager;
  private CalculCrueContrat currentRunner;

  public AocExecRunLauncher(AocExecContainer aocExecContainer, CalculCrueRunnerManager calculCrueRunnerManager, ConnexionInformation connexionInformation) {
    this.aocExecContainer = aocExecContainer;
    controller = new EMHProjetController(aocExecContainer.getProjet(), connexionInformation);
    this.calculCrueRunnerManager = calculCrueRunnerManager;
  }

  public void stop() {
    if (currentRunner != null) {
      currentRunner.stop();
    }
  }

  /**
   * @param parent
   * @return nouveau créé
   */
  public CrueOperationResult<EMHRun> launchRun(CtuluLogGroup parent) {
    final RunCreatorOptions runOptions = new RunCreatorOptions(null);
    runOptions.setComputeAll(RunCalculOption.FORCE_EXCUTE);
    final ManagerEMHScenario managerScenarioCible = aocExecContainer.getManagerScenarioCible();
    final EMHProjet projet = aocExecContainer.getProjet();
    CrueOperationResult<EMHRun> runResult = controller.createRun(managerScenarioCible.getNom(), runOptions,
        projet.getInfos().getEtuFile());
    parent.addGroup(runResult.getLogs().createCleanGroup());
    if (runResult.isErrorOrEmpty()) {
      return runResult;
    }
    if (runResult.getResult() != null) {
      final ExecInputDefault execInput = new ExecInputDefault(projet, managerScenarioCible,
          runResult.getResult());
      Crue10OptionBuilder optionsBuilder = new Crue10OptionBuilder(calculCrueRunnerManager.getExecOptions());
      ExecConfigurer defaultConfigurer = optionsBuilder.createConfigurer(execInput, RunCreatorOptions.createMap(RunCalculOption.FORCE_EXCUTE));
      currentRunner = calculCrueRunnerManager.getCrue10Runner(projet.getCoeurConfig()).createRunner(execInput, defaultConfigurer);
      currentRunner.run();
    }
    return runResult;
  }

  public boolean isComputeFinishedWithError() {
    return !isComputeFinishedCorrectly();
  }

  public boolean isComputeFinishedCorrectly() {
    return currentRunner != null && currentRunner.isComputeFinishedCorrectly();
  }

  /**
   * @param run du scenario cible
   * @return run loaded
   */
  CrueOperationResult<AocExecResultReader> loadRun(EMHRun run) {
    ScenarioLoader loader = new ScenarioLoader(aocExecContainer.getManagerScenarioCible(), aocExecContainer.getProjet(), aocExecContainer.getProjet().getCoeurConfig());
    final ScenarioLoaderOperation loadRun = loader.load(run);
    if (loadRun.isErrorOrEmpty()) {
      return new CrueOperationResult<>(null, loadRun.getLogs());
    }
    return new CrueOperationResult<>(new AocExecResultReader(loadRun.getResult()), loadRun.getLogs());
  }

  public void deleteAllRunsAndSave(ManagerEMHScenario scenario, CtuluLogGroup parentLog) {
    controller.deleteAllRuns(scenario, new HashMap<>());
    saveProject(parentLog);
  }

  public void deleteRunsAndSave(ManagerEMHScenario scenario, List<EMHRun> runsToDelete, CtuluLogGroup parentLog) {
    controller.deleteRuns(scenario, runsToDelete, new HashMap<>());
    saveProject(parentLog);
  }

  public void saveProject(CtuluLogGroup parentLog) {
    controller.saveProjet(aocExecContainer.getProjet().getInfos().getEtuFile(), parentLog);
  }
}
