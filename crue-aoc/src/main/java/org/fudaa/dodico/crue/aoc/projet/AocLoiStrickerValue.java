package org.fudaa.dodico.crue.aoc.projet;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

/**
 * @author deniger
 */
public enum AocLoiStrickerValue implements ToStringInternationalizable {

    MIN,
    INI,
    MAX;

    public int getValue(AocLoiStrickler stricker) {
        if (stricker != null) {
            switch (this) {
                case INI:
                    return stricker.getIni();
                case MIN:
                    return stricker.getMin();
                case MAX:
                    return stricker.getMax();
            }
        }
        return 0;
    }

    /**
     * @return le nom de l'EMH.
     */
    @Override
    public String geti18n() {
        return BusinessMessages.getString("aoc." + name().toLowerCase() + ".shortName");
    }

    @Override
    public String geti18nLongName() {
        return BusinessMessages.getString("aoc." + name().toLowerCase() + ".longName");
    }
}
