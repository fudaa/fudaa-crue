package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.dodico.crue.aoc.exec.writer.AocExecFormatterHelper;
import org.fudaa.dodico.crue.aoc.exec.writer.AocExecWriterHelper;

import java.text.NumberFormat;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.TreeMap;

/**
 * @author deniger
 */
public class AocDataContainer {
  private final Map<String, Double> valueById = new TreeMap<>();

  public void addValue(String id, double value) {
    valueById.put(id, value);
  }

  public boolean contains(String id) {
    return valueById.containsKey(id);
  }

  public Double get(String id) {
    return valueById.get(id);
  }


  /**
   * @param ids la liste des ids
   * @param nameColumnOne premier colonne. Si null ignore
   * @param nameColumnTwo deuxieme colonne.. Si null ignore
   * @param fmt le formatteur. si null ecrit le double en l'état
   * @return nameColumnOne;nameColumnTwo,value[id0],....
   */
  public String lineToString(List<String> ids, String nameColumnOne, String nameColumnTwo, NumberFormat fmt) {
    final StringJoiner valueJoiner = new StringJoiner(AocExecWriterHelper.DELIMITER);
    if (nameColumnOne != null) {
      valueJoiner.add(nameColumnOne);
    }
    if (nameColumnTwo != null) {
      valueJoiner.add(nameColumnTwo);
    }
    ids.forEach(id -> valueJoiner.add(AocExecFormatterHelper.formatValue(fmt, get(id))));
    return valueJoiner.toString();
  }

  public boolean isEmpty() {
    return valueById.isEmpty();
  }
}
