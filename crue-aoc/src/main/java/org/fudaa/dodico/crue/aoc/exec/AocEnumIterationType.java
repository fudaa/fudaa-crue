package org.fudaa.dodico.crue.aoc.exec;

/**
 * @author deniger
 */
public enum AocEnumIterationType {

  INITIALISATION,
  NORMAL,
  STRICKLER_TRONQUE,
  SENSIBILITE_MIN,
  SENSIBILITE_MAX
}
