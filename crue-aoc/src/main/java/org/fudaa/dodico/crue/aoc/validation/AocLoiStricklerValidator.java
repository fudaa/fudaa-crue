package org.fudaa.dodico.crue.aoc.validation;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluDefaultLogFormatter;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculPermanent;
import org.fudaa.dodico.crue.aoc.projet.AocLoiStrickler;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.validation.ValidatingItem;

import java.util.*;

/**
 * Classe permettant de valider une liste de  AocLoiCalculPermanent
 *
 * @author deniger
 */
public class AocLoiStricklerValidator {
    /**
     * @param nodes                list des {@link AocLoiCalculPermanent}
     * @param availableFrottements les frottements disponibles dans le scenario
     */
    public CtuluLog validate(List<ValidatingItem<AocLoiStrickler>> nodes, String[] availableFrottements) {
        return validate(nodes, Arrays.asList(availableFrottements));
    }

    /**
     * @param nodes                list des {@link AocLoiCalculPermanent}
     * @param availableFrottements les frottements disponibles dans le scenario
     */
    public CtuluLog validate(List<ValidatingItem<AocLoiStrickler>> nodes, List<String> availableFrottements) {
        Set<String> usedFrottements = new HashSet<>();
        CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
        res.setDesc("aoc.validation.loiStickler");
        if (nodes.isEmpty()) {
            return res;
        }
        int idx = 0;
        Set<String> lois = AocLoiCalculPermanentValidator.createHashSet(availableFrottements);
        for (ValidatingItem<AocLoiStrickler> node : nodes) {
            idx++;
            List<CtuluLogRecord> errorMsg = new ArrayList<>();

            //Validation du nom de calcul
            String loiRef = node.getObjectToValidate().getLoiRef();
            if (StringUtils.isBlank(loiRef)) {
                errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiStickler.loiBlank", idx));
            } else {
                if (usedFrottements.contains(loiRef.toUpperCase())) {
                    errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiStickler.frottementAlreadyUsed", idx, loiRef));
                }
                if (!lois.contains(loiRef)) {
                    errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiStickler.frottementNotFound", idx, loiRef));
                }
                if (CruePrefix.prefixMustBeAdded(CruePrefix.P_FROTT_STRICKLER, loiRef)
                        && CruePrefix.prefixMustBeAdded(CruePrefix.P_FROTT_STRICKLER_STO, loiRef)
                        && CruePrefix.prefixMustBeAdded(CruePrefix.P_FROTT_MANNING, loiRef)
                        ) {
                    errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiStickler.frottementInvalid", idx, loiRef));
                }
                usedFrottements.add(loiRef.toUpperCase());
            }

            //min,ini,max
            if (node.getObjectToValidate().getMin() < 0) {
                errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiStickler.minInvalid", idx));
            }
            if (node.getObjectToValidate().getIni() < 0) {
                errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiStickler.iniInvalid", idx));
            }
            if (node.getObjectToValidate().getMax() < 0) {
                errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiStickler.maxInvalid", idx));
            }

            if (node.getObjectToValidate().getMin() > node.getObjectToValidate().getMax()) {
                errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiStickler.minGreaterThanMax", idx));
            }

            if (node.getObjectToValidate().getIni() > node.getObjectToValidate().getMax() || node.getObjectToValidate().getIni() < node.getObjectToValidate()
                    .getMin()) {
                errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiStickler.iniNotInMinMax", idx));
            }

            if (!errorMsg.isEmpty()) {
                node.setErrorMessage(CtuluDefaultLogFormatter.asHtml(errorMsg, BusinessMessages.RESOURCE_BUNDLE));
            } else {
                node.clearMessage();
            }
        }
        res.updateLocalizedMessage(BusinessMessages.RESOURCE_BUNDLE);
        return res;
    }
}
