package org.fudaa.dodico.crue.aoc.validation;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluDefaultLogFormatter;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculPermanent;
import org.fudaa.dodico.crue.aoc.projet.AocParamCalc;
import org.fudaa.dodico.crue.aoc.projet.AocTempoCCM;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPermItem;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.validation.ValidatingItem;

import java.util.*;

/**
 * Classe permettant de valider une liste de  AocLoiCalculPermanent
 *
 * @author deniger
 */
public class AocParamCalcValidator {
    /**
     * @param nodes      list des {@link AocLoiCalculPermanent}
     * @param dclmByCalc les dclm utilisable
     */
    public CtuluLog validate(List<ValidatingItem<AocParamCalc>> nodes, Map<String, List<CalcPseudoPermItem>> dclmByCalc) {
        CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
        res.setDesc("aoc.validation.paramCalc");
        if (nodes.isEmpty()) {
            return res;
        }
        Map<String, Set<String>> availableEMHsByCalc = new HashMap<>();
        Map<String, Set<String>> definedEMHsByCalc = new HashMap<>();
        for (Map.Entry<String, List<CalcPseudoPermItem>> entry : dclmByCalc.entrySet()) {
            Set<String> emhs = new HashSet<>();
            availableEMHsByCalc.put(entry.getKey(), emhs);
            definedEMHsByCalc.put(entry.getKey(), new HashSet<>());
            for (DonCLimM donCLimM : entry.getValue()) {
                emhs.add(donCLimM.getEmh().getNom());
            }
        }
        for (ValidatingItem<AocParamCalc> node : nodes) {
            String identifiant = node.getObjectToValidate().getParamIdentifiant();
            List<CtuluLogRecord> errorMsg = new ArrayList<>();

            //Validation du nom de calcul
            String calculRef = node.getObjectToValidate().getCalculRef();
            if (StringUtils.isBlank(calculRef)) {
                errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.paramCalc.calculBlank", identifiant));
            } else {
                if (!availableEMHsByCalc.containsKey(calculRef)) {
                    errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.paramCalc.calculNotFound", identifiant, calculRef));
                }
                if (CruePrefix.prefixMustBeAdded(CruePrefix.P_CALCUL_PSEUDOPERMANENT, calculRef)) {
                    errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.paramCalc.calculInvalid", identifiant, calculRef));
                }
            }

            //Validation de l emh
            String emhRef = node.getObjectToValidate().getEmhRef();
            Set<String> availableEMHs = availableEMHsByCalc.get(calculRef);
            if (StringUtils.isBlank(emhRef)) {
                errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.paramCalc.emhBlank", identifiant));
            } else if (availableEMHs == null || !availableEMHs.contains(emhRef)) {
                errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.paramCalc.emhNotFound", identifiant, emhRef));
            }
            if (StringUtils.isNotBlank(calculRef) && StringUtils.isNotBlank(emhRef)) {
                Set<String> definedEMHs = definedEMHsByCalc.get(calculRef);
                if (definedEMHs != null) {
                    if (definedEMHs.contains(emhRef)) {
                        errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.paramCalc.calculDclmAlreadyUsed", identifiant, emhRef));
                    }
                    definedEMHs.add(emhRef);
                }
            }
            ItemVariable itemVariable = AocTempoCCM.getDeltaQ();
            final String errorForValue = itemVariable.getValidator().getValidateErrorForValue(node.getObjectToValidate().getDeltaQ());
            if (errorForValue != null) {
                errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, identifiant + ": " + errorForValue));
            } else {
                final String warningForValue = itemVariable.getValidator().getNormaliteWarningForValue(node.getObjectToValidate().getDeltaQ());
                if (warningForValue != null) {
                    errorMsg.add(res.addRecord(CtuluLogLevel.WARNING, identifiant + ": " + warningForValue));
                }
            }
            if (!errorMsg.isEmpty()) {
                if (isAllWarning(errorMsg)) {
                    node.setWarningMessage(CtuluDefaultLogFormatter.asHtml(errorMsg, BusinessMessages.RESOURCE_BUNDLE));
                } else {
                    node.setErrorMessage(CtuluDefaultLogFormatter.asHtml(errorMsg, BusinessMessages.RESOURCE_BUNDLE));
                }
            } else {
                node.clearMessage();
            }
        }
        res.updateLocalizedMessage(BusinessMessages.RESOURCE_BUNDLE);
        return res;
    }

    private boolean isAllWarning(List<CtuluLogRecord> errorMsg) {
        for (CtuluLogRecord ctuluLogRecord : errorMsg) {
            if (ctuluLogRecord.getLevel() != CtuluLogLevel.WARNING) {
                return false;
            }
        }
        return true;
    }
}
