package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.projet.AbstractLoiCalcul;

/**
 * Contient les données de calcul des criteres
 */
class AocExecCritereData {
  private final AocExecResultContainer resultContainer;
  private final AocExecAllLogger logger;
  private final CtuluLog log;
  private int sumPonderation = 0;
  private double sumCritere = 0;
  private int sumPonderationValidation = 0;
  private double sumCritereValidation = 0;
  private int sumPonderationApprentissage = 0;
  private double sumCritereApprentissage = 0;

  /**
   * @param result le resultat
   * @param logger le logger est un logger technique pour produit les fichiers .log
   * @param logOperation contient les warning/erreur d'execution.
   */
  public AocExecCritereData(AocExecResultContainer result, AocExecAllLogger logger, CtuluLog logOperation) {
    this.resultContainer = result;
    this.logger = logger;
    this.log = logOperation;
  }

  public CtuluLog getLog() {
    return log;
  }

  /**
   * @return le logger
   */
  public AocExecAllLogger getLogger() {
    return logger;
  }

  /**
   * @param critere le critere a ajouter en suivant la ponderation demande
   * @param calcul le calcul utilisé pour ventiler apprentissage/validation en particulier
   */
  public void addCalculCritere(double critere, AbstractLoiCalcul calcul) {
    sumCritere = sumCritere + critere * calcul.getPonderation();
    sumPonderation = sumPonderation + calcul.getPonderation();

    if (resultContainer.isApprentissage(calcul.getCalculRef())) {
      sumCritereApprentissage = sumCritereApprentissage + critere * calcul.getPonderation();
      sumPonderationApprentissage = sumPonderationApprentissage + calcul.getPonderation();
      logger.addLogFormated(";;Calcul pris en compte dans Apprentissage;" + calcul.getCalculRef());
    } else if (resultContainer.isValidation(calcul.getCalculRef())) {
      sumCritereValidation = sumCritereValidation + critere * calcul.getPonderation();
      sumPonderationValidation = sumPonderationValidation + calcul.getPonderation();
      logger.addLogFormated(";;Calcul pris en compte dans Validation;" + calcul.getCalculRef());
    }
  }

  public double getCritere() {
    return sumCritere / ((double) sumPonderation);
  }

  public double getCritereValidation() {
    return sumCritereValidation / ((double) sumPonderationValidation);
  }

  public double getCritereApprentissage() {
    return sumCritereApprentissage / ((double) sumPonderationApprentissage);
  }
}
