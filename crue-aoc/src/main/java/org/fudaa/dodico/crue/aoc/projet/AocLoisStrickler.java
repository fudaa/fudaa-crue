package org.fudaa.dodico.crue.aoc.projet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deniger on 28/06/2017.
 */
public class AocLoisStrickler {
    List<AocLoiStrickler> lois = new ArrayList<>();

    public List<AocLoiStrickler> getLois() {
        return lois;
    }

    public void setLois(List<AocLoiStrickler> lois) {
        this.lois = lois;
    }

    public void addStrickler(String loiRef, int min, int ini, int max) {
        lois.add(new AocLoiStrickler(loiRef, min, ini, max));
    }

    public void setClonedLois(List<AocLoiStrickler> aocLoiStricklers) {
        this.lois = new ArrayList<>();
        if (lois != null) {
            for (AocLoiStrickler aocLoiCalculTransitoire : aocLoiStricklers) {
                this.lois.add(aocLoiCalculTransitoire.clone());
            }
        }
    }
}
