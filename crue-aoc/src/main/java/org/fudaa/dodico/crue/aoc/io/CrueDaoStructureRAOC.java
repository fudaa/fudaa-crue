package org.fudaa.dodico.crue.aoc.io;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureLog;
import org.fudaa.dodico.crue.io.common.CrueDataDaoStructure;

/**
 * @author CANEL Christophe
 */
public class CrueDaoStructureRAOC implements CrueDataDaoStructure {

  /**
   * {@inheritDoc}
   */
  @Override
  public void configureXStream(final XStream xstream, final CtuluLog ctuluLog, final org.fudaa.dodico.crue.config.loi.LoiTypeContainer props) {
    xstream.alias("AOC-Resultats", CrueDaoRAOC.class);
    new CrueDaoStructureLog().configureXStream(xstream, ctuluLog, props);
  }

}
