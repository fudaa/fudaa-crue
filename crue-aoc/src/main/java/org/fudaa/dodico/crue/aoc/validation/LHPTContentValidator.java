package org.fudaa.dodico.crue.aoc.validation;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.PropertyValidator;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.aoc.LoiMesuresPost;
import org.fudaa.dodico.crue.metier.aoc.ParametrageProfilSection;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.validation.ValidatingItem;
import org.fudaa.dodico.crue.validation.ValidationHelperLoi;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;

import java.util.*;

/**
 * Validation des données LHPT.
 *
 * @author fred deniger
 */
public class LHPTContentValidator {
  /**
   * @param loiMesurePost les données loi Mesure post
   * @param availableSections les sections du scenario attachés
   * @param scenarioName le nom du scenario
   * @param ccm Le crueConfigMetier associé
   * @return résultat de validation
   */
  public CtuluLogGroup valid(LoiMesuresPost loiMesurePost, Set<String> availableSections, String scenarioName, CrueConfigMetier ccm) {
    CtuluLogGroup res = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    res.setDescription("aoc.validation.lhpt");
    res.setDescriptionArgs(scenarioName);
    if (loiMesurePost == null || loiMesurePost.getEchellesSections() == null) {
      return res;
    }

    //echelles <=> sections
    validProfilSection(loiMesurePost, availableSections, res);

    CtuluLogGroup loiGroup = res.createGroup("aoc.validation.loiMesure");
    final List<AbstractLoi> lois = validLoiNamesAreUnique(loiMesurePost, loiGroup);
    validatLois(loiMesurePost.getEchellesSections().getDefinedEchelles(), ccm, loiGroup, lois);

    return res.createCleanGroup();
  }

  private void validatLois(Set<String> availableSections, CrueConfigMetier ccm, CtuluLogGroup loiGroup, List<AbstractLoi> lois) {
    final PropertyValidator loiOrdonneeValidatorForLoiTF = ccm.getLoiOrdonneeValidator(EnumTypeLoi.LoiSectionsZ);
    for (AbstractLoi abstractLoi : lois) {
      CtuluLog log = validateLoi(availableSections, ccm, loiOrdonneeValidatorForLoiTF, abstractLoi);
      if (log.isNotEmpty()) {
        log.setDesc(abstractLoi.getNom());
        loiGroup.addLog(log);
      }
    }
  }

  /**
   * Valide une loi.
   *
   * @param loiMesurePost
   * @param ccm
   * @param loi
   */
  public CtuluLog validatLoi(LoiMesuresPost loiMesurePost, CrueConfigMetier ccm, AbstractLoi loi) {
    final PropertyValidator loiOrdonneeValidatorForLoiTF = ccm.getLoiOrdonneeValidator(EnumTypeLoi.LoiSectionsZ);
    return validateLoi(loiMesurePost.getEchellesSections().getDefinedEchelles(), ccm, loiOrdonneeValidatorForLoiTF, loi);
  }

  private CtuluLog validateLoi(Set<String> availableProfils, CrueConfigMetier ccm, PropertyValidator loiOrdonneeValidatorForLoiTF, AbstractLoi abstractLoi) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc(abstractLoi.getNom());
    if (abstractLoi instanceof Loi) {
      log = ValidationHelperLoi.valideLoi((Loi) abstractLoi, ccm);
    } else {
      if (abstractLoi instanceof LoiTF) {
        List<PtEvolutionTF> loiTF = ((LoiTF) abstractLoi).getEvolutionTF().getPtEvolutionTF();
        int line = 0;
        Set<String> definedProfil = new HashSet<>();
        for (PtEvolutionTF ptEvolutionTF : loiTF) {
          line++;
          //erreur lors de la sauvegarde ?
          if (ptEvolutionTF == null) {
            log.addError("aoc.validation.loiMesure.profilNull", Integer.toString(line));
          } else {
            final String profilName = ptEvolutionTF.getAbscisse();
            if (StringUtils.isBlank(profilName)) {
              log.addError("aoc.validation.loiMesure.profilBlank", Integer.toString(line));
            } else if (!availableProfils.contains(profilName)) {
              log.addError("aoc.validation.loiMesure.profilUsedInLoiAndNotFound", Integer.toString(line), profilName);
            } else {
              if (definedProfil.contains(profilName)) {
                log.addError("aoc.validation.loiMesure.profilUsedSeveralTime", Integer.toString(line), profilName);
              }
              definedProfil.add(profilName);
            }
            loiOrdonneeValidatorForLoiTF.validateNumber(profilName, ptEvolutionTF.getOrdonnee(), log);
          }
        }
      } else {
        log.addError("aoc.validation.loiMesure.loiUnknown", abstractLoi.getNom());
      }
    }
    String nameError = ValidationPatternHelper.isNameValidei18nMessage(abstractLoi.getNom(), abstractLoi.getType());
    if (nameError != null) {
      log.addError(nameError);
    }
    if (abstractLoi.getType().isDateAbscisse() && !(abstractLoi instanceof LoiDF)) {
      log.addError("dateLoi.notInCorrectFormat", abstractLoi.getNom());
    }
    return log;
  }

  private List<AbstractLoi> validLoiNamesAreUnique(LoiMesuresPost loiMesurePost, CtuluLogGroup loiGroup) {
    final CtuluLog logLois = loiGroup.createNewLog("aoc.validation.loiMesure.loiAreUnique");
    //les lois
    TreeSet<String> loiNames = new TreeSet<>();
    final List<AbstractLoi> lois = loiMesurePost.getLois();
    for (AbstractLoi abstractLoi : lois) {
      if (loiNames.contains(abstractLoi.getNom())) {
        logLois.addError("aoc.validation.loiMesure.loiDoubon", abstractLoi.getNom());
      }
      loiNames.add(abstractLoi.getNom());
    }
    return lois;
  }

  private void validProfilSection(LoiMesuresPost loiMesurePost, Set<String> availableSections, CtuluLogGroup res) {
    final List<ParametrageProfilSection> echellesSections = loiMesurePost.getEchellesSections().getEchellesSectionList();
    final List<ValidatingItem<ParametrageProfilSection>> echellesSectionsValidation = new ArrayList<>();
    for (ParametrageProfilSection echellesSection : echellesSections) {
      echellesSectionsValidation.add(new ValidatingItem<>(echellesSection, null));
    }
    final CtuluLog logProfilSection = new AocProfilSectionValidator().validate(echellesSectionsValidation, availableSections);
    res.addLog(logProfilSection);
  }
}
