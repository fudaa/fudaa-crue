package org.fudaa.dodico.crue.aoc.exec.writer;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmDataItem;
import org.fudaa.dodico.crue.aoc.exec.AocExecContainer;
import org.fudaa.dodico.crue.aoc.exec.AocExecResult;
import org.fudaa.dodico.crue.aoc.exec.AocExecResultContainer;
import org.fudaa.dodico.crue.aoc.exec.AocExecResultIteration;
import org.fudaa.dodico.crue.aoc.helper.AocValidationCroiseeLine;
import org.fudaa.dodico.crue.aoc.helper.AocValidationCroiseeLineBuilder;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageAlgorithme;

import java.io.File;
import java.text.NumberFormat;
import java.util.*;

/**
 * @author deniger
 */
public class AocExecResultWriter {
  private static final String FILE_PREFIX = "resultats_";

  /**
   * Ecrit tous les fichiers
   *
   * @param result le resultats
   * @return le log
   */
  public CtuluLog write(AocExecContainer result) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("aoc.exec.write.result");
    writeTrajectoire(result, log);
    final AocExecResultContainer resultContainer = result.getResultContainer();
    if (!resultContainer.isStoppedDueToCrue10Error() && resultContainer.getIterationWithOptimumStricklerAsInteger() != null) {
      final File stricklersFile = getFile(result, FILE_PREFIX, "_stricklers");
      AocExecWriterHelper
          .writeDfrtResults(false, result, resultContainer.getIterationWithOptimumStricklerAsInteger(), resultContainer.getIterationWithMinCritere(), log, stricklersFile);

      AocExecWriterHelper.writeDclmResults(false, result, resultContainer.getIterationWithMinCritere(), log, getFile(result, FILE_PREFIX, "_debits_optimum"));
      writeBilan(result, log);
    }
    AocExecWriterHelper.writeCrue10Failures(result, log, getFile(result, FILE_PREFIX, "_crue10_failure"));

    return log;
  }

  /**
   * Ecrit le bilan du calage dans le fichier resultat_XXX.csv
   *
   * @param aocExecContainer les resultats
   * @param log les logs
   */
  private void writeBilan(AocExecContainer aocExecContainer, CtuluLog log) {
    AocExecResultContainer resultContainer = aocExecContainer.getResultContainer();
    List<String> lines = AocExecWriterHelper.createBilanHeader(aocExecContainer);
    lines.add("");
    lines.add("#Valeur du critere avec scenario initial non modifie");
    final AocExecResultIteration initialIteration = resultContainer.getResultIterations().get(0);
    lines.add("Valeur critere global initial;" + initialIteration.getCritereTotal());
    if (initialIteration.getValidationCroiseeLine() != null) {
      lines.add("Valeur critere validation initial;" + initialIteration.getCritereValidation());
      lines.add("Valeur critere apprentissage initial;" + initialIteration.getCritereApprentissage());
    }

    lines.add("");
    lines.add("#Valeur des criteres Optimum/Troncature");
    final AocExecResultIteration iterationWithMinRMSE = resultContainer.getIterationWithMinCritere();
    lines.add("Valeur critere global Optimal;" + iterationWithMinRMSE.getCritereTotal());
    if (iterationWithMinRMSE.getValidationCroiseeLine() != null) {
      lines.add("Valeur critere validation Optimal;" + iterationWithMinRMSE.getCritereValidation());
      lines.add("Valeur critere apprentissage Optimal;" + iterationWithMinRMSE.getCritereApprentissage());
    }
    lines.add("");
    final AocExecResultIteration iterationWithOptimumStricklerAsInteger = resultContainer.getIterationWithOptimumStricklerAsInteger();
    lines.add("Valeur critere global Troncature;" + iterationWithOptimumStricklerAsInteger.getCritereTotal());
    if (iterationWithOptimumStricklerAsInteger.getValidationCroiseeLine() != null) {
      lines.add("Valeur critere validation Troncature;" + iterationWithOptimumStricklerAsInteger.getCritereValidation());
      lines.add("Valeur critere apprentissage Troncature;" + iterationWithOptimumStricklerAsInteger.getCritereApprentissage());
    }
    lines.add("");
    //on affiche les validations
    final AocValidationCroiseeLine validationCroiseeLine = resultContainer.getValidationCroiseeLine();
    if (validationCroiseeLine != null) {
      lines.add("#Validation Croisée");
      lines.add("Lignes eau apprentissage;" + String.join(AocExecWriterHelper.DELIMITER, validationCroiseeLine.getLigneEauApprentissage()));
      lines.add("Lignes eau validation;" + String.join(AocExecWriterHelper.DELIMITER, validationCroiseeLine.getLigneEauValidation()));
      final List<String> allLigneEau = AocValidationCroiseeLineBuilder.getAllLigneEau(aocExecContainer.getCampagne().getDonnees().getValidationCroisee());
      Collections.sort(allLigneEau);
      lines.add("");
      lines.add("#Validation Croisée: valeurs par iteration");
      lines.add("Iteration;Critere Apprentissage;Critere Validation;Critere Total;" + String.join(AocExecWriterHelper.DELIMITER, allLigneEau));
      resultContainer.getResultIterations().forEach(result -> addValidationCroiseeLine(result, lines, allLigneEau));
      lines.add("");
      lines.add("#Validation Croisée: valeurs par ligne eau");
      lines.add("Ligne eau;Critere initial;Critere Optimum;Critere Troncature");
      allLigneEau.forEach(ligneEau -> addLigneEauValidationCroisee(ligneEau, initialIteration.getResultsByLoi(), iterationWithMinRMSE.getResultsByLoi(),
          iterationWithOptimumStricklerAsInteger.getResultsByLoi(), lines));

      lines.add("");
    } else {
      final List<AocExecResult> resultsByCalculTroncature = iterationWithOptimumStricklerAsInteger.getAllResults();
      final List<AocExecResult> resultsByCalculOptimum = iterationWithMinRMSE.getAllResults();
      if (aocExecContainer.getCampagne().isPermanent()) {
        lines.add("#Valeur des criteres individuels pour chaque ligne d'eau pour Troncature");
        resultsByCalculTroncature
            .forEach(line -> lines.add(line.getNomCalcul() + AocExecWriterHelper.DELIMITER + line.getNomLoi() + AocExecWriterHelper.DELIMITER + line.getCritere()));

        lines.add("");
        lines.add("#Valeur des criteres individuels pour chaque ligne d'eau pour Optimum");
        resultsByCalculOptimum
            .forEach(line -> lines.add(line.getNomCalcul() + AocExecWriterHelper.DELIMITER + line.getNomLoi() + AocExecWriterHelper.DELIMITER + line.getCritere()));
      } else {
        final String linePrefix = "#Valeur des criteres individuels \'" + aocExecContainer.getCampagne().getTypeCalageCritere()
            .geti18nLongName() + "\' pour les courbes de type " + aocExecContainer
            .getCampagne()
            .getTypeCalageDonnees().getCourbeName();
        lines.add(linePrefix + " pour Troncature");
        lines.add("Calcul;Loi;Section;" + aocExecContainer.getCampagne().getTypeCalageCritere().geti18nLongName());
        resultsByCalculTroncature.forEach(line -> lines.add(
            line.getNomCalcul() + AocExecWriterHelper.DELIMITER + line.getNomLoi() + AocExecWriterHelper.DELIMITER + line.getSection() + AocExecWriterHelper.DELIMITER + line
                .getCritere()));

        lines.add("");
        lines.add(linePrefix + " pour Optimum");
        lines.add("Calcul;Loi;Section;" + aocExecContainer.getCampagne().getTypeCalageCritere().geti18nLongName());
        resultsByCalculOptimum.forEach(line -> lines.add(
            line.getNomCalcul() + AocExecWriterHelper.DELIMITER + line.getNomLoi() + AocExecWriterHelper.DELIMITER + line.getSection() + AocExecWriterHelper.DELIMITER + line
                .getCritere()));
      }
    }
    final NumberFormat dfrtFormater = AocExecFormatterHelper.getDfrtFormater(aocExecContainer);
    lines.add("");
    lines.add("#Valeurs des Stricklers tronqués (Troncature)");
    addFrottementValuesToLines(lines, iterationWithOptimumStricklerAsInteger, dfrtFormater);

    //on affiche les valeurs reelles:
    lines.add("");
    lines.add("#Valeurs des Stricklers optimums (Optimum)");
    addFrottementValuesToLines(lines, iterationWithMinRMSE, dfrtFormater);

    if (initialIteration.getDclmValues() != null) {
      lines.add("");
      lines.add("#Valeurs des débits initiaux");
      addDebitValuesToLines(aocExecContainer, lines, initialIteration);
    }

    if (iterationWithMinRMSE.getDclmValues() != null) {
      lines.add("");
      lines.add("#Valeurs des débits Optimum");
      addDebitValuesToLines(aocExecContainer, lines, iterationWithMinRMSE);
    }
    //resultats aux points de controles:
    lines.add("");
    if (aocExecContainer.getCampagne().isPermanent()) {
      lines.add("#Hauteur/ecart aux echelles pour Troncature");
      AocExecWriterHelper.writeErrorControlPointsPermanent(iterationWithOptimumStricklerAsInteger, aocExecContainer, lines);
      lines.add("");
      lines.add("#Hauteur/ecart aux echelles pour Optimum");
      AocExecWriterHelper.writeErrorControlPointsPermanent(iterationWithMinRMSE, aocExecContainer, lines);
    } else {
      final String linePrefix = "#Valeur/ecart pour " + aocExecContainer.getCampagne().getTypeCalageDonnees().getCourbeName();
      lines.add(linePrefix + " pour Troncature");
      AocExecWriterHelper.writeControlPointsTransitoire(iterationWithOptimumStricklerAsInteger, aocExecContainer, lines, "");
      lines.add("");
      lines.add(linePrefix + " pour Optimum");
      AocExecWriterHelper.writeControlPointsTransitoire(iterationWithMinRMSE, aocExecContainer, lines, "");
    }

    //the aocExecContainer line to write:
    //resultat_montecarlo.csv
    File resultFile = getFile(aocExecContainer, FILE_PREFIX, "_bilan");
    AocExecWriterHelper.writeToFiles(log, lines, resultFile);
  }

  private void addLigneEauValidationCroisee(String ligneEau, Map<String, AocExecResult> initialIteration, Map<String, AocExecResult> iterationWithMinRMSE,
                                            Map<String, AocExecResult> iterationWithOptimumStricklerAsInteger, List<String> lines) {
    lines.add(ligneEau
        + AocExecWriterHelper.DELIMITER + AocExecWriterHelper.getLoiValue(initialIteration, ligneEau)
        + AocExecWriterHelper.DELIMITER + AocExecWriterHelper.getLoiValue(iterationWithMinRMSE, ligneEau)
        + AocExecWriterHelper.DELIMITER + AocExecWriterHelper.getLoiValue(iterationWithOptimumStricklerAsInteger, ligneEau)
    );
  }

  private void addValidationCroiseeLine(AocExecResultIteration resultat, List<String> lines, List<String> allLigneEau) {
    final Map<String, AocExecResult> resultsByLoi = resultat.getResultsByLoi();

    final StringJoiner joinerLineValues = new StringJoiner(AocExecWriterHelper.DELIMITER);
    joinerLineValues.add(Integer.toString(1 + resultat.getIndex()));
    joinerLineValues.add(Double.toString(resultat.getCritereApprentissage()));
    joinerLineValues.add(Double.toString(resultat.getCritereValidation()));
    joinerLineValues.add(Double.toString(resultat.getCritereTotal()));
    allLigneEau.forEach(loi -> joinerLineValues.add(resultsByLoi.get(loi) == null ? StringUtils.EMPTY : Double.toString(resultsByLoi.get(loi).getCritere())));

    lines.add(joinerLineValues.toString());
  }

  public void addDebitValuesToLines(AocExecContainer result, List<String> lines, AocExecResultIteration iterationWithMinRMSE) {
    final List<AocDclmDataItem> dclmValues = iterationWithMinRMSE.getDclmValues();
    AocExecWriterHelper.addDebitValueToLines(lines, dclmValues, AocExecFormatterHelper.getQFormater(result));
  }

  public void addFrottementValuesToLines(List<String> lines, AocExecResultIteration iterationWithStrickerAsInteger, NumberFormat dfrtFormater) {
    final Map<String, Double> dfrtValues = iterationWithStrickerAsInteger.getDfrtValues();
    AocExecWriterHelper.addFrottementValuesToLines(lines, dfrtFormater, dfrtValues);
  }

  public File getFile(AocExecContainer aocExecContainer, String prefix, String suffix) {

    String fileName = StringUtils.defaultString(prefix)
        + aocExecContainer.getCampagne().getCalage().getAlgorithme().getIdentifiant().toLowerCase()
        + StringUtils.defaultString(suffix)
        + aocExecContainer.getResultContainer().getSuffixToAddForFile() + ".csv";
    return new File(aocExecContainer.getReportDirectory(), fileName);
  }

  /**
   * ecrit les valeurs des criteres au cours des iterations dans le fichier trajectoire_XXXX.csv
   *
   * @param aocExecContainer les resultats
   * @param log les logs
   */
  public void writeTrajectoire(AocExecContainer aocExecContainer, CtuluLog log) {
    AocExecResultContainer resultContainer = aocExecContainer.getResultContainer();
    if (EnumAocCalageAlgorithme.MONTE_CARLO.equals(aocExecContainer.getCampagne().getCalage().getAlgorithme())) {
      writeTajectoireMonteCarlo(aocExecContainer, resultContainer, log);
    } else {
      writeTajectoireRecuit(aocExecContainer, resultContainer, log);
    }
  }

  public void writeTajectoireRecuit(AocExecContainer aocExecContainer, AocExecResultContainer resultContainer, CtuluLog log) {
    final List<AocExecResultIteration> resultIterations = resultContainer.getResultIterations();
    List<String> lines = new ArrayList<>();

    String critereName = AocExecFormatterHelper.getCritereName(aocExecContainer);
    lines.add("Iteration;Type;Temperature;Amplitude;" + critereName + " Initial;" + critereName + " Courant;" + critereName + " Min");
    String numberOfIteration = "/" + resultIterations.size();
    resultIterations.forEach(line -> lines.add(
        "iteration: " + (line.getIndex() + 1) + numberOfIteration + (line.getIndex() == line.getCritereMinIteration() ? "*" : "")
            + AocExecWriterHelper.DELIMITER + line.getIterationType()
            + AocExecWriterHelper.DELIMITER + (line.isNormal() ? Double.toString(line.getTemperature()) : "")
            + AocExecWriterHelper.DELIMITER + (line.isNormal() ? Double.toString(line.getMaxAmplitudeFrottement()) : "")
            + AocExecWriterHelper.DELIMITER + (line.isNormal() ? Double.toString(line.getDernierCritere()) : "")
            + AocExecWriterHelper.DELIMITER + line.getCritere()
            + AocExecWriterHelper.DELIMITER + line.getCritereLocalMin())
    );
    File trajectoire = getFile(aocExecContainer, "trajectoire_", null);
    AocExecWriterHelper.writeToFiles(log, lines, trajectoire);
  }

  public void writeTajectoireMonteCarlo(AocExecContainer aocExecContainer, AocExecResultContainer resultContainer, CtuluLog log) {
    final List<AocExecResultIteration> resultIterations = resultContainer.getResultIterations();
    List<String> lines = new ArrayList<>();
    String critereName = AocExecFormatterHelper.getCritereName(aocExecContainer);
    lines.add("Iteration;Type;" + critereName + " Courant;" + critereName + " Min");
    String numberOfIteration = "/" + resultIterations.size();
    resultIterations.forEach(line -> lines.add(
        "iteration: " + (line.getIndex() + 1) + numberOfIteration + (line.getIndex() == line.getCritereMinIteration() ? "*" : "")
            + AocExecWriterHelper.DELIMITER + line.getIterationType()
            + AocExecWriterHelper.DELIMITER + line.getCritere()
            + AocExecWriterHelper.DELIMITER + line.getCritereLocalMin()
    ));
    File trajectoire = getFile(aocExecContainer, "trajectoire_", null);
    AocExecWriterHelper.writeToFiles(log, lines, trajectoire);
  }
}
