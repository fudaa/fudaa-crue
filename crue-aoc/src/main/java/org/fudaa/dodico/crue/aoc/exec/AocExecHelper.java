package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmContainer;
import org.fudaa.dodico.crue.aoc.projet.*;
import org.fudaa.dodico.crue.aoc.validation.AocValidationHelper;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageType;
import org.fudaa.dodico.crue.metier.aoc.EnumAocTypeCalageDonnees;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;

import java.util.*;

/**
 * @author deniger
 */
public class AocExecHelper {
  private AocExecHelper() {

  }

  /**
   * @param campagne la campagne
   * @return le nombre d'itération à effectuer en fonction du {@link EnumAocCalageType} choisi
   */
  public static int getNbIterations(AocCampagne campagne) {
    int nbIteration = campagne.getCalage().getNombreIterationSeul();
    if (isTestRepetabilite(campagne)) {
      nbIteration = campagne.getCalage().getNombreIterationTir();
    }
    if (isValidationCroisee(campagne)) {
      nbIteration = campagne.getCalage().getNombreIterationValidationCroisee();
    }
    return nbIteration;
  }

  /**
   * @param campagne la campagne
   * @return le nombre d'itération à effectuer en fonction du {@link EnumAocCalageType} choisi
   */
  public static int getNbIterationsTotal(AocCampagne campagne) {
    //il y a juste 2 iterations avec le min et le max.
    if (isAnaylseSensibilite(campagne)) {
      return 2;
    }
    return getNbIterations(campagne) + 2;
  }

  /**
   * @param builder le builder
   * @param campagne la campagne non null
   * @return true si permanent et si des valeurs dclm sont presentes
   */
  public static boolean debitCouldBeModified(AocDclmContainer builder, AocCampagne campagne) {
    return EnumAocTypeCalageDonnees.PERMANENT.equals(campagne.getTypeCalageDonnees()) && builder.isNotEmpty() && builder.hasDeltaQ();
  }

  public static boolean isTestRepetabilite(AocCampagne campagne) {
    return EnumAocCalageType.TEST_REPETABILITE.equals(campagne.getCalage().getOperation());
  }

  public static boolean isAnaylseSensibilite(AocCampagne campagne) {
    return EnumAocCalageType.ANALYSE_SENSIBILITE.equals(campagne.getCalage().getOperation());
  }

  public static boolean isValidationCroisee(AocCampagne campagne) {
    return EnumAocCalageType.VALIDATION_CROISEE.equals(campagne.getCalage().getOperation());
  }

  /**
   * @param campagne la campagne
   * @return le nombre de tir à effectuer en fonction du {@link EnumAocCalageType} choisi. Pour {@link EnumAocCalageType#SEUL} sera 1.
   */
  public static int getNbTirs(AocCampagne campagne) {
    int nbIteration = 1;
    if (isTestRepetabilite(campagne)) {
      nbIteration = campagne.getCalage().getNombreTir();
    }
    return nbIteration;
  }

  public static AocExecResultIteration getWithType(List<AocExecResultIteration> resultIterations, AocEnumIterationType type) {
    final Optional<AocExecResultIteration> first = resultIterations.stream().filter(e -> type.equals(e.getIterationType())).findFirst();
    return first.orElse(null);
  }

  /**
   * @param aocExecContainer le conteneur
   * @return les AocLoiCalculTransitoire utilisable pour le calcul des critère
   */
  public static List<AocLoiCalculTransitoire> extractLoiCalculTransitoire(AocExecContainer aocExecContainer, Map<String, AbstractLoi> loiByName) {
    List<AocLoiCalculTransitoire> calculsTransitoire = new ArrayList<>();
    final boolean useLoiTZ = AocValidationHelper.useLoiTZForTransitoire(aocExecContainer.getCampagne());
    aocExecContainer.getCampagne().getDonnees().getLoisCalculsTransitoires().getLois().forEach(loiCalculTransitoire -> {
      AbstractLoi loi = loiByName.get(loiCalculTransitoire.getLoiRef());
      //si on doit utiliser les loiTZ et c'est une loiTZ
      if (useLoiTZ && loi.getType().equals(EnumTypeLoi.LoiTZ)) {
        calculsTransitoire.add(loiCalculTransitoire);
      }
      //si on ne doit pas utiliser les loiTZ et c'est une loiTQ
      if (!useLoiTZ && loi.getType().equals(EnumTypeLoi.LoiTQ)) {
        calculsTransitoire.add(loiCalculTransitoire);
      }
    });
    return calculsTransitoire;
  }

  /**
   * @param aocExecContainer le conteneur
   * @return l'identifiant soit l'identifiant de l'algo ou  AS si sensibilite. Ajoute T si transitoire
   */
  public static String generateCalageIdentifiant(AocExecContainer aocExecContainer) {
    String identifiant = aocExecContainer.getCampagne().getCalage().getAlgorithme().getIdentifiantShort();
    if (isAnaylseSensibilite(aocExecContainer.getCampagne())) {
      identifiant = "AS";
    }
    //on ajoute T pour les cas transitoires.
    if (!aocExecContainer.getCampagne().isPermanent()) {
      identifiant = identifiant + "T";
    }
    return identifiant;
  }

  /**
   * @return group with empty logs or child group
   */
  public static CtuluLogGroup createGroupWtihErrorOnly(CtuluLogGroup init) {
    CtuluLogGroup res = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    res.setDescriptionArgs(init.getDescriptionArgs());
    res.setDescription(init.getDescription());
    for (CtuluLog log : res.getLogs()) {
      if (log.containsErrorOrSevereError()) {
        CtuluLog logWithErrorOnly = res.createLog();
        logWithErrorOnly.setDesc(log.getDesc());
        logWithErrorOnly.setDescriptionArgs(log.getDescriptionArgs());
        for (CtuluLogRecord ctuluLogRecord : log.getRecords()) {
          if (ctuluLogRecord.getLevel() != null && ctuluLogRecord.getLevel().isLessVerboseThan(CtuluLogLevel.ERROR, false)) {
            logWithErrorOnly.addRecord(ctuluLogRecord);
          }
        }
      }
    }
    if (res.getGroups() != null) {
      for (CtuluLogGroup log : res.getGroups()) {
        CtuluLogGroup clean = createGroupWtihErrorOnly(log);
        if (clean.containsSomething()) {
          res.addGroup(clean);
        }
      }
    }
    return res;
  }
}
