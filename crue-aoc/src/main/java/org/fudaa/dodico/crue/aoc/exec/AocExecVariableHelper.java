package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;

/**
 * @author deniger
 */
public class AocExecVariableHelper {
  public static final String PROP_Z = CrueConfigMetierConstants.PROP_Z;
  public static final String PROP_Q = CrueConfigMetierConstants.PROP_Q;
  private static final String OBSERVED_PREFIX = " " + BusinessMessages.getString("aoc.observedSuffix");
  private static final String COMPUTED_PREFIX = " " + BusinessMessages.getString("aoc.computedSuffix");
  private static final String DELTA_PREFIX = " " + BusinessMessages.getString("aoc.deltaSuffix");

  private AocExecVariableHelper() {

  }

  public static String getNomVariableObserved(String variableName) {
    return variableName + OBSERVED_PREFIX;
  }

  public static String getNomVariableComputed(String variableName) {
    return variableName + COMPUTED_PREFIX;
  }

  public static String getNomVariableDelta(String variableName) {
    return variableName + DELTA_PREFIX;
  }
}
