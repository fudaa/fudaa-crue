package org.fudaa.dodico.crue.aoc.projet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deniger on 28/06/2017.
 */
public class AocLoisCalculsTransitoires {
  private final List<AocLoiCalculTransitoire> lois = new ArrayList<>();

  public List<AocLoiCalculTransitoire> getLois() {
    return lois;
  }

  public void setClonedLois(List<AocLoiCalculTransitoire> loiCalculTransitoires) {
    this.lois.clear();
    if (lois != null) {
      for (AocLoiCalculTransitoire aocLoiCalculTransitoire : loiCalculTransitoires) {
        this.lois.add(aocLoiCalculTransitoire.clone());
      }
    }
  }

  public void addLoiCalculTransitoire(String calculRef, String loiRef, String sectionRef, int ponderation) {
    lois.add(new AocLoiCalculTransitoire(calculRef, loiRef, sectionRef, ponderation));
  }
}
