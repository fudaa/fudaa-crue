package org.fudaa.dodico.crue.aoc.compute;

import gnu.trove.TLongHashSet;
import org.fudaa.dodico.crue.metier.result.TimeSimuConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.LongStream;

/**
 * Calcul du RMSE sur des données temporelles. On suppose que x est en seconde et sera traduit en millis (long)
 *
 * @author deniger
 */
public class RmseOnTimeCourbeCompute {
  /**
   * @param list la liste des valeurs
   * @param otherT la valeur de t à comparer
   * @return un indice position si trouvé. Si -i-1 par rapport aux éléments englobant
   */
  protected static int indexedBinarySearch(List<Value> list, long otherT) {
    int low = 0;
    int high = list.size() - 1;

    while (low <= high) {
      int mid = (low + high) >>> 1;
      Value midVal = list.get(mid);
      int cmp = midVal.compareToT(otherT);

      if (cmp < 0) {
        low = mid + 1;
      } else if (cmp > 0) {
        high = mid - 1;
      } else {
        return mid; // key found
      }
    }
    return -(low + 1);  // key not found
  }

  /**
   * Attention ce sont les valeurs observees et calculees
   *
   * @param expected observe
   * @param actual calculé
   */
  public double compute(CollectCompute expected, CollectCompute actual) {

    ValueContent expectedValues = extractValues(expected);
    ValueContent actualValues = extractValues(actual);

    long[] times = extractValuesToUseToInterpolate(expectedValues, actualValues);
    RmseCompute compute = new RmseCompute();
    LongStream.of(times).forEach(time -> compute.add(expectedValues.getValueForT(time), actualValues.getValueForT(time)));
    return compute.getRMSE();
  }

  public long[] extractValuesToUseToInterpolate(ValueContent expectedValues, ValueContent actualValues) {
    TLongHashSet values = new TLongHashSet();
    long min = Math.max(expectedValues.getMin(), actualValues.getMin());
    long max = Math.min(expectedValues.getMax(), actualValues.getMax());
    expectedValues.fillWithTimeInRange(min, max, values);
    actualValues.fillWithTimeInRange(min, max, values);
    long[] res = values.toArray();
    Arrays.sort(res);
    return res;
  }

  public ValueContent extractValues(CollectCompute collection) {
    int nb = collection.getNbValues();
    List<Value> values = new ArrayList<>(nb);
    for (int i = 0; i < nb; i++) {
      values.add(new Value(collection.getX(i), collection.getY(i)));
    }
    Collections.sort(values);
    return new ValueContent(values);
  }

  public static class ValueContent {
    private final List<Value> values;

    private ValueContent(List<Value> values) {
      this.values = Collections.unmodifiableList(values);
    }

    public double getValueForT(long t) {
      if (values.isEmpty()) {
        return 0;
      }
      if (t < getMin()) {
        return getFirst().y;
      }
      if (t > getMax()) {
        return getLast().y;
      }
      int idx = indexedBinarySearch(values, t);
      if (idx >= 0) {
        return values.get(idx).y;
      }
      final Value valueInf = getValues().get(-idx - 2);
      final Value valueSup = getValues().get(-idx - 1);
      double ySup = valueSup.y;
      double xSup = valueSup.t;
      double yInf = valueInf.y;
      double xInf = valueInf.t;
      return ySup - (xSup - t) / (xSup - xInf) * (ySup - yInf);
    }

    public Value getLast() {
      return values.get(values.size() - 1);
    }

    /**
     * @param tMin le tMin
     * @param tMax le tMax
     * @param target contiendra tous les t tel que tMin<=t<=tMax
     */
    public void fillWithTimeInRange(final long tMin, final long tMax, final TLongHashSet target) {
      values.forEach(value -> {
        if (value.t >= tMin && value.t <= tMax) {
          target.add(value.t);
        }
      });
    }

    public int getSize() {
      return values.size();
    }

    public long getMin() {
      if (values.isEmpty()) {
        return 0;
      }
      return getFirst().t;
    }

    public Value getFirst() {
      return values.get(0);
    }

    public long getMax() {
      if (values.isEmpty()) {
        return 0;
      }
      return getLast().t;
    }

    public List<Value> getValues() {
      return values;
    }
  }

  /**
   * contient le coupe t, value. La comparaison (Comparable) ne porte que sur t.
   */
  public static class Value implements Comparable<Value> {
    private final long t;
    private final double y;

    public Value(double x, double y) {
      this.t = TimeSimuConverter.toMilis(x);
      this.y = y;
    }

    public long getT() {
      return t;
    }

    public double getY() {
      return y;
    }

    /**
     * @param o
     * @return 0 si t est le meme
     */
    @Override
    public int compareTo(Value o) {
      if (o == this) {
        return 0;
      }
      final long otherT = o.t;
      return compareToT(otherT);
    }

    public int compareToT(long otherT) {
      long delta = this.t - otherT;
      if (delta == 0) {
        return 0;
      }
      return delta > 0 ? 1 : -1;
    }
  }
}
