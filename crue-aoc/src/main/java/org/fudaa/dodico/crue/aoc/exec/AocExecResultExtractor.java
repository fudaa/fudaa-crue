package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.compute.*;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmDataItem;
import org.fudaa.dodico.crue.aoc.exec.writer.AocExecFormatterHelper;
import org.fudaa.dodico.crue.aoc.exec.writer.AocExecWriterHelper;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculPermanent;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculTransitoire;
import org.fudaa.dodico.crue.metier.aoc.EnumAocTypeCalageCritere;
import org.fudaa.dodico.crue.aoc.validation.AocValidationHelper;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.aoc.ParametrageProfilSection;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.result.TimeSimuConverter;

import java.text.NumberFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Permet de calculer et persister les données pour chaque iteration
 *
 * @author deniger
 */
public class AocExecResultExtractor {
  /**
   * le contenu
   */
  private final AocExecContainer aocExecContainer;
  /**
   * les calculs permanents a prendre en compte
   */
  private final List<AocLoiCalculPermanent> calculsPermanent;
  /**
   * pk par sections (utile)
   */
  private final Map<String, String> sectionByPk;
  private final Map<String, AbstractLoi> loiByName;
  private final List<AocLoiCalculTransitoire> calculsTransitoire;
  private CtuluLog operationLog;

  public AocExecResultExtractor(AocExecContainer aocExecContainer) {
    this.aocExecContainer = aocExecContainer;
    loiByName = aocExecContainer.getLhpt().getLois().stream().collect(Collectors.toMap(AbstractLoi::getNom, Function.identity()));
    if (AocExecHelper.isValidationCroisee(aocExecContainer.getCampagne()) || aocExecContainer.getCampagne().isPermanent()) {
      calculsPermanent = aocExecContainer.getCampagne().getDonnees().getLoisCalculsPermanents().getLois();
      calculsTransitoire = Collections.emptyList();
      sectionByPk = aocExecContainer.getLhpt().getEchellesSections().getEchellesSectionList()
          .stream().collect(Collectors.toMap(ParametrageProfilSection::getPk, ParametrageProfilSection::getSectionRef));
    } else {
      calculsPermanent = Collections.emptyList();
      calculsTransitoire = AocExecHelper.extractLoiCalculTransitoire(aocExecContainer, loiByName);
      sectionByPk = null;
    }
  }

  /**
   * @param resultReader le lecteur de résultats
   * @param dfrtValues les valeurs dfrt
   * @param dclmValues les valeurs dclm
   * @param logger le logger
   * @param iterationIdx l'indice de l'iteration
   * @return l'iteration créee
   */
  AocExecResultIteration create(AocExecResultReader resultReader, Map<String, Double> dfrtValues, List<AocDclmDataItem> dclmValues, AocExecAllLogger logger,
                                int iterationIdx) {
    if (aocExecContainer.getCampagne().isPermanent()) {
      logger.addLogFormated(";Calcul RMSE pour iteration %o", iterationIdx + 1);
    } else {
      logger.addLogFormated(";Calcul " + aocExecContainer.getCampagne().getTypeCalageCritere().getIdentifiant() + " pour iteration %o sur " +
          aocExecContainer.getCampagne().getTypeCalageDonnees().getCourbeName(), iterationIdx + 1);
    }

    AocExecResultIteration iteration = new AocExecResultIteration();
    iteration.setDclmValues(dclmValues);
    iteration.setDfrtValues(dfrtValues);
    iteration.setIndex(iterationIdx);

    final boolean validationCroisee = AocExecHelper.isValidationCroisee(aocExecContainer.getCampagne());
    if (validationCroisee) {
      final String calculApprentissage = aocExecContainer.getResultContainer().getValidationCroiseeLine().getCalculApprentissage().stream().sorted()
          .collect(Collectors.joining(","));
      final String calculValidation = aocExecContainer.getResultContainer().getValidationCroiseeLine().getCalculValidation().stream().sorted().collect(Collectors.joining(","));
      logger.addLogFormated(";;Validation croisee;Apprentissage sur les calculs " + calculApprentissage);
      logger.addLogFormated(";;Validation croisee;Validation sur les calculs " + calculValidation);
    }
    AocExecCritereData critereData = new AocExecCritereData(aocExecContainer.getResultContainer(), logger, operationLog);
    if (!calculsPermanent.isEmpty()) {
      extractCalculPermanent(resultReader, iteration, critereData);
    } else if (!calculsTransitoire.isEmpty()) {
      extractCalculTransitoire(resultReader, iteration, critereData);
    }
    if (operationLog.containsErrorOrSevereError()) {
      return iteration;
    }
    iteration.setCritere(critereData.getCritere(), EnumAocTypeCritere.GLOBAL);
    iteration.setCritereTotal(iteration.getCritere());
    iteration.setCritereValidation(critereData.getCritereValidation());
    iteration.setCritereApprentissage(critereData.getCritereApprentissage());
    if (validationCroisee) {
      //ATTENTION: le critere d'apprentissage est utilisee pour le calage
      iteration.setCritere(iteration.getCritereApprentissage(), EnumAocTypeCritere.VALIDATION_CROISEE_APPRENTISSAGE);
      iteration.setValidationCroiseeLine(aocExecContainer.getResultContainer().getValidationCroiseeLine());
      logger
          .addLogFormated(";RMSE iteration %o;RMSE (apprentissage):%f;RMSE validation;%f;RMSE total:%f",
              iterationIdx + 1,
              iteration.getCritere(),
              iteration.getCritereValidation(),
              iteration.getCritereTotal());
    } else {
      logger.addLogFormated(";Critere iteration %o;Critere =" + iteration.getCritere(), iterationIdx + 1);
    }
    return iteration;
  }

  /**
   * on extrait les résultats et les calculs de critere dans le cas permanent
   *
   * @param resultReader le lecteur de résultats
   * @param iteration l'iteration
   * @param critereData les donnees de calcul des criteres
   */
  private void extractCalculPermanent(AocExecResultReader resultReader, AocExecResultIteration iteration, AocExecCritereData critereData) {
    AocExecAllLogger logger = critereData.getLogger();
    final NumberFormat zFormater = AocExecFormatterHelper.getZFormater(this.aocExecContainer);
    for (AocLoiCalculPermanent calcul : calculsPermanent) {
      logger.addLogFormated(";;Démarrage Calcul RMSE pour calcul %s et loi %s", calcul.getCalculRef(), calcul.getLoiRef());
      final LoiTF loi = (LoiTF) loiByName.get(calcul.getLoiRef());
      if (loi == null) {
        continue;
      }

      //pour le logger
      StringJoiner sectionJoiner = new StringJoiner(";") ;
      StringJoiner valueObserveJoiner = new StringJoiner(";");
      StringJoiner valueComputeJoiner = new StringJoiner(";");
      StringJoiner deltaJoiner = new StringJoiner(";");

      //les resultats par iterations pour ecrire ensuite les resultats dans les fichiers bilan
      AocExecResult resultatForCalcul = iteration.createResultByCalcul(calcul.getCalculRef(), calcul.getLoiRef(), calcul.getPonderation());
      //le pas de temps a prendre en compte
      final ResultatTimeKey resultatTimeKey = resultReader.getPermanentResultatKey(calcul.getCalculRef());
      RmseCompute rmseCompute = new RmseCompute();

      //on calcul pour chaque LoiTZ
      for (PtEvolutionTF ptEvolutionTF : loi.getEvolutionTF().getPtEvolutionTF()) {
        String pk = ptEvolutionTF.getAbscisse();
        String section = sectionByPk.get(pk);
        //les valeurs observées et calculées
        final double valueObserved = ptEvolutionTF.getOrdonnee();
        final Double valueComputed = resultReader.readValue(section, resultatTimeKey, AocExecVariableHelper.PROP_Z);
        //on stocke les valeurs aux points de controles pour l'écriture des fichiers csv de compte-rendu
        final AocExecResultByEMH resultByVariable = resultatForCalcul.getOrCreateResultByVariable(AocExecVariableHelper.PROP_Z, zFormater);
        resultByVariable.addObservedValueForEMH(section, valueObserved);
        if (valueComputed != null) {
          resultByVariable.addComputedValueForEMH(section, valueComputed);
          resultByVariable.addDeltaValueForEMH(section, valueObserved - valueComputed);
        }

        //on logue
        if (logger.isOn()) {
          sectionJoiner.add(section);
          valueObserveJoiner.add(Double.toString(valueObserved));
          valueComputeJoiner.add(valueComputed == null ? "" : Double.toString(valueComputed));
          if (valueComputed != null) {
            deltaJoiner.add(Double.toString(valueObserved - valueComputed));
          }
        }
        //calcul du RMSE:
        if (valueComputed != null) {
          rmseCompute.add(valueObserved, valueComputed);
        }
      }
      final double rmse = rmseCompute.getRMSE();

      //on logue
      if (logger.isOn()) {

        logger.addLogFormated(";;Sections;" + sectionJoiner.toString());
        logger.addLogFormated(";;Valeurs observées;" + valueObserveJoiner.toString());
        logger.addLogFormated(";;Valeurs calculées;" + valueComputeJoiner.toString());
        logger.addLogFormated(";;Deltas;" + deltaJoiner.toString());
        logger.addLogFormated(";;Nombre valeurs prise en compte dans RMSE;Nombre= " + rmseCompute.getN());
        logger.addLogFormated(";;Resultat Calcul RMSE pour calcul %s et loi %s;RMSE=" + rmse, calcul.getCalculRef(), calcul.getLoiRef());
        logger.addLogFormated("");
      }

      //on met à jour le critere pour le calcul
      resultatForCalcul.setCritere(rmse);

      critereData.addCalculCritere(rmse, calcul);
    }
  }

  private ComputeXYContrat getComputeForTransitoire(EnumAocTypeCalageCritere calageCritere) {
    switch (calageCritere) {
      case ECART_VOLUMES:
        return new AreaCompute();
      case ECART_TEMPS_ARRIVEE_MAX:
        return new XForMaxYCompute();
      case ECART_NIVEAUX_MAX:
        return new MinMaxMoyCompute();
      case ERREUR_QUADRATIQUE:
        return new CollectCompute();
      default:
        return null;
    }
  }

  /**
   * on extrait les résultats et les calculs de critere dans le cas transitoire
   *
   * @param resultReader le lecteur de résultats
   * @param iteration l'iteration
   * @param critereData les donnees de calcul des criteres
   */
  private void extractCalculTransitoire(AocExecResultReader resultReader, AocExecResultIteration iteration, AocExecCritereData critereData) {
    AocExecAllLogger logger = critereData.getLogger();

    final EnumAocTypeCalageCritere calageCritere = aocExecContainer.getCampagne().getTypeCalageCritere();

    for (AocLoiCalculTransitoire calcul : calculsTransitoire) {

      logger.addLogFormated(";;Calcul Critere %s;Calcul %s;Loi %s;Section %s", calageCritere.geti18n(), calcul.getCalculRef(), calcul.getLoiRef(), calcul.getSectionRef());
      final LoiDF loi = (LoiDF) loiByName.get(calcul.getLoiRef());
      if (loi == null) {
        continue;
      }

      String variable = AocValidationHelper.useLoiTZForTransitoire(aocExecContainer.getCampagne()) ? AocExecVariableHelper.PROP_Z : AocExecVariableHelper.PROP_Q;

      ComputeXYContrat computeXYContratObserve = getComputeForTransitoire(calageCritere);
      ComputeXYContrat computeXYContratComputed = getComputeForTransitoire(calageCritere);
      if (computeXYContratObserve == null || computeXYContratComputed == null) {
        critereData.getLog().addError("aoc.exec.noComputerForCritere", calageCritere.geti18n());
        return;
      }

      //on prend en comte la DateZero de la loi.
      //Attention les données des lois sont en secondes
      //les resultats (ResultatTimeKey) stockent les données en millis ( long)

      //on calcul le critere pour les valeurs observees
      final double dateZeroSecond = TimeSimuConverter.getDeltaToApplyForDateTZero(aocExecContainer.getScenarioReference(), loi);
      loi.getEvolutionFF().getPtEvolutionFF().forEach(pt -> computeXYContratObserve.addXY(dateZeroSecond + pt.getAbscisse(), pt.getOrdonnee()));
      double observedValue = computeXYContratObserve.getValue();

      //on calcul le critere pour les valeurs calculées
      //la loi issue du calcul
      LoiDF computedLoi = extractLoiComputed(resultReader, calcul, variable);
      computedLoi.getEvolutionFF().getPtEvolutionFF()
          .forEach(ptEvolutionFF -> computeXYContratComputed.addXY(ptEvolutionFF.getAbscisse(), ptEvolutionFF.getOrdonnee()));
      double computedValue = computeXYContratComputed.getValue();

      //on initialise le conteneur de resultat:
      AocExecResult resultatForCalcul = iteration.createResultByCalcul(calcul.getCalculRef(), calcul.getLoiRef(), calcul.getPonderation());
      resultatForCalcul.setHydroOrLimniObserved(loi);
      resultatForCalcul.setHydroOrLimniCompute(computedLoi);
      resultatForCalcul.setSection(calcul.getSectionRef());

      final AocExecResultByEMH resultByEMH = resultatForCalcul.getOrCreateResultByVariable(calageCritere.getNomVariable(), null);

      double critere;
      //dans le cas du critere erreur quadratique, il faut faire un calcul RMSE sur les points interpolés
      if (EnumAocTypeCalageCritere.ERREUR_QUADRATIQUE.equals(calageCritere)) {
        critere = new RmseOnTimeCourbeCompute().compute((CollectCompute) computeXYContratObserve, (CollectCompute) computeXYContratComputed);
      } else {
        //sinon on prend la valeur absolu du delta

        critere = Math.abs(observedValue - computedValue);

        //on stocke les valeurs aux sections de controle pour l'écriture des fichiers csv de compte-rendu
        resultByEMH.addObservedValueForEMH(calcul.getSectionRef(), observedValue);
        resultByEMH.addComputedValueForEMH(calcul.getSectionRef(), computedValue);
      }

      resultByEMH.setNameVariableDelta(calageCritere.getIdentifiant());
      resultByEMH.addDeltaValueForEMH(calcul.getSectionRef(), critere);

      resultatForCalcul.setCritere(critere);
      critereData.addCalculCritere(critere, calcul);
    }
    //on logue
    if (logger.isOn()) {
      List<String> lines = new ArrayList<>();
      AocExecWriterHelper.writeControlPointsTransitoire(iteration, aocExecContainer, lines, ";;");
      lines.forEach(logger::addLogFormated);
    }
  }

  private LoiDF extractLoiComputed(AocExecResultReader resultReader, AocLoiCalculTransitoire calcul, String variable) {
    final List<ResultatTimeKey> resultatTimeKey = resultReader.getTransientKey(calcul.getCalculRef());
    LoiDF computedLoi = new LoiDF();
    EnumTypeLoi loiType = AocValidationHelper.useLoiTZForTransitoire(aocExecContainer.getCampagne()) ? EnumTypeLoi.LoiTZ : EnumTypeLoi.LoiTQ;
    computedLoi.setType(loiType);
    final EvolutionFF newEvolutionFF = new EvolutionFF();
    newEvolutionFF.initListPoints();
    computedLoi.setEvolutionFF(newEvolutionFF);
    if (resultatTimeKey != null) {
      resultatTimeKey.forEach(time -> newEvolutionFF.addPoint(TimeSimuConverter.toSecond(time.getDuree()), resultReader.readValue(calcul.getSectionRef(), time, variable)));
    }
    return computedLoi;
  }

  public CtuluLog getOperationLog() {
    return operationLog;
  }

  public void setOperationLog(CtuluLog operationLog) {
    this.operationLog = operationLog;
  }
}
