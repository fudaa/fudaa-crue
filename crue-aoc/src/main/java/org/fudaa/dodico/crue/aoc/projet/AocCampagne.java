package org.fudaa.dodico.crue.aoc.projet;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.aoc.EnumAocTypeCalageCritere;
import org.fudaa.dodico.crue.metier.aoc.EnumAocTypeCalageDonnees;
import org.joda.time.LocalDateTime;

import java.io.File;

/**
 * Une campagne de tests
 *
 * @author deniger
 */
public class AocCampagne {
  private String commentaire;
  private String auteurCreation;
  private LocalDateTime dateCreation;
  private String auteurModification;
  private LocalDateTime dateModification;
  private File aocFile;
  private String etudeChemin;
  private String nomScenario;
  private EnumAocTypeCalageCritere typeCalageCritere = EnumAocTypeCalageCritere.ERREUR_QUADRATIQUE;
  private EnumAocTypeCalageDonnees typeCalageDonnees = EnumAocTypeCalageDonnees.PERMANENT;
  private final AocCampagneCalage calage = new AocCampagneCalage();
  private final AocCampagneDonnees donnees = new AocCampagneDonnees();
  private final AocParametreNumeriques parametreNumeriques;
  private boolean logAll;

  public AocCampagne(CrueConfigMetier ccm) {
    parametreNumeriques = new AocParametreNumeriques(ccm);
  }

  public AocParametreNumeriques getParametreNumeriques() {
    return parametreNumeriques;
  }


  public AocCampagneDonnees getDonnees() {
    return donnees;
  }

  /**
   * @return true si calage permanent
   */
  public boolean isPermanent() {
    return EnumAocTypeCalageDonnees.PERMANENT.equals(getTypeCalageDonnees());
  }


  public AocCampagneCalage getCalage() {
    return calage;
  }


  public EnumAocTypeCalageCritere getTypeCalageCritere() {
    return typeCalageCritere;
  }

  public void setTypeCalageCritere(EnumAocTypeCalageCritere typeCalageCritere) {
    this.typeCalageCritere = typeCalageCritere;
  }

  public EnumAocTypeCalageDonnees getTypeCalageDonnees() {
    return typeCalageDonnees;
  }

  public void setTypeCalageDonnees(EnumAocTypeCalageDonnees typeCalageDonnees) {
    this.typeCalageDonnees = typeCalageDonnees;
  }

  public String getEtudeChemin() {
    return etudeChemin;
  }

  public void setEtudeChemin(String etudeChemin) {
    this.etudeChemin = etudeChemin;
  }

  public String getNomScenario() {
    return nomScenario;
  }

  public void setNomScenario(String nomScenario) {
    this.nomScenario = nomScenario;
  }

  public File getAocFile() {
    return aocFile;
  }

  public void setAocFile(File aocFile) {
    this.aocFile = aocFile;
  }

  public String getCommentaire() {
    return commentaire;
  }

  public void setCommentaire(String commentaire) {
    this.commentaire = commentaire;
  }

  public String getAuteurCreation() {
    return auteurCreation;
  }

  public void setAuteurCreation(String auteurCreation) {
    this.auteurCreation = auteurCreation;
  }

  public LocalDateTime getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(LocalDateTime dateCreation) {
    this.dateCreation = dateCreation;
  }

  public String getAuteurModification() {
    return auteurModification;
  }

  public void setAuteurModification(String auteurModification) {
    this.auteurModification = auteurModification;
  }

  public LocalDateTime getDateModification() {
    return dateModification;
  }

  public void setDateModification(LocalDateTime dateModification) {
    this.dateModification = dateModification;
  }

  public void setLogAll(boolean logAll) {
    this.logAll = logAll;
  }

  public boolean isLogAll() {
    return logAll;
  }

  public double getSeuilToUse() {
    return parametreNumeriques.getSeuilToUse(this);
  }
}
