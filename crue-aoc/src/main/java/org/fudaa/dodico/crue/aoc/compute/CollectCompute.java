package org.fudaa.dodico.crue.aoc.compute;

import gnu.trove.TDoubleArrayList;

/**
 * Utilise pour le calcul quadratique. Dans un premier temps, stocke toutes les valeurs.
 *
 * @author deniger
 */
public class CollectCompute implements ComputeXYContrat {
  private final TDoubleArrayList xValues = new TDoubleArrayList();
  private final TDoubleArrayList yValues = new TDoubleArrayList();

  @Override
  public int getNbValues() {
    return xValues.size();
  }

  @Override
  public void addXY(double x, double y) {
    xValues.add(x);
    yValues.add(y);
  }

  public double getX(int i) {
    return xValues.get(i);
  }

  public double getY(int i) {
    return yValues.get(i);
  }

  @Override
  public double getValue() {
    return 0;
  }
}
