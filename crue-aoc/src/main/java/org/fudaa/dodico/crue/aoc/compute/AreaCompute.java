package org.fudaa.dodico.crue.aoc.compute;

/**
 * Permet de calculer l'air d'une courbe
 * @author deniger
 */
public class AreaCompute implements ComputeXYContrat {
  private int nbValues;
  private double t;
  private double f;
  private double area;

  public double getArea() {
    return area;
  }

  @Override
  public double getValue() {
    return getArea();
  }

  public int getNbValues() {
    return nbValues;
  }

  @Override
  public void addXY(double x, double y) {
    add(x, y);
  }

  public void add(double ti, double fi) {
    if (nbValues > 0) {
      double vi = (ti - t) * (f + fi) / 2;
      area = area + vi;
    }
    t = ti;
    f = fi;
    nbValues++;
  }
}
