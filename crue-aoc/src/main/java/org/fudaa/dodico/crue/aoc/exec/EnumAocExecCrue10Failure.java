package org.fudaa.dodico.crue.aoc.exec;

/**
 * Enum permettant de caractériser les echecs d'itération.
 * @author deniger
 */
public enum EnumAocExecCrue10Failure {

  /**
   * Crue10 echoue à l'initialisation: inutile de recommencer
   */
  FAILURE_AT_INIT_ITERATION,
  /**
   * Crue10 echoue lors du caclcul de l'optimum avec les strickers tronqués: inutile de recommencer
   */
  FAILURE_AT_OPTIMUM_STRICKLER_AS_INTEGER,
  /**
   * Trop d'échec -> arret des iérations
   */
  TOO_MANY_FAILURE

}
