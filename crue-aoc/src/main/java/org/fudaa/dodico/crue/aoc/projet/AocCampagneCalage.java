package org.fudaa.dodico.crue.aoc.projet;

import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageAlgorithme;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageType;

/**
 * Created by deniger on 28/06/2017.
 */
public class AocCampagneCalage {

    private EnumAocCalageType operation = EnumAocCalageType.SEUL;
    private EnumAocCalageAlgorithme algorithme = EnumAocCalageAlgorithme.RECUIT;
    private int nombreIterationSeul=1;
    private int nombreIterationValidationCroisee=1;
    private int nombreIterationTir=1;
    private int nombreTir=1;
    private double ponderationApprentissage;
    private double PonderationValidation;

    public EnumAocCalageType getOperation() {
        return operation;
    }

    public void setOperation(EnumAocCalageType operation) {
        this.operation = operation;
    }

    public EnumAocCalageAlgorithme getAlgorithme() {
        return algorithme;
    }

    public void setAlgorithme(EnumAocCalageAlgorithme algorithme) {
        this.algorithme = algorithme;
    }

    public int getNombreIterationSeul() {
        return nombreIterationSeul;
    }

    public void setNombreIterationSeul(int nombreIterationSeul) {
        this.nombreIterationSeul = nombreIterationSeul;
    }

    public int getNombreIterationValidationCroisee() {
        return nombreIterationValidationCroisee;
    }

    public void setNombreIterationValidationCroisee(int nombreIterationValidationCroisee) {
        this.nombreIterationValidationCroisee = nombreIterationValidationCroisee;
    }

    public int getNombreIterationTir() {
        return nombreIterationTir;
    }

    public void setNombreIterationTir(int nombreIterationTir) {
        this.nombreIterationTir = nombreIterationTir;
    }

    public int getNombreTir() {
        return nombreTir;
    }

    public void setNombreTir(int nombreTir) {
        this.nombreTir = nombreTir;
    }

    public double getPonderationApprentissage() {
        return ponderationApprentissage;
    }

    public void setPonderationApprentissage(double ponderationApprentissage) {
        this.ponderationApprentissage = ponderationApprentissage;
    }

    public double getPonderationValidation() {
        return PonderationValidation;
    }

    public void setPonderationValidation(double ponderationValidation) {
        PonderationValidation = ponderationValidation;
    }
}
