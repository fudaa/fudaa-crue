package org.fudaa.dodico.crue.aoc.validation;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluDefaultLogFormatter;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculTransitoire;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.validation.ValidatingItem;

import java.util.*;

/**
 * Classe permettant de valider une liste de  AocLoiCalculPermanent
 *
 * @author deniger
 */
public class AocLoiCalculTransitoireValidator {
  /**
   * @param nodes list des {@link org.fudaa.dodico.crue.aoc.projet.AocLoiCalculTransitoire}
   * @param availableCalculsArray les calculs disponibles dans le scenario
   * @param availableLoisArray les lois disponibles dans le fichier LHPT
   * @param availableSectionsArray les sections actives disponibles dans le scenario
   */
  public CtuluLog validate(List<ValidatingItem<AocLoiCalculTransitoire>> nodes, String[] availableCalculsArray, String[] availableLoisArray,
                           String[] availableSectionsArray) {
    return validate(nodes, Arrays.asList(availableCalculsArray), Arrays.asList(availableLoisArray), Arrays.asList(availableSectionsArray));
  }

  /**
   * @param nodes list des {@link org.fudaa.dodico.crue.aoc.projet.AocLoiCalculTransitoire}
   * @param availableCalculsArray les calculs disponibles dans le scenario
   * @param availableLoisArray les lois disponibles dans le fichier LHPT
   * @param availableSectionsArray les sections actives disponibles dans le scenario
   */
  public CtuluLog validate(List<ValidatingItem<AocLoiCalculTransitoire>> nodes, List<String> availableCalculsArray, List<String> availableLoisArray,
                           List<String> availableSectionsArray) {
    Set<String> usedLois = new HashSet<>();
    Map<String, Set<String>> usedSectionByCalcul = new HashMap<>();
    CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    res.setDesc("aoc.validation.loiCalculTransitoire");
    int idx = 0;
    if (nodes.isEmpty()) {
      return res;
    }
    Set<String> availableCalculs = AocLoiCalculPermanentValidator.createHashSet(availableCalculsArray);
    Set<String> availableLois = AocLoiCalculPermanentValidator.createHashSet(availableLoisArray);
    Set<String> availableSections = AocLoiCalculPermanentValidator.createHashSet(availableSectionsArray);
    for (ValidatingItem<AocLoiCalculTransitoire> node : nodes) {
      idx++;
      List<CtuluLogRecord> errorMsg = new ArrayList<>();

      //Validation du nom de calcul
      String calculRef = node.getObjectToValidate().getCalculRef();
      if (StringUtils.isBlank(calculRef)) {
        errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculTransitoire.calculBlank", idx));
      } else {
        if (!availableCalculs.contains(calculRef)) {
          errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculTransitoire.calculNotFound", idx, calculRef));
        }
      }

      //Validation de la loi
      String loiRef = node.getObjectToValidate().getLoiRef();
      if (StringUtils.isBlank(loiRef)) {
        errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculTransitoire.loiBlank", idx));
      } else {
        if (!availableLois.contains(loiRef)) {
          errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculTransitoire.loiNotFound", idx, loiRef));
        }
        if (usedLois.contains(loiRef.toUpperCase())) {
          errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculTransitoire.loiRefAlreadyUsed", idx, loiRef));
        }
        if (CruePrefix.prefixMustBeAdded(EnumTypeLoi.LoiTQ.getId(), loiRef) && CruePrefix.prefixMustBeAdded(EnumTypeLoi.LoiTZ.getId(), loiRef)) {
          errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculTransitoire.loiInvalid", idx, loiRef));
        }
        usedLois.add(loiRef.toUpperCase());
      }

      String sectionRef = node.getObjectToValidate().getSectionRef();
      //Validation de la section
      if (StringUtils.isBlank(sectionRef)) {
        errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculTransitoire.sectionBlank", idx));
      } else {
        if (!availableSections.contains(sectionRef)) {
          errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculTransitoire.sectionNotFound", idx, sectionRef));
        }
        if (CruePrefix.prefixMustBeAdded(CruePrefix.P_SECTION, sectionRef)) {
          errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculTransitoire.sectionInvalid", idx, sectionRef));
        }
        if (StringUtils.isNotBlank(calculRef)) {
          Set<String> usedSections = usedSectionByCalcul.computeIfAbsent(calculRef, k -> new HashSet<>());
          if (usedSections.contains(sectionRef)) {
            errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculTransitoire.sectionUsedSeveralTimeForSameCalcul", idx,sectionRef, calculRef));
          }
          usedSections.add(sectionRef);
        }
      }

      //ponderation
      if (node.getObjectToValidate().getPonderation() < 1) {
        errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculTransitoire.ponderationInvalid", idx));
      }

      if (!errorMsg.isEmpty()) {
        node.setErrorMessage(CtuluDefaultLogFormatter.asHtml(errorMsg, BusinessMessages.RESOURCE_BUNDLE));
      } else {
        node.clearMessage();
      }
    }
    res.updateLocalizedMessage(BusinessMessages.RESOURCE_BUNDLE);
    return res;
  }
}
