package org.fudaa.dodico.crue.aoc.exec.writer;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.aoc.exec.*;
import org.fudaa.dodico.crue.aoc.helper.AocValidationCroiseeLineBuilder;

import java.io.File;
import java.util.*;

/**
 * Ecrivain pour les résultats des validations croisées.
 *
 * @author deniger
 */
public class AocExecResultWriterValidationCroisee {
  private final AocExecContainer aocExecContainer;
  private final AocExecAllLogger logger;
  private final CtuluLogGroup parentLogs;

  public AocExecResultWriterValidationCroisee(AocExecContainer aocExecContainer, AocExecAllLogger logger, CtuluLogGroup parentLogs) {
    this.aocExecContainer = aocExecContainer;
    this.logger = logger;
    this.parentLogs = parentLogs;
  }

  private double getCriterePondere(AocExecResultIteration aocExecResultIteration) {
    double ponderationApprentissage = aocExecContainer.getCampagne().getCalage().getPonderationApprentissage();
    double ponderationValidation = aocExecContainer.getCampagne().getCalage().getPonderationValidation();
    double sum = ponderationApprentissage + ponderationValidation;
    return (ponderationApprentissage * aocExecResultIteration.getCritereApprentissage() + ponderationValidation * aocExecResultIteration
        .getCritereValidation()) / sum;
  }

  /**
   * Ecrit tous les résultats.
   *
   * @param resultByTir
   * @param parentMainTimeInfo
   */
  public void writeResultats(List<AocExecResultIteration> resultByTir, AocExecTimeInfo parentMainTimeInfo) {

    logger.start("_validation_croisee");
    CtuluLog log = parentLogs.createNewLog("aoc.exec.write.result");
    //on determine la meilleure combinaison
    final AocExecResultIteration iterationWithMinCritere = computeCritereMin(resultByTir);
    if (iterationWithMinCritere == null) {
      log.addError("aoc.iteration.noOptimumDataFound");
      logger.addLogFormated("Pas d'iteration avec le critere minimal trouvé");
      return;
    }
    //on logue les resultats
    logger
        .addLogFormated("Iteration avec 'erreur apprentissage validation' min sur combinaison %o et erreur=%f", iterationWithMinCritere.getIndex() + 1,
            iterationWithMinCritere.getCritere());

    //le scenario cible doit contenir les valeurs optimales.
    logger.addLogDfrtDclm("Modification du scenario cible avec meileur tir", iterationWithMinCritere.getDfrtValues(), iterationWithMinCritere.getDclmValues());

    //on ecrit dans le scenario cible les valeurs.
    writeDfrtAndDclmDansScenarioCible(iterationWithMinCritere);
    //la trajectoire des combinaisons
    writeTrajectoire(resultByTir, log);

    //ecriture des données des frottements pour le meilleur résultat
    AocExecWriterHelper
        .writeDfrtResults(true, aocExecContainer, iterationWithMinCritere, iterationWithMinCritere.getOptimumResultat(), log,
            new File(aocExecContainer.getReportDirectory(), "resultats_validation_croisee_stricklers.csv"));

    //ecriture des données dclm ( si présentes) pour le meilleur résultat
    AocExecWriterHelper
        .writeDclmResults(true, aocExecContainer, iterationWithMinCritere, log, new File(aocExecContainer.getReportDirectory(), "resultats_validation_croisee_debits.csv"));

    //le bilan durée + date debut/fin
    final List<String> bilanHeader = AocExecWriterHelper.createBilanHeader(parentMainTimeInfo);
    bilanHeader.add("");
    bilanHeader.add("#Hauteur/ecart aux echelles pour la troncature (combinaison: " + (iterationWithMinCritere.getIndex() + 1) + ")");
    AocExecWriterHelper.writeErrorControlPointsPermanent(iterationWithMinCritere, aocExecContainer, bilanHeader);
    AocExecWriterHelper.writeToFiles(log, bilanHeader,
        new File(aocExecContainer.getReportDirectory(), "resultats_validation_croisee_bilan.csv"));

    logger.stop();
  }

  private void writeDfrtAndDclmDansScenarioCible(AocExecResultIteration iterationWithMinCritere) {
    AOCStricklerModifier dfrtModifier = new AOCStricklerModifier();
    CtuluLogGroup dftrModifiedLogs = dfrtModifier
        .applyFrottement(aocExecContainer.getProjet(), aocExecContainer.getManagerScenarioCible(), iterationWithMinCritere.getDfrtValues());
    parentLogs.addGroup(dftrModifiedLogs);
    if (iterationWithMinCritere.getDclmValues() != null) {
      final CtuluLog logModificationDclm = AOCDclmModifier
          .applyDclms(aocExecContainer.getProjet(), aocExecContainer.getManagerScenarioCible(), iterationWithMinCritere.getDclmValues());
      parentLogs.addLog(logModificationDclm);
    }
  }

  /**
   * Calcul pour chaque iteration de l erreur apprentissage validation
   *
   * @param resultByTir les resultats par tir/combinaison
   */
  private AocExecResultIteration computeCritereMin(List<AocExecResultIteration> resultByTir) {
    int idx = 0;
    double ponderationApprentissage = aocExecContainer.getCampagne().getCalage().getPonderationApprentissage();
    double ponderationValidation = aocExecContainer.getCampagne().getCalage().getPonderationValidation();
    logger
        .addLogFormated("Calcul erreur apprentissage valiation;ponderationApprentissage=%f;ponderationValidation=%f", ponderationApprentissage, ponderationValidation);
    for (AocExecResultIteration aocExecResultIteration : resultByTir) {
      aocExecResultIteration.setIndex(idx++);
      //calcul de l'erreur et affectation en tant que critere:
      final double valeurPonderee = getCriterePondere(aocExecResultIteration);
      final double valeurPondereeOptimum = getCriterePondere(aocExecResultIteration.getOptimumResultat());

      //on met à jour les criteres avec l'erreur d'apprentissage max.
      aocExecResultIteration.setCritere(
          valeurPonderee,
          EnumAocTypeCritere.VALIDATION_CROISEE_ERREUR_APPRENTISSAGE);

      aocExecResultIteration.getOptimumResultat().setCritere(
          valeurPondereeOptimum,
          EnumAocTypeCritere.VALIDATION_CROISEE_ERREUR_APPRENTISSAGE);
      logger.addLogFormated(";Combinaison %o;critere=%f", idx, aocExecResultIteration.getCritere());
    }

    int idxMin = AocExecResultIteration.computeMinCritere(resultByTir);
    if (idxMin >= 0) {
      return resultByTir.get(idxMin);
    }
    return null;
  }

  private void createLineForCritere(final AocExecResultIteration line, int size, List<String> lines) {
    final StringJoiner joiner = new StringJoiner(AocExecWriterHelper.DELIMITER);
    joiner.add("comb. " + (line.getIndex() + 1) + "/" + size + (line.getIndex() == line.getCritereMinIteration() ? "*" : ""));
    //il s'agit de l'erreur d'apprentissage
    joiner.add(Double.toString(line.getInitialResultat().getCritere()));
    joiner.add(Double.toString(line.getOptimumResultat().getCritereApprentissage()));
    joiner.add(Double.toString(line.getOptimumResultat().getCritereValidation()));
    joiner.add(Double.toString(line.getOptimumResultat().getCritere()));
    joiner.add(Double.toString(line.getCritereApprentissage()));
    joiner.add(Double.toString(line.getCritereValidation()));
    joiner.add(Double.toString(line.getCritere()));

    lines.add(joiner.toString());
  }

  /**
   * @param resultByTir les resultats par tir
   * @param log le logger
   */
  private void writeTrajectoire(List<AocExecResultIteration> resultByTir, CtuluLog log) {
    File resultatRepetabiliteTrajectoire = new File(aocExecContainer.getReportDirectory(), "resultats_validation_croisee_critere.csv");
    ArrayList<String> lines = new ArrayList<>();

    lines.add(
        "Combinaison;Critere initial;Critere apprentissage optimum;Critere validation optimum;Critere global optimum;Critere apprentissage troncature;Critere validation troncature;Critere global troncature;");
    resultByTir.forEach(line -> createLineForCritere(line, resultByTir.size(), lines));

    lines.add("");
    final List<String> allLigneEau = AocValidationCroiseeLineBuilder.getAllLigneEau(aocExecContainer.getCampagne().getDonnees().getValidationCroisee());
    Collections.sort(allLigneEau);
    lines.add("Combinaison;Donnée;" + String.join(AocExecWriterHelper.DELIMITER, allLigneEau));
    resultByTir.forEach(line -> createLigneEauContent(line, resultByTir.size(), allLigneEau, lines));

    AocExecWriterHelper.writeToFiles(log, lines, resultatRepetabiliteTrajectoire);
  }

  private void createLigneEauContent(AocExecResultIteration line, int size, List<String> allLigneEau, ArrayList<String> targetLines) {
    final String firstColumnContent = "comb. " + (line.getIndex() + 1) + "/" + size + (line.getIndex() == line.getCritereMinIteration() ? "*" : "");
    final StringJoiner joiner = new StringJoiner(AocExecWriterHelper.DELIMITER);
    joiner.add(firstColumnContent);
    joiner.add("Apprentissage");
    allLigneEau.forEach(ligneEau -> joiner.add(line.getValidationCroiseeLine().isLigneEauApprentissage(ligneEau) ? "1" : "0"));
    targetLines.add(joiner.toString());

    addValuesForLoi("Critere Initial", line.getInitialResultat().getResultsByLoi(), allLigneEau, targetLines);
    addValuesForLoi("Critere Optimum", line.getOptimumResultat().getResultsByLoi(), allLigneEau, targetLines);
    addValuesForLoi("Critere Troncature", line.getResultsByLoi(), allLigneEau, targetLines);
  }

  private void addValuesForLoi(String lineTitle, Map<String, AocExecResult> resultsByLoi, List<String> allLigneEau, ArrayList<String> targetLines) {
    final StringJoiner valueJoiner = new StringJoiner(AocExecWriterHelper.DELIMITER);
    valueJoiner.add("");
    valueJoiner.add(lineTitle);
    allLigneEau.forEach(ligneEau -> valueJoiner.add(AocExecWriterHelper.getLoiValue(resultsByLoi, ligneEau)));
    targetLines.add(valueJoiner.toString());
  }
}
