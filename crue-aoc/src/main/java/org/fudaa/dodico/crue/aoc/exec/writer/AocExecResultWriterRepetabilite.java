package org.fudaa.dodico.crue.aoc.exec.writer;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.aoc.compute.MinMaxMoyCompute;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmDataItem;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmKey;
import org.fudaa.dodico.crue.aoc.exec.*;

import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Ecriture des résultats de repetabilité.
 *
 * @author deniger
 */
public class AocExecResultWriterRepetabilite {
  private final AocExecContainer aocExecContainer;
  private final AocExecAllLogger logger;
  private final CtuluLogGroup parentLogs;

  public AocExecResultWriterRepetabilite(AocExecContainer aocExecContainer, AocExecAllLogger logger, CtuluLogGroup parentLogs) {
    this.aocExecContainer = aocExecContainer;
    this.logger = logger;
    this.parentLogs = parentLogs;
  }

  public void writeResultats(List<AocExecResultIteration> resultByTir, AocExecTimeInfo mainTimeInfo) {

    logger.start("_repetabilite");
    //on renitialise les indexs
    int idx = 0;
    for (AocExecResultIteration aocExecResultIteration : resultByTir) {
      aocExecResultIteration.setIndex(idx++);
    }
    int minCritereIdx = AocExecResultIteration.computeMinCritere(resultByTir);
    final AocExecResultIteration iterationTroncature = resultByTir.get(minCritereIdx);
    logger.addLogFormated("Iteration avec RMSE min sur tir %o et valeur %f", minCritereIdx + 1, iterationTroncature.getCritere());

    //le scenario cible doit contenir les valeurs optimales.
    logger.addLogDfrtDclm("Modification du scenario cible avec meileur tir", iterationTroncature.getDfrtValues(), iterationTroncature.getDclmValues());
    writeDfrtAndDclm(iterationTroncature);

    //ecriture trajectoire + results
    CtuluLog log = parentLogs.createNewLog("aoc.exec.write.result");
    writeTrajectoire(resultByTir, log);
    writeSynthese(resultByTir, iterationTroncature, log);

    //le bilan durée + date debut/fin
    File resultatRepetabiliteTrajectoire = new File(aocExecContainer.getReportDirectory(), "resultats_repetabilite_bilan.csv");
    AocExecWriterHelper.writeToFiles(log, AocExecWriterHelper.createBilanHeader(mainTimeInfo), resultatRepetabiliteTrajectoire);

    logger.stop();
  }

  private void writeDfrtAndDclm(AocExecResultIteration iterationWithMinRMSE) {
    AOCStricklerModifier dfrtModifier = new AOCStricklerModifier();
    CtuluLogGroup dftrModifiedLogs = dfrtModifier
        .applyFrottement(aocExecContainer.getProjet(), aocExecContainer.getManagerScenarioCible(), iterationWithMinRMSE.getDfrtValues());
    parentLogs.addGroup(dftrModifiedLogs);
    if (iterationWithMinRMSE.getDclmValues() != null) {
      final CtuluLog logModificationDclm = AOCDclmModifier
          .applyDclms(aocExecContainer.getProjet(), aocExecContainer.getManagerScenarioCible(), iterationWithMinRMSE.getDclmValues());
      parentLogs.addLog(logModificationDclm);
    }
  }

  private void writeTrajectoire(List<AocExecResultIteration> resultByTir, CtuluLog log) {
    File resultatRepetabiliteTrajectoire = new File(aocExecContainer.getReportDirectory(), "resultats_repetabilite_critere.csv");
    ArrayList<String> lines = new ArrayList<>();

    lines.add("Tir;Critère initial;Critère Optimum;Critère Troncature");
    String numberOfIteration = "/" + resultByTir.size();

    resultByTir.forEach(line -> lines.add(
        "Tir " + (line.getIndex() + 1) + numberOfIteration + (line.getIndex() == line.getCritereMinIteration() ? "*" : "")
            + AocExecWriterHelper.DELIMITER + line.getInitialResultat().getCritere()
            + AocExecWriterHelper.DELIMITER + line.getOptimumResultat().getCritere()
            + AocExecWriterHelper.DELIMITER + line.getCritere()
    ));

    AocExecWriterHelper.writeToFiles(log, lines, resultatRepetabiliteTrajectoire);
  }

  /**
   * Ecrit le fichier synthese
   *
   * @param resultByTir
   * @param iterationTroncature
   * @param log
   */
  private void writeSynthese(List<AocExecResultIteration> resultByTir, AocExecResultIteration iterationTroncature, CtuluLog log) {

    ArrayList<String> lines = new ArrayList<>();

    writeFrottements(aocExecContainer, resultByTir, iterationTroncature, lines);
    //ligne blanches
    if (iterationTroncature.getDclmValues() != null) {
      lines.add("");
      writeDebits(aocExecContainer, resultByTir, iterationTroncature, lines);
    }

    File resultatRepetabiliteTrajectoire = new File(aocExecContainer.getReportDirectory(), "resultats_repetabilite_synthese.csv");
    AocExecWriterHelper.writeToFiles(log, lines, resultatRepetabiliteTrajectoire);
  }

  private void writeFrottements(AocExecContainer aocExecContainer, List<AocExecResultIteration> resultByTir, AocExecResultIteration iterationTroncature, ArrayList<String> lines) {
    final AocExecResultIteration aocExecResultIteration = resultByTir.get(0);
    //les noms des frottements triés.

    final String columnsForTirs = IntStream.rangeClosed(1, resultByTir.size()).mapToObj(i -> "Tir " + i).collect(Collectors.joining(AocExecWriterHelper.DELIMITER));
    List<String> frtNames = aocExecResultIteration.getDfrtValues().keySet().stream().sorted().collect(Collectors.toList());
    final NumberFormat dfrtFormater = AocExecFormatterHelper.getDfrtFormater(aocExecContainer);
    lines.add(
        "Nom loi;Min;Max;Moy;" + columnsForTirs + AocExecWriterHelper.DELIMITER + "Valeur pour Optimum" + AocExecWriterHelper.DELIMITER + "Valeur pour Troncature");
    final Map<String, Double> troncatureDfrtValues = iterationTroncature.getDfrtValues();
    final Map<String, Double> optimumDfrtValues = iterationTroncature.getOptimumResultat().getDfrtValues();
    for (String frtName : frtNames) {
      StringJoiner joiner = new StringJoiner(AocExecWriterHelper.DELIMITER);
      joiner.add(frtName);
      MinMaxMoyCompute minMaxMoyContent = new MinMaxMoyCompute();
      //on calcul min, max, moy
      resultByTir.forEach(resultIteration -> minMaxMoyContent.add(resultIteration.getDfrtValues().get(frtName)));

      //on ecrit les valeurs min,max, mox
      joiner.add(dfrtFormater.format(minMaxMoyContent.getMin()));
      joiner.add(dfrtFormater.format(minMaxMoyContent.getMax()));
      joiner.add(dfrtFormater.format(minMaxMoyContent.getMoy()));
      //on ecrit les valeurs sur chaque tir
      resultByTir.forEach(resultIteration -> joiner.add(dfrtFormater.format(resultIteration.getDfrtValues().get(frtName))));
      //l'optimum
      joiner.add(dfrtFormater.format(optimumDfrtValues.get(frtName)));
      //la troncature
      joiner.add(dfrtFormater.format(troncatureDfrtValues.get(frtName)));
      lines.add(joiner.toString());
    }
  }

  private void writeDebits(AocExecContainer aocExecContainer, List<AocExecResultIteration> resultByTir, AocExecResultIteration iterationTroncature, ArrayList<String> lines) {
    final AocExecResultIteration aocExecResultIteration = resultByTir.get(0);
    //les noms des frottements triés.

    if (aocExecResultIteration.getDclmValues() != null) {
      final String columnsForTirs = IntStream.rangeClosed(1, resultByTir.size()).mapToObj(i -> "Tir " + i).collect(Collectors.joining(AocExecWriterHelper.DELIMITER));
      List<AocDclmKey> dclms = aocExecResultIteration.getDclmValues().stream().map(AocDclmDataItem::getDclmKey).sorted().collect(Collectors.toList());

      final NumberFormat debitFormater = AocExecFormatterHelper.getQFormater(aocExecContainer);
      lines.add(
          "DCLM;Min;Max;Moy;" + columnsForTirs + AocExecWriterHelper.DELIMITER + "Valeur pour Optimum");
      final List<AocDclmDataItem> optimumDclmValues = iterationTroncature.getOptimumResultat().getDclmValues();
      //les valeurs sur l'optimum
      final Map<AocDclmKey, Double> optimumValueByDclmKey = getDclmValuesByKeys(optimumDclmValues);
      //les valeurs pour chaque tir dans le meme ordre
      final List<Map<AocDclmKey, Double>> tirsValuesByDclmKey = resultByTir.stream().map(resultIteration -> getDclmValuesByKeys(resultIteration.getDclmValues()))
          .collect(Collectors.toList());

      for (AocDclmKey aocDclmKey : dclms) {
        StringJoiner joiner = new StringJoiner(AocExecWriterHelper.DELIMITER);
        joiner.add(aocDclmKey.toString());
        //calculateur des min,max,moy
        MinMaxMoyCompute minMaxMoyContent = new MinMaxMoyCompute();
        //on calcul min, max, moy
        tirsValuesByDclmKey.forEach(valuesByDclmKey -> minMaxMoyContent.add(valuesByDclmKey.get(aocDclmKey)));

        //on ecrit les valeurs min,max, mox
        joiner.add(debitFormater.format(minMaxMoyContent.getMin()));
        joiner.add(debitFormater.format(minMaxMoyContent.getMax()));
        joiner.add(debitFormater.format(minMaxMoyContent.getMoy()));
        //on ecrit les valeurs sur chaque tir
        tirsValuesByDclmKey.forEach(valuesByDclmKey -> joiner.add(debitFormater.format(valuesByDclmKey.get(aocDclmKey))));
        //l'optimum
        joiner.add(debitFormater.format(optimumValueByDclmKey.get(aocDclmKey)));
        lines.add(joiner.toString());
      }
    }
  }

  private Map<AocDclmKey, Double> getDclmValuesByKeys(List<AocDclmDataItem> optimumDclm) {
    return optimumDclm.stream().collect(Collectors.toMap(AocDclmDataItem::getDclmKey, AocDclmDataItem::getValue));
  }
}

