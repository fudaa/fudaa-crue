package org.fudaa.dodico.crue.aoc.exec;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * @author deniger
 */
public class AocExecTimeInfo {
  private final DateTimeFormatter dtf = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
  /**
   * Utile dans le cas des lancements d'iteration successif.
   */
  AocExecTimeInfo parentTimeInfo;
  private boolean firstTir = true;
  private long tInit;
  private double simulationCrueXDuration;
  private long tInitCrueX;

  public void setParentTimeInfo(AocExecTimeInfo parentTimeInfo) {
    this.parentTimeInfo = parentTimeInfo;
  }

  /**
   * Initialise le tInit du calage. Prend en compte le temps d'initialisation
   * du parent si c'est le premier tir.
   */
  public void startTimer() {
    if (parentTimeInfo != null && parentTimeInfo.isFirstTir()) {
      tInit = parentTimeInfo.tInit;
      parentTimeInfo.clearFirstTir();
    } else {
      tInit = System.currentTimeMillis();
    }
  }

  protected void clearFirstTir() {
    firstTir = false;
  }

  protected boolean isFirstTir() {
    return firstTir;
  }

  public double getSimulationDurationInSec() {
    return (System.currentTimeMillis() - tInit) / 1000d;
  }

  public String getStartDateTime() {
    return dtf.print(tInit);
  }

  public String getEndDateTime() {
    return dtf.print(System.currentTimeMillis());
  }

  public double getSimulationDurationCrueXInSec() {
    return simulationCrueXDuration / 1000d;
  }

  public void startCrueX() {
    tInitCrueX = System.currentTimeMillis();
  }

  public void endCrueX() {
    long delta = System.currentTimeMillis() - tInitCrueX;
    addToCrueXDuration(delta);
    if (parentTimeInfo != null) {
      parentTimeInfo.addToCrueXDuration(delta);
    }
  }

  protected void addToCrueXDuration(long delta) {
    simulationCrueXDuration = simulationCrueXDuration + delta;
  }
}
