package org.fudaa.dodico.crue.aoc.compute;

/**
 * @author deniger
 */
public interface ComputeXYContrat {
  /**
   *
   * @return le nombre de valeurs utilisees pour le calcul
   */
  int getNbValues();

  /**
   *
   * @param x la valeur de l'abscisse
   * @param y la valeur de l'ordonnee
   */
  void addXY(double x, double y);

  double getValue();
}
