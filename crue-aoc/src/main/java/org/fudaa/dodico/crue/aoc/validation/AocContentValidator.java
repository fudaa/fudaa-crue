package org.fudaa.dodico.crue.aoc.validation;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.CrueFileFormatBuilderETU;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.etu.CrueDaoETU;
import org.fudaa.dodico.crue.io.etu.CrueDaoStructureETU;
import org.fudaa.dodico.crue.metier.aoc.EnumAocCalageType;
import org.fudaa.dodico.crue.metier.aoc.EnumAocTypeCalageDonnees;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;

import java.io.File;
import java.util.List;

/**
 * Validation d'une campagne AOC
 *
 * @author f deniger
 */
public class AocContentValidator {
  /**
   * @param aocCampagne la campagne a valider
   * @return la validation concernant la campagne fournie
   */
  public CtuluLogGroup valid(final AocCampagne aocCampagne) {
    final CtuluLogGroup res = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    res.setDescription("aoc.validation.campagne");
    final CtuluLog validationLog = res.createNewLog("aoc.validation.campagne.calage");
    validateEtuAndScenario(aocCampagne, validationLog);

    //test sur le type de calage
    final EnumAocTypeCalageDonnees typeCalageDonnees = aocCampagne.getTypeCalageDonnees();
    if (typeCalageDonnees == null) {
      validationLog.addError("aoc.validation.campagne.typeCalageRequired");
    }
    validationTypeCalage(aocCampagne, validationLog, typeCalageDonnees);

    //l'operation
    validateOperation(aocCampagne, validationLog);
    return res;
  }

  public void validationTypeCalage(final AocCampagne aocCampagne, final CtuluLog validationLog, final EnumAocTypeCalageDonnees typeCalageDonnees) {
    final boolean isTransitoireLimnigramme = EnumAocTypeCalageDonnees.TRANSITOIRE_LIMNIGRAMME.equals(typeCalageDonnees);
    final boolean isTransitoireHydrograme = EnumAocTypeCalageDonnees.TRANSITOIRE_HYDROGRAMME.equals(typeCalageDonnees);

    //calage transitoire critere
    if (isTransitoireHydrograme || isTransitoireLimnigramme) {
      if (aocCampagne.getTypeCalageCritere() == null) {
        validationLog.addError("aoc.validation.campagne.calageTransitoireCritereRequired");
      }
      final boolean useTz = AocValidationHelper.useLoiTZForTransitoire(aocCampagne);
      if (!containsLoiTransitoire(aocCampagne, useTz)) {
        validationLog.addError("aoc.validation.campagne.calageTransitoireNoLoiFound");
      }
    } else if (CollectionUtils.isEmpty(aocCampagne.getDonnees().getLoisCalculsPermanents().getLois())) {
      validationLog.addError("aoc.validation.campagne.calagePermanentNoLoiFound");
    }
  }

  private boolean containsLoiTransitoire(final AocCampagne campagne, final boolean tz) {
    return campagne.getDonnees().getLoisCalculsTransitoires().getLois().stream().anyMatch(
        loi -> {
          if (tz && loi.getLoiRef().startsWith(EnumTypeLoi.LoiTZ.getId())) {
            return true;
          }
          return !tz && loi.getLoiRef().startsWith(EnumTypeLoi.LoiTQ.getId());
        });
  }

  public void validateEtuAndScenario(final AocCampagne aocCampagne, final CtuluLog validationLog) {
    //le fichier etu existe
    final String etuFileName = aocCampagne.getEtudeChemin();
    File etuFile = null;
    if (StringUtils.isEmpty(etuFileName)) {
      validationLog.addError("aoc.validation.campagne.etuFileRequired");
    } else {
      etuFile = new File(etuFileName);
      if (!etuFile.exists()) {
        validationLog.addError("aoc.validation.campagne.etuFileNotFound", etuFile.getAbsolutePath());
      }
    }

    //le scenario est défini et existe
    final String nomScenario = aocCampagne.getNomScenario();
    if (StringUtils.isEmpty(nomScenario)) {
      validationLog.addError("aoc.validation.campagne.scenarioRequired");
    } else if (etuFile != null && etuFile.exists()) {
      final boolean scenarioFound = scenarioExists(nomScenario, etuFile, validationLog);
      if (!scenarioFound) {
        validationLog.addError("aoc.validation.campagne.scenarioNotFound", nomScenario);
      }
    }
    if (nomScenario.length() > ValidationPatternHelper.TAILLE_MAX_NOM_SCENARIO_AOC) {
      validationLog.addError("aoc.validation.campagne.scenarioNameTooLong", nomScenario, nomScenario.length(), ValidationPatternHelper.TAILLE_MAX_NOM_SCENARIO_AOC);
    }
    //le nom du scenario ne doit pas se terminer par une extension utilisé sur les scenarios resultats de campagne.
    if (!ValidationPatternHelper.getScenarioFilterForAoc().test(nomScenario)) {
      validationLog.addError("aoc.validation.campagne.scenarioNameNotValid", nomScenario);
    }
  }

  public void validateOperation(final AocCampagne aocCampagne, final CtuluLog validationLog) {
    final EnumAocCalageType operation = aocCampagne.getCalage().getOperation();
    if (operation == null) {
      validationLog.addError("aoc.validation.campagne.operationRequired");
    }
    final boolean isSeul = EnumAocCalageType.SEUL.equals(operation);
    final boolean isRepetabilite = EnumAocCalageType.TEST_REPETABILITE.equals(operation);
    final boolean isValidation = EnumAocCalageType.VALIDATION_CROISEE.equals(operation);
    if (isRepetabilite || isSeul || isValidation) {
      if (aocCampagne.getCalage().getAlgorithme() == null) {
        validationLog.addError("aoc.validation.campagne.algorithmeRequired");
      }
    }
    if (isSeul) {
      final int val = aocCampagne.getCalage().getNombreIterationSeul();
      if (val < 1) {
        validationLog.addError("aoc.validation.campagne.nbIterSeul");
      }
    }
    if (isRepetabilite) {
      int val = aocCampagne.getCalage().getNombreIterationTir();
      if (val < 1) {
        validationLog.addError("aoc.validation.campagne.nbIterTir");
      }
      val = aocCampagne.getCalage().getNombreTir();
      if (val < 1) {
        validationLog.addError("aoc.validation.campagne.nbTir");
      }
    }
    if (isValidation) {
      final int val = aocCampagne.getCalage().getNombreIterationValidationCroisee();
      if (CollectionUtils.isEmpty(aocCampagne.getDonnees().getValidationCroisee().getGroupes())) {
        validationLog.addError("aoc.validation.campagne.noValidationCroisee");
      }
      if (val < 1) {
        validationLog.addError("aoc.validation.campagne.nbIterationValidationCroisee");
      }
      final double pondApp = aocCampagne.getCalage().getPonderationApprentissage();
      if (pondApp > 1 || pondApp < 0) {
        validationLog.addError("aoc.validation.campagne.pondApp");
      }
      final double pondVal = aocCampagne.getCalage().getPonderationValidation();
      if (pondVal > 1 || pondVal < 0) {
        validationLog.addError("aoc.validation.campagne.pondVal");
      }
    }
  }

  /**
   * @param scenarioNom nom du scenario
   * @param etuFile le fichier etu
   * @param validationLog le log
   * @return true si le nom du scenario est trouvé
   */
  private boolean scenarioExists(final String scenarioNom, final File etuFile, final CtuluLog validationLog) {
    final Crue10FileFormat<EMHProjet> fileFormat = new CrueFileFormatBuilderETU().getFileFormat(null);
    final CrueDaoETU daoETU = (CrueDaoETU) ((CrueDataXmlReaderWriterImpl) fileFormat.getReaderWriter()).readDao(etuFile, validationLog, null);
    if (daoETU != null) {
      final List<CrueDaoStructureETU.Scenario> scenarios = daoETU.getScenarios();
      for (final CrueDaoStructureETU.Scenario scenario : scenarios) {
        if (scenarioNom.equals(scenario.Nom)) {
          return true;
        }
      }
    }
    return false;
  }
}
