package org.fudaa.dodico.crue.aoc.exec;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmDataItem;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmDataItemByEMH;
import org.fudaa.dodico.crue.aoc.helper.AocValidationCroiseeLine;
import org.fudaa.dodico.crue.metier.etude.EMHRun;

import java.util.*;

/**
 * @author deniger
 */
public class AocExecResultIteration {
  /**
   * le coefficient d'amplitude
   */
  private double coefficientAmplitude;
  /**
   * contient le resultat de l'iteration RMSE.
   * dans le cas de la validation croisee contient à la fin l'erreur apprentissage validation
   */
  private double critere;
  /**
   * utilisation pour la validation croisee. Critere d'erreur obtenu sur les lois d'apprentissage
   */
  private double critereApprentissage;
  /**
   * le critere min sur les iterations precedente
   */
  private double critereLocalMin;
  /**
   * le critere min sur toutes les iterations
   */
  private double critereMin;
  /**
   * l'indice de l'iteration correspondant au min du critere sur toutes les iterations
   */
  private int critereMinIteration;
  /**
   * est egal au critere sauf pour le cas de la validation croisee.
   * le champ critere contient à la fin l'erreur apprentissage validation dans le cas de la validation croisee
   */
  private double critereTotal;
  /**
   * utilisation pour la validation croisee. Critere d'erreur obtenu sur les lois de validation
   */
  private double critereValidation;
  private List<AocDclmDataItem> dclmValues;
  /**
   * critere utilise dans le cas recuit
   */
  private double dernierCritere;
  private Map<String, Double> dfrtValues;
  private int index;
  /**
   * Dans le cas de tir multiple, utilise pour stocker les resultats à l'itération initiale.
   * Rappel: dans le cas de tir multiple, c'est le resultat troncature qui est conservé
   */
  private AocExecResultIteration initialResultat;
  private AocEnumIterationType iterationType;
  /**
   * Le delta max des frottements sur l'iteration en cours
   */
  private double maxAmplitudeFrottement;
  private String name;
  /**
   * Dans le cas de tir multiple, utilise pour stocker les resultats à l'itération Optimum ( meilleurs resultats).
   * Rappel: dans le cas de tirs multiples, c'est le resultat troncature qui est conservé.
   * L'instance qui contient ces valeurs seraient donc l'itération troncature.
   */
  private AocExecResultIteration optimumResultat;
  private final Map<String, AocExecResult> resultsByLoi = new LinkedHashMap<>();
  private EMHRun runTroncature;
  private double temperature;
  /**
   * represente le type du critere utilise
   */
  private EnumAocTypeCritere typeCritere;
  /**
   * les donnees de validation croisee
   */
  private AocValidationCroiseeLine validationCroiseeLine;

  public AocExecResultIteration() {
    iterationType = AocEnumIterationType.NORMAL;
  }

  public EnumAocTypeCritere getTypeCritere() {
    return typeCritere;
  }

  /**
   * Met à jour le min rmse dans la liste des iterations.
   *
   * @param iterations les iterations
   */
  public static int computeMinCritere(List<AocExecResultIteration> iterations) {
    if (CollectionUtils.isEmpty(iterations)) {
      return -1;
    }
    int idxRmseMin = 0;
    double min = iterations.get(0).getCritere();
    iterations.get(0).critereLocalMin = min;
    for (int i = 1; i < iterations.size(); i++) {
      double rmse = iterations.get(i).getCritere();
      //
      if (!Double.isNaN(rmse)) {
        if (rmse < min || Double.isNaN(min)) {
          idxRmseMin = i;
          min = rmse;
        }
      }
      iterations.get(i).critereLocalMin = min;
    }
    for (AocExecResultIteration iteration : iterations) {
      iteration.critereMin = min;
      iteration.critereMinIteration = idxRmseMin;
    }
    return idxRmseMin;
  }

  public double getMaxAmplitudeFrottement() {
    return maxAmplitudeFrottement;
  }

  public void setMaxAmplitudeFrottement(double maxAmplitudeFrottement) {
    this.maxAmplitudeFrottement = maxAmplitudeFrottement;
  }

  /**
   * Utiliser uniquement pour les rapports.
   *
   * @return le critere minimal local: le minimal du critere sur cette iteration et les precedentes.
   */
  public double getCritereLocalMin() {
    return critereLocalMin;
  }

  public AocExecResultIteration getInitialResultat() {
    return initialResultat;
  }

  public void setInitialResultat(AocExecResultIteration initialResultat) {
    this.initialResultat = initialResultat;
  }

  public AocExecResultIteration getOptimumResultat() {
    return optimumResultat;
  }

  public void setOptimumResultat(AocExecResultIteration optimumResultat) {
    this.optimumResultat = optimumResultat;
  }

  public AocEnumIterationType getIterationType() {
    return iterationType;
  }

  public void setIterationType(AocEnumIterationType iterationType) {
    this.iterationType = iterationType;
  }

  public int getCritereMinIteration() {
    return critereMinIteration;
  }

  public double getCritereMin() {
    return critereMin;
  }

  public double getCritere() {
    return critere;
  }

  /**
   * @return true si le critere est bien defini et différent de Double.Nan
   */
  public boolean isCritereDefined() {
    return !Double.isNaN(critere);
  }

  /**
   * le critere. Pour la validation croisee, c'est la valeur d'apprentissage lors des tir puis l'erreur ponderée.
   *
   * @param critere la valeur
   */
  public void setCritere(double critere, EnumAocTypeCritere typeCritere) {
    this.critere = critere;
    this.typeCritere = typeCritere;
  }

  public Map<String, Double> getDfrtValues() {
    return dfrtValues;
  }

  public void setDfrtValues(Map<String, Double> dfrtValues) {
    this.dfrtValues = dfrtValues;
  }

  public List<AocDclmDataItem> getDclmValues() {
    return dclmValues;
  }

  public void setDclmValues(List<AocDclmDataItem> dclmValues) {
    this.dclmValues = dclmValues;
  }

  public List<AocDclmDataItemByEMH> getDclmSortedByEMH() {
    return AocDclmDataItem.getByEMHSorted(dclmValues);
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  public AocExecResult createResultByCalcul(String calcul, String nomLigneEau, int ponderation) {
    AocExecResult execResultCalcul = new AocExecResult();
    execResultCalcul.setNomCalcul(calcul, nomLigneEau);
    execResultCalcul.setPonderation(ponderation);
    resultsByLoi.put(nomLigneEau, execResultCalcul);
    return execResultCalcul;
  }

  public List<AocExecResult> getAllResults() {
    List<AocExecResult> list = new ArrayList<>(getResultsByLoi().values());
    list.sort(Comparator.comparing(AocExecResult::getNomCalcul).thenComparing(AocExecResult::getNomLoi));
    return list;
  }

  public Map<String, AocExecResult> getResultsByLoi() {
    return resultsByLoi;
  }

  public boolean isOptimumtricklerAsInt() {
    return AocEnumIterationType.STRICKLER_TRONQUE.equals(iterationType);
  }

  public boolean isNormal() {
    return AocEnumIterationType.NORMAL.equals(iterationType);
  }

  public double getTemperature() {
    return temperature;
  }

  public void setTemperature(double temperature) {
    this.temperature = temperature;
  }

  public void setCoefficientAmplitude(double coefficientAmplitude) {
    this.coefficientAmplitude = coefficientAmplitude;
  }

  public double getDernierCritere() {
    return dernierCritere;
  }

  public void setDernierCritere(double dernierCritere) {
    this.dernierCritere = dernierCritere;
  }

  public double getCritereValidation() {
    return critereValidation;
  }

  public void setCritereValidation(double critereValidation) {
    this.critereValidation = critereValidation;
  }

  public double getCritereApprentissage() {
    return critereApprentissage;
  }

  public void setCritereApprentissage(double critereApprentissage) {
    this.critereApprentissage = critereApprentissage;
  }

  /**
   * @return le critere sur toutes les lois ( validation + apprentissage dans le cas  validation croisee)
   */
  public double getCritereTotal() {
    return critereTotal;
  }

  public void setCritereTotal(double critereTotal) {
    this.critereTotal = critereTotal;
  }

  public AocValidationCroiseeLine getValidationCroiseeLine() {
    return validationCroiseeLine;
  }

  /**
   * les données utilisees pour la validation croisee
   *
   * @param validationCroiseeLine les donnees de la validation croisee
   */
  public void setValidationCroiseeLine(AocValidationCroiseeLine validationCroiseeLine) {
    this.validationCroiseeLine = validationCroiseeLine;
  }

  public EMHRun getRunTroncature() {
    return runTroncature;
  }

  /**
   * @param run le run qui a été generé pour la troncature.
   */
  public void setRunTroncature(EMHRun run) {
    this.runTroncature = run;
  }

  public double getCoefficientAmplitude() {
    return coefficientAmplitude;
  }
}
