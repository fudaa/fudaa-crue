package org.fudaa.dodico.crue.aoc.io;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.projet.*;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.metier.CrueData;

import java.util.ArrayList;
import java.util.List;

/**
 * @author deniger
 */
public class CrueConverterAOC implements CrueDataConverter<CrueDaoAOC, AocCampagne> {
  private final CrueConfigMetier crueConfigMetier;

  public CrueConverterAOC(final CrueConfigMetier crueConfigMetier) {
    this.crueConfigMetier = crueConfigMetier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AocCampagne convertDaoToMetier(final CrueDaoAOC dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
    final AocCampagne metier = new AocCampagne(crueConfigMetier);

    metier.setCommentaire(dao.getCommentaire());
    metier.setAuteurCreation(dao.AuteurCreation);
    metier.setDateCreation(DateDurationConverter.getDate(dao.DateCreation));
    metier.setAuteurModification(dao.AuteurDerniereModif);
    if (dao.DateDerniereModif != null) {
      metier.setDateModification(DateDurationConverter.getDate(dao.DateDerniereModif));
    }
    metier.setEtudeChemin(dao.EtudeAssociee.Chemin);
    metier.setNomScenario(dao.EtudeAssociee.NomScenario);
    metier.setTypeCalageCritere(dao.TypeCalage.Critere);
    metier.setTypeCalageDonnees(dao.TypeCalage.Donnees);
    metier.setLogAll(dao.LogAll);

    convertDaoToMetierCalage(dao, metier);

    //LoisCalculsPermanents
    convertDaoToMetierLoisCalculsPermanents(dao, metier);
    //LoisCalculsTransitoire
    convertDaoToMetierLoiCalculsTransitoires(dao, metier);
    // LoiStrickler
    convertDaoToMetierLoisStrickler(dao, metier);

    // ValidationCroisee
    convertDaoToMetierListeCalculs(dao, metier);
    // Liste de calcul
    convertDaoToMetierValidationCroisee(dao, metier);
    convertDaoToMetierParametresNumeriques(dao, metier);

    return metier;
  }

  private void convertDaoToMetierParametresNumeriques(final CrueDaoAOC dao, final AocCampagne metier) {
    if (dao.ParametresNumeriques != null) {
      metier.getParametreNumeriques().setCoefAmplitude_Recuit(dao.ParametresNumeriques.Amplitude);
      metier.getParametreNumeriques().setTemperatureInitiale_Recuit(dao.ParametresNumeriques.TemperatureInitiale);
      metier.getParametreNumeriques().setTemperatureFinale_Recuit(dao.ParametresNumeriques.TemperatureFinale);
      metier.getParametreNumeriques().setSeuilRMSEQ_CritereTransAOC(dao.ParametresNumeriques.SeuilRMSEQ);
      metier.getParametreNumeriques().setSeuilRMSEZ_CriterePermTransAOC(dao.ParametresNumeriques.SeuilRMSEZ);
      metier.getParametreNumeriques().setSeuilQmax_CritereTransAOC(dao.ParametresNumeriques.SeuilQmax);
      metier.getParametreNumeriques().setSeuilTarriveeMax_CritereTransAOC(dao.ParametresNumeriques.SeuilT);
      metier.getParametreNumeriques().setSeuilZmax_CritereTransAOC(dao.ParametresNumeriques.SeuilZmax);
      metier.getParametreNumeriques().setSeuilQmax_CritereTransAOC(dao.ParametresNumeriques.SeuilQmax);
      metier.getParametreNumeriques().setSeuilVol_CritereTransAOC(dao.ParametresNumeriques.SeuilVol);
    }
  }

  public void convertDaoToMetierCalage(final CrueDaoAOC dao, final AocCampagne metier) {
    metier.getCalage().setAlgorithme(dao.Calage.Algorithme);
    metier.getCalage().setOperation(dao.Calage.Type);

    metier.getCalage().setNombreIterationSeul(dao.Calage.Seul.NombreIteration);
    metier.getCalage().setNombreIterationTir(dao.Calage.TestRepetabilite.NombreIterationTir);
    metier.getCalage().setNombreIterationValidationCroisee(dao.Calage.ValidationCroisee.NombreIteration);
    metier.getCalage().setNombreTir(dao.Calage.TestRepetabilite.NombreTir);
    metier.getCalage().setPonderationApprentissage(dao.Calage.ValidationCroisee.PonderationApprentissage);
    metier.getCalage().setPonderationValidation(dao.Calage.ValidationCroisee.PonderationValidation);
  }

  public void convertDaoToMetierValidationCroisee(final CrueDaoAOC dao, final AocCampagne metier) {
    if (dao.DonneesCampagne.ValidationCroisee != null && dao.DonneesCampagne.ValidationCroisee.groupes != null) {
      final List<CrueDaoStructureAOC.ValidationGroupe> listValidations = dao.DonneesCampagne.ValidationCroisee.groupes;
      if (listValidations != null) {
        for (final CrueDaoStructureAOC.ValidationGroupe validation : listValidations) {
          final AocValidationGroupe addGroupe = metier.getDonnees().getValidationCroisee()
              .addGroupe(
                  validation.Id,
                  validation.Nom,
                  validation.EffectifApprentissage,
                  validation.Commentaire
              );
          if (validation.Lois != null) {
            for (final CrueDaoStructureAOC.ValidationLoi loi : validation.Lois) {
              addGroupe.addLoi(loi.LoiRef);
            }
          }
        }
      }
    }
  }

  public void convertDaoToMetierListeCalculs(final CrueDaoAOC dao, final AocCampagne metier) {
    if (dao.DonneesCampagne.ListeCalculs != null && dao.DonneesCampagne.ListeCalculs.calculs != null) {
      final List<CrueDaoStructureAOC.ParamCalc> listCalculs = dao.DonneesCampagne.ListeCalculs.calculs;
      if (listCalculs != null) {
        for (final CrueDaoStructureAOC.ParamCalc calc : listCalculs) {
          metier.getDonnees().getListeCalculs()
              .addParamCalcul(
                  calc.CalculRef,
                  calc.EMHRef,
                  calc.DeltaQ);
        }
      }
    }
  }

  public void convertDaoToMetierLoisStrickler(final CrueDaoAOC dao, final AocCampagne metier) {
    if (dao.DonneesCampagne.LoisStrickler != null && dao.DonneesCampagne.LoisStrickler.stricklers != null) {
      final List<CrueDaoStructureAOC.LoiStrickler> loiStricklers = dao.DonneesCampagne.LoisStrickler.stricklers;
      if (loiStricklers != null) {
        for (final CrueDaoStructureAOC.LoiStrickler strickler : loiStricklers) {
          metier.getDonnees().getLoisStrickler()
              .addStrickler(
                  strickler.LoiRef,
                  strickler.Min,
                  strickler.Ini,
                  strickler.Max);
        }
      }
    }
  }

  public void convertDaoToMetierLoiCalculsTransitoires(final CrueDaoAOC dao, final AocCampagne metier) {
    if (dao.DonneesCampagne.LoisCalculsTransitoires != null && dao.DonneesCampagne.LoisCalculsTransitoires.calculs != null) {
      final List<CrueDaoStructureAOC.LoiCalculTransitoire> loiCalculTransitoires = dao.DonneesCampagne.LoisCalculsTransitoires.calculs;
      if (loiCalculTransitoires != null) {
        for (final CrueDaoStructureAOC.LoiCalculTransitoire calculPermanent : loiCalculTransitoires) {
          metier.getDonnees().getLoisCalculsTransitoires()
              .addLoiCalculTransitoire(calculPermanent.CalculRef, calculPermanent.LoiRef,
                  calculPermanent.SectionRef,
                  calculPermanent.Ponderation);
        }
      }
    }
  }

  public void convertDaoToMetierLoisCalculsPermanents(final CrueDaoAOC dao, final AocCampagne metier) {
    if (dao.DonneesCampagne.LoisCalculsPermanents != null && dao.DonneesCampagne.LoisCalculsPermanents.calculs != null) {
      final List<CrueDaoStructureAOC.LoiCalculPermanent> loiCalculPermanents = dao.DonneesCampagne.LoisCalculsPermanents.calculs;
      if (loiCalculPermanents != null) {
        for (final CrueDaoStructureAOC.LoiCalculPermanent calculPermanent : loiCalculPermanents) {
          metier.getDonnees().getLoisCalculsPermanents()
              .addLoiCalculPermanent(calculPermanent.CalculRef, calculPermanent.LoiRef,
                  calculPermanent.Ponderation);
        }
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AocCampagne getConverterData(final CrueData in) {
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CrueDaoAOC convertMetierToDao(final AocCampagne metier, final CtuluLog ctuluLog) {
    final CrueDaoAOC dao = new CrueDaoAOC();
    dao.EtudeAssociee = new CrueDaoStructureAOC.EtudeAssociee();
    dao.setCommentaire(metier.getCommentaire());
    dao.AuteurCreation = metier.getAuteurCreation();
    if (metier.getDateCreation() != null) {
      dao.DateCreation = DateDurationConverter.dateToXsd(metier.getDateCreation());
    }
    dao.AuteurDerniereModif = metier.getAuteurModification();
    if (metier.getDateModification() != null) {
      dao.DateDerniereModif = DateDurationConverter.dateToXsd(metier.getDateModification());
    }
    dao.LogAll = metier.isLogAll();
    dao.EtudeAssociee.Chemin = metier.getEtudeChemin();
    dao.EtudeAssociee.NomScenario = metier.getNomScenario();

    dao.TypeCalage = new CrueDaoStructureAOC.TypeCalage();
    dao.TypeCalage.Critere = metier.getTypeCalageCritere();
    dao.TypeCalage.Donnees = metier.getTypeCalageDonnees();

    convertMetierToDaoCalage(metier, dao);

    //donnees campagne
    dao.DonneesCampagne = new CrueDaoStructureAOC.DonneesCampagne();

    //permanent
    convertMetierToDaoLoisCalculsPermanents(metier, dao);

    //transitoire
    convertMetierToDaoLoisCalculsTransitoires(metier, dao);
    //stricklers
    convertMetierToDaoLoisStrickler(metier, dao);
    convertMetierToDaoListeCalculs(metier, dao);
    convertMetierToDaoValidationCroisee(metier, dao);
    convertMetierToDaoParametresNumeriques(metier, dao);
    return dao;
  }

  private void convertMetierToDaoParametresNumeriques(final AocCampagne metier, final CrueDaoAOC dao) {
    dao.ParametresNumeriques = new CrueDaoStructureAOC.ParametresNumeriques();
    dao.ParametresNumeriques.Amplitude = metier.getParametreNumeriques().getCoefAmplitude_Recuit();
    dao.ParametresNumeriques.TemperatureInitiale = metier.getParametreNumeriques().getTemperatureInitiale_Recuit();
    dao.ParametresNumeriques.TemperatureFinale = metier.getParametreNumeriques().getTemperatureFinale_Recuit();
    dao.ParametresNumeriques.SeuilRMSEZ = metier.getParametreNumeriques().getSeuilRMSEZ_CriterePermTransAOC();
    dao.ParametresNumeriques.SeuilRMSEQ = metier.getParametreNumeriques().getSeuilRMSEQ_CritereTransAOC();
    dao.ParametresNumeriques.SeuilT = metier.getParametreNumeriques().getSeuilTarriveeMax_CritereTransAOC();
    dao.ParametresNumeriques.SeuilZmax = metier.getParametreNumeriques().getSeuilZmax_CritereTransAOC();
    dao.ParametresNumeriques.SeuilQmax = metier.getParametreNumeriques().getSeuilQmax_CritereTransAOC();
    dao.ParametresNumeriques.SeuilVol = metier.getParametreNumeriques().getSeuilVol_CritereTransAOC();
  }

  public void convertMetierToDaoLoisStrickler(final AocCampagne metier, final CrueDaoAOC dao) {
    dao.DonneesCampagne.LoisStrickler = new CrueDaoStructureAOC.LoisStrickler();
    dao.DonneesCampagne.LoisStrickler.stricklers = new ArrayList<>();
    for (final AocLoiStrickler loiTransitoire : metier.getDonnees().getLoisStrickler()
        .getLois()) {
      dao.DonneesCampagne.LoisStrickler.stricklers
          .add(new CrueDaoStructureAOC.LoiStrickler(
              loiTransitoire.getLoiRef(),
              loiTransitoire.getMin(),
              loiTransitoire.getIni(),
              loiTransitoire.getMax()));
    }
  }

  public void convertMetierToDaoListeCalculs(final AocCampagne metier, final CrueDaoAOC dao) {
    dao.DonneesCampagne.ListeCalculs = new CrueDaoStructureAOC.ListeCalculs();
    dao.DonneesCampagne.ListeCalculs.calculs = new ArrayList<>();
    for (final AocParamCalc paramCalc : metier.getDonnees().getListeCalculs()
        .getParamCalcs()) {
      dao.DonneesCampagne.ListeCalculs.calculs
          .add(new CrueDaoStructureAOC.ParamCalc(
              paramCalc.getCalculRef(),
              paramCalc.getEmhRef(),
              paramCalc.getDeltaQ()));
    }
  }

  public void convertMetierToDaoValidationCroisee(final AocCampagne metier, final CrueDaoAOC dao) {
    dao.DonneesCampagne.ValidationCroisee = new CrueDaoStructureAOC.ValidationCroisee();
    dao.DonneesCampagne.ValidationCroisee.groupes = new ArrayList<>();
    for (final AocValidationGroupe paramCalc : metier.getDonnees().getValidationCroisee().getGroupes()) {

      final CrueDaoStructureAOC.ValidationGroupe validationGroupe = new CrueDaoStructureAOC.ValidationGroupe(paramCalc.getId(), paramCalc.getNom(),
          paramCalc.getEffectifApprentissage(),
          paramCalc.getCommentaire()
      );
      validationGroupe.Lois = new ArrayList<>();
      if (paramCalc.getLois() != null) {
        for (final AocValidationLoi aocValidationLoi : paramCalc.getLois()) {
          validationGroupe.Lois.add(new CrueDaoStructureAOC.ValidationLoi(aocValidationLoi.getLoiRef()));
        }
      }
      dao.DonneesCampagne.ValidationCroisee.groupes.add(validationGroupe);
    }
  }

  public void convertMetierToDaoLoisCalculsTransitoires(final AocCampagne metier, final CrueDaoAOC dao) {
    dao.DonneesCampagne.LoisCalculsTransitoires = new CrueDaoStructureAOC.LoisCalculsTransitoires();
    dao.DonneesCampagne.LoisCalculsTransitoires.calculs = new ArrayList<>();
    for (final AocLoiCalculTransitoire loiTransitoire : metier.getDonnees().getLoisCalculsTransitoires()
        .getLois()) {
      dao.DonneesCampagne.LoisCalculsTransitoires.calculs
          .add(new CrueDaoStructureAOC.LoiCalculTransitoire(loiTransitoire.getCalculRef(),
              loiTransitoire.getLoiRef(), loiTransitoire.getSectionRef(),
              loiTransitoire.getPonderation()));
    }
  }

  public void convertMetierToDaoLoisCalculsPermanents(final AocCampagne metier, final CrueDaoAOC dao) {
    dao.DonneesCampagne.LoisCalculsPermanents = new CrueDaoStructureAOC.LoisCalculsPermanents();
    dao.DonneesCampagne.LoisCalculsPermanents.calculs = new ArrayList<>();
    for (final AocLoiCalculPermanent loiPermanent : metier.getDonnees().getLoisCalculsPermanents()
        .getLois()) {
      dao.DonneesCampagne.LoisCalculsPermanents.calculs
          .add(new CrueDaoStructureAOC.LoiCalculPermanent(loiPermanent.getCalculRef(),
              loiPermanent.getLoiRef(), loiPermanent.getPonderation()));
    }
  }

  public void convertMetierToDaoCalage(final AocCampagne metier, final CrueDaoAOC dao) {
    dao.Calage = new CrueDaoStructureAOC.Calage();
    dao.Calage.Seul = new CrueDaoStructureAOC.CalageSeul();
    dao.Calage.TestRepetabilite = new CrueDaoStructureAOC.CalageTestRepetabilite();
    dao.Calage.ValidationCroisee = new CrueDaoStructureAOC.CalageValidationCroisee();
    dao.Calage.Algorithme = metier.getCalage().getAlgorithme();
    dao.Calage.Type = metier.getCalage().getOperation();

    dao.Calage.Seul.NombreIteration = metier.getCalage().getNombreIterationSeul();
    dao.Calage.TestRepetabilite.NombreIterationTir = metier.getCalage().getNombreIterationTir();
    dao.Calage.TestRepetabilite.NombreTir = metier.getCalage().getNombreTir();
    dao.Calage.ValidationCroisee.NombreIteration = metier.getCalage().getNombreIterationValidationCroisee();
    dao.Calage.ValidationCroisee.PonderationApprentissage = metier.getCalage().getPonderationApprentissage();
    dao.Calage.ValidationCroisee.PonderationValidation = metier.getCalage().getPonderationValidation();
  }
}
