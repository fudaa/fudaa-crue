package org.fudaa.dodico.crue.aoc.projet;

import org.fudaa.dodico.crue.common.contrat.ObjetNomme;

/**
 * Created by deniger on 30/06/2017.
 */
public class AocValidationLoi implements ObjetNomme {
    private String LoiRef;

    public AocValidationLoi(final String loiRef) {
        LoiRef = loiRef;
    }

    @SuppressWarnings("unused")
    public AocValidationLoi() {
    }

    public AocValidationLoi(final AocValidationLoi aocValidationLoi) {
        this(aocValidationLoi.LoiRef);
    }

    public String getLoiRef() {
        return LoiRef;
    }

    public void setLoiRef(final String loiRef) {
        LoiRef = loiRef;
    }

    @Override
    public String getId() {
        return getLoiRef();
    }

    @Override
    public String getNom() {
        return getLoiRef();
    }

    @Override
    public void setNom(final String newNom) {
        setLoiRef(newNom);
    }
}
