package org.fudaa.dodico.crue.aoc.dclm;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 * @author deniger
 */
public class AocDclmDataItem {
    private final AocDclmKey dclmKey;
    private final double value;
    private final double initDeltaQ;
    private final boolean isZero;

    public AocDclmDataItem(String calculRef, String emhRef, double value, double initDeltaQ, boolean isZero) {
        this(new AocDclmKey(calculRef, emhRef), value, initDeltaQ, isZero);
    }

    public AocDclmDataItem(AocDclmKey key, double value, double initDeltaQ, boolean isZero) {
        this.dclmKey = key;
        this.value = value;
        this.initDeltaQ = initDeltaQ;
        this.isZero = isZero;
    }


    public AocDclmDataItem(AocDclmItem item) {
        this.dclmKey = new AocDclmKey(item.getParamCalc().getCalculRef(), item.getParamCalc().getEmhRef());
        value = item.getInitValue();
        initDeltaQ = item.getParamCalc().getDeltaQ();
        isZero = item.isZero();
    }

    /**
     * @param current              les données actuelles
     * @param coefficientAmplitude le coefficient à appliquer pour le deltaQ soit deltaQUtilise = deltaQ* coefficientAmplitude
     * @return liste avec de nouvelles valeurs aléatoires correspondantes. retourne null si current est null
     */
    public static List<AocDclmDataItem> generateRandomValuesCentered(List<AocDclmDataItem> current, double coefficientAmplitude) {
        if (current == null) {
            return null;
        }
        return current.stream().map(e -> e.generateRandomCenteredValue(coefficientAmplitude)).collect(Collectors.toList());
    }

    public static List<AocDclmDataItemByEMH> getByEMHSorted(List<AocDclmDataItem> list) {
        if (list == null) {
            return null;
        }
        TreeMap<String, AocDclmDataItemByEMH> byEMHTreeMap = new TreeMap<>();
        for (AocDclmDataItem dataItem : list) {
            AocDclmDataItemByEMH dataItemByEMH = byEMHTreeMap.get(dataItem.getEmhRef());
            if (dataItemByEMH == null) {
                dataItemByEMH = new AocDclmDataItemByEMH(dataItem.getEmhRef());
                byEMHTreeMap.put(dataItem.getEmhRef(), dataItemByEMH);
            }
            dataItemByEMH.addValues(dataItem);
        }
        return new ArrayList<>(byEMHTreeMap.values());
    }

    /**
     * @param list
     * @return items sorted with AodDclmKey
     */
    public static TreeMap<AocDclmKey, Double> getByKeys(List<AocDclmDataItem> list) {
        return list.stream().collect(Collectors.toMap(AocDclmDataItem::getDclmKey, AocDclmDataItem::getValue, (oldValue, newValue) -> oldValue, TreeMap::new));
    }

    public AocDclmDataItem generateRandomCenteredValue(double coeffAmplitude) {
        double newValue = value;
        if (!isZero) {
            double deltaToUse = coeffAmplitude * initDeltaQ;
            if (deltaToUse > 0) {
                newValue = ThreadLocalRandom.current().nextDouble(value * (1 - deltaToUse), value * (1 + deltaToUse));
            }
        }
        return new AocDclmDataItem(dclmKey, newValue, initDeltaQ, isZero);
    }

    public AocDclmKey getDclmKey() {
        return dclmKey;
    }

    public double getInitDeltaQ() {
        return initDeltaQ;
    }

    public String getEmhRef() {
        return dclmKey.getEmhRef();
    }

    public String getCalculRef() {
        return dclmKey.getCalculRef();
    }

    public double getValue() {
        return value;
    }
}
