package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.dodico.crue.metier.emh.LoiDF;

import java.text.NumberFormat;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author deniger
 */
public class AocExecResult {
  private final Map<String, AocExecResultByEMH> resultsByVariableName = new TreeMap<>();
  private String nomCalcul;
  private String nomLoi;
  private double critere;
  private int ponderation;
  private LoiDF hydroOrLimniObserved;
  private String section;
  private LoiDF hydroOrLimniCompute;

  public LoiDF getHydroOrLimniObserved() {
    return hydroOrLimniObserved;
  }

  public void setHydroOrLimniObserved(LoiDF hydroOrLimni) {
    this.hydroOrLimniObserved = hydroOrLimni;
  }

  public Map<String, AocExecResultByEMH> getResultsByVariableName() {
    return resultsByVariableName;
  }

  public String getSection() {
    return section;
  }

  public void setSection(String section) {
    this.section = section;
  }

  public String getNomLoi() {
    return nomLoi;
  }

  public AocExecResultByEMH getResultByVariable(String variableName) {
    return resultsByVariableName.get(variableName);
  }

  public AocExecResultByEMH getOrCreateResultByVariable(String variableName, NumberFormat numberFormat) {
    AocExecResultByEMH resultEMH = getResultByVariable(variableName);
    if (resultEMH == null) {
      resultEMH = new AocExecResultByEMH(variableName, numberFormat);
      resultEMH.setNomVariable(variableName);
      resultsByVariableName.put(variableName, resultEMH);
    }
    return resultEMH;
  }

  public String getNomCalcul() {
    return nomCalcul;
  }

  public void setNomCalcul(String nomCalcul, String nomLigneEau) {
    this.nomCalcul = nomCalcul;
    this.nomLoi = nomLigneEau;
  }

  /**
   *
   * @return nomCalcul + nom Loi
   */
  public String getNomComplet(){
    return  nomCalcul+" / "+nomLoi;
  }

  public double getCritere() {
    return critere;
  }

  public void setCritere(double critere) {
    this.critere = critere;
  }

  public int getPonderation() {
    return ponderation;
  }

  public void setPonderation(int ponderation) {
    this.ponderation = ponderation;
  }

  public LoiDF getHydroOrLimniCompute() {
    return hydroOrLimniCompute;
  }

  public void setHydroOrLimniCompute(LoiDF hydroOrLimniCompute) {
    this.hydroOrLimniCompute = hydroOrLimniCompute;
  }
}
