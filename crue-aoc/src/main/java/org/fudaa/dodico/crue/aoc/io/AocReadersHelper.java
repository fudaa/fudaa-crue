package org.fudaa.dodico.crue.aoc.io;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.CrueFileFormatBuilderPCAL;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.pcal.CrueDaoPCAL;
import org.fudaa.dodico.crue.metier.CrueData;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.CatEMHBranche;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.RelationEMHSectionDansBranche;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.ScenarioLoader;
import org.fudaa.dodico.crue.projet.ScenarioLoaderOperation;
import org.joda.time.LocalDateTime;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Permet de lire des données utile pour aoc
 */
public class AocReadersHelper {
  private final EMHProjet projet;

  public AocReadersHelper(EMHProjet projet) {
    this.projet = projet;
  }

  public CrueIOResu<LinkedHashMap<String, Double>> readSection(ManagerEMHScenario scenario) {
    CtuluLogGroup logGroup = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    logGroup.setDescription("loader.readFile");
    CtuluLog log = logGroup.createNewLog("loader.fichiers.scenario.chargt");
    log.setDescriptionArgs(scenario.getNom());
    ScenarioLoader loaderCrue10 = new ScenarioLoader(scenario, projet, projet.getCoeurConfig());
    final ScenarioLoaderOperation load = loaderCrue10.load(null);
    CrueIOResu<LinkedHashMap<String, Double>> resu = new CrueIOResu<>();
    LinkedHashMap<String, Double> sectionPosition = new LinkedHashMap<>();
    resu.setMetier(sectionPosition);
    resu.setAnalyse(load.getLogs().getLastLog());
    final EMHScenario emhScenario = load.getResult();
    if (emhScenario == null) {
      return resu;
    }
    final List<CatEMHBranche> branches = emhScenario.getBranches();
    double initXp = 0;
    for (CatEMHBranche branch : branches) {
      if (branch.getActuallyActive()) {
        final List<RelationEMHSectionDansBranche> sections = branch.getSections();
        for (RelationEMHSectionDansBranche section : sections) {
          double xp = initXp + section.getXp();
          sectionPosition.put(section.getEmhNom(), xp);
        }
      }
      initXp = initXp + branch.getLength();
    }
    return resu;
  }

  public CrueIOResu<LocalDateTime> readDateDebScenario(ManagerEMHScenario scenario) {

    CrueIOResu<LocalDateTime> res = new CrueIOResu<>();
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    res.setAnalyse(log);
    if (scenario == null || scenario.isCrue9()) {
      return res;
    }
    final Crue10FileFormat<CrueData> pcalFileFormat = new CrueFileFormatBuilderPCAL().getFileFormat(projet.getCoeurConfig());
    CrueDataXmlReaderWriterImpl reader = (CrueDataXmlReaderWriterImpl) pcalFileFormat.getReaderWriter();
    Map<String, File> allFilesById = projet.getInputFiles(scenario, (EMHRun) null);
    if (scenario.getListeFichiers().getFile(CrueFileType.PCAL) != null) {
      final String nom = scenario.getListeFichiers().getFile(CrueFileType.PCAL).getNom();
      File pcalFile = allFilesById.get(nom);
      final CrueDaoPCAL dao = (CrueDaoPCAL) reader.readDao(pcalFile, log, null);
      res.setMetier(dao == null ? null : DateDurationConverter.getDate(dao.DateDebSce));
    }
    return res;
  }
}
