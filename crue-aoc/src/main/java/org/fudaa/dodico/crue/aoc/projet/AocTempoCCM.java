package org.fudaa.dodico.crue.aoc.projet;

import org.fudaa.dodico.crue.config.ccm.*;

/**
 * Configuration en dure pour les données AOC en attendant la nouvelle grammaire.
 *
 * @author deniger
 */
public class AocTempoCCM {
    public static final double DEFAULT_DELTA = 0;
    private static ItemVariable deltaQ;

    private AocTempoCCM() {
    }

    public static ItemVariable getDeltaQ() {
        if (deltaQ == null) {
            PropertyTypeNumerique typeNumerique = new PropertyTypeNumerique("Tnu_Reel", "", 1E30);
            PropertyNature nature = new PropertyNature("Nat_Delta", new PropertyEpsilon(0.01, 0.01), "", typeNumerique);
            PropertyValidator validator = new PropertyValidator(new NumberRangeValidator(0d, 1d), new NumberRangeValidator(0d, 0.1));
            deltaQ = new ItemVariable("DeltaQ", DEFAULT_DELTA, nature, validator);
        }
        return deltaQ;
    }
}
