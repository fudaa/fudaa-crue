package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.dodico.crue.aoc.dclm.AocDclmContainer;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmDataItem;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.aoc.projet.AocLoisStrickler;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author deniger
 */
public class AocExecAlgoRecuit implements AocExecAlgoContrat {
  //par rapport au code python le seuil a été divisé par 100 car le rmse dans le code python est multiplié par 100....
  private double seuilAmelioration ;
  private double temperatureInitiale;
  private double temperatureFinale;
  private boolean initialized;
  private double facteurDecroissanceTemperature;
  private double temperatureCourante;
  private double coefficientAmplitude;
  /**
   * Doit contenir l'amplitude max des frottements
   */
  private double maxAmplitudeFrottement;
  private AocExecAlgoResult currentValues;
  private double currentRmse;

  public AocExecAlgoRecuit(AocCampagne campagne) {
    coefficientAmplitude = campagne.getParametreNumeriques().getCoefAmplitude_Recuit();
    seuilAmelioration = campagne.getSeuilToUse();
    temperatureInitiale = campagne.getParametreNumeriques().getTemperatureInitiale_Recuit();
    temperatureFinale = campagne.getParametreNumeriques().getTemperatureFinale_Recuit();
  }

  @Override
  public void fillIterationResultWithParamater(AocExecResultIteration resultIteration) {
    resultIteration.setTemperature(temperatureCourante);
    resultIteration.setCoefficientAmplitude(coefficientAmplitude);
    resultIteration.setMaxAmplitudeFrottement(maxAmplitudeFrottement);
    resultIteration.setDernierCritere(currentRmse);
  }

  @Override
  public AocExecAlgoResult generateForNextIteration(AocExecContainer aocExecContainer, AocDclmContainer dclmBuilder, AocExecAllLogger logger, boolean doIterate) {

    final List<AocExecResultIteration> resultIterations = aocExecContainer.getResultContainer().getResultIterations();
    if (!initialized) {
      seuilAmelioration = aocExecContainer.getCampagne().getSeuilToUse();
      temperatureFinale = aocExecContainer.getCampagne().getParametreNumeriques().getTemperatureFinale_Recuit();
      temperatureInitiale = aocExecContainer.getCampagne().getParametreNumeriques().getTemperatureInitiale_Recuit();
      assert resultIterations.size() == 1;
      final AocExecResultIteration resultIteration = resultIterations.get(0);
      currentRmse = resultIteration.getCritere();
      currentValues = new AocExecAlgoResult(resultIteration.getDfrtValues(), resultIteration.getDclmValues());
      coefficientAmplitude = aocExecContainer.getCampagne().getParametreNumeriques().getCoefAmplitude_Recuit();
      initialized = true;
      facteurDecroissanceTemperature = 1.0 - Math
          .pow((temperatureFinale * temperatureInitiale / temperatureInitiale), (1.0 / (AocExecHelper.getNbIterations(aocExecContainer.getCampagne()))));
      temperatureCourante = temperatureInitiale;
      logger.addLogFormated(";Algo Recuit;Initialisation des parametres numeriques;Amplitude=%f;t0=%f;TFinal=%f;Seuil=%f", coefficientAmplitude,
          temperatureInitiale, temperatureFinale, seuilAmelioration);
      logger.addLogFormated(";Algo Recuit;Initialisation des valeurs;currentCritere=%f;facteurDecroissanceTemperature=%f;temperatureCourante=%f", currentRmse,
          facteurDecroissanceTemperature, temperatureCourante);
    } else {
      if (couldChangeToCurrentValueWithLastIteration(aocExecContainer.getResultContainer(), logger)) {
        final AocExecResultIteration lastIteration = resultIterations.get(resultIterations.size() - 1);
        currentRmse = lastIteration.getCritere();
        currentValues = new AocExecAlgoResult(lastIteration.getDfrtValues(), lastIteration.getDclmValues());
      }
      if (doIterate) {
        temperatureCourante = temperatureCourante * (1.0 - facteurDecroissanceTemperature);
        //on recalcule le coefficient d'amplitude:
        coefficientAmplitude = temperatureCourante / temperatureInitiale;
        logger
            .addLogFormated(";Algo Recuit;Valeurs temperatureCourante et coefficientAmplitude modifiées");
      } else {
        logger
            .addLogFormated(";Algo Recuit;Valeurs temperatureCourante et coefficientAmplitude NON modifiées car erreur Crue10");
      }
    }
    logger.addLogFormated(";Algo Recuit;coefficient Amplitude=%f", coefficientAmplitude);
    logger.addLogFormated(";Algo Recuit;temperatureCourante=%f", temperatureCourante);
    logger.addLogDfrtDclm(";Algo Recuit;valeurs utilisees pour tirage aleatoire", currentValues.getDfrtValues(), currentValues.getDclmValues());
    //on genere les valeurs sur les frottement courants.
    final AocLoisStrickler loisStrickler = aocExecContainer.getCampagne().getDonnees().getLoisStrickler();
    Map<String, Double> dfrtValues = AOCStricklerBuilder
        .buildValuesCentered(loisStrickler, currentValues.getDfrtValues(), coefficientAmplitude);
    maxAmplitudeFrottement = AOCStricklerBuilder.getMaxAmplitude(loisStrickler, coefficientAmplitude);
    List<AocDclmDataItem> dclmdValues = null;
    if (AocExecHelper.debitCouldBeModified(dclmBuilder, aocExecContainer.getCampagne())) {
      dclmdValues = AocDclmDataItem.generateRandomValuesCentered(currentValues.getDclmValues(), coefficientAmplitude);
    }
    logger.addLogDfrtDclm(";Algo Recuit;valeurs obtenues par tirage aleatoire", dfrtValues, dclmdValues);
    return new AocExecAlgoResult(dfrtValues, dclmdValues);
  }

  /**
   * @param aocExecResultContainer les resultats
   * @return true si on peut on utiliser les valeurs de la dernière itération.
   */
  private boolean couldChangeToCurrentValueWithLastIteration(AocExecResultContainer aocExecResultContainer, AocExecAllLogger logger) {

    final List<AocExecResultIteration> resultIterations = aocExecResultContainer.getResultIterations();
    final AocExecResultIteration lastIteration = resultIterations.get(resultIterations.size() - 1);
    double lastRmse = lastIteration.getCritere();
    logger.addLogFormated(";Algo Recuit;Comparaison Critere precedente iteration=%f avec Critere courant=%f", lastRmse, currentRmse);
    if (!lastIteration.isCritereDefined()) {
      logger.addLogFormated(";Algo Recuit;la dernière itération n'a pas donné de valeur définie.");
      return false;
    }

    //il y a amélioration on peut changer
    if (lastRmse - currentRmse < seuilAmelioration) {
      logger.addLogFormated(";Algo Recuit;Amélioration détectée => coefficients dfrt et dclm modifiés");
      return true;
    }
    //sinon on applique l'algo de recuit simulé
    final int aocExecResultIteration = AocExecResultIteration.computeMinCritere(resultIterations);
    double minCritere = resultIterations.get(aocExecResultIteration).getCritere();
    double test = Math.exp(-(lastRmse - minCritere) / temperatureCourante);
    //entre 0 et 1
    double randomValue = ThreadLocalRandom.current().nextDouble();
    logger.addLogFormated(";Algo Recuit;Amélioration non détectée -> calcul de fc=%f et R=%f ( si fc > R modification)", test, randomValue);
    boolean canModified = test > randomValue;
    if (canModified) {
      logger.addLogFormated(";Algo Recuit;fc>R => coefficients dfrt et dclm modifiés");
    } else {
      logger.addLogFormated(";Algo Recuit;fc<=R => coefficients dfrt et dclm NON modifiés");
    }

    return canModified;
  }
}
