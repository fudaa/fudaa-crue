package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmDataItem;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmContainer;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.metier.etude.EMHRun;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Classe executant les iterations
 *
 * @author deniger
 */
public class AocExecIteratorMinMax implements AocExecIteratorContrat {
  private final AocExecProcessor parent;
  AocExecRunLauncher currentLauncher;

  public AocExecIteratorMinMax(AocExecProcessor parent) {
    this.parent = parent;
  }

  /**
   * @param in
   */
  public CrueOperationResult<AocExecContainer> compute(CrueOperationResult<AocExecContainer> in, ProgressionUpdater progressionUpdater, String prefix, AocExecAllLogger logger) {
    final AocExecContainer aocExecContainer = in.getResult();
    //pour extraire les resultats
    AocExecResultExtractor extractor = new AocExecResultExtractor(aocExecContainer);

    //utiliser pour afficher le temps pris par le calcul
    //les resultats:
    List<AocExecResultIteration> iterations = new ArrayList<>();
    in.getResult().getResultContainer().setResultIterations(iterations);
    //pour la gestion des dclm
    final AocDclmContainer dclmBuilder = AocDclmContainer
        .create(aocExecContainer.getCampagne(), aocExecContainer.getScenarioReference(), aocExecContainer.getProjet().getPropDefinition());
    final boolean debitCouldBeModified = dclmBuilder.hasDeltaQ();

    final int iterationForMax = 1;
    logger.addLogFormated("Analyse sensibilite avec 2 iterations");
    //pour cette operation, le calcul de sensibilite est opérée sur dclm et dfrt.
    if (!dclmBuilder.hasDeltaQ()) {
      logger.addLogFormated("Pas de variations demandées sur le débit ( deltaQ=0)");
    }
    for (int i = 0; i < 2; i++) {
      final CtuluLogGroup logsIteration = in.getLogs().createGroup("aoc.iteration.log");

      //initialisation des valeurs
      Map<String, Double> dfrtValues;
      AocEnumIterationType iterationType;
      String iterationName;
      List<AocDclmDataItem> dclmValues = null;
      if (i == iterationForMax) {
        iterationName = "max";
        iterationType = AocEnumIterationType.SENSIBILITE_MAX;
        //cf CRUE-793
        //SENSIBILITE_MAX : il faut les coeff. de Strickler min (rivière rugueuse) et les débit max
        dfrtValues = AOCStricklerBuilder.buildMinValue(aocExecContainer.getCampagne().getDonnees().getLoisStrickler());
        if (debitCouldBeModified) {
          dclmValues = dclmBuilder.generateMaxValues();
        }
        //on ajoute une ligne blanche
        logger.addLogFormated("");
      } else {
        iterationType = AocEnumIterationType.SENSIBILITE_MIN;
        iterationName = "min";
//        SENSIBILITE_MIN : il faut les coeff. de Strickler max (rivière lisse) et les débit min
        dfrtValues = AOCStricklerBuilder.buildMaxValue(aocExecContainer.getCampagne().getDonnees().getLoisStrickler());
        if (debitCouldBeModified) {
          dclmValues = dclmBuilder.generateMinValues();
        }
      }
      logsIteration.setDescriptionArgs(iterationName);

      //Ecriture des fichiers
      //frottements toujours modifiés
      AOCStricklerModifier dfrtModifier = new AOCStricklerModifier();
      CtuluLogGroup dftrModifiedLogs = dfrtModifier.applyFrottement(aocExecContainer.getProjet(), aocExecContainer.getManagerScenarioCible(), dfrtValues);
      logsIteration.addGroup(dftrModifiedLogs.createCleanGroup());
      if (dftrModifiedLogs.containsError()) {
        deleteAllRunsInCible(in);
        return in;
      }

      //dclm que si deltaQ>0
      if (debitCouldBeModified) {
        final CtuluLog logModificationDclm = AOCDclmModifier.applyDclms(aocExecContainer.getProjet(), aocExecContainer.getManagerScenarioCible(), dclmValues);
        if (logModificationDclm.isNotEmpty()) {
          logsIteration.addLog(logModificationDclm);
          if (logModificationDclm.containsErrorOrSevereError()) {
            deleteAllRunsInCible(in);
            return in;
          }
        }
      }

      //Des logs dans le fichier .log
      logger.addLogFormated("Iteration " + iterationType);
      logger.addLogDfrtDclm(";Utilisation des valeurs " + iterationName, dfrtValues, dclmValues);

      if (parent.isStop()) {
        deleteAllRunsInCible(in);
        return in;
      }
      //step 2: lancer un run complet
      currentLauncher = parent.createLauncher(aocExecContainer);
      //demarrage crueX avec chronométrage:
      in.getResult().getResultContainer().getTimeInfo().startCrueX();
      CrueOperationResult<EMHRun> runCreated = currentLauncher.launchRun(logsIteration);

      in.getResult().getResultContainer().getTimeInfo().endCrueX();
      boolean error = currentLauncher.isComputeFinishedWithError();
      if (error) {
        logger.addLogDfrtDclm("Echec Crue 10 sur iteration " + iterationName, dfrtValues, dclmValues);
        final CtuluLog logErrorCrueX = logsIteration.createNewLog("aoc.iteration.crue10Failure.log");
        logErrorCrueX.addError("aoc.iteration.crue10Failure.iteration", iterationName);
        in.getResult().getResultContainer().addFatalErrorCrue10(dfrtValues, dclmValues, i);
      }
      if (parent.isStop() || runCreated.isErrorOrEmpty()) {
        deleteAllRunsInCible(in);
        return in;
      }

      CrueOperationResult<AocExecResultReader> runLoadOperation = currentLauncher.loadRun(runCreated.getResult());
      logsIteration.addGroup(runLoadOperation.getLogs().createCleanGroup());
      if (runLoadOperation.isErrorOrEmpty()) {
        deleteAllRunsInCible(in);
        return in;
      }
      final CtuluLog logExtractionResultat = logsIteration.createNewLog("aoc.exec.extractionResultat");
      logExtractionResultat.setDescriptionArgs(iterationName);
      extractor.setOperationLog(logExtractionResultat);
      //apres le calcul, on extrait les données de l'iteration
      AocExecResultIteration iteration = extractor.create(runLoadOperation.getResult(), dfrtValues, dclmValues, logger, 0);
      iteration.setIterationType(iterationType);
      iterations.add(iteration);
    }
    logger.addLogFormated("Fin iterations");
    //on efface les runs
    deleteAllRunsInCible(in);
    //on recherche le critere optimal
    AocExecResultIteration.computeMinCritere(iterations);
    in.getResult().getResultContainer().setResultIterations(iterations);
    return in;
  }

  public void deleteAllRunsInCible(CrueOperationResult<AocExecContainer> in) {
    currentLauncher.deleteAllRunsAndSave(in.getResult().getManagerScenarioCible(),in.getLogs());
  }

  public void stop() {
    if (currentLauncher != null) {
      currentLauncher.stop();
    }
  }
}
