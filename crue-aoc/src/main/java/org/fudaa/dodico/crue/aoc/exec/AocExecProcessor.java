package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.crue.aoc.exec.writer.AocExecResultWriter;
import org.fudaa.dodico.crue.aoc.exec.writer.AocExecResultWriterMinMax;
import org.fudaa.dodico.crue.aoc.exec.writer.AocExecResultWriterRepetabilite;
import org.fudaa.dodico.crue.aoc.exec.writer.AocExecResultWriterValidationCroisee;
import org.fudaa.dodico.crue.aoc.helper.AocValidationCroiseeLine;
import org.fudaa.dodico.crue.aoc.helper.AocValidationCroiseeLineBuilder;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculPermanent;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.config.coeur.CoeurManager;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.projet.calcul.CalculCrueRunnerManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author deniger
 */
public class AocExecProcessor {
  private final CoeurManager coeurManager;
  private final ConnexionInformation connexionInformation;
  private final CalculCrueRunnerManager calculCrueRunnerManager;
  AocExecIteratorContrat aocExecIterator;
  boolean stop;

  public AocExecProcessor(CoeurManager coeurManager, ConnexionInformation connexionInformation, CalculCrueRunnerManager calculCrueRunnerManager) {
    this.coeurManager = coeurManager;
    this.connexionInformation = connexionInformation;
    this.calculCrueRunnerManager = calculCrueRunnerManager;
  }

  public AocExecRunLauncher createLauncher(AocExecContainer aocExecLaunchRun) {
    return new AocExecRunLauncher(aocExecLaunchRun, calculCrueRunnerManager, connexionInformation);
  }

  public void stop() {
    stop = true;
    if (aocExecIterator != null) {
      aocExecIterator.stop();
    }
  }

  public boolean isStop() {
    return stop;
  }

  public CrueOperationResult<AocExecContainer> launch(File aocFile, ProgressionInterface progressAdapter) {
    //pour les infos sur les durées des calages
    AocExecTimeInfo parentMainTimeInfo = new AocExecTimeInfo();
    parentMainTimeInfo.startTimer();

    AocExecContainerLoader loader = new AocExecContainerLoader(coeurManager);
    progressAdapter.setDesc(BusinessMessages.getString("aoc.exec.loadContainers"));
    final CrueOperationResult<AocExecContainer> load = loader.load(aocFile);
    final AocExecContainer aocExecContainer = load.getResult();
    if (aocExecContainer == null) {
      return load;
    }
    if (stop) {
      return load;
    }
    progressAdapter.setDesc(BusinessMessages.getString("aoc.exec.createScenario"));
    final CrueOperationResult<AocExecContainer> operationResult = loader.build(load, connexionInformation);
    if (operationResult.getLogs().containsError()) {
      return operationResult;
    }

    //on lance l'algo
    final AocCampagne campagne = aocExecContainer.getCampagne();
    aocExecIterator = AocExecHelper.isAnaylseSensibilite(campagne) ? new AocExecIteratorMinMax(this) : new AocExecIterator(this);
    int nbTirs = AocExecHelper.getNbTirs(campagne);
    List<AocValidationCroiseeLine> lines = null;
    final boolean validationCroisee = AocExecHelper.isValidationCroisee(campagne);
    if (validationCroisee) {
      lines = new AocValidationCroiseeLineBuilder()
          .generatePermutations(campagne.getDonnees().getValidationCroisee());
      Map<String, String> calculByLois = campagne.getDonnees().getLoisCalculsPermanents().getLois().stream()
          .collect(Collectors.toMap(AocLoiCalculPermanent::getLoiRef, AocLoiCalculPermanent::getCalculRef));
      lines.forEach(line -> line.updateCalculNames(calculByLois));
      nbTirs = lines.size();
    }
    final int nbIterationMax = AocExecHelper.getNbIterationsTotal(campagne) * nbTirs;
    AocExecAllLogger logger = new AocExecAllLogger(aocExecContainer, load.getLogs().createNewLog("logger"));

    //contient les meilleurs résultats par itérations
    List<AocExecResultIteration> resultByTir = new ArrayList<>();

    ProgressionUpdater updater = new ProgressionUpdater(progressAdapter, true);
    updater.setValue(100, nbIterationMax);

    //on efface les runs au démarrage
    createLauncher(aocExecContainer).deleteAllRunsAndSave(aocExecContainer.getManagerScenarioCible(), operationResult.getLogs());

    //a chaque fin de tir le run troncature est gardé
    for (int i = 0; i < nbTirs; i++) {
      //Demarrage du tir avec initialisation des timers
      aocExecContainer.clearResult(parentMainTimeInfo);
      aocExecContainer.getResultContainer().getTimeInfo().startTimer();

      String suffixe = "";
      String prefixForProgressionUpdater = "";
      if (nbTirs > 1) {
        suffixe = "_" + (i + 1);
        prefixForProgressionUpdater = "Tir " + (i + 1) + "/" + nbTirs + ": ";
      }
      logger.start(suffixe);

      if (validationCroisee) {
        aocExecContainer.getResultContainer().setValidationCroiseeLine(lines.get(i));
      }

      aocExecIterator.compute(operationResult, updater, prefixForProgressionUpdater, logger);
      logger.stop();
      if (nbTirs > 1) {
        aocExecContainer.getResultContainer().setSuffix(suffixe);
      }

      if (!operationResult.getLogs().containsError() && !operationResult.getResult().getResultContainer().isStoppedDueToCrue10Error()) {
        //les valeurs avec les strickler tronqués en entier doivent être prise en compte
        //on stocke les 2 autres valeurs pour log/ecriture de résultats
        final AocExecResultIteration iterationWithOptimumStricklerAsInteger = aocExecContainer.getResultContainer().getIterationWithOptimumStricklerAsInteger();
        //cette iteration n'est pas toujours créée (analyse de sensibilité par exemple)
        if(iterationWithOptimumStricklerAsInteger!=null) {
          iterationWithOptimumStricklerAsInteger.setInitialResultat(aocExecContainer.getResultContainer().getResultIterations().get(0));
          iterationWithOptimumStricklerAsInteger.setOptimumResultat(aocExecContainer.getResultContainer().getIterationWithMinCritere());
          resultByTir.add(iterationWithOptimumStricklerAsInteger);
        }
      }
      progressAdapter.setDesc(prefixForProgressionUpdater + BusinessMessages.getString("aoc.exec.progression.writeResults"));

      //analyse de sensibilté
      if (AocExecHelper.isAnaylseSensibilite(campagne)) {
        final CtuluLog writeMinMax = new AocExecResultWriterMinMax().write(aocExecContainer);
        operationResult.getLogs().addLog(writeMinMax);
        //autre cas:
      } else {
        final CtuluLog write = new AocExecResultWriter().write(aocExecContainer);
        operationResult.getLogs().addLog(write);
      }
    }
    //repetabilite
    if (resultByTir.size() > 1 && AocExecHelper.isTestRepetabilite(campagne)) {
      new AocExecResultWriterRepetabilite(aocExecContainer, logger, operationResult.getLogs()).writeResultats(resultByTir, parentMainTimeInfo);
    }
    if (validationCroisee) {
      new AocExecResultWriterValidationCroisee(aocExecContainer, logger, operationResult.getLogs()).writeResultats(resultByTir, parentMainTimeInfo);
    }
    //conservation du run pour le meilleur tir
    if (resultByTir.size() > 1) {
      logger.start("suppression_runs");
      logger.addLogFormated("Seul le run donnant le meilleur resultat troncature sera conserve");
      List<EMHRun> runsToDelete = new ArrayList<>();
      for (AocExecResultIteration tir : resultByTir) {
        if (tir.getRunTroncature() != null) {
          if (tir.getIndex() != tir.getCritereMinIteration()) {
            logger.addLogFormated("Suppression run %s (tir %o)", tir.getRunTroncature().getNom(), tir.getIndex() + 1);
            runsToDelete.add(tir.getRunTroncature());
          } else {
            logger.addLogFormated("Conservation run %s (tir %o) car meilleure troncature", tir.getRunTroncature().getNom(), tir.getIndex() + 1);
          }
        }
      }
      createLauncher(aocExecContainer).deleteRunsAndSave(aocExecContainer.getManagerScenarioCible(), runsToDelete, operationResult.getLogs());
      logger.stop();
    }
    return operationResult;
  }
}
