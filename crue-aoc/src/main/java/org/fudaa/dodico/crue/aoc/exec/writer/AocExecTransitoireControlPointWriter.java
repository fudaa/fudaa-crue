package org.fudaa.dodico.crue.aoc.exec.writer;

import org.fudaa.dodico.crue.aoc.exec.AocExecContainer;
import org.fudaa.dodico.crue.metier.emh.Loi;

import java.text.NumberFormat;
import java.util.*;

/**
 * Permet d'écrire dans un tableau les valeurs de 2 loisT avec les différences.
 * Ne fait pas d'interpolation et se base sur les formatage des temps pour retrouver les valeurs sur le meme pas de teps
 *
 * @author deniger
 */
public class AocExecTransitoireControlPointWriter {
  private final Map<String, Point> pointsByTime = new HashMap<>();
  private final NumberFormat timeFormatter;
  private final NumberFormat valueFormatter;

  public AocExecTransitoireControlPointWriter(NumberFormat timeFormatter, NumberFormat valueFormatter) {
    this.timeFormatter = timeFormatter;
    this.valueFormatter = valueFormatter;
  }

  public static AocExecTransitoireControlPointWriter create(Loi loi, AocExecContainer aocExecContainer) {
    final NumberFormat tFormater = AocExecFormatterHelper.getAbscisseFormater(aocExecContainer, loi);
    final NumberFormat yFormater = AocExecFormatterHelper.getOrdonneeFormater(aocExecContainer, loi);
    return new AocExecTransitoireControlPointWriter(tFormater, yFormater);
  }

  /**
   * @param loi les valeurs mesurees
   */
  public void addValuesObserved(Loi loi) {
    loi.getEvolutionFF().getPtEvolutionFF().forEach(pt -> addValueObserved(pt.getAbscisse(), pt.getOrdonnee()));
  }

  /**
   * @param loi les valeurs calculees
   */
  public void addValuesComputed(Loi loi) {
    loi.getEvolutionFF().getPtEvolutionFF().forEach(pt -> addValueComputed(pt.getAbscisse(), pt.getOrdonnee()));
  }

  /**
   * @param prefixTime le prefix pour la ligne contenant le temps
   * @param prefixObserved le prefix pour la ligne contenant les données mesurees
   * @param prefixComputed le prefix pour la ligne contenant les données calcules
   * @param prefixDelta le prefix pour la ligne contenant le delta
   * @param lines la lsite qui contiendra 4 lignes de plus avec les temps, les valeurs mesurees, les valeurs calculees et les delta
   */
  public void addToLines(String prefixTime, String prefixObserved, String prefixComputed, String prefixDelta, List<String> lines) {
    List<Point> points = new ArrayList<>(pointsByTime.values());
    Collections.sort(points, new PointComparator());
    addTimesValues(prefixTime, lines, points);
    addObservedValuesFormattedInList(prefixObserved, lines, points);
    addComputedValuesFormattedInList(prefixComputed, lines, points);
    addDeltaValuesFormattedInList(prefixDelta, lines, points);
  }

  private void addTimesValues(String prefixTime, List<String> lines, List<Point> points) {
    StringJoiner joiner = new StringJoiner(AocExecWriterHelper.DELIMITER);
    if (prefixTime != null) {
      joiner.add(prefixTime);
    }
    points.forEach(p -> joiner.add(p.timeAsString));
    lines.add(joiner.toString());
  }

  private void addObservedValuesFormattedInList(String prefixObserved, List<String> lines, List<Point> points) {
    StringJoiner joiner = new StringJoiner(AocExecWriterHelper.DELIMITER);
    if (prefixObserved != null) {
      joiner.add(prefixObserved);
    }
    points.forEach(p -> joiner.add(p.getObservedAsString(valueFormatter)));
    lines.add(joiner.toString());
  }

  private void addComputedValuesFormattedInList(String prefixComputed, List<String> lines, List<Point> points) {
    StringJoiner joiner = new StringJoiner(AocExecWriterHelper.DELIMITER);
    if (prefixComputed != null) {
      joiner.add(prefixComputed);
    }
    points.forEach(p -> joiner.add(p.getComputedAsString(valueFormatter)));
    lines.add(joiner.toString());
  }

  private void addDeltaValuesFormattedInList(String prefixDelta, List<String> lines, List<Point> points) {
    StringJoiner joiner = new StringJoiner(AocExecWriterHelper.DELIMITER);
    if (prefixDelta != null) {
      joiner.add(prefixDelta);
    }
    points.forEach(p -> joiner.add(p.getDeltaAsString(valueFormatter)));
    lines.add(joiner.toString());
  }

  /**
   * @param time le temps
   * @param value la valeur mesuree
   */
  public void addValueObserved(double time, double value) {
    String timeAsString = timeFormatter.format(time);
    Point pt = getPoint(time, timeAsString);
    pt.setValueObserved(value);
  }

  /**
   * @param time le temps
   * @param value la valeur calculee
   */
  public void addValueComputed(double time, double value) {
    String timeAsString = timeFormatter.format(time);
    Point pt = getPoint(time, timeAsString);
    pt.setValueComputed(value);
  }

  private Point getPoint(double time, String timeAsString) {
    Point pt = pointsByTime.get(timeAsString);
    if (pt == null) {
      pt = new Point(timeAsString, time);
      pointsByTime.put(timeAsString, pt);
    }
    return pt;
  }

  public static class Point {
    final String timeAsString;
    final double time;
    double valueObserved;
    boolean hasValueObserved;
    boolean hasValueComputed;
    double valueComputed;

    public Point(String timeAsString, double time) {
      this.timeAsString = timeAsString;
      this.time = time;
    }

    public String getObservedAsString(NumberFormat valueFormatter) {
      if (hasValueObserved) {
        return AocExecFormatterHelper.formatDoubleValue(valueFormatter, valueObserved);
      }
      return "";
    }

    public String getComputedAsString(NumberFormat valueFormatter) {
      if (hasValueComputed) {
        return AocExecFormatterHelper.formatDoubleValue(valueFormatter, valueComputed);
      }
      return "";
    }

    public String getDeltaAsString(NumberFormat valueFormatter) {
      if (hasValueComputed && hasValueObserved) {
        return AocExecFormatterHelper.formatDoubleValue(valueFormatter, valueObserved - valueComputed);
      }
      return "";
    }

    public void setValueComputed(double valueComputed) {
      hasValueComputed = true;
      this.valueComputed = valueComputed;
    }

    public void setValueObserved(double valueObserved) {
      this.valueObserved = valueObserved;
      hasValueObserved = true;
    }
  }

  public static class PointComparator implements Comparator<Point> {
    @Override
    public int compare(Point o1, Point o2) {
      if (o1.time > o2.time) {
        return 1;
      }
      if (o1.time < o2.time) {
        return -1;
      }
      return 0;
    }
  }
}
